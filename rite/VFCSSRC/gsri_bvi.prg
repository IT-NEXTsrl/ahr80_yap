* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bvi                                                        *
*              Distinte versamento per F24                                     *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-08                                                      *
* Last revis.: 2007-03-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bvi",oParentObject,m.pParame)
return(i_retval)

define class tgsri_bvi as StdBatch
  * --- Local variables
  pParame = space(2)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_CAMBIO = 0
  w_IMPORTO = 0
  w_DATAPRES = ctod("  /  /  ")
  w_TIPRIT = space(1)
  w_MFMESRI1 = 0
  w_MFANNRI1 = 0
  w_CAMVER = 0
  w_DECTOT = 0
  w_PCURSOR = space(10)
  w_ANNO = space(4)
  w_MESE = space(2)
  w_VALVER = space(3)
  w_RIFDISTI = space(10)
  w_NUMERO = 0
  w_DATAREG = ctod("  /  /  ")
  w_MESS = space(100)
  w_MESS1 = space(100)
  w_MEMO = space(0)
  w_ELABORA = .f.
  w_PARAMET = space(2)
  w_CODAZI = space(5)
  w_SEDE = space(5)
  * --- WorkFile variables
  VALUTE_idx=0
  TAB_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene lanciato dall'anagrafica GSCG_AMF solo se attivato il modulo ritenute.
    * --- Pu� essere lanciato dalla pagina erario se il parametro assume il valore 'IR',
    * --- dalla pagina INPS se il parametro assume 'IN' ed infine da altri enti se il parametro vale 'PR'.
    * --- Lancio questo batch per preparare il cursore da inserire nello zoom dei versamenti.
    * --- Variabile utilizzate per poter filtrare l'anno ed il mese di riferimento
    * --- Data presentazione F24
    * --- Cambio della valuta del F24
    * --- Numero decimali
    * --- Variabile utilizzata per leggere il nome del cursore.Cursore contenente versamenti da generare.
    * --- Riferimento distinta di versamento
    * --- Variabile contenente la messaggistica di errore.
    * --- Serve per poter sapere ,nella maschera Gscg_Kvi, se ho almeno una distinta importabile nel modello F24
    * --- Variabile contenente il valore del parametro.Utilizzata nella maschera Gsri_kvi
    * --- Variabile contenente il codice azienda.Serve nel caso di versamenti I.N.P.S.
    * --- Effettuo queste due operazioni per avere il valore all'interno della maschera GSCG_KVI
    this.w_oMESS=createobject("AH_MESSAGE")
    this.w_ANNO = this.oParentObject.w_Mfannrif
    this.w_MESE = this.oParentObject.w_Mfmesrif
    this.w_VALVER = this.oParentObject.w_Mfvaluta
    this.w_CODAZI = I_CODAZI
    this.w_PCURSOR = sys(2015)
    this.w_PARAMET = this.pParame
    * --- Se figlio non instanziato lo instanzio
    if Upper( This.OparentObject.Gscg_avf.class)="STDDYNAMICCHILD"
      This.OparentObject.oPgFrm.Pages[7].opag.uienable(.T.)
    endif
    this.w_DATAPRES = this.oParentObject.GSCG_AVF.w_VFDTPRES
    do case
      case (this.w_DATAPRES < Cp_CharToDate("01-01-2002") and this.oParentObject.w_TIPOMOD="NEW" and this.oParentObject.w_ANNORIF="2002") or (this.w_DATAPRES < Cp_CharToDate("01-01-2003") and this.oParentObject.w_TIPOMOD="NEW" and this.oParentObject.w_ANNORIF="2003")
        this.w_MESS = "La data di presentazione del modello deve essere successiva al 01-01-%1%0Impossibile proseguire"
        ah_ERRORMSG(this.w_MESS,48,"", this.oParentObject.w_ANNORIF)
        i_retcode = 'stop'
        return
      case (this.w_DATAPRES >= Cp_CharToDate("01-01-2002") and this.oParentObject.w_TIPOMOD="OLD" and this.oParentObject.w_ANNORIF="2002") or (this.w_DATAPRES >= Cp_CharToDate("01-01-2003") and this.oParentObject.w_TIPOMOD="OLD" and this.oParentObject.w_ANNORIF="2003")
        this.w_MESS = "La data di presentazione del modello deve essere antecedente al 01-01-%1%0Impossibile proseguire"
        ah_ERRORMSG(this.w_MESS,48,"", this.oParentObject.w_ANNORIF)
        i_retcode = 'stop'
        return
    endcase
    do case
      case this.w_DATAPRES < Cp_CharToDate("01-01-2001") and this.oParentObject.w_TIPOMOD="NEW" and this.oParentObject.w_ANNORIF="2001"
        this.w_MESS = "La data di presentazione del modello deve essere successiva al 01-01-%1%0Impossibile proseguire"
        ah_ERRORMSG(this.w_MESS,48,"", this.oParentObject.w_ANNORIF)
        i_retcode = 'stop'
        return
      case this.w_DATAPRES >= Cp_CharToDate("01-01-2001") and this.oParentObject.w_TIPOMOD="OLD" and this.oParentObject.w_ANNORIF="2001"
        this.w_MESS = "La data di presentazione del modello deve essere antecedente al 01-01-%1%0Impossibile proseguire"
        ah_ERRORMSG(this.w_MESS,48,"", this.oParentObject.w_ANNORIF)
        i_retcode = 'stop'
        return
    endcase
    * --- Effettuo una lettura sulla tabella Valute per avere il cambio ed i decimali corretti.
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_MFVALUTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_MFVALUTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAMVER = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Effettuo questo assegnamento per poter convertire le variabili w_MFANNRIF e
    *     w_MFMESRIF da carattere a numerici
    this.w_MFANNRI1 = VAL(this.oParentObject.w_MFANNRIF)
    this.w_MFMESRI1 = VAL(this.oParentObject.w_MFMESRIF)
    do case
      case this.pParame="IR"
        * --- Lancio la query per poter recuperare i dati da inserire nello zoom.Versamenti I.R.PE.F.
        vq_exec("..\rite\exe\query\gsri_bvi.vqr",this,"VERSA")
      case this.pParame="IN"
        * --- Effettuo una lettura nella tabella Ritenute per controllare se � valorizzato il codice della sede.
        this.w_TIPRIT = "A"
        * --- Read from TAB_RITE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TAB_RITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2],.t.,this.TAB_RITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRSEDINP"+;
            " from "+i_cTable+" TAB_RITE where ";
                +"TRCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and TRTIPRIT = "+cp_ToStrODBC(this.w_TIPRIT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRSEDINP;
            from (i_cTable) where;
                TRCODAZI = this.w_CODAZI;
                and TRTIPRIT = this.w_TIPRIT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SEDE = NVL(cp_ToDate(_read_.TRSEDINP),cp_NullValue(_read_.TRSEDINP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(NVL(this.w_SEDE,SPACE(5)))
          this.w_MESS1 = "Codice sede INPS non specificato nella tabella ritenute%0"
          ah_ERRORMSG(this.w_MESS1,48)
          i_retcode = 'stop'
          return
        endif
        * --- Lancio la query per poter recuperare i dati da inserire nello zoom.Versamenti I.N.P.S.
        vq_exec("..\rite\exe\query\gsri1bvi.vqr",this,"VERSA")
      case this.pParame="PR"
        * --- Lancio la query per poter recuperare i dati da inserire nello zoom.Versamenti Previdenziali
        vq_exec("..\rite\exe\query\gsri2bvi.vqr",this,"VERSA")
    endcase
    if RECCOUNT("VERSA")>0
      * --- Eseguo diverse elaborazioni in base al valore del parametro
      do case
        case this.pParame="IR"
          this.w_ELABORA = .F.
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.pParame="IN"
          this.w_ELABORA = .F.
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.pParame="PR"
          this.w_ELABORA = .F.
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if used ("Versa")
        select Versa
        use
      endif
      if used ("Errore")
        select Errore
        use
      endif
      if used ("Zoom")
        select Zoom
        use
      endif
      * --- Lancio la maschera di visualizzazione movimenti
      do GSRI_KVI with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      do case
        case this.pParame="IR"
          ah_ERRORMSG("Non esistono distinte di versamento IRPEF confermate",48)
        case this.pParame="IN"
          ah_ERRORMSG("Non esistono distinte di versamento INPS confermate",48)
        case this.pParame="PR"
          ah_ERRORMSG("Non esistono distinte di versamento previdenziali confermate",48)
      endcase
      if used ("Versa")
        select Versa
        use
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico quali sono le distinte che non devo visualizzare all'interno dello zoom.
    Select Mrrifdi1,Vpnumreg,Vpdatreg from VERSA into cursor Errore where Empty(Nvl(Trcotrif,space(5))) order by Mrrifdi1
    * --- Nello zoom riporto solo le righe di versamento che hanno valorizzato il codice tributi F24
    Select Vpnumreg,Vpcodese,Vpdatreg,Importo,Vpserial,Trcotrif,Mrrifdi1,Mrcaoval,Mrcodval;
    from VERSA into cursor Zoom order by Vpdatreg,Vpserial
    * --- Elimino i record che si trovano nel cursore Versa e sono presenti anche nel cursore Errore.
    =Wrcursor("Zoom")
    Delete from Zoom where Mrrifdi1 in (Select Mrrifdi1 from Errore)
    if Reccount("ZOOM")>0
      this.w_ELABORA = .T.
      * --- Rendo scrivibile il cursore Zoom utilizzato per riempire lo zoom dei versamenti.
      * --- Verifico che la valuta presente nei movimenti ritenute sia identica alla valuta di conto altrimenti
      * --- devo effettuare le conversione adatta.
      Select ZOOM
      Go Top
      Scan for this.oParentObject.w_Mfvaluta<>Zoom.Mrcodval
      this.w_CAMBIO = NVL(Zoom.Mrcaoval,g_CAOVAL)
      this.w_IMPORTO = cp_ROUND(VAL2MON(Zoom.Importo,this.w_Cambio,this.w_Camver,this.w_Datapres,this.oParentObject.w_Mfvaluta), this.w_Dectot)
      Replace Importo with this.w_Importo
      Endscan
      * --- Preparo il cursore da inserire nello zoom.
      Select Max(Vpnumreg) as Vpnumreg,Max(Vpcodese) as Vpcodese,Max(Vpdatreg) as Vpdatreg,;
      Sum(Importo) as Importo,Vpserial,Trcotrif;
      from Zoom into cursor (this.w_Pcursor) order by Vpdatreg,Vpserial group by Trcotrif,Vpserial
    endif
    if Reccount("Errore")>0
      Select Errore
      Go Top
      this.w_RIFDISTI = "XXXXXXXXXX"
      Scan For this.w_RIFDISTI<>Errore.Mrrifdi1
      this.w_NUMERO = NVL(Errore.Vpnumreg,0)
      this.w_DATAREG = CP_TODATE(Vpdatreg)
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Versamento n� %1 del %2%0")
      this.w_oPART.AddParam(str(this.w_NUMERO))     
      this.w_oPART.AddParam(Dtoc(this.w_Datareg))     
      * --- --
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Distinta non importabile per mancanza di codice tributo F24%0")
      this.w_RIFDISTI = Errore.Mrrifdi1
      Endscan
      this.w_MEMO = this.w_oMESS.COMPOSEMESSAGE()
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico quali sono le distinte che non devo visualizzare all'interno dello zoom.
    Select Mrrifdi1,Vpnumreg,Vpdatreg from VERSA into cursor Errore where Empty(Nvl(Trcaucon,space(5))) order by Mrrifdi1
    * --- Nello zoom riporto solo le righe di versamento che hanno valorizzato la causale contributo F24.
    Select Vpnumreg,Vpcodese,Vpdatreg,Importo,Vpserial,Trcaucon,Mrrifdi1,Mrcaoval,Mrcodval;
    from VERSA into cursor Zoom order by Vpdatreg,Vpserial
    * --- Elimino i record che si trovano nel cursore Versa e sono presenti anche nel cursore Errore.
    =Wrcursor("Zoom")
    Delete from Zoom where Mrrifdi1 in (Select Mrrifdi1 from Errore)
    if Reccount("ZOOM")>0
      this.w_ELABORA = .T.
      * --- Rendo scrivibile il cursore Zoom utilizzato per riempire lo zoom dei versamenti.
      * --- Verifico che la valuta presente nei movimenti ritenute sia identica alla valuta di conto altrimenti
      * --- devo effettuare le conversione adatta.
      Select ZOOM
      Go Top
      Scan for this.oParentObject.w_Mfvaluta<>Zoom.Mrcodval
      this.w_CAMBIO = NVL(Zoom.Mrcaoval,g_CAOVAL)
      this.w_IMPORTO = cp_ROUND(VAL2MON(Zoom.Importo,this.w_Cambio,this.w_Camver,this.w_Datapres,this.oParentObject.w_Mfvaluta), this.w_Dectot)
      Replace Importo with this.w_Importo
      Endscan
      * --- Preparo il cursore da inserire nello zoom.
      Select Max(Vpnumreg) as Vpnumreg,Max(Vpcodese) as Vpcodese,Max(Vpdatreg) as Vpdatreg,;
      Sum(Importo) as Importo,Vpserial,Trcaucon;
      from Zoom into cursor (this.w_Pcursor) order by Vpdatreg,Vpserial group by Trcaucon,Vpserial
    endif
    if Reccount("Errore")>0
      Select Errore
      Go Top
      this.w_RIFDISTI = "XXXXXXXXXX"
      Scan For this.w_RIFDISTI<>Errore.Mrrifdi1
      this.w_NUMERO = NVL(Errore.Vpnumreg,0)
      this.w_DATAREG = CP_TODATE(Vpdatreg)
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Versamento n� %1 del %2%0")
      this.w_oPART.AddParam(str(this.w_NUMERO))     
      this.w_oPART.AddParam(Dtoc(this.w_Datareg))     
      * --- --
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Distinta non importabile per mancanza di codice tributo F24%0")
      this.w_RIFDISTI = Errore.Mrrifdi1
      Endscan
      this.w_MEMO = this.w_oMESS.COMPOSEMESSAGE()
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico quali sono le distinte che non devo visualizzare all'interno dello zoom. Non impostato la causale contributo
    * --- Devo verificare all'interno del cursore zoom che i record selezionati abbiano lo stesso codice
    * --- ente che � stato selezionato nel F24
    Select Mrrifdi1,Vpnumreg,Vpdatreg,Trcodent from VERSA into cursor Errore where Empty(Nvl(Trcodent,space(5))) or ;
    Nvl(Trcodent,space(5))<>this.oParentObject.w_Mfcdente order by Mrrifdi1
    * --- Nello zoom riporto solo le righe di versamento che hanno valorizzato il codice ente e la causale contributo F24.
    Select Vpnumreg,Vpcodese,Vpdatreg,Importo,Vpserial,Trcodent,Trcaupre,Mrrifdi1,Mrcaoval,Mrcodval;
    from VERSA into cursor Zoom order by Vpdatreg,Vpserial
    * --- Elimino i record che si trovano nel cursore Versa e sono presenti anche nel cursore Errore.
    =Wrcursor("Zoom")
    Delete from Zoom where Mrrifdi1 in (Select Mrrifdi1 from Errore)
    if Reccount("ZOOM")>0
      this.w_ELABORA = .T.
      * --- Rendo scrivibile il cursore Zoom utilizzato per riempire lo zoom dei versamenti.
      * --- Verifico che la valuta presente nei movimenti ritenute sia identica alla valuta di conto altrimenti
      * --- devo effettuare le conversione adatta.
      Select ZOOM
      Go Top
      Scan for this.oParentObject.w_Mfvaluta<>Zoom.Mrcodval
      this.w_CAMBIO = NVL(Zoom.Mrcaoval,g_CAOVAL)
      this.w_IMPORTO = cp_ROUND(VAL2MON(Zoom.Importo,this.w_Cambio,this.w_Camver,this.w_Datapres,this.oParentObject.w_Mfvaluta), this.w_Dectot)
      Replace Importo with this.w_Importo
      Endscan
      * --- Preparo il cursore da inserire nello zoom.
      Select Max(Vpnumreg) as Vpnumreg,Max(Vpcodese) as Vpcodese,Max(Vpdatreg) as Vpdatreg,;
      Sum(Importo) as Importo,Vpserial,Trcodent,Trcaupre;
      from Zoom into cursor (this.w_Pcursor) order by Vpdatreg,Vpserial group by Trcodent,Trcaupre,Vpserial
    endif
    if Reccount("Errore")>0
      Select Errore
      Go Top
      this.w_RIFDISTI = "XXXXXXXXXX"
      Scan For this.w_RIFDISTI<>Errore.Mrrifdi1
      * --- Controllo se la riga � stata eliminata perch� non � stato specificato il codice ente,oppure perch� il codice ente
      * --- � differente a quello impostato nell'anagrafica.
      this.w_NUMERO = NVL(Errore.Vpnumreg,0)
      this.w_DATAREG = CP_TODATE(Vpdatreg)
      do case
        case Empty(Nvl(Trcodent,space(5)))
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Versamento n� %1 del %2%0")
          this.w_oPART.AddParam(str(this.w_NUMERO))     
          this.w_oPART.AddParam(Dtoc(this.w_Datareg))     
          * --- --
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Distinta non importabile per mancanza di causale contributo previdenziale%0")
        case Nvl(Trcodent,space(5))<>this.oParentObject.w_Mfcdente
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Versamento n� %1 del %2%0")
          this.w_oPART.AddParam(str(this.w_NUMERO))     
          this.w_oPART.AddParam(Dtoc(this.w_Datareg))     
          * --- --
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Codice ente presente nella distinta diverso dal codice ente specificato nel modello F24%0")
      endcase
      this.w_RIFDISTI = Errore.Mrrifdi1
      Endscan
      this.w_MEMO = this.w_oMESS.COMPOSEMESSAGE()
    endif
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='TAB_RITE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
