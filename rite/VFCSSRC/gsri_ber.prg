* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_ber                                                        *
*              Elimina rif.giroconto                                           *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_6]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-04                                                      *
* Last revis.: 2007-01-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_ber",oParentObject,m.pSERIAL)
return(i_retval)

define class tgsri_ber as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  w_SERIAL = space(10)
  w_APPO = space(10)
  * --- WorkFile variables
  MOV_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo Batch viene eseguito dalla procedura GSCG_BCK del Modulo COGE
    * --- E' necessario che avvenga dentro tale Modulo poiche' non verrebbe gestito dalla sua Analisi
    this.w_SERIAL = this.pSERIAL
    if NOT EMPTY(this.w_SERIAL)
      ah_Msg("Elimino riferimento movimento ritenute...",.t.,.f.,.f.)
      * --- Select from MOV_RITE
      i_nConn=i_TableProp[this.MOV_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2],.t.,this.MOV_RITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MRSERIAL  from "+i_cTable+" MOV_RITE ";
            +" where MRGENGIR="+cp_ToStrODBC(this.w_SERIAL)+"";
             ,"_Curs_MOV_RITE")
      else
        select MRSERIAL from (i_cTable);
         where MRGENGIR=this.w_SERIAL;
          into cursor _Curs_MOV_RITE
      endif
      if used('_Curs_MOV_RITE')
        select _Curs_MOV_RITE
        locate for 1=1
        do while not(eof())
        this.w_APPO = NVL(_Curs_MOV_RITE.MRSERIAL,SPACE(10))
        if NOT EMPTY(this.w_APPO)
          * --- Write into MOV_RITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MOV_RITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MRGENGIR ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'MOV_RITE','MRGENGIR');
                +i_ccchkf ;
            +" where ";
                +"MRSERIAL = "+cp_ToStrODBC(this.w_APPO);
                   )
          else
            update (i_cTable) set;
                MRGENGIR = SPACE(10);
                &i_ccchkf. ;
             where;
                MRSERIAL = this.w_APPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_MOV_RITE
          continue
        enddo
        use
      endif
      WAIT CLEAR
    endif
  endproc


  proc Init(oParentObject,pSERIAL)
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_RITE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_MOV_RITE')
      use in _Curs_MOV_RITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL"
endproc
