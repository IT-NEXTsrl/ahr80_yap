* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_smo                                                        *
*              Stampa ritenute subite                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-03                                                      *
* Last revis.: 2013-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_smo",oParentObject))

* --- Class definition
define class tgsri_smo as StdForm
  Top    = 10
  Left   = 9

  * --- Standard Properties
  Width  = 544
  Height = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-26"
  HelpContextID=76941719
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  BUSIUNIT_IDX = 0
  cPrg = "gsri_smo"
  cComment = "Stampa ritenute subite"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  w_azienda = space(5)
  w_DADATDOC = ctod('  /  /  ')
  w_A_DATDOC = ctod('  /  /  ')
  o_A_DATDOC = ctod('  /  /  ')
  w_DACODCON = space(15)
  w_A_CODCON = space(15)
  w_TIPOSTAM = space(10)
  w_forni1 = space(40)
  w_forni2 = space(40)
  w_CODVAL = space(3)
  o_CODVAL = space(3)
  w_valdes = space(35)
  w_valsim = space(5)
  w_extraeur = 0
  w_decimi = 0
  w_VALCAM = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CODBUN = space(3)
  w_CODBUN = space(3)
  w_desbus = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_smoPag1","gsri_smo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDADATDOC_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='BUSIUNIT'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_azienda=space(5)
      .w_DADATDOC=ctod("  /  /  ")
      .w_A_DATDOC=ctod("  /  /  ")
      .w_DACODCON=space(15)
      .w_A_CODCON=space(15)
      .w_TIPOSTAM=space(10)
      .w_forni1=space(40)
      .w_forni2=space(40)
      .w_CODVAL=space(3)
      .w_valdes=space(35)
      .w_valsim=space(5)
      .w_extraeur=0
      .w_decimi=0
      .w_VALCAM=0
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CODBUN=space(3)
      .w_CODBUN=space(3)
      .w_desbus=space(40)
        .w_TIPCON = 'C'
        .w_azienda = i_codazi
        .DoRTCalc(3,5,.f.)
        if not(empty(.w_DACODCON))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_A_CODCON))
          .link_1_6('Full')
        endif
        .w_TIPOSTAM = 'A'
          .DoRTCalc(8,9,.f.)
        .w_CODVAL = g_PERVAL
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODVAL))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,14,.f.)
        .w_VALCAM = GETCAM(.w_CODVAL, .w_A_DATDOC)
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_OBTEST = i_INIDAT
        .DoRTCalc(17,19,.f.)
        if not(empty(.w_CODBUN))
          .link_1_27('Full')
        endif
    endwith
    this.DoRTCalc(20,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPCON = 'C'
        .DoRTCalc(2,9,.t.)
            .w_CODVAL = g_PERVAL
          .link_1_10('Full')
        .DoRTCalc(11,14,.t.)
        if .o_A_DATDOC<>.w_A_DATDOC.or. .o_CODVAL<>.w_CODVAL
            .w_VALCAM = GETCAM(.w_CODVAL, .w_A_DATDOC)
        endif
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODBUN_1_27.visible=!this.oPgFrm.Page1.oPag.oCODBUN_1_27.mHide()
    this.oPgFrm.Page1.oPag.odesbus_1_28.visible=!this.oPgFrm.Page1.oPag.odesbus_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DACODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_DACODCON))
          select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDACODCON_1_5'),i_cWhere,'',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("L'intestatario iniziale � maggiore di quello finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DACODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_DACODCON)
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_forni1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DACODCON = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_forni1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_A_CODCON)) or (.w_DACODCON<=.w_A_CODCON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'intestatario iniziale � maggiore di quello finale")
        endif
        this.w_DACODCON = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_forni1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=A_CODCON
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_A_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_A_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_A_CODCON))
          select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_A_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_A_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oA_CODCON_1_6'),i_cWhere,'',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("L'intestatario iniziale � maggiore di quello finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_A_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_A_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_A_CODCON)
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_A_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_forni2 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_A_CODCON = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_forni2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_A_CODCON>=.w_DACODCON) or empty(.w_DACODCON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'intestatario iniziale � maggiore di quello finale")
        endif
        this.w_A_CODCON = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_forni2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_A_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_valdes = NVL(_Link_.VADESVAL,space(35))
      this.w_valsim = NVL(_Link_.VASIMVAL,space(5))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_valdes = space(35)
      this.w_valsim = space(5)
      this.w_decimi = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_azienda);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_azienda;
                     ,'BUCODICE',trim(this.w_CODBUN))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BUDESCRI like "+cp_ToStrODBC(trim(this.w_CODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_azienda);

            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BUDESCRI like "+cp_ToStr(trim(this.w_CODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStr(this.w_azienda);

            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODBUN_1_27'),i_cWhere,'',"Business Unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_azienda<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_azienda);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_azienda);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_azienda;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_desbus = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(3)
      endif
      this.w_desbus = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDADATDOC_1_3.value==this.w_DADATDOC)
      this.oPgFrm.Page1.oPag.oDADATDOC_1_3.value=this.w_DADATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oA_DATDOC_1_4.value==this.w_A_DATDOC)
      this.oPgFrm.Page1.oPag.oA_DATDOC_1_4.value=this.w_A_DATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODCON_1_5.value==this.w_DACODCON)
      this.oPgFrm.Page1.oPag.oDACODCON_1_5.value=this.w_DACODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oA_CODCON_1_6.value==this.w_A_CODCON)
      this.oPgFrm.Page1.oPag.oA_CODCON_1_6.value=this.w_A_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oforni1_1_8.value==this.w_forni1)
      this.oPgFrm.Page1.oPag.oforni1_1_8.value=this.w_forni1
    endif
    if not(this.oPgFrm.Page1.oPag.oforni2_1_9.value==this.w_forni2)
      this.oPgFrm.Page1.oPag.oforni2_1_9.value=this.w_forni2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBUN_1_27.value==this.w_CODBUN)
      this.oPgFrm.Page1.oPag.oCODBUN_1_27.value=this.w_CODBUN
    endif
    if not(this.oPgFrm.Page1.oPag.odesbus_1_28.value==this.w_desbus)
      this.oPgFrm.Page1.oPag.odesbus_1_28.value=this.w_desbus
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_A_DATDOC) or .w_DADATDOC<=.w_A_DATDOC)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADATDOC_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale anteriore a data iniziale")
          case   not(empty(.w_DADATDOC) or .w_A_DATDOC>=.w_DADATDOC)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oA_DATDOC_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale anteriore a data iniziale")
          case   not(((empty(.w_A_CODCON)) or (.w_DACODCON<=.w_A_CODCON)))  and not(empty(.w_DACODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACODCON_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'intestatario iniziale � maggiore di quello finale")
          case   not(((.w_A_CODCON>=.w_DACODCON) or empty(.w_DACODCON)))  and not(empty(.w_A_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oA_CODCON_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'intestatario iniziale � maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_A_DATDOC = this.w_A_DATDOC
    this.o_CODVAL = this.w_CODVAL
    return

enddefine

* --- Define pages as container
define class tgsri_smoPag1 as StdContainer
  Width  = 540
  height = 217
  stdWidth  = 540
  stdheight = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDADATDOC_1_3 as StdField with uid="EUTANISLCW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DADATDOC", cQueryName = "DADATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale anteriore a data iniziale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 31748231,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=113, Top=11

  func oDADATDOC_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_A_DATDOC) or .w_DADATDOC<=.w_A_DATDOC)
    endwith
    return bRes
  endfunc

  add object oA_DATDOC_1_4 as StdField with uid="CGEGGWBLRL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_A_DATDOC", cQueryName = "A_DATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale anteriore a data iniziale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 31740599,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=303, Top=11

  func oA_DATDOC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DADATDOC) or .w_A_DATDOC>=.w_DADATDOC)
    endwith
    return bRes
  endfunc

  add object oDACODCON_1_5 as StdField with uid="MZTTLBVCNP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DACODCON", cQueryName = "DACODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "L'intestatario iniziale � maggiore di quello finale",;
    ToolTipText = "Intestatario di inizio selezione",;
    HelpContextID = 64389244,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=113, Top=39, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DACODCON"

  func oDACODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODCON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODCON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDACODCON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti",'',this.parent.oContained
  endproc

  add object oA_CODCON_1_6 as StdField with uid="TKLKTMDDSJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_A_CODCON", cQueryName = "A_CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "L'intestatario iniziale � maggiore di quello finale",;
    ToolTipText = "Intestatario di fine selezione",;
    HelpContextID = 64381612,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=113, Top=67, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_A_CODCON"

  func oA_CODCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oA_CODCON_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oA_CODCON_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oA_CODCON_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti",'',this.parent.oContained
  endproc

  add object oforni1_1_8 as StdField with uid="THWQWTCCRD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_forni1", cQueryName = "forni1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 56909994,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=259, Top=39, InputMask=replicate('X',40)

  add object oforni2_1_9 as StdField with uid="IEKCWPSREQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_forni2", cQueryName = "forni2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 40132778,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=259, Top=67, InputMask=replicate('X',40)


  add object oObj_1_16 as cp_outputCombo with uid="EKPLGTKCAL",left=113, top=133, width=390,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 13496090


  add object oBtn_1_17 as StdButton with uid="SNZOXAIEOU",left=432, top=164, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 49704154;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="UXANNFTRBO",left=483, top=164, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 84259142;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODBUN_1_27 as StdField with uid="KUHIBJKRTQ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODBUN", cQueryName = "CODBUN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Business Unit selezionata",;
    HelpContextID = 131293914,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=113, Top=95, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", oKey_1_1="BUCODAZI", oKey_1_2="this.w_azienda", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODBUN"

  func oCODBUN_1_27.mHide()
    with this.Parent.oContained
      return (!ISAHE())
    endwith
  endfunc

  func oCODBUN_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBUN_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBUN_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_azienda)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_azienda)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODBUN_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Business Unit",'',this.parent.oContained
  endproc

  add object odesbus_1_28 as StdField with uid="UQLJZGDEVQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_desbus", cQueryName = "desbus",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 11557578,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=165, Top=95, InputMask=replicate('X',40)

  func odesbus_1_28.mHide()
    with this.Parent.oContained
      return (!ISAHE())
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="MANILEZUNP",Visible=.t., Left=8, Top=133,;
    Alignment=1, Width=103, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ZKJIFAHDUZ",Visible=.t., Left=8, Top=39,;
    Alignment=1, Width=103, Height=18,;
    Caption="Da intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="XIZGZWQWXM",Visible=.t., Left=8, Top=67,;
    Alignment=1, Width=103, Height=18,;
    Caption="A intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="CYBAFXHLAM",Visible=.t., Left=8, Top=11,;
    Alignment=1, Width=103, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="RQAJJRLPVC",Visible=.t., Left=236, Top=11,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="RMBILMPHQH",Visible=.t., Left=14, Top=95,;
    Alignment=1, Width=97, Height=15,;
    Caption="B. U.:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (!ISAHE())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_smo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
