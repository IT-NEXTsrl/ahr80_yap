* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_sin                                                        *
*              Stampa versamenti previdenziali                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_60]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-16                                                      *
* Last revis.: 2009-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_sin",oParentObject))

* --- Class definition
define class tgsri_sin as StdForm
  Top    = 44
  Left   = 95

  * --- Standard Properties
  Width  = 521
  Height = 261
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-15"
  HelpContextID=9832855
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  VEP_RITE_IDX = 0
  TRI_BUTI_IDX = 0
  CONTI_IDX = 0
  COC_MAST_IDX = 0
  VEN_RITE_IDX = 0
  cPrg = "gsri_sin"
  cComment = "Stampa versamenti previdenziali"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_STATO = space(10)
  w_giaver = space(10)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_NREGIS = 0
  w_ESERCI = space(4)
  w_DATADI = ctod('  /  /  ')
  w_DATCAM = ctod('  /  /  ')
  o_DATCAM = ctod('  /  /  ')
  w_DATADI2 = ctod('  /  /  ')
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_CODTRI = space(5)
  w_TIPCON = space(1)
  w_FORNITORE = space(15)
  w_divisa = space(3)
  o_divisa = space(3)
  w_simval = space(5)
  w_extraeur = 0
  w_desval = space(35)
  w_fornde = space(40)
  w_NUMCOR = space(15)
  w_ccdesc = space(35)
  w_decimi = 0
  w_OBTEST = ctod('  /  /  ')
  w_flrite = space(1)
  w_flfoag = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_MRDATREG = ctod('  /  /  ')
  w_NDISTI = space(10)
  w_DESTRI = space(35)
  w_FLACON = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_sinPag1","gsri_sin",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTATO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='VEP_RITE'
    this.cWorkTables[3]='TRI_BUTI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='COC_MAST'
    this.cWorkTables[6]='VEN_RITE'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STATO=space(10)
      .w_giaver=space(10)
      .w_OQRY=space(50)
      .w_OREP=space(50)
      .w_NREGIS=0
      .w_ESERCI=space(4)
      .w_DATADI=ctod("  /  /  ")
      .w_DATCAM=ctod("  /  /  ")
      .w_DATADI2=ctod("  /  /  ")
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_CODTRI=space(5)
      .w_TIPCON=space(1)
      .w_FORNITORE=space(15)
      .w_divisa=space(3)
      .w_simval=space(5)
      .w_extraeur=0
      .w_desval=space(35)
      .w_fornde=space(40)
      .w_NUMCOR=space(15)
      .w_ccdesc=space(35)
      .w_decimi=0
      .w_OBTEST=ctod("  /  /  ")
      .w_flrite=space(1)
      .w_flfoag=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_MRDATREG=ctod("  /  /  ")
      .w_NDISTI=space(10)
      .w_DESTRI=space(35)
      .w_FLACON=space(1)
        .w_STATO = 'P'
        .w_giaver = '5'
        .w_OQRY = "..\RITE\EXE\QUERY\GSRI"+.w_giaver+"INP.VQR"
        .w_OREP = "..\RITE\EXE\QUERY\GSRI3INP.FRX"
          .DoRTCalc(5,8,.f.)
        .w_DATADI2 = CP_TODATE(.w_DATADI)
        .DoRTCalc(10,12,.f.)
        if not(empty(.w_CODTRI))
          .link_1_12('Full')
        endif
        .w_TIPCON = 'F'
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_FORNITORE))
          .link_1_16('Full')
        endif
        .w_divisa = g_PERVAL
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_divisa))
          .link_1_17('Full')
        endif
          .DoRTCalc(16,16,.f.)
        .w_extraeur = GETCAM(.w_DIVISA, .w_DATCAM)
        .DoRTCalc(18,20,.f.)
        if not(empty(.w_NUMCOR))
          .link_1_23('Full')
        endif
          .DoRTCalc(21,22,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(24,26,.f.)
        .w_MRDATREG = i_datsys
      .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
    endwith
    this.DoRTCalc(28,30,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_OQRY = "..\RITE\EXE\QUERY\GSRI"+.w_giaver+"INP.VQR"
            .w_OREP = "..\RITE\EXE\QUERY\GSRI3INP.FRX"
        .DoRTCalc(5,8,.t.)
            .w_DATADI2 = CP_TODATE(.w_DATADI)
        .DoRTCalc(10,12,.t.)
            .w_TIPCON = 'F'
        .DoRTCalc(14,14,.t.)
            .w_divisa = g_PERVAL
          .link_1_17('Full')
        .DoRTCalc(16,16,.t.)
        if .o_DATCAM<>.w_DATCAM.or. .o_divisa<>.w_divisa
            .w_extraeur = GETCAM(.w_DIVISA, .w_DATCAM)
        endif
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATA1_1_10.enabled = this.oPgFrm.Page1.oPag.oDATA1_1_10.mCond()
    this.oPgFrm.Page1.oPag.oDATA2_1_11.enabled = this.oPgFrm.Page1.oPag.oDATA2_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCODTRI_1_12.enabled = this.oPgFrm.Page1.oPag.oCODTRI_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFORNITORE_1_16.enabled = this.oPgFrm.Page1.oPag.oFORNITORE_1_16.mCond()
    this.oPgFrm.Page1.oPag.oNUMCOR_1_23.enabled = this.oPgFrm.Page1.oPag.oNUMCOR_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODTRI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_CODTRI)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_CODTRI))
          select TRCODTRI,TRDESTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODTRI)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODTRI) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oCODTRI_1_12'),i_cWhere,'GSAR_ATB',"Elenco tributi",'GSRI2AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_CODTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_CODTRI)
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODTRI = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESTRI = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLACON = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODTRI = space(5)
      endif
      this.w_DESTRI = space(35)
      this.w_FLACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLACON<>"R"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODTRI = space(5)
        this.w_DESTRI = space(35)
        this.w_FLACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORNITORE
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORNITORE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORNITORE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FORNITORE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORNITORE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORNITORE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORNITORE_1_16'),i_cWhere,'',"Percipienti",'GSRI3AMR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORNITORE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORNITORE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORNITORE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORNITORE = NVL(_Link_.ANCODICE,space(15))
      this.w_fornde = NVL(_Link_.ANDESCRI,space(40))
      this.w_flrite = NVL(_Link_.ANRITENU,space(1))
      this.w_flfoag = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_FORNITORE = space(15)
      endif
      this.w_fornde = space(40)
      this.w_flrite = space(1)
      this.w_flfoag = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBSO>.w_MRDATREG OR EMPTY(.w_DTOBSO)) AND (.w_FLRITE = 'C' AND .w_FLFOAG<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FORNITORE = space(15)
        this.w_fornde = space(40)
        this.w_flrite = space(1)
        this.w_flfoag = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORNITORE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=divisa
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_divisa) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_divisa)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADESVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_divisa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_divisa)
            select VACODVAL,VASIMVAL,VADESVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_divisa = NVL(_Link_.VACODVAL,space(3))
      this.w_simval = NVL(_Link_.VASIMVAL,space(5))
      this.w_desval = NVL(_Link_.VADESVAL,space(35))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_divisa = space(3)
      endif
      this.w_simval = space(5)
      this.w_desval = space(35)
      this.w_decimi = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_divisa Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMCOR
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_NUMCOR)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_NUMCOR))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMCOR)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMCOR) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oNUMCOR_1_23'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_NUMCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_NUMCOR)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMCOR = NVL(_Link_.BACODBAN,space(15))
      this.w_ccdesc = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NUMCOR = space(15)
      endif
      this.w_ccdesc = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_1.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ogiaver_1_2.RadioValue()==this.w_giaver)
      this.oPgFrm.Page1.oPag.ogiaver_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNREGIS_1_5.value==this.w_NREGIS)
      this.oPgFrm.Page1.oPag.oNREGIS_1_5.value=this.w_NREGIS
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCI_1_6.value==this.w_ESERCI)
      this.oPgFrm.Page1.oPag.oESERCI_1_6.value=this.w_ESERCI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATADI2_1_9.value==this.w_DATADI2)
      this.oPgFrm.Page1.oPag.oDATADI2_1_9.value=this.w_DATADI2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_10.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_10.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_11.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_11.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODTRI_1_12.value==this.w_CODTRI)
      this.oPgFrm.Page1.oPag.oCODTRI_1_12.value=this.w_CODTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oFORNITORE_1_16.value==this.w_FORNITORE)
      this.oPgFrm.Page1.oPag.oFORNITORE_1_16.value=this.w_FORNITORE
    endif
    if not(this.oPgFrm.Page1.oPag.ofornde_1_21.value==this.w_fornde)
      this.oPgFrm.Page1.oPag.ofornde_1_21.value=this.w_fornde
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMCOR_1_23.value==this.w_NUMCOR)
      this.oPgFrm.Page1.oPag.oNUMCOR_1_23.value=this.w_NUMCOR
    endif
    if not(this.oPgFrm.Page1.oPag.occdesc_1_24.value==this.w_ccdesc)
      this.oPgFrm.Page1.oPag.occdesc_1_24.value=this.w_ccdesc
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRI_1_40.value==this.w_DESTRI)
      this.oPgFrm.Page1.oPag.oDESTRI_1_40.value=this.w_DESTRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_DATA2) or .w_DATA2>=.w_DATA1)  and (empty(.w_NREGIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data inizio posteriore a data fine")
          case   not(empty(.w_DATA1) or .w_DATA1<=.w_DATA2)  and (empty(.w_NREGIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data inizio posteriore a data fine")
          case   not(.w_FLACON<>"R")  and (empty(.w_NREGIS))  and not(empty(.w_CODTRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODTRI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_DTOBSO>.w_MRDATREG OR EMPTY(.w_DTOBSO)) AND (.w_FLRITE = 'C' AND .w_FLFOAG<>'A'))  and (empty(.w_NREGIS))  and not(empty(.w_FORNITORE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORNITORE_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATCAM = this.w_DATCAM
    this.o_divisa = this.w_divisa
    return

enddefine

* --- Define pages as container
define class tgsri_sinPag1 as StdContainer
  Width  = 517
  height = 261
  stdWidth  = 517
  stdheight = 261
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSTATO_1_1 as StdCombo with uid="VJUGBHKONI",value=3,rtseq=1,rtrep=.f.,left=113,top=12,width=92,height=21;
    , ToolTipText = "Status della registrazione";
    , HelpContextID = 98464550;
    , cFormVar="w_STATO",RowSource=""+"Provvisorio,"+"Confermato,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_1.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'',;
    space(10)))))
  endfunc
  func oSTATO_1_1.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_1.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='P',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='',3,;
      0)))
  endfunc


  add object ogiaver_1_2 as StdCombo with uid="HWXBZHWKGD",rtseq=2,rtrep=.f.,left=241,top=12,width=92,height=21;
    , HelpContextID = 110982810;
    , cFormVar="w_giaver",RowSource=""+"Gi� versati,"+"Da versare,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ogiaver_1_2.RadioValue()
    return(iif(this.value =1,'5',;
    iif(this.value =2,'4',;
    iif(this.value =3,'3',;
    space(10)))))
  endfunc
  func ogiaver_1_2.GetRadio()
    this.Parent.oContained.w_giaver = this.RadioValue()
    return .t.
  endfunc

  func ogiaver_1_2.SetRadio()
    this.Parent.oContained.w_giaver=trim(this.Parent.oContained.w_giaver)
    this.value = ;
      iif(this.Parent.oContained.w_giaver=='5',1,;
      iif(this.Parent.oContained.w_giaver=='4',2,;
      iif(this.Parent.oContained.w_giaver=='3',3,;
      0)))
  endfunc

  add object oNREGIS_1_5 as StdField with uid="EPXBHRSMHQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NREGIS", cQueryName = "NREGIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 126766890,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=113, Top=49, cSayPict="'999999'", cGetPict="'999999'"

  add object oESERCI_1_6 as StdField with uid="DAMPEEGXOJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ESERCI", cQueryName = "ESERCI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 31674042,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=241, Top=49, InputMask=replicate('X',4)

  add object oDATADI2_1_9 as StdField with uid="UAJAHSUKZL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATADI2", cQueryName = "DATADI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 31682762,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=430, Top=49

  add object oDATA1_1_10 as StdField with uid="REXQBCVUEU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inizio posteriore a data fine",;
    ToolTipText = "Data versamento di inizio selezione",;
    HelpContextID = 65834806,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=113, Top=83

  func oDATA1_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NREGIS))
    endwith
   endif
  endfunc

  func oDATA1_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA2) or .w_DATA2>=.w_DATA1)
    endwith
    return bRes
  endfunc

  add object oDATA2_1_11 as StdField with uid="WTEMEFQWXV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inizio posteriore a data fine",;
    ToolTipText = "Data versamento di fine selezione",;
    HelpContextID = 66883382,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=241, Top=83

  func oDATA2_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NREGIS))
    endwith
   endif
  endfunc

  func oDATA2_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA1) or .w_DATA1<=.w_DATA2)
    endwith
    return bRes
  endfunc

  add object oCODTRI_1_12 as StdField with uid="BCPEMHYTWB",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODTRI", cQueryName = "CODTRI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 15819482,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=113, Top=117, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_CODTRI"

  func oCODTRI_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NREGIS))
    endwith
   endif
  endfunc

  func oCODTRI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODTRI_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODTRI_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oCODTRI_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'GSRI2AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oCODTRI_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_CODTRI
     i_obj.ecpSave()
  endproc

  add object oFORNITORE_1_16 as StdField with uid="COPFPJNZYW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FORNITORE", cQueryName = "FORNITORE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Percipiente selezionato",;
    HelpContextID = 109477384,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=113, Top=151, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORNITORE"

  func oFORNITORE_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NREGIS))
    endwith
   endif
  endfunc

  func oFORNITORE_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORNITORE_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORNITORE_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORNITORE_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Percipienti",'GSRI3AMR.CONTI_VZM',this.parent.oContained
  endproc

  add object ofornde_1_21 as StdField with uid="NWDURVJTWG",rtseq=19,rtrep=.f.,;
    cFormVar = "w_fornde", cQueryName = "fornde",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 62152874,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=241, Top=151, InputMask=replicate('X',40)

  add object oNUMCOR_1_23 as StdField with uid="GBHSYWSREK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_NUMCOR", cQueryName = "NUMCOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del conto di tesoreria di pagamento dell'imposta",;
    HelpContextID = 137481258,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=113, Top=185, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", oKey_1_1="BACODBAN", oKey_1_2="this.w_NUMCOR"

  func oNUMCOR_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NREGIS))
    endwith
   endif
  endfunc

  func oNUMCOR_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMCOR_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMCOR_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oNUMCOR_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object occdesc_1_24 as StdField with uid="EWXGFOEDNF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ccdesc", cQueryName = "ccdesc",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 80628954,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=241, Top=185, InputMask=replicate('X',35)


  add object oBtn_1_25 as StdButton with uid="LNPQEWFDUT",left=409, top=212, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 9861606;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="TSIINTMURE",left=461, top=212, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 17150278;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="PIGWNDLAKL",left=290, top=49, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 10033878;
  , bGlobalFont=.t.

    proc oBtn_1_38.Click()
      with this.Parent.oContained
        do GSRI_BI2 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESTRI_1_40 as StdField with uid="KLBOWHZIIB",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESTRI", cQueryName = "DESTRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 15760586,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=241, Top=117, InputMask=replicate('X',35)


  add object oObj_1_43 as cp_runprogram with uid="XICPOWMLVJ",left=107, top=268, width=244,height=20,;
    caption='GSRI_BCV',;
   bGlobalFont=.t.,;
    prg="GSRI_BCV",;
    cEvent = "w_giaver Changed,w_STATO Changed",;
    nPag=1;
    , HelpContextID = 120290884

  add object oStr_1_13 as StdString with uid="WBJINFQTGX",Visible=.t., Left=7, Top=83,;
    Alignment=1, Width=103, Height=15,;
    Caption="Versamenti dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WTLDBGWPGX",Visible=.t., Left=7, Top=151,;
    Alignment=1, Width=103, Height=15,;
    Caption="Percipiente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="XAOEGEZUEH",Visible=.t., Left=197, Top=80,;
    Alignment=1, Width=41, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="KDVZJGGREH",Visible=.t., Left=7, Top=12,;
    Alignment=1, Width=103, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="EUTEZNOBVL",Visible=.t., Left=7, Top=49,;
    Alignment=1, Width=103, Height=15,;
    Caption="Reg. n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="GWGINCJOBL",Visible=.t., Left=361, Top=46,;
    Alignment=1, Width=65, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="NVYAHDKNJK",Visible=.t., Left=181, Top=49,;
    Alignment=1, Width=57, Height=15,;
    Caption="Eserc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="NQMPFXPZAO",Visible=.t., Left=7, Top=117,;
    Alignment=1, Width=103, Height=15,;
    Caption="Codice tributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="RWJDYBJVAA",Visible=.t., Left=7, Top=185,;
    Alignment=1, Width=103, Height=15,;
    Caption="Conto banca:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_sin','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
