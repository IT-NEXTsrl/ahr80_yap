* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bar                                                        *
*              Aggiorna ritenute                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_36]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-04                                                      *
* Last revis.: 2013-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bar",oParentObject)
return(i_retval)

define class tgsri_bar as StdBatch
  * --- Local variables
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_APPO = 0
  w_NUMRIGA = 0
  w_ANTIPCLF = space(1)
  w_PERCIMP = 0
  w_AGRITIRP = 0
  w_AGPERIMP = 0
  w_ANCOINPS = 0
  w_DRPERRIT = 0
  w_DRNONSO1 = 0
  w_DRCODCAU = space(1)
  w_DRFODPRO = 0
  w_DRCODTRI = space(5)
  w_IMPORIGA = 0
  w_IMPOESCL = 0
  w_IMPONONS = 0
  w_INPSNS = 0
  w_IMPOINPS = 0
  w_IMPONIBI = 0
  w_RITENUTA = 0
  w_OSERRIF = space(10)
  w_SERRIF = space(10)
  w_TRFLINPS = space(1)
  w_TRPERRIT = 0
  w_ANCODTR2 = space(5)
  w_CODTRI = space(5)
  w_PNTIPCON = space(1)
  w_ANRIINPS = 0
  w_PNCODCON = space(15)
  w_ANPEINPS = 0
  w_APPO = space(10)
  w_APPO1 = 0
  w_MESS = space(100)
  w_DATAREG = ctod("  /  /  ")
  w_ANRITENU = space(1)
  w_DRTIPFOR = space(1)
  w_AZIENDA = space(5)
  w_TIPO = space(1)
  w_oERRORMESS = .NULL.
  w_DRDATREG = ctod("  /  /  ")
  w_DECTOP = 0
  w_CODCLF = space(15)
  w_TIPRIT = space(1)
  w_PERCEN = 0
  w_DRCODCAU = space(1)
  w_NONSOGGCA = 0
  w_DRNONSOG = 0
  w_AGEIMPO = 0
  w_AGERITE = 0
  w_FLACON = space(1)
  w_DRIMPONI = 0
  w_DRRIINPS = 0
  w_DRRITENU = 0
  w_DRPERPRO = 0
  w_DRIMPOSTA = 0
  * --- WorkFile variables
  DATIRITE_idx=0
  MOV_RITE_idx=0
  TAB_RITE_idx=0
  ESERCIZI_idx=0
  CONTI_idx=0
  AGENTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione ritenute 
    this.w_oERRORMESS=createobject("AH_ErrorLog")
    this.w_AZIENDA = i_CODAZI
    * --- Verifico congruenza tabella ritenute
    * --- Select from ESERCIZI
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ESCODAZI,MIN(ESINIESE) AS INIESE  from "+i_cTable+" ESERCIZI ";
          +" where ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA)+"";
          +" group by ESCODAZI";
           ,"_Curs_ESERCIZI")
    else
      select ESCODAZI,MIN(ESINIESE) AS INIESE from (i_cTable);
       where ESCODAZI=this.w_AZIENDA;
       group by ESCODAZI;
        into cursor _Curs_ESERCIZI
    endif
    if used('_Curs_ESERCIZI')
      select _Curs_ESERCIZI
      locate for 1=1
      do while not(eof())
      this.w_DATAREG = CP_TODATE(INIESE)
        select _Curs_ESERCIZI
        continue
      enddo
      use
    endif
    this.w_DECTOP = g_PERPVL
    this.w_DRDATREG = this.oParentObject.w_DATINI
    * --- Carica Documenti da Generare
    ah_Msg("Ricerca documenti di primanota...",.T.)
    vq_exec("..\RITE\EXE\QUERY\GSRI_BAR.VQR",this,"Dettrite")
    if USED("DETTRITE") AND RECCOUNT("DETTRITE")>0
      * --- Inizio Aggiornamento vero e proprio
      ah_Msg("Inizio conversione...",.T.)
      * --- Testa il Cambio di Documento
      this.w_OSERRIF = "xxxyyykkka"
      SELECT DETTRITE
      GO TOP
      this.w_OSERRIF = PNSERIAL
      do while !EOF("DETTRITE")
        this.w_SERRIF = PNSERIAL
        * --- Legge i conti associati alla tabella ritenute
        this.w_PNCODCON = PNCODCON
        this.w_PNTIPCON = PNTIPCON
        this.w_TIPO = IIF(this.w_PNTIPCON="C" , "V","A")
        this.w_CODCLF = NVL(PNCODCLF,SPACE(15))
        * --- Codice e percentuali Previdenziali
        this.w_ANRITENU = NVL(ANRITENU, "N")
        this.w_ANRIINPS = NVL(ANRIINPS, 0)
        this.w_ANPEINPS = NVL(ANPEINPS, 0)
        this.w_ANCODTR2 = NVL(ANCODTR2, " ")
        this.w_TIPRIT = Nvl(ANFLRITE," ")
        this.w_DRTIPFOR = NVL(ANTIPCLF,"")
        * --- % Carico Percipiente
        this.w_ANCOINPS = NVL(ANCOINPS, 0)
        * --- Calcolo dati ritenuta
        this.w_IMPORIGA = Abs(NVL(PNIMPDAR,0))
        this.w_IMPOESCL = NVL(IVAIND,0)
        this.w_IMPONONS = IIF(this.w_IMPORIGA<0,-1,1) * NVL(PNIMPIND,0)
        this.w_INPSNS = this.w_IMPONONS
        this.w_DRNONSO1 = this.w_IMPONONS + this.w_DRNONSO1
        this.w_IMPONIBI = this.w_IMPORIGA - this.w_IMPOESCL - this.w_IMPONONS
        this.w_IMPOINPS = this.w_IMPONIBI
        this.w_PERCIMP = this.w_ANPEINPS
        this.w_DRPERRIT = this.w_ANCOINPS
        this.w_DRCODTRI = this.w_ANCODTR2
        this.w_DRPERPRO = NVL(ANCASPRO,0)
        this.w_DRIMPOSTA = NVL(DRIMPOSTA,0)
        this.w_APPO = this.w_IMPONIBI
        this.w_IMPONONS = this.w_IMPONONS + (this.w_APPO - this.w_IMPONIBI)
        this.w_DRNONSO1 = this.w_IMPONONS + this.w_DRNONSO1
        this.w_RITENUTA = cp_ROUND(this.w_IMPONIBI * this.w_DRPERRIT/100, 2)
        if this.w_SERRIF <> this.w_OSERRIF
          this.w_NUMRIGA = 10
          this.w_OSERRIF = this.w_SERRIF
        else
          this.w_NUMRIGA = this.w_NUMRIGA + 10
        endif
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT DETTRITE
        SKIP
      enddo
    else
      this.w_oERRORMESS.AddMsgLog("Non esistono dati da elaborare. Esecuzione terminata")     
    endif
    Ah_msg("Elaborazione terminata")
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_oERRORMESS.IsFullLog()
      this.w_oERRORMESS.PrintLog(this,"Errori /warning riscontrati")     
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("RITENUTE")
      SELECT RITENUTE
      USE
    endif
    if USED("TABRITE")
      SELECT TABRITE
      USE
    endif
    if USED("DETTRITE")
      SELECT DETTRITE
      USE
    endif
    if USED("GENERA")
      SELECT GENERA 
 USE
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PERCEN = IIF(this.w_DRPERRIT<=0, this.w_ANRIINPS, this.w_DRPERRIT)
    this.w_DRCODTRI = DETTRITE.ANCODIRP
    this.w_DRCODCAU = DETTRITE.ANCAURIT
    this.w_FLACON = "R"
    if Not Empty(this.w_DRCODTRI)
      if this.w_DRTIPFOR = "A"
        * --- Se ho riempito le somme non soggette, non devo rieffettuare il calcolo per la percentuale imponibile
        * --- Read from AGENTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AGENTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AGPERIMP,AGRITIRP"+;
            " from "+i_cTable+" AGENTI where ";
                +"AGCODFOR = "+cp_ToStrODBC(this.w_CODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AGPERIMP,AGRITIRP;
            from (i_cTable) where;
                AGCODFOR = this.w_CODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_AGEIMPO = NVL(cp_ToDate(_read_.AGPERIMP),cp_NullValue(_read_.AGPERIMP))
          this.w_AGERITE = NVL(cp_ToDate(_read_.AGRITIRP),cp_NullValue(_read_.AGRITIRP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DRFODPRO = cp_ROUND(this.w_DRPERPRO/(100 + this.w_DRPERPRO) * (this.w_IMPORIGA - this.w_DRIMPOSTA - this.w_NONSOGGCA - this.w_IMPOESCL),this.w_DECTOP)
        this.w_IMPONIBI = this.w_IMPONIBI-this.w_DRFODPRO
        this.w_DRIMPONI = cp_ROUND(this.w_AGEIMPO/100*this.w_IMPONIBI,this.w_DECTOP)
        this.w_DRRITENU = cp_ROUND(this.w_DRIMPONI*this.w_AGERITE/100,this.w_DECTOP)
        this.w_NONSOGGCA = this.w_IMPONIBI - this.w_DRIMPONI
        this.w_DRPERRIT = this.w_AGERITE
        this.w_DRRIINPS = this.w_ANRIINPS
      else
        vq_exec("..\RITE\EXE\QUERY\GSRI_BRT", this,"PERCENTUALI")
        SELECT PERCENTUALI
        if RECCOUNT("PERCENTUALI") > 0
          this.w_DRFODPRO = cp_ROUND(this.w_DRPERPRO/(100 + this.w_DRPERPRO) * (this.w_IMPORIGA - this.w_DRIMPOSTA - this.w_NONSOGGCA - this.w_IMPOESCL),this.w_DECTOP)
          this.w_IMPONIBI = this.w_IMPONIBI-this.w_DRFODPRO
          this.w_DRIMPONI = cp_ROUND(IMPONIBILE/100*this.w_IMPONIBI,this.w_DECTOP)
          this.w_DRRITENU = cp_ROUND(this.w_DRIMPONI*RITENUTA/100,this.w_DECTOP)
          this.w_NONSOGGCA = this.w_IMPONIBI - this.w_DRIMPONI
          this.w_DRPERRIT = IIF(this.w_DRPERRIT<=0,RITENUTA,this.w_DRPERRIT)
          this.w_DRRIINPS = this.w_ANRIINPS
        else
          this.w_oERRORMESS.AddMsgLog("Codice tributo %1 non presente, %2 %3, registrazione %4", this.w_DRCODTRI, ICASE(this.w_PNTIPCON="C","Cliente",this.w_PNTIPCON="F","Fornitore","Conto"), this.w_PNCODCON, this.w_SERRIF)     
        endif
      endif
      * --- Try
      local bErr_0371ABC0
      bErr_0371ABC0=bTrsErr
      this.Try_0371ABC0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.w_oERRORMESS.AddMsgLog("Errore nella generazione ritenute primanota n. %1", this.w_SERRIF)     
      endif
      bTrsErr=bTrsErr or bErr_0371ABC0
      * --- End
      if used ("PERCENTUALI")
        select PERCENTUALI
        use
      endif
      this.w_DRNONSOG = this.w_NONSOGGCA
    endif
    if this.w_ANRITENU="C" And Not empty(this.w_ANCODTR2)
      * --- Fornitore soggetto sia ad I.R.PE.F. che I.N.P.S.
      *     Devo inserire prima la riga IRPEF e poi successivamente la riga INPS
      * --- Ricalcolo sempre l'importo da versare
      this.w_DRCODTRI = this.w_ANCODTR2
      this.w_DRCODCAU = "A"
      this.w_FLACON = "Z"
      * --- Calcolo Imponibile INPS
      this.w_DRIMPONI = cp_ROUND((this.w_IMPONIBI) * this.w_PERCIMP/100,this.w_DECTOP)
      this.w_NONSOGGCA = this.w_IMPONIBI - this.w_DRIMPONI
      this.w_DRPERRIT = this.w_ANCOINPS
      this.w_DRRITENU = cp_ROUND(this.w_DRIMPONI*(this.w_DRPERRIT)/100,this.w_DECTOP)
      this.w_DRRIINPS = this.w_ANRIINPS
      this.w_DRNONSOG = 0
      this.w_NUMRIGA = this.w_NUMRIGA + 10
      * --- Try
      local bErr_0370CE10
      bErr_0370CE10=bTrsErr
      this.Try_0370CE10()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.w_oERRORMESS.AddMsgLog("Errore nella generazione ritenute primanota n. %1", this.w_SERRIF)     
      endif
      bTrsErr=bTrsErr or bErr_0370CE10
      * --- End
    endif
  endproc
  proc Try_0371ABC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_DRCODCAU = "A"
    this.w_DRNONSO1 = this.w_NONSOGGCA
    ah_Msg("Aggiorno registrazione di primanota n� %1",.T.,.F.,.F., this.w_SERRIF)
    * --- Try
    local bErr_0371E8E0
    bErr_0371E8E0=bTrsErr
    this.Try_0371E8E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into DATIRITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DATIRITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DATIRITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DRSOMESC ="+cp_NullLink(cp_ToStrODBC(this.w_IMPOESCL),'DATIRITE','DRSOMESC');
        +",DRFODPRO ="+cp_NullLink(cp_ToStrODBC(this.w_DRFODPRO),'DATIRITE','DRFODPRO');
        +",DRCODTRI ="+cp_NullLink(cp_ToStrODBC(this.w_DRCODTRI),'DATIRITE','DRCODTRI');
        +",DRCODCAU ="+cp_NullLink(cp_ToStrODBC(this.w_DRCODCAU),'DATIRITE','DRCODCAU');
        +",DRIMPONI ="+cp_NullLink(cp_ToStrODBC(this.w_DRIMPONI),'DATIRITE','DRIMPONI');
        +",DRRITENU ="+cp_NullLink(cp_ToStrODBC(this.w_DRRITENU),'DATIRITE','DRRITENU');
        +",DRNONSOG ="+cp_NullLink(cp_ToStrODBC(this.w_NONSOGGCA),'DATIRITE','DRNONSOG');
        +",DRPERRIT ="+cp_NullLink(cp_ToStrODBC(this.w_DRPERRIT),'DATIRITE','DRPERRIT');
        +",DRNONSO1 ="+cp_NullLink(cp_ToStrODBC(this.w_DRNONSO1),'DATIRITE','DRNONSO1');
        +",DRRIINPS ="+cp_NullLink(cp_ToStrODBC(this.w_DRRIINPS),'DATIRITE','DRRIINPS');
            +i_ccchkf ;
        +" where ";
            +"DRSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIGA);
               )
      else
        update (i_cTable) set;
            DRSOMESC = this.w_IMPOESCL;
            ,DRFODPRO = this.w_DRFODPRO;
            ,DRCODTRI = this.w_DRCODTRI;
            ,DRCODCAU = this.w_DRCODCAU;
            ,DRIMPONI = this.w_DRIMPONI;
            ,DRRITENU = this.w_DRRITENU;
            ,DRNONSOG = this.w_NONSOGGCA;
            ,DRPERRIT = this.w_DRPERRIT;
            ,DRNONSO1 = this.w_DRNONSO1;
            ,DRRIINPS = this.w_DRRIINPS;
            &i_ccchkf. ;
         where;
            DRSERIAL = this.w_SERRIF;
            and CPROWNUM = this.w_NUMRIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_0371E8E0
    * --- End
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    ah_msg("Aggiornamento eseguito correttamente")
    return
  proc Try_0370CE10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_DRCODCAU = "A"
    this.w_DRFODPRO = cp_ROUND(this.w_DRPERPRO/(100 + this.w_DRPERPRO) * (this.w_IMPORIGA - this.w_DRIMPOSTA - this.w_NONSOGGCA - this.w_IMPOESCL),this.w_DECTOP)
    this.w_DRNONSO1 = this.w_NONSOGGCA
    ah_Msg("Aggiorno registrazione di primanota n� %1",.T.,.F.,.F., this.w_SERRIF)
    * --- Try
    local bErr_0370F750
    bErr_0370F750=bTrsErr
    this.Try_0370F750()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into DATIRITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DATIRITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DATIRITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DRSOMESC ="+cp_NullLink(cp_ToStrODBC(this.w_IMPOESCL),'DATIRITE','DRSOMESC');
        +",DRFODPRO ="+cp_NullLink(cp_ToStrODBC(this.w_DRFODPRO),'DATIRITE','DRFODPRO');
        +",DRCODTRI ="+cp_NullLink(cp_ToStrODBC(this.w_DRCODTRI),'DATIRITE','DRCODTRI');
        +",DRCODCAU ="+cp_NullLink(cp_ToStrODBC(this.w_DRCODCAU),'DATIRITE','DRCODCAU');
        +",DRIMPONI ="+cp_NullLink(cp_ToStrODBC(this.w_DRIMPONI),'DATIRITE','DRIMPONI');
        +",DRRITENU ="+cp_NullLink(cp_ToStrODBC(this.w_DRRITENU),'DATIRITE','DRRITENU');
        +",DRNONSOG ="+cp_NullLink(cp_ToStrODBC(this.w_NONSOGGCA),'DATIRITE','DRNONSOG');
        +",DRPERRIT ="+cp_NullLink(cp_ToStrODBC(this.w_DRPERRIT),'DATIRITE','DRPERRIT');
        +",DRNONSO1 ="+cp_NullLink(cp_ToStrODBC(this.w_DRNONSO1),'DATIRITE','DRNONSO1');
        +",DRRIINPS ="+cp_NullLink(cp_ToStrODBC(this.w_DRRIINPS),'DATIRITE','DRRIINPS');
            +i_ccchkf ;
        +" where ";
            +"DRSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIGA);
               )
      else
        update (i_cTable) set;
            DRSOMESC = this.w_IMPOESCL;
            ,DRFODPRO = this.w_DRFODPRO;
            ,DRCODTRI = this.w_DRCODTRI;
            ,DRCODCAU = this.w_DRCODCAU;
            ,DRIMPONI = this.w_DRIMPONI;
            ,DRRITENU = this.w_DRRITENU;
            ,DRNONSOG = this.w_NONSOGGCA;
            ,DRPERRIT = this.w_DRPERRIT;
            ,DRNONSO1 = this.w_DRNONSO1;
            ,DRRIINPS = this.w_DRRIINPS;
            &i_ccchkf. ;
         where;
            DRSERIAL = this.w_SERRIF;
            and CPROWNUM = this.w_NUMRIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_0370F750
    * --- End
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    ah_msg("Aggiornamento eseguito correttamente")
    return
  proc Try_0371E8E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DATIRITE
    i_nConn=i_TableProp[this.DATIRITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DATIRITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DRSERIAL"+",CPROWNUM"+",DRSOMESC"+",DRFODPRO"+",DRCODTRI"+",DRCODCAU"+",DRIMPONI"+",DRRITENU"+",DRNONSOG"+",DRPERRIT"+",DRNONSO1"+",DRRIINPS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERRIF),'DATIRITE','DRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DATIRITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPOESCL),'DATIRITE','DRSOMESC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRFODPRO),'DATIRITE','DRFODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODTRI),'DATIRITE','DRCODTRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODCAU),'DATIRITE','DRCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRIMPONI),'DATIRITE','DRIMPONI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRRITENU),'DATIRITE','DRRITENU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NONSOGGCA),'DATIRITE','DRNONSOG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRPERRIT),'DATIRITE','DRPERRIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRNONSO1),'DATIRITE','DRNONSO1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRRIINPS),'DATIRITE','DRRIINPS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_SERRIF,'CPROWNUM',this.w_NUMRIGA,'DRSOMESC',this.w_IMPOESCL,'DRFODPRO',this.w_DRFODPRO,'DRCODTRI',this.w_DRCODTRI,'DRCODCAU',this.w_DRCODCAU,'DRIMPONI',this.w_DRIMPONI,'DRRITENU',this.w_DRRITENU,'DRNONSOG',this.w_NONSOGGCA,'DRPERRIT',this.w_DRPERRIT,'DRNONSO1',this.w_DRNONSO1,'DRRIINPS',this.w_DRRIINPS)
      insert into (i_cTable) (DRSERIAL,CPROWNUM,DRSOMESC,DRFODPRO,DRCODTRI,DRCODCAU,DRIMPONI,DRRITENU,DRNONSOG,DRPERRIT,DRNONSO1,DRRIINPS &i_ccchkf. );
         values (;
           this.w_SERRIF;
           ,this.w_NUMRIGA;
           ,this.w_IMPOESCL;
           ,this.w_DRFODPRO;
           ,this.w_DRCODTRI;
           ,this.w_DRCODCAU;
           ,this.w_DRIMPONI;
           ,this.w_DRRITENU;
           ,this.w_NONSOGGCA;
           ,this.w_DRPERRIT;
           ,this.w_DRNONSO1;
           ,this.w_DRRIINPS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nella scrittura'
      return
    endif
    return
  proc Try_0370F750()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DATIRITE
    i_nConn=i_TableProp[this.DATIRITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DATIRITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DRSERIAL"+",CPROWNUM"+",DRSOMESC"+",DRFODPRO"+",DRCODTRI"+",DRCODCAU"+",DRIMPONI"+",DRRITENU"+",DRNONSOG"+",DRPERRIT"+",DRNONSO1"+",DRRIINPS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERRIF),'DATIRITE','DRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DATIRITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPOESCL),'DATIRITE','DRSOMESC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRFODPRO),'DATIRITE','DRFODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODTRI),'DATIRITE','DRCODTRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODCAU),'DATIRITE','DRCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRIMPONI),'DATIRITE','DRIMPONI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRRITENU),'DATIRITE','DRRITENU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NONSOGGCA),'DATIRITE','DRNONSOG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRPERRIT),'DATIRITE','DRPERRIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRNONSO1),'DATIRITE','DRNONSO1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRRIINPS),'DATIRITE','DRRIINPS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_SERRIF,'CPROWNUM',this.w_NUMRIGA,'DRSOMESC',this.w_IMPOESCL,'DRFODPRO',this.w_DRFODPRO,'DRCODTRI',this.w_DRCODTRI,'DRCODCAU',this.w_DRCODCAU,'DRIMPONI',this.w_DRIMPONI,'DRRITENU',this.w_DRRITENU,'DRNONSOG',this.w_NONSOGGCA,'DRPERRIT',this.w_DRPERRIT,'DRNONSO1',this.w_DRNONSO1,'DRRIINPS',this.w_DRRIINPS)
      insert into (i_cTable) (DRSERIAL,CPROWNUM,DRSOMESC,DRFODPRO,DRCODTRI,DRCODCAU,DRIMPONI,DRRITENU,DRNONSOG,DRPERRIT,DRNONSO1,DRRIINPS &i_ccchkf. );
         values (;
           this.w_SERRIF;
           ,this.w_NUMRIGA;
           ,this.w_IMPOESCL;
           ,this.w_DRFODPRO;
           ,this.w_DRCODTRI;
           ,this.w_DRCODCAU;
           ,this.w_DRIMPONI;
           ,this.w_DRRITENU;
           ,this.w_NONSOGGCA;
           ,this.w_DRPERRIT;
           ,this.w_DRNONSO1;
           ,this.w_DRRIINPS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nella scrittura'
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='DATIRITE'
    this.cWorkTables[2]='MOV_RITE'
    this.cWorkTables[3]='TAB_RITE'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='AGENTI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
