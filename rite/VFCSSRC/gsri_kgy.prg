* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kgy                                                        *
*              STAMPA QUADRI                                                   *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-08-31                                                      *
* Last revis.: 2018-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_kgy",oParentObject))

* --- Class definition
define class tgsri_kgy as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 795
  Height = 380
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-09-20"
  HelpContextID=32110185
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri_kgy"
  cComment = "STAMPA QUADRI"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIP770 = space(5)
  w_STAMPAST = space(1)
  w_STAMPASY = space(1)
  w_STAMPAF = space(1)
  w_FIRMA_DICHIARAZIONE = space(250)
  w_FIRMA_SOGGETTO1 = space(250)
  w_FIRMA_SOGGETTO2 = space(250)
  w_FIRMA_SOGGETTO3 = space(250)
  w_FIRMA_SOGGETTO4 = space(250)
  w_FIRMA_SOGGETTO5 = space(250)
  w_FIRMA_INCARICATO = space(250)
  w_FIRMA_CAF = space(250)
  w_STAMPASS = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kgyPag1","gsri_kgy",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTAMPAST_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsri_kgy
    thisform.Autocenter= .T.
    thisform.Closable = .F.
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIP770=space(5)
      .w_STAMPAST=space(1)
      .w_STAMPASY=space(1)
      .w_STAMPAF=space(1)
      .w_FIRMA_DICHIARAZIONE=space(250)
      .w_FIRMA_SOGGETTO1=space(250)
      .w_FIRMA_SOGGETTO2=space(250)
      .w_FIRMA_SOGGETTO3=space(250)
      .w_FIRMA_SOGGETTO4=space(250)
      .w_FIRMA_SOGGETTO5=space(250)
      .w_FIRMA_INCARICATO=space(250)
      .w_FIRMA_CAF=space(250)
      .w_STAMPASS=space(1)
      .w_TIP770=oParentObject.w_TIP770
      .w_STAMPAST=oParentObject.w_STAMPAST
      .w_STAMPASY=oParentObject.w_STAMPASY
      .w_STAMPAF=oParentObject.w_STAMPAF
      .w_FIRMA_DICHIARAZIONE=oParentObject.w_FIRMA_DICHIARAZIONE
      .w_FIRMA_SOGGETTO1=oParentObject.w_FIRMA_SOGGETTO1
      .w_FIRMA_SOGGETTO2=oParentObject.w_FIRMA_SOGGETTO2
      .w_FIRMA_SOGGETTO3=oParentObject.w_FIRMA_SOGGETTO3
      .w_FIRMA_SOGGETTO4=oParentObject.w_FIRMA_SOGGETTO4
      .w_FIRMA_SOGGETTO5=oParentObject.w_FIRMA_SOGGETTO5
      .w_FIRMA_INCARICATO=oParentObject.w_FIRMA_INCARICATO
      .w_FIRMA_CAF=oParentObject.w_FIRMA_CAF
      .w_STAMPASS=oParentObject.w_STAMPASS
          .DoRTCalc(1,1,.f.)
        .w_STAMPAST = .w_STAMPAST
        .w_STAMPASY = .w_STAMPASY
          .DoRTCalc(4,12,.f.)
        .w_STAMPASS = .w_STAMPASS
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIP770=.w_TIP770
      .oParentObject.w_STAMPAST=.w_STAMPAST
      .oParentObject.w_STAMPASY=.w_STAMPASY
      .oParentObject.w_STAMPAF=.w_STAMPAF
      .oParentObject.w_FIRMA_DICHIARAZIONE=.w_FIRMA_DICHIARAZIONE
      .oParentObject.w_FIRMA_SOGGETTO1=.w_FIRMA_SOGGETTO1
      .oParentObject.w_FIRMA_SOGGETTO2=.w_FIRMA_SOGGETTO2
      .oParentObject.w_FIRMA_SOGGETTO3=.w_FIRMA_SOGGETTO3
      .oParentObject.w_FIRMA_SOGGETTO4=.w_FIRMA_SOGGETTO4
      .oParentObject.w_FIRMA_SOGGETTO5=.w_FIRMA_SOGGETTO5
      .oParentObject.w_FIRMA_INCARICATO=.w_FIRMA_INCARICATO
      .oParentObject.w_FIRMA_CAF=.w_FIRMA_CAF
      .oParentObject.w_STAMPASS=.w_STAMPASS
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_YNKSLDBNSX()
    with this
          * --- Assegno il valore 'N' alle variabili w_STAMPASY e w_STAMPAST
          .oParentobject.w_STAMPASY = 'N'
          .oParentobject.w_STAMPAST = 'N'
          .oParentobject.w_STAMPAF = 'N'
          . oParentobject.w_STAMPASS = 'N'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTAMPASS_1_26.visible=!this.oPgFrm.Page1.oPag.oSTAMPASS_1_26.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_YNKSLDBNSX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTAMPAST_1_2.RadioValue()==this.w_STAMPAST)
      this.oPgFrm.Page1.oPag.oSTAMPAST_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPASY_1_3.RadioValue()==this.w_STAMPASY)
      this.oPgFrm.Page1.oPag.oSTAMPASY_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_DICHIARAZIONE_1_8.value==this.w_FIRMA_DICHIARAZIONE)
      this.oPgFrm.Page1.oPag.oFIRMA_DICHIARAZIONE_1_8.value=this.w_FIRMA_DICHIARAZIONE
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO1_1_11.value==this.w_FIRMA_SOGGETTO1)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO1_1_11.value=this.w_FIRMA_SOGGETTO1
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO2_1_17.value==this.w_FIRMA_SOGGETTO2)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO2_1_17.value=this.w_FIRMA_SOGGETTO2
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO3_1_18.value==this.w_FIRMA_SOGGETTO3)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO3_1_18.value=this.w_FIRMA_SOGGETTO3
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO4_1_19.value==this.w_FIRMA_SOGGETTO4)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO4_1_19.value=this.w_FIRMA_SOGGETTO4
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO5_1_20.value==this.w_FIRMA_SOGGETTO5)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO5_1_20.value=this.w_FIRMA_SOGGETTO5
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_INCARICATO_1_21.value==this.w_FIRMA_INCARICATO)
      this.oPgFrm.Page1.oPag.oFIRMA_INCARICATO_1_21.value=this.w_FIRMA_INCARICATO
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_CAF_1_22.value==this.w_FIRMA_CAF)
      this.oPgFrm.Page1.oPag.oFIRMA_CAF_1_22.value=this.w_FIRMA_CAF
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPASS_1_26.RadioValue()==this.w_STAMPASS)
      this.oPgFrm.Page1.oPag.oSTAMPASS_1_26.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri_kgyPag1 as StdContainer
  Width  = 791
  height = 380
  stdWidth  = 791
  stdheight = 380
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTAMPAST_1_2 as StdCheck with uid="BJLXRETCBZ",rtseq=2,rtrep=.f.,left=215, top=36, caption="Stampa quadri ST",;
    ToolTipText = "Se attivo, vengono stampati i quadri ST della dichiarazione ",;
    HelpContextID = 73888634,;
    cFormVar="w_STAMPAST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPAST_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPAST_1_2.GetRadio()
    this.Parent.oContained.w_STAMPAST = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPAST_1_2.SetRadio()
    this.Parent.oContained.w_STAMPAST=trim(this.Parent.oContained.w_STAMPAST)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPAST=='S',1,;
      0)
  endfunc

  add object oSTAMPASY_1_3 as StdCheck with uid="PVMRAJWLNA",rtseq=3,rtrep=.f.,left=392, top=36, caption="Stampa quadri SY",;
    ToolTipText = "Se attivo, vengono stampati i quadri SY della dichiarazione",;
    HelpContextID = 73888639,;
    cFormVar="w_STAMPASY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPASY_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPASY_1_3.GetRadio()
    this.Parent.oContained.w_STAMPASY = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPASY_1_3.SetRadio()
    this.Parent.oContained.w_STAMPASY=trim(this.Parent.oContained.w_STAMPASY)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPASY=='S',1,;
      0)
  endfunc

  add object oFIRMA_DICHIARAZIONE_1_8 as StdField with uid="SORZZBTLBS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FIRMA_DICHIARAZIONE", cQueryName = "FIRMA_DICHIARAZIONE",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 136120568,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=88, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO1_1_11 as StdField with uid="ZEXVOOLKWC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO1", cQueryName = "FIRMA_SOGGETTO1",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 166653163,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=119, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO2_1_17 as StdField with uid="QZZQOVYBZD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO2", cQueryName = "FIRMA_SOGGETTO2",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 101782293,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=150, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO3_1_18 as StdField with uid="HTEMGNIMID",rtseq=8,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO3", cQueryName = "FIRMA_SOGGETTO3",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 101782293,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=181, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO4_1_19 as StdField with uid="HSTWLQIQEN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO4", cQueryName = "FIRMA_SOGGETTO4",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 101782293,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=212, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO5_1_20 as StdField with uid="HQGSGTDSZZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO5", cQueryName = "FIRMA_SOGGETTO5",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 101782293,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=243, InputMask=replicate('X',250)

  add object oFIRMA_INCARICATO_1_21 as StdField with uid="FVTGJEEOOZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FIRMA_INCARICATO", cQueryName = "FIRMA_INCARICATO",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma dell'incaricato",;
    HelpContextID = 116841763,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=274, InputMask=replicate('X',250)

  add object oFIRMA_CAF_1_22 as StdField with uid="MGYPSTJFMC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FIRMA_CAF", cQueryName = "FIRMA_CAF",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del responsabile del C.A.F o del professionista",;
    HelpContextID = 243762185,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=305, InputMask=replicate('X',250)


  add object oBtn_1_23 as StdButton with uid="QFDCFAAMDM",left=678, top=330, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare i quadri selezionati";
    , HelpContextID = 58835734;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_24 as StdButton with uid="BRAIRZJTXI",left=731, top=330, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 58835734;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSTAMPASS_1_26 as StdCheck with uid="IJDRGUKSEP",rtseq=13,rtrep=.f.,left=548, top=36, caption="Stampa quadri SS",;
    ToolTipText = "Se attivo, vengono stampati i quadri SS della dichiarazione",;
    HelpContextID = 73888633,;
    cFormVar="w_STAMPASS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPASS_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPASS_1_26.GetRadio()
    this.Parent.oContained.w_STAMPASS = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPASS_1_26.SetRadio()
    this.Parent.oContained.w_STAMPASS=trim(this.Parent.oContained.w_STAMPASS)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPASS=='S',1,;
      0)
  endfunc

  func oSTAMPASS_1_26.mHide()
    with this.Parent.oContained
      return (.w_TIP770<>'77S18')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="DOUVHPOJSN",Visible=.t., Left=7, Top=5,;
    Alignment=0, Width=271, Height=18,;
    Caption="Selezione quadri della dichiarazione da stampare"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="NPMUGKBSUY",Visible=.t., Left=48, Top=90,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del dichiarante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="KLBNRNWNFY",Visible=.t., Left=69, Top=276,;
    Alignment=1, Width=143, Height=18,;
    Caption="Firma dell'incaricato"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NZVRWIKTIO",Visible=.t., Left=48, Top=121,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="RKLRAALRAR",Visible=.t., Left=48, Top=150,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="YEYNQJATPA",Visible=.t., Left=48, Top=183,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="VOEQMHNMLZ",Visible=.t., Left=48, Top=214,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="CSVRQFWVYJ",Visible=.t., Left=48, Top=245,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="GATLWGMZWS",Visible=.t., Left=8, Top=305,;
    Alignment=1, Width=204, Height=18,;
    Caption="Firma del resp. C.A.F o del profess.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_7 as StdBox with uid="KHJKEHAASK",left=4, top=27, width=390,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kgy','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
