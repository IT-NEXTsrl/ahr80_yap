* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bse                                                        *
*              Quadro 770/SE                                                   *
*                                                                              *
*      Author: TAMSoftware & CODELAB                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_24]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-19                                                      *
* Last revis.: 2007-12-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bse",oParentObject)
return(i_retval)

define class tgsri_bse as StdBatch
  * --- Local variables
  w_MESS = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch serve per convertire i movimenti ritenute in base alla maschera
    * --- di stampa del quadro 770/SE
    if STR(YEAR(this.oParentObject.w_DATFIN),4,0)<>STR(YEAR(this.oParentObject.w_DATINI),4,0)
      this.w_MESS = "L'intervallo di date deve essere interno ad un anno solare"
      ah_ErrorMsg(this.w_MESS,"","")
      i_retcode = 'stop'
      return
    endif
    * --- Lancio la query che va a leggere nei movimenti ritenute
    vq_exec("..\RITE\EXE\QUERY\GSRI4SSC.VQR",this,"RITE")
    * --- Rendo scrivibile il cursore RITE
    WRCURSOR("RITE")
    SCAN FOR MRCODVAL<>this.oParentObject.w_CODVAL
    REPLACE SOMTOTA WITH VAL2MON(NVL(SOMTOTA,0), this.oParentObject.w_CAMBIO,1,I_DATSYS,G_PERVAL, this.oParentObject.w_DECIMI)
    REPLACE IMPONI WITH VAL2MON(NVL(IMPONI,0), this.oParentObject.w_CAMBIO,1,I_DATSYS,G_PERVAL, this.oParentObject.w_DECIMI)
    REPLACE NONSOGG WITH VAL2MON(NVL(NONSOGG,0), this.oParentObject.w_CAMBIO,1,I_DATSYS,G_PERVAL, this.oParentObject.w_DECIMI)
    REPLACE MRTOTIM1 WITH VAL2MON(NVL(MRTOTIM1,0), this.oParentObject.w_CAMBIO,1,I_DATSYS,G_PERVAL, this.oParentObject.w_DECIMI)
    ENDSCAN
    * --- Seleziono il cursore RITE e sommo i vari campi nella valuta selezionata in maschera
    SELECT MRCODCON,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME, ANDESCRI, AN_SESSO, ANDATNAS, ANLOCNAS,;
    ANPRONAS, ANLOCALI, ANPROVIN, ANINDIRI, ANNAZION, NADESNAZ,;
    MRCODTRI, SUM(SOMTOTA) AS SOMTOTA, SUM(IMPONI) AS IMPONI, SUM(NONSOGG) AS NONSOGG,;
    MRCODVAL, SUM(MRTOTIM1) AS MRTOTIM1 FROM RITE;
    GROUP BY MRCODCON, MRCODTRI ORDER BY MRCODCON ,MRCODTRI into cursor __tmp__
    * --- Passo le seguenti variabili al report
    L_PERCFIN=this.oParentObject.w_PERCFIN
    L_CODVAL=this.oParentObject.w_CODVAL
    L_DATFIN=this.oParentObject.w_DATFIN
    L_ANNO=this.oParentObject.w_ANNO
    L_DECIMI=this.oParentObject.w_DECIMI
    L_PERCIN=this.oParentObject.w_PERCIN
    L_DATINI=this.oParentObject.w_DATINI
    * --- Lancio il report di stampa
    CP_CHPRN("..\RITE\EXE\QUERY\GSRI1SSC.FRX", " ", this)
    if used("RITE")
      select ("RITE")
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
