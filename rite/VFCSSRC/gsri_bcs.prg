* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bcs                                                        *
*              Stampa certificazione sintetica                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-01-05                                                      *
* Last revis.: 2018-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bcs",oParentObject,m.pParame)
return(i_retval)

define class tgsri_bcs as StdBatch
  * --- Local variables
  pParame = space(1)
  w_Chserial = space(10)
  w_NumeroRecord = 0
  w_Zoom = .NULL.
  w_Pos_Rec = 0
  w_CUD2016_Temp = space(0)
  w_Chdatsta = ctod("  /  /  ")
  w_Conferma = space(1)
  w_StampaFront = space(1)
  w_StampaDettaglio = space(1)
  w_Firma_Dichiarazione = space(250)
  w_Firma_Intermediario = space(250)
  w_Firma_Imposta = space(250)
  w_NomeCursore = space(10)
  w_Tipostampa = space(1)
  w_Stampa = space(1)
  w_Domfis = space(1)
  w_Cuserial = space(10)
  w_TipoGest = space(3)
  * --- WorkFile variables
  CUDTETRH_idx=0
  GENCOMUN_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa certificazione sintetica
    * --- Data utilizzata per verificare se � stata lanciata la stampa 
    L_Data={}
    do case
      case this.pParame="S" or this.pParame="D" or this.pParame="I"
        this.w_Zoom = This.oParentObject.w_ElePerc
        Select (this.w_Zoom.cCursor)
        this.w_Pos_Rec = Recno()
        Update (this.w_Zoom.cCursor) Set Xchk=Icase(this.pParame=="S",1,this.pParame=="D",0,iif(Xchk=1,0,1))
        Select (this.w_Zoom.cCursor)
        if this.w_Pos_Rec<Reccount()
          GO this.w_Pos_Rec
        endif
      case this.pParame="C" Or this.pParame="O"
        if this.pParame="O"
          this.w_StampaFront = "S"
          this.w_StampaDettaglio = "N"
          do Gsri_Sor with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_StampaDettaglio="N"
            ah_ErrorMsg("Nessuna selezione effettuata")
            use in Select ("ElencoPercipienti")
            i_retcode = 'stop'
            return
          endif
          this.w_Stampa = "O"
          this.w_Cuserial = This.oParentObject.w_Cuserial
          * --- Read from GENCOMUN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.GENCOMUN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GENCOMUN_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CUDOMFIS"+;
              " from "+i_cTable+" GENCOMUN where ";
                  +"CUSERIAL = "+cp_ToStrODBC(this.w_Cuserial);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CUDOMFIS;
              from (i_cTable) where;
                  CUSERIAL = this.w_Cuserial;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_Domfis = NVL(cp_ToDate(_read_.CUDOMFIS),cp_NullValue(_read_.CUDOMFIS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_Zoom = This.oParentObject.w_ElePerc
          Select * from (this.w_Zoom.ccursor) where xchk=1 into cursor ElencoPercipienti
          this.w_Firma_Imposta = This.oParentObject.w_Firma_Imposta
          this.w_Stampa = This.oParentObject.w_TipoStampa
          this.w_Domfis = This.oParentObject.w_Domfis
        endif
        if Reccount("ElencoPercipienti")>0
          Curtotab("ElencoPercipienti","ElePerc")
          use in Select ("ElencoPercipienti")
        else
          ah_ErrorMsg("Nessuna selezione effettuata")
          use in Select ("ElencoPercipienti")
          i_retcode = 'stop'
          return
        endif
        this.w_NomeCursore = iif(this.w_Stampa="S","StampaSintetica","StampaOrdinaria")
        Vq_Exec("..\Rite\Exe\Query\GsrifBcs", this, this.w_NomeCursore)
        this.w_CUD2016_Temp = ADDBS(tempadhoc())+"CUD2016-Temp\"
        * --- Se non esiste crea la cartella per i file temporanei
        if NOT DIRECTORY (this.w_CUD2016_Temp)
          MD (this.w_CUD2016_Temp)
        endif
        * --- --Lancia Batch di creazione immagini 
        do case
          case this.oParentObject.w_Cu__Anno<2016
            this.w_TipoGest = "Cud"
          case this.oParentObject.w_Cu__Anno=2016
            this.w_TipoGest = "Cud_2017"
          case this.oParentObject.w_Cu__Anno>=2017
            this.w_TipoGest = "Cud_2018"
        endcase
        GSRI_BCI (this,this.w_NomeCursore,this.w_CUD2016_Temp,this.w_TipoGest )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Select * from (this.w_NomeCursore) order by Ordina desc,Stampa,Comunicati,Gtdatgen,Gtserial,Gdprocer,Chdatsta,; 
 Chcodcon,Au001001,Chserial,Tiporec,Chpromod into cursor __TMP__
        if this.w_Stampa="S"
          CP_CHPRN("QUERY\CU2016_SINT.FRX","",this.oParentObject)
        else
          CP_CHPRN("QUERY\CU2016_ORDI.FRX","",this.oParentObject)
        endif
        * --- Nel caso l'ultima operazione prima di uscire dalla maschera di stampa e ALT F12- equivale a un cancel
        if type("l_data")="U"
          L_Data={}
        endif
        if Not empty(L_Data) or (Type("g_Pagenum")="N" and g_Pagenum>0)
          this.w_Chdatsta = i_Datsys
          this.w_Conferma = "N"
          do Gsri1Sdh with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_Conferma="S"
            * --- Ciclo il cursore contenente l'elenco dei dati estratti che sono stati selezionati
            Select distinct Chserial from (this.w_NomeCursore) into cursor DataStampa
            Select DataStampa
            Go Top
            Scan
            this.w_Chserial = DataStampa.Chserial
            * --- Write into CUDTETRH
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CUDTETRH_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CHDATSTA ="+cp_NullLink(cp_ToStrODBC(this.w_CHDATSTA),'CUDTETRH','CHDATSTA');
              +",CHCERDEF ="+cp_NullLink(cp_ToStrODBC("S"),'CUDTETRH','CHCERDEF');
                  +i_ccchkf ;
              +" where ";
                  +"CHSERIAL = "+cp_ToStrODBC(this.w_CHSERIAL);
                     )
            else
              update (i_cTable) set;
                  CHDATSTA = this.w_CHDATSTA;
                  ,CHCERDEF = "S";
                  &i_ccchkf. ;
               where;
                  CHSERIAL = this.w_CHSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            Endscan
            This.oParentObject.NotifyEvent("Ricerca")
          endif
        endif
        * --- Distrugge la cartella dei file temporanei
        if DIRECTORY (this.w_CUD2016_Temp)
          DELETE file ( this.w_CUD2016_Temp +"*.*")
          RD (this.w_CUD2016_Temp)
        endif
        * --- Drop temporary table ElePerc
        i_nIdx=cp_GetTableDefIdx('ElePerc')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('ElePerc')
        endif
        use in Select (this.w_NomeCursore)
        use in Select ("DataStampa")
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CUDTETRH'
    this.cWorkTables[2]='GENCOMUN'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
