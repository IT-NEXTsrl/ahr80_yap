* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri4sft                                                        *
*              Gen. file telematico 770/2004                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_783]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-11                                                      *
* Last revis.: 2009-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri4sft",oParentObject))

* --- Class definition
define class tgsri4sft as StdForm
  Top    = 2
  Left   = 7

  * --- Standard Properties
  Width  = 699
  Height = 436+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-02"
  HelpContextID=43316841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=126

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  SEDIAZIE_IDX = 0
  TITOLARI_IDX = 0
  cPrg = "gsri4sft"
  cComment = "Gen. file telematico 770/2004"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_PERAZI = space(1)
  o_PERAZI = space(1)
  w_ROWNUM = 0
  w_PERFIS = space(5)
  w_TIPSL = space(2)
  w_SEDILEG = space(5)
  w_TIPFS = space(2)
  w_SEDIFIS = space(5)
  w_CodFisAzi = space(16)
  w_PERAZI2 = space(1)
  w_FOCOGNOME = space(24)
  o_FOCOGNOME = space(24)
  w_FONOME = space(20)
  o_FONOME = space(20)
  w_FODENOMINA = space(60)
  o_FODENOMINA = space(60)
  w_FOCODFIS = space(16)
  o_FOCODFIS = space(16)
  w_FLAGEURO = space(1)
  w_FLAGEVE = space(2)
  w_FLAGCONF = space(1)
  w_Forza1 = .F.
  o_Forza1 = .F.
  w_FLAGCOR = space(1)
  o_FLAGCOR = space(1)
  w_FLAGINT = space(1)
  o_FLAGINT = space(1)
  w_TIPOPERAZ = space(1)
  w_DATANASC = ctod('  /  /  ')
  o_DATANASC = ctod('  /  /  ')
  w_COMUNE = space(40)
  o_COMUNE = space(40)
  w_SIGLA = space(2)
  o_SIGLA = space(2)
  w_SESSO = space(1)
  o_SESSO = space(1)
  w_VARRES = ctod('  /  /  ')
  w_RECOMUNE = space(40)
  o_RECOMUNE = space(40)
  w_RESIGLA = space(2)
  o_RESIGLA = space(2)
  w_INDIRIZ = space(35)
  o_INDIRIZ = space(35)
  w_CAP = space(5)
  o_CAP = space(5)
  w_CODATT = space(5)
  w_TELEFONO = space(12)
  w_SEVARSED = ctod('  /  /  ')
  w_SECOMUNE = space(40)
  o_SECOMUNE = space(40)
  w_SESIGLA = space(2)
  o_SESIGLA = space(2)
  w_SEINDIRI2 = space(35)
  o_SEINDIRI2 = space(35)
  w_SECAP = space(5)
  o_SECAP = space(5)
  w_SEVARDOM = ctod('  /  /  ')
  w_SERCOMUN = space(40)
  o_SERCOMUN = space(40)
  w_SERSIGLA = space(2)
  o_SERSIGLA = space(2)
  w_SERCAP = space(5)
  o_SERCAP = space(5)
  w_NATGIU = space(2)
  w_SERINDIR = space(35)
  o_SERINDIR = space(35)
  w_SECODATT = space(5)
  w_SETELEFONO = space(12)
  w_STATO = space(1)
  w_SITUAZ = space(1)
  w_CODFISDA = space(11)
  w_ANNO = space(4)
  w_ESERCIZIO = space(4)
  w_VALUTAESE = space(3)
  w_VALUTA = space(1)
  w_CODVAL = space(3)
  w_decimi = 0
  w_TIPCON = space(1)
  w_RITE = space(1)
  w_TIPCLF = space(1)
  w_COGNOME = space(24)
  w_NOME = space(20)
  o_NOME = space(20)
  w_DENOMINA = space(60)
  w_ONLUS = space(1)
  w_SETATT = space(2)
  w_Forza2 = .F.
  o_Forza2 = .F.
  w_RFCODFIS = space(16)
  w_RFCODCAR = space(2)
  w_RFCOGNOME = space(24)
  w_RFNOME = space(20)
  w_RFSESSO = space(1)
  w_RFDATANASC = ctod('  /  /  ')
  w_RFCOMNAS = space(40)
  w_RFSIGNAS = space(2)
  w_RFCOMUNE = space(40)
  w_RFSIGLA = space(2)
  w_RFCAP = space(5)
  w_RFINDIRIZ = space(35)
  w_RFTELEFONO = space(12)
  w_SEZ1 = space(1)
  o_SEZ1 = space(1)
  w_qST1 = space(1)
  w_qSX1 = space(1)
  w_FIRMDICH = space(1)
  w_FIRMPRES = space(1)
  w_SEZ2 = space(1)
  o_SEZ2 = space(1)
  w_qST12 = space(1)
  w_qSX12 = space(1)
  w_FIRMDICH2 = space(1)
  w_FIRMPRES2 = space(1)
  w_RFCFINS2 = space(13)
  w_NUMCAF = 0
  w_RFDATIMP = ctod('  /  /  ')
  w_IMPTRTEL = space(1)
  o_IMPTRTEL = space(1)
  w_IMPTRSOG = space(1)
  o_IMPTRSOG = space(1)
  w_FIRMINT = space(1)
  w_TIPFORN = space(2)
  o_TIPFORN = space(2)
  w_CODFISIN = space(16)
  o_CODFISIN = space(16)
  w_FODATANASC = ctod('  /  /  ')
  w_FOSESSO = space(1)
  w_FOCOMUNE = space(40)
  w_FOSIGLA = space(2)
  w_FORECOMUNE = space(40)
  w_FORESIGLA = space(2)
  w_FOCAP = space(5)
  w_FOINDIRIZ = space(35)
  w_FOSECOMUNE = space(40)
  w_FOSESIGLA = space(2)
  w_FOSECAP = space(5)
  w_FOSEINDIRI2 = space(35)
  w_FOSERCOMUN = space(40)
  w_FOSERSIGLA = space(2)
  w_FOSERCAP = space(5)
  w_FOSERINDIR = space(35)
  w_CFRESCAF = space(16)
  w_VISTOA35 = space(1)
  w_FLAGFIR = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_DirName = space(200)
  w_FileName = space(30)
  w_IDESTA = space(17)
  w_PROIFT = space(6)
  w_CODAZI = space(5)
  w_PERCIN = space(15)
  w_DESCINI = space(40)
  w_PERCFIN = space(15)
  w_DESCFIN = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_NUMCERTIF2 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri4sftPag1","gsri4sft",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pers.fis.\Altri sogg")
      .Pages(2).addobject("oPag","tgsri4sftPag2","gsri4sft",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Rapp.fimatario\Firma")
      .Pages(3).addobject("oPag","tgsri4sftPag3","gsri4sft",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Fornitore file")
      .Pages(4).addobject("oPag","tgsri4sftPag4","gsri4sft",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("File telematico")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFOCOGNOME_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='SEDIAZIE'
    this.cWorkTables[6]='TITOLARI'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsri4sft
    *this.opgfrm.tabstyle = 1
    this.opgfrm.page1.caption = ah_msgformat('Persone fisiche\Altri soggetti')
    this.opgfrm.page2.caption = ah_msgformat('Rappresentante firmatario\Firma')
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_PERAZI=space(1)
      .w_ROWNUM=0
      .w_PERFIS=space(5)
      .w_TIPSL=space(2)
      .w_SEDILEG=space(5)
      .w_TIPFS=space(2)
      .w_SEDIFIS=space(5)
      .w_CodFisAzi=space(16)
      .w_PERAZI2=space(1)
      .w_FOCOGNOME=space(24)
      .w_FONOME=space(20)
      .w_FODENOMINA=space(60)
      .w_FOCODFIS=space(16)
      .w_FLAGEURO=space(1)
      .w_FLAGEVE=space(2)
      .w_FLAGCONF=space(1)
      .w_Forza1=.f.
      .w_FLAGCOR=space(1)
      .w_FLAGINT=space(1)
      .w_TIPOPERAZ=space(1)
      .w_DATANASC=ctod("  /  /  ")
      .w_COMUNE=space(40)
      .w_SIGLA=space(2)
      .w_SESSO=space(1)
      .w_VARRES=ctod("  /  /  ")
      .w_RECOMUNE=space(40)
      .w_RESIGLA=space(2)
      .w_INDIRIZ=space(35)
      .w_CAP=space(5)
      .w_CODATT=space(5)
      .w_TELEFONO=space(12)
      .w_SEVARSED=ctod("  /  /  ")
      .w_SECOMUNE=space(40)
      .w_SESIGLA=space(2)
      .w_SEINDIRI2=space(35)
      .w_SECAP=space(5)
      .w_SEVARDOM=ctod("  /  /  ")
      .w_SERCOMUN=space(40)
      .w_SERSIGLA=space(2)
      .w_SERCAP=space(5)
      .w_NATGIU=space(2)
      .w_SERINDIR=space(35)
      .w_SECODATT=space(5)
      .w_SETELEFONO=space(12)
      .w_STATO=space(1)
      .w_SITUAZ=space(1)
      .w_CODFISDA=space(11)
      .w_ANNO=space(4)
      .w_ESERCIZIO=space(4)
      .w_VALUTAESE=space(3)
      .w_VALUTA=space(1)
      .w_CODVAL=space(3)
      .w_decimi=0
      .w_TIPCON=space(1)
      .w_RITE=space(1)
      .w_TIPCLF=space(1)
      .w_COGNOME=space(24)
      .w_NOME=space(20)
      .w_DENOMINA=space(60)
      .w_ONLUS=space(1)
      .w_SETATT=space(2)
      .w_Forza2=.f.
      .w_RFCODFIS=space(16)
      .w_RFCODCAR=space(2)
      .w_RFCOGNOME=space(24)
      .w_RFNOME=space(20)
      .w_RFSESSO=space(1)
      .w_RFDATANASC=ctod("  /  /  ")
      .w_RFCOMNAS=space(40)
      .w_RFSIGNAS=space(2)
      .w_RFCOMUNE=space(40)
      .w_RFSIGLA=space(2)
      .w_RFCAP=space(5)
      .w_RFINDIRIZ=space(35)
      .w_RFTELEFONO=space(12)
      .w_SEZ1=space(1)
      .w_qST1=space(1)
      .w_qSX1=space(1)
      .w_FIRMDICH=space(1)
      .w_FIRMPRES=space(1)
      .w_SEZ2=space(1)
      .w_qST12=space(1)
      .w_qSX12=space(1)
      .w_FIRMDICH2=space(1)
      .w_FIRMPRES2=space(1)
      .w_RFCFINS2=space(13)
      .w_NUMCAF=0
      .w_RFDATIMP=ctod("  /  /  ")
      .w_IMPTRTEL=space(1)
      .w_IMPTRSOG=space(1)
      .w_FIRMINT=space(1)
      .w_TIPFORN=space(2)
      .w_CODFISIN=space(16)
      .w_FODATANASC=ctod("  /  /  ")
      .w_FOSESSO=space(1)
      .w_FOCOMUNE=space(40)
      .w_FOSIGLA=space(2)
      .w_FORECOMUNE=space(40)
      .w_FORESIGLA=space(2)
      .w_FOCAP=space(5)
      .w_FOINDIRIZ=space(35)
      .w_FOSECOMUNE=space(40)
      .w_FOSESIGLA=space(2)
      .w_FOSECAP=space(5)
      .w_FOSEINDIRI2=space(35)
      .w_FOSERCOMUN=space(40)
      .w_FOSERSIGLA=space(2)
      .w_FOSERCAP=space(5)
      .w_FOSERINDIR=space(35)
      .w_CFRESCAF=space(16)
      .w_VISTOA35=space(1)
      .w_FLAGFIR=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DirName=space(200)
      .w_FileName=space(30)
      .w_IDESTA=space(17)
      .w_PROIFT=space(6)
      .w_CODAZI=space(5)
      .w_PERCIN=space(15)
      .w_DESCINI=space(40)
      .w_PERCFIN=space(15)
      .w_DESCFIN=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_NUMCERTIF2=0
        .w_CODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_ROWNUM = 1
        .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PERFIS))
          .link_1_4('Full')
        endif
        .w_TIPSL = 'SL'
        .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_SEDILEG))
          .link_1_6('Full')
        endif
        .w_TIPFS = 'DF'
        .w_SEDIFIS = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SEDIFIS))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(60),60),space(60))
        .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
        .w_FLAGEURO = '1'
        .w_FLAGEVE = '00'
        .w_FLAGCONF = '0'
        .w_Forza1 = iif(.w_Perazi<>'S',.w_Forza1,.f.)
        .w_FLAGCOR = iif(.w_FLAGINT='1','0',.w_FLAGCOR)
        .w_FLAGINT = iif(.w_FLAGCOR='1','0',.w_FLAGINT)
        .w_TIPOPERAZ = ' '
        .w_DATANASC = iif(.w_Perazi='S' or .w_Forza1,.w_DATANASC,cp_CharToDate('  -  -  '))
        .w_COMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_COMUNE+space(40),40),space(40))
        .w_SIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_SIGLA,'  ')
        .w_SESSO = iif(.w_Perazi='S' or .w_Forza1,.w_SESSO,' ')
        .w_VARRES = iif(.w_Perazi='S' or .w_Forza1,.w_VARRES,cp_CharToDate('  -  -  '))
        .w_RECOMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_RECOMUNE+space(40),40),space(40))
        .w_RESIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_RESIGLA,'  ')
        .w_INDIRIZ = iif(.w_Perazi='S' or .w_Forza1,left(.w_INDIRIZ+space(35),35),space(35))
        .w_CAP = iif(.w_Perazi='S' or .w_Forza1,.w_CAP,space(5))
        .w_CODATT = iif(.w_Perazi='S' or .w_Forza1,.w_CODATT,SPACE(5))
        .w_TELEFONO = iif(.w_Perazi='S' or .w_Forza1,left(.w_TELEFONO,12),space(12))
        .w_SEVARSED = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARSED,cp_CharToDate('  -  -  '))
        .w_SECOMUNE = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SECOMUNE+space(40),40),space(40))
        .w_SESIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SESIGLA,'  ')
        .w_SEINDIRI2 = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SEINDIRI2+space(35),35),space(35))
        .w_SECAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECAP,space(5))
        .w_SEVARDOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARDOM,cp_CharToDate('  -  -  '))
        .w_SERCOMUN = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERCOMUN+space(40),40),space(40))
        .w_SERSIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERSIGLA,'  ')
        .w_SERCAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCAP,space(5))
        .w_NATGIU = iif(.w_Perazi<>'S' or .w_Forza1,.w_NATGIU,'  ')
        .w_SERINDIR = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERINDIR+space(35),35),space(35))
        .w_SECODATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECODATT,space(5))
        .w_SETELEFONO = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SETELEFONO,12),space(12))
        .w_STATO = iif(.w_Perazi<>'S' or .w_Forza1,.w_STATO,' ')
        .w_SITUAZ = iif(.w_Perazi<>'S' or .w_Forza1,.w_SITUAZ,' ')
        .w_CODFISDA = iif(.w_Perazi<>'S' or .w_Forza1,.w_CODFISDA,space(11))
        .w_ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_ESERCIZIO = calceser(.w_DATFIN)
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_ESERCIZIO))
          .link_1_50('Full')
        endif
          .DoRTCalc(51,51,.f.)
        .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
        .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_perval)
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_CODVAL))
          .link_1_53('Full')
        endif
          .DoRTCalc(54,54,.f.)
        .w_TIPCON = 'F'
          .DoRTCalc(56,60,.f.)
        .w_ONLUS = iif(.w_Perazi<>'S' or .w_Forza1,.w_ONLUS,' ')
        .w_SETATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SETATT,'  ')
        .w_Forza2 = iif(.w_Perazi<>'S',.w_Forza2,.f.)
        .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODFIS,space(16))
        .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_Forza2,'1','  ')
        .w_RFCOGNOME = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOGNOME,space(24))
        .w_RFNOME = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFNOME,space(20))
        .w_RFSESSO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSESSO,' ')
        .w_RFDATANASC = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFDATANASC,cp_CharToDate('  -  -  '))
        .w_RFCOMNAS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMNAS,space(40))
        .w_RFSIGNAS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGNAS,'  ')
        .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMUNE,space(40))
        .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGLA,'  ')
        .w_RFCAP = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCAP,space(5))
        .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFINDIRIZ,space(35))
        .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFTELEFONO,space(12))
        .w_SEZ1 = iif(.w_SEZ2='1','0','1')
        .w_qST1 = iif(.w_SEZ1='1','1','0')
        .w_qSX1 = iif(.w_SEZ1='1',.w_qSX1,'0')
        .w_FIRMDICH = iif(.w_SEZ1='1',.w_FIRMDICH,'0')
        .w_FIRMPRES = iif(.w_SEZ1='1',.w_FIRMPRES,'0')
        .w_SEZ2 = iif(.w_SEZ1='1','0',.w_SEZ2)
        .w_qST12 = iif(.w_SEZ2='1','1','0')
        .w_qSX12 = iif(.w_SEZ2='1',.w_qSX12,'0')
        .w_FIRMDICH2 = iif(.w_SEZ2='1',.w_FIRMDICH2,'0')
        .w_FIRMPRES2 = iif(.w_SEZ2='1',.w_FIRMPRES2,'0')
        .w_RFCFINS2 = iif(.w_SEZ2='1',.w_RFCFINS2,space(13))
        .w_NUMCAF = 0
        .w_RFDATIMP = cp_CharToDate('  -  -  ')
        .w_IMPTRTEL = iif(.w_IMPTRSOG='1','0',.w_IMPTRTEL)
        .w_IMPTRSOG = iif(.w_IMPTRTEL='1','0',.w_IMPTRSOG)
        .w_FIRMINT = '0'
        .w_TIPFORN = '01'
        .w_CODFISIN = space(16)
        .w_FODATANASC = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_DATANASC,cp_CharToDate('  -  -  '))
        .w_FOSESSO = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SESSO,' ')
        .w_FOCOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_COMUNE,space(40))
        .w_FOSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SIGLA,'  ')
        .w_FORECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RECOMUNE,space(40))
        .w_FORESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RESIGLA,'  ')
        .w_FOCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_CAP,space(5))
        .w_FOINDIRIZ = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_INDIRIZ,space(35))
        .w_FOSECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECOMUNE,space(40))
        .w_FOSESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SESIGLA,'  ')
        .w_FOSECAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECAP,space(5))
        .w_FOSEINDIRI2 = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SEINDIRI2,space(35))
        .w_FOSERCOMUN = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCOMUN,space(40))
        .w_FOSERSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERSIGLA,'  ')
        .w_FOSERCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCAP,space(5))
        .w_FOSERINDIR = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERINDIR,space(35))
        .w_CFRESCAF = SPACE(16)
        .w_VISTOA35 = '0'
        .w_FLAGFIR = '0'
        .w_DATINI = cp_CharToDate("01-01-2003")
        .w_DATFIN = cp_CharToDate("31-12-2003")
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_FileName = alltrim(.w_FOCODFIS)+'_77S04.77S'
        .w_IDESTA = ' '
        .w_PROIFT = ' '
        .w_CODAZI = i_codazi
        .DoRTCalc(121,121,.f.)
        if not(empty(.w_PERCIN))
          .link_4_12('Full')
        endif
        .DoRTCalc(122,123,.f.)
        if not(empty(.w_PERCFIN))
          .link_4_14('Full')
        endif
          .DoRTCalc(124,124,.f.)
        .w_OBTEST = .w_DATINI
        .w_NUMCERTIF2 = 0
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page4.oPag.oBtn_4_4.enabled = this.oPgFrm.Page4.oPag.oBtn_4_4.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_8.enabled = this.oPgFrm.Page4.oPag.oBtn_4_8.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_9.enabled = this.oPgFrm.Page4.oPag.oBtn_4_9.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_10.enabled = this.oPgFrm.Page4.oPag.oBtn_4_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CODAZI<>.w_CODAZI
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_SEDIFIS = iif(.w_PERAZI<>'S',i_CODAZI,' ')
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
            .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        if .o_PERAZI<>.w_PERAZI
            .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(60),60),space(60))
        endif
        if .o_FOCODFIS<>.w_FOCODFIS
            .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
        endif
        .DoRTCalc(15,17,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_Forza1 = iif(.w_Perazi<>'S',.w_Forza1,.f.)
        endif
        if .o_FLAGINT<>.w_FLAGINT
            .w_FLAGCOR = iif(.w_FLAGINT='1','0',.w_FLAGCOR)
        endif
        if .o_FLAGCOR<>.w_FLAGCOR
            .w_FLAGINT = iif(.w_FLAGCOR='1','0',.w_FLAGINT)
        endif
        if .o_FLAGCOR<>.w_FLAGCOR.or. .o_FLAGINT<>.w_FLAGINT
            .w_TIPOPERAZ = ' '
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_DATANASC = iif(.w_Perazi='S' or .w_Forza1,.w_DATANASC,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_COMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_COMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_SIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_NOME<>.w_NOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SESSO = iif(.w_Perazi='S' or .w_Forza1,.w_SESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_VARRES = iif(.w_Perazi='S' or .w_Forza1,.w_VARRES,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_RECOMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_RECOMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_RESIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_RESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_INDIRIZ = iif(.w_Perazi='S' or .w_Forza1,left(.w_INDIRIZ+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CAP = iif(.w_Perazi='S' or .w_Forza1,.w_CAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CODATT = iif(.w_Perazi='S' or .w_Forza1,.w_CODATT,SPACE(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_TELEFONO = iif(.w_Perazi='S' or .w_Forza1,left(.w_TELEFONO,12),space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SEVARSED = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARSED,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECOMUNE = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SECOMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SESIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SEINDIRI2 = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SEINDIRI2+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SEVARDOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARDOM,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERCOMUN = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERCOMUN+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERSIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERCAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_NATGIU = iif(.w_Perazi<>'S' or .w_Forza1,.w_NATGIU,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERINDIR = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERINDIR+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECODATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECODATT,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SETELEFONO = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SETELEFONO,12),space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_STATO = iif(.w_Perazi<>'S' or .w_Forza1,.w_STATO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SITUAZ = iif(.w_Perazi<>'S' or .w_Forza1,.w_SITUAZ,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CODFISDA = iif(.w_Perazi<>'S' or .w_Forza1,.w_CODFISDA,space(11))
        endif
        .DoRTCalc(49,49,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_ESERCIZIO = calceser(.w_DATFIN)
          .link_1_50('Full')
        endif
        .DoRTCalc(51,51,.t.)
            .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
            .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_perval)
          .link_1_53('Full')
        .DoRTCalc(54,60,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_ONLUS = iif(.w_Perazi<>'S' or .w_Forza1,.w_ONLUS,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SETATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SETATT,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_Forza2 = iif(.w_Perazi<>'S',.w_Forza2,.f.)
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODFIS,space(16))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_Forza2,'1','  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCOGNOME = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOGNOME,space(24))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFNOME = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFNOME,space(20))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFSESSO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFDATANASC = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFDATANASC,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCOMNAS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMNAS,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFSIGNAS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGNAS,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCAP = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFINDIRIZ,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFTELEFONO,space(12))
        endif
        if .o_SEZ2<>.w_SEZ2
            .w_SEZ1 = iif(.w_SEZ2='1','0','1')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2
            .w_qST1 = iif(.w_SEZ1='1','1','0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2
            .w_qSX1 = iif(.w_SEZ1='1',.w_qSX1,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2
            .w_FIRMDICH = iif(.w_SEZ1='1',.w_FIRMDICH,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2
            .w_FIRMPRES = iif(.w_SEZ1='1',.w_FIRMPRES,'0')
        endif
        if .o_SEZ1<>.w_SEZ1
            .w_SEZ2 = iif(.w_SEZ1='1','0',.w_SEZ2)
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1
            .w_qST12 = iif(.w_SEZ2='1','1','0')
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1
            .w_qSX12 = iif(.w_SEZ2='1',.w_qSX12,'0')
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1
            .w_FIRMDICH2 = iif(.w_SEZ2='1',.w_FIRMDICH2,'0')
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1
            .w_FIRMPRES2 = iif(.w_SEZ2='1',.w_FIRMPRES2,'0')
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1
            .w_RFCFINS2 = iif(.w_SEZ2='1',.w_RFCFINS2,space(13))
        endif
        .DoRTCalc(88,89,.t.)
        if .o_IMPTRSOG<>.w_IMPTRSOG
            .w_IMPTRTEL = iif(.w_IMPTRSOG='1','0',.w_IMPTRTEL)
        endif
        if .o_IMPTRTEL<>.w_IMPTRTEL
            .w_IMPTRSOG = iif(.w_IMPTRTEL='1','0',.w_IMPTRSOG)
        endif
        .DoRTCalc(92,93,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_FOCODFIS<>.w_FOCODFIS
            .w_CODFISIN = space(16)
        endif
        if .o_TIPFORN<>.w_TIPFORN.or. .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_DATANASC<>.w_DATANASC
            .w_FODATANASC = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_DATANASC,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SESSO<>.w_SESSO
            .w_FOSESSO = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_COMUNE<>.w_COMUNE
            .w_FOCOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_COMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SIGLA<>.w_SIGLA
            .w_FOSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_RECOMUNE<>.w_RECOMUNE
            .w_FORECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RECOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_RESIGLA<>.w_RESIGLA
            .w_FORESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_CAP<>.w_CAP
            .w_FOCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_CAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_INDIRIZ<>.w_INDIRIZ
            .w_FOINDIRIZ = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_INDIRIZ,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SECOMUNE<>.w_SECOMUNE
            .w_FOSECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SESIGLA<>.w_SESIGLA
            .w_FOSESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SECAP<>.w_SECAP
            .w_FOSECAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SEINDIRI2<>.w_SEINDIRI2
            .w_FOSEINDIRI2 = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SEINDIRI2,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERCOMUN<>.w_SERCOMUN
            .w_FOSERCOMUN = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCOMUN,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERSIGLA<>.w_SERSIGLA
            .w_FOSERSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERCAP<>.w_SERCAP
            .w_FOSERCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERINDIR<>.w_SERINDIR
            .w_FOSERINDIR = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERINDIR,space(35))
        endif
        .DoRTCalc(111,117,.t.)
        if .o_FLAGCOR<>.w_FLAGCOR.or. .o_FLAGINT<>.w_FLAGINT
            .w_IDESTA = ' '
        endif
        if .o_FLAGCOR<>.w_FLAGCOR.or. .o_FLAGINT<>.w_FLAGINT
            .w_PROIFT = ' '
        endif
            .w_CODAZI = i_codazi
          .link_4_12('Full')
        .DoRTCalc(122,122,.t.)
          .link_4_14('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(124,126,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.enabled = this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.mCond()
    this.oPgFrm.Page1.oPag.oFONOME_1_12.enabled = this.oPgFrm.Page1.oPag.oFONOME_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.enabled = this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.mCond()
    this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_21.enabled = this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_21.mCond()
    this.oPgFrm.Page1.oPag.oDATANASC_1_22.enabled = this.oPgFrm.Page1.oPag.oDATANASC_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCOMUNE_1_23.enabled = this.oPgFrm.Page1.oPag.oCOMUNE_1_23.mCond()
    this.oPgFrm.Page1.oPag.oSIGLA_1_24.enabled = this.oPgFrm.Page1.oPag.oSIGLA_1_24.mCond()
    this.oPgFrm.Page1.oPag.oSESSO_1_25.enabled = this.oPgFrm.Page1.oPag.oSESSO_1_25.mCond()
    this.oPgFrm.Page1.oPag.oVARRES_1_26.enabled = this.oPgFrm.Page1.oPag.oVARRES_1_26.mCond()
    this.oPgFrm.Page1.oPag.oRECOMUNE_1_27.enabled = this.oPgFrm.Page1.oPag.oRECOMUNE_1_27.mCond()
    this.oPgFrm.Page1.oPag.oRESIGLA_1_28.enabled = this.oPgFrm.Page1.oPag.oRESIGLA_1_28.mCond()
    this.oPgFrm.Page1.oPag.oINDIRIZ_1_29.enabled = this.oPgFrm.Page1.oPag.oINDIRIZ_1_29.mCond()
    this.oPgFrm.Page1.oPag.oCAP_1_30.enabled = this.oPgFrm.Page1.oPag.oCAP_1_30.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_31.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_31.mCond()
    this.oPgFrm.Page1.oPag.oTELEFONO_1_32.enabled = this.oPgFrm.Page1.oPag.oTELEFONO_1_32.mCond()
    this.oPgFrm.Page1.oPag.oSEVARSED_1_33.enabled = this.oPgFrm.Page1.oPag.oSEVARSED_1_33.mCond()
    this.oPgFrm.Page1.oPag.oSECOMUNE_1_34.enabled = this.oPgFrm.Page1.oPag.oSECOMUNE_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSESIGLA_1_35.enabled = this.oPgFrm.Page1.oPag.oSESIGLA_1_35.mCond()
    this.oPgFrm.Page1.oPag.oSEINDIRI2_1_36.enabled = this.oPgFrm.Page1.oPag.oSEINDIRI2_1_36.mCond()
    this.oPgFrm.Page1.oPag.oSECAP_1_37.enabled = this.oPgFrm.Page1.oPag.oSECAP_1_37.mCond()
    this.oPgFrm.Page1.oPag.oSEVARDOM_1_38.enabled = this.oPgFrm.Page1.oPag.oSEVARDOM_1_38.mCond()
    this.oPgFrm.Page1.oPag.oSERCOMUN_1_39.enabled = this.oPgFrm.Page1.oPag.oSERCOMUN_1_39.mCond()
    this.oPgFrm.Page1.oPag.oSERSIGLA_1_40.enabled = this.oPgFrm.Page1.oPag.oSERSIGLA_1_40.mCond()
    this.oPgFrm.Page1.oPag.oSERCAP_1_41.enabled = this.oPgFrm.Page1.oPag.oSERCAP_1_41.mCond()
    this.oPgFrm.Page1.oPag.oNATGIU_1_42.enabled = this.oPgFrm.Page1.oPag.oNATGIU_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSERINDIR_1_43.enabled = this.oPgFrm.Page1.oPag.oSERINDIR_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSECODATT_1_44.enabled = this.oPgFrm.Page1.oPag.oSECODATT_1_44.mCond()
    this.oPgFrm.Page1.oPag.oSETELEFONO_1_45.enabled = this.oPgFrm.Page1.oPag.oSETELEFONO_1_45.mCond()
    this.oPgFrm.Page1.oPag.oSTATO_1_46.enabled = this.oPgFrm.Page1.oPag.oSTATO_1_46.mCond()
    this.oPgFrm.Page1.oPag.oSITUAZ_1_47.enabled = this.oPgFrm.Page1.oPag.oSITUAZ_1_47.mCond()
    this.oPgFrm.Page1.oPag.oCODFISDA_1_48.enabled = this.oPgFrm.Page1.oPag.oCODFISDA_1_48.mCond()
    this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.enabled = this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.mCond()
    this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.enabled = this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.mCond()
    this.oPgFrm.Page2.oPag.oRFCOGNOME_2_4.enabled = this.oPgFrm.Page2.oPag.oRFCOGNOME_2_4.mCond()
    this.oPgFrm.Page2.oPag.oRFNOME_2_5.enabled = this.oPgFrm.Page2.oPag.oRFNOME_2_5.mCond()
    this.oPgFrm.Page2.oPag.oRFSESSO_2_6.enabled = this.oPgFrm.Page2.oPag.oRFSESSO_2_6.mCond()
    this.oPgFrm.Page2.oPag.oRFDATANASC_2_7.enabled = this.oPgFrm.Page2.oPag.oRFDATANASC_2_7.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMNAS_2_8.enabled = this.oPgFrm.Page2.oPag.oRFCOMNAS_2_8.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGNAS_2_9.enabled = this.oPgFrm.Page2.oPag.oRFSIGNAS_2_9.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMUNE_2_10.enabled = this.oPgFrm.Page2.oPag.oRFCOMUNE_2_10.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.enabled = this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.mCond()
    this.oPgFrm.Page2.oPag.oRFCAP_2_12.enabled = this.oPgFrm.Page2.oPag.oRFCAP_2_12.mCond()
    this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_13.enabled = this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_13.mCond()
    this.oPgFrm.Page2.oPag.oRFTELEFONO_2_14.enabled = this.oPgFrm.Page2.oPag.oRFTELEFONO_2_14.mCond()
    this.oPgFrm.Page2.oPag.oqST1_2_16.enabled = this.oPgFrm.Page2.oPag.oqST1_2_16.mCond()
    this.oPgFrm.Page2.oPag.oqSX1_2_17.enabled = this.oPgFrm.Page2.oPag.oqSX1_2_17.mCond()
    this.oPgFrm.Page2.oPag.oFIRMDICH_2_18.enabled = this.oPgFrm.Page2.oPag.oFIRMDICH_2_18.mCond()
    this.oPgFrm.Page2.oPag.oFIRMPRES_2_19.enabled = this.oPgFrm.Page2.oPag.oFIRMPRES_2_19.mCond()
    this.oPgFrm.Page2.oPag.oqST12_2_21.enabled = this.oPgFrm.Page2.oPag.oqST12_2_21.mCond()
    this.oPgFrm.Page2.oPag.oqSX12_2_22.enabled = this.oPgFrm.Page2.oPag.oqSX12_2_22.mCond()
    this.oPgFrm.Page2.oPag.oFIRMDICH2_2_23.enabled = this.oPgFrm.Page2.oPag.oFIRMDICH2_2_23.mCond()
    this.oPgFrm.Page2.oPag.oFIRMPRES2_2_24.enabled = this.oPgFrm.Page2.oPag.oFIRMPRES2_2_24.mCond()
    this.oPgFrm.Page2.oPag.oRFCFINS2_2_25.enabled = this.oPgFrm.Page2.oPag.oRFCFINS2_2_25.mCond()
    this.oPgFrm.Page3.oPag.oFODATANASC_3_3.enabled = this.oPgFrm.Page3.oPag.oFODATANASC_3_3.mCond()
    this.oPgFrm.Page3.oPag.oFOSESSO_3_4.enabled = this.oPgFrm.Page3.oPag.oFOSESSO_3_4.mCond()
    this.oPgFrm.Page3.oPag.oFOCOMUNE_3_5.enabled = this.oPgFrm.Page3.oPag.oFOCOMUNE_3_5.mCond()
    this.oPgFrm.Page3.oPag.oFOSIGLA_3_6.enabled = this.oPgFrm.Page3.oPag.oFOSIGLA_3_6.mCond()
    this.oPgFrm.Page3.oPag.oFORECOMUNE_3_7.enabled = this.oPgFrm.Page3.oPag.oFORECOMUNE_3_7.mCond()
    this.oPgFrm.Page3.oPag.oFORESIGLA_3_8.enabled = this.oPgFrm.Page3.oPag.oFORESIGLA_3_8.mCond()
    this.oPgFrm.Page3.oPag.oFOCAP_3_9.enabled = this.oPgFrm.Page3.oPag.oFOCAP_3_9.mCond()
    this.oPgFrm.Page3.oPag.oFOINDIRIZ_3_10.enabled = this.oPgFrm.Page3.oPag.oFOINDIRIZ_3_10.mCond()
    this.oPgFrm.Page3.oPag.oFOSECOMUNE_3_12.enabled = this.oPgFrm.Page3.oPag.oFOSECOMUNE_3_12.mCond()
    this.oPgFrm.Page3.oPag.oFOSESIGLA_3_13.enabled = this.oPgFrm.Page3.oPag.oFOSESIGLA_3_13.mCond()
    this.oPgFrm.Page3.oPag.oFOSECAP_3_14.enabled = this.oPgFrm.Page3.oPag.oFOSECAP_3_14.mCond()
    this.oPgFrm.Page3.oPag.oFOSEINDIRI2_3_15.enabled = this.oPgFrm.Page3.oPag.oFOSEINDIRI2_3_15.mCond()
    this.oPgFrm.Page3.oPag.oFOSERCOMUN_3_16.enabled = this.oPgFrm.Page3.oPag.oFOSERCOMUN_3_16.mCond()
    this.oPgFrm.Page3.oPag.oFOSERSIGLA_3_17.enabled = this.oPgFrm.Page3.oPag.oFOSERSIGLA_3_17.mCond()
    this.oPgFrm.Page3.oPag.oFOSERCAP_3_18.enabled = this.oPgFrm.Page3.oPag.oFOSERCAP_3_18.mCond()
    this.oPgFrm.Page3.oPag.oFOSERINDIR_3_19.enabled = this.oPgFrm.Page3.oPag.oFOSERINDIR_3_19.mCond()
    this.oPgFrm.Page4.oPag.oIDESTA_4_6.enabled = this.oPgFrm.Page4.oPag.oIDESTA_4_6.mCond()
    this.oPgFrm.Page4.oPag.oPROIFT_4_7.enabled = this.oPgFrm.Page4.oPag.oPROIFT_4_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZIVACOF";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZIVACOF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_CodFisAzi = NVL(_Link_.AZCOFAZI,space(16))
      this.w_FODENOMINA = NVL(_Link_.AZRAGAZI,space(60))
      this.w_PERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_RFCODCAR = NVL(_Link_.AZIVACAR,space(2))
      this.w_RFCODFIS = NVL(_Link_.AZIVACOF,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CodFisAzi = space(16)
      this.w_FODENOMINA = space(60)
      this.w_PERAZI = space(1)
      this.w_RFCODCAR = space(2)
      this.w_RFCODFIS = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERFIS
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTTELEFO,TTCOGTIT,TTNOMTIT";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_PERFIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_PERFIS)
            select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTTELEFO,TTCOGTIT,TTNOMTIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERFIS = NVL(_Link_.TTCODAZI,space(5))
      this.w_DATANASC = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_SESSO = NVL(_Link_.TT_SESSO,space(1))
      this.w_COMUNE = NVL(_Link_.TTLUONAS,space(40))
      this.w_SIGLA = NVL(_Link_.TTPRONAS,space(2))
      this.w_RECOMUNE = NVL(_Link_.TTLOCTIT,space(40))
      this.w_RESIGLA = NVL(_Link_.TTPROTIT,space(2))
      this.w_INDIRIZ = NVL(_Link_.TTINDIRI,space(35))
      this.w_CAP = NVL(_Link_.TTCAPTIT,space(5))
      this.w_TELEFONO = NVL(_Link_.TTTELEFO,space(12))
      this.w_FOCOGNOME = NVL(_Link_.TTCOGTIT,space(24))
      this.w_FONOME = NVL(_Link_.TTNOMTIT,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PERFIS = space(5)
      endif
      this.w_DATANASC = ctod("  /  /  ")
      this.w_SESSO = space(1)
      this.w_COMUNE = space(40)
      this.w_SIGLA = space(2)
      this.w_RECOMUNE = space(40)
      this.w_RESIGLA = space(2)
      this.w_INDIRIZ = space(35)
      this.w_CAP = space(5)
      this.w_TELEFONO = space(12)
      this.w_FOCOGNOME = space(24)
      this.w_FONOME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDILEG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDILEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDILEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDILEG);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPSL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPSL;
                       ,'SECODAZI',this.w_SEDILEG)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDILEG = NVL(_Link_.SECODAZI,space(5))
      this.w_SECOMUNE = NVL(_Link_.SELOCALI,space(40))
      this.w_SESIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SEINDIRI2 = NVL(_Link_.SEINDIRI,space(35))
      this.w_SECAP = NVL(_Link_.SE___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SEDILEG = space(5)
      endif
      this.w_SECOMUNE = space(40)
      this.w_SESIGLA = space(2)
      this.w_SEINDIRI2 = space(35)
      this.w_SECAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDILEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDIFIS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDIFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDIFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP,SETELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDIFIS);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPFS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPFS;
                       ,'SECODAZI',this.w_SEDIFIS)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP,SETELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDIFIS = NVL(_Link_.SECODAZI,space(5))
      this.w_SERCOMUN = NVL(_Link_.SELOCALI,space(40))
      this.w_SERSIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SERINDIR = NVL(_Link_.SEINDIRI,space(35))
      this.w_SERCAP = NVL(_Link_.SE___CAP,space(5))
      this.w_SETELEFONO = NVL(_Link_.SETELEFO,space(12))
    else
      if i_cCtrl<>'Load'
        this.w_SEDIFIS = space(5)
      endif
      this.w_SERCOMUN = space(40)
      this.w_SERSIGLA = space(2)
      this.w_SERINDIR = space(35)
      this.w_SERCAP = space(5)
      this.w_SETELEFONO = space(12)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDIFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCIZIO
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCIZIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCIZIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCIZIO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCIZIO)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCIZIO = NVL(_Link_.ESCODESE,space(4))
      this.w_VALUTAESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCIZIO = space(4)
      endif
      this.w_VALUTAESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCIZIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_decimi = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCIN
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCIN = space(15)
      endif
      this.w_DESCINI = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCIN = space(15)
        this.w_DESCINI = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCFIN
  func Link_4_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCFIN = space(15)
      endif
      this.w_DESCFIN = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCFIN = space(15)
        this.w_DESCFIN = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value==this.w_FOCOGNOME)
      this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value=this.w_FOCOGNOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFONOME_1_12.value==this.w_FONOME)
      this.oPgFrm.Page1.oPag.oFONOME_1_12.value=this.w_FONOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value==this.w_FODENOMINA)
      this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value=this.w_FODENOMINA
    endif
    if not(this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value==this.w_FOCODFIS)
      this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value=this.w_FOCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGEVE_1_16.RadioValue()==this.w_FLAGEVE)
      this.oPgFrm.Page1.oPag.oFLAGEVE_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCONF_1_17.RadioValue()==this.w_FLAGCONF)
      this.oPgFrm.Page1.oPag.oFLAGCONF_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oForza1_1_18.RadioValue()==this.w_Forza1)
      this.oPgFrm.Page1.oPag.oForza1_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCOR_1_19.RadioValue()==this.w_FLAGCOR)
      this.oPgFrm.Page1.oPag.oFLAGCOR_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGINT_1_20.RadioValue()==this.w_FLAGINT)
      this.oPgFrm.Page1.oPag.oFLAGINT_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_21.RadioValue()==this.w_TIPOPERAZ)
      this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATANASC_1_22.value==this.w_DATANASC)
      this.oPgFrm.Page1.oPag.oDATANASC_1_22.value=this.w_DATANASC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMUNE_1_23.value==this.w_COMUNE)
      this.oPgFrm.Page1.oPag.oCOMUNE_1_23.value=this.w_COMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSIGLA_1_24.value==this.w_SIGLA)
      this.oPgFrm.Page1.oPag.oSIGLA_1_24.value=this.w_SIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSESSO_1_25.RadioValue()==this.w_SESSO)
      this.oPgFrm.Page1.oPag.oSESSO_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVARRES_1_26.value==this.w_VARRES)
      this.oPgFrm.Page1.oPag.oVARRES_1_26.value=this.w_VARRES
    endif
    if not(this.oPgFrm.Page1.oPag.oRECOMUNE_1_27.value==this.w_RECOMUNE)
      this.oPgFrm.Page1.oPag.oRECOMUNE_1_27.value=this.w_RECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oRESIGLA_1_28.value==this.w_RESIGLA)
      this.oPgFrm.Page1.oPag.oRESIGLA_1_28.value=this.w_RESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oINDIRIZ_1_29.value==this.w_INDIRIZ)
      this.oPgFrm.Page1.oPag.oINDIRIZ_1_29.value=this.w_INDIRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCAP_1_30.value==this.w_CAP)
      this.oPgFrm.Page1.oPag.oCAP_1_30.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_31.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_31.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oTELEFONO_1_32.value==this.w_TELEFONO)
      this.oPgFrm.Page1.oPag.oTELEFONO_1_32.value=this.w_TELEFONO
    endif
    if not(this.oPgFrm.Page1.oPag.oSEVARSED_1_33.value==this.w_SEVARSED)
      this.oPgFrm.Page1.oPag.oSEVARSED_1_33.value=this.w_SEVARSED
    endif
    if not(this.oPgFrm.Page1.oPag.oSECOMUNE_1_34.value==this.w_SECOMUNE)
      this.oPgFrm.Page1.oPag.oSECOMUNE_1_34.value=this.w_SECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSESIGLA_1_35.value==this.w_SESIGLA)
      this.oPgFrm.Page1.oPag.oSESIGLA_1_35.value=this.w_SESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSEINDIRI2_1_36.value==this.w_SEINDIRI2)
      this.oPgFrm.Page1.oPag.oSEINDIRI2_1_36.value=this.w_SEINDIRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oSECAP_1_37.value==this.w_SECAP)
      this.oPgFrm.Page1.oPag.oSECAP_1_37.value=this.w_SECAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSEVARDOM_1_38.value==this.w_SEVARDOM)
      this.oPgFrm.Page1.oPag.oSEVARDOM_1_38.value=this.w_SEVARDOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCOMUN_1_39.value==this.w_SERCOMUN)
      this.oPgFrm.Page1.oPag.oSERCOMUN_1_39.value=this.w_SERCOMUN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERSIGLA_1_40.value==this.w_SERSIGLA)
      this.oPgFrm.Page1.oPag.oSERSIGLA_1_40.value=this.w_SERSIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCAP_1_41.value==this.w_SERCAP)
      this.oPgFrm.Page1.oPag.oSERCAP_1_41.value=this.w_SERCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oNATGIU_1_42.value==this.w_NATGIU)
      this.oPgFrm.Page1.oPag.oNATGIU_1_42.value=this.w_NATGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oSERINDIR_1_43.value==this.w_SERINDIR)
      this.oPgFrm.Page1.oPag.oSERINDIR_1_43.value=this.w_SERINDIR
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODATT_1_44.value==this.w_SECODATT)
      this.oPgFrm.Page1.oPag.oSECODATT_1_44.value=this.w_SECODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oSETELEFONO_1_45.value==this.w_SETELEFONO)
      this.oPgFrm.Page1.oPag.oSETELEFONO_1_45.value=this.w_SETELEFONO
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_46.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSITUAZ_1_47.RadioValue()==this.w_SITUAZ)
      this.oPgFrm.Page1.oPag.oSITUAZ_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFISDA_1_48.value==this.w_CODFISDA)
      this.oPgFrm.Page1.oPag.oCODFISDA_1_48.value=this.w_CODFISDA
    endif
    if not(this.oPgFrm.Page2.oPag.oForza2_2_1.RadioValue()==this.w_Forza2)
      this.oPgFrm.Page2.oPag.oForza2_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.value==this.w_RFCODFIS)
      this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.value=this.w_RFCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.RadioValue()==this.w_RFCODCAR)
      this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOGNOME_2_4.value==this.w_RFCOGNOME)
      this.oPgFrm.Page2.oPag.oRFCOGNOME_2_4.value=this.w_RFCOGNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFNOME_2_5.value==this.w_RFNOME)
      this.oPgFrm.Page2.oPag.oRFNOME_2_5.value=this.w_RFNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSESSO_2_6.RadioValue()==this.w_RFSESSO)
      this.oPgFrm.Page2.oPag.oRFSESSO_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATANASC_2_7.value==this.w_RFDATANASC)
      this.oPgFrm.Page2.oPag.oRFDATANASC_2_7.value=this.w_RFDATANASC
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMNAS_2_8.value==this.w_RFCOMNAS)
      this.oPgFrm.Page2.oPag.oRFCOMNAS_2_8.value=this.w_RFCOMNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGNAS_2_9.value==this.w_RFSIGNAS)
      this.oPgFrm.Page2.oPag.oRFSIGNAS_2_9.value=this.w_RFSIGNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMUNE_2_10.value==this.w_RFCOMUNE)
      this.oPgFrm.Page2.oPag.oRFCOMUNE_2_10.value=this.w_RFCOMUNE
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.value==this.w_RFSIGLA)
      this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.value=this.w_RFSIGLA
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCAP_2_12.value==this.w_RFCAP)
      this.oPgFrm.Page2.oPag.oRFCAP_2_12.value=this.w_RFCAP
    endif
    if not(this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_13.value==this.w_RFINDIRIZ)
      this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_13.value=this.w_RFINDIRIZ
    endif
    if not(this.oPgFrm.Page2.oPag.oRFTELEFONO_2_14.value==this.w_RFTELEFONO)
      this.oPgFrm.Page2.oPag.oRFTELEFONO_2_14.value=this.w_RFTELEFONO
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ1_2_15.RadioValue()==this.w_SEZ1)
      this.oPgFrm.Page2.oPag.oSEZ1_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqST1_2_16.RadioValue()==this.w_qST1)
      this.oPgFrm.Page2.oPag.oqST1_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSX1_2_17.RadioValue()==this.w_qSX1)
      this.oPgFrm.Page2.oPag.oqSX1_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMDICH_2_18.RadioValue()==this.w_FIRMDICH)
      this.oPgFrm.Page2.oPag.oFIRMDICH_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMPRES_2_19.RadioValue()==this.w_FIRMPRES)
      this.oPgFrm.Page2.oPag.oFIRMPRES_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ2_2_20.RadioValue()==this.w_SEZ2)
      this.oPgFrm.Page2.oPag.oSEZ2_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqST12_2_21.RadioValue()==this.w_qST12)
      this.oPgFrm.Page2.oPag.oqST12_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSX12_2_22.RadioValue()==this.w_qSX12)
      this.oPgFrm.Page2.oPag.oqSX12_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMDICH2_2_23.RadioValue()==this.w_FIRMDICH2)
      this.oPgFrm.Page2.oPag.oFIRMDICH2_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMPRES2_2_24.RadioValue()==this.w_FIRMPRES2)
      this.oPgFrm.Page2.oPag.oFIRMPRES2_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCFINS2_2_25.value==this.w_RFCFINS2)
      this.oPgFrm.Page2.oPag.oRFCFINS2_2_25.value=this.w_RFCFINS2
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMCAF_2_26.value==this.w_NUMCAF)
      this.oPgFrm.Page2.oPag.oNUMCAF_2_26.value=this.w_NUMCAF
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATIMP_2_27.value==this.w_RFDATIMP)
      this.oPgFrm.Page2.oPag.oRFDATIMP_2_27.value=this.w_RFDATIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPTRTEL_2_28.RadioValue()==this.w_IMPTRTEL)
      this.oPgFrm.Page2.oPag.oIMPTRTEL_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPTRSOG_2_29.RadioValue()==this.w_IMPTRSOG)
      this.oPgFrm.Page2.oPag.oIMPTRSOG_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMINT_2_30.RadioValue()==this.w_FIRMINT)
      this.oPgFrm.Page2.oPag.oFIRMINT_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPFORN_3_1.RadioValue()==this.w_TIPFORN)
      this.oPgFrm.Page3.oPag.oTIPFORN_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCODFISIN_3_2.value==this.w_CODFISIN)
      this.oPgFrm.Page3.oPag.oCODFISIN_3_2.value=this.w_CODFISIN
    endif
    if not(this.oPgFrm.Page3.oPag.oFODATANASC_3_3.value==this.w_FODATANASC)
      this.oPgFrm.Page3.oPag.oFODATANASC_3_3.value=this.w_FODATANASC
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSESSO_3_4.RadioValue()==this.w_FOSESSO)
      this.oPgFrm.Page3.oPag.oFOSESSO_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFOCOMUNE_3_5.value==this.w_FOCOMUNE)
      this.oPgFrm.Page3.oPag.oFOCOMUNE_3_5.value=this.w_FOCOMUNE
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSIGLA_3_6.value==this.w_FOSIGLA)
      this.oPgFrm.Page3.oPag.oFOSIGLA_3_6.value=this.w_FOSIGLA
    endif
    if not(this.oPgFrm.Page3.oPag.oFORECOMUNE_3_7.value==this.w_FORECOMUNE)
      this.oPgFrm.Page3.oPag.oFORECOMUNE_3_7.value=this.w_FORECOMUNE
    endif
    if not(this.oPgFrm.Page3.oPag.oFORESIGLA_3_8.value==this.w_FORESIGLA)
      this.oPgFrm.Page3.oPag.oFORESIGLA_3_8.value=this.w_FORESIGLA
    endif
    if not(this.oPgFrm.Page3.oPag.oFOCAP_3_9.value==this.w_FOCAP)
      this.oPgFrm.Page3.oPag.oFOCAP_3_9.value=this.w_FOCAP
    endif
    if not(this.oPgFrm.Page3.oPag.oFOINDIRIZ_3_10.value==this.w_FOINDIRIZ)
      this.oPgFrm.Page3.oPag.oFOINDIRIZ_3_10.value=this.w_FOINDIRIZ
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSECOMUNE_3_12.value==this.w_FOSECOMUNE)
      this.oPgFrm.Page3.oPag.oFOSECOMUNE_3_12.value=this.w_FOSECOMUNE
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSESIGLA_3_13.value==this.w_FOSESIGLA)
      this.oPgFrm.Page3.oPag.oFOSESIGLA_3_13.value=this.w_FOSESIGLA
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSECAP_3_14.value==this.w_FOSECAP)
      this.oPgFrm.Page3.oPag.oFOSECAP_3_14.value=this.w_FOSECAP
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSEINDIRI2_3_15.value==this.w_FOSEINDIRI2)
      this.oPgFrm.Page3.oPag.oFOSEINDIRI2_3_15.value=this.w_FOSEINDIRI2
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSERCOMUN_3_16.value==this.w_FOSERCOMUN)
      this.oPgFrm.Page3.oPag.oFOSERCOMUN_3_16.value=this.w_FOSERCOMUN
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSERSIGLA_3_17.value==this.w_FOSERSIGLA)
      this.oPgFrm.Page3.oPag.oFOSERSIGLA_3_17.value=this.w_FOSERSIGLA
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSERCAP_3_18.value==this.w_FOSERCAP)
      this.oPgFrm.Page3.oPag.oFOSERCAP_3_18.value=this.w_FOSERCAP
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSERINDIR_3_19.value==this.w_FOSERINDIR)
      this.oPgFrm.Page3.oPag.oFOSERINDIR_3_19.value=this.w_FOSERINDIR
    endif
    if not(this.oPgFrm.Page3.oPag.oCFRESCAF_3_20.value==this.w_CFRESCAF)
      this.oPgFrm.Page3.oPag.oCFRESCAF_3_20.value=this.w_CFRESCAF
    endif
    if not(this.oPgFrm.Page3.oPag.oVISTOA35_3_21.RadioValue()==this.w_VISTOA35)
      this.oPgFrm.Page3.oPag.oVISTOA35_3_21.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFLAGFIR_3_22.RadioValue()==this.w_FLAGFIR)
      this.oPgFrm.Page3.oPag.oFLAGFIR_3_22.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDATINI_4_1.value==this.w_DATINI)
      this.oPgFrm.Page4.oPag.oDATINI_4_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page4.oPag.oDATFIN_4_2.value==this.w_DATFIN)
      this.oPgFrm.Page4.oPag.oDATFIN_4_2.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page4.oPag.oDirName_4_3.value==this.w_DirName)
      this.oPgFrm.Page4.oPag.oDirName_4_3.value=this.w_DirName
    endif
    if not(this.oPgFrm.Page4.oPag.oFileName_4_5.value==this.w_FileName)
      this.oPgFrm.Page4.oPag.oFileName_4_5.value=this.w_FileName
    endif
    if not(this.oPgFrm.Page4.oPag.oIDESTA_4_6.value==this.w_IDESTA)
      this.oPgFrm.Page4.oPag.oIDESTA_4_6.value=this.w_IDESTA
    endif
    if not(this.oPgFrm.Page4.oPag.oPROIFT_4_7.value==this.w_PROIFT)
      this.oPgFrm.Page4.oPag.oPROIFT_4_7.value=this.w_PROIFT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FOCOGNOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCOGNOME_1_11.SetFocus()
            i_bnoObbl = !empty(.w_FOCOGNOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FONOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFONOME_1_12.SetFocus()
            i_bnoObbl = !empty(.w_FONOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FODENOMINA)) or not(not empty(.w_FODENOMINA)))  and (empty(.w_FOCOGNOME) and empty(.w_FONOME))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFODENOMINA_1_13.SetFocus()
            i_bnoObbl = !empty(.w_FODENOMINA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FOCODFIS)) or not(iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODFIS_1_14.SetFocus()
            i_bnoObbl = !empty(.w_FOCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lunghezza codice fiscale o partita IVA non corretta")
          case   not((.w_FLAGCOR='1' or .w_FLAGINT='1') and not empty(.w_TIPOPERAZ))  and ((.w_FLAGCOR = '1') OR (.w_FLAGINT = '1'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOPERAZ_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il tipo dell'operazione")
          case   (empty(.w_DATANASC))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATANASC_1_22.SetFocus()
            i_bnoObbl = !empty(.w_DATANASC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COMUNE))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMUNE_1_23.SetFocus()
            i_bnoObbl = !empty(.w_COMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SESSO))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSESSO_1_25.SetFocus()
            i_bnoObbl = !empty(.w_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RECOMUNE))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRECOMUNE_1_27.SetFocus()
            i_bnoObbl = !empty(.w_RECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(! empty(.w_RESIGLA))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRESIGLA_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire la sigla della provincia di residenza anagrafica o di domicilio fiscale")
          case   not(! empty(.w_CAP))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAP_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore")
          case   not(at(' ',alltrim(.w_TELEFONO))=0 and at('/',alltrim(.w_TELEFONO))=0 and at('-',alltrim(.w_TELEFONO))=0)  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTELEFONO_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   (empty(.w_SECOMUNE))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECOMUNE_1_34.SetFocus()
            i_bnoObbl = !empty(.w_SECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(! empty(.w_SECAP))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECAP_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.A.P. della sede legale del fornitore")
          case   not((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=43) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=51)  OR val(.w_NATGIU)=53)  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNATGIU_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso per natura giuridica")
          case   not(at(' ',alltrim(.w_SETELEFONO))=0 and at('/',alltrim(.w_SETELEFONO))=0 and at('-',alltrim(.w_SETELEFONO))=0)  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSETELEFONO_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(iif(not empty(.w_CODFISDA),chkcfp(alltrim(.w_CODFISDA),'PI'),.T.))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFISDA_1_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))  and (.w_PERAZI <> 'S' or .w_Forza2)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCODFIS_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)  and (.w_PERAZI <> 'S' or .w_Forza2)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFTELEFONO_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(iif(not empty(.w_RFCFINS2),chkcfp(alltrim(.w_RFCFINS2),'CF'),iif(.w_SEZ2='1',.F.,.T.) ))  and (.w_SEZ2='1')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCFINS2_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.F. dell'intermediario della sezione II")
          case   not((.w_Perazi='S' and  .w_TIPFORN<>'06' ) or .w_Perazi<>'S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTIPFORN_3_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione non consentita con tipo fornitore persona fisica")
          case   not(iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODFISIN_3_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_CFRESCAF),chkcfp(alltrim(.w_CFRESCAF),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCFRESCAF_3_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATINI)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATINI > cp_CharToDate("31-12-02"))))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDATINI_4_1.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   ((empty(.w_DATFIN)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN < cp_CharToDate("01-01-04"))))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDATFIN_4_2.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   (empty(.w_IDESTA))  and (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oIDESTA_4_6.SetFocus()
            i_bnoObbl = !empty(.w_IDESTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PROIFT))  and (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPROIFT_4_7.SetFocus()
            i_bnoObbl = !empty(.w_PROIFT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPERCIN_4_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
          case   not(((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCFIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPERCFIN_4_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_PERAZI = this.w_PERAZI
    this.o_FOCOGNOME = this.w_FOCOGNOME
    this.o_FONOME = this.w_FONOME
    this.o_FODENOMINA = this.w_FODENOMINA
    this.o_FOCODFIS = this.w_FOCODFIS
    this.o_Forza1 = this.w_Forza1
    this.o_FLAGCOR = this.w_FLAGCOR
    this.o_FLAGINT = this.w_FLAGINT
    this.o_DATANASC = this.w_DATANASC
    this.o_COMUNE = this.w_COMUNE
    this.o_SIGLA = this.w_SIGLA
    this.o_SESSO = this.w_SESSO
    this.o_RECOMUNE = this.w_RECOMUNE
    this.o_RESIGLA = this.w_RESIGLA
    this.o_INDIRIZ = this.w_INDIRIZ
    this.o_CAP = this.w_CAP
    this.o_SECOMUNE = this.w_SECOMUNE
    this.o_SESIGLA = this.w_SESIGLA
    this.o_SEINDIRI2 = this.w_SEINDIRI2
    this.o_SECAP = this.w_SECAP
    this.o_SERCOMUN = this.w_SERCOMUN
    this.o_SERSIGLA = this.w_SERSIGLA
    this.o_SERCAP = this.w_SERCAP
    this.o_SERINDIR = this.w_SERINDIR
    this.o_NOME = this.w_NOME
    this.o_Forza2 = this.w_Forza2
    this.o_SEZ1 = this.w_SEZ1
    this.o_SEZ2 = this.w_SEZ2
    this.o_IMPTRTEL = this.w_IMPTRTEL
    this.o_IMPTRSOG = this.w_IMPTRSOG
    this.o_TIPFORN = this.w_TIPFORN
    this.o_CODFISIN = this.w_CODFISIN
    this.o_DATFIN = this.w_DATFIN
    return

enddefine

* --- Define pages as container
define class tgsri4sftPag1 as StdContainer
  Width  = 695
  height = 436
  stdWidth  = 695
  stdheight = 436
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFOCOGNOME_1_11 as StdField with uid="SZIDZXIFTU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FOCOGNOME", cQueryName = "FOCOGNOME",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del fornitore",;
    HelpContextID = 265383437,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=104, Top=10, cSayPict="repl('!',24)", cGetPict="repl('!',24)", InputMask=replicate('X',24)

  func oFOCOGNOME_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFONOME_1_12 as StdField with uid="NPXPQLRYZB",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FONOME", cQueryName = "FONOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del fornitore",;
    HelpContextID = 141607594,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=355, Top=10, cSayPict="repl('!',20)", cGetPict="repl('!',20)", InputMask=replicate('X',20)

  func oFONOME_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFODENOMINA_1_13 as StdField with uid="WNCFJWGGXA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FODENOMINA", cQueryName = "FODENOMINA",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione del fornitore",;
    HelpContextID = 241900673,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=104, Top=34, cSayPict="repl('!',60)", cGetPict="repl('!',60)", InputMask=replicate('X',60)

  func oFODENOMINA_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FOCOGNOME) and empty(.w_FONOME))
    endwith
   endif
  endfunc

  func oFODENOMINA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_FODENOMINA))
    endwith
    return bRes
  endfunc

  add object oFOCODFIS_1_14 as StdField with uid="NQSHPZVXNW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FOCODFIS", cQueryName = "FOCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Lunghezza codice fiscale o partita IVA non corretta",;
    HelpContextID = 134312535,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=543, Top=34, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oFOCODFIS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI')))
    endwith
    return bRes
  endfunc

  add object oFLAGEVE_1_16 as StdCheck with uid="WYTEXMRWTY",rtseq=16,rtrep=.f.,left=26, top=84, caption="Eventi eccezionali",;
    HelpContextID = 134638166,;
    cFormVar="w_FLAGEVE", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGEVE_1_16.RadioValue()
    return(iif(this.value =1,'01',;
    '00'))
  endfunc
  func oFLAGEVE_1_16.GetRadio()
    this.Parent.oContained.w_FLAGEVE = this.RadioValue()
    return .t.
  endfunc

  func oFLAGEVE_1_16.SetRadio()
    this.Parent.oContained.w_FLAGEVE=trim(this.Parent.oContained.w_FLAGEVE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGEVE=='01',1,;
      0)
  endfunc

  add object oFLAGCONF_1_17 as StdCheck with uid="EXJMLGCXGO",rtseq=17,rtrep=.f.,left=273, top=84, caption="Conferma",;
    HelpContextID = 253334884,;
    cFormVar="w_FLAGCONF", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCONF_1_17.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCONF_1_17.GetRadio()
    this.Parent.oContained.w_FLAGCONF = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCONF_1_17.SetRadio()
    this.Parent.oContained.w_FLAGCONF=trim(this.Parent.oContained.w_FLAGCONF)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCONF=='1',1,;
      0)
  endfunc

  add object oForza1_1_18 as StdCheck with uid="WYTWDOWGTW",rtseq=18,rtrep=.f.,left=273, top=105, caption="Forza editing",;
    HelpContextID = 184771242,;
    cFormVar="w_Forza1", bObbl = .f. , nPag = 1;
    , tabstop = .f.;
   , bGlobalFont=.t.


  func oForza1_1_18.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oForza1_1_18.GetRadio()
    this.Parent.oContained.w_Forza1 = this.RadioValue()
    return .t.
  endfunc

  func oForza1_1_18.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Forza1==.t.,1,;
      0)
  endfunc

  add object oFLAGCOR_1_19 as StdCheck with uid="KAIBPTXLER",rtseq=19,rtrep=.f.,left=431, top=84, caption="Correttiva",;
    HelpContextID = 15100502,;
    cFormVar="w_FLAGCOR", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCOR_1_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCOR_1_19.GetRadio()
    this.Parent.oContained.w_FLAGCOR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCOR_1_19.SetRadio()
    this.Parent.oContained.w_FLAGCOR=trim(this.Parent.oContained.w_FLAGCOR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCOR=='1',1,;
      0)
  endfunc

  add object oFLAGINT_1_20 as StdCheck with uid="SSJZDRXGKM",rtseq=20,rtrep=.f.,left=431, top=105, caption="Integrativa",;
    HelpContextID = 4614742,;
    cFormVar="w_FLAGINT", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGINT_1_20.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGINT_1_20.GetRadio()
    this.Parent.oContained.w_FLAGINT = this.RadioValue()
    return .t.
  endfunc

  func oFLAGINT_1_20.SetRadio()
    this.Parent.oContained.w_FLAGINT=trim(this.Parent.oContained.w_FLAGINT)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGINT=='1',1,;
      0)
  endfunc


  add object oTIPOPERAZ_1_21 as StdCombo with uid="FVZJLSCZCJ",rtseq=21,rtrep=.f.,left=547,top=100,width=114,height=21;
    , tabstop =.f.;
    , ToolTipText = "Tipo operazioni possibili in correzione o integrazione";
    , HelpContextID = 129981975;
    , cFormVar="w_TIPOPERAZ",RowSource=""+"Inserimento,"+"Aggiornamento,"+"Cancellazione", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Inserire il tipo dell'operazione";
  , bGlobalFont=.t.


  func oTIPOPERAZ_1_21.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    ' '))))
  endfunc
  func oTIPOPERAZ_1_21.GetRadio()
    this.Parent.oContained.w_TIPOPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPERAZ_1_21.SetRadio()
    this.Parent.oContained.w_TIPOPERAZ=trim(this.Parent.oContained.w_TIPOPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPERAZ=='I',1,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='A',2,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='C',3,;
      0)))
  endfunc

  func oTIPOPERAZ_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLAGCOR = '1') OR (.w_FLAGINT = '1'))
    endwith
   endif
  endfunc

  func oTIPOPERAZ_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_FLAGCOR='1' or .w_FLAGINT='1') and not empty(.w_TIPOPERAZ))
    endwith
    return bRes
  endfunc

  add object oDATANASC_1_22 as StdField with uid="WGYOTWHLZZ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DATANASC", cQueryName = "DATANASC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 59871097,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=13, Top=156

  func oDATANASC_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCOMUNE_1_23 as StdField with uid="QDXVFBYYJG",rtseq=23,rtrep=.f.,;
    cFormVar = "w_COMUNE", cQueryName = "COMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 140169946,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=126, Top=156, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCOMUNE_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSIGLA_1_24 as StdField with uid="AQSAXFLTQI",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SIGLA", cQueryName = "SIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 238303194,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=448, Top=156, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSIGLA_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc


  add object oSESSO_1_25 as StdCombo with uid="NCCDEVHYQI",rtseq=25,rtrep=.f.,left=529,top=157,width=92,height=21;
    , height = 21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 223116250;
    , cFormVar="w_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSESSO_1_25.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSESSO_1_25.GetRadio()
    this.Parent.oContained.w_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oSESSO_1_25.SetRadio()
    this.Parent.oContained.w_SESSO=trim(this.Parent.oContained.w_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_SESSO=='M',1,;
      iif(this.Parent.oContained.w_SESSO=='F',2,;
      0))
  endfunc

  func oSESSO_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oVARRES_1_26 as StdField with uid="LCJAHFQKSB",rtseq=26,rtrep=.f.,;
    cFormVar = "w_VARRES", cQueryName = "VARRES",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della variazione",;
    HelpContextID = 85094486,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=13, Top=193

  func oVARRES_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oRECOMUNE_1_27 as StdField with uid="OZIDXVLWTW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_RECOMUNE", cQueryName = "RECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 141654949,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=126, Top=193, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRECOMUNE_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oRESIGLA_1_28 as StdField with uid="SUWJKWJLTK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_RESIGLA", cQueryName = "RESIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire la sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 237601814,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=448, Top=193, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRESIGLA_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oRESIGLA_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_RESIGLA))
    endwith
    return bRes
  endfunc

  add object oINDIRIZ_1_29 as StdField with uid="ZFPHCRDCWK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_INDIRIZ", cQueryName = "INDIRIZ",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 69690234,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=126, Top=228, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oINDIRIZ_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCAP_1_30 as StdField with uid="MDJULVDKRQ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 42971354,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=448, Top=228, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oCAP_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oCAP_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_CAP))
    endwith
    return bRes
  endfunc

  add object oCODATT_1_31 as StdField with uid="UFNDOTUYER",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 116432166,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=519, Top=228, cSayPict='repl("!",5)', cGetPict='repl("!",5)', InputMask=replicate('X',5)

  func oCODATT_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oTELEFONO_1_32 as StdField with uid="QVUZNFWEXA",rtseq=32,rtrep=.f.,;
    cFormVar = "w_TELEFONO", cQueryName = "TELEFONO",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono, inserire solo numeri",;
    HelpContextID = 250276731,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=597, Top=228, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oTELEFONO_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oTELEFONO_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_TELEFONO))=0 and at('/',alltrim(.w_TELEFONO))=0 and at('-',alltrim(.w_TELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oSEVARSED_1_33 as StdField with uid="MKYAOPUZVB",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SEVARSED", cQueryName = "SEVARSED",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data variazione sede legale",;
    HelpContextID = 97629290,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=13, Top=277

  func oSEVARSED_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECOMUNE_1_34 as StdField with uid="NFZSOTAWKE",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SECOMUNE", cQueryName = "SECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 141654933,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=126, Top=277, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSECOMUNE_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSESIGLA_1_35 as StdField with uid="OOQMIUMTFG",rtseq=35,rtrep=.f.,;
    cFormVar = "w_SESIGLA", cQueryName = "SESIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 237601830,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=448, Top=277, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSESIGLA_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSEINDIRI2_1_36 as StdField with uid="OXQZLOKZQT",rtseq=36,rtrep=.f.,;
    cFormVar = "w_SEINDIRI2", cQueryName = "SEINDIRI2",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 184412047,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=126, Top=311, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSEINDIRI2_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECAP_1_37 as StdField with uid="AVSZVWBCSZ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_SECAP", cQueryName = "SECAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.A.P. della sede legale del fornitore",;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 223312858,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=448, Top=311, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oSECAP_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oSECAP_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_SECAP))
    endwith
    return bRes
  endfunc

  add object oSEVARDOM_1_38 as StdField with uid="CYDZEIXVFX",rtseq=38,rtrep=.f.,;
    cFormVar = "w_SEVARDOM", cQueryName = "SEVARDOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data variazione domicilio fiscale",;
    HelpContextID = 154028941,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=13, Top=345

  func oSEVARDOM_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERCOMUN_1_39 as StdField with uid="KMAFMNRNPP",rtseq=39,rtrep=.f.,;
    cFormVar = "w_SERCOMUN", cQueryName = "SERCOMUN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 262370420,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=126, Top=345, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSERCOMUN_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERSIGLA_1_40 as StdField with uid="TGOHNSUWWG",rtseq=40,rtrep=.f.,;
    cFormVar = "w_SERSIGLA", cQueryName = "SERSIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore",;
    HelpContextID = 111971225,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=448, Top=345, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSERSIGLA_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERCAP_1_41 as StdField with uid="KHVCIKTTRJ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_SERCAP", cQueryName = "SERCAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 29586470,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=545, Top=345, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oSERCAP_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oNATGIU_1_42 as StdField with uid="GGFCANZHTS",rtseq=42,rtrep=.f.,;
    cFormVar = "w_NATGIU", cQueryName = "NATGIU",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso per natura giuridica",;
    ToolTipText = "Tipologia natura giuridica",;
    HelpContextID = 122130390,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=26, Left=607, Top=345, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oNATGIU_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oNATGIU_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=43) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=51)  OR val(.w_NATGIU)=53)
    endwith
    return bRes
  endfunc

  add object oSERINDIR_1_43 as StdField with uid="ZEWEROQCEA",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SERINDIR", cQueryName = "SERINDIR",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 157715336,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=126, Top=379, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSERINDIR_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECODATT_1_44 as StdField with uid="RQBOLJQSDR",rtseq=44,rtrep=.f.,;
    cFormVar = "w_SECODATT", cQueryName = "SECODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 50234490,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=448, Top=379, cSayPict='repl("!",5)', cGetPict='repl("!",5)', InputMask=replicate('X',5)

  func oSECODATT_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSETELEFONO_1_45 as StdField with uid="TDMFKZTMEM",rtseq=45,rtrep=.f.,;
    cFormVar = "w_SETELEFONO", cQueryName = "SETELEFONO",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono, inserire solo numeri",;
    HelpContextID = 125167701,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=545, Top=379, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oSETELEFONO_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oSETELEFONO_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_SETELEFONO))=0 and at('/',alltrim(.w_SETELEFONO))=0 and at('-',alltrim(.w_SETELEFONO))=0)
    endwith
    return bRes
  endfunc


  add object oSTATO_1_46 as StdCombo with uid="HVJLCFQSUG",rtseq=46,rtrep=.f.,left=126,top=414,width=132,height=21;
    , height = 21;
    , ToolTipText = "Stato della societ� o ente.";
    , HelpContextID = 223120602;
    , cFormVar="w_STATO",RowSource=""+"1 - Soggetto in normale attivit�,"+"2 - Soggetto in liquidazione per cessazione attivit�,"+"3 - Soggetto in fallimento o in liquidazione coatta amministrativa,"+"4 - Soggetto estinto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_46.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    space(1))))))
  endfunc
  func oSTATO_1_46.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_46.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='1',1,;
      iif(this.Parent.oContained.w_STATO=='2',2,;
      iif(this.Parent.oContained.w_STATO=='3',3,;
      iif(this.Parent.oContained.w_STATO=='4',4,;
      0))))
  endfunc

  func oSTATO_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc


  add object oSITUAZ_1_47 as StdCombo with uid="QWWEIZUDRN",value=1,rtseq=47,rtrep=.f.,left=261,top=414,width=184,height=21;
    , height = 21;
    , ToolTipText = "Situazione della societ� o ente";
    , HelpContextID = 198547494;
    , cFormVar="w_SITUAZ",RowSource=""+"Non selezionata,"+"1 - Periodo imposta inizio liquidazione,"+"2 - Periodo imposta successivo a periodo liquidazione o fallimento,"+"3 - Periodo imposta termine liquidazione,"+"5 - Periodo imposta avvenuta trasformazione,"+"6 - Periodo normale di imposta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSITUAZ_1_47.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    space(1))))))))
  endfunc
  func oSITUAZ_1_47.GetRadio()
    this.Parent.oContained.w_SITUAZ = this.RadioValue()
    return .t.
  endfunc

  func oSITUAZ_1_47.SetRadio()
    this.Parent.oContained.w_SITUAZ=trim(this.Parent.oContained.w_SITUAZ)
    this.value = ;
      iif(this.Parent.oContained.w_SITUAZ=='',1,;
      iif(this.Parent.oContained.w_SITUAZ=='1',2,;
      iif(this.Parent.oContained.w_SITUAZ=='2',3,;
      iif(this.Parent.oContained.w_SITUAZ=='3',4,;
      iif(this.Parent.oContained.w_SITUAZ=='5',5,;
      iif(this.Parent.oContained.w_SITUAZ=='6',6,;
      0))))))
  endfunc

  func oSITUAZ_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCODFISDA_1_48 as StdField with uid="XSNKRKYERI",rtseq=48,rtrep=.f.,;
    cFormVar = "w_CODFISDA", cQueryName = "CODFISDA",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del dicastero di appartenenza",;
    HelpContextID = 88448359,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=448, Top=414, cSayPict='repl("!",11)', cGetPict='repl("!",11)', InputMask=replicate('X',11)

  func oCODFISDA_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oCODFISDA_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CODFISDA),chkcfp(alltrim(.w_CODFISDA),'PI'),.T.))
    endwith
    return bRes
  endfunc

  add object oStr_1_58 as StdString with uid="PZCKHMGKWT",Visible=.t., Left=3, Top=11,;
    Alignment=1, Width=98, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="MIJLOSOMKI",Visible=.t., Left=299, Top=10,;
    Alignment=1, Width=53, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="TZKQRFHOUY",Visible=.t., Left=12, Top=60,;
    Alignment=0, Width=204, Height=15,;
    Caption="Tipo di dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_62 as StdString with uid="UCIEAMMITE",Visible=.t., Left=3, Top=35,;
    Alignment=1, Width=98, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="HLIVBEVOMR",Visible=.t., Left=543, Top=17,;
    Alignment=0, Width=124, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="EFRWRPPCTM",Visible=.t., Left=14, Top=126,;
    Alignment=0, Width=204, Height=15,;
    Caption="Persona fisica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="HRDBHHFBYE",Visible=.t., Left=126, Top=180,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di residenza o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_70 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=13, Top=143,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_71 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=126, Top=143,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_72 as StdString with uid="VETKYUGDVO",Visible=.t., Left=448, Top=143,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_73 as StdString with uid="FWEPYXGQZJ",Visible=.t., Left=13, Top=180,;
    Alignment=0, Width=87, Height=17,;
    Caption="Data variazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_74 as StdString with uid="HVQAZPYBNO",Visible=.t., Left=448, Top=180,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="ATTHQLRPRN",Visible=.t., Left=126, Top=215,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_76 as StdString with uid="KSQHILFMHS",Visible=.t., Left=529, Top=143,;
    Alignment=0, Width=48, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_77 as StdString with uid="IVLKBJFSJW",Visible=.t., Left=448, Top=215,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_78 as StdString with uid="YZOMBVBHCP",Visible=.t., Left=519, Top=215,;
    Alignment=0, Width=77, Height=17,;
    Caption="Cod.attivit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_79 as StdString with uid="AIZMGLVLMZ",Visible=.t., Left=597, Top=215,;
    Alignment=0, Width=48, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_83 as StdString with uid="DEKDZLFCML",Visible=.t., Left=14, Top=248,;
    Alignment=0, Width=228, Height=15,;
    Caption="Altri soggetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_84 as StdString with uid="EBKJKEHJKK",Visible=.t., Left=13, Top=264,;
    Alignment=0, Width=80, Height=13,;
    Caption="Sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_85 as StdString with uid="RZVEDQJFMM",Visible=.t., Left=126, Top=264,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_86 as StdString with uid="UNXIIROKSZ",Visible=.t., Left=607, Top=332,;
    Alignment=0, Width=84, Height=13,;
    Caption="Natura giuridica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_87 as StdString with uid="UQKLITXGSW",Visible=.t., Left=448, Top=264,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_88 as StdString with uid="EXNBDAOCWO",Visible=.t., Left=126, Top=298,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_89 as StdString with uid="WLXGABIKNW",Visible=.t., Left=448, Top=298,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_90 as StdString with uid="RMDNMKOJQJ",Visible=.t., Left=13, Top=332,;
    Alignment=0, Width=76, Height=13,;
    Caption="Dom. fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_91 as StdString with uid="ZVHEBJFODD",Visible=.t., Left=126, Top=332,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_92 as StdString with uid="VJNNNJGRSB",Visible=.t., Left=448, Top=332,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_93 as StdString with uid="XMGFDNXRGY",Visible=.t., Left=126, Top=366,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_94 as StdString with uid="CNBFEAFDFD",Visible=.t., Left=545, Top=332,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_95 as StdString with uid="DYEPGPKKPJ",Visible=.t., Left=448, Top=366,;
    Alignment=0, Width=91, Height=13,;
    Caption="Codice attivit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_96 as StdString with uid="ZUHOUEPINF",Visible=.t., Left=545, Top=366,;
    Alignment=0, Width=48, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_97 as StdString with uid="TVXFVOYTZB",Visible=.t., Left=126, Top=401,;
    Alignment=0, Width=48, Height=13,;
    Caption="Stato"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_98 as StdString with uid="GPUYEQERVJ",Visible=.t., Left=260, Top=401,;
    Alignment=0, Width=64, Height=13,;
    Caption="Situazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_99 as StdString with uid="ARBZPVKLZS",Visible=.t., Left=448, Top=400,;
    Alignment=0, Width=243, Height=13,;
    Caption="Codice fiscale del dicastero di appartenenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_100 as StdString with uid="BBSCZKFXTA",Visible=.t., Left=545, Top=86,;
    Alignment=0, Width=89, Height=13,;
    Caption="Tipo operazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_60 as StdBox with uid="YPBURJVQFL",left=10, top=78, width=681,height=2

  add object oBox_1_67 as StdBox with uid="TFBVYLMEDX",left=10, top=140, width=681,height=2

  add object oBox_1_82 as StdBox with uid="HJLYLCJQUK",left=10, top=263, width=681,height=2
enddefine
define class tgsri4sftPag2 as StdContainer
  Width  = 695
  height = 436
  stdWidth  = 695
  stdheight = 436
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oForza2_2_1 as StdCheck with uid="ANPGYPQFYE",rtseq=63,rtrep=.f.,left=538, top=8, caption="Forza editing",;
    HelpContextID = 167994026,;
    cFormVar="w_Forza2", bObbl = .f. , nPag = 2;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oForza2_2_1.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oForza2_2_1.GetRadio()
    this.Parent.oContained.w_Forza2 = this.RadioValue()
    return .t.
  endfunc

  func oForza2_2_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Forza2==.t.,1,;
      0)
  endfunc

  add object oRFCODFIS_2_2 as StdField with uid="MTXMRCWFQH",rtseq=64,rtrep=.f.,;
    cFormVar = "w_RFCODFIS", cQueryName = "RFCODFIS",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 134314647,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=30, Top=42, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oRFCODFIS_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  func oRFCODFIS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))
    endwith
    return bRes
  endfunc


  add object oRFCODCAR_2_3 as StdCombo with uid="YYMESQRZRO",rtseq=65,rtrep=.f.,left=237,top=42,width=286,height=21;
    , ToolTipText = "Codice carica";
    , HelpContextID = 83789160;
    , cFormVar="w_RFCODCAR",RowSource=""+"1) Rappr. legale o negoziale,"+"2) Rappr. di minore - curatore eredit�,"+"3) Curatore fallimentare,"+"4) Commissario liquidatore,"+"5) Commissario giudiziale,"+"6) Rappr. fiscale di soggetto non residente,"+"7) Erede del dichiarante,"+"8) Liquidatore volontario,"+"9) Sogg. dich. IVA operaz. straord.,"+"11) Tutore,"+"12) Liquid. volont. ditta indiv.,"+"13) Ammin. condominio,"+"14) Sogg. per conto P.A.,"+"15) Comm. liquid. P.A.", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRFCODCAR_2_3.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    iif(this.value =10,'11',;
    iif(this.value =11,'12',;
    iif(this.value =12,'13',;
    iif(this.value =13,'14',;
    iif(this.value =14,'15',;
    '  ')))))))))))))))
  endfunc
  func oRFCODCAR_2_3.GetRadio()
    this.Parent.oContained.w_RFCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oRFCODCAR_2_3.SetRadio()
    this.Parent.oContained.w_RFCODCAR=trim(this.Parent.oContained.w_RFCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_RFCODCAR=='1',1,;
      iif(this.Parent.oContained.w_RFCODCAR=='2',2,;
      iif(this.Parent.oContained.w_RFCODCAR=='3',3,;
      iif(this.Parent.oContained.w_RFCODCAR=='4',4,;
      iif(this.Parent.oContained.w_RFCODCAR=='5',5,;
      iif(this.Parent.oContained.w_RFCODCAR=='6',6,;
      iif(this.Parent.oContained.w_RFCODCAR=='7',7,;
      iif(this.Parent.oContained.w_RFCODCAR=='8',8,;
      iif(this.Parent.oContained.w_RFCODCAR=='9',9,;
      iif(this.Parent.oContained.w_RFCODCAR=='11',10,;
      iif(this.Parent.oContained.w_RFCODCAR=='12',11,;
      iif(this.Parent.oContained.w_RFCODCAR=='13',12,;
      iif(this.Parent.oContained.w_RFCODCAR=='14',13,;
      iif(this.Parent.oContained.w_RFCODCAR=='15',14,;
      0))))))))))))))
  endfunc

  func oRFCODCAR_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCOGNOME_2_4 as StdField with uid="TUTTYIGKSZ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_RFCOGNOME", cQueryName = "RFCOGNOME",;
    bObbl = .f. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del rappresentante",;
    HelpContextID = 265385549,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=30, Top=77, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oRFCOGNOME_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFNOME_2_5 as StdField with uid="RKLEAKPRVC",rtseq=67,rtrep=.f.,;
    cFormVar = "w_RFNOME", cQueryName = "RFNOME",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del rappresentante",;
    HelpContextID = 141609706,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=237, Top=77, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  func oRFNOME_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc


  add object oRFSESSO_2_6 as StdCombo with uid="WGXEPZGXVZ",value=1,rtseq=68,rtrep=.f.,left=418,top=77,width=114,height=21;
    , height = 21;
    , ToolTipText = "Sesso del rappresentante";
    , HelpContextID = 169507562;
    , cFormVar="w_RFSESSO",RowSource=""+"Non selezionato,"+"Maschio,"+"Femmina", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRFSESSO_2_6.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oRFSESSO_2_6.GetRadio()
    this.Parent.oContained.w_RFSESSO = this.RadioValue()
    return .t.
  endfunc

  func oRFSESSO_2_6.SetRadio()
    this.Parent.oContained.w_RFSESSO=trim(this.Parent.oContained.w_RFSESSO)
    this.value = ;
      iif(this.Parent.oContained.w_RFSESSO=='',1,;
      iif(this.Parent.oContained.w_RFSESSO=='M',2,;
      iif(this.Parent.oContained.w_RFSESSO=='F',3,;
      0)))
  endfunc

  func oRFSESSO_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFDATANASC_2_7 as StdField with uid="AUOENXXQZR",rtseq=69,rtrep=.f.,;
    cFormVar = "w_RFDATANASC", cQueryName = "RFDATANASC",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del rappresentante",;
    HelpContextID = 202318457,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=30, Top=114

  func oRFDATANASC_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCOMNAS_2_8 as StdField with uid="RPGGCLVJHX",rtseq=70,rtrep=.f.,;
    cFormVar = "w_RFCOMNAS", cQueryName = "RFCOMNAS",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del rappresentante",;
    HelpContextID = 9340265,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=136, Top=114, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRFCOMNAS_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFSIGNAS_2_9 as StdField with uid="DAVHPWZQVP",rtseq=71,rtrep=.f.,;
    cFormVar = "w_RFSIGNAS", cQueryName = "RFSIGNAS",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del rappresentante",;
    HelpContextID = 2721129,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=416, Top=114, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRFSIGNAS_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCOMUNE_2_10 as StdField with uid="DDNELJFMGC",rtseq=72,rtrep=.f.,;
    cFormVar = "w_RFCOMUNE", cQueryName = "RFCOMUNE",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del rappresentante",;
    HelpContextID = 141654693,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=30, Top=150, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRFCOMUNE_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFSIGLA_2_11 as StdField with uid="MPTADLFURD",rtseq=73,rtrep=.f.,;
    cFormVar = "w_RFSIGLA", cQueryName = "RFSIGLA",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 237602070,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=363, Top=150, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRFSIGLA_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCAP_2_12 as StdField with uid="VMMUJSTIQO",rtseq=74,rtrep=.f.,;
    cFormVar = "w_RFCAP", cQueryName = "RFCAP",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del rappresentante",;
    HelpContextID = 223312618,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=416, Top=150, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oRFCAP_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFINDIRIZ_2_13 as StdField with uid="HCOWWPHROD",rtseq=75,rtrep=.f.,;
    cFormVar = "w_RFINDIRIZ", cQueryName = "RFINDIRIZ",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 184412927,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=30, Top=186, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oRFINDIRIZ_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFTELEFONO_2_14 as StdField with uid="MMGWOVEOZI",rtseq=76,rtrep=.f.,;
    cFormVar = "w_RFTELEFONO", cQueryName = "RFTELEFONO",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    HelpContextID = 125167941,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=363, Top=186, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oRFTELEFONO_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  func oRFTELEFONO_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oSEZ1_2_15 as StdCheck with uid="IATICRAGRO",rtseq=77,rtrep=.f.,left=6, top=227, caption="Sezione I - trasmissione modello 770 semplificato",;
    HelpContextID = 39717850,;
    cFormVar="w_SEZ1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ1_2_15.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ1_2_15.GetRadio()
    this.Parent.oContained.w_SEZ1 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ1_2_15.SetRadio()
    this.Parent.oContained.w_SEZ1=trim(this.Parent.oContained.w_SEZ1)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ1=='1',1,;
      0)
  endfunc

  add object oqST1_2_16 as StdCheck with uid="BGCLFWVZAL",rtseq=78,rtrep=.f.,left=611, top=230, caption="ST",;
    HelpContextID = 39738362,;
    cFormVar="w_qST1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqST1_2_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqST1_2_16.GetRadio()
    this.Parent.oContained.w_qST1 = this.RadioValue()
    return .t.
  endfunc

  func oqST1_2_16.SetRadio()
    this.Parent.oContained.w_qST1=trim(this.Parent.oContained.w_qST1)
    this.value = ;
      iif(this.Parent.oContained.w_qST1=='1',1,;
      0)
  endfunc

  func oqST1_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oqSX1_2_17 as StdCheck with uid="ZXSRWVYXVB",rtseq=79,rtrep=.f.,left=652, top=230, caption="SX",;
    HelpContextID = 39721978,;
    cFormVar="w_qSX1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSX1_2_17.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSX1_2_17.GetRadio()
    this.Parent.oContained.w_qSX1 = this.RadioValue()
    return .t.
  endfunc

  func oqSX1_2_17.SetRadio()
    this.Parent.oContained.w_qSX1=trim(this.Parent.oContained.w_qSX1)
    this.value = ;
      iif(this.Parent.oContained.w_qSX1=='1',1,;
      0)
  endfunc

  func oqSX1_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oFIRMDICH_2_18 as StdCheck with uid="ZFJVFMHAUQ",rtseq=80,rtrep=.f.,left=289, top=249, caption="Dichiarante",;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 184383390,;
    cFormVar="w_FIRMDICH", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMDICH_2_18.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMDICH_2_18.GetRadio()
    this.Parent.oContained.w_FIRMDICH = this.RadioValue()
    return .t.
  endfunc

  func oFIRMDICH_2_18.SetRadio()
    this.Parent.oContained.w_FIRMDICH=trim(this.Parent.oContained.w_FIRMDICH)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMDICH=='1',1,;
      0)
  endfunc

  func oFIRMDICH_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oFIRMPRES_2_19 as StdCheck with uid="XOLWNXUWLK",rtseq=81,rtrep=.f.,left=392, top=249, caption="Presidente o componenti dell'organo di controllo",;
    ToolTipText = "Firma del presidente o dei componenti dell'organo di controllo",;
    HelpContextID = 79525801,;
    cFormVar="w_FIRMPRES", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMPRES_2_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMPRES_2_19.GetRadio()
    this.Parent.oContained.w_FIRMPRES = this.RadioValue()
    return .t.
  endfunc

  func oFIRMPRES_2_19.SetRadio()
    this.Parent.oContained.w_FIRMPRES=trim(this.Parent.oContained.w_FIRMPRES)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMPRES=='1',1,;
      0)
  endfunc

  func oFIRMPRES_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oSEZ2_2_20 as StdCheck with uid="SEIYBKNVNP",rtseq=82,rtrep=.f.,left=6, top=273, caption="Sezione II - trasmissione modello 770 semplificato in due parti",;
    HelpContextID = 39652314,;
    cFormVar="w_SEZ2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ2_2_20.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ2_2_20.GetRadio()
    this.Parent.oContained.w_SEZ2 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ2_2_20.SetRadio()
    this.Parent.oContained.w_SEZ2=trim(this.Parent.oContained.w_SEZ2)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ2=='1',1,;
      0)
  endfunc

  add object oqST12_2_21 as StdCheck with uid="QSHIZVMMBR",rtseq=83,rtrep=.f.,left=611, top=276, caption="ST",;
    HelpContextID = 255745018,;
    cFormVar="w_qST12", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqST12_2_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqST12_2_21.GetRadio()
    this.Parent.oContained.w_qST12 = this.RadioValue()
    return .t.
  endfunc

  func oqST12_2_21.SetRadio()
    this.Parent.oContained.w_qST12=trim(this.Parent.oContained.w_qST12)
    this.value = ;
      iif(this.Parent.oContained.w_qST12=='1',1,;
      0)
  endfunc

  func oqST12_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ2='1')
    endwith
   endif
  endfunc

  add object oqSX12_2_22 as StdCheck with uid="NEUPSDSTFF",rtseq=84,rtrep=.f.,left=652, top=276, caption="SX",;
    HelpContextID = 255728634,;
    cFormVar="w_qSX12", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSX12_2_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSX12_2_22.GetRadio()
    this.Parent.oContained.w_qSX12 = this.RadioValue()
    return .t.
  endfunc

  func oqSX12_2_22.SetRadio()
    this.Parent.oContained.w_qSX12=trim(this.Parent.oContained.w_qSX12)
    this.value = ;
      iif(this.Parent.oContained.w_qSX12=='1',1,;
      0)
  endfunc

  func oqSX12_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ2='1')
    endwith
   endif
  endfunc

  add object oFIRMDICH2_2_23 as StdCheck with uid="APZARCSPGJ",rtseq=85,rtrep=.f.,left=289, top=295, caption="Dichiarante",;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 184384190,;
    cFormVar="w_FIRMDICH2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMDICH2_2_23.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMDICH2_2_23.GetRadio()
    this.Parent.oContained.w_FIRMDICH2 = this.RadioValue()
    return .t.
  endfunc

  func oFIRMDICH2_2_23.SetRadio()
    this.Parent.oContained.w_FIRMDICH2=trim(this.Parent.oContained.w_FIRMDICH2)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMDICH2=='1',1,;
      0)
  endfunc

  func oFIRMDICH2_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ2='1')
    endwith
   endif
  endfunc

  add object oFIRMPRES2_2_24 as StdCheck with uid="ELCGXDDHTP",rtseq=86,rtrep=.f.,left=392, top=295, caption="Presidente o componenti dell'organo di controllo",;
    ToolTipText = "Firma del presidente o dei componenti dell'organo di controllo",;
    HelpContextID = 79526601,;
    cFormVar="w_FIRMPRES2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMPRES2_2_24.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMPRES2_2_24.GetRadio()
    this.Parent.oContained.w_FIRMPRES2 = this.RadioValue()
    return .t.
  endfunc

  func oFIRMPRES2_2_24.SetRadio()
    this.Parent.oContained.w_FIRMPRES2=trim(this.Parent.oContained.w_FIRMPRES2)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMPRES2=='1',1,;
      0)
  endfunc

  func oFIRMPRES2_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ2='1')
    endwith
   endif
  endfunc

  add object oRFCFINS2_2_25 as StdField with uid="ZFMCVGTPTY",rtseq=87,rtrep=.f.,;
    cFormVar = "w_RFCFINS2", cQueryName = "RFCFINS2",;
    bObbl = .f. , nPag = 2, value=space(13), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.F. dell'intermediario della sezione II",;
    ToolTipText = "Codice fiscale dell'intermediario della sezione II",;
    HelpContextID = 4556104,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=543, Top=318, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',13)

  func oRFCFINS2_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ2='1')
    endwith
   endif
  endfunc

  func oRFCFINS2_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFCFINS2),chkcfp(alltrim(.w_RFCFINS2),'CF'),iif(.w_SEZ2='1',.F.,.T.) ))
    endwith
    return bRes
  endfunc

  add object oNUMCAF_2_26 as StdField with uid="KVCYFTJAQL",rtseq=88,rtrep=.f.,;
    cFormVar = "w_NUMCAF", cQueryName = "NUMCAF",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero iscrizione all'albo del C.A.F.",;
    HelpContextID = 138202154,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=9, Top=375, cSayPict='"99999"', cGetPict='"99999"'

  add object oRFDATIMP_2_27 as StdField with uid="UVYTNIYLIS",rtseq=89,rtrep=.f.,;
    cFormVar = "w_RFDATIMP", cQueryName = "RFDATIMP",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data impegno trasmissione fornitura",;
    HelpContextID = 68119194,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=9, Top=413

  add object oIMPTRTEL_2_28 as StdCheck with uid="UWMCFDUCPC",rtseq=90,rtrep=.f.,left=219, top=373, caption="Impegno a trasmettere in via telematica la dich. predisposta dal contribuente",;
    ToolTipText = "Impegno trasmissione telematica dichiarazione predisposta dal contribuente",;
    HelpContextID = 115629010,;
    cFormVar="w_IMPTRTEL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIMPTRTEL_2_28.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oIMPTRTEL_2_28.GetRadio()
    this.Parent.oContained.w_IMPTRTEL = this.RadioValue()
    return .t.
  endfunc

  func oIMPTRTEL_2_28.SetRadio()
    this.Parent.oContained.w_IMPTRTEL=trim(this.Parent.oContained.w_IMPTRTEL)
    this.value = ;
      iif(this.Parent.oContained.w_IMPTRTEL=='1',1,;
      0)
  endfunc

  add object oIMPTRSOG_2_29 as StdCheck with uid="XXHZPERTNM",rtseq=91,rtrep=.f.,left=219, top=394, caption="Impegno a trasmettere in via telematica la dich. del contribuente",;
    ToolTipText = "Impegno a trasmettere in via telematica la dichiarazione del contribuente",;
    HelpContextID = 169583667,;
    cFormVar="w_IMPTRSOG", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIMPTRSOG_2_29.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oIMPTRSOG_2_29.GetRadio()
    this.Parent.oContained.w_IMPTRSOG = this.RadioValue()
    return .t.
  endfunc

  func oIMPTRSOG_2_29.SetRadio()
    this.Parent.oContained.w_IMPTRSOG=trim(this.Parent.oContained.w_IMPTRSOG)
    this.value = ;
      iif(this.Parent.oContained.w_IMPTRSOG=='1',1,;
      0)
  endfunc

  add object oFIRMINT_2_30 as StdCheck with uid="PJOQBGGUFU",rtseq=92,rtrep=.f.,left=219, top=414, caption="Firma intermediario",;
    ToolTipText = "Firma intermediario",;
    HelpContextID = 5076822,;
    cFormVar="w_FIRMINT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMINT_2_30.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMINT_2_30.GetRadio()
    this.Parent.oContained.w_FIRMINT = this.RadioValue()
    return .t.
  endfunc

  func oFIRMINT_2_30.SetRadio()
    this.Parent.oContained.w_FIRMINT=trim(this.Parent.oContained.w_FIRMINT)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMINT=='1',1,;
      0)
  endfunc

  add object oStr_2_31 as StdString with uid="XHNHYLLXLG",Visible=.t., Left=400, Top=232,;
    Alignment=1, Width=208, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="FTEQCYTUWC",Visible=.t., Left=11, Top=211,;
    Alignment=0, Width=204, Height=15,;
    Caption="Firma della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_35 as StdString with uid="FHMRICXWEG",Visible=.t., Left=11, Top=11,;
    Alignment=0, Width=477, Height=15,;
    Caption="Dati relativi al rappresentante firmatario della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_36 as StdString with uid="RULZHJZXRW",Visible=.t., Left=30, Top=29,;
    Alignment=0, Width=112, Height=13,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_37 as StdString with uid="USKIUPDQMV",Visible=.t., Left=237, Top=29,;
    Alignment=0, Width=84, Height=13,;
    Caption="Codice carica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_38 as StdString with uid="GPZTYMBFPO",Visible=.t., Left=136, Top=101,;
    Alignment=0, Width=104, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_39 as StdString with uid="RCZFFIGQZJ",Visible=.t., Left=30, Top=64,;
    Alignment=0, Width=76, Height=13,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_40 as StdString with uid="BXGOIXINBX",Visible=.t., Left=237, Top=64,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_41 as StdString with uid="XMDUKEDFTN",Visible=.t., Left=418, Top=63,;
    Alignment=0, Width=76, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_42 as StdString with uid="VNNLUPWWEO",Visible=.t., Left=30, Top=101,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_43 as StdString with uid="LCBMVTEUCO",Visible=.t., Left=416, Top=101,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_44 as StdString with uid="ECENBGSUXY",Visible=.t., Left=30, Top=137,;
    Alignment=0, Width=120, Height=13,;
    Caption="Comune di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_45 as StdString with uid="WXIGENELJC",Visible=.t., Left=363, Top=137,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_46 as StdString with uid="FSUMAEAACK",Visible=.t., Left=416, Top=137,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_47 as StdString with uid="XDOCDGJPIJ",Visible=.t., Left=30, Top=173,;
    Alignment=0, Width=48, Height=13,;
    Caption="Indirizzo"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_48 as StdString with uid="XJBBNGRGLQ",Visible=.t., Left=363, Top=173,;
    Alignment=0, Width=48, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_49 as StdString with uid="LQRAAIJAJN",Visible=.t., Left=9, Top=396,;
    Alignment=0, Width=183, Height=13,;
    Caption="Data dell'impegno a trasmettere"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_50 as StdString with uid="ELNVKYREGV",Visible=.t., Left=11, Top=341,;
    Alignment=0, Width=253, Height=15,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_52 as StdString with uid="EMANNTQNBZ",Visible=.t., Left=9, Top=359,;
    Alignment=0, Width=203, Height=13,;
    Caption="Numero di iscrizione all'albo del C.A.F."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_53 as StdString with uid="MZPJYLMUUD",Visible=.t., Left=76, Top=251,;
    Alignment=1, Width=208, Height=18,;
    Caption="Firma della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="TXZKOFBIQL",Visible=.t., Left=400, Top=278,;
    Alignment=1, Width=208, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="YRXWPQTKSR",Visible=.t., Left=76, Top=297,;
    Alignment=1, Width=208, Height=18,;
    Caption="Firma della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="OXYILKHLHG",Visible=.t., Left=16, Top=319,;
    Alignment=1, Width=523, Height=18,;
    Caption="Codice fiscale dell'intermediario che presenta la restante parte della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oBox_2_32 as StdBox with uid="FZZKXTTQTF",left=11, top=226, width=681,height=2

  add object oBox_2_34 as StdBox with uid="LEBGSAUSDO",left=11, top=26, width=681,height=2

  add object oBox_2_51 as StdBox with uid="DTCNDAKETU",left=11, top=358, width=681,height=2

  add object oBox_2_55 as StdBox with uid="LDVQJNYPWN",left=11, top=272, width=672,height=2
enddefine
define class tgsri4sftPag3 as StdContainer
  Width  = 695
  height = 436
  stdWidth  = 695
  stdheight = 436
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPFORN_3_1 as StdCombo with uid="TMIIFPZQPN",rtseq=93,rtrep=.f.,left=95,top=11,width=300,height=21;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 190425034;
    , cFormVar="w_TIPFORN",RowSource=""+"01: Soggetti che inviano le dichiarazioni,"+"06: Amministrazione dello stato,"+"07: Ente poste,"+"08: Banche convenzionate,"+"10: C.A.F. dip. e pens C.A.F. imp art3 C 2 altri int.", bObbl = .f. , nPag = 3;
    , sErrorMsg = "Selezione non consentita con tipo fornitore persona fisica";
  , bGlobalFont=.t.


  func oTIPFORN_3_1.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'06',;
    iif(this.value =3,'07',;
    iif(this.value =4,'08',;
    iif(this.value =5,'10',;
    space(2)))))))
  endfunc
  func oTIPFORN_3_1.GetRadio()
    this.Parent.oContained.w_TIPFORN = this.RadioValue()
    return .t.
  endfunc

  func oTIPFORN_3_1.SetRadio()
    this.Parent.oContained.w_TIPFORN=trim(this.Parent.oContained.w_TIPFORN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFORN=='01',1,;
      iif(this.Parent.oContained.w_TIPFORN=='06',2,;
      iif(this.Parent.oContained.w_TIPFORN=='07',3,;
      iif(this.Parent.oContained.w_TIPFORN=='08',4,;
      iif(this.Parent.oContained.w_TIPFORN=='10',5,;
      0)))))
  endfunc

  func oTIPFORN_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_Perazi='S' and  .w_TIPFORN<>'06' ) or .w_Perazi<>'S')
    endwith
    return bRes
  endfunc

  add object oCODFISIN_3_2 as StdField with uid="OIWLMRKWII",rtseq=94,rtrep=.f.,;
    cFormVar = "w_CODFISIN", cQueryName = "CODFISIN",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'intermediario che effettua la trasmissione",;
    HelpContextID = 179987084,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=544, Top=11, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCODFISIN_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oFODATANASC_3_3 as StdField with uid="DDIBJMNRLH",rtseq=95,rtrep=.f.,;
    cFormVar = "w_FODATANASC", cQueryName = "FODATANASC",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 202316345,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=61, Top=77

  func oFODATANASC_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc


  add object oFOSESSO_3_4 as StdCombo with uid="EZVQYTQPNM",value=1,rtseq=96,rtrep=.f.,left=171,top=77,width=96,height=21;
    , height = 21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 169505450;
    , cFormVar="w_FOSESSO",RowSource=""+"Non selezionato,"+"Maschio,"+"Femmina", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oFOSESSO_3_4.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oFOSESSO_3_4.GetRadio()
    this.Parent.oContained.w_FOSESSO = this.RadioValue()
    return .t.
  endfunc

  func oFOSESSO_3_4.SetRadio()
    this.Parent.oContained.w_FOSESSO=trim(this.Parent.oContained.w_FOSESSO)
    this.value = ;
      iif(this.Parent.oContained.w_FOSESSO=='',1,;
      iif(this.Parent.oContained.w_FOSESSO=='M',2,;
      iif(this.Parent.oContained.w_FOSESSO=='F',3,;
      0)))
  endfunc

  func oFOSESSO_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S'  or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOCOMUNE_3_5 as StdField with uid="QAAPEJDMEJ",rtseq=97,rtrep=.f.,;
    cFormVar = "w_FOCOMUNE", cQueryName = "FOCOMUNE",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 141652581,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=275, Top=77, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOCOMUNE_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSIGLA_3_6 as StdField with uid="PWEJHDBSSF",rtseq=98,rtrep=.f.,;
    cFormVar = "w_FOSIGLA", cQueryName = "FOSIGLA",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 237604182,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=603, Top=77, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSIGLA_3_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFORECOMUNE_3_7 as StdField with uid="RGOQWOEVVH",rtseq=99,rtrep=.f.,;
    cFormVar = "w_FORECOMUNE", cQueryName = "FORECOMUNE",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 253376629,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=61, Top=110, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFORECOMUNE_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFORESIGLA_3_8 as StdField with uid="MJCFSUINVZ",rtseq=100,rtrep=.f.,;
    cFormVar = "w_FORESIGLA", cQueryName = "FORESIGLA",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 199590322,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=475, Top=110, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFORESIGLA_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOCAP_3_9 as StdField with uid="TTYPCQBXFI",rtseq=101,rtrep=.f.,;
    cFormVar = "w_FOCAP", cQueryName = "FOCAP",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 223310506,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=539, Top=110, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oFOCAP_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOINDIRIZ_3_10 as StdField with uid="ZFPYJFYYCZ",rtseq=102,rtrep=.f.,;
    cFormVar = "w_FOINDIRIZ", cQueryName = "FOINDIRIZ",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 184415039,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=61, Top=143, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOINDIRIZ_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSECOMUNE_3_12 as StdField with uid="FTDECCKCEA",rtseq=103,rtrep=.f.,;
    cFormVar = "w_FOSECOMUNE", cQueryName = "FOSECOMUNE",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 253372533,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=61, Top=205, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOSECOMUNE_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSESIGLA_3_13 as StdField with uid="QBWKROZPRH",rtseq=104,rtrep=.f.,;
    cFormVar = "w_FOSESIGLA", cQueryName = "FOSESIGLA",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 199594418,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=475, Top=205, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSESIGLA_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSECAP_3_14 as StdField with uid="UHFFPYICUM",rtseq=105,rtrep=.f.,;
    cFormVar = "w_FOSECAP", cQueryName = "FOSECAP",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 219837098,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=539, Top=205, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oFOSECAP_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSEINDIRI2_3_15 as StdField with uid="UCUBGSOZHZ",rtseq=106,rtrep=.f.,;
    cFormVar = "w_FOSEINDIRI2", cQueryName = "FOSEINDIRI2",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 4783039,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=61, Top=238, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOSEINDIRI2_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERCOMUN_3_16 as StdField with uid="VRBXTIUTMI",rtseq=107,rtrep=.f.,;
    cFormVar = "w_FOSERCOMUN", cQueryName = "FOSERCOMUN",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 170532621,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=61, Top=271, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOSERCOMUN_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERSIGLA_3_17 as StdField with uid="RALWYRLTJY",rtseq=108,rtrep=.f.,;
    cFormVar = "w_FOSERSIGLA", cQueryName = "FOSERSIGLA",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore",;
    HelpContextID = 170536099,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=475, Top=271, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSERSIGLA_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERCAP_3_18 as StdField with uid="UZATTOELQF",rtseq=109,rtrep=.f.,;
    cFormVar = "w_FOSERCAP", cQueryName = "FOSERCAP",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 97881510,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=539, Top=271, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oFOSERCAP_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERINDIR_3_19 as StdField with uid="OCERQWQXAX",rtseq=110,rtrep=.f.,;
    cFormVar = "w_FOSERINDIR", cQueryName = "FOSERINDIR",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 69868502,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=61, Top=304, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOSERINDIR_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oCFRESCAF_3_20 as StdField with uid="TRSLJXUKSK",rtseq=111,rtrep=.f.,;
    cFormVar = "w_CFRESCAF", cQueryName = "CFRESCAF",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 98923628,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=20, Top=376, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCFRESCAF_3_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFRESCAF),chkcfp(alltrim(.w_CFRESCAF),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oVISTOA35_3_21 as StdCheck with uid="GWFBKDOAOR",rtseq=112,rtrep=.f.,left=20, top=407, caption="Visto ai sensi art. 35 del D.lgs. 9 luglio 1997, n. 241 e suc. mod.",;
    ToolTipText = "Visto art.35 del D.lgs.9 luglio 1997, n.241",;
    HelpContextID = 62163083,;
    cFormVar="w_VISTOA35", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVISTOA35_3_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oVISTOA35_3_21.GetRadio()
    this.Parent.oContained.w_VISTOA35 = this.RadioValue()
    return .t.
  endfunc

  func oVISTOA35_3_21.SetRadio()
    this.Parent.oContained.w_VISTOA35=trim(this.Parent.oContained.w_VISTOA35)
    this.value = ;
      iif(this.Parent.oContained.w_VISTOA35=='1',1,;
      0)
  endfunc

  add object oFLAGFIR_3_22 as StdCheck with uid="GMLRFHNQTY",rtseq=113,rtrep=.f.,left=353, top=379, caption="Flag firma del riquadro visto di conformit�",;
    ToolTipText = "Flag firma riquadro 'visto conformit�'",;
    HelpContextID = 186018390,;
    cFormVar="w_FLAGFIR", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLAGFIR_3_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGFIR_3_22.GetRadio()
    this.Parent.oContained.w_FLAGFIR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGFIR_3_22.SetRadio()
    this.Parent.oContained.w_FLAGFIR=trim(this.Parent.oContained.w_FLAGFIR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGFIR=='1',1,;
      0)
  endfunc

  add object oStr_3_24 as StdString with uid="DCPHCCFCXL",Visible=.t., Left=14, Top=44,;
    Alignment=0, Width=204, Height=15,;
    Caption="Persona fisica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_25 as StdString with uid="YGYYBXXOBR",Visible=.t., Left=14, Top=172,;
    Alignment=0, Width=228, Height=15,;
    Caption="Altri soggetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_26 as StdString with uid="CCNGTZXXUE",Visible=.t., Left=61, Top=97,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di residenza o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_27 as StdString with uid="AKNIDKFOUL",Visible=.t., Left=61, Top=64,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_28 as StdString with uid="XXYSRZPODK",Visible=.t., Left=275, Top=64,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_29 as StdString with uid="GTEVNWAGWC",Visible=.t., Left=603, Top=64,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_30 as StdString with uid="AVMJOZOAMZ",Visible=.t., Left=61, Top=192,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune della sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_31 as StdString with uid="ARKFUFVBKV",Visible=.t., Left=475, Top=97,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_32 as StdString with uid="OWDEPTUCER",Visible=.t., Left=61, Top=130,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_33 as StdString with uid="ASIOLJGQNU",Visible=.t., Left=171, Top=64,;
    Alignment=0, Width=48, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_34 as StdString with uid="RFSIWFUVPA",Visible=.t., Left=539, Top=97,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_35 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=3, Top=11,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_3_36 as StdString with uid="HNBYHSXXZT",Visible=.t., Left=475, Top=192,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_37 as StdString with uid="GQYCDLXSEK",Visible=.t., Left=61, Top=291,;
    Alignment=0, Width=149, Height=13,;
    Caption="Indirizzo di domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_38 as StdString with uid="XRGWLJIBWO",Visible=.t., Left=475, Top=258,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_39 as StdString with uid="BCAUNHALPM",Visible=.t., Left=539, Top=192,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_40 as StdString with uid="OXXFNCZRJN",Visible=.t., Left=539, Top=258,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_41 as StdString with uid="ZFKUQNCSTW",Visible=.t., Left=61, Top=225,;
    Alignment=0, Width=223, Height=13,;
    Caption="Indirizzo della sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_42 as StdString with uid="TNQFMKZSKA",Visible=.t., Left=61, Top=258,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_43 as StdString with uid="AJIXNHKAAD",Visible=.t., Left=397, Top=11,;
    Alignment=1, Width=145, Height=15,;
    Caption="Cod. fis. intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_3_44 as StdString with uid="CSLGHNVXQQ",Visible=.t., Left=14, Top=332,;
    Alignment=0, Width=228, Height=15,;
    Caption="Visto di conformit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_46 as StdString with uid="PFUNCMOJXV",Visible=.t., Left=20, Top=363,;
    Alignment=0, Width=348, Height=13,;
    Caption="Codice fiscale del responsabile del C.A.F. o professionista"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_11 as StdBox with uid="BTOQXYGKKW",left=10, top=59, width=672,height=2

  add object oBox_3_23 as StdBox with uid="QVBCKAWCXU",left=10, top=187, width=672,height=2

  add object oBox_3_45 as StdBox with uid="FWAQWRIPNX",left=10, top=347, width=672,height=2
enddefine
define class tgsri4sftPag4 as StdContainer
  Width  = 695
  height = 436
  stdWidth  = 695
  stdheight = 436
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_4_1 as StdField with uid="JPXZKEKVIP",rtseq=114,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data iniziale di stampa",;
    HelpContextID = 73822410,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=108, Top=34

  func oDATINI_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATINI > cp_CharToDate("31-12-02")))
    endwith
    return bRes
  endfunc

  add object oDATFIN_4_2 as StdField with uid="FNNEPIJJDP",rtseq=115,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data finale di stampa",;
    HelpContextID = 4624182,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=373, Top=34

  func oDATFIN_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN < cp_CharToDate("01-01-04")))
    endwith
    return bRes
  endfunc

  add object oDirName_4_3 as StdField with uid="EJPBDNOIEF",rtseq=116,rtrep=.f.,;
    cFormVar = "w_DirName", cQueryName = "DirName",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 13670198,;
   bGlobalFont=.t.,;
    Height=21, Width=446, Left=108, Top=123, InputMask=replicate('X',200)


  add object oBtn_4_4 as StdButton with uid="QIOPRPPDCF",left=556, top=123, width=18,height=21,;
    caption="...", nPag=4;
    , HelpContextID = 43115818;
    , tabstop = .f.;
  , bGlobalFont=.t.

    proc oBtn_4_4.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFileName_4_5 as StdField with uid="OOIIRZMLFQ",rtseq=117,rtrep=.f.,;
    cFormVar = "w_FileName", cQueryName = "FileName",;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 206096453,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=108, Top=145, InputMask=replicate('X',30)

  add object oIDESTA_4_6 as StdField with uid="QWRFCHFTQX",rtseq=118,rtrep=.f.,;
    cFormVar = "w_IDESTA", cQueryName = "IDESTA",;
    bObbl = .t. , nPag = 4, value=space(17), bMultilanguage =  .f.,;
    HelpContextID = 201153914,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=373, Top=243, InputMask=replicate('X',17), inputmask='99999999999999999'

  func oIDESTA_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
    endwith
   endif
  endfunc

  add object oPROIFT_4_7 as StdField with uid="COXZWAWVRC",rtseq=119,rtrep=.f.,;
    cFormVar = "w_PROIFT", cQueryName = "PROIFT",;
    bObbl = .t. , nPag = 4, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 102322422,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=373, Top=265, InputMask=replicate('X',6), inputmask='999999'

  func oPROIFT_4_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
    endwith
   endif
  endfunc


  add object oBtn_4_8 as StdButton with uid="AXYVHWBWMA",left=542, top=387, width=48,height=45,;
    CpPicture="bmp\Stampa.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 98472742;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_8.Click()
      with this.Parent.oContained
        do gsri1bsp with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_9 as StdButton with uid="ARLJUNADDL",left=592, top=387, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per inizio elaborazione";
    , HelpContextID = 43288090;
    , tabstop = .f.,Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_9.Click()
      with this.Parent.oContained
        do GSRI2BFT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_10 as StdButton with uid="SIRVPTMAGL",left=642, top=387, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 35999418;
    , tabstop = .f.,Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_4_16 as StdString with uid="LCHEPLEODA",Visible=.t., Left=34, Top=34,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_4_17 as StdString with uid="GUHHFLLZER",Visible=.t., Left=306, Top=34,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_4_18 as StdString with uid="QDPZXPECZB",Visible=.t., Left=16, Top=123,;
    Alignment=1, Width=88, Height=15,;
    Caption="Directory:"  ;
  , bGlobalFont=.t.

  add object oStr_4_19 as StdString with uid="FKJYZDRMHF",Visible=.t., Left=16, Top=145,;
    Alignment=1, Width=88, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_4_20 as StdString with uid="TKFFKWNXKB",Visible=.t., Left=11, Top=11,;
    Alignment=0, Width=163, Height=15,;
    Caption="Parametri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_23 as StdString with uid="KLZWVWGXPN",Visible=.t., Left=11, Top=90,;
    Alignment=0, Width=204, Height=15,;
    Caption="File telematico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_25 as StdString with uid="XKVUVPRKKQ",Visible=.t., Left=11, Top=211,;
    Alignment=0, Width=389, Height=15,;
    Caption="Protocollo telematico della dichiarazione da sostituire"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_26 as StdString with uid="ETMYQQHNMJ",Visible=.t., Left=33, Top=243,;
    Alignment=1, Width=336, Height=15,;
    Caption="Identificativo assegnato dal servizio telematico all'invio:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="ZMKICUMICZ",Visible=.t., Left=33, Top=265,;
    Alignment=1, Width=336, Height=15,;
    Caption="Progressivo dichiarazione all'interno del file inviato:"  ;
  , bGlobalFont=.t.

  add object oBox_4_21 as StdBox with uid="EWLUHYXJCA",left=7, top=26, width=676,height=2

  add object oBox_4_22 as StdBox with uid="ALLGPWEGAO",left=7, top=105, width=676,height=2

  add object oBox_4_24 as StdBox with uid="FUJGWUZXRO",left=7, top=226, width=676,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri4sft','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsri4sft
proc SfogliaDir (parent)
local PathName, oField
  PathName = Cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
  endif
endproc

func IsPerFis(Cognome,Nome,Denominazione)
do case
case empty(Cognome) and empty(Nome) and empty(Denominazione)
  return 'N'
case !empty(Cognome) or !empty(Nome)
  return 'S'
case !empty(Denominazione)
  return 'N'
endcase
* --- Fine Area Manuale
