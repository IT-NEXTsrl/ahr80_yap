* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bdm                                                        *
*              Elimina rif.partita                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_25]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2013-06-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bdm",oParentObject,m.pEXEC)
return(i_retval)

define class tgsri_bdm as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_OK = .f.
  w_MESS = space(100)
  w_SERIALE = space(10)
  w_MESS1 = space(100)
  w_NUMREG = 0
  w_DATREG = ctod("  /  /  ")
  w_CMRRIFDI1 = space(10)
  w_CMRRIFDI2 = space(10)
  w_MESS2 = space(100)
  w_MESS3 = space(100)
  w_NUMPRIMA = 0
  w_DATPRIMA = ctod("  /  /  ")
  w_MESS4 = space(100)
  * --- WorkFile variables
  PAR_TITE_idx=0
  MOV_RITE_idx=0
  VEP_RITE_idx=0
  VEN_RITE_idx=0
  DEP_RITE_idx=0
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch permette di controllare se il movimento Ritenuta che
    *     si intende cancellare � stato abbinato ad un movimento IRPEF oppure INPS.
    *     Inoltre cancella dopo richiesta i movimenti generati dalla medesima partita.
    * --- D: esegue controlli in modifica e cancellazione 
    *     S: esegue stampa compensi pagati al F2
    do case
      case this.pEXEC="D"
        this.w_OK = .T.
        this.w_MESS4 = ah_msgformat("Operazione annullata")
        do case
          case NOT EMPTY(NVL(this.oParentObject.w_MRRIFDI1,""))
            * --- Leggo il movimento di distinta assocciato.
            * --- Read from VEP_RITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VEP_RITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VEP_RITE_idx,2],.t.,this.VEP_RITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VPNUMREG,VPDATREG"+;
                " from "+i_cTable+" VEP_RITE where ";
                    +"VPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MRRIFDI1);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VPNUMREG,VPDATREG;
                from (i_cTable) where;
                    VPSERIAL = this.oParentObject.w_MRRIFDI1;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NUMREG = NVL(cp_ToDate(_read_.VPNUMREG),cp_NullValue(_read_.VPNUMREG))
              this.w_DATREG = NVL(cp_ToDate(_read_.VPDATREG),cp_NullValue(_read_.VPDATREG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_MESS1 = "Il movimento ritenuta � associato alla distinta di versamento IRPEF numero: %1 del %2%0eliminare prima il movimento dalla distinta"
            ah_ErrorMsg(this.w_MESS1,"","",ALLTRIM(STR(this.w_NUMREG)),Dtoc(this.w_DATREG))
            if This.OparentObject.cFunction="Edit"
              This.Oparentobject.EcpSave()
            else
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg="Operazione annullata"
            endif
            i_retcode = 'stop'
            return
        endcase
        do case
          case NOT EMPTY(NVL(this.oParentObject.w_MRRIFDI2,""))
            * --- Leggo il movimento di distinta assocciato.
            * --- Read from VEN_RITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VEN_RITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VEN_RITE_idx,2],.t.,this.VEN_RITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VPNUMREG,VPDATREG"+;
                " from "+i_cTable+" VEN_RITE where ";
                    +"VPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MRRIFDI2);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VPNUMREG,VPDATREG;
                from (i_cTable) where;
                    VPSERIAL = this.oParentObject.w_MRRIFDI2;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NUMREG = NVL(cp_ToDate(_read_.VPNUMREG),cp_NullValue(_read_.VPNUMREG))
              this.w_DATREG = NVL(cp_ToDate(_read_.VPDATREG),cp_NullValue(_read_.VPDATREG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_MESS1 = "Il movimento ritenuta � associato alla distinta di versamento INPS numero: %1 del %2%0eliminare prima il movimento dalla distinta"
            ah_ErrorMsg(this.w_MESS1,"","",ALLTRIM(STR(this.w_NUMREG)),Dtoc(this.w_DATREG))
            if This.OparentObject.cFunction="Edit"
              This.Oparentobject.EcpSave()
            else
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS4
            endif
            i_retcode = 'stop'
            return
        endcase
        * --- Devo verificare se il movimento in questione ha generato un giroconto ritenute.
        do case
          case NOT EMPTY(this.oParentObject.w_MRGENGIR)
            * --- Risalgo agli estremi del movimento di primanota.
            * --- Read from PNT_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PNNUMRER,PNDATREG"+;
                " from "+i_cTable+" PNT_MAST where ";
                    +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MRGENGIR);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PNNUMRER,PNDATREG;
                from (i_cTable) where;
                    PNSERIAL = this.oParentObject.w_MRGENGIR;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NUMPRIMA = NVL(cp_ToDate(_read_.PNNUMRER),cp_NullValue(_read_.PNNUMRER))
              this.w_DATPRIMA = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_MESS3 = "Il movimento ritenuta � associato al movimento di primanota numero: %1 del %2%0eliminare prima il movimento di primanota"
            ah_ErrorMsg(this.w_MESS3,"!","",ALLTRIM(STR(this.w_NUMPRIMA)),Dtoc(this.w_DATPRIMA))
            if This.OparentObject.cFunction="Edit"
              This.Oparentobject.EcpSave()
            else
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS4
            endif
            i_retcode = 'stop'
            return
        endcase
        * --- Questa parte di batch deve essere lanciata solo nel caso in cui mi trovo in cancellazione.
        if This.OparentObject.cFunction<>"Edit"
          this.w_OK = .T.
          if NOT EMPTY(this.oParentObject.w_MRRIFCON) 
            if this.oParentObject.w_MRRIFORD<>0 AND this.oParentObject.w_MRRIFNUM<>0
              * --- Controllo che non esistano Movimenti Ritenute
              * --- riferite alla medesima Partita.
              * --- Controllo se esistono movimenti assocciati alla medesima partita
              vq_exec("..\RITE\EXE\QUERY\GSRI_BDM.VQR",this,"RIFCON")
              if Reccount("RIFCON")>1
                Select RIFCON
                Go Top
                Scan
                this.w_CMRRIFDI1 = NVL(MRRIFDI1,"")
                this.w_CMRRIFDI2 = NVL(MRRIFDI2,"")
                if Not Empty(Nvl(this.w_CMRRIFDI1,"")) Or Not Empty(Nvl(this.w_CMRRIFDI2,""))
                  this.w_MESS2 = "Esistono movimenti ritenute generati dalla stessa registrazione contabile ma associati a distinte versamento%0eliminare prima il movimento della distinta"
                  ah_ErrorMsg(this.w_MESS2,"","")
                  this.w_OK = .F.
                  EXIT
                endif
                Endscan
                if this.w_OK=.T.
                  this.w_OK = ah_YesNo("Attenzione:%0Esistono movimenti ritenute generati dalla stessa registrazione contabile%0La cancellazione di questo movimento provocher� l'eliminazione dei movimenti collegati%0Confermi ugualmente?","")
                endif
                if this.w_OK=.T.
                  * --- Elimino le Ritenute trovate associate ai Riferimenti alla Partita 
                  Select RIFCON
                  Go Top
                  Scan for MRSERIAL<>this.oParentObject.w_MRSERIAL
                  this.w_SERIALE = NVL(MRSERIAL,"")
                  * --- Delete from DEP_RITE
                  i_nConn=i_TableProp[this.DEP_RITE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DEP_RITE_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                          +"DPSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                           )
                  else
                    delete from (i_cTable) where;
                          DPSERIAL = this.w_SERIALE;

                    i_Rows=_tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    * --- Error: delete not accepted
                    i_Error=MSG_DELETE_ERROR
                    return
                  endif
                  * --- Delete from MOV_RITE
                  i_nConn=i_TableProp[this.MOV_RITE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                          +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                           )
                  else
                    delete from (i_cTable) where;
                          MRSERIAL = this.w_SERIALE;

                    i_Rows=_tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    * --- Error: delete not accepted
                    i_Error=MSG_DELETE_ERROR
                    return
                  endif
                  Endscan
                endif
              endif
              if this.w_OK=.T.
                * --- Write into PAR_TITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PTFLRITE ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTFLRITE');
                      +i_ccchkf ;
                  +" where ";
                      +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MRRIFCON);
                      +" and PTROWORD = "+cp_ToStrODBC(this.oParentObject.w_MRRIFORD);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MRRIFNUM);
                         )
                else
                  update (i_cTable) set;
                      PTFLRITE = 0;
                      &i_ccchkf. ;
                   where;
                      PTSERIAL = this.oParentObject.w_MRRIFCON;
                      and PTROWORD = this.oParentObject.w_MRRIFORD;
                      and CPROWNUM = this.oParentObject.w_MRRIFNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
            if USED("RIFCON")
              Select RIFCON
              Use
            endif
            if this.w_OK=.F.
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS4
            endif
          endif
        endif
      case this.pEXEC="S"
        if this.oParentObject.w_MRTIPCON="F"
          * --- Lancio stampa compensi pagati nel caso di Ritenute Operate
          do GSRI_SMR with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Lancio stampa movimenti ritenute subite
          do GSRI_SMO with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pEXEC $ "R-T"
        * --- Select from GSRI_AMR
        do vq_exec with 'GSRI_AMR',this,'_Curs_GSRI_AMR','',.f.,.t.
        if used('_Curs_GSRI_AMR')
          select _Curs_GSRI_AMR
          locate for 1=1
          if not(eof())
          do while not(eof())
          this.oParentObject.w_MRPERRI1 = RITENUTA
            select _Curs_GSRI_AMR
            continue
          enddo
          else
            if this.pEXEC="T"
              ah_ERRORmsg("Codice tributo non definito in tabella",64)
            endif
            this.oParentObject.w_MRPERRI1 = 0
            select _Curs_GSRI_AMR
          endif
          use
        endif
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='MOV_RITE'
    this.cWorkTables[3]='VEP_RITE'
    this.cWorkTables[4]='VEN_RITE'
    this.cWorkTables[5]='DEP_RITE'
    this.cWorkTables[6]='PNT_MAST'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_GSRI_AMR')
      use in _Curs_GSRI_AMR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
