* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_mcu                                                        *
*              Dettaglio Certificazione Unica                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-01-27                                                      *
* Last revis.: 2015-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsri_mcu")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsri_mcu")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsri_mcu")
  return

* --- Class definition
define class tgsri_mcu as StdPCForm
  Width  = 596
  Height = 277
  Top    = 10
  Left   = 10
  cComment = "Dettaglio Certificazione Unica"
  cPrg = "gsri_mcu"
  HelpContextID=97121897
  add object cnt as tcgsri_mcu
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsri_mcu as PCContext
  w_CDSERIAL = space(10)
  w_CDCODFIS = space(16)
  w_CDFPERCI = space(15)
  w_CDPEREST = space(1)
  w_CDCOGNOM = space(60)
  w_CD__NOME = space(50)
  w_CD_SESSO = space(1)
  w_CDDATNAS = space(8)
  w_CDCOMNAS = space(30)
  w_CDPRONAS = space(2)
  w_CDCATPAR = space(2)
  w_CDEVEECC = space(1)
  w_CDCOMFIS = space(30)
  w_CDPROFIS = space(2)
  w_CDCOMUNE = space(4)
  w_CDCODFES = space(25)
  w_CDLOCEST = space(30)
  w_CDINDIRI = space(35)
  w_CDCODEST = space(5)
  w_CDCAUPRE = space(2)
  w_CDAMMCOR = 0
  w_CDSOGRES = space(1)
  w_CDSNSOGG = 0
  w_CDCODICE = space(1)
  w_CDIMPONI = 0
  w_CDRITACC = 0
  w_CDSOGERO = 0
  w_CDPERCIP = 0
  w_CDSPERIM = 0
  w_CDRITRIM = 0
  w_CDPROCER = 0
  w_CDPROTEC = space(17)
  w_CDPRODOC = 0
  w_CDSERCER = space(30)
  w_CDPROMOD = 0
  proc Save(i_oFrom)
    this.w_CDSERIAL = i_oFrom.w_CDSERIAL
    this.w_CDCODFIS = i_oFrom.w_CDCODFIS
    this.w_CDFPERCI = i_oFrom.w_CDFPERCI
    this.w_CDPEREST = i_oFrom.w_CDPEREST
    this.w_CDCOGNOM = i_oFrom.w_CDCOGNOM
    this.w_CD__NOME = i_oFrom.w_CD__NOME
    this.w_CD_SESSO = i_oFrom.w_CD_SESSO
    this.w_CDDATNAS = i_oFrom.w_CDDATNAS
    this.w_CDCOMNAS = i_oFrom.w_CDCOMNAS
    this.w_CDPRONAS = i_oFrom.w_CDPRONAS
    this.w_CDCATPAR = i_oFrom.w_CDCATPAR
    this.w_CDEVEECC = i_oFrom.w_CDEVEECC
    this.w_CDCOMFIS = i_oFrom.w_CDCOMFIS
    this.w_CDPROFIS = i_oFrom.w_CDPROFIS
    this.w_CDCOMUNE = i_oFrom.w_CDCOMUNE
    this.w_CDCODFES = i_oFrom.w_CDCODFES
    this.w_CDLOCEST = i_oFrom.w_CDLOCEST
    this.w_CDINDIRI = i_oFrom.w_CDINDIRI
    this.w_CDCODEST = i_oFrom.w_CDCODEST
    this.w_CDCAUPRE = i_oFrom.w_CDCAUPRE
    this.w_CDAMMCOR = i_oFrom.w_CDAMMCOR
    this.w_CDSOGRES = i_oFrom.w_CDSOGRES
    this.w_CDSNSOGG = i_oFrom.w_CDSNSOGG
    this.w_CDCODICE = i_oFrom.w_CDCODICE
    this.w_CDIMPONI = i_oFrom.w_CDIMPONI
    this.w_CDRITACC = i_oFrom.w_CDRITACC
    this.w_CDSOGERO = i_oFrom.w_CDSOGERO
    this.w_CDPERCIP = i_oFrom.w_CDPERCIP
    this.w_CDSPERIM = i_oFrom.w_CDSPERIM
    this.w_CDRITRIM = i_oFrom.w_CDRITRIM
    this.w_CDPROCER = i_oFrom.w_CDPROCER
    this.w_CDPROTEC = i_oFrom.w_CDPROTEC
    this.w_CDPRODOC = i_oFrom.w_CDPRODOC
    this.w_CDSERCER = i_oFrom.w_CDSERCER
    this.w_CDPROMOD = i_oFrom.w_CDPROMOD
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CDSERIAL = this.w_CDSERIAL
    i_oTo.w_CDCODFIS = this.w_CDCODFIS
    i_oTo.w_CDFPERCI = this.w_CDFPERCI
    i_oTo.w_CDPEREST = this.w_CDPEREST
    i_oTo.w_CDCOGNOM = this.w_CDCOGNOM
    i_oTo.w_CD__NOME = this.w_CD__NOME
    i_oTo.w_CD_SESSO = this.w_CD_SESSO
    i_oTo.w_CDDATNAS = this.w_CDDATNAS
    i_oTo.w_CDCOMNAS = this.w_CDCOMNAS
    i_oTo.w_CDPRONAS = this.w_CDPRONAS
    i_oTo.w_CDCATPAR = this.w_CDCATPAR
    i_oTo.w_CDEVEECC = this.w_CDEVEECC
    i_oTo.w_CDCOMFIS = this.w_CDCOMFIS
    i_oTo.w_CDPROFIS = this.w_CDPROFIS
    i_oTo.w_CDCOMUNE = this.w_CDCOMUNE
    i_oTo.w_CDCODFES = this.w_CDCODFES
    i_oTo.w_CDLOCEST = this.w_CDLOCEST
    i_oTo.w_CDINDIRI = this.w_CDINDIRI
    i_oTo.w_CDCODEST = this.w_CDCODEST
    i_oTo.w_CDCAUPRE = this.w_CDCAUPRE
    i_oTo.w_CDAMMCOR = this.w_CDAMMCOR
    i_oTo.w_CDSOGRES = this.w_CDSOGRES
    i_oTo.w_CDSNSOGG = this.w_CDSNSOGG
    i_oTo.w_CDCODICE = this.w_CDCODICE
    i_oTo.w_CDIMPONI = this.w_CDIMPONI
    i_oTo.w_CDRITACC = this.w_CDRITACC
    i_oTo.w_CDSOGERO = this.w_CDSOGERO
    i_oTo.w_CDPERCIP = this.w_CDPERCIP
    i_oTo.w_CDSPERIM = this.w_CDSPERIM
    i_oTo.w_CDRITRIM = this.w_CDRITRIM
    i_oTo.w_CDPROCER = this.w_CDPROCER
    i_oTo.w_CDPROTEC = this.w_CDPROTEC
    i_oTo.w_CDPRODOC = this.w_CDPRODOC
    i_oTo.w_CDSERCER = this.w_CDSERCER
    i_oTo.w_CDPROMOD = this.w_CDPROMOD
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsri_mcu as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 596
  Height = 277
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-09"
  HelpContextID=97121897
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  GENDCOMU_IDX = 0
  cFile = "GENDCOMU"
  cKeySelect = "CDSERIAL"
  cKeyWhere  = "CDSERIAL=this.w_CDSERIAL"
  cKeyDetail  = "CDSERIAL=this.w_CDSERIAL"
  cKeyWhereODBC = '"CDSERIAL="+cp_ToStrODBC(this.w_CDSERIAL)';

  cKeyDetailWhereODBC = '"CDSERIAL="+cp_ToStrODBC(this.w_CDSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"GENDCOMU.CDSERIAL="+cp_ToStrODBC(this.w_CDSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'GENDCOMU.CPROWNUM '
  cPrg = "gsri_mcu"
  cComment = "Dettaglio Certificazione Unica"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 1
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CDSERIAL = space(10)
  w_CDCODFIS = space(16)
  w_CDFPERCI = space(15)
  w_CDPEREST = space(1)
  w_CDCOGNOM = space(60)
  w_CD__NOME = space(50)
  w_CD_SESSO = space(1)
  w_CDDATNAS = ctod('  /  /  ')
  w_CDCOMNAS = space(30)
  w_CDPRONAS = space(2)
  w_CDCATPAR = space(2)
  w_CDEVEECC = space(1)
  w_CDCOMFIS = space(30)
  w_CDPROFIS = space(2)
  w_CDCOMUNE = space(4)
  w_CDCODFES = space(25)
  w_CDLOCEST = space(30)
  w_CDINDIRI = space(35)
  w_CDCODEST = space(5)
  w_CDCAUPRE = space(2)
  w_CDAMMCOR = 0
  w_CDSOGRES = space(1)
  w_CDSNSOGG = 0
  w_CDCODICE = space(1)
  w_CDIMPONI = 0
  w_CDRITACC = 0
  w_CDSOGERO = 0
  w_CDPERCIP = 0
  w_CDSPERIM = 0
  w_CDRITRIM = 0
  w_CDPROCER = 0
  w_CDPROTEC = space(17)
  w_CDPRODOC = 0
  w_CDSERCER = space(30)
  w_CDPROMOD = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_mcuPag1","gsri_mcu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCDSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='GENDCOMU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GENDCOMU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GENDCOMU_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsri_mcu'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from GENDCOMU where CDSERIAL=KeySet.CDSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.GENDCOMU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENDCOMU_IDX,2],this.bLoadRecFilter,this.GENDCOMU_IDX,"gsri_mcu")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GENDCOMU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GENDCOMU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GENDCOMU '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CDSERIAL',this.w_CDSERIAL  )
      select * from (i_cTable) GENDCOMU where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CDSERIAL = NVL(CDSERIAL,space(10))
        cp_LoadRecExtFlds(this,'GENDCOMU')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CDCODFIS = NVL(CDCODFIS,space(16))
          .w_CDFPERCI = NVL(CDFPERCI,space(15))
          .w_CDPEREST = NVL(CDPEREST,space(1))
          .w_CDCOGNOM = NVL(CDCOGNOM,space(60))
          .w_CD__NOME = NVL(CD__NOME,space(50))
          .w_CD_SESSO = NVL(CD_SESSO,space(1))
          .w_CDDATNAS = NVL(cp_ToDate(CDDATNAS),ctod("  /  /  "))
          .w_CDCOMNAS = NVL(CDCOMNAS,space(30))
          .w_CDPRONAS = NVL(CDPRONAS,space(2))
          .w_CDCATPAR = NVL(CDCATPAR,space(2))
          .w_CDEVEECC = NVL(CDEVEECC,space(1))
          .w_CDCOMFIS = NVL(CDCOMFIS,space(30))
          .w_CDPROFIS = NVL(CDPROFIS,space(2))
          .w_CDCOMUNE = NVL(CDCOMUNE,space(4))
          .w_CDCODFES = NVL(CDCODFES,space(25))
          .w_CDLOCEST = NVL(CDLOCEST,space(30))
          .w_CDINDIRI = NVL(CDINDIRI,space(35))
          .w_CDCODEST = NVL(CDCODEST,space(5))
          .w_CDCAUPRE = NVL(CDCAUPRE,space(2))
          .w_CDAMMCOR = NVL(CDAMMCOR,0)
          .w_CDSOGRES = NVL(CDSOGRES,space(1))
          .w_CDSNSOGG = NVL(CDSNSOGG,0)
          .w_CDCODICE = NVL(CDCODICE,space(1))
          .w_CDIMPONI = NVL(CDIMPONI,0)
          .w_CDRITACC = NVL(CDRITACC,0)
          .w_CDSOGERO = NVL(CDSOGERO,0)
          .w_CDPERCIP = NVL(CDPERCIP,0)
          .w_CDSPERIM = NVL(CDSPERIM,0)
          .w_CDRITRIM = NVL(CDRITRIM,0)
          .w_CDPROCER = NVL(CDPROCER,0)
          .w_CDPROTEC = NVL(CDPROTEC,space(17))
          .w_CDPRODOC = NVL(CDPRODOC,0)
          .w_CDSERCER = NVL(CDSERCER,space(30))
          .w_CDPROMOD = NVL(CDPROMOD,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CDSERIAL=space(10)
      .w_CDCODFIS=space(16)
      .w_CDFPERCI=space(15)
      .w_CDPEREST=space(1)
      .w_CDCOGNOM=space(60)
      .w_CD__NOME=space(50)
      .w_CD_SESSO=space(1)
      .w_CDDATNAS=ctod("  /  /  ")
      .w_CDCOMNAS=space(30)
      .w_CDPRONAS=space(2)
      .w_CDCATPAR=space(2)
      .w_CDEVEECC=space(1)
      .w_CDCOMFIS=space(30)
      .w_CDPROFIS=space(2)
      .w_CDCOMUNE=space(4)
      .w_CDCODFES=space(25)
      .w_CDLOCEST=space(30)
      .w_CDINDIRI=space(35)
      .w_CDCODEST=space(5)
      .w_CDCAUPRE=space(2)
      .w_CDAMMCOR=0
      .w_CDSOGRES=space(1)
      .w_CDSNSOGG=0
      .w_CDCODICE=space(1)
      .w_CDIMPONI=0
      .w_CDRITACC=0
      .w_CDSOGERO=0
      .w_CDPERCIP=0
      .w_CDSPERIM=0
      .w_CDRITRIM=0
      .w_CDPROCER=0
      .w_CDPROTEC=space(17)
      .w_CDPRODOC=0
      .w_CDSERCER=space(30)
      .w_CDPROMOD=0
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'GENDCOMU')
    this.DoRTCalc(1,35,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCDSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCDSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCDSERIAL_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'GENDCOMU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GENDCOMU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDSERIAL,"CDSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CDCODFIS C(16);
      ,t_CDFPERCI C(15);
      ,t_CDPEREST C(1);
      ,t_CDCOGNOM C(60);
      ,t_CD__NOME C(50);
      ,t_CD_SESSO C(1);
      ,t_CDDATNAS D(8);
      ,t_CDCOMNAS C(30);
      ,t_CDPRONAS C(2);
      ,t_CDCATPAR C(2);
      ,t_CDEVEECC C(1);
      ,t_CDCOMFIS C(30);
      ,t_CDPROFIS C(2);
      ,t_CDCOMUNE C(4);
      ,t_CDCODFES C(25);
      ,t_CDLOCEST C(30);
      ,t_CDINDIRI C(35);
      ,t_CDCODEST C(5);
      ,t_CDCAUPRE C(2);
      ,t_CDAMMCOR N(18,4);
      ,t_CDSOGRES C(1);
      ,t_CDSNSOGG N(18,4);
      ,t_CDCODICE C(1);
      ,t_CDIMPONI N(18,4);
      ,t_CDRITACC N(18,4);
      ,t_CDSOGERO N(18,4);
      ,t_CDPERCIP N(18,4);
      ,t_CDSPERIM N(18,4);
      ,t_CDRITRIM N(18,4);
      ,t_CDPROCER N(5);
      ,t_CDPROTEC C(17);
      ,t_CDPRODOC N(6);
      ,t_CDSERCER C(30);
      ,t_CDPROMOD N(9);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsri_mcubodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODFIS_2_1.controlsource=this.cTrsName+'.t_CDCODFIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDFPERCI_2_2.controlsource=this.cTrsName+'.t_CDFPERCI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDPEREST_2_3.controlsource=this.cTrsName+'.t_CDPEREST'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOGNOM_2_4.controlsource=this.cTrsName+'.t_CDCOGNOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCD__NOME_2_5.controlsource=this.cTrsName+'.t_CD__NOME'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCD_SESSO_2_6.controlsource=this.cTrsName+'.t_CD_SESSO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDDATNAS_2_7.controlsource=this.cTrsName+'.t_CDDATNAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMNAS_2_8.controlsource=this.cTrsName+'.t_CDCOMNAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDPRONAS_2_9.controlsource=this.cTrsName+'.t_CDPRONAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCATPAR_2_10.controlsource=this.cTrsName+'.t_CDCATPAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDEVEECC_2_11.controlsource=this.cTrsName+'.t_CDEVEECC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMFIS_2_12.controlsource=this.cTrsName+'.t_CDCOMFIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROFIS_2_13.controlsource=this.cTrsName+'.t_CDPROFIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMUNE_2_14.controlsource=this.cTrsName+'.t_CDCOMUNE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODFES_2_15.controlsource=this.cTrsName+'.t_CDCODFES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDLOCEST_2_16.controlsource=this.cTrsName+'.t_CDLOCEST'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDINDIRI_2_17.controlsource=this.cTrsName+'.t_CDINDIRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODEST_2_18.controlsource=this.cTrsName+'.t_CDCODEST'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCAUPRE_2_19.controlsource=this.cTrsName+'.t_CDCAUPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDAMMCOR_2_20.controlsource=this.cTrsName+'.t_CDAMMCOR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDSOGRES_2_21.controlsource=this.cTrsName+'.t_CDSOGRES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDSNSOGG_2_22.controlsource=this.cTrsName+'.t_CDSNSOGG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODICE_2_23.controlsource=this.cTrsName+'.t_CDCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDIMPONI_2_24.controlsource=this.cTrsName+'.t_CDIMPONI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDRITACC_2_25.controlsource=this.cTrsName+'.t_CDRITACC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDSOGERO_2_26.controlsource=this.cTrsName+'.t_CDSOGERO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDPERCIP_2_27.controlsource=this.cTrsName+'.t_CDPERCIP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDSPERIM_2_28.controlsource=this.cTrsName+'.t_CDSPERIM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDRITRIM_2_29.controlsource=this.cTrsName+'.t_CDRITRIM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROCER_2_30.controlsource=this.cTrsName+'.t_CDPROCER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROTEC_2_31.controlsource=this.cTrsName+'.t_CDPROTEC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDPRODOC_2_32.controlsource=this.cTrsName+'.t_CDPRODOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDSERCER_2_33.controlsource=this.cTrsName+'.t_CDSERCER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROMOD_2_34.controlsource=this.cTrsName+'.t_CDPROMOD'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODFIS_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GENDCOMU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENDCOMU_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GENDCOMU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENDCOMU_IDX,2])
      *
      * insert into GENDCOMU
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GENDCOMU')
        i_extval=cp_InsertValODBCExtFlds(this,'GENDCOMU')
        i_cFldBody=" "+;
                  "(CDSERIAL,CDCODFIS,CDFPERCI,CDPEREST,CDCOGNOM"+;
                  ",CD__NOME,CD_SESSO,CDDATNAS,CDCOMNAS,CDPRONAS"+;
                  ",CDCATPAR,CDEVEECC,CDCOMFIS,CDPROFIS,CDCOMUNE"+;
                  ",CDCODFES,CDLOCEST,CDINDIRI,CDCODEST,CDCAUPRE"+;
                  ",CDAMMCOR,CDSOGRES,CDSNSOGG,CDCODICE,CDIMPONI"+;
                  ",CDRITACC,CDSOGERO,CDPERCIP,CDSPERIM,CDRITRIM"+;
                  ",CDPROCER,CDPROTEC,CDPRODOC,CDSERCER,CDPROMOD,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CDSERIAL)+","+cp_ToStrODBC(this.w_CDCODFIS)+","+cp_ToStrODBC(this.w_CDFPERCI)+","+cp_ToStrODBC(this.w_CDPEREST)+","+cp_ToStrODBC(this.w_CDCOGNOM)+;
             ","+cp_ToStrODBC(this.w_CD__NOME)+","+cp_ToStrODBC(this.w_CD_SESSO)+","+cp_ToStrODBC(this.w_CDDATNAS)+","+cp_ToStrODBC(this.w_CDCOMNAS)+","+cp_ToStrODBC(this.w_CDPRONAS)+;
             ","+cp_ToStrODBC(this.w_CDCATPAR)+","+cp_ToStrODBC(this.w_CDEVEECC)+","+cp_ToStrODBC(this.w_CDCOMFIS)+","+cp_ToStrODBC(this.w_CDPROFIS)+","+cp_ToStrODBC(this.w_CDCOMUNE)+;
             ","+cp_ToStrODBC(this.w_CDCODFES)+","+cp_ToStrODBC(this.w_CDLOCEST)+","+cp_ToStrODBC(this.w_CDINDIRI)+","+cp_ToStrODBC(this.w_CDCODEST)+","+cp_ToStrODBC(this.w_CDCAUPRE)+;
             ","+cp_ToStrODBC(this.w_CDAMMCOR)+","+cp_ToStrODBC(this.w_CDSOGRES)+","+cp_ToStrODBC(this.w_CDSNSOGG)+","+cp_ToStrODBC(this.w_CDCODICE)+","+cp_ToStrODBC(this.w_CDIMPONI)+;
             ","+cp_ToStrODBC(this.w_CDRITACC)+","+cp_ToStrODBC(this.w_CDSOGERO)+","+cp_ToStrODBC(this.w_CDPERCIP)+","+cp_ToStrODBC(this.w_CDSPERIM)+","+cp_ToStrODBC(this.w_CDRITRIM)+;
             ","+cp_ToStrODBC(this.w_CDPROCER)+","+cp_ToStrODBC(this.w_CDPROTEC)+","+cp_ToStrODBC(this.w_CDPRODOC)+","+cp_ToStrODBC(this.w_CDSERCER)+","+cp_ToStrODBC(this.w_CDPROMOD)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GENDCOMU')
        i_extval=cp_InsertValVFPExtFlds(this,'GENDCOMU')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CDSERIAL',this.w_CDSERIAL)
        INSERT INTO (i_cTable) (;
                   CDSERIAL;
                  ,CDCODFIS;
                  ,CDFPERCI;
                  ,CDPEREST;
                  ,CDCOGNOM;
                  ,CD__NOME;
                  ,CD_SESSO;
                  ,CDDATNAS;
                  ,CDCOMNAS;
                  ,CDPRONAS;
                  ,CDCATPAR;
                  ,CDEVEECC;
                  ,CDCOMFIS;
                  ,CDPROFIS;
                  ,CDCOMUNE;
                  ,CDCODFES;
                  ,CDLOCEST;
                  ,CDINDIRI;
                  ,CDCODEST;
                  ,CDCAUPRE;
                  ,CDAMMCOR;
                  ,CDSOGRES;
                  ,CDSNSOGG;
                  ,CDCODICE;
                  ,CDIMPONI;
                  ,CDRITACC;
                  ,CDSOGERO;
                  ,CDPERCIP;
                  ,CDSPERIM;
                  ,CDRITRIM;
                  ,CDPROCER;
                  ,CDPROTEC;
                  ,CDPRODOC;
                  ,CDSERCER;
                  ,CDPROMOD;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CDSERIAL;
                  ,this.w_CDCODFIS;
                  ,this.w_CDFPERCI;
                  ,this.w_CDPEREST;
                  ,this.w_CDCOGNOM;
                  ,this.w_CD__NOME;
                  ,this.w_CD_SESSO;
                  ,this.w_CDDATNAS;
                  ,this.w_CDCOMNAS;
                  ,this.w_CDPRONAS;
                  ,this.w_CDCATPAR;
                  ,this.w_CDEVEECC;
                  ,this.w_CDCOMFIS;
                  ,this.w_CDPROFIS;
                  ,this.w_CDCOMUNE;
                  ,this.w_CDCODFES;
                  ,this.w_CDLOCEST;
                  ,this.w_CDINDIRI;
                  ,this.w_CDCODEST;
                  ,this.w_CDCAUPRE;
                  ,this.w_CDAMMCOR;
                  ,this.w_CDSOGRES;
                  ,this.w_CDSNSOGG;
                  ,this.w_CDCODICE;
                  ,this.w_CDIMPONI;
                  ,this.w_CDRITACC;
                  ,this.w_CDSOGERO;
                  ,this.w_CDPERCIP;
                  ,this.w_CDSPERIM;
                  ,this.w_CDRITRIM;
                  ,this.w_CDPROCER;
                  ,this.w_CDPROTEC;
                  ,this.w_CDPRODOC;
                  ,this.w_CDSERCER;
                  ,this.w_CDPROMOD;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.GENDCOMU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENDCOMU_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CDFPERCI))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'GENDCOMU')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'GENDCOMU')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CDFPERCI))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update GENDCOMU
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'GENDCOMU')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CDCODFIS="+cp_ToStrODBC(this.w_CDCODFIS)+;
                     ",CDFPERCI="+cp_ToStrODBC(this.w_CDFPERCI)+;
                     ",CDPEREST="+cp_ToStrODBC(this.w_CDPEREST)+;
                     ",CDCOGNOM="+cp_ToStrODBC(this.w_CDCOGNOM)+;
                     ",CD__NOME="+cp_ToStrODBC(this.w_CD__NOME)+;
                     ",CD_SESSO="+cp_ToStrODBC(this.w_CD_SESSO)+;
                     ",CDDATNAS="+cp_ToStrODBC(this.w_CDDATNAS)+;
                     ",CDCOMNAS="+cp_ToStrODBC(this.w_CDCOMNAS)+;
                     ",CDPRONAS="+cp_ToStrODBC(this.w_CDPRONAS)+;
                     ",CDCATPAR="+cp_ToStrODBC(this.w_CDCATPAR)+;
                     ",CDEVEECC="+cp_ToStrODBC(this.w_CDEVEECC)+;
                     ",CDCOMFIS="+cp_ToStrODBC(this.w_CDCOMFIS)+;
                     ",CDPROFIS="+cp_ToStrODBC(this.w_CDPROFIS)+;
                     ",CDCOMUNE="+cp_ToStrODBC(this.w_CDCOMUNE)+;
                     ",CDCODFES="+cp_ToStrODBC(this.w_CDCODFES)+;
                     ",CDLOCEST="+cp_ToStrODBC(this.w_CDLOCEST)+;
                     ",CDINDIRI="+cp_ToStrODBC(this.w_CDINDIRI)+;
                     ",CDCODEST="+cp_ToStrODBC(this.w_CDCODEST)+;
                     ",CDCAUPRE="+cp_ToStrODBC(this.w_CDCAUPRE)+;
                     ",CDAMMCOR="+cp_ToStrODBC(this.w_CDAMMCOR)+;
                     ",CDSOGRES="+cp_ToStrODBC(this.w_CDSOGRES)+;
                     ",CDSNSOGG="+cp_ToStrODBC(this.w_CDSNSOGG)+;
                     ",CDCODICE="+cp_ToStrODBC(this.w_CDCODICE)+;
                     ",CDIMPONI="+cp_ToStrODBC(this.w_CDIMPONI)+;
                     ",CDRITACC="+cp_ToStrODBC(this.w_CDRITACC)+;
                     ",CDSOGERO="+cp_ToStrODBC(this.w_CDSOGERO)+;
                     ",CDPERCIP="+cp_ToStrODBC(this.w_CDPERCIP)+;
                     ",CDSPERIM="+cp_ToStrODBC(this.w_CDSPERIM)+;
                     ",CDRITRIM="+cp_ToStrODBC(this.w_CDRITRIM)+;
                     ",CDPROCER="+cp_ToStrODBC(this.w_CDPROCER)+;
                     ",CDPROTEC="+cp_ToStrODBC(this.w_CDPROTEC)+;
                     ",CDPRODOC="+cp_ToStrODBC(this.w_CDPRODOC)+;
                     ",CDSERCER="+cp_ToStrODBC(this.w_CDSERCER)+;
                     ",CDPROMOD="+cp_ToStrODBC(this.w_CDPROMOD)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'GENDCOMU')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CDCODFIS=this.w_CDCODFIS;
                     ,CDFPERCI=this.w_CDFPERCI;
                     ,CDPEREST=this.w_CDPEREST;
                     ,CDCOGNOM=this.w_CDCOGNOM;
                     ,CD__NOME=this.w_CD__NOME;
                     ,CD_SESSO=this.w_CD_SESSO;
                     ,CDDATNAS=this.w_CDDATNAS;
                     ,CDCOMNAS=this.w_CDCOMNAS;
                     ,CDPRONAS=this.w_CDPRONAS;
                     ,CDCATPAR=this.w_CDCATPAR;
                     ,CDEVEECC=this.w_CDEVEECC;
                     ,CDCOMFIS=this.w_CDCOMFIS;
                     ,CDPROFIS=this.w_CDPROFIS;
                     ,CDCOMUNE=this.w_CDCOMUNE;
                     ,CDCODFES=this.w_CDCODFES;
                     ,CDLOCEST=this.w_CDLOCEST;
                     ,CDINDIRI=this.w_CDINDIRI;
                     ,CDCODEST=this.w_CDCODEST;
                     ,CDCAUPRE=this.w_CDCAUPRE;
                     ,CDAMMCOR=this.w_CDAMMCOR;
                     ,CDSOGRES=this.w_CDSOGRES;
                     ,CDSNSOGG=this.w_CDSNSOGG;
                     ,CDCODICE=this.w_CDCODICE;
                     ,CDIMPONI=this.w_CDIMPONI;
                     ,CDRITACC=this.w_CDRITACC;
                     ,CDSOGERO=this.w_CDSOGERO;
                     ,CDPERCIP=this.w_CDPERCIP;
                     ,CDSPERIM=this.w_CDSPERIM;
                     ,CDRITRIM=this.w_CDRITRIM;
                     ,CDPROCER=this.w_CDPROCER;
                     ,CDPROTEC=this.w_CDPROTEC;
                     ,CDPRODOC=this.w_CDPRODOC;
                     ,CDSERCER=this.w_CDSERCER;
                     ,CDPROMOD=this.w_CDPROMOD;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GENDCOMU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENDCOMU_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CDFPERCI))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete GENDCOMU
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CDFPERCI))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GENDCOMU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENDCOMU_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCDSERIAL_1_1.value==this.w_CDSERIAL)
      this.oPgFrm.Page1.oPag.oCDSERIAL_1_1.value=this.w_CDSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODFIS_2_1.value==this.w_CDCODFIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODFIS_2_1.value=this.w_CDCODFIS
      replace t_CDCODFIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODFIS_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDFPERCI_2_2.value==this.w_CDFPERCI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDFPERCI_2_2.value=this.w_CDFPERCI
      replace t_CDFPERCI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDFPERCI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPEREST_2_3.value==this.w_CDPEREST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPEREST_2_3.value=this.w_CDPEREST
      replace t_CDPEREST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPEREST_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOGNOM_2_4.value==this.w_CDCOGNOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOGNOM_2_4.value=this.w_CDCOGNOM
      replace t_CDCOGNOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOGNOM_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCD__NOME_2_5.value==this.w_CD__NOME)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCD__NOME_2_5.value=this.w_CD__NOME
      replace t_CD__NOME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCD__NOME_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCD_SESSO_2_6.value==this.w_CD_SESSO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCD_SESSO_2_6.value=this.w_CD_SESSO
      replace t_CD_SESSO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCD_SESSO_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDDATNAS_2_7.value==this.w_CDDATNAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDDATNAS_2_7.value=this.w_CDDATNAS
      replace t_CDDATNAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDDATNAS_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMNAS_2_8.value==this.w_CDCOMNAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMNAS_2_8.value=this.w_CDCOMNAS
      replace t_CDCOMNAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMNAS_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPRONAS_2_9.value==this.w_CDPRONAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPRONAS_2_9.value=this.w_CDPRONAS
      replace t_CDPRONAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPRONAS_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCATPAR_2_10.value==this.w_CDCATPAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCATPAR_2_10.value=this.w_CDCATPAR
      replace t_CDCATPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCATPAR_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDEVEECC_2_11.value==this.w_CDEVEECC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDEVEECC_2_11.value=this.w_CDEVEECC
      replace t_CDEVEECC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDEVEECC_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMFIS_2_12.value==this.w_CDCOMFIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMFIS_2_12.value=this.w_CDCOMFIS
      replace t_CDCOMFIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMFIS_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROFIS_2_13.value==this.w_CDPROFIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROFIS_2_13.value=this.w_CDPROFIS
      replace t_CDPROFIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROFIS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMUNE_2_14.value==this.w_CDCOMUNE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMUNE_2_14.value=this.w_CDCOMUNE
      replace t_CDCOMUNE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCOMUNE_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODFES_2_15.value==this.w_CDCODFES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODFES_2_15.value=this.w_CDCODFES
      replace t_CDCODFES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODFES_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDLOCEST_2_16.value==this.w_CDLOCEST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDLOCEST_2_16.value=this.w_CDLOCEST
      replace t_CDLOCEST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDLOCEST_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDINDIRI_2_17.value==this.w_CDINDIRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDINDIRI_2_17.value=this.w_CDINDIRI
      replace t_CDINDIRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDINDIRI_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODEST_2_18.value==this.w_CDCODEST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODEST_2_18.value=this.w_CDCODEST
      replace t_CDCODEST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODEST_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCAUPRE_2_19.value==this.w_CDCAUPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCAUPRE_2_19.value=this.w_CDCAUPRE
      replace t_CDCAUPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCAUPRE_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDAMMCOR_2_20.value==this.w_CDAMMCOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDAMMCOR_2_20.value=this.w_CDAMMCOR
      replace t_CDAMMCOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDAMMCOR_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSOGRES_2_21.value==this.w_CDSOGRES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSOGRES_2_21.value=this.w_CDSOGRES
      replace t_CDSOGRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSOGRES_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSNSOGG_2_22.value==this.w_CDSNSOGG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSNSOGG_2_22.value=this.w_CDSNSOGG
      replace t_CDSNSOGG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSNSOGG_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODICE_2_23.value==this.w_CDCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODICE_2_23.value=this.w_CDCODICE
      replace t_CDCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODICE_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDIMPONI_2_24.value==this.w_CDIMPONI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDIMPONI_2_24.value=this.w_CDIMPONI
      replace t_CDIMPONI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDIMPONI_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRITACC_2_25.value==this.w_CDRITACC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRITACC_2_25.value=this.w_CDRITACC
      replace t_CDRITACC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRITACC_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSOGERO_2_26.value==this.w_CDSOGERO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSOGERO_2_26.value=this.w_CDSOGERO
      replace t_CDSOGERO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSOGERO_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPERCIP_2_27.value==this.w_CDPERCIP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPERCIP_2_27.value=this.w_CDPERCIP
      replace t_CDPERCIP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPERCIP_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSPERIM_2_28.value==this.w_CDSPERIM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSPERIM_2_28.value=this.w_CDSPERIM
      replace t_CDSPERIM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSPERIM_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRITRIM_2_29.value==this.w_CDRITRIM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRITRIM_2_29.value=this.w_CDRITRIM
      replace t_CDRITRIM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRITRIM_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROCER_2_30.value==this.w_CDPROCER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROCER_2_30.value=this.w_CDPROCER
      replace t_CDPROCER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROCER_2_30.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROTEC_2_31.value==this.w_CDPROTEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROTEC_2_31.value=this.w_CDPROTEC
      replace t_CDPROTEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROTEC_2_31.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPRODOC_2_32.value==this.w_CDPRODOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPRODOC_2_32.value=this.w_CDPRODOC
      replace t_CDPRODOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPRODOC_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSERCER_2_33.value==this.w_CDSERCER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSERCER_2_33.value=this.w_CDSERCER
      replace t_CDSERCER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDSERCER_2_33.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROMOD_2_34.value==this.w_CDPROMOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROMOD_2_34.value=this.w_CDPROMOD
      replace t_CDPROMOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDPROMOD_2_34.value
    endif
    cp_SetControlsValueExtFlds(this,'GENDCOMU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CDFPERCI))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CDFPERCI)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CDCODFIS=space(16)
      .w_CDFPERCI=space(15)
      .w_CDPEREST=space(1)
      .w_CDCOGNOM=space(60)
      .w_CD__NOME=space(50)
      .w_CD_SESSO=space(1)
      .w_CDDATNAS=ctod("  /  /  ")
      .w_CDCOMNAS=space(30)
      .w_CDPRONAS=space(2)
      .w_CDCATPAR=space(2)
      .w_CDEVEECC=space(1)
      .w_CDCOMFIS=space(30)
      .w_CDPROFIS=space(2)
      .w_CDCOMUNE=space(4)
      .w_CDCODFES=space(25)
      .w_CDLOCEST=space(30)
      .w_CDINDIRI=space(35)
      .w_CDCODEST=space(5)
      .w_CDCAUPRE=space(2)
      .w_CDAMMCOR=0
      .w_CDSOGRES=space(1)
      .w_CDSNSOGG=0
      .w_CDCODICE=space(1)
      .w_CDIMPONI=0
      .w_CDRITACC=0
      .w_CDSOGERO=0
      .w_CDPERCIP=0
      .w_CDSPERIM=0
      .w_CDRITRIM=0
      .w_CDPROCER=0
      .w_CDPROTEC=space(17)
      .w_CDPRODOC=0
      .w_CDSERCER=space(30)
      .w_CDPROMOD=0
    endwith
    this.DoRTCalc(1,35,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CDCODFIS = t_CDCODFIS
    this.w_CDFPERCI = t_CDFPERCI
    this.w_CDPEREST = t_CDPEREST
    this.w_CDCOGNOM = t_CDCOGNOM
    this.w_CD__NOME = t_CD__NOME
    this.w_CD_SESSO = t_CD_SESSO
    this.w_CDDATNAS = t_CDDATNAS
    this.w_CDCOMNAS = t_CDCOMNAS
    this.w_CDPRONAS = t_CDPRONAS
    this.w_CDCATPAR = t_CDCATPAR
    this.w_CDEVEECC = t_CDEVEECC
    this.w_CDCOMFIS = t_CDCOMFIS
    this.w_CDPROFIS = t_CDPROFIS
    this.w_CDCOMUNE = t_CDCOMUNE
    this.w_CDCODFES = t_CDCODFES
    this.w_CDLOCEST = t_CDLOCEST
    this.w_CDINDIRI = t_CDINDIRI
    this.w_CDCODEST = t_CDCODEST
    this.w_CDCAUPRE = t_CDCAUPRE
    this.w_CDAMMCOR = t_CDAMMCOR
    this.w_CDSOGRES = t_CDSOGRES
    this.w_CDSNSOGG = t_CDSNSOGG
    this.w_CDCODICE = t_CDCODICE
    this.w_CDIMPONI = t_CDIMPONI
    this.w_CDRITACC = t_CDRITACC
    this.w_CDSOGERO = t_CDSOGERO
    this.w_CDPERCIP = t_CDPERCIP
    this.w_CDSPERIM = t_CDSPERIM
    this.w_CDRITRIM = t_CDRITRIM
    this.w_CDPROCER = t_CDPROCER
    this.w_CDPROTEC = t_CDPROTEC
    this.w_CDPRODOC = t_CDPRODOC
    this.w_CDSERCER = t_CDSERCER
    this.w_CDPROMOD = t_CDPROMOD
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CDCODFIS with this.w_CDCODFIS
    replace t_CDFPERCI with this.w_CDFPERCI
    replace t_CDPEREST with this.w_CDPEREST
    replace t_CDCOGNOM with this.w_CDCOGNOM
    replace t_CD__NOME with this.w_CD__NOME
    replace t_CD_SESSO with this.w_CD_SESSO
    replace t_CDDATNAS with this.w_CDDATNAS
    replace t_CDCOMNAS with this.w_CDCOMNAS
    replace t_CDPRONAS with this.w_CDPRONAS
    replace t_CDCATPAR with this.w_CDCATPAR
    replace t_CDEVEECC with this.w_CDEVEECC
    replace t_CDCOMFIS with this.w_CDCOMFIS
    replace t_CDPROFIS with this.w_CDPROFIS
    replace t_CDCOMUNE with this.w_CDCOMUNE
    replace t_CDCODFES with this.w_CDCODFES
    replace t_CDLOCEST with this.w_CDLOCEST
    replace t_CDINDIRI with this.w_CDINDIRI
    replace t_CDCODEST with this.w_CDCODEST
    replace t_CDCAUPRE with this.w_CDCAUPRE
    replace t_CDAMMCOR with this.w_CDAMMCOR
    replace t_CDSOGRES with this.w_CDSOGRES
    replace t_CDSNSOGG with this.w_CDSNSOGG
    replace t_CDCODICE with this.w_CDCODICE
    replace t_CDIMPONI with this.w_CDIMPONI
    replace t_CDRITACC with this.w_CDRITACC
    replace t_CDSOGERO with this.w_CDSOGERO
    replace t_CDPERCIP with this.w_CDPERCIP
    replace t_CDSPERIM with this.w_CDSPERIM
    replace t_CDRITRIM with this.w_CDRITRIM
    replace t_CDPROCER with this.w_CDPROCER
    replace t_CDPROTEC with this.w_CDPROTEC
    replace t_CDPRODOC with this.w_CDPRODOC
    replace t_CDSERCER with this.w_CDSERCER
    replace t_CDPROMOD with this.w_CDPROMOD
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsri_mcuPag1 as StdContainer
  Width  = 592
  height = 277
  stdWidth  = 592
  stdheight = 277
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCDSERIAL_1_1 as StdField with uid="QBZKKABXJY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CDSERIAL", cQueryName = "CDSERIAL",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 144736882,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=120, Top=25, InputMask=replicate('X',10)

  add object oStr_1_2 as StdString with uid="PREQWYJAOJ",Visible=.t., Left=6, Top=29,;
    Alignment=1, Width=113, Height=18,;
    Caption="Seriale file generato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="QEDTZBMLNM",Visible=.t., Left=19, Top=51,;
    Alignment=0, Width=162, Height=18,;
    Caption="Codice fiscale del percipiente"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="QAZOUYDLZZ",Visible=.t., Left=149, Top=51,;
    Alignment=0, Width=62, Height=18,;
    Caption="Percipiente"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="OZZAIFBYBV",Visible=.t., Left=272, Top=51,;
    Alignment=0, Width=100, Height=18,;
    Caption="Percipiente estero"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VPLWIIAEQQ",Visible=.t., Left=19, Top=61,;
    Alignment=0, Width=153, Height=18,;
    Caption="Cognome o ragione sociale"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=9,top=73,;
    width=554+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=10,top=74,width=553+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsri_mcuBodyRow as CPBodyRowCnt
  Width=544
  Height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCDCODFIS_2_1 as StdTrsField with uid="ULKQLLLAQO",rtseq=2,rtrep=.t.,;
    cFormVar="w_CDCODFIS",value=space(16),;
    HelpContextID = 188120455,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=-1, Top=0, InputMask=replicate('X',16)

  add object oCDFPERCI_2_2 as StdTrsField with uid="UIWHLGGAUC",rtseq=3,rtrep=.t.,;
    cFormVar="w_CDFPERCI",value=space(15),;
    HelpContextID = 14332527,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=129, Top=0, InputMask=replicate('X',15)

  add object oCDPEREST_2_3 as StdTrsField with uid="EVGCJJWPPK",rtseq=4,rtrep=.t.,;
    cFormVar="w_CDPEREST",value=space(1),;
    HelpContextID = 77615738,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=252, Top=0, InputMask=replicate('X',1)

  add object oCDCOGNOM_2_4 as StdTrsField with uid="ZAKQHRHUZU",rtseq=5,rtrep=.t.,;
    cFormVar="w_CDCOGNOM",value=space(60),;
    HelpContextID = 50757005,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=433, Left=-1, Top=19, InputMask=replicate('X',60)

  add object oCD__NOME_2_5 as StdTrsField with uid="JIGNULYKRH",rtseq=6,rtrep=.t.,;
    cFormVar="w_CD__NOME",value=space(50),;
    HelpContextID = 25476501,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=363, Left=-1, Top=38, InputMask=replicate('X',50)

  add object oCD_SESSO_2_6 as StdTrsField with uid="KFYVDTTRVQ",rtseq=7,rtrep=.t.,;
    cFormVar="w_CD_SESSO",value=space(1),;
    HelpContextID = 31408757,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=367, Top=38, InputMask=replicate('X',1)

  add object oCDDATNAS_2_7 as StdTrsField with uid="WRQMFWIOWZ",rtseq=8,rtrep=.t.,;
    cFormVar="w_CDDATNAS",value=ctod("  /  /  "),;
    HelpContextID = 230396537,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=392, Top=38

  add object oCDCOMNAS_2_8 as StdTrsField with uid="ECLEGIPVUE",rtseq=9,rtrep=.t.,;
    cFormVar="w_CDCOMNAS",value=space(30),;
    HelpContextID = 223969913,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=-1, Top=57, InputMask=replicate('X',30)

  add object oCDPRONAS_2_9 as StdTrsField with uid="CCMQFPPVFI",rtseq=10,rtrep=.t.,;
    cFormVar="w_CDPRONAS",value=space(2),;
    HelpContextID = 226316921,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=227, Top=57, InputMask=replicate('X',2)

  add object oCDCATPAR_2_10 as StdTrsField with uid="GCXZHLWPVA",rtseq=11,rtrep=.t.,;
    cFormVar="w_CDCATPAR",value=space(2),;
    HelpContextID = 263946872,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=259, Top=57, InputMask=replicate('X',2)

  add object oCDEVEECC_2_11 as StdTrsField with uid="DOVNZVPXTD",rtseq=12,rtrep=.t.,;
    cFormVar="w_CDEVEECC",value=space(1),;
    HelpContextID = 65053289,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=291, Top=57, InputMask=replicate('X',1)

  add object oCDCOMFIS_2_12 as StdTrsField with uid="WUOCJOHGVQ",rtseq=13,rtrep=.t.,;
    cFormVar="w_CDCOMFIS",value=space(30),;
    HelpContextID = 178683271,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=316, Top=57, InputMask=replicate('X',30)

  add object oCDPROFIS_2_13 as StdTrsField with uid="VFSFOKUUHI",rtseq=14,rtrep=.t.,;
    cFormVar="w_CDPROFIS",value=space(2),;
    HelpContextID = 176336263,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=-1, Top=76, InputMask=replicate('X',2)

  add object oCDCOMUNE_2_14 as StdTrsField with uid="OSKKVTKZHA",rtseq=15,rtrep=.t.,;
    cFormVar="w_CDCOMUNE",value=space(4),;
    HelpContextID = 195460501,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=31, Top=76, InputMask=replicate('X',4)

  add object oCDCODFES_2_15 as StdTrsField with uid="YJIZJEIBAW",rtseq=16,rtrep=.t.,;
    cFormVar="w_CDCODFES",value=space(25),;
    HelpContextID = 80315001,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=188, Left=77, Top=76, InputMask=replicate('X',25)

  add object oCDLOCEST_2_16 as StdTrsField with uid="WQIFLUVULY",rtseq=17,rtrep=.t.,;
    cFormVar="w_CDLOCEST",value=space(30),;
    HelpContextID = 62526074,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=270, Top=76, InputMask=replicate('X',30)

  add object oCDINDIRI_2_17 as StdTrsField with uid="ZYBNDCPQEH",rtseq=18,rtrep=.t.,;
    cFormVar="w_CDINDIRI",value=space(35),;
    HelpContextID = 130605679,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=258, Left=-1, Top=95, InputMask=replicate('X',35)

  add object oCDCODEST_2_18 as StdTrsField with uid="DKQEKLWWBW",rtseq=19,rtrep=.t.,;
    cFormVar="w_CDCODEST",value=space(5),;
    HelpContextID = 63537786,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=262, Top=95, InputMask=replicate('X',5)

  add object oCDCAUPRE_2_19 as StdTrsField with uid="ABKWUASPFK",rtseq=20,rtrep=.t.,;
    cFormVar="w_CDCAUPRE",value=space(2),;
    HelpContextID = 264995435,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=315, Top=95, InputMask=replicate('X',2)

  add object oCDAMMCOR_2_20 as StdTrsField with uid="QBTEEHCXQG",rtseq=21,rtrep=.t.,;
    cFormVar="w_CDAMMCOR",value=0,;
    HelpContextID = 229154184,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=347, Top=95, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oCDSOGRES_2_21 as StdTrsField with uid="VGBNAZIOZB",rtseq=22,rtrep=.t.,;
    cFormVar="w_CDSOGRES",value=space(1),;
    HelpContextID = 16417401,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=491, Top=95, InputMask=replicate('X',1)

  add object oCDSNSOGG_2_22 as StdTrsField with uid="HPGWQHTPSJ",rtseq=23,rtrep=.t.,;
    cFormVar="w_CDSNSOGG",value=0,;
    HelpContextID = 247038573,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=-1, Top=114, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oCDCODICE_2_23 as StdTrsField with uid="AWNFZJUVGE",rtseq=24,rtrep=.t.,;
    cFormVar="w_CDCODICE",value=space(1),;
    HelpContextID = 130646635,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=143, Top=114, InputMask=replicate('X',1)

  add object oCDIMPONI_2_24 as StdTrsField with uid="KCIFIKMICI",rtseq=25,rtrep=.t.,;
    cFormVar="w_CDIMPONI",value=0,;
    HelpContextID = 24649105,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=168, Top=114, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oCDRITACC_2_25 as StdTrsField with uid="WLRHXPNRKC",rtseq=26,rtrep=.t.,;
    cFormVar="w_CDRITACC",value=0,;
    HelpContextID = 12874345,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=312, Top=114, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oCDSOGERO_2_26 as StdTrsField with uid="WYEMVNVZIQ",rtseq=27,rtrep=.t.,;
    cFormVar="w_CDSOGERO",value=0,;
    HelpContextID = 66749045,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=143, Top=133, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oCDPERCIP_2_27 as StdTrsField with uid="MJHCPPGRXF",rtseq=28,rtrep=.t.,;
    cFormVar="w_CDPERCIP",value=0,;
    HelpContextID = 224374154,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=287, Top=133, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oCDSPERIM_2_28 as StdTrsField with uid="XYOSJQNGET",rtseq=29,rtrep=.t.,;
    cFormVar="w_CDSPERIM",value=0,;
    HelpContextID = 254049677,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=-1, Top=152, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oCDRITRIM_2_29 as StdTrsField with uid="REEIRMKHUI",rtseq=30,rtrep=.t.,;
    cFormVar="w_CDRITRIM",value=0,;
    HelpContextID = 238783885,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=143, Top=152, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oCDPROCER_2_30 as StdTrsField with uid="SNEHTNYTHU",rtseq=31,rtrep=.t.,;
    cFormVar="w_CDPROCER",value=0,;
    HelpContextID = 41767544,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=287, Top=152

  add object oCDPROTEC_2_31 as StdTrsField with uid="LURYQJSYUO",rtseq=32,rtrep=.t.,;
    cFormVar="w_CDPROTEC",value=space(17),;
    HelpContextID = 58544745,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=338, Top=152, InputMask=replicate('X',17)

  add object oCDPRODOC_2_32 as StdTrsField with uid="CXGUUSUPXB",rtseq=33,rtrep=.t.,;
    cFormVar="w_CDPRODOC",value=0,;
    HelpContextID = 209890711,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=473, Top=151

  add object oCDSERCER_2_33 as StdTrsField with uid="LDRSENPABQ",rtseq=34,rtrep=.t.,;
    cFormVar="w_CDSERCER",value=space(30),;
    HelpContextID = 44073592,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=278, Top=0, InputMask=replicate('X',30)

  add object oCDPROMOD_2_34 as StdTrsField with uid="TMRGGZSFKB",rtseq=35,rtrep=.t.,;
    cFormVar="w_CDPROMOD",value=0,;
    HelpContextID = 58895766,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=-2, Top=172
  add object oLast as LastKeyMover
  * ---
  func oCDCODFIS_2_1.When()
    return(.t.)
  proc oCDCODFIS_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCDCODFIS_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=0
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_mcu','GENDCOMU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CDSERIAL=GENDCOMU.CDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
