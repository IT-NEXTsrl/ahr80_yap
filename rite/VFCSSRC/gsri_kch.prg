* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kch                                                        *
*              Altri dati fiscali                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-01-14                                                      *
* Last revis.: 2016-02-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_kch",oParentObject))

* --- Class definition
define class tgsri_kch as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 692
  Height = 205
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-02-04"
  HelpContextID=99219049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri_kch"
  cComment = "Altri dati fiscali"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CHTOT010 = 0
  o_CHTOT010 = 0
  w_CHTOT011 = 0
  o_CHTOT011 = 0
  w_CHTOT012 = 0
  o_CHTOT012 = 0
  w_CHTOT013 = 0
  o_CHTOT013 = 0
  w_CHTOT014 = 0
  o_CHTOT014 = 0
  w_CHTOT015 = 0
  o_CHTOT015 = 0
  w_CHTOT016 = 0
  o_CHTOT016 = 0
  w_CHTOT017 = 0
  o_CHTOT017 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kchPag1","gsri_kch",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCHTOT010_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CHTOT010=0
      .w_CHTOT011=0
      .w_CHTOT012=0
      .w_CHTOT013=0
      .w_CHTOT014=0
      .w_CHTOT015=0
      .w_CHTOT016=0
      .w_CHTOT017=0
      .w_CHTOT010=oParentObject.w_CHTOT010
      .w_CHTOT011=oParentObject.w_CHTOT011
      .w_CHTOT012=oParentObject.w_CHTOT012
      .w_CHTOT013=oParentObject.w_CHTOT013
      .w_CHTOT014=oParentObject.w_CHTOT014
      .w_CHTOT015=oParentObject.w_CHTOT015
      .w_CHTOT016=oParentObject.w_CHTOT016
      .w_CHTOT017=oParentObject.w_CHTOT017
    endwith
    this.DoRTCalc(1,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCHTOT010_1_2.enabled = i_bVal
      .Page1.oPag.oCHTOT011_1_10.enabled = i_bVal
      .Page1.oPag.oCHTOT012_1_11.enabled = i_bVal
      .Page1.oPag.oCHTOT013_1_12.enabled = i_bVal
      .Page1.oPag.oCHTOT014_1_13.enabled = i_bVal
      .Page1.oPag.oCHTOT015_1_14.enabled = i_bVal
      .Page1.oPag.oCHTOT016_1_15.enabled = i_bVal
      .Page1.oPag.oCHTOT017_1_16.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CHTOT010=.w_CHTOT010
      .oParentObject.w_CHTOT011=.w_CHTOT011
      .oParentObject.w_CHTOT012=.w_CHTOT012
      .oParentObject.w_CHTOT013=.w_CHTOT013
      .oParentObject.w_CHTOT014=.w_CHTOT014
      .oParentObject.w_CHTOT015=.w_CHTOT015
      .oParentObject.w_CHTOT016=.w_CHTOT016
      .oParentObject.w_CHTOT017=.w_CHTOT017
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_Chtot010<>.w_Chtot010
            .w_CHTOT010 = iif(.w_Chtot010<0,0,.w_Chtot010)
        endif
        if .o_Chtot011<>.w_Chtot011
            .w_CHTOT011 = iif(.w_Chtot011<0,0,.w_Chtot011)
        endif
        if .o_Chtot012<>.w_Chtot012
            .w_CHTOT012 = iif(.w_Chtot012<0,0,.w_Chtot012)
        endif
        if .o_Chtot013<>.w_Chtot013
            .w_CHTOT013 = iif(.w_Chtot013<0,0,.w_Chtot013)
        endif
        if .o_Chtot014<>.w_Chtot014
            .w_CHTOT014 = iif(.w_Chtot014<0,0,.w_Chtot014)
        endif
        if .o_Chtot015<>.w_Chtot015
            .w_CHTOT015 = iif(.w_Chtot015<0,0,.w_Chtot015)
        endif
        if .o_Chtot016<>.w_Chtot016
            .w_CHTOT016 = iif(.w_Chtot016<0,0,.w_Chtot016)
        endif
        if .o_Chtot017<>.w_Chtot017
            .w_CHTOT017 = iif(.w_Chtot017<0,0,.w_Chtot017)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCHTOT010_1_2.value==this.w_CHTOT010)
      this.oPgFrm.Page1.oPag.oCHTOT010_1_2.value=this.w_CHTOT010
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTOT011_1_10.value==this.w_CHTOT011)
      this.oPgFrm.Page1.oPag.oCHTOT011_1_10.value=this.w_CHTOT011
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTOT012_1_11.value==this.w_CHTOT012)
      this.oPgFrm.Page1.oPag.oCHTOT012_1_11.value=this.w_CHTOT012
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTOT013_1_12.value==this.w_CHTOT013)
      this.oPgFrm.Page1.oPag.oCHTOT013_1_12.value=this.w_CHTOT013
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTOT014_1_13.value==this.w_CHTOT014)
      this.oPgFrm.Page1.oPag.oCHTOT014_1_13.value=this.w_CHTOT014
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTOT015_1_14.value==this.w_CHTOT015)
      this.oPgFrm.Page1.oPag.oCHTOT015_1_14.value=this.w_CHTOT015
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTOT016_1_15.value==this.w_CHTOT016)
      this.oPgFrm.Page1.oPag.oCHTOT016_1_15.value=this.w_CHTOT016
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTOT017_1_16.value==this.w_CHTOT017)
      this.oPgFrm.Page1.oPag.oCHTOT017_1_16.value=this.w_CHTOT017
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_Chtot010>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHTOT010_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot011>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHTOT011_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot012>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHTOT012_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot013>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHTOT013_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot014>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHTOT014_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot015>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHTOT015_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot016>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHTOT016_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot017>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHTOT017_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CHTOT010 = this.w_CHTOT010
    this.o_CHTOT011 = this.w_CHTOT011
    this.o_CHTOT012 = this.w_CHTOT012
    this.o_CHTOT013 = this.w_CHTOT013
    this.o_CHTOT014 = this.w_CHTOT014
    this.o_CHTOT015 = this.w_CHTOT015
    this.o_CHTOT016 = this.w_CHTOT016
    this.o_CHTOT017 = this.w_CHTOT017
    return

enddefine

* --- Define pages as container
define class tgsri_kchPag1 as StdContainer
  Width  = 688
  height = 205
  stdWidth  = 688
  stdheight = 205
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCHTOT010_1_2 as StdField with uid="HFGMVSLSCY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CHTOT010", cQueryName = "CHTOT010",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ritenute a titolo di imposta",;
    HelpContextID = 262837846,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=8, Top=32, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT010_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot010>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT011_1_10 as StdField with uid="GLWFDRUTNI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CHTOT011", cQueryName = "CHTOT011",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ritenute sospese",;
    HelpContextID = 262837847,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=8, Top=78, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT011_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot011>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT012_1_11 as StdField with uid="BZQCLDYHLN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CHTOT012", cQueryName = "CHTOT012",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Addizionale regionale a titolo di acconto",;
    HelpContextID = 262837848,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=8, Top=124, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT012_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot012>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT013_1_12 as StdField with uid="THVJFMDMXE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CHTOT013", cQueryName = "CHTOT013",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Addizionale regionale a titolo di imposta",;
    HelpContextID = 262837849,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=8, Top=175, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT013_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot013>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT014_1_13 as StdField with uid="ZEHMYNJROZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CHTOT014", cQueryName = "CHTOT014",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Addizionale regionale sospesa",;
    HelpContextID = 262837850,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=319, Top=32, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT014_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot014>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT015_1_14 as StdField with uid="DUMUCBKWGB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CHTOT015", cQueryName = "CHTOT015",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Addizionale comunale a titolo di acconto",;
    HelpContextID = 262837851,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=319, Top=78, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT015_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot015>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT016_1_15 as StdField with uid="PZLBMGCASO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CHTOT016", cQueryName = "CHTOT016",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Addizionale comunale a titolo di imposta",;
    HelpContextID = 262837852,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=319, Top=124, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT016_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot016>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT017_1_16 as StdField with uid="TXOMOUTTIC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CHTOT017", cQueryName = "CHTOT017",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Addizionale comunale sospesa",;
    HelpContextID = 262837853,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=319, Top=175, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT017_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot017>=0)
    endwith
    return bRes
  endfunc

  add object oStr_1_1 as StdString with uid="VKSNIUYZAB",Visible=.t., Left=9, Top=13,;
    Alignment=0, Width=182, Height=18,;
    Caption="Ritenute a titolo di imposta"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="GGQJYSQPPQ",Visible=.t., Left=9, Top=59,;
    Alignment=0, Width=182, Height=18,;
    Caption="Ritenute sospese"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="WNXMOYNGFP",Visible=.t., Left=9, Top=103,;
    Alignment=0, Width=216, Height=18,;
    Caption="Addizionale regionale a titolo di acconto"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="YZCQXGCNFH",Visible=.t., Left=9, Top=151,;
    Alignment=0, Width=218, Height=18,;
    Caption="Addizionale regionale a titolo di imposta"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="XQZOQKFEXV",Visible=.t., Left=319, Top=13,;
    Alignment=0, Width=218, Height=18,;
    Caption="Addizionale regionale sospesa"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="IGLSNMRDNE",Visible=.t., Left=319, Top=59,;
    Alignment=0, Width=219, Height=18,;
    Caption="Addizionale comunale a titolo di acconto"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="HPXAOSWWSB",Visible=.t., Left=319, Top=104,;
    Alignment=0, Width=221, Height=18,;
    Caption="Addizionale comunale a titolo di imposta"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XVNTLGWECW",Visible=.t., Left=319, Top=151,;
    Alignment=0, Width=221, Height=18,;
    Caption="Addizionale comunale sospesa"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kch','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
