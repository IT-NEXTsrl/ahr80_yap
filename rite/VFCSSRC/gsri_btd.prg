* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_btd                                                        *
*              Dist.vers.ritenuta su floppy                                    *
*                                                                              *
*      Author: TAM Software & Codelab                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_616]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-02                                                      *
* Last revis.: 2007-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_btd",oParentObject)
return(i_retval)

define class tgsri_btd as StdBatch
  * --- Local variables
  w_TIPRIT = space(1)
  w_Confermato = .f.
  w_DATINI = ctod("  /  /  ")
  w_MESE = 0
  w_Record = space(120)
  w_DATFIN = ctod("  /  /  ")
  w_NUMVER = 0
  w_HANDLE = 0
  w_CONTINUO = .f.
  w_TOTVER = 0
  w_Separa = space(2)
  w_MRTOTIM2 = 0
  w_MESE1 = space(2)
  w_CODAZI = space(5)
  w_VPIMPVER = 0
  w_NUMVER1 = space(4)
  w_ANNO1 = space(4)
  w_MRIMPRI2 = 0
  w_TOTVER1 = space(10)
  w_RAGAZI = space(60)
  w_MESS = space(100)
  w_TOTVER1 = space(10)
  w_INDAZI = space(40)
  w_NUMCOLL = 0
  w_REPLI = 0
  w_CAPAZI = space(5)
  w_NUMCOLL1 = space(6)
  w_RIEMPIE = space(10)
  w_LOCAZI = space(30)
  w_TOTIMPVE = 0
  w_ECCEDE = space(15)
  w_PROAZI = space(2)
  w_TOTCONDO = 0
  w_SALCRE = space(15)
  w_SEDINP1 = space(4)
  w_Ragsoc1 = space(50)
  w_SALDO = 0
  w_TELEFO1 = space(15)
  w_Codpart1 = space(16)
  w_SALDEB = space(15)
  w_E_MAIL = space(30)
  w_Numcomm = 0
  w_CODFOR = space(15)
  w_TELEFAX = space(15)
  w_Numcoll1 = space(6)
  w_CONTA1 = 0
  w_GISTAT = space(5)
  w_Lunghezza = 0
  w_FPROGR = space(6)
  w_GISTAT1 = space(5)
  w_Valore = space(15)
  w_FCODFIS = space(16)
  w_CODNAZ = space(3)
  w_Valore1 = space(15)
  w_FCOGNOM = space(20)
  w_CONTA = 0
  w_Cognom1 = space(40)
  w_FNOME = space(20)
  w_VALUTA = space(3)
  w_Telefo1 = space(15)
  w_DATNAS = ctod("  /  /  ")
  w_TIPVAL = space(1)
  w_NumrecGR = space(6)
  w_FGIORNO = 0
  w_DECTOT = 0
  w_Email1 = space(30)
  w_Denuncia1 = space(1)
  w_FMESE = 0
  w_FANNO = 0
  w_Cofazi = space(16)
  w_CONFOREC = 0
  w_FGIORNO1 = space(2)
  w_Codiso = space(3)
  w_MESEGC = 0
  w_FMESE1 = space(2)
  w_FLOCNAS = space(30)
  w_MESEGC1 = space(2)
  w_FANNO1 = space(4)
  w_FNAZION = space(3)
  w_IMPONIGC = space(10)
  w_SCRITTO = .f.
  w_FPRONAS = space(2)
  w_CONTRGC = space(10)
  w_TOTIMPTC = space(15)
  w_FINDIRI = space(40)
  w_PERCENT = 0
  w_TOTCONTC = space(15)
  w_FCAP = space(5)
  w_PERCENT1 = space(4)
  w_SEDINPS = 0
  w_FLOCALI = space(30)
  w_DATFIGC = space(8)
  w_NumrecGC = space(6)
  w_FPROVIN = space(2)
  w_DATINGC = space(8)
  w_STAMPO = .f.
  w_FCODISO = space(3)
  w_DATFEB = ctod("  /  /  ")
  w_TELEFO2 = space(15)
  w_FCODATT = space(2)
  w_DAYDATFE = 0
  w_DDCODICE = space(5)
  w_DDCODNAZ = space(3)
  w_DDINDIRI = space(35)
  w_DD___CAP = space(8)
  w_DDLOCALI = space(30)
  w_DDPROVIN = space(2)
  w_NUMRECGC = 0
  w_ARRIMPGC = 0
  w_IMPONITC = 0
  w_TOTAGC = 0
  w_ARRCONGC = 0
  w_CONTRITC = 0
  w_TOTAGC1 = space(6)
  w_CODALASS = space(3)
  w_ARRIMVER = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  TAB_RITE_idx=0
  VALUTE_idx=0
  NAZIONI_idx=0
  DES_DIVE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato per la compilazione del GLA
    * --- I records (obbligatori) devono essere registrati in questa sequenza:
    * --- Tipo "GS":dati utente
    * --- Tipo "GM":dati del committente
    * --- Tipo "GR":distinta versamenti
    * --- Tipo "TR":totali versamenti
    * --- Tipo "GL":dati del collaboratore
    * --- Tipo "GC":dati contributivi.Pu� comparire pi� volte per lo stesso collaboratore
    * --- Tipo "TC":Totali contributivi collaboratore
    * --- Tipo "TT":Record chiusura committente
    this.w_STAMPO = .F.
    * --- Dichiarazione variabili
    this.Pag8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Assegnamento variabili
    this.w_Confermato = .T.
    this.w_Record = space(120)
    this.w_HANDLE = 0
    this.w_Separa = chr(13)+chr(10)
    this.w_CODAZI = i_Codazi
    this.w_ANNO1 = Alltrim(Str(this.oParentObject.w_Anno,4,0))
    this.w_RAGAZI = g_Ragazi
    this.w_INDAZI = g_Indazi
    this.w_CAPAZI = g_Capazi
    this.w_LOCAZI = g_Locazi
    this.w_PROAZI = g_Proazi
    this.w_TELEFO2 = g_Telefo
    this.w_E_MAIL = this.oParentObject.w_AZ_EMAIL
    this.w_TELEFAX = g_Telfax
    this.w_GISTAT = Alltrim(Str(this.oParentObject.w_Istat,5,0))
    this.w_GISTAT1 = Alltrim(Str(this.oParentObject.w_Istat1,5,0))
    this.w_CODNAZ = g_Codnaz
    this.w_CONTA = 0
    this.w_TIPRIT = "A"
    * --- Seleziono la valuta con cui effettuare la dichiarazione
    this.w_VALUTA = IIF(this.oParentObject.w_ANNO>2000,g_PERVAL,this.oParentObject.w_VALNAZ)
    * --- Leggo il numero di decimali
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(G_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = G_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Attribuisco al campo Tipval il valore 1 se la valuta � Euro, 0 se Lire.
    this.w_TIPVAL = IIF(this.oParentObject.w_Anno>2000,"1",IIF(this.oParentObject.w_Valnaz=g_PERVAL,"1","0"))
    * --- Creazione File Distinta Versamento Ritenuta
    ah_Msg("Creazione file dati versamento ritenute...",.T.)
    * --- Richiesta azzeramento file esistente
    if file( this.oParentObject.w_NomeFile )
      if NOT ah_YesNo("File esistente! Ricreo?")
        this.w_Confermato = .F.
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Creazione file ascii
    if this.w_Confermato
      this.w_Handle = fcreate(this.oParentObject.w_NomeFile, 0)
      if this.w_Handle = -1
        ah_ErrorMsg("Non � possibile creare il file dati versamento ritenute","!","")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Verifico se la dichiarazione � dell'anno 2000 oppure successiva
    if this.oParentObject.w_ANNO>2000 OR this.oParentObject.w_INCLTRIM="S" OR this.oParentObject.w_DENUNCIA="S"
      * --- Preparo due campi che contengano la data di inizio e fine anno.
      this.w_DATINI = cp_CharToDate("01-01-"+STR((this.oParentObject.w_ANNO),4,0))
      this.w_DATFIN = cp_CharToDate("31-12-"+STR((this.oParentObject.w_ANNO),4,0))
      * --- Valuta sempre in EURO
    else
      * --- Preparo due campi che contengano la data di inizio e fine anno.
      this.w_DATINI = cp_CharToDate("01-04-"+STR((this.oParentObject.w_ANNO),4,0))
      this.w_DATFIN = cp_CharToDate("31-12-"+STR((this.oParentObject.w_ANNO),4,0))
    endif
    * --- I campi w_Datini e w_Datfin vengono utilizzati come filtro nella query Gsri_Ktd
    * --- Vengono presi in considerazione solo i movimenti che sono inseriti all'interno di una distinta
    * --- Questa distinta deve avere la data versamento e questa data deve essere compresa all'interno di w_Datini e w_Datfin .
    vq_exec("..\RITE\EXE\QUERY\GSRI_KTD.VQR",this,"RITE")
    * --- Verifico se sono presenti dei record per effettuare l'elaborazione
    if RECCOUNT("RITE")>0
      * --- Devo convertire tutti i record nella valuta dell'anno in cui effettuo la compilazione.
      * --- Per i campi Mrtotim2 e Mrimpri2 devo considerare il campo Mrcodval mentre per il campo Vpimver
      * --- devo considerare il campo Vpvalver.
      WRCURSOR("RITE")
      Scan
      if this.w_Valuta<>Rite.Mrcodval
        REPLACE RITE.MRTOTIM2 WITH cp_ROUND(VALCAM(NVL(RITE.MRTOTIM2,0), RITE.MRCODVAL, this.w_VALUTA, GETVALUT(g_PERVAL, "VADATEUR"), 0), w_DECIMI)
        REPLACE RITE.MRIMPRI2 WITH cp_ROUND(VALCAM(NVL(RITE.MRIMPRI2,0), RITE.MRCODVAL, this.w_VALUTA, GETVALUT(g_PERVAL, "VADATEUR"), 0), w_DECIMI)
      endif
      Endscan
      * --- Leggo i dati di testata.Seleziono solo il codice fiscale dato che gli altri dati
      * --- sono dichiarati dentro il batch Ambiente.
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZCOFAZI"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZCOFAZI;
          from (i_cTable) where;
              AZCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se non � presente il codice fiscale dell'azienda non effettuo nessuna elaborazione.
      if EMPTY(this.w_COFAZI)
        ah_ErrorMsg("Codice fiscale azienda non inserito. Impossibile continuare","!","")
        this.w_Handle = fclose( this.w_Handle )
        ERASE (this.oParentObject.w_NomeFile)
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      * --- Leggo nella tabella ritenute il codice sede I.N.P.S.
      * --- Read from TAB_RITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TAB_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2],.t.,this.TAB_RITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRCODINP"+;
          " from "+i_cTable+" TAB_RITE where ";
              +"TRCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and TRTIPRIT = "+cp_ToStrODBC(this.w_TIPRIT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRCODINP;
          from (i_cTable) where;
              TRCODAZI = this.w_CODAZI;
              and TRTIPRIT = this.w_TIPRIT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEDINPS = NVL(cp_ToDate(_read_.TRCODINP),cp_NullValue(_read_.TRCODINP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo il codice ISO della nazione
      * --- Read from NAZIONI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.NAZIONI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NACODISO"+;
          " from "+i_cTable+" NAZIONI where ";
              +"NACODNAZ = "+cp_ToStrODBC(this.w_CODNAZ);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NACODISO;
          from (i_cTable) where;
              NACODNAZ = this.w_CODNAZ;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Preparo il cursore per i record da inserire nella tipologia "GS"
      * --- Elaboro il cursore per avere i valori da inserire nei campi che devono contenere
      * --- il numero collaboratori presenti nel supporto, l'importo totale versato, il totale contributo dovuto
      * --- Verifico quanti sono i collaboratori presenti nel supporto
      Select distinct Rite.Mrcodcon as NumColl from Rite into cursor Rite3
      Select Rite3
      this.w_NUMCOLL = Reccount("Rite3")
      this.w_NUMCOLL1 = Alltrim(Str(this.w_Numcoll,6,0))
      * --- Preparo il cursore per poter inserire i valori nel record di tipo 'GR'.
      * --- Effettuo un ragruppamento per mese e fornitore.
      * --- Possono essere presenti pi� movimenti per lo stesso fornitore nella stessa distinta
      * --- in questo caso devo sommare e poi successivamente arrotondare
      Select Month(Mrdatreg) as Mese, Sum(cp_Round(Mrimpri2,0) * Nvl(Mrperri2,0)/100) as Totimpver,Mrcodcon as Mrcodcon;
      From Rite into cursor Rite4 group by Mese,Mrcodcon
      * --- Totale dei campi "importo totale versato".
      this.w_TOTIMPVE = 0
      WRCURSOR("RITE4")
      Select Rite4
      Go Top
      Scan
      if this.w_Valuta=g_Codlir
        * --- Arrotondo alle 1000 Lire.
        this.w_ARRIMVER = cp_Round((Rite4.Totimpver/1000),0)*1000
        Replace Rite4.Totimpver With this.w_Arrimver
        this.w_TOTIMPVE = this.w_Totimpve+this.w_Arrimver
      else
        * --- Arrotondo all'unit� di Euro.
        this.w_ARRIMVER = cp_Round(Rite4.Totimpver,0)
        Replace Rite4.Totimpver with this.w_Arrimver
        this.w_TOTIMPVE = this.w_Totimpve+this.w_Arrimver
      endif
      Endscan
      * --- Dopo aver arrotondato devo sommare i versamenti effettuati nello stesso mese
      Select Mese as Mese, Sum(Totimpver) as Totimpver;
      From Rite4 into cursor Rite2 group by Mese order by Mese
      * --- Calcolo il numero dei versamenti da inserire nel cursore Rite2
      Select Distinct Month(Mrdatreg) as Mese1,Vpserial as Serial From Rite into cursor Rite7 group by Mese1,Serial
      * --- Calcolo il numero dei versamenti
      Select Count(*) as Numver,Mese1 as Mese1 from Rite7 into cursor Rite9 group by Mese1 order by Mese1
      * --- Aggiungo il numero dei versamenti al cursore Rite2
      Select Rite2.*,Rite9.Numver from Rite2 inner join Rite9 on Rite2.Mese=Rite9.Mese1 order by 1 into cursor Rite8 nofilter
      * --- Calcolo un cursore di appoggio (Rite5) per avere i valori da inserire nel record "GC"
      * --- Raggruppo per fornitore e Mese.Il record GC pu� comparire pi� volte per lo stesso fornitore.
      Select Month(Mrdatreg) as Mese,Mrcodcon as Mrcodcon,Sum(Mrtotim2) as Mrtotim2,;
      Max(Mrperri2) as Mrperri2,Sum(Mrimpri2) as Mrimpri2,Max(Ancodfis) as Ancodfis, Max(Ancodass) as Ancodass from Rite into cursor Rite5;
      Group by Mrcodcon,Mese Order by Mrcodcon,Mese
      Select Rite5
      this.w_TOTCONDO = 0
      Go Top
      Scan
      if this.w_Valuta=g_Codlir
        * --- Arrotondo alle 1000 Lire.
        this.w_TOTCONDO = this.w_Totcondo+cp_Round((Rite5.Mrtotim2/1000),0)*1000
      else
        * --- Arrotondo all'unit� di Euro.
        this.w_TOTCONDO = this.w_Totcondo+cp_Round((cp_Round(Mrimpri2,0) * Nvl(Mrperri2,0)/100),0)
      endif
      Endscan
      * --- Calcolo i versamenti effettuati per ogni fornitore.
      Select count(Mese) as Numese,mrcodcon as codice from Rite5;
      into cursor Rite6 group by Codice order by Codice
      Select Rite6
      Go Top
      this.w_TOTAGC = 0
      Scan
      * --- Devo calcolare il numero di records presenti di tipo 'GC' da inserire nel campo 'GS'
      this.w_NUMRECGC = Int(Rite6.Numese/2)
      if Int(Rite6.Numese/2)=(Rite6.Numese/2)
        this.w_NUMRECGC = Int(Rite6.Numese/2)
      else
        this.w_NUMRECGC = Int(Rite6.Numese/2)+1
      endif
      this.w_TOTAGC = this.w_Totagc+this.w_Numrecgc
      Endscan
      * --- Preparo il cursore da inserire nel campo del record "GS"
      this.w_TOTAGC1 = Alltrim(Str(this.w_Totagc,6,0))
      * --- Lancio in esecuzione la scrittura del file
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrivo record del corpo "GL - CG - TC"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrivo Record 'TT'
      this.Pag10()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Controllo se � stato chiuso correttamente il file creato
      if .not. fclose(this.w_Handle)
        NAMEFILE=this.OparentObject.w_NOMEFILE
        DELETE FILE &NAMEFILE
        ah_ErrorMsg("Non � possibile creare il file dati versamento ritenute","!","")
      else
        ah_ErrorMsg("Generazione file dati versamento ritenute terminata","!","")
      endif
    else
      this.w_MESS = "Non esistono dati da trasmettere"
      ah_ErrorMsg(this.w_MESS,"!","")
      this.w_Handle = fclose( this.w_Handle )
      ERASE (this.oParentObject.w_NomeFile)
    endif
    * --- Chiudo i cursori
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrivo record ("GS","GM","GR","TR")
    * --- Tipologia record "GS".Dati utente
    * --- Ragione sociale dell'utente.Letto sulla maschera
    this.w_Ragsoc1 = Upper(left( this.oParentObject.w_Ragsoc + space(50) , 50))
    * --- Codice fiscale o partita IVA dell'utente.Letto sulla maschera
    this.w_Codpart1 = Upper(left( this.oParentObject.w_Codpart + space(16) , 16))
    * --- Numero Committenti presenti nel supporto.Assume sempre come valore 1.
    this.w_Numcomm = "000001"
    * --- Numero collaboratori presenti nel supporto
    this.w_Numcoll1 = right("000000" + alltrim(this.w_Numcoll1), 6)
    * --- Totale dei campi "Importo totale versato"
    this.w_Lunghezza = 15
    this.w_Valore = Alltrim(Str(this.w_Totimpve,this.w_Lunghezza,0))
    this.w_Valore = right( repl("0",this.w_Lunghezza) + this.w_Valore, this.w_Lunghezza )
    * --- Totale dei campi "contributo dovuto"
    this.w_Valore1 = Alltrim(Str(this.w_Totcondo,this.w_Lunghezza,0))
    this.w_Valore1 = right( repl("0",this.w_Lunghezza) + this.w_Valore1, this.w_Lunghezza )
    * --- Cognome e nome persona di riferimento.Letto sulla maschera
    this.w_Cognom1 = Upper(left( this.oParentObject.w_Cognom + space(40) , 40))
    * --- Numero telefono di riferimento.Letto sulla maschera
    this.w_Telefo1 = Upper(left( this.oParentObject.w_Telefo + space(15) , 15))
    * --- Numero dei records presenti di tipo "GR".Assume sempre come valore 1.
    this.w_NumrecGR = "000001"
    * --- Numero dei records presenti di tipo "GC"
    this.w_NumrecGC = right("000000" + alltrim(this.w_Totagc1), 6)
    * --- Indirizzo e-mail.Letto sulla maschera
    this.w_Email1 = left( this.oParentObject.w_Email + space(30) , 30)
    * --- Composizione record e scrittura
    this.w_Record = "GS"
    this.w_Record = this.w_Record + this.w_Ragsoc1 + this.w_Codpart1 + this.w_Numcomm + this.w_Numcoll1+this.w_Valore+ this.w_Valore1
    this.w_Record = this.w_Record +this.w_Cognom1+this.w_Telefo1+this.w_NumrecGR+this.w_NumrecGC+this.w_Email1+space(49)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Tipologia record "GM".Dati del Committente
    * --- Codice fiscale del committente.Dati Azienda
    this.w_Cofazi = Upper(left( this.w_Cofazi + space(16) , 16))
    * --- Anno denuncia.Letto sulla maschera
    this.w_Anno1 = right("0000" + alltrim(this.w_Anno1), 4)
    * --- Denominazione del Committente.Dati Azienda
    this.w_Ragazi = Upper(left( this.w_Ragazi + space(60) , 60))
    * --- Indirizzo del Committente.Dati Azienda
    this.w_Indazi = Upper(left( this.w_Indazi + space(40) , 40))
    * --- C.A.P.Dati Azienda
    this.w_Capazi = right("00000" + alltrim(this.w_Capazi), 5)
    * --- Comune.Dati Azienda
    this.w_Locazi = Upper(left( this.w_Locazi + space(30) , 30))
    * --- Sigla della provincia.Dati Azienda
    this.w_Proazi = Upper(left( this.w_Proazi + space(2) , 2))
    * --- Codice Sede I.N.P.S. di competenza.Letto sulla tabella Ritenute.
    this.w_Sedinp1 = right("0000" + alltrim(Str(this.w_Sedinps,4,0)), 4)
    * --- Numero telefonico.Dati Azienda
    this.w_Telefo2 = Upper(left( this.w_Telefo2 + space(15) , 15))
    * --- Numero Fax.Dati Azienda
    this.w_Telefax = Upper(left( this.w_Telefax + space(15) , 15))
    * --- Indirizzo E-Mail.Dati Azienda
    this.w_E_Mail = left( this.w_E_Mail + space(30) , 30)
    * --- Codice ISTAT dell'attivit� svolta.Letto sulla maschera
    this.w_Gistat = right("00000" + alltrim(this.w_Gistat), 5)
    * --- Codice ISTAT della seconda attivit� svolta.Letto sulla maschera
    this.w_Gistat1 = right("00000" + alltrim(this.w_Gistat1), 5)
    * --- Se lo Stato non � estero inserisco space(3) altrimenti il codice ISO.
    if this.w_Codiso="IT "
      this.w_Codiso = space(3)
    else
      * --- Sigla Stato Estero.Codice ISO dati azienda
      this.w_Codiso = Upper(left( this.w_Codiso + space(3) , 3))
    endif
    * --- Tipo Valuta.Calcolata a pagina 1
    this.w_Tipval = right("0" + alltrim(this.w_Tipval), 1)
    * --- Tipo Denuncia.Letto sulla maschera
    this.w_Denuncia1 = Upper(left( this.oParentObject.w_Denuncia + space(1) , 1))
    * --- Dalla posizione 123 alla posizione 127 dovrebbe comparire il numero civico ma su Revolution
    * --- non esiste quindi riempito con 5 spazi.
    * --- Composizione record e scrittura
    this.w_Record = "GM"
    this.w_Record = this.w_Record + this.w_Cofazi + this.w_Anno1 + this.w_Ragazi + this.w_Indazi+space(5)+this.w_Capazi+ this.w_Locazi
    this.w_Record = this.w_Record +this.w_Proazi+this.w_Sedinp1+this.w_Telefo2+this.w_Telefax+this.w_E_mail+this.w_Gistat+this.w_Gistat1
    this.w_Record = this.w_Record +this.w_Codiso+this.w_Tipval+space(11)+this.w_Denuncia1+space(2)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Tipologia record "GR".Distinta Versamenti
    this.w_Record = "GR"
    this.w_Record = this.w_Record + this.w_Cofazi + this.w_Anno1
    * --- Seleziono il cursore Rite2 creato a pagina 1 per inserire i vari dati :
    * --- Mese del pagamento , Numero totale dei versamenti, Importo totale versato.
    * --- Va indicato il 1� mese con versamento significativo.Se ci sono pi� versamenti nello stesso mese
    * --- vanno sommati gli importi.
    Select Rite8
    Go Top
    Scan
    * --- Preparo la scrittura del mese,numero versamenti,totale versato
    this.w_MESE = Rite8.Mese
    this.w_MESE1 = Alltrim(Str(this.w_Mese,2,0))
    this.w_MESE1 = right("00" + alltrim(this.w_Mese1), 2)
    this.w_NUMVER = Rite8.Numver
    this.w_NUMVER1 = Alltrim(Str(this.w_Numver,4,0))
    this.w_NUMVER1 = right("0000" + alltrim(this.w_Numver1), 4)
    this.w_LUNGHEZZA = 10
    * --- Effettuo questa operazione per inserire gli arrottondamenti corretti.
    * --- Se valuta Lire arrotondo alle 1000.
    * --- Se valuta Euro arrotondo all'unit� di Euro
    if this.w_Valuta=g_Codlir
      * --- Arrotondo alle 1000 Lire.
      this.w_TOTVER = cp_Round((Rite8.Totimpver/1000),0)*1000
    else
      * --- Arrotondo all'unit� di Euro.
      this.w_TOTVER = cp_Round(Rite8.Totimpver,0)
    endif
    this.w_TOTVER1 = Alltrim(Str(this.w_Totver,this.w_Lunghezza,0))
    this.w_TOTVER1 = right( repl("0",this.w_Lunghezza) + this.w_Totver1, this.w_Lunghezza )
    * --- Questa variabile serve per potermi dire quanti mesi sono presenti.
    this.w_CONTA = this.w_Conta+1
    this.w_Record = this.w_Record+this.w_Mese1+this.w_Numver1+this.w_Totver1
    Endscan
    * --- Controllo se sono stati effettuati 12 versamenti. (Uno per ogni mese)
    if this.w_Conta<12
      * --- Devo riempire i record per i versamenti non effettuati.
      * --- Per ogni mese non inserito devo scrivere 16 zeri.Corrisponde al valore dei campi Mese,numero versamenti,importo totale versato.
      this.w_REPLI = (12-this.w_Conta)*16
      this.w_RIEMPIE = repl("0",this.w_Repli)
      this.w_Record = this.w_Record+this.w_Riempie
    endif
    this.w_Record = this.w_Record+space(42)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Tipologia record 'TR'.Totale Versamenti.
    * --- I campi R21 e R25 non vengono gestite su AHE.
    this.w_ECCEDE = "000000000000000"
    * --- Calcolo se il committente ha un saldo a credito oppure a debito.
    * --- Totale dei contributi dovuti-Totale versato.
    do case
      case this.w_Totcondo>this.w_Totimpve
        * --- Saldo a debito
        this.w_SALCRE = "000000000000000"
        this.w_SALDO = Abs(this.w_Totcondo-this.w_Totimpve)
        this.w_LUNGHEZZA = 15
        this.w_SALDEB = Alltrim(Str(this.w_Saldo,this.w_Lunghezza,0))
        this.w_SALDEB = right( repl("0",this.w_Lunghezza) + this.w_Saldeb, this.w_Lunghezza )
      case this.w_Totcondo<this.w_Totimpve
        * --- Saldo a debito
        this.w_SALDEB = "000000000000000"
        this.w_SALDO = Abs(this.w_Totcondo-this.w_Totimpve)
        this.w_LUNGHEZZA = 15
        this.w_SALCRE = Alltrim(Str(this.w_Saldo,this.w_Lunghezza,0))
        this.w_SALCRE = right( repl("0",this.w_Lunghezza) + this.w_Salcre, this.w_Lunghezza )
      case this.w_Totcondo=this.w_Totimpve
        this.w_SALCRE = "000000000000000"
        this.w_SALDEB = "000000000000000"
    endcase
    * --- Composizione record e scrittura
    this.w_Record = "TR"
    this.w_Record = this.w_Record + this.w_Cofazi + this.w_Anno1 +this.w_Valore+this.w_Eccede+this.w_Valore1
    this.w_Record = this.w_Record +this.w_Saldeb+this.w_Salcre+this.w_Eccede+space(144)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrivo Record ('GL')
    * --- Ciclo sul cursore Rite. Prima di effettuare questa operazione devo ordinare per codice fornitore
    Select * From Rite into cursor Rite1 order by Mrcodcon
    Select Rite1
    Go Top
    this.w_CODFOR = Space(15)
    Scan for not empty(nvl(Rite1.Mrcodcon,""))
    * --- Se � un nuovo collaboratore devo scrivere un record nuovo altrimenti niente.
    if this.w_Codfor<>Nvl(Rite1.Mrcodcon,"")
      this.w_CODFOR = Rite1.Mrcodcon
      * --- Progressivo del record 'GL'
      this.w_CONTA1 = this.w_CONTA1+1
      this.w_FPROGR = right("000000" + alltrim(Str(this.w_Conta1,6,0)), 6)
      * --- Devo verificare se il collaboratore � persona fisica
      if Rite1.Anperfis="S"
        * --- Cognome collaboratore
        this.w_FCOGNOM = Nvl(Rite1.Ancognom,"")
        this.w_FCOGNOM = Upper(left( this.w_Fcognom + space(20) , 20))
        * --- Nome del collaboratore
        this.w_FNOME = Nvl(Rite1.An__nome,"")
        this.w_FNOME = Upper(left( this.w_Fnome + space(20) , 20))
        * --- Dati di nascita del collaboratore
        this.w_DATNAS = Cp_Todate(Rite1.Andatnas)
        * --- Giorno
        this.w_FGIORNO = Day(this.w_Datnas)
        this.w_FGIORNO1 = right("00" + alltrim(Str(this.w_Fgiorno,2,0)), 2)
        * --- Mese
        this.w_FMESE = Month(this.w_Datnas)
        this.w_FMESE1 = right("00" + alltrim(Str(this.w_Fmese,2,0)), 2)
        * --- Anno
        this.w_FANNO = Year(this.w_Datnas)
        this.w_FANNO1 = right("0000" + alltrim(Str(this.w_Fanno,4,0)), 4)
        * --- Comune o stato estero di nascita.
        this.w_FLOCNAS = Nvl(Rite1.Anlocnas,"")
        this.w_FLOCNAS = Upper(left( this.w_Flocnas + space(30) , 30))
        * --- Leggo i dati delle destinazioni diverse in base al fornitore ed al codice della destinazione.
        this.w_DDCODICE = Nvl(Rite1.Ddcoddes,"")
        * --- Read from DES_DIVE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DDCODNAZ,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN"+;
            " from "+i_cTable+" DES_DIVE where ";
                +"DDTIPCON = "+cp_ToStrODBC("F");
                +" and DDCODICE = "+cp_ToStrODBC(this.w_CODFOR);
                +" and DDCODDES = "+cp_ToStrODBC(this.w_DDCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DDCODNAZ,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN;
            from (i_cTable) where;
                DDTIPCON = "F";
                and DDCODICE = this.w_CODFOR;
                and DDCODDES = this.w_DDCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DDCODNAZ = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
          this.w_DDINDIRI = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
          this.w_DD___CAP = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
          this.w_DDLOCALI = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
          this.w_DDPROVIN = NVL(cp_ToDate(_read_.DDPROVIN),cp_NullValue(_read_.DDPROVIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Devo verificare il Codice ISO.Se il codice ISO � uguale a 'IT' inserisco
        * --- il campo Anpronas altrimenti 'EE'.Il codice ISO viene letto dalla nazione delle dstinazioni diverse
        * --- se esiste una sede di tipo Residenza.
        this.w_FNAZION = iif(empty(Nvl(this.w_Ddcodnaz,"")),Nvl(Rite1.Annazion,""),Nvl(this.w_Ddcodnaz,""))
        * --- Leggo il codice ISO sulla tabella delle Nazioni.
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NACODISO"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_FNAZION);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NACODISO;
            from (i_cTable) where;
                NACODNAZ = this.w_FNAZION;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FCODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Legge il codice ISO sulla tabella delle Nazioni.
        if this.w_Fcodiso="IT "
          this.w_FPRONAS = Nvl(Rite1.Anpronas,"")
          * --- Sigla Stato Estero
          this.w_FCODISO = space(3)
        else
          this.w_FPRONAS = "EE"
          this.w_FCODISO = Upper(left( this.w_Fcodiso + space(3) , 3))
        endif
        this.w_FPRONAS = Upper(left( this.w_Fpronas + space(2) , 2))
        * --- Indirizzo del collaboratore.Letto sulle destinazione diverse.
        this.w_FINDIRI = iif(empty(Nvl(this.w_Ddindiri,"")),Nvl(Rite1.Anindiri,""),Nvl(this.w_Ddindiri,""))
        this.w_FINDIRI = Upper(left( this.w_Findiri + space(40) , 40))
        * --- C.A.P.
        this.w_FCAP = iif(empty(Nvl(this.w_Dd___Cap,"")),Nvl(Rite1.An___Cap,""),Nvl(this.w_Dd___Cap,""))
        this.w_FCAP = right("00000" + alltrim(this.w_Fcap), 5)
        * --- Comune o citt� estera.
        this.w_FLOCALI = iif(empty(Nvl(this.w_Ddlocali,"")),Nvl(Rite1.Anlocali,""),Nvl(this.w_Ddlocali,""))
        this.w_FLOCALI = Upper(left( this.w_Flocali + space(30) , 30))
        * --- Sigla della provincia
        this.w_FPROVIN = iif(empty(Nvl(this.w_Ddprovin,"")),Nvl(Rite1.Anprovin,""),Nvl(this.w_Ddprovin,""))
        this.w_FPROVIN = Upper(left( this.w_Fprovin + space(2) , 2))
      else
        * --- Cognome e Nome del collaboratore letti dalla ragione sociale del fornitore.
        this.w_FCOGNOM = Left(Nvl(Rite1.Andescri,""),20)
        this.w_FCOGNOM = Upper(left( this.w_Fcognom + space(20) , 20))
        this.w_FNOME = Right(Nvl(Rite1.Andescri,""),20)
        this.w_FNOME = Upper(left( this.w_Fnome + space(20) , 20))
        * --- I campi riferiti alla data di nascita,comune o stato estero di nascita non sono presenti
        * --- se il fornitore non � persona fisica.
        this.w_FGIORNO1 = "00"
        this.w_FMESE1 = "00"
        this.w_FANNO1 = "0000"
        this.w_FLOCNAS = space(30)
        this.w_FPRONAS = space(2)
        * --- Indirizzo del collaboratore
        this.w_FINDIRI = Nvl(Rite1.Anindiri,"")
        this.w_FINDIRI = Upper(left( this.w_Findiri + space(40) , 40))
        * --- C.A.P.
        this.w_FCAP = Nvl(Rite1.An___cap,"")
        this.w_FCAP = right("00000" + alltrim(this.w_Fcap), 5)
        * --- Comune o citt� estera.
        this.w_FLOCALI = Nvl(Rite1.Anlocali,"")
        this.w_FLOCALI = Upper(left( this.w_Flocali + space(30) , 30))
        * --- Sigla della provincia
        this.w_FPROVIN = Nvl(Rite1.Anprovin,"")
        this.w_FPROVIN = Upper(left( this.w_Fprovin + space(2) , 2))
        this.w_FNAZION = Nvl(Rite1.Annazion,"")
        * --- Leggo il codice ISO sulla tabella delle Nazioni.
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NACODISO"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_FNAZION);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NACODISO;
            from (i_cTable) where;
                NACODNAZ = this.w_FNAZION;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FCODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Legge il codice ISO sulla tabella delle Nazioni.
        if this.w_Fcodiso="IT "
          this.w_FCODISO = space(3)
        else
          * --- Sigla Stato Estero
          this.w_FCODISO = Upper(left( this.w_Fcodiso + space(3) , 3))
        endif
      endif
      * --- Codice Fiscale del collaboratore
      this.w_FCODFIS = Nvl(Rite1.Ancodfis,"")
      this.w_FCODFIS = Upper(left( this.w_Fcodfis + space(16) , 16))
      * --- Codice attivit� svolta dal collaboratore.
      this.w_FCODATT = Alltrim(Str(Nvl(Rite1.Ancodatt,0),2,0))
      this.w_FCODATT = right("00" + alltrim(this.w_Fcodatt),2 )
      * --- Scrivo i dati relativi al tipo di Record "GL".Dati del collaboratore.
      this.w_Record = "GL"
      this.w_Record = this.w_Record+this.w_Cofazi+this.w_Anno1+this.w_Fcodfis+this.w_Fcognom+this.w_Fnome+this.w_Fgiorno1
      this.w_Record = this.w_Record+this.w_Fmese1+this.w_Fanno1+this.w_Flocnas+this.w_Fpronas+this.w_Findiri+space(5)
      this.w_Record = this.w_Record+this.w_Fcap+this.w_Flocali+this.w_Fprovin+this.w_Fcodiso+this.w_Fcodatt+this.w_Fprogr+space(45)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrivo record "GC"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrivo Record ('TC')
      this.Pag9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    Endscan
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura record su file ascii
    * --- Scrittura record su File versamento ritenute
    w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipo Record "GC" - Record Contributivo
    Select Rite5
    Go Top
    * --- Contatore per i mesi.Infatti su una riga del record "GC" posso inserire due mesi per lo stesso fornitore
    this.w_CONFOREC = 0
    Scan for Nvl(Rite5.Mrcodcon,SPACE(15))=this.w_CODFOR
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Endscan
    * --- Scrivo i valori dell'ultimo record
    if this.w_Scritto=.F.
      * --- Non ho scritto la seconda parte.Scrivo valori vuoti nella parte mancante.
      this.w_Record = this.w_Record+"00"+"0000000000"+"0000"+"0000000000"+space(2)+"0000000000"
      this.w_Record = this.w_Record+space(2)+"0000000000"+space(2)+"0000000000"+"00000000"+"00000000"
      this.w_Record = this.w_Record+space(4)+space(54)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Leggo i valori da inserire nel codice "GC"
    this.w_CONFOREC = this.w_Conforec+1
    * --- Codice Fiscale del collaboratore
    this.w_FCODFIS = Nvl(Rite5.Ancodfis,"")
    this.w_FCODFIS = Upper(left( this.w_Fcodfis + space(16) , 16))
    * --- Mese in cui � stato effettuato il pagamento al collaboratore
    this.w_MESEGC = Rite5.Mese
    this.w_MESEGC1 = Alltrim(Str(this.w_Mesegc,2,0))
    this.w_MESEGC1 = right("00" + alltrim(this.w_Mesegc1), 2)
    * --- Effettuo questa operazione per inserire gli arrottondamenti corretti.
    * --- Se valuta Lire arrotondo alle 1000.
    * --- Se valuta Euro arrotondo all'unit� di Euro
    if this.w_Valuta=g_Codlir
      * --- Arrotondo alle 1000 Lire.
      this.w_ARRIMPGC = cp_Round((Rite5.Mrtotim2/1000),0)*1000
      this.w_ARRCONGC = cp_Round((Rite5.Mrimpri2/1000),0)*1000
    else
      * --- Arrotondo all'unit� di Euro.
      this.w_ARRCONGC = cp_Round(Rite5.Mrimpri2,0)
      this.w_ARRIMPGC = cp_ROUND((this.w_ARRCONGC * Nvl(Rite5.Mrperri2,0)/100),0)
    endif
    * --- Imponibile
    this.w_LUNGHEZZA = 10
    this.w_IMPONIGC = Alltrim(Str(this.w_Arrimpgc,this.w_Lunghezza,0))
    this.w_IMPONIGC = right( repl("0",this.w_Lunghezza) + this.w_Imponigc, this.w_Lunghezza )
    * --- Contributo dovuto
    this.w_CONTRGC = Alltrim(Str(this.w_Arrcongc,this.w_Lunghezza,0))
    this.w_CONTRGC = right( repl("0",this.w_Lunghezza) + this.w_Contrgc, this.w_Lunghezza )
    * --- Aliquota applicata per il calcolo del contributo
    this.w_LUNGHEZZA = 4
    this.w_PERCENT = Nvl(Rite5.Mrperri2,0)
    this.w_PERCENT1 = Alltrim(Str(this.w_Percent*100))
    this.w_PERCENT1 = right( repl("0",this.w_Lunghezza) + this.w_Percent1, this.w_Lunghezza )
    * --- Dal (inizio attivit� a cui si riferisce il compenso erogato)
    this.w_DATINGC = "01"+this.w_Mesegc1+Str(this.oParentObject.w_Anno,4,0)
    this.w_DATINGC = right("00000000" + alltrim(this.w_Datingc), 8)
    * --- Al (fine attivit� a cui si riferisce il compenso erogato)
    * --- Verifico in che mese mi trovo
    do case
      case this.w_Mesegc=1 or this.w_Mesegc=3 or this.w_Mesegc=5 or this.w_Mesegc=7 or this.w_Mesegc=8 or this.w_Mesegc=10 or this.w_Mesegc=12
        this.w_DATFIGC = "31"+this.w_Mesegc1+this.w_Anno1
      case this.w_Mesegc=2
        this.w_DATFEB = cp_CharToDate("01-03-"+this.w_ANNO1)
        this.w_DATFEB = this.w_DATFEB-1
        this.w_DAYDATFE = Day(this.w_Datfeb)
        this.w_DATFIGC = Str(this.w_Daydatfe,2,0)+this.w_Mesegc1+this.w_Anno1
      case this.w_Mesegc=4 or this.w_Mesegc=6 or this.w_Mesegc=9 or this.w_Mesegc=11
        this.w_DATFIGC = "30"+this.w_Mesegc1+this.w_Anno1
    endcase
    this.w_DATFIGC = right("00000000" + alltrim(this.w_Datfigc), 8)
    this.w_CODALASS = right(SPACE(3)+iif(this.w_PERCENT<>10 , SPACE(3),nvl(RITE5.ANCODASS,space(3))),3)
    * --- Verifico se sto scrivendo la prima parte di record
    if this.w_Conforec=1
      * --- I campi riguardanti le eccedenze non vengono calcolati. Dalla posizione 65 alla posizione 100
      this.w_Record = "GC"
      this.w_Record = this.w_Record+this.w_Cofazi+this.w_Anno1
      this.w_Record = this.w_Record+this.w_Fcodfis+this.w_Mesegc1+this.w_Contrgc+this.w_Percent1+this.w_Imponigc
      this.w_Record = this.w_Record+space(2)+"0000000000"+space(2)+"0000000000"+space(2)+"0000000000"
      this.w_Record = this.w_Record+this.w_Datingc+this.w_Datfigc+this.w_Codalass+space(1)
      this.w_SCRITTO = .F.
      * --- La variabile Scritto serve per poter sapere se ho scritto il record oppure no.
    else
      * --- Sono sulla seconda parte del record. Effetto la scrittura
      this.w_Record = this.w_Record+this.w_Mesegc1+this.w_Contrgc+this.w_Percent1+this.w_Imponigc
      this.w_Record = this.w_Record+space(2)+"0000000000"+space(2)+"0000000000"+space(2)+"0000000000"
      this.w_Record = this.w_Record+this.w_Datingc+this.w_Datfigc+ this.w_Codalass +space(1)+space(54)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CONFOREC = 0
      this.w_SCRITTO = .T.
      * --- La variabile Scritto serve per poter sapere se ho scritto il record oppure no.
      this.w_Record = space(120)
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursori
    if used("RITE")
      select ("RITE")
      USE
    endif
    if used("RITE1")
      select ("RITE1")
      USE
    endif
    if used("RITE2")
      select ("RITE2")
      USE
    endif
    if used("RITE3")
      select ("RITE3")
      USE
    endif
    if used("RITE4")
      select ("RITE4")
      USE
    endif
    if used("RITE5")
      select ("RITE5")
      USE
    endif
    if used("RITE6")
      select ("RITE6")
      USE
    endif
    if used("RITE7")
      select ("RITE7")
      USE
    endif
    if used("RITE8")
      select ("RITE8")
      USE
    endif
    if used("RITE9")
      select ("RITE9")
      USE
    endif
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Compilazione GLA
    * --- Dichiarazione variabili locali
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrivo record "TC". Totali contributivi collaboratore
    Select Rite5
    Go Top
    this.w_CONTRITC = 0
    this.w_IMPONITC = 0
    Scan for Nvl(Rite5.Mrcodcon,SPACE(15))=this.w_CODFOR
    * --- Codice Fiscale del collaboratore
    this.w_FCODFIS = Nvl(Rite5.Ancodfis,"")
    this.w_FCODFIS = Upper(left( this.w_Fcodfis + space(16) , 16))
    this.w_LUNGHEZZA = 15
    if this.w_Valuta=g_Codlir
      * --- Arrotondo alle 1000 Lire.
      this.w_CONTRITC = this.w_Contritc+cp_Round((Rite5.Mrimpri2/1000),0)*1000
      this.w_IMPONITC = this.w_Imponitc+cp_Round((Rite5.Mrtotim2/1000),0)*1000
    else
      * --- Arrotondo all'unit� di Euro.
      this.w_CONTRITC = this.w_Contritc+cp_Round(Rite5.Mrimpri2,0)
      this.w_IMPONITC = this.w_Imponitc+cp_ROUND((this.w_CONTRITC * Nvl(Rite5.Mrperri2,0)/100),0)
    endif
    Endscan
    * --- Totale imponibile
    this.w_TOTCONTC = Alltrim(Str(this.w_Contritc,this.w_Lunghezza,0))
    this.w_TOTCONTC = right( repl("0",this.w_Lunghezza) + this.w_Totcontc, this.w_Lunghezza )
    * --- Totale Contributi
    this.w_TOTIMPTC = Alltrim(Str(this.w_Imponitc,this.w_Lunghezza,0))
    this.w_TOTIMPTC = right( repl("0",this.w_Lunghezza) + this.w_Totimptc, this.w_Lunghezza )
    * --- I campi da 84 a 119 e da 244 a 253 non sono gestiti e quindi creati vuoti.
    this.w_Record = "TC"
    this.w_Record = this.w_Record+this.w_Cofazi+this.w_Anno1+this.w_Fcodfis+this.w_Totcontc+this.w_Totimptc
    this.w_Record = this.w_Record+"000000000000000"+space(2)+"0000000000"+space(2)+"0000000000"
    this.w_Record = this.w_Record+space(2)+"0000000000"+space(124)+"0000000000"+space(3)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrivo Record "TT" Dati Riassuntivi
    this.w_Record = "TT"
    this.w_Record = this.w_Record+this.w_Cofazi+this.w_Anno1+this.w_Numcoll1+space(226)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TAB_RITE'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='NAZIONI'
    this.cWorkTables[5]='DES_DIVE'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
