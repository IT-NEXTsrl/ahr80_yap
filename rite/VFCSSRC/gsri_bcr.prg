* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bcr                                                        *
*              Certificazioni ritenute                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][82]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2014-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bcr",oParentObject)
return(i_retval)

define class tgsri_bcr as StdBatch
  * --- Local variables
  w_MESS = space(50)
  w_CAMBIO = 0
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_INDIRI = space(35)
  w_LOCALI = space(30)
  w_PROVIN = space(2)
  w_OBTEST = ctod("  /  /  ")
  w_DTOBSO = ctod("  /  /  ")
  w_TEST = .f.
  w_CAP = space(10)
  w_MESS = space(50)
  * --- WorkFile variables
  DES_DIVE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch serve per convertire i movimenti ritenute in base alla maschera (da GSRI_SCR)
    * --- di stampa delle certificazioni ritenute
    this.w_OBTEST = i_DATSYS
    if this.oParentObject.w_DATFIN<this.oParentObject.w_DATINI
      this.w_MESS = "La data iniziale � maggiore della data finale"
      ah_ErrorMsg(this.w_MESS,"","")
      i_retcode = 'stop'
      return
    endif
    if YEAR(this.oParentObject.w_DATFIN)<>YEAR(this.oParentObject.w_DATINI)
      this.w_MESS = "L'intervallo di date deve essere interno ad un anno solare"
      ah_ErrorMsg(this.w_MESS,"","")
      i_retcode = 'stop'
      return
    endif
    * --- Valorizzo le variabili per gli indirizzi email e il numero di fax
    if this.oParentObject.w_PERCIN = this.oParentObject.w_PERCFIN and not empty(this.oParentObject.w_PERCIN)
      GSAR_BRM(this,"E_M","F",this.oParentObject.w_PERCIN,"RE","V")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Valorizzo le variabili globali per il Fax (per la selezione del numero di telefono)
      i_CLIFORDES = "F" 
 i_CODDES = this.oParentObject.w_PERCIN 
 i_TIPDES = "RE" 
 i_DEST=this.oParentObject.w_DESCINI
    endif
    do case
      case this.oParentObject.w_STAMPA="S"
        * --- Lancio la query che va a leggere nei movimenti ritenute
        vq_exec("..\RITE\EXE\QUERY\GSRI_SCR.VQR",this,"RITE2")
        * --- Leggo la query di collegamento dei versamenti  IRPEF
        vq_exec("..\RITE\EXE\QUERY\GSRI2SCR.VQR",this,"RITE1")
        SELECT * FROM RITE2 WHERE MRRIFDI1 IN (SELECT SERIAL FROM RITE1) INTO CURSOR RITE 
        * --- Rendo scrivibile il cursore RITE
        WRCURSOR("RITE")
        * --- Legge la prima residenza diversa e la sostituisce nel cursore con i campi fittizi
        GO TOP
        SCAN
        this.w_INDIRI = NVL(ANINDIRI,SPACE(35))
        this.w_LOCALI = NVL(ANLOCALI, SPACE(30))
        this.w_PROVIN = NVL(ANPROVIN,SPACE(2))
        this.w_CAP = NVL(AN___CAP,SPACE(10))
        this.w_TEST = .F.
        this.w_CODCON = NVL(MRCODCON,SPACE(15))
        * --- Select from DES_DIVE
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
              +" where DDTIPCON='F' AND DDCODICE="+cp_ToStrODBC(this.w_CODCON)+" AND DDTIPRIF='RE'";
               ,"_Curs_DES_DIVE")
        else
          select * from (i_cTable);
           where DDTIPCON="F" AND DDCODICE=this.w_CODCON AND DDTIPRIF="RE";
            into cursor _Curs_DES_DIVE
        endif
        if used('_Curs_DES_DIVE')
          select _Curs_DES_DIVE
          locate for 1=1
          do while not(eof())
          this.w_DTOBSO = NVL(CP_TODATE(DDDTOBSO),cp_CharToDate("  -  -  "))
          if (this.w_DTOBSO>this.w_OBTEST OR EMPTY(this.w_DTOBSO)) AND this.w_TEST=.F.
            this.w_TEST = .T.
            this.w_INDIRI = NVL(DDINDIRI,SPACE(35))
            this.w_LOCALI = NVL(DDLOCALI, SPACE(30))
            this.w_PROVIN = NVL(DDPROVIN,SPACE(2))
            this.w_CAP = NVL(DD___CAP,SPACE(10))
          endif
            select _Curs_DES_DIVE
            continue
          enddo
          use
        endif
        SELECT RITE
        REPLACE DDINDIRI WITH this.w_INDIRI
        REPLACE DDLOCALI WITH this.w_LOCALI
        REPLACE DDPROVIN WITH this.w_PROVIN
        REPLACE DD___CAP WITH this.w_CAP
        ENDSCAN
        * --- Rendo scrivibile il cursore RITE
        WRCURSOR("RITE")
        SCAN FOR MRCODVAL<>this.oParentObject.w_CODVAL
        REPLACE SOMTOTA WITH VAL2MON(NVL(SOMTOTA,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE IMPONI WITH VAL2MON(NVL(IMPONI,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE NONSOGG WITH VAL2MON(NVL(NONSOGG,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE MRTOTIM1 WITH VAL2MON(NVL(MRTOTIM1,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE NETCORR WITH VAL2MON(NVL(NETCORR,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE IMPINPS WITH VAL2MON(NVL(IMPINPS,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE SOMTOTA2 WITH VAL2MON(NVL(SOMTOTA2,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE NONSOGG2 WITH VAL2MON(NVL(NONSOGG2,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE NETCORR2 WITH VAL2MON(NVL(NETCORR2,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE IMPRITIN WITH VAL2MON(NVL(IMPRITIN,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        REPLACE IMPPERC WITH VAL2MON(NVL(IMPPERC,0), this.w_CAMBIO,1,I_DATSYS,G_PERVAL,this.oParentObject.w_DECIMI)
        ENDSCAN
        * --- Seleziono il cursore RITE e sommo i vari campi nella valuta selezionata in maschera
        SELECT MRCODCON, MAX(ANLOCNAS) AS ANLOCNAS, MAX(ANPRONAS) AS ANPRONAS,; 
 MAX(ANDATNAS) AS ANDATNAS, MAX(ANLOCALI) AS ANLOCALI,; 
 MAX(ANPROVIN) AS ANPROVIN, MAX(AN___CAP) AS AN___CAP,; 
 MAX(ANINDIRI) AS ANINDIRI,MAX(ANCODFIS) AS ANCODFIS,; 
 MRCODTRI, SUM(SOMTOTA) AS SOMTOTA, SUM(IMPONI) AS IMPONI, SUM(NONSOGG) AS NONSOGG,; 
 MRPERRI1 AS MRPERRI1,SUM(MRTOTIM1) AS MRTOTIM1, SUM(NETCORR) AS NETCORR,; 
 MAX(TRFLACON) AS TRFLACON,MAX(ANDESCRI) AS ANDESCRI,MAX(TRDESTRI) AS TRDESTRI,; 
 MAX(MRCODVAL) AS MRCODVAL, MAX(ANCOINPS) AS ANCOINPS,MAX(ANPERFIS) AS ANPERFIS,; 
 MAX(ANCOGNOM) AS ANCOGNOM,MAX(AN__NOME) AN__NOME,; 
 MAX(DDINDIRI) AS DDINDIRI,MAX(DDLOCALI) AS DDLOCALI,MAX(DDPROVIN) AS DDPROVIN,; 
 SUM(IMPPERC) AS IMPPERC,MAX(MRCODTR2) AS MRCODTR2,MAX(ANRIINPS) AS ANRIINPS,MAX(IMPRITIN) AS IMPRITIN,; 
 MAX(DRRIINPS) AS DRRIINPS, MIN(DRRIINPS1) AS DRRIINPS1, MAX(DRPERRIT) AS DRPERRIT, MIN(DRPERRIT1) AS DRPERRIT1,; 
 MAX(ANDESCR2) AS ANDESCR2,MAX(ANNAZION) AS ANNAZION,MAX(DD___CAP) AS DD___CAP,; 
 MAX(DDCODNAZ) AS DDCODNAZ,MAX(APPOGGIO) AS APPOGGIO,MAX("D") AS TIPO, MAX(CODICE) AS CODICE,MAX(MRPERPER) AS MRPERPER, MIN(MRPERPER1) AS MRPERPER1 FROM RITE ; 
 into cursor RITE1 GROUP BY MRCODCON, MRCODTRI,MRPERRI1 ORDER BY MRCODCON ,MRCODTRI,MRPERRI1
        * --- Controllo se l'utente ha selezionato il flag 'Stampa descrizione codice tributo', in questo
        *     caso devo lanciare una query che mi legga tutti i codice F24 con la descrizione associata ed inserirli
        *     in un cursore di appoggio (F24).
        *     Successivamente devo mettere in join i due cursori e prendere dal cursore F24 solo la descrizione
        vq_exec("..\RITE\EXE\QUERY\GSRI8SCR.VQR",this,"F24")
        if Reccount("F24")>0
          * --- Metto in join i cursori __TMP__ e F24 per riportare nel cursore di stampa la descrizione.
          Select distinct RITE1.*,F24.DESCF24 from RITE1;
          LEFT OUTER JOIN F24 on RITE1.MRCODTRI=F24.TRCODICE;
          order by 1,10,14 into cursor __TMP__ nofilter
        else
          Select Rite1
          Select *,space(100) as descf24 from RITE1 into cursor __TMP__ order by 1,10,14
        endif
        * --- Passo le seguenti variabili al report
        L_DATINI=this.oParentObject.w_DATINI
        L_DATFIN=this.oParentObject.w_DATFIN
        L_ANNO=STR(YEAR(this.oParentObject.w_DATINI), 4, 0)
        L_PERCIN=this.oParentObject.w_PERCIN
        L_PERCFIN=this.oParentObject.w_PERCFIN
        L_CODVAL=this.oParentObject.w_CODVAL
        L_DECIMI=this.oParentObject.w_DECIMI
        L_DESCTRIB=this.oParentObject.w_DESCTRIB
        L_STAMPA=this.oParentObject.w_STAMPA
        L_NONOTE=this.oParentObject.w_NOSTANOTE
        * --- Creo un nuovo cursore per poter stampare i dati del percipiente in una nuova
        *     pagina.
        SELECT DISTINCT MRCODCON,MAX(ANLOCALI) AS ANLOCALI,MAX(ANPROVIN) AS ANPROVIN,; 
 MAX(AN___CAP) AS AN___CAP,MAX(ANINDIRI) AS ANINDIRI,MAX(ANNAZION) AS ANNAZION,MAX(ANDESCRI) AS ANDESCRI,; 
 MAX(ANPERFIS) ANPERFIS, MAX(ANCOGNOM) AS ANCOGNOM,MAX(AN__NOME) AS AN__NOME,; 
 MAX(DDINDIRI) AS DDINDIRI,MAX(DDLOCALI) AS DDLOCALI,; 
 MAX(DDPROVIN) AS DDPROVIN,MAX(DD___CAP) AS DD___CAP,MAX(DDCODNAZ) AS DDCODNAZ,MAX(ANDESCR2) AS ANDESCR2 ; 
 FROM __TMP__ INTO CURSOR INTESTA GROUP BY MRCODCON ORDER BY MRCODCON
        * --- Lancio il report di stampa
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI_SCR.FRX", " ", this)
        * --- Reimposto le variabili per indirizzi email e numero Fax
        if this.oParentObject.w_PERCIN = this.oParentObject.w_PERCFIN and not empty(this.oParentObject.w_PERCIN)
          i_EMAIL = " "
          i_EMAIL_PEC = " "
          * --- Resetto le variabili globali per il Fax
          i_CLIFORDES = " " 
 i_CODDES = " " 
 i_TIPDES = " "
        endif
      case this.oParentObject.w_STAMPA="D"
        * --- Lancio la query di elaborazione dei dati
        vq_exec("..\RITE\EXE\QUERY\GSRIDSCR.VQR",this,"RITE")
        * --- Legge la prima residenza diversa e la sostituisce nel cursore con i campi fittizi
        SELECT RITE
        GO TOP
        SCAN
        this.w_INDIRI = NVL(ANINDIRI,SPACE(35))
        this.w_LOCALI = NVL(ANLOCALI, SPACE(30))
        this.w_PROVIN = NVL(ANPROVIN,SPACE(2))
        this.w_CAP = NVL(AN___CAP,SPACE(10))
        this.w_TEST = .F.
        this.w_CODCON = NVL(MRCODCON,SPACE(15))
        * --- Select from DES_DIVE
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
              +" where DDTIPCON='F' AND DDCODICE="+cp_ToStrODBC(this.w_CODCON)+" AND DDTIPRIF='RE'";
               ,"_Curs_DES_DIVE")
        else
          select * from (i_cTable);
           where DDTIPCON="F" AND DDCODICE=this.w_CODCON AND DDTIPRIF="RE";
            into cursor _Curs_DES_DIVE
        endif
        if used('_Curs_DES_DIVE')
          select _Curs_DES_DIVE
          locate for 1=1
          do while not(eof())
          this.w_DTOBSO = NVL(CP_TODATE(DDDTOBSO),cp_CharToDate("  -  -  "))
          if (this.w_DTOBSO>this.w_OBTEST OR EMPTY(this.w_DTOBSO)) AND this.w_TEST=.F.
            this.w_TEST = .T.
            this.w_INDIRI = NVL(DDINDIRI,SPACE(35))
            this.w_LOCALI = NVL(DDLOCALI, SPACE(30))
            this.w_PROVIN = NVL(DDPROVIN,SPACE(2))
            this.w_CAP = NVL(DD___CAP,SPACE(10))
          endif
            select _Curs_DES_DIVE
            continue
          enddo
          use
        endif
        SELECT RITE
        REPLACE DDINDIRI WITH this.w_INDIRI
        REPLACE DDLOCALI WITH this.w_LOCALI
        REPLACE DDPROVIN WITH this.w_PROVIN
        REPLACE DD___CAP WITH this.w_CAP
        ENDSCAN
        * --- Aggiorna importi
        SELECT RITE
        WRCURSOR("RITE")
        GO TOP
        SCAN FOR MRCODVAL<>this.oParentObject.w_CODVAL
        this.w_CAMBIO = NVL(RITE.MRCAOVAL,g_CAOVAL)
        REPLACE SOMTOTA WITH cp_ROUND(VAL2MON(NVL(SOMTOTA,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE IMPONI WITH cp_ROUND(VAL2MON(NVL(IMPONI,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE NONSOGG WITH cp_ROUND(VAL2MON(NVL(NONSOGG,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE MRTOTIM1 WITH cp_ROUND(VAL2MON(NVL(MRTOTIM1,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE NETCORR WITH cp_ROUND(VAL2MON(NVL(NETCORR,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE IMPPERC WITH cp_ROUND(VAL2MON(NVL(IMPPERC,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        ENDSCAN
        * --- Seleziono il cursore RITE e sommo i vari campi nella valuta selezionata in maschera
        SELECT Mrcodcon, Max(Anlocnas) as Anlocnas, Max(Anpronas) as Anpronas, Max(Andatnas) as Andatnas,; 
 Max(Anlocali) as Anlocali,Max(Anprovin) as Anprovin, Max(An___cap) as An___cap,; 
 Max(Anindiri) as Anindiri,Max(Ancodfis) as Ancodfis,Mrcodtri,Sum(Somtota) as Somtota,; 
 Sum(Imponi) as Imponi, Sum(Nonsogg) as Nonsogg, Mrperri1 as Mrperri1,; 
 Sum(Mrtotim1) as Mrtotim1, Sum(Netcorr) as Netcorr, Max(Trflacon) as Trflacon,Mrrifdi1,; 
 Max(Andescri) as Andescri,Max(Trdestri) as Trdestri, Max(Mrcodval) as Mrcodval, Max(Ancoinps) as Ancoinps,; 
 Max(Anperfis) as Anperfis,Max(Ancognom) as Ancognom,Max(An__nome) An__nome,; 
 Max(Ddindiri) as Ddindiri,Max(Ddlocali) as Ddlocali,Max(Ddprovin) as Ddprovin,; 
 Sum(Impperc) as Impperc,Max(Mrcodtr2) as Mrcodtr2,Max(Anriinps) as Anriinps,; 
 Max(Vpnumreg) as Vpnumreg,Max(Vpcodese) as Vpcodese,Max(Vpdatreg) as Vpdatreg,; 
 Max(Vpnumver) as Vpnumver,Max(Vpdatver) as Vpdatver,Max(Vpcodban) as Vpcodban,; 
 Max(Vpnumcor) as Vpnumcor,Max(Vpcodabi) as Vpcodabi,Max(Vpcodcab) as Vpcodcab,; 
 Max(Vpcodcon) as Vpcodcon,Max(Vptipver) as Vptipver,Max(Vpmodver) as Vpmodver,; 
 Max("D") as Tipo,Max(Andescr2) as Andescr2, Max(Annazion) as Annazion,; 
 Max(Dd___Cap) as Dd___Cap,Max(Ddcodnaz) as Ddcodnaz,Max(Appoggio) as Appoggio,Max(Codice) as Codice; 
 From RITE into cursor Stampa Group by Mrcodcon,Mrrifdi1,Mrcodtri,Mrperri1 Order by Mrcodcon,Vpcodese,Vpdatreg,Vpnumreg,Mrcodtri,Mrperri1
        * --- Costruisco il cursore contenente i totali da riportare all'interno del report.
        SELECT Mrcodcon, Max(Anlocnas) as Anlocnas, Max(Anpronas) as Anpronas, Max(Andatnas) as Andatnas,; 
 Max(Anlocali) as Anlocali,Max(Anprovin) as Anprovin, Max(An___cap) as An___cap,; 
 Max(Anindiri) as Anindiri,Max(Ancodfis) as Ancodfis,Mrcodtri,Sum(Somtota) as Somtota,; 
 Sum(Imponi) as Imponi, Sum(Nonsogg) as Nonsogg, Max(Mrperri1) as Mrperri1,; 
 Sum(Mrtotim1) as Mrtotim1, Sum(Netcorr) as Netcorr, Max(Trflacon) as Trflacon,Max(Mrrifdi1) as Mrrifdi1,; 
 Max(Andescri) as Andescri,Max(Trdestri) as Trdestri, Max(Mrcodval) as Mrcodval, Max(Ancoinps) as Ancoinps,; 
 Max(Anperfis) as Anperfis,Max(Ancognom) as Ancognom,Max(An__nome) An__nome,; 
 Max(Ddindiri) as Ddindiri,Max(Ddlocali) as Ddlocali,Max(Ddprovin) as Ddprovin,; 
 Sum(Impperc) as Impperc,Max(Mrcodtr2) as Mrcodtr2,Max(Anriinps) as Anriinps,; 
 Max(Vpnumreg) as Vpnumreg,Max(Vpcodese) as Vpcodese,Max(Vpdatreg) as Vpdatreg,; 
 Max(Vpnumver) as Vpnumver,Max(Vpdatver) as Vpdatver,Max(Vpcodban) as Vpcodban,; 
 Max(Vpnumcor) as Vpnumcor,Max(Vpcodabi) as Vpcodabi,Max(Vpcodcab) as Vpcodcab,; 
 Max(Vpcodcon) as Vpcodcon,Max(Vptipver) as Vptipver,Max(Vpmodver) as Vpmodver,; 
 Max("T") as Tipo,Max(Andescr2) as Andescr2,Max(Annazion) as Annazion,; 
 Max(Dd___Cap) as Dd___Cap,Max(Ddcodnaz) as Ddcodnaz,Max(Appoggio) as Appoggio,Max(Codice) as Codice; 
 From Stampa into cursor Totale Group by Mrcodcon,Mrcodtri Order by Mrcodcon,Mrcodtri
        * --- Creo un cursore unico da stampare
        Select * from Stampa union Select * from Totale;
        into cursor Elabora order by 1,44,33,34,32
        * --- Controllo se l'utente ha selezionato il flag 'Stampa descrizione codice tributo', in questo
        *     caso devo lanciare una query che mi legga tutti i codice F24 con la descrizione associata ed inserirli
        *     in un cursore di appoggio (F24).
        *     Successivamente devo mettere in join i due cursori e prendere dal cursore F24 solo la descrizione
        vq_exec("..\RITE\EXE\QUERY\GSRI8SCR.VQR",this,"F24")
        if Reccount("F24")>0
          * --- Metto in join i cursori __TMP__ e F24 per riportare nel cursore di stampa la descrizione.
          SELECT DISTINCT ELABORA.*,F24.DESCF24 FROM ELABORA;
          LEFT OUTER JOIN F24 on ELABORA.MRCODTRI=F24.TRCODICE;
          ORDER BY 1,44,33,34,32 INTO CURSOR __TMP__ NOFILTER
        else
          Select Elabora
          Select *,space(100) as descf24 from ELABORA into cursor __TMP__ order by 1,44,33,34,32
        endif
        * --- Passo le seguenti variabili al report
        L_ANNO=this.oParentObject.w_ANNO
        L_CODVAL=this.oParentObject.w_CODVAL
        L_DECIMI=this.oParentObject.w_DECIMI
        L_DATINI=this.oParentObject.w_DATINI
        L_DATFIN=this.oParentObject.w_DATFIN
        L_PERCIN=this.oParentObject.w_PERCIN
        L_PERCFIN=this.oParentObject.w_PERCFIN
        L_STAMPA=this.oParentObject.w_STAMPA
        L_SIMVAL=this.oParentObject.w_SIMVAL
        L_DESCTRIB=this.oParentObject.w_DESCTRIB
        L_NONOTE=this.oParentObject.w_NOSTANOTE
        * --- Creo un nuovo cursore per poter stampare i dati del percipiente in una nuova
        *     pagina.
        SELECT DISTINCT MRCODCON,MAX(ANLOCALI) AS ANLOCALI,MAX(ANPROVIN) AS ANPROVIN,; 
 MAX(AN___CAP) AS AN___CAP,MAX(ANINDIRI) AS ANINDIRI,MAX(ANDESCRI) AS ANDESCRI,; 
 MAX(ANPERFIS) ANPERFIS, MAX(ANCOGNOM) AS ANCOGNOM,MAX(AN__NOME) AS AN__NOME,; 
 MAX(DDINDIRI) AS DDINDIRI,MAX(DDLOCALI) AS DDLOCALI,MAX(DDPROVIN) AS DDPROVIN,MAX(ANDESCR2) AS ANDESCR2,; 
 MAX(ANNAZION) AS ANNAZION,MAX(DD___CAP) AS DD___CAP,MAX(DDCODNAZ) AS DDCODNAZ FROM __TMP__ INTO CURSOR INTESTA GROUP BY MRCODCON ORDER BY MRCODCON
        SELECT __TMP__
        * --- Lancio il report di stampa
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI1SCR.FRX","",this.oParentObject)
      case this.oParentObject.w_STAMPA="U"
        * --- Lancio la query di elaborazione dei dati
        vq_exec("..\RITE\EXE\QUERY\GSRIUSCR.VQR",this,"RITE")
        * --- Legge la prima residenza diversa e la sostituisce nel cursore con i campi fittizi
        SELECT RITE
        GO TOP
        SCAN
        this.w_INDIRI = NVL(ANINDIRI,SPACE(35))
        this.w_LOCALI = NVL(ANLOCALI, SPACE(30))
        this.w_PROVIN = NVL(ANPROVIN,SPACE(2))
        this.w_CAP = NVL(AN___CAP,SPACE(10))
        this.w_TEST = .F.
        this.w_CODCON = NVL(MRCODCON,SPACE(15))
        * --- Select from DES_DIVE
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
              +" where DDTIPCON='F' AND DDCODICE="+cp_ToStrODBC(this.w_CODCON)+" AND DDTIPRIF='RE'";
               ,"_Curs_DES_DIVE")
        else
          select * from (i_cTable);
           where DDTIPCON="F" AND DDCODICE=this.w_CODCON AND DDTIPRIF="RE";
            into cursor _Curs_DES_DIVE
        endif
        if used('_Curs_DES_DIVE')
          select _Curs_DES_DIVE
          locate for 1=1
          do while not(eof())
          this.w_DTOBSO = NVL(CP_TODATE(DDDTOBSO),cp_CharToDate("  -  -  "))
          if (this.w_DTOBSO>this.w_OBTEST OR EMPTY(this.w_DTOBSO)) AND this.w_TEST=.F.
            this.w_TEST = .T.
            this.w_INDIRI = NVL(DDINDIRI,SPACE(35))
            this.w_LOCALI = NVL(DDLOCALI, SPACE(30))
            this.w_PROVIN = NVL(DDPROVIN,SPACE(2))
            this.w_CAP = NVL(DD___CAP,SPACE(10))
          endif
            select _Curs_DES_DIVE
            continue
          enddo
          use
        endif
        SELECT RITE
        REPLACE DDINDIRI WITH this.w_INDIRI
        REPLACE DDLOCALI WITH this.w_LOCALI
        REPLACE DDPROVIN WITH this.w_PROVIN
        REPLACE DD___CAP WITH this.w_CAP
        ENDSCAN
        * --- Aggiorna importi
        SELECT RITE
        WRCURSOR("RITE")
        GO TOP
        SCAN FOR MRCODVAL<>this.oParentObject.w_CODVAL
        this.w_CAMBIO = NVL(RITE.MRCAOVAL,g_CAOVAL)
        REPLACE SOMTOTA WITH cp_ROUND(VAL2MON(NVL(SOMTOTA,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE IMPONI WITH cp_ROUND(VAL2MON(NVL(IMPONI,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE NONSOGG WITH cp_ROUND(VAL2MON(NVL(NONSOGG,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE MRTOTIM1 WITH cp_ROUND(VAL2MON(NVL(MRTOTIM1,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE NETCORR WITH cp_ROUND(VAL2MON(NVL(NETCORR,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        REPLACE IMPPERC WITH cp_ROUND(VAL2MON(NVL(IMPPERC,0), this.w_CAMBIO, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATINI,this.oParentObject.w_CODVAL), this.oParentObject.w_DECIMI)
        ENDSCAN
        * --- Costruisco il cursore contenente i totali da riportare all'interno del report.
        SELECT Mrcodcon,Mrcodtri,Max(Mrcodval) as Mrcodval,Max(Anlocnas) as Anlocnas, Max(Anpronas) as Anpronas, ; 
 Max(Andatnas) as Andatnas,Max(Anlocali) as Anlocali,Max(Anprovin) as Anprovin, Max(An___cap) as An___cap,; 
 Max(Anindiri) as Anindiri,Max(Ancodfis) as Ancodfis,Sum(Somtota) as Somtota,; 
 Sum(Imponi) as Imponi, Sum(Nonsogg) as Nonsogg, Max(Mrperri1) as Mrperri1,; 
 Sum(Mrtotim1) as Mrtotim1, Sum(Netcorr) as Netcorr,Max(Trdestri) as Trdestri,; 
 Max(Ancoimps) as Ancoimps,Max(Ancoinps) as Ancoinps,; 
 Max(Anperfis) as Anperfis,Max(Andescri) as Andescri,Max(Ancognom) as Ancognom,Max(An__nome) An__nome,; 
 Max(Ddindiri) as Ddindiri,Max(Ddlocali) as Ddlocali,Max(Ddprovin) as Ddprovin,; 
 Max(Mrrifdi1) as Mrrifdi1,Sum(Impperc) as Impperc,Max(Trflacon) as Trflacon,; 
 Max(Mrcodtr2) as Mrcodtr2,Max(Anriinps) as Anriinps,; 
 Max("T") as Tipo,Max(Andescr2) as Andescr2,Max(Annazion) as Annazion,Max(Dd___Cap) as Dd___Cap,; 
 Max(Ddcodnaz) as Ddcodnaz,Max(Appoggio) as Appoggio,Max(Mrcaoval) as Mrcaoval,Max(Codice) as Codice,; 
 Max(Mrdatreg) as Mrdatreg,Max(Mrnumdoc) as Mrnumdoc,Max(Mrserdoc) as Mrserdoc,Max(Mrdatdoc) as Mrdatdoc ; 
 From Rite into cursor Totale Group by Mrcodcon,Mrcodtri Order by Mrcodcon,Mrcodtri
        * --- Creo un cursore unico da stampare
        Select * from Rite union Select * from Totale;
        into cursor Elabora order by 1,2,33,41
        * --- Controllo se l'utente ha selezionato il flag 'Stampa descrizione codice tributo', in questo
        *     caso devo lanciare una query che mi legga tutti i codice F24 con la descrizione associata ed inserirli
        *     in un cursore di appoggio (F24).
        *     Successivamente devo mettere in join i due cursori e prendere dal cursore F24 solo la descrizione
        vq_exec("..\RITE\EXE\QUERY\GSRI8SCR.VQR",this,"F24")
        if Reccount("F24")>0
          * --- Metto in join i cursori __TMP__ e F24 per riportare nel cursore di stampa la descrizione.
          SELECT DISTINCT ELABORA.*,F24.DESCF24 FROM ELABORA;
          LEFT OUTER JOIN F24 on ELABORA.MRCODTRI=F24.TRCODICE;
          ORDER BY 1,2,33,41 INTO CURSOR __TMP__ NOFILTER
        else
          Select Elabora
          Select *,space(100) as descf24 from ELABORA into cursor __TMP__ order by 1,2,33,41
        endif
        * --- Passo le seguenti variabili al report
        L_ANNO=this.oParentObject.w_ANNO
        L_CODVAL=this.oParentObject.w_CODVAL
        L_DECIMI=this.oParentObject.w_DECIMI
        L_DATINI=this.oParentObject.w_DATINI
        L_PERCFIN=this.oParentObject.w_PERCFIN
        L_DATFIN=this.oParentObject.w_DATFIN
        L_PERCIN=this.oParentObject.w_PERCIN
        L_STAMPA=this.oParentObject.w_STAMPA
        L_SIMVAL=this.oParentObject.w_SIMVAL
        L_DESCTRIB=this.oParentObject.w_DESCTRIB
        L_NONOTE=this.oParentObject.w_NOSTANOTE
        * --- Creo un nuovo cursore per poter stampare i dati del percipiente in una nuova
        *     pagina.
        SELECT DISTINCT MRCODCON,MAX(ANLOCALI) AS ANLOCALI,MAX(ANPROVIN) AS ANPROVIN,; 
 MAX(AN___CAP) AS AN___CAP,MAX(ANINDIRI) AS ANINDIRI,MAX(ANDESCRI) AS ANDESCRI,; 
 MAX(ANPERFIS) ANPERFIS, MAX(ANCOGNOM) AS ANCOGNOM,MAX(AN__NOME) AS AN__NOME,; 
 MAX(DDINDIRI) AS DDINDIRI,MAX(DDLOCALI) AS DDLOCALI,MAX(DDPROVIN) AS DDPROVIN,MAX(ANDESCR2) AS ANDESCR2,; 
 MAX(ANNAZION) AS ANNAZION,MAX(DD___CAP) AS DD___CAP,MAX(DDCODNAZ) AS DDCODNAZ FROM __TMP__ INTO CURSOR INTESTA GROUP BY MRCODCON ORDER BY MRCODCON
        SELECT __TMP__
        * --- Lancio il report di stampa
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI2SCR.FRX","",this.oParentObject)
    endcase
    * --- Controllo se esiste almeno un record nel cursore Intesta
    if RECCOUNT("INTESTA")>0
      if ah_YESNO("Si desidera stampare l'indirizzo dei percipienti?")
        * --- Valorizzo le variabili per gli indirizzi email e il numero di fax
        if this.oParentObject.w_PERCIN = this.oParentObject.w_PERCFIN and not empty(this.oParentObject.w_PERCIN)
          GSAR_BRM(this,"E_M","F",this.oParentObject.w_PERCIN,"RE","V")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Valorizzo le variabili globali per il Fax (per la selezione del numero di telefono)
          i_CLIFORDES = "F" 
 i_CODDES = this.oParentObject.w_PERCIN 
 i_TIPDES = "RE" 
 i_DEST=this.oParentObject.w_DESCINI
        endif
        * --- Assegnemento per poter stampare il logo aziendale
        L_LOGO=GETBMP("RITE"+I_CODAZI)
        SELECT * FROM INTESTA INTO CURSOR __TMP__ ORDER BY 1
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI_SCR_N.FRX","",this.oParentObject)
      endif
    endif
    if used("RITE")
      select ("RITE")
      USE
    endif
    if used("RITE1")
      select ("RITE1")
      USE
    endif
    if used("RITE2")
      select ("RITE2")
      USE
    endif
    if used("STAMPA")
      select ("STAMPA")
      USE
    endif
    if used("TOTALE")
      select ("TOTALE")
      USE
    endif
    if used("ELABORA")
      select ("ELABORA")
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DES_DIVE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
