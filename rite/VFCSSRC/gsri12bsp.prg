* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri12bsp                                                       *
*              STAMPA PDF  770/2015                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-19                                                      *
* Last revis.: 2015-06-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri12bsp",oParentObject)
return(i_retval)

define class tgsri12bsp as StdBatch
  * --- Local variables
  w_TOTRITACC = 0
  w_FIRDIC = space(1)
  w_FIRSG1 = space(1)
  w_FIRSG2 = space(1)
  w_FIRSG3 = space(1)
  w_FIRSG4 = space(1)
  w_FIRSG5 = space(1)
  w_FIRINT = space(1)
  w_FIRCAF = space(1)
  w_Firma_Dichiarazione = space(250)
  w_Firma_Soggetto1 = space(250)
  w_Firma_Soggetto2 = space(250)
  w_Firma_Soggetto3 = space(250)
  w_Firma_Soggetto4 = space(250)
  w_Firma_Soggetto5 = space(250)
  w_Firma_Intermediario = space(250)
  w_Firma_CAF = space(250)
  w_GTSEZ1ST = space(1)
  w_GTSEZ4ST = space(1)
  w_GTSEZ1SS = space(1)
  w_GTSEZ2SS = space(1)
  w_GTSEZ4SS = space(1)
  w_GTFIRDIC = space(1)
  w_GTFIRSG1 = space(1)
  w_GTFIRSG2 = space(1)
  w_GTFIRSG3 = space(1)
  w_GTFIRSG4 = space(1)
  w_GTFIRSG5 = space(1)
  w_GTFIRINT = space(1)
  w_GTFLGFIR = space(1)
  w_Modello = space(20)
  w_Resfdf = 0
  w_Quadro = 0
  w_GTEVEECC = space(1)
  w_GTCODFIS = space(16)
  w_GTCODSOS = space(16)
  w_STAMPAST = space(1)
  w_STAMPAH = space(1)
  w_STAMPAJ = space(1)
  w_STAMPAA = space(1)
  w_STAPDF = space(0)
  w_TIPO = space(1)
  w_GDSERIAL = space(10)
  w_TIPOPERAZ = space(1)
  w_ANNO = space(4)
  w_PERCIN = space(15)
  w_PERCFIN = space(15)
  w_RFDATFAL = ctod("  /  /  ")
  w_Quadro = space(1)
  w_Domfis = space(1)
  w_RFDATFAL = ctod("  /  /  ")
  w_REGAGE = space(1)
  w_CALCOLI = 0
  w_MESS = space(254)
  w_DATIPRES = .f.
  w_GDPROMOD = 0
  w_RESFDF = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  NAZIONI_idx=0
  GEFILTEL_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'elaborazione dei dati relativi al frontespizio del Mod. 770/2011, per la stampa su formato PDF
    * --- Creo il cursore __TMP__ contenente i dati presenti nel frontespizio, tali dati saranno elaborati dalla CP_FFDF
    * --- Read from GEFILTEL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.GEFILTEL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GEFILTEL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "GTSEZ1ST,GTSEZ4ST,GTSEZ1SS,GTSEZ2SS,GTSEZ4SS,GTFIRDIC,GTFIRSG1,GTFIRSG2,GTFIRSG3,GTFIRSG4,GTFIRSG5,GTFIRINT,GTCODFIS,GTCODSOS,GTEVEECC,GTRAPFAL,GTFLGFIR,GTDOMFIS"+;
        " from "+i_cTable+" GEFILTEL where ";
            +"GTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_GTSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        GTSEZ1ST,GTSEZ4ST,GTSEZ1SS,GTSEZ2SS,GTSEZ4SS,GTFIRDIC,GTFIRSG1,GTFIRSG2,GTFIRSG3,GTFIRSG4,GTFIRSG5,GTFIRINT,GTCODFIS,GTCODSOS,GTEVEECC,GTRAPFAL,GTFLGFIR,GTDOMFIS;
        from (i_cTable) where;
            GTSERIAL = this.oParentObject.w_GTSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_GTSEZ1ST = NVL(cp_ToDate(_read_.GTSEZ1ST),cp_NullValue(_read_.GTSEZ1ST))
      this.w_GTSEZ4ST = NVL(cp_ToDate(_read_.GTSEZ4ST),cp_NullValue(_read_.GTSEZ4ST))
      this.w_GTSEZ1SS = NVL(cp_ToDate(_read_.GTSEZ1SS),cp_NullValue(_read_.GTSEZ1SS))
      this.w_GTSEZ2SS = NVL(cp_ToDate(_read_.GTSEZ2SS),cp_NullValue(_read_.GTSEZ2SS))
      this.w_GTSEZ4SS = NVL(cp_ToDate(_read_.GTSEZ4SS),cp_NullValue(_read_.GTSEZ4SS))
      this.w_GTFIRDIC = NVL(cp_ToDate(_read_.GTFIRDIC),cp_NullValue(_read_.GTFIRDIC))
      this.w_GTFIRSG1 = NVL(cp_ToDate(_read_.GTFIRSG1),cp_NullValue(_read_.GTFIRSG1))
      this.w_GTFIRSG2 = NVL(cp_ToDate(_read_.GTFIRSG2),cp_NullValue(_read_.GTFIRSG2))
      this.w_GTFIRSG3 = NVL(cp_ToDate(_read_.GTFIRSG3),cp_NullValue(_read_.GTFIRSG3))
      this.w_GTFIRSG4 = NVL(cp_ToDate(_read_.GTFIRSG4),cp_NullValue(_read_.GTFIRSG4))
      this.w_GTFIRSG5 = NVL(cp_ToDate(_read_.GTFIRSG5),cp_NullValue(_read_.GTFIRSG5))
      this.w_GTFIRINT = NVL(cp_ToDate(_read_.GTFIRINT),cp_NullValue(_read_.GTFIRINT))
      this.w_GTCODFIS = NVL(cp_ToDate(_read_.GTCODFIS),cp_NullValue(_read_.GTCODFIS))
      this.w_GTCODSOS = NVL(cp_ToDate(_read_.GTCODSOS),cp_NullValue(_read_.GTCODSOS))
      this.w_GTEVEECC = NVL(cp_ToDate(_read_.GTEVEECC),cp_NullValue(_read_.GTEVEECC))
      this.w_RFDATFAL = NVL(cp_ToDate(_read_.GTRAPFAL),cp_NullValue(_read_.GTRAPFAL))
      this.w_GTFLGFIR = NVL(cp_ToDate(_read_.GTFLGFIR),cp_NullValue(_read_.GTFLGFIR))
      this.w_DOMFIS = NVL(cp_ToDate(_read_.GTDOMFIS),cp_NullValue(_read_.GTDOMFIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_STAMPAST = IIF(this.w_GTSEZ1ST="1" OR this.w_GTSEZ4ST="1","S","N")
    this.w_STAMPAH = "S"
    this.w_STAMPAJ = IIF(this.w_GTSEZ1SS="1" OR this.w_GTSEZ2SS="1" OR this.w_GTSEZ4SS="1","S","N")
    this.w_STAMPAA = "S"
    this.w_FIRDIC = IIF(this.w_GTFIRDIC="1","S","N")
    this.w_FIRSG1 = IIF(this.w_GTFIRSG1="1","S","N")
    this.w_FIRSG2 = IIF(this.w_GTFIRSG2="1","S","N")
    this.w_FIRSG3 = IIF(this.w_GTFIRSG3="1","S","N")
    this.w_FIRSG4 = IIF(this.w_GTFIRSG4="1","S","N")
    this.w_FIRSG5 = IIF(this.w_GTFIRSG5="1","S","N")
    this.w_FIRINT = IIF(this.w_GTFIRINT="1","S","N")
    this.w_FIRCAF = IIF(this.w_GTFLGFIR="1","S","N")
    * --- Chiedo se si vuole stampare anche i Quadri St,  H  e J del modello 770
    do GSRI_KGT with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_STAPDF = "ANTEPRIMA"
    if this.w_STAMPAA="S"
      Vq_Exec("..\Rite\Exe\Query\Gsri1Agt", this, "__TMP__")
      SELECT __TMP__
      DECLARE ARRFDF (1,2)
      ARRFDF (1,1) = "GTCODFED"
      ARRFDF (1,2) = 11
      this.w_Modello = "770Frontespizio15"
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Lancio i batches standard che creano i cursori
    this.w_STAPDF = "STAMPA"
    if this.w_STAMPAST = "S"
      * --- Stampo in formato PDF i prospetti del quadro ST
      this.w_TIPO = "V"
      this.w_GDSERIAL = this.oParentObject.w_GTSERIAL
      this.w_TIPOPERAZ = this.oParentObject.w_GTTIPOPE
      this.w_ANNO = this.oParentObject.w_GT__ANNO
      this.w_Modello = "Quadro_ST_15"
      do GSRI_BRT with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("RITE3") 
        SELECT * from RITE3 into cursor RITE3ORD order by GDPROMOD asc, ST__ANNO asc, ST__MESE asc, STCODTRI asc
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_STAMPAH = "S"
      * --- Stampo in formato PDF i prospetti del quadro H
      this.w_TIPO = "V"
      this.w_GDSERIAL = this.oParentObject.w_GTSERIAL
      this.w_TIPOPERAZ = this.oParentObject.w_GTTIPOPE
      this.w_ANNO = this.oParentObject.w_GT__ANNO
      this.w_Modello = "Quadro_H_15"
      this.w_Quadro = "H"
      do GSRI_BSH with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("RITE1") 
        SELECT * from RITE1 into cursor RITE1ORD order by GDPROMOD,RHCODCON ,RHCAUPRE,RHAUX006,RHSERIAL
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_STAMPAJ = "S"
      this.w_Modello = "Quadro_SS_15"
      if used("RITE1ORD") 
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Effettuiamo questo assegnamento perch� altrimenti i tasti funzione
    *     sarebbero disabilitati
    i_Curform=This.oParentObject
    * --- Cancello i cursori
    if used("RITE1")
      select RITE1
      use
    endif
    if used("RITE3")
      select RITE3
      use
    endif
    if used("RITE3ORD")
      select RITE3ORD
      use
    endif
    if used("RITE1ORD")
      select RITE1ORD
      use
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ritardo l'operazione successiva
    if type("g_PDFDELAY")="N"
      wait "" timeout g_PDFDELAY
    else
      wait "" timeout 5
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa prospetto ST in PDF
    DECLARE ARRFDF (2,2)
    ARRFDF (1,1) = "CODFIS"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "NUMMOD"
    ARRFDF (2,2) = 2
    this.w_DATIPRES = .F.
    * --- Creo il cursore dei dati
    L_CAMPI="CREATE CURSOR __TMP__ (CODFIS C(16),CODFISOS C(16), NUMMOD C(2), EVEECC C(1),"
    this.w_QUADRO = 0
    do while this.w_QUADRO < 13
      this.w_QUADRO = this.w_QUADRO+1
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM01 C(2), DI"+ALLTRIM(STR(this.w_QUADRO))+"STA01 C(4), "
      L_CAMPI=L_CAMPI+" DI"+ALLTRIM(STR(this.w_QUADRO))+"ST002 C(30), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST004 C(30), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST006 C(30), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST007 C(30), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST008 C(30), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST009 C(1), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST010 C(254), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST011 C(16), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST012 C(16), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STG14 C(2), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM14 C(2), DI"+ALLTRIM(STR(this.w_QUADRO))+"STA14 C(4), "
    enddo
    L_CAMPI=substr(L_CAMPI,1,len(L_CAMPI)-2)+")"
    &L_CAMPI
    select RITE3ORD
    Go Top
    this.w_QUADRO = 0
    L_CAMPI="INSERT INTO __TMP__ (CODFIS, CODFISOS, NUMMOD, EVEECC, "
    L_VALORI=' VALUES ("'+this.w_GTCODFIS+'","'+this.w_GTCODSOS+'", "01","'+iif(! empty(this.w_GTCODSOS) AND this.w_GTEVEECC<>"0",this.w_GTEVEECC," ")+'","'
    do while !eof()
      select RITE3ORD
      this.w_GDPROMOD = Nvl(RITE3ORD.Gdpromod,0)
      this.w_QUADRO = this.w_QUADRO+1
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM01, DI"+ALLTRIM(STR(this.w_QUADRO))+"STA01, "
      L_CAMPI= L_CAMPI+ "DI"+ALLTRIM(STR(this.w_QUADRO))+"ST002, "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST004, DI"+ALLTRIM(STR(this.w_QUADRO))+"ST006, "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST007,  DI"+ALLTRIM(STR(this.w_QUADRO))+"ST008, "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST009, DI"+ALLTRIM(STR(this.w_QUADRO))+"ST010, "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST011, DI"+ALLTRIM(STR(this.w_QUADRO))+"ST012, "
      L_CAMPI=L_CAMPI+ "DI"+ALLTRIM(STR(this.w_QUADRO))+"STG14, "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM14, DI"+ALLTRIM(STR(this.w_QUADRO))+"STA14, "
      if this.w_quadro=1
        L_VALORI=L_VALORI+right("00"+alltrim(str(ST__MESE)),2)+'",'
        L_VALORI=L_VALORI+'"'+str(ST__ANNO ,4,0)+'"'
      else
        L_VALORI=L_VALORI+'"'+right("00"+alltrim(str(ST__MESE)),2)+'",'
        L_VALORI=L_VALORI+'"'+str(ST__ANNO ,4,0)+'"'
      endif
      L_VALORI=L_VALORI+", "+'"'+IIF(STRITOPE<=0," ",ALLTRIM(STR(STRITOPE,18,4)))+'"'+", "+'"'+IIF(STRITECC<=0 ," ",ALLTRIM(STR(STRITECC,18,4)))+'"'+", "+'"'+IIF(STRITCOM<=0 ," ",ALLTRIM(STR(STRITCOM,18,4)))+'"'
      L_VALORI = L_VALORI +"," +'"'+IIF(STIMPVER<=0," ",ALLTRIM(STR(STIMPVER,18,4)))+'"'+","+'"'+IIF(STIMPINT<=0," ",ALLTRIM(STR(STIMPINT,18,4)))+'"'
      if STRAVOPE = "S"
        L_VALORI=L_VALORI+", "+'"'+"X"+'"'
      else
        L_VALORI=L_VALORI+", "+'" "'
      endif
      L_VALORI = L_VALORI+","+'"'+ALLTRIM(ST__NOTE)+'","'+ALLTRIM(STCODTRI)+'"'
      * --- Tesoreria
      if STTIPVER = "S"
        L_VALORI=L_VALORI+", "+'"'+"X"+'"'
      else
        L_VALORI=L_VALORI+", "+'" "'
      endif
      * --- Campo 14
      if ! empty(nvl(STDATVER,""))
        L_VALORI=L_VALORI+", "+'"'+right("00"+alltrim(str(day(STDATVER))),2)+'"'
        L_VALORI=L_VALORI+", "+'"'+right("00"+alltrim(str(month(STDATVER))),2)+'"'
        L_VALORI=L_VALORI+", "+'"'+str(year(STDATVER),4,0)+'", '
      else
        L_VALORI=L_VALORI+", "+'"  "'
        L_VALORI=L_VALORI+", "+'"  "'
        L_VALORI=L_VALORI+", "+'"  "'+", "
      endif
      * --- Incremento il numero di Rigo
      skip in RITE3ORD
      if this.w_GDPROMOD<>Nvl(RITE3ORD.Gdpromod,0)
        * --- Incremento il numero di Modello
        L_MACRO=substr(L_CAMPI,1,len(L_CAMPI)-2)+")"+substr(L_VALORI,1,len(L_VALORI)-2)+")"
        &L_MACRO
        this.w_QUADRO = 0
        L_CAMPI="INSERT INTO __TMP__ (CODFIS, CODFISOS, NUMMOD, EVEECC, "
        L_VALORI=' VALUES ("'+this.w_GTCODFIS+'", "'+this.w_GTCODSOS+'", "'+RIGHT("00"+ALLTRIM(STR(Nvl(RITE3ORD.Gdpromod,0),2,0)),2)+'","'+iif(! empty(this.w_GTCODSOS),this.w_GTEVEECC," ")+'","'
        this.w_DATIPRES = .T.
      endif
    enddo
    if this.w_QUADRO>0
      L_MACRO=substr(L_CAMPI,1,len(L_CAMPI)-2)+")"+substr(L_VALORI,1,len(L_VALORI)-2)+")"
      &L_MACRO
      this.w_DATIPRES = .T.
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa  in formato PDF del quadro H
    * --- Se i controlli non restituiscono nessun errore proseguo
    *     con l'elaborazione
    DECLARE ARRFDF (2,2)
    ARRFDF (1,1) = "CODFIS"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "NUMMOD"
    ARRFDF (2,2) = 8
    * --- Creo il cursore dei dati
    L_CAMPI="CREATE CURSOR __TMP__ (CODFIS C(16), CODFISOS C(16), NUMMOD C(8), AU001 C(16), AU002 C(50), AU003 C(20), "
    L_CAMPI = L_CAMPI + "AU004 C(1), AU005G C(2), AU005M C(2), AU005A C(4), AU006 C(50), AU007 C(2),  AU008 C(2), AU009 C(1), "
    L_CAMPI = L_CAMPI +"AU020 C(30) , AU021 C(2), AU022 C(4), "
    L_CAMPI = L_CAMPI + "AU040 C(16), AU041 C(30),AU042 C(35), AU043 C(3), "
    L_CAMPI = L_CAMPI + "BU001 C(2), BU002 C(4), BU004 C(30), BU005 C(30) ,"
    L_CAMPI = L_CAMPI + "BU006 C(1), BU007 C(30), BU008 C(30), BU009 C(30),"
    L_CAMPI = L_CAMPI + "BU020 C(30), BU021 C(30), BU022 C(30), BU023 C(30), BU024 C(30), BU025 C(30) )"
    &L_CAMPI
    Select RITE1ORD 
 Go Top
    do while !eof()
      * --- Record H
      L_CAMPI="INSERT INTO  __TMP__ (CODFIS , CODFISOS, NUMMOD , AU001 , AU002 , AU003 , "
      L_CAMPI = L_CAMPI + "AU004 , AU005G , AU005M , AU005A , AU006 , AU007 , AU008 , AU009 ,AU020, "
      L_CAMPI = L_CAMPI +"AU021 , AU022 , AU040,AU041,AU042,AU043,BU001, BU002 , BU004 , BU005 ,"
      L_CAMPI = L_CAMPI +"BU006 , BU007 , BU008 , BU009 ,BU020 , BU021 , BU022 , BU023 , BU024 , BU025 )"
      select RITE1ORD
      L_VALORI=' VALUES ("'+this.w_GTCODFIS+'", "'+this.w_GTCODSOS+'", "'+RIGHT(REPL("0",8)+ALLTRIM(STR(Gdpromod)),8)+'",'
      * --- 'A01'
      L_VALORI = L_VALORI +' "'+iif(Nvl(RITE1ord.Rhperest," ")<>"S",Upper(RITE1ord.Rhcodfis),Space(16))+'",'
      * --- 'A02'
      L_VALORI = L_VALORI +' "'+iif(RITE1ord.Rhperfis="S", UPPER(RITE1ord.Rhcognom),UPPER(RITE1ord.Rhdescri))+'",'
      * --- 'A03'
      L_VALORI = L_VALORI +' "'+iif(RITE1ord.Rhperfis="S",UPPER(NVL(RITE1ord.Rh__Nome," "))," ")+'",'
      * --- 'A04'
      L_VALORI = L_VALORI +' "'+iif(RITE1ord.Rhperfis="S",RITE1ord.Rh_Sesso," ")+'",'
      * --- 'A05'
      L_VALORI = L_VALORI + '"'+iif(RITE1ord.Rhperfis="S" AND NOT EMPTY (RHDATNAS),right ( "00"+ALLTRIM(NVL(STR(DAY(RHDATNAS))," ")),2),"  ")+'",'
      L_VALORI = L_VALORI + '"'+iif(RITE1ord.Rhperfis="S" AND NOT EMPTY (RHDATNAS), RIGHT ("00"+ALLTRIM(NVL(STR(MONTH(RHDATNAS))," ")),2),"  ")+'",'
      L_VALORI = L_VALORI + '"'+iif(RITE1ord.Rhperfis="S" ,ALLTRIM(NVL(STR(YEAR(RHDATNAS))," ")),"  ")+'",'
      * --- 'A06'
      L_VALORI = L_VALORI +' "'+iif(RITE1ord.Rhperfis="S", UPPER(NVL(RITE1ord.Rhcomest," "))," ")+'",'
      * --- 'A07'
      L_VALORI = L_VALORI +' "'+iif(RITE1ord.Rhperfis="S",UPPER(NVL(RITE1ord.Rhpronas," "))," ")+'",'
      * --- 'A08'
      L_VALORI = L_VALORI +' "'+Nvl(Rhcatpar,"  ")+'",'
      * --- 'A09'
      if Nvl(Rheveecc,"0")<> "0" 
        L_VALORI = L_VALORI +' "'+alltrim(NVL(Rheveecc," "))+'",'
      else
        L_VALORI = L_VALORI +' " ",'
      endif
      * --- 'A20'
      L_VALORI = L_VALORI +' "'+iif(Nvl(RITE1ord.Rhperest," ")<>"S",UPPER(Nvl(Rhcomune,"  "))," ")+'",'
      * --- 'A21'
      L_VALORI = L_VALORI +' "'+iif(Nvl(RITE1ord.Rhperest," ")<>"S",UPPER(Nvl(Rhprovin,"  "))," ")+'",'
      * --- 'A22'
      L_VALORI = L_VALORI +' "'+iif(Nvl(RITE1ord.Rhperest," ")<>"S" And Alltrim(Nvl(Rhcaupre," "))="N",UPPER(Nvl(Rhcodcom,"  "))," ")+'",'
      if Nvl(RITE1ord.Rhperest," ")="S"
        * --- 'A40'
        L_VALORI = L_VALORI +' "'+UPPER(NVL(RITE1ord.Rhcofisc," "))+'",'
        * --- 'A41'
        L_VALORI = L_VALORI +' "'+UPPER(Nvl(Rhcitest," "))+'",'
        * --- 'A42'
        L_VALORI = L_VALORI +' "'+UPPER(Nvl(Rhindest," "))+'",'
        * --- 'A43'
        L_VALORI = L_VALORI +' "'+Right(Repl("0",3)+Alltrim(UPPER(Nvl(Rhstaest," "))),3)+'",'
      else
        L_VALORI = L_VALORI +' " ",'
        L_VALORI = L_VALORI +' " ",'
        L_VALORI = L_VALORI +' " ",'
        L_VALORI = L_VALORI +' " ",'
      endif
      * --- '001'
      L_VALORI = L_VALORI +' "'+UPPER(Nvl(Rhcaupre," "))+'",'
      * --- '002'
      L_VALORI = L_VALORI +' "'+iif(Alltrim(Nvl(Rhcaupre," ")) $ "G-H-I",Padl(Alltrim(Str(this.w_Anno)),4),Space(4))+'",'
      * --- '004'
      L_VALORI = L_VALORI +' "'+iif(Nvl(Rhaux004,0) <= 0," ",alltrim(STR(Nvl(Rhaux004,0),18,2)))+'",'
      if Nvl(RITE1ord.Rhperest," ")="S" And Nvl(RITE1ord.Anflsgre," ")="S"
        * --- '005'
        L_VALORI = L_VALORI +' "'+iif(Nvl(Rhaux005,0) <= 0," ",alltrim(STR(Nvl(Rhaux005,0),18,2)))+'",'
        L_VALORI = L_VALORI +' " ",'
        L_VALORI = L_VALORI +' " ",'
      else
        L_VALORI = L_VALORI +' " ",'
        * --- '006'
        if Nvl(Rhaux006," ") $ "123"
          L_VALORI = L_VALORI +' "'+NVL(Rhaux006," ")+'",'
        else
          L_VALORI = L_VALORI +' " ",'
        endif
        L_VALORI = L_VALORI +' "'+iif(Nvl(Rhaux005,0) <= 0," ",alltrim(STR(Nvl(Rhaux005,0),18,2)))+'",'
      endif
      * --- '008'
      L_VALORI = L_VALORI +' "'+iif(Nvl(Rhaux008,0) <= 0," ",alltrim(STR(Nvl(Rhaux008,0),18,2)))+'",'
      * --- '009'
      L_VALORI = L_VALORI +' "'+iif(Nvl(Rhaux009,0) <= 0," ",alltrim(STR(Nvl(Rhaux009,0),18,2)))+'",'
      * --- '020'
      L_VALORI = L_VALORI +' "'+iif(Nvl(Rhaux020,0) <= 0," ",alltrim(STR(Nvl(Rhaux020,0),18,2)))+'",'
      * --- '021'
      L_VALORI = L_VALORI +' "'+iif(Nvl(Rhaux021,0) <= 0," ",alltrim(STR(Nvl(Rhaux021,0),18,2)))+'",'
      * --- '022'
      L_VALORI = L_VALORI +' "'+iif(Nvl(Rhaux022,0) <= 0," ",alltrim(STR(Nvl(Rhaux022,0),18,2)))+'",'
      * --- '023'
      L_VALORI = L_VALORI +' "'+iif(Nvl(Rhaux023,0) <= 0," ",alltrim(STR(Nvl(Rhaux023,0),18,2)))+'",'
      * --- '024'
      L_VALORI = L_VALORI +' "'+iif(Nvl(Imprefall,0) <= 0," ",STR(Nvl(Imprefall,0),18,2))+'",'
      * --- '025'
      L_VALORI = L_VALORI +' "'+iif(Nvl(Imposfall,0) <= 0," ",STR(Nvl(Imposfall,0),18,2))+'"'+")"
      Select RITE1ORD
      L_MACRO = L_CAMPI+L_VALORI
      &L_MACRO
      Select RITE1ORD
      skip in RITE1ORD
    enddo
    * --- Stampa quadro H attuale
    Select RITE1ORD
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Create cursor __TMP__ (CODFIS C(16), CODFISOS C(16), NUMMOD C(8),TOTRITAC C(30),CODFISCALE C(16))
    DECLARE ARRFDF (2,2)
    ARRFDF (1,1) = "CODFIS"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "NUMMOD"
    ARRFDF (2,2) = 8
    select RITE1ORD
    * --- Calcolo la somma delle ritenute a titolo di acconto
    SUM(RHAUX009) TO this.w_TOTRITACC
    Insert into __TMP__ values (this.w_GTCODFIS, this.w_GTCODSOS, "01", ALLTRIM(STR(this.w_TOTRITACC,18,4)),this.w_GTCODFIS)
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Select __TMP__
    this.w_Resfdf = cp_ffdf(" ",alltrim(this.w_Modello)+".pdf",this," ",@ARRFDF)
    * --- Se non mi ritorna un numero significa che ho incontrato un errore
    do case
      case Not Left( this.w_Resfdf , 1) $ "0123456789"
        * --- Errore durante creazione di un FDF..
        this.w_Mess = this.w_Resfdf
        ah_ErrorMsg(this.w_Mess,)
      case Left( this.w_RESFDF , 1) = "0"
        * --- Nessun FDF creato...
        this.w_MESS = "Nessun file PDF da generare."
        ah_ErrorMsg(this.w_Mess)
      otherwise
        this.w_Quadro = 1
        do while this.w_Quadro<=Val( this.w_Resfdf )
          if this.w_STAPDF="ANTEPRIMA"
            * --- Anteprima
            Cp_Chprn(tempadhoc()+"\"+alltrim(this.w_Modello)+Alltrim(Str(this.w_Quadro))+".Fdf")
          else
            * --- Stampa
            Cp_Chprn(tempadhoc()+"\"+alltrim(this.w_Modello)+Alltrim(Str(this.w_Quadro))+".Fdf",.NULL.,2)
          endif
          * --- Ritardo l'operazione successiva
          if type("g_PDFDELAY")="N"
            wait "" timeout g_PDFDELAY
          else
            wait "" timeout 5
          endif
          this.w_Quadro = this.w_Quadro+1
        enddo
    endcase
    if used("__TMP__")
      select __TMP__ 
 use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DES_DIVE'
    this.cWorkTables[4]='NAZIONI'
    this.cWorkTables[5]='GEFILTEL'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
