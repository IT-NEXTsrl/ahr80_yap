* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri1bsp                                                        *
*              Frontespizio 770/2003                                           *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_74]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-19                                                      *
* Last revis.: 2006-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri1bsp",oParentObject)
return(i_retval)

define class tgsri1bsp as StdBatch
  * --- Local variables
  w_EMAIL = space(100)
  w_PHONE = space(12)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'elaborazione dei dati relativi al frontespizio del Mod. 770/2003, per la stampa su formato PDF
    * --- Creo il cursore __TMP__ contenente i dati presenti nel frontespizio, tali dati saranno elaborati dalla CP_FFDF
    * --- Sez. I
    * --- Sez. II
    * --- Fine Sez. II
    this.w_PHONE = iif(empty(this.oParentObject.w_TELEFONO),this.oParentObject.w_SETELEFONO,this.oParentObject.w_TELEFONO)
    CREATE CURSOR __TMP__ (COGDEN C(60), NOME C(20), CODFIS C(16), CODFISC C(16), CORRET C(1), INTEGRA C(1), EVEECC C(1), ; 
 CODEATT C(5), TELNUM C(12), FAXPREF C(4), FAXNUM C(8), EMAIL C(100), PFCOMUNE C(40), PFPROV C(2), ; 
 PFDATNAS1 C(2), PFDATNAS2 C(2), PFDATNAS3 C(4), PFMAS C(1), PFFEM C(1), PFCOMRES C(40), PFPRORES C(2), ; 
 PFVIARES C(35), PFCAPRES C(5), PFDATVAR1 C(2), PFDATVAR2 C(2), PFDATVAR3 C(4), ASSEDLEG1 C(2), ASSEDLEG2 C(4), ; 
 ASCOMUNE C(40), ASPROV C(2), ASVIA C(35), ASCAP C(5), ASDOMFIS1 C(2), ASDOMFIS2 C(4), ASCOMFIS C(40), ASPROFIS C(2), ; 
 ASVIAFIS C(35), ASCAPFIS C(5), STATO C(1), NATGIU C(2), SITUAZ C(1), CFDICAPP C(11), RFCODFIS C(16), CODCAR C(2), ; 
 RFCOG C(24), RFNOM C(20), RFSEXM C(1), RFSEXF C(1), RFDATNAS1 C(2), RFDATNAS2 C(2), RFDATNAS3 C(4), RFCOMNAS C(40), ; 
 RFPRONAS C(2), RFCOMUNE C(40), RFPROV C(2), RFCAP C(5), RFVIA C(35), RFNUM C(12), NUMCOM N(8) , ; 
 ST C(1), SX C(1),ST2 C(1),SX2 C(1),RFCFINS2 C(16),NUMCOM2 N(8) ,CFINTER C(16), ; 
 NUMCAF C(5), IMPTRAS C(1), IMPTEL C(1), DATIMP1 C(2), DATIMP2 C(2), DATIMP3 C(4),CFRESCAF C(16), ART35 C(1))
    DECLARE ARRFDF (1,2)
    ARRFDF (1,1) = "CODFIS"
    * --- Riempio l'Array ARRFDF con i nomi dei campi che nel Modello PDF sono rappresentati come N, N>1, celle distinte.
    ARRFDF (1,2) = 16
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZ_EMAIL"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZ_EMAIL;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_EMAIL = NVL(cp_ToDate(_read_.AZ_EMAIL),cp_NullValue(_read_.AZ_EMAIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Memorizzo i dati nel cursore __TMP__
    INSERT INTO __TMP__ VALUES(iif (empty(this.oParentObject.w_FODENOMINA),upper(this.oParentObject.w_FOCOGNOME),upper(this.oParentObject.w_FODENOMINA)),upper(this.oParentObject.w_FONOME),this.oParentObject.w_FOCODFIS,this.oParentObject.w_FOCODFIS, ; 
 iif(this.oParentObject.w_FLAGCOR="1","X",""),iif(this.oParentObject.w_FLAGINT="1","X",""), iif(this.oParentObject.w_FLAGEVE="01","X",""),iif(empty(this.oParentObject.w_SECODATT),this.oParentObject.w_CODATT,this.oParentObject.w_SECODATT),; 
 this.w_PHONE, space(4),space(8),upper(this.w_EMAIL),upper(this.oParentObject.w_COMUNE),upper(this.oParentObject.w_SIGLA), ; 
 iif(empty(this.oParentObject.w_DATANASC),"",right("0"+alltrim(str(day(this.oParentObject.w_DATANASC))),2)),iif(empty(this.oParentObject.w_DATANASC),"",right("0"+alltrim(str(month(this.oParentObject.w_DATANASC))),2)), ; 
 iif(empty(this.oParentObject.w_DATANASC),"",alltrim(str(year(this.oParentObject.w_DATANASC)))), iif(this.oParentObject.w_SESSO="M","X",""),iif(this.oParentObject.w_SESSO="F","X",""),upper(this.oParentObject.w_RECOMUNE),upper(this.oParentObject.w_RESIGLA),upper(this.oParentObject.w_INDIRIZ),this.oParentObject.w_CAP, ; 
 iif(empty(this.oParentObject.w_VARRES),"",right("0"+alltrim(str(day(this.oParentObject.w_VARRES))),2)),iif(empty(this.oParentObject.w_VARRES),"",right("0"+alltrim(str(month(this.oParentObject.w_VARRES))),2)), ; 
 iif(empty(this.oParentObject.w_VARRES),"",alltrim(str(year(this.oParentObject.w_VARRES)))), iif(empty(this.oParentObject.w_SEVARSED),"",right("0"+alltrim(str(month(this.oParentObject.w_SEVARSED))),2)), ; 
 iif(empty(this.oParentObject.w_SEVARSED),"",alltrim(str(year(this.oParentObject.w_SEVARSED)))), upper(this.oParentObject.w_SECOMUNE),upper(this.oParentObject.w_SESIGLA),upper(this.oParentObject.w_SEINDIRI2),this.oParentObject.w_SECAP, ; 
 iif(empty(this.oParentObject.w_SEVARDOM),"",right("0"+alltrim(str(month(this.oParentObject.w_SEVARDOM))),2)),iif(empty(this.oParentObject.w_SEVARDOM),"",alltrim(str(year(this.oParentObject.w_SEVARDOM)))),upper(this.oParentObject.w_SERCOMUN), ; 
 upper(this.oParentObject.w_SERSIGLA),upper(this.oParentObject.w_SERINDIR),this.oParentObject.w_SERCAP, this.oParentObject.w_STATO,iif(empty(this.oParentObject.w_NATGIU),"  ",right("00"+alltrim(this.oParentObject.w_NATGIU),2)),this.oParentObject.w_SITUAZ,this.oParentObject.w_CODFISDA,this.oParentObject.w_RFCODFIS,iif(empty(this.oParentObject.w_RFCODCAR),"  ",right("00"+alltrim(this.oParentObject.w_RFCODCAR),2)), ; 
 upper(this.oParentObject.w_RFCOGNOME),upper(this.oParentObject.w_RFNOME), iif(this.oParentObject.w_RFSESSO="M","X",""),iif(this.oParentObject.w_RFSESSO="F","X",""),iif(empty(this.oParentObject.w_RFDATANASC),"",right("0"+alltrim(str(day(this.oParentObject.w_RFDATANASC))),2)), ; 
 iif(empty(this.oParentObject.w_RFDATANASC),"",right("0"+alltrim(str(month(this.oParentObject.w_RFDATANASC))),2)),iif(empty(this.oParentObject.w_RFDATANASC),"",alltrim(str(year(this.oParentObject.w_RFDATANASC)))), ; 
 upper(this.oParentObject.w_RFCOMNAS),upper(this.oParentObject.w_RFSIGNAS),upper(this.oParentObject.w_RFCOMUNE),upper(this.oParentObject.w_RFSIGLA), this.oParentObject.w_RFCAP,upper(this.oParentObject.w_RFINDIRIZ), ; 
 this.oParentObject.w_RFTELEFONO,iif(this.oParentObject.w_SEZ1="1",this.oParentObject.w_NUMCERTIF2,0), ; 
 iif(this.oParentObject.w_qST1="1","X",""),iif(this.oParentObject.w_qSX1="1","X",""),iif(this.oParentObject.w_qST12="1","X",""),iif(this.oParentObject.w_qSX12="1","X",""),this.oParentObject.w_RFCFINS2,iif(this.oParentObject.w_SEZ2="1" ,this.oParentObject.w_NUMCERTIF2,0),this.oParentObject.w_CODFISIN, ; 
 iif(this.oParentObject.w_NUMCAF=0,"",str(this.oParentObject.w_NUMCAF,5)),iif(this.oParentObject.w_IMPTRTEL="1","X",""),iif(this.oParentObject.w_IMPTRSOG="1","X",""), ; 
 iif(empty(this.oParentObject.w_RFDATIMP),"",right("0"+alltrim(str(day(this.oParentObject.w_RFDATIMP))),2)),iif(empty(this.oParentObject.w_RFDATIMP),"",right("0"+alltrim(str(month(this.oParentObject.w_RFDATIMP))),2)), ; 
 iif(empty(this.oParentObject.w_RFDATIMP),"",alltrim(str(year(this.oParentObject.w_RFDATIMP)))),this.oParentObject.w_CFRESCAF,iif(this.oParentObject.w_VISTOA35="1","X","")) 
 
    do case
      case year(this.oParentObject.w_DATFIN)=2002
        result1=cp_ffdf(" ","770Frontespizio03.pdf",this," ",@ARRFDF)
        CP_CHPRN(tempadhoc()+"\770Frontespizio031.FDF","",this.oParentObject)
      case year(this.oParentObject.w_DATFIN)=2003
        result1=cp_ffdf(" ","770Frontespizio04.pdf",this," ",@ARRFDF)
        CP_CHPRN(tempadhoc()+"\770Frontespizio041.FDF","",this.oParentObject)
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
