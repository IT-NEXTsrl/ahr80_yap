* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bdk                                                        *
*              Elimina la distinta IRPEF/INPS                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_6]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2006-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bdk",oParentObject,m.pOpz)
return(i_retval)

define class tgsri_bdk as StdBatch
  * --- Local variables
  pOpz = space(10)
  w_OK = .f.
  w_MESS = space(10)
  w_SERIAL = space(10)
  * --- WorkFile variables
  MOV_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione, Variazione Distinta: Controlli Preliminari (Lanciato da GSRI_AVP e GSRI_AVN) Evento: Delete Start
    * --- INPS o IRPEF
    this.w_OK = .T.
    this.w_MESS = ah_msgformat("Transazione abbandonata!")
    if NOT EMPTY(this.oParentObject.w_VPSERIAL)
      if this.oParentObject.w_VPSTATUS="D"
        * --- Distinta Definitiva
        this.w_OK = ah_YesNo("Attenzione%0distinta definitiva; confermi ugualmente?")
      endif
      if this.w_OK=.T.
        * --- Cancellazione dei Movimenti ritenute associati
        ah_Msg("Elimino riferimento distinta %1 in archivio ritenute...",.t.,.f.,.f.,this.pOpz)
        if this.pOpz="IRPEF"
          * --- Select from MOV_RITE
          i_nConn=i_TableProp[this.MOV_RITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2],.t.,this.MOV_RITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MRSERIAL  from "+i_cTable+" MOV_RITE ";
                +" where MRRIFDI1="+cp_ToStrODBC(this.oParentObject.w_VPSERIAL)+"";
                 ,"_Curs_MOV_RITE")
          else
            select MRSERIAL from (i_cTable);
             where MRRIFDI1=this.oParentObject.w_VPSERIAL;
              into cursor _Curs_MOV_RITE
          endif
          if used('_Curs_MOV_RITE')
            select _Curs_MOV_RITE
            locate for 1=1
            do while not(eof())
            this.w_SERIAL = _Curs_MOV_RITE.MRSERIAL
            if NOT EMPTY(NVL(this.w_SERIAL," "))
              * --- Try
              local bErr_035E6E10
              bErr_035E6E10=bTrsErr
              this.Try_035E6E10()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- Possono anche non esserci movimenti
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_035E6E10
              * --- End
            endif
              select _Curs_MOV_RITE
              continue
            enddo
            use
          endif
        else
          * --- Select from MOV_RITE
          i_nConn=i_TableProp[this.MOV_RITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2],.t.,this.MOV_RITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MRSERIAL  from "+i_cTable+" MOV_RITE ";
                +" where MRRIFDI2="+cp_ToStrODBC(this.oParentObject.w_VPSERIAL)+"";
                 ,"_Curs_MOV_RITE")
          else
            select MRSERIAL from (i_cTable);
             where MRRIFDI2=this.oParentObject.w_VPSERIAL;
              into cursor _Curs_MOV_RITE
          endif
          if used('_Curs_MOV_RITE')
            select _Curs_MOV_RITE
            locate for 1=1
            do while not(eof())
            this.w_SERIAL = _Curs_MOV_RITE.MRSERIAL
            if NOT EMPTY(NVL(this.w_SERIAL," "))
              * --- Try
              local bErr_035E7B60
              bErr_035E7B60=bTrsErr
              this.Try_035E7B60()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- Possono anche non esserci movimenti
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_035E7B60
              * --- End
            endif
              select _Curs_MOV_RITE
              continue
            enddo
            use
          endif
        endif
        WAIT CLEAR
      endif
      if this.w_OK = .F.
        * --- Abbandona la Transazione
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    endif
  endproc
  proc Try_035E6E10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MOV_RITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOV_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRIFDI1 ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'MOV_RITE','MRRIFDI1');
          +i_ccchkf ;
      +" where ";
          +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MRRIFDI1 = SPACE(10);
          &i_ccchkf. ;
       where;
          MRSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_035E7B60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MOV_RITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOV_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRIFDI2 ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'MOV_RITE','MRRIFDI2');
          +i_ccchkf ;
      +" where ";
          +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MRRIFDI2 = SPACE(10);
          &i_ccchkf. ;
       where;
          MRSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_RITE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_MOV_RITE')
      use in _Curs_MOV_RITE
    endif
    if used('_Curs_MOV_RITE')
      use in _Curs_MOV_RITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
