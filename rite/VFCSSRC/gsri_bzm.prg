* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bzm                                                        *
*              Aggiorna dati certificazione                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-27                                                      *
* Last revis.: 2011-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bzm",oParentObject,m.pPARAM)
return(i_retval)

define class tgsri_bzm as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_MRSERIAL = space(10)
  w_DATPAG = ctod("  /  /  ")
  w_DATCER = ctod("  /  /  ")
  w_STACER = space(1)
  w_PROG = .NULL.
  w_OK = .f.
  * --- WorkFile variables
  MOV_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da GSRI_SZM
    this.w_OK = .F.
    if this.pPARAM="B"
       
 Select (this.oParentObject.w_ZOOMRIT.cCursor) 
 Go top 
 Scan for XCHK=1
      this.w_MRSERIAL = MRSERIAL
      this.w_DATPAG = IIF(Empty(this.oParentObject.w_MRDATPAG),MRDATPAG,this.oParentObject.w_MRDATPAG)
      this.w_DATCER = IIF(Empty(this.oParentObject.w_MRDATCER),MRDATCER,this.oParentObject.w_MRDATCER)
      this.w_STACER = IIF(Empty(this.oParentObject.w_MRSTACER),Nvl(MRSTACER,"N"),this.oParentObject.w_MRSTACER)
      * --- Try
      local bErr_03751F58
      bErr_03751F58=bTrsErr
      this.Try_03751F58()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03751F58
      * --- End
      this.w_OK = .T.
       
 Endscan
      this.oparentobject.notifyevent("Ricerca")
      if !this.w_OK
        ah_ErrorMsg("Non ci sono movimenti selezionati")
      endif
    else
       
 Select (this.oParentObject.w_ZOOMRIT.cCursor)
      this.w_MRSERIAL = MRSERIAL
      * --- Movimenti ritenute
      this.w_PROG = GSRI_AMR()
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_PROG.bSec1)
        Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
        i_retcode = 'stop'
        return
      endif
      this.w_PROG.EcpFilter()     
      this.w_PROG.w_MRSERIAL = this.w_MRSERIAL
      this.w_PROG.EcpSave()     
    endif
  endproc
  proc Try_03751F58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MOV_RITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOV_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRSTACER ="+cp_NullLink(cp_ToStrODBC(this.w_STACER),'MOV_RITE','MRSTACER');
      +",MRDATCER ="+cp_NullLink(cp_ToStrODBC(this.w_DATCER),'MOV_RITE','MRDATCER');
      +",MRDATPAG ="+cp_NullLink(cp_ToStrODBC(this.w_DATPAG),'MOV_RITE','MRDATPAG');
          +i_ccchkf ;
      +" where ";
          +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
             )
    else
      update (i_cTable) set;
          MRSTACER = this.w_STACER;
          ,MRDATCER = this.w_DATCER;
          ,MRDATPAG = this.w_DATPAG;
          &i_ccchkf. ;
       where;
          MRSERIAL = this.w_MRSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_RITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
