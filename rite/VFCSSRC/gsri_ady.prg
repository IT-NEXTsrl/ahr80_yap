* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_ady                                                        *
*              Dati estratti record SY                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2018-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_ady"))

* --- Class definition
define class tgsri_ady as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 856
  Height = 532+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-09-20"
  HelpContextID=92927593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=53

  * --- Constant Properties
  DATESTSY_IDX = 0
  CONTI_IDX = 0
  ENTI_COM_IDX = 0
  NAZIONI_IDX = 0
  GEDETTEL_IDX = 0
  cFile = "DATESTSY"
  cKeySelect = "SYSERIAL"
  cKeyWhere  = "SYSERIAL=this.w_SYSERIAL"
  cKeyWhereODBC = '"SYSERIAL="+cp_ToStrODBC(this.w_SYSERIAL)';

  cKeyWhereODBCqualified = '"DATESTSY.SYSERIAL="+cp_ToStrODBC(this.w_SYSERIAL)';

  cPrg = "gsri_ady"
  cComment = "Dati estratti record SY"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SYSERIAL = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_SY__ANNO = 0
  o_SY__ANNO = 0
  w_SYTIPCON = space(1)
  w_SYCODCON = space(15)
  o_SYCODCON = space(15)
  w_ANCAURIT = space(2)
  w_ANCODFIS = space(16)
  w_ANPERFIS = space(1)
  w_ANDESCRI = space(40)
  w_ANCOGNOM = space(40)
  w_AN__NOME = space(20)
  w_AN_SESSO = space(1)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANFLGEST = space(1)
  w_ANCOFISC = space(25)
  w_ANINDIRI = space(35)
  w_ANNAZION = space(3)
  w_NACODEST = space(5)
  w_ANRITENU = space(1)
  w_ANFLSGRE = space(1)
  w_SYPERFIS = space(1)
  o_SYPERFIS = space(1)
  w_SYPEREST = space(1)
  o_SYPEREST = space(1)
  w_CAUPRE = space(2)
  o_CAUPRE = space(2)
  w_CAUPRE_2016 = space(2)
  o_CAUPRE_2016 = space(2)
  w_CAUPRE_2017 = space(2)
  o_CAUPRE_2017 = space(2)
  w_SYCAUPRE = space(2)
  w_SYSCLGEN = space(1)
  w_SYDESCRI = space(60)
  w_SYCOGNOM = space(50)
  w_SY__NOME = space(50)
  w_SY_SESSO = space(1)
  w_SYDATNAS = ctod('  /  /  ')
  w_SYCOFISC = space(25)
  w_SYCITEST = space(30)
  w_SYINDEST = space(35)
  w_SYSTAEST = space(3)
  w_SYDAVERI = space(1)
  w_HASEVCOP = space(50)
  w_HASEVENT = .F.
  w_SYDATEST = space(10)
  w_ANLOCALI = space(30)
  w_SYTOT010 = 0
  o_SYTOT010 = 0
  w_SYTOT011 = 0
  o_SYTOT011 = 0
  w_SYTOT012 = 0
  o_SYTOT012 = 0
  w_SYTOT013 = 0
  o_SYTOT013 = 0
  w_SYTOT014 = 0
  o_SYTOT014 = 0
  w_SYTOT015 = 0
  o_SYTOT015 = 0
  w_GDDATEST = space(10)
  w_GDTIPEST = space(1)
  w_GTSERIAL = space(10)
  w_GDSERIAL = space(10)
  w_VISPEREST = space(1)
  w_TIPEST = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_SYSERIAL = this.W_SYSERIAL
  * --- Area Manuale = Declare Variables
  * --- gsri_ady
   * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  func HasCPEvents(i_cOp)
   if this.cFunction='Query'
  	  	If Upper(i_cop)='ECPEDIT' Or Upper(i_cop)='ECPDELETE'
  	  		* esegue controllo se movimento bloccato se da problemi si pu� chiamare direttamente il Batch
          This.w_HASEVCOP=i_cop
  	  		This.NotifyEvent('HasEvent')
  	  		Return ( This.w_HASEVENT )
       Else
  	  		Return(.t.)
  	  	endif
  	Else
  	  	* --- Se l'evento non � modifica/cancellazione/inserimento prosegue
        Return(.t.)
    Endif
   EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DATESTSY','gsri_ady')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_adyPag1","gsri_ady",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati estratti record SY")
      .Pages(1).HelpContextID = 27398817
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ENTI_COM'
    this.cWorkTables[3]='NAZIONI'
    this.cWorkTables[4]='GEDETTEL'
    this.cWorkTables[5]='DATESTSY'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATESTSY_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATESTSY_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SYSERIAL = NVL(SYSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DATESTSY where SYSERIAL=KeySet.SYSERIAL
    *
    i_nConn = i_TableProp[this.DATESTSY_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSY_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATESTSY')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATESTSY.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATESTSY '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SYSERIAL',this.w_SYSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_ANCAURIT = space(2)
        .w_ANCODFIS = space(16)
        .w_ANPERFIS = space(1)
        .w_ANDESCRI = space(40)
        .w_ANCOGNOM = space(40)
        .w_AN__NOME = space(20)
        .w_AN_SESSO = space(1)
        .w_ANDATNAS = ctod("  /  /  ")
        .w_ANFLGEST = space(1)
        .w_ANCOFISC = space(25)
        .w_ANINDIRI = space(35)
        .w_ANNAZION = space(3)
        .w_NACODEST = space(5)
        .w_ANRITENU = space(1)
        .w_ANFLSGRE = space(1)
        .w_HASEVCOP = space(50)
        .w_HASEVENT = .f.
        .w_ANLOCALI = space(30)
        .w_GDDATEST = .w_Syserial
        .w_GDTIPEST = 'I'
        .w_GTSERIAL = .w_SYSERIAL
        .w_GDSERIAL = space(10)
        .w_VISPEREST = 'S'
        .w_TIPEST = 'G'
        .w_SYSERIAL = NVL(SYSERIAL,space(10))
        .op_SYSERIAL = .w_SYSERIAL
        .w_SY__ANNO = NVL(SY__ANNO,0)
        .w_SYTIPCON = NVL(SYTIPCON,space(1))
        .w_SYCODCON = NVL(SYCODCON,space(15))
          if link_1_5_joined
            this.w_SYCODCON = NVL(ANCODICE105,NVL(this.w_SYCODCON,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI105,space(40))
            this.w_ANCAURIT = NVL(ANCAURIT105,space(2))
            this.w_ANCODFIS = NVL(ANCODFIS105,space(16))
            this.w_ANPERFIS = NVL(ANPERFIS105,space(1))
            this.w_ANCOGNOM = NVL(ANCOGNOM105,space(40))
            this.w_AN__NOME = NVL(AN__NOME105,space(20))
            this.w_AN_SESSO = NVL(AN_SESSO105,space(1))
            this.w_ANDATNAS = NVL(cp_ToDate(ANDATNAS105),ctod("  /  /  "))
            this.w_ANFLGEST = NVL(ANFLGEST105,space(1))
            this.w_ANCOFISC = NVL(ANCOFISC105,space(25))
            this.w_ANINDIRI = NVL(ANINDIRI105,space(35))
            this.w_ANNAZION = NVL(ANNAZION105,space(3))
            this.w_ANRITENU = NVL(ANRITENU105,space(1))
            this.w_ANFLSGRE = NVL(ANFLSGRE105,space(1))
            this.w_ANLOCALI = NVL(ANLOCALI105,space(30))
          else
          .link_1_5('Load')
          endif
          .link_1_17('Load')
        .w_SYPERFIS = NVL(SYPERFIS,space(1))
        .w_SYPEREST = NVL(SYPEREST,space(1))
        .w_CAUPRE = IIF(.w_ANCAURIT='F' OR .w_ANCAURIT=='ZO' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_CAUPRE_2016 = IIF(.w_ANCAURIT= 'F' OR Alltrim(.w_ANCAURIT) == 'Z' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_CAUPRE_2017 = IIF(Alltrim(.w_ANCAURIT) == 'Z' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_SYCAUPRE = NVL(SYCAUPRE,space(2))
        .w_SYSCLGEN = NVL(SYSCLGEN,space(1))
        .w_SYDESCRI = NVL(SYDESCRI,space(60))
        .w_SYCOGNOM = NVL(SYCOGNOM,space(50))
        .w_SY__NOME = NVL(SY__NOME,space(50))
        .w_SY_SESSO = NVL(SY_SESSO,space(1))
        .w_SYDATNAS = NVL(cp_ToDate(SYDATNAS),ctod("  /  /  "))
        .w_SYCOFISC = NVL(SYCOFISC,space(25))
        .w_SYCITEST = NVL(SYCITEST,space(30))
        .w_SYINDEST = NVL(SYINDEST,space(35))
        .w_SYSTAEST = NVL(SYSTAEST,space(3))
        .w_SYDAVERI = NVL(SYDAVERI,space(1))
        .w_SYDATEST = NVL(SYDATEST,space(10))
        .w_SYTOT010 = NVL(SYTOT010,0)
        .w_SYTOT011 = NVL(SYTOT011,0)
        .w_SYTOT012 = NVL(SYTOT012,0)
        .w_SYTOT013 = NVL(SYTOT013,0)
        .w_SYTOT014 = NVL(SYTOT014,0)
        .w_SYTOT015 = NVL(SYTOT015,0)
          .link_1_73('Load')
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DATESTSY')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SYSERIAL = space(10)
      .w_OBTEST = ctod("  /  /  ")
      .w_SY__ANNO = 0
      .w_SYTIPCON = space(1)
      .w_SYCODCON = space(15)
      .w_ANCAURIT = space(2)
      .w_ANCODFIS = space(16)
      .w_ANPERFIS = space(1)
      .w_ANDESCRI = space(40)
      .w_ANCOGNOM = space(40)
      .w_AN__NOME = space(20)
      .w_AN_SESSO = space(1)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANFLGEST = space(1)
      .w_ANCOFISC = space(25)
      .w_ANINDIRI = space(35)
      .w_ANNAZION = space(3)
      .w_NACODEST = space(5)
      .w_ANRITENU = space(1)
      .w_ANFLSGRE = space(1)
      .w_SYPERFIS = space(1)
      .w_SYPEREST = space(1)
      .w_CAUPRE = space(2)
      .w_CAUPRE_2016 = space(2)
      .w_CAUPRE_2017 = space(2)
      .w_SYCAUPRE = space(2)
      .w_SYSCLGEN = space(1)
      .w_SYDESCRI = space(60)
      .w_SYCOGNOM = space(50)
      .w_SY__NOME = space(50)
      .w_SY_SESSO = space(1)
      .w_SYDATNAS = ctod("  /  /  ")
      .w_SYCOFISC = space(25)
      .w_SYCITEST = space(30)
      .w_SYINDEST = space(35)
      .w_SYSTAEST = space(3)
      .w_SYDAVERI = space(1)
      .w_HASEVCOP = space(50)
      .w_HASEVENT = .f.
      .w_SYDATEST = space(10)
      .w_ANLOCALI = space(30)
      .w_SYTOT010 = 0
      .w_SYTOT011 = 0
      .w_SYTOT012 = 0
      .w_SYTOT013 = 0
      .w_SYTOT014 = 0
      .w_SYTOT015 = 0
      .w_GDDATEST = space(10)
      .w_GDTIPEST = space(1)
      .w_GTSERIAL = space(10)
      .w_GDSERIAL = space(10)
      .w_VISPEREST = space(1)
      .w_TIPEST = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_OBTEST = CTOD('01-01-1900')
        .w_SY__ANNO = year(i_DATSYS)-1
        .w_SYTIPCON = 'F'
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_SYCODCON))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,17,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_17('Full')
          endif
          .DoRTCalc(18,20,.f.)
        .w_SYPERFIS = .w_ANPERFIS
        .w_SYPEREST = .w_ANFLGEST
        .w_CAUPRE = IIF(.w_ANCAURIT='F' OR .w_ANCAURIT=='ZO' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_CAUPRE_2016 = IIF(.w_ANCAURIT= 'F' OR Alltrim(.w_ANCAURIT) == 'Z' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_CAUPRE_2017 = IIF(Alltrim(.w_ANCAURIT) == 'Z' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
          .DoRTCalc(26,26,.f.)
        .w_SYSCLGEN = 'N'
        .w_SYDESCRI = Upper(.w_ANDESCRI)
        .w_SYCOGNOM = IIF(.w_SYPERFIS='S',Upper(.w_ANCOGNOM),SPACE(50))
        .w_SY__NOME = IIF(.w_SYPERFIS='S',Upper(.w_AN__NOME),SPACE(50))
        .w_SY_SESSO = iif(.w_SYPERFIS='S' and not empty(.w_SY_SESSO), .w_SY_SESSO, 'M')
        .w_SYDATNAS = IIF(.w_SYPERFIS='S',.w_ANDATNAS,CTOD('  -  -   '))
        .w_SYCOFISC = Upper(.w_Ancofisc)
        .w_SYCITEST = IIF(.w_SYPEREST='S',Upper(.w_ANLOCALI),SPACE(30))
        .w_SYINDEST = IIF(.w_SYPEREST='S',Upper(.w_ANINDIRI),SPACE(35))
        .w_SYSTAEST = IIF(.w_SYPEREST='S',Upper(Right(Alltrim(.w_Nacodest),3)),Space(3))
        .w_SYDAVERI = 'N'
          .DoRTCalc(38,41,.f.)
        .w_SYTOT010 = iif(.w_Sytot010<0,0,.w_Sytot010)
        .w_SYTOT011 = iif(.w_Sytot011<0,0,.w_Sytot011)
        .w_SYTOT012 = iif(.w_Sytot012<0,0,.w_Sytot012)
        .w_SYTOT013 = iif(.w_Sytot013<0,0,.w_Sytot013)
        .w_SYTOT014 = iif(.w_Sytot014<0,0,.w_Sytot014)
        .w_SYTOT015 = iif(.w_Sytot015<0,0,.w_Sytot015)
        .w_GDDATEST = .w_Syserial
        .w_GDTIPEST = 'I'
        .w_GTSERIAL = .w_SYSERIAL
        .DoRTCalc(50,50,.f.)
          if not(empty(.w_GTSERIAL))
          .link_1_73('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
          .DoRTCalc(51,51,.f.)
        .w_VISPEREST = 'S'
        .w_TIPEST = 'G'
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATESTSY')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTSY_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSY_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SYSER","i_codazi,w_SYSERIAL")
      .op_codazi = .w_codazi
      .op_SYSERIAL = .w_SYSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSY__ANNO_1_3.enabled = i_bVal
      .Page1.oPag.oSYCODCON_1_5.enabled = i_bVal
      .Page1.oPag.oCAUPRE_1_23.enabled = i_bVal
      .Page1.oPag.oCAUPRE_2016_1_24.enabled = i_bVal
      .Page1.oPag.oCAUPRE_2017_1_25.enabled = i_bVal
      .Page1.oPag.oSYSCLGEN_1_28.enabled = i_bVal
      .Page1.oPag.oSYDESCRI_1_33.enabled = i_bVal
      .Page1.oPag.oSYCOGNOM_1_34.enabled = i_bVal
      .Page1.oPag.oSY__NOME_1_35.enabled = i_bVal
      .Page1.oPag.oSY_SESSO_1_39.enabled = i_bVal
      .Page1.oPag.oSYDATNAS_1_40.enabled = i_bVal
      .Page1.oPag.oSYCOFISC_1_41.enabled = i_bVal
      .Page1.oPag.oSYCITEST_1_42.enabled = i_bVal
      .Page1.oPag.oSYINDEST_1_43.enabled = i_bVal
      .Page1.oPag.oSYSTAEST_1_44.enabled = i_bVal
      .Page1.oPag.oSYDAVERI_1_52.enabled = i_bVal
      .Page1.oPag.oSYTOT010_1_61.enabled = i_bVal
      .Page1.oPag.oSYTOT011_1_62.enabled = i_bVal
      .Page1.oPag.oSYTOT012_1_63.enabled = i_bVal
      .Page1.oPag.oSYTOT013_1_64.enabled = i_bVal
      .Page1.oPag.oSYTOT014_1_65.enabled = i_bVal
      .Page1.oPag.oSYTOT015_1_67.enabled = i_bVal
      .Page1.oPag.oBtn_1_77.enabled = .Page1.oPag.oBtn_1_77.mCond()
      .Page1.oPag.oObj_1_75.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DATESTSY',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATESTSY_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYSERIAL,"SYSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SY__ANNO,"SY__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYTIPCON,"SYTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYCODCON,"SYCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYPERFIS,"SYPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYPEREST,"SYPEREST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYCAUPRE,"SYCAUPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYSCLGEN,"SYSCLGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYDESCRI,"SYDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYCOGNOM,"SYCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SY__NOME,"SY__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SY_SESSO,"SY_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYDATNAS,"SYDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYCOFISC,"SYCOFISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYCITEST,"SYCITEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYINDEST,"SYINDEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYSTAEST,"SYSTAEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYDAVERI,"SYDAVERI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYDATEST,"SYDATEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYTOT010,"SYTOT010",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYTOT011,"SYTOT011",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYTOT012,"SYTOT012",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYTOT013,"SYTOT013",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYTOT014,"SYTOT014",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SYTOT015,"SYTOT015",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATESTSY_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSY_IDX,2])
    i_lTable = "DATESTSY"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DATESTSY_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATESTSY_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTSY_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DATESTSY_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SYSER","i_codazi,w_SYSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DATESTSY
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATESTSY')
        i_extval=cp_InsertValODBCExtFlds(this,'DATESTSY')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SYSERIAL,SY__ANNO,SYTIPCON,SYCODCON,SYPERFIS"+;
                  ",SYPEREST,SYCAUPRE,SYSCLGEN,SYDESCRI,SYCOGNOM"+;
                  ",SY__NOME,SY_SESSO,SYDATNAS,SYCOFISC,SYCITEST"+;
                  ",SYINDEST,SYSTAEST,SYDAVERI,SYDATEST,SYTOT010"+;
                  ",SYTOT011,SYTOT012,SYTOT013,SYTOT014,SYTOT015 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SYSERIAL)+;
                  ","+cp_ToStrODBC(this.w_SY__ANNO)+;
                  ","+cp_ToStrODBC(this.w_SYTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_SYCODCON)+;
                  ","+cp_ToStrODBC(this.w_SYPERFIS)+;
                  ","+cp_ToStrODBC(this.w_SYPEREST)+;
                  ","+cp_ToStrODBC(this.w_SYCAUPRE)+;
                  ","+cp_ToStrODBC(this.w_SYSCLGEN)+;
                  ","+cp_ToStrODBC(this.w_SYDESCRI)+;
                  ","+cp_ToStrODBC(this.w_SYCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_SY__NOME)+;
                  ","+cp_ToStrODBC(this.w_SY_SESSO)+;
                  ","+cp_ToStrODBC(this.w_SYDATNAS)+;
                  ","+cp_ToStrODBC(this.w_SYCOFISC)+;
                  ","+cp_ToStrODBC(this.w_SYCITEST)+;
                  ","+cp_ToStrODBC(this.w_SYINDEST)+;
                  ","+cp_ToStrODBC(this.w_SYSTAEST)+;
                  ","+cp_ToStrODBC(this.w_SYDAVERI)+;
                  ","+cp_ToStrODBC(this.w_SYDATEST)+;
                  ","+cp_ToStrODBC(this.w_SYTOT010)+;
                  ","+cp_ToStrODBC(this.w_SYTOT011)+;
                  ","+cp_ToStrODBC(this.w_SYTOT012)+;
                  ","+cp_ToStrODBC(this.w_SYTOT013)+;
                  ","+cp_ToStrODBC(this.w_SYTOT014)+;
                  ","+cp_ToStrODBC(this.w_SYTOT015)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATESTSY')
        i_extval=cp_InsertValVFPExtFlds(this,'DATESTSY')
        cp_CheckDeletedKey(i_cTable,0,'SYSERIAL',this.w_SYSERIAL)
        INSERT INTO (i_cTable);
              (SYSERIAL,SY__ANNO,SYTIPCON,SYCODCON,SYPERFIS,SYPEREST,SYCAUPRE,SYSCLGEN,SYDESCRI,SYCOGNOM,SY__NOME,SY_SESSO,SYDATNAS,SYCOFISC,SYCITEST,SYINDEST,SYSTAEST,SYDAVERI,SYDATEST,SYTOT010,SYTOT011,SYTOT012,SYTOT013,SYTOT014,SYTOT015  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SYSERIAL;
                  ,this.w_SY__ANNO;
                  ,this.w_SYTIPCON;
                  ,this.w_SYCODCON;
                  ,this.w_SYPERFIS;
                  ,this.w_SYPEREST;
                  ,this.w_SYCAUPRE;
                  ,this.w_SYSCLGEN;
                  ,this.w_SYDESCRI;
                  ,this.w_SYCOGNOM;
                  ,this.w_SY__NOME;
                  ,this.w_SY_SESSO;
                  ,this.w_SYDATNAS;
                  ,this.w_SYCOFISC;
                  ,this.w_SYCITEST;
                  ,this.w_SYINDEST;
                  ,this.w_SYSTAEST;
                  ,this.w_SYDAVERI;
                  ,this.w_SYDATEST;
                  ,this.w_SYTOT010;
                  ,this.w_SYTOT011;
                  ,this.w_SYTOT012;
                  ,this.w_SYTOT013;
                  ,this.w_SYTOT014;
                  ,this.w_SYTOT015;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DATESTSY_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTSY_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DATESTSY_IDX,i_nConn)
      *
      * update DATESTSY
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DATESTSY')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SY__ANNO="+cp_ToStrODBC(this.w_SY__ANNO)+;
             ",SYTIPCON="+cp_ToStrODBC(this.w_SYTIPCON)+;
             ",SYCODCON="+cp_ToStrODBCNull(this.w_SYCODCON)+;
             ",SYPERFIS="+cp_ToStrODBC(this.w_SYPERFIS)+;
             ",SYPEREST="+cp_ToStrODBC(this.w_SYPEREST)+;
             ",SYCAUPRE="+cp_ToStrODBC(this.w_SYCAUPRE)+;
             ",SYSCLGEN="+cp_ToStrODBC(this.w_SYSCLGEN)+;
             ",SYDESCRI="+cp_ToStrODBC(this.w_SYDESCRI)+;
             ",SYCOGNOM="+cp_ToStrODBC(this.w_SYCOGNOM)+;
             ",SY__NOME="+cp_ToStrODBC(this.w_SY__NOME)+;
             ",SY_SESSO="+cp_ToStrODBC(this.w_SY_SESSO)+;
             ",SYDATNAS="+cp_ToStrODBC(this.w_SYDATNAS)+;
             ",SYCOFISC="+cp_ToStrODBC(this.w_SYCOFISC)+;
             ",SYCITEST="+cp_ToStrODBC(this.w_SYCITEST)+;
             ",SYINDEST="+cp_ToStrODBC(this.w_SYINDEST)+;
             ",SYSTAEST="+cp_ToStrODBC(this.w_SYSTAEST)+;
             ",SYDAVERI="+cp_ToStrODBC(this.w_SYDAVERI)+;
             ",SYDATEST="+cp_ToStrODBC(this.w_SYDATEST)+;
             ",SYTOT010="+cp_ToStrODBC(this.w_SYTOT010)+;
             ",SYTOT011="+cp_ToStrODBC(this.w_SYTOT011)+;
             ",SYTOT012="+cp_ToStrODBC(this.w_SYTOT012)+;
             ",SYTOT013="+cp_ToStrODBC(this.w_SYTOT013)+;
             ",SYTOT014="+cp_ToStrODBC(this.w_SYTOT014)+;
             ",SYTOT015="+cp_ToStrODBC(this.w_SYTOT015)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DATESTSY')
        i_cWhere = cp_PKFox(i_cTable  ,'SYSERIAL',this.w_SYSERIAL  )
        UPDATE (i_cTable) SET;
              SY__ANNO=this.w_SY__ANNO;
             ,SYTIPCON=this.w_SYTIPCON;
             ,SYCODCON=this.w_SYCODCON;
             ,SYPERFIS=this.w_SYPERFIS;
             ,SYPEREST=this.w_SYPEREST;
             ,SYCAUPRE=this.w_SYCAUPRE;
             ,SYSCLGEN=this.w_SYSCLGEN;
             ,SYDESCRI=this.w_SYDESCRI;
             ,SYCOGNOM=this.w_SYCOGNOM;
             ,SY__NOME=this.w_SY__NOME;
             ,SY_SESSO=this.w_SY_SESSO;
             ,SYDATNAS=this.w_SYDATNAS;
             ,SYCOFISC=this.w_SYCOFISC;
             ,SYCITEST=this.w_SYCITEST;
             ,SYINDEST=this.w_SYINDEST;
             ,SYSTAEST=this.w_SYSTAEST;
             ,SYDAVERI=this.w_SYDAVERI;
             ,SYDATEST=this.w_SYDATEST;
             ,SYTOT010=this.w_SYTOT010;
             ,SYTOT011=this.w_SYTOT011;
             ,SYTOT012=this.w_SYTOT012;
             ,SYTOT013=this.w_SYTOT013;
             ,SYTOT014=this.w_SYTOT014;
             ,SYTOT015=this.w_SYTOT015;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATESTSY_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTSY_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DATESTSY_IDX,i_nConn)
      *
      * delete DATESTSY
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SYSERIAL',this.w_SYSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTSY_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTSY_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,16,.t.)
          .link_1_17('Full')
        .DoRTCalc(18,20,.t.)
        if .o_SYCODCON<>.w_SYCODCON
            .w_SYPERFIS = .w_ANPERFIS
        endif
        if .o_SYCODCON<>.w_SYCODCON
            .w_SYPEREST = .w_ANFLGEST
        endif
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SY__ANNO<>.w_SY__ANNO
            .w_CAUPRE = IIF(.w_ANCAURIT='F' OR .w_ANCAURIT=='ZO' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        endif
        if .o_SY__ANNO<>.w_SY__ANNO.or. .o_SYCODCON<>.w_SYCODCON
            .w_CAUPRE_2016 = IIF(.w_ANCAURIT= 'F' OR Alltrim(.w_ANCAURIT) == 'Z' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        endif
        if .o_SY__ANNO<>.w_SY__ANNO.or. .o_SYCODCON<>.w_SYCODCON
            .w_CAUPRE_2017 = IIF(Alltrim(.w_ANCAURIT) == 'Z' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        endif
        if .o_SY__ANNO<>.w_SY__ANNO.or. .o_CAUPRE<>.w_CAUPRE.or. .o_CAUPRE_2016<>.w_CAUPRE_2016.or. .o_CAUPRE_2017<>.w_CAUPRE_2017
          .Calculate_ITYOIUGTQG()
        endif
        .DoRTCalc(26,27,.t.)
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SYPERFIS<>.w_SYPERFIS
            .w_SYDESCRI = Upper(.w_ANDESCRI)
        endif
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SYPERFIS<>.w_SYPERFIS
            .w_SYCOGNOM = IIF(.w_SYPERFIS='S',Upper(.w_ANCOGNOM),SPACE(50))
        endif
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SYPERFIS<>.w_SYPERFIS
            .w_SY__NOME = IIF(.w_SYPERFIS='S',Upper(.w_AN__NOME),SPACE(50))
        endif
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SYPERFIS<>.w_SYPERFIS
            .w_SY_SESSO = iif(.w_SYPERFIS='S' and not empty(.w_SY_SESSO), .w_SY_SESSO, 'M')
        endif
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SYPERFIS <>.w_SYPERFIS 
            .w_SYDATNAS = IIF(.w_SYPERFIS='S',.w_ANDATNAS,CTOD('  -  -   '))
        endif
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SYPEREST<>.w_SYPEREST
            .w_SYCOFISC = Upper(.w_Ancofisc)
        endif
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SYPEREST<>.w_SYPEREST
            .w_SYCITEST = IIF(.w_SYPEREST='S',Upper(.w_ANLOCALI),SPACE(30))
        endif
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SYPEREST<>.w_SYPEREST
            .w_SYINDEST = IIF(.w_SYPEREST='S',Upper(.w_ANINDIRI),SPACE(35))
        endif
        if .o_SYCODCON<>.w_SYCODCON.or. .o_SYPEREST<>.w_SYPEREST
            .w_SYSTAEST = IIF(.w_SYPEREST='S',Upper(Right(Alltrim(.w_Nacodest),3)),Space(3))
        endif
        .DoRTCalc(37,41,.t.)
        if .o_Sytot010<>.w_Sytot010
            .w_SYTOT010 = iif(.w_Sytot010<0,0,.w_Sytot010)
        endif
        if .o_Sytot011<>.w_Sytot011
            .w_SYTOT011 = iif(.w_Sytot011<0,0,.w_Sytot011)
        endif
        if .o_Sytot012<>.w_Sytot012
            .w_SYTOT012 = iif(.w_Sytot012<0,0,.w_Sytot012)
        endif
        if .o_Sytot013<>.w_Sytot013
            .w_SYTOT013 = iif(.w_Sytot013<0,0,.w_Sytot013)
        endif
        if .o_Sytot014<>.w_Sytot014
            .w_SYTOT014 = iif(.w_Sytot014<0,0,.w_Sytot014)
        endif
        if .o_Sytot015<>.w_Sytot015
            .w_SYTOT015 = iif(.w_Sytot015<0,0,.w_Sytot015)
        endif
        .DoRTCalc(48,49,.t.)
          .link_1_73('Full')
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SYSER","i_codazi,w_SYSERIAL")
          .op_SYSERIAL = .w_SYSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(51,53,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
    endwith
  return

  proc Calculate_ITYOIUGTQG()
    with this
          * --- Valorizzo campo SYCAUPRE
          .w_SYCAUPRE = iif(.w_SY__ANNO<2016,.w_CAUPRE,iif(.w_SY__ANNO=2016,.w_CAUPRE_2016,.w_CAUPRE_2017))
    endwith
  endproc
  proc Calculate_LFLBLFKCYE()
    with this
          * --- Valorizzo Causale prestazione
          .w_CAUPRE = .w_SYCAUPRE
          .w_CAUPRE_2016 = .w_SYCAUPRE
          .w_CAUPRE_2017 = .w_SYCAUPRE
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSY__ANNO_1_3.enabled = this.oPgFrm.Page1.oPag.oSY__ANNO_1_3.mCond()
    this.oPgFrm.Page1.oPag.oSYCODCON_1_5.enabled = this.oPgFrm.Page1.oPag.oSYCODCON_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCAUPRE_1_23.enabled = this.oPgFrm.Page1.oPag.oCAUPRE_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_24.enabled = this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_25.enabled = this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_25.mCond()
    this.oPgFrm.Page1.oPag.oSYDESCRI_1_33.enabled = this.oPgFrm.Page1.oPag.oSYDESCRI_1_33.mCond()
    this.oPgFrm.Page1.oPag.oSYCOGNOM_1_34.enabled = this.oPgFrm.Page1.oPag.oSYCOGNOM_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSY__NOME_1_35.enabled = this.oPgFrm.Page1.oPag.oSY__NOME_1_35.mCond()
    this.oPgFrm.Page1.oPag.oSY_SESSO_1_39.enabled = this.oPgFrm.Page1.oPag.oSY_SESSO_1_39.mCond()
    this.oPgFrm.Page1.oPag.oSYDATNAS_1_40.enabled = this.oPgFrm.Page1.oPag.oSYDATNAS_1_40.mCond()
    this.oPgFrm.Page1.oPag.oSYCOFISC_1_41.enabled = this.oPgFrm.Page1.oPag.oSYCOFISC_1_41.mCond()
    this.oPgFrm.Page1.oPag.oSYCITEST_1_42.enabled = this.oPgFrm.Page1.oPag.oSYCITEST_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSYINDEST_1_43.enabled = this.oPgFrm.Page1.oPag.oSYINDEST_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSYSTAEST_1_44.enabled = this.oPgFrm.Page1.oPag.oSYSTAEST_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAUPRE_1_23.visible=!this.oPgFrm.Page1.oPag.oCAUPRE_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_24.visible=!this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_24.mHide()
    this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_25.visible=!this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_77.visible=!this.oPgFrm.Page1.oPag.oBtn_1_77.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_ITYOIUGTQG()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_75.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_LFLBLFKCYE()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SYCODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SYCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SYCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SYTIPCON;
                     ,'ANCODICE',trim(this.w_SYCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SYCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SYCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SYCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SYTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SYCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSYCODCON_1_5'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSRI_ADY.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SYTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute o non estero")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SYCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SYCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SYTIPCON;
                       ,'ANCODICE',this.w_SYCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANFLGEST,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SYCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANCAURIT = NVL(_Link_.ANCAURIT,space(2))
      this.w_ANCODFIS = NVL(_Link_.ANCODFIS,space(16))
      this.w_ANPERFIS = NVL(_Link_.ANPERFIS,space(1))
      this.w_ANCOGNOM = NVL(_Link_.ANCOGNOM,space(40))
      this.w_AN__NOME = NVL(_Link_.AN__NOME,space(20))
      this.w_AN_SESSO = NVL(_Link_.AN_SESSO,space(1))
      this.w_ANDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_ANFLGEST = NVL(_Link_.ANFLGEST,space(1))
      this.w_ANCOFISC = NVL(_Link_.ANCOFISC,space(25))
      this.w_ANINDIRI = NVL(_Link_.ANINDIRI,space(35))
      this.w_ANNAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_ANRITENU = NVL(_Link_.ANRITENU,space(1))
      this.w_ANFLSGRE = NVL(_Link_.ANFLSGRE,space(1))
      this.w_ANLOCALI = NVL(_Link_.ANLOCALI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_SYCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_ANCAURIT = space(2)
      this.w_ANCODFIS = space(16)
      this.w_ANPERFIS = space(1)
      this.w_ANCOGNOM = space(40)
      this.w_AN__NOME = space(20)
      this.w_AN_SESSO = space(1)
      this.w_ANDATNAS = ctod("  /  /  ")
      this.w_ANFLGEST = space(1)
      this.w_ANCOFISC = space(25)
      this.w_ANINDIRI = space(35)
      this.w_ANNAZION = space(3)
      this.w_ANRITENU = space(1)
      this.w_ANFLSGRE = space(1)
      this.w_ANLOCALI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_Anritenu $ 'CS' And .w_Anflgest='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute o non estero")
        endif
        this.w_SYCODCON = space(15)
        this.w_ANDESCRI = space(40)
        this.w_ANCAURIT = space(2)
        this.w_ANCODFIS = space(16)
        this.w_ANPERFIS = space(1)
        this.w_ANCOGNOM = space(40)
        this.w_AN__NOME = space(20)
        this.w_AN_SESSO = space(1)
        this.w_ANDATNAS = ctod("  /  /  ")
        this.w_ANFLGEST = space(1)
        this.w_ANCOFISC = space(25)
        this.w_ANINDIRI = space(35)
        this.w_ANNAZION = space(3)
        this.w_ANRITENU = space(1)
        this.w_ANFLSGRE = space(1)
        this.w_ANLOCALI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SYCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 16 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+16<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.ANCODICE as ANCODICE105"+ ",link_1_5.ANDESCRI as ANDESCRI105"+ ",link_1_5.ANCAURIT as ANCAURIT105"+ ",link_1_5.ANCODFIS as ANCODFIS105"+ ",link_1_5.ANPERFIS as ANPERFIS105"+ ",link_1_5.ANCOGNOM as ANCOGNOM105"+ ",link_1_5.AN__NOME as AN__NOME105"+ ",link_1_5.AN_SESSO as AN_SESSO105"+ ",link_1_5.ANDATNAS as ANDATNAS105"+ ",link_1_5.ANFLGEST as ANFLGEST105"+ ",link_1_5.ANCOFISC as ANCOFISC105"+ ",link_1_5.ANINDIRI as ANINDIRI105"+ ",link_1_5.ANNAZION as ANNAZION105"+ ",link_1_5.ANRITENU as ANRITENU105"+ ",link_1_5.ANFLSGRE as ANFLSGRE105"+ ",link_1_5.ANLOCALI as ANLOCALI105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on DATESTSY.SYCODCON=link_1_5.ANCODICE"+" and DATESTSY.SYTIPCON=link_1_5.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+16
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and DATESTSY.SYCODCON=link_1_5.ANCODICE(+)"'+'+" and DATESTSY.SYTIPCON=link_1_5.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+16
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODEST";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NACODEST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_NACODEST = NVL(_Link_.NACODEST,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_NACODEST = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GTSERIAL
  func Link_1_73(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GEDETTEL_IDX,3]
    i_lTable = "GEDETTEL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GEDETTEL_IDX,2], .t., this.GEDETTEL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GEDETTEL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GTSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GTSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDTIPEST,GDDATEST,GDSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where GDDATEST="+cp_ToStrODBC(this.w_GTSERIAL);
                   +" and GDTIPEST="+cp_ToStrODBC(this.w_GDTIPEST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDTIPEST',this.w_GDTIPEST;
                       ,'GDDATEST',this.w_GTSERIAL)
            select GDTIPEST,GDDATEST,GDSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GTSERIAL = NVL(_Link_.GDDATEST,space(10))
      this.w_GDSERIAL = NVL(_Link_.GDSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_GTSERIAL = space(10)
      endif
      this.w_GDSERIAL = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GEDETTEL_IDX,2])+'\'+cp_ToStr(_Link_.GDTIPEST,1)+'\'+cp_ToStr(_Link_.GDDATEST,1)
      cp_ShowWarn(i_cKey,this.GEDETTEL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GTSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSY__ANNO_1_3.value==this.w_SY__ANNO)
      this.oPgFrm.Page1.oPag.oSY__ANNO_1_3.value=this.w_SY__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oSYCODCON_1_5.value==this.w_SYCODCON)
      this.oPgFrm.Page1.oPag.oSYCODCON_1_5.value=this.w_SYCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oSYPERFIS_1_21.RadioValue()==this.w_SYPERFIS)
      this.oPgFrm.Page1.oPag.oSYPERFIS_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUPRE_1_23.RadioValue()==this.w_CAUPRE)
      this.oPgFrm.Page1.oPag.oCAUPRE_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_24.RadioValue()==this.w_CAUPRE_2016)
      this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_25.RadioValue()==this.w_CAUPRE_2017)
      this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSYSCLGEN_1_28.RadioValue()==this.w_SYSCLGEN)
      this.oPgFrm.Page1.oPag.oSYSCLGEN_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSYDESCRI_1_33.value==this.w_SYDESCRI)
      this.oPgFrm.Page1.oPag.oSYDESCRI_1_33.value=this.w_SYDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSYCOGNOM_1_34.value==this.w_SYCOGNOM)
      this.oPgFrm.Page1.oPag.oSYCOGNOM_1_34.value=this.w_SYCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSY__NOME_1_35.value==this.w_SY__NOME)
      this.oPgFrm.Page1.oPag.oSY__NOME_1_35.value=this.w_SY__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oSY_SESSO_1_39.RadioValue()==this.w_SY_SESSO)
      this.oPgFrm.Page1.oPag.oSY_SESSO_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSYDATNAS_1_40.value==this.w_SYDATNAS)
      this.oPgFrm.Page1.oPag.oSYDATNAS_1_40.value=this.w_SYDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oSYCOFISC_1_41.value==this.w_SYCOFISC)
      this.oPgFrm.Page1.oPag.oSYCOFISC_1_41.value=this.w_SYCOFISC
    endif
    if not(this.oPgFrm.Page1.oPag.oSYCITEST_1_42.value==this.w_SYCITEST)
      this.oPgFrm.Page1.oPag.oSYCITEST_1_42.value=this.w_SYCITEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSYINDEST_1_43.value==this.w_SYINDEST)
      this.oPgFrm.Page1.oPag.oSYINDEST_1_43.value=this.w_SYINDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSYSTAEST_1_44.value==this.w_SYSTAEST)
      this.oPgFrm.Page1.oPag.oSYSTAEST_1_44.value=this.w_SYSTAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSYDAVERI_1_52.RadioValue()==this.w_SYDAVERI)
      this.oPgFrm.Page1.oPag.oSYDAVERI_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSYTOT010_1_61.value==this.w_SYTOT010)
      this.oPgFrm.Page1.oPag.oSYTOT010_1_61.value=this.w_SYTOT010
    endif
    if not(this.oPgFrm.Page1.oPag.oSYTOT011_1_62.value==this.w_SYTOT011)
      this.oPgFrm.Page1.oPag.oSYTOT011_1_62.value=this.w_SYTOT011
    endif
    if not(this.oPgFrm.Page1.oPag.oSYTOT012_1_63.value==this.w_SYTOT012)
      this.oPgFrm.Page1.oPag.oSYTOT012_1_63.value=this.w_SYTOT012
    endif
    if not(this.oPgFrm.Page1.oPag.oSYTOT013_1_64.value==this.w_SYTOT013)
      this.oPgFrm.Page1.oPag.oSYTOT013_1_64.value=this.w_SYTOT013
    endif
    if not(this.oPgFrm.Page1.oPag.oSYTOT014_1_65.value==this.w_SYTOT014)
      this.oPgFrm.Page1.oPag.oSYTOT014_1_65.value=this.w_SYTOT014
    endif
    if not(this.oPgFrm.Page1.oPag.oSYTOT015_1_67.value==this.w_SYTOT015)
      this.oPgFrm.Page1.oPag.oSYTOT015_1_67.value=this.w_SYTOT015
    endif
    cp_SetControlsValueExtFlds(this,'DATESTSY')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_Sy__Anno>=2015)  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSY__ANNO_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_SYCODCON)) or not(.w_Anritenu $ 'CS' And .w_Anflgest='S'))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYCODCON_1_5.SetFocus()
            i_bnoObbl = !empty(.w_SYCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute o non estero")
          case   (empty(.w_CAUPRE))  and not(.w_Sy__Anno>=2016)  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUPRE_1_23.SetFocus()
            i_bnoObbl = !empty(.w_CAUPRE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAUPRE_2016))  and not(.w_Sy__Anno<>2016)  and (.cFunction='Load' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUPRE_2016_1_24.SetFocus()
            i_bnoObbl = !empty(.w_CAUPRE_2016)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAUPRE_2017))  and not(.w_Sy__Anno<2017)  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUPRE_2017_1_25.SetFocus()
            i_bnoObbl = !empty(.w_CAUPRE_2017)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SYDESCRI))  and (.cFunction<>'Query' And  .w_SYPERFIS<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYDESCRI_1_33.SetFocus()
            i_bnoObbl = !empty(.w_SYDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SYCOGNOM))  and (.cFunction<>'Query' AND .w_SYPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYCOGNOM_1_34.SetFocus()
            i_bnoObbl = !empty(.w_SYCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SY__NOME))  and (.cFunction<>'Query' AND .w_SYPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSY__NOME_1_35.SetFocus()
            i_bnoObbl = !empty(.w_SY__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SY_SESSO))  and (.cFunction<>'Query' AND .w_SYPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSY_SESSO_1_39.SetFocus()
            i_bnoObbl = !empty(.w_SY_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SYDATNAS))  and (.cFunction<>'Query' AND .w_SYPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYDATNAS_1_40.SetFocus()
            i_bnoObbl = !empty(.w_SYDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SYSTAEST))  and (.cFunction<>'Query' And .w_SYPEREST='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYSTAEST_1_44.SetFocus()
            i_bnoObbl = !empty(.w_SYSTAEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_Sytot010>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYTOT010_1_61.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Sytot011>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYTOT011_1_62.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Sytot012>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYTOT012_1_63.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Sytot013>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYTOT013_1_64.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Sytot014>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYTOT014_1_65.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Sytot015>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYTOT015_1_67.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SY__ANNO = this.w_SY__ANNO
    this.o_SYCODCON = this.w_SYCODCON
    this.o_SYPERFIS = this.w_SYPERFIS
    this.o_SYPEREST = this.w_SYPEREST
    this.o_CAUPRE = this.w_CAUPRE
    this.o_CAUPRE_2016 = this.w_CAUPRE_2016
    this.o_CAUPRE_2017 = this.w_CAUPRE_2017
    this.o_SYTOT010 = this.w_SYTOT010
    this.o_SYTOT011 = this.w_SYTOT011
    this.o_SYTOT012 = this.w_SYTOT012
    this.o_SYTOT013 = this.w_SYTOT013
    this.o_SYTOT014 = this.w_SYTOT014
    this.o_SYTOT015 = this.w_SYTOT015
    return

enddefine

* --- Define pages as container
define class tgsri_adyPag1 as StdContainer
  Width  = 852
  height = 533
  stdWidth  = 852
  stdheight = 533
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSY__ANNO_1_3 as StdField with uid="HYNPMMDCOL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SY__ANNO", cQueryName = "SY__ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 216750197,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=99, Top=14, cSayPict='"9999"', cGetPict='"9999"'

  func oSY__ANNO_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oSY__ANNO_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Sy__Anno>=2015)
    endwith
    return bRes
  endfunc

  add object oSYCODCON_1_5 as StdField with uid="EAHXPIIFWI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SYCODCON", cQueryName = "SYCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o non soggetto a ritenute o non estero",;
    ToolTipText = "Codice conto",;
    HelpContextID = 34183284,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=325, Top=14, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SYTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SYCODCON"

  func oSYCODCON_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oSYCODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSYCODCON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSYCODCON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SYTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SYTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSYCODCON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSRI_ADY.CONTI_VZM',this.parent.oContained
  endproc
  proc oSYCODCON_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SYTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SYCODCON
     i_obj.ecpSave()
  endproc

  add object oSYPERFIS_1_21 as StdCheck with uid="PJITHOZBWZ",rtseq=21,rtrep=.f.,left=669, top=12, caption="Persona fisica", enabled=.f.,;
    ToolTipText = "Se attivo: il fornitore � una persona fisica",;
    HelpContextID = 169842567,;
    cFormVar="w_SYPERFIS", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oSYPERFIS_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSYPERFIS_1_21.GetRadio()
    this.Parent.oContained.w_SYPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oSYPERFIS_1_21.SetRadio()
    this.Parent.oContained.w_SYPERFIS=trim(this.Parent.oContained.w_SYPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_SYPERFIS=='S',1,;
      0)
  endfunc


  add object oCAUPRE_1_23 as StdCombo with uid="CWTVWQSDOO",rtseq=23,rtrep=.f.,left=325,top=49,width=90,height=22;
    , ToolTipText = "Valore predefinito causale prestazione";
    , HelpContextID = 82550566;
    , cFormVar="w_CAUPRE",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCAUPRE_1_23.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'G',;
    iif(this.value =7,'H',;
    iif(this.value =8,'I',;
    iif(this.value =9,'L',;
    iif(this.value =10,'L1',;
    iif(this.value =11,'M',;
    iif(this.value =12,'M1',;
    iif(this.value =13,'M2',;
    iif(this.value =14,'N',;
    iif(this.value =15,'O',;
    iif(this.value =16,'O1',;
    iif(this.value =17,'P',;
    iif(this.value =18,'Q',;
    iif(this.value =19,'R',;
    iif(this.value =20,'S',;
    iif(this.value =21,'T',;
    iif(this.value =22,'U',;
    iif(this.value =23,'V',;
    iif(this.value =24,'V1',;
    iif(this.value =25,'V2',;
    iif(this.value =26,'W',;
    iif(this.value =27,'X',;
    iif(this.value =28,'Y',;
    iif(this.value =29,'Z',;
    space(2)))))))))))))))))))))))))))))))
  endfunc
  func oCAUPRE_1_23.GetRadio()
    this.Parent.oContained.w_CAUPRE = this.RadioValue()
    return .t.
  endfunc

  func oCAUPRE_1_23.SetRadio()
    this.Parent.oContained.w_CAUPRE=trim(this.Parent.oContained.w_CAUPRE)
    this.value = ;
      iif(this.Parent.oContained.w_CAUPRE=='A',1,;
      iif(this.Parent.oContained.w_CAUPRE=='B',2,;
      iif(this.Parent.oContained.w_CAUPRE=='C',3,;
      iif(this.Parent.oContained.w_CAUPRE=='D',4,;
      iif(this.Parent.oContained.w_CAUPRE=='E',5,;
      iif(this.Parent.oContained.w_CAUPRE=='G',6,;
      iif(this.Parent.oContained.w_CAUPRE=='H',7,;
      iif(this.Parent.oContained.w_CAUPRE=='I',8,;
      iif(this.Parent.oContained.w_CAUPRE=='L',9,;
      iif(this.Parent.oContained.w_CAUPRE=='L1',10,;
      iif(this.Parent.oContained.w_CAUPRE=='M',11,;
      iif(this.Parent.oContained.w_CAUPRE=='M1',12,;
      iif(this.Parent.oContained.w_CAUPRE=='M2',13,;
      iif(this.Parent.oContained.w_CAUPRE=='N',14,;
      iif(this.Parent.oContained.w_CAUPRE=='O',15,;
      iif(this.Parent.oContained.w_CAUPRE=='O1',16,;
      iif(this.Parent.oContained.w_CAUPRE=='P',17,;
      iif(this.Parent.oContained.w_CAUPRE=='Q',18,;
      iif(this.Parent.oContained.w_CAUPRE=='R',19,;
      iif(this.Parent.oContained.w_CAUPRE=='S',20,;
      iif(this.Parent.oContained.w_CAUPRE=='T',21,;
      iif(this.Parent.oContained.w_CAUPRE=='U',22,;
      iif(this.Parent.oContained.w_CAUPRE=='V',23,;
      iif(this.Parent.oContained.w_CAUPRE=='V1',24,;
      iif(this.Parent.oContained.w_CAUPRE=='V2',25,;
      iif(this.Parent.oContained.w_CAUPRE=='W',26,;
      iif(this.Parent.oContained.w_CAUPRE=='X',27,;
      iif(this.Parent.oContained.w_CAUPRE=='Y',28,;
      iif(this.Parent.oContained.w_CAUPRE=='Z',29,;
      0)))))))))))))))))))))))))))))
  endfunc

  func oCAUPRE_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCAUPRE_1_23.mHide()
    with this.Parent.oContained
      return (.w_Sy__Anno>=2016)
    endwith
  endfunc


  add object oCAUPRE_2016_1_24 as StdCombo with uid="VNVMOGRAFD",rtseq=24,rtrep=.f.,left=325,top=49,width=90,height=22;
    , ToolTipText = "Valore predefinito causale prestazione";
    , HelpContextID = 82785112;
    , cFormVar="w_CAUPRE_2016",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo ZO", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCAUPRE_2016_1_24.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'G',;
    iif(this.value =7,'H',;
    iif(this.value =8,'I',;
    iif(this.value =9,'L',;
    iif(this.value =10,'L1',;
    iif(this.value =11,'M',;
    iif(this.value =12,'M1',;
    iif(this.value =13,'M2',;
    iif(this.value =14,'N',;
    iif(this.value =15,'O',;
    iif(this.value =16,'O1',;
    iif(this.value =17,'P',;
    iif(this.value =18,'Q',;
    iif(this.value =19,'R',;
    iif(this.value =20,'S',;
    iif(this.value =21,'T',;
    iif(this.value =22,'U',;
    iif(this.value =23,'V',;
    iif(this.value =24,'V1',;
    iif(this.value =25,'V2',;
    iif(this.value =26,'W',;
    iif(this.value =27,'X',;
    iif(this.value =28,'Y',;
    iif(this.value =29,'ZO',;
    space(2)))))))))))))))))))))))))))))))
  endfunc
  func oCAUPRE_2016_1_24.GetRadio()
    this.Parent.oContained.w_CAUPRE_2016 = this.RadioValue()
    return .t.
  endfunc

  func oCAUPRE_2016_1_24.SetRadio()
    this.Parent.oContained.w_CAUPRE_2016=trim(this.Parent.oContained.w_CAUPRE_2016)
    this.value = ;
      iif(this.Parent.oContained.w_CAUPRE_2016=='A',1,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='B',2,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='C',3,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='D',4,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='E',5,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='G',6,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='H',7,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='I',8,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='L',9,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='L1',10,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='M',11,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='M1',12,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='M2',13,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='N',14,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='O',15,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='O1',16,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='P',17,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='Q',18,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='R',19,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='S',20,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='T',21,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='U',22,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='V',23,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='V1',24,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='V2',25,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='W',26,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='X',27,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='Y',28,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='ZO',29,;
      0)))))))))))))))))))))))))))))
  endfunc

  func oCAUPRE_2016_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' )
    endwith
   endif
  endfunc

  func oCAUPRE_2016_1_24.mHide()
    with this.Parent.oContained
      return (.w_Sy__Anno<>2016)
    endwith
  endfunc


  add object oCAUPRE_2017_1_25 as StdCombo with uid="KSBFGRPYFO",rtseq=25,rtrep=.f.,left=325,top=49,width=90,height=22;
    , ToolTipText = "Valore predefinito causale prestazione";
    , HelpContextID = 82789208;
    , cFormVar="w_CAUPRE_2017",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo ZO", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCAUPRE_2017_1_25.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'ZO',;
    space(2))))))))))))))))))))))))))))))))))
  endfunc
  func oCAUPRE_2017_1_25.GetRadio()
    this.Parent.oContained.w_CAUPRE_2017 = this.RadioValue()
    return .t.
  endfunc

  func oCAUPRE_2017_1_25.SetRadio()
    this.Parent.oContained.w_CAUPRE_2017=trim(this.Parent.oContained.w_CAUPRE_2017)
    this.value = ;
      iif(this.Parent.oContained.w_CAUPRE_2017=='A',1,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='B',2,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='C',3,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='D',4,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='E',5,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='F',6,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='G',7,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='H',8,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='I',9,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='J',10,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='K',11,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='L',12,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='L1',13,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='M',14,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='M1',15,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='M2',16,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='N',17,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='O',18,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='O1',19,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='P',20,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='Q',21,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='R',22,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='S',23,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='T',24,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='U',25,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='V',26,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='V1',27,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='V2',28,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='W',29,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='X',30,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='Y',31,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='ZO',32,;
      0))))))))))))))))))))))))))))))))
  endfunc

  func oCAUPRE_2017_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCAUPRE_2017_1_25.mHide()
    with this.Parent.oContained
      return (.w_Sy__Anno<2017)
    endwith
  endfunc

  add object oSYSCLGEN_1_28 as StdCheck with uid="FZBDCCDEUF",rtseq=27,rtrep=.f.,left=669, top=66, caption="Escludi da generazione",;
    ToolTipText = "Se attivo, il dato estratto sar� escluso dalla generazione file telematico",;
    HelpContextID = 159475596,;
    cFormVar="w_SYSCLGEN", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oSYSCLGEN_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSYSCLGEN_1_28.GetRadio()
    this.Parent.oContained.w_SYSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oSYSCLGEN_1_28.SetRadio()
    this.Parent.oContained.w_SYSCLGEN=trim(this.Parent.oContained.w_SYSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_SYSCLGEN=='S',1,;
      0)
  endfunc

  add object oSYDESCRI_1_33 as StdField with uid="EPFAFIXABF",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SYDESCRI", cQueryName = "SYDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione,ditta o ragione sociale",;
    HelpContextID = 49260655,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=8, Top=161, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oSYDESCRI_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And  .w_SYPERFIS<>'S')
    endwith
   endif
  endfunc

  add object oSYCOGNOM_1_34 as StdField with uid="VBWHBYPETD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_SYCOGNOM", cQueryName = "SYCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 221878387,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=8, Top=227, cSayPict="REPL('!',50)", cGetPict="REPL('!',50)", InputMask=replicate('X',50)

  func oSYCOGNOM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_SYPERFIS='S')
    endwith
   endif
  endfunc

  add object oSY__NOME_1_35 as StdField with uid="TZTOWOZVVJ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_SY__NOME", cQueryName = "SY__NOME",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 247158891,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=334, Top=227, cSayPict="REPL('!',50)", cGetPict="REPL('!',50)", InputMask=replicate('X',50)

  func oSY__NOME_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_SYPERFIS='S')
    endwith
   endif
  endfunc


  add object oSY_SESSO_1_39 as StdCombo with uid="UFMSDHWBVZ",rtseq=31,rtrep=.f.,left=695,top=227,width=91,height=22;
    , HelpContextID = 232826763;
    , cFormVar="w_SY_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSY_SESSO_1_39.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSY_SESSO_1_39.GetRadio()
    this.Parent.oContained.w_SY_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oSY_SESSO_1_39.SetRadio()
    this.Parent.oContained.w_SY_SESSO=trim(this.Parent.oContained.w_SY_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_SY_SESSO=='M',1,;
      iif(this.Parent.oContained.w_SY_SESSO=='F',2,;
      0))
  endfunc

  func oSY_SESSO_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_SYPERFIS='S')
    endwith
   endif
  endfunc

  add object oSYDATNAS_1_40 as StdField with uid="VTKSSEXLQD",rtseq=32,rtrep=.f.,;
    cFormVar = "w_SYDATNAS", cQueryName = "SYDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 234596473,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=8, Top=287

  func oSYDATNAS_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_SYPERFIS='S')
    endwith
   endif
  endfunc

  add object oSYCOFISC_1_41 as StdField with uid="NEICLLJLQF",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SYCOFISC", cQueryName = "SYCOFISC",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 136943721,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=334, Top=287, cSayPict='REPLICATE("!",25)', cGetPict='REPLICATE("!",25)', InputMask=replicate('X',25)

  func oSYCOFISC_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_SYPEREST='S')
    endwith
   endif
  endfunc

  add object oSYCITEST_1_42 as StdField with uid="VZATVQHIMD",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SYCITEST", cQueryName = "SYCITEST",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza estera",;
    HelpContextID = 84121722,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=8, Top=341, cSayPict='REPLICATE("!",30)', cGetPict='REPLICATE("!",30)', InputMask=replicate('X',30)

  func oSYCITEST_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_SYPEREST='S')
    endwith
   endif
  endfunc

  add object oSYINDEST_1_43 as StdField with uid="PPBQNCJMIR",rtseq=35,rtrep=.f.,;
    cFormVar = "w_SYINDEST", cQueryName = "SYINDEST",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero",;
    HelpContextID = 67696762,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=334, Top=341, cSayPict="REPL('!',35)", cGetPict="REPL('!',35)", InputMask=replicate('X',35)

  func oSYINDEST_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SYPEREST='S')
    endwith
   endif
  endfunc

  add object oSYSTAEST_1_44 as StdField with uid="JZJZNJGHQI",rtseq=36,rtrep=.f.,;
    cFormVar = "w_SYSTAEST", cQueryName = "SYSTAEST",nZero=3,;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 64985210,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=695, Top=341, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oSYSTAEST_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_SYPEREST='S')
    endwith
   endif
  endfunc

  add object oSYDAVERI_1_52 as StdCheck with uid="CGOCJPGUTC",rtseq=37,rtrep=.f.,left=669, top=39, caption="Da verificare",;
    ToolTipText = "Se attivo, il dato estratto � da verificare",;
    HelpContextID = 85698671,;
    cFormVar="w_SYDAVERI", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oSYDAVERI_1_52.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSYDAVERI_1_52.GetRadio()
    this.Parent.oContained.w_SYDAVERI = this.RadioValue()
    return .t.
  endfunc

  func oSYDAVERI_1_52.SetRadio()
    this.Parent.oContained.w_SYDAVERI=trim(this.Parent.oContained.w_SYDAVERI)
    this.value = ;
      iif(this.Parent.oContained.w_SYDAVERI=='S',1,;
      0)
  endfunc

  add object oSYTOT010_1_61 as StdField with uid="RIGUXYQZMJ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_SYTOT010", cQueryName = "SYTOT010",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ammontare lordo corrisposto",;
    HelpContextID = 698454,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=8, Top=433, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oSYTOT010_1_61.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Sytot010>=0)
    endwith
    return bRes
  endfunc

  add object oSYTOT011_1_62 as StdField with uid="DICFWXAPUC",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SYTOT011", cQueryName = "SYTOT011",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Somme non soggette a ritenuta per regime convenzionale",;
    HelpContextID = 698455,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=298, Top=433, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oSYTOT011_1_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Sytot011>=0)
    endwith
    return bRes
  endfunc

  add object oSYTOT012_1_63 as StdField with uid="QMFUFVQAGE",rtseq=44,rtrep=.f.,;
    cFormVar = "w_SYTOT012", cQueryName = "SYTOT012",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Altre somme non soggette a ritenute",;
    HelpContextID = 698456,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=649, Top=433, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oSYTOT012_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Sytot012>=0)
    endwith
    return bRes
  endfunc

  add object oSYTOT013_1_64 as StdField with uid="WOECNUYPNB",rtseq=45,rtrep=.f.,;
    cFormVar = "w_SYTOT013", cQueryName = "SYTOT013",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Imponibile",;
    HelpContextID = 698457,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=8, Top=499, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oSYTOT013_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Sytot013>=0)
    endwith
    return bRes
  endfunc

  add object oSYTOT014_1_65 as StdField with uid="TXSKUPXCUQ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_SYTOT014", cQueryName = "SYTOT014",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ritenute a titolo di imposta",;
    HelpContextID = 698458,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=298, Top=499, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oSYTOT014_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Sytot014>=0)
    endwith
    return bRes
  endfunc

  add object oSYTOT015_1_67 as StdField with uid="WRKFQLWOWO",rtseq=47,rtrep=.f.,;
    cFormVar = "w_SYTOT015", cQueryName = "SYTOT015",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ritenute sospese",;
    HelpContextID = 698459,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=649, Top=499, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oSYTOT015_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Sytot015>=0)
    endwith
    return bRes
  endfunc


  add object oObj_1_75 as cp_runprogram with uid="NBFRWAJSRD",left=-1, top=551, width=172,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BHE(w_HASEVCOP)",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Richiamato dall'area manuale declare (hascpevent)";
    , HelpContextID = 85070054


  add object oBtn_1_77 as StdButton with uid="WVWNIOAYSH",left=3, top=59, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla generazione file 770";
    , HelpContextID = 53697927;
    , tabstop=.f., caption='\<File 770';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_77.Click()
      with this.Parent.oContained
        Gsri_Bdy(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_77.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_Gdserial)        )
      endwith
    endif
  endfunc

  func oBtn_1_77.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_Gdserial))
     endwith
    endif
  endfunc

  add object oStr_1_29 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=29, Top=16,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ELWHFLJCPX",Visible=.t., Left=255, Top=16,;
    Alignment=1, Width=66, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="RINPZJSKLJ",Visible=.t., Left=181, Top=52,;
    Alignment=1, Width=140, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="OBGOXQVJWR",Visible=.t., Left=11, Top=142,;
    Alignment=0, Width=130, Height=17,;
    Caption="Ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="KOKISJQUOL",Visible=.t., Left=11, Top=209,;
    Alignment=0, Width=130, Height=17,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="GZLERJIXDA",Visible=.t., Left=334, Top=213,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=11, Top=270,;
    Alignment=0, Width=76, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="BBXIREUHWZ",Visible=.t., Left=695, Top=209,;
    Alignment=0, Width=76, Height=17,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="JYMPJPJYDE",Visible=.t., Left=228, Top=115,;
    Alignment=0, Width=407, Height=19,;
    Caption="Riservato al percipiente estero privo di codice fiscale"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_48 as StdString with uid="NOWYYKZWAZ",Visible=.t., Left=695, Top=325,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="WQAUIMBAAE",Visible=.t., Left=11, Top=321,;
    Alignment=0, Width=137, Height=17,;
    Caption="Localit� di residenza estera"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_50 as StdString with uid="HGABSVUXLY",Visible=.t., Left=334, Top=321,;
    Alignment=0, Width=139, Height=17,;
    Caption="Via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_51 as StdString with uid="XXZKENNKMI",Visible=.t., Left=334, Top=270,;
    Alignment=0, Width=204, Height=17,;
    Caption="Codice identificativo fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="YYMJTWBDUY",Visible=.t., Left=11, Top=414,;
    Alignment=0, Width=175, Height=17,;
    Caption="Ammontare lordo corrisposto"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_58 as StdString with uid="UGJJMMROVZ",Visible=.t., Left=298, Top=414,;
    Alignment=0, Width=282, Height=17,;
    Caption="Somme non soggette a ritenuta per regime convenzionale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_59 as StdString with uid="WZHQHWDYEB",Visible=.t., Left=11, Top=478,;
    Alignment=0, Width=149, Height=17,;
    Caption="Imponibile"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="MNYJGJHCOV",Visible=.t., Left=298, Top=478,;
    Alignment=0, Width=175, Height=17,;
    Caption="Ritenute a titolo di imposta"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_66 as StdString with uid="LOKMPZHYJR",Visible=.t., Left=649, Top=478,;
    Alignment=0, Width=146, Height=17,;
    Caption="Ritenute sospese"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="FINEJRYZNQ",Visible=.t., Left=263, Top=385,;
    Alignment=0, Width=306, Height=19,;
    Caption="Dettaglio valori"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_70 as StdString with uid="VAQZJDULDX",Visible=.t., Left=649, Top=414,;
    Alignment=0, Width=186, Height=17,;
    Caption="Altre somme non soggette a ritenuta"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_30 as StdBox with uid="RDMWYOEEFE",left=3, top=107, width=846,height=268

  add object oBox_1_68 as StdBox with uid="YAUEIGPEJC",left=3, top=372, width=846,height=161
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_ady','DATESTSY','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SYSERIAL=DATESTSY.SYSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
