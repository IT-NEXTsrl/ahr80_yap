* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_sft                                                        *
*              Generazione file telematico 770/2002                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_729]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-05                                                      *
* Last revis.: 2008-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_sft",oParentObject))

* --- Class definition
define class tgsri_sft as StdForm
  Top    = 2
  Left   = 7

  * --- Standard Properties
  Width  = 699
  Height = 446+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-26"
  HelpContextID=40498793
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=122

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  SEDIAZIE_IDX = 0
  TITOLARI_IDX = 0
  cPrg = "gsri_sft"
  cComment = "Generazione file telematico 770/2002"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_PERAZI = space(1)
  o_PERAZI = space(1)
  w_ROWNUM = 0
  w_PERFIS = space(5)
  w_TIPSL = space(2)
  w_SEDILEG = space(5)
  w_TIPFS = space(2)
  w_SEDIFIS = space(5)
  w_CodFisAzi = space(16)
  w_PERAZI2 = space(1)
  w_FOCOGNOME = space(24)
  o_FOCOGNOME = space(24)
  w_FONOME = space(20)
  o_FONOME = space(20)
  w_FODENOMINA = space(60)
  o_FODENOMINA = space(60)
  w_FOCODFIS = space(16)
  o_FOCODFIS = space(16)
  w_FLAGEURO = space(1)
  w_FLAGCOR = space(1)
  w_FLAGINT = space(1)
  w_FLAGEVE = space(2)
  w_FLAGCONF = space(1)
  w_Forza1 = .F.
  o_Forza1 = .F.
  w_TIPOPERAZ = space(1)
  w_DATANASC = ctod('  /  /  ')
  o_DATANASC = ctod('  /  /  ')
  w_COMUNE = space(40)
  o_COMUNE = space(40)
  w_SIGLA = space(2)
  o_SIGLA = space(2)
  w_SESSO = space(1)
  o_SESSO = space(1)
  w_VARRES = ctod('  /  /  ')
  w_RECOMUNE = space(40)
  o_RECOMUNE = space(40)
  w_RESIGLA = space(2)
  o_RESIGLA = space(2)
  w_TELEFONO1 = space(12)
  w_INDIRIZ = space(35)
  o_INDIRIZ = space(35)
  w_CAP = space(8)
  o_CAP = space(8)
  w_CODATT = space(5)
  w_TELEFAX1 = space(18)
  w_SEVARSED = ctod('  /  /  ')
  w_SECOMUNE = space(40)
  o_SECOMUNE = space(40)
  w_SESIGLA = space(2)
  o_SESIGLA = space(2)
  w_TELEFONO2 = space(12)
  w_SEINDIRI2 = space(35)
  o_SEINDIRI2 = space(35)
  w_SECAP = space(8)
  o_SECAP = space(8)
  w_TELEFAX2 = space(18)
  w_SEVARDOM = ctod('  /  /  ')
  w_SERCOMUN = space(40)
  o_SERCOMUN = space(40)
  w_SERSIGLA = space(2)
  o_SERSIGLA = space(2)
  w_SERCAP = space(8)
  o_SERCAP = space(8)
  w_NATGIU = space(2)
  w_SERINDIR = space(35)
  o_SERINDIR = space(35)
  w_SECODATT = space(5)
  w_STATO = space(1)
  w_SITUAZ = space(1)
  w_CODFISDA = space(11)
  w_ANNO = space(4)
  w_ESERCIZIO = space(4)
  w_VALUTAESE = space(3)
  w_VALUTA = space(1)
  w_CODVAL = space(3)
  w_decimi = 0
  w_TIPCON = space(1)
  w_RITE = space(1)
  w_TIPCLF = space(1)
  w_COGNOME = space(24)
  w_NOME = space(20)
  o_NOME = space(20)
  w_DENOMINA = space(60)
  w_ONLUS = space(1)
  w_SETATT = space(2)
  w_Forza2 = .F.
  o_Forza2 = .F.
  w_RFCODFIS = space(16)
  w_RFCODCAR = space(2)
  w_RFCOGNOME = space(24)
  w_RFNOME = space(20)
  w_RFSESSO = space(1)
  w_RFDATANASC = ctod('  /  /  ')
  w_RFCOMNAS = space(40)
  w_RFSIGNAS = space(2)
  w_RFCOMUNE = space(40)
  w_RFSIGLA = space(2)
  w_RFCAP = space(8)
  w_RFINDIRIZ = space(35)
  w_RFTELEFONO = space(12)
  w_qST1 = space(1)
  w_qSX1 = space(1)
  w_FIRMDICH = space(1)
  w_FIRMPRES = space(1)
  w_NUMCAF = 0
  w_RFDATIMP = ctod('  /  /  ')
  w_IMPTRTEL = space(1)
  w_IMPTRSOG = space(1)
  w_FIRMINT = space(1)
  w_TIPFORN = space(2)
  o_TIPFORN = space(2)
  w_CODFISIN = space(16)
  o_CODFISIN = space(16)
  w_FODATANASC = ctod('  /  /  ')
  w_FOSESSO = space(1)
  w_FOCOMUNE = space(40)
  w_FOSIGLA = space(2)
  w_FORECOMUNE = space(40)
  w_FORESIGLA = space(2)
  w_FOCAP = space(8)
  w_FOINDIRIZ = space(35)
  w_FOSECOMUNE = space(40)
  w_FOSESIGLA = space(2)
  w_FOSECAP = space(8)
  w_FOSEINDIRI2 = space(35)
  w_FOSERCOMUN = space(40)
  w_FOSERSIGLA = space(2)
  w_FOSERCAP = space(8)
  w_FOSERINDIR = space(35)
  w_CFRESCAF = space(16)
  w_VISTOA35 = space(1)
  w_FLAGFIR = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_DirName = space(200)
  w_FileName = space(30)
  w_IDESTA = 0
  w_PROIFT = 0
  w_CODAZI = space(5)
  w_PERCIN = space(15)
  w_DESCINI = space(40)
  w_PERCFIN = space(15)
  w_DESCFIN = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_TELEFONO = space(12)
  w_TELEFAX = space(12)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_sftPag1","gsri_sft",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pers.fis.\Altri sogg")
      .Pages(2).addobject("oPag","tgsri_sftPag2","gsri_sft",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Rapp.fimatario\Firma")
      .Pages(3).addobject("oPag","tgsri_sftPag3","gsri_sft",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Fornitore file")
      .Pages(4).addobject("oPag","tgsri_sftPag4","gsri_sft",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("File telematico")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFOCOGNOME_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='SEDIAZIE'
    this.cWorkTables[6]='TITOLARI'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsri_sft
    *this.opgfrm.tabstyle = 1
    this.opgfrm.page1.caption = ah_msgformat('Persone fisiche\Altri soggetti')
    this.opgfrm.page2.caption = ah_msgformat('Rappresentante firmatario\Firma')
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_PERAZI=space(1)
      .w_ROWNUM=0
      .w_PERFIS=space(5)
      .w_TIPSL=space(2)
      .w_SEDILEG=space(5)
      .w_TIPFS=space(2)
      .w_SEDIFIS=space(5)
      .w_CodFisAzi=space(16)
      .w_PERAZI2=space(1)
      .w_FOCOGNOME=space(24)
      .w_FONOME=space(20)
      .w_FODENOMINA=space(60)
      .w_FOCODFIS=space(16)
      .w_FLAGEURO=space(1)
      .w_FLAGCOR=space(1)
      .w_FLAGINT=space(1)
      .w_FLAGEVE=space(2)
      .w_FLAGCONF=space(1)
      .w_Forza1=.f.
      .w_TIPOPERAZ=space(1)
      .w_DATANASC=ctod("  /  /  ")
      .w_COMUNE=space(40)
      .w_SIGLA=space(2)
      .w_SESSO=space(1)
      .w_VARRES=ctod("  /  /  ")
      .w_RECOMUNE=space(40)
      .w_RESIGLA=space(2)
      .w_TELEFONO1=space(12)
      .w_INDIRIZ=space(35)
      .w_CAP=space(8)
      .w_CODATT=space(5)
      .w_TELEFAX1=space(18)
      .w_SEVARSED=ctod("  /  /  ")
      .w_SECOMUNE=space(40)
      .w_SESIGLA=space(2)
      .w_TELEFONO2=space(12)
      .w_SEINDIRI2=space(35)
      .w_SECAP=space(8)
      .w_TELEFAX2=space(18)
      .w_SEVARDOM=ctod("  /  /  ")
      .w_SERCOMUN=space(40)
      .w_SERSIGLA=space(2)
      .w_SERCAP=space(8)
      .w_NATGIU=space(2)
      .w_SERINDIR=space(35)
      .w_SECODATT=space(5)
      .w_STATO=space(1)
      .w_SITUAZ=space(1)
      .w_CODFISDA=space(11)
      .w_ANNO=space(4)
      .w_ESERCIZIO=space(4)
      .w_VALUTAESE=space(3)
      .w_VALUTA=space(1)
      .w_CODVAL=space(3)
      .w_decimi=0
      .w_TIPCON=space(1)
      .w_RITE=space(1)
      .w_TIPCLF=space(1)
      .w_COGNOME=space(24)
      .w_NOME=space(20)
      .w_DENOMINA=space(60)
      .w_ONLUS=space(1)
      .w_SETATT=space(2)
      .w_Forza2=.f.
      .w_RFCODFIS=space(16)
      .w_RFCODCAR=space(2)
      .w_RFCOGNOME=space(24)
      .w_RFNOME=space(20)
      .w_RFSESSO=space(1)
      .w_RFDATANASC=ctod("  /  /  ")
      .w_RFCOMNAS=space(40)
      .w_RFSIGNAS=space(2)
      .w_RFCOMUNE=space(40)
      .w_RFSIGLA=space(2)
      .w_RFCAP=space(8)
      .w_RFINDIRIZ=space(35)
      .w_RFTELEFONO=space(12)
      .w_qST1=space(1)
      .w_qSX1=space(1)
      .w_FIRMDICH=space(1)
      .w_FIRMPRES=space(1)
      .w_NUMCAF=0
      .w_RFDATIMP=ctod("  /  /  ")
      .w_IMPTRTEL=space(1)
      .w_IMPTRSOG=space(1)
      .w_FIRMINT=space(1)
      .w_TIPFORN=space(2)
      .w_CODFISIN=space(16)
      .w_FODATANASC=ctod("  /  /  ")
      .w_FOSESSO=space(1)
      .w_FOCOMUNE=space(40)
      .w_FOSIGLA=space(2)
      .w_FORECOMUNE=space(40)
      .w_FORESIGLA=space(2)
      .w_FOCAP=space(8)
      .w_FOINDIRIZ=space(35)
      .w_FOSECOMUNE=space(40)
      .w_FOSESIGLA=space(2)
      .w_FOSECAP=space(8)
      .w_FOSEINDIRI2=space(35)
      .w_FOSERCOMUN=space(40)
      .w_FOSERSIGLA=space(2)
      .w_FOSERCAP=space(8)
      .w_FOSERINDIR=space(35)
      .w_CFRESCAF=space(16)
      .w_VISTOA35=space(1)
      .w_FLAGFIR=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DirName=space(200)
      .w_FileName=space(30)
      .w_IDESTA=0
      .w_PROIFT=0
      .w_CODAZI=space(5)
      .w_PERCIN=space(15)
      .w_DESCINI=space(40)
      .w_PERCFIN=space(15)
      .w_DESCFIN=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_TELEFONO=space(12)
      .w_TELEFAX=space(12)
        .w_CODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_ROWNUM = 1
        .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PERFIS))
          .link_1_4('Full')
        endif
        .w_TIPSL = 'SL'
        .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_SEDILEG))
          .link_1_6('Full')
        endif
        .w_TIPFS = 'DF'
        .w_SEDIFIS = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SEDIFIS))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(80),80),space(80))
        .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
        .w_FLAGEURO = '1'
        .w_FLAGCOR = '0'
        .w_FLAGINT = '0'
        .w_FLAGEVE = '00'
        .w_FLAGCONF = '0'
        .w_Forza1 = iif(.w_Perazi<>'S',.w_Forza1,.f.)
        .w_TIPOPERAZ = ' '
        .w_DATANASC = iif(.w_Perazi='S' or .w_Forza1,.w_DATANASC,cp_CharToDate('  -  -  '))
        .w_COMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_COMUNE+space(40),40),space(40))
        .w_SIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_SIGLA,'  ')
        .w_SESSO = iif(.w_Perazi='S' or .w_Forza1,.w_SESSO,' ')
        .w_VARRES = iif(.w_Perazi='S' or .w_Forza1,.w_VARRES,cp_CharToDate('  -  -  '))
        .w_RECOMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_RECOMUNE+space(40),40),space(40))
        .w_RESIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_RESIGLA,'  ')
        .w_TELEFONO1 = iif(.w_Perazi='S' ,left(.w_TELEFONO1,12), space(12))
        .w_INDIRIZ = iif(.w_Perazi='S' or .w_Forza1,left(.w_INDIRIZ+space(35),35),space(35))
        .w_CAP = iif(.w_Perazi='S' or .w_Forza1,.w_CAP,space(8))
        .w_CODATT = iif(.w_Perazi='S' or .w_Forza1,.w_CODATT,space(5))
        .w_TELEFAX1 = iif(.w_Perazi='S' ,Left(.w_TELEFAX1,12), space(12))
        .w_SEVARSED = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARSED,cp_CharToDate('  -  -  '))
        .w_SECOMUNE = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SECOMUNE+space(40),40),space(40))
        .w_SESIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SESIGLA,'  ')
        .w_TELEFONO2 = iif(.w_Perazi<>'S' ,left(.w_TELEFONO2,12), space(12))
        .w_SEINDIRI2 = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SEINDIRI2+space(35),35),space(35))
        .w_SECAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECAP,space(8))
        .w_TELEFAX2 = iif(.w_Perazi<>'S' ,Left(.w_TELEFAX2,12), space(12))
        .w_SEVARDOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARDOM,cp_CharToDate('  -  -  '))
        .w_SERCOMUN = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERCOMUN+space(40),40),space(40))
        .w_SERSIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERSIGLA,'  ')
        .w_SERCAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCAP,space(8))
        .w_NATGIU = iif(.w_Perazi<>'S' or .w_Forza1,.w_NATGIU,'  ')
        .w_SERINDIR = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERINDIR+space(35),35),space(35))
        .w_SECODATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECODATT,space(5))
        .w_STATO = iif(.w_Perazi<>'S' or .w_Forza1,.w_STATO,' ')
        .w_SITUAZ = iif(.w_Perazi<>'S' or .w_Forza1,.w_SITUAZ,' ')
        .w_CODFISDA = iif(.w_Perazi<>'S' or .w_Forza1,.w_CODFISDA,space(11))
        .w_ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_ESERCIZIO = calceser(.w_DATFIN)
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_ESERCIZIO))
          .link_1_52('Full')
        endif
          .DoRTCalc(53,53,.f.)
        .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
        .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_PERVAL)
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_CODVAL))
          .link_1_55('Full')
        endif
          .DoRTCalc(56,56,.f.)
        .w_TIPCON = 'F'
          .DoRTCalc(58,62,.f.)
        .w_ONLUS = iif(.w_Perazi<>'S' or .w_Forza1,.w_ONLUS,' ')
        .w_SETATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SETATT,'  ')
        .w_Forza2 = iif(.w_Perazi<>'S',.w_Forza2,.f.)
        .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODFIS,space(16))
        .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODCAR,'  ')
        .w_RFCOGNOME = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOGNOME,space(24))
        .w_RFNOME = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFNOME,space(20))
        .w_RFSESSO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSESSO,' ')
        .w_RFDATANASC = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFDATANASC,cp_CharToDate('  -  -  '))
        .w_RFCOMNAS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMNAS,space(40))
        .w_RFSIGNAS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGNAS,'  ')
        .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMUNE,space(40))
        .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGLA,'  ')
        .w_RFCAP = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCAP,space(8))
        .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFINDIRIZ,space(35))
        .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFTELEFONO,space(12))
        .w_qST1 = '0'
        .w_qSX1 = '0'
        .w_FIRMDICH = '0'
        .w_FIRMPRES = '0'
        .w_NUMCAF = 0
        .w_RFDATIMP = cp_CharToDate('  -  -  ')
        .w_IMPTRTEL = '0'
        .w_IMPTRSOG = '0'
        .w_FIRMINT = '0'
        .w_TIPFORN = '02'
        .w_CODFISIN = iif(inlist(.w_TIPFORN,'01','02'),left(.w_FOCODFIS+space(16),16),space(16))
        .w_FODATANASC = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_DATANASC,cp_CharToDate('  -  -  '))
        .w_FOSESSO = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SESSO,' ')
        .w_FOCOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_COMUNE,space(40))
        .w_FOSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SIGLA,'  ')
        .w_FORECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RECOMUNE,space(40))
        .w_FORESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RESIGLA,'  ')
        .w_FOCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_CAP,space(8))
        .w_FOINDIRIZ = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_INDIRIZ,space(35))
        .w_FOSECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECOMUNE,space(40))
        .w_FOSESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SESIGLA,'  ')
        .w_FOSECAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECAP,space(8))
        .w_FOSEINDIRI2 = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SEINDIRI2,space(35))
        .w_FOSERCOMUN = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCOMUN,space(40))
        .w_FOSERSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERSIGLA,'  ')
        .w_FOSERCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCAP,space(8))
        .w_FOSERINDIR = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERINDIR,space(35))
        .w_CFRESCAF = SPACE(16)
        .w_VISTOA35 = '0'
        .w_FLAGFIR = '0'
        .w_DATINI = cp_CharToDate("01-01-2001")
        .w_DATFIN = cp_CharToDate("31-12-2001")
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_FileName = left(time(),2)+substr(time(),4,2)+right(time(),2)+'.770'
          .DoRTCalc(113,114,.f.)
        .w_CODAZI = i_codazi
        .DoRTCalc(116,116,.f.)
        if not(empty(.w_PERCIN))
          .link_4_9('Full')
        endif
        .DoRTCalc(117,118,.f.)
        if not(empty(.w_PERCFIN))
          .link_4_11('Full')
        endif
          .DoRTCalc(119,119,.f.)
        .w_OBTEST = .w_DATINI
      .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .w_TELEFONO = iif(.w_Perazi='S' , .w_TELEFONO1, .w_TELEFONO2)
        .w_TELEFAX = iif(.w_Perazi='S' , .w_TELEFAX1, .w_TELEFAX2)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page4.oPag.oBtn_4_4.enabled = this.oPgFrm.Page4.oPag.oBtn_4_4.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_26.enabled = this.oPgFrm.Page4.oPag.oBtn_4_26.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_27.enabled = this.oPgFrm.Page4.oPag.oBtn_4_27.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CODAZI<>.w_CODAZI
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_SEDIFIS = iif(.w_PERAZI<>'S',i_CODAZI,' ')
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
            .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        if .o_PERAZI<>.w_PERAZI
            .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(80),80),space(80))
        endif
        if .o_FOCODFIS<>.w_FOCODFIS
            .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
        endif
        .DoRTCalc(15,19,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_Forza1 = iif(.w_Perazi<>'S',.w_Forza1,.f.)
        endif
        .DoRTCalc(21,21,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_DATANASC = iif(.w_Perazi='S' or .w_Forza1,.w_DATANASC,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_COMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_COMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_SIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_NOME<>.w_NOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SESSO = iif(.w_Perazi='S' or .w_Forza1,.w_SESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_VARRES = iif(.w_Perazi='S' or .w_Forza1,.w_VARRES,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_RECOMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_RECOMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_RESIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_RESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_TELEFONO1 = iif(.w_Perazi='S' ,left(.w_TELEFONO1,12), space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_INDIRIZ = iif(.w_Perazi='S' or .w_Forza1,left(.w_INDIRIZ+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CAP = iif(.w_Perazi='S' or .w_Forza1,.w_CAP,space(8))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CODATT = iif(.w_Perazi='S' or .w_Forza1,.w_CODATT,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_TELEFAX1 = iif(.w_Perazi='S' ,Left(.w_TELEFAX1,12), space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SEVARSED = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARSED,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECOMUNE = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SECOMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SESIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_TELEFONO2 = iif(.w_Perazi<>'S' ,left(.w_TELEFONO2,12), space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SEINDIRI2 = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SEINDIRI2+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECAP,space(8))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_TELEFAX2 = iif(.w_Perazi<>'S' ,Left(.w_TELEFAX2,12), space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SEVARDOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARDOM,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERCOMUN = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERCOMUN+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERSIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERCAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCAP,space(8))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_NATGIU = iif(.w_Perazi<>'S' or .w_Forza1,.w_NATGIU,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERINDIR = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERINDIR+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECODATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECODATT,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_STATO = iif(.w_Perazi<>'S' or .w_Forza1,.w_STATO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SITUAZ = iif(.w_Perazi<>'S' or .w_Forza1,.w_SITUAZ,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CODFISDA = iif(.w_Perazi<>'S' or .w_Forza1,.w_CODFISDA,space(11))
        endif
        .DoRTCalc(51,51,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_ESERCIZIO = calceser(.w_DATFIN)
          .link_1_52('Full')
        endif
        .DoRTCalc(53,53,.t.)
            .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
            .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_PERVAL)
          .link_1_55('Full')
        .DoRTCalc(56,62,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_ONLUS = iif(.w_Perazi<>'S' or .w_Forza1,.w_ONLUS,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SETATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SETATT,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_Forza2 = iif(.w_Perazi<>'S',.w_Forza2,.f.)
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODFIS,space(16))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODCAR,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCOGNOME = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOGNOME,space(24))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFNOME = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFNOME,space(20))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFSESSO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFDATANASC = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFDATANASC,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCOMNAS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMNAS,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFSIGNAS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGNAS,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCAP = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCAP,space(8))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFINDIRIZ,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFTELEFONO,space(12))
        endif
        .DoRTCalc(79,88,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_FOCODFIS<>.w_FOCODFIS
            .w_CODFISIN = iif(inlist(.w_TIPFORN,'01','02'),left(.w_FOCODFIS+space(16),16),space(16))
        endif
        if .o_TIPFORN<>.w_TIPFORN.or. .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_DATANASC<>.w_DATANASC
            .w_FODATANASC = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_DATANASC,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SESSO<>.w_SESSO
            .w_FOSESSO = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_COMUNE<>.w_COMUNE
            .w_FOCOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_COMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SIGLA<>.w_SIGLA
            .w_FOSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_RECOMUNE<>.w_RECOMUNE
            .w_FORECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RECOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_RESIGLA<>.w_RESIGLA
            .w_FORESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_CAP<>.w_CAP
            .w_FOCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_CAP,space(8))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_INDIRIZ<>.w_INDIRIZ
            .w_FOINDIRIZ = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_INDIRIZ,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SECOMUNE<>.w_SECOMUNE
            .w_FOSECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SESIGLA<>.w_SESIGLA
            .w_FOSESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SECAP<>.w_SECAP
            .w_FOSECAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECAP,space(8))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SEINDIRI2<>.w_SEINDIRI2
            .w_FOSEINDIRI2 = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SEINDIRI2,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERCOMUN<>.w_SERCOMUN
            .w_FOSERCOMUN = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCOMUN,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERSIGLA<>.w_SERSIGLA
            .w_FOSERSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERCAP<>.w_SERCAP
            .w_FOSERCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCAP,space(8))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERINDIR<>.w_SERINDIR
            .w_FOSERINDIR = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERINDIR,space(35))
        endif
        .DoRTCalc(106,114,.t.)
            .w_CODAZI = i_codazi
          .link_4_9('Full')
        .DoRTCalc(117,117,.t.)
          .link_4_11('Full')
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .DoRTCalc(119,120,.t.)
            .w_TELEFONO = iif(.w_Perazi='S' , .w_TELEFONO1, .w_TELEFONO2)
            .w_TELEFAX = iif(.w_Perazi='S' , .w_TELEFAX1, .w_TELEFAX2)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.enabled = this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.mCond()
    this.oPgFrm.Page1.oPag.oFONOME_1_12.enabled = this.oPgFrm.Page1.oPag.oFONOME_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.enabled = this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.mCond()
    this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_21.enabled = this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_21.mCond()
    this.oPgFrm.Page1.oPag.oDATANASC_1_22.enabled = this.oPgFrm.Page1.oPag.oDATANASC_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCOMUNE_1_23.enabled = this.oPgFrm.Page1.oPag.oCOMUNE_1_23.mCond()
    this.oPgFrm.Page1.oPag.oSIGLA_1_24.enabled = this.oPgFrm.Page1.oPag.oSIGLA_1_24.mCond()
    this.oPgFrm.Page1.oPag.oSESSO_1_25.enabled = this.oPgFrm.Page1.oPag.oSESSO_1_25.mCond()
    this.oPgFrm.Page1.oPag.oVARRES_1_26.enabled = this.oPgFrm.Page1.oPag.oVARRES_1_26.mCond()
    this.oPgFrm.Page1.oPag.oRECOMUNE_1_27.enabled = this.oPgFrm.Page1.oPag.oRECOMUNE_1_27.mCond()
    this.oPgFrm.Page1.oPag.oRESIGLA_1_28.enabled = this.oPgFrm.Page1.oPag.oRESIGLA_1_28.mCond()
    this.oPgFrm.Page1.oPag.oTELEFONO1_1_29.enabled = this.oPgFrm.Page1.oPag.oTELEFONO1_1_29.mCond()
    this.oPgFrm.Page1.oPag.oINDIRIZ_1_30.enabled = this.oPgFrm.Page1.oPag.oINDIRIZ_1_30.mCond()
    this.oPgFrm.Page1.oPag.oCAP_1_31.enabled = this.oPgFrm.Page1.oPag.oCAP_1_31.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_32.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_32.mCond()
    this.oPgFrm.Page1.oPag.oTELEFAX1_1_33.enabled = this.oPgFrm.Page1.oPag.oTELEFAX1_1_33.mCond()
    this.oPgFrm.Page1.oPag.oSEVARSED_1_34.enabled = this.oPgFrm.Page1.oPag.oSEVARSED_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSECOMUNE_1_35.enabled = this.oPgFrm.Page1.oPag.oSECOMUNE_1_35.mCond()
    this.oPgFrm.Page1.oPag.oSESIGLA_1_36.enabled = this.oPgFrm.Page1.oPag.oSESIGLA_1_36.mCond()
    this.oPgFrm.Page1.oPag.oTELEFONO2_1_37.enabled = this.oPgFrm.Page1.oPag.oTELEFONO2_1_37.mCond()
    this.oPgFrm.Page1.oPag.oSEINDIRI2_1_38.enabled = this.oPgFrm.Page1.oPag.oSEINDIRI2_1_38.mCond()
    this.oPgFrm.Page1.oPag.oSECAP_1_39.enabled = this.oPgFrm.Page1.oPag.oSECAP_1_39.mCond()
    this.oPgFrm.Page1.oPag.oTELEFAX2_1_40.enabled = this.oPgFrm.Page1.oPag.oTELEFAX2_1_40.mCond()
    this.oPgFrm.Page1.oPag.oSEVARDOM_1_41.enabled = this.oPgFrm.Page1.oPag.oSEVARDOM_1_41.mCond()
    this.oPgFrm.Page1.oPag.oSERCOMUN_1_42.enabled = this.oPgFrm.Page1.oPag.oSERCOMUN_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSERSIGLA_1_43.enabled = this.oPgFrm.Page1.oPag.oSERSIGLA_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSERCAP_1_44.enabled = this.oPgFrm.Page1.oPag.oSERCAP_1_44.mCond()
    this.oPgFrm.Page1.oPag.oNATGIU_1_45.enabled = this.oPgFrm.Page1.oPag.oNATGIU_1_45.mCond()
    this.oPgFrm.Page1.oPag.oSERINDIR_1_46.enabled = this.oPgFrm.Page1.oPag.oSERINDIR_1_46.mCond()
    this.oPgFrm.Page1.oPag.oSECODATT_1_47.enabled = this.oPgFrm.Page1.oPag.oSECODATT_1_47.mCond()
    this.oPgFrm.Page1.oPag.oSTATO_1_48.enabled = this.oPgFrm.Page1.oPag.oSTATO_1_48.mCond()
    this.oPgFrm.Page1.oPag.oSITUAZ_1_49.enabled = this.oPgFrm.Page1.oPag.oSITUAZ_1_49.mCond()
    this.oPgFrm.Page1.oPag.oCODFISDA_1_50.enabled = this.oPgFrm.Page1.oPag.oCODFISDA_1_50.mCond()
    this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.enabled = this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.mCond()
    this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.enabled = this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.mCond()
    this.oPgFrm.Page2.oPag.oRFCOGNOME_2_4.enabled = this.oPgFrm.Page2.oPag.oRFCOGNOME_2_4.mCond()
    this.oPgFrm.Page2.oPag.oRFNOME_2_5.enabled = this.oPgFrm.Page2.oPag.oRFNOME_2_5.mCond()
    this.oPgFrm.Page2.oPag.oRFSESSO_2_6.enabled = this.oPgFrm.Page2.oPag.oRFSESSO_2_6.mCond()
    this.oPgFrm.Page2.oPag.oRFDATANASC_2_7.enabled = this.oPgFrm.Page2.oPag.oRFDATANASC_2_7.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMNAS_2_8.enabled = this.oPgFrm.Page2.oPag.oRFCOMNAS_2_8.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGNAS_2_9.enabled = this.oPgFrm.Page2.oPag.oRFSIGNAS_2_9.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMUNE_2_10.enabled = this.oPgFrm.Page2.oPag.oRFCOMUNE_2_10.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.enabled = this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.mCond()
    this.oPgFrm.Page2.oPag.oRFCAP_2_12.enabled = this.oPgFrm.Page2.oPag.oRFCAP_2_12.mCond()
    this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_13.enabled = this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_13.mCond()
    this.oPgFrm.Page2.oPag.oRFTELEFONO_2_14.enabled = this.oPgFrm.Page2.oPag.oRFTELEFONO_2_14.mCond()
    this.oPgFrm.Page3.oPag.oFODATANASC_3_3.enabled = this.oPgFrm.Page3.oPag.oFODATANASC_3_3.mCond()
    this.oPgFrm.Page3.oPag.oFOSESSO_3_4.enabled = this.oPgFrm.Page3.oPag.oFOSESSO_3_4.mCond()
    this.oPgFrm.Page3.oPag.oFOCOMUNE_3_5.enabled = this.oPgFrm.Page3.oPag.oFOCOMUNE_3_5.mCond()
    this.oPgFrm.Page3.oPag.oFOSIGLA_3_6.enabled = this.oPgFrm.Page3.oPag.oFOSIGLA_3_6.mCond()
    this.oPgFrm.Page3.oPag.oFORECOMUNE_3_7.enabled = this.oPgFrm.Page3.oPag.oFORECOMUNE_3_7.mCond()
    this.oPgFrm.Page3.oPag.oFORESIGLA_3_8.enabled = this.oPgFrm.Page3.oPag.oFORESIGLA_3_8.mCond()
    this.oPgFrm.Page3.oPag.oFOCAP_3_9.enabled = this.oPgFrm.Page3.oPag.oFOCAP_3_9.mCond()
    this.oPgFrm.Page3.oPag.oFOINDIRIZ_3_10.enabled = this.oPgFrm.Page3.oPag.oFOINDIRIZ_3_10.mCond()
    this.oPgFrm.Page3.oPag.oFOSECOMUNE_3_12.enabled = this.oPgFrm.Page3.oPag.oFOSECOMUNE_3_12.mCond()
    this.oPgFrm.Page3.oPag.oFOSESIGLA_3_13.enabled = this.oPgFrm.Page3.oPag.oFOSESIGLA_3_13.mCond()
    this.oPgFrm.Page3.oPag.oFOSECAP_3_14.enabled = this.oPgFrm.Page3.oPag.oFOSECAP_3_14.mCond()
    this.oPgFrm.Page3.oPag.oFOSEINDIRI2_3_15.enabled = this.oPgFrm.Page3.oPag.oFOSEINDIRI2_3_15.mCond()
    this.oPgFrm.Page3.oPag.oFOSERCOMUN_3_16.enabled = this.oPgFrm.Page3.oPag.oFOSERCOMUN_3_16.mCond()
    this.oPgFrm.Page3.oPag.oFOSERSIGLA_3_17.enabled = this.oPgFrm.Page3.oPag.oFOSERSIGLA_3_17.mCond()
    this.oPgFrm.Page3.oPag.oFOSERCAP_3_18.enabled = this.oPgFrm.Page3.oPag.oFOSERCAP_3_18.mCond()
    this.oPgFrm.Page3.oPag.oFOSERINDIR_3_19.enabled = this.oPgFrm.Page3.oPag.oFOSERINDIR_3_19.mCond()
    this.oPgFrm.Page4.oPag.oIDESTA_4_6.enabled = this.oPgFrm.Page4.oPag.oIDESTA_4_6.mCond()
    this.oPgFrm.Page4.oPag.oPROIFT_4_7.enabled = this.oPgFrm.Page4.oPag.oPROIFT_4_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_102.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZTELEFO,AZTELFAX";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZTELEFO,AZTELFAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_CodFisAzi = NVL(_Link_.AZCOFAZI,space(16))
      this.w_FODENOMINA = NVL(_Link_.AZRAGAZI,space(60))
      this.w_PERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_RFCODCAR = NVL(_Link_.AZIVACAR,space(2))
      this.w_TELEFONO1 = NVL(_Link_.AZTELEFO,space(12))
      this.w_TELEFAX1 = NVL(_Link_.AZTELFAX,space(18))
      this.w_TELEFONO2 = NVL(_Link_.AZTELEFO,space(12))
      this.w_TELEFAX2 = NVL(_Link_.AZTELFAX,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CodFisAzi = space(16)
      this.w_FODENOMINA = space(60)
      this.w_PERAZI = space(1)
      this.w_RFCODCAR = space(2)
      this.w_TELEFONO1 = space(12)
      this.w_TELEFAX1 = space(18)
      this.w_TELEFONO2 = space(12)
      this.w_TELEFAX2 = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERFIS
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTCOGTIT,TTNOMTIT";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_PERFIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_PERFIS)
            select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTCOGTIT,TTNOMTIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERFIS = NVL(_Link_.TTCODAZI,space(5))
      this.w_DATANASC = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_SESSO = NVL(_Link_.TT_SESSO,space(1))
      this.w_COMUNE = NVL(_Link_.TTLUONAS,space(40))
      this.w_SIGLA = NVL(_Link_.TTPRONAS,space(2))
      this.w_RECOMUNE = NVL(_Link_.TTLOCTIT,space(40))
      this.w_RESIGLA = NVL(_Link_.TTPROTIT,space(2))
      this.w_INDIRIZ = NVL(_Link_.TTINDIRI,space(35))
      this.w_CAP = NVL(_Link_.TTCAPTIT,space(8))
      this.w_FOCOGNOME = NVL(_Link_.TTCOGTIT,space(24))
      this.w_FONOME = NVL(_Link_.TTNOMTIT,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PERFIS = space(5)
      endif
      this.w_DATANASC = ctod("  /  /  ")
      this.w_SESSO = space(1)
      this.w_COMUNE = space(40)
      this.w_SIGLA = space(2)
      this.w_RECOMUNE = space(40)
      this.w_RESIGLA = space(2)
      this.w_INDIRIZ = space(35)
      this.w_CAP = space(8)
      this.w_FOCOGNOME = space(24)
      this.w_FONOME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDILEG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDILEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDILEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDILEG);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPSL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPSL;
                       ,'SECODAZI',this.w_SEDILEG)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDILEG = NVL(_Link_.SECODAZI,space(5))
      this.w_SECOMUNE = NVL(_Link_.SELOCALI,space(40))
      this.w_SESIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SEINDIRI2 = NVL(_Link_.SEINDIRI,space(35))
      this.w_SECAP = NVL(_Link_.SE___CAP,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_SEDILEG = space(5)
      endif
      this.w_SECOMUNE = space(40)
      this.w_SESIGLA = space(2)
      this.w_SEINDIRI2 = space(35)
      this.w_SECAP = space(8)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDILEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDIFIS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDIFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDIFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDIFIS);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPFS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPFS;
                       ,'SECODAZI',this.w_SEDIFIS)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDIFIS = NVL(_Link_.SECODAZI,space(5))
      this.w_SERCOMUN = NVL(_Link_.SELOCALI,space(40))
      this.w_SERSIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SERINDIR = NVL(_Link_.SEINDIRI,space(35))
      this.w_SERCAP = NVL(_Link_.SE___CAP,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_SEDIFIS = space(5)
      endif
      this.w_SERCOMUN = space(40)
      this.w_SERSIGLA = space(2)
      this.w_SERINDIR = space(35)
      this.w_SERCAP = space(8)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDIFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCIZIO
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCIZIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCIZIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCIZIO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCIZIO)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCIZIO = NVL(_Link_.ESCODESE,space(4))
      this.w_VALUTAESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCIZIO = space(4)
      endif
      this.w_VALUTAESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCIZIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_decimi = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCIN
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCIN = space(15)
      endif
      this.w_DESCINI = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCIN = space(15)
        this.w_DESCINI = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCFIN
  func Link_4_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCFIN = space(15)
      endif
      this.w_DESCFIN = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCFIN = space(15)
        this.w_DESCFIN = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value==this.w_FOCOGNOME)
      this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value=this.w_FOCOGNOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFONOME_1_12.value==this.w_FONOME)
      this.oPgFrm.Page1.oPag.oFONOME_1_12.value=this.w_FONOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value==this.w_FODENOMINA)
      this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value=this.w_FODENOMINA
    endif
    if not(this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value==this.w_FOCODFIS)
      this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value=this.w_FOCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGEURO_1_15.RadioValue()==this.w_FLAGEURO)
      this.oPgFrm.Page1.oPag.oFLAGEURO_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCOR_1_16.RadioValue()==this.w_FLAGCOR)
      this.oPgFrm.Page1.oPag.oFLAGCOR_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGINT_1_17.RadioValue()==this.w_FLAGINT)
      this.oPgFrm.Page1.oPag.oFLAGINT_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGEVE_1_18.RadioValue()==this.w_FLAGEVE)
      this.oPgFrm.Page1.oPag.oFLAGEVE_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCONF_1_19.RadioValue()==this.w_FLAGCONF)
      this.oPgFrm.Page1.oPag.oFLAGCONF_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oForza1_1_20.RadioValue()==this.w_Forza1)
      this.oPgFrm.Page1.oPag.oForza1_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_21.RadioValue()==this.w_TIPOPERAZ)
      this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATANASC_1_22.value==this.w_DATANASC)
      this.oPgFrm.Page1.oPag.oDATANASC_1_22.value=this.w_DATANASC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMUNE_1_23.value==this.w_COMUNE)
      this.oPgFrm.Page1.oPag.oCOMUNE_1_23.value=this.w_COMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSIGLA_1_24.value==this.w_SIGLA)
      this.oPgFrm.Page1.oPag.oSIGLA_1_24.value=this.w_SIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSESSO_1_25.RadioValue()==this.w_SESSO)
      this.oPgFrm.Page1.oPag.oSESSO_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVARRES_1_26.value==this.w_VARRES)
      this.oPgFrm.Page1.oPag.oVARRES_1_26.value=this.w_VARRES
    endif
    if not(this.oPgFrm.Page1.oPag.oRECOMUNE_1_27.value==this.w_RECOMUNE)
      this.oPgFrm.Page1.oPag.oRECOMUNE_1_27.value=this.w_RECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oRESIGLA_1_28.value==this.w_RESIGLA)
      this.oPgFrm.Page1.oPag.oRESIGLA_1_28.value=this.w_RESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oTELEFONO1_1_29.value==this.w_TELEFONO1)
      this.oPgFrm.Page1.oPag.oTELEFONO1_1_29.value=this.w_TELEFONO1
    endif
    if not(this.oPgFrm.Page1.oPag.oINDIRIZ_1_30.value==this.w_INDIRIZ)
      this.oPgFrm.Page1.oPag.oINDIRIZ_1_30.value=this.w_INDIRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCAP_1_31.value==this.w_CAP)
      this.oPgFrm.Page1.oPag.oCAP_1_31.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_32.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_32.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oTELEFAX1_1_33.value==this.w_TELEFAX1)
      this.oPgFrm.Page1.oPag.oTELEFAX1_1_33.value=this.w_TELEFAX1
    endif
    if not(this.oPgFrm.Page1.oPag.oSEVARSED_1_34.value==this.w_SEVARSED)
      this.oPgFrm.Page1.oPag.oSEVARSED_1_34.value=this.w_SEVARSED
    endif
    if not(this.oPgFrm.Page1.oPag.oSECOMUNE_1_35.value==this.w_SECOMUNE)
      this.oPgFrm.Page1.oPag.oSECOMUNE_1_35.value=this.w_SECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSESIGLA_1_36.value==this.w_SESIGLA)
      this.oPgFrm.Page1.oPag.oSESIGLA_1_36.value=this.w_SESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oTELEFONO2_1_37.value==this.w_TELEFONO2)
      this.oPgFrm.Page1.oPag.oTELEFONO2_1_37.value=this.w_TELEFONO2
    endif
    if not(this.oPgFrm.Page1.oPag.oSEINDIRI2_1_38.value==this.w_SEINDIRI2)
      this.oPgFrm.Page1.oPag.oSEINDIRI2_1_38.value=this.w_SEINDIRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oSECAP_1_39.value==this.w_SECAP)
      this.oPgFrm.Page1.oPag.oSECAP_1_39.value=this.w_SECAP
    endif
    if not(this.oPgFrm.Page1.oPag.oTELEFAX2_1_40.value==this.w_TELEFAX2)
      this.oPgFrm.Page1.oPag.oTELEFAX2_1_40.value=this.w_TELEFAX2
    endif
    if not(this.oPgFrm.Page1.oPag.oSEVARDOM_1_41.value==this.w_SEVARDOM)
      this.oPgFrm.Page1.oPag.oSEVARDOM_1_41.value=this.w_SEVARDOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCOMUN_1_42.value==this.w_SERCOMUN)
      this.oPgFrm.Page1.oPag.oSERCOMUN_1_42.value=this.w_SERCOMUN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERSIGLA_1_43.value==this.w_SERSIGLA)
      this.oPgFrm.Page1.oPag.oSERSIGLA_1_43.value=this.w_SERSIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCAP_1_44.value==this.w_SERCAP)
      this.oPgFrm.Page1.oPag.oSERCAP_1_44.value=this.w_SERCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oNATGIU_1_45.value==this.w_NATGIU)
      this.oPgFrm.Page1.oPag.oNATGIU_1_45.value=this.w_NATGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oSERINDIR_1_46.value==this.w_SERINDIR)
      this.oPgFrm.Page1.oPag.oSERINDIR_1_46.value=this.w_SERINDIR
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODATT_1_47.value==this.w_SECODATT)
      this.oPgFrm.Page1.oPag.oSECODATT_1_47.value=this.w_SECODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_48.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSITUAZ_1_49.RadioValue()==this.w_SITUAZ)
      this.oPgFrm.Page1.oPag.oSITUAZ_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFISDA_1_50.value==this.w_CODFISDA)
      this.oPgFrm.Page1.oPag.oCODFISDA_1_50.value=this.w_CODFISDA
    endif
    if not(this.oPgFrm.Page2.oPag.oForza2_2_1.RadioValue()==this.w_Forza2)
      this.oPgFrm.Page2.oPag.oForza2_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.value==this.w_RFCODFIS)
      this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.value=this.w_RFCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.value==this.w_RFCODCAR)
      this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.value=this.w_RFCODCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOGNOME_2_4.value==this.w_RFCOGNOME)
      this.oPgFrm.Page2.oPag.oRFCOGNOME_2_4.value=this.w_RFCOGNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFNOME_2_5.value==this.w_RFNOME)
      this.oPgFrm.Page2.oPag.oRFNOME_2_5.value=this.w_RFNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSESSO_2_6.RadioValue()==this.w_RFSESSO)
      this.oPgFrm.Page2.oPag.oRFSESSO_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATANASC_2_7.value==this.w_RFDATANASC)
      this.oPgFrm.Page2.oPag.oRFDATANASC_2_7.value=this.w_RFDATANASC
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMNAS_2_8.value==this.w_RFCOMNAS)
      this.oPgFrm.Page2.oPag.oRFCOMNAS_2_8.value=this.w_RFCOMNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGNAS_2_9.value==this.w_RFSIGNAS)
      this.oPgFrm.Page2.oPag.oRFSIGNAS_2_9.value=this.w_RFSIGNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMUNE_2_10.value==this.w_RFCOMUNE)
      this.oPgFrm.Page2.oPag.oRFCOMUNE_2_10.value=this.w_RFCOMUNE
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.value==this.w_RFSIGLA)
      this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.value=this.w_RFSIGLA
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCAP_2_12.value==this.w_RFCAP)
      this.oPgFrm.Page2.oPag.oRFCAP_2_12.value=this.w_RFCAP
    endif
    if not(this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_13.value==this.w_RFINDIRIZ)
      this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_13.value=this.w_RFINDIRIZ
    endif
    if not(this.oPgFrm.Page2.oPag.oRFTELEFONO_2_14.value==this.w_RFTELEFONO)
      this.oPgFrm.Page2.oPag.oRFTELEFONO_2_14.value=this.w_RFTELEFONO
    endif
    if not(this.oPgFrm.Page2.oPag.oqST1_2_15.RadioValue()==this.w_qST1)
      this.oPgFrm.Page2.oPag.oqST1_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSX1_2_16.RadioValue()==this.w_qSX1)
      this.oPgFrm.Page2.oPag.oqSX1_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMDICH_2_17.RadioValue()==this.w_FIRMDICH)
      this.oPgFrm.Page2.oPag.oFIRMDICH_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMPRES_2_18.RadioValue()==this.w_FIRMPRES)
      this.oPgFrm.Page2.oPag.oFIRMPRES_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMCAF_2_19.value==this.w_NUMCAF)
      this.oPgFrm.Page2.oPag.oNUMCAF_2_19.value=this.w_NUMCAF
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATIMP_2_20.value==this.w_RFDATIMP)
      this.oPgFrm.Page2.oPag.oRFDATIMP_2_20.value=this.w_RFDATIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPTRTEL_2_21.RadioValue()==this.w_IMPTRTEL)
      this.oPgFrm.Page2.oPag.oIMPTRTEL_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPTRSOG_2_22.RadioValue()==this.w_IMPTRSOG)
      this.oPgFrm.Page2.oPag.oIMPTRSOG_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMINT_2_23.RadioValue()==this.w_FIRMINT)
      this.oPgFrm.Page2.oPag.oFIRMINT_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPFORN_3_1.RadioValue()==this.w_TIPFORN)
      this.oPgFrm.Page3.oPag.oTIPFORN_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCODFISIN_3_2.value==this.w_CODFISIN)
      this.oPgFrm.Page3.oPag.oCODFISIN_3_2.value=this.w_CODFISIN
    endif
    if not(this.oPgFrm.Page3.oPag.oFODATANASC_3_3.value==this.w_FODATANASC)
      this.oPgFrm.Page3.oPag.oFODATANASC_3_3.value=this.w_FODATANASC
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSESSO_3_4.RadioValue()==this.w_FOSESSO)
      this.oPgFrm.Page3.oPag.oFOSESSO_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFOCOMUNE_3_5.value==this.w_FOCOMUNE)
      this.oPgFrm.Page3.oPag.oFOCOMUNE_3_5.value=this.w_FOCOMUNE
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSIGLA_3_6.value==this.w_FOSIGLA)
      this.oPgFrm.Page3.oPag.oFOSIGLA_3_6.value=this.w_FOSIGLA
    endif
    if not(this.oPgFrm.Page3.oPag.oFORECOMUNE_3_7.value==this.w_FORECOMUNE)
      this.oPgFrm.Page3.oPag.oFORECOMUNE_3_7.value=this.w_FORECOMUNE
    endif
    if not(this.oPgFrm.Page3.oPag.oFORESIGLA_3_8.value==this.w_FORESIGLA)
      this.oPgFrm.Page3.oPag.oFORESIGLA_3_8.value=this.w_FORESIGLA
    endif
    if not(this.oPgFrm.Page3.oPag.oFOCAP_3_9.value==this.w_FOCAP)
      this.oPgFrm.Page3.oPag.oFOCAP_3_9.value=this.w_FOCAP
    endif
    if not(this.oPgFrm.Page3.oPag.oFOINDIRIZ_3_10.value==this.w_FOINDIRIZ)
      this.oPgFrm.Page3.oPag.oFOINDIRIZ_3_10.value=this.w_FOINDIRIZ
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSECOMUNE_3_12.value==this.w_FOSECOMUNE)
      this.oPgFrm.Page3.oPag.oFOSECOMUNE_3_12.value=this.w_FOSECOMUNE
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSESIGLA_3_13.value==this.w_FOSESIGLA)
      this.oPgFrm.Page3.oPag.oFOSESIGLA_3_13.value=this.w_FOSESIGLA
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSECAP_3_14.value==this.w_FOSECAP)
      this.oPgFrm.Page3.oPag.oFOSECAP_3_14.value=this.w_FOSECAP
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSEINDIRI2_3_15.value==this.w_FOSEINDIRI2)
      this.oPgFrm.Page3.oPag.oFOSEINDIRI2_3_15.value=this.w_FOSEINDIRI2
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSERCOMUN_3_16.value==this.w_FOSERCOMUN)
      this.oPgFrm.Page3.oPag.oFOSERCOMUN_3_16.value=this.w_FOSERCOMUN
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSERSIGLA_3_17.value==this.w_FOSERSIGLA)
      this.oPgFrm.Page3.oPag.oFOSERSIGLA_3_17.value=this.w_FOSERSIGLA
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSERCAP_3_18.value==this.w_FOSERCAP)
      this.oPgFrm.Page3.oPag.oFOSERCAP_3_18.value=this.w_FOSERCAP
    endif
    if not(this.oPgFrm.Page3.oPag.oFOSERINDIR_3_19.value==this.w_FOSERINDIR)
      this.oPgFrm.Page3.oPag.oFOSERINDIR_3_19.value=this.w_FOSERINDIR
    endif
    if not(this.oPgFrm.Page3.oPag.oCFRESCAF_3_20.value==this.w_CFRESCAF)
      this.oPgFrm.Page3.oPag.oCFRESCAF_3_20.value=this.w_CFRESCAF
    endif
    if not(this.oPgFrm.Page3.oPag.oVISTOA35_3_21.RadioValue()==this.w_VISTOA35)
      this.oPgFrm.Page3.oPag.oVISTOA35_3_21.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFLAGFIR_3_22.RadioValue()==this.w_FLAGFIR)
      this.oPgFrm.Page3.oPag.oFLAGFIR_3_22.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDATINI_4_1.value==this.w_DATINI)
      this.oPgFrm.Page4.oPag.oDATINI_4_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page4.oPag.oDATFIN_4_2.value==this.w_DATFIN)
      this.oPgFrm.Page4.oPag.oDATFIN_4_2.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page4.oPag.oDirName_4_3.value==this.w_DirName)
      this.oPgFrm.Page4.oPag.oDirName_4_3.value=this.w_DirName
    endif
    if not(this.oPgFrm.Page4.oPag.oFileName_4_5.value==this.w_FileName)
      this.oPgFrm.Page4.oPag.oFileName_4_5.value=this.w_FileName
    endif
    if not(this.oPgFrm.Page4.oPag.oIDESTA_4_6.value==this.w_IDESTA)
      this.oPgFrm.Page4.oPag.oIDESTA_4_6.value=this.w_IDESTA
    endif
    if not(this.oPgFrm.Page4.oPag.oPROIFT_4_7.value==this.w_PROIFT)
      this.oPgFrm.Page4.oPag.oPROIFT_4_7.value=this.w_PROIFT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FOCOGNOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCOGNOME_1_11.SetFocus()
            i_bnoObbl = !empty(.w_FOCOGNOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FONOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFONOME_1_12.SetFocus()
            i_bnoObbl = !empty(.w_FONOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FODENOMINA)) or not(not empty(.w_FODENOMINA)))  and (empty(.w_FOCOGNOME) and empty(.w_FONOME))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFODENOMINA_1_13.SetFocus()
            i_bnoObbl = !empty(.w_FODENOMINA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FOCODFIS)) or not(iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODFIS_1_14.SetFocus()
            i_bnoObbl = !empty(.w_FOCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lunghezza codice fiscale o partita IVA non corretta")
          case   (empty(.w_DATANASC))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATANASC_1_22.SetFocus()
            i_bnoObbl = !empty(.w_DATANASC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COMUNE))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMUNE_1_23.SetFocus()
            i_bnoObbl = !empty(.w_COMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SESSO))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSESSO_1_25.SetFocus()
            i_bnoObbl = !empty(.w_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RECOMUNE))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRECOMUNE_1_27.SetFocus()
            i_bnoObbl = !empty(.w_RECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_TELEFONO1))=0 and at('/',alltrim(.w_TELEFONO1))=0 and at('-',alltrim(.w_TELEFONO1))=0)  and (.w_Perazi='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTELEFONO1_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(at(' ',alltrim(.w_TELEFAX1))=0 and at('/',alltrim(.w_TELEFAX1))=0 and at('-',alltrim(.w_TELEFAX1))=0)  and (.w_Perazi='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTELEFAX1_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   (empty(.w_SECOMUNE))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECOMUNE_1_35.SetFocus()
            i_bnoObbl = !empty(.w_SECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_TELEFONO2))=0 and at('/',alltrim(.w_TELEFONO2))=0 and at('-',alltrim(.w_TELEFONO2))=0)  and (.w_Perazi<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTELEFONO2_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(at(' ',alltrim(.w_TELEFAX2))=0 and at('/',alltrim(.w_TELEFAX2))=0 and at('-',alltrim(.w_TELEFAX2))=0)  and (.w_Perazi<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTELEFAX2_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=43) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=51))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNATGIU_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso per natura giuridica")
          case   not(iif(not empty(.w_CODFISDA),chkcfp(alltrim(.w_CODFISDA),'PI'),.T.))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFISDA_1_50.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))  and (.w_PERAZI <> 'S' or .w_Forza2)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCODFIS_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(val(.w_RFCODCAR)>=1 AND VAL(.w_RFCODCAR)<=12 or empty(.w_RFCODCAR))  and (.w_PERAZI <> 'S' or .w_Forza2)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCODCAR_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non ammesso, vedi legenda fondo pagina")
          case   not(at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)  and (.w_PERAZI <> 'S' or .w_Forza2)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFTELEFONO_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODFISIN_3_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_CFRESCAF),chkcfp(alltrim(.w_CFRESCAF),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCFRESCAF_3_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATINI)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATINI < cp_CharToDate("01-01-02"))))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDATINI_4_1.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   ((empty(.w_DATFIN)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN < cp_CharToDate("01-01-02"))))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDATFIN_4_2.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   (empty(.w_IDESTA))  and (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oIDESTA_4_6.SetFocus()
            i_bnoObbl = !empty(.w_IDESTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PROIFT))  and (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPROIFT_4_7.SetFocus()
            i_bnoObbl = !empty(.w_PROIFT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPERCIN_4_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
          case   not(((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCFIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPERCFIN_4_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsri_sft
      if i_bRes=.t.
         this.NotifyEvent("Lancia")
         *** Al termine non deve chiudere la Maschera
         return .f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_PERAZI = this.w_PERAZI
    this.o_FOCOGNOME = this.w_FOCOGNOME
    this.o_FONOME = this.w_FONOME
    this.o_FODENOMINA = this.w_FODENOMINA
    this.o_FOCODFIS = this.w_FOCODFIS
    this.o_Forza1 = this.w_Forza1
    this.o_DATANASC = this.w_DATANASC
    this.o_COMUNE = this.w_COMUNE
    this.o_SIGLA = this.w_SIGLA
    this.o_SESSO = this.w_SESSO
    this.o_RECOMUNE = this.w_RECOMUNE
    this.o_RESIGLA = this.w_RESIGLA
    this.o_INDIRIZ = this.w_INDIRIZ
    this.o_CAP = this.w_CAP
    this.o_SECOMUNE = this.w_SECOMUNE
    this.o_SESIGLA = this.w_SESIGLA
    this.o_SEINDIRI2 = this.w_SEINDIRI2
    this.o_SECAP = this.w_SECAP
    this.o_SERCOMUN = this.w_SERCOMUN
    this.o_SERSIGLA = this.w_SERSIGLA
    this.o_SERCAP = this.w_SERCAP
    this.o_SERINDIR = this.w_SERINDIR
    this.o_NOME = this.w_NOME
    this.o_Forza2 = this.w_Forza2
    this.o_TIPFORN = this.w_TIPFORN
    this.o_CODFISIN = this.w_CODFISIN
    this.o_DATFIN = this.w_DATFIN
    return

enddefine

* --- Define pages as container
define class tgsri_sftPag1 as StdContainer
  Width  = 695
  height = 447
  stdWidth  = 695
  stdheight = 447
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFOCOGNOME_1_11 as StdField with uid="SZIDZXIFTU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FOCOGNOME", cQueryName = "FOCOGNOME",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del fornitore",;
    HelpContextID = 262565389,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=101, Top=10, cSayPict="repl('!',24)", cGetPict="repl('!',24)", InputMask=replicate('X',24)

  func oFOCOGNOME_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFONOME_1_12 as StdField with uid="NPXPQLRYZB",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FONOME", cQueryName = "FONOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del fornitore",;
    HelpContextID = 138789546,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=385, Top=10, cSayPict="repl('!',20)", cGetPict="repl('!',20)", InputMask=replicate('X',20)

  func oFONOME_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFODENOMINA_1_13 as StdField with uid="WNCFJWGGXA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FODENOMINA", cQueryName = "FODENOMINA",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione del fornitore",;
    HelpContextID = 239082625,;
   bGlobalFont=.t.,;
    Height=34, Width=433, Left=101, Top=32, cSayPict="repl('!',80)", cGetPict="repl('!',80)", InputMask=replicate('X',60)

  func oFODENOMINA_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FOCOGNOME) and empty(.w_FONOME))
    endwith
   endif
  endfunc

  func oFODENOMINA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_FODENOMINA))
    endwith
    return bRes
  endfunc

  add object oFOCODFIS_1_14 as StdField with uid="NQSHPZVXNW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FOCODFIS", cQueryName = "FOCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Lunghezza codice fiscale o partita IVA non corretta",;
    HelpContextID = 131494487,;
   bGlobalFont=.t.,;
    Height=21, Width=144, Left=540, Top=32, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oFOCODFIS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI')))
    endwith
    return bRes
  endfunc

  add object oFLAGEURO_1_15 as StdCheck with uid="DBOTAUNOGI",rtseq=15,rtrep=.f.,left=23, top=95, caption="Importi espressi in unit� di Euro",;
    HelpContextID = 120679077,;
    cFormVar="w_FLAGEURO", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGEURO_1_15.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGEURO_1_15.GetRadio()
    this.Parent.oContained.w_FLAGEURO = this.RadioValue()
    return .t.
  endfunc

  func oFLAGEURO_1_15.SetRadio()
    this.Parent.oContained.w_FLAGEURO=trim(this.Parent.oContained.w_FLAGEURO)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGEURO=='1',1,;
      0)
  endfunc

  add object oFLAGCOR_1_16 as StdCheck with uid="KAIBPTXLER",rtseq=16,rtrep=.f.,left=416, top=95, caption="Correttiva",;
    HelpContextID = 17918550,;
    cFormVar="w_FLAGCOR", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCOR_1_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCOR_1_16.GetRadio()
    this.Parent.oContained.w_FLAGCOR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCOR_1_16.SetRadio()
    this.Parent.oContained.w_FLAGCOR=trim(this.Parent.oContained.w_FLAGCOR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCOR=='1',1,;
      0)
  endfunc

  add object oFLAGINT_1_17 as StdCheck with uid="SSJZDRXGKM",rtseq=17,rtrep=.f.,left=416, top=116, caption="Integrativa",;
    HelpContextID = 7432790,;
    cFormVar="w_FLAGINT", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGINT_1_17.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGINT_1_17.GetRadio()
    this.Parent.oContained.w_FLAGINT = this.RadioValue()
    return .t.
  endfunc

  func oFLAGINT_1_17.SetRadio()
    this.Parent.oContained.w_FLAGINT=trim(this.Parent.oContained.w_FLAGINT)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGINT=='1',1,;
      0)
  endfunc

  add object oFLAGEVE_1_18 as StdCheck with uid="WYTEXMRWTY",rtseq=18,rtrep=.f.,left=23, top=116, caption="Eventi eccezionali",;
    HelpContextID = 137456214,;
    cFormVar="w_FLAGEVE", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGEVE_1_18.RadioValue()
    return(iif(this.value =1,'01',;
    '00'))
  endfunc
  func oFLAGEVE_1_18.GetRadio()
    this.Parent.oContained.w_FLAGEVE = this.RadioValue()
    return .t.
  endfunc

  func oFLAGEVE_1_18.SetRadio()
    this.Parent.oContained.w_FLAGEVE=trim(this.Parent.oContained.w_FLAGEVE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGEVE=='01',1,;
      0)
  endfunc

  add object oFLAGCONF_1_19 as StdCheck with uid="EXJMLGCXGO",rtseq=19,rtrep=.f.,left=270, top=95, caption="Conferma",;
    HelpContextID = 250516836,;
    cFormVar="w_FLAGCONF", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCONF_1_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCONF_1_19.GetRadio()
    this.Parent.oContained.w_FLAGCONF = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCONF_1_19.SetRadio()
    this.Parent.oContained.w_FLAGCONF=trim(this.Parent.oContained.w_FLAGCONF)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCONF=='1',1,;
      0)
  endfunc

  add object oForza1_1_20 as StdCheck with uid="WYTWDOWGTW",rtseq=20,rtrep=.f.,left=270, top=116, caption="Forza editing",;
    HelpContextID = 181953194,;
    cFormVar="w_Forza1", bObbl = .f. , nPag = 1;
    , tabstop = .f.;
   , bGlobalFont=.t.


  func oForza1_1_20.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oForza1_1_20.GetRadio()
    this.Parent.oContained.w_Forza1 = this.RadioValue()
    return .t.
  endfunc

  func oForza1_1_20.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Forza1==.t.,1,;
      0)
  endfunc


  add object oTIPOPERAZ_1_21 as StdCombo with uid="FVZJLSCZCJ",rtseq=21,rtrep=.f.,left=544,top=111,width=114,height=21;
    , tabstop =.f.;
    , ToolTipText = "Tipo operazioni possibili in correzione o integrazione";
    , HelpContextID = 132800023;
    , cFormVar="w_TIPOPERAZ",RowSource=""+"Inserimento,"+"Aggiornamento,"+"Cancellazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPERAZ_1_21.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    ' '))))
  endfunc
  func oTIPOPERAZ_1_21.GetRadio()
    this.Parent.oContained.w_TIPOPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPERAZ_1_21.SetRadio()
    this.Parent.oContained.w_TIPOPERAZ=trim(this.Parent.oContained.w_TIPOPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPERAZ=='I',1,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='A',2,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='C',3,;
      0)))
  endfunc

  func oTIPOPERAZ_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLAGCOR = '1') OR (.w_FLAGINT = '1'))
    endwith
   endif
  endfunc

  add object oDATANASC_1_22 as StdField with uid="WGYOTWHLZZ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DATANASC", cQueryName = "DATANASC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 62689145,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=6, Top=167

  func oDATANASC_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCOMUNE_1_23 as StdField with uid="QDXVFBYYJG",rtseq=23,rtrep=.f.,;
    cFormVar = "w_COMUNE", cQueryName = "COMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 137351898,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=111, Top=167, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCOMUNE_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSIGLA_1_24 as StdField with uid="AQSAXFLTQI",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SIGLA", cQueryName = "SIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 235485146,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=436, Top=167, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSIGLA_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc


  add object oSESSO_1_25 as StdCombo with uid="NCCDEVHYQI",rtseq=25,rtrep=.f.,left=509,top=168,width=92,height=21;
    , height = 21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 220298202;
    , cFormVar="w_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSESSO_1_25.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSESSO_1_25.GetRadio()
    this.Parent.oContained.w_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oSESSO_1_25.SetRadio()
    this.Parent.oContained.w_SESSO=trim(this.Parent.oContained.w_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_SESSO=='M',1,;
      iif(this.Parent.oContained.w_SESSO=='F',2,;
      0))
  endfunc

  func oSESSO_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oVARRES_1_26 as StdField with uid="LCJAHFQKSB",rtseq=26,rtrep=.f.,;
    cFormVar = "w_VARRES", cQueryName = "VARRES",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della variazione",;
    HelpContextID = 87912534,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=6, Top=204

  func oVARRES_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oRECOMUNE_1_27 as StdField with uid="OZIDXVLWTW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_RECOMUNE", cQueryName = "RECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 138836901,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=111, Top=204, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRECOMUNE_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oRESIGLA_1_28 as StdField with uid="SUWJKWJLTK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_RESIGLA", cQueryName = "RESIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 240419862,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=436, Top=204, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRESIGLA_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oTELEFONO1_1_29 as StdField with uid="QVUZNFWEXA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_TELEFONO1", cQueryName = "TELEFONO1",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono, inserire solo numeri",;
    HelpContextID = 247457899,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=596, Top=204, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oTELEFONO1_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  func oTELEFONO1_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_TELEFONO1))=0 and at('/',alltrim(.w_TELEFONO1))=0 and at('-',alltrim(.w_TELEFONO1))=0)
    endwith
    return bRes
  endfunc

  add object oINDIRIZ_1_30 as StdField with uid="ZFPHCRDCWK",rtseq=30,rtrep=.f.,;
    cFormVar = "w_INDIRIZ", cQueryName = "INDIRIZ",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 66872186,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=111, Top=239, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oINDIRIZ_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCAP_1_31 as StdField with uid="MDJULVDKRQ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 40153306,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=436, Top=239, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oCAP_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCODATT_1_32 as StdField with uid="UFNDOTUYER",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 119250214,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=505, Top=239, cSayPict='repl("!",5)', cGetPict='repl("!",5)', InputMask=replicate('X',5)

  func oCODATT_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oTELEFAX1_1_33 as StdField with uid="CXWSDCTASP",rtseq=33,rtrep=.f.,;
    cFormVar = "w_TELEFAX1", cQueryName = "TELEFAX1",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero del FAX, inserire solo numeri",;
    HelpContextID = 54531175,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=596, Top=239, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',18)

  func oTELEFAX1_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  func oTELEFAX1_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_TELEFAX1))=0 and at('/',alltrim(.w_TELEFAX1))=0 and at('-',alltrim(.w_TELEFAX1))=0)
    endwith
    return bRes
  endfunc

  add object oSEVARSED_1_34 as StdField with uid="MKYAOPUZVB",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SEVARSED", cQueryName = "SEVARSED",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data variazione sede legale",;
    HelpContextID = 100447338,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=6, Top=288

  func oSEVARSED_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECOMUNE_1_35 as StdField with uid="NFZSOTAWKE",rtseq=35,rtrep=.f.,;
    cFormVar = "w_SECOMUNE", cQueryName = "SECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 138836885,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=111, Top=288, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSECOMUNE_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSESIGLA_1_36 as StdField with uid="OOQMIUMTFG",rtseq=36,rtrep=.f.,;
    cFormVar = "w_SESIGLA", cQueryName = "SESIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 240419878,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=436, Top=288, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSESIGLA_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oTELEFONO2_1_37 as StdField with uid="FRFWOUQRXF",rtseq=37,rtrep=.f.,;
    cFormVar = "w_TELEFONO2", cQueryName = "TELEFONO2",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono, inserire solo numeri",;
    HelpContextID = 247457883,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=596, Top=288, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oTELEFONO2_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  func oTELEFONO2_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_TELEFONO2))=0 and at('/',alltrim(.w_TELEFONO2))=0 and at('-',alltrim(.w_TELEFONO2))=0)
    endwith
    return bRes
  endfunc

  add object oSEINDIRI2_1_38 as StdField with uid="OXQZLOKZQT",rtseq=38,rtrep=.f.,;
    cFormVar = "w_SEINDIRI2", cQueryName = "SEINDIRI2",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 187230095,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=111, Top=322, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSEINDIRI2_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECAP_1_39 as StdField with uid="AVSZVWBCSZ",rtseq=39,rtrep=.f.,;
    cFormVar = "w_SECAP", cQueryName = "SECAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 220494810,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=436, Top=322, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oSECAP_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oTELEFAX2_1_40 as StdField with uid="SPLXQQDCPJ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_TELEFAX2", cQueryName = "TELEFAX2",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero del FAX, inserire solo numeri",;
    HelpContextID = 54531176,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=596, Top=322, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',18)

  func oTELEFAX2_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  func oTELEFAX2_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_TELEFAX2))=0 and at('/',alltrim(.w_TELEFAX2))=0 and at('-',alltrim(.w_TELEFAX2))=0)
    endwith
    return bRes
  endfunc

  add object oSEVARDOM_1_41 as StdField with uid="CYDZEIXVFX",rtseq=41,rtrep=.f.,;
    cFormVar = "w_SEVARDOM", cQueryName = "SEVARDOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data variazione domicilio fiscale",;
    HelpContextID = 151210893,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=6, Top=356

  func oSEVARDOM_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERCOMUN_1_42 as StdField with uid="KMAFMNRNPP",rtseq=42,rtrep=.f.,;
    cFormVar = "w_SERCOMUN", cQueryName = "SERCOMUN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 265188468,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=111, Top=356, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSERCOMUN_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERSIGLA_1_43 as StdField with uid="TGOHNSUWWG",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SERSIGLA", cQueryName = "SERSIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore",;
    HelpContextID = 109153177,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=436, Top=356, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSERSIGLA_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERCAP_1_44 as StdField with uid="KHVCIKTTRJ",rtseq=44,rtrep=.f.,;
    cFormVar = "w_SERCAP", cQueryName = "SERCAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 32404518,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=509, Top=356, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oSERCAP_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oNATGIU_1_45 as StdField with uid="GGFCANZHTS",rtseq=45,rtrep=.f.,;
    cFormVar = "w_NATGIU", cQueryName = "NATGIU",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso per natura giuridica",;
    ToolTipText = "Tipologia natura giuridica",;
    HelpContextID = 124948438,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=26, Left=596, Top=356, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oNATGIU_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oNATGIU_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=43) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=51))
    endwith
    return bRes
  endfunc

  add object oSERINDIR_1_46 as StdField with uid="ZEWEROQCEA",rtseq=46,rtrep=.f.,;
    cFormVar = "w_SERINDIR", cQueryName = "SERINDIR",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 154897288,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=111, Top=390, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSERINDIR_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECODATT_1_47 as StdField with uid="RQBOLJQSDR",rtseq=47,rtrep=.f.,;
    cFormVar = "w_SECODATT", cQueryName = "SECODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 53052538,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=436, Top=390, cSayPict='repl("!",5)', cGetPict='repl("!",5)', InputMask=replicate('X',5)

  func oSECODATT_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc


  add object oSTATO_1_48 as StdCombo with uid="HVJLCFQSUG",rtseq=48,rtrep=.f.,left=111,top=425,width=132,height=21;
    , height = 21;
    , ToolTipText = "Stato della societ� o ente.";
    , HelpContextID = 220302554;
    , cFormVar="w_STATO",RowSource=""+"1 - Soggetto in normale attivit�,"+"2 - Soggetto in liquidazione per cessazione attivit�,"+"3 - Soggetto in fallimento o in liquidazione coatta amministrativa,"+"4 - Soggetto estinto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_48.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    space(1))))))
  endfunc
  func oSTATO_1_48.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_48.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='1',1,;
      iif(this.Parent.oContained.w_STATO=='2',2,;
      iif(this.Parent.oContained.w_STATO=='3',3,;
      iif(this.Parent.oContained.w_STATO=='4',4,;
      0))))
  endfunc

  func oSTATO_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc


  add object oSITUAZ_1_49 as StdCombo with uid="QWWEIZUDRN",value=1,rtseq=49,rtrep=.f.,left=248,top=425,width=182,height=21;
    , height = 21;
    , ToolTipText = "Situazione della societ� o ente";
    , HelpContextID = 201365542;
    , cFormVar="w_SITUAZ",RowSource=""+"Non selezionata,"+"1 - Periodo imposta inizio liquidazione,"+"2 - Periodo imposta successivo a periodo liquidazione o fallimento,"+"3 - Periodo imposta termine liquidazione,"+"5 - Periodo imposta avvenuta trasformazione,"+"6 - Periodo normale di imposta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSITUAZ_1_49.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    space(1))))))))
  endfunc
  func oSITUAZ_1_49.GetRadio()
    this.Parent.oContained.w_SITUAZ = this.RadioValue()
    return .t.
  endfunc

  func oSITUAZ_1_49.SetRadio()
    this.Parent.oContained.w_SITUAZ=trim(this.Parent.oContained.w_SITUAZ)
    this.value = ;
      iif(this.Parent.oContained.w_SITUAZ=='',1,;
      iif(this.Parent.oContained.w_SITUAZ=='1',2,;
      iif(this.Parent.oContained.w_SITUAZ=='2',3,;
      iif(this.Parent.oContained.w_SITUAZ=='3',4,;
      iif(this.Parent.oContained.w_SITUAZ=='5',5,;
      iif(this.Parent.oContained.w_SITUAZ=='6',6,;
      0))))))
  endfunc

  func oSITUAZ_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCODFISDA_1_50 as StdField with uid="XSNKRKYERI",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CODFISDA", cQueryName = "CODFISDA",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del dicastero di appartenenza",;
    HelpContextID = 91266407,;
   bGlobalFont=.t.,;
    Height=21, Width=112, Left=436, Top=425, cSayPict='repl("!",11)', cGetPict='repl("!",11)', InputMask=replicate('X',11)

  func oCODFISDA_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oCODFISDA_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CODFISDA),chkcfp(alltrim(.w_CODFISDA),'PI'),.T.))
    endwith
    return bRes
  endfunc


  add object oObj_1_102 as cp_runprogram with uid="XZKGQTLPEA",left=496, top=454, width=216,height=33,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BFT",;
    cEvent = "Lancia",;
    nPag=1;
    , HelpContextID = 137498854

  add object oStr_1_60 as StdString with uid="PZCKHMGKWT",Visible=.t., Left=0, Top=11,;
    Alignment=1, Width=98, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="MIJLOSOMKI",Visible=.t., Left=314, Top=10,;
    Alignment=1, Width=67, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="TZKQRFHOUY",Visible=.t., Left=9, Top=71,;
    Alignment=0, Width=204, Height=15,;
    Caption="Tipo di dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_64 as StdString with uid="UCIEAMMITE",Visible=.t., Left=0, Top=33,;
    Alignment=1, Width=98, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="HLIVBEVOMR",Visible=.t., Left=540, Top=18,;
    Alignment=0, Width=135, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="EFRWRPPCTM",Visible=.t., Left=11, Top=137,;
    Alignment=0, Width=204, Height=15,;
    Caption="Persona fisica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_71 as StdString with uid="HRDBHHFBYE",Visible=.t., Left=111, Top=191,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di residenza o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_72 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=6, Top=154,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_73 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=111, Top=154,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_74 as StdString with uid="VETKYUGDVO",Visible=.t., Left=436, Top=154,;
    Alignment=0, Width=63, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="FWEPYXGQZJ",Visible=.t., Left=6, Top=191,;
    Alignment=0, Width=87, Height=17,;
    Caption="Data variazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_76 as StdString with uid="HVQAZPYBNO",Visible=.t., Left=436, Top=191,;
    Alignment=0, Width=60, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_77 as StdString with uid="ATTHQLRPRN",Visible=.t., Left=111, Top=226,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_78 as StdString with uid="KSQHILFMHS",Visible=.t., Left=509, Top=154,;
    Alignment=0, Width=48, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_79 as StdString with uid="IVLKBJFSJW",Visible=.t., Left=436, Top=226,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_80 as StdString with uid="YZOMBVBHCP",Visible=.t., Left=505, Top=226,;
    Alignment=0, Width=77, Height=17,;
    Caption="Cod.attivit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_81 as StdString with uid="AIZMGLVLMZ",Visible=.t., Left=596, Top=191,;
    Alignment=0, Width=68, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_85 as StdString with uid="DEKDZLFCML",Visible=.t., Left=11, Top=259,;
    Alignment=0, Width=228, Height=15,;
    Caption="Altri soggetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_86 as StdString with uid="EBKJKEHJKK",Visible=.t., Left=6, Top=275,;
    Alignment=0, Width=76, Height=13,;
    Caption="Sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_87 as StdString with uid="RZVEDQJFMM",Visible=.t., Left=111, Top=275,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_88 as StdString with uid="UNXIIROKSZ",Visible=.t., Left=596, Top=343,;
    Alignment=0, Width=92, Height=13,;
    Caption="Natura giuridica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_89 as StdString with uid="UQKLITXGSW",Visible=.t., Left=436, Top=275,;
    Alignment=0, Width=68, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_90 as StdString with uid="EXNBDAOCWO",Visible=.t., Left=111, Top=309,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_91 as StdString with uid="WLXGABIKNW",Visible=.t., Left=436, Top=309,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_92 as StdString with uid="RMDNMKOJQJ",Visible=.t., Left=6, Top=343,;
    Alignment=0, Width=82, Height=13,;
    Caption="Dom. fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_93 as StdString with uid="ZVHEBJFODD",Visible=.t., Left=111, Top=343,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_94 as StdString with uid="VJNNNJGRSB",Visible=.t., Left=436, Top=343,;
    Alignment=0, Width=60, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_95 as StdString with uid="XMGFDNXRGY",Visible=.t., Left=111, Top=377,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_96 as StdString with uid="CNBFEAFDFD",Visible=.t., Left=509, Top=343,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_97 as StdString with uid="DYEPGPKKPJ",Visible=.t., Left=436, Top=377,;
    Alignment=0, Width=84, Height=13,;
    Caption="Codice attivit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_98 as StdString with uid="TVXFVOYTZB",Visible=.t., Left=111, Top=412,;
    Alignment=0, Width=48, Height=13,;
    Caption="Stato"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_99 as StdString with uid="GPUYEQERVJ",Visible=.t., Left=248, Top=412,;
    Alignment=0, Width=64, Height=13,;
    Caption="Situazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_100 as StdString with uid="ARBZPVKLZS",Visible=.t., Left=436, Top=411,;
    Alignment=0, Width=244, Height=13,;
    Caption="Codice fiscale del dicastero di appartenenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_101 as StdString with uid="BBSCZKFXTA",Visible=.t., Left=542, Top=97,;
    Alignment=0, Width=106, Height=13,;
    Caption="Tipo operazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_103 as StdString with uid="AEVZXFOOTE",Visible=.t., Left=596, Top=226,;
    Alignment=0, Width=48, Height=13,;
    Caption="FAX"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_104 as StdString with uid="CRPRRVNJLN",Visible=.t., Left=596, Top=275,;
    Alignment=0, Width=68, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_105 as StdString with uid="OJWZHMAIVJ",Visible=.t., Left=596, Top=309,;
    Alignment=0, Width=48, Height=13,;
    Caption="FAX"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_62 as StdBox with uid="YPBURJVQFL",left=7, top=89, width=681,height=2

  add object oBox_1_69 as StdBox with uid="TFBVYLMEDX",left=7, top=151, width=681,height=2

  add object oBox_1_84 as StdBox with uid="HJLYLCJQUK",left=7, top=274, width=681,height=2
enddefine
define class tgsri_sftPag2 as StdContainer
  Width  = 695
  height = 447
  stdWidth  = 695
  stdheight = 447
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oForza2_2_1 as StdCheck with uid="ANPGYPQFYE",rtseq=65,rtrep=.f.,left=530, top=8, caption="Forza editing",;
    HelpContextID = 165175978,;
    cFormVar="w_Forza2", bObbl = .f. , nPag = 2;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oForza2_2_1.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oForza2_2_1.GetRadio()
    this.Parent.oContained.w_Forza2 = this.RadioValue()
    return .t.
  endfunc

  func oForza2_2_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Forza2==.t.,1,;
      0)
  endfunc

  add object oRFCODFIS_2_2 as StdField with uid="MTXMRCWFQH",rtseq=66,rtrep=.f.,;
    cFormVar = "w_RFCODFIS", cQueryName = "RFCODFIS",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 131496599,;
   bGlobalFont=.t.,;
    Height=21, Width=165, Left=48, Top=47, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oRFCODFIS_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  func oRFCODFIS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oRFCODCAR_2_3 as StdField with uid="EBKXVBBOAC",rtseq=67,rtrep=.f.,;
    cFormVar = "w_RFCODCAR", cQueryName = "RFCODCAR",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non ammesso, vedi legenda fondo pagina",;
    ToolTipText = "Codice carica, vedi legenda (*)",;
    HelpContextID = 86607208,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=237, Top=47, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2), height = 21

  func oRFCODCAR_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  func oRFCODCAR_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_RFCODCAR)>=1 AND VAL(.w_RFCODCAR)<=12 or empty(.w_RFCODCAR))
    endwith
    return bRes
  endfunc

  add object oRFCOGNOME_2_4 as StdField with uid="TUTTYIGKSZ",rtseq=68,rtrep=.f.,;
    cFormVar = "w_RFCOGNOME", cQueryName = "RFCOGNOME",;
    bObbl = .f. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del rappresentante",;
    HelpContextID = 262567501,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=48, Top=82, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oRFCOGNOME_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFNOME_2_5 as StdField with uid="RKLEAKPRVC",rtseq=69,rtrep=.f.,;
    cFormVar = "w_RFNOME", cQueryName = "RFNOME",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del rappresentante",;
    HelpContextID = 138791658,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=237, Top=82, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  func oRFNOME_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc


  add object oRFSESSO_2_6 as StdCombo with uid="WGXEPZGXVZ",value=1,rtseq=70,rtrep=.f.,left=418,top=82,width=114,height=21;
    , height = 21;
    , ToolTipText = "Sesso del rappresentante";
    , HelpContextID = 166689514;
    , cFormVar="w_RFSESSO",RowSource=""+"Non selezionato,"+"Maschio,"+"Femmina", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRFSESSO_2_6.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oRFSESSO_2_6.GetRadio()
    this.Parent.oContained.w_RFSESSO = this.RadioValue()
    return .t.
  endfunc

  func oRFSESSO_2_6.SetRadio()
    this.Parent.oContained.w_RFSESSO=trim(this.Parent.oContained.w_RFSESSO)
    this.value = ;
      iif(this.Parent.oContained.w_RFSESSO=='',1,;
      iif(this.Parent.oContained.w_RFSESSO=='M',2,;
      iif(this.Parent.oContained.w_RFSESSO=='F',3,;
      0)))
  endfunc

  func oRFSESSO_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFDATANASC_2_7 as StdField with uid="AUOENXXQZR",rtseq=71,rtrep=.f.,;
    cFormVar = "w_RFDATANASC", cQueryName = "RFDATANASC",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del rappresentante",;
    HelpContextID = 199500409,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=48, Top=119

  func oRFDATANASC_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCOMNAS_2_8 as StdField with uid="RPGGCLVJHX",rtseq=72,rtrep=.f.,;
    cFormVar = "w_RFCOMNAS", cQueryName = "RFCOMNAS",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del rappresentante",;
    HelpContextID = 12158313,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=157, Top=119, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRFCOMNAS_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFSIGNAS_2_9 as StdField with uid="DAVHPWZQVP",rtseq=73,rtrep=.f.,;
    cFormVar = "w_RFSIGNAS", cQueryName = "RFSIGNAS",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del rappresentante",;
    HelpContextID = 5539177,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=418, Top=119, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRFSIGNAS_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCOMUNE_2_10 as StdField with uid="DDNELJFMGC",rtseq=74,rtrep=.f.,;
    cFormVar = "w_RFCOMUNE", cQueryName = "RFCOMUNE",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del rappresentante",;
    HelpContextID = 138836645,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=48, Top=155, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRFCOMUNE_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFSIGLA_2_11 as StdField with uid="MPTADLFURD",rtseq=75,rtrep=.f.,;
    cFormVar = "w_RFSIGLA", cQueryName = "RFSIGLA",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 240420118,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=359, Top=155, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRFSIGLA_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCAP_2_12 as StdField with uid="VMMUJSTIQO",rtseq=76,rtrep=.f.,;
    cFormVar = "w_RFCAP", cQueryName = "RFCAP",;
    bObbl = .f. , nPag = 2, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del rappresentante",;
    HelpContextID = 220494570,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=418, Top=155, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oRFCAP_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFINDIRIZ_2_13 as StdField with uid="HCOWWPHROD",rtseq=77,rtrep=.f.,;
    cFormVar = "w_RFINDIRIZ", cQueryName = "RFINDIRIZ",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 187230975,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=48, Top=191, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oRFINDIRIZ_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFTELEFONO_2_14 as StdField with uid="MMGWOVEOZI",rtseq=78,rtrep=.f.,;
    cFormVar = "w_RFTELEFONO", cQueryName = "RFTELEFONO",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    HelpContextID = 127985989,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=359, Top=191, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oRFTELEFONO_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  func oRFTELEFONO_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oqST1_2_15 as StdCheck with uid="BGCLFWVZAL",rtseq=79,rtrep=.f.,left=248, top=236, caption="ST",;
    HelpContextID = 36920314,;
    cFormVar="w_qST1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqST1_2_15.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqST1_2_15.GetRadio()
    this.Parent.oContained.w_qST1 = this.RadioValue()
    return .t.
  endfunc

  func oqST1_2_15.SetRadio()
    this.Parent.oContained.w_qST1=trim(this.Parent.oContained.w_qST1)
    this.value = ;
      iif(this.Parent.oContained.w_qST1=='1',1,;
      0)
  endfunc

  add object oqSX1_2_16 as StdCheck with uid="ZXSRWVYXVB",rtseq=80,rtrep=.f.,left=379, top=236, caption="SX",;
    HelpContextID = 36903930,;
    cFormVar="w_qSX1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSX1_2_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSX1_2_16.GetRadio()
    this.Parent.oContained.w_qSX1 = this.RadioValue()
    return .t.
  endfunc

  func oqSX1_2_16.SetRadio()
    this.Parent.oContained.w_qSX1=trim(this.Parent.oContained.w_qSX1)
    this.value = ;
      iif(this.Parent.oContained.w_qSX1=='1',1,;
      0)
  endfunc

  add object oFIRMDICH_2_17 as StdCheck with uid="ZFJVFMHAUQ",rtseq=81,rtrep=.f.,left=248, top=258, caption="Dichiarante",;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 187201438,;
    cFormVar="w_FIRMDICH", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMDICH_2_17.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMDICH_2_17.GetRadio()
    this.Parent.oContained.w_FIRMDICH = this.RadioValue()
    return .t.
  endfunc

  func oFIRMDICH_2_17.SetRadio()
    this.Parent.oContained.w_FIRMDICH=trim(this.Parent.oContained.w_FIRMDICH)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMDICH=='1',1,;
      0)
  endfunc

  add object oFIRMPRES_2_18 as StdCheck with uid="XOLWNXUWLK",rtseq=82,rtrep=.f.,left=379, top=258, caption="Presidente o componenti dell'organo di controllo",;
    ToolTipText = "Firma del presidente o dei componenti dell'organo di controllo",;
    HelpContextID = 82343849,;
    cFormVar="w_FIRMPRES", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMPRES_2_18.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMPRES_2_18.GetRadio()
    this.Parent.oContained.w_FIRMPRES = this.RadioValue()
    return .t.
  endfunc

  func oFIRMPRES_2_18.SetRadio()
    this.Parent.oContained.w_FIRMPRES=trim(this.Parent.oContained.w_FIRMPRES)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMPRES=='1',1,;
      0)
  endfunc

  add object oNUMCAF_2_19 as StdField with uid="KVCYFTJAQL",rtseq=83,rtrep=.f.,;
    cFormVar = "w_NUMCAF", cQueryName = "NUMCAF",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero iscrizione all'albo del C.A.F.",;
    HelpContextID = 135384106,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=12, Top=315, cSayPict='"99999"', cGetPict='"99999"'

  add object oRFDATIMP_2_20 as StdField with uid="UVYTNIYLIS",rtseq=84,rtrep=.f.,;
    cFormVar = "w_RFDATIMP", cQueryName = "RFDATIMP",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data impegno trasmissione fornitura",;
    HelpContextID = 65301146,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=12, Top=353

  add object oIMPTRTEL_2_21 as StdCheck with uid="UWMCFDUCPC",rtseq=85,rtrep=.f.,left=200, top=313, caption="Impegno a trasmettere in via telematica la dich. predisposta dal contribuente",;
    ToolTipText = "Impegno trasmissione telematica dichiarazione predisposta dal contribuente",;
    HelpContextID = 118447058,;
    cFormVar="w_IMPTRTEL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIMPTRTEL_2_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oIMPTRTEL_2_21.GetRadio()
    this.Parent.oContained.w_IMPTRTEL = this.RadioValue()
    return .t.
  endfunc

  func oIMPTRTEL_2_21.SetRadio()
    this.Parent.oContained.w_IMPTRTEL=trim(this.Parent.oContained.w_IMPTRTEL)
    this.value = ;
      iif(this.Parent.oContained.w_IMPTRTEL=='1',1,;
      0)
  endfunc

  add object oIMPTRSOG_2_22 as StdCheck with uid="XXHZPERTNM",rtseq=86,rtrep=.f.,left=200, top=334, caption="Impegno a trasmettere in via telematica la dich. del contribuente",;
    ToolTipText = "Impegno a trasmettere in via telematica la dichiarazione del contribuente",;
    HelpContextID = 166765619,;
    cFormVar="w_IMPTRSOG", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIMPTRSOG_2_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oIMPTRSOG_2_22.GetRadio()
    this.Parent.oContained.w_IMPTRSOG = this.RadioValue()
    return .t.
  endfunc

  func oIMPTRSOG_2_22.SetRadio()
    this.Parent.oContained.w_IMPTRSOG=trim(this.Parent.oContained.w_IMPTRSOG)
    this.value = ;
      iif(this.Parent.oContained.w_IMPTRSOG=='1',1,;
      0)
  endfunc

  add object oFIRMINT_2_23 as StdCheck with uid="PJOQBGGUFU",rtseq=87,rtrep=.f.,left=200, top=354, caption="Firma intermediario",;
    ToolTipText = "Firma intermediario",;
    HelpContextID = 7894870,;
    cFormVar="w_FIRMINT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMINT_2_23.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMINT_2_23.GetRadio()
    this.Parent.oContained.w_FIRMINT = this.RadioValue()
    return .t.
  endfunc

  func oFIRMINT_2_23.SetRadio()
    this.Parent.oContained.w_FIRMINT=trim(this.Parent.oContained.w_FIRMINT)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMINT=='1',1,;
      0)
  endfunc

  add object oStr_2_24 as StdString with uid="XHNHYLLXLG",Visible=.t., Left=13, Top=243,;
    Alignment=1, Width=228, Height=18,;
    Caption="Sezione I - quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="FTEQCYTUWC",Visible=.t., Left=11, Top=217,;
    Alignment=0, Width=317, Height=15,;
    Caption="Firma della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_28 as StdString with uid="FHMRICXWEG",Visible=.t., Left=11, Top=11,;
    Alignment=0, Width=485, Height=15,;
    Caption="Dati relativi al rappresentante firmatario della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_29 as StdString with uid="RULZHJZXRW",Visible=.t., Left=48, Top=34,;
    Alignment=0, Width=160, Height=13,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_30 as StdString with uid="USKIUPDQMV",Visible=.t., Left=237, Top=30,;
    Alignment=0, Width=131, Height=17,;
    Caption="Codice carica (*)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_31 as StdString with uid="GPZTYMBFPO",Visible=.t., Left=157, Top=106,;
    Alignment=0, Width=155, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_32 as StdString with uid="RCZFFIGQZJ",Visible=.t., Left=48, Top=69,;
    Alignment=0, Width=104, Height=13,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_33 as StdString with uid="BXGOIXINBX",Visible=.t., Left=237, Top=69,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_34 as StdString with uid="XMDUKEDFTN",Visible=.t., Left=418, Top=68,;
    Alignment=0, Width=76, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_35 as StdString with uid="VNNLUPWWEO",Visible=.t., Left=48, Top=106,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_36 as StdString with uid="LCBMVTEUCO",Visible=.t., Left=418, Top=106,;
    Alignment=0, Width=78, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_37 as StdString with uid="ECENBGSUXY",Visible=.t., Left=48, Top=142,;
    Alignment=0, Width=176, Height=13,;
    Caption="Comune di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_38 as StdString with uid="WXIGENELJC",Visible=.t., Left=359, Top=142,;
    Alignment=0, Width=57, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_39 as StdString with uid="FSUMAEAACK",Visible=.t., Left=418, Top=142,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_40 as StdString with uid="XDOCDGJPIJ",Visible=.t., Left=48, Top=178,;
    Alignment=0, Width=128, Height=13,;
    Caption="Indirizzo"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_41 as StdString with uid="XJBBNGRGLQ",Visible=.t., Left=359, Top=178,;
    Alignment=0, Width=97, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_42 as StdString with uid="LQRAAIJAJN",Visible=.t., Left=12, Top=336,;
    Alignment=0, Width=183, Height=13,;
    Caption="Data dell'impegno a trasmettere"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_43 as StdString with uid="ELNVKYREGV",Visible=.t., Left=11, Top=281,;
    Alignment=0, Width=397, Height=15,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_45 as StdString with uid="EMANNTQNBZ",Visible=.t., Left=12, Top=299,;
    Alignment=0, Width=244, Height=13,;
    Caption="Numero di iscrizione all'albo del C.A.F."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_46 as StdString with uid="WFXGNFKAUI",Visible=.t., Left=12, Top=380,;
    Alignment=0, Width=90, Height=13,;
    Caption="(*) Codici carica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_47 as StdString with uid="DXNKDVQUJI",Visible=.t., Left=12, Top=395,;
    Alignment=0, Width=180, Height=13,;
    Caption="1-Rappres. legale o socio amminis."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_48 as StdString with uid="IHZJPZTZXJ",Visible=.t., Left=204, Top=395,;
    Alignment=0, Width=138, Height=13,;
    Caption="4-Commissario liquidatore"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_49 as StdString with uid="NXGTXENCIQ",Visible=.t., Left=204, Top=425,;
    Alignment=0, Width=127, Height=13,;
    Caption="6-Rappresentante fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_50 as StdString with uid="NUVYIXSHXR",Visible=.t., Left=357, Top=426,;
    Alignment=0, Width=125, Height=13,;
    Caption="9-Amm. del condominio"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_51 as StdString with uid="AYHEZQLUXC",Visible=.t., Left=498, Top=410,;
    Alignment=0, Width=164, Height=17,;
    Caption="11-Per conto di una pubbl. amm."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_52 as StdString with uid="GVQHRFTHTK",Visible=.t., Left=498, Top=426,;
    Alignment=0, Width=182, Height=13,;
    Caption="12-Commiss. liquid. di pubbl. amm."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_53 as StdString with uid="CVLHYQGHEZ",Visible=.t., Left=12, Top=410,;
    Alignment=0, Width=110, Height=13,;
    Caption="2-Curatore eredit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_54 as StdString with uid="DOYSIXIXAE",Visible=.t., Left=12, Top=425,;
    Alignment=0, Width=121, Height=13,;
    Caption="3-Curatore fallimentare"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_55 as StdString with uid="WHTUFVRVSF",Visible=.t., Left=207, Top=410,;
    Alignment=0, Width=129, Height=13,;
    Caption="5-Commissario giudiziale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_56 as StdString with uid="TQJPGIZBAA",Visible=.t., Left=360, Top=395,;
    Alignment=0, Width=121, Height=13,;
    Caption="7-Erede del dichiarante"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_57 as StdString with uid="RZJBCNFMTM",Visible=.t., Left=357, Top=410,;
    Alignment=0, Width=123, Height=13,;
    Caption="8-Liquidatore volontario"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_58 as StdString with uid="MGAEIFHDYE",Visible=.t., Left=498, Top=395,;
    Alignment=0, Width=106, Height=13,;
    Caption="10-Condominio"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_59 as StdString with uid="MZPJYLMUUD",Visible=.t., Left=13, Top=260,;
    Alignment=1, Width=228, Height=18,;
    Caption="Sezione I - firma della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oBox_2_25 as StdBox with uid="FZZKXTTQTF",left=11, top=232, width=672,height=2

  add object oBox_2_27 as StdBox with uid="LEBGSAUSDO",left=7, top=26, width=677,height=2

  add object oBox_2_44 as StdBox with uid="DTCNDAKETU",left=11, top=296, width=672,height=2
enddefine
define class tgsri_sftPag3 as StdContainer
  Width  = 695
  height = 447
  stdWidth  = 695
  stdheight = 447
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPFORN_3_1 as StdCombo with uid="TMIIFPZQPN",rtseq=88,rtrep=.f.,left=100,top=11,width=244,height=21;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 187606986;
    , cFormVar="w_TIPFORN",RowSource=""+"01: Soggetti che utilizzano il canale Internet,"+"02: Soggetti che utilizzano il canale Entratel,"+"03: C.A.F. dipendenti e pensionati,"+"05: C.A.F: imprese,"+"09: Art 3 comma 2,"+"10: Altri intermediari", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTIPFORN_3_1.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'02',;
    iif(this.value =3,'03',;
    iif(this.value =4,'05',;
    iif(this.value =5,'09',;
    iif(this.value =6,'10',;
    space(2))))))))
  endfunc
  func oTIPFORN_3_1.GetRadio()
    this.Parent.oContained.w_TIPFORN = this.RadioValue()
    return .t.
  endfunc

  func oTIPFORN_3_1.SetRadio()
    this.Parent.oContained.w_TIPFORN=trim(this.Parent.oContained.w_TIPFORN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFORN=='01',1,;
      iif(this.Parent.oContained.w_TIPFORN=='02',2,;
      iif(this.Parent.oContained.w_TIPFORN=='03',3,;
      iif(this.Parent.oContained.w_TIPFORN=='05',4,;
      iif(this.Parent.oContained.w_TIPFORN=='09',5,;
      iif(this.Parent.oContained.w_TIPFORN=='10',6,;
      0))))))
  endfunc

  add object oCODFISIN_3_2 as StdField with uid="OIWLMRKWII",rtseq=89,rtrep=.f.,;
    cFormVar = "w_CODFISIN", cQueryName = "CODFISIN",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'intermediario che effettua la trasmissione",;
    HelpContextID = 177169036,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=507, Top=11, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCODFISIN_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oFODATANASC_3_3 as StdField with uid="DDIBJMNRLH",rtseq=90,rtrep=.f.,;
    cFormVar = "w_FODATANASC", cQueryName = "FODATANASC",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 199498297,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=33, Top=77

  func oFODATANASC_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' and .w_TIPFORN $ '01-02')
    endwith
   endif
  endfunc


  add object oFOSESSO_3_4 as StdCombo with uid="EZVQYTQPNM",value=1,rtseq=91,rtrep=.f.,left=145,top=77,width=96,height=21;
    , height = 21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 166687402;
    , cFormVar="w_FOSESSO",RowSource=""+"Non selezionato,"+"Maschio,"+"Femmina", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oFOSESSO_3_4.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oFOSESSO_3_4.GetRadio()
    this.Parent.oContained.w_FOSESSO = this.RadioValue()
    return .t.
  endfunc

  func oFOSESSO_3_4.SetRadio()
    this.Parent.oContained.w_FOSESSO=trim(this.Parent.oContained.w_FOSESSO)
    this.value = ;
      iif(this.Parent.oContained.w_FOSESSO=='',1,;
      iif(this.Parent.oContained.w_FOSESSO=='M',2,;
      iif(this.Parent.oContained.w_FOSESSO=='F',3,;
      0)))
  endfunc

  func oFOSESSO_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' and .w_TIPFORN $ '01-02')
    endwith
   endif
  endfunc

  add object oFOCOMUNE_3_5 as StdField with uid="QAAPEJDMEJ",rtseq=92,rtrep=.f.,;
    cFormVar = "w_FOCOMUNE", cQueryName = "FOCOMUNE",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 138834533,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=275, Top=77, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOCOMUNE_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' and .w_TIPFORN $ '01-02')
    endwith
   endif
  endfunc

  add object oFOSIGLA_3_6 as StdField with uid="PWEJHDBSSF",rtseq=93,rtrep=.f.,;
    cFormVar = "w_FOSIGLA", cQueryName = "FOSIGLA",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 240422230,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=603, Top=77, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSIGLA_3_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' and .w_TIPFORN $ '01-02')
    endwith
   endif
  endfunc

  add object oFORECOMUNE_3_7 as StdField with uid="RGOQWOEVVH",rtseq=94,rtrep=.f.,;
    cFormVar = "w_FORECOMUNE", cQueryName = "FORECOMUNE",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 250558581,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=33, Top=110, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFORECOMUNE_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' and .w_TIPFORN $ '01-02')
    endwith
   endif
  endfunc

  add object oFORESIGLA_3_8 as StdField with uid="MJCFSUINVZ",rtseq=95,rtrep=.f.,;
    cFormVar = "w_FORESIGLA", cQueryName = "FORESIGLA",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 202408370,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=468, Top=110, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFORESIGLA_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' and .w_TIPFORN $ '01-02')
    endwith
   endif
  endfunc

  add object oFOCAP_3_9 as StdField with uid="TTYPCQBXFI",rtseq=96,rtrep=.f.,;
    cFormVar = "w_FOCAP", cQueryName = "FOCAP",;
    bObbl = .f. , nPag = 3, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 220492458,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=539, Top=110, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oFOCAP_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' and .w_TIPFORN $ '01-02')
    endwith
   endif
  endfunc

  add object oFOINDIRIZ_3_10 as StdField with uid="ZFPYJFYYCZ",rtseq=97,rtrep=.f.,;
    cFormVar = "w_FOINDIRIZ", cQueryName = "FOINDIRIZ",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 187233087,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=33, Top=143, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOINDIRIZ_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' and .w_TIPFORN $ '01-02')
    endwith
   endif
  endfunc

  add object oFOSECOMUNE_3_12 as StdField with uid="FTDECCKCEA",rtseq=98,rtrep=.f.,;
    cFormVar = "w_FOSECOMUNE", cQueryName = "FOSECOMUNE",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 250554485,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=33, Top=205, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOSECOMUNE_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oFOSESIGLA_3_13 as StdField with uid="QBWKROZPRH",rtseq=99,rtrep=.f.,;
    cFormVar = "w_FOSESIGLA", cQueryName = "FOSESIGLA",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 202412466,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=468, Top=205, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSESIGLA_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oFOSECAP_3_14 as StdField with uid="UHFFPYICUM",rtseq=100,rtrep=.f.,;
    cFormVar = "w_FOSECAP", cQueryName = "FOSECAP",;
    bObbl = .f. , nPag = 3, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 217019050,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=539, Top=205, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oFOSECAP_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oFOSEINDIRI2_3_15 as StdField with uid="UCUBGSOZHZ",rtseq=101,rtrep=.f.,;
    cFormVar = "w_FOSEINDIRI2", cQueryName = "FOSEINDIRI2",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 7601087,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=33, Top=238, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOSEINDIRI2_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oFOSERCOMUN_3_16 as StdField with uid="VRBXTIUTMI",rtseq=102,rtrep=.f.,;
    cFormVar = "w_FOSERCOMUN", cQueryName = "FOSERCOMUN",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 167714573,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=33, Top=271, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOSERCOMUN_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oFOSERSIGLA_3_17 as StdField with uid="RALWYRLTJY",rtseq=103,rtrep=.f.,;
    cFormVar = "w_FOSERSIGLA", cQueryName = "FOSERSIGLA",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore",;
    HelpContextID = 167718051,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=468, Top=271, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSERSIGLA_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oFOSERCAP_3_18 as StdField with uid="UZATTOELQF",rtseq=104,rtrep=.f.,;
    cFormVar = "w_FOSERCAP", cQueryName = "FOSERCAP",;
    bObbl = .f. , nPag = 3, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 100699558,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=539, Top=271, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oFOSERCAP_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oFOSERINDIR_3_19 as StdField with uid="OCERQWQXAX",rtseq=105,rtrep=.f.,;
    cFormVar = "w_FOSERINDIR", cQueryName = "FOSERINDIR",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 67050454,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=33, Top=304, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOSERINDIR_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oCFRESCAF_3_20 as StdField with uid="TRSLJXUKSK",rtseq=106,rtrep=.f.,;
    cFormVar = "w_CFRESCAF", cQueryName = "CFRESCAF",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 101741676,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=20, Top=376, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCFRESCAF_3_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFRESCAF),chkcfp(alltrim(.w_CFRESCAF),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oVISTOA35_3_21 as StdCheck with uid="GWFBKDOAOR",rtseq=107,rtrep=.f.,left=20, top=407, caption="Visto ai sensi art. 35 del D.lgs. 9 luglio 1997, n. 241 e suc. mod.",;
    ToolTipText = "Visto art.35 del D.lgs.9 luglio 1997, n.241",;
    HelpContextID = 64981131,;
    cFormVar="w_VISTOA35", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVISTOA35_3_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oVISTOA35_3_21.GetRadio()
    this.Parent.oContained.w_VISTOA35 = this.RadioValue()
    return .t.
  endfunc

  func oVISTOA35_3_21.SetRadio()
    this.Parent.oContained.w_VISTOA35=trim(this.Parent.oContained.w_VISTOA35)
    this.value = ;
      iif(this.Parent.oContained.w_VISTOA35=='1',1,;
      0)
  endfunc

  add object oFLAGFIR_3_22 as StdCheck with uid="GMLRFHNQTY",rtseq=108,rtrep=.f.,left=352, top=375, caption="Flag firma del riquadro visto di conformit�",;
    ToolTipText = "Flag firma riquadro 'visto conformit�'",;
    HelpContextID = 188836438,;
    cFormVar="w_FLAGFIR", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLAGFIR_3_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGFIR_3_22.GetRadio()
    this.Parent.oContained.w_FLAGFIR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGFIR_3_22.SetRadio()
    this.Parent.oContained.w_FLAGFIR=trim(this.Parent.oContained.w_FLAGFIR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGFIR=='1',1,;
      0)
  endfunc

  add object oStr_3_24 as StdString with uid="DCPHCCFCXL",Visible=.t., Left=14, Top=44,;
    Alignment=0, Width=204, Height=15,;
    Caption="Persona fisica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_25 as StdString with uid="YGYYBXXOBR",Visible=.t., Left=14, Top=172,;
    Alignment=0, Width=228, Height=15,;
    Caption="Altri soggetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_26 as StdString with uid="CCNGTZXXUE",Visible=.t., Left=33, Top=97,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di residenza o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_27 as StdString with uid="AKNIDKFOUL",Visible=.t., Left=33, Top=64,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_28 as StdString with uid="XXYSRZPODK",Visible=.t., Left=275, Top=64,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_29 as StdString with uid="GTEVNWAGWC",Visible=.t., Left=603, Top=64,;
    Alignment=0, Width=60, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_30 as StdString with uid="AVMJOZOAMZ",Visible=.t., Left=33, Top=192,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune della sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_31 as StdString with uid="ARKFUFVBKV",Visible=.t., Left=468, Top=97,;
    Alignment=0, Width=60, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_32 as StdString with uid="OWDEPTUCER",Visible=.t., Left=33, Top=130,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_33 as StdString with uid="ASIOLJGQNU",Visible=.t., Left=145, Top=64,;
    Alignment=0, Width=48, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_34 as StdString with uid="RFSIWFUVPA",Visible=.t., Left=539, Top=97,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_35 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=0, Top=11,;
    Alignment=1, Width=95, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_3_36 as StdString with uid="HNBYHSXXZT",Visible=.t., Left=468, Top=192,;
    Alignment=0, Width=60, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_37 as StdString with uid="GQYCDLXSEK",Visible=.t., Left=33, Top=291,;
    Alignment=0, Width=223, Height=13,;
    Caption="Indirizzo di domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_38 as StdString with uid="XRGWLJIBWO",Visible=.t., Left=468, Top=258,;
    Alignment=0, Width=60, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_39 as StdString with uid="BCAUNHALPM",Visible=.t., Left=539, Top=192,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_40 as StdString with uid="OXXFNCZRJN",Visible=.t., Left=539, Top=258,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_41 as StdString with uid="ZFKUQNCSTW",Visible=.t., Left=33, Top=225,;
    Alignment=0, Width=223, Height=13,;
    Caption="Indirizzo della sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_42 as StdString with uid="TNQFMKZSKA",Visible=.t., Left=33, Top=258,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_43 as StdString with uid="AJIXNHKAAD",Visible=.t., Left=344, Top=11,;
    Alignment=1, Width=156, Height=15,;
    Caption="Cod. fis. intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_3_44 as StdString with uid="CSLGHNVXQQ",Visible=.t., Left=14, Top=332,;
    Alignment=0, Width=228, Height=15,;
    Caption="Visto di conformit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_46 as StdString with uid="PFUNCMOJXV",Visible=.t., Left=20, Top=363,;
    Alignment=0, Width=324, Height=13,;
    Caption="Codice fiscale del responsabile del C.A.F. o professionista"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_11 as StdBox with uid="BTOQXYGKKW",left=10, top=59, width=672,height=2

  add object oBox_3_23 as StdBox with uid="QVBCKAWCXU",left=10, top=187, width=672,height=2

  add object oBox_3_45 as StdBox with uid="FWAQWRIPNX",left=10, top=347, width=672,height=2
enddefine
define class tgsri_sftPag4 as StdContainer
  Width  = 695
  height = 447
  stdWidth  = 695
  stdheight = 447
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_4_1 as StdField with uid="JPXZKEKVIP",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data iniziale di stampa",;
    HelpContextID = 71004362,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=108, Top=33

  func oDATINI_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATINI < cp_CharToDate("01-01-02")))
    endwith
    return bRes
  endfunc

  add object oDATFIN_4_2 as StdField with uid="FNNEPIJJDP",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data finale di stampa",;
    HelpContextID = 7442230,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=356, Top=33

  func oDATFIN_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN < cp_CharToDate("01-01-02")))
    endwith
    return bRes
  endfunc

  add object oDirName_4_3 as StdField with uid="EJPBDNOIEF",rtseq=111,rtrep=.f.,;
    cFormVar = "w_DirName", cQueryName = "DirName",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 16488246,;
   bGlobalFont=.t.,;
    Height=21, Width=446, Left=108, Top=123, InputMask=replicate('X',200)


  add object oBtn_4_4 as StdButton with uid="QIOPRPPDCF",left=556, top=123, width=19,height=21,;
    caption="...", nPag=4;
    , HelpContextID = 40297770;
    , tabstop = .f.;
  , bGlobalFont=.t.

    proc oBtn_4_4.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFileName_4_5 as StdField with uid="OOIIRZMLFQ",rtseq=112,rtrep=.f.,;
    cFormVar = "w_FileName", cQueryName = "FileName",;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 203278405,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=108, Top=145, InputMask=replicate('X',30)

  add object oIDESTA_4_6 as StdField with uid="QWRFCHFTQX",rtseq=113,rtrep=.f.,;
    cFormVar = "w_IDESTA", cQueryName = "IDESTA",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 198335866,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=356, Top=243, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"'

  func oIDESTA_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
    endwith
   endif
  endfunc

  add object oPROIFT_4_7 as StdField with uid="COXZWAWVRC",rtseq=114,rtrep=.f.,;
    cFormVar = "w_PROIFT", cQueryName = "PROIFT",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 105140470,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=356, Top=265, cSayPict='"999999"', cGetPict='"999999"'

  func oPROIFT_4_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
    endwith
   endif
  endfunc


  add object oBtn_4_26 as StdButton with uid="MGXPFVZZOE",left=590, top=386, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per inizio elaborazione";
    , HelpContextID = 40470042;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_26.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_27 as StdButton with uid="WONLLGUZCO",left=640, top=386, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 33181370;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_4_13 as StdString with uid="LCHEPLEODA",Visible=.t., Left=33, Top=33,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="GUHHFLLZER",Visible=.t., Left=289, Top=33,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="QDPZXPECZB",Visible=.t., Left=24, Top=123,;
    Alignment=1, Width=80, Height=15,;
    Caption="Directory:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="FKJYZDRMHF",Visible=.t., Left=24, Top=145,;
    Alignment=1, Width=80, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_4_17 as StdString with uid="TKFFKWNXKB",Visible=.t., Left=11, Top=11,;
    Alignment=0, Width=213, Height=15,;
    Caption="Parametri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_20 as StdString with uid="KLZWVWGXPN",Visible=.t., Left=11, Top=90,;
    Alignment=0, Width=204, Height=15,;
    Caption="File telematico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_22 as StdString with uid="XKVUVPRKKQ",Visible=.t., Left=11, Top=211,;
    Alignment=0, Width=461, Height=15,;
    Caption="Protocollo telematico della dichiarazione da sostituire"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_23 as StdString with uid="ETMYQQHNMJ",Visible=.t., Left=0, Top=243,;
    Alignment=1, Width=352, Height=15,;
    Caption="Identificativo assegnato dal servizio telematico all'invio:"  ;
  , bGlobalFont=.t.

  add object oStr_4_24 as StdString with uid="ZMKICUMICZ",Visible=.t., Left=0, Top=265,;
    Alignment=1, Width=352, Height=15,;
    Caption="Progressivo dichiarazione all'interno del file inviato:"  ;
  , bGlobalFont=.t.

  add object oBox_4_18 as StdBox with uid="EWLUHYXJCA",left=7, top=26, width=676,height=2

  add object oBox_4_19 as StdBox with uid="ALLGPWEGAO",left=7, top=105, width=676,height=2

  add object oBox_4_21 as StdBox with uid="FUJGWUZXRO",left=7, top=226, width=676,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_sft','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsri_sft
proc SfogliaDir (parent)
local PathName, oField
  PathName = Cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
  endif
endproc

func IsPerFis(Cognome,Nome,Denominazione)
do case
case empty(Cognome) and empty(Nome) and empty(Denominazione)
  return 'N'
case !empty(Cognome) or !empty(Nome)
  return 'S'
case !empty(Denominazione)
  return 'N'
endcase
* --- Fine Area Manuale
