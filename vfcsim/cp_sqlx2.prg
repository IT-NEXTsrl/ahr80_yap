* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_SQLX2
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Operazioni con connessione ODBC: query asincrona, sqlexec che indica quante
* righe sono state modificate
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

#Define SQL_NTS                         (-3)
#Define SQL_NULL_HSTMT                    0
#Define SQL_CHAR                          1
#Define SQL_NUMERIC                       2
#Define SQL_DECIMAL                       3
#Define SQL_INTEGER                       4
#Define SQL_SMALLINT                      5
#Define SQL_FLOAT                         6
#Define SQL_REAL                          7
#Define SQL_DOUBLE                        8
#Define SQL_VARCHAR                      12
#Define SQL_DATE                          9
#Define SQL_TIME                         10
#Define SQL_TIMESTAMP                    11
#Define SQL_LONGVARCHAR               65535    && (-1)
#Define SQL_BINARY                    65534    && (-2)
#Define SQL_VARBINARY                 65533    && (-3)
#Define SQL_LONGVARBINARY             65532    && (-4)
#Define SQL_BIGINT                    65531    && (-5)
#Define SQL_TINYINT                   65530    && (-6)
#Define SQL_BIT                       65529    && (-7)
#Define SQL_SUCCESS                       0
#Define SQL_SUCCESS_WITH_INFO             1
#Define SQL_NO_DATA                     100
#Define SQL_CLOSE                         0
#Define SQL_DROP                          1
#Define SQL_FETCH_NEXT                    1
#Define SQL_FETCH_ABSOLUTE                5
#Define SQL_CURSOR_TYPE                  6
#Define SQL_CURSOR_DYNAMIC               2
#Define SQL_BEST_ROWID                   1
#Define SQL_ROWVER                       2
#Define SQL_SCOPE_CURROW                 0
#Define SQL_NO_NULLS                      0

#Define SQL_DRIVER_HDBC                   3
#Define SQL_DRIVER_HENV                   4
#Define SQL_DRIVER_HSTMT                  5
#Define SQL_DRIVER_NAME                   6
#Define SQL_DRIVER_VER                    7
#Define SQL_DBMS_NAME                    17
#Define SQL_DBMS_VER                     18
#Define SQL_DATABASE_NAME                16

Proc SQLXodbcapi
    If Type("__SQLXodbcapi__")="C"
        Return
    Endif
    Public __SQLXodbcapi__
    __SQLXodbcapi__="Installed"
    Declare short SQLAllocStmt In odbc32.Dll ;
        integer  nhdbc, ;
        integer @nHstmt
    Declare short SQLExecDirect In odbc32.Dll ;
        integer  nHstmt, ;
        string   cSqlStr, ;
        integer  nSqlStrSize
    Declare short SQLFetch In odbc32.Dll ;
        integer  nHstmt
    Declare short SQLDescribeCol In odbc32.Dll ;
        integer  nHstmt, ;
        integer  nCol, ;
        string  @cColName, ;
        integer  nColNameMax, ;
        integer @nColNameSize, ;
        integer @nSqlType, ;
        integer @nColDef, ;
        integer @nScale, ;
        integer @nNullable
    Declare short SQLError In odbc32.Dll ;
        integer  nHenv, ;
        integer  nHdbc, ;
        integer  nHstmt, ;
        string  @cSqlState, ;
        integer @nNativeError, ;
        string  @cErrorMsg, ;
        integer  nErrorMsgMax, ;
        integer @nErrorMsgSize
    Declare short SQLFreeHandle In odbc32.Dll ;
        integer  nHandleType, ;
        integer  nHandle
    Declare short SQLFreeStmt In odbc32.Dll ;
        integer  nHstmt, ;
        integer  nOption
    Declare short SQLNumResultCols In odbc32.Dll ;
        integer  nHstmt, ;
        integer @nCol
    Declare short SQLGetData In odbc32.Dll ;
        integer  nHstmt, ;
        integer  nCol, ;
        integer  nCType, ;
        string  @cValue, ;
        integer  nValueMax, ;
        integer @nValueSize
    Declare short SQLExtendedFetch In odbc32.Dll ;
        integer  nHstmt, ;
        integer  nFetchType, ;
        integer  nRow, ;
        integer @nRowFeched, ;
        integer @cRowStatus
    Declare SHORT SQLSetStmtOption In odbc32.Dll ;
        INTEGER  nHstmt, ;
        INTEGER  nOption, ;
        INTEGER  nParam
    Declare SHORT SQLRowCount In odbc32.Dll ;
        INTEGER  nHstmt, ;
        INTEGER  @cRow
    Declare SHORT SQLPrimaryKeys In odbc32.Dll ;
        INTEGER  nHstmt, ;
        string  @cCatalog, ;
        integer  nCatalog, ;
        string  @cSchema, ;
        integer  nSchema, ;
        string  @cTable, ;
        integer  nTable
    Declare SHORT SQLForeignKeys In odbc32.Dll ;
        INTEGER  nHstmt, ;
        string  @cPKCatalog, ;
        integer  nPKCatalog, ;
        string  @cPKSchema, ;
        integer  nPKSchema, ;
        string  @cPKTable, ;
        integer  nPKTable, ;
        string  @cFKCatalog, ;
        integer  nFKCatalog, ;
        string  @cFKSchema, ;
        integer  nFKSchema, ;
        string  @cFKTable, ;
        integer  nFKTable
    Declare SHORT SQLSpecialColumns In odbc32.Dll ;
        INTEGER  nHstmt, ;
        integer  nIdType, ;
        string  @cCatalog, ;
        integer  nCatalog, ;
        string  @cSchema, ;
        integer  nSchema, ;
        string  @cTable, ;
        integer  nTable, ;
        integer  nScope, ;
        integer  nNullable
    Declare SHORT SQLGetInfo In odbc32.Dll ;
        INTEGER  nHdbc, ;
        INTEGER  nInfoType, ;
        STRING  @cInfoValue, ;
        INTEGER  nInfoValueMax, ;
        INTEGER @nInfoValueSize
Endproc

Define Class sqlx As Custom
    nConn=-1
    o_nConn=0
    o_cCmd=''
    o_cCursor=''
    o_cDatabaseType=''
    haspost=.F.
    cfrom='' && Riconessione automatica
    Dimension poststruct[1]
    poststruct[1]=0
    postname=''
    connectiontimestamp='valoreacaso'
    bDuplicateConnection=.F.
    bAsyncConn=.T.
    bWaitforreply=.F. && Se .t. oggetto in attesa di query asincrona...
    Proc Destroy()
        This.CloseConn()
        
    *--- Zucchetti Aulla Inizio - gestione riconessione degli zoom secondari degli elenchi 
    **riesegue la query (metodo utilizzato per ripristinare la conessione secondaria quando si attiva la finestra)
     PROCEDURE requery ()   
     IF  TYPE ("this.parent.ozoomelenco")='O'
     	this.parent.ozoomelenco.DeferredQuery()
     ENDIF
     ENDPROC
     *--- Zucchetti Aulla Fine - gestione riconessione degli zoom secondari degli elenchi
    
    Func SQLExec(nConn,cCmd,cCursor,cDatabaseType , parcfrom) && Riconessione automatica aggiunto parametro
        Local fs,cu,fsz,T,bResetCursor,i_olderr
        Private re
        bResetCursor=.F.
        This.o_nConn=nConn
        *---- Riconnessione automatica
        Local nElements
        cfrom=parcfrom

        *memorizza il riferimento al server a cui � collegato poiche quando si effettua
        *la riconessione il riferimento alla connessione principale potrebbe essere sbagliato (la principale e gi� stata ripristinata)
        * e perci� si p� usare il nome del server per recuperare la stringa di connessione
        *----Riconnessione automatica
        This.o_cCmd=cCmd
        This.o_cCursor=cCursor
        This.o_cDatabaseType=cDatabaseType

        *IF nConn>1000000
        *   RETURN i_oRemoteConn[nConn-1000000].Query(cCmd,cCursor)
        *ENDIF
        If Empty(cDatabaseType)
            cDatabaseType=SQLXGetDatabaseType(nConn)
        Endif

        *wait window "Prima della connessione:"+cCursor+' '+cDatabaseType

        This.SetConn(nConn,cDatabaseType)
        If Vartype(cDatabaseType)='C'
        	IF m.i_bOptimizerHints
	            cCmd=This.OptimizerHints(nConn,cCmd,cDatabaseType)
	        EndIf
        Else
            cDatabaseType='unk.'
        Endif
        re=0
        i_olderr=On('ERROR')
        On Error re=-1

        *ACTIVATE SCREEN
        *? this.nConn,i_sqlx2nconn,i_sqlx2ndupl
        *? 'query '+IIF(this.bAsyncConn,'asincrona','sincrona')+' '+cCmd
        *cCmd=cp_AggiungiTraduzioni(cCmd)

        *** Zucchetti Aulla - controllo tabella temporanea
        * non deve essere fatta mai una query asincrona su tabella temporanea
        If This.bAsyncConn
            SQLCancel(This.nConn)
            *wait window "sqlx2="+cCmd
            If Vartype(cDatabaseType)='C' And cDatabaseType='SQLServer' And SQLGetprop(This.nConn,'Async')
                * Zucchetti Aulla - aggiunta gestione accessi
                cp_sqlexec(This.nConn,'set transaction isolation level read uncommitted','',This) && Zucchetti aulla aggiunto , this
                * Zucchetti Aulla -fine aggiunta gestione accessi
            Endif
            fs=CursorGetProp('FetchAsNeeded',0)
            CursorSetProp('FetchAsNeeded',.T.,0)
            fsz=CursorSetProp('FetchSize',200,0)
            If Vartype(cCursor)<>'C'
                cCursor='sqlresult'
            Endif
            This.PreCloseCursor(cCursor)
            re=cp_sqlexec(This.nConn,cCmd,cCursor, This, .T.) &&Riconnessione automatica - aggiunto this per gestione riconessione automatica query asincrone
            re=This.waitforreply(re,cCmd,cCursor,cDatabaseType)
            If re=-1
                bResetCursor=.T.
                cp_ErrorMsg(MSG_ERROR_EXECUTING_QUERY)
            Else
                CursorSetProp('FetchSize',50)
            Endif
            CursorSetProp('FetchAsNeeded',fs,0)
            CursorSetProp('FetchSize',fsz,0)
            This.PostCloseCursor(bResetCursor,cCursor)
        Else
            If Inlist(cDatabaseType,'PostgreSQL','MySQL','Interbase','SAPDB','Adabas','DB2')
                CursorSetProp('MaxRecords',Iif(Type('i_nZoomMaxRows')='N',i_nZoomMaxRows,250),0)
                re=cp_sqlexec(This.nConn,cCmd,cCursor,This) &&Riconnessione automatica - aggiunto this per gestione riconessione automatica query asincrone
                CursorSetProp('MaxRecords',-1,0)
            Else
                This.PreCloseCursor(cCursor)
                fs=CursorSetProp('FetchAsNeeded',.T.,0)
                fsz=CursorSetProp('FetchSize',Iif(Type('i_nZoomMaxRows')='N',i_nZoomMaxRows,250),0)
                SQLSetprop(This.nConn,'Async',.T.)

                ********* Zucchetti Aulla - aggiunta gestione accessi
                If Vartype(cDatabaseType)='C' And cDatabaseType='SQLServer'
                    cp_sqlexec(This.nConn,'set transaction isolation level read uncommitted',This)
                Endif
                ********* Zucchetti Aulla -fine aggiunta gestione accessi

                re=cp_sqlexec(This.nConn,cCmd,cCursor, .T.)
                re=This.waitforreply(re,cCmd,cCursor,cDatabaseType)
                *--- 
                LOCAL msgErr_tmp
                msgErr_tmp = ''
                *---
                If re=-1
                    bResetCursor=.T.
                    cp_ErrorMsg(MSG_ERROR_EXECUTING_QUERY)
                    *---
                    msgErr_tmp = MESSAGE()
                    *---
                Else
                    SQLCancel(This.nConn)
                Endif
                SQLSetprop(This.nConn,'Async',.F.)
                *--- La SQLSetprop sbianca la message() quindi devo rivalorizzarla
                *--- altrimenti non viene mostrato l'errore sql
		IF re=-1 AND !EMPTY(msgErr_tmp) 
		    ERROR msgErr_tmp             
                Endif
                ********* Zucchetti Aulla -fine aggiunta gestione accessi

                CursorSetProp('FetchAsNeeded',fs,0)
                CursorSetProp('FetchSize',fsz,0)
                This.PostCloseCursor(bResetCursor,cCursor)
            Endif
        Endif
        On Error &i_olderr
        Return(re)
    Function waitforreply(re,cCmd,cCursor,cDatabaseType)
        Local i_wt,T, l_outtime
        This.bWaitforreply=.T.
        i_wt=0.001
        If re<>-1
            *--- Activity Logger, disabilito la traccia nel caso di query asincrona...
            If i_nACTIVATEPROFILER<>0
                Local nOld_Activity,i_fTime,i_fTime_sec
                nOld_Activity=i_nACTIVATEPROFILER
                i_nACTIVATEPROFILER=-1
                i_fTime=Datetime()
                i_fTime_sec=Seconds()
            Endif
            *---Activity Logger, disabilito la traccia nel caso di query asincrona...
            m.t=Seconds()
            l_outtime = .F.
            Do While Not(Used(cCursor)) And re<>-1
                re=cp_sqlexec(This.nConn,cCmd,cCursor,This) &&Riconnessione automatica - aggiunto this per gestione riconessione automatica query asincrone
                If m.t+2<Seconds()
                    CP_MSG(cp_MsgFormat(MSG_EXECUTING_QUERY___D_ESC_TO_INTERRUPT,cDatabaseType))
                    T=Seconds()
                    l_outtime = .T.
                Endif
                If l_outtime
                    If Inkey(i_wt,'MH')=94
                        SQLCancel(This.nConn)
                        re=-1
                        CP_MSG(cp_Translate(MSG_INTERRUPTED_QM))
                    Endif
                    i_wt=i_wt+0.01
                    l_outtime = .F.
                Endif
            Enddo
            Wait Clear
            *--- Activity Logger, disabilito la traccia nel caso di query asincrona...
            If i_nACTIVATEPROFILER=-1
                i_nACTIVATEPROFILER=nOld_Activity
            Endif
            If i_nACTIVATEPROFILER>0
                cp_ActivityLogger(i_fTime, i_fTime_sec, Seconds(), "ASY", cCmd, Iif(Type("i_curform")="C",i_curform,Space(10)), This.nConn, re, Iif(re=-1,Message(),''), , Iif(Type("cCursor")="C",cCursor,"")," ",.T.)
            Endif
            *--- Acctivity Logger, disabilito la traccia nel caso di query asincrona...

        Endif
        This.bWaitforreply=.F.
        Return(re)
        *func ReQuery()
        *  return(this.SQLExec(this.o_nConn,this.o_cCmd,this.o_cCursor,this.o_cDatabaseType))
    Proc SetConn(nConn,cDatabaseType , recon) && Riconnessione automatica - aggiunto parametro per gestione riconessione automatica query asincrone
        If This.nConn=-1 Or recon && Riconnessione automatica - aggiunto or per gestione riconessione automatica query asincrone
            * --- duplica la connessione corrente per poter operare liberamente
            This.DuplConn(nConn,cDatabaseType)
        Else
            * --- c'era una connessione aperta, verifica se e' uguale alla precedente
            Local i_cn1,i_cn2,i_olderr
            Private i_err
            i_olderr=On('ERROR')
            i_err=.F.
            On Error i_err=.T.
            * --- potrebbero dare errore se la connessione e' caduta
            i_cn1=SQLGetprop(This.nConn,"ConnectString")
            i_cn2=SQLGetprop(nConn,"ConnectString")
            On Error &i_olderr
            If i_err
                Return
            Endif
            If !(i_cn2==i_cn1)
                * --- deve cambiare connessione, la chiude ed apre la nuova
                This.CloseConn()
                This.DuplConn(nConn,cDatabaseType)
            Else
                * --- chiude la query precedente
                SQLCancel(This.nConn)
            Endif
        Endif
    Proc DuplConn(nConn,cDatabaseType)
        Local s,bNew
        If Type('i_sqlx2nconn')<>'U' And !This.bDuplicateConnection
            If i_sqlx2nconn=nConn
                If i_sqlx2ndupl<>0
                    * --- posso riusare la connessione asincrona gia' aperta
                    SQLCancel(i_sqlx2ndupl)
                    *--- Zucchetti Aulla Inizio - su database PostgreSQL nella connessione asincrona non funziona la SQLCancel() per un errore del Driver ODBC
                    If SQLXGetDatabaseType(nConn)="PostgreSQL"
                        SQLDisconnect(i_sqlx2ndupl)
                        i_sqlx2ndupl=Sqlstringconnect(i_serverconn[1,8])
                        SQLSetprop(i_sqlx2ndupl,'Async',.T.)
                    Endif
                    *--- Zucchetti Aulla Fine - su database PostgreSQL nella connessione asincrona non funziona la SQLCancel() per un errore del Driver ODBC
                    i_sqlx2dupltimestamp=Str(Seconds())+Str(Rand())
                    This.connectiontimestamp=i_sqlx2dupltimestamp
                    This.nConn=i_sqlx2ndupl
                    This.bAsyncConn=.T.
                    Return
                Else
                    * --- si e' scelto di non usare la connessione secondaria asincrona
                    This.nConn=nConn
                    This.bAsyncConn=.F.
                    Return
                Endif
            Endif
        Endif
        If Vartype(i_bDisableAsyncConn)<>'U' And i_bDisableAsyncConn
            This.nConn=nConn
            This.bAsyncConn=.F.
            Return
        Endif
        s=SQLGetprop(nConn,"ConnectString")
        Do Case
            Case cDatabaseType='Oracle' And Not('CONNECTSTRING'$s)
                s=s+';CONNECTSTRING='
            Case At('DB2',cDatabaseType)=1 And ('PWD=;'$s Or Not('PWD'$s))
                If At('PWD',s)=0
                    s=s+'PWD= ;'
                Else
                    s=Strtran(s,'PWD=;','PWD= ;')
                Endif
        Endcase
        This.nConn=Sqlstringconnect(s)
        * --- se il comando va in errore lo segnalo
        If This.nConn=-1
            * Impossibile riconnettersi...
            cp_errormsg(MSG_CONNECTION_ERROR)
            Return
        Endif
        SQLSetprop(This.nConn,'Async',.T.)
        cp_PostOpenConn(This.nConn)
        Return
    Proc CloseConn()
        If This.nConn<>-1
            If !This.bAsyncConn
                * --- era stata utilizzata la connesione principale che non va chiusa
                This.nConn=-1
                Return
            Endif
            If Type('i_sqlx2nconn')<>'U' And This.nConn=i_sqlx2ndupl
                If i_sqlx2dupltimestamp=This.connectiontimestamp
                    SQLCancel(This.nConn)
                Endif
                This.nConn=-1
                Return  && non chiudere questa connessione che verra' riutilizzata
            Endif
            *--- Zucchetti Aulla Inizio - Se il disegnatore di query � aperto  e chiudo adhoc mi restituisce errore
            Local l_oldError, l_Error
            l_oldError = On("Error")
            On Error l_Error=.T.
            SQLCancel(This.nConn)
            SQLDisconn(This.nConn)
            On Error &l_oldError
            *--- Zucchetti Aulla Fine - Se il disegnatore di query � aperto  e chiudo adhoc mi restituisce errore
            *wait window "sqlx2=Close connection"
        Endif
        This.nConn=-1
        Return
    Proc PreCloseCursor(cCursor)
        This.haspost=.F.
        If Used(cCursor)
            *select (cCursor)
            *=Afields(this.poststruct)
            *this.haspost=.t.
            *use
            This.haspost=.T.
            Select (cCursor)
            This.postname=Sys(2015)
            Use Dbf() Again In 0 Alias (This.postname)
            Select (cCursor)
            Use
        Endif
    Proc PostCloseCursor(bResetCursor,cCursor)
        *if this.haspost and bResetCursor
        *  create cursor (cCursor) from array this.poststruct
        *endif
        If This.haspost
            If bResetCursor
                Select (This.postname)
                Use Dbf() Again In 0 Alias (cCursor)
                Select (This.postname)
                Use
            Else
                Select (This.postname)
                Use
            Endif
        Endif
        *proc CloseCursor(cCursor)
        *  if used(cCursor)
        *    select (cCursor)
        *    use
        *  endif
        *  return
    Func OptimizerHints(nConn,cCmd,cDatabaseType)
        Local p,l,a,w
        If cDatabaseType='SQLServer'
            *p=at(' from ',lower(cCmd))
            *if p<>0
            *  p=p+5
            *  l=len(cCmd)
            *  do while !substr(cCmd,p,1)$" ," and p<=l
            *    p=p+1
            *  enddo
            *  if p>0 and p<l
            *    if substr(cCmd,p,1)=","
            *      * --- e' una ",", non c'e' sicuramente alias
            *      cCmd=substr(cCmd,1,p-1)+'(fastfirstrow nolock)'+substr(cCmd,p)
            *    else
            *      * --- l' ottimizzazione va messa dopo l' eventuale alias
            *      a=p+1
            *      do while !substr(cCmd,a,1)$" ," and a<=l
            *        a=a+1
            *      enddo
            *      w=lower(substr(cCmd,p+1,a-p-1))
            *      if empty(w) or inlist(w,"right","left","full","where","inner")
            *        * --- andava bene la posizione p
            *        cCmd=substr(cCmd,1,p-1)+'(fastfirstrow nolock)'+substr(cCmd,p)
            *      else
            *        * --- spostarsi dopo l' alias
            *        cCmd=substr(cCmd,1,a-1)+'(fastfirstrow nolock)'+substr(cCmd,a)
            *      endif
            *    endif
            *  endif
            *endif
            cCmd=OptimizeForSQLServer(cCmd)
            cCmd=cCmd+' OPTION(FAST 10)'
        Endif
        If cDatabaseType='Oracle'
            p=At('select /*+ ',Lower(cCmd))
            If p<>0
                cCmd=Stuff(cCmd,p,11,'select /*+ FIRST_ROWS ')
            Else
                p=At('select ',Lower(cCmd))
                If p<>0
                    cCmd=Stuff(cCmd,p,7,'select /*+ FIRST_ROWS */ ')
                Endif
            Endif
        Endif
        *wait window "sqlx2="+cCmd
        If At('DB2',cDatabaseType)=1
            cCmd=cCmd+' optimize for 10 rows'
        Endif
        Return(cCmd)
Enddefine

* --- Zucchetti Aulla - errore odbc di postgres: aggiunto parametro cStm
Func SqlxODBCError(nhdbc,nHstmt,nCoderr , nConn,cStm) && Riconnesione automatica - aggiunto parametro per gestione riconessione automatica query asincrone
    Local cSqlState,nNativeError,nErrorMsgSize
    cSqlState=Replicate(Chr(0),255)
    nNativeError=0
    cErrorMsg=Replicate(Chr(0),255)
    nErrorMsgSize = 0
    If Inlist(SQLError(Val(Sys(3053)),nhdbc,nHstmt,@cSqlState,@nNativeError,@cErrorMsg,Len(cErrorMsg),@nErrorMsgSize),SQL_SUCCESS,SQL_NO_DATA,SQL_SUCCESS_WITH_INFO)
        *cMsg=     'State : '+substr(cSqlState,1,at(chr(0),cSqlState)-1)+chr(13)+chr(10)
        *cMsg=cMsg+'Native Error : '+str(nNativeError)+chr(13)+chr(10)
        *cMsg=cMsg+'Error : '+substr(cErrorMsg,1,nErrorMsgSize-1)+chr(13)+chr(10)
        cMsg=Substr(cErrorMsg,1,nErrorMsgSize-1)

        * --- Zucchetti Aulla Inizio - errore odbc di postgres 
        * Nel driver odbc non sono mappati alcuni messaggi di errore; 
        * ripeto l'ultimo comando eseguito e prendo l'errore dalla message()
        If Vartype(cStm)="C" And Not Empty(cStm) And Vartype(nConn)="N" And ALEN(i_serverconn,1)<=nConn And i_serverconn[nConn,6]="PostgreSQL"
            If nNativeError=7 And cSqlState="23503"
                * Violazione chiave esterna
                SQLExec(nConn, cStm)
                cMsg=Message()
            Endif
        Endif
        * --- Zucchetti Aulla Fine - errore odbc di postgres         

    Else
        cMsg= cp_Translate(MSG_ERROR_CL_NO_INFO_ON_ERROR_FROM_ODBC_DRIVER)
    Endif
    * Riconnesione automatica -inizio
    If Vartype(nCoderr)='N'
        nCoderr=nNativeError
    Endif
    If nNativeError=10054 And (Type("i_AUTORICONNECT")<>'L' Or i_AUTORICONNECT<>.T.)
        * Riconnesione automatica -fine
        If cp_YesNo(MSG_CONNECTION_TO_SERVER_FALLEN_F_EXIT_QP,32,.t.,.f.,MSG_ERROR)
            Quit
        Endif
    Endif
    * Riconnesione automatica -inizio
    If(Vartype(i_AUTORICONNECT)='L' And  i_AUTORICONNECT=.T. And Vartype(i_nconn)='N' And Not Cp_IsConnected(i_nconn)) Or ( Vartype(i_DEADREQUERY)='L' And i_DEADREQUERY=.T. And CP_DBTYPE='SQLServer' And Vartype( nCoderr )='N' And nCoderr =1205 ) Or ( Vartype(i_DEADREQUERY)='L' And i_DEADREQUERY=.T. And CP_DBTYPE='Oracle' And Vartype( nCoderr )='N' And nCoderr =60 )
        * Riconnesione automatica -fine
        * Se il fallimento della query � dovuto alla caduta della connessione o ad un deadlock non si visualizzano messaggi di errore
    Else
        Error (cMsg)
        * Zucchetti Aulla -inizio
    Endif
    * Zucchetti Aulla -fine
    Return(.T.)

Func SQLXInsUpdDel(nConn,cStm , nCoderr) && Zucchetti Aulla - aggiunto parametro per gestione riconessione automatica query asincrone
    Local hs,nhdbc,r
    *--- Riconnessione automatica
    If (Alen(i_ServerConnBis)>2)
        nConn=cp_SearchConn (nConn)
    Endif
    *--- Riconnessione automatica
    If Not(Upper(Left(cStm,3))+"|"$"UPD|DEL|INS|")
        CP_MSG(cp_MsgFormat(MSG_SQLXUPDDEL_WITH_DISABLED_STATEMENT__ ,cStm),.F.)
        Return(0)
    Endif
    Do SQLXodbcapi
    * --- Get connection handle
    nhdbc=SQLGetprop(nConn,"ODBCHdbc")
    hs=0
    * --- Allocate statement
    r=SQLAllocStmt(nhdbc,@hs)
    If r<>SQL_SUCCESS
        Return(-1)
    Endif
    * --- sistema gli =NULL
    Local p
    * --- ricerca l'ultime Where (la routine � utilizzata per scritture pu�)
    * --- dare problemi per frasi del tipo
    * --- UPDATE CONTI SET ANDESCRI='PROVA' where ANDESCRI='it is where you are '
    p=Rat(' where ',Lower(cStm))
    If p<>0
        * Conversione da =NULL a IS NULL
        * aggiunge uno spazio in coda a cStm per convertire l'ultimo =NULL
        cStm=Left(cStm,p)+Strtran(Substr(cStm+' ',p+1),'=NULL ',' is NULL ')
    Endif
    *wait window "cCmd: "+cStm
    * --- Excute statement
    r=SQLExecDirect(hs,cStm,SQL_NTS)
    If r<>SQL_SUCCESS And r<>SQL_SUCCESS_WITH_INFO And r<>SQL_NO_DATA
    	* --- Zucchetti Aulla - errore odbc di postgres: aggiunto parametro cStm
        =SqlxODBCError(nhdbc,hs,@nCoderr,nConn,cStm) && Riconnesione automatica - aggiunto parametro per gestione riconessione automatica query asincrone
        =SQLFreeStmt(hs,SQL_DROP)
        Return(-1)
    Endif
    If r=SQL_NO_DATA
        r=0
    Else
        =SQLRowCount(hs,@r)
    Endif
    =SQLFreeStmt(hs,SQL_DROP)
    Return(r)

Func SQLXPrimaryKey(nConn,cTable,cDatabaseType)
    Local hs,nhdbc,r,cTableSchem,bError
    Do SQLXodbcapi
    *--- Riconnessione automatica
    If (Alen(i_ServerConnBis)>2)
        nConn=cp_SearchConn (nConn)
    Endif
    *--- Riconnessione automatica
    * --- Get connection handle
    nhdbc=SQLGetprop(nConn,"ODBCHdbc")
    hs=0
    * --- Allocate statement
    r=SQLAllocStmt(nhdbc,@hs)
    If r<>SQL_SUCCESS
        Return(-1)
    Endif
    Do Case
        Case At('DB2',cDatabaseType)=1
            * --- Excute statement
            cTableSchem=Trim(SQLXGetDatabaseName(nConn))
            r=SQLPrimaryKeys(hs,.Null.,0,cTableSchem,Len(cTableSchem),Upper(cTable),Len(cTable))
        Case cDatabaseType='Access'
            * --- Excute statement
            r=SQLSpecialColumns(hs,SQL_BEST_ROWID,.Null.,0,.Null.,0,cTable,Len(cTable),SQL_SCOPE_CURROW,SQL_NO_NULLS)
        Otherwise
            * --- Excute statement
            r=SQLPrimaryKeys(hs,.Null.,0,.Null.,0,Upper(cTable),Len(cTable))
    Endcase
    bError=SQLXGetResult(nhdbc,hs,r,'__pkresult__')
    If Not bError
        Return(-1)
    Endif
    r=''
    Scan
        If cDatabaseType='Access'
            r=Trim(column_name)+Iif(Empty(r),'',',')+r
        Else
            r=r+Iif(Empty(r),'',',')+Trim(column_name)
        Endif
    Endscan
    Use
    =SQLFreeStmt(hs,SQL_DROP)
    Return(r)

Func SQLXSpecialColumn(nConn,cTable)
    Local hs,nhdbc,r,N,bError
    Do SQLXodbcapi
    *--- Riconnessione automatica
    If (Alen(i_ServerConnBis)>2)
        nConn=cp_SearchConn (nConn)
    Endif
    *--- Riconnessione automatica
    * --- Get connection handle
    nhdbc=SQLGetprop(nConn,"ODBCHdbc")
    hs=0
    * --- Allocate statement
    r=SQLAllocStmt(nhdbc,@hs)
    If r<>SQL_SUCCESS
        Return(-1)
    Endif
    * --- Excute statement
    r=SQLSpecialColumns(hs,SQL_ROWVER,.Null.,0,.Null.,0,cTable,Len(cTable),SQL_SCOPE_CURROW,SQL_NO_NULLS)
    bError=SQLXGetResult(nhdbc,hs,r,'__scresult__')
    If Not bError
        Return(-1)
    Endif
    r=''
    Scan
        Wait Window "r="+r
        r=Trim(column_name)+Iif(Empty(r),'',',')+r
    Endscan
    Brow
    Use
    =SQLFreeStmt(hs,SQL_DROP)
    Return(r)

Func SQLXForeignKey(nConn,cPKTableName)
    Local hs,nhdbc,r,N,bError
    Do SQLXodbcapi
    *---  Riconnessione automatica
    If (Alen(i_ServerConnBis)>2)
        nConn=cp_SearchConn (nConn)
    Endif
    *--- Riconnessione automatica
    * --- Get connection handle
    nhdbc=SQLGetprop(nConn,"ODBCHdbc")
    hs=0
    * --- Allocate statement
    r=SQLAllocStmt(nhdbc,@hs)
    If r<>SQL_SUCCESS
        Return(-1)
    Endif
    * --- Excute statement
    r=SQLForeignKeys(hs,.Null.,0,.Null.,0,cPKTableName,Len(cPKTableName),.Null.,0,.Null.,0,.Null.,0)
    bError=SQLXGetResult(nhdbc,hs,r,'__fkresult__')
    If Not bError
        Return(-1)
    Endif
    r=''
    Scan
        r=r+Iif(Empty(r),'',',')+Trim(FKtable_name)+','+Trim(FK_name)
    Endscan
    Use
    =SQLFreeStmt(hs,SQL_DROP)
    Return(r)

Func SQLXGetResult(nhdbc,hs,nStmtResult,cCursorName)
    Local N,cColumns
    If nStmtResult<>SQL_SUCCESS And nStmtResult<>SQL_SUCCESS_WITH_INFO And nStmtResult<>SQL_NO_DATA
        =SqlxODBCError(nhdbc,hs)
        =SQLFreeStmt(hs,SQL_DROP)
        Return(.F.)
    Endif
    * --- Number of columns in the result
    N=0
    nStmtResult=SQLNumResultCols(hs,@N)
    If nStmtResult<>SQL_SUCCESS
        =SqlxODBCError(nhdbc,hs)
        =SQLFreeStmt(hs,SQL_DROP)
        hs=0
        Return(.F.)
    Endif
    * --- Definitions for the result cursor
    cColumns=sqlx2_GetCursorInfo(hs,N)
    cColumns=Strtran(cColumns,' C(0)',' C(1)')
    * --- Create result cursor
    Create Cursor (cCursorName) (&cColumns)
    Select (cCursorName)
    nStmtResult=SQLFetch(hs)
    Do While nStmtResult=SQL_SUCCESS
        Append Blank
        sqlx2_GetData(hs,N,cCursorName)
        nStmtResult=SQLFetch(hs)
    Enddo
    Return(.T.)

Proc sqlx2_GetCursorInfo(nHstmt,N)
    * --- Return the string 'cc' that contains the descriptor for
    * --- the result set 'n' coloums
    Local i,cColName,nColName,nColDef,nSqlType,nScale,nNullable,cc
    PRIVATE i,cColName,nColName,nColDef,nSqlType,nScale,nNullable,cc
    cc=''
    For i=1 To N
        cColName=Replicate(Chr(0),256)
        nColName=0
        nColDef=0
        nSqlType=0
        nScale=0
        nNullable=0
        * --- Returns the result descriptor
        r=SQLDescribeCol(nHstmt,i, @cColName,Len(cColName),@nColName,@nSqlType,@nColDef,@nScale,@nNullable)
        If r<>SQL_SUCCESS And r<>SQL_SUCCESS_WITH_INFO
            CP_MSG( cp_MsgFormat(MSG_ERROR_IN_DESCRIPTION_OF__,Substr(cColName,1,At(Chr(0),cColName)-1)),.F.)
        Endif
        * --- Name of column
        cc=cc+Substr(cColName,1,At(Chr(0),cColName)-1)+' '
        * --- Datatype of column
        Do Case
            Case Inlist(nSqlType,SQL_CHAR,SQL_VARCHAR,SQL_LONGVARCHAR,65527)
                If nColDef>=255
                    cc=cc+'M'
                Else
                    cc=cc+'C('+Ltrim(Str(nColDef))+')'
                Endif
            Case Inlist(nSqlType,SQL_BINARY,SQL_VARBINARY,SQL_LONGVARBINARY)
                cc=cc+'M'
            Case Inlist(nSqlType,SQL_DECIMAL,SQL_NUMERIC)
                cc=cc+'N('+Ltrim(Str(nColDef))+','+Ltrim(Str(nScale))+')'
            Case nSqlType=SQL_BIT
                cc=cc+ 'L'
            Case Inlist(nSqlType,SQL_TINYINT,SQL_SMALLINT,SQL_INTEGER)
                cc=cc+'I'
            Case nSqlType=SQL_BIGINT
                cc=cc+'C('+Ltrim(Str(nColDef))+')'
            Case Inlist(nSqlType,SQL_REAL,SQL_FLOAT,SQL_DOUBLE )
                cc=cc+'B('+Ltrim(Str(nScale))+')'
            Case nSqlType = SQL_DATE
                cc=cc+'D'
            Case Inlist(nSqlType,SQL_TIME,SQL_TIMESTAMP )
                cc=cc+'T'
            Otherwise
                cc=cc+'C(10)'
                CP_MSG( cp_MsgFormat(MSG_UNKNOWN_SQL_TYPE_FOR____,Substr(cColName,1,At(Chr(0),cColName)-1),Str(nSqlType)),.F.)
        Endcase
        If i!=N
            cc = cc+','
        Endif
    Next
    Return(cc)

Func sqlx2_GetData(nHstmt,N,cCursor)
    * --- Put a result set row in the cursor 'cCursor'
    Local i,r,cBuffer,cField,nDataLen,cFieldType
    Private i,r,cBuffer,cField,nDataLen,cFieldType
    For i=1 To N
        * --- Get a data for the 'i' coloumn in 'cBuffer' pointer
        cBuffer=Replicate(Chr(0),256)
        nDataLen=0
        r=SQLGetData(nHstmt,i,1,@cBuffer, 256, @nDataLen)
        * --- Put into cursor
        cField=Field(i)
        cFieldType=Type(Field(i))
        Do Case
            Case cFieldType='C'
                Replace (cField) With Substr(cBuffer,1,At(Chr(0),cBuffer)-1)
            Case cFieldType='M'
                If nDataLen>=256
                    cBuffer=Replicate(Chr(0),nDataLen+1)
                    r=SQLGetData(nHstmt,i,1,@cBuffer, nDataLen, @nDataLen)
                Endif
                Replace (cField) With Substr(cBuffer,1,At(Chr(0),cBuffer)-1)
            Case Inlist(cFieldType,'N','I','B','Y')
                Replace (cField) With Val(Substr(cBuffer,1,At(Chr(0),cBuffer)-1))
            Case cFieldType='D'
                Replace (cField) With Ctod(Substr(cBuffer,1,At(Chr(0),cBuffer)-1))
            Case cFieldType='T'
                Replace (cField) With Ctot(Substr(cBuffer,1,At(Chr(0),cBuffer)-1))
            Case cFielsType='L'
                Replace (cField) With Iif(Left(cBuffer,1)='1',.T.,.F.)
        Endcase
    Next
    Return(r)

Func SQLXGetDatabaseType(nConn)
    Local hs,nhdbc,r,cInfoValue,nInfoValueSize,cDatabaseType
    *--- Riconnessione automatica
    If (Alen(i_ServerConnBis)>2)
        nConn=cp_SearchConn (nConn)
    Endif
    *--- Riconnessione automatica
    Do SQLXodbcapi
    * --- Get connection handle
    If nConn<=0
        Return('Unknown')
    Endif

    *if nConn>=1000000
    *  return('SQLServer')
    *endif

    nhdbc=SQLGetprop(nConn,"ODBCHdbc")
    * --- Execute statement
    cInfoValue=Space(50)
    r=SQLGetInfo(nhdbc,SQL_DBMS_NAME,@cInfoValue,50,@nInfoValueSize)
    cDatabaseType='Unknown'
    Do Case
        Case "Microsoft SQL Server"$cInfoValue
            cDatabaseType='SQLServer'
        Case "ACCESS"==Substr(cInfoValue,1,6)
            cDatabaseType='Access'
        Case "DB2"==Substr(cInfoValue,1,3)
            If CP_DBTYPE=='DB2/390'
                cDatabaseType=CP_DBTYPE
            Else
                cDatabaseType='DB2'
            Endif
        Case "Oracle"==Substr(cInfoValue,1,6)
            cDatabaseType='Oracle'
        Case "ADABAS"==Substr(cInfoValue,1,6)
            cDatabaseType='Adabas'
        Case "LIVECACHE"$Upper(cInfoValue) Or "SAP DB"$cInfoValue
            cDatabaseType='SAPDB'
        Case "Informix"$cInfoValue
            cDatabaseType='Informix'
        Case "INTERBASE"$Upper(cInfoValue)
            cDatabaseType='Interbase'
        Case "PostgreSQL"$cInfoValue
            cDatabaseType='PostgreSQL'
        Case "MySQL"$cInfoValue
            cDatabaseType='MySQL'
        Case "DBMaker"$cInfoValue
            cDatabaseType='DBMaker'
    Endcase
    Return(cDatabaseType)

Func SQLXGetDatabaseVersion(nConn)
    Local hs,nhdbc,r,cInfoValue,nInfoValueSize,cDatabaseVer
    *--- Riconnessione automatica
    If (Alen(i_ServerConnBis)>2)
        nConn=cp_SearchConn (nConn)
    Endif
    *--- Riconnessione automatica
    Do SQLXodbcapi
    * --- Get connection handle
    If nConn<=0
        Return('Unknown')
    Endif
    nhdbc=SQLGetprop(nConn,"ODBCHdbc")
    * --- Execute statement
    cInfoValue=Space(50)
    r=SQLGetInfo(nhdbc,SQL_DBMS_VER,@cInfoValue,50,@nInfoValueSize)
    cDatabaseVer=cInfoValue
    Return(cDatabaseVer)

Func SQLXGetDatabaseName(nConn)
    Local hs,nhdbc,r,cInfoValue,nInfoValueSize,cDatabaseType
    *--- Riconnessione automatica
    If (Alen(i_ServerConnBis)>2)
        nConn=cp_SearchConn (nConn)
    Endif
    *--- Riconnessione automatica
    Do SQLXodbcapi
    * --- Get connection handle
    nhdbc=SQLGetprop(nConn,"ODBCHdbc")
    * --- Execute statement
    cInfoValue=Space(50)
    r=SQLGetInfo(nhdbc,SQL_DATABASE_NAME,@cInfoValue,50,@nInfoValueSize)
    Return(cInfoValue)

Function OptimizeForSQLServer(cCmd)

    Local i_p,i_cCmd1,i_cCmd2,i_w, i_cCmd3, i_p2, i_cIter, i_pUnion, i_pJoin, i_nLenIter
    i_p = At(' from ',Lower(cCmd))
    If i_p<>0
        * --- cerca di inserire gli hint fastfirstrow e nolock
        i_cCmd1 = Left(cCmd,i_p+5)
        i_cCmd2 = Substr(cCmd,i_p+6)
        i_p = GetPosCommaSpace(i_cCmd2)
        If i_p<>0
            * --- posizione della prima tabella
            If Substr(i_cCmd2,i_p,1)=' '
                * --- e' seguita dall' alias, si sposta fino a dopo l' alias
                i_cCmd1 = i_cCmd1+Left(i_cCmd2,i_p)
                i_cCmd2 = Substr(i_cCmd2,i_p+1)
                i_p = GetPosCommaSpace(i_cCmd2)
                i_w = Lower(Left(i_cCmd2,i_p-1))
                If Inlist(i_w,'right','left','inner','full','where')
                    i_p = 1
                Endif
            Endif
            If Lower(Right(i_cCmd1,7))<>"select "
                i_cCmd1 = i_cCmd1+Left(i_cCmd2,i_p-1)+' with (nolock) '
            Else
                i_cCmd1 = i_cCmd1+Left(i_cCmd2,i_p-1)
            Endif
            i_cCmd2  =Substr(i_cCmd2,i_p)
            * --- ha sistemato il primo hint, ora deve trovare le altre tabelle e aggiungere gli altri nolock
            *     lo fa solo per le tabelle in outer join
            *-- Valuto i casi Union e Join
            i_pUnion = Evl(At(' union ',Lower(i_cCmd2)),Len(i_cCmd2))
            i_pJoin = Evl(At(' join ',Lower(i_cCmd2)),Len(i_cCmd2))
        	i_p = Min( i_pJoin, i_pUnion)
        	i_p = Iif( i_p = Len(i_cCmd2), 0, i_p)
            i_cIter = Iif(i_p = At(' join ',Lower(i_cCmd2)), ' join ', ' union ')
            Do While i_p<>0
				i_nLenIter = Len(Alltrim(i_cIter))
				i_cCmd1=i_cCmd1+Left(i_cCmd2,i_p + i_nLenIter)
				i_cCmd2=Substr(i_cCmd2,i_p + i_nLenIter+1)
				i_p = Iif( i_cIter =' join ', At(' on ',Lower(i_cCmd2)), Evl(At(' join ',Lower(i_cCmd2)),Len(i_cCmd2)))
                If i_p<>0
                    If i_cIter = ' join ' And Left(Ltrim(i_cCmd2),1)<>"(" And Left(Lower(Ltrim(Substr(Ltrim(i_cCmd2),2,Len(i_cCmd2)))),6)<>"select"
                        i_cCmd1 = i_cCmd1+Left(i_cCmd2,i_p-1)+' with (nolock) '
					Else
                    	*-- i_cCmd3 tiene solo ci� che � tra join e on
			            i_cCmd3 = Left(i_cCmd2,i_p-1)
			            i_cCmd1 = i_cCmd1+Left(i_cCmd3, At(' from ',Lower(i_cCmd3))+5)
			            i_cCmd3 = Substr(i_cCmd3,At(' from ',Lower(i_cCmd3))+6)
			            i_p2 = GetPosCommaSpace(i_cCmd3)
			            If i_p2<>0
			            	* --- e' seguita dall' alias, si sposta fino a dopo l' alias
			            	i_cCmd1 = i_cCmd1+Left(i_cCmd3,i_p2)
			                i_cCmd3 = Substr(i_cCmd3,i_p2+1)
			                i_p2 = Evl(GetPosCommaSpace(i_cCmd3),1)
			                i_w = Lower(Left(i_cCmd3,i_p2-1))
			                If Inlist(i_w,'right','left','inner','full','where')
			                    i_p2 = 1
			                EndIf
			            Else
			            	i_p2 = 1
			            EndIf
                    	i_cCmd1 = i_cCmd1+Left(i_cCmd3,i_p2-1)+ IIF (LOWER(RIGHT(ALLTRIM(i_cCmd1),6))='select',' ',' with (nolock) ')+Ltrim(Substr(i_cCmd3,i_p2,Len(i_cCmd3)))
                    Endif
                    i_cCmd2 = Substr(i_cCmd2,i_p)
				EndIf
                *-- Valuto i casi Union e Join
                i_pUnion = Evl(At(' union ',Lower(i_cCmd2)),Len(i_cCmd2))
            	i_pJoin = Evl(At(' join ',Lower(i_cCmd2)),Len(i_cCmd2))
            	i_p = Min( i_pJoin, i_pUnion)
            	i_p = Iif( i_p = Len(i_cCmd2), 0, i_p)
            	i_cIter = Iif(i_p = At(' join ',Lower(i_cCmd2)), ' join ', ' union ')
            Enddo
        Endif
    Else
        i_cCmd1=cCmd
        i_cCmd2=''
    Endif
    *activate screen
    *? i_cCmd1+i_cCmd2
    Return(i_cCmd1+i_cCmd2)

Function GetPosCommaSpace(i_cCmd)
    Local i_p1,i_p2
    i_p1=At(' ',i_cCmd)
    i_p2=At(',',i_cCmd)
    If i_p1<>0 And i_p2<>0
        Return Min(i_p1,i_p2)
    Else
        If i_p1<>0
            Return(i_p1)
        Endif
    Endif
    Return(i_p2)

