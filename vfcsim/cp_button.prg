* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: CP_BUTTON
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : 
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Libreria per le query
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

* -------------------------------------------------------------------------------
* Classi per le nuovi bottoni
* -------------------------------------------------------------------------------
#Define TOOLBAR_BTN_TEXT 59

Define Class StdButton As ButtonMenu
	FontSize   = 9
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	FontName   = ""
	* --
	cfgCond=''
	cfgHide=''
	bSetFont = .T.
	bGlobalFont=.F.	
	#If Version(5)>=800
		SpecialEffect = i_nBtnSpEfc
	#Endif

	Procedure Init()
		If Empty(This.FontName)
			This.FontName   = This.Parent.FontName
			This.FontSize   = This.Parent.FontSize
			This.FontBold   = This.Parent.FontBold
			This.FontItalic = This.Parent.FontItalic
			This.FontUnderline  = This.Parent.FontUnderline
			This.FontStrikethru = This.Parent.FontStrikethru
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
		If Vartype(i_ThemesManager)=='O'
			this.LblButton.Forecolor=i_ThemesManager.getProp(TOOLBAR_BTN_TEXT)
		Else
			this.LblButton.Forecolor=0
		Endif
		This.Caption=cp_Translate(This.Caption)
		This.ToolTipText=cp_Translate(This.ToolTipText)
		DoDefault()
		Return

	Procedure SetFont()
		If This.bGlobalFont
			SetFont('B', This)
		Endif
	Endproc

	Func When()
		Local i_bRes,i_e
		* --- Procedure standard del campo
		i_bRes=.T.
		If Type('this.parent.oContained.cFunction')='O' && bisogna controllare: un bottone che implementa la
			&& funzione di sistema "Quit" puo' non trovare piu' il contenitore
			If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
				i_bRes = This.mCond()
				If !Empty(This.cfgCond)
					i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
					i_bRes=&i_e
				Endif
			Endif
		Endif
		Return(i_bRes)
	Func mCond()
		Return(.T.)
		* --- Procedura di Hide standard del campo
	Func mHide()
		Return (.F.)
Enddefine
