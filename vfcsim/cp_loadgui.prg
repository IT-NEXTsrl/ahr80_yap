* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_loadgui                                                      *
*              cp_LoadGUI                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-25                                                      *
* Last revis.: 2010-11-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_loadgui",oParentObject,m.pOper)
return(i_retval)

define class tcp_loadgui as StdBatch
  * --- Local variables
  pOper = space(1)
  w_CODUTE = 0
  w_TEST = 0
  w_OLDVISUALTHEME = 0
  w_STABAR = space(1)
  w_DSKBAR = space(1)
  w_CPTBAR = space(1)
  w_TOOLMN = space(1)
  w_CIBCKGRD = space(1)
  w_CIMRKGRD = space(1)
  w_CILBLFNM = space(50)
  w_CITXTFNM = space(50)
  w_CICBXFNM = space(50)
  w_CIBTNFNM = space(50)
  w_CIGRDFNM = space(50)
  w_CIPAGFNM = space(50)
  w_CITBSIZE = 0
  w_CISEARMN = space(1)
  w_CIMENFIX = space(1)
  w_CIMENIMM = space(1)
  w_WAITWND = space(1)
  w_CILBLFIT = space(1)
  w_CITXTFIT = space(1)
  w_CICBXFIT = space(1)
  w_CIBTNFIT = space(1)
  w_CIGRDFIT = space(1)
  w_CIPAGFIT = space(1)
  w_CILBLFBO = space(1)
  w_CITXTFBO = space(1)
  w_CICBXFBO = space(1)
  w_CIBTNFBO = space(1)
  w_CIGRDFBO = space(1)
  w_CIPAGFBO = space(1)
  * --- WorkFile variables
  cpsetgui_idx=0
  cpazi_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Load GUI
    *     pOper:
    *     'S' -> Start
    *     'U' -> User
    * --- Old value
    this.w_OLDVISUALTHEME = i_VisualTheme
    * --- Variabili di appoggio per valorizzazione variabili pubbliche
    * --- Inizio caricamento cfg
    this.w_TEST = -2
    if this.pOper = "S"
      * --- Start
      *     Da cp3start, utente = -1
      this.w_CODUTE = -1
    else
      * --- User
      *     Da cp_login, utente = i_codute
      this.w_CODUTE = i_CODUTE
      * --- Read from cpsetgui
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.cpsetgui_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.cpsetgui_idx,2],.t.,this.cpsetgui_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "usrcode"+;
          " from "+i_cTable+" cpsetgui where ";
              +"usrcode = "+cp_ToStrODBC(this.w_CODUTE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          usrcode;
          from (i_cTable) where;
              usrcode = this.w_CODUTE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TEST = NVL(cp_ToDate(_read_.usrcode),cp_NullValue(_read_.usrcode))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_CODUTE <> this.w_TEST
        this.w_CODUTE = -1
      endif
    endif
    * --- Controllo in caso di installazione se ho una configurazione salvata
    if this.w_CODUTE = -1
      * --- Read from cpsetgui
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.cpsetgui_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.cpsetgui_idx,2],.t.,this.cpsetgui_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "usrcode"+;
          " from "+i_cTable+" cpsetgui where ";
              +"usrcode = "+cp_ToStrODBC(this.w_CODUTE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          usrcode;
          from (i_cTable) where;
              usrcode = this.w_CODUTE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TEST = NVL(cp_ToDate(_read_.usrcode),cp_NullValue(_read_.usrcode))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se non trovo una confgurazione uso quella di default
      if this.w_TEST <> -1
        * --- Scrivo cfg di default per installazione (-1)
        * --- Insert into cpsetgui
        i_nConn=i_TableProp[this.cpsetgui_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpsetgui_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cpsetgui_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"usrcode"+",cigridcl"+",ciselecl"+",ciedtcol"+",cidsblfc"+",cidsblbc"+",cistabar"+",cixpthem"+",ciscrcol"+",cicptbar"+",cidskbar"+",cimdifrm"+",citoolmn"+",cidtlclr"+",civtheme"+",citabmen"+",cibckgrd"+",cimrkgrd"+",cidskmen"+",cilblfnm"+",citxtfnm"+",cicbxfnm"+",cibtnfnm"+",cigrdfnm"+",cipagfnm"+",cilblfsz"+",citxtfsz"+",cicbxfsz"+",cibtnfsz"+",cigrdfsz"+",cipagfsz"+",cimnafnm"+",cimnafsz"+",ciwmafnm"+",ciwmafsz"+",cilblfbo"+",citxtfbo"+",cicbxfbo"+",cibtnfbo"+",cigrdfbo"+",cipagfbo"+",cilblfit"+",citxtfit"+",cicbxfit"+",cibtnfit"+",cigrdfit"+",cipagfit"+",cinavnub"+",cinavsta"+",cinavdim"+",citbsize"+",cisearmn"+",cimenfix"+",cishwbtn"+",cioblcol"+",ciobl_on"+",cicolzom"+",cievizom"+",cirepbeh"+",ciwaitwd"+",cimenimm"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(-1),'cpsetgui','usrcode');
          +","+cp_NullLink(cp_ToStrODBC(RGB(208, 215, 229)),'cpsetgui','cigridcl');
          +","+cp_NullLink(cp_ToStrODBC(RGB(255,231,162)),'cpsetgui','ciselecl');
          +","+cp_NullLink(cp_ToStrODBC(RGB(246,246,246)),'cpsetgui','ciedtcol');
          +","+cp_NullLink(cp_ToStrODBC(RGB(0,41,91)),'cpsetgui','cidsblfc');
          +","+cp_NullLink(cp_ToStrODBC(RGB(233,238,238)),'cpsetgui','cidsblbc');
          +","+cp_NullLink(cp_ToStrODBC(" "),'cpsetgui','cistabar');
          +","+cp_NullLink(cp_ToStrODBC(1),'cpsetgui','cixpthem');
          +","+cp_NullLink(cp_ToStrODBC(RGB(255,255,255)),'cpsetgui','ciscrcol');
          +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cicptbar');
          +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cidskbar');
          +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cimdifrm');
          +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','citoolmn');
          +","+cp_NullLink(cp_ToStrODBC(RGB(255,231,162)),'cpsetgui','cidtlclr');
          +","+cp_NullLink(cp_ToStrODBC(-1),'cpsetgui','civtheme');
          +","+cp_NullLink(cp_ToStrODBC("T"),'cpsetgui','citabmen');
          +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cibckgrd');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cimrkgrd');
          +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cidskmen');
          +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cilblfnm');
          +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','citxtfnm');
          +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cicbxfnm');
          +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cibtnfnm');
          +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cigrdfnm');
          +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cipagfnm');
          +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cilblfsz');
          +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','citxtfsz');
          +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cicbxfsz');
          +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cibtnfsz');
          +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cigrdfsz');
          +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cipagfsz');
          +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cimnafnm');
          +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cimnafsz');
          +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','ciwmafnm');
          +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','ciwmafsz');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cilblfbo');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','citxtfbo');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cicbxfbo');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cibtnfbo');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cigrdfbo');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cipagfbo');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cilblfit');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','citxtfit');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cicbxfit');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cibtnfit');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cigrdfit');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cipagfit');
          +","+cp_NullLink(cp_ToStrODBC(7),'cpsetgui','cinavnub');
          +","+cp_NullLink(cp_ToStrODBC("A"),'cpsetgui','cinavsta');
          +","+cp_NullLink(cp_ToStrODBC(179),'cpsetgui','cinavdim');
          +","+cp_NullLink(cp_ToStrODBC(24),'cpsetgui','citbsize');
          +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cisearmn');
          +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cimenfix');
          +","+cp_NullLink(cp_ToStrODBC(" "),'cpsetgui','cishwbtn');
          +","+cp_NullLink(cp_ToStrODBC(0),'cpsetgui','cioblcol');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','ciobl_on');
          +","+cp_NullLink(cp_ToStrODBC(RGB(255,231,162)),'cpsetgui','cicolzom');
          +","+cp_NullLink(cp_ToStrODBC(0),'cpsetgui','cievizom');
          +","+cp_NullLink(cp_ToStrODBC("8"),'cpsetgui','cirepbeh');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','ciwaitwd');
          +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cimenimm');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'usrcode',-1,'cigridcl',RGB(208, 215, 229),'ciselecl',RGB(255,231,162),'ciedtcol',RGB(246,246,246),'cidsblfc',RGB(0,41,91),'cidsblbc',RGB(233,238,238),'cistabar'," ",'cixpthem',1,'ciscrcol',RGB(255,255,255),'cicptbar',"S",'cidskbar',"S",'cimdifrm',"S")
          insert into (i_cTable) (usrcode,cigridcl,ciselecl,ciedtcol,cidsblfc,cidsblbc,cistabar,cixpthem,ciscrcol,cicptbar,cidskbar,cimdifrm,citoolmn,cidtlclr,civtheme,citabmen,cibckgrd,cimrkgrd,cidskmen,cilblfnm,citxtfnm,cicbxfnm,cibtnfnm,cigrdfnm,cipagfnm,cilblfsz,citxtfsz,cicbxfsz,cibtnfsz,cigrdfsz,cipagfsz,cimnafnm,cimnafsz,ciwmafnm,ciwmafsz,cilblfbo,citxtfbo,cicbxfbo,cibtnfbo,cigrdfbo,cipagfbo,cilblfit,citxtfit,cicbxfit,cibtnfit,cigrdfit,cipagfit,cinavnub,cinavsta,cinavdim,citbsize,cisearmn,cimenfix,cishwbtn,cioblcol,ciobl_on,cicolzom,cievizom,cirepbeh,ciwaitwd,cimenimm &i_ccchkf. );
             values (;
               -1;
               ,RGB(208, 215, 229);
               ,RGB(255,231,162);
               ,RGB(246,246,246);
               ,RGB(0,41,91);
               ,RGB(233,238,238);
               ," ";
               ,1;
               ,RGB(255,255,255);
               ,"S";
               ,"S";
               ,"S";
               ,"S";
               ,RGB(255,231,162);
               ,-1;
               ,"T";
               ,"S";
               ,"N";
               ,"S";
               ,i_cProjectFontName;
               ,i_cProjectFontName;
               ,i_cProjectFontName;
               ,i_cProjectFontName;
               ,i_cProjectFontName;
               ,i_cProjectFontName;
               ,i_nProjectFontSize;
               ,i_nProjectFontSize;
               ,i_nProjectFontSize;
               ,i_nProjectFontSize;
               ,i_nProjectFontSize;
               ,i_nProjectFontSize;
               ,i_cProjectFontName;
               ,i_nProjectFontSize;
               ,i_cProjectFontName;
               ,i_nProjectFontSize;
               ,"N";
               ,"N";
               ,"N";
               ,"N";
               ,"N";
               ,"N";
               ,"N";
               ,"N";
               ,"N";
               ,"N";
               ,"N";
               ,"N";
               ,7;
               ,"A";
               ,179;
               ,24;
               ,"S";
               ,"S";
               ," ";
               ,0;
               ,"N";
               ,RGB(255,231,162);
               ,0;
               ,"8";
               ,"N";
               ,"N";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    * --- Leggo la cfg Installazione/Utente
    * --- Read from cpsetgui
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.cpsetgui_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cpsetgui_idx,2],.t.,this.cpsetgui_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" cpsetgui where ";
            +"usrcode = "+cp_ToStrODBC(this.w_CODUTE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            usrcode = this.w_CODUTE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      i_nGridLineColor = NVL(cp_ToDate(_read_.cigridcl),cp_NullValue(_read_.cigridcl))
      i_nBackColor = NVL(cp_ToDate(_read_.ciselecl),cp_NullValue(_read_.ciselecl))
      i_nEBackColor = NVL(cp_ToDate(_read_.ciedtcol),cp_NullValue(_read_.ciedtcol))
      i_uDisabledForecolor = NVL(cp_ToDate(_read_.cidsblfc),cp_NullValue(_read_.cidsblfc))
      i_uDisabledBackcolor = NVL(cp_ToDate(_read_.cidsblbc),cp_NullValue(_read_.cidsblbc))
      this.w_STABAR = NVL(cp_ToDate(_read_.cistabar),cp_NullValue(_read_.cistabar))
      i_nXPTheme = NVL(cp_ToDate(_read_.cixpthem),cp_NullValue(_read_.cixpthem))
      i_nScreenColor = NVL(cp_ToDate(_read_.ciscrcol),cp_NullValue(_read_.ciscrcol))
      this.w_CPTBAR = NVL(cp_ToDate(_read_.cicptbar),cp_NullValue(_read_.cicptbar))
      this.w_DSKBAR = NVL(cp_ToDate(_read_.cidskbar),cp_NullValue(_read_.cidskbar))
      i_cViewMode = NVL(cp_ToDate(_read_.cimdifrm),cp_NullValue(_read_.cimdifrm))
      this.w_TOOLMN = NVL(cp_ToDate(_read_.citoolmn),cp_NullValue(_read_.citoolmn))
      i_nDtlRowClr = NVL(cp_ToDate(_read_.cidtlclr),cp_NullValue(_read_.cidtlclr))
      i_VisualTheme = NVL(cp_ToDate(_read_.civtheme),cp_NullValue(_read_.civtheme))
      i_cMenuTab = NVL(cp_ToDate(_read_.citabmen),cp_NullValue(_read_.citabmen))
      this.w_CIBCKGRD = NVL(cp_ToDate(_read_.cibckgrd),cp_NullValue(_read_.cibckgrd))
      this.w_CIMRKGRD = NVL(cp_ToDate(_read_.cimrkgrd),cp_NullValue(_read_.cimrkgrd))
      i_cDeskMenu = NVL(cp_ToDate(_read_.cidskmen),cp_NullValue(_read_.cidskmen))
      this.w_CILBLFNM = NVL(cp_ToDate(_read_.cilblfnm),cp_NullValue(_read_.cilblfnm))
      this.w_CITXTFNM = NVL(cp_ToDate(_read_.citxtfnm),cp_NullValue(_read_.citxtfnm))
      this.w_CICBXFNM = NVL(cp_ToDate(_read_.cicbxfnm),cp_NullValue(_read_.cicbxfnm))
      this.w_CIBTNFNM = NVL(cp_ToDate(_read_.cibtnfnm),cp_NullValue(_read_.cibtnfnm))
      this.w_CIGRDFNM = NVL(cp_ToDate(_read_.cigrdfnm),cp_NullValue(_read_.cigrdfnm))
      this.w_CIPAGFNM = NVL(cp_ToDate(_read_.cipagfnm),cp_NullValue(_read_.cipagfnm))
      i_nLblFontSize = NVL(cp_ToDate(_read_.cilblfsz),cp_NullValue(_read_.cilblfsz))
      i_nFontSize = NVL(cp_ToDate(_read_.citxtfsz),cp_NullValue(_read_.citxtfsz))
      i_nCboxFontSize = NVL(cp_ToDate(_read_.cicbxfsz),cp_NullValue(_read_.cicbxfsz))
      i_nBtnFontSize = NVL(cp_ToDate(_read_.cibtnfsz),cp_NullValue(_read_.cibtnfsz))
      i_nGrdFontSize = NVL(cp_ToDate(_read_.cigrdfsz),cp_NullValue(_read_.cigrdfsz))
      i_nPageFontSize = NVL(cp_ToDate(_read_.cipagfsz),cp_NullValue(_read_.cipagfsz))
      i_cMenuNavFontName = NVL(cp_ToDate(_read_.cimnafnm),cp_NullValue(_read_.cimnafnm))
      i_nMenuNavFontSize = NVL(cp_ToDate(_read_.ciwmafsz),cp_NullValue(_read_.ciwmafsz))
      i_cWindowManagerFontName = NVL(cp_ToDate(_read_.ciwmafnm),cp_NullValue(_read_.ciwmafnm))
      i_cWindowManagerFontSize = NVL(cp_ToDate(_read_.ciwmafsz),cp_NullValue(_read_.ciwmafsz))
      i_nDeskMenuMaxButton = NVL(cp_ToDate(_read_.cinavnub),cp_NullValue(_read_.cinavnub))
      i_cDeskMenuStatus = NVL(cp_ToDate(_read_.cinavsta),cp_NullValue(_read_.cinavsta))
      i_nDeskMenuInitDim = NVL(cp_ToDate(_read_.cinavdim),cp_NullValue(_read_.cinavdim))
      this.w_CITBSIZE = NVL(cp_ToDate(_read_.citbsize),cp_NullValue(_read_.citbsize))
      this.w_CISEARMN = NVL(cp_ToDate(_read_.cisearmn),cp_NullValue(_read_.cisearmn))
      this.w_CIMENFIX = NVL(cp_ToDate(_read_.cimenfix),cp_NullValue(_read_.cimenfix))
      i_cZBtnShw = NVL(cp_ToDate(_read_.cishwbtn),cp_NullValue(_read_.cishwbtn))
      i_nOblColor = NVL(cp_ToDate(_read_.cioblcol),cp_NullValue(_read_.cioblcol))
      i_cHlOblColor = NVL(cp_ToDate(_read_.ciobl_on),cp_NullValue(_read_.ciobl_on))
      i_nEviRigaZoom = NVL(cp_ToDate(_read_.cievizom),cp_NullValue(_read_.cievizom))
      i_nZoomColor = NVL(cp_ToDate(_read_.cicolzom),cp_NullValue(_read_.cicolzom))
      i_cRepBehavior = NVL(cp_ToDate(_read_.cirepbeh),cp_NullValue(_read_.cirepbeh))
      this.w_WAITWND = NVL(cp_ToDate(_read_.ciwaitwd),cp_NullValue(_read_.ciwaitwd))
      this.w_CILBLFIT = NVL(cp_ToDate(_read_.cilblfit),cp_NullValue(_read_.cilblfit))
      this.w_CITXTFIT = NVL(cp_ToDate(_read_.citxtfit),cp_NullValue(_read_.citxtfit))
      this.w_CICBXFIT = NVL(cp_ToDate(_read_.cicbxfit),cp_NullValue(_read_.cicbxfit))
      this.w_CIBTNFIT = NVL(cp_ToDate(_read_.cibtnfit),cp_NullValue(_read_.cibtnfit))
      this.w_CIGRDFIT = NVL(cp_ToDate(_read_.cigrdfit),cp_NullValue(_read_.cigrdfit))
      this.w_CIPAGFIT = NVL(cp_ToDate(_read_.cipagfit),cp_NullValue(_read_.cipagfit))
      this.w_CILBLFBO = NVL(cp_ToDate(_read_.cilblfbo),cp_NullValue(_read_.cilblfbo))
      this.w_CITXTFBO = NVL(cp_ToDate(_read_.citxtfbo),cp_NullValue(_read_.citxtfbo))
      this.w_CICBXFBO = NVL(cp_ToDate(_read_.cicbxfbo),cp_NullValue(_read_.cicbxfbo))
      this.w_CIBTNFBO = NVL(cp_ToDate(_read_.cibtnfbo),cp_NullValue(_read_.cibtnfbo))
      this.w_CIGRDFBO = NVL(cp_ToDate(_read_.cigrdfbo),cp_NullValue(_read_.cigrdfbo))
      this.w_CIPAGFBO = NVL(cp_ToDate(_read_.cipagfbo),cp_NullValue(_read_.cipagfbo))
      this.w_CIMENIMM = NVL(cp_ToDate(_read_.cimenimm),cp_NullValue(_read_.cimenimm))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Applico la cfg
    * --- Se fox inferiore a 9.0 disabilito nuova interfaccia
    # if Version(5)<900 
 i_VisualTheme = -1 && -1 Vista Classica, 5 Office 2007 Blue 
 this.w_OLDVISUALTHEME = -1 
 #endif
    * --- Setto variabili pubbliche
     
 i_bShowCPToolBar = this.w_CPTBAR = "S" 
 i_bShowDeskTopBar = this.w_DSKBAR = "S" 
 i_bShowToolMenu = this.w_TOOLMN = "S" 
 i_bGradientBck = this.w_CIBCKGRD = "S" 
 i_bRecordMark = this.w_CIMRKGRD = "S" 
 i_nTBTNW = this.w_CITBSIZE 
 i_nTBTNH = this.w_CITBSIZE 
 i_bSearchMenu = this.w_CISEARMN = "S" 
 i_bMenuFix = this.w_CIMENFIX="S" 
 g_DisableMenuImage = this.w_CIMENIMM="S" 
 i_bWaitWindowTheme = this.w_WAITWND="S" 
 
 *-- Font 
 i_cFontName = Alltrim(this.w_CITXTFNM) 
 i_cCboxFontName = Alltrim(this.w_CICBXFNM) 
 i_cBtnFontName =Alltrim(this.w_CIBTNFNM) 
 i_cPageFontName = Alltrim(this.w_CIPAGFNM) 
 i_cGrdFontName = Alltrim(this.w_CIGRDFNM) 
 i_cLblFontName = Alltrim(this.w_CILBLFNM) 
 
 *-- Font Italic 
 i_bLblFontItalic = this.w_CILBLFIT="S" 
 i_bFontItalic = this.w_CITXTFIT="S" 
 i_bCboxFontItalic = this.w_CICBXFIT="S" 
 i_bBtnFontItalic = this.w_CIBTNFIT="S" 
 i_bGrdFontItalic = this.w_CIGRDFIT="S" 
 i_bPageFontItalic = this.w_CIPAGFIT="S" 
 
 *-- Font Bold 
 i_bLblFontBold = this.w_CILBLFBO="S" 
 i_bFontBold = this.w_CITXTFBO="S" 
 i_bCboxFontBold = this.w_CICBXFBO="S" 
 i_bBtnFontBold = this.w_CIBTNFBO="S" 
 i_bGrdFontBold = this.w_CIGRDFBO="S" 
 i_bPageFontBold = this.w_CIPAGFBO="S"
    * --- Controllo cambio tema
    *     Se passo da std a nuovo o voceversa devo ricreare le toolbar e menu
    *     oldVisualTheme contiene il vecchio valore del tema
    *     -1 = std
    if i_VisualTheme <> -1
      i_ThemesManager.ThemeNumber = i_VisualTheme
    endif
    * --- Status Bar
    if this.w_STABAR="S"
       
 SET STATUS BAR ON 
 SET STATUS OFF
      if i_VisualTheme <> -1
         
 i_oStatusBar = CreateObject("cp_StatusBar") 
 i_oStatusBar.addpanel(0,100,.t.) 
 i_oStatusBar.addpanel(3) &&OVR 
 i_oStatusBar.addpanel(1) &&NUM 
 i_oStatusBar.addpanel(2) &&CAPS
      endif
    else
       
 SET STATUS BAR OFF
    endif
    if (this.w_OLDVISUALTHEME = -1 And i_VisualTheme <> - 1) Or (this.w_OLDVISUALTHEME <> -1 And i_VisualTheme = - 1)
      * --- Se cambio da nuova a standard devo sgangiare le toolbar in modo tale da non avere pi� visibile la dockarea (poich� resta colorata)
      oCpToolBar.Visible=.F.
      if VARTYPE(oDesktopBar)="O"
        oDesktopBar.Visible= .F.
      endif
      if this.w_OLDVISUALTHEME <> -1 AND VARTYPE(i_MenuToolbar) = "O"
         
 i_MenuToolbar.Visible=.F.
      endif
      * --- ToolBar sistema
       
 oCpToolBar.Destroy() 
 oCpToolBar = .null. 
 oCPToolBar=createobject("CPToolBar", i_VisualTheme<>-1) 
 oCPToolBar.Dock(0)
      * --- Application bar
      if VARTYPE(oDesktopBar)="O"
         
 oDesktopBar.Destroy() 
 oDesktopBar = .Null. 
 cp_desk()
      endif
      * --- Menu
      if this.w_OLDVISUALTHEME <> -1 AND VARTYPE(i_MenuToolbar) = "O"
         
 i_MenuToolbar.Visible=.F. 
 cb_Destroy(i_MenuToolbar.hWnd) 
 i_MenuToolbar.oPopupMenu.oParentObject=.Null. 
 i_MenuToolbar.Destroy() 
 i_MenuToolbar = .null. 
 RELEASE i_MenuToolbar
      else
        HIDE MENU _MSYSMENU
      endif
      * --- Ricarico menu
      if this.pOper = "U" And VARTYPE(i_CUR_MENU)="C" And USED(i_CUR_MENU)
        CP_MENU()
      endif
    endif
    * --- Setto screen color
    _screen.Backcolor = i_nScreenColor
    * --- Toolbar
    if VarType(oCPToolBar)="O"
       
 oCPToolBar.visible = i_bShowCPToolBar 
 oCPToolBar.Customize = i_bShowCPToolBar 
 oCPToolBar.ChangeSettings()
    endif
    * --- Application Bar
    if VarType(oDesktopBar) = "O"
       
 oDesktopBar.visible = i_bShowDeskTopBar 
 oDesktopBar.Customize = oDesktopBar.Visible 
 oDesktopBar.ChangeSettings()
    endif
    * --- Desktop Menu
    if VarType(i_oDeskmenu)="O"
       
 i_oDeskMenu.ChangeSettings()
    endif
    * --- Cambio tema XP
    SYS(2700, i_nXPTheme)
    if i_nXPTheme = 1 And i_nZBtnWidth < 14
      * --- Se tema Xp Attivo il bottone contestuale al mimimo deve essere largo 14 pixel 
      *     altrimenti risulta tagliato
      *     Se cambia impostazione tra installazioen e utente,
      *      non funziona da Xp attivo a disattivo, per farlo funzionare 
      *     occorre memorizzare in un'ulteriore var. il valore prima della modifica..
       
 Public i_OldZBtnWidth 
 i_OldZBtnWidth = i_nZBtnWidth 
 i_nZBtnWidth = 14
      * --- Rimuovo la classe del bottoncino nel caso in cui le impostazioni
      *     relative al tema Xp differiscono tra Installazione e utente...
      Clear Class btnZoom
    endif
    if i_nXPTheme = 0 And i_nZBtnWidth = 14 And vartype(i_OldZBtnWidth)="N" And i_nZBtnWidth <> i_OldZBtnWidth
      i_nZBtnWidth = i_OldZBtnWidth
      Clear Class btnZoom
    endif
    if this.pOper = "U" And i_VisualTheme <> -1
      if VarType(i_MenuToolbar) = "O"
        * --- Menu in posizione fissa
         
 i_MenuToolbar.Dock(0,-1,0) 
 i_MenuToolbar.Movable = not i_bMenuFix 
 i_MenuToolbar.Gripper = not i_bMenuFix
        * --- Ricerca voci menu
        if i_bSearchMenu
          if Type("i_MenuToolbar.oSearchMenu") <> "O"
             
 i_MenuToolbar.Addobject("oSearchMenu", "SearchMenu", i_MenuToolbar.oPopupMenu.cCursorName) 
 i_MenuToolbar.oSearchMenu.Visible=.T. 
 i_MenuToolbar.Rearrange()
          endif
        else
          if Type("i_MenuToolbar.oSearchMenu") = "O"
             
 i_MenuToolbar.RemoveObject("oSearchMenu") 
 i_MenuToolbar.Rearrange()
          endif
        endif
      endif
    endif
    * --- DeskMenu
    if this.pOper = "U" 
      * --- Rimovo l'oggetto
      if VarType(i_oDeskmenu)="O"
         
 _Screen.RemoveObject("NavBar") 
 Release i_oDeskmenu 
 _Screen.RemoveObject("ImgBackground") 
 _Screen.RemoveObject("TBMDI")
      endif
      do case
        case i_cDeskMenu = "O"
          * --- Apro e visualizzo
          cp_NavBar(.T.)
        case i_cDeskMenu = "S"
          * --- Carico ma non visualizzo
          cp_NavBar(.F.)
      endcase
    endif
    * --- Report engine behavior
    if i_cRepBehavior = "8"
       
 SET REPORTBEHAVIOR 80 
 _REPORTOUTPUT = "" 
 _REPORTPREVIEW = "" 
 _REPORTBUILDER = ""
    else
       
 SET REPORTBEHAVIOR 90 
 _REPORTOUTPUT = FULLPATH("ReportOutput.app") 
 _REPORTPREVIEW = FULLPATH("ReportPreview.app") 
 _REPORTBUILDER = FULLPATH("ReportBuilder.app")
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='cpsetgui'
    this.cWorkTables[2]='cpazi'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
