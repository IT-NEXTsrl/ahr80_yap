* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: oExpDis
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Samuele Masetto
* Data creazione: 08/03/99
* Aggiornato il : 09/09/99
* Translated:     03/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Oggetto per l'esplosione distinta base
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"
* --- Zucchetti Aulla, aggiunto ordinamento
Parameters cOutCursor,cInpCursor,cRifTable,cRifKey,cExpTable,cExpKey,cRepKey,cExpField,cOtherField,cOrderField
*cOutCursor=nome del cursore di output contenente i dati esplosi
*cInpCursor=nome del cursore da esplodere
*cRifTable=nome della tabella di riferimento
*cRifKey=chiavi per la tabella di riferimento
*cExpTable=nome della tabella di esplosione
*cExpKey=chiavi per la tabella di esplosione
*cRepKey=chiave ripetuta della tabella di esplosione
*cExpField=nome dei campi di esplosione della tabella di esplosione (si comportano come il campo "Quantit�" dei componenti degli articoli)
*cOtherField=nome dei campi della tabella di esplosione da includere nel cursore risultato "cOutCursor"
*cOrderField=campi per ordinamento dei figli
Private i_retval,i_Rows,o
i_retval = ''
i_Rows = 0
o=Createobject("cpExpDb")
o.cOutCursor=cOutCursor
o.cInpCursor=cInpCursor
o.cRifTable=cRifTable
o.cRifKey=cRifKey
o.cExpTable=cExpTable
o.cExpKey=cExpKey
o.cRepKey=cRepKey
o.cExpField=cExpField
o.cOtherField=Iif(Type('cOtherField')='L','',cOtherField)
*Zucchetti Inizio ordinamento figli
o.cOrderField=Iif(Type('cOrderField')='L','',cOrderField)
*Zucchetti Fine ordinamento figli
o.Explode()
Return(i_retval)

Define Class cpExpDb As Custom
    cOutCursor = ''
    nCntComp = 0
    sTmpLevel = ''
    tmpQta = 0
    sLevel = ''
    nCntLevel = 0
    nRifIdx=0
    nExpIdx=0
    cExpTable=''
    cInpCursor=''
    cExpField=''
    cRifTable=''
    cRifKey=''
    cExpKey=''
    cRepKey=''
    cOtherField=''
    *Zucchetti Inizio ordinamento figli
    cOrderField=''
    *Zucchetti Fine ordinamento figli
    * ---
    cTypeBmpFld='' && nome del campo della tabella di rif. che funge da chiave per la tabella dei bitmap
    cSQLBmp=''  && frase SQL (es: "SELECT <campo che contiene il file di bitmap> FROM <tabella di
    && dei bitmap> WHERE <campo chiave tabella bitmap>=") per la selezione dei bitmap parametrici
    cNoExpCond='' && condizione di filtro per non esplodere un record. Sintassi: <nome campo><op><valore> <op.logico> ...
    cNoExpCondTable='' && eventuale tabella per la condizione di filtro se i campi non sono nella
    && nella tabella di riferimento
    cOtherCond='' && Condizioni di filtro per i componenti
    nTableCondIdx = 0
    i_BASE_MIN=35
    i_BASE_MAX=128
    Proc Explode()
        Local s1
        This.nExpIdx=cp_OpenTable(This.cExpTable)
        This.nRifIdx=cp_OpenTable(This.cRifTable)
        If Not Empty(This.cNoExpCond) And Not Empty(This.cNoExpCondTable)
            This.nTableCondIdx=cp_OpenTable(This.cNoExpCondTable)
        Endif
        If Used(This.cInpCursor)
            This.ExplodeDB()
            Select (This.cOutCursor)
        Else
            cp_ErrorMsg(cp_MsgFormat(MSG_CURSOR__IS_NOT_OPENED_F,This.cInpCursor),'','',.F.)
        Endif
        This.CloseOpenTables()
        cp_CloseTable(This.cExpTable)
        cp_CloseTable(This.cRifTable)
        Return
    Procedure ExplodeDB()
        Local i_nConn,i_cTable,cFields,i_a,cSearch,cValue,i,cExpField,nCurrRec,nNodeIdx,nTmp,nRes
        Local i_bInsert,nCurrentPos,i_cOutFields,bHasComp
        i_nConn=i_TableProp[this.nRifIdx,3]
        *i_cTable=cp_SetAzi(i_TableProp[this.nRifIdx,2])
        nNodeIdx=1
        Dimension nCurrentPos[1]
        Dimension i_cOutFields[1]
        *---Creazione della tabella temporanea e dell'array dei campi della tabella temporanea
        *select * from  (i_cTable) where 1=2 into cursor __tmp__
        This.OpenCursorFields("__tmp__")
        Select __tmp__
        This.CreateTmpTable(This.cOutCursor,1,This.cRifKey)
        Select (This.cOutCursor)
        Afields(i_cOutFields)
        *---Crea un array con la disposizione delle chiavi
        Select (This.cInpCursor)
        Scan &&sul cursore
            This.sLevel = This.GetLevel(nNodeIdx)
            This.sTmpLevel = This.sLevel
            Dimension i_a[1,2]
            SelectFromTable(@i_a,This.nRifIdx,This.cRifKey,This.cRifKey,"_oDis_")
            *---Cerca se questo record � gi� stato caricato
            Select (This.cInpCursor)
            cSearch=''
            For i=1 To Alen(i_a,1)
                *cSearch=cSearch+cp_ConvertToStr(&i_a[i,1])
                cSearch=cSearch+cp_ToStr(&i_a[i,1])
            Next
            Select (This.cOutCursor)
            nCurrRec=0
            If Not Eof()
                nCurrRec=Recno()
            Endif
            Set Order To Idx_1
            Seek (cSearch)
            If nCurrRec<>0
                Go nCurrRec
            Endif
            Set Order To
            If Not Found()
                nNodeIdx=nNodeIdx+1
                If Used('_oDis_')
                    Select _oDis_
                    Locate For 1=1
                    Do While Not(Eof())
                        This.AppendField(This.cOutCursor,1,@i_cOutFields) &&Scrive la riga del cursore nella tabella di esplosione
                        Select _oDis_
                        Continue
                    Enddo
                    Use
                Endif
                i_nConn=i_TableProp[this.nExpIdx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.nExpIdx,2])
                Select (This.cOutCursor)
                If Not This.CheckType(i_nConn,i_cTable)
                    Return
                Endif
                Select (This.cOutCursor)
                nCurrentPos[1]=0
                Scan For "!"+This.GetLevel(nNodeIdx-1)$"!"+lvlkey &&sulla tabella creata con i dati esplosi
                    *---Conta il livello
                    i=Len(lvlkey)-Len(Strtran(lvlkey,Iif(g_APPLICATION <> "ADHOC REVOLUTION",Chr(46),Chr(255)),''))+1
                    If Alen(nCurrentPos)<i
                        Dimension nCurrentPos[i]
                        nCurrentPos[i]=Recno()
                    Else
                        nCurrentPos[i]=Max(Recno(),nCurrentPos[i])
                    Endif
                    This.nCntLevel = 0
                    This.sLevel = Alltrim(lvlkey+Iif(This.nCntLevel<>0,Iif(g_APPLICATION <> "ADHOC REVOLUTION",Chr(46),Chr(255))+This.GetLevel(This.nCntLevel),''))
                    This.nCntComp = 0
                    This.tmpQta = qtacomp
                    bHasComp=.T.
                    If Not Empty(This.cNoExpCond)
                        bHasComp=This.HasComponents()
                    Endif
                    If bHasComp
                        Select (This.cOutCursor)
                        * --- Zucchetti Aulla, aggiunto ordinamento
                        SelectFromTable(@i_a,This.nExpIdx,This.cRifKey,This.cExpKey,"_dis_",This.cOtherCond,.F.,This.cOrderField)
                        If Used('_dis_')
                            Select _dis_  && cursore dei componenti per ogni record
                            Locate For 1=1
                            Do While Not(Eof())
                                cExpField=This.cExpField
                                SelectFromTable(@i_a,This.nRifIdx,This.cRepKey,This.cRifKey,"_Rif_")
                                If Used('_Rif_')
                                    Select _Rif_
                                    Locate For 1=1
                                    nRes=_Tally
                                    Do While Not(Eof())
                                        If Empty(cExpField)
                                            nTmp=0
                                        Else
                                            nTmp=_dis_.&cExpField*This.tmpQta
                                        Endif
                                        This.AppendField(This.cOutCursor,nTmp,@i_cOutFields,"_dis_")
                                        This.nCntLevel = This.nCntLevel+1
                                        This.sTmpLevel = This.sLevel+Iif(This.nCntLevel<>0,Iif(g_APPLICATION <> "ADHOC REVOLUTION",Chr(46),Chr(255))+This.GetLevel(This.nCntLevel),'')
                                        i_nConn=i_TableProp[this.nExpIdx,3]
                                        i_cTable=cp_SetAzi(i_TableProp[this.nExpIdx,2])
                                        Select (This.cOutCursor)
                                        If Not This.CheckType(i_nConn,i_cTable)
                                            Return
                                        Endif
                                        Select _Rif_
                                        Continue
                                    Enddo
                                    Use
                                Endif
                                If nRes<>0
                                    This.nCntComp = This.nCntComp+1
                                Endif
                                Select _dis_
                                Continue
                            Enddo
                            Use
                        Endif
                    Endif
                    Select (This.cOutCursor)
                    If This.nCntComp<>0
                        Go nCurrentPos[i]
                    Endif
                Endscan
            Endif
            Select (This.cInpCursor)
        Endscan
        Return
    Proc OpenCursorFields(i_cCursor)
        Local i_a,i,i_cFields,i_cTableRif,i_cTableExp,i_nConn
        Dimension i_a[1]
        i_cTableRif=cp_SetAzi(i_TableProp[this.nRifIdx,2])
        i_cTableExp=cp_SetAzi(i_TableProp[this.nExpIdx,2])
        i_nConn=i_TableProp[this.nExpIdx,3]
        Select (This.cInpCursor)
        Afields(i_a)
        i_cFields=''
        For i=1 To Fcount()
            i_cFields=i_cFields+i_cTableRif+"."+i_a[i,1]+","
        Next
        i_cFields=Left(i_cFields,Len(i_cFields)-1)
        i_cFields=i_cFields+Iif(Empty(This.cOtherField),'',',')+This.cOtherField
        If i_nConn<>0
            cp_sqlexec(i_nConn,"select "+i_cFields+" from "+i_cTableRif+","+i_cTableExp+" where 1=2",i_cCursor)
        Else
            Select &i_cFields From (i_cTableRif),(i_cTableExp) Where 1=2 Into Cursor (i_cCursor)
        Endif
        Return
    Proc FillPKFields()
        Local cExp,cRif,i
        This.cExpKey=''
        cp_ReadXdc()
        cExp=i_dcx.GetIdxDef(This.cExpTable,1)
        cRif=i_dcx.GetIdxDef(This.cRifTable,1)
        For i=1 To i_dcx.GetFieldsCount(This.cExpTable)
            If At(Upper(i_dcx.GetFieldName(This.cExpTable,i)),Upper(cExp))<>0
                This.cExpKey=This.cExpKey+i_dcx.GetFieldName(This.cExpTable,i)+","
            Endif
        Next
        For i=1 To i_dcx.GetFieldsCount(This.cRifTable)
            If At(i_dcx.GetFieldName(This.cRifTable,i),cRif)<>0
                This.cRifKey=This.RifKey+i_dcx.GetFieldName(This.cRifTable,i)+","
            Endif
        Next
        Return
    Proc CloseCursors()
        If Used('_Rif_')
            Use In _Rif_
        Endif
        If Used('_Dis_')
            Use In _dis_
        Endif
        Return
    Proc CreateTmpTable(cTableName,nQta,cKey)
        Local i_a,i_OldCursor,nLen,i,j,cIdx,i_b
        Local FIELD_TO_ADD
        FIELD_TO_ADD=4
        Dimension i_a[1]
        i_OldCursor=Alias()
        Afields(i_a)
        nLen=Alen(i_a,1)
        Dimension i_a[nLen+FIELD_TO_ADD,alen(i_a,2)]
        *---Copia la struttura dell'ultimo elemento nei nuovi campi che si devono inserire
        For i=nLen To nLen+FIELD_TO_ADD
            For j=1 To Alen(i_a,2)
                i_a[i,j]=i_a[nLen,j]
            Next
        Next
        *---Aggiunta dei nuovi campi
        i_a[nLen+1,1]="tippro"
        i_a[nLen+1,2]="C"
        i_a[nLen+1,3]="1"
        i_a[nLen+1,4]="0"
        i_a[nLen+2,1]="qtacomp"
        i_a[nLen+2,2]="N"
        i_a[nLen+2,3]="15"
        i_a[nLen+2,4]="5"
        i_a[nLen+3,1]="LvlKey"
        i_a[nLen+3,2]="C"
        i_a[nLen+3,3]="200"
        i_a[nLen+3,4]="0"
        i_a[nLen+4,1]="CPBmpName"
        i_a[nLen+4,2]="C"
        i_a[nLen+4,3]="150"
        i_a[nLen+4,4]="0"
        Create Cursor (cTableName) From Array i_a
        Dimension i_b[1,2]
        CreateKeysArray(@i_b,cKey,cKey)
        cIdx=''
        For i=1 To Alen(i_b,1)
            cIdx=cIdx+cp_ConvertToStr(i_b[i,1])+"+"
        Next
        cIdx=Left(cIdx,Len(cIdx)-1)
        Index On &cIdx Tag Idx_1 COLLATE "MACHINE"
        Index On lvlkey Tag lvlkey COLLATE "MACHINE"
        Return
    Proc AppendField(cTableName,nQta,i_cOutFields,i_cCursor)
        Local i_a,i_Fields,i_bAdd,cTmp,i_cOther,i_Value,i,j,bFind,i_nIdx,i_nCount
        Local i_aBmpName,i_cTypeBmp,i_cSQL
        i_bAdd=.F.
        If Pcount()>3
            i_bAdd=.T.
        Endif
        Dimension i_a[1]
        Dimension i_aBmpName[1]
        Dimension i_Fields[1]
        Dimension i_Value[1]
        Scatter Memo To i_a
        Afields(i_Fields)
        *--- Creazione dell'array dei valori
        i_nCount=0
        For i=1 To Alen(i_cOutFields,1)
            i_nIdx=0
            For j=1 To Alen(i_a)
                If i_cOutFields[i,1]==i_Fields[j,1]
                    i_nIdx=j
                    Exit
                Endif
            Next
            If i_nIdx>0
                i_nCount=i_nCount+1
                Dimension i_Value[i_nCount]
                i_Value[i_nCount]=i_a[j]
            Endif
        Next
        *--- Inserisce gli eventuali valori aggiuntivi
        If i_bAdd
            If Not Empty(This.cOtherField) And Used(i_cCursor)
                *---Popola l'array dei campi aggiuntivi
                cTmp=This.cOtherField+','
                i=0
                Do While At(",",cTmp)<>0
                    i=i+1
                    i_cOther=Left(cTmp,At(",",cTmp)-1)
                    Select (i_cCursor)
                    i_nCount=i_nCount+1
                    Dimension i_Value[i_nCount]
                    i_Value[i_nCount]=&i_cOther
                    cTmp=Substr(cTmp,At(",",cTmp)+1)
                Enddo
            Endif
        Endif
        Select (cTableName)
        Append From Array i_Value
        Replace qtacomp With nQta
        * ---
        If Not Empty(This.cSQLBmp)
            * --- impostazione del campo "CPBmpName" per i bitmap parametrici
            i_cTypeBmp=This.cTypeBmpFld
            i_cSQL=This.cSQLBmp+cp_ToStr(&i_cTypeBmp)+" into array i_aBmpName"
            &i_cSQL
            If Type('i_aBmpName[1]')<>'L'
                Replace CPBmpName With Alltrim(i_aBmpName[1])
            Endif
        Endif
        * ---
        Return
    Proc SegnaTab(cTableName,cType,sLevel)
        Select (cTableName)
        Replace tippro With cType
        Replace lvlkey With sLevel
        Return
    Func CheckType(i_nConn,i_cTable)
        Local cFields,i_a,cSearch,i,bHasComp
        Dimension i_a[1,2]
        bHasComp=.T.
        If Not Empty(This.cNoExpCond)
            bHasComp=This.HasComponents()
        Else
            CreateKeysArray(@i_a,This.cRifKey,This.cExpKey)
            cSearch=FillWhereFromArray(@i_a,i_nConn," and ")
            If i_nConn<>0
                cp_sqlexec(i_nConn,"select "+This.cExpKey+" from "+i_cTable+" where "+cSearch,"_read_")
                i_Rows=Iif(Used('_read_'),Reccount(),0)
            Else
                cFields=This.cExpKey
                Select &cFields From (i_cTable) Where &cSearch Into Cursor _read_
                i_Rows=_Tally
            Endif
            If Used('_read_')
                Locate For 1=1
                If i_Rows=0
                    bHasComp=.F.
                Else
                    bHasComp=.T.
                Endif
                Select _read_
                Use
            Else
                * --- errore: non e' stato trovato nessun record
                Return(.F.)
            Endif
        Endif
        If bHasComp
            * --- L'articolo ha dei componenti
            This.SegnaTab(This.cOutCursor,"F",This.sTmpLevel)
        Else
            * --- Segno come materia prima
            This.SegnaTab(This.cOutCursor,"P",This.sTmpLevel)
        Endif
        Return(.T.)
    Endfunc
    Func GetLevel(i_nLevel)
        * --- ogni livello contiene caratteri ASCII dal 35 dec. al 127 dec.
        * --- se i livello sono da due cifre ci stanno 66 livelli
        Local i_cLevel,i_nMod,i_nDiv
        i_nMod=i_nLevel%(This.i_BASE_MAX-This.i_BASE_MIN)
        i_cLevel=Chr(This.i_BASE_MIN+i_nMod)
        i_nDiv=Int(i_nLevel/(This.i_BASE_MAX-This.i_BASE_MIN))
        Do While i_nDiv<>0
            i_nMod=Int(i_nDiv)%(This.i_BASE_MAX-This.i_BASE_MIN)
            i_cLevel=Chr(This.i_BASE_MIN+i_nMod)+i_cLevel
            i_nDiv=Int(i_nDiv/(This.i_BASE_MAX-This.i_BASE_MIN))
        Enddo
        Return(i_cLevel)
    Endfunc
    Func HasComponents()
        Local i_bHas,i_cCond
        i_cCond=This.cNoExpCond
        i_bHas=.T.
        Select (This.cOutCursor)
        If Empty(This.cNoExpCondTable)
            If &i_cCond
                i_bHas=.F.
            Endif
        Else
            Local i_a
            Dimension i_a[1,2]
            SelectFromTable(@i_a,This.nTableCondIdx,This.cRifKey,This.cRifKey,"_cond_",.F.,i_cCond)
            If Used('_cond_')
                Select _cond_
                If Reccount()>0
                    i_bHas=.F.
                Endif
                Use
            Endif
        Endif
        Return(i_bHas)
    Endfunc
    Proc CloseOpenTables()
        Local i_cTable,i,i_aTables
        Dimension i_aTables[2]
        i_aTables[1]=This.nExpIdx
        i_aTables[2]=This.nRifIdx
        If This.nTableCondIdx<>0
            Dimension i_aTables[alen(i_aTables)+1]
            i_aTables[alen(i_aTables)]=This.nTableCondIdx
        Endif
        For i=1 To Alen(i_aTables)
            i_cTable=cp_SetAzi(i_TableProp[i_aTables[i],2])
            If Used(i_cTable)
                Select (i_cTable)
                Use
            Endif
        Next
        If Used('__tmp__')
            Select __tmp__
            Use
        Endif
    Endproc
Enddefine

* ---
Proc SelectFromTable(i_a,i_nTableWhereIdx,i_cFieldValue,i_cFieldWhere,i_cCursor,i_cOtherCond,i_cCond,i_cOrder)
    Local i_nConn,i_cTable,cSearch,cTmpCurs , cOrderBy
    *Zucchetti Inizio ordinamento figli
    i_cOrder=Iif( Vartype( i_cOrder )<>'C','',i_cOrder )
    cOrderBy=Iif( Not Empty( i_cOrder ) ,' Order By '+i_cOrder  ,'' )
    *Zucchetti Fine ordinamento figli
    cTmpCurs=Iif(Type('i_cOtherCond')='L' Or Empty(i_cOtherCond),i_cCursor,"_TCurs_")
    i_nConn=i_TableProp[i_nTableWhereIdx,3]
    i_cTable=cp_SetAzi(i_TableProp[i_nTableWhereIdx,2])
    CreateKeysArray(@i_a,i_cFieldValue,i_cFieldWhere)
    cSearch=FillWhereFromArray(@i_a,i_nConn," and ")
    If Vartype(i_cCond)='C'
        cSearch=cSearch+" and ("+i_cCond+")"
    Endif
    cSearch=cSearch+cOrderBy
    If i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" where "+cSearch,cTmpCurs)
    Else
        Select * From (i_cTable) Where &cSearch Into Cursor (cTmpCurs)
    Endif
    If cTmpCurs="_TCurs"
        * --- Seleziona solo legami congruenti con parametri di selezione
        Select * From (cTmpCurs) Where &i_cOtherCond Into Cursor (i_cCursor)
        Select (cTmpCurs)
        Use
        Select (i_cCursor)
    Endif
    Return
Proc CreateKeysArray(i_a,cKey1,cKey2)
    Local i,cTmp,c1,c2,N,cTmp2
    cTmp=cKey1+","
    cTmp2=Iif(Empty(cKey2),cKey1,cKey2)+","
    i=0
    Do While At(",",cTmp)<>0
        i=i+1
        Dimension i_a[i,2]
        i_a[i,1]=Left(cTmp,At(",",cTmp)-1)
        cTmp=Substr(cTmp,At(",",cTmp)+1)
        i_a[i,2]=Left(cTmp2,At(",",cTmp2)-1)
        cTmp2=Substr(cTmp2,At(",",cTmp2)+1)
    Enddo
    Return
Func FillWhereFromArray(i_a,i_nConn,cToAdd)
    Local cSearch,i
    cSearch=''
    For i=1 To Alen(i_a,1)
        If i_nConn<>0
            cSearch=cSearch+i_a[i,2]+"="+cp_ToStrODBC(&i_a[i,1])+cToAdd
        Else
            cSearch=cSearch+i_a[i,2]+"=="+cp_ToStr(&i_a[i,1])+cToAdd
        Endif
    Next
    If Not Empty(cSearch)
        cSearch=Left(cSearch,Len(cSearch)-Len(cToAdd))
    Endif
    Return(cSearch)
Func cp_ConvertToStr(i_cField)
    Local i_cConvert
    Do Case
        Case Type(i_cField)='C'
            Return(i_cField)
        Case Type(i_cField)='N'
            Return('str('+i_cField+')')
        Case Type(i_cField)='D'
            Return('dtoc('+i_cField+')')
    Endcase
    Return
