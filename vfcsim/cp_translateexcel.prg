* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_TRANSLATEEXCEL
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Zucchetti Spa (EO)
* Data creazione: 06/01/2006
* Aggiornato il :
* Translated    : 06/01/2006
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Traduzione a runtime di un file XLT
* ----------------------------------------------------------------------------
#include "cp_app_lang.inc"
Parameters p_ExcelName,p_cOrigLang,p_cDestLang,p_cSessionCode,p_nomfile
*
p_cSessionCode=Iif(Vartype(p_cSessionCode)<>'C' Or Empty(p_cSessionCode), Sys(2015), p_cSessionCode)
*
Local i_cExpr, i_cNewExcel,i_cdirExcel, i_cTmp1, i_cTmp2, o_cExpr, i_cLanguageReport, i_cOldError, i_lErr, i_nErr, i_cOldArea
i_cExpr=""
i_cNewExcel=""
i_cdirExcel=""
i_cOldArea=Alias()
i_cOldError=On('ERROR')
i_lErr=.F.
i_nErr=0
p_ExcelName = Alltrim(p_ExcelName)
* Check parametri
If Empty(p_cOrigLang) Or Empty(p_cDestLang) Or Empty(p_ExcelName)
    * Errore, restituisce nome excel vuoto !
    i_cNewExcel=""
    i_lErr = .T.
ENDIF

* Controllo preliminare sulla lingua del foglio excel
If Not(i_lErr) And p_cOrigLang=p_cDestLang
    * Il foglio � gi� nella lingua desiderata, va bene cos� !
    i_cNewExcel=p_ExcelName
    i_lErr = .T.
Endif
If Vartype(p_nomfile) <>'C'
    p_nomfile=''
Endif
* Esegue traduzione del foglio Excel
If Not(i_lErr)
    * Nuovo nome foglio Excel
    *i_cdirExcel = Alltrim(Addbs(cp_AdvancedDir(Addbs(Cp_getAppData())+Addbs(i_cAppDir),'REPORT','')))
    i_cdirExcel = TEMPADHOC()+'\'
    i_cNewExcel = i_cdirExcel+Substr(p_ExcelName, Rat('\',p_ExcelName)+1)+'_'+Alltrim(p_cDestLang)+'_'+Alltrim(p_cSessionCode)

    If Not(cp_fileexist(i_cNewExcel+'.xlt')) And (Cp_makedir ( i_cdirExcel))
        * Esegue copia del file originale
        i_cTmp1 = Iif (Not Empty(p_nomfile),p_nomfile, p_ExcelName+'.xlt')
        i_cTmp2 = i_cNewExcel+'.xlt'
        * Set errori
        On Error i_lErr=.T.
        * Copia il file originale
        Copy File "&i_cTmp1" To "&i_cTmp2"
        *** Apro il nuovo foglio Excel ed eseguo le traduzioni delle labels e delle celle di tipo stringa...
        If Not(i_lErr) And cp_fileexist(i_cNewExcel+".xlt")
            **Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
			DecoratorState ("D")
            objExcel=Createobject('excel.application')
            objExcel.workbooks.Add(i_cNewExcel+".xlt")
            oName=objExcel.activeworkbook.Name
            oNum=objExcel.workbooks(oName).Worksheets.Count
            * Ciclo su tutti gli sheets del modello
            For s=1 To oNum
                objExcel.workbooks(oName).Sheets(s).Activate
                bcount=objExcel.ActiveSheet.shapes.Count
                *** Eseguo la traduzione anche sulle caption dei bottoni.....
                Wait Window "Translating Excel [ Sheet("+Alltrim(Str(s,5,0))+") ] ..." Nowait
                * Ciclo sui bottoni...
                For b=1 To bcount
                    *** Bottone semplice
                    If objExcel.ActiveSheet.shapes(b).Type = 12
                        i_Text=objExcel.ActiveSheet.shapes(b).DrawingObject.Object.Caption
                        objExcel.ActiveSheet.shapes(b).DrawingObject.Object.Caption = cp_Translate(i_Text,p_cOrigLang,p_cDestLang)
                    Endif
                    *** Bottone che attiva la macro
                    If objExcel.ActiveSheet.shapes(b).Type = 8
                        i_Text=objExcel.ActiveSheet.shapes(b).DrawingObject.Caption
                        objExcel.ActiveSheet.shapes(b).DrawingObject.Caption = cp_Translate(i_Text,p_cOrigLang,p_cDestLang)
                    Endif
                Endfor
                *** Eseguo la traduzione delle celle di tipo carattere
                * Ciclo sullle colonne
                For i=1 To 256
                    objExcel.cells(65536,i).Select
                    * Determino l'ultima cella valorizzata
                    objExcel.Selection.End(-4162).Select
                    MaxRow = objExcel.ActiveCell.Row
                    * Ciclo sulle righe fino a MaxRow...
                    For T=1 To MaxRow
                        i_cExpr = objExcel.cells(T,i).Value
                        if type('i_cExpr')='C' AND !((i_cExpr=='BODY') OR (i_cExpr=='ENDBODY')) AND i_cExpr==objExcel.cells(t,i).formula
                            objExcel.cells(T,i).Value = cp_Translate(i_cExpr,p_cOrigLang,p_cDestLang)
                        Endif
                    Endfor
                Endfor
            Endfor
            * Chiudo e salvo il nuovo modello tradotto
            objExcel.DisplayAlerts=.F.
            objExcel.activeworkbook.SaveAs(i_cNewExcel+".xlt")
            objExcel.activeworkbook.Close
            objExcel.Quit
            Release objExcel
            **Al termine del esportazion riattiva il decorator dei form 
			DecoratorState ("A")
        Else
            * Svuota nome del foglio excel
            i_cNewExcel=""
            Do cp_ErrorMsg With cp_MsgFormat(MSG_CANNOT_COPY_REPORT,Chr(10),i_cTmp1,i_cTmp2),,48,"",.F.
        Endif
    Endif
Endif
* Check Error
On Error &i_cOldError
* Riposiziona area
If Not(Empty(i_cOldArea))
    Select &i_cOldArea
Endif
Wait Clear
* Ritorna eventuale nome del foglio Excel tradotto
Return(i_cNewExcel)
