* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: saverepmonfw                                                    *
*              Save report monitorframework                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-12                                                      *
* Last revis.: 2011-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pRepOrig,pRepTmp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tsaverepmonfw",oParentObject,m.pRepOrig,m.pRepTmp)
return(i_retval)

define class tsaverepmonfw as StdBatch
  * --- Local variables
  pRepOrig = space(0)
  pRepTmp = space(0)
  w_original_file_name = space(10)
  w_NEWFILE_NAME = space(10)
  w_edit_condition = .f.
  w_vrt_condition = .f.
  w_OK = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_original_file_name =  FORCEEXT(this.pRepOrig, "FRX")
    this.w_edit_condition = .F.
    this.w_vrt_condition = .T.
    this.w_OK = .F.
    do cp_savefor with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if !EMPTY(JUSTSTEM(this.w_original_file_name)) AND this.w_OK
      * --- Try del vfp
      if !DIRECTORY (JUSTPATH(this.w_NEWFILE_NAME))
         MKDIR (JUSTPATH(this.w_NEWFILE_NAME))
      endif
      Try
       
 COPY FILE FORCEEXT(this.pRepTmp, "FRX") TO FORCEEXT(this.w_NEWFILE_NAME, "FRX") 
 COPY FILE FORCEEXT(this.pRepTmp, "FRT") TO FORCEEXT(this.w_NEWFILE_NAME, "FRT")
      Catch
      * --- *--- Errore copia
      *     *RETURN 
      EndTry
    endif
  endproc


  proc Init(oParentObject,pRepOrig,pRepTmp)
    this.pRepOrig=pRepOrig
    this.pRepTmp=pRepTmp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pRepOrig,pRepTmp"
endproc
