* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VE_EXEC
* Ver      : 2.3.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Samuele Masetto
* Data creazione: 07/07/98
* Aggiornato il : 17/12/98
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Interprete di Excel
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam cModel,oParentObject,cFile,i_ex
* --- Zucchetti Aulla inizio  - Gestione OpenOffice
Local i_cCursor,i_cGraph,l,i_olderr,i,bFileO,bFileE,i_bOldExport
Private i_err,i_extE,i_extO
Public i_eSheet
i_extE='xlt'
* --- Zucchetti Aulla inizio - Gestione XLTX (i modelli xltx hanno la precedenza rispetto a vecchi modelli xlt)
If Vartype(i_ex)='C' 
	If Upper(i_ex)='XLT'
		If cp_IsStdFile(cModel,'XLTX')
	i_extE='xltx'
	i_ex='xltx'
		EndIf
		If Upper(i_ex)=='XLTM'
			i_extE='xltm'
		EndIf
	EndIf
EndIf
i_extO='stc'

If Type('cModel')<>'L'

    * --- Zucchetti Aulla inizio - Gestione OpenOffice
    If Lower(JustExt(cModel))$(i_extO+'-'+i_extE)
        cModel=Addbs(JustPath(cModel))+JustStem(cModel)
    Endif

    bFileE=cp_IsStdFile(cModel,i_extE)
    bFileO=cp_IsStdFile(cModel,i_extO)

    If (g_office='M' And !bFileE) Or (g_office='O' And !bFileO And !bFileE)
        cp_ErrorMsg(cp_MsgFormat(MSG_FILE__NOT_FOUND_QM,cModel),0,'',.f.)
        Return(.Null.)
    Endif
    * --- Zucchetti Aulla fine - Gestione OpenOffice
Else
    cModel=Iif(At('.vqr',Lower(cFile))=0,cFile,Left(cFile,Len(cFile)-4))
Endif
If Type('cFile')='C'
    l=cFile+Iif(At('.vqr',Lower(cFile))=0,'.vqr','')
    If cp_IsPFile(cFile+Iif(At('.vqr',Lower(cFile))=0,'.vqr',''))
        i_cCursor='__tmp__'
        If Type("i_bFox26")='L'
            cpi_qry(cFile,i_cCursor)
        Else
            vq_exec(cFile,oParentObject,i_cCursor)
        Endif
    Else
        i_cCursor=cFile
    Endif
Else
    i_cCursor='__tmp__'
Endif
* --- Zucchetti Aulla - Gestione Office/OpenOffice
* --- L'utilizzo della ve_build � condizionato dal check Automazione office all'interno della Suite Office, dalla variabile g_ExcelNoModel nel caso di assenza modello, 
* --- oppure se non � prevista l'Automazione ma non � presente il modello XLTX (fondamentale per l'esportazione tramite XML)
LOCAL i_bAutoOffice, i_cOldExp_NoModel, i_cOldExp_OldModel
i_bAutoOffice = Type("g_AUTOFFICE")="U" Or g_AUTOFFICE
i_cOldExp_NoModel = m.i_bAutoOffice And ((cp_IsStdFile(m.cModel, "XLTX") Or cp_IsStdFile(m.cModel, "XLT") Or cp_IsStdFile(m.cModel, "XLTM")) Or (Type("g_ExcelNoModel")="U" Or g_ExcelNoModel<>1))
i_cOldExp_OldModel = !m.i_bAutoOffice And !cp_IsStdFile(m.cModel, "XLTX") And (cp_IsStdFile(m.cModel, "XLT") Or cp_IsStdFile(m.cModel, "XLTM"))
i_bOldExport = m.i_cOldExp_NoModel Or m.i_cOldExp_OldModel
If g_office='M'
	If m.i_bOldExport
		*-- Verifico esistenza componente excel
		Local i_olderr, i_err, oObj
    i_eSheet=.Null.
		i_olderr=On('ERROR')
		On Error i_err=.T.
	i_eSheet=createobject('excel.application')
		On Error &i_olderr
		If m.i_err
			i_eSheet=.Null.
			cp_ErrorMsg(MSG_CANNOT_OPEN_EXCEL_WORKSHEET_QM,16,MSG_ERROR)
			return
		EndIf
	EndIf
    * --- L'inizializzazione corretta di cModel per OpenOffice � compito della vo_build...
    cModel=cp_GetStdFile(cModel,i_extE)
Endif

l=At(Sys(5)+Sys(2003),cModel)
If l=0
    If At(Sys(5),cModel)=0
        cModel=Sys(5)+Sys(2003)+'\'+cModel
    Endif
Endif
i_err=.F.
i_olderr=On('error')
On Error i_err=.T.
Select (i_cCursor)
On Error &i_olderr
If Not i_err
    * --- Zucchetti Aulla inizio - Gestione Office/OpenOffice
    Do Case
        Case g_office='M'
        	If m.i_bOldExport
            CP_MSG(cp_Translate(MSG_BUILDING_EXCEL_WORKSHEET_D))
            ve_build(i_eSheet,i_cCursor,cModel,.T.,.F.,i_extE)
		      Else
		      	Local cExpFile, bAutoDir 
		      	m.bAutoDir = Type('g_NoAutOfficeDir')='C' And !Empty(g_NoAutOfficeDir) And Directory(g_NoAutOfficeDir)
		      	m.cExpFile = Iif(m.bAutoDir, Addbs(g_NoAutOfficeDir)+JustStem(m.cModel)+Sys(2015), Putfile('Save as', Addbs(g_tempadhoc)+ForceExt(JustFname(cModel), 'xlsx'), 'xlsx'))
		      	If !Empty(m.cExpFile)
			      	vx_build('X', m.cModel, m.i_cCursor, m.cExpFile, .F., .T.)
		      	EndIf 
		      EndIf 
        Case g_office='O'
            CP_MSG(cp_Translate(MSG_BUILDING_OPENOFFICE_WORKSHEET_D))
            vo_build(i_cCursor,cModel,.F.)
        Otherwise
            cp_ErrorMsg(cp_MsgFormat(MSG_SUITE_OFFICE_NOT_INSTALLED,i_cCursor),"stop",cp_Translate(MSG_PRINT_ERROR),.F.)
    Endcase
    If Used(i_cCursor)
        Select (i_cCursor)
        Use
    Endif
    * --- Zucchetti Aulla fine
Else
    cp_ErrorMsg(cp_MsgFormat(MSG_ERROR_OPENING_CURSOR___F,i_cCursor),"stop",cp_Translate(MSG_PRINT_ERROR),.F.)
Endif
Return
