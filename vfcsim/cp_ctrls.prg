* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_STFLD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 13/03/98
* Translated    : 02/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Controlli standard
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

#Define CON_BTPOS_NONE 			1
#Define CON_BTPOS_ACTIVECTRL 	2
#Define CON_BTPOS_CARET			3
#Define CON_BTPOS_SYS1270		4
#Define CON_BTPOS_CTRLREF		5
#Define CON_BTPOS_MOUSE			6

#Define CON_STYLE_BALLOON		1
#Define CON_STYLE_RECT			2
#Define CON_STYLE_NOBORDER		3

#Define TTI_NONE				0
#Define TTI_INFO				1
#Define TTI_WARNING				2
#Define TTI_ERROR				3
#Define TTI_INFO_LARGE          4		&& VISTA
#Define TTI_WARNING_LARGE       5		&& VISTA
#Define TTI_ERROR_LARGE         6		&& VISTA

Define Class CPTextBox As TextBox
	*forecolor=rgb(96,0,0)
	*backcolor=rgb(255,242,192)
	* propriet� per contenere il nome del shape per i campi obbligatori
	CShape=''
	* --- Gestione men� contestuale (tasto destro)
	cMenuFile=''
	zbName='' && gestione bottone contestuale
	transbName='' && Nome bottoncino traduzione del dato
	bNoRightClick = .F. && disabilita tasto dx
	bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
	Century=2 &&SET CENTURY determina se l'anno � a due o quattro cifre, Cfg Interfaccia
	bNoDisabledBackColor = .F. &&Di Default non applica la configurazione interfaccia
	Procedure Init
		This.BackColor = Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.SpecialEffect = i_nSpecialEffect
		This.BorderColor = i_nBorderColor
		This.DisabledForeColor = i_udisabledforecolor
		This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
		#If Version(5)<800
			This.StatusBarText = Padr(This.ToolTipText, 100)
		#Else
			This.StatusBarText = This.ToolTipText
		#Endif
		DoDefault()
	Endproc

	* --- Al cambio della posizione (Run-Time) del control distruggo e ricreo eventuale rettangolo
	* --- per evidenziare campo obbligatorio o bottone contestuale
	Procedure Left_Assign(xvalue)
		If m.xvalue<>This.Left
			This.Left=m.xvalue
			This.Draw_Link_Obj()
		Endif
	Endproc

	Procedure Top_Assign(xvalue)
		If m.xvalue<>This.Top
			This.Top=m.xvalue
			This.Draw_Link_Obj()
		Endif
	Endproc

	Procedure Width_Assign(xvalue)
		If m.xvalue<>This.Width
			This.Width=m.xvalue
			This.Draw_Link_Obj()
		Endif
	Endproc

	Procedure Height_Assign(xvalue)
		If m.xvalue<>This.Height
			This.Height=m.xvalue
			This.Draw_Link_Obj()
		Endif
	Endproc

	Proc Draw_Link_Obj
		Local btnName
		* ---  Bottone contestuale
		Local i_btnZoom,ctShape
		i_btnZoom=.F.
		btnName='this.parent.'+This.zbName
		* --- Riposiziono bottone contestuale se presente...
		If Type(btnName)='O'
			btnName=This.zbName
			This.Parent.&btnName..Riposiziona_Btn()
			i_btnZoom=.T.
		Endif
		* --- Zucchetti Aulla fine - Bottone contestuale
		btnName='this.parent.'+This.transbName
		* --- Riposiziono bottone traduzioni...
		If Type(btnName)='O'
			btnName=This.transbName
			This.Parent.&btnName..Riposiziona_Btn(i_btnZoom)
		Endif

		* ---  Campo obbligatorio
		* --- Riposiziono shape se campo obbligatorio e se shape presente...
		If Type('this.bObbl')='L' And This.bObbl And Not Empty(This.CShape)
			* --- verifico se l'oggetto esiste...
			ctShape='this.parent.'+This.CShape
			If Type(ctShape)='O'
				ctShape=This.CShape
				This.Parent.&ctShape..Riposiziona()
			Endif
		Endif
	Endproc

	Procedure visible_assign(xvalue)
		If This.Visible <> m.xvalue
			This.Visible = m.xvalue
			* --- Campo obbligatorio
			Local CShape
			If Type('this.cShape')='C' And Not Empty(This.CShape)
				CShape = This.CShape
				* nascondo il rettangolo di campo obbligatorio
				* se campo non visible
				This.Parent.&CShape..Visible = This.Visible And This.Enabled
			Endif

			Local zbName
			zbName = This.zbName
			* --- Mostro bottone contestuale se presente...
			If Not Empty(zbName) And Type('this.bHasZoom')='L' And This.bHasZoom
				This.Parent.&zbName..Visible = m.xvalue
			Endif
			Local transbName
			transbName = This.transbName
			* --- Mostro bottone contestuale se presente...
			If Not Empty(transbName)
				This.Parent.&transbName..Visible = m.xvalue
			Endif
		Endif
	Endproc

	* --- Campo obbligatorio
	Procedure enabled_assign(xvalue)
		Local CShape
		If This.Enabled <> m.xvalue
			This.Enabled = m.xvalue
			If Type('this.cShape')='C' And Not Empty(This.CShape)
				CShape = This.CShape
				* nascondo il rettangolo di campo obbligatorio
				* se campo non editabile
				This.Parent.&CShape..Visible = This.Visible And This.Enabled
			Endif
		Endif
	Endproc

	Procedure KeyPress(nKeyCode, nShiftAltCtrl)
		Local pt
		If Vartype(This.Value)='N'
			pt=Set('point')
			Do Case
				Case nKeyCode=46 And nKeyCode<>Asc(pt)
					* Sostituzione carattere '.'
					Nodefault
					Keyboard pt
				Case nKeyCode=44 And nKeyCode<>Asc(pt)
					* Sostituzione carattere '.'
					Nodefault
					Keyboard pt
				Case nKeyCode=9
					* uscita con il tasto return, in modo da sovrascrivere sempre il contenuto
					Nodefault
					Keyboar '{ENTER}'
			Endcase
		ENDIF
		If type("This.Parent.oContained.bDontReportError")='L' 
			this.Parent.oContained.bDontReportError=.F.
		Endif
	Endproc

	Procedure RightClick
		If !This.bNoRightClick   &&  disabilito men� contestuale
			If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
				g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
				* --- Zucchetti Aulla Inizio personalizazione lancio men� contestuale
				g_oMenu.BeforeCreaMenu()
				*g_oMenu.CreaMenu()
				* --- Zucchetti Aulla Fine
				** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
				g_oMenu=.Null.
			Endif
		Endif
	Endproc

	***** Zucchetti Aulla Inizio - Controllo flussi
	Function FormFlus()
		Local res
		res=FormFlus(This.Parent)
		Return res
	Endfunc
	***** Zucchetti Aulla Fine - Controllo flussi

	*PROCEDURE MouseEnter(nButton, nShift, nXCoord, nYCoord)
	*  IF VARTYPE(thisform.__tb__)='O'
	*    thisform.__tb__.top=this.top+28
	*    thisform.__tb__.left=this.left+this.width-5
	*    thisform.__tb__.height=this.height
	*    thisform.__tb__.zorder(0)
	*    thisform.__tb__.visible=.t.
	*  ENDIF
	*  return
	*PROCEDURE MouseLeave(nButton, nShift, nXCoord, nYCoord)
	*  IF VARTYPE(thisform.__tb__)='O'
	*    thisform.__tb__.visible=.f.
	*  ENDIF
	*  return
Enddefine

Define Class CPEditBox As EditBox
	*forecolor=rgb(96,0,0)
	*backcolor=rgb(255,255,224)
	cMenuFile=''
	nSeconds=0 && Gestione frasi modello, ritardo zoom on zoom
	bNoRightClick = .F. && disabilito il tasto dx

	bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
	bNoDisabledBackColor = .F. &&Di Default non applica la configurazione interfaccia

	Procedure Init
		This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.SpecialEffect = i_nSpecialEffect
		This.BorderColor = i_nBorderColor
		This.DisabledForeColor = i_udisabledforecolor
		This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
		#If Version(5)<800
			This.StatusBarText = Padr(This.ToolTipText, 100)
		#Else
			This.StatusBarText = This.ToolTipText
		#Endif
		DoDefault()
	Endproc

	Procedure RightClick
		** Se il campo memo non � editabile allora non viene visualizzato il menu contestuale
		If This.ReadOnly=.T.
			Return
		Endif
		If Vartype(This.Parent.oContained)='O' Or Vartype( This.cMenuFile )='C'
			This.Parent.oContained.NotifyEvent(This.cFormVar+' MouseRightClick')
			If !This.bNoRightClick  && disabilito il tasto dx
				g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
				* --- Zucchetti Aulla Inizio personalizazione lancio men� contestuale
				g_oMenu.BeforeCreaMenu()
				*g_oMenu.CreaMenu()
				* --- Zucchetti Aulla Fine
				** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta dell'applicazione
				g_oMenu=.Null.
			Endif
		Endif
	Endproc

	***** Zucchetti Aulla Inizio - Controllo flussi
	Function FormFlus()
		Local res
		res=FormFlus(This.Parent)
		Return res
	Endfunc
	***** Zucchetti Aulla Fine - Controllo flussi

Enddefine
Define Class CPLabel As Label
	*forecolor=rgb(255,0,0)
	Procedure RightClick
		This.Parent.RightClick()
Enddefine
Define Class CPCommandButton As CommandButton
	*forecolor=rgb(255,0,0)
	ImgSize = i_nBtnImgSize	&&Dimenisione bmp
	NoImgAllign = 1 &&Allignamento in caso di g_NoButtonImage; 0 = Top, 1 = Bottom
	Procedure Init
		#If Version(5)>=800
			this.SpecialEffect = i_nBtnSpEfc
		#Endif
		This.StatusBarText=Padr(This.ToolTipText,100)
		If Pemstatus(This, "CpPicture", 5)
			This.Picture = This.CpPicture
		Else
			This.Picture = This.Picture
		Endif		
		DoDefault()
	Endproc

	Procedure Picture_assign
		Lparameters cNewVal
		* --- Zucchetti Aulla escludere le immagini
		Local nHeightOld
		Do case
			case Vartype(g_NoButtonImage)='L' And g_NoButtonImage And Not Empty(This.Caption)
				nHeightOld = This.Height
				If Vartype(i_ThemesManager)<>'O' Or i_ThemesManager.GetProp(103)=1
					This.Height = Max(21, This.Height - 24)
					If This.NoImgAllign = 1 and nHeightOld>40
						This.Top = This.Top + nHeightOld - This.Height
					Endif
				Endif
				This.Picture = ''
					* --- Zucchetti Aulla fine	
				Return
			case Empty(m.cNewVal)
				Return			
		Endcase
		If Vartype(g_DMIP)='C' And g_DMIP='S' And Inlist(Justext(Lower(Justfname(m.cNewVal))),"bmp","ico") And Inlist(Lower(Juststem(m.cNewVal)),"cattura","folder_edit","scanner","visualicat","zarchivia")
			m.cNewVal=Forceext(Addbs(Justpath(m.cNewVal))+Juststem(m.cNewVal)+"_infinityproject", Justext(Justfname(m.cNewVal)))
		Endif
		Local cFileBmp, l_Dim
		l_Dim= Iif(This.ImgSize<>0, This.ImgSize, i_nBtnImgSize)
		If Vartype(i_ThemesManager)=='O'
			m.cFileBmp  = i_ThemesManager.PutBmp(This,m.cNewVal, l_Dim)
		Else
			This.Picture = m.cNewVal
		Endif
	Endproc
Enddefine
Define Class CPShape As Shape
Enddefine
Define Class CPCheckBox As Checkbox
	cMenuFile=""
	bNoRightClick = .F.  && Disabilito tasto dx
	bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
	bNoDisabledBackColor = .F. &&Di Default non applica la configurazione interfaccia

	Procedure RightClick
		If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
			This.Parent.oContained.NotifyEvent(This.cFormVar+' MouseRightClick')
			If !This.bNoRightClick  &&  Disabilito tasto dx
				g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
				* --- Zucchetti Aulla Inizio personalizazione lancio men� contestuale
				g_oMenu.BeforeCreaMenu()
				*g_oMenu.CreaMenu()
				* --- Zucchetti Aulla Fine
				** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
				g_oMenu=.Null.
			Endif
		Endif
	Endproc

	***** Zucchetti Aulla Inizio - Controllo flussi
	Function FormFlus()
		Local res
		res=FormFlus(This.Parent)
		Return res
	Endfunc
	***** Zucchetti Aulla Fine - Controllo flussi

	Procedure Init
		#If Version(5)<800
			This.StatusBarText = Padr(This.ToolTipText, 100)
		#Else
			This.StatusBarText = This.ToolTipText
		#Endif
		This.SpecialEffect = i_nSpecialEffect
		DoDefault()
	Endproc
Enddefine
Define Class CPOptionGroup As OptionGroup
	cMenuFile=""
	bNoRightClick = .F.  && Disabilito tasto dx

	Procedure RightClick
		If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
			If !This.bNoRightClick  && disabilito tasto dx
				g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
				* --- Zucchetti Aulla Inizio personalizazione lancio men� contestuale
				g_oMenu.BeforeCreaMenu()
				*g_oMenu.CreaMenu()
				* --- Zucchetti Aulla Fine
				** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
				g_oMenu=.Null.
			Endif
		Endif
	Endproc

	***** Zucchetti Aulla Inizio - Controllo flussi
	Function FormFlus()
		Local res
		res=FormFlus(This.Parent)
		Return res
	Endfunc
	***** Zucchetti Aulla Fine - Controllo flussi

	Proc Init
		DoDefault()
		This.SpecialEffect = i_nSpecialEffect
		This.BorderColor = i_nBorderColor
	Endproc
Enddefine
Define Class CPComboBox As ComboBox
	*forecolor=rgb(96,0,0)
	*backcolor=rgb(255,255,224)
	*disabledforecolor=rgb(0,0,96)
	*disabledbackcolor=rgb(224,224,224)
	cMenuFile=""
	bNoRightClick = .F.  && Disabilito tasto dx

	bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
	bNoDisabledBackColor=.F. &&Di Default non applica la configurazione interfaccia

	Procedure Init
		This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.SpecialEffect = i_nSpecialEffect
		This.BorderColor = i_nBorderColor
		This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
		#If Version(5)<800
			This.StatusBarText = Padr(This.ToolTipText, 100)
		#Else
			This.StatusBarText = This.ToolTipText
		#Endif
		DoDefault()
	Endproc
	Procedure RightClick
		If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
			This.Parent.oContained.NotifyEvent(This.cFormVar+' MouseRightClick')
			If !This.bNoRightClick    && Disabilito tasto dx
				g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
				* --- Zucchetti Aulla Inizio personalizazione lancio men� contestuale
				g_oMenu.BeforeCreaMenu()
				*g_oMenu.CreaMenu()
				* --- Zucchetti Aulla Fine
				** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
				g_oMenu=.Null.
			Endif
		Endif
	Endproc
	***** Zucchetti Aulla Inizio - Controllo flussi
	Function FormFlus()
		Local res
		res=FormFlus(This.Parent)
		Return res
	Endfunc
	***** Zucchetti Aulla Fine - Controllo flussi
Enddefine

Define Class CPGrid As Grid
	* --- Gestione ottimizzata Scroll griglie e metodo Paint della form che le ospita
	PROCEDURE scrolled(nDir)
		local i_olderr
		i_olderr=on('ERROR')
		* -- La on error potrebbe essere ommessa in questo caso (lasciata per similitudine di comportamento con la Scrolled di Zoombox
		on error =.t.
		thisform._bPaint_ErrorGrid=.t.    
		on error &i_olderr 
		DODEFAULT(ndir)  
	ENDPROC	
Enddefine

Define Class cpzoomcolumn As Column
Enddefine

Define Class cpzoomHeader As Header
Enddefine

Define Class cpzoomtextbox As TextBox
Enddefine

Define Class cpcolumn As Column
	DynamicBackColor="iif(this.nAbsRow<>RECNO(),RGB(255,255,255), i_nDtlRowClr)"
Enddefine

Define Class CpPrnBtn As CommandButton

	* classe per i bottoni della Print System (cp_chprn)
	Height = 45
	Width = 48
	BackStyle=0
	#If Version(5)>=700
		SpecialEffect = i_nPrnBtnSpEfc
	#Endif
	FontName = "Arial"
	FontSize = 7
	FontBold = .F.
	FontItalic=.F.
	FontUnderline=.F.
	FontStrikethru=.F.
	*--- Zucchetti Aulla
	AlternativeDisableImage=.F.
	*--- Zucchetti Aulla

	Procedure Init
		DoDefault()
		This.Picture = This.Picture
	Endproc

	Procedure Picture_assign
		Lparameters cNewVal
		*--- Zucchetti Aulla
		Local cFileBmp, cDisabledBmp
		*--- Zucchetti Aulla
		If Vartype(i_ThemesManager)=='O'
			*--- Zucchetti Aulla
			If This.AlternativeDisableImage
				cDisabledBmp=This.DisabledPicture
			Endif
			*--- Zucchetti Aulla
			m.cFileBmp  = i_ThemesManager.PutBmp(This,m.cNewVal, 24)
			*--- Zucchetti Aulla
			If This.AlternativeDisableImage
				cFileBmp  = i_ThemesManager.RetBmpFromIco(Forceext(cDisabledBmp,'ICO'), 24)
				This.DisabledPicture=Forceext(cFileBmp,'BMP')
			Endif
			*--- Zucchetti Aulla
		Else
			This.Picture = m.cNewVal
		Endif
	Endproc

Enddefine

Define Class CPToolBtn As CommandButton

	* classe per i bottoni della toolbar
	Height=i_nTBTNH+6
	Width=i_nTBTNW+9
	BackStyle=0
	#If Version(5)>=700
		SpecialEffect = i_nTbBtnSpEfc
	#Endif
	cImage = "" &&Bitmap Pulsante
	_OnClick = ""  &&Comando da eseguire al click
	_OnRightClick = ""	&&Comando da eseguire al rightclick
	cType='' 	&& Utilizzato per disegnatore di maschere (tipo di operazione associato al bottone)
	Ndx=0 	&& per compatabilit� con le chiamate alla nuova interfaccia
	nDY=0 	&& per compatabilit� con le chiamate alla nuova interfaccia
	cName=''	&& per compatabilit� con le chiamate alla nuova interfaccia
	*--- Valorizzazione cambio immagine
	Procedure cImage_Assign(xvalue)
		Local nDimension
		With This
			.cImage = m.xvalue
			Do Case
				Case i_nTBTNH>30
					m.nDimension = 32
				Case i_nTBTNH>22
					m.nDimension = 24
				Otherwise
					m.nDimension = 16
			Endcase
			*--- Assegno la nuova bitmap
			If Vartype(i_ThemesManager)=='O'
				i_ThemesManager.PutBmp(This, .cImage ,m.nDimension)
			Else
				.Picture = .cImage
			Endif
		Endwith
	Endproc
	*--- OnClick
	Procedure Click()
		If Not Empty(This._OnClick)
			Local l_OnClick
			m.l_OnClick = This._OnClick
			&l_OnClick
		Endif
	Endproc
	*--- OnRightClick
	Procedure RightClick()
		If Not Empty(This._OnRightClick)
			Local l_OnRightClick
			m.l_OnRightClick = This._OnRightClick
			&l_OnRightClick
		Endif
	Endproc

Enddefine

* === Definizione Campo Standard
Define Class StdField As CPTextBox
	*
	* --- PROPRIETA' VFP
	*
	Height = 23
	Caption = ""
	* visible = .t.                          && ERRORE Per gli oggetti contenuti va settato da fuori
	FontSize   = 9
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	FontName   = ""
	*DisabledBackColor=rgb(255,255,255)
	Format='K'
	Margin=1
	*
	* --- PROPRIETA' CODEPAINTER
	*
	cQueryName = ""
	bObbl = .F.                            && Obbligatorio
	sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
	cLinkFile=''
	cOldIcon=''
	bHasZoom=.F.
	nPag=0
	cFormVar=''
	bIsInHeader=.T.
	nZero=0
	bUpd=.F.
	cSayPict=''
	cGetPict=''
	nSeconds=0
	bInInput=.F.
	* --
	cfgCalc=''
	cfgCond=''
	cfgCheck=''
	cfgDefault=''
	cfgHide=''
	cfgInit=''
	bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
	bNoMenuAction=.F.         	&& Disabilita nel tasto destro sottomenu Azioni
	bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
	bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
	bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
	bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
	bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
	bSetFont = .T. && Attiva / disattiva la possibilt� di pilotare i font
	bGlobalFont=.F.
	bNoZoomDateType=.F.

	*
	* --- COSTRUTTORE/DISTRUTTORE
	*
	* --- Inizializzazione
	Procedure Init()
		Local i_im
		* ---Add the button contextual
		* Aggiunta del bottoncino contestuale di fianco ai campi con linktable
		If Type('this.bHasZoom')='L' And This.bHasZoom
			If ((i_cZBtnShw='S' And (Type('this.bNoContxtBtn')='U' Or (Type('this.bNoContxtBtn')='N' And This.bNoContxtBtn=2 ))) Or	(i_cZBtnShw='N' And Type('this.bNoContxtBtn')='N' And This.bNoContxtBtn=2 ))
				Local btnName
				This.zbName = 'z'+This.Name
				* --- Gestisco l'errore in alcuni casi va in errore perch� il Parent non � ancora stato definito
				Local l_olderr, berr
				l_olderr=On('error')
				berr=.F.
				On Error berr=.T.
				This.Parent.AddObject(This.zbName,'btnZoom',This,This.Top,This.Left+Iif(i_bmobileMode, 4 , 0),This.Width,This.Height)
				On Error &l_olderr
				If Not berr
					* Per la macro non posso utilizzare una propriet�
					btnName = This.zbName
					This.Parent.&btnName..Visible = This.Visible
					This.Width=This.Width-i_nZBtnWidth + Iif(i_bmobileMode, 4 , 0)
				Endif
			Endif
		Endif
		* --- Add button translations
		If Type('this.bMultilanguage')='L' And This.bMultilanguage And Not Empty(i_aCpLangs[1])
			Local btnName
			This.transbName = 'trans'+This.Name
			* --- Gestisco l'errore in alcuni casi va in errore perch� il Parent non � ancora stato definito
			Local l_olderr, berr
			l_olderr=On('error')
			berr=.F.
			On Error berr=.T.
			This.Parent.AddObject(This.transbName,'btnTrans',This,This.Top,This.Left+Iif(i_bmobileMode, 4 , 0),This.Width,This.Height)
			On Error &l_olderr
			If Not berr
				* Per la macro non posso utilizzare una propriet�
				btnName = This.transbName
				This.Parent.&btnName..Visible = This.Visible
				This.Width=This.Width-i_nZBtnWidth+ Iif(i_bmobileMode, 4 , 0)
			Endif
		Endif
		* Add a red rectangle around the required fields
		If i_cHlOblColor='S' And Type('this.bObbl')='L' And This.bObbl
			Local l_olderr, berr
			l_olderr=On('error')
			On Error berr=.T.
			This.CShape='TTxOb'+This.Name
			This.Parent.AddObject(This.CShape,'OblShape',This.Top-1,This.Left-1,This.Width+2+Iif(i_bmobileMode, 4 , 0),This.Height+2,1,This)
			On Error &l_olderr
		Endif
		If Empty(This.FontName)
			This.FontName = This.Parent.FontName
			This.FontSize = This.Parent.FontSize
			This.FontBold = This.Parent.FontBold
			This.FontItalic = This.Parent.FontItalic
			This.FontUnderline  = This.Parent.FontUnderline
			This.FontStrikethru = This.Parent.FontStrikethru
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
		If Empty(This.cSayPict) And !Empty(This.cGetPict)
			This.cSayPict=This.cGetPict
		Endif
		If !Empty(This.cSayPict) And Empty(This.cGetPict)
			This.cGetPict=This.cSayPict
		Endif
		If !Empty(This.cSayPict)
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
		This.ToolTipText=cp_Translate(This.ToolTipText)
		DoDefault()
		Return

	Procedure SetFont()
		If This.bGlobalFont
			SetFont('T', This)
		Endif
	Endproc
	*
	* --- EVENTI/METODI VFP
	*
	* --- Quando prende il Focus
	Proc GotFocus()
		Local i_var,i_im,e
		Local i_oldvalue
		LOCAL TestMacro
		This.bUpd=.F.
		If This.Parent.oContained.cFunction="Query"
			* --- attiva il bottone di edit
			oCpToolBar.b2.Enabled=This.Parent.oContained.bLoaded
			* --- attiva i bottoni prev-next
			oCpToolBar.b7.Enabled=Used(This.Parent.oContained.cKeySet) And !Bof(This.Parent.oContained.cKeySet)
			oCpToolBar.b8.Enabled=Used(This.Parent.oContained.cKeySet) And !Eof(This.Parent.oContained.cKeySet)
		Endif
		* --- calcola il default
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			This.Parent.oContained.SaveDependsOn()
			This.ResetTotal()
			i_oldvalue=This.Value
			This.mDefault()
			oCpToolBar.b9.Enabled=This.bHasZoom Or Type('this.parent.oContained.'+This.cFormVar)='D'
			This.bUpd=i_oldvalue<>This.Value
		Endif
		* -- default da configurazione
		If Not(Empty(This.cfgDefault)) And Empty(This.Value)
			i_oldvalue=This.Value
			e=Strtran(This.cfgDefault,'w_','this.parent.oContained.w_')
			This.Value=&e
			This.bUpd=This.bUpd Or This.Value<>i_oldvalue
		Endif
		* --- esegue la funzione "before input"
		If This.Parent.oContained.cFunction<>"Filter"
			This.mBefore()
			* --- il valore puo' essere cambiato dalla funzione di "Before"
			i_var=This.cFormVar
			TestMacro=This.Parent.oContained.&i_var
			If This.Value<>TestMacro And This.IsUpdated()
				This.SetControlValue()
				This.bUpd=.T.
			Endif
		Endif
		This.bInInput=.T.
		* --- setta la picture di "get"
		If !Empty(This.cGetPict)
			i_im=Trim(cp_GetMask(This.cGetPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cGetPict)
		Endif
		If !This.bNoBackColor
			This.BackColor= i_nBackColor
		Endif
		If Inlist(This.Parent.oContained.cFunction,"Load","Edit")
			If Vartype(i_var)='C'
				This.Parent.oContained.NotifyEvent(i_var+' GotFocus')
			Endif
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Inizio
		If Type('This.parent.aPageMovers')='O'
			Do While This.Top+This.Parent.Top<0 And This.Parent.aPageMovers.Count>0
				If This.Parent.aPageMovers.Count>0
					* Se non sono arrivato all'inizio della collezione sposto
					* il pageframe alla posizione precedente e elimino l'informazioni dalla collezione
					This.Parent.Top = This.Parent.aPageMovers[This.parent.aPageMovers.Count]
					This.Parent.aPageMovers.Remove(This.Parent.aPageMovers.Count)
				Endif
			Enddo
			This.Parent.Refresh()
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Fine
		Return
	Func When()
		Local i_bRes,i_e
		* --- Procedure standard del campo
		i_bRes=.T.
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			i_bRes = This.mCond()
			If !Empty(This.cfgCond)
				i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
				i_bRes=&i_e
			Endif
		Endif
		Return(i_bRes)
	Func Valid()
		Local i_nRes,i_bErr,i_var,i_bUpd,e,l,i_oldform
		*--- Zucchetti Aulla Inizio - Evito problema campi obbligatori e bottoni contestuali
		LOCAL obj, zbName, btnZoom
		LOCAL TestMacro
		zbName = This.zbName 
		IF PEMSTATUS(this.parent,m.zbName,5)
			obj = SYS(1270)
 		   	btnZoom = this.parent.&zbName
			IF This.bObbl And Empty(This.Value) And This.CondObbl() AND MDOWN() AND TYPE("obj")='O' AND !ISNULL(m.obj) AND !ISNULL(m.btnZoom) And m.btnZoom.Enabled AND (LOWER(obj.name)=LOWER(m.btnZoom.Name) OR (pemstatus(this,"bOverBtnZoom",5) AND this.bOverBtnZoom))
				btnZoom._Click()
			ENDIF
			btnZoom = .Null.
			obj = .null.
		endif
		*--- Zucchetti Aulla Fine - Evito problema campi obbligatori e bottoni contestuali
		i_nRes=1
		* --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
		i_var=This.cFormVar
		i_bUpd=.F.
		if vartype(i_curform)='U'
			i_oldform=.null.
		else			
			i_oldform=i_curform
		endif
		If This.nZero<>0
			If !Empty(This.Value) And Left(This.Value,1)$'123456789'
				l=Iif(This.nZero>0,This.nZero,Len(This.Value))
				This.Value=Right(Repl('0',l)+Alltrim(This.Value),l)
			Endif
		Endif
		If This.IsUpdated()
			i_bUpd=.T.
			This.Parent.oContained.bUpdated=.T.
			If This.bIsInHeader
				This.Parent.oContained.bHeaderUpdated=.T.
			Endif
			This.Parent.oContained.&i_var=This.Value
			This.SetModified()
		Endif
		This.bUpd=.F.
		* --- After Input solo alla variazione del campo
		If i_bUpd
			* --- Zucchetti Aulla Inizio - non f� scattare l after imput in modalit� filtro altrimenti alcuni campi possono essere valorizzati e inclusi nel filtro
		    IF This.Parent.oContained.cFunction<>'Filter'
				This.mAfter()
			ENDIF
			* --- Zucchetti Aulla Inizio - Lancio della funzione CALCZER per gestire la codifica numerica di Clienti, Fornitori, Nominativi, Pratiche, Impianti
			Local l_OldLink, bNoCtrlValue,L_oldvalue
			l_OldLink = ''
			L_oldvalue = This.Parent.oContained.&i_var
			Do Case
				Case This.cLinkFile="CONTI" And Type('this.oKey_2_1')='C' And This.oKey_2_1 = "ANCODICE" And chknum(This.Value)
					mTIPCON = Strtran(This.oKey_1_2, '.', '.parent.oContained.')
					mVARNAME=Strtran(i_var, 'w_','o_')
					TestMacro=&mTIPCON
					If TestMacro$'C-F' And (Type('this.bNotCalczer')='U' Or (Type('this.bNotCalczer')='L' And Not This.bNotCalczer))
						This.Parent.oContained.&i_var = CALCZER(This.Parent.oContained.&i_var, This.cLinkFile,&mTIPCON)
					Endif
				Case This.cLinkFile="CAN_TIER" And Type('this.oKey_1_1')='C' And This.oKey_1_1 = "CNCODCAN" And chknum(This.Value)
					mVARNAME=Strtran(i_var, 'w_','o_')
					If (Type('this.bNotCalczer')='U' Or (Type('this.bNotCalczer')='L' And Not This.bNotCalczer))
						This.Parent.oContained.&i_var = CALCZER(This.Parent.oContained.&i_var, This.cLinkFile)
					Endif
				Case This.cLinkFile="OFF_NOMI" And Type('this.oKey_1_1')='C' And This.oKey_1_1 = "NOCODICE" And chknum(This.Value)
					mVARNAME=Strtran(i_var, 'w_','o_')
					If (Type('this.bNotCalczer')='U' Or (Type('this.bNotCalczer')='L' And Not This.bNotCalczer))
						This.Parent.oContained.&i_var = CALCZER(This.Parent.oContained.&i_var, This.cLinkFile)
					Endif
				Case This.cLinkFile="IMP_MAST" And Type('this.oKey_1_1')='C' And This.oKey_1_1 = "IMCODICE" And chknum(This.Value)
					mVARNAME=Strtran(i_var, 'w_','o_')
					If (Type('this.bNotCalczer')='U' Or (Type('this.bNotCalczer')='L' And Not This.bNotCalczer))
						This.Parent.oContained.&i_var = CALCZER(This.Parent.oContained.&i_var, This.cLinkFile)
					Endif
			Endcase
			* --- Se lo zerofilling opzionale non entra in azione (valore di partenza non modificato) allore tento la
			* --- ricerca all'interno del contenuto
			TestMacro=(This.Parent.oContained.&i_var==L_oldvalue) And g_RicPerCont$'TSL' And Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query')) And !Empty(This.cLinkFile) And !Empty(This.Value) And Vartype(This.Parent.oContained.&i_var)='C'
			If TestMacro
				* -- Inserimento della '%' all'inizio della variabile
				This.Parent.oContained.&i_var = '%'+ This.Parent.oContained.&i_var
				l_OldLink = This.Parent.oContained.&i_var
				bNoCtrlValue=.T. && non eseguo la setcontrolvalue altrimenti mi mostra i %
			Endif
			* --- Zucchetti Aulla Fine - Lancio della funzione CALCZER per gestire la codifica numerica di Clienti, Fornitori, Nominativi, Pratiche
			* --- il valore puo' essere cambiato dalla funzione di "After"
			TestMacro=This.Value<>This.Parent.oContained.&i_var And Not bNoCtrlValue && Zucchetti Aulla Aggiunto not bNoCtrlValue per ctrl con % aggiunta in automatico
			If TestMacro
				This.SetControlValue()
			Endif
		Endif
		* ---
		If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
			i_bErr=.F.
			i_nRes=Iif(i_bUpd,0,1)
			oCpToolBar.b9.Enabled=.F.
			* --- Obbligatorieta'
			If This.bObbl And Empty(This.Value) And This.CondObbl()
				If Not(This.Parent.oContained.bDontReportError)
					If type("Thisform.oBalloon")='O'
						Thisform.oBalloon.ctltop = Objtoclient( This ,1)  + This.Height - 5
						Thisform.oBalloon.ctlleft = Objtoclient( This ,2) + This.Width / 2
						Thisform.oBalloon.ctlShow(CON_BTPOS_NONE , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , MSG_FIELD_CANNOT_BE_NULL_QM, TTI_WARNING)
					Else
						Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
						If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
							i_curform=This.Parent.oContained
							Createobject('cp_DetailHeader_deferred',This.Parent.oContained.w_oHeaderDetail)
							This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
						Endif
					Endif
				Endif
				i_nRes=0
				i_bErr=.T.
			Endif
			If Not(i_bErr) And i_bUpd
				If !Empty(This.cfgCheck)
					e=Strtran(This.cfgCheck,'w_','this.parent.oContained.w_')
					i_bErr=Not(&e)
					If i_bErr
						If Not(This.Parent.oContained.bDontReportError)
							If type("Thisform.oBalloon")='O'
								If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
									Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
								Else
									Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
								Endif
							Else
								Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
								If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
									i_curform=This.Parent.oContained
									Createobject('cp_DetailHeader_deferred',This.Parent.oContained.w_oHeaderDetail)
									This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
								Endif
							Endif
						Endif
						This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
						This.SetControlValue()
						i_nRes=0
					Endif
				Endif
				If Not(i_bErr) And i_bUpd
					* --- Procedura di check e calcolo se il campo e' cambiato
					***** Zucchetti Aulla Inizio  - Controllo validit� numeri e date in intervallo consentito
					Local nININUM, nFINNUM, nINIDAT, nFINDAT
					nININUM = Iif(Type('This.nMinValue')='N',This.nMinValue, i_ININUM)
					nFINNUM = Iif(Type('This.nMaxValue')='N',This.nMaxValue, i_FINNUM)
					nINIDAT = Iif(Type('This.dMinData')='C',Cp_CharToDate(This.dMinData), i_INIDAT)
					nFINDAT = Iif(Type('This.dMaxData')='C',Cp_CharToDate(This.dMaxData), i_FINDAT)
					i_MsgErrDat=CP_MSGFORMAT(MSG_INSERT_A_DATE_INCLUDED_BETWEEN__AND__,Dtoc(nINIDAT),Dtoc(nFINDAT))
					i_MsgErrNum=CP_MSGFORMAT(MSG_INSERT_AMOUNTS_INCLUDED_BETWEEN__AND__,Alltrim(Tran(nININUM,'@z 999,999,999,999,999,999,999,999,999')),Alltrim(Tran(nFINNUM,'@z 999,999,999,999,999,999,999,999,999')))

					If This.Check() And ( (Type('This.Value')='D' And ((This.Value>=nINIDAT And This.Value<=nFINDAT) Or Empty(This.Value)) Or Type('This.Value')<>'D'));
							And ( (Type('This.Value')='N' And ((This.Value>=nININUM And This.Value<=nFINNUM) Or Empty(This.Value)) Or Type('This.Value')<>'N') )
						i_nRes=1
						* --- Zucchetti Aulla Inizio - Eliminazione '%' su campi loosely linked
						TestMacro=g_RicPerCont$'TSL' And !Empty(This.cLinkFile) And Vartype(This.Parent.oContained.&i_var)='C' And !Empty(This.Parent.oContained.&i_var) And This.Parent.oContained.&i_var == l_OldLink And Left(This.Parent.oContained.&i_var,1) = '%'
						If TestMacro
							This.Parent.oContained.&i_var = Substr(This.Parent.oContained.&i_var, 2)
						Endif
						* --- Zucchetti Aulla Fine - Eliminazione '%' su campi loosely linked
					Else
						If Not(This.Parent.oContained.bDontReportError)
							If (Type('This.Value')='D' And (This.Value<nINIDAT Or This.Value>nFINDAT ) And Not Empty(This.Value))
								If Empty(i_MsgErrDat)
									If type("Thisform.oBalloon")='O'
										If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
											Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
										Else
											Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
										Endif
									Else
										Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
										If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
											i_curform=This.Parent.oContained
											Createobject('cp_DetailHeader_deferred',This.Parent.oContained.w_oHeaderDetail)
											This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
										Endif
									Endif
								Else
									If type("Thisform.oBalloon")='O'
										If Len(Alltrim(i_MsgErrDat))>100
											Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , i_MsgErrDat + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
										Else
											Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , i_MsgErrDat , TTI_WARNING)
										Endif
									Else
										cp_ErrorMsg (i_MsgErrDat,'','',.F.)
										If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
											i_curform=This.Parent.oContained
											Createobject('cp_DetailHeader_deferred',This.Parent.oContained.w_oHeaderDetail)
											This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
										Endif
									Endif
								Endif
							Else
								If (Type('This.Value')='N' And (This.Value<nININUM Or This.Value>nFINNUM) And Not Empty(This.Value))
									If Empty(i_MsgErrNum)
										If type("Thisform.oBalloon")='O'
											If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
												Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
											Else
												Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
											Endif
										Else
											Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
											If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
												i_curform=This.Parent.oContained
												Createobject('cp_DetailHeader_deferred',This.Parent.oContained.w_oHeaderDetail)
												This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
											Endif
										Endif
									Else
										If type("Thisform.oBalloon")='O'
											If Len(Alltrim(i_MsgErrNum))>100
												Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , i_MsgErrNum + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
											Else
												Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , i_MsgErrNum , TTI_WARNING)
											Endif
										Else
											cp_ErrorMsg (i_MsgErrNum,'','',.F.)
											If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
												i_curform=This.Parent.oContained
												Createobject('cp_DetailHeader_deferred',This.Parent.oContained.w_oHeaderDetail)
												This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
											Endif
										Endif
									Endif
								Else
									* --- Zucchetti Aulla Inizio - Disabilito Bordo Rosso se valore campo non valido..
									*this.specialeffect=1
									*this.bordercolor=RGB(0,0,255)
									* --- Zucchetti Aulla Fine
									If type("Thisform.oBalloon")='O'
										Thisform.oBalloon.ctltop = Objtoclient( This ,1)  + This.Height - 5
										Thisform.oBalloon.ctlleft = Objtoclient( This ,2) + This.Width / 2
										If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
											Thisform.oBalloon.ctlShow( CON_BTPOS_NONE , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
										Else
											Thisform.oBalloon.ctlShow( CON_BTPOS_NONE , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
										Endif
									Else
										Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
										If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
											i_curform=This.Parent.oContained
											Createobject('cp_DetailHeader_deferred',This.Parent.oContained.w_oHeaderDetail)
											This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
										Endif
									Endif
								Endif
							Endif
						Endif
						***** Zucchetti Aulla Fine
						* --- azzeramento della variabile e del control
						This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
						This.SetControlValue()
					Endif
				Endif
				This.SetTotal()
				This.Parent.oContained.NotifyEvent(i_var+' Changed')
				This.Parent.oContained.mCalc(i_bUpd)
				This.Parent.oContained.SaveDependsOn()
			Else
				This.SetTotal()
			Endif
		Endif
		i_curform=i_oldform
		This.Parent.oContained.bDontReportError=.F.
		Return(i_nRes)
	Proc LostFocus()
		Local i_cFlt,i_cFile,i_nConn,i_im
		*this.specialeffect=0 && Zucchetti Aulla - Commentato assegnamento (evidenziazione campi su cui � fallito il check)
		If This.Parent.oContained.cFunction='Query'
			If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
				Nodefault
				i_cFile=This.Parent.oContained.cFile
				i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
				i_cFlt=cp_BuildKeyWhere(This.Parent.oContained,This.cQueryName,i_nConn)
				This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
				This.Parent.oContained.LoadRecWarn()
			Endif
		Endif
		If !Empty(This.cSayPict)
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
		This.bInInput=.F.
		This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
		If !This.bNoBackColor
			This.BackColor=i_nEBackColor
		Endif
		Return
	Func IsUpdated()
		If This.bUpd
			Return (.T.)
		Endif
		Local i_var,i_m
		i_var=This.cFormVar
		i_m=This.InputMask
		If Vartype(This.Value)='C'
			If Empty(i_m)
				Return(Not(Trim(This.Value)==Trim(This.Parent.oContained.&i_var)))
			Else
				* c'e una picture, il valore della varibile deve essere formattato di conseguenza
				Return(Not(Trim(Transform(This.Value,i_m))==Trim(Transform(This.Parent.oContained.&i_var,i_m))))
			Endif
		Endif
		Return(Not(This.Value==This.Parent.oContained.&i_var))
	Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
		If nButton=1
			If Seconds()-0.3<This.nSeconds
				If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
					Nodefault
					This.Parent.oContained.bDontReportError=.T.
					Public i_lastindirectaction
					i_lastindirectaction='ecpZoom'
					This.mZoom()
				Endif
			Endif
			This.nSeconds=Seconds()
		Endif
		Return
		*
		* ------ METODI CODEPAINTER
		*
		* --- Procedura di default standard del campo
	Proc mDefault()
		Return
		* --- Procedura di Condizione standard del campo
	Func mCond()
		Return (.T.)
		* --- Procedura di Hide standard del campo
	Func mHide()
		Return (.F.)
		* --- Procedura di Before input standard del campo
	Proc mBefore()
		Return
		* --- Procedura di After input standard del campo
	Proc mAfter()
		Return
		* --- Procedura di check
	Func Check()
		Return(.T.)
	Func CondObbl()
		Return (.T.)
	Proc mZoom()
		If Not This.bNoZoomDateType And Type('this.parent.oContained.'+This.cFormVar)='D'
			cp_zdate(This)
		Endif
		Return
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And type("oSource.oContained")='O' And oSource.oContained.bLoaded
			Do Case
				Case nState=0   && Enter
					This.cOldIcon=oSource.DragIcon
					If oSource.cFile==This.cLinkFile
						oSource.DragIcon=i_cBmpPath+"cross01.cur"
					Endif
				Case nState=1   && Leave
					oSource.DragIcon=This.cOldIcon
			Endcase
		Endif
		Return
	Proc DragDrop(oSource, nXCoord, nYCoord)
		If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And type("oSource.oContained")='O' And oSource.oContained.bLoaded
			If oSource.cFile==This.cLinkFile
				This.ecpDrop(oSource)
			Endif
		Endif
		Return
	Proc ecpDrop(oSource)
		Return
	Proc SetModified()
		Return
	Proc ResetTotal()
		Return
	Proc SetTotal()
		Return
	Proc SetControlValue()
		Local i_var,i_cs
		i_var=This.cFormVar
		i_cs=This.ControlSource
		This.Value=This.Parent.oContained.&i_var
		If !(Empty(i_cs))
			Select (This.Parent.oContained.cTrsName)
			Replace &i_cs With This.Value
		Endif
		Return
	Proc ProgrammaticChange()
		* --- questa routine gestisce le picture dinamiche
		Local i_im
		If !This.bInInput And !Empty(This.cSayPict) And !(Left(This.cSayPict,1)$["'])
			* wait window "calcola picture "+this.name nowait
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
Enddefine

* === Definizione campo MEMO Standard
Define Class StdMemo As CPEditBox
	*
	* --- PROPRIETA' VFP
	*
	Height = 23
	Caption = ""
	* visible = .t.                          && ERRORE Per gli oggetti contenuti va settato da fuori
	FontSize   = 9
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	FontName   = ""
	*
	* --- PROPRIETA' CODEPAINTER
	*
	cQueryName = ""
	bObbl = .F.                            && Obbligatorio
	sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
	cLinkFile=''
	cOldIcon=''
	bHasZoom=.F.
	nPag=0
	cFormVar=''
	bIsInHeader=.T.
	bUpd=.F.
	nSeconds=0
	* --
	cfgCalc=''
	cfgCond=''
	cfgCheck=''
	cfgDefault=''
	cfgHide=''
	cfgInit=''
	*** Zucchetti Aulla Inizio -frasi modello
	bdisablefrasimodello=.F.      && Disabilita lo zoom frasi modello
	*** Zucchetti Aulla fine
	bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
	bNoMenuAction=.F.         	&& Disabilita nel tasto destro sottomenu Azioni
	bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
	bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
	bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
	bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
	bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
	bSetFont = .T.
	bGlobalFont=.F.

	*
	* --- COSTRUTTORE/DISTRUTTORE
	*
	* --- Inizializzazione
	Procedure Init()
		If Empty(This.FontName)
			This.FontName   = This.Parent.FontName
			This.FontSize   = This.Parent.FontSize
			This.FontBold   = This.Parent.FontBold
			This.FontItalic = This.Parent.FontItalic
			This.FontUnderline  = This.Parent.FontUnderline
			This.FontStrikethru = This.Parent.FontStrikethru
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
		This.ToolTipText=cp_Translate(This.ToolTipText)
		DoDefault()
		Return

	Procedure SetFont()
		If This.bGlobalFont
			SetFont('M', This)
		Endif
	Endproc
	*
	* --- EVENTI/METODI VFP
	*
	* --- Quando prende il Focus
	Proc GotFocus()
		Local i_var
		Local i_oldvalue
		LOCAL TestMacro
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			This.Parent.oContained.SaveDependsOn()
			i_oldvalue=This.Value
			This.mDefault()
			* --- Zucchetti Aulla - Inizio Interfaccia (abilito il bottone sui campi memo)
			If Not This.bdisablefrasimodello And Not This.ReadOnly
				oCpToolBar.b9.Enabled=.T.
			Endif
			* --- Zucchetti Aulla - Fine Interfaccia
			This.bUpd=This.Value<>i_oldvalue
		Endif
		If This.Parent.oContained.cFunction<>"Filter"
			This.bUpd=.F.
			This.mBefore()
			* --- il valore puo' essere cambiato dalla funzione di "Before"
			i_var=This.cFormVar
			TestMacro=This.Parent.oContained.&i_var
			If This.Value<>TestMacro
				This.SetControlValue()
				This.bUpd=.T.
			Endif
		Endif
		If !This.bNoBackColor
			This.BackColor=i_nBackColor
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Inizio
		If Type('This.parent.aPageMovers')='O'
			Do While This.Top+This.Parent.Top<0 And This.Parent.aPageMovers.Count>0
				If This.Parent.aPageMovers.Count>0
					* Se non sono arrivato all'inizio della collezione sposto
					* il pageframe alla posizione precedente e elimino l'informazioni dalla collezione
					This.Parent.Top = This.Parent.aPageMovers[This.parent.aPageMovers.Count]
					This.Parent.aPageMovers.Remove(This.Parent.aPageMovers.Count)
				Endif
			Enddo
			This.Parent.Refresh()
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Fine
		Return

		* --- Zucchetti Aulla - Frasi modello
	Proc mZoom()
		*passo propriet� per parametri query
		If  ((Type('this.bdisablefrasimodello') = 'L' And Not This.bdisablefrasimodello) Or Type('this.bdisablefrasimodello') <> 'L') And Not This.ReadOnly
			This.Parent.oContained.AddProperty("ptipo",'C')
			This.Parent.oContained.AddProperty("pgest", Iif(Pemstatus(This.Parent.oContained, 'getSecurityCode', 5), This.Parent.oContained.getSecurityCode(), This.Parent.oContained.cprg))
			Public i_lastindirectaction
			i_lastindirectaction='ecpZoom'
			Do cp_zoom With 'FRA_MODE','*','FMSERIAL',cp_AbsName(This.Parent,This.Name),.F.,'GSUT_AFM',cp_Translate(MSG_PHRASE_MODEL),'GSUT_AFM.FRA_MODE_VZM',This.Parent.oContained
			Removeproperty(This.Parent.oContained, "ptipo")
			Removeproperty(This.Parent.oContained, "pgest")
		Endif
		Return
		* --- Zucchetti Aulla -  Frasi modello

	Func When()
		Local bRes,i_e
		* --- Procedure standard del campo
		bRes=.T.
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			bRes = This.mCond()
			If !Empty(This.cfgCond)
				i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
				bRes=&i_e
			Endif
		Endif
		Return(bRes)
	Func Valid()
		Local i_nRes,i_bErr,i_var,i_bUpd
		LOCAL TestMacro
		i_nRes=1
		* --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
		i_var=This.cFormVar
		i_bUpd=.F.
		TestMacro=This.bUpd Or (Not(Trim(This.Value)==Trim(This.Parent.oContained.&i_var)))
		If TestMacro
			i_bUpd=.T.
			This.Parent.oContained.bUpdated=.T.
			If This.bIsInHeader
				This.Parent.oContained.bHeaderUpdated=.T.
			Endif
			This.Parent.oContained.&i_var=This.Value
			This.SetModified()
			This.bUpd=.F.
		Endif
		* --- After Input solo alla variazione del campo
		If i_bUpd
			This.mAfter()
			* --- il valore puo' essere cambiato dalla funzione di "After"
			TestMacro=This.Parent.oContained.&i_var
			If This.Value<>TestMacro
				This.SetControlValue()
			Endif
		Endif
		* ---
		If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
			i_bErr=.F.
			i_nRes=Iif(i_bUpd,0,1)
			oCpToolBar.b9.Enabled=.F.
			* --- Obbligatorieta'
			If This.bObbl And Empty(This.Value) And This.CondObbl()
				If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
					This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
				Endif
				If Not(This.Parent.oContained.bDontReportError)
					If type("Thisform.oBalloon")='O'
						Thisform.oBalloon.ctlShow(CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()'), MSG_FIELD_CANNOT_BE_NULL_QM , TTI_WARNING)
					Else
						Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
					Endif
				Endif
				i_nRes=0
				i_bErr=.T.
			Endif
			If Not(i_bErr) And i_bUpd
				* --- Procedura di check e calcolo se il campo e' cambiato
				If This.Check()
					i_nRes=1
				Else
					If Not(This.Parent.oContained.bDontReportError)
						If type("Thisform.oBalloon")='O'
							If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
								Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
							Else
								Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
							Endif
						Else
							Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
						Endif
					Endif
					* --- azzeramento della variabile e del control
					This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
					This.SetControlValue()
				Endif
				This.SetTotal()
				This.Parent.oContained.NotifyEvent(i_var+' Changed')
				This.Parent.oContained.mCalc(i_bUpd)
				This.Parent.oContained.SaveDependsOn()
			Else
				This.SetTotal()
			Endif
		Endif
		This.Parent.oContained.bDontReportError=.F.
		Return(i_nRes)
	Proc LostFocus()
		Local i_cFile,i_nConn
		If This.Parent.oContained.cFunction='Query'
			If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName)) And Upper(This.cQueryName) $ Upper(This.Parent.oContained.cKeyselect) && All'uscita di un campo memo il filtro va in errore
				Nodefault
				i_cFile=This.Parent.oContained.cFile
				i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
				cFlt=cp_BuildWhere('',This.Value,This.cQueryName,i_nConn)
				This.Parent.oContained.QueryKeySet(cFlt,This.cQueryName)
				This.Parent.oContained.LoadRecWarn()
			Endif
		Endif
		This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
		If !This.bNoBackColor
			This.BackColor=i_nEBackColor
		Endif
		Return
		*
		* ------ METODI CODEPAINTER
		*
		* --- Procedura di default standard del campo
	Proc mDefault()
		Return
		* --- Procedura di Conizione standard del campo
	Func mCond()
		Return (.T.)
		* --- Procedura di Hide standard del campo
	Func mHide()
		Return (.F.)
		* --- Procedura di Before input standard del campo
	Proc mBefore()
		Return
		* --- Procedura di After input standard del campo
	Proc mAfter()
		Return
		* --- Procedura di check
	Func Check()
		Return(.T.)
	Func CondObbl()
		Return (.T.)
		* --- Procedure di Drag & Drop
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And type("oSource.oContained")='O' And oSource.oContained.bLoaded
			Do Case
				Case nState=0   && Enter
					This.cOldIcon=oSource.DragIcon
					If oSource.Parent.Parent.Parent.cFile==This.cLinkFile
						oSource.DragIcon=i_cBmpPath+"cross01.cur"
					Endif
				Case nState=1   && Leave
					oSource.DragIcon=This.cOldIcon
			Endcase
		Endif
		Return
	Proc DragDrop(oSource, nXCoord, nYCoord)
		If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And type("oSource.oContained")='O' And oSource.oContained.bLoaded
			If oSource.cFile==This.cLinkFile
				This.ecpDrop(oSource)
			Endif
		Endif
		Return
	Proc SetModified()
		Return
	Proc ResetTotal()
		Return
	Proc SetTotal()
		Return
	Proc SetControlValue()
		Local i_var,i_cs
		i_var=This.cFormVar
		i_cs=This.ControlSource
		This.Value=This.Parent.oContained.&i_var
		If !(Empty(i_cs))
			Select (This.Parent.oContained.cTrsName)
			Replace &i_cs With This.Value
		Endif
		Return
	Proc DblClick
		** Zucchetti Aulla Inizio - Per gestire il doppio click sul campo memo e visualizzare la tabella delle frasi modello
		If This.bHasZoom
			** Zucchetti Aulla fine - Per gestire il doppio click sul campo memo e visualizzare la tabella delle frasi modello
			If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
				Nodefault
				This.Parent.oContained.bDontReportError=.T.
				Public i_lastindirectaction
				i_lastindirectaction='ecpZoom'
				This.mZoom()
			Endif
			** Zucchetti Aulla Inizio - Per gestire il doppio click sul campo memo e visualizzare la tabella delle frasi modello
		Else
			If  Not This.bdisablefrasimodello
				If Not This.ReadOnly
					If Inlist(This.Parent.oContained.cFunction,'Edit','Load') And This.ReadOnly=.F.
						Nodefault
						This.Parent.oContained.bDontReportError=.T.
						Public i_lastindirectaction
						i_lastindirectaction='ecpZoom'
						If Type('this.bdisablefrasimodello') = 'L' And Not This.bdisablefrasimodello
							*passo propriet� per parametri query
							This.Parent.oContained.AddProperty("ptipo",'C')
							This.Parent.oContained.AddProperty("pgest", Iif(Pemstatus(This.Parent.oContained, 'getSecurityCode', 5), This.Parent.oContained.getSecurityCode(), This.Parent.oContained.cprg))
							Do cp_zoom With 'FRA_MODE','*','FMSERIAL',cp_AbsName(This.Parent,This.Name),.F.,'GSUT_AFM',cp_Translate(MSG_PHRASE_MODEL),'GSUT_AFM.FRA_MODE_VZM',This.Parent.oContained
							Removeproperty(This.Parent.oContained, "ptipo")
							Removeproperty(This.Parent.oContained, "pgest")
						Else
							This.mZoom()
						Endif
					Endif
				Endif
			Endif

		Endif
		** Zucchetti Aulla fine - Per gestire il doppio click sul campo memo e visualizzare la tabella delle frasi modello
		Return

		* --- Zucchetti Aulla inizio (frasi modello)
		*passo il valore del campo memo al campo (simulando un link sulla maschera)
	Procedure ecpDrop(oSource)
		Local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt,i_n,i_cVarMem
		i_n=cp_OpenTable('FRA_MODE')
		i_nConn = i_TableProp[i_n,3]
		i_cTable = cp_SetAzi(i_TableProp[i_n,2])
		i_nArea = Select()
		i_bEmpty = .F.
		If Used("_Link_")
			Select _Link_
			Use
		Endif
		If .Not. Empty(oSource.xKey(1))
			If i_nConn<>0
				i_ret=cp_SQL(i_nConn,"select FMSERIAL,FM__MEMO,FMEMLPRI";
					+" from "+i_cTable+" where FMSERIAL="+cp_ToStrODBC(oSource.xKey(1));
					,"_Link_")
				i_reccount = Iif(i_ret=-1,0,Reccount())
			Else
				i_cWhere = cp_PKFox(i_cTable;
					,'FMSERIAL',oSource.xKey(1))
				Select FMSERIAL,FM__MEMO,FMEMLPRI;
					from (i_cTable) Where &i_cWhere. Into Cursor _Link_
				i_reccount = _Tally
			Endif
		Else
			i_reccount = 0
		Endif
		If i_reccount>0 And Used("_Link_")
			*--- Priorit� email
			If Type("this.parent.oContained.class")='C' And Lower(This.Parent.oContained.Class)="tgsut_kim"
				setvaluelinked("M", This.Parent.oContained, "w_SMTPPRIO", Nvl(_Link_.FMEMLPRI, 3))
			Endif
			This.Value=Stuff(This.Value, this.selstart+1,this.SelLength , Nvl(_Link_.FM__MEMO,Space(0)))
			i_cVarMem=This.cformvar
			This.Parent.oContained.&i_cVarMem=this.value
		Else
			This.Value = Space(0)
		Endif
		cp_CloseTable('FRA_MODE')
		Select (i_nArea)
	Endproc
	* --- Zucchetti Aulla Fine (frasi modello)
Enddefine

* === Definizione stringa Standard
Define Class StdString As CPLabel
	*
	* --- PROPRIETA' VFP
	*
	*autosize = .t.
	FontSize   = 9
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	FontName   = ""
	BackStyle=0
	* Visible = .t.
	* --
	cfgHide=''
	bSetFont = .T.
	bGlobalFont=.F.
	*
	* --- COSTRUTTORE/DISTRUTTORE
	*
	Procedure Init()
		If Empty(This.FontName)
			This.FontName   = This.Parent.FontName
			This.FontSize   = This.Parent.FontSize
			This.FontBold   = This.Parent.FontBold
			This.FontItalic = This.Parent.FontItalic
			This.FontUnderline  = This.Parent.FontUnderline
			This.FontStrikethru = This.Parent.FontStrikethru
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
		This.Caption=cp_Translate(This.Caption)
		If TYPE("i_bGradientBck")<>"U" AND !i_bGradientBck AND i_VisualTheme<>-1
			This.backcolor = i_ThemesManager.GetProp(7)
			This.BackStyle=1
		ENDIF 
		DoDefault()
		Return
	Procedure SetFont()
		If This.bGlobalFont
			SetFont('L', This)
		Endif
	Endproc

	* --- Procedura di Hide standard del campo
	Func mHide()
		Return (.F.)

		* --- Zucchetti Aulla - non visualizza i due punti finali per le label
	Procedure caption_assign( cValue )
		If !g_Colon And Right(Alltrim(m.cValue),1)==':' And !Alltrim(m.cValue)==':'
			m.cValue = Left(Alltrim(m.cValue),Len(Alltrim(m.cValue))-1)
		Endif
		This.Caption = m.cValue
	Endproc
	* ---- Zucchetti Aulla - - non visualizza i due punti finali per le label

Enddefine

* === Definizione Button Standard
Define Class StdButton As CPCommandButton
	FontSize   = 9
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	FontName   = ""
	* --
	cfgCond=''
	cfgHide=''
	bSetFont = .T.
	bGlobalFont=.F.
	#If Version(5)>=800
		SpecialEffect = i_nBtnSpEfc
	#Endif

	Procedure Init()
		If Empty(This.FontName)
			This.FontName   = This.Parent.FontName
			This.FontSize   = This.Parent.FontSize
			This.FontBold   = This.Parent.FontBold
			This.FontItalic = This.Parent.FontItalic
			This.FontUnderline  = This.Parent.FontUnderline
			This.FontStrikethru = This.Parent.FontStrikethru
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
		This.Caption=cp_Translate(This.Caption)
		This.ToolTipText=cp_Translate(This.ToolTipText)
		DoDefault()
		Return

	Procedure SetFont()
		If This.bGlobalFont
			SetFont('B', This)
		Endif
	Endproc

	Func When()
		Local i_bRes,i_e
		* --- Procedure standard del campo
		i_bRes=.T.
		If Type('this.parent.oContained.cFunction')='O' && bisogna controllare: un bottone che implementa la
			&& funzione di sistema "Quit" puo' non trovare piu' il contenitore
			If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
				i_bRes = This.mCond()
				If !Empty(This.cfgCond)
					i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
					i_bRes=&i_e
				Endif
			Endif
		Endif
		Return(i_bRes)
	Func mCond()
		Return(.T.)
		* --- Procedura di Hide standard del campo
	Func mHide()
		Return (.F.)
Enddefine

* === Definizione Box Standard
Define Class StdBox As CPShape
	*
	* --- PROPRIETA' VFP
	*
	SpecialEffect = 0
	BackStyle = 0
	*
	* --- COSTRUTTORE/DISTRUTTORE
	*
	* --- Inizializzazione
	Proc Init()
		This.ZOrder(1)
		If Type("this.parent.pagebmp")='O'
			This.Parent.pagebmp.ZOrder(1)
		Endif
		DoDefault()
		Return
	Procedure RightClick()
		*--- Ribalto il rightclick sul parent
		This.Parent.RightClick()
	Endproc
Enddefine

* === Definizione CheckBox Standard
Define Class StdCheck As CPCheckBox
	*
	* --- PROPRIETA' VFP
	*
	Caption = ""
	BackStyle=0
	AutoSize = .T.
	FontName = ""
	FontSize   = 9
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	Value=0
	*
	* --- PROPRIETA' CODEPAINTER
	*
	cQueryName = ""
	bObbl = .F.                            && Obbligatorio
	sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
	cLinkFile=''
	cOldIcon=''
	bHasZoom=.F.
	nPag=0
	cFormVar=''
	bIsInHeader=.T.
	bUpd=.F.
	* --
	cfgCalc=''
	cfgCond=''
	cfgCheck=''
	cfgDefault=''
	cfgHide=''
	cfgInit=''
	bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
	bNoMenuAction=.T.         	&& Disabilita nel tasto destro sottomenu Azioni
	bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
	bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
	bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
	bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
	bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
	bSetFont = .T.
	bGlobalFont=.F.
	*
	* --- COSTRUTTORE/DISTRUTTORE
	*
	* --- Inizializzazione
	Procedure Init()
		If Empty(This.FontName)
			This.FontName       = This.Parent.FontName
			This.FontSize       = This.Parent.FontSize
			This.FontBold       = This.Parent.FontBold
			This.FontItalic     = This.Parent.FontItalic
			This.FontUnderline  = This.Parent.FontUnderline
			This.FontStrikethru = This.Parent.FontStrikethru
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
		This.Caption=cp_Translate(This.Caption)
		This.ToolTipText=cp_Translate(This.ToolTipText)
		DoDefault()
		Return

	Procedure SetFont()
		If This.bGlobalFont
			SetFont('X', This)
		Endif
	Endproc
	*
	* --- EVENTI/METODI VFP
	*
	* --- Quando prende il Focus
	Proc GotFocus()
		Local i_var
		Local i_oldvalue
		LOCAL TestMacro
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			This.Parent.oContained.SaveDependsOn()
			i_oldvalue=This.Value
			This.mDefault()
			oCpToolBar.b9.Enabled=This.bHasZoom
			This.bUpd=This.Value<>i_oldvalue
		Endif
		If This.Parent.oContained.cFunction<>"Filter"
			This.bUpd=.F.
			This.mBefore()
			* --- il valore puo' essere cambiato dalla funzione di "Before"
			i_var=This.cFormVar
			TestMacro=This.Parent.oContained.&i_var
			If This.RadioValue()<>TestMacro
				This.SetControlValue()
				This.bUpd=.T.
			Endif
		Endif
		If !This.bNoBackColor
			This.BackColor = i_nBackColor
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Inizio
		If Type('This.parent.aPageMovers')='O'
			Do While This.Top+This.Parent.Top<0 And This.Parent.aPageMovers.Count>0
				If This.Parent.aPageMovers.Count>0
					* Se non sono arrivato all'inizio della collezione sposto
					* il pageframe alla posizione precedente e elimino l'informazioni dalla collezione
					This.Parent.Top = This.Parent.aPageMovers[This.parent.aPageMovers.Count]
					This.Parent.aPageMovers.Remove(This.Parent.aPageMovers.Count)
				Endif
			Enddo
			This.Parent.Refresh()
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Fine
		Return
	Func When()
		Local i_bRes
		* --- Procedure standard del campo
		i_bRes=.T.
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			i_bRes = This.mCond()
			If !Empty(This.cfgCond)
				i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
				i_bRes=&i_e
			Endif
		Endif
		Return(i_bRes)
	Func Valid()
		Local i_bRes,i_bErr,i_var,i_bUpd,i_xOldValue
		LOCAL TestMacro
		i_bRes=.T.
		* --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
		i_var=This.cFormVar
		i_bUpd=.F.
		i_xOldValue=This.Parent.oContained.&i_var
		TestMacro=This.bUpd Or (Not(This.RadioValue()==i_xOldValue) And Not(Empty(This.Value) And Empty(This.Parent.oContained.&i_var)))
		If TestMacro
			i_bUpd=.T.
			This.Parent.oContained.bUpdated=.T.
			If This.bIsInHeader
				This.Parent.oContained.bHeaderUpdated=.T.
			Endif
			This.GetRadio()
			This.SetModified()
			This.bUpd=.F.
		Endif
		* --- After Input solo alla variazione del campo
		If i_bUpd
			This.mAfter()
			TestMacro=This.Parent.oContained.&i_var
			If This.RadioValue()<>TestMacro
				This.SetControlValue()
			Endif
		Endif
		* ---
		If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
			i_bErr=.F.
			i_bRes=Iif(i_bUpd,.F.,.T.)
			oCpToolBar.b9.Enabled=.F.
			* --- Obbligatorieta'
			If This.bObbl And Empty(This.Value)
				If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
					This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
				Endif
				If Not(This.Parent.oContained.bDontReportError)
					If type("Thisform.oBalloon")='O'
						Thisform.oBalloon.ctlShow(CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , MSG_FIELD_CANNOT_BE_NULL_QM , TTI_WARNING)
					Else
						Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
					Endif
				Endif
				i_bRes=.F.
				i_bErr=.T.
			Endif
			If Not(i_bErr) And i_bUpd
				* --- Procedura di check e calcolo se il campo e' cambiato
				If This.Check()
					i_bRes=.T.
				Else
					If Not(This.Parent.oContained.bDontReportError)
						If type("Thisform.oBalloon")='O'
							If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
								Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
							Else
								Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
							Endif
						Else
							Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
						Endif
					Endif
					* --- azzeramento della variabile e del control
					This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
					This.SetControlValue()
				Endif
				This.SetTotal(i_xOldValue)
				This.Parent.oContained.NotifyEvent(i_var+' Changed')
				This.Parent.oContained.mCalc(i_bUpd)
				This.Parent.oContained.SaveDependsOn()
			Else
				This.SetTotal(i_xOldValue)
			Endif
		Endif
		This.Parent.oContained.bDontReportError=.F.
		Return(i_bRes)
	Proc LostFocus()
		Local i_cFlt,i_cFile,i_nConn
		If This.Parent.oContained.cFunction='Query'
			If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
				Nodefault
				i_cFile=This.Parent.oContained.cFile
				i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
				i_cFlt=cp_BuildWhere('',This.RadioValue(),This.cQueryName,i_nConn)
				This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
				This.Parent.oContained.LoadRecWarn()
			Endif
		Endif
		This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
		If !This.bNoBackColor
			This.BackColor=i_nEBackColor
		Endif
		Return
		*
		* ------ METODI CODEPAINTER
		*
		* --- Procedura di default standard del campo
	Proc mDefault()
		Return
		* --- Procedura di Conizione standard del campo
	Func mCond()
		Return (.T.)
		* --- Procedura di Hide standard del campo
	Func mHide()
		Return (.F.)
		* --- Procedura di Before input standard del campo
	Proc mBefore()
		Return
		* --- Procedura di After input standard del campo
	Proc mAfter()
		Return
		* --- Procedura di check
	Func Check()
		Return(.T.)
		* --- Procedure di Drag & Drop
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
        	If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			Do Case
				Case nState=0   && Enter
					This.cOldIcon=oSource.DragIcon
					If oSource.Parent.Parent.Parent.cFile==This.cLinkFile
						oSource.DragIcon=i_cBmpPath+"cross01.cur"
					Endif
				Case nState=1   && Leave
					oSource.DragIcon=This.cOldIcon
			Endcase
		Endif
		Return
	Proc DragDrop(oSource, nXCoord, nYCoord)
        	If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			If oSource.cFile==This.cLinkFile
				This.ecpDrop(oSource)
			Endif
		Endif
		Return
	Proc SetModified()
		Return
	Proc SetTotal(i_xOldValue)
		Return
	Proc SetControlValue()
		Local i_var,i_cs
		i_var=This.cFormVar
		i_cs=This.ControlSource
		This.SetRadio()
		If !(Empty(i_cs))
			Select (This.Parent.oContained.cTrsName)
			Replace &i_cs With This.Value
		Endif
		Return
Enddefine
* ---

* === Definizione Radio Button Standard
Define Class StdRadio As CPOptionGroup
	*
	* --- PROPRIETA' VFP
	*
	BackStyle=0
	Visible = .T.
	BorderStyle = 0
	ButtonCount=1
	FntName   = ""
	FntSize   = 9
	FntBold   = .F.
	FntItalic = .F.
	FntUnderline  = .F.
	FntStrikeThru = .F.
	Value=0
	*
	* --- PROPRIETA' CODEPAINTER
	*
	cQueryName = ""
	bObbl = .F.                            && Obbligatorio
	sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
	cLinkFile=''
	cOldIcon=''
	bHasZoom=.F.
	nPag=0
	cFormVar=''
	bIsInHeader=.T.
	bUpd=.F.
	ToolTipText=''
	* --
	cfgCalc=''
	cfgCond=''
	cfgCheck=''
	cfgDefault=''
	cfgHide=''
	cfgInit=''
	bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
	bNoMenuAction=.T.         	&& Disabilita nel tasto destro sottomenu Azioni
	bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
	bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
	bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
	bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
	bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
	bSetFont = .T.
	bGlobalFont=.F.
	bNoBackColor = .F.
	bNoDisabledBackColor = .F.
	*
	* --- COSTRUTTORE/DISTRUTTORE
	*
	* --- Inizializzazione
	Procedure Init()
		Local i
		If This.bSetFont
			This.SetFont()
		Endif
		If Empty(This.FntName)
			This.SetAll("FontName",This.Parent.FontName)
			This.SetAll("FontSize",This.Parent.FontSize)
			This.SetAll("FontBold",This.Parent.FontBold)
			This.SetAll("FontItalic",This.Parent.FontItalic)
			This.SetAll("FontUnderline",This.Parent.FontUnderline)
			This.SetAll("FontStrikeThru",This.Parent.FontStrikethru)
		Else
			This.SetAll("FontName",This.FntName)
			This.SetAll("FontSize",This.FntSize)
			This.SetAll("FontBold",This.FntBold)
			This.SetAll("FontItalic",This.FntItalic)
			This.SetAll("FontUnderline",This.FntUnderline)
			This.SetAll("FontStrikeThru",This.FntStrikeThru)
		Endif
		For i=1 To This.ButtonCount
			This.Buttons(i).Caption=cp_Translate(This.Buttons(i).Caption)
		Next
		Local translatedtooltiptext
		translatedtooltiptext=cp_Translate(This.ToolTipText)
		This.SetAll("ToolTipText",translatedtooltiptext)
		DoDefault()
		Return
	Procedure SetFont()
		This.FntName = SetFontName('R', Evl(This.FntName, This.Parent.FontName), This.bGlobalFont)
		This.FntSize = SetFontSize('R', Evl(This.FntSize, This.Parent.FontSize), This.bGlobalFont)
		This.FntItalic = SetFontItalic('R', Evl(This.FntItalic, This.Parent.FontItalic), This.bGlobalFont)
		This.FntBold = SetFontBold('R', Evl(This.FntBold, This.Parent.FontBold), This.bGlobalFont)
	Endproc
	*
	* --- EVENTI/METODI VFP
	*
	* --- Quando prende il Focus
	Proc GotFocus()
		Local i_var
		Local i_oldvalue
		LOCAL TestMacro
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			This.Parent.oContained.SaveDependsOn()
			i_oldvalue=This.Value
			This.mDefault()
			oCpToolBar.b9.Enabled=This.bHasZoom
			This.bUpd=This.Value<>i_oldvalue
		Endif
		If This.Parent.oContained.cFunction<>"Filter"
			This.bUpd=.F.
			This.mBefore()
			i_var=This.cFormVar
			TestMacro=This.Parent.oContained.&i_var
			If This.RadioValue()<>TestMacro
				This.SetControlValue()
				This.bUpd=.T.
			Endif
		Endif
		If !This.bNoBackColor
			This.BackColor = i_nBackColor
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Inizio
		If Type('This.parent.aPageMovers')='O'
			Do While This.Top+This.Parent.Top<0 And This.Parent.aPageMovers.Count>0
				If This.Parent.aPageMovers.Count>0
					* Se non sono arrivato all'inizio della collezione sposto
					* il pageframe alla posizione precedente e elimino l'informazioni dalla collezione
					This.Parent.Top = This.Parent.aPageMovers[This.parent.aPageMovers.Count]
					This.Parent.aPageMovers.Remove(This.Parent.aPageMovers.Count)
				Endif
			Enddo
			This.Parent.Refresh()
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Fine
		Return
	Func When()
		Local i_bRes
		* --- Procedure standard del campo
		i_bRes=.T.
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			i_bRes = This.mCond()
		Endif
		Return(i_bRes)
	Func Valid()
		Local i_bRes,i_bErr,i_var,i_bUpd,i_xOldValue
		LOCAL TestMacro
		i_bRes=.T.
		* --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
		i_var=This.cFormVar
		i_bUpd=.F.
		i_xOldValue=This.Parent.oContained.&i_var
		TestMacro=This.bUpd Or (Not(This.RadioValue()==i_xOldValue) And Not(Empty(This.Value) And Empty(This.Parent.oContained.&i_var)))
		If TestMacro
			i_bUpd=.T.
			This.Parent.oContained.bUpdated=.T.
			If This.bIsInHeader
				This.Parent.oContained.bHeaderUpdated=.T.
			Endif
			This.GetRadio()
			This.SetModified()
			This.bUpd=.F.
		Endif
		* --- After Input solo alla variazione del campo
		If i_bUpd
			This.mAfter()
			TestMacro=This.Parent.oContained.&i_var
			If This.RadioValue()<>TestMacro
				This.SetControlValue()
			Endif
		Endif
		* ---
		If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
			i_bErr=.F.
			i_bRes=Iif(i_bUpd,.F.,.T.)
			oCpToolBar.b9.Enabled=.F.
			* --- Obbligatorieta'
			If This.bObbl And Empty(This.Value)
				If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
					This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
				Endif
				If Not(This.Parent.oContained.bDontReportError)
					If type("Thisform.oBalloon")='O'
						Thisform.oBalloon.ctlShow(CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , MSG_FIELD_CANNOT_BE_NULL_QM , TTI_WARNING)
					Else
						Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
					Endif
				Endif
				i_bRes=.F.
				i_bErr=.T.
			Endif
			If Not(i_bErr) And i_bUpd
				* --- Procedura di check e calcolo se il campo e' cambiato
				If This.Check()
					i_bRes=.T.
				Else
					If Not(This.Parent.oContained.bDontReportError)
						If type("Thisform.oBalloon")='O'
							If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
								Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
							Else
								Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
							Endif
						Else
							Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
						Endif
					Endif
					This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
					This.SetControlValue()
				Endif
				This.SetTotal(i_xOldValue)
				This.Parent.oContained.NotifyEvent(i_var+' Changed')
				This.Parent.oContained.mCalc(i_bUpd)
				This.Parent.oContained.SaveDependsOn()
			Else
				This.SetTotal(i_xOldValue)
			Endif
		Endif
		This.Parent.oContained.bDontReportError=.F.
		Return(i_bRes)
	Proc LostFocus()
		Local i_cFlt,i_cFile,i_nConn
		If This.Parent.oContained.cFunction='Query'
			If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
				Nodefault
				i_cFile=This.Parent.oContained.cFile
				i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
				i_cFlt=cp_BuildWhere('',This.RadioValue(),This.cQueryName,i_nConn)
				This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
				This.Parent.oContained.LoadRecWarn()
			Endif
		Endif
		This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
		If !This.bNoBackColor
			This.BackColor=i_nEBackColor
		Endif
		Return
		*
		* ------ METODI CODEPAINTER
		*
		* --- Procedura di default standard del campo
	Proc mDefault()
		Return
		* --- Procedura di Conizione standard del campo
	Func mCond()
		Return (.T.)
		* --- Procedura di Hide standard del campo
	Func mHide()
		Return (.F.)
		* --- Procedura di Before input standard del campo
	Proc mBefore()
		Return
		* --- Procedura di After input standard del campo
	Proc mAfter()
		Return
		* --- Procedura di check
	Func Check()
		Return(.T.)
		* --- Procedure di Drag & Drop
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
        If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			Do Case
				Case nState=0   && Enter
					This.cOldIcon=oSource.DragIcon
					If oSource.Parent.Parent.Parent.cFile==This.cLinkFile
						oSource.DragIcon=i_cBmpPath+"cross01.cur"
					Endif
				Case nState=1   && Leave
					oSource.DragIcon=This.cOldIcon
			Endcase
		Endif
		Return
	Proc DragDrop(oSource, nXCoord, nYCoord)
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			If oSource.cFile==This.cLinkFile
				This.ecpDrop(oSource)
			Endif
		Endif
		Return
	Proc SetModified()
		Return
	Proc ResetTotal()
		Return
	Proc SetTotal(i_xOldValue)
		Return
	Proc SetControlValue()
		Local i_var,i_cs
		i_var=This.cFormVar
		i_cs=This.ControlSource
		This.SetRadio()
		If !(Empty(i_cs))
			Select (This.Parent.oContained.cTrsName)
			Replace &i_cs With This.Value
		Endif
		Return
	Proc SetFocus()
		This.Buttons(1).SetFocus()
		Return
	Proc Enabled_(bEnable)
		Local i
		For i=1 To This.ButtonCount
			This.Buttons(i).Enabled=bEnable
		Next
		Return
Enddefine
* ---

* === Definizione ComboBox Standard
Define Class StdCombo As CPComboBox
	*
	* --- PROPRIETA' VFP
	*
	RowSourceType = 0
	FontName   = ""
	FontSize   = 9
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	Value=-1
	Style=2
	*
	* --- PROPRIETA' CODEPAINTER
	*
	cQueryName = ""
	bObbl = .F.                            && Obbligatorio
	sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
	cLinkFile=''
	cOldIcon=''
	bHasZoom=.F.
	nPag=0
	cFormVar=''
	bIsInHeader=.T.
	bUpd=.F.
	* --
	cfgCalc=''
	cfgCond=''
	cfgCheck=''
	cfgDefault=''
	cfgHide=''
	cfgInit=''
	bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
	bNoMenuAction=.T.         	&& Disabilita nel tasto destro sottomenu Azioni
	bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
	bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
	bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
	bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
	bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
	bSetFont = .T.
	bGlobalFont=.F.
	*
	* --- COSTRUTTORE/DISTRUTTORE
	*
	* --- Inizializzazione
	Procedure Init()
		If Empty(This.FontName)
			This.FontName   = This.Parent.FontName
			This.FontSize   = This.Parent.FontSize
			This.FontBold   = This.Parent.FontBold
			This.FontItalic = This.Parent.FontItalic
			This.FontUnderline  = This.Parent.FontUnderline
			This.FontStrikethru = This.Parent.FontStrikethru
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
		If .T. Or Type('i_cLanguage')<>'U' And !Empty(i_cLanguage)
			Local i,j,s,o
			o=This.RowSource
			s=''
			i=At(',',o)
			Do While i<>0
				*s=s+','+cp_Translate(left(o,i-1))
				This.AddItem(cp_Translate(Left(o,i-1)))
				o=Substr(o,i+1)
				i=At(',',o)
			Enddo
			*s=s+','+cp_Translate(o)
			*this.RowSource=substr(s,2)
			This.AddItem(cp_Translate(o))
		Endif
		This.ToolTipText=cp_Translate(This.ToolTipText)
		DoDefault()
		Return
	Procedure SetFont()
		If This.bGlobalFont
			SetFont('C', This)
		Endif
	Endproc
	*
	* --- EVENTI/METODI VFP
	*
	* --- Quando prende il Focus
	Proc GotFocus()
		Local i_var
		Local i_oldvalue
		LOCAL TestMacro
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			This.Parent.oContained.SaveDependsOn()
			i_oldvalue=This.Value
			This.mDefault()
			oCpToolBar.b9.Enabled=This.bHasZoom
			This.bUpd=This.Value<>i_oldvalue
		Endif
		If This.Parent.oContained.cFunction<>"Filter"
			This.bUpd=.F.
			This.mBefore()
			* --- il valore puo' essere cambiato dalla funzione di "Before"
			i_var=This.cFormVar
			TestMacro=This.Parent.oContained.&i_var
			If This.RadioValue()<>TestMacro
				This.SetControlValue()
				This.bUpd=.T.
			Endif
		Endif
		If !This.bNoBackColor
			This.BackColor = i_nBackColor
		Endif
		* --- Numero di elementi nella lista di drop down della combo
		If Vartype(g_DispCnt)='N'
			This.DisplayCount = g_DispCnt
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Inizio
		If Type('This.parent.aPageMovers')='O'
			Do While This.Top+This.Parent.Top<0 And This.Parent.aPageMovers.Count>0
				If This.Parent.aPageMovers.Count>0
					* Se non sono arrivato all'inizio della collezione sposto
					* il pageframe alla posizione precedente e elimino l'informazioni dalla collezione
					This.Parent.Top = This.Parent.aPageMovers[This.parent.aPageMovers.Count]
					This.Parent.aPageMovers.Remove(This.Parent.aPageMovers.Count)
				Endif
			Enddo
			This.Parent.Refresh()
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Fine
		Return
	Func When()
		Local i_bRes
		* --- Procedure standard del campo
		i_bRes=.T.
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			i_bRes = This.mCond()
			If !Empty(This.cfgCond)
				i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
				i_bRes=&i_e
			Endif
		Endif
		Return(i_bRes)
	Func Valid()
		Local i_bRes,i_bErr,i_var,i_bUpd,i_xOldValue
		LOCAL TestMacro
		i_bRes=.T.
		* --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
		i_var=This.cFormVar
		i_bUpd=.F.
		i_xOldValue=This.Parent.oContained.&i_var
		TestMacro=This.bUpd Or (Not(This.RadioValue()==i_xOldValue) And Not(Empty(This.Value) And Empty(This.Parent.oContained.&i_var)))
		If TestMacro
			i_bUpd=.T.
			This.Parent.oContained.bUpdated=.T.
			If This.bIsInHeader
				This.Parent.oContained.bHeaderUpdated=.T.
			Endif
			This.GetRadio()
			This.SetModified()
			This.bUpd=.F.
		Endif
		* --- After Input solo alla variazione del campo
		If i_bUpd
			This.mAfter()
			TestMacro=This.Parent.oContained.&i_var
			If This.RadioValue()<>TestMacro
				This.SetControlValue()
			Endif
		Endif
		* ---
		If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
			i_bErr=.F.
			i_bRes=Iif(i_bUpd,.F.,.T.)
			oCpToolBar.b9.Enabled=.F.
			* --- Obbligatorieta'
			If This.bObbl And Empty(Nvl(This.RadioValue(),"")) And This.CondObbl()
				If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
					This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
				Endif
				If Not(This.Parent.oContained.bDontReportError)
					If type("Thisform.oBalloon")='O'
						Thisform.oBalloon.ctlShow(CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , MSG_FIELD_CANNOT_BE_NULL_QM , TTI_WARNING)
					Else
						Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
					Endif
				Endif
				i_bRes=.F.
				i_bErr=.T.
			Endif
			If Not(i_bErr) And i_bUpd
				* --- Procedura di check e calcolo se il campo e' cambiato
				If This.Check()
					i_bRes=.T.
				Else
					If Not(This.Parent.oContained.bDontReportError)
						If type("Thisform.oBalloon")='O'
							If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
								Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
							Else
								Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
							Endif
						Else
							Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
						Endif
					Endif
					This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
					This.SetControlValue()
				Endif
				This.SetTotal(i_xOldValue)
				This.Parent.oContained.NotifyEvent(i_var+' Changed')
				This.Parent.oContained.mCalc(i_bUpd)
				This.Parent.oContained.SaveDependsOn()
			Else
				This.SetTotal(i_xOldValue)
			Endif
		Endif
		This.Parent.oContained.bDontReportError=.F.
		Return(i_bRes)
	Proc LostFocus()
		Local i_cFlt,i_cFile,i_nConn
		If This.Parent.oContained.cFunction='Query'
			If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
				Nodefault
				i_cFile=This.Parent.oContained.cFile
				i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
				i_cFlt=cp_BuildWhere('',This.Value,This.cQueryName,i_nConn)
				This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
				This.Parent.oContained.LoadRecWarn()
			Endif
		Endif
		This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
		If !This.bNoBackColor
			This.BackColor=i_nEBackColor
		Endif
		Return
		*
		* ------ METODI CODEPAINTER
		*
		* --- Procedura di default standard del campo
	Proc mDefault()
		Return
		* --- Procedura di Conizione standard del campo
	Func mCond()
		Return(.T.)
		* --- Procedura di Hide standard del campo
	Func mHide()
		Return (.F.)
		* --- Procedura di Before input standard del campo
	Proc mBefore()
		Return
		* --- Procedura di After input standard del campo
	Proc mAfter()
		Return
		* --- Procedura di check
	Func Check()
		Return(.T.)
		* --- Procedure di Drag & Drop
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
        	If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			Do Case
				Case nState=0   && Enter
					This.cOldIcon=oSource.DragIcon
					If oSource.Parent.Parent.Parent.cFile==This.cLinkFile
						oSource.DragIcon=i_cBmpPath+"cross01.cur"
					Endif
				Case nState=1   && Leave
					oSource.DragIcon=This.cOldIcon
			Endcase
		Endif
		Return
	Proc DragDrop(oSource, nXCoord, nYCoord)
        	If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			If oSource.cFile==This.cLinkFile
				This.ecpDrop(oSource)
			Endif
		Endif
		Return
	Proc SetModified()
		Return
	Proc SetTotal(i_xOldValue)
		Return
	Proc SetControlValue()
		Local i_var,i_cs
		i_var=This.cFormVar
		i_cs=This.ControlSource
		This.SetRadio()
		If !(Empty(i_cs))
			Select (This.Parent.oContained.cTrsName)
			Replace &i_cs With This.Value
		Endif
		Return
	Func CondObbl()
		Return (.T.)
Enddefine
* ---

Define Class StdTableCombo As StdCombo
	Dimension combovalues[1]
	nValues=0
	RowSourceType=0
	tablefilter=''
	bSetFont = .T.
	bNoBackColor=.F.
	bNoDisabledBackColor = .F.
	cDescEmptyElement = ' '
	xEmptyKey = .Null.		&&Valore della chiave del primo elemento (vuoto) di solito � space(1) o 0 in base al tipo della chiave

	Proc Init()
		This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
		This.ToolTipText=cp_Translate(This.ToolTipText)
		Local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
		Local i_fk,i_fd
		i_curs=Sys(2015)
		If Lower(Right(This.cTable,4))='.vqr'
			*--- Inizio richiesta parametri combotabellare
			vq_exec(This.cTable,Thisform,i_curs)
			i_fk=This.cKey
			i_fd=This.cValue
		Else
			i_nIdx=cp_OpenTable(This.cTable)
			If i_nIdx<>0
				i_nConn=i_TableProp[i_nIdx,3]
				i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
				i_n1=This.cKey
				i_n2=This.cValue
				If !Empty(This.cOrderBy)
					i_n3=' order by '+This.cOrderBy
				Else
					i_n3=''
				Endif
				i_flt=Iif(Empty(This.tablefilter),'',' where '+This.tablefilter)
				If i_nConn<>0
					cp_SQL(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
				Else
					Select &i_n1 As combokey,&i_n2 As combodescr From (i_cTable) &i_flt &i_n3 Into Cursor (i_curs)
				Endif
				i_fk='combokey'
				i_fd='combodescr'
				cp_CloseTable(This.cTable)
			Endif
		Endif
		If Used(i_curs)
			Select (i_curs)
			This.nValues=Reccount()+1
			Dimension This.combovalues[MAX(1,this.nValues)]
			i_bCharKey=Type(i_fk)='C'
			*this.AddItem(cp_Translate('<Nessun Valore>'))
			This.AddItem(This.cDescEmptyElement)
			*--- Zucchetti Aulla
			IF ISNULL(This.xEmptyKey)
				This.combovalues[1] = Iif(i_bCharKey,Space(1),0)
			Else
				This.combovalues[1] = This.xEmptyKey
			ENDIF
			*--- Zucchetti Aulla
			Do While !Eof()
				This.AddItem(Iif(Type(i_fd)='C',Alltrim(&i_fd),Alltrim(Str(&i_fd))))
				If i_bCharKey
					This.combovalues[recno()+1]=Trim(&i_fk)
				Else
					This.combovalues[recno()+1]=&i_fk
				Endif
				Skip
			Enddo
			Use
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
	Func RadioValue()
		If This.Value>0 And This.Value<=This.nValues
			Return(This.combovalues[this.value])
		Endif
		Return(This.xDefault)
	Func SetRadio()
		Local i_n
		i_n=This.cFormVar
		If Type('this.Parent.oContained.'+i_n)='C'
			This.Parent.oContained.&i_n.=Trim(This.Parent.oContained.&i_n.)
		Endif
		*--- premendo esc se un master con combo in hide viene evidenziato l'errore che non esiste l'oggetto
		If Type('this.Parent.oContained.'+i_n)<>'U'
			If Version(5)>=900
				This.Value=Ascan(This.combovalues,This.Parent.oContained.&i_n., -1, -1, -1, 6)
			Else
				Local cSetExact
				cSetExact=Set("EXACT")
				Set Exact On
				This.Value=Ascan(This.combovalues,This.Parent.oContained.&i_n.)
				Set Exact &cSetExact
			Endif
		Endif
		Return
	Func GetRadio()
		Local i_n
		i_n=This.cFormVar
		This.Parent.oContained.&i_n.=This.RadioValue()
		Return(.T.)
		*--- Zucchetti Aulla
	Func InteractiveChange()
		Local i_n , i_var
		i_n=DoDefault()
		*  		this.valid()
		i_var=This.cFormVar
		If Vartype(i_var)='C'
			This.Parent.oContained.NotifyEvent(i_var+' InteractiveChange')
		Endif
		Return i_n
		*--- Zucchetti Aulla
Enddefine

Define Class StdTrsField As StdField
	cTotal=''
	*
	* --- questa implementazione serve a similare il @K che sulle righe sembra
	*     non funzionare correttamente
	isprimarykey=.F.
	bFixedPos=.F.
	oldvalue=0
	
	*--- Zucchetti Aulla Inizio - Evito problema campi obbligatori e bottoni contestuali
	bOverBtnZoom = .F.
	*--- Zucchetti Aulla Fine - Evito problema campi obbligatori e bottoni contestuali
	
	Procedure Init()
		DoDefault()
		If !This.bFixedPos
			This.BackStyle=Iif(i_nDtlRowClr=Rgb(255,255,255),1,0)
		Endif
		Return
		LOCAL TestMacro
		Local l_olderr,l_err
		l_olderr=On('Error')
		l_err=.F.
		On Error l_err=.T.

		Local L_Cmd,l_oBJ
		If Not Empty(This.Parent.oContained.cOldbtn)
			L_Cmd=This.Parent.oContained.cOldbtn+'.visible'
			l_oBJ=This.Parent.oContained.cOldbtn+'.visible'
			TestMacro=Type("&l_obj")
			If TestMacro='L'
				&L_Cmd=.T.
			Endif
		Endif

		* visualizzo il bottone
		If Type('this.parent.'+This.zbName)='O'
			L_Cmd='this.parent.'+This.zbName+'.visible'
			l_oBJ='this.parent.'+This.zbName+'.visible'
			TestMacro=Type("&L_obj")
			If TestMacro='L'
				&L_Cmd = .T.
			Endif
			This.Parent.oContained.cOldbtn='this.parent.'+This.zbName
		Endif

		* visualizzo il bottone per la gestione delle traduzioni
		If Type('this.parent.'+This.transbName)='O'
			L_Cmd='this.parent.'+This.transbName+'.visible'
			l_oBJ='this.parent.'+This.transbName+'.visible'
			TestMacro=Type("&L_obj")
			If TestMacro='L'
				&L_Cmd = .T.
			Endif
			This.Parent.oContained.cOldbtn='this.parent.'+This.transbName
		Endif

		On Error &l_olderr

		Return
		* ---
	Proc SetModified()
		Select (This.Parent.oContained.cTrsName)
		If i_SRV<>'A'
			Replace i_SRV With 'U'
		Endif
		Return
	Proc ResetTotal()
		This.oldvalue=This.Value
		Return
	Proc SetTotal()
		Local i_tot
		If Not(Empty(This.cTotal))
			i_tot=This.cTotal
			&i_tot=&i_tot+This.Value-This.oldvalue
			This.oldvalue=This.Value
		Endif
		Return
	Function CheckObbl()
		Return This.Parent.oContained.FullRow()
Enddefine

Define Class StdTrsMemo As StdMemo
	cTotal=''
	oldvalue=0
	Proc SetModified()
		Select (This.Parent.oContained.cTrsName)
		If i_SRV<>'A'
			Replace i_SRV With 'U'
		Endif
		Return
	Proc ResetTotal()
		This.oldvalue=This.Value
		Return
	Proc SetTotal()
		Local i_tot
		If Not(Empty(This.cTotal))
			i_tot=This.cTotal
			&i_tot=&i_tot+This.Value-This.oldvalue
			This.oldvalue=This.Value
		Endif
		Return
Enddefine

Define Class StdTrsCheck As StdCheck
	cTotal=''
	Proc SetModified()
		Select (This.Parent.oContained.cTrsName)
		If i_SRV<>'A'
			Replace i_SRV With 'U'
		Endif
		Return
	Proc SetTotal(i_xOldValue)
		Local i_tot
		If Not(Empty(This.cTotal))
			i_tot=This.cTotal
			&i_tot=&i_tot+This.RadioValue()-i_xOldValue
		Endif
		Return
Enddefine

Define Class StdTrsRadio As StdRadio
	cTotal=''
	Proc SetModified()
		Select (This.Parent.oContained.cTrsName)
		If i_SRV<>'A'
			Replace i_SRV With 'U'
		Endif
		Return
	Proc SetTotal(i_xOldValue)
		Local i_tot
		If Not(Empty(This.cTotal))
			i_tot=This.cTotal
			&i_tot=&i_tot+This.RadioValue()-i_xOldValue
		Endif
		Return
Enddefine

Define Class StdTrsCombo As StdCombo
	cTotal=''
	Proc SetModified()
		Select (This.Parent.oContained.cTrsName)
		If i_SRV<>'A'
			Replace i_SRV With 'U'
		Endif
		Return
	Proc SetTotal(i_xOldValue)
		Local i_tot
		If Not(Empty(This.cTotal))
			i_tot=This.cTotal
			&i_tot=&i_tot+This.RadioValue()-i_xOldValue
		Endif
		Return
Enddefine

Define Class StdTrsTableCombo As StdTableCombo
	cTotal=''
	Proc SetModified()
		Select (This.Parent.oContained.cTrsName)
		If i_SRV<>'A'
			Replace i_SRV With 'U'
		Endif
		Return
	Proc SetTotal(i_xOldValue)
		Local i_tot
		If Not(Empty(This.cTotal))
			i_tot=This.cTotal
			&i_tot=&i_tot+This.RadioValue()-i_xOldValue
		Endif
		Return
	Func RadioValue(i_bTrs)
		Local i_xVal,i_n
		If i_bTrs
			i_n=Substr(This.cFormVar,3)
			i_xVal=t_&i_n.
		Else
			i_xVal=This.Value
		Endif
		If i_xVal>0 And i_xVal<=This.nValues
			Return(This.combovalues[i_xVal])
		Endif
		Return(This.xDefault)
	Func ToRadio()
		Local i_n
		i_n=This.cFormVar
		If Type('this.Parent.oContained.'+i_n)='C'
			This.Parent.oContained.&i_n.=Trim(This.Parent.oContained.&i_n.)
		Endif
		Local nRetVal
		If Version(5)>=900
			nRetVal=Ascan(This.combovalues,This.Parent.oContained.&i_n., -1, -1, -1, 6)
		Else
			Local cSetExact
			cSetExact=Set("EXACT")
			Set Exact On
			nRetVal=Ascan(This.combovalues,This.Parent.oContained.&i_n.)
			Set Exact &cSetExact
		Endif
		Return(nRetVal)
Enddefine
* ---

Define Class StdBody As CPGrid
	* --- Proprieta' VFP
	FontName = i_cProjectFontName
	FontSize=9
	FontBold=.F.
	FontItalic=.F.
	FontUnderline=.F.
	FontStrikethru=.F.
	GridLineColor = i_nGridLineColor
	* --- Proprieta' CP
	oFirstControl=.Null.
	cLinkFile=''
	cOldIcon=''
	nRelRow=0
	nAbsRow=0
	*bRowChanged=.f.
	nPrevRow=0
	nBeforeAfter=0
	AllowHeaderSizing=.F.
	
	* --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
	bNoSetAnchorForm = .T.
	* --- Zucchetti Aulla Fine  - Gestione SetAnchorForm
	
	Proc BeforeRowColChange(nIdx)
		*ACTIVATE SCREEN
		*? 'before',nIdx,RECNO(),this.nAbsRow
		If Type("this.parent.parent.parent.parent._paint_error_c")='N' And This.Parent.Parent.Parent.Parent._paint_error_c>=6
			Nodefault
			Return
		Endif
		If This.nBeforeAfter<1
			This.nBeforeAfter=This.nBeforeAfter+1
		Endif
		Return
	Proc AfterRowColChange(nIdx)
		*ACTIVATE SCREEN
		*? 'after',nIdx,RECNO(),this.nAbsRow
		If This.nBeforeAfter>0
			This.nBeforeAfter=This.nBeforeAfter-1
		Endif
		If This.nBeforeAfter=0
			Thisform.LockScreen=.F.
		Endif
		If !Inlist(This.Parent.oContained.cFunction,'Edit','Load')
			This.SetCurrentRow()
			This.Refresh()
		Endif
		Return
		*PROCEDURE scrolled(nDir)
		*  ACTIVATE SCREEN
		*  ? 'scrolled',nDir,RECNO(),this.Parent.oContained.oPgFrm.Page1.oPag.oBody.nAbsRow,this.Parent.oContained.oPgFrm.Page1.oPag.oBody.nRelRow
		*  if inlist(this.Parent.oContained.cFunction,'Edit','Load')
		*    *this.SetCurrentRow()
		*    *this.Parent.oContained.SetControlsValue()
		*  endif
		*  return
		*--- Zucchetti Aulla - Eliminato MouseDown per errore di duplicazione righe nel caso di errore nella CheckRow
		*    Procedure MouseDown(i_nBtn,i_nShift,i_nX,i_nY)
		*        If i_nX>This.Left+This.Width-15 AND Inlist(This.Parent.oContained.cFunction,'Edit','Load')
		*            * --- i campi memo non vengono scritti sulla riga corrente quando si scrolla perch� non perdono il focus!
		*            This.Parent.oContained.__dummy__.Enabled=.T.
		*            This.Parent.oContained.__dummy__.SetFocus()
		*            This.Parent.oContained.__dummy__.Enabled=.F.
		*        Endif
		*        Return
	Proc Click()
		Thisform.LockScreen=.T.
		This.Parent.oContained.__dummy__.Enabled=.T.
		This.Parent.oContained.__dummy__.SetFocus()
		If !(Alias()==This.Parent.oContained.cTrsName)
			Select (This.Parent.oContained.cTrsName)
		Endif
		If This.Parent.oContained.CheckRow()
			Go Bottom
			This.Parent.oContained.WorkFromTrs()
			If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow() And This.Parent.oContained.cFunction<>'Query'
				This.Parent.oContained.InitRow()
			Else
				This.Parent.oContained.ChildrenChangeRow()
			Endif
		Endif
		This.SetFocus()
		This.Parent.oContained.__dummy__.Enabled=.F.
		Thisform.LockScreen=.F.
		Return
	Proc mZoom()
		This.oBodyCol.oRow.ActiveControl.mZoom()
		Return
	Proc mZoomOnZoom()
		This.oBodyCol.oRow.ActiveControl.mZoomOnZoom()
		Return
	Proc SetFullFocus()
		This.SetFocus()
		This.oFirstControl.SetFocus()
		Return
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
        If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			Do Case
				Case nState=0   && Enter
					This.cOldIcon=oSource.DragIcon
					If oSource.Parent.Parent.Parent.cFile+'|'$This.cLinkFile
						oSource.DragIcon=i_cBmpPath+"cross01.cur"
					Endif
				Case nState=1   && Leave
					oSource.DragIcon=This.cOldIcon
			Endcase
		Endif
		Return
	Proc DragDrop(oSource, nXCoord, nYCoord)
		Local nRiga,nCurrRec,nRec,oDropInto,i
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			oDropInto=This.GetDropTarget(oSource.cFile,nXCoord,nYCoord)
			If Not(Isnull(oDropInto))
				If This.Parent.oContained.CheckRow()
					Select (This.Parent.oContained.cTrsName)
					nRiga=Int((nYCoord-cp_AbsTop(This,0))/This.RowHeight)+1
					nCurrRec=Recno()
					nRec=Recno()-Iif(This.nRelRow=0,1,This.nRelRow)+nRiga
					If nRec>Reccount()
						Go Bottom
						This.Parent.oContained.WorkFromTrs()
						If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow()
							This.Parent.oContained.InitRow()
						Endif
					Else
						Goto nRec
						This.Parent.oContained.WorkFromTrs()
					Endif
					This.SetFullFocus()
					oDropInto.ecpDrop(oSource)
					This.Parent.oContained.mCalc(.T.)
					This.Parent.oContained.SaveDependsOn()
					If i_SRV<>'A'
						Replace i_SRV With 'U'
					Endif
					This.Parent.oContained.TrsFromWork()
					Goto nCurrRec
					oSource.SetFocus()
				Else
					oSource.SetFocus()
					This.Parent.oContained.oNewFocus=.Null.
				Endif
			Endif
		Endif
		Return
	Func GetDropTarget(cFile,nX,nY)
		Return(.Null.)
Enddefine

Define Class BodyKeyMover As CPCommandButton
	Height=0
	Width=0
	BorderWidth=0
	Style=1
	nDirection=0
	bMoveFocus=.F.
	Proc GotFocus()
		If !This.bMoveFocus
			If This.nDirection=-1
				If Inlist(Lastkey(),5,15)
					This.Parent.oBody.SetFullFocus()
				Else
					Keyboard '{TAB}'
				Endif
			Else
				If Inlist(Lastkey(),5,15)
					Keyboard '{UPARROW}'
				Else
					This.Parent.oBody.SetFullFocus()
				Endif
			Endif
		Endif
		Return
	Proc MoveFocus()
		This.bMoveFocus=.T.
		This.SetFocus()
		If This.nDirection=-1
			Thisform.LockScreen=.F.
			Keyboard '{TAB}'
		Else
			Keyboard '{UPARROW}'
		Endif
		This.bMoveFocus=.F.
		Return
Enddefine

Define Class LastKeyMover As CPCommandButton
	Height=0
	Width=0
	Style=1
	BorderWidth=0
	bHasFixedBody=.F.
	bMoveFocus=.F.
	Proc GotFocus()
		If !This.bMoveFocus
			If This.bHasFixedBody
				This.Parent.Parent.Parent.Parent.oBeginFixedBody.MoveFocus()
			Else
				If This.Parent.oContained.CheckRow()
					Thisform.LockScreen=.T.
					Select (This.Parent.oContained.cTrsName)
					*if recno()>=reccount()
					If Recno()>=This.Parent.oContained.nLastRow
						If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow()
							This.Parent.oContained.InitRow()
						Else
							This.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
						Endif
					Else
						This.Parent.oContained.i_lastcheckrow=This.Parent.oContained.oPgFrm.Page1.oPag.oBody.nAbsRow
						If This.Parent.oContained.oPgFrm.Page1.oPag.oBody.RelativeRow=(This.Parent.oContained.i_nRowPerPage-1)
							This.Parent.oContained.oPgFrm.Page1.oPag.oBody.DoScroll(1)
						Endif
						Skip
						This.Parent.Parent.Parent.SetFullFocus()
					Endif
				Else
					If Not(Isnull(This.Parent.oContained.oNewFocus))
						This.Parent.oContained.oNewFocus.SetFocus()
						This.Parent.oContained.oNewFocus=.Null.
					Endif
				Endif
			Endif
		Endif
		Return
	Proc MoveFocus()
		This.bMoveFocus=.T.
		This.SetFocus()
		Keyboard '{UPARROW}{UPARROW}'
		This.bMoveFocus=.F.
		Return
Enddefine

Define Class FixedBodyKeyMover As CPCommandButton
	Height=0
	Width=0
	Style=1
	nDirection=0
	bMoveFocus=.F.
	Proc GotFocus()
		If !This.bMoveFocus
			If This.nDirection=-1
				If Inlist(Lastkey(),5,15)
					Keyboard '{UPARROW}'
				Else
					If This.Parent.oContained.CheckRow()
						Thisform.LockScreen=.T.
						Select (This.Parent.oContained.cTrsName)
						*if recno()>=reccount()
						If Recno()>=This.Parent.oContained.nLastRow
							If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow()
								This.Parent.oContained.InitRow()
								This.Parent.oBody.SetFullFocus()
							Else
								This.Parent.oBeginFooter.MoveFocus()
							Endif
						Else
							This.Parent.oContained.i_lastcheckrow=This.Parent.oContained.oPgFrm.Page1.oPag.oBody.nAbsRow
							If This.Parent.oContained.oPgFrm.Page1.oPag.oBody.RelativeRow=(This.Parent.oContained.i_nRowPerPage-1)
								This.Parent.oContained.oPgFrm.Page1.oPag.oBody.DoScroll(1)
							Endif
							Skip
							This.Parent.oBody.SetFullFocus()
						Endif
					Else
						If Not(Isnull(This.Parent.oContained.oNewFocus))
							If Lower(This.Parent.oContained.oNewFocus.Parent.Name)=='orow'
								* --- il campo che ha dato errore e' nel corpo, bisogna selezionare sia il corpo che il campo
								* Focus da campi fixed position INIZIO
								Local i_obj_focus
								i_obj_focus=This.Parent.oContained.oNewFocus
								This.Parent.oBody.SetFocus()
								This.Parent.oContained.oNewFocus=i_obj_focus
								i_obj_focus=.Null.
							Endif
							* --- il campo che ha dato errore e' nella parte fissa, bisogna selezionare solo il campo
							This.Parent.oContained.oNewFocus.SetFocus()
							This.Parent.oContained.oNewFocus=.Null.
						Endif
					Endif
				Endif
			Else
				If Inlist(Lastkey(),5,15)
					This.Parent.oBody.oBodyCol.oRow.oLast.MoveFocus()
				Endif
			Endif
		Endif
		Return
	Proc MoveFocus()
		This.bMoveFocus=.T.
		This.SetFocus()
		If This.nDirection=1
			Keyboard '{TAB}'
		Else
			Keyboard '{UPARROW}'
		Endif
		This.bMoveFocus=.F.
		Return
Enddefine
* --- Gestione Bottone contestuale
Define Class btnZoom As CommandButton
	Caption = ''
	Visible = .T.
	Width = i_nZBtnWidth
	TabStop = .F.
	ObjColl = .Null.
	Picture = ''
	ToolTipText = MSG_CONTEXT_MENU
	BackColor=Rgb(206,219,255)
	ImgSize=16

	#If Version(5)>=800
		SpecialEffect = i_nBtnSpEfc
	#Endif

	Procedure Init(tObj,Ttop,tLeft,tWidth,tHeight)
		This.ObjColl = tObj
		If Empty(This.ObjColl.cLinkFile)
			If Vartype(g_BtnZoomGear)<>'C'
				Local cFileBmp, l_Dim
				l_Dim= Iif(This.ImgSize<>0, This.ImgSize, i_nBtnImgSize)
				Public g_BtnZoomGear
				If Vartype(i_ThemesManager)=='O'
					g_BtnZoomGear  = Forceext(i_ThemesManager.RetBmpFromIco('gear.bmp', m.l_Dim),'bmp')
				Else
					g_BtnZoomGear = 'gear.bmp'
				Endif
			Endif
			This.Picture= g_BtnZoomGear
		Else
			If Vartype(g_BtnZoomArrow)<>'C'
				Local cFileBmp, l_Dim
				l_Dim= Iif(This.ImgSize<>0, This.ImgSize, i_nBtnImgSize)
				Public g_BtnZoomArrow
				If Vartype(i_ThemesManager)=='O'
					g_BtnZoomArrow  = Forceext(i_ThemesManager.RetBmpFromIco('arrow.bmp', m.l_Dim),'bmp')
				Else
					g_BtnZoomArrow = 'arrow.bmp'
				Endif
			Endif
			This.Picture= g_BtnZoomArrow
		Endif
		This.Riposiziona_Btn()
		This.Enabled = .T. &&this.ObjColl.Enabled
		* Sono un control su di una griglia (propriet� ctotal vediamo)
		* se possibile fare meglio
		* se bottone di una griglia lo disabilito
		This.Visible = Type('tObj.cTotal')='U' Or tObj.Visible
		This.ToolTipText=cp_Translate(This.ToolTipText)
	Endproc

	* --- Zucchetti Aulla Inizio - Gestione Bottoncino contestuale
	* --- Al cambio di posizione/dimensione del ctrl allinea il bottone
	Procedure Riposiziona_Btn
		This.Top = This.ObjColl.Top
		This.Left = This.ObjColl.Left + This.ObjColl.Width
		This.Height = This.ObjColl.Height
	Endproc
	PROCEDURE Click
        *--- Zucchetti Aulla Inizio - Evito problema campi obbligatori e bottoni contestuali
		This._Click()
	ENDPROC

	Procedure _Click
        *--- Zucchetti Aulla Fine - Evito problema campi obbligatori e bottoni contestuali
		If Type('This.ObjColl.name')='C'
			If This.ObjColl.Enabled And Not This.ObjColl.ReadOnly And This.ObjColl.bHasZoom And (Upper(This.ObjColl.Parent.oContained.cFunction)==Upper("Edit") Or Upper(This.ObjColl.Parent.oContained.cFunction)==Upper("Load") )
				* lancio eventuale zoom...
				This.ObjColl.Parent.oContained.bDontReportError=.T.
				Public i_lastindirectaction
				i_lastindirectaction='ecpZoom'
                                *--- Zucchetti Aulla Inizio - Evito problema campi obbligatori e bottoni contestuali
				IF TYPE("Thisform.activecontrol")='O' AND !ISNULL(Thisform.ActiveControl) AND Thisform.ActiveControl<>This.ObjColl AND LOWER(This.ObjColl.Class)<>"stdtrsfield"
				   This.ObjColl.SetFocus()
				ENDIF
                                *--- Zucchetti Aulla Fine - Evito problema campi obbligatori e bottoni contestuali
				This.ObjColl.mZoom()
			Else
				This.RightClick
			Endif
		Endif

	Endproc
	* --- Zucchetti Aulla Fine - Gestione Bottoncino contestuale

	Procedure RightClick
		If Type('This.ObjColl.name')='C'
			This.ObjColl.RightClick()
		Endif

	Endproc

	*--- Zucchetti Aulla Inizio - Evito problema campi obbligatori e bottoni contestuali
	PROCEDURE MouseEnter
		LPARAMETERS nButton, nShift, nXCoord, nYCoord
		IF LOWER(This.ObjColl.Class) = "stdtrsfield"
			This.ObjColl.bOverBtnZoom=.t.
		endif
	endproc
		
	PROCEDURE MouseLeave
		LPARAMETERS nButton, nShift, nXCoord, nYCoord
		IF LOWER(This.ObjColl.Class) = "stdtrsfield"
			This.ObjColl.bOverBtnZoom=.f.
		EndIf
 	endproc	
        *--- Zucchetti Aulla Fine - Evito problema campi obbligatori e bottoni contestuali
Enddefine
*** Bottone per traduzione del dato
Define Class btnTrans As btnZoom
	Picture = ''
	ToolTipText = MSG_CONTEXT_TRANSLATE
	
	* --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
	bNoSetAnchorForm = .T.
	* --- Zucchetti Aulla Fine  - Gestione SetAnchorForm
	
	#If Version(5)>=800
		SpecialEffect = i_nBtnSpEfc
	#Endif

	Procedure Init(tObj,Ttop,tLeft,tWidth,tHeight)
		This.ObjColl = tObj
		This.Picture=i_ThemesManager.RetBmpFromIco('trans.bmp',16)+'.bmp'
		This.Riposiziona_Btn()
		This.Enabled = .T. &&this.ObjColl.Enabled
		This.Visible = Type('tObj.cTotal')='U' Or tObj.Visible
		This.ToolTipText=cp_Translate(This.ToolTipText)
	Endproc

	Procedure Click
		If Type('This.ObjColl.name')='C'
			Local i_ObjColl, i_ObjCollPar
			i_ObjColl=This.ObjColl
			i_ObjCollPar=This.ObjColl.Parent.oContained
			If Not i_ObjColl.ReadOnly And i_ObjColl.bMultilanguage And (Upper(i_ObjCollPar.cFunction)==Upper("Query") Or (Upper(i_ObjCollPar.cFunction)==Upper("Edit") And "Detail"$cp_getEntityType(i_ObjCollPar.cprg))) And i_ObjCollPar.bLoaded
				* lancio la maschera di traduzione
				This.ObjColl.Parent.oContained.bDontReportError=.T.
				cp_TranslateMask(This.ObjColl)
			Endif
		Endif
	Endproc

	* --- Al cambio di posizione/dimensione del ctrl allinea il bottone
	Procedure Riposiziona_Btn(i_btnZoom)
		This.Top = This.ObjColl.Top
		This.Left = This.ObjColl.Left + Iif(i_btnZoom=.T.,This.Width,0) + This.ObjColl.Width
		This.Height = This.ObjColl.Height
	Endproc
Enddefine

* --- Classe per definire rettangolo da disegnare attorno
* --- a campi obbligatori.
Define Class OblShape As Shape
	SpecialEffect=1
	Visible=.T.
	BackStyle=0
	BorderStyle=1
	oFather= .Null.

	Procedure Init(Ttop,tLeft,tWidth,tHeight,_Zorder,father)
		* lo sposto in secondo piano...
		If _Zorder=1
			This.ZOrder(1)
		Endif
		This.oFather=father
		This.Riposiziona()
		This.BorderColor=i_nOblColor
		This.Curvature=i_nCurvature
		This.Visible = This.oFather.Visible And This.oFather.Enabled
	Endproc

	* --- Allinea shape in caso si sposti a run-time il control
	Procedure Riposiziona
		This.Top = This.oFather.Top-1
		This.Left = This.oFather.Left -1
		This.Height = This.oFather.Height + 2
		* --- Se il control al quale � legato ha lo zoom devo includerlo nello shape
		If Type('this.oFather.bHasZoom')='L' And This.oFather.bHasZoom And i_cZBtnShw='S' And Type('this.oFather.bNoContxtBtn')='U'
			This.Width=This.oFather.Width + 2 + i_nZBtnWidth
		Else
			This.Width=This.oFather.Width + 2
		Endif
	Endproc

Enddefine

*--- Classe contenitore oBodyRow
Define Class CPBodyRowCnt as container
  proc BackStyle_Access()
	return this.BackStyle
  endproc
EndDefine

* ===================================================================================================
* cp_Spinner
* ===================================================================================================
Define Class CPSpinner As Spinner
	*forecolor=rgb(96,0,0)
	*backcolor=rgb(255,242,192)
	* propriet� per contenere il nome del shape per i campi obbligatori
	CShape=''
	* --- Gestione men� contestuale (tasto destro)
	cMenuFile=''
	zbName='' && gestione bottone contestuale
	transbName='' && Nome bottoncino traduzione del dato
	bNoRightClick = .F. && disabilita tasto dx

	bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
	bNoDisabledBackColor = .F. &&Di Default non applica la configurazione interfaccia
	SpecialEffect = i_nSpecialEffect
	BorderColor = i_nBorderColor
	DisabledForeColor = i_udisabledforecolor

	Procedure Init
		This.BackColor = Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
		#If Version(5)<800
			This.StatusBarText=Padr(This.ToolTipText,100)
		#Else
			This.StatusBarText = This.ToolTipText
		#Endif
		DoDefault()
	Endproc

	* --- Al cambio della posizione (Run-Time) del control distruggo e ricreo eventuale rettangolo
	* --- per evidenziare campo obbligatorio o bottone contestuale
	Procedure Left_Assign(xvalue)
		If m.xvalue<>This.Left
			This.Left=m.xvalue
			This.Draw_Link_Obj()
		Endif
	Endproc

	Procedure Top_Assign(xvalue)
		If m.xvalue<>This.Top
			This.Top=m.xvalue
			This.Draw_Link_Obj()
		Endif
	Endproc

	Procedure Width_Assign(xvalue)
		If m.xvalue<>This.Width
			This.Width=m.xvalue
			This.Draw_Link_Obj()
		Endif
	Endproc

	Procedure Height_Assign(xvalue)
		If m.xvalue<>This.Height
			This.Height=m.xvalue
			This.Draw_Link_Obj()
		Endif
	Endproc

	Proc Draw_Link_Obj
		Local btnName
		* --- Bottone contestuale
		Local i_btnZoom,ctShape
		i_btnZoom=.F.
		btnName='this.parent.'+This.zbName
		* --- Riposiziono bottone contestuale se presente...
		If Type(btnName)='O'
			btnName=This.zbName
			This.Parent.&btnName..Riposiziona_Btn()
			i_btnZoom=.T.
		Endif
		* --- Zucchetti Aulla fine - Bottone contestuale
		btnName='this.parent.'+This.transbName
		* --- Riposiziono bottone traduzioni...
		If Type(btnName)='O'
			btnName=This.transbName
			This.Parent.&btnName..Riposiziona_Btn(i_btnZoom)
		Endif

		* ---  Campo obbligatorio
		* --- Riposiziono shape se campo obbligatorio e se shape presente...
		If Type('this.bObbl')='L' And This.bObbl And Not Empty(This.CShape)
			* --- verifico se l'oggetto esiste...
			ctShape='this.parent.'+This.CShape
			If Type(ctShape)='O'
				ctShape=This.CShape
				This.Parent.&ctShape..Riposiziona()
			Endif
		Endif
	Endproc

	Procedure visible_assign(xvalue)
		If This.Visible <> m.xvalue
			This.Visible = m.xvalue
			* --- Campo obbligatorio
			Local CShape
			If Type('this.cShape')='C' And Not Empty(This.CShape)
				CShape = This.CShape
				* nascondo il rettangolo di campo obbligatorio
				* se campo non visible
				This.Parent.&CShape..Visible = This.Visible And This.Enabled
			Endif

			Local zbName
			zbName = This.zbName
			* --- Mostro bottone contestuale se presente...
			If Not Empty(zbName) And Type('this.bHasZoom')='L' And This.bHasZoom
				This.Parent.&zbName..Visible = m.xvalue
			Endif
			Local transbName
			transbName = This.transbName
			* --- Mostro bottone contestuale se presente...
			If Not Empty(transbName)
				This.Parent.&transbName..Visible = m.xvalue
			Endif
		Endif
	Endproc

	* --- Campo obbligatorio
	Procedure enabled_assign(xvalue)
		Local CShape
		If This.Enabled <> m.xvalue
			This.Enabled = m.xvalue
			If Type('this.cShape')='C' And Not Empty(This.CShape)
				CShape = This.CShape
				* nascondo il rettangolo di campo obbligatorio
				* se campo non editabile
				This.Parent.&CShape..Visible = This.Visible And This.Enabled
			Endif
		Endif
	Endproc

	Procedure KeyPress(nKeyCode, nShiftAltCtrl)
		Local pt
		If Vartype(This.Value)='N'
			pt=Set('point')
			Do Case
				Case nKeyCode=46 And nKeyCode<>Asc(pt)
					* Sostituzione carattere '.'
					Nodefault
					Keyboard pt
				Case nKeyCode=44 And nKeyCode<>Asc(pt)
					* Sostituzione carattere '.'
					Nodefault
					Keyboard pt
				Case nKeyCode=9
					* uscita con il tasto return, in modo da sovrascrivere sempre il contenuto
					Nodefault
					Keyboar '{ENTER}'
			Endcase
		Endif
	Endproc

	Procedure RightClick
		If !This.bNoRightClick   &&  disabilito men� contestuale
			If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
				g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
				g_oMenu.creaMenu()
				** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
				g_oMenu=.Null.
			Endif
		Endif
	Endproc

	*PROCEDURE MouseEnter(nButton, nShift, nXCoord, nYCoord)
	*  IF VARTYPE(thisform.__tb__)='O'
	*    thisform.__tb__.top=this.top+28
	*    thisform.__tb__.left=this.left+this.width-5
	*    thisform.__tb__.height=this.height
	*    thisform.__tb__.zorder(0)
	*    thisform.__tb__.visible=.t.
	*  ENDIF
	*  return
	*PROCEDURE MouseLeave(nButton, nShift, nXCoord, nYCoord)
	*  IF VARTYPE(thisform.__tb__)='O'
	*    thisform.__tb__.visible=.f.
	*  ENDIF
	*  return
Enddefine

* === Definizione Spinner Standard
Define Class StdSpinner As CPSpinner
	*
	* --- PROPRIETA' VFP
	*
	Height = 23
	Caption = ""
	* visible = .t.                          && ERRORE Per gli oggetti contenuti va settato da fuori
	FontSize   = 9
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	FontName   = ""
	*DisabledBackColor=rgb(255,255,255)
	Format='K'
	Margin=1
	*
	* --- PROPRIETA' CODEPAINTER
	*
	cQueryName = ""
	bObbl = .F.                            && Obbligatorio
	sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
	cLinkFile=''
	cOldIcon=''
	bHasZoom=.F.
	nPag=0
	cFormVar=''
	bIsInHeader=.T.
	nZero=0
	bUpd=.F.
	cSayPict=''
	cGetPict=''
	nSeconds=0
	bInInput=.F.
	* --
	cfgCalc=''
	cfgCond=''
	cfgCheck=''
	cfgDefault=''
	cfgHide=''
	cfgInit=''
	bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
	bNoMenuAction=.F.         	&& Disabilita nel tasto destro sottomenu Azioni
	bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
	bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
	bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
	bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
	bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
	bSetFont = .T. && Attiva / disattiva la possibilt� di pilotare i font
	*** Correzione Aulla Gest. Font
	bGlobalFont=.F.
	*** Fine Aulla Gest. Font

	*
	* --- COSTRUTTORE/DISTRUTTORE
	*
	* --- Inizializzazione
	Procedure Init()
		Local i_im
		* ---Add the button contextual
		* Aggiunta del bottoncino contestuale di fianco ai campi con linktable
		If Type('this.bHasZoom')='L' And This.bHasZoom
			If ((i_cZBtnShw='S' And (Type('this.bNoContxtBtn')='U' Or (Type('this.bNoContxtBtn')='N' And This.bNoContxtBtn=2 ))) Or (i_cZBtnShw='N' And Type('this.bNoContxtBtn')='N' And This.bNoContxtBtn=2 ))
				Local btnName
				This.zbName = 'z'+This.Name
				* --- Gestisco l'errore in alcuni casi va in errore perch� il Parent non � ancora stato definito
				Local l_olderr, berr
				l_olderr=On('error')
				berr=.F.
				On Error berr=.T.
				This.Parent.AddObject(This.zbName,'btnZoom',This,This.Top,This.Left,This.Width,This.Height)
				On Error &l_olderr
				If Not berr
					* Per la macro non posso utilizzare una propriet�
					btnName = This.zbName
					This.Parent.&btnName..Visible = This.Visible
					This.Width=This.Width-i_nZBtnWidth
				Endif
			Endif
		Endif
		* --- Add button translations
		If Type('this.bMultilanguage')='L' And This.bMultilanguage And Not Empty(i_aCpLangs[1])
			Local btnName
			This.transbName = 'trans'+This.Name
			* --- Gestisco l'errore in alcuni casi va in errore perch� il Parent non � ancora stato definito
			Local l_olderr, berr
			l_olderr=On('error')
			berr=.F.
			On Error berr=.T.
			This.Parent.AddObject(This.transbName,'btnTrans',This,This.Top,This.Left,This.Width,This.Height)
			On Error &l_olderr
			If Not berr
				* Per la macro non posso utilizzare una propriet�
				btnName = This.transbName
				This.Parent.&btnName..Visible = This.Visible
				This.Width=This.Width-i_nZBtnWidth
			Endif
		Endif
		* Add a red rectangle around the required fields
		If i_cHlOblColor='S' And Type('this.bObbl')='L' And This.bObbl
			Local l_olderr, berr
			l_olderr=On('error')
			On Error berr=.T.
			This.CShape='TTxOb'+This.Name
			This.Parent.AddObject(This.CShape,'OblShape',This.Top-1,This.Left-1,This.Width+2,This.Height+2,1,This)
			On Error &l_olderr
		Endif
		If Empty(This.FontName)
			This.FontName = This.Parent.FontName
			This.FontSize = This.Parent.FontSize
			This.FontBold = This.Parent.FontBold
			This.FontItalic = This.Parent.FontItalic
			This.FontUnderline  = This.Parent.FontUnderline
			This.FontStrikethru = This.Parent.FontStrikethru
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
		If Empty(This.cSayPict) And !Empty(This.cGetPict)
			This.cSayPict=This.cGetPict
		Endif
		If !Empty(This.cSayPict) And Empty(This.cGetPict)
			This.cGetPict=This.cSayPict
		Endif
		If !Empty(This.cSayPict)
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
		This.ToolTipText=cp_Translate(This.ToolTipText)
		DoDefault()
		Return

	Procedure SetFont()
		If This.bGlobalFont
			SetFont('T', This)
		Endif
	Endproc
	*
	* --- EVENTI/METODI VFP
	*
	* --- Quando prende il Focus
	Proc GotFocus()
		Local i_var,i_im,e
		Local i_oldvalue
		LOCAL TestMacro
		This.bUpd=.F.
		If This.Parent.oContained.cFunction="Query"
			* --- attiva il bottone di edit
			oCpToolBar.b2.Enabled=This.Parent.oContained.bLoaded
			* --- attiva i bottoni prev-next
			oCpToolBar.b7.Enabled=Used(This.Parent.oContained.cKeySet) And !Bof(This.Parent.oContained.cKeySet)
			oCpToolBar.b8.Enabled=Used(This.Parent.oContained.cKeySet) And !Eof(This.Parent.oContained.cKeySet)
		Endif
		* --- calcola il default
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			This.Parent.oContained.SaveDependsOn()
			This.ResetTotal()
			i_oldvalue=This.Value
			This.mDefault()
			oCpToolBar.b9.Enabled=This.bHasZoom Or Type('this.parent.oContained.'+This.cFormVar)='D'
			This.bUpd=i_oldvalue<>This.Value
		Endif
		* -- default da configurazione
		If Not(Empty(This.cfgDefault)) And Empty(This.Value)
			i_oldvalue=This.Value
			e=Strtran(This.cfgDefault,'w_','this.parent.oContained.w_')
			This.Value=&e
			This.bUpd=This.bUpd Or This.Value<>i_oldvalue
		Endif
		* --- esegue la funzione "before input"
		If This.Parent.oContained.cFunction<>"Filter"
			This.mBefore()
			* --- il valore puo' essere cambiato dalla funzione di "Before"
			i_var=This.cFormVar
			TestMacro=This.Parent.oContained.&i_var 
			If This.Value<>TestMacro And This.IsUpdated()
				This.SetControlValue()
				This.bUpd=.T.
			Endif
		Endif
		This.bInInput=.T.
		* --- setta la picture di "get"
		If !Empty(This.cGetPict)
			i_im=Trim(cp_GetMask(This.cGetPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cGetPict)
		Endif
		If !This.bNoBackColor
			This.BackColor= i_nBackColor
		Endif
		If Inlist(This.Parent.oContained.cFunction,"Load","Edit")
			If Vartype(i_var)='C'
				This.Parent.oContained.NotifyEvent(i_var+' GotFocus')
			Endif
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Inizio
		If Type('This.parent.aPageMovers')='O'
			Do While This.Top+This.Parent.Top<0 And This.Parent.aPageMovers.Count>0
				If This.Parent.aPageMovers.Count>0
					* Se non sono arrivato all'inizio della collezione sposto
					* il pageframe alla posizione precedente e elimino l'informazioni dalla collezione
					This.Parent.Top = This.Parent.aPageMovers[This.parent.aPageMovers.Count]
					This.Parent.aPageMovers.Remove(This.Parent.aPageMovers.Count)
				Endif
			Enddo
			This.Parent.Refresh()
		Endif
		* Zucchetti Aulla verifica posizione controllo per bottoni sezioni - Fine
		Return
	Func When()
		Local i_bRes,i_e
		* --- Procedure standard del campo
		i_bRes=.T.
		If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
			i_bRes = This.mCond()
			If !Empty(This.cfgCond)
				i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
				i_bRes=&i_e
			Endif
		Endif
		Return(i_bRes)
	Func Valid()
		Local i_nRes,i_bErr,i_var,i_bUpd,e,l
		i_nRes=1
		LOCAL TestMacro
		* --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
		i_var=This.cFormVar
		i_bUpd=.F.
		If This.nZero<>0
			If !Empty(This.Value) And Left(This.Value,1)$'123456789'
				l=Iif(This.nZero>0,This.nZero,Len(This.Value))
				This.Value=Right(Repl('0',l)+Alltrim(This.Value),l)
			Endif
		Endif
		If This.IsUpdated()
			i_bUpd=.T.
			This.Parent.oContained.bUpdated=.T.
			If This.bIsInHeader
				This.Parent.oContained.bHeaderUpdated=.T.
			Endif
			This.Parent.oContained.&i_var=This.Value
			This.SetModified()
		Endif
		This.bUpd=.F.
		* --- After Input solo alla variazione del campo
		If i_bUpd
			This.mAfter()
			* --- il valore puo' essere cambiato dalla funzione di "After"
			TestMacro=This.Parent.oContained.&i_var
			If This.Value<>TestMacro
				This.SetControlValue()
			Endif
		Endif
		* ---
		If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
			i_bErr=.F.
			i_nRes=Iif(i_bUpd,0,1)
			oCpToolBar.b9.Enabled=.F.
			* --- Obbligatorieta'
			If This.bObbl And Empty(This.Value) And This.CondObbl()
				If Vartype(This.Parent.oContained.w_oHeaderDetail)='O'
					This.Parent.oContained.w_oHeaderDetail.bCtrlObbl=.T.
				Endif
				If Not(This.Parent.oContained.bDontReportError)
					If type("Thisform.oBalloon")='O'
						Thisform.oBalloon.ctlShow(CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , MSG_FIELD_CANNOT_BE_NULL_QM ,  TTI_WARNING)
					Else
						Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
					Endif
				Endif
				i_nRes=0
				i_bErr=.T.
			Endif
			If Not(i_bErr) And i_bUpd
				If !Empty(This.cfgCheck)
					e=Strtran(This.cfgCheck,'w_','this.parent.oContained.w_')
					i_bErr=Not(&e)
					If i_bErr
						If Not(This.Parent.oContained.bDontReportError)
							If type("Thisform.oBalloon")='O'
								If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
									Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
								Else
									Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
								Endif
							Else
								Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
							Endif
						Endif
						This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
						This.SetControlValue()
						i_nRes=0
					Endif
				Endif
				If Not(i_bErr) And i_bUpd
					* --- Procedura di check e calcolo se il campo e' cambiato
					If This.Check()
						i_nRes=1
					Else
						If Not(This.Parent.oContained.bDontReportError)
							This.SpecialEffect=1
							This.BorderColor=Rgb(255,0,0)
							If type("Thisform.oBalloon")='O'
								If Len(Alltrim(Thisform.msgFmt(This.sErrorMsg)))>100
									Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , Thisform.msgFmt(This.sErrorMsg) + Chr(10)+Chr(13) + cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , cp_Translate(i_msgtitle) , TTI_WARNING)
								Else
									Thisform.oBalloon.ctlShow( CON_BTPOS_ACTIVECTRL , cp_Translate('Per maggiori informazioni consultare la ')+Thisform.oBalloon.ctlMakeLink('guida in linea', 'cp_Help()') , Thisform.msgFmt(This.sErrorMsg) , TTI_WARNING)
								Endif
							Else
								Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
							Endif
						Endif
						* --- azzeramento della variabile e del control
						This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
						This.SetControlValue()
					Endif
				Endif
				This.SetTotal()
				This.Parent.oContained.NotifyEvent(i_var+' Changed')
				This.Parent.oContained.mCalc(i_bUpd)
				This.Parent.oContained.SaveDependsOn()
			Else
				This.SetTotal()
			Endif
		Endif
		This.Parent.oContained.bDontReportError=.F.
		Return(i_nRes)
	Proc LostFocus()
		Local i_cFlt,i_cFile,i_nConn,i_im
		This.SpecialEffect=0
		If This.Parent.oContained.cFunction='Query'
			If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
				Nodefault
				i_cFile=This.Parent.oContained.cFile
				i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
				i_cFlt=cp_BuildKeyWhere(This.Parent.oContained,This.cQueryName,i_nConn)
				This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
				This.Parent.oContained.LoadRecWarn()
			Endif
		Endif
		If !Empty(This.cSayPict)
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
		This.bInInput=.F.
		This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
		If !This.bNoBackColor
			This.BackColor=i_nEBackColor
		Endif
		Return
	Func IsUpdated()
		If This.bUpd
			Return (.T.)
		Endif
		Local i_var,i_m
		i_var=This.cFormVar
		i_m=This.InputMask
		If Vartype(This.Value)='C'
			If Empty(i_m)
				Return(Not(Trim(This.Value)==Trim(This.Parent.oContained.&i_var)))
			Else
				* c'e una picture, il valore della varibile deve essere formattato di conseguenza
				Return(Not(Trim(Transform(This.Value,i_m))==Trim(Transform(This.Parent.oContained.&i_var,i_m))))
			Endif
		Endif
		Return(Not(This.Value==This.Parent.oContained.&i_var))
	Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
		If nButton=1
			If Seconds()-0.3<This.nSeconds
				If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
					Nodefault
					This.Parent.oContained.bDontReportError=.T.
					Public i_lastindirectaction
					i_lastindirectaction='ecpZoom'
					This.mZoom()
				Endif
			Endif
			This.nSeconds=Seconds()
		Endif
		Return
		*
		* ------ METODI CODEPAINTER
		*
		* --- Procedura di default standard del campo
	Proc mDefault()
		Return
		* --- Procedura di Condizione standard del campo
	Func mCond()
		Return (.T.)
		* --- Procedura di Hide standard del campo
	Func mHide()
		Return (.F.)
		* --- Procedura di Before input standard del campo
	Proc mBefore()
		Return
		* --- Procedura di After input standard del campo
	Proc mAfter()
		Return
		* --- Procedura di check
	Func Check()
		Return(.T.)
	Proc mZoom()
		If Type('this.parent.oContained.'+This.cFormVar)='D'
			cp_zdate(This)
		Endif
		Return
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
        	If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			Do Case
				Case nState=0   && Enter
					This.cOldIcon=oSource.DragIcon
					If oSource.cFile==This.cLinkFile
						oSource.DragIcon=i_cBmpPath+"cross01.cur"
					Endif
				Case nState=1   && Leave
					oSource.DragIcon=This.cOldIcon
			Endcase
		Endif
		Return
	Proc DragDrop(oSource, nXCoord, nYCoord)
        	If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And (Type("oSource.oContained.bLoaded")='L' and oSource.oContained.bLoaded)
			If oSource.cFile==This.cLinkFile
				This.ecpDrop(oSource)
			Endif
		Endif
		Return
	Proc ecpDrop(oSource)
		Return
	Proc SetModified()
		Return
	Proc ResetTotal()
		Return
	Proc SetTotal()
		Return
	Proc SetControlValue()
		Local i_var,i_cs
		i_var=This.cFormVar
		i_cs=This.ControlSource
		This.Value=This.Parent.oContained.&i_var
		If !(Empty(i_cs))
			Select (This.Parent.oContained.cTrsName)
			Replace &i_cs With This.Value
		Endif
		Return
	Proc ProgrammaticChange()
		* --- questa routine gestisce le picture dinamiche
		Local i_im
		If !This.bInInput And !Empty(This.cSayPict) And !(Left(This.cSayPict,1)$["'])
			* wait window "calcola picture "+this.name nowait
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
	Function CondObbl()
		Return (.T.)
	Proc ProgrammaticChange()
		* --- questa routine gestisce le picture dinamiche
		Local i_im
		If !This.bInInput And !Empty(This.cSayPict) And !(Left(This.cSayPict,1)$["'])
			* wait window "calcola picture "+this.name nowait
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
Enddefine
* --- FINE ---
