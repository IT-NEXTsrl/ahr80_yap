* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_askcolumntitler                                              *
*              Routine di riempimento elenco campi zoom                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-01-24                                                      *
* Last revis.: 2016-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_askcolumntitler",oParentObject,m.pParam)
return(i_retval)

define class tcp_askcolumntitler as StdBatch
  * --- Local variables
  pParam = space(10)
  w_OBJGRID = .NULL.
  w_POSITION = 0
  w_ControlSource = space(50)
  w_IdxField = 0
  w_FIELDNAME = space(100)
  w_IdxcField = 0
  w_OKCHECK = .f.
  w_FLDSELECTED = 0
  w_IdColumZoom = 0
  w_bOpenADVPAGE = .f.
  w_bOpenADVPAGE = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    local n, i
    do case
      case this.pParam="Init"
        this.w_OBJGRID = this.oParentObject.oParentObject.Parent.Parent
        ZAP IN ( this.oParentObject.w_ZoomTitle.cCursor )
        For i=1 To this.w_OBJGRID.ColumnCount
        N=Alltrim(Str(i))
        if Type("this.oParentObject.oParentObject.Parent.Parent.column"+N)="O"
          this.w_ControlSource = this.w_OBJGRID.column&N..ControlSource
          if UPPER(this.w_ControlSource)<>"XCHK"
            if Left(this.w_ControlSource,17)="cp_MemoFirstLine("
              this.w_ControlSource = cp_ControlSourceMemo(this.w_ControlSource)
            endif
            if this.w_ControlSource==iif(left(this.oParentObject.oParentObject.Parent.ControlSource, 17)="cp_MemoFirstLine(", cp_ControlSourceMemo(this.oParentObject.oParentObject.Parent.ControlSource), this.oParentObject.oParentObject.Parent.ControlSource)
              this.w_POSITION = i
            endif
            Insert into (this.oParentObject.w_ZoomTitle.cCursor) ( ; 
 cFldZoom,cTitle, ; 
 cLabelFont,nDimFont, ; 
 bItalicFont,bBoldFont,; 
 cLabelFontC,nDimFontC, ; 
 bItalicFontC,bBoldFontC, ; 
 cFormat,nHeight,nWidth, ; 
 bIsImage,cForce,cBack, ; 
 check1,cHyperLink,cHyperTarget, ; 
 cLayerContent,nColumnType,cToolTip,IsFldInZoom,xchk) ; 
 values (this.w_ControlSource,; 
 this.w_OBJGRID.column&N..hdr.Caption,; 
 this.w_OBJGRID.column&N..hdr.cLabelFont,; 
 this.w_OBJGRID.column&N..hdr.nDimFont,; 
 iif(this.w_OBJGRID.column&N..hdr.bItalicFont, "S", " "),; 
 iif(this.w_OBJGRID.column&N..hdr.bBoldFont, "S", " "),; 
 this.w_OBJGRID.column&N..cLabelFontC,; 
 this.w_OBJGRID.column&N..nDimFontC,; 
 iif(this.w_OBJGRID.column&N..bItalicFontC, "S", " "),; 
 iif(this.w_OBJGRID.column&N..bBoldFontC, "S", " "),; 
 this.w_OBJGRID.column&N..Tag,; 
 this.w_OBJGRID.column&N..hdr.nHeight,; 
 this.w_OBJGRID.column&N..Width,; 
 iif(this.w_OBJGRID.column&N..hdr.bIsImage, "S", " "),; 
 this.w_OBJGRID.column&N..DynamicForeColor,; 
 this.w_OBJGRID.column&N..DynamicBackColor,; 
 iif(this.w_OBJGRID.column&N..rw=1, "S", " "),; 
 this.w_OBJGRID.column&N..hdr.cHyperLink,; 
 this.w_OBJGRID.column&N..hdr.cHyperLinkTarget,; 
 this.w_OBJGRID.column&N..hdr.cLayerContent,; 
 this.w_OBJGRID.column&N..hdr.nColumnType,; 
 this.w_OBJGRID.column&N..hdr.cTooltip,1,1)
          endif
        endif
        Endfor
        if vartype(this.w_OBJGRID.Parent.oCpQuery)<>"O" or isnull(this.w_OBJGRID.Parent.oCpQuery)
          i_nidx=i_dcx.GetTableIdx(this.w_OBJGRID.Parent.cFile)
          FOR this.w_IdxField=1 to i_dcx.GetFieldsCount(this.w_OBJGRID.Parent.cFile,i_nidx)
          this.w_FIELDNAME = UPPER(i_dcx.GetFieldName(this.w_OBJGRID.Parent.cFile,this.w_IdxField,i_nidx))
          this.w_IdxcField = ASCAN(this.w_OBJGRID.Parent.cFields, this.w_FIELDNAME,-1,-1,-1,6)
          if this.w_IdxcField=0 or this.w_IdxcField>this.w_OBJGRID.ColumnCount
            Insert into (this.oParentObject.w_ZoomTitle.cCursor) ( ; 
 cFldZoom,cTitle, cLabelFont,nDimFont, ; 
 bItalicFont,bBoldFont,; 
 cLabelFontC,nDimFontC, ; 
 bItalicFontC,bBoldFontC,; 
 cFormat,nHeight,nWidth, ; 
 bIsImage,cForce,cBack, ; 
 check1,cHyperLink,cHyperTarget, ; 
 cLayerContent,nColumnType,cToolTip,IsFldInZoom,xchk) ; 
 values (this.w_FIELDNAME,; 
 i_DCX.GetFieldDescr(this.w_OBJGRID.Parent.cFile,this.w_IdxField),; 
 "",0," "," ","",0," "," ","",0,0," ","",""," ","","","",0,"",0,0)
          endif
          Endfor
        else
          FOR this.w_IdxField=1 to this.w_OBJGRID.Parent.oCpQuery.nFields
          this.w_IdxcField = ASCAN(this.w_OBJGRID.Parent.cFields, UPPER(this.w_OBJGRID.Parent.oCpQuery.i_Fields[this.w_IdxField,2]),-1,-1,-1,6)
          if this.w_IdxcField=0 or this.w_IdxcField>this.w_OBJGRID.ColumnCount
            Insert into (this.oParentObject.w_ZoomTitle.cCursor) ( ; 
 cFldZoom,cTitle, cLabelFont,nDimFont, ; 
 bItalicFont,bBoldFont,; 
 cLabelFontC,nDimFontC, ; 
 bItalicFontC,bBoldFontC,; 
 cFormat,nHeight,nWidth, ; 
 bIsImage,cForce,cBack, ; 
 check1,cHyperLink,cHyperTarget, ; 
 cLayerContent,nColumnType,cToolTip,IsFldInZoom,xchk) ; 
 values (UPPER(this.w_OBJGRID.Parent.oCpQuery.i_Fields[this.w_IdxField,2]),; 
 this.w_OBJGRID.Parent.oCpQuery.i_Fields[this.w_IdxField,6],; 
 "",0,"","","",0,"","","",0,0,"","","","","","","",0,"",0,0)
          endif
          Endfor
        endif
        this.oParentObject.w_ZoomTitle.grd.Column3.Enabled = this.w_OBJGRID.Parent.bQueryOnDblClick and not this.w_OBJGRID.Parent.bAddedCheckBox and this.w_OBJGRID.Parent.adv.bsec1
        this.oParentObject.w_ZoomTitle.DisableSelMenu = not this.oParentObject.w_ZoomTitle.grd.Column3.Enabled
        Select (this.oParentObject.w_ZoomTitle.cCursor)
        if this.w_POSITION=0
          GO TOP
        else
          GO this.w_POSITION
        endif
      case this.pParam="SaveInfo"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParam="SaveAll"
        this.w_OKCHECK = .f.
        this.w_OBJGRID = this.oParentObject.oParentObject.Parent.Parent
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_OKCHECK
          Select (this.oParentObject.w_ZoomTitle.cCursor) 
 go top 
 COUNT FOR xchk=1 TO this.w_FLDSELECTED
          if this.w_FLDSELECTED=0
            cp_ErrorMsg("Impossibile eliminare tutte le colonne dallo zoom")
          else
            if this.w_OKCHECK
              Select (this.oParentObject.w_ZoomTitle.cCursor)
              SCAN FOR xchk<>IsFldInZoom
              this.w_OBJGRID.Parent.bChangedFld = .t.
              if xchk=1
                this.w_OBJGRID.Parent.nFields = this.w_OBJGRID.Parent.nFields + 1
                dimension this.w_OBJGRID.Parent.cFields(this.w_OBJGRID.Parent.nFields)
                this.w_OBJGRID.Parent.cFields(this.w_OBJGRID.Parent.nFields) = ALLTRIM(cFldZoom)
              else
                this.w_IdColumZoom = Ascan(this.w_OBJGRID.Parent.cFields,Alltrim(cFldZoom),-1,-1,-1,6)
                if this.w_IdColumZoom>0
                  this.w_OBJGRID.Parent.DeleteColumn(this.w_IdColumZoom)     
                endif
              endif
              replace IsFldInZoom with xchk
              ENDSCAN
              if this.w_OBJGRID.Parent.bChangedFld
                this.w_bOpenADVPAGE = .f.
                if type("this.w_OBJGRID.Parent.ADVPAGE")<>"O"
                  if this.w_OBJGRID.Parent.bQueryOnDblClick and this.w_OBJGRID.Parent.adv.bsec1
                    this.w_OBJGRID.Parent.ADV.Click(.t.)     
                    this.w_bOpenADVPAGE = .t.
                  endif
                endif
                this.w_OBJGRID.Parent.ADVPAGE.FillSQL()     
                this.w_OBJGRID.Parent.RefreshParamWnd()     
                this.w_OBJGRID.Parent.Qry.Click()     
                if this.w_bOpenADVPAGE
                  if this.w_OBJGRID.Parent.bQueryOnDblClick and this.w_OBJGRID.Parent.adv.bsec1
                    this.w_OBJGRID.Parent.ADV.Click()     
                  endif
                endif
              endif
              Select (this.oParentObject.w_ZoomTitle.cCursor)
              SCAN
              this.w_IdColumZoom = Ascan(this.w_OBJGRID.Parent.cFields,Alltrim(cFldZoom),-1,-1,-1,6)
              N=Alltrim(Str(this.w_IdColumZoom))
              if TYPE("this.w_OBJGRID.column"+Alltrim(Str(this.w_IdColumZoom)))="O"
                this.w_OBJGRID.column&N..hdr.Caption=alltrim(cTitle) 
 this.w_OBJGRID.column&N..hdr.cLabelFont=alltrim(cLabelFont) 
 this.w_OBJGRID.column&N..hdr.nDimFont=nDimFont 
 this.w_OBJGRID.column&N..hdr.bItalicFont=(bItalicFont="S") 
 this.w_OBJGRID.column&N..hdr.bBoldFont=(bBoldFont="S") 
 this.w_OBJGRID.column&N..cLabelFontC=alltrim(cLabelFontC) 
 this.w_OBJGRID.column&N..nDimFontC=nDimFontC 
 this.w_OBJGRID.column&N..bItalicFontC=(bItalicFontC="S") 
 this.w_OBJGRID.column&N..bBoldFontC=(bBoldFontC="S") 
 this.w_OBJGRID.column&N..Tag=alltrim(cFormat) 
 this.w_OBJGRID.column&N..hdr.nHeight=nHeight 
 this.w_OBJGRID.column&N..Width=nWidth 
 this.w_OBJGRID.column&N..hdr.bIsImage=(bIsImage="S") 
 this.w_OBJGRID.column&N..DynamicForeColor=alltrim(cForce) 
 this.w_OBJGRID.column&N..DynamicBackColor=alltrim(cBack) 
 this.w_OBJGRID.column&N..rw=iif(check1="S",1,0) 
 this.w_OBJGRID.column&N..hdr.cHyperLink=alltrim(cHyperLink) 
 this.w_OBJGRID.column&N..hdr.cHyperLinkTarget=alltrim(cHyperTarget) 
 this.w_OBJGRID.column&N..hdr.cLayerContent=alltrim(cLayerContent) 
 this.w_OBJGRID.column&N..hdr.nColumnType=nColumnType 
 this.w_OBJGRID.column&N..hdr.cTooltip=alltrim(cTooltip)
              endif
              ENDSCAN
              this.w_OBJGRID.Parent.savecolumnstatus()     
              this.w_bOpenADVPAGE = .f.
              if type("this.w_OBJGRID.Parent.ADVPAGE")<>"O"
                if this.w_OBJGRID.Parent.bQueryOnDblClick and this.w_OBJGRID.Parent.adv.bsec1
                  this.w_OBJGRID.Parent.ADV.Click(.t.)     
                  this.w_bOpenADVPAGE = .t.
                endif
              endif
              this.w_OBJGRID.Parent.Qry.Click(.t.)     
              if this.w_bOpenADVPAGE
                if this.w_OBJGRID.Parent.bQueryOnDblClick and this.w_OBJGRID.Parent.adv.bsec1
                  this.w_OBJGRID.Parent.ADV.Click()     
                endif
              endif
              this.oParentObject.ecpsave()
            endif
          endif
        endif
      case this.pParam="CheckKey"
        this.w_OBJGRID = this.oParentObject.oParentObject.Parent.Parent
        Select (this.oParentObject.w_ZoomTitle.cCursor)
        if ALLTRIM(cFldZoom)$this.w_OBJGRID.Parent.cKeyFields
          if NOT cp_YesNo(cp_MsgFormat(cp_Translate(cp_RetrieveMacroVcx("MSG__BELONGS_TO_PRIMARY_KEY_F_DRAG_AND_DROP_WILL_BE_DISABLED_C_CONTINUE_QP")),ALLTRIM(cFldZoom)),,.f.)
            this.oParentObject.w_ZoomTitle.grd.Column3.Chk.Value = 1
            replace xchk with 1
            this.oParentObject.w_ZoomTitle.grd.Refresh()     
            this.bUpdateParentObject=.f.
          endif
        endif
      case this.pParam="CheckAllKey"
        this.w_OBJGRID = this.oParentObject.oParentObject.Parent.Parent
        Select (this.oParentObject.w_ZoomTitle.cCursor)
        this.w_POSITION = RECNO()
        SCAN FOR xchk=0 AND ALLTRIM(cFldZoom)$this.w_OBJGRID.Parent.cKeyFields
        if Not Cp_YesNo(cp_MsgFormat(cp_Translate(cp_RetrieveMacroVcx("MSG__BELONGS_TO_PRIMARY_KEY_F_DRAG_AND_DROP_WILL_BE_DISABLED_C_CONTINUE_QP")),ALLTRIM(cFldZoom)),,.f.)
          this.oParentObject.w_ZoomTitle.grd.Column3.Chk.Value = 1
          replace xchk with 1
          this.oParentObject.w_ZoomTitle.grd.Refresh()     
          this.bUpdateParentObject=.f. 
 EXIT
        else
          EXIT
        endif
        ENDSCAN
        Select (this.oParentObject.w_ZoomTitle.cCursor) 
 GO this.w_POSITION
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Select (this.oParentObject.w_ZoomTitle.cCursor)
    this.w_POSITION = RECNO()
    Select (this.oParentObject.w_ZoomTitle.cCursor) 
 LOCATE FOR this.oParentObject.w_cFld_old==cFldZoom
    if this.oParentObject.CheckForm()
      this.w_OKCHECK = .t.
      replace cTitle with this.oParentObject.w_cTitle 
 replace cLabelFont with this.oParentObject.w_cLabelFont 
 replace nDimFont with this.oParentObject.w_nDimFont 
 replace bItalicFont with this.oParentObject.w_bItalicFont 
 replace bBoldFont with this.oParentObject.w_bBoldFont 
 replace cLabelFontC with this.oParentObject.w_cLabelFontC 
 replace nDimFontC with this.oParentObject.w_nDimFontC 
 replace bItalicFontC with this.oParentObject.w_bItalicFontC 
 replace bBoldFontC with this.oParentObject.w_bBoldFontC 
 replace cFormat with this.oParentObject.w_cFormat 
 replace nHeight with this.oParentObject.w_nHeight 
 replace nWidth with this.oParentObject.w_nWidth 
 replace bIsImage with this.oParentObject.w_bIsImage 
 replace cForce with this.oParentObject.w_cForce 
 replace cBack with this.oParentObject.w_cBack 
 replace check1 with this.oParentObject.w_check1 
 replace cHyperLink with this.oParentObject.w_cHyperLink 
 replace cHyperTarget with this.oParentObject.w_cHyperTarget 
 replace cLayerContent with this.oParentObject.w_cLayerContent 
 replace nColumnType with this.oParentObject.w_nColumnType 
 replace cToolTip with this.oParentObject.w_cToolTip
      if RECNO()=1 AND (this.oParentObject.w_ApplyAllColumn="S" OR this.oParentObject.w_ApplyAllColumnC="S")
        SCAN
        if this.oParentObject.w_ApplyAllColumn="S"
          replace cLabelFont with this.oParentObject.w_cLabelFont 
 replace nDimFont with this.oParentObject.w_nDimFont 
 replace bItalicFont with this.oParentObject.w_bItalicFont 
 replace bBoldFont with this.oParentObject.w_bBoldFont
        endif
        if this.oParentObject.w_ApplyAllColumnC="S"
          replace cLabelFontC with this.oParentObject.w_cLabelFontC 
 replace nDimFontC with this.oParentObject.w_nDimFontC 
 replace bItalicFontC with this.oParentObject.w_bItalicFontC 
 replace bBoldFontC with this.oParentObject.w_bBoldFontC
        endif
        ENDSCAN
      endif
      Select (this.oParentObject.w_ZoomTitle.cCursor) 
 GO this.w_POSITION
    else
      Select (this.oParentObject.w_ZoomTitle.cCursor) 
 LOCATE FOR this.oParentObject.w_cFld_old==cFldZoom 
 this.oParentObject.w_cFld=this.oParentObject.w_cFld_old
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
