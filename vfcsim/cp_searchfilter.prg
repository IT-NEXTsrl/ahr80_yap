* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_searchfilter                                                 *
*              Gestione filtro e ricerca zoom                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-12-20                                                      *
* Last revis.: 2016-10-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_searchfilter",oParentObject))

* --- Class definition
define class tcp_searchfilter as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 230
  Height = 8
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-10-04"
  HelpContextID=21481003
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_searchfilter"
  cComment = "Gestione filtro e ricerca zoom"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CtrlType = space(1)
  w_CHECKED = .F.
  o_CHECKED = .F.
  w_SearchValue = space(100)
  o_SearchValue = space(100)
  w_SearchValueN = 0
  o_SearchValueN = 0
  w_SearchValueD = ctot('')
  o_SearchValueD = ctot('')
  w_Searchwrap = space(1)
  w_TypeFilter = space(10)
  w_TypeFilter1 = space(10)
  o_TypeFilter1 = space(10)
  w_RemPrevFilter = space(1)
  w_ExpandForm = 0
  w_SecondSearch = .F.
  w_FILTER = .F.
  w_ZoomValueN = .NULL.
  w_ZoomValue = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_searchfilter
  Top    = -1000
  Left   = -1000
  *Left=_Screen.Width + 100
  *ShowWindow=1
  AlwaysOnTop=.t.
  Closable=.f.
  TitleBar=0
  * --- disabilito il men� sulla barra
  ControlBox = .f.
  * ---- disabilito l'ingrandimento della maschera
  MinButton=.f.
  MaxButton=.f.
  BorderStyle=2
  *--- Form di sistema non modificabile da window menu
  bSysForm = .T.
  
  *-- INTERNAL USE: Determines whether or not this form should be be released when the deactivate even fires.
  	rundeactivaterelease = .T.
  *-- INTERNAL USE: Used to tell the class whether the mouse (cursor) is within the boundaries of the Search/Filter form or not.
  	_inform = .T.
  *-- INTERNAL USE: Used to tell the class whether the form is fading or not.
  	_infademode = .T.
  
  *-- INTERNAL USE: Holds the SECONDS since midnight when the last user interaction with this form occurred. Ensures that fading of the form will not occur while the user is actively using the form. See the FadeWait property of the GridExtra class.
  	lastuseraction = 0
    
  *-- INTERNAL USE: Determines whether or not this form should be be released when the deactivate even fires.
    rundeactivaterelease = .T.
    
  *-- imposto di non sizare la form secondo le impostazioni di interfaccia
  bCheckSize=.t.  
    
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_searchfilterPag1","cp_searchfilter",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSearchValue_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_searchfilter
    this.Pages(1).oPag.addobject("oStr_StringAdvanced","cp_StringAdvanced")      
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomValueN = this.oPgFrm.Pages(1).oPag.ZoomValueN
    this.w_ZoomValue = this.oPgFrm.Pages(1).oPag.ZoomValue
    DoDefault()
    proc Destroy()
      this.w_ZoomValueN = .NULL.
      this.w_ZoomValue = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- cp_searchfilter
    This.Height=40
    This.oPgFrm.Height=40
    this.left=this.oParentObject.Parent.Parent.oRefHeaderObject.orefFORM.Left+objToClient(this.oparentObject.arefheaders[1],2)+;
              iif(type("_SCREEN.TBMDI.Visible")='L' AND _SCREEN.TBMDI.Visible,0, IIF(this.oParentObject.Parent.Parent.oRefHeaderObject.orefFORM.BorderStyle=3 and !this.oParentObject.Parent.Parent.oRefHeaderObject.orefFORM.bApplyFormDecorator,SYSMETRIC(3),0))
    if this.left<_screen.Width and this.left+This.width>_screen.Width
       this.left=_screen.Width-This.width-4
    endif
    this.nOldLeft=this.left
    this.top=this.oParentObject.Parent.Parent.oRefHeaderObject.orefFORM.Top+objToClient(this.oparentObject.arefheaders[1],1)+; 
             this.oparentObject.arefheaders[1].height+iif(type("_SCREEN.TBMDI.Visible")='L' AND _SCREEN.TBMDI.Visible,0, ;
             IIF(this.oParentObject.Parent.Parent.oRefHeaderObject.orefFORM.BorderStyle=3 and !this.oParentObject.Parent.Parent.oRefHeaderObject.orefFORM.bApplyFormDecorator,SYSMETRIC(4),0)+;
             IIF(this.oParentObject.Parent.Parent.oRefHeaderObject.orefFORM.Titlebar=1,SYSMETRIC(9),0))
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CtrlType=space(1)
      .w_CHECKED=.f.
      .w_SearchValue=space(100)
      .w_SearchValueN=0
      .w_SearchValueD=ctot("")
      .w_Searchwrap=space(1)
      .w_TypeFilter=space(10)
      .w_TypeFilter1=space(10)
      .w_RemPrevFilter=space(1)
      .w_ExpandForm=0
      .w_SecondSearch=.f.
      .w_FILTER=.f.
        .w_CtrlType = 'C'
          .DoRTCalc(2,5,.f.)
        .w_Searchwrap = 'S'
        .w_TypeFilter = iif(.w_CHECKED, '=', iif(EMPTY(.w_TypeFilter), IIF(g_RicPerCont='T', 'Like %', 'Like'), .w_TypeFilter))
        .w_TypeFilter1 = iif(.w_CHECKED OR EMPTY(.w_TypeFilter1), '=', .w_TypeFilter1)
        .w_RemPrevFilter = 'S'
        .w_ExpandForm = 1
      .oPgFrm.Page1.oPag.ZoomValueN.Calculate()
      .oPgFrm.Page1.oPag.ZoomValue.Calculate()
        .w_SecondSearch = .f.
        .w_FILTER = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_searchfilter
      if TYPE ("this.oParentObject.parent.parent.parent.parent.parent.ozoomelenco") ="O"
         this.oParentObject.parent.parent.parent.parent.parent.briposiz=.T.
      endif
    endproc
    
    PROCEDURE Deactivate
    		IF !this.RunDeactivateRelease
    			RETURN
    		ENDIF
      this.Notifyevent ("ExecQuery")
    		this.RELEASE()
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_CHECKED<>.w_CHECKED
            .w_TypeFilter = iif(.w_CHECKED, '=', iif(EMPTY(.w_TypeFilter), IIF(g_RicPerCont='T', 'Like %', 'Like'), .w_TypeFilter))
        endif
        if .o_CHECKED<>.w_CHECKED
            .w_TypeFilter1 = iif(.w_CHECKED OR EMPTY(.w_TypeFilter1), '=', .w_TypeFilter1)
        endif
        if .o_TypeFilter1<>.w_TypeFilter1
          .Calculate_DQJXHCMTBC()
        endif
        .oPgFrm.Page1.oPag.ZoomValueN.Calculate()
        .oPgFrm.Page1.oPag.ZoomValue.Calculate()
        .DoRTCalc(9,10,.t.)
        if .o_SearchValueD<>.w_SearchValueD.or. .o_SearchValueN<>.w_SearchValueN.or. .o_SearchValue<>.w_SearchValue
            .w_SecondSearch = .f.
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomValueN.Calculate()
        .oPgFrm.Page1.oPag.ZoomValue.Calculate()
    endwith
  return

  proc Calculate_MEZADULZWN()
    with this
          * --- Assign type variabile filter/search
          .w_CtrlType = cp_SearchFilterR(this, "Type")
          .w_TypeFilter = iif(.w_CTRLTYPE='C', IIF(g_RicPerCont='T', 'Like %', 'Like'), '=')
    endwith
  endproc
  proc Calculate_DQJXHCMTBC()
    with this
          * --- Assign type filter numeric/data
          .w_TypeFilter = .w_TypeFilter1
    endwith
  endproc
  proc Calculate_CGMNTNQRZM()
    with this
          * --- Carica zoom
          cp_SearchFilterR(this;
              ,"LoadZoom";
             )
    endwith
  endproc
  proc Calculate_LBXNOJPNRF()
    with this
          * --- Seleziona/Deseleziona record
          cp_SearchFilterR(this;
              ,"Checked";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSearchValue_1_4.enabled = this.oPgFrm.Page1.oPag.oSearchValue_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSearchValueN_1_5.enabled = this.oPgFrm.Page1.oPag.oSearchValueN_1_5.mCond()
    this.oPgFrm.Page1.oPag.oSearchValueD_1_6.enabled = this.oPgFrm.Page1.oPag.oSearchValueD_1_6.mCond()
    this.oPgFrm.Page1.oPag.oSearchwrap_1_11.enabled = this.oPgFrm.Page1.oPag.oSearchwrap_1_11.mCond()
    this.oPgFrm.Page1.oPag.oTypeFilter_1_12.enabled = this.oPgFrm.Page1.oPag.oTypeFilter_1_12.mCond()
    this.oPgFrm.Page1.oPag.oTypeFilter1_1_13.enabled = this.oPgFrm.Page1.oPag.oTypeFilter1_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSearchValue_1_4.visible=!this.oPgFrm.Page1.oPag.oSearchValue_1_4.mHide()
    this.oPgFrm.Page1.oPag.oSearchValueN_1_5.visible=!this.oPgFrm.Page1.oPag.oSearchValueN_1_5.mHide()
    this.oPgFrm.Page1.oPag.oSearchValueD_1_6.visible=!this.oPgFrm.Page1.oPag.oSearchValueD_1_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oTypeFilter_1_12.visible=!this.oPgFrm.Page1.oPag.oTypeFilter_1_12.mHide()
    this.oPgFrm.Page1.oPag.oTypeFilter1_1_13.visible=!this.oPgFrm.Page1.oPag.oTypeFilter1_1_13.mHide()
    this.oPgFrm.Page1.oPag.oRemPrevFilter_1_14.visible=!this.oPgFrm.Page1.oPag.oRemPrevFilter_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_MEZADULZWN()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZoomValueN.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomValue.Event(cEvent)
        if lower(cEvent)==lower("LoadZoom")
          .Calculate_CGMNTNQRZM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZoomValue row checked") or lower(cEvent)==lower("w_ZoomValue row unchecked") or lower(cEvent)==lower("w_ZoomValueN row checked") or lower(cEvent)==lower("w_ZoomValueN row unchecked") or lower(cEvent)==lower("ZoomValue menucheck") or lower(cEvent)==lower("ZoomValueN menucheck")
          .Calculate_LBXNOJPNRF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- cp_searchfilter
         **Gli zoom asincroni disattivano la conessione quando viene aperto un altro form percio bisogna rilanziare la requery per riattivare  la conessione
           
      IF (UPPER (cevent)='DONE' Or cevent="ExecQuery") and vartype(this.oParentObject)='O' and this.oParentObject.Parent.Parent.Name='AUTOZOOM'
                  LOCAL posiz, numrec
                  SELECT (this.oParentObject.Parent.Parent.ccursor) 
                  numrec=RECCOUNT()
                  posiz=RECNO()
         *riesegue l query solo per gli zoom asincroni tranne il caso in cui si esce con il bottone filter (il bottone filter riesegue sempre la query)   
         IF  this.oParentObject.parent.parent.bOptions AND Not this.w_CHECKED AND not this.w_FILTER
                  SELECT (this.oParentObject.Parent.Parent.ccursor) 
                  this.oParentObject.Parent.Parent.DeferredQuery()
                  SELECT (this.oParentObject.Parent.Parent.ccursor)     
                   IF posiz>0 AND posiz<=numrec AND numrec>0
                     GOTO posiz
                   ENDIF
          ENDIF
           
      ENDIF
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSearchValue_1_4.value==this.w_SearchValue)
      this.oPgFrm.Page1.oPag.oSearchValue_1_4.value=this.w_SearchValue
    endif
    if not(this.oPgFrm.Page1.oPag.oSearchValueN_1_5.value==this.w_SearchValueN)
      this.oPgFrm.Page1.oPag.oSearchValueN_1_5.value=this.w_SearchValueN
    endif
    if not(this.oPgFrm.Page1.oPag.oSearchValueD_1_6.value==this.w_SearchValueD)
      this.oPgFrm.Page1.oPag.oSearchValueD_1_6.value=this.w_SearchValueD
    endif
    if not(this.oPgFrm.Page1.oPag.oSearchwrap_1_11.RadioValue()==this.w_Searchwrap)
      this.oPgFrm.Page1.oPag.oSearchwrap_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTypeFilter_1_12.RadioValue()==this.w_TypeFilter)
      this.oPgFrm.Page1.oPag.oTypeFilter_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTypeFilter1_1_13.RadioValue()==this.w_TypeFilter1)
      this.oPgFrm.Page1.oPag.oTypeFilter1_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRemPrevFilter_1_14.RadioValue()==this.w_RemPrevFilter)
      this.oPgFrm.Page1.oPag.oRemPrevFilter_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CHECKED = this.w_CHECKED
    this.o_SearchValue = this.w_SearchValue
    this.o_SearchValueN = this.w_SearchValueN
    this.o_SearchValueD = this.w_SearchValueD
    this.o_TypeFilter1 = this.w_TypeFilter1
    return

enddefine

* --- Define pages as container
define class tcp_searchfilterPag1 as StdContainer
  Width  = 229
  height = 81
  stdWidth  = 229
  stdheight = 81
  resizeXpos=135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSearchValue_1_4 as StdField with uid="DIMWKFNASX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SearchValue", cQueryName = "SearchValue",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 238205578,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=3, Top=1, InputMask=replicate('X',100)

  func oSearchValue_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oSearchValue_1_4.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE<>'C')
    endwith
  endfunc

  add object oSearchValueN_1_5 as StdField with uid="ODBRSHKJJI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SearchValueN", cQueryName = "SearchValueN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 238525066,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=3, Top=1, cSayPict='"9999999999999.9999"', cGetPict='"9999999999999.9999"'

  func oSearchValueN_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oSearchValueN_1_5.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE<>'N')
    endwith
  endfunc

  add object oSearchValueD_1_6 as StdField with uid="YGGDOAVEMR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SearchValueD", cQueryName = "SearchValueD",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 238484106,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=3, Top=1, bNoZoomDateType=.t.

  func oSearchValueD_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oSearchValueD_1_6.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE<>'D')
    endwith
  endfunc


  add object oBtn_1_7 as StdButton with uid="BDBFGXXSGH",left=150, top=1, width=25,height=25,;
    CpPicture="find.ico", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 137514446;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        cp_SearchFilterR(this.Parent.oContained,"Search")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CtrlType='C' and not empty(.w_SearchValue) OR .w_CtrlType<>'C')
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="NTXGYAVXWY",left=177, top=1, width=25,height=25,;
    CpPicture="filter.ico", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire il filtro";
    , HelpContextID = 145848577;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        cp_SearchFilterR(this.Parent.oContained,"Filter")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT .w_CHECKED)
      endwith
    endif
  endfunc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not .oParentObject.parent.parent.bOptions OR .w_CHECKED)
     endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="TVJQHCICBA",left=177, top=1, width=25,height=25,;
    CpPicture="filter.ico", caption="", nPag=1;
    , HelpContextID = 145848577;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        cp_SearchFilterR(this.Parent.oContained,"Select")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CHECKED)
      endwith
    endif
  endfunc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not .oParentObject.parent.parent.bOptions OR NOT .w_CHECKED)
     endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="BIRMKPXGII",left=204, top=1, width=25,height=25,;
    CpPicture="esc.ico", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 21938336;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSearchwrap_1_11 as StdCheck with uid="LQXXRUPICT",rtseq=6,rtrep=.f.,left=3, top=40, caption="Torna all'inizio",;
    ToolTipText = "Torna all'inizio se raggiunta la fine",;
    HelpContextID = 13478609,;
    cFormVar="w_Searchwrap", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSearchwrap_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSearchwrap_1_11.GetRadio()
    this.Parent.oContained.w_Searchwrap = this.RadioValue()
    return .t.
  endfunc

  func oSearchwrap_1_11.SetRadio()
    this.Parent.oContained.w_Searchwrap=trim(this.Parent.oContained.w_Searchwrap)
    this.value = ;
      iif(this.Parent.oContained.w_Searchwrap=='S',1,;
      0)
  endfunc

  func oSearchwrap_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CtrlType='C' and not empty(.w_SearchValue) OR .w_CtrlType<>'C')
    endwith
   endif
  endfunc


  add object oTypeFilter_1_12 as StdCombo with uid="SJFGANFPZK",rtseq=7,rtrep=.f.,left=3,top=59,width=111,height=21;
    , ToolTipText = "Condizione di filtro o ricerca";
    , HelpContextID = 198929004;
    , cFormVar="w_TypeFilter",RowSource=""+"Comincia per,"+"Contiene,"+"Uguale,"+"Minore uguale,"+"Minore,"+"Maggiore uguale,"+"Maggiore,"+"Non comincia per,"+"Non contiene,"+"Diverso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTypeFilter_1_12.RadioValue()
    return(iif(this.value =1,'Like',;
    iif(this.value =2,'Like %',;
    iif(this.value =3,'=',;
    iif(this.value =4,'<=',;
    iif(this.value =5,'<',;
    iif(this.value =6,'>=',;
    iif(this.value =7,'>',;
    iif(this.value =8,'Not Like',;
    iif(this.value =9,'Not Like %',;
    iif(this.value =10,'<>',;
    space(10))))))))))))
  endfunc
  func oTypeFilter_1_12.GetRadio()
    this.Parent.oContained.w_TypeFilter = this.RadioValue()
    return .t.
  endfunc

  func oTypeFilter_1_12.SetRadio()
    this.Parent.oContained.w_TypeFilter=trim(this.Parent.oContained.w_TypeFilter)
    this.value = ;
      iif(this.Parent.oContained.w_TypeFilter=='Like',1,;
      iif(this.Parent.oContained.w_TypeFilter=='Like %',2,;
      iif(this.Parent.oContained.w_TypeFilter=='=',3,;
      iif(this.Parent.oContained.w_TypeFilter=='<=',4,;
      iif(this.Parent.oContained.w_TypeFilter=='<',5,;
      iif(this.Parent.oContained.w_TypeFilter=='>=',6,;
      iif(this.Parent.oContained.w_TypeFilter=='>',7,;
      iif(this.Parent.oContained.w_TypeFilter=='Not Like',8,;
      iif(this.Parent.oContained.w_TypeFilter=='Not Like %',9,;
      iif(this.Parent.oContained.w_TypeFilter=='<>',10,;
      0))))))))))
  endfunc

  func oTypeFilter_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oTypeFilter_1_12.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE<>'C')
    endwith
  endfunc


  add object oTypeFilter1_1_13 as StdCombo with uid="YSXFKELPRY",rtseq=8,rtrep=.f.,left=3,top=58,width=111,height=21;
    , ToolTipText = "Condizione di filtro o ricerca";
    , HelpContextID = 198916460;
    , cFormVar="w_TypeFilter1",RowSource=""+"Uguale,"+"Minore uguale,"+"Minore,"+"Maggiore uguale,"+"Maggiore,"+"Diverso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTypeFilter1_1_13.RadioValue()
    return(iif(this.value =1,'=',;
    iif(this.value =2,'<=',;
    iif(this.value =3,'<',;
    iif(this.value =4,'>=',;
    iif(this.value =5,'>',;
    iif(this.value =6,'<>',;
    space(10))))))))
  endfunc
  func oTypeFilter1_1_13.GetRadio()
    this.Parent.oContained.w_TypeFilter1 = this.RadioValue()
    return .t.
  endfunc

  func oTypeFilter1_1_13.SetRadio()
    this.Parent.oContained.w_TypeFilter1=trim(this.Parent.oContained.w_TypeFilter1)
    this.value = ;
      iif(this.Parent.oContained.w_TypeFilter1=='=',1,;
      iif(this.Parent.oContained.w_TypeFilter1=='<=',2,;
      iif(this.Parent.oContained.w_TypeFilter1=='<',3,;
      iif(this.Parent.oContained.w_TypeFilter1=='>=',4,;
      iif(this.Parent.oContained.w_TypeFilter1=='>',5,;
      iif(this.Parent.oContained.w_TypeFilter1=='<>',6,;
      0))))))
  endfunc

  func oTypeFilter1_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oTypeFilter1_1_13.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE='C')
    endwith
  endfunc

  add object oRemPrevFilter_1_14 as StdCheck with uid="NLDZBMOWAM",rtseq=9,rtrep=.f.,left=116, top=58, caption="Annulla preced.",;
    ToolTipText = "Annulla filtro precedentemente inserito",;
    HelpContextID = 24640266,;
    cFormVar="w_RemPrevFilter", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRemPrevFilter_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oRemPrevFilter_1_14.GetRadio()
    this.Parent.oContained.w_RemPrevFilter = this.RadioValue()
    return .t.
  endfunc

  func oRemPrevFilter_1_14.SetRadio()
    this.Parent.oContained.w_RemPrevFilter=trim(this.Parent.oContained.w_RemPrevFilter)
    this.value = ;
      iif(this.Parent.oContained.w_RemPrevFilter=='S',1,;
      0)
  endfunc

  func oRemPrevFilter_1_14.mHide()
    with this.Parent.oContained
      return (not .oParentObject.parent.parent.bOptions)
    endwith
  endfunc


  add object ZoomValueN as cp_szoombox with uid="XOFIVMTMNB",left=-8, top=73, width=243,height=121,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="CP_ZOOMVALUEN",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="cpazi",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",bQueryOnDblClick=.f.,bSearchFilterOnClick=.f.,;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Elenco valori presenti nello zoom";
    , HelpContextID = 150046362


  add object ZoomValue as cp_szoombox with uid="BQESICZURG",left=-8, top=73, width=243,height=121,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="CP_ZOOMVALUE",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="cpazi",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",bQueryOnDblClick=.f.,bSearchFilterOnClick=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Elenco valori presenti nello zoom";
    , HelpContextID = 150046362
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_searchfilter','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_searchfilter
**************************************************
* Definizione label per filtri avanzati
*
DEFINE CLASS cp_StringAdvanced as StdString 
    uid="YYABKDNGPX"
    Visible=.t.
    Left=6
    Top=24
    Alignment=0
    Width=130
    Height=18
    Caption="Visualizza filtri avanzati"
    bGlobalFont=.t.
  
  PROCEDURE MouseEnter
		LPARAMETERS nButton, nShift, nXCoord, nYCoord

		WITH THIS
			.FONTUNDERLINE = .T.
			.FORECOLOR = RGB(0,0,255)
		ENDWITH
	ENDPROC

	PROCEDURE MouseLeave
		LPARAMETERS nButton, nShift, nXCoord, nYCoord

		WITH THIS
			.FONTUNDERLINE = .F.
			.FORECOLOR = RGB(128,128,255)
		ENDWITH
	ENDPROC
  
	PROCEDURE Click
		WITH THIS
			IF .CAPTION = cp_Translate("Visualizza filtri avanzati")
				.CAPTION = cp_Translate("Nascondi filtri avanzati")
        if .parent.Parent.Parent.Parent.oParentObject.Parent.Parent.bOptions
          .parent.parent.parent.parent.Height=195
          .parent.parent.parent.parent.oPgFrm.Height=195
        else
          .parent.parent.parent.parent.Height=83
          .parent.parent.parent.parent.oPgFrm.Height=83
        endif
        if .Parent.oContained.w_ExpandForm<>3
          .Parent.oContained.w_ExpandForm=3
          do cp_SearchFilterr with .Parent.oContained, "LoadZoom"
        endif
			ELSE
				.CAPTION = cp_Translate("Visualizza filtri avanzati")
        .parent.parent.parent.parent.Height=40
        .parent.parent.parent.parent.oPgFrm.Height=40
			ENDIF
		ENDWITH
	ENDPROC
ENDDEFINE

* --- Fine Area Manuale
