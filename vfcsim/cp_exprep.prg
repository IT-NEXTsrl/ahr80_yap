* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_exprep                                                       *
*              Export report                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-10                                                      *
* Last revis.: 2010-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_Tipo,i_nFile,pParent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_exprep",oParentObject,m.i_Tipo,m.i_nFile,m.pParent)
return(i_retval)

define class tcp_exprep as StdBatch
  * --- Local variables
  i_Tipo = space(0)
  i_nFile = space(0)
  pParent = space(0)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     LOCAL i_err 
 LOCAL l_Cursore
    if UPPER(this.pParent.Class)=="MULTIREPORT"
      l_Cursore = this.pParent.GetCursorName(this.pParent.GetMainReport())
      if USED(l_Cursore) AND RECCOUNT(l_Cursore)>0
        SELECT(l_Cursore)
      else
        i_retcode = 'stop'
        return
      endif
    else
      this.pParent.SelectReport("")
      if this.pParent.nIndexReport>0
        l_Cursore = this.pParent.cNomeReport.GetCursorName(this.pParent.nIndexReport)
        if USED(l_Cursore) AND RECCOUNT(l_Cursore)>0
          SELECT(l_Cursore)
        else
          i_retcode = 'stop'
          return
        endif
      else
        i_retcode = 'stop'
        return
      endif
    endif
     i_err=on("ERROR")
    * --- Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
    *      perch� serve solo per decidere l'icona da utilizzare.
    do case
      case this.i_Tipo = "DBF"
        * --- Export DBF
         on error cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,upper(ExitName)),48,"",.F.) 
 copy to (this.i_nFile) TYPE FOX2X
      case this.i_Tipo = "SDF"
        * --- Export SDF
         on error cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,upper(ExitName)),48,"",.F.) 
 copy to (this.i_nFile) SDF
      case  this.i_Tipo = "DLM"
        * --- Export DELIMITED
         on error cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,upper(ExitName)),48,"",.F.) 
 copy to (this.i_nFile) DELIMITED
    endcase
  endproc


  proc Init(oParentObject,i_Tipo,i_nFile,pParent)
    this.i_Tipo=i_Tipo
    this.i_nFile=i_nFile
    this.pParent=pParent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="i_Tipo,i_nFile,pParent"
endproc
