* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_chpfun                                                       *
*              Read fields multilanguage                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-31                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar,pMultiReportSplit
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_chpfun",oParentObject,m.pPar,m.pMultiReportSplit)
return(i_retval)

define class tcp_chpfun as StdBatch
  * --- Local variables
  pPar = space(10)
  w_IDMSAsa = .f.
  pMultiReportSplit = .NULL.
  w_MACCHINA = space(1)
  w_OBJ = .NULL.
  w_elemento = 0
  w_ret = 0
  w_NameExport = space(255)
  w_oExcel = .NULL.
  w_aggpath = space(254)
  w_bOldExport = .f.
  w_cTranslate = space(20)
  w_cNameTranslate = space(100)
  w_AutoDir = .f.
  w_NameExport = space(255)
  w_bAutoOffice = .f.
  w_cOldExp_NoModel = space(254)
  w_cOldExp_OldModel = space(254)
  w_nOperaz = 0
  w_SERIAL = space(10)
  w_MVFLSEND = space(1)
  w_MVCLADOC = space(2)
  w_STAMPANTE = space(10)
  w_OLDFLOPEN = space(1)
  w_FILENAME = space(254)
  w_var = space(10)
  w_oldpath = space(10)
  w_path_ps = space(10)
  w_CAMPO = .NULL.
  * --- WorkFile variables
  STMPFILE_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per la Nuova print System il batch cp_chpfun3 ne gestisce le funzionalit�
    if i_bNewPrintSystem
      this.w_ret = CP_CHPFUN3 (this.oparentobject , this.pPar ,this.pMultiReportSplit ) 
      i_retcode = 'stop'
      i_retval = this.w_RET
      return
    endif
    * --- Vecchia print System
    this.w_MACCHINA = "N"
    this.w_OBJ = this.oparentobject
    private L_OldCaption
    L_OldCaption = _screen.caption
    do case
      case this.pPar="init"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="AnteClick"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="StpClick"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="oPDBtnProcClick"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="StpOptClick"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="PDBtnStatusClick"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="WordClick"
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="ExcelClick"
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="GraphClick"
        this.Page_10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="MailClick"
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="WWP_ZCPClick"
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="PliteClick"
        this.Page_13()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="FaxClick"
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="QueryClick"
        this.Page_15()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="EdiClick" or this.pPar="FatelClick"
        this.Page_16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="EscClick"
        this.Page_17()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="StpFileOptClick"
        this.Page_18()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="StpFileClick"
        this.Page_19()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="StpArchiClick"
        this.Page_20()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="SaveIndClick"
        this.Page_21()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="AssocClick"
        this.Page_22()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="SStpClick"
        this.Page_23()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="SFileClick"
        this.Page_24()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="Txt_Copies LostFocus"
        this.Page_25()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="Opt_Language Changed"
        this.Page_26()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="BlankRecEnd"
        this.Page_27()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="SettaggiLingue" 
        this.Page_28()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="ecpSecurity" 
        this.Page_29()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza la combobox con l elenco delle estensioni file della macchina oppure dell'installazione
    * --- w_OBJ=this.oparentobject.getCTRL ("w_Opt_File")   // Non funziona!!!
    this.w_OBJ = Application.activeform.getCTRL ("w_Opt_File")
    if TYPE ("this.w_OBJ.class")<>"C" OR this.w_OBJ.class<>"Stdtablecombo"
      this.w_OBJ = this.oparentobject.getCTRL ("w_Opt_File")
    endif
    this.oParentObject.w_PCNAME = Left( Sys(0), Rat("#",Sys( 0 ))-1)
    * --- Select from STMPFILE
    i_nConn=i_TableProp[this.STMPFILE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SFFLGSEL  from "+i_cTable+" STMPFILE ";
          +" where SFFLGSEL = 'S' AND SFPCNAME = "+cp_ToStrODBC(this.oParentObject.w_PCNAME)+"";
           ,"_Curs_STMPFILE")
    else
      select SFFLGSEL from (i_cTable);
       where SFFLGSEL = "S" AND SFPCNAME = this.oParentObject.w_PCNAME;
        into cursor _Curs_STMPFILE
    endif
    if used('_Curs_STMPFILE')
      select _Curs_STMPFILE
      locate for 1=1
      do while not(eof())
      this.w_MACCHINA = SFFLGSEL
        select _Curs_STMPFILE
        continue
      enddo
      use
    endif
    if NVL (this.w_MACCHINA,"N")<>"S"
      this.oParentObject.w_PCNAME = "INSTALLAZIONE"
    endif
    this.oparentobject.mcalc (.t.)
    * --- Per forzare il caricamento dei dati dopo che sono stati impostati i parametri della query di estrazione dati
    this.w_OBJ.clear()     
    this.w_OBJ.init()     
    * --- Elimina l'elemento vuoto e seleziona il primo elemento della lista
    if this.w_OBJ.listcount>=1
      this.w_OBJ.removeitem(1)     
      * --- Aggiorna l'array contenente le estensioni  (non bisogna considerare l elemento cancellato che nell array delle estensioni � ancora presente)
      this.w_elemento = 1
      do while this.w_elemento<= this.w_OBJ.listcount
        this.w_OBJ.combovalues[this.w_elemento] = this.w_OBJ.combovalues[this.w_elemento + 1]
        this.w_elemento = this.w_elemento + 1
      enddo
    endif
    if this.w_OBJ.listcount>=1
      this.w_OBJ.listindex = 1
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invia a Video
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = LEFT( L_OldCaption, 30 )
    endif
    this.w_OBJ.alwaysontop = .F.
    if this.w_OBJ.i_FDFFile
       ShellExecute(FindWindow(0,_screen.caption),"Open",this.w_OBJ.cNomeReport.FirstReport()+".FDF","",cHomeDir,1)
    else
      * --- Invia a Video
      if this.w_OBJ.cTipoReport="S"
        * --- Invio nuovo parametro 'V' per anteprima a Video
        do Run2Rep with this.w_OBJ,this.w_OBJ.oSplitReport, this.w_OBJ.prCopie, this.w_OBJ.prStamp, "V", this.w_OBJ.cModSst , this.w_OBJ
      else
        if this.w_OBJ.tReport="I"
          l_sPrg="gsir_bir"
          if substr(this.w_OBJ.cNomeReport.FirstReport(),len(this.w_OBJ.cNomeReport.FirstReport())-3,1)="."
             do (l_sPrg) with this.w_OBJ, left(this.w_OBJ.cNomeReport.FirstReport(),len(this.w_OBJ.cNomeReport.FirstReport())-4), noParentObject, "V", -1
          else
             do (l_sPrg) with this.w_OBJ, this.w_OBJ.cNomeReport.FirstReport(), noParentObject, "F", -1
          endif
        else
          * --- Memorizzo lo stato delle maschere MDI se attivo MDI Form
          if i_VisualTheme=-1 AND (i_cViewMode="A" Or i_cViewMode="M")
             LOCAL L_OLDSTATE,L_NewState,I_X,L_RifForm 
 L_OLDSTATE=0 
 L_NEWSTATE=0 
 L_RifForm=.Null. 
 I_X=1
            do while I_X <= _Screen.FORMCOUNT
              if LOWER(_Screen.FORMS(I_X).BaseClass)="form" AND _Screen.FORMS(I_X).MDIFORM
                 L_OLDSTATE=_Screen.FORMS(I_X).WindowState 
 L_RifForm = _Screen.FORMS(I_X) 
 exit
              endif
               I_X= I_X+1
            enddo
          endif
           do RunRep with this.w_OBJ,this.w_OBJ.oSplitReport, "V", this.w_OBJ.prFile, this.w_OBJ.prCopie
          * --- Ripristino lo stato delle maschere se attivo MDI Form 
          if i_VisualTheme=-1 AND (i_cViewMode="A" Or i_cViewMode="M")
             I_X= 1
            do while I_X <= _Screen.FORMCOUNT
              if LOWER(_Screen.FORMS(I_X).BaseClass)="form" AND _Screen.FORMS(I_X).MDIFORM
                 L_NEWSTATE=_Screen.FORMS(I_X).WindowState 
 exit
              endif
               I_X= I_X+1
            enddo
            if L_NEWSTATE<>L_OLDSTATE
              L_RifForm.Windowstate=L_OldState
              * --- Nel caso la form MDi era allargata riporto la Print System sopra di essa per renderl visibile
              if L_RifForm.Windowstate=2
                this.w_OBJ.Show()     
              endif
              L_RifForm=.Null.
            endif
          endif
        endif
      endif
    endif
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = L_OldCaption
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     local bPrintErr 
 bPrintErr=.F. 
 on error bPrintErr=.T.
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = LEFT( L_OldCaption, 30 )
    endif
    * --- Invia a Stampante senza Richiesta
    if this.w_OBJ.i_FDFFile
       ShellExecute(FindWindow(0,_screen.caption),"Print",this.w_OBJ.cNomeReport.FirstReport()+".FDF","",chomedir,0)
    else
      if not(empty(this.w_OBJ.prStamp))
         set printer to name (this.w_OBJ.prStamp)
      else
         set printer to default
      endif
      if not bPrintErr
        if this.w_OBJ.cTipoReport="S"
          * --- Modifica per stampa solo Testo (tutto il resto e' standard del metodo click)
          *     Passo nuovo parametro 'S' per Invio a stampante
          *     Passo nuovo parametro cModSst che contiene il modello di stampante su Associazione Report/Device
           do Run2Rep with this.w_OBJ, this.w_OBJ.oSplitReport, this.w_OBJ.prCopie, this.w_OBJ.prStamp, "S", this.w_OBJ.cModSst , this.w_OBJ
        else
           do RunRep with this.w_OBJ,this.w_OBJ.oSplitReport, "S", this.w_OBJ.prFile, this.w_OBJ.prCopie
        endif
      else
         cp_Errormsg(MSG_UNABLE_TO_USE_PRINTER,16)
      endif
    endif
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = L_OldCaption
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia routine di esecuzione processo documentale
     local l_sPrg, l_jj, l_nomCur, ini_EMAIL, ini_EMAILSUBJECT, ini_EMAILTEXT, ini_FAXNO, ini_DEST, ini_FAXSUBJECT
    * --- Memorizzo i dati relativi a invio Email e Fax
     ini_EMAIL = i_EMAIL 
 ini_EMAILSUBJECT = i_EMAILSUBJECT 
 ini_EMAILTEXT = i_EMAILTEXT 
 ini_FAXSUBJECT = i_FAXSUBJECT 
 ini_DEST = i_DEST 
 ini_FAXNO = i_FAXNO 
    if this.w_OBJ.tReport="I"
       l_sPrg="gsir_bir"
      if substr(this.w_OBJ.cNomeReport,len(this.w_OBJ.cNomeReport)-3,1)="."
         do (l_sPrg) with this.w_OBJ, left(this.w_OBJ.cNomeReport,len(this.w_OBJ.cNomeReport)-4), noParentObject, "P", -1
      else
         do (l_sPrg) with _OBJ, this.w_OBJ.cNomeReport, noParentObject, "P", -1
      endif
    else
       LOCAL oReport_Text, oReport_Graphic 
 oReport_Text=CREATEOBJECT("MultiReport") 
 oReport_Graphic=CREATEOBJECT("MultiReport") 
 this.w_OBJ.oSplitReport.SplitTextGraphic(oReport_Text, oReport_Graphic) 
 l_sPrg="GSDM_BPD" 
 do (l_sPrg) with this.w_OBJ, "EP", this.w_OBJ.pdKeyIstanza, oReport_Graphic 
 oReport_Text=.NULL. 
 oReport_Graphic=.NULL. 
 
      * --- Ripristino i dati relativi a invio Email e Fax
    endif
     i_EMAIL = ini_EMAIL 
 i_EMAILSUBJECT = ini_EMAILSUBJECT 
 i_EMAILTEXT = ini_EMAILTEXT 
 i_FAXSUBJECT = ini_FAXSUBJECT 
 i_DEST = ini_DEST 
 i_FAXNO = ini_FAXNO 
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invia a Stampante senza Richiesat
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = LEFT( L_OldCaption, 30 )
    endif
    if not(empty(this.w_OBJ.prStamp))
      set printer to name (this.w_OBJ.prStamp)
    else
       set printer to default
    endif
     do RunRep with this.w_OBJ,this.w_OBJ.oSplitReport, "O", this.w_OBJ.prFile, this.w_OBJ.prCopie
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = L_OldCaption
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizio DOCM definizione metodi 'Click' per i bottoni
    * --- lancia routine di lancio maschera
     LOCAL oReport_Text, oReport_Graphic 
 oReport_Text=CREATEOBJECT("MultiReport") 
 oReport_Graphic=CREATEOBJECT("MultiReport")
    this.w_OBJ.oSplitReport.SplitTextGraphic(oReport_Text, oReport_Graphic)     
     local l_sPrg 
 l_sPrg="GSDM_BPD" 
 do (l_sPrg) with this.w_OBJ, "VL", this.w_OBJ.pdKeyIstanza, oReport_Graphic 
 oReport_Text=.NULL. 
 oReport_Graphic=.NULL.
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziono report
    this.w_OBJ.SelectReport("W")     
    if this.w_OBJ.nIndexReport>0 AND !this.w_OBJ.cNomeReport.IsEmpty()
       Local l_CurName, l_RepName, cexte 
 l_CurName=this.w_OBJ.cNomeReport.GetCursorName(this.w_OBJ.nIndexReport) 
 l_RepName=this.w_OBJ.cNomeReport.GetReport(this.w_OBJ.nIndexReport)
      * --- Zucchetti Aulla Inizio
       local n,i_err,bOk,cModelName,cDocName 
 local cformat,cconfirmconv,creadonly,clinktosource,caddtofilelist,crevert 
 local cpassworddoc,cpasswordtemp,cprotectdoc,cprotecttemp,cdsname,cwconn,csqlstatement 
 store 0 to cformat,cconfirmconv,creadonly,clinktosource,caddtofilelist,crevert 
 store "" to cpassworddoc,cpasswordtemp,cprotectdoc,cprotecttemp
      * --- Zucchetti Aulla Fine
      SELECT(l_CurName)
      if VARTYPE(g_OFFICE_TYPE_CONV) = "C" AND g_OFFICE_TYPE_CONV $ "C-S"
        if !convcuraccentedchars(l_CurName, g_OFFICE_TYPE_CONV )
          ah_errormsg("Errori durante la conversioen dei caratteri nel cursore di esportazione")
        endif
      endif
      n_tuple = RECCOUNT(l_CurName)
      SELECT(l_CurName)
      go top
      if not(eof())
        if g_OFFICE="M" And Type("g_AUTOFFICE")<>"U" And !g_AUTOFFICE And cp_IsStdFile(l_RepName, "docx")
          * --- Esportazione senza automazione solo se il file docx esiste
          this.w_aggpath = cp_GetStdFile(l_RepName, "docx")
          this.w_AutoDir = Type("g_NoAutOfficeDir")="C" And !Empty(g_NoAutOfficeDir) And Directory(g_NoAutOfficeDir)
          this.w_NameExport = IIF(this.w_AutoDir, Addbs(g_NoAutOfficeDir)+JustStem(this.w_aggpath)+Sys(2015), Putfile("Save as", Addbs(g_tempadhoc)+Justfname(this.w_aggpath), "docx"))
          if !Empty(this.w_NameExport)
            vx_build("D", this.w_aggpath, l_CurName, this.w_NameExport, .F., .T.)
          endif
        else
           local n,i_err,bOk,cModelName,cDocName 
 n=ADDBS(Alltrim(g_tempadhoc))+ IIF(g_OFFICE="M",Sys(2015), "__word__")
          if ! g_OFFICE="M"
            if VARTYPE(g_OFFICE_BIT)="C" AND g_OFFICE_BIT="3"
              copy to (n) fox2x
            else
              copy to (n) foxplus
            endif
          endif
          if g_OFFICE="M"
            if cp_fileexist(forceext(n, "xlsx"),.T.)
              nomefiext=forceext(n, "xlsx") 
              erase &nomefiext
            endif
            * --- Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
            DecoratorState ("D")
            * --- Non lascia messaggi e non rende visibile il file
            cp_cursortoxlsx( FullPath(forceext(n, "xlsx")) , "Foglio1",SELECT(0))
          endif
           bOk=.t. 
 i_err=on("ERROR") 
 on error bOk=.f.
          if g_OFFICE="M"
            this.w_OBJ.word = createobject("word.application")
            this.w_OBJ.word.Visible = .F.
          endif
          if bOk
            if g_OFFICE="M"
               cdsname=forceext(n, "xlsx")
               cexte=IIF(cp_IsStdFile(l_RepName,"DOCX"),"DOCX","DOC")
              this.w_aggpath = cp_GetStdFile(l_RepName,cexte)
              if Empty(this.w_aggpath)
                this.w_OBJ.word.documents.open(Forceext(cp_GetStdFile(FULLPATH(l_RepName),cexte),cexte))     
              else
                this.w_OBJ.word.documents.open(Forceext(FULLPATH(this.w_aggpath),cexte))     
              endif
               cModelName=this.w_OBJ.word.activedocument.name
              * --- Zucchetti Aulla Inizio
              *     Setta correttamente il datasource sul modello
               csqlstatement="SELECT * FROM [Foglio1$]"
              this.w_OBJ.word.ActiveDocument.MailMerge.OpenDataSource(cdsname,cformat,cconfirmconv,creadonly,clinktosource,caddtofilelist,"","",crevert,cprotectdoc,cprotecttemp,"",csqlstatement)     
              * --- Zucchetti Aulla Fine
              this.w_OBJ.word.ActiveDocument.MailMerge.Execute()     
               cDocName=this.w_OBJ.word.activedocument.name
              * --- La close va in errore per i documenti di word 97'
              this.w_OBJ.word.documents(cModelName).Close(0)     
              this.w_OBJ.word.documents(cDocName).activate()     
              this.w_OBJ.word.visible = .t.
              if cp_fileexist( cdsname ,.T.)
                cdsname='"'+cdsname+'"'
                erase &cdsname
              endif
              * --- *** Gestione OpenOffice ***
            else
               cdsname=n+".dbf"
               oManager= CreateObject("com.sun.star.ServiceManager") 
 oDesktop= oManager.createInstance("com.sun.star.frame.Desktop") 
 comarray(oDesktop,10) 
 oDBc=oManager.createInstance("com.sun.star.sdb.DatabaseContext") 
 oDataSource=oDBc.getByName("Bibliography") 
 oDataSource.URL="sdbc:dbase:file:///"+strtran(ADDBS(Alltrim(g_tempadhoc)),CHR(92),CHR(47)) 
 objMailMerge = oManager.createInstance("com.sun.star.text.MailMerge")
              this.w_aggpath = cp_GetStdFile(l_RepName,"SXW")
              if Empty(this.w_aggpath)
                objMailMerge.DocumentURL="file:///"+StrTran(ALLTRIM(FULLPATH(Forceext(l_repname,"SXW"))),CHR(92),CHR(47)) 
              else
                objMailMerge.DocumentURL="file:///"+StrTran(ALLTRIM(Forceext(FULLPATH(this.w_aggpath),"SXW")),CHR(92),CHR(47)) 
              endif
               objMailMerge.CommandType=0 
 outType=INT(VAL(ALLTRIM(g_PRINTMERGE))) 
 objMailMerge.OutputType=IIF(outType=1 or outType=2, outType, 2) 
 objMailMerge.OutputURL="file:///"+STRTRAN(ALLTRIM(tempadhoc()),CHR(92),CHR(47)) 
               DIMENSION Args(2), DocArgs(1), newDoc(1) 
 Args[1] = oManager.Bridge_GetStruct("com.sun.star.beans.NamedValue") 
 Args[1].name = "DataSourceName" 
 Args[1].value = "Bibliography" 
 Args[2] = oManager.Bridge_GetStruct("com.sun.star.beans.NamedValue") 
 Args[2].name = "Command" 
 Args[2].value = "__word__" 
 objMailMerge.execute(@Args) 
              if objMailMerge.OutputType = 2
                * --- Se non mando in stampa concateno i files in un unico documento...
                 DocArgs[1] = oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue") 
 newDoc[1] = oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue") 
 newDoc[1].name="AsTemplate" 
 newDoc[1].Value=".T."
                * --- Crea nuovo Documento Writer vuoto ***
                 oWriter = oDesktop.loadComponentFromURL("private:factory/swriter", "_blank", 0, @newDoc) 
 oText=oWriter.getText() 
 oCurs=oText.createTextCursorByRange(oText.getEnd()) 
 oCurs.collapseToStart()
                * --- Carica le stampe unione salvate in un unico file
                 
 nomefile=ALLTRIM(justfname(this.w_aggpath)) 
 i=0
                do while i<= n_tuple
                   w_path=ALLTRIM(addbs(tempadhoc()))+nomefile+ALLTRIM(STR(INT(i)))+".sxw" 
 Local cPath_new, bmod_exists, cPath_ok 
 cPath_new=ALLTRIM(addbs(tempadhoc()))+nomefile+ALLTRIM(STR(INT(i)))+".odt" 
 bmod_exists=.f. 
 
                  if cp_fileexist(w_path)
                     cPath_ok=w_path 
 bmod_exists=.t.
                  else
                    if cp_fileexist(cPath_new)
                       cPath_ok=cPath_new 
 bmod_exists=.t.
                    endif
                  endif
                  if bmod_exists
                     oCurs.insertDocumentFromURL("file:///"+StrTran(ALLTRIM(cPath_ok),CHR(92),CHR(47)), @DocArgs) 
 oCurs.collapseToEnd()
                    if i!=(n_tuple-1)
                       oCurs.BreakType=5 
 oText.insertControlCharacter(oCurs,0,.f.) 
 ocurs.setString(chr(13)+chr(10)) 
 oCurs.collapseToEnd() 
 
                    endif
                    * --- Cancella i file temporanei
                    DELETE FILE (cPath_ok)
                  endif
                  i=i+1
                enddo
              endif
            endif
            * --- Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
            *     perch� serve solo per decidere l'icona da utilizzare.
            if !bOk
              if g_OFFICE="M"
                cp_ErrorMsg(MSG_CANNOT_FIND_WORD_QM,"stop",MSG_ERROR)
              else
                cp_ErrorMsg(MSG_CANNOT_FIND_WRITER,"stop",MSG_ERROR)
              endif
            else
              cp_ErrorMsg(MSG_WRITING_FINISHED_CORRECTLY)
            endif
          else
            if g_OFFICE="M"
              cp_ErrorMsg(MSG_CANNOT_OPEN_WORD_DOCUMENT_QM,"stop",MSG_ERROR) 
            else
              cp_ErrorMsg(MSG_CANNOT_OPEN_WRITER_DOCUMENT,"stop",MSG_ERROR)
            endif
          endif
          if g_OFFICE="M"
            * --- Al termine del esportazion riattiva il decorator dei form 
            DecoratorState ("A")
          endif
          * --- Zucchetti Aulla Inizio
          *     Elimina file temporaneo
          n=forceext(n, "dbf")
          if cp_fileexist(n,.T.)
            fileext='"'+n+'"'
            erase &fileext
            * --- Cerca eventuale .fpt
            n=forceext(n, "fpt")
            if cp_fileexist(n,.T.)
              fileext='"'+n+'"'
              erase &fileext
            endif
            * --- Cerca eventuale dbt
            n=forceext(n, "dbt")
            if cp_fileexist(n,.T.)
              fileext='"'+n+'"'
              erase &fileext
            endif
          endif
          * --- Zucchetti Aulla Fine
           on error &i_err
        endif
      else
        cp_ErrorMsg(MSG_NO_DATA_TO_PRINT,"stop",MSG_ERROR)
      endif
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziono report
    this.w_OBJ.SelectReport("E")     
    if this.w_OBJ.nIndexReport>0
       Local l_CurName, l_RepName, i_Ext, i_NomeCalc, l_RepCodISO 
 l_CurName=this.w_OBJ.cNomeReport.GetCursorName(this.w_OBJ.nIndexReport) 
 l_RepName=this.w_OBJ.cNomeReport.GetReportOrig(this.w_OBJ.nIndexReport) 
 l_RepCodISO=this.w_OBJ.cNomeReport.GetCodISO(this.w_OBJ.nIndexReport) 
 Local i_nFile,i_err,bOk 
 
      * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
       Local loSManager, loSDesktop, loReflection, loPropertyValue, loStarCalc,i_str,i_arr,i_curs 
 Local Array NoArgs[1] 
 
      * --- Zucchetti Aulla - Fine Gestione Suite OpenOffice/StarOffice
       Select(l_CurName) 
 Go Top 
 bOk=.T. 
 i_err=On("ERROR") 
 On Error bOk=.F. 
 
      * --- L'utilizzo della ve_build � condizionato dal check Automazione office all'interno della Suite Office, dalla variabile g_ExcelNoModel nel caso di assenza modello, 
      * --- oppure se non � prevista l'Automazione ma non � presente il modello XLTX (fondamentale per l'esportazione tramite XML)
      this.w_bAutoOffice = Type("g_AUTOFFICE")="U" Or g_AUTOFFICE
      this.w_cOldExp_NoModel = this.w_bAutoOffice And ((cp_IsStdFile(l_RepName, "XLTX") Or cp_IsStdFile(l_RepName, "XLT") Or cp_IsStdFile(l_RepName, "XLTM")) Or (Type("g_ExcelNoModel")="U" Or g_ExcelNoModel<>1))
      this.w_cOldExp_OldModel = !this.w_bAutoOffice And !cp_IsStdFile(l_RepName, "XLTX") And (cp_IsStdFile(l_RepName, "XLT") Or cp_IsStdFile(l_RepName, "XLTM"))
      this.w_bOldExport = this.w_cOldExp_NoModel Or this.w_cOldExp_OldModel
      * --- Se � stato indicato un modello esplicitamente (nell output utente o da batch)  allora si utilizza tale modello altrimenti si cerca priam il modello per excel/word 97
       i_Ext=this.w_OBJ.cNomeReport.GetExtReport(this.w_OBJ.nIndexReport) 
 i_Ext= IIF (cp_IsStdFile(l_RepName,"XLTX"), "XLTX", "XLT")
      * --- Ho un modello oppure un errore nell'aprire il foglio Excel..
      if Not bOk Or (!this.w_OBJ.cNomeReport.IsEmpty() AND ((cp_IsStdFile(l_RepName,i_Ext) Or this.w_bOldExport) And g_OFFICE="M") Or (cp_IsStdFile(l_RepName,"STC") Or cp_IsStdFile(l_RepName,"OTS") And g_OFFICE="O"))
        if g_OFFICE="M"
          i_NomeExcel=FULLPATH(cp_GetStdFile(l_RepName,i_Ext)+"."+i_Ext)
          if bOk
            this.w_OBJ.excel = Createobject("excel.application")
            * --- Eseguo la traduzione se necessario...
            if Not(this.w_OBJ.cNomeReport.GetCodISO(this.w_OBJ.nIndexReport)==this.w_OBJ.cLanguage) And this.w_bOldExport
              this.w_OBJ.excel.Quit()     
              this.w_OBJ.excel = null
              i_NewExcel= cp_TranslateExcel(JUSTSTEM(i_NomeExcel),l_RepCodISO,this.w_OBJ.cLanguage,this.w_OBJ.cSessionCode,i_NomeExcel)
              if Not(Empty(i_NewExcel))
                i_NomeExcel= i_NewExcel
              endif
              this.w_OBJ.excel = Createobject("excel.application")
            endif
            * --- Se non c � excel l esportazione senza modello pu� essere fatta  (nella ve_build c � il controllo che blocca l elaborazione nel caso in cui non sia stato istanziato l oggetto excel.applicatioon)
            bOk=.T.
          endif
          if bOk
            if this.w_bOldExport
               cp_msg(cp_Translate(MSG_BUILDING_EXCEL_WORKSHEET_D)) 
 ve_build(this.w_OBJ.excel,l_CurName,EVL((FORCEEXT(i_NomeExcel, i_Ext)),Forceext(Justfname(Alltrim(this.w_OBJ.prFile)), ".XLS")),.F.) 
 this.w_OBJ.excel = .null.
            else
              if !Empty(l_RepCodISO) And !Empty(this.w_OBJ.cLanguage) And Not(l_RepCodISO==this.w_OBJ.cLanguage)
                this.w_cTranslate = l_RepCodISO+";"+this.w_OBJ.cLanguage
                this.w_cNameTranslate = Justfname( ForceExt( Addbs(JustPath(i_NomeExcel))+JustStem(i_NomeExcel)+"_"+Alltrim(this.w_OBJ.cLanguage)+Iif(Vartype(this.w_OBJ.cSessionCode)<>"C" Or Empty(this.w_OBJ.cSessionCode), Sys(2015), this.w_OBJ.cSessionCode), "xlsx"))
              else
                this.w_cTranslate = ""
                this.w_cNameTranslate = ForceExt(Justfname(i_NomeExcel), "xlsx")
              endif
              this.w_AutoDir = Type("g_NoAutOfficeDir")="C" And !Empty(g_NoAutOfficeDir) And Directory(g_NoAutOfficeDir)
              this.w_NameExport = IIF(this.w_AutoDir, Addbs(g_NoAutOfficeDir)+JustStem(this.w_cNameTranslate)+Sys(2015), Putfile("Save as", Addbs(g_tempadhoc)+this.w_cNameTranslate, "xlsx"))
              if !Empty(this.w_NameExport)
                vx_build("X", i_NomeExcel, l_CurName, this.w_NameExport, .F., .T., this.w_cTranslate)
              endif
            endif
          else
            * --- Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
            *      perch� serve solo per decidere l'icona da utilizzare.
            cp_ErrorMsg(MSG_CANNOT_OPEN_EXCEL_WORKSHEET_QM+Chr(13)+Message()+Message(1),"stop",MSG_ERROR)
          endif
        else
          do case
            case cp_IsStdFile(l_RepName,"OTS")
              i_Ext="OTS"
            case cp_IsStdFile(l_RepName,"STC")
              i_Ext="STC"
            case cp_IsStdFile(l_RepName,"XLT")
              i_Ext="XLT"
            case cp_IsStdFile(l_RepName,"XLTX")
              i_Ext="XLTX"
            case cp_IsStdFile(l_RepName,"XLTM")
              i_Ext="XLTM"
            otherwise
              i_Ext=""
          endcase
          i_NomeCalc=FULLPATH(cp_GetStdFile(l_RepName,i_Ext))
          if Not(this.w_OBJ.cNomeReport.GetCodISO(this.w_OBJ.nIndexReport)==this.w_OBJ.cLanguage)
            i_NewCalc= cp_TranslateCalc(i_NomeCalc,l_RepCodISO,this.w_OBJ.cLanguage,this.w_OBJ.cSessionCode)
            if Not(Empty(i_NewCalc))
              i_NomeCalc= i_NewCalc
            endif
          endif
          cp_msg(cp_Translate(MSG_CREATING_SPREADSHEET_DOCUMENT)) 
 vo_build(l_CurName,ForceExt(i_NomeCalc,i_Ext),.F.)
        endif
        Wait Clear
      else
        i_nFile = ALLTRIM(Addbs(g_tempadhoc))+Forceext(Justfname(Alltrim(this.w_OBJ.prFile)), ".XLS")
        if g_OFFICE="M" And !this.w_bOldExport
          this.w_AutoDir = Type("g_NoAutOfficeDir")="C" And !Empty(g_NoAutOfficeDir) And Directory(g_NoAutOfficeDir)
          this.w_NameExport = IIF(this.w_AutoDir, Addbs(g_NoAutOfficeDir)+JustStem(i_nFile)+Sys(2015), Putfile("Save as", Addbs(g_tempadhoc)+ForceExt(Justfname(i_nFile), "xlsx"), "xlsx"))
          if !Empty(this.w_NameExport)
            vx_build("X", ForceExt(i_nFile, "xltx"), l_CurName, this.w_NameExport,.F., .T.)
          endif
        else
          * --- Zucchetti Aulla Inizio: campi Memo nell'esportazione in excel in Oracle
          *     tale tipologia di campo veniva esclusa dall'esportazione su excel (E 4619)
          if CP_DBTYPE="Oracle"
            i_curs=Sys(2015) 
 Select(l_CurName) 
 i_str="" 
 i_arr=Afields(Stru) 
 i=1
            do while i <= i_arr
              if Stru(i,2)="M"
                i_str= i_str + "LEFT("+ Stru(i,1)+ " ,254)AS " + Stru(i,1)+Iif(i=i_arr,"",",")
              else
                i_str=i_str + Stru(i,1)+Iif(i=i_arr,"",",")
              endif
              i = i+1
            enddo
             Select &i_str From (l_CurName) Into Cursor &i_curs nofilter 
 Select(l_CurName) 
 Use 
 Select * From &i_curs Into Cursor (l_CurName) nofilter 
 Select &i_curs 
 Use 
 Select(l_CurName)
          endif
          * --- Zucchetti Aulla Fine
          *     i_nFile = getenv('TEMP')+'\'+strtran(w_OBJ.prFile,iif(w_OBJ.prTyExp='2','.DBF','.TXT'),'.XLS')
           i_nFile = Getenv("TEMP")+"\"+Forceext(Justfname(Alltrim(this.w_OBJ.prFile)), ".XLS") 
 On Error cp_ErrorMsg(CP_MSGFORMAT(MSG_CANNOT_CREATE_FILE_CL__,Upper(i_nFile)+ Chr(13))+Message()+Message(1),48,"",.F.)
          * --- Zucchetti Aulla Inizio: Aggira un problema legato alla copy to .. XL5
          if TYPE ("g_EXCELCOPYTO") = "C"
            EXCELCOPYTO( FORCEEXT(i_nFile,"XLS") , ALIAS())
          else
            cp_cursortoxlsx( FORCEEXT (i_nFile,"XLSX") , IIF ( EMPTY (JUSTSTEM(i_nFile)) , "Foglio1" ,JUSTSTEM(i_nFile)),SELECT(0))
          endif
          * --- Zucchetti Aulla Fine
          *     Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
          if g_OFFICE="M"
            STAPDF( FORCEEXT (i_nFile, IIF (TYPE ("g_EXCELCOPYTO") = "C","XLS" , "XLSX")) ,"OPEN"," ") 
          else
             i_nFile = "file:///"+Strtran(i_nFile,Chr(92),Chr(47)) 
 loSManager = Createobject("Com.Sun.Star.ServiceManager") 
 loSDesktop = loSManager.createInstance("com.sun.star.frame.Desktop") 
 loSFrame = loSManager.createInstance("com.sun.star.frame") 
 Comarray(loSDesktop, 10) 
 loReflection = loSManager.createInstance("com.sun.star.reflection.CoreReflection" ) 
 Comarray( loReflection, 10 ) 
 NoArgs[1] = createStruct( @loReflection,"com.sun.star.beans.PropertyValue" ) 
 NoArgs[1].Name = "ReadOnly" 
 NoArgs[1].Value = .F. 
 loStarCalc = loSDesktop.loadComponentFromUrl( FORCEEXT (i_nFile, IIF (TYPE ("g_EXCELCOPYTO") = "C","XLS" , "XLSX")) , "_blank", 0, @NoArgs )
          endif
          * --- Zucchetti Aulla - Fine Gestione Suite OpenOffice/StarOffice
          On Error
        endif
      endif
      On Error &i_err
    endif
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziono report
    this.w_OBJ.SelectReport("G")     
    if this.w_OBJ.nIndexReport>0 AND !this.w_OBJ.cNomeReport.IsEmpty()
       Local l_CurName, l_RepName 
 l_CurName=this.w_OBJ.cNomeReport.GetCursorName(this.w_OBJ.nIndexReport) 
 l_RepName=this.w_OBJ.cNomeReport.GetReport(this.w_OBJ.nIndexReport) 
 SELECT(l_CurName) 
 go top
      if FILE( FULLPATH(cp_GetStdFile(l_RepName,"VFC")+".VFC") )
        local l_param
        l_param = l_CurName + "," + FULLPATH(cp_GetStdFile(l_RepName,"VFC")+".VFC") 
        VX_EXEC( l_param )
      else
        vg_build(l_CurName,FULLPATH(cp_GetStdFile(l_RepName,"VGR")+".vgr"),.t.,.t.)
      endif
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OBJ.cNomeReport.IsStdFile(iif(this.w_OBJ.i_FDFFile,"FDF","FRX"),"G")
       local l_sPrg 
 l_sPrg="GSUT_BXP" 
 do (l_sPrg) with this.w_OBJ, 2, iif(this.w_OBJ.i_FDFFile,"S","N")
    else
       cp_ErrorMsg(MSG_NO_PRINTFILE_SPECIFIED,"stop")
    endif
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_IDMSAsa
        this.w_nOperaz = 6
      case g_WEENABLED="S" AND g_IZCP<>"S" and g_IZCP<>"A"
        this.w_nOperaz = 4
      otherwise
        this.w_nOperaz = IIF(g_IZCP$"SA" OR g_CPIN="S" OR g_DMIP="S",5,6)
    endcase
    if this.w_OBJ.cNomeReport.IsStdFile(iif(this.w_OBJ.i_FDFFile,"FDF","FRX"))
       local l_sPrg 
 l_sPrg="GSUT_BXP" 
 do (l_sPrg) with this.w_OBJ, this.w_nOperaz, iif(this.w_OBJ.i_FDFFile,"S","N") 
 l_sPrg=Null
    else
      cp_ErrorMsg(MSG_NO_PRINTFILE_SPECIFIED,"stop")
    endif
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    =PLiteEXE(this.w_OBJ.oSplitReport)
  endproc


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OBJ.cNomeReport.IsStdFile("FRX")
       local l_sPrg 
 l_sPrg="GSUT_BXP" 
 do (l_sPrg) with this.w_OBJ, 3 
 l_sPrg=Null
    else
      cp_ErrorMsg(MSG_NO_PRINTFILE_SPECIFIED,"stop")
    endif
  endproc


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    public g_Debug_Query
    do case
      case vartype(noParentObject) = "O" and UPPER(noParentObject.parentclass) = "STDBATCH" and type("noParentObject.oParentObject.w_OQRY")="C"
        i_DesignFile = alltrim(noParentObject.oParentObject.w_OQRY)
      case vartype(noParentObject) = "O" and type("noParentObject.w_OQRY")="C"
        i_DesignFile = alltrim(noParentObject.w_OQRY)
      otherwise
        i_DesignFile = null
    endcase
     vq_build() 
 release g_Debug_Query
  endproc


  procedure Page_16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    LOCAL l_prg
    if TYPE("noParentObject.oParentobject.w_codstr")="C"
      if this.pPar="EdiClick"
         l_prg="GSVA_BGF(.null.,noParentObject)" 
 &l_prg
      else
        this.w_SERIAL = noParentObject.oParentobject.w_MVSERIAL
        this.w_MVCLADOC = noParentObject.oParentobject.w_MVCLADOC
        this.w_MVFLSEND = noParentObject.oParentobject.w_MVFLSEND
        do case
          case ! this.w_MVCLADOC $ "FA-NC"
            ah_errormsg("Approva documento per fatturazione elettronica non prevista per la tipologia documento")
          case this.w_MVFLSEND $ "S-F"
            ah_errormsg("Documento gi� approvato")
          otherwise
            * --- Try
            local bErr_05292848
            bErr_05292848=bTrsErr
            this.Try_05292848()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              ah_errormsg("Errore nell'approvazione del documento per fatturazione elettronica")
            endif
            bTrsErr=bTrsErr or bErr_05292848
            * --- End
        endcase
      endif
    else
      if this.pPar="EdiClick"
        ah_errormsg("Generazione file EDI non prevista!")
      else
        ah_errormsg("Approvazione fatturazione elettronica non prevista!")
      endif
    endif
  endproc
  proc Try_05292848()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLSEND ="+cp_NullLink(cp_ToStrODBC("F"),'DOC_MAST','MVFLSEND');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MVFLSEND = "F";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_errormsg("Approvato documento da inviare a Fatel","!")
    return


  procedure Page_17
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     LOCAL l_bReOpen 
 l_bReOpen = .f.
    if this.w_OBJ.pdProcesso
      this.w_OBJ.releaseconfig()     
      * --- Se c'� un processo ed � stato eseguito
      if this.w_OBJ.pdExeProcesso OR this.w_OBJ.pdObbligatorio
        * --- Se ho eseguito tutto il treno di stampe con il processo esco,
        *     altrimenti seleziono solo i report che non hanno partecipato al processo documentale
         LOCAL l_numrep, l_count 
 l_numrep = this.w_OBJ.oOrigMReport.GetNumReport()
        l_count=l_numrep
        do while l_count>=1
          if this.w_OBJ.cNomeReport.GetIndexByKey(this.w_OBJ.oOrigMReport.GetKeyReport(l_count))<>-1
            * --- Se gi� processato rimuovo il report
            this.w_OBJ.oOrigMReport.RemoveReport(l_count)     
          endif
          l_count=l_count-1
        enddo
        if !this.w_OBJ.oOrigMReport.IsEmpty()
          l_bReOpen = .t.
        endif
      else
        l_bReOpen = .t.
      endif
    endif
    * --- Riapertura print system dopo processo documentale  
    if l_bReOpen
      * --- Processo non eseguito e non obbligatorio => passa a print system standard
      this.w_OBJ.PdProcesso = .f.
      * --- Ripristina il multireport originale
      this.w_OBJ.cNomeReport = this.w_OBJ.oOrigMReport
      this.w_OBJ.cNomeReport.SetAllPrinter()     
      this.w_STAMPANTE = alltrim (this.w_OBJ.W_DEVICE)
      LoadConfig (this.w_OBJ)
      * --- Aggiorna valori stampante
      this.w_OBJ.oTxt_Copies.enabled = this.w_OBJ.cNomeReport.bNumCopy
      this.w_OBJ.w_Txt_Copies = this.w_OBJ.cNomeReport.nNumCopy
      this.w_OBJ.prCopie = this.w_OBJ.cNomeReport.nNumCopy
      this.w_OBJ.oTxt_Device.enabled = this.w_OBJ.cNomeReport.bStampanti
      this.w_OBJ.w_DEVICE = this.w_OBJ.cNomeReport.cStampante
      this.w_OBJ.prStamp = this.w_OBJ.cNomeReport.cStampante
      * --- Numero copie
      this.w_OBJ.oSplitReport.bNumCopy = this.w_OBJ.cNomeReport.bNumCopy
      this.w_OBJ.oSplitReport.nNumCopy = this.w_OBJ.cNomeReport.nNumCopy
      * --- Zucchetti Aulla-Schedulatore Inizio*
      if TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
        this.w_OBJ.Schedulatore()     
      else
        this.w_OBJ.Refresh()     
      endif
      * --- Zucchetti Aulla-Schedulatore Fine
    else
      this.w_OBJ.CloseAllTables()     
      this.w_OBJ.release()     
    endif
  endproc


  procedure Page_18
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia maschera delle opzioni associata al formato e alla classe
     local l_sPrg , obj ,risultato 
 obj=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_S_PRINT)) 
 l_sPrg="GSUT_BSF(this.w_OBJ, 'OPEN', this.w_OBJ.w_FormFile)"
    risultato=&l_sPrg 
    this.w_OBJ.cOption = risultato
    * --- Stampo su file
    if NOT EMPTY(this.w_OBJ.cOption)
      * --- Sulla maschera per la stampa con opzioni c'� gi� il flag apri dopo generazione
      *     In questo caso non controllo il flag
      this.w_OLDFLOPEN = this.w_OBJ.w_SFFLOPEN
      this.w_OBJ.w_SFFLOPEN = "N"
      this.w_OBJ.oBtn_StpFile.Click()     
      this.w_OBJ.w_SFFLOPEN = this.w_OLDFLOPEN
    endif
    * --- Resetto i parametri
    this.w_OBJ.cOption = ""
  endproc


  procedure Page_19
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     local i_Type
    * --- Invia su File
    if not(empty(this.w_OBJ.w_txt_file))
      this.w_OBJ.w_txt_file = alltrim(this.w_OBJ.w_txt_file)
      if cp_fileexist(this.w_OBJ.w_txt_file)
        if !ah_yesno("File gi� esistente vuoi sovrascrivere?",64)
          i_retcode = 'stop'
          return
        endif
      endif
      if this.w_OBJ.tReport="I"
         l_sPrg="gsir_bir" 
 do (l_sPrg) with this.w_OBJ, this.w_OBJ.cNomeReport.FirstReport(), noParentObject, "F", -1, .null., this.w_OBJ.w_txt_file
      else
        if chknfile(this.w_OBJ.w_txt_file,"F")
          if INLIST( UPPER(ALLTRIM(this.w_OBJ.w_TIPOFILE)),"DBF","SDF","DELIMITED") Or this.w_OBJ.cNomeReport.IsStdFile("FRX", "G")
            GSUT_BXP(this.w_OBJ, 1)
          else
            ah_ErrorMsg("Funzione non disponibile. Utilizzare i formati DBF, SDF o Delimited, se disponibili","stop")
          endif
          if TYPE("This.w_OBJ.nAzAutom")="L" 
 
            do case
              case FILE(this.w_OBJ.w_txt_file)
                if this.oParentObject.w_SFFLOPEN = "S" And ah_YesNo("Visualizzare il file %1 con l'applicazione ad esso associata?", 64, this.w_OBJ.w_txt_file, , , , ,.T.)
                  StaPDF(this.w_OBJ.w_txt_file, "Open", "", .T.)
                endif
              case FILE(ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file)
                if this.oParentObject.w_SFFLOPEN = "S" And ah_YesNo("Visualizzare il file %1 con l'applicazione ad esso associata?", 64, ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file, , , , ,.T.)
                  StaPDF(ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file, "Open", "", .T.)
                endif
                this.w_OBJ.w_txt_file = ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file
              case FILE(ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file))
                if FILE(ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_2."+JUSTEXT(this.w_OBJ.w_txt_file))
                  this.w_FILENAME = ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file)
                  if this.oParentObject.w_SFFLOPEN = "S" And ah_YesNo("Il salvataggio nel formato %1 ha generato pi� files%0Si vuole aprire la cartella di destinazione %2?", 64, JustExt(this.w_FILENAME), JustPath(this.w_FILENAME), , , ,.T.)
                    StaPDF("Explorer.exe","Open", '/n,/select,"'+this.w_FILENAME+'"', .T.)
                  endif
                else
                  this.w_FILENAME = ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file)
                  if this.oParentObject.w_SFFLOPEN = "S" And ah_YesNo("Visualizzare il file %1 con l'applicazione ad esso associata?", 64, this.w_FILENAME, , , , ,.T.)
                    StaPDF(this.w_FILENAME, "Open", "", .T.)
                  endif
                endif
              otherwise
                cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,upper(this.w_OBJ.w_txt_file)),"stop")
            endcase
          endif
        endif
      endif
    endif
  endproc


  procedure Page_20
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_ARCHI="S"
       LOCAL tempstamp 
 tempStamp=PrinterChecked(ALLTRIM(g_PRINTERARCHI))
      if NOT EMPTY(tempStamp)
        do RunRep with this.w_OBJ,this.w_OBJ.oSplitReport, "S", this.w_OBJ.prFile, this.w_OBJ.prCopie, tempStamp, .t.
      else
        cp_ErrorMsg(MSG_PRINTER_ARCHEASY_NOT_FOUND,"stop") 
      endif
    else
      if g_CPIN="N" AND g_DMIP="N" AND g_IZCP$"SA" AND this.w_OBJ.oBtn_WWP_ZCP.enabled
        this.w_IDMSAsa = .T.
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Page_21
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    local i_Type
    * --- Invia su File
    if not(empty(this.w_OBJ.w_txt_file))
      if tReport="I"
        * --- Non gestito in AET
        Return
      else
        if chknfile(this.w_OBJ.w_txt_file,"F")
          if this.w_OBJ.cNomeReport.IsStdFile("FRX", "G")
             local l_sPrg
            this.w_OBJ.prFile = FORCEEXT(this.w_OBJ.w_txt_file, this.w_OBJ.cExtFormat) 
             l_sPrg="GSUT_BXP" 
 do (l_sPrg) with this.w_OBJ, 1 && Creo solo il file 
 l_sPrg=Null
          else
            cp_ErrorMsg(MSG_NO_PRINTFILE_SPECIFIED,"stop")
          endif
          * --- Se il file � stato creato nella temp aggiorna il nome del file sulla print system
          if NOT FILE(this.w_OBJ.w_txt_file) AND NOT FILE(ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file))
            if FILE(ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file)
              this.w_OBJ.w_txt_file = ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file
            endif
          endif
          if NOT FILE(this.w_OBJ.w_txt_file) AND NOT FILE(ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file))
            cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,upper(this.w_OBJ.w_txt_file)),"stop")
          else
             * Creo il nuovo indice 
 l_sPrg="GSUT_BSO" 
 do (l_sPrg) with this.w_OBJ, "GB",this.w_OBJ.w_txt_file 
 l_sPrg=Null
          endif
        endif
      endif
    else
      cp_ErrorMsg(MSG_FILE_NAME_MISSING_QM,"stop")
    endif
  endproc


  procedure Page_22
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziono repor
    this.w_OBJ.SelectReport()     
    if this.w_OBJ.nIndexReport>0
       local i_obj, i_nro, i_oa, i_ovar,i_nCurs, l_reportname, l_Ext, i_workstation 
 l_reportname=this.w_OBJ.cNomeReport.GetReportOrig(this.w_OBJ.nIndexReport) 
 l_Ext=IIF(this.w_OBJ.cNomeReport.GetText(this.w_OBJ.nIndexReport), ".FXP", ".FRX") 
 i_oa = select() 
 i_nCurs = sys(2015) 
 i_workstation=LEFT(SYS(0), RAT(" # ",SYS(0))-1)
      * --- Zucchetti Aulla Inizio : modificata frase Sql per intercettare anche associazioni su report solo testo
      if i_ServerConn[1,2]<>0
        cp_sql(i_ServerConn[1,2],"select * from cpusrrep where repasso="+cp_ToStr(l_reportname+l_Ext)+" and (aziasso="+cp_ToStr(i_codazi)+" or aziasso='          ' or aziasso is null) and (usrasso="+cp_ToStr(i_codute)+" or usrasso=0 or usrasso is null) and (wstasso="+cp_ToStr(i_workstation)+" or wstasso='          ' or wstasso is null)" ,i_nCurs)
      else
        select * from cpusrrep where repasso=l_reportname+l_Ext AND (TRIM(i_codazi)==TRIM(aziasso) OR EMPTY(aziasso)) AND (usrasso=i_codute or usrasso=0) into curs (i_nCurs)
      endif
      * --- Zucchetti Aulla Fine
      if used(i_nCurs)
        * --- Ordino per utente e azienda in modo da prendere sempre prima le associazioni con utente e/o azienda valorizzate 
        *     indipendentemente dall'ordine di caricamento; ordino il cursore fox per evitare problemi con DB2/Oracle e il verso asc o desc
        SELECT * FROM (i_nCurs) ORDER BY usrasso DESC, aziasso DESC INTO CURSOR (i_nCurs)
        if not(eof())
           i_nro = NROASSO 
 i_obj = cpusrrep() 
 i_obj.w_NROASSO = i_nro
          * --- i_obj.QueryKeySet("NROASSO=w_OBJ.w_NROASSO","")
          *     Zucchetti Aulla Inizio : inserito IIF(tReport='S',RptTxt_Search+'.FXP',w_OBJ.cNomeReport+'.FRX'). Prima gestiva solo FXP
          *     i_obj.QueryKeySet("REPASSO="+cp_ToStr(l_reportname+l_Ext),"") 
          *     Zucchetti Aulla Fine
          *     i_obj.LoadRec()
           i_obj.ecpFilter() 
 i_obj.w_REPASSO = l_reportname+l_Ext 
 i_obj.w_USRASSO = NVL(&i_nCurs..USRASSO,0) 
 i_obj.w_AZIASSO = NVL(&i_nCurs..AZIASSO , "") 
 i_obj.ecpSave() 
 i_obj.ecpEdit()
        else
           i_obj = cpusrrep() 
 i_obj.ecpLoad() 
 i_obj.w_USRASSO=i_codute 
 i_oVar=i_obj.GetCtrl("w_USRASSO") 
 i_oVar.check() 
 i_obj.w_AZIASSO=i_codazi
          * --- Zucchetti Aulla Inizio : inserito IIF(tReport='S',RptTxt_Search+'.FXP',w_OBJ.parent.cNomeReport+'.FRX'). Prima gestiva solo FXP
           i_obj.w_REPASSO=l_reportname+l_Ext
          * --- Zucchetti Aulla Fine
           i_obj.w_COPASSO=1
          if not(empty(this.w_OBJ.prStamp))
            i_obj.w_DEVASSO=this.w_OBJ.prStamp
          endif
          i_obj.mCalc(.t.)
        endif
        USE IN SELECT(i_nCurs)
      else
         cp_msg(cp_Translate(MSG_ERROR_OPENING_PRINTING_TABLE))
      endif
       select (i_oa)
    endif
  endproc


  procedure Page_23
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    local i_stp
    * --- Scelta Stampante
    if this.w_OBJ.cNomeReport.GetNumReport()=1
      i_stp = getPrinter()
      if not(empty(i_stp))
        this.w_OBJ.w_DEVICE = i_stp
        this.w_OBJ.prStamp = i_stp
        this.w_OBJ.cNomeReport.SetRepPrinter(i_stp, 1)     
        this.w_OBJ.oSplitReport.SetRepPrinter(i_stp, 1)     
      endif
    else
       LOCAL l_prg 
 l_prg="GSUT_BRL( this.w_OBJ , 'M', this.w_OBJ.cNomeReport)" 
 &l_prg
      this.w_OBJ.oSplitReport.SetInfoFromParent(this.w_OBJ.cNomeReport)     
      this.w_OBJ.oTxt_Copies.enabled = this.w_OBJ.cNomeReport.bNumCopy
      this.w_OBJ.w_Txt_Copies = this.w_OBJ.cNomeReport.nNumCopy
      this.w_OBJ.prCopie = this.w_OBJ.cNomeReport.nNumCopy
      this.w_OBJ.oTxt_Device.enabled = this.w_OBJ.cNomeReport.bStampanti
      this.w_OBJ.w_DEVICE = this.w_OBJ.cNomeReport.cStampante
      this.w_OBJ.prStamp = this.w_OBJ.cNomeReport.cStampante
      * --- Numero copie
      this.w_OBJ.oSplitReport.bNumCopy = this.w_OBJ.cNomeReport.bNumCopy
      this.w_OBJ.oSplitReport.nNumCopy = this.w_OBJ.cNomeReport.nNumCopy
    endif
  endproc


  procedure Page_24
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Clicca Export
    this.w_var = ""
    this.w_oldpath = sys(5)+sys(2003)
    this.w_path_ps = path_print_system()
    if not empty(this.w_path_ps) and directory(this.w_path_ps)
      set default to (this.w_path_ps)
    else
      if DIRECTORY("C:\")
        set default to C:\
      else
        set default to (this.w_oldpath)
      endif
    endif
    this.w_var = putfile("Nome file:", this.w_OBJ.w_Txt_File, this.w_OBJ.cExtFormat)
    set default to (this.w_oldpath)
    if .not. empty(this.w_var)
      this.w_OBJ.w_Txt_File = this.w_var
      this.w_OBJ.prFile = this.w_var
    endif
  endproc


  procedure Page_25
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OBJ.w_Txt_Copies = iif(this.w_OBJ.w_Txt_Copies<=0,1,this.w_OBJ.w_Txt_Copies)
    this.w_OBJ.prCopie = this.w_OBJ.w_Txt_Copies
    * --- Zucchetti Aulla Inizio - Multireport
    this.w_OBJ.cNomeReport.SetAllNumCopy(this.w_OBJ.prCopie)     
    this.w_OBJ.oSplitReport.SetAllNumCopy(this.w_OBJ.prCopie)     
    * --- Zucchetti Aulla Fine - Multireport
  endproc


  procedure Page_26
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     private obj
    this.w_OBJ.cLanguage = RTRIM(STRTRAN(this.w_OBJ.oOpt_Language.Displayvalue,"<<",""))
    this.w_OBJ.w_Opt_Language = RTRIM(STRTRAN(this.w_OBJ.oOpt_Language.Displayvalue,"<<",""))
    * --- Traduco il multireprot
    if !this.w_OBJ.oSplitReport.IsEmpty()
      if this.w_OBJ.pdProcesso AND this.w_OBJ.pdISOLang AND this.w_OBJ.oOpt_Language.ListIndex=1
        this.w_OBJ.oSplitReport.TranslateReport("@@@", this.w_OBJ.cSessionCode)     
      else
        this.w_OBJ.oSplitReport.TranslateReport(this.w_OBJ.cLanguage, this.w_OBJ.cSessionCode)     
      endif
      this.w_OBJ.cNomeReport.CopyTranslateReport(this.w_OBJ.oSplitReport)     
      * --- al cambio lingua, se siamo dentro un processo documentale, risetta l'abilitazione dei bottoni
      if this.w_OBJ.pdProcesso AND this.w_OBJ.pdISOLang 
        this.w_OBJ.SettaConfigurazione(this.w_OBJ.pdProcesso)     
      endif
    endif
  endproc


  procedure Page_27
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Imposta bitmap e titolo bottone CPZ/WE
    this.w_CAMPO = this.w_OBJ.GETCTRL (AH_MSGFORMAT("wwp/zpc"))
    this.w_CAMPO.Picture = IIF(g_WEENABLED="S" AND g_IZCP<>"S" and g_IZCP<>"A","fxwe.bmp",IIF(g_CPIN="N" AND g_DMIP="N" AND g_IZCP$"S-A", "fxzcp.bmp", "fxInf.bmp"))
    this.w_CAMPO.disabledpicture = iif(g_WEENABLED="S" and g_IZCP<>"S" and g_IZCP<>"A","fxzcpd.bmp", "fxInfd.bmp")
    this.w_CAMPO.Caption = iif(g_WEENABLED="S" and g_IZCP<>"S" and g_IZCP<>"A","\<We",IIF(g_CPIN="N" AND g_DMIP="N" AND g_IZCP$"S-A", "\<C.Portal", "\<Infinity"))
    this.w_CAMPO.ToolTipText = CP_TRANSLATE(iif(g_WEENABLED="S" and g_IZCP<>"S"and g_IZCP<>"A", MSG_SENDING_DOCUMENTS_TO_NET_FOLDER, ; 
 IIF(g_CPIN="N" AND g_DMIP="N" AND g_IZCP$"S-A", MSG_SENDING_DOCUMENTS_TO_CORPORATE_PORTAL, MSG_SENDING_DOCUMENTS_TO_INFINITY)))
    this.w_OBJ.o_ZCP=this.w_campo 
 this.w_OBJ.oBtn_StpArchi=this.w_OBJ.GETCTRL (AH_MSGFORMAT("Arc\<heasy")) 
 this.w_OBJ.oOpt_File=this.w_OBJ.GETCTRL ("w_Opt_File") 
 this.w_OBJ.oBtn_StpFileOpt=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_OPTIONS_SAVE)) 
 this.w_OBJ.oOpt_Language=this.w_OBJ.GETCTRL ("w_Opt_Language") 
 this.w_OBJ.oParentobject=oParentobject 
 this.w_OBJ.oBtn_StpOpt=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_OPTIONS_PRINTER)) 
 this.w_OBJ.oBtn_Mail=this.w_OBJ.GETCTRL (AH_MSGFORMAT("E-\<mail")) 
 this.w_OBJ.oBtn_MPEC=this.w_OBJ.GETCTRL (AH_MSGFORMAT("\<PEC")) 
 this.w_OBJ.oBtn_Fax=this.w_OBJ.GETCTRL (AH_MSGFORMAT("\<Fax")) 
 this.w_OBJ.oBtn_Word=this.w_OBJ.GETCTRL (AH_MSGFORMAT("\<Word")) 
 this.w_OBJ.oBtn_Word.caption="" 
 this.w_OBJ.oBtn_Excel=this.w_OBJ.GETCTRL (AH_MSGFORMAT("\<Excel")) 
 this.w_OBJ.oBtn_Graph=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_GRAPH )) 
 this.w_OBJ.oBtn_Plite=this.w_OBJ.GETCTRL (AH_MSGFORMAT("\<PostaLite")) 
 this.w_OBJ.oBtn_Edi=this.w_OBJ.GETCTRL (AH_MSGFORMAT("E\<DI")) 
 this.w_OBJ.oPDBtn_Proc=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_EXEC )) 
 this.w_OBJ.oPDBtn_Status=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_DETAIL)) 
 this.w_OBJ.oTxt_File=this.w_OBJ.GETCTRL ("w_Txt_File")
    this.w_OBJ.oBtn_SFile=this.w_OBJ.opgfrm.page1.opag.oBtn_1_13 
 this.w_OBJ.oBtn_StpFile=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_S_SAVE)) 
 this.w_OBJ.oBtn_SaveInd=this.w_OBJ.GETCTRL (AH_MSGFORMAT("Arc.\<doc.")) 
 this.w_OBJ.oBtn_SStp=this.w_OBJ.GETCTRL ("...") 
 this.w_OBJ.oBtn_Assoc=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_REPORT_BS_PRINTER)) 
 this.w_OBJ.oPDLbl4=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_WORKING_PROGRESS)) 
 this.w_OBJ.oTxt_Copies=this.w_OBJ.GETCTRL ("w_Txt_Copies") 
 this.w_OBJ.oOpt_File=this.w_OBJ.GETCTRL ("w_Opt_File") 
 this.w_OBJ.oTxt_Device=this.w_OBJ.GETCTRL ("w_DEVICE") 
 this.w_OBJ.oBtn_Ante=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_PREVIEW)) 
 this.w_OBJ.oBtn_Esc=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_S_EXIT)) 
 this.w_OBJ.oBtn_Query=this.w_OBJ.GETCTRL (AH_MSGFORMAT("\<Query")) 
 this.w_OBJ.oBtn_Stp=this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_S_PRINT)) 
 this.w_OBJ.oBtn_WWP_ZCP=this.w_OBJ.o_ZCP 
 this.w_OBJ.cSessionCode=SYS(2015)
    this.w_CAMPO = this.w_OBJ.getCTRL (AH_MSGFORMAT("Arc\<heasy"))
    * --- * --- Zucchetti Aulla Inizio
    *       * --- Disabilito il Resize della maschera
    *       * --- Bordi insensibili
     this.w_OBJ.BorderStyle= 2
    * --- Bottone allarga / Restore disabilitato
    this.w_OBJ.Maxbutton=.f.
    * --- * Carico i formati
    *       *CfgCBStmp(this,'CFG', 'oOpt_File')
    *     w_OBJ.oPgFrm.Zorder(1)  
    if (TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S")
       this.w_OBJ.prTyExp = "5" 
 this.w_OBJ.w_Opt_File = "PDF" 
 this.w_OBJ.cExtFormat = "PDF"
      * --- Zucchetti Aulla-Schedulatore Fine
    else
       this.w_OBJ.prTyExp = "1" 
 this.w_OBJ.w_Opt_File = "TXT" 
 this.w_OBJ.cExtFormat = "TXT" 
 this.w_OBJ.oOpt_File.Click
    endif
     this.w_OBJ.prFile=iif (TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S" , FORCEEXT(this.w_OBJ.prFile, "PDF") , path_print_system()+"DEFA"+right("0000"+alltrim(str(i_codute,4,0)),4)+".TXT" ) 
 this.w_OBJ.w_Txt_File=this.w_OBJ.prFile
     this.w_OBJ.oBtn_Query.visible=this.w_OBJ.tReport <> "I" AND cp_isAdministrator(.t.) and vartype(g_Debug_Print)="C" and g_Debug_Print="S" 
 this.w_OBJ.mcalc(.t.)
  endproc


  procedure Page_28
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    LOCAL w_SelOpt
    * --- Zucchetti aulla Inizio - Sezione lingua (zzzz)
    w_SelOpt=CfgCBStmp(this.w_OBJ,IIF(this.w_OBJ.pdProcesso,"PDL","LAN"), "oOpt_Language")
    this.w_OBJ.w_Opt_Language= left (this.w_OBJ.oOpt_Language.DISPLAYVALUE, 3)
    * --- valore di default della combo
    if this.w_OBJ.pdProcesso AND this.w_OBJ.pdISOLang
      if this.w_OBJ.oOpt_Language.ListIndex<=1
        * --- imposta "lingua destinatario" disabilita la preview
        this.w_OBJ.oOpt_Language.ListIndex=1
        * --- lingua destinatario, antiprima non disponibile (per ora)
        *     this.w_OBJ.oBtn_Ante.enabled=(w_SelOpt<>1) and this.w_OBJ.pdnTotDocOk>0 and not(this.w_OBJ.pdEXEProcesso)
      else
        * --- * disabilita esecuzione processo, la lingua deve essere "destinarario"
        *     *this.w_OBJ.oPDBtn_Proc.enabled=(w_SelOpt=0) and this.w_OBJ.pdnTotDocOk>0 and not(this.w_OBJ.pdEXEProcesso)
        *     
      endif
    else
      * --- * lingua del destinatario, utente, ecc
      *     *this.w_OBJ.oOpt_Language.Value = IIF(VARTYPE(g_DEMANDEDLANGUAGE)='C' and Not(EMPTY(g_DEMANDEDLANGUAGE)), g_DEMANDEDLANGUAGE, this.w_OBJ.oOpt_Language.Value)
      *     *--- Zucchetti Aulla Inizio - Correzione lingua
       this.w_OBJ.w_Opt_Language = IIF(VARTYPE(g_DEMANDEDLANGUAGE)="C" and Not(EMPTY(g_DEMANDEDLANGUAGE)) and ALLTRIM(g_DEMANDEDLANGUAGE)+","$ALLTRIM(this.w_OBJ.oOpt_Language.rowsource)+",", g_DEMANDEDLANGUAGE, ; 
 IIF(Not(EMPTY(i_cLANGUAGE)) and ALLTRIM(i_cLANGUAGE)+","$ALLTRIM(this.w_OBJ.oOpt_Language.rowsource)+",", ; 
 i_cLANGUAGE, g_PROJECTLANGUAGE)) 
 LOCAL cGetExtReport 
 cGetExtReport=this.w_OBJ.oSplitReport.GetExtReport(1) 
 this.w_OBJ.w_Opt_Language = IIF(TYPE("cGetExtReport")<>"C" or cGetExtReport="FDF", g_PROJECTLANGUAGE, this.w_OBJ.w_Opt_Language)
      this.w_OBJ.oOpt_Language.DISPLAYVALUE=this.w_OBJ.w_Opt_Language
      * --- Zucchetti Aulla Fine
    endif
     this.w_OBJ.cLanguage=RTRIM(STRTRAN(this.w_OBJ.oOpt_Language.Displayvalue,"<<","")) 
 this.w_OBJ.oOpt_Language.Enabled = .t.
    if this.w_OBJ.oOpt_Language.ListIndex<1
      * --- setta la prima lingua della lista
      this.w_OBJ.oOpt_Language.ListIndex=1
    endif
    * --- font
    if this.w_OBJ.pdProcesso AND w_SelOpt=1
       this.w_OBJ.oOpt_Language.fontsize=8 
 this.w_OBJ.oOpt_Language.fontitalic=true
    else
       this.w_OBJ.oOpt_Language.fontsize=9 
 this.w_OBJ.oOpt_Language.fontitalic=false
    endif
    if !this.w_OBJ.oSplitReport.IsEmpty()
      if this.w_OBJ.pdProcesso
        if this.w_OBJ.pdISOLang AND this.w_OBJ.oOpt_Language.ListIndex=1
          this.w_OBJ.oSplitReport.TranslateReport("@@@", this.w_OBJ.cSessionCode)
        else
          this.w_OBJ.oSplitReport.TranslateReport(this.w_OBJ.cLanguage, this.w_OBJ.cSessionCode)
        endif
        this.w_OBJ.cNomeReport.CopyTranslateReport(this.w_OBJ.oSplitReport)
      else
         this.w_OBJ.oSplitReport.TranslateReport(this.w_OBJ.cLanguage, this.w_OBJ.cSessionCode) 
 this.w_OBJ.cNomeReport.CopyTranslateReport(this.w_OBJ.oSplitReport)
      endif
    endif
    this.w_OBJ.oOpt_Language.FontItalic=.t.
  endproc


  procedure Page_29
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Zucchetti Aulla Inizio
    *     Se report solo testo non � modificabile attraverso il disegnatore di report del Fox
     Local l_dbfname, l_dataname,l_tblname 
 l_dbfname="" 
 l_dataname="" 
 l_tblname=""
    if Upper(this.w_OBJ.cTipoReport)<>"S"
      * --- ***** Zucchetti Aulla Fine
      *     *--- Zucchetti Aulla inizio - Sostituito check tra amministratore e gestione framework
      *     * framework monitor
      *     * Controllo se si hanno i diritti
      *     *if cp_IsAdministrator()
       LOCAL l_UsrRight 
 l_UsrRight = cp_set_get_right()
      if (cp_IsAdministrator() or isalt() or i_MonitorFramework) and m.l_UsrRight >1
        * --- Zucchetti Aulla Inizio - Multireport
         Local l_nNumRep, l_cCurName, l_SelForm 
 Local l_Count, l_NumRow, l_i, bFound 
 l_NumRow=Alen(this.w_OBJ.cNomeReport.aReportList, 1)
        if (cp_IsAdministrator() or isalt() or i_MonitorFramework) And l_NumRow>1 And !this.w_OBJ.cNomeReport.IsEmpty()
           l_SelForm=Createobject("SelForm") 
 l_SelForm.Icon=i_curform.Icon
          l_Count = 1
          do while l_Count<=l_NumRow
            if Not Empty(this.w_OBJ.cNomeReport.GetReport(l_Count))
              if Type("g_MRDuplicateReport")="L" And g_MRDuplicateReport
                l_SelForm.RepList.AddItem(this.w_OBJ.cNomeReport.GetReport(l_Count))
              else
                bFound=.F.
                 l_i = 1
                do while l_i <= l_SelForm.RepList.ListCount
                  if this.w_OBJ.cNomeReport.GetReport(l_Count) == l_SelForm.RepList.List[l_i]
                    bFound=.T.
                  endif
                   l_i = l_i + 1
                enddo
                if !bFound
                  l_SelForm.RepList.AddItem(this.w_OBJ.cNomeReport.GetReport(l_Count))
                endif
              endif
            endif
            l_Count = l_Count + 1
          enddo
          * --- Ordino in modo alfabetico
          if Type("g_MROrderReport")="L" And g_MROrderReport
            l_SelForm.RepList.Sorted=.T.
          endif
          l_SelForm.Show(1) &&Apro la machera in modo modale
          * --- Se esiste la propriet� la maschera non � stata ancora distrutta
          if Type("l_SelForm.nNumReport")="N" 
             l_nNumRep=l_SelForm.nNumReport 
 l_SelForm.Release()
          else
            i_retcode = 'stop'
            return
          endif
          l_SelForm=.Null.
        else
          l_nNumRep=1
        endif
        * --- Zucchetti Aulla Fine - Multireport
        if !this.w_OBJ.cNomeReport.IsEmpty() And cp_IsStdFile(Iif(l_nNumRep>0, this.w_OBJ.cNomeReport.GetReport(l_nNumRep), ""),"FRX")
           l_cCurName=this.w_OBJ.cNomeReport.GetCursorName(l_nNumRep) 
 Select(l_cCurName) 
 Local d 
 d=Set("DATABASE")
          if Not(This.w_OBJ.bModifiedFrx)
             l_dataname=Getenv("TEMP")+"\"+Sys(2015) 
 l_tblname= Sys(2015) 
 l_dbfname=Getenv("TEMP")+"\"+l_tblname
            * --- crea una tabella temporanea in un database temporaneo per la gestione dei nomi lunghi
             Crea Database (l_dataname) 
 Copy To (l_dbfname) Database (l_dataname) 
 Use In Select(l_cCurName)
            if Used("__tmp__")
               Select * From __tmp__ Into Cursor __tmp__temporaneo__ 
 Use In Select("__tmp__")
            endif
            * --- usa la tabella appena creata (in \windows\temp) con alias __tmp__
             Select 0 
 Use (l_dbfname) Alias __tmp__ 
 This.w_OBJ.bModifiedFrx=.T.
          endif
          Local l_j,l_VarEnv,l_bVarEnv
          * --- Setto i_paramqry per permettere l'uso di cp_queryparam()
          i_paramqry=this.w_OBJ.cNomeReport.GetParamqry(l_nNumRep)
          * --- Creo l'environment con le variabili l_
           l_VarEnv=this.w_OBJ.cNomeReport.GetVarEnv(l_nNumRep) 
 l_bVarEnv=.F.
          if Not Isnull(l_VarEnv) And Not l_VarEnv.IsEmpty()
            l_NumVar=l_VarEnv.GetNumVar()
            * --- Creo le variabili
             l_j = 1 
            do while l_j <= l_NumVar
               l_VarName=l_VarEnv.GetVarName(l_j) 
 &l_VarName=l_VarEnv.GetVarValue(l_j)
               l_j = l_j + 1 
            enddo
            l_bVarEnv=.T.
          endif
          Public g_CurCursor
          if USED("__tmp__")
            g_CurCursor="__tmp__"
          else
            g_CurCursor = l_cCurName
          endif
          * --- Monitor FrameWork
           LOCAL l_cFilename 
 l_cFilename = cp_GetStdfile(this.w_OBJ.cNomeReport.GetReport(l_nNumRep),"FRX") 
 modifyreport (this,l_cFilename, l_UsrRight)
          * --- Rivalorizzo le variabili dell' environment in modo da poterle poi interrogare alla fine della stampa
          * --- Se � true ho creato le variabili quindi le risalvo all'interno dell'oggetto
          if l_bVarEnv
             l_j = 1
            do while l_j <= l_NumVar
               l_VarName=l_VarEnv.GetVarName(l_j) 
 l_VarEnv.SetVarValue(&l_VarName,l_j)
               l_j = l_j + 1
            enddo
          endif
          Release g_CurCursor
          if Not(Empty(d))
            * --- potrebbe non esserci un database aperto
            Set Database To (d)
          endif
        else
          cp_errormsg(CP_MSGFORMAT(MSG_FILE__NOT_FOUND_QM,this.w_OBJ.cNomeReport.GetReport(l_nNumRep)),48,"",.F.)
        endif
        * --- Zucchetti Aulla Inizio - FrameWorkMonitor
      else
        cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN )
        * --- Zucchetti Aulla Fine - FrameWorkMonitor
      endif
    else
      cp_errormsg(MSG_REPORT_CHANGEABLE_BY_TEXTREPORTKIT)
    endif
    * --- distrugge la tabella e il database temporanei
    if !Empty(l_dbfname)
       Local N 
 N=Getenv("TEMP") 
 Local d 
 d=Set("DATABASE") 
 Set Database To (l_dataname) 
 Select __tmp__ 
 Use 
 Close Database 
 Delete Database (l_dataname) 
 Select * From (l_dbfname) Into Cursor (l_cCurName) 
 Use In Select(l_tblname) 
 Delete File (l_dbfname+".dbf")
      * --- Se ho dei campi memo � stato creato anche .fpt
      if cp_fileexist(l_dbfname+".fpt",.T.)
        Delete File (l_dbfname+".fpt")
      endif
      if Used("__tmp__temporaneo__")
         Select * From __tmp__temporaneo__ Into Cursor __tmp__ 
 Use In Select("__tmp__temporaneo__")
      endif
    endif
    * --- Zucchetti Aulla Fine
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPar,pMultiReportSplit)
    this.pPar=pPar
    this.pMultiReportSplit=pMultiReportSplit
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STMPFILE'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_STMPFILE')
      use in _Curs_STMPFILE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar,pMultiReportSplit"
endproc
