* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_html_cmd                                                     *
*              IMPOSTAZIONI DI STAMPA HTML                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-21                                                      *
* Last revis.: 2012-04-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_html_cmd",oParentObject,m.pOPER)
return(i_retval)

define class tcp_html_cmd as StdBatch
  * --- Local variables
  pOPER = space(5)
  oBROWSER = .NULL.
  regsh = .NULL.
  w_regpath = space(10)
  w_REGVAL = space(1)
  w_ERR = .f.
  w_LISTAMP = space(250)
  w_OCCURS = 0
  w_MYSTAMP = space(250)
  w_MYTIME = ctot("")
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPOSTAZIONI DI STAMPA HTML
    * --- Help EXECWB:
    *     http://msdn.microsoft.com/en-us/library/aa752117(v=vs.85).aspx
    this.oBROWSER = this.oParentObject.w_BROWSER.oBROWSER
    if this.pOPER<>"INIT"
      this.regsh=createobject("WScript.Shell")
      this.w_regpath = "HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\"
    endif
    do case
      case this.pOPER="INIT"
        this.oBROWSER.Navigate(this.oParentObject.w_pathhtml)     
      case this.pOPER="PREWIEW"
        * --- ANTEPRIMA DI STAMPA DI IE
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oBROWSER.execWB(7,0)     
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER="PRINT"
        * --- COMANDO DI STAMPA DI IE
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oBROWSER.execWB(6,0)     
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER="DONE"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if not this.w_ERR
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOPER="SETUP"
        * --- Ripristina le impostazioni di default
        TRY
        this.regsh.RegDelete(this.w_regpath+"PageSetup_bck\")     
        CATCH
        this.w_ERR = TRUE
        ENDTRY
        TRY
        this.regsh.RegDelete(this.w_regpath+"PageSetup\")     
        CATCH
        this.w_ERR = TRUE
        ENDTRY
        this.oBROWSER.execWB(8,0)     
    endcase
    if this.pOPER<>"INIT"
      ptr = this.regsh
      this.regsh = .null.
      release ptr
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Backup impostazioni di stampa
    TRY
    this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup\")
    CATCH
    * --- Non esiste la chiave, la creo (da IE)
    this.oBROWSER.execWB(8,0)     
    ENDTRY
    TRY
    this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup_bck\")
    this.w_ERR = FALSE
    this.w_LISTAMP = this.regsh.Regread(this.w_regpath+"PageSetup_bck\listamp")
    if not this.oParentObject.w_STAMP $ this.w_LISTAMP
      * --- aggiungo il mio stamp
      this.w_LISTAMP = this.w_LISTAMP+this.oParentObject.w_STAMP
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\listamp", this.w_LISTAMP, "REG_SZ")     
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\"+this.oParentObject.w_STAMP, this.oParentObject.w_TIME, "REG_SZ")     
    endif
    CATCH
    this.w_ERR = TRUE
    this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\listamp", this.oParentObject.w_STAMP, "REG_SZ")     
    this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\"+this.oParentObject.w_STAMP, this.oParentObject.w_TIME, "REG_SZ")     
    ENDTRY
    if this.w_ERR
      * --- footer
      this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup\footer")
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\footer", this.w_REGVAL, "REG_SZ")     
      * --- header
      this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup\header")
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\header", this.w_REGVAL, "REG_SZ")     
      * --- margin_bottom
      this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup\margin_bottom")
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\margin_bottom", this.w_REGVAL, "REG_SZ")     
      * --- margin_left
      this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup\margin_left")
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\margin_left", this.w_REGVAL, "REG_SZ")     
      * --- margin_right
      this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup\margin_right")
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\margin_right", this.w_REGVAL, "REG_SZ")     
      * --- margin_top
      this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup\margin_top")
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\margin_top", this.w_REGVAL, "REG_SZ")     
      * --- Shrink_To_Fit
      this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup\Shrink_To_Fit")
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\Shrink_To_Fit", this.w_REGVAL, "REG_SZ")     
    endif
    this.regsh.Regwrite(this.w_regpath+"PageSetup\footer", "", "REG_SZ")     
    this.regsh.Regwrite(this.w_regpath+"PageSetup\header", "", "REG_SZ")     
    this.regsh.Regwrite(this.w_regpath+"PageSetup\margin_bottom", "0.16667", "REG_SZ")     
    this.regsh.Regwrite(this.w_regpath+"PageSetup\margin_left", "0.16667", "REG_SZ")     
    this.regsh.Regwrite(this.w_regpath+"PageSetup\margin_right", "0.16667", "REG_SZ")     
    this.regsh.Regwrite(this.w_regpath+"PageSetup\margin_top", "0.16667", "REG_SZ")     
    this.regsh.Regwrite(this.w_regpath+"PageSetup\Shrink_To_Fit", "yes", "REG_SZ")     
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripristino impostazioni di stampa
    TRY
    this.w_ERR = FALSE
    this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup_bck\")
    CATCH
    * --- non esiste, non faccio nulla
    this.w_ERR = TRUE
    ENDTRY
    if not this.w_ERR
      TRY
      this.w_ERR = FALSE
      this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup\")
      CATCH
      * --- non esiste, non faccio nulla
      this.w_ERR = TRUE
      ENDTRY
      if not this.w_ERR
        * --- footer
        this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup_bck\footer")
        this.regsh.Regwrite(this.w_regpath+"PageSetup\footer", this.w_REGVAL, "REG_SZ")     
        * --- header
        this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup_bck\header")
        this.regsh.Regwrite(this.w_regpath+"PageSetup\header", this.w_REGVAL, "REG_SZ")     
        * --- margin_bottom
        this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup_bck\margin_bottom")
        this.regsh.Regwrite(this.w_regpath+"PageSetup\margin_bottom", this.w_REGVAL, "REG_SZ")     
        * --- margin_left
        this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup_bck\margin_left")
        this.regsh.Regwrite(this.w_regpath+"PageSetup\margin_left", this.w_REGVAL, "REG_SZ")     
        * --- margin_right
        this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup_bck\margin_right")
        this.regsh.Regwrite(this.w_regpath+"PageSetup\margin_right", this.w_REGVAL, "REG_SZ")     
        * --- margin_top
        this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup_bck\margin_top")
        this.regsh.Regwrite(this.w_regpath+"PageSetup\margin_top", this.w_REGVAL, "REG_SZ")     
        * --- Shrink_To_Fit
        this.w_REGVAL = this.regsh.Regread(this.w_regpath+"PageSetup_bck\Shrink_To_Fit")
        this.regsh.Regwrite(this.w_regpath+"PageSetup\Shrink_To_Fit", this.w_REGVAL, "REG_SZ")     
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina backup
    this.w_LISTAMP = this.regsh.Regread(this.w_regpath+"PageSetup_bck\listamp")
    * --- elimino il mio stamp
    this.w_LISTAMP = STRTRAN(this.w_LISTAMP, this.oParentObject.w_STAMP, "")
    this.regsh.RegDelete(this.w_regpath+"PageSetup_bck\"+this.oParentObject.w_STAMP)     
    * --- elimino tutti gli stamp scaduti
    this.w_OCCURS = OCCURS( ";", this.w_LISTAMP)
    do while this.w_OCCURS>0
      this.w_OCCURS = this.w_OCCURS-1
      this.w_MYSTAMP = SUBSTR(this.w_LISTAMP, this.w_OCCURS*LEN(this.oParentObject.w_STAMP)+1, LEN(this.oParentObject.w_STAMP))
      this.w_MYTIME = this.regsh.Regread(this.w_regpath+"PageSetup_bck\"+this.w_MYSTAMP)
      this.w_MYTIME = CTOT(this.w_MYTIME)
      if DATETIME() - this.w_MYTIME > 4*3600
        * --- CANCELLO GLI STAMP SCADUTI DOPO 4 ORE
        this.w_LISTAMP = STRTRAN(this.w_LISTAMP, this.w_MYSTAMP, "")
      endif
    enddo
    if empty(this.w_LISTAMP)
      * --- ELIMINO TUTTO L'ALBERO DEL BACKUP
      this.regsh.RegDelete(this.w_regpath+"PageSetup_bck\")     
    else
      * --- Aggiorno la lista degli stamp
      this.regsh.Regwrite(this.w_regpath+"PageSetup_bck\listamp", this.w_LISTAMP, "REG_SZ")     
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
