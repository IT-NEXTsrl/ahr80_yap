* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_SEC
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated:     01/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Oggetti e classi per la gestione della sicurezza
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam i_bModal

*** Zucchetti Aulla Inizio
*return(createobject('stdsManageUsrGrp',i_bModal))
LOCAL l_batch
l_batch = "cp_grpusr(.t.)"
&l_batch
Return .T.
*** Zucchetti Aulla Fine

Proc cp_MangageUsrGrp(i_bModal)
	If !(Used('_cpusers_') And Used('_cpgroups_'))
		Return(Createobject('stdsManageUsrGrp',i_bModal))
	Endif
Endproc

Proc cp_ProgAuth()
	Return(Createobject('stdsAuth'))
Endproc

Proc cp_InitTranslate(o)
	Local i_olderr
	i_olderr=On('error')
	On Error =.T.
	o.Caption=cp_Translate(o.Caption)
	o.ToolTipText=cp_Translate(o.ToolTipText)
	On Error &i_olderr
Endproc

Define Class stdsManageUsrGrp As CPSysForm
	Top=10
	Left=10
	Width=775
	Height=335
	*backcolor=rgb(192,192,192)
	oThis=.Null.
	Sizable=.F.
	Caption=MSG_GROUPS_AND_USERS_ADMIN
	bIsAdmin=.F.
	Icon=i_cBmpPath+i_cStdIcon
	Add Object usrgrd As stdsUsrGrd Noinit With Top=5,Left=5,Width=250,Height=Thisform.Height-20
	Add Object grpgrd As stdsGrpGrd Noinit With Top=5,Left=400,Width=250,Height=Thisform.Height-20
	Add Object usradd As cpcommandbutton With Top=10,Left=270,Caption=MSG_NEW_USER,FontBold=.F.,Width=85
	Add Object usrmod As cpcommandbutton With Top=30,Left=270,Caption=MSG_CHANGE_BUTTON ,FontBold=.F.,Width=85
	Add Object usrdel As cpcommandbutton With Top=50,Left=270,Caption=MSG_DELETE_BUTTON ,FontBold=.F.,Width=85
	Add Object grpadd As cpcommandbutton With Top=10,Left=670,Caption=MSG_NEW_GROUP,FontBold=.F.,Width=85
	Add Object grpmod As cpcommandbutton With Top=30,Left=670,Caption=MSG_CHANGE_BUTTON,FontBold=.F.,Width=85
	Add Object grpdel As cpcommandbutton With Top=50,Left=670,Caption=MSG_DELETE_BUTTON,FontBold=.F.,Width=85
	Add Object btnok As cpcommandbutton With Top=Thisform.Height-25,FontBold=.F.,Caption=MSG_OK_BUTTON,Left=670,Width=85
	Add Object langsys As cpcommandbutton With Top=100,FontBold=.F.,Caption=MSG_TRANSLATE,Left=270,Width=85
	Proc Init(i_bModal)
		cp_InitTranslate(This)
		cp_InitTranslate(This.usradd)
		cp_InitTranslate(This.usrmod)
		cp_InitTranslate(This.usrdel)
		cp_InitTranslate(This.grpadd)
		cp_InitTranslate(This.grpmod)
		cp_InitTranslate(This.grpdel)
		cp_InitTranslate(This.btnok)
		cp_InitTranslate(This.langsys)
		If i_bModal
			This.WindowType=1
			This.Closable=.F.
		Endi
		This.oThis=This
		This.usrgrd.Fill()
		This.grpgrd.Fill()
		This.bIsAdmin=cp_IsAdministrator()
		If !This.bIsAdmin and reccount('_cpusers_')>0
			This.usradd.Enabled=.F.
			This.usrdel.Enabled=.F.
			This.grpadd.Enabled=.F.
			This.grpmod.Enabled=.F.
			This.grpdel.Enabled=.F.
			This.langsys.Enabled=.F.
		Endif
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
		This.Show()
	Proc Destroy()
		If Used('_cpusers_')
			Select _cpusers_
			Use
		Endif
		If Used('_cpgroups_')
			Select _cpgroups_
			Use
		Endif
		This.oThis=.Null.
		This.Release()
	Proc usradd.Click()
		Local wnd
		wnd=Createobject('stdsUser',0)
		wnd.Show()
		Thisform.usrgrd.Fill()
	Proc usrmod.Click()
		Local wnd
		If Thisform.bIsAdmin Or _cpusers_.Code=i_codute Or i_codute=0
			wnd=Createobject('stdsUser',_cpusers_.Code,Thisform.bIsAdmin)
			wnd.Show()
			*thisform.usrgrd.Fill()
			* Zucchetti Aulla Inizio Selezione delle aziende in cui abilitare l'utente
			Local I_sPrg
			I_sPrg="GS___KNU"
			Do (I_sPrg) With Alltrim(Str(_cpusers_.Code))+'*'+Alltrim(_cpusers_.Name)
			* Zucchetti Aulla fine Selezione delle aziende in cui abilitare l'utente
			Createobject('sec_deferred_fill',Thisform,3)
		Else
			cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
		Endif
	Proc usrdel.Click()
		*** Zucchetti Aulla Inizio
		Private lRes
		lRes = cp_SQL(i_ServerConn[1,2],"select * from UTE_AZI where UACODUTE="+cp_ToStrODBC(_cpusers_.Code),'appo')
		If lRes<>-1 And Not(Eof())
			CP_MSG(cp_Translate(MSG_CANNOT_DELETE_USERCODE_LINKED_TO_COMPANY))
		Else
			*** Zucchetti Aulla Fine
			If cp_YesNo(cp_MsgFormat(MSG_DELETE_USER__C__QP,cp_ToStr(_cpusers_.Code),Trim(_cpusers_.Name),""),32,.f.)
				*** Zucchetti Aulla Inizio
				* --- Eseguo le cancellazioni sotto transazione
				* --- begin transaction
				cp_BeginTrs()
				*** Zucchetti Aulla Fine
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],"delete from cpusers where code="+cp_ToStrODBC(_cpusers_.Code))
					=cp_SQL(i_ServerConn[1,2],"delete from cpusrgrp where usercode="+cp_ToStrODBC(_cpusers_.Code))
					=cp_SQL(i_ServerConn[1,2],"delete from cpprgsec where usrcode="+cp_ToStrODBC(_cpusers_.Code))
					=cp_SQL(i_ServerConn[1,2],"delete from cpssomap where userid="+cp_ToStrODBC(_cpusers_.Code))
				Else
					Delete From cpusers Where Code=_cpusers_.Code
					Delete From cpusrgrp Where usercode=_cpusers_.Code
					Delete From cpprgsec Where usrcode=_cpusers_.Code
					Delete From cpssomap Where Userid=_cpusers_.Code
				Endif
				***Zucchetti Aulla Inizio
				* --- Lancio il batch della cancellazione utente, questo batch serve per poter eliminare il codice utente da
				* --- tutte quelle gestioni che lo utilizzano
				I_sPrg="GS___BDU"
				Do (I_sPrg) With  .Null., _cpusers_.Code
				* --- commit
				cp_EndTrs(.F.)
				*** Zucchetti Aulla Fine
				Thisform.usrgrd.Fill()
			Endif
			*** Zucchetti Aulla Inizio
		Endif
		If Used('appo')
			Select appo
			Use
		Endif
		*** Zucchetti Aulla Fine
	Proc grpadd.Click()
		Local wnd
		wnd=Createobject('stdsGroup',0)
		wnd.Show()
		Thisform.grpgrd.Fill()
	Proc grpmod.Click()
		Local wnd
		If Thisform.bIsAdmin Or i_codute=0
			wnd=Createobject('stdsGroup',_cpgroups_.Code)
			wnd.Show()
			*thisform.grpgrd.Fill()
			Createobject('sec_deferred_fill',Thisform,4)
		Else
			cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
		Endif
	Proc grpdel.Click()
		If cp_YesNo(cp_MsgFormat(Iif(_cpgroups_.grptype='R',MSG_DELETE_ROLE__C__QP,MSG_DELETE_GROUP__C__QP),cp_ToStr(_cpgroups_.Code),Trim(_cpgroups_.Name)),32,.f.)
			*** Zucchetti Aulla Inizio
			* --- Eseguo le cancellazioni sotto transazione
			* --- begin transaction
			cp_BeginTrs()
			*** Zucchetti Aulla Fine
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"delete from cpgroups where code="+cp_ToStrODBC(_cpgroups_.Code))
				=cp_SQL(i_ServerConn[1,2],"delete from cpusrgrp where groupcode="+cp_ToStrODBC(_cpgroups_.Code))
				=cp_SQL(i_ServerConn[1,2],"delete from cpprgsec where grpcode="+cp_ToStrODBC(_cpgroups_.Code))
			Else
				Delete From cpgroups Where Code=_cpgroups_.Code
				Delete From cpusrgrp Where groupcode=_cpgroups_.Code
				Delete From cpprgsec Where grpcode=_cpgroups_.Code
			Endif
			***Zucchetti Aulla Inizio
			* --- Lancio il batch della cancellazione del gruppo, questo batch serve per poter eliminare il codice gruppo da
			* --- tutte quelle gestioni che lo utilizzano
			I_sPrg="GS___BDG"
			Do (I_sPrg) With .Null., _cpgroups_.Code
			* --- commit
			cp_EndTrs(.F.)
			*** Zucchetti Aulla Fine
			Thisform.grpgrd.Fill()
		Endif
	Proc langsys.Click()
		cp_etrad()
	Proc btnok.Click()
		Thisform.Hide()
		Thisform.Release()
Enddefine

Define Class stdsUsrGrd As Grid
	DeleteMark=.F.
	*recordmark=.f.
	GridLines=0
	RowHeight=16
	cOldIcon=''
	nActiverow = 0
	* --- I positioned on the line again selected
	Procedure AfterRowColChange
		Lparameters nColIndex
		If This.RowColChange=1
			This.nActiverow = This.ActiveRow
		Endif
	Endproc
	Proc Fill()
		Thisform.LockScreen=.T.
		If i_VisualTheme<>-1
			This.Anchor = 65
		Endif
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'select 1 as xmg,code,name from cpusers order by code','_cpusers_')
		Else
			Select 1,Code,Name From cpusers  Where Not(Deleted()) Order By Code Into Cursor _cpusers_
		Endif
		If Not(Used('_cpusers_'))
			Return
		Endif
		This.RecordSource='_cpusers_'
		This.column1.Header1.Caption=''
		*  Allargo colonna bitmap utente
		This.column1.Width=23
		This.column1.Sparse=.F.
		This.column1.AddObject('img','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.img, i_cBmpPath+'user.bmp', 16)
		Else
			This.column1.img.Picture=i_cBmpPath+'user.bmp'
		Endif
		This.column1.img.Visible=.T.
		This.column1.CurrentControl='img'
		This.column2.ControlSource='Code'
		This.column2.InputMask='999999'
		This.column2.RemoveObject('Text1')
		This.column2.AddObject('stdsUsr','stdsTxt')
		This.column2.stdsUsr.Visible=.T.
		This.column2.Width=50
		This.column2.FontBold=.F.
		This.column2.Header1.Caption=cp_Translate(MSG_USERS)
		This.column3.ControlSource='Name'
		This.column3.RemoveObject('Text1')
		This.column3.AddObject('stdsUsr','stdsTxt')
		This.column3.stdsUsr.Visible=.T.
		This.column3.Header1.Caption=cp_Translate(MSG_NAME)
		This.column3.FontBold=.F.
		Thisform.LockScreen=.F.
		This.SetFocus()
		This.ActivateCell(This.nActiverow,2)
	Proc rowclick()
		Thisform.usrmod.Click()
	Proc DragDrop(oSource,nXCoord,nYCoord,nState)
		If oSource.Name=='POSTITEDT'
			oSource.Parent.SendTo(_cpusers_.Code,_cpusers_.Name)
		Endif
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		Do Case
			Case nState=0   && Enter
				This.cOldIcon=oSource.DragIcon
				If oSource.Name=='POSTITEDT'
					oSource.DragIcon=i_cBmpPath+"cross01.cur"
				Endif
			Case nState=1   && Leave
				oSource.DragIcon=This.cOldIcon
		Endcase
Enddefine

Define Class stdsGrpGrd As Grid
	DeleteMark=.F.
	*recordmark=.f.
	GridLines=0
	RowHeight=16
	nActiverow = 0
	* --- I positioned on the line again selected
	Procedure AfterRowColChange
		Lparameters nColIndex
		If This.RowColChange=1
			This.nActiverow = This.ActiveRow
		Endif
	Endproc
	Proc Fill()
		Thisform.LockScreen=.T.
		If i_VisualTheme<>-1
			This.Anchor = 65
		Endif
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'select 1 as xmg,code,name,grptype from cpgroups order by code','_cpgroups_')
		Else
			Select 1,Code,Name,grptype From cpgroups Where Not(Deleted()) Order By Code Into Cursor _cpgroups_
		Endif
		If Not(Used('_cpgroups_'))
			Return
		Endif
		This.RecordSource='_cpgroups_'
		This.column1.Header1.Caption=''
		* --- Allargo colonna bitmap Gruppo
		This.column1.Width=23
		This.column1.Sparse=.F.
		This.column1.AddObject('img','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.img, i_cBmpPath+'group.bmp', 16)
		Else
			This.column1.img.Picture=i_cBmpPath+'group.bmp'
		Endif
		This.column1.img.Visible=.T.
		This.column1.AddObject('img2','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.img2, i_cBmpPath+'role.bmp', 16)
		Else
			This.column1.img2.Picture=i_cBmpPath+'role.bmp'
		Endif
		This.column1.img2.Visible=.T.
		This.column1.DynamicCurrentControl="IIF(_cpgroups_.grptype='R','img2','img')"
		This.column2.RemoveObject('Text1')
		This.column2.AddObject('stdsGrp','stdsTxt')
		This.column2.stdsGrp.Visible=.T.
		This.column2.ControlSource='Code'
		This.column2.Width=50
		This.column2.FontBold=.F.
		This.column2.InputMask='999999'
		This.column2.Header1.Caption=cp_Translate(MSG_GROUPS)
		This.column3.RemoveObject('Text1')
		This.column3.AddObject('stdsGrp','stdsTxt')
		This.column3.stdsGrp.Visible=.T.
		This.column3.ControlSource='Name'
		This.column3.Header1.Caption=cp_Translate(MSG_NAME)
		This.column3.FontBold=.F.
		This.column3.Width=155
		Thisform.LockScreen=.F.
		This.SetFocus()
		This.ActivateCell(This.nActiverow,2)
	Proc rowclick()
		Thisform.grpmod.Click()
Enddefine

Define Class stdsTxt As cptextbox
	BorderWidth=0
	BorderStyle=0
	*  backcolor=rgb(192,192,192)
	nSeconds=0
	Proc DblClick()
		This.Parent.Parent.rowclick()
		* --- gestione del drag&drop
	Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
		If nButton=1
			Nodefault
		Endif
	Proc MouseUp(nButton, nShift, nXCoord, nYCoord)
		Local ot
		If nButton=1
			If Seconds()-0.3<This.nSeconds
				This.DblClick()
			Endif
			This.nSeconds=Seconds()
		Endif
	Proc MouseMove(nButton, nShift, nXCoord, nYCoord)
		Local kf,errsav,oldarea
		Private err
		If nButton=1
			Nodefault
			This.DragIcon=i_cBmpPath+"DragMove.cur"
			This.Drag(1)
		Endif
	Procedure Click
		If Type("this.parent.parent.nActiverow")='N'
			This.Parent.Parent.nActiverow = This.Parent.Parent.ActiveRow
		Endif
		DoDefault()
	Endproc
Enddefine

Define Class stdsUser As cpsysform
	* --- Gestione singolo utente
	Caption=MSG_USER_ADMIN
	*  backcolor=rgb(192,192,192)
	WindowType=1
	Width=515
	Height=270
	Top=80
	Left=20
	nCode=0
	bNew=.F.
	hFld = 24
	bIsAdmin=.F.
	*** Zucchetti Aulla Inizio
	oldName=''
	bChange=.F.	&& Se a .T. forza cambio la password
	*** Zucchetti Aulla Fine
	Icon=i_cBmpPath+i_cStdIcon
	hasenabled=.F.
	Add Object codel  As cplabel With Top=4,Left=5,Width=90,FontBold=.F.,Alignment=1,BackStyle=0,Caption=(MSG_CODE +MSG_FS)
	Add Object unamel As cplabel With Top=30,Left=5,Width=90,FontBold=.F.,Alignment=1,BackStyle=0,Caption=(MSG_NAME+MSG_FS)
	*** Zucchetti Aulla Inizio
	Add Object loginl As cplabel With Top=4,Left=100,Width=90,FontBold=.F.,Alignment=1,BackStyle=0,Caption='Login:'&&(MSG_NAME+MSG_FS)
	*** Zucchetti Aulla FIne
	Add Object pwdl   As cplabel With Top=56,Left=5,Width=90,FontBold=.F.,Alignment=1,BackStyle=0,Caption=(MSG_PASSWORD+MSG_FS)
	Add Object pwdcl  As cplabel With Top=82,Left=5,Width=90,FontBold=.F.,Alignment=1,BackStyle=0,Caption=(MSG_CONFIRM_PWD+ MSG_FS)
	Add Object langl  As cplabel With Top=56,Left=200,Width=60,FontBold=.F.,Alignment=1,BackStyle=0,Caption=(MSG_LANGUAGE+MSG_FS)
	Add Object Code   As cptextbox With Top=4,Left=100,Width=40,FontBold=.F.,Height=This.hFld,InputMask='9999'
	*** Zucchetti Aulla Inizio
	Add Object login  As cptextbox With Top=4,Left=195,Width=105,FontBold=.F.,Height=This.hFld,Format='K'
	* Al posto del codice commentato...
	*add object uname  as cptextbox with top=30,left=100,width=200,fontbold=.f.,height=this.hFld
	Add Object uname  As cptextbox With Top=30,Left=100,Width=200,FontBold=.F.,Height=This.hFld,Enabled=.F.
	*** Zucchetti Aulla Fine
	Add Object pwd    As cptextbox With Top=56,Left=100,Width=100,FontBold=.F.,PasswordChar='*',Height=This.hFld,Format='K'
	Add Object pwdc   As cptextbox With Top=82,Left=100,Width=100,FontBold=.F.,PasswordChar='*',Height=This.hFld,Format='K'
	Add Object Lang   As cptextbox With Top=56,Left=260,Width=40,FontBold=.F.,Height=This.hFld,InputMask='XXX'
	Add Object btnok As cpcommandbutton With Top=5,Left=Thisform.Width-60,FontBold=.F.,Width=50,Caption=MSG_OK_BUTTON
	Add Object btncancel As cpcommandbutton With Top=25,Left=Thisform.Width-60,FontBold=.F.,Width=50,Caption=MSG_CANCEL_BUTTON
	Add Object grpgrd As stdsUsrGrpGrd Noinit With Top=120,Left=5,Width=250,Height=140
	Add Object ungrpgrd As stdsUnUsrGrpGrd Noinit With Top=120,Left=260,Width=250,Height=140
	Add Object grplbl As cplabel With Left=5,Top=100,BackStyle=0,Caption=MSG_BELONGS_TO_CL,FontBold=.F.
	Add Object ugrplbl As cplabel With Left=260,Top=100,BackStyle=0,Caption=MSG_DOES_NOT_BELONG_TO_CL,FontBold=.F.
	Add Object disabledchk As cpcheckbox With Left=260,Top=82,Caption=MSG_DISABLED_MENU,Visible=.F.

	*--- Zucchetti Aulla Inizio - Monitor Framework
	* Aggiunta combobox per politiche di sicurezza (Framework monitor)
	* 1 = NO RIGHT : Nessun diritto
	* 2 = READ ONLY: Solo lettura
	* 3 = R\W USER : Lettura\Scrittura solo utente
	* 4 = R\W ALL  : Lettura\Scrittura per tutti
	* 5 = DEVELOPER: Sviluppatore
	*---

	* variabile locale
	w_user_policy = 1
	Add Object user_policy As cpcombobox With Left=Thisform.Width-170,Top=5,RowSource="NO RIGHT,READ ONLY,R\W USER,R\W ALL,DEVELOPER",RowSourceType=1,Visible=.T.
	Add Object user_policy_label As cplabel With Top=9,Left=300,Width=40,FontBold=.F.,Alignment=1,BackStyle=0,Caption=("Diritti:")
	Proc user_policy.Valid()
		Thisform.w_user_policy = ;
			IIF(This.Value="NO RIGHT",1,;
			IIF(This.Value="READ ONLY",2,;
			IIF(This.Value="R\W USER",3,;
			IIF(This.Value="R\W ALL",4,5))))
	Endproc

	*---
	*   FINE aggiunta combobox per politiche di sicurezza (Framework monitor)
	*--- Zucchetti Aulla Fine - Monitor Framework
	* --- Zucchetti Aulla - Inizio - politiche di sicurezza
	*  proc Init(nCode,bIsAdmin)
	Proc Init(nCode,bIsAdmin,bChange)
		* --- Zucchetti Aulla - Fine - politiche di sicurezza
		cp_InitTranslate(This)
		cp_InitTranslate(This.codel)
		cp_InitTranslate(This.unamel)
		cp_InitTranslate(This.pwdl)
		cp_InitTranslate(This.pwdcl)
		cp_InitTranslate(This.langl)
		cp_InitTranslate(This.btnok)
		cp_InitTranslate(This.btncancel)
		cp_InitTranslate(This.grplbl)
		cp_InitTranslate(This.ugrplbl)
		This.bIsAdmin=bIsAdmin
		* --- Zucchetti Aulla - Inizio - politiche di sicurezza
		This.bChange=bChange
		* --- Zucchetti Aulla - Fine - politiche di sicurezza
		If !bIsAdmin
			This.grpgrd.Enabled=.F.
			This.ungrpgrd.Enabled=.F.
			*** Zucchetti Aulla Inizio
			This.login.Enabled=.F.
		Else
			This.uname.Enabled=.T.
			This.login.Enabled=.T.
			*** Zucchetti Aulla Fine
		ENDIF
	    PUBLIC b_ValidPwd
	    b_ValidPwd=.T.
		If nCode<>0
			This.nCode=nCode
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],'select * from cpusers where code='+cp_ToStr(nCode),'__tmp__')
			Else
				Select * From cpusers Where Code=nCode And Not(Deleted()) Into Cursor __tmp__
			Endif
			If Type('__tmp__.enabled')='C'
				This.hasenabled=.T.
			Endif
			This.Code.Enabled=.F.
			This.uname.Value=__tmp__.Name
			*** Zucchetti Aulla Inizio
			This.login.Value=__tmp__.cp_login
			This.oldName=Trim(Left(This.login.Value,20))
			*** Zucchetti Aulla Fine
			This.pwd.Value= '__non_definita__'+Space(184) &&__tmp__.passwd
			This.pwdc.Value= '__non_definita__'+Space(184) &&__tmp__.passwd
			This.Lang.Value=Nvl(Iif(i_ServerConn[1,6]='DBMaker',__tmp__.Lang,__tmp__.Language),'')
			*--- Zucchetti Aulla Inizio - Monitor Framework
			*Framework popolazione combo diritti
			This.user_policy.Enabled = cp_IsAdministrator()
			This.user_policy.Value = ;
				IIF(__tmp__.USERRIGHT=5,"DEVELOPER",;
				IIF(__tmp__.USERRIGHT=4,"R\W ALL",;
				IIF(__tmp__.USERRIGHT=3,"R\W USER",;
				IIF(__tmp__.USERRIGHT=2,"READ ONLY","NO RIGHT"))))
			This.w_user_policy = __tmp__.USERRIGHT
			*Framework fine popolazione combo diritti
			*--- Zucchetti Aulla Fine - Monitor Framework

			If This.hasenabled
				This.disabledchk.Visible=.T.
				This.disabledchk.Value=Nvl(__tmp__.Enabled,'Y')='N'
				If !bIsAdmin
					This.disabledchk.Enabled=.F.
				Endif
			Endif
			Use
		Else
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],'select max(code)+1 as newcode from cpusers','__tmp__')
			Else
				Select Max(Code)+1 As newcode From cpusers Where Not(Deleted()) Into Cursor __tmp__
			Endif
			If Used('__tmp__')
				This.nCode=Max(1,Nvl(__tmp__.newcode,1))
				Use
			Else
				This.nCode=1
			Endif
			This.bNew=.T.
			This.uname.Value=''
			*** Zucchetti Aulla Inizio
			This.login.Value=''
			This.login.Enabled=.T.
			This.uname.Enabled=.T.
			*** Zucchetti Aulla Fine
			This.pwd.Value=Space(200)
			This.pwdc.Value=Space(200)
			This.Lang.Value=''
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],'select * from cpusers where code='+cp_ToStr(nCode),'__tmp__')
			Else
				Select * From cpusers Where Code=nCode And Not(Deleted()) Into Cursor __tmp__
			Endif
			If Type('__tmp__.enabled')='C'
				This.hasenabled=.T.
				This.disabledchk.Visible=.T.
				This.disabledchk.Value=.F.
			Endif
		Endif
		*----------Zucchetti Aulla Inizio-----------
		*Leggo le impostazioni di sicurezza
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'select PSLUNMIN,PS_NONUM,PS_NORIP,PSATTIVO from POL_SIC','limPwd' )
		Else
			Select PS_NONUM From POL_SIC Into Cursor limPwd
		Endif
		If Used('limPwd')
			Select limPwd
			nocodute=PS_NONUM
			Use
		Endif
		If nocodute='S'
			This.login.Visible=.T.
			This.loginl.Visible=.T.
		Else
			This.login.Visible=.F.
			This.loginl.Visible=.F.
		Endif
		*----------Zucchetti Aulla Fine-----------
		This.Code.Value=This.nCode
		This.grpgrd.Fill()
		This.ungrpgrd.Fill()
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
	Proc Destroy()
		Local d
		If Used('_usrgrp_')
			Select _usrgrp_
			Use
		Endif
		If Used('_unusrgrp_')
			Select _unusrgrp_
			Use
		Endif
	Func Code.When()
		Local r
		r=Reccount('_usrgrp_')=0
		If Not(r)
			This.Enabled=.F.
		Endif
		Return(r)
	Func Code.Valid()
		Local r,i_exp
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'select count(*) as exp from cpusers where code='+cp_ToStr(This.Value),'__tmp__')
		Else
			Select Count(*) As Exp From cpusers Where Code=This.Value And Not(Deleted()) Into Cursor __tmp__
		Endif
		If Type('__tmp__.exp')='C'
			i_exp=Val(__tmp__.Exp)
		Else
			i_exp=__tmp__.Exp
		Endif
		r=(i_exp=0)
		Use
		If r
			Thisform.nCode=This.Value
		Endif
		Return(r)
	Func Lang.Valid()
		* --- 	Verify that the language used is present in the languages
		Local r, sCode
		r=.T.
		If Not Empty( This.Value )
			If i_ServerConn[1,2]<>0
				* --- Zucchetti Aulla Inizio (controllo su tabella lingue dell'applicativo)
				*=cp_SQL(i_ServerConn[1,2],'select code as exp from cplangs where code='+cp_ToStr(this.value),'__tmp__')
				Local i_cTable
				i_cTable = cp_SetAzi(i_TableProp[cp_OpenTable('LINGUE')  ,2])
				=cp_SQL(i_ServerConn[1,2],'select LUCODISO as exp from '+i_cTable+' where LUCODISO='+cp_ToStr(This.Value),'__tmp__')
				* --- Zucchetti Aulla Fine (controllo su tabella lingue dell'applicativo)
			Else
				Select Code As Exp From cplangs Where Code=Alltrim(This.Value) And Not(Deleted()) Into Cursor __tmp__
			Endif
			r=(Reccount('__tmp__')>0)
			sCode=__tmp__.Exp
			Use In Select('__tmp__')
			If Not r
				cp_ErrorMsg(MSG_WRONG_LANGUAGE_CODE)
			Else
				This.Value=sCode
			Endif
		Endif
		Return(r)
	Endproc
	Proc btnok.Click()
		Local un,pw,lg
		*----------Zucchetti Aulla Inizio-----------
		*Leggo le impostazioni di sicurezza
		Local lgn,limitpwd,dataagg,l_ret,l_reccount,nocodute,norip,attivo, cprow
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'select PSLUNMIN,PS_NONUM,PS_NORIP,PSATTIVO from POL_SIC','limPwd' )
		Else
			Select PSLUNMIN,PS_NONUM,PS_NORIP,PSATTIVO From POL_SIC Into Cursor limPwd
		Endif
		If Used('limPwd')
			Select limPwd
			limitpwd=PSLUNMIN
			nocodute=PS_NONUM
			norip=PS_NORIP
			attivo=PSATTIVO
			Use
		ENDIF
		*----------Zucchetti Aulla Fine-----------
		If Thisform.pwd.Value<>Thisform.pwdc.Value
			cp_ErrorMsg(MSG_CONFIRM_PWD_DIFFERENT_FROM_PWD)
		Else
			If Thisform.nCode=0
				cp_ErrorMsg(MSG_USER_CODE_MUST_BE_DIFFERENT_FROM_ZERO)
			ELSE
			  IF !b_ValidPwd
	              cp_Errormsg(MSG_PWD_IS_NOT_SAFE_ENOUGH)
	          ELSE  
				*** Zucchetti Aulla Inizio
				If Empty(Nvl(Thisform.login.Value,'')) And nocodute<>'S'
					Thisform.login.Value=Alltrim(Str(Thisform.nCode)) &&Non gestisco solo la login, quindi valorizzo con il codice utente che � univoco
				Else
					If Empty(Nvl(Thisform.login.Value,''))
						cp_ErrorMsg(MSG_SPECIFY_LOGIN)
					Endif
				Endif
				If Not Empty(Nvl(Thisform.login.Value,''))
					If Len(Alltrim(Thisform.pwd.Value)) < limitpwd And attivo='S'    &&Controllo lunghezza pwd
						cp_ErrorMsg(cp_MsgFormat(MSG_PWD_LESS_THAN__CHARACTERS,Alltrim(Str(limitpwd))),0,'',.f.)
					Else
						pw=Trim(Left(Thisform.pwd.Value,20))
						* --- Se pwd non ripetibile, attiva gest. multi utenza check pwd cambiata
						* --- se utente non modifica la pwd assume come valore '__non_definita__' e quindi
						* --- il controllo scatta cmq.
	                	If norip='S' And ((Thisform.bChange And pw='__non_definita__') Or cp_CheckPwd(Thisform.nCode,pw)) And attivo='S' &&Controllo ripetibilit� pwd
							cp_ErrorMsg(MSG_INSERT_A_DIFFERENT_PWD_FROM_THE_PREVIOUS)
						Else
							lgn=Trim(Left(Thisform.login.Value,20))
							If i_ServerConn[1,2]<>0
								l_ret=cp_SQL(i_ServerConn[1,2],"select cp_login from cpusers where cp_login="+cp_ToStrODBC(lgn))
								l_reccount = Iif(l_ret=-1,0,Reccount())
							Else
								Select cp_login From cpusers Where cp_login=(lgn)
								l_reccount = _Tally
							Endif
							If l_reccount<>0 And (lgn<>Thisform.oldName Or Thisform.bNew) And nocodute='S' &&Controllo se la login utente � gi� esistente
								cp_ErrorMsg(MSG_USER_LOGIN_ALREADY_EXISTING)
							Else
								*** Zucchetti Aulla Fine
								If Thisform.bNew
									*** Zucchetti Aulla Inizio
									* --- Eseguo le creazione sotto transazione
									* --- begin transaction
									cp_BeginTrs()
									*** Zucchetti Aulla Fine
									If i_ServerConn[1,2]<>0
										=cp_SQL(i_ServerConn[1,2],'insert into cpusers (code) values ('+cp_ToStrODBC(Thisform.nCode)+')')
									Else
										Insert Into cpusers (Code) Values (Thisform.nCode)
									Endif
									*** Zucchetti Aulla Inizio
									* --- Lancio il batch della craezione nuovo utente, questo batch serve per poter inserire il codice utente in
									* --- tutte quelle gestioni che lo utilizzano
									I_sPrg="GS___BNU"
									Do (I_sPrg) With  .Null., Thisform.nCode
									* --- commit
									cp_EndTrs(.F.)
									* Selezione delle aziende in cui abilitare l'utente
									I_sPrg="GS___KNU"
									Do (I_sPrg) With Alltrim(Str(Thisform.nCode))+'*'+Alltrim(Thisform.uname.Value)
									*** Zucchetti Aulla Fine
									Thisform.bNew=.F.
								Endif
								un=Trim(Left(Thisform.uname.Value,20))
								pw=Trim(Left(Thisform.pwd.Value,20))
								lg=Trim(Thisform.Lang.Value)
								*** Zucchetti Aulla Inizio
								lgn=Trim(Left(Thisform.login.Value,20))
								* --- Eseguo le creazione sotto transazione
								* --- begin transaction
								cp_BeginTrs()
								*** Zucchetti Aulla Fine
								If pw='__non_definita__'
									If i_ServerConn[1,2]<>0
										Local langfld
										langfld=Iif(i_ServerConn[1,6]='DBMaker',"lang","language")
										*--- Zucchetti Aulla Monitor Framework
										This.Parent.user_policy.Valid()
										* --- Zucchetti Aulla (aggiunta modifica campo Cp_Login)
										=cp_SQL(i_ServerConn[1,2],"update cpusers set name="+cp_ToStrODBC(un)+","+langfld+"="+cp_ToStrODBC(lg)+",cp_login="+cp_ToStrODBC(lgn)+",userright="+cp_ToStrODBC(This.Parent.w_user_policy)+" where code="+cp_ToStrODBC(Thisform.nCode))&& Zucchetti aulla aggiunta scrittura campo Policy
									Else
										Update cpusers Set Name=(un),Language=(lg),cp_login=(lgn),USERRIGHT=(This.Parent.w_user_policy) Where Code=Thisform.nCode && Zucchetti aulla aggiunta scrittura campo Policy
									Endif
									* Zucchetti aulla Framework monitor Inizio, forzo l'aggiornamento della variabile dei diritti
									If Thisform.nCode=i_codute
										=cp_set_get_right(.T.)
									Endif
									* Zucchetti aulla  Framework monitor Fine
								Else
									pw=cp_CriptPwd(Thisform.nCode,pw)
									If i_ServerConn[1,2]<>0
										Local langfld
										langfld=Iif(i_ServerConn[1,6]='DBMaker',"lang","language")
	                                	If i_ServerConn[1,2]<>0
		                                    =cp_SQL(i_ServerConn[1,2],"select MAX(cprownum)+1 as max from CP_PREVPWD where cpusrcod="+cp_ToStrODBC(lgn),"lastCprow" )
	                                    Else
	                                        Select MAX(cprownum)+1 as max From CP_PREVPWD WHERE cpusrcod=(lgn) Into Cursor lastCprow
	                                    ENDIF
	                                	IF USED('lastCprow')
	                                	   SELECT ('lastCprow')
	                                	   GO top
	                                	   cprow=NVL(max,1)
	                                	   USE
	                                	endif
										* Framework monitor (aggiunta riga sotto)
										This.Parent.user_policy.Valid()
										* --- Zucchetti Aulla (aggiunta modifica campo Cp_Login)
										=cp_SQL(i_ServerConn[1,2],"update cpusers set name="+cp_ToStrODBC(un)+",passwd="+cp_ToStrODBC(pw)+","+langfld+"="+cp_ToStrODBC(lg)+",cp_login="+cp_ToStrODBC(lgn)+",userright="+cp_ToStrODBC(This.Parent.w_user_policy)+" where code="+cp_ToStrODBC(Thisform.nCode))
										=cp_SQL(i_ServerConn[1,2],"insert into CP_PREVPWD (cpusrcod,cprownum,cppsswrd)  values ("+cp_ToStrODBC(lgn)+","+cp_ToStrODBC(cprow)+","+cp_ToStrODBC(pw)+")")
										=cp_SQL(i_ServerConn[1,2],"delete from cpssomap where userid="+cp_ToStrODBC(Thisform.nCode))
									Else
										Update cpusers Set Name=(un),PASSWD=(pw),Language=(lg),cp_login=(lgn) Where Code=Thisform.nCode
										INSERT into CP_PREVPWD (cpusrcod,cprownum,cppsswrd) Values ((lgn),(cprow),(pw))
										Delete From cpssomap Where Userid=Thisform.nCode
									Endif
									* Zucchetti aulla Framework monitor Inizio, forzo l'aggiornamento della variabile dei diritti
									If Thisform.nCode=i_codute
										=cp_set_get_right(.T.)
									Endif
									* Zucchetti aulla  Framework monitor Fine
								Endif
								If Thisform.hasenabled
									Local en
									en=Iif(Thisform.disabledchk.Value,'N',' ')
									If i_ServerConn[1,2]<>0
										=cp_SQL(i_ServerConn[1,2],"update cpusers set enabled="+cp_ToStrODBC(en)+" where code="+cp_ToStrODBC(Thisform.nCode))
									Else
										Update cpusers Set Enabled=(en) Where Code=Thisform.nCode
									Endif
								Endif
								*** Zucchetti Aulla Inizio
								* --- Lancio il batch della modifica utente, questo batch serve per poter modificare i dati utente in
								* --- tutte quelle gestioni che lo utilizzano
								I_sPrg="GS___BNU"
								Do (I_sPrg) With  .Null., Thisform.nCode, .T.
								* --- commit
								cp_EndTrs(.F.)
								*** Zucchetti Aulla Fine
								Thisform.Release()
							Endif
						Endif
						*** Zucchetti Aulla Inizio
					Endif
				ENDIF
		      endif
			Endif
		Endif
		*** Zucchetti Aulla Fine
	Proc btncancel.Click()
		Thisform.Release()
	Proc pwd.InteractiveChange()
	     LOCAL mi,ma,sp,nu,c1,c2,c3,ch,i_i
	     LOCAL psw,complex,limitpwd,attivo
	     b_ValidPwd=.f.
	     mi=0
	     ma=0
	     nu=0
	     sp=0
	     c1='abcdefghijklmnopqrstuvwxyz'
	     c2='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	     c3='0123456789'
	     If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'select PSCMPLES from POL_SIC','comPwd' )
	     Else
			Select PSCMPLES From POL_SIC Into Cursor comPwd
		 endif
	     If Used('comPwd')
			Select comPwd
			complex=PSCMPLES
			Use
		 ENDIF
		 If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'select PSLUNMIN,PSATTIVO from POL_SIC','limPwd' )
		 Else
			Select PSLUNMIN,PSATTIVO From POL_SIC Into Cursor limPwd
		 Endif
		 If Used('limPwd')
			Select limPwd
			limitpwd=PSLUNMIN
			attivo=PSATTIVO
			Use
		 ENDIF
		 psw=ALLTRIM(thisform.pwd.value)
		 FOR i_i=1 TO LEN(psw)
		   ch=ALLTRIM(SUBSTR(psw,i_i,1))
		   DO CASE 
		       CASE ch$c1
		              mi=mi+1
		       CASE ch$c2
		              ma=ma+1
		       CASE ch$c3
		              nu=nu+1
		       OTHERWISE 
		              sp=sp+1
		   ENDCASE
		 NEXT 
         mi=IIF(mi>0,1,0)
         ma=IIF(ma>0,1,0)
         nu=IIF(nu>0,1,0)
         sp=IIF(sp>0,1,0)
         IF mi+ma+nu+sp >= complex OR attivo<>'S' OR (LEN(psw)=0 AND limitpwd=0)
            thisform.pwd.backcolor=RGB(0,255,0)
            b_ValidPwd=.T.
         ELSE
            thisform.pwd.backcolor=RGB(255,0,0)
            b_ValidPwd=.F.
         ENDIF 
	ENDPROC
Enddefine

Define Class stdsUsrGrpGrd As Grid
	DeleteMark=.F.
	*recordmark=.f.
	GridLines=0
	RowHeight=16
	cOldIcon=''
	Proc Fill()
		Thisform.LockScreen=.T.
		If i_VisualTheme<>-1
			This.Anchor = 65
		Endif
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select 1 as xmg,code,name,grptype from cpgroups where code in (select groupcode from cpusrgrp where usercode="+cp_ToStr(Thisform.nCode)+") order by code","_usrgrp_")
		Else
			*select 1,code,name from cpgroups where code in (select groupcode from cpusrgrp where usercode=thisform.nCode and not(deleted())) and not(deleted()) order by code into cursor _usrgrp_
			Select 1,Code,Name,grptype From cpgroups Where Code In (Select groupcode From cpusrgrp Where usercode=Thisform.nCode) Order By Code Into Cursor _usrgrp_
		Endif
		This.RecordSource='_usrgrp_'
		This.column1.Header1.Caption=''
		* --- Allargo colonna bitmap gruppo
		This.column1.Width=23
		This.column1.Sparse=.F.
		This.column1.AddObject('img','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.img, i_cBmpPath+'group.bmp', 16)
		Else
			This.column1.img.Picture=i_cBmpPath+'group.bmp'
		Endif
		This.column1.img.Visible=.T.
		This.column1.AddObject('img2','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.img2, i_cBmpPath+'role.bmp', 16)
		Else
			This.column1.img2.Picture=i_cBmpPath+'role.bmp'
		Endif
		This.column1.img2.Visible=.T.
		This.column1.DynamicCurrentControl="IIF(_usrgrp_.grptype='R','img2','img')"
		This.column2.RemoveObject('Text1')
		This.column2.AddObject('stdsUsrGrpGrd','stdsTxt')
		This.column2.stdsUsrGrpGrd.Visible=.T.
		This.column2.ControlSource='Code'
		This.column2.Width=50
		This.column2.FontBold=.F.
		This.column2.InputMask='999999'
		This.column2.Header1.Caption=cp_Translate(MSG_GROUPS)
		This.column3.RemoveObject('Text1')
		This.column3.AddObject('stdsUsrGrpGrd','stdsTxt')
		This.column3.stdsUsrGrpGrd.Visible=.T.
		This.column3.ControlSource='Name'
		This.column3.Header1.Caption=cp_Translate(MSG_NAME)
		This.column3.FontBold=.F.
		This.column3.Width=155
		Thisform.LockScreen=.F.
	Proc rowclick()
		Local u,g
		u=Thisform.nCode
		g=_usrgrp_.Code
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'delete from cpusrgrp where usercode='+cp_ToStr(u)+' and groupcode='+cp_ToStr(g))
		Else
			Delete From cpusrgrp Where usercode=u And groupcode=g
		Endif
		Createobject('sec_deferred_fill',This.Parent,2)
		*this.Fill()
		*thisform.ungrpgrd.Fill()
	Proc DragDrop(oSource,nXCoord,nYCoord,nState)
		If oSource.Name=='stdsUnUsrGrpGrd'
			This.Parent.ungrpgrd.rowclick()
		Endif
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		Do Case
			Case nState=0   && Enter
				This.cOldIcon=oSource.DragIcon
				If oSource.Name=='stdsUnUsrGrpGrd'
					oSource.DragIcon=i_cBmpPath+"cross01.cur"
				Endif
			Case nState=1   && Leave
				oSource.DragIcon=This.cOldIcon
		Endcase
Enddefine

Define Class stdsUnUsrGrpGrd As Grid
	DeleteMark=.F.
	*recordmark=.f.
	GridLines=0
	RowHeight=16
	cOldIcon=''
	Proc Fill()
		Thisform.LockScreen=.T.
		If i_VisualTheme<>-1
			This.Anchor = 65
		Endif
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'select 1 as xmg,code,name,grptype from cpgroups where code not in (select groupcode from cpusrgrp where usercode='+cp_ToStr(Thisform.nCode)+') order by code','_unusrgrp_')
		Else
			*select 1,code,name from cpgroups where code not in (select groupcode from cpusrgrp where usercode=thisform.nCode and not(deleted())) and not(Deleted()) order by code into cursor _unusrgrp_
			Select 1,Code,Name,grptype From cpgroups Where Code Not In (Select groupcode From cpusrgrp Where usercode=Thisform.nCode) Order By Code Into Cursor _unusrgrp_
		Endif
		This.RecordSource='_unusrgrp_'
		This.column1.Header1.Caption=''
		* Allargo colonna bitmap utente
		This.column1.Width=23
		This.column1.Sparse=.F.
		This.column1.AddObject('img','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.img, i_cBmpPath+'group.bmp', 16)
		Else
			This.column1.img.Picture=i_cBmpPath+'group.bmp'
		Endif
		This.column1.img.Visible=.T.
		This.column1.AddObject('img2','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.img2, i_cBmpPath+'role.bmp', 16)
		Else
			This.column1.img2.Picture=i_cBmpPath+'role.bmp'
		Endif
		This.column1.img2.Visible=.T.
		This.column1.DynamicCurrentControl="IIF(_unusrgrp_.grptype='R','img2','img')"
		This.column2.RemoveObject('Text1')
		This.column2.AddObject('stdsUnUsrGrpGrd','stdsTxt')
		This.column2.stdsUnUsrGrpGrd.Visible=.T.
		This.column2.ControlSource='Code'
		This.column2.Width=50
		This.column2.FontBold=.F.
		This.column2.InputMask='999999'
		This.column2.Header1.Caption=cp_Translate(MSG_GROUPS)
		This.column3.RemoveObject('Text1')
		This.column3.AddObject('stdsUnUsrGrpGrd','stdsTxt')
		This.column3.stdsUnUsrGrpGrd.Visible=.T.
		This.column3.ControlSource='Name'
		This.column3.Header1.Caption=cp_Translate(MSG_NAME)
		This.column3.FontBold=.F.
		This.column3.Width=155
		Thisform.LockScreen=.F.
	Proc rowclick()
		Local u,g
		u=Thisform.nCode
		g=_unusrgrp_.Code
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"insert into cpusrgrp (usercode,groupcode) values ("+cp_ToStrODBC(u)+","+cp_ToStrODBC(g)+")")
		Else
			Insert Into cpusrgrp (usercode,groupcode) Values (u,g)
		Endif
		Createobject('sec_deferred_fill',This.Parent,2)
		*this.Fill()
		*thisform.grpgrd.Fill()
	Proc DragDrop(oSource,nXCoord,nYCoord,nState)
		If oSource.Name=='stdsUsrGrpGrd'
			This.Parent.grpgrd.rowclick()
		Endif
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		Do Case
			Case nState=0   && Enter
				This.cOldIcon=oSource.DragIcon
				If oSource.Name=='stdsUsrGrpGrd'
					oSource.DragIcon=i_cBmpPath+"cross01.cur"
				Endif
			Case nState=1   && Leave
				oSource.DragIcon=This.cOldIcon
		Endcase
Enddefine

Define Class stdsGroup As CPsysform
	* --- gestione singolo gruppo
	Caption=MSG_GROUP_ADMIN
	*backcolor=rgb(192,192,192)
	WindowType=1
	Width=515
	Height=270
	Top=80
	Left=20
	nCode=0
	bNew=.F.
	hFld = 24
	Icon=i_cBmpPath+i_cStdIcon
	hasgrptype=.F.
	Add Object codel  As cplabel With Top=7,Left=5,Width=90,FontBold=.F.,Alignment=1,BackStyle=0,Caption=(MSG_CODE+MSG_FS)
	Add Object unamel As cplabel With Top=32,Left=5,Width=90,FontBold=.F.,Alignment=1,BackStyle=0,Caption=(MSG_NAME+MSG_FS)
	Add Object Code   As cptextbox With Top=5,Left=100,Width=40,FontBold=.F.,Height=This.hFld,InputMask='9999'
	Add Object uname  As cptextbox With Top=30,Left=100,Width=200,FontBold=.F.,Height=This.hFld
	Add Object btnok As cpcommandbutton With Top=5,Left=Thisform.Width-60,FontBold=.F.,Width=50,Caption=MSG_OK_BUTTON
	Add Object btncancel As cpcommandbutton With Top=25,Left=Thisform.Width-60,FontBold=.F.,Width=50,Caption=MSG_CANCEL_BUTTON
	Add Object usrgrd As stdsGrpUsrGrd Noinit With Top=120,Left=5,Width=250,Height=140
	Add Object unusrgrd As stdsUnGrpUsrGrd Noinit With Top=120,Left=260,Width=250,Height=140
	Add Object usrlbl As cplabel With Left=5,Top=100,BackStyle=0,Caption=(MSG_BELONGS_TO_CL),FontBold=.F.
	Add Object uusrlbl As cplabel With Left=260,Top=100,BackStyle=0,Caption=(MSG_DOES_NOT_BELONG_TO_CL),FontBold=.F.
	Add Object grptype As cpcombobox With Left=100,Top=60,RowSource="Gruppo,Ruolo",RowSourceType=1,Visible=.F.
	Proc Init(nCode)
		cp_InitTranslate(This)
		cp_InitTranslate(This.codel)
		cp_InitTranslate(This.unamel)
		cp_InitTranslate(This.btnok)
		cp_InitTranslate(This.btncancel)
		cp_InitTranslate(This.usrlbl)
		cp_InitTranslate(This.uusrlbl)
		If nCode<>0
			This.nCode=nCode
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"select * from cpgroups where code="+cp_ToStrODBC(nCode),"__tmp__")
			Else
				Select * From cpgroups Where Code=nCode And Not(Deleted()) Into Cursor __tmp__
			Endif
			This.Code.Enabled=.F.
			This.uname.Value=__tmp__.Name
			If Type('__tmp__.grptype')='C'
				This.hasgrptype=.T.
				This.grptype.Value=Iif(__tmp__.grptype='R','Ruolo','Gruppo')
				This.grptype.Visible=.T.
			Endif
			Use
		Else
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"select max(code)+1 as newcode from cpgroups","__tmp__")
			Else
				Select Max(Code)+1 As newcode From cpgroups Where Not(Deleted()) Into Cursor __tmp__
			Endif
			If Used('__tmp__')
				This.nCode=Max(1,Nvl(__tmp__.newcode,1))
				Use
			Else
				This.nCode=1
			Endif
			This.bNew=.T.
			This.uname.Value=''
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"select * from cpgroups where code="+cp_ToStrODBC(nCode),"__tmp__")
			Else
				Select * From cpgroups Where Code=nCode And Not(Deleted()) Into Cursor __tmp__
			Endif
			If Type('__tmp__.grptype')='C'
				This.hasgrptype=.T.
				This.grptype.Value='Gruppo'
				This.grptype.Visible=.T.
			Endif
		Endif
		This.Code.Value=This.nCode
		This.usrgrd.Fill()
		This.unusrgrd.Fill()
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
	Proc Destroy()
		Local d
		If Used('_usrgrp_')
			Select _usrgrp_
			Use
		Endif
		If Used('_unusrgrp_')
			Select _unusrgrp_
			Use
		Endif
		*if this.bNew
		*  if i_ServerConn[1,2]<>0
		*    =cp_SQL(i_ServerConn[1,2],"insert into cpgroups (code) values ("+cp_ToStr(thisform.nCode)+")")
		*  else
		*    insert into cpgroups (code) values (thisform.nCode)
		*  endif
		*endif
	Func Code.When()
		Local r
		r=Reccount('_usrgrp_')=0
		If Not(r)
			This.Enabled=.F.
		Endif
		Return(r)
	Func Code.Valid()
		Local r,i_exp
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select count(*) as exp from cpgroups where code="+cp_ToStr(This.Value),"__tmp__")
		Else
			Select Count(*) As Exp From cpgroups Where Code=This.Value And Not(Deleted()) Into Cursor __tmp__
		Endif
		If Type('__tmp__.exp')='C'
			i_exp=Val(__tmp__.Exp)
		Else
			i_exp=__tmp__.Exp
		Endif
		r=(i_exp=0)
		Use
		If r
			Thisform.nCode=This.Value
		Endif
		Return(r)
	Proc btnok.Click()
		Local un
		un=Trim(Left(Thisform.uname.Value,20))
		If Thisform.bNew
			*** Zucchetti Aulla Inizio
			* --- Eseguo le creazione sotto transazione
			* --- begin transaction
			cp_BeginTrs()
			*** Zucchetti Aulla Fine
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"insert into cpgroups (code,name) values ("+cp_ToStrODBC(Thisform.nCode)+",'')")
			Else
				Insert Into cpgroups (Code,Name) Values (Thisform.nCode,'')
			Endif
			***Zucchetti Aulla Inizio
			* --- Lancio il batch della creazione nuovo gruppo, questo batch serve per poter inserire il codice gruppo in
			* --- tutte quelle gestioni che lo utilizzano
			I_sPrg="GS___BNG"
			Do (I_sPrg) With  .Null., Thisform.nCode
			* --- commit
			cp_EndTrs(.F.)
			*** Zucchetti Aulla Fine
			Thisform.bNew=.F.
		Endif
		*** Zucchetti Aulla Inizio
		* --- Eseguo le creazione sotto transazione
		* --- begin transaction
		cp_BeginTrs()
		*** Zucchetti Aulla Fine
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"update cpgroups set name="+cp_ToStrODBC(un)+" where code="+cp_ToStrODBC(Thisform.nCode))
		Else
			Update cpgroups Set Name=(un) Where Code=Thisform.nCode
		Endif
		If Thisform.hasgrptype
			Local gt
			gt=Left(Thisform.grptype.Value,1)
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"update cpgroups set grptype="+cp_ToStrODBC(gt)+" where code="+cp_ToStrODBC(Thisform.nCode))
			Else
				Update cpgroups Set grptype=(gt) Where Code=Thisform.nCode
			Endif
		Endif
		***Zucchetti Aulla Inizio
		* --- Lancio il batch della creazione nuovo gruppo, questo batch serve per poter inserire il codice gruppo in
		* --- tutte quelle gestioni che lo utilizzano
		I_sPrg="GS___BNG"
		Do (I_sPrg) With  .Null., Thisform.nCode, .T.
		* --- commit
		cp_EndTrs(.F.)
		*** Zucchetti Aulla Fine
		Thisform.Release()
	Proc btncancel.Click()
		Thisform.Release()
Enddefine

Define Class stdsGrpUsrGrd As Grid
	DeleteMark=.F.
	*recordmark=.f.
	GridLines=0
	RowHeight=16
	cOldIcon=''
	Proc Fill()
		Thisform.LockScreen=.T.
		If i_VisualTheme<>-1
			This.Anchor = 65
		Endif
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select 1 as xmg,code,name from cpusers where code in (select usercode from cpusrgrp where groupcode="+cp_ToStr(Thisform.nCode)+")","_usrgrp_")
		Else
			*select 1,code,name from cpusers where code in (select usercode from cpusrgrp where groupcode=thisform.nCode and not(deleted())) and not(deleted()) into cursor _usrgrp_
			Select 1,Code,Name From cpusers Where Code In (Select usercode From cpusrgrp Where groupcode=Thisform.nCode) Into Cursor _usrgrp_
		Endif
		This.RecordSource='_usrgrp_'
		This.column1.Header1.Caption=''
		* Allargo colonna bitmap
		This.column1.Width=23
		This.column1.Sparse=.F.
		This.column1.AddObject('img','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.img, i_cBmpPath+'user.bmp', 16)
		Else
			This.column1.img.Picture=i_cBmpPath+'user.bmp'
		Endif
		This.column1.img.Visible=.T.
		This.column1.CurrentControl='img'
		This.column2.RemoveObject('Text1')
		This.column2.AddObject('stdsGrpUsrGrd','stdsTxt')
		This.column2.stdsGrpUsrGrd.Visible=.T.
		This.column2.ControlSource='Code'
		This.column2.Width=50
		This.column2.FontBold=.F.
		This.column2.InputMask='999999'
		This.column2.Header1.Caption=cp_Translate(MSG_USERS)
		This.column3.RemoveObject('Text1')
		This.column3.AddObject('stdsGrpUsrGrd','stdsTxt')
		This.column3.stdsGrpUsrGrd.Visible=.T.
		This.column3.ControlSource='Name'
		This.column3.Header1.Caption=cp_Translate(MSG_NAME)
		This.column3.FontBold=.F.
		Thisform.LockScreen=.F.
	Proc rowclick()
		Local u,g
		g=Thisform.nCode
		u=_usrgrp_.Code
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"delete from cpusrgrp where usercode="+cp_ToStr(u)+" and groupcode="+cp_ToStr(g))
		Else
			Delete From cpusrgrp Where usercode=u And groupcode=g
		Endif
		Createobject('sec_deferred_fill',This.Parent,1)
		*this.Fill()
		*thisform.unusrgrd.Fill()
	Proc DragDrop(oSource,nXCoord,nYCoord,nState)
		If oSource.Name=='stdsUnGrpUsrGrd'
			This.Parent.unusrgrd.rowclick()
		Endif
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		Do Case
			Case nState=0   && Enter
				This.cOldIcon=oSource.DragIcon
				If oSource.Name=='stdsUnGrpUsrGrd'
					oSource.DragIcon=i_cBmpPath+"cross01.cur"
				Endif
			Case nState=1   && Leave
				oSource.DragIcon=This.cOldIcon
		Endcase
Enddefine

Define Class stdsUnGrpUsrGrd As Grid
	DeleteMark=.F.
	*recordmark=.f.
	GridLines=0
	RowHeight=16
	cOldIcon=''
	Proc Fill()
		Thisform.LockScreen=.T.
		If i_VisualTheme<>-1
			This.Anchor = 65
		Endif
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select 1 as xmg,code,name from cpusers where code not in (select usercode from cpusrgrp where groupcode="+cp_ToStr(Thisform.nCode)+")","_unusrgrp_")
		Else
			*select 1,code,name from cpusers where code not in (select usercode from cpusrgrp where groupcode=thisform.nCode and not(deleted())) and not(deleted()) into cursor _unusrgrp_
			Select 1,Code,Name From cpusers Where Code Not In (Select usercode From cpusrgrp Where groupcode=Thisform.nCode) Into Cursor _unusrgrp_
		Endif
		This.RecordSource='_unusrgrp_'
		This.column1.Header1.Caption=''
		* --- Zucchetti Aulla Inizio Allargo colonna bitmap
		This.column1.Width=23
		This.column1.Sparse=.F.
		This.column1.AddObject('img','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.img, i_cBmpPath+'user.bmp', 16)
		Else
			This.column1.img.Picture=i_cBmpPath+'user.bmp'
		Endif
		This.column1.img.Visible=.T.
		This.column1.CurrentControl='img'
		This.column2.RemoveObject('Text1')
		This.column2.AddObject('stdsUnGrpUsrGrd','stdsTxt')
		This.column2.stdsUnGrpUsrGrd.Visible=.T.
		This.column2.ControlSource='Code'
		This.column2.Width=50
		This.column2.FontBold=.F.
		This.column2.InputMask='999999'
		This.column2.Header1.Caption=cp_Translate(MSG_USERS)
		This.column3.RemoveObject('Text1')
		This.column3.AddObject('stdsUnGrpUsrGrd','stdsTxt')
		This.column3.stdsUnGrpUsrGrd.Visible=.T.
		This.column3.ControlSource='Name'
		This.column3.Header1.Caption=cp_Translate(MSG_NAME)
		This.column3.FontBold=.F.
		Thisform.LockScreen=.F.
	Proc rowclick()
		Local u,g
		g=Thisform.nCode
		u=_unusrgrp_.Code
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"insert into cpusrgrp (usercode,groupcode) values ("+cp_ToStrODBC(u)+","+cp_ToStrODBC(g)+")")
		Else
			Insert Into cpusrgrp (usercode,groupcode) Values (u,g)
		Endif
		Createobject('sec_deferred_fill',This.Parent,1)
		*this.Fill()
		*thisform.usrgrd.Fill()
	Proc DragDrop(oSource,nXCoord,nYCoord,nState)
		If oSource.Name=='stdsGrpUsrGrd'
			This.Parent.usrgrd.rowclick()
		Endif
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		Do Case
			Case nState=0   && Enter
				This.cOldIcon=oSource.DragIcon
				If oSource.Name=='stdsGrpUsrGrd'
					oSource.DragIcon=i_cBmpPath+"cross01.cur"
				Endif
			Case nState=1   && Leave
				oSource.DragIcon=This.cOldIcon
		Endcase
Enddefine

Define Class sec_deferred_fill As Timer
	* --- solito trucco per poter eseguire delle operazioni in ritardo
	Interval=50
	oThis=.Null.
	oObj=.Null.
	nType=0
	Proc Init(oObj,nType)
		This.oObj=oObj
		This.oThis=This
		This.nType=nType
	Proc Timer()
		This.oObj.btnok.SetFocus()
		Do Case
			Case This.nType=1
				This.oObj.unusrgrd.Fill()
				This.oObj.usrgrd.Fill()
			Case This.nType=2
				This.oObj.ungrpgrd.Fill()
				This.oObj.grpgrd.Fill()
			Case This.nType=3
				This.oObj.usrgrd.Fill()
			Case This.nType=4
		Endcase
		This.oThis=.Null.
Enddefine

*
* --- Maschera di selezione iniziale utente
*

Define Class stdsUserAuth As CPsysform
	Width=375
	Height=80
	AutoCenter=.T.
	*  backcolor=rgb(192,192,192)
	WindowType=1
	Caption=MSG_INSERT_PWD
	* ---
	uPwd=''
	bOk=.F.
	nCnt=0
	hFld=24
	Icon=i_cBmpPath+i_cStdIcon
	cLang=''
	validsso=.F.
	Add Object img As Image With Top=10,Left=5,Picture=i_cBmpPath+'questg.bmp',Height=50
	Add Object usrl As cplabel With Left=50,Top=10,Width=63,Caption=(MSG_USER+MSG_FS),Alignment=1,FontBold=.F.,BackStyle=0
	Add Object pwdl As cplabel With Left=50,Top=37,Width=63,Caption=(MSG_PASSWORD+MSG_FS),Alignment=1,FontBold=.F.,BackStyle=0
	Add Object usr As cptextbox With Left=115,Top=10,Width=40,FontBold=.F.,Value=0,Height=This.hFld,InputMask="9999"
	Add Object pwd As cptextbox With Left=115,Top=37,FontBold=.F.,PasswordChar='*',Value=Space(200),Height=This.hFld
	Add Object des As cptextbox With Left=160,Top=10,Width=200,FontBold=.F.,Enabled=.F.,Height = This.hFld
	Add Object btnok As cpcommandbutton With Top=55,Left=Thisform.Width-115,FontBold=.F.,Width=50,Caption=MSG_OK_BUTTON
	Add Object btncancel As cpcommandbutton With Top=55,Left=Thisform.Width-60,FontBold=.F.,Width=50,Caption=MSG_CANCEL_BUTTON
	Add Object ssoidlbl As Label With Left=25,Top=79,Caption='SingleSignOn:',Width=88,Alignment=1,Visible=.F.
	Add Object ssoid As TextBox With Top=75,Left=115,Width=250,Height=25,Visible=.F.,Enabled=.F.
	Add Object ssoidmap As Checkbox With Top=75,Left=5,Width=15,Height=25,Visible=.F.,Enabled=.F.,Caption='',Value=.F.
	Proc Init()
		cp_InitTranslate(This)
		cp_InitTranslate(This.usrl)
		cp_InitTranslate(This.pwd)
		cp_InitTranslate(This.btnok)
		cp_InitTranslate(This.btncancel)
		With This
			If Vartype(i_ThemesManager)=='O'
				i_ThemesManager.PutBmp(.img, i_cBmpPath+'quest.bmp', 38)
			Endif
		Endwith
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
		Return
	Procedure Show()
		DoDefault()
		If Type('i_cSsoid')='C' And !Empty(i_cSsoid) And cp_VerifySingleSignOn(i_cSsoid,i_xSsoCredentials)
			This.validsso=.T.
			This.ssoid.Visible=.T.
			This.ssoidlbl.Visible=.T.
			This.ssoid.Value=i_cSsoid
			This.ssoidmap.Visible=.T.
			This.ssoidmap.Enabled=.T.
			This.ssoidmap.Value=.T.
			This.Height=100
			Local i_usr
			i_usr=cp_SingleSignOnUser(i_cSsoid)
			If i_usr<>0
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],'select * from cpusers where code='+cp_ToStr(i_usr),'__tmp__')
				Else
					Select * From cpusers Where Code=i_usr Into Cursor __tmp__
				Endif
				i_cLanguage=Nvl(Iif(i_ServerConn[1,6]='DBMaker',__tmp__.Lang,__tmp__.Language),'')
				*** controllo se la lingua dell'utente � abilitata alla traduzione del dato
				If islanguagetranslate(i_cLanguage)
					i_cLanguageData=i_cLanguage
				Else
					i_cLanguageData=Space(3)
				Endif
				i_codute=i_usr
				Use
				This.Release()
			Endif
		Else
			This.validsso=.F.
		Endif
		Return
	Proc usr.Valid()
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],'select * from cpusers where code='+cp_ToStr(This.Value),'__tmp__')
		Else
			Select * From cpusers Where Code=This.Value Into Cursor __tmp__
		Endif
		Thisform.des.Value=__tmp__.Name
		Thisform.uPwd=__tmp__.PASSWD
		Thisform.bOk=Reccount('__tmp__')<>0
		Thisform.cLang=Nvl(Iif(i_ServerConn[1,6]='DBMaker',__tmp__.Lang,__tmp__.Language),'')
		If Type('__tmp__.enabled')='C' And __tmp__.Enabled='N'
			CP_MSG(cp_Translate(MSG_DISABLED_MENU),.F.)
			Thisform.usr.Value=0
			Thisform.des.Value=''
			Thisform.uPwd=''
		Endif
		If Not(Thisform.bOk) And Thisform.usr.Value<>0
			CP_MSG(cp_Translate(MSG_USER_DOES_NOT_EXIST),.F.)
			Thisform.usr.Value=0
		Endif
		Use
		Return(.T.)
	Proc btnok.Click()
		Thisform.nCnt=Thisform.nCnt+1
		Thisform.bOk=cp_CheckPwd(Thisform.usr.Value,Thisform.pwd.Value,Thisform.uPwd)
		If Thisform.bOk
			i_codute = Thisform.usr.Value
			If Not Empty(Thisform.cLang)
				i_cLanguage = Thisform.cLang
				*** controllo se la lingua dell'utente � abilitata alla traduzione del dato
				If islanguagetranslate(i_cLanguage)
					i_cLanguageData=i_cLanguage
				Else
					i_cLanguageData=Space(3)
				Endif
			Endif
			* --- aggiunge la mappatura di signle-sign-on
			If Thisform.validsso And Thisform.ssoidmap.Value
				cp_AddSingleSignOnMap(i_codute,i_cSsoid)
			Endif
			Thisform.Release()
		Else
			CP_MSG(cp_Translate(MSG_WRONG_PWD),.F.)
		Endif
		If Thisform.nCnt>=5
			CP_MSG(cp_Translate(MSG_TOO_MANY_TRIALS),.F.)
			i_codute = 0
			Thisform.Release()
		Endif
	Proc btncancel.Click()
		i_codute = 0
		Thisform.Release()
Enddefine

Define Class stdsProgAuth As CPsysform
	cProgName=''
	*  backcolor=rgb(192,192,192)
	Width=410
	Height=470
	Caption=(MSG_PROC_SECURITY)
	oThis=.Null.
	cCursor=''
	cCursorRoot=''
	Top=10
	Left=10
	Icon=i_cBmpPath+i_cStdIcon
	Add Object secgrd As stdsProgGrd Noinit With Left=5,Top=5,Width=400,Height=200
	Add Object btndel As cpcommandbutton With Left=5,FontBold=.F.,Top=210,Caption=MSG_DELETE_BUTTON,Height=20,Width=50
	Add Object btnok As cpcommandbutton With Left=295,FontBold=.F.,Top=210,Caption=MSG_OK_BUTTON,Height=20,Width=50
	Add Object btncancel As cpcommandbutton With Left=350,FontBold=.F.,Top=210,Caption=MSG_CANCEL_BUTTON,Height=20,Width=50
	Add Object CodRoot  As cplabel With Top=240,Left=5,Width=255,FontBold=.F.,Alignment=0,BackStyle=0,Caption=(MSG_PROC_SECURITY_MODULE)
	Add Object secgrdRoot As stdsProgGrdRoot Noinit With Left=5,Top=260,Width=400,Height=200
	Proc Init(cProgName)
		cp_InitTranslate(This.Caption)
		cp_InitTranslate(This.btndel)
		cp_InitTranslate(This.btnok)
		cp_InitTranslate(This.btncancel)
		If !cp_IsAdministrator()
			cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
			Return
		Endif
		This.cProgName=cProgName
		This.oThis=This
		This.cCursor=Sys(2015)
		This.cCursorRoot=Sys(2015)
		This.Caption=This.Caption+' '+cProgName
		Create Cursor (This.cCursor) (dummy N(1),gu_code N(6),gu_name C(20),sec1 N(6),sec2 N(6), sec3 N(6),sec4 N(6))
		* inserisco i dati per nome programma
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select 0 as gu,cpprgsec.grpcode as gu_code,sec1,sec2,sec3,sec4,cpgroups.name as name from cpprgsec,cpgroups where progname="+cp_ToStr(This.cProgName)+" and cpprgsec.grpcode=cpgroups.code"+;
				" union select 1 as gu,cpprgsec.usrcode as gu_code,sec1,sec2,sec3,sec4,cpusers.name as name from cpprgsec,cpusers where progname="+cp_ToStr(This.cProgName)+" and cpprgsec.usrcode=cpusers.code order by 1,2","__tmp__")
		Else
			Select 0 As gu,cpprgsec.grpcode As gu_code,sec1,sec2,sec3,sec4,cpgroups.Name As Name From cpprgsec,cpgroups Where cpprgsec.progname=cProgName And cpprgsec.grpcode=cpgroups.Code ;
				UNION Select 1 As gu,cpprgsec.usrcode As gu_code,sec1,sec2,sec3,sec4,cpusers.Name As Name From cpprgsec,cpusers Where cpprgsec.progname=cProgName And cpprgsec.usrcode=cpusers.Code ;
				order By 1,2 Into Cursor __tmp__
		Endif
		Local s1,s2,s3,s4
		Scan
			s1=Iif(__tmp__.sec1=-1000,2,__tmp__.sec1)
			s2=Iif(__tmp__.sec2=-1000,2,__tmp__.sec2)
			s3=Iif(__tmp__.sec3=-1000,2,__tmp__.sec3)
			s4=Iif(__tmp__.sec4=-1000,2,__tmp__.sec4)
			If __tmp__.gu=0
				Insert Into (This.cCursor) (dummy,gu_code,gu_name,sec1,sec2,sec3,sec4) Values (0,__tmp__.gu_code,__tmp__.Name,s1,s2,s3,s4)
			Else
				Insert Into (This.cCursor) (dummy,gu_code,gu_name,sec1,sec2,sec3,sec4) Values (1,__tmp__.gu_code,__tmp__.Name,s1,s2,s3,s4)
			Endif
		Endscan
		Use
		Select (This.cCursor)
		Go Top
		This.secgrd.Fill()
		* inserisco i dati per radice programma
		Local i_shortCode,i_p
		i_p=At("_",This.cProgName)
		If i_p<>0
			i_shortCode=Left(This.cProgName,i_p-1)+'*'
		Else
			i_shortCode=This.cProgName
		Endif
		This.CodRoot.Caption=cp_MsgFormat(MSG_PROC_SECURITY_MODULE,i_shortCode)
		Create Cursor (This.cCursorRoot) (dummy N(1),gu_code N(6),gu_name C(20),sec1 N(6),sec2 N(6), sec3 N(6),sec4 N(6))
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select 0 as gu,cpprgsec.grpcode as gu_code,sec1,sec2,sec3,sec4,cpgroups.name as name from cpprgsec,cpgroups where progname="+cp_ToStr(i_shortCode)+" and cpprgsec.grpcode=cpgroups.code"+;
				" union select 1 as gu,cpprgsec.usrcode as gu_code,sec1,sec2,sec3,sec4,cpusers.name as name from cpprgsec,cpusers where progname="+cp_ToStr(i_shortCode)+" and cpprgsec.usrcode=cpusers.code order by 1,2","__tmp__")
		Else
			Select 0 As gu,cpprgsec.grpcode As gu_code,sec1,sec2,sec3,sec4,cpgroups.Name As Name From cpprgsec,cpgroups Where cpprgsec.progname=i_shortCode And cpprgsec.grpcode=cpgroups.Code ;
				UNION Select 1 As gu,cpprgsec.usrcode As gu_code,sec1,sec2,sec3,sec4,cpusers.Name As Name From cpprgsec,cpusers Where cpprgsec.progname=i_shortCode And cpprgsec.usrcode=cpusers.Code ;
				order By 1,2 Into Cursor __tmp__
		Endif
		Local s1,s2,s3,s4
		Scan
			s1=Iif(__tmp__.sec1=-1000,2,__tmp__.sec1)
			s2=Iif(__tmp__.sec2=-1000,2,__tmp__.sec2)
			s3=Iif(__tmp__.sec3=-1000,2,__tmp__.sec3)
			s4=Iif(__tmp__.sec4=-1000,2,__tmp__.sec4)
			If __tmp__.gu=0
				Insert Into (This.cCursorRoot) (dummy,gu_code,gu_name,sec1,sec2,sec3,sec4) Values (0,__tmp__.gu_code,__tmp__.Name,s1,s2,s3,s4)
			Else
				Insert Into (This.cCursorRoot) (dummy,gu_code,gu_name,sec1,sec2,sec3,sec4) Values (1,__tmp__.gu_code,__tmp__.Name,s1,s2,s3,s4)
			Endif
		Endscan
		Use
		Select (This.cCursorRoot)
		Go Top
		This.secgrdRoot.Fill()
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
		This.Show()
	Proc Destroy()
		If Used(This.cCursor)
			Select (This.cCursor)
			Use
		Endif
		If Used(This.cCursorRoot)
			Select (This.cCursorRoot)
			Use
		Endif
		This.oThis=.Null.
	Proc btndel.Click()
		Local N,p,d
		Select (Thisform.cCursor)
		If cp_YesNo(cp_MsgFormat(MSG_CONFIRM_DELETING__C__QP,Alltrim(Str(gu_code)),gu_name),32,.f.)
			d=dummy
			N=gu_code
			p=Thisform.cProgName
			Delete
			If d=0
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],"delete from cpprgsec where progname="+cp_ToStrODBC(p)+" and grpcode="+cp_ToStrODBC(N))
				Else
					Delete From cpprgsec Where progname=p And grpcode=N
				Endif
			Else
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],"delete from cpprgsec where progname="+cp_ToStrODBC(p)+" and usrcode="+cp_ToStrODBC(N))
				Else
					Delete From cpprgsec Where progname=p And usrcode=N
				Endif
			Endif
			Thisform.secgrd.Refresh()
		Endif
	Proc btnok.Click()
		Local N,p,s1,s2,s3,s4
		Select (Thisform.cCursor)
		p=Thisform.cProgName
		Scan
			N=gu_code
			s1=Iif(sec1=2,-1000,sec1)
			s2=Iif(sec2=2,-1000,sec2)
			s3=Iif(sec3=2,-1000,sec3)
			s4=Iif(sec4=2,-1000,sec4)
			If dummy=0
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],"update cpprgsec set sec1="+cp_ToStrODBC(s1)+",sec2="+cp_ToStrODBC(s2)+",sec3="+cp_ToStrODBC(s3)+",sec4="+cp_ToStrODBC(s4)+" where progname="+cp_ToStrODBC(p)+" and grpcode="+cp_ToStrODBC(N))
				Else
					Update cpprgsec Set sec1=s1,sec2=s2,sec3=s3,sec4=s4 Where progname=p And grpcode=N
				Endif
			Else
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],"update cpprgsec set sec1="+cp_ToStrODBC(s1)+",sec2="+cp_ToStrODBC(s2)+",sec3="+cp_ToStrODBC(s3)+",sec4="+cp_ToStrODBC(s4)+" where progname="+cp_ToStrODBC(p)+" and usrcode="+cp_ToStrODBC(N))
				Else
					Update cpprgsec Set sec1=s1,sec2=s2,sec3=s3,sec4=s4 Where progname=p And usrcode=N
				Endif
			Endif
		Endscan
		Thisform.Release()
	Proc btncancel.Click
		Thisform.Release()
Enddefine

Define Class stdsProgGrd As Grid
	DeleteMark=.F.
	FontBold=.F.
	GridLines=0
	RowHeight=18
	cOldIcon=''
	Proc Fill()
		This.RecordSource=Thisform.cCursor
		This.column1.Header1.Caption=''
		* --- Zucchetti Aulla Inizio Allargo colonna bitmap
		This.column1.Width=23
		This.column1.Sparse=.F.
		This.column1.AddObject('grp','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.grp, i_cBmpPath+'group.bmp', 16)
		Else
			This.column1.grp.Picture=i_cBmpPath+'group.bmp'
		Endif
		This.column1.grp.Visible=.T.
		This.column1.AddObject('usr','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.usr, i_cBmpPath+'user.bmp', 16)
		Else
			This.column1.usr.Picture=i_cBmpPath+'user.bmp'
		Endif
		This.column1.usr.Visible=.T.
		This.column1.DynamicCurrentControl="IIF(dummy=0,'grp','usr')"
		This.column2.Header1.Caption=cp_Translate(MSG_CODE)
		This.column2.Width=40
		This.column2.text1.Enabled=.F.
		This.column3.Header1.Caption=cp_Translate(MSG_NAME)
		This.column3.Width=105
		This.column3.text1.Enabled=.F.
		This.column4.Width=45
		This.column4.AddObject('chk','seccheckbox')
		This.column4.Sparse=.F.
		This.column4.chk.Visible=.T.
		This.column4.chk.Caption=''
		This.column4.CurrentControl='chk'
		This.column4.Header1.Caption=cp_Translate(MSG_ENTER)

		This.column5.Width=55
		This.column5.AddObject('chk','seccheckbox')
		This.column5.Sparse=.F.
		This.column5.chk.Visible=.T.
		This.column5.chk.Caption=''
		This.column5.CurrentControl='chk'
		This.column5.Header1.Caption=cp_Translate(MSG_INSERT)
		This.column6.Width=50
		This.column6.AddObject('chk','seccheckbox')
		This.column6.Sparse=.F.
		This.column6.chk.Visible=.T.
		This.column6.chk.Caption=''
		This.column6.CurrentControl='chk'
		This.column6.Header1.Caption=cp_Translate(MSG_CHANGE)
		This.column7.Width=55
		This.column7.AddObject('chk','seccheckbox')
		This.column7.Sparse=.F.
		This.column7.chk.Visible=.T.
		This.column7.chk.Caption=''
		This.column7.CurrentControl='chk'
		This.column7.Header1.Caption=cp_Translate(MSG_DELETE)
	Proc DragDrop(oSource,nXCoord,nYCoord,nState)
		Local p,g,N
		If oSource.Name=='STDSGRP'
			Select (This.RecordSource)
			Locate For gu_code=_cpgroups_.Code And dummy=0
			If Found()
				CP_MSG(cp_Translate(MSG_COMMAND_IS_DUPLICATED),.F.)
				Return
			Endif
			p=Thisform.cProgName
			g=_cpgroups_.Code
			N=_cpgroups_.Name
			Insert Into (Thisform.cCursor) (dummy,gu_code,gu_name,sec1,sec2,sec3,sec4) Values (0,g,N,0,0,0,0)
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"insert into cpprgsec (progname,grpcode,usrcode,sec1,sec2,sec3,sec4) values ("+cp_ToStr(p)+","+cp_ToStr(g)+",0,0,0,0,0)")
			Else
				Insert Into cpprgsec (progname,grpcode,usrcode,sec1,sec2,sec3,sec4) Values (p,g,0,0,0,0,0)
			Endif
			This.Refresh()
		Endif
		If oSource.Name=='STDSUSR'
			Select (This.RecordSource)
			Locate For gu_code=_cpusers_.Code And dummy=1
			If Found()
				CP_MSG(cp_Translate(MSG_COMMAND_IS_DUPLICATED),.F.)
				Return
			Endif
			p=Thisform.cProgName
			g=_cpusers_.Code
			N=_cpusers_.Name
			Insert Into (Thisform.cCursor) (dummy,gu_code,gu_name,sec1,sec2,sec3,sec4) Values (1,g,N,2,2,2,2)
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"insert into cpprgsec (progname,grpcode,usrcode,sec1,sec2,sec3,sec4) values ("+cp_ToStr(p)+",0,"+cp_ToStr(g)+",-1000,-1000,-1000,-1000)")
			Else
				Insert Into cpprgsec (progname,grpcode,usrcode,sec1,sec2,sec3,sec4) Values (p,0,g,-1000,-1000,-1000,-1000)
			Endif
			This.Refresh()
		Endif
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		Do Case
			Case nState=0   && Enter
				This.cOldIcon=oSource.DragIcon
				If oSource.Name=='STDSGRP'
					oSource.DragIcon=i_cBmpPath+"cross01.cur"
				Endif
			Case nState=1   && Leave
				oSource.DragIcon=This.cOldIcon
		Endcase
Enddefine

Define Class stdsProgGrdRoot As Grid
	DeleteMark=.F.
	FontBold=.F.
	GridLines=0
	RowHeight=18
	cOldIcon=''
	Proc Fill()
		This.RecordSource=Thisform.cCursorRoot
		This.column1.Header1.Caption=''
		This.column1.Width=23
		This.column1.Sparse=.F.
		This.column1.AddObject('grp','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.grp, i_cBmpPath+'group.bmp', 16)
		Else
			This.column1.grp.Picture=i_cBmpPath+'group.bmp'
		Endif
		This.column1.grp.Visible=.T.
		This.column1.AddObject('usr','image')
		If Vartype(i_ThemesManager)=='O'
			i_ThemesManager.PutBmp(This.column1.usr, i_cBmpPath+'user.bmp', 16)
		Else
			This.column1.usr.Picture=i_cBmpPath+'user.bmp'
		Endif
		This.column1.usr.Visible=.T.
		This.column1.DynamicCurrentControl="IIF(dummy=0,'grp','usr')"
		This.column2.Header1.Caption=cp_Translate(MSG_CODE)
		This.column2.Width=40
		This.column2.text1.Enabled=.F.
		This.column3.Header1.Caption=cp_Translate(MSG_NAME)
		This.column3.Width=105
		This.column3.text1.Enabled=.F.
		This.column4.Width=45
		This.column4.AddObject('chk','seccheckbox')
		This.column4.Sparse=.F.
		This.column4.chk.Visible=.T.
		This.column4.chk.Caption=''
		This.column4.chk.Enabled=.F.
		This.column4.CurrentControl='chk'
		This.column4.Header1.Caption=cp_Translate(MSG_ENTER)
		This.column5.Width=55
		This.column5.AddObject('chk','seccheckbox')
		This.column5.Sparse=.F.
		This.column5.chk.Visible=.T.
		This.column5.chk.Caption=''
		This.column5.chk.Enabled=.F.
		This.column5.CurrentControl='chk'
		This.column5.Header1.Caption=cp_Translate(MSG_INSERT)
		This.column6.Width=50
		This.column6.AddObject('chk','seccheckbox')
		This.column6.Sparse=.F.
		This.column6.chk.Visible=.T.
		This.column6.chk.Caption=''
		This.column6.chk.Enabled=.F.
		This.column6.CurrentControl='chk'
		This.column6.Header1.Caption=cp_Translate(MSG_CHANGE)
		This.column7.Width=55
		This.column7.AddObject('chk','seccheckbox')
		This.column7.Sparse=.F.
		This.column7.chk.Visible=.T.
		This.column7.chk.Caption=''
		This.column7.chk.Enabled=.F.
		This.column7.CurrentControl='chk'
		This.column7.Header1.Caption=cp_Translate(MSG_DELETE)
Enddefine

Define Class seccheckbox As Checkbox
	oldvalue=0
	Procedure Click()
		Do Case
			Case This.oldvalue=1
				This.Value=2
			Case This.oldvalue=2
				This.Value=0
		Endcase
		This.oldvalue=This.Value
		Return
Enddefine

Define Class stdsAuth As CPsysform
	*  backcolor=rgb(192,192,192)
	Width=590
	Caption=(MSG_PROC_SECURITY)
	oThis=.Null.
	cCursor=''
	Top=10
	Left=10
	Icon=i_cBmpPath+i_cStdIcon
	cProgName=''
	Add Object lstProg As ListBox With Left=5,Top=5,Width=170,Height=200
	Add Object secgrd As stdsProgGrd Noinit With Left=185,Top=5,Width=400
	Add Object btndel As cpcommandbutton With Left=185,FontBold=.F.,Top=210,Caption=MSG_DELETE_BUTTON,Height=20,Width=50
	Add Object btnok As cpcommandbutton With Left=530,FontBold=.F.,Top=210,Caption=MSG_OK_BUTTON,Height=20,Width=50
	*add object btncancel as cpcommandbutton with left=530,fontbold=.f.,top=210,caption=MSG_CANCEL_BUTTON,height=20,width=50
	Add Object btnNew As cpcommandbutton With Left=5,FontBold=.F.,Top=210,Caption=MSG_NEW_BUTTON,Height=20,Width=60
	Proc Init()
		cp_InitTranslate(This)
		cp_InitTranslate(This.btndel)
		cp_InitTranslate(This.btnok)
		cp_InitTranslate(This.btnNew)
		If !cp_IsAdministrator()
			cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
			Return
		Endif
		This.oThis=This
		This.cCursor=Sys(2015)
		This.FillProgList()
		If This.lstProg.ListCount>0
			This.cProgName=This.lstProg.List(1)
		Endif
		Create Cursor (This.cCursor) (dummy N(1),gu_code N(6),gu_name C(20),sec1 N(6),sec2 N(6), sec3 N(6),sec4 N(6))
		This.ChangeProgSec(This.cProgName)
		This.secgrd.Fill()
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
		This.Show()
	Proc Destroy()
		If Used(This.cCursor)
			Select (This.cCursor)
			Use
		Endif
		This.oThis=.Null.
	Proc btnNew.Click()
		Local oNewProg
		oNewProg=Createobject('stdsNewProg',Thisform)
		If i_VisualTheme<>-1
			If i_bGradientBck
				With oNewProg.ImgBackGround
					.Width = oNewProg.Width
					.Height = oNewProg.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			oNewProg.BackColor = i_ThemesManager.GetProp(7)
		Endif
		oNewProg.Show()
	Proc lstProg.Click()
		If Thisform.cProgName<>This.List(This.ListIndex)
			Thisform.UpdateProg(Thisform.cProgName)
			Thisform.cProgName=This.List(This.ListIndex)
			Thisform.ChangeProgSec(Thisform.cProgName)
		Endif
	Proc lstProg.KeyPress(nKeyCode, nShiftAltCtrl)
		If (nKeyCode=5 Or nKeyCode=24) And nShiftAltCtrl=0  &&up arrow OR down arrow
			If This.ListIndex>1 And This.ListIndex<This.ListCount
				Thisform.UpdateProg(Thisform.cProgName)
				Thisform.cProgName=This.List(This.ListIndex)
				Thisform.ChangeProgSec(Thisform.cProgName)
			Endif
		Endif
	Proc btndel.Click
		Local N,p,d
		Select (Thisform.cCursor)
		If !Empty(Thisform.cProgName) And gu_code<>0
			If cp_YesNo(cp_MsgFormat(MSG_CONFIRM_DELETING__C__QP,Alltrim(Str(gu_code)),gu_name),32,.f.)
				N=gu_code
				d=dummy
				p=Thisform.cProgName
				Delete
				If d=0
					If i_ServerConn[1,2]<>0
						=cp_SQL(i_ServerConn[1,2],"delete from cpprgsec where progname="+cp_ToStrODBC(p)+" and grpcode="+cp_ToStrODBC(N))
					Else
						Delete From cpprgsec Where progname=p And grpcode=N
					Endif
				Else
					If i_ServerConn[1,2]<>0
						=cp_SQL(i_ServerConn[1,2],"delete from cpprgsec where progname="+cp_ToStrODBC(p)+" and usrcode="+cp_ToStrODBC(N))
					Else
						Delete From cpprgsec Where progname=p And usrcode=N
					Endif
				Endif
				Thisform.secgrd.Refresh()
			Endif
		Endif
	Proc btnok.Click
		Thisform.UpdateProg(Thisform.cProgName)
		Thisform.Release()
	Proc UpdateProg(cProg)
		Local N,p,s1,s2,s3,s4
		Select (Thisform.cCursor)
		Scan
			N=gu_code
			s1=Iif(sec1=2,-1000,sec1)
			s2=Iif(sec2=2,-1000,sec2)
			s3=Iif(sec3=2,-1000,sec3)
			s4=Iif(sec4=2,-1000,sec4)
			If dummy=0
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],"update cpprgsec set sec1="+cp_ToStrODBC(s1)+",sec2="+cp_ToStrODBC(s2)+",sec3="+cp_ToStrODBC(s3)+",sec4="+cp_ToStrODBC(s4)+" where progname="+cp_ToStrODBC(cProg)+" and grpcode="+cp_ToStrODBC(N))
				Else
					Update cpprgsec Set sec1=s1,sec2=s2,sec3=s3,sec4=s4 Where progname=cProg And grpcode=N
				Endif
			Else
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],"update cpprgsec set sec1="+cp_ToStrODBC(s1)+",sec2="+cp_ToStrODBC(s2)+",sec3="+cp_ToStrODBC(s3)+",sec4="+cp_ToStrODBC(s4)+" where progname="+cp_ToStrODBC(cProg)+" and usrcode="+cp_ToStrODBC(N))
				Else
					Update cpprgsec Set sec1=s1,sec2=s2,sec3=s3,sec4=s4 Where progname=cProg And usrcode=N
				Endif
			Endif
		Endscan
		Return
	Proc FillProgList()
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select distinct ProgName from cpprgsec","__tmp__")
		Else
			Select Distinct progname From cpprgsec Into Cursor __tmp__
		Endif
		Scan
			Thisform.lstProg.AddItem(__tmp__.progname)
		Endscan
		Use
		Thisform.lstProg.Selected(1)=.T.
		Return
	Proc ChangeProgSec(cProg)
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select 0 as gu,cpprgsec.grpcode as gu_code,sec1,sec2,sec3,sec4,cpgroups.name as name from cpprgsec,cpgroups where progname="+cp_ToStr(cProg)+" and cpprgsec.grpcode=cpgroups.code"+;
				" union select 1 as gu,cpprgsec.usrcode as gu_code,sec1,sec2,sec3,sec4,cpusers.name as name from cpprgsec,cpusers where progname="+cp_ToStr(cProg)+" and cpprgsec.usrcode=cpusers.code order by 1,2","__tmp__")
		Else
			Select 0 As gu,cpprgsec.grpcode As gu_code,sec1,sec2,sec3,sec4,cpgroups.Name As Name From cpprgsec,cpgroups Where cpprgsec.progname=cProg And cpprgsec.grpcode=cpgroups.Code ;
				UNION Select 1 As gu,cpprgsec.usrcode As gu_code,sec1,sec2,sec3,sec4,cpusers.Name As Name From cpprgsec,cpusers Where cpprgsec.progname=cProg And cpprgsec.usrcode=cpusers.Code ;
				order By 1,2 Into Cursor __tmp__
		Endif
		Local s1,s2,s3,s4
		Delete From (This.cCursor) Where 1=1
		Scan
			s1=Iif(__tmp__.sec1=-1000,2,__tmp__.sec1)
			s2=Iif(__tmp__.sec2=-1000,2,__tmp__.sec2)
			s3=Iif(__tmp__.sec3=-1000,2,__tmp__.sec3)
			s4=Iif(__tmp__.sec4=-1000,2,__tmp__.sec4)
			If __tmp__.gu=0
				Insert Into (This.cCursor) (dummy,gu_code,gu_name,sec1,sec2,sec3,sec4) Values (0,__tmp__.gu_code,__tmp__.Name,s1,s2,s3,s4)
			Else
				Insert Into (This.cCursor) (dummy,gu_code,gu_name,sec1,sec2,sec3,sec4) Values (1,__tmp__.gu_code,__tmp__.Name,s1,s2,s3,s4)
			Endif
		Endscan
		Use
		Select (This.cCursor)
		Go Top
		This.secgrd.Refresh
		Return
Enddefine

Define Class stdsNewProg As CPsysform
	WindowType=1
	*  backcolor=rgb(192,192,192)
	Width=330
	Height=40
	oForm=.Null.
	Caption=(MSG_PROC_SECURITY)
	AutoCenter=.T.
	Icon=i_cBmpPath+i_cStdIcon
	Add Object lProg As cplabel With Left=10,Top=10,Width=80,Caption=(MSG_PROC+MSG_FS),Align=1,FontBold=.F.,BackStyle=0
	Add Object txtNewProg As cptextbox With Left=80,FontBold=.F.,Top=10,Width=215,Height=24
	Proc Init(i_oForm)
		This.oForm=i_oForm
		cp_InitTranslate(This)
		cp_InitTranslate(This.lProg)

	Proc txtNewProg.Valid()
		If !Empty(This.Value)
			Thisform.oForm.lstProg.AddItem(This.Value)
			Thisform.oForm.lstProg.Selected(Thisform.oForm.lstProg.ListCount)=.T.
			Thisform.oForm.cProgName=Thisform.oForm.lstProg.List(Thisform.oForm.lstProg.ListIndex)
			Thisform.oForm.ChangeProgSec(Thisform.oForm.cProgName)
		Endif
		Thisform.Release()
	Proc Destroy()
		This.oForm=.Null.
Enddefine

Func cp_CriptPwd_old(i_cPwd)
	Local i_i,i_c,i_r,i_n
	i_cPwd=Trim(i_cPwd)
	i_c=0
	For i_i=1 To Min(Len(i_cPwd),10)
		i_n=Asc(Substr(i_cPwd,i_i,1))*27^i_i
		If i_c+i_n<999999999999999
			i_c=i_c+i_n
		Endif
	Next
	i_r=''
	For i_i=1 To 10
		i_r=i_r+Chr(Int(Rand()*26)+97)
		i_r=i_r+Chr(Mod(i_c,13)+i_i+97)
		i_c=Int(i_c/13)
	Next
	Return(i_r)

Func cp_CriptPwd_old2(i_cPwd)
	Local i_i,i_c,i_r,i_n,i_t
	i_cPwd=Trim(i_cPwd)
	If Len(i_cPwd)<15
		Do While Len(i_cPwd)<15
			i_cPwd=i_cPwd+'a'+i_cPwd
		Enddo
		i_cPwd=Left(i_cPwd,15)
	Endif
	i_c=0
	For i_i=1 To Len(i_cPwd)
		i_n=Asc(Substr(i_cPwd,i_i,1))*27^(Max(Int(i_i/2),1))
		If i_c+i_n<999999999999999
			i_c=i_c+i_n
		Endif
	Next
	i_r=''
	For i_i=1 To 10
		i_t=Int(Rand()*26)
		i_r=i_r+Chr(i_t+Iif(i_i=1,64,97))
		i_r=i_r+Chr(Mod(i_c,13)+i_i+97)
		i_c=Int(i_c/13)
	Next
	Return(i_r)

Function cp_Criptmd5(i_usercode,i_tcData)
	i_tcData=Chr(36)+Chr(165)+Chr(34)+Chr(172)+Chr(236)+Chr(110)+Chr(256-122)+Chr(0)+Str(i_usercode,10)+i_tcData
	Local lnStatus, lnErr, lhProv, lhHashObject, lnDataSize, lcHashValue, lnHashSize, i ,lbignum,riporto,interm,k
	lhProv = 0
	lhHashObject = 0
	lnDataSize = Len(i_tcData)
	lcHashValue = Replicate(Chr(0), 16)
	lnHashSize = Len(lcHashValue)


	*  TRY

	Declare Integer GetLastError In kernel32.Dll

	Declare Integer CryptAcquireContextA ;
		IN Advapi32.Dll As CryptAcquireContext ;
		INTEGER @lhProvHandle, ;
		STRING  cContainer, ;
		STRING  cProvider, ;
		INTEGER nProvType, ;
		INTEGER nFlags

	* load a crypto provider
	lnStatus = CryptAcquireContext(@lhProv, 0, 0, 1, 0xF0000000)

	Declare Integer CryptCreateHash ;
		IN Advapi32.Dll As CryptCreateHash ;
		INTEGER hProviderHandle, ;
		INTEGER nALG_ID, ;
		INTEGER hKeyhandle, ;
		INTEGER nFlags, ;
		INTEGER @hCryptHashHandle

	* create a hash object that uses MD5 algorithm
	If lnStatus <> 0
		lnStatus = CryptCreateHash(lhProv, Bitor(Bitlshift(4, 13),3), 0, 0, @lhHashObject)
	Endif


	Declare Integer CryptHashData ;
		IN Advapi32.Dll As CryptHashData ;
		INTEGER hHashHandle, ;
		STRING  @cData, ;
		INTEGER nDataLen, ;
		INTEGER nFlags

	* add the input data to the hash object
	If lnStatus <> 0
		lnStatus = CryptHashData(lhHashObject, i_tcData, lnDataSize, 0)
	Endif


	Declare Integer CryptGetHashParam ;
		IN Advapi32.Dll As CryptGetHashParam ;
		INTEGER hHashHandle, ;
		INTEGER nParam, ;
		STRING  @cHashValue, ;
		INTEGER @nHashSize, ;
		INTEGER nFlags

	* retrieve the hash value, if caller did not provide enough storage (16 bytes for MD5)
	* this will fail with dnERROR_MORE_DATA and lnHashSize will contain needed storage size
	If lnStatus <> 0
		lnStatus = CryptGetHashParam(lhHashObject, 0x0002, @lcHashValue, @lnHashSize, 0)
	Endif


	Declare Integer CryptDestroyHash ;
		IN Advapi32.Dll As CryptDestroyHash;
		INTEGER hKeyHandle

	* free the hash object
	If lnStatus <> 0
		lnStatus = CryptDestroyHash(lhHashObject)
	Endif


	Declare Integer CryptReleaseContext ;
		IN Advapi32.Dll As CryptReleaseContext ;
		INTEGER hProvHandle, ;
		INTEGER nReserved

	* release the crypto provider
	If lnStatus <> 0
		lnStatus = CryptReleaseContext(lhProv, 0)
	Endif

	*  CATCH TO lnErr

	* clean up the hash object and release provider
	If lhHashObject != 0
		CryptDestroyHash(lhHashObject)
	Endif


	If lhProv != 0
		CryptReleaseContext(lhProv, 0)
	Endif

	If lnStatus = 0
		Error("HashMD5 Failed")
	Endif

	*ENDTRY
	lbignum=''
	For j=1 To 20
		riporto=0
		For i=1 To 16
			riporto=riporto*256+Asc(Substr(lcHashValue,i,1))
			lcHashValue=Substr(lcHashValue,1,i-1)+Chr(Int(riporto/95))+Substr(lcHashValue,i+1)
			riporto=riporto%95
		Next
		lbignum=Chr(32+riporto)+lbignum
	Next
	*  RETURN lcHashValue
	Return lbignum

Func cp_CriptPwd(i_nUser,i_cPwd)
	*activate screen
	*? i_nUser,i_cPwd,'--->',cp_Criptmd5(i_nUser,trim(i_cPwd))
	Return cp_Criptmd5(i_nUser,Trim(i_cPwd))

Func cp_CheckPwd(i_codute,i_cPwd1,i_cPwd2)
	Local i_i,i_r,i_r1,i_r2
	Local i_cp, i_cpold
	i_cp=cp_CriptPwd(i_codute,i_cPwd1)
	*activate screen
	*? 'check pwd',i_cp,i_cpwd2
	If i_cp==i_cPwd2
		Return .T.
	Endif
	* -- vecchi algoritmi
    If ASC(Left(i_cPwd2,1))<ASC('a')
		i_cPwd1=cp_CriptPwd_old2(Trim(i_cPwd1))
	Else
		i_cPwd1=cp_CriptPwd_old(Trim(i_cPwd1))
	Endif
	i_r=.T.
	For i_i=1 To 5
		If Substr(i_cPwd1,i_i*2,1)<>Substr(i_cPwd2,i_i*2,1)
			i_r=.F.
		Endif
	Next
	Return(i_r)


	* --- Zucchetti Aulla - Gestione Sicurezza
	* --- Inserito parametro numero tentativi..
	*--- Single Sign-On non controllo la password
	*func cp_ChangeUser(i_nCodute,i_cPwd)
Func cp_ChangeUser(i_nCodute,i_cPwd,i_nNum, i_bNoCheckPwd)
	Local i_cPwd2,i_r,i_nusers
	* --- controllo dei tentativi
	*----Zucchetti Aulla Inizio-----*
	If Vartype(i_nNum)<>'N'
		i_nNum = 3
	Endif
	If i_nNum < 1
		i_nNum = 3
	Endif
	If Vartype(i_pwdtrys)='U'
		Public i_pwdtrys
		i_pwdtrys=0
	Endif
	*----Zucchetti Aulla Fine-----*
	If Vartype(i_oldpwdcheck)='U'
		Public i_oldpwdcheck,i_pwdtrys
		i_oldpwdcheck=i_nCodute
		i_pwdtrys=0
	Endif
	*i_pwdtrys=iif(i_oldpwdcheck=i_nCodute,i_pwdtrys+1,0)  && resetta il contatore al cambio utente
	i_pwdtrys=i_pwdtrys+1
	* --- Zucchetti Aulla - Gestione Sicurezza
	* --- Inserito parametro numero tentativi..
	*if i_pwdtrys>3
	If i_pwdtrys>i_nNum
		CP_MSG(cp_Translate(MSG_TOO_MANY_TRIALS),.F.)

		Return(.F.)
	Endif
	* --- controlla se non sono stati definiti gli utenti
	If i_ServerConn[1,2]<>0
		=cp_SQL(i_ServerConn[1,2],'select count(*) as  nusers from cpusers','__tmp__')
	Else
		Select Count(*) As nusers From cpusers Where Not(Deleted()) Into Cursor __tmp__
	Endif
	If Type('__tmp__.nusers')='C'
		i_nusers=Val(__tmp__.nusers)
	Else
		i_nusers=__tmp__.nusers
	Endif
	If i_nusers=0 And i_nCodute=0
		Use
		i_codute=1
		* --- Zucchetti Aulla - Inizio Interfaccia
		*oCpToolBar.boxute.value=MSG_TB_USER+chr(10)+alltrim(str(i_codute))
		* --- Zucchetti Aulla - Inizio Interfaccia
		Return(.T.)
	Endif
	* --- Controllo della password
	If i_ServerConn[1,2]<>0
		=cp_SQL(i_ServerConn[1,2],'select * from cpusers where code='+cp_ToStr(i_nCodute),'__tmp__')
	Else
		Select * From cpusers Where Code=(i_nCodute) Into Cursor __tmp__
	Endif
	i_codute = 0
	If Not(Used('__tmp__'))
		Return(.F.)
	Endif
	Local i_cUserLang
	i_cUserLang=Nvl(Iif(i_ServerConn[1,6]='DBMaker',__tmp__.Lang,__tmp__.Language),'')
	If Eof() or empty(nvl(__tmp__.PASSWD,''))
		Use
		Return(.F.)
	ENDIF
	i_cPwd2=__tmp__.PASSWD
	Use
	i_r=cp_CheckPwd(i_nCodute,i_cPwd,i_cPwd2)
	*--- Zucchetti Aulla Inizio - Single Sign-On
	*if i_r
	If i_r Or i_bNoCheckPwd
		*--- Zucchetti Aulla Fine - Single Sign-On
		i_codute=i_nCodute
		* --- Zucchetti Aulla - valorizzo g_CODUTE per riproporlo al cambio contesto
		g_CODUTE=i_codute
		* --- Zucchetti Aulla - Inizio Interfaccia
		*oCpToolBar.boxute.value=MSG_TB_USER+chr(10)+alltrim(str(i_codute))
		* --- Zucchetti Aulla - Inizio Interfaccia
		i_pwdtrys=0
		i_cLanguage=i_cUserLang
		*** controllo se la lingua dell'utente � abilitata alla traduzione del dato
		If islanguagetranslate(i_cLanguage)
			i_cLanguageData=i_cLanguage
		Else
			i_cLanguageData=Space(3)
		Endif
		Release i_aUsrGrps
	Endif
	Return(i_r Or i_bNoCheckPwd)

Function cp_AddSingleSignOnMap(i_nUserID,i_cSsoid)
	Local i_nConn
	i_nConn=i_ServerConn[1,2]
	If !Empty(i_nUserID) And !Empty(i_cSsoid)
		If i_nConn<>0
			* --- Zucchetti Aulla Inizio - Verifico se devo andare in update
			If Empty(cp_SingleSignOnUser(i_nUserID))
				cp_SQL(i_nConn,"insert into cpssomap (ssoid,userid) values ('"+i_cSsoid+"',"+Alltrim(Str(i_nUserID))+")")
			Else
				cp_SQL(i_nConn,"update cpssomap set ssoid='"+i_cSsoid+"' where userid="+Tran(i_nUserID))
			Endif
		Else
			If Empty(cp_SingleSignOnUser(i_nUserID))
				Insert Into cpssomap (ssoid,Userid) Values (i_cSsoid,i_nUserID)
			Else
				Update cpssomap set ssoid=(i_cSsoid) Where userid=(i_nUserID)
			Endif
		Endif
		* --- Zucchetti Aulla Fine
	Endif
	Return

Function cp_SingleSignOnUser(i_cSsoid)
	Local i_nConn,i_res
	i_nConn=i_ServerConn[1,2]
	* --- Zucchetti Aulla - verifico la tipologia del dato per decidere se restituire l'ssoid o il codice utente
	i_res=Iif(Vartype(i_cSsoid)='C',0,"")
	If i_nConn<>0
		* --- Zucchetti Aulla Inizio - verifico la tipologia del dato per decidere se restituire l'ssoid o il codice utente
		If Vartype(i_cSsoid)='C'
			cp_SQL(i_nConn,"select * from cpssomap where ssoid='"+i_cSsoid+"'",'_SignUser_')
		Else
			cp_SQL(i_nConn,"select * from cpssomap where userid="+Tran(i_cSsoid),'_SignUser_')
		Endif
	Else
		If Vartype(i_cSsoid)='C'
			Select * From cpssomap Where ssoid=i_cSsoid Into Cursor _SignUser_
		Else
			Select * From cpssomap Where userid=i_cSsoid Into Cursor _SignUser_
		Endif
		* --- Zucchetti Aulla Fine
	Endif
	If Used('_SignUser_')
		Select _SignUser_
		*--- Zucchetti Aulla - verifico la tipologia del dato per decidere se restituire l'ssoid o il codice utente
		i_res=Iif(Vartype(i_cSsoid)='C',_SignUser_.Userid,_SignUser_.Ssoid)
		Use
	Endif
	Return(i_res)

Function cp_VerifySingleSignOn(i_cSsoid,i_xSsoCredentials)
	If Empty(i_xSsoCredentials)
		Activate Screen
		? 'empty SingleSignOn credentials'
	Endif
	Return(.T.)

Func islanguagetranslate(cLanguage)
	Local i_nConn,i_nOldArea,i_LanguageTrans
	i_LanguageTrans=''
	i_nOldArea=Select()
	If Used('_read_')
		Select _read_
		Use
	Endif
	i_nConn=i_ServerConn[1,2]
	If i_nConn<>0
		cp_sqlexec(i_nConn,"select code from cplangs where datalanguage = 'S' and lower(code)="+cp_ToStrODBC(Lower(cLanguage)),"_read_")
		i_Rows=Iif(Used('_read_'),Reccount(),0)
	Else
		Select Code From cplangs Where datalanguage = 'S' And Lower(Code)=Lower(cLanguage)  Into Cursor _read_
		i_Rows=_Tally
	Endif
	If Used('_read_')
		Locate For 1=1
		i_LanguageTrans = Nvl(cp_ToDate(_read_.Code),cp_NullValue(_read_.Code))
		Use
	Else
		* --- Error: sql sentence error.
		i_Error = MSG_READ_ERROR
		Return
	Endif
	Select (i_nOldArea)
	Return(Not Empty(Nvl(i_LanguageTrans,"")))
Endfunc
