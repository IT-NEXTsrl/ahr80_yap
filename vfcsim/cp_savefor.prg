* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_savefor                                                      *
*              Save as                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-09                                                      *
* Last revis.: 2012-02-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_savefor",oParentObject))

* --- Class definition
define class tcp_savefor as StdForm
  Top    = 5
  Left   = 7

  * --- Standard Properties
  Width  = 567
  Height = 228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-02-02"
  HelpContextID=53961230
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  azienda_IDX = 0
  cPrg = "cp_savefor"
  cComment = "Save as"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_original_file_name = space(10)
  w_NOUTEGRP = .F.
  w_FILE_NAME_NEW = space(254)
  o_FILE_NAME_NEW = space(254)
  w_USERS_CODE = 0
  o_USERS_CODE = 0
  w_GROUP_CODE = 0
  o_GROUP_CODE = 0
  w_COMPANY_CODE = space(5)
  o_COMPANY_CODE = space(5)
  w_NEWFILE_NAME = space(254)
  w_MENUCOMBO = space(1)
  o_MENUCOMBO = space(1)
  w_users_desc = space(25)
  w_group_desc = space(25)
  w_company_desc = space(40)
  w_LOGIC_TEMP = .F.
  w_OK = .F.
  w_edit_condition = .F.
  w_vrt_condition = .F.
  * --- Area Manuale = Declare Variables
  * --- cp_savefor
  function namebuild(w_file_name_new,w_users_code,w_group_code,w_company_code,w_menucombo)
     local oldfilename,temp, l_ret
     if empty(juststem(this.w_original_file_name))
       oldfilename=alltrim(w_file_name_new)+alltrim(this.w_original_file_name)
     else
       if this.w_edit_condition
         oldfilename=w_file_name_new+"."+justext(this.w_original_file_name)
       else
         oldfilename=this.w_original_file_name
       endif
     endif
     Do Case
       Case w_menucombo=='M'
         l_ret =cp_monitor_buildname(oldfilename,w_users_code,w_group_code,w_company_code,"E","U")
       Case w_menucombo=='C'
         temp = cp_monitor_buildname(oldfilename,w_users_code,w_group_code,"","E","U")
         l_ret = ".\custom\userdir\RightClick\"+alltrim(justfname(temp))
       Case w_menucombo=='D'
         temp = cp_monitor_buildname(oldfilename,w_users_code,w_group_code,"","E","U")
         l_ret = ".\custom\userdir\DeskMenu\"+alltrim(justfname(temp))
     EndCase
     This.w_newfile_name = l_ret
     This.mHideControls()
     return l_ret
  endfunc
  
  Function set_original_file_name(string_to_set)
     if len(alltrim(string_to_set))==0
       * questo per l'eventuale pressione del tasto cancel
       this.w_original_file_name = ""
       return .t.
     else
       this.w_original_file_name = alltrim(string_to_set)
       return .t.
     endif
  endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_saveforPag1","cp_savefor",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILE_NAME_NEW_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='cpgroups'
    this.cWorkTables[3]='azienda'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_original_file_name=space(10)
      .w_NOUTEGRP=.f.
      .w_FILE_NAME_NEW=space(254)
      .w_USERS_CODE=0
      .w_GROUP_CODE=0
      .w_COMPANY_CODE=space(5)
      .w_NEWFILE_NAME=space(254)
      .w_MENUCOMBO=space(1)
      .w_users_desc=space(25)
      .w_group_desc=space(25)
      .w_company_desc=space(40)
      .w_LOGIC_TEMP=.f.
      .w_OK=.f.
      .w_edit_condition=.f.
      .w_vrt_condition=.f.
      .w_original_file_name=oParentObject.w_original_file_name
      .w_NEWFILE_NAME=oParentObject.w_NEWFILE_NAME
      .w_OK=oParentObject.w_OK
      .w_edit_condition=oParentObject.w_edit_condition
      .w_vrt_condition=oParentObject.w_vrt_condition
          .DoRTCalc(1,1,.f.)
        .w_NOUTEGRP = UPPER(JUSTEXT(.w_original_file_name)) == 'VQR'
        .w_FILE_NAME_NEW = cp_monitor_GetFilename(.w_original_file_name)
        .w_USERS_CODE = IIF(.w_NOUTEGRP, 0, iif(cp_set_get_right()>3, cp_monitor_GetCodute(.w_original_file_name), i_codute))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_USERS_CODE))
          .link_1_4('Full')
        endif
        .w_GROUP_CODE = IIF(.w_NOUTEGRP, 0, iif(cp_set_get_right()>3, cp_monitor_GetCodgrp(.w_original_file_name), 0))
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_GROUP_CODE))
          .link_1_5('Full')
        endif
        .w_COMPANY_CODE = cp_monitor_GetCodazi(.w_original_file_name)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_COMPANY_CODE))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_MENUCOMBO = 'M'
          .DoRTCalc(9,12,.f.)
        .w_OK = .T.
    endwith
    this.DoRTCalc(14,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- cp_savefor
    if !pemstatus(this.oParentObject, "SetControlsValue", 5)
        this.NotifyEvent('Update start')
        with this
          .oParentObject.w_original_file_name=.w_original_file_name
          .oParentObject.w_NEWFILE_NAME=.w_NEWFILE_NAME
          .oParentObject.w_OK=.w_OK
          .oParentObject.w_edit_condition=.w_edit_condition
          .oParentObject.w_vrt_condition=.w_vrt_condition
        endwith
        this.NotifyEvent('Update end')  
        return(.t.)
    endif
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_original_file_name=.w_original_file_name
      .oParentObject.w_NEWFILE_NAME=.w_NEWFILE_NAME
      .oParentObject.w_OK=.w_OK
      .oParentObject.w_edit_condition=.w_edit_condition
      .oParentObject.w_vrt_condition=.w_vrt_condition
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_file_name_new<>.w_file_name_new.or. .o_users_code<>.w_users_code.or. .o_group_code<>.w_group_code.or. .o_company_code<>.w_company_code.or. .o_menucombo<>.w_menucombo
          .Calculate_FXNVPUQTHU()
        endif
        .DoRTCalc(1,12,.t.)
            .w_OK = .T.
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_FXNVPUQTHU()
    with this
          * --- set new file name
          .w_newfile_name = .namebuild(.w_file_name_new,.w_users_code,.w_group_code,.w_company_code,.w_menucombo)
    endwith
  endproc
  proc Calculate_GNYAKOOCNK()
    with this
          * --- Salva
          .w_logic_temp = .set_original_file_name(.w_newfile_name)
    endwith
  endproc
  proc Calculate_WNHYEKWUVH()
    with this
          * --- Cancel
          .w_logic_temp = .set_original_file_name("")
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFILE_NAME_NEW_1_3.enabled = this.oPgFrm.Page1.oPag.oFILE_NAME_NEW_1_3.mCond()
    this.oPgFrm.Page1.oPag.oUSERS_CODE_1_4.enabled = this.oPgFrm.Page1.oPag.oUSERS_CODE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oGROUP_CODE_1_5.enabled = this.oPgFrm.Page1.oPag.oGROUP_CODE_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCOMPANY_CODE_1_6.enabled = this.oPgFrm.Page1.oPag.oCOMPANY_CODE_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMENUCOMBO_1_8.visible=!this.oPgFrm.Page1.oPag.oMENUCOMBO_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_FXNVPUQTHU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Update End")
          .Calculate_GNYAKOOCNK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_WNHYEKWUVH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=USERS_CODE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_USERS_CODE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_USERS_CODE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_USERS_CODE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_USERS_CODE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oUSERS_CODE_1_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_USERS_CODE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_USERS_CODE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_USERS_CODE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_USERS_CODE = NVL(_Link_.CODE,0)
      this.w_users_desc = NVL(_Link_.NAME,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_USERS_CODE = 0
      endif
      this.w_users_desc = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_USERS_CODE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GROUP_CODE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GROUP_CODE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_GROUP_CODE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_GROUP_CODE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_GROUP_CODE) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','CODE',cp_AbsName(oSource.parent,'oGROUP_CODE_1_5'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GROUP_CODE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_GROUP_CODE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_GROUP_CODE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GROUP_CODE = NVL(_Link_.CODE,0)
      this.w_group_desc = NVL(_Link_.NAME,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_GROUP_CODE = 0
      endif
      this.w_group_desc = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GROUP_CODE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPANY_CODE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.azienda_IDX,3]
    i_lTable = "azienda"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.azienda_IDX,2], .t., this.azienda_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.azienda_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPANY_CODE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'azienda')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_COMPANY_CODE)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_COMPANY_CODE))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMPANY_CODE)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMPANY_CODE) and !this.bDontReportError
            deferred_cp_zoom('azienda','*','AZCODAZI',cp_AbsName(oSource.parent,'oCOMPANY_CODE_1_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPANY_CODE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COMPANY_CODE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COMPANY_CODE)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPANY_CODE = NVL(_Link_.AZCODAZI,space(5))
      this.w_company_desc = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COMPANY_CODE = space(5)
      endif
      this.w_company_desc = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.azienda_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.azienda_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPANY_CODE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFILE_NAME_NEW_1_3.value==this.w_FILE_NAME_NEW)
      this.oPgFrm.Page1.oPag.oFILE_NAME_NEW_1_3.value=this.w_FILE_NAME_NEW
    endif
    if not(this.oPgFrm.Page1.oPag.oUSERS_CODE_1_4.value==this.w_USERS_CODE)
      this.oPgFrm.Page1.oPag.oUSERS_CODE_1_4.value=this.w_USERS_CODE
    endif
    if not(this.oPgFrm.Page1.oPag.oGROUP_CODE_1_5.value==this.w_GROUP_CODE)
      this.oPgFrm.Page1.oPag.oGROUP_CODE_1_5.value=this.w_GROUP_CODE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPANY_CODE_1_6.value==this.w_COMPANY_CODE)
      this.oPgFrm.Page1.oPag.oCOMPANY_CODE_1_6.value=this.w_COMPANY_CODE
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWFILE_NAME_1_7.value==this.w_NEWFILE_NAME)
      this.oPgFrm.Page1.oPag.oNEWFILE_NAME_1_7.value=this.w_NEWFILE_NAME
    endif
    if not(this.oPgFrm.Page1.oPag.oMENUCOMBO_1_8.RadioValue()==this.w_MENUCOMBO)
      this.oPgFrm.Page1.oPag.oMENUCOMBO_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ousers_desc_1_11.value==this.w_users_desc)
      this.oPgFrm.Page1.oPag.ousers_desc_1_11.value=this.w_users_desc
    endif
    if not(this.oPgFrm.Page1.oPag.ogroup_desc_1_12.value==this.w_group_desc)
      this.oPgFrm.Page1.oPag.ogroup_desc_1_12.value=this.w_group_desc
    endif
    if not(this.oPgFrm.Page1.oPag.ocompany_desc_1_13.value==this.w_company_desc)
      this.oPgFrm.Page1.oPag.ocompany_desc_1_13.value=this.w_company_desc
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FILE_NAME_NEW))  and ((empty(juststem(.w_original_file_name)) or .w_edit_condition) and !.w_vrt_condition)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFILE_NAME_NEW_1_3.SetFocus()
            i_bnoObbl = !empty(.w_FILE_NAME_NEW)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Immettere il nome del file")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FILE_NAME_NEW = this.w_FILE_NAME_NEW
    this.o_USERS_CODE = this.w_USERS_CODE
    this.o_GROUP_CODE = this.w_GROUP_CODE
    this.o_COMPANY_CODE = this.w_COMPANY_CODE
    this.o_MENUCOMBO = this.w_MENUCOMBO
    return

enddefine

* --- Define pages as container
define class tcp_saveforPag1 as StdContainer
  Width  = 563
  height = 228
  stdWidth  = 563
  stdheight = 228
  resizeXpos=233
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFILE_NAME_NEW_1_3 as StdField with uid="VVAMGJDLRT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FILE_NAME_NEW", cQueryName = "FILE_NAME_NEW",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Immettere il nome del file",;
    ToolTipText = "Name",;
    HelpContextID = 249954736,;
   bGlobalFont=.t.,;
    Height=21, Width=481, Left=75, Top=6, InputMask=replicate('X',254)

  func oFILE_NAME_NEW_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((empty(juststem(.w_original_file_name)) or .w_edit_condition) and !.w_vrt_condition)
    endwith
   endif
  endfunc

  add object oUSERS_CODE_1_4 as StdField with uid="AHAOSPGBMC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_USERS_CODE", cQueryName = "USERS_CODE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 62578817,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=75, Top=38, cSayPict='"99999"', cGetPict='"99999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_USERS_CODE"

  func oUSERS_CODE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_group_code==0 AND cp_set_get_right() > 3 AND !.w_NOUTEGRP)
    endwith
   endif
  endfunc

  func oUSERS_CODE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oUSERS_CODE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUSERS_CODE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oUSERS_CODE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oGROUP_CODE_1_5 as StdField with uid="DIMMCJKIIY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GROUP_CODE", cQueryName = "GROUP_CODE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 123273345,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=75, Top=64, cSayPict='"99999"', cGetPict='"99999"', bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="CODE", oKey_1_2="this.w_GROUP_CODE"

  func oGROUP_CODE_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_users_code==0 AND !.w_NOUTEGRP)
    endwith
   endif
  endfunc

  func oGROUP_CODE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oGROUP_CODE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGROUP_CODE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','CODE',cp_AbsName(this.parent,'oGROUP_CODE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCOMPANY_CODE_1_6 as StdField with uid="AJCFRRJSVU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COMPANY_CODE", cQueryName = "COMPANY_CODE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 192922064,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=75, Top=91, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="azienda", oKey_1_1="AZCODAZI", oKey_1_2="this.w_COMPANY_CODE"

  func oCOMPANY_CODE_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_menucombo='M')
    endwith
   endif
  endfunc

  func oCOMPANY_CODE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMPANY_CODE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMPANY_CODE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'azienda','*','AZCODAZI',cp_AbsName(this.parent,'oCOMPANY_CODE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oNEWFILE_NAME_1_7 as StdField with uid="XCJTXYRPZO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NEWFILE_NAME", cQueryName = "NEWFILE_NAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 224845682,;
   bGlobalFont=.t.,;
    Height=21, Width=481, Left=75, Top=145, InputMask=replicate('X',254)


  add object oMENUCOMBO_1_8 as StdCombo with uid="WJKSELLMSC",rtseq=8,rtrep=.f.,left=75,top=118,width=146,height=21;
    , HelpContextID = 116917265;
    , cFormVar="w_MENUCOMBO",RowSource=""+"Context Menu,"+"Main Menu,"+"Desk Menu", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMENUCOMBO_1_8.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'M',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oMENUCOMBO_1_8.GetRadio()
    this.Parent.oContained.w_MENUCOMBO = this.RadioValue()
    return .t.
  endfunc

  func oMENUCOMBO_1_8.SetRadio()
    this.Parent.oContained.w_MENUCOMBO=trim(this.Parent.oContained.w_MENUCOMBO)
    this.value = ;
      iif(this.Parent.oContained.w_MENUCOMBO=='C',1,;
      iif(this.Parent.oContained.w_MENUCOMBO=='M',2,;
      iif(this.Parent.oContained.w_MENUCOMBO=='D',3,;
      0)))
  endfunc

  func oMENUCOMBO_1_8.mHide()
    with this.Parent.oContained
      return (lower(justext(.w_newfile_name)) != 'vmn')
    endwith
  endfunc


  add object oBtn_1_9 as StdButton with uid="GPQYTBCQYS",left=456, top=172, width=48,height=45,;
    CpPicture="bmp\save.bmp", caption="", nPag=1;
    , ToolTipText = "Save";
    , HelpContextID = 160378098;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="CZAXUUBTZR",left=508, top=172, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 150065826;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object ousers_desc_1_11 as StdField with uid="GARVGTGGUV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_users_desc", cQueryName = "users_desc",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    HelpContextID = 100526225,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=159, Top=38, InputMask=replicate('X',25)

  add object ogroup_desc_1_12 as StdField with uid="DKHRSCEQTO",rtseq=10,rtrep=.f.,;
    cFormVar = "w_group_desc", cQueryName = "group_desc",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    HelpContextID = 161220753,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=159, Top=63, InputMask=replicate('X',25)

  add object ocompany_desc_1_13 as StdField with uid="JGJVCIYGZS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_company_desc", cQueryName = "company_desc",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 22424592,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=159, Top=89, InputMask=replicate('X',40)

  add object oStr_1_15 as StdString with uid="LPVMMICYPZ",Visible=.t., Left=38, Top=38,;
    Alignment=1, Width=37, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DLDDUBAMUS",Visible=.t., Left=31, Top=64,;
    Alignment=1, Width=44, Height=18,;
    Caption="Group:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="XHUHEKMAVZ",Visible=.t., Left=19, Top=91,;
    Alignment=1, Width=56, Height=18,;
    Caption="Company:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KJWMQOLGXZ",Visible=.t., Left=9, Top=145,;
    Alignment=1, Width=66, Height=18,;
    Caption="File name:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="RFMMFTXQNA",Visible=.t., Left=38, Top=6,;
    Alignment=1, Width=37, Height=18,;
    Caption="Name:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="HSBOXYXFBP",Visible=.t., Left=44, Top=118,;
    Alignment=1, Width=29, Height=22,;
    Caption="Type:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (lower(justext(.w_newfile_name)) != 'vmn')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_savefor','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
