* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VQ_BUILD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Disegnatore di query
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

*--- Zucchetti Aulla inizio monitor framework
If cp_set_get_right()<2
    cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
    Return
Endif
*--- Zucchetti Aulla fine monitor framework

*--- Massima lunghezza espressione visual query
#Define MAX_EXPR_LENGHT 1200
#Define MIN_WIDTH 50
#Define MIN_HEIGHT 100

*define pad vista of _msysmenu prompt 'Vista'
*on pad vista of _msysmenu activate popup _msm_view

If At('cp_dcx',Lower(Set('proc')))=0 Or At('cp_ppx',Lower(Set('proc')))=0 ;
        OR At('cp_query',Lower(Set('proc')))=0 Or At('vq_lib',Lower(Set('proc')))=0
    Set Proc To cp_dcx,cp_ppx,cp_query,vq_lib Additive
Endif

o=Createobject('query_coordinator', Vartype(_Screen.cp_ThemesManager)=="O" And i_VisualTheme<>-1)
i_curform=o
If (Vartype(i_designfile)='C') And cp_fileexist(i_designfile)
    o.cFileName = Alltrim(i_designfile)
    o.SetTitle()
    o.LoadDoc()
Endif
o.EditQuery()
o.oform.WindowState = 2	&&Massimizzo la form

Define Class CpSysContainer As Container
    Closable=.F.
    Caption =''
    Icon=''
    ShowTips=.T.
    MinWidth=0
    MinHeight=0
    BackStyle = 0
    BorderStyle = 0
    BorderWidth=0
    Width = 500
    Height=500

    Procedure Show()
        This.Visible=.T.
        This.SetFocus()
    Endproc
    Procedure Hide()
        This.Visible=.F.
    Endproc

Enddefine

Define Class query_builder As CPForm
    oParentObject = .Null.
    Closable = .F.
    ShowTips=.T.
    Add Object oPgFrm As PageFrame With PageCount=0, Width = 500, Height=500, TabStyle=1
    Add Object __dummy__ As CommandButton With Enabled=.F.,Width=0,Height=0,Style=0,SpecialEffect=1
    Add Object oCtrlPage As ControlPage
    nActivePage = 0
    Icon = i_cBmpPath+i_cStdIcon
    oXmlQueryFunction = .Null.
    cFunctionsXmlFile = ''
    cFavoritesXmlFile = ''
    bXmlQueryFunctionError = .F.

	*--- Zucchetti Aulla Inizio - Form decorator
	bApplyFormDecorator=.t.
	*-- Zucchetti Aulla Fine - Form decorator 

    Procedure Init(pParent)
        This.Caption = cp_Translate(MSG_QUERY_PAINTER)
        This.Width = _Screen.Width-2*Sysmetric(3)
        This.Height = _Screen.Height-Sysmetric(9)-Sysmetric(4)
        This.Top = 0
        This.Left = 0
        This.oParentObject= m.pParent
        
    	*--- Form Decorator
    	If This.bApplyFormDecorator And i_ThemesManager.GetProp(123)=0
    		This.AddObject("oDec", "cp_FormDecorator")	
        Else
            This.bApplyFormDecorator = .F.    		
    	EndIf
        This.AddQuery()
        If i_VisualTheme<>-1
            If i_bGradientBck
                With This.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            This.BackColor = i_ThemesManager.GetProp(7)
            If i_cMenuTab<>"S"
                This.oPgFrm.Tabs=.F.
                This.oPgFrm.BorderWidth=0
                This.AddObject("oTabMenu", "TabMenu", This, .F., .F., "oPgFrm", .T.)
                This.oPgFrm.Height = This.oPgFrm.Height - This.oTabMenu.Height
            Endif
            This.oCtrlPage.ZOrder(0)
            This.oCtrlPage.SetBorderColor(i_ThemesManager.GetProp(7))
        EndIf
    Endproc

    Procedure AddQuery()
        Local nPage
        This.LockScreen = .T.
        nPage = This.oPgFrm.PageCount + 1
        This.oPgFrm.PageCount = m.nPage
        This.oPgFrm.Pages(m.nPage).AddObject("oQuery", "query_builderContainer", This.oParentObject)
        This.oPgFrm.Pages(m.nPage).oQuery.Visible=.T.
        This.oPgFrm.ActivePage = This.oPgFrm.PageCount
        This.oPgFrm.Click()
        This.oParentObject.NewDoc()
        This.oParentObject.bModified=.F.
        This.oParentObject.cFileName=''
        This.oPgFrm.Pages(m.nPage).Caption=Forceext(CP_MSGFORMAT(MSG_NO_NAME), "VQR")
        This.oPgFrm.Pages(m.nPage).ToolTipText=This.oPgFrm.Pages(m.nPage).Caption
        This.oPgFrm.Pages(m.nPage).oQuery.LFileName.Caption = Lower(This.oPgFrm.Pages(m.nPage).Caption)
        This.Resize()
        This.oPgFrm.Pages(m.nPage).oQuery.VSplitter.DblClick()
        This.oPgFrm.Pages(m.nPage).oQuery.HSplitter.DblClick()
        This.LockScreen = .F.
    Endproc

    Function RemoveQuery(nPage)
        If This.oParentObject.SaveModified()
            This.LockScreen = .T.
            If This.oPgFrm.PageCount > 1
                This.oPgFrm.Pages(m.nPage).oQuery.oQuery.oPage.page2.opag.fdlfields.olist.oParentObject=.Null.
                This.oPgFrm.Pages(m.nPage).oQuery.oQuery.oPage.page3.opag.rllrelation.olist.oParentObject=.Null.
                This.oPgFrm.Pages(m.nPage).oQuery.oQuery.oPage.page4.opag.fllfilter.olist.oParentObject=.Null.
                This.oPgFrm.Pages(m.nPage).oQuery.oQuery.oPage.page5.opag.fplparam.olist.oParentObject=.Null.
                This.oPgFrm.Pages(m.nPage).oQuery.oDic.oQC=.Null.
                This.oPgFrm.Pages(m.nPage).oQuery.oQueryInfo.oPage.Page1.oGraph.oQC=.Null.
                This.oPgFrm.Pages(m.nPage).oQuery.oQuery.oQC=.Null.
                This.oPgFrm.Pages(m.nPage).oQuery.oQuery.oPivot=.Null.
                This.oPgFrm.Pages(m.nPage).oQuery.RemoveObject("oDic")
                This.oPgFrm.Pages(m.nPage).oQuery.RemoveObject("oQueryInfo")
                This.oPgFrm.Pages(m.nPage).oQuery.RemoveObject("oQuery")
                If Type("this.oPgFrm.pages(m.nPage).oQuery.oFindForm")<>'U'
                    This.oPgFrm.Pages(m.nPage).oQuery.RemoveObject("oFindForm")
                Endif
                If Type("this.oPgFrm.pages(m.nPage).oQuery.oFindRel")<>'U'
                    This.oPgFrm.Pages(m.nPage).oQuery.RemoveObject("oFindRel")
                Endif
                This.oPgFrm.Pages(m.nPage).RemoveObject("oQuery")
                This.oPgFrm.ActivePage = Iif(m.nPage=1, This.oPgFrm.PageCount, 1)
                This.oPgFrm.Click()
                This.oPgFrm.Pages(m.nPage).PageOrder = This.oPgFrm.PageCount
                This.oPgFrm.PageCount = This.oPgFrm.PageCount-1
                This.nActivePage = This.oPgFrm.ActivePage
                If i_VisualTheme<>-1 And i_cMenuTab<>"S"
                    This.oPgFrm.Top = This.oPgFrm.Top - This.oTabMenu.Height
                    This.ImgBackGround.Height = This.ImgBackGround.Height + This.oTabMenu.Height
                    This.RemoveObject("oTabMenu")
                    This.AddObject("oTabMenu", "TabMenu", This, .F., .F., "oPgFrm", .T.)
                    This.oCtrlPage.ZOrder(0)
                Endif
                This.SetCoordinatorInfo(This.oPgFrm.ActivePage)
            Else
                This.oParentObject.NewDoc()
                This.oParentObject.bModified=.F.
                This.oParentObject.bEmpty=.T.
                This.oParentObject.cFileName=''
                This.oParentObject.SetTitle()
            Endif
            This.LockScreen = .F.
            Return .T.
        Else
            Return .F.
        Endif
    Endproc

    Procedure SetPageInfo(nPage)
        If Type("this.oPgFrm.pages(m.nPage).oQuery")<>'U'
            l_numCol = Alen(This.oParentObject.cTables,2)
            l_numRow = Alen(This.oParentObject.cTables,1)
            Dimension This.oPgFrm.Pages(m.nPage).oQuery.cTables(l_numRow, l_numCol)
            Acopy(This.oParentObject.cTables, This.oPgFrm.Pages(This.nActivePage).oQuery.cTables)
            This.oPgFrm.Pages(m.nPage).oQuery.nTables=This.oParentObject.nTables
            This.oPgFrm.Pages(m.nPage).oQuery.cExtMask=This.oParentObject.cExtMask
            This.oPgFrm.Pages(m.nPage).oQuery.bRemoveWhereOnEmptyParam=This.oParentObject.bRemoveWhereOnEmptyParam
            This.oPgFrm.Pages(m.nPage).oQuery.bOrderByParm=This.oParentObject.bOrderByParm
            This.oPgFrm.Pages(m.nPage).oQuery.nVersion=This.oParentObject.nVersion
            This.oPgFrm.Pages(m.nPage).oQuery.cFileName = This.oParentObject.cFileName
            This.oPgFrm.Pages(m.nPage).oQuery.cFileExt=This.oParentObject.cFileExt
            This.oPgFrm.Pages(m.nPage).oQuery.bModified = This.oParentObject.bModified
            This.oPgFrm.Pages(m.nPage).oQuery.bEmpty = This.oParentObject.bEmpty
        Endif
    Endproc

    Procedure SetCoordinatorInfo(nPage)
        If Type("this.oPgFrm.pages(m.nPage).oQuery")<>'U'
            This.oParentObject.oDic = This.oPgFrm.Pages(m.nPage).oQuery.oDic
            This.oParentObject.oGraph = This.oPgFrm.Pages(m.nPage).oQuery.oQueryInfo.oPage.Page1.oGraph
            This.oParentObject.oQuery = This.oPgFrm.Pages(m.nPage).oQuery.oQuery
            This.oParentObject.oFindForm=Iif(Type("this.pages(m.nPage).oQuery.oFindForm")<>'U', This.oPgFrm.Pages(m.nPage).oQuery.oFindForm, .Null.)
            l_numCol = Alen(This.oPgFrm.Pages(m.nPage).oQuery.cTables,2)
            l_numRow = Alen(This.oPgFrm.Pages(m.nPage).oQuery.cTables,1)
            Dimension This.oParentObject.cTables(l_numRow, l_numCol)
            Acopy(This.oPgFrm.Pages(m.nPage).oQuery.cTables, This.oParentObject.cTables)
            This.oParentObject.nTables=This.oPgFrm.Pages(m.nPage).oQuery.nTables
            This.oParentObject.cExtMask=This.oPgFrm.Pages(m.nPage).oQuery.cExtMask
            This.oParentObject.bRemoveWhereOnEmptyParam=This.oPgFrm.Pages(m.nPage).oQuery.bRemoveWhereOnEmptyParam
            This.oParentObject.bOrderByParm=This.oPgFrm.Pages(m.nPage).oQuery.bOrderByParm
            This.oParentObject.nVersion=This.oPgFrm.Pages(m.nPage).oQuery.nVersion
            This.oParentObject.cFileName=This.oPgFrm.Pages(m.nPage).oQuery.cFileName
            This.oParentObject.cFileExt=This.oPgFrm.Pages(m.nPage).oQuery.cFileExt
            This.oParentObject.bModified=This.oPgFrm.Pages(m.nPage).oQuery.bModified
            This.oParentObject.bEmpty = This.oPgFrm.Pages(m.nPage).oQuery.bEmpty
        Endif
    Endproc

    Procedure oPgFrm.Click()
        *--- Salvo i valori
        Local l_numCol, l_numRow, tAssign
        If This.Parent.nActivePage<>This.ActivePage
            This.Parent.SetPageInfo(This.Parent.nActivePage)
            *--- Cambio pagina
            tAssign = This.ActivePage
            This.Parent.SetCoordinatorInfo(tAssign)
        Endif
        This.Parent.nActivePage = This.ActivePage
    Endproc

    Procedure oPgFrm.ActivePage_Assign(xValue)
        If This.ActivePage <> m.xValue
            This.ActivePage = m.xValue
            This.Click()
        Endif
    Endproc

    Procedure Release()
        If Type("this.oParentObject")<>'U' And !Isnull(This.oParentObject)
            This.oParentObject.Exit()
        Endif
        *--- Chiudo i cursori
        DoDefault()
    Endproc

    Procedure Resize()
        Local l_i
        With This.oPgFrm
        	*--- Gadget
	        If Type("oGadgetManager")="O" And !Isnull(oGadgetManager)
                Local nCharmWidth
                If oGadgetManager.Visible And !oGadgetManager.bFullScreen
                    This.MaxWidth= _Screen.Width - oGadgetManager.Width
                Else
                    If !Isnull(oGadgetManager.oCharmBar)
                        If oGadgetManager.oCharmBar.bPin
                            nCharmWidth = oGadgetManager.oCharmBar.oPgFrm.Page1.oPag.oBox_1_2.Width + 1
                        Else
                            nCharmWidth = oGadgetManager.oCharmBar.oPgFrm.Page1.oPag.oBox_1_1.Width - 1
                        Endif
                        This.MaxWidth = _Screen.Width -  m.nCharmWidth
                    Endif
                Endif
	        EndIf
           	.Width = Max(This.Width, MIN_WIDTH)
           	If This.bApplyFormDecorator And This.WindowState = 2
	            .Height = Max(This.Height - This.oDec.Height, MIN_HEIGHT)
	        Else
	        	.Height = Max(This.Height, MIN_HEIGHT)
            EndIf
            For l_i = 1 To .PageCount
                .Pages[l_i].oQuery.Top=1
                .Pages[l_i].oQuery.Left=0
                .Pages[l_i].oQuery.Width = .PageWidth
                .Pages[l_i].oQuery.Height = .PageHeight
            Endfor
        Endwith
        This.oCtrlPage.Top=30
        This.oCtrlPage.Left = This.Width-This.oCtrlPage.Width-4
    Endproc
Enddef

Define Class ControlPage As Container
    Width = 55
    Height = 20
    BackStyle=0
    BorderStyle=0
    BorderWidth=0
    Add Object oPageDown As Image With Picture = "SHRINKBUTTON_NOTSHRUNK_PICTURE.ICO", Top=1, Left=1, ToolTipText=MSG_PAGE_UP, BackStyle=0
    Add Object oPageUp As Image With Picture = "SHRINKBUTTON_SHRUNK_PICTURE.ICO", Top=1, Left=17, ToolTipText=MSG_PAGE_DOWN, BackStyle=0
    Add Object oClosePage As Image With Picture = "CLOSEBUTTON_PICTURE.ico", Top=1, Left=35, ToolTipText=MSG_CLOSE, BackStyle=0
    Add Object __dummy__ As CommandButton With Enabled=.F.,Width=0,Height=0,Style=0,SpecialEffect=1
    Procedure Init()
        This.oPageUp.Picture=i_ThemesManager.GetBmpFromIco("SHRINKBUTTON_SHRUNK_PICTURE.ICO", 16)+'.bmp'
        This.oPageDown.Picture=i_ThemesManager.GetBmpFromIco("SHRINKBUTTON_NOTSHRUNK_PICTURE.ICO", 16)+'.bmp'
        This.oClosePage.Picture=i_ThemesManager.GetBmpFromIco("CLOSEBUTTON_PICTURE.ico", 16)+'.bmp'
    Endproc
    Procedure SetBorderColor(nColor)
        This.oPageUp.BorderColor=nColor
        This.oPageDown.BorderColor=nColor
        This.oClosePage.BorderColor=nColor
    Endproc
    Procedure oPageDown.Click()
        This.Parent.__dummy__.Enabled=.T.
        This.Parent.__dummy__.SetFocus()
        This.Parent.Parent.oPgFrm.ActivePage = Max(This.Parent.Parent.oPgFrm.ActivePage-1, 1)
        This.Parent.Parent.oPgFrm.Click()
        This.Parent.__dummy__.Enabled=.F.
    Endproc
    Procedure oPageUp.Click()
        This.Parent.__dummy__.Enabled=.T.
        This.Parent.__dummy__.SetFocus()
        This.Parent.Parent.oPgFrm.ActivePage = Min(This.Parent.Parent.oPgFrm.ActivePage+1, This.Parent.Parent.oPgFrm.PageCount)
        This.Parent.Parent.oPgFrm.Click()
        This.Parent.__dummy__.Enabled=.F.
    Endproc
    Procedure oClosePage.Click()
        This.Parent.__dummy__.Enabled=.T.
        This.Parent.__dummy__.SetFocus()
        This.Parent.Parent.RemoveQuery(This.Parent.Parent.oPgFrm.ActivePage)
        This.Parent.__dummy__.Enabled=.F.
    Endproc
    Procedure oPageDown.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.BorderStyle=1
        This.Parent.oPageUp.BorderStyle=0
        This.Parent.oClosePage.BorderStyle=0
    Endproc
    Procedure oPageUp.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.BorderStyle=1
        This.Parent.oPageDown.BorderStyle=0
        This.Parent.oClosePage.BorderStyle=0
    Endproc
    Procedure oClosePage.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.BorderStyle=1
        This.Parent.oPageDown.BorderStyle=0
        This.Parent.oPageUp.BorderStyle=0
    Endproc

    Procedure oPageDown.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.BorderStyle=0
    Endproc
    Procedure oPageUp.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.BorderStyle=0
    Endproc
    Procedure oClosePage.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.BorderStyle=0
    Endproc
Enddefine

Define Class ResizerLine As Shape
    Width = 4
    Height = 4
    bVertical = .T.
    nMinHeight = MIN_HEIGHT
    nMinWidth = MIN_WIDTH
    nBackColor = 0
    Procedure Init(bVertical)
        This.bVertical = m.bVertical
        This.MousePointer = Iif(m.bVertical, 9, 7)
        If i_VisualTheme<>-1
            This.BorderColor=i_ThemesManager.GetProp(7)
            This.BackColor=i_ThemesManager.GetProp(7)
        Else
            This.BorderColor=Rgb(236,233,216)
            This.BackColor=Rgb(236,233,216)
        EndIf
        This.nBackColor = This.BackColor
    Endproc
    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton == 1
            If This.bVertical
                This.Move(Max(This.nMinWidth, Min(This.Parent.Width-This.nMinWidth,nXCoord)),This.Top,This.Width,This.Height)
            Else
            	Local l_nTop
            	If Type("ThisForm.oDec")<>"U"
            		l_nTop = Max(This.nMinHeight, Min(This.Parent.Height-This.nMinHeight,nYCoord-Sysmetric(9))-ThisForm.oDec.Height)
            	Else
            		l_nTop = Max(This.nMinHeight, Min(This.Parent.Height-This.nMinHeight,nYCoord-Sysmetric(9)))
            	EndIf
                This.Move(This.Left,l_nTop,This.Width,This.Height)
            Endif
            This.Parent.Resize()
        Endif
    Endproc
    Procedure DblClick()
        If This.bVertical
            This.Move(Max(This.nMinWidth, Min(This.Parent.Width-This.nMinWidth,This.Parent.Width/2)),This.Top,This.Width,This.Height)
        Else
            This.Move(This.Left, Max(This.nMinHeight, Min(This.Parent.Height-This.nMinHeight,This.Parent.Height*1/2-Sysmetric(9))),This.Width,This.Height)
        Endif
        This.Parent.Resize()
    EndProc
    
	Proc MouseEnter(nButton, nShift, nXCoord, nYCoord)
        If Type("ThisForm.oDec")<>"U"
        	This.BackColor = RGBAlphaBlending(This.BackColor,GetBestForeColor(ThisForm.BackColor),0.3)
        EndIf
    Endproc

    Proc MouseLeave(nButton, nShift, nXCoord, nYCoord)
        This.BackColor = This.nBackColor
    Endproc
Enddefine

Define Class query_result As Container
	Width = 100
	Height = 100
	cCursor = ""
	Add Object oGrd As Grid with Top = 3, Left = 3, Width = 94, Height = 94, Anchor = 15, ReadOnly=.T., DeleteMark=.F.
	Add Object oBtnBrowse As CommandButton with Top=80,Left=80, Height=15, Width=15, Anchor = 12, Caption = "...", Visible=.F.,ToolTipText="Browse"
	&& Add Object oLblMsg As Label with Top=38,Left=3,Width=94, Height=25, Caption="Waiting for result...", Alignment=2, Visible=.F., Anchor=522, BackStyle=0,;
									&& FontSize=15
    #If Version(5)>=900
        Add Object ImgBackGround As Image With Stretch=2, Visible=.F.
    #Endif
	
	Proc oGrd.MouseWheel(nDirection, nShift, nXCoord, nYCoord)
		If m.nShift = 2
			NoDefault
			If nDirection>0
				This.FontSize=Max(5,This.FontSize+1)
			Else
				This.FontSize=Max(5,This.FontSize-1)
			EndIf
		EndIf
	EndProc
	
	Proc ogrd.RightClick()
		This.Parent._RightClick()
	EndProc
	
	Proc _RightClick()
		*--- Creo menu
		nCmd=0
		Define Popup popCmd From Mrow(),Mcol() SHORTCUT
		Define Bar 1 Of popCmd Prompt cp_Translate(MSG_EXCEL_EXPORT) Skip For !Used(This.cCursor)
		On Selection Bar 1 Of popCmd nCmd = 1
		Define Bar 2 Of popCmd Prompt cp_Translate(MSG_DBF_EXPORT) Skip For !Used(This.cCursor)
		On Selection Bar 2 Of popCmd nCmd = 2
		Define Bar 3 Of popCmd Prompt cp_Translate(MSG_CSV_EXPORT) Skip For !Used(This.cCursor)
		On Selection Bar 3 Of popCmd nCmd = 3		
		Activate Popup popCmd
		Deactivate Popup popCmd
		Release Popups popCmd
		*--- Azioni

		local l_oldArea, cFileName , ftemp
		cFileName = EVL(This.Parent.Parent.Parent.Parent.cFilename, CP_MSGFORMAT(MSG_NO_NAME))
		l_oldArea = Select()
		IF USED(This.cCursor)
			Select(This.cCursor)
			Do Case
				Case nCmd=1
					cFileName = Forceext(cFileName, "XLSX")
					cFileName =PUTFILE("Excel file:" , cFileName , "XLSX")
					If !Empty(cFileName)
						cursortoxlsx( cFileName  , "Foglio1",ALIAS())
						STAPDF(cFileName ,"OPEN"," ")
					ENDIF
				Case nCmd=2
					cFileName = Forceext(cFileName, "DBF")
					cFileName = PUTFILE("Dbf file:" , cFileName , "DBF")
					If !Empty(cFileName)
						COPY TO (cFileName)
					EndIf
				Case nCmd=3
					cFileName = Forceext(cFileName, "CSV")
					cFileName = PUTFILE("CSV file:" , cFileName , "CSV")
					If !Empty(cFileName)
						COPY TO (cFileName) CSV
					EndIf
			EndCase
			Select(l_oldArea)
		endif
	EndProc
	
	Proc Init(pParent)
		This.cCursor = "VQB_"+sys(2015)
		Create cursor (This.cCursor) (VUOTO C(10))
		This.oGrd.RecordSource = This.cCursor
		pParent.cCursor = This.cCursor	&&oQuery
        If i_VisualTheme<>-1
            If i_bGradientBck
                With This.ImgBackGround
                    .Width = This.Width
                    .Height = This.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            This.BackColor = i_ThemesManager.GetProp(7)
        Else
            This.BackColor = Rgb(252,252,254)
        Endif		
	Endproc

	Proc SetGrd()
		This.oGrd.FontSize = 8
		This.oGrd.FontName = "MS Sans Serif"
		This.oGrd.SetAll("FontBold", .T., "Header")
		This.oGrd.GridLineColor = RGB(175,175,175)
		This.oBtnBrowse.Visible= (Used(This.cCursor) And Reccount(This.cCursor)>0)
		FOR EACH loColumn IN THIS.oGrd.Columns
			BINDEVENT(loColumn.text1, "Rightclick", This, "_RightClick")
		ENDFOR
	EndProc
	
	Proc oBtnBrowse.Click()
		If Used(This.Parent.cCursor)
			Select(This.Parent.cCursor)
			Activate Screen
			*--- Brow Title cp_Translate(MSG_VIEW_QUERY) NOWAIT NODELETE NOMODIFY NOAPPEND
			Local cFileName
			cFileName=EVL(This.Parent.Parent.Parent.Parent.Parent.cFilename, Forceext(CP_MSGFORMAT(MSG_NO_NAME), "VQR"))
			Brow Title (cFileName) NOWAIT NODELETE NOMODIFY NOAPPEND
		EndIf
	EndProc
	
	Proc Destroy()
		*--- Chiusura cursore risultato
		USE IN SELECT(This.cCursor)
	Endproc
	
EndDefine

Define Class query_msg As Container
	Width = 100
	Height = 100

	Add Object oEdit As EditBox with Top = 3, Left = 3, Width = 94, Height = 60, Anchor = 15, ReadOnly=.T.,;
								FontName = "Courier New", FontSize = 8, Margin=1
	Add Object oBtnClipBoard As CommandButton With Top=65,Left=3, Width=105, Height=25, Caption='Copy to Clipboard', Anchor=6
	
	Proc oBtnClipBoard.Click()
		_cliptext=This.Parent.oEdit.Value
	EndProc
	
	PROCEDURE oEdit.MouseWheel(nDirection, nShift, nXCoord, nYCoord)
		If m.nShift = 2
			NoDefault
			If nDirection>0
				This.FontSize=Max(5,This.FontSize+1)
			Else
				This.FontSize=Max(5,This.FontSize-1)
			EndIf
		EndIf
	EndProc
	
EndDefine


*--- Container per pageframe con Graph, Result, Messages, Pivot, Find, Find Rel.
Define Class query_info As Container
    BackStyle = 0
    BorderStyle = 0
    BorderWidth = 0
    
	Add Object oPage As PageFrame With PageCount=6, Width=This.Width, Height=This.Height, TabStyle=1,;
        Page1.BackStyle=0,page2.BackStyle=0,page3.BackStyle=0,page4.BackStyle=0,page5.BackStyle=0,Page6.BackStyle=0
    Add Object __dummy__ As CommandButton With Enabled=.F.,Width=0,Height=0,Style=0,SpecialEffect=1
	
	Proc Init(pParent)
		With This
			.Top = 0
			.Left = (.Parent.Width/2)
			.Width = (.Parent.Width/2)-5
			.Height = (.Parent.Height/2)-5		
			
			.oPage.Width = .Width
			.oPage.Height = .Height
			
			.oPage.Page1.Caption = "Graph"
			.oPage.Page2.Caption = "Find"
			.oPage.Page3.Caption = "Find Relations"
			.oPage.Page4.Caption = "Pivot"
			.oPage.Page5.Caption = "Result"
			.oPage.Page6.Caption = "Messages"

		*--- Aggiungo oggetti all'interno delle pagine
			*--- Graph
			.oPage.Page1.AddObject("oGraph", "query_graph", pParent)
			.oPage.Page1.oGraph.Top = 0
			.oPage.Page1.oGraph.Left = 0
			.oPage.Page1.oGraph.Width = .oPage.PageWidth
			.oPage.Page1.oGraph.Height = .oPage.PageHeight
			.oPage.Page1.oGraph.Anchor = 15			
			.oPage.Page1.oGraph.Visible = .T.
			*--- Find
			.oPage.Page2.AddObject("oFindForm", 'find_tables_and_fields', .Parent.oDic)
            .Parent.oDic.oFindForm = .oPage.Page2.oFindForm
			.oPage.Page2.oFindForm.Top = 1
			.oPage.Page2.oFindForm.Left = 1
			.oPage.Page2.oFindForm.Width = .oPage.PageWidth
			.oPage.Page2.oFindForm.Height = .oPage.PageHeight
			.oPage.Page2.oFindForm.Anchor = 15
			.oPage.Page2.oFindForm.Visible = .T.
			*--- Find Relations
			.oPage.Page3.AddObject("oFindRel", "query_findrelations", pParent)
			.oPage.Page3.oFindRel.Top = 0
			.oPage.Page3.oFindRel.Left = 0
			.oPage.Page3.oFindRel.Width = .oPage.PageWidth-3
			.oPage.Page3.oFindRel.Height = .oPage.PageHeight-5
			.oPage.Page3.oFindRel.Anchor = 15
			.oPage.Page3.oFindRel.Visible = .T.
			*--- Pivot
			.oPage.Page4.AddObject("oPivot", "query_pivot", .Parent.oQuery)
			.Parent.oQuery.oPivot = .oPage.Page4.oPivot			
			.oPage.Page4.oPivot.Top = 0
			.oPage.Page4.oPivot.Left = 0
			.oPage.Page4.oPivot.Width = .oPage.PageWidth-5
			.oPage.Page4.oPivot.Height = .oPage.PageHeight-5
			.oPage.Page4.oPivot.Anchor = 15
			.oPage.Page4.oPivot.Visible = .T.			
			*--- Result
			.oPage.Page5.AddObject("oResult", "query_result", .Parent.oQuery)
			.oPage.Page5.oResult.Top = 0
			.oPage.Page5.oResult.Left = 0
			.oPage.Page5.oResult.Width = .oPage.PageWidth
			.oPage.Page5.oResult.Height = .oPage.PageHeight
			.oPage.Page5.oResult.Anchor = 15
			.oPage.Page5.oResult.Visible = .T.
			*--- Messages
			.oPage.Page6.AddObject("oMsg", "query_msg")
			.oPage.Page6.oMsg.Top = 0
			.oPage.Page6.oMsg.Left = 0
			.oPage.Page6.oMsg.Width = .oPage.PageWidth
			.oPage.Page6.oMsg.Height = .oPage.PageHeight
			.oPage.Page6.oMsg.Anchor = 15
			.oPage.Page6.oMsg.Visible = .T.
			
			If i_VisualTheme<>-1 And i_cMenuTab<>"S"
				.oPage.Tabs = .F.
				.oPage.BorderWidth = 0
				.AddObject("oTabMenu", "TabMenu", This, .F., .F., "oPage")
				.oPage.Height = .oPage.Height - .oTabMenu.Height
				.BackColor = i_ThemesManager.GetProp(7)
				.oPage.BorderWidth = 1
			EndIf
			.oPage.Anchor = 15
		EndWith
	EndProc
	
	Proc oPage.Page3.Activate()
		This.oFindRel.SetInfo()
	EndProc
	
EndDefine

Define Class query_builderContainer As Container
    BackStyle = 0
    BorderStyle= 0
    BorderWidth=0
    Width = 500
    Height = 500
    #If Version(5)>=900
        Add Object ImgBackGround As Image With Stretch=2, Visible=.F.
    #Endif
    Dimension cTables[1,2]
    nTables=0
    cExtMask=''
    bRemoveWhereOnEmptyParam=.F.
    bOrderByParm=.F.
    nVersion=0
    oThis=.Null.

    oWord=.Null.
    oExcel=.Null.
    cFileName=''
    cFileExt=''
    bModified=.F.
    bEmpty = .T.

    Procedure Init(pParent)
        This.AddObject("oDic", "query_tables", pParent)
        This.oDic.Visible=.T.

        This.AddObject("oQuery", "query_selitems", pParent)
        This.oQuery.Visible=.T.
		
        This.AddObject("oQueryInfo", "query_info", pParent)
        This.oQueryInfo.Visible=.T.
		
        && This.AddObject("oGraph", "query_graph", pParent)
        && This.oGraph.Visible=.T.
        *--- Splitter
        This.AddObject("VSplitter", "ResizerLine", .T.)
        This.VSplitter.Left=(This.Width/2)-2
        This.VSplitter.Top = 0
        This.VSplitter.Height = This.Height/2
        This.VSplitter.Visible=.T.
        *--- Splitter
        This.AddObject("HSplitter", "ResizerLine", .F.)
        This.HSplitter.Left=2
        This.HSplitter.Top = (This.Height/2)
        This.HSplitter.Width = This.Parent.Parent.PageWidth	- 4
        This.HSplitter.Visible=.T.
        *--- Filename label
        This.AddObject("LFileName", "Label")
        This.LFileName.BackStyle=0
        This.LFileName.Visible = .T.
        This.AddObject("LLine", "Line")
        This.LLine.Visible = .T.
        This.LLine.BorderColor=Rgb(127,157,185)

        If i_VisualTheme<>-1
            If i_bGradientBck
                With This.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            This.BackColor = i_ThemesManager.GetProp(7)
            This.BackStyle = 1
            This.LLine.BorderColor=i_ThemesManager.GetProp(7)
        Endif
		This.Resize()
    Endproc

    Procedure Resize()
        This.oDic.Top = 1
        This.oDic.Left = 1
        This.oDic.Width=Max(1,This.VSplitter.Left)
        This.oDic.Height=This.HSplitter.Top

        This.oQueryInfo.Top = 21
        This.oQueryInfo.Left =This.VSplitter.Left+5
        This.oQueryInfo.Width=Max(MIN_WIDTH,This.Width-This.VSplitter.Left-5)
        This.oQueryInfo.Height=This.HSplitter.Top - 24
        This.VSplitter.Height = This.oQueryInfo.Height + This.oQueryInfo.Top

        This.LFileName.Top = 1
        This.LFileName.Left = This.oQueryInfo.Left
        This.LFileName.Width = Max(MIN_WIDTH,This.oQueryInfo.Width - 60)

        This.LLine.Top = 20
        This.LLine.Height = 0
        This.LLine.Left = This.oQueryInfo.Left
        This.LLine.Width = This.oQueryInfo.Width

        This.oQuery.Top = This.HSplitter.Top+3
        This.oQuery.Left = 1
        This.oQuery.Width= This.Width-3
        If i_VisualTheme<>-1 And i_cMenuTab<>"S"
            This.oQuery.Height= Max(MIN_HEIGHT,This.Height-This.HSplitter.Top-This.oQuery.oTabMenu.Height)
        Else
            This.oQuery.Height= Max(MIN_HEIGHT,This.Height-This.HSplitter.Top-6)
        Endif

        && This.oPivot.stdSize()

        && If Type("This.oFindRel")<>"U"
            && This.oFindRel.Resize()
        && Endif
        && If Type("this.oFindForm")<>"U"
            && This.oFindForm.stdSize()
        && Endif

        This.HSplitter.Width = This.Parent.Parent.PageWidth
    Endproc
Enddefine

Define Class ArrayList As ListBox
    Width=200
    Top=20
    ColumnWidths="150,150"
    ColumnCount=2
    Sorted=.T.
    Dimension aInfo[1,2]
    *--- Utilizzato nell'expression builder, per visualizzazione fields list
    Dimension aTmpInfo[1,2]

Enddefine

Define Class query_tables As CpSysContainer
    oQC=.Null.
    Closable=.F.
    cCurrAlias=''
    cCurrTable=''
    Caption = "Dictionary"
    Icon=i_cBmpPath+i_cStdIcon
    nquerydefs=0
    ShowTips=.T.
    oFindForm=.Null.
    bFox26=.F.
    Dimension querydefs[1,2]
    Add Object LTables As ArrayList With Width=200, Top=20, ColumnWidths="150,150", ColumnCount=2, Sorted=.T.
    Add Object LTbls   As ArrayList With Width=200, Top=20, ColumnWidths="150,150", ColumnCount=2, Sorted=.T., Visible=.F.
    Add Object LFields As ListBox With Width=200, Left=205, Top=20, ColumnWidths="100,100", ColumnCount=2, MultiSelect=1
    Add Object LRelated As ListBox With Width=405, Top=210, Height=90
    Add Object TxtTbl As Label With Caption="Tables", BackStyle = 0
    && Add Object FindTbl As CommandButton With Caption="Find",Width=30,Left=190
    Add Object AddTbl As CommandButton With Caption="Add",Width=30,Left=220 &&, visible=Not(VARTYPE(i_bFox26)='L' AND i_bfox26)
    Add Object TxtFld As Label With Width=200, Left=210, Caption="Fields", BackStyle = 0
    Add Object TxtRel As Label With Width=200, Top=193, Caption="Relationships", BackStyle = 0
    && Add Object SrcRel As CommandButton With Width=50, Height=18 ,Left=345, Top=166, Caption="Find"
    && Proc SrcRel.Click()
        && This.Parent.oQC.SearchRelations()
    Proc Init(oQC)
        Local i, i_NameTable
        This.oQC=oQC
        This.LoadList()
        This.Left = This.Parent.Width/2
        This.Width=This.Parent.Width/2
        This.Height=This.Parent.Height/2
        This.Resize
        If Vartype(i_bFox26)='L'
            If i_bFox26
                This.bFox26=.T.
            Endif
        Endif
        If !This.bFox26
            This.AddTbl.Visible=.T.
        Else
            This.AddTbl.Visible=.F.
        Endif
    Procedure LoadList()
        Local l_NameTable, l_DesTable, l_numTab
        l_numTab = This.oQC.oDcx.GetTablesCount()
        Dimension This.LTables.aInfo(l_numTab, 2)
        Dimension This.LTbls.aInfo(l_numTab, 2)
        For i=1 To l_numTab
            l_NameTable=This.oQC.oDcx.GetTable(i)
            l_DesTable = cp_Translate(This.oQC.oDcx.GetTableDescr(i))
            This.LTables.aInfo(i, 1) = Alltrim(l_DesTable)
            This.LTables.aInfo(i, 2) = l_NameTable
            This.LTbls.aInfo(i, 1) = Alltrim(Lower(l_NameTable))
            This.LTbls.aInfo(i, 2) = l_NameTable
        Next
        Asort(This.LTables.aInfo, 1,-1,0,1)
        This.LTables.RowSourceType=5
        This.LTables.RowSource = "This.aInfo"
        Asort(This.LTbls.aInfo, 1,-1,0,1)
        This.LTbls.RowSourceType=5
        This.LTbls.RowSource = "This.aInfo"
    Endproc
    Proc Reset()
        If This.LTables.ListIndex<>0
            This.LTables.Selected(This.LTables.ListIndex)=.F.
        Endif
        This.LFields.Clear()
        This.LRelated.Clear()
        This.cCurrAlias=''
        This.cCurrTable=''
    Proc Resize()
        * --- Modifico la larghezza
        This.LRelated.Width = This.Width-5
        This.LTables.Width = Int(This.Width/2)-5
        This.LTables.ColumnWidths = Ltrim(Str(Int((This.Width/2)-5)))+",0"
        This.LFields.Width = Int(This.Width/2)-5

        This.LTbls.Width = Int(This.Width/2)-5
        This.LTbls.ColumnWidths = Ltrim(Str(Int(This.Width/2)-5))+",0"
        This.LTbls.Height = Int(This.Height/3*2)-35
        This.TxtFld.Width = This.LFields.Width
        This.TxtRel.Width = This.LRelated.Width
        This.LFields.ColumnWidths = Ltrim(Str(Int((This.Width/2)-5)/2))+","+Ltrim(Str(Int((This.Width/2)-5)/2))

        * --- Modifico la posizione
        This.LFields.Left = Int(This.Width/2)
        This.TxtFld.Left = Int(This.Width/2)
        && This.SrcRel.Left = This.Width-55
        This.LRelated.Top = Int(This.Height/3*2)+5
        This.TxtRel.Top = Int(This.Height/3*2)-11
        && This.SrcRel.Top = Int(This.Height/3*2)-13
        This.AddTbl.Left = Int(This.Width/2)-35
        && This.FindTbl.Left = Int(This.Width/2)-65

        * --- Modifico la lunghezza
        This.LRelated.Height = Int(This.Height/3)-10
        This.LTables.Height = Int(This.Height/3*2)-35
        This.LFields.Height = Int(This.Height/3*2)-35
    Proc RemoveCurrentTable(cAlias)
        If cAlias==This.cCurrAlias And This.cCurrTable<>cAlias
            This.cCurrAlias=This.cCurrTable
            This.TxtFld.Caption="Fields of "+This.cCurrTable
            This.TxtRel.Caption="Relations of "+This.cCurrTable
        Endif
    Proc FillRelated(cName,cDescr)
        Local i
        Thisform.LockScreen=.T.
        This.LRelated.Clear()
        For i=1 To This.oQC.oDcx.GetFKCount(cName)
            This.LRelated.AddItem(This.oQC.oDcx.GetFKDescr(cName,i))
        Next
        For i=1 To This.oQC.oDcx.GetIFKCount(cName)
            This.LRelated.AddItem(This.oQC.oDcx.GetIFKDescr(cName,i))
        Next
        This.TxtRel.Caption='Relations of '+Alltrim(cDescr)
        Thisform.LockScreen=.F.
    Proc FillFields(cName,cDescr)
        Local i
        Thisform.LockScreen=.T.
        This.LFields.Clear()
        If Not('.VQR'$cDescr)
            For i=1 To This.oQC.oDcx.GetFieldsCount(cName)
                * --- Implemento la traduzione della Descrizione dei Campi del Dizionario
                **this.LFields.AddItem(this.oQC.oDcx.GetFieldDescr(cName,i))
                This.LFields.AddItem(cp_Translate(This.oQC.oDcx.GetFieldDescr(cName,i)))
                This.LFields.List(i,2)=This.oQC.oDcx.GetFieldName(cName,i)
            Next
        Else
            Local i_oQry
            i_oQry=This.GetQueryDefinitions(cName)
            For i=1 To i_oQry.nFields
                This.LFields.AddItem(i_oQry.i_Fields[i,2])
                This.LFields.List(i,2)=i_oQry.i_Fields[i,2]
            Next
        Endif
        This.TxtFld.Caption='Fields of '+Alltrim(cDescr)
        Thisform.LockScreen=.F.
    Proc GetQueryDefinitions(cName)
        Local i_oQry,i_oQryLoader
        * --- cerca nell' array delle query memorizzate
        j=0
        For i=1 To This.nquerydefs
            If This.querydefs[i,1]==cName
                j=i
            Endif
        Next
        If j=0
            i_oQry=Createobject('cpquery')
            i_oQryLoader=Createobject('cpqueryloader')
            i_oQryLoader.LoadQuery(cName,i_oQry,.Null.)
            * --- aggiunge all' elenco delle query cachate
            This.nquerydefs=This.nquerydefs+1
            Dimension This.querydefs[this.nquerydefs,2]
            This.querydefs[this.nquerydefs,1]=cName
            This.querydefs[this.nquerydefs,2]=i_oQry
        Else
            i_oQry=This.querydefs[j,2]
        Endif
        Return (i_oQry)
    Proc SelectTable(cName,cDescr,cAlias)
        * --- seleziona nella lista la tabella cName
        Local i
        For i=1 To This.LTables.ListCount
            If This.LTables.List(i,2)==cName
                This.LTables.Selected(i)=.T.
            Endif
            If This.LTbls.List(i,2)==cName
                This.LTbls.Selected(i)=.T.
            Endif
        Next
        This.LTables.Refresh()
        * --- riempie i campi e le realzioni
        This.cCurrAlias=cAlias
        This.cCurrTable=cName
        This.FillFields(cName,cDescr)
        This.FillRelated(cName,cDescr)

    Proc LTbls.Click()
        Local i,j
        *j=this.GetSel()
        j=This.ListIndex
        *wait window str(this.listitemid)+' '+str(this.listindex)+' l1='+this.list(j,1)+' l2='+this.list(j,2)
        If j<>0
            This.Parent.cCurrAlias=This.List(j,2)
            This.Parent.cCurrTable=This.List(j,2)
            This.Parent.FillRelated(This.List(j,2),This.List(j,1))
            This.Parent.FillFields(This.List(j,2),This.List(j,1))
            This.Parent.LFields.ToolTipText=''
        Endif

    Proc LTbls.DblClick()
        Local j, Descri, tblname
        *j=this.GetSel()
        j=This.ListIndex
        If j<>0
            tblname=This.Parent.oQC.oDcx.GetTableDescr(This.Parent.oQC.oDcx.GetTableIdx(This.List(j,2)))
            Descri=Iif(Not Empty(tblname), tblname, This.List(j,1))
            This.Parent.oQC.AddTable(This.List(j,2),Alltrim(Descri))
        Endif

	Proc LTbls.MouseDown(nButton, nShift, nXCoord, nYCoord)
		If nButton=1
			*--- Mi posizioni su Graph	
			This.Parent.Parent.oQueryInfo.oPage.ActivePage=1
			This.Parent.oQC.oQuery.oPage.ActivePage=1
			This.SetFocus()
		EndIf
	EndProc		
		
    Proc LTbls.MouseMove(nButton, nShift, nXCoord, nYCoord)
        Local N
        *n=this.GetSel()
        N=This.ListIndex
		IF nButton=1
			If m.N<>0
				This.Selected(m.N)=.T.			
				This.DragIcon='dragmove.cur'
				This.Drag(1)
			EndIf
        Endif

    Proc LTbls.DragDrop(oSource,nX,nY)
        If Upper(oSource.Name)==Upper(This.Name)
            This.Click()
        Endif

    Proc LTbls.RightClick()
        This.Parent.RightClick()

    Proc LTables.RightClick()
        This.Parent.RightClick()

    Proc RightClick()
        *--- Menu visualizza
        Local cVis, i
        cVis=''
        Define Popup popVis From Mrow(),Mcol() SHORTCUT Margin
        Define Bar 1 Of popVis Prompt 'Table \<Description'
        Define Bar 2 Of popVis Prompt 'Table \<Name'
        On Selection Bar 1 Of popVis cVis = 'D'
        On Selection Bar 2 Of popVis cVis = 'N'
        Set Mark Of Bar 1 Of popVis To This.LTables.Visible
        Set Mark Of Bar 2 Of popVis To This.LTbls.Visible
        Activate Popup popVis
        Deactivate Popup popVis
        Release Popups popVis
        Do Case
            Case cVis = 'D'
                cName = This.LTbls.List(This.LTbls.ListIndex, 2)
                For i=1 To This.LTables.ListCount
                    If This.LTables.List(i,2)==cName
                        This.LTables.Selected(i)=.T.
                    Endif
                Endfor
                This.LTables.Click()
                This.LTbls.Visible=.F.
                This.LTables.Visible=.T.
                This.LTables.Requery()
            Case cVis = 'N'
                cName = This.LTables.List(This.LTables.ListIndex, 2)
                For i=1 To This.LTbls.ListCount
                    If This.LTbls.List(i,2)==cName
                        This.LTbls.Selected(i)=.T.
                    Endif
                Endfor
                This.LTbls.Click()
                This.LTables.Visible=.F.
                This.LTbls.Visible=.T.
                This.LTbls.Requery()
        Endcase

    Proc LTables.Click()
        Local i,j
        *j=this.GetSel()
        j=This.ListIndex
        *wait window str(this.listitemid)+' '+str(this.listindex)+' l1='+this.list(j,1)+' l2='+this.list(j,2)
        If j<>0
            This.Parent.cCurrAlias=This.List(j,2)
            This.Parent.cCurrTable=This.List(j,2)
            This.Parent.FillRelated(This.List(j,2),This.List(j,1))
            This.Parent.FillFields(This.List(j,2),This.List(j,1))
            This.Parent.LFields.ToolTipText=''
        Endif
    Proc LTables.DblClick()
        Local j
        *j=this.GetSel()
        j=This.ListIndex
        If j<>0
            This.Parent.oQC.AddTable(This.List(j,2),Alltrim(This.List(j,1)))
        Endif
	Proc LTables.MouseDown(nButton, nShift, nXCoord, nYCoord)
		If nButton=1
			*--- Mi posizioni su Graph	
			This.Parent.Parent.oQueryInfo.oPage.ActivePage=1
			This.Parent.oQC.oQuery.oPage.ActivePage=1
			This.SetFocus()
		EndIf
	EndProc
    Proc LTables.MouseMove(nButton, nShift, nXCoord, nYCoord)
        Local N
        *n=this.GetSel()
        N=This.ListIndex
        If nButton=1 And m.N<>0
			This.Selected(m.N)=.T.			
			This.DragIcon='dragmove.cur'
			This.Drag(1)
        Endif

    Proc LTables.DragDrop(oSource,nX,nY)
        If Upper(oSource.Name)==Upper(This.Name)
            This.Click()
        Endif
    Proc LFields.DblClick()
        This._Dblclick(This.ListIndex)
    Proc LFields._Dblclick(pIndex)
        Local i,j,nFld,cTable,cType,nLen,nDec
        j = m.pIndex
        If j<>0
            cTable=This.Parent.cCurrTable
            If  This.Parent.oQC.oDcx.GetTableIdx(cTable)<>0
                nFld=This.Parent.oQC.oDcx.GetFieldIdx(cTable,This.List(j,2))
                cType=This.Parent.oQC.oDcx.GetFieldType(cTable,nFld)
                nLen=This.Parent.oQC.oDcx.GetFieldLen(cTable,nFld)
                nDec=This.Parent.oQC.oDcx.GetFieldDec(cTable,nFld)
            Else
                Local i_oQry,i,j
                i_oQry=This.Parent.GetQueryDefinitions(cTable)
                nFld=i_oQry.i_Fields[j,2]
                cType=i_oQry.i_Fields[j,3]
                nLen=i_oQry.i_Fields[j,4]
                nDec=i_oQry.i_Fields[j,5]
            Endif
            This.Parent.oQC.AddField(This.List(j,2),This.List(j,1),This.Parent.cCurrAlias,cType,nLen,nDec)
        Endif
    Proc LFields.MouseMove(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            If This.Parent.oQC.oQuery.oPage.ActivePage<>4 And This.Parent.oQC.oQuery.oPage.ActivePage<>2;
                    and This.Parent.oQC.oQuery.oPage.ActivePage<>5
                This.Parent.oQC.oQuery.oPage.ActivePage=2
            Endif
            This.DragIcon='dragmove.cur'
            This.Drag(1)
            This.Click()
        Endif
    Proc LRelated.DblClick()
        Local i,j,oDcx,cName,cAlias2,cExp,cDescr,bIsChild,cType
        j=This.ListIndex
        If j<>0
            cName=This.Parent.cCurrTable
            cAlias1=This.Parent.cCurrAlias
            oDcx=This.Parent.oQC.oDcx
            If j<=oDcx.GetFKCount(cName)
                cAlias2=oDcx.GetFKTable(cName,j)
                cExp=oDcx.GetFKExpr(cName,j,cAlias2,cAlias1)
                cDescr=oDcx.GetFKDescr(cName,j)
                If oDcx.GetFKIsChild(cName,j) && bIsChild
                    This.Parent.oQC.Addrelation(cAlias2,cAlias1,cDescr,cExp,'Left outer',.T.)
                Else
                    This.Parent.oQC.Addrelation(cAlias2,cAlias1,cDescr,cExp,'Right outer',.T.)
                Endif
            Else
                j=j-oDcx.GetFKCount(cName)
                cAlias2=oDcx.GetIFKTable(cName,j)
                cExp=oDcx.GetIFKExpr(cName,j,cAlias1,cAlias2)
                cDescr=oDcx.GetIFKDescr(cName,j)
                If oDcx.GetIFKIsChild(cName,j) && bIsChild
                    This.Parent.oQC.Addrelation(cAlias1,cAlias2,cDescr,cExp,'Left outer',.T.)
                Else
                    This.Parent.oQC.Addrelation(cAlias1,cAlias2,cDescr,cExp,'Right outer',.T.)
                Endif
            Endif
        Endif
    Proc LRelated.MouseMove(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            This.Parent.oQC.oQuery.oPage.ActivePage=3
            This.DragIcon='dragmove.cur'
            This.Drag(1)
        Endif
    && Proc FindTbl.Click()
        && If Type("this.parent.Parent.oFindForm")='U'
            && This.Parent.Parent.AddObject("oFindForm", 'find_tables_and_fields',This.Parent)
            && This.Parent.oFindForm=This.Parent.Parent.oFindForm
        && Endif
        && This.Parent.oFindForm.show_()
        && Return
    Proc AddTbl.Click()
        Local i_fn,i,i_n,i_bStop
        * --- chiede il nome della query
        i_fn=Getfile('vqr')
        If Not(Empty(i_fn))
            * --- toglie la path relativa
            i_n=Sys(5)+Sys(2003)
            If Right(i_n,1)<>'\'
                i_n=i_n+'\'
            Endif
            i_fn=Strtran(Upper(i_fn),Upper(i_n),'')
            * --- definisce il nome che avra' la tabella: il nome della query senza path
            i=Rat('\',i_fn)
            If i=0
                i=At(':',i_fn)
            Endif
            i_n=i_fn
            If i<>0
                i_n=Substr(i_n,i+1)
            Endif
            * --- aggiunge il nome cosi' formato alla lista delle tabelle
            i_bStop=.F.
            For i=1 To This.Parent.LTables.ListCount
                If This.Parent.LTables.List(i,1)==i_fn
                    i_bStop=.T.
                Endif
            Next
            If !i_bStop
                i=This.Parent.LTables.ListCount+1
                Dimension This.Parent.LTables.aInfo(i,2)
                Dimension This.Parent.LTbls.aInfo(i,2)
                This.Parent.LTables.aInfo(i,1) = i_fn
                This.Parent.LTables.aInfo(i,2) = Strtran(Upper(i_n),'.VQR','')
                Asort(This.Parent.LTables.aInfo, 1,-1,0,1)
                This.Parent.LTbls.aInfo(i,1)	= i_fn
                This.Parent.LTbls.aInfo(i,2) = Strtran(Upper(i_n),'.VQR','')
                Asort(This.Parent.LTbls.aInfo, 1,-1,0,1)
                If This.Parent.LTables.Visible
                    This.Parent.LTables.Requery()
                Else
                    This.Parent.LTbls.Requery()
                Endif
            Else
                CP_MSG(cp_Translate(MSG_DUPLICATED_TABLE_NAME),.F.)
            Endif
        Endif
        Return
Enddefine

Define Class find_tables_and_fields As CpSysContainer
    Closable=.F.
    Top=1
    Left = 1
    Height=305
    Width = 320
    BackStyle=1
    Caption='Find Table and Fields'
    oListPage=.Null.
    #If Version(5)>=900
        Add Object ImgBackGround As Image With Stretch=2, Visible=.F.
    #Endif
	MinHeight=100
    Add Object lbltd As Label With Top=10,Left=10,Width=100,Alignment=1,Caption='Table description:', BackStyle = 0
    Add Object lbltn As Label With Top=35,Left=10,Width=100,Alignment=1,Caption='Table name:', BackStyle = 0
    Add Object lblfd As Label With Top=60,Left=10,Width=100,Alignment=1,Caption='Field description:', BackStyle = 0
    Add Object lblfn As Label With Top=85,Left=10,Width=100,Alignment=1,Caption='Field name:', BackStyle = 0
    Add Object txttd As TextBox With Top=8,Left=111,Width=200,Height=23, Anchor=10
    Add Object txttn As TextBox With Top=33,Left=111,Width=200,Height=23, Anchor=10
    Add Object txtfd As TextBox With Top=58,Left=111,Width=200,Height=23, Anchor=10
    Add Object txtfn As TextBox With Top=83,Left=111,Width=200,Height=23, Anchor=10
    Add Object findbtn As CommandButton With Top=110,Left=155, Width=75, Height=25, Caption='Find', Anchor=8
    Add Object findnextbtn As CommandButton With Top=110,Left=237,Width=75, Height=25, Caption='Find next', Anchor=8

    Procedure Init(oListPage)
        This.oListPage=oListPage
        If i_VisualTheme<>-1
            If i_bGradientBck
                With This.ImgBackGround
                    .Width = This.Width
                    .Height = This.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            This.BackColor = i_ThemesManager.GetProp(7)
        Else
            This.BackColor = Rgb(252,252,254)
        Endif
    && Procedure show_()
        && If This.Visible And Upper(This.Parent.Controls[this.parent.controlcount].Class) == Upper(This.Class)
            && This.Hide()
            && This.Parent.oGraph.ZOrder(0)
        && Else
            && This.Show()
            && This.ZOrder(0)
        && Endif
    && Procedure stdSize()
        && This.Top=This.Parent.oGraph.Top
        && This.Left =This.Parent.oGraph.Left
        && This.Width=This.Parent.oGraph.Width
        && This.Height=This.Parent.oGraph.Height
    Procedure findbtn.Click()
        This.Parent.Find(0,0)
    Procedure findnextbtn.Click()
        Local j,k
        j=This.Parent.oListPage.LTables.ListIndex
        k=This.Parent.oListPage.LFields.ListIndex
        This.Parent.Find(j,k)
    Procedure Find(starttbl,startfld)
        Local r
        Do Case
            Case Empty(This.txtfd.Text) And Empty(This.txtfn.Text)
                r=This.findTable(starttbl)
                If r=0
                    cp_ErrorMsg("Table not found.",64,'')
                Endif
            Case Empty(This.txttd.Text) And Empty(This.txttn.Text)
                r=This.findField(startfld)
                If r=0
                    cp_ErrorMsg("Field not found.",64,'')
                Endif
            Otherwise
                r=This.findTable(starttbl)
                If r<>0
                    r=This.findField(startfld)
                    If r=0
                        cp_ErrorMsg("Field not found.",64,'')
                    Endif
                Else
                    cp_ErrorMsg("Table not found.",64,'')
                Endif
        Endcase
    Func findTable(starttbl)
        Local i,j,tn,td, k
        j=0
        k=0
        tn=Trim(Lower(This.txttn.Text))
        td=Trim(Lower(This.txttd.Text))
        For i=starttbl+1 To This.oListPage.LTables.ListCount
            If j=0
                If Lower(This.oListPage.LTables.List(i,1))=td And Lower(This.oListPage.LTables.List(i,2))=tn
                    j=i
                Endif
            Endif
        Next
        If j<>0
            *--- Ricerco il corrispondente nell'altra lista
            For k=1 To This.oListPage.LTbls.ListCount
                If Lower(This.oListPage.LTbls.List(k,2)) == Lower(This.oListPage.LTables.List(j,2))
                    Exit
                Endif
            Next
            This.oListPage.LTables.Selected(j)=True
            This.oListPage.LTbls.Selected(k)=True
            * --- disbilita la vecchia selezione dei campi
            i=This.oListPage.LFields.ListIndex
            If i<>0
                This.oListPage.LFields.Selected(i)=False
            Endif
            *
            This.oListPage.LTables.Click()
        Endif
        Return j
    Func findField(startfld)
        Local i,j,fn,fd
        j=0
        fd=Lower(Trim(This.txtfd.Text))
        fn=Lower(Trim(This.txtfn.Text))
        For i=startfld+1 To This.oListPage.LFields.ListCount
            If j=0
                If Lower(This.oListPage.LFields.List(i,1))=fd And Lower(This.oListPage.LFields.List(i,2))=fn
                    j=i
                Endif
            Endif
        Next
        If j<>0
            This.oListPage.LFields.Selected(j)=True
            This.oListPage.LFields.Click()
        Endif
        Return j
Enddefine

Define Class query_pivot As CpSysContainer
    Closable=.F.
    Top=1
    Left = 1
    Height = 270
    Width = 380
	MinHeight=200
    Caption='Pivot'
    BackStyle=1
    Icon=i_cBmpPath+i_cStdIcon
    cOldicon=''
    oPage=.Null.
    Add Object lblpivotflds As Label With Top=2,Left=10,Caption='Pivot fields', BackStyle = 0
    Add Object pivotflds As ListBox With Top=22,Left=5,Width=295,Height=100,ColumnWidths="100,100,100", ColumnCount=3,MoverBars=.T., Visible=.T., Anchor=15
    Add Object lblobsflds As Label With Top=130,Left=10,Caption='Data fields', BackStyle = 0, Anchor=4
    Add Object dataflds As ListBox With Top=150, Left=5,Width=295,Height=80,ColumnWidths="100,100,100", ColumnCount=3,MoverBars=.T., Visible=.T., Anchor=14
    Add Object delpivot As CommandButton With Top=150,Left=305,Width=75,Height=25,Caption='Delete Pivot', Anchor=12
    Add Object deldata As CommandButton With Top=180,Left=305,Width=75,Height=25,Caption='Delete Data', Anchor=12
    Add Object pivottype As OptionGroup With Top=22,Left=305,Width=75,Height=100,ButtonCount=5 , BackStyle = 0, Anchor=9 &&height=120,buttoncount=6
    Add Object lbllist As Label With Caption='List/Query:',Top=240,Left=10,Width=60, BackStyle = 0, Anchor=6
    Add Object listqry As TextBox With Top=238,Width=230,Left=70,Value=Space(200), Anchor=14
    #If Version(5)>=900
        Add Object ImgBackGround As Image With Stretch=2, Visible=.F.
    #Endif

    Proc pivottype.Init()
        This.Buttons(1).Left=5
        This.Buttons(1).Width=70
        This.Buttons(1).Caption='Value'
        This.Buttons(1).BackStyle = 0
        This.Buttons(2).Left=5
        This.Buttons(2).Width=70
        This.Buttons(2).Caption='Index'
        This.Buttons(2).BackStyle = 0
        This.Buttons(3).Left=5
        This.Buttons(3).Width=70
        This.Buttons(3).Caption='Month'
        This.Buttons(3).BackStyle = 0
        This.Buttons(4).Left=5
        This.Buttons(4).Width=70
        This.Buttons(4).Caption='Day'
        This.Buttons(4).BackStyle = 0
        This.Buttons(5).Left=5
        This.Buttons(5).Width=70
        This.Buttons(5).Caption='List'
        This.Buttons(5).BackStyle = 0
        *this.Buttons(6).left=5
        *this.Buttons(6).width=70
        *this.Buttons(6).caption='Query'
    Proc delpivot.Click()
        Local i
        For i=1 To This.Parent.pivotflds.ListCount
            If This.Parent.pivotflds.Selected(i)
                This.Parent.pivotflds.Selected(i)=.F.
                This.Parent.pivotflds.RemoveItem(i)
                i=i-1
                This.Parent.oPage.oQC.bModified=.T.
            Endif
        Next
    Proc deldata.Click()
        Local i
        For i=1 To This.Parent.dataflds.ListCount
            If This.Parent.dataflds.Selected(i)
                This.Parent.dataflds.Selected(i)=.F.
                This.Parent.dataflds.RemoveItem(i)
                i=i-1
                This.Parent.oPage.oQC.bModified=.T.
            Endif
        Next
    Proc pivotflds.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='fdlfields'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldicon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldicon
            Endcase
        Endif
    Proc pivotflds.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local i
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='fdlfields'
            oSource.Drag(2)
            This.Parent.FillPivot()
        Endif
        Thisform.LockScreen=.F.
    Proc dataflds.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='fdlfields'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldicon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldicon
            Endcase
        Endif
    Proc dataflds.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local i
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='fdlfields'
            oSource.Drag(2)
            This.Parent.FillData()
        Endif
        Thisform.LockScreen=.F.
    Proc Init(oPage)
        This.oPage=oPage
        If i_VisualTheme<>-1
            If i_bGradientBck
                With This.ImgBackGround
                    .Width = This.Width
                    .Height = This.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            This.BackColor = i_ThemesManager.GetProp(7)
        Else
            This.BackColor = Rgb(252,252,254)
        Endif
    && Procedure stdSize()
        && This.Top=This.Parent.oGraph.Top
        && This.Left =This.Parent.oGraph.Left
        && This.Width=This.Parent.oGraph.Width
        && This.Height=This.Parent.oGraph.Height
    Proc show_()
        && If This.Visible And Upper(This.Parent.Controls[this.parent.controlcount].Class) == Upper(This.Class)
            && This.Parent.oGraph.ZOrder(0)
            && This.pivotflds.Visible=.F.
            && This.dataflds.Visible=.F.
            && This.Hide()
        && Else
            && This.stdSize()
            && This.ZOrder(0)
            && This.pivotflds.Visible=.T.
            && This.dataflds.Visible=.T.
            && This.Show()
            && Thisform.Refresh()
        && Endif
    Func IsUsedFieldInPivot(cAlias)
        Local i
        For i=1 To This.pivotflds.ListCount
            If This.pivotflds.List(i,3)==cAlias
                Return(.T.)
            Endif
        Next
        Return(.F.)
    Endfunc
    Proc FillPivot()
        Local oFldList
        oFldList=This.oPage.oPage.page2.opag.fdlfields
        For i=1 To oFldList.ListCount
            If oFldList.Selected(i) And Not(This.IsUsedFieldInPivot(oFldList.List(i,3)))
                This.pivotflds.AddItem(oFldList.List(i,1))
                This.pivotflds.List(This.pivotflds.ListCount,2)=oFldList.List(i,2)
                This.pivotflds.List(This.pivotflds.ListCount,3)=oFldList.List(i,3)
                This.oPage.oQC.bModified=.T.
            Endif
        Next
    Func IsUsedFieldInData(cAlias)
        Local i
        For i=1 To This.pivotflds.ListCount
            If This.dataflds.List(i,3)==cAlias
                Return(.T.)
            Endif
        Next
        Return(.F.)
    Endfunc
    Proc FillData()
        Local oFldList
        oFldList=This.oPage.oPage.page2.opag.fdlfields
        For i=1 To oFldList.ListCount
            If oFldList.Selected(i) And Not(This.IsUsedFieldInData(oFldList.List(i,3)))
                This.dataflds.AddItem(oFldList.List(i,1))
                This.dataflds.List(This.dataflds.ListCount,2)=oFldList.List(i,2)
                This.dataflds.List(This.dataflds.ListCount,3)=oFldList.List(i,3)
                This.oPage.oQC.bModified=.T.
            Endif
        Next
Enddefine

Define Class query_selitems As CpSysContainer
    oQC=.Null.
    oPivot=.Null.
    Closable=.F.
    Caption = "Query"
    Icon=i_cBmpPath+i_cStdIcon
    ShowTips = .T.
	cCursor = ""
    Proc Init(oQC)
        This.oQC=oQC
        This.Top = (This.Parent.Height/2)
        This.Width= This.Parent.Width
        This.Height= (This.Parent.Height/2)
        && This.Parent.AddObject("oPivot",'query_pivot',This)
        && This.oPivot = This.Parent.oPivot
        If i_VisualTheme<>-1 And i_cMenuTab<>"S"
            This.oPage.Tabs=.F.
            This.oPage.BorderWidth=0
            This.AddObject("oTabMenu", "TabMenu", This, .F., .F., "oPage")
            This.oPage.Height = This.oPage.Height - This.oTabMenu.Height
            This.BackColor = i_ThemesManager.GetProp(7)
        Endif
        This.Resize()
    Endproc
    Procedure Release()
        This.oPage.page2.opag.fdlfields.olist.oParentObject=.Null.
        This.oPage.page3.opag.rllrelation.olist.oParentObject=.Null.
        This.oPage.page4.opag.fllfilter.olist.oParentObject=.Null.
        This.oPage.page5.opag.fplparam.olist.oParentObject=.Null.
        DoDefault()
    Endproc
    Add Object oPage As PageFrame With PageCount=6, Width=This.Width, Height=This.Height-5, TabStyle=1,;
        Page1.BackStyle=0,page2.BackStyle=0,page3.BackStyle=0,page4.BackStyle=0,page5.BackStyle=0,Page6.BackStyle=0
    Add Object __dummy__ As CommandButton With Enabled=.F.,Width=0,Height=0,Style=0,SpecialEffect=1
    Proc oPage.Init()
        With This
            .Pages(1).Caption="Tables"
            .Pages(2).Caption="Fields, Group By and Order By"
            .Pages(3).Caption="Join"
            .Pages(4).Caption="Filter"
            .Pages(5).Caption="Filter Parameters"
            .Pages(6).Caption="Note"
            .Width=This.Parent.Width-10
            .Height=(This.Parent.Height/2)-60
            .Pages(1).AddObject("oPag",'oTbContainer')
            .Pages(1).opag.Visible=.T.
            .Pages(2).AddObject("oPag",'oFdContainer')
            .Pages(2).opag.Visible=.T.
            .Pages(3).AddObject("oPag",'oRlContainer')
            .Pages(3).opag.Visible=.T.
            .Pages(4).AddObject("oPag",'oFlContainer')
            .Pages(4).opag.Visible=.T.
            .Pages(5).AddObject("oPag",'oFpContainer')
            .Pages(5).opag.Visible=.T.
            .Pages(6).AddObject("txtnote","vqnoteeditbox")
            With .Pages(6).txtnote
                .Visible=.T.
                .Top=0
                .Left=0
                .Width=.Parent.Parent.Width
                .Height=.Parent.Parent.Height-30
            Endwith
        Endwith
    Endproc
    Proc Resize()
        Local dx, dy
        * --- Modifico la lunghezza
        This.oPage.Height = This.Height
        This.oPage.Width = This.Width
        This.oPage.Page1.opag.Height = This.oPage.PageHeight
        This.oPage.Page1.opag.Width = This.oPage.PageWidth
        This.oPage.page2.opag.Height = This.oPage.PageHeight
        This.oPage.page2.opag.Width = This.oPage.PageWidth
        This.oPage.page3.opag.Height = This.oPage.PageHeight
        This.oPage.page3.opag.Width = This.oPage.PageWidth
        This.oPage.page4.opag.Height = This.oPage.PageHeight
        This.oPage.page4.opag.Width = This.oPage.PageWidth
        This.oPage.page5.opag.Height = This.oPage.PageHeight
        This.oPage.page5.opag.Width = This.oPage.PageWidth

        dy = Iif(This.Height-60<0,0,This.Height-60)
        This.oPage.Page1.opag.TblTables.Height = dy
        This.oPage.page2.opag.fdlfields.Height = dy
        This.oPage.page3.opag.rllrelation.Height = dy
        This.oPage.page4.opag.fllfilter.Height = dy
        This.oPage.page5.opag.fplparam.Height = dy
        dy = Iif((This.Height - 80)/2<0,0,(This.Height - 80)/2)
        *--- Pag2
        This.oPage.page2.opag.FdLOrder.Height = dy
        This.oPage.page2.opag.FdLGroups.Top = 40+dy
        This.oPage.page2.opag.FdLGroups.Height = dy
        This.oPage.page2.opag.Txt1Gr.Top = 25+dy
        This.oPage.page2.opag.FdLOrder.Width = 350
        This.oPage.page2.opag.FdLGroups.Width = 350

        Local l_delta, l_deltaCnt
        This.oPage.page2.opag.fdlfields.Width = Max(This.oPage.PageWidth - 455, MIN_WIDTH)
        l_delta = This.oPage.page2.opag.fdlfields.Width + This.oPage.page2.opag.fdlfields.Left
        This.oPage.page2.opag.FLCnt.Width = This.oPage.page2.opag.fdlfields.Width-35
        This.oPage.page2.opag.FLCnt.IFName.Width = Max(This.oPage.page2.opag.fdlfields.Width-388,MIN_WIDTH)
        This.oPage.page2.opag.FLCnt.IFName.ExprText.Width = This.oPage.page2.opag.FLCnt.IFName.Width
        l_deltaCnt = This.oPage.page2.opag.FLCnt.IFName.Width + This.oPage.page2.opag.FLCnt.IFName.Left
        This.oPage.page2.opag.FLCnt.IFAlias.Left = l_deltaCnt + 2
        This.oPage.page2.opag.FLCnt.IFType.Left = l_deltaCnt + 85
        This.oPage.page2.opag.FLCnt.IFLen.Left = l_deltaCnt + 158
        This.oPage.page2.opag.FLCnt.IFDec.Left = l_deltaCnt + 190
        This.oPage.page2.opag.Txt3FD.Left = l_delta - 235
        This.oPage.page2.opag.Txt4FD.Left = l_delta - 150
        This.oPage.page2.opag.Txt5FD.Left = l_delta - 78
        This.oPage.page2.opag.Txt6FD.Left = l_delta - 45
        This.oPage.page2.opag.FdDis.Left = l_delta + 25
        This.oPage.page2.opag.BtnDelFld.Left = l_delta + 10
        This.oPage.page2.opag.BtnDelOrd.Left = l_delta + 10
        This.oPage.page2.opag.BtnDelGrp.Left = l_delta + 10
        && This.oPage.page2.opag.BtnPivot.Left = l_delta + 10
		This.oPage.page2.opag.BtnSetType.Left = l_delta + 10
        This.oPage.page2.opag.Txt1Or.Left = l_delta + 95
        This.oPage.page2.opag.FdLOrder.Left = l_delta + 95
        This.oPage.page2.opag.Txt1Gr.Left = l_delta + 95
        This.oPage.page2.opag.FdLGroups.Left = l_delta + 95
        This.oPage.page2.opag.fdlfields.ColumnWidths = "130,"+Alltrim(Str(This.oPage.page2.opag.fdlfields.Width-390))+",80,70,30,30"
        *--- Pag3
        This.oPage.page3.opag.rllrelation.Width = Max(This.oPage.PageWidth - 87, MIN_WIDTH)
        l_delta = This.oPage.page3.opag.rllrelation.Width + This.oPage.page3.opag.rllrelation.Left
        This.oPage.page3.opag.Txt3FD.Left = l_delta - 273
        This.oPage.page3.opag.Txt4FD.Left = l_delta - 168
        This.oPage.page3.opag.Txt5FD.Left = l_delta - 98
        This.oPage.page3.opag.BtnAddRel.Left = l_delta + 7
        This.oPage.page3.opag.BtnDelRel.Left = l_delta + 7
        This.oPage.page3.opag.rllrelation.ColumnWidths = "135,"+Alltrim(Str(This.oPage.page3.opag.rllrelation.Width-433))+",100,70,70"
        This.oPage.page3.opag.RLCnt.Width = This.oPage.page3.opag.rllrelation.Width - 39
        This.oPage.page3.opag.RLCnt.IRExpr.Width = Max(This.oPage.page3.opag.rllrelation.Width-433,MIN_WIDTH)
        This.oPage.page3.opag.RLCnt.IRExpr.ExprText.Width = This.oPage.page3.opag.RLCnt.IRExpr.Width
        l_deltaCnt = This.oPage.page3.opag.RLCnt.IRExpr.Width + This.oPage.page3.opag.RLCnt.IRExpr.Left
        This.oPage.page3.opag.RLCnt.IRType.Left = l_deltaCnt + 2
        This.oPage.page3.opag.RLCnt.IRAli1.Left = l_deltaCnt + 106
        This.oPage.page3.opag.RLCnt.IRAli2.Left = l_deltaCnt + 178
        *--- Pag4
        This.oPage.page4.opag.fllfilter.Width = Max(This.oPage.PageWidth - 100, MIN_WIDTH)
        l_delta = This.oPage.page4.opag.fllfilter.Width + This.oPage.page4.opag.fllfilter.Left
        l_width = Max(1,Floor((This.oPage.page4.opag.fllfilter.Width-266)/2))
        l_width1 = Max(1, Ceiling((This.oPage.page4.opag.fllfilter.Width-266)/2))
        This.oPage.page4.opag.Txt2Fil.Left = l_width + 40
        This.oPage.page4.opag.Txt3Fil.Left = l_width + 72
        This.oPage.page4.opag.Txt4Fil.Left = l_width + 145
        This.oPage.page4.opag.Txt5Fil.Left = l_delta - 114
        This.oPage.page4.opag.BtnDelFil.Left = l_delta + 10
        This.oPage.page4.opag.BtnAddFil.Left = l_delta + 10
        This.oPage.page4.opag.BtnOpenFil.Left = l_delta + 10
        This.oPage.page4.opag.fllfilter.ColumnWidths = Alltrim(Str(l_width))+",40,70,"+Alltrim(Str(l_width1))+",46,50"
        This.oPage.page4.opag.FiLCnt.Width = This.oPage.page4.opag.fllfilter.Width - 39
        l_width = l_width + 2
        This.oPage.page4.opag.FiLCnt.IFField.Width = l_width
        This.oPage.page4.opag.FiLCnt.IFField.ExprText.Width = l_width
        This.oPage.page4.opag.FiLCnt.IFExamp.Width = l_width + 2
        This.oPage.page4.opag.FiLCnt.IFExamp.ExprText.Width = l_width + 2 - Iif(This.oPage.page4.opag.FiLCnt.GetQryBtn.Visible, 19,0)
        l_deltaCnt = This.oPage.page4.opag.FiLCnt.IFField.Width + This.oPage.page4.opag.FiLCnt.IFField.Left
        This.oPage.page4.opag.FiLCnt.IFExamp.Left = l_deltaCnt + 115
        This.oPage.page4.opag.FiLCnt.IFNot.Left = l_deltaCnt + 3
        This.oPage.page4.opag.FiLCnt.IFCrite.Left = l_deltaCnt + 42
        This.oPage.page4.opag.FiLCnt.GetQryBtn.Left = This.oPage.page4.opag.FiLCnt.IFExamp.ExprText.Width + This.oPage.page4.opag.FiLCnt.IFExamp.Left + This.oPage.page4.opag.FiLCnt.IFExamp.ExprText.Left
        This.oPage.page4.opag.FiLCnt.IFLOp.Left = This.oPage.page4.opag.fllfilter.Width - 145
        This.oPage.page4.opag.FiLCnt.IFLHaving.Left = This.oPage.page4.opag.FiLCnt.IFLOp.Left + This.oPage.page4.opag.FiLCnt.IFLOp.Width + 2
        *dx = IIF((this.Width - 580)/3<0,0,(this.Width - 580)/3)
        *this.oPage.Page2.oPag.FdLOrder.columnwidths = ALLTRIM(STR(dx))+','+ALLTRIM(STR(dx))+','+ALLTRIM(STR(dx))
        *this.oPage.Page2.oPag.FdLGroups.columnwidths = ALLTRIM(STR(dx))+','+ALLTRIM(STR(dx*2))
        This.oPage.Height = Iif(This.Height<35,35,This.Height)
        This.oPage.Page1.opag.Height = Iif(This.Height<35,35,This.Height)
        This.oPage.page2.opag.Height = Iif(This.Height<35,35,This.Height)
        This.oPage.page3.opag.Height = Iif(This.Height<35,35,This.Height)
        This.oPage.page4.opag.Height = Iif(This.Height<35,35,This.Height)
        This.oPage.page5.opag.Height = Iif(This.Height<35,35,This.Height)
        This.oPage.Page6.txtnote.Width=This.oPage.PageWidth
        This.oPage.Page6.txtnote.Height=This.oPage.PageHeight

    Proc AddTable(cName,cAlias,cDescr,cMode)
        If Type('cMode')='L'
            cMode=''
        Endif
        This.oPage.Page1.opag.TblTables.AddItem(cDescr)
        This.oPage.Page1.opag.TblTables.List(This.oPage.Page1.opag.TblTables.ListCount,2)=cAlias
        This.oPage.Page1.opag.TblTables.List(This.oPage.Page1.opag.TblTables.ListCount,3)=cName
        This.oPage.Page1.opag.TblTables.List(This.oPage.Page1.opag.TblTables.ListCount,4)=cMode
    Proc AddField(cName,cDescr,cAlias,cType,nLen,nDec)
        This.oPage.page2.opag.fdlfields.AddItem(cDescr)
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,2)=cAlias+'.'+cName
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,3)=This.GetFieldAlias(cName)
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,4)=This.CharToType(cType)
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,5)=Str(nLen,3,0)
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,6)=Str(nDec,2,0)
    Proc FullAddField(cExp,cDescr,cAlias,cType,nLen,nDec)
        This.oPage.page2.opag.fdlfields.AddItem(cDescr)
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,2)=cExp
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,3)=cAlias
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,4)=This.CharToType(cType)
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,5)=Str(nLen,3,0)
        This.oPage.page2.opag.fdlfields.List(This.oPage.page2.opag.fdlfields.ListCount,6)=Str(nDec,2,0)
    Proc Addrelation(cDescr,cExpr,cType,cAlias1,cAlias2)
        This.oPage.page3.opag.rllrelation.AddItem(cDescr)
        This.oPage.page3.opag.rllrelation.List(This.oPage.page3.opag.rllrelation.ListCount,2)=cExpr
        This.oPage.page3.opag.rllrelation.List(This.oPage.page3.opag.rllrelation.ListCount,3)=cType
        This.oPage.page3.opag.rllrelation.List(This.oPage.page3.opag.rllrelation.ListCount,4)=cAlias1
        This.oPage.page3.opag.rllrelation.List(This.oPage.page3.opag.rllrelation.ListCount,5)=cAlias2
    Proc AddGroupBy(cDescr,cFld)
        This.oPage.page2.opag.FdLGroups.AddItem(cDescr)
        This.oPage.page2.opag.FdLGroups.List(This.oPage.page2.opag.FdLGroups.ListCount,2)=cFld
    Proc AddOrderBy(cDescr,cFld,Casc)
        This.oPage.page2.opag.FdLOrder.AddItem(cDescr)
        This.oPage.page2.opag.FdLOrder.List(This.oPage.page2.opag.FdLOrder.ListCount,2)=cFld
        This.oPage.page2.opag.FdLOrder.List(This.oPage.page2.opag.FdLOrder.ListCount,3)=Casc
    Proc AddWhere(cFld,cNot,cOp,cConst,cLOp,cHaving)
        This.oPage.page4.opag.fllfilter.AddItem('')
        This.oPage.page4.opag.fllfilter.List(This.oPage.page4.opag.fllfilter.ListCount,1)=cFld
        This.oPage.page4.opag.fllfilter.List(This.oPage.page4.opag.fllfilter.ListCount,2)=cNot
        This.oPage.page4.opag.fllfilter.List(This.oPage.page4.opag.fllfilter.ListCount,3)=cOp
        This.oPage.page4.opag.fllfilter.List(This.oPage.page4.opag.fllfilter.ListCount,4)=cConst
        This.oPage.page4.opag.fllfilter.List(This.oPage.page4.opag.fllfilter.ListCount,5)=cLOp
        This.oPage.page4.opag.fllfilter.List(This.oPage.page4.opag.fllfilter.ListCount,6)=cHaving
    Proc AddFilPar(cFld,cDes,cType,cLen,cDec,cRemove)
        This.oPage.page5.opag.fplparam.AddItem(cFld)
        This.oPage.page5.opag.fplparam.List(This.oPage.page5.opag.fplparam.ListCount,2)=cDes
        This.oPage.page5.opag.fplparam.List(This.oPage.page5.opag.fplparam.ListCount,3)=This.CharToType(cType)
        This.oPage.page5.opag.fplparam.List(This.oPage.page5.opag.fplparam.ListCount,4)=cLen
        This.oPage.page5.opag.fplparam.List(This.oPage.page5.opag.fplparam.ListCount,5)=cDec
        This.oPage.page5.opag.fplparam.List(This.oPage.page5.opag.fplparam.ListCount,6)=cRemove
    Proc RemoveTable(cAlias)
        Local i
        For i=1 To This.oPage.Page1.opag.TblTables.ListCount
            If This.oPage.Page1.opag.TblTables.List(i,2)==cAlias
                This.oPage.Page1.opag.TblTables.RemoveItem(i)
                This.RemoveFields(cAlias)
                This.RemoveRelation(cAlias,'')
                This.RemoveFilters(cAlias)
                Return
            Endif
        Next
    Proc RemoveFields(cAlias)
        Local i,i_cAlias,i_cName
        i=1
        Do While i<=This.oPage.page2.opag.fdlfields.ListCount
            *if substr(this.oPage.Page2.oPag.FdLFields.List(i,2),1,at('.',this.oPage.Page2.oPag.FdLFields.List(i,2))-1)==cAlias
            If cAlias+'.'$This.oPage.page2.opag.fdlfields.List(i,2)
                i_cName=This.oPage.page2.opag.fdlfields.List(i,2)
                i_cAlias=This.oPage.page2.opag.fdlfields.List(i,3)
                This.RemoveGroupBy(Iif(At('.'+i_cAlias+'|',i_cName+'|')<>0,i_cName,i_cAlias))
                This.RemoveOrderBy(Iif(At('.'+i_cAlias+'|',i_cName+'|')<>0,i_cName,i_cAlias))
                This.RemovePivot(i_cAlias)
                This.oPage.page2.opag.fdlfields.RemoveItem(i)
                i=i-1
            Endif
            i=i+1
        Enddo
    Proc RemoveGroupBy(cAlias)
        Local i
        i=1
        Do While i<=This.oPage.page2.opag.FdLGroups.ListCount
            If This.oPage.page2.opag.FdLGroups.List(i,2)==cAlias
                This.oPage.page2.opag.FdLGroups.RemoveItem(i)
                i=i-1
            Endif
            i=i+1
        Enddo
    Proc RemoveOrderBy(cAlias)
        Local i
        i=1
        Do While i<=This.oPage.page2.opag.FdLOrder.ListCount
            If This.oPage.page2.opag.FdLOrder.List(i,2)==cAlias
                This.oPage.page2.opag.FdLOrder.RemoveItem(i)
                i=i-1
            Endif
            i=i+1
        Enddo
    Proc RemovePivot(cAlias)
        Local i
        For i=1 To This.oPivot.pivotflds.ListCount
            If This.oPivot.pivotflds.List(i,3)==cAlias
                This.oPivot.pivotflds.RemoveItem(i)
                i=i-1
            Endif
        Next
        For i=1 To This.oPivot.dataflds.ListCount
            If This.oPivot.dataflds.List(i,3)==cAlias
                This.oPivot.dataflds.RemoveItem(i)
                i=i-1
            Endif
        Next
    Proc RemoveRelation(cAlias1,cAlias2)
        Local i,i_a1,i_a2
        i=1
        Do While i<=This.oPage.page3.opag.rllrelation.ListCount
            i_a1=This.oPage.page3.opag.rllrelation.List(i,4)
            i_a2=This.oPage.page3.opag.rllrelation.List(i,5)
            If (!Empty(cAlias2) And ((i_a1==cAlias1 And i_a2==cAlias2) Or (i_a1==cAlias2 And i_a2==cAlias1)));
                    or (Empty(cAlias2) And (i_a1==cAlias1 Or i_a2==cAlias1))
                This.oPage.page3.opag.rllrelation.RemoveItem(i)
                i=i-1
            Endif
            i=i+1
        Enddo
    Proc RemoveFilters(cAlias)
        Local i,i_a1
        i=1
        Do While i<=This.oPage.page4.opag.fllfilter.ListCount
            i_a1=This.oPage.page4.opag.fllfilter.List(i,1)
            If At(cAlias+'.',i_a1)=1
                This.oPage.page4.opag.fllfilter.RemoveItem(i)
                i=i-1
            Endif
            i=i+1
        Enddo
    Func GetFieldAlias(cName)
        Local i,j,cNewName
        cNewName=cName
        j=0
        For i=1 To This.oPage.page2.opag.fdlfields.ListCount
            If This.oPage.page2.opag.fdlfields.List(i,3)==cNewName
                j=j+1
                cNewName=cName+Alltrim(Str(j))
                i=1
            Endif
        Next
        Return(cNewName)
    Func IsUsedField(cName,cAlias)
        Return(This.oPage.page2.opag.IsUsedField(cName,cAlias))
    Func ExistRelation(cAlias1,cAlias2)
        Return(This.oPage.page3.opag.ExistRelation(cAlias1,cAlias2))
    Proc RemoveAll()
        This.oPage.Page1.opag.TblTables.Clear
        This.oPage.page2.opag.fdlfields.Clear
        This.oPage.page2.opag.FdLGroups.Clear
        This.oPage.page2.opag.FdLOrder.Clear
        This.oPage.page3.opag.rllrelation.Clear
        This.oPage.page4.opag.fllfilter.Clear
        This.oPage.page5.opag.fplparam.Clear
        This.oPage.page5.opag.MaskFp.Value=''
        This.oPage.page5.opag.RemoveFp.Value=.F.
        This.oPage.page5.opag.OrderByParm.Value=.T.
        This.oPage.Page6.txtnote.Value=''
        This.oQC.oDic.LTables.Click()
        This.SetUnion('')
        This.SetUnionAll(.F.)
        This.SetMultiCompany(.F.)
        This.SetRoles('')
        This.oPivot.pivotflds.Clear
        This.oPivot.dataflds.Clear
        This.oPivot.pivottype.Value=1
        This.oPivot.listqry.Value=''
		USE IN SELECT(this.cCursor)
    Proc SetNote(cText)
        This.oPage.Page6.txtnote.Value=cText
    Func GetNote()
        Return(This.oPage.Page6.txtnote.Value)
    Proc SetMask(cName)
        This.oPage.page5.opag.MaskFp.Value=cName
    Proc SetUnion(cName)
        This.oPage.Page1.opag.TxtUnion.Value=cName
    Proc GetUnion()
        Return(Trim(This.oPage.Page1.opag.TxtUnion.Value))
    Proc SetUnionAll(bAll)
        This.oPage.Page1.opag.chkUnionAll.Value=Iif(bAll,1,0)
    Proc GetUnionAll()
        Return(This.oPage.Page1.opag.chkUnionAll.Value=1)
    Proc SetMultiCompany(bMultiCompany)
        This.oPage.Page1.opag.chkMultiCompany.Value=Iif(bMultiCompany,1,0)
    Proc GetMultiCompany()
        Return(This.oPage.Page1.opag.chkMultiCompany.Value=1)
    Proc GetTestCompanyList()
        Return(Trim(This.oPage.Page1.opag.TestCompanyList.Value))
    Proc SetRoles(cRoles)
        This.oPage.Page1.opag.TxtRoles.Value=cRoles
    Proc GetRoles(cRoles)
        Return This.oPage.Page1.opag.TxtRoles.Value
    Proc SetRemove(bValue)
        This.oPage.page5.opag.RemoveFp.Value=bValue
    Proc SetOrderByParm(bValue)
        This.oPage.page5.opag.OrderByParm.Value=bValue
    Func ExecSelect(i_oper)
        Local i,i_cNome,i_cNomeL,i_cNomeF,i_cExpr,i_cDescr,i_nLen,i_nDec,i_cRemove
        Local i_cNome1,i_cNome2,i_cNome3,i_cType,oQ,T,i_op,i_bSubQuery
        Local i_mode
        oQ=Createobject('CPQuery')
        For i=1 To This.oPage.Page1.opag.TblTables.ListCount
            i_cNome=This.oPage.Page1.opag.TblTables.List(i,1)
            i_cNomeL=This.oPage.Page1.opag.TblTables.List(i,2)
            i_cNomeF=This.oPage.Page1.opag.TblTables.List(i,3)
            i_mode=This.oPage.Page1.opag.TblTables.List(i,4)
            If Lower(Right(i_cNome,4))='.vqr'
                Do Case
                    Case i_mode='Temp. Table'
                    Case i_mode='Routine'
                        i_cNomeF=i_cNomeF+'+'
                    Otherwise
                        i_cNomeF=i_cNomeF+'*'
                Endcase
            Endif
            oQ.mLoadFile(i_cNome,i_cNomeL,i_cNomeF,0)
        Next
        For i=1 To This.oPage.page2.opag.fdlfields.ListCount
            i_cExpr=This.oPage.page2.opag.fdlfields.List(i,2)
            i_cNome=This.oPage.page2.opag.fdlfields.List(i,3)
            i_cType=This.oPage.page2.opag.fdlfields.List(i,4)
            i_nLen=Val(This.oPage.page2.opag.fdlfields.List(i,5))
            i_nDec=Val(This.oPage.page2.opag.fdlfields.List(i,5))
            oQ.mLoadField(i_cExpr,i_cNome,i_cType,i_nLen,i_nDec)
        Next
        For i=1 To This.oPage.page2.opag.FdLOrder.ListCount
            i_cNome=This.oPage.page2.opag.FdLOrder.List(i,2)
            i_cExpr=This.oPage.page2.opag.FdLOrder.List(i,3)
            oQ.mLoadOrder(i_cNome,i_cExpr)
        Next
        For i=1 To This.oPage.page2.opag.FdLGroups.ListCount
            i_cExpr=This.oPage.page2.opag.FdLGroups.List(i,2)
            oQ.mLoadGroupBy(i_cExpr)
        Next
        If This.oPage.page2.opag.FdDis.Value=1
            oQ.cDistinct="distinct"
        Endif
        For i=1 To This.oPage.page3.opag.rllrelation.ListCount
            i_cNome1=''
            i_cNome2=This.oPage.page3.opag.rllrelation.List(i,4)
            i_cNome3=This.oPage.page3.opag.rllrelation.List(i,5)
            i_cExpr=This.oPage.page3.opag.rllrelation.List(i,2)
            i_cType=This.oPage.page3.opag.rllrelation.List(i,3)
            oQ.mLoadJoin(i_cNome1,i_cNome2,i_cNome3,i_cExpr,i_cType)
        Next
        For i=1 To This.oPage.page4.opag.fllfilter.ListCount
            i_cExpr=''
            i_op=Trim(This.oPage.page4.opag.fllfilter.List(i,3))
            i_bSubQuery=Inlist(i_op,'in','not in','exists','not exists') Or 'any'$i_op Or 'all'$i_op
            If i_bSubQuery And Inlist(i_op,'in','not in') And Left(This.oPage.page4.opag.fllfilter.List(i,4),1)='('
                i_bSubQuery=.F.
            Endif
            If Len(Trim(This.oPage.page4.opag.fllfilter.List(i,2)))<>0
                i_cExpr=This.oPage.page4.opag.fllfilter.List(i,2)+'('
            Endif
            i_cExpr=i_cExpr+This.oPage.page4.opag.fllfilter.List(i,1)
            Local i_cOp,i_cConst
            i_cOp=This.oPage.page4.opag.fllfilter.List(i,3)
            i_cExpr=i_cExpr+' '+i_cOp+' '
            i_cConst=This.oPage.page4.opag.fllfilter.List(i,4)
            If i_cOp='like'
                i_cConst='('+i_cConst+')'
            Endif
            i_cExpr=i_cExpr+Iif(i_bSubQuery,'!','')+i_cConst
            If Len(Trim(This.oPage.page4.opag.fllfilter.List(i,2)))<>0
                i_cExpr=i_cExpr+')'
            Endif
            i_cExpr=i_cExpr+' '+This.oPage.page4.opag.fllfilter.List(i,5)+' '
            T=This.oPage.page4.opag.fllfilter.List(i,4)
            If This.oPage.page4.opag.fllfilter.List(i,6)='Having'
                oQ.mLoadHaving(i_cExpr,Iif(Left(T,1)='?',T,Iif(i_bSubQuery,'!'+T,.F.)),i_bSubQuery)
            Else
                oQ.mLoadWhere(i_cExpr,Iif(Left(T,1)='?',T,Iif(i_bSubQuery,'!'+T,.F.)),i_bSubQuery)
            Endif
        Next
        For i=1 To This.oPage.page5.opag.fplparam.ListCount
            i_cNome=This.oPage.page5.opag.fplparam.List(i,1)
            i_cDescr=This.oPage.page5.opag.fplparam.List(i,2)
            i_cType=This.oPage.page5.opag.fplparam.List(i,3)
            i_nLen=Val(This.oPage.page5.opag.fplparam.List(i,4))
            i_nDec=Val(This.oPage.page5.opag.fplparam.List(i,5))
            i_cRemove=This.oPage.page5.opag.fplparam.List(i,6)
            oQ.mLoadFilterParam(i_cNome,i_cDescr,i_cType,i_nLen,i_nDec,i_cRemove)
        Next
        For i=1 To This.oPivot.pivotflds.ListCount
            oQ.mLoadPivotFlds(This.oPivot.pivotflds.List(i,3))
        Next
        For i=1 To This.oPivot.dataflds.ListCount
            oQ.mLoadPivotData(This.oPivot.dataflds.List(i,3))
        Next
        oQ.nPivotType=This.oPivot.pivottype.Value
        oQ.cPivotListQry=This.oPivot.listqry.Value
        oQ.mLoadExtMask(Trim(This.oPage.page5.opag.MaskFp.Value))
        oQ.bRemoveWhereOnEmptyParam=This.oPage.page5.opag.RemoveFp.Value
        oQ.bOrderByParm=This.oPage.page5.opag.OrderByParm.Value
        oQ.cUnion=This.GetUnion()
        oQ.bUnionAll=This.GetUnionAll()
        oQ.bMultiCompany=This.GetMultiCompany()
        oQ.cRoles=This.GetRoles()
        oQ.CreateSubQueryTables()
        oQ.mLoadCompanyList(This.GetTestCompanyList())
        If Type('i_bDisableAsyncConn')<>'U' And i_bDisableAsyncConn
            Local i_oldzoomrec
            i_oldzoomrec=Iif(Vartype(i_nZoomMaxRows)='N' And i_nZoomMaxRows>0, i_nZoomMaxRows, 250)
            i_nZoomMaxRows=Iif(Vartype(i_nVQMaxRows)='N' And i_nVQMaxRows>0, i_nVQMaxRows, 10000)
        Endif
        * --- Se al posto di this.oQC.__sqlx__ metto .f. tutte le query nel disegnatore diventano sincrone (restituiscono tutti i record)
		Private i_cError
		i_cError = ""
		&& If i_oper="Exec"
			&& this.opivot.parent.parent.ActivePage = 5
			&& this.opivot.parent.parent.page5.oresult.ogrd.Visible=.F.
			&& this.opivot.parent.parent.page5.oresult.oLblMsg.Visible=.T.
		&& Endif
		
        i_ok=oQ.mDoQuery(This.cCursor,Iif(i_oper=='Report','Exec',i_oper),This.oQC.__sqlx__,.F.,.F.,.F.,.F.,@i_cError)
        * ---
        If Type('i_bDisableAsyncConn')<>'U' And i_bDisableAsyncConn
            i_nZoomMaxRows=i_oldzoomrec
            If i_oper='Exec' And i_ok And Reccount(This.cCursor)>=i_oldzoomrec
                CP_MSG( CP_MSGFORMAT(MSG_ASYNC_CONNECTION_DISABLED_C_RETURNING_A_MAXIMUM_OF__ROWS,Alltrim(Str(i_nZoomMaxRows,15,0))),.F.)
            Endif
        Endif
        If i_oper='Exec' 
			If i_ok
				*--- Utilizzo il riferimento a "oPivot" per raggiungere "oResult"
				this.opivot.parent.parent.page5.oresult.ogrd.RecordSource = This.cCursor
				this.opivot.parent.parent.page5.oresult.ogrd.AutoFit()
				this.opivot.parent.parent.page5.oresult.SetGrd()
				cp_msg(MSG_QUERY_SUCCESSFULLY,.T.)
				this.opivot.parent.parent.ActivePage = 5
				this.opivot.parent.parent.page6.oMsg.oEdit.Value = MSG_QUERY_SUCCESSFULLY
				If !This.CheckFieldsType() And Cp_YesNo(MSG_CORRECT_DIFFERENT_TYPE_FIELD)
					This.CheckFieldsType(.T.)
				Endif
			Else
				this.opivot.parent.parent.page6.oMsg.oEdit.Value = i_cError
				this.opivot.parent.parent.ActivePage = 6
			EndIf
        Endif
		If i_oper='Get' And Not Empty(i_ok)
		*--- i_ok contiene la frase SQL
		*--- Utilizzo il riferimento a "oPivot" per raggiungere "oMsg"
			this.opivot.parent.parent.page6.oMsg.oEdit.Value = i_ok
			this.opivot.parent.parent.ActivePage = 6
		EndIf
        If i_oper='Report' And i_ok
            i_paramqry=oQ
        Endif
        Return(i_ok)
		
    Func CheckFieldsType(bForce)
	*--- Se bForce a True forza i tipi
        Local i, N, i_current[1], ret, j
		ret = .T.
		with This.oPage.page2.opag.fdlfields
			If .ListCount>0
				Local l_oldArea
				If Used(This.cCursor)
					l_oldArea = Select()
					Select(This.cCursor)
					N = Afields(i_current)
					For i = 1 To m.N
						j = AScan(.List, i_current[m.i,1], 1,-1, 3, 15)
						if m.j<>0
							i_cType = .List(m.j,4)
							i_nLen = Val(.List(m.j,5))
							i_nDec = Val(.List(m.j,6))
							If m.i_current[m.i,2]$'IBFY'
								i_current[m.i,2] = 'N'
							Endif
							If m.bForce								
								*--- Allineo il tipo
								.List(m.j,4) = This.CharToType(m.i_current[m.i,2])
								.List(m.j,5) = Str(m.i_current[m.i,3],3,0)
								.List(m.j,6) = Str(m.i_current[m.i,4],2,0)
							Else
								ret = m.ret And !(Left(m.i_cType,1)<>Left(m.i_current[m.i,2],1) And Not(m.i_current[m.i,2]='T' And m.i_cType='DateTime'))
							EndIf
						EndIf
					Next
					Select(m.l_oldArea)
				Else	
					Cp_ErrorMsg(MSG_FIRST_RUN_THE_QUERY)
				EndIf
			Endif
		EndWith
		Return (m.ret)
	EndFunc
	
	Func CharToType(pChar)
		Local cRet
	    Do Case 
			Case pChar='C'
				cRet = "Char"
			Case pChar='N'	
				cRet = 'Numeric'
			Case pChar='T' OR pChar=='DateTime'
				cRet = 'DateTime'
			Case pChar='D'				
				cRet = 'Date'
			Case pChar='L'
				cRet = 'Logic'
			Case pChar='M'
				cRet = 'Memo'
			Otherwise
				cRet = pChar
		EndCase
		Return cRet
	EndFunc
Enddefine

Define Class oTbContainer As Container
    BorderWidth=0
    BackStyle = 0
    cOldicon=''
    Add Object TblTables As ListBox With Width=500, Height=150, Left=5, Top=20,;
        columnwidths="200,80,80,100", ColumnCount=4, MultiSelect=1, MoverBars=.T.
    Add Object Txt1TB As Label With Left=25, Top=2, Caption="Description", BackStyle = 0
    Add Object Txt2TB As Label With Left=230, Top=2, Caption="Alias", BackStyle = 0
    Add Object Txt3TB As Label With Left=310, Top=2, Caption="Table Name", BackStyle = 0
    Add Object Txt4TB As Label With Left=410, Top=2, Caption="Mode", BackStyle = 0
    Add Object BtnDelTbl As CommandButton With Left=550, Top=40, Width=70, Height=25, Caption="Delete"
    Add Object BtnOpenTbl As CommandButton With Left=630, Top=40, Width=70, Height=25, Caption="Open", ToolTipText = "Open query"
    Add Object lblunion As Label With Left=550,Top=82,Width=40,Caption='Union:', BackStyle = 0
    Add Object TxtUnion As TextBox With Left=590,Top=80,Width=165,Margin=1
    Add Object btnunion As CommandButton With Left=755,Width=18,Height=21,Top=80,Caption='...'
    Add Object btnOpenUnion As CommandButton With Left=778,Width=70,Height=21,Top=80,Caption='Open'
    Add Object chkUnionAll As Checkbox With Left=550,Top=105,Caption='All',Value=0, BackStyle = 0
    Add Object chkMultiCompany As Checkbox With Left=550,Top=135,Caption='Multi-company',Value=0,Width=200, BackStyle = 0
    Add Object Txt5TB As Label With Left=550, Top=157, Caption="Test with companies:",Width=120, BackStyle = 0
    Add Object TestCompanyList As TextBox With Left=670,Top=155,Width=400
    Add Object btnTestCompanyList As CommandButton With Left=1072,Width=70,Height=21,Top=155,Caption='Company'
    Add Object lblRoles As Label With Left=550,Top=181, Caption="Roles:",Width=118,Alignment=1, BackStyle = 0, Visible=.F.
    Add Object TxtRoles As TextBox With Left=670,Top=180,Width=200, BackStyle = 0, Visible=.F.
    Proc TxtUnion.Valid()
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Proc btnunion.Click()
        Local F,p
        F=Getfile('vqr')
        *--- inserisce path minimo riferito al path di SET DEFAULT
        F=Sys(2014,F)
        If Lower(Right(F,4))='.vqr'
            F=Left(F,Len(F)-4)
        Endif
        This.Parent.TxtUnion.Value=F
        Return
    Procedure btnOpenUnion.Click()
        Local l_FileName
        l_FileName = Fullpath(Forceext(This.Parent.TxtUnion.Value, 'VQR'))
        If File(l_FileName)
            This.Parent.Parent.Parent.Parent.oQC.oForm.AddQuery()
            This.Parent.Parent.Parent.Parent.oQC.cFileName = l_FileName
            This.Parent.Parent.Parent.Parent.oQC.LoadDoc()
            This.Parent.Parent.Parent.Parent.oQC.SetTitle()
        Endif
    Proc BtnDelTbl.Click()
        Local i
        For i=1 To This.Parent.TblTables.ListCount
            If This.Parent.TblTables.Selected(i)
                This.Parent.Parent.Parent.Parent.oQC.RemoveTable(This.Parent.TblTables.List(i,2))
                i=i-1
            Endif
        Next
    Proc BtnOpenTbl.Click()
        Local i
        For i=1 To This.Parent.TblTables.ListCount
            If This.Parent.TblTables.Selected(i)
                If Lower(Right(This.Parent.TblTables.List(i,1),4))='.vqr' And This.Parent.TblTables.List(i,4)<>'Routine' And File(Fullpath(This.Parent.TblTables.List(i,1)))
                    This.Parent.Parent.Parent.Parent.oQC.oForm.AddQuery()
                    This.Parent.Parent.Parent.Parent.oQC.cFileName = Fullpath(This.Parent.TblTables.List(i,1))
                    This.Parent.Parent.Parent.Parent.oQC.LoadDoc()
                    This.Parent.Parent.Parent.Parent.oQC.SetTitle()
                Endif
            Endif
        Next
    Procedure btnTestCompanyList.Click()
        This.Parent.TestCompanyList.Value = cp_GetCompanyList()
    Proc Init()
        This.Width=Thisform.Width-10
        This.Height= (Thisform.Height/2)-60
    Endproc
    Proc TblTables.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='ltables' Or Lower(oSource.Name)=='ltbls'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldicon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldicon
            Endcase
        Endif
    Proc TblTables.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local c_m
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='ltables' Or Lower(oSource.Name)=='ltbls'
            oSource.Drag(2)
            oSource.Parent.LTables.DblClick()
            *c_m = lower(oSource.name)+'.DblClick()'
            *oSource.parent.&c_m
        Endif
        Thisform.LockScreen=.F.
    Proc TblTables.DblClick()
        Local i,j
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
        j=This.ListIndex
        If j<>0 And Lower(Right(This.List(j,1),4))='.vqr'
            Do Case
                Case Empty(This.List(j,4))
                    This.List(j,4)='Temp. Table'
                Case This.List(j,4)='Temp. Table'
                    This.List(j,4)='Routine'
                Case This.List(j,4)='Routine'
                    This.List(j,4)=''
            Endcase
        Endif

    Proc chkUnionAll.Click()
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Endproc

    Proc chkMultiCompany.Click()
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Endproc
Enddefine

Define Class oFdContainer As Container
    BorderWidth=0
    BackStyle = 0
    cOldicon=''
    Add Object fdlfields As ListboxWithContainer With Width=440, Height=150, Left=5, Top=20,;
        columnwidths="80,100,80,70,30,30", ColumnCount=6, MultiSelect=1, MoverBars=.T.
    Add Object FLCnt As LBFContainer
    Add Object Txt1FD As Label With Left=25, Top=2, Caption="Description", BackStyle = 0
    Add Object Txt2FD As Label With Left=160, Top=2, Caption="Name", BackStyle = 0
    Add Object Txt3FD As Label With Left=340, Top=2, Caption="Alias", BackStyle = 0
    Add Object Txt4FD As Label With Left=410, Top=2, Caption="Type", BackStyle = 0
    Add Object Txt5FD As Label With Left=440, Top=2, Caption="Len", BackStyle = 0
    Add Object Txt6FD As Label With Left=470, Top=2, Caption="Dec", BackStyle = 0
    Add Object FdDis  As Checkbox With Left=470, Top=10, Width=60, Caption="Distinct", BackStyle = 0
    Add Object BtnDelFld As CommandButton With Left=455, Top=30, Width=80, Height=25, Caption="Delete Field"
    Add Object BtnDelOrd As CommandButton With Left=455, Top=60, Width=80, Height=25, Caption="Delete Order"
    Add Object BtnDelGrp As CommandButton With Left=455, Top=90, Width=80, Height=25, Caption="Delete Group"
    && Add Object BtnPivot As CommandButton With Left=455, Top=120, Width=80, Height=25, Caption="Pivot"
	Add Object BtnSetType As CommandButton With Left=455, Top=120, Width=80, Height=25, Caption="Set Type"
    Add Object Txt1Or As Label With Left=560, Top=5, Caption="Order By", BackStyle = 0
    Add Object FdLOrder As ListBox With Width=550, Height=70, Left=540, Top=20,;
        columnwidths="125,125,60", ColumnCount=3, MultiSelect=1, MoverBars=.T.
    Add Object Txt1Gr As Label With Left=560, Top=95, Caption="Group By", BackStyle = 0
    Add Object FdLGroups As ListBox With Width=550, Height=70, Left=540, Top=110,;
        columnwidths="150,150", ColumnCount=2, MultiSelect=1, MoverBars=.T.
    Proc fdlfields.GetRowContainer()
        Return(This.Parent.FLCnt)
    Proc BtnDelFld.Click()
        Local i,cAlias,cDescr,i_cAlias,i_cName
        For i=1 To This.Parent.fdlfields.ListCount
            If This.Parent.fdlfields.Selected(i)
                cDescr=This.Parent.fdlfields.List(i,1)
                cAlias=This.Parent.fdlfields.List(i,2)
                cAlias=Left(cAlias,At('.',cAlias)-1)
                This.Parent.Parent.Parent.Parent.oQC.oGraph.RemoveField(cAlias,cDescr)
                i_cName=This.Parent.fdlfields.List(i,2)
                i_cAlias=This.Parent.fdlfields.List(i,3)
                This.Parent.Parent.Parent.Parent.RemoveOrderBy(Iif(At('.'+i_cAlias+'|',i_cName+'|')<>0,i_cName,i_cAlias))
                This.Parent.Parent.Parent.Parent.RemoveGroupBy(Iif(At('.'+i_cAlias+'|',i_cName+'|')<>0,i_cName,i_cAlias))
                This.Parent.Parent.Parent.Parent.RemovePivot(i_cAlias)
                This.Parent.fdlfields.RemoveItem(i)
                i=i-1
            Endif
        Next
    Endproc
    Proc BtnDelGrp.Click()
        Local i
        For i=1 To This.Parent.FdLGroups.ListCount
            If This.Parent.FdLGroups.Selected(i)
                This.Parent.FdLGroups.RemoveItem(i)
                This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
                i=i-1
            Endif
        Next
    Endproc
    Proc BtnDelOrd.Click()
        Local i
        For i=1 To This.Parent.FdLOrder.ListCount
            If This.Parent.FdLOrder.Selected(i)
                This.Parent.FdLOrder.RemoveItem(i)
                This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
                i=i-1
            Endif
        Next
    Endproc
    && Proc BtnPivot.Click()
        && This.Parent.Parent.Parent.Parent.oPivot.show_()
	Proc BtnSetType.Click()
		This.Parent.Parent.Parent.Parent.CheckFieldsType(.T.)
    Proc Init()
        This.Width=Thisform.Width-10
        This.Height= (Thisform.Height/2)-60
        This.FdDis.Value=0
    Endproc
    Proc fdlfields.MouseMove(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            This.DragIcon='dragmove.cur'
            This.Drag(1)
        Endif
    Func IsUsedField(cName,cAlias)
        Local i
        For i=1 To This.fdlfields.ListCount
            If This.fdlfields.List(i,2)==cAlias+'.'+cName
                Return(.T.)
            Endif
        Next
        Return(.F.)
    Endfunc
    Func IsUsedFieldInGroup(cAlias)
        Local i
        For i=1 To This.FdLGroups.ListCount
            If This.FdLGroups.List(i,2)==cAlias
                Return(.T.)
            Endif
        Next
        Return(.F.)
    Endfunc
    Func IsUsedFieldInOrder(cAlias)
        Local i
        For i=1 To This.FdLOrder.ListCount
            If This.FdLOrder.List(i,2)==cAlias
                Return(.T.)
            Endif
        Next
        Return(.F.)
    Endfunc
    Proc fdlfields.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='lfields'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldicon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldicon
            Endcase
        Endif
    Proc fdlfields.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local nCnt
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='lfields'
            oSource.Drag(2)
            For nCnt = 1 To oSource.ListCount
                If oSource.Selected(nCnt)  && Is item selected?
                    oSource.Parent.LFields._Dblclick(nCnt)
                Endif
            Endfor
        Endif
        Thisform.LockScreen=.F.
    Proc FdLGroups.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='fdlfields'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldicon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldicon
            Endcase
        Endif
    Proc FdLGroups.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local i
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='fdlfields'
            oSource.Drag(2)
            This.Parent.FillGroup()
        Endif
        Thisform.LockScreen=.F.
    Proc FdLOrder.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='fdlfields'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldicon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldicon
            Endcase
        Endif
    Proc FdLOrder.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local i
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='fdlfields'
            oSource.Drag(2)
            This.Parent.FillOrder()
        Endif
        Thisform.LockScreen=.F.

    Procedure FdLOrder.OnMoveItem
        Lparameters nSource, nShift, nCurrentIndex, nMoveBy
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
        Return .T.
    Endproc

    Procedure FdLGroups.OnMoveItem
        Lparameters nSource, nShift, nCurrentIndex, nMoveBy
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
        Return .T.
    Endproc


    Proc FillGroup()
        Local i,cName,cAlias
        For i=1 To This.fdlfields.ListCount
            If This.fdlfields.Selected(i) And Not(This.IsUsedFieldInGroup(This.fdlfields.List(i,3)))
                cName=This.fdlfields.List(i,2)
                cAlias=This.fdlfields.List(i,3)
                This.FdLGroups.AddItem(This.fdlfields.List(i,1))
                This.FdLGroups.List(This.FdLGroups.ListCount,2)=Iif(At('.'+cAlias+'|',cName+'|')<>0,cName,cAlias)
                This.Parent.Parent.Parent.oQC.bModified=.T.
            Endif
        Next
    Proc FillOrder()
        Local i
        For i=1 To This.fdlfields.ListCount
            If This.fdlfields.Selected(i) And Not(This.IsUsedFieldInOrder(This.fdlfields.List(i,3)))
                cName=This.fdlfields.List(i,2)
                cAlias=This.fdlfields.List(i,3)
                This.FdLOrder.AddItem(This.fdlfields.List(i,1))
                This.FdLOrder.List(This.FdLOrder.ListCount,2)=Iif(At('.'+cAlias+'|',cName+'|')<>0,cName,cAlias)
                This.FdLOrder.List(This.FdLOrder.ListCount,3)='Ascending'
                This.Parent.Parent.Parent.oQC.bModified=.T.
            Endif
        Next
    Proc FdLOrder.DblClick()
        Local i,j
        *j=0
        *for i=1 to this.listcount
        *  if this.selected(i)
        *    j=i
        *  endif
        *next
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
        j=This.ListIndex
        If j<>0
            If This.List(j,3)='Ascending'
                This.List(j,3)='Descending'
            Else
                This.List(j,3)='Ascending'
            Endif
        Endif
        This.Click()
    Endproc
    Proc FdLOrder.Click()
        If Version(5)>700
            This.ToolTipText = Alltrim(This.List(This.ListIndex, 1))+"|"+Alltrim(This.List(This.ListIndex, 2))+"|"+Alltrim(This.List(This.ListIndex, 3))
        Else
            This.ToolTipText = Left(Alltrim(This.List(This.ListIndex, 1))+"|"+Alltrim(This.List(This.ListIndex, 2))+"|"+Alltrim(This.List(This.ListIndex, 3)),127)
        Endif
    Endproc
    Proc FdLGroups.Click()
        If Version(5)>700
            This.ToolTipText = Alltrim(This.List(This.ListIndex, 1))+"|"+Alltrim(This.List(This.ListIndex, 2))
        Else
            This.ToolTipText = Left(Alltrim(This.List(This.ListIndex, 1))+"|"+Alltrim(This.List(This.ListIndex, 2)),127)
        Endif
    Endproc

    Proc FdDis.Click()
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Endproc
Enddefine

Define Class LBFContainer As ContainerForListbox
    Width=540
    Height=20
    BorderWidth=1
    Add Object IFDesc  As TextBox With SpecialEffect=1,Width=130,Height=20,Left=1,MaxLength=200
    Add Object IFAlias As TextBox With SpecialEffect=1,Width=80,Height=20,Left=316,MaxLength=MAX_EXPR_LENGHT
    Add Object IFType  As ComboBox With SpecialEffect=1,Width=71,Height=20,Left=400,Style=2
    Add Object IFLen As TextBox With SpecialEffect=1,Width=30,Height=20,Left=474,InputMask="999"
    Add Object IFDec As TextBox With SpecialEffect=1,Width=30,Height=20,Left=507,InputMask="99"
    Proc Init
        This.AddObject('IFName', "ExprBox", 1, 100, 20, 133, MAX_EXPR_LENGHT)
        This.IFType.AddItem('Char')
        This.IFType.AddItem('Numeric')
        This.IFType.AddItem('Date')
        This.IFType.AddItem('Logic')
        This.IFType.AddItem('Memo')
        This.IFType.AddItem('DateTime')
    Proc SetValues(olist,nRow)
        DoDefault(olist,nRow)
        olist.List(nRow,1)=Trim(This.IFDesc.Value)
        olist.List(nRow,2)=Trim(This.IFName.Value)
        olist.List(nRow,3)=Trim(This.IFAlias.Value)
        olist.List(nRow,4)=Trim(This.IFType.Value)
        olist.List(nRow,5)=Str(This.IFLen.Value,3,0)
        olist.List(nRow,6)=Str(This.IFDec.Value,2,0)
        olist.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Proc GetValues(olist,nRow)
        Local N
        DoDefault(olist,nRow)
        This.IFDesc.Value=olist.List(nRow,1)
        This.IFName.Value=olist.List(nRow,2)
        This.IFAlias.Value=olist.List(nRow,3)
        N=Left(olist.List(nRow,4),1)
        Do Case
            Case N='C'
                N='Char'
            Case N='M'
                N='Memo'
            Case N='N'
                N='Numeric'
            Case N='T' Or olist.List(nRow,4)='DateTime'
                N='DateTime'
            Case N='D'
                N='Date'
            Otherwise
                N='Logic'
        Endcase
        This.IFType.Value=N
        This.IFLen.Value=Val(olist.List(nRow,5))
        This.IFDec.Value=Val(olist.List(nRow,6))
    Proc BlankValues()
        This.IFDesc.Value=''
        This.IFName.Value=''
        This.IFAlias.Value=''
    Proc GetList()
        Return(This.Parent.fdlfields)
Enddefine

Define Class oRlContainer As Container
    BorderWidth=0
    BackStyle = 0
    cOldicon=''
    Add Object rllrelation As ListboxWithContainer With Width=698, Height=150, Left=5, Top=20,;
        columnwidths="135,265,100,70,70", ColumnCount=5, MultiSelect=1, MoverBars=.T.
    Add Object RLCnt As LBRContainer
    Add Object Txt1FD As Label With Left=25, Top=2, Caption="Description", BackStyle = 0
    Add Object Txt2FD As Label With Left=165, Top=2, Caption="Expression", BackStyle = 0
    Add Object Txt3FD As Label With Left=430, Top=2, Caption="Type", BackStyle = 0
    Add Object Txt4FD As Label With Left=535, Top=2, Caption="Table 1", BackStyle = 0
    Add Object Txt5FD As Label With Left=605, Top=2, Caption="Table 2", BackStyle = 0
    Add Object BtnDelRel As CommandButton With Left=710, Top=40, Width=70, Height=25, Caption="Delete"
    Add Object BtnAddRel As CommandButton With Left=710, Top=70, Width=70, Height=25, Caption="Add"
    Proc rllrelation.GetRowContainer()
        Return(This.Parent.RLCnt)
    Proc BtnDelRel.Click()
        Local i
        For i=1 To This.Parent.rllrelation.ListCount
            If This.Parent.rllrelation.Selected(i)
                This.Parent.Parent.Parent.Parent.oQC.RemoveRelation(This.Parent.rllrelation.List(i,4),This.Parent.rllrelation.List(i,5))
            Endif
        Next
    Proc BtnAddRel.Click()
        This.Parent.rllrelation.AddItem('')
    Proc Init()
        This.Width=Thisform.Width-10
        This.Height= (Thisform.Height/2)-60
    Endproc
    Func ExistRelation(cAlias1,cAlias2)
        Local i
        For i=1 To This.rllrelation.ListCount
            If    (This.rllrelation.List(i,4)==cAlias1 .And. This.rllrelation.List(i,5)==cAlias2);
                    .Or. (This.rllrelation.List(i,4)==cAlias2 .And. This.rllrelation.List(i,5)==cAlias1)
                Return(.T.)
            Endif
        Next
        Return(.F.)
    Endfunc
    Proc rllrelation.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='lrelated'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldicon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldicon
            Endcase
        Endif
    Proc rllrelation.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local i
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='lrelated'
            oSource.Drag(2)
            oSource.Parent.LRelated.DblClick()
        Endif
        Thisform.LockScreen=.F.
Enddefine

Define Class LBRContainer As ContainerForListbox
    Width=660
    Height=20
    Add Object IRDesc  As TextBox With SpecialEffect=1,Width=135,Height=20,Left=1,MaxLength=200
    *add object IRExpr  as textbox with specialeffect=1,width=265,height=20,left=139,BorderStyle=1,maxlength=255
    Add Object IRType  As ComboBox With SpecialEffect=1,Width=100,Height=20,Left=406,Style=2
    Add Object IRAli1  As ComboBox With SpecialEffect=1,Width=70,Height=20,Left=510,BorderStyle=1
    Add Object IRAli2  As ComboBox With SpecialEffect=1,Width=70,Height=20,Left=582,BorderStyle=1
    Procedure LostFocus
        DoDefault()
        This.Parent.Parent.Parent.Parent.Parent.oQueryInfo.oPage.Page1.oGraph.RemoveAllRelation()
        Local i, l_type, l_alias1, l_alias2
        For i=1 To This.Parent.rllrelation.ListCount
            l_type = This.Parent.rllrelation.List(i,3)
            l_alias1 = This.Parent.rllrelation.List(i,4)
            l_alias2 = This.Parent.rllrelation.List(i,5)
            If !Empty(l_alias2) And !Empty(l_alias1) And !Empty(l_type)
                This.Parent.Parent.Parent.Parent.Parent.oQueryInfo.oPage.Page1.oGraph.Addrelation(l_alias1, l_alias2, l_type)
            Endif
        Next

    Endproc
    Proc Init()
        This.AddObject('IRExpr', "ExprBox", 1, 265, 20, 139, MAX_EXPR_LENGHT)
        This.IRType.AddItem('Left outer')
        This.IRType.AddItem('Right outer')
        This.IRType.AddItem('Inner')
        This.IRType.AddItem('Full')
        This.IRType.AddItem('Where')
    Proc SetValues(olist,nRow)
        DoDefault(olist,nRow)
        olist.List(nRow,1)=Trim(This.IRDesc.Value)
        olist.List(nRow,2)=Trim(This.IRExpr.Value)
        olist.List(nRow,3)=Trim(This.IRType.Value)
        olist.List(nRow,4)=Trim(This.IRAli1.Value)
        olist.List(nRow,5)=Trim(This.IRAli2.Value)
        olist.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Proc GetValues(olist,nRow)
        DoDefault(olist,nRow)
        Local i
        This.IRAli1.Clear()
        This.IRAli2.Clear()
        For i=1 To olist.Parent.Parent.Parent.Page1.opag.TblTables.ListCount
            This.IRAli1.AddItem(olist.Parent.Parent.Parent.Page1.opag.TblTables.List(i,2))
            This.IRAli2.AddItem(olist.Parent.Parent.Parent.Page1.opag.TblTables.List(i,2))
        Next
        This.IRDesc.Value=olist.List(nRow,1)
        This.IRExpr.Value=olist.List(nRow,2)
        This.IRType.Value=olist.List(nRow,3)
        This.IRAli1.Value=olist.List(nRow,4)
        This.IRAli2.Value=olist.List(nRow,5)
    Proc BlankValues()
        This.IRDesc.Value=''
        This.IRExpr.Value=''
        This.IRType.Value=''
        This.IRAli1.Value=''
        This.IRAli2.Value=''
    Proc GetList()
        Return(This.Parent.rllrelation)
Enddefine

Define Class oFlContainer As Container
    BorderWidth=0
    BackStyle = 0
    cOldicon=''
    Add Object fllfilter As ListboxWithContainer With Width=650, Height=150, Left=5, Top=20,;
        columnwidths="180,40,70,200,46,50", ColumnCount=6, MultiSelect=1, MoverBars=.T.
    Add Object FiLCnt As LBFilContainer
    Add Object Txt1Fil As Label With Left=25, Top=2, Caption="Field Name", BackStyle = 0
    Add Object Txt2Fil As Label With Left=220, Top=2, Caption="Not", BackStyle = 0
    Add Object Txt3Fil As Label With Left=250, Top=2, Caption="Criteria", BackStyle = 0
    Add Object Txt4Fil As Label With Left=325, Top=2, Caption="Example", BackStyle = 0
    Add Object Txt5Fil As Label With Left=525, Top=2, Caption="Logical", BackStyle = 0
    Add Object BtnDelFil As CommandButton With Left=687, Top=40, Width=80, Height=25, Caption="Delete Filter"
    Add Object BtnAddFil As CommandButton With Left=687, Top=70, Width=80, Height=25, Caption="Add Filter"
    Add Object BtnOpenFil As CommandButton With Left=687, Top=100, Width=80, Height=25, Caption="Open"
    Proc fllfilter.GetRowContainer()
        Return(This.Parent.FiLCnt)
    Proc BtnDelFil.Click()
        Local i
        For i=1 To This.Parent.fllfilter.ListCount
            If This.Parent.fllfilter.Selected(i)
                This.Parent.fllfilter.RemoveItem(i)
                i=i-1
            Endif
        Next
        Return
    Proc BtnAddFil.Click()
        This.Parent.fllfilter.AddItem('')
        This.Parent.fllfilter.List(This.Parent.fllfilter.ListCount,5)='AND'
        Return
    Procedure BtnOpenFil.Click()
        Local i, l_FileName
        For i=1 To This.Parent.fllfilter.ListCount
            If This.Parent.fllfilter.Selected(i)
                *--- Elimino eventuali parentesi
                l_FileName = This.Parent.fllfilter.List(i,4)
                Do While Right(Alltrim(l_FileName),1)=')'
                    l_FileName = Left(l_FileName, Len(Alltrim(l_FileName))-1)
                Enddo
                l_FileName = Fullpath(Forceext(l_FileName,"VQR"))
                If (Inlist(Trim(This.Parent.fllfilter.List(i,3)),'in','not in','exists','not exists') Or 'any'$This.Parent.fllfilter.List(i,3) Or 'all'$This.Parent.fllfilter.List(i,3)) And File(l_FileName)
                    This.Parent.Parent.Parent.Parent.oQC.oForm.AddQuery()
                    This.Parent.Parent.Parent.Parent.oQC.cFileName = l_FileName
                    This.Parent.Parent.Parent.Parent.oQC.LoadDoc()
                    This.Parent.Parent.Parent.Parent.oQC.SetTitle()
                Endif
            Endif
        Next
    Proc Init()
        This.Width=Thisform.Width-10
        This.Height= (Thisform.Height/2)-60
    Endproc
    Proc fllfilter.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='lfields'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldicon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldicon
            Endcase
        Endif
    Proc fllfilter.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local i
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='lfields'
            oSource.Drag(2)
            This.Parent.FillFilter(oSource)
            If Not(oSource.Parent.oQC.IsUsedTable(oSource.Parent.cCurrAlias))
                oSource.Parent.LTables.DblClick()
            Endif
        Endif
        Thisform.LockScreen=.F.
    Proc FillFilter(oSource,cParam, nIndex)
        Local i,j
        If Vartype(m.nIndex)='N'
            This.fllfilter.AddItem(oSource.Parent.cCurrAlias+'.'+oSource.Parent.LFields.List(m.nIndex,2))
            This.fllfilter.List(This.fllfilter.ListCount,2)=''
            This.fllfilter.List(This.fllfilter.ListCount,3)='='
            This.fllfilter.List(This.fllfilter.ListCount,4)=Iif(Type('cParam')='C',cParam,'')
            This.fllfilter.List(This.fllfilter.ListCount,5)='AND'
        Else
            j=0
            For i=1 To oSource.Parent.LFields.ListCount
                If oSource.Parent.LFields.Selected(i)
                    j=i
                    If j<>0
                        This.fllfilter.AddItem(oSource.Parent.cCurrAlias+'.'+oSource.Parent.LFields.List(j,2))
                        This.fllfilter.List(This.fllfilter.ListCount,2)=''
                        This.fllfilter.List(This.fllfilter.ListCount,3)='='
                        This.fllfilter.List(This.fllfilter.ListCount,4)=Iif(Type('cParam')='C',cParam,'')
                        This.fllfilter.List(This.fllfilter.ListCount,5)='AND'
                    Endif
                Endif
            Next
        Endif
    Proc ChangeParam(old,new)
        Local i
        For i=1 To This.fllfilter.ListCount
            If Trim(This.fllfilter.List(i,4))==Trim('?'+old)
                This.fllfilter.List(i,4)='?'+Trim(new)
            Endif
        Next
Enddefine

Define Class LBFilContainer As ContainerForListbox
    Width=612
    Height=20
    cFileName=''
    *add object IFField as textbox with specialeffect=1,width=182,height=20,left=1,MaxLength=255
    Add Object IFNot   As Checkbox With SpecialEffect=1,Width=40,Height=20,Left=186, Caption='Not'
    Add Object IFCrite As ComboBox With SpecialEffect=1,Width=74,Height=20,Left=225,Style=2
    *add object IFExamp as textbox with specialeffect=1,width=204,height=20,left=298,MaxLength=255
    Add Object GetQryBtn As CommandButton With Width=19,Caption='...',Visible=.F.,Height=19,Left=483,Top=1
    Add Object IFLOp   As ComboBox With SpecialEffect=1,Width=50,Height=20,Left=501,Style=2
    Add Object IFLHaving As Checkbox With SpecialEffect=1,Width=55,Height=20,Left=553,Caption='Having'
    Proc Init()
        This.AddObject('IFField', "ExprBox", 1, 182, 20, 1, MAX_EXPR_LENGHT)
        This.AddObject('IFExamp', "ExprBox", 1, 204, 20, 298, MAX_EXPR_LENGHT)
        This.GetQryBtn.ZOrder(0)
        This.IFCrite.AddItem('=')
        This.IFCrite.AddItem('like')
        This.IFCrite.AddItem('>')
        This.IFCrite.AddItem('<')
        This.IFCrite.AddItem('>=')
        This.IFCrite.AddItem('<=')
        This.IFCrite.AddItem('<>')
        This.IFCrite.AddItem('in')
        This.IFCrite.AddItem('not in')
        This.IFCrite.AddItem('exists')
        This.IFCrite.AddItem('not exists ')
        This.IFCrite.AddItem('between')
        This.IFCrite.AddItem('not between')
        This.IFCrite.AddItem('is null')
        This.IFCrite.AddItem('is not null')
        This.IFCrite.AddItem('> all')
        This.IFCrite.AddItem('> any')
        This.IFCrite.AddItem('< all')
        This.IFCrite.AddItem('< any')
        This.IFCrite.AddItem('>= all')
        This.IFCrite.AddItem('>= any')
        This.IFCrite.AddItem('<= all')
        This.IFCrite.AddItem('<= any')
        This.IFCrite.AddItem('<> all')
        This.IFCrite.AddItem('<> any')
        This.IFLOp.AddItem('AND')
        This.IFLOp.AddItem('OR')
    Proc SetValues(olist,nRow)
        DoDefault(olist,nRow)
        olist.List(nRow,1)=Trim(This.IFField.Value)
        olist.List(nRow,2)=Iif(This.IFNot.Value=0,'','Not')
        olist.List(nRow,3)=Trim(This.IFCrite.Value)
        olist.List(nRow,4)=Trim(This.IFExamp.Value)
        olist.List(nRow,5)=Trim(This.IFLOp.Value)
        Local i_bHaving,c
        i_bHaving=Iif(This.IFLHaving.Value=1,'Having','')
        c=Lower(This.IFField.Value)
        If 'sum('$c Or 'max('$c Or 'min('$c Or 'avg('$c
            i_bHaving='Having'
        Endif
        olist.List(nRow,6)=i_bHaving
        olist.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Proc GetValues(olist,nRow)
        DoDefault(olist,nRow)
        This.cFileName=olist.Parent.Parent.Parent.Parent.oQC.cFileName
        This.IFField.Value=olist.List(nRow,1)
        This.IFNot.Value=Iif(Len(Trim(olist.List(nRow,2)))=0,0,1)
        This.IFCrite.Value=olist.List(nRow,3)
        This.IFExamp.Value=olist.List(nRow,4)
        This.IFLOp.Value=olist.List(nRow,5)
        This.IFLHaving.Value=Iif(olist.List(nRow,6)='Having',1,0)
        This.SetSubQueryButton()
    Proc BlankValues()
        This.IFField.Caption=''
        This.IFNot.Value=0
        This.IFCrite.Value=''
        This.IFExamp.Value=''
        This.IFLOp.Value=''
    Proc GetList()
        Return(This.Parent.fllfilter)
    Proc IFCrite.Valid()
        This.Parent.SetSubQueryButton()
        Return(.T.)
    Proc SetSubQueryButton()
        Local i_bSubQuery, l_width
        i_bSubQuery=Inlist(Trim(This.IFCrite.Value),'in','not in','exists','not exists') Or 'any'$This.IFCrite.Value Or 'all'$This.IFCrite.Value
        This.GetQryBtn.Visible=i_bSubQuery
        This.Parent.Parent.Parent.Parent.Resize()
        *l_width = CEILING((this.parent.FlLFilter.width-266)/2)
        *this.IFExamp.width=iif(i_bSubQuery, l_width-19, l_width)
        *this.IFExamp.ExprText.width=this.IFExamp.width
        Return
    Proc GetQryBtn.Click()
        Local F,p
        F=Getfile('vqr')
        *--- Path minimo
        F=Sys(2014,F)
        If Lower(Right(F,4))='.vqr'
            F=Left(F,Len(F)-4)
        Endif
        This.Parent.IFExamp.Value=F
        Return
Enddefine

Define Class oFpContainer As Container
    BorderWidth=0
    BackStyle = 0
    cOldicon=''
    Add Object fplparam As ListboxWithContainer With Width=550, Height=150, Left=5, Top=20,;
        columnwidths="150,150,70,50,27,50", ColumnCount=6, MultiSelect=1, MoverBars=.T.
    Add Object FiLParCnt As LBFilParContainer
    Add Object Txt1Fp As Label With Left=25, Top=2, Caption="Field Name", BackStyle = 0
    Add Object Txt2Fp As Label With Left=176, Top=2, Caption="Description", BackStyle = 0
    Add Object Txt3Fp As Label With Left=332, Top=2, Caption="Type", BackStyle = 0
    Add Object Txt4Fp As Label With Left=405, Top=2, Caption="Len", BackStyle = 0
    Add Object Txt5Fp As Label With Left=460, Top=2, Caption="Dec", BackStyle = 0
    Add Object Txt7Fp As Label With Left=485, Top=2, Caption="Remove on empty", BackStyle = 0
    Add Object BtnDelFp As CommandButton With Left=607, Top=40, Width=85, Height=25, Caption="Delete Param."
    Add Object BtnAddFp As CommandButton With Left=702, Top=40, Width=85, Height=25, Caption="Add Param."
    Add Object Txt6Fp As Label With Left=565, Top=77, Caption="Dialog:", BackStyle = 0
    Add Object MaskFp As TextBox With Width=160,Height=22,Top=75,Left=607, MaxLength=255
    Add Object BtnMskFp As CommandButton With Left=771, Top=75, Width=15, Height=22, Caption="..."
    Add Object RemoveFp As Checkbox With Left=570,Top=100,Width=230,Caption='Remove filter on empty parameter',Value=.F., BackStyle = 0
    Add Object OrderByParm As Checkbox With Left=570,Top=150,Width=230,Caption='Use OrderBy parameter',Value=.F., BackStyle = 0
    Add Object BtnCode As CommandButton With Left=607, Top=120, Width=100, Height=25, Caption="Generate code"
    Proc fplparam.GetRowContainer()
        Return(This.Parent.FiLParCnt)
    Proc BtnDelFp.Click()
        Local i,j,N
        For i=1 To This.Parent.fplparam.ListCount
            If This.Parent.fplparam.Selected(i)
                N=This.Parent.fplparam.List(i,1)
                This.Parent.fplparam.RemoveItem(i)
                For j=This.Parent.Parent.Parent.page4.opag.fllfilter.ListCount To 1 Step -1
                    If This.Parent.Parent.Parent.page4.opag.fllfilter.List(j,4)=='?'+N
                        This.Parent.Parent.Parent.page4.opag.fllfilter.RemoveItem(j)
                    Endif
                Next
                i=i-1
                This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
            Endif
        Next
        Return
    Proc BtnAddFp.Click()
        Local N, i, k
        k=0
        N='pNONAMEVAR'
        For i=1 To This.Parent.fplparam.ListCount
            If This.Parent.fplparam.List(i,1)=N
                k=k+1
            Endif
        Next
        If k<>0
            N=N+Alltrim(Str(k))
        Endif
        This.Parent.fplparam.AddItem(N)
        This.Parent.fplparam.List(This.Parent.fplparam.ListCount,2)=""
        Local cType,nLen,nDec
        cType="Char"
        nLen=10
        nDec=0
        This.Parent.fplparam.List(This.Parent.fplparam.ListCount,3)=cType
        This.Parent.fplparam.List(This.Parent.fplparam.ListCount,4)=Alltrim(Str(nLen))
        This.Parent.fplparam.List(This.Parent.fplparam.ListCount,5)=Alltrim(Str(nDec))
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Endproc
    Proc BtnCode.Click()
        If Empty(This.Parent.Parent.Parent.Parent.oQC.cFileName)
            CP_MSG(cp_Translate(MSG_QUERY_NAME_MISSING_C_CANNOT_GENERATE_CODE),.F.)
            Return
        Endif
        This.Parent.Parent.Parent.Parent.oQC.CreateQueryCode()
    Proc Init()
        This.Width=Thisform.Width-10
        This.Height= (Thisform.Height/2)-60
        Return
    Proc fplparam.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='lfields'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldicon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldicon
            Endcase
        Endif
    Proc fplparam.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local i
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='lfields'
            oSource.Drag(2)
            This.Parent.FillParam(oSource)
        Endif
        Thisform.LockScreen=.F.
    Proc FillParam(oSource)
        Local i,j,k,cTable,nFld,N
        *j=oSource.parent.lFields.ListIndex
        For j = 1 To oSource.ListCount
            If oSource.Selected(j)  && Is item selected?
                If j<>0
                    cTable=oSource.Parent.cCurrTable
                    nFld=oSource.Parent.oQC.oDcx.GetFieldIdx(cTable,oSource.Parent.LFields.List(j,2))
                    k=0
                    N='p'+oSource.Parent.LFields.List(j,2)
                    For i=1 To This.fplparam.ListCount
                        If This.fplparam.List(i,1)=N
                            k=k+1
                        Endif
                    Next
                    If k<>0
                        N=N+Alltrim(Str(k))
                    Endif
                    This.fplparam.AddItem(N)
                    This.fplparam.List(This.fplparam.ListCount,2)=oSource.Parent.LFields.List(j,1)
                    Local cType,nLen,nDec,i_oQry
                    If oSource.Parent.oQC.oDcx.GetTableIdx(cTable)<>0
                        cType=oSource.Parent.oQC.oDcx.GetFieldType(cTable,nFld)
                        nLen=oSource.Parent.oQC.oDcx.GetFieldLen(cTable,nFld)
                        nDec=oSource.Parent.oQC.oDcx.GetFieldDec(cTable,nFld)
                    Else
                        i_oQry=oSource.Parent.GetQueryDefinitions(cTable)
                        cType=i_oQry.i_Fields[j,3]
                        nLen=i_oQry.i_Fields[j,4]
                        nDec=i_oQry.i_Fields[j,5]
                    Endif
                    This.fplparam.List(This.fplparam.ListCount,3)=This.Parent.Parent.Parent.CharToType(cType)
                    This.fplparam.List(This.fplparam.ListCount,4)=Alltrim(Str(nLen))
                    This.fplparam.List(This.fplparam.ListCount,5)=Alltrim(Str(nDec))

                    This.Parent.Parent.page4.opag.FillFilter(oSource,'?'+N, j)
                Endif
            Endif
        Endfor

        *this.MaskFp.value=oSource.parent.cExtMask
    Proc BtnMskFp.Click()
        Local F,p
        F=Getfile('vfm')
        If Empty(This.Parent.Parent.Parent.Parent.oQC.cFileName)
            p=Sys(5)+Sys(2003)+''
        Else
            p=This.Parent.Parent.Parent.Parent.oQC.cFileName
            If Rat('\',p)<>0
                p=Left(p,Rat('\',p)-1)
            Else
                p=''
            Endif
        Endif
        If Left(Lower(F),Len(p))=Lower(p)
            F=Substr(F,Len(p)+2)
        Endif
        This.Parent.MaskFp.Value=Iif(Empty(F),'',Left(F,Len(F)-4))
        This.Parent.Parent.Parent.Parent.oQC.cExtMask=Trim(This.Parent.MaskFp.Value)
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Proc MaskFp.Valid()
        This.Parent.Parent.Parent.Parent.oQC.cExtMask=Trim(This.Value)
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Proc RemoveFp.Valid()
        This.Parent.Parent.Parent.Parent.oQC.bRemoveWhereOnEmptyParam=This.Value
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Proc OrderByParm.Valid()
        This.Parent.Parent.Parent.Parent.oQC.bOrderByParm=This.Value
        This.Parent.Parent.Parent.Parent.oQC.bModified=.T.
Enddefine

Define Class LBFilParContainer As ContainerForListbox
    Width=510
    Height=20
    Add Object IFPField As TextBox With SpecialEffect=1,Width=151,Height=20,Left=1,MaxLength=125
    Add Object IFPDescr As TextBox With SpecialEffect=1,Width=152,Height=20,Left=152,MaxLength=125
    Add Object IFPType  As ComboBox With SpecialEffect=1,Width=71,Height=20,Left=306,Style=2
    Add Object IFPLen   As TextBox With SpecialEffect=1,Width=51,Height=20,Left=379,MaxLength=20
    Add Object IFPDec   As TextBox With SpecialEffect=1,Width=30,Height=20,Left=432,MaxLength=20
    Add Object IFPRemove As Checkbox With SpecialEffect=1,Width=20,Height=20,Left=475,Caption=''
    Proc Init
        This.IFPType.AddItem('Char')
        This.IFPType.AddItem('Numeric')
        This.IFPType.AddItem('Date')
        This.IFPType.AddItem('Logic')
        This.IFPType.AddItem('Memo')
        This.IFPType.AddItem('DateTime')
    Proc SetValues(olist,nRow)
        Local N,Oldval
        DoDefault(olist,nRow)
        Oldval=olist.List(nRow,1)
        olist.List(nRow,1)=Trim(This.IFPField.Value)
        olist.List(nRow,2)=Trim(This.IFPDescr.Value)
        olist.List(nRow,3)=Trim(This.IFPType.Value)
        olist.List(nRow,4)=Trim(This.IFPLen.Value)
        olist.List(nRow,5)=Trim(This.IFPDec.Value)
        olist.List(nRow,6)=Iif(This.IFPRemove.Value,'Remove','')
        olist.Parent.Parent.Parent.page4.opag.ChangeParam(Oldval,olist.List(nRow,1))
        olist.Parent.Parent.Parent.Parent.oQC.bModified=.T.
    Proc GetValues(olist,nRow)
        DoDefault(olist,nRow)
        This.IFPField.Value=olist.List(nRow,1)
        This.IFPDescr.Value=olist.List(nRow,2)
        N=Left(olist.List(nRow,3),1)
        Do Case
            Case N='C'
                N='Char'
            Case N='M'
                N='Memo'
            Case N='N'
                N='Numeric'
            Case N='T' Or olist.List(nRow,3)='DateTime'
                N='DateTime'
            Case N='D'
                N='Date'
            Otherwise
                N='Logic'
        Endcase
        This.IFPType.Value=N &&oList.list(nRow,3)
        This.IFPLen.Value=olist.List(nRow,4)
        This.IFPDec.Value=olist.List(nRow,5)
        This.IFPRemove.Value=(olist.List(nRow,6)='Remove')
    Proc BlankValues()
        This.IFPField.Value=''
        This.IFPDescr.Value=''
        This.IFPType.Value=''
        This.IFPLen.Value=''
        This.IFPDec.Value=''
        This.IFPRemove.Value=0
    Proc GetList()
        Return(This.Parent.fplparam)
Enddefine

Define Class vqnoteeditbox As EditBox
    Proc InteractiveChange()
        This.Parent.Parent.Parent.oQC.bModified=.T.
Enddefine

Define Class ExprBox As Container
    Add Object ExprText As EditBox With Left = 16
    Add Object ExprImg As Image With Left=0, Top = 2, Width = 16, Height = 16
    Value = Space(MAX_EXPR_LENGHT)	&&Contiene il memo con l'espressione
    Procedure Init (l_specialeffect, l_width, l_height, l_left, l_MaxLength)
        With This
            .Width=l_width
            .Height=l_height
            .Left=l_left
            .ExprText.ToolTipText='DblClick for Expression Builder'
            .ExprText.SpecialEffect=l_specialeffect
            .ExprText.Width=l_width
            .ExprText.Height=l_height
            .ExprText.MaxLength=l_MaxLength
            .ExprText.Visible=.T.
            .ExprImg.Picture = Forceext(i_ThemesManager.RetBmpFromIco("pen.ico", 16), "Bmp")
            .Visible=.T.
        Endwith

    Procedure ExprText.Width_Assign
        Parameters xValue
        If m.xValue<>This.Width
            This.Width = Max(This.Parent.ExprImg.Width, m.xValue - This.Parent.ExprImg.Width)
        Endif
    Endproc

    Procedure ExprImg.DblClick()
        This.Parent.ExprText.DblClick()

    Procedure ExprText.DblClick()
        This.Valid()
        oForm = Createobject("ExprBuilder", This.Parent)
        oForm.Show()
        oForm = .Null.
        Local i,o
        o=This.Parent.Parent.GetList()
        For i=1 To o.ListCount
            If o.Selected(i)
                This.Parent.Parent.SetValues(o, i)
                Exit
            Endif
        Next
    Endproc

    Procedure ExprText.Valid()
        This.Parent.Value = This.Value
    Endproc

    Procedure value_Assign(uValue)
        This.ExprText.Value = uValue
        This.Value = uValue
    Endproc

    Procedure value_Access
        Return This.Value
Enddefine && ExprBox

#Define BTN_H 23
#Define BTN_W 74
#Define BTN_OFFSET 26
#Define BTN_L 12
#Define BTN_T 12

#Define ARR_CAT_ORDER		1
#Define ARR_CAT_NAME		2
#Define ARR_CAT_TYPE		3
#Define ARR_CAT_FIELD_LIST	4
#Define ARR_CAT_FAVORITES	5
#Define ARR_CAT_SHOW_ALL		6

#Define ARR_FUN_ORDER			1
#Define ARR_FUN_NAME			2
#Define ARR_FUN_SINTAX			3
#Define ARR_FUN_CURS_POS		4
#Define ARR_FUN_DESCRI			5
#Define ARR_FUN_TYPE				6
#Define ARR_FUN_COLLKEY		7

Define Class _Category as custom
	sName = ""
	sType = "S"
	bFieldList = .F.
	bFavorites = .F.
	bShowAll = .F.
	bShowParameters = .F.
	oFunctionsCollection = .Null.
	
	Proc Init
		this.oFunctionsCollection = CREATEOBJECT("collection")
	EndProc
	
Enddefine

Define Class _Function as custom
	sName =""
	sSintax = ""
	nCursorPos = 0
	sDescri = ""
	sType = "S"
Enddefine

Define Class _ExpressionEditor as container

	Width = 598
    Height = 508 - BTN_H
	BorderStyle = 0
	BorderWidth = 0
	BackStyle = 0
	bResizerMoved = .F.
	nBottomMargin = 10

	ADD OBJECT lblExpression AS label WITH ;
		Anchor = 11, ;
		Alignment = 2, ;
		Caption = "Expression", ;
		Height = 17, ;
		Left = 1, ;
		Top = 1, ;
		Width = 595, ;
		BackStyle = 0, ;
		Name = "lblExpression"
	
	ADD OBJECT ExprMemo AS editbox WITH ;
		Anchor = 15, ;
		Height = 168, ;
		Left = 1, ;
		Top = 20, ;
		Width = 595, ;
		Name = "ExprMemo", ;
		MaxLength=MAX_EXPR_LENGHT, ;
                HideSelection = .F.
		
	ADD OBJECT ExprResizer AS ResizerLine WITH ;
		Top = 188, ;
		Left = 0, ;
		Width = 598, ;
		Anchor = 14, ;
		nMinHeight = 188
	
	ADD OBJECT lblCategories AS label WITH ;
		Anchor = 6, ;
		Alignment = 2, ;
		Caption = "Categories", ;
		Height = 17, ;
		Left = 1, ;
		Top = 195, ;
		Width = 296, ;
		BackStyle = 0, ;
		Name = "lblCategories"
	
	ADD OBJECT lstCategories AS ArrayList WITH ;
		Anchor = 6, ;
		Height = 131, ;
		Left = 1, ;
		Top = 212, ;
		Width = 296, ;
		ColumnCount = 2, ;
		Sorted = .F., ;
		ColumnWidths="0,150", ;
		Name = "lstCategories"

	ADD OBJECT lblFunctions AS label WITH ;
		Anchor = 14, ;
		Alignment = 2, ;
		Caption = "Available functions", ;
		Height = 17, ;
		Left = 298, ;
		Top = 195, ;
		Width = 298, ;
		BackStyle = 0, ;
		Name = "lblFunctions"

	ADD OBJECT lstFunctions AS ArrayList WITH ;
		Anchor = 14, ;
		Height = 131, ;
		Left = 298, ;
		Top = 212, ;
		Width = 298, ;
		ColumnCount = 2, ;
		ColumnWidths="0,1500", ;
		Sorted = .F., ;
		Name = "lstFunctions"
	
	ADD OBJECT lblDescription AS label WITH ;
		Anchor = 14, ;
		Alignment = 2, ;
		Caption = "Description", ;
		Height = 17, ;
		Left = 1, ;
		Top = 344, ;
		Width = 596, ;
		BackStyle = 0, ;
		Name = "lblDescription"

	ADD OBJECT txtDescription AS editbox WITH ;
		Anchor = 14, ;
		Height = 92, ;
		Left = 1, ;
		ReadOnly = .T., ;
		ScrollBars = 0, ;
		Top = 363, ;
		Width = 595, ;
		DisabledBackColor = RGB(255,255,255), ;
		DisabledForeColor = RGB(0,0,0), ;
		Name = "txtDescription"
	
	Procedure ExprResizer.MouseMove
		Lparameters nButton, nShift, nXCoord, nYCoord
		If nButton == 1
			With This.Parent
				.bResizerMoved = .T.
			EndWith
            If This.bVertical
                This.Move(Max(This.nMinWidth, Min(This.Parent.Width-This.nMinWidth,nXCoord)),This.Top,This.Width,This.Height)
            Else
                This.Move(This.Left, Max(This.nMinHeight, Min(This.Parent.Height - BTN_H - This.Parent.nBottomMargin ,nYCoord-Sysmetric(9))),This.Width,This.Height)
		EndIf
            This.Parent.Resize()
		This.Parent.bResizerMoved = .F.
        Endif
	EndProc
	
	Proc ExprResizer.DblClick
		This.Parent.bResizerMoved = .T.
		If This.Top < This.Parent.Height - BTN_H - This.Height - This.Parent.nBottomMargin
			This.Move(This.Left, This.Parent.Height - BTN_H - This.Height - This.Parent.nBottomMargin, This.Width, This.Height)
		Else
			With This.Parent
				This.Move(This.Left, .Height - (.txtDescription.Top + .txtDescription.Height - .lblCategories.Top) - BTN_H - .nBottomMargin , This.Width, This.Height)
			EndWith
		EndIf
		This.Parent.Resize()
		This.Parent.bResizerMoved = .F.
	EndProc
	
	Procedure Resize
		If This.bResizerMoved
			Dimension aOldAnchors(This.ControlCount)
			Local oCtrl, nAttCtrlIdx
			FOR nAttCtrlIdx=1 TO This.ControlCount
				aOldAnchors(nAttCtrlIdx) = This.Controls(nAttCtrlIdx).Anchor
				This.Controls(nAttCtrlIdx).Anchor = 0
			NEXT
			With This
				.ExprMemo.Height = .ExprResizer.Top - .ExprMemo.Top
				.lblFunctions.Top = .ExprResizer.Top + .ExprResizer.Height
				.lstFunctions.Top = .lblFunctions.Top + .lblFunctions.Height
				.lblCategories.Top = .ExprResizer.Top + .ExprResizer.Height
				.lstCategories.Top = .lblCategories.Top + .lblCategories.Height
				.lblDescription.Top = .lstCategories.Top + .lstCategories.Height
				.txtDescription.Top = .lblDescription.Top+ .lblDescription.Height
				.lblCategories.Visible = .lblCategories.Top + .Top < .Height - BTN_H - .nBottomMargin
				.lblFunctions.Visible = .lblFunctions.Top + .Top < .Height - BTN_H - .nBottomMargin
			EndWith
			FOR nAttCtrlIdx=1 TO This.ControlCount
				This.Controls(nAttCtrlIdx).Anchor = aOldAnchors(nAttCtrlIdx)
			NEXT
			With This
				If !.txtDescription.Top + .txtDescription.Height >= .Height - BTN_H - .nBottomMargin
					.ExprResizer.nMinHeight = .Height - (.txtDescription.Top + .txtDescription.Height - .lblCategories.Top) - BTN_H - .nBottomMargin
				EndIf
			EndWith
		Else
			DODEFAULT()
			With This
				.ExprResizer.nMinHeight = .Height - (.txtDescription.Top + .txtDescription.Height - .lblCategories.Top) - BTN_H - .nBottomMargin
			EndWith
		EndIf
	EndProc
	
	Procedure lstFunctions.ProgrammaticChange
		This.Parent.Parent.Parent.Parent.UpdateFunctionDescription()
	EndProc
	
	Procedure lstFunctions.InteractiveChange
		This.Parent.Parent.Parent.Parent.UpdateFunctionDescription()
	EndProc
	
	Procedure lstFunctions.MouseUp
		LPARAMETERS nButton, nShift, nXCoord, nYCoord
		This.Parent.Parent.Parent.Parent.ShowFunctionsMenu(nButton, nShift, nXCoord, nYCoord)
	EndProc
	
	Procedure lstFunctions.DblClick
		This.Parent.Parent.Parent.Parent.InsertSelectedFunction()
	EndProc
	
	Procedure lstCategories.ProgrammaticChange
		This.Parent.Parent.Parent.Parent.FillFunctionsList()
	EndProc
	
	Procedure lstCategories.InteractiveChange
		This.Parent.Parent.Parent.Parent.FillFunctionsList()
	EndProc
	
	Proc lstCategories.MouseUp
		LPARAMETERS nButton, nShift, nXCoord, nYCoord
		This.Parent.Parent.Parent.Parent.ShowCategoriesMenu(nButton, nShift, nXCoord, nYCoord)
	EndProc
	
Enddefine

Define Class ExprBuilder As CpSysform
    Width = 600
    Height = 510
    WindowType=1
    AutoCenter = .T.
    Icon=i_cBmpPath+i_cStdIcon
    oParentObject = .Null.
    cComment=''
	Closable = .T.
	KeyPreview = .T.
	cOriginalExpression = ""
	MinWidth = 410
	MinHeight = 380
	cPathFavoritesXml = ''

    oCategoriesCollection = .null.
	
	Dimension aFunctionsList[1,6]
	Dimension aCategoryOrder[1,1]
	
	Add Object __dummy__ As CommandButton With Enabled=.F.,Width=0,Height=0,Style=0,SpecialEffect=1

	ADD OBJECT oPgFrm AS PageFrame WITH ;
		PageCount = 1, ;
		Width = 605, ;
		Height = 485, ;
		Top = 0, ;
		Left = 0, ;
		TabStyle = 1, ;
		Anchor = 15, ;
		Name = "oPgFrm"

	ADD OBJECT cmdHelp AS commandbutton WITH ;
		Top = 485, ;
		Left = 1, ;
		Height = BTN_H, ;
		Width = BTN_W, ;
		Anchor = 6, ;
		Caption = "\<?", ;
		Name = "cmdHelp"

	ADD OBJECT cmdCancel AS commandbutton WITH ;
		Top = 485, ;
		Left = 525, ;
		Height = BTN_H, ;
		Width = BTN_W, ;
		Anchor = 12, ;
		Caption = "Cancel", ;
		Name = "cmdCancel"

	ADD OBJECT cmdOk AS commandbutton WITH ;
		Top = 485, ;
		Left = 445, ;
		Height = BTN_H, ;
		Width = BTN_W, ;
		Anchor = 12, ;
		Caption = "Ok", ;
		Name = "cmdOk"

    Procedure Init(oParentObject)
        * --- La finestra � modale quindi disabilito tutti i tasti funzione
        Push Key Clear
        * --- Valorizzo oParentObject e il valore del campo memo
        With This
			.oParentObject = oParentObject
			.cComment = cp_Translate( MSG_VISUAL_QUERY_FUNCTION )
			.Caption = cp_Translate( MSG_EXPRESSION_BUILDER )
			.oPgFrm.Page1.Caption = cp_Translate( MSG_EXPRESSION_BUILDER )
			#If Version(5)>=900
				.oPgFrm.Page1.AddObject("ImgBackGround", "Image")
				With .oPgFrm.Page1.ImgBackGround
					.Stretch=2
					.Visible=.F.
				EndWith
			#Endif
        If i_VisualTheme<>-1
            If i_bGradientBck
					With .oPgFrm.Page1.ImgBackGround
						.Width = This.oPgFrm.Width
						.Height = This.oPgFrm.Height
						.Anchor = 15 && Resize Width and Height
						.Picture = i_ThemesManager.GetProp(11)
						.ZOrder(1)
						.Visible = .T.
					Endwith
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
				.oPgFrm.Page1.BackColor = i_ThemesManager.GetProp(7)
            Thisform.BackColor = i_ThemesManager.GetProp(7)
			If i_cMenuTab<>"S"
                .oPgFrm.Tabs=.F.
                .oPgFrm.BorderWidth=0
                .AddObject("oTabMenu", "TabMenu", This, .F., .F., "oPgFrm", .T.)
				.oPgFrm.Anchor = 0
                .oPgFrm.Height = .oPgFrm.Height - .oTabMenu.Height
				.oPgFrm.Anchor = 15
            Endif
			Endif
			.oPgFrm.Page1.AddObject("oPag", "_ExpressionEditor")
			With .oPgFrm.Page1.oPag
				.Top = 1
				.Left = 1
				.Width = this.Width
				.Height = this.Height - BTN_H
				.Anchor = 15
				.Visible = .T.
			EndWith

			.cmdOk.Caption = cp_Translate( MSG_OK_BUTTON )
			.cmdCancel.Caption = cp_Translate( MSG_CANCEL_BUTTON )
			.oCategoriesCollection = CREATEOBJECT("collection")
		EndWith
		This.cPathFavoritesXml = ADDBS(sys(2023))
		* Zucchetti Aulla - Inizio percorso temporane ad hoc
		This.cPathFavoritesXml = ADDBS(tempadhoc())
		* Zucchetti Aulla - Fine percorso temporane ad hoc
		This.InitLists()
		This.oPgFrm.Page1.oPag.ExprMemo.Value = This.oParentObject.Value
		This.cOriginalExpression = This.oPgFrm.Page1.oPag.ExprMemo.Value
		This.oPgFrm.Page1.oPag.ExprMemo.SetFocus()
		This.oPgFrm.Page1.oPag.ExprMemo.SelStart=0
    Endproc
	
	Proc InitLists
		Do While This.oCategoriesCollection.Count > 0
			Do While This.oCategoriesCollection.item(1).oFunctionsCollection.Count > 0
				This.oCategoriesCollection.item(1).oFunctionsCollection.Remove(1)
			EndDo
			This.oCategoriesCollection.Remove(1)
		EndDo
		If This.ReadFunctionsFile()
			This.FillCategoriesList()
			This.oPgFrm.Page1.oPag.lstCategories.RowSourceType=5
			This.oPgFrm.Page1.oPag.lstCategories.RowSource = "This.aInfo"
			If This.oPgFrm.Page1.oPag.lstCategories.ListCount > 0
				This.oPgFrm.Page1.oPag.lstCategories.Selected(1) = .T.
				If this.oPgFrm.Page1.oPag.lstFunctions.ListCount < 1 AND This.oPgFrm.Page1.oPag.lstCategories.ListCount > 1
					This.oPgFrm.Page1.oPag.lstCategories.Selected(2) = .T.
				Endif
			Endif
		EndIf
    Endproc
	
	Func CheckBeforeClose
		return this.oPgFrm.Page1.oPag.ExprMemo.Value==this.cOriginalExpression OR cp_YesNo(MSG_DISCARD_CHANGES_QP)
	EndFunc
	
	Proc KeyPress
		LPARAMETERS nKeyCode, nShiftAltCtrl
		If nShiftAltCtrl = 0 AND nKeyCode = 27 AND This.CheckBeforeClose()
			thisform.Release()
		else
			DODEFAULT()
		endif
    Endproc
	
	Proc FillFunctionsList
		local sKeyCategorySelected, nIdxCategorySelected, nIdxAttFunction, oAttCategory, bSourceOk
		sKeyCategorySelected = this.oPgFrm.Page1.oPag.lstCategories.aInfo[ this.oPgFrm.Page1.oPag.lstCategories.ListIndex, ARR_CAT_ORDER ]
		*Recupero l'indice della collezione dalla sua chiave
		nIdxCategorySelected = This.oCategoriesCollection.GetKey( sKeyCategorySelected )
		bSourceOk = .F.
		if This.oCategoriesCollection.item(nIdxCategorySelected).bShowAll
			*Scansione di tutte le categorie e inserimento di tutte le funzioni (tranne le funzioni della lista dei preferiti)
			*Dimensiono l'array della lista delle funzioni a 1 in modo da poterlo dimensionare in base al nuemro di funzioni presenti in seguito
			Dimension this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ 1, 7]
			For Each oAttCategory IN This.oCategoriesCollection
				nIdxAttFunction = 1
				For nIdxAttFunction = 1 TO oAttCategory.oFunctionsCollection.Count
					If !oAttCategory.bFavorites
						this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1), ARR_FUN_ORDER] = oAttCategory.oFunctionsCollection.Getkey(nIdxAttFunction)
						this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1), ARR_FUN_NAME] = oAttCategory.oFunctionsCollection.Item(nIdxAttFunction).sName
						this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1), ARR_FUN_SINTAX] = oAttCategory.oFunctionsCollection.Item(nIdxAttFunction).sSintax
						this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1), ARR_FUN_CURS_POS] = oAttCategory.oFunctionsCollection.Item(nIdxAttFunction).nCursorPos
						this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1), ARR_FUN_DESCRI] = oAttCategory.oFunctionsCollection.Item(nIdxAttFunction).sDescri
						this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1), ARR_FUN_TYPE] = oAttCategory.oFunctionsCollection.Item(nIdxAttFunction).sType
						this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1), ARR_FUN_COLLKEY] = this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1), ARR_FUN_ORDER]
						*Creo lo spazio per un eventuale altra funzione
						Dimension this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1) + 1, 7]
						bSourceOk = .T.
					EndIf
				Next
			Next
			*Alla fine dell'array c'� un elemento vuoto, lo elimino
			Dimension this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1) - 1, 7]
		else
			If This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.Count > 0
				bSourceOk = .T.
				This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.KeySort = 2
				Dimension this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.Count, 7]
				nIdxAttFunction = 1
				For nIdxAttFunction = 1 TO This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.Count
					this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ nIdxAttFunction, ARR_FUN_ORDER] = This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.Getkey(nIdxAttFunction)
					this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ nIdxAttFunction, ARR_FUN_NAME] = This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxAttFunction).sName
					this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ nIdxAttFunction, ARR_FUN_SINTAX] = This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxAttFunction).sSintax
					this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ nIdxAttFunction, ARR_FUN_CURS_POS] = This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxAttFunction).nCursorPos
					this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ nIdxAttFunction, ARR_FUN_DESCRI] = This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxAttFunction).sDescri
					this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ nIdxAttFunction, ARR_FUN_TYPE] = This.oCategoriesCollection.item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxAttFunction).sType
					this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ nIdxAttFunction, ARR_FUN_COLLKEY] = this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ nIdxAttFunction, ARR_FUN_ORDER]
				Next
			EndIf
		endif
		If bSourceOk
			*Ordino per indice espresso nel file xml
			ASORT( this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1, -1, 0)
			*Metto nella prima colonna dell'array il nome della funzione al posto dell'indice in modo da ordinare l'array
			*alfabeticamente per facilitare le ricerche
			Dimension this.oPgFrm.Page1.oPag.lstFunctions.aTmpInfo[ ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1), 2]
			For nIdxField = 1 To ALEN(this.oPgFrm.Page1.oPag.lstFunctions.aInfo, 1)
				this.oPgFrm.Page1.oPag.lstFunctions.aTmpInfo[ nIdxField, 1] = LEFT(this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ nIdxField, 2],254)
				this.oPgFrm.Page1.oPag.lstFunctions.aTmpInfo[ nIdxField, 2] = this.oPgFrm.Page1.oPag.lstFunctions.aTmpInfo[ nIdxField, 1]
			EndFor
			This.oPgFrm.Page1.oPag.lstFunctions.RowSourceType=5
			This.oPgFrm.Page1.oPag.lstFunctions.RowSource = "This.aTmpInfo"
			This.oPgFrm.Page1.oPag.lstFunctions.Selected(1) = .T.
			This.oPgFrm.Page1.oPag.lstFunctions.SetFocus()
		Else
			Dimension this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ 1, 5]
			this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ 1, 1] = .F.
			this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ 1, 2] = ''
			This.oPgFrm.Page1.oPag.lstFunctions.RowSourceType=0
			This.oPgFrm.Page1.oPag.lstFunctions.Clear()
		EndIf
    Endproc
	
	Proc InsertSelectedFunction
		this.InsCmd(this.oPgFrm.Page1.oPag.lstFunctions.aInfo[this.oPgFrm.Page1.oPag.lstFunctions.ListIndex,3], this.oPgFrm.Page1.oPag.lstFunctions.aInfo[this.oPgFrm.Page1.oPag.lstFunctions.ListIndex,4])
    Endproc
	
	Proc UpdateFunctionDescription
     	IF this.oPgFrm.Page1.oPag.lstFunctions.ListIndex>0
		This.oPgFrm.Page1.oPag.txtDescription.Value = this.oPgFrm.Page1.oPag.lstFunctions.aInfo[this.oPgFrm.Page1.oPag.lstFunctions.ListIndex,5]
	Endif
    Endproc
	
	Func GetCategoryOrder
		LPARAMETERS sCategoryName, sDefaultOrder
		local bNotFound, nIdxCurrentCategory, sRetVal
		bNotFound = .T.
		nIdxCurrentCategory = 1
		If TYPE("This.aCategoryOrder[ nIdxCurrentCategory, 2 ]")='C'
			Do While bNotFound AND nIdxCurrentCategory <= ALEN(This.aCategoryOrder, 1)
				bNotFound = This.aCategoryOrder[ nIdxCurrentCategory, ARR_CAT_NAME ] <> sCategoryName
				nIdxCurrentCategory = nIdxCurrentCategory + 1
			EndDo
		EndIf
		If bNotFound
			sRetVal = sDefaultOrder
		Else
			sRetVal = This.aCategoryOrder[ nIdxCurrentCategory - 1, ARR_CAT_ORDER ]
		EndIf
		return sRetVal
	EndFunc
	
	Proc FillCategoriesList
		Dimension this.oPgFrm.Page1.oPag.lstCategories.aInfo[ This.oCategoriesCollection.Count , 2]
		local oCategory, nIdxCurrentCategory
		nIdxCurrentCategory = 1
		This.oCategoriesCollection.KeySort = 2
		FOR nIdxCurrentCategory = 1 TO This.oCategoriesCollection.Count
			this.oPgFrm.Page1.oPag.lstCategories.aInfo[ nIdxCurrentCategory , ARR_CAT_ORDER] = This.oCategoriesCollection.GetKey(nIdxCurrentCategory)
			this.oPgFrm.Page1.oPag.lstCategories.aInfo[ nIdxCurrentCategory , ARR_CAT_NAME] = This.oCategoriesCollection.Item(nIdxCurrentCategory).sName
		NEXT
		ASort( this.oPgFrm.Page1.oPag.lstCategories.aInfo, 1, -1, 0, 1) 
    Endproc
	
	Proc ReadFavoritesFile
		*Funzioni preferite, apro il file dei preferiti se presente e inserisco nell'array delle funzioni il contenuto del file
		Local oMainForm, oCurrentNode
		oMainForm = This.oParentObject.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent
		if Empty(oMainForm.cFavoritesXmlFile) AND cp_FileExist(This.cPathFavoritesXml + "Favorites_QueryFunctionsList.xml")
			oMainForm.cFavoritesXmlFile = FileToStr(This.cPathFavoritesXml + "Favorites_QueryFunctionsList.xml")
		endif
		if !Empty(oMainForm.cFavoritesXmlFile)
			oMainForm.oXmlQueryFunction.loadxml( oMainForm.cFavoritesXmlFile )
			If oMainForm.oXmlQueryFunction.parseError.errorCode = 0
				oCurrentNode = oMainForm.oXmlQueryFunction.documentElement
                                Dimension This.aFunctionsList[ 1 , 6 ]
                                This.aFunctionsList[ 1 , 1 ] = .F.
                                This.aFunctionsList[ 1 , 2 ] = .F.
                                This.aFunctionsList[ 1 , 3 ] = .F.
                                This.aFunctionsList[ 1 , 4 ] = .F.
                                This.aFunctionsList[ 1 , 5 ] = .F.
                                This.aFunctionsList[ 1 , 6 ] = .F.
				If oCurrentNode.getElementsByTagName("Function").length > 0
					Dimension This.aFunctionsList[ oCurrentNode.getElementsByTagName("Function").length, 6 ]
					nFunctionNodeIdx = 0
					Do While nFunctionNodeIdx < oCurrentNode.getElementsByTagName("Function").length
						This.aFunctionsList [ nFunctionNodeIdx + 1 , ARR_FUN_ORDER ] = oCurrentNode.getElementsByTagName("Function/Order").item(nFunctionNodeIdx).text
						This.aFunctionsList [ nFunctionNodeIdx + 1 , ARR_FUN_NAME ] = oCurrentNode.getElementsByTagName("Function/Name").item(nFunctionNodeIdx).text
						This.aFunctionsList [ nFunctionNodeIdx + 1 , ARR_FUN_SINTAX ] = oCurrentNode.getElementsByTagName("Function/Sintax").item(nFunctionNodeIdx).text
						This.aFunctionsList [ nFunctionNodeIdx + 1 , ARR_FUN_CURS_POS ] = Val(oCurrentNode.getElementsByTagName("Function/CursorPosition").item(nFunctionNodeIdx).text)
						This.aFunctionsList [ nFunctionNodeIdx + 1 , ARR_FUN_DESCRI ] = oCurrentNode.getElementsByTagName("Function/Description").item(nFunctionNodeIdx).text
						This.aFunctionsList [ nFunctionNodeIdx + 1 , ARR_FUN_TYPE ] = oCurrentNode.getElementsByTagName("Function/Type").item(nFunctionNodeIdx).text
						nFunctionNodeIdx = nFunctionNodeIdx + 1
					EndDo
				EndIf
				*Verifico se all'interno vi � l'ordinamento personalizzato delle categorie
				Dimension This.aCategoryOrder[ 1, 2 ]
				This.aCategoryOrder[ 1, 1 ] = .F.
				This.aCategoryOrder[ 1, 2 ] = .F.
				If oCurrentNode.getElementsByTagName("Categories/Category").length > 0
					*Memorizzo l'ordinamento personalizzato per applicarlo in seguito
					Dimension This.aCategoryOrder[ oCurrentNode.getElementsByTagName("Categories/Category").length, 2 ]
					nCategoryIdx = 0
					Do While nCategoryIdx < oCurrentNode.getElementsByTagName("Categories/Category").length
						This.aCategoryOrder[ nCategoryIdx + 1, ARR_CAT_ORDER ] = oCurrentNode.getElementsByTagName("Categories/Category/Order").item(nCategoryIdx).text
						This.aCategoryOrder[ nCategoryIdx + 1, ARR_CAT_NAME ] = oCurrentNode.getElementsByTagName("Categories/Category/Name").item(nCategoryIdx).text
						nCategoryIdx = nCategoryIdx + 1
					EndDo
				EndIf
			else
				cp_ErrorMsg("Errore caricamento file delle funzioni preferite",48,"")
			EndIf
		EndIf
    Endproc
	
	Func ReadFunctionsFile
		local cOldErr, bErr, nCurrentNodeIdx, oCurrentNode, nFunctionNodeIdx, nFirstIdxArrayFunction, oMainForm, bRetVal, nCategoryIdx, nIdxCategoryToReorder, aFld
		oMainForm = This.oParentObject.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent
		bRetVal = .F.
		If TYPE('oMainForm.oXmlQueryFunction')='L' AND !oMainForm.bXmlQueryFunctionError
			cOldErr = ON('ERROR')
			bErr=.F.
			ON ERROR bErr=.T.
			oMainForm.oXmlQueryFunction = createobject("Msxml2.DOMDocument")
			If bErr
				oMainForm.bXmlQueryFunctionError = .T.
			EndIf
			ON ERROR &cOldErr
		EndIf
		if Empty(oMainForm.cFunctionsXmlFile)
			oMainForm.cFunctionsXmlFile = FileToStr(ADDBS(ALLTRIM(cPathVfcsim)) + "QueryFunctionsList.xml")
		endif
		If TYPE('oMainForm.oXmlQueryFunction') = 'O'
			*Lettura file tramite oggetto xml
			*Leggo subito il file dei preferiti per recuperare funzioni preferite e ordine categorie se presenti
			This.ReadFavoritesFile()
			oMainForm.oXmlQueryFunction.loadxml( oMainForm.cFunctionsXmlFile )
			If oMainForm.oXmlQueryFunction.parseError.errorCode = 0
				bRetVal = .T.
				nCurrentNodeIdx = 0
				Do While nCurrentNodeIdx < oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category").length
					oCurrentNode = oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category").item(nCurrentNodeIdx)
					With this.oCategoriesCollection
						.Add( CREATEOBJECT("_Category"), This.GetCategoryOrder( oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category/Name").item(nCurrentNodeIdx).text, oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category/Order").item(nCurrentNodeIdx).text ) )
						*Se ho almeno un elemento dell'array dell'ordinamento delle categorie allora valorizzo anche la propriet� tag dell'oggetto per non perdere l'ordinamento
						*delle categorie in caso di riordinamento delle funzioni preferite
						If Type('This.aCategoryOrder[1,1]')='C'
							.Item(.Count).Tag = .GetKey(.Count)
						EndIf
						.Item(.Count).sName = oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category/Name").item(nCurrentNodeIdx).text
						.Item(.Count).sType = oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category/Type").item(nCurrentNodeIdx).text
						.Item(.Count).bFieldList = oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category/FieldsList").item(nCurrentNodeIdx).text == "S"
						.Item(.Count).bFavorites = oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category/Favorites").item(nCurrentNodeIdx).text == "S"
						.Item(.Count).bShowAll = oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category/ShowAllFunctions").item(nCurrentNodeIdx).text == "S"
						.Item(.Count).bShowParameters = oMainForm.oXmlQueryFunction.documentElement.getElementsByTagName("Categories/Category/ShowQueryParams").item(nCurrentNodeIdx).text == "S"
						DO CASE
							CASE .Item(.Count).bFieldList
								*Lista campi della query, inserisco la lista dei campi della query
								aFld = This.oParentObject.Parent.Parent.Parent.Parent.Parent.oPage.Pages(2).opag.fdlfields
								If aFld.ListCount > 0
									nFunctionNodeIdx = 0
									Do While nFunctionNodeIdx < aFld.ListCount
										If .Item(.Count).oFunctionsCollection.Getkey( ALLTRIM( afld.list( nFunctionNodeIdx + 1, 3 ) ) + " [ " + ALLTRIM( afld.list( nFunctionNodeIdx + 1, 2 ) ) + " ]" ) = 0
											.Item(.Count).oFunctionsCollection.Add( CREATEOBJECT("_Function"), ALLTRIM( afld.list( nFunctionNodeIdx + 1, 3 ) ) + " [ " + ALLTRIM( afld.list( nFunctionNodeIdx + 1, 2 ) ) + " ]" )
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sName = ALLTRIM( afld.list( nFunctionNodeIdx + 1, 3 ) ) + " [ " + ALLTRIM( afld.list( nFunctionNodeIdx + 1, 2 ) ) + " ]"
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sSintax = Alltrim(aFld.List( nFunctionNodeIdx + 1, 2) )
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).nCursorPos = Len( Alltrim(aFld.List( nFunctionNodeIdx + 1, 2) ) )
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sDescri = Alltrim(aFld.List( nFunctionNodeIdx + 1, 1) )
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sType = "S"
										EndIf
										nFunctionNodeIdx = nFunctionNodeIdx + 1
									EndDo
								Endif
							CASE .Item(.Count).bFavorites
								*Funzioni preferite, leggo l'array delle funzioni preferite
								nFunctionNodeIdx = 1
								If Type("this.aFunctionsList[ nFunctionNodeIdx, 2 ]")='C'
									Do While nFunctionNodeIdx <= ALEN(this.aFunctionsList, 1)
										If .Item(.Count).oFunctionsCollection.Getkey( this.aFunctionsList[ nFunctionNodeIdx, ARR_FUN_ORDER ] ) = 0
											.Item(.Count).oFunctionsCollection.Add( CREATEOBJECT("_Function"), this.aFunctionsList[ nFunctionNodeIdx, ARR_FUN_ORDER ] )
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sName = this.aFunctionsList[ nFunctionNodeIdx, ARR_FUN_NAME ]
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sSintax = this.aFunctionsList[ nFunctionNodeIdx, ARR_FUN_SINTAX ]
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).nCursorPos = this.aFunctionsList[ nFunctionNodeIdx, ARR_FUN_CURS_POS ]
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sDescri = this.aFunctionsList[ nFunctionNodeIdx, ARR_FUN_DESCRI ]
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sType = this.aFunctionsList[ nFunctionNodeIdx, ARR_FUN_TYPE ]
										Endif
										nFunctionNodeIdx = nFunctionNodeIdx + 1
									EndDo
								EndIf
							CASE .Item(.Count).bShowAll
								*Mostra tutte le funzioni disponibili, non aggiungo le funzioni, quando verr�
								*selezionata questa categoria tutte le funzioni delle altre categorie
								*escluse le funzioni preferite verranno aggiunte alla lista
							CASE .Item(.Count).bShowParameters
								*Mostra i parametri della query
								aFld = This.oParentObject.Parent.Parent.Parent.Parent.Parent.oPage.Pages(5).opag.fplparam
								If aFld.ListCount > 0
									nFunctionNodeIdx = 0
									Do While nFunctionNodeIdx < aFld.ListCount
										If .Item(.Count).oFunctionsCollection.Getkey( ALLTRIM( afld.list( nFunctionNodeIdx + 1, 1 ) ) + " [ " + ALLTRIM( afld.list( nFunctionNodeIdx + 1, 2 ) ) + " ]" ) = 0
											.Item(.Count).oFunctionsCollection.Add( CREATEOBJECT("_Function"), ALLTRIM( afld.list( nFunctionNodeIdx + 1, 1 ) ) + " [ " + ALLTRIM( afld.list( nFunctionNodeIdx + 1, 2 ) ) + " ]" )
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sName = ALLTRIM( afld.list( nFunctionNodeIdx + 1, 1 ) ) + " [ " + ALLTRIM( afld.list( nFunctionNodeIdx + 1, 2 ) ) + " ]"
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sSintax = "?"+Alltrim(aFld.List( nFunctionNodeIdx + 1, 1) )
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).nCursorPos = Len( Alltrim(aFld.List( nFunctionNodeIdx + 1, 1) ) ) + 1
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sDescri = Alltrim(aFld.List( nFunctionNodeIdx + 1, 2) )
											.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sType = "S"
										Endif
										nFunctionNodeIdx = nFunctionNodeIdx + 1
									EndDo
								Endif
							OTHERWISE
								* Inserisco nella collezione le funzioni della categoria corrente
								nFunctionNodeIdx = 0
								Do While nFunctionNodeIdx < oCurrentNode.getElementsByTagName("Function").length
									.Item(.Count).oFunctionsCollection.Add( CREATEOBJECT("_Function"), oCurrentNode.getElementsByTagName("Function/Order").item(nFunctionNodeIdx).text )
									.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sName = oCurrentNode.getElementsByTagName("Function/Name").item(nFunctionNodeIdx).text
									.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sSintax = oCurrentNode.getElementsByTagName("Function/Sintax").item(nFunctionNodeIdx).text
									.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).nCursorPos = Val(oCurrentNode.getElementsByTagName("Function/CursorPosition").item(nFunctionNodeIdx).text)
									.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sDescri = oCurrentNode.getElementsByTagName("Function/Description").item(nFunctionNodeIdx).text
									.Item(.Count).oFunctionsCollection.Item( .Item(.Count).oFunctionsCollection.Count ).sType = oCurrentNode.getElementsByTagName("Function/Type").item(nFunctionNodeIdx).text
									nFunctionNodeIdx = nFunctionNodeIdx + 1
								EndDo
						ENDCASE
					endwith
					nCurrentNodeIdx = nCurrentNodeIdx + 1
				EndDo
			else
				cp_ErrorMsg("Errore caricamento file delle funzioni",48,"")
			endif
		else
			*Lettura file tramite parsing del testo
			cp_ErrorMsg("Not yet implemented",48,"")
		EndIf
		return bRetVal
	EndFunc

	Proc ShowFunctionsMenu
		LPARAMETERS nButton, nShift, nXCoord, nYCoord
		Local loPopupMenu, loMenuItem, cSelectedMenuItem, cFunctionToExec, sKeyCategorySelected, nIdxCategorySelected, sKeyFunctionSelected, nIdxFunctionSelected, nIdxAttCategory, nIdxAttFunction, nMaxFunctionsOrder, sKeyDestination, nIdxDestination, nIdxAttFunction, sKeyAttFunction, nOldCategoryListIndex, nOldFunctionListIndex
		LOCAL l_iMenu
		l_iMenu = 1
		If nButton=2 AND nShift = 0 AND this.oPgFrm.Page1.oPag.lstFunctions.ListIndex>0
			cSelectedMenuItem = Sys(2015)
			Public &cSelectedMenuItem
			&cSelectedMenuItem = ""
			If i_VisualTheme<>-1
				*--- Creo nuovo popup
				loPopupMenu = Createobject("cbPopupMenu")
			Else
				Define Popup loPopupMenu From Mrow(),Mcol() shortcut Margin
			Endif
			*Verifico se sono nella categoria preferiti per mostrare il men�
			*rimuovi dai preferiti, sposta in alto, sposta in basso, seleziona
			*Se sono in qualsiasi altra categoria mostro solo il men�
			*aggiungi ai preferiti e seleziona
			sKeyCategorySelected = this.oPgFrm.Page1.oPag.lstCategories.aInfo[ this.oPgFrm.Page1.oPag.lstCategories.ListIndex, ARR_CAT_ORDER ]
			nIdxCategorySelected = this.oCategoriesCollection.GetKey(sKeyCategorySelected)
			sKeyFunctionSelected = this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ this.oPgFrm.Page1.oPag.lstFunctions.ListIndex, ARR_FUN_COLLKEY ]
			nIdxFunctionSelected = this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.GetKey(sKeyFunctionSelected)
			nOldCategoryListIndex = This.oPgFrm.Page1.oPag.lstCategories.ListIndex
			nOldFunctionListIndex = This.oPgFrm.Page1.oPag.lstFunctions.ListIndex
			If this.oCategoriesCollection.Item(nIdxCategorySelected).bFavorites
				If i_VisualTheme <> -1
					If This.oPgFrm.Page1.oPag.lstFunctions.ListIndex > 1
						loMenuItem = m.loPopupMenu.AddMenuItem()
						loMenuItem.Caption = cp_Translate(MSG_FAVORITES_MOVEUP)
						loMenuItem.BeginGroup = .F.
						loMenuItem.Visible = .T.
						loMenuItem.OnClick = cSelectedMenuItem + [= 'U']
					EndIf
					If this.oPgFrm.Page1.oPag.lstFunctions.ListIndex < This.oPgFrm.Page1.oPag.lstFunctions.ListCount
						loMenuItem = m.loPopupMenu.AddMenuItem()
						loMenuItem.Caption = cp_Translate(MSG_FAVORITES_MOVEDOWN)
						loMenuItem.BeginGroup = .F.
						loMenuItem.Visible = .T.
						loMenuItem.OnClick = cSelectedMenuItem + [= 'D']
					EndIf
					loMenuItem = m.loPopupMenu.AddMenuItem()
					loMenuItem.Caption = cp_Translate(MSG_FAVORITES_REMOVE)
					loMenuItem.BeginGroup = .T.
					loMenuItem.Visible = .T.
					loMenuItem.OnClick = cSelectedMenuItem + [= 'R']
				Else
					If This.oPgFrm.Page1.oPag.lstFunctions.ListIndex > 1
						Define Bar l_iMenu Of loPopupMenu Prompt cp_Translate(MSG_FAVORITES_MOVEUP)
						On Sele Bar l_iMenu Of loPopupMenu cSelectedMenuItem='U'
						l_iMenu = l_iMenu + 1
					EndIf
					If this.oPgFrm.Page1.oPag.lstFunctions.ListIndex < This.oPgFrm.Page1.oPag.lstFunctions.ListCount
						Define Bar l_iMenu Of loPopupMenu Prompt cp_Translate(MSG_FAVORITES_MOVEDOWN)
						On Sele Bar l_iMenu Of loPopupMenu cSelectedMenuItem='D'
						l_iMenu = l_iMenu + 1
					EndIf
					Define Bar l_iMenu Of loPopupMenu Prompt "\-"
					l_iMenu = l_iMenu + 1
					Define Bar l_iMenu Of loPopupMenu Prompt cp_Translate(MSG_FAVORITES_REMOVE)
					On Sele Bar l_iMenu Of loPopupMenu cSelectedMenuItem='R'
                                        l_iMenu = l_iMenu + 1
				Endif
			Else
				If i_VisualTheme <> -1
					loMenuItem = m.loPopupMenu.AddMenuItem()
					loMenuItem.Caption = cp_Translate(MSG_FAVORITES_ADD)
					loMenuItem.BeginGroup = .F.
					loMenuItem.Visible = .T.
					loMenuItem.OnClick = cSelectedMenuItem + [= 'A']
				Else
					Define Bar l_iMenu Of loPopupMenu Prompt cp_Translate(MSG_FAVORITES_ADD)
					On Sele Bar l_iMenu Of loPopupMenu cSelectedMenuItem='A'
					l_iMenu = l_iMenu + 1
				EndIf
			EndIf
			If i_VisualTheme <> -1
				loMenuItem = m.loPopupMenu.AddMenuItem()
				loMenuItem.Caption = cp_Translate(MSG_FAVORITES_SELECT)
				loMenuItem.BeginGroup = .T.
				loMenuItem.Visible = .T.
				loMenuItem.OnClick = cSelectedMenuItem + [= 'S']
			Else
				Define Bar l_iMenu Of loPopupMenu Prompt cp_Translate(MSG_FAVORITES_SELECT)
				On Sele Bar l_iMenu Of loPopupMenu cSelectedMenuItem = 'S'
				l_iMenu = l_iMenu + 1
			EndIf
			*Visualizzo il men�
			If i_VisualTheme <> -1
				loPopupMenu.InitMenu(.T.)
				loPopupMenu.ShowMenu()
			Else
				Activate Popup loPopupMenu
				Deactivate Popup loPopupMenu
			Endif
			If i_VisualTheme <> -1
			cFunctionToExec = &cSelectedMenuItem
			  Release &cSelectedMenuItem
		        ELSE
		    	  cFunctionToExec = cSelectedMenuItem
			ENDIF 
			DO CASE
				CASE cFunctionToExec=='U' OR cFunctionToExec=='D'
					*Sposta su/gi� (Aumenta/Diminuisce priorit� ordinamento funzione nella lista delle funzioni preferite)
					sKeyDestination = this.oPgFrm.Page1.oPag.lstFunctions.aInfo[ this.oPgFrm.Page1.oPag.lstFunctions.ListIndex + IIF(cFunctionToExec=='U', -1, 1), ARR_FUN_COLLKEY ]
					nIdxDestination = this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.GetKey(sKeyDestination)
					*Riassegno i valori di ordinamento sfruttango la propriet� Tag dell'oggetto collection poich�
					*l'ordinamento � posizionato nella propriet� key che non � modificabile (Chiave della collezione)
					For nIdxAttFunction=1 to this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Count
						sKeyAttFunction = this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.GetKey(nIdxAttFunction)
						DO CASE
							CASE sKeyAttFunction==sKeyFunctionSelected
								This.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxAttFunction).Tag = sKeyDestination
							CASE sKeyAttFunction==sKeyDestination
								This.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxAttFunction).Tag = sKeyFunctionSelected
							OTHERWISE
								This.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxAttFunction).Tag = sKeyAttFunction
						ENDCASE
					Next
					nOldFunctionListIndex = nOldFunctionListIndex + IIF(cFunctionToExec=='U', -1, 1)
				CASE cFunctionToExec=='R'
					*Rimuovi (Rimuove la funzione dalla lista delle funzioni preferite)
					this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Remove(nIdxFunctionSelected)
					nOldFunctionListIndex = MAX(nOldFunctionListIndex - 1, 1)
				CASE cFunctionToExec=='A'
					*Aggiungi (Aggiunge la funzione alla lista delle funzioni preferite)
					*Ricerco la categoria delle funzioni preferite
					nIdxAttCategory = 1
					Do While nIdxAttCategory <= this.oCategoriesCollection.Count AND !this.oCategoriesCollection.Item(nIdxAttCategory).bFavorites
						nIdxAttCategory = nIdxAttCategory + 1
					EndDo
					*Recuper il valore massimo delle funzioni preferite
					nMaxFunctionsOrder = 0
					For nIdxAttFunction = 1 To this.oCategoriesCollection.Item(nIdxAttCategory).oFunctionsCollection.Count
						nMaxFunctionsOrder = MAX(nMaxFunctionsOrder, Val(this.oCategoriesCollection.Item(nIdxAttCategory).oFunctionsCollection.GetKey(nIdxAttFunction) ))
					Next
					nMaxFunctionsOrder = nMaxFunctionsOrder + 10
					*Aggiungo alla collezione delle funzioni preferite la funzione selezionata
					With this.oCategoriesCollection.Item(nIdxAttCategory).oFunctionsCollection
						.Add( CREATEOBJECT("_Function"), ALLTRIM(STR(nMaxFunctionsOrder)) )
						If this.oCategoriesCollection.Item(nIdxCategorySelected).bShowAll
							.Item( .Count ).sName = this.oPgFrm.Page1.oPag.lstFUNCTIONS.ainfo(this.oPgFrm.Page1.oPag.lstFUNCTIONS.ListIndex,2)
							.Item(.Count).sSintax = this.oPgFrm.Page1.oPag.lstFUNCTIONS.ainfo(this.oPgFrm.Page1.oPag.lstFUNCTIONS.ListIndex,3)
							.Item(.Count).nCursorPos = this.oPgFrm.Page1.oPag.lstFUNCTIONS.ainfo(this.oPgFrm.Page1.oPag.lstFUNCTIONS.ListIndex,4)
							.Item(.Count).sDescri = this.oPgFrm.Page1.oPag.lstFUNCTIONS.ainfo(this.oPgFrm.Page1.oPag.lstFUNCTIONS.ListIndex,5)
							.Item(.Count).sType = this.oPgFrm.Page1.oPag.lstFUNCTIONS.ainfo(this.oPgFrm.Page1.oPag.lstFUNCTIONS.ListIndex,6)
						Else
							.Item( .Count ).sName = this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxFunctionSelected).sName
							.Item(.Count).sSintax = this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxFunctionSelected).sSintax
							.Item(.Count).nCursorPos = this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxFunctionSelected).nCursorPos
							.Item(.Count).sDescri = this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxFunctionSelected).sDescri
							.Item(.Count).sType = this.oCategoriesCollection.Item(nIdxCategorySelected).oFunctionsCollection.Item(nIdxFunctionSelected).sType
						EndIf
					EndWith
				CASE cFunctionToExec=='S'
					*Seleziona la funzione e la aggiunge all'espressione
					This.InsertSelectedFunction()
			ENDCASE
			
			If cFunctionToExec=='U' OR cFunctionToExec=='D' OR cFunctionToExec=='A' OR cFunctionToExec=='R'
				this.WriteFavoritesFile()
				This.InitLists()
				This.oPgFrm.Page1.oPag.lstCategories.ListIndex = nOldCategoryListIndex
				This.oPgFrm.Page1.oPag.lstFunctions.ListIndex = nOldFunctionListIndex
			EndIf
		EndIf
    Endproc
	
	Proc ShowCategoriesMenu
		LPARAMETERS nButton, nShift, nXCoord, nYCoord
		Local loPopupMenu, loMenuItem, cSelectedMenuItem, cFunctionToExec, sKeyCategorySelected, nFirstIdxArrayFunction, nIdxAttCategory, sKeyAttCategory, nOldListIndex
		LOCAL l_iMenu 
		l_iMenu = 1
		If nButton=2 AND nShift = 0
			cSelectedMenuItem = Sys(2015)
			Public &cSelectedMenuItem
			&cSelectedMenuItem = ""
			If i_VisualTheme<>-1
				*--- Creo nuovo popup
				loPopupMenu = Createobject("cbPopupMenu")
			Else
				Define Popup loPopupMenu From Mrow(),Mcol() shortcut Margin
			Endif
			If i_VisualTheme <> -1
				If This.oPgFrm.Page1.oPag.lstCategories.ListIndex > 1
					loMenuItem = m.loPopupMenu.AddMenuItem()
					loMenuItem.Caption = cp_Translate(MSG_FAVORITES_MOVEUP)
					loMenuItem.BeginGroup = .F.
					loMenuItem.Visible = .T.
					loMenuItem.OnClick = cSelectedMenuItem + [= 'U']
				EndIf
				If this.oPgFrm.Page1.oPag.lstCategories.ListIndex < This.oPgFrm.Page1.oPag.lstCategories.ListCount
					loMenuItem = m.loPopupMenu.AddMenuItem()
					loMenuItem.Caption = cp_Translate(MSG_FAVORITES_MOVEDOWN)
					loMenuItem.BeginGroup = .F.
					loMenuItem.Visible = .T.
					loMenuItem.OnClick = cSelectedMenuItem + [= 'D']
				EndIf
			Else
				If This.oPgFrm.Page1.oPag.lstCategories.ListIndex > 1
					Define Bar l_iMenu Of loPopupMenu Prompt cp_Translate(MSG_FAVORITES_MOVEUP)
					On Sele Bar l_iMenu Of loPopupMenu cSelectedMenuItem='U'
					l_iMenu = l_iMenu + 1
				EndIf
				If this.oPgFrm.Page1.oPag.lstCategories.ListIndex < This.oPgFrm.Page1.oPag.lstCategories.ListCount
					Define Bar l_iMenu Of loPopupMenu Prompt cp_Translate(MSG_FAVORITES_MOVEDOWN)
					On Sele Bar l_iMenu Of loPopupMenu cSelectedMenuItem='D'
					l_iMenu = l_iMenu + 1
				EndIf
			Endif
			*Visualizzo il men�
			If i_VisualTheme <> -1
				loPopupMenu.InitMenu(.T.)
				loPopupMenu.ShowMenu()
			Else
				Activate Popup loPopupMenu
				Deactivate Popup loPopupMenu
			Endif
			If i_VisualTheme <> -1
			  cFunctionToExec = &cSelectedMenuItem
			  Release &cSelectedMenuItem
		        ELSE
		    	  cFunctionToExec = cSelectedMenuItem
			ENDIF 			
			If cFunctionToExec=='U' OR cFunctionToExec=='D'
				sKeyCategorySelected = this.oPgFrm.Page1.oPag.lstCategories.aInfo[ this.oPgFrm.Page1.oPag.lstCategories.ListIndex, ARR_CAT_ORDER ]
				nIdxCategorySelected = this.oCategoriesCollection.GetKey(sKeyCategorySelected)
				sKeyDestinationCategory = this.oPgFrm.Page1.oPag.lstCategories.aInfo[ this.oPgFrm.Page1.oPag.lstCategories.ListIndex + IIF(cFunctionToExec=='U', -1, 1), ARR_CAT_ORDER ]
				nIdxDestinationCategory = this.oCategoriesCollection.GetKey(sKeyDestinationCategory)
				*Riassegno i valori di ordinamento sfruttango la propriet� Tag dell'oggetto collection poich�
				*l'ordinamento � posizionato nella propriet� key che non � modificabile (Chiave della collezione)
				For nIdxAttCategory=1 to This.oCategoriesCollection.Count
					sKeyAttCategory = This.oCategoriesCollection.GetKey(nIdxAttCategory)
					DO CASE
						CASE sKeyAttCategory==sKeyCategorySelected
							This.oCategoriesCollection.Item(nIdxAttCategory).Tag = sKeyDestinationCategory
						CASE sKeyAttCategory==sKeyDestinationCategory
							This.oCategoriesCollection.Item(nIdxAttCategory).Tag = sKeyCategorySelected
						OTHERWISE
							This.oCategoriesCollection.Item(nIdxAttCategory).Tag = sKeyAttCategory
					ENDCASE
				Next
				nOldListIndex = this.oPgFrm.Page1.oPag.lstCategories.ListIndex + IIF(cFunctionToExec=='U', -1, 1)
				this.WriteFavoritesFile()
				This.InitLists()
				This.oPgFrm.Page1.oPag.lstCategories.ListIndex = m.nOldListIndex
			EndIf
		EndIf
    Endproc
	
	Proc WriteFavoritesFile
		LPARAMETERS bParOrderCategories
		Local oMainForm, sFavoritesXmlFileContent, cCrLf, nIdxCategory, nIdxFunction, sCategoriesPartXmlFile, sFunctionsPartXmlFile, nIdxAttCategory, oAttFunction, nIdxAttFunction
		oMainForm = This.oParentObject.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent
		cCrLf = Chr(13)+Chr(10)
		sFavoritesXmlFileContent = '<?xml version="1.0" encoding="ISO-8859-1"?>' + cCrLf
		sFavoritesXmlFileContent = sFavoritesXmlFileContent + "<!-- Type field can be: S = Standard, A = ad hoc, P = Personalized -->" + cCrLf
		sFavoritesXmlFileContent = sFavoritesXmlFileContent + '<!-- Each tag "Order" must be unique -->' + cCrLf
		sFavoritesXmlFileContent = sFavoritesXmlFileContent + "<!-- Each category must have unique name -->" + cCrLf
		sFavoritesXmlFileContent = sFavoritesXmlFileContent + "<QueryExpressionBuilder>" + cCrLf
		sFavoritesXmlFileContent = sFavoritesXmlFileContent + Chr(9) +  "<Version>1.0</Version>" + cCrLf
		sCategoriesPartXmlFile = ""
		sFunctionsPartXmlFile = ""
		For nIdxAttCategory=1 to This.oCategoriesCollection.Count
			If !EMPTY(This.oCategoriesCollection.Item(nIdxAttCategory).Tag)
				If nIdxAttCategory=1
					sCategoriesPartXmlFile = Chr(9) + "<Categories>" + cCrLf
				EndIf
				sCategoriesPartXmlFile = sCategoriesPartXmlFile + Chr(9) + Chr(9) + "<Category>" + cCrLf
				sCategoriesPartXmlFile = sCategoriesPartXmlFile + Chr(9) + Chr(9) + Chr(9) + "<Name>" + This.oCategoriesCollection.Item(nIdxAttCategory).sName + "</Name>" + cCrLf
				sCategoriesPartXmlFile = sCategoriesPartXmlFile + Chr(9) + Chr(9) + Chr(9) + "<Order>" + This.oCategoriesCollection.Item(nIdxAttCategory).Tag + "</Order>" + cCrLf
				sCategoriesPartXmlFile = sCategoriesPartXmlFile + Chr(9) + Chr(9) + "</Category>" + cCrLf
			EndIf
			If This.oCategoriesCollection.Item(nIdxAttCategory).bFavorites
				For nIdxAttFunction=1 To This.oCategoriesCollection.Item(nIdxAttCategory).oFunctionsCollection.Count
					oAttFunction = This.oCategoriesCollection.Item(nIdxAttCategory).oFunctionsCollection.Item(nIdxAttFunction)
					If Empty(sFunctionsPartXmlFile)
						sFunctionsPartXmlFile = Chr(9) + "<Functions>" + cCrLf
					EndIf
					sFunctionsPartXmlFile = sFunctionsPartXmlFile + Chr(9) + Chr(9) +  "<Function>" + cCrLf
					sFunctionsPartXmlFile = sFunctionsPartXmlFile + Chr(9) + Chr(9) + Chr(9) +  "<Name>" + oAttFunction.sName +  "</Name>" + cCrLf
					sFunctionsPartXmlFile = sFunctionsPartXmlFile + Chr(9) + Chr(9) + Chr(9) +  "<Sintax>" + oAttFunction.sSintax +  "</Sintax>" + cCrLf
					sFunctionsPartXmlFile = sFunctionsPartXmlFile + Chr(9) + Chr(9) + Chr(9) +  "<CursorPosition>" + ALLTRIM(STR(oAttFunction.nCursorPos )) +  "</CursorPosition>" + cCrLf
					sFunctionsPartXmlFile = sFunctionsPartXmlFile + Chr(9) + Chr(9) + Chr(9) +  "<Description>" + oAttFunction.sDescri +  "</Description>" + cCrLf
					*Se ho valorizzato la propriet� tag dell'oggetto funzione della collezione sto riordinando le funzioni, altrimenti 
					*prelevo la chiave della collezione che rappresenta gi� l'ordinamento
					sFunctionsPartXmlFile = sFunctionsPartXmlFile + Chr(9) + Chr(9) + Chr(9) +  "<Order>" + RIGHT("0000"+ALLTRIM(EVL(oAttFunction.Tag, This.oCategoriesCollection.Item(nIdxAttCategory).oFunctionsCollection.GetKey(nIdxAttFunction) )), 4) +  "</Order>" + cCrLf
					sFunctionsPartXmlFile = sFunctionsPartXmlFile + Chr(9) + Chr(9) + Chr(9) +  "<Type>" + oAttFunction.sType +  "</Type>" + cCrLf
					sFunctionsPartXmlFile = sFunctionsPartXmlFile + Chr(9) + Chr(9) +  "</Function>" + cCrLf
				Next
				If !Empty(sFunctionsPartXmlFile)
					sFunctionsPartXmlFile = sFunctionsPartXmlFile + Chr(9) + "</Functions>" + cCrLf
				EndIf
			EndIf
		Next
		If !EMPTY(sCategoriesPartXmlFile)
			sCategoriesPartXmlFile = sCategoriesPartXmlFile + Chr(9) + "</Categories>" + cCrLf
		EndIf
		sFavoritesXmlFileContent = sFavoritesXmlFileContent + sFunctionsPartXmlFile + sCategoriesPartXmlFile
		sFavoritesXmlFileContent = sFavoritesXmlFileContent + "</QueryExpressionBuilder>"
		StrToFile(sFavoritesXmlFileContent, This.cPathFavoritesXml + "Favorites_QueryFunctionsList.xml")
		oMainForm.cFavoritesXmlFile = sFavoritesXmlFileContent
    Endproc

    Procedure Activate()
        i_curform=This
    Endproc

    Function  GetHelpFile()
        Return(i_CpDic)
    Function  GetHelpTopic()
        Return(This.cComment)

    * --- gestione bottoni Ok, Annulla, Help
    Procedure cmdOK.Click()
        This.Parent.oParentObject.Value = Chrtran(Alltrim(This.Parent.oPgFrm.Page1.oPag.ExprMemo.Value), Chr(10)+Chr(13), '')
        This.Parent.Release()
    Endproc
	
    Procedure cmdCancel.Click()
        This.Parent.Release()
    Endproc
	
    Procedure cmdHelp.Click()
        cp_Help()
    Endproc

    Procedure QueryUnload()
		If !This.CheckBeforeClose()
			nodefault
		else
        * --- Ripristino i tasti funzione
        Pop Key
        DoDefault()
		Endif
        
    Procedure Release()
        * --- Ripristino i tasti funzione
        Pop Key
        DoDefault()
    Endproc

    * --- cCmd = testo funzione, nPos = posizione in cui inserire il parametro
    Procedure InsCmd(cCmd, nPos)
		l_Pos = This.oPgFrm.Page1.oPag.ExprMemo.SelStart+1
        l_Len = This.oPgFrm.Page1.oPag.ExprMemo.SelLength
        If Not Empty(This.oPgFrm.Page1.oPag.ExprMemo.SelText)
            cCmd = Stuff(cCmd, nPos+1, 0, This.oPgFrm.Page1.oPag.ExprMemo.SelText)
            l_NewPos = l_Pos + Len(cCmd) - 1
        Else
            If nPos <> 0
                l_NewPos = l_Pos + nPos - 1
            Else
                l_NewPos = l_Pos + Len(cCmd) - 1
            Endif
        Endif
        This.oPgFrm.Page1.oPag.ExprMemo.Value = Stuff(This.oPgFrm.Page1.oPag.ExprMemo.Value, l_Pos, l_Len, cCmd)
        This.oPgFrm.Page1.oPag.ExprMemo.SelStart = l_NewPos
        This.oPgFrm.Page1.oPag.ExprMemo.SetFocus()
    Endproc

Enddefine && ExprBuilder
