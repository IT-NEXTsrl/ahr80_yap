Local o_miooggetto
cp_LoadExtDict() && carica l'array pubblico i_extdict con le info sulle tabelle con campi aggiunti
o_miooggetto = Createobject("MonitorAddField")
o_miooggetto.Show()
o_miooggetto = .Null.

Define Class MonitorAddField As Form
    Top=100
    Left=100
    Width=300
    Height=300
    Caption="Monitor Add Fields"
    Add Object ListAddField As ListBox With Top=5,Left=5,Width=290,Height=268,ColumnCount=1,ColumnWidths="289",columntitles="Table Name"
    Add Object okbtn  As CommandButton With Caption="Ok",Top=275,Left=210,Width=30,Height=23
    Add Object cancelbtn  As CommandButton With Caption="Cancel",Top=275,Left=245,Width=50,Height=23
    WindowType=1

    Procedure Init()
        This.FillFields()
        This.Caption="Select Table"
    Endproc

    Procedure FillFields()
        Local i_tablecount, i_tablelist
        i_tablecount=1
        i_tablelist = Alltrim("")
        Do While i_tablecount<=Alen(i_extdict.ftbls) And !Empty(i_extdict.ftbls[i_tablecount])
            This.ListAddField.AddItem(Alltrim(i_extdict.ftbls[i_tablecount]))
            i_tablecount = i_tablecount + 1
        Enddo
        Return
    Endproc

    Procedure ListAddField.DblClick()
        Local i_i,i_j
        i_j=0
        For i_i=1 To This.ListCount
            If This.Selected(i_i)
                i_j=i_i
            Endif
        Next
        If i_j<>0
            Do addfieldstable In vrt_addfield With i_extdict.ftbls[i_j]
        Endif
        Return
    Endproc

    Procedure okbtn.Click()
        This.Parent.ListAddField.DblClick()
    Endproc

    Procedure cancelbtn.Click()
        This.Parent.Release()
    Endproc

Enddefine



