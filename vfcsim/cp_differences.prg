* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_differences                                                  *
*              Differences                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-04-19                                                      *
* Last revis.: 2009-08-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_differences",oParentObject))

* --- Class definition
define class tcp_differences as StdForm
  Top    = 4
  Left   = 8

  * --- Standard Properties
  Width  = 601
  Height = 508
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-08-04"
  HelpContextID=194295003
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_differences"
  cComment = "Differences"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MEMO_DIFF = space(0)
  w_CURFLORGNAM = space(254)
  w_FILE_DIFF = space(254)
  w_FIND_FILE_C = space(1)
  w_FIND_FILE_U = space(1)
  w_FILE1 = space(254)
  w_FILE2 = space(254)
  w_EDIT_COMBO_C = .F.
  w_EDIT_COMBO_U = .F.
  * --- Area Manuale = Declare Variables
  * --- cp_differences
  *--- Funzione di ricerca del secondo file per fare le differenze
  *--- filename1: nome completo del file con estensione e percorso relativo
  FUNCTION SearchFileToDiff(filename1,searchfolder)
     LOCAL filetocompare1, filetocompare2, tempuser1, tempuser2, tempcust1, tempcust2
     filetocompare1=""
     filetocompare2=""
     tempuser1=""
     tempuser2=""
     tempcust1=""
     tempcust2=""
     DO CASE
        CASE searchfolder=="C" && cerco un file in user e in standard
           tempuser1 = cp_monitor_SearchUserDir(ALLTRIM(cp_monitor_GetFilename(filename1))+"."+ALLTRIM(JUSTEXT(filename1)))
           tempuser2 = cp_monitor_SearchUserDir(ALLTRIM(JUSTFNAME(filename1)))
  
           filetocompare2 = cp_monitor_SearchStd(ALLTRIM(cp_monitor_GetFilename(filename1))+"."+ALLTRIM(JUSTEXT(filename1)))
  
           IF EMPTY(tempuser1) AND !EMPTY(tempuser2)
              filetocompare1=ALLTRIM(tempuser2)
           ELSE
              IF !EMPTY(tempuser1) AND EMPTY(tempuser2)
                 filetocompare1=ALLTRIM(tempuser1)
              ELSE
                 IF !EMPTY(tempuser1) AND !EMPTY(tempuser2)
                    filetocompare1 = ALLTRIM(tempuser2)
                 ENDIF
              ENDIF
           ENDIF
  
        CASE searchfolder=="U" && cerco un file in custom e in standard
           tempcust1 = cp_monitor_SearchCustom(ALLTRIM(cp_monitor_GetFilename(filename1))+"."+ALLTRIM(JUSTEXT(filename1)))
           tempcust2 = cp_monitor_SearchCustom(ALLTRIM(JUSTFNAME(filename1)))
  
           filetocompare2 = cp_monitor_SearchStd(ALLTRIM(cp_monitor_GetFilename(filename1))+"."+ALLTRIM(JUSTEXT(filename1)))
  
           IF EMPTY(tempcust1) AND !EMPTY(tempcust2)
              filetocompare1=ALLTRIM(tempcust2)
           ELSE
              IF !EMPTY(tempcust1) AND EMPTY(tempcust2)
                 filetocompare1=ALLTRIM(tempcust1)
              ELSE
                 IF !EMPTY(tempcust1) AND !EMPTY(tempcust2)
                    filetocompare1 = ALLTRIM(tempcust2)
                 ENDIF
              ENDIF
           ENDIF
     ENDCASE
  
     RETURN ALLTRIM(filetocompare1)+"#"+ALLTRIM(filetocompare2)
  ENDFUNC
  
  *--- Restituisce le differenze tra due file framework
  Procedure MakeDiff(pFile1, pFile2)
     local l_ObjDiff, L_OldErr, L_Err, l_cDiff
     L_Err = .F.
     l_ObjDiff = .null.
     L_OldErr = ON('ERROR')
     ON ERROR L_Err=.T.
     l_ObjDiff = CREATEOBJECT("cpdiff.DocGenerator")
     If IsNull(l_ObjDiff) or L_Err
       cp_ErrorMsg("Errore creazione oggetto differenze")
       return
     Endif
     l_cDiff = l_ObjDiff.toDiff(Alltrim(m.pFile1), Alltrim(m.pFile2), .t., .t.)
     This.w_Memo_Diff = IIF(Empty(m.l_cDiff), "No differences", m.l_cDiff)
     l_ObjDiff = .Null.
     ON ERROR &L_OldErr
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_differencesPag1","cp_differences",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMEMO_DIFF_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MEMO_DIFF=space(0)
      .w_CURFLORGNAM=space(254)
      .w_FILE_DIFF=space(254)
      .w_FIND_FILE_C=space(1)
      .w_FIND_FILE_U=space(1)
      .w_FILE1=space(254)
      .w_FILE2=space(254)
      .w_EDIT_COMBO_C=.f.
      .w_EDIT_COMBO_U=.f.
      .w_CURFLORGNAM=oParentObject.w_CURFLORGNAM
          .DoRTCalc(1,2,.f.)
        .w_FILE_DIFF = iif(.w_FIND_FILE_C=="U" or .w_FIND_FILE_U=="C",.w_FILE1,.w_FILE2)
    endwith
    this.DoRTCalc(4,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_differences
    local l_cFile
    l_cFile = This.SearchFileToDiff(alltrim(this.oparentobject.w_CURFLORGNAM), this.oparentobject.w_CURFLCUSTOM)
    this.w_FILE1 = substr(l_cFile,1,at("#",l_cFile)-1)
    this.w_FILE2 = substr(l_cFile,at("#",l_cFile)+1,len(l_cFile))
    if This.oparentobject.w_CURFLCUSTOM='C'
       if empty(this.w_FILE1) and !empty(this.w_FILE2)
         this.w_FIND_FILE_C="S"
         this.w_EDIT_COMBO_C=.f.
       else
         if !empty(this.w_FILE1) and empty(this.w_FILE2)
           this.w_FIND_FILE_C="U"
           this.w_EDIT_COMBO_C=.f.
         else
           if empty(this.w_FILE1) and empty(this.w_FILE2)
             this.w_FIND_FILE_C=""
             this.w_EDIT_COMBO_C=.f.
           else
             this.w_EDIT_COMBO_C=.t.
           endif
         endif
       endif
    else
       if empty(this.w_FILE1) and !empty(this.w_FILE2)
         this.w_FIND_FILE_U="S"
         this.w_EDIT_COMBO_U=.f.
       else
         if !empty(this.w_FILE1) and empty(this.w_FILE2)
           this.w_FIND_FILE_U="C"
           this.w_EDIT_COMBO_U=.f.
         else
           if empty(this.w_FILE1) and empty(this.w_FILE2)
             this.w_FIND_FILE_U=""
             this.w_EDIT_COMBO_U=.f.
           else
             this.w_EDIT_COMBO_U=.t.
           endif
         endif
       endif
    endif
    This.mCalc(.t.)
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CURFLORGNAM=.w_CURFLORGNAM
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_FILE_DIFF = iif(.w_FIND_FILE_C=="U" or .w_FIND_FILE_U=="C",.w_FILE1,.w_FILE2)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFIND_FILE_C_1_8.enabled = this.oPgFrm.Page1.oPag.oFIND_FILE_C_1_8.mCond()
    this.oPgFrm.Page1.oPag.oFIND_FILE_U_1_10.enabled = this.oPgFrm.Page1.oPag.oFIND_FILE_U_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFIND_FILE_C_1_8.visible=!this.oPgFrm.Page1.oPag.oFIND_FILE_C_1_8.mHide()
    this.oPgFrm.Page1.oPag.oFIND_FILE_U_1_10.visible=!this.oPgFrm.Page1.oPag.oFIND_FILE_U_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMEMO_DIFF_1_3.value==this.w_MEMO_DIFF)
      this.oPgFrm.Page1.oPag.oMEMO_DIFF_1_3.value=this.w_MEMO_DIFF
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLORGNAM_1_4.value==this.w_CURFLORGNAM)
      this.oPgFrm.Page1.oPag.oCURFLORGNAM_1_4.value=this.w_CURFLORGNAM
    endif
    if not(this.oPgFrm.Page1.oPag.oFILE_DIFF_1_5.value==this.w_FILE_DIFF)
      this.oPgFrm.Page1.oPag.oFILE_DIFF_1_5.value=this.w_FILE_DIFF
    endif
    if not(this.oPgFrm.Page1.oPag.oFIND_FILE_C_1_8.RadioValue()==this.w_FIND_FILE_C)
      this.oPgFrm.Page1.oPag.oFIND_FILE_C_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFIND_FILE_U_1_10.RadioValue()==this.w_FIND_FILE_U)
      this.oPgFrm.Page1.oPag.oFIND_FILE_U_1_10.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_differencesPag1 as StdContainer
  Width  = 597
  height = 508
  stdWidth  = 597
  stdheight = 508
  resizeXpos=307
  resizeYpos=276
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="STJCOTLCVF",left=416, top=483, width=115,height=19,;
    caption="Copy to clipboard", nPag=1;
    , HelpContextID = 56058512;
  , bGlobalFont=.t.

    proc oBtn_1_1.Click()
      with this.Parent.oContained
        _CLIPTEXT=.w_MEMO_DIFF
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_2 as StdButton with uid="LWTIRHIGJC",left=538, top=484, width=50,height=18,;
    caption="Cancel", nPag=1;
    , HelpContextID = 64882022;
  , bGlobalFont=.t.

    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMEMO_DIFF_1_3 as StdMemo with uid="WGTYSGQTXG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MEMO_DIFF", cQueryName = "MEMO_DIFF",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 28995782,;
   bGlobalFont=.t.,;
    Height=390, Width=582, Left=7, Top=88, readonly=.t.

  add object oCURFLORGNAM_1_4 as StdField with uid="SEZOUHOVIJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CURFLORGNAM", cQueryName = "CURFLORGNAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Selected File",;
    HelpContextID = 266296235,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=93, Top=31, InputMask=replicate('X',254)

  add object oFILE_DIFF_1_5 as StdField with uid="JYIPDFABHM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FILE_DIFF", cQueryName = "FILE_DIFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Compare File",;
    HelpContextID = 239437102,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=93, Top=62, InputMask=replicate('X',254)


  add object oFIND_FILE_C_1_8 as StdCombo with uid="CPFWIIIQRM",rtseq=4,rtrep=.f.,left=93,top=7,width=92,height=21;
    , HelpContextID = 71796957;
    , cFormVar="w_FIND_FILE_C",RowSource=""+"Userdir,"+"Standard", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFIND_FILE_C_1_8.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFIND_FILE_C_1_8.GetRadio()
    this.Parent.oContained.w_FIND_FILE_C = this.RadioValue()
    return .t.
  endfunc

  func oFIND_FILE_C_1_8.SetRadio()
    this.Parent.oContained.w_FIND_FILE_C=trim(this.Parent.oContained.w_FIND_FILE_C)
    this.value = ;
      iif(this.Parent.oContained.w_FIND_FILE_C=='U',1,;
      iif(this.Parent.oContained.w_FIND_FILE_C=='S',2,;
      0))
  endfunc

  func oFIND_FILE_C_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDIT_COMBO_C)
    endwith
   endif
  endfunc

  func oFIND_FILE_C_1_8.mHide()
    with this.Parent.oContained
      return (.oParentObject .w_CURFLCUSTOM=='U')
    endwith
  endfunc


  add object oFIND_FILE_U_1_10 as StdCombo with uid="RTVWOEVSRQ",rtseq=5,rtrep=.f.,left=94,top=7,width=92,height=21;
    , HelpContextID = 71797245;
    , cFormVar="w_FIND_FILE_U",RowSource=""+"Custom,"+"Standard", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFIND_FILE_U_1_10.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFIND_FILE_U_1_10.GetRadio()
    this.Parent.oContained.w_FIND_FILE_U = this.RadioValue()
    return .t.
  endfunc

  func oFIND_FILE_U_1_10.SetRadio()
    this.Parent.oContained.w_FIND_FILE_U=trim(this.Parent.oContained.w_FIND_FILE_U)
    this.value = ;
      iif(this.Parent.oContained.w_FIND_FILE_U=='C',1,;
      iif(this.Parent.oContained.w_FIND_FILE_U=='S',2,;
      0))
  endfunc

  func oFIND_FILE_U_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDIT_COMBO_U)
    endwith
   endif
  endfunc

  func oFIND_FILE_U_1_10.mHide()
    with this.Parent.oContained
      return (.oParentObject .w_CURFLCUSTOM=='C')
    endwith
  endfunc


  add object oBtn_1_11 as StdButton with uid="KFTDJWHVJA",left=196, top=6, width=92,height=19,;
    caption="Compare", nPag=1;
    , HelpContextID = 171168356;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .MakeDiff(.w_CURFLORGNAM,.w_file_diff)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_file_diff))
      endwith
    endif
  endfunc

  add object oStr_1_6 as StdString with uid="KKGJUFMTCC",Visible=.t., Left=16, Top=35,;
    Alignment=1, Width=74, Height=18,;
    Caption="Selected file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ZQTMROKMGD",Visible=.t., Left=12, Top=66,;
    Alignment=1, Width=78, Height=18,;
    Caption="Compare file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XQLUSSASZD",Visible=.t., Left=42, Top=9,;
    Alignment=1, Width=48, Height=17,;
    Caption="Folder:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="XITIXNWDQN",Visible=.t., Left=8, Top=524,;
    Alignment=0, Width=221, Height=18,;
    Caption="ATTENZIONE: ComboBox sovrapposte"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_differences','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
