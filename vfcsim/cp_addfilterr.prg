* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_addfilterr                                                   *
*              Gestione editor filtro                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-11                                                      *
* Last revis.: 2014-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_addfilterr",oParentObject,m.pOPER)
return(i_retval)

define class tcp_addfilterr as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_PADRE = .NULL.
  w_TMPFILTER = space(254)
  w_SETCHSEP = space(10)
  w_POS_OR = 0
  w_POS_AND = 0
  w_SPLITPOS = 0
  w_SPLITSTR = space(3)
  w_FILTERAT = space(254)
  w_FL_FOUND = .f.
  w_OR_FOUND = .f.
  w_AND_FOUND = .f.
  w_IDFLTATT = 0
  w_IDATTOCC = 0
  w_IDOR_OCC = 0
  w_IDANDOCC = 0
  w_CONDPOS = 0
  w_CONDLEN = 0
  w_FLTFIELD = space(30)
  w_FLTCOND = space(10)
  w_FLTVALUE = space(30)
  w_ORFLDNAM = space(30)
  w_ORCONDIT = space(10)
  w_ORVALFLT = space(30)
  w_ORCONDAO = space(3)
  w_DEFLDNAM = space(30)
  w_DECONDIT = space(10)
  w_DEVALFLT = space(30)
  w_DECONDAO = space(3)
  w_FLNAME = space(30)
  w_FLTYPE = space(1)
  w_FLCOMMEN = space(80)
  w_FLLENGHT = 0
  w_FLDECIMA = 0
  w_GetFKKey = space(50)
  w_FLDNAM = space(50)
  * --- WorkFile variables
  TMPADVFILTER_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="NEW_RECORD"
        * --- Inizializzo tabella
        CURTOTAB(this.oParentObject.w_FBCURNAM, "TMPADVFILTER", this)
        USE IN SELECT(this.oParentObject.w_FBCURNAM)
        * --- Analisi campo filtro e trasformazione in righe dettaglio
        this.w_TMPFILTER = ALLTRIM(this.oParentObject.w_FBOLDEXP)
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER=="FILTER_UP" OR this.pOPER=="FILTERDOWN"
        if (this.pOPER=="FILTER_UP" AND this.w_PADRE.RowIndex()>1) OR ( this.pOPER=="FILTERDOWN" AND this.w_PADRE.RowIndex()<this.w_PADRE.NumRow())
          this.w_PADRE.SetRow()     
          * --- Memorizzo le informazioni della riga attuale
          this.w_ORFLDNAM = this.oParentObject.w_FBFLDNAM
          this.w_ORCONDIT = this.oParentObject.w_FBCONDIT
          this.w_ORVALFLT = this.oParentObject.w_FBVALFLT
          this.w_ORCONDAO = this.oParentObject.w_FBCONDAO
          this.w_PADRE.MarkPos()     
          * --- Mi posiziono nella nuova riga (superiore o inferiore)
          if this.pOPER=="FILTER_UP"
            this.w_PADRE.PriorRow()     
          else
            this.w_PADRE.NextRow()     
          endif
          this.w_PADRE.SetRow()     
          * --- Memorizzo le informazioni della riga di destinazione
          this.w_DEFLDNAM = this.oParentObject.w_FBFLDNAM
          this.w_DECONDIT = this.oParentObject.w_FBCONDIT
          this.w_DEVALFLT = this.oParentObject.w_FBVALFLT
          this.w_DECONDAO = this.oParentObject.w_FBCONDAO
          * --- Sovrascrivo la riga con i dati della riga di partenza
          this.oParentObject.w_FBFLDNAM = this.w_ORFLDNAM
          this.oParentObject.w_FBCONDIT = this.w_ORCONDIT
          this.oParentObject.w_FBVALFLT = this.w_ORVALFLT
          this.oParentObject.w_FBCONDAO = this.w_ORCONDAO
          this.w_PADRE.SaveRow()     
          * --- Riposiziono nella riga di partenza
          if this.pOPER=="FILTER_UP"
            this.w_PADRE.NextRow()     
          else
            this.w_PADRE.PriorRow()     
          endif
          this.w_PADRE.SetRow()     
          * --- Sovrascrivo la riga con le informazioni prelevate dalla diga di destinazione
          this.oParentObject.w_FBFLDNAM = this.w_DEFLDNAM
          this.oParentObject.w_FBCONDIT = this.w_DECONDIT
          this.oParentObject.w_FBVALFLT = this.w_DEVALFLT
          this.oParentObject.w_FBCONDAO = this.w_DECONDAO
          this.w_PADRE.SaveRow()     
          * --- Riposiziono sul filtro di partenza (adesso si trova nella riga di destinazione
          if this.pOPER=="FILTER_UP"
            this.w_PADRE.PriorRow()     
          else
            this.w_PADRE.NextRow()     
          endif
          this.w_PADRE.SetRow()     
        else
          ah_ErrorMsg("Operazione non consentita")
        endif
      case this.pOPER=="CLOSE_MASK"
        * --- Drop temporary table TMPADVFILTER
        i_nIdx=cp_GetTableDefIdx('TMPADVFILTER')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPADVFILTER')
        endif
      case this.pOPER=="SAVEANDCLS"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        local L_Macro
        L_Macro = "this.oParentObject.w_FBPARENT."+ALLTRIM(this.oParentObject.w_FBOBJECT)
        &L_Macro = this.w_TMPFILTER
        release L_Macro
      case this.pOPER=="FILTERTEST"
        this.w_PADRE.MarkPos()     
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_FL_FOUND = .T.
        if OCCURS( "(", this.w_TMPFILTER) <> OCCURS( ")", this.w_TMPFILTER)
          ah_ErrorMsg("Coerenza parentesi errata.%0Aperte %1%0Chiuse %2", , , ALLTRIM(STR(OCCURS( "(", this.w_TMPFILTER) ) ), ALLTRIM(STR(OCCURS( ")", this.w_TMPFILTER) ) ) )
          this.w_FL_FOUND = .F.
        endif
        if this.w_FL_FOUND
          * --- Verifico se esiste almeno una parentesi aperta o chiusa e se non ho riscontrato errori
          do while this.w_FL_FOUND AND !EMPTY(this.w_TMPFILTER) AND (RAT( "(", this.w_TMPFILTER)>0 OR AT( ")", SUBSTR(this.w_TMPFILTER,RAT( "(", this.w_TMPFILTER)+1)) >0)
            * --- Se esistono una parentesi aperta e una chiusa e la chiusa non precede l'aperta allora continuo l'analisi
            *     estraenda la sola parte interna alle parentesi incontrate in modo da proseguire l'analisi sulle sezioni via via pi� interne del filtro
            this.w_FL_FOUND = !( ( RAT( "(", this.w_TMPFILTER) > 0 AND AT(")", SUBSTR(this.w_TMPFILTER, RAT( "(", this.w_TMPFILTER)+1)) =0 ) OR ( RAT( "(", this.w_TMPFILTER) = 0 AND AT(")", this.w_TMPFILTER) >0 ))
            if this.w_FL_FOUND
              this.w_TMPFILTER = SUBSTR( this.w_TMPFILTER , 1, RAT( "(", this.w_TMPFILTER)-1)+ SUBSTR(SUBSTR( this.w_TMPFILTER, RAT( "(", this.w_TMPFILTER)+1), AT(")", SUBSTR( this.w_TMPFILTER, RAT( "(", this.w_TMPFILTER)+1))+1)
            endif
          enddo
          if !this.w_FL_FOUND
            ah_ErrorMsg("Bilanciamento parentesi errato.%0Verificare l'espressione di filtro")
          endif
        endif
        if this.w_FL_FOUND
          * --- Ignoro l'ultima riga dove la condizione pu� essere vuota
          this.w_PADRE.LastRow()     
          this.w_PADRE.PriorRow()     
          this.w_PADRE.SetRow()     
          do while this.w_FL_FOUND AND !this.w_PADRE.Bof_Trs()
            this.w_FL_FOUND = !EMPTY(this.oParentObject.w_FBCONDAO)
            this.w_PADRE.PriorRow()     
          enddo
          if !this.w_FL_FOUND
            ah_ErrorMsg("Occorre specificare la condizione di and/or per tutte le righe del filtro")
          endif
        endif
        this.w_PADRE.RePos()     
        this.w_FL_FOUND = !this.w_FL_FOUND OR ah_ErrorMsg("Sintassi filtro corretta", 64)
      case this.pOPER=="MAN2EDITOR"
        * --- Analisi campo filtro e trasformazione in righe dettaglio
        this.w_PADRE.MarkPos()     
        this.w_TMPFILTER = ALLTRIM(this.oParentObject.w_FBMANFLT)
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_PADRE.oPgFrm.ActivePage = 1
        this.w_PADRE.RePos()     
      case this.pOPER=="EDITOR2MAN"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_FBMANFLT = this.w_TMPFILTER
      case this.pOPER=="ZOOMFIELDS"
        this.w_FLNAME = ""
        this.w_FLTYPE = ""
        this.w_FLCOMMEN = ""
        this.w_FLLENGHT = 0
        this.w_FLDECIMA = 0
        vx_exec("CP_ADDFILTER.VZM",this)
        if !EMPTY(NVL(this.w_FLNAME, " "))
          i_retcode = 'stop'
          i_retval = this.w_FLNAME
          return
        endif
      case this.pOPER=="SETFKTABLE"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER=="ZOOMTABLE"
        * --- Esegue lo zoom sulla tabella esterna
        L_Macro = "GSUT_KKA(this.oParentObject)" 
 &L_Macro
        if "'" $ this.oParentObject.w_IDVALATT 
          this.oParentObject.w_FBVALFLT = "[" + this.oParentObject.w_IDVALATT + "]"
        else
          this.oParentObject.w_FBVALFLT = "'" + this.oParentObject.w_IDVALATT + "'"
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caratteri separatori del filtro (' " () [] SPAZIO .)
    *     uno di questi caratteri deve trovarsi prima e dopo di un AND o di un OR
    *     per evitare falsi positivi di AND ed OR nei nomi dei campi/tabelle
    this.w_SETCHSEP = "()'[] ."+'"'
    this.w_PADRE.FirstRow()     
    do while !this.w_PADRE.Eof_Trs() AND this.w_PADRE.FullRow()
      this.w_PADRE.SetRow()     
      if this.w_PADRE.FullRow()
        this.w_PADRE.DeleteRow()     
      endif
      this.w_PADRE.FirstRow()     
    enddo
    do while !EMPTY(this.w_TMPFILTER)
      this.w_POS_OR = 1
      this.w_POS_AND = 1
      this.w_IDOR_OCC = 1
      this.w_IDANDOCC = 1
      this.w_OR_FOUND = .F.
      this.w_AND_FOUND = .F.
      do while !this.w_OR_FOUND AND this.w_POS_OR>0
        this.w_POS_OR = ATC( " OR ", UPPER(this.w_TMPFILTER), this.w_IDOR_OCC )
        * --- Verifico se prima e dopo l'espressione logica esiste il separatore
        *     Se l'or trovato � in posizione 1 sicuramente non � valido
        if this.w_POS_OR>1 AND !( SUBSTR(this.w_TMPFILTER, this.w_POS_OR, 1)$this.w_SETCHSEP AND SUBSTR(this.w_TMPFILTER, this.w_POS_OR+3, 1)$this.w_SETCHSEP)
          this.w_POS_OR = 1
          this.w_IDOR_OCC = this.w_IDOR_OCC + 1
          this.w_OR_FOUND = .F.
        else
          this.w_OR_FOUND = .T.
        endif
      enddo
      do while !this.w_AND_FOUND AND this.w_POS_AND>0
        this.w_POS_AND = ATC( " AND ", UPPER(this.w_TMPFILTER), this.w_IDANDOCC )
        * --- Verifico se prima e dopo l'espressione logica esiste il separatore
        if this.w_POS_AND>1 AND !(SUBSTR(this.w_TMPFILTER, this.w_POS_AND, 1)$this.w_SETCHSEP AND SUBSTR(this.w_TMPFILTER, this.w_POS_AND+4, 1)$this.w_SETCHSEP)
          this.w_POS_AND = 1
          this.w_IDANDOCC = this.w_IDANDOCC + 1
          this.w_AND_FOUND = .F.
        else
          this.w_AND_FOUND = .T.
        endif
      enddo
      if this.w_OR_FOUND OR this.w_AND_FOUND
        * --- Determino quale separatore viene prima in modo da analizzare la
        *     sola prima parte di filtro
        do case
          case this.w_POS_OR>0 AND this.w_POS_AND>0 AND this.w_POS_OR<this.w_POS_AND
            this.w_SPLITPOS = this.w_POS_OR
            this.w_SPLITSTR = "OR"
          case this.w_POS_AND>0 AND this.w_POS_OR>0 AND this.w_POS_AND<this.w_POS_OR
            this.w_SPLITPOS = this.w_POS_AND
            this.w_SPLITSTR = "AND"
          case this.w_POS_OR>0 AND this.w_POS_AND=0
            this.w_SPLITPOS = this.w_POS_OR
            this.w_SPLITSTR = "OR"
          case this.w_POS_AND>0 AND this.w_POS_OR=0
            this.w_SPLITPOS = this.w_POS_AND
            this.w_SPLITSTR = "AND"
          otherwise
            this.w_SPLITPOS = LEN(this.w_TMPFILTER)
            this.w_SPLITSTR = "AND"
        endcase
      endif
      * --- Analizzo la sezione del filtro per individuare: campo condizione e valore
      *     Per ogni filtro esiste una sola condizione, testo le varie condizioni
      *     e appena ne trovo una suddivido il filtro
      * --- Per testare condizioni tipo in, not in, like, ecc. occorre verificare che 
      *     prima e dopo la parola vi sia uno spazio, non si pu� utilizzare solo la
      *     ATC poich� in caso di falsi positivi occorre analizzare un'eventuale
      *     occorrenza successiva. Le espressioni precedute dal not (Es. Not like)
      *     devono precedere le corrispondenti (Es. like) altrimenti in presenza di
      *     una not like verrebbe cercata (e trovata) prima la like e si avrebbe
      *     un'errata separazione del filtro. Per le altre espressioni non � obbligatorio
      *     lo spazio prima e dopo quindi le valuto in tal modo
      this.w_FILTERAT = LEFT(this.w_TMPFILTER, this.w_SPLITPOS )
      this.w_FL_FOUND = .F.
      DIMENSION L_ArrCond(13)
      L_ArrCond(1)="not like"
      L_ArrCond(2)="like"
      L_ArrCond(3)="not in"
      L_ArrCond(4)="in"
      L_ArrCond(5)="not exist"
      L_ArrCond(6)="exist"
      L_ArrCond(7)="between"
      L_ArrCond(8)="<>"
      L_ArrCond(9)=">="
      L_ArrCond(10)="<="
      L_ArrCond(11)="="
      L_ArrCond(12)=">"
      L_ArrCond(13)="<"
      this.w_IDFLTATT = 1
      do while !this.w_FL_FOUND AND this.w_IDFLTATT<ALEN(L_ArrCond,1)
        * --- Testo tutte le espressioi finch� non le ho esaurite o non ho trovato l'occorrenza
        this.w_IDATTOCC = 1
        do while !this.w_FL_FOUND AND ATC( L_ArrCond( this.w_IDFLTATT), this.w_FILTERAT, this.w_IDATTOCC) > 0
          * --- Se trovo l'occorrenza della condizione attuale verifico se non � un falso
          *     positivo mediante ricerca degli spazi prima e dopo.
          *     Se la trovo setto il flag a .T. per terminare le ricerche, altrimenti aumento
          *     il numero dell'occorrenza da ricercare
          if this.w_IDFLTATT<8
            if SUBSTR( this.w_FILTERAT, ATC( L_ArrCond( this.w_IDFLTATT), this.w_FILTERAT, this.w_IDATTOCC) -1, 1)=" " AND SUBSTR( this.w_FILTERAT, ATC( L_ArrCond( this.w_IDFLTATT), this.w_FILTERAT, this.w_IDATTOCC) +LEN( L_ArrCond( this.w_IDFLTATT) ), 1)=" "
              this.w_FL_FOUND = .T.
            else
              this.w_IDATTOCC = this.w_IDATTOCC + 1
            endif
          else
            this.w_FL_FOUND = .T.
          endif
          if this.w_FL_FOUND
            this.w_CONDPOS = ATC( L_ArrCond( this.w_IDFLTATT), this.w_FILTERAT, this.w_IDATTOCC)
            this.w_CONDLEN = LEN( L_ArrCond( this.w_IDFLTATT) )
            this.w_FLTFIELD = ALLTRIM( SUBSTR( this.w_FILTERAT, 1, this.w_CONDPOS - 1 ) )
            this.w_FLTCOND = L_ArrCond( this.w_IDFLTATT)
            this.w_FLTVALUE = ALLTRIM( SUBSTR( this.w_FILTERAT, this.w_CONDPOS + LEN( this.w_FLTCOND) ) )
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        enddo
        this.w_IDFLTATT = this.w_IDFLTATT + 1
      enddo
      if !this.w_FL_FOUND
        * --- Non ho trovato nessun separatore valido, probabilmente � un espressione fox
        this.w_FLTFIELD = ALLTRIM( this.w_FILTERAT )
        this.w_FLTCOND = " "
        this.w_FLTVALUE = ""
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_TMPFILTER = ALLTRIM(SUBSTR( this.w_TMPFILTER, LEN(this.w_FILTERAT) + LEN(this.w_SPLITSTR) + 1 ) )
    enddo
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE.AddRow()     
    this.oParentObject.w_FBFLDNAM = this.w_FLTFIELD
    this.oParentObject.w_FBCONDIT = this.w_FLTCOND
    this.oParentObject.w_FBVALFLT = this.w_FLTVALUE
    this.oParentObject.w_FBCONDAO = this.w_SPLITSTR
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PADRE.SaveRow()     
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_TMPFILTER = ""
    this.w_PADRE.FirstRow()     
    do while !this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      if this.w_PADRE.FullRow()
        this.w_TMPFILTER = this.w_TMPFILTER + " " + ALLTRIM( this.oParentObject.w_FBFLDNAM) + " " + IIF(EMPTY(ALLTRIM( this.oParentObject.w_FBCONDIT)), "" , ALLTRIM( this.oParentObject.w_FBCONDIT) + " " + ALLTRIM( this.oParentObject.w_FBVALFLT) + " " ) + ALLTRIM( this.oParentObject.w_FBCONDAO)
        this.w_SPLITSTR = ALLTRIM( this.oParentObject.w_FBCONDAO)
      endif
      this.w_PADRE.NextRow()     
    enddo
    * --- Elimino l'ultima condizione AND/OR che per l'ultimo filtro � innutile e darebbe errore in esecuzione
    this.w_TMPFILTER = ALLTRIM(LEFT(this.w_TMPFILTER, LEN(this.w_TMPFILTER)- LEN(this.w_SPLITSTR) ))
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerca una relazione con una tabella esterna
    this.oParentObject.w_IDTABKEY = ""
    this.oParentObject.w_IDFLDKEY = ""
    this.w_FLDNAM = alltrim(upper(CHRTRAN(this.oParentObject.w_FBFLDNAM,"()", "  ")))
    if this.w_FLDNAM="W_"
      this.w_FLDNAM = substr(this.w_FLDNAM,3)
    endif
    if this.oParentObject.w_NRELEXT1>0
      FOR JJ=1 TO this.oParentObject.w_NRELEXT1
      this.w_GetFKKey = alltrim(I_DCX.GetFKKey(this.oParentObject.w_FBTABLE1, JJ))
      if "," $ this.w_GetFKKey
        this.w_GetFKKey = SUBSTR(this.w_GetFKKey,RAT(",",this.w_GetFKKey)+1)
      endif
      if this.w_FLDNAM == rtrim(upper(this.w_GetFKKey))
        this.oParentObject.w_IDTABKEY = i_DCX.GetFKTable(this.oParentObject.w_FBTABLE1, JJ)
        this.oParentObject.w_IDFLDKEY = alltrim(i_DCX.GetFKExtKey(this.oParentObject.w_FBTABLE1, JJ))
        if "," $ this.oParentObject.w_IDFLDKEY
          this.oParentObject.w_IDFLDKEY = RTRIM(SUBSTR(this.oParentObject.w_IDFLDKEY,RAT(",", this.oParentObject.w_IDFLDKEY)+1))
        endif
      endif
      NEXT
    endif
    if EMPTY(this.oParentObject.w_IDFLDKEY) AND this.oParentObject.w_NRELEXT2>0
      FOR JJ=1 TO this.oParentObject.w_NRELEXT2
      this.w_GetFKKey = alltrim(I_DCX.GetFKKey(this.oParentObject.w_FBTABLE2, JJ))
      if "," $ this.w_GetFKKey
        this.w_GetFKKey = SUBSTR(this.w_GetFKKey,RAT(",",this.w_GetFKKey)+2)
      endif
      if this.w_FLDNAM == rtrim(upper(this.w_GetFKKey))
        this.oParentObject.w_IDTABKEY = i_DCX.GetFKTable(this.oParentObject.w_FBTABLE2, JJ)
        this.oParentObject.w_IDFLDKEY = alltrim(i_DCX.GetFKExtKey(this.oParentObject.w_FBTABLE2, JJ))
        if "," $ this.oParentObject.w_IDFLDKEY
          this.oParentObject.w_IDFLDKEY = RTRIM(SUBSTR(this.oParentObject.w_IDFLDKEY,RAT(",", this.oParentObject.w_IDFLDKEY)+2))
        endif
      endif
      NEXT
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMPADVFILTER'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
