* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VG_BUILD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Samuele Masetto
* Data creazione: 26/06/98
* Aggiornato il : 26/06/98
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Grafici
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

#Define bartype -4099
#Define coltype	-4100
#Define pietype	-4102

#Define CRLF Chr(13)+Chr(10)
#Define Tab Chr(9)
Lparam i_cCursor,i_cFileName,i_bMod,i_bDontAutoActivate
Local cOldErr,bFox26
Private bErr
If Type("i_bFox26")='L'
    bFox26=.T.
    If At('vg_lib',Lower(Set('proc')))=0 Or At('cp_pfile',Lower(Set('proc')))=0
        Set Proc To vg_lib,cp_pfile Additive
    Endif

    i_cBmpPath=''
    i_cStdIcon=''
Endif
If Lower(Right(i_cFileName,4))='.vgr'
    i_cFileName=Left(i_cFileName,Len(i_cFileName)-4)
Endif
bErr=.F.
cOldErr=On('ERROR')
On Error bErr=.T.
o=Createobject('fGraph',i_cCursor,i_cFileName,i_bMod,i_bDontAutoActivate)
On Error &cOldErr
If bErr
    If bFox26
        cp_errormsg(MSG_CANNOT_OPEN_MSGRAPH)
    Else
        cp_errormsg(MSG_CANNOT_OPEN_MSGRAPH,'stop')
    Endif
Else
    If bFox26
        Read When showwnd26(o)
    Else
        o.Show()
    Endif
Endif

Func showwnd26(o)
    o.Show()
    Return(.F.)

Define Class fGraph As CpSysform
    Icon=i_cBmpPath+i_cStdIcon
    Top=10
    Left=10
    Width=400
    Height=230
    WindowType=1
    i_cFileName=''
    i_cCursor=''
    i_bMod=.T.
    Add Object ole1 As cp_graph With Width=200,Height=200
    Proc Init(i_cCursor,i_cFileName,i_bMod,i_bDontAutoActivate)
        If Lower(Right(i_cFileName,4))='.vgr'
            i_cFileName=Left(i_cFileName,Len(i_cFileName)-4)
        Endif
        This.i_cFileName=i_cFileName
        This.Caption='Graph '+This.i_cFileName+'.vgr'
        This.i_cCursor=i_cCursor
        This.i_bMod=i_bMod
        This.ole1.Height=This.Height
        This.ole1.Width=This.Width
        This.ole1.m_graph.Height=This.ole1.Height
        This.ole1.m_graph.Width=This.ole1.Width
        If cp_IsStdFile(This.i_cFileName,'vgr')
            Local i_cName
            i_cName=cp_GetStdFile(This.i_cFileName,'vgr')
            i_cName=i_cName+'.vgr'
            If cp_fileexist(Sys(5)+Sys(2003)+'\'+i_cName)
                i_cName=Sys(5)+Sys(2003)+'\'+i_cName
            Endif
            If !Thisform.ole1.m_graph.Open(i_cName)
                bErr=.T.
                Return
            Endif
        Else
            Thisform.ole1.m_graph.i_bNew=.T.
        Endif
        *if type('this.i_cCursor')<>'L'
        *  this.ole1.ClearDataSheet()
        *endif
        This.ole1.m_graph.CreateDataSheet(This.i_cCursor)
        If This.i_bMod And !i_bDontAutoActivate
            This.ole1.m_graph.OpenDesign(1)
        Else
            If i_bDontAutoActivate
                This.ole1.m_graph.OpenDesign(2)
            Endif
        Endif
        This.i_cFileName=This.i_cFileName+'.vgr'
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
        * Nasconde Toolbar
        If i_VisualTheme<>-1 And Vartype(i_ThemesManager)='O'
            i_ThemesManager.HideToolbar()
        Else
            If Type('oDesktopbar.name')='C'
                oDesktopbar.Hide()
            Endif
            oCpToolBar.Hide()
        Endif
    Proc Resize()
        This.ole1.m_graph.Height=This.Height
        This.ole1.m_graph.Width=This.Width
        This.ole1.Height = This.ole1.m_graph.Height
        This.ole1.Width = This.ole1.m_graph.Width
    Proc QueryUnload()
        Local nRet
        If This.i_bMod
            nRet=Messagebox(cp_MsgFormat(MSG_SAVE_FILE__QP,This.i_cFileName),3+32,cp_translate(MSG_SAVE))
            If nRet=6
                This.ole1.m_graph.Save(This.i_cFileName)
            Endif
            If nRet=2
                Nodefault
            Else
                * Visualizza Toolbar
                If i_VisualTheme<>-1 And Vartype(i_ThemesManager)='O'
                    i_ThemesManager.ShowToolbar()
                    If Type('oDesktopbar.name')='C'
                        oDesktopbar.SetAfterCpToolbar()
                    Endif
                    If Type('i_MenuToolbar.name')='C'
                        i_MenuToolbar.Dock(i_nCPToolBarPos,0,0)
                    Endif

                Else
                    oCpToolBar.Show()
                    If Type('oDesktopbar.name')='C'
                        oDesktopbar.Show()
                    Endif
                Endif
            Endif
        Endif
        _Screen.WindowState=_Screen.WindowState
        Return
Enddefine
