#Define WM_THEMECHANGED   0x031A
#Define IDI_INFORMATION   32516
#Define IDI_ERROR          32513
#Define IDI_EXCLAMATION    32515
#Define IDI_QUESTION       32514
#Define WM_NULL        0
#Define WM_SYSCOMMAND  0x112
#Define WM_LBUTTONUP   0x202
#Define MOUSE_MOVE     0xf012
#Define WM_MOUSEHOVER	0x2A1
#Define GWL_EXSTYLE	-20
#Define WS_EX_LAYERED	0x80000
#Define LWA_ALPHA	0x2
#Define WM_MOUSEMOVE	0x200
#Define WM_LBUTTONDOWN	0x201
#Define WM_KEYDOWN 0x0100
#Define GCL_STYLE	-26
#Define	CS_DROPSHADOW	0x20000
#Define WM_NCACTIVATE	0x86


#Define NAVBAR_BORDERCOLOR	1
#Define CONTAINER_BORDERCOLOR	2
#Define TITLE_BORDERCOLOR	3
#Define TITLE_FONTCOLOR	4
#Define TITLE_BACKGROUND_LEFT	5
#Define TITLE_BACKGROUND_WIDTH	6
#Define PANELVERTICAL_BACKCOLOR	7
#Define PANELVERTICAL_FONTCOLOR	8
#Define PANELBUTTON_SELECTED_FONTCOLOR	9
#Define PANELBUTTON_NOTSELECTED_FONTCOLOR	10
#Define FORM_BACKGROUND_PICTURE	11
#Define OVERFLOWPANELBUTTON_FOCUSEDNOTSELECTED_PICTURE	12
#Define OVERFLOWPANELBUTTON_FOCUSEDSELECTED_PICTURE	13
#Define OVERFLOWPANELBUTTON_NOTFOCUSEDSELECTED_PICTURE	14
#Define OVERFLOWPANEL_BACKGROUND_PICTURE	15
#Define OVERFLOWPANEL_MENUBUTTON_PICTURE	16
#Define PANELBUTTON_FOCUSEDNOTSELECTED_PICTURE	17
#Define PANELBUTTON_FOCUSEDSELECTED_PICTURE	18
#Define PANELBUTTON_NOTFOCUSEDSELECTED_PICTURE	19
#Define PANELBUTTON_NOTFOCUSEDNOTSELECTED_PICTURE	20
#Define SHRINKBUTTON_FOCUSEDNOTSHRUNK_PICTURE	21
#Define SHRINKBUTTON_FOCUSEDSHRUNK_PICTURE	22
#Define SHRINKBUTTON_NOTFOCUSEDNOTSHRUNK_PICTURE	23
#Define SHRINKBUTTON_NOTFOCUSEDSHRUNK_PICTURE	24
#Define SPLITTER_BACKGROUND_PICTURE	25
#Define SPLITTER_GRIPPER_PICTURE	26
#Define TITLE_BACKGROUND_PICTURE	27
#Define VERTICAL_SPLITTER_BACKGROUND_PICTURE	28
#Define VERTICAL_SPLITTER_GRIPPER_PICTURE	29
#Define CONTAINER_BACKCOLOR	30
#Define VERTICALSHRINKBUTTON_SHRUNK_PICTURE	31
#Define VERTICALSHRINKBUTTON_NOTSHRUNK_PICTURE	32
#Define PANELTITLE_NOTFOCUSEDBACKGROUND_PICTURE	33
#Define PANELTITLE_FOCUSEDBACKGROUND_PICTURE	34
#Define PANELTITLE_BOTTOMLINE_COLOR	35
#Define PANELTITLE_TOPLINE_COLOR	36
#Define PANELTITLE_FONTCOLOR	37
#Define CLOSEBUTTON_PICTURE	38
#Define CLOSEBUTTONFOCUSED_PICTURE	39
#Define PINBUTTON_PICTURE	40
#Define PINBUTTON_FOCUSED_PICTURE	41
#Define	PINBUTTON_PINNED_PICTURE	42
#Define	PINBUTTON_FOCUSEDPINNED_PICTURE	43
#Define DELETEBUTTON_PICTURE	44
#Define DELETEBUTTONFOCUSED_PICTURE	45
#Define OKBUTTON_PICTURE	46
#Define OKBUTTONFOCUSED_PICTURE	47
#Define OVERFLOWPANELFOCUSED_MENUBUTTON_PICTURE	48
#Define DOTSBUTTON_PICTURE	49
#Define DOTSBUTTONFOCUSED_PICTURE	50

#Define SENDBUTTON_PICTURE 77
#Define SENDBUTTON_FOCUSED_PICTURE 78
#Define SENDBUTTON_PINNED_PICTURE 79
#Define SENDBUTTON_FOCUSEDPINNED_PICTURE 80

#Define BLACKNESS	0x42
#Define DSTINVERT	0x550009
#Define MERGECOPY	0xC000CA
#Define MERGEPAINT	0xBB0226
#Define NOTSRCCOPY	0x330008
#Define NOTSRCERASE	0x1100A6
#Define PATCOPY		0xF00021
#Define PATINVERT	0x5A0049
#Define PATPAINT	0xFB0A09
#Define SRCAND		0x8800C6
#Define SRCCOPY		0xCC0020
#Define SRCERASE	0x440328
#Define SRCINVERT	0x660046
#Define SRCPAINT	0xEE0086
#Define WHITENESS	0xFF0062

#Define HKEY_LOCAL_MACHINE	0x80000002
#Define HKEY_CLASSES_ROOT	0x80000000
#Define ERROR_SUCCESS	0
#Define KEY_READ	131097

#Define DI_NORMAL	0x3
#Define DI_MASK	0x1
#Define DI_IMAGE	0x2

#Define MSG_NO_TEXT_POSTIT	"<PostIt without text>"

#Include "cp_app_lang.inc"
Define Class cp_AlertManager As Custom
    Name = "cp_AlertManager"
    StdTransparency = 30
    StdFadeTime = 2
    WaitAlert = .Null.

    Add Object AlertCollection As Collection

    Procedure Init()
        With This
            .WaitAlert = .AddAlert("cp_WaitAlert","",-1)
            .WaitAlert.Height = .WaitAlert.MinHeight
            .WaitAlert.Width = .WaitAlert.MinWidth
        Endwith
    Endproc


    Procedure AddAlert
        Lparameters cClass, cAction, nServerConn
        Local oAlert
        If Upper(m.cClass)==Upper("cp_WaitAlert") And Vartype(This.WaitAlert)=='O'
            cp_ErrorMsg("Impossibile aggiungere pi� di un WaitAlert", 16)
            Return .Null.
        Else

            m.oAlert=Createobject(m.cClass, cAction, nServerConn)
            This.AlertCollection.Add(m.oAlert)
            SetFocus(_vfp.HWnd)
            Return m.oAlert
        Endif
    Endproc

    Procedure Destroy
        Local oAlert, nAlertAtt
        m.AlertAtt=1
        With This
            Do While .AlertCollection.Count>0 And m.AlertAtt<=.AlertCollection.Count
                Try
                    m.oAlert = .AlertCollection.Item(m.AlertAtt)
                    m.oAlert.Release()
                Catch
                    m.AlertAtt = m.AlertAtt + 1
                Endtry
            Enddo
        Endwith
    Endproc

Enddefine
***************************************************************************************
Define Class cp_AlertForm As Form
    Top = 0
    Left = 0
    Height = 74
    Width = 329
    Desktop = .T.
    ShowWindow = 2
    ShowInTaskbar = .F.
    BorderStyle = 0
    Caption = ""
    MaxButton = .F.
    MinButton = .F.
    TitleBar = 0
    AlwaysOnTop = .T.
    MinWidth = 329
    MinHeight = 74
    nStatus = -1
    bClosable = .T.
    nTransparency = 0
    nFadeFactor = 5.1
    nFadeTimer = 20
    nFadePercent = 60
    bCanFade = .T.
    Pinned = .F.
    nTimeOut = 0
    bInitialized = .F.
    bFullShowed = .F.
    bNoClear = .F.

    Name = "CP_DSKALERTS"

    Add Object imgbackground As Image With ;
        Top = 1, Left = 1 , ;
        Height = 72, Width = 327, ;
        Anchor = 15, Stretch = 2, ;
        MousePointer = 0, ;
        Name = "imgBackground"

    Add Object tmrFader As Timer With ;
        Top = 0, ;
        Left = 0, ;
        Height = 23, ;
        Width = 23, ;
        Enabled = .F., ;
        Interval = 20, ;
        Name = "tmrFader"

    Add Object tmrFadeOut As Timer With ;
        Top = 0, ;
        Left = 0, ;
        Height = 23, ;
        Width = 23, ;
        Enabled = .F., ;
        Interval = 800, ;
        Name = "tmrFadeOut"

    Add Object tmrTimeOut As Timer With ;
        Top = 0, ;
        Left = 0, ;
        Height = 23, ;
        Width = 23, ;
        Enabled = .F., ;
        Interval = 1000, ;
        Name = "tmrTimeOut"

    Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            .nFadeFactor = 255/(1000/.nFadeTimer)
            .ChangeTheme()
            .bCanFade = (Val(Os(3))>=5)
        Endwith
    Endproc

    Procedure DoAlert
        With This
            If .bCanFade
                .nStatus = 1
                .tmrFader.Enabled=.T.
            Else
                .Visible = .T.
            Endif
            .bInitialized = .T.
            If .nTimeOut>0
                .tmrTimeOut.Enabled = .T.
            Endif
        Endwith
    Endproc

    Procedure bCanFade_assign
        Lparameters vNewVal
        Local Ret
        This.bCanFade = m.vNewVal
        If This.bCanFade
            m.Ret = GetWindowLong(This.HWnd, GWL_EXSTYLE)
            m.Ret = Bitor(m.Ret, WS_EX_LAYERED)
            SetWindowLong(This.HWnd, GWL_EXSTYLE, m.Ret)
            SetLayeredWindowAttributes(This.HWnd, 0, 0, LWA_ALPHA)
        Endif
    Endproc

    Procedure WaitClear
        With This
            If .bNoClear
                .bNoClear = .F.
                .nStatus = 0
                .HideForm()
            Endif
        Endwith
    Endproc

    Procedure tmrFader.Timer
        With This.Parent
            Do Case
                Case .nStatus = 0	&& Chiusura
                    If !.bNoClear
                        .nTransparency = .nTransparency - .nFadeFactor
                        SetLayeredWindowAttributes(.HWnd, 0, .nTransparency, 2)
                    Endif
                    If .nTransparency <= 1 Or .bNoClear
                        This.Enabled = .F.
                        If .bClosable
                            .Release()
                        Else
                            .Hide()
                        Endif
                    Endif
                Case .nStatus = 1	&& Apertura
                    .nTransparency = .nTransparency  + .nFadeFactor
                    If .nTransparency>255
                        .nTransparency = 255
                    Endif
                    SetLayeredWindowAttributes(.HWnd, 0, .nTransparency, 2)
                    If .nTransparency >= 255 * ((100 - .nFadePercent)/100)
                        If .Pinned
                            SetLayeredWindowAttributes(.HWnd, 0, 255, 2)
                        Endif
                        This.Enabled = .F.
                        .bFullShowed = .T.
                    Endif
            Endcase
        Endwith
    Endproc

    Procedure tmrFadeOut.Timer
        With This.Parent
            SetLayeredWindowAttributes(.HWnd, 0, .nTransparency, 2)
        Endwith
    Endproc

    Procedure tmrTimeOut.Timer
        With This.Parent
            .nTimeOut = .nTimeOut - 1
            If .nTimeOut<=0
                This.Enabled = .F.
                .nStatus = 0
                .HideForm()
            Endif
        Endwith
    Endproc

    Procedure nTimeOut_Assign
        Lparameters nNewVal
        With This
            .nTimeOut = m.nNewVal
            .tmrTimeOut.Enabled = .bInitialized And .nTimeOut>0
        Endwith
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This
            SetLayeredWindowAttributes(.HWnd, 0, 255, 2)
            .tmrFadeOut.Enabled = .F.
        Endwith
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This
            If !.Pinned
                .tmrFadeOut.Enabled = .T.
            Endif
        Endwith
    Endproc

    Procedure imgbackground.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseEnter()
    Endproc

    Procedure imgbackground.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseEnter()
    Endproc

    Procedure imgbackground.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseLeave()
    Endproc

    Procedure Pinned_assign
        Lparameters bNewVal
        This.Pinned = m.bNewVal
    Endproc

    Procedure HideForm
        This.tmrFader.Enabled = .T.
    Endproc

    Procedure bNoClear_assign
        Lparameters bNewVal
        This.bNoClear = m.bNewVal
    Endproc

    Procedure ChangeTheme()
        With _Screen.cp_Themesmanager
            This.imgbackground.Picture = .GetProp(TITLE_BACKGROUND_PICTURE)
            This.BackColor = .GetProp(TITLE_BORDERCOLOR)
        Endwith
    Endproc

    Procedure getscreenheight

        Local cBuffer, cDWord

        m.cBuffer = Space(16)
        SystemParametersInfo(48, 0, @m.cBuffer, 0)

        m.cDWord = Substr(m.cBuffer, 13, 4)

        Return Asc(Substr(m.cDWord, 1,1)) + ;
            BITLSHIFT(Asc(Substr(m.cDWord, 2,1)),  8) +;
            BITLSHIFT(Asc(Substr(m.cDWord, 3,1)), 16) +;
            BITLSHIFT(Asc(Substr(m.cDWord, 4,1)), 24)
    Endproc


    Procedure getscreenwidth
        Local cBuffer As String, cDWord As String
        m.cBuffer = Space(16)

        SystemParametersInfo(48, 0, @m.cBuffer, 0)

        m.cDWord = Substr(m.cBuffer, 9, 4)

        Return Asc(Substr(m.cDWord, 1,1)) + ;
            BITLSHIFT(Asc(Substr(m.cDWord, 2,1)),  8) +;
            BITLSHIFT(Asc(Substr(m.cDWord, 3,1)), 16) +;
            BITLSHIFT(Asc(Substr(m.cDWord, 4,1)), 24)
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine
***************************************************************************
Define Class cp_WaitAlert As cp_AlertForm
    Top = 0
    Left = 0
    nFadePercent = 100
    MinHeight = 30
    MinWidth = 30
    bNoWait = .T.
    nMoved=0
    bClosable=.F.
    nOldCaptureHwnd = 0
    bWaiting = .F.
    #Define WM_NCACTIVATE	0x86

    Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1

    Add Object TextArea As cp_TextArea

    Procedure Init
        Lparameters cAction, nServerConn
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            .TextArea.Left = 5
            .TextArea.Top = 5
            .TextArea.Inizialized = .T.
            .Visible = .F.
            .nFadeFactor = 255
            .ChangeTheme()
            .bCanFade = .F. &&(VAL(OS(3))>=5)
            .TextAssign("")
        Endwith
    Endproc

    Procedure DoAlert
        With This
            .nMoved = 0
            .nStatus=1
            .nFadeFactor = 255
            .nFadePercent = 100
            .nTransparency=255
            .bInitialized = .T.
            .bClosable = .F.
            .Visible = .T.
            SetLayeredWindowAttributes(.HWnd, 0, 255, 2)
            .tmrFader.Enabled=.T.
            Bindevent(This.HWnd,WM_MOUSEMOVE,This,"MouseCheck")
            Bindevent(This.HWnd,WM_LBUTTONDOWN,This,"MouseCheck")
            If .nTimeOut>0 And !.bNoWait
                .tmrTimeOut.Enabled = .T.
            Endif
            .nOldCaptureHwnd = SetCapture( .HWnd)
            SetFocus(_vfp.HWnd)
            If !.bNoWait
                .Waiting()
            Endif
        Endwith
    Endproc

    Procedure Waiting
        Local nIniSec
        With This
            .bWaiting = .T.
            m.nIniSec = Seconds()
            Do While .bWaiting
                If !.bFullShowed Or (Vartype(g_TimerEnabledOnWaitWindow)='L' And g_TimerEnabledOnWaitWindow)
                    DoEvent Force
                Else
                    .bWaiting = .bWaiting Or Inkey(0.1,"H")=0
                Endif
                If Seconds()-m.nIniSec>=.nTimeOut And .nTimeOut>0
                    .bWaiting=.F.
                Endif
            Enddo
            .nStatus = 0
            .nMoved = 0
            .bWaiting = .F.
            .HideForm()
        Endwith
    Endproc

    Procedure Hide
        If This.bNoClear
            Nodefault
        Else
            DoDefault()
        Endif
        ReleaseCapture()
        SetCapture(This.nOldCaptureHwnd)
        Unbindevent(This.HWnd,WM_MOUSEMOVE)
        Unbindevent(This.HWnd,WM_LBUTTONDOWN)
    Endproc

    Procedure bNoWait_Assign
        Lparameters bNewVal
        This.bNoWait = m.bNewVal
    Endproc

    Procedure MouseCheck
        Lparameters HWnd, Msg, WPARAM, Lparam
        With This
            If (.bNoWait And Msg=WM_MOUSEMOVE And .nMoved>2) Or (!.bNoWait And Msg=WM_LBUTTONDOWN )
                .bWaiting = .F.
                If .bNoWait
                    .nStatus = 0
                    .nMoved = 0
                    .HideForm()
                Endif
            Else
                If .bNoWait
                    .nMoved=.nMoved + 1
                Endif
            Endif
        Endwith
    Endproc

    Procedure TextAssign
        Lparameters cNewVal
        If Len(m.cNewVal)>254
            m.cNewVal = Left(m.cNewVal, 254)
        Endif
        This.TextArea.lblText.Caption = m.cNewVal
    Endproc

    Procedure TextArea.ResizeTextArea()
        DoDefault()
        With This.Parent
            This.Top = (.Height - This.Height)/2
            .Width = .Width + 5
            .Left = .getscreenwidth() - .Width - 10
            .Top = _Screen.Top + 13
        Endwith
    Endproc

Enddefine
***************************************************************************
Define Class cp_Alert As cp_AlertForm

    AlertType = IDI_INFORMATION
    ShowPushPin = .T.
    ShowClose = .T.
    Name = "CP_DSKALERTS"

    Add Object TopGripper As cp_Gripper

    Add Object Title As Label With ;
        Top = 8, Left = 5 , ;
        Height = 15, Width = 283, ;
        Anchor = 0, Caption = "", ;
        FontBold = .T., BackStyle = 0

    Add Object PinButton As cp_ImgButton With ;
        Top = 8, Left = 290, ;
        Height = 15, Width = 15, ;
        Anchor = 0, Pinnable=.T., Visible = .F.

    Add Object CloseButton As cp_ImgButton With ;
        Top = 8, Left = 310, ;
        Height = 15, Width = 15, ;
        Anchor = 0, Pinnable = .F., Visible = .F.

    Add Object TextArea As cp_TextArea

    Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            .GetFreePosition()
            .Title.Width = .Width - .Title.Left - Iif(.ShowPushPin, 20, 0) - Iif(.ShowClose, 20, 0) - 5
            .Title.Anchor = 11
            If .ShowPushPin
                If !.ShowClose
                    .PinButton.Left = .PinButton.Left + .PinButton.Width + 5
                Endif
                .PinButton.Anchor = 9
                .PinButton.Visible=.T.
            Endif
            If .ShowClose
                .CloseButton.Anchor = 9
                .CloseButton.Visible=.T.
            Endif
            .TextArea.Left = 45
            .TextArea.Top = 24
            .TextArea.Inizialized = .T.
            .Visible = .T.
            .nFadeFactor = 255/(1000/.nFadeTimer)
            .ChangeTheme()
            .bCanFade = (Val(Os(3))>=5)
            .Paint()
        Endwith
    Endproc

    Procedure ChangeTheme()
        With _Screen.cp_Themesmanager
            This.Title.ForeColor = .GetProp(TITLE_FONTCOLOR)
        Endwith
        DoDefault()
    Endproc

    Procedure AlertType_assign
        Lparameters nNewVal
        With This
            .AlertType = m.nNewVal
            .Paint()
        Endwith
    Endproc

    Procedure TextAssign
        Lparameters cNewVal
        If Len(m.cNewVal)>254
            m.cNewVal = Left(m.cNewVal, 254)
        Endif
        This.TextArea.lblText.Caption = m.cNewVal
    Endproc

    Procedure TitleAssign
        Lparameters cNewVal
        If Len(m.cNewVal)>254
            m.cNewVal = Left(m.cNewVal, 254)
        Endif
        This.Title.Caption = m.cNewVal
    Endproc

    Procedure Pinned_assign
        Lparameters bNewVal
        This.Pinned = m.bNewVal
        This.PinButton.Pinned = m.bNewVal
    Endproc

    Procedure Paint
        Local HWnd, hdc, hicon
        With This
            If .AlertType<>0
                m.hwnd = .HWnd
                m.hdc = GetWindowDC(m.hwnd)
                m.hicon = LoadIcon(0, .AlertType)
                If m.hicon <> 0
                    DrawIcon(m.hdc, 5,25, m.hicon)
                Endif
                ReleaseDC(m.hwnd, m.hdc)
            Endif
        Endwith

    Endproc

    Procedure Title.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseEnter()
    Endproc

    Procedure Title.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseLeave()
    Endproc

    Procedure Moved
        This.Paint()
    Endproc

    Procedure GetFreePosition
        Local nMaxRows As Integer, nMaxCols As Integer, ;
            nMaxAlerts As Integer, nAlerts As Integer, ;
            nRow As Integer, nCol As Integer, nScreenHeight, nScreenWidth
        With This
            m.nScreenHeight = .getscreenheight()
            m.nScreenWidth = .getscreenwidth()
            .Top = m.nScreenHeight - .Height - 50
            .Left = m.nScreenWidth - .Width - 50
            m.nMaxRows = Int(m.nScreenHeight/100)
            m.nMaxCols = Int(m.nScreenWidth/350)
            m.nMaxAlerts = m.nMaxRows*m.nMaxCols
            m.nAlerts = This.FindAlertsWindow()
            m.nRow = Mod(m.nAlerts,m.nMaxRows)
            m.nCol = Int(m.nAlerts/m.nMaxRows)
            If m.nRow = 0
                If m.nAlerts > m.nMaxRows
                    .Top = m.nScreenHeight - (100*m.nRow)
                    m.nCol = m.nCol + 1
                Else
                    .Top = m.nScreenHeight - (100*m.nMaxRows)
                Endif
            Else
                .Top = m.nScreenHeight - (100*m.nRow)
                m.nCol = m.nCol + 1
            Endif
            .Left = m.nScreenWidth - (350 * m.nCol)
        Endwith
    Endproc

    Function FindAlertsWindow
        Local hParent, hWindow, cTitle, cRect , nWinAtt
        Create Cursor cs (winhandle I, x0 I, y0 I, x1 I, y1 I, wintitle C(200))
        Index On Upper(Alltrim(wintitle)) Tag wintitle COLLATE "MACHINE"
        m.nWinAtt = 1
        Do While m.nWinAtt<=_Screen.cp_AlertManager.AlertCollection.Count
            Store Replicate(Chr(0),255) To cClass, cTitle
            hWindow = _Screen.cp_AlertManager.AlertCollection.Item(m.nWinAtt).HWnd
            cTitle = _Screen.cp_AlertManager.AlertCollection.Item(m.nWinAtt).Name
            cRect = This.GetWinRect(hWindow)
            Insert Into cs Values (hWindow,;
                this.buf2dword(Substr(cRect, 1,4)),;
                this.buf2dword(Substr(cRect, 5,4)),;
                this.buf2dword(Substr(cRect, 9,4)),;
                this.buf2dword(Substr(cRect, 13,4)),;
                cTitle)
            m.nWinAtt = m.nWinAtt + 1
        Enddo

        Select * From cs Where Upper(Alltrim(wintitle)) == "CP_DSKALERTS" Into Array laTemp
        If _Tally > 0
            Use In Select("cs")
            Return Alen(laTemp,1)
        Else
            Use In Select("cs")
            Return 0
        Endif
    Endfunc

    Function GetWinRect(hWindow)
        Local cBuffer
        cBuffer = Replicate(Chr(0), 16)
        = GetWindowRect(hWindow, @cBuffer)
        Return cBuffer
    Endfunc

    Function buf2dword(lcBuffer)
        Return Asc(Substr(lcBuffer, 1,1)) + ;
            BitLShift(Asc(Substr(lcBuffer, 2,1)),  8) +;
            BitLShift(Asc(Substr(lcBuffer, 3,1)), 16) +;
            BitLShift(Asc(Substr(lcBuffer, 4,1)), 2)
    Endfunc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine
**********************************************************************************
Define Class cp_AdvPostIt As Form &&cp_AlertForm

    Width = 200
    Height = 200
    *MinWidth = 200
    *MinHeight = 200
    ShowWindow = 1
    ShowInTaskbar = .F.
    ShowTips = .T.
    BorderStyle = 0
    Caption = ""
    MaxButton = .F.
    MinButton = .F.
    TitleBar = 0
    AlwaysOnTop = .T.
    Top = 0
    Left = 0
    *nStatus = 0
    cStatus = "D"
    Name = "CP_ADVPOSTIT"
    StartX = 0
    StartY = 0

    Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1

    Add Object shpBorder As Shape With ;
        Top = 1, Left = 1, Height = 200, Width = 200, ;
        Anchor = 0, BorderWidth = 1, BorderStyle = 1, ;
        Visible = .T., BackStyle = 0

    Add Object oPostItContainer As cp_AdvPostItContainer With ;
        Top = 1, Left = 1, Width = 100, Height =100, ;
        bCanResize = .T. , ShowClose = .T., OLEDropMode = 1

    Add Object TopGripper As cp_Gripper

    Add Object BottomResizer As Container With ;
        Top = 195, Left = 0, Width = 190, Height = 4 , ;
        BackStyle = 0, Anchor = 14, BorderWidth = 0, ;
        MousePointer = 7, OLEDropMode = 1

    Add Object RightResizer As Container With ;
        Top = 7, Left = 196, Width = 4, Height = 183, ;
        BackStyle = 0, Anchor = 13, BorderWidth = 0, ;
        MousePointer = 9, OLEDropMode = 1

    Add Object BottomRightResizer As Container With ;
        Top = 190, Left = 190, Width = 10, Height = 9, ;
        BackStyle = 0, Anchor = 12, BorderWidth = 0, ;
        MousePointer = 8, OLEDropMode = 1

    Procedure Init
        Lparameters cAction, nServerConn
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            .oPostItContainer.SetUpPostIt(m.cAction, m.nServerConn, This)
            .oPostItContainer.Anchor = 0
            .oPostItContainer.Top = 0
            .oPostItContainer.Left = 0
            .oPostItContainer.Width = .Width
            .oPostItContainer.Height = .Height
            .oPostItContainer.Anchor = 15
            .TopGripper.Anchor = 0
            .TopGripper.Width = .Width - 2
            .TopGripper.Anchor = 11
            .shpBorder.Width = .Width
            .shpBorder.Height = .Height
            .shpBorder.Anchor = 15
        Endwith
    Endproc

    Procedure ChangeTheme()
        Local L_OutImg
        With _Screen.cp_Themesmanager
            This.BackColor = .GetProp(TITLE_BORDERCOLOR)
            This.shpBorder.BorderColor = .GetProp(TITLE_BORDERCOLOR)
        Endwith
    Endproc

    Procedure ActivateScrollBars
        Lparameters oTextArea
        If This.TextHeight(m.oTextArea.Value)>m.oTextArea.Height
            m.oTextArea.ScrollBars = 2
        Else
            m.oTextArea.ScrollBars = 0
        Endif
    Endproc

    Procedure RightResizer.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        This.Parent.oPostItContainer.MyDragDrop(This,oSource, nXCoord, nYCoord)
    Endproc

    Procedure BottomResizer.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        This.Parent.oPostItContainer.MyDragDrop(This,oSource, nXCoord, nYCoord)
    Endproc

    Procedure BottomRightResizer.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        This.Parent.oPostItContainer.MyDragDrop(This,oSource, nXCoord, nYCoord)
    Endproc

    Procedure  RightResizer.OLEDragOver
        Lparameters oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        This.Parent.oPostItContainer.MyOLEDragOver(This, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState)
    Endproc

    Procedure  BottomResizer.OLEDragOver
        Lparameters oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        This.Parent.oPostItContainer.MyOLEDragOver(This, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState)
    Endproc

    Procedure  BottomRightResizer.OLEDragOver
        Lparameters oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        This.Parent.oPostItContainer.MyOLEDragOver(This, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState)
    Endproc

    Procedure BottomResizer.OLEDragDrop
        Lparameters oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        This.Parent.oPostItContainer.MyOLEDragDrop(This, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord)
    Endproc

    Procedure RightResizer.OLEDragDrop
        Lparameters oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        This.Parent.oPostItContainer.MyOLEDragDrop(This, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord)
    Endproc

    Procedure BottomRightResizer.OLEDragDrop
        Lparameters oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        This.Parent.oPostItContainer.MyOLEDragDrop(This, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord)
    Endproc

    Procedure BottomResizer.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.BeforeResize(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
    Endproc

    Procedure RightResizer.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.BeforeResize(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
    Endproc

    Procedure BottomRightResizer.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.BeforeResize(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
    Endproc

    Procedure BottomResizer.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.AfterResize(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
    Endproc

    Procedure RightResizer.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.AfterResize(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
    Endproc

    Procedure BottomRightResizer.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.AfterResize(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
    Endproc

    Procedure RightResizer.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ResizeMe(nButton, nShift, nXCoord, nYCoord, .T., .F.)
    Endproc

    Procedure BottomResizer.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ResizeMe(nButton, nShift, nXCoord, nYCoord, .F., .T.)
    Endproc

    Procedure BottomRightResizer.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ResizeMe(nButton, nShift, nXCoord, nYCoord, .T., .T.)
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
    Endproc

    Procedure BeforeResize
        Lparameters nButton, nShift, nXCoord, nYCoord
        If m.nButton=1
            With This
                .StartX = m.nXCoord
                .StartY = m.nYCoord
            Endwith
        Endif
    Endproc

    Procedure AfterResize
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This
            .StartX = 0
            .StartY = 0
        Endwith
    Endproc

    Procedure ResizeMe
        Lparameters nButton, nShift, nXCoord, nYCoord, bWidth, bHeight
        With This
            If m.nButton=1 And This.StartX>0 And .oPostItContainer.bCanResize
                If m.bWidth
                    .Width = .Width + m.nXCoord - .StartX
                Endif
                If m.bHeight
                    .Height = .Height + m.nYCoord - .StartY
                Endif
                .StartX=m.nXCoord
                .StartY=m.nYCoord
            Endif
        Endwith
    Endproc

    Procedure DoAlert
        With This
            .Visible = .T.
            .oPostItContainer.TextArea.SetFocus()
        Endwith
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

    Proc Activate()
        i_curform=This
        Return
    Proc Deactivate()
        i_curform=.Null.
        Return
    Func HasCPEvents(i_cOp)
        Return(.T.)

    Proc ecpDelete()
        If This.oPostItContainer.ecpDelete()
            This.Release()
        Endif
        Return
    Proc ecpEdit()
        If !This.oPostItContainer.bReadonly
            This.oPostItContainer.ShowProperties()
        Endif
        Return
    Proc ecpF6()
        Return
    Proc ecpFilter()
        Return
    Proc ecpLoad()
        Return
    Proc ecpNext()
        Return
    Proc ecpPgUp()
        Return
    Proc ecpPgDn()
        Return
    Proc ecpPrint()
        Return
    Proc ecpPrior()
        Return
    Proc ecpQuit
        This.Release()
    Proc ecpSave()
        Return
    Proc ecpSecurity()
        Return
    Proc ecpZoom()
        Return
    Proc ecpZoomOnZoom()
        Return
    Proc ecpInfo()
        Return
    Func GetHelpFile()
        Return('postit')
    Func GetHelpTopic()
        Return('')
Enddefine

**********************************************************************************

Define Class cp_AdvPostItContainer As Container

    #Define POSTIN_START_COLOR	"Modifica colore postin"
    #Define POSTIN_PROPERTIES	"Mostra impostazioni postin"

    Width=150
    Height=150
    BorderWidth=0
    Name = "CP_ADVPOSTITCONTAINER"
    nNumAttach = 0
    Pinned = .F.
    cOldIcon = ""
    *Appare dal /al (Date)
    * Creato da (numero)
    *Destinatario (numero)
    *Check appare nei link
    *Check integrato nel form
    *Nome pagina (se attivo ultimo check) (testo)
    *Colore postin
    dStartDate = Ctod("  /  /    ")
    dEndDate = Ctod("  /  /    ")
    nOwner = 0
    nToUser = 0
    nInLink = 1
    nInForm = 0
    cPageName = ""
    nStartColor = -1
    nEndColor = -1
    bDeleted = .F.
    nOldTop = 0
    nOldLeft = 0
    nOldHeight = 0
    nHeight = 0
    bCanResize = .F.
    bUpdated = .F.
    cStatus = ""
    nConn = 0
    cVar = ""
    cNewStatus = ""
    nMouseDownX = 0
    nMouseDownY = 0
    nBtnPos=130
    nTxtPos=0
    bDeleted=.F.
    nUser=1
    nCreatedBy=1
    bQueryUnLoad=.F.
    nDimension=This.Height
    cItemTag=''
    Dimension cDataTag[1]
    cDataTag[1]=''
    oParentForm=.Null.
    ShowClose = .F.
    cAction = ""
    nPostItConn = 0
    nStatus = 0
    cOldStatus = ""
    bReadonly = .F.

    Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1

    Add Object imgbackground As Image With ;
        Top = 1, Left = 1 , ;
        Height = 148, Width = 148, ;
        Anchor = 15, Stretch = 2, ;
        MousePointer = 0, ;
        Name = "imgBackground", ;
        OLEDropMode = 1

    Add Object CloseButton As cp_ImgButton With ;
        Top = 8, Left = 181, ;
        Height = 15, Width = 15, ;
        Anchor = 0, Pinnable = .F., Visible = .F., nType=0

    Add Object MenuButton As cp_ImgButton With ;
        Top = 8, Left = 135, ;
        Height = 15, Width = 15, ;
        Anchor = 0, Pinnable = .T., Visible = .T., nType=1

    Add Object SendButton As cp_ImgButton With ;
        Top = 8, Left = 89, ;
        Height = 15, Width = 15, ;
        Anchor = 0, Pinnable = .F., Visible = .T., nType=2

    Add Object SubjectArea As TextBox With ;
        Top = 25, Left = 2, Width = 148, Height = 20, ;
        BackStyle = 1, BorderStyle = 0, SpecialEffect = 2, ;
        Anchor = 10, DisabledBackColor=Rgb(255,255,255), BackColor=Rgb(255,255,255)

    Add Object TextArea As EditBox With ;
        Top = 50, Left = 2, Height = 148, Width = 148, ;
        BackStyle = 1, BorderStyle = 0, SpecialEffect = 2, ;
        EnableHyperlinks = .T., ScrollBars = 0, Anchor = 15, ;
        OLEDropMode = 1, DisabledBackColor=Rgb(255,255,255), BackColor=Rgb(255,255,255)

    Add Object AttachArea As cp_AttachArea With ;
        Top = 90, Left = 2, Width = 58, Height = 30, ;
        Visible = .F., Anchor = 14, OLEDropMode = 1

    Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.ChangeTheme()
    Endproc

    Procedure SetUpPostIt
        Parameters cAction, nServerConn, oCalledForm
        With This
            .cAction = m.cAction
            .nPostItConn = m.nServerConn
            .oParentForm = m.oCalledForm
            If .ShowClose
                .CloseButton.Left = .Width - .CloseButton.Width - 2
                .CloseButton.Anchor = 9
                .CloseButton.Visible=.T.
            Endif
            .TextArea.Anchor = 0
            .Width = Iif(Vartype(.oParentForm.opGFRM)="O",  .oParentForm.opGFRM.PageWidth, .oParentForm.Width)
            .Height = Iif(Vartype(.oParentForm.opGFRM)="O",  .oParentForm.opGFRM.PageHeight, .oParentForm.Height)
            .TextArea.Width = .Width - .TextArea.Left - Iif(.bCanResize, .Parent.RightResizer.Width, 0) - 2
            .TextArea.Anchor = 15
            .SubjectArea.Anchor = 0
            .SubjectArea.Width = .TextArea.Width
            .SubjectArea.ToolTipText = cp_translate("Subject")
            .SubjectArea.Anchor = 10
            .imgbackground.Anchor = 0
            .imgbackground.Height = .Height - .imgbackground.Top - 1
            .imgbackground.Width = .Width - .imgbackground.Left - 1
            .imgbackground.Anchor = 15
            .MenuButton.Left = .Width - .MenuButton.Width - Iif(.ShowClose, .CloseButton.Width + 1, 0) - 2
            .MenuButton.Anchor = 9
            .SendButton.Left = .Width - .SendButton.Width - .MenuButton.Width - Iif(.ShowClose, .CloseButton.Width + 1, 0) - 2
            .SendButton.Anchor = 9
            .AttachArea.Anchor = 0
            .AttachArea.Width = .Width - .AttachArea.Left - Iif(.bCanResize, .Parent.RightResizer.Width, 0) - 2
            .AttachArea.Anchor = 14
            .nConn = m.nServerConn
            Do Case
                Case .cAction = 'new'
                    .cVar = Sys(2015)
                    .nConn=i_ServerConn[1,2]
                    If This.nConn<>0
                        =cp_SQL(This.nConn,"insert into postit (code,usercode,created,createdby,status,postit,wi,he,px,py) values ("+cp_ToStr(This.cVar)+","+cp_ToStr(i_codute)+","+cp_ToStrODBC(Datetime())+","+cp_ToStr(i_codute)+",'D','"+MSG_NO_TEXT_POSTIT+Chr(10)+Chr(13)+'*'+"',150,150,0,0)")
                    Else
                        Insert Into postit (Code,usercode,created,createdby,Status,postit,wi,he,px,py) Values (This.cVar,i_codute,Datetime(),i_codute,'D',MSG_NO_TEXT_POSTIT+Chr(10)+Chr(13)+'*',150,150,0,0)
                    Endif
                Case .cAction = 'set'
                    _Screen.LockScreen = .T.
                    .Visible = .T.
                    .SetPostIT()
                    _Screen.LockScreen = .F.
                Case .cAction = 'contained'
                    This.WarnInForm( .oParentForm )
            Endcase
            .SetPropFromStatus()
            .Anchor = 15
            .Visible = .T.
            If !.bCanResize
                .AttachArea.imgShrink.Click()
            Endif
        Endwith
    Endproc

    Procedure nInForm_assign
        Lparameters nNewVal
        With This
            .nInForm = m.nNewVal
            If .nInForm = 0
                If Left( .cStatus, 1) <> "I"
                    .cStatus = Left( .cStatus, 1)
                Else
                    .cStatus = ''
                Endif
            Else
                .cStatus = Trim( .cStatus)+"I"
            Endif
        Endwith
    Endproc

    Procedure ChangeTheme()
        Local L_OutImg
        With _Screen.cp_Themesmanager
            If This.nStartColor<0 And This.nEndColor<0
                This.imgbackground.Picture = .GetProp(TITLE_BACKGROUND_PICTURE)
            Else
                m.L_OutImg = .VGradient(.GetSingleProp(TITLE_BACKGROUND_PICTURE,9), ;
                    This.Height, ;
                    IIF(This.nStartColor<0, .GetSingleProp(TITLE_BACKGROUND_PICTURE, 2) ,This.nStartColor), ;
                    IIF(This.nEndColor<0, .GetSingleProp(TITLE_BACKGROUND_PICTURE, 3) ,This.nEndColor))
                This.imgbackground.Picture = m.L_OutImg
                If cp_FileExist(m.L_OutImg)
                    Delete File (m.L_OutImg)
                Endif
            Endif
            This.BackColor = .GetProp(TITLE_BORDERCOLOR)
        Endwith
    Endproc

    Procedure MenuButton.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Picture = .GetProp(OVERFLOWPANELFOCUSED_MENUBUTTON_PICTURE)
            This.ImgFocusedPinned.Picture = .GetProp(OVERFLOWPANELFOCUSED_MENUBUTTON_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(OVERFLOWPANEL_MENUBUTTON_PICTURE)
            This.ImgNotFocusedPinned.Picture = .GetProp(OVERFLOWPANEL_MENUBUTTON_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure SendButton.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Picture = .GetProp(SENDBUTTON_FOCUSED_PICTURE)
            This.ImgFocusedPinned.Picture = .GetProp(SENDBUTTON_FOCUSEDPINNED_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(SENDBUTTON_PICTURE)
            This.ImgNotFocusedPinned.Picture = .GetProp(SENDBUTTON_PINNED_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure Pinned_assign
        Lparameters bNewVal
        With This
            .Pinned=.F.
            Define Popup ShortcutMenu From Mrow(), Mcol() Relative Shortcut
            Define Bar 1 Of ShortcutMenu Prompt (POSTIN_PROPERTIES)
            On Selection Bar 1 Of ShortcutMenu .ShowProperties()
            Define Bar 2 Of ShortcutMenu Prompt (POSTIN_START_COLOR)
            On Selection Bar 2 Of ShortcutMenu .DefineColors()
            Activate Popup ShortcutMenu
            Clear Popups
        Endwith
    Endproc

    Procedure ShowProperties
        Local oSettingForm
        With This
            .MenuButton.ChangeBackground(.F.)
            m.oSettingForm = Createobject("cp_SetPostInProp", This)
            m.oSettingForm.Top = .Top
            m.oSettingForm.Left = .Left
            m.oSettingForm.Show(1)
            .ChangeTheme()
        Endwith
    Endproc

    Procedure DefineColors
        Local oSettingForm
        With This
            .MenuButton.ChangeBackground(.F.)
            m.oSettingForm = Createobject("cp_SetPostInColor", This, ;
                IIF(.nStartColor<0, _Screen.cp_Themesmanager.GetSingleProp(TITLE_BACKGROUND_PICTURE, 2) , .nStartColor), ;
                IIF(.nEndColor<0, _Screen.cp_Themesmanager.GetSingleProp(TITLE_BACKGROUND_PICTURE, 3) , .nEndColor))
            m.oSettingForm.Top = .Top + (.Height/2)-(m.oSettingForm.Height/2)
            m.oSettingForm.Left = .Left
            m.oSettingForm.Show(1)
            .ChangeTheme()
        Endwith
    Endproc

    Procedure TextAssign
        Lparameters cNewVal
        If Len(m.cNewVal)>254
            m.cNewVal = Left(m.cNewVal, 254)
        Endif
        This.TextArea.Value = m.cNewVal
    Endproc

    Procedure ActivateScrollBars
        Lparameters oTextArea
        *If this.TextHeight(m.oTextArea.Value)>m.oTextArea.Height
        m.oTextArea.ScrollBars = 2
        *else
        *	m.oTextArea.ScrollBars = 0
        *EndIf
    Endproc

    Procedure Resize
        With This
            .ActivateScrollBars( .TextArea )
        Endwith
    Endproc

    Procedure TextArea.InteractiveChange
        With This.Parent
            .bUpdated = .T.
            .ActivateScrollBars(This)
        Endwith
    Endproc

    Procedure TextArea.ProgrammaticChange
        With This.Parent
            .bUpdated = .T.
            .ActivateScrollBars(This)
        Endwith
    Endproc

    Procedure TextArea.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton=1
            Nodefault
            * --- per ritardare un po il drag e drop
            With This.Parent
                If .OLEDropHasData <= 0
                    .nMouseDownX=nXCoord
                    .nMouseDownY=nYCoord
                Endif
            Endwith
        Endif
    Endproc

    Procedure TextArea.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.Parent.nMouseDownX>=0 And This.Parent.nMouseDownY>=0
            If nButton=1 And (Abs(nXCoord-This.Parent.nMouseDownX)+Abs(nYCoord-This.Parent.nMouseDownY))>8
                Nodefault
                With This
                    If .Parent.OLEDropHasData <=0
                        .DragIcon=i_cBmpPath+"DragMove.cur"
                        .Drag(1)
                    Endif
                Endwith
            Endif
        Endif
    Endproc

    Procedure TextArea.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        With This.Parent
            .MyDragDrop(This, oSource, nXCoord, nYCoord)
        Endwith
    Endproc

    Procedure imgbackground.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        This.Parent.MyDragDrop(This,oSource, nXCoord, nYCoord)
    Endproc

    Procedure TextArea.DragOver
        Lparameters oSource, nXCoord, nYCoord, nState
        This.Parent.MyDragOver(This, oSource, nXCoord, nYCoord, nState)
    Endproc

    Procedure MyDragOver
        Lparameters oParent, oSource, nXCoord, nYCoord, nState
        Do Case
            Case nState=0   && Enter
                This.cOldIcon=oSource.DragIcon
                If oSource.ParentClass='Stdcontainer' Or oSource.Name='STDSUSR' Or oSource.Name='STDSGRP'
                    oSource.DragIcon=i_cBmpPath+"cross01.cur"
                Endif
            Case nState=1   && Leave
                oSource.DragIcon=This.cOldIcon
        Endcase
    Endproc

    Procedure MyDragDrop
        Lparameters oParent, oSource, nXCoord, nYCoord
        With This
            If Upper(m.oParent.Parent.Name)<>"OWARNING" And m.oParent.Parent.Name<>m.oSource.Parent.Name And !.bReadonly
                .AttachArea.AddItem(m.oSource,"O")
            Endif
            .nMouseDownX=-1
            .nMouseDownY=-1
        Endwith
    Endproc

    Procedure  imgbackground.OLEDragOver
        Lparameters oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        This.Parent.MyOLEDragOver(This, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState)
    Endproc

    Procedure TextArea.OLEDragOver
        Lparameters oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        If !oDataObject.GetFormat(1)
            This.Parent.MyOLEDragOver(This, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState)
        Endif
    Endproc

    Procedure MyOLEDragOver
        Lparameters oParent, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        If (oDataObject.GetFormat(15) Or oDataObject.GetFormat(1))
            oParent.OLEDropHasData=1
            oParent.OLEDropEffects=5
        Endif
    Endproc

    Procedure TextArea.OLEDragDrop
        Lparameters oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        If !oDataObject.GetFormat(1)
            This.Parent.MyOLEDragDrop(This, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord)
        Endif
    Endproc

    Procedure  imgbackground.OLEDragDrop
        Lparameters oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        This.Parent.MyOLEDragDrop(This, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord)
    Endproc

    Procedure MyOLEDragDrop
        Lparameters oParent, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        Local sURL
        If oParent.OLEDropEffects>1 And !This.bReadonly
            If oDataObject.GetFormat(15)
                *---D&D iniziato da file
                Dimension i_aFiles[1]
                i_aFiles[1]=''
                oDataObject.GetData(15,@i_aFiles)
                For I=1 To Alen(i_aFiles)
                    This.AttachArea.AddItem(i_aFiles[i],"F")
                Next
            Else
                If oDataObject.GetFormat(1)
                    m.sURL = oDataObject.GetData(1)
                    If At("http",Lower(sURL)) <> 0
                        This.AttachArea.AddItem(m.sURL, "U")
                    Endif
                Endif
            Endif
            oParent.OLEDropHasData = 0
            This.nMouseDownX = -1
            This.nMouseDownY = -1
        Endif
    Endproc

    Procedure relasePostIT
        This.HideForm()
    Endproc

    Procedure ecpDelete
        With This
            If cp_YesNo(MSG_CONFIRM_DELETING_POSTIN_QP)
                If .nConn<>0
                    cp_SQL( .nConn,"delete from postit where code="+cp_ToStr( .cVar ))
                    SQLExec( .nConn,"delete from cpwarn where warncode='"+ .cVar+"'") && in db2 CP_SQL da un messaggio di errore
                Else
                    Delete From postit Where Code = .cVar
                    Delete From cpwarn Where warncode = .cVar
                Endif
                .bDeleted = .T.
                .HideForm()
            Endif
        Endwith
    Endproc

    Procedure HideForm
        *Salvataggio postin
        Local xcg,I,fo,i_n,i_nL,i_nT
        If Not(This.bDeleted)
            * --- salva il valore corrente nel database
            i_nL=This.nOldLeft
            i_nT=This.nOldTop
            If This.bCanResize
                i_nL=This.Parent.Left
                i_nT=This.Parent.Top
            Endif
            If (This.bUpdated Or i_nT<>This.nOldTop Or i_nL<>This.nOldLeft) And !This.bReadonly
                xcg=Createobject('pxcg','save')
                This.Serialize(xcg,.F.)
                If This.bCanResize
                    If This.nConn<>0
                        =cp_SQL(This.nConn,"update postit set subject="+cp_ToStrODBC(This.SubjectArea.Value)+", postit="+cp_ToStrODBC(xcg.cText)+", px="+cp_ToStrODBC(i_nL)+", py="+cp_ToStrODBC(i_nT)+", wi="+cp_ToStrODBC(This.Width)+", he="+cp_ToStrODBC(This.Height)+", datestart="+cp_ToStrODBC(This.dStartDate)+", datestop="+cp_ToStrODBC(This.dEndDate)+", status="+cp_ToStrODBC(This.cStatus)+" where code="+cp_ToStrODBC(This.cVar))
                    Else
                        Update postit Set subejct=This.SubjectArea.Value, postit=xcg.cText, px=i_nL, py=i_nT, wi=This.Width, he=This.Height, datestart=This.dStartDate, datestop=This.dEndDate, Status=This.cStatus Where Code=This.cVar
                    Endif
                Else &&quando il warning � integrato non aggiorno le coordinate del form
                    i_n=This.nOldHeight
                    If This.nConn<>0
                        =cp_SQL(This.nConn,"update postit set subject="+cp_ToStrODBC(This.SubjectArea.Value)+",postit="+cp_ToStrODBC(xcg.cText)+", he="+cp_ToStrODBC(i_n)+", datestart="+cp_ToStrODBC(This.dStartDate)+", datestop="+cp_ToStrODBC(This.dEndDate)+", status="+cp_ToStrODBC(This.cStatus)+" where code="+cp_ToStrODBC(This.cVar))
                    Else
                        Update postit Set subejct=This.SubjectArea.Value, postit=xcg.cText, he=i_n, datestart=This.dStartDate, datestop=This.dEndDate, Status=This.cStatus Where Code=This.cVar
                    Endif
                Endif
            Endif
            *---Inserisce il postit nel folder
            If This.cStatus = "D" Then
                If This.nConn<>0
                    =cp_SQL(This.nConn,"update postit set status='F' where code="+cp_ToStr(This.cVar))
                Else
                    Update postit Set Status='F' Where Code=This.cVar
                Endif
                For I=1 To _Screen.FormCount
                    fo=_Screen.Forms(I)
                    Do Case
                        Case Lower(fo.Class)="postitfolder"
                            fo.FillPostit()
                        Case Lower(fo.Class)="tcp_postitfolder"
                            fo.NotifyEvent("ZoomRefresh")
                    Endcase
                Next
            Endif
        Endif
        If This.bCanResize
            This.oParentForm.Release()
        Endif
    Endproc

    Procedure SetPropFromStatus
        With This
            If .cStatus <> "W" And .cStatus <> "A" And At("I",.cStatus) = 0
                .cStatus = "D"
                .nInLink = 1
            Else
                If At("W", .cStatus) <> 0
                    .nInLink = 1
                    If At("I", .cStatus) <> 0
                        .nInForm = 1
                    Endif
                Else
                    .nInLink = 0
                    If At("I", .cStatus) <> 0
                        .nInForm = 0
                    Endif
                Endif
            Endif
        Endwith
    Endproc

    Procedure SetPostIT
        Local pname,xcg
        With This
            .cStatus=Alltrim(postit_c.Status)
            .cOldStatus = .cStatus
            .SetPropFromStatus()
            If .cStatus='F'
                .cStatus = 'D'
            Endif
            xcg=Createobject('pxcg','load',postit_c.postit)
            .cVar=postit_c.Code
            .Parent.Top=postit_c.py
            .nOldTop=This.Parent.Top
            .Parent.Left=postit_c.px
            .nOldLeft=This.Parent.Left
            .Parent.Width=postit_c.wi
            .Parent.Height=postit_c.he
            .nOldHeight=postit_c.he
            .dStartDate = Iif(Isnull(postit_c.datestart), Ctod('  /  /  '), postit_c.datestart)
            .dEndDate = Iif(Isnull(postit_c.datestop), Ctod('  /  /  '), postit_c.datestop)
            .nToUser = postit_c.usercode
            .nOwner = postit_c.createdby
            .SubjectArea.Value = postit_c.subject
            *--- Postit in sola lettura
            If .nOwner=i_codute And .nToUser<>.nOwner
                .bReadonly = .T.
                .TextArea.ReadOnly=.T.
                .SubjectArea.ReadOnly=.T.
                .MenuButton.Visible=.F.
                .SendButton.Visible=.F.
            Endif
            *--- Postit in sola lettura
            .Serialize(xcg,.F.)
            .ChangeTheme()
            .bUpdated=.F.
        Endwith
    Endproc

    Procedure WarnInForm
        Lparameters oForm
        Local pname,xcg
        With This
            .bCanResize = .F.
            If Isnull(.oParentForm)
                .oParentForm = m.oForm
            Endif
            m.pname=postit_c.Code
            Public &pname
            &pname=This
            .cStatus=postit_c.Status
            xcg=Createobject('pxcg','load',postit_c.postit)
            .cVar=m.pname
            .nOldHeight=postit_c.he
            .dStartDate = Iif(Isnull(postit_c.datestart), Ctod('  /  /  '), postit_c.datestart)
            .dEndDate = Iif(Isnull(postit_c.datestop), Ctod('  /  /  '), postit_c.datestop)
            .nToUser = postit_c.usercode
            .nOwner = postit_c.createdby
            .Serialize(xcg,.T.)
            .bUpdated=.F.
        Endwith
    Endproc


    Procedure Serialize
        Lparameters xcg, bLockedForm
        Local nLstHeight,I,nItem,cKey,cIcon,nCount,cComment,cPrg, oAttach, cType
        If xcg.bIsStoring
            xcg.Save('C',Iif(Empty(This.TextArea.Value), MSG_NO_TEXT_POSTIT ,This.TextArea.Value))
            nLstHeight = This.Height
            xcg.Save("N",nLstHeight)
            *---Scrittura degli allegati
            nItem=This.nNumAttach
            xcg.Save("N",nItem)
            xcg.Save("N",130)
            xcg.Container(This)
            For I = 1 To nItem
                m.oAttach = This.AttachArea.oATTACHELEMENTCOL.Item(m.I)
                xcg.Save("C", m.oAttach.cType)
                xcg.Save("C", m.oAttach.lblCaption.Caption)
                xcg.Save("C", m.oAttach.imgIcon.Picture)
                If m.oAttach.cType="O"
                    xcg.Save("C", m.oAttach.cPrg)
                    xcg.Save("N", Alen(m.oAttach.xKey,1))
                    For nAtt=1 To Alen(m.oAttach.xKey,1)
                        xcg.Save("X", m.oAttach.xKey[m.nAtt])
                    Next
                    xcg.Save('C',m.oAttach.cAzi)
                Endif
            Next
            xcg.EndContainer()
            xcg.Save('N',This.nStartColor)
            xcg.Save("C",This.cPageName)
            xcg.Save('N',This.nEndColor)
            xcg.Eoc()
        Else
            This.TextArea.Value=xcg.Load('C','')
            If This.TextArea.Value=MSG_NO_TEXT_POSTIT
                This.TextArea.Value=""
            Endif
            nLstHeight = xcg.Load("N",157)
            *---Lettura degli allegati
            nCount = xcg.Load("N",0)
            nCount=Int(nCount)
            This.nHeight = nLstHeight
            aa = xcg.Load("N",130)
            xcg.Container(This)
            For I = 1 To nCount
                xcg.Load('C','')
                m.cType = xcg.Load('C','')
                cComment=xcg.Load("C","")
                cIcon=xcg.Load("C","")
                If m.cType<>"O"
                    This.AttachArea.AddItem(m.cComment,m.cType)
                Else
                    cPrg=xcg.Load("C","")
                    nKey=xcg.Load('N',0)
                    m.nKey=Int(m.nKey)
                    Dimension xKey[max(1,m.nKey)]
                    For j=1 To m.nKey
                        m.xKey[j]=xcg.Load('X','')
                    Next
                    m.oAttach = This.AttachArea.AddItem(m.cComment,m.cType)
                    m.oAttach.cPrg = m.cPrg
                    m.oAttach.SetSourceKey(@m.xKey)
                    m.oAttach.cAzi=xcg.Load('C','')
                Endif
                xcg.Load('C','')
            Next
            This.nStartColor=xcg.Load('N',-1)
            This.cPageName=xcg.Load("C","")
            This.nEndColor=xcg.Load('N',-1)
            xcg.Eoc()
        Endif
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine

*****************************************************************************************

Define Class cp_SetPostInProp As Form

    #Define LABEL_DATE_START	"Appare dal"
    #Define	LABEL_DATE_END	"Fino al"
    #Define	LABEL_TO_USER	"Destinatario"
    #Define	LABEL_OWNER		"Creato da"
    #Define	LABEL_CHK_LINK	"Appare nei link"
    #Define LABEL_CHK_IN_FORM	"Integrato nel form"
    #Define	LABEL_PAGE_NAME	"Nome pagina"
    #Define LABEL_SETUP_COLORS	"Colori postin"

    Width = 300
    Height = 300
    MinWidth = 300
    MinHeight = 300
    WindowType = 1
    ShowInTaskbar = .F.
    BorderStyle = 0
    Caption = ""
    MaxButton = .F.
    MinButton = .F.
    TitleBar = 0
    AlwaysOnTop = .T.
    Top = 0
    Left = 0
    nStatus = 0
    ShowClose = .T.
    Name = "CP_SetPostInColor"
    oParentObject = .Null.

    Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1

    Add Object shpBorder As Shape With ;
        Top = 1, Left = 1, Height = 200, Width = 200, ;
        Anchor = 0, BorderWidth = 1, BorderStyle = 1, ;
        Visible = .T., BackStyle = 0

    Add Object imgbackground As Image With ;
        Top = 1, Left = 1 , ;
        Height = 398, Width = 298, ;
        Anchor = 15, Stretch = 2, ;
        MousePointer = 0, ;
        Name = "imgBackground", ;
        OLEDropMode = 1

    Add Object TopGripper As cp_Gripper

    Add Object CloseButton As cp_ImgButton With ;
        Top = 8, Left = 281, ;
        Height = 15, Width = 15, ;
        Anchor = 0, Pinnable = .F., Visible = .F.

    Add Object OkButton As cp_ImgButton With ;
        Top = 280, Left = 281, ;
        Height = 15, Width = 15, ;
        Anchor = 12, Pinnable = .F., Visible = .T.

    Add Object lblDateStart As Label With ;
        TOP = 30, Left = 5, AutoSize = .F. , ;
        Caption = LABEL_DATE_START, ;
        Anchor = 11, Visible = .T., BackStyle = 0, ;
        Alignment = 1, Width = 90

    Add Object txtDateStart As TextBox With ;
        TOP = 28, Left = 100, Width = 70, ;
        Anchor = 9, Visible = .T., ;
        InputMask = "99/99/9999"

    Add Object lblDateEnd As Label With ;
        TOP = 60, Left = 5, AutoSize = .F. , ;
        Caption = LABEL_DATE_END, ;
        Anchor = 11, Visible = .T., BackStyle = 0, ;
        Alignment = 1, Width = 90

    Add Object txtDateEnd As TextBox With ;
        TOP = 58, Left = 100, Width = 70, ;
        Anchor = 9, Visible = .T., ;
        InputMask = "99/99/9999"

    Add Object lblOwner As Label With ;
        TOP = 90, Left = 5, AutoSize = .F. , ;
        Caption = LABEL_OWNER, ;
        Anchor = 11, Visible = .T., BackStyle = 0, ;
        Alignment = 1, Width = 90

    Add Object txtOwner As TextBox With ;
        TOP = 88, Left = 100, Width = 40, ;
        Anchor = 9, Visible = .T., Enabled = .F. , ;
        InputMask = "9999"

    Add Object txtOwnerDes As TextBox With ;
        TOP = 88, Left = 150, Width = 110, ;
        Anchor = 9, Visible = .T., Enabled = .F.

    Add Object lblToUser As Label With ;
        TOP = 120, Left = 5, AutoSize = .F. , ;
        Caption = LABEL_TO_USER, ;
        Anchor = 11, Visible = .T., BackStyle = 0, ;
        Alignment = 1, Width = 90

    Add Object txtToUser As TextBox With ;
        TOP = 118, Left = 100, Width = 40, ;
        Anchor = 9, Visible = .T., Enabled = .F. , ;
        InputMask = "9999"

    Add Object txtToUserDes As TextBox With ;
        TOP = 118, Left = 150, Width = 110, ;
        Anchor = 9, Visible = .T., Enabled = .F.

    Add Object chkLink As Checkbox With ;
        TOP = 150, Left = 100, Caption = LABEL_CHK_LINK, ;
        Value = 1, BackStyle = 0, Width = 100

    Add Object chkInForm As Checkbox With ;
        Top = 180, Left = 100, Caption = LABEL_CHK_IN_FORM, ;
        Value = 0, BackStyle = 0, Width = 150

    Add Object lblPageName As Label With ;
        TOP = 210, Left = 100, AutoSize = .F. , ;
        Caption = LABEL_PAGE_NAME, ;
        Anchor = 11, Visible = .F., BackStyle = 0, ;
        Alignment = 1, Width = 90

    Add Object txtPageName As TextBox With ;
        TOP = 208, Left = 195, Width = 100, ;
        Anchor = 9, Visible = .F.

    Add Object lblSetupColors As Label With ;
        TOP = 240, Left = 5, AutoSize = .F. , ;
        Caption = LABEL_SETUP_COLORS, ;
        Anchor = 11, Visible = .T., BackStyle = 0, ;
        Alignment = 1, Width = 90

    Add Object ColorButton As cp_ImgButton With ;
        Top = 240, Left = 100, ;
        Height = 15, Width = 15, ;
        Anchor = 0, Pinnable = .F., Visible = .T. , ;
        MousePointer = 15, ImgFocused.MousePointer = 15

    Procedure Init
        Lparameters oParent
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            If .ShowClose
                .CloseButton.Anchor = 9
                .CloseButton.Visible=.T.
            Endif
            .TopGripper.Anchor = 0
            .TopGripper.Width = .Width - 2
            .TopGripper.Anchor = 11
            .imgbackground.Anchor = 0
            .imgbackground.Height = .Height - .imgbackground.Top - 1
            .imgbackground.Anchor = 15
            .oParentObject = m.oParent
            .txtDateStart.Value = .oParentObject.dStartDate
            .txtDateEnd.Value = .oParentObject.dEndDate
            .txtOwner.Value = .oParentObject.nOwner
            If i_ServerConn[1,2]<>0
                =cp_SQL(i_ServerConn[1,2],"select name from cpusers where code="+cp_ToStrODBC(.oParentObject.nOwner),"users_c")
            Else
                Select Name From cpusers Where Code=.oParentObject.nOwner Into Cursor users_c
            Endif
            .txtOwnerDes.Value = users_c.Name
            Use In Select("users_c")
            .txtToUser.Value = .oParentObject.nToUser
            If i_ServerConn[1,2]<>0
                =cp_SQL(i_ServerConn[1,2],"select name from cpusers where code="+cp_ToStrODBC(.oParentObject.nToUser),"users_c")
            Else
                Select Name From cpusers Where Code=.oParentObject.nToUser Into Cursor users_c
            Endif
            .txtToUserDes.Value = users_c.Name
            Use In Select("users_c")
            .chkLink.Value = .oParentObject.nInLink
            .chkInForm.Value = .oParentObject.nInForm
            .txtPageName.Value = .oParentObject.cPageName
            .ChangeTheme()
        Endwith
    Endproc

    Procedure txtDateStart.Valid
        Return .T.
    Endproc

    Procedure chkInForm.ProgrammaticChange
        This.SetPageNameVisibility()
    Endproc

    Procedure chkInForm.InteractiveChange
        This.SetPageNameVisibility()
    Endproc

    Procedure chkInForm.SetPageNameVisibility
        With This.Parent
            .lblPageName.Visible = This.Value = 1
            .txtPageName.Visible = This.Value = 1
        Endwith
    Endproc

    Procedure OkButton.Click
        With This.Parent
            .oParentObject.dStartDate= .txtDateStart.Value
            .oParentObject.dEndDate = .txtDateEnd.Value
            .oParentObject.nOwner = .txtOwner.Value
            .oParentObject.nToUser = .txtToUser.Value
            .oParentObject.nInLink = .chkLink.Value
            .oParentObject.nInForm = .chkInForm.Value
            .oParentObject.cPageName = .txtPageName.Value
            .oParentObject.bUpdated = .T.
            With .oParentObject
                If .cStatus = "W" Or .cStatus = "A" Or At("I",.cStatus) <> 0 Then
                    If Not .cStatus==.cOldStatus
                        If At("I",.cStatus) <> 0
                            cp_msg(cp_translate(MSG_POSTIN_WILL_BE_INTEGRATED_IN_THE_WINDOW_WHEN_NEXT_RECORD_WILL_BE_READ_F),.F.)
                        Else
                            cp_msg(cp_translate(MSG_POSTIN_WILL_BE_DETACHED_FROM_THE_WINDOW_WHEN_NEXT_RECORD_WILL_BE_READ_F),.F.)
                        Endif
                    Endif
                Endif
            Endwith
        Endwith
        DoDefault()
    Endproc

    Procedure OkButton.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Picture = .GetProp(OKBUTTONFOCUSED_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(OKBUTTON_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure ColorButton.Click
        This.Parent.oParentObject.DefineColors()
    Endproc

    Procedure ColorButton.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Picture = .GetProp(DOTSBUTTONFOCUSED_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(DOTSBUTTON_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure ChangeTheme()
        Local L_OutImg
        With _Screen.cp_Themesmanager
            This.imgbackground.Picture = .GetProp(TITLE_BACKGROUND_PICTURE)
            This.BackColor = .GetProp(TITLE_BORDERCOLOR)
        Endwith
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
    Endproc

    Procedure HideForm
        This.Release
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine

Define Class cp_SetPostInColor As Form

    #Define LABEL_START_COLOR	"Colore iniziale"
    #Define LABEL_END_COLOR	"Colore finale"

    Width = 200
    Height = 100
    MinWidth = 200
    MinHeight = 100
    WindowType = 1
    ShowInTaskbar = .F.
    BorderStyle = 0
    Caption = ""
    MaxButton = .F.
    MinButton = .F.
    TitleBar = 0
    AlwaysOnTop = .T.
    Top = 0
    Left = 0
    nStatus = 0
    ShowClose = .T.
    Name = "CP_SetPostInColor"
    oParentObject = .Null.

    Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1

    Add Object shpBorder As Shape With ;
        Top = 1, Left = 1, Height = 200, Width = 200, ;
        Anchor = 0, BorderWidth = 1, BorderStyle = 1, ;
        Visible = .T., BackStyle = 0

    Add Object imgbackground As Image With ;
        Top = 1, Left = 1 , ;
        Height = 198, Width = 198, ;
        Anchor = 15, Stretch = 2, ;
        MousePointer = 0, ;
        Name = "imgBackground", ;
        OLEDropMode = 1

    Add Object TopGripper As cp_Gripper

    Add Object CloseButton As cp_ImgButton With ;
        Top = 8, Left = 181, ;
        Height = 15, Width = 15, ;
        Anchor = 0, Pinnable = .F., Visible = .F.

    Add Object lblStartColor As Label With ;
        TOP = 30, Left = 5, AutoSize = .F. , ;
        Caption = LABEL_START_COLOR, ;
        Anchor = 11, Visible = .T., BackStyle = 0, ;
        Alignment = 1, Width = 90

    Add Object lblEndColor As Label With ;
        TOP = 60, Left = 5, AutoSize = .F. , ;
        Caption = LABEL_END_COLOR, ;
        Anchor = 11, Visible = .T., BackStyle = 0, ;
        Alignment = 1, Width = 90

    Add Object shpStartColor As Shape With ;
        Top = 28, Left = 100, Width = 20, Height = 20 , ;
        FillStyle = 0, BorderStyle = 1, BackStyle = 0, ;
        Anchor = 9, BorderWidth = 1, BorderColor = 0, ;
        MousePointer = 15

    Add Object shpEndColor As Shape With ;
        Top = 58, Left = 100, Width = 20, Height = 20 , ;
        FillStyle = 0, BorderStyle = 1, BackStyle = 0, ;
        Anchor = 9, BorderWidth = 1, BorderColor = 0, ;
        MousePointer = 15

    Add Object imgExample As Image With ;
        Top = 28, Left = 130, Height = 50, Width = 50, ;
        BorderStyle = 0, BackStyle = 0,Anchor = 13, ;
        BorderWidth = 1, BorderColor = 0, Themed = .F.

    Add Object shpExample As Shape With ;
        Top = 27, Left = 129, Width = 51, Height = 51 , ;
        FillStyle = 1, BorderStyle = 1, BackStyle = 0, ;
        Anchor = 13, BorderWidth = 1, BorderColor = 0

    Add Object OkButton As cp_ImgButton With ;
        Top = 83, Left = 181, ;
        Height = 15, Width = 15, ;
        Anchor = 12, Pinnable = .F., Visible = .T.

    Procedure Init
        Lparameters oParent, nStartColor, nEndColor
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            If .ShowClose
                .CloseButton.Anchor = 9
                .CloseButton.Visible=.T.
            Endif
            .TopGripper.Anchor = 0
            .TopGripper.Width = .Width - 2
            .TopGripper.Anchor = 11
            .imgbackground.Anchor = 0
            .imgbackground.Height = .Height - .imgbackground.Top - 1
            .imgbackground.Anchor = 15
            .shpStartColor.FillColor = m.nStartColor
            .shpEndColor.FillColor = m.nEndColor
            .oParentObject = m.oParent
            .ChangeTheme()
            .ShowExample()
        Endwith
    Endproc

    Procedure OkButton.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Picture = .GetProp(OKBUTTONFOCUSED_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(OKBUTTON_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure OkButton.Click
        With This.Parent.oParentObject
            .nStartColor = This.Parent.shpStartColor.FillColor
            .nEndColor = This.Parent.shpEndColor.FillColor
            .bUpdated = .T.
            .ChangeTheme()
        Endwith
        DoDefault()
    Endproc

    Procedure ShowExample
        Local L_OutImg
        With This
            m.L_OutImg = _Screen.cp_Themesmanager.VGradient(This.imgExample.Width, ;
                This.imgExample.Height, .shpStartColor.FillColor , .shpEndColor.FillColor)
            .imgExample.Picture = m.L_OutImg
            If File(m.L_OutImg)
                Delete File (m.L_OutImg)
            Endif
        Endwith
    Endproc

    Procedure shpStartColor.DblClick
        Local nColor
        m.nColor = Getcolor(This.FillColor)
        This.FillColor = m.nColor
        This.Parent.ShowExample()
    Endproc

    Procedure shpEndColor.DblClick
        Local nColor
        m.nColor = Getcolor(This.FillColor)
        This.FillColor = m.nColor
        This.Parent.ShowExample()
    Endproc

    Procedure ChangeTheme()
        Local L_OutImg
        With _Screen.cp_Themesmanager
            This.imgbackground.Picture = .GetProp(TITLE_BACKGROUND_PICTURE)
            This.BackColor = .GetProp(TITLE_BORDERCOLOR)
        Endwith
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
    Endproc

    Procedure HideForm
        This.Release
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine

**********************************************************************************

Define Class cp_AttachArea As Container
    BorderWidth = 0
    BackStyle = 0
    Anchor = 14
    Width = 188
    nStatus = 0
    Pinned = .F.
    nIndexSelected = -1
    OLEDropMode = 1

    Add Object oATTACHELEMENTCOL As Collection

    Add Object imgDelete As cp_ImgButton With ;
        Pinnable = .F., Pinned = .F., Visible = .F., ;
        Top = 0 , Left = 0, Anchor = 3, ;
        Height = 15, Width = 15

    Add Object imgShrink As cp_ImgButton With ;
        Pinnable = .T., Pinned = .F. , Visible = .T., ;
        Top = 0 , Left = 15, Anchor = 0, ;
        Height = 15, Width = 15

    Procedure Init
        With This
            .imgShrink.Left = .Width - .imgShrink.Width - 2
            .imgShrink.Anchor = 9
            With .imgShrink
                .ImgFocused.MousePointer = 15
                .ImgFocusedPinned.MousePointer = 15
                .ImgNotFocused.MousePointer = 15
                .ImgNotFocusedPinned.MousePointer = 15
            Endwith
        Endwith
    Endproc

    Procedure imgShrink.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Picture = .GetProp(VERTICALSHRINKBUTTON_NOTSHRUNK_PICTURE)
            This.ImgFocusedPinned.Picture = .GetProp(VERTICALSHRINKBUTTON_SHRUNK_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(VERTICALSHRINKBUTTON_NOTSHRUNK_PICTURE)
            This.ImgNotFocusedPinned.Picture = .GetProp(VERTICALSHRINKBUTTON_SHRUNK_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure imgDelete.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Picture = .GetProp(DELETEBUTTONFOCUSED_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(DELETEBUTTON_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure Pinned_assign
        Lparameters bNewVal
        With This
            .Pinned = Iif(.Parent.bCanResize, bNewVal, !bNewVal)
            .imgDelete.Visible = !.Pinned And .nIndexSelected>=0
            .ResizeParent()
        Endwith
    Endproc

    Procedure OLEDragOver
        Lparameters oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        If !oDataObject.GetFormat(1)
            This.Parent.MyOLEDragOver(This, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState)
        Endif
    Endproc

    Procedure OLEDragDrop
        Lparameters oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        If !oDataObject.GetFormat(1)
            This.Parent.MyOLEDragDrop(This, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord)
        Endif
    Endproc

    Procedure HideForm
        *In realt� eliminiamo l'attach attualmente selezionato
        *il metodo � chiamato cos� per ereditare il pulsante con immagine
        With This
            If !.Parent.bReadonly
                For Each oElem In .oATTACHELEMENTCOL
                    If oElem.nIndex = .nIndexSelected
                        If cp_YesNo("Eliminare l'elemento selezionato", 32)
                            .RemoveItem(oElem)
                            .imgDelete.Visible=.F.
                        Endif
                    Endif
                Next
            Else
                cp_msg("Controllo in sola lettura")
            Endif
        Endwith
    Endproc

    Procedure AddItem
        Lparameters oAttach, cObjType
        Local oAttElem, cResult
        With This
            m.oAttElem = "cp_AE"+Alltrim(Sys(2015))
            Do Case
                Case m.cObjType ="O"
                    *Oggetto gestionale (Cerco l'icona nelle propriet�)
                    m.cResult = ""
                Case m.cObjType ="F"
                    *File	(Cerco l'icona nel registry o nel file)
                    m.cResult = _Screen.cp_Themesmanager.GetBmpFromRegistryIco(m.oAttach)+".bmp"
                Case m.cObjType ="U"
                    *Internet Url	(Icona predefinita)
                    m.cResult = "root.bmp"
            Endcase
            If Vartype(m.oAttach)="O" And (m.cObjType="O" And (m.oAttach.Name='STDSUSR' Or m.oAttach.Name='STDSGRP'))
                Do Case
                    Case m.oAttach.Name='STDSUSR'
                        This.SendTo(_cpusers_.Code,_cpusers_.Name)
                    Case m.oAttach.Name='STDSGRP'
                        This.SendToGroup(_cpgroups_.Code,_cpgroups_.Name)
                Endcase
            Else
                .Parent.nNumAttach = .Parent.nNumAttach + 1
                If Vartype(m.oAttach)="C" Or (Vartype(m.oAttach)<>"C" And Upper(m.oAttach.Parent.Name) <> "OPOSTITCONTAINER" And Upper(m.oAttach.Parent.Name)<>"OWARNING")
                    .AddObject( m.oAttElem , "cp_AttachElement" , m.cResult ,  m.oAttach, m.cObjType )
                    m.oAttElem = "this."+m.oAttElem
                    m.oAttElem = &oAttElem
                    m.oAttElem.Anchor = 11
                    .oATTACHELEMENTCOL.Add(m.oAttElem)
                    m.oAttElem.nIndex = .oATTACHELEMENTCOL.Count
                    .ReorgAttach()
                    .ResizeParent()
                    Return m.oAttElem
                Endif
            Endif
        Endwith
    Endproc

    Procedure SendTo
        Lparameters nUsercode,cName,bNoRequest
        With This.Parent
            If .cStatus='D'
                If i_codute=m.nUsercode
                    cp_msg(cp_translate(MSG_YOU_CANNOT_SEND_MSG_TO_YOURSELF),.F.)
                Else
                    If bNoRequest Or cp_YesNo(cp_MsgFormat(MSG_SEND_POSTIN_TO__C__,cp_ToStr(m.nUsercode),m.cName))
                        If .nConn<>0
                            =cp_SQL(.nConn,"update postit set usercode="+cp_ToStr(m.nUsercode)+", status='P' where code="+cp_ToStr(.cVar))
                        Else
                            Update postit Set usercode=m.nUsercode,Status='P' Where Code = .cVar
                        Endif
                        .cStatus='P'
                        .relasePostIT()
                    Endif
                Endif
            Else
                cp_msg(cp_translate(MSG_WHEN_A_POSTIN_IS_ADDED_TO_A_RECORD_IT_CANNOT_BE_SENT_TO_ANOTHER_USER),.F.)
            Endif
        Endwith
    Endproc

    Procedure SendToGroup
        Lparameters nGroupcode,cName,bNoRequest,cCurUsers
        With This.Parent
            If .cStatus='D'
                * --- Zucchetti Aulla Aggiuno bNORequest or (invio postit a gruppo)
                * --- sostituita la Update..
                If bNoRequest Or cp_YesNo(cp_MsgFormat(MSG_SEND_POSTIN_TO__C__,cp_ToStr(nGroupcode),cName))
                    * --- Zucchetti Aulla Inizio - Se trascino un gruppo sul post it invio il post it a tutti gli utenti del gruppo..
                    * --- creo un temporaneo con gli utenti che appartengono al gruppo
                    Private cCurs,i_OldArea, nCode, bFirst, nUsercode
                    cCurs=Sys(2015)
                    i_OldArea=Select()
                    bFirst=.T.
                    *--- Se passo io il cursore con la lista degli utenti nn leggo dal db
                    If Vartype(cCurUsers)<>'C'
                        If .nConn<>0
                            cp_sqlexec(.nConn,"select * from CpUsrGrp Where groupcode="+cp_ToStrODBC(nGroupcode),cCurs)
                        Else
                            Select * From CpUsrGrp Where groupcode=nGroupcode Into Cursor (cCurs )nofilter
                        Endif
                    Else
                        cCurs=cCurUsers
                    Endif
                    Select(cCurs)
                    Go Top
                    Scan
                        nUsercode=usercode
                        * --- Il post it aperto lo assegno al primo utente..
                        If bFirst
                            If .nConn<>0
                                =cp_SQL(.nConn,"update postit set usercode="+cp_ToStr(nUsercode)+", status='P' where code="+cp_ToStr(.cVar))
                            Else
                                Update postit Set usercode=nUsercode,Status='P' Where Code=.cVar
                            Endif
                            bFirst=.F.
                            .cStatus='P'
                            .relasePostIT()
                        Else
                            * --- Per ogni utente duplico il Postin e lo invio...
                            * --- Raddoppio il post it direttamente sul database..
                            nCode=Sys(2015)
                            If .nConn<>0
                                =cp_SQL(.nConn,"Insert Into Postit (code,usercode, created , createdby  , postit  , status, "+;
                                    "px, py ,wi,he, datestart,  datestop, subject ) "+;
                                    " SELECT "+cp_ToStrODBC(nCode)+" As code , "+cp_ToStr(nUsercode)+" As usercode , "+;
                                    "created , createdby  , postit  , "+cp_ToStr('P')+" As  status,"+;
                                    "px, py ,wi,he, datestart,  datestop, subject FROM Postit WHERE code="+cp_ToStr(.cVar))
                            Else
                                #If Version(5)>=900
                                    Insert Into postit(Code,usercode, created , createdby  , postit  , Status,;
                                        px, py ,wi,he, datestart, datestop, subject)  ;
                                        SELECT nCode As Code , nUsercode As usercode , created , createdby  , postit  ,  'P',;
                                        px, py ,wi,he, datestart, datestop, subject From postit Where Code=.cVar
                                #Else
                                    Local cCursName
                                    cCursName=Sys(2015)
                                    Select nCode As Code , nUsercode As usercode , created , createdby  , postit  ,  'P',;
                                        px, py ,wi,he, datestart, datestop, subject From postit Where Code=.cVar Into Cursor &cCursName
                                    Select (cCursName)
                                    Scatter
                                    Select postit
                                    Append Blank
                                    Gather
                                    Select (cCursName)
                                    Use
                                    Select (postit)
                                    Use
                                #Endif
                            Endif
                        Endif
                        Select(cCurs)
                    Endscan
                    Use In (cCurs)
                    Select( i_OldArea )
                Endif
            Else
                cp_msg(cp_translate(MSG_WHEN_A_POSTIN_IS_ADDED_TO_A_RECORD_IT_CANNOT_BE_SENT_TO_ANOTHER_USER),.F.)
            Endif
        Endwith
    Endproc

    Procedure RemoveItem
        Lparameters oElement
        With This
            .RemoveObject(m.oElement.Name)
            .nIndexSelected = -1
            .Parent.nNumAttach = .Parent.nNumAttach - 1
            .ReorgAttach()
            .ResizeParent()
        Endwith
    Endproc

    Procedure ReorgAttach
        Local oAttachAtt, TopAtt
        m.TopAtt = 16
        With This
            For Each oAttachAtt In .oATTACHELEMENTCOL
                m.oAttachAtt.Anchor = 0
                m.oAttachAtt.Top = m.TopAtt
                m.oAttachAtt.Anchor = 11
                m.TopAtt = m.TopAtt + m.oAttachAtt.Height
                .Height = m.TopAtt
            Next
        Endwith
    Endproc

    Procedure ResizeParent
        Local nOldTextAreaAnchor, nOldAnchor, nOldParentAnchor, nOldHeight
        m.nOldParentAnchor = This.Parent.Anchor
        m.nOldTextAreaAnchor = This.Parent.TextArea.Anchor
        This.Parent.TextArea.Anchor = 0
        m.nOldAnchor = This.Anchor
        This.Parent.Anchor = 0
        This.Anchor = 0
        With This.Parent
            If .bCanResize
                This.Top = .TextArea.Top + .TextArea.Height + 2
                If This.Pinned
                    m.nOldHeight = .Height
                    .Height = .TextArea.Top + .TextArea.Height + Iif(Vartype(.Parent.BottomResizer)="O", .Parent.BottomResizer.Height, 0) + 14
                    .Parent.Height = .Parent.Height - (m.nOldHeight - .Height)
                Else
                    m.nOldHeight = .Height
                    If .nNumAttach>0 Then
                        .Height = .TextArea.Top + .TextArea.Height + Iif(Vartype(.Parent.BottomResizer)="O", .Parent.BottomResizer.Height, 0) + 5 + This.Height
                        This.Visible = .T.
                    Else
                        .Height = .TextArea.Top + .TextArea.Height + Iif(Vartype(.Parent.BottomResizer)="O", .Parent.BottomResizer.Height, 0) + 5
                        This.Visible = .F.
                    Endif
                    .Parent.Height = .Parent.Height - (m.nOldHeight - .Height)
                Endif
            Else
                If This.Pinned
                    .TextArea.Height = .Height - .TextArea.Top - 19
                    This.Top = .Height - 17
                Else
                    This.Top = .Height - This.Height - 2
                    If .nNumAttach>0 Then
                        .TextArea.Height = .Height - This.Height - .TextArea.Top - 5
                        This.Visible = .T.
                    Else
                        .TextArea.Height = .Height - .TextArea.Top - 5
                        This.Visible = .F.
                    Endif
                Endif
            Endif
            .TextArea.Anchor = m.nOldTextAreaAnchor
            This.Anchor = m.nOldAnchor
            This.Parent.Anchor = m.nOldParentAnchor
            .bUpdated = .T.
        Endwith
    Endproc
Enddefine

Define Class cp_AttachElement As Container
    BorderWidth = 0
    BackStyle = 0
    nIndex = 0
    cType = ""
    Visible = .T.
    Height = 16
    Dimension xKey[1]
    xKey[1]=""
    cAzi = ""
    cPrg = ""
    OLEDropMode = 1
    nMouseDownX = -1
    nMouseDownY = -1

    Add Object imgIcon As Image With ;
        Top = 0, Left = 0 , Width = 16, Height = 16, ;
        BackStyle = 0, Anchor = 3, Picture = "imgIcon", ;
        MousePointer = 15, OLEDropMode = 1

    Add Object lblCaption As Label With ;
        Top = 1, Left = 17, Height = 14, ;
        AutoSize = .T., BackStyle = 0, MousePointer = 15, ;
        OLEDropMode = 1

    Procedure Init
        Lparameters cImage, oAttach, cType
        With This
            If !File(m.cImage)
                m.cImage = ".\new.bmp"
            Endif
            If File(m.cImage)
                .imgIcon.Picture = m.cImage
            Else
                .imgIcon.Visible = .F.
            Endif
            If m.cType="O"
                If Vartype(m.oAttach)="O"
                    *=m.oAttach.Parent.Parent.Parent.getsecurityCode()
                    .lblCaption.Caption = Alltrim(m.oAttach.Parent.Parent.Parent.cComment)
                    .cPrg = m.oAttach.Parent.Parent.Parent.cPrg
                    .SetSourceKey(m.oAttach)
                Else
                    .lblCaption.Caption = Alltrim(m.oAttach)
                Endif
            Else
                .lblCaption.Caption = Alltrim(m.oAttach)
            Endif
            .cType = m.cType
            .Width = .Parent.Width - 20
        Endwith
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        Return This
    Endproc

    Procedure MyMouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton=1
            Nodefault
            * --- per ritardare un po il drag e drop
            With This
                If .OLEDropHasData <= 0
                    .nMouseDownX=nXCoord
                    .nMouseDownY=nYCoord
                Endif
            Endwith
        Endif
    Endproc

    Procedure MyMouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.nMouseDownX>=0 And This.nMouseDownY>=0
            If nButton=1 And (Abs(nXCoord-This.nMouseDownX)+Abs(nYCoord-This.nMouseDownY))>8
                Nodefault
                With This
                    If .OLEDropHasData <=0
                        .DragIcon=i_cBmpPath+"DragMove.cur"
                        .Drag(1)
                    Endif
                Endwith
            Endif
        Endif
    Endproc

    Procedure lblCaption.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MyMouseDown(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure lblCaption.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MyMouseMove(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure imgIcon.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MyMouseDown(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure imgIcon.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MyMouseMove(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure ShiftUpAttach
        Lparameters oSource, nXCoord, nYCoord

    Endproc

    Procedure ShiftDownAttach
        Lparameters oSource, nXCoord, nYCoord

    Endproc

    Procedure ShiftAttach
        Lparameters oSource, nXCoord, nYCoord
        Local nDestTop, nOrigTop
        m.nDestTop = This.Top
        m.nOrigTop = oSource.Top
        If m.nDestTop<m.nOrigTop
            This.ShiftUpAttach(oSource, nXCoord, nYCoord)
        Else
            This.ShiftDownAttach(oSource, nXCoord, nYCoord)
        Endif
    Endproc

    Procedure MyDragDrop
        Lparameters oSource, nXCoord, nYCoord
        If Lower(oSource.Parent.Class) = "cp_attachelement"
            This.ShiftAttach(oSource, nXCoord, nYCoord)
        Else
            With This.Parent.Parent
                .MyDragDrop(This, oSource, nXCoord, nYCoord)
            Endwith
        Endif
    Endproc

    Procedure lblCaption.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        This.Parent.MyDragDrop(oSource, nXCoord, nYCoord)
    Endproc

    Procedure imgIcon.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        This.Parent.MyDragDrop(oSource, nXCoord, nYCoord)
    Endproc

    Procedure lblCaption.OLEDragOver
        Lparameters oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        If !oDataObject.GetFormat(1)
            This.Parent.Parent.Parent.MyOLEDragOver(This, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState)
        Endif
    Endproc

    Procedure lblCaption.OLEDragDrop
        Lparameters oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        If !oDataObject.GetFormat(1)
            This.Parent.Parent.Parent.MyOLEDragDrop(This, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord)
        Endif
    Endproc

    Procedure imgIcon.OLEDragOver
        Lparameters oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        If !oDataObject.GetFormat(1)
            This.Parent.Parent.Parent.MyOLEDragOver(This, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState)
        Endif
    Endproc

    Procedure imgIcon.OLEDragDrop
        Lparameters oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        If !oDataObject.GetFormat(1)
            This.Parent.Parent.Parent.MyOLEDragDrop(This, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord)
        Endif
    Endproc

    Procedure OLEDragOver
        Lparameters oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState
        If !oDataObject.GetFormat(1)
            This.Parent.Parent.MyOLEDragOver(This, oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord, nState)
        Endif
    Endproc

    Procedure OLEDragDrop
        Lparameters oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord
        If !oDataObject.GetFormat(1)
            This.Parent.Parent.MyOLEDragDrop(This, oDataObject, nEffect, nButton, nShift,   nXCoord, nYCoord)
        Endif
    Endproc

    Procedure SetSourceKey(vSource)
        If Vartype(vSource)="O"
            This.ObjSourceKey(m.vSource)
        Else
            This.ArrSourceKey(@m.vSource)
        Endif
    Endproc

    Procedure ArrSourceKey(aSource)
        Local I
        Dimension This.xKey[alen(aSource)]
        For I=1 To Alen(aSource)
            This.xKey[i]=aSource[i]
        Next
    Endproc

    Procedure ObjSourceKey(oSource)
        Local I
        Dimension This.xKey[alen(oSource.xKey)]
        For I=1 To Alen(oSource.xKey)
            This.xKey[i]=oSource.xKey[i]
        Next
        I=cp_ascan(@i_TableProp,Lower(oSource.cFile),i_nTables)
        This.cAzi=Strtran(cp_SetAzi(i_TableProp[i,2]),Strtran(i_TableProp[i,2],'xxx',''),'')
    Endproc

    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.lblCaption.ForeColor = .GetProp(TITLE_FONTCOLOR)
        Endwith
    Endproc

    Procedure SelectElement
        With This.Parent
            For Each oElem In .oATTACHELEMENTCOL
                oElem.lblCaption.FontBold=.F.
            Next
            This.lblCaption.FontBold=.T.
            .nIndexSelected = This.nIndex
            .imgDelete.Visible=.T.
        Endwith
    Endproc

    Procedure imgIcon.Click
        This.Parent.SelectElement()
    Endproc

    Procedure lblCaption.Click
        This.Parent.SelectElement()
    Endproc

    Procedure imgIcon.DblClick
        This.Parent.OpenAttach()
    Endproc

    Procedure lblCaption.DblClick
        This.Parent.OpenAttach()
    Endproc

    Procedure OpenAttach
        With This
            Do Case
                Case .cType = "F" Or .cType = "U"
                    ShellExecute(_Screen.HWnd , "open" , Alltrim(.lblCaption.Caption) ," " , Sys(5)+Sys(2003) , 1)
                Case .cType = "O"
                    Local i_cmd
                    Local i_o,i_i,i_k,i_v,i_p
                    i_cmd= .cPrg
                    If Not(Empty(i_cmd))
                        If Empty( .cAzi) Or Trim( .cAzi)=Trim(i_codazi)
                            i_o=&i_cmd()
                            If !Isnull(i_o)
                                i_o.ecpFilter()
                                * --- carica il record specificato
                                i_k=i_o.cKeySelect
                                For i_i=1 To Alen( .xKey)
                                    If Len(i_k)>0
                                        i_p=At(',',i_k)
                                        If i_p=0
                                            i_v=i_k
                                            i_k=''
                                        Else
                                            i_v=Left(i_k,i_p-1)
                                            i_k=Substr(i_k,i_p+1)
                                        Endif
                                        i_o.w_&i_v=This.xKey[i_i]
                                    Endif
                                Next
                                i_o.ecpSave()
                            Endif
                        Else
                            cp_ErrorMsg(cp_MsgFormat(MSG_BUTTON_LINKED_TO_COMPANY__,This.cAzi),"!",cp_translate(MSG_POSTIN_ADMIN),.F.)
                        Endif
                    Endif
            Endcase
        Endwith
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine

Define Class cp_ImgButton As Container
    BorderWidth = 0
    BackStyle = 0
    MousePointer = 15
    Anchor = 0
    Pinnable = .F.
    Pinned = .F.
    nType = 0  &&0 CloseButton, 1 MenuButton, 2 SendButton

    #Define SB_STDWIDTH 15
    #Define SB_STDHEIGHT 15

    Add Object ImgFocused As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0

    Add Object ImgNotFocused As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0

    Add Object ImgFocusedPinned As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0

    Add Object ImgNotFocusedPinned As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0

    Protected Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.ChangeTheme()
    Endproc

    Procedure Pinnable_assign
        Lparameters bNewVal
        This.Pinnable = b.NewVal
        This.Pinned = This.Parent.Pinned
    Endproc

    Procedure Pinned_assign
        Lparameters bNewVal
        This.Pinned = m.bNewVal And This.Pinnable
        This.Parent.Pinned = This.Pinned
        This.ChangeBackground(.F.)
    Endproc

    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            Do Case
                Case This.nType = 0  &&CloseButton
                    This.ImgFocused.Picture = .GetProp(CLOSEBUTTONFOCUSED_PICTURE)
                    This.ImgNotFocused.Picture = .GetProp(CLOSEBUTTON_PICTURE)
                Case This.nType = 1	&&MenuButton
                    This.ImgFocused.Picture = .GetProp(PINBUTTON_FOCUSED_PICTURE)
                    This.ImgFocusedPinned.Picture = .GetProp(PINBUTTON_FOCUSEDPINNED_PICTURE)
                    This.ImgNotFocused.Picture = .GetProp(PINBUTTON_PICTURE)
                    This.ImgNotFocusedPinned.Picture = .GetProp(PINBUTTON_PINNED_PICTURE)
                Case This.nType = 3	&&SendButton
                    This.ImgFocused.Picture = .GetProp(SENDBUTTON_FOCUSED_PICTURE)
                    This.ImgFocusedPinned.Picture = .GetProp(SENDBUTTON_FOCUSEDPINNED_PICTURE)
                    This.ImgNotFocused.Picture = .GetProp(SENDBUTTON_PICTURE)
                    This.ImgNotFocusedPinned.Picture = .GetProp(SENDBUTTON_PINNED_PICTURE)
            Endcase
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure ChangeBackground
        Lparameters llGotFocus
        #Define lnFocused 8
        #Define lnNotFocused 16
        #Define lnPinned 32
        #Define lnNotPinned 64
        Local lnState
        With This
            m.lnState = (Iif(m.llGotFocus,lnFocused,lnNotFocused) + ;
                Iif(.Pinned,lnPinned,lnNotPinned))
            .ImgFocused.Visible=.F.
            .ImgNotFocused.Visible=.F.
            .ImgFocusedPinned.Visible=.F.
            .ImgNotFocusedPinned.Visible=.F.
            Do Case
                Case m.lnState = (lnFocused + lnPinned)
                    .ImgFocusedPinned.Visible=.T.
                Case m.lnState = (lnFocused + lnNotPinned)
                    .ImgFocused.Visible=.T.
                Case m.lnState = (lnNotFocused + lnPinned)
                    .ImgNotFocusedPinned.Visible=.T.
                Case m.lnState = (lnNotFocused + lnNotPinned)
                    .ImgNotFocused.Visible=.T.
            Endcase
        Endwith
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.T.)
        This.Parent.MouseEnter()
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.F.)
        This.Parent.MouseLeave()
    Endproc

    Procedure ImgFocused.Click
        This.Parent.Click()
    Endproc

    Procedure ImgFocusedPinned.Click
        This.Parent.Click()
    Endproc

    Procedure Click
        With This
            Do Case
                Case .nType=1
                    .Pinned = !.Pinned
                    .ChangeBackground(.T.)
                    If !.Pinned
                        If Vartype(.Parent.HWnd) = "N"
                            postmessage(.Parent.HWnd,  WM_MOUSEHOVER, 0, 0)
                        Endif
                    Endif
                Case .nType=0
                    .Parent.nStatus = 0
                    .Parent.HideForm()
                Case .nType=2
                    Do cp_PostitSendTo With .Parent
            Endcase
        Endwith
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine

*********************************************************************************************

Define Class cp_TextArea As Container
    Top = 0
    Left = 0
    Width = 10
    Height = 10
    BorderWidth = 0
    BorderColor = 0
    Anchor = 0
    Inizialized = .F.
    BackStyle = 0

    Add Object lblText As Label With ;
        Top = 0, Left = 0, ;
        Height = 10, Width = 10, ;
        Anchor = 0, Caption = "", ;
        BackStyle = 0

    Protected Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.ChangeTheme()
    Endproc

    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.lblText.ForeColor = .GetProp(PANELTITLE_FONTCOLOR)
        Endwith
    Endproc

    Procedure Inizialized_Assign
        Lparameters bNewVal
        This.Inizialized = m.bNewVal
    Endproc

    Procedure lblText.Caption_Assign
        Lparameters cNewVal
        If Len(m.cNewVal)>254
            m.cNewVal = Left(m.cNewVal, 254)
        Endif
        This.Caption = m.cNewVal
        This.Parent.ResizeTextArea()
    Endproc

    Procedure ResizeTextArea()
        Local  cText, nTxtWidth, nTxtHeight
        m.cText = This.lblText.Caption
        m.nTxtWidth = This.Parent.TextWidth( m.cText )
        m.nTxtHeight = This.Parent.TextHeight( m.cText )
        With This.Parent
            .Height = Min(This.Top + m.nTxtHeight, _Screen.Height - 15)
            .Width = Min(This.Left + m.nTxtWidth, _Screen.Width - 10)
        Endwith
        With This
            .Height = m.nTxtHeight
            .Width = m.nTxtWidth
        Endwith
        With This.lblText
            .Height = m.nTxtHeight
            .Width = m.nTxtWidth
        Endwith
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseEnter()
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseLeave()
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine


Define Class cp_Gripper As Container

    Top = 1
    Left = 1
    Width = 327
    Height = 7
    BorderWidth = 0
    BorderColor = 0
    MousePointer = 5
    Anchor = 11

    Add Object imgbackground As Image With ;
        Top = 0, Left = 0 , ;
        Height = 7, Width = 329, ;
        Anchor = 15, Stretch = 2, ;
        MousePointer = 5, ;
        Name = "imgBackground"

    Add Object imggripper As Image With ;
        Top = 3, Left = 165, ;
        Height = 4, Width = 35, ;
        Anchor = 768, ;
        BackStyle = 1, ;
        MousePointer = 5, ;
        Name = "imgGripper"

    Protected Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.ChangeTheme()
    Endproc

    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.imgbackground.Picture = .GetProp(SPLITTER_BACKGROUND_PICTURE)
            This.imggripper.Picture = .GetProp(SPLITTER_GRIPPER_PICTURE)
        Endwith
    Endproc

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        If m.nButton = 1
            Local hWindow
            m.hWindow = This.Parent.HWnd
            ReleaseCapture()
            SendMessage(m.hWindow, WM_SYSCOMMAND, MOUSE_MOVE, WM_NULL)
            SendMessage(m.hWindow, WM_LBUTTONUP, 0, 0)
        Endif
    Endproc

    Procedure imgbackground.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseEnter()
        This.Parent.MouseMove(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure imggripper.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseEnter()
        This.Parent.MouseMove(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure imggripper.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseEnter()
    Endproc

    Procedure imggripper.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseLeave()
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseEnter()
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseLeave()
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine

*--------------------------------------------------------------------------------------------------------------------------*
Define Class cp_PostInInfo As Container
    BackStyle = 0
    BorderWidth = 0
    Visible = .T.
    bSelected = .F.

    cPostItCode = ""

    Add Object lblCaption As Label With ;
        Top = 0, Left = 0, ;
        Width = 10, Height = 20, ;
        BackStyle = 0, Visible = .T., ;
        MousePointer = 15

    Add Object imgAttach As Image With ;
        Top = 0, Left = 11, ;
        Width = 16, Height = 16, ;
        BackStyle = 0, Visible = .T., ;
        MousePointer = 15

    Add Object lblNumAttach As Label With ;
        Top = 0, Left = 27, ;
        Width = 35, Height = 20, ;
        BackStyle = 0, Visible = .T., ;
        MousePointer = 15

    Procedure Init
        Lparameters oParent, cCaption, cCode, nNumAttach, cFileBmp
        With This
            .Width = m.oParent.Width
            .Height = 20
            .cPostItCode = m.cCode
            If m.nNumAttach>0
                .lblNumAttach.Caption = Alltrim(Str(m.nNumAttach,10))
                .lblNumAttach.Left = m.oParent.Width - .lblNumAttach.Width - 1
                .lblNumAttach.Anchor = 9
                m.cFileBmp  = i_ThemesManager.RetBmpFromIco("..\vfcsim\Attach.ico", 16)
                .imgAttach.Picture = m.cFileBmp+".bmp"
                .imgAttach.Left = m.oParent.Width - .imgAttach.Width - .lblNumAttach.Width - 1
                .imgAttach.Anchor = 9
            Else
                .lblNumAttach.Visible = 0
                .imgAttach.Visible = 0
            Endif
            .lblCaption.Caption = m.cCaption
            If m.nNumAttach>0
                .lblCaption.Width = m.oParent.Width - .imgAttach.Width - .lblNumAttach.Width - 3
            Else
                .lblCaption.Width = m.oParent.Width - 1
            Endif
            .lblCaption.Anchor = 11
        Endwith
    Endproc

    Procedure MyClick
        Local oAtt, nItem
        Local cBuffer, nIndex, nShiftState

        m.cBuffer = Repli(Chr(0), 256)
        = GetKeyboardState (@cBuffer)

        m.nShiftState= Asc(Substr(m.cBuffer, 17,1))

        m.nItem = 1
        Do While m.nItem<= This.Parent.oPostInCol.Count
            m.oAtt = This.Parent.oPostInCol.Item(m.nItem)
            m.oAtt.lblCaption.FontBold = .F.
            m.oAtt.lblNumAttach.FontBold = .F.
            m.nItem = m.nItem + 1
        Enddo
        With This
            .lblCaption.FontBold=.T.
            .lblNumAttach.FontBold = .T.
        Endwith
    Endproc

    Procedure MyDblClick
        Local m.oPostIt
        If i_ServerConn[1,2]<>0
            =cp_SQL(i_ServerConn[1,2],"select * from postit where code="+cp_ToStr(This.cPostItCode)+" and usercode="+cp_ToStr(i_codute),"postit_c")
        Else
            Select * From postit Where Code=This.cPostItCode And usercode=i_codute Into Cursor postit_c
        Endif
        m.oPostIt = g_Alertmanager.AddAlert("cp_AdvPostIt", 'set', i_ServerConn[1,2])
        m.oPostIt.DoAlert()
        m.oPostIt = .Null.
        Use In Select ("postit_c")
        If i_ServerConn[1,2]<>0
            =cp_SQL(i_ServerConn[1,2],"update postit set status='D' where code="+cp_ToStr(This.cPostItCode)+" and usercode="+cp_ToStr(i_codute))
        Else
            Update postit Set Status='D' Where Code=This.cPostItCode And usercode=i_codute
        Endif
        This.Parent.FillPostit()
    Endproc

    Procedure lblCaption.Click
        This.Parent.MyClick()
    Endproc

    Procedure imgAttach.Click
        This.Parent.MyClick()
    Endproc

    Procedure lblNumAttach.Click
        This.Parent.MyClick()
    Endproc

    Procedure lblCaption.DblClick
        This.Parent.MyDblClick()
    Endproc

    Procedure imgAttach.DblClick
        This.Parent.MyDblClick()
    Endproc

    Procedure lblNumAttach.DblClick
        This.Parent.MyDblClick()
    Endproc

Enddefine
