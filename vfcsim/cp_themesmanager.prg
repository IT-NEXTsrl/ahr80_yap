* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_THEMESMANAGER
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Stefano D'Amico
* Data creazione: 20/03/2008
* Aggiornato il : 01/09/2008
* Traduzione    : 21/03/2008
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Classi di gestione temi interfaccia stile office 2007
* ----------------------------------------------------------------------------
#include "cp_app_lang.inc"

#Define WM_THEMECHANGED   0x031A
#Define IDI_INFORMATION   32516
#Define IDI_ERROR          32513
#Define IDI_EXCLAMATION    32515
#Define IDI_QUESTION       32514
#Define WM_NULL        0
#Define WM_SYSCOMMAND  0x112
#Define WM_LBUTTONUP   0x202
#Define MOUSE_MOVE     0xf012
#Define WM_MOUSEHOVER	0x2A1
#Define GWL_EXSTYLE	-20
#Define WS_EX_LAYERED	0x80000
#Define LWA_ALPHA	0x2
#Define WM_MOUSEMOVE	0x200
#Define WM_LBUTTONDOWN	0x201
#Define WM_KEYDOWN 0x0100
#Define GCL_STYLE	-26
#Define	CS_DROPSHADOW	0x20000
#Define WM_NCACTIVATE	0x86
#Define WM_MOVE	0x3


#Define THEME_NAME 0
#Define NAVBAR_BORDERCOLOR	1
#Define CONTAINER_BORDERCOLOR	2
#Define TITLE_BORDERCOLOR	3
#Define TITLE_FONTCOLOR	4
#Define TITLE_BACKGROUND_LEFT	5
#Define TITLE_BACKGROUND_WIDTH	6
#Define PANELVERTICAL_BACKCOLOR	7
#Define PANELVERTICAL_FONTCOLOR	8
#Define PANELBUTTON_SELECTED_FONTCOLOR	9
#Define PANELBUTTON_NOTSELECTED_FONTCOLOR	10
#Define FORM_BACKGROUND_PICTURE	11
#Define OVERFLOWPANELBUTTON_FOCUSEDNOTSELECTED_PICTURE	12
#Define OVERFLOWPANELBUTTON_FOCUSEDSELECTED_PICTURE	13
#Define OVERFLOWPANELBUTTON_NOTFOCUSEDSELECTED_PICTURE	14
#Define OVERFLOWPANEL_BACKGROUND_PICTURE	15
#Define OVERFLOWPANEL_MENUBUTTON_PICTURE	16
#Define PANELBUTTON_FOCUSEDNOTSELECTED_PICTURE	17
#Define PANELBUTTON_FOCUSEDSELECTED_PICTURE	18
#Define PANELBUTTON_NOTFOCUSEDSELECTED_PICTURE	19
#Define PANELBUTTON_NOTFOCUSEDNOTSELECTED_PICTURE	20
#Define SHRINKBUTTON_FOCUSEDNOTSHRUNK_PICTURE	21
#Define SHRINKBUTTON_FOCUSEDSHRUNK_PICTURE	22
#Define SHRINKBUTTON_NOTFOCUSEDNOTSHRUNK_PICTURE	23
#Define SHRINKBUTTON_NOTFOCUSEDSHRUNK_PICTURE	24
#Define SPLITTER_BACKGROUND_PICTURE	25
#Define SPLITTER_GRIPPER_PICTURE	26
#Define TITLE_BACKGROUND_PICTURE	27
#Define VERTICAL_SPLITTER_BACKGROUND_PICTURE	28
#Define VERTICAL_SPLITTER_GRIPPER_PICTURE	29
#Define CONTAINER_BACKCOLOR	30
#Define VERTICALSHRINKBUTTON_SHRUNK_PICTURE	31
#Define VERTICALSHRINKBUTTON_NOTSHRUNK_PICTURE	32
#Define PANELTITLE_NOTFOCUSEDBACKGROUND_PICTURE	33
#Define PANELTITLE_FOCUSEDBACKGROUND_PICTURE	34
#Define PANELTITLE_BOTTOMLINE_COLOR	35
#Define PANELTITLE_TOPLINE_COLOR	36
#Define PANELTITLE_FONTCOLOR	37
#Define CLOSEBUTTON_PICTURE	38
#Define CLOSEBUTTONFOCUSED_PICTURE	39
#Define PINBUTTON_PICTURE	40
#Define PINBUTTON_FOCUSED_PICTURE	41
#Define PINBUTTON_PINNED_PICTURE	42
#Define PINBUTTON_FOCUSEDPINNED_PICTURE	43
#Define DELETEBUTTON_PICTURE	44
#Define DELETEBUTTONFOCUSED_PICTURE	45
#Define OKBUTTON_PICTURE	46
#Define OKBUTTONFOCUSED_PICTURE	47
#Define OVERFLOWPANELFOCUSED_MENUBUTTON_PICTURE	48
#Define DOTSBUTTON_PICTURE	49
#Define DOTSBUTTONFOCUSED_PICTURE	50
#Define TOOLBAR_BTN_HOVER 51
#Define TOOLBAR_BTN_DOWN 52
#Define TOOLBAR_BACKGRD_V 53
#Define TOOLBAR_BACKGRD_H 54
#Define TOOLBAR_BORDERCOLOR 55
#Define TOOLBAR_BACKGRD_H 54
#Define TOOLBARFRAME_COLOR 56
#Define MENUBAR_BTN_COLOR_BCK 57
#Define MENUBAR_BTN_COLOR_BORDER 58
#Define TOOLBAR_BTN_TEXT 59
#Define TOOLBAR_BTN_HOVER_TEXT 60
#Define TOOLBAR_BTN_DOWN_TEXT 61
#Define TOOLBAR_BTN_HOVER_BORDER	62
#Define TOOLBAR_BTN_DOWN_BORDER	63
#Define FORM_TBMDI_BACKGROUND_PICTURE 64
#Define MDIMINBUTTONFOCUSED_PICTURE 65
#Define MDIMINBUTTON_PICTURE 66
#Define MDIMAXBUTTONFOCUSED_PICTURE 67
#Define MDIMAXBUTTON_PICTURE 68
#Define MDICLOSEBUTTONFOCUSED_PICTURE 69
#Define MDICLOSEBUTTON_PICTURE 70
#Define TOOLBAR_BTN_DISABLEDCOLOR	71
#Define SEARCHMENU_HOVER 76
#Define SPINNER_DATETIME_PICKER_BACKGROUND 97

#Define BLACKNESS	0x42
#Define DSTINVERT	0x550009
#Define MERGECOPY	0xC000CA
#Define MERGEPAINT	0xBB0226
#Define NOTSRCCOPY	0x330008
#Define NOTSRCERASE	0x1100A6
#Define PATCOPY		0xF00021
#Define PATINVERT	0x5A0049
#Define PATPAINT	0xFB0A09
#Define SRCAND		0x8800C6
#Define SRCCOPY		0xCC0020
#Define SRCERASE	0x440328
#Define SRCINVERT	0x660046
#Define SRCPAINT	0xEE0086
#Define WHITENESS	0xFF0062

#Define HKEY_LOCAL_MACHINE	0x80000002
#Define HKEY_CLASSES_ROOT	0x80000000
#Define ERROR_SUCCESS	0
#Define KEY_READ	131097

#Define DI_NORMAL	0x3
#Define DI_MASK	0x1
#Define DI_IMAGE	0x2


Define Class CP_THEMESMANAGER As Custom


    *-- 0 - Automatic (XP only) 1- 2003 Blue, 2- 2003 Olive, 3- 2003 Silver, 4- 2007 Black, 5- 2007 Blue and 6- 2007 Silver.
    Hidden TotalThemes
    * Numero temi
    TotalThemes = 7

    Hidden TotThemesProp
    * Totale propriet� temi
    TotThemesProp = 80

    ThemeNumber = 0
    ThemeName = ''
    *-- Currently OS name.
    OSname = ""
    *-- Specifies the active theme number.
    ActiveTheme = 0
    NumThemes = 0

    DefaultStdTheme = 0

    Name = "cp_Themesmanager"
    Visible = .T.

    ImgPath = ""

    * Numero oggetti che utilizzano il themes manager
    Hidden NumChild
    NumChild = 0
    *-- Specifies if the control will automatically respond to Windows XP theme change event.
    Hidden IsThemeChangedBound

    *-- List of created images to delete when control is destroyed.
    Hidden ThemeList[this.TotalThemes,this.TotThemesProp]
    Hidden Name
    Hidden ClassLibrary
    Hidden AddObject
    Hidden AddProperty
    Hidden BaseClass
    Hidden Class
    Hidden CloneObject
    Hidden Comment
    Hidden ControlCount
    Hidden Controls
    Hidden Error
    Hidden Height
    Hidden HelpContextID
    Hidden Init
    Hidden Newobject
    Hidden Objects
    Hidden Parent
    Hidden ParentClass
    Hidden Picture
    Hidden ReadExpression
    Hidden ReadMethod
    Hidden RemoveObject
    Hidden ResetToDefault
    Hidden SaveAsClass
    Hidden ShowWhatsThis
    Hidden Tag
    Hidden WhatsThisHelpID
    Hidden Width
    Hidden WriteExpression
    Hidden WriteMethod
    Hidden PropCurName
    Hidden RBmpCurName
    Hidden cCBMName

    TabMenuItemSel=''
    TabMenuItemSelLeft=''
    TabMenuItemSelRight=''
    #If Version(5)>=900
        Add Object  aToolBar As Collection
    #Else
        Add Object  aToolBar As Cp_Collection
    #Endif

    nOldLockRedraw = 0
    nLockRedraw = 0
    nOldLockVfp = 0
    nLockVfp = 0

    *--- Global variable
    Add Object oThemesManagerSetup As ThemesManagerSetup

    Procedure Init()
        * Definisco l'array delle propriet� dei temi
        With This
            .cCBMName = 'CBM_'+Sys(2015)		&&Nome della variabile pubblica utilizzata dalla FLL per riferire la classe
            *If i_VisualTheme <> -1
            *--- Carico libreria Interfaccia
            If !("CP_DOCKWND"$Upper(Set("Library")))
                If cp_fileexist('..\VFCSIM\cp_DockWnd.fll', .T.)
                    Set Library To '..\VFCSIM\cp_DockWnd.fll' Additive
                Else
                    Set Library To 'cp_DockWnd.fll' Additive
                Endif
            Endif
            *endif
            Local cMacro
            cMacro = This.cCBMName
            Public &cMacro
            &cMacro = This
            &&Aggancia la finestra principale e crea la finestra invisibile per la gestione dei menu
            If i_VisualTheme <> -1
                cb_HookMainWnd(_vfp.HWnd, This.cCBMName)
                .OSname = This.osname_access()
            Endif

            .ImgPath = i_cTmpImg
            If !cp_fileexist(.ImgPath)
                * ---
                If !Cp_makedir (.ImgPath)
                    cp_ErrorMsg(cp_MsgFormat(MSG_PATH_TMPIMG  , .ImgPath), 'stop')
                    Do mhit  In cp_menu With 'quitting'
                    Return
                Endif
            Endif
            .ImgPath = Addbs(.ImgPath)
            
            LOCAL nCount
		    If Not IsAhe() And Not IsAhr() And Not IsAlt()
		    	i_VisualTheme = -1
		    	nCount = 1
		    ELSE
		        local i,fname,cPath,cFile, cThemePath
			    cThemePath = cPathVfcsim+'\Themes\'
			    ADIR(aDirectories, cThemePath+'*','D')
			    i=1
			    nCount =0
			    Do While m.i<=ALEN(aDirectories,1)
			    	fname = aDirectories[i,1]
		     			bApplication = (IsAlt() And Not 'AHR'$Upper(fname) And Not 'AHE'$Upper(fname)) Or (IsAhe() And Not 'AHR'$Upper(fname) And Not 'AETOP'$Upper(fname)) or (IsAhr() And Not 'AHE'$Upper(fname) And Not 'AETOP'$Upper(fname))
					If m.bApplication And m.fname<>'.' and m.fname<>'..' and 'D'$aDirectories[m.i,5] and IsDigit(m.fname)
				        cPath = m.cThemePath+aDirectories[m.i,1]
				        cFile = Alltrim(Substr(m.fname, At('.', m.fname)+1))
				        If cp_fileexist(m.cPath+"\"+m.cFile+".xml") 
							nCount = nCount + 1
				        Endif
			     	Endif
				    i=m.i+1
			    EndDo
		    Endif
		    .NumThemes = nCount
            
            .ThemeNumber = Iif(Vartype(i_VisualTheme)='N', i_VisualTheme, 7)
            If Empty(.OSname) And .ThemeNumber=0
                .ThemeNumber = 7
                .ActiveTheme = 7
            Endif
            .DefaultStdTheme = Iif(Vartype(g_DefaultStdTheme)='N', g_DefaultStdTheme, 7 )
            .Init_Graphics_API()
            .Init_ThemesArray()
            If i_VisualTheme <> -1
                .CreateTabPage()
            Endif
        Endwith
    Endproc

    Procedure GetMaxThemes()
        Return This.TotalThemes
    Endproc

    Procedure GetMaxProps()
        Return This.TotThemesProp
    Endproc

    Procedure Resize()
        *---- Aggiorno posizione SearchMenu
        If Vartype(i_MenuToolbar)='O'
            i_MenuToolbar.Rearrange()
        Endif
        If Vartype(i_oDeskmenu)='O'
            i_oDeskmenu.ResizeNavBar()
        Endif
    Endproc


    *--- Gestione Toolbars
    Procedure AddToolbar(pToolbar, cCBName)
        This.aToolBar.Add(m.pToolbar, m.cCBName)
    Endproc

    Procedure RemoveToolbar(cCBName)
        If This.aToolBar.GetKey(m.cCBName)<>0
            This.aToolBar.Remove(m.cCBName)
        Endif
    Endproc

    Procedure HideToolBar(bMenu)
        Local oToolbar
        For Each m.oToolbar In This.aToolBar
            If !bMenu Or Lower(m.oToolbar.Class) <> "menubar"
                m.oToolbar.OldVisible = m.oToolbar.Visible
                m.oToolbar.Visible = .F.
            Endif
        Next
        m.oToolbar = .Null.
    Endproc

    *--- Ripristina
    Procedure ShowToolBar(bShowAll, bMenu)
        Local oForm
        For Each m.oToolbar In This.aToolBar
            If !bMenu Or Lower(m.oToolbar.Class) <> "menubar"
                m.oToolbar.Visible =m.bShowAll Or m.oToolbar.OldVisible
            Endif
        Next
        m.oToolbar = .Null.
    Endproc

    *--- Mostra menu contestuale con le toolbar visibili permettendo di nasconderle
    Procedure ShowCustomizeMenu()
        Local l_i, oTb, oCMenu, oMenuItem, cTbVisible, l_find
        oCMenu = Createobject("cbPopupMenu")
        l_find = .F.
        For l_i=1 To This.aToolBar.Count
            oTb = This.aToolBar.Item(l_i)
            *--- Costruisco menu
            If oTb.Customize
                l_find = .T.
                cTbVisible = "i_ThemesManager.aToolbar.Item("+Str(l_i)+").Visible"
                m.oMenuItem = m.oCMenu.AddMenuItem()
                m.oMenuItem.Caption = oTb.Caption
                m.oMenuItem.MarkFor = m.cTbVisible
                m.oMenuItem.OnClick = m.cTbVisible+" = !" + m.cTbVisible
                m.oMenuItem.Visible=.T.
            Endif
        Next
        oTb = .Null.
        If m.l_find
            m.oCMenu.InitMenu()
            m.oCMenu.ShowMenu()
        Endif
        oCMenu = .Null.
    Endproc

    *--- Blocca fll
    Procedure LockVfp(Block, bForce)
        If m.bForce
            If !m.bLock
                This.nOldLockVfp = This.nLockVfp
                This.nLockVfp = 0
            Else
                This.nLockVfp = This.nOldLockVfp
                This.nOldLockVfp = 0
            Endif
        Else
            This.nLockVfp = Max(This.nLockVfp + Iif(Block, 1, -1), 0)
        Endif
        If This.nLockVfp>0
            cb_LockVfp(.T.)
        Else
            cb_LockVfp(.F.)
        Endif
    Endproc

    *--- Blocca tutta fll (compreso paint)
    Procedure LockRedraw(Block, bForce)
        If m.bForce
            If m.bLock
                This.nOldLockRedraw = This.nLockRedraw
                This.nLockRedraw = 1
            Else
                This.nLockRedraw = This.nOldLockRedraw
            Endif
        Else
            This.nLockRedraw = Max(This.nLockRedraw + Iif(Block, 1, -1), 0)
        Endif
        If This.nLockRedraw>0
            cb_LockRedraw(.T.)
        Else
            cb_LockRedraw(.F.)
        Endif
    Endproc

    Procedure AddInstance
        Lparameters oParentObject, cParentProc
        With This
            Bindevent(This,"ChangeTheme", m.oParentObject, m.cParentProc)
            .NumChild = .NumChild + 1
        Endwith
    Endproc

    Procedure RemoveInstance
        Lparameters oParentObject, cParentProc
        With This
            Unbindevents(This,"ChangeTheme",m.oParentObject, m.cParentProc)
            If .NumChild>0
                .NumChild = .NumChild - 1
            Endif
        Endwith
    Endproc

    Hidden Procedure Init_Graphics_API
    	Declare Integer GetKeyState In WIN32API Integer vKey
        Declare Integer CallWindowProc In Win32API Integer lpPrevWndFunc, Integer HWnd, Integer Msg, Integer wParam, Integer Lparam
        Declare Integer ShowWindow In WIN32API As apiShowWindow;
            INTEGER HWnd,;
            INTEGER nCmdShow
        Declare Integer GetWindow In WIN32API As apiGetWindow ;
            INTEGER HWnd,;
            INTEGER wFlag
        Declare Integer SetParent In WIN32API As apiSetParent;
            INTEGER hWndChild,;
            INTEGER hWndNewParent

        Declare Integer GetParent In WIN32API As apiGetParent ;
            INTEGER HWnd
        Declare Integer LoadImage In WIN32API;
            INTEGER hinst,;
            STRING  lpszName,;
            INTEGER uType,;
            INTEGER cxDesired,;
            INTEGER cyDesired,;
            INTEGER fuLoad
        Declare Integer GetDC In Win32API Integer
        Declare Integer ReleaseDC In Win32API Integer, Integer
        Declare Integer GetPixel In Win32API Integer, Integer, Integer
        Declare Integer SetPixel In Win32API Integer, Integer, Integer, Integer
        Declare Integer CreateCompatibleDC In Win32API Integer
        Declare Integer DeleteDC In Win32API Integer
        Declare Integer SelectObject In Win32API Integer, Integer
        Declare SHORT DrawIconEx In WIN32API ;
            INTEGER hdc ,;
            INTEGER xLeft ,;
            INTEGER yTop ,;
            INTEGER hIcon ,;
            INTEGER cxWidth ,;
            INTEGER cyWidth ,;
            INTEGER istepIfAniCur ,;
            INTEGER hbrFlickerFreeDraw ,;
            INTEGER diFlags
        Declare Integer CreateCompatibleBitmap In WIN32API ;
            INTEGER, Integer, Integer
        Declare Integer GetSysColor In WIN32API Integer
        Declare Integer CreateSolidBrush In WIN32API Integer
        Declare Integer FillRect In WIN32API ;
            INTEGER hDC,String @ Rect, Integer hBrush
        Declare Integer DeleteObject In WIN32API Integer hObject
        Declare Integer MulDiv In WIN32API ;
            INTEGER nNumber , Integer nNumerator , Integer nDenominator
        Declare Long BitBlt In Win32API ;
            Long hDestDC, Long x, Long Y, Long nWidth, Long nHeight, ;
            Long hSrcDC, Long xSrc, Long ySrc, Long dwRop
        Declare Integer GetFocus In WIN32API
        Declare Integer LoadIcon In WIN32API;
            INTEGER hInstance, Integer lpIconName
        Declare Integer GetWindowDC In WIN32API Integer HWnd
        Declare SHORT DrawIcon In WIN32API;
            INTEGER hDC, Integer X, Integer Y, Integer hIcon
        Declare Integer SetCapture In WIN32API Integer HWnd
        Declare Integer ReleaseCapture In WIN32API
        Declare Integer SendMessage In WIN32API;
            INTEGER HWnd, Integer Msg, Integer wParam, Integer Lparam
        Declare Integer SetLayeredWindowAttributes In WIN32API;
            INTEGER HWnd, Integer crKey,SHORT bAlpha, Integer dwFlags
        Declare Integer SetWindowLong In WIN32API;
            INTEGER HWnd, Integer nIndex, Integer dwNewLong
        Declare Integer GetWindowLong In WIN32API;
            INTEGER HWnd,Integer nIndex
        Declare Integer PostMessage In WIN32API ;
            INTEGER HWnd, Integer wMsg, Integer wParam, Integer Lparam
        Declare Integer SystemParametersInfo In WIN32API;
            INTEGER uAction, Integer uParam, String @ lpvParam, Integer fuWinIni
        Declare Integer GetDesktopWindow In WIN32API
        Declare Integer FindWindowEx In WIN32API ;
            INTEGER hWnd1, Integer hWnd2, String lpsz1, String lpsz2
        Declare Integer GetWindowText In WIN32API ;
            INTEGER HWnd, String lpString,	Integer cch
        Declare Integer GetWindowRect In WIN32API ;
            INTEGER HWnd, String @ lpRect
        Declare Integer SetClassLong In WIN32API;
            INTEGER HWnd, Integer nIndex, Integer dwNewLong
        Declare Integer GetClassLong In WIN32API;
            INTEGER HWnd, Integer nIndex
        Declare Integer DefWindowProc In WIN32API;
            INTEGER HWnd, Integer wMsg, Integer wParam, Integer Lparam
        Declare Integer SetFocus In WIN32API;
            INTEGER HWnd
        Declare Integer RegCloseKey In WIN32API Integer hKey
        Declare Integer RegOpenKeyEx In WIN32API;
            INTEGER hKey, String lpSubKey, Integer ulOptions,;
            INTEGER samDesired, Integer @phkResult
        Declare Integer RegQueryValueEx In WIN32API;
            INTEGER hKey, String lpValueName, Integer lpReserved,;
            INTEGER @lpType, String @lpData, Integer @lpcbData
        Declare Integer ExtractIconEx In shell32;
            STRING lpszFile, Integer nIconIndex, Integer phiconLarge, Integer phiconSmall, Integer nIcons
        Declare Integer ExtractIcon In shell32;
            INTEGER hInst, String lpszExeFileName, Integer nIconIndex
        Declare Integer ExtractAssociatedIcon In shell32;
            INTEGER hInst, String lpIconPath, Integer @lpiIcon
        Declare Integer ShellExecute In shell32.Dll ;
            integer nHwnd, String cOperation, String cFileName, String cParms, String cDir, Integer nShowCmd
        Declare Integer GetKeyboardState In WIN32API String @lpKeyState
        Declare Integer AlphaBlend In Msimg32;
            INTEGER hDestDC, Integer x, Integer Y,;
            INTEGER nWidth, Integer nHeight, Integer hSrcDC,;
            INTEGER xSrc, Integer ySrc, Integer nWidthSrc,;
            INTEGER nHeightSrc, Integer blendFunction
        Declare Integer CreatePopupMenu In WIN32API
        Declare Integer DestroyWindow In WIN32API;
            INTEGER HWnd
        Declare Integer RealGetWindowClass In WIN32API;
            INTEGER  HWnd,;
            STRING @ pszType,;
            INTEGER  cchType
        Declare Integer GetActiveWindow In WIN32API
        Declare Integer CreateWindowEx In WIN32API;
            INTEGER dwExStyle,;
            STRING lpClassName,;
            STRING lpWindowName,;
            INTEGER dwStyle,;
            INTEGER x,;
            INTEGER Y,;
            INTEGER nWidth,;
            INTEGER nHeight,;
            INTEGER hWndParent,;
            INTEGER hMenu,;
            INTEGER hInstance,;
            INTEGER lpParam
        Declare Integer IsWindow In WIN32API Integer HWnd
        Declare Integer GetLastError In WIN32API
        Declare Integer GetClassName In WIN32API ;
            Integer nhWnd, String lpClassName, Integer nMaxCount
        Declare SetWindowRgn In WIN32API Integer HWnd, Integer hRgn, Integer bRedraw
        Declare Integer CreateRoundRectRgn In WIN32API;
            Integer nLeftRect, Integer nTopRect,;
            Integer nRightRect, Integer nBottomRect,;
            Integer nWidthEllipse, Integer nHeightEllipse
        Declare Integer GetCursorPos In user32 String @lpPoint
        Declare Integer ScreenToClient In user32;
        	Integer hWindow, String @lpPoint
        Declare Long WindowFromPoint In win32api Long x, Long Y
        Declare Short CreateProcess In Win32API String lpszModuleName, String @lpszCommandLine,;
        	String lpSecurityAttributesProcess, String lpSecurityAttributesThread,;
			Short bInheritHandles, Integer dwCreateFlags, String lpvEnvironment, String lpszStartupDir,;
			String @lpStartInfo, String @lpProcessInfo
		Declare Integer OpenProcess In Win32API;
			Integer dwDesiredAccess, Integer bInheritHandle, Integer dwProcessID
		Declare Integer TerminateProcess in Win32API Integer hProcess, Integer uExitCode
		Declare Integer GetCurrentProcessId In WIN32API
		Declare Integer GetForegroundWindow In WIN32API
    Endproc

    Hidden Procedure Remove_Graphics_API
        Clear Dlls	;
            "LoadImage", "GetDC","ReleaseDC", ;
            "GetPixel", "SetPixel", "CreateCompatibleDC", ;
            "DeleteDC", "SelectObject", "DrawIconEx",;
            "CreateCompatibleBitmap", "GetSysColor", ;
            "CreateSolidBrush", "FillRect", "DeleteObject", ;
            "MulDiv", "GetFocus", "LoadIcon", "GetWindowDC", ;
            "DrawIcon" , "SetCapture", "ReleaseCapture", ;
            "SendMessage", "SetLayeredWindowAttributes", ;
            "SetWindowLong", "GetWindowLong", "PostMessage", ;
            "SystemParametersInfo", "GetDesktopWindow", ;
            "FindWindowEx", "GetWindowText", "GetWindowRect", ;
            "SetClassLong", "GetClassLong", "DefWindowProc", ;
            "SetFocus", "RegOpenKeyEx", "RegQueryValueEx", ;
            "RegCloseKey", "ExtractIconEx", "ExtractAssociatedIcon", ;
            "GetKeyboardState", "AlphaBlend"
    Endproc

	Proc CreateCursorPropFromXML(bChangedTheme)
		LOCAL cHome,i,cPath,cFile,bTrovato,i,j
		IF !cp_fileexist (This.ImgPath + This.PropCurName+".dbf")
			Create Table (This.ImgPath + This.PropCurName) Free (ThemeNo N(3), PropertyNo N(3), PropertyTy N(1), ;
			    FirstBegin N(8), FirstEnd N(8), ;
			    SecondBegi N(8), SecondEnd N(8) , ;
			    FadeDir N(3), SplitPerc N(3), SplitSpace N(5), ;
			    ImgWidth N(8), ImgHeight N(8), MaskBmp C(254), ;
			    BmpForeCol N(8), PicPath C(254))
			Index On Str(ThemeNo)+Str(PropertyNo) Tag "PCN_Idx"
			Insert Into (This.PropCurName) Values (0,0,2,-1,-1,-1,-1,-1,-1,-1,-1,-1,"",-1,"")	&&Applications count
		ELSE
			IF !Used (This.PropCurName)
           		Use (This.ImgPath + This.PropCurName) Shared In 0
            ENDIF
		ENDIF		
		If !bChangedTheme
		
			bTrovato = .t.
		    If This.ActiveTheme>0 AND (IsAhe() Or IsAhr() Or IsAlt())
			    local i,j,fname,cPath,cFile, cNumTheme, cThemePath
			    cThemePath = cPathVfcsim+'\Themes\'
			    ADIR(aDirectories, cThemePath+'*','D')
			    i=1
			    bTrovato = .F.
			    Do While m.i<=ALEN(aDirectories,1) And Not bTrovato
			    	fname = aDirectories[i,1]
					bApplication = (IsAlt() And Not 'AHR'$Upper(m.fname) And Not 'AHE'$Upper(m.fname)) Or (IsAhe() And Not 'AHR'$Upper(m.fname) And Not 'AETOP'$Upper(m.fname)) or (IsAhr() And Not 'AHE'$Upper(m.fname) And Not 'AETOP'$Upper(m.fname))
					If m.bApplication And m.fname<>'.' and m.fname<>'..' and 'D'$aDirectories[m.i,5] and IsDigit(m.fname) And Alltrim(Str(This.ActiveTheme)) == Left(m.fname, At('.', m.fname)-1)
				        cPath = m.cThemePath+aDirectories[m.i,1]
				        cFile = Alltrim(Substr(m.fname, At('.', m.fname)+1))
				        If cp_fileexist(m.cPath+"\"+m.cFile+".xml") 
							If This.ReadXMLTheme(cPath+"\"+cFile+".xml") = -1
								cp_ErrorMsg("Errore in formattazione file tema")
							Endif
						    bTrovato = .T.
				        Endif
			     	Endif
				    i=m.i+1
			    EndDo
			Endif
			If Not (IsAhe() Or IsAhr() Or IsAlt()) Or Not bTrovato
				If Not bTrovato
					Local nCodute, nIdx, nConn, cTable
					cp_ErrorMsg("File tema non trovato, verr� applicato il tema Standard")
					If Type("i_TableProp")<>"U"
						*-- Aggiorno il DB settando il tema -1
						nIdx = cp_GetTableDefIdx("CONF_INT")
					    nConn=i_TableProp[m.nIdx,3] 
						cTable=cp_SetAzi(i_TableProp[m.nIdx,2])
				    	nCodute = Iif(Type("i_codute")<>"U",i_codute,-1)
				    	iRows = cp_SqlExec( m.nConn,"update "+m.cTable+" set CIVTHEME ="+cp_ToStrODBC(-1)+" where CICODUTE = "+cp_ToStrODBC(m.nCodute) )
				    Endif
			    Endif
				i_VisualTheme = -1
				*This.ActiveTheme = -1
				*This.ThemeNumber = -1
			EndIf
		Endif
	EndProc
	
	Func IsInThemes(nValue, oObj)
		*-- Funzione che mi ritorna .T. se il tema � presente nella cartella Vfcsim\Themes, chiamato dalla CPToolBar
		If nValue=-1
			Return .F.
		Endif
		If IsAhe() Or IsAhr() Or IsAlt()
	      local i,fname,cPath,cFile, cThemePath
		    cThemePath = cPathVfcsim+'\Themes\'
		    ADIR(aDirectories, cThemePath+'*','D')
		    i=1
		    Do While m.i<=ALEN(aDirectories,1)
		    	fname = aDirectories[i,1]
	     		bApplication = (IsAlt() And Not 'AHR'$Upper(fname) And Not 'AHE'$Upper(fname)) Or (IsAhe() And Not 'AHR'$Upper(fname) And Not 'AETOP'$Upper(fname)) or (IsAhr() And Not 'AHE'$Upper(fname) And Not 'AETOP'$Upper(fname))
				  If m.bApplication And m.fname<>'.' and m.fname<>'..' and 'D'$aDirectories[m.i,5] and IsDigit(m.fname) And Alltrim(Str(nValue)) == Left(m.fname, At('.', m.fname)-1)
			        cPath = m.cThemePath+aDirectories[m.i,1]
			        cFile = Alltrim(Substr(m.fname, At('.', m.fname)+1))
			        If cp_fileexist(m.cPath+"\"+m.cFile+".xml")
             			Return .T.
			        Endif
		     	Endif
			    i=m.i+1
		    EndDo
	    EndIf
		Local nCodute, nIdx, nConn, cTable
		cp_ErrorMsg("File tema non trovato, verr� applicato il tema Standard")
		*-- Se si viene dalla CPToolbar
		If Type("m.oObj.bNewStyle")<>"U"
			m.oObj.bNewStyle = .F.
		EndIf
		If Type("i_TableProp")<>"U"
			*-- Aggiorno il DB settando il tema -1
			nIdx = cp_GetTableDefIdx("CONF_INT")
		    nConn=i_TableProp[m.nIdx,3] 
			cTable=cp_SetAzi(i_TableProp[m.nIdx,2])
	    	nCodute = Iif(Type("i_codute")<>"U",i_codute,-1)
	    	iRows = cp_SqlExec( m.nConn,"update "+m.cTable+" set CIVTHEME ="+cp_ToStrODBC(-1)+" where CICODUTE = "+cp_ToStrODBC(m.nCodute) )
	    Endif
		i_VisualTheme = -1
		*This.ActiveTheme = -1
		*This.ThemeNumber = -1
		Return .F.
	Endfunc

    Procedure Init_ThemesArray(bChangedTheme)
        Local bResult, cTmpCursor, bNewTheme
        This.PropCurName = 'ThemeCurs'	&&ALLTRIM(SYS(2015))
        This.RBmpCurName = 'RBmpCurs'
        * PropertyTy	(PropertyType)
        *		1	=	Color
        *		2	=	Space/Dimension
        *		3	=	Gradient/Mask/Gradient+Mask
        *		4	=	Gripper
        * FadeDir
        *		0	=	Horizontal
        *		1	=	Vertical
        If cp_fileexist (This.ImgPath + This.RBmpCurName+".dbf")
            Use (This.ImgPath + This.RBmpCurName) Shared In 0
        Endif
        If cp_fileexist (This.ImgPath + This.PropCurName+".dbf")
            Use (This.ImgPath + This.PropCurName) Shared In 0
*!*	            If Reccount()<This.TotalThemes * This.TotThemesProp
*!*	                Use In (This.PropCurName)
*!*	                Local l_OldErr, l_err
*!*	                l_err = .F.
*!*	                l_OldErr = On("error")
*!*	                On Error l_err=.T.
*!*	                Delete File (This.ImgPath + This.PropCurName+".dbf")
*!*	                On Error &l_OldErr
*!*	                If !cp_fileexist (This.ImgPath + This.PropCurName+".dbf")	&&Non ho errore sono riuscito a cancellare il file
*!*	                    This.CreateCursorPropFromXML()
*!*	                    Use In (This.PropCurName)
*!*	                Endif
*!*	                Use (This.ImgPath + This.PropCurName) Shared In 0
*!*	            Endif
        Endif
        m.bResult = This.Remove_Graphics(.T.)
        If m.bResult
            This.Remove_Bitmaps()
        EndIf
        If !Used(This.RBmpCurName)
            *Creazione cursore con le immagini gi� elaborate
            Create Table (This.ImgPath + This.RBmpCurName) Free (ThemeNo N(1), PropertyNo N(3), OrigImg C(240), OrigDim N(3), OutImg C(240))
            Index On OrigImg Tag "OI_Idx" COLLATE "MACHINE"
            Use In (This.RBmpCurName)
            Use (This.ImgPath + This.RBmpCurName) Shared In 0
        Endif
        *Creazione cursore propriet� dei temi
        If Used(This.PropCurName) And i_visualtheme<>-1
	        cTmpCursor = SYS(2015)
	  		Select Distinct ThemeNo From (This.PropCurName) Where ThemeNo=(This.ActiveTheme) Into Cursor (cTmpCursor)
	  		bNewTheme = (Reccount()=0)
	  		Use In Select(cTmpCursor)
		Endif
        If (!Used(This.PropCurName) Or m.bNewTheme)
            This.CreateCursorPropFromXML(m.bChangedTheme)
            Use In (This.PropCurName)
            Use (This.ImgPath + This.PropCurName) Shared In 0
        Else
            *Aggiorno numero installazioni attive
            Update (This.PropCurName) Set PropertyTy = PropertyTy + 1 Where ThemeNo=0 And PropertyNo=0
        Endif
    Endproc

    *-- Get a specified theme member.
    Procedure GetProp

        * Parametri:
        *!* L_Prop 			=	Propriet� da ritornare se maggiore di zero viene recuperata dal cursore e se sono valorizzati i
        *!* parametri opzionali questi vanno a sostituire quelli standard. Se zero non viene effettuata
        *!* ricerca nel cursore e vengono sfruttate le propriet� passate (in questo caso obbligatorie) per
        *!* costruire la propriet� e restituirla
        *!* p_PropTy		=	Tipo propriet� (Valorizzabile solo se L_Prop=0)
        *!* 1 = Spaziature in pixels
        *!* 2 = Colore
        *!* 3 = Gradienti e/o immagini
        *!* 4 = Gripper
        *!* 5 = NameTheme
        *!* p_FirstBegin	=	Colore iniziale primo gradiente
        *!* p_FirstEnd		=	Colore finale primo gradiente
        *!* p_SecondBegi=	Colore iniziale secondo gradiente
        *!* p_SecondEnd	=	Colore finale secondo gradiente
        *!* p_FadeDir		=	Direzione gradiente
        *!* 0 = Orizzontale
        *!* 1 = Verticale
        *!* p_SplitPerc		=	Percentuale di divisione fra i due gradienti
        *!* p_SplitSpace	=	Spazio in pixel fra i due gradienti
        *!* p_ImgWidth	=	Larghezza dell'immagine o gradiente da creare
        *!* p_ImgHeight	=	Lunghezza dell'immagine o gradiente da creare
        *!* p_MaskBmp		=	Nome icona della maschera  da utilizzare
        *!* p_BmpForeCol	=	Colore da applicare alla maschera

        Lparameters L_Prop, p_PropTy, p_FirstBegin, p_FirstEnd, p_SecondBegi, p_SecondEnd, p_FadeDir, p_SplitPerc, p_SplitSpace, p_ImgWidth, p_ImgHeight, p_MaskBmp, p_BmpForeCol
        Local vProp, nOldArea, bMod, cModStr, nUsedTheme, L_FirstBegin, L_FirstEnd, L_SecondBegi, L_SecondEnd, L_FadeDir, L_SplitPerc, L_SplitSpace, L_ImgWidth, L_ImgHeight, L_MaskBmp, L_BmpForeCol, L_GradWidth, L_GradHeight, L_bGrad, L_OutImg, L_TmpBmp, L_ICOPath, L_PicPath, L_OldBmp, L_PropertyTy
        m.vProp=-1
        m.bMod = Vartype(m.p_FirstBegin)='N' Or Vartype(m.p_FirstEnd)='N' Or Vartype(m.p_SecondBegi)='N' Or Vartype(m.p_SecondEnd)='N' Or Vartype(m.p_SplitPerc)='N' Or Vartype(m.p_SplitSpace)='N' Or Vartype(m.p_ImgWidth)='N' Or Vartype(m.p_ImgHeight)='N' Or Vartype(m.p_MaskBmp)='C' Or Vartype(m.p_BmpForeCol)='N'
        m.cModStr = "" + Iif(Vartype(m.p_FirstBegin)='N', Alltrim(Str(m.p_FirstBegin)), "") + Iif(Vartype(m.p_FirstEnd)='N', Alltrim(Str(m.p_FirstEnd)), "") + Iif(Vartype(m.p_SecondBegi)='N', Alltrim(Str(m.p_SecondBegi)), "") + Iif(Vartype(m.p_SecondEnd)='N', Alltrim(Str(m.p_SecondEnd)), "") + Iif(Vartype(m.p_SplitPerc)='N', Alltrim(Str(m.p_SplitPerc)), "") + Iif(Vartype(m.p_SplitSpace)='N', Alltrim(Str(m.p_SplitSpace)), "") + Iif(Vartype(m.p_ImgWidth)='N', Alltrim(Str(m.p_ImgWidth)), "") + Iif(Vartype(m.p_MaskBmp)='C', Alltrim(m.p_MaskBmp), "") + Iif(Vartype(m.p_BmpForeCol)='N', Alltrim(Str(m.p_BmpForeCol)), "")
        m.nOldArea=Select()
        m.L_PropertyTy = 1000
        If m.L_Prop>0 Or p_PropTy=5
            Select (This.PropCurName)
            Go Top
            Locate For ThemeNo=This.ActiveTheme And PropertyNo=m.L_Prop
            m.nUsedTheme = This.ActiveTheme
            If Not Found()  And This.ActiveTheme>This.NumThemes
                Go Top
                Locate For ThemeNo=This.DefaultStdTheme And PropertyNo=m.L_Prop
                m.nUsedTheme = This.DefaultStdTheme
            Endif
            If Found()
                Do Case
                    Case PropertyTy<=2
                        *Per le propriet� colore singolo e spaziature ritorno
                        *direttamente il valore
                        m.L_PropertyTy = 2
                        m.L_FirstBegin = Iif(Vartype(m.p_FirstBegin)<>'N', FirstBegin, m.p_FirstBegin)
                    Case PropertyTy=3
                        *Per le propriet� gradienti, maschere e gradienti+maschere
                        *elaboro l'immagine e restituisco la posizione di dove
                        *� stata salvata e con che nome
                        m.L_FirstBegin = Iif(Vartype(m.p_FirstBegin)<>'N', FirstBegin, m.p_FirstBegin)
                        m.L_FirstEnd = Iif(Vartype(m.p_FirstEnd)<>'N', FirstEnd, m.p_FirstEnd)
                        m.L_SecondBegi = Iif(Vartype(m.p_SecondBegi)<>'N', SecondBegi, m.p_SecondBegi)
                        m.L_SecondEnd = Iif(Vartype(m.p_SecondEnd)<>'N', SecondEnd, m.p_SecondEnd)
                        m.L_FadeDir = Iif(Vartype(m.p_FadeDir)<>'N', FadeDir, m.p_FadeDir)
                        m.L_SplitPerc = Iif(Vartype(m.p_SplitPerc)<>'N', SplitPerc, m.p_SplitPerc)
                        m.L_SplitSpace = Iif(Vartype(m.p_SplitSpace)<>'N', SplitSpace, m.p_SplitSpace)
                        m.L_ImgWidth = Iif(Vartype(m.p_ImgWidth)<>'N', ImgWidth, m.p_ImgWidth)
                        m.L_ImgHeight = Iif(Vartype(m.p_ImgHeight)<>'N', ImgHeight, m.p_ImgHeight)
                        m.L_MaskBmp = Iif(Vartype(m.p_MaskBmp)<>'C', MaskBmp, Alltrim(m.p_MaskBmp))
                        m.L_BmpForeCol = Iif(Vartype(m.p_BmpForeCol)<>'N', BmpForeCol, m.p_BmpForeCol)
                        m.L_bGrad= .F.
                        m.L_PicPath = PicPath
                        m.L_OutImg = Iif(!cp_fileexist(m.L_PicPath),"", m.L_PicPath)
                        m.L_PropertyTy = 3
                    Case PropertyTy=4
                        m.L_FadeDir = FadeDir
                        m.L_ImgWidth = ImgWidth
                        m.L_ImgHeight = ImgHeight
                        m.L_BmpForeCol = BmpForeCol
                        m.L_PicPath = PicPath
                        m.L_OutImg = Iif(!cp_fileexist(m.L_PicPath),"", m.L_PicPath)
                        m.L_PropertyTy = 4
                    Case PropertyTy=5
                        m.L_MaskBmp = Iif(Vartype(m.p_MaskBmp)<>'C', MaskBmp, Alltrim(m.p_MaskBmp))
                Endcase
            Endif
        Else
            Select (This.RBmpCurName)
            Go Top
            Locate For ThemeNo=This.ActiveTheme And PropertyNo=m.L_Prop And OrigImg=m.cModStr
            m.nUsedTheme = This.ActiveTheme
            If Not Found()  And This.ActiveTheme>This.NumThemes
                Go Top
                Locate For ThemeNo=This.DefaultStdTheme And PropertyNo=m.L_Prop And OrigImg=m.cModStr
                m.nUsedTheme = This.DefaultStdTheme
            Endif
            If Found()
                m.vProp = Alltrim(OutImg)
                m.L_PropertyTy = 1000
            Else
                Do Case
                    Case PropertyTy<=2
                        *Per le propriet� colore singolo e spaziature ritorno
                        *direttamente il valore
                        m.L_PropertyTy = 2
                        m.L_FirstBegin = Iif(Vartype(m.p_FirstBegin)<>'N', -1, m.p_FirstBegin)
                    Case PropertyTy=3
                        *Per le propriet� gradienti, maschere e gradienti+maschere
                        *elaboro l'immagine e restituisco la posizione di dove
                        *� stata salvata e con che nome
                        m.L_FirstBegin = Iif(Vartype(m.p_FirstBegin)<>'N', -1, m.p_FirstBegin)
                        m.L_FirstEnd = Iif(Vartype(m.p_FirstEnd)<>'N', -1, m.p_FirstEnd)
                        m.L_SecondBegi = Iif(Vartype(m.p_SecondBegi)<>'N', -1, m.p_SecondBegi)
                        m.L_SecondEnd = Iif(Vartype(m.p_SecondEnd)<>'N', -1, m.p_SecondEnd)
                        m.L_FadeDir = Iif(Vartype(m.p_FadeDir)<>'N', -1, m.p_FadeDir)
                        m.L_SplitPerc = Iif(Vartype(m.p_SplitPerc)<>'N', -1, m.p_SplitPerc)
                        m.L_SplitSpace = Iif(Vartype(m.p_SplitSpace)<>'N', -1, m.p_SplitSpace)
                        m.L_ImgWidth = Iif(Vartype(m.p_ImgWidth)<>'N', -1, m.p_ImgWidth)
                        m.L_ImgHeight = Iif(Vartype(m.p_ImgHeight)<>'N', -1, m.p_ImgHeight)
                        m.L_MaskBmp = Iif(Vartype(m.p_MaskBmp)<>'C', "", Alltrim(m.p_MaskBmp))
                        m.L_BmpForeCol = Iif(Vartype(m.p_BmpForeCol)<>'N', -1, m.p_BmpForeCol)
                        m.L_bGrad= .F.
                        m.L_PicPath = ""
                        m.L_OutImg = ""
                        m.L_PropertyTy = 3
                    Case PropertyTy=4
                        m.L_FadeDir = FadeDir
                        m.L_ImgWidth = ImgWidth
                        m.L_ImgHeight = ImgHeight
                        m.L_BmpForeCol = BmpForeCol
                        m.L_PicPath = PicPath
                        m.L_OutImg = Iif(!cp_fileexist(m.L_PicPath),"", m.L_PicPath)
                        m.L_PropertyTy = 4
                Endcase
            Endif
        Endif

        Do Case
            Case m.L_PropertyTy <= 2
                *Per le propriet� colore singolo e spaziature ritorno
                *direttamente il valore
                m.vProp = m.L_FirstBegin
            Case PropertyTy=3
                *Per le propriet� gradienti, maschere e gradienti+maschere
                *elaboro l'immagine e restituisco la posizione di dove
                *� stata salvata e con che nome
                m.L_bGrad= .F.
                If Empty(m.L_OutImg)
                    If m.L_FirstBegin>-1
                        *Gradiente
                        m.L_bGrad= .T.
                        L_GradWidth = m.L_ImgWidth
                        L_GradHeight = m.L_ImgHeight
                        If m.L_SecondBegi>-1
                            *Gradiente doppio
                            If m.L_FadeDir=1
                                m.L_OutImg = This.DoubleVGradient(m.L_GradWidth, m.L_GradHeight, m.L_FirstBegin, m.L_FirstEnd, ;
                                    m.L_SplitPerc, m.L_SplitSpace, m.L_SecondBegi, m.L_SecondEnd)
                            Else
                                m.L_OutImg = This.DoubleHGradient(m.L_GradWidth, m.L_GradHeight, m.L_FirstBegin, m.L_FirstEnd, ;
                                    m.L_SplitPerc, m.L_SplitSpace, m.L_SecondBegi, m.L_SecondEnd)
                            Endif
                        Else
                            *Gradiente singolo
                            If m.L_FadeDir=1
                                m.L_OutImg = This.VGradient(m.L_GradWidth, m.L_GradHeight, m.L_FirstBegin, m.L_FirstEnd)
                            Else
                                m.L_OutImg = This.HGradient(m.L_GradWidth, m.L_GradHeight, m.L_FirstBegin, m.L_FirstEnd)
                            Endif
                        Endif
                    Endif
                    If Vartype(m.L_MaskBmp)='C' And !Empty(m.L_MaskBmp)
                        *Maschera immagine, la devo aggiungere al gradiente
                        *appena creato o creare l'immagine e la maschera
                        *relativa nuove se non ho il gradiente
                        m.L_ICOPath = cp_GetStdImg(Forceext(m.L_MaskBmp,''),'ico')
                        If !Empty(m.L_ICOPath)
                            m.L_TmpBmp = This.GetBmpFromIco(m.L_ICOPath+'.ico', m.L_ImgWidth, m.L_Prop, .T., .T.)
                            m.L_OldBmp = m.L_OutImg
                            m.L_OutImg = This.OverlayBitmap(m.L_ImgWidth, m.L_OutImg, m.L_TmpBmp+'.bmp',m.L_BmpForeCol)
                            If !Empty(m.L_OldBmp) And cp_fileexist(m.L_OldBmp)
                                Delete File (m.L_OldBmp)
                            Endif
                        Endif
                    Endif
                    If m.bMod
                        Insert Into (This.RBmpCurName) Values ( m.nUsedTheme, m.L_Prop, m.cModStr, m.L_ImgWidth, m.L_OutImg)
                    Else
                        Select (This.PropCurName)
                        Replace PicPath With m.L_OutImg
                    Endif
                Endif
                m.vProp = m.L_OutImg
            Case PropertyTy=4

                If Empty(m.L_OutImg)
                    m.L_OutImg = This.CreateGripper(m.L_ImgWidth, m.L_ImgHeight, m.L_FadeDir, m.L_BmpForeCol)
                    If m.bMod
                        Insert Into (This.RBmpCurName) Values ( m.nUsedTheme, m.L_Prop, m.cModStr, m.L_ImgWidth, m.L_OutImg)
                    Else
                        Select (This.PropCurName)
                        Replace PicPath With m.L_OutImg
                    Endif
                Endif
                m.vProp = m.L_OutImg
            Case PropertyTy=5
                m.vProp = m.L_MaskBmp
        Endcase
        Select (m.nOldArea)
        Return m.vProp
    Endproc

    Procedure GetSingleProp
        Lparameters L_Prop, L_IdxProp
        * L_IdxProp =
        *		1 	=	PropertyTy
        *		2	=	FirstBegin
        *		3	=	FirstEnd
        *		4	=	SecondBegi
        *		5	=	SecondEnd
        *		6	=	FadeDir
        *		7	=	SplitPerc
        *		8	=	SplitSpace
        *		9	=	ImgWidth
        *		10	=	ImgHeight
        *		11	=	MaskBmp
        *		12	=	BmpForeCol
        *		13	=	PicPath

        Local vProp, nOldArea, L_FirstBegin, L_FirstEnd, L_SecondBegi, L_SecondEnd, L_FadeDir, L_SplitPerc, L_SplitSpace, L_ImgWidth, L_ImgHeight, L_MaskBmp, L_BmpForeCol, L_GradWidth, L_GradHeight, L_bGrad, L_OutImg, L_TmpBmp, L_ICOPath, L_PicPath, L_OldBmp
        m.vProp=-1
        m.nOldArea = Select()
        Select (This.PropCurName)
        Go Top
        Locate For ThemeNo=This.ActiveTheme And PropertyNo=m.L_Prop
        If Not Found() And This.ActiveTheme>This.NumThemes
            Go Top
            Locate For ThemeNo=This.DefaultStdTheme And PropertyNo=m.L_Prop
        Endif
        If Found()
            If m.L_IdxProp = 1
                m.vProp = PropertyTy
            Else
                Do Case
                    Case PropertyTy<=2
                        *Per le propriet� colore singolo e spaziature ritorno
                        *direttamente il valore
                        If m.L_IdxProp = 2
                            m.vProp = FirstBegin
                        Endif
                    Case PropertyTy=3
                        *Per le propriet� gradienti, maschere e gradienti+maschere
                        *elaboro l'immagine e restituisco la posizione di dove
                        *� stata salvata e con che nome
                        Do Case
                            Case m.L_IdxProp = 2
                                m.vProp = FirstBegin
                            Case m.L_IdxProp = 3
                                m.vProp = FirstEnd
                            Case m.L_IdxProp = 4
                                m.vProp = SecondBegi
                            Case m.L_IdxProp = 5
                                m.vProp = SecondEnd
                            Case m.L_IdxProp = 6
                                m.vProp = FadeDir
                            Case m.L_IdxProp = 7
                                m.vProp = SplitPerc
                            Case m.L_IdxProp = 8
                                m.vProp = SplitSpace
                            Case m.L_IdxProp = 9
                                m.vProp = ImgWidth
                            Case m.L_IdxProp = 10
                                m.vProp = ImgHeight
                            Case m.L_IdxProp = 11
                                m.vProp = MaskBmp
                            Case m.L_IdxProp = 12
                                m.vProp = BmpForeCol
                            Case m.L_IdxProp = 13
                                m.vProp = PicPath
                        Endcase
                    Case PropertyTy=4
                        Do Case
                            Case m.L_IdxProp = 6
                                m.vProp = FadeDir
                            Case m.L_IdxProp = 9
                                m.vProp = ImgWidth
                            Case m.L_IdxProp = 10
                                m.vProp = ImgHeight
                            Case m.L_IdxProp = 12
                                m.vProp = BmpForeCol
                            Case m.L_IdxProp = 13
                                m.vProp = PicPath
                        Endcase
                Endcase
            Endif
        Endif
        Select (m.nOldArea)
        Return m.vProp
    Endproc

    Procedure CreateGripper
        Lparameters nDimX, nDimY, nDirection, nColor
        *Per i gripper occorre creare un immagine di altezza (largezza se verticale)
        *uguale a quella del gripper e di largezza (altezza se verticale) uguale alla
        *dimensione+1 in modo che quando viene replicato gli spazi vengono rispettati
        Local L_EmptyBmp, L_oImg,i_HDC, i_HMemDC, i_HSel, L_oMsk, i_HMskDC, k_HSel
        Local L_NumGripper, L_AttGrip, L_AttX, L_AttY
        m.L_EmptyBmp = This.CreateBitmap(m.nDimX,m.nDimY,Right(Transform(Rgb(255,255,255),"@0"),6))
        If m.nDirection=0
            m.L_NumGripper=Int((m.nDimX-10)/4)
        Else
            m.L_NumGripper=Int((m.nDimY-10)/4)
        Endif
        m.L_oImg=LoadPicture(m.L_EmptyBmp)
        m.L_oMsk=LoadPicture(m.L_EmptyBmp)
        m.i_HDC = GetDC(0)
        m.i_HMemDC= CreateCompatibleDC(m.i_HDC)
        DeleteObject(SelectObject(m.i_HMemDC, m.L_oImg.handle))
        m.i_HMskDC= CreateCompatibleDC(m.i_HDC)
        DeleteObject(SelectObject(m.i_HMskDC, m.L_oMsk.handle))
        m.L_AttX=0
        m.L_AttY=0
        For m.L_AttGrip=1 To m.L_NumGripper
            SetPixel(m.i_HMemDC, m.L_AttX, m.L_AttY, m.nColor)
            SetPixel(m.i_HMskDC, m.L_AttX, m.L_AttY, 0)
            SetPixel(m.i_HMemDC, m.L_AttX+1, m.L_AttY, m.nColor)
            SetPixel(m.i_HMskDC, m.L_AttX+1, m.L_AttY, 0)
            SetPixel(m.i_HMemDC, m.L_AttX, m.L_AttY+1, m.nColor)
            SetPixel(m.i_HMskDC, m.L_AttX, m.L_AttY+1, 0)
            SetPixel(m.i_HMemDC, m.L_AttX+1, m.L_AttY+1, m.nColor)
            SetPixel(m.i_HMskDC, m.L_AttX+1, m.L_AttY+1, 0)
            SetPixel(m.i_HMemDC, m.L_AttX+2, m.L_AttY+1, Rgb(255,255,255))
            SetPixel(m.i_HMskDC, m.L_AttX+2, m.L_AttY+1, 0)
            SetPixel(m.i_HMemDC, m.L_AttX+2, m.L_AttY+2, Rgb(255,255,255))
            SetPixel(m.i_HMskDC, m.L_AttX+2, m.L_AttY+2, 0)
            SetPixel(m.i_HMemDC, m.L_AttX+1, m.L_AttY+2, Rgb(255,255,255))
            SetPixel(m.i_HMskDC, m.L_AttX+1, m.L_AttY+2, 0)
            If m.nDirection=0
                m.L_AttX = m.L_AttX + 4
            Else
                m.L_AttY = m.L_AttY + 4
            Endif
        Next
        DeleteDC(m.i_HMemDC)
        ReleaseDC(0, m.i_HDC)
        DeleteDC(m.i_HMskDC)
        SavePicture(m.L_oImg, m.L_EmptyBmp)
        SavePicture(m.L_oMsk, Forceext(m.L_EmptyBmp, "msk"))
        Return m.L_EmptyBmp
    Endproc

    Procedure GrayScaleBmp
        Lparameters cOrigBmp, nDimension, nPropertyNum, bColorize, cExt
        Local cOutBmp, pX, pY, i_HDC, i_HMemDC, i_HSel
        Local oImg, nColorAtt, nRed, nGreen, nBlue, nLuma
        Local nOldArea, nUsedTheme, m.nH, m.nS, m.nL, l_cOrigBmpFullPath, l_cFullPath, l_bAlpha, l_cFullPathMsk
        #Define GetRColor Bitand(255,Bitrshift(nColorAtt,0))
        #Define GetGColor Bitand(255,Bitrshift(nColorAtt,8))
        #Define GetBColor Bitand(255,Bitrshift(nColorAtt,16))
        m.nOldArea = Select()
        m.nPropertyNum = Iif(Vartype(m.nPropertyNum)='N', m.nPropertyNum, 0)
        m.cExt = Iif(Vartype(m.cExt)='C', m.cExt, "Bmp")
        l_cOrigBmpFullPath = Upper(Alltrim(m.cOrigBmp)+Iif(m.bColorize, '_C', ''))
        Select (This.RBmpCurName)
        Go Top
        Locate For ThemeNo=Iif(m.bColorize, This.ActiveTheme,0) And Alltrim(OrigImg)==l_cOrigBmpFullPath And OrigDim=m.nDimension And PropertyNo=m.nPropertyNum
        m.nUsedTheme=Iif(m.bColorize, This.ActiveTheme,0)
        If Not Found() And m.bColorize And This.ActiveTheme>This.NumThemes
            Go Top
            Locate For ThemeNo=This.DefaultStdTheme And Alltrim(OrigImg)==l_cOrigBmpFullPath And OrigDim=m.nDimension  And PropertyNo=m.nPropertyNum
            m.nUsedTheme=This.DefaultStdTheme
        Endif
        If Not Found()
            m.cOutBmp = Alltrim(Sys(2015))
            m.cOutBmp = _Screen.CP_THEMESMANAGER.ImgPath + Right(m.cOutBmp, Len(m.cOutBmp)-1)
            l_cFullPath = Forceext(m.cOrigBmp, m.cExt)
            l_cFullPathMsk = Forceext(m.cOrigBmp, ".msk")
            If cp_fileexist(m.l_cFullPath)
                *--- Occorrerebbe fare una conversione se la tipologia iniziale non � bmp
                *--- Copy File (m.l_cFullPath) To (m.cOutBmp+".bmp")
                If cp_fileexist(l_cFullPathMsk)
                    Copy File (l_cFullPathMsk) To (m.cOutBmp+".msk")
                Endif
                If !m.bColorize
                    cb_GrayScaleBmp(l_cFullPath, m.cOutBmp+".bmp")
                Else
                    cb_ColorizeBmp(l_cFullPath, m.cOutBmp+".bmp", i_ThemesManager.GetProp(TOOLBAR_BTN_DISABLEDCOLOR))
                Endif
                Insert Into (This.RBmpCurName) Values (m.nUsedTheme, m.nPropertyNum, l_cOrigBmpFullPath, m.nDimension, Alltrim(m.cOutBmp))
            Else
                m.cOutBmp=m.cOrigBmp
            Endif
        Else
            *Bitmap gi� processata
            m.cOutBmp = Alltrim(OutImg)
        Endif
        Select (m.nOldArea)
        Return m.cOutBmp
    Endproc

    Procedure ColorizeBmp
        Lparameters cOrigBmp, nDimension, nPropertyNum
        Return This.GrayScaleBmp(m.cOrigBmp, m.nDimension, m.nPropertyNum, .T.)
    Endproc

    Procedure PutBmp
		Lparameters oDest, cBmp, nDim, nPropertyNum
		Local cOrigBmp, cAttBmp, cDisabledBmp, nOldArea, nUsedTheme, l_bmpFullName
		m.nPropertyNum = Iif(Vartype(m.nPropertyNum)='N', m.nPropertyNum, 0)
		m.nOldArea = Select()
		If Vartype(m.cBmp)='C' And !Empty(m.cBmp)
			m.cOrigBmp = Iif(Lower(Justext(m.cBmp)) == 'ico', Forceext(m.cBmp, "bmp"), m.cBmp)
			m.cAttBmp = i_ThemesManager.RetBmpFromIco(m.cOrigBmp, m.nDim)
			If type("_screen.ConvImageTrasp")<>'O'
				_screen.addobject('ConvImageTrasp','image')
			Endif
			l_bmpFullName = Forceext(m.cAttBmp, Justext(m.cOrigBmp))	
			IF LOWER(m.oDest.baseclass) = "commandbutton"
				_screen.ConvImageTrasp.picture= m.l_bmpFullName	
			EndIf
			m.oDest.Picture = m.l_bmpFullName	
			If Vartype(m.oDest.DisabledPicture)<>'U'
				m.cDisabledBmp = i_ThemesManager.GrayScaleBmp(m.cAttBmp, m.nDim,,,Justext(m.cOrigBmp))
				_screen.ConvImageTrasp.picture=m.cDisabledBmp + ".bmp"
				m.oDest.DisabledPicture = m.cDisabledBmp + ".bmp"
			Endif
		Endif
		Select (m.nOldArea)
    Endproc

    Proc GetBmpFromIco
        Lparameters cIconFile, nDimension, nPropertyNum, bForceTheme, bNoGetStdImage
        Local hIco, nOldArea, nUsedTheme
        Local pX, pY, i_HDC, i_HMemDC, i_HSel
        Local b_HDC, b_HMemDC, b_HSel
        Local oImg, oMsk, cEmptyBmp, cOutBmp, l_cIconFullPath, cNameTheme
        If bNoGetStdImage
            l_cIconFullPath = Upper(Alltrim(cHomeDir)+Forceext(Alltrim(m.cIconFile),Justext(m.cIconFile)))
        Else
            l_cIconFullPath = Upper(Alltrim(cHomeDir)+Forceext(Alltrim(cp_GetStdImg(m.cIconFile,Justext(m.cIconFile))),Justext(m.cIconFile)))
        Endif
        m.nPropertyNum = Iif(Vartype(m.nPropertyNum)='N', m.nPropertyNum, 0)
        m.nOldArea = Select()
        m.nDimension = Iif(Vartype(m.nDimension)='N',m.nDimension,24)
        If !Empty(m.cIconFile)
            Select (This.RBmpCurName)
            Go Top
            m.nUsedTheme=Iif(m.bForceTheme, This.ActiveTheme ,0)
            Locate For ThemeNo=m.nUsedTheme And Alltrim(OrigImg) == l_cIconFullPath And OrigDim=m.nDimension And PropertyNo=m.nPropertyNum
            *m.nUsedTheme=0 &&This.ActiveTheme
            If Not Found() And m.bForceTheme And m.nUsedTheme > This.NumThemes
                Go Top
                Locate For ThemeNo=This.DefaultStdTheme And Alltrim(OrigImg)==l_cIconFullPath And OrigDim=m.nDimension And PropertyNo=m.nPropertyNum
                m.nUsedTheme=This.DefaultStdTheme
            Endif
            If Not Found()
                *!*                Dimension nIconColors[1]

                m.cOutBmp = Alltrim(Sys(2015))
                m.cOutBmp = _Screen.CP_THEMESMANAGER.ImgPath + Right(m.cOutBmp, Len(m.cOutBmp)-1)

                If cb_IsTrueColorXPIco(l_cIconFullPath)
                    cb_GetPngFromIco(l_cIconFullPath, m.nDimension, Alltrim(m.cOutBmp)+".bmp")
                Else
                    m.hIco = LoadImage(0,l_cIconFullPath, 1, m.nDimension, m.nDimension, 16)
                    m.cEmptyBmp= _Screen.CP_THEMESMANAGER.CreateBitmap(m.nDimension,m.nDimension,"FFFFFF")
                    m.oImg=LoadPicture(m.cEmptyBmp)
                    i_HDC = GetDC(0)
                    i_HMemDC= CreateCompatibleDC(i_HDC)
                    DeleteObject(SelectObject(m.i_HMemDC, oImg.handle))
                    DrawIconEx(m.i_HMemDC , 0, 0,m.hIco,m.nDimension,m.nDimension,0,0,DI_NORMAL)
                    DeleteDC(m.i_HMemDC)
                    ReleaseDC(0, m.i_HDC)
                    SavePicture(oImg, m.cOutBmp+".bmp" )
                    Clear Resources m.cEmptyBmp
                    Clear Resources m.cOutBmp+".bmp"
                    oMsk=LoadPicture(cEmptyBmp)
                    i_HDC = GetDC(0)
                    i_HMskDC= CreateCompatibleDC(i_HDC)
                    DeleteObject(SelectObject(i_HMskDC, oMsk.handle))
                    DrawIconEx(m.i_HMskDC , 0, 0,m.hIco,m.nDimension,m.nDimension,0,0,DI_MASK)
                    DeleteDC(m.i_HMskDC)
                    ReleaseDC(0, m.i_HDC)
                    SavePicture(oMsk,cOutBmp+".msk")
                    Clear Resources m.cEmptyBmp
                    Clear Resources m.cOutBmp+".msk"
                    Delete File (cEmptyBmp)
                Endif
                Insert Into (This.RBmpCurName) Values (m.nUsedTheme, m.nPropertyNum, l_cIconFullPath, m.nDimension, Alltrim(m.cOutBmp))
            Else
                *Bitmap gi� processata
                m.cOutBmp = Alltrim(OutImg)
            Endif
        Else
            m.cOutBmp = ""
        Endif
        Return m.cOutBmp
    Endproc

    Proc GetBmpFromStandardMessageBoxIco
        Lparameters nIcon
        Local hIco, nOldArea, nUsedTheme
        Local pX, pY, i_HDC, i_HMemDC, i_HSel
        Local b_HDC, b_HMemDC, b_HSel
        Local oImg, oMsk, cEmptyBmp, cOutBmp, l_cIconFullPath
        Local nStdIconNumber, nStdSize
        nStdSize = 32
        m.nOldArea = Select()
        If !Empty(m.nIcon)
            Select (This.RBmpCurName)
            Go Top
            m.nUsedTheme=This.ActiveTheme
            Locate For ThemeNo=m.nUsedTheme And Alltrim(OrigImg) == Alltrim(Str(m.nIcon)) And PropertyNo=m.nIcon*-1 And OrigDim=m.nStdSize
            If Not Found()
                Dimension nIconColors[1]
                Do Case
                    Case m.nIcon=16
                        *Errore
                        nStdIconNumber = 32513
                    Case m.nIcon=32
                        *Domanda
                        nStdIconNumber = 32514
                    Case m.nIcon=48
                        *Esclamazione
                        nStdIconNumber = 32515
                    Case m.nIcon=64
                        *Esclamazione
                        nStdIconNumber = 32516
                    Otherwise
                        nStdIconNumber = 32512
                Endcase
                *m.hIco = LoadImage(0,l_cIconFullPath, 1, m.nDimension, m.nDimension, 16)
                m.hIco = LoadIcon(0, m.nStdIconNumber )
                m.cEmptyBmp= _Screen.CP_THEMESMANAGER.CreateBitmap(m.nStdSize,m.nStdSize,"FFFFFF")
                m.cOutBmp = Alltrim(Sys(2015))
                m.cOutBmp = _Screen.CP_THEMESMANAGER.ImgPath + Right(m.cOutBmp, Len(m.cOutBmp)-1)
                m.oImg=LoadPicture(m.cEmptyBmp)
                i_HDC = GetDC(0)
                i_HMemDC= CreateCompatibleDC(i_HDC)
                DeleteObject(SelectObject(m.i_HMemDC, oImg.handle))
                DrawIconEx(m.i_HMemDC , 0, 0,m.hIco,m.nStdSize,m.nStdSize,0,0,DI_NORMAL)
                DeleteDC(m.i_HMemDC)
                ReleaseDC(0, m.i_HDC)
                SavePicture(oImg, m.cOutBmp+".bmp" )
                Clear Resources m.cEmptyBmp
                Clear Resources m.cOutBmp+".bmp"
                oMsk=LoadPicture(cEmptyBmp)
                i_HDC = GetDC(0)
                i_HMskDC= CreateCompatibleDC(i_HDC)
                DeleteObject(SelectObject(i_HMskDC, oMsk.handle))
                DrawIconEx(m.i_HMskDC , 0, 0,m.hIco,m.nStdSize,m.nStdSize,0,0,DI_MASK)
                DeleteDC(m.i_HMskDC)
                ReleaseDC(0, m.i_HDC)
                SavePicture(oMsk,cOutBmp+".msk")
                Clear Resources m.cEmptyBmp
                Clear Resources m.cOutBmp+".msk"
                Delete File (cEmptyBmp)
                Insert Into (This.RBmpCurName) Values (m.nUsedTheme, m.nIcon*-1, Alltrim(Str(m.nIcon)), m.nStdSize, Alltrim(m.cOutBmp))
            Else
                *Bitmap gi� processata
                m.cOutBmp = Alltrim(OutImg)
            Endif
        Else
            m.cOutBmp = ""
        Endif
        Return m.cOutBmp
    Endproc

    Procedure RetBmpFromIco
        Lparameters cFileBmp, nDimension, nPropertyNum
        Local cFileIco, nOldArea, nUsedTheme
        m.cFileIco = ""
        m.nOldArea = Select()
        m.nDimension = Iif(Vartype(m.nDimension)='N',m.nDimension,24)
        If Lower(Justext(m.cFileBmp))=="bmp" Or Lower(Justext(m.cFileBmp))=="ico"
            m.cFileIco = cp_GetStdImg(Forceext(m.cFileBmp,''), 'ico')
        Endif
        If !Empty(m.cFileIco)
            m.nPropertyNum = Iif(Vartype(m.nPropertyNum)='N', m.nPropertyNum, 0)
            m.cFileBmp = _Screen.CP_THEMESMANAGER.GetBmpFromIco(Forceext(m.cFileIco, 'ico'), m.nDimension, .Null., .Null., .T.)
        Else
            m.cFileBmp = Forceext(m.cFileBmp,'')
        Endif
        Select (m.nOldArea)
        Return m.cFileBmp
    Endproc

    Procedure GetBmpFromRegistryIco
        Lparameters cFileName, nPropertyNum
        Local oRetRegVal, cRegStrval, cResult, cKeyVal, nSize, cData, nOldArea, nUsedTheme, cFileIconName, nIconIndex, hSmallICO, hBigICO, cEmptyBmpm, oImg, hIco, pX, pY, i_HDC, i_HMemDC, i_HSel, b_HDC, b_HMemDC, b_HSel, oImg, oMsk, cEmptyBmp, cOutBmp
        Dimension nIconColors[1]
        m.cResult = ""
        m.cRegStrval = 0
        m.cFileIconName = ""
        m.nIconIndex = -1
        m.nPropertyNum = Iif(Vartype(m.nPropertyNum)='N', m.nPropertyNum, 0)
        m.nOldArea = Select()
        Select (This.RBmpCurName)
        Go Top
        &&This.ActiveTheme
        Locate For ThemeNo=0 And Alltrim(OrigImg)==Alltrim(Justext(m.cFileName)) And OrigDim=1 And PropertyNo=m.nPropertyNum
        m.nUsedTheme=0
        If Not Found()
            If RegOpenKeyEx(HKEY_CLASSES_ROOT, "."+Alltrim(Justext(m.cFileName)), ;
                    0, KEY_READ, @m.cRegStrval) = ERROR_SUCCESS
                m.nSize = 250
                m.cData = Space(m.nSize)
                m.cResult = ""
                If RegQueryValueEx(m.cRegStrval, "", 0,0, @m.cData, @m.nSize) = ERROR_SUCCESS
                    m.cResult = Left(m.cData, m.nSize-1)
                Endif
                If !Empty(m.cResult)
                    m.cKeyVal = 0
                    If RegOpenKeyEx(HKEY_CLASSES_ROOT, "\"+Alltrim(m.cResult)+"\DefaultIcon", ;
                            0, KEY_READ, @m.cKeyVal) = ERROR_SUCCESS
                        m.nSize = 250
                        m.cData = Space(m.nSize)
                        m.cResult = ""
                        If RegQueryValueEx(m.cKeyVal, "", 0,0, @m.cData, @m.nSize) = ERROR_SUCCESS
                            m.cResult = Left(m.cData, m.nSize-1)
                        Endif
                    Else
                        m.cResult = ""
                    Endif
                    RegCloseKey (m.cKeyVal)
                    If !Empty(m.cResult)
                        m.cResult = Alltrim(m.cResult)
                        m.cFileIconName = Left(m.cResult, At(",",m.cResult) - 1)
                        m.nIconIndex = Int(Val(Right(m.cResult, Len(m.cResult) - At(",",m.cResult) )))
                    Endif
                    If !Empty(m.cFileIconName)
                        *Estrazione icone da file rilevato nel registry
                        m.hBigICO = 0
                        m.hSmallICO = 0
                        m.hSmallICO=ExtractIcon(_Screen.HWnd, m.cFileIconName,m.nIconIndex - Iif(m.nIconIndex<0, 0, 1))
                        m.cResult = "OK"
                    Else
                        m.cResult = ""
                    Endif
                Endif
            Endif
            RegCloseKey (m.cRegStrval)
            If Empty(m.cResult)
                *Estrazione icona associata al file
                m.hSmallICO = 0
                m.hSmallICO=ExtractAssociatedIcon( 0 , m.cFileName , 2)
                If m.hSmallICO<>0
                    m.cResult = "OK"
                Endif
            Endif

            If !Empty(m.cResult)
                m.cEmptyBmp= _Screen.CP_THEMESMANAGER.CreateBitmap(16,16,"FFFFFF")
                cOutBmp = Alltrim(Sys(2015))
                cOutBmp = _Screen.CP_THEMESMANAGER.ImgPath + Right(cOutBmp, Len(cOutBmp)-1)
                m.oImg=LoadPicture(m.cEmptyBmp)
                i_HDC = GetDC(0)
                i_HMemDC= CreateCompatibleDC(i_HDC)
                DeleteObject(SelectObject(i_HMemDC, oImg.handle))
                DrawIconEx(m.i_HMemDC , 0, 0,m.hSmallICO,16,16,0,0,DI_NORMAL)
                DeleteDC(m.i_HMemDC)
                ReleaseDC(0, m.i_HDC)
                SavePicture(oImg, m.cOutBmp+".bmp" )
                Clear Resources m.cEmptyBmp
                Clear Resources m.cOutBmp+".bmp"
                oMsk=LoadPicture(cEmptyBmp)
                i_HDC = GetDC(0)
                i_HMskDC= CreateCompatibleDC(i_HDC)
                DeleteObject(SelectObject(i_HMskDC, oMsk.handle))
                DrawIconEx(m.i_HMskDC , 0, 0,m.hSmallICO,16,16,0,0,DI_MASK)
                DeleteDC(m.i_HMskDC)
                ReleaseDC(0, m.i_HDC)
                SavePicture(oMsk,cOutBmp+".msk")
                Clear Resources m.cEmptyBmp
                Delete File (cEmptyBmp)
                m.cResult = m.cOutBmp
                *La dimensione viene passata simbolicamente a 1 poich� il nome del file varia e salvando la sola estensione
                *la dimensione dell'icona non � presente
                Insert Into (This.RBmpCurName) Values (m.nUsedTheme, m.nPropertyNum, Alltrim(Justext(m.cFileName)), 1, Alltrim(m.cOutBmp))
            Endif
        Else
            *Bitmap gi� processata
            m.cResult = Alltrim(OutImg)
        Endif
        Select (m.nOldArea)
        Return m.cResult
    Endproc

    Procedure OverlayBitmap
        Lparameters nDimension, cBackBmp, cMaskBmp, nForecolor
        Local h_Bmp, oImg, h_CBmp, i_bmp, h_sel, Rect, vFillColor
        Local i_HDC, i_HMemDC, i_HSel
        Local oBack, h_BckHDC, h_HMemBckDC, h_BckSel, MskName
        m.h_CBmp= CreateCompatibleBitmap(GetDC(0), m.nDimension, m.nDimension)
        m.i_bmp = GetDC(0)
        m.h_Bmp= CreateCompatibleDC(m.i_bmp)
        DeleteObject(SelectObject(m.h_Bmp, m.h_CBmp))
        m.Rect = This.NumToDWORD(0)+This.NumToDWORD(0)+This.NumToDWORD(m.nDimension)+This.NumToDWORD(m.nDimension)
        m.vFillColor = CreateSolidBrush( m.nForecolor )
        m.L_Res=FillRect(m.h_Bmp, @m.Rect, m.vFillColor)
        DeleteObject(m.vFillColor)
        m.oImg=LoadPicture(m.cMaskBmp)
        m.i_HDC = GetDC(0)
        m.i_HMemDC= CreateCompatibleDC(i_HDC)
        DeleteObject(SelectObject(i_HMemDC, m.oImg.handle))
        If Vartype(m.cBackBmp)='C' And !Empty(m.cBackBmp)
            *L'immagine ha un gradiente di sfondo lo devo
            *incollare sotto la maschera colorata
            m.oBack=LoadPicture(m.cBackBmp)
            m.h_BckHDC=GetDC(0)
            m.h_HMemBckDC=CreateCompatibleDC(m.h_BckHDC)
            DeleteObject(SelectObject(m.h_HMemBckDC, m.oBack.handle))

            m.h_CBmpMask= CreateCompatibleBitmap(GetDC(0), m.nDimension, m.nDimension)
            m.i_bmpMask = GetDC(0)
            m.h_bmpMask= CreateCompatibleDC(m.i_bmpMask)
            DeleteObject(SelectObject(m.h_bmpMask, m.h_CBmpMask))
            BitBlt(m.h_bmpMask, 0, 0,m.nDimension,m.nDimension, m.h_HMemBckDC, 0, 0, SRCCOPY)
            BitBlt(m.h_bmpMask, 0, 0,m.nDimension,m.nDimension, m.i_HMemDC, 0, 0, MERGEPAINT)

            m.L_Res=BitBlt(m.i_HMemDC, 0,0,m.nDimension,m.nDimension, m.h_Bmp, 0,0, NOTSRCERASE)
            m.L_Res=BitBlt(m.i_HMemDC, 0,0,m.nDimension,m.nDimension, m.i_HMemDC, 0,0, DSTINVERT)

            BitBlt(m.i_HMemDC, 0, 0, m.nDimension,m.nDimension, m.h_bmpMask, 0, 0, SRCAND)
            m.h_BckSel = .Null.
            DeleteDC(m.h_HMemBckDC)
            ReleaseDC(0, m.h_BckHDC)
            m.oBack=.Null.
            *Elimino la maschera
            m.MskName = Forceext(m.cMaskBmp, 'msk')
            Delete File (m.MskName)
        Else
            m.L_Res=BitBlt(m.i_HMemDC, 0,0,m.nDimension,m.nDimension, m.h_Bmp, 0,0, NOTSRCERASE)
            m.L_Res=BitBlt(m.i_HMemDC, 0,0,m.nDimension,m.nDimension, m.i_HMemDC, 0,0, DSTINVERT)
        Endif
        m.h_CBmp=.Null.
        m.h_sel=.Null.
        m.i_HSel=.Null.
        DeleteDC(m.h_Bmp)
        ReleaseDC(0, m.i_bmp)
        DeleteDC(m.i_HMemDC)
        ReleaseDC(0, m.i_HDC)
        SavePicture(m.oImg, m.cMaskBmp)
        m.oImg=.Null.
        Return m.cMaskBmp
    Endproc

    *-- Internal to the class. Occurs when ThemeNumber property is changed.
    Hidden Procedure ThemeNumber_assign
        Lparameters vNewVal
        *--- Se tema std (-1) non faccio nulla
        If m.vNewVal = -1
            This.ThemeName='Standard'
            This.ThemeNumber=7
            Return
        Endif
        This.ThemeNumber = m.vNewVal
        If This.ActiveTheme <> m.vNewVal
            This.ActiveTheme = m.vNewVal
            This.ThemeName=''
			This.ChangeTheme()		
        Endif
		This.CreateTabPage()
    Endproc

    Hidden Procedure osname_assign
        Lparameters vNewVal
        This.OSname = Iif("5.01" $ Os(1),"XP","")
    Endproc


    Hidden Procedure osname_access
        Return Iif("5.01" $ Os(1),"XP","")
    Endproc


    Hidden Procedure Destroy
        With This
            Local cMacro, bResult
            cMacro = This.cCBMName
            &cMacro = .Null.
            *--- Rimuovo hook
            If i_VisualTheme <> -1
                cb_UnHookMainWnd(_vfp.HWnd)
            Endif
            *--- Rilascio la libreria Interfaccia
            *Release Library "..\VFCSIM\cp_DockWnd.fll"
            If .NumChild<=0
                .IsThemeChangedBound = .F.
                m.bResult = .T.
                If Used( .PropCurName )
                    m.bResult = .Remove_Graphics()
                Endif
                If m.bResult And Used( .RBmpCurName)
                    .Remove_Bitmaps()
                Endif
                Clear Resources
                .Remove_Graphics_API()
            Else
                cp_ErrorMsg(cp_MsgFormat("Il Themes Manager � ancora utilizzato da %1 oggetti. Impossibile distruggerlo",Alltrim(Str(.NumChild))),16,'',.f.)
            Endif
        Endwith
    Endproc

    Hidden Procedure Remove_Bitmaps()
        Local PictName
        If Used(This.RBmpCurName)
            Select (This.RBmpCurName)
            Go Top
            Scan
                m.PictName = Alltrim(OutImg)
                If cp_fileexist(m.PictName + ".bmp")
                    Delete File (m.PictName + ".bmp")
                Endif
                If cp_fileexist(m.PictName + ".msk")
                    Delete File (m.PictName + ".msk")
                Endif
            Endscan
            Use In (This.RBmpCurName)
        Endif
        Delete File (This.ImgPath + This.RBmpCurName+".dbf")
        If cp_fileexist(This.ImgPath + This.RBmpCurName+".cdx")
            Delete File (This.ImgPath + This.RBmpCurName+".cdx")
        Endif
    Endproc

    Procedure Remove_Graphics
        Lparameters bOnInit, bForceRemove
        Local PictName, MskName, nApps
        If Used(This.PropCurName)
            Select (This.PropCurName)
            If !m.bOnInit
                Update (This.PropCurName) Set PropertyTy = PropertyTy - 1 Where ThemeNo=0 And PropertyNo=0
            Endif
            Count For PropertyTy<=1 And ThemeNo=0 And  PropertyNo=0 To m.nApps
            If m.nApps>0 OR bForceRemove
                If !cp_fileexist(Addbs(Alltrim(This.ImgPath)) + "bDelGraphics.fla")
                    Return .F.
                Endif
                Go Top
                Scan For !Empty(Nvl(PicPath, ' '))
                    m.PictName= PicPath
                    m.MskName = Forceext(m.PictName, 'msk')
                    If cp_fileexist(m.PictName)
                        Delete File (m.PictName)
                    Endif
                    If cp_fileexist(m.MskName)
                        Delete File (m.MskName)
                    Endif
                Endscan
                Use In (This.PropCurName)
                Delete File (This.ImgPath + This.PropCurName+".dbf")
                If cp_fileexist(This.ImgPath + This.PropCurName+".cdx")
                    Delete File (This.ImgPath + This.PropCurName+".cdx")
                Endif
                If cp_fileexist(Addbs(Alltrim(This.ImgPath)) + "bDelGraphics.fla")
                    Delete File (Addbs(Alltrim(This.ImgPath)) + "bDelGraphics.fla")
                Endif
                Return .T.
            Else
                Return .F.
            Endif
        Endif
    Endproc

    *-- Cambio tema.
    Procedure ChangeTheme
	Endproc


    Procedure CreateBitmap
        Lparameters DimImgX, DimImgY, cMskColor
        Local SizeImg, ImgDataSize, StrOut, nPixel, cOutImg, extrabytes
        m.SizeImg = (108 + (6*m.DimImgX*m.DimImgY))/2
        m.SizeImg = m.SizeImg + ( 4 - ( ( 3 * m.SizeImg ) % 4 ) ) % 4
        m.ImgDataSize = m.SizeImg - 54
        m.extrabytes = (4 - (m.DimImgX * 3) % 4) % 4
        * File BMP Header
        * Signature, must be 4D42 hex
        * size of BMP file in bytes (unreliable)
        * reserved, must be zero
        * reserved, must be zero
        * offset to start of image data in bytes
        * size of BITMAPINFOHEADER structure, must be 40
        * image width in pixels
        * image height in pixels
        * number of planes in the image, must be 1
        * number of bits per pixel (1, 4, 8, or 24)
        * compression type (0=none, 1=RLE-8, 2=RLE-4)
        * size of image data in bytes (including padding)
        * horizontal resolution in pixels per meter (unreliable)
        * vertical resolution in pixels per meter (unreliable)
        * number of colors in image, or zero
        * number of important colors, or zero
        *Scrittura pixel immagine
        *m.StrOut="424D" + this.HexStr(m.SizeImg) + "0000" + "0000" + "36000000"	+ "28000000" + this.HexStr(m.DimImgX) + this.HexStr(m.DimImgY) + "0100" + "1800" + "00000000" + this.HexStr(m.ImgDataSize) + "120B0000" + "120B0000" + "00000000" + "00000000" + REPLICATE(REPLICATE(m.cMskColor, DimImgX)+REPLICATE("00", m.extrabytes), m.DimImgY)
        m.StrOut="424D" + This.HexStr(m.SizeImg) + "000000003600000028000000" + This.HexStr(m.DimImgX) + This.HexStr(m.DimImgY) + "0100180000000000" + This.HexStr(m.ImgDataSize) + "120B0000120B00000000000000000000" + Replicate(Replicate(m.cMskColor, DimImgX)+Replicate("00", m.extrabytes), m.DimImgY)
        *Scrittura immagine su disco
        cOutImg =  Alltrim(Sys(2015))
        cOutImg = _Screen.CP_THEMESMANAGER.ImgPath + Right(m.cOutImg, Len(m.cOutImg)-1)+".bmp"
        #If Version(5)>=900
            If Strtofile(Strconv(m.StrOut,16), m.cOutImg)>0
                Return m.cOutImg
            Else
                Return ""
            Endif
        #Else
            If Cp_HexToFile(m.StrOut, m.cOutImg)>0
                Return m.cOutImg
            Else
                Return ""
            Endif

        #Endif
    Endproc

    *Trasforma un valore decimale in stringa esadecimale di 8 valori
    Function HexStr
        Lparameters nDecim
        HStr= Right(Transform(nDecim,"@0"),8)
        Return Right(HStr,2)+Substr(HStr,5,2)+Substr(HStr,3,2)+Left(HStr,2)
    Endfunc

    Procedure VGradient
        Lparameters nDimX, nDimY, nBeginColor, nEndColor
        Return This.GradientFill(0,0,m.nDimX, m.nDimY, m.nDimX, m.nDimY ,m.nBeginColor, m.nEndColor,'',.T.)
    Endproc

    Procedure HGradient
        Lparameters nDimX, nDimY, nBeginColor, nEndColor
        Return This.GradientFill(0,0,m.nDimX, m.nDimY, m.nDimX, m.nDimY, m.nBeginColor, m.nEndColor,'',.F.)
    Endproc

    Procedure DoubleVGradient
        Lparameters nDimX, nDimY, nFirstBeginColor, nFirstEndColor, nSplitPerc, nCenterSpace, nSecondBeginColor, nSecondEndColor
        Return This.DoubleGradient(m.nDimX, m.nDimY, m.nFirstBeginColor, m.nFirstEndColor, m.nSplitPerc, m.nCenterSpace, m.nSecondBeginColor, m.nSecondEndColor, .T.)
    Endproc

    Procedure DoubleHGradient
        Lparameters nDimX, nDimY, nFirstBeginColor, nFirstEndColor, nSplitPerc, nCenterSpace, nSecondBeginColor, nSecondEndColor
        Return This.DoubleGradient(m.nDimX, m.nDimY, m.nFirstBeginColor, m.nFirstEndColor, m.nSplitPerc, m.nCenterSpace, m.nSecondBeginColor, m.nSecondEndColor, .F.)
    Endproc

    Procedure DoubleGradient
        Lparameters nDimX, nDimY, nFirstBeginColor, nFirstEndColor, nSplitPerc, nCenterSpace, nSecondBeginColor, nSecondEndColor, bVertical
        Local TmpBmp, OutBmp, nNewStartX, nNewStartY
        If m.bVertical
            m.nNewStartY = Round((m.nDimY * m.nSplitPerc) / 100,0) - m.nCenterSpace
            m.nNewStartX = m.nDimX
        Else
            m.nNewStartY = m.nDimY
            m.nNewStartX = Round((m.nDimX * m.nSplitPerc) / 100,0) - m.nCenterSpace
        Endif
        m.TmpBmp = This.GradientFill(0,0, m.nNewStartX , m.nNewStartY, m.nDimX, m.nDimY,m.nFirstBeginColor, m.nFirstEndColor,'',m.bVertical)
        If m.bVertical
            m.nNewStartX = 0
            m.nNewStartY = m.nNewStartY + m.nCenterSpace*2
        Else
            m.nNewStartX = m.nNewStartX + m.nCenterSpace*2
            m.nNewStartY = 0
        Endif
        m.OutBmp = This.GradientFill(m.nNewStartX ,m.nNewStartY , m.nDimX, m.nDimY, m.nDimX, m.nDimY,m.nSecondBeginColor, m.nSecondEndColor, m.TmpBmp ,m.bVertical)
        Delete File (m.TmpBmp)
        Return m.OutBmp
    Endproc

    Procedure GradientFill
        Lparameters nStartX, nStartY, nEndX, nEndY, nDimX, nDimY, nBeginColor, nEndColor, cEmptyBmp, bVertical
        #Define GetRColor Bitand(255,Bitrshift(nColorAtt,0))
        #Define GetGColor Bitand(255,Bitrshift(nColorAtt,8))
        #Define GetBColor Bitand(255,Bitrshift(nColorAtt,16))
        Local nStartRed, nStartGreen, nStartBlue, nEndRed, nEndGreen, nEndBlue, nCurrRed, nCurrGreen, nCurrBlue, nColorAtt, i_HDC, i_HMemDC, h_Bmp, vFillColor, nDelta, nLenght, cCurrentRect, bDelBmp, cHeadRect, cFootRect
        nColorAtt = nBeginColor
        nStartRed = GetRColor
        nStartGreen = GetGColor
        nStartBlue = GetBColor
        nColorAtt = nEndColor
        nEndRed = GetRColor
        nEndGreen = GetGColor
        nEndBlue = GetBColor
        bDelBmp = .F.
        If Vartype(cEmptyBmp)<>'C'
            cEmptyBmp=""
        Endif
        If Empty(cEmptyBmp)
            bDelBmp = .T.
            cEmptyBmp= This.CreateBitmap(nDimX,nDimY,Right(Transform(nEndColor,"@0"),6))
        Endif
        oImg=LoadPicture(cEmptyBmp)
        i_HDC = GetDC(0)
        i_HMemDC= CreateCompatibleDC(i_HDC)
        DeleteObject(SelectObject(i_HMemDC, oImg.handle))
        nDelta = 1
        nLenght = Iif(bVertical , nEndY-nStartY, nEndX-nStartX) - nDelta
        If m.bVertical
            m.cHeadRect = This.NumToDWORD(nStartX)
            m.cFootRect = This.NumToDWORD(nStartX+nEndX) + This.NumToDWORD(nStartY+nEndY)
        Else
            m.cFootRect = This.NumToDWORD(nStartY) + This.NumToDWORD(nStartX+nEndX) + ;
                this.NumToDWORD(nStartY+nEndY)
        Endif
        For nDN=0 To nLenght Step nDelta
            nCurrRed = MulDiv(nEndRed - nStartRed, nDN, nLenght) + nStartRed
            nCurrGreen = MulDiv(nEndGreen - nStartGreen, nDN, nLenght) + nStartGreen
            nCurrBlue = MulDiv(nEndBlue - nStartBlue, nDN, nLenght) + nStartBlue
            If bVertical
                cCurrentRect = m.cHeadRect + This.NumToDWORD(nStartY+nDN) + m.cFootRect
            Else
                cCurrentRect = This.NumToDWORD(nStartX+nDN) + m.cFootRect
            Endif
            vFillColor = CreateSolidBrush( Rgb(nCurrRed, nCurrGreen, nCurrBlue) )
            FillRect(m.i_HMemDC, @cCurrentRect, vFillColor)
            DeleteObject(vFillColor)
        Endfor
        DeleteDC(m.i_HMemDC)
        ReleaseDC(0, m.i_HDC)
        cOutBmp = Alltrim(Sys(2015))
        cOutBmp = _Screen.CP_THEMESMANAGER.ImgPath + Right(cOutBmp, Len(cOutBmp)-1)
        SavePicture(oImg,cOutBmp+".bmp")
        If bDelBmp
            Delete File (cEmptyBmp)
        Endif
        Return cOutBmp+".bmp"
    Endproc

    Function  NumToDWORD (lnValue)
        #Define m0       256
        #Define m1     65536
        #Define m2  16777216
        Return Chr(Mod(m.lnValue, m0))+Chr(Bitrshift(m.lnValue - Bitrshift(m.lnValue,24) * m2 - Bitrshift(m.lnValue - Bitrshift(m.lnValue,24) * m2,16)*m1,8))+Chr(Bitrshift(m.lnValue - Bitrshift(m.lnValue,24) * m2,16))+Chr(Bitrshift(m.lnValue,24))
    Endfunc

    Procedure CreateTabPage()
        Local nOldArea, nUsedTheme
        *--- BckGrd Item Selected
        If Vartype(This.RBmpCurName)<>'C'
            Return
        Endif
        If Not Empty(This.TabMenuItemSel)
            Clear Resource (This.TabMenuItemSel)
            Clear Resource (This.TabMenuItemSelLeft)
            Clear Resource (This.TabMenuItemSelRight)
        Endif
        m.nOldArea = Select()
        Select (This.RBmpCurName)
        Go Top
        Locate For ThemeNo=This.ActiveTheme And Alltrim(OrigImg)=="TABMENUITEM_SEL.bmp" And OrigDim=26
        m.nUsedTheme=This.ActiveTheme
        If Not Found() And This.ActiveTheme>This.NumThemes
            Go Top
            Locate For ThemeNo=This.DefaultStdTheme And Alltrim(OrigImg)=="TABMENUITEM_SEL.bmp" And OrigDim=26
            m.nUsedTheme=This.DefaultStdTheme
        Endif
        If Not Found()
            This.TabMenuItemSel= Addbs(This.ImgPath)+ Sys(2015)
            This.GetThemePicture("TabMenuItem_Sel.BMP", This.TabMenuItemSel, 2, 26, This.GetProp(2))
            This.GetThemePicture(Forceext(This.TabMenuItemSel , "bmp"), This.TabMenuItemSel, 2, 26, This.GetSingleProp(11,2), Rgb(255,0,255))
            Insert Into (This.RBmpCurName) Values (m.nUsedTheme, 0, "TABMENUITEM_SEL.bmp", 26, This.TabMenuItemSel)
        Else
            This.TabMenuItemSel=Alltrim(OutImg)
        Endif
        This.TabMenuItemSel = Forceext(This.TabMenuItemSel , "bmp")

        *--- Img Left Item Selected
        Select (This.RBmpCurName)
        Go Top
        Locate For ThemeNo=This.ActiveTheme And Alltrim(OrigImg)=="TABMENUITEM_SEL_L.bmp" And OrigDim=26
        m.nUsedTheme=This.ActiveTheme
        If Not Found() And This.ActiveTheme>This.NumThemes
            Go Top
            Locate For ThemeNo=This.DefaultStdTheme And Alltrim(OrigImg)=="TABMENUITEM_SEL_L.bmp" And OrigDim=26
            m.nUsedTheme=This.DefaultStdTheme
        Endif
        If Not Found()
            This.TabMenuItemSelLeft= Addbs(This.ImgPath)+ Sys(2015)
            This.GetThemePicture(This.GetProp(106), This.TabMenuItemSelLeft, 2, 26, This.GetProp(2))
            This.GetThemePicture(Forceext(This.TabMenuItemSelLeft , "bmp"), This.TabMenuItemSelLeft, 2, 26, This.GetSingleProp(11,2), Rgb(255,0,255))
            Insert Into (This.RBmpCurName) Values (m.nUsedTheme, 0, "TABMENUITEM_SEL_L.bmp", 26, This.TabMenuItemSelLeft)
        Else
            This.TabMenuItemSelLeft=Alltrim(OutImg)
        Endif

        This.TabMenuItemSelLeft = Forceext(This.TabMenuItemSelLeft , "bmp")

        *--- Img Right Item Selected

        Select (This.RBmpCurName)
        Go Top
        Locate For ThemeNo=This.ActiveTheme And Alltrim(OrigImg)=="TABMENUITEM_SEL_R.bmp" And OrigDim=26
        m.nUsedTheme=This.ActiveTheme
        If Not Found()  And This.ActiveTheme>This.NumThemes
            Go Top
            Locate For ThemeNo=This.DefaultStdTheme And Alltrim(OrigImg)=="TABMENUITEM_SEL_R.bmp" And OrigDim=26
            m.nUsedTheme=This.DefaultStdTheme
        Endif
        If Not Found()
            This.TabMenuItemSelRight= Addbs(This.ImgPath)+ Sys(2015)
            This.GetThemePicture(This.GetProp(107), This.TabMenuItemSelRight, 2, 26, This.GetProp(2))
            This.GetThemePicture(Forceext(This.TabMenuItemSelRight , "bmp"), This.TabMenuItemSelRight, 2, 26, This.GetSingleProp(11,2), Rgb(255,0,255))
            Insert Into (This.RBmpCurName) Values (m.nUsedTheme, 0, "TABMENUITEM_SEL_R.bmp", 26, This.TabMenuItemSelRight)
        Else
            This.TabMenuItemSelRight=Alltrim(OutImg)
        Endif
        This.TabMenuItemSelRight = Forceext(This.TabMenuItemSelRight , "bmp")
        Select (m.nOldArea)
    Endproc

    *--- Cambia un colore specifico di un bmp
    Procedure GetThemePicture(pSourceFile, pFile, pWidth, pHeight, pNewColor, pOldColor)
        Local oBmp, l_OldColor
        l_OldColor = Iif(Vartype(m.pOldColor)='L', Rgb(0,0,0), m.pOldColor)
        If File(m.pSourceFile) .And. m.pNewColor<>l_OldColor
            oBmp = LoadPicture(m.pSourceFile)

            *!*			     DECLARE INTEGER GetDC IN Win32API INTEGER
            *!*			     DECLARE INTEGER ReleaseDC IN Win32API ;
            *!*			     	INTEGER, INTEGER
            *!*			     DECLARE INTEGER GetPixel IN Win32API ;
            *!*			     	INTEGER, INTEGER, INTEGER
            *!*			     DECLARE INTEGER SetPixel IN Win32API ;
            *!*			     	INTEGER, INTEGER, INTEGER, INTEGER
            *!*			     DECLARE INTEGER CreateCompatibleDC IN Win32API ;
            *!*			     	INTEGER
            *!*			     DECLARE INTEGER DeleteDC IN Win32API ;
            *!*			     	INTEGER
            *!*			     DECLARE INTEGER SelectObject IN Win32API ;
            *!*			     	INTEGER, INTEGER

            Local l_X, l_Y, l_HDC, l_HMemDC, l_HSel
            l_HDC = GetDC(0)
            l_HMemDC= CreateCompatibleDC(l_HDC)
            l_HSel= SelectObject(l_HMemDC, oBmp.handle)
            For l_Y= 0 To m.pHeight - 1
                For l_X= 0 To m.pWidth - 1
                    If GetPixel(m.l_HMemDC, m.l_X, m.l_Y) = m.l_OldColor
                        SetPixel(m.l_HMemDC, m.l_X, m.l_Y, m.pNewColor)
                    Endif
                Endfor
            Endfor
            SelectObject(m.l_HMemDC, m.l_HSel)
            DeleteDC(m.l_HMemDC)
            ReleaseDC(0, m.l_HDC)
            SavePicture(m.oBmp, m.pFile)
        Endif
    Endproc


	FUNCTION ReadXMLTheme
	  LPARAMETERS pcXMLFile
	  LOCAL oXML, oRootNode, cParentName, nType, nNumNodes, ;
	    cRootTagName, oNodeList, oNode, bHasChild, cTagName, ;
	    oChildNodeList, nChildLen, nPass, oChildNode, ;
	    cTextData, nTextDataLen, cAttrName, cAttrValue, nNumAttr
	  LOCAL oFatherNodeList, oFatherNode, nFatherLen, nFatherPass

	  LOCAL nRecord, nIdTheme, bInsert
	  nRecord = 0
	  nIdTheme = 0 &&Numero del tema
	  bInsert = .T. &&.T. se il tema non � presente nella tabella 
	  
	  * Start out by creating the actual xml parser object
	  oXML = CREATEOBJECT('MSXML2.DomDocument')

	  * Wait for the document to be parsed and loaded
	  oXML.ASYNC = .F.

	  * Load the document into the object, if this was a stream instead
	  * of a file name, we would use loadXML(cCharStream)
	  
	  oXML.LOAD(pcXMLFile)

	  IF Type("oXML.documentElement")='O' AND Not IsNull(oXML.documentElement)

		  * Get the root element
		  oRootNode = oXML.documentElement

		  * What is the root tag name?
		  cRootTagName = oRootNode.tagName

		  * Get all the nodes in the document with the special '*'
		  * parameter, we could just pass in a tag name to get the
		  * node list for that specific tag
		  
		  *-- Eseguo la lettura dei nodi sotto la tag Properties
		  oNodeList = oRootNode.getElementsByTagName("Properties")

		  * How many nodes did we retrieve
		  nNumNodes = oNodeList.LENGTH

		  * Go through all the nodes in the NodeList.
		  * Note that Attribute and Character/Text Data is NOT
		  * counted as part of this list, you must get that data
		  * separately, this list only contains tag elements
		  * Note that this uses C like array positioning by
		  * starting at zero
		  FOR nPos = 0 TO (nNumNodes-1) STEP 1
		    * Get the next node in the list
		    oNode = oNodeList.ITEM(nPos)

		    * What is the value of this node, if it is an element
		    * then this value is the tag name
		    cParentName = oNode.nodeName

		    * Does this node have any children?
		    bHasChild = oNode.hasChildNodes()

		    * What is the node type, element or text?
		    nType = oNode.nodeType
		    
		    IF bHasChild
		      * Ok, we know we have children but what type are they?
		      * Just test the first one to see if it is something
		      * other than an element and if so, get it
		      
		      oPropNodeList = oNode.childNodes
		      nPropLen = oPropNodeList.LENGTH
		      FOR nPropPass = 0 TO (nPropLen-1) STEP 1
			      oFatherNode = oPropNodeList.ITEM(nPropPass)
			      
		          * Get the node list and determine how man child
		          * nodes this element has
			      oFatherNodeList = oFatherNode.childNodes
			      nFatherLen = oFatherNodeList.LENGTH

			      * Go through all child nodes and grab the non-element
			      * data to do with what you like
			      FOR nFatherPass = 0 TO (nFatherLen-1) STEP 1
			      	
			      	oChildNode =oFatherNodeList.ITEM(nFatherPass)
			      	oChildNodeList = oChildNode.childNodes
			      	nChildLen = oChildNodeList.LENGTH
			      	
			      	  FOR nPass = 0 TO (nChildLen-1) STEP 1
		        
				          * We know we have something other than an element, get
				          * the tag name of the element we are parsing
				          *-- cTagName � il nome del campo del cursore da aggiornare
				          cTagName = oChildNode.tagName
				          oFinalNode = oChildNodeList.ITEM(nPass)
				          nType = oFinalNode.nodeType
				          bHasChild = oFinalNode.hasChildNodes()
			             
			             *-- Valore da inserire nel cursore
			              cTextData = oFinalNode.DATA
			              nTextDataLen = oFinalNode.LENGTH
						  IF Upper(cTagName)=="THEMENO"
						  		*-- Verifico se il tema � gi� presente nel cursore
						  		nIdTheme = Int(Val(cTextData))
						  		cCursor = SYS(2015)
						  		Select Distinct ThemeNo From (This.PropCurName) Where ThemeNo=nIdTheme Into Cursor (cCursor)
						  		IF Reccount()>0
						  			Use In Select(cCursor)
						  			Return 0 &&bInsert=.F.
						  		ENDIF
						  		Use In Select(cCursor)
						  ELSE
				          		IF Upper(cTagName)=="PROPERTYNO"
				          			*-- Inserimento chiave
				          			nRecord = Int(Val(cTextData))
				          			*IF bInsert
					          			Insert Into (This.PropCurName) Values (nIdTheme,nRecord,2,-1,-1,-1,-1,-1,-1,-1,-1,-1,"",-1,"")
				          			*ENDIF
				          		ELSE
				          			*-- Inserimento dato
				          			If nRecord=0 And Upper(cTagName)="MASKBMP"
				          				*-- Il Record 0 contiene il nome della cartella, con la IsDigit sarebbe valutato come numero
				          				InsData = cTextData
				          			Else
				          	  			InsData = IIF(IsDigit(cTextData), Int(Val(cTextData)),IIF(Upper(Left(cTextData,3))="RGB",Evaluate(cTextData),cTextData))
				          	  		Endif
				             		Update (This.PropCurName) Set &cTagName=InsData Where ThemeNo=nIdTheme And PropertyNo=nRecord
				          	    ENDIF
			          	    ENDIF
		          	    ENDFOR &&nPass = 0 TO (nPropLen-1) STEP 1
			        ENDFOR &&nChildPass = 0 TO (nChildLen-1) STEP 1
		        ENDFOR &&nFatherPass = 0 TO (nFatherLen-1) STEP 1
		    ENDIF &&bHasChild
		  ENDFOR &&nPos = 0 TO (nNumNodes-1) STEP 1
		  RETURN 0
	  ELSE
		  RETURN -1
	  ENDIF
	  oXML = .Null.
	ENDFUNC

Enddefine

* Classe che simula se non VFP9.0 la classe Collection
Define Class Cp_Collection As Custom

    Count=0 && Numero di item
    Dimension Item[1] && item
    KeySort=0 && ordinamento dell'array...  0  Index ascending (default) ,1 Index descending , 2  Key ascending ,3  Key descending

    Proc Add(pToolbar, cCBName)
        * --- Aggiunge un item all'array..
        This.Count=This.Count+1
        Dimension This.Item[this.count]
        This.Item[this.count]=cCBName
        * --- Se KeySort<>0 allora ordino...
        Do Case
            Case This.KeySort=1
                Asort( This.Item )
            Case This.KeySort=2
                Asort(This.Item,1,-1,1)
            Case This.KeySort=3
                Asort( This.Item,2 )
            Case This.KeySort=3
                Asort(This.Item,2,-1,1)
            Otherwise
                * --- nessuna operazione
        Endcase
    Endproc

    Function GetKey(cCBName)
        * --- ritorna l'indice dell'Item ricercato..
        Local n_i, nIdx
        nIdx=0
        * -- Se il parametro � char cerco la chiave e restituisco l'indic,  se numerico restituisco la chiave
        If Vartype(m.cCBName)='C'
            For n_i=1 To This.Count
                If This.Item[n_i]=m.cCBName
                    nIdx=n_i
                    Exit
                Endif
            Next
            Return(nIdx)
        Else
            Return(This.Item[m.cCBName])
        Endif
    Endfunc

    Proc Remove(cCBName)
        * --- Rimuove un item
        * --- ricerco l'item...
        Local nIdx, n_i,n_j
        nIdx=This.GetKey(m.cCBName)
        If nIdx>0
            n_j=0
            This.Count=This.Count-1
            * --- Sposto gli elementi a partire
            * --- da quello da eliminare
            For n_i=1 To Count
                If n_i<>nIdx
                    n_j=n_j+1
                    This.Item[n_j]=This.Item[n_i]
                Endif
            Next
        Endif
        * --- l'ultimo fatalmente � di troppo
        This.Count=This.Count-1
        Dimension This.Item[this.count]
    Endproc

    Proc Destroy()
        Local n_i
        For n_i=1 To This.Count
            This.Item[this.Count]=.Null.
        Next
        This.Count=1
    Endproc

Enddefine

* Fine classe cp_Themesmanager
*******************************************************************************


* --- Riceve una stringa che contiene le cifre esadecimali, la salva su file in formato esadecimale (x bitmap)
Function Cp_HexToFile(  sInput ,cNomeFIle)
    * Scorro carattere per carattere la stringa...
    Local i, sTmp, sOutput, cFirst,cSecond, ntmp
    sOutput=""
    * Valuto la stringa a coppie di caratteri
    For i=1 To Len(sInput)/2
        * Valuto
        sTmp=Substr(sInput,(i-1)*2+1,2)
        cFirst=Lower(Left(sTmp,1))
        cSecond=Lower(Right(sTmp,1))
        ntmp=0
        Do Case
            Case cFirst>='0' And cFirst<='9'
                ntmp=(Asc(cFirst) - Asc('0'))*16
            Case cFirst>='a' And cFirst<='f'
                ntmp=(Asc(cFirst) - Asc('a')+10)*16
        Endcase

        Do Case
            Case cSecond>='0' And cSecond<='9'
                ntmp=ntmp+(Asc(cSecond) - Asc('0'))
            Case cSecond>='a' And cSecond<='f'
                ntmp=ntmp+(Asc(cSecond) - Asc('a'))+10
        Endcase

        sOutput=sOutput+Chr(ntmp)
    Next
    Return( Strtofile(sOutput,cNomeFIle))
Endfunc


Define Class GlobalVarSetup As Custom
    Procedure Init()
        This.Initialize()
    Endproc

    Procedure Initialize
        Local l_cVarName, l_numVarGlobal
        l_numVarGlobal = Amembers(l_aVarGlobal, This)
        For l_i=1 To l_numVarGlobal
            l_cVarName = l_aVarGlobal[l_i]
            If Lower(Left(l_cVarName ,2)) == 'i_' Or Lower(Left(l_cVarName ,2)) == 'g_' &&Variabile std codepainter
                If Type(l_cVarName)='U'
                    Public &l_cVarName
                    *--- Assegno il valore di dafault
                    &l_cVarName = This.&l_cVarName
                Endif
            Endif
        Endfor
    Endproc
Enddefine

Define Class ThemesManagerSetup As GlobalVarSetup
    *--- Caricamento automatico delle voci di men� associate alle funzionalit� dei bottoni del form solo per la pagina corrente (True)
    i_bPageFrmButton = .F.
    *--- Caricamento automatico delle voci di men� associate alle funzionalit� dei bottoni del form (True)
    i_bLoadFuncButton = .F.
    *--- Abilita gradiente sfondo form (solo per nuovi temi attivi)
    i_bGradientBck = .T.
    *--- Abilita nuova interfaccia per le tab 'S' (Standard), 'T' (Tab con tema)
    i_cMenuTab = "T"
    *--- Abilita Record mark su zoom e detail
    i_bRecordMark = .F.
    *--- Abilita controllo per intestazione avanzata degli zoom
    i_AdvancedHeaderZoom = .T.
    *--- Altezza testata zoom
    i_nHeaderHeight = 19
    *--- Altezza righe zoom
    i_nRowHeight = 19
    *--- Espandi zoom parameter
    i_bExpandZoomParameter = .F.
    *--- Abilita deskmenu 'H' Disabilitato, 'S' Abilitato, 'O' Aperto in ingresso
    i_cDeskMenu = "S"
    *--- Formato controlli 0 3D, 1 Plain, 2 Hot tracking
    i_nSpecialEffect = 1
    *--- Colore bordo controlli
    i_nBorderColor = Rgb(101,147,207)
    *--- Colori campi disabilitati
    i_udisabledforecolor=Rgb(0,41,91)
    i_udisabledbackcolor=Rgb(233,238,238)
    *--- Disabilita nuovi postit
    i_bDisableNewPostIt = .T.
    *--- Ricerca bottoni anche nei figli integrati (ButtonGest e Nuova interfaccia menu mnemonic key)
    i_bNoChildSearch = .F.
    *--- Abilita ricerca menu
    i_bSearchMenu = .T.
    *--- Abilita ricerca menu nel deskmenu
    i_bSearchMenuDesk = .F.
    *--- Numero minimo di caratteri da inserire per la ricerca
    i_nMaxSearchMenu = 3
    *--- Abilita ricerca dei recenti nella toolbar (application bar)
    i_bRecentMenu_toolbar = .T.
    *--- Abilita ricerca dei recenti nella navbar (desktop menu)
    i_bRecentMenu_navbar = .T.
    *--- Abilita ricerca dei recenti nel menu di windows
    i_bRecentMenu_windows = .T.
    *--- Numero di dati recenti memorizzati
    g_nRecRecent = 10
    *--- Ricerca bmp/ico immagini menu anche in custom
    i_bEnableCustomImage = .T.
    *--- Font
    i_cFontName = "Tahoma"
    i_cCboxFontName = "Tahoma"
    i_cBtnFontName = "Tahoma"
    i_cPageFontName = "Tahoma"
    i_cGrdFontName = "Tahoma"
    i_cLblFontName = "Tahoma"

    i_nFontSize = 9
    i_nPageFontSize = 9
    i_nCboxFontSize = 8
    i_nBtnFontSize = 7
    i_nGrdFontSize = 9
    i_nLblFontSize = 9

    *-- Font Italic
    i_bLblFontItalic = .F.
    i_bFontItalic = .F.
    i_bCboxFontItalic = .F.
    i_bBtnFontItalic = .F.
    i_bGrdFontItalic = .F.
    i_bPageFontItalic = .F.

    *-- Font Bold
    i_bLblFontBold = .F.
    i_bFontBold = .F.
    i_bCboxFontBold = .F.
    i_bBtnFontBold = .F.
    i_bGrdFontBold = .F.
    i_bPageFontBold = .F.

    *--- Tolleranza font size
    i_nThresholdFontSize = 5

    *--- Stato DeskMenu all'ingresso 'A' Aperto, 'C' Chiuso
    i_cDeskMenuStatus = 'A'
    i_nDeskMenuMaxButton = 5
    i_nDeskMenuInitDim = 258
    *--- Font DeskMenu
    i_cMenuNavFontName = "Tahoma"
    i_nMenuNavFontSize = 9
    i_cWindowManagerFontName = "Tahoma"
    i_nWindowManagerFontSize = 9

    *--- DateTime Mask, la dimensione delle colonne che contengono campi data viene forzata
    *--- per non mostrare l'ora (00:00:00), ma se nel formato della colonna dello zoom inserisco i_DateTimeMask,
    *--- viene mantenuta la dimensione standard
    i_cDateTimeMask = 'DT'
    *--- DatePickerFloating (F9 su data)
    i_bDatePickerFloating = .T.

    *--- Colore righe griglie
    i_nGridLineColor = Rgb(236,233,216)
    *--- Colore sfondo control con focus
    i_nBackColor = Rgb(255,231,162)
    *--- Colore di sfondo del box editabile
    i_nEBackColor = Rgb(255,231,162)

    *--- Colore rettangolo evidenziatore control obbligatori
    i_nOblColor = Rgb(255,128,128)
    *--- Curvatura rettangolo campi obbligatori
    i_nCurvature = 3
    *--- Abilita o meno l'evidenziazione dei campi obbligatori
    *--- Zucchetti Aulla
    *i_cHlOblColor = 'N'
    i_cHlOblColor = 'S'
    *--- Zucchetti Aulla

    *--- Evidenzia riga zoom selezionata
    i_nEviRigaZoom = 0
    i_nZoomColor = Rgb(255,231,162)

    *--- Dimensione Toolbar Button
    i_nTBtnW = 24
    i_nTBtnH = 24

    *--- Tema XP attivo o meno
    i_nXPTheme = 1

    *--- Settaggio Special Effect bottoni su tool bars/zoom
    i_nTbBtnSpEfc = 2
    *--- Settaggio Special Effect bottoni standard
    i_nBtnSpEfc = 2
    *--- Settaggio Special Effect bottoni Cp_Chprn (obsoleto)
    i_nPrnBtnSpEfc = 2

    *--- Larghezza bottone contestuale
    i_nZBtnWidth = 14
    *--- Bottone Contestuale Abilitato
    i_cZBtnShw='S'
    *--- Colore di sfondo riga selezionata dettagli
    i_nDtlRowClr=Rgb(215,215,253)
    *--- Menu in posizione fissa
    i_bMenuFix  = .T.
    *--- Mostra cptoolbar
    i_bShowCPToolBar = .T.
    *--- Mostra application bar
    i_bShowDeskTopBar = .T.
    *--- Abilita CTRL+T Menu
    i_bShowToolMenu = .T.
    *--- Alla presssione dell'F3 rimane nella pagina corrente..
    i_bFirstPage=.T.
    *--- Windows menu
    i_bWindowMenu = .F.
    *--- Colore sfondo applicativo
    i_nScreenColor = Rgb(255, 255, 255)
    *--- Dimensione di default delle bitmap all'interno dei bottoni
    i_nBtnImgSize = 24
    *--- Modalit� visualizzazione form ('C' = ClientArea, 'S' = Standard, 'A' = Entrambe)
    i_cViewMode = "S"
    *--- Apertura di default in modalit� ClientArea
    i_bClientAreaMode = .F.
    *--- Oggetto status bar
    i_oStatusBar = .Null.
    *--- Report engine behavior
    i_cRepBehavior = '8'
    *--- Nuove WaitWindow
    i_bWaitWindowTheme = .F.
    *--- Disattiva GotFocus sui tab in caso di CTRL+F6
    i_bGotFocusTab=.T.
    *--- Configurazione salvataggio posizione form
    i_cConfSavePosForm=' '
    *---  Inizio data AdvSpinner
    i_dIniDatAdvSpinner = {^1980/01/01}
    i_dFinDatAdvSpinner = {^2100/01/01}
    *---  Ritardo esecuzione tasti funzione in cp_doAction
    i_nDoActionDelay=50
    
    *--- Abilita l'interfaccia dei gadget
    g_cGadgetInterface = ''
    
    *--- Modalit� esportazione dati su excel
    g_ExlExpFas = 1 && copia solo le celle piene
    
    *--- iRevolution
    g_PUBPROCNAME = 'PUBBLICAZIONE_DATI'
    g_ASKPROCNAME = 'RICHIESTA_DATI'
    g_ASKSYNCPROCNAME = 'RICHIESTA_DATI_SINC'
    
Enddefine
