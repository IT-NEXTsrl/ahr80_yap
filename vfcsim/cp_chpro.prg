* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_CHPRO
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        :
* Data creazione:
* Aggiornato il :
* Translated:     01/03/2000
* #%&%#Build:  56
* ----------------------------------------------------------------------------
* Oggetti e classi per la gestione dei progressivi
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Parameters oParentObject
Return(Createobject("tCP_CHPRO",oParentObject))

* --- Definizione della classe
Define Class tCP_CHPRO As StdForm
    Top    = 59
    Left   = 44

    * --- Standard Properties
    Width  = 409+170
    Height = 143
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    * --- Constants Properties
    _IDX = 0
    cPrg = "CP_CHPRO"
    cComment = ""
    oParentObject = .Null.
    Icon = "anag.ico"
    WindowType = 1
    Closable = .F.

    * --- Local Variables
    w_elenco_p = 0
    w_valprg = 0
    w_var_des = Space(30)
    i_valprg = .F.

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc Init(oParentObject)
        If Vartype(oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        This.cComment= cp_Translate(MSG_AUTONUMBERED_VARIABLES_TABLE)
        StdForm::Init()
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tCP_CHPROPag1")
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate(MSG_PAGE_ONE)
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oelenco_p_1_1
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        This.Caption = This.cComment
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- si sposta senza attivare i controlli
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        * ---
        This.Hide()
        This.Release()
    Endproc
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.Hide()
            This.Release()
        Endif
    Endproc

    * --- Blank delle variabili del form
    Procedure BlankRec()
        This.ChildrenNewDocument()
        With This
            .w_elenco_p=0
            .w_valprg=0
            .w_var_des=Space(30)
            If .cFunction<>"Filter"
                .w_elenco_p = 1
            Endif
        Endwith
        This.SetControlsValue()
        This.NotifyEvent('Blank')
    Endproc

    Procedure SetEnabled(i_cOp)
        * --- Disattivazione pagina Elenco per situazioni <> da Query
        This.oPgFrm.Pages(This.oPgFrm.PageCount).Enabled=.T.
    Endproc

    * --- Genera il filtro
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Aggiornamento del Database
    Function mReplace(i_bEditing)
        This.NotifyEvent('Update start')
        This.NotifyEvent('Update end')
        Return(.T.)

        * --- Calcoli
    Function mCalc(i_bUpd)
        If i_bUpd
            With This
            Endwith
            This.SetControlsValue()
            This.bUpdated=This.bUpdated .Or. Inlist(This.cFunction,'Edit','Load')
            This.mEnableControls()
        Endif
        Return

        * --- Abilita controls condizionati
    Procedure mEnableControls()
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.ovalprg_1_3.Value==This.w_valprg)
            This.oPgFrm.Page1.oPag.ovalprg_1_3.Value=This.w_valprg
        Endif
        If Not(This.oPgFrm.Page1.oPag.ovar_des_1_5.Value==This.w_var_des)
            This.oPgFrm.Page1.oPag.ovar_des_1_5.Value=This.w_var_des
        Endif
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        If Not(i_bnoObbl)
            Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
        Endif
        If Not(i_bnoChk)
            Do cp_ErrorMsg With i_cErrorMsg
        Endif
        Return(i_bRes)
    Endfunc

    Proc Update_prg
        Local i_nModRow,i_kk
        i_nModRow = 0
        If This.oPgFrm.Page1.oPag.oelenco_p_1_1.ListIndex<>0
            cp_BeginTrs()
            cp_ErrorMsg(cp_MsgFormat(MSG_PROGRESSIVE__UPDATED_TO__,Trim(This.oPgFrm.Page1.oPag.i_valprg[this.oPgFrm.Page1.oPag.oelenco_p_1_1.listindex,1]),Ltrim(Str(This.w_valprg,10,0))),48,MSG_WARNING,.f.)
            If Empty(i_ServerConn[1,3])
                Update cpwarn Set autonum=This.w_valprg ;
                    where tablecode='prog\'+This.oPgFrm.Page1.oPag.i_valprg[this.oPgFrm.Page1.oPag.oelenco_p_1_1.listindex,1]
                i_nModRow = _Tally
            Else
                i_kk='prog\'+This.oPgFrm.Page1.oPag.i_valprg[this.oPgFrm.Page1.oPag.oelenco_p_1_1.listindex,1]
                i_nModRow=cp_TrsSQL(i_ServerConn[1,2],"UPDATE CPWARN SET AUTONUM="+cp_ToStrODBC(This.w_valprg)+" WHERE TABLECODE="+cp_ToStrODBC(i_kk))
            Endif
            cp_EndTrs()
            If i_nModRow<>0
                This.oPgFrm.Page1.oPag.i_valprg[this.oPgFrm.Page1.oPag.oelenco_p_1_1.listindex,2]=This.w_valprg
            Else
                cp_ErrorMsg(MSG_CANNOT_UPDATE_PROGRESSIVE_QM)
            Endif
        Endif
        Return

    Proc ChangeValue
        This.w_valprg=This.oPgFrm.Page1.oPag.i_valprg[this.oPgFrm.Page1.oPag.oelenco_p_1_1.listindex,2]
        This.oPgFrm.Page1.oPag.ovalprg_1_3.Value=This.oPgFrm.Page1.oPag.i_valprg[this.oPgFrm.Page1.oPag.oelenco_p_1_1.listindex,2]
        Return

Enddefine

* --- Definizione delle pagine come container
Define Class tCP_CHPROPag1 As StdContainer
    Width  = 413+170
    Height = 143
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    Dimension i_valprg[1,2]

    Add Object oBtn_1_8 As CPCommandButton With uid="ASBRNZBJTZ", Left=331+165, Top=100, Width=70,Height=27,;
        caption="", nPag=1
    Proc oBtn_1_8.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained)
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oelenco_p_1_1 As ListBox With Left=7, Top=9, Width=209+140, Height=127;
        ,cFormVar="w_elenco_p", RowSourceType=0, ButtonCount=1, bObbl = .F. , nPag = 1

    Procedure Init(cPrgName,nPag)

        * --- Zucchetti Aulla Inizio - traduzioni caption e tooltip
        This.oBtn_1_8.Caption=cp_Translate(MSG_EXIT_BUTTON)
        This.oBtn_1_7.Caption=cp_Translate(MSG_UPDATE_BUTTON)
        This.oStr_1_2.Caption=cp_Translate(MSG_PROGRESSIVE+MSG_FS)
        This.oStr_1_4.Caption=cp_Translate(MSG_DESCRIPTION)
        DoDefault(cPrgName,nPag)
        * --- Zucchetti Aulla Fine

        If Empty(This.FontName)
            This.FontName   = This.Parent.FontName
            This.FontSize   = This.Parent.FontSize
            This.FontBold   = This.Parent.FontBold
            This.FontItalic = This.Parent.FontItalic
            This.FontUnderline  = This.Parent.FontUnderline
            This.FontStrikethru = This.Parent.FontStrikethru
        Endif
        If Empty(i_ServerConn[1,3])
            Select * From cpwarn Into Cursor __wrn__
        Else
            =cp_SQL(i_ServerConn[1,2],"SELECT * FROM CPWARN",'__wrn__')
        Endif
        Go Top
        i=0
        Do While .Not. Eof()
            If Substr(tablecode,1,5)='prog\'
                i=i+1
                Dimension This.i_valprg[i,2]
                **** Zucchetti Aulla Inizio - Ripristino assegnamento come Build 49.
                **** Assegnamento Build 53 : this.i_valprg[i,1]= alltrim(substr(tablecode,RAT("\",tablecode)+1,45))
                **** In questo modo veniva riportata solo l'ultima parte del Codice del Progressivo
                This.i_valprg[i,1]=Substr(tablecode,6,45)
                **** Zucchetti Aulla Fine.
                This.i_valprg[i,2]=autonum
                This.oelenco_p_1_1.AddItem(This.i_valprg[i,1])
            Endif
            Skip
        Enddo
        Select __wrn__
        Use
        This.oelenco_p_1_1.ListIndex=0
    Endproc
    Proc oelenco_p_1_1.InteractiveChange()
        This.Parent.oContained.ChangeValue()
    Endproc

    **** Zucchetti Aulla - modificata la classe dell'oggetto per fare la valid su 15 caratteri invece che 14
    Add Object ovalprg_1_3 As  StdField  With ;
        cFormVar = "w_valprg", cQueryName = "valprg",     bObbl = .F. , nPag = 1, Value=0,;
        Height=21, Width=120, Left=308+135, Top=14  , cSayPict='"999999999999999"'  , cGetPict='"999999999999999"'

    Func ovalprg_1_3.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes=(.w_valprg<=999999999999999 And .w_valprg>=-999999999999999)
        Endwith
        Return bRes
    Endfunc

    Add Object ovar_des_1_5 As  StdField  With Visible=.F., ;
        cFormVar = "w_var_des", cQueryName = "var_des",     bObbl = .F. , nPag = 1, Value=Space(30),;
        Height=21, Width=212, Left=230+135, Top=61  , InputMask=Replicate('X',30)


    Add Object oBtn_1_7 As CPCommandButton With uid="ASBRNZBJTY", Left=230+135, Top=50, Width=70,Height=28,;
        caption="", nPag=1
    Proc oBtn_1_7.Click()
        With This.Parent.oContained
            This.Parent.oContained.Update_prg()
        Endwith
        If !Isnull(This.Parent.oContained)
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oStr_1_2 As StdString With Visible=.T., Left=231+135, Top=14,;
        Alignment=0, Width=72, Hight=15,;
        Caption=""  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_1_4 As StdString With Visible=.F., Left=232+135, Top=42,;
        Alignment=0, Width=70, Hight=15,;
        Caption=""  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oBox_1_6 As StdBox With Left=224+135, Top=8, Width=210,Height=78
Enddefine

