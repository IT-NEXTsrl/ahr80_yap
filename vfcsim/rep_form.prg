* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: rep_form                                                        *
*              Report form con XFRX                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-11                                                      *
* Last revis.: 2013-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_REPORT,w_ACTION,w_FILE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("trep_form",oParentObject,m.w_REPORT,m.w_ACTION,m.w_FILE)
return(i_retval)

define class trep_form as StdBatch
  * --- Local variables
  w_REPORT = space(50)
  w_ACTION = space(10)
  w_FILE = space(10)
  w_PREVIEW = .NULL.
  w_NORESET = .f.
  w_XFRXDRAWOBJECT = .NULL.
  w_XFFFILE = space(10)
  w_PRINTER = space(100)
  w_PAGERANGE = space(100)
  w_PRINTEROPTIONS = space(100)
  w_COPIES = 0
  w_PAGES = 0
  w_OKPRINT = .f.
  w_SECONDS = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Report form
    this.w_ACTION = UPPER( this.w_ACTION )
    this.w_NORESET = "NORESET" $ this.w_ACTION
    do case
      case "PREVIEW" $ this.w_ACTION
        if "NOPAGEEJECT" $ this.w_ACTION
          this.oParentObject.w_RUNREP_SESSION.cReport = this.w_REPORT
          this.oParentObject.w_RUNREP_SESSION.PreviewNoPageEject(this.w_NORESET)     
        else
          this.oParentObject.w_RUNREP_SESSION.cReport = this.w_REPORT
          this.oParentObject.w_RUNREP_SESSION.Preview(this.w_NORESET)     
          this.w_PREVIEW = CP_SHOWPREVIEW(THIS)
          this.w_PREVIEW = .Null.
        endif
      case "TO PRINTER" $ this.w_ACTION
        this.w_XFFFILE = TEMPADHOC()+"\"+"XFF_TEMP"+".XFF"
        L_VecchioErrore=On("error")
        On Error L_ERRORECANCELLAZIONEFILE=.T.
        if CP_FILEEXIST( this.w_XFFFILE )
          DELETE FILE ( this.w_XFFFILE )
        endif
        On Error &L_VecchioErrore
        this.oParentObject.w_RUNREP_SESSION.cReport = this.w_REPORT
        if EMPTY( this.oParentObject.w_RUNREP_SESSION.oSESSION.TargetFileName )
          this.oParentObject.w_RUNREP_SESSION.SetTargetType("XFF")     
          this.oParentObject.w_RUNREP_SESSION.SetTargetFilename(this.w_XFFFILE)     
        endif
        if "NOPAGEEJECT" $ this.w_ACTION
          this.oParentObject.w_RUNREP_SESSION.PreviewNoPageEject(this.w_NORESET)     
        else
          this.oParentObject.w_RUNREP_SESSION.oSESSION.DoNotOpenViewer = .T.
          this.oParentObject.w_RUNREP_SESSION.Preview(this.w_NORESET)     
          * --- w_PRINTER: Stampante selezionata
          this.w_PRINTER = SET( "Printer", 3 )
          * --- w_PAGERANGE: Intervallo di pagine
          this.w_PAGERANGE = ""
          * --- w_PRINTEROPTIONS: Opzioni di stampa
          this.w_PRINTEROPTIONS = _xfPrinterProperties(this.w_PRINTER, "", .F.)
          this.w_COPIES = 1
          this.w_PAGES = this.oParentObject.w_RUNREP_SESSION.oXffDocument.nPages
          * --- w_OKPRINT: Stampa confermata
          this.w_OKPRINT = .F.
          if "TO PRINTER PROMPT" $ this.w_ACTION
            do CP_GETPRINTEROPTIONS with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_OKPRINT
              do while this.w_COPIES >= 1
                this.oParentObject.w_RUNREP_SESSION.oxffDOCUMENT.printdocument(this.w_PRINTER, "Stampa xff", this.w_PAGERANGE, , this.w_PRINTEROPTIONS)     
                this.w_COPIES = this.w_COPIES - 1
              enddo
            endif
          else
            this.oParentObject.w_RUNREP_SESSION.oxffDOCUMENT.printdocument(SET( "Printer", 3 ), "Stampa xff")     
          endif
          this.w_SECONDS = SECONDS()
          L_VecchioErrore=On("error")
          On Error L_ERRORECANCELLAZIONEFILE=.T.
          if CP_FILEEXIST( this.w_XFFFILE )
            DELETE FILE ( this.w_XFFFILE )
          endif
          this.oParentObject.w_RUNREP_SESSION = .NULL.
          On Error &L_VecchioErrore
        endif
      case this.w_ACTION = "DOC/FLOW L"
        this.oParentObject.w_RUNREP_SESSION.cReport = this.w_REPORT
        this.oParentObject.w_RUNREP_SESSION.SetTargetType("FDOC")     
        this.oParentObject.w_RUNREP_SESSION.SetTargetFileName(this.w_FILE)     
        this.oParentObject.w_RUNREP_SESSION.Preview(this.w_NORESET)     
      case this.w_ACTION = "RTF/FLOW L"
        this.oParentObject.w_RUNREP_SESSION.cReport = this.w_REPORT
        this.oParentObject.w_RUNREP_SESSION.SetTargetType("FRTF")     
        this.oParentObject.w_RUNREP_SESSION.SetTargetFileName(this.w_FILE)     
        this.oParentObject.w_RUNREP_SESSION.Preview(this.w_NORESET)     
    endcase
  endproc


  proc Init(oParentObject,w_REPORT,w_ACTION,w_FILE)
    this.w_REPORT=w_REPORT
    this.w_ACTION=w_ACTION
    this.w_FILE=w_FILE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_REPORT,w_ACTION,w_FILE"
endproc
