* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: loadconfig                                                      *
*              LoadConfig                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-18                                                      *
* Last revis.: 2015-01-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tloadconfig",oParentObject)
return(i_retval)

define class tloadconfig as StdBatch
  * --- Local variables
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per la Nuova print System il batch LoadConfig3 ne gestisce l inizializzazione
    if i_bNewPrintSystem
      LoadConfig3 (this.oparentobject ) 
      i_retcode = 'stop'
      return
    endif
    this.w_OBJ = this.oparentobject
    * --- Inizializzo il nome stampante sulla print system con quella di default...
    if this.w_OBJ.cNomeReport.IsOnePrinter()
      this.w_OBJ.w_DEVICE = SET("printer",2)
      this.w_OBJ.prStamp = SET("printer",2)
    else
      this.w_OBJ.oTxt_Device.Enabled = .F.
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Zucchetti Aulla Inizio - Multireport
    if this.w_OBJ.cNomeReport.GetNumReport()>1
      this.w_OBJ.cComment=cp_translate(MSG_PRINT_SYSTEM_MR) 
 this.w_OBJ.Caption=this.w_OBJ.cComment
    endif
    * --- *--- Fascicolazione
    *       *--- Per DOCM --- Verifico che non sia gi� istanziato l'oggetto. Questo per ovviare alla cancellazione dei cursori
    *       *--- all'uscita dall'esecuzione del processo documentale  --- 
    this.w_OBJ.oSplitReport=CREATEOBJECT("MultiReport")
    if !SplitReport(this,this.w_OBJ.cNomeReport,this.w_OBJ.oSplitReport)
      this.w_OBJ.Release()
    endif
     oRepPlite=UPPER (this.w_OBJ.oSplitReport.FirstReport())
    if this.w_OBJ.w_PRODOCM<>"S"
       this.w_OBJ.oTxt_Copies.enabled=this.w_OBJ.cNomeReport.bNumCopy 
 this.w_OBJ.oTxt_Copies.value=this.w_OBJ.cNomeReport.nNumCopy 
 this.w_OBJ.prCopie=this.w_OBJ.cNomeReport.nNumCopy
    endif
    * --- Zucchetti Aulla Inizio - Multireport
    if this.w_OBJ.tReport = "I"
       this.w_OBJ.oBtn_Excel.enabled = .f. 
 this.w_OBJ.oBtn_Assoc.enabled = .f.
    endif
    * --- Per i report solo testo applica regole standard  
    if this.w_OBJ.cNomeReport.GetText(this.w_OBJ.nIndexReport)
      this.w_OBJ.cTipoReport="S" 
 this.w_OBJ.pdProcesso=this.w_OBJ.cNomeReport.CountDOCMReportsNotSkipped()>0 
 this.w_OBJ.SettaConfigurazione(this.w_OBJ.pdProcesso) 
 this.w_OBJ.oBtn_StpOpt.enabled = .f.
      * --- * Zucchetti Aulla Inizio
      *          * Gestisco anche anteprima di stampa
      this.w_OBJ.oBtn_Ante.enabled = cp_IsStdFile(this.w_OBJ.cNomeReport.GetReport(this.w_OBJ.nIndexReport),"FXP")
      * --- Zucchetti Aulla Fine
       this.w_OBJ.oBtn_Mail.enabled = this.w_OBJ.cNomeReport.IsStdFile(iif(this.w_OBJ.i_FDFFile,"FDF","FRX"),"G") 
 this.w_OBJ.oBtn_MPEC.enabled = this.w_OBJ.oBtn_Mail.enabled 
 this.w_OBJ.oBtn_Fax.enabled = .f. 
 this.w_OBJ.oBtn_Stp.enabled = this.w_OBJ.oBtn_Ante.enabled 
 this.w_OBJ.oBtn_Plite.enabled = .f. 
 
    else
      this.w_OBJ.cTipoReport=""
      if this.w_OBJ.i_FDFFile
         this.w_OBJ.oBtn_Stp.enabled = .t. 
 this.w_OBJ.oBtn_Ante.enabled = .t. 
 this.w_OBJ.oBtn_Mail.Enabled = .t. 
 this.w_OBJ.oBtn_MPEC.Enabled = .t. 
 this.w_OBJ.oBtn_StpOpt.enabled = .f. 
 this.w_OBJ.oBtn_Fax.Enabled = .f. 
      else
         this.w_OBJ.pdProcesso=this.w_OBJ.cNomeReport.CountDOCMReportsNotSkipped()>0 
 this.w_OBJ.SettaConfigurazione(this.w_OBJ.pdProcesso)
        * --- n ogni caso, la mail ed il fax pu� essere inviato solo se c'� il report
         this.w_OBJ.oBtn_Mail.Enabled=this.w_OBJ.oBtn_Stp.enabled 
 this.w_OBJ.oBtn_MPEC.Enabled=this.w_OBJ.oBtn_Stp.enabled 
 this.w_OBJ.oBtn_Fax.Enabled =this.w_OBJ.oBtn_Stp.enabled
      endif
       this.w_OBJ.oBtn_Plite.Enabled = not bReportVuoto AND this.w_OBJ.tReport<>"I" AND chkplite(oRepPlite)=0
    endif
    this.w_OBJ.oBtn_WWP_ZCP.Enabled = this.w_OBJ.oBtn_Mail.Enabled and iif(g_WEENABLED="S" and g_IZCP<>"S" and g_IZCP<>"A", g_WEUTEENABLED="S", g_ZCPUENABLED="S")
    * --- Zucchetti aulla Inizio - Integrazione Archeasy
     this.w_OBJ.oBtn_StpArchi.Enabled = not this.w_OBJ.PDProcesso AND this.w_OBJ.oBtn_StpOpt.enabled And (g_ARCHI="S" OR g_ARCHI<>"S" AND g_CPIN="N" AND g_DMIP="N" AND g_IZCP$"SA") 
 this.w_OBJ.oBtn_StpArchi.Visible = ! IsAlt() 
 this.w_OBJ.oOpt_File.Click() 
 this.w_OBJ.oBtn_StpFileOpt.enabled = not this.w_OBJ.pdProcesso and this.w_OBJ.oBtn_StpFileOpt.enabled AND NOT bReportVuoto 
    * --- * --- Zucchetti Aulla Fine
    *     *--- Zucchetti Aulla Inizio - Traduzione report
    *       * Settaggi per lingue
    cp_CHPFUN (this.w_OBJ,"SettaggiLingue")
    if this.w_OBJ.oOpt_File.listcount>=1
      this.w_OBJ.oOpt_File.listindex = 1
    endif
    * --- Zucchetti Aulla Fine - Traduzione report
    * --- Imposta il focus sul bottone esegui o stampa a seconda che sia la print system del docm o la print system normale
    if this.w_OBJ.pdProcesso
      * --- Imposta il focus su esegui
      this.w_OBJ.oPDBtn_Proc.setfocus()
    else
      * --- Imposta il focus sul bottone stampa
      this.w_OBJ.oBtn_Stp.setfocus()
    endif
    this.w_OBJ.w_PRODOCM = "S"
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     local i_nCurs, i_found, i_oa,i_workstation 
 i_oa = select() 
 i_nCurs = sys(2015) 
 i_workstation=LEFT(SYS(0), RAT(" # ",SYS(0))-1)
    * --- *--- Multireport
    *         *--- Se � presente una stampa principale solo testo, leggo l'associazione di quel report
    if !this.w_OBJ.cNomeReport.IsEmptyCursor()
       LOCAL l_i 
 l_i=0
      do while l_i<>-1
        l_i=this.w_OBJ.cNomeReport.NextReport(l_i) 
        if l_i>0 AND this.w_OBJ.cNomeReport.GetText(l_i)
           this.w_OBJ.nIndexReport=l_i 
 this.w_OBJ.tReport="S" 
 EXIT
        endif
      enddo
    endif
    RptTxt_Search=this.w_OBJ.cNomeReport.GetReport(this.w_OBJ.nIndexReport)
    * --- Multireport
    if this.w_OBJ.cNomeReport.bStampanti
      if i_ServerConn[1,2]<>0
        cp_sql(i_ServerConn[1,2],"select * from cpusrrep where repasso="+cp_ToStr(IIF(this.w_OBJ.tReport="S",RptTxt_Search+".FXP",this.w_OBJ.cNomeReport.FirstReport()+".FRX"))+" and (aziasso="+cp_ToStr(i_codazi)+" or aziasso='          ' or aziasso is null) and (wstasso="+cp_ToStr(i_workstation)+" or wstasso='          ' or wstasso is null) and (usrasso="+cp_ToStr(i_codute)+" or usrasso=0 or usrasso is null) order by wstasso desc,aziasso desc, usrasso desc",i_nCurs)
      else
        select * from cpusrrep where repasso=IIF(this.w_OBJ.tReport="S",RptTxt_Search+".FXP",this.w_OBJ.cNomeReport.FirstReport()+".FRX") AND (TRIM(i_codazi)==TRIM(aziasso) OR EMPTY(aziasso)) AND (TRIM(wstasso)==TRIM(i_workstation) OR EMPTY(wstasso)) AND (usrasso=i_codute or usrasso=0) ORDER BY wstasso desc,aziasso desc, usrasso desc into curs (i_nCurs)
      endif
      if used(i_nCurs)
        * --- *--- Ordino per utente e azienda in modo da prendere sempre prima le associazioni con utente e/o azienda valorizzate 
        *           *--- indipendentemente dall'ordine di caricamento; ordino il cursore fox per evitare problemi con DB2/Oracle e il verso asc o desc
        SELECT * FROM (i_nCurs) ORDER BY usrasso DESC, aziasso DESC INTO CURSOR (i_nCurs)
        if not(eof())
           i_found = .f. 
 go top 
 this.w_OBJ.w_Device = trim(DEVASSO) 
 this.w_OBJ.prStamp = trim(DEVASSO) 
 this.w_OBJ.cModSst = trim(MODASSO)
          if this.w_OBJ.w_PRODOCM<>"S"
             this.w_OBJ.prCopie = iif(COPASSO=0,1,COPASSO) 
 this.w_OBJ.w_Txt_Copies= iif(COPASSO=0,1,COPASSO) 
          endif
        endif
        use
        * --- Zucchetti Aulla Inizio : Ricerce stampante windows corretta su terminal Server
        if Right(Alltrim(this.w_OBJ.w_Device),Len(g_PRTJOLLY))= g_PRTJOLLY
          tempStamp=printerChecked(Left(Alltrim(this.w_OBJ.w_Device),Len(Alltrim(this.w_OBJ.w_Device))-Len(g_PRTJOLLY)))
          if NOT EMPTY(tempStamp)
             this.w_OBJ.w_Device=tempStamp 
 this.w_OBJ.prStamp=tempStamp
          endif
        endif
        * --- Zucchetti Aulla Fine : Ricerca stampante windows corretta 
      else
        cp_msg(cp_Translate(MSG_ERROR_OPENING_PRINTING_TABLE))
      endif
    endif
    select (i_oa)
    * --- *--- Multireport
    *         *--- Ricerco modelli
    this.w_OBJ.cNomeReport.CheckModels()
    * --- Abilito word
    if this.w_OBJ.cNomeReport.CheckWord()
      this.w_OBJ.oBtn_Word.enabled = .t.
    else
       this.w_OBJ.oBtn_Word.enabled = .f.
    endif
    * --- Zucchetti Aulla Inizio: Disabilito bottone excell se non ho attiva una Suite Office
    if g_OFFICE$"MO" AND NOT bReportVuoto 
      this.w_OBJ.oBtn_Excel.enabled = .t.
    else
      this.w_OBJ.oBtn_Excel.enabled = .f.
    endif
    * --- Zucchetti Aulla Fine
    local l_CheckGraphVer
    l_CheckGraphVer = this.w_OBJ.cNomeReport.CheckGraphVer() 
    if l_CheckGraphVer = "N"
      this.w_OBJ.oBtn_Graph.enabled = .f.
    else
      if l_CheckGraphVer = "G"
        this.w_OBJ.oBtn_Graph.Picture = "graph.ico"
        this.w_OBJ.oBtn_Graph.ToolTipText = MSG_MSGRAPH_GRAPH
      else
        this.w_OBJ.oBtn_Graph.Picture = "foxcharts.ico"
        this.w_OBJ.oBtn_Graph.ToolTipText = MSG_MSGRAPH_FOXCHARTS
      endif
      this.w_OBJ.oBtn_Graph.enabled = .t.
    endif
    * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
    if g_OFFICE="M"
      if !this.w_OBJ.cNomeReport.CheckExcel()
         this.w_OBJ.oBtn_Excel.picture = i_cBmpPath+"prexcel.bmp"
      else
         this.w_OBJ.oBtn_Excel.DisabledPicture = i_cBmpPath+"prmexceld.bmp" 
 this.w_OBJ.oBtn_Excel.picture = i_cBmpPath+"prmexcel.bmp"
      endif
       this.w_OBJ.oBtn_Excel.ToolTipText=cp_Translate(MSG_MICROSOFT_EXCEL_DOCUMENT) 
 this.w_OBJ.oBtn_Excel.Caption = "E\<xcel" 
 this.w_OBJ.oBtn_Word.DisabledPicture = i_cBmpPath+"prmwordd.bmp" 
 this.w_OBJ.oBtn_Word.picture = i_cBmpPath+"prmword.bmp" 
 this.w_OBJ.oBtn_Word.Caption = "\<Word" 
 this.w_OBJ.oBtn_Word.ToolTipText = cp_Translate(MSG_MICROSOFT_WORD_DOCUMENT)
    else
       this.w_OBJ.oBtn_Word.picture = i_cBmpPath+"writers.bmp" 
 this.w_OBJ.oBtn_Word.DisabledPicture = i_cBmpPath+"writersd.bmp" 
 this.w_OBJ.oBtn_Word.Caption = "\<Write" 
 this.w_OBJ.oBtn_Word.ToolTipText=cp_Translate(MSG_OPENOFFICE_WRITER_DOCUMENT) 
 this.w_OBJ.oBtn_Excel.picture = i_cBmpPath+"osheets.bmp" 
 this.w_OBJ.oBtn_Excel.DisabledPicture = i_cBmpPath+"osheet.bmp" 
 this.w_OBJ.oBtn_Excel.Caption = "\<Calc" 
 this.w_OBJ.oBtn_Excel.ToolTipText=cp_Translate(MSG_OPENOFFICE_CALC_DOCUMENT)
    endif
    * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
    if !this.w_OBJ.cNomeReport.IsEmptyCursor() AND this.w_OBJ.cNomeReport.IsStdFile("FRX")
      * --- Multireport
       this.w_OBJ.oBtn_Stp.enabled = .t. 
 this.w_OBJ.oBtn_StpOpt.enabled = .t. 
 this.w_OBJ.oBtn_Ante.enabled = .t.
    else
       this.w_OBJ.oBtn_Stp.enabled = .f. 
 this.w_OBJ.oBtn_StpOpt.enabled = .f. 
 this.w_OBJ.oBtn_Ante.enabled = iif(this.w_OBJ.tReport="I",.t.,.f.)
    endif
    * --- *w_OBJ.oBtn_StpFile.enabled = w_OBJ.oBtn_Ante.enabled
    this.w_OBJ.oBtn_Assoc.enabled = !this.w_OBJ.cNomeReport.IsEmpty()
    * --- Modifica bottone stampa GDI+
     this.w_OBJ.oBtn_Stp.picture=IIF(this.w_OBJ.cNomeReport.cReportBehavior="90", "PRSTAMPAPLUS.BMP", iif(Pemstatus(this.w_OBJ.oBtn_Stp, "CpPicture", 5), this.w_OBJ.oBtn_Stp.CpPicture, this.w_OBJ.oBtn_Stp.picture)) 
 this.w_OBJ.oBtn_StpOpt.picture=IIF(this.w_OBJ.cNomeReport.cReportBehavior="90", "PROPTIONPLUS.BMP", iif(Pemstatus(this.w_OBJ.oBtn_StpOpt, "CpPicture", 5), this.w_OBJ.oBtn_StpOpt.CpPicture, this.w_OBJ.oBtn_StpOpt.picture)) 
 this.w_OBJ.oBtn_Stp.DisabledPicture =IIF(this.w_OBJ.cNomeReport.cReportBehavior="90", "PRSTAMPAPLUSD.BMP", this.w_OBJ.oBtn_Stp.DisabledPicture) 
 this.w_OBJ.oBtn_StpOpt.DisabledPicture =IIF(this.w_OBJ.cNomeReport.cReportBehavior="90", "PROPTIONPLUSD.BMP", this.w_OBJ.oBtn_StpOpt.DisabledPicture)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
