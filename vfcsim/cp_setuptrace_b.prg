* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_setuptrace_b                                                 *
*              Messa a punto log                                               *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-24                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_setuptrace_b",oParentObject,m.pAZIONE)
return(i_retval)

define class tcp_setuptrace_b as StdBatch
  * --- Local variables
  pAZIONE = space(1)
  w_POSIZIONEZIP = space(250)
  w_GETDIR = space(250)
  w_OLD_ACTIVATEPROFILER = 0
  w_FILECNFSTR = space(250)
  w_LUNGHEZZA = 0
  w_POSIZIONECORRENTE = 0
  w_ACAPO = space(2)
  w_VARIABILE = space(10)
  w_VALORE = space(10)
  w_TROVATO = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messa a punto log
    do case
      case this.pAZIONE = "I"
        * --- Inizio
        *     Popola le variabili della maschera
        this.oParentObject.w_ATTIVATO = i_nACTIVATEPROFILER
        this.oParentObject.w_POSIZIONELOG = i_cACTIVATEPROFILER_POSIZIONELOG
        * --- Gesione multisessione
        if i_nACTIVATEPROFILER_MULTISESSION = 1
          if EMPTY( i_nACTIVATEPROFILER_MULTISESSION )
            this.oParentObject.w_SESSIONNAME = SYS( 2015 )
          else
            this.oParentObject.w_SESSIONNAME = i_cACTIVATEPROFILER_SESSIONNAME
          endif
          this.oParentObject.w_NOMELOG = STRTRAN( i_cACTIVATEPROFILER_NOMELOG , i_cACTIVATEPROFILER_SESSIONNAME , "" )
        else
          this.oParentObject.w_SESSIONNAME = ""
          this.oParentObject.w_NOMELOG = i_cACTIVATEPROFILER_NOMELOG
        endif
        this.oParentObject.w_TIPOROLLOVER = i_cACTIVATEPROFILER_TIPOROLLOVER
        if i_nACTIVATEPROFILER_ROLLOVER <> 0
          this.oParentObject.w_ROLLOVER = i_nACTIVATEPROFILER_ROLLOVER
          this.oParentObject.o_TIPOROLLOVER = this.oParentObject.w_TIPOROLLOVER
        endif
        this.oParentObject.w_NUMERODIFILEDILOG = i_nACTIVATEPROFILER_NUMERODIFILEDILOG
        this.oParentObject.w_TRACCIATURAGESTIONI = i_nACTIVATEPROFILER_TRACCIATURAGESTIONI
        this.oParentObject.w_TRACCIATURAROUTINE = i_nACTIVATEPROFILER_TRACCIATURAROUTINE
        this.oParentObject.w_TRACCIATURAFUNZIONI = i_nACTIVATEPROFILER_TRACCIATURAFUNZIONI
        this.oParentObject.w_TRACCIATURAQUERYASINCRONA = i_nACTIVATEPROFILER_TRACCIATURAQUERYASINCRONA
        this.oParentObject.w_TRACCIATURASQLLETTURA = i_nACTIVATEPROFILER_TRACCIATURASQLLETTURA
        this.oParentObject.w_TRACCIATURASQLSCRITTURA = i_nACTIVATEPROFILER_TRACCIATURASQLSCRITTURA
        this.oParentObject.w_TRACCIATURASQLINSERIMENTO = i_nACTIVATEPROFILER_TRACCIATURASQLINSERIMENTO
        this.oParentObject.w_TRACCIATURASQLCANCELLAZIONE = i_nACTIVATEPROFILER_TRACCIATURASQLCANCELLAZIONE
        this.oParentObject.w_TRACCIATURASQLTEMPORANEO = i_nACTIVATEPROFILER_TRACCIATURASQLTEMPORANEO
        this.oParentObject.w_TRACCIATURASQLMODIFICADB = i_nACTIVATEPROFILER_TRACCIATURASQLMODIFICADB
        this.oParentObject.w_TRACCIATURASQLALTROSUDB = i_nACTIVATEPROFILER_TRACCIATURASQLALTROSUDB
        this.oParentObject.w_TRACCIATURATRANSAZIONI = i_nACTIVATEPROFILER_TRACCIATURATRANSAZIONI
        this.oParentObject.w_TRACCIATURADEADLOCK = i_nACTIVATEPROFILER_TRACCIATURADEADLOCK
        this.oParentObject.w_TRACCIATURARICONNESSIONE = i_nACTIVATEPROFILER_TRACCIATURARICONNESSIONE
        this.oParentObject.w_TRACCIATURATASTIFUN = i_nACTIVATEPROFILER_TRACCIATURATASTIFUN
        this.oParentObject.w_TRACCIATURAEVENTI = i_nACTIVATEPROFILER_TRACCIATURAEVENTI
        this.oParentObject.w_RIGHE = i_nACTIVATEPROFILER_RIGHE
        this.oParentObject.w_MILLISECONDI = i_nACTIVATEPROFILER_MILLISECONDI
        this.oParentObject.w_MULTISESSION = i_nACTIVATEPROFILER_MULTISESSION
        this.oParentObject.w_TRACCIATURAVQR = i_nACTIVATEPROFILER_TRACCIATURAVQR
        this.oParentObject.mCalc( .T. )
      case this.pAZIONE = "S"
        * --- Inizio
        *     Valorizza le variabili pubbliche
        i_nACTIVATEPROFILER = this.oParentObject.w_ATTIVATO
        if ALLTRIM( i_cACTIVATEPROFILER_POSIZIONELOG ) <> ALLTRIM( this.oParentObject.w_POSIZIONELOG ) OR ALLTRIM( i_cACTIVATEPROFILER_NOMELOG ) <> ALLTRIM( this.oParentObject.w_NOMELOG )
          * --- Se cambia la posizione ( o il nome ) del file di log, occorre chiudere il log corrente
          if USED( ALLTRIM( i_cACTIVATEPROFILER_POSIZIONELOG ) )
            SELECT( ALLTRIM( i_cACTIVATEPROFILER_POSIZIONELOG ) )
            USE
          endif
        endif
        i_cACTIVATEPROFILER_POSIZIONELOG = ALLTRIM( this.oParentObject.w_POSIZIONELOG )
        * --- L'utente vede il nome senza la sessione
        if EMPTY( this.oParentObject.w_NOMELOG )
          this.oParentObject.w_NOMELOG = "ActivityLogger"
        endif
        if ALLTRIM( i_cACTIVATEPROFILER_NOMELOG ) <> ALLTRIM( this.oParentObject.w_NOMELOG ) + IIF( this.oParentObject.w_MULTISESSION = 1 , ALLTRIM( this.oParentObject.w_SESSIONNAME ) , "" )
          if USED( ALLTRIM( i_cACTIVATEPROFILER_NOMELOG ) )
            SELECT( ALLTRIM( i_cACTIVATEPROFILER_NOMELOG ) )
            USE
          endif
        endif
        i_cACTIVATEPROFILER_NOMELOG = ALLTRIM( this.oParentObject.w_NOMELOG ) + IIF( this.oParentObject.w_MULTISESSION = 1 , ALLTRIM( this.oParentObject.w_SESSIONNAME ) , "" )
        if VARTYPE( i_nACTIVATEPROFILER_ALIAS ) <> "C"
          PUBLIC i_nACTIVATEPROFILER_ALIAS
        else
          if Used( i_nACTIVATEPROFILER_ALIAS )
            USE IN (i_nACTIVATEPROFILER_ALIAS)
          endif
        endif
        i_nACTIVATEPROFILER_ALIAS = SYS(2015)
        i_cACTIVATEPROFILER_TIPOROLLOVER = this.oParentObject.w_TIPOROLLOVER
        i_nACTIVATEPROFILER_ROLLOVER = this.oParentObject.w_ROLLOVER
        i_nACTIVATEPROFILER_NUMERODIFILEDILOG = this.oParentObject.w_NUMERODIFILEDILOG
        i_nACTIVATEPROFILER_TRACCIATURAGESTIONI = this.oParentObject.w_TRACCIATURAGESTIONI
        i_nACTIVATEPROFILER_TRACCIATURAROUTINE = this.oParentObject.w_TRACCIATURAROUTINE
        i_nACTIVATEPROFILER_TRACCIATURAFUNZIONI = this.oParentObject.w_TRACCIATURAFUNZIONI
        i_nACTIVATEPROFILER_TRACCIATURAQUERYASINCRONA = this.oParentObject.w_TRACCIATURAQUERYASINCRONA
        i_nACTIVATEPROFILER_TRACCIATURASQLLETTURA = this.oParentObject.w_TRACCIATURASQLLETTURA
        i_nACTIVATEPROFILER_TRACCIATURASQLSCRITTURA = this.oParentObject.w_TRACCIATURASQLSCRITTURA
        i_nACTIVATEPROFILER_TRACCIATURASQLINSERIMENTO = this.oParentObject.w_TRACCIATURASQLINSERIMENTO
        i_nACTIVATEPROFILER_TRACCIATURASQLCANCELLAZIONE = this.oParentObject.w_TRACCIATURASQLCANCELLAZIONE
        i_nACTIVATEPROFILER_TRACCIATURASQLTEMPORANEO = this.oParentObject.w_TRACCIATURASQLTEMPORANEO
        i_nACTIVATEPROFILER_TRACCIATURASQLMODIFICADB = this.oParentObject.w_TRACCIATURASQLMODIFICADB
        i_nACTIVATEPROFILER_TRACCIATURASQLALTROSUDB = this.oParentObject.w_TRACCIATURASQLALTROSUDB
        i_nACTIVATEPROFILER_TRACCIATURATRANSAZIONI = this.oParentObject.w_TRACCIATURATRANSAZIONI
        i_nACTIVATEPROFILER_TRACCIATURADEADLOCK = this.oParentObject.w_TRACCIATURADEADLOCK
        i_nACTIVATEPROFILER_TRACCIATURARICONNESSIONE = this.oParentObject.w_TRACCIATURARICONNESSIONE
        i_nACTIVATEPROFILER_MULTISESSION = this.oParentObject.w_MULTISESSION
        i_cACTIVATEPROFILER_SESSIONNAME = this.oParentObject.w_SESSIONNAME
        i_nACTIVATEPROFILER_RIGHE = this.oParentObject.w_RIGHE
        i_nACTIVATEPROFILER_MILLISECONDI = this.oParentObject.w_MILLISECONDI
        i_nACTIVATEPROFILER_TRACCIATURAVQR = this.oParentObject.w_TRACCIATURAVQR
        i_nACTIVATEPROFILER_TRACCIATURATASTIFUN = this.oParentObject.w_TRACCIATURATASTIFUN
        i_nACTIVATEPROFILER_TRACCIATURAEVENTI = this.oParentObject.w_TRACCIATURAEVENTI
        if Vartype(g_DEBUGMODE)<>"N"
          Public g_DEBUGMODE
        endif
         g_DEBUGMODE=this.oParentObject.w_DEBUG
      case this.pAZIONE = "G"
        * --- Zoom sulla posizione del file di log
        this.w_GETDIR = cp_GETDIR()
        if " " $ ALLTRIM( this.w_GETDIR )
          AH_ERRORMSG("Selezionare un percorso che non contenga spazi")
        else
          this.oParentObject.w_POSIZIONELOG = this.w_GETDIR
          THIS.OPARENTOBJECT.NOTIFYEVENT("w_POSIZIONELOG Changed")
        endif
      case this.pAZIONE = "Z"
        * --- Crea la cartella compressa con il contenuto del log
        this.w_POSIZIONEZIP = GETFILE( "7Z" , "Selezionare un file per lo zip" )
        if EMPTY( this.w_POSIZIONEZIP )
          cp_ERRORMSG("Operazione abbandonata","i","")
          i_retcode = 'stop'
          return
        endif
        this.w_OLD_ACTIVATEPROFILER = i_nACTIVATEPROFILER
        * --- Disattivazione log
        i_nACTIVATEPROFILER = 0
        if UPPER( ALLTRIM( RIGHT( this.w_POSIZIONEZIP , 3 ) ) ) <> ".7Z"
          this.w_POSIZIONEZIP = ALLTRIM( this.w_POSIZIONEZIP ) + "7Z"
        endif
        if USED( ALLTRIM( i_cACTIVATEPROFILER_NOMELOG ) )
          SELECT( ALLTRIM( i_cACTIVATEPROFILER_NOMELOG ) )
          USE
        endif
        if VARTYPE( i_nACTIVATEPROFILER_ALIAS ) = "C"
          if USED( ALLTRIM( i_nACTIVATEPROFILER_ALIAS ) )
            SELECT( ALLTRIM( i_nACTIVATEPROFILER_ALIAS ) )
            USE
          endif
        endif
        DELETE FILE (this.w_POSIZIONEZIP)
        L_ERRORE = 0
        if ZIPUNZIP("Z",this.w_POSIZIONEZIP,ADDBS(ALLTRIM(this.oParentObject.w_POSIZIONELOG)),"",@L_ERRORE)
          * --- Operazione eseguita con successo
          cp_ERRORMSG("Operazione eseguita","i","")
        else
          _CLIPTEXT = L_ERRORE
          if TYPE("L_ERRORE")="C"
            cp_ERRORMSG("Operazione fallita:"+CHR(13)+L_ERRORE,"!","")
          else
            cp_ERRORMSG("Operazione fallita","!","")
          endif
        endif
        * --- Riattivazione log
        i_nACTIVATEPROFILER = this.w_OLD_ACTIVATEPROFILER
      case this.pAZIONE = "C"
        * --- Salva le impostazioni nel file CNF
        this.oParentObject.w_FILECNF = GETFILE("CNF")
        if EMPTY( this.oParentObject.w_FILECNF )
          cp_ERRORMSG("Operazione interrotta","i","")
          i_retcode = 'stop'
          return
        endif
        * --- Copia il file in una stringa
        if cp_FileExist(this.oParentObject.w_FILECNF)
          this.w_FILECNFSTR = FILETOSTR( this.oParentObject.w_FILECNF )
        else
          this.w_FILECNFSTR = ""
        endif
        * --- calcola il carattere utilizzato per l'invio a capo
        this.w_ACAPO = CHR( 13 ) + CHR ( 10 )
        if NOT this.w_ACAPO $ this.w_FILECNFSTR AND CHR( 10 ) $ this.w_FILECNFSTR
          this.w_ACAPO = CHR ( 10 )
        endif
        if NOT this.w_ACAPO $ this.w_FILECNFSTR AND CHR( 13 ) $ this.w_FILECNFSTR
          this.w_ACAPO = CHR ( 13 )
        endif
        * --- Viene copiato il contenuto in un array, compresi gli invii a capo
        ALINES(ARRAYCNF, this.w_FILECNFSTR, 16)
        * --- Variabili da cercare e sostituire e relativo valore da assegnare
        this.w_VARIABILE = "i_nACTIVATEPROFILER"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_ATTIVATO ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_cACTIVATEPROFILER_POSIZIONELOG"
        this.w_VALORE = '"' + STRTRAN( ALLTRIM( this.oParentObject.w_POSIZIONELOG ) , '"', '""' ) + '"'
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_cACTIVATEPROFILER_NOMELOG"
        this.w_VALORE = '"' + STRTRAN( ALLTRIM( this.oParentObject.w_NOMELOG ) , '"', '""' ) + '"'
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_ROLLOVER"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_ROLLOVER ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_cACTIVATEPROFILER_TIPOROLLOVER"
        this.w_VALORE = "'" + this.oParentObject.w_TIPOROLLOVER + "'"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_NUMERODIFILEDILOG"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_NUMERODIFILEDILOG ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAGESTIONI"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURAGESTIONI ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAROUTINE"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURAROUTINE ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAFUNZIONI"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURAFUNZIONI ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAQUERYASINCRONA"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURAQUERYASINCRONA ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLLETTURA"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURASQLLETTURA ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLSCRITTURA"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURASQLSCRITTURA ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLINSERIMENTO"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURASQLINSERIMENTO ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLCANCELLAZIONE"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURASQLCANCELLAZIONE ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLTEMPORANEO"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURASQLTEMPORANEO ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLMODIFICADB"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURASQLMODIFICADB ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLALTROSUDB"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURASQLALTROSUDB ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURATRANSAZIONI"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURATRANSAZIONI ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURADEADLOCK"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURADEADLOCK ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURARICONNESSIONE"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURARICONNESSIONE ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_MULTISESSION"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_MULTISESSION ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_RIGHE"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_RIGHE ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_MILLISECONDI"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_MILLISECONDI ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAVQR"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURAVQR ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAEVENTI"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURAEVENTI ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURATASTIFUN"
        this.w_VALORE = ALLTRIM( STR ( this.oParentObject.w_TRACCIATURATASTIFUN ) )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- --
        * --- Ricostruisce la stringa da copiare nel file
        this.w_LUNGHEZZA = ALEN( ARRAYCNF )
        this.w_POSIZIONECORRENTE = 1
        this.w_FILECNFSTR = ""
        do while this.w_POSIZIONECORRENTE <= this.w_LUNGHEZZA
          this.w_FILECNFSTR = this.w_FILECNFSTR + ARRAYCNF(this.w_POSIZIONECORRENTE)
          this.w_POSIZIONECORRENTE = this.w_POSIZIONECORRENTE + 1
        enddo
        * --- Aggiorna il file CNF
        VecchioErrore=ON("error")
        w_bErrore = .F.
        ON ERROR w_bErrore = .T.
        STRTOFILE( this.w_FILECNFSTR, this.oParentObject.w_FILECNF )
        ON ERROR &VecchioErrore
        if w_bERRORE
          cp_ERRORMSG("Operazione fallita."+CHR(13)+"Controllare i diritti di accesso al file","!","")
        else
          cp_ERRORMSG("Operazione eseguita","i","")
        endif
      case this.pAZIONE = "U"
        * --- Decomprime la cartella compressa contenente il log, all'interno della
        *     cartella corrente di log
        this.w_POSIZIONEZIP = GETFILE( "7Z" , "Selezionare un file da decomprimere" )
        if EMPTY( this.w_POSIZIONEZIP )
          cp_ERRORMSG("Operazione abbandonata","i","")
          i_retcode = 'stop'
          return
        endif
        L_ERRORE = 0
        if ZIPUNZIP("U",this.w_POSIZIONEZIP,ADDBS(ALLTRIM(this.oParentObject.w_POSIZIONELOG))+ADDBS(JUSTSTEM(this.w_POSIZIONEZIP)),"",@L_ERRORE)
          cp_ERRORMSG("Operazione eseguita","i","")
        else
          _CLIPTEXT = L_ERRORE
          if TYPE("L_ERRORE")="C"
            cp_ERRORMSG("Operazione fallita:"+CHR(13)+L_ERRORE,"!","")
          else
            cp_ERRORMSG("Operazione fallita","!","")
          endif
        endif
      case this.pAZIONE = "D"
        * --- Controlla se la cartella di destinazione del log � su una unit� locale
        if NOT EMPTY( JUSTDRIVE( this.oParentObject.w_POSIZIONELOG ) )
          if DRIVETYPE( JUSTDRIVE( this.oParentObject.w_POSIZIONELOG ) ) = 1
            cp_ERRORMSG( "Le unit� diverse da disco rigido potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
          endif
          if DRIVETYPE( JUSTDRIVE( this.oParentObject.w_POSIZIONELOG ) ) = 2
            cp_ERRORMSG( "Le unit� disco di tipo floppy potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
          endif
          if DRIVETYPE( JUSTDRIVE( this.oParentObject.w_POSIZIONELOG ) ) = 4
            cp_ERRORMSG( "Le unit� disco di rete o removibili potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
          endif
          if DRIVETYPE( JUSTDRIVE( this.oParentObject.w_POSIZIONELOG ) ) = 5
            cp_ERRORMSG( "Le unit� disco di tipo CD-ROM potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
          endif
          if DRIVETYPE( JUSTDRIVE( this.oParentObject.w_POSIZIONELOG ) ) = 6
            cp_ERRORMSG( "Le unit� disco di tipo RAM-DISK potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
          endif
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LUNGHEZZA = ALEN( ARRAYCNF )
    this.w_POSIZIONECORRENTE = 1
    this.w_TROVATO = .F.
    do while this.w_POSIZIONECORRENTE <= this.w_LUNGHEZZA
      if LEFT( ALLTRIM( ARRAYCNF(this.w_POSIZIONECORRENTE) ) , 1 ) <> "*" AND UPPER(this.w_VARIABILE)+"=" $ STRTRAN(UPPER(ARRAYCNF(this.w_POSIZIONECORRENTE))," ","")
        * --- Trovata la stringa da sostituire
        ARRAYCNF(this.w_POSIZIONECORRENTE) = this.w_VARIABILE + " = " + this.w_VALORE + this.w_ACAPO
        this.w_TROVATO = .T.
      endif
      this.w_POSIZIONECORRENTE = this.w_POSIZIONECORRENTE + 1
    enddo
    if NOT this.w_TROVATO
      * --- Se nell'ultimo elemento non c'� l'invio a capo lo aggiunge
      if NOT this.w_ACAPO $ ARRAYCNF(this.w_LUNGHEZZA)
        ARRAYCNF(this.w_LUNGHEZZA) = ARRAYCNF(this.w_LUNGHEZZA) + this.w_ACAPO
      endif
      * --- Aggiunge una riga
      DIMENSION ARRAYCNF( this.w_LUNGHEZZA+1)
      ARRAYCNF( this.w_LUNGHEZZA+1) = this.w_VARIABILE + " = " + this.w_VALORE + this.w_ACAPO
    endif
    this.w_TROVATO = .F.
  endproc


  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
