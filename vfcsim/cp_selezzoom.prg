* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_selezzoom                                                    *
*              Seleziona utenti/gruppi                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-08                                                      *
* Last revis.: 2008-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_selezzoom",oParentObject,m.w_PARAM)
return(i_retval)

define class tcp_selezzoom as StdBatch
  * --- Local variables
  w_PARAM = space(3)
  w_DETTAGLIO = .NULL.
  w_ZOOM = .NULL.
  w_VUOTO = .f.
  w_AbsRow = 0
  w_RECPOS = 0
  w_nRelRow = 0
  w_CODE = 0
  * --- WorkFile variables
  cpgroups_idx=0
  cpusers_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_DETTAGLIO = this.oParentObject.oParentObject
    if this.w_PARAM="USR"
      this.w_ZOOM = this.oParentObject.w_UsrZoom
    else
      this.w_ZOOM = this.oParentObject.w_GrpZoom
    endif
    * --- Assegno posizione per ritorno sul fondo dei controlli
    this.w_AbsRow = this.w_DETTAGLIO.oPgFrm.Page1.oPag.oBody.nAbsRow
    this.w_nRelRow = this.w_DETTAGLIO.oPgFrm.Page1.oPag.oBody.nRelRow
    SELECT (this.w_DETTAGLIO.cTrsName)
    this.w_RECPOS = IIF( Eof() , RECNO(this.w_DETTAGLIO.cTrsName)-1 , RECNO(this.w_DETTAGLIO.cTrsName) )
    SELECT (this.w_DETTAGLIO.cTrsName) 
 Go bottom
    do while deleted() and !bof() 
      skip -1
    enddo
    * --- Se si � posizionato su un record vuoto allora pu� essere sovrascritto
    SELECT (this.w_DETTAGLIO.cTrsName)
    if NVL(GRPCODE,0)<0 and NVL(USRCODE,0)<0
      this.w_VUOTO = .T.
    endif
    * --- Aggiorno la visualizzazione
    SELECT (this.w_DETTAGLIO.cTrsName) 
 With this.w_DETTAGLIO 
 .WorkFromTrs() 
 .mCalcRowObjs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 EndWith
    Select (this.w_ZOOM.cCursor) 
 Go Top
    Scan For XCHK = 1
    * --- Verifica che il record non sia gi� presente usl transitorio
    this.w_CODE = CODE
    if this.w_PARAM="USR"
      SELECT COUNT (*) from (this.w_DETTAGLIO.cTrsName) where NVL (USRCODE,0) = NVL (this.w_CODE,0) AND NOT DELETED() into array CONTA
    else
      SELECT COUNT (*) from (this.w_DETTAGLIO.cTrsName) where NVL (GRPCODE,0) = NVL (this.w_CODE,0) AND NOT DELETED() into array CONTA
    endif
    * --- Se non � gia presente sul transitorio scrivo il nuovo record
    if CONTA=0
      if NOT this.w_VUOTO
        this.w_DETTAGLIO.InitRow()     
      else
        this.w_VUOTO = .F.
      endif
      if this.w_PARAM="USR"
        this.w_DETTAGLIO.w_USRCODE = this.w_CODE
        this.w_DETTAGLIO.w_GRPCODE = -1
      else
        this.w_DETTAGLIO.w_GRPCODE = this.w_CODE
        this.w_DETTAGLIO.w_USRCODE = -1
      endif
      this.w_DETTAGLIO.w_XGRPCODE = iif(this.w_DETTAGLIO.w_GRPCODE=-1, 0, this.w_DETTAGLIO.w_GRPCODE)
      this.w_DETTAGLIO.w_XUSRCODE = iif(this.w_DETTAGLIO.w_USRCODE=-1, 0, this.w_DETTAGLIO.w_USRCODE)
      * --- Read from cpgroups
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.cpgroups_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2],.t.,this.cpgroups_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "name"+;
          " from "+i_cTable+" cpgroups where ";
              +"code = "+cp_ToStrODBC(this.w_DETTAGLIO.w_XGRPCODE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          name;
          from (i_cTable) where;
              code = this.w_DETTAGLIO.w_XGRPCODE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DETTAGLIO.w_namegrp = NVL(cp_ToDate(_read_.name),cp_NullValue(_read_.name))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from cpusers
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.cpusers_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2],.t.,this.cpusers_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "name"+;
          " from "+i_cTable+" cpusers where ";
              +"code = "+cp_ToStrODBC(this.w_DETTAGLIO.w_XUSRCODE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          name;
          from (i_cTable) where;
              code = this.w_DETTAGLIO.w_XUSRCODE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DETTAGLIO.w_nameusr = NVL(cp_ToDate(_read_.name),cp_NullValue(_read_.name))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DETTAGLIO.TrsFromWork()     
    else
      CONTA=0
    endif
    Select (this.w_ZOOM.cCursor)
    EndScan
    SELECT (this.w_DETTAGLIO.cTrsName)
    GO this.w_RECPOS
    With this.w_DETTAGLIO
    .WorkFromTrs()
    .SaveDependsOn()
    .SetControlsValue()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    EndWith
    this.w_DETTAGLIO.bUpdated = .t.
    this.oParentObject.ecpSave()
    this.w_DETTAGLIO.refresh()     
    this.w_DETTAGLIO.mcalc(.t.)     
  endproc


  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='cpgroups'
    this.cWorkTables[2]='cpusers'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
