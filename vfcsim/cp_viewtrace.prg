* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_viewtrace                                                    *
*              Activity logger viewer                                          *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-23                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_viewtrace
If i_ServerConn[1,2]=0
   cp_ErrorMsg(cp_translate("Activity Logger non disponibile per database Visual FoxPro"))
   return
EndIf
* --- Fine Area Manuale
return(createobject("tcp_viewtrace",oParentObject))

* --- Class definition
define class tcp_viewtrace as StdForm
  Top    = 4
  Left   = 3

  * --- Standard Properties
  Width  = 795
  Height = 539+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-07"
  HelpContextID=37257243
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=46

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  cpusers_IDX = 0
  cPrg = "cp_viewtrace"
  cComment = "Activity logger viewer"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CARTELLALOG = space(250)
  w_ALCMDSQL = space(0)
  w_NOMECURSORE = space(10)
  w_DATI = space(10)
  w_NOMECURSOREDATI = space(10)
  w_FILEDILOG = space(250)
  w_FILT_DBR = space(1)
  w_FILT_DBW = space(1)
  w_FILT_DBI = space(1)
  w_FILT_DBD = space(1)
  w_FILT_VQR = space(1)
  w_FILT_GSO = space(1)
  w_FILT_GSD = space(1)
  w_FILT_BTO = space(1)
  w_FILT_BTD = space(1)
  w_FILT_FCO = space(1)
  w_FILT_FCD = space(1)
  w_FILT_TASTIFUN = space(1)
  w_FILT_EVENT = space(1)
  w_FILT_DBM = space(1)
  w_FILT_DBT = space(1)
  w_FILT_DBX = space(1)
  w_FILT_TBT = space(1)
  w_FILT_TCT = space(1)
  w_FILT_TRT = space(1)
  w_FILT_ASI = space(1)
  w_FILT_RIC = space(1)
  w_FILT_DEA = space(1)
  w_RIGHE = 0
  w_MILLISECONDI = 0
  w_AZIENDA = space(5)
  w_ONLYERR = space(1)
  w_ONLYHOST = space(1)
  w_CONTENUTOINFRASESQL = space(250)
  w_TROVASUCCESSIVO = .F.
  w_CARTELLALOG = space(250)
  w_HOST = space(100)
  w_AZRAGAZI = space(40)
  w_UTENTE = 0
  w_NOMEUTENTE = space(40)
  w_HOSTNAME = space(150)
  w_ZOOMPIENO = .F.
  w_BFUNC = .F.
  w_BPROC = .F.
  w_BGEST = .F.
  w_VOID = .F.
  w_ELENCO = .NULL.
  w_ELENCO2 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_viewtrace
  w_ATTIVAZIONEPROVVISORIA = .F.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_viewtracePag1","cp_viewtrace",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Traccia")
      .Pages(2).addobject("oPag","tcp_viewtracePag2","cp_viewtrace",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_viewtrace
       if i_nACTIVATEPROFILER = 0
          i_nACTIVATEPROFILER = 1
          cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UEV", "Attivazione temporanea", Space(10), 0, 0, "")
          THIS.parent.w_ATTIVAZIONEPROVVISORIA = .T.
       ENDIF
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ELENCO = this.oPgFrm.Pages(1).oPag.ELENCO
    this.w_ELENCO2 = this.oPgFrm.Pages(1).oPag.ELENCO2
    DoDefault()
    proc Destroy()
      this.w_ELENCO = .NULL.
      this.w_ELENCO2 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='cpusers'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CARTELLALOG=space(250)
      .w_ALCMDSQL=space(0)
      .w_NOMECURSORE=space(10)
      .w_DATI=space(10)
      .w_NOMECURSOREDATI=space(10)
      .w_FILEDILOG=space(250)
      .w_FILT_DBR=space(1)
      .w_FILT_DBW=space(1)
      .w_FILT_DBI=space(1)
      .w_FILT_DBD=space(1)
      .w_FILT_VQR=space(1)
      .w_FILT_GSO=space(1)
      .w_FILT_GSD=space(1)
      .w_FILT_BTO=space(1)
      .w_FILT_BTD=space(1)
      .w_FILT_FCO=space(1)
      .w_FILT_FCD=space(1)
      .w_FILT_TASTIFUN=space(1)
      .w_FILT_EVENT=space(1)
      .w_FILT_DBM=space(1)
      .w_FILT_DBT=space(1)
      .w_FILT_DBX=space(1)
      .w_FILT_TBT=space(1)
      .w_FILT_TCT=space(1)
      .w_FILT_TRT=space(1)
      .w_FILT_ASI=space(1)
      .w_FILT_RIC=space(1)
      .w_FILT_DEA=space(1)
      .w_RIGHE=0
      .w_MILLISECONDI=0
      .w_AZIENDA=space(5)
      .w_ONLYERR=space(1)
      .w_ONLYHOST=space(1)
      .w_CONTENUTOINFRASESQL=space(250)
      .w_TROVASUCCESSIVO=.f.
      .w_CARTELLALOG=space(250)
      .w_HOST=space(100)
      .w_AZRAGAZI=space(40)
      .w_UTENTE=0
      .w_NOMEUTENTE=space(40)
      .w_HOSTNAME=space(150)
      .w_ZOOMPIENO=.f.
      .w_BFUNC=.f.
      .w_BPROC=.f.
      .w_BGEST=.f.
      .w_VOID=.f.
          .DoRTCalc(1,1,.f.)
        .w_ALCMDSQL = IIF( .w_ELENCO.VISIBLE, .w_ELENCO.GETVAR("ALCMDSQL"), .w_ELENCO2.GETVAR("ALCMDSQL") )
      .oPgFrm.Page1.oPag.ELENCO.Calculate()
          .DoRTCalc(3,3,.f.)
        .w_DATI = IIF(inlist( i_nACTIVATEPROFILER, 1, 3), .w_ELENCO.GETVAR("ALCRDATI"), .w_ELENCO2.GETVAR("ALCRDATI") )
        .w_NOMECURSOREDATI = ADDBS( .w_CARTELLALOG ) + NVL( .w_DATI , "")
          .DoRTCalc(6,6,.f.)
        .w_FILT_DBR = 'S'
        .w_FILT_DBW = 'S'
        .w_FILT_DBI = 'S'
        .w_FILT_DBD = 'S'
        .w_FILT_VQR = 'S'
        .w_FILT_GSO = 'S'
        .w_FILT_GSD = 'S'
        .w_FILT_BTO = 'S'
        .w_FILT_BTD = 'S'
        .w_FILT_FCO = 'S'
        .w_FILT_FCD = 'S'
        .w_FILT_TASTIFUN = 'S'
        .w_FILT_EVENT = 'S'
        .w_FILT_DBM = 'S'
        .w_FILT_DBT = 'S'
        .w_FILT_DBX = 'S'
        .w_FILT_TBT = 'S'
        .w_FILT_TCT = 'S'
        .w_FILT_TRT = 'S'
        .w_FILT_ASI = 'S'
        .w_FILT_RIC = 'S'
        .w_FILT_DEA = 'S'
        .DoRTCalc(29,31,.f.)
        if not(empty(.w_AZIENDA))
          .link_2_33('Full')
        endif
        .w_ONLYERR = 'N'
        .w_ONLYHOST = 'N'
          .DoRTCalc(34,34,.f.)
        .w_TROVASUCCESSIVO = .F.
      .oPgFrm.Page1.oPag.ELENCO2.Calculate()
          .DoRTCalc(36,36,.f.)
        .w_HOST = SYS( 0 )
        .DoRTCalc(38,39,.f.)
        if not(empty(.w_UTENTE))
          .link_2_51('Full')
        endif
          .DoRTCalc(40,40,.f.)
        .w_HOSTNAME = IIF( inlist( i_nACTIVATEPROFILER, 1, 3), .w_ELENCO.GETVAR("AL__HOST"), .w_ELENCO2.GETVAR("AL__HOST") )
        .w_ZOOMPIENO = .F.
        .w_BFUNC = .w_FILT_FCO="S" AND .w_FILT_FCD="S"
        .w_BPROC = .w_FILT_BTO="S" AND .w_FILT_BTD="S"
        .w_BGEST = .w_FILT_GSO="S" AND .w_FILT_GSD="S"
    endwith
    this.DoRTCalc(46,46,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_27.enabled = this.oPgFrm.Page2.oPag.oBtn_2_27.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_28.enabled = this.oPgFrm.Page2.oPag.oBtn_2_28.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_29.enabled = this.oPgFrm.Page2.oPag.oBtn_2_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_viewtrace
    THIS.W_ELENCO.bNoMenuFunction = .T.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_ALCMDSQL = IIF( .w_ELENCO.VISIBLE, .w_ELENCO.GETVAR("ALCMDSQL"), .w_ELENCO2.GETVAR("ALCMDSQL") )
        .oPgFrm.Page1.oPag.ELENCO.Calculate()
        .DoRTCalc(3,3,.t.)
            .w_DATI = IIF(inlist( i_nACTIVATEPROFILER, 1, 3), .w_ELENCO.GETVAR("ALCRDATI"), .w_ELENCO2.GETVAR("ALCRDATI") )
            .w_NOMECURSOREDATI = ADDBS( .w_CARTELLALOG ) + NVL( .w_DATI , "")
        .oPgFrm.Page1.oPag.ELENCO2.Calculate()
        .DoRTCalc(6,40,.t.)
            .w_HOSTNAME = IIF( inlist( i_nACTIVATEPROFILER, 1, 3), .w_ELENCO.GETVAR("AL__HOST"), .w_ELENCO2.GETVAR("AL__HOST") )
        .DoRTCalc(42,42,.t.)
            .w_BFUNC = .w_FILT_FCO="S" AND .w_FILT_FCD="S"
            .w_BPROC = .w_FILT_BTO="S" AND .w_FILT_BTD="S"
            .w_BGEST = .w_FILT_GSO="S" AND .w_FILT_GSD="S"
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(46,46,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ELENCO.Calculate()
        .oPgFrm.Page1.oPag.ELENCO2.Calculate()
    endwith
  return

  proc Calculate_YNIJLJXNGG()
    with this
          * --- CALLING
          .w_VOID = CALLING(.w_ELENCO2.cCursor, .w_bFunc, .w_bProc, .w_bGest)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_1.visible=!this.oPgFrm.Page2.oPag.oStr_2_1.mHide()
    this.oPgFrm.Page2.oPag.oFILEDILOG_2_2.visible=!this.oPgFrm.Page2.oPag.oFILEDILOG_2_2.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_3.visible=!this.oPgFrm.Page2.oPag.oBtn_2_3.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_4.visible=!this.oPgFrm.Page2.oPag.oBtn_2_4.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_36.visible=!this.oPgFrm.Page2.oPag.oBtn_2_36.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_37.visible=!this.oPgFrm.Page2.oPag.oBtn_2_37.mHide()
    this.oPgFrm.Page2.oPag.oCARTELLALOG_2_44.visible=!this.oPgFrm.Page2.oPag.oCARTELLALOG_2_44.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_15.visible=!this.oPgFrm.Page1.oPag.oBtn_1_15.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_45.visible=!this.oPgFrm.Page2.oPag.oStr_2_45.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- cp_viewtrace
    if cEvent = "Init"
       if EMPTY( THIS.w_CARTELLALOG )
          THIS.w_CARTELLALOG=ADDBS(i_cACTIVATEPROFILER_POSIZIONELOG)
       endif
    
       if NOT inlist( i_nACTIVATEPROFILER, 1, 3)
          * Disattiva l'elenco da DBF
          THIS.W_ELENCO.VISIBLE=.F.
          THIS.W_ELENCO.ENABLED=.F.
       ENDIF
    
       if NOT inlist( i_nACTIVATEPROFILER, 5, 7 )
          * Disattiva l'elenco da database
          THIS.W_ELENCO2.VISIBLE=.F.
          THIS.W_ELENCO2.ENABLED=.F.
       ENDIF
    ENdif
    
    if cEvent = "PopolaZoom" OR cEvent = "Popola2Zoom"
        THIS.w_TROVASUCCESSIVO = .F.
        THIS.mEnableControls()
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ELENCO.Event(cEvent)
      .oPgFrm.Page1.oPag.ELENCO2.Event(cEvent)
        if lower(cEvent)==lower("w_ELENCO2 after query")
          .Calculate_YNIJLJXNGG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- cp_viewtrace
    *visualizza la pagina 1
    if inlist(cevent, 'PopolaZoom', 'Popola2Zoom', 'Init')
        this.opgfrm.activepage=1
        IF this.w_ELENCO.visible
          this.w_ZOOMPIENO=RECCOUNT(this.w_ELENCO.cCursor)>0
          if cEvent = "Init"
            this.NotifyEvent("PopolaZoom")
          Endif
        ENDIF
        IF this.w_ELENCO2.visible
          this.w_ZOOMPIENO=RECCOUNT(this.w_ELENCO2.cCursor)>0
          if cEvent = "Init"
            this.NotifyEvent("Popola2Zoom")
          Endif
        ENDIF
        this.mCalc(.T.)
        this.mEnableControls()
    ENDIF
    
    if cEvent = "Done"
       if THIS.w_ATTIVAZIONEPROVVISORIA
          i_nACTIVATEPROFILER = 0
       ENDIF
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_2_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_AZIENDA)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_AZIENDA))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZIENDA)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZIENDA) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oAZIENDA_2_33'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZRAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_AZRAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTENTE
  func Link_2_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_UTENTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_UTENTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTENTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oUTENTE_2_51'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_UTENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_UTENTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTENTE = NVL(_Link_.code,0)
      this.w_NOMEUTENTE = NVL(_Link_.name,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_UTENTE = 0
      endif
      this.w_NOMEUTENTE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oALCMDSQL_1_2.value==this.w_ALCMDSQL)
      this.oPgFrm.Page1.oPag.oALCMDSQL_1_2.value=this.w_ALCMDSQL
    endif
    if not(this.oPgFrm.Page2.oPag.oFILEDILOG_2_2.value==this.w_FILEDILOG)
      this.oPgFrm.Page2.oPag.oFILEDILOG_2_2.value=this.w_FILEDILOG
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_DBR_2_5.RadioValue()==this.w_FILT_DBR)
      this.oPgFrm.Page2.oPag.oFILT_DBR_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_DBW_2_6.RadioValue()==this.w_FILT_DBW)
      this.oPgFrm.Page2.oPag.oFILT_DBW_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_DBI_2_7.RadioValue()==this.w_FILT_DBI)
      this.oPgFrm.Page2.oPag.oFILT_DBI_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_DBD_2_8.RadioValue()==this.w_FILT_DBD)
      this.oPgFrm.Page2.oPag.oFILT_DBD_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_VQR_2_9.RadioValue()==this.w_FILT_VQR)
      this.oPgFrm.Page2.oPag.oFILT_VQR_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_GSO_2_10.RadioValue()==this.w_FILT_GSO)
      this.oPgFrm.Page2.oPag.oFILT_GSO_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_GSD_2_11.RadioValue()==this.w_FILT_GSD)
      this.oPgFrm.Page2.oPag.oFILT_GSD_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_BTO_2_12.RadioValue()==this.w_FILT_BTO)
      this.oPgFrm.Page2.oPag.oFILT_BTO_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_BTD_2_13.RadioValue()==this.w_FILT_BTD)
      this.oPgFrm.Page2.oPag.oFILT_BTD_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_FCO_2_14.RadioValue()==this.w_FILT_FCO)
      this.oPgFrm.Page2.oPag.oFILT_FCO_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_FCD_2_15.RadioValue()==this.w_FILT_FCD)
      this.oPgFrm.Page2.oPag.oFILT_FCD_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_TASTIFUN_2_16.RadioValue()==this.w_FILT_TASTIFUN)
      this.oPgFrm.Page2.oPag.oFILT_TASTIFUN_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_EVENT_2_17.RadioValue()==this.w_FILT_EVENT)
      this.oPgFrm.Page2.oPag.oFILT_EVENT_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_DBM_2_18.RadioValue()==this.w_FILT_DBM)
      this.oPgFrm.Page2.oPag.oFILT_DBM_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_DBT_2_19.RadioValue()==this.w_FILT_DBT)
      this.oPgFrm.Page2.oPag.oFILT_DBT_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_DBX_2_20.RadioValue()==this.w_FILT_DBX)
      this.oPgFrm.Page2.oPag.oFILT_DBX_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_TBT_2_22.RadioValue()==this.w_FILT_TBT)
      this.oPgFrm.Page2.oPag.oFILT_TBT_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_TCT_2_23.RadioValue()==this.w_FILT_TCT)
      this.oPgFrm.Page2.oPag.oFILT_TCT_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_TRT_2_24.RadioValue()==this.w_FILT_TRT)
      this.oPgFrm.Page2.oPag.oFILT_TRT_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_ASI_2_25.RadioValue()==this.w_FILT_ASI)
      this.oPgFrm.Page2.oPag.oFILT_ASI_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_RIC_2_26.RadioValue()==this.w_FILT_RIC)
      this.oPgFrm.Page2.oPag.oFILT_RIC_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILT_DEA_2_30.RadioValue()==this.w_FILT_DEA)
      this.oPgFrm.Page2.oPag.oFILT_DEA_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRIGHE_2_31.value==this.w_RIGHE)
      this.oPgFrm.Page2.oPag.oRIGHE_2_31.value=this.w_RIGHE
    endif
    if not(this.oPgFrm.Page2.oPag.oMILLISECONDI_2_32.value==this.w_MILLISECONDI)
      this.oPgFrm.Page2.oPag.oMILLISECONDI_2_32.value=this.w_MILLISECONDI
    endif
    if not(this.oPgFrm.Page2.oPag.oAZIENDA_2_33.value==this.w_AZIENDA)
      this.oPgFrm.Page2.oPag.oAZIENDA_2_33.value=this.w_AZIENDA
    endif
    if not(this.oPgFrm.Page2.oPag.oONLYERR_2_34.RadioValue()==this.w_ONLYERR)
      this.oPgFrm.Page2.oPag.oONLYERR_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oONLYHOST_2_35.RadioValue()==this.w_ONLYHOST)
      this.oPgFrm.Page2.oPag.oONLYHOST_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCONTENUTOINFRASESQL_2_40.value==this.w_CONTENUTOINFRASESQL)
      this.oPgFrm.Page2.oPag.oCONTENUTOINFRASESQL_2_40.value=this.w_CONTENUTOINFRASESQL
    endif
    if not(this.oPgFrm.Page2.oPag.oCARTELLALOG_2_44.value==this.w_CARTELLALOG)
      this.oPgFrm.Page2.oPag.oCARTELLALOG_2_44.value=this.w_CARTELLALOG
    endif
    if not(this.oPgFrm.Page2.oPag.oAZRAGAZI_2_49.value==this.w_AZRAGAZI)
      this.oPgFrm.Page2.oPag.oAZRAGAZI_2_49.value=this.w_AZRAGAZI
    endif
    if not(this.oPgFrm.Page2.oPag.oUTENTE_2_51.value==this.w_UTENTE)
      this.oPgFrm.Page2.oPag.oUTENTE_2_51.value=this.w_UTENTE
    endif
    if not(this.oPgFrm.Page2.oPag.oNOMEUTENTE_2_52.value==this.w_NOMEUTENTE)
      this.oPgFrm.Page2.oPag.oNOMEUTENTE_2_52.value=this.w_NOMEUTENTE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(NOT " " $ ALLTRIM( .w_FILEDILOG ))  and not(inlist( i_nACTIVATEPROFILER, 5, 7))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFILEDILOG_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un percorso e un nome di file che non contengano spazi")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_viewtracePag1 as StdContainer
  Width  = 791
  height = 541
  stdWidth  = 791
  stdheight = 541
  resizeXpos=344
  resizeYpos=231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oALCMDSQL_1_2 as StdMemo with uid="ZWWLMXPBDG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ALCMDSQL", cQueryName = "ALCMDSQL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 243787634,;
   bGlobalFont=.t.,;
    Height=75, Width=630, Left=1, Top=462, ReadOnly=.T., enabled=.T.


  add object ELENCO as cp_zoombox_log with uid="OPFFMBUHES",left=-3, top=10, width=799,height=431,;
    caption='Object',;
   bGlobalFont=.t.,;
    bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,cMenuFile="",cZoomOnZoom="",cTable="TMP_DBFLOG",bRetriveAllRows=.f.,cZoomFile="CP_VIEWTRACE",;
    cEvent = "PopolaZoom",;
    nPag=1;
    , HelpContextID = 183043462


  add object oBtn_1_7 as StdButton with uid="VDPQDABGNJ",left=637, top=446, width=48,height=45,;
    CpPicture="REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Cerca la riga con la frase sql o il messaggio di errore contenente l'espressione richiesta a video dopo la pressione del pulsante";
    , HelpContextID = 206371434;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ZOOMPIENO)
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="JWUPQWSTHH",left=637, top=496, width=48,height=45,;
    CpPicture="REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Cerca la riga successiva con la frase sql o il messaggio di errore contenente l'espressione specificata";
    , HelpContextID = 14210664;
    , caption='\<Succes.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TROVASUCCESSIVO AND .w_ZOOMPIENO)
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="PLRGXQKEQF",left=689, top=446, width=48,height=45,;
    CpPicture="VISUALIZ.bmp", caption="", nPag=1;
    , ToolTipText = "Mostra il risultato dell'istruzione eseguita";
    , HelpContextID = 57049166;
    , caption='\<SQL';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT EMPTY( .w_DATI ))
     endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="HRVAJKMQXI",left=689, top=446, width=48,height=45,;
    CpPicture="VISUALIZ.bmp", caption="", nPag=1;
    , ToolTipText = "Apre la maschera per le istruzioni sql";
    , HelpContextID = 57049166;
    , caption='\<Dati';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY( .w_DATI ))
     endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="HKYUBHHNJZ",left=689, top=495, width=48,height=46,;
    CpPicture="new_interrogT.bmp", caption="", nPag=1;
    , ToolTipText = "Azzera il log";
    , HelpContextID = 63510422;
    , caption='\<Azzera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained,"Z")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ZOOMPIENO)
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="OLNMHXADLC",left=742, top=446, width=48,height=45,;
    CpPicture="REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Ripete l'interrogazione";
    , HelpContextID = 236355557;
    , caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        .NotifyEvent("PopolaZoom")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT inlist( i_nACTIVATEPROFILER, 1, 3))
     endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="UXNSLOLVTR",left=742, top=446, width=48,height=45,;
    CpPicture="REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Ripete l'interrogazione";
    , HelpContextID = 236355557;
    , caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        .NotifyEvent("Popola2Zoom")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT inlist( i_nACTIVATEPROFILER, 5, 7))
     endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="PTAFAFGHOT",left=742, top=495, width=48,height=46,;
    CpPicture="ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Chiude la maschera";
    , HelpContextID = 91652174;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ELENCO2 as cp_zoombox with uid="ZKHPBNYNMI",left=-3, top=10, width=799,height=431,;
    caption='Object',;
   bGlobalFont=.t.,;
    bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,cMenuFile="",cZoomOnZoom="",cTable="SHOW_LOG",bRetriveAllRows=.f.,cZoomFile="CP_VIEWTRACE2",bNoZoomGridShape=.f.,;
    cEvent = "Popola2Zoom",;
    nPag=1;
    , HelpContextID = 183043462

  add object oStr_1_12 as StdString with uid="EWTEYNTLSQ",Visible=.t., Left=2, Top=443,;
    Alignment=0, Width=133, Height=18,;
    Caption="Comando SQL"  ;
  , bGlobalFont=.t.
enddefine
define class tcp_viewtracePag2 as StdContainer
  Width  = 791
  height = 541
  stdWidth  = 791
  stdheight = 541
  resizeXpos=675
  resizeYpos=321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFILEDILOG_2_2 as StdField with uid="PPVUSBZQTE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FILEDILOG", cQueryName = "FILEDILOG",;
    bObbl = .f. , nPag = 2, value=space(250), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un percorso e un nome di file che non contengano spazi",;
    HelpContextID = 263549590,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=136, Top=10, InputMask=replicate('X',250), bHasZoom = .t. 

  func oFILEDILOG_2_2.mHide()
    with this.Parent.oContained
      return (inlist( i_nACTIVATEPROFILER, 5, 7))
    endwith
  endfunc

  func oFILEDILOG_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT " " $ ALLTRIM( .w_FILEDILOG ))
    endwith
    return bRes
  endfunc

  proc oFILEDILOG_2_2.mZoom
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained, "F" )
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_2_3 as StdButton with uid="JXCWSUOJYP",left=688, top=5, width=50,height=45,;
    CpPicture="aperturafile.ico", caption="", nPag=2;
    , ToolTipText = "Trasferisce log sul database";
    , HelpContextID = 44238410;
    , caption='\<Apre dbf';
    , FontName = "Arial", FontSize = 6, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      do cp_addlog with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_3.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (inlis( i_nActivateProfiler, 1, 3 ))
     endwith
    endif
  endfunc


  add object oBtn_2_4 as StdButton with uid="CSQULHGOGN",left=741, top=5, width=47,height=45,;
    CpPicture="SAVE.bmp", caption="", nPag=2;
    , ToolTipText = "Trasferisce log su DBF";
    , HelpContextID = 81861013;
    , caption='\<Salva dbf';
    , FontName = "Arial", FontSize = 6, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_4.Click()
      do cp_savelog with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_4.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (inlis( i_nActivateProfiler, 1, 3 ))
     endwith
    endif
  endfunc

  add object oFILT_DBR_2_5 as StdCheck with uid="CPFRTCCADS",rtseq=7,rtrep=.f.,left=29, top=84, caption="Select",;
    ToolTipText = "Visualizza frasi select SQL",;
    HelpContextID = 69408215,;
    cFormVar="w_FILT_DBR", bObbl = .f. , nPag = 2;
    , backcolor=4145087;
   , bGlobalFont=.t.


  func oFILT_DBR_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_DBR_2_5.GetRadio()
    this.Parent.oContained.w_FILT_DBR = this.RadioValue()
    return .t.
  endfunc

  func oFILT_DBR_2_5.SetRadio()
    this.Parent.oContained.w_FILT_DBR=trim(this.Parent.oContained.w_FILT_DBR)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_DBR=='S',1,;
      0)
  endfunc

  add object oFILT_DBW_2_6 as StdCheck with uid="BTMMWBCGQO",rtseq=8,rtrep=.f.,left=29, top=110, caption="Update",;
    ToolTipText = "Visualizza frasi update SQL",;
    HelpContextID = 69080535,;
    cFormVar="w_FILT_DBW", bObbl = .f. , nPag = 2;
    , backcolor=4145087;
   , bGlobalFont=.t.


  func oFILT_DBW_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_DBW_2_6.GetRadio()
    this.Parent.oContained.w_FILT_DBW = this.RadioValue()
    return .t.
  endfunc

  func oFILT_DBW_2_6.SetRadio()
    this.Parent.oContained.w_FILT_DBW=trim(this.Parent.oContained.w_FILT_DBW)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_DBW=='S',1,;
      0)
  endfunc

  add object oFILT_DBI_2_7 as StdCheck with uid="SNHRIQSKCP",rtseq=9,rtrep=.f.,left=29, top=136, caption="Insert",;
    ToolTipText = "Visualizza frasi insert SQL",;
    HelpContextID = 69998039,;
    cFormVar="w_FILT_DBI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_DBI_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_DBI_2_7.GetRadio()
    this.Parent.oContained.w_FILT_DBI = this.RadioValue()
    return .t.
  endfunc

  func oFILT_DBI_2_7.SetRadio()
    this.Parent.oContained.w_FILT_DBI=trim(this.Parent.oContained.w_FILT_DBI)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_DBI=='S',1,;
      0)
  endfunc

  add object oFILT_DBD_2_8 as StdCheck with uid="LKEHYFCFRE",rtseq=10,rtrep=.f.,left=29, top=162, caption="Delete",;
    ToolTipText = "Visualizza frasi delete SQL",;
    HelpContextID = 70325719,;
    cFormVar="w_FILT_DBD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_DBD_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_DBD_2_8.GetRadio()
    this.Parent.oContained.w_FILT_DBD = this.RadioValue()
    return .t.
  endfunc

  func oFILT_DBD_2_8.SetRadio()
    this.Parent.oContained.w_FILT_DBD=trim(this.Parent.oContained.w_FILT_DBD)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_DBD=='S',1,;
      0)
  endfunc

  add object oFILT_VQR_2_9 as StdCheck with uid="EOIEVJSAQD",rtseq=11,rtrep=.f.,left=29, top=188, caption="VQR",;
    ToolTipText = "Visualizza esecuzioni di visual query",;
    HelpContextID = 69342167,;
    cFormVar="w_FILT_VQR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_VQR_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_VQR_2_9.GetRadio()
    this.Parent.oContained.w_FILT_VQR = this.RadioValue()
    return .t.
  endfunc

  func oFILT_VQR_2_9.SetRadio()
    this.Parent.oContained.w_FILT_VQR=trim(this.Parent.oContained.w_FILT_VQR)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_VQR=='S',1,;
      0)
  endfunc

  add object oFILT_GSO_2_10 as StdCheck with uid="SDOQPCEGOO",rtseq=12,rtrep=.f.,left=227, top=84, caption="Apertura gestione",;
    ToolTipText = "Visualizza l'apertura a video dei form",;
    HelpContextID = 69534423,;
    cFormVar="w_FILT_GSO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_GSO_2_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_GSO_2_10.GetRadio()
    this.Parent.oContained.w_FILT_GSO = this.RadioValue()
    return .t.
  endfunc

  func oFILT_GSO_2_10.SetRadio()
    this.Parent.oContained.w_FILT_GSO=trim(this.Parent.oContained.w_FILT_GSO)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_GSO=='S',1,;
      0)
  endfunc

  add object oFILT_GSD_2_11 as StdCheck with uid="NNIOVSGXJL",rtseq=13,rtrep=.f.,left=227, top=110, caption="Chiusura gestione",;
    ToolTipText = "Visualizza la chiusura dei form",;
    HelpContextID = 70255319,;
    cFormVar="w_FILT_GSD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_GSD_2_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_GSD_2_11.GetRadio()
    this.Parent.oContained.w_FILT_GSD = this.RadioValue()
    return .t.
  endfunc

  func oFILT_GSD_2_11.SetRadio()
    this.Parent.oContained.w_FILT_GSD=trim(this.Parent.oContained.w_FILT_GSD)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_GSD=='S',1,;
      0)
  endfunc

  add object oFILT_BTO_2_12 as StdCheck with uid="AUZKUQFIIH",rtseq=14,rtrep=.f.,left=227, top=136, caption="Lancio routine",;
    ToolTipText = "Visualizza l'avvio delle routine",;
    HelpContextID = 69531607,;
    cFormVar="w_FILT_BTO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_BTO_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_BTO_2_12.GetRadio()
    this.Parent.oContained.w_FILT_BTO = this.RadioValue()
    return .t.
  endfunc

  func oFILT_BTO_2_12.SetRadio()
    this.Parent.oContained.w_FILT_BTO=trim(this.Parent.oContained.w_FILT_BTO)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_BTO=='S',1,;
      0)
  endfunc

  add object oFILT_BTD_2_13 as StdCheck with uid="HEVDXZKNQR",rtseq=15,rtrep=.f.,left=227, top=162, caption="Chiusura routine",;
    ToolTipText = "Visualizza la chiusura delle routine",;
    HelpContextID = 70252503,;
    cFormVar="w_FILT_BTD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_BTD_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_BTD_2_13.GetRadio()
    this.Parent.oContained.w_FILT_BTD = this.RadioValue()
    return .t.
  endfunc

  func oFILT_BTD_2_13.SetRadio()
    this.Parent.oContained.w_FILT_BTD=trim(this.Parent.oContained.w_FILT_BTD)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_BTD=='S',1,;
      0)
  endfunc

  add object oFILT_FCO_2_14 as StdCheck with uid="OTBKEAZSLB",rtseq=16,rtrep=.f.,left=227, top=188, caption="Lancio funzione",;
    ToolTipText = "Visualizza l'avvio delle funzioni",;
    HelpContextID = 69600215,;
    cFormVar="w_FILT_FCO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_FCO_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_FCO_2_14.GetRadio()
    this.Parent.oContained.w_FILT_FCO = this.RadioValue()
    return .t.
  endfunc

  func oFILT_FCO_2_14.SetRadio()
    this.Parent.oContained.w_FILT_FCO=trim(this.Parent.oContained.w_FILT_FCO)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_FCO=='S',1,;
      0)
  endfunc

  add object oFILT_FCD_2_15 as StdCheck with uid="CASNIVTLOV",rtseq=17,rtrep=.f.,left=227, top=214, caption="Chiusura funzione",;
    ToolTipText = "Visualizza la chiusura delle funzioni",;
    HelpContextID = 70321111,;
    cFormVar="w_FILT_FCD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_FCD_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_FCD_2_15.GetRadio()
    this.Parent.oContained.w_FILT_FCD = this.RadioValue()
    return .t.
  endfunc

  func oFILT_FCD_2_15.SetRadio()
    this.Parent.oContained.w_FILT_FCD=trim(this.Parent.oContained.w_FILT_FCD)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_FCD=='S',1,;
      0)
  endfunc

  add object oFILT_TASTIFUN_2_16 as StdCheck with uid="YEGZHGGUDW",rtseq=18,rtrep=.f.,left=227, top=240, caption="Tasti funzione",;
    ToolTipText = "Tasti funzione",;
    HelpContextID = 169733982,;
    cFormVar="w_FILT_TASTIFUN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_TASTIFUN_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_TASTIFUN_2_16.GetRadio()
    this.Parent.oContained.w_FILT_TASTIFUN = this.RadioValue()
    return .t.
  endfunc

  func oFILT_TASTIFUN_2_16.SetRadio()
    this.Parent.oContained.w_FILT_TASTIFUN=trim(this.Parent.oContained.w_FILT_TASTIFUN)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_TASTIFUN=='S',1,;
      0)
  endfunc

  add object oFILT_EVENT_2_17 as StdCheck with uid="XVWDLGRVSD",rtseq=19,rtrep=.f.,left=227, top=266, caption="Eventi",;
    ToolTipText = "Visualizza gli eventi",;
    HelpContextID = 78719785,;
    cFormVar="w_FILT_EVENT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_EVENT_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_EVENT_2_17.GetRadio()
    this.Parent.oContained.w_FILT_EVENT = this.RadioValue()
    return .t.
  endfunc

  func oFILT_EVENT_2_17.SetRadio()
    this.Parent.oContained.w_FILT_EVENT=trim(this.Parent.oContained.w_FILT_EVENT)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_EVENT=='S',1,;
      0)
  endfunc

  add object oFILT_DBM_2_18 as StdCheck with uid="YHTAORWGMF",rtseq=20,rtrep=.f.,left=498, top=84, caption="Modifica DB",;
    ToolTipText = "Visualizza frasi di ricostruzione database",;
    HelpContextID = 69735895,;
    cFormVar="w_FILT_DBM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_DBM_2_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_DBM_2_18.GetRadio()
    this.Parent.oContained.w_FILT_DBM = this.RadioValue()
    return .t.
  endfunc

  func oFILT_DBM_2_18.SetRadio()
    this.Parent.oContained.w_FILT_DBM=trim(this.Parent.oContained.w_FILT_DBM)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_DBM=='S',1,;
      0)
  endfunc

  add object oFILT_DBT_2_19 as StdCheck with uid="XTULRTLHBE",rtseq=21,rtrep=.f.,left=498, top=110, caption="Creazione temporaneo",;
    ToolTipText = "Visualizza frasi di creazione cursore lato server",;
    HelpContextID = 69277143,;
    cFormVar="w_FILT_DBT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_DBT_2_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_DBT_2_19.GetRadio()
    this.Parent.oContained.w_FILT_DBT = this.RadioValue()
    return .t.
  endfunc

  func oFILT_DBT_2_19.SetRadio()
    this.Parent.oContained.w_FILT_DBT=trim(this.Parent.oContained.w_FILT_DBT)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_DBT=='S',1,;
      0)
  endfunc

  add object oFILT_DBX_2_20 as StdCheck with uid="NFVDKRBHJZ",rtseq=22,rtrep=.f.,left=498, top=136, caption="Altre istruzioni sul DB",;
    ToolTipText = "Visualizza altre frasi eseguite dal database",;
    HelpContextID = 69014999,;
    cFormVar="w_FILT_DBX", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_DBX_2_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_DBX_2_20.GetRadio()
    this.Parent.oContained.w_FILT_DBX = this.RadioValue()
    return .t.
  endfunc

  func oFILT_DBX_2_20.SetRadio()
    this.Parent.oContained.w_FILT_DBX=trim(this.Parent.oContained.w_FILT_DBX)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_DBX=='S',1,;
      0)
  endfunc

  add object oFILT_TBT_2_22 as StdCheck with uid="VDDLEILTIF",rtseq=23,rtrep=.f.,left=498, top=162, caption="Begin transaction",;
    ToolTipText = "Visualizza le istruzioni di begin transaction",;
    HelpContextID = 69273047,;
    cFormVar="w_FILT_TBT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_TBT_2_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_TBT_2_22.GetRadio()
    this.Parent.oContained.w_FILT_TBT = this.RadioValue()
    return .t.
  endfunc

  func oFILT_TBT_2_22.SetRadio()
    this.Parent.oContained.w_FILT_TBT=trim(this.Parent.oContained.w_FILT_TBT)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_TBT=='S',1,;
      0)
  endfunc

  add object oFILT_TCT_2_23 as StdCheck with uid="XTZKHKVBWE",rtseq=24,rtrep=.f.,left=498, top=188, caption="Commit",;
    ToolTipText = "Visualizza le commit",;
    HelpContextID = 69268951,;
    cFormVar="w_FILT_TCT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_TCT_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_TCT_2_23.GetRadio()
    this.Parent.oContained.w_FILT_TCT = this.RadioValue()
    return .t.
  endfunc

  func oFILT_TCT_2_23.SetRadio()
    this.Parent.oContained.w_FILT_TCT=trim(this.Parent.oContained.w_FILT_TCT)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_TCT=='S',1,;
      0)
  endfunc

  add object oFILT_TRT_2_24 as StdCheck with uid="SHUBUUQZWL",rtseq=25,rtrep=.f.,left=498, top=214, caption="Rollback",;
    ToolTipText = "Visualizza le rollback",;
    HelpContextID = 69207511,;
    cFormVar="w_FILT_TRT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_TRT_2_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_TRT_2_24.GetRadio()
    this.Parent.oContained.w_FILT_TRT = this.RadioValue()
    return .t.
  endfunc

  func oFILT_TRT_2_24.SetRadio()
    this.Parent.oContained.w_FILT_TRT=trim(this.Parent.oContained.w_FILT_TRT)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_TRT=='S',1,;
      0)
  endfunc

  add object oFILT_ASI_2_25 as StdCheck with uid="WQCXVHKVNW",rtseq=26,rtrep=.f.,left=498, top=240, caption="Query asincrona",;
    ToolTipText = "Visualizza query asincrone (elenchi, zoom)",;
    HelpContextID = 69929175,;
    cFormVar="w_FILT_ASI", bObbl = .f. , nPag = 2;
    , backcolor=4145087;
   , bGlobalFont=.t.


  func oFILT_ASI_2_25.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_ASI_2_25.GetRadio()
    this.Parent.oContained.w_FILT_ASI = this.RadioValue()
    return .t.
  endfunc

  func oFILT_ASI_2_25.SetRadio()
    this.Parent.oContained.w_FILT_ASI=trim(this.Parent.oContained.w_FILT_ASI)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_ASI=='S',1,;
      0)
  endfunc

  add object oFILT_RIC_2_26 as StdCheck with uid="YBPEWVJVRZ",rtseq=27,rtrep=.f.,left=498, top=292, caption="Riconnessione",;
    ToolTipText = "Visualizza le riconnessioni in seguito a cadute di connessione",;
    HelpContextID = 70358999,;
    cFormVar="w_FILT_RIC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_RIC_2_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_RIC_2_26.GetRadio()
    this.Parent.oContained.w_FILT_RIC = this.RadioValue()
    return .t.
  endfunc

  func oFILT_RIC_2_26.SetRadio()
    this.Parent.oContained.w_FILT_RIC=trim(this.Parent.oContained.w_FILT_RIC)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_RIC=='S',1,;
      0)
  endfunc


  add object oBtn_2_27 as StdButton with uid="GQMUPTTTCY",left=15, top=274, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare tutti i filtri";
    , HelpContextID = 122090825;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_27.Click()
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained,"1")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_28 as StdButton with uid="ZCYHLDNLVF",left=63, top=274, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per deselezionare tutti i filtri";
    , HelpContextID = 122090825;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_28.Click()
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained,"0")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_29 as StdButton with uid="GUQBLQMZBT",left=111, top=274, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per invertire la selezione di tutti i filtri";
    , HelpContextID = 122090825;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_29.Click()
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained,"X")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFILT_DEA_2_30 as StdCheck with uid="KYXPJVHKIS",rtseq=28,rtrep=.f.,left=498, top=266, caption="Deadlock",;
    ToolTipText = "Visualizza i deadlock",;
    HelpContextID = 70510039,;
    cFormVar="w_FILT_DEA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFILT_DEA_2_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILT_DEA_2_30.GetRadio()
    this.Parent.oContained.w_FILT_DEA = this.RadioValue()
    return .t.
  endfunc

  func oFILT_DEA_2_30.SetRadio()
    this.Parent.oContained.w_FILT_DEA=trim(this.Parent.oContained.w_FILT_DEA)
    this.value = ;
      iif(this.Parent.oContained.w_FILT_DEA=='S',1,;
      0)
  endfunc

  add object oRIGHE_2_31 as StdField with uid="ZYQSVUKXYD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_RIGHE", cQueryName = "RIGHE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo di righe interessate per cui l'istruzione SQLviene tracciata",;
    HelpContextID = 205948029,;
   bGlobalFont=.t.,;
    Height=21, Width=131, Left=169, Top=436, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

  add object oMILLISECONDI_2_32 as StdField with uid="FAHYKCYVRA",rtseq=30,rtrep=.f.,;
    cFormVar = "w_MILLISECONDI", cQueryName = "MILLISECONDI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata minima in millisecondi per cui l'istruzione SQLviene tracciata",;
    HelpContextID = 254683146,;
   bGlobalFont=.t.,;
    Height=21, Width=131, Left=169, Top=462, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

  add object oAZIENDA_2_33 as StdField with uid="HUDRDXEOQD",rtseq=31,rtrep=.f.,;
    cFormVar = "w_AZIENDA", cQueryName = "AZIENDA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 63252214,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=169, Top=488, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_AZIENDA"

  func oAZIENDA_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZIENDA_2_33.ecpDrop(oSource)
    this.Parent.oContained.link_2_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZIENDA_2_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oAZIENDA_2_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oONLYERR_2_34 as StdCheck with uid="TACPKLKWGE",rtseq=32,rtrep=.f.,left=506, top=436, caption="Solo errori",;
    ToolTipText = "Visualizza solo le righe con errori",;
    HelpContextID = 249825650,;
    cFormVar="w_ONLYERR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oONLYERR_2_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oONLYERR_2_34.GetRadio()
    this.Parent.oContained.w_ONLYERR = this.RadioValue()
    return .t.
  endfunc

  func oONLYERR_2_34.SetRadio()
    this.Parent.oContained.w_ONLYERR=trim(this.Parent.oContained.w_ONLYERR)
    this.value = ;
      iif(this.Parent.oContained.w_ONLYERR=='S',1,;
      0)
  endfunc

  add object oONLYHOST_2_35 as StdCheck with uid="ZHTENDQFQG",rtseq=33,rtrep=.f.,left=506, top=462, caption="Solo host",;
    ToolTipText = "Visualizza solo l'attivit� dell'host",;
    HelpContextID = 244317250,;
    cFormVar="w_ONLYHOST", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oONLYHOST_2_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oONLYHOST_2_35.GetRadio()
    this.Parent.oContained.w_ONLYHOST = this.RadioValue()
    return .t.
  endfunc

  func oONLYHOST_2_35.SetRadio()
    this.Parent.oContained.w_ONLYHOST=trim(this.Parent.oContained.w_ONLYHOST)
    this.value = ;
      iif(this.Parent.oContained.w_ONLYHOST=='S',1,;
      0)
  endfunc


  add object oBtn_2_36 as StdButton with uid="WZVBUJARTZ",left=740, top=444, width=48,height=45,;
    CpPicture="REQUERY.bmp", caption="", nPag=2;
    , ToolTipText = "Ripete l'interrogazione";
    , HelpContextID = 236355557;
    , caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_36.Click()
      with this.Parent.oContained
        .NotifyEvent("Popola2Zoom")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT inlist( i_nACTIVATEPROFILER, 5, 7))
     endwith
    endif
  endfunc


  add object oBtn_2_37 as StdButton with uid="FLDESAZSWD",left=740, top=444, width=48,height=45,;
    CpPicture="REQUERY.bmp", caption="", nPag=2;
    , ToolTipText = "Ripete l'interrogazione";
    , HelpContextID = 236355557;
    , caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_37.Click()
      with this.Parent.oContained
        .NotifyEvent("PopolaZoom")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_37.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT inlist( i_nACTIVATEPROFILER, 1, 3))
     endwith
    endif
  endfunc


  add object oBtn_2_39 as StdButton with uid="OZWDZVHQXJ",left=740, top=494, width=48,height=45,;
    CpPicture="ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Chiude la maschera";
    , HelpContextID = 91652174;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_39.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCONTENUTOINFRASESQL_2_40 as StdField with uid="ZWYULJVRRW",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CONTENUTOINFRASESQL", cQueryName = "CONTENUTOINFRASESQL",;
    bObbl = .f. , nPag = 2, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Si posiziona sulla frase SQL contenente l'espressione",;
    HelpContextID = 102406383,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=136, Top=360, InputMask=replicate('X',250)


  add object oBtn_2_41 as StdButton with uid="WUITYAGFGU",left=740, top=360, width=48,height=45,;
    CpPicture="REQUERY.bmp", caption="", nPag=2;
    , ToolTipText = "Cerca la riga con la frase sql o il messaggio di errore contenente l'espressione specificata";
    , HelpContextID = 206371434;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_41.Click()
      with this.Parent.oContained
        cp_viewtrace_b(this.Parent.oContained,"Q")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCARTELLALOG_2_44 as StdField with uid="OVUYKADRSW",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CARTELLALOG", cQueryName = "CARTELLALOG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Percorso utilizzato per DBF del log",;
    HelpContextID = 144928631,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=136, Top=11, InputMask=replicate('X',250)

  func oCARTELLALOG_2_44.mHide()
    with this.Parent.oContained
      return (inlist( i_nACTIVATEPROFILER, 1, 3))
    endwith
  endfunc

  add object oAZRAGAZI_2_49 as StdField with uid="LBTMVXKUMT",rtseq=38,rtrep=.f.,;
    cFormVar = "w_AZRAGAZI", cQueryName = "AZRAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210068886,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=251, Top=488, InputMask=replicate('X',40)

  add object oUTENTE_2_51 as StdField with uid="SLBNRYOJHT",rtseq=39,rtrep=.f.,;
    cFormVar = "w_UTENTE", cQueryName = "UTENTE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 125225587,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=169, Top=513, bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_UTENTE"

  func oUTENTE_2_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTENTE_2_51.ecpDrop(oSource)
    this.Parent.oContained.link_2_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTENTE_2_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oUTENTE_2_51'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oNOMEUTENTE_2_52 as StdField with uid="TRLNGRLYRX",rtseq=40,rtrep=.f.,;
    cFormVar = "w_NOMEUTENTE", cQueryName = "NOMEUTENTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 211364218,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=251, Top=512, InputMask=replicate('X',40)

  add object oStr_2_1 as StdString with uid="MLJYOZTCIX",Visible=.t., Left=14, Top=12,;
    Alignment=1, Width=118, Height=18,;
    Caption="File di log:"  ;
  , bGlobalFont=.t.

  func oStr_2_1.mHide()
    with this.Parent.oContained
      return (inlist( i_nACTIVATEPROFILER, 5, 7))
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="ZPHILCBLSZ",Visible=.t., Left=18, Top=52,;
    Alignment=0, Width=75, Height=19,;
    Caption="Filtri"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_42 as StdString with uid="WGBEEEDINI",Visible=.t., Left=27, Top=437,;
    Alignment=1, Width=136, Height=18,;
    Caption="Nro minimo righe:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="WCWIWEYKKB",Visible=.t., Left=26, Top=463,;
    Alignment=1, Width=138, Height=18,;
    Caption="Durata minima:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="FEVIHIQVJL",Visible=.t., Left=14, Top=12,;
    Alignment=1, Width=118, Height=18,;
    Caption="Cartella di log:"  ;
  , bGlobalFont=.t.

  func oStr_2_45.mHide()
    with this.Parent.oContained
      return (inlist( i_nACTIVATEPROFILER, 1, 3))
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="UQKOELSXKD",Visible=.t., Left=77, Top=490,;
    Alignment=1, Width=85, Height=18,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="NCQMNPSJSN",Visible=.t., Left=122, Top=514,;
    Alignment=0, Width=39, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_53 as StdString with uid="OEIPNCMTOD",Visible=.t., Left=19, Top=337,;
    Alignment=0, Width=65, Height=19,;
    Caption="Ricerca"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_54 as StdString with uid="SZCYOQGWPL",Visible=.t., Left=13, Top=361,;
    Alignment=1, Width=119, Height=18,;
    Caption="Cerca in frase SQL:"  ;
  , bGlobalFont=.t.

  add object oBox_2_38 as StdBox with uid="ABRXYWOJLS",left=16, top=71, width=755,height=2

  add object oBox_2_46 as StdBox with uid="TDCOEHZZXE",left=16, top=354, width=755,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_viewtrace','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_viewtrace
define class cp_zoombox_log as cp_zoombox

* Ridefinisce la procedura Query, popolando cCursor da DBF invece che da __sqlx__.SQLExec(this.nConn,s,this.cCursor,this.cDatabaseType)

    Proc Query(initsel,bSameTable,i_bNoFilter,i_bDontFill,i_bCreateHeader,i_bNotRemoveObject)
        * --- esegue la query sull' archivio principale, creando il cursore su cui operera'
        Local fld,ord,whe,i,j,k,del,op,Key,aFlds,i_timedmsg,s,i_tmpcursor,whe_t,bReCreateHeader, bThisFormLockScreen
        Dimension aFlds[1]
        This.o_initsel=initsel
        If Empty(This.cSymFile)
            Return
        Endif
        bThisFormLockScreen=Thisform.LockScreen
        Thisform.LockScreen=.T.
        This.grd.Enabled=.F.
        if not i_bNotRemoveObject
          local cObjManyHeaderName
          cObjManyHeaderName="oManyHeader_"+This.name
          if vartype(this.oRefHeaderObject)='O' and (vartype(i_bNoreMoveDblClick)<>'L' or not i_bNoreMoveDblClick)
             this.oRefHeaderObject.parent.RemoveObject(cObjManyHeaderName)
             this.oRefHeaderObject=null
             bReCreateHeader=.t.
          else
             bReCreateHeader=.f.
          endif
        endif
        * --- se e' sulla stessa tabella, crea un cursore fasullo che ottimizza la riscrittura della griglia
        If bSameTable And Used(This.cCursor)
            Select (This.cCursor)
            =Afields(aFlds)
            i_tmpcursor=Sys(2015)
            Create Cursor (i_tmpcursor) From Array aFlds
            This.grd.RecordSource=i_tmpcursor
            Select (This.cCursor)
        Else
            bSameTable=.F.
        Endif
        * --- i campi da selezionare
        If This.bFillFields
            fld=This.cStartFields && '*'
        Else
            fld=''
            *--- Rileggo XDC
            Cp_ReadXdc()
            For i=1 To This.nFields
                If Isnull(This.oCpQuery) And i_Dcx.IsFieldNameTranslate(This.cSymFile,This.cFields(i))
                    fld=fld+Iif(i=1,'',',')+cp_TransLinkFldName(This.cFields(i))+' as '+This.cFields(i)
                Else
                    fld=fld+Iif(i=1,'',',')+This.cFields(i)
                Endif
            Next
            If Empty(fld)
                fld='*'
            Endif
        Endif
        * -- l' ordine da utilizzare
        ord=''
        For i=1 To This.nOrderBy
            j=0
            * e' preferibile utilizzare il numero del campo piuttosto che il nome:
            * meno problemi in query complesse con campi qualificati o con union e piu' veloce
            If Isnull(This.oCpQuery)
                For k=1 To This.nFields
                    If Lower(This.cOrderBy[i])==Lower(This.cFields[k])
                        j=k
                    Endif
                Next
            Else
                * --- se ho la query vado a ricerca l'alias....
                For k=1 To This.oCpQuery.nFields
                    If Lower(This.cOrderBy[i])==Lower(This.oCpQuery.i_Fields[k,2])
                        j=k
                    Endif
                Next
            Endif
            ord=ord+Iif(i=1,'',',')+Iif(j=0,This.cOrderBy[i],Alltrim(Str(j)))
            If This.bOrderDesc[i]
                ord=ord+' desc'
            Endif
        Next
        *** Zucchetti Aulla - mancava il controllo della variabile w_ORERBY che modifica l'ordinamento
        If Not Isnull(This.oCpQuery) And Not Empty(This.oCpQuery.cOrderByTmp)
            k=This.oCpQuery.cOrderByTmp
            i=At(' desc',Lower(k))
            If i<>0
                k=Left(k,i-1)
            Endif
            If At(' '+k+' ',ord)=0
                ord = ord+Iif(Empty(ord),'',',')+This.oCpQuery.cOrderByTmp
            Endif
        Endif
        * --- costruisce la where
        whe=''
        op=''
        For i=1 To This.nWhere
            whe_t=This.cWhere(i)
            If Right(Trim(whe_t),2)=']]'
                whe_t=Left(whe_t,At('[[',whe_t)-1)
            Endif
            If !(whe_t=='OR')
                If This.nConn<>0
                    whe=whe+op+Trim(This.toodbc(whe_t))
                Else
                    whe=whe+op+Trim(whe_t)
                Endif
                op=' and '
            Else
                op=Iif(!Empty(op),' or ',op)  && la or non puo' essere al primo posto
            Endif
        Next
        * ---
        If Isnull(This.oCpQuery)
            If Vartype(initsel)='C' And !Empty(initsel)
                * --- caso di query inizializzata dall' esterno (zoom standard su selezioni aperte)
                whe=Iif(Empty(whe),initsel,'('+whe+') and ('+initsel+')')
            Endif
            * --- costruisce la frase SQL (query interna)
            If !Empty(whe)
                whe=' where '+whe
            Endif
            If !Empty(ord)
                ord=' order by '+ord
            Endif
            * --- crea effettivamente la frase SQL
            If This.bModSQL And !Empty(This.cSQL)
                * --- Zucchetti Aulla inizio - i_Codazi non trimmato
                s=Strtran(Strtran(This.cSQL,'xxx',Alltrim(i_codazi)),Chr(13)+Chr(10),' ')
                If !Empty(whe)
                    If ' where '$s
                        s=Strtran(s,' where ',whe+' and ')
                    Else
                        s=s+whe
                    Endif
                Endif
                If !Empty(ord)
                    If ' order by '$s
                        s=Strtran(s,' order by ',ord+',')
                    Else
                        s=s+ord
                    Endif
                Endif
            Else
                s="select "+fld+" from "+This.cFile+Iif(This.nConn<>0," "," as ")+This.lFile+" "+whe+" "+ord
            Endif
            * --- esegue
            If This.nConn<>0
                *=cp_SQL(this.nConn,"select "+fld+" from "+this.cFile+" "+whe+" "+ord,this.cCursor)
                Local __sqlx__
                __sqlx__=This.GetSqlx()
                If Type("__sqlx__")<>'O'
                    cp_sql(This.nConn,s,This.cCursor)
                Else
          *__sqlx__.SQLExec(this.nConn,s,this.cCursor,this.cDatabaseType)
          *Parte sostitutiva, inizio
          *Ad ogni nuova interrogazione dello zoom le ricerche per contenuto devono
          *ricominciare, per cui occorre azzerare le variabili w_CONTENUTOINFRASESQL e w_TROVASUCCESSIVO
          if empty(this.parent.ocontained.w_FILEDILOG)
            * Cartella contenente i file di log
            IF DIRECTORY(i_cACTIVATEPROFILER_POSIZIONELOG)
              THIS.parent.ocontained.w_CARTELLALOG=ADDBS(i_cACTIVATEPROFILER_POSIZIONELOG)
            ELSE
              THIS.parent.ocontained.w_CARTELLALOG=ADDBS(tempadhoc()+'Logger')
              i_cACTIVATEPROFILER_POSIZIONELOG=THIS.parent.ocontained.w_CARTELLALOG
            ENDIF
            * Nome fisico del DBF da mostrare
            IF FILE(ALLTRIM(THIS.parent.ocontained.w_CARTELLALOG)+ALLTRIM(i_cACTIVATEPROFILER_NOMELOG)+".DBF")
              THIS.parent.ocontained.w_FILEDILOG=ALLTRIM(THIS.parent.ocontained.w_CARTELLALOG)+ALLTRIM(i_cACTIVATEPROFILER_NOMELOG)+".DBF"
            ELSE
              IF FILE(THIS.parent.ocontained.w_CARTELLALOG+"ACTIVITYLOGGER.DBF")
                THIS.parent.ocontained.w_FILEDILOG=THIS.parent.ocontained.w_CARTELLALOG+"ACTIVITYLOGGER.DBF"
              ELSE
                THIS.parent.ocontained.w_FILEDILOG=SPACE( 250 )
              ENDIF
            ENDIF
          ENDIF
	  local cCursoreDiAppoggio,l_where,l_comandodaeseguire
          if not empty(this.parent.ocontained.w_FILEDILOG) and FILE(this.parent.ocontained.w_FILEDILOG)
            cCursoreDiAppoggio = SYS( 2015 )
            IF USED( this.cCursor )
	            * --- copio il contenuto del cursore attuale in un cursore di appoggio, se ho un errore posso sempre ripristinare
              l_comandodaeseguire = "SELECT * FROM " + this.cCursor + " INTO CURSOR " + cCursoreDiAppoggio
              &l_comandodaeseguire
              * --- Chiudo il cursore di partenza dell'elenco
              if vartype(this.parent.parent.oManyHeader_ELENCO)='O'
                _screen.LockScreen=.T.
                this.parent.parent.RemoveObject("oManyHeader_ELENCO")
              endif
              SELECT( this.cCursor )
              USE
            ENDIF
            l_where = " where 1=1 "
            if this.parent.ocontained.w_FILT_DBR = "N"
              l_where = l_where + " AND NOT (AL__TIPO = 'DBR' AND ALTIPCON<>'A') "
            endif
            if this.parent.ocontained.w_FILT_DBW = "N"
              l_where = l_where + " AND AL__TIPO <> 'DBW' "
            endif
            if this.parent.ocontained.w_FILT_DBI = "N"
              l_where = l_where + " AND AL__TIPO <> 'DBI' "
            endif
            if this.parent.ocontained.w_FILT_DBD = "N"
              l_where = l_where + " AND AL__TIPO <> 'DBD' "
            endif
            if this.parent.ocontained.w_FILT_DBM = "N"
              l_where = l_where + " AND AL__TIPO <> 'DBM' "
            endif
            if this.parent.ocontained.w_FILT_DBT = "N"
              l_where = l_where + " AND AL__TIPO <> 'DBT' "
            endif
            if this.parent.ocontained.w_FILT_DBX = "N"
              l_where = l_where + " AND AL__TIPO <> 'DBX' "
            endif
            if this.parent.ocontained.w_FILT_GSO = "N"
              l_where = l_where + " AND AL__TIPO <> 'GSO' "
            endif
            if this.parent.ocontained.w_FILT_GSD = "N"
              l_where = l_where + " AND AL__TIPO <> 'GSD' "
            endif
            if this.parent.ocontained.w_FILT_BTO = "N"
              l_where = l_where + " AND AL__TIPO <> 'BTO' "
            endif
            if this.parent.ocontained.w_FILT_BTD = "N"
              l_where = l_where + " AND AL__TIPO <> 'BTD' "
            endif
            if this.parent.ocontained.w_FILT_FCO = "N"
              l_where = l_where + " AND AL__TIPO <> 'FCO' "
            endif
            if this.parent.ocontained.w_FILT_FCD = "N"
              l_where = l_where + " AND AL__TIPO <> 'FCD' "
            endif
            if this.parent.ocontained.w_FILT_TBT = "N"
              l_where = l_where + " AND AL__TIPO <> 'TBT' "
            endif
            if this.parent.ocontained.w_FILT_TCT = "N"
              l_where = l_where + " AND AL__TIPO <> 'TCT' "
            endif
            if this.parent.ocontained.w_FILT_TRT = "N"
              l_where = l_where + " AND AL__TIPO <> 'TRT' "
            endif
            if this.parent.ocontained.w_FILT_ASI = "N"
              l_where = l_where + " AND NOT (AL__TIPO = 'DBR' AND ALTIPCON='A') "
            endif
            if this.parent.ocontained.w_FILT_RIC = "N"
              l_where = l_where + " AND ALDEARIC <> 'R' "
            endif
            if this.parent.ocontained.w_FILT_DEA = "N"
              l_where = l_where + " AND ALDEARIC <> 'D' "
            endif
            if this.parent.ocontained.w_FILT_VQR = "N"
              l_where = l_where + " AND AL__TIPO <> 'VQR' "
            endif
            if this.parent.ocontained.w_FILT_TASTIFUN = "N"
              * --- i tasti funzione iniziano per U cone gli eventi
              l_where = l_where + " AND Not(Left(AL__TIPO,1) = 'U' And AL__TIPO<>'UEV')"
            endif
            if this.parent.ocontained.w_FILT_EVENT = "N"
              l_where = l_where + " AND AL__TIPO <> 'UEV' "
            endif
            if this.parent.ocontained.w_RIGHE > 0
              l_where = l_where + " AND ALROWAFF >= " + ALLTRIM(STR(this.parent.ocontained.w_RIGHE)) + " "
            endif
            if this.parent.ocontained.w_MILLISECONDI > 0
              l_where = l_where + " AND ALTOTTMS >= " + ALLTRIM(STR(this.parent.ocontained.w_MILLISECONDI)) + " "
            endif
            if this.parent.ocontained.w_ONLYERR = "S"
              l_where = l_where + " AND NOT EMPTY(NVL(ALERROR,' '))"
            endif
            if this.parent.ocontained.w_ONLYHOST = "S"
              l_where = l_where + " AND AL__HOST = ALLTRIM(SYS(0)) "
            endif
	          local VecchioErrore2,l_comandodaeseguire,cErroreInCaricamento
            l_comandodaeseguire = "select *, cast(ALCMDSQL as char(254)) As ALSQLCHR, cast(ALERROR as char(254)) As ALERRCHR, LEFT(JUSTFNAME(ALPROCED)+SPACE(50),50) As ALLIBNAM from " + " '"+ALLTRIM(this.parent.ocontained.w_FILEDILOG) +"' "+ l_where + " into cursor " + this.cCursor 
            VecchioErrore2=ON('error')
            cErroreInCaricamento=.f.
            ON ERROR cErroreInCaricamento=.t.
            &l_comandodaeseguire
            * Controlla la presenza del campo ALOPNAME
            afields(aFieldNames, this.cCursor)
            IF Ascan(aFieldNames,'ALOPNAME',1,-1,1,1)=0
                * se manca inserisce il campo alopname
                l_comandodaeseguire = "ALTER table " + this.cCursor + " ADD COLUMN ALOPNAME C(100)"
                &l_comandodaeseguire
            ENDIF
              If i_AdvancedHeaderZoom and vartype(this.parent.parent.oManyHeader_ELENCO)<>"O"
                this.parent.parent.AddObject("oManyHeader_ELENCO","ManyHeader")
                this.oRefHeaderObject=This.parent.parent.oManyHeader_ELENCO
                this.oRefHeaderObject.InitHeader(this.grd,.t.)
                _screen.LockScreen=.f.
              Endif
              * -- La select apre una nuova sessione per il DBF di traccia, la chiudo
              If Used( JUSTSTEM(ALLTRIM(this.parent.ocontained.w_FILEDILOG) ))
               Use in(JUSTSTEM(ALLTRIM(this.parent.ocontained.w_FILEDILOG) ))
              Endif
            ON ERROR &VecchioErrore2
	            * --- Ho avuto un errore, ripristino il vecchio cursore come cCursor
            if cErroreInCaricamento
              cp_errormsg("Non � stato possibile caricare i dati","!","")
              l_comandodaeseguire = "SELECT * FROM " + cCursoreDiAppoggio + " INTO CURSOR " + this.cCursor
              &l_comandodaeseguire
            endif
            * --- rimozione cursore di appoggio...
            if used(cCursoreDiAppoggio)
             Use in (cCursoreDiAppoggio)
            endif
            *Parte sostitutiva, fine
          else
            __sqlx__.SQLExec(this.nConn,STRTRAN(s,'ALSQLCHR','CAST (ALCMDSQL as char(254)) as ALSQLCHR'),this.cCursor,this.cDatabaseType)
          endif
                Endif
            Else
                i_timedmsg=Createobject('timedmsg',cp_Translate(MSG_EXECUTING_QUERY_D),3)
                s=s+Iif(' where '$s Or i_bNoFilter Or !This.bReadOnly,' nofilter','')
                &s Into Cursor (This.cCursor)
            Endif
            This.cOldSelect=Left(s,At('FROM',Upper(s)))
        Else
            * --- utilizza la query esterna
            i_timedmsg=.Null.
            This.oCpQuery.mLoadWhereTmp(whe)
            Thisform.LockScreen=bThisFormLockScreen
            * --- Se query con filtro aggiuntivo e non trovo l'alias della tabella all'interno della
            * --- visual query allora applico una ulteriore Sub select sulla query
            * --- perch� se utilizzo alias nei query filter potrebbero essere diversi da quelli specificati nella query
            Local bSub
            bSub=.F.
            If i_bSecurityRecord
                Local nIdx
                If Not Empty( initsel )
                    #If Version(5)>=900
                        nIdx=Ascan(This.oCpQuery.i_Files,This.lFile,1,1,2,15)
                    #Else
                        Local i_i
                        nIdx=0
                        For i_i=1 To Alen(This.oCpQuery.i_Files,1)
                            If  Not Empty( This.oCpQuery.i_Files[i_i,2])
                                If Lower(This.oCpQuery.i_Files[i_i,2])==Lower(This.lFile)
                                    nIdx=i_i
                                    Exit
                                Endif
                            Else
                                nIdx=0
                                Exit
                            Endif
                        Next
                    #Endif
                    bSub= (nIdx=0 )
                Endif
            Endif

            If bSub
                This.oCpQuery.nFiles=Iif(This.oCpQuery.nFiles=0,This.oCpQuery.nFilesOld,This.oCpQuery.nFiles)
                This.oCpQuery.nOrderBy=0 && Una subselect non deve avere ordinamenti...
                s='select * from ('+This.oCpQuery.mDoQuery('nocursor','Get',.Null.)+') '+Iif(This.nConn<>0," "," as ")+This.lFile+' where '+initsel+Iif(Not Empty(ord)," order by "+ord,"")
                If This.nConn<>0
                    Local __sqlx__
                    __sqlx__=This.GetSqlx()
                    If Type("__sqlx__")<>'O'
                        cp_sql(This.nConn,s,This.cCursor)
                    Else
                        __sqlx__.SQLExec(This.nConn,s,This.cCursor,This.cDatabaseType)
                    Endif
                Else
                    i_timedmsg=Createobject('timedmsg',cp_Translate(MSG_EXECUTING_QUERY_D),3)
                    s=s+Iif(' where '$s Or i_bNoFilter Or !This.bReadOnly,' nofilter','')
                    &s Into Cursor (This.cCursor)
                Endif
            Else
                If Vartype(initsel)='C' And !Empty(initsel)
                    This.oCpQuery.mLoadWhereTmp(initsel)
                Endif
                This.oCpQuery.mLoadOrderByTmp(ord)
                *--- Zucchetti Aulla Inizio, query sincrona
                *this.oCpQuery.mDoQuery(this.cCursor,'Exec',this.GetSqlx(),i_bNoFilter or !this.bReadOnly)
                This.oCpQuery.mDoQuery(This.cCursor,'Exec',Iif(Type('this.bSinc') = 'L' And This.bSinc, .T.,This.GetSqlx()),i_bNoFilter Or !This.bReadOnly)
                *--- Zucchetti Aulla Fine, query sincrona
            Endif
            Thisform.LockScreen=.T.
        Endif
        If !This.bReadOnly
            Select (This.cCursor)
            Local i_tmpname
            i_tmpname=Sys(2015)
            Use Dbf() Again In 0 Alias (i_tmpname)
            Use
            Select (i_tmpname)
            Use Dbf() Again In 0 Alias (This.cCursor)
            Use
            Select (This.cCursor)
        Endif
        If !i_bDontFill Or This.nColPositions=0
            * --- riempie la lista dei campi
            If This.bFillFields And Used(This.cCursor)
                This.FillFields()
                bSameTable=.F.
                This.nColPositions=0
            Endif
            * --- riempie la griglia
            If bSameTable
                This.grd.fillagain()
            Else
                This.grd.ColumnCount=0
                If Used(This.cCursor)
                    This.grd.Fill()
                Endif
            Endif
        Endif
        If This.nColPositions=0 And Used(This.cCursor)
            This.SaveColumnStatus()
        Endif
        If not i_bNotRemoveObject and i_AdvancedHeaderZoom
            do case
               case (bReCreateHeader or i_bCreateHeader) and i_AdvancedHeaderZoom and this.visible and vartype(this.oRefHeaderObject)<>"O"
                  This.parent.AddObject(cObjManyHeaderName,"ManyHeader")
                  this.oRefHeaderObject=This.parent.&cObjManyHeaderName
                  this.oRefHeaderObject.InitHeader(This.grd,.t.)
               case not (vartype(i_bNoreMoveDblClick)<>'L' or not i_bNoreMoveDblClick) and this.visible and vartype(this.oRefHeaderObject)="O"
                  this.oRefHeaderObject.HeaderRedraw()
            endcase
        Endif
        This.grd.Enabled=.T.
        This.bChangedFld=.F.
        If Len(whe)<=127
            This.src.ToolTipText=whe
        Else
            This.src.ToolTipText=Left(whe,124)+'...'
        Endif
        Wait Clear
        If bSameTable
            Select (i_tmpcursor)
            Use
            Select (This.cCursor)
        Endif
        Thisform.LockScreen=bThisFormLockScreen
        * --- gestione dell' avanzamento dello zoom nel caso di risultato troncato
        Local i_nrec
        If Type('i_nZoomMaxRows')='N' And Type('i_bDisableAsyncConn')<>'U' And i_bDisableAsyncConn And i_ServerConn[1,2]<>0
            i_nrec=Reccount(This.cCursor)
            If i_nrec>0 And i_nrec=i_nZoomMaxRows
                This.morerows.Visible=.T.
            Else
                This.morerows.Visible=.F.
            Endif
        Endif
        =CALLING(This.cCursor,Thisform.w_bFunc,Thisform.w_bProc,Thisform.w_bGest)
        * selezione della prima riga
        This.grd.Refresh
        * ---
endproc

enddefine

FUNCTION calling(cCursor,bFunc,bProc,bGest)
	LOCAL idxstack,cFCO,cFCD,cBTO,cBTD,cGSO,cGSD
	LOCAL ARRAY procstack(100)
	idxstack=0
	IF bFunc
		cFCO="FCO"
		cFCD="FCD"
	ELSE
		cFCO="   "
		cFCD="   "
	ENDIF
	IF bProc
		cBTO="BTO"
		cBTD="BTD"
	ELSE
		cBTO="   "
		cBTD="   "
	ENDIF
	IF bGest
		cGSO="GSO"
		cGSD="GSD"
	ELSE
		cGSO="   "
		cGSD="   "
	ENDIF
	SELECT (cCursor)
	SCAN
		DO CASE 
			CASE AL__TIPO=cFCO OR AL__TIPO=cBTO OR AL__TIPO=cGSO
				idxstack = idxstack+1
				IF idxstack<=100
					procstack(idxstack)=ALLIBNAM
				ENDIF
			CASE AL__TIPO=cFCD OR AL__TIPO=cBTD OR AL__TIPO=cGSD
				IF idxstack>0 AND procstack(idxstack)=ALLIBNAM
					idxstack = idxstack-1
				ENDIF
			OTHERWISE
				IF idxstack>0 AND idxstack<=100 AND EMPTY(ALLIBNAM)
					REPLACE ALLIBNAM WITH procstack(idxstack)
				ENDIF
		ENDCASE
	ENDSCAN
	GO TOP
RETURN .T.
ENDFUNC

* --- Fine Area Manuale
