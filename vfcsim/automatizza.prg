* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: automatizza                                                     *
*              Automatizza stampa                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-12                                                      *
* Last revis.: 2013-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tautomatizza",oParentObject)
return(i_retval)

define class tautomatizza as StdBatch
  * --- Local variables
  w_OBJ = .NULL.
  w_bChAutom = .f.
  w_ANTECOND = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Zucchetti Aulla-Automatizza printsystem Inizio
    * --- Se � attiva la nuova Print System esegue automatizza a pag 2
    this.w_OBJ = this.oparentobject
    bChAutom=.T.
    this.w_bChAutom = .T.
    * --- Verifico il contenuto della propriet� 'pdnAzAutom'
    if  this.w_OBJ.pdnAzAutom <> -1
      * --- * Assegno il valore corrispondente all'operazione da eseguire in automatico al parametro 'nAzAutom'
      *     * evitando di modificare tutte le occorenze di quest'ultimo e la funzionalit� stessa.
      nAzAutom = this.w_OBJ.pdnAzAutom
    endif
    * --- Se nAzAutom � maggiore di venti non chiudo automaticamente la printsystem
    if  nAzAutom>20
      bChAutom=.F. 
 nAzAutom=nAzAutom-20
    endif
    if i_bNewPrintSystem
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Vecchia Print System
    do case
      case nAzAutom=1
        * --- Anteprima Automatica
        this.w_OBJ.oBtn_Ante.Click
      case nAzAutom=2
        * --- Stampa Automatica (Stampante predefinita 1 copia)
        this.w_OBJ.oBtn_Stp.Click
      case nAzAutom=3
        * --- Stampa word Automatica
        if this.w_OBJ.oBtn_Word.enabled
          this.w_OBJ.oBtn_Word.Click
        else
          if vartype(g_OFFICE)<>"C" or g_OFFICE="M"
            cp_msg(MSG_WORD_PRINT_DISABLED,.f.,.f.,10,.t.)
          else
            cp_msg(MSG_WRITER_PRINT_DISABLED,.f.,.f.,10,.t.)
          endif
          bChAutom=.F.
        endif
      case nAzAutom=4
        * --- Stampa Excel Automatica
        if this.w_OBJ.oBtn_Excel.enabled
          this.w_OBJ.oBtn_Excel.Click
        else
          if vartype(g_OFFICE)<>"C" or g_OFFICE="M"
            cp_msg(MSG_EXCEL_PRINT_DISABLED,.f.,.f.,10,.t.)
          else
            cp_msg(MSG_SPREADSHEET_PRINT_DISABLED,.f.,.f.,10,.t.)
          endif
          bChAutom=.F.
        endif
      case nAzAutom=5
        * --- Grafico Automatico
        if this.w_OBJ.oBtn_Graph.enabled
          this.w_OBJ.oBtn_Graph.Click
        else
          cp_msg(MSG_GRAPH_PRINT_DISABLED,.f.,.f.,10,.t.) 
 bChAutom=.F.
        endif
      case nAzAutom=6
        * --- E-Mail Automatica
        this.w_OBJ.oBtn_Mail.Click
      case nAzAutom=7
        * --- Invio Corporate Automatico
        if this.w_OBJ.oBtn_WWP_ZCP.enabled
          this.w_OBJ.oBtn_WWP_ZCP.Click
        else
          cp_msg(MSG_SENDING_DATA_DISABLED,.f.,.f.,10,.t.) 
 bChAutom=.F.
        endif
      case nAzAutom=8
        * ---  Invio postaLite Automatico
        if this.w_OBJ.oBtn_Plite.enabled
          this.w_OBJ.oBtn_Plite.Click
        else
          cp_msg(MSG_SENDING_POSTALITE_DATA_DISABLED,.f.,.f.,10,.t.) 
 bChAutom=.F.
        endif
      case nAzAutom=9
        * ---  Invio fax Automatico
        this.w_OBJ.oBtn_Fax.Click
      case nAzAutom=10
        * --- Stampa su file Automatico
        if TYPE("noParentObject.w_Nomefile")="C"and TYPE("noParentObject.w_Pathfile")="C" AND Directory(noParentObject.w_Pathfile)
          * --- path specifico di generazione file
          this.w_OBJ.prFile = IIF(UPPER(RIGHT(ALLTRIM(noParentObject.w_Nomefile),4))=".PDF",noParentObject.w_Nomefile,ALLTRIM(noParentObject.w_Nomefile)+".PDF")
        else
          this.w_OBJ.prFile = path_print_system()+"DEFA"+right("0000"+alltrim(str(i_codute,4,0)),4)+".PDF"
        endif
        this.w_OBJ.w_Txt_File = this.w_OBJ.prFile 
 this.w_OBJ.prTyExp = "5" 
 this.w_OBJ.w_Opt_File = "PDF"
        if TYPE("noParentObject.w_NOMEFILE")="C"
          noParentObject.w_NOMEFILE =this.w_OBJ.prfile
        endif
        this.w_OBJ.obtn_stpfile.Click
      case nAzAutom=11
        * --- Archivia documento PDF
        if this.w_OBJ.oBtn_SaveInd.enabled
          this.w_OBJ.obtn_SaveInd.Click
        else
          bChAutom=.F.
        endif
    endcase
    if bChAutom
      this.w_OBJ.oBtn_Esc.Click
    endif
    * --- Zucchetti Aulla-Automatizza printsystem Fine
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case nAzAutom=1
        * --- Anteprima Automatica
        CP_CHPFUN (this.w_OBJ,"AnteClick") 
      case nAzAutom=2
        * --- Stampa Automatica (Stampante predefinita 1 copia)
        Cp_ChpFun ( this.w_OBJ, "StpClick" )
      case nAzAutom=3
        * --- Stampa word Automatica
        if cp_PrintCondition ( this.w_OBJ , "Btn_Word_Edit") 
          CP_CHPFUN (this.w_OBJ,"WordClick") 
        else
          if vartype(g_OFFICE)<>"C" or g_OFFICE="M"
            cp_msg(MSG_WORD_PRINT_DISABLED,.f.,.f.,10,.t.)
          else
            cp_msg(MSG_WRITER_PRINT_DISABLED,.f.,.f.,10,.t.)
          endif
          this.w_bChAutom = .F.
        endif
      case nAzAutom=4
        * --- Stampa Excel Automatica
        if cp_PrintCondition (this.w_OBJ, "Btn_Excel_Edit") 
          CP_CHPFUN (this.w_OBJ,"ExcelClick") 
        else
          if vartype(g_OFFICE)<>"C" or g_OFFICE="M"
            cp_msg(MSG_EXCEL_PRINT_DISABLED,.f.,.f.,10,.t.)
          else
            cp_msg(MSG_SPREADSHEET_PRINT_DISABLED,.f.,.f.,10,.t.)
          endif
          this.w_bChAutom = .F.
        endif
      case nAzAutom=5
        * --- Grafico Automatico
        if cp_PrintCondition ( this.w_OBJ , "Btn_Graph_Edit") 
          CP_CHPFUN (this.w_OBJ,"GraphClick") 
        else
          cp_msg(MSG_GRAPH_PRINT_DISABLED,.f.,.f.,10,.t.)
          this.w_bChAutom = .F.
        endif
      case nAzAutom=6
        * --- E-Mail Automatica
        CP_CHPFUN (this.w_OBJ,"MailClick") 
      case nAzAutom=7
        * --- Invio Corporate Automatico
        * --- Condizione di editing del bottone Anteprima
        this.w_ANTECOND = cp_PrintCondition ( this.w_OBJ , "Btn_Ante_Edit") 
        if cp_PrintCondition ( this.w_OBJ , "Btn_WWP_ZCP_Edit") 
          CP_CHPFUN (this.w_OBJ,"WWP_ZCPClick") 
        else
          cp_msg(MSG_SENDING_DATA_DISABLED,.f.,.f.,10,.t.)
          this.w_bChAutom = .F.
        endif
      case nAzAutom=8
        * ---  Invio postaLite Automatico
        if cp_PrintCondition (this.parent.parent.parent.parent , "Btn_Plite_Edit") 
          CP_CHPFUN (this.w_OBJ,"PliteClick") 
        else
          cp_msg(MSG_SENDING_POSTALITE_DATA_DISABLED,.f.,.f.,10,.t.)
          this.w_bChAutom = .F.
        endif
      case nAzAutom=9
        * ---  Invio fax Automatico
        CP_CHPFUN (this.w_OBJ,"FaxClick") 
      case nAzAutom=10
        * --- Stampa su file Automatico
        if TYPE("noParentObject.w_Nomefile")="C"and TYPE("noParentObject.w_Pathfile")="C" AND Directory(noParentObject.w_Pathfile)
          * --- path specifico di generazione file
          this.w_OBJ.prFile = IIF(UPPER(RIGHT(ALLTRIM(noParentObject.w_Nomefile),4))=".PDF",noParentObject.w_Nomefile,ALLTRIM(noParentObject.w_Nomefile)+".PDF")
        else
          this.w_OBJ.prFile = path_print_system()+"DEFA"+right("0000"+alltrim(str(i_codute,4,0)),4)+".PDF"
        endif
        this.w_OBJ.w_Txt_File = this.w_OBJ.prFile
        this.w_OBJ.prTyExp = "5"
        this.w_OBJ.w_Opt_File  = "PDF"
        if TYPE("noParentObject.w_NOMEFILE")="C"
          noParentObject.w_NOMEFILE =this.w_OBJ.prfile
        endif
        CP_CHPFUN (this.w_OBJ,"StpFileClick") 
      case nAzAutom=11
        * --- Archivia documento PDF
        CP_CHPFUN (this.w_OBJ,"SaveIndClick") 
    endcase
    if this.w_bChAutom
      CP_CHPFUN (this.w_OBJ,"EscClick") 
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
