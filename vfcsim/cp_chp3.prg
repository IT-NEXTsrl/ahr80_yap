* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_chp3                                                         *
*              "+MSG_PRINT_SYSTEM+"                                            *
*                                                                              *
*      Author: GiorGio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-06                                                      *
* Last revis.: 2018-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_chp3",oParentObject))

* --- Class definition
define class tcp_chp3 as StdForm
  Top    = 12
  Left   = 35

  * --- Standard Properties
  Width  = 656
  Height = 193+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-13"
  HelpContextID=175019619
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  STMPFILE_IDX = 0
  cPrg = "cp_chp3"
  cComment = ""+MSG_PRINT_SYSTEM+""
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FRXPDFFILE = .F.
  w_FRXFILE = .F.
  w_FRXGRPFILE = .F.
  w_PDBTN = space(10)
  w_NOMECURS = space(10)
  w_Opt_Language = space(20)
  w_DEVICE = space(200)
  w_Txt_Copies = 0
  w_Opt_File = space(20)
  o_Opt_File = space(20)
  w_FORMFILE = space(10)
  o_FORMFILE = space(10)
  w_TIPOFILE = space(10)
  w_GESTIONE = space(10)
  w_Txt_File = space(200)
  o_Txt_File = space(200)
  w_PCNAME = space(10)
  w_SEL = space(10)
  w_SFFLOPEN = space(1)
  w_MOD_SEL = space(1)
  w_EXCLWRD = space(1)
  w_EXCEL = space(10)
  w_INDICE = 0
  w_SELECTED = 0
  o_SELECTED = 0
  w_oMultiReport = space(10)
  w_TREPORT = space(10)
  w_BTNANTE = space(10)
  w_PRODOCM = space(10)
  w_OLDSTATE = 0
  w_SELEZ = space(1)
  o_SELEZ = space(1)
  w_MULCHECK = .F.
  w_Zoom_Printer = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_chp3
    nIndexReport=1	&&Indice report corrente, selezionato nella ComboBox
    oSplitReport=.null.  &&MultiReport esplosione per fascicolazione report
    oOrigMReport=.null.  &&MultiReport originario (DOCM)
    cDescReport    =''
    cTipoReport    = ""
    cModSst =""     && Modello stampa solo testo da Cpusrrep
    ** Inizio DOCM
    cAnteprimaSN   = .T.
    cSessionCode=""
    cLanguage=""    		&& Lingua nella quale si vuole visualizzare/stampare il report
    * ProprietÓ per processo documentale
    pdProcesso     = .F.
    pdoParentObject= NULL
    pdObbligatorio = .F.
    pdMantieniLog  = .F.
    pdCodProcesso  = ""
    pdKeyIstanza   = ""
    pdExeProcesso  = .F.
    pdStrControl   = 'NNNNNN'
    pdnTotDoc      = 0
    pdnTotDocOk    = 0
    pdnTotDocKo    = 0
    pdISOLang = .f.
    pdnAzAutom = -1
    noParentObject=''
    oParentObject = .Null.
    cNomeReport = ""
    prStamp = ""
    prFile = ""
    prTyExp = ""
    prCopie = 1
    excel=.NULL.
    word=.NULL.
    bModifiedFrx=.f.
    i_FDFFile=.F.
  nAzAutom=0
  o_ZCP=null
  oOpt_File=null
  oOpt_Language=null
  oTxt_Copies=null
  oTxt_Device=null
  oTxt_File=null
  oPDLbl4=null
  oPDPrgBar=null
  oPDLbl1=null
  oPDLbl5=null
  oPDLbl2=null
  oPDLbl6=null
  oPDLbl3=null
  
  *--- non salvo la posizione della form
  bChkPositionForm = .F.
    
   w_Opt_File='TXT' && Formato di stampa selezionato
   cExtFormat = 'TXT' &&Estensione del formato
   cOption=' ' && Opzioni per stampa su file
   ADD OBJECT oFormatToExtension as Collection
  
  func HasCPEvents(i_cOp)
      return(i_cOp+'|'$'ecpQuit|ecpSecurity|')
    endfunc
    
  proc ecpQuit()
      this.oBtn_Esc.Click()
    endproc
    
    
  Proc ecpSecurity()
     cp_CHPFUN (this ,'ecpSecurity')
    Endproc
  
  Procedure Activate()
      DoDefault()
      * A volte la maschera di stampa appare disegnata male
      Local w_PrintSystemBorderStyle
      w_PrintSystemBorderStyle = this.BorderStyle
          If w_PrintSystemBorderStyle <> 0
              this.BorderStyle = 0
              this.BorderStyle = w_PrintSystemBorderStyle
          Else
              this.BorderStyle = 1
              this.BorderStyle = 0
          EndIf
  endproc
    
  Procedure Resize()
      DoDefault()
      * A volte l'etichetta della seconda pagina appare tagliata
      if i_visualtheme = -1
        this.oPgFrm.Width = this.Width
      endif
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_chp3Pag1","cp_chp3",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Print")
      .Pages(2).addobject("oPag","tcp_chp3Pag2","cp_chp3",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Report")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOpt_Language_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_chp3
    this.parent.cNomeReport = oParentObject
    	g_oPrintForm=this.parent
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom_Printer = this.oPgFrm.Pages(2).oPag.Zoom_Printer
    DoDefault()
    proc Destroy()
      this.w_Zoom_Printer = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='STMPFILE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FRXPDFFILE=.f.
      .w_FRXFILE=.f.
      .w_FRXGRPFILE=.f.
      .w_PDBTN=space(10)
      .w_NOMECURS=space(10)
      .w_Opt_Language=space(20)
      .w_DEVICE=space(200)
      .w_Txt_Copies=0
      .w_Opt_File=space(20)
      .w_FORMFILE=space(10)
      .w_TIPOFILE=space(10)
      .w_GESTIONE=space(10)
      .w_Txt_File=space(200)
      .w_PCNAME=space(10)
      .w_SEL=space(10)
      .w_SFFLOPEN=space(1)
      .w_MOD_SEL=space(1)
      .w_EXCLWRD=space(1)
      .w_EXCEL=space(10)
      .w_INDICE=0
      .w_SELECTED=0
      .w_oMultiReport=space(10)
      .w_TREPORT=space(10)
      .w_BTNANTE=space(10)
      .w_PRODOCM=space(10)
      .w_OLDSTATE=0
      .w_SELEZ=space(1)
      .w_MULCHECK=.f.
        .w_FRXPDFFILE = .cNomeReport.IsStdFile(iif(.i_FDFFile,'FDF','FRX'))
        .w_FRXFILE = .cNomeReport.IsStdFile('FRX')
        .w_FRXGRPFILE = .cNomeReport.IsStdFile('FRX')
        .w_PDBTN = .T.
        .w_NOMECURS = SELECT()
        .w_Opt_Language = 'ITA'
          .DoRTCalc(7,7,.f.)
        .w_Txt_Copies = .cNomeReport.nNumCopy
          .DoRTCalc(9,9,.f.)
        .w_FORMFILE = IIF (TYPE ("this.oOpt_File.displayvalue")='C' ,this.oOpt_File.displayvalue,this.w_Opt_File)
        .w_TIPOFILE = IIF (TYPE ("this.oOpt_File.displayvalue")='C' ,this.oOpt_File.displayvalue,this.w_Opt_File)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_TIPOFILE))
          .link_1_13('Full')
        endif
          .DoRTCalc(12,13,.f.)
        .w_PCNAME = Left( Sys(0), Rat('#',Sys( 0 ))-1)
        .w_SEL = 'S'
          .DoRTCalc(16,16,.f.)
        .w_MOD_SEL = 'M'
        .w_EXCLWRD = .w_Zoom_Printer.GetVar('EXCLWRD')
      .oPgFrm.Page2.oPag.Zoom_Printer.Calculate()
          .DoRTCalc(19,19,.f.)
        .w_INDICE = .w_Zoom_Printer.GetVar('INDICE')
        .w_SELECTED = .cNomeReport.getNumReport()
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
          .DoRTCalc(22,23,.f.)
        .w_BTNANTE = .T.
        .w_PRODOCM = 'N'
          .DoRTCalc(26,27,.f.)
        .w_MULCHECK = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_12.enabled = this.oPgFrm.Page2.oPag.oBtn_2_12.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_14.enabled = this.oPgFrm.Page2.oPag.oBtn_2_14.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_15.enabled = this.oPgFrm.Page2.oPag.oBtn_2_15.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_16.enabled = this.oPgFrm.Page2.oPag.oBtn_2_16.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_17.enabled = this.oPgFrm.Page2.oPag.oBtn_2_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_chp3
      this.opgfrm.width=this.width
      if i_VisualTheme=-1 AND (i_cViewMode="A" Or i_cViewMode="M")
      ***problema fatal error
       this.w_oldstate=this.windowstate
       this.windowstate=0
      endif  
      
      ENDPROC
      
      proc ecpQuit()
        CP_CHPFUN (this,'EscClick')  
      
      
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_Opt_File<>.w_Opt_File
            .w_FORMFILE = IIF (TYPE ("this.oOpt_File.displayvalue")='C' ,this.oOpt_File.displayvalue,this.w_Opt_File)
        endif
        if .o_Opt_File<>.w_Opt_File.or. .o_FORMFILE<>.w_FORMFILE
            .w_TIPOFILE = IIF (TYPE ("this.oOpt_File.displayvalue")='C' ,this.oOpt_File.displayvalue,this.w_Opt_File)
          .link_1_13('Full')
        endif
        .DoRTCalc(12,17,.t.)
            .w_EXCLWRD = .w_Zoom_Printer.GetVar('EXCLWRD')
        .oPgFrm.Page2.oPag.Zoom_Printer.Calculate()
        .DoRTCalc(19,19,.t.)
            .w_INDICE = .w_Zoom_Printer.GetVar('INDICE')
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        if .o_Opt_File<>.w_Opt_File.or. .o_Txt_File<>.w_Txt_File
          .Calculate_LRIUXYFAYZ()
        endif
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        if .o_SELEZ<>.w_SELEZ
          .Calculate_IYXVEYNHEH()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.Zoom_Printer.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
    endwith
  return

  proc Calculate_KEIRZNUFQK()
    with this
          * --- Cambio pagina - aggiorna il numero copie se sono state modificate
          GSUT_BRL(this;
              ,'S';
              ,this.cNomeReport;
             )
          CP_CHPFUN(this;
              ,'SStpClick';
             )
    endwith
  endproc
  proc Calculate_GITFTJUGHY()
    with this
          * --- Seleziono report al doppio click
          .oParentObject.w_RETINDEX = .w_INDICE
    endwith
  endproc
  proc Calculate_GDFKCFUCHH()
    with this
          * --- Cambio stampante al doppio click
          GSUT_BRL(this;
              ,'P';
              ,this.cNomeReport;
             )
    endwith
  endproc
  proc Calculate_RDQHOCWIEZ()
    with this
          * --- Calcola zoom
          .w_EXCEL = IIF(TYPE('.w_EXCLWRD')='C', .w_EXCLWRD='E',.F.)
          GSUT_BRL(this;
              ,'Z';
              ,this.cNomeReport;
             )
    endwith
  endproc
  proc Calculate_LRIUXYFAYZ()
    with this
          * --- Modifica estensione file
          .w_Txt_File = ForceExt(.w_Txt_File, .w_Opt_File)
          .prFile = .w_Txt_File
          .cDescReport = .w_Txt_File
          .cExtFormat = .w_Opt_File
          .prTyExp = .w_Opt_File
    endwith
  endproc
  proc Calculate_FDGOTESUYE()
    with this
          * --- Inizializza combo delle estensioni file
          .w_opt_file = iif(TYPE('g_SCHEDULER')='C' AND g_SCHEDULER='S' , 'PDF',this.oopt_file.combovalues[1])
    endwith
  endproc
  proc Calculate_IYXVEYNHEH()
    with this
          * --- Seleziona/Desleziona tutti i report dello zoom
          GSUT_BRL(this;
              ,'D';
              ,this.cNomeReport;
             )
    endwith
  endproc
  proc Calculate_JQGPIEOYBI()
    with this
          * --- Check su un report (se non si arriva da un multiple check)
      if NOT .w_MULCHECK
          GSUT_BRL(this;
              ,'X';
              ,this.cNomeReport;
             )
      endif
          .w_MULCHECK = .F.
    endwith
  endproc
  proc Calculate_KUYKTSUJSR()
    with this
          * --- Uncheck su un report (se non si arriva da un multiple check)
      if NOT .w_MULCHECK
          GSUT_BRL(this;
              ,'U';
              ,this.cNomeReport;
             )
      endif
          .w_MULCHECK = .F.
    endwith
  endproc
  proc Calculate_WRHSDAIQGQ()
    with this
          * --- Selezioni/Deslezioni multiple da tasto destro
          GSUT_BRL(this;
              ,'R';
              ,this.cNomeReport;
             )
    endwith
  endproc
  proc Calculate_OBDDHCSUEF()
    with this
          * --- Selezioni/Deslezioni multiple da tasto SHIFT
          .w_MULCHECK = .T.
          GSUT_BRL(this;
              ,'R';
              ,this.cNomeReport;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDEVICE_1_9.enabled = this.oPgFrm.Page1.oPag.oDEVICE_1_9.mCond()
    this.oPgFrm.Page1.oPag.oTxt_Copies_1_10.enabled = this.oPgFrm.Page1.oPag.oTxt_Copies_1_10.mCond()
    this.oPgFrm.Page1.oPag.oOpt_File_1_11.enabled = this.oPgFrm.Page1.oPag.oOpt_File_1_11.mCond()
    this.oPgFrm.Page1.oPag.oTxt_File_1_15.enabled = this.oPgFrm.Page1.oPag.oTxt_File_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_12.enabled = this.oPgFrm.Page2.oPag.oBtn_2_12.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_14.enabled = this.oPgFrm.Page2.oPag.oBtn_2_14.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_15.enabled = this.oPgFrm.Page2.oPag.oBtn_2_15.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_16.enabled = this.oPgFrm.Page2.oPag.oBtn_2_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.visible=!this.oPgFrm.Page1.oPag.oBtn_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_6.visible=!this.oPgFrm.Page1.oPag.oBtn_1_6.mHide()
    this.oPgFrm.Page1.oPag.oFORMFILE_1_12.visible=!this.oPgFrm.Page1.oPag.oFORMFILE_1_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_19.visible=!this.oPgFrm.Page1.oPag.oBtn_1_19.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_27.visible=!this.oPgFrm.Page1.oPag.oBtn_1_27.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_28.visible=!this.oPgFrm.Page1.oPag.oBtn_1_28.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_31.visible=!this.oPgFrm.Page1.oPag.oBtn_1_31.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_33.visible=!this.oPgFrm.Page1.oPag.oBtn_1_33.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_6.visible=!this.oPgFrm.Page2.oPag.oStr_2_6.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_8.visible=!this.oPgFrm.Page2.oPag.oStr_2_8.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_12.visible=!this.oPgFrm.Page2.oPag.oBtn_2_12.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_13.visible=!this.oPgFrm.Page2.oPag.oBtn_2_13.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_16.visible=!this.oPgFrm.Page2.oPag.oBtn_2_16.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_19.visible=!this.oPgFrm.Page2.oPag.oStr_2_19.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.Zoom_Printer.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 1")
          .Calculate_KEIRZNUFQK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoom_printer selected")
          .Calculate_GITFTJUGHY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoom_printer selected")
          .Calculate_GDFKCFUCHH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggiornaZoom")
          .Calculate_RDQHOCWIEZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_FDGOTESUYE()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
        if lower(cEvent)==lower("w_Zoom_Printer row checked")
          .Calculate_JQGPIEOYBI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Zoom_Printer row unchecked")
          .Calculate_KUYKTSUJSR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Zoom_Printer rowcheckall") or lower(cEvent)==lower("w_Zoom_Printer rowcheckfrom") or lower(cEvent)==lower("w_Zoom_Printer rowcheckto") or lower(cEvent)==lower("w_Zoom_Printer rowinvertselection") or lower(cEvent)==lower("w_Zoom_Printer rowuncheckall")
          .Calculate_WRHSDAIQGQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Zoom_Printer multiple rows checked") or lower(cEvent)==lower("w_Zoom_Printer multiple rows unchecked")
          .Calculate_OBDDHCSUEF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- cp_chp3
    IF cevent = 'w_Opt_File InteractiveChange'
    this.oOpt_File.valid()
    this.w_FORMFILE=this.oOpt_File.displayvalue
    this.o_FORMFILE='x'
    this.mcalc(.t.)
    ENDIF
    
    
    
    
    IF cevent = 'AggiornaZoom'
    i_LoadMenu=.T.
    this.mEnableControls ()
    i_LoadMenu=.F.
    this.w_Zoom_Printer.grd.Refresh()
    ENDIF
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPOFILE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STMPFILE_IDX,3]
    i_lTable = "STMPFILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2], .t., this.STMPFILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOFILE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOFILE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SFFLGSEL,SFPCNAME,SFFORMAT,SFMSKCFG,SFFLOPEN";
                   +" from "+i_cTable+" "+i_lTable+" where SFFORMAT="+cp_ToStrODBC(this.w_TIPOFILE);
                   +" and SFFLGSEL="+cp_ToStrODBC(this.w_SEL);
                   +" and SFPCNAME="+cp_ToStrODBC(this.w_PCNAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SFFLGSEL',this.w_SEL;
                       ,'SFPCNAME',this.w_PCNAME;
                       ,'SFFORMAT',this.w_TIPOFILE)
            select SFFLGSEL,SFPCNAME,SFFORMAT,SFMSKCFG,SFFLOPEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOFILE = NVL(_Link_.SFFORMAT,space(10))
      this.w_GESTIONE = NVL(_Link_.SFMSKCFG,space(10))
      this.w_SFFLOPEN = NVL(_Link_.SFFLOPEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOFILE = space(10)
      endif
      this.w_GESTIONE = space(10)
      this.w_SFFLOPEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])+'\'+cp_ToStr(_Link_.SFFLGSEL,1)+'\'+cp_ToStr(_Link_.SFPCNAME,1)+'\'+cp_ToStr(_Link_.SFFORMAT,1)
      cp_ShowWarn(i_cKey,this.STMPFILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOFILE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOpt_Language_1_8.RadioValue()==this.w_Opt_Language)
      this.oPgFrm.Page1.oPag.oOpt_Language_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEVICE_1_9.RadioValue()==this.w_DEVICE)
      this.oPgFrm.Page1.oPag.oDEVICE_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTxt_Copies_1_10.value==this.w_Txt_Copies)
      this.oPgFrm.Page1.oPag.oTxt_Copies_1_10.value=this.w_Txt_Copies
    endif
    if not(this.oPgFrm.Page1.oPag.oOpt_File_1_11.RadioValue()==this.w_Opt_File)
      this.oPgFrm.Page1.oPag.oOpt_File_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFORMFILE_1_12.value==this.w_FORMFILE)
      this.oPgFrm.Page1.oPag.oFORMFILE_1_12.value=this.w_FORMFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oTxt_File_1_15.value==this.w_Txt_File)
      this.oPgFrm.Page1.oPag.oTxt_File_1_15.value=this.w_Txt_File
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZ_2_24.RadioValue()==this.w_SELEZ)
      this.oPgFrm.Page2.oPag.oSELEZ_2_24.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKNFILE(.w_Txt_File, 'F'))  and (not .pdProcesso  AND .w_SELECTED>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTxt_File_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_Opt_File = this.w_Opt_File
    this.o_FORMFILE = this.w_FORMFILE
    this.o_Txt_File = this.w_Txt_File
    this.o_SELECTED = this.w_SELECTED
    this.o_SELEZ = this.w_SELEZ
    return

enddefine

* --- Define pages as container
define class tcp_chp3Pag1 as StdContainer
  Width  = 859
  height = 193
  stdWidth  = 859
  stdheight = 193
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_5 as AdvButtonMenu with uid="SWBJPZRBER",left=79, top=144, width=65,height=47,;
    CpPicture="PRESCI.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SEND_TO_PRINTER+"";
    , HelpContextID = 34105358;
    , CommandsMenu='Print.vmn' , Caption=MSG_S_PRINT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        dodefault()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (doDefault())
      endwith
    endif
  endfunc

  func oBtn_1_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso)
     endwith
    endif
  endfunc


  add object oBtn_1_6 as ButtonMenu with uid="ODPIUYUKDW",left=80, top=144, width=48,height=47,;
    CpPicture="bmp\auto.bmp", caption="", nPag=1;
    , ToolTipText = ""+MSG_EXEC_DOCUMENT_PROCESS+"";
    , HelpContextID = 254532718;
    , Caption=MSG_EXEC ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"oPDBtnProcClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (cp_PrintCondition (this.parent.parent.parent.parent , 'PDBtn_Proc_Edit') )
      endwith
    endif
  endfunc

  func oBtn_1_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Not .pdProcesso)
     endwith
    endif
  endfunc


  add object oOpt_Language_1_8 as StdZLanCombo with uid="DLGURVVZZC",rtseq=6,rtrep=.f.,left=12,top=23,width=89,height=21;
    , RowSourceType=1;
    , ToolTipText = "tipo file";
    , HelpContextID = 249020215;
    , cFormVar="w_Opt_Language",tablefilter="ITA=ITA", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.



  add object oDEVICE_1_9 as StdZLanCombo with uid="YJTBDAQENK",rtseq=7,rtrep=.f.,left=116,top=23,width=420,height=22;
    , ToolTipText = "Stampante selezionata";
    , HelpContextID = 252084471;
    , cFormVar="w_DEVICE",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(200);
  , bGlobalFont=.t.


  func oDEVICE_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (isPrinter () AND .w_treport<>'I' and .oTxt_Device.Enabled  AND .w_SELECTED>0)
    endwith
   endif
  endfunc

  add object oTxt_Copies_1_10 as StdField with uid="GBLWNLOKKP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_Txt_Copies", cQueryName = "Txt_Copies",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 27789772,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=588, Top=23

  func oTxt_Copies_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cNomeReport.bNumCopy  AND .w_tReport<>'I'  AND .w_SELECTED>0)
    endwith
   endif
  endfunc

  proc oTxt_Copies_1_10.mAfter
    with this.Parent.oContained
      CP_CHPFUN3(this.Parent.oContained, 'Txt_Copies LostFocus')
    endwith
  endproc


  add object oOpt_File_1_11 as StdTableCombo with uid="QDOWFWNDEA",rtseq=9,rtrep=.f.,left=12,top=90,width=89,height=21;
    , ToolTipText = "tipo file";
    , HelpContextID = 45416014;
    , cFormVar="w_Opt_File",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='cp_chpfu3.vqr',cKey='SFEXTFOR',cValue='SFFORMAT',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  func oOpt_File_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not   .pdProcesso  AND .w_SELECTED>0)
    endwith
   endif
  endfunc

  add object oFORMFILE_1_12 as StdField with uid="TFKCTHXSTR",rtseq=10,rtrep=.f.,;
    cFormVar = "w_FORMFILE", cQueryName = "FORMFILE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 79053415,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=776, Top=98, InputMask=replicate('X',10)

  func oFORMFILE_1_12.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oTxt_File_1_15 as StdField with uid="CIRLONFEHJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_Txt_File", cQueryName = "Txt_File",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path+nome file",;
    HelpContextID = 45415881,;
   bGlobalFont=.t.,;
    Height=21, Width=420, Left=114, Top=90, InputMask=replicate('X',200)

  func oTxt_File_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .pdProcesso  AND .w_SELECTED>0)
    endwith
   endif
  endfunc

  func oTxt_File_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKNFILE(.w_Txt_File, 'F'))
    endwith
    return bRes
  endfunc


  add object oBtn_1_16 as ButtonMenu with uid="KFZKZIEDQW",left=542, top=90, width=27,height=23,;
    caption="....", nPag=1;
    , ToolTipText = ""+MSG_SELECT_FILE+"";
    , HelpContextID = 175220593;
    , Caption = "...";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"SFileClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_treport<>'I' and not .pdProcesso AND .w_SELECTED>0 )
      endwith
    endif
  endfunc


  add object oBtn_1_17 as AdvButtonMenu with uid="UVPWVZIIRW",left=583, top=76, width=65,height=47,;
    CpPicture="PRESCI.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_PRINT_OR_EXPORT_ON_FILE+"";
    , HelpContextID = 34105358;
    , CommandsMenu='salva.vmn' , Caption=MSG_PRINT_OR_EXPORT_ON_FILE;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        dodefault()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (doDefault())
      endwith
    endif
  endfunc


  add object oBtn_1_18 as AdvButtonMenu with uid="VFOOFOVCCT",left=9, top=144, width=65,height=47,;
    CpPicture="PRVIDEO.ico", caption="", nPag=1;
    , ToolTipText = ""+  MSG_PRINT_PREVIEW  +"";
    , HelpContextID = 174720027;
    , CommandsMenu='preview.vmn', Caption='ButtonMenu';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        dodefault()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (doDefault()   )
      endwith
    endif
  endfunc


  add object oBtn_1_19 as ButtonMenu with uid="ZBIZEJCNQZ",left=137, top=144, width=48,height=47,;
    CpPicture="..\exe\bmp\TZOOM.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_DOCUMENT_PROCESS_DETAIL+"";
    , HelpContextID = 203699820;
    , Caption=MSG_DETAIL ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"PDBtnStatusClick")   
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.pdProcesso   AND .w_SELECTED>0)
      endwith
    endif
  endfunc

  func oBtn_1_19.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Not .pdProcesso)
     endwith
    endif
  endfunc


  add object oBtn_1_25 as AdvButtonMenu with uid="RNSBPECZRQ",left=151, top=144, width=65,height=47,;
    CpPicture="PRESCI.ico", caption="", nPag=1;
    , ToolTipText = "Esportazioni";
    , HelpContextID = 180749231;
    , CommandsMenu='export.vmn' , Caption='ButtonMenu';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        dodefault()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (doDefault())
      endwith
    endif
  endfunc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso)
     endwith
    endif
  endfunc


  add object oBtn_1_26 as AdvButtonMenu with uid="HSXWFOEJOP",left=222, top=144, width=66,height=47,;
    CpPicture="..\exe\bmp\Fxmail.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SENDING_EMAIL+"";
    , HelpContextID = 164756453;
    , CommandsMenu='MAIL_VMN.VQR' ,Caption='E-\<mail', FixedCaption=.t.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        doDefault()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (doDefault())
      endwith
    endif
  endfunc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso)
     endwith
    endif
  endfunc


  add object oBtn_1_27 as ButtonMenu with uid="IEXJASHSUC",left=295, top=144, width=48,height=47,;
    CpPicture="bmp\FXFAX.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SENDING_FAX+"";
    , HelpContextID = 120914971;
    , Caption='\<Fax' ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"FaxClick") 
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (cp_PrintCondition (this.parent.parent.parent.parent , 'Btn_Fax_Edit') )
      endwith
    endif
  endfunc

  func oBtn_1_27.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso)
     endwith
    endif
  endfunc


  add object oBtn_1_28 as AdvButtonMenu with uid="LZOKPNAHGV",left=352, top=144, width=65,height=47,;
    CpPicture="PRESCI.ico", caption="", nPag=1;
    , ToolTipText = "Integrazione";
    , HelpContextID = 3952751;
    , CommandsMenu='infinity.vmn' , Caption='ButtonMenu';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        dodefault()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (doDefault())
      endwith
    endif
  endfunc

  func oBtn_1_28.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso)
     endwith
    endif
  endfunc


  add object oBtn_1_29 as ButtonMenu with uid="RRBNPQFGUH",left=426, top=144, width=48,height=47,;
    CpPicture="bmp\logoedi.ico", caption="", nPag=1;
    , ToolTipText = "" + MSG_CHANGE_EDI + "";
    , HelpContextID = 101057563;
    , Caption = "E\<DI";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"EdiClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_VEFA='S'   AND .w_SELECTED>0 AND  .w_treport<>'I'   AND NOT bReportVuoto )
      endwith
    endif
  endfunc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso or g_VEFA<>'S' )
     endwith
    endif
  endfunc


  add object oBtn_1_30 as ButtonMenu with uid="WUKCDSBPIV",left=426, top=144, width=48,height=47,;
    CpPicture="bmp\fatel.ico", caption="", nPag=1;
    , ToolTipText = "" + MSG_CHANGE_FATEL + "";
    , HelpContextID = 188023727;
    , Caption = 'Fa\<tel';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"FatelClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_VEFA<>'S'   AND .w_SELECTED>0 AND  .w_treport<>'I'   AND NOT bReportVuoto )
      endwith
    endif
  endfunc

  func oBtn_1_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso or g_VEFA='S' )
     endwith
    endif
  endfunc


  add object oBtn_1_31 as ButtonMenu with uid="KWGCKUABPU",left=482, top=144, width=48,height=47,;
    CpPicture="bmp\PLite.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SENDING_BY_POSTALITE+"";
    , HelpContextID = 91947958;
    , Caption='\<PostaLite';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"PliteClick")    
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (cp_PrintCondition (this.parent.parent.parent.parent , 'Btn_Plite_Edit') )
      endwith
    endif
  endfunc

  func oBtn_1_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso)
     endwith
    endif
  endfunc


  add object oBtn_1_33 as ButtonMenu with uid="GHFIMVPFCJ",left=541, top=144, width=48,height=47,;
    CpPicture="..\exe\bmp\pdfarch.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_ARCHIVING_DOCUMENTS+"";
    , HelpContextID = 153590002;
    , Caption="Arc.\<doc.";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"SaveIndClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not .pdProcesso AND  .w_treport<>'I'   AND  .w_FRXGRPFILE)
      endwith
    endif
  endfunc

  func oBtn_1_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! isAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_34 as ButtonMenu with uid="OGTSETTKJY",left=541, top=144, width=48,height=47,;
    CpPicture="bmp\Archeasy.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_ARCHIVING_ARCHEASY_DOCUMENTS+"";
    , HelpContextID = 233899385;
    , Caption="Arc\<heasy";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"StpArchiClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_treport<>'I'  AND  IIF(g_ARCHI='S',.T.,.F.) And  (NOT .cNomeReport.GetText(.nIndexReport) OR cp_PrintCondition (this.parent.parent.parent.parent , 'Btn_Ante_Edit')  )  And not .PDProcesso   AND .w_SELECTED>0 AND  .w_FRXGRPFILE   AND NOT bReportVuoto )
      endwith
    endif
  endfunc

  func oBtn_1_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_36 as ButtonMenu with uid="BDHEEBNWXU",left=600, top=144, width=48,height=47,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = ""+MSG_EXIT+"";
    , HelpContextID = 29708789;
    , Caption=MSG_S_EXIT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"EscClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_41 as cp_runprogram with uid="STMQUKDDXO",left=-5, top=211, width=230,height=28,;
    caption='Inizializza',;
   bGlobalFont=.t.,;
    prg="CP_CHPFUN('init')",;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Inizializza maschera";
    , HelpContextID = 217059238


  add object oObj_1_43 as cp_runprogram with uid="SGRAMLPXRJ",left=231, top=214, width=230,height=28,;
    caption='Inizializza',;
   bGlobalFont=.t.,;
    prg="CP_CHPFUN('Opt_Language Changed')",;
    cEvent = "w_Opt_Language Changed",;
    nPag=1;
    , ToolTipText = "Inizializza maschera";
    , HelpContextID = 217059238


  add object oObj_1_44 as cp_runprogram with uid="LYSIXNDBCA",left=231, top=245, width=230,height=28,;
    caption='Inizializza',;
   bGlobalFont=.t.,;
    prg="CP_CHPFUN('BlankRecEnd')",;
    cEvent = "Blank",;
    nPag=1;
    , ToolTipText = "Inizializza maschera";
    , HelpContextID = 217059238


  add object oObj_1_46 as cp_runprogram with uid="MKCBIYQIKE",left=231, top=277, width=230,height=28,;
    caption='Inizializza',;
   bGlobalFont=.t.,;
    prg="CP_CHPFUN('NumeroCopie')",;
    cEvent = "w_Txt_Copies Changed",;
    nPag=1;
    , ToolTipText = "Al cambio del numero copie si aggiorna lo zooom a pag 2";
    , HelpContextID = 217059238


  add object oObj_1_47 as cp_runprogram with uid="BWAEJBXSMD",left=231, top=307, width=230,height=28,;
    caption='Inizializza',;
   bGlobalFont=.t.,;
    prg="CP_CHPFUN('CambioStampante')",;
    cEvent = "w_DEVICE Changed",;
    nPag=1;
    , ToolTipText = "Al cambio del numero copie si aggiorna lo zooom a pag 2";
    , HelpContextID = 217059238

  add object oStr_1_22 as StdString with uid="SHCDKROQHP",Visible=.t., Left=12, Top=3,;
    Alignment=0, Width=120, Height=18,;
    Caption=""+MSG_LANGUAGE+""  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="BQSULVPABR",Visible=.t., Left=114, Top=69,;
    Alignment=0, Width=120, Height=18,;
    Caption=""+MSG_FILE_NAME+""  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="UOQLUQMRXZ",Visible=.t., Left=12, Top=69,;
    Alignment=0, Width=148, Height=18,;
    Caption=""+MSG_PRINT_ON_FILE+""  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="KGOBYFPQME",Visible=.t., Left=506, Top=7,;
    Alignment=1, Width=139, Height=18,;
    Caption=""+MSG_COPIES_NO_F+""  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="RJGGVUYORV",Visible=.t., Left=116, Top=3,;
    Alignment=0, Width=120, Height=18,;
    Caption=""+MSG_PRINTER+""  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="ZXOIZTONXD",Visible=.t., Left=203, Top=120,;
    Alignment=0, Width=326, Height=18,;
    Caption=""+MSG_WORKING_PROGRESS+""  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (NOT this.Parent.ocontained.pdProcesso)
    endwith
  endfunc

  add object oBox_1_38 as StdBox with uid="ZTNPVHYKLO",left=2, top=136, width=645,height=1

  add object oBox_1_39 as StdBox with uid="DAXSITZOYC",left=2, top=61, width=645,height=1
enddefine
define class tcp_chp3Pag2 as StdContainer
  Width  = 859
  height = 193
  stdWidth  = 859
  stdheight = 193
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Zoom_Printer as cp_szoombox with uid="ZHYPWGJHCG",left=-2, top=-1, width=654,height=127,;
    caption='Zoom_Printer',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.t.,cZoomFile="GSUT_KRL",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,cTable="AZIENDA",cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.f.,bNoZoomGridShape=.f.,;
    nPag=2;
    , ToolTipText = "Doppio click per cambiare la stampante del report selezionato";
    , HelpContextID = 31222107


  add object oBtn_2_12 as ButtonMenu with uid="ZPVAYUWSLC",left=338, top=129, width=48,height=47,;
    CpPicture="..\exe\bmp\run16.ico", caption="", nPag=2;
    , ToolTipText = "Visual Query";
    , HelpContextID = 186321164;
    , Caption="\<Query" , disabledpicture="..\exe\bmp\RUN16D.ico";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_12.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"QueryClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_treport <> 'I' AND cp_isAdministrator(.t.) AND not .pdProcesso AND .w_SELECTED>0)
      endwith
    endif
  endfunc

  func oBtn_2_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ( vartype ( this.Parent.oContained.w_tReport  )='C' AND .w_tReport <> 'I' AND cp_isAdministrator(.t.) and vartype(g_Debug_Print)='C' and g_Debug_Print='S'))
     endwith
    endif
  endfunc


  add object oBtn_2_13 as ButtonMenu with uid="IUSVCZZALP",left=390, top=129, width=48,height=47,;
    CpPicture="..\exeBMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza cursore";
    , HelpContextID = 252355687;
    , Caption="\<Browse" ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"BrowseClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_treport <> 'I' AND cp_isAdministrator(.t.))
      endwith
    endif
  endfunc

  func oBtn_2_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT (  .w_tReport <> 'I' AND cp_isAdministrator(.t.) ))
     endwith
    endif
  endfunc


  add object oBtn_2_14 as ButtonMenu with uid="NUOPZJSCRA",left=441, top=129, width=48,height=47,;
    CpPicture="PRASSOC.ico", caption="", nPag=2;
    , ToolTipText = ""+MSG_CONNECT_REPORT_TO_PRINTER+"";
    , HelpContextID = 186254264;
    , Caption=MSG_REPORT_BS_PRINTER ,Disabledpicture ="..\exe\bmp\prassocd.ico";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_14.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"AssocClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_treport <> 'I' AND NOT .cNomeReport.IsEmpty()  AND not .pdProcesso )
      endwith
    endif
  endfunc


  add object oBtn_2_15 as ButtonMenu with uid="TPMPZXYMVN",left=493, top=129, width=48,height=47,;
    CpPicture="MODIFTBL.ico", caption="", nPag=2;
    , ToolTipText = ""+MSG_CREATE_BS_MODIFY_REPORT+"";
    , HelpContextID = 96035463;
    , Caption=MSG_CHANGE_BUTTON2 , Disabledpicture = "MODIFTBD.ico";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_15.Click()
      with this.Parent.oContained
        cp_DoAction ("ecpSecurity")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_treport <> 'I' AND .cNomeReport.IsReport()  AND NOT .cNomeReport.IsEmpty()  AND  .w_FRXGRPFILE)
      endwith
    endif
  endfunc


  add object oBtn_2_16 as ButtonMenu with uid="VIPYCQGTTW",left=543, top=129, width=48,height=47,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per cambiare la stampante dei report selezionati";
    , HelpContextID = 15608246;
    , Caption='\<Cambia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_16.Click()
      with this.Parent.oContained
        GSUT_BRL(this.Parent.oContained,"P", .cNomeReport)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_treport <> 'I' AND .w_SELECTED<>0  AND NOT .cNomeReport.IsEmpty())
      endwith
    endif
  endfunc

  func oBtn_2_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MOD_SEL='C')
     endwith
    endif
  endfunc


  add object oBtn_2_17 as ButtonMenu with uid="OTRYDVZYMN",left=594, top=129, width=48,height=47,;
    CpPicture="ESC.ico", caption="", nPag=2;
    , ToolTipText = ""+MSG_EXIT+"";
    , HelpContextID = 29708789;
    , Caption=MSG_S_EXIT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_17.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"EscClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZ_2_24 as StdRadio with uid="EVWZMNCSKG",rtseq=27,rtrep=.f.,left=7, top=128, width=138,height=36;
    , cFormVar="w_SELEZ", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZ_2_24.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 181221126
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 181221126
      this.Buttons(2).Top=17
      this.SetAll("Width",136)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZ_2_24.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZ_2_24.GetRadio()
    this.Parent.oContained.w_SELEZ = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ_2_24.SetRadio()
    this.Parent.oContained.w_SELEZ=trim(this.Parent.oContained.w_SELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ=='S',1,;
      iif(this.Parent.oContained.w_SELEZ=='D',2,;
      0))
  endfunc

  add object oStr_2_6 as StdString with uid="MRMGOYYYRX",Visible=.t., Left=156, Top=129,;
    Alignment=0, Width=94, Height=17,;
    Caption="Report solo testo"    , forecolor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_8 as StdString with uid="MGOSZSMGGW",Visible=.t., Left=156, Top=143,;
    Alignment=0, Width=94, Height=17,;
    Caption="Report grafici"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_10 as StdString with uid="PAADJQXHZD",Visible=.t., Left=156, Top=162,;
    Alignment=0, Width=20, Height=18,;
    Caption="EX"    , backcolor=RGB(90,200,70), forecolor=RGB(90,200,70), BackStyle=1;
  ;
  , bGlobalFont=.t.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (NOT ('E' $ .w_EXCLWRD))
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="SWKTAFPYAD",Visible=.t., Left=182, Top=163,;
    Alignment=0, Width=146, Height=17,;
    Caption="Modello Excel presente"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (NOT ('E' $ .w_EXCLWRD))
    endwith
  endfunc

  add object oStr_2_18 as StdString with uid="JRPWMRBLOJ",Visible=.t., Left=156, Top=177,;
    Alignment=0, Width=22, Height=17,;
    Caption="WR"    , backcolor=RGB(0,0,255), forecolor=RGB(0,0,255), BackStyle=1;
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (NOT ('W' $ .w_EXCLWRD))
    endwith
  endfunc

  add object oStr_2_19 as StdString with uid="MJBTXRRLWZ",Visible=.t., Left=182, Top=177,;
    Alignment=0, Width=146, Height=17,;
    Caption="Modello Word presente"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_19.mHide()
    with this.Parent.oContained
      return (NOT ('W' $ .w_EXCLWRD))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_chp3','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_chp3
define class StdZLanCombo  as  StdCombo 
  func RadioValue()
    return  " "
  endfunc
  
  func GetRadio()
    return  " "
  endfunc
  
  func SetRadio()
    return  " "
  endfunc
enddefine


func isPrinter ()
  APRINTERS(Stampanti) 
  IF type('Stampanti')='U'
     return .F.
    ELSE
     return .T.
  ENDIF
endfunc


**** Zucchetti Aulla Inizio: Funzioni utilizzate in report per invio caratteri di controllo
Function Set10CPI()
  if ts_Enabled
     return iif(empty(ts_10Cpi),'',eval(ts_10Cpi))
  else
     return ''
  endif   
endfunc

Function Set12CPI()
  if ts_Enabled
     return iif(empty(ts_12Cpi),'',eval(ts_12Cpi))
  else
     return ''
  endif   
endfunc

Function Set15CPI()
  if ts_Enabled
     return iif(empty(ts_15Cpi),'',eval(ts_15Cpi))
  else
     return ''
  endif   
endfunc


Function SetCompress()
  if ts_Enabled
     return iif(empty(ts_Comp),'',eval(ts_Comp))
  else
     return ''
  endif   
endfunc

Function ResetCompress()
  if ts_Enabled
     return iif(empty(ts_RtComp),'',eval(ts_RtComp))
  else
     return ''
  endif   
endfunc

Function SetBold()
  if ts_Enabled
     return iif(empty(ts_StBold),'',eval(ts_StBold))
  else
     return ''
  endif   
endfunc

Function ResetBold()
  if ts_Enabled
     return iif(empty(ts_FiBold),'',eval(ts_FiBold))
  else
     return ''
  endif   
endfunc

Function SetDouble()
  if ts_Enabled
     return iif(empty(ts_StDoub),'',eval(ts_StDoub))
  else
     return ''
  endif   
endfunc

Function ResetDouble()
  if ts_Enabled
     return iif(empty(ts_FiDoub),'',eval(ts_FiDoub))
  else
     return ''
  endif   
endfunc

Function SetItalics()
  if ts_Enabled
     return iif(empty(ts_StItal),'',eval(ts_StItal))
  else
     return ''
  endif   
endfunc

Function ResetItalics()
  if ts_Enabled
     return iif(empty(ts_FiItal),'',eval(ts_FiItal))
  else
     return ''
  endif   
endfunc

Function SetUnderline()
  if ts_Enabled
     return iif(empty(ts_StUnde),'',eval(ts_StUnde))
  else
     return ''
  endif   
endfunc

Function ResetUnderline()
  if ts_Enabled
     return iif(empty(ts_FiUnde),'',eval(ts_FiUnde))
  else
     return ''
  endif   
endfunc
* --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
* --- Fine Area Manuale
