* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_viewtrace_b                                                  *
*              Visualizza log                                                  *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-24                                                      *
* Last revis.: 2012-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_viewtrace_b",oParentObject,m.pAZIONE)
return(i_retval)

define class tcp_viewtrace_b as StdBatch
  * --- Local variables
  pAZIONE = space(1)
  w_SHOWDBF_CURS = .NULL.
  w_MESS = space(10)
  w_SHOWDBF_CURS = .NULL.
  w_GETFILE = space(250)
  w_POSIZIONE = 0
  w_RECCOUNT = 0
  w_NUMEROELEMENTI = 0
  w_ELEMENTOCORRENTE = 0
  w_CP_LOGUNION = .NULL.
  * --- WorkFile variables
  show_log_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine di gestione dell' Activity logger viewer
    *     pAZIONE = 'D' mostra i dati dell'istruzione select corrente memorizzati in un DBF al momento dell'esecuzione del programma tracciato
    *     pAZIONE = 'Z' azzera il risultato della traccia corrente
    *     pAZIONE = 'F' Seleziona un file di log da visualizzare
    *     pAZIONE = 'Q' Lanciato dal bottone ricerca a pag2. Si posiziona sul primo record in cui la stringa 'Cerca in frase SQL' (pag2) � contenuta nella frase sql o nel messaggio di errore
    *     pAZIONE = 'R' Lanciato dal bottone ricerca a pag1. Richiede una stringa e si posiziona sul primo record in cui la stringa stessa � contenuta nella frase sql o nel messaggio di errore
    *     pAZIONE = 'S' Lanciato dal bottone 'Successivo' ricerca a pag1. Si posiziona sul record successivo che soddisfa la ricerca fatta con parametro Q o R
    * --- --
    * --- --Filtri checkbox sulla seconda pagina
    do case
      case this.pAZIONE = "D"
        * --- Mostra i dati dell'istruzione eseguita
        if NOT EMPTY(this.oParentObject.w_DATI)
          if FILE( ALLTRIM( this.oParentObject.w_NOMECURSOREDATI ) + ".DBF" )
            VecchioErrore3 = ON("error")
            bErroreAperturaDBF=.f. 
 ON ERROR bErroreAperturaDBF=.t.
            this.w_SHOWDBF_CURS = SYS( 2015 )
            SELECT * FROM ( this.oParentObject.w_NOMECURSOREDATI ) INTO CURSOR ( this.w_SHOWDBF_CURS )
            do CP_SHOWDBF with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if USED( JUSTFNAME( this.oParentObject.w_NOMECURSOREDATI ) )
              SELECT( JUSTFNAME( this.oParentObject.w_NOMECURSOREDATI ) )
              USE
            endif
            if bErroreAperturaDBF
              cp_ErrorMsg("Non � possibile aprire il DBF","!","")
            endif
            ON ERROR &VecchioErrore3
          else
            if inlist(i_nACTIVATEPROFILER, 1, 3)
              cp_ErrorMsg("Non � possibile visualizzare i dati perch� il seguente file DBF non � stato trovato:"+chr(13)+ALLTRIM( this.oParentObject.w_NOMECURSOREDATI ) + ".DBF","!","" )
            else
              this.w_MESS = "Non � possibile visualizzare i dati perch� il seguente file DBF non � stato trovato:"+chr(13)+ALLTRIM( this.oParentObject.w_NOMECURSOREDATI ) + ".DBF"
              this.w_MESS = this.w_MESS + CHR(13) + "Provare ad eseguire l'operazione da questa postazione:"+chr(13)+ALLTRIM(this.oParentObject.w_HOSTNAME)
              cp_ErrorMsg(this.w_MESS,"!","" )
            endif
          endif
        else
          this.oParentObject.w_ALCMDSQL = " "
          this.w_SHOWDBF_CURS = SYS( 2015 )
          CREATE CURSOR CURS_SHOW (CAMPO C (1))
          INSERT INTO CURS_SHOW (CAMPO) VALUES (" ")
          SELECT * FROM CURS_SHOW INTO CURSOR ( this.w_SHOWDBF_CURS )
          do CP_SHOWDBF with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pAZIONE = "Z"
        * --- Svuota il log
        * --- Mostra i dati dell'istruzione eseguita
        if CP_YESNO("Si desidera azzerare il log?")
          if inlist(i_nACTIVATEPROFILER, 1, 3)
            * --- Caso in cui si utilizza il DBF
            VecchioErrore3 = ON("error")
            bErroreAperturaDBF=.f. 
 ON ERROR bErroreAperturaDBF=.t.
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if FILE( this.oParentObject.w_FILEDILOG )
              * --- prima di eliminare il logo devo elminare tutti i dbf di lettura ad esso associati
              if not used(i_nACTIVATEPROFILER_ALIAS)
                * --- Attiva il cursore del log per poter eseguire la scansione
                if i_nACTIVATEPROFILER_TRACCIATURAEVENTI = 0
                  i_nACTIVATEPROFILER_TRACCIATURAEVENTI = 1
                  cp_ActivityLogger(DATETIME(), SECONDS(), SECONDS(), "UEV", "", this, 0, 0, "")
                  i_nACTIVATEPROFILER_TRACCIATURAEVENTI = 0
                else
                  cp_ActivityLogger(DATETIME(), SECONDS(), SECONDS(), "UEV", "", this, 0, 0, "")
                endif
              endif
              Select( i_nACTIVATEPROFILER_ALIAS ) 
 Go Top 
 Scan For not empty(nvl(ALCRDATI,""))
              * --- Provo ad elminiare il dbf se non riesco lo lascio, non diamo avvisi
              TRY 
 cp_msg("Eliminazione DBF..",.t.,.f.,0,.f.) 
 Drop Table ( ADDBS(JUSTPATH(this.oParentObject.w_FILEDILOG) )+ALLTRIM(ALCRDATI)+".dbf" ) 
 CATCH 
 ENDTRY
              EndScan
              DELETE FROM ( this.oParentObject.w_FILEDILOG )
            else
              bErroreAperturaDBF=.t.
            endif
            if bErroreAperturaDBF
              cp_ErrorMsg("Non � possibile azzerare il log","!","")
            endif
            ON ERROR &VecchioErrore3
            this.oParentObject.w_ELENCO.QUERY() 
 this.oParentObject.w_ELENCO.REFRESH()
            * --- Attiva lo zoom
            this.oParentObject.Notifyevent("PopolaZoom")
          else
            * --- Caso in cui si utilizza la tabella sul database
            i_nACTIVATEPROFILER_OLD = i_nACTIVATEPROFILER
            i_nACTIVATEPROFILER = 0
            * --- Sospende momentaneamente l'attivit� di log
            * --- Select from show_log
            i_nConn=i_TableProp[this.show_log_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.show_log_idx,2],.t.,this.show_log_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" show_log ";
                   ,"_Curs_show_log")
            else
              select * from (i_cTable);
                into cursor _Curs_show_log
            endif
            if used('_Curs_show_log')
              select _Curs_show_log
              locate for 1=1
              do while not(eof())
              if NOT EMPTY( NVL( ALCRDATI, "" ) )
                * --- Provo ad elminiare il dbf se non riesco lo lascio, non diamo avvisi
                TRY 
 cp_msg("Eliminazione DBF..",.t.,.f.,0,.f.) 
 Drop Table ( ADDBS( this.oParentObject.w_CARTELLALOG )+ALLTRIM(ALCRDATI)+".dbf" ) 
 CATCH 
 ENDTRY
              endif
                select _Curs_show_log
                continue
              enddo
              use
            endif
            * --- Delete from show_log
            i_nConn=i_TableProp[this.show_log_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.show_log_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"1 = "+cp_ToStrODBC(1);
                     )
            else
              delete from (i_cTable) where;
                    1 = 1;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            this.oParentObject.notifyevent("Popola2Zoom")
            i_nACTIVATEPROFILER = i_nACTIVATEPROFILER_OLD
          endif
        endif
      case this.pAZIONE = "F"
        * --- Seleziona un file di log da visualizzare
        this.w_GETFILE = GETFILE( "DBF" , "Selezionare un file di log" )
        if " " $ ALLTRIM( this.w_GETFILE )
          AH_ERRORMSG("Selezionare un percorso e un nome di file che non contengano spazi")
        else
          this.oParentObject.w_FILEDILOG = this.w_GETFILE
        endif
      case this.pAZIONE = "Q" OR this.pAZIONE = "R" OR this.pAZIONE = "S"
        * --- R ricerca
        * --- S ricerca successivo
        * --- Q ricerca da pag2
        * --- Ricerca la frase SQL contenuta nella variabile w_CONTENUTOINFRASESQL
        SELECT( this.oParentObject.w_ELENCO.cCURSOR )
        if this.pAZIONE = "R" OR this.pAZIONE = "Q"
          if this.pAZIONE = "R"
            * --- Chiede la stringa solo se lanciato da pag2
            this.oParentObject.w_CONTENUTOINFRASESQL = INPUTBOX("Cerca in istruzione SQL:", i_MsgTitle)
          endif
          * --- Cerca la prima occorrenza all'interno dell'istruzione SQL (ALCMDSQL) oppure nel messaggio di errore ( ALERROR )
          *     La ricerca avviene tramite l'operatore $ (contenuto in).
          L_FRASE = "LOCATE FOR " + CP_TOSTR( UPPER( ALLTRIM( this.oParentObject.w_CONTENUTOINFRASESQL ) ) ) + " $ UPPER( ALCMDSQL ) OR " + CP_TOSTR( UPPER( ALLTRIM( this.oParentObject.w_CONTENUTOINFRASESQL ) ) ) + " $ UPPER( ALERROR )"
          L_FILTRO = UPPER(ALLTRIM(this.oParentObject.w_CONTENUTOINFRASESQL))
          L_FRASE = "LOCATE FOR UPPER(ALLTRIM(this.oParentObject.w_CONTENUTOINFRASESQL)) $ UPPER( ALCMDSQL ) OR UPPER(ALLTRIM(this.oParentObject.w_CONTENUTOINFRASESQL)) $ UPPER( ALERROR )"
        else
          * --- Cerca l'occorrenza successiva dell'ultima locate applicata
          L_FRASE = "CONTINUE"
        endif
        if NOT EMPTY( this.oParentObject.w_CONTENUTOINFRASESQL )
          if this.oParentObject.w_ELENCO.VISIBLE
            SELECT( this.oParentObject.w_ELENCO.cCURSOR )
            this.w_POSIZIONE = RECNO( this.oParentObject.w_ELENCO.cCURSOR )
            this.w_RECCOUNT = RECCOUNT( this.oParentObject.w_ELENCO.cCURSOR )
          else
            SELECT( this.oParentObject.w_ELENCO2.cCURSOR )
            this.w_POSIZIONE = RECNO( this.oParentObject.w_ELENCO2.cCURSOR )
            this.w_RECCOUNT = RECCOUNT( this.oParentObject.w_ELENCO2.cCURSOR )
          endif
          &L_FRASE
          if FOUND()
            if this.oParentObject.opgfrm.activepage <> 1
              * --- Se la ricerca � lanciata dal bottone sulla seconda pagina
              this.oParentObject.opgfrm.activepage=1
            endif
            * --- Abilita il bottone per la ricerca del record successivo
            this.oParentObject.w_TROVASUCCESSIVO = .T.
          else
            if this.w_POSIZIONE >0 AND this.w_POSIZIONE <= this.w_RECCOUNT
              * --- La ricerca � fallita viene ripristinata la posizione originale e disabilito la ricerca successiva
              GOTO this.w_POSIZIONE
              if this.pAZIONE = "R"
                CP_ERRORMSG("La ricerca non ha prodotto risultati" , "i" , "" )
              else
                CP_ERRORMSG("� stata raggiunta la fine del log" , "i" , "" )
              endif
              this.oParentObject.w_TROVASUCCESSIVO = .f.
            endif
          endif
          * --- Aggiorna lo zoom e passa il controllo alla griglia
          if this.oParentObject.w_ELENCO.VISIBLE
            this.oParentObject.w_ELENCO.REFRESH()     
            this.oParentObject.w_ELENCO.SETFOCUS()     
          else
            this.oParentObject.w_ELENCO2.REFRESH()     
            this.oParentObject.w_ELENCO2.SETFOCUS()     
          endif
        endif
      case this.pAZIONE = "U"
        * --- Costruisce l'elenco dei dbf che possono essere uniti
        ADIR(A_ELENCOFILEDILOG,ALLTRIM(this.oParentObject.w_CARTELLALOG)+"*.dbf")
        * --- A_ELENCOFILEDILOG contiene l'elenco dei dbf presenti nella cartella adibita a log.
        *     Di questi verranno tenuti solo quelli che contengono i campi previsti
        CREATE CURSOR ELENCODBFDAUNIRE( NOMECURS C(250) )
        =WRCURSOR("ELENCODBFDAUNIRE")
        this.w_NUMEROELEMENTI = ALEN( A_ELENCOFILEDILOG , 1)
        this.w_ELEMENTOCORRENTE = 1
        do while this.w_ELEMENTOCORRENTE <= this.w_NUMEROELEMENTI
          * --- Controlla se il DBF corrente contiene i campi previsti
          L_ISTRUZIONETEST = "SELECT * FROM " + ALLTRIM( this.oParentObject.w_CARTELLALOG ) + ALLTRIM( A_ELENCOFILEDILOG( this.w_ELEMENTOCORRENTE , 1 ) ) + " WHERE 1=0 INTO CURSOR TESTCAMPI"
          &L_ISTRUZIONETEST
          if TYPE( "TESTCAMPI.ALDATTIM" ) = "T" AND TYPE( "TESTCAMPI.AL____MS" ) = "N" AND TYPE( "TESTCAMPI.AL__TIPO" ) = "C" AND TYPE( "TESTCAMPI.ALCMDSQL" ) = "M"
            if LEN( ALLTRIM( this.oParentObject.w_CARTELLALOG ) + ALLTRIM( A_ELENCOFILEDILOG( this.w_ELEMENTOCORRENTE , 1 ) ) ) <= 254
              SELECT( "ELENCODBFDAUNIRE" )
              APPEND BLANK
              REPLACE NOMECURS WITH ALLTRIM( this.oParentObject.w_CARTELLALOG ) + ALLTRIM( A_ELENCOFILEDILOG( this.w_ELEMENTOCORRENTE , 1 ) )
            endif
          endif
          this.w_ELEMENTOCORRENTE = this.w_ELEMENTOCORRENTE + 1
        enddo
        this.w_CP_LOGUNION = cp_logunion( this )
        if USED( "TESTCAMPI" )
          SELECT( "TESTCAMPI" )
          USE
        endif
        SELECT( "ELENCODBFDAUNIRE" )
        BROWSE
      case this.pAZIONE = "1"
        * --- Abilita tutti i filtri check
        this.oParentObject.w_FILT_ASI = "S"
        this.oParentObject.w_FILT_BTD = "S"
        this.oParentObject.w_FILT_BTO = "S"
        this.oParentObject.w_FILT_DBD = "S"
        this.oParentObject.w_FILT_DBI = "S"
        this.oParentObject.w_FILT_DBM = "S"
        this.oParentObject.w_FILT_DBR = "S"
        this.oParentObject.w_FILT_DBT = "S"
        this.oParentObject.w_FILT_DBW = "S"
        this.oParentObject.w_FILT_DBX = "S"
        this.oParentObject.w_FILT_DEA = "S"
        this.oParentObject.w_FILT_EVENT = "S"
        this.oParentObject.w_FILT_FCD = "S"
        this.oParentObject.w_FILT_FCO = "S"
        this.oParentObject.w_FILT_GSD = "S"
        this.oParentObject.w_FILT_GSO = "S"
        this.oParentObject.w_FILT_RIC = "S"
        this.oParentObject.w_FILT_TASTIFUN = "S"
        this.oParentObject.w_FILT_TBT = "S"
        this.oParentObject.w_FILT_TCT = "S"
        this.oParentObject.w_FILT_TRT = "S"
        this.oParentObject.w_FILT_VQR = "S"
      case this.pAZIONE = "0"
        * --- Disabilita tutti i filtri check
        this.oParentObject.w_FILT_ASI = "N"
        this.oParentObject.w_FILT_BTD = "N"
        this.oParentObject.w_FILT_BTO = "N"
        this.oParentObject.w_FILT_DBD = "N"
        this.oParentObject.w_FILT_DBI = "N"
        this.oParentObject.w_FILT_DBM = "N"
        this.oParentObject.w_FILT_DBR = "N"
        this.oParentObject.w_FILT_DBT = "N"
        this.oParentObject.w_FILT_DBW = "N"
        this.oParentObject.w_FILT_DBX = "N"
        this.oParentObject.w_FILT_DEA = "N"
        this.oParentObject.w_FILT_EVENT = "N"
        this.oParentObject.w_FILT_FCD = "N"
        this.oParentObject.w_FILT_FCO = "N"
        this.oParentObject.w_FILT_GSD = "N"
        this.oParentObject.w_FILT_GSO = "N"
        this.oParentObject.w_FILT_RIC = "N"
        this.oParentObject.w_FILT_TASTIFUN = "N"
        this.oParentObject.w_FILT_TBT = "N"
        this.oParentObject.w_FILT_TCT = "N"
        this.oParentObject.w_FILT_TRT = "N"
        this.oParentObject.w_FILT_VQR = "N"
      case this.pAZIONE = "X"
        * --- Disabilita tutti i filtri check
        this.oParentObject.w_FILT_ASI = IIF(this.oParentObject.w_FILT_ASI="S", "N", "S")
        this.oParentObject.w_FILT_BTD = IIF(this.oParentObject.w_FILT_BTD="S", "N", "S")
        this.oParentObject.w_FILT_BTO = IIF(this.oParentObject.w_FILT_BTO="S", "N", "S")
        this.oParentObject.w_FILT_DBD = IIF(this.oParentObject.w_FILT_DBD="S", "N", "S")
        this.oParentObject.w_FILT_DBI = IIF(this.oParentObject.w_FILT_DBI="S", "N", "S")
        this.oParentObject.w_FILT_DBM = IIF(this.oParentObject.w_FILT_DBM="S", "N", "S")
        this.oParentObject.w_FILT_DBR = IIF(this.oParentObject.w_FILT_DBR="S", "N", "S")
        this.oParentObject.w_FILT_DBT = IIF(this.oParentObject.w_FILT_DBT="S", "N", "S")
        this.oParentObject.w_FILT_DBW = IIF(this.oParentObject.w_FILT_DBW="S", "N", "S")
        this.oParentObject.w_FILT_DBX = IIF(this.oParentObject.w_FILT_DBX="S", "N", "S")
        this.oParentObject.w_FILT_DEA = IIF(this.oParentObject.w_FILT_DEA="S", "N", "S")
        this.oParentObject.w_FILT_EVENT = IIF(this.oParentObject.w_FILT_EVENT="S", "N", "S")
        this.oParentObject.w_FILT_FCD = IIF(this.oParentObject.w_FILT_FCD="S", "N", "S")
        this.oParentObject.w_FILT_FCO = IIF(this.oParentObject.w_FILT_FCO="S", "N", "S")
        this.oParentObject.w_FILT_GSD = IIF(this.oParentObject.w_FILT_GSD="S", "N", "S")
        this.oParentObject.w_FILT_GSO = IIF(this.oParentObject.w_FILT_GSO="S", "N", "S")
        this.oParentObject.w_FILT_RIC = IIF(this.oParentObject.w_FILT_RIC="S", "N", "S")
        this.oParentObject.w_FILT_TASTIFUN = IIF(this.oParentObject.w_FILT_TASTIFUN="S", "N", "S")
        this.oParentObject.w_FILT_TBT = IIF(this.oParentObject.w_FILT_TBT="S", "N", "S")
        this.oParentObject.w_FILT_TCT = IIF(this.oParentObject.w_FILT_TCT="S", "N", "S")
        this.oParentObject.w_FILT_TRT = IIF(this.oParentObject.w_FILT_TRT="S", "N", "S")
        this.oParentObject.w_FILT_VQR = IIF(this.oParentObject.w_FILT_VQR="S", "N", "S")
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if EMPTY( this.oParentObject.w_FILEDILOG )
      * --- Cartella contenente i file di log
      if DIRECTORY(i_cACTIVATEPROFILER_POSIZIONELOG)
        this.oParentObject.w_CARTELLALOG = ADDBS(i_cACTIVATEPROFILER_POSIZIONELOG)
      else
        this.oParentObject.w_CARTELLALOG = ADDBS(tempadhoc()+"Logger")
        i_cACTIVATEPROFILER_POSIZIONELOG=this.oParentObject.w_CARTELLALOG
      endif
      * --- Nome fisico del DBF da mostrare
      if FILE(ALLTRIM(this.oParentObject.w_CARTELLALOG)+ALLTRIM(i_cACTIVATEPROFILER_NOMELOG)+".DBF")
        this.oParentObject.w_FILEDILOG = ALLTRIM(this.oParentObject.w_CARTELLALOG)+ALLTRIM(i_cACTIVATEPROFILER_NOMELOG)+".DBF"
      else
        if FILE(this.oParentObject.w_CARTELLALOG+"ACTIVITYLOGGER.DBF")
          this.oParentObject.w_FILEDILOG = this.oParentObject.w_CARTELLALOG+"ACTIVITYLOGGER.DBF"
        else
          this.oParentObject.w_FILEDILOG = SPACE( 250 )
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='show_log'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_show_log')
      use in _Curs_show_log
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
