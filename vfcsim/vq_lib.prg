* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VQ_LIB
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Libreria per le query
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

* -------------------------------------------------------------------------------
* Classi per le listbox con editing
* -------------------------------------------------------------------------------
*--- Classe che gestisce l'array che conterr� gli elementi della lista
Define Class ArrList As Custom
    Dimension List(1,1)	&&Array
    nDimList=1	&&Dimensione attuale Array
    oParentObject=.Null.  &&Puntatore alla lista
    bThis=.F.	&&flag per evitare This_ACCESS
    bNoList=.F.  &&flag aggiornamento lista se a true non aggiorna la lista
    Function List_ASSIGN(tvValue, tnRow, tnCol)
        Local oldbThis, cValue
        oldbThis=This.bThis
        *--- Pongo bThis a true affinch� non si vada in loop quando eseguo l'assegnamento alla lista (this.oParentObject.list(tnRow, tnCol)=cValue)
        This.bThis=.T.
        cValue=Chrtran(Alltrim(tvValue), Chr(10)+Chr(13), '')
        *--- valorizzo la lista affinch� a video veda i campi valorizzati
        If !This.bNoList
            This.oParentObject.List(tnRow, tnCol)=cValue
        Endif
        *--- valorizzo l'array di appoggio
        This.List[tnRow,IIF(VARTYPE(tnCol)="L",1,tnCol)]=cValue
        *--- rimetto bThis al vecchio valore
        This.bThis=oldbThis
        Return

    Function List_ACCESS(tnRow, tnCol)
        Return This.List[tnRow,IIF(VARTYPE(tnCol)="L",1,tnCol)]

    Procedure Destroy()
        This.oParentObject=.Null.
    Endproc
Enddef

Define Class ListboxWithContainer As ListBox
    nRow=0  && il contatore della riga passata al contenitore
    nY=0
    oList=.Null.	&&puntatore all' oggetto che contiene l'array di appoggio
    *--- Devo utilizzare la init perch� non � possibile aggiungere l'oggetto oList tramite ADD OBJECT
    Procedure Init()
        This.oList=Createobject("ArrList")
        *--- Valorizzare oParentObject pu� creare dei problemi nella chiusura della maschera Query
        *--- per evitare la mancata chiusura della finestra i vari oParentObject sono stati messi a null all'interno della
        *--- funzione Exit
        This.oList.oParentObject=This
        DoDefault()
    Endproc

    *--- Ridefinisco l'accesso alla classe, quando entra in causa la propriet� list restituisco il puntatore alla classe di appoggio
    *--- anzich� this
    Procedure This_ACCESS(cMemberName)
        If Lower(cMemberName)=='list' And !This.oList.bThis
            Return This.oList
        Endif
        Return This
    Endproc

    *--- Ridefinisco la AddItem per gestire l'array di appoggio
    Procedure AddItem(cValue)
        Local i
        Dimension This.oList.List(This.oList.nDimList,This.ColumnCount)
        *--- Assegno un nuovo elemento all'arry
        This.oList.List(This.oList.nDimList,1)=cValue
        For i=2 To This.ColumnCount
            This.oList.List(This.oList.nDimList,i)=""
        Next
        This.oList.nDimList=This.oList.nDimList+1
        *--- non eseguo il default perch� l'incremento della lista � gi� avvenuto quando ho assegnato cValue al primo elemento
        *--- dell'array di appoggio (List_ASSIGN)
        Nodefault
    Endproc
    *--- Ridefinisco la RemoveItem per gestire l'array di appoggio, non serve il dodefault() perch� viene eseguito ugualmente
    Procedure RemoveItem(nRow)
        Adel(This.oList.List,nRow)
        This.oList.nDimList=This.oList.nDimList-1
        Dimension This.oList.List(This.oList.nDimList,This.ColumnCount)
    Endproc
    *--- Ridefinisco la Clear per gestire l'array di appoggio, non serve il dodefault() perch� viene eseguito ugualmente
    Procedure Clear()
        Dimension This.oList.List(1,This.ColumnCount)
        This.oList.nDimList=1
    Endproc
    *--- Ridefinosco OnMoveItem
    Procedure OnMoveItem(nSource, nShift, nCurrentIndex, nMoveBy)
        If nCurrentIndex+nMoveBy>0 And nCurrentIndex+nMoveBy<=This.ListCount
            Local i
            Dimension aItem(1,This.ColumnCount)
            For i=1 To This.ColumnCount
                aItem(1,i)=This.oList.List(nCurrentIndex,i)
            Next
            Adel(This.oList.List,nCurrentIndex)
            Ains(This.oList.List,nCurrentIndex+nMoveBy)
            This.oList.bNoList=.T.
            For i=1 To This.ColumnCount
                This.oList.List(nCurrentIndex+nMoveBy,i)=aItem(1,i)
            Next
            This.oList.bNoList=.F.
        Endif
        DoDefault()
    Endproc

    Proc MouseDown(nButton,nShift,nX,nY)
        This.nY=nY
        *--- Copy list
        Local nCmd, bSkipPaste, i, bSkipSel
        If nButton=2
            nCmd = 0
            bSkipPaste=!("* --- CodePainter Revolution VQR "+Alltrim(Upper(This.Name))+" Vers. 1.0"$_Cliptext)
            bSkipSel=.T.
            For i=1 To This.ListCount
                If This.SelectedID(i)
                    bSkipSel=.F.
                Endif
            Next
            *--- Creo menu
            Define Popup popCmd From Mrow(),Mcol() SHORTCUT
            Define Bar 1 Of popCmd Prompt cp_Translate(MSG_CUT) Skip For bSkipSel
            On Selection Bar 1 Of popCmd nCmd = 1
            Define Bar 2 Of popCmd Prompt cp_Translate(MSG_COPY) Skip For bSkipSel
            On Selection Bar 2 Of popCmd nCmd = 2
            Define Bar 3 Of popCmd Prompt cp_Translate(MSG_PASTE) Skip For bSkipPaste
            On Selection Bar 3 Of popCmd nCmd = 3
            Define Bar 4 Of popCmd Prompt cp_Translate(MSG_DELETE) Skip For bSkipSel
            On Selection Bar 4 Of popCmd nCmd = 4
            If Alltrim(Upper(This.Name)) == 'FDLFIELDS'
                Define Bar 5 Of popCmd Prompt "\-"
                Define Bar 6 Of popCmd Prompt "MAX()" Skip For bSkipSel
                On Selection Bar 6 Of popCmd nCmd = 6
                Define Bar 7 Of popCmd Prompt "MIN()" Skip For bSkipSel
                On Selection Bar 7 Of popCmd nCmd = 7
                Define Bar 8 Of popCmd Prompt "SUM()" Skip For bSkipSel
                On Selection Bar 8 Of popCmd nCmd = 8
                Define Bar 9 Of popCmd Prompt cp_Translate(MSG_REMOVE_AGGREGATE_FUN) Skip For bSkipSel
                On Selection Bar 9 Of popCmd nCmd = 9
            * Zucchetti Aulla Inizio: aggiunta funzione CASTP(char(n))
            Define Bar 10 Of popCmd Prompt "\-"
            Define Bar 11 Of popCmd Prompt "CASTP(CHAR(n))" Skip For bSkipSel
            On Selection Bar 11 Of popCmd nCmd = 11
            Define Bar 12 Of popCmd Prompt " Rimuovi CASTP(CHAR(n))" Skip For bSkipSel
            On Selection Bar 12 Of popCmd nCmd = 12
            * Zucchetti Aulla Fine: aggiunta funzione CASTP(char(n))
            Endif
            Activate Popup popCmd
            Deactivate Popup popCmd
            Release Popups popCmd
            *--- Azioni
            Do Case
                Case nCmd=1 Or nCmd=4
                    *--- Cut & Delete
                    If nCmd=1
                        CopyVQR(This, This.Name)
                    Endif
                    *--- Elimino
                    Do Case
                        Case Alltrim(Upper(This.Name)) == 'FDLFIELDS'
                            This.Parent.BtnDelFld.Click()
                        Case Alltrim(Upper(This.Name)) == 'RLLRELATION'
                            This.Parent.BtnDelRel.Click()
                        Case Alltrim(Upper(This.Name)) == 'FLLFILTER'
                            This.Parent.BtnDelFil.Click()
                        Case Alltrim(Upper(This.Name)) == 'FPLPARAM'
                            This.Parent.BtnDelFp.Click()
                    Endcase
                Case nCmd=2
                    *--- Copy
                    CopyVQR(This, This.Name)
                Case nCmd=3
                    *--- Paste
                    PasteVQR(This, This.Name)
                Case nCmd=6
                    *--- Max
                    InsCmdVQR(This, "MAX", 2)
                Case nCmd=7
                    *--- Min
                    InsCmdVQR(This, "MIN", 2)
                Case nCmd=8
                    *--- Sum
                    InsCmdVQR(This, "SUM", 2)
                Case nCmd=9
                    *--- Remove aggregate function
                    RemCmdVQR(This, "", 2)
            * Zucchetti Aulla Inizio: aggiunta funzione CASTP(char(n))
        Case nCmd=11
            *--- Cast Char (forza il tipo cast, utile per Postgres)
            InsCmdVQR(This, "CASTP", 2)
        Case nCmd=12
            RemCmdVQR(This, "CASTP", 2)
            * Zucchetti Aulla Inizio: aggiunta funzione CASTP(char(n))
            Endcase
        Endif
        *--- Copy list
        *  local oRow
        *  if this.nRow<>0
        *    oRow=this.GetRowContainer()
        *    if !isnull(oRow)
        *      oRow.SetValues(this,this.nRow)
        *      oRow.visible=.f.
        *      this.nRow=0
        *    endif
        *  endif
    Proc MouseUp(nButton,nShift,nX,nY)
        Local i,j,rh,rr,dh,dx,oo,oRow
        Nodefault
        oRow=This.GetRowContainer()
        If This.nY<>nY And This.MoverBars
            For i=1 To This.ListCount
                This.Selected(i)=.F.
            Next
        Endif
        If !Isnull(oRow) And This.nY=nY
            * --- trova la riga selezionata
            j=0
            ns=0
            For i=1 To This.ListCount
                If This.Selected(i)
                    j=i
                    ns=ns+1
                Endif
            Next
            j=This.ListIndex
            If ns<=1
                * --- trova la riga in base alla posizione del click
                rh=Fontmetric(3,This.FontName,This.FontSize)+Fontmetric(1,This.FontName,This.FontSize)-1
                If i_VisualTheme=-1
                    dh=0
                Else
                    If Type("Thisform.oDec")='O'
                    	dh=Thisform.oDec.Height-3
                    Else
	                    dh=2
                    Endif
                Endif
                dx=0
                oo=This
                Do While oo.BaseClass<>'Form'
                    If !Inlist(oo.BaseClass,'Page','Pageframe')
                        dh=dh+oo.Top
                        dx=dx+oo.Left
                    Endif
                    If oo.BaseClass=='Pageframe'
                        If i_VisualTheme=-1
                            dh=dh+30
                        Else
                            dh=dh+26
                        Endif
                    Endif
                    oo=oo.Parent
                Enddo
                rr=Int((nY-dh)/rh)
                * --- se c'e' un container, lo attiva
                oRow.SetPos(This.Top+2+rr*rh,This.Left+Iif(This.MoverBars,20,1))
                If j<>0 And nX-dx<This.Width-20 And nY-dh>0 And rr<This.ListCount
                    oRow.GetValues(This,j)
                    This.nRow=j
                    oRow.Visible=.T.
                    oRow.ZOrder(0)
                    oRow.SetFocus()
                Else
                    This.nRow=0
                    oRow.Visible=.F.
                    *this.additem('')
                    *oRow.BlankValues()
                    *this.nRow=this.listcount
                Endif
            Endif
        Endif
    Func GetRowContainer()
        Return(.Null.)
Enddefine

*--- Inserisce comandi (Es. Max(),Min())
Procedure InsCmdVQR(obj, pCmd, pPos)
    Local i,j
    For i=1 To obj.ListCount
        If obj.Selected(i)
            * Zucchetti Aulla Inizio: aggiunta funzione CASTP(char(n))
            If pCmd="CASTP"
                If Left(obj.List(i,4),1)="C"
                    If obj.List(i,5)="0"
                        * Determino la lunghezza del campo dall'analisi
                        i_cExpr=Alltrim(obj.List(i,pPos))
                        If "." $ i_cExpr And i_cExpr==Chrtran(i_cExpr,"[]'","")
                            If Inlist(Left(i_cExpr,4),"MIN(","MAX(","SUM(")
                                i_cExpr = Alltrim(Substr(i_cExpr,5))
                                i_cExpr = Left(i_cExpr,Len(i_cExpr)-1)
                            Endif
                            i_cType = cp_FieldType(i_cExpr)
                            If Left(i_cType,1)="C"
                                i_cType = Chrtran(i_cType, "C()", "")
                                If Val(i_cType)>0
                                    obj.List(i,5)=i_cType
                                Endif
                            Endif
                        Endif
                    Endif
                    If obj.List(i,5)>"0"
                        obj.List(i,pPos)="[CASTP("+Alltrim(obj.List(i,pPos))+" AS CHAR(" +Alltrim(obj.List(i,5))+ "))]"
                    Endif
                Endif
            Else
                obj.List(i,pPos)=pCmd+"("+Alltrim(obj.List(i,pPos))+")"
            EndIf
            * Zucchetti Aulla Fine: aggiunta funzione CASTP(char(n))
            obj.Parent.Parent.Parent.Parent.oqc.bModified = .T.
        Endif
    Next
Endproc
*--- Rimuove funzioni di aggregazione (Es. Max(),Min())
Procedure RemCmdVQR(obj, pCmd, pPos)
    Local i,j, nInitLen, nTimes
    If Empty(m.pCmd)
        For i=1 To obj.ListCount
	        m.nInitLen = Len(Alltrim(obj.List(i,pPos)))
            If obj.Selected(i) And ("MIN("$Alltrim(obj.List(i,pPos)) Or "MAX("$Alltrim(obj.List(i,pPos)) Or "SUM("$Alltrim(obj.List(i,pPos)))
                obj.List(i,pPos)=Strtran(Alltrim(obj.List(i,pPos)), "MIN(", "")
                obj.List(i,pPos)=Strtran(Alltrim(obj.List(i,pPos)), "MAX(", "")
                obj.List(i,pPos)=Strtran(Alltrim(obj.List(i,pPos)), "SUM(", "")
                m.nTimes = Int((m.nInitLen - Len(Alltrim(obj.List(i,pPos))))/4) &&Conto quante volte � stata utilizzata la StrTran
                If Replicate(')',m.nTimes) = Right(Alltrim(obj.List(i,pPos)),m.nTimes)
                    obj.List(i,pPos)=Left(Alltrim(obj.List(i,pPos)), Len(Alltrim(obj.List(i,pPos)))-m.nTimes)
                Endif
            Endif
        Next
    Endif
* Zucchetti Aulla Inizio: aggiunta funzione CAST(char(n))
If m.pCmd="CASTP"
    For i=1 To obj.ListCount
        If obj.Selected(i) And Left(Alltrim(obj.List(i,pPos)),7)="[CASTP(" And " AS CHAR("$Alltrim(obj.List(i,pPos))
            obj.List(i,pPos)=Substr(Alltrim(obj.List(i,pPos)),8)
            obj.List(i,pPos)=Alltrim(Left(obj.List(i,pPos), Rat(" AS CHAR(",obj.List(i,pPos))))
        Endif
    Next
Endif
* Zucchetti Aulla Fine: aggiunta funzione CAST(char(n))
	obj.Parent.Parent.Parent.Parent.oqc.bModified = .T.
Endproc
*--- Copia nella clipboard i dati della lista
Procedure CopyVQR(obj, pOper)
    Local i,j,l_Cliptext
    m.l_Cliptext=""
    m.l_Cliptext="* --- CodePainter Revolution VQR "+Alltrim(Upper(m.pOper))+" Vers. 1.0"+Chr(13)
    For m.i=1 To m.obj.ListCount
        If m.obj.Selected(i)
            For m.j=1 To m.obj.ColumnCount
                m.l_Cliptext=m.l_Cliptext+Vartype(m.obj.List(m.i,m.j))+Transform(m.obj.List(m.i,m.j))+Chr(13)
            Next
        Endif
    Next
	_Cliptext = m.l_Cliptext
Endproc
*--- Esegue paste dalla clipboard
Procedure PasteVQR(obj, pOper)
    Local i,j, cTxtFields, tmpListItem
    If  "* --- CodePainter Revolution VQR "+Alltrim(Upper(pOper))+" Vers. 1.0"$_Cliptext
        *--- Paste Fields
        cTxtFields=Substr(_Cliptext, Len("* --- CodePainter Revolution VQR "+Alltrim(Upper(pOper))+" Vers. 1.0")+2)
        If Alines(lfields, cTxtFields) > 1
            i=1
            Do While i <= Alen(lfields)
                tmpListItem=Iif(Left(lfields[i],1)<>'C',Val(Substr(lfields[i],2)), Substr(lfields[i],2))
                obj.AddItem(tmpListItem)
                For j=2 To obj.ColumnCount
                    tmpListItem=Iif(Left(lfields[i+j-1],1)<>'C',Val(Substr(lfields[i+j-1],2)), Substr(lfields[i+j-1],2))
                    obj.List(obj.ListCount,j)=tmpListItem
                Next
                i=i+obj.ColumnCount
            Enddo
            obj.Parent.Parent.Parent.Parent.oqc.bModified = .T.
        Endif
    Endif
Endproc

Define Class ContainerForListbox As Container
    Visible=.F.
    BorderWidth=0
    Proc SetValues(oList,nRow)
    Proc GetValues(oList,nRow)
    Proc BlankValues()
    Proc SetPos(nTop,nLeft)
        This.Top=nTop
        This.Left=nLeft
    Proc LostFocus()
        Local o
        o=This.GetList()
        If !Isnull(o) And o.nRow<>0
            This.SetValues(o,o.nRow)
            o.nRow=0
            This.Visible=.F.
        Endif
    Func GetList()
        Return(.Null.)
Enddefine

* -------------------------------------------------------------------------------
* Classi per il grafo della query
* -------------------------------------------------------------------------------

Define Class query_graph As CpSysContainer
    oqc=.Null.
    Closable=.F.
    Left = 420
    Height=305
    Width = 370
    Caption=""
    Icon=i_cBmpPath+i_cStdIcon
    BackStyle=1
    Add Object vq As painter_graph With Height=This.Height,Width=This.Width,BorderWidth=0
    #If Version(5)>=900
        Add Object ImgBackGround As Image With Stretch=2, Visible=.F.
    #Endif
    Proc Init(oqc)
        This.oqc=oqc
        && This.Left =(This.Parent.Width/2)
        && This.Width=(This.Parent.Width/2)
        && This.Height=This.Parent.Height/2
        This.vq.Width=This.Width
        This.vq.Height=This.Height
        This.Caption=cp_Translate(MSG_DESIGN)
        If i_VisualTheme<>-1
            If i_bGradientBck
                With This.ImgBackGround
                    .Width = This.Width
                    .Height = This.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            This.BackColor = i_ThemesManager.GetProp(7)
        Else
            This.BackColor = Rgb(252,252,254)
        Endif
    Proc Resize()
        This.vq.Width=This.Width
        This.vq.Height=This.Height
    Proc AddTable(cName,cAlias,cDescr,nX,nY)
        Local o,x,Y
        If Pcount()<4 Or nX=0
            x=5
            Y=5
            For i=1 To This.vq.ControlCount
                If At('pg_node_',This.vq.Controls(i).Name)=1 And This.vq.Controls(i).Top=Y And This.vq.Controls(i).Left=x
                    x=x+20
                    Y=Y+20
                    i=1
                Endif
            Next
        Else
            x=nX
            Y=nY
        Endif
        o=This.vq.AddNode(x,Y,'query_table')
        o.SetName(cName,cAlias,cDescr)
    Proc AddField(cAlias,cDescr)
        Local i
        For i=1 To This.vq.ControlCount
            If At('pg_node_',This.vq.Controls(i).Name)=1
                If This.vq.Controls(i).cAlias==cAlias
                    This.vq.Controls(i).shp.AddItem(cDescr)
                Endif
            Endif
        Next
        *--- Cambio colore link
    Proc Addrelation(cAlias1,cAlias2,cTypeLink)
        Local i,oStart,oEnd
        * --- DEVONO ESISTERE GIA' LE TABELLE
        For i=1 To This.vq.ControlCount
            If At('pg_node_',This.vq.Controls(i).Name)=1
                If This.vq.Controls(i).cAlias==cAlias1
                    oStart=This.vq.Controls(i)
                Endif
                If This.vq.Controls(i).cAlias==cAlias2
                    oEnd=This.vq.Controls(i)
                Endif
            Endif
        Next
        *--- Cambio colore link
        This.vq.AddLink(oStart,oEnd,Iif(cTypeLink="Right outer",'query_link_right', Iif(cTypeLink="Left outer",'query_link_left','query_link')))
    Proc RemoveTable(cAlias)
        Local i,cStart,cEnd
        LOCAL TestMacro
        i=1
        Do While i<=This.vq.ControlCount
            If At('pg_link_',This.vq.Controls(i).Name)=1
                cStart=This.vq.Controls(i).cStart
                cEnd=This.vq.Controls(i).cEnd
                TestMacro=This.vq.&cStart..cAlias==cAlias Or This.vq.&cEnd..cAlias==cAlias
                If TestMacro
                    This.vq.RemoveObject(This.vq.Controls(i).Name)
                    i=i-1
                Endif
            Endif
            i=i+1
        Enddo
        i=1
        Do While i<=This.vq.ControlCount
            If At('pg_node_',This.vq.Controls(i).Name)=1 And This.vq.Controls(i).cAlias==cAlias
                This.vq.RemoveObject(This.vq.Controls(i).Name)
                i=i-1
            Endif
            i=i+1
        Enddo
    Proc RemoveField(cAlias,cDescr)
        Local i,j
        For i=1 To This.vq.ControlCount
            If At('pg_node_',This.vq.Controls(i).Name)=1
                If This.vq.Controls(i).cAlias==cAlias
                    j=1
                    Do While j<=This.vq.Controls(i).shp.ListCount
                        If This.vq.Controls(i).shp.List(j)==cDescr
                            This.vq.Controls(i).shp.RemoveItem(j)
                        Else
                            j=j+1
                        Endif
                    Enddo
                Endif
            Endif
        Next
    Proc RemoveRelation(cAlias1,cAlias2)
        Local i,cStart,cEnd
        LOCAL TestMacro
        i=1
        Do While i<=This.vq.ControlCount
            If At('pg_link_',This.vq.Controls(i).Name)=1
                cStart=This.vq.Controls(i).cStart
                cEnd=This.vq.Controls(i).cEnd
                TestMacro=(This.vq.&cStart..cAlias==cAlias1 And This.vq.&cEnd..cAlias==cAlias2) Or (This.vq.&cEnd..cAlias==cAlias1 And This.vq.&cStart..cAlias==cAlias2)
                If TestMacro
                    This.vq.RemoveObject(This.vq.Controls(i).Name)
                    i=i-1
                Endif
            Endif
            i=i+1
        Enddo

    Proc RemoveAllRelation()
        Local i,cStart,cEnd
        i=1
        Do While i<=This.vq.ControlCount
            If At('pg_link_',This.vq.Controls(i).Name)=1
                This.vq.RemoveObject(This.vq.Controls(i).Name)
                i=i-1
            Endif
            i=i+1
        Enddo
    Proc RemoveAll()
        Do While This.vq.ControlCount>0
            This.vq.RemoveObject(This.vq.Controls(1).Name)
        Enddo
    Func GetX(cAlias)
        Local i
        For i=1 To This.vq.ControlCount
            If At('pg_node_',This.vq.Controls(i).Name)=1 And This.vq.Controls(i).cAlias==cAlias
                Return(This.vq.Controls(i).Left)
            Endif
        Next
        Return(0)
    Func GetY(cAlias)
        Local i
        For i=1 To This.vq.ControlCount
            If At('pg_node_',This.vq.Controls(i).Name)=1 And This.vq.Controls(i).cAlias==cAlias
                Return(This.vq.Controls(i).Top)
            Endif
        Next
        Return(0)
    Proc vq.DragOver(oSource, nXCoord, nYCoord, nState)
        DoDefault(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='ltables' Or Lower(oSource.Name)=='lrelated' Or Lower(oSource.Name)=='lfields' Or Lower(oSource.Name)=='ltbls'
            Do Case
                Case nState=0 && enter
                    This.cOldIcon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
    Proc vq.DragDrop(oSource, nXCoord, nYCoord)
        DoDefault(oSource, nXCoord, nYCoord)
        Local i, cmd
        Thisform.LockScreen=.T.
        If Lower(oSource.Name)=='ltables' Or Lower(oSource.Name)=='ltbls'
            oSource.Drag(2)
            cmd=oSource.Name
            oSource.Parent.&cmd..DblClick()
            * --- sposta la tabella appena aggiunta
            i=Alltrim(Str(This.nNodes))
            This.pg_node_&i..Left=nXCoord-This.Left-This.Parent.Parent.Parent.Parent.Left
            If i_VisualTheme<>-1 And i_cMenuTab<>"S"
				This.pg_node_&i..Top=nYCoord-This.Parent.Parent.Parent.Top-This.Parent.Parent.Parent.Parent.Top-Sysmetric(9)
			Else
				This.pg_node_&i..Top=nYCoord-OBJTOCLIENT(This, 1)+This.Parent.Parent.Parent.Parent.Top-Sysmetric(9)
			EndIf
            This.RecalcLinksCoords(This.pg_node_&i)
        Endif
        If Lower(oSource.Name)=='lrelated'
            oSource.Drag(2)
            oSource.Parent.lrelated.DblClick()
        Endif
        If Lower(oSource.Name)=='lfields'
            oSource.Drag(2)
            For nCnt = 1 To oSource.ListCount
                If oSource.Selected(nCnt)  && Is item selected?
                    oSource.Parent.lfields._Dblclick(nCnt)
                Endif
            Endfor
        Endif
        Thisform.LockScreen=.F.
Enddefine

Define Class query_table As painter_graph_node
    Top=25
    Left=25
    Width=50
    Height=70
    SpecialEffect=0
    * ---
    cName=''
    cAlias=''
    cDescr=''
    * ---
    Add Object lbl As Label With Caption='?',Top=2,Left=2,FontBold=.T.,Alignment=2,ForeColor=Rgb(255,255,255),BackColor=Rgb(0,0,128),FontName='MS Sans Serif'
    *add object shp as shape with top=18,left=2,backcolor=rgb(255,255,255),width=this.width-7,height=this.height-20
    Add Object shp As ListBox With Top=18,Left=2,Width=This.Width-7,Height=This.Height-20,FontSize=6,FontName='MS Sans Serif'
    Proc Click()
        This.Parent.Parent.oqc.oDic.SelectTable(This.cName,This.cDescr,This.cAlias)
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        DoDefault(oSource,nXCoord,nYCoord,nState)
        This.Parent.DragOver(oSource,nXCoord,nYCoord,nState)
    Proc DragDrop(oSource,nXCoord,nYCoord)
        DoDefault(oSource,nXCoord,nYCoord)
        If Lower(oSource.Name)=='lfields'
            This.Parent.DragDrop(oSource,nXCoord,nYCoord)
        Endif
    Proc lbl.DragDrop(oSource,nXCoord,nYCoord)
        This.Parent.DragDrop(oSource,nXCoord,nYCoord)
    Proc lbl.DragOver(oSource,nXCoord,nYCoord,nState)
        This.Parent.DragOver(oSource,nXCoord,nYCoord,nState)
    Proc lbl.MouseMove(nButton, nShift, nXCoord, nYCoord)
        This.Parent.MouseMove(nButton, nShift, nXCoord, nYCoord)
    Proc lbl.Click()
        This.Parent.Click()
    Proc shp.DragDrop(oSource,nXCoord,nYCoord)
        This.Parent.DragDrop(oSource,nXCoord,nYCoord)
    Proc shp.DragOver(oSource,nXCoord,nYCoord,nState)
        This.Parent.DragOver(oSource,nXCoord,nYCoord,nState)
    Proc shp.MouseMove(nButton, nShift, nXCoord, nYCoord)
        This.Parent.MouseMove(nButton, nShift, nXCoord, nYCoord)
    Proc shp.Click()
        This.Parent.Click()
    Func GetLinkClass(oNode)
        Return('query_link')
    Func CanLink(oNode)
        Local i,j
        If !This.IsNewLink(oNode)
            CP_MSG(cp_Translate(MSG_LINK_DUPLICATED),.F.)
            Return(.F.)
        Endif
        * ---
        Local oDcx,cExp,cDescr,cType,bIsChild
        cExp=''
        cType=''
        cDescr=''
        oDcx=This.Parent.Parent.oqc.oDcx
        For i=1 To oDcx.GetFKCount(This.cName)
            If oDcx.GetFKTable(This.cName,i)==oNode.cName
                cExp=oDcx.GetFKExpr(This.cName,i,oNode.cAlias,This.cAlias)
                cDescr=This.cDescr+','+oNode.cDescr
                bIsChild=oDcx.GetFKIsChild(This.cName,i)
                cType=Iif(bIsChild,'Right outer','Left outer')
            Endif
        Next
        For i=1 To oDcx.GetIFKCount(This.cName)
            If oDcx.GetIFKTable(This.cName,i)==oNode.cName
                cExp=oDcx.GetIFKExpr(This.cName,i,This.cAlias,oNode.cAlias)
                cDescr=This.cDescr+','+oNode.cDescr
                bIsChild=oDcx.GetIFKIsChild(This.cName,i)
                cType=Iif(bIsChild,'Left outer','Right outer')
            Endif
        Next
        If Empty(cExp)
            CP_MSG(cp_Translate(MSG_RELATIONSHIP_DEFINITIONS_MISSING),.F.)
            Return(.F.)
        Endif
        This.Parent.Parent.oqc.Addrelation(This.cAlias,oNode.cAlias,cDescr,cExp,cType,.F.)
        * ---
        Return(.T.)
    Func IsNewLink(oNode)
        Local i,j
        j=0
        For i=1 To This.Parent.ControlCount
            If At('pg_link_',This.Parent.Controls(i).Name)=1
                If (This.Parent.Controls(i).cStart==This.Name And This.Parent.Controls(i).cEnd==oNode.Name) Or;
                        (This.Parent.Controls(i).cStart==oNode.Name And This.Parent.Controls(i).cEnd==This.Name)
                    j=j+1
                Endif
            Endif
        Next
        Return(j=0)
    Proc SetName(cName,cAlias,cDescr)
        Local w
        w=Max(Txtwidth(cDescr,'Arial',9)*Fontmetric(6,'Arial',9)+8,60)
        This.Width=w+4
        This.lbl.Width=w
        This.lbl.Caption=cDescr
        This.cName=cName
        This.cAlias=cAlias
        This.cDescr=cDescr
        This.shp.Width=w
Enddefine

*--- Cambio colore link
Define Class query_link_right As painter_graph_link
    BorderColor=Rgb(0,0,200)
Enddefine

Define Class query_link_left As painter_graph_link
    BorderColor=Rgb(200,0,0)
Enddefine

Define Class query_link As painter_graph_link
    BorderColor=Rgb(0,200,0)
Enddefine

* -------------------------------------------------------------------------------
* classi che definiscono il generico painter di grafi
* -------------------------------------------------------------------------------

Define Class painter_graph As Container
    BackStyle = 0
    nNodes=0
    nLinks=0
    cOldIcon=''
    Func AddNode(nX,nY,cNodeClass)
        Local i_n,i_c
        i_c=Iif(Pcount()>2,cNodeClass,'painter_graph_node')
        This.nNodes=This.nNodes+1
        i_n='pg_node_'+Alltrim(Str(This.nNodes))
        This.AddObject(i_n,i_c)
        This.&i_n..Top=nY
        This.&i_n..Left=nX
        This.&i_n..Visible=.T.
        Return(This.&i_n)
    Func AddLink(oStart,oEnd,cLinkClass)
        Local i_n,i_c
        i_c=Iif(Pcount()>2,cLinkClass,'painter_graph_link')
        This.nLinks=This.nLinks+1
        i_n='pg_link_'+Alltrim(Str(This.nLinks))
        This.AddObject(i_n,i_c)
        This.&i_n..cStart=oStart.Name
        This.&i_n..cEnd=oEnd.Name
        This.&i_n..SetCoords()
        This.&i_n..Visible=.T.
        Return(This.&i_n)
    Proc DragDrop(oSource,nXCoord,nYCoord)
        If At('pg_node_',oSource.Name)=1 And oSource.Parent.Name==This.Name
            oSource.Drag(2)
            oSource.Top=oSource.Top+nYCoord-oSource.noy
            oSource.Left=oSource.Left+nXCoord-oSource.nox
            This.RecalcLinksCoords(oSource)
        Endif
    Proc RecalcLinksCoords(oTable)
        Thisform.LockScreen=.T.
        For i=1 To This.ControlCount
            If At('pg_link_',This.Controls(i).Name)=1 And ((This.Controls(i).cStart==oTable.Name)Or(This.Controls(i).cEnd==oTable.Name))
                This.Controls(i).SetCoords()
            Endif
        Next
        Thisform.LockScreen=.F.
Enddefine

Define Class painter_graph_node As Container
    Top=0
    Left=0
    Width=50
    Height=50
    nox=0
    noy=0
    SpecialEffect=0
    cOldIcon=''
    Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            This.nox=nXCoord
            This.noy=nYCoord
        Endif
    Proc MouseMove(nButton, nShift, nXCoord, nYCoord)
        If nButton=1 And Abs(nXCoord-This.nox)+Abs(nYCoord-This.noy)>4
            This.nox=nXCoord
            This.noy=nYCoord
            This.DragIcon=''
            This.Drag(1)
        Endif
    Proc DragOver(oSource, nXCoord, nYCoord, nState)
        If At('pg_node_',oSource.Name)=1 And oSource.Name!=This.Name And oSource.Parent.Name==This.Parent.Name
            Do Case
                Case nState=0 && enter
                    This.cOldIcon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
    Proc DragDrop(oSource,nXCoord,nYCoord)
        Do Case
            Case oSource.Name==This.Name And oSource.Parent.Name==This.Parent.Name
                oSource.Drag(2)
                oSource.Top=oSource.Top+nYCoord-oSource.noy
                oSource.Left=oSource.Left+nXCoord-oSource.nox
                This.Parent.RecalcLinksCoords(oSource)
            Case At('pg_node_',oSource.Name)=1 And oSource.Parent.Name==This.Parent.Name
                oSource.Drag(2)
                If oSource.CanLink(This)
                    This.Parent.AddLink(This,oSource,oSource.GetLinkClass(This))
                Endif
        Endcase
    Func GetLinkClass(oEnd)
        Return('painter_graph_link')
    Func CanLink(oEnd)
        Return(.T.)
Enddefine

Define Class painter_graph_link As Line
    cStart=''
    cEnd=''
    bCenterToCenter=.T.
    Proc DragDrop(oSource,nXCoord,nYCoord)
        * --- passa il drop al form, non si fa mai il drop su un link
        This.Parent.DragDrop(oSource,nXCoord,nYCoord)
    Proc SetCoords()
        Local sx,sy,dx,dy,oStart,oEnd,i_n
        i_n=This.cStart
        oStart=This.Parent.&i_n
        i_n=This.cEnd
        oEnd=This.Parent.&i_n
        If This.bCenterToCenter
            sy=oStart.Top+oStart.Height/2 && centro con centro
        Else
            sy=oStart.Top+oStart.Height && come analisi di codepainter
        Endif
        sx=oStart.Left+oStart.Width/2
        dy=oEnd.Top+oEnd.Height/2-sy
        *dy=this.oEnd.top+this.oEnd.height-sy && link contrari rispetto codepainter
        dx=oEnd.Left+oEnd.Width/2-sx && come analisi di codepainter
        This.LineSlant=Iif(dx*dy<0,'/','\')
        If dx<0
            sx=sx+dx
            dx=-dx
        Endif
        If dy<0
            sy=sy+dy
            dy=-dy
        Endif
        This.Top=sy
        This.Left=sx
        This.Width=dx
        This.Height=dy
        This.ZOrder(1)
Enddefine

* -----------------------------------------------------------------------------------------
* Classi per il microtool
* -----------------------------------------------------------------------------------------
Define Class microtool_toolbar As CommandBar
    cFileName=''
    cFileExt=''
    bModified=.F.
    cToolName='Undef. Tool'
    bEmpty = .T.
    Procedure Init(bStyle)
        DoDefault(m.bStyle)
        With This
            .Caption='ToolBar'
            .ControlBox=.F.
            Local oBtn
            *--- New Btn
            Local oBtn
            m.oBtn = .AddButton("newbtn")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "new.bmp"
            m.oBtn._OnClick = "This.parent.newbtn_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_NEW)
            m.oBtn.Visible=.T.
            *--- Load Btn
            m.oBtn = .AddButton("loadbtn")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "folder.bmp"
            m.oBtn._OnClick = "This.parent.loadbtn_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_OPEN)
            m.oBtn.Visible = .T.
            *--- Save Btn
            m.oBtn = .AddButton("savebtn")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "save.bmp"
            m.oBtn._OnClick = "This.parent.savebtn_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_SAVE)
            m.oBtn.Visible = .T.
            m.oBtn.Enabled = cp_set_get_right()>=3 && Zucchetti Aulla  - Framework monitor
            *--- SaveAs Btn
            m.oBtn = .AddButton("saveasbtn")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "saveas.bmp"
            m.oBtn._OnClick = "This.parent.saveasbtn_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_SAVE_AS)
            m.oBtn.Visible = .T.
            m.oBtn.Enabled = cp_set_get_right()>=4 && Zucchetti Aulla  - Framework monitor
            *--- Exit Btn
            m.oBtn = .AddButton("exbtn")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "ESCTb.bmp"
            m.oBtn._OnClick = "This.parent.exbtn_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_EXIT)
            m.oBtn.Visible = .T.
            *--- Sblocco in caso si _susp_ la libreria se nuova interfaccia
            If Vartype(i_ThemesManager)='O'  And i_VisualTheme <>-1
                i_ThemesManager.LockVfp(.F., .T.)
            Endif
        Endwith
    Endproc

    * -- Zucchetti Aulla Inizio - Framework monitor
    Function MonitorGetFileName()
        If Empty(This.cFileName)
            Local old_default_path
            old_default_path = Alltrim(Sys(5))+Alltrim(Sys(2003))
            Set Default To Alltrim(old_default_path)+"\custom\userdir"
            This.cFileName=Getfile(This.cExt)
            Set Default To Alltrim(old_default_path)
            If !Empty(This.cFileName)
                This.SetTitle()
            Endif
        Endif
        Return(Not(Empty(This.cFileName)))
    Endfunc

    Function MonitorPutFileName()
        * setta a .t. se l'estensione del file � vrt
       
        vrt_condition = Alltrim(Upper(This.cExt))=="VRT"
        If vrt_condition
            original_file_name = Alltrim(This.cclassname)+".vrt"
            vrt_caption = "Gestione "+Alltrim(This.oform.oformcontainer.cComment)
        Else
            If Empty(This.cFileName)
                original_file_name = "."+Alltrim(This.cExt)
            Else
                original_file_name=Alltrim(This.cFileName)
            Endif
        Endif
        *--- Se VQR non posso salvare per utente o gruppo
        Do cp_savefor_b With This
        If !Empty(Juststem(original_file_name))
            This.cFileName = Alltrim(original_file_name)
            If Not Directory(Alltrim(Justpath(This.cFileName)))
                cp_makedir(Justpath(This.cFileName))
            Endif
            This.SetTitle()
        Endif
        *RETURN(NOT(EMPTY(THIS.cFileName)))
        * necessario se clicco sul bottone cancel della maschera savefor
        Return(Not(Empty(Alltrim(original_file_name))))
    Endfunc
    * -- Zucchetti Aulla Fine - Framework monitor

    Proc Focus()
        Return
    Proc newbtn_click()
        This.Focus()
        If This.cToolName=='Visual Query' And (!This.bEmpty Or This.oform.oPgFrm.PageCount>1)
            This.oform.AddQuery()
        Endif
        If This.cToolName<>'Visual Query' And This.SaveModified()
            This.NewDoc()
            *this.bEmpty = .t.
            This.bModified=.F.
            This.bEmpty=.T.
            This.cFileName=''
            This.SetTitle()
        Endif
    Proc loadbtn_click()
        Local F
        This.Focus()
        If This.cToolName=='Visual Query' Or This.SaveModified()
            F=This.cFileName
            If This.cToolName<>'Visual Query'
                This.cFileName=''
            Endif
            * --- Zucchetti Aulla  - Framework monitor
            If cp_IsDeveloper()
                * --- Zucchetti Aulla  - Framework monitor
                If This.GetFileName()
                    If cp_IsPFile(This.cFileName)
                        This.LoadDoc()
                    Else
                        cp_ErrorMsg(cp_MsgFormat(MSG_FILE__NOT_FOUND_QM,This.cFileName),0,'',.f.)
                        This.cFileName=F
                        If This.cToolName=='Visual Query'
                            This.oform.RemoveQuery(This.oform.oPgFrm.ActivePage)
                        Endif
                        This.SetTitle()
                    Endif
                Else
                    This.cFileName=F
                Endif
                *--- Zucchetti Aulla inizio  - Framework monitor
            Else
                If This.MonitorGetFileName()
                    If cp_IsPFile(This.cFileName)
                        This.LoadDoc()
                    Else
                        cp_ErrorMsg(cp_MsgFormat(MSG_FILE__NOT_FOUND_QM,This.cFileName),0,'',.f.)
                        This.cFileName=F
                        This.SetTitle()
                    Endif
                Else
                    This.cFileName=F
                Endif
            Endif
            *--- Zucchetti Aulla fine - Framework monitor
        Endif
    Endproc
    Proc savebtn_click()
       #Define FILEDESC_IDX 2
		cp_CreateCacheFile()
        This.Focus()
        *--- Zucchetti Aulla inizio  - Framework monitor
        If cp_IsDeveloper()  && verifico se sono sviluppatore
            If This.PutFileName()
                This.SaveDoc()
                This.SetTitle()
                *--- Zucchetti Aulla inizio  - Framework monitor
                If Lower(Alltrim(Justext(This.cFileName)))=="vrt" And i_MonitorFramework
                    Local vrt_campi
                    cp_monitor_SetFields(Alltrim(Upper(".\"+Sys(2014,This.cFileName))),@vrt_campi)
                    vrt_campi[FILEDESC_IDX] = "Gestione "+Alltrim(This.oform.oformcontainer.cComment)
                    Insert Into cp_monitor From Array vrt_campi
                Endif
                *--- Zucchetti Aulla fine  - Framework monitor
            Else
                *This.SetTitle()
            Endif
            Return
        Endif
        If Empty(This.cFileName)
            If !cp_IsDeveloper()  && verifico se sono sviluppatore
                edit_condition = .T.
                vrt_caption = ""
                Local vrt_campi
                If This.MonitorPutFileName()
                    This.SaveDoc()
                 cp_monitor_SetFields(Alltrim(Upper(This.cFileName)),@vrt_campi)
                 If Len(vrt_caption)>0
                    vrt_campi[FILEDESC_IDX] = Alltrim(vrt_caption)
                    Insert Into cp_monitor From Array vrt_campi
                 Endif
                Endif
            Endif && !cp_IsDeveloper()
        Else
            If At(Lower(Alltrim(Sys(2014, i_custompath))),Lower(Alltrim(This.cFileName)))!=0
                This.SaveDoc()
            Else
                This.saveasbtn_Click()
            Endif
        Endif	&& EMPTY(THIS.cfilename)
        *--- Zucchetti Aulla fine  - Framework monitor
    Endproc
    Proc saveasbtn_Click()
        #Define FILEDESC_IDX 2
        Local F
        This.Focus()
		cp_CreateCacheFile()
        *--- Zucchetti Aulla inizio  - Framework monitor
        If cp_IsDeveloper()
            *--- Zucchetti Aulla fine  - Framework monitor
            F=This.cFileName
            This.cFileName=''
            If This.PutFileName()
                This.SaveDoc()
                This.SetTitle()
                *--- Zucchetti Aulla inizio  - Framework monitor
                If Lower(Alltrim(Justext(This.cFileName)))=="vrt" And i_MonitorFramework
                    Local vrt_campi
                    cp_monitor_SetFields(Alltrim(Upper(".\"+Sys(2014,This.cFileName))),@vrt_campi)
                    vrt_campi[FILEDESC_IDX] = "Gestione "+Alltrim(This.oform.oformcontainer.cComment)
                    Insert Into cp_monitor From Array vrt_campi
                Endif
                *--- Zucchetti Aulla fine  - Framework monitor
            Else
                This.cFileName=F
                *This.SetTitle()
            Endif
            *--- Zucchetti Aulla inizio  - Framework monitor
        Else && non sono sviluppatore
            edit_condition = .T.
            vrt_caption = ""
            Local vrt_campi
            If This.MonitorPutFileName()
                This.SaveDoc()
                cp_monitor_SetFields(Alltrim(Upper(This.cFileName)),@vrt_campi)
                If Len(vrt_caption)>0
                    vrt_campi[FILEDESC_IDX] = Alltrim(vrt_caption)
                    Insert Into cp_monitor From Array vrt_campi
                Endif
            Endif
        Endif
        *--- Zucchetti Aulla fine  - Framework monitor
    Proc exbtn_click()
        This.Focus()
        If This.cToolName=='Visual Query' Or This.SaveModified()
            This.Exit()
        Endif
    Func GetFileName()
        Local l_filename
        l_filename = ''
        If This.cToolName=='Visual Query' Or Empty(This.cFileName)
            l_filename=Getfile(This.cExt)
            If !Empty(m.l_filename)
                If This.cToolName=='Visual Query' And !This.bEmpty
                    This.oform.AddQuery()
                Endif
                This.cFileName = m.l_filename
                This.SetTitle()
            Endif
        Endif
        Return(Not(Empty(l_filename)))
    Func PutFileName()
        If Empty(This.cFileName)
            This.cFileName=Putfile('File:',This.cFileName,This.cExt)
            *This.SetTitle()
        Endif
        Return(Not(Empty(This.cFileName)))
    Func SaveModified()
        *--- Zucchetti Aulla inizio - Framework monitor
        * Non chiede di salvare se non hai i diritti di scrittura
        If cp_set_get_right()<3
            Return (.T.)
        Endif
        *--- Zucchetti Aulla fine  - Framework monitor
        If This.bModified
            i=Messagebox(cp_Translate(MSG_SAVE_CHANGES_QP),3+32,This.cToolName)
            If i=2 && Cancel
                Return(.F.)
            Endif
            If i=6 && yes
                *--- Zucchetti Aulla inizio - Framework monitor
                * se ho come diritto il numero 3
                If cp_set_get_right()==3
                    This.saveasbtn_Click()
                Endif
                If cp_set_get_right()>3
                    *--- Zucchetti Aulla fine - Framework monitor
                    This.savebtn_click()
                Endif
                *--- Zucchetti Aulla inizio - Framework monitor
            Endif
            *--- Zucchetti Aulla fine - Framework monitor
        Endif
        Return(.T.)
    Proc SetTitle()
        Return
    Func GetHelpFile()
        Return('visuatol')
    Func GetHelpTopic()
        Return('')
    Func HasCPEvents(i_cOp)
        Return(i_cOp='ecpQuit' Or i_cOp='ecpSecurity')
    Proc ecpQuit()
        This.exbtn_click()
Enddefine

Define Class query_coordinator As microtool_toolbar
    oDic=.Null.
    oGraph=.Null.
    oQuery=.Null.
    oDcx=.Null.
    oRelForm=.Null.
    oFindForm=.Null.
    HelpContextID=1
    Dimension cTables[1,2]
    nTables=0
    Caption=""
    cToolName='Visual Query'
    cExt='vqr'
    cExtMask=''
    bRemoveWhereOnEmptyParam=.F.
    bOrderByParm=.F.
    nVersion=0
    oThis=.Null.
    __sqlx__=.Null.
    *
    bSec1=.T.
    bSec2=.T.
    bSec3=.T.
    bSec4=.T.
    *
    bFox26=.F.

    oWord=.Null.
    oExcel=.Null.

    oform = .Null.

    bCpToolbarVisible = .T.
    bDeskTopBarVisible = .T.

    Procedure Init(bStyle)
        DoDefault(m.bStyle)
        Local oBtn
        With This
            *--- Separatore
            .AddObject("sep","Separator")
            #If Version(5)>=700
                .sep.Visible=.T.
            #Endif
            *--- Run Btn
            m.oBtn = .AddButton("run")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "run.bmp"
            m.oBtn._OnClick = "This.parent.run_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_EXECUTE_QUERY)
            m.oBtn.Visible=.T.
            *--- Sql Btn
            m.oBtn = .AddButton("sql")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "sql.bmp"
            m.oBtn._OnClick = "This.parent.sql_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_SQL_SENTENCE)
            m.oBtn.Visible=.T.
            *--- Prn Btn
            m.oBtn = .AddButton("prn")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "print.bmp"
            m.oBtn._OnClick = "this.parent.DoPrn(.f.)"
            m.oBtn._OnRightClick = "this.parent.DoPrn(.t.)"
            If i_VisualTheme <> -1
                Local oBtnMenu, oMenuItem
                m.oBtnMenu = Createobject("cbPopupMenu")
                m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
                *---Excel
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_REPORT)
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoPrn(.f.)')"
                m.oMenuItem.Picture = "print.ico"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_CREATE_BS_MODIFY_REPORT)
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoPrn(.t.)')"
                m.oMenuItem.Picture = "batch.ico"
                m.oMenuItem.Visible=.T.
                m.oBtnMenu.InitMenu()
                m.oBtn.oPopupMenu = m.oBtnMenu
                m.oBtn.nButtonType = 2
            Endif
            m.oBtn.ToolTipText = cp_Translate(MSG_CREATE_BS_MODIFY_REPORT)
            m.oBtn.Visible=.T.
            *--- Word Btn
            m.oBtn = .AddButton("word")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "prword.bmp"
            m.oBtn._OnClick = "this.parent.DoWord(.f.)"
            m.oBtn._OnRightClick = "this.parent.DoWord(.t.)"
            If i_VisualTheme <> -1
                Local oBtnMenu, oMenuItem
                m.oBtnMenu = Createobject("cbPopupMenu")
                m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
                *---Excel
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_MICROSOFT_WORD_DOCUMENT)
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoWord(.f.)')"
                m.oMenuItem.Picture = "prword.ico"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_MODIFY_BS_CREATE_MSWORD)
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoWord(.t.)')"
                m.oMenuItem.Picture = "batch.ico"
                m.oMenuItem.Visible=.T.
                m.oBtnMenu.InitMenu()
                m.oBtn.oPopupMenu = m.oBtnMenu
                m.oBtn.nButtonType = 2
            Endif
            m.oBtn.ToolTipText = ""
            m.oBtn.Visible=.T.
            *--- Excel Btn
            m.oBtn = .AddButton("excl")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "prexcel.bmp"
            m.oBtn._OnClick = " this.parent.DoExcel(.f.)"
            m.oBtn._OnRightClick = "this.parent.DoExcel(.t.)"
            If i_VisualTheme <> -1
                Local oBtnMenu, oMenuItem
                m.oBtnMenu = Createobject("cbPopupMenu")
                m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_MICROSOFT_EXCEL_DOCUMENT)
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoExcel(.f.)')"
                m.oMenuItem.Picture = "prexcel.ico"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_CREATE_BS_MODIFY_EXCEL_WORKSHEET)
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoExcel(.t.)')"
                m.oMenuItem.Picture = "batch.ico"
                m.oMenuItem.Visible=.T.
                m.oBtnMenu.InitMenu()
                m.oBtn.oPopupMenu = m.oBtnMenu
                m.oBtn.nButtonType = 2
            Endif
            m.oBtn.ToolTipText = ""
            m.oBtn.Visible=.T.
            *--- Grf Btn
            m.oBtn = .AddButton("grf")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "FOXCHARTS.bmp"
            m.oBtn._OnClick = "this.parent.DoGraph(.f., .f.)"
            m.oBtn._OnRightClick = "this.parent.DoGraph(.f., .t.)"
            If i_VisualTheme <> -1
                Local oBtnMenu, oMenuItem
                m.oBtnMenu = Createobject("cbPopupMenu")
                m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
                *---FoxChart
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_EDITOR_FOXCHARTS)
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoGraph(.f., .f.)')"
                m.oMenuItem.Picture = "FOXCHARTS.ico"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_EDITOR_OPEN_FOXCHARTS_D)
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoGraph(.t., .f.)')"
                m.oMenuItem.Picture = "batch.ico"
                m.oMenuItem.Visible=.T.
                *---MsGraph
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_EDITOR_MSGRAPH)
                m.oMenuItem.Picture = "graph.ico"
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoGraph(.f., .t.)')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate(MSG_EDITOR_OPEN_MSGRAPH_D)
                m.oMenuItem.Picture = "batch.ico"
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','DoGraph(.t., .t.)')"
                m.oMenuItem.Visible=.T.
                m.oBtnMenu.InitMenu()
                m.oBtn.oPopupMenu = m.oBtnMenu
                m.oBtn.nButtonType = 2
                m.oBtn.ToolTipText = ""
            Endif
            m.oBtn.Visible=.T.
            Local nDimension
            If Vartype(i_bFox26)='L'
                If i_bFox26
                    .bFox26=.T.
                Endif
            Endif
            If !.bFox26
                cp_GetSecurity(This,'VisualQry')
            Endif
            cp_ReadXdc()
            .oDcx=i_dcx
            .oform = Createobject("query_builder", This)
            *!*	      .oDic=createobject('query_tables',this)
            *!*	      .oGraph=createobject('query_graph',this)
            *!*	      .oQuery=createobject('query_selitems',this)
            .oform.Show()
            .Dock(i_nCPToolBarPos)
            .oThis=This
            .__sqlx__=Createobject('sqlx')
            .Caption=cp_Translate(MSG_QUERY_PAINTER)
            *--- Nascondo le altre toolbar
            If Vartype(i_bHideToolbarUTK)<>'U' And i_bHideToolbarUTK
                If i_VisualTheme <> -1
                    i_ThemesManager.HideToolBar(.T.)
                Else
                    If Vartype(oCptoolbar)='O' And !Isnull(oCptoolbar)
                        .bCpToolbarVisible = oCptoolbar.Visible
                        oCptoolbar.Visible = .F.
                    Endif
                    If Vartype(oDesktopBar)='O' And !Isnull(oDesktopBar)
                        .bDeskTopBarVisible = oDesktopBar.Visible
                        oDesktopBar.Visible = .F.
                    Endif
                Endif
            Endif
            *--- Zucchetti Aulla - Inizio Gestione OpenOffice
            .Excl.cImage=Iif(g_OFFICE='M','prexcel.bmp','osheets.bmp')
            .Excl.ToolTipText=Iif(g_OFFICE='M',cp_Translate(MSG_CREATE_BS_MODIFY_EXCEL_WORKSHEET),cp_Translate(MSG_CREATE_BS_MODIFY_SPREADSHEET))
            .Excl.Enabled=Iif(Inlist(g_OFFICE,'M','O'),.T.,.F.)
            .Word.cImage=Iif(g_OFFICE='M','prword.bmp','owrites.bmp')
            .Word.ToolTipText=Iif(g_OFFICE='M',cp_Translate(MSG_CREATE_BS_MODIFY_MAILMERGE),cp_Translate(MSG_CREATE_BS_MODIFY_OPENOFFICE_DOCUMENT))
            .Word.Enabled=Iif(Inlist(g_OFFICE,'M','O'),.T.,.F.)
            *--- Zucchetti Aulla - Fine Gestione OpenOffice
            *--- Monitor Framework
            .savebtn.Enabled = cp_set_get_right()>3
            .saveasbtn.Enabled = cp_set_get_right()>3
        Endwith
    Func SaveModified()
        *--- Zucchetti Aulla inizio - Framework monitor
        * Non chiede di salvare se non hai i diritti di scrittura
        If cp_set_get_right()<=3
            Return (.T.)
        Else
            Return DoDefault()
        Endif
        *--- RunClick
    Proc run_click()
        This.Focus()
        This.oQuery.ExecSelect('Exec')
    Endproc
    *--- SqlClick
    Proc sql_click()
        This.Focus()
        This.oQuery.ExecSelect('Get')
    Proc DoPrn(bAskFile)
        This.Focus()
        If Empty(This.cFileName)
            CP_MSG(cp_Translate(MSG_QUERY_NAME_MISSING_C_CANNOT_CREATE_PRINT_LAYOUT),.F.)
            Return
        Endif
        Private i_paramqry
        i_paramqry=.Null.
        If This.oQuery.ExecSelect('Report')
            Private cName
            cName=Left(This.cFileName,Len(This.cFileName)-4)
            If bAskFile
                Local i_err
                i_err=On('ERROR')
                On Error cName=''
                cName=Locfile(cName+'*.frx','frx')
                On Error &i_err
                If Empty(cName)
                    Return
                Else
                    cName=Left(cName,Len(cName)-4)
                Endif
            Endif
            
            SELECT * FROM (This.oQuery.cCursor) INTO CURSOR __tmp__
            SELECT __tmp__
            
            Local N
            N=Getenv('TEMP')
            * --- crea una tabella temporanea in un database temporaneo per la gestione dei nomi lunghi
            Local d
            d=Set('DATABASE')
            Crea Database (N+'\_data_')
            Copy To (N+'\__tmp__') Database (N+'\_data_')
            Use
            * --- usa la tabella appena creata (in \windows\temp) con alias __tmp__
            Select 0
            Use (N+'\__tmp__') Alias __tmp__
            Local cPATHREPORT
            If !cp_fileexist(cName+'.frx')
                *--- Zucchetti Aulla Inizio - Report Wizard
                * --- Verifica avvio Report Wizard
                If IsAlt() Or (Vartype(g_bnousewizard) <> 'U' And  g_bnousewizard)
                    cPATHREPORT = cName
                    Crea Report (cName) From __tmp__
                Else
                    * --- Preparazione cursore ed avvio del Report Wizard
                    Local tabella,numpar,tabpar, nFields, cFieldDescri
                    tabella = Sys(2015)
                    tabpar = Sys(2015)
                    Create Cursor (tabella) (FIELDNAME C(60), FIELDDESC C(80), FIELDTYPE C(1), FIELDUSED C(1))
                    nFields = AFIELDS(L_ArrFields, '__tmp__')
                    * --- Inserimento dati campi cursore di stampa
                    For i=1 To nFields
                    	* Verifico se nell'array dei campi � presente quello attuale, se � presente recupero la descrizione
                    	* impostata nel disegnatore di query, altrimenti metto il solito nome del campo (Es. query senza campi o con campi e *)
                    	cFieldDescri = ""
                    	IF ALEN(This.oQuery.opage.page2.opag.fdlfields.oList.List,1)>=i AND VARTYPE(This.oQuery.opage.page2.opag.fdlfields.oList.List(i,1))<>'L'
                    		cFieldDescri = NVL( This.oQuery.opage.page2.opag.fdlfields.oList.List(i,1), '')
                    	ENDIF
                    	cFieldDescri = EVL(cFieldDescri, LEFT(ALLTRIM(L_ArrFields[i,1]),80) )
                        Insert Into (tabella) Values( LEFT(ALLTRIM(L_ArrFields[i,1]),60), cFieldDescri, LEFT(ALLTRIM(L_ArrFields[i,2]),1), 'N')
                    NEXT
                    RELEASE L_ArrFields
                    * --- Inserimento parametri associati alla visual query
                    Create Cursor (tabpar) (FIELDNAME C(20), FIELDDESC C(50), FIELDTYPE C(1), FIELDUSED C(1))
                    numpar = Alen(This.oQuery.opage.page5.opag.fplparam.oList.List,1)
                    If numpar > 0
                        For i = 1 To numpar
                            If Vartype(This.oQuery.opage.page5.opag.fplparam.oList.List(i,1)) = 'C'
                                Insert Into (tabpar) Values(This.oQuery.opage.page5.opag.fplparam.oList.List(i,1),This.oQuery.opage.page5.opag.fplparam.oList.List(i,2), This.oQuery.opage.page5.opag.fplparam.oList.List(i,3), 'N')
                            Endif
                        Next
                    Endif
                    L_Macro = "cPATHREPORT = GSUT_BWR(.Null.,tabella,Forceext(cName,'frx'), tabpar, .F.)"
                    &L_Macro
                    Use In Select(tabella)
                    Use In Select(tabpar)
                Endif
                *--- Zucchetti Aulla Fine - Report Wizard
            Else
                cPATHREPORT = FORCEEXT(cName,'frx')
            Endif
            If !Empty(m.cPATHREPORT) And cp_fileexist(m.cPATHREPORT)
				If Used("__tmp__")
					SELECT("__tmp__")
					GO TOP
				endif
                cp_ModifyReport(m.cPATHREPORT)
            Endif
            * --- distrugge la tabella e il database temporanei
            USE IN SELECT("__tmp__")
            Close Database
            Delete Database (N+'\_data_')
            Delete File (N+'\__tmp__.dbf')
            If Not(Empty(d))
                * --- potrebbe non esserci un database aperto
                Set Database To (d)
            Endif
            i_paramqry=.Null.
            *--- Zucchetti Aulla Inizio - Multireport
            *--- Elimino informazioni sul data environment
            Local l_oldArea, l_Set
            l_oldArea=Select()
            cName=Forceext(cName, "FRX")
            Try
                Use (cName) Exclusive
                l_Set=  "Top = 0"+Chr(13)+Chr(10)+;
                    "Left = 0"+Chr(13)+Chr(10)+;
                    "Width = 0"+Chr(13)+Chr(10)+;
                    "Height = 0"+Chr(13)+Chr(10)+;
                    "DataSource = .NULL."+Chr(13)+Chr(10)+;
                    'Name = "Dataenvironment"'
                Update (cName) Set Expr=l_Set, Environ=.F. Where OBJTYPE=25
                Delete From (cName) Where OBJTYPE=26
                Use	 In Select(Juststem(cName))
            Catch
                *--- Non riesco ad aprire il file in modo esclusivo
                Use	 In Select(Juststem(cName))
            Endtry
            Select(l_oldArea)
            *--- Zucchetti Aulla Fine - Multireport
        Else
            CP_MSG(cp_Translate(MSG_ERROR_EXECUTING_QUERY),.F.)
        Endif
    Proc DoWord(bAskFile)
        This.Focus()
        If Empty(This.cFileName)
            CP_MSG(cp_Translate(MSG_QUERY_NAME_MISSING_C_CANNOT_CREATE_PRINT_LAYOUT),.F.)
            Return
        Endif
        If This.oQuery.ExecSelect('Report')
            local w
            Private cName
            SELECT * FROM (This.oQuery.cCursor) INTO CURSOR __tmp__
            Select __tmp__

            Go Top
            If Not(Eof())
                local N,i_err,bOk,i_oDs
                Delete File (Alltrim(Addbs(g_tempadhoc))+'__word__.dbf')
                N="'"+Alltrim(Addbs(g_tempadhoc))+'__word__'+"'"
                local l_errOld, l_err
                l_errOld = On("error")
                l_err = .F.
                On Error l_err = .T.
                If g_OFFICE<>"M"
					If vartype(g_WordFoxPlus)='L' and g_WordFoxPlus
						Copy To &N FoxPlus					
					Else
						Copy To &N Fox2x
					Endif
                ELSE
                N=FORCEEXT(N,"XLSX")
                cp_cursortoxlsx(N, IIF (EMPTY (N), "Foglio1",JUSTSTEM(N)),SELECT(0))
                 *   Copy To &N Xl5
                Endif
                If l_err
                    CP_MSG(cp_Translate(MSG_WORD_ALREADY_OPEN_C_CANNOT_CREATE_PRINT_LAYOUT),.F.)
                    Return
                Endif
                On Error &l_errOld
                cName=Left(This.cFileName,Len(This.cFileName)-4)
                If bAskFile
                    i_err=On('ERROR')
                    On Error cName=''
                    * ---Zucchetti Aulla Inizio - Gestione openoffice
                    If g_OFFICE="M"
                        * ---Zucchetti Aulla fine - Gestione openoffice
                        cName=Locfile(cName+'*.doc','doc')
                        * ---Zucchetti Aulla Inizio - Gestione openoffice
                    Else
                        cName=Locfile(cName+'*.sxw','sxw')
                    Endif
                    * ---Zucchetti Aulla Fine - Gestione openoffice
                    On Error &i_err
                    If Empty(cName)
                        Return
                    Endif
                Endif
                m.bOldExport = g_OFFICE="M" And (Type('g_AUTOFFICE')='U' Or g_AUTOFFICE Or !cp_fileexist(ForceExt(cName, 'docx')))
                bOk=.T.
                i_err=On('ERROR')
                On Error bOk=.F.
                * --- Zucchetti Aulla Inizio - Gestione OpenOffice aggiunto IF
                If g_OFFICE="M"
                    w=Createobject('word.application')
                    **Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
					DecoratorState ("D")
                Endif
                On Error
                If bOk
                    If g_OFFICE="M"
                    	If m.bOldExport
                        w.Visible=.T.
                        EndIf
                    Else && Gestione OpenOffice
                        *** Impostazioni Apertura documento ***
                        Dimension Args(1)
                        oManager= Createobject("com.sun.star.ServiceManager")
                        oDesktop= oManager.createInstance("com.sun.star.frame.Desktop")
                        Comarray(oDesktop,10)
                        Args[1] = oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
                        Args[1].Name="AsTemplate"
                        *** Impostazioni Sorgente Dati ***
                        ODBC=oManager.createInstance("com.sun.star.sdb.DatabaseContext")
                        oDataSource=ODBC.getByName("Bibliography")
                        oDataSource.URL="sdbc:dbase:file:///"+STRTRAN(g_tempadhoc,'\','/')
                    Endif

                    If ((cp_fileexist(ForceExt(cName,'doc')) or cp_fileexist(ForceExt(cName,'docx'))) And g_OFFICE="M") Or (cp_fileexist(cName+'.sxw') And g_OFFICE="O")
                        If g_OFFICE="M"
                        	If Type('g_AUTOFFICE')='U' Or g_AUTOFFICE Or !cp_fileexist(ForceExt(cName,'docx'))
                        		If cp_fileexist(ForceExt(cName,'docx'))
                        			cName=ForceExt(cName,'docx')
                        		Else
                            cName=FORCEEXT(cName,'doc')
                        		EndIf
                            w.documents.Open(cName)
                            i_oDs=w.activedocument.mailmerge.Datasource
                            If Not(Upper(i_oDs.Name)==Upper(FORCEEXT(N,"xlsx")))
                                w.activedocument.mailmerge.opendatasource(FORCEEXT(N,'xlsx'))
                                w.activedocument.SaveAs(cName)
                            Endif
                            Else
                            	Local cNameFile, bAutoDir 
								m.bAutoDir = Type('g_NoAutOfficeDir')='C' And !Empty(g_NoAutOfficeDir) And Directory(g_NoAutOfficeDir)
								m.cNameFile = Iif(m.bAutoDir, Addbs(g_NoAutOfficeDir)+JUSTSTEM(m.cName)+Sys(2015), Putfile('Save as', Addbs(g_tempadhoc)+JUSTFNAME(m.cName), 'DOCX'))
                            	vx_build('D', m.cName,'__tmp__', m.cNameFile, .F., !m.bAutoDir)
                            EndIf
                        Else  &&Gestione OpenOffice
                            cName=cName+'.sxw'
                            Args[1] = oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
                            Args[1].Value=".f."
                            oWriter=oDesktop.loadComponentFromURL("file:///"+Strtran(Alltrim(cName),Chr(92),Chr(47)), "_blank", 0, @Args)
                        Endif
                    Else
                        If g_OFFICE="M"
                            cName=cName+'.doc'
                            w.documents.Add()
                            w.activedocument.mailmerge.maindocumenttype=3
                            w.activedocument.mailmerge.opendatasource(FORCEEXT(N,'xlsx'))
                            w.activedocument.SaveAs(cName)
                        Else   && Gestione OpenOffice
                            cName=cName+'.sxw'
                            Args[1] = oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
                            Args[1].Value=.T.
                            oWriter = oDesktop.loadComponentFromURL("private:factory/swriter", "_blank", 0, @Args)
                            Comarray(oWriter,10)
                        Endif
                    Endif
                    If g_OFFICE="M"
                    	If m.bOldExport
                        This.oWord=w
                      	**Al termine del esportazion riattiva il decorator dei form 
						DecoratorState ("A")
                        EndIf
                    Else
                        *** Salvataggio Documento OpenOffice ***
                        Args[1] = oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
                        Args[1].Name="Overwrite"
                        Args[1].Value=.T.
                        Doc=oWriter.CurrentController.Frame
                     	hWindow = FindWindow(null, Doc.Title)
                     	IF hWindow>0
	                     	SetForegroundWindow(hWindow)
	                    ENDIF
                        oWriter.storeToURL("file:///"+Strtran(Alltrim(tempadhoc()+JUSTFNAME(cName)),Chr(92),Chr(47)), @Args)
                        oDispatch=oManager.createInstance("com.sun.star.frame.DispatchHelper")
                        Comarray(oDispatch,10)
                        Args[1] = oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
                        Args[1].Name = "ViewDataSourceBrowser"
                        Args[1].Value = .T.
                        oDispatch.executeDispatch(Doc,".uno:ViewDataSourceBrowser", "", 0,@Args)
                        sleep(3000)
                    Endif
                    If !bOk
                        If g_OFFICE="M"
                            cp_ErrorMsg(cp_Translate(MSG_CANNOT_OPEN_MSWORD)+Message(),16,MSG_ERROR,.f.)
                        Else
                            cp_ErrorMsg(cp_Translate(MSG_CANNOT_OPEN_OPENOFFICE)+Message(),16,MSG_ERROR,.f.)
                        Endif
                    Endif
                Else
                    If g_OFFICE="M"
                        cp_ErrorMsg(cp_Translate(MSG_CANNOT_OPEN_WORD_DOCUMENT_QM),16,MSG_ERROR,.f.)
                    Else
                       cp_ErrorMsg(cp_Translate(MSG_CANNOT_OPEN_WRITER_DOCUMENT),16,MSG_ERROR,.f.)
                    Endif
                Endif
                On Error &i_err
            Else
                If g_OFFICE="M"
                    cp_ErrorMsg(MSG_DATA_MISSING_C_CANNOT_CREATE_MAILMERGE,'stop',MSG_ERROR)
                Else
                    cp_ErrorMsg(MSG_CANNOT_CONNECT_TO_DATA_SOURCE,'stop',MSG_ERROR)
                Endif
            Endif
        Endif
    Proc DoGraph(bAskFile, bOldGraph)
        This.Focus()
        If Empty(This.cFileName)
            CP_MSG(cp_Translate(MSG_QUERY_NAME_MISSING_C_CANNOT_CREATE_GRAPH),.F.)
            Return
        Endif
        If Vartype(m.bOldGraph)='L' And m.bOldGraph
            If This.oQuery.ExecSelect('Report')
                If Not(Eof())
                    Local cName
                    cName=Left(This.cFileName,Len(This.cFileName)-4)
                    If m.bAskFile
                        i_err=On('ERROR')
                        On Error m.cName=''
                        cName=Locfile(m.cName+'*.vgr','vgr')
                        On Error &i_err
                        If Empty(m.cName)
                            Return
                        Endif
                    Endif
					SELECT * FROM (This.oQuery.cCursor) INTO CURSOR __tmp__
                    Select __tmp__
                    vg_build("__tmp__",m.cName,.T.)
                Else
                    cp_ErrorMsg(MSG_DATA_MISSING_C_CANNOT_CREATE_GRAPH,'stop',MSG_ERROR)
                Endif
            Endif
        Else
            Local cName
            cName=""
            If m.bAskFile
                i_err=On('ERROR')
                On Error m.cName=''
                cName=Locfile(m.cName+'*.vfc','vfc')
                On Error &i_err
                If Empty(m.cName)
                    Return
                Endif
                Do vfcbuild With "", m.cName
            Else
                Do vfcbuild With This.cFileName
            Endif
        Endif
    Proc DoExcel(bAskFile)
        This.Focus()
        Local cName,i_eSheet,bOk,i_err
        Local bOldExport
        If Empty(This.cFileName)
            CP_MSG(cp_Translate(MSG_QUERY_NAME_MISSING_C_CANNOT_CREATE_EXCEL_WORKSHEET),.F.)
            Return
        Endif
        If This.oQuery.ExecSelect('Report')
            If Not(Eof())
                cName=Left(This.cFileName,Len(This.cFileName)-4)
                m.bOldExport = Type("g_AUTOFFICE")="U" Or g_AUTOFFICE Or (!cp_IsStdFile(cName, 'XLTX') And cp_IsStdFile(cName, 'XLT'))
                If bAskFile
                    i_err=On('ERROR')
                    On Error cName=''
                    * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
                    If g_OFFICE='M'
                    	If Type("g_AUTOFFICE")="U" Or g_AUTOFFICE
                        cName=Locfile(cName+'*.xlt','xlt')
                    Else
                        	cName=Locfile(cName+'*.xltx','xltx')
                        EndIf
                    Else
                        cName=Locfile(cName+'*.stc','stc')
                    Endif
                    * --- Zucchetti Aulla - Fine Gestione Suite OpenOffice/StarOffice
                    On Error &i_err
                    If Empty(cName)
                        Return
                    Endif
                ENDIF
                SELECT * FROM (This.oQuery.cCursor) INTO CURSOR __tmp__
                Select __tmp__
                bOk=.T.
                i_err=On('ERROR')
                On Error bOk=.F.
                * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
                If g_OFFICE='M' And m.bOldExport
                    i_eSheet=Createobject('excel.application')
                Endif
                * --- Zucchetti Aulla - Fine Gestione Suite OpenOffice/StarOffice
                If bOk
                    * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
                    If g_OFFICE='M'
                    	If m.bOldExport
                        CP_MSG(cp_Translate(MSG_BUILDING_EXCEL_WORKSHEET_D))
	                        ve_build(i_eSheet,"__tmp__",cName,.T.,.T.,,This.oQuery)
                        i_eSheet = .null.
                    Else
	                    	vx_build('X', JustFname(cName),'__tmp__', Iif(m.bAskFile, cName, JustFname(cName)+SYS(2015)), m.bAskFile)
	                    EndIf
                    Else
                        CP_MSG(cp_Translate(MSG_CREATING_SPREADSHEET_D))
                        vo_build("__tmp__",cName,.T.,.T.,This.oQuery)
                    Endif
                    * --- Zucchetti Aulla - Fine Gestione Suite OpenOffice/StarOffice
                    Wait Clear
                    * --- Istruzione commentata perch� lascia appesi processi excel
                    *This.oExcel=i_eSheet
                Else
                    * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
                    If g_OFFICE='M'
                        cp_ErrorMsg(MSG_CANNOT_OPEN_EXCEL_WORKSHEET_QM,16,MSG_ERROR)
                    Else
                        cp_ErrorMsg(MSG_CANNOT_OPEN_SPREADSHEET)
                    Endif
                    * --- Zucchetti Aulla - Fine Gestione Suite OpenOffice/StarOffice
                Endif
                On Error &i_err
            Else
                * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
                If g_OFFICE='M'
                    cp_ErrorMsg(MSG_DATA_MISSING_C_CANNOT_CREATE_EXCEL_WORKSHEET,'stop',MSG_ERROR)
                Else
                    cp_ErrorMsg(MSG_CANNOT_CREATE_SPREADSHEET)
                Endif
                * --- Zucchetti Aulla - Fine Gestione Suite OpenOffice/StarOffice
            Endif
        Endif
    Proc EditQuery()
        If !This.bSec1
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            This.Exit()
        Else
            This.Show()
            This.oDic.Show()
            This.oGraph.Show()
            This.oQuery.Show()
        Endif
    Proc Focus()
        * --- si assicura che tutto sia salvato e chiuso
        This.oQuery.Show()
        This.oGraph.Show()
        Return

    Proc Exit()
        *release pad vista of _msysmenu
        *--- devo porre a null queste propriet� altrimenti ho dei problemi con la chiusura della finestra
        Local l_i,l_bNoExit
        l_bNoExit = .F.
        If !Isnull(This.oform)
            For l_i=This.oform.oPgFrm.PageCount To 1 Step -1
                This.oform.oPgFrm.ActivePage = l_i
                This.oform.oPgFrm.Click()
                If !This.oform.RemoveQuery(l_i)
                    l_bNoExit = .T.
                    Exit
                Endif
            Endfor
        Endif
        If !l_bNoExit
            This.oQuery.opage.page2.opag.fdlfields.oList.oParentObject=.Null.
            This.oQuery.opage.page3.opag.rllrelation.oList.oParentObject=.Null.
            This.oQuery.opage.page4.opag.fllfilter.oList.oParentObject=.Null.
            This.oQuery.opage.page5.opag.fplparam.oList.oParentObject=.Null.
            *this.oDic.oFindForm=.NULL.
            This.oDic.oqc=.Null.
            This.oGraph.oqc=.Null.
            This.oQuery.oqc=.Null.
            *this.oQuery.oPivot=.NULL.
            This.oDic=.Null.
            This.oGraph=.Null.
            This.oQuery=.Null.
            This.oDcx=.Null.
            This.oFindForm=.Null.
            This.oThis=.Null.
            If Vartype(This.oform)='O' And !Isnull(This.oform)
                If i_VisualTheme<>-1 And i_cMenuTab<>"S"
                    This.oform.oTabmenu.oParentObject=.Null.
                Endif
                This.oform.oParentObject = .Null.
                This.oform.Release()
            Endif
            This.oform = .Null.
            This.__sqlx__=.Null.
            This.Visible=.F.
            i_curform=.Null.
            *--- Mostro le altre toolbar
            If Vartype(i_bHideToolbarUTK)<>'U' And i_bHideToolbarUTK
                If i_VisualTheme <> -1
                    i_ThemesManager.ShowToolBar(.F.,.T.)
                    oDesktopBar.SetAfterCpToolbar()
                Else
                    If Vartype(oCptoolbar)='O' And !Isnull(oCptoolbar)
                        oCptoolbar.Visible = This.bCpToolbarVisible
                    Endif
                    If Vartype(oDesktopBar)='O' And !Isnull(oDesktopBar)
                        oDesktopBar.Visible = This.bDeskTopBarVisible
                    Endif
                Endif
            Endif
            This.Destroy()
        Endif

    Proc NewDoc()
        This.oGraph.RemoveAll()
        This.oQuery.RemoveAll()
        This.oDic.Reset()
        This.nTables=0
        This.cExtMask=''
        This.bRemoveWhereOnEmptyParam=.F.
    Proc LoadDoc()
        Local xcg,C,h,F,l
        * --- nuovo documento
        This.oDic.Reset()
        This.oGraph.RemoveAll()
        This.oQuery.RemoveAll()
        This.nTables=0
        C=cp_GetPFile(This.cFileName)
        xcg=Createobject('pxcg','load',C)
        This.Serialize(xcg)
        This.bModified=.F.
        This.bEmpty=.F.
        This.oform.SetPageInfo(This.oform.nActivePage)
    Proc SaveDoc()
        Local xcg,h
        xcg=Createobject('pxcg','save')
        This.Serialize(xcg)
        This.bModified=.F.
        *--- Creo file bck
        If File(This.cFileName)
            Local l_Safety
            l_Safety = Set("Safety")
            Set Safety Off
            Copy File (This.cFileName) To Forceext(This.cFileName, "BAK")
            Set Safety &l_Safety
        Endif
        h=Fcreate(This.cFileName)
        =Fwrite(h,xcg.cText)
        =Fclose(h)
        Return
    Procedure bModified_Assign(bValue)
        This.bModified=m.bValue
        If Type("This.oForm.oPgFrm.Pages(This.oForm.oPgFrm.ActivePage).caption")="C"
            If m.bValue
                Local l_caption
                l_caption = This.oform.oPgFrm.Pages(This.oform.oPgFrm.ActivePage).Caption
                l_caption = Iif(Rat('*', l_caption)<>0, l_caption, Alltrim(l_caption)+" *")
                This.oform.oPgFrm.Pages(This.oform.oPgFrm.ActivePage).Caption = l_caption
                This.bEmpty=.F.
            Else
                l_caption = This.oform.oPgFrm.Pages(This.oform.oPgFrm.ActivePage).Caption
                l_caption = Iif(Rat('*', l_caption)<>0, Left(l_caption, Len(Alltrim(l_caption))-1), Alltrim(l_caption))
                This.oform.oPgFrm.Pages(This.oform.oPgFrm.ActivePage).Caption = l_caption
            Endif
        Endif
    Endproc
    Proc SetTitle()
        *this.oGraph.caption=CP_MSGFORMAT(MSG_DESIGN__,this.cFileName)
        If Not Empty(This.cFileName)
            This.oform.oPgFrm.Pages[this.oForm.oPgFrm.ActivePage].Caption=Displaypath(This.cFileName, 30)
            This.oform.oPgFrm.Pages[this.oForm.oPgFrm.ActivePage].ToolTipText=Alltrim(This.cFileName)
        Else
            This.oform.oPgFrm.Pages[this.oForm.oPgFrm.ActivePage].Caption=Forceext(cp_MsgFormat(MSG_NO_NAME), "VQR")
            This.oform.oPgFrm.Pages[this.oForm.oPgFrm.ActivePage].ToolTipText=This.oform.oPgFrm.Pages[this.oForm.oPgFrm.ActivePage].Caption
        Endif
        This.oform.oPgFrm.Pages[this.oForm.oPgFrm.ActivePage].oQuery.LFileName.Caption = Lower(This.oform.oPgFrm.Pages[this.oForm.oPgFrm.ActivePage].ToolTipText)
        Return
    Func GetHelpTopic()
        Return('vq_build')
    Proc AddField(cName,cDescr,cAlias,cType,nLen,nDec)
        If This.oQuery.IsUsedField(cName,cAlias)
            If Not cp_YesNo(cAlias+'.'+cName+Chr(13)+cp_Translate(MSG_FIELD_ALREADY_IN_OUTPUT_LIST_C_ADD_ANYWAY_QP),32,.f.)
                Return
            Endif
        Endif
        If !This.IsUsedTable(cAlias)
            If This.oDcx.GetTableIdx(cAlias)<>0
                This.AddTable(cAlias,This.oDcx.GetTableDescr(This.oDcx.GetTableIdx(cAlias)))
            Else
                This.AddTable(cAlias,cAlias)
            Endif
        Endif
        This.oQuery.AddField(cName,cDescr,cAlias,cType,nLen,nDec)
        This.oGraph.AddField(cAlias,cDescr)
        This.bModified=.T.
        Return
    Proc FullAddField(cExp,cDescr,cAlias,cType,nLen,nDec)
        Local cA
        This.oQuery.FullAddField(cExp,cDescr,cAlias,cType,nLen,nDec)
        cA=Left(cExp,At('.',cExp)-1)
        This.oGraph.AddField(cA,cDescr)
        Return
    Proc AddTable(cName,cDescr,bDontAskRelations)
        * --- controlla se e' gia' inserita per assegnare alias
        Local cAlias,i,j
        cAlias=cName
        j=0
        For i=1 To This.nTables
            If This.cTables[i,2]==cAlias
                j=j+1
                cAlias=cName+Alltrim(Str(j))
                i=1
            Endif
        Next
        If j<>0
            * --- cambia la descrizione
            cDescr=cDescr+'('+Alltrim(Str(j))+')'
        Endif
        This.FullAddTable(cAlias,cName,cDescr,0,0,bDontAskRelations)
        This.bModified=.T.
        Return
    Proc FullAddTable(cAlias,cName,cDescr,nX,nY,bDontAskRelations,cMode)
        * --- aggiunge
        This.nTables=This.nTables+1
        Dimension This.cTables[this.nTables,2]
        This.cTables[this.nTables,1]=cName
        This.cTables[this.nTables,2]=cAlias
        * --- aggiunge a disegno
        This.oGraph.AddTable(cName,cAlias,cDescr,nX,nY)
        * --- aggiunge a lista
        This.oQuery.AddTable(cName,cAlias,cDescr,cMode)
        If !bDontAskRelations
            * --- trova le relazioni
            This.oRelForm=Createobject('query_relation_list')
            If This.FindRelations(cName,cAlias)>0
                This.oRelForm.oqc=This
                This.oRelForm.Show()
            Endif
            This.oRelForm=.Null.
            * --- la seleziona come tabella corrente
            This.oDic.SelectTable(cName,cDescr,cAlias)
        Endif
    Proc Addrelation(cAlias1,cAlias2,cDescr,cExpr,cType,bAddLink)
        If This.oQuery.ExistRelation(cAlias1,cAlias2)
            Return
        Endif
        * --- controlla esistenza tabella con Alias1
        If !This.IsUsedTable(cAlias1)
            This.AddTable(cAlias1,This.oDcx.GetTableDescr(This.oDcx.GetTableIdx(cAlias1)),.T.)
        Endif
        * --- controlla esistenza tabella con Alias2
        If !This.IsUsedTable(cAlias2)
            This.AddTable(cAlias2,This.oDcx.GetTableDescr(This.oDcx.GetTableIdx(cAlias2)),.T.)
        Endif
        * --- aggiunge la relazione nella lista delle relazioni
        This.oQuery.Addrelation(cDescr,cExpr,cType,cAlias1,cAlias2)
        * --- aggiunge il link nel disegno
        If bAddLink
            *---  Cambio colore link
            This.oGraph.Addrelation(cAlias1,cAlias2, cType)
        Endif
        This.bModified=.T.
    Proc RemoveTable(cAlias)
        * --- rimuove dalla query
        This.oDic.RemoveCurrentTable(cAlias)
        This.oQuery.RemoveTable(cAlias)
        * --- rimuove dal grafico
        This.oGraph.RemoveTable(cAlias)
        * --- rimuove dall' array delle tabelle
        Local i
        For i=1 To This.nTables
            If This.cTables[i,2]==cAlias
                Adel(This.cTables,i,1)
                This.nTables=This.nTables-1
                Exit
            Endif
        Next
        This.bModified=.T.
    Proc RemoveRelation(cAlias1,cAlias2)
        * --- rimuove dalla query
        This.oQuery.RemoveRelation(cAlias1,cAlias2)
        * --- rimuove dal grafico
        This.oGraph.RemoveRelation(cAlias1,cAlias2)
        This.bModified=.T.
    Func FindRelations(cName,cAlias)
        Local i,j,cDescr,cExp,nRel,nTRel,bIsChild
        nTRel=0
        For i=1 To This.nTables
            nRel=0
            * --- controlla i possibili link di tutti gli oggetti presenti nella lista
            For j=1 To This.oDcx.GetFKCount(This.cTables[i,1])
                If This.oDcx.GetFKTable(This.cTables[i,1],j)==cName
                    nRel=nRel+1
                    cDescr=This.oDcx.GetFKDescr(This.cTables[i,1],j)
                    cExp=This.oDcx.GetFKExpr(This.cTables[i,1],j,cAlias,This.cTables[i,2])
                    If This.oDcx.GetFKIsChild(This.cTables[i,1],j) && bIsChild
                        This.oRelForm.Addrelation(cAlias,This.cTables[i,2],cDescr,cExp,'Left outer',nRel=1)
                    Else
                        This.oRelForm.Addrelation(cAlias,This.cTables[i,2],cDescr,cExp,'Right outer',nRel=1)
                    Endif
                Endif
            Next
            For j=1 To This.oDcx.GetIFKCount(This.cTables[i,1])
                If This.oDcx.GetIFKTable(This.cTables[i,1],j)==cName
                    nRel=nRel+1
                    cDescr=This.oDcx.GetIFKDescr(This.cTables[i,1],j)
                    cExp=This.oDcx.GetIFKExpr(This.cTables[i,1],j,This.cTables[i,2],cAlias)
                    If This.oDcx.GetIFKIsChild(This.cTables[i,1],j) && bIsChild
                        This.oRelForm.Addrelation(This.cTables[i,2],cAlias,cDescr,cExp,'Left outer',nRel=1)
                    Else
                        This.oRelForm.Addrelation(This.cTables[i,2],cAlias,cDescr,cExp,'Right outer',nRel=1)
                    Endif
                Endif
            Next
            nTRel=nTRel+nRel
        Next
        Return(nTRel)
    Func IsUsedTable(cAlias)
        Local i
        For i=1 To This.nTables
            If This.cTables[i,2]==cAlias
                Return(.T.)
            Endif
        Next
        Return(.F.)
    Proc SearchRelations()
        *this.oFindForm=createobject('query_findrelations',this)
        *this.oFindForm.show()
        With This.oform.oPgFrm
            If Type("this.oForm.oPgFrm.Pages[.ActivePage].oQuery.oFindRel")='U'
                .Pages[.ActivePage].oQuery.AddObject("oFindRel", "query_findrelations", This)
            Endif
            .Pages[.ActivePage].oQuery.oFindRel.Show_()
        Endwith
    Proc Serialize(xcg)
        * --- salva le tabelle, con le coordinate del disegno
        This.SerializeTables(xcg)
        * --- salva i campi
        This.SerializeFields(xcg)
        * --- salva le relazioni
        This.SerializeRelations(xcg)
        * --- salva la where
        This.SerializeWhere(xcg)
        * --- salva la order by
        This.SerializeOrderBy(xcg)
        * --- salva la group by
        This.SerializeGroupBy(xcg)
        * --- salva la distinct
        This.SerializeDistinct(xcg)
        * --- salva i parametri
        This.SerializeParam(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cExtMask)
            xcg.Save('L',This.bRemoveWhereOnEmptyParam)
            xcg.Save('C',This.oQuery.GetUnion())
        Else
            This.cExtMask=xcg.Load('C','')
            This.oQuery.SetMask(This.cExtMask)
            This.bRemoveWhereOnEmptyParam=xcg.Load('L',.F.)
            This.oQuery.SetRemove(This.bRemoveWhereOnEmptyParam)
            This.oQuery.SetUnion(xcg.Load('C',''))
        Endif
        This.SerializePivot(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.oQuery.GetNote())
            xcg.Save('L',This.oQuery.GetUnionAll())
            xcg.Save('L',This.bOrderByParm)
            This.nVersion=This.nVersion+1
            xcg.Save('N',This.nVersion)
            xcg.Save('L',This.oQuery.GetMultiCompany())
            xcg.Save('C',This.oQuery.GetRoles())
        Else
            This.oQuery.SetNote(xcg.Load('C',''))
            This.oQuery.SetUnionAll(xcg.Load('L',.F.))
            This.bOrderByParm=xcg.Load('L',.T.)
            This.oQuery.SetOrderByParm(This.bOrderByParm)
            This.nVersion=xcg.Load('N',0)
            This.oQuery.SetMultiCompany(xcg.Load('L',.F.))
            This.oQuery.SetRoles(xcg.Load('C',''))
        Endif
        xcg.Eoc()
    Proc SerializeTables(xcg)
        Local i,j,o,cDescr,cName,cAlias,nX,nY,cMode
        If xcg.bIsStoring
            o=This.oQuery.opage.Page1.opag.TbLTables
            xcg.Save('N',o.ListCount)
            For i=1 To o.ListCount
                cDescr=o.List(i,1)
                xcg.Save('C',cDescr)  && descrizione
                xcg.Save('C',o.List(i,2))  && alias
                cName=o.List(i,3)
                cMode=o.List(i,4)
                If Lower(Right(cDescr,4))='.vqr'
                    Do Case
                        Case cMode='Temp. Table'
                        Case cMode='Routine'
                            cName=cName+'+'
                        Otherwise
                            cName=cName+'*'
                    Endcase
                Endif
                xcg.Save('C',cName)  && nome tabella
                xcg.Save('N',This.oGraph.GetX(o.List(i,2)))  && posizione X
                xcg.Save('N',This.oGraph.GetY(o.List(i,2)))  && posizione Y
                xcg.Eoc()
            Next
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cAlias=xcg.Load('C','')
                cName=xcg.Load('C','')
                cMode=""
                If Lower(Right(cDescr,4))='.vqr'
                    cMode="Temp. Table"
                    If Right(cName,1)='*'
                        cMode=""
                        cName=Left(cName,Len(cName)-1)
                    Endif
                    If Right(cName,1)='+'
                        cMode="Routine"
                        cName=Left(cName,Len(cName)-1)
                    Endif
                Endif
                nX=xcg.Load('N',0)
                nY=xcg.Load('N',0)
                xcg.Eoc()
                This.FullAddTable(cAlias,cName,cDescr,nX,nY,.T.,cMode)
            Next
        Endif
    Proc SerializeFields(xcg)
        Local i,j,o,cDescr,cExp,cAlias,cType,nLen,nDec
        If xcg.bIsStoring
            o=This.oQuery.opage.page2.opag.fdlfields
            xcg.Save('N',o.ListCount)
            For i=1 To o.ListCount
                xcg.Save('C',o.List(i,1)) && descrizione
                xcg.Save('C',o.List(i,2)) && espressione
                xcg.Save('C',o.List(i,3)) && alias
                xcg.Save('C',o.List(i,4)) && type
                xcg.Save('N',Val(o.List(i,5))) && len
                xcg.Save('N',Val(o.List(i,6))) && dec
                xcg.Eoc()
            Next
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cExp=xcg.Load('C','')
                cAlias=xcg.Load('C','')
                cType=xcg.Load('C','C')
                nLen=xcg.Load('N',0)
                nDec=xcg.Load('N',0)
                This.FullAddField(cExp,cDescr,cAlias,cType,nLen,nDec)
                xcg.Eoc()
            Next
        Endif
    Proc SerializeRelations(xcg)
        Local i,j,o,cDescr,cExp,cAlias1,cAlias2,cType
        If xcg.bIsStoring
            o=This.oQuery.opage.page3.opag.rllrelation
            xcg.Save('N',o.ListCount)
            For i=1 To o.ListCount
                xcg.Save('C',o.List(i,1)) && descrizione
                xcg.Save('C',o.List(i,2)) && espressione
                xcg.Save('C',o.List(i,3)) && tipo
                xcg.Save('C',o.List(i,4)) && alias1
                xcg.Save('C',o.List(i,5)) && alias2
                xcg.Eoc()
            Next
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cExp=xcg.Load('C','')
                cType=xcg.Load('C','')
                cAlias1=xcg.Load('C','')
                cAlias2=xcg.Load('C','')
                This.Addrelation(cAlias1,cAlias2,cDescr,cExp,cType,.T.)
                xcg.Eoc()
            Next
        Endif
    Proc SerializeWhere(xcg)
        Local i,j,o,cFld,cNot,cOp,cConst,cLOp,cHaving
        If xcg.bIsStoring
            o=This.oQuery.opage.page4.opag.fllfilter
            xcg.Save('N',o.ListCount)
            For i=1 To o.ListCount
                xcg.Save('C',o.List(i,1)) && campo
                xcg.Save('C',o.List(i,2)) && not
                xcg.Save('C',o.List(i,3)) && operazione
                xcg.Save('C',o.List(i,4)) && costante
                xcg.Save('C',o.List(i,5)) && operazione logica
                xcg.Save('C',o.List(i,6)) && having
                xcg.Eoc()
            Next
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cFld=xcg.Load('C','')
                cNot=xcg.Load('C','')
                cOp=xcg.Load('C','')
                cConst=xcg.Load('C','')
                cLOp=xcg.Load('C','')
                cHaving=xcg.Load('C','')
                xcg.Eoc()
                This.oQuery.AddWhere(cFld,cNot,cOp,cConst,cLOp,cHaving)
            Next
        Endif
    Proc SerializeOrderBy(xcg)
        Local i,j,o,cFld,cDescr,cAscDesc
        If xcg.bIsStoring
            o=This.oQuery.opage.page2.opag.FdLOrder
            xcg.Save('N',o.ListCount)
            For i=1 To o.ListCount
                xcg.Save('C',o.List(i,1)) && descr
                xcg.Save('C',o.List(i,2)) && fld
                xcg.Save('C',o.List(i,3)) && asc/desc
                xcg.Eoc()
            Next
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cFld=xcg.Load('C','')
                cAscDesc=xcg.Load('C','')
                xcg.Eoc()
                This.oQuery.AddOrderBy(cDescr,cFld,cAscDesc)
            Next
        Endif
    Proc SerializeGroupBy(xcg)
        Local i,j,o,cFld,cDescr
        If xcg.bIsStoring
            o=This.oQuery.opage.page2.opag.FdLGroups
            xcg.Save('N',o.ListCount)
            For i=1 To o.ListCount
                xcg.Save('C',o.List(i,1)) && descr
                xcg.Save('C',o.List(i,2)) && campo
                xcg.Eoc()
            Next
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cFld=xcg.Load('C','')
                xcg.Eoc()
                This.oQuery.AddGroupBy(cDescr,cFld)
            Next
        Endif
    Proc SerializeDistinct(xcg)
        If xcg.bIsStoring
            xcg.Save('N',This.oQuery.opage.page2.opag.FdDis.Value)
        Else
            This.oQuery.opage.page2.opag.FdDis.Value=xcg.Load('N',0)
        Endif
    Proc SerializeParam(xcg)
        Local i,j,o,cVar,cDescr,cType,cLen,cDec,cRemove
        If xcg.bIsStoring
            o=This.oQuery.opage.page5.opag.fplparam
            xcg.Save('N',o.ListCount)
            For i=1 To o.ListCount
                xcg.Save('C',o.List(i,1)) && variabile
                xcg.Save('C',o.List(i,2)) && descrizione
                xcg.Save('C',o.List(i,3)) && tipo
                xcg.Save('C',o.List(i,4)) && len
                xcg.Save('C',o.List(i,5)) && dec
                xcg.Save('C',o.List(i,6)) && remove
                xcg.Eoc()
            Next
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cVar=xcg.Load('C','')
                cDescr=xcg.Load('C','')
                cType=xcg.Load('C','C')
                cLen=xcg.Load('C','10')
                cDec=xcg.Load('C','0')
                cRemove=xcg.Load('C','')
                xcg.Eoc()
                This.oQuery.AddFilPar(cVar,cDescr,cType,cLen,cDec,cRemove)
            Next
        Endif
    Proc SerializePivot(xcg)
        Local o,i,j
        o=This.oQuery.oPivot
        If xcg.bIsStoring
            xcg.Save('N',o.pivotflds.ListCount)
            For i=1 To o.pivotflds.ListCount
                xcg.Save('C',o.pivotflds.List(i,1))
                xcg.Save('C',o.pivotflds.List(i,2))
                xcg.Save('C',o.pivotflds.List(i,3))
            Next
            xcg.Save('N',o.dataflds.ListCount)
            For i=1 To o.dataflds.ListCount
                xcg.Save('C',o.dataflds.List(i,1))
                xcg.Save('C',o.dataflds.List(i,2))
                xcg.Save('C',o.dataflds.List(i,3))
            Next
            xcg.Save('N',o.pivottype.Value)
            xcg.Save('C',o.listqry.Value)
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                o.pivotflds.AddItem(xcg.Load('C',''))
                o.pivotflds.List(o.pivotflds.ListCount,2)=xcg.Load('C','')
                o.pivotflds.List(o.pivotflds.ListCount,3)=xcg.Load('C','')
            Next
            j=xcg.Load('N',0)
            For i=1 To j
                o.dataflds.AddItem(xcg.Load('C',''))
                o.dataflds.List(o.dataflds.ListCount,2)=xcg.Load('C','')
                o.dataflds.List(o.dataflds.ListCount,3)=xcg.Load('C','')
            Next
            o.pivottype.Value=xcg.Load('N',1)
            o.listqry.Value=xcg.Load('C','')
        Endif
    Proc ecpSecurity()
        Createobject('stdsProgAuth','VisualQry')
        Return
    Proc CreateQueryCode()
        Local F,cName,i,o,T,i_bSubQuery
        cName=Left(This.cFileName,Len(This.cFileName)-4)
        If cp_fileexist('..\vfcssrc\cp3start.prg')
            cName=Substr(cName,Rat('\',cName)+1)
            F=Fcreate('..\'+cName+'.inc')
        Else
            F=Fcreate(cName+'.inc')
        Endif
        Fwrite(F,'*'+Chr(13)+Chr(10))
        Fwrite(F,'* Static Visual Query'+Chr(13)+Chr(10))
        Fwrite(F,'*'+Chr(13)+Chr(10))
        Fwrite(F,'i_qry=createobject("cpquery")'+Chr(13)+Chr(10))
        o=This.oQuery.opage.Page1.opag.TbLTables
        For i=1 To o.ListCount
            Fwrite(F,'i_qry.mLoadFile("'+o.List(i,1)+'","'+o.List(i,2)+'","'+o.List(i,3)+'")'+Chr(13)+Chr(10))
        Next
        o=This.oQuery.opage.page2.opag.fdlfields
        For i=1 To o.ListCount
            Fwrite(F,'i_qry.mLoadField("'+o.List(i,2)+'","'+o.List(i,3)+'","C",10,0)'+Chr(13)+Chr(10))
        Next
        o=This.oQuery.opage.page3.opag.rllrelation
        For i=1 To o.ListCount
            Fwrite(F,'i_qry.mLoadJoin("'+o.List(i,1)+'","'+o.List(i,4)+'","'+o.List(i,5)+'","'+o.List(i,2)+'","'+o.List(i,3)+'")'+Chr(13)+Chr(10))
        Next
        o=This.oQuery.opage.page4.opag.fllfilter
        For i=1 To o.ListCount
            If o.List(i,6)='Having'
                If Empty(o.List(i,2)) && empty(cNot)
                    Fwrite(F,'i_qry.mLoadHaving("'+o.List(i,1)+' '+o.List(i,3)+' '+o.List(i,4)+' '+o.List(i,5)+' '+'")'+Chr(13)+Chr(10))
                Else
                    Fwrite(F,'i_qry.mLoadHaving("not('+o.List(i,1)+' '+o.List(i,3)+' '+o.List(i,4)+')'+' '+o.List(i,5)+' '+'")'+Chr(13)+Chr(10))
                Endif
            Else
                i_bSubQuery=Inlist(Trim(o.List(i,3)),'in','not in','exists','not exists') Or 'any'$Trim(o.List(i,3)) Or 'all'$Trim(o.List(i,3))
                T=o.List(i,4)
                T=Iif(Left(T,1)='?',T,Iif(i_bSubQuery,'!'+T,'.f.'))
                If Empty(o.List(i,2)) && empty(cNot)
                    Fwrite(F,'i_qry.mLoadWhere("'+o.List(i,1)+' '+o.List(i,3)+' '+Iif(i_bSubQuery,'!','')+o.List(i,4)+' '+o.List(i,5)+' '+'","'+T+'",'+Iif(i_bSubQuery,'.t.','.f.')+')'+Chr(13)+Chr(10))
                Else
                    Fwrite(F,'i_qry.mLoadWhere("not('+o.List(i,1)+' '+o.List(i,3)+' '+Iif(i_bSubQuery,'!','')+o.List(i,4)+')'+' '+o.List(i,5)+' '+'","'+T+'",'+Iif(i_bSubQuery,'.t.','.f.')+')'+Chr(13)+Chr(10))
                Endif
            Endif
        Next
        o=This.oQuery.opage.page2.opag.FdLOrder
        For i=1 To o.ListCount
            Fwrite(F,'i_qry.mLoadOrder("'+o.List(i,2)+'","'+o.List(i,3)+'")'+Chr(13)+Chr(10))
        Next
        o=This.oQuery.opage.page2.opag.FdLGroups
        For i=1 To o.ListCount
            Fwrite(F,'i_qry.mLoadGroupBy("'+o.List(i,2)+'")'+Chr(13)+Chr(10))
        NEXT
        If This.oQuery.opage.page2.opag.FdDis.Value=1
            Fwrite(F,'i_qry.cDistinct="distinct"'+Chr(13)+Chr(10))
        Endif
        o=This.oQuery.opage.page5.opag.fplparam
        For i=1 To o.ListCount
            Fwrite(F,'if i_ctx.IsVar("'+o.List(i,1)+'")'+Chr(13)+Chr(10))
            Fwrite(F,'  i_qry.mLoadValuedFilterParam("'+o.List(i,1)+'","'+o.List(i,2)+'","'+o.List(i,3)+'",'+o.List(i,4)+','+o.List(i,5)+',i_ctx.GetVar("'+o.List(i,1)+'"),"'+o.List(i,6)+'")'+Chr(13)+Chr(10))
            Fwrite(F,'else'+Chr(13)+Chr(10))
            Fwrite(F,'  i_qry.mLoadFilterParam("'+o.List(i,1)+'","'+o.List(i,2)+'","'+o.List(i,3)+'",'+o.List(i,4)+','+o.List(i,5)+',"'+o.List(i,6)+'")'+Chr(13)+Chr(10))
            Fwrite(F,'endif'+Chr(13)+Chr(10))
        Next
        If !Empty(This.cExtMask)
            Fwrite(F,'i_qry.cExtMask="'+This.cExtMask+'"')
        Endif
        If This.bRemoveWhereOnEmptyParam
            Fwrite(F,'i_qry.bRemoveWhereOnEmptyParam=.t.')
        Endif
        Fwrite(F,'')
        Fclose(F)
Enddefine

Define Class query_relation_list As CpForm
    WindowType=1
    AutoCenter=.T.
    Width=480
    oqc=.Null.
    Caption=""
    Icon=i_cBmpPath+i_cStdIcon
    Add Object btnok As CommandButton With Caption="",Width=50,Height=25,Top=This.Height-25,Left=This.Width-110
    Add Object btncancel As CommandButton With Caption="",Width=50,Height=25,Top=This.Height-25,Left=This.Width-55
    Add Object rl As ListBox With Width=This.Width,Height=This.Height-30,MultiSelect=.T.,ColumnCount=6,ColumnWidths='150,200,50,50,50,50'
    Proc btnok.Click()
        Local i,cDescr,cExp,cType,cAlias1,cAlias2
        For i=1 To This.Parent.rl.ListCount
            If Empty(This.Parent.rl.List(i,4))
                * --- i non esclusi
                cAlias1=This.Parent.rl.List(i,5)
                cAlias2=This.Parent.rl.List(i,6)
                cDescr=This.Parent.rl.List(i,1)
                cExp=This.Parent.rl.List(i,2)
                cType=This.Parent.rl.List(i,3)
                This.Parent.oqc.Addrelation(cAlias1,cAlias2,cDescr,cExp,cType,.T.)
            Endif
        Next
        This.Parent.oqc=.Null.
        Thisform.Release()
    Proc btncancel.Click()
        This.Parent.oqc=.Null.
        Thisform.Release()
    Proc Resize()
        This.rl.Width=This.Width
        This.rl.Height=This.Height-30
        This.btnok.Top=This.Height-25
        This.btnok.Left=This.Width-110
        This.btncancel.Top=This.Height-25
        This.btncancel.Left=This.Width-55
    Proc Addrelation(cAlias1,cAlias2,cDescr,cExpr,cType,bSelected)
        This.rl.AddItem(cDescr)
        This.rl.ListItem(This.rl.ListCount,2)=cExpr
        This.rl.ListItem(This.rl.ListCount,3)=cType
        This.rl.ListItem(This.rl.ListCount,4)=Iif(bSelected,'','Exclude')
        This.rl.ListItem(This.rl.ListCount,5)=cAlias1
        This.rl.ListItem(This.rl.ListCount,6)=cAlias2
    Proc rl.Click()
        Local i
        For i=1 To This.ListCount
            If This.Selected(i)
                This.List(i,4)=Iif(This.List(i,4)=='Exclude','','Exclude')
            Endif
        Next

    Proc Init()
        This.Caption=cp_Translate(MSG_RELATIONSHIPS)
        This.btnok.Caption=cp_Translate(MSG_OK_BUTTON)
        This.btncancel.Caption=cp_Translate(MSG_CANCEL_BUTTON)
        DoDefault()
Enddefine

Define Class query_findrelations As CpSysContainer
    Dimension acFR[1,3]
    Dimension aRelDef[1]
    oqc=.Null.
    cTable1=''
    cTable2=''
    oThis=.Null.
    MinHeight=100
    Top = 1
    Width = 430
	Height = 420
    BackStyle=1
    cOldIcon=''
    Icon=i_cBmpPath+i_cStdIcon
    Caption='Find relations'
    Add Object lbl1 As Label With Caption="",Alignment=1,Width=71,Top=9, BackStyle = 0, Left = 10
    Add Object txt1 As ComboBox With Top=5,Left=85,Height=25,Width=250,ColumnCount=2,ColumnWidths='250,0',ColumnLines=.F.,Style=2, Anchor=10
    Add Object lbl2 As Label With Top=40,Caption="",Alignment=1,Width=71, BackStyle = 0, Left = 10
    Add Object txt2 As ComboBox With Left=85,Top=36,Height=25,Width=250,ColumnCount=2,ColumnWidths='250,0',ColumnLines=.F.,Style=2, Anchor=10
    Add Object btn As CommandButton With Top = 5,Left=350,Width=75,Height=25,Caption="", Anchor=8
    Add Object lst As ListBox With Top=65,Width=420,Height=350, Left = 5, Anchor=15
    #If Version(5)>=900
        Add Object ImgBackGround As Image With Stretch=2, Visible=.F.
    #Endif
	
	Proc txt1.Width_Assign(xValue)
		This.Width = m.xValue
		This.ColumnWidths = Alltrim(Str(Max(m.xValue-40,0)))+',0'
	EndProc

	Proc txt2.Width_Assign(xValue)
		This.Width = m.xValue
		This.ColumnWidths = Alltrim(Str(Max(m.xValue-40,0)))+',0'
	EndProc
	
    Proc btn.Click()
        Local i
        For i=1 To This.Parent.txt1.ListCount
            If This.Parent.txt1.Selected(i)
                This.Parent.cTable1=This.Parent.txt1.List(i,2)
            Endif
            If This.Parent.txt2.Selected(i)
                This.Parent.cTable2=This.Parent.txt2.List(i,2)
            Endif
        Next
        This.Parent.lst.Clear()
        j=3
        i=1
        Do While i<=j

            CP_MSG( cp_MsgFormat(MSG_LEVEL__,Alltrim(Str(i))))
            This.Parent.FindRelations(Trim(This.Parent.cTable1),Trim(This.Parent.cTable2),1,i)
            If i=j And cp_YesNo(cp_MsgFormat(MSG_REACHED_LEVEL___C_CONTINUE_QP,Alltrim(Str(j))),32,.f.,.f.,MSG_VISUAL_QUERY)
                j=j+1
            Endif
            i=i+1
        Enddo
    Proc txt1.DragOver(oSource, nXCoord, nYCoord, nState)
        If Lower(oSource.Name)=='ltables'
            Do Case
                Case nState=0 && enter
                    This.Parent.cOldIcon=oSource.DragIcon
                    oSource.DragIcon='cross01.cur'
                Case nState=1 && leave
                    oSource.DragIcon=This.Parent.cOldIcon
            Endcase
        Endif
    Proc txt2.DragOver(oSource, nXCoord, nYCoord, nState)
        This.Parent.txt1.DragOver(oSource, nXCoord, nYCoord, nState)
    Proc txt1.DragDrop(oSource,nX,nY)
        Local i
        If Lower(oSource.Name)=='ltables'
            i=oSource.ListIndex
            If i<>0
                This.Value=oSource.List(i,1)
            Endif
        Endif
    Proc txt2.DragDrop(oSource,nX,nY)
        Local i
        If Lower(oSource.Name)=='ltables'
            i=oSource.ListIndex
            If i<>0
                This.Value=oSource.List(i,1)
            Endif
        Endif
    Endproc
    Procedure Show_()
        If This.Visible And Upper(This.Parent.Controls[this.parent.controlcount].Class) == Upper(This.Class)
            This.Hide()
            This.Parent.oGraph.ZOrder(0)
        Else
            This.Show()
            This.Resize()
            This.ZOrder(0)
        Endif
    Endproc
	
	Proc SetInfo()
	*--- Lanciato all'activate della pagina di Find Rel.
		Local i
		if This.txt1.ListCount = 0
			For i=1 To This.oqc.oDic.lTables.ListCount
				This.txt1.AddItem(This.oqc.oDic.lTables.List(i,1))
				This.txt1.List(i,2)=This.oqc.oDic.lTables.List(i,2)
				This.txt2.AddItem(This.oqc.oDic.lTables.List(i,1))
				This.txt2.List(i,2)=This.oqc.oDic.lTables.List(i,2)
			Next
		Endif			
		If !Empty(This.oqc.oDic.cCurrTable)
			This.txt1.Value=This.oqc.oDcx.GetTableDescr(This.oqc.oDcx.GetTableIdx(This.oqc.oDic.cCurrTable))
		Endif	
	EndProc
	
    Proc Init(oqc)
        This.oqc=oqc
        && For i=1 To This.oqc.oDic.lTables.ListCount
            && This.txt1.AddItem(This.oqc.oDic.lTables.List(i,1))
            && This.txt1.List(i,2)=This.oqc.oDic.lTables.List(i,2)
            && This.txt2.AddItem(This.oqc.oDic.lTables.List(i,1))
            && This.txt2.List(i,2)=This.oqc.oDic.lTables.List(i,2)
        && Next
        && If !Empty(This.oqc.oDic.cCurrTable)
            && This.txt1.Value=This.oqc.oDcx.GetTableDescr(This.oqc.oDcx.GetTableIdx(This.oqc.oDic.cCurrTable))
        && Endif
        This.lbl1.Caption=cp_Translate(MSG_FROM_TABLE+MSG_FS)
        This.lbl2.Caption=cp_Translate(MSG_TO_TABLE+MSG_FS)
        This.btn.Caption=cp_Translate(MSG_FIND)
        If i_VisualTheme<>-1
            If i_bGradientBck
                With This.ImgBackGround
                    .Width = This.Width
                    .Height = This.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            This.BackColor = i_ThemesManager.GetProp(7)
        Else
            This.BackColor = Rgb(252,252,254)
        Endif

    && Proc Resize()
        && This.lst.Height=This.Height-60
        && This.lst.Width=This.Width
    Proc FindRelations(cTable1,cTable2,nDeep,nMaxDeep)
        Local i,j,cTmp1,cTmp2
        If nDeep>nMaxDeep
            Return
        Endif
        If cTable1==cTable2
            If nDeep=nMaxDeep
                * --- ha trovato la relazione richiesta!
                cTmp1=''
                cTmp2=''
                For i=1 To nDeep-1
                    cTmp1=cTmp1+Iif(!Empty(cTmp1),',','')+This.oqc.oDcx.GetTableDescr(This.oqc.oDcx.GetTableIdx(This.acFR[i,1])) &&+'('+alltrim(str(this.acFR[i,2]))+')'
                    cTmp2=cTmp2+This.acFR[i,1]+','+Alltrim(Str(This.acFR[i,2]))+','+Iif(This.acFR[i,3],'T','F')+';'
                Next
                cTmp1=cTmp1+','+This.oqc.oDcx.GetTableDescr(This.oqc.oDcx.GetTableIdx(cTable1))
                cTmp2=cTmp2+cTable1
                This.lst.AddItem(cTmp1)
                i=This.lst.ListCount
                Dimension This.aRelDef[i]
                This.aRelDef[i]=cTmp2
            Endif
        Else
            Dimension This.acFR[nDeep,3]
            This.acFR[nDeep,1]=cTable1
            * --- cerca verso le relazioni "dritte"
            For i=1 To This.oqc.oDcx.GetFKCount(cTable1)
                cTmp=This.oqc.oDcx.GetFKTable(cTable1,i) && file con cui e' in relazione
                This.acFR[nDeep,2]=i && la relazione utilizzata
                This.acFR[nDeep,3]=.T.  && relazione diretta
                * --- scende di livello
                If !This.CircleFR(cTmp,nDeep) && evita le circolarita'
                    This.FindRelations(cTmp,cTable2,nDeep+1,nMaxDeep)
                Endif
            Next
            * --- cerca verso le relazioni "inverse"
            For i=1 To This.oqc.oDcx.GetIFKCount(cTable1)
                cTmp=This.oqc.oDcx.GetIFKTable(cTable1,i) && nome della tabella
                This.acFR[nDeep,2]=i && la relazione utilizzata
                This.acFR[nDeep,3]=.F. && relazione inversa
                * --- scende di livello
                If !This.CircleFR(cTmp,nDeep) && evita le circolarita'
                    This.FindRelations(cTmp,cTable2,nDeep+1,nMaxDeep)
                Endif
            Next
        Endif
        Return
    Func CircleFR(cTable,nDeep)
        Local i
        For i=1 To nDeep
            If This.acFR[i,1]==cTable
                Return(.T.)
            Endif
        Next
        Return(.F.)
    Proc AddTableS(cOp)
        Local p,cTb,i,j,cRel,nRel,bFK,cDescr,cExpr
        cRel=''
        nRel=0
        bIFK=.F.
        p=At(';',cOp)
        Do While p<>0
            * --- trova la tabella
            cTb=Left(cOp,p-1)
            j=At(',',cTb)
            i=At(',',cTb,2)
            cTable=Left(cTb,j-1)
            nRel=Val(Substr(cTb,j+1,i-j))
            bFK=Right(cTb,1)='T'
            If bFK
                cRel=This.oqc.oDcx.GetFKTable(cTable,nRel)
            Else
                cRel=This.oqc.oDcx.GetIFKTable(cTable,nRel)
            Endif
            * --- aggiunge la tabella e la tabella correlata
            If !This.oqc.IsUsedTable(cTable)
                This.oqc.AddTable(cTable,This.oqc.oDcx.GetTableDescr(This.oqc.oDcx.GetTableIdx(cTable)),.T.)
            Endif
            If !This.oqc.IsUsedTable(cRel)
                This.oqc.AddTable(cRel,This.oqc.oDcx.GetTableDescr(This.oqc.oDcx.GetTableIdx(cRel)),.T.)
            Endif
            * --- aggiunge la relazione
            If bFK
                cDescr=This.oqc.oDcx.GetFKDescr(cTable,nRel)
                cExpr=This.oqc.oDcx.GetFKExpr(cTable,nRel,cRel,cTable)
                This.oqc.Addrelation(cTable,cRel,cDescr,cExpr,'Inner',.T.)
            Else
                cDescr=This.oqc.oDcx.GetIFKDescr(cTable,nRel)
                cExpr=This.oqc.oDcx.GetIFKExpr(cTable,nRel,cTable,cRel)
                This.oqc.Addrelation(cTable,cRel,cDescr,cExpr,'Inner',.T.)
            Endif
            * --- passa alla prossima relazione
            cOp=Substr(cOp,p+1)
            p=At(';',cOp)
        Enddo
        * --- aggiunge l' ultima tabella, se non gia' definita
        If !This.oqc.IsUsedTable(cOp)
            This.oqc.AddTable(cOp,This.oqc.oDcx.GetTableDescr(This.oqc.oDcx.GetTableIdx(cOp)),.T.)
        Endif
    Proc lst.DblClick()
        Local i,j
        j=0
        For i=1 To This.ListCount
            If This.Selected(i)
                j=i
            Endif
        Next
        If j<>0
            This.Parent.AddTableS(This.Parent.aRelDef[j])
        Endif
Enddefine
