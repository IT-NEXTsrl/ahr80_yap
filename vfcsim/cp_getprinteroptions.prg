* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_getprinteroptions                                            *
*              Stampa                                                          *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-04-10                                                      *
* Last revis.: 2012-04-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_getprinteroptions",oParentObject))

* --- Class definition
define class tcp_getprinteroptions as StdForm
  Top    = 72
  Left   = 10

  * --- Standard Properties
  Width  = 517
  Height = 321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-20"
  HelpContextID=253511429
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_getprinteroptions"
  cComment = "Stampa"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PRINTER = space(100)
  w_PAGERANGE1 = space(10)
  w_PAGERANGE = space(100)
  w_PRINTEROPTIONS = space(100)
  w_COPIES = 0
  w_PAGES = 0
  w_OKPRINT = space(10)
  w_TXT1 = space(10)
  w_Lbl1Pag1 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_getprinteroptionsPag1","cp_getprinteroptions",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPAGERANGE1_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Lbl1Pag1 = this.oPgFrm.Pages(1).oPag.Lbl1Pag1
    DoDefault()
    proc Destroy()
      this.w_Lbl1Pag1 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PRINTER=space(100)
      .w_PAGERANGE1=space(10)
      .w_PAGERANGE=space(100)
      .w_PRINTEROPTIONS=space(100)
      .w_COPIES=0
      .w_PAGES=0
      .w_OKPRINT=space(10)
      .w_TXT1=space(10)
      .w_PRINTER=oParentObject.w_PRINTER
      .w_PAGERANGE=oParentObject.w_PAGERANGE
      .w_PRINTEROPTIONS=oParentObject.w_PRINTEROPTIONS
      .w_COPIES=oParentObject.w_COPIES
      .w_PAGES=oParentObject.w_PAGES
      .w_OKPRINT=oParentObject.w_OKPRINT
          .DoRTCalc(1,1,.f.)
        .w_PAGERANGE1 = "T"
          .DoRTCalc(3,6,.f.)
        .w_OKPRINT = .T.
        .w_TXT1 = AH_MSGFORMAT("Immettere numeri di pagina e/o intervalli di%0pagine separati da virgole. Esempio 1,3,5-12")
      .oPgFrm.Page1.oPag.Lbl1Pag1.Calculate(.w_TXT1)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PRINTER=.w_PRINTER
      .oParentObject.w_PAGERANGE=.w_PAGERANGE
      .oParentObject.w_PRINTEROPTIONS=.w_PRINTEROPTIONS
      .oParentObject.w_COPIES=.w_COPIES
      .oParentObject.w_PAGES=.w_PAGES
      .oParentObject.w_OKPRINT=.w_OKPRINT
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Lbl1Pag1.Calculate(.w_TXT1)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Lbl1Pag1.Calculate(.w_TXT1)
    endwith
  return

  proc Calculate_MSLBSTMMXZ()
    with this
          * --- Sceglie la stampante
          .w_PRINTER = GetPrinter()
          .w_PRINTEROPTIONS = ""
    endwith
  endproc
  proc Calculate_CZYPWDBGPB()
    with this
          * --- Azzera l'intervallo di stampa
          .w_PAGERANGE = IIF( .w_PAGERANGE1 ="T", "", "1 - " + ALLTRIM(STR( .w_PAGES )) )
    endwith
  endproc
  proc Calculate_ENPHMFCWFT()
    with this
          * --- Imposta le preferenze di stampa
          .w_PRINTEROPTIONS = _xfPrinterProperties(.w_PRINTER, "", .T.)
    endwith
  endproc
  proc Calculate_AYTZTKAPGF()
    with this
          * --- Aumenta le copie
          .w_COPIES = .w_COPIES + 1
    endwith
  endproc
  proc Calculate_JVNUCBIFTR()
    with this
          * --- Diminuisce le copie
          .w_COPIES = .w_COPIES - 1
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPAGERANGE_1_10.enabled = this.oPgFrm.Page1.oPag.oPAGERANGE_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("GetPrinter")
          .Calculate_MSLBSTMMXZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_PAGERANGE1 Changed")
          .Calculate_CZYPWDBGPB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("PrinterOptions")
          .Calculate_ENPHMFCWFT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CopiesUp")
          .Calculate_AYTZTKAPGF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CopiesDown")
          .Calculate_JVNUCBIFTR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.Lbl1Pag1.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPRINTER_1_2.value==this.w_PRINTER)
      this.oPgFrm.Page1.oPag.oPRINTER_1_2.value=this.w_PRINTER
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGERANGE1_1_8.RadioValue()==this.w_PAGERANGE1)
      this.oPgFrm.Page1.oPag.oPAGERANGE1_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGERANGE_1_10.value==this.w_PAGERANGE)
      this.oPgFrm.Page1.oPag.oPAGERANGE_1_10.value=this.w_PAGERANGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPIES_1_18.value==this.w_COPIES)
      this.oPgFrm.Page1.oPag.oCOPIES_1_18.value=this.w_COPIES
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_getprinteroptionsPag1 as StdContainer
  Width  = 513
  height = 321
  stdWidth  = 513
  stdheight = 321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="UXNSLOLVTR",left=15, top=33, width=48,height=45,;
    CpPicture="PRSTAMPA.bmp", caption="", nPag=1;
    , ToolTipText = "Ripete l'interrogazione";
    , HelpContextID = 109237171;
    , caption='\<Printer';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        .NotifyEvent("GetPrinter")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPRINTER_1_2 as StdField with uid="XTAUARIGKV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PRINTER", cQueryName = "PRINTER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 109097363,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=90, Top=44, InputMask=replicate('X',100)

  add object oPAGERANGE1_1_8 as StdRadio with uid="NXLCOXWLAT",rtseq=2,rtrep=.f.,left=15, top=174, width=75,height=35;
    , cFormVar="w_PAGERANGE1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPAGERANGE1_1_8.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Tutte"
      this.Buttons(1).HelpContextID = 186084714
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Pagine"
      this.Buttons(2).HelpContextID = 186084714
      this.Buttons(2).Top=16
      this.SetAll("Width",73)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oPAGERANGE1_1_8.RadioValue()
    return(iif(this.value =1,"T",;
    iif(this.value =2,"P",;
    space(10))))
  endfunc
  func oPAGERANGE1_1_8.GetRadio()
    this.Parent.oContained.w_PAGERANGE1 = this.RadioValue()
    return .t.
  endfunc

  func oPAGERANGE1_1_8.SetRadio()
    this.Parent.oContained.w_PAGERANGE1=trim(this.Parent.oContained.w_PAGERANGE1)
    this.value = ;
      iif(this.Parent.oContained.w_PAGERANGE1=="T",1,;
      iif(this.Parent.oContained.w_PAGERANGE1=="P",2,;
      0))
  endfunc

  add object oPAGERANGE_1_10 as StdField with uid="FNGVQGAGDE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PAGERANGE", cQueryName = "PAGERANGE",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 169307498,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=90, Top=185, InputMask=replicate('X',100)

  func oPAGERANGE_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PAGERANGE1 = "P")
    endwith
   endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="YLADTDBSKH",left=15, top=84, width=48,height=45,;
    CpPicture="PROPTION.bmp", caption="", nPag=1;
    , ToolTipText = "Imposta le preferenze di stampa";
    , HelpContextID = 74640478;
    , caption='\<Options';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .NotifyEvent("PrinterOptions")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="OLNMHXADLC",left=407, top=272, width=48,height=45,;
    CpPicture="OK.bmp", caption="", nPag=1;
    , ToolTipText = "Lancia la stampa";
    , HelpContextID = 9746683;
    , caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="PTAFAFGHOT",left=459, top=272, width=48,height=45,;
    CpPicture="ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Chiude la maschera";
    , HelpContextID = 154450066;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOPIES_1_18 as StdField with uid="SVVYRMZMEY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_COPIES", cQueryName = "COPIES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di copie",;
    HelpContextID = 223633762,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=346, Top=174


  add object oBtn_1_19 as StdButton with uid="DKCEKBFEWS",left=427, top=171, width=15,height=15,;
    CpPicture="UP.bmp", caption="", nPag=1;
    , HelpContextID = 188004603;
    , ImgSize=8;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        .NotifyEvent("CopiesUp")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="QAFIURAGOO",left=427, top=184, width=15,height=15,;
    CpPicture="DOWN.bmp", caption="", nPag=1;
    , HelpContextID = 239384827;
    , ImgSize=8;
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      with this.Parent.oContained
        .NotifyEvent("CopiesDown")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_COPIES > 1)
      endwith
    endif
  endfunc


  add object Lbl1Pag1 as cp_calclbl with uid="YHQRYRYVUQ",left=12, top=219, width=226,height=37,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="",FontName="Arial",FontSize=9,FontBold=True,Alignment=0,ForeColor=0,;
    nPag=1;
    , HelpContextID = 108207414

  add object oStr_1_4 as StdString with uid="WWPQAEZJBN",Visible=.t., Left=14, Top=12,;
    Alignment=0, Width=116, Height=18,;
    Caption="Seleziona stampante"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="NHMWVDREJY",Visible=.t., Left=14, Top=140,;
    Alignment=0, Width=112, Height=18,;
    Caption="Pagine da stampare"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="MBVYJZLPXB",Visible=.t., Left=268, Top=143,;
    Alignment=0, Width=91, Height=18,;
    Caption="Numero di copie"  ;
  , bGlobalFont=.t.

  add object oBox_1_5 as StdBox with uid="YIUVDFQQGV",left=6, top=5, width=502,height=130

  add object oBox_1_7 as StdBox with uid="GHRQZIOYQL",left=6, top=137, width=247,height=130

  add object oBox_1_15 as StdBox with uid="YTFXKKUREB",left=261, top=137, width=247,height=131
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_getprinteroptions','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
