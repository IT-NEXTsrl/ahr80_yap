* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_EXPRT
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Samuele Masetto
* Data creazione: 17/09/97
* Aggiornato il : 17/09/97
* Translated    : 03/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Esporta i dati del cursore 'i_cCursor' nella tabella esterna 'i_cTableName'
* Ritorna TRUE se la tabella � stata trasferita con successo, FALSE altrimenti
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Parameters i_cTableName,i_cCursor
Local i_nTableIdx,i_nConn,i_bTransfer
If At('cp_tblib',Lower(Set('proc')))=0
    Set Procedure To cp_TbLib Additive
Endif


i_nConn=0
i_bTransfer=.T.
* --- apre la tabella
i_nTableIdx=cp_OpenTable(i_cTableName)
If i_nTableIdx=0
    cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_OPEN_TABLE_A___A_QM,i_cTableName),0,'',.f.)
Else
    i_nConn=i_TableProp[i_nTableIdx,3] && connessione della tabella i_cDbTo
    i_cTable=cp_SetAzi(i_TableProp[i_nTableIdx,2]) && file fisico della tabella i_cDbTo
Endif
* --- esporta i dati
i_bTransfer=InsertOneTable(i_nConn,i_cTable,i_cCursor)
* --- chiude le tabelle
If i_nTableIdx<>0
    cp_CloseTable(i_cTableName)
Endif
Select (i_cCursor)
Use
Return(i_bTransfer)

