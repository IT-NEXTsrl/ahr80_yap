* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_infomon                                                      *
*              Information                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-04-16                                                      *
* Last revis.: 2008-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_infomon",oParentObject))

* --- Class definition
define class tcp_infomon as StdForm
  Top    = 6
  Left   = 5

  * --- Standard Properties
  Width  = 498
  Height = 388
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-10"
  HelpContextID=97844658
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "cp_infomon"
  cComment = "Information"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CURFILENAME = space(254)
  w_CURFILEDESC = space(80)
  w_CURFLSTATUS = space(1)
  w_CURFILETYPE = space(4)
  w_CURFLDESUTE = space(20)
  w_CURFLDESGRP = space(20)
  w_CURFLDESAZI = space(40)
  w_CURFLCODUTE = 0
  w_CURFLCODGRP = 0
  w_CURFLCODAZI = space(5)
  w_CURFLREVFIL = 0
  w_CURFLAUTHOR = space(50)
  w_CURFL_NOTES = space(0)
  w_CURFLORGNAM = space(254)
  w_CURFLCUSTOM = space(1)
  w_CURFLCUSTOM = space(1)
  * --- Area Manuale = Declare Variables
  * --- cp_infomon
  Autocenter=.t.
  
  * Bottone Info
  *Bottone Ok
  FUNCTION Info_OK()
     LOCAL new_fullname, _infocursor_, _infocursor2_, row_repeated, row_count, l_modified, l_oldArea
     l_oldArea = SELECT()
     * Controllo se c'� un record selezionato
     SELECT * FROM (This.oparentobject.w_zoom.cCursor) WHERE UPPER(ALLTRIM(FLORGNAM)) = UPPER(ALLTRIM(This.w_CURFLORGNAM)) INTO CURSOR _infocursor_
     row_count = IIF(USED('_infocursor_'),RECCOUNT(),0)
     IF row_count=1
        *--- Controllo se � stato modificato l'utente o il gruppo o l'azienda o lo stato (@)
        l_modified = (This.w_CURFLCODUTE != _infocursor_.FLCODUTE OR ;
                      This.w_CURFLCODGRP != _infocursor_.FLCODGRP OR ;
                      This.w_CURFLCODAZI != _infocursor_.FLCODAZI OR ;
                      This.w_CURFLSTATUS != _infocursor_.FLSTATUS)
        IF l_modified && creo il nuovo nome del file
           new_fullname = cp_monitor_buildname(_infocursor_.FLORGNAM, This.w_CURFLCODUTE, This.w_CURFLCODGRP, This.w_CURFLCODAZI, This.w_CURFLSTATUS, _infocursor_.FLCUSTOM)
           SELECT * FROM (This.oparentobject.w_zoom.cCursor) WHERE FLORGNAM = new_fullname INTO CURSOR _infocursor2_
           row_repeated=IIF(USED('_infocursor2_'),RECCOUNT(),0)
           IF row_repeated == 0
              * Aggiorno il monitor con i campi modificati
              UPDATE cp_monitor ;
                 SET FILEDESC=This.w_CURFILEDESC, FLSTATUS= This.w_CURFLSTATUS, ;
                 FLCODUTE= This.w_CURFLCODUTE, FLCODGRP= This.w_CURFLCODGRP, ;
                 FLCODAZI= This.w_CURFLCODAZI, FLREVFIL= This.w_CURFLREVFIL, ;
                 FLAUTHOR= This.w_CURFLAUTHOR, FL_NOTES= This.w_CURFL_NOTES, FLDESUTE= This.w_CURFLDESUTE,;
                 FLDESGRP= This.w_CURFLDESGRP, FLDESAZI= This.w_CURFLDESAZI, ;
                 FLORGNAM= new_fullname , FLMODFIL="M";
                 WHERE UPPER(ALLTRIM(FLORGNAM)) = UPPER(ALLTRIM(This.w_CURFLORGNAM))
           ELSE
              cp_errormsg("Modifica non consentita",'i')
              RETURN .F.
           ENDIF
           USE IN SELECT('_infocursor2_')
        ELSE && non sono stati modificati utente gruppo azienda o stato
           UPDATE cp_monitor ;
              SET FILEDESC=This.w_CURFILEDESC, FLREVFIL=This.w_CURFLREVFIL, ;
              FLAUTHOR=This.w_CURFLAUTHOR, FL_NOTES=This.w_CURFL_NOTES, FLMODFIL="M"  ;
              WHERE UPPER(ALLTRIM(FLORGNAM)) = UPPER(ALLTRIM(This.w_CURFLORGNAM))
        ENDIF
     ENDIF
     USE IN SELECT('_infocursor_')
     *--- Restore old area
     SELECT(m.l_oldArea)
     This.oparentobject.w_REFRESHZOOMVAR = .T.
     This.oparentobject.NotifyEvent('RefreshZoom')
     RETURN .T.
  ENDFUNC
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_infomonPag1","cp_infomon",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCURFILEDESC_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CPGROUPS'
    this.cWorkTables[3]='AZIENDA'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CURFILENAME=space(254)
      .w_CURFILEDESC=space(80)
      .w_CURFLSTATUS=space(1)
      .w_CURFILETYPE=space(4)
      .w_CURFLDESUTE=space(20)
      .w_CURFLDESGRP=space(20)
      .w_CURFLDESAZI=space(40)
      .w_CURFLCODUTE=0
      .w_CURFLCODGRP=0
      .w_CURFLCODAZI=space(5)
      .w_CURFLREVFIL=0
      .w_CURFLAUTHOR=space(50)
      .w_CURFL_NOTES=space(0)
      .w_CURFLORGNAM=space(254)
      .w_CURFLCUSTOM=space(1)
      .w_CURFLCUSTOM=space(1)
      .w_CURFILENAME=oParentObject.w_CURFILENAME
      .w_CURFILEDESC=oParentObject.w_CURFILEDESC
      .w_CURFLSTATUS=oParentObject.w_CURFLSTATUS
      .w_CURFILETYPE=oParentObject.w_CURFILETYPE
      .w_CURFLCODUTE=oParentObject.w_CURFLCODUTE
      .w_CURFLCODGRP=oParentObject.w_CURFLCODGRP
      .w_CURFLCODAZI=oParentObject.w_CURFLCODAZI
      .w_CURFLREVFIL=oParentObject.w_CURFLREVFIL
      .w_CURFLAUTHOR=oParentObject.w_CURFLAUTHOR
      .w_CURFL_NOTES=oParentObject.w_CURFL_NOTES
      .w_CURFLORGNAM=oParentObject.w_CURFLORGNAM
      .w_CURFLCUSTOM=oParentObject.w_CURFLCUSTOM
      .w_CURFLCUSTOM=oParentObject.w_CURFLCUSTOM
        .DoRTCalc(1,8,.f.)
        if not(empty(.w_CURFLCODUTE))
          .link_1_12('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CURFLCODGRP))
          .link_1_14('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CURFLCODAZI))
          .link_1_16('Full')
        endif
    endwith
    this.DoRTCalc(11,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CURFILENAME=.w_CURFILENAME
      .oParentObject.w_CURFILEDESC=.w_CURFILEDESC
      .oParentObject.w_CURFLSTATUS=.w_CURFLSTATUS
      .oParentObject.w_CURFILETYPE=.w_CURFILETYPE
      .oParentObject.w_CURFLCODUTE=.w_CURFLCODUTE
      .oParentObject.w_CURFLCODGRP=.w_CURFLCODGRP
      .oParentObject.w_CURFLCODAZI=.w_CURFLCODAZI
      .oParentObject.w_CURFLREVFIL=.w_CURFLREVFIL
      .oParentObject.w_CURFLAUTHOR=.w_CURFLAUTHOR
      .oParentObject.w_CURFL_NOTES=.w_CURFL_NOTES
      .oParentObject.w_CURFLORGNAM=.w_CURFLORGNAM
      .oParentObject.w_CURFLCUSTOM=.w_CURFLCUSTOM
      .oParentObject.w_CURFLCUSTOM=.w_CURFLCUSTOM
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCURFILEDESC_1_3.enabled = this.oPgFrm.Page1.oPag.oCURFILEDESC_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCURFLSTATUS_1_5.enabled = this.oPgFrm.Page1.oPag.oCURFLSTATUS_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCURFLCODUTE_1_12.enabled = this.oPgFrm.Page1.oPag.oCURFLCODUTE_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCURFLCODGRP_1_14.enabled = this.oPgFrm.Page1.oPag.oCURFLCODGRP_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCURFLCODAZI_1_16.enabled = this.oPgFrm.Page1.oPag.oCURFLCODAZI_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCURFLREVFIL_1_18.enabled = this.oPgFrm.Page1.oPag.oCURFLREVFIL_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCURFLAUTHOR_1_19.enabled = this.oPgFrm.Page1.oPag.oCURFLAUTHOR_1_19.mCond()
    this.oPgFrm.Page1.oPag.oCURFL_NOTES_1_20.enabled = this.oPgFrm.Page1.oPag.oCURFL_NOTES_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CURFLCODUTE
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CURFLCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CURFLCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CURFLCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CURFLCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCURFLCODUTE_1_12'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CURFLCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CURFLCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CURFLCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CURFLCODUTE = NVL(_Link_.CODE,0)
      this.w_CURFLDESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CURFLCODUTE = 0
      endif
      this.w_CURFLDESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CURFLCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CURFLCODGRP
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CURFLCODGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CURFLCODGRP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CURFLCODGRP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CURFLCODGRP) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oCURFLCODGRP_1_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CURFLCODGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CURFLCODGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CURFLCODGRP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CURFLCODGRP = NVL(_Link_.CODE,0)
      this.w_CURFLDESGRP = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CURFLCODGRP = 0
      endif
      this.w_CURFLDESGRP = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CURFLCODGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CURFLCODAZI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CURFLCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZI',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_CURFLCODAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_CURFLCODAZI))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CURFLCODAZI)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CURFLCODAZI) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oCURFLCODAZI_1_16'),i_cWhere,'GSAR_AZI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CURFLCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CURFLCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CURFLCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CURFLCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_CURFLDESAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CURFLCODAZI = space(5)
      endif
      this.w_CURFLDESAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CURFLCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCURFILENAME_1_1.value==this.w_CURFILENAME)
      this.oPgFrm.Page1.oPag.oCURFILENAME_1_1.value=this.w_CURFILENAME
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFILEDESC_1_3.value==this.w_CURFILEDESC)
      this.oPgFrm.Page1.oPag.oCURFILEDESC_1_3.value=this.w_CURFILEDESC
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLSTATUS_1_5.RadioValue()==this.w_CURFLSTATUS)
      this.oPgFrm.Page1.oPag.oCURFLSTATUS_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFILETYPE_1_7.RadioValue()==this.w_CURFILETYPE)
      this.oPgFrm.Page1.oPag.oCURFILETYPE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLDESUTE_1_9.value==this.w_CURFLDESUTE)
      this.oPgFrm.Page1.oPag.oCURFLDESUTE_1_9.value=this.w_CURFLDESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLDESGRP_1_10.value==this.w_CURFLDESGRP)
      this.oPgFrm.Page1.oPag.oCURFLDESGRP_1_10.value=this.w_CURFLDESGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLDESAZI_1_11.value==this.w_CURFLDESAZI)
      this.oPgFrm.Page1.oPag.oCURFLDESAZI_1_11.value=this.w_CURFLDESAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLCODUTE_1_12.value==this.w_CURFLCODUTE)
      this.oPgFrm.Page1.oPag.oCURFLCODUTE_1_12.value=this.w_CURFLCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLCODGRP_1_14.value==this.w_CURFLCODGRP)
      this.oPgFrm.Page1.oPag.oCURFLCODGRP_1_14.value=this.w_CURFLCODGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLCODAZI_1_16.value==this.w_CURFLCODAZI)
      this.oPgFrm.Page1.oPag.oCURFLCODAZI_1_16.value=this.w_CURFLCODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLREVFIL_1_18.value==this.w_CURFLREVFIL)
      this.oPgFrm.Page1.oPag.oCURFLREVFIL_1_18.value=this.w_CURFLREVFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLAUTHOR_1_19.value==this.w_CURFLAUTHOR)
      this.oPgFrm.Page1.oPag.oCURFLAUTHOR_1_19.value=this.w_CURFLAUTHOR
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFL_NOTES_1_20.value==this.w_CURFL_NOTES)
      this.oPgFrm.Page1.oPag.oCURFL_NOTES_1_20.value=this.w_CURFL_NOTES
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFLCUSTOM_1_26.RadioValue()==this.w_CURFLCUSTOM)
      this.oPgFrm.Page1.oPag.oCURFLCUSTOM_1_26.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- cp_infomon
      if i_bRes
         i_bRes = This.Info_OK()
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_infomonPag1 as StdContainer
  Width  = 494
  height = 388
  stdWidth  = 494
  stdheight = 388
  resizeXpos=297
  resizeYpos=265
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCURFILENAME_1_1 as StdField with uid="GUJNFTOWRR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CURFILENAME", cQueryName = "CURFILENAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 99610446,;
   bGlobalFont=.t.,;
    Height=21, Width=249, Left=84, Top=5, InputMask=replicate('X',254)

  add object oCURFILEDESC_1_3 as StdField with uid="LKYPPJHDYZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CURFILEDESC", cQueryName = "CURFILEDESC",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 97920334,;
   bGlobalFont=.t.,;
    Height=21, Width=401, Left=84, Top=32, InputMask=replicate('X',80)

  func oCURFILEDESC_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (cp_set_get_right()>3 or (cp_set_get_right()==3 and .w_curflcodute==i_codute))
    endwith
   endif
  endfunc


  add object oCURFLSTATUS_1_5 as StdCombo with uid="QAZKTXKGID",rtseq=3,rtrep=.f.,left=84,top=58,width=104,height=21;
    , HelpContextID = 114889541;
    , cFormVar="w_CURFLSTATUS",RowSource=""+"Enable,"+"Disable", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCURFLSTATUS_1_5.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oCURFLSTATUS_1_5.GetRadio()
    this.Parent.oContained.w_CURFLSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oCURFLSTATUS_1_5.SetRadio()
    this.Parent.oContained.w_CURFLSTATUS=trim(this.Parent.oContained.w_CURFLSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_CURFLSTATUS=='E',1,;
      iif(this.Parent.oContained.w_CURFLSTATUS=='D',2,;
      0))
  endfunc

  func oCURFLSTATUS_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (cp_set_get_right()>3 or (cp_set_get_right()==3 and .w_curflcodute==i_codute))
    endwith
   endif
  endfunc


  add object oCURFILETYPE_1_7 as StdCombo with uid="IDQCMAXYXR",rtseq=4,rtrep=.f.,left=385,top=6,width=101,height=21, enabled=.f.;
    , HelpContextID = 99906894;
    , cFormVar="w_CURFILETYPE",RowSource=""+"Visual Query,"+"Visual Mask,"+"Visual Zoom,"+"Visual Zoom 2,"+"Men�,"+"Module Excel,"+"Graphics,"+"Report,"+"Module Word,"+"OpenOffice Calc,"+"OpenOffice Writer,"+"Add Fields,"+"Visual run-time", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCURFILETYPE_1_7.RadioValue()
    return(iif(this.value =1,'VQR',;
    iif(this.value =2,'VFM',;
    iif(this.value =3,'VZM',;
    iif(this.value =4,'_VZM',;
    iif(this.value =5,'VMN',;
    iif(this.value =6,'XLT',;
    iif(this.value =7,'VGR',;
    iif(this.value =8,'FRX',;
    iif(this.value =9,'DOC',;
    iif(this.value =10,'STC',;
    iif(this.value =11,'SXW',;
    iif(this.value =12,'XDC',;
    iif(this.value =13,'VRT',;
    space(4)))))))))))))))
  endfunc
  func oCURFILETYPE_1_7.GetRadio()
    this.Parent.oContained.w_CURFILETYPE = this.RadioValue()
    return .t.
  endfunc

  func oCURFILETYPE_1_7.SetRadio()
    this.Parent.oContained.w_CURFILETYPE=trim(this.Parent.oContained.w_CURFILETYPE)
    this.value = ;
      iif(this.Parent.oContained.w_CURFILETYPE=='VQR',1,;
      iif(this.Parent.oContained.w_CURFILETYPE=='VFM',2,;
      iif(this.Parent.oContained.w_CURFILETYPE=='VZM',3,;
      iif(this.Parent.oContained.w_CURFILETYPE=='_VZM',4,;
      iif(this.Parent.oContained.w_CURFILETYPE=='VMN',5,;
      iif(this.Parent.oContained.w_CURFILETYPE=='XLT',6,;
      iif(this.Parent.oContained.w_CURFILETYPE=='VGR',7,;
      iif(this.Parent.oContained.w_CURFILETYPE=='FRX',8,;
      iif(this.Parent.oContained.w_CURFILETYPE=='DOC',9,;
      iif(this.Parent.oContained.w_CURFILETYPE=='STC',10,;
      iif(this.Parent.oContained.w_CURFILETYPE=='SXW',11,;
      iif(this.Parent.oContained.w_CURFILETYPE=='XDC',12,;
      iif(this.Parent.oContained.w_CURFILETYPE=='VRT',13,;
      0)))))))))))))
  endfunc

  add object oCURFLDESUTE_1_9 as StdField with uid="WUECNIACSX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CURFLDESUTE", cQueryName = "CURFLDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 100152390,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=193, Top=84, InputMask=replicate('X',20)

  add object oCURFLDESGRP_1_10 as StdField with uid="MYUOMYHIPK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CURFLDESGRP", cQueryName = "CURFLDESGRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 111498310,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=193, Top=109, InputMask=replicate('X',20)

  add object oCURFLDESAZI_1_11 as StdField with uid="VBWEZATOHZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CURFLDESAZI", cQueryName = "CURFLDESAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104657990,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=193, Top=135, InputMask=replicate('X',40)

  add object oCURFLCODUTE_1_12 as StdField with uid="DADKBIFEFO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CURFLCODUTE", cQueryName = "CURFLCODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 100148709,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=84, Top=84, cSayPict="'99'", cGetPict="'99'", bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CURFLCODUTE"

  func oCURFLCODUTE_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CURFLCODGRP==0 and cp_set_get_right()>3 And .w_CURFILETYPE<>'VQR')
    endwith
   endif
  endfunc

  func oCURFLCODUTE_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCURFLCODUTE_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCURFLCODUTE_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCURFLCODUTE_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCURFLCODGRP_1_14 as StdField with uid="WRRMONDJHO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CURFLCODGRP", cQueryName = "CURFLCODGRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 111494629,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=84, Top=109, cSayPict="'99'", cGetPict="'99'", bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_CURFLCODGRP"

  func oCURFLCODGRP_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CURFLCODUTE==0 and cp_set_get_right()>3 And .w_CURFILETYPE<>'VQR')
    endwith
   endif
  endfunc

  func oCURFLCODGRP_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCURFLCODGRP_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCURFLCODGRP_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oCURFLCODGRP_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCURFLCODAZI_1_16 as StdField with uid="XIHEENYALY",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CURFLCODAZI", cQueryName = "CURFLCODAZI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 104654309,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=84, Top=135, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AZIENDA", cZoomOnZoom="GSAR_AZI", oKey_1_1="AZCODAZI", oKey_1_2="this.w_CURFLCODAZI"

  func oCURFLCODAZI_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CURFLCUSTOM=="U" and at(upper("\rightclick\"),upper(alltrim(.w_CURFLORGNAM)))==0 And at(upper("\deskmenu\"),upper(alltrim(.w_CURFLORGNAM)))==0 and (cp_set_get_right()>3 or (cp_set_get_right()==3 and .w_curflcodute==i_codute)))
    endwith
   endif
  endfunc

  func oCURFLCODAZI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCURFLCODAZI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCURFLCODAZI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oCURFLCODAZI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZI',"",'',this.parent.oContained
  endproc
  proc oCURFLCODAZI_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AZCODAZI=this.parent.oContained.w_CURFLCODAZI
     i_obj.ecpSave()
  endproc

  add object oCURFLREVFIL_1_18 as StdField with uid="WFYIHBDGLY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CURFLREVFIL", cQueryName = "CURFLREVFIL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 106710868,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=84, Top=161, cSayPict="'999.99'", cGetPict="'999.99'"

  func oCURFLREVFIL_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (cp_set_get_right()>3 or (cp_set_get_right()==3 and .w_curflcodute==i_codute))
    endwith
   endif
  endfunc

  add object oCURFLAUTHOR_1_19 as StdField with uid="HCAUVEOICX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CURFLAUTHOR", cQueryName = "CURFLAUTHOR",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 113403459,;
   bGlobalFont=.t.,;
    Height=21, Width=401, Left=84, Top=187, InputMask=replicate('X',50)

  func oCURFLAUTHOR_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (cp_set_get_right()>3 or (cp_set_get_right()==3 and .w_curflcodute==i_codute))
    endwith
   endif
  endfunc

  add object oCURFL_NOTES_1_20 as StdMemo with uid="KGRLHGRTPM",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CURFL_NOTES", cQueryName = "CURFL_NOTES",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 113844465,;
   bGlobalFont=.t.,;
    Height=96, Width=472, Left=12, Top=236

  func oCURFL_NOTES_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (cp_set_get_right()>3 or (cp_set_get_right()==3 and .w_curflcodute==i_codute))
    endwith
   endif
  endfunc


  add object oCURFLCUSTOM_1_26 as StdCombo with uid="PIVUGUSWCA",rtseq=16,rtrep=.f.,left=385,top=58,width=101,height=21, enabled=.f.;
    , HelpContextID = 108209477;
    , cFormVar="w_CURFLCUSTOM",RowSource=""+"Userdir,"+"Custom", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCURFLCUSTOM_1_26.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oCURFLCUSTOM_1_26.GetRadio()
    this.Parent.oContained.w_CURFLCUSTOM = this.RadioValue()
    return .t.
  endfunc

  func oCURFLCUSTOM_1_26.SetRadio()
    this.Parent.oContained.w_CURFLCUSTOM=trim(this.Parent.oContained.w_CURFLCUSTOM)
    this.value = ;
      iif(this.Parent.oContained.w_CURFLCUSTOM=='U',1,;
      iif(this.Parent.oContained.w_CURFLCUSTOM=='C',2,;
      0))
  endfunc


  add object oBtn_1_28 as StdButton with uid="GPQYTBCQYS",left=379, top=336, width=48,height=45,;
    CpPicture="bmp\save.bmp", caption="", nPag=1;
    , ToolTipText = "Save";
    , HelpContextID = 224686926;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="CZAXUUBTZR",left=431, top=336, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 1740062;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="BDFSQMAFKR",Visible=.t., Left=15, Top=9,;
    Alignment=1, Width=66, Height=18,;
    Caption="File name:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="JFTONXJQIK",Visible=.t., Left=15, Top=36,;
    Alignment=1, Width=66, Height=18,;
    Caption="Description:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="TFCPMXINLC",Visible=.t., Left=37, Top=59,;
    Alignment=1, Width=44, Height=18,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="KRFJCGBOVY",Visible=.t., Left=352, Top=8,;
    Alignment=1, Width=29, Height=18,;
    Caption="Type:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="BCTYABNZMG",Visible=.t., Left=18, Top=84,;
    Alignment=1, Width=63, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ERDSCGYNCK",Visible=.t., Left=11, Top=109,;
    Alignment=1, Width=70, Height=18,;
    Caption="Group:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="BDUGCXMHVV",Visible=.t., Left=8, Top=135,;
    Alignment=1, Width=73, Height=18,;
    Caption="Company:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="WFNSNAIYSG",Visible=.t., Left=15, Top=220,;
    Alignment=0, Width=47, Height=18,;
    Caption="Note"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="MEYFSKZEWK",Visible=.t., Left=30, Top=165,;
    Alignment=1, Width=51, Height=18,;
    Caption="Revision:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="PGQUFVEEHU",Visible=.t., Left=43, Top=191,;
    Alignment=1, Width=38, Height=18,;
    Caption="Author:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="FKIDVNAVXU",Visible=.t., Left=341, Top=58,;
    Alignment=1, Width=38, Height=18,;
    Caption="Folder:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_infomon','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
