* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_showdbf_b                                                    *
*              Controllo check cp_showdbf                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-27                                                      *
* Last revis.: 2012-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_showdbf_b",oParentObject,m.pOper)
return(i_retval)

define class tcp_showdbf_b as StdBatch
  * --- Local variables
  pOper = space(1)
  w_CP_SHOWDBF = .NULL.
  w_result = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    * --- Controlla il check di commit automatico e quello di sole istruzioni di tipo select 
    *     presenti sulla CP_SHOWDBF
    * --- pOper pu� assumere i seguenti valori:
    *     'S' esegue le frasi sql indicate nella maschera
    *     'C' esegue la commit
    *     'R' esegue la rollback
    * --- --
    * --- --
    this.w_CP_SHOWDBF = THIS.oPARENTOBJECT
    * --- --
    if this.oParentObject.w_ISTRSEL="S" AND NOT UPPER( ALLTRIM (this.oParentObject.w_ALCMDSQL) ) = "SELECT"
      AH_ERRORMSG("Sono consentite solo istruzioni di tipo Select",64)
      i_retcode = 'stop'
      return
    endif
    * --- Esegue istruzione SQL
    if this.pOper="S"
      if NOT this.oParentObject.w_SOTTOTRANSAZIONE
      endif
      i_nConn = i_TableProp[this.AZIENDA_idx,3]
      if UPPER( ALLTRIM (this.oParentObject.w_ALCMDSQL) ) = "SELECT"
        this.w_result = CP_SQLEXEC(i_nConn, this.oParentObject.w_ALCMDSQL,this.w_CP_SHOWDBF.oPARENTOBJECT.w_SHOWDBF_CURS)
      else
        this.w_result = cp_TrsSQL(i_nConn, this.oParentObject.w_ALCMDSQL )
      endif
      if this.w_result=-1
        * --- Raise
        i_Error=message()
        return
      else
        this.oParentObject.w_BROWSEOBJ.BROWSE()     
        this.w_CP_SHOWDBF.SETCONTROLSVALUE()     
      endif
      if this.oParentObject.w_AUTOCOM = "S" AND this.oParentObject.w_SOTTOTRANSAZIONE
      endif
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
