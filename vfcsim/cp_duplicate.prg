* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_duplicate                                                    *
*              Duplicate                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-04-16                                                      *
* Last revis.: 2008-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_duplicate
*--- Indici colonne cp_monitor
#Define FILENAME_IDX 1
#Define FILEDESC_IDX 2
#Define FLSTATUS_IDX 3
#Define FLCUSTOM_IDX 4
#Define FILETYPE_IDX 5
#Define FLDESUTE_IDX 6
#Define FLDESGRP_IDX 7
#Define FLDESAZI_IDX 8
#Define FLORGNAM_IDX 9
#Define FLCODUTE_IDX 10
#Define FLCODGRP_IDX 11   
#Define FLCODAZI_IDX 12
#Define FLREFFIL_IDX 13
#Define FL_ERASE_IDX 14
#Define FLMODFIL_IDX 15
#Define FLNEWFIL_IDX 16
#Define FLREVFIL_IDX 17
#Define FLAUTHOR_IDX 18
#Define FL_NOTES_IDX 19
* --- Fine Area Manuale
return(createobject("tcp_duplicate",oParentObject))

* --- Class definition
define class tcp_duplicate as StdForm
  Top    = 15
  Left   = 18

  * --- Standard Properties
  Width  = 416
  Height = 115
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-10"
  HelpContextID=58880214
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "cp_duplicate"
  cComment = "Duplicate"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FLDESUTE_D = space(20)
  w_FLDESGRP_D = space(20)
  w_FLCODUTE_D = 0
  w_FLCODGRP_D = 0
  w_FLDESAZI_D = space(40)
  w_FLCODAZI_D = space(10)
  * --- Area Manuale = Declare Variables
  * --- cp_duplicate
  Autocenter=.t.
  
  *  *--- Bottone duplicate
    PROCEDURE Duplicate()
       *--- Recupero le chiavi dei record selezionati (da duplicare)
       LOCAL l_table_ , rowcounts , l_rowcounts_itr, new_usr, new_grp, new_azi, parentfile, new_FLORGNAME
       LOCAL repeated_records, l_OldArea
       l_OldArea = Select()
       l_table_ = This.oparentobject.w_zoom.cCursor
       SELECT * FROM (l_table_) WHERE xchk=1 INTO CURSOR _cursore_
       l_rowcounts=IIF(USED('_cursore_'),RECCOUNT(),0)
       if l_rowcounts==0
          cp_errormsg("Nessun file selezionato",'x')
          USE IN SELECT('_cursore_')
          return
       endif 
       IF l_rowcounts>0 && se ho dei files da duplicare
          * Recupero le variavili dalla maschera
          new_codute = This.w_FLCODUTE_D
          new_codgrp = This.w_FLCODGRP_D
          new_codazi = This.w_FLCODAZI_D
          new_desute = This.w_FLDESUTE_D
          new_desgrp = This.w_FLDESGRP_D
          new_desazi = This.w_FLDESAZI_D
          LOCAL lenghtab , files_rightclick, files_deskmenu
          lenghtab=FCOUNT('cp_monitor')
          files_rightclick = ""
          files_deskmenu = ""
          SELECT('_cursore_')
          GO TOP
          SCAN && per ogni record
             * L'ultimo parametro relativo al flag di custom � cablato ad "U" perch� tutti i files duplicati
             * sono messi in automatico dentro la cartella userdir
             new_FLORGNAME = cp_monitor_buildname(_cursore_.FLORGNAM,new_codute,new_codgrp,new_codazi,_cursore_.FLSTATUS,"U")
             * Adesso dovrei avere il path completo del nuovo file
             * Controllo che non sia gi� presente
             repeated_records = 0
             SELECT(l_table_)
             COUNT FOR UPPER(FLORGNAM) = UPPER(new_FLORGNAME) TO repeated_records
             SELECT('_cursore_')
             IF repeated_records = 0 && se non c'� un record con lo stesso FLORGNAM
                If _cursore_.FILETYPE='VQR' And (new_codute<>0 Or new_codgrp<>0)
                  cp_errormsg("Non � possibile duplicare una Visual query per utente o gruppo")
                Else
                  DIMENSION arFields(lenghtab)
                  arFields=''
                  arFields[FILENAME_IDX] = ALLTRIM(_cursore_.filename)
                  arFields[FILEDESC_IDX] = _cursore_.FILEDESC
                  arFields[FLSTATUS_IDX] = _cursore_.FLSTATUS
                  arFields[FILETYPE_IDX] = _cursore_.FILETYPE
                  arFields[FLDESUTE_IDX] = IIF(new_codute!=0,new_desute,"")
                  arFields[FLDESGRP_IDX] = IIF(new_codgrp!=0,new_desgrp,"")
                  arFields[FLORGNAM_IDX] = new_FLORGNAME
                  arFields[FLCODUTE_IDX] = new_codute
                  arFields[FLCODGRP_IDX] = new_codgrp
                * Rightclick/Deskmenu: se il nuovo percorso contiene la cartella rightclick/Deskmenu
                * Il codice azienda viene ignorato, questo perche altrimenti il nuovo record creato avrebbe 
                * un'inconsistenza sul codice aziendale rispetto al percorso del file
                Do Case
                    Case AT(UPPER("\rightclick\"),UPPER(ALLTRIM(new_FLORGNAME)))!=0
                     arFields[FLCODAZI_IDX] = ""
                     arFields[FLDESAZI_IDX] = ""
                     files_rightclick = LOWER(ALLTRIM(files_rightclick) + ALLTRIM(arFields[FILENAME_IDX]) + CHR(13))
                    Case AT(UPPER("\deskmenu\"),UPPER(ALLTRIM(new_FLORGNAME)))!=0
                     arFields[FLCODAZI_IDX] = ""
                     arFields[FLDESAZI_IDX] = ""
                     files_deskmenu = LOWER(ALLTRIM(files_deskmenu) + ALLTRIM(arFields[FILENAME_IDX]) + CHR(13))		   
                    Otherwise 
                     arFields[FLCODAZI_IDX] = ALLTRIM(new_codazi)
                     arFields[FLDESAZI_IDX] = IIF(!EMPTY(ALLTRIM(new_codazi)),new_desazi,"")
                  EndCase 
                  arFields[FLREFFIL_IDX] = _cursore_.FLREFFIL
                  arFields[FL_ERASE_IDX] = "N"
                  arFields[FLMODFIL_IDX] = "D"
                  arFields[FLCUSTOM_IDX] = "U"
                  arFields[FLNEWFIL_IDX] = _cursore_.FLNEWFIL
                  arFields[FLREVFIL_IDX] = _cursore_.FLREVFIL
                  arFields[FLAUTHOR_IDX] = _cursore_.FLAUTHOR
                  arFields[FL_NOTES_IDX] = _cursore_.FL_NOTES
                  INSERT INTO 'cp_monitor' FROM ARRAY arFields
                  This.oparentobject.NotifyEvent("RefreshZoom")
                Endif
             ELSE
                IF AT(UPPER("\rightclick\"),UPPER(ALLTRIM(new_FLORGNAME)))!=0 Or AT(UPPER("\deskmenu\"),UPPER(ALLTRIM(new_FLORGNAME)))!=0
                   cp_errormsg("File "+ALLTRIM(JUSTFNAME(_cursore_.FLORGNAM))+CHR(13)+"non duplicabile per azienda.","i")
                ELSE 
                   cp_errormsg("File "+ALLTRIM(JUSTFNAME(_cursore_.FLORGNAM))+" non duplicabile."+CHR(13)+"File destinazione "+ALLTRIM(JUSTFNAME(new_FLORGNAME))+" gi� esistente.")
                ENDIF 
             ENDIF
             USE IN SELECT('_cursore2_')
             SELECT('_cursore_')
          ENDSCAN
          IF NOT ( EMPTY(new_codazi) OR EMPTY(files_rightclick) )
             cp_errormsg("I file:"+CHR(13)+files_rightclick+"sono duplicati dentro rightclick.","i")
          ENDIF 
          IF NOT ( EMPTY(new_codazi) OR EMPTY(files_deskmenu) )
             cp_errormsg("I file:"+CHR(13)+files_deskmenu+"sono duplicati dentro deskmenu.","i")
          ENDIF 	
       ENDIF
       USE IN SELECT('_cursore_')
       *--- Restore Area
       Select(l_OldArea)
    ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_duplicatePag1","cp_duplicate",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLCODUTE_D_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CPGROUPS'
    this.cWorkTables[3]='AZIENDA'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      This.Duplicate
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLDESUTE_D=space(20)
      .w_FLDESGRP_D=space(20)
      .w_FLCODUTE_D=0
      .w_FLCODGRP_D=0
      .w_FLDESAZI_D=space(40)
      .w_FLCODAZI_D=space(10)
          .DoRTCalc(1,2,.f.)
        .w_FLCODUTE_D = iif(cp_set_get_right()>3,0,i_codute)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_FLCODUTE_D))
          .link_1_5('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_FLCODGRP_D))
          .link_1_6('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_FLCODAZI_D))
          .link_1_8('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLCODUTE_D_1_5.enabled = this.oPgFrm.Page1.oPag.oFLCODUTE_D_1_5.mCond()
    this.oPgFrm.Page1.oPag.oFLCODGRP_D_1_6.enabled = this.oPgFrm.Page1.oPag.oFLCODGRP_D_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FLCODUTE_D
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FLCODUTE_D) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_FLCODUTE_D);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_FLCODUTE_D)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_FLCODUTE_D) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oFLCODUTE_D_1_5'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FLCODUTE_D)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_FLCODUTE_D);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_FLCODUTE_D)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FLCODUTE_D = NVL(_Link_.CODE,0)
      this.w_FLDESUTE_D = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_FLCODUTE_D = 0
      endif
      this.w_FLDESUTE_D = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FLCODUTE_D Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FLCODGRP_D
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FLCODGRP_D) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_FLCODGRP_D);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_FLCODGRP_D)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_FLCODGRP_D) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oFLCODGRP_D_1_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FLCODGRP_D)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_FLCODGRP_D);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_FLCODGRP_D)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FLCODGRP_D = NVL(_Link_.CODE,0)
      this.w_FLDESGRP_D = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_FLCODGRP_D = 0
      endif
      this.w_FLDESGRP_D = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FLCODGRP_D Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FLCODAZI_D
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FLCODAZI_D) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_FLCODAZI_D)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_FLCODAZI_D))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FLCODAZI_D)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FLCODAZI_D) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oFLCODAZI_D_1_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FLCODAZI_D)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_FLCODAZI_D);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_FLCODAZI_D)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FLCODAZI_D = NVL(_Link_.AZCODAZI,space(10))
      this.w_FLDESAZI_D = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FLCODAZI_D = space(10)
      endif
      this.w_FLDESAZI_D = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FLCODAZI_D Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLDESUTE_D_1_1.value==this.w_FLDESUTE_D)
      this.oPgFrm.Page1.oPag.oFLDESUTE_D_1_1.value=this.w_FLDESUTE_D
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESGRP_D_1_2.value==this.w_FLDESGRP_D)
      this.oPgFrm.Page1.oPag.oFLDESGRP_D_1_2.value=this.w_FLDESGRP_D
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCODUTE_D_1_5.value==this.w_FLCODUTE_D)
      this.oPgFrm.Page1.oPag.oFLCODUTE_D_1_5.value=this.w_FLCODUTE_D
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCODGRP_D_1_6.value==this.w_FLCODGRP_D)
      this.oPgFrm.Page1.oPag.oFLCODGRP_D_1_6.value=this.w_FLCODGRP_D
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESAZI_D_1_7.value==this.w_FLDESAZI_D)
      this.oPgFrm.Page1.oPag.oFLDESAZI_D_1_7.value=this.w_FLDESAZI_D
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCODAZI_D_1_8.value==this.w_FLCODAZI_D)
      this.oPgFrm.Page1.oPag.oFLCODAZI_D_1_8.value=this.w_FLCODAZI_D
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_duplicatePag1 as StdContainer
  Width  = 412
  height = 115
  stdWidth  = 412
  stdheight = 115
  resizeXpos=189
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLDESUTE_D_1_1 as StdField with uid="JVAZTKZPOK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FLDESUTE_D", cQueryName = "FLDESUTE_D",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 25425077,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=159, Top=7, InputMask=replicate('X',20)

  add object oFLDESGRP_D_1_2 as StdField with uid="OTRKEFSLDU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FLDESGRP_D", cQueryName = "FLDESGRP_D",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 24715957,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=159, Top=33, InputMask=replicate('X',20)

  add object oFLCODUTE_D_1_5 as StdField with uid="IIJYJPNYOV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FLCODUTE_D", cQueryName = "FLCODUTE_D",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "User Code",;
    HelpContextID = 25425307,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=67, Top=7, cSayPict='"99"', cGetPict='"99"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_FLCODUTE_D"

  func oFLCODUTE_D_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCODGRP_D==0 and cp_set_get_right()>3)
    endwith
   endif
  endfunc

  func oFLCODUTE_D_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oFLCODUTE_D_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFLCODUTE_D_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oFLCODUTE_D_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oFLCODGRP_D_1_6 as StdField with uid="UHNOZGVPQV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FLCODGRP_D", cQueryName = "FLCODGRP_D",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Group Code",;
    HelpContextID = 24716187,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=67, Top=35, cSayPict='"99"', cGetPict='"99"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_FLCODGRP_D"

  func oFLCODGRP_D_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCODUTE_D==0)
    endwith
   endif
  endfunc

  func oFLCODGRP_D_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oFLCODGRP_D_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFLCODGRP_D_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oFLCODGRP_D_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oFLDESAZI_D_1_7 as StdField with uid="MFOORKQOHG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FLDESAZI_D", cQueryName = "FLDESAZI_D",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 25143477,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=159, Top=59, InputMask=replicate('X',40)

  add object oFLCODAZI_D_1_8 as StdField with uid="UCWKKGIWDN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FLCODAZI_D", cQueryName = "FLCODAZI_D",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Company Code",;
    HelpContextID = 25143707,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=67, Top=59, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_FLCODAZI_D"

  func oFLCODAZI_D_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oFLCODAZI_D_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFLCODAZI_D_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oFLCODAZI_D_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oBtn_1_10 as StdButton with uid="KOJJKMPGVS",left=278, top=90, width=68,height=18,;
    caption="Duplicate", nPag=1;
    , ToolTipText = "Execute Duplication";
    , HelpContextID = 202642734;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="LRBLDYKQMH",left=353, top=90, width=50,height=18,;
    caption="Cancel", nPag=1;
    , ToolTipText = "Close Window";
    , HelpContextID = 152167305;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="OPBNQBQRKF",Visible=.t., Left=20, Top=36,;
    Alignment=1, Width=44, Height=18,;
    Caption="Group:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="USNSTLJBVM",Visible=.t., Left=25, Top=10,;
    Alignment=1, Width=39, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="IFMMUVRUDC",Visible=.t., Left=8, Top=61,;
    Alignment=1, Width=56, Height=18,;
    Caption="Company:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_duplicate','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
