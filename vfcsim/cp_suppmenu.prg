* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_suppmenu                                                     *
*              Gestione eventi treeview menu                                   *
*                                                                              *
*      Author: GiorGio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language: Visual Fox Pro                                                  *
*          OS: Windows                                                         *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-18                                                      *
* Last revis.: 2009-08-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Event,cText,tipo,bBtnClick,cPosizione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_suppmenu",oParentObject,m.Event,m.cText,m.tipo,m.bBtnClick,m.cPosizione)
return(i_retval)

define class tcp_suppmenu as StdBatch
  * --- Local variables
  Event = space(10)
  cText = space(150)
  tipo = 0
  bBtnClick = .f.
  cPosizione = space(1)
  indexTemp = 0
  w_SYSKEY = space(254)
  w_sysIndex = 0
  oldLevel = 0
  w_SEQUENZA = space(5)
  padreNodo = .NULL.
  oldInsert = space(254)
  w_OINDEX = 0
  w_BMPINDEX = 0
  SysType = 0
  w_Obj_Pos = .NULL.
  bitCanc = space(254)
  w_PADRE = .NULL.
  w_CMENUFILE = space(254)
  w_TempCursor = space(10)
  w_OBJ = .NULL.
  w_TOOLOBJ = .NULL.
  w_MUTENTE = space(25)
  w_FILENAME = space(25)
  w_TREESX = space(10)
  w_CURSAPP = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestisce gli eventi che avvengono sulle due Treeview dei menu
    *     Tutte le variabili che hanno suffisso SYS si riferiscono alla Treeview del menu di sistema.
    *     Tutte le variabili che hanno suffisso USR si riferiscono alla TreeView del menu utente.
    * --- La prima pagina condiziona gli eventi e rimanda alle relative pagine di gestione
    * --- Variabile per determinare se il batch � stato lanciato premendo uno dei bottoncini del menu du destra
    * --- Variabile per determinare se la voce deve essere inserita Prima (P), Dopo (D) o occorre
    *     inserire il flag Cancellato (C)
    do case
      case this.Event=="SYSCLICK"
        * --- Memorizzo l'indice del nodo selezionato nella TreeView di destra
        this.w_sysIndex = this.oParentObject.w_SYSTreeView.oTree.selectedItem.index
        * --- Controllo che il nodo selezionato a destra, se � un separatore, non venga 
        *     direttamente attaccato al "Menu Principale"
        this.w_SYSKEY = this.oParentObject.w_SYSTreeView.oTree.nodes(this.w_sysIndex).key
        select (this.oParentObject.w_SYSCURSOR) 
 seek alltrim(substr(this.w_SYSKEY,2))
        if found() and DIRECTORY=4 and this.oParentObject.w_directory=0
           return
        endif
        * --- Metto in un cursore d'appoggio le voci che dovr� copiare a sinistra
         
 #if version(5)>=700 
 Select * From ( this.oParentObject.w_SYSTreeView.cCursor ) Where 1=0 Into Cursor Appo noFilter READWRITE 
 #else 
 local w_nome 
 w_NOME = sys(2015) 
 Select * From ( this.oParentObject.w_SYSTreeView.cCursor ) Into Cursor (w_nome) Where 1=0 NOFILTER 
 use dbf() again in 0 alias "Appo" 
 USE 
 SELECT ( "Appo" ) 
 #endif
        this.oParentObject.w_SYSTreeView.oTree.nodes(this.w_sysIndex).expanded = .F.
        this.oParentObject.w_SYSTreeView.contatore = 1
        this.oParentObject.w_SYSTreeView.rifpadre = -1
        this.oParentObject.w_SYSTreeView.visita(this.oParentObject.w_SYSTreeView.oTree.nodes(this.w_sysIndex),"appo",0)     
        * --- Mantengo l'indice originale del men� di sinistra per espanderlo al termine
        this.w_OINDEX = this.oParentObject.w_INDEX
        select ("appo") 
 go top 
 scan
        * --- Doppio click sul menu principale, skippo la prima voce... 
        if Directory=0
          Skip 1
        endif
        this.w_BMPINDEX = this.oParentObject.w_USRTreeView.GetBitmapIndex(Appo.CPBmpName)
        if recno("Appo")=1
          if this.oParentObject.w_DIRECTORY=2 or this.oParentObject.w_DIRECTORY=0
            * --- Inseriamo la nuova voce all'interno del submenu sul quale siamo posizionati
            *     (il valore 4 indica "figlio di " w_INDEX)
            this.oParentObject.w_USRTreeView.oTree.nodes.add(this.oParentObject.w_INDEX,4,,alltrim(appo.VOCEMENU),this.w_BMPINDEX)     
          else
            * --- Inseriamo la nuova voce subito dopo il menu sul quale siamo posizionati
            *     (il valore 2 indica "successivo a " w_INDEX)
            this.oParentObject.w_USRTreeView.oTree.nodes.add(this.oParentObject.w_INDEX,2,,alltrim(appo.VOCEMENU),this.w_BMPINDEX)     
          endif
        else
          if LEVEL=this.oldlevel
            * --- Se il livello del nodo che devo inserire � uguale al nodo precedente, inserisco il nodo subito dopo
            this.oParentObject.w_USRTreeView.oTree.nodes.add(this.oParentObject.w_INDEX,2,,alltrim(appo.VOCEMENU),this.w_BMPINDEX)     
          else
            if LEVEL>this.oldlevel
              * --- Se il livello del nodo che devo inserire � maggiore del nodo precedente, inserisco il nodo all'interno dello stesso
              this.oParentObject.w_USRTreeView.oTree.nodes.add(this.oParentObject.w_INDEX,4,,alltrim(appo.VOCEMENU),this.w_BMPINDEX)     
            else
              * --- Calcolo l'index del nodo all'interno del quale devo inserire la voce
              macro="this.oParentObject.w_USRTreeView.oTree.nodes(this.oParentObject.w_INDEX).parent"
              this.padreNodo = &macro
              k=this.oldLevel-LEVEL
              do while k>0
                this.padreNodo = this.padreNodo.parent
                k=k-1
              enddo
              this.oParentObject.w_INDEX = this.PadreNodo.Index
              this.oParentObject.w_USRTreeView.oTree.nodes.add(this.oParentObject.w_INDEX,4,,alltrim(appo.VOCEMENU),this.w_BMPINDEX)     
            endif
          endif
        endif
        * --- tengo aggiornato oldLevel e la chiave
        this.oldLevel = LEVEL
        * --- Portiamo l'indice sul nodo appena creato
        this.oParentObject.w_INDEX = this.oParentObject.w_USRTreeView.oTree.nodes.count()
        * --- Salviamo il Tag della TreeView
        this.oParentObject.w_USRTreeView.SetTag(this.oParentObject.w_USRTreeView.oTree.nodes( this.oParentObject.w_INDEX ) , Appo.NAMEPROC , Appo.DIRECTORY , Appo.ENABLED , Appo.CPBmpName , Appo.MODULO , Appo.INS_POINT , Appo.POSITION , Appo.INSERTION , Appo.DELETED, Appo.NOTE, Appo.MRU,Appo.TEAROFF)     
        endscan
        select ("appo") 
 use
        * --- Mettiamo a TRUE la variabile modified ogni volta che salviamo.
        this.oParentObject.oParentObject.bModified=.T.
        * --- Espando l'elemento passato..
        this.oParentObject.w_USRTreeView.oTree.nodes(this.w_OINDEX).Expanded = .t.
        * --- Riposizioniamo il w_INDEX
        this.oParentObject.w_INDEX = this.w_OINDEX
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Event=="USRDEL"
        * --- Il "Menu Principale" non si pu� eliminare...
        if this.oParentObject.w_INDEX=1
          i_retcode = 'stop'
          return
        endif
        * --- Cancellazione dell'elemento
        this.oParentObject.w_USRTreeView.delElement(this.oParentObject.w_INDEX)     
        this.oParentObject.w_INDEX = this.oParentObject.w_USRTreeView.oTree.selectedItem.Index
        * --- Mettiamo a TRUE la variabile modificata ogni volta che salviamo.
        this.oParentObject.oParentObject.bModified=.T.
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Event=="ADD"
        if this.bBtnClick
          * --- Quando bRigthClick �.T. obbiamo inizializzare w_index  per posizionarci sul primo nodo.
          *     Inoltre dobbiamo mettere w_directory a 0 che � il valore necessario affinch� i nodi aggiunti
          *     vengano correttamente inseriti in prima posizione
          this.oParentObject.w_INDEX = 1
          this.oParentObject.w_DIRECTORY = 0
        endif
        if this.oParentObject.w_DIRECTORY = 2 or this.oParentObject.w_DIRECTORY = 0
          * --- Inseriamo la nuova voce all'interno del submenu sul quale siamo posizionati
          *     (il valore 4 indica "figlio di " w_INDEX)
          this.oParentObject.w_USRTreeView.oTree.nodes.add(this.oParentObject.w_INDEX,4,,this.cText,this.tipo)     
        else
          * --- Inseriamo la nuova voce subito dopo il menu sul quale siamo posizionati
          *     (il valore 2 indica "successivo a " w_INDEX)
          this.oParentObject.w_USRTreeView.oTree.nodes.add(this.oParentObject.w_INDEX,2,,this.cText,this.tipo)     
        endif
        * --- Inseriamo il fatto che sia una directory o un menu...
        this.oParentObject.w_DIRECTORY = iif(this.tipo=1,2,1)
        * --- ... o un separatore
        if this.ctext="------------"
          this.oParentObject.w_DIRECTORY = 4
        endif
        this.oParentObject.w_USRTreeView.expandElement(this.oParentObject.w_INDEX)     
        * --- Portiamo l'indice sul nodo appena creato
        this.oParentObject.w_INDEX = this.oParentObject.w_USRTreeView.oTree.nodes.count()
        * --- Condizioniamo tutto per vedere se la chiamata � arrivata da un tasto destro.
        * --- La variabile viene inizializzata in pag.2 con il valore di DIRECTORY del nodo della SysTreeView
        *     E' utilizzata per condizionare la bitmap da uitlizzare premendo il bottoncino Elimina
        if this.bBtnClick
          * --- Se la chiamata � arrivata dal tasto destro, allora dobbiamo inserire il punto d'inserzione.
          *     o mettere il flag cancellato
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Variabile per memorizzare la bitmap del menu cancellato da inserire
          this.bitCanc = iif(this.sysType=1,"MLEAFCanc","MNODECanc")+".bmp"
          * --- Settiamo anche la combobox
          if this.cPosizione="P" or this.cPosizione="D"
            this.oParentObject.w_POSITION = this.cPosizione
          endif
        endif
        * --- Salviamo il Tag della TreeView
        this.oParentObject.w_USRTreeView.oTree.nodes(this.oParentObject.w_INDEX).text = iif(this.bBtnClick and this.cPosizione="C",alltrim(iif(this.sysType=1,cp_Translate(MSG_OPTION),cp_Translate(MSG_MENU))+" - " + cp_Translate(MSG_ERASE)+" "+this.oParentObject.w_insertPoint),alltrim(this.cText))
        * --- w_USRTreeView.SetTag(oNodo,cNameProc,cDirectory,bAbilitato,cBitmap,cModulo,cInsertPoint,cPosition,cInsertion,cDeleted)
        this.oParentObject.w_USRTreeView.SetTag(this.oParentObject.w_USRTreeView.oTree.nodes( this.oParentObject.w_INDEX ) , "" , this.oParentObject.w_DIRECTORY , .T. , iif(vartype(this.cPosizione)="C" and this.cPosizione="C",this.bitCanc,"") , "" , iif(this.bBtnClick,this.oParentObject.w_InsertPoint,"") ,iif(this.bBtnClick, this.oParentObject.w_POSITION,"") , "" , iif(vartype(this.cPosizione)="C" and this.cPosizione="C","F","T"), this.oParentObject.w_NOTE, this.oParentObject.w_MRU, this.oParentObject.w_TEAROFF)     
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.bBtnClick and vartype(this.cPosizione)="C" and this.cPosizione="C"
          * --- Visualizziamo la bitmap solo se aggiungiamo un nodo coi bottoni di destra
          this.oParentObject.w_USRTreeView.oTree.nodes(this.oParentObject.w_INDEX).image = this.oParentObject.w_USRTreeView.GetBitmapIndex( this.bitCanc )
        endif
        * --- Mettiamo a TRUE la variabile modificata ogni volta che salviamo.
        this.oParentObject.oParentObject.bModified=.T.
      case this.Event=="LOST"
        * --- Ogni volta che lascio il focus da una qualche variabile della maschera, aggiorna la USRTreeView.
        this.oParentObject.w_USRTreeView.oTree.nodes(this.oParentObject.w_INDEX).text = Alltrim(this.oParentObject.w_VOCEMENU)
        this.oParentObject.w_USRTreeView.SetTag(this.oParentObject.w_USRTreeView.oTree.nodes( this.oParentObject.w_INDEX ) , this.oParentObject.w_NAMEPROC , this.oParentObject.w_DIRECTORY , this.oParentObject.w_ABILITATO , this.oParentObject.w_BITMAP , this.oParentObject.w_MODULO , this.oParentObject.w_INSERTPOINT , this.oParentObject.w_POSITION , "" , iif(this.oParentObject.w_DELETED,"F", "T"), this.oParentObject.w_NOTE, this.oParentObject.w_MRU, this.oParentObject.w_TEAROFF)     
        * --- Aggiorniamo le voci del nodo
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Mettiamo a TRUE la variabile modificata ogni volta che salviamo.
        this.oParentObject.oParentObject.bModified=.T.
        * --- Se cambio la Bitmap aggiorno l'immagine del nodo...
        if (this.oparentobject.CurrentEvent="w_BITMAP LostFocus" or this.oparentobject.CurrentEvent="w_DELETED LostFocus") 
          if this.oParentObject.w_DELETED
            this.oParentObject.w_USRTreeView.oTree.nodes(this.oParentObject.w_INDEX).image = this.oParentObject.w_USRTreeView.GetBitmapIndex( iif(this.oParentObject.w_DIRECTORY=1,"MLEAFCanc","MNODECanc")+".bmp")
          else
            * --- Gestione Bitmap vuota
            if empty(this.oParentObject.w_BITMAP)
              this.oParentObject.w_USRTreeView.oTree.nodes(this.oParentObject.w_INDEX).image = this.oParentObject.w_USRTreeView.GetBitmapIndex( iif(this.oParentObject.w_DIRECTORY=1,g_MLEAFBMP,g_MNODEBMP))
            else
              this.oParentObject.w_USRTreeView.oTree.nodes(this.oParentObject.w_INDEX).image = this.oParentObject.w_USRTreeView.GetBitmapIndex( this.oParentObject.w_BITMAP )
            endif
          endif
        endif
      case this.Event=="INS"
        * --- Inseriamo il punto d'inserzione alla voce selezionata
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Imposto a Prima la posizione se vuota..
        this.oParentObject.w_POSITION = iif(empty( this.oParentObject.w_POSITION ) , "P" , this.oParentObject.w_POSITION )
        * --- Salva il punto d'inserzione nella USRTreeView relativamente al nodo della USRTreeView 
        *     precedentemente selezionato
        this.oParentObject.w_USRTreeView.SetTag(this.oParentObject.w_USRTreeView.oTree.nodes( this.oParentObject.w_INDEX ) , this.oParentObject.w_NAMEPROC , this.oParentObject.w_DIRECTORY , this.oParentObject.w_ABILITATO , this.oParentObject.w_BITMAP , this.oParentObject.w_MODULO , this.oParentObject.w_INSERTPOINT , this.oParentObject.w_POSITION , "" , iif(this.oParentObject.w_DELETED,"F", "T"), this.oParentObject.w_NOTE, this.oParentObject.w_MRU, this.oParentObject.w_TEAROFF)     
        * --- Mettiamo a TRUE la variabile modificata ogni volta che salviamo.
        this.oParentObject.oParentObject.bModified=.T.
      case this.Event=="USRSEL"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Event=="INITMENU"
        * --- Popola all'avvio entrambe le tree view del disegnatore di men�...
        this.w_PADRE = This.oParentobject
        if empty (this.oParentObject.w_MenuName)
          * --- Se la Cp_FindDefault non trova niente significa che mi st� basando sul Plan:vmn
          if Empty( cp_FindDefault("vmn") )
            this.oParentObject.w_MENUNAME = Upper( i_CpDic )
            this.w_CMENUFILE = Upper(i_CpDic)+".vmn"
          else
            this.oParentObject.w_MENUNAME = "DEFAULT"
            this.w_CMENUFILE = "DEFAULT.vmn"
          endif
        else
          this.w_CMENUFILE = this.oParentObject.w_MENUNAME+".vmn"
          this.oParentObject.w_MENUNAME = UPPER(ALLTRIM(SUBSTR(this.oParentObject.w_MENUNAME,rAT("\",this.oParentObject.w_MENUNAME)+1)))
        endif
        if this.w_PADRE.currentEvent="Avvio"
          * --- controlliamo se il cursore � gi� stato creato
          if used (this.oParentObject.w_SYSCURSOR)
             
 Select (this.oParentObject.w_SYSCURSOR) 
 Use
          endif
          if Not Empty(i_cMenuLoaded)
            * --- Per il caricamento della TreeView di destra del Disegnatore di Menu, non viene pi�
            *     utilizzato il cursore i_CUR_MENU, ma viene utilizzato un nuovo cursore nel quale
            this.w_TempCursor = Sys(2015)
             
 DO Vmn_To_Cur IN vu_exec WITH i_cMenuLoaded+".VMN", this.w_TempCursor 
 DO merge_all IN vu_exec WITH this.w_TempCursor , i_cmodules, .T. 
 
            this.oParentObject.w_SYSCURSOR = this.w_TempCursor
          else
            this.oParentObject.w_SYSCURSOR = Sys(2015)
             
 DO cp_Create_Cur_Menu IN VU_EXEC WITH this.oParentObject.w_SYSCURSOR 
 Select ( this.oParentObject.w_SYSCURSOR ) 
 INDEX ON LVLKEY TAG LVLKEY 
 
 New_Root( this.oParentObject.w_SYSCURSOR )
          endif
          * --- Inizializzo anche il cursore del menu utente...
          if empty(this.oParentObject.w_USRCURSOR)
            this.w_PADRE.oParentobject.NewDoc()     
          endif
        else
          if used ( this.oParentObject.w_SYSCURSOR )
             
 Select ( this.oParentObject.w_SYSCURSOR ) 
 Use
          endif
          this.oParentObject.w_SYSCURSOR = Sys(2015)
          * --- Carico il men� selezionato...
          Do Vmn_To_Cur in vu_exec with this.w_CMENUFILE , this.oParentObject.w_SYSCURSOR 
        endif
        * --- Popolo la Tree view con il contenuto del cursore...
        this.oParentObject.w_SYSTREEVIEW.cCursor = this.oParentObject.w_SYSCURSOR
        this.w_PADRE.NotifyEvent("SysMenu")     
        if type("this.oParentObject.w_SysTreeView.oTree.nodes(1)")="O"
          this.oParentObject.w_SYSTreeView.oTree.nodes(1).expanded = .T.
        endif
      case this.Event=="TRY"
        * --- Gestione Prova Men� da Disegnatore di Men�.
        this.w_TOOLOBJ = This.oparentobject.oparentobject
        this.w_PADRE = This.oParentobject
        if Not this.w_PADRE.bTestMenu
          this.oParentObject.w_OLD_MENU = SYS(5)+SYS(2003)+"\"+SYS(2015)+".VMN"
          * --- Salvo Nome Menu Aperto
          this.w_MUTENTE = this.oParentObject.w_MENUUTENTE
          this.w_FILENAME = this.w_TOOLOBJ.cFileName
          this.w_TOOLOBJ.cfilename = this.oParentObject.w_OLD_MENU
           
 CURSORE=SYS(2015) 
 CUR_CURSOR=cp_GetGlobalVar("i_CUR_MENU") 
 SELECT * FROM (CUR_CURSOR) INTO CURSOR &cursore 
 Select (cursore) 
 Index On LVLKEY Tag LVLKEY
          * --- Salvo Menu di Prova per aggiornare per poter creare DBF con le modifiche
          this.w_TOOLOBJ.savedoc()     
          this.w_TOOLOBJ.cfilename = this.w_FILENAME
          * --- Ripristino Nome Menu Aperto
          this.oParentObject.w_MENUUTENTE = this.w_MUTENTE
          this.w_MUTENTE = Alltrim(cp_findDefault("VMN"))
          if Empty( this.w_MUTENTE )
            * --- Se non ho definito il file Default cerco il men� con il nome dell'analisi..
            this.w_MUTENTE = cp_FindNamedDefaultFile(i_CpDic,"vmn")
          endif
          * --- Creo il Men� di Default senza men� Custom in w_CUR_MENU 
          *     Mergio il DBF che contiene il men� da provare con il men� di default appena caricato
          this.oParentObject.w_CUR_MENU = SYS(2015)
          this.w_TREESX = SYS(2015)
          * --- La domanda se caricare il men� di defaultl a faccio solo se il men� di base
          *     � un visual men�, se men� vecchio stile non posso svoglere il merge
          if not Empty( this.w_MUTENTE ) And Cp_YesNo(MSG_LOADING_DEFAULT_MENU_QP)
             
 Do Vmn_To_Cur in vu_exec with this.oParentObject.w_old_menu , this.w_TREESX 
 
 Do Vmn_To_Cur in vu_exec with this.w_MUTENTE+".VMN" , this.oParentObject.w_CUR_MENU 
 Do Merge_all IN vu_exec With this.oParentObject.w_CUR_MENU,i_cModules,.t. 
 Do Merge IN vu_exec With this.oParentObject.w_CUR_MENU,this.w_TREESX 
 
          else
            Do Vmn_To_Cur in vu_exec with this.oParentObject.w_old_menu , this.oParentObject.w_CUR_MENU
          endif
          if used ( this.w_TREESX )
             
 Select ( this.w_TREESX ) 
 Use
          endif
          if used ( CUR_CURSOR )
             
 Select ( CUR_CURSOR ) 
 Use
          endif
          * --- Swapp tra i_CUR_MENU e w_CUR_MENU
          this.w_CURSAPP = cp_GetGlobalVar("i_CUR_MENU")
          * --- Valorizzo i_CUR_MENU per visualizzare il men� anche con il CTRL-T
          cp_SetGlobalVar("i_CUR_MENU",this.oParentObject.w_CUR_MENU)
          this.oParentObject.w_CUR_MENU = CURSORE
          * --- Carico Menu di Prova
           
 Do CaricaMenu IN vu_exec With cp_GetGlobalVar("i_CUR_MENU"),.F.
          if cp_fileexist(this.oParentObject.w_old_menu)
            * --- Elimino Menu di Prova
            DELETE FILE (this.oParentObject.w_old_menu)
          endif
          this.w_OBJ = this.w_padre.getctrl("TJEHKLWKFJ")
          this.w_OBJ.caption = cp_translate(MSG_RECOVER)
          this.w_OBJ.ToolTipText = cp_translate(MSG_SET_RECOVER_ITEM)
          this.w_PADRE.bTestMenu = .t.
        else
          * --- Eseguo Ripristina
          * --- Swapp tra w_CUR_MENU e i_CUR_MENU
          this.w_CURSAPP = cp_GetGlobalVar("i_CUR_MENU")
          * --- Valorizzo i_CUR_MENU per visualizzare il men� anche con il CTRL-T
          cp_SetGlobalVar("i_CUR_MENU",this.oParentObject.w_CUR_MENU)
          this.oParentObject.w_CUR_MENU = this.w_CURSAPP
          * --- Elimino Cursore Menu di Prova
          if used ( this.oParentObject.w_CUR_MENU )
             
 Select ( this.oParentObject.w_CUR_MENU ) 
 Use
          endif
           
 Do CaricaMenu IN vu_exec With cp_GetGlobalVar("i_CUR_MENU"),.F.
          * --- Rinomino Caption Bottone
          this.w_OBJ = this.w_padre.getctrl("TJEHKLWKFJ")
          this.w_OBJ.caption = cp_translate(MSG_TRY)
          this.w_OBJ.ToolTipText = cp_translate(MSG_SET_TRY_ITEM)
          this.w_PADRE.bTestMenu = .f.
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce il punto d'inserzione in automatico
    this.oParentObject.w_SYSTreeView.setFocus()     
    this.w_sysIndex = this.oParentObject.w_SYSTreeView.oTree.selectedItem.index
    this.w_SYSKEY = this.oParentObject.w_SYSTreeView.oTree.nodes(this.w_sysIndex).key
    * --- Se esiste la chiave, significa che la voce fa parte del menu base e NON � una voce agganciata;
    *     in questo caso, non serve calcolare il path, ma basta recuperarlo dal cursore contenente il menu.
    select (this.oParentObject.w_SYSCURSOR) 
 seek alltrim(substr(this.w_SYSKEY,2))
    if found()
      this.oParentObject.w_INSERTPOINT = INSERTION
      this.SysType = DIRECTORY
    else
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Al cambio di nodo nel men� utente aggiorno le informazioni ad esso legate..
    LOCAL L_VOCEMENU,L_NAMEPROC,L_DIRECTORY,L_ABILITATO,L_BITMAP,L_MODULO,L_INSERTPOINT,L_POSITION,L_INSERTION,L_DELETED, L_NOTE, L_MRU, L_TEAROFF
    this.oParentObject.w_USRTreeView.GetValues(this.oParentObject.w_USRTreeView.oTree.Nodes( this.oParentObject.w_INDEX ) , @L_vocemenu,@L_nameproc,@L_directory,@L_ABILITATO,@L_bitmap,@L_modulo,@L_insertpoint,@L_position,@L_insertion,@L_deleted,@L_Note,@L_Mru,@L_Tearoff)     
    this.oParentObject.w_VOCEMENU = L_VOCEMENU
    this.oParentObject.w_NAMEPROC = L_NAMEPROC
    this.oParentObject.w_DIRECTORY = L_DIRECTORY
    this.oParentObject.w_ABILITATO = L_ABILITATO
    this.oParentObject.w_BITMAP = L_BITMAP
    this.oParentObject.w_MODULO = L_MODULO
    this.oParentObject.w_INSERTPOINT = L_INSERTPOINT
    this.oParentObject.w_POSITION = L_POSITION
    this.oParentObject.w_DELETED = L_DELETED="F"
    this.oParentObject.w_NOTE = L_NOTE
    this.oParentObject.w_MRU = L_MRU
    this.oParentObject.w_TEAROFF = L_TEAROFF
  endproc


  proc Init(oParentObject,Event,cText,tipo,bBtnClick,cPosizione)
    this.Event=Event
    this.cText=cText
    this.tipo=tipo
    this.bBtnClick=bBtnClick
    this.cPosizione=cPosizione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Event,cText,tipo,bBtnClick,cPosizione"
endproc
