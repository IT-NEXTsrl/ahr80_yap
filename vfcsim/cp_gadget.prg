* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_GADGET
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Stefano D'Amico
* Data creazione: 17/12/13
* Aggiornato il : 17/12/13
* Translated:
* #%&%#Build:  00
* ----------------------------------------------------------------------------
* Definizione gadget manager
* ----------------------------------------------------------------------------
#include "cp_app_lang.inc"

#Define RGN_OR 2
#Define GW_CHILD 5
*!*	#Define i_nMinGadgetWidth 56
*!*	#Define i_nMinGadgetHeight 56
#Define i_nMinGadgetWidth 24
#Define i_nMinGadgetHeight 24

#Define i_nOffsetGadget 8

#Define GWL_WNDPROC	-4
#Define WM_WINDOWPOSCHANGING 0x0046
#Define GW_HWNDNEXT 2

#Define RefreshTime 30

#Define HOME_PAGE "HOME"
#Define HOME_PAGETITLE "Home"

*--- L'utente deve essere maggiore di zero
If i_codute>0
    If Type("oGadgetManager")#"O" Or Isnull(oGadgetManager)
		If g_cGadgetInterface=='S'
        Public oGadgetManager
        oGadgetManager = Createobject("cp_gadgetmanager", 10, 32)
	    Else
	    	cp_ErrorMsg(cp_Translate("Interfaccia Gadget disabilitata dalla configurazione interfaccia"),48)
	    EndIf
    Else
        With oGadgetManager
            .SetView(.Visible, .bFullScreen, .bOnTop)
        Endwith
    Endif
Endif
*--- La toolbar con dock(-1) � necessaria per rendere le form always on top
*--- Le form del vfp (figlie dello _screen stanno sempre sotto le toolbar undocked), in questo modo ce n'� sempre una
Define Class GadgetBar_OnTop As Toolbar
    Width=200
    heigth=30
    Visible=.F.

    Procedure Init()
        With This
            .Dock(-1)
        Endwith
    Endproc
Enddefine

*--- Shape per il riposizionamento gadget (ombra)
Define Class cp_GadgetShape As Container
    Left=0
    Top=0
    Width=i_nMinGadgetWidth
    Height=i_nMinGadgetHeight

    Proc SetShapeFrom(GadgetID)
        With This
            Local nLeft, nTop, nRight, nBottom
            .Parent.GetRect(m.GadgetID, @nLeft, @nTop, @nRight, @nBottom)
            .Move(m.nLeft, m.nTop, m.nRight-m.nLeft, m.nBottom-m.nTop)
            .Visible=.T.
        Endwith
    Endproc
Enddefine

*--- Splitter per gestire la parte laterale (side bar)
Define Class cp_GadgetManagerSplitter As Container
    nXCoord = 0
    bMove = .F.
    BorderWidth = 0
    nAlphaColor = Rgb(150,150,150)

    Add Object oVSplitter As Container With Top=0, Left=0, Width=i_nOffsetGadget-2 ,MousePointer = 9, BackStyle=1

    Procedure Init
        With This
            .oVSplitter.Move(.Width-.oVSplitter.Width ,0, .oVSplitter.Width, .Height-2)
            .oVSplitter.Anchor = 13
        Endwith
    Endproc

    Procedure oVSplitter.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
	    If m.nButton=1 And Thisform.nActivePage=1
           	*--- Permetto l'attivazione della finestra
           	Thisform.bNoActivate = .F.
            This.Parent.bMove = .T.
            This.Parent.nXCoord = m.nXCoord
        Endif
    Endproc

    Procedure oVSplitter.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.MousePointer = Iif(Thisform.nActivePage=1,9,0)
        With This.Parent
            If m.nButton=1 And .bMove
                .Move(.Left,.Top,Max(.Width+m.nXCoord-.nXCoord,This.Width))
                Thisform.Draw()
                .nXCoord = m.nXCoord
            Endif
        Endwith
    Endproc

    Procedure oVSplitter.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If m.nButton=1 And Thisform.nActivePage=1
            This.Parent.nXCoord = 0
            This.Parent.bMove =.F.
            This.Parent.Parent.MoveSplitter()
           	*--- Blocco l'attivazione della finestra
           	Thisform.bNoActivate = .T.            
        Endif
    Endproc

    Procedure Visible_Assign(xValue)
        Local nBackColor1, nBackColor2
        With This
        	IF .Visible#m.xValue
	            If m.xValue
	                .Visible = .T.
	                nBackColor1 = RGBAlphaBlending(.Parent.BackColor, .nAlphaColor, 0.5)
	                nBackColor2 = RGBAlphaBlending(.Parent.BackColor, .nAlphaColor, 0.3)
	            Else
	                nBackColor1 = .Parent.BackColor
	                nBackColor2 = .Parent.BackColor
	            Endif
	            Local tm
	            tm = Createobject("cp_TransitionManager")
	            tm.Add(This, "Backcolor", m.nBackColor1, "C", 200, "Linear")
	            tm.Add(.oVSplitter, "Backcolor", m.nBackColor2, "C", 200, "Linear")
	            tm.Add(.oVSplitter, "BorderColor", m.nBackColor2, "C", 200, "Linear")
	            tm.Start(.T.)
	            .Visible = m.xValue
	        Endif
        Endwith
    Endproc
Enddefine

*--- Form contenitore dei gadget
Define Class cp_GadgetManager As Form
    *--- Form di sistema non modificabile da window menu
    bSysForm = .T.

    Top = 0
    Left = 0
    ScrollBars = 2
    VscrollSmallChange = 100
    hRegion = 0
    nScale=6&&3
    bFullScreen = .F.
    bOnTop = .F.
    nModalHWnd = 0 && Handle della maschera modale dietro a cui deve sempre stare l'interfaccia 
    ShowWindow=2
    Visible = .F.
    MinWidth=-1

    cEffect = "BounceEaseOut"

    *--- Dummy
    Add Object _oDummy As CommandButton With Top=-1, Left=-1, Width=1, Height=1, Style=1
    
    *--- Balloon per tooltip
    ADD Object oBalloon AS cp_balloontip
	
    *--- Necessario per mostare la scrollbar
    Add Object Cnt As Shape With BackStyle=0, Top=0, Left=0, BorderStyle=0
    *--- Elenco gadget
    Add Object aGadget As Collection
    *--- TimerMover
    Add Object oMoveTimer As cp_MoveGadgetTimer With Enabled=.F.

    *--- TimerUpdater
    bUpdateOnTimer = .F.	&&Se attivo il timer ogni 5 min verifica se occorre aggiornare i gadget
    bUpdateHiddenGadget = .F.	&&Aggiorna sempre anche i gadget non visibili
    Add Object oTimerUpdater As cp_UpdateGadgetTimer With Enabled=.F.

    *--- Modalit� SideBar
    nWidthCollapse = 0
    Add Object oCntResizer As cp_GadgetManagerSplitter With Left=0, Top=0, Width=i_nOffsetGadget-2, Height=This.Height, Visible=.F.

    *--- Mostra la nuova posizione del gadget (ombra)
    Add Object oDropShape As cp_GadgetShape With BackColor=Rgb(0,136,158), BorderColor=Rgb(26,143,163), Visible=.F.

    Add Object oHighLightGadget As Shape With Visible = .F., BorderWidth=3

    Add Object oTabPage As cp_GadgetTab With Left=0, Top=0, Height=24, Width=This.Width
    
    *--- Gadget attualmente in spostamento, deve essere escluso dallo Arrangegadget
    GadgetToExclude = ""

    *--- Margini e offset
    nMarginTop = 0
    nMarginLeft = 0
    nMinGadgetWidth = 0
    nMinGadgetHeight = 0
    nOffsetGadget = 0

    *--- Memorizza le posizoni dei gadget
    cCursorGadget = Sys(2015)
    *--- Memorizza i gadget da rimuovere o aggiungere
    cCursorModify = Sys(2015)


    *--- Step dello scroll
    nScrollPos = 0

    bStop = .F. &&Rende atomica la OnMouseMoveGadget

    *--- Handle client area
    nHwndClient = 0
    nHwndClientScroll = 0
    nOldProc = 0

    *--- CharmBar di destra, toolbar fac-simile per gestire il Gadget manager
    oCharmBar = .Null.

    oGadgetBar_OnTop =.Null.

    *--- Gadget Library
    oGadgetLibrary = .Null.

    nAlphaColor = Rgb(255,255,255)

    bEventArranged = .F.	&&Setta la propriet� bEventArranged di tutti gadget

    bOpenGadgetLibrary=.F.	&&Apre la gadget library in apertura se non ho nessuna configurazione mygadget

    nActivePage = 1	&&Pagina attiva (1=Home pagina principale, 2=...)
    
    bArrangedFast = .F. &&Indica se i gadget sono animati durante l'arranged
    
    b_Release=.T. &&La Release lancia la _Release()
    
    bNoActivate = .T. &&Non premette l'attivazione della form (problema maschere modali)
    bActivateScreen = .T.
    
    cPagesInitCollapsed = '*' &&Memorizza gli indici delle pagine per cui � stato fatto il collassamento iniziale del gadget

    *--- Elenco pagine
    Add Object aPage As Collection
    Dimension aOrderPage(1)

    Procedure Init(nMarginLeft, nMarginTop, nMinGadgetWidth, nMinGadgetHeight, nOffsetGadget)
        *--- nMinGadgetWidth: 	Larghezza minima gadget (default i_MinGadgetWidth)
        *--- nMinGadgetHeight: 	Altezza minima gadget (default i_MinGadgetHeight)
        *--- nOffsetGadget:		Distanza tra i gadget
        With This
            .Declare()
            .oGadgetBar_OnTop = Createobject("GadgetBar_OnTop")

            *--- La form nasce As-Top Level (figlia del Desktop), poi trasformata in In-Screen form
            *--- i gadget con ShowWindow = 1 vengono automaticamente inseriti come figli della form cp_GadgetManager
            *--- quando la form ha il focus!
            #Define GWL_EXSTYLE -20
            #Define GWL_STYLE -16
            nExStyle = GetWindowLong(.HWnd, GWL_EXSTYLE)
            SetWindowLong(.HWnd, GWL_EXSTYLE, Bitand(nExStyle, Bitnot(0x00040000)))
            nStyle = GetWindowLong(.HWnd, GWL_STYLE)
            SetWindowLong(.HWnd, GWL_STYLE, Bitor(nStyle, 0x40000000))

            *--- La rendo figlia dello _screen
            SetParent(.HWnd, _Screen.HWnd)

            *--- Via bordi e titolo
            .TitleBar=0
            .BorderStyle=0
            *--- Client area
            .nHwndClient = GetWindow(.HWnd, GW_CHILD)
            .nHwndClientScroll = GetWindow(.nHwndClient, GW_CHILD)
            .nOldProc = GetWindowLong(This.HWnd, GWL_WNDPROC)
            Bindevent(This.HWnd, WM_WINDOWPOSCHANGING, This, "EventHandler")

            *--- Cursore con le posizioni dei gadget
            .CreateCursor()

            *--- Imposto Margini e Bordi
            .nMinGadgetWidth = Iif(Type("nMinGadgetWidth")="N", m.nMinGadgetWidth, i_nMinGadgetWidth)
            .nMinGadgetHeight = Iif(Type("nMinGadgetHeight")="N", m.nMinGadgetHeight, i_nMinGadgetHeight)
            .nOffsetGadget = Iif(Type("nOffsetGadget")="N", m.nOffsetGadget, i_nOffsetGadget)
            .nMarginLeft = Iif(Type("nMarginLeft")="N", m.nMarginLeft, .nOffsetGadget)
            .nMarginTop = Iif(Type("nMarginTop")="N", m.nMarginTop, .nOffsetGadget*5)

            *--- Necessario per mostarre la scrollbar
            .Cnt.Move(.nMarginLeft,.nMarginTop,.Width-.nMarginLeft,.Height-.nMarginTop)

            *--- Aggiungo pagina 1 Home
            .aPage.Add(HOME_PAGETITLE, HOME_PAGE)
            *--- Aggiungere pagina anche al controllo pageframe
            .oTabPage.AddPage(HOME_PAGETITLE, HOME_PAGE)

            *--- Carico le info e gadget
            .LoadCfg()

			*--- Stile del balloon (ToolTip classico)
            .oBalloon.ctlStyle = 2
            .oBalloon.ctlHideDelay = 5000
            
            *--- Attivo il resize
            Bindevent(_Screen, "Resize", This, "OnResize")
            Bindevent(_Screen, "Moved", This, "OnMoved")
			This.OnResize()
        Endwith
    Endproc

    Procedure Activate()
        This.OnDeActivate()

    Procedure Deactivate()
        This.OnDeActivate()

    Procedure OnDeActivate
        *--- Attiva la titlebar di VFP
        SendMessage(_vfp.HWnd, 0x0086, 1,0)
        If Vartype(oGadgetManager)='O' And This.bNoActivate AND This.bActivateScreen
	        This.bActivateScreen = .F.
	        cp_CallAfter("Iif(Vartype(oGadgetManager)='O', oGadgetManager.ActivateScreen(), 1=1)")
	    EndIf

	Procedure ActivateScreen()
		IF _screen.WindowState # 1 AND This.bNoActivate 
	        SendMessage(_vfp.HWnd, 0x00A1, 2, 0)
    	    SendMessage(_vfp.HWnd, 0x00A2, 2, 0)
    	    This.bActivateScreen = .T.
    	EndIf
	EndProc
	
	Procedure OnMoved()
		*--- Messaggi WM_SIZE per riattivare il click sulle form
		SendMessage(This.oCharmBar.HWnd, 0x005,0,0)
		SendMessage(This.HWnd, 0x005,0,0)
	EndProc

    Procedure OnResize()
        With This
            If !Isnull(.oCharmBar)
				.oCharmBar.Move(_Screen.Width - .oCharmBar.Width + 5, 0)
                .oCharmBar.Visible=.T.
                .SetView(!.Visible, .bFullScreen, .bOnTop, .T.)
                .oTabPage.Width=_Screen.Width
		        *--- Scroll
		        local lblUp, lblDown
		        lblUp = .oCharmBar.GetCtrl("<<")
		        lblDown = .oCharmBar.GetCtrl(">>")
		        lblUp.Top = _screen.Height - 2*m.lblUp.Height
		        lblDown.Top = _screen.Height - m.lblDown.Height
		        lblUp=.null.      
		        lblDown=.null.                  
            Endif
        Endwith
    Endproc

    Procedure CreateCursor()
        Create Cursor (This.cCursorGadget) (GadgetID C(50), X1 I(4), Y1 I(4), X2 I(4), Y2 I(4), bSkip L, nPage I(2))
        Create Cursor (This.cCursorModify) (cKeyGadget C(10), cCompany C(5), cGroup C(5), cAction C(1))
    Endproc

    *--- Simulo un padre CPR
    Proc mCalc(cVal)
        Return

    Proc mCalledBatchEnd(i_bUpb)
        Return

        *--- Caricamento info e gadget
    Procedure LoadCfg()
        With This
            *--- Carico i gadget dell'utente
            Local l_Macro
            Dimension .aOrderPage(1)
            l_Macro = [GSUT_BGG(This, "Load")]
            &l_Macro
            *--- Ordino le pagine in base a quanto letto dalla configurazione
            .oTabPage.Ordering(.T.)
            .ModifyMode(.F., .T.)
            .oCntResizer.BackColor = .BackColor
            .oCntResizer.oVSplitter.BackColor = .BackColor
            .oCntResizer.oVSplitter.BorderColor = .BackColor
            If .bOpenGadgetLibrary
                .bOpenGadgetLibrary=.F.
                .oCharmBar.OnStore()
            ENDIF
        Endwith
    Endproc

    Procedure BackColor_Assign(nValue)
        This.BackColor = m.nValue
        This.oTabPage.ChangeTheme()
        nHoverColor = RGBAlphaBlending(m.nValue, GetBestForeColor(m.nValue), 0.3)
        This.oHighLightGadget.BorderColor = m.nHoverColor
        This.oHighLightGadget.BackColor = m.nHoverColor
        This.oDropShape.BackColor = RGBAlphaBlending(m.nValue, GetBestForeColor(m.nValue), 0.4)
        This.oDropShape.BorderColor = This.oDropShape.BackColor
    Endproc

    Procedure nWidthCollapse_Assign(nValue)
        This.nWidthCollapse = m.nValue
        This.oCntResizer.Width = m.nValue
    Endproc

    Procedure SetActionModify(oGadget, cAction)
        Local cCompany, oldArea, cGroup
        With This
            oldArea = Select()
            cGroup = .aPage.GetKey(m.oGadget.nPage)
            cGroup = Iif(Upper(m.cGroup) == HOME_PAGE, "", m.cGroup)
            If m.oGadget.bCompany
                cCompany = m.i_CODAZI
            Else
                cCompany = "xxx  "
            Endif
            *--- Gi� presente lo elimino, solita pagina
            Delete From (.cCursorModify) Where cKeyGadget==Alltrim(m.oGadget.cKeyGadget) And cGroup==m.cGroup
            If _Tally=0
                *--- Inserisco l'azione per questo gadget se non ho cancellato righe
                Insert Into (.cCursorModify) (cKeyGadget, cCompany, cGroup, cAction) Values (Alltrim(m.oGadget.cKeyGadget), m.cCompany, m.cGroup, m.cAction)
            Endif
            Select(m.oldArea)
        Endwith
    Endproc

    *--- Rimuovo solo i gadget selezionati della pagina attiva
    Procedure RemoveGadgetSelected(nPage)
        Local l_gadget, l_index
        With This
            l_nPage = Evl(m.nPage, .nActivePage)
            For l_index = .aGadget.Count To 1 Step -1
                m.l_gadget = .aGadget[m.l_index]
                If m.l_gadget.bSelected And m.l_gadget.nPage==m.l_nPage
                    .RemoveGadget(m.l_gadget)
                Endif
            Endfor
        Endwith
        l_gadget = .Null.
    Endproc

    Procedure RemoveGadget(oGadget, bNoSetModify)
        With This
            Delete From (.cCursorGadget) Where GadgetID==Alltrim(m.oGadget.Name)
            m.oGadget.NotifyEvent("GadgetRemoved")

            *--- Marco il gadget come da rimuovere, sar� rimosso dalla tabella my_gadget all'uscita dello stato modifica
            If !m.bNoSetModify
                .SetActionModify(m.oGadget, "R")
            Endif
            *--- Aggiorno la library
            If !Isnull(.oGadgetLibrary)
                .oGadgetLibrary.NotifyEvent("Filtra")
            Endif

			.aGadget.Remove(m.oGadget.name)
            m.oGadget.bAutoPageSwap = .F.
            *--- Fermo le transizioni di pagina
            IF TYPE(m.oGadget.oPgFrm.cTransitionId)='O'
                oTransitionPgfrm = m.oGadget.oPgFrm.cTransitionId 
                &oTransitionPgfrm..Release()
            EndIf			
            m.oGadget.oParentObject=.Null.
            m.oGadget.ecpQuit()

            .MoveUpGadgets()
            .ArrangeContainer()
            .Refresh()
            .nScrollPos = GetScrollPos(.nHwndClient, 1)
            .Arrangegadget()
            .MoveSplitter()
        Endwith
	ENDPROC
	
	*--- Muove un gadget nel'area specificata
	PROCEDURE MoveInGroup(oGadget, nAddType)
		*--- nAddType=1 in Home
		*--- nAddType=2 nell'area specifica
		LOCAL cKeyGadget, l_Macro
		cKeyGadget = m.oGadget.cKeyGadget
		*--- Rimuovo il gadget
		This.RemoveGadget(m.oGadget)
		*--- Riaggiungo il gadget, appico il tema di default
		l_Macro = [GSUT_BGG(This, "AddFromLibrary", m.cKeyGadget, .F., This.oCharmBar.w_MGDTHEME, m.nAddType)]
        &l_Macro
	EndProc
	
	PROCEDURE Release()
		IF This.b_Release
			This._Release(.T.)
		EndIf
	EndProc
	
    Procedure _Release(bForce)
        Local oForm, nIndex, nCount, i_nConn, bClose, l_ret, oTransitionPgfrm, bFlgExp
        bClose = .T.
        WITH This
	        *--- Sposto il focus
	        ._oDummy.SetFocus()
	        *--- Controllo salvataggio in uscita
	        If Type("This.oCharmBar")='O' And !Isnull(.oCharmBar) And .oCharmBar.bModifyMode
	            l_ret = 7
	            If Not (Type('g_UserScheduler')='L' And g_UserScheduler)
	            	IF m.bForce
	            		l_ret = Messagebox(cp_Translate("Salvare le modifiche apportate alla My Gadget?"), 4+32,Iif(Vartype(i_MsgTitle)='C',cp_Translate(i_MsgTitle),''))
	            	Else
		                l_ret = Messagebox(cp_Translate("Salvare le modifiche?"), 3+32,Iif(Vartype(i_MsgTitle)='C',cp_Translate(i_MsgTitle),''))
		            EndIf
	            Endif
	            Do Case
	                Case l_ret=6	&&Salva ed esci
	                    *--- Esco dalla modifica e salvo, eventualmente chiude la library
	                    .oCharmBar.ExitModify()
	                    *CASE l_ret=7	&&Esci senza salvare
	                Case l_ret=2	&&Annulla
	                    bClose = .F.
	            Endcase
	        Endif
	        If m.bClose
	            *--- Sposto il focus
		        ._oDummy.SetFocus()
		        *--- Se lanciata da evento release std devo controllare l'esistenza
		        IF Type("This.oCharmBar")='O' And !Isnull(.oCharmBar)
	            	.oCharmBar.oParentObject=.Null.
	           	 	UnBindEvents(.oCharmBar.nHwndClient)
	            	ReleaseCapture()
	            	.oCharmBar.ecpQuit()
	            EndIf
	            nCount = .aGadget.Count
	            i_nConn = i_ServerConn[1,2]
	            For nIndex = m.nCount To 1 Step -1
	                oForm=.aGadget[m.nIndex]
	                .aGadget.Remove(m.nIndex)
	                If Type("oForm")='O' And !Isnull(m.oForm)
	                    *--- Memorizzo lo stato del gadget (espanso/collassato)
	                    *--- Non devo salvare lo stato del gadget se in modifica (sempre espanso)
	                    *--- e verificare se � stato inizializzato al vecchio valore di MGFLGEXP (altrimenti espanso di default)
	                    m.bFlgExp = ICase(oForm.oMoveShape.Visible And oForm.bExpandedOld And !oForm.bInitCollapsed,'S',;
	                    !oForm.oMoveShape.Visible And oForm.bExpanded And !oForm.bInitCollapsed,'S','N')
	                    If i_nConn<>0
							cp_SQL(i_nConn,Textmerge("update MYG_DETT set MGFLGEXP='<<m.bFlgExp>>' where MGCODUTE=<<i_codute>> And MGCODGAD='<<Alltrim(oForm.cKeyGadget)>>'"))
						Else
							Update MYG_DETT set MGFLGEXP=(m.bFlgExp) Where MGCODUTE=(i_codute) And MGCODGAD=Alltrim(oForm.cKeyGadget)
						Endif
	                    oForm.bAutoPageSwap = .F.
	                    *--- Fermo le transizioni di pagina
	                    IF TYPE(m.oForm.oPgFrm.cTransitionId)='O'
		                    oTransitionPgfrm = m.oForm.oPgFrm.cTransitionId 
		                    &oTransitionPgfrm..Release()
		                EndIf
	                    oForm.oParentObject = .Null.
	                    oForm.ecpQuit()
	                    oForm=.Null.
	                Endif
	            Next
				
	            If Type("This.oGadgetBar_OnTop")='O' And !Isnull(.oGadgetBar_OnTop)
	                .oGadgetBar_OnTop.Release()
	                .oGadgetBar_OnTop = .Null.
	            Endif
	            If Type("This.oGadgetLibrary")='O' And !Isnull(.oGadgetLibrary)
	                .oGadgetLibrary.oParentObject = .Null.
	                .oGadgetLibrary.Release()
	            Endif

	            *--- Unbind Resize Screen
	            Unbindevent(_Screen, "Resize", This, "OnResize")
	            Unbindevent(_Screen, "Moved", This, "OnResize")

	            *--- Chiusura cursori gadget
	            Use In Select(.cCursorGadget)
	            Use In Select(.cCursorModify)
	            
				This.b_Release = .F.	&&La Release non deve richiamare la _Release
	            .Release()
	        Endif
		EndWith
    Endproc

    *--- Attivazione pagina
    Procedure nActivePage_Assign(xValue)
        *--- Mostra i gadget attivi nella pagina
        Local tmpCursor, oldArea, GadgetID
        oldArea = Select()
        With This
            If .nActivePage # m.xValue
                .nActivePage = m.xValue
                tmpCursor="tmpCursor"+Sys(2015)
                Select * From (.cCursorGadget) Order By GadgetID Into Cursor (tmpCursor)
                Select(tmpCursor)
                Go Top In (tmpCursor)
                Do While !Eof()
                    GadgetID=Alltrim(&tmpCursor..GadgetID)
                    If &tmpCursor..nPage == .nActivePage
                    	IF !.bArrangedFast OR !.aGadget[m.GadgetID].bEventArranged
                        	.aGadget[m.GadgetID].Show()
                        EndIf
                    Else
                        .aGadget[m.GadgetID].Hide()
                    Endif
                    Select(m.tmpCursor)
                    Skip
                Enddo
                .ArrangeAllGadgets()
                *--- Se la library � aperta filtro al cambio pagina
                If Type("This.oGadgetLibrary")="O" And !Isnull(.oGadgetLibrary)
                    .oGadgetLibrary.NotifyEvent("Filtra")
                Endif

                Use In Select(m.tmpCursor)
            Endif
        Endwith
        Select(m.oldArea)
    Endproc

    *--- Metodo di interpolazione lanciato da SetView
    Procedure _Move(oTrs, nPercentage, nLeftStart, nWidthStart, nLeftEnd, nWidthEnd)
        Local nLeft, nWidth
        nLeft = m.oTrs.Interpolate(m.nLeftStart, m.nLeftEnd, m.nPercentage)
        nWidth = m.oTrs.Interpolate(m.nWidthStart, m.nWidthEnd, m.nPercentage)
        This.Move(m.nLeft, 0, m.nWidth)
        oTrs = .Null.
    Endproc

    Procedure SetView(bHide, bFullScreen, bOnTop, bNoEffect)
        With This
            *--- FullScreen
            .bFullScreen = m.bFullScreen
            Local nLeft, nWidth, cParam
            If m.bFullScreen
                *--- Attivo l'aggiornamento dei visibili
                *.Move(0,0,_Screen.Width,_Screen.Height)
                nLeft = 0
                nWidth = _Screen.Width
            Else
                *--- Sposto la finestra
                nLeft = _Screen.Width-.nWidthCollapse
                nWidth = .nWidthCollapse
                If !Isnull(.oCharmBar)
                    If .oCharmBar.bPin
                        nCharmWidth = .oCharmBar.oPgFrm.Page1.oPag.oBox_1_2.Width
                    Else
                        nCharmWidth = .oCharmBar.oPgFrm.Page1.oPag.oBox_1_1.Width - 1
                    Endif
                    nLeft = m.nLeft- m.nCharmWidth
                    nWidth = .nWidthCollapse + m.nCharmWidth
                Endif
            Endif
            If m.bHide
                nLeft = _Screen.Width
            Endif
            *.Move(.Left,0,.Width,_Screen.Height)
            .Width=.Width+1
            If .Visible Or !m.bHide
                .Show()
                LOCAL lpRect,lpRectParent, X1,_X1
                lpRect = Space(16)
                GetWindowRect(This.HWnd, @lpRect)
                X1 = CToBin(Substr(m.lpRect, 1,4), "4SR")
                lpRectParent = Space(16)
                GetWindowRect(_screen.HWnd, @lpRectParent)
                _X1 = CToBin(Substr(m.lpRectParent, 1,4), "4SR")
                If !Empty(.cEffect) And !m.bNoEffect
                    cParam = Transform(m.X1 - m._X1)+","+Transform(.Width)+","+Transform(m.nLeft)+","+Transform(m.nWidth)
                    cp_SingleTransition(This, "_Move", m.cParam , "M", 500, .cEffect, m.bHide)
                Else
                    .Move(m.nLeft, 0, m.nWidth, _Screen.Height)
                Endif
            Else
                .Move(m.nLeft, 0, m.nWidth, _Screen.Height)
            Endif
            *--- Show
            If m.bHide
                .Hide()
            Endif
            *--- Back/Front
            .bOnTop = m.bOnTop
            SetWindowPos(.HWnd, 0, 0, 0, 0, 0, 0x0001+0x0002+0x0010)
        Endwith
    Endproc


    Procedure ModifyMode(bActivate, bNoSave, bOpenGadgetLibrary)
        Local l_gadget
        With This
            *--- Se gadget library � aperta devo chiuderla
            If !Isnull(This.oGadgetLibrary) And !m.bOpenGadgetLibrary
                .oGadgetLibrary.Release()
                .oGadgetLibrary = .Null.
            Endif
            Local l_Macro
            *--- Apertura gadget library
            If m.bOpenGadgetLibrary And m.bActivate
                l_Macro = [This.oGadgetLibrary = GSUT_KGS(This)]
                &l_Macro
            Endif
            .SetView(.F., m.bActivate Or .bFullScreen, .bOnTop)
            .oCntResizer.Visible = m.bActivate
            .oHighLightGadget.Visible = .F.
            *--- Timer aggiornamento automatico
            .oTimerUpdater.Enabled = .bUpdateOnTimer And !m.bActivate
            *--- Ordinamento pagine, gestione pagine cancellate
            If !m.bActivate And !m.bNoSave
                l_Macro = [GSUT_BGG(This, "Save")]
                &l_Macro
            Endif
            *--- Gadget in modalit� modifica
            For Each l_gadget In .aGadget
                m.l_gadget.ModifyMode(m.bActivate)
            Endfor
            .Arrangegadget()
            _vfp.AutoYield =.T.
            .Draw()
            _vfp.AutoYield =.F.
            If m.bOpenGadgetLibrary And m.bActivate
                This.oGadgetLibrary.SetFocusOnFirstControl()
            Endif            
        Endwith
    Endproc

    *--- Aggiusto i bordi del gadget con mousehover
    Procedure Cnt.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseHover()
    Endproc

    Procedure oCntResizer.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseHover()
    Endproc

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.MouseHover()
    Endproc

    Procedure MouseHover(oGadget)
        Local oForm
        If This.oCntResizer.Visible
            For Each oForm In This.aGadget
                If Type("oGadget")#"O" Or m.oGadget.Name#m.oForm.Name
                    m.oForm.ShowGripper(.F.)
                Endif
            Next
            If Type("oGadget")="O"
                m.oGadget.ShowGripper(.T.)
            Endif
        Endif
    Endproc
    *---
    *--- Il check si effettua solo per i gadget della prima pagina
    Func CheckCollisionSplitter()
        Local nLeft, nTop, nRight, nBottom, nX
        Local tmpCursor, oldArea
        oldArea = Select()
        With This
            tmpCursor="tmpCursor"+Sys(2015)
            nLeft = .nWidthCollapse - .nOffsetGadget + 2
            nTop = 0
            nRight = .nWidthCollapse
            nBottom = .Height
            Select * From (.cCursorGadget) Where !(m.nBottom<Y1 Or m.nTop>Y2 Or m.nRight<X1 Or m.nLeft>X2) And nPage=1 Order By X2 Desc Into Cursor (tmpCursor)
            If Used(tmpCursor)
                nX = &tmpCursor..X2
            Else
                nX = 0
            Endif
        Endwith
        Use In Select(m.tmpCursor)
        Select(m.oldArea)
        Return m.nX
    Endfunc

    Procedure MoveSplitter()
        Local nW, tmpCursor, oldArea
        oldArea = Select()
        With This
            .nWidthCollapse = .oCntResizer.Width
            nW = .CheckCollisionSplitter()
            Do While m.nW<>0
                .nWidthCollapse = m.nW + .nOffsetGadget - 1
                nW = .CheckCollisionSplitter()
            Enddo
            tmpCursor="tmpCursor"+Sys(2015)
            Select * From (.cCursorGadget) Where X2<.nWidthCollapse And nPage = 1 Order By X2 Desc Into Cursor (tmpCursor)
            If Used(tmpCursor)
                nW = &tmpCursor..X2
                .nWidthCollapse = Min(m.nW + .nOffsetGadget - 1, .Width)
            Endif
            Use In Select(m.tmpCursor)
            *--- Interpolazione
            This._ArrangeSplitter()
        Endwith
        Select(m.oldArea)
    Endproc

    Func _ArrangeSplitter()
        Local bStop, l_delta, nWidth
        With This.oCntResizer
            If .Width <> This.nWidthCollapse
                .Width = This.nWidthCollapse
            Endif
        Endwith
    Endfunc

    *--- Mouse events
    Procedure OnMouseDownGadget(oGadget)
        With This
            .oDropShape.SetShapeFrom(m.oGadget.Name)
        Endwith
    Endproc

    Procedure OnMouseUpGadget(oGadget)
        With This
            .oDropShape.Visible=.F.
            .ArrangeContainer()
            .Refresh()
            .nScrollPos = GetScrollPos(.nHwndClient, 1)
            .Arrangegadget()
            .MoveSplitter()
        Endwith
    Endproc

    Procedure OnMouseMoveGadget(nX, nY, oGadget)
        With This
            If !.bStop
                .bStop = .T.
                Local nLeft, nTop, nRight, nBottom
                nX= m.nX+Bitrshift(.nMinGadgetWidth,1)-.nMarginLeft
                nY= m.nY+Bitrshift(.nMinGadgetHeight,1)+.nScrollPos-.nMarginTop
                nLeft= Max(0, Int((m.nX+.nOffsetGadget)/(.nMinGadgetWidth+.nOffsetGadget))*(.nMinGadgetWidth+.nOffsetGadget))+.nMarginLeft
                nTop= Max(0, Int((m.nY+.nOffsetGadget)/(.nMinGadgetHeight+.nOffsetGadget))*(.nMinGadgetHeight+.nOffsetGadget))+.nMarginTop
                nRight=m.nLeft + .oDropShape.Width
                nBottom=m.nTop + .oDropShape.Height
                .MoveGadget(m.oGadget.Name, m.nLeft, m.nTop, m.nRight, m.nBottom, .T.)
                .GetRect(m.oGadget.Name, @nLeft, @nTop, @nRight, @nBottom)
                .oDropShape.Move(m.nLeft, m.nTop)
                .bStop = .F.
            Endif
        Endwith
    Endproc

    *!* Notifico gli eventi di MouseDown e MouseUp sull'area che consente il resize sul gadget.
    *!* Questo d� la possibilit�, ad esempio, di forzare la notifica dell'evento 'GadgetArranged' quando 
    *!* quest'ultimo avr� preso le dimensioni corrette (l'evento normalmente viene notificato solo alla prima apertura)
    *!* E' presente un esempio di questo utilizzo � nel modello gadget GSUT_GVC .
    Procedure OnMouseDownResizeGadget(oGadget)
    	m.oGadget.NotifyEvent('MouseDownResizeGadget')
        This.oDropShape.SetShapeFrom(m.oGadget.Name)
    Endproc

    Procedure OnMouseUpResizeGadget(oGadget)
    	m.oGadget.NotifyEvent('MouseUpResizeGadget')
        This.OnMouseUpGadget(m.oGadget)
    Endproc

    Procedure OnMouseResizeGadget(oGadget)
        Local nLeft, nTop, nRight, nBottom, nWidth, nHeight
        With This
            If !.bStop
                .bStop = .T.
                .GetRect(m.oGadget.Name, @nLeft, @nTop, @nRight, @nBottom)
                nWidth = (Int(m.oGadget.Width/(.nMinGadgetWidth+.nOffsetGadget))+1)*(.nMinGadgetWidth+.nOffsetGadget)-.nOffsetGadget
                nHeight = (Int(m.oGadget.Height/(.nMinGadgetHeight+.nOffsetGadget))+1)*(.nMinGadgetHeight+.nOffsetGadget)-.nOffsetGadget
                nRight=m.nLeft + m.nWidth
                nBottom=m.nTop + m.nHeight
                .oDropShape.Move(m.nLeft, m.nTop, m.nWidth, m.nHeight)
                .MoveGadget(m.oGadget.Name, m.nLeft, m.nTop, m.nRight, m.nBottom)
                .Draw()
                .bStop = .F.
            Endif
        Endwith
    Endproc

    *-------------------------------------*
    Func AddGadget(cGadgetPrg, bHide, cPag, cTitlePag, bAskPage)
        *--- cGadgetPrg:	Nome file prg del gadget
        Local l_gadget, X1,Y1,X2,Y2, cPageKey, cTitlePage, nPage, bNewPage
        bNewPage = .F.
        With This
            *--- Focus alla form cp_GadgetManager
            .bNoActivate = .F. &&Permetto l'attivazione
            ._oDummy.SetFocus()
            *--- Aggiungo il gadget
            l_gadget = &cGadgetPrg(This)
            .bNoActivate = .T.  &&Disabilito l'attivazione
            .aGadget.Add(m.l_gadget, m.l_gadget.Name)
            *--- Posiziono il nuovo gadget sempre al primo posto
            X1= .nMarginLeft
            Y1= .nMarginTop
            X2= m.l_gadget.Width + .nMarginLeft
            Y2= m.l_gadget.Height + .nMarginTop

            *--- Gestione pagine
            cPageKey = Alltrim(Evl(Nvl(m.cPag, HOME_PAGE), HOME_PAGE))
            cTitlePage = Evl(Nvl(m.cTitlePag, m.cPageKey), m.cPageKey)

            *--- Cerco se la pagina � gi� presente
            *--- .nActivePage vale 1 oppure la pagina specifica del gadget (la Gadget Library filtra gi� per pagina attiva)
            m.nPage = .nActivePage
            If .aPage.GetKey(m.cPageKey) == 0
                *--- Se non trovo la pagina e sto aggiungendo da Gadget Library domando se si vuole creare la nuova pagina
                *--- (in questo caso dovrei essere sempre in pagina 1, dalle altre pagina non posso aggiungere gadget non per quella pagina)
                If !m.bAskPage Or !ah_YesNo([Gadget assegnato all'area "%1".%0Aggiungere il gadget all'area Home?%0(S�=Area Home, No=Area "%1")], 32 ,m.cTitlePage)
                    .aPage.Add(m.cTitlePage, m.cPageKey)
                    nPage = .aPage.Count
                    *--- Aggiungere pagina anche al controllo pageframe
                    .oTabPage.AddPage(m.cTitlePage, m.cPageKey)
                    *--- Occorre gestire anche la rimozione gadget, se non ho pi� gadget tolgo la pagina? Lo farebbe gi� al prossimo riavvio
                    bNewPage = .T.
                Endif
            Else
                *--- Se sono in Load inserisco sempre nell'area, altrimenti nella pagina corrente
                If !m.bAskPage
                    m.nPage = .aPage.GetKey(m.cPageKey)
                Endif
            Endif

            *--- Aggiungo il gadget nel cursore
            Insert Into (.cCursorGadget) (GadgetID, X1, Y1, X2, Y2, bSkip, nPage) Values (m.l_gadget.Name, m.X1, m.Y1, m.X2, m.Y2, .F., m.nPage)
            .SetPage(m.l_gadget.Name, m.nPage, m.bHide)

            *--- Solo se sono nella pagina giusto mostro il gadget
            If !m.bHide And m.nPage==.nActivePage
                l_gadget.Show()
            Endif
            *--- Se sono in modalit� interattiva mi sposto anche di pagina
            If m.bAskPage And m.bNewPage
                .nActivePage = m.nPage
                .oTabPage.HightLightPage(.nActivePage)
            Endif
        Endwith
        Return l_gadget
    Endfunc

    *--- Adatta le dimensioni o posizione del gadget in modo che sia compatibile con la "griglia" di posizionamento dei gadget
    *--- nX,nY,nW,nH sono opzionali, se non passati utilizza quelli presenti nel cCursorGadget
    Procedure ArrangePositionGadget(GadgetID, nX,nY,nW,nH)
        With This
            Local nLeft, nTop, nRight, nBottom, nWidth, nHeight
            .GetRect(m.GadgetID, @nLeft, @nTop, @nRight, @nBottom)
            m.nWidth = Iif(Type("nW")=="L", m.nRight-m.nLeft, m.nW)
            m.nHeight = Iif(Type("nH")=="L", m.nBottom-m.nTop, m.nH)
            m.nLeft = Iif(Type("nX")=="L", m.nLeft, m.nX)
            m.nTop = Iif(Type("nY")=="L", m.nTop, m.nY) + m.nHeight

            m.nWidth = (Int(m.nWidth/(.nMinGadgetWidth+.nOffsetGadget))+1)*(.nMinGadgetWidth+.nOffsetGadget)-.nOffsetGadget
            m.nHeight = (Int(m.nHeight/(.nMinGadgetHeight+.nOffsetGadget))+1)*(.nMinGadgetHeight+.nOffsetGadget)-.nOffsetGadget

            m.nLeft = m.nLeft+Bitrshift(.nMinGadgetWidth,1)-.nMarginLeft
            m.nTop = m.nTop+Bitrshift(.nMinGadgetHeight,1)+.nScrollPos-.nMarginTop

            m.nLeft = Max(0, Int((m.nLeft+.nOffsetGadget)/(.nMinGadgetWidth+.nOffsetGadget))*(.nMinGadgetWidth+.nOffsetGadget))+.nMarginLeft
            m.nTop = Max(0, Int((m.nTop+.nOffsetGadget)/(.nMinGadgetHeight+.nOffsetGadget))*(.nMinGadgetHeight+.nOffsetGadget))+.nMarginTop
			
            m.nRight= m.nLeft + m.nWidth
            m.nBottom= m.nTop + m.nHeight

            .MoveGadget(m.GadgetID, m.nLeft, m.nTop, m.nRight, m.nBottom, .T.)

            .MoveUpGadgets()
            .ArrangeContainer()
            .Arrangegadget()
            .MoveSplitter()
        Endwith
    Endproc

    Procedure GetRect(GadgetID, nX1,nY1,nX2,nY2)
        *--- X1,Y1,X2,Y2 sono passati per riferimento
        Local tmpCursor,oldArea
        tmpCursor="tmpCursor"+Sys(2015)
        GadgetID = Alltrim(m.GadgetID)
        oldArea = Select()
        Select * From (This.cCursorGadget) Where Alltrim(GadgetID)=m.GadgetID Into Cursor (m.tmpCursor)
        If Used(tmpCursor)
            nX1=&tmpCursor..X1
            nY1=&tmpCursor..Y1
            nX2=&tmpCursor..X2
            nY2=&tmpCursor..Y2
        Endif
        Use In Select(m.tmpCursor)
        Select(m.oldArea)
    Endproc

    *--- Set
    *--- bHideOnLoad, setpage da addgadget
    Procedure SetPage(GadgetID, nPage, bHideOnLoad)
        GadgetID = Alltrim(m.GadgetID)
        With This
            Update (.cCursorGadget) Set nPage=m.nPage Where Alltrim(GadgetID)=m.GadgetID
            .aGadget[m.GadgetID].nPage = m.nPage
            If m.nPage # .nActivePage
                .aGadget[m.GadgetID].Hide()
            Else
                If !m.bHideOnLoad
                    .aGadget[m.GadgetID].Show()
                Endif
                .CheckCollision(m.GadgetID)
            Endif
        Endwith
    Endproc

    Procedure SetSkip(GadgetID, bSkip)
        GadgetID = Alltrim(m.GadgetID)
        Update (This.cCursorGadget) Set bSkip=m.bSkip Where Alltrim(GadgetID)=m.GadgetID
    Endproc

    Procedure SetRect(GadgetID, nX1,nY1,nX2,nY2)
        GadgetID = Alltrim(m.GadgetID)
        Update (This.cCursorGadget) Set X1=m.nX1, Y1=m.nY1, X2=m.nX2, Y2=m.nY2 Where Alltrim(GadgetID)=m.GadgetID
    Endproc
    *---

    Func CheckCollision(GadgetID)
        *--- Ritorna il primo gadget che collide
        Local nLeft, nTop, nRight, nBottom, GIdToRet
        Local tmpCursor,oldArea
        tmpCursor="tmpCursor"+Sys(2015)
        This.GetRect(m.GadgetID, @nLeft, @nTop, @nRight, @nBottom)
        oldArea = Select()
        Select GadgetID From (This.cCursorGadget) Where !(m.nBottom<=Y1 Or m.nTop>=Y2 Or m.nRight<=X1 Or m.nLeft>=X2) And !bSkip And nPage=This.nActivePage Order By Y1,X1 Into Cursor (tmpCursor)
        If Used(tmpCursor)
            GIdToRet = &tmpCursor..GadgetID
        Else
            GIdToRet = ""
        Endif
        Use In Select(tmpCursor)
        Select(m.oldArea)
        Return GIdToRet
    Endfunc

    *--- Sposta e ridimensiona un gadget
    Procedure MoveGadget(GadgetID, nLeft, nTop, nRight, nBottom, bForce)
        With This
            ._MoveGadget(m.GadgetID, m.nLeft, m.nTop, m.nRight, m.nBottom)
            .MoveUpGadgets()
            .Arrangegadget(m.GadgetID, bForce)
            .aGadget[m.GadgetID].nCoordX1 = m.nLeft
            .aGadget[m.GadgetID].nCoordY1 = m.nTop
            .aGadget[m.GadgetID].nCoordX2 = m.nRight
            .aGadget[m.GadgetID].nCoordY2 = m.nBottom
        Endwith
    Endproc

    Procedure _MoveGadget(GadgetID, nLeft, nTop, nRight, nBottom)
        Local CollisionID, nLeftNew, nTopNew, nRightNew, nBottomNew
        With This
            .SetSkip(m.GadgetID, .T.)
            .SetRect(m.GadgetID, m.nLeft, m.nTop, m.nRight, m.nBottom)
            CollisionID = .CheckCollision(m.GadgetID)
            Do While Not Empty(m.CollisionID)
                .GetRect(m.CollisionID, @nLeftNew, @nTopNew, @nRightNew, @nBottomNew)
                nTopNew = m.nTopNew + .nMinGadgetHeight + .nOffsetGadget
                nBottomNew = m.nBottomNew + .nMinGadgetHeight + .nOffsetGadget
                ._MoveGadget(m.CollisionID, m.nLeftNew, m.nTopNew, m.nRightNew, m.nBottomNew)
                CollisionID = .CheckCollision(m.GadgetID)
            Enddo
            .SetSkip(m.GadgetID, .F.)
        Endwith
    Endproc

    *--- verifico la presenza di collisioni
    Procedure CheckCollisionGadget()
        Local nLeft, nTop, nRight, nBottom, GadgetID
        Local tmpCursor,oldArea
        oldArea = Select()
        tmpCursor="tmpMoveUp"+Sys(2015)
        With This
            Select * From (.cCursorGadget) Where nPage=.nActivePage Order By Y1, X1 Into Cursor (tmpCursor)
            Select(tmpCursor)
            Go Top In (tmpCursor)
            Do While !Eof()
                GadgetID=&tmpCursor..GadgetID
                nLeft=&tmpCursor..X1
                nTop=&tmpCursor..Y1
                nRight=&tmpCursor..X2
                nBottom=&tmpCursor..Y2
                ._MoveGadget(m.GadgetID, m.nLeft, m.nTop, m.nRight, m.nBottom)
                Select(m.tmpCursor)
                Skip
            Enddo
            Use In Select(m.tmpCursor)
        Endwith
        Select(m.oldArea)
    Endproc

    *--- Sposto i gadget verso l'alto se c'� spazio
    Procedure MoveUpGadgets()
        Local CollisionID, nLeft, nTop, nRight, nBottom, GadgetID
        Local tmpCursor,oldArea
        oldArea = Select()
        tmpCursor="tmpMoveUp"+Sys(2015)
        With This
            *--- Ordino dal pi� alto al pi� basso, escludendo la prima riga (Y1>This.nOffsetGadget)
            *Select * From (.cCursorGadget) Where Y1>.nMarginTop And nPage=.nActivePage Order By Y1, X1 Into Cursor (tmpCursor)
            Select * From (.cCursorGadget) Where nPage=.nActivePage Order By Y1, X1 Into Cursor (tmpCursor)
            Select(tmpCursor)
            Go Top In (tmpCursor)
            Do While !Eof()
                GadgetID=&tmpCursor..GadgetID
                nLeft=&tmpCursor..X1
                nTop=&tmpCursor..Y1
                nRight=&tmpCursor..X2
                nBottom=&tmpCursor..Y2
                .SetSkip(m.GadgetID, .T.)
                CollisionID = ""
                Do While Empty(m.CollisionID) And m.nTop>=.nMarginTop
                    nTop = m.nTop - .nMinGadgetHeight - .nOffsetGadget
                    If nTop<.nMarginTop
                        nTop = 0
                    Endif
                    nBottom = m.nBottom - .nMinGadgetHeight - .nOffsetGadget
                    .SetRect(m.GadgetID, m.nLeft, m.nTop, m.nRight, m.nBottom)
                    CollisionID = .CheckCollision(m.GadgetID)
                Enddo
                *--- Riaggiungo sempre l'altezza
                nTop = m.nTop + .nMinGadgetHeight + .nOffsetGadget
                nBottom = m.nBottom + .nMinGadgetHeight + .nOffsetGadget
                .SetRect(m.GadgetID, m.nLeft, m.nTop, m.nRight, m.nBottom)
                .SetSkip(m.GadgetID, .F.)
                Select(m.tmpCursor)
                Skip
            Enddo
            Use In Select(m.tmpCursor)
        Endwith
        Select(m.oldArea)
    Endproc

    *--- Sistemo i gadget nella loro posizione
    Func _Arrangegadget()
        Local nLeft, nTop, nWidth, nHeight, GadgetID, tmpArrange,deltaX,deltaY,deltaW,deltaH
        Local deltaX_OffSet,deltaY_OffSet,deltaW_OffSet,deltaH_OffSet,bStop,bMoved,oldArea
        tmpArrange="tmpArrange"+Sys(2015)
        oldArea = Select()
        With This
            *--- Riassegno sempre il padre
            bStop = .T.
            bMoved = .T.
            Select * From (.cCursorGadget) Where (Alltrim(GadgetID)<>.GadgetToExclude Or Empty(.GadgetToExclude)) And nPage=.nActivePage Order By Y1, X1 Into Cursor (tmpArrange)
            Select(m.tmpArrange)
            Go Top In (m.tmpArrange)
            Do While !Eof()
                GadgetID=Alltrim(&tmpArrange..GadgetID)
                nLeft=&tmpArrange..X1
                nTop=&tmpArrange..Y1
                nWidth=&tmpArrange..X2-m.nLeft
                nHeight=&tmpArrange..Y2-m.nTop
                If (.aGadget[m.GadgetID].Left)<>m.nLeft Or (.aGadget[m.GadgetID].Top+.nScrollPos)<>m.nTop Or .aGadget[m.GadgetID].Width<>m.nWidth Or m.nHeight<>.aGadget[m.GadgetID].Height
                	*--- Se l'animazione � spenta vado subito alla posizione giusta
                	*--- nLeft,nTop,nWidth,nHeight sono gi� corretti
					IF !.bArrangedFast                 
	                    *--- Si pu� pensare ad una interpolazione ---*
	                    deltaX=m.nLeft-.aGadget[m.GadgetID].Left
	                    deltaY=m.nTop-.aGadget[m.GadgetID].Top+.nScrollPos
	                    deltaW=m.nWidth-.aGadget[m.GadgetID].Width
	                    deltaH=m.nHeight-.aGadget[m.GadgetID].Height

	                    deltaX_OffSet = Iif(m.deltaX<0,-1,1)
	                    deltaY_OffSet = Iif(m.deltaY<0,-1,1)
	                    deltaW_OffSet = Iif(m.deltaW<0,-1,1)
	                    deltaH_OffSet = Iif(m.deltaH<0,-1,1)

	                    deltaX_OffSet = Evl(Int(m.deltaX/.nScale), m.deltaX_OffSet)
	                    deltaY_OffSet = Evl(Int(m.deltaY/.nScale), m.deltaY_OffSet)
	                    deltaW_OffSet = Evl(Int(m.deltaW/.nScale), m.deltaW_OffSet)
	                    deltaH_OffSet = Evl(Int(m.deltaH/.nScale), m.deltaH_OffSet)

	                    nLeft=Iif(.aGadget[m.GadgetID].Left=m.nLeft,m.nLeft,.aGadget[m.GadgetID].Left+m.deltaX_OffSet)
	                    nTop=Iif(.aGadget[m.GadgetID].Top+.nScrollPos=m.nTop,m.nTop,.aGadget[m.GadgetID].Top+m.deltaY_OffSet)
	                    nWidth=Iif(.aGadget[m.GadgetID].Width=m.nWidth,m.nWidth,.aGadget[m.GadgetID].Width+m.deltaW_OffSet)
	                    nHeight=Iif(.aGadget[m.GadgetID].Height=m.nHeight,m.nHeight,.aGadget[m.GadgetID].Height+m.deltaH_OffSet)
	                ENDIF 

                    .aGadget[m.GadgetID].Move(m.nLeft, m.nTop-.nScrollPos, m.nWidth, m.nHeight)
                    .aGadget[m.GadgetID].nCoordX1 = m.nLeft
                    .aGadget[m.GadgetID].nCoordY1 = m.nTop-.nScrollPos
                    .aGadget[m.GadgetID].nCoordX2 = m.nWidth + .aGadget[m.GadgetID].nCoordX1
                    .aGadget[m.GadgetID].nCoordY2 = m.nHeight + .aGadget[m.GadgetID].nCoordY1
                    bMoved = &tmpArrange..X1=m.nLeft And &tmpArrange..Y1=m.nTop And (&tmpArrange..X2-m.nLeft)=m.nWidth And m.nHeight=(&tmpArrange..Y2-m.nTop)
                    *--- Notifico al gadget che � stato sistemato al suo posto
                    If m.bMoved And .aGadget[m.GadgetID].bEventArranged
	                    IF .bArrangedFast
	                    	*--- Forzo il resize, chiamare la resize non � sufficente
		                    .aGadget[m.GadgetID].Show()
		                    If .aGadget[m.GadgetID].bExpanded
		                    .aGadget[m.GadgetID].Width = .aGadget[m.GadgetID].Width+1 
		                    .aGadget[m.GadgetID].Width = .aGadget[m.GadgetID].Width-1
		                    EndIf
		                Endif
                        .aGadget[m.GadgetID].NotifyEvent("GadgetArranged")
                        *--- Notificato quindi rimetto a false
                        .aGadget[m.GadgetID].bEventArranged = .F.
                    Else
						If .bArrangedFast And .aGadget[m.GadgetID].bExpanded
	                    	*--- Forzo il resize, chiamare la resize non � sufficente
		                    .aGadget[m.GadgetID].Width = .aGadget[m.GadgetID].Width+1 
		                    .aGadget[m.GadgetID].Width = .aGadget[m.GadgetID].Width-1
		                Endif                    
                    Endif

                Endif
                bStop = m.bStop And m.bMoved
                Select(m.tmpArrange)
                Skip 1
            Enddo
            Use In Select(m.tmpArrange)
            *--- Se necessario mostro le scrollbar
            If m.bStop
                .ArrangeContainer()
            Endif
        Endwith
        Select(m.oldArea)
        Return m.bStop
    Endfunc

    *--- Riposiziona tutti i gadget
    Procedure ArrangeAllGadgets()
        With This
            .CheckCollisionGadget()
            .MoveUpGadgets()
            .ArrangeContainer()
            .Refresh()
            .nScrollPos = GetScrollPos(.nHwndClient, 1)
            .Arrangegadget()
        Endwith
    Endproc

    Procedure Arrangegadget(GadgetToExclude, bForce)
        With This
            .GadgetToExclude = Alltrim(Evl(m.GadgetToExclude, ""))
            If m.bForce
                =._Arrangegadget()
            Endif
            .oMoveTimer.Enabled= .T.
        Endwith
    Endproc

    Procedure ArrangeContainer()
        Local tmpArrange, oldArea
        tmpArrange="tmpArrange"+Sys(2015)
        With This
            oldArea = Select()
            Select Max(X2) As X2, Max(Y2) As Y2 From (.cCursorGadget) Where nPage=.nActivePage Into Cursor (tmpArrange)
            .Cnt.Move(.nMarginLeft, .nMarginTop, Max(&tmpArrange..X2-.nMarginLeft,0), Max(&tmpArrange..Y2-.nMarginTop,0))
            .oCntResizer.Height = Max(.Cnt.Height, .Height)
            .VscrollSmallChange = .Height/5
            .Refresh()
            IF !ISNULL(.oCharmBar)
            	LOCAL oLblUp,oLblDown
	            oLblUp = .oCharmBar.GetCtrl("<<")
	            oLblDown = .oCharmBar.GetCtrl(">>")
				LOCAL lpRect,lpRectParent, Y1,ParentY1 , Y2,ParentY2 
                lpRect= Space(16)
                GetWindowRect(.nHwndClientScroll, @lpRect)
                *X1 = CToBin(Substr(m.lpRect, 1,4), "4SR")
                Y1 = CToBin(Substr(m.lpRect, 5,4), "4SR")
                *X2 = CToBin(Substr(m.lpRect, 9,4), "4SR")
                Y2 = CToBin(Substr(m.lpRect, 13,4), "4SR")
                lpRectParent = Space(16)
                GetWindowRect(.nHwndClient, @lpRectParent)
                *ParentX1 = CToBin(Substr(m.lpRectParent, 1,4), "4SR")
                ParentY1 = CToBin(Substr(m.lpRectParent, 5,4), "4SR")
                *ParentX2 = CToBin(Substr(m.lpRectParent, 9,4), "4SR")
                ParentY2 = CToBin(Substr(m.lpRectParent, 13,4), "4SR")
                
	            oLblUp.Visible = m.ParentY1-m.Y1>0
	            oLblDown.Visible = (m.ParentY2-m.ParentY1 < m.Y2-m.Y1) And m.ParentY1-m.Y1<(m.Y2-m.Y1)-(m.ParentY2-m.ParentY1)
	            oLblUp = .Null.
	            oLblDown = .Null.
	        ENDIF 
        Endwith
        Use In Select(m.tmpArrange)
        Select(m.oldArea)
    Endproc

    Procedure Scrolled(nDirection)
        With This
            .nScrollPos = GetScrollPos(.nHwndClient, 1)
            .Arrangegadget()
        Endwith
    Endproc

    *--- Forza Aggiornamento di tutti i gadget
    Procedure NotifyEvent(cEvent, bActivePage)
    	Local l_gadget
    	With This
    		*--- Se necessario informo l'utente che � stata aperta una finestra dietro l'interfaccia gadget
    		If m.cEvent=="GadgetShowForm" And .bOnTop And .nModalHWnd=0
    			*--- Nascondo precedenti tooltip o balloon
    			.oBalloon.ctlHide(2)
    			.oBalloon.ctlVisible = .F.
    			.oCharmBar.oBalloon.ctlHide(2)
    			.oCharmBar.oBalloon.ctlVisible = .F.
    			.oCharmBar.onShow() && mostro il bottone su cui puntare il balloon
    			.oCharmBar.oBalloon.ctlShow(1,cp_Translate("Sono presenti maschere in secondo piano"),"")
    			cp_CallAfter("Iif(Vartype(oGadgetManager)='O',oGadgetManager.oCharmBar.onHide(),1=1)",.oCharmBar.oBalloon.ctlHideDelay)
    		Endif
    		*--- Notifico l'evento ai gadget
    		For Each l_gadget In .aGadget
    			If !m.bActivePage
    				m.l_gadget.NotifyEvent(m.cEvent)
    				If m.l_gadget.nPage <> .nActivePage
    					m.l_gadget.Hide()	&& Nascondo sempre i gadget delle pagine non attive
    				Endif
    			Else
    				If m.l_gadget.nPage = .nActivePage
    					m.l_gadget.NotifyEvent(m.cEvent)
    				Endif
    			Endif
    		Endfor
    	Endwith
    	l_gadget = .Null.
    Endproc

    *--- Verifica una propriet� logica di ogni gadget
    *--- cProp
    *--- cOp = 'AND' / 'OR'
    *--- bOnlySelected= .T. solo selezionati, .F. tutti
    *--- nPage= -1 tutte le pagine, .F./0 pagina corrente, numero pagina
    Function GetLogicProp(cProp, cOp, bOnlySelected, nPage)
        Local l_gadget, l_cOp, l_bRet, l_nPage
        l_cOp = Upper(Evl(m.cOp, "AND"))
        l_nPage = Evl(m.nPage, This.nActivePage)
        l_bRet = (m.l_cOp # "OR")
        With This
            For Each l_gadget In .aGadget
                If ((m.bOnlySelected And m.l_gadget.bSelected) Or !m.bOnlySelected) And (m.l_gadget.nPage = m.l_nPage Or m.l_nPage=-1)
                    l_bRet = m.l_bRet &l_cOp m.l_gadget.&cProp
                Endif
            Endfor
        Endwith
        l_gadget = .Null.
        Return m.l_bRet
    Endfunc

    *--- Setta una propriet� di ogni gadget
    *--- cProp
    *--- xValue
    *--- bOnlySelected= .T. solo selezionati, .F. tutti
    *--- nPage= -1 tutte le pagine, .F./0 pagina corrente, numero pagina
    Procedure SetAllProp(cProp, xValue, bOnlySelected, nPage)
        Local l_gadget, l_nPage
        l_nPage = Evl(m.nPage, This.nActivePage)
        With This
            For Each l_gadget In .aGadget
                If Pemstatus(l_gadget, m.cProp,5) And ((m.bOnlySelected And m.l_gadget.bSelected) Or !m.bOnlySelected) And (m.l_gadget.nPage = m.l_nPage Or m.l_nPage=-1)
                    m.l_gadget.&cProp = m.xValue
                Endif
            Endfor
        Endwith
        l_gadget = .Null.
    Endproc

    *--- Setto la propriet� bEventArranged
    Procedure bEventArranged_Assign(xValue)
        Local l_gadget
        With This
            For Each l_gadget In .aGadget
                m.l_gadget.bEventArranged = m.xValue
            Endfor
        Endwith
        l_gadget = .Null.
    Endproc


    *--- test hwnd gaget per WM_NCACTIVATE
    Function TestHwnd(nHwnd)
        Local l_gadget, bTest
        bTest = .F.
        With This
            For Each l_gadget In .aGadget
                bTest = m.bTest Or m.l_gadget.HWnd=m.nHwnd
            Endfor
            bTest = m.bTest Or .HWnd=m.nHwnd Or .oCharmBar.HWnd=m.nHwnd
        Endwith
        l_gadget = .Null.
        Return m.bTest
    Endfunc

    Procedure RegionOff()
        If This.hRegion <> 0
            If SetWindowRgn(This.HWnd, 0, 1) <> 0
                DeleteObject(This.hRegion)
                This.hRegion=0
            Endif
        Endif
    Endproc

    Procedure RegionOn()
        This.RegionOff()
        Local oForm, hReg, nX, nY, nW, nH

        If This.oDropShape.Visible
            nX = This.oDropShape.Left
            nY = This.oDropShape.Top
            nW = nX + This.oDropShape.Width
            nH = nY + This.oDropShape.Height
        Else
            nX = 0
            nY = 0
            nW = 0
            nH = 0
        Endif
        This.hRegion = CreateRectRgn(nX, nY, nW, nH)

        For Each oForm In This.aGadget
            nY = oForm.Top
            nX = oForm.Left
            nW = nX + oForm.Width
            nH = nY + oForm.Height
            hReg = CreateRectRgn(nX, nY, nW, nH)
            CombineRgn(This.hRegion, This.hRegion, hReg, RGN_OR)
            DeleteObject(hReg)
        Next

        If This.hRegion <> 0
            SetWindowRgn(This.HWnd, This.hRegion, 1)
        Endif

    Endproc

    Procedure Cnt.MouseWheel
        Lparameters nDirection, nShift, nXCoord, nYCoord
 		ThisForm.MouseWheel(m.nDirection, m.nShift, m.nXCoord, m.nYCoord)

    Procedure oCntResizer.MouseWheel
        Lparameters nDirection, nShift, nXCoord, nYCoord
        ThisForm.MouseWheel(m.nDirection, m.nShift, m.nXCoord, m.nYCoord)

	Procedure MouseWheel
        Lparameters nDirection, nShift, nXCoord, nYCoord
        If m.nDirection>0
            SendMessage(Thisform.HWnd, 0x115, 0, 0)
            SendMessage(Thisform.nHwndClient, 0x115, 0, 0)
        Else
            SendMessage(Thisform.HWnd, 0x115, 1, 0)
            SendMessage(Thisform.nHwndClient, 0x115, 1, 0)
        Endif
	ENDPROC
	
    Procedure Declare()

        Declare Integer SetParent In WIN32API;
            INTEGER hWndChild,;
            INTEGER hWndNewParent
        Declare Integer DeleteObject In WIN32API;
            INTEGER hObject
        Declare Integer CreateRectRgn In WIN32API;
            INTEGER nLeftRect, Integer nTopRect,;
            INTEGER nRightRect, Integer nBottomRect
        Declare Integer SetWindowRgn In WIN32API;
            INTEGER HWnd, Integer hRgn, SHORT bRedraw
        Declare Integer CombineRgn In WIN32API;
            INTEGER hrgnDest, Integer hrgnSrc1,;
            INTEGER hrgnSrc2,;
            INTEGER fnCombineMode
        Declare Integer GetWindowLong In Win32API Integer HWnd, Integer nIndex
        Declare Integer SetWindowLong In WIN32API;
            INTEGER HWnd,;
            INTEGER nIndex,;
            INTEGER dwNewLong
        Declare Integer GetScrollPos In WIN32API Integer HWnd, Integer nBar
        Declare Integer SendMessage In WIN32API As SendMessageStr ;
            INTEGER HWnd,;
            INTEGER Msg,;
            INTEGER wParam,;
            STRING @Lparam
        Declare Integer SetWindowPos In WIN32API;
            INTEGER HWnd,;
            INTEGER hWndInsertAfter,;
            INTEGER x,;
            INTEGER Y,;
            INTEGER cx,;
            INTEGER cy,;
            INTEGER wFlags
        Declare Integer GetWindow In WIN32API;
            INTEGER HWnd,;
            INTEGER wFlag
        Declare Integer GetTopWindow In WIN32API Integer HWnd
        Declare Integer LockWindowUpdate In WIN32API Integer hWndLock

    Endproc

    Procedure Eventhandler(HWnd, Msg, wParam, Lparam)
        With This
            If !.bOnTop
                WriteInt(m.Lparam + 4, .GetBottomWindow())
            Else
                WriteInt(m.Lparam + 4, .GetTopWindow())
            Endif
            Return CallWindowProc(.nOldProc, m.HWnd, m.Msg, m.wParam, m.Lparam)
        Endwith
    Endproc

    Function GetTopWindow()
        Local HWnd
        *--- Se � aperta una maschera modale l'interfaccia deve sempre starci dietro
        If This.nModalHWnd<>0 And IsWindow(This.nModalHWnd)<>0
        	Return This.nModalHWnd &&GetWindow(This.nModalHWnd, GW_HWNDNEXT)
        Else
        	This.nModalHWnd = 0
        Endif
        HWnd = GetTopWindow(_Screen.HWnd)
        If m.HWnd=This.HWnd
            Return GetWindow(m.HWnd, GW_HWNDNEXT)
        Else
            Return m.HWnd
        Endif
    Endfunc

    Function GetBottomWindow()
        Local HWnd, hWndNext
        HWnd = 0
        hWndNext = GetTopWindow(_Screen.HWnd)
        Do While hWndNext<>0
            HWnd = m.hWndNext
            hWndNext = GetWindow(m.HWnd, GW_HWNDNEXT)
        Enddo
        Return m.HWnd

    Function GetColorRandom()
        Local numColor

        Dimension aColor[5]
        aColor[1]=Rgb(42,119,244)
        aColor[2]=Rgb(85,53,175)
        aColor[3]=Rgb(0,159,0)
        aColor[4]=Rgb(176,26,64)
        aColor[5]=Rgb(218,84,45)
        numColor = Int(Mod(100*Rand(), Alen(aColor)))+1
        Return aColor[numColor]
    Endfunc

    Proc Cnt.DragDrop(oSource,nXCoord,nYCoord)
        If Vartype(m.oSource)="O" And Lower(m.oSource.Class)=="librarygrid"
            This.Parent.DragDrop(m.oSource,m.nXCoord,m.nYCoord)
        Endif
    Endproc

    Proc oCntResizer.DragDrop(oSource,nXCoord,nYCoord)
        If Vartype(m.oSource)="O" And Lower(m.oSource.Class)=="librarygrid"
            This.Parent.DragDrop(m.oSource,m.nXCoord,m.nYCoord)
        Endif
    Endproc

    Proc DragDrop(oSource,nXCoord,nYCoord)
        Local oItem
        With This
            If Vartype(m.oSource)="O" And Lower(m.oSource.Class)=="librarygrid"
                If Vartype(.oGadgetLibrary)=='O'
                    .oGadgetLibrary.AddGadget(.f., m.nXCoord, m.nYCoord)
                Endif
                m.oSource.Drag(0)
            Endif
        Endwith
    Endproc

	*--- OnRightClick
	PROCEDURE RightClick()
		This.OnRightClick()
	EndProc

    Procedure Cnt.RightClick()
        This.Parent.OnRightClick()
    Endproc

    Procedure oCntResizer.RightClick()
        This.Parent.OnRightClick()
    Endproc

	PROCEDURE OnRightClick()
        *--- Show menu
        *--- per maggiore velocit� carico il menu da cursore
        LOCAL oldarea
        oldarea = Select()
        WITH This
            IF Vartype(.oCharmBar)=='O' And !ISNULL(.oCharmBar) And .oCharmBar.bModifyMode
	        	LOCAL cCursor, l_MenuID, l_i, l_lvlKey, l_cCaption, l_cProgram, l_bSkip, l_bCheck, l_nDirectory, l_cBmp, l_lvlGrp, l_lvlCustom
	            Local loPopupMenu, cnCmd
	            cnCmd = Sys(2015)
	            Public &cnCmd	&&Contiene il comando da eseguire alla selezione voce di menu
	            &cnCmd = ""      
	            *--- Nuovo menu contestuale  	
				loPopupMenu = Createobject("cbPopupMenu")
				loPopupMenu.oPOPUPMENU.Init()
				cCursor = loPopupMenu.oPOPUPMENU.CCURSORNAME

				If VARTYPE(g_MenuPopupID)="U"
					PUBLIC g_MenuPopupID
					g_MenuPopupID = 5000
				EndIf
				g_MenuPopupID = m.g_MenuPopupID + 5000
				l_MenuID = m.g_MenuPopupID
				l_i=1

				*--- Livello 0
				INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
										("",0,"",0,.T., m.l_MenuID,"","001",.F.,.F.)
				*--- Area
				l_lvlGrp = '001.'
				LOCAL l_g
				l_g = 1

				l_bIsSelectedGadget = .GetLogicProp("bSelected", "OR")	&&selezionati solo dell'area attuale
				l_MenuID = m.l_MenuID + 1
				l_lvlKey = m.l_lvlGrp+PADL(m.l_g, 3, "0")
				l_lvlTheme_grp = l_lvlKey+"." &&mi salvo il livello
				l_g = m.l_g + 1
				l_cCaption = cp_Translate("Tema")
				l_cProgram = ""
				l_bSkip = !m.l_bIsSelectedGadget 	&&.F. disabilitato, .T. abilitato
				l_bCheck = .F.  &&.T. checked
				l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
				l_cBmp = "" &&".\bmp\miobitmap.bmp"
				INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
										(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
				*--- Submenu Area/Aggiornamento
				l_bIsSelectedUpdateOnTimer = .GetLogicProp("bUpdateOnTimer", "OR", .T.)
				l_MenuID = m.l_MenuID + 1
				l_lvlKey = l_lvlGrp+PADL(m.l_g, 3, "0")
				l_lvlUpdate_grp = l_lvlKey+"." &&mi salvo il livello
				l_g = m.l_g + 1
				l_cCaption = cp_Translate("Aggiorna ogni...")
				l_cProgram = ""
				l_bSkip = !m.l_bIsSelectedGadget OR !l_bIsSelectedUpdateOnTimer 	&&.F. disabilitato, .T. abilitato
				l_bCheck = .F.  &&.T. checked
				l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
				l_cBmp = "" &&".\bmp\miobitmap.bmp"
				INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
										(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)									
				*--- Rimuovi tutti
				l_MenuID = m.l_MenuID + 1
				l_lvlKey = l_lvlGrp+PADL(m.l_g, 3, "0")
				l_g = m.l_g + 1
				l_cCaption = cp_Translate("Rimuovi selezionati")
				l_cProgram = cnCmd+"='.RemoveGadgetSelected()'"
				l_bSkip = !m.l_bIsSelectedGadget	&&.F. disabilitato, .T. abilitato
				l_bCheck = .F.  &&.T. checked
				l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
				l_cBmp = "" &&".\bmp\miobitmap.bmp"
				INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
										(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)									
				*--- Separatore
				l_MenuID = m.l_MenuID + 1
				l_lvlKey = l_lvlGrp+PADL(m.l_g, 3, "0")
				l_g = m.l_g + 1		
				INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
										("", 0, "", 4, .T., m.l_MenuID, "", m.l_lvlKey, .F., .F.)
		
				*--- Seleziona tutti
				l_MenuID = m.l_MenuID + 1
				l_lvlKey = l_lvlGrp+PADL(m.l_g, 3, "0")
				l_g = m.l_g + 1
				l_cCaption = cp_Translate("Seleziona tutti")
				l_cProgram = cnCmd+"='.SetAllProp([bSelected], .T.)'"
				l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
				l_bCheck = .F.  &&.T. checked
				l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
				l_cBmp = "" &&".\bmp\miobitmap.bmp"
				INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
										(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
				*--- Deseleziona tutti
				l_MenuID = m.l_MenuID + 1
				l_lvlKey = l_lvlGrp+PADL(m.l_g, 3, "0")
				l_g = m.l_g + 1
				l_cCaption = cp_Translate("Deseleziona tutti")
				l_cProgram = cnCmd+"='.SetAllProp([bSelected], .F.)'"
				l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
				l_bCheck = .F.  &&.T. checked
				l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
				l_cBmp = "" &&".\bmp\miobitmap.bmp"
				INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
										(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)

				*--- Riempio i submenu condivisi tra gadget corrente e area
				*--- Submenu "Temi"
				IF m.l_bIsSelectedGadget
		            tmpCur = Sys(2015)
		            vq_exec("QUERY\GSUT_MGC.VQR", .Null., m.tmpCur)
		            If Used(m.tmpCur)
		            	Local l_t,tmpSubCur,aSubLvlKey
		            	l_t = 0
						l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
						l_bCheck = .F.  &&.T. checked
						l_cBmp = "" &&".\bmp\miobitmap.bmp"            	
		                
		                *--- Cerco eventuali sottomenu e li inserisco (Raggruppo i temi se trovo un '-' nella descrizione)
		                m.tmpSubCur = Sys(2015)
		                Select Distinct Padr(GetWordNum(GCDESCRI,1,'-'),50) as GCDESCRI from (m.tmpCur) Where At('-',GCDESCRI)>0 into cursor (m.tmpSubCur)
		                l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
		                l_cProgram = ""
		                Select(m.tmpSubCur)
		                Locate For 1=1
		                Do While !Eof()
		                    *--- se ho almeno una configurazione attivo la voce "Temi"
		                    l_MenuID = m.l_MenuID + 1
		                    l_t = m.l_t + 1
		                    Dimension aSubLvlKey(m.l_t,2) && matrice con le chiavi di livello dei sottomenu
		                    l_cCaption = Alltrim(&tmpSubCur..GCDESCRI)
		                    aSubLvlKey(m.l_t,1) = Alltrim(m.l_cCaption)
		                    *--- Gadget corrente
		                    l_lvlKey = l_lvlTheme_grp+Padl(m.l_t, 3, "0")
		                    aSubLvlKey(m.l_t,2) = m.l_lvlKey+'.'
		                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
		                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
		                    Select(m.tmpSubCur)
		                    Continue
		                Enddo
		                Use In Select(m.tmpSubCur)
		                
		        		*--- Inserisco le voci dei temi distribuendole nei sottomenu
		                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
		                Select(m.tmpCur)
		                Locate For 1=1
		                Do While !Eof()
		                    *--- se ho almeno una configurazione attivo la voce "Temi"
							l_MenuID = m.l_MenuID + 1					
		                    l_t = m.l_t + 1
		                    l_cCaption = Evl(Alltrim(&tmpCur..GCDESCRI), Alltrim(&tmpCur..GCCODICE))		                
		                    l_i = Iif(At('-',l_cCaption)>0, Ascan(aSubLvlKey,Alltrim(GetWordNum(l_cCaption,1,'-')),1,Alen(aSubLvlKey,1),1,14), 0)
		                    *--- Voce da inserire in un sottomenu
		                    If l_i>0
		                    	m.l_cCaption = Alltrim(SubStr(m.l_cCaption,At('-',m.l_cCaption)+1,Len(m.l_cCaption)))
		                    Endif
							*--- Gadget selezionati
							l_lvlKey = Iif(l_i>0, aSubLvlKey(l_i,2), l_lvlTheme_grp)+Padl(m.l_t, 3, "0")
							l_cProgram = cnCmd+"=[GSUT_BGG(This, 'ApplyTheme', '', .F., '"+&tmpCur..GCCODICE+"')]"
							INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
													(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)					
		                    Select(m.tmpCur)
		                    Continue
		                Enddo
		            Endif
		            Use In Select(m.tmpCur)
		            Release aSubLvlKey
	            EndIf
				
				*--- Submenu "Aggiorna ogni..."
				*--- Se il gadget corrente oppure uno dei selezionati ha bUpdateOnTimer=.T. aggiungo le voci altrimenti sono disabilitate
				IF m.l_bIsSelectedUpdateOnTimer 
					LOCAL l_u
					l_u = 0
					*--- Mai
					l_MenuID = m.l_MenuID + 1
					l_u = m.l_u + 1
					l_cCaption = cp_Translate("Mai")
					l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
					l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
					l_MenuID = m.l_MenuID + 1
					l_lvlKey = l_lvlUpdate_grp+PADL(m.l_u, 3, "0")
					l_cProgram = cnCmd+"='.SetAllProp([nUpdateInterval], 0, .T.)'"
					l_bCheck = .F.
					INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
											(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)										

					*-- 5 minuti
					l_MenuID = m.l_MenuID + 1
					l_u = m.l_u + 1
					l_cCaption = cp_Translate("5 Minuti")
					l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
					l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
					l_MenuID = m.l_MenuID + 1
					l_lvlKey = l_lvlUpdate_grp+PADL(m.l_u, 3, "0")
					l_cProgram = cnCmd+"='.SetAllProp([nUpdateInterval], 5*60, .T.)'"
					l_bCheck = .F.
					INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
											(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)										
					
					*-- 15 minuti
					l_MenuID = m.l_MenuID + 1
					l_u = m.l_u + 1
					l_cCaption = cp_Translate("15 Minuti")
					l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
					l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
					l_MenuID = m.l_MenuID + 1
					l_lvlKey = l_lvlUpdate_grp+PADL(m.l_u, 3, "0")
					l_cProgram = cnCmd+"='.SetAllProp([nUpdateInterval], 15*60, .T.)'"
					l_bCheck = .F.
					INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
											(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)										
					*-- 30 minuti
					l_MenuID = m.l_MenuID + 1
					l_u = m.l_u + 1
					l_cCaption = cp_Translate("30 Minuti")
					l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
					l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
					l_MenuID = m.l_MenuID + 1
					l_lvlKey = l_lvlUpdate_grp+PADL(m.l_u, 3, "0")
					l_cProgram = cnCmd+"='.SetAllProp([nUpdateInterval], 30*60, .T.)'"
					l_bCheck = .F.
					INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
											(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)										
					*-- 60 minuti
					l_MenuID = m.l_MenuID + 1
					l_u = m.l_u + 1
					l_cCaption = cp_Translate("1 Ora")
					l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
					l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
					l_MenuID = m.l_MenuID + 1
					l_lvlKey = l_lvlUpdate_grp+PADL(m.l_u, 3, "0")
					l_cProgram = cnCmd+"='.SetAllProp([nUpdateInterval], 60*60, .T.)'"
					l_bCheck = .F.
					INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
											(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)										
					*-- 240 minuti
					l_MenuID = m.l_MenuID + 1
					l_u = m.l_u + 1
					l_cCaption = cp_Translate("4 Ore")
					l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
					l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
					l_MenuID = m.l_MenuID + 1
					l_lvlKey = l_lvlUpdate_grp+PADL(m.l_u, 3, "0")
					l_cProgram = cnCmd+"='.SetAllProp([nUpdateInterval], 240*60, .T.)'"
					l_bCheck = .F.
					INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
											(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)										
					*-- 480 minuti
					l_MenuID = m.l_MenuID + 1
					l_u = m.l_u + 1
					l_cCaption = cp_Translate("8 Ore")
					l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
					l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
					l_MenuID = m.l_MenuID + 1
					l_lvlKey = l_lvlUpdate_grp+PADL(m.l_u, 3, "0")
					l_cProgram = cnCmd+"='.SetAllProp([nUpdateInterval], 480*60, .T.)'"
					l_bCheck = .F.
					INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
											(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)										
				EndIf	&&.bUpdateOnTimer OR m.l_bIsSelectedUpdateOnTimer

				*--- Separatore
				l_MenuID = m.l_MenuID + 1
				l_lvlKey = '001.'+PADL(m.l_i, 3, "0")
				l_i = m.l_i + 1		
				INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
										("", 0, "", 4, .T., m.l_MenuID, "", m.l_lvlKey, .F., .F.)

				*--- Salva modifiche
				l_MenuID = m.l_MenuID + 1
				l_lvlKey = '001.'+PADL(m.l_i, 3, "0")
				l_i = m.l_i + 1
				l_cCaption = cp_Translate("Salva modifiche")
				l_cProgram = cnCmd+"='.oCharmBar.ExitModify()'"
				l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
				l_bCheck = .F.  &&.T. checked
				l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
				INSERT INTO (m.cCursor) (VOCEMENU, LEVEL, NAMEPROC, DIRECTORY, ENABLED, MENUID, CPBMPNAME, LVLKEY, SKIP, CHECKED) VALUES ;
										(m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)

				loPopupMenu.oPopupMenu.InitMenuFromCursor()
				loPopupMenu.oPopupMenu.cOnClickProc = "ExecScript(NAMEPROC)"   
				loPopupMenu.ShowMenu()     
				
				*--- Azioni
	            If !Empty(&cnCmd)
	                Local cmd
	                cmd = Evaluate(m.cnCmd)
	                &cmd
	            Endif
			EndIf		
        ENDWITH 
        RELEASE cnCmd
        SELECT(m.oldarea)
	Endproc
Enddefine

Define Class cp_MoveGadgetTimer As Timer
    Interval= RefreshTime
    Enabled=.F.
    Procedure Timer()
        *--- Fermo il timer
        This.Enabled = .F.
        *--- Se non ho finito lo riattivo
        This.Enabled = !This.Parent._Arrangegadget()
        *--- Quando i gadget sono tutti nella loro posizione
        *--- verifico se devono essere collassati all'apertura
        If Vartype(oGadgetManager)='O'
        	With oGadgetManager
        	Local l_nActivePage
        	m.l_nActivePage = '*'+Tran(.nActivePage)+'*'
	        If !This.Enabled And !(m.l_nActivePage $ .cPagesInitCollapsed)
	        	*--- Inserisco la pagina nell'elenco: non devo pi� passare di qua
	        	.cPagesInitCollapsed = Left(.cPagesInitCollapsed,Len(.cPagesInitCollapsed)-1) + m.l_nActivePage
	        	For Each l_Gadget in .aGadget
	        		If l_Gadget.Visible And l_Gadget.bInitCollapsed
	        			If !oGadgetManager.OCharmBar.bModifyMode
	        				l_Gadget.oHeader.plusminus_click()
	        			Else
	        				l_Gadget.oHeader.oBtnPlusMinus._Draw("P")
	        				l_Gadget.bExpandedOld = .F.
	        			Endif
	        			l_Gadget.bInitCollapsed = .F.
	        		Endif
	        	Endfor
	        	*--- Riposiziono i gadget (collassano prima che altri abbiano
	        	*--- finito e potrebbero rimanere in posizioni sbagliate)
	        	.Arrangegadget()
	        	*--- Se ho aggiornato la prima pagina (passo di qua solo la prima volta) resetto
	        	*--- il timer per evitare che alcuni gadget saltino l'aggiornamento al primo giro.
	        	*--- (il timer si attiva prima del loro primo aggiornamento)
	        	If .nActivePage==1
	        		.oTimerUpdater.Reset()
	        	Endif
	        Endif
    		Endwith
    	Endif
    Endproc
Enddefine


Define Class GadgetButton As Container
    Width = 150
    Height = 150
    BackStyle = 1
    BorderWidth = 0
    Name = "GadgetButton"
    nBackColor = Rgb(0,79,153)
    nBackColorHover = Rgb(29,112,204)
    nBackColorDown = Rgb(37,120,211)
    Caption = ""
    CpPicture = ""
    bFocused = .F.
    ImgSize = 48
    nAlphaColor = Rgb(255,255,255)

    Add Object oImg As Image With ;
        BackStyle = 0, ;
        Height = 0, ;
        Left = 0, ;
        Top = 0, ;
        Width = 0, ;
        Name = "oImg"


    Add Object oLbl As Label With ;
        Anchor = 14, ;
        Alignment = 2, ;
        BackStyle = 0, ;
        Caption = "", ;
        Height = 17, ;
        Left = 0, ;
        Top = 123, ;
        Width = 150, ;
        Visible = .F., ;
        Name = "oLbl"


    Add Object oBtn As CommandButton With ;
        Top = -1, ;
        Left = -1, ;
        Height = 151, ;
        Width = 151, ;
        Anchor = 15, ;
        Caption = "", ;
        Style = 1, ;
        SpecialEffect = 1, ;
        Name = "oBtn"

    Procedure Init()
        This.ChangeTheme()
    Endproc

    Procedure ChangeTheme()
        With This
            .ToolTipText = .ToolTipText
            .CpPicture= .CpPicture
            .nBackColor = Thisform.BackColor
            .nBackColorHover = RGBAlphaBlending(.nBackColor, This.nAlphaColor, 0.4)
            .nBackColorDown = RGBAlphaBlending(.nBackColor, This.nAlphaColor, 0.2)
            .BackColor = .nBackColor
        Endwith

    Endproc

	Procedure oBtn.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
		*--- Attivo il timer per la visualizzazione del tooltip
		If Vartype(This.Parent.Parent.oContained.oToolTipTimer)='O'
			This.Parent.Parent.oContained.oToolTipTimer.Enabled = .T.
		Else
			This.Parent.Parent.oContained.oToolTipTimer = Createobject("ToolTipGadgetTimer")
		Endif

    Procedure oBtn.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        Do Case
            Case m.nButton = 0
                This.Parent.BackColor = This.Parent.nBackColorHover
            Case m.nButton = 1
                This.Parent.BackColor = This.Parent.nBackColorDown
        Endcase

    Procedure oBtn.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        Do Case
            Case m.nButton = 1
                This.Parent.BackColor = This.Parent.nBackColorDown
        Endcase

    Procedure oBtn.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        If m.nButton = 0 And !This.Parent.bFocused
            This.Parent.BackColor = This.Parent.nBackColor
        Endif
		*--- Disattivo il timer per la visualizzazione del tooltip
		If Vartype(This.Parent.Parent.oContained.oToolTipTimer)='O'
			This.Parent.Parent.oContained.oToolTipTimer.Enabled = .F.
		Endif
		This.Parent.Parent.oContained.oToolTipTimer = .null.
		
	Procedure Click()
        *--- Nascondo l'eventuale ToolTip
        If Vartype(oGadgetManager)='O' And PemStatus(oGadgetManager,'oBalloon',5)
        	oGadgetManager.oBalloon.ctlHide(2)
        	oGadgetManager.oBalloon.ctlVisible = .F.
        Endif
        DoDefault()

    Procedure oBtn.Click()
        This.Parent.Click()

    Procedure CpPicture_Assign(xValue)
        With This
            .oImg.Move(0,0,0,0)

            Local cFileBmp, l_Dim
            l_Dim= .ImgSize
            If Vartype(i_ThemesManager)=='O'
                .CpPicture = i_ThemesManager.PutBmp(.oImg,m.xValue, l_Dim)
            Else
                .oImg.Picture = m.xValue
                .CpPicture = m.xValue
            Endif
            .oImg.Stretch = 0
            *---Centro l'immagine
            .oImg.Move(Bitrshift(.Width-.oImg.Width,1), Bitrshift(.Height-.oImg.Height,1))
        Endwith
    Endproc

    Procedure Caption_Assign(xValue)
        This.oLbl.Caption = m.xValue
        This.Caption = m.xValue
    Endproc

    Procedure oBtn.GotFocus()
        This.Parent.BackColor = This.Parent.nBackColorHover
        This.Parent.bFocused = .T.
    Endproc

    Procedure oBtn.LostFocus()
        This.Parent.BackColor = This.Parent.nBackColor
        This.Parent.bFocused = .F.
    Endproc

    Procedure ToolTipText_Assign(xValue)
        This.oBtn.ToolTipText = m.xValue
        This.ToolTipText = m.xValue
    Endproc
Enddefine

*--- Timer per l'aggiornamento automatico dei gadget
*--- Il timer ogni 5 min verifica se i gadget devono essere aggiornati
*--- effettuando la differenza tra datatime corrente e datatime ultimo aggiornamento
Define Class cp_UpdateGadgetTimer As Timer
    Enabled = .F.
    Interval = 1000 * 60 * 5	&& 5 min

    Procedure Init()
        With This
            .Interval = Iif(Type("i_nUpdateGadgetInterval")='N', i_nUpdateGadgetInterval, .Interval)
        Endwith
    Endproc

    Procedure Timer()
        Local l_gadget, l_OldArea
        With This.Parent
            If .bUpdateOnTimer And .Visible And Lower(Alltrim(cHomeDir))==Lower(Alltrim(Sys(5)+Curdir()))
                *--- durante l'aggiornamento fermo il timer
                This.Enabled = .F.
                For Each l_gadget In .aGadget
                    *--- Se non visibile non aggiorno il gadget
                    If (.bUpdateHiddenGadget Or ((.bFullScreen Or m.l_gadget.Left <= .nWidthCollapse) And m.l_gadget.nPage=.nActivePage)) And ;
                            m.l_gadget.bUpdateOnTimer And ;
                            m.l_gadget.nUpdateInterval#0 And (Datetime() - m.l_gadget.tLastUpdate >= m.l_gadget.nUpdateInterval)

                        m.l_gadget.tLastUpdate = Datetime() &&Aggiorno la data di ultimo aggiornamento
                        *--- sicurezza per cambio cursore
                        l_OldArea = Select()
                        m.l_gadget.NotifyEvent("GadgetOnTimer")
                        *--- sicurezza per cambio cursore
                        Select(m.l_OldArea)
                    Endif
                Endfor
                *--- riattivo il timer
                This.Enabled = .T.
            Endif	&& bUpdateOnTimer
        Endwith
        l_gadget = .Null.
    Endproc

Enddefine

Define Class cp_GadgetTabItem As Label
    BorderStyle=1
    FontName = "Open Sans"
    FontSize = 10
    Alignment = 2
    BorderStyle = 0
    BackStyle = 0
    Caption = ""
    Height = 20
    MousePointer = 15
    Top = 4
    ForeColor = Rgb(255,255,255)
    nPage = 0
    nPosition = 0
    cKey = ""
    DragIcon = i_cBmpPath+"size.cur"

    Procedure Init(nPage, cKey)
        With This
            .nPage = m.nPage
            .nPosition = m.nPage
            .cKey = Upper(Alltrim(m.cKey))
            *--- Colore pi� leggibile dato il colore di sfondo della ogadgetmanager
            .ForeColor = GetBestForeColor(.Parent.Parent.BackColor)
        Endwith
    Endproc

    Procedure Caption_Assign(xValue)
        With This
            .Caption = m.xValue
            .Width = cp_LabelWidth2(m.xValue, .FontName, .FontSize) + 10
        Endwith
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This
            .FontUnderline= (.BorderStyle#1)
        Endwith
    Endproc


    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This
            .FontUnderline=.F.
        Endwith
    Endproc

    Procedure Click
        Local ctl
        With This
            For Each ctl In .Parent.Controls
                m.ctl.BorderStyle=0
            Next
            .BorderStyle=1
            .FontUnderline=.F.
            .Parent._Click(.nPage)
        Endwith
    Endproc

    Procedure MouseMove(nButton, nShift, nXCoord, nYCoord)
        With This
            If m.nButton=1 And m.nShift=2 And Vartype(.Parent.Parent.oCharmBar)=='O' And .Parent.Parent.oCharmBar.bModifyMode And .Parent.nPageCount>1
            	*--- Permetto l'attivazione della finestra
            	.Parent.Parent.bNoActivate = .F.
                .Drag(1)
                *--- Blocco l'attivazione della finestra
                .Parent.Parent.bNoActivate = .T.
            Endif
        Endwith
    Endproc

    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        With This
            If Vartype(m.oSource)="O" And Lower(m.oSource.Class)=="cp_gadgettabitem" And m.oSource.Name<>.Name
                Do Case
                    Case nState=0   && Enter
                        m.oSource.FontBold = .T.
                        .FontBold = .T.
                    Case nState=1   && Leave
                        m.oSource.FontBold = .F.
                        .FontBold = .F.
                Endcase
            Endif
        Endwith
    Endproc

    Proc DragDrop(oSource,nXCoord,nYCoord)
        With This.Parent.Parent
            If Vartype(m.oSource)="O" And Lower(m.oSource.Class)=="cp_gadgettabitem"
                m.oSource.FontBold = .F.
                If m.oSource.Name<>.Name
                    This.FontBold = .F.
                    .aOrderPage(m.oSource.nPosition) = This.cKey
                    .aOrderPage(This.nPosition) = m.oSource.cKey
                    .oTabPage.Ordering()
                Endif
            Endif
        Endwith
    Endproc
Enddefine

#Define INIT_LEFT 10

Define Class cp_GadgetTab As Container
    BackStyle=0
    BorderWidth=0
    nPageCount = 0
    nLeft = INIT_LEFT
    nTop = 4
    nOffset = 5
    w_NGADGRP = 0

    Procedure AddPage(cTitle, cKey)
        With This
            .nPageCount = .nPageCount + 1
            l_ItemName = "Tab_"+Alltrim(Str(.nPageCount))
            .AddObject(l_ItemName, "cp_GadgetTabItem", .nPageCount, m.cKey)
            .&l_ItemName..Move(.nLeft, .nTop, 40)
            .&l_ItemName..Caption = Proper(Alltrim(m.cTitle))
            .&l_ItemName..Visible=.T.
            *--- Se prima pagina disegno il bordo
            If .nPageCount = 1
                .&l_ItemName..BorderStyle=1
            Endif
            .nLeft = .&l_ItemName..Left+.&l_ItemName..Width + .nOffset
            Dimension .Parent.aOrderPage(.nPageCount)
            .Parent.aOrderPage(.nPageCount) = m.cKey
        Endwith
    Endproc

    Procedure _Click(nPage)
        This.Parent.nActivePage = m.nPage
    Endproc

    Procedure HightLightPage(nPage)
        Local ctl
        With This
            For Each ctl In .Controls
                m.ctl.BorderStyle= Iif(m.ctl.nPage # m.nPage, 0, 1)
                m.ctl.FontUnderline=.F.
            Next
        Endwith
    Endproc

    *--- Restituisce il nome del Tab data la chiave della Pagina
    *--- Se non trova la chiave restituisce una stringa vuota
    Procedure getNameByKey(cKey)
        Local ctl, rKey
        m.rKey = ""
        With This
            For Each ctl In .Controls
                If m.ctl.cKey == Upper(Alltrim(m.cKey))
                    m.rKey = m.ctl.Name
                    Exit
                Endif
            Endfor
        Endwith
        Return m.rKey
    Endproc

    *--- Ordina le label dei Tab
    *--- bActive = .t. > La prima pagina secondo l'ordinamente viene anche attivata
    *--- bActive = .t. > La pagina attiva al momento dell'ordinamento non viene cambiata
    Proc Ordering(bActive)
        Local l_ItemName,l_Page,nCleared,nDiscarded
        With This
            If Vartype(.Parent.aOrderPage)=='C'
                .nLeft = INIT_LEFT
                m.nDiscarded = 0
                m.nCleared = 0
                For l_i=1 To Alen(.Parent.aOrderPage)
                    m.l_Page = .Parent.aOrderPage(m.l_i-m.nDiscarded)
                    m.l_ItemName = .getNameByKey(m.l_Page)
                    If !Empty(m.l_ItemName)
                        .&l_ItemName..nPosition = m.l_i-m.nDiscarded
                        If !m.bActive
                            cp_SingleTransition(.&l_ItemName, "Left", .nLeft, "N", 200, "Linear")
                        Else
                            .&l_ItemName..Left = .nLeft
                        Endif
                        *--- Se prima pagina disegno il bordo
                        If m.l_i==1 And m.bActive
                            .&l_ItemName..Click()
                        Endif
                        .nLeft = .nLeft +.&l_ItemName..Width + .nOffset
                    Else
                        *--- Sposto la pagina in fondo all'array di ordinamento
                        For l_j=(m.l_i-m.nDiscarded) To Alen(.Parent.aOrderPage)-1
                            .Parent.aOrderPage(l_j) = .Parent.aOrderPage(l_j+1)
                        Endfor
                        .Parent.aOrderPage(l_j) = m.l_Page
                        m.nDiscarded = m.nDiscarded + 1 && al fine dell'ordinamento questa pagina va comunque scartata
                        *--- Verifico se la pagina � in uso per gadget aziendali
                        *--- altrimenti la marco come da rimuovere
                        l_Macro = [GSUT_BGG(This, "PageUse", m.l_Page)]
                        &l_Macro
                        If .w_NGADGRP==0
                            m.nCleared = m.nCleared + 1
                        Endif
                    Endif
                Endfor
                Dimension .Parent.aOrderPage(Alen(.Parent.aOrderPage)-m.nCleared)
            Else
                Dimension .Parent.aOrderPage(.nPageCount)
                For l_i=1 To .nPageCount
                    l_ItemName = "Tab_"+Trans(m.l_i)
                    .Parent.aOrderPage(m.l_i) = .&l_ItemName..cKey
                Endfor
            Endif
        Endwith
    Endproc

    *--- Colore dei tab, se c'� troppo contrasto devo cambiare colore ai tab
    Procedure ChangeTheme()
        Local ctl
        With This
            For Each ctl In .Controls
                ctl.ForeColor = GetBestForeColor(.Parent.BackColor)
            Next
        Endwith
    Endproc

    *--- Simulo un padre CPR
    Proc mCalc(cVal)
        Return

    Proc mCalledBatchEnd(i_bUpb)
        Return
Enddefine
