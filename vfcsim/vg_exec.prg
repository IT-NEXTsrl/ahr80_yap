* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VG_EXEC
* Ver      : 2.3.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Samuele Masetto
* Data creazione: 29/06/98
* Aggiornato il : 29/06/98
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Interprete di grafici
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam cModel,oParentObject,cFile
Local i_cCursor,i_cGraph
If Type('cModel')<>'L'
    If Lower(Right(cModel,4))='.vgr'
        cModel=Left(cModel,Len(cModel)-4)
    Endif
    If !cp_IsStdFile(cModel,'vgr')
        cp_ErrorMsg(cp_MsgFormat(MSG_FILE__NOT_FOUND_QM,cModel),16,'',.f.)
        Return(.Null.)
    Endif
Else
    cModel=Iif(At('.vqr',Lower(cFile))=0,cFile,Left(cFile,Len(cFile)-4))
Endif
If Type('cFile')='C'
    If cp_IsPFile(cFile+Iif(At('.vqr',Lower(cFile))=0,'.vqr',''))
        i_cCursor='__tmp__'
        If Type("i_bFox26")='L'
            cpi_qry(cFile,i_cCursor)
        Else
            vq_exec(cFile,oParentObject,i_cCursor)
        Endif
    Else
        i_cCursor=cFile
    Endif
Else
    i_cCursor='__tmp__'
Endif
vg_build(i_cCursor,cModel,.F.)
If Used(i_cCursor)
    Select (i_cCursor)
    Use
Endif
Return
