* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: loadconfig3                                                     *
*              LoadConfig                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-18                                                      *
* Last revis.: 2015-01-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tloadconfig3",oParentObject)
return(i_retval)

define class tloadconfig3 as StdBatch
  * --- Local variables
  w_OBJ = .NULL.
  w_NCURS = space(10)
  w_CICLO = 0
  w_DEFAULTPRIN = 0
  w_PEDEFINITA = space(10)
  w_FOUND = .f.
  w_OA = 0
  w_i = 0
  w_WORKSTATION = space(10)
  w_RptTxt_Search = space(10)
  w_tempStamp = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_DEFAULTPRIN = 1
    this.w_OBJ = this.oparentobject
    * --- Carica la combo delle stampanti
    APRINTERS(Stampanti)
    if NOT type("Stampanti")="U"
      this.w_CICLO = 1
      if this.w_OBJ.oTxt_Device.NumberOfElements=0
        this.w_OBJ.oTxt_Device.removeitem(1)     
        this.w_PEDEFINITA = UPPER( alltrim ( SET("printer to" , 2 )))
        do while this.w_CICLO <= alen(Stampanti,1)
          this.w_OBJ.oTxt_Device.addItem(UPPER (iif( left(Stampanti(this.w_ciclo,1),1)="\","\"+Stampanti(this.w_ciclo,1),Stampanti(this.w_ciclo,1))))     
          if UPPER(alltrim(NVL (Stampanti(this.w_ciclo,1),"")))=this.w_PEDEFINITA
            this.w_DEFAULTPRIN = this.w_ciclo
          endif
          this.w_CICLO = this.w_CICLO + 1
        enddo
        this.w_OBJ.oTxt_Device.NumberOfElements=alen(Stampanti,1)
        this.w_OBJ.oTxt_Device.listindex = this.w_DEFAULTPRIN
        this.w_OBJ.w_DEVICE = Stampanti(this.w_DEFAULTPRIN,1) 
        this.w_OBJ.prStamp = this.w_OBJ.w_DEVICE
      endif
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Zucchetti Aulla Inizio - Multireport
    if this.w_OBJ.cNomeReport.GetNumReport()>1
      this.w_OBJ.cComment = cp_translate(MSG_PRINT_SYSTEM_MR)
      this.w_OBJ.Caption = this.w_OBJ.cComment
    endif
    * --- *--- Fascicolazione
    *       *--- Per DOCM --- Verifico che non sia gi� istanziato l'oggetto. Questo per ovviare alla cancellazione dei cursori
    *       *--- all'uscita dall'esecuzione del processo documentale  --- 
    this.w_OBJ.oSplitReport = CREATEOBJECT("MultiReport")
    if !SplitReport(this,this.w_OBJ.cNomeReport, this.w_OBJ.oSplitReport)
      this.w_OBJ.release()     
    endif
     oRepPlite= UPPER(this.w_OBJ.oSplitReport.FirstReport())
    if this.w_OBJ.w_PRODOCM<>"S" AND this.w_OBJ.cNomeReport.nNumCopy>0
      * --- Se � stato lanciato il Document managment il numero di copie � gi� inizializzato 
      this.w_OBJ.prCopie = this.w_OBJ.cNomeReport.nNumCopy
      this.w_OBJ.w_Txt_Copies = this.w_OBJ.cNomeReport.nNumCopy
    endif
    * --- Per i report solo testo applica regole standard  
    if this.w_OBJ.cNomeReport.GetText(this.w_OBJ.nIndexReport)
      this.w_OBJ.cTipoReport = "S"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- * Zucchetti Aulla Inizio
      *          * Gestisco anche anteprima di stampa
      * --- Zucchetti Aulla Fine
    else
      this.w_OBJ.cTipoReport = ""
      if this.w_OBJ.i_FDFFile
      else
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- n ogni caso, la mail ed il fax pu� essere inviato solo se c'� il report
      endif
    endif
    * --- Zucchetti aulla Inizio - Integrazione Archeasy
    this.w_OBJ.oOpt_File.Click()     
    * --- * --- Zucchetti Aulla Fine
    *     *--- Zucchetti Aulla Inizio - Traduzione report
    *       * Settaggi per lingue
    cp_CHPFUN (this.w_OBJ,"SettaggiLingue")
    if this.w_OBJ.oOpt_File.listcount>=1
      this.w_OBJ.oOpt_File.listindex = 1
    endif
    * --- Zucchetti Aulla Fine - Traduzione report
    this.w_OBJ.w_PRODOCM = "S"
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_NCURS = sys(2015)
    this.w_OA = select()
    this.w_WORKSTATION = LEFT(SYS(0), RAT(" # ",SYS(0))-1)
    * --- *--- Multireport
    *         *--- Se � presente una stampa principale solo testo, leggo l'associazione di quel report
    if !this.w_OBJ.cNomeReport.IsEmptyCursor()
      this.w_i = 0
      do while this.w_i<>-1
        l_i=this.w_OBJ.cNomeReport.NextReport(this.w_i) 
        this.w_i = this.w_OBJ.cNomeReport.NextReport(this.w_i) 
        if this.w_i>0 AND this.w_OBJ.cNomeReport.GetText(this.w_i)
          this.w_OBJ.nIndexReport = this.w_i
          this.w_OBJ.w_tReport = "S"
          EXIT
        endif
      enddo
    endif
    this.w_RptTxt_Search = this.w_OBJ.cNomeReport.GetReport(this.w_OBJ.nIndexReport)
    * --- --
    * --- Multireport
    if i_ServerConn[1,2]<>0
      cp_sql(i_ServerConn[1,2],"select * from cpusrrep where repasso="+cp_ToStr(IIF(this.w_OBJ.w_treport="S",this.w_RptTxt_Search+".FXP",this.w_OBJ.cNomeReport.FirstReport()+".FRX"))+" and (aziasso="+cp_ToStr(i_codazi)+" or aziasso='          ' or aziasso is null) and (wstasso="+cp_ToStr(this.w_workstation)+" or wstasso='          ' or wstasso is null) and (usrasso="+cp_ToStr(i_codute)+" or usrasso=0 or usrasso is null) order by wstasso desc,aziasso desc, usrasso desc",this.w_nCurs)
    else
      select * from cpusrrep where repasso=IIF(this.w_OBJ.w_treport="S",this.w_RptTxt_Search+".FXP",this.w_OBJ.cNomeReport.FirstReport()+".FRX") AND (TRIM(i_codazi)==TRIM(aziasso) OR EMPTY(aziasso)) AND (TRIM(wstasso)==TRIM(this.w_workstation) OR EMPTY(wstasso)) AND (usrasso=i_codute or usrasso=0) ORDER BY wstasso desc,aziasso desc, usrasso desc into curs (this.w_nCurs)
    endif
    if used(this.w_nCurs)
      * --- *--- Ordino per utente e azienda in modo da prendere sempre prima le associazioni con utente e/o azienda valorizzate 
      *           *--- indipendentemente dall'ordine di caricamento; ordino il cursore fox per evitare problemi con DB2/Oracle e il verso asc o desc
      SELECT * FROM (this.w_nCurs) ORDER BY usrasso DESC, aziasso DESC INTO CURSOR (this.w_nCurs)
      * --- ** LA PART DI INIZIALIZZAZIONE DELLA STAMPANTE w_DEVICE E FATTA DALLA FUNZIONE SETALLPRINTER DEL MULTIREPORT
      if not(eof())
        this.w_FOUND = .F.
        go top
        this.w_OBJ.cModSst = trim(MODASSO)
      endif
      use
    else
      cp_msg(cp_Translate(MSG_ERROR_OPENING_PRINTING_TABLE))
    endif
    select (this.w_oa)
    * --- *--- Multireport
    *         *--- Ricerco modelli
    this.w_OBJ.cNomeReport.CheckModels()     
    * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli di visibilit� (parte 1) - Non interferisce con propriet� enabled
    this.w_OBJ.pdProcesso = this.w_OBJ.cNomeReport.CountDOCMReportsNotSkipped()>0
    if NVL (g_DOCM," ")="S" AND this.w_OBJ.pdProcesso AND TYPE("this.w_OBJ.oPDPrgBar") <> "O"
      this.w_OBJ.opgfrm.page1.opag.ADDOBJECT("oPDPrgBar","oProgressBar")     
      this.w_OBJ.oPDPrgBar = this.w_OBJ.opgfrm.page1.opag.oPDPrgBar
      this.w_OBJ.oPDPrgBar.Top = 147
      this.w_OBJ.oPDPrgBar.Left = 195
      this.w_OBJ.oPDPrgBar.Height = 20
      this.w_OBJ.oPDPrgBar.Width = 339
      this.w_OBJ.oPDPrgBar.Min = 0
      this.w_OBJ.oPDPrgBar.Max = 100
      this.w_OBJ.oPDPrgBar.Visible = .T.
      * --- label1
      this.w_OBJ.opgfrm.page1.opag.ADDOBJECT("oPDLbl1","label")     
      this.w_OBJ.oPDLbl1 = this.w_OBJ.opgfrm.page1.opag.oPDLbl1
      this.w_OBJ.oPDLbl1.Caption = "0%"
      this.w_OBJ.oPDLbl1.Top = 168
      this.w_OBJ.oPDLbl1.Left = 190
      this.w_OBJ.oPDLbl1.Height = 17
      this.w_OBJ.oPDLbl1.Width = 30
      this.w_OBJ.oPDLbl1.FontSize = 8
      this.w_OBJ.oPDLbl1.Visible = .T.
      this.w_OBJ.oPDLbl1.Alignment = 2
      this.w_OBJ.oPDLbl1.BackStyle = 0
      * --- label2
      this.w_OBJ.opgfrm.page1.opag.ADDOBJECT("oPDLbl5","label")     
      this.w_OBJ.oPDLbl5 = this.w_OBJ.opgfrm.page1.opag.oPDLbl5
      this.w_OBJ.oPDLbl5.Caption = "25%"
      this.w_OBJ.oPDLbl5.Top = 168
      this.w_OBJ.oPDLbl5.Left = 258
      this.w_OBJ.oPDLbl5.Height = 17
      this.w_OBJ.oPDLbl5.Width = 30
      this.w_OBJ.oPDLbl5.FontSize = 8
      this.w_OBJ.oPDLbl5.Visible = .T.
      this.w_OBJ.oPDLbl5.Alignment = 2
      this.w_OBJ.oPDLbl5.BackStyle = 0
      * --- label3
      this.w_OBJ.opgfrm.page1.opag.ADDOBJECT("oPDLbl2","label")     
      this.w_OBJ.oPDLbl2 = this.w_OBJ.opgfrm.page1.opag.oPDLbl2
      this.w_OBJ.oPDLbl2.Caption = "50%"
      this.w_OBJ.oPDLbl2.Top = 168
      this.w_OBJ.oPDLbl2.Left = 338
      this.w_OBJ.oPDLbl2.Height = 17
      this.w_OBJ.oPDLbl2.Width = 30
      this.w_OBJ.oPDLbl2.FontSize = 8
      this.w_OBJ.oPDLbl2.Visible = .T.
      this.w_OBJ.oPDLbl2.Alignment = 2
      this.w_OBJ.oPDLbl2.BackStyle = 0
      * --- label4
      this.w_OBJ.opgfrm.page1.opag.ADDOBJECT("oPDLbl6","label")     
      this.w_OBJ.oPDLbl6 = this.w_OBJ.opgfrm.page1.opag.oPDLbl6
      this.w_OBJ.oPDLbl6.Caption = "75%"
      this.w_OBJ.oPDLbl6.Top = 168
      this.w_OBJ.oPDLbl6.Left = 418
      this.w_OBJ.oPDLbl6.Height = 17
      this.w_OBJ.oPDLbl6.Width = 30
      this.w_OBJ.oPDLbl6.FontSize = 8
      this.w_OBJ.oPDLbl6.Visible = .T.
      this.w_OBJ.oPDLbl6.Alignment = 2
      this.w_OBJ.oPDLbl6.BackStyle = 0
      * --- label5
      this.w_OBJ.opgfrm.page1.opag.ADDOBJECT("oPDLbl3","label")     
      this.w_OBJ.oPDLbl3 = this.w_OBJ.opgfrm.page1.opag.oPDLbl3
      this.w_OBJ.oPDLbl3.Caption = "100%"
      this.w_OBJ.oPDLbl3.Top = 168
      this.w_OBJ.oPDLbl3.Left = 503
      this.w_OBJ.oPDLbl3.Height = 17
      this.w_OBJ.oPDLbl3.Width = 30
      this.w_OBJ.oPDLbl3.FontSize = 8
      this.w_OBJ.oPDLbl3.Visible = .T.
      this.w_OBJ.oPDLbl3.Alignment = 2
      this.w_OBJ.oPDLbl3.BackStyle = 0
    endif
    if NVL (g_DOCM," ")="S" AND NOT this.w_OBJ.pdProcesso AND vartype (this.w_OBJ.oPDPrgBar)="O"
      * --- Se esiste una progress bar da nascondere 
      this.w_OBJ.oPDPrgBar.visible = this.w_OBJ.pdProcesso
      this.w_OBJ.oPDLbl1.visible = this.w_OBJ.pdProcesso
      this.w_OBJ.oPDLbl2.visible = this.w_OBJ.pdProcesso
      this.w_OBJ.oPDLbl3.visible = this.w_OBJ.pdProcesso
      this.w_OBJ.oPDLbl5.visible = this.w_OBJ.pdProcesso
      this.w_OBJ.oPDLbl6.visible = this.w_OBJ.pdProcesso
    endif
    this.w_OBJ.oPDLbl4.visible = this.w_OBJ.pdProcesso
    * --- Bottone Anteprima e ESC
    cp_CHPFUN ( this.w_OBJ ,"SettaggiLingue")
    * --- Zucchetti Aulla Fine
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
