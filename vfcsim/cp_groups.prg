* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_groups                                                       *
*              Gestione gruppi                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-09-25                                                      *
* Last revis.: 2016-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_groups",oParentObject))

* --- Class definition
define class tcp_groups as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 341
  Height = 321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-10-07"
  HelpContextID=208197334
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_groups"
  cComment = "Gestione gruppi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ISADMIN = .F.
  w_GCODE = 0
  w_GNAME = space(30)
  w_GRPTYPE = space(1)
  w_NEW = .F.
  w_HASGRPTYPE = .F.
  w_NCODE = space(4)
  w_user = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_groupsPag1","cp_groups",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGCODE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_user = this.oPgFrm.Pages(1).oPag.user
    DoDefault()
    proc Destroy()
      this.w_user = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ISADMIN=.f.
      .w_GCODE=0
      .w_GNAME=space(30)
      .w_GRPTYPE=space(1)
      .w_NEW=.f.
      .w_HASGRPTYPE=.f.
      .w_NCODE=space(4)
      .w_ISADMIN=oParentObject.w_ISADMIN
      .w_GCODE=oParentObject.w_GCODE
      .w_GNAME=oParentObject.w_GNAME
      .w_GRPTYPE=oParentObject.w_GRPTYPE
          .DoRTCalc(1,3,.f.)
        .w_GRPTYPE = 'G'
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_CODE))
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_NAME))
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
      .oPgFrm.Page1.oPag.user.Calculate()
          .DoRTCalc(5,6,.f.)
        .w_NCODE = .w_GCODE
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate(cp_translate(MSG_TYPE))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ISADMIN=.w_ISADMIN
      .oParentObject.w_GCODE=.w_GCODE
      .oParentObject.w_GNAME=.w_GNAME
      .oParentObject.w_GRPTYPE=.w_GRPTYPE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_CODE))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_NAME))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.user.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(cp_translate(MSG_TYPE))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_CODE))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_NAME))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.user.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(cp_translate(MSG_TYPE))
    endwith
  return

  proc Calculate_HZCVSRCMAW()
    with this
          * --- Init
          cp_grpusr_b(this;
              ,'GrpInit';
             )
    endwith
  endproc
  proc Calculate_SEDUAVQAME()
    with this
          * --- Popola cursore
          cp_grpusr_b(this;
              ,'GrpCursor';
             )
    endwith
  endproc
  proc Calculate_NZIYQZUKCS()
    with this
          * --- Salvataggio cursore
          cp_grpusr_b(this;
              ,'GrpBeforeQ';
             )
    endwith
  endproc
  proc Calculate_PRIGTSDEYC()
    with this
          * --- Ripristina cursore
          cp_grpusr_b(this;
              ,'GrpAfterQ';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGCODE_1_2.enabled = this.oPgFrm.Page1.oPag.oGCODE_1_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.user.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_HZCVSRCMAW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_SEDUAVQAME()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_user before query")
          .Calculate_NZIYQZUKCS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_user after query")
          .Calculate_PRIGTSDEYC()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGCODE_1_2.value==this.w_GCODE)
      this.oPgFrm.Page1.oPag.oGCODE_1_2.value=this.w_GCODE
    endif
    if not(this.oPgFrm.Page1.oPag.oGNAME_1_4.value==this.w_GNAME)
      this.oPgFrm.Page1.oPag.oGNAME_1_4.value=this.w_GNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oGRPTYPE_1_6.RadioValue()==this.w_GRPTYPE)
      this.oPgFrm.Page1.oPag.oGRPTYPE_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_groupsPag1 as StdContainer
  Width  = 337
  height = 321
  stdWidth  = 337
  stdheight = 321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGCODE_1_2 as StdField with uid="SXNJAFAEIP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GCODE", cQueryName = "GCODE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 168012858,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=14, Top=22, cSayPict='"9999"', cGetPict='"9999"'

  func oGCODE_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NEW)
    endwith
   endif
  endfunc

  add object oGNAME_1_4 as StdField with uid="SWBDBLLRUV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GNAME", cQueryName = "GNAME",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome gruppo",;
    HelpContextID = 159448122,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=14, Top=65, InputMask=replicate('X',30)


  add object oGRPTYPE_1_6 as StdCombo with uid="NQCQHGPUJA",rtseq=4,rtrep=.f.,left=87,top=22,width=83,height=22;
    , ToolTipText = "Tipo gruppo";
    , HelpContextID = 83999733;
    , cFormVar="w_GRPTYPE",RowSource=""+"Gruppo,"+"Ruolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGRPTYPE_1_6.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oGRPTYPE_1_6.GetRadio()
    this.Parent.oContained.w_GRPTYPE = this.RadioValue()
    return .t.
  endfunc

  func oGRPTYPE_1_6.SetRadio()
    this.Parent.oContained.w_GRPTYPE=trim(this.Parent.oContained.w_GRPTYPE)
    this.value = ;
      iif(this.Parent.oContained.w_GRPTYPE=='G',1,;
      iif(this.Parent.oContained.w_GRPTYPE=='R',2,;
      0))
  endfunc


  add object oBtn_1_7 as StdButton with uid="OQBIDCNZRB",left=169, top=289, width=77,height=22,;
    caption="Ok", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 208657350;
    , Caption=MSG_OK_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        cp_grpusr_b(this.Parent.oContained,"GrpOk")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="IMQMRMOAOO",left=253, top=289, width=77,height=22,;
    caption="Annulla", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 265421273;
    , Caption=MSG_CANCEL_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_9 as cp_setCtrlObjprop with uid="STMQUKDDXO",left=351, top=-19, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Codice",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 103369670


  add object oObj_1_10 as cp_setCtrlObjprop with uid="SDWMSALZFF",left=351, top=8, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nome",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 103369670


  add object oObj_1_13 as cp_runprogram with uid="GDOPCYPOZJ",left=5, top=335, width=330,height=26,;
    caption='CP_GRPUSR_B(GRPMODCODE)',;
   bGlobalFont=.t.,;
    prg="cp_grpusr_b('GrpModCode')",;
    cEvent = "w_GCODE Changed",;
    nPag=1;
    , HelpContextID = 163775596


  add object user as cp_szoombox with uid="LIELHMYYLI",left=5, top=88, width=325,height=197,;
    caption='user',;
   bGlobalFont=.t.,;
    bNoZoomGridShape=.f.,cTable="cpusers",cZoomOnZoom="",cZoomFile="cpusers",bOptions=.f.,bAdvOptions=.t.,bQueryOnDblClick=.t.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202014266


  add object oObj_1_21 as cp_setCtrlObjprop with uid="CPGLLJVDKQ",left=435, top=-19, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tipo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 103369670

  add object oStr_1_3 as StdString with uid="HYOKPDYDWJ",Visible=.t., Left=14, Top=4,;
    Alignment=0, Width=42, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="LKAHYWDTZG",Visible=.t., Left=14, Top=46,;
    Alignment=0, Width=37, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="BYZGCSLYWW",Visible=.t., Left=87, Top=4,;
    Alignment=0, Width=27, Height=18,;
    Caption="Tipo"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_groups','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
