* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_users                                                        *
*              Gestione utenti                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-09-24                                                      *
* Last revis.: 2016-10-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_users",oParentObject))

* --- Class definition
define class tcp_users as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 426
  Height = 429
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-10-11"
  HelpContextID=120932765
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  _IDX = 0
  POL_SIC_IDX = 0
  POL_UTE_IDX = 0
  cPrg = "cp_users"
  cComment = "Gestione utenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ISADMIN = .F.
  w_CHANGE = .F.
  w_UTEADMIN = .F.
  w_CODE = 0
  w_LOGIN = space(20)
  w_NAME = space(20)
  w_PWDO = space(15)
  w_CHANGEPWD = space(1)
  w_PWD = space(15)
  w_PWDC = space(15)
  w_RIGHTS = 0
  w_LANGSYS = space(3)
  w_DISABLE = space(1)
  w_NCODE = 0
  w_OLDNAME = space(30)
  w_NEW = .F.
  w_HASENABLED = .F.
  w_POLSIC = space(3)
  w_ATTIVO = space(1)
  w_NUMLIM = 0
  w_COMPLEX = 0
  w_CODUTE = space(1)
  w_NORIP = space(1)
  w_NORIPA = space(1)
  w_MAXPWD = 0
  w_NUMLIA = 0
  w_COMPLEA = 0
  w_VALIDPWD = 0
  w_pParam = .F.
  w_USERS = .F.
  w_group = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_usersPag1","cp_users",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_group = this.oPgFrm.Pages(1).oPag.group
    DoDefault()
    proc Destroy()
      this.w_group = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='POL_SIC'
    this.cWorkTables[2]='POL_UTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        cp_grpusr_b(this,"UsrOk")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ISADMIN=.f.
      .w_CHANGE=.f.
      .w_UTEADMIN=.f.
      .w_CODE=0
      .w_LOGIN=space(20)
      .w_NAME=space(20)
      .w_PWDO=space(15)
      .w_CHANGEPWD=space(1)
      .w_PWD=space(15)
      .w_PWDC=space(15)
      .w_RIGHTS=0
      .w_LANGSYS=space(3)
      .w_DISABLE=space(1)
      .w_NCODE=0
      .w_OLDNAME=space(30)
      .w_NEW=.f.
      .w_HASENABLED=.f.
      .w_POLSIC=space(3)
      .w_ATTIVO=space(1)
      .w_NUMLIM=0
      .w_COMPLEX=0
      .w_CODUTE=space(1)
      .w_NORIP=space(1)
      .w_NORIPA=space(1)
      .w_MAXPWD=0
      .w_NUMLIA=0
      .w_COMPLEA=0
      .w_VALIDPWD=0
      .w_pParam=.f.
      .w_USERS=.f.
      .w_CHANGE=oParentObject.w_CHANGE
      .w_CODE=oParentObject.w_CODE
      .w_NAME=oParentObject.w_NAME
      .w_pParam=oParentObject.w_pParam
      .w_USERS=oParentObject.w_USERS
        .w_ISADMIN = cp_isadministrator()
          .DoRTCalc(2,2,.f.)
        .w_UTEADMIN = cp_isadministrator(.F.,.w_CODE)
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate(cp_translate(MSG_CODE))
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate(cp_translate(MSG_NAME))
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate(cp_translate(MSG_LAST_PASSWORD))
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate(cp_translate(MSG_CONFIRM_PWD))
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate(cp_translate(MSG_LANGUAGE))
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
          .DoRTCalc(4,17,.f.)
        .w_POLSIC = 'AHE'
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_POLSIC))
          .link_1_35('Full')
        endif
      .oPgFrm.Page1.oPag.group.Calculate()
          .DoRTCalc(19,27,.f.)
        .w_VALIDPWD = 0
      .oPgFrm.Page1.oPag.oObj_1_51.Calculate(cp_translate(MSG_NEW_PASSWORD))
      .oPgFrm.Page1.oPag.oObj_1_52.Calculate(cp_translate(MSG_RIGHTS))
    endwith
    this.DoRTCalc(29,30,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CHANGE=.w_CHANGE
      .oParentObject.w_CODE=.w_CODE
      .oParentObject.w_NAME=.w_NAME
      .oParentObject.w_pParam=.w_pParam
      .oParentObject.w_USERS=.w_USERS
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(cp_translate(MSG_CODE))
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(cp_translate(MSG_NAME))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(cp_translate(MSG_LAST_PASSWORD))
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(cp_translate(MSG_CONFIRM_PWD))
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate(cp_translate(MSG_LANGUAGE))
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(1,17,.t.)
          .link_1_35('Full')
        .oPgFrm.Page1.oPag.group.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(cp_translate(MSG_NEW_PASSWORD))
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(cp_translate(MSG_RIGHTS))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(cp_translate(MSG_CODE))
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(cp_translate(MSG_NAME))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(cp_translate(MSG_LAST_PASSWORD))
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(cp_translate(MSG_CONFIRM_PWD))
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate(cp_translate(MSG_LANGUAGE))
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.group.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(cp_translate(MSG_NEW_PASSWORD))
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(cp_translate(MSG_RIGHTS))
    endwith
  return

  proc Calculate_KVUWOJNSCJ()
    with this
          * --- Inizializza variabili
          cp_grpusr_b(this;
              ,'UsrInit';
             )
    endwith
  endproc
  proc Calculate_UQKBVRWAHQ()
    with this
          * --- Popola cursore
          cp_grpusr_b(this;
              ,'UsrCursor';
             )
    endwith
  endproc
  proc Calculate_NZIYQZUKCS()
    with this
          * --- Salvataggio cursore
          cp_grpusr_b(this;
              ,'UsrBeforeQ';
             )
    endwith
  endproc
  proc Calculate_PRIGTSDEYC()
    with this
          * --- Ripristina cursore
          cp_grpusr_b(this;
              ,'UsrAfterQ';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODE_1_4.enabled = this.oPgFrm.Page1.oPag.oCODE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oLOGIN_1_6.enabled = this.oPgFrm.Page1.oPag.oLOGIN_1_6.mCond()
    this.oPgFrm.Page1.oPag.oNAME_1_8.enabled = this.oPgFrm.Page1.oPag.oNAME_1_8.mCond()
    this.oPgFrm.Page1.oPag.oRIGHTS_1_17.enabled = this.oPgFrm.Page1.oPag.oRIGHTS_1_17.mCond()
    this.oPgFrm.Page1.oPag.oDISABLE_1_21.enabled = this.oPgFrm.Page1.oPag.oDISABLE_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLOGIN_1_6.visible=!this.oPgFrm.Page1.oPag.oLOGIN_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oPWDO_1_10.visible=!this.oPgFrm.Page1.oPag.oPWDO_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDISABLE_1_21.visible=!this.oPgFrm.Page1.oPag.oDISABLE_1_21.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.group.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_KVUWOJNSCJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_UQKBVRWAHQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_group before query")
          .Calculate_NZIYQZUKCS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_group after query")
          .Calculate_PRIGTSDEYC()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=POLSIC
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.POL_SIC_IDX,3]
    i_lTable = "POL_SIC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2], .t., this.POL_SIC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POLSIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POLSIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSSERIAL,PSATTIVO,PSCMPLES,PSCMPLEA,PSLUNMIN,PSLUNMIA,PS_NONUM,PS_NORIP,PSNORIPA,PSNUMPWD";
                   +" from "+i_cTable+" "+i_lTable+" where PSSERIAL="+cp_ToStrODBC(this.w_POLSIC);
                   +" and PSSERIAL="+cp_ToStrODBC(this.w_POLSIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSSERIAL',this.w_POLSIC;
                       ,'PSSERIAL',this.w_POLSIC)
            select PSSERIAL,PSATTIVO,PSCMPLES,PSCMPLEA,PSLUNMIN,PSLUNMIA,PS_NONUM,PS_NORIP,PSNORIPA,PSNUMPWD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POLSIC = NVL(_Link_.PSSERIAL,space(3))
      this.w_ATTIVO = NVL(_Link_.PSATTIVO,space(1))
      this.w_COMPLEX = NVL(_Link_.PSCMPLES,0)
      this.w_COMPLEA = NVL(_Link_.PSCMPLEA,0)
      this.w_NUMLIM = NVL(_Link_.PSLUNMIN,0)
      this.w_NUMLIA = NVL(_Link_.PSLUNMIA,0)
      this.w_CODUTE = NVL(_Link_.PS_NONUM,space(1))
      this.w_NORIP = NVL(_Link_.PS_NORIP,space(1))
      this.w_NORIPA = NVL(_Link_.PSNORIPA,space(1))
      this.w_MAXPWD = NVL(_Link_.PSNUMPWD,0)
    else
      if i_cCtrl<>'Load'
        this.w_POLSIC = space(3)
      endif
      this.w_ATTIVO = space(1)
      this.w_COMPLEX = 0
      this.w_COMPLEA = 0
      this.w_NUMLIM = 0
      this.w_NUMLIA = 0
      this.w_CODUTE = space(1)
      this.w_NORIP = space(1)
      this.w_NORIPA = space(1)
      this.w_MAXPWD = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])+'\'+cp_ToStr(_Link_.PSSERIAL,1)+'\'+cp_ToStr(_Link_.PSSERIAL,1)
      cp_ShowWarn(i_cKey,this.POL_SIC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POLSIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODE_1_4.value==this.w_CODE)
      this.oPgFrm.Page1.oPag.oCODE_1_4.value=this.w_CODE
    endif
    if not(this.oPgFrm.Page1.oPag.oLOGIN_1_6.value==this.w_LOGIN)
      this.oPgFrm.Page1.oPag.oLOGIN_1_6.value=this.w_LOGIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNAME_1_8.value==this.w_NAME)
      this.oPgFrm.Page1.oPag.oNAME_1_8.value=this.w_NAME
    endif
    if not(this.oPgFrm.Page1.oPag.oPWDO_1_10.value==this.w_PWDO)
      this.oPgFrm.Page1.oPag.oPWDO_1_10.value=this.w_PWDO
    endif
    if not(this.oPgFrm.Page1.oPag.oCHANGEPWD_1_12.RadioValue()==this.w_CHANGEPWD)
      this.oPgFrm.Page1.oPag.oCHANGEPWD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPWD_1_13.value==this.w_PWD)
      this.oPgFrm.Page1.oPag.oPWD_1_13.value=this.w_PWD
    endif
    if not(this.oPgFrm.Page1.oPag.oPWDC_1_15.value==this.w_PWDC)
      this.oPgFrm.Page1.oPag.oPWDC_1_15.value=this.w_PWDC
    endif
    if not(this.oPgFrm.Page1.oPag.oRIGHTS_1_17.RadioValue()==this.w_RIGHTS)
      this.oPgFrm.Page1.oPag.oRIGHTS_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLANGSYS_1_19.value==this.w_LANGSYS)
      this.oPgFrm.Page1.oPag.oLANGSYS_1_19.value=this.w_LANGSYS
    endif
    if not(this.oPgFrm.Page1.oPag.oDISABLE_1_21.RadioValue()==this.w_DISABLE)
      this.oPgFrm.Page1.oPag.oDISABLE_1_21.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- cp_users
      *-- Verifico errori da batch prima di effettuare il salvataggio
      IF i_bRes 
          i_bRes=cp_grpusr_b(this,'UsrCheck')
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_usersPag1 as StdContainer
  Width  = 422
  height = 429
  stdWidth  = 422
  stdheight = 429
  resizeXpos=181
  resizeYpos=265
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODE_1_4 as StdField with uid="TFWUWAYTBI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODE", cQueryName = "CODE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 116110862,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=15, Top=21, cSayPict='"9999"', cGetPict='"9999"'

  func oCODE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NEW)
    endwith
   endif
  endfunc

  add object oLOGIN_1_6 as StdField with uid="YZQMPOTUGO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LOGIN", cQueryName = "LOGIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Login utente",;
    HelpContextID = 34047358,;
   bGlobalFont=.t.,;
    Height=21, Width=158, Left=193, Top=21, InputMask=replicate('X',20)

  func oLOGIN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISADMIN Or Not .w_USERS)
    endwith
   endif
  endfunc

  func oLOGIN_1_6.mHide()
    with this.Parent.oContained
      return (.w_CODUTE<>'S')
    endwith
  endfunc

  add object oNAME_1_8 as StdField with uid="AVHOVOHOPI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NAME", cQueryName = "NAME",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome utente",;
    HelpContextID = 116077406,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=15, Top=60, InputMask=replicate('X',20)

  func oNAME_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISADMIN Or Not .w_USERS)
    endwith
   endif
  endfunc

  add object oPWDO_1_10 as StdField with uid="NZNOCXKOPK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PWDO", cQueryName = "PWDO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Password in uso",;
    HelpContextID = 115453246,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=15, Top=99, InputMask=replicate('X',15), PasswordChar='*'

  func oPWDO_1_10.mHide()
    with this.Parent.oContained
      return (.w_NEW OR .w_CODE<>i_codute)
    endwith
  endfunc

  add object oCHANGEPWD_1_12 as StdCheck with uid="PRYDRUBAXH",rtseq=8,rtrep=.f.,left=193, top=98, caption="Cambia password al primo accesso",;
    ToolTipText = "Se attivo permette di cambiare la password al primo accesso",;
    HelpContextID = 225634423,;
    cFormVar="w_CHANGEPWD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHANGEPWD_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHANGEPWD_1_12.GetRadio()
    this.Parent.oContained.w_CHANGEPWD = this.RadioValue()
    return .t.
  endfunc

  func oCHANGEPWD_1_12.SetRadio()
    this.Parent.oContained.w_CHANGEPWD=trim(this.Parent.oContained.w_CHANGEPWD)
    this.value = ;
      iif(this.Parent.oContained.w_CHANGEPWD=='S',1,;
      0)
  endfunc

  add object oPWD_1_13 as CheckSafePwd with uid="XFUCEDWHLE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PWD", cQueryName = "PWD",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nuova password",;
    HelpContextID = 120630590,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=15, Top=139, InputMask=replicate('X',15), PasswordChar='*'

  add object oPWDC_1_15 as StdField with uid="SEGNIBVXOO",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PWDC", cQueryName = "PWDC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conferma nuova password",;
    HelpContextID = 116239678,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=193, Top=138, InputMask=replicate('X',15), PasswordChar='*'


  add object oRIGHTS_1_17 as StdCombo with uid="RVONEKTVRP",rtseq=11,rtrep=.f.,left=15,top=180,width=118,height=22;
    , ToolTipText = "Diritti d'accesso alla procedura";
    , HelpContextID = 22508770;
    , cFormVar="w_RIGHTS",RowSource=""+"'NO RIGHT',"+"'READ ONLY',"+"'R\W USER',"+"'R\W ALL',"+"'DEVELOPER'", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRIGHTS_1_17.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    0))))))
  endfunc
  func oRIGHTS_1_17.GetRadio()
    this.Parent.oContained.w_RIGHTS = this.RadioValue()
    return .t.
  endfunc

  func oRIGHTS_1_17.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_RIGHTS==1,1,;
      iif(this.Parent.oContained.w_RIGHTS==2,2,;
      iif(this.Parent.oContained.w_RIGHTS==3,3,;
      iif(this.Parent.oContained.w_RIGHTS==4,4,;
      iif(this.Parent.oContained.w_RIGHTS==5,5,;
      0)))))
  endfunc

  func oRIGHTS_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISADMIN Or Not .w_USERS)
    endwith
   endif
  endfunc

  add object oLANGSYS_1_19 as StdField with uid="SMZZXWREME",rtseq=12,rtrep=.f.,;
    cFormVar = "w_LANGSYS", cQueryName = "LANGSYS",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Lingua utente",;
    HelpContextID = 122084482,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=193, Top=180, InputMask=replicate('X',3)

  add object oDISABLE_1_21 as StdCheck with uid="DVUUAVTSZG",rtseq=13,rtrep=.f.,left=267, top=180, caption="Disabilitato",;
    HelpContextID = 154219522,;
    cFormVar="w_DISABLE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDISABLE_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDISABLE_1_21.GetRadio()
    this.Parent.oContained.w_DISABLE = this.RadioValue()
    return .t.
  endfunc

  func oDISABLE_1_21.SetRadio()
    this.Parent.oContained.w_DISABLE=trim(this.Parent.oContained.w_DISABLE)
    this.value = ;
      iif(this.Parent.oContained.w_DISABLE=='S',1,;
      0)
  endfunc

  func oDISABLE_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISADMIN Or Not .w_USERS)
    endwith
   endif
  endfunc

  func oDISABLE_1_21.mHide()
    with this.Parent.oContained
      return (not .w_HASENABLED)
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="OQBIDCNZRB",left=249, top=401, width=77,height=22,;
    caption="Ok", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 120904014;
    , Caption='\<Ok';
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="IMQMRMOAOO",left=336, top=401, width=77,height=22,;
    caption="Annulla", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 201787602;
    , Caption='\<Annulla';
  , bGlobalFont=.t.

    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_24 as cp_setCtrlObjprop with uid="STMQUKDDXO",left=460, top=-18, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Codice",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 57064882


  add object oObj_1_25 as cp_setCtrlObjprop with uid="SDWMSALZFF",left=460, top=8, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nome",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 57064882


  add object oObj_1_26 as cp_setCtrlObjprop with uid="OXGLOGRFGP",left=460, top=34, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Vecchia password",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 57064882


  add object oObj_1_27 as cp_setCtrlObjprop with uid="DTASHCFBDZ",left=460, top=60, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Conferma password",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 57064882


  add object oObj_1_28 as cp_setCtrlObjprop with uid="CJNSDIYYZM",left=460, top=86, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Lingua",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 57064882


  add object oObj_1_33 as cp_runprogram with uid="GDOPCYPOZJ",left=556, top=-18, width=192,height=26,;
    caption='CP_GRPUSR_B(USRMODCODE)',;
   bGlobalFont=.t.,;
    prg="cp_grpusr_b('UsrModCode')",;
    cEvent = "w_CODE Changed",;
    nPag=1;
    , HelpContextID = 263718750


  add object oObj_1_34 as cp_runprogram with uid="CEJPVREZDM",left=556, top=8, width=192,height=26,;
    caption='CP_GRPUSR_B(USRMODLANG)',;
   bGlobalFont=.t.,;
    prg="cp_grpusr_b('UsrModLang')",;
    cEvent = "w_LANGSYS Changed",;
    nPag=1;
    , HelpContextID = 220559198


  add object group as cp_szoombox with uid="LIELHMYYLI",left=5, top=203, width=415,height=193,;
    caption='group',;
   bGlobalFont=.t.,;
    bNoZoomGridShape=.f.,cTable="cpgroups",cZoomOnZoom="",cZoomFile="cpgroups",bOptions=.f.,bAdvOptions=.t.,bQueryOnDblClick=.t.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 263774414


  add object oObj_1_51 as cp_setCtrlObjprop with uid="TECPTNGDSA",left=556, top=34, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nuova password",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 57064882


  add object oObj_1_52 as cp_setCtrlObjprop with uid="SICELUOJVZ",left=556, top=60, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Diritti",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 57064882

  add object oStr_1_5 as StdString with uid="TFIGKZFVDU",Visible=.t., Left=15, Top=6,;
    Alignment=0, Width=42, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="HTHMXRTMWE",Visible=.t., Left=193, Top=6,;
    Alignment=0, Width=34, Height=18,;
    Caption="Login"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_CODUTE<>'S')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="YPRXWNZHUK",Visible=.t., Left=15, Top=44,;
    Alignment=0, Width=37, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="LNEEFUZQCN",Visible=.t., Left=15, Top=82,;
    Alignment=0, Width=118, Height=18,;
    Caption="Vecchia password"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_NEW OR .w_CODE<>i_codute)
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="YOXLFJNNGQ",Visible=.t., Left=15, Top=123,;
    Alignment=0, Width=117, Height=18,;
    Caption="Nuova Password"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="OMXFRYQUHQ",Visible=.t., Left=193, Top=123,;
    Alignment=0, Width=127, Height=18,;
    Caption="Conferma password"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="UQFUKQUNLX",Visible=.t., Left=15, Top=162,;
    Alignment=0, Width=42, Height=18,;
    Caption="Diritti"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="FCJBWWPXHE",Visible=.t., Left=193, Top=164,;
    Alignment=0, Width=47, Height=18,;
    Caption="Lingua"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_users','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_users
define class CheckSafePwd as StdField

proc interactivechange()

LOCAL mi,ma,sp,nu,c1,c2,c3,ch,i_i,psw
LOCAL compless, limite, valid
	     mi=0     &&contatore lettere minuscole
	     ma=0     &&contatore lettere maiuscole
	     nu=0     &&contatore numeri
	     sp=0     &&contatore caratteri speciali
	     c1='abcdefghijklmnopqrstuvwxyz'
	     c2='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	     c3='0123456789'
		 psw=ALLTRIM(this.value)
		 FOR i_i=1 TO LEN(psw)
		   ch=ALLTRIM(SUBSTR(psw,i_i,1))
		   DO CASE 
		       CASE ch$c1
		              mi=mi+1
		       CASE ch$c2
		              ma=ma+1
		       CASE ch$c3
		              nu=nu+1
		       OTHERWISE 
		              sp=sp+1
		   ENDCASE
		 NEXT 
         mi=IIF(mi>0,1,0)
         ma=IIF(ma>0,1,0)
         nu=IIF(nu>0,1,0)
         sp=IIF(sp>0,1,0)
         IF thisform.w_UTEADMIN
            compless=thisform.w_complea
            limite=thisform.w_numlia
         ELSE
            compless=thisform.w_complex
            limite=thisform.w_numlim
         ENDIF
         IF compless=1
            valid=0
         ELSE
            valid=(compless*2)
         ENDIF
         IF (mi+ma+nu+sp >= compless AND (LEN(alltrim(this.value))>=limite and LEN(alltrim(this.value))>=valid)) OR thisform.w_attivo<>'S' OR (LEN(alltrim(this.value))=0 AND limite=0 AND compless=1)
            this.backcolor=RGB(0,255,0)
            thisform.w_ValidPwd=0
         ELSE
            IF mi+ma+nu+sp < compless or LEN(alltrim(this.value))<limite
              this.backcolor=RGB(255,0,0)
              thisform.w_ValidPwd=2
            ELSE
              this.backcolor=RGB(255,255,0)
              thisform.w_ValidPwd=1
            ENDIF
         ENDIF
ENDPROC

enddefine
* --- Fine Area Manuale
