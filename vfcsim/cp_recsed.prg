* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_recsed                                                       *
*              Security record detail                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-27                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tcp_recsed")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tccp_recsed")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tccp_recsed")
  return

* --- Class definition
define class tcp_recsed as StdPCForm
  Width  = 149
  Height = 212
  Top    = 10
  Left   = 7
  cComment = "Security record detail"
  cPrg = "cp_recsed"
  HelpContextID=107182393
  add object cnt as tccp_recsed
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tscp_recsed as PCContext
  w_GRPCODE = 0
  w_USRCODE = 0
  w_XGRPCODE = 0
  w_XUSRCODE = 0
  w_namegrp = space(20)
  w_nameusr = space(20)
  w_tablename = space(50)
  w_progname = space(50)
  w_rfrownum = 0
  proc Save(i_oFrom)
    this.w_GRPCODE = i_oFrom.w_GRPCODE
    this.w_USRCODE = i_oFrom.w_USRCODE
    this.w_XGRPCODE = i_oFrom.w_XGRPCODE
    this.w_XUSRCODE = i_oFrom.w_XUSRCODE
    this.w_namegrp = i_oFrom.w_namegrp
    this.w_nameusr = i_oFrom.w_nameusr
    this.w_tablename = i_oFrom.w_tablename
    this.w_progname = i_oFrom.w_progname
    this.w_rfrownum = i_oFrom.w_rfrownum
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_GRPCODE = this.w_GRPCODE
    i_oTo.w_USRCODE = this.w_USRCODE
    i_oTo.w_XGRPCODE = this.w_XGRPCODE
    i_oTo.w_XUSRCODE = this.w_XUSRCODE
    i_oTo.w_namegrp = this.w_namegrp
    i_oTo.w_nameusr = this.w_nameusr
    i_oTo.w_tablename = this.w_tablename
    i_oTo.w_progname = this.w_progname
    i_oTo.w_rfrownum = this.w_rfrownum
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tccp_recsed as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 149
  Height = 212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=107182393
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  cprecsed_IDX = 0
  cpgroups_IDX = 0
  cpusers_IDX = 0
  cFile = "cprecsed"
  cKeySelect = "tablename,progname,rfrownum"
  cKeyWhere  = "tablename=this.w_tablename and progname=this.w_progname and rfrownum=this.w_rfrownum"
  cKeyDetail  = "GRPCODE=this.w_GRPCODE and USRCODE=this.w_USRCODE and tablename=this.w_tablename and progname=this.w_progname and rfrownum=this.w_rfrownum"
  cKeyWhereODBC = '"tablename="+cp_ToStrODBC(this.w_tablename)';
      +'+" and progname="+cp_ToStrODBC(this.w_progname)';
      +'+" and rfrownum="+cp_ToStrODBC(this.w_rfrownum)';

  cKeyDetailWhereODBC = '"GRPCODE="+cp_ToStrODBC(this.w_GRPCODE)';
      +'+" and USRCODE="+cp_ToStrODBC(this.w_USRCODE)';
      +'+" and tablename="+cp_ToStrODBC(this.w_tablename)';
      +'+" and progname="+cp_ToStrODBC(this.w_progname)';
      +'+" and rfrownum="+cp_ToStrODBC(this.w_rfrownum)';

  cKeyWhereODBCqualified = '"cprecsed.tablename="+cp_ToStrODBC(this.w_tablename)';
      +'+" and cprecsed.progname="+cp_ToStrODBC(this.w_progname)';
      +'+" and cprecsed.rfrownum="+cp_ToStrODBC(this.w_rfrownum)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'cprecsed.GRPCODE,cprecsed.USRCODE'
  cPrg = "cp_recsed"
  cComment = "Security record detail"
  i_nRowNum = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GRPCODE = 0
  o_GRPCODE = 0
  w_USRCODE = 0
  o_USRCODE = 0
  w_XGRPCODE = 0
  o_XGRPCODE = 0
  w_XUSRCODE = 0
  o_XUSRCODE = 0
  w_namegrp = space(20)
  w_nameusr = space(20)
  w_tablename = space(50)
  w_progname = space(50)
  w_rfrownum = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_recsedPag1","cp_recsed",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='cpgroups'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='cprecsed'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.cprecsed_IDX,5],7]
    this.nPostItConn=i_TableProp[this.cprecsed_IDX,3]
  return

  procedure NewContext()
    return(createobject('tscp_recsed'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from cprecsed where GRPCODE=KeySet.GRPCODE
    *                            and USRCODE=KeySet.USRCODE
    *                            and tablename=KeySet.tablename
    *                            and progname=KeySet.progname
    *                            and rfrownum=KeySet.rfrownum
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.cprecsed_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2],this.bLoadRecFilter,this.cprecsed_IDX,"cp_recsed")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('cprecsed')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "cprecsed.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' cprecsed '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'tablename',this.w_tablename  ,'progname',this.w_progname  ,'rfrownum',this.w_rfrownum  )
      select * from (i_cTable) cprecsed where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_tablename = NVL(tablename,space(50))
        .w_progname = NVL(progname,space(50))
        .w_rfrownum = NVL(rfrownum,0)
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate(MSG_GROUPS)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(MSG_USERS)
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(cp_translate(MSG_GROUP_DESC))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_USER_DESC))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_GROUP_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_USER_TOOLTIP))
        cp_LoadRecExtFlds(this,'cprecsed')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_namegrp = space(20)
          .w_nameusr = space(20)
          .w_GRPCODE = NVL(GRPCODE,0)
          .w_USRCODE = NVL(USRCODE,0)
        .w_XGRPCODE = iif(.w_GRPCODE=-1, 0, .w_GRPCODE)
          .link_2_3('Load')
        .w_XUSRCODE = iif(.w_XGRPCODE<>0, 0, iif(.w_USRCODE=-1,0,.w_USRCODE))
          .link_2_4('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace GRPCODE with .w_GRPCODE
          replace USRCODE with .w_USRCODE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate(MSG_GROUPS)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(MSG_USERS)
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(cp_translate(MSG_GROUP_DESC))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_USER_DESC))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_GROUP_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_USER_TOOLTIP))
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_GRPCODE=0
      .w_USRCODE=0
      .w_XGRPCODE=0
      .w_XUSRCODE=0
      .w_namegrp=space(20)
      .w_nameusr=space(20)
      .w_tablename=space(50)
      .w_progname=space(50)
      .w_rfrownum=0
      if .cFunction<>"Filter"
        .w_GRPCODE = iif(.w_XGRPCODE=0,-1,.w_XGRPCODE)
        .w_USRCODE = iif(.w_XUSRCODE=0 or .w_XGRPCODE<>0,-1,.w_XUSRCODE)
        .w_XGRPCODE = iif(.w_GRPCODE=-1, 0, .w_GRPCODE)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_XGRPCODE))
         .link_2_3('Full')
        endif
        .w_XUSRCODE = iif(.w_XGRPCODE<>0, 0, iif(.w_USRCODE=-1,0,.w_USRCODE))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_XUSRCODE))
         .link_2_4('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate(MSG_GROUPS)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(MSG_USERS)
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(cp_translate(MSG_GROUP_DESC))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_USER_DESC))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_GROUP_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_USER_TOOLTIP))
      endif
    endwith
    cp_BlankRecExtFlds(this,'cprecsed')
    this.DoRTCalc(5,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page1.oPag.oObj_1_6.enabled = i_bVal
      .Page1.oPag.oObj_1_7.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oObj_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_10.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'cprecsed',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.cprecsed_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_tablename,"tablename",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_progname,"progname",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_rfrownum,"rfrownum",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_XGRPCODE N(5);
      ,t_XUSRCODE N(5);
      ,t_namegrp C(20);
      ,t_nameusr C(20);
      ,GRPCODE N(5);
      ,USRCODE N(5);
      ,t_GRPCODE N(5);
      ,t_USRCODE N(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tcp_recsedbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3.controlsource=this.cTrsName+'.t_XGRPCODE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.controlsource=this.cTrsName+'.t_XUSRCODE'
    this.oPgFRm.Page1.oPag.onamegrp_2_5.controlsource=this.cTrsName+'.t_namegrp'
    this.oPgFRm.Page1.oPag.onameusr_2_6.controlsource=this.cTrsName+'.t_nameusr'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(71)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.cprecsed_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.cprecsed_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
      *
      * insert into cprecsed
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'cprecsed')
        i_extval=cp_InsertValODBCExtFlds(this,'cprecsed')
        i_cFldBody=" "+;
                  "(GRPCODE,USRCODE,tablename,progname,rfrownum,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_GRPCODE)+","+cp_ToStrODBC(this.w_USRCODE)+","+cp_ToStrODBC(this.w_tablename)+","+cp_ToStrODBC(this.w_progname)+","+cp_ToStrODBC(this.w_rfrownum)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'cprecsed')
        i_extval=cp_InsertValVFPExtFlds(this,'cprecsed')
        cp_CheckDeletedKey(i_cTable,0,'GRPCODE',this.w_GRPCODE,'USRCODE',this.w_USRCODE,'tablename',this.w_tablename,'progname',this.w_progname,'rfrownum',this.w_rfrownum)
        INSERT INTO (i_cTable) (;
                   GRPCODE;
                  ,USRCODE;
                  ,tablename;
                  ,progname;
                  ,rfrownum;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_GRPCODE;
                  ,this.w_USRCODE;
                  ,this.w_tablename;
                  ,this.w_progname;
                  ,this.w_rfrownum;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.cprecsed_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_GRPCODE>0 or t_USRCODE>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'cprecsed')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and GRPCODE="+cp_ToStrODBC(&i_TN.->GRPCODE)+;
                 " and USRCODE="+cp_ToStrODBC(&i_TN.->USRCODE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'cprecsed')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and GRPCODE=&i_TN.->GRPCODE;
                      and USRCODE=&i_TN.->USRCODE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_GRPCODE>0 or t_USRCODE>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and GRPCODE="+cp_ToStrODBC(&i_TN.->GRPCODE)+;
                            " and USRCODE="+cp_ToStrODBC(&i_TN.->USRCODE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and GRPCODE=&i_TN.->GRPCODE;
                            and USRCODE=&i_TN.->USRCODE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace GRPCODE with this.w_GRPCODE
              replace USRCODE with this.w_USRCODE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update cprecsed
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'cprecsed')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     "GRPCODE="+cp_ToStrODBC(this.w_GRPCODE)+;
                     ",USRCODE="+cp_ToStrODBC(this.w_USRCODE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and GRPCODE="+cp_ToStrODBC(GRPCODE)+;
                             " and USRCODE="+cp_ToStrODBC(USRCODE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'cprecsed')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                     GRPCODE=this.w_GRPCODE;
                     ,USRCODE=this.w_USRCODE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and GRPCODE=&i_TN.->GRPCODE;
                                      and USRCODE=&i_TN.->USRCODE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.cprecsed_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_GRPCODE>0 or t_USRCODE>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete cprecsed
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and GRPCODE="+cp_ToStrODBC(&i_TN.->GRPCODE)+;
                            " and USRCODE="+cp_ToStrODBC(&i_TN.->USRCODE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and GRPCODE=&i_TN.->GRPCODE;
                              and USRCODE=&i_TN.->USRCODE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_GRPCODE>0 or t_USRCODE>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.cprecsed_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
    if i_bUpd
      with this
        if .o_XGRPCODE<>.w_XGRPCODE.or. .o_XUSRCODE<>.w_XUSRCODE
          .w_GRPCODE = iif(.w_XGRPCODE=0,-1,.w_XGRPCODE)
        endif
        if .o_XUSRCODE<>.w_XUSRCODE.or. .o_XGRPCODE<>.w_XGRPCODE
          .w_USRCODE = iif(.w_XUSRCODE=0 or .w_XGRPCODE<>0,-1,.w_XUSRCODE)
        endif
        if .o_GRPCODE<>.w_GRPCODE
          .w_XGRPCODE = iif(.w_GRPCODE=-1, 0, .w_GRPCODE)
          .link_2_3('Full')
        endif
        if .o_USRCODE<>.w_USRCODE.or. .o_XGRPCODE<>.w_XGRPCODE
          .w_XUSRCODE = iif(.w_XGRPCODE<>0, 0, iif(.w_USRCODE=-1,0,.w_USRCODE))
          .link_2_4('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate(MSG_GROUPS)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(MSG_USERS)
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(cp_translate(MSG_GROUP_DESC))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_USER_DESC))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_GROUP_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_USER_TOOLTIP))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_GRPCODE with this.w_GRPCODE
      replace t_USRCODE with this.w_USRCODE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate(MSG_GROUPS)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(MSG_USERS)
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(cp_translate(MSG_GROUP_DESC))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_USER_DESC))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_GROUP_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_USER_TOOLTIP))
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oXUSRCODE_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oXUSRCODE_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=XGRPCODE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_XGRPCODE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_XGRPCODE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_XGRPCODE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_XGRPCODE) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.parent,'oXGRPCODE_2_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_XGRPCODE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_XGRPCODE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_XGRPCODE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_XGRPCODE = NVL(_Link_.code,0)
      this.w_namegrp = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_XGRPCODE = 0
      endif
      this.w_namegrp = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_XGRPCODE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=XUSRCODE
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_XUSRCODE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_XUSRCODE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_XUSRCODE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_XUSRCODE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oXUSRCODE_2_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_XUSRCODE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_XUSRCODE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_XUSRCODE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_XUSRCODE = NVL(_Link_.code,0)
      this.w_nameusr = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_XUSRCODE = 0
      endif
      this.w_nameusr = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_XUSRCODE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.onamegrp_2_5.value==this.w_namegrp)
      this.oPgFrm.Page1.oPag.onamegrp_2_5.value=this.w_namegrp
      replace t_namegrp with this.oPgFrm.Page1.oPag.onamegrp_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.onameusr_2_6.value==this.w_nameusr)
      this.oPgFrm.Page1.oPag.onameusr_2_6.value=this.w_nameusr
      replace t_nameusr with this.oPgFrm.Page1.oPag.onameusr_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3.value==this.w_XGRPCODE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3.value=this.w_XGRPCODE
      replace t_XGRPCODE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.value==this.w_XUSRCODE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.value=this.w_XUSRCODE
      replace t_XUSRCODE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'cprecsed')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_GRPCODE>0 or .w_USRCODE>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GRPCODE = this.w_GRPCODE
    this.o_USRCODE = this.w_USRCODE
    this.o_XGRPCODE = this.w_XGRPCODE
    this.o_XUSRCODE = this.w_XUSRCODE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_GRPCODE>0 or t_USRCODE>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_GRPCODE=0
      .w_USRCODE=0
      .w_XGRPCODE=0
      .w_XUSRCODE=0
      .w_namegrp=space(20)
      .w_nameusr=space(20)
        .w_GRPCODE = iif(.w_XGRPCODE=0,-1,.w_XGRPCODE)
        .w_USRCODE = iif(.w_XUSRCODE=0 or .w_XGRPCODE<>0,-1,.w_XUSRCODE)
        .w_XGRPCODE = iif(.w_GRPCODE=-1, 0, .w_GRPCODE)
      .DoRTCalc(3,3,.f.)
      if not(empty(.w_XGRPCODE))
        .link_2_3('Full')
      endif
        .w_XUSRCODE = iif(.w_XGRPCODE<>0, 0, iif(.w_USRCODE=-1,0,.w_USRCODE))
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_XUSRCODE))
        .link_2_4('Full')
      endif
    endwith
    this.DoRTCalc(5,9,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_GRPCODE = t_GRPCODE
    this.w_USRCODE = t_USRCODE
    this.w_XGRPCODE = t_XGRPCODE
    this.w_XUSRCODE = t_XUSRCODE
    this.w_namegrp = t_namegrp
    this.w_nameusr = t_nameusr
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_GRPCODE with this.w_GRPCODE
    replace t_USRCODE with this.w_USRCODE
    replace t_XGRPCODE with this.w_XGRPCODE
    replace t_XUSRCODE with this.w_XUSRCODE
    replace t_namegrp with this.w_namegrp
    replace t_nameusr with this.w_nameusr
    if i_srv='A'
      replace GRPCODE with this.w_GRPCODE
      replace USRCODE with this.w_USRCODE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tcp_recsedPag1 as StdContainer
  Width  = 145
  height = 212
  stdWidth  = 145
  stdheight = 212
  resizeYpos=87
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=2, width=137,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="XGRPCODE",Label1="Gruppi",Field2="XUSRCODE",Label2="Utenti",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 150002327


  add object oObj_1_5 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=615, top=-1, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Gruppi",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425399


  add object oObj_1_6 as cp_setCtrlObjprop with uid="GGRDGFFQVZ",left=750, top=-1, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Utenti",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425399


  add object oObj_1_7 as cp_setobjprop with uid="SKPUZOAYUU",left=615, top=162, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_namegrp",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425399


  add object oObj_1_8 as cp_setobjprop with uid="YIGARNUYFX",left=615, top=187, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_nameusr",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425399


  add object oObj_1_9 as cp_setobjprop with uid="FLKYMPZHIR",left=615, top=26, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_XGRPCODE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425399


  add object oObj_1_10 as cp_setobjprop with uid="FWHFXVEOJG",left=750, top=26, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_XUSRCODE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425399

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=21,;
    width=140,height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=22,width=139,height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=0,enabled=.f.,;
    cLinkFile='cpgroups|cpusers|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.onamegrp_2_5.Refresh()
      this.Parent.onameusr_2_6.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='cpgroups'
        oDropInto=this.oBodyCol.oRow.oXGRPCODE_2_3
      case cFile='cpusers'
        oDropInto=this.oBodyCol.oRow.oXUSRCODE_2_4
    endcase
    return(oDropInto)
  EndFunc


  add object onamegrp_2_5 as StdTrsField with uid="BACRHQMRWI",rtseq=5,rtrep=.t.,;
    cFormVar="w_namegrp",value=space(20),enabled=.f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 123734823,;
    cTotal="", bFixedPos=.t., cQueryName = "namegrp",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=5, Top=159, InputMask=replicate('X',20)

  add object onameusr_2_6 as StdTrsField with uid="KIKNHAOVJO",rtseq=6,rtrep=.t.,;
    cFormVar="w_nameusr",value=space(20),enabled=.f.,;
    ToolTipText = "Nome utente",;
    HelpContextID = 90180393,;
    cTotal="", bFixedPos=.t., cQueryName = "nameusr",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=5, Top=184, InputMask=replicate('X',20)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tcp_recsedBodyRow as CPBodyRowCnt
  Width=130
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oXGRPCODE_2_3 as StdTrsField with uid="BJTDZFWRTC",rtseq=3,rtrep=.t.,;
    cFormVar="w_XGRPCODE",value=0,;
    ToolTipText = "Gruppo da controllare nell'applicazione delle politiche di sicurezza",;
    HelpContextID = 32725323,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_XGRPCODE"

  func oXGRPCODE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oXGRPCODE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oXGRPCODE_2_3.mZoom
    do cp_grpzoom with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oXUSRCODE_2_4 as StdTrsField with uid="CUQZUDDGHO",rtseq=4,rtrep=.t.,;
    cFormVar="w_XUSRCODE",value=0,;
    ToolTipText = "Utente da controllare nell'applicazione delle politiche di sicurezza",;
    HelpContextID = 34945355,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=63, Top=0, bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_XUSRCODE"

  func oXUSRCODE_2_4.mCond()
    with this.Parent.oContained
      return (.w_XGRPCODE=0)
    endwith
  endfunc

  func oXUSRCODE_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oXUSRCODE_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oXUSRCODE_2_4.mZoom
    do cp_usrzoom with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  add object oLast as LastKeyMover
  * ---
  func oXGRPCODE_2_3.When()
    return(.t.)
  proc oXGRPCODE_2_3.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oXGRPCODE_2_3.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_recsed','cprecsed','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".tablename=cprecsed.tablename";
  +" and "+i_cAliasName2+".progname=cprecsed.progname";
  +" and "+i_cAliasName2+".rfrownum=cprecsed.rfrownum";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
