* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_ZDATE
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Zoom standard su campi data
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

#Define CONTAINER_BORDERCOLOR	2
#Define PANELVERTICAL_BACKCOLOR	7
#Define FORM_BACKGROUND_PICTURE	11
#Define OVERFLOWPANELBUTTON_FOCUSEDNOTSELECTED_PICTURE	12
#Define OVERFLOWPANEL_BACKGROUND_PICTURE	15
#Define SPINNER_DATETIME_PICKER_BACKGROUND 97

Lparam oSource
Local i_err
Private i_bErr
i_bErr=.F.
i_err=On('ERROR')
*On Error i_bErr=.T.
If i_bMobileMode OR g_typecalendar='R'
	If g_typecalendar='S'
		o=Createobject("zoom_data_mobility_form",oSource)
	Else
		o=Createobject("zoom_data_spinner_form",oSource)
	EndIf
Else
    If !(Upper(Set("Classlib"))$"DATEPICKER")
        Set Classlib To DatePicker Additive
    Endif
    Local l_bDatePickerFloating
    l_bDatePickerFloating = i_bDatePickerFloating
    Public o
    o=Createobject("calform",oSource, oSource.Parent.oContained)
    o.ControlBox = .T.
    o.TitleBar = Iif(l_bDatePickerFloating, 1, 0)
    o.HalfHeightCaption =l_bDatePickerFloating
    o.BorderStyle = Iif(l_bDatePickerFloating, 2, 0)
    o.WindowType = Iif(l_bDatePickerFloating, 1, 0)
    o.bSumDays = !(Type("g_NoDatePickerSumDays")="L" And g_NoDatePickerSumDays)
Endif
On Error &i_err
If i_bErr
    CP_MSG(cp_Translate(MSG_CALENDAR_NOT_AVALIABLE),.F.)
Else
    o.Show()
Endif
*Return

Define Class ScrollingButton As Container
    Top = 0
    Left = 0
    Width = 40
    Height = 40
    Visible = .F.
    nDirection = 1
    nDefaultDelay=500
    BorderWidth=0
    BackStyle = 0

    Add Object oImg As Image With Top=4, Left=4, Height=32,Width=32,Visible=.F., Anchor=768
    Add Object oTimerScroll As Timer With Enabled=.F., Interval=1000

    Procedure Init
        *Inizializzo la bitmap del tasto
        This.SetBtnProperties()
    Endproc

    Proc SetBtnProperties
        Do Case
            Case This.nDirection=0	&&Up
                This.oImg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('.\bmp\up.bmp', 32),'bmp')
                This.oImg.ToolTipText = MGS_PREVIOUS_RECORD
            Case This.nDirection=1	&&Down
                This.oImg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('.\bmp\down.bmp', 32),'bmp')
                This.oImg.ToolTipText = MSG_NEXT_RECORD
            Case This.nDirection=4	&&Left
                This.oImg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('.\bmp\left.bmp', 32),'bmp')
                This.oImg.ToolTipText = MSG_LEFT
            Case This.nDirection=5	&&Right
                This.oImg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('.\bmp\right.bmp', 32),'bmp')
                This.oImg.ToolTipText = MSG_RIGHT
        Endcase
    Endproc

    Proc Direction_Assign
        Lparameters nNewVal
        This.nDirection = m.nNewVal
        This.SetBtnProperties()
    Endproc

    Proc oImg.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton=1 And nShift=0
            This.Parent.oTimerScroll.Timer()
            This.Parent.oTimerScroll.Interval = This.Parent.nDefaultDelay
            This.Parent.oTimerScroll.Enabled=.T.
        Endif
    Endproc

    Proc oImg.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.oTimerScroll.Enabled=.F.
    Endproc

    Proc oTimerScroll.Timer
        This.Parent.SetNewValue()
        This.Interval = Max( Floor(This.Interval/1.3) , 10 )
    Endproc

    Proc SetNewValue
        This.Parent.nAttValue = This.Parent.nAttValue + Iif(This.nDirection=0 Or This.nDirection=4, -1, 1)
        This.Parent.Parent.CheckDate(This.nDirection)
    Endproc

    Proc Visible_Assign
        Lparameters bNewVal
        This.Visible = m.bNewVal
        This.oImg.Visible = m.bNewVal
    Endproc

Enddefine

Define Class ScrollingDate As Container
    nMinValue = 0
    nMaxValue = 100
    nAttValue = 0
    Width = 40
    BorderWidth=0
    BackStyle = 0
    FontName = "Arial"
    FontSize = 9
    FontItalic = .F.
    FontBold = .F.
    FontUnderline = .F.
    FontStrikethru = .F.
    cValueTransform = ""
    nOldYVal = -1
    nOldYTime = 0
    nScrollDirection = 0
    bMouseHold = .F.
    nAutoScrollRemain = 0

    Add Object btnUp As ScrollingButton With ;
        Top=3, Left=3, Width=40, Height=40, Caption='',FontBold=.F., ;
        Visible=.F., nDirection=0, nMinValue = 1,	nMaxValue = 31, Anchor=10
    Add Object lblValue As StdString With ;
        Top = 44, Left=3, Width=40,Height=40, Caption='',Visible=.F., Alignment=2, Anchor=10
    Add Object btnDown As ScrollingButton With ;
        Top=85, Left=3, Width=40, Height=40, Caption='',FontBold=.F., ;
        Visible=.F., nDirection=1, nMinValue = 1,	nMaxValue = 31, Anchor=10
    Add Object oTimerAutoScroll As Timer With Interval=100, Enabled=.F.

    Proc Init
        With This
            .FontName = SetFontName('P', This.FontName, .T.)
            .FontSize = SetFontSize('P', This.FontSize, .T.)
            .FontItalic = SetFontItalic('P', This.FontItalic, .T.)
            .FontBold = SetFontBold('P', This.FontBold, .T.)
        Endwith

        With This.lblValue
            nFontHeight = .FontSize
            Do While Fontmetric(1, .FontName, m.nFontHeight)<.Height-10 And Fontmetric(6, .FontName, m.nFontHeight)<.Width-10 And m.nFontHeight<50
                nFontHeight = m.nFontHeight + 1
            Enddo
            .FontSize = m.nFontHeight - 1
        Endwith

    Endproc

    Proc oTimerAutoScroll.Timer
        If This.Parent.nAutoScrollRemain >0 And This.Parent.nScrollDirection<>0
            If This.Parent.nScrollDirection=1
                This.Parent.btnDown.SetNewValue()
            Else
                This.Parent.btnUp.SetNewValue()
            Endif
            This.Parent.nAutoScrollRemain = This.Parent.nAutoScrollRemain -1
            This.Interval = This.Interval + 10
        Else
            This.Enabled = .F.
            This.Parent.nScrollDirection = 0
        Endif
    Endproc

    Proc SetForeColor
        Lparameters nNewVal
        This.lblValue.ForeColor = m.nNewVal
    Endproc

    Proc GetForeColor
        Return This.lblValue.ForeColor
    Endproc

    Proc Visible_Assign
        Lparameters bNewVal
        This.Visible = m.bNewVal
        This.btnUp.Visible = m.bNewVal
        This.lblValue.Visible = m.bNewVal
        This.btnDown.Visible = m.bNewVal
    Endproc

    Procedure nMinValue_Assign
        Lparameters nNewVal
        This.nMinValue = m.nNewVal
        If This.nAttValue<This.nMinValue
            This.nAttValue = This.nMinValue
        Endif
    Endproc

    Procedure nMaxValue_Assign
        Lparameters nNewVal
        This.nMaxValue = m.nNewVal
        If This.nAttValue>This.nMaxValue
            This.nAttValue = This.nMaxValue
        Endif
    Endproc

    Proc nAttValue_Assign
        Lparameters nNewVal
        This.nAttValue = m.nNewVal
        If This.nAttValue<This.nMinValue
            This.nAttValue=This.nMaxValue
        Else
            If This.nAttValue>This.nMaxValue
                This.nAttValue = This.nMinValue
            Endif
        Endif
        If Empty(This.cValueTransform)
            This.lblValue.Caption = Alltrim(Str(This.nAttValue))
        Else
            Local l_macro
            l_macro = This.cValueTransform
            This.lblValue.Caption = ""
            This.lblValue.Caption = &l_macro
            If Empty(This.lblValue.Caption) And !Empty(This.nAttValue)
                This.lblValue.Caption = Alltrim(Str(This.nAttValue))
            Endif
        Endif
    Endproc

Enddefine

Define Class zoom_data_mobility_form As CpSysform
    WindowType=1
    oSource=.Null.
    Caption=""
    BorderStyle=0
    MaxButton=.F.
    MinButton=.F.
    ControlBox=.F.
    TitleBar=0
    ShowTips = .T. &&  mostra il tooltip
    Height=130
    Width=300
    nOldForeColor = -1

    Add Object shpBorder As Shape

    Add Object scrDay As ScrollingDate With ;
        Top=3, Left=3, Width=40, Height=125, Caption='',FontBold=.F., ;
        Visible=.F., nMinValue = 1,	nMaxValue = 31

    Add Object scrMonth As ScrollingDate With ;
        Top=3, Left=44, Width=40, Height=125, Caption='',FontBold=.F., ;
        Visible=.F., nMinValue = 1,	nMaxValue = 12, cValueTransform = "g_Mese[this.nAttValue]"

    Add Object scrYear As ScrollingDate With ;
        Top=3, Left=175, Width=40, Height=125, Caption='',FontBold=.F., ;
        Visible=.F., nMinValue = 1900,	nMaxValue = 3000

    Add Object oBtnOk As StdButton With uid="ELGVTCNZJZ",Left=246, Top=18, Width=48,Height=45,;
        Picture="BMP\OK.BMP", Caption="", nPag=1 , ToolTipText = "Premere per confermare";
        , HelpContextID = 226640406 , Caption='\<Ok';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oBtnCancel As StdButton With uid="ELGVTCNZJZ",Left=246, Top=64, Width=48,Height=45,;
        Picture="BMP\ESC.BMP", Caption="", nPag=1 , ToolTipText = "Premere per annullare";
        , HelpContextID = 226640406 , Caption='\<Esci';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Proc Init
        Lparameters oSource
        Local BottomSpace, RightSpace, i_oParentObject, nFontHeight
        This.oSource = m.oSource
        i_oParentObject = Iif(Type('i_fromobj')<>'O',i_curform,i_fromobj)
        This.Top =  This.oSource.Height + Objtoclient( This.oSource ,1)  + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,  Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top + 1
        This.Left = Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left -1
        *Verifiche per visualizzazione (verticali)
        *Se visualizzando lo zoom sotto il controllo non sta nello schermo provo a visualizzarlo sopra al controllo
        *Se visualizzandolo sopra al controllo non ci sta comunque verifico se lo spazio maggiore � sopra o sotto il
        *controllo e visualizzo li lo zoom
        BottomSpace = _Screen.Height - This.Top
        If This.Top+This.Height>_Screen.Height
            *Visualizzo lo zoom sopra al controllo
            This.Top =  (Objtoclient( This.oSource ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top) - This.Height - 1
            If This.Top<0
                *Poco spazio sopra al controllo. Verifico se lo spazio � maggiore sopra o sotto
                *visualizzo nell'area maggiore lo zoom ridimensionandolo
                If  (Objtoclient( This.oSource ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top) > BottomSpace
                    *Ridimensiono lo zoom e lo visualizzo comunque sopra al controllo
                    This.Height = Max(Objtoclient( This.oSource ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top + 4, Thisform.MinHeight)
                    This.Top = (Objtoclient( This.oSource ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top) - This.Height - 1
                Else
                    *Visualizzo sotto al controllo
                    This.Top =  This.oSource.Height + Objtoclient( This.oSource ,1)  + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) ) + Sysmetric(9) + i_oParentObject.Top + 1
                Endif
            Endif
        Endif

        *Verifiche per visualizzazione (orizzontali)
        *Se visualizzando lo zoom allineato a sinistra del controllo non sta nello schermo provo ad allinearlo a destra
        *Se allineato a destra non ci sta comunque verifico se lo spazio maggiore � a destra o a sinistra del
        *controllo e visualizzo li lo zoom
        RightSpace = _Screen.Width - This.Left
        If This.Left+This.Width>_Screen.Width
            *Visualizzo lo zoom allineato a destra
            This.Left = Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left - This.Width + Objtoclient( This.oSource ,3) - 1
            If This.Left<0
                *poco spazio a destra del controllo. verifico se lo spazio � maggiore a destra o a sinistra
                *visualizzo nell'area maggiore lo zoom
                If Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left > RightSpace
                    *Lo visualizzo comunque allineato a destra
                    This.Left = Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left - This.Width + Objtoclient( This.oSource ,3) - 1
                Else
                    *Lo visualizzo allineato a sinistra
                    This.Left = Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left -1
                Endif
            Endif
        Endif

        With This.shpBorder
            .Top=0
            .Left=0
            .BorderWidth=2
            .BorderColor=0
            .BackStyle=0
            .Visible=.T.
            .Width = This.Width
            .Height = This.Height
            .Anchor = 15
        Endwith

        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(FORM_BACKGROUND_PICTURE)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
            Thisform.shpBorder.BorderColor = i_ThemesManager.GetProp(CONTAINER_BORDERCOLOR)
        Endif

        With This.scrDay
            .nAttValue = Day(i_datsys)
            .Visible = .T.
        Endwith

        With This.scrMonth
            .nAttValue = Month(i_datsys)
            .Width = 130
            .Visible = .T.
        Endwith

        With This.scrYear
            .nMinValue = Year(i_dIniDatAdvSpinner)
            .nMaxValue = Year(i_dFinDatAdvSpinner)
            .nAttValue = Year(i_datsys)
            .Width = 70
            .Visible = .T.
        Endwith
        This.CheckDate()
    Endproc

    Proc oBtnOk.Click
        This.Parent.oSource.Value = cp_CharToDate(Padl(Alltrim(Str(This.Parent.scrDay.nAttValue)),2,"0")+'/'+Padl(Alltrim(Str(This.Parent.scrMonth.nAttValue)),2,"0")+'/'+Padl(Alltrim(Str(This.Parent.scrYear.nAttValue)),4,"0"))
        This.Parent.oSource.bUpd = .T.
        This.Parent.oSource.Valid()
        This.Parent.oSource = .Null.
        Thisform.Release()
    Endproc

    Proc oBtnCancel.Click
        Thisform.Release()
    Endproc

    Proc CheckDate
        Lparameters nDirection
        Local nDayValue
        If !Empty(This.scrDay.nAttValue) And !Empty(This.scrMonth.nAttValue) And !Empty(This.scrYear.nAttValue)
            nDayValue = This.scrDay.nAttValue
            Do While Empty(cp_CharToDate(Padl(Alltrim(Str(m.nDayValue)),2,"0")+'/'+Padl(Alltrim(Str(This.scrMonth.nAttValue)),2,"0")+'/'+Padl(Alltrim(Str(This.scrYear.nAttValue)),4,"0")))
                nDayValue = m.nDayValue + Iif(m.nDirection=0, -1, 1)
                If m.nDayValue>This.scrDay.nMaxValue
                    nDayValue = This.scrDay.nMinValue
                Else
                    If m.nDayValue<This.scrDay.nMinValue
                        nDayValue = This.scrDay.nMaxValue
                    Endif
                Endif
            Enddo
            This.scrDay.nAttValue = m.nDayValue
            If i_VisualTheme<>-1

                If m.nDayValue=Day(i_datsys)
                    This.nOldForeColor = Iif(This.nOldForeColor<0, This.scrDay.GetForeColor(), This.nOldForeColor)
                    This.scrDay.SetForeColor(i_ThemesManager.GetProp(CONTAINER_BORDERCOLOR))
                Else
                    This.scrDay.SetForeColor(This.nOldForeColor)
                Endif
                If This.scrMonth.nAttValue=Month(i_datsys)
                    This.nOldForeColor = Iif(This.nOldForeColor<0, This.scrDay.GetForeColor(), This.nOldForeColor)
                    This.scrMonth.SetForeColor(i_ThemesManager.GetProp(CONTAINER_BORDERCOLOR))
                Else
                    This.scrMonth.SetForeColor(This.nOldForeColor)
                Endif
                If This.scrYear.nAttValue=Year(i_datsys)
                    This.nOldForeColor = Iif(This.nOldForeColor<0, This.scrDay.GetForeColor(), This.nOldForeColor)
                    This.scrYear.SetForeColor(i_ThemesManager.GetProp(CONTAINER_BORDERCOLOR))
                Else
                    This.scrYear.SetForeColor(This.nOldForeColor)
                Endif

                If cp_CharToDate(Padl(Alltrim(Str(m.nDayValue)),2,"0")+'/'+Padl(Alltrim(Str(This.scrMonth.nAttValue)),2,"0")+'/'+Padl(Alltrim(Str(This.scrYear.nAttValue)),4,"0"))=i_datsys

                Else

                Endif
            Endif
        Endif
    Endproc

    Proc Activate()
        i_curform=Thisform
        Return
    Endproc

    Proc Deactivate()
        Thisform.Release()
        Return
    Endproc

    Proc ecpQuit()
        Thisform.Release()
        Return
	endproc

Enddefine


DEFINE CLASS zoom_data_spinner_form As CpSysform

	WindowType=1
    oSource=.Null.
    Caption=""
    BorderStyle=0
    MaxButton=.F.
    MinButton=.F.
    ControlBox=.F.
    TitleBar=0
    ShowTips = .T. &&  mostra il tooltip
    Height=240
    Width=322
    nOldForeColor = -1
	KeyPreview = .T.

    Add Object shpBorder As Shape

	BorderColor = RGB(220,220,220)
	BackStyle = 1
	BackColor = RGB(60,60,60)
	
	bHighLightCurrentDay = .T.
	bShowDate = .T.
	bShowTime = .F.
	
	DIMENSION arrayDays(31,8)
	DIMENSION arrayMonths(12,8)
	DIMENSION arrayYears(YEAR(i_dFinDatAdvSpinner)-YEAR(i_dIniDatAdvSpinner),8)
	
	DIMENSION arrayHours(24,8)
	DIMENSION arrayMinutes(60,8)
	
	
	ADD OBJECT imgIcon AS Image WITH ;
		Top = 3, ;
		Left = 3, ;
		Width = 24, ;
		Height = 24, ;
		Borderwidth = 0,;
		backStyle = 0, ;
		Visible = .T.
	
	ADD OBJECT lblSelectedInfo as label WITH ;
		Top = 10, ;
		Left = 29, ;
		Width = 290, ;
		Height = 20, ;
		FontName = "Tahoma", ;
		FontSize = 12, ;
		ForeColor = 0, ;
		BackStyle = 0, ;
		Caption = "", ;
		Visible = .T.
		
	ADD OBJECT lblDay as label WITH ;
		Top = 40, ;
		Left = 1, ;
		Width = 65, ;
		Height = 12, ;
		FontName = "Tahoma", ;
		FontSize = 8, ;
		ForeColor = 0, ;
		BackStyle = 0, ;
		Caption = "Giorno", ;
		Alignment = 2, ;
		Visible = .T.

	ADD OBJECT scDay AS DateSpinner WITH ;
		TOP = 55, ;
		LEFT = 4
	
	ADD OBJECT lblMonth as label WITH ;
		Top = 40, ;
		Left = 70, ;
		Width = 150, ;
		Height = 12, ;
		FontName = "Tahoma", ;
		FontSize = 8, ;
		ForeColor = 0, ;
		BackStyle = 0, ;
		Caption = "Mese", ;
		Alignment = 2, ;
		Visible = .T.
		
	ADD OBJECT scMonth AS DateSpinner WITH ;
		TOP = 55, ;
		LEFT = 68, ;
		Width = 150
	
	ADD OBJECT lblYear as label WITH ;
		Top = 40, ;
		Left = 220, ;
		Width = 100, ;
		Height = 12, ;
		FontName = "Tahoma", ;
		FontSize = 8, ;
		ForeColor = 0, ;
		BackStyle = 0, ;
		Caption = "Anno", ;
		Alignment = 2, ;
		Visible = .T.
		
	ADD OBJECT scYear AS DateSpinner WITH ;
		TOP = 55, ;
		LEFT = 220, ;
		Width = 100
	
	ADD OBJECT lblHour as label WITH ;
		Top = 40, ;
		Left = 322, ;
		Width = 65, ;
		Height = 12, ;
		FontName = "Tahoma", ;
		FontSize = 8, ;
		ForeColor = 0, ;
		BackStyle = 0, ;
		Caption = "Ora", ;
		Alignment = 2, ;
		Visible = .F.
		
	ADD OBJECT scHour AS DateSpinner WITH ;
		TOP = 55, ;
		LEFT = 322, ;
		VISIBLE = .F.
	
	ADD OBJECT lblMinute as label WITH ;
		Top = 40, ;
		Left = 389, ;
		Width = 65, ;
		Height = 12, ;
		FontName = "Tahoma", ;
		FontSize = 8, ;
		ForeColor = 0, ;
		BackStyle = 0, ;
		Caption = "Minuto", ;
		Alignment = 2, ;
		Visible = .F.
	
	ADD OBJECT scMinute AS DateSpinner WITH ;
		TOP = 55, ;
		LEFT = 389, ;
		VISIBLE = .F.
	
	Add Object cmdSave As StdButton With uid="EHDGRTYJDD",Left=2, Top=192, Width=158,Height=45,;
        Picture="BMP\OK.BMP", Caption="", nPag=1 , ToolTipText = "Premere per confermare";
        , HelpContextID = 226640406 , Caption='\<Ok';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object cmdCancel As StdButton With uid="KIJGHTYSTR",Left=162, Top=192, Width=48,Height=45,;
        Picture="BMP\ESC.BMP", Caption="", nPag=1 , ToolTipText = "Premere per annullare";
        , HelpContextID = 226640406 , Caption='\<Esci';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
	
	
	Proc Init
        Lparameters oSource
        Local BottomSpace, RightSpace, i_oParentObject, nFontHeight
        This.oSource = m.oSource
        i_oParentObject = Iif(Type('i_fromobj')<>'O',i_curform,i_fromobj)
        This.Top =  This.oSource.Height + Objtoclient( This.oSource ,1)  + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,  Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top + 1
        This.Left = Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left -1
        *Verifiche per visualizzazione (verticali)
        *Se visualizzando lo zoom sotto il controllo non sta nello schermo provo a visualizzarlo sopra al controllo
        *Se visualizzandolo sopra al controllo non ci sta comunque verifico se lo spazio maggiore � sopra o sotto il
        *controllo e visualizzo li lo zoom
        BottomSpace = _Screen.Height - This.Top
        If This.Top+This.Height>_Screen.Height
            *Visualizzo lo zoom sopra al controllo
            This.Top =  (Objtoclient( This.oSource ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top) - This.Height - 1
            If This.Top<0
                *Poco spazio sopra al controllo. Verifico se lo spazio � maggiore sopra o sotto
                *visualizzo nell'area maggiore lo zoom ridimensionandolo
                If  (Objtoclient( This.oSource ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top) > BottomSpace
                    *Ridimensiono lo zoom e lo visualizzo comunque sopra al controllo
                    This.Height = Max(Objtoclient( This.oSource ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top + 4, Thisform.MinHeight)
                    This.Top = (Objtoclient( This.oSource ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top) - This.Height - 1
                Else
                    *Visualizzo sotto al controllo
                    This.Top =  This.oSource.Height + Objtoclient( This.oSource ,1)  + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) ) + Sysmetric(9) + i_oParentObject.Top + 1
                Endif
            Endif
        Endif

        *Verifiche per visualizzazione (orizzontali)
        *Se visualizzando lo zoom allineato a sinistra del controllo non sta nello schermo provo ad allinearlo a destra
        *Se allineato a destra non ci sta comunque verifico se lo spazio maggiore � a destra o a sinistra del
        *controllo e visualizzo li lo zoom
        RightSpace = _Screen.Width - This.Left
        If This.Left+This.Width>_Screen.Width
            *Visualizzo lo zoom allineato a destra
            This.Left = Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left - This.Width + Objtoclient( This.oSource ,3) - 1
            If This.Left<0
                *poco spazio a destra del controllo. verifico se lo spazio � maggiore a destra o a sinistra
                *visualizzo nell'area maggiore lo zoom
                If Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left > RightSpace
                    *Lo visualizzo comunque allineato a destra
                    This.Left = Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left - This.Width + Objtoclient( This.oSource ,3) - 1
                Else
                    *Lo visualizzo allineato a sinistra
                    This.Left = Objtoclient( This.oSource ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left -1
                Endif
            Endif
        Endif

        With This.shpBorder
            .Top=0
            .Left=0
            .BorderWidth=2
            .BorderColor=0
            .BackStyle=0
            .Visible=.T.
            .Width = This.Width
            .Height = This.Height
            .Anchor = 15
        Endwith

        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(SPINNER_DATETIME_PICKER_BACKGROUND)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
            Thisform.shpBorder.BorderColor = i_ThemesManager.GetProp(CONTAINER_BORDERCOLOR)
        Endif
        
		Thisform.imgIcon.Picture =  FORCEEXT(i_ThemesManager.GetBmpFromIco("date-time.ico",24), "bmp")
		Thisform.InitArrays()
		Thisform.InitCalendar()
		IF !EMPTY(Thisform.oSource.Value)
			with thisform
				.scDay.setValueDirect(RIGHT("00"+ALLTRIM(STR(DAY(.oSource.Value))),2))
				.scDay.setDefaultValue(RIGHT("00"+ALLTRIM(STR(DAY(.oSource.Value))),2))
				.scMonth.setValueDirect(.arrayMonths(MONTH(.oSource.Value),1)) 
				.scMonth.setDefaultValue(.arrayMonths(MONTH(.oSource.Value),1)) 
				.scYear.setValueDirect(ALLTRIM(STR(YEAR(.oSource.Value))))
				.scYear.setDefaultValue(ALLTRIM(STR(YEAR(.oSource.Value))))
			endwith
		endif
    Endproc
	
	PROC KeyPress
		LPARAMETERS nKeyCode, nShiftAltCtrl
		IF m.nKeyCode=13
			thisform.cmdSave.Click()
		ENDIF
	ENDPROC
	
	PROCEDURE bShowDate_Assign(bNewVal)
		this.bShowDate = m.bNewVal
		this.setUpSpinners()
	ENDPROC
	
	PROCEDURE bShowTime_Assign(bNewVal)
		this.bShowTime = bNewVal
		this.setUpSpinners()
	ENDPROC
	
	PROCEDURE cmdCancel.Click
		Thisform.Release()
	ENDPROC
	
	Proc cmdSave.Click
		local nMonthValueIdx
		nMonthValueIdx = ASCAN(g_Mese, This.Parent.scMonth.getValue(), 1, -1, 1, 15)
        This.Parent.oSource.Value = cp_CharToDate(Padl(Alltrim(This.Parent.scDay.getValue()),2,"0")+'/'+Padl(Alltrim(Str(nMonthValueIdx)),2,"0")+'/'+Padl(Alltrim(This.Parent.scYear.getValue()),4,"0"))
        This.Parent.oSource.bUpd = .T.
        This.Parent.oSource.Valid()
        This.Parent.oSource = .Null.
        Thisform.Release()
    Endproc
	
	PROCEDURE setUpSpinners
		LOCAL nAttLeft, nSpace
		nAttLeft = 5
		nSpace = 0
		WITH This
			.lblDay.Visible = .bShowDate
			.scDay.Visible = .bShowDate
			.lblMonth.Visible = .bShowDate
			.scMonth.Visible = .bShowDate 
			.lblYear.Visible = .bShowDate
			.scYear.Visible = .bShowDate 
			IF .bShowDate
				.scDay.Left = m.nAttLeft
				.lblDay.Left = m.nAttLeft
				nAttLeft = m.nAttLeft + m.nSpace + .scDay.Width
				.scMonth.Left = m.nAttLeft
				.lblMonth.Left = m.nAttLeft
				nAttLeft = m.nAttLeft + m.nSpace + .scMonth.Width
				.scYear.Left = m.nAttLeft
				.lblYear.Left = m.nAttLeft
				nAttLeft = m.nAttLeft + m.nSpace + .scYear.Width
			ELSE
				nAttLeft = 50
			ENDIF
			.scHour.Visible = .bShowTime
			.lblHour.Visible = .bShowTime
			.scMinute.Visible = .bShowTime
			.lblMinute.Visible = .bShowTime
			IF .bShowTime
				.scHour.Left = m.nAttLeft
				.lblHour.Left = m.nAttLeft
				nAttLeft = m.nAttLeft + m.nSpace + .scHour.Width
				.scMinute.Left = m.nAttLeft
				.lblMinute.Left = m.nAttLeft
				nAttLeft = m.nAttLeft + m.nSpace + .scMinute.Width + IIF(.bShowDate, 0, 50)
			ENDIF
			.Width = m.nAttLeft + 5
			.cmdSave.Width = .Width / 2 - 4 * (m.nSpace + 1)
			.cmdSave.Left = .Width / 2 - .cmdSave.Width - m.nSpace
			.cmdCancel.Width = .Width / 2 - 4 * (m.nSpace + 1)
			.cmdCancel.Left = .Width / 2 + m.nSpace
			LOCAL cDaySet, cMonthSet, cYearSet, dDateSet, bChangeNeeded, bApplyChanges
			cDaySet = this.scDay.getValue()
			cMonthSet = RIGHT("00"+STR(ASCAN(this.scMonth.aValues,this.scMonth.getValue() ,1,-1,1,15)),2)
			cYearSet = this.scYear.getValue()
			dDateSet = CTOD(m.cDaySet + "/" + m.cMonthSet + "/" + m.cYearSet )
			.setlabelInfo(dDateSet, this.scHour.getValue() + ":" + this.scMinute.getValue())
		ENDWITH
	ENDPROC 
	
	PROCEDURE setlabelInfo(dDate, cTime)
		this.lblSelectedInfo.Caption = IIF(This.bShowDate AND VARTYPE(dDate)='D', DTOC(dDate)+" ", "")
		this.lblSelectedInfo.Caption = this.lblSelectedInfo.Caption + IIF(this.bShowTime AND VARTYPE(cTime)='C',cTime,"")
	ENDPROC 
	
	PROCEDURE InitArrays
		LOCAL i
		WITH this
			FOR i=1 TO 31
				.arrayDays(i,1) = RIGHT("00"+ALLTRIM(STR(i)),2)
				.arrayDays(i,2) = i_cFontName
				.arrayDays(i,3) = 20
				.arrayDays(i,4) = 255
				.arrayDays(i,5) = 0
				.arrayDays(i,6) = .bHighLightCurrentDay AND i=DAY(DATE())
				.arrayDays(i,7) = ""
				.arrayDays(i,8) = -1
			NEXT
			FOR i=1 TO 12
				.arrayMonths(i,1) = g_MESE[i]
				.arrayMonths(i,2) = i_cFontName
				.arrayMonths(i,3) = 20
				.arrayMonths(i,4) = 255
				.arrayMonths(i,5) = 0
				.arrayMonths(i,6) = .bHighLightCurrentDay AND i=MONTH(DATE())
				.arrayMonths(i,7) = ""
				.arrayMonths(i,8) = -1
			NEXT
			FOR i=1 TO YEAR(i_dFinDatAdvSpinner)-YEAR(i_dIniDatAdvSpinner)
				.arrayYears(i,1) = ALLTRIM(STR(YEAR(i_dIniDatAdvSpinner)+i))
				.arrayYears(i,2) = i_cFontName
				.arrayYears(i,3) = 20
				.arrayYears(i,4) = 255
				.arrayYears(i,5) = 0
				.arrayYears(i,6) = .bHighLightCurrentDay AND .arrayYears(i,1)=ALLTRIM(STR(YEAR(DATE())))
				.arrayYears(i,7) = ""
				.arrayYears(i,8) = -1
			NEXT
			FOR i=1 TO 24
				.arrayHours(i,1) = RIGHT("00"+ALLTRIM(STR(i)),2)
				.arrayHours(i,2) = ""
				.arrayHours(i,3) = 20
				.arrayHours(i,4) = 255
				.arrayHours(i,5) = 0
				.arrayHours(i,6) = .F.
				.arrayHours(i,7) = ""
				.arrayHours(i,8) = -1
			NEXT
			FOR i=1 TO 60
				.arrayMinutes(i,1) = RIGHT("00"+ALLTRIM(STR(i-1)),2)
				.arrayMinutes(i,2) = ""
				.arrayMinutes(i,3) = 20
				.arrayMinutes(i,4) = 255
				.arrayMinutes(i,5) = 0
				.arrayMinutes(i,6) = .F.
				.arrayMinutes(i,7) = ""
				.arrayMinutes(i,8) = -1
			NEXT
		ENDWITH
	ENDPROC
	
	
	PROCEDURE InitCalendar
		WITH this
			ACOPY(This.arrayDays,aTempDay,1,ALEN(This.arrayDays),1)
			.scDay.oParentObject = This
			.scDay.setFontProp(i_cFontName)
			.scDay.InitValues(@aTempDay)
			.scDay.setForegroundImage(Forceext(cp_GetStdImg("SpinnerSxMask","png"),"png"))
			.scDay.setBackgroundImage(Forceext(cp_GetStdImg("SpinnerSx","png"),"png"))
			.scDay.InitGraphics()

			ACOPY(This.arrayMonths,aTempMonth,1,ALEN(This.arrayMonths),1)
			.scMonth.oParentObject = This
			.scMonth.InitValues(@aTempMonth)
			.scMonth.setForegroundImage(Forceext(cp_GetStdImg("SpinnerMask","png"),"png"))
			.scMonth.setBackgroundImage(Forceext(cp_GetStdImg("SpinnerCenterLarge","png"),"png"))
			.scMonth.InitGraphics()
			
			ACOPY(This.arrayYears,aTempYear,1,ALEN(This.arrayYears),1)
			.scYear.oParentObject = This
			.scYear.InitValues(@aTempYear)
			.scYear.setForegroundImage(Forceext(cp_GetStdImg("SpinnerDxMask","png"),"png"))
			.scYear.setBackgroundImage(Forceext(cp_GetStdImg("SpinnerDx","png"),"png"))
			.scYear.InitGraphics() 
			ACOPY(This.arrayHours,aTempHour,1,ALEN(This.arrayHours),1)
			.scHour.oParentObject = This
			.scHour.InitValues(@aTempHour)
			.scHour.setForegroundImage(Forceext(cp_GetStdImg("SpinnerMask","png"),"png"))
			.scHour.setBackgroundImage(Forceext(cp_GetStdImg("SpinnerSx","png"),"png"))
			.scHour.InitGraphics()
			ACOPY(This.arrayMinutes,aTempMinute,1,ALEN(This.arrayMinutes),1)
			.scMinute.oParentObject = This
			.scMinute.InitValues(@aTempMinute)
			.scMinute.setForegroundImage(Forceext(cp_GetStdImg("SpinnerMask","png"),"png"))
			.scMinute.setBackgroundImage(Forceext(cp_GetStdImg("SpinnerDx","png"),"png"))
			.scMinute.InitGraphics() 

			.scDay.setValueDirect(RIGHT("00"+ALLTRIM(STR(DAY(DATE()))),2))
			.scDay.setDefaultValue(RIGHT("00"+ALLTRIM(STR(DAY(DATE()))),2))
			.scMonth.setValueDirect(aTempMonth(MONTH(DATE()),1)) 
			.scMonth.setDefaultValue(aTempMonth(MONTH(DATE()),1)) 
			.scYear.setValueDirect(ALLTRIM(STR(YEAR(DATE()))))
			.scYear.setDefaultValue(ALLTRIM(STR(YEAR(DATE()))))
			.scHour.setValueDirect(RIGHT("00"+ALLTRIM(STR(HOUR(DATETIME()))),2))
			.scMinute.setValueDirect(RIGHT("00"+ALLTRIM(STR(MINUTE(DATETIME()))),2))
			.setUpSpinners()
			RELEASE aTempDay,aTempMonth,aTempYear, aTempHour, aTempMinute
		ENDWITH
	ENDPROC 
	
	PROCEDURE NotifyEvent(cEvent)
		if LOWER(cEvent)=="scday changed" OR LOWER(cEvent)=="scmonth changed" OR LOWER(cEvent)=="scyear changed" ;
			OR LOWER(cEvent)=="schour changed" OR LOWER(cEvent)=="scminute changed"
			LOCAL cDaySet, cMonthSet, cYearSet, dDateSet, bChangeNeeded, bApplyChanges
			cDaySet = this.scDay.getValue()
			cMonthSet = RIGHT("00"+STR(ASCAN(this.scMonth.aValues,this.scMonth.getValue() ,1,-1,1,15)),2)
			cYearSet = this.scYear.getValue()
			IF !EMPTY(cDaySet) AND !EMPTY(cMonthSet) AND !EMPTY(cYearSet)
				dDateSet = CTOD(m.cDaySet + "/" + m.cMonthSet + "/" + m.cYearSet )
				bChangeNeeded = EMPTY(dDateSet)
				bApplyChanges = .F.
				DO WHILE bChangeNeeded 
					cDaySet = RIGHT("00"+ ALLTRIM(STR(INT(VAL(cDaySet))-1)),2)
					dDateSet = CTOD(m.cDaySet + "/" + m.cMonthSet + "/" + m.cYearSet )
					bChangeNeeded = EMPTY(dDateSet)
					bApplyChanges = .T.
				ENDDO
				IF m.bApplyChanges 
					this.scDay.setValue(m.cDaySet)
				ENDIF
			ENDIF
			this.setLabelInfo(dDateSet,this.scHour.getValue()+":"+this.scMinute.getValue())
		Endif
	ENDPROC
	
	Proc Activate()
        i_curform=Thisform
        Return
    Endproc

    Proc Deactivate()
        Thisform.Release()
        Return
    Endproc

    Proc ecpQuit()
        Thisform.Release()
        Return
	Endproc
		
ENDDEFINE 

DEFINE CLASS DateSpinner AS Container
	Top = 0
	Left = 0
	Height = 130
	Width = 65
	Visible=.T.
	BackStyle = 0
	BorderStyle = 0
	BorderWidth = 0
	bMoving = .F. 
	nOrigY = 0
	nStartY = 0
	nOrigTime = 0
	nDeltaTime = 0
	cFontName = "Tahoma"
	nFontSize = 20
	nFontColor = RGB(90,90,90) &&RGB(255,255,255)
	nFontAttrib = 0
	nFontOffset = 0
	nAttTop = 0
	nRectHeight = 0
	nSerDay = 0
	nStartImage = 0
	nEndImage = 0
	cForegroundImage = ""
	cBackgroundImage = ""
	nDebugMode = 0
	bGraphInited = .F.
	cDefaultValue = ""
	bNotifyEvents = .F.
	bNeverNotify = .F.
	oParentObject = .null.
	
	DIMENSION aValues(1)

	ADD OBJECT "cmdKey" AS CommandButton WITH ;
		TOP = 0, ;
		Left = 0, ;
		Width = 65, ;
		Height = 130, ;
		Caption = "", ;
		Style = 1, ;
		Visible = .T.
	
	ADD Object "Fronte" AS Image WITH ;
		top=0, ;
		Left=0, ;
		Width=65, ;
		Height =130, ;
		Picture="", ;
		BackStyle = 0, ;
		Anchor = 0, ;
		Visible=.T.
	
	ADD OBJECT "spinTimer" AS TIMER WITH ;
		Enabled = .F., ;
		Interval = 0, ;
		bSpinDown = .T., ;
		nStep = 1, ;
		nSpeed = 0
		
	PROCEDURE Init
		This.nSerDay = This.getNewUid()  
		This.Fronte.Width = This.width
		This.setFontProp(i_cFontName, This.nFontSize, this.nFontColor, IIF(i_bFontItalic, 2, 0) + IIF(i_bFontBold, 1, 0) )
	ENDPROC
	
	PROCEDURE getNewUid
		LOCAL cTmpUid, i, nUid
		cTmpUid= SYS(2015)
		cTmpUid= RIGHT(m.cTmpUid, LEN(m.cTmpUid)-1)
		nUid = ""
		FOR i = 1 TO LEN(m.cTmpUid)
			IF VAL(SUBSTR(m.cTmpUid,i,1))=0
				nUid = m.nUid + ALLTRIM(STR(ASC(SUBSTR(m.cTmpUid,i,1))))
			ELSE
				nUid = m.nUid + ALLTRIM(STR(VAL(SUBSTR(m.cTmpUid,i,1))))
			ENDIF
		NEXT
		RETURN INT(VAL(m.nUid))
	ENDPROC 
	
	PROCEDURE oParentObject_Assign(oNewVal) 
		This.oParentObject = m.oNewVal
		IF VARTYPE(oNewVal)='O'
			This.bNotifyEvents = PEMSTATUS(This.oParentObject, "NotifyEvent", 5)
		ENDIF
	ENDPROC
	
	
	PROCEDURE destroy()
		cb_RemoveAdvSpinner(This.nSerDay)
	endproc	
	
	PROCEDURE Width_Assign(nNewVal)
		IF nNewVal>0
			This.Width = nNewVal 
			This.Fronte.Width = This.width
			This.RefreshGraphics()
		ENDIF
	ENDPROC 
	
	PROCEDURE Resize
		WITH This
			.Fronte.Width = .width
			.Fronte.height = .height
		ENDWITH 
		This.RefreshGraphics()
	ENDPROC
	
	PROCEDURE RefreshGraphics
		LOCAL cOldVal
		IF this.bGraphInited 
			cOldVal = This.getValue()
			cb_RemoveAdvSpinner(This.nSerDay)
			this.InitGraphics()
			IF !EMPTY(m.cOldVal)
				This.setValue(m.cOldVal)
			ENDIF
			With this.cmdKey
				.Width = this.width
				.Height = this.Height
			Endwith
		ENDIF
	ENDPROC
	
	PROC cmdKey.KeyPress
		LPARAMETERS nKeyCode, nShiftAltCtrl 
		local nAttIdx, nMaxIdx
		IF m.nKeyCode = 5 OR m.nKeyCode = 24
			nAttIdx = ASCAN(this.Parent.aValues, this.Parent.getValue(),1,-1,1,15)
			nMaxIdx = ALEN(this.Parent.aValues, 1)
		ENDIF
		DO CASE
			CASE m.nKeyCode = 5 		&&Up arrow
				If nAttIdx = 1
					nAttIdx = m.nMaxIdx
				ELSE
					nAttIdx = m.nAttIdx - 1
				ENDIF
				this.Parent.setValueDirect(this.Parent.aValues[m.nAttIdx, 1])
				nodefault
			CASE nKeyCode = 24 		&&Down arrow
				If nAttIdx = m.nMaxIdx
					nAttIdx = 1
				ELSE
					nAttIdx = m.nAttIdx + 1
				ENDIF
				this.Parent.setValueDirect(this.Parent.aValues[m.nAttIdx, 1])
				nodefault
			OTHERWISE
				*Do nothing
				DODEFAULT()
		ENDCASE
	ENDPROC
	
	PROCEDURE InitValues(aValRef)
		ACOPY(aValRef,This.aValues,1,ALEN(aValRef),1)
		This.RefreshGraphics()
	ENDPROC
	
	PROCEDURE setForegroundImage(cNewImagePath)
		This.cForegroundImage = m.cNewImagePath
		This.RefreshGraphics()
	ENDPROC
	
	PROCEDURE setBackgroundImage (cNewImagePath)
		This.cBackgroundImage = m.cNewImagePath
		This.RefreshGraphics()
	ENDPROC
	
	PROCEDURE setDefaultValue(cNewValue)
		LOCAL nIdx
		nIdx = ASCAN(This.aValues, m.cNewValue, 1, -1, 1, 14)
		IF m.nIdx>0
			This.cDefaultValue = m.cNewValue
		ENDIF
	ENDPROC 
	
	PROCEDURE setFontProp(cFontName, nFontSize, nFontColor, nFontAttrib, nFontOffset )
		IF VARTYPE(cFontName)='C'
			This.cFontName =  ALLTRIM(m.cFontName)
		ENDIF
		IF VARTYPE(nFontSize)='N'
			this.nFontSize = m.nFontSize
		ENDIF
		IF VARTYPE(nFontColor)='N'
			This.nFontColor = m.nFontColor
		ENDIF
		IF VARTYPE(nFontAttrib)='N'
			this.nFontAttrib = m.nFontAttrib
		ENDIF
		IF VARTYPE(nFontOffset)='N'
			this.nFontOffset = m.nFontOffset
		ENDIF
		This.RefreshGraphics()
	ENDPROC
	
	PROCEDURE setDebugMode(bDebug)
		This.nDebugMode = IIF(m.bDebug, 1, 0)
		This.RefreshGraphics()
	ENDPROC
	
	FUNCTION InitGraphics
		LOCAL bNotCreated, nTryIdx, nRetVal
		ACOPY(This.aValues,aValRef,1,ALEN(This.aValues),1)
		*Parametri advSpinner
			*UID controllo 					Numerico univoco
			*Array lista valori 			Array di caratteri passato per riferimento
			*Dimensione orizzontale			Numerico: derivato dalla dimensione dell'immagine che ospiter� il controllo
			*Dimensione verticale			Numerico: derivato dalla dimensione dell'immagine che ospiter� il controllo
			*Imamgine di sfondo				Carattere: contiene il percorso completo dell'immagine da utilizzare come sfondo
			*Imamgine di primo piano		Carattere: contiene il percorso completo dell'immagine da utilizzare in primo piano
												*quest'immagine deve essere pi� o meno trasparente in base all'effetto che
												*si vuol ottenere, un immagine completamente opaca non permetter� la
												*visualizzazione dei valori
			*Nome del font da utilizzare	Carattere: font da utilizzare per la rappresentazione dei valori
			*Dimensione del font			Numerico: dimensione del font da utilizzare per la rappresentazione dei valori
			*Colore del font				Numerico: colore con cui disegnare i valori
			*Attributi del font				Numerico: attributi da impostare nel font da utilizzare per la rappresentazione dei valori
												*Bold, italic, underline, ecc.
			*Font offset					Numerico: valore in pixel di distanza da aggiungere/rimuovere tra un valore ed un'altro
			*Debug mode						Numerico: Se passato a 1 la libreria disegner� anche le informaizoni del debug
		
		bNotCreated = .T.
		nTryIdx = 1
		DO WHILE bNotCreated
			This.nRectHeight = cb_InitAdvSpinner(This.nSerDay, @aValRef, This.Fronte.Width,This.Fronte.Height, ;
				This.cBackgroundImage, This.cForegroundImage, ;
				This.cFontName, this.nFontSize, This.nFontColor, this.nFontAttrib, This.nFontOffset, ;
				This.nDebugMode)
			IF This.nRectHeight>0
				bNotCreated = .F.
			ELSE
				nTryIdx = nTryIdx + 1
				bNotCreated = nTryIdx < 10
				This.nSerDay = This.getNewUid()
			ENDIF
		ENDDO
		RELEASE aValRef
		IF This.nRectHeight<=0
			nRetVal = -1
		ELSE
			*Mi posiziono sul primo valore dell'array (serie centrale per garantire il funzionamento su/gi�)
			this.nStartImage = This.nRectHeight  * ALEN(This.aValues,1)
			this.nEndImage = This.nStartImage * 2
			IF !this.bGraphInited
				This.nAttTop = (This.nRectHeight * ALEN(This.aValues,1) - This.Fronte.height / 2 + This.nRectHeight /2) - 1
			ENDIF
 			This.DrawSpinner(This.nAttTop )
 			this.bGraphInited = .T.
 			nRetVal = This.nRectHeight
		ENDIF
		RETURN m.nRetval
	ENDFUNC
	
	PROCEDURE setValue(sValue)
		LOCAL nIdx, nDest
		nIdx = ASCAN(This.aValues, sValue, 1, -1, 1, 14)
		IF nIdx>0
			*Mi posiziono sul primo valore dell'array (serie centrale per garantire il funzionamento su/gi�) e
			*aggiungo l'altezza dei singoli numeri per selezionare il valore passato alla funzione
			nDest = (This.nRectHeight * ALEN(This.aValues,1) - This.Fronte.height / 2 + This.nRectHeight / 2) + This.nRectHeight * (nIdx-1)
			DO WHILE .T.
				This.nAttTop = this.nAttTop + IIF(This.nAttTop<m.nDest, 1, -1)
				This.DrawSpinner(This.nAttTop )
				DOEVENTS
				IF This.nAttTop=m.nDest
					EXIT
				ENDIF
			ENDDO
		ENDIF
	ENDPROC
	
	PROCEDURE setValueDirect(sValue)
		LOCAL nIdx, nDest
		nIdx = ASCAN(This.aValues, sValue, 1, -1, 1, 14)
		IF nIdx>0
			*Mi posiziono sul primo valore dell'array (serie centrale per garantire il funzionamento su/gi�) e
			*aggiungo l'altezza dei singoli numeri per selezionare il valore passato alla funzione
			nDest = (This.nRectHeight * ALEN(This.aValues,1) - This.Fronte.height / 2 + This.nRectHeight / 2) + This.nRectHeight * (nIdx-1)
			This.nAttTop = m.nDest
			This.DrawSpinner(This.nAttTop )
			IF This.bNotifyEvents AND !This.bNeverNotify AND this.bGraphInited
				This.Parent.NotifyEvent(ALLTRIM(This.Name)+" Changed")
			ENDIF
		ENDIF
	ENDPROC
	
	FUNCTION getValue
		RETURN this.avalues(This.getValueIdx(),1)
	ENDFUNC
	
	FUNCTION getValueIdx
		LOCAL nHalf, nCenter,nTop, i, j
		nHalf = This.Fronte.Height / 2
		nCenter = This.nAttTop + m.nHalf
		nTop = 0
		j = 1
		FOR i=1 TO ALEN(this.aValues,1)*3
			IF nCenter >= m.nTop AND nCenter <= m.nTop + This.nRectHeight
				nRetval = j
			ENDIF
			nTop = nTop + This.nRectHeight 
			j = m.j + 1
			IF j > ALEN(this.aValues,1)
				j = 1
			ENDIF
		NEXT
		RETURN nRetVal 
	ENDFUNC
	
	FUNCTION getCellIdx
		LOCAL nHalf, nCenter,nTop, i, j
		nHalf = This.Fronte.Height / 2
		nCenter = This.nAttTop + m.nHalf
		nTop = 0
		j = 1
		FOR i=1 TO ALEN(this.aValues,1) * 3
			IF nCenter >= m.nTop AND nCenter <= m.nTop + This.nRectHeight
				nRetval = m.j
			ENDIF
			nTop = m.nTop + This.nRectHeight
			j = m.j + 1
		NEXT
		RETURN nRetVal 
	ENDFUNC
	
	PROCEDURE setCell(nCellIdx)
		LOCAL nTop
		nTop=This.nRectHeight * m.nCellIdx
		nTop = m.nTop - This.Fronte.height/2 - This.nRectHeight/2
		this.nAttTop = m.nTop
		This.DrawSpinner( This.nAttTop )
		IF This.bNotifyEvents AND !This.bNeverNotify AND this.bGraphInited
			This.Parent.NotifyEvent(ALLTRIM(This.Name)+" Changed")
		ENDIF
	ENDPROC 
	
	PROCEDURE nAttTop_Assign(nNewvalue)
		IF nNewValue >= this.nEndImage
			this.nAttTop = this.nStartImage
		ELSE
			IF nNewValue + this.Fronte.height <= this.nStartImage
				this.nAttTop = this.nEndImage - this.Fronte.height
			ELSE
				this.nAttTop = m.nNewvalue
			ENDIF
		ENDIF
	ENDPROC 
	
	PROCEDURE calcCellCenter()
		This.bMoving = .F.
		this.setCell(this.getCellIdx())
	ENDPROC

	PROCEDURE DrawSpinner(nCurrenTop)
		This.Fronte.PictureVal = cb_DrawAdvSpinner(This.nSerDay, m.nCurrenTop)
	ENDPROC 
	
	PROCEDURE Fronte.MouseMove
		LPARAMETERS nButton, nShift, nXCoord, nYCoord
		LOCAL nDelta
		IF nShift = 0 AND nButton = 1
			IF This.Parent.bMoving
				nDelta = m.nYCoord - This.Parent.nOrigY
				This.Parent.nOrigY = m.nYCoord
				This.Parent.nAttTop = This.Parent.nAttTop - m.nDelta
				This.Parent.spinTimer.Enabled = .F.
				This.Parent.DrawSpinner(This.Parent.nAttTop)
			Else
				This.Parent.bMoving=.T.
				This.Parent.spinTimer.Enabled = .F.
				This.Parent.nOrigY = m.nYCoord
				This.Parent.nStartY = m.nYCoord 
				This.Parent.nOrigTime = SECONDS()
			Endif
		ENDIF
		DODEFAULT()
	ENDPROC
	
	PROCEDURE checkSpin(nTimeRelease, nEndY)
		LOCAL nDeltaTime, nDeltaSpace, nSpeed
		IF This.bMoving
			This.bMoving = .F.
			nDeltaTime = (m.nTimeRelease - This.nOrigTime ) * 1000
			nDeltaSpace = ABS(m.nEndY - This.nStartY)
			nSpeed = INT( m.nDeltaSpace / m.nDeltaTime * 100)
			IF nSpeed >= 10
				This.spinTimer.Interval = INT(1000 / m.nSpeed )
				This.spinTimer.bSpinDown = (m.nEndY - This.nStartY) < 0
				This.spinTimer.nStep = 1
				This.spinTimer.nSpeed = m.nSpeed
				This.spinTimer.Enabled = .T.
			ELSE
				This.calcCellCenter()
			ENDIF
		ENDIF
	ENDPROC
	
	PROCEDURE spinTimer.Timer
		This.Enabled = .F.
		This.nStep = This.nStep + 1
		This.Interval = (This.nStep/6)^2+1
		This.Parent.nAttTop = This.Parent.nAttTop + 5 * IIF(This.bSpinDown, 1, -1)
		This.Parent.DrawSpinner(This.Parent.nAttTop)
		IF This.Interval >= this.nSpeed / 3
			This.Interval = 0
			This.Parent.calcCellCenter()
		ELSE
			This.Enabled = .T.
		ENDIF
	ENDPROC 
	
	PROCEDURE Fronte.MouseUp
		LPARAMETERS nButton, nShift, nXCoord, nYCoord	
		this.Parent.checkSpin(SECONDS(), nYCoord)
	ENDPROC 
	
	PROCEDURE Fronte.MouseLeave
		LPARAMETERS nButton, nShift, nXCoord, nYCoord	
		this.Parent.checkSpin(SECONDS(), nYCoord)
	ENDPROC 
	
	PROCEDURE Fronte.DblClick
		IF !EMPTY(This.parent.cDefaultValue)
			This.Parent.setvalue(This.parent.cDefaultValue)
			This.Parent.calcCellCenter()
		ENDIF
	ENDPROC

ENDDEFINE

*--- Obsoleta ---*
Define Class zoom_data_form As CpSysform
    WindowType=1
    oSource=.Null.
    AutoCenter=.T.
    Height=238
    Width=300
    Caption=""
    ShowTips = .T. &&  motra il tool tip
    Icon=i_cBmpPath+i_cStdIcon
    *-- Cambia OCX calendario
    *add object cal as olecontrol with oleclass="MSCAL.Calendar.7",width=300,height=200
    Add Object cal As OleControl With OleClass="MSACAL.MSACALCtrl.7",Width=300,Height=200
    Add Object oBtn_dtOD As CommandButton With ;
        Top = 210, Left = 6, Height = 23, Width = 73,;
        Caption = "Vai a oggi", ToolTipText = "Data odierna", Picture = "",;
        FontName="Arial", FontSize=8, FontBold=.F.

    Add Object N_Giorni As TextBox With Top=210, Left=233, Width=38, MaxLength=4, ToolTipText ="Numero di giorni"

    Add Object oBtn_ggIN As CommandButton With ;
        Top = 210, Left = 215, Height = 23, Width = 14,;
        Caption = "",  ToolTipText = "Giorni indietro", Picture = "PriorT.bmp"

    Add Object oBtn_ggAV As CommandButton With ;
        Top = 210, Left = 275, Height = 23, Width = 14,;
        Caption = "", ToolTipText = "Giorni in avanti", Picture = "NextT.bmp"

    Procedure oBtn_ggAV.Click
        * --- Clicca >
        Data=Ctod(Right('0'+Alltrim(Str(Thisform.cal.Day)),2)+'-'+Right('0'+Alltrim(Str(Thisform.cal.Month)),2)+'-'+Alltrim(Str(Thisform.cal.Year))) + Val(Thisform.N_Giorni.Value)
        Thisform.cal.Day=Day(Data)
        Thisform.cal.Month=Month(Data)
        Thisform.cal.Year=Year(Data)
    Endproc

    Procedure oBtn_ggIN.Click
        * --- Clicca <
        Data=Ctod(Right('0'+Alltrim(Str(Thisform.cal.Day)),2)+'-'+Right('0'+Alltrim(Str(Thisform.cal.Month)),2)+'-'+Alltrim(Str(Thisform.cal.Year))) - Val(Thisform.N_Giorni.Value)
        Thisform.cal.Day=Day(Data)
        Thisform.cal.Month=Month(Data)
        Thisform.cal.Year=Year(Data)
    Endproc

    Procedure oBtn_dtOD.Click
        * --- Vai a oggi
        Thisform.cal.Day=Day(Date())
        Thisform.cal.Month=Month(Date())
        Thisform.cal.Year=Year(Date())
    Endproc
    Proc cal.DblClick()
        Local N,sd,d,ov
        N=Thisform.oSource.cFormVar
        d='{^'+Alltrim(Str(This.Year))+'-'+Alltrim(Str(This.Month))+'-'+Alltrim(Str(This.Day))+'}'
        ov=Thisform.oSource.Parent.oContained.&N
        Thisform.oSource.Parent.oContained.&N=&d
        If Thisform.oSource.Check()
            Thisform.oSource.Parent.oContained.NotifyEvent(N+' Changed')
            cp_UpdatedFromObj(Thisform.oSource.Parent.oContained,Thisform.oSource.nPag)
        Else
            Thisform.oSource.Parent.oContained.&N=ov
            cp_ErrorMsg(MSG_VALUE_NOT_CORRECT_QM)
        Endif
        Createobject('timedzdaterelease',Thisform)
    Proc Init(oSource)
        This.cal.TitleFont.Size=10
        This.cal.TitleFont.Bold=.F.
        This.oSource=oSource
        This.Caption=cp_Translate(MSG_DATE)
        *this.cal.value=oSource.value
        If Not(Empty(oSource.Value))
            This.cal.Year=Year(oSource.Value)
            This.cal.Month=Month(oSource.Value)
            This.cal.Day=Day(oSource.Value)
        Endif
    Proc Activate()
        i_curform=This
    Func HasCPEvents(i_cOp)
        Return(i_cOp='ecpQuit' Or i_cOp='ecpSave')
    Proc ecpQuit()
        Thisform.Release()
    Proc ecpSave()
        Thisform.cal.DblClick()
Enddefine

Define Class timedzdaterelease As Timer
    oThis=.Null.
    oForm=.Null.
    Interval=100
    Proc Init(oForm)
        This.oThis=This
        This.oForm=oForm
    Proc Timer()
        This.oForm.Release()
        This.oThis=.Null.
        This.oForm=.Null.
Enddefine
