* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: cp_tmenu
* Ver      : 3.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Giorgio Montali
* Data creazione: 07/02/2005
* Aggiornato il : 07/07/2005
* Translated    :
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* ToolMenu
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"
#Define _TOOLMENU 3

Public oCpToolMenu
If g_UserScheduler
   Return
Endif
If Type("oCPToolMenu")='O'
	oCpToolMenu.Visible=!oCpToolMenu.Visible
	* --- Zucchetti Aulla Gestione del mark nel menu Window
	If i_bWindowMenu And i_VisualTheme = -1
		Set Mark Of Bar _TOOLMENU Of Visualizza To oCpToolMenu.Visible
	Endif
	* ---- Se non trovo il cursore contenente il men� ricarico tutto....
	If !Used( i_CUR_MENU )
		cp_menu()
	Endif
	* ---- carico con il cursore la tree view
	oCpToolMenu.SysOutLine.cCursor=i_CUR_MENU
	* --- Zucchetti Aulla fine
Else
	If i_VisualTheme <> -1
		oCpToolMenu=Newobject("cb_tmenu")
	Else
		oCpToolMenu=Newobject("Tcp_tmenu")
		oCpToolMenu.Movable=.T.
		oCpToolMenu.AlwaysOnTop=.T.
	Endif
	* --- Controllo Versione: nelle vers. precedenti, la propriet� non � definita
	If Version(5)>=900
		oCpToolMenu.Dock(1)
	Endif
	oCpToolMenu.Caption='Tool Menu'
	oCpToolMenu.Visible=.T.
	* --- Espansione del ToolMenu
	If Type("oCpToolMenu.SysOutLine.oTree.nodes(1)")='O'
		oCpToolMenu.SysOutLine.oTree.nodes(1).expanded=.T.
	Endif
	* --- Show the treeview
	oCpToolMenu.SysOutLine.oTree.SetFocus()
	* --- Zucchetti Aulla Gestione del mark nel menu Window
	If i_bWindowMenu And i_VisualTheme = -1
		Set Mark Of Bar _TOOLMENU Of Visualizza To .T.
	Endif
	* --- Zucchetti Aulla fine
Endif
_SCREEN.Resize()

*--- TreeMenuToolbar
Define Class cb_tmenu As CommandBar
    Add Object SysOutLine As Cp_ToolBarTree With Width=200, Height=_Screen.Height-20, Resizable = .T.;
        ,cLeafBmp=g_MLEAFBMP,cEvent = "Init",nIndent=20,cCursor="",cShowFields="cp_translate(VOCEMENU)",cNodeShowField="",cLeafShowField="",cNodeBmp=g_MNODEBMP, cLvlSep='.'

    Procedure Init()
        DoDefault(.T.)
        *--- Nessuno sfondo
        This.BackImg_V = 0
        This.BackImg_H = 0
        * ---- Se non trovo il cursore contenente il men� ricarico tutto....
        If !Used( i_CUR_MENU )
            cp_menu()
        Endif
        * ---- carico con il cursore la tree view
        This.SysOutLine.cCursor=i_CUR_MENU
        * --- riempio la tree view...
        This.SysOutLine.InitTree()
Enddefine

* --- Class definition
Define Class tcp_tmenu As Form
    Top    = 10
    Left   = 10

    * --- Standard Properties
    Width  = 195
    Height = 448
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    HelpContextID=188438941

    * --- Constant Properties
    Caption = "ToolMenu"
    Icon = "anag.ico"
    WindowType = 0
    MinButton = .F.
    Visible=.T.
    AlwaysOnTop=.T.


    Add Object SysOutLine As Cp_ToolBarTree With Width=200, Height=_Screen.Height-20, Resizable = .T.;
        ,cLeafBmp=g_MLEAFBMP,cEvent = "Init",nIndent=20,cCursor="",cShowFields="VOCEMENU",cNodeShowField="",cLeafShowField="",cNodeBmp=g_MNODEBMP, cLvlSep='.'

    Proc Init()
        * --- Controllo Versione: nelle vers. precedenti, la propriet� non � definita
        If Version(5)>=900 And i_VisualTheme = -1
            This.Dockable=1
        Endif
        If Version(5)>=700 And i_VisualTheme = -1
            This.AlwaysOnBottom=.T.
        Endif
        * ---- Se non trovo il cursore contenente il men� ricarico tutto....
        If !Used( i_CUR_MENU )
            cp_menu()
        Endif
        * ---- carico con il cursore la tree view
        This.SysOutLine.cCursor=i_CUR_MENU
        * --- riempio la tree view...
        This.SysOutLine.InitTree()
        * --- Controllo Versione: nelle vers. precedenti, il metodo non � definito
        If Version(5)>=900 And i_VisualTheme = -1
            This.Dock(1)
        Endif
    Endproc

    * --- Zucchetti Aulla Gestione del mark nel menu Window
    Procedure visible_assign
        Lparameters tAssign
        If i_bWindowMenu And i_VisualTheme = -1
            Set Mark Of Bar _TOOLMENU Of Visualizza To tAssign
        Endif
        Thisform.Visible=tAssign
        If Type("_screen._CpToolMenu")<>"U"
            _Screen._CpToolMenu.Visible = Thisform.Visible
        Endif
    Endproc
    * --- Zucchetti Aulla fine

    Proc Resize()
        This.SysOutLine.Width = This.Width
        This.SysOutLine.Height = This.Height
        This.SysOutLine.oTree.Width = This.Width
        This.SysOutLine.oTree.Height = This.Height
        DoDefault()
    Endproc


    Procedure Destroy()
        * --- Zucchetti Aulla Gestione del mark nel menu Window
        If i_bWindowMenu And i_VisualTheme = -1
            Set Mark Of Bar _TOOLMENU Of Visualizza To .F.
        Endif
        * --- Zucchetti Aulla fine
        oCpToolMenu=.F.
    Endproc

Enddefine


* --- cp_tmenu
Define Class Cp_ToolBarTree As cp_Treeview

    Proc Event(cEvent)
    Endproc

    Proc InitTree()
        Local cOldErr,cLev,cCursor,cKey
        Private err
        If Used(This.cCursor)
            Select (This.cCursor)
            err=.F.
            cCursor=This.cCursor
            cOldErr=On('ERROR')
            On Error err=.T.
            cLev=&cCursor->LvlKey
            On Error &cOldErr
            If Not err
                This.FillTree(.T.)
                * --- Elimina dalla Tree View i nodi non abilitati
                * --- occorre farlo a valle perch� anche i figli di
                * --- nodi disabilitati vanno eliminati
                This.DelNode()
            Else
                cp_errormsg(cp_MsgFormat(MSG_A__A_STRUCTURE_IS_NOT_CORRECT,This.cCursor),"!",'',.F.)
            Endif
        Endif

    Procedure DelNode()
        Local i_bModuleInstalled,i_cMod,cCur,nIndex
        cCur=This.cCursor
        Select( This.cCursor )
        Go Top
        Scan For Not &cCur..Enabled Or Not Empty(&cCur..modulo)
            * --- Se il record esaminato � gia stato eliminato
            * --- caso in cui ho una voce ocndizionata con figli condizionati
            * --- non lo valuto neppure..
            If Type("This.oTree.nodes('k'+Alltrim(&cCur..LvlKey)).Index")='N'
                * --- Se nodo disabilitato lo elimino..
                i_bModuleInstalled=Enabled
                i_cMod=Upper(Alltrim(&cCur..modulo))
                If !Empty(i_cMod)
                    If At('(',i_cMod)>0
                        i_cMod  = Strtran(i_cMod,"*","'")
                        i_bModuleInstalled=Eval(i_cMod)
                    Else
                        If At(','+i_cMod+',',','+i_cModules+',')=0
                            i_bModuleInstalled=.F.
                        Endif
                    Endif
                Endif
                If Not i_bModuleInstalled
                    nIndex=This.oTree.nodes('k'+Alltrim(&cCur..LvlKey)).Index
                    This.oTree.nodes.Remove(nIndex)
                Endif
            Endif
            Select( This.cCursor )
        Endscan

    Endproc

    Proc oTree.DblClick()
        Local cKey,cCursor,cOldErr
        Private err
        err=.F.
        If Not Isnull(This.SelectedItem) And This.SelectedItem.Index > 0
            If This.SelectedItem.Children = 0
                cCursor=This.Parent.cCursor
                cOldErr=On('ERROR')
                On Error err=.T.
                cKey=This.SelectedItem.Tag
                On Error &cOldErr
                If err
                    cKey=This.nodes.Item(1).Tag
                Endif

                cOldErr=On('ERROR')
                On Error err=.T.
                Select (This.Parent.cCursor)
                Go (cKey)
                On Error &cOldErr

                cOldErr=On('ERROR')
                On Error err=.T.
                i_Value=&cCursor->NAMEPROC
                On Error &cOldErr
                If Not err
                    i_Value = Strtran(Trim(i_Value),'#',' with ')
                    * ---Se clicco su separatore non lancio niente...
                    If Not Empty( i_Value )
                        * ---Zucchetti Aulla Aggiunta voce per gestione Recenti
                        Do mhit In cp_menu With i_Value, Alltrim(Strtran(cp_translate(&cCursor->VOCEMENU),'&'))
                    Endif
                Else
                    cp_errormsg(cp_MsgFormat(MSG_ERROR_WHILE_CALLING_PROCEDURE__,Upper(i_Value)),"!",'',.F.)
                Endif
            Endif
        Endif
        Return

    Proc oTree.expand(Node)
        This.Parent.nNodeExpanded=Node.Index
        This.Parent.nKeyExpanded=Node.Key
    Endproc
    Proc oTree.collapse(Node)
        This.Parent.nNodeCollapsed=Node.Tag
        This.Parent.nKeyCollapsed=Node.Key
    Endproc

    Proc oTree.NodeClick(oNode)
        This.Parent.nNoExecFill=.T.
        This.Parent.nNoExecFill=.F.
        If This.Parent.bRightMouse And This.Parent.nDblClick<>1
            This.Parent.bRightMouse=.F.
        Else
            If This.Parent.nDblClick=1
                This.Parent.nDblClick=2
            Endif
        Endif

    Proc oTree.Click()
    Endproc

Enddefine
