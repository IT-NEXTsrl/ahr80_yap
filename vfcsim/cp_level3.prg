* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: oAddLevel
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Samuele Masetto
* Data creazione: 08/03/99
* Aggiornato il : 08/03/99
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Oggetto per l'esplosione distinta base
* ----------------------------------------------------------------------------
Parameters cCursor,cCursorNodes,cTotField
*cCursor=nome del cursore dal quale si deve creare il campo di livello per il treeview
*cCursorNodes=elenco dei campi che fungono da nodo del cursore "cCursor"
*cTotField=elenco dei campi di totalizzazione di nodo
Private i_retval,i_Rows
i_retval = .T.
i_Rows = 0
Createobject("tAddLevel",cCursor,cCursorNodes,cTotField)
Return(i_retval)

Define Class tAddLevel As Custom
    cCursor=''
    cCursorNodes=''
    cTmpCursor='prova'
    Dimension cTotField[1]
    cTotField[1]=''
    Dimension aNodes[1]
    aNodes[1]=''
    i_BASE_MIN=35
    i_BASE_MAX=128
    Proc Init(cCursor,cCursorNodes,cTotField)
        Local i,cTmp
        This.cCursor=cCursor
        This.cCursorNodes=cCursorNodes
        *---Costruisce l'array dei campi di totalizzazione
        If Type('cTotField')<>'L'
            cTmp=cTotField+','
            i=0
            Do While At(",",cTmp)<>0
                i=i+1
                Dimension This.cTotField[i]
                This.cTotField[i]=Left(cTmp,At(",",cTmp)-1)
                cTmp=Substr(cTmp,At(",",cTmp)+1)
            Enddo
        Endif
        *---Costruisce l'array dei nodi
        cTmp=cCursorNodes+','
        i=0
        Do While At(",",cTmp)<>0
            i=i+1
            Dimension This.aNodes[i]
            This.aNodes[i]=Left(cTmp,At(",",cTmp)-1)
            cTmp=Substr(cTmp,At(",",cTmp)+1)
        Enddo
        *---Modifica il cursore con il campo di livello ("LvlKey")
        i_retval=This.Explode()
        Return
    Func Explode()
        Local cField,cWhere,i,i_tmp,cLevel,nCntLevel,nCount
        Private nCntTot
        Dimension nCntTot[1,alen(this.cTotField)]
        For i=1 To Alen(This.cTotField)
            nCntTot[1,i]=0
        Next
        If Not Used(This.cCursor)
            Return(.F.)
        Endif
        *---Creazione del cursore temporaneo
        This.CreateCursor(This.cTmpCursor)
        i_tmp=This.aNodes[1]
        nCntLevel=0
        Select (This.cCursor)
        If Not Eof()
            Select Distinct &i_tmp As NodeKey From (This.cCursor) Into Cursor __tmp1__ nofilter
            Scan
                nCntLevel=nCntLevel+1
                *cLevel=alltrim(str(nCntLevel))
                cLevel=This.GetLevel(nCntLevel)
                cField=This.aNodes[1]
                cWhere=' where '
                This.AppendField(This.cTmpCursor,cField)
                This.SegnaTab(This.cTmpCursor,cLevel)
                i_tmp=This.aNodes[1]
                If Alen(This.aNodes)=1
                    This.AddElement(cWhere+i_tmp+'='+cp_ToStr(__tmp1__.NodeKey),cLevel)
                Else
                    This.ScanCursor(cField,cWhere,"__tmp1__",2,cLevel)
                Endif
                cTmpLevel=''
                *--Aggiornamento del campo totalizzatore
                For i=1 To Alen(This.cTotField)
                    This.ReplaceTot(nCntTot[1,i],cLevel,i)
                    nCntTot[1,i]=0
                Next
                Select __tmp1__
            Endscan
        Endif
        If Used('__tmp1__')
            Select __tmp1__
            Use
        Endif
        If Used('__tmp2__')
            Select __tmp2__
            Use
        Endif
        Select (This.cCursor)
        Use
        Select (This.cTmpCursor)
        Go Top
        Local i_b,i_a,bEOF
        bEOF=Eof()
        If Not bEOF
            Dimension i_a[reccount(),fcount()]
        Else
            Dimension i_a[1,fcount()]
        Endif
        Dimension i_b[1]
        Copy To Array i_a
        Afields(i_b)
        Create Cursor (This.cCursor) From Array i_b
        Index On LvlKey Tag LvlKey COLLATE "MACHINE"
        If Not bEOF
            Append From Array i_a
        Endif
        Select (This.cTmpCursor)
        Use
        Return(.T.)
    Proc AddElement(cWhere,cLevel)
        Local nCntLevel,cTmpLevel
        nCntLevel=0
        Select * From (This.cCursor) &cWhere Into Cursor __curs__
        Scan
            nCntLevel=nCntLevel+1
            cTmpLevel=Alltrim(cLevel+Iif(nCntLevel<>0,'.'+This.GetLevel(nCntLevel),''))
            This.AppendField(This.cTmpCursor)
            This.SegnaTab(This.cTmpCursor,cTmpLevel)
            Select (This.cTmpCursor)
            *brows
            Select __curs__
        Endscan
        If Used('__curs__')
            Select __curs__
            Use
        Endif
        Return
    Proc CreateCursor(cTableName)
        Local i_a,nLen,i,j
        Dimension i_a[1]
        Afields(i_a)
        nLen=Alen(i_a,1)
        Dimension i_a[nLen+1,alen(i_a,2)]
        *copia la struttura dell'ultimo elemento nei nuovi campi che si devono inserire
        For i=nLen To nLen+1
            For j=1 To Alen(i_a,2)
                i_a[i,j]=i_a[nLen,j]
            Next
        Next
        *aggiunta dei nuovi campi
        i_a[nLen+1,1]="LvlKey"
        i_a[nLen+1,2]="C"
        i_a[nLen+1,3]="200"
        i_a[nLen+1,4]="0"
        Create Cursor (cTableName) From Array i_a
        Return
    Proc AppendField(cTableName,cFields)
        Local i_a,i,cValues,cTotField,i
        Dimension i_a[1]
        Scatter To i_a
        If Pcount()>1
            cValues=''
            For i=1 To Alen(i_a)
                cValues=cValues+cp_ToStr(i_a[i])+','
            Next
            cValues=Left(cValues,Len(cValues)-1)
            Dimension i_a[1,2]
            CreateKeysArray(@i_a,cValues,cFields)
            cSearch=FillWhereFromArray(@i_a," and ")
            Select * From (This.cCursor) Where &cSearch Into Cursor __curs__
            Dimension i_a[1]
            Scatter To i_a
        Else
            For i=1 To Alen(This.cTotField)
                cTotField=This.cTotField[i]
                If Not Empty(cTotField)
                    nCntTot[alen(nCntTot,1),i]=nCntTot[alen(nCntTot,1),i]+&cTotField
                Endif
            Next
        Endif
        Select (cTableName)
        Append From Array i_a
        Return
    Proc SegnaTab(cTableName,sLevel)
        Select (cTableName)
        Replace LvlKey With sLevel
        Return
    Func GetLevel(i_nLevel)
        * --- ogni livello contiene caratteri ASCII dal 35 dec. al 127 dec.
        * --- se i livello sono da due cifre ci stanno 66 livelli
        Local i_cLevel,i_nMod,i_nDiv
        i_nMod=i_nLevel%(This.i_BASE_MAX-This.i_BASE_MIN)
        i_cLevel=Chr(This.i_BASE_MIN+i_nMod)
        i_nDiv=Int(i_nLevel/(This.i_BASE_MAX-This.i_BASE_MIN))
        Do While i_nDiv<>0
            i_nMod=Int(i_nDiv)%(This.i_BASE_MAX-This.i_BASE_MIN)
            i_cLevel=Chr(This.i_BASE_MIN+i_nMod)+i_cLevel
            i_nDiv=Int(i_nDiv/(This.i_BASE_MAX-This.i_BASE_MIN))
        Enddo
        Return(i_cLevel)
    Endfunc
    Proc ScanCursor(cField,cWhere,cCursor,nIdx,cLevel)
        Local nTmpIdx,nCnt,cTmpLevel,cTotField,i
        cField=cField+","+This.aNodes[nIdx]
        i_tmp=This.aNodes[nIdx-1]
        Select (cCursor)
        cWhere=cWhere+This.aNodes[nIdx-1]+'='+cp_ToStr(&cCursor->NodeKey)
        Select Distinct &cField As NodeKey From (This.cCursor) &cWhere Into Cursor ("__tmp"+Alltrim(Str(nIdx))+"__")
        nTmpIdx=nIdx
        i_tmp=This.aNodes[nIdx]
        nCnt=0
        cTmpLevel=cLevel
        Scan
            nCnt=nCnt+1
            cTmpLevel=Alltrim(cLevel+Iif(nCnt<>0,'.'+This.GetLevel(nCnt),''))
            This.AppendField(This.cTmpCursor,cField)
            This.SegnaTab(This.cTmpCursor,cTmpLevel)
            Select ("__tmp"+Alltrim(Str(nTmpIdx))+"__")
            If nTmpIdx=Alen(This.aNodes)
                This.AddElement(cWhere+' and '+i_tmp+'='+cp_ToStr(NodeKey),cTmpLevel)
            Else
                Dimension nCntTot[nIdx,alen(this.cTotField)]
                For i=1 To Alen(This.cTotField)
                    nCntTot[nIdx,i]=0
                Next
                *---Chiamata ricorsiva
                This.ScanCursor(cField,cWhere+" and ","__tmp"+Alltrim(Str(nTmpIdx))+"__",nTmpIdx+1,cTmpLevel)
                *wait wind "Livello:"+cTmpLevel+"totale:"+str(ncnttot[nIdx])+"iNsix:"+str(nIdx)
                *--Aggiornamento del campo totalizzatore
                For i=1 To Alen(This.cTotField)
                    This.ReplaceTot(nCntTot[nIdx,i],cTmpLevel,i)
                    *--Aggiorna il totalizzatore del nodo padre
                    nCntTot[nIdx-1,i]=nCntTot[nIdx-1,i]+nCntTot[nIdx,i]
                    nCntTot[nIdx,i]=0
                Next
            Endif
            Select ("__tmp"+Alltrim(Str(nTmpIdx))+"__")
            cTmpLevel=cLevel
            cLevel=cTmpLevel
        Endscan
        If Used("__tmp"+Alltrim(Str(nTmpIdx))+"__")
            Select ("__tmp"+Alltrim(Str(nTmpIdx))+"__")
            Use
        Endif
        Return
    Proc ReplaceTot(nTot,cLevel,nIdx)
        Local cTotField
        If Not Empty(This.cTotField[nIdx])
            cTotField=This.cTotField[nIdx]
            Update (This.cTmpCursor) Set &cTotField=nTot Where LvlKey==cLevel
            Select (This.cTmpCursor)
        Endif
        Return
Enddefine
Proc CreateKeysArray(i_a,cKey1,cKey2)
    Local i,cTmp,c1,c2,N,cTmp2
    cTmp=cKey1+","
    cTmp2=Iif(Empty(cKey2),cKey1,cKey2)+","
    i=0
    Do While At(",",cTmp)<>0
        i=i+1
        Dimension i_a[i,2]
        i_a[i,1]=Left(cTmp,At(",",cTmp)-1)
        cTmp=Substr(cTmp,At(",",cTmp)+1)
        i_a[i,2]=Left(cTmp2,At(",",cTmp2)-1)
        cTmp2=Substr(cTmp2,At(",",cTmp2)+1)
    Enddo
    Return
Func FillWhereFromArray(i_a,cToAdd)
    Local cSearch,i
    cSearch=''
    For i=1 To Alen(i_a,1)
        cSearch=cSearch+i_a[i,2]+"=="+cp_ToStr(&i_a[i,1])+cToAdd
    Next
    If Not Empty(cSearch)
        cSearch=Left(cSearch,Len(cSearch)-Len(cToAdd))
    Endif
    Return(cSearch)
