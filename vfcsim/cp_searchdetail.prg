* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_searchdetail                                                 *
*              Gestione ricerca detail                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-12-20                                                      *
* Last revis.: 2014-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_searchdetail",oParentObject))

* --- Class definition
define class tcp_searchdetail as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 230
  Height = 8
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-23"
  HelpContextID=192233995
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_searchdetail"
  cComment = "Gestione ricerca detail"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CtrlType = space(1)
  w_SearchValue = space(100)
  o_SearchValue = space(100)
  w_SearchValueN = 0
  o_SearchValueN = 0
  w_SearchValueD = ctod('  /  /  ')
  o_SearchValueD = ctod('  /  /  ')
  w_TypeFilter = space(10)
  w_TypeFilter1 = space(10)
  o_TypeFilter1 = space(10)
  w_ExpandForm = 0
  w_SecondSearch = .F.
  w_CHECKED = .F.
  o_CHECKED = .F.
  w_Searchwrap = space(1)
  w_RemPrevFilter = space(1)
  w_ZoomValueN = .NULL.
  w_ZoomValue = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_searchdetail
  Top    = -1000
  Left   = -1000
  *Left=_Screen.Width + 100
  ShowWindow=1
  AlwaysOnTop=.t.
  Closable=.f.
  TitleBar=0
  * --- disabilito il men� sulla barra
  ControlBox = .f.
  * ---- disabilito l'ingrandimento della maschera
  MinButton=.f.
  MaxButton=.f.
  BorderStyle=2
  *--- Form di sistema non modificabile da window menu
  bSysForm = .T.
  
  *-- INTERNAL USE: Determines whether or not this form should be be released when the deactivate even fires.
  	rundeactivaterelease = .T.
  *-- INTERNAL USE: Used to tell the class whether the mouse (cursor) is within the boundaries of the Search/Filter form or not.
  	_inform = .T.
  *-- INTERNAL USE: Used to tell the class whether the form is fading or not.
  	_infademode = .T.
  
  *-- INTERNAL USE: Holds the SECONDS since midnight when the last user interaction with this form occurred. Ensures that fading of the form will not occur while the user is actively using the form. See the FadeWait property of the GridExtra class.
  	lastuseraction = 0
    
  *-- INTERNAL USE: Determines whether or not this form should be be released when the deactivate even fires.
    rundeactivaterelease = .T.
    
  *-- imposto di non sizare la form secondo le impostazioni di interfaccia
  bCheckSize=.t. 
  
  ADD OBJECT oTimerInform AS TimerInform WITH ;
    		Top = 355, ;
    		Left = 42, ;
    		Name = "oTimerInform"
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_searchdetailPag1","cp_searchdetail",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSearchValue_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_searchdetail
    this.Pages(1).oPag.addobject("oStr_StringAdvanced","cp_StringAdvanced")      
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomValueN = this.oPgFrm.Pages(1).oPag.ZoomValueN
    this.w_ZoomValue = this.oPgFrm.Pages(1).oPag.ZoomValue
    DoDefault()
    proc Destroy()
      this.w_ZoomValueN = .NULL.
      this.w_ZoomValue = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- cp_searchdetail
    This.Height=40
    This.oPgFrm.Height=40
    this.left=this.oParentObject.Parent.oRefForm.Left+objToClient(this.oparentObject,2)+iif(type("_SCREEN.TBMDI.Visible")='L' AND _SCREEN.TBMDI.Visible,0, ;
              IIF(this.oParentObject.Parent.oRefForm.BorderStyle=3 and !this.oParentObject.Parent.oRefForm.bApplyFormDecorator,SYSMETRIC(3),0))
    if this.left<_screen.Width and this.left+This.width>_screen.Width
       this.left=_screen.Width-This.width-4
    endif
    this.nOldLeft=this.left
    this.top=this.oParentObject.Parent.oRefForm.Top+objToClient(this.oParentObject.parent,1)+iif(type("_SCREEN.TBMDI.Visible")='L' AND _SCREEN.TBMDI.Visible,0,;
             IIF(this.oParentObject.Parent.oRefForm.BorderStyle=3 and !this.oParentObject.Parent.oRefForm.bApplyFormDecorator,SYSMETRIC(4),0)+;
             IIF(this.oParentObject.Parent.oRefForm.Titlebar=1,SYSMETRIC(9),0))+this.oParentObject.parent.height
    
    this.oTimerInform.Enabled=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CtrlType=space(1)
      .w_SearchValue=space(100)
      .w_SearchValueN=0
      .w_SearchValueD=ctod("  /  /  ")
      .w_TypeFilter=space(10)
      .w_TypeFilter1=space(10)
      .w_ExpandForm=0
      .w_SecondSearch=.f.
      .w_CHECKED=.f.
      .w_Searchwrap=space(1)
      .w_RemPrevFilter=space(1)
        .w_CtrlType = 'C'
          .DoRTCalc(2,4,.f.)
        .w_TypeFilter = iif(.w_CHECKED, '=', iif(EMPTY(.w_TypeFilter), IIF(g_RicPerCont='T', 'Like %', 'Like'), .w_TypeFilter))
        .w_TypeFilter1 = iif(.w_CHECKED OR EMPTY(.w_TypeFilter1), '=', .w_TypeFilter1)
        .w_ExpandForm = 1
        .w_SecondSearch = .f.
          .DoRTCalc(9,9,.f.)
        .w_Searchwrap = 'S'
        .w_RemPrevFilter = 'S'
      .oPgFrm.Page1.oPag.ZoomValueN.Calculate()
      .oPgFrm.Page1.oPag.ZoomValue.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_searchdetail
    endproc
    
    PROCEDURE Deactivate
    		IF !this.RunDeactivateRelease
    			RETURN
    		ENDIF
    		this.RELEASE()
    ENDPROC
        
    PROCEDURE GotFocus
        this.oTimerInform.Enabled=.f.
    		DoDefault()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CHECKED<>.w_CHECKED
            .w_TypeFilter = iif(.w_CHECKED, '=', iif(EMPTY(.w_TypeFilter), IIF(g_RicPerCont='T', 'Like %', 'Like'), .w_TypeFilter))
        endif
        if .o_CHECKED<>.w_CHECKED
            .w_TypeFilter1 = iif(.w_CHECKED OR EMPTY(.w_TypeFilter1), '=', .w_TypeFilter1)
        endif
        if .o_TypeFilter1<>.w_TypeFilter1
          .Calculate_DQJXHCMTBC()
        endif
        .DoRTCalc(7,7,.t.)
        if .o_SearchValueD<>.w_SearchValueD.or. .o_SearchValueN<>.w_SearchValueN.or. .o_SearchValue<>.w_SearchValue
            .w_SecondSearch = .f.
        endif
        .oPgFrm.Page1.oPag.ZoomValueN.Calculate()
        .oPgFrm.Page1.oPag.ZoomValue.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomValueN.Calculate()
        .oPgFrm.Page1.oPag.ZoomValue.Calculate()
    endwith
  return

  proc Calculate_MEZADULZWN()
    with this
          * --- Assign type variabile filter/search
          .w_CtrlType = cp_SearchDetailR(this, "Type")
          .w_TypeFilter = iif(.w_CTRLTYPE='C', 'Like', '=')
    endwith
  endproc
  proc Calculate_DQJXHCMTBC()
    with this
          * --- Assign type filter numeric/data
          .w_TypeFilter = .w_TypeFilter1
    endwith
  endproc
  proc Calculate_CGMNTNQRZM()
    with this
          * --- Carica zoom
          cp_SearchDetailR(this;
              ,"LoadZoom";
             )
    endwith
  endproc
  proc Calculate_LBXNOJPNRF()
    with this
          * --- Seleziona/Deseleziona record
          cp_SearchDetailR(this;
              ,"Checked";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSearchValue_1_3.enabled = this.oPgFrm.Page1.oPag.oSearchValue_1_3.mCond()
    this.oPgFrm.Page1.oPag.oSearchValueN_1_4.enabled = this.oPgFrm.Page1.oPag.oSearchValueN_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSearchValueD_1_5.enabled = this.oPgFrm.Page1.oPag.oSearchValueD_1_5.mCond()
    this.oPgFrm.Page1.oPag.oTypeFilter_1_10.enabled = this.oPgFrm.Page1.oPag.oTypeFilter_1_10.mCond()
    this.oPgFrm.Page1.oPag.oTypeFilter1_1_11.enabled = this.oPgFrm.Page1.oPag.oTypeFilter1_1_11.mCond()
    this.oPgFrm.Page1.oPag.oSearchwrap_1_16.enabled = this.oPgFrm.Page1.oPag.oSearchwrap_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSearchValue_1_3.visible=!this.oPgFrm.Page1.oPag.oSearchValue_1_3.mHide()
    this.oPgFrm.Page1.oPag.oSearchValueN_1_4.visible=!this.oPgFrm.Page1.oPag.oSearchValueN_1_4.mHide()
    this.oPgFrm.Page1.oPag.oSearchValueD_1_5.visible=!this.oPgFrm.Page1.oPag.oSearchValueD_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oTypeFilter_1_10.visible=!this.oPgFrm.Page1.oPag.oTypeFilter_1_10.mHide()
    this.oPgFrm.Page1.oPag.oTypeFilter1_1_11.visible=!this.oPgFrm.Page1.oPag.oTypeFilter1_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_MEZADULZWN()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZoomValueN.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomValue.Event(cEvent)
        if lower(cEvent)==lower("LoadZoom")
          .Calculate_CGMNTNQRZM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZoomValue row checked") or lower(cEvent)==lower("w_ZoomValue row unchecked") or lower(cEvent)==lower("ZoomValue menucheck") or lower(cEvent)==lower("w_ZoomValueN row checked") or lower(cEvent)==lower("w_ZoomValueN row unchecked") or lower(cEvent)==lower("ZoomValueN menucheck")
          .Calculate_LBXNOJPNRF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSearchValue_1_3.value==this.w_SearchValue)
      this.oPgFrm.Page1.oPag.oSearchValue_1_3.value=this.w_SearchValue
    endif
    if not(this.oPgFrm.Page1.oPag.oSearchValueN_1_4.value==this.w_SearchValueN)
      this.oPgFrm.Page1.oPag.oSearchValueN_1_4.value=this.w_SearchValueN
    endif
    if not(this.oPgFrm.Page1.oPag.oSearchValueD_1_5.value==this.w_SearchValueD)
      this.oPgFrm.Page1.oPag.oSearchValueD_1_5.value=this.w_SearchValueD
    endif
    if not(this.oPgFrm.Page1.oPag.oTypeFilter_1_10.RadioValue()==this.w_TypeFilter)
      this.oPgFrm.Page1.oPag.oTypeFilter_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTypeFilter1_1_11.RadioValue()==this.w_TypeFilter1)
      this.oPgFrm.Page1.oPag.oTypeFilter1_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSearchwrap_1_16.RadioValue()==this.w_Searchwrap)
      this.oPgFrm.Page1.oPag.oSearchwrap_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRemPrevFilter_1_17.RadioValue()==this.w_RemPrevFilter)
      this.oPgFrm.Page1.oPag.oRemPrevFilter_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SearchValue = this.w_SearchValue
    this.o_SearchValueN = this.w_SearchValueN
    this.o_SearchValueD = this.w_SearchValueD
    this.o_TypeFilter1 = this.w_TypeFilter1
    this.o_CHECKED = this.w_CHECKED
    return

enddefine

* --- Define pages as container
define class tcp_searchdetailPag1 as StdContainer
  Width  = 229
  height = 81
  stdWidth  = 229
  stdheight = 81
  resizeXpos=135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSearchValue_1_3 as StdField with uid="DIMWKFNASX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SearchValue", cQueryName = "SearchValue",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 140523114,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=3, Top=1, InputMask=replicate('X',100)

  func oSearchValue_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oSearchValue_1_3.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE<>'C')
    endwith
  endfunc

  add object oSearchValueN_1_4 as StdField with uid="ODBRSHKJJI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SearchValueN", cQueryName = "SearchValueN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 140842602,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=3, Top=1, cSayPict='"9999999999999.9999"', cGetPict='"9999999999999.9999"'

  func oSearchValueN_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oSearchValueN_1_4.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE<>'N')
    endwith
  endfunc

  add object oSearchValueD_1_5 as StdField with uid="YGGDOAVEMR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SearchValueD", cQueryName = "SearchValueD",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 140801642,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=3, Top=1, bNoZoomDateType=.t.

  func oSearchValueD_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oSearchValueD_1_5.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE<>'D')
    endwith
  endfunc


  add object oBtn_1_6 as StdButton with uid="BDBFGXXSGH",left=150, top=1, width=25,height=25,;
    CpPicture="find.ico", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 39831982;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        cp_SearchDetailR(this.Parent.oContained,"Search")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CtrlType='C' and not empty(.w_SearchValue) OR .w_CtrlType<>'C')
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="NTXGYAVXWY",left=177, top=1, width=25,height=25,;
    CpPicture="filter.ico", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire il filtro";
    , HelpContextID = 48166113;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        cp_SearchDetailR(this.Parent.oContained,"Filter")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT .w_CHECKED)
      endwith
    endif
  endfunc

  func oBtn_1_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CHECKED)
     endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="TVJQHCICBA",left=177, top=1, width=25,height=25,;
    CpPicture="filter.ico", caption="", nPag=1;
    , HelpContextID = 48166113;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        cp_SearchDetailR(this.Parent.oContained,"Select")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CHECKED)
      endwith
    endif
  endfunc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_CHECKED)
     endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="BIRMKPXGII",left=204, top=1, width=25,height=25,;
    CpPicture="esc.ico", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 192691328;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTypeFilter_1_10 as StdCombo with uid="SJFGANFPZK",rtseq=5,rtrep=.f.,left=3,top=59,width=111,height=21;
    , ToolTipText = "Condizione di filtro o ricerca";
    , HelpContextID = 28176012;
    , cFormVar="w_TypeFilter",RowSource=""+"Comincia per,"+"Contiene,"+"Uguale,"+"Minore uguale,"+"Minore,"+"Maggiore uguale,"+"Maggiore,"+"Non comincia per,"+"Non contiene,"+"Diverso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTypeFilter_1_10.RadioValue()
    return(iif(this.value =1,'Like',;
    iif(this.value =2,'Like %',;
    iif(this.value =3,'=',;
    iif(this.value =4,'<=',;
    iif(this.value =5,'<',;
    iif(this.value =6,'>=',;
    iif(this.value =7,'>',;
    iif(this.value =8,'Not Like',;
    iif(this.value =9,'Not Like %',;
    iif(this.value =10,'<>',;
    space(10))))))))))))
  endfunc
  func oTypeFilter_1_10.GetRadio()
    this.Parent.oContained.w_TypeFilter = this.RadioValue()
    return .t.
  endfunc

  func oTypeFilter_1_10.SetRadio()
    this.Parent.oContained.w_TypeFilter=trim(this.Parent.oContained.w_TypeFilter)
    this.value = ;
      iif(this.Parent.oContained.w_TypeFilter=='Like',1,;
      iif(this.Parent.oContained.w_TypeFilter=='Like %',2,;
      iif(this.Parent.oContained.w_TypeFilter=='=',3,;
      iif(this.Parent.oContained.w_TypeFilter=='<=',4,;
      iif(this.Parent.oContained.w_TypeFilter=='<',5,;
      iif(this.Parent.oContained.w_TypeFilter=='>=',6,;
      iif(this.Parent.oContained.w_TypeFilter=='>',7,;
      iif(this.Parent.oContained.w_TypeFilter=='Not Like',8,;
      iif(this.Parent.oContained.w_TypeFilter=='Not Like %',9,;
      iif(this.Parent.oContained.w_TypeFilter=='<>',10,;
      0))))))))))
  endfunc

  func oTypeFilter_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oTypeFilter_1_10.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE<>'C')
    endwith
  endfunc


  add object oTypeFilter1_1_11 as StdCombo with uid="YSXFKELPRY",rtseq=6,rtrep=.f.,left=3,top=58,width=111,height=21;
    , ToolTipText = "Condizione di filtro o ricerca";
    , HelpContextID = 28163468;
    , cFormVar="w_TypeFilter1",RowSource=""+"Uguale,"+"Minore uguale,"+"Minore,"+"Maggiore uguale,"+"Maggiore,"+"Diverso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTypeFilter1_1_11.RadioValue()
    return(iif(this.value =1,'=',;
    iif(this.value =2,'<=',;
    iif(this.value =3,'<',;
    iif(this.value =4,'>=',;
    iif(this.value =5,'>',;
    iif(this.value =6,'<>',;
    space(10))))))))
  endfunc
  func oTypeFilter1_1_11.GetRadio()
    this.Parent.oContained.w_TypeFilter1 = this.RadioValue()
    return .t.
  endfunc

  func oTypeFilter1_1_11.SetRadio()
    this.Parent.oContained.w_TypeFilter1=trim(this.Parent.oContained.w_TypeFilter1)
    this.value = ;
      iif(this.Parent.oContained.w_TypeFilter1=='=',1,;
      iif(this.Parent.oContained.w_TypeFilter1=='<=',2,;
      iif(this.Parent.oContained.w_TypeFilter1=='<',3,;
      iif(this.Parent.oContained.w_TypeFilter1=='>=',4,;
      iif(this.Parent.oContained.w_TypeFilter1=='>',5,;
      iif(this.Parent.oContained.w_TypeFilter1=='<>',6,;
      0))))))
  endfunc

  func oTypeFilter1_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CHECKED)
    endwith
   endif
  endfunc

  func oTypeFilter1_1_11.mHide()
    with this.Parent.oContained
      return (.w_CTRLTYPE='C')
    endwith
  endfunc

  add object oSearchwrap_1_16 as StdCheck with uid="LQXXRUPICT",rtseq=10,rtrep=.f.,left=3, top=40, caption="Torna all'inizio",;
    ToolTipText = "Torna all'inizio se raggiunta la fine",;
    HelpContextID = 111161073,;
    cFormVar="w_Searchwrap", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSearchwrap_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSearchwrap_1_16.GetRadio()
    this.Parent.oContained.w_Searchwrap = this.RadioValue()
    return .t.
  endfunc

  func oSearchwrap_1_16.SetRadio()
    this.Parent.oContained.w_Searchwrap=trim(this.Parent.oContained.w_Searchwrap)
    this.value = ;
      iif(this.Parent.oContained.w_Searchwrap=='S',1,;
      0)
  endfunc

  func oSearchwrap_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CtrlType='C' and not empty(.w_SearchValue) OR .w_CtrlType<>'C')
    endwith
   endif
  endfunc

  add object oRemPrevFilter_1_17 as StdCheck with uid="NLDZBMOWAM",rtseq=11,rtrep=.f.,left=116, top=58, caption="Annulla preced.", enabled=.f.,;
    ToolTipText = "Annulla filtro precedentemente inserito",;
    HelpContextID = 122322730,;
    cFormVar="w_RemPrevFilter", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRemPrevFilter_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oRemPrevFilter_1_17.GetRadio()
    this.Parent.oContained.w_RemPrevFilter = this.RadioValue()
    return .t.
  endfunc

  func oRemPrevFilter_1_17.SetRadio()
    this.Parent.oContained.w_RemPrevFilter=trim(this.Parent.oContained.w_RemPrevFilter)
    this.value = ;
      iif(this.Parent.oContained.w_RemPrevFilter=='S',1,;
      0)
  endfunc


  add object ZoomValueN as cp_szoombox with uid="XOFIVMTMNB",left=-8, top=73, width=243,height=121,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="CP_ZOOMVALUEN",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="cpazi",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",bQueryOnDblClick=.f.,bSearchFilterOnClick=.f.,;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Elenco valori presenti nello zoom";
    , HelpContextID = 52363898


  add object ZoomValue as cp_szoombox with uid="BQESICZURG",left=-8, top=73, width=243,height=121,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="CP_ZOOMVALUE",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="cpazi",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",bQueryOnDblClick=.f.,bSearchFilterOnClick=.f.,;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Elenco valori presenti nello zoom";
    , HelpContextID = 52363898
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_searchdetail','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_searchdetail
**************************************************
* Definizione label per filtri avanzati
*
DEFINE CLASS cp_StringAdvanced as StdString 
    uid="YYABKDNGPX"
    Visible=.t.
    Left=6
    Top=24
    Alignment=0
    Width=130
    Height=18
    Caption="Visualizza filtri avanzati"
    bGlobalFont=.t.
  
  PROCEDURE MouseEnter
		LPARAMETERS nButton, nShift, nXCoord, nYCoord

		WITH THIS
			.FONTUNDERLINE = .T.
			.FORECOLOR = RGB(0,0,255)
		ENDWITH
	ENDPROC

	PROCEDURE MouseLeave
		LPARAMETERS nButton, nShift, nXCoord, nYCoord

		WITH THIS
			.FONTUNDERLINE = .F.
			.FORECOLOR = RGB(128,128,255)
		ENDWITH
	ENDPROC
  
	PROCEDURE Click
		WITH THIS
			IF .CAPTION = cp_Translate("Visualizza filtri avanzati")
				.CAPTION = cp_Translate("Nascondi filtri avanzati")
        .parent.parent.parent.parent.Height=195
        .parent.parent.parent.parent.oPgFrm.Height=195
        if .Parent.oContained.w_ExpandForm<>3
          .Parent.oContained.w_ExpandForm=3
          do cp_SearchDetailR with .Parent.oContained, "LoadZoom"
        endif
			ELSE
				.CAPTION = cp_Translate("Visualizza filtri avanzati")
        .parent.parent.parent.parent.Height=40
        .parent.parent.parent.parent.oPgFrm.Height=40
			ENDIF
		ENDWITH
	ENDPROC
ENDDEFINE

**************************************************
*-- Class:        TimerInform
*-- ParentClass:  timer
*
DEFINE CLASS TimerInform AS timer

	Height = 23
	Width = 23
	Name = "TimerInform"
  Interval=500

	PROCEDURE Timer
		TRY && Ignor errors for this feature (though there shouldn't be any errors thrown)
			IF MROW(this.parent.Name,3) = -1 OR MCOL(this.parent.Name,3) = -1
		    IF !this.parent.RunDeactivateRelease
			    RETURN
		    ENDIF
		    this.parent.RELEASE()
      ENDIF
		CATCH
		ENDTRY
	ENDPROC
ENDDEFINE
**************************************************
* --- Fine Area Manuale
