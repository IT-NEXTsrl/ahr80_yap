* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_showdbf                                                      *
*              Mostra il risultato                                             *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-02                                                      *
* Last revis.: 2012-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_showdbf",oParentObject))

* --- Class definition
define class tcp_showdbf as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 785
  Height = 565
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-17"
  HelpContextID=27288283
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_showdbf"
  cComment = "Mostra il risultato"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ALCMDSQL = space(0)
  w_ISTRSEL = space(1)
  w_AUTOCOM = space(1)
  w_SOTTOTRANSAZIONE = .F.
  w_EXECSQL = .F.
  w_CURS_SQL = space(10)
  w_EDIT_SUCCESSIVO = .F.
  w_EDIT_PRECEDENTE = .F.
  w_BrowseOBJ = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_showdbfPag1","cp_showdbf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oALCMDSQL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_BrowseOBJ = this.oPgFrm.Pages(1).oPag.BrowseOBJ
    DoDefault()
    proc Destroy()
      this.w_BrowseOBJ = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ALCMDSQL=space(0)
      .w_ISTRSEL=space(1)
      .w_AUTOCOM=space(1)
      .w_SOTTOTRANSAZIONE=.f.
      .w_EXECSQL=.f.
      .w_CURS_SQL=space(10)
      .w_EDIT_SUCCESSIVO=.f.
      .w_EDIT_PRECEDENTE=.f.
      .oPgFrm.Page1.oPag.BrowseOBJ.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_ISTRSEL = 'S'
          .DoRTCalc(3,3,.f.)
        .w_SOTTOTRANSAZIONE = .F.
          .DoRTCalc(5,5,.f.)
        .w_CURS_SQL = SYS(2015)
        .w_EDIT_SUCCESSIVO = .F.
        .w_EDIT_PRECEDENTE = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.BrowseOBJ.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BrowseOBJ.Calculate()
    endwith
  return

  proc Calculate_SIKKRSOHCY()
    with this
          * --- Esegue frase sql
          .w_SOTTOTRANSAZIONE = IIF( NOT .w_SOTTOTRANSAZIONE, CP_SHOWDBF_BEGIN(), .w_SOTTOTRANSAZIONE )
          .w_EXECSQL = CP_SHOWDBF_SQL( .w_ALCMDSQL, THIS )
          .w_SOTTOTRANSAZIONE = IIF( .w_AUTOCOM = "S" AND .w_SOTTOTRANSAZIONE, CP_SHOWDBF_COMMIT(), .w_SOTTOTRANSAZIONE )
          .w_EDIT_SUCCESSIVO = .F.
    endwith
  endproc
  proc Calculate_FGVKQADXVV()
    with this
          * --- Apri Query
          .w_ALCMDSQL = getsqlstatement('')
    endwith
  endproc
  proc Calculate_FNLWLCCDZK()
    with this
          * --- Commit
          .w_SOTTOTRANSAZIONE = CP_SHOWDBF_COMMIT()
    endwith
  endproc
  proc Calculate_MZQTFUUBUV()
    with this
          * --- Rollback
          .w_SOTTOTRANSAZIONE = CP_SHOWDBF_ROLLBACK()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oISTRSEL_1_4.enabled = this.oPgFrm.Page1.oPag.oISTRSEL_1_4.mCond()
    this.oPgFrm.Page1.oPag.oAUTOCOM_1_8.enabled = this.oPgFrm.Page1.oPag.oAUTOCOM_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.BrowseOBJ.Event(cEvent)
        if lower(cEvent)==lower("DoSQLCommand")
          .Calculate_SIKKRSOHCY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ApriQuery")
          .Calculate_FGVKQADXVV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DoCommit")
          .Calculate_FNLWLCCDZK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DoRollback")
          .Calculate_MZQTFUUBUV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- cp_showdbf
    IF cEVENT = "Init"
        THIS.w_ALCMDSQL = THIS.oPARENTOBJECT.oPARENTOBJECT.w_ALCMDSQL
        THIS.w_BROWSEOBJ.cCURSOR = THIS.oPARENTOBJECT.w_SHOWDBF_CURS
        THIS.w_BROWSEOBJ.BROWSE()
        THIS.SETCONTROLSVALUE()
        *Crea il cursore per ricordare i comandi eseguiti
        CREATE CURSOR ( THIS.w_CURS_SQL ) ( COMANDO M(10) )
        =WRCURSOR( THIS.w_CURS_SQL )
    ENDIF
    
    IF cEVENT = "Done" AND USED( THIS.oPARENTOBJECT.w_SHOWDBF_CURS )
        SELECT( THIS.oPARENTOBJECT.w_SHOWDBF_CURS )
        USE
    ENDIF
    
    IF cEVENT = "Done"
        *Elimina il cursore con i comandi eseguiti
        SELECT( THIS.w_CURS_SQL )
        USE
         IF THIS.w_SOTTOTRANSAZIONE
           THIS.w_SOTTOTRANSAZIONE = CP_SHOWDBF_ROLLBACK()
         ENDIF
    ENDIF
    
    IF cEVENT = "DoPrevious"
       SELECT( THIS.w_CURS_SQL )
       IF RECNO() > 1
         SKIP - 1
         IF RECNO() = 1
           THIS.w_EDIT_PRECEDENTE = .F.
         ENDIF
         THIS.w_ALCMDSQL = COMANDO
         THIS.w_EDIT_SUCCESSIVO = .T.
         THIS.SETCONTROLSVALUE()
         THIS.mEnableControls()
       ENDIF
    ENDIF
    
    IF cEVENT = "DoNext"
       SELECT( THIS.w_CURS_SQL )
       IF RECNO() < RECCOUNT()
         SKIP 
         IF RECNO() = RECCOUNT()
           THIS.w_EDIT_SUCCESSIVO = .F.
         ENDIF
         THIS.w_ALCMDSQL = COMANDO
         THIS.w_EDIT_PRECEDENTE = .T.
         THIS.SETCONTROLSVALUE()
         THIS.mEnableControls()
       ENDIF
       
    ENDIF
    
    IF cEVENT = "w_AUTOCOM Changed"
       IF THIS.w_SOTTOTRANSAZIONE
          IF CP_YESNO("Si desidera eseguire la commit dei comandi gi� eseguiti?",,,.T.)
           THIS.w_SOTTOTRANSAZIONE = CP_SHOWDBF_COMMIT()
          ELSE
           THIS.w_SOTTOTRANSAZIONE = CP_SHOWDBF_ROLLBACK()
          ENDIF
        ENDIF  
    ENDIF
    
    IF cEVENT = "DoSQLCommand"
       SELECT( THIS.w_CURS_SQL )
       IF RECNO() > 1
         THIS.w_EDIT_PRECEDENTE = .T.
         THIS.mEnableControls()
       ENDIF
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oALCMDSQL_1_1.value==this.w_ALCMDSQL)
      this.oPgFrm.Page1.oPag.oALCMDSQL_1_1.value=this.w_ALCMDSQL
    endif
    if not(this.oPgFrm.Page1.oPag.oISTRSEL_1_4.RadioValue()==this.w_ISTRSEL)
      this.oPgFrm.Page1.oPag.oISTRSEL_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAUTOCOM_1_8.RadioValue()==this.w_AUTOCOM)
      this.oPgFrm.Page1.oPag.oAUTOCOM_1_8.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_showdbfPag1 as StdContainer
  Width  = 781
  height = 565
  stdWidth  = 781
  stdheight = 565
  resizeXpos=315
  resizeYpos=330
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oALCMDSQL_1_1 as StdMemo with uid="ZWWLMXPBDG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ALCMDSQL", cQueryName = "ALCMDSQL",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 266362248,;
   bGlobalFont=.t.,;
    Height=134, Width=563, Left=4, Top=20, enabled=.T.


  add object BrowseOBJ as ah_CBrowse with uid="LYNWLXXRLE",left=5, top=158, width=776,height=406,;
    caption='BrowseOBJ',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 214631290

  add object oISTRSEL_1_4 as StdCheck with uid="OEVKSEYMTV",rtseq=2,rtrep=.f.,left=577, top=15, caption="Consenti solo istruzioni SELECT",;
    ToolTipText = "Consente solo istruzioni Select",;
    HelpContextID = 100110634,;
    cFormVar="w_ISTRSEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oISTRSEL_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oISTRSEL_1_4.GetRadio()
    this.Parent.oContained.w_ISTRSEL = this.RadioValue()
    return .t.
  endfunc

  func oISTRSEL_1_4.SetRadio()
    this.Parent.oContained.w_ISTRSEL=trim(this.Parent.oContained.w_ISTRSEL)
    this.value = ;
      iif(this.Parent.oContained.w_ISTRSEL=='S',1,;
      0)
  endfunc

  func oISTRSEL_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (cp_isadministrator())
    endwith
   endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="KMRXCDEZOZ",left=623, top=41, width=48,height=45,;
    CpPicture="bmp\FOLDER.BMP", caption="", nPag=1;
    , ToolTipText = "Genera frase SQL da una visual query selezionata";
    , HelpContextID = 250874917;
    , caption='\<Query';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        .NotifyEvent('ApriQuery')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_6 as StdButton with uid="VWZFGTVPZJ",left=675, top=41, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue il comando SQL";
    , HelpContextID = 203902094;
    , caption='\<Esegui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        .NotifyEvent("DoSQLCommand")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="PTAFAFGHOT",left=727, top=41, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Chiude la maschera";
    , HelpContextID = 235359269;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAUTOCOM_1_8 as StdCheck with uid="WYAOSSGOPM",rtseq=3,rtrep=.f.,left=577, top=86, caption="Commit automatico",;
    ToolTipText = "Commit automatico",;
    HelpContextID = 49877316,;
    cFormVar="w_AUTOCOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAUTOCOM_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAUTOCOM_1_8.GetRadio()
    this.Parent.oContained.w_AUTOCOM = this.RadioValue()
    return .t.
  endfunc

  func oAUTOCOM_1_8.SetRadio()
    this.Parent.oContained.w_AUTOCOM=trim(this.Parent.oContained.w_AUTOCOM)
    this.value = ;
      iif(this.Parent.oContained.w_AUTOCOM=='S',1,;
      0)
  endfunc

  func oAUTOCOM_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (cp_isadministrator())
    endwith
   endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="ILVBGNMNHY",left=571, top=109, width=48,height=45,;
    CpPicture="bmp\upload.bmp", caption="", nPag=1;
    , ToolTipText = "Conferma le operazioni eseguite sotto transazione";
    , HelpContextID = 224187239;
    , caption='\<Commit';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .NotifyEvent("DoCommit")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (cp_isadministrator())
      endwith
    endif
  endfunc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_AUTOCOM = "S" AND NOT .w_SOTTOTRANSAZIONE)
     endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="LVBQHSBPCR",left=623, top=109, width=48,height=45,;
    CpPicture="bmp\DIBA3.bmp", caption="", nPag=1;
    , ToolTipText = "Annulla le operazioni eseguite sotto transazione";
    , HelpContextID = 26512822;
    , caption='\<Rollback';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .NotifyEvent("DoRollback")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (cp_isadministrator())
      endwith
    endif
  endfunc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_AUTOCOM = "S" AND NOT .w_SOTTOTRANSAZIONE)
     endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="WRYQNCVDQT",left=675, top=109, width=48,height=45,;
    CpPicture="bmp\Docrett.bmp", caption="", nPag=1;
    , ToolTipText = "Mostra il comando precedente";
    , HelpContextID = 143896793;
    , caption='\<Prec.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        .NotifyEvent("DoPrevious")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_EDIT_PRECEDENTE)
      endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="JOETJYKEJA",left=727, top=109, width=48,height=45,;
    CpPicture="bmp\DocDest.bmp", caption="", nPag=1;
    , ToolTipText = "Mostra il comando successivo";
    , HelpContextID = 142671048;
    , caption='\<Succ.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        .NotifyEvent("DoNext")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_EDIT_SUCCESSIVO)
      endwith
    endif
  endfunc

  add object oStr_1_2 as StdString with uid="EWTEYNTLSQ",Visible=.t., Left=12, Top=5,;
    Alignment=0, Width=133, Height=18,;
    Caption="Comando SQL"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_showdbf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_showdbf
DEFINE CLASS ah_CBrowse AS Grid

  cCursor=""

  * Standard
  RecordSource=""
  RecordSourceType=1   && ALIAS
  ColumnCount=0
  ReadOnly=.t.
  DeleteMark=.f.

  proc Calculate(xValue)
  endproc

  proc Event(cEvent)
    if cEvent="Browse"
       this.Browse()
    endif
  endproc

  proc Browse
    local nI,cI,oCol
    with this
       .RecordSource=''
       .ColumnCount=0
       if !empty(.cCursor)
          .ColumnCount=fcount(.cCursor)
          .recordsource=.cCursor
          for nI=1 to .ColumnCount
	    cI=alltrim(str(nI))
	    oCol=.Columns(nI)
            oCol.Header1.Caption=field(nI,.cCursor)
   	    oCol.Bound=.f.
            oCol.ReadOnly=.ReadOnly
   	    oCol.ControlSource=alltrim(.cCursor)+'.'+field(nI,.cCursor)
   	    oCol.SelectOnEntry=.f.   	
	   next
       endif
    endwith
    Grid::Refresh()
  endproc

ENDDEFINE
* --- Fine Area Manuale
