* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: splitsecreport                                                  *
*              Split second report                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-10                                                      *
* Last revis.: 2015-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,oMultiReport,oRetMultiReport,cMainCursor,nMainIndex,cStmpOrder,cSplitField,cOldMainCursor
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tsplitsecreport",oParentObject,m.oMultiReport,m.oRetMultiReport,m.cMainCursor,m.nMainIndex,m.cStmpOrder,m.cSplitField,m.cOldMainCursor)
return(i_retval)

define class tsplitsecreport as StdBatch
  * --- Local variables
  oMultiReport = .NULL.
  oRetMultiReport = space(0)
  cMainCursor = space(0)
  nMainIndex = space(0)
  cStmpOrder = space(0)
  cSplitField = space(0)
  cOldMainCursor = space(0)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     LOCAL l_nIndex, l_RowNumName, l_cIndexOrder, l_cDettOrder 
 l_RowNumName=SYS(2015) &&Nome colonna contenente il numero riga 
 LOCAL l_j, l_SecIndex, l_cSecCursor, l_cLink, l_splitSecExpr, l_SplitSecCursor, l_tempCursor 
 LOCAL l_cLink, l_tempSecCursor, l_tempCursor_RecNo, bFirst 
 l_SecIndex=0
    * --- Ciclo sui report secondari
    bFirst=.t.
    * --- Ciclo sui figli
    l_child=this.oMultiReport.GetChild(this.nMainIndex)
    if  EMPTY(l_child)
      if EMPTY(this.cOldMainCursor)
        * --- Ricopio tutti i dati dal multireport sorgente a quello destinatario
        l_nIndex=this.oRetMultiReport.CopyReport(this.oMultiReport, this.nMainIndex,, .t.)
        * --- Cambio il cursore
         this.oRetMultiReport.SetCursor(this.cMainCursor, l_nIndex) 
 this.oRetMultiReport.SetIndex(this.nMainIndex, l_nIndex) 
 l_cIndexOrder=RIGHT("00000"+ALLTRIM(TRANSFORM(l_nIndex)),5) 
 l_cDettOrder=RIGHT("00000"+ALLTRIM(TRANSFORM(this.oRetMultiReport.GetDettOrder(l_nIndex))),5) 
 this.oRetMultiReport.SetOrder(this.oRetMultiReport.GetStmpOrder(l_nIndex)+"_"+l_cIndexOrder+"_"+l_cDettOrder, l_nIndex) 
 this.cOldMainCursor=this.cMainCursor &&Sbianco il vecchio nome del cursore 
      else
        * ---  Eseguo la union dei due cursori	cOldMainCursor e cMainCursor
        *      Manca orderby
        if this.cOldMainCursor<>this.cMainCursor
           SELECT * FROM (this.cOldMainCursor) UNION ALL SELECT * FROM (this.cMainCursor) INTO CURSOR (this.cOldMainCursor) READWRITE 
 USE IN SELECT(this.cMainCursor) 
        endif
      endif
    endif
    do while NOT EMPTY(l_child)
       l_tempCursor=SYS(2015) 
 l_tempCursor_RecNo=SYS(2015) 
 l_SecIndex=this.oMultiReport.GetIndexByKey(l_child, this.nMainIndex)
      if l_SecIndex=-1
        l_SecIndex=this.oMultiReport.GetIndexByKey(l_child)
      endif
      if l_SecIndex=-1
        * --- Se non ho (pi�) report secondari esco
        EXIT
      endif
       l_cSecCursor=this.oMultiReport.GetCursorName(l_SecIndex) 
 l_cLink=this.oMultiReport.GetLink(l_SecIndex)
      if NOT EMPTY(l_cLink) AND TYPE("cSplitField")="C" AND NOT EMPTY(this.cSplitField)
        * --- Utilizzando il link estraggo dal cursore secondario i dati relativi alla rottura con il principale
         SELECT *, RECNO() AS (l_RowNumName) FROM (l_cSecCursor) WHERE &l_cLink=this.cSplitField INTO CURSOR (l_tempCursor_RecNo) 
 SELECT * FROM (l_cSecCursor) WHERE &l_cLink=this.cSplitField INTO CURSOR (l_tempCursor) READWRITE
      else
        * --- Non ho link o rottura quindi prendo tutto il cursore
         SELECT *, RECNO() AS (l_RowNumName) FROM (l_cSecCursor) INTO CURSOR (l_tempCursor_RecNo) 
 SELECT * FROM (l_cSecCursor) INTO CURSOR (l_tempCursor) READWRITE
      endif
      * --- Verifico se ho una rottura sul cursore secondario e che sia pieno
      l_splitSecExpr=this.oMultiReport.GetSplit(l_SecIndex)
      if (RecordCount(l_tempCursor)>0 OR EMPTY(this.cOldMainCursor)) AND bFirst
        * --- Ricopio tutti i dati dal multireport sorgente a quello destinatario
        l_nIndex=this.oRetMultiReport.CopyReport(this.oMultiReport, this.nMainIndex,, .t.)
        * --- Cambio il cursore
         this.oRetMultiReport.SetCursor(this.cMainCursor, l_nIndex) 
 this.oRetMultiReport.SetIndex(this.nMainIndex, l_nIndex) 
 l_cIndexOrder=RIGHT("00000"+ALLTRIM(TRANSFORM(l_nIndex)),5) 
 l_cDettOrder=RIGHT("00000"+ALLTRIM(TRANSFORM(this.oRetMultiReport.GetDettOrder(l_nIndex))),5) 
 this.oRetMultiReport.SetOrder(this.oRetMultiReport.GetStmpOrder(l_nIndex)+"_"+l_cIndexOrder+"_"+l_cDettOrder, l_nIndex) 
 this.cOldMainCursor=this.cMainCursor &&Sbianco il vecchio nome del cursore 
 bFirst=.f.
      else
        * --- *--- Eseguo la union dei due cursori	cOldMainCursor e cMainCursor
        *     *--- Manca orderby
        if this.cOldMainCursor<>this.cMainCursor AND NOT EMPTY(this.cOldMainCursor) AND USED(this.cMainCursor)
           SELECT * FROM (this.cOldMainCursor) UNION ALL SELECT * FROM (this.cMainCursor) INTO CURSOR (this.cOldMainCursor) READWRITE 
 USE IN SELECT(this.cMainCursor)
        endif
      endif
      if NOT EMPTY(l_splitSecExpr) AND RecordCount(l_tempCursor)>0
         l_SplitSecCursor=SYS(2015) 
 SELECT DISTINCT &l_splitSecExpr AS SPLITFIELD FROM (l_tempCursor_RecNo) ORDER BY &l_RowNumName INTO CURSOR (l_SplitSecCursor) READWRITE 
 USE IN SELECT(l_tempCursor_RecNo)
        * ---  l_SplitCursor contiene le chiavi di rottura
         SELECT(l_SplitSecCursor) 
 GO TOP 
 SCAN 
 l_tempSecCursor=SYS(2015) 
 l_cSplitField=SPLITFIELD 
 SELECT * FROM (l_tempCursor) WHERE &l_splitSecExpr=l_cSplitField INTO CURSOR (l_tempSecCursor) READWRITE
        if RecordCount(l_tempSecCursor)>0
          * --- Ricopio tutti i dati dal multireport sorgente a quello destinatario
          l_nIndex=this.oRetMultiReport.CopyReport(this.oMultiReport, l_SecIndex,, .t.)
          * --- Cambio il cursore
           this.oRetMultiReport.SetCursor(l_tempSecCursor, l_nIndex) 
 this.oRetMultiReport.SetIndex(l_SecIndex, l_nIndex) 
 l_cDettOrder=RIGHT("00000"+ALLTRIM(TRANSFORM(this.oRetMultiReport.GetDettOrder(l_nIndex))),5) 
 this.oRetMultiReport.SetOrder(this.oRetMultiReport.GetStmpOrder(l_nIndex)+"_"+l_cIndexOrder+"_"+l_cDettOrder, l_nIndex) 
 this.cOldMainCursor=""
        endif
         ENDSCAN &&(l_SplitSecCursor) 
 USE IN SELECT(l_tempCursor) 
 USE IN SELECT(l_SplitSecCursor)
      else
        if RecordCount(l_tempCursor)>0
          USE IN SELECT(l_tempCursor_RecNo)
          * --- Ricopio tutti i dati dal multireport sorgente a quello destinatario
          l_nIndex=this.oRetMultiReport.CopyReport(this.oMultiReport, l_SecIndex,, .t.)
          * --- Cambio il cursore
           this.oRetMultiReport.SetCursor(l_tempCursor, l_nIndex) 
 this.oRetMultiReport.SetIndex(l_SecIndex, l_nIndex) 
 l_cDettOrder=RIGHT("00000"+ALLTRIM(TRANSFORM(this.oRetMultiReport.GetDettOrder(l_nIndex))),5) 
 this.oRetMultiReport.SetOrder(this.oRetMultiReport.GetStmpOrder(l_nIndex)+"_"+l_cIndexOrder+"_"+l_cDettOrder, l_nIndex) 
 this.cOldMainCursor="" 
        else
          * --- Il cursore � vuoto quindi lo chiudo
           USE IN SELECT(l_tempCursor_RecNo) 
 USE IN SELECT(l_tempCursor)
        endif
      endif
      l_child=this.oMultiReport.GetChild(l_SecIndex)
    enddo
  endproc


  proc Init(oParentObject,oMultiReport,oRetMultiReport,cMainCursor,nMainIndex,cStmpOrder,cSplitField,cOldMainCursor)
    this.oMultiReport=oMultiReport
    this.oRetMultiReport=oRetMultiReport
    this.cMainCursor=cMainCursor
    this.nMainIndex=nMainIndex
    this.cStmpOrder=cStmpOrder
    this.cSplitField=cSplitField
    this.cOldMainCursor=cOldMainCursor
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="oMultiReport,oRetMultiReport,cMainCursor,nMainIndex,cStmpOrder,cSplitField,cOldMainCursor"
endproc
