* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_infoctrl                                                     *
*              ProprietÓ controllo                                             *
*                                                                              *
*      Author: GiorGio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-06                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_infoctrl
cp_ReadXdc()
* --- Fine Area Manuale
return(createobject("tcp_infoctrl",oParentObject))

* --- Class definition
define class tcp_infoctrl as StdForm
  Top    = 71
  Left   = 134

  * --- Standard Properties
  Width  = 608
  Height = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-02"
  HelpContextID=170563081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_infoctrl"
  cComment = "ProprietÓ controllo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CONTROL = space(20)
  w_HELP = space(100)
  w_TIPO = space(20)
  w_GESTIONE = space(20)
  w_LOGTAB = space(30)
  w_FISTAB = space(20)
  w_LENGTH = space(5)
  w_cMenuFile = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_infoctrlPag1","cp_infoctrl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCONTROL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_infoctrl
    * --- Translate interface...
     this.parent.cComment=cp_Translate(MSG_CTLR_PROPERTY)
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CONTROL=space(20)
      .w_HELP=space(100)
      .w_TIPO=space(20)
      .w_GESTIONE=space(20)
      .w_LOGTAB=space(30)
      .w_FISTAB=space(20)
      .w_LENGTH=space(5)
      .w_cMenuFile=space(30)
        .w_CONTROL = alltrim(substr(this.oParentObject.oCtrl.cFormVar,3))
        .w_HELP = alltrim(this.oParentObject.oCtrl.tooltiptext)
        .w_TIPO = getType(IIF(INLIST(UPPER(this.oParentObject.oCtrl.class), 'STDCOMBO','STDCHECK','STDRADIO','STDTABLECOMBO','STDTRSCOMBO','STDTRSCHECK','STDTRSRADIO','STDTRSTABLECOMBO') , this.oParentObject.oCtrl.RadioValue(), this.oParentObject.oCtrl.value))
        .w_GESTIONE = alltrim(this.oParentObject.oContained.cPrg)
        .w_LOGTAB = i_dcx.GetTableDescr(i_dcx.GetTableIdx(upper(alltrim(this.oParentObject.oCtrl.cLinkFile))))
        .w_FISTAB = alltrim(this.oParentObject.oCtrl.cLinkFile)
        .w_LENGTH = getLength(IIF(INLIST(UPPER(this.oParentObject.oCtrl.class), 'STDCOMBO','STDCHECK','STDRADIO','STDTABLECOMBO','STDTRSCOMBO','STDTRSCHECK','STDTRSRADIO','STDTRSTABLECOMBO') , this.oParentObject.oCtrl.RadioValue(),this.oParentObject.oCtrl.value))
        .w_cMenuFile = .oParentObject.cMenuFile
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_NAME+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_CTLR_NAME)
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_HELP+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_DESCRIPTION)
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_TYPE+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_CTLR_TYPE)
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_LENGTH+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_LENGTH)
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_ENTITY+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_PRG_NAME)
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_CONTEXT_MENU+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
      .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_TABLE_NAME)
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_LOGICAL+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_PHYSICAL+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_NAME+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_CTLR_NAME)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_HELP+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_DESCRIPTION)
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_TYPE+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_CTLR_TYPE)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_LENGTH+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_LENGTH)
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_ENTITY+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_PRG_NAME)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_CONTEXT_MENU+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_TABLE_NAME)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_LOGICAL+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_PHYSICAL+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_NAME+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_CTLR_NAME)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_HELP+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_DESCRIPTION)
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_TYPE+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_CTLR_TYPE)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_LENGTH+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_LENGTH)
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_ENTITY+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_PRG_NAME)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_CONTEXT_MENU+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_TABLE_NAME)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_LOGICAL+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_PHYSICAL+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCONTROL_1_1.value==this.w_CONTROL)
      this.oPgFrm.Page1.oPag.oCONTROL_1_1.value=this.w_CONTROL
    endif
    if not(this.oPgFrm.Page1.oPag.oHELP_1_2.value==this.w_HELP)
      this.oPgFrm.Page1.oPag.oHELP_1_2.value=this.w_HELP
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_3.value==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_3.value=this.w_TIPO
    endif
    if not(this.oPgFrm.Page1.oPag.oGESTIONE_1_4.value==this.w_GESTIONE)
      this.oPgFrm.Page1.oPag.oGESTIONE_1_4.value=this.w_GESTIONE
    endif
    if not(this.oPgFrm.Page1.oPag.oLOGTAB_1_5.value==this.w_LOGTAB)
      this.oPgFrm.Page1.oPag.oLOGTAB_1_5.value=this.w_LOGTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oFISTAB_1_6.value==this.w_FISTAB)
      this.oPgFrm.Page1.oPag.oFISTAB_1_6.value=this.w_FISTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oLENGTH_1_7.value==this.w_LENGTH)
      this.oPgFrm.Page1.oPag.oLENGTH_1_7.value=this.w_LENGTH
    endif
    if not(this.oPgFrm.Page1.oPag.ocMenuFile_1_17.value==this.w_cMenuFile)
      this.oPgFrm.Page1.oPag.ocMenuFile_1_17.value=this.w_cMenuFile
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_infoctrlPag1 as StdContainer
  Width  = 604
  height = 200
  stdWidth  = 604
  stdheight = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCONTROL_1_1 as StdField with uid="LBCSHXROLU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CONTROL", cQueryName = "CONTROL",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome controllo",;
    HelpContextID = 151956281,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=118, Top=13, InputMask=replicate('X',20), readOnly=.T.

  add object oHELP_1_2 as StdField with uid="ZDCAJAZYII",rtseq=2,rtrep=.f.,;
    cFormVar = "w_HELP", cQueryName = "HELP",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 160212489,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=388, Top=13, InputMask=replicate('X',100), readOnly=.T.

  add object oTIPO_1_3 as StdField with uid="TGRGFMTVCD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TIPO", cQueryName = "TIPO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo contenuto",;
    HelpContextID = 88122889,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=118, Top=44, InputMask=replicate('X',20), readOnly=.T.

  add object oGESTIONE_1_4 as StdField with uid="QPLSALJFVF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GESTIONE", cQueryName = "GESTIONE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del programma che contiene il controllo",;
    HelpContextID = 225901872,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=118, Top=76, InputMask=replicate('X',20), readOnly=.T.

  add object oLOGTAB_1_5 as StdField with uid="XPACORHWJU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LOGTAB", cQueryName = "LOGTAB",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome logico della tabella",;
    HelpContextID = 35085912,;
   bGlobalFont=.t.,;
    Height=21, Width=159, Left=119, Top=128, InputMask=replicate('X',30), readOnly=.T.

  add object oFISTAB_1_6 as StdField with uid="TZPDAHCDVK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FISTAB", cQueryName = "FISTAB",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome fisico della tabella",;
    HelpContextID = 229727832,;
   bGlobalFont=.t.,;
    Height=21, Width=159, Left=413, Top=128, InputMask=replicate('X',20), readOnly=.T.

  add object oLENGTH_1_7 as StdField with uid="BSUWDFMBIZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LENGTH", cQueryName = "LENGTH",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza",;
    HelpContextID = 142040779,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=517, Top=44, InputMask=replicate('X',5), readOnly=.T.

  add object ocMenuFile_1_17 as StdField with uid="YLUBTYZYIL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_cMenuFile", cQueryName = "cMenuFile",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome del menu contestuale associato",;
    HelpContextID = 260405300,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=443, Top=77, InputMask=replicate('X',30), readOnly=.T.


  add object oObj_1_19 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=144, top=207, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nome:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_20 as cp_setobjprop with uid="SKPUZOAYUU",left=417, top=207, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CONTROL",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oBtn_1_21 as StdButton with uid="FZOOQYPNID",left=536, top=171, width=50,height=20,;
    caption="Ok", nPag=1;
    , HelpContextID = 52798985;
    , caption=MSG_OK_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_22 as cp_setCtrlObjprop with uid="ERPBVLZROL",left=144, top=233, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Guida in linea:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_23 as cp_setobjprop with uid="UWNIRGQRUQ",left=417, top=233, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_HELP",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_28 as cp_setCtrlObjprop with uid="MQMSEQTTWN",left=144, top=259, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tipo:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_29 as cp_setobjprop with uid="YLJCDSBJVP",left=417, top=259, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_TIPO",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_30 as cp_setCtrlObjprop with uid="HTWFCECFKC",left=144, top=285, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Lunghezza:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_31 as cp_setobjprop with uid="OVNWHCVKIW",left=417, top=285, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_LENGHT",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_32 as cp_setCtrlObjprop with uid="WSSTXAONIK",left=144, top=311, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Gestione:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_33 as cp_setobjprop with uid="RBBKDJKLLT",left=417, top=311, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_GESTIONE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_34 as cp_setCtrlObjprop with uid="SKMQWAALBO",left=144, top=337, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Menu contestuale:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_35 as cp_setobjprop with uid="ORWTLCWGZE",left=417, top=337, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cMenuFile",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_36 as cp_setCtrlObjprop with uid="HYQKHWFMMN",left=-52, top=347, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nome Tabella",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_37 as cp_setCtrlObjprop with uid="BSNCVUWGAS",left=144, top=363, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Logico:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_38 as cp_setobjprop with uid="RVKFDLBTMV",left=417, top=363, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_logtab",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_39 as cp_setCtrlObjprop with uid="OAFQHIXFQX",left=144, top=389, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Fisico:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946


  add object oObj_1_40 as cp_setobjprop with uid="KLOCHIONDT",left=417, top=389, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_fistab",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105537946

  add object oStr_1_8 as StdString with uid="APJVQEIHTH",Visible=.t., Left=64, Top=13,;
    Alignment=1, Width=50, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="JTTAJLASHK",Visible=.t., Left=26, Top=44,;
    Alignment=1, Width=88, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="EUNOUSSAOP",Visible=.t., Left=408, Top=44,;
    Alignment=1, Width=107, Height=18,;
    Caption="Lunghezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="ICKGCHGJEG",Visible=.t., Left=15, Top=76,;
    Alignment=1, Width=99, Height=18,;
    Caption="Gestione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="DHVXOCQNLS",Visible=.t., Left=72, Top=105,;
    Alignment=0, Width=132, Height=18,;
    Caption="Nome Tabella"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="OIBSMWNTWV",Visible=.t., Left=11, Top=128,;
    Alignment=1, Width=103, Height=18,;
    Caption="Logico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="AAZOZHRFZA",Visible=.t., Left=292, Top=128,;
    Alignment=1, Width=119, Height=18,;
    Caption="Fisico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QGDWNXVMBZ",Visible=.t., Left=302, Top=13,;
    Alignment=1, Width=85, Height=18,;
    Caption="Guida in linea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="FFBNAJVRKR",Visible=.t., Left=285, Top=77,;
    Alignment=1, Width=156, Height=18,;
    Caption="Menu contestuale:"  ;
  , bGlobalFont=.t.

  add object oBox_1_13 as StdBox with uid="GGOOVTWZJH",left=10, top=113, width=53,height=2

  add object oBox_1_24 as StdBox with uid="RLLRDGUCLR",left=9, top=114, width=2,height=46

  add object oBox_1_25 as StdBox with uid="SWPZABPGCR",left=586, top=115, width=2,height=46

  add object oBox_1_26 as StdBox with uid="KVVXQRBPRW",left=163, top=115, width=423,height=1

  add object oBox_1_27 as StdBox with uid="QYMUVVOCEP",left=9, top=159, width=578,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_infoctrl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_infoctrl
proc getType(valore)
  do case
   case vartype(valore)=='C'
     return(cp_translate(MSG_CHARACTER))
   case vartype(valore)=='N'
    return(cp_translate(MSG_NUMERIC))
   case vartype(valore)=='D'
     return(cp_translate(MSG_DATE))
   case vartype(valore)=='T'
     return(cp_translate(MSG_DATETIME))
   case vartype(valore)=='L'
     return(cp_translate(MSG_LOGICAL))
   case vartype(valore)=='M'
     return(cp_translate(MSG_MEMO))
   case vartype(valore)=='O'
     return(cp_translate(MSG_OBJECT))
   otherwise
     return(Cp_translate(MSG_UNKNOWN))
  endcase
endproc

Proc getLength(valore)
  do case
   case vartype(valore)=='C'
     return(alltrim(str(len(valore))))
   case vartype(valore)=='N'
    return('N.D.')
   case vartype(valore)=='D'
     return('8')
   case vartype(valore)=='T'
     return('14')
   case vartype(valore)=='L'
     return('1')
   case vartype(valore)=='M'
     return('10')
   case vartype(valore)=='O'
     return('N.D.')
   otherwise
     return('N.D.')
  endcase
* --- Fine Area Manuale
