* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_chp                                                          *
*              "+MSG_PRINT_SYSTEM+"                                            *
*                                                                              *
*      Author: GiorGio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-06                                                      *
* Last revis.: 2018-07-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_chp",oParentObject))

* --- Class definition
define class tcp_chp as StdForm
  Top    = 11
  Left   = 35

  * --- Standard Properties
  Width  = 710
  Height = 191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-24"
  HelpContextID=124687971
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  STMPFILE_IDX = 0
  cPrg = "cp_chp"
  cComment = ""+MSG_PRINT_SYSTEM+""
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_Opt_Language = space(20)
  w_DEVICE = space(200)
  w_Txt_Copies = 0
  w_Opt_File = space(20)
  o_Opt_File = space(20)
  w_FORMFILE = space(10)
  o_FORMFILE = space(10)
  w_TIPOFILE = space(10)
  w_GESTIONE = space(10)
  w_Txt_File = space(200)
  o_Txt_File = space(200)
  w_PCNAME = space(10)
  w_SEL = space(10)
  w_SFFLOPEN = space(1)
  w_PRODOCM = space(10)
  w_TREPORT = space(10)
  w_FRXFILE = .F.
  w_SETCONF = .F.
  * --- Area Manuale = Declare Variables
  * --- cp_chp
    nIndexReport=1	&&Indice report corrente, selezionato nella ComboBox
    oSplitReport=.null.  &&MultiReport esplosione per fascicolazione report
    oOrigMReport=.null.  &&MultiReport originario (DOCM)
    cDescReport    =''
    cTipoReport    = ""
    tReport=" "
    cModSst =""     && Modello stampa solo testo da Cpusrrep
    ** Inizio DOCM
    cAnteprimaSN   = .T.
    cSessionCode=""
    cLanguage=""    		&& Lingua nella quale si vuole visualizzare/stampare il report
    * Propriet� per processo documentale
    pdProcesso     = .F.
    pdoParentObject= NULL
    pdObbligatorio = .F.
    pdMantieniLog  = .F.
    pdCodProcesso  = ""
    pdKeyIstanza   = ""
    pdExeProcesso  = .F.
    pdStrControl   = 'NNNNNNNN'
    pdnTotDoc      = 0
    pdnTotDocOk    = 0
    pdnTotDocKo    = 0
    pdISOLang = .f.
    pdnAzAutom = -1
    noParentObject=''
    oParentObject = .Null.
    cNomeReport = ""
    prStamp = ""
    prFile = ""
    prTyExp = ""
    prCopie = 1
    excel=.NULL.
    word=.NULL.
    bModifiedFrx=.f.
    i_FDFFile=''
  nAzAutom=0
  
  o_ZCP=null
  oBtn_StpArchi=null
  oOpt_File=null
  oBtn_StpFileOpt=null
  oOpt_Language=null
  oBtn_WWP_ZCP=null
  oBtn_SFile=null
  oBtn_StpFile=null
  oBtn_SaveInd=null
  oBtn_SStp=null
  oBtn_Assoc=null
  oTxt_Copies=null
  oTxt_Device=null
  oBtn_Ante=null
  oBtn_Esc=null
  oBtn_Query=null
  oBtn_Stp=null
  oBtn_StpOpt=null
  oBtn_Mail=null
  oBtn_MPEC=null
  oBtn_Fax=null
  oBtn_Word=null
  oBtn_Excel=null
  oBtn_Graph=null
  oBtn_Plite=null
  oBtn_Edi=null
  oPDBtn_Proc=null
  oPDBtn_Status=null
  oBtn_WWP_ZCP=null
  oTxt_File=null
  oPDLbl4=null
  
  oPDPrgBar=null
  oPDLbl1=null
  oPDLbl5=null
  oPDLbl2=null
  oPDLbl6=null
  oPDLbl3=null
  
  *--- non salvo la posizione della form
  bChkPositionForm = .F.
    
   w_Opt_File='TXT' && Formato di stampa selezionato
   cExtFormat = 'TXT' &&Estensione del formato
   cOption=' ' && Opzioni per stampa su file
   ADD OBJECT oFormatToExtension as Collection
  
     
   ****Zucchetti Aulla-Schedulatore Inizio****
   Function Schedulatore()
     If This.pdProcesso
       ***Processo documentale***
       If This.oPDBtn_Proc.Enabled
         g_MSG=g_MSG+' '+ cp_Translate(MSG_EXEC_DOCUMENT_PROCESS)+Chr(13)
         This.oPDBtn_Proc.Click()
       Else
         g_MSG=g_MSG+' '+ cp_Translate(MSG_CANNOT_EXEC_DOCUMENT_PROCESS)+Chr(13)
       Endif
     Else
       ***Stampa Std***
       g_MSG=g_MSG+' '+ cp_Translate(MSG_EXEC_STANDARD_REPORT)+Chr(13)
       setvaluelinked("M", This, "w_opt_File", 'PDF')
       w_year=Substr(Dtoc(i_DATSYS),7,4)
       w_month=Substr(Dtoc(i_DATSYS),4,2)
       w_day=Substr(Dtoc(i_DATSYS),1,2)
       w_hour=Substr(Time(),1,2)
       w_min=Substr(Time(),4,2)
       w_sec=Substr(Time(),7,2)
       If Left(This.prfile,2)='\\' Or Substr(This.prfile,2,1)=':'
         This.prfile=Right(This.prfile,Len(This.prfile)-Rat('\',This.prfile))
         This.prfile=g_PATHSJ+Left(Alltrim(This.prfile),Rat(".",Alltrim(This.prfile),1)-1)+;
           iif(Empty(This.prfile) Or 'DEFA'$This.prfile,'_'+g_JBNOME,'')+;
           '_'+Alltrim(g_JBCODAZI)+'_'+Alltrim(w_year)+Alltrim(w_month)+Alltrim(w_day)+;
           ALLTRIM(w_hour)+Alltrim(w_min)+Alltrim(w_sec)+'.'+;
           RIGHT(Alltrim(This.prfile),Len(Alltrim(This.prfile))-Rat(".",Alltrim(This.prfile),1))
       Else
         This.prfile=g_PATHSJ+Left(Alltrim(This.prfile),Rat(".",Alltrim(This.prfile),1)-1)+;
           iif(Empty(This.prfile) Or 'DEFA'$This.prfile,'_'+g_JBNOME,'')+;
           '_'+Alltrim(g_JBCODAZI)+'_'+Alltrim(w_year)+Alltrim(w_month)+Alltrim(w_day)+;
           ALLTRIM(w_hour)+Alltrim(w_min)+Alltrim(w_sec)+'.'+;
           RIGHT(Alltrim(This.prfile),Len(Alltrim(This.prfile))-Rat(".",Alltrim(This.prfile),1))
       Endif
       setvaluelinked("M", This, "w_Txt_File", This.prfile)
       Thisform.oBtn_StpFile.Click()
       If File (This.prfile)
         g_MSG=g_MSG+' '+ cp_Msgformat(MSG_FILE_CREATED_CL__,This.prfile)+ Chr(13)
         w_N=Alen(g_STAMPJOB)
         g_STAMPJOB(w_N)=This.prfile
         Dimension g_STAMPJOB(w_N+1)
         g_FLAGSTMP=.T.
       Endif
       *****Stampa cartacea***
       If(g_FLGSTC='S')
         This.oBtn_Stp.Click()
         g_MSG=g_MSG+' '+ cp_Translate(MSG_EXEC_PAPERY_REPORT)+Chr(13)
       Endif
     Endif
     ***Chiudo il form
     This.oBtn_Esc.Click
   Endfunc
   ****Zucchetti Aulla-Schedulatore Fine****
  
   *--- Zucchetti Aulla Inizio - Multireport
   Procedure SelectReport(cType)
     Local l_prg
     If This.cNomeReport.GetNumReport()>1
       l_prg="GSUT_BRL(this, 'C', this.cNomeReport, cType)"
       This.nIndexReport=&l_prg
     Else
       *--- Nel caso di un solo elemento nel multireport ritorno 1, il controllo sul fatto che sia presente un modello o meno
       *--- viene gi� eseguito dal LoadConfig, il bottone � abilitato
       This.nIndexReport=1
     Endif
   Endproc
  
   *** Fine DOCM
   Procedure Release()
     *--- Cancello i report tradotti
     If !(Vartype(g_DISABLEREPORTTRANSLATION)='L' And g_DISABLEREPORTTRANSLATION)
       Local cNomeFile, l_NumRep
       *--- Cancello FRX
       l_NumRep = Adir(aRepList, Addbs(TEMPADHOC())+"*"+This.cSessionCode+".FR?")
       For i=1 To l_NumRep
         cNomeFile = Addbs(TEMPADHOC())+aRepList[i,1]
         If cp_FileExist(cNomeFile)
           Erase(cNomeFile)
         Endif
       Next
     Endif
     DoDefault()
   Endproc
  
   Procedure ReleaseConfig
     * Se non � richiesta la conservazione del log oppure se il processo non � stato eseguito, procede con cancellazione
     If Not This.pdMantieniLog Or Not This.pdExeProcesso
       Local l_sPrg
       l_sPrg="GSDM_BPD"
       Do (l_sPrg) With This, 'CL', This.pdKeyIstanza
     Endif
   Endproc
  
   **DOCM inizio
   Procedure SettaConfigurazione(pProcessoSN)
     * Controlli di visibilit� (parte 1) - Non interferisce con propriet� enabled
     This.oBtn_Stp.Visible      = Not pProcessoSN
     This.oBtn_StpOpt.Visible   = Not pProcessoSN
     This.oBtn_Mail.Visible     = Not pProcessoSN
     This.oBtn_MPEC.Visible     = Not pProcessoSN
     This.oBtn_Fax.Visible      = Not pProcessoSN
     This.oBtn_WWP_ZCP.Visible  = Not pProcessoSN
     This.oBtn_Word.Visible     = Not pProcessoSN
     This.oBtn_Excel.Visible    = Not pProcessoSN
     This.oBtn_Graph.Visible    = Not pProcessoSN
     This.oBtn_Plite.Visible    = Not pProcessoSN
     This.oBtn_Edi.Visible      = Not pProcessoSN
     * Controlli di visibilit� (parte 2) - Non interferisce con propriet� enabled
     This.oPDBtn_Proc.Visible   = pProcessoSN
     This.oPDBtn_Status.Visible = pProcessoSN
     If (Nvl (g_DOCM,' ')='S')
       This.oPDPrgBar.Visible     = pProcessoSN
       This.oPDLbl1.Visible       = pProcessoSN
       This.oPDLbl2.Visible       = pProcessoSN
       This.oPDLbl3.Visible       = pProcessoSN
       This.oPDLbl5.Visible       = pProcessoSN
       This.oPDLbl6.Visible       = pProcessoSN
     Endif
     This.oPDLbl4.Visible       = pProcessoSN
     * Controlli di abilitazione
     This.oPDBtn_Proc.Enabled   = pProcessoSN And This.pdnTotDocOk>0 And Not(This.pdExeProcesso)
     This.oPDBtn_Status.Enabled = pProcessoSN
     This.oBtn_SFile.Enabled    = This.tReport<>'I' And Not pProcessoSN
     This.oBtn_StpFile.Enabled  = Not pProcessoSN
     *this.oBtn_SStp.enabled     = not pProcessoSN
     This.oBtn_Assoc.Enabled    = This.tReport<>'I' And Not pProcessoSN And This.oBtn_Assoc.Enabled
     This.oTxt_Copies.Enabled   = This.tReport<>'I' And This.cNomeReport.bNumCopy
     This.oOpt_File.Enabled     = Not pProcessoSN
     This.oTxt_File.Enabled     = Not pProcessoSN
     This.oTxt_Device.Enabled   = This.tReport<>'I' And This.oTxt_Device.Enabled
     * Bottone Anteprima e ESC
     This.oBtn_Ante.Enabled     = This.oBtn_Ante.Enabled And Iif(pProcessoSN,This.cAnteprimaSN,.T.) And Iif(pProcessoSN,This.pdnTotDocOk>0 And Not(This.pdExeProcesso),.T.)
     This.oBtn_Ante.ToolTipText = Iif(pProcessoSN,cp_Translate(MSG_SHOW_DOCUMENTS_TO_PROCESS),cp_Translate(MSG_PRINT_PREVIEW) )
     This.oBtn_Esc.ToolTipText  = Iif(pProcessoSN,Iif(This.pdObbligatorio,cp_Translate(MSG_PRESS_TO_EXIT),cp_Translate(MSG_PRESS_TO_ACCESS_TO_PRINT_SYSTEM_STANDARD)),cp_Translate(MSG_EXIT))
     This.oBtn_Edi.Enabled      = g_VEFA='S'
     * --- Zucchetti aulla Inizio - Integrazione Archeasy
     This.oBtn_StpArchi.Enabled= Not pProcessoSN And (g_ARCHI='S' Or g_ARCHI<>'S' And g_CPIN='N' And g_DMIP='N' And g_IZCP$'SA')
     This.oBtn_StpArchi.Visible = ! IsAlt()
     Thisform.oBtn_Query.Enabled=This.tReport <> 'I' And cp_isAdministrator(.T.) And Not pProcessoSN
     * Settaggi per lingue Verificare meglio utilit� e correggere errori
     cp_CHPFUN (This,'SettaggiLingue')
     * --- Zucchetti Aulla Fine
   Endproc
  
   Procedure ExecuteReport(pMultiReportSplit)
     Private i_oErr,i_sok
     * inizializzazioni
     i_sok=.T.
     i_oErr=On('ERROR')
     On Error i_sok=.F.
     *
     If Not(Empty(This.prStamp))
       Set Printer To Name (This.prStamp)
     Else
       Set Printer To Default
     Endif
     If i_sok
       Do RunRep With This,pMultiReportSplit, 'S', This.prfile, This.prCopie
     Endif
     * Ripristina stampante predefinita
     Set Printer To Default
     * Ripristina on-error
     If Empty(i_oErr)
       On Error
     Else
       On Error &i_oErr
     Endif
     * Risultato
     Return i_sok
   Endproc
   ** DOCM Fine
   Func GetHelpFile()
     Return(i_CpDic)
   Endfunc
   Func GetHelpTopic()
     Return(This.Caption)
   Endfunc
   Func HasCPEvents(i_cOp)
     Return(i_cOp+'|'$'ecpQuit|ecpSecurity|')
   Endfunc
   Proc ecpQuit()
     This.oBtn_Esc.Click()
   Endproc
  
   Proc ecpSecurity()
     cp_CHPFUN (This ,'ecpSecurity')
   Endproc
  
   Proc Activate()
     i_curform=This
   Endproc
  
   Proc Destroy()
     *!*	    if thisform.bModifiedFrx
     *!*	      * --- distrugge la tabella e il database temporanei
     *!*	      local n
     *!*	      n=getenv('TEMP')
     *!*	      local d
     *!*	      d=set('DATABASE')
     *!*	      set database to (n+'\_data_')
     *!*	      select __tmp__
     *!*	      use
     *!*	      close database
     *!*	      delete database (n+'\_data_')
     *!*	      delete file (n+'\__tmp__.dbf')
     *!*	    endif
     i_curform=.Null.
   Endproc
   Procedure Activate()
     DoDefault()
     * A volte la maschera di stampa appare disegnata male
     Local w_PrintSystemBorderStyle
     w_PrintSystemBorderStyle = This.BorderStyle
     If w_PrintSystemBorderStyle <> 0
       This.BorderStyle = 0
       This.BorderStyle = w_PrintSystemBorderStyle
     Else
       This.BorderStyle = 1
       This.BorderStyle = 0
     Endif
   Endproc
   Procedure Resize()
     DoDefault()
     * A volte l'etichetta della seconda pagina appare tagliata
     If i_visualtheme = -1
       This.oPgFrm.Width = This.Width
     Endif
   Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_chpPag1","cp_chp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOpt_Language_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_chp
        IF (NVL (g_DOCM,' ')='S')
          this.page1.opag.ADDOBJECT  ( 'oPDPrgBar','oProgressBar'   )
          this.parent.oPDPrgBar=this.page1.opag.oPDPrgBar
      *WITH Top = 147,Left = 200, Height = 20, Width = 424, Min=0, Max=100, Visible=.f.
        this.parent.oPDPrgBar.Top = 147
        this.parent.oPDPrgBar.Left = 200
        this.parent.oPDPrgBar.Height = 20
        this.parent.oPDPrgBar.Width = 370
        this.parent.oPDPrgBar.Min=0
        this.parent.oPDPrgBar.Max=100
        this.parent.oPDPrgBar.Visible=.f.
          
        this.page1.opag.ADDOBJECT (  'oPDLbl1','label')
        this.parent.oPDLbl1=this.page1.opag.oPDLbl1
       * WITH Caption='0%', Top=168, Left=200, Height=17, Width=30, FontSize=8, Visible=.f., Alignment=2, BackStyle=0
        this.parent.oPDLbl1.Caption='0%'
        this.parent.oPDLbl1.Top=168
        this.parent.oPDLbl1.Left=this.parent.oPDPrgBar.Left-15
        this.parent.oPDLbl1.Height=17
        this.parent.oPDLbl1.Width=30
        this.parent.oPDLbl1.FontSize=8
        this.parent.oPDLbl1.Visible=.f.
        this.parent.oPDLbl1.Alignment=2
        this.parent.oPDLbl1.BackStyle=0
        
        this.page1.opag.ADDOBJECT ( 'oPDLbl5' , 'label' )
        this.parent.oPDLbl5=this.page1.opag.oPDLbl5
       * WITH Caption='25%', Top=168, Left=306-15, Height=17, Width=30, FontSize=8, Visible=.f., Alignment=2, BackStyle=0          
        this.parent.oPDLbl5.Caption='25%'
        this.parent.oPDLbl5.Top=168
        this.parent.oPDLbl5.Left=this.parent.oPDPrgBar.Left-15+int(this.parent.oPDPrgBar.Width/4)
        this.parent.oPDLbl5.Height=17
        this.parent.oPDLbl5.Width=30
        this.parent.oPDLbl5.FontSize=8
        this.parent.oPDLbl5.Visible=.f.
        this.parent.oPDLbl5.Alignment=2
        this.parent.oPDLbl5.BackStyle=0
    
        this.page1.opag.ADDOBJECT ( 'oPDLbl2' , 'label' )
        this.parent.oPDLbl2=this.page1.opag.oPDLbl2
     *WITH Caption='50%', Top=168, Left=412-15, Height=17, Width=30, FontSize=8, Visible=.f., Alignment=2, BackStyle=0          
        this.parent.oPDLbl2.Caption='50%'
        this.parent.oPDLbl2.Top=168
        this.parent.oPDLbl2.Left=this.parent.oPDPrgBar.Left-15+int(this.parent.oPDPrgBar.Width/4)*2
        this.parent.oPDLbl2.Height=17
        this.parent.oPDLbl2.Width=30
        this.parent.oPDLbl2.FontSize=8
        this.parent.oPDLbl2.Visible=.f.
        this.parent.oPDLbl2.Alignment=2
        this.parent.oPDLbl2.BackStyle=0
        
        this.page1.opag.ADDOBJECT ( 'oPDLbl6' , 'label')
        this.parent.oPDLbl6=this.page1.opag.oPDLbl6
     *WITH Caption='75%', Top=168, Left=518-15, Height=17, Width=30, FontSize=8, Visible=.f., Alignment=2, BackStyle=0          
        this.parent.oPDLbl6.Caption='75%'
        this.parent.oPDLbl6.Top=168
        this.parent.oPDLbl6.Left=this.parent.oPDPrgBar.Left-15+int(this.parent.oPDPrgBar.Width/4)*3
        this.parent.oPDLbl6.Height=17
        this.parent.oPDLbl6.Width=30
        this.parent.oPDLbl6.FontSize=8
        this.parent.oPDLbl6.Visible=.f.
        this.parent.oPDLbl6.Alignment=2
        this.parent.oPDLbl6.BackStyle=0
     
        this.page1.opag.ADDOBJECT ('oPDLbl3','label' ) 
        this.parent.oPDLbl3=this.page1.opag.oPDLbl3
        *WITH Caption='100%', Top=168, Left=624-30, Height=17, *Width=30, FontSize=8, Visible=.f., Alignment=2, BackStyle=0          
        this.parent.oPDLbl3.Caption='100%'
        this.parent.oPDLbl3.Top=168
        this.parent.oPDLbl3.Left=this.parent.oPDPrgBar.Left-15+this.parent.oPDPrgBar.Width
        this.parent.oPDLbl3.Height=17
        this.parent.oPDLbl3.Width=30
        this.parent.oPDLbl3.FontSize=8
        this.parent.oPDLbl3.Visible=.f.
        this.parent.oPDLbl3.Alignment=2
        this.parent.oPDLbl3.BackStyle=0
    
        ENDIF
       g_oPrintForm=this.parent
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='STMPFILE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- cp_chp
    this.treport=EVL(NVL(treport,"")," ")
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_Opt_Language=space(20)
      .w_DEVICE=space(200)
      .w_Txt_Copies=0
      .w_Opt_File=space(20)
      .w_FORMFILE=space(10)
      .w_TIPOFILE=space(10)
      .w_GESTIONE=space(10)
      .w_Txt_File=space(200)
      .w_PCNAME=space(10)
      .w_SEL=space(10)
      .w_SFFLOPEN=space(1)
      .w_PRODOCM=space(10)
      .w_TREPORT=space(10)
      .w_FRXFILE=.f.
      .w_SETCONF=.f.
        .w_Opt_Language = 'ITA'
          .DoRTCalc(2,2,.f.)
        .w_Txt_Copies = 1
          .DoRTCalc(4,4,.f.)
        .w_FORMFILE = IIF (TYPE ("this.oOpt_File.displayvalue")='C' ,this.oOpt_File.displayvalue,this.w_Opt_File)
        .w_TIPOFILE = IIF (TYPE ("this.oOpt_File.displayvalue")='C' ,this.oOpt_File.displayvalue,this.w_Opt_File)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_TIPOFILE))
          .link_1_9('Full')
        endif
          .DoRTCalc(7,8,.f.)
        .w_PCNAME = Left( Sys(0), Rat('#',Sys( 0 ))-1)
        .w_SEL = 'S'
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
          .DoRTCalc(11,11,.f.)
        .w_PRODOCM = 'N'
        .w_TREPORT = this.treport
        .w_FRXFILE = .f.
    endwith
    this.DoRTCalc(15,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_chp
      proc ecpQuit()
        CP_CHPFUN (this,'EscClick')  
      
      
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_Opt_File<>.w_Opt_File
            .w_FORMFILE = IIF (TYPE ("this.oOpt_File.displayvalue")='C' ,this.oOpt_File.displayvalue,this.w_Opt_File)
        endif
        if .o_Opt_File<>.w_Opt_File.or. .o_FORMFILE<>.w_FORMFILE
            .w_TIPOFILE = IIF (TYPE ("this.oOpt_File.displayvalue")='C' ,this.oOpt_File.displayvalue,this.w_Opt_File)
          .link_1_9('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        if .o_Opt_File<>.w_Opt_File.or. .o_Txt_File<>.w_Txt_File
          .Calculate_LRIUXYFAYZ()
        endif
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
    endwith
  return

  proc Calculate_LRIUXYFAYZ()
    with this
          * --- Modifica estensione file
          .w_Txt_File = ForceExt(.w_Txt_File, .w_Opt_File)
          .prFile = .w_Txt_File
          .cDescReport = .w_Txt_File
          .cExtFormat = .w_Opt_File
          .prTyExp = .w_Opt_File
    endwith
  endproc
  proc Calculate_FDGOTESUYE()
    with this
          * --- Inizializza combo delle estenzioni file
          .w_opt_file = iif(TYPE('g_SCHEDULER')='C' AND g_SCHEDULER='S' , 'PDF',this.oopt_file.combovalues[1])
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFORMFILE_1_8.visible=!this.oPgFrm.Page1.oPag.oFORMFILE_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_18.visible=!this.oPgFrm.Page1.oPag.oBtn_1_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_50.visible=!this.oPgFrm.Page1.oPag.oBtn_1_50.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_54.visible=!this.oPgFrm.Page1.oPag.oBtn_1_54.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_FDGOTESUYE()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- cp_chp
    IF cevent = 'w_Opt_File InteractiveChange'
    this.oOpt_File.valid()
    this.w_FORMFILE=this.oOpt_File.displayvalue
    this.o_FORMFILE='x'
    this.mcalc(.t.)
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPOFILE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STMPFILE_IDX,3]
    i_lTable = "STMPFILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2], .t., this.STMPFILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOFILE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOFILE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SFFLGSEL,SFPCNAME,SFFORMAT,SFMSKCFG,SFFLOPEN";
                   +" from "+i_cTable+" "+i_lTable+" where SFFORMAT="+cp_ToStrODBC(this.w_TIPOFILE);
                   +" and SFFLGSEL="+cp_ToStrODBC(this.w_SEL);
                   +" and SFPCNAME="+cp_ToStrODBC(this.w_PCNAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SFFLGSEL',this.w_SEL;
                       ,'SFPCNAME',this.w_PCNAME;
                       ,'SFFORMAT',this.w_TIPOFILE)
            select SFFLGSEL,SFPCNAME,SFFORMAT,SFMSKCFG,SFFLOPEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOFILE = NVL(_Link_.SFFORMAT,space(10))
      this.w_GESTIONE = NVL(_Link_.SFMSKCFG,space(10))
      this.w_SFFLOPEN = NVL(_Link_.SFFLOPEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOFILE = space(10)
      endif
      this.w_GESTIONE = space(10)
      this.w_SFFLOPEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])+'\'+cp_ToStr(_Link_.SFFLGSEL,1)+'\'+cp_ToStr(_Link_.SFPCNAME,1)+'\'+cp_ToStr(_Link_.SFFORMAT,1)
      cp_ShowWarn(i_cKey,this.STMPFILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOFILE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOpt_Language_1_1.RadioValue()==this.w_Opt_Language)
      this.oPgFrm.Page1.oPag.oOpt_Language_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEVICE_1_2.value==this.w_DEVICE)
      this.oPgFrm.Page1.oPag.oDEVICE_1_2.value=this.w_DEVICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTxt_Copies_1_4.value==this.w_Txt_Copies)
      this.oPgFrm.Page1.oPag.oTxt_Copies_1_4.value=this.w_Txt_Copies
    endif
    if not(this.oPgFrm.Page1.oPag.oOpt_File_1_7.RadioValue()==this.w_Opt_File)
      this.oPgFrm.Page1.oPag.oOpt_File_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFORMFILE_1_8.value==this.w_FORMFILE)
      this.oPgFrm.Page1.oPag.oFORMFILE_1_8.value=this.w_FORMFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oTxt_File_1_11.value==this.w_Txt_File)
      this.oPgFrm.Page1.oPag.oTxt_File_1_11.value=this.w_Txt_File
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKNFILE(.w_Txt_File, 'F'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTxt_File_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_Opt_File = this.w_Opt_File
    this.o_FORMFILE = this.w_FORMFILE
    this.o_Txt_File = this.w_Txt_File
    return

enddefine

* --- Define pages as container
define class tcp_chpPag1 as StdContainer
  Width  = 795
  height = 191
  stdWidth  = 795
  stdheight = 191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oOpt_Language_1_1 as StdZLanCombo with uid="DLGURVVZZC",rtseq=1,rtrep=.f.,left=12,top=23,width=89,height=21;
    , RowSourceType=1;
    , ToolTipText = "tipo file";
    , HelpContextID = 78981348;
    , cFormVar="w_Opt_Language",tablefilter="ITA=ITA", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  add object oDEVICE_1_2 as StdField with uid="YJTBDAQENK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DEVICE", cQueryName = "DEVICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Stampante selezionata",;
    HelpContextID = 155708152,;
   bGlobalFont=.t.,;
    Height=21, Width=332, Left=116, Top=23, InputMask=replicate('X',200)


  add object oBtn_1_3 as StdButton with uid="BYWCYEOHKL",left=452, top=23, width=27,height=23,;
    caption="...", nPag=1;
    , ToolTipText = ""+MSG_CHANGE_PRINTER+"";
    , HelpContextID = 107911537;
    , Caption="...";
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"SStpClick") 
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTxt_Copies_1_4 as StdField with uid="GBLWNLOKKP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_Txt_Copies", cQueryName = "Txt_Copies",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 115486094,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=492, Top=23


  add object oBtn_1_5 as StdButton with uid="NUOPZJSCRA",left=598, top=9, width=48,height=47,;
    CpPicture="PRASSOC.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_CONNECT_REPORT_TO_PRINTER+"";
    , HelpContextID = 219548855;
    , Caption=MSG_REPORT_BS_PRINTER;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"AssocClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_6 as StdButton with uid="TPMPZXYMVN",left=652, top=9, width=48,height=47,;
    CpPicture="MODIFTBL.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_CREATE_BS_MODIFY_REPORT+"";
    , HelpContextID = 102974236;
    , Caption=MSG_CHANGE_BUTTON2;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        cp_DoAction ("ecpSecurity")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TREPORT<>'I')
      endwith
    endif
  endfunc


  add object oOpt_File_1_7 as StdTableCombo with uid="QDOWFWNDEA",rtseq=4,rtrep=.f.,left=12,top=90,width=89,height=21;
    , ToolTipText = "tipo file";
    , HelpContextID = 224633581;
    , cFormVar="w_Opt_File",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='cp_chpfu.vqr',cKey='SFEXTFOR',cValue='SFFORMAT',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  add object oFORMFILE_1_8 as StdField with uid="TFKCTHXSTR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FORMFILE", cQueryName = "FORMFILE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 8145106,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=712, Top=91, InputMask=replicate('X',10)

  func oFORMFILE_1_8.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oTxt_File_1_11 as StdField with uid="CIRLONFEHJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_Txt_File", cQueryName = "Txt_File",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path+nome file",;
    HelpContextID = 43801883,;
   bGlobalFont=.t.,;
    Height=21, Width=387, Left=114, Top=90, InputMask=replicate('X',200)

  func oTxt_File_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKNFILE(.w_Txt_File, 'F'))
    endwith
    return bRes
  endfunc


  add object oBtn_1_12 as StdButton with uid="KFZKZIEDQW",left=505, top=90, width=27,height=23,;
    CpPicture="PROPEN.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SELECT_FILE+"";
    , HelpContextID = 107923313;
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"SFileClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="FBQYQNLATQ",left=542, top=76, width=49,height=47,;
    CpPicture="..\exe\bmp\PRFILEOPT.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_PRINT_OR_EXPORT_ON_FILE_WITH_OPTIONS+"";
    , HelpContextID = 73630323;
    , Caption=MSG_OPTIONS_SAVE;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"StpFileOptClick") 
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY (NVL(.w_GESTIONE,'')) and .treport<>'I' AND NOT.pdProcesso AND NOT bReportVuoto )
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="KJIGGTDLVC",left=598, top=76, width=48,height=47,;
    CpPicture="PRFILE.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_PRINT_OR_EXPORT_ON_FILE+"";
    , HelpContextID = 65549171;
    , Caption=MSG_S_SAVE ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"StpFileClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT.pdProcesso AND NOT bReportVuoto )
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="GHFIMVPFCJ",left=652, top=76, width=48,height=47,;
    CpPicture="..\exe\bmp\pdfarch.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_ARCHIVING_DOCUMENTS+"";
    , HelpContextID = 214617243;
    , Caption="Arc.\<doc.";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"SaveIndClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="ODPIUYUKDW",left=75, top=140, width=48,height=47,;
    CpPicture="bmp\auto.bmp", caption="", nPag=1;
    , ToolTipText = ""+MSG_EXEC_DOCUMENT_PROCESS+"";
    , HelpContextID = 36376790;
    , Caption=MSG_EXEC;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"oPDBtnProcClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="ZBIZEJCNQZ",left=132, top=140, width=48,height=47,;
    CpPicture="bmp\TZOOM.bmp", caption="", nPag=1;
    , ToolTipText = ""+MSG_DOCUMENT_PROCESS_DETAIL+"";
    , HelpContextID = 268080757;
    , Caption=MSG_DETAIL ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"PDBtnStatusClick")   
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="OGTSETTKJY",left=652, top=76, width=48,height=47,;
    CpPicture="..\exe\bmp\Archeasy.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_ARCHIVING_ARCHEASY_DOCUMENTS+"";
    , HelpContextID = 18309990;
    , Caption="Arc\<heasy" ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"StpArchiClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.T.)
     endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="WVROOYKFOO",left=652, top=76, width=48,height=47,;
    CpPicture="fxzcp.ico", caption="", nPag=1;
    , HelpContextID = 87330310;
    , Caption='wwp/zpc';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"WWP_ZCPClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="SYUJSWCUEJ",left=75, top=140, width=48,height=47,;
    CpPicture="PRSTAMPA.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SEND_TO_PRINTER+"";
    , HelpContextID = 35115849;
    , Caption=MSG_S_PRINT ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"StpClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="YCIJBGHPJT",left=132, top=140, width=48,height=47,;
    CpPicture="PROPTION.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SEND_TO_PRINTER_WITH_OPTIONS+"";
    , HelpContextID = 51892949;
    , Caption=MSG_OPTIONS_PRINTER ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"StpOptClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_24 as StdButton with uid="GBDTDRAQVH",left=191, top=140, width=48,height=47,;
    CpPicture="PRWORD.ico", caption="", nPag=1;
    , HelpContextID = 234656585;
    , Caption='\<Word' ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"WordClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_25 as StdButton with uid="DZERHNUCVM",left=249, top=140, width=48,height=47,;
    CpPicture="PREXCEL.ico", caption="", nPag=1;
    , HelpContextID = 233091913;
    , Caption='\<Excel';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"ExcelClick")   
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="LPRBZXUKYV",left=307, top=140, width=48,height=47,;
    CpPicture="prgraph.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_MSGRAPH_GRAPH+"";
    , HelpContextID = 217410743;
    , Caption= MSG_GRAPH;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"GraphClick")       
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_30 as StdButton with uid="RRBNPQFGUH",left=598, top=140, width=48,height=47,;
    CpPicture="..\exe\bmp\logoedi.ico", caption="", nPag=1;
    , ToolTipText = "" + MSG_CHANGE_EDI + "";
    , HelpContextID = 43561801;
    , Caption = "E\<DI" ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"EdiClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_VEFA='S' AND not bReportVuoto AND .w_TREPORT<>'I')
      endwith
    endif
  endfunc

  func oBtn_1_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso or g_VEFA<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="IEXJASHSUC",left=542, top=140, width=48,height=47,;
    CpPicture="..\exe\bmp\FXFAX.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SENDING_FAX+"";
    , HelpContextID = 28025673;
    , Caption='\<Fax' , DisabledPicture="..\exe\bmp\fxfaxd.ico";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"FaxClick") 
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_32 as StdButton with uid="HSXWFOEJOP",left=365, top=140, width=48,height=47,;
    CpPicture="..\exe\bmp\Fxmail.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SENDING_EMAIL+"";
    , HelpContextID = 110834505;
    , Caption='E-\<mail';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"MailClick")   
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="CLLNAFRCMU",left=423, top=140, width=48,height=47,;
    CpPicture="..\exe\bmp\Fxmail_pec.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SENDING_EMAIL_PEC+"";
    , HelpContextID = 32256841;
    , Caption='\<PEC';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSUT_BVG(this.Parent.oContained,"",.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_34 as StdButton with uid="KWGCKUABPU",left=481, top=140, width=48,height=47,;
    CpPicture="..\exe\bmp\PLite.ico", caption="", nPag=1;
    , ToolTipText = ""+MSG_SENDING_BY_POSTALITE+"";
    , HelpContextID = 24116407;
    , Caption='\<PostaLite' , DisabledPicture="..\exe\bmp\plited.ico";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"PliteClick")    
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_37 as StdButton with uid="BDHEEBNWXU",left=652, top=140, width=48,height=47,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = ""+MSG_EXIT+"";
    , HelpContextID = 152829416;
    , Caption=MSG_S_EXIT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"EscClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_39 as cp_runprogram with uid="STMQUKDDXO",left=4, top=204, width=230,height=28,;
    caption='Inizializza',;
   bGlobalFont=.t.,;
    prg="CP_CHPFUN('init')",;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Inizializza maschera";
    , HelpContextID = 110538219


  add object oBtn_1_43 as StdButton with uid="VFOOFOVCCT",left=16, top=140, width=48,height=47,;
    CpPicture="bmp\PRVIDEO.bmp", caption="", nPag=1;
    , ToolTipText = ""+MSG_PRINT_PREVIEW+"";
    , HelpContextID = 215937865;
    , Caption=MSG_PREVIEW;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"AnteClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_44 as cp_runprogram with uid="VPMCJOETEN",left=6, top=263, width=230,height=28,;
    caption='Inizializza',;
   bGlobalFont=.t.,;
    prg="CP_CHPFUN('Txt_Copies LostFocus')",;
    cEvent = "w_Txt_Copies LostFocus",;
    nPag=1;
    , ToolTipText = "Inizializza maschera";
    , HelpContextID = 110538219


  add object oObj_1_45 as cp_runprogram with uid="SGRAMLPXRJ",left=240, top=207, width=230,height=28,;
    caption='Inizializza',;
   bGlobalFont=.t.,;
    prg="CP_CHPFUN('Opt_Language Changed')",;
    cEvent = "w_Opt_Language Changed",;
    nPag=1;
    , ToolTipText = "Inizializza maschera";
    , HelpContextID = 110538219


  add object oObj_1_46 as cp_runprogram with uid="LYSIXNDBCA",left=243, top=238, width=230,height=28,;
    caption='Inizializza',;
   bGlobalFont=.t.,;
    prg="CP_CHPFUN('BlankRecEnd')",;
    cEvent = "Blank",;
    nPag=1;
    , ToolTipText = "Inizializza maschera";
    , HelpContextID = 110538219


  add object oBtn_1_50 as StdButton with uid="ZPVAYUWSLC",left=543, top=9, width=48,height=47,;
    CpPicture="..\exe\bmp\run16.bmp", caption="", nPag=1;
    , ToolTipText = "Visual Query";
    , HelpContextID = 216662946;
    , Caption="\<Query" ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_50.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"QueryClick")  
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_50.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(cp_isAdministrator(.t.) and vartype(g_Debug_Print)='C' and g_Debug_Print='S'))
     endwith
    endif
  endfunc


  add object oBtn_1_54 as StdButton with uid="AYVTSRRPIJ",left=598, top=140, width=48,height=47,;
    CpPicture="..\exe\bmp\fatel.ico", caption="", nPag=1;
    , ToolTipText = "" + MSG_CHANGE_FATEL + "";
    , HelpContextID = 216769353;
    , Caption = "Fa\<tel" ;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      with this.Parent.oContained
        CP_CHPFUN (this.Parent.oContained,"FatelClick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_54.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_VEFA<>'S' AND not bReportVuoto AND .w_TREPORT<>'I')
      endwith
    endif
  endfunc

  func oBtn_1_54.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.pdProcesso or g_VEFA='S' )
     endwith
    endif
  endfunc

  add object oStr_1_27 as StdString with uid="SHCDKROQHP",Visible=.t., Left=12, Top=3,;
    Alignment=0, Width=120, Height=18,;
    Caption=""+MSG_LANGUAGE+""  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="BQSULVPABR",Visible=.t., Left=114, Top=69,;
    Alignment=0, Width=120, Height=18,;
    Caption=""+MSG_FILE_NAME+""  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="UOQLUQMRXZ",Visible=.t., Left=12, Top=69,;
    Alignment=0, Width=148, Height=18,;
    Caption=""+MSG_PRINT_ON_FILE+""  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="KGOBYFPQME",Visible=.t., Left=432, Top=7,;
    Alignment=1, Width=108, Height=18,;
    Caption=""+MSG_COPIES_NO_F+""  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="RJGGVUYORV",Visible=.t., Left=116, Top=3,;
    Alignment=0, Width=120, Height=18,;
    Caption=""+MSG_PRINTER+""  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="ZXOIZTONXD",Visible=.t., Left=203, Top=120,;
    Alignment=0, Width=188, Height=18,;
    Caption=""+MSG_WORKING_PROGRESS+""  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (NOT this.Parent.ocontained.pdProcesso)
    endwith
  endfunc

  add object oBox_1_40 as StdBox with uid="ZTNPVHYKLO",left=2, top=136, width=702,height=1

  add object oBox_1_41 as StdBox with uid="DAXSITZOYC",left=2, top=61, width=702,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_chp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_chp
define class StdZLanCombo  as  StdCombo 
  func RadioValue()
    return  " "
  endfunc
  
  func GetRadio()
    return  " "
  endfunc
  
  func SetRadio()
    return  " "
  endfunc
enddefine

**** Zucchetti Aulla Inizio: Funzioni utilizzate in report per invio caratteri di controllo
Function Set10CPI()
  if ts_Enabled
     return iif(empty(ts_10Cpi),'',eval(ts_10Cpi))
  else
     return ''
  endif   
endfunc

Function Set12CPI()
  if ts_Enabled
     return iif(empty(ts_12Cpi),'',eval(ts_12Cpi))
  else
     return ''
  endif   
endfunc

Function Set15CPI()
  if ts_Enabled
     return iif(empty(ts_15Cpi),'',eval(ts_15Cpi))
  else
     return ''
  endif   
endfunc

Function SetCompress()
  if ts_Enabled
     return iif(empty(ts_Comp),'',eval(ts_Comp))
  else
     return ''
  endif   
endfunc

Function ResetCompress()
  if ts_Enabled
     return iif(empty(ts_RtComp),'',eval(ts_RtComp))
  else
     return ''
  endif   
endfunc

Function SetBold()
  if ts_Enabled
     return iif(empty(ts_StBold),'',eval(ts_StBold))
  else
     return ''
  endif   
endfunc

Function ResetBold()
  if ts_Enabled
     return iif(empty(ts_FiBold),'',eval(ts_FiBold))
  else
     return ''
  endif   
endfunc

Function SetDouble()
  if ts_Enabled
     return iif(empty(ts_StDoub),'',eval(ts_StDoub))
  else
     return ''
  endif   
endfunc

Function ResetDouble()
  if ts_Enabled
     return iif(empty(ts_FiDoub),'',eval(ts_FiDoub))
  else
     return ''
  endif   
endfunc

Function SetItalics()
  if ts_Enabled
     return iif(empty(ts_StItal),'',eval(ts_StItal))
  else
     return ''
  endif   
endfunc

Function ResetItalics()
  if ts_Enabled
     return iif(empty(ts_FiItal),'',eval(ts_FiItal))
  else
     return ''
  endif   
endfunc

Function SetUnderline()
  if ts_Enabled
     return iif(empty(ts_StUnde),'',eval(ts_StUnde))
  else
     return ''
  endif   
endfunc

Function ResetUnderline()
  if ts_Enabled
     return iif(empty(ts_FiUnde),'',eval(ts_FiUnde))
  else
     return ''
  endif   
endfunc
* --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
Function createStruct( toReflection, tcTypeName )
  local loPropertyValue, loTemp
  loPropertyValue = createobject( "relation")
  toReflection.forName( tcTypeName ).createobject( @loPropertyValue )
  return( loPropertyValue )
endfunc
* --- Zucchetti Aulla - Fine Gestione Suite OpenOffice/StarOffice
***** Zucchetti Aulla Fine


* --- Fine Area Manuale
