* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_postitsendto                                                 *
*              "+MSG_SENDTO+"                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-20                                                      *
* Last revis.: 2009-05-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_postitsendto",oParentObject))

* --- Class definition
define class tcp_postitsendto as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 650
  Height = 518
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-05-27"
  HelpContextID=229526777
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=0

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_postitsendto"
  cComment = ""+MSG_SENDTO+""
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PostitUserZoom = .NULL.
  w_PostitGroupZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_postitsendtoPag1","cp_postitsendto",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_PostitUserZoom = this.oPgFrm.Pages(1).oPag.PostitUserZoom
    this.w_PostitGroupZoom = this.oPgFrm.Pages(1).oPag.PostitGroupZoom
    DoDefault()
    proc Destroy()
      this.w_PostitUserZoom = .NULL.
      this.w_PostitGroupZoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do cp_PostitSending with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .oPgFrm.Page1.oPag.PostitUserZoom.Calculate()
      .oPgFrm.Page1.oPag.PostitGroupZoom.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.PostitUserZoom.Calculate()
        .oPgFrm.Page1.oPag.PostitGroupZoom.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.PostitUserZoom.Calculate()
        .oPgFrm.Page1.oPag.PostitGroupZoom.Calculate()
    endwith
  return

  proc Calculate_AOBMCLRUAQ()
    with this
          * --- cp_PostitGroupsZoomSel
          cp_PostitGroupsZoomSel(this;
             )
    endwith
  endproc
  proc Calculate_ATHEPNDLJK()
    with this
          * --- cp_PostitUserZoomSel
          cp_PostitUserZoomSel(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.PostitUserZoom.Event(cEvent)
      .oPgFrm.Page1.oPag.PostitGroupZoom.Event(cEvent)
        if lower(cEvent)==lower("w_postitgroupzoom row checked") or lower(cEvent)==lower("w_postitgroupzoom row unchecked")
          .Calculate_AOBMCLRUAQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_postituserzoom row checked") or lower(cEvent)==lower("w_postituserzoom row unchecked")
          .Calculate_ATHEPNDLJK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_postitsendtoPag1 as StdContainer
  Width  = 646
  height = 518
  stdWidth  = 646
  stdheight = 518
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object PostitUserZoom as cp_szoombox with uid="KHZECFPCQZ",left=14, top=25, width=617,height=204,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="PostitUsers",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,cTable="cpusers",cMenuFile="",cZoomOnZoom="",bQueryOnLoad=.t.,;
    nPag=1;
    , HelpContextID = 167474038


  add object PostitGroupZoom as cp_szoombox with uid="GBZJUOSUJU",left=14, top=256, width=617,height=204,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="PostitGroups",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,cTable="cpgroups",cMenuFile="",cZoomOnZoom="",bQueryOnLoad=.t.,;
    nPag=1;
    , HelpContextID = 167474038


  add object oBtn_1_5 as StdButton with uid="OXLJQSTOTY",left=520, top=465, width=48,height=45,;
    CpPicture="bmp\btsend.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per inviare il post-in agli utenti selezionati";
    , HelpContextID = 23908132;
    , Caption=MSG_SENDING_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_6 as StdButton with uid="DKNBURUTRO",left=582, top=465, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 239367127;
    , Caption=MSG_CANCEL_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="ENBAHJCAQL",Visible=.t., Left=32, Top=241,;
    Alignment=0, Width=103, Height=18,;
    Caption="Gruppi"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="UEWGCPKFLX",Visible=.t., Left=32, Top=10,;
    Alignment=0, Width=103, Height=18,;
    Caption="Utenti"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_postitsendto','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_postitsendto
Proc cp_PostitGroupsZoomSel(pParent)
  local l_GroupsCur, l_UsersCur, l_nGroup
  l_GroupsCur = pParent.w_PostitGroupZoom.cCursor
  l_UsersCur = pParent.w_PostitUserZoom.cCursor
  Update (l_UsersCur) Set xChk=0
  *--- Sposto il focus per far aggiornare la colonna xchk dello zoom dei gruppi
  pParent.w_PostitUserZoom.SetFocus()
  Select * From (l_GroupsCur) Where xChk=1 Into Cursor tmp_GroupSel
  Scan
    l_nGroup = tmp_GroupSel.code
    If i_ServerConn[1,2]<>0
      =cp_SQL(i_ServerConn[1,2],"select cpusrgrp.usercode as code from cpusrgrp where cpusrgrp.groupcode =" +cp_ToStrODBC(m.l_nGroup), "tmp_users")
    Else
      select cpusrgrp.usercode from cpusrgrp as code where cpusrgrp.groupcode = m.l_nGroup into cursor tmp_users
    Endif
    Update (l_UsersCur) Set xchk=1 Where code in (select code from tmp_users)
    use in select("tmp_users") 
    Select tmp_GroupSel
  EndScan
  Use In Select("tmp_GroupSel")
  Select(l_UsersCur)
  Go Top
  pParent.w_PostitUserZoom.Refresh()
EndProc

Proc cp_PostitUserZoomSel(pParent)
  local l_GroupsCur
  l_GroupsCur = pParent.w_PostitGroupZoom.cCursor
  Update (l_GroupsCur) Set xChk=0
  Select(l_GroupsCur)
  Go Top
  pParent.w_PostitGroupZoom.Refresh()  
EndProc

Proc cp_PostitSending(pParent)
  local l_UsersCur
  l_UsersCur = pParent.w_PostitUserZoom.cCursor
  Select code as UserCode from (l_UsersCur) where xchk=1 into cursor _tmp_users_ 
  If type("pParent.oParentObject.AttachArea")<>'U'
    pParent.oParentObject.AttachArea.SendToGroup(0, '', .t., "_tmp_users_")
  Else
    pParent.oParentObject.SendToGroup(0, '', .t., "_tmp_users_")
  EndIf
  use in select("_tmp_users_")
EndProc
* --- Fine Area Manuale
