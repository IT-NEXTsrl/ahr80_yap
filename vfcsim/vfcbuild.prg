* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VFCBUILD
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Marco Pasquali
* Data creazione: 13/04/2010
* Aggiornato il : 13/04/2010
* Translated    :
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Disegnatore di grafici
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"
Lparameters pSource, pConfigFileName
Local l_Source, l_ConfigFileName
l_Source = Iif(Vartype(pSource)='C', pSource, "")
l_ConfigFileName = Iif(Vartype(pConfigFileName)='C', pConfigFileName, "")

oFoxChartEditor=Createobject('menu_toolbar_new',Type("i_bFox26")<>'U',Vartype(_Screen.cp_ThemesManager)=="O" And i_VisualTheme<>-1, l_Source, l_ConfigFileName)

If Type('oFoxChartEditor')<>'L'
    && i_curform=oFoxChartEditor
    * --- Zucchetti aulla inizio Framework monitor
    If (Type('i_designfile')='C')
        oFoxChartEditor.cFileName = Alltrim(i_designfile)
        oFoxChartEditor.EditFoxCharts()
        oFoxChartEditor.SetTitle()
        oFoxChartEditor.LoadDoc()
    Else
        * --- Zucchetti aulla fine Framework monitor
        oFoxChartEditor.EditFoxCharts()
        * --- Zucchetti aulla inizio Framework monitor
    Endif
    * --- Zucchetti aulla fine Framework monitor
Endif
Return


Define Class menu_toolbar_new As microtool_toolbar
    oForm=.Null.
    oThis=.Null.
    HelpContextID=3
    cExt='vfc'
    bFox26=.F.

    bSec1=.T.
    bSec2=.T.
    bSec3=.T.
    bSec4=.T.

    w_Source=''
    w_ConfigFileName=''
    bModified =.F.

    Proc Init(i_bFox26,bStyle, pSource, pConfigFileName)
        DoDefault(m.bStyle)
        This.cToolNAME = cp_Translate(MSG_FOXCHARTS)
        This.bFox26=i_bFox26
        If Type("i_bFox26")='U'
            cp_GetSecurity(This,'FoxChartEditor')
        Endif
        This.Dock(i_nCPToolBarPos)
        This.oThis=This
        This.w_Source = Iif(Vartype(pSource)='C', pSource, "")
        This.w_ConfigFileName = Iif(Vartype(pConfigFileName)='C', pConfigFileName, "")
        Return

    Proc EditFoxCharts()
        If !This.bSec1
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            This.Exit()
        Else
            This.Show()
            This.oForm = cp_FoxChartEditor(This)
            If !Empty(This.w_ConfigFileName)
                This.cFileName = This.w_ConfigFileName
                This.oForm.w_oChart.LoadFoxChartsFromFile(This.cFileName, This.w_Source)
                This.SetTitle()
                This.bModified=.F.
            Else
                This.SetTitle()
                If !Empty(This.w_Source) And cp_fileexist(Forceext(This.w_Source, 'VQR'))
                    This.oForm.w_oChart.oChart.QUERYMODEL = Sys(2014, This.w_Source)
                    This.oForm.NotifyEvent("RUNBYPARAM")
                    This.bModified=.T.
                Else
                    If !Empty(This.w_Source) And Used(This.w_Source)
                        This.oForm.w_oChart.cSource = This.w_Source
                        This.oForm.NotifyEvent("RUNBYPARAM")
                        This.bModified=.T.
                    Endif
                Endif
            Endif
            This.oForm.w_oChart.bDesignMode = .T.
        Endif
        Return

    Proc Exit()
        * ---- Eseguo Ripristina se non eseguito
        This.oForm.EcpQuit1()
        This.oForm=.Null.
        This.oThis=.Null.
        i_curform=.Null.
        This.Visible=.F.
        This.Destroy()
        On Key Label RIGHTMOUSE
        Return

    Func GetHelpTopic()
        Return('vfcbuild')

    Proc SetTitle()
        If Not Empty(This.cFileName)
            This.oForm.Caption=cp_Translate(MSG_FOXCHARTS)+ Space(1) + Displaypath(Forceext(This.cFileName,'VFC'),30)
        Else
            This.oForm.Caption=cp_Translate(MSG_FOXCHARTS)+ Space(1) + Forceext(cp_MsgFormat(MSG_NO_NAME), "VFC")
        Endif
        Return
    Proc ecpSecurity()
        Createobject('stdsProgAuth','FoxChartEditor')
        Return

    Proc NewDoc()
        This.SetInitValue()
        This.bModified=.F.
        Return

    Proc LoadDoc()
        Local xcg,h,c,l,c_,f_ext
        This.SetInitValue()
        c_=Rat('\',This.cFileName)+1
        fName=Substr(This.cFileName,c_,Len(This.cFileName)-(c_+3))
        * --- Inserisco il nome del menu per L'utente
        f_ext=Right(Substr(This.cFileName,c_),3)
        If(f_ext<>'VFC')
            cp_ErrorMsg(MSG_CANNOT_OPEN_FILE)
            Return
        Endif
        This.oForm.w_oChart.LoadFoxChartsFromFile(This.cFileName)
        This.SetTitle()
        This.bModified=.F.
        Return

    Proc SaveDoc()
        This.oForm.w_oChart.SaveFoxChartsToFile(This.cFileName)
        This.bModified=.F.
        Return

    Procedure SetInitValue()
        Use In Select(This.oForm.w_oChart.oChart.SourceAlias)
        This.oForm.w_oChart.oChart.SourceAlias=""
        Use In Select(This.oForm.w_oChart.oChart._DataCursor)
        This.oForm.w_oChart.oChart._DataCursor=""
        *!*	        This.oForm.w_oChart.cSource=""
        *!*	        This.oForm.cp_FoxchartProperties.w_cSource=""
        *!*	        This.oForm.cp_FoxchartProperties.w_FILECFG=""
        *!*	        This.oForm.cp_FoxchartProperties.w_QUERY=""
        *!*	        This.oForm.cp_FoxchartProperties.w_FILERPT=""
        This.oForm.BlankRec()
        *!*	        This.oForm.cp_FoxchartProperties.BlankRec()
        *!*	        This.oForm.cp_FoxchartProperties.NotifyEvent("SetValueToChart")

    Endproc

    Procedure bModified_Assign(bValue)
        This.bModified=m.bValue
        If m.bValue
            Local l_caption
            l_caption = This.oForm.Caption
            l_caption = Iif(Rat('*', l_caption)<>0, l_caption, Alltrim(l_caption)+" *")
            This.oForm.Caption = l_caption
            This.bEmpty=.F.
        Else
            l_caption = This.oForm.Caption
            l_caption = Iif(Rat('*', l_caption)<>0, Left(l_caption, Len(Alltrim(l_caption))-1), Alltrim(l_caption))
            This.oForm.Caption = l_caption
            This.oForm.cp_foxchartproperties.w_MODIFIED = m.bValue
        Endif
    Endproc


Enddefine
