* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_mngdatabase                                                  *
*              Installazione tabelle/database                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-10-11                                                      *
* Last revis.: 2015-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_mngdatabase
#Define TABLEPROPDIM 8

If cp_ascan(@i_TableProp,'cpttbls',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cpttbls'
		i_TableProp[i_nTables,2]='cpttbls'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif
If cp_ascan(@i_TableProp,'cptsrvr',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cptsrvr'
		i_TableProp[i_nTables,2]='cptsrvr'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif
If cp_ascan(@i_TableProp,'cpazi',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cpazi'
		i_TableProp[i_nTables,2]='cpazi'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif
If cp_ascan(@i_TableProp,'cprecsec',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cprecsec'
		i_TableProp[i_nTables,2]='cprecsec'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif
If cp_ascan(@i_TableProp,'cprecsed',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cprecsed'
		i_TableProp[i_nTables,2]='cprecsed'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1    
Endif
If cp_ascan(@i_TableProp,'cprecsecd',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cprecsecd'
		i_TableProp[i_nTables,2]='cprecsecd'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif
*-- Zucchetti Aulla Inizio
If cp_ascan(@i_TableProp,'moddconf',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='moddconf'
		i_TableProp[i_nTables,2]='MODDCONF'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif
If cp_ascan(@i_TableProp,'prcfgges',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='prcfgges'
		i_TableProp[i_nTables,2]='PRCFGGES'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif
If cp_ascan(@i_TableProp,'cfg_gest',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cfg_gest'
		i_TableProp[i_nTables,2]='CFG_GEST'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif

If cp_ascan(@i_TableProp,'conf_int',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='conf_int'
		i_TableProp[i_nTables,2]='CONF_INT'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif

*-- disabilito la configurazione per non dare errore al primo accesso con db vuoto
if Vartype(g_bDisableCfgGest)="U" or !g_bDisableCfgGest
    g_bDisableCfgGest=.T.
    Public g_CfgGestMod
    g_CfgGestMod = Vartype(g_bDisableCfgGest)="U"
endif
*-- Zucchetti Aulla Fine
* --- Fine Area Manuale
return(createobject("tcp_mngdatabase",oParentObject))

* --- Class definition
define class tcp_mngdatabase as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 590
  Height = 247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-07"
  HelpContextID=240210201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_mngdatabase"
  cComment = "Installazione tabelle/database"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SIZE = .F.
  w_BNEWPROCS = .F.
  w_ADDSRV = .F.
  w_ENOK = .F.
  w_PARAM_MNGDB = space(10)
  w_FILENAME = space(10)
  w_SERVER = space(10)
  w_MSG = .F.
  w_NSRV = .F.
  w_UPDATED = .F.
  w_ZTab = .NULL.
  w_ZSrv = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_mngdatabasePag1","cp_mngdatabase",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZTab = this.oPgFrm.Pages(1).oPag.ZTab
    this.w_ZSrv = this.oPgFrm.Pages(1).oPag.ZSrv
    DoDefault()
    proc Destroy()
      this.w_ZTab = .NULL.
      this.w_ZSrv = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SIZE=.f.
      .w_BNEWPROCS=.f.
      .w_ADDSRV=.f.
      .w_ENOK=.f.
      .w_PARAM_MNGDB=space(10)
      .w_FILENAME=space(10)
      .w_SERVER=space(10)
      .w_MSG=.f.
      .w_NSRV=.f.
      .w_UPDATED=.f.
        .w_SIZE = .T.
        .w_BNEWPROCS = .F.
          .DoRTCalc(3,3,.f.)
        .w_ENOK = .F.
        .w_PARAM_MNGDB = iif(type("this.oParentObject.w_PARAM_MNGDB")="C", this.oParentObject.w_PARAM_MNGDB, .w_PARAM_MNGDB)
        .w_FILENAME = .w_ZTAB.GETVAR('filename')
      .oPgFrm.Page1.oPag.ZTab.Calculate()
      .oPgFrm.Page1.oPag.ZSrv.Calculate()
        .w_SERVER = .w_ZSRV.GETVAR('servername')
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
    endwith
    this.DoRTCalc(8,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_FILENAME = .w_ZTAB.GETVAR('filename')
        .oPgFrm.Page1.oPag.ZTab.Calculate()
        .oPgFrm.Page1.oPag.ZSrv.Calculate()
            .w_SERVER = .w_ZSRV.GETVAR('servername')
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZTab.Calculate()
        .oPgFrm.Page1.oPag.ZSrv.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
    endwith
  return

  proc Calculate_JTNZUZYTIR()
    with this
          * --- Aggiorna registri
          cp_mngdatabase_b(this;
              ,'Update Reg';
             )
    endwith
  endproc
  proc Calculate_ADHUPNUYKF()
    with this
          * --- Caricamento tab temporanea
          cp_mngdatabase_b(this;
              ,'Init ZTab';
             )
    endwith
  endproc
  proc Calculate_UNMIIMNHOH()
    with this
          * --- Visualizza bottone server
          cp_mngdatabase_b(this;
              ,'Server View';
             )
    endwith
  endproc
  proc Calculate_JEOHIMTIAL()
    with this
          * --- Doppio clic tabella
          cp_mngdatabase_b(this;
              ,'Mod Tab';
             )
    endwith
  endproc
  proc Calculate_RYHDQOTGXD()
    with this
          * --- Selezionati tutti
          .w_BNEWPROCS = .T.
    endwith
  endproc
  proc Calculate_PQTBQHWRIF()
    with this
          * --- Deselezionati tutti
          .w_BNEWPROCS = .F.
    endwith
  endproc
  proc Calculate_PPUEREKOBQ()
    with this
          * --- Caricamento server
          .w_MSG = cp_msg(cp_Translate(MSG_LOADING_SERVERS))
    endwith
  endproc
  proc Calculate_XCFAYEDHGJ()
    with this
          * --- Chiusura tab temporanea
          cp_mngdatabase_b(this;
              ,'Close ZTab';
             )
    endwith
  endproc
  proc Calculate_XFVYIIWMQZ()
    with this
          * --- Aggiorna selezione ZTab
          cp_mngdatabase_b(this;
              ,'SelectAfterQ';
             )
    endwith
  endproc
  proc Calculate_RIXFAQEFLY()
    with this
          * --- Salvataggio ZTAB
          cp_mngdatabase_b(this;
              ,'SelectBeforeQ';
             )
    endwith
  endproc
  proc Calculate_DCRPSJGOEJ()
    with this
          * --- Doppio clic server
          cp_mngdatabase_b(this;
              ,'Mod Server';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.visible=!this.oPgFrm.Page1.oPag.oBtn_1_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_15.visible=!this.oPgFrm.Page1.oPag.oBtn_1_15.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_16.visible=!this.oPgFrm.Page1.oPag.oBtn_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("FormLoad")
          .Calculate_JTNZUZYTIR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FormLoad")
          .Calculate_ADHUPNUYKF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_UNMIIMNHOH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZTab.Event(cEvent)
        if lower(cEvent)==lower("w_ZTab selected")
          .Calculate_JEOHIMTIAL()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZSrv.Event(cEvent)
        if lower(cEvent)==lower("w_ZTab rowcheckall")
          .Calculate_RYHDQOTGXD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZTab rowuncheckall")
          .Calculate_PQTBQHWRIF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZSrv before query")
          .Calculate_PPUEREKOBQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_XCFAYEDHGJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZTab after query")
          .Calculate_XFVYIIWMQZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZTab before query")
          .Calculate_RIXFAQEFLY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZSrv selected")
          .Calculate_DCRPSJGOEJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_mngdatabasePag1 as StdContainer
  Width  = 586
  height = 247
  stdWidth  = 586
  stdheight = 247
  resizeXpos=375
  resizeYpos=191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_6 as StdButton with uid="AJRZJGXOJI",left=406, top=8, width=72,height=19,;
    caption="Ok", nPag=1;
    , HelpContextID = 256987310;
    , Caption=MSG_OK_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ENOK)
      endwith
    endif
  endfunc

  func oBtn_1_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_SIZE)
     endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="SJUIEVLDOB",left=406, top=35, width=72,height=19,;
    caption="Annulla", nPag=1;
    , HelpContextID = 147723467;
    , Caption=MSG_CANCEL_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_SIZE)
     endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="KVGLSHVJSA",left=406, top=62, width=72,height=19,;
    caption="Azienda", nPag=1;
    , HelpContextID = 148243727;
    , Caption='A\<zienda';
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      do cp_azi with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Vartype(bexternaldatabaseadmin)='U' OR NOT .w_SIZE)
     endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="BWSVTLDXXQ",left=406, top=89, width=72,height=19,;
    caption="Server", nPag=1;
    , HelpContextID = 19364972;
    , Caption=MSG_SERVERS_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        cp_mngdatabase_b(this.Parent.oContained,"Resize")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_SIZE OR .w_NSRV)
     endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="EWGSJTXWYR",left=507, top=8, width=72,height=19,;
    caption="Ok", nPag=1;
    , HelpContextID = 256987310;
    , Caption=MSG_OK_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ENOK)
      endwith
    endif
  endfunc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SIZE)
     endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="CSHQRXJMJG",left=507, top=35, width=72,height=19,;
    caption="Annulla", nPag=1;
    , HelpContextID = 147723467;
    , Caption=MSG_CANCEL_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SIZE)
     endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="NXQJUMZYLK",left=507, top=62, width=72,height=19,;
    caption="Azienda", nPag=1;
    , HelpContextID = 148243727;
    , Caption='A\<zienda';
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      do cp_azi with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Vartype(bexternaldatabaseadmin)='U' OR .w_SIZE)
     endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="OCTHBRIKSQ",left=507, top=89, width=72,height=19,;
    caption="Server", nPag=1;
    , HelpContextID = 19364972;
    , Caption=MSG_SERVERS_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        cp_mngdatabase_b(this.Parent.oContained,"Resize")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SIZE OR .w_NSRV)
     endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="DQBNFYAJQX",left=507, top=116, width=72,height=19,;
    caption="Nuovo", nPag=1;
    , HelpContextID = 256500660;
    , Caption=MSG_NEW_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        cp_mngdatabase_b(this.Parent.oContained,"Add Server")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SIZE)
     endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="EVAZEPXERP",left=507, top=143, width=72,height=19,;
    caption="Modifica", nPag=1;
    , HelpContextID = 139361942;
    , Caption=MSG_CHANGE_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_15.Click()
      with this.Parent.oContained
        cp_mngdatabase_b(this.Parent.oContained,"Mod Server")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SIZE)
     endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="CBNHEKVLWM",left=507, top=170, width=72,height=19,;
    caption="Elimina", nPag=1;
    , HelpContextID = 120828643;
    , Caption=MSG_DELETE_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_16.Click()
      with this.Parent.oContained
        cp_mngdatabase_b(this.Parent.oContained,"Delete Server")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SIZE)
     endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="JLRGYZGWSP",left=11, top=214, width=133,height=19,;
    caption="Aggiorna Database", nPag=1;
    , HelpContextID = 41982226;
    , Caption=MSG_UPDATE_DATABASE;
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      with this.Parent.oContained
        cp_mngdatabase_b(this.Parent.oContained,"Update DB")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="ZNGRIKXIJB",left=155, top=214, width=133,height=19,;
    caption="Verifica connessioni", nPag=1;
    , HelpContextID = 219462386;
    , Caption=MSG_CHECK_LINKS;
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      with this.Parent.oContained
        cp_mngdatabase_b(this.Parent.oContained,"Ver Conn")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="HOVLCFPJUZ",left=299, top=214, width=72,height=19,;
    caption="Modifica", nPag=1;
    , HelpContextID = 139361942;
    , Caption=MSG_CHANGE_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        cp_mngdatabase_b(this.Parent.oContained,"Mod Tab")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZTab as cp_szoombox with uid="YTDKXCLMXA",left=5, top=1, width=407,height=205,;
    caption='ZTab',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.f.,bReadOnly=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",cTable="mngdatabase",bQueryOnDblClick=.t.,cZoomFile="default",bOptions=.f.,bAdvOptions=.f.,;
    cEvent = "Init,AggiornaTab",;
    nPag=1;
    , HelpContextID = 256960693


  add object ZSrv as cp_zoombox with uid="AAQWZFMXUU",left=396, top=1, width=109,height=205,;
    caption='ZSrv',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.t.,bReadOnly=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",cTable="cptsrvr",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,cZoomFile="cp_mngdatabase",bOptions=.f.,bAdvOptions=.f.,;
    cEvent = "Init, AggiornaSrv",;
    nPag=1;
    , HelpContextID = 256955302


  add object oObj_1_38 as cp_runprogram with uid="VKVQUNAUYH",left=428, top=257, width=122,height=20,;
    caption='autoexec',;
   bGlobalFont=.t.,;
    prg="cp_mngdatabase_b('AutoexecUpdate DB')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 175994268
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_mngdatabase','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
