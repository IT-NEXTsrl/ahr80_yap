* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VM_EXEC
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Interprete di maschere
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

#Define ID_VFM 9999999991

#Define FONTSTYLEREGULAR 0
#Define FONTSTYLEBOLD 1
#Define FONTSTYLEITALIC 2
#Define FONTSTYLEBOLDITALIC 3
#Define FONTSTYLEUNDERLINE 4
#Define FONTSTYLESTRIKEOUT 8 

Lparam cFile,oParentObject
If At('vm_lib',Lower(Set('proc')))=0
    Set Proc To vm_lib Additive
Endif
Local o,h,c,l,xcg,bFox26
Private i_function
i_function=''
If Vartype(i_bFox26)='L'
    bFox26=.T.
Endif
If At('cp_ppx',Lower(Set('proc')))=0
    Set Proc To cp_ppx Additive
Endif

If Type("cFile")<>'C' Or cFile='?'
    cFile=Getfile('vfm')
    cFile=Left(cFile,Len(cFile)-4)
EndIf
If Type("cFile")='C'
	cFile=Alltrim(cFile)
EndIf
If Right(cFile,4)='.vfm'
    cFile=Left(cFile,Len(cFile)-4)
Endif
*--- Zucchetti Aulla inizio - Framework monitor
* Modifica per ottimizzazione Framework
*---
If i_MonitorFramework And (At(Upper(cFile+".vfm"),Upper(i_pathFindFile))==0 Or (At("|",i_pathFindFile)!=0 And At(Upper(MSG_FILEEXIST),Upper(i_pathFindFile))==0))
    *--- Zucchetti Aulla fine - Framework monitor
    If !cp_IsPFile(cFile+'.vfm')
        Return(.Null.)
    Endif
    *--- Zucchetti Aulla inizio - Framework monitor
Endif
*--- Zucchetti Aulla Fine - Framework monitor
*** Zucchetti Aulla Inizio - se in attivazione mobile devo cercare prima la maschera per la risoluzione dello schermo
If g_MOBY='S' And i_bMobileMode
    i_nsize=Sysmetric(1)
    i_nsize=Iif(i_nsize<=800,800,1024)
    F=cp_FindNamedDefaultFile(cFile+'_s'+Alltrim(Str(i_nsize)),'vfm')
    If !Empty(F)
        cFile=F
    Endif
Endif
*** Zucchetti Aulla Fine - se in attivazione mobile devo cercare prima la maschera per la risoluzione dello schermo
c=cp_GetPFile(cFile+'.vfm')
If "OnScreen"$c or "FullScreen"$c
    *--- Zucchetti Aulla Inizio - Gestione caricamento maschera su gadget
    If Vartype(oParentObject)='O' And (Lower(oParentObject.class)="cp_vfmviewer" Or (Type('oParentObject.Parent')='O' And Lower(oParentObject.Parent.class)="cp_vfmviewer"))
        If Lower(oParentObject.class)="cp_vfmviewer" && primo caricamento
        	o=oParentObject
        	If Vartype(oParentObject.Cnt)='O'
            	o.RemoveObject(o.Cnt.Name)
        	Endif
        Else && caricamento a seguito di navigazione fra maschere
        	o=oParentObject.Parent	
        	o.RemoveObject(o.Cnt.Name)	
        Endif    	
    	o.AddObject('cnt','vm_frm_exec_cnt')
        o.Cnt.bOnScreen=.T.
    	o.AddItem(cFile)  &&Aggiungo la vfm al gestore interno al gadget
    *--- Zucchetti Aulla Fine - Gestione caricamento maschera su gadget
    Else
        *--- Gestore Vfm di sfondo
	    If Vartype(i_oVfmManager)='U'
	        Public i_oVfmManager
	        i_oVfmManager = Createobject("VfmManager")
	    Endif
        If Type("_screen.cnt")='O'
            Createobject('deferredmaskexec',cFile)
            Return(.Null.)
        Else
            i_oVfmManager.AddItem(cFile)  &&Aggiungo la vfm al gestore
            _Screen.AddObject('cnt','vm_frm_exec_cnt')
            _Screen.Cnt.bOnScreen=.T.
        Endif
        o=_Screen
    Endif
Else
    o=Createobject('vm_frm_exec')
    *--- Zucchetti Aulla Inizio - Gestione caricamento maschera su gadget
    If Vartype(oParentObject)='O' And (Lower(oParentObject.class)="cp_vfmviewer" Or (Type('oParentObject.Parent')='O' And Lower(oParentObject.Parent.class)="cp_vfmviewer"))
        If Lower(oParentObject.class)="cp_vfmviewer" && primo caricamento
        	o=oParentObject
        	If Vartype(oParentObject.Cnt)='O'
            	o.RemoveObject(o.Cnt.Name)
        	Endif
        Else && caricamento a seguito di navigazione fra maschere
        	o=oParentObject.Parent	
        	o.RemoveObject(o.Cnt.Name)	
        EndIf
	EndIf
	*--- Zucchetti Aulla Fine - Gestione caricamento maschera su gadget
	If TYPE ("oParentObject.class")='C' AND Lower(oParentObject.class)='vm_frm'
		oParentObject.oExec = o
	EndIf
    o.AddObject('cnt','vm_frm_exec_cnt')
Endif
xcg=Createobject('pxcg','load',c)
o.Cnt.Serialize(xcg)
If Vartype(oParentObject)='O' And !Isnull(oParentObject)
    If Lower(o.class)<>"cp_vfmviewer" and Lower(oParentObject.class)<>'vm_frm'
        o.Cnt.oParentObject=oParentObject
        o.Cnt.ReadVars(oParentObject)
    EndIf
Else
	If Lower(o.class)="cp_vfmviewer"
		o.Parent.oContained.NotifyEvent("w_"+Lower(o.name)+" Cnt Created")
    Endif
Endif
o.Cnt.ZOrder(1)
*** Zucchetti Aulla - gestione immagine di sfondo
If TYPE("_SCREEN.ahlogo")="O"
	_SCREEN.ahlogo.ZOrder(1)
Endif
If TYPE("_screen.bckImg")="O"
	_SCREEN.bckImg.ZOrder(1)
Endif
o.Cnt.Visible=.T.
If bFox26
    Read When showwnd26(o)
Else
    o.Show()
Endif
*--- Zucchetti Aulla Inizio - Gestione caricamento maschera su gadget
*--- salvo nel contenitore nel gadget che la maschera � cambiata e la riposiziono
If Lower(o.class)=="cp_vfmviewer"
	o.cFile=cFile
	o.CntAdjust()
Endif
*--- Zucchetti Aulla Fine - Gestione caricamento maschera su gadget
Return(o)

Func showwnd26(o)
    o.Show()
    Return(.F.)
Endfunc

Define Class vm_frm_exec As CpSysform
    Width=400
    Closable=.F.
    Caption=""
    WindowType=1
    *cVars=''
    *cModal=''
    oThis=.Null.
    cThis=''
    Icon=i_cBmpPath+i_cStdIcon
    bFox26=.F.
    Proc Show(bModal)
        If Type("i_bFox26")='L'
            This.bFox26=.T.
            This.KeyPreview=.T.
        Endif
        This.Cnt.btnok.Top=This.Height-30
        This.Cnt.btnok.Left=This.Width-110
        This.Cnt.btncan.Top=This.Height-30
        This.Cnt.btncan.Left=This.Width-55
        i_curform=This
        Local N
        N=Sys(2015)
        This.cThis=N
        Public &N
        &N=This
        DoDefault(m.bModal)
    Proc Hide()
        DoDefault()
        If !Empty(This.cThis)
        Local N
        N=This.cThis
        Release &N
        This.cThis=''
        EndIf
    Func GetVar(cName)
        Return(This.Cnt.GetVar(cName))
    Proc SetVar(cName,xValue)
        This.Cnt.SetVar(cName,xValue)
        Return
    Func IsVar(cName)
        Return(This.Cnt.IsVar(cName))
    Proc Activate()
        i_curform=This
    Proc Destroy()
        If Vartype(i_curform)='O' And !Isnull(i_curform) And i_curform.Name=This.Name
            i_curform=.Null.
        Endif
    Func HasCPEvents(i_cOp)
        Return(i_cOp=="ecpSave" Or i_cOp=="ecpZoom" Or i_cOp=="ecpQuit" Or i_cOp=="ecpZoomOnZoom")
    Proc ecpSave()
        This.Cnt.btnok.Click()
        Return
    Proc ecpQuit()
        This.Cnt.btncan.Click()
        Return
    Proc ecpZoom()
        If Lower(This.Cnt.ActiveControl.Class)='painter_txt_exec'
            This.Cnt.ActiveControl.Zoom()
        Endif
        Return
    Proc ecpZoomOnZoom()
        Return
    Proc KeyPress(i_key,i_shift)
        If i_shift=2 And i_key=23
            Do Case
                Case i_function='Save'
                    This.ecpSave()
                Case i_function='Quit'
                    This.ecpQuit()
                Case i_function='Zoom'
                    This.ecpZoom()
            Endcase
        Endif
    Procedure Init()
        This.Caption=cp_Translate(MSG_DRAW)
        DoDefault()
    Endproc
    Procedure Resize()
    	*--- Zucchetti Aulla inizio
*!*	        This.Cnt.Width = This.Width
*!*	        This.Cnt.Height = This.Height
		This.Cnt.Move(This.Cnt.Left, This.Cnt.Top, This.Width, This.Height)
*!*	        This.Cnt.bckImg.Width = This.Width
*!*	        This.Cnt.bckImg.Height = This.Height
		This.Cnt.bckImg.Move(This.Cnt.bckImg.Left, This.Cnt.bckImg.Top, This.Width, This.Height)
		*--- Zucchetti Aulla fine
        This.Cnt.bckImg.Setup(This.Cnt.cBckImage, This.Cnt.cGradient, This.Cnt.nFColor, This.Cnt.nLColor, This.Cnt.nGradientType, This.Width, This.Height)
    Endproc

    Procedure LockScreen_Assign
        Lparameters tlNewLockScreen
        If tlNewLockScreen
            This.LockScreen = .T.
        Else
            LockWindowUpdate(Thisform.HWnd)
            Thisform.LockScreen = .F.
            DoEvents Force
            LockWindowUpdate( 0 )
        Endif
Enddefine

Define Class vm_frm_exec_cnt As Container
    Width=400
    *closable=.f.
    caption='Draw'
    *windowtype=1
    cVars=''
    cModal=''
	cScreen='Form'
    *oThis=.NULL.
    *icon=i_cBmpPath+i_cStdIcon
    BorderWidth=0
    oParentObject=.Null.
    * --- Proprieta' CP
    cFile = ''
    Dimension xKey[1]
    cOldIcon=''
    oContained=.Null.
    nMouseDownX=0
    nMouseDownY=0
    bOnScreen=.F.
    nLeft = 0

    idVfm=0
    nAlignmentCnt=6
    cGradient='N'
    cBckImage=""
    nGradientType=1
    nFColor=Rgb(255,255,255)
    nLColor=Rgb(255,255,255)
    nSnapGrid=0
    nShowGrid=0
    
    bNoResize=.F. && da settare per evitare che venga eseguito il resize della maschera
    oldWidth=0    && dimensioni precedenti dello spazio su schermo disponibile
    oldHeight=0
    
    
    bCenterAlign=.F.
    xCenter=0          && coordinate del centro del contenitore per il resize di maschere a schermo pieno
    yCenter=0

    Add Object dummy As CommandButton With Visible=.T., SpecialEffect=2, Width=1, Height=1, Top=-1, Left=-1, cVarName='.-.'
    Add Object bckImg As vm_frm_bck_image With Top=0, Left=0, Visible=.F.
    Add Object lineImg As vm_frm_line_image With Top=0, Left=0, Visible=.F.

    Add Object btnok As vm_frm_btn With Caption="",Width=50,Height=25
    Add Object btncan As vm_frm_btn With Caption="",Width=50,Height=25

    Proc btnok.Click()
        If !Isnull(This.Parent.oParentObject)
            This.Parent.WriteVars()
        Endif
        If !This.Parent.bOnScreen
            Thisform.Hide()
            Thisform.oThis=.Null.
        Endif
        i_curform=.Null.
    Proc btncan.Click()
        If !This.Parent.bOnScreen
            Thisform.Hide()
            Thisform.oThis=.Null.
        Endif
        i_curform=.Null.

    Procedure Init()
        This.btnok.Caption=cp_Translate(MSG_OK_BUTTON)
        This.btncan.Caption=cp_Translate(MSG_CANCEL_BUTTON)
        DoDefault()
        This.dummy.SetFocus()
        This.bckImg.Anchor = 0
        This.bckImg.Move(0, 0, This.Width, This.Height)
        This.bckImg.Anchor = 15
    Endproc

    Proc ReadVars(oParentObject)
        Local i
        This.oParentObject=oParentObject
        For i=1 To This.ControlCount
            This.Controls(i).ReadVar(oParentObject)
        Next
    Proc WriteVars()
        Local i
        For i=1 To This.ControlCount
            This.Controls(i).WriteVar(This.oParentObject)
        Next
    Proc Serialize(xcg)
        Local i,j,N,c,l,T,w,h
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_LOAD_ONLY),.F.)
        Else
            This.cVars=''
            Local l_id
            l_id = xcg.Load('N',0)
            If l_id = ID_VFM
                *--- Nuove VFM
                l=xcg.Load('N',0)
                This.idVfm = l_id
            Else
                This.idVfm = 0
                l=l_id
            Endif
            T=xcg.Load('N',0)
            w=xcg.Load('N',300)
            h=xcg.Load('N',200)
            *-- Zucchetti Aulla - Gestione unificata per Gadget Visual Mask
            If !This.bOnScreen And (Type('This.parent')<>'O' Or Lower(This.Parent.Class) <>'cp_vfmviewer')
            	*--- Zucchetti Aulla inizio
*!*	                Thisform.Left=l
*!*	                Thisform.Top=T
*!*	                Thisform.Width=w
*!*	                Thisform.Height=h
				Thisform.Move(l,T,w,h)
				*--- Zucchetti Aulla fine
            Else
                This.Top=m.T
                This.Left=m.l
                This.nLeft=m.l
            ENDIF
            *--- Zucchetti Aulla inizio
            This.bNoResize=.T.
            This.Move(This.Left,This.Top,w,h)
            This.bNoResize=.F.
*!*	            This.Width=w
*!*	            This.Height=h
			This.bckImg.Move(0,0,w,h)
*!*	            This.bckImg.Width=w
*!*	            This.bckImg.Height=h
			*--- Zucchetti Aulla fine

            j=xcg.Load('N',0)
            For i=1 To j
                N=xcg.Load('C','')
                c=xcg.Load('C','')
                This.AddObject(N,c+'_exec')
                This.&N..Serialize(xcg)
                This.&N..Visible=.T.
                This.&N..settabindex(i)
                This.cVars=This.cVars+This.&N..cVarName+'|'
            Next
            c=xcg.Load('C','')
            This.Caption=c
            If !This.bOnScreen
                Thisform.Caption=c
            Endif
            This.cModal=xcg.Load('C','S')
            c=xcg.Load('C','Form')
			this.cScreen=c
            If Not xcg.Eoc(.T.)
                This.idVfm = ID_VFM
                *--- Nuove Propriet�
                This.nAlignmentCnt=xcg.Load('N',6)
                This.cGradient=(xcg.Load('C','N'))
                This.cBckImage=xcg.Load('C','')
                This.nGradientType=xcg.Load('N',1)
                This.nFColor=xcg.Load('N',Rgb(255,255,255))
                This.nLColor=xcg.Load('N',Rgb(255,255,255))
                IF NOT xcg.Eoc(.T.)
                    This.nSnapGrid=xcg.Load('N',0)
                    This.nShowGrid=xcg.Load('N',0)
                    IF NOT xcg.Eoc(.T.)
	                    *-- Campi della grandezza degli step, non servono in esecuzione
	                    nGridHPoint=xcg.Load('N',0)
	                    nGridVPoint=xcg.Load('N',0)
	                    xcg.Eoc()
                    EndIf
                endif
            Endif

            *--- Immagine di sfondo
            This.bckImg.Setup(This.cBckImage, This.cGradient, This.nFColor, This.nLColor, This.nGradientType, This.Width, This.Height)
            *--- Line connector
            This.lineImg.DrawLines()

            If This.cModal='N' And !This.bOnScreen And (Type('This.parent')<>'O' Or Lower(This.Parent.Class)<>'cp_vfmviewer')
                Thisform.WindowType=0
                Thisform.oThis=This
            Endif
            If This.bOnScreen Or (Type('This.parent')='O' And Lower(This.Parent.Class)='cp_vfmviewer')
                This.btnok.Visible=.F.
                This.btncan.Visible=.F.
                *--- Dimensione contenitore e Allineamento
                This.OnResize()
            Endif
        Endif

    Procedure Resize()
    	If Not This.bNoResize
    		Local nLeft, nTop, nWidth, nHeight, l_xCenter, l_yCenter
    		
    		freeScreenSpace(@nLeft, @nTop, @nWidth, @nHeight)
    		*--se � uma maschera di sfondo o a schermo pieno e lo spazio utile � cambiato
    		If (This.cScreen<>'Form' AND nWidth<>This.oldWidth OR nHeight<>This.oldHeight)
    		
    			*--- maschera a schermo pieno, la ridimensiono
    			IF This.cScreen='FullScreen' 
    				This.Anchor = 0
    				This.Move(nLeft, nTop, nWidth, nHeight)
		    		This.bckImg.Setup(This.cBckImage, This.cGradient, This.nFColor, This.nLColor, This.nGradientType, nWidth, nHeight)
		    		This.Anchor = 15
		    		
		    		*--- Ancoraggio dei controlli al centro
    				If This.bCenterAlign
    					Local dX, dY
    					l_xCenter = Bitrshift(nWidth,1)
    					l_yCenter = Bitrshift(nHeight,1)
    					dX = l_xCenter - This.xCenter
    					dY = l_yCenter - This.yCenter
    					For Each cCtrl In This.Controls
    						If Lower(cCtrl.Name) <> 'bckimg'
    							cCtrl.Move(cCtrl.Left + dX, cCtrl.Top + dY)
    						Endif
    					Endfor
    					This.xCenter = l_xCenter
    					This.yCenter = l_yCenter
    				ENDIF
    			ENDIF
    			
    			*--- maschera di sfondo non a schermo pieno, non va ridimensionata ma solo fatta slittare (se non � allineata a Dx)
    			IF This.cScreen='OnScreen' And This.nAlignmentCnt<>3 And (Type('i_oDeskMenu')='O' And Not Isnull(i_oDeskMenu))
    				This.Move(This.nLeft+nLeft)
    			ENDIF

    			*--- aggiorno le dimensioni dello spazio utile
    			This.oldWidth = nWidth
    			This.oldHeight = nHeight
    		Endif
    	ENDIF
       
    Procedure OnResize()
    		LOCAL nLeft, nTop, nWidth, nHeight
	    	freeScreenSpace(@nLeft, @nTop, @nWidth, @nHeight)
	    	This.oldWidth = This.Width
	    	This.oldHeight = This.Height
    		If This.cScreen='FullScreen'
    			This.SetAll('Anchor',0)
    			This.Anchor = 0
    			This.bNoResize=.T.
    		    *--- Allineamento
    		    *--- per mantenere i controlli nella loro posizione originale (distanza dal bordo di sinistra in caso di ancoraggio
    		    *--- in alto o in basso oppure distanza dal bordo in alto in caso di ancoraggio a sinistra o a destra)
    		    *--- ancoro prima i controlli nel lato opposto a quello da mantenere, allargo la maschera della dimensione necessaria
    		    *--- per farli tornare nella posizione di partenza, e poi risetto l'ancoraggio richiesto
    			Do Case
                    Case This.nAlignmentCnt=1	&&Center
                        This.bCenterAlign = .T.
                        This.xCenter = INT(This.Width/2)
                        This.yCenter = INT(This.Height/2)
                    Case This.nAlignmentCnt=2	&&Sx
                    	This.SetAll('Anchor',4)
                    	This.Height = This.Height + This.Top
                    	This.SetAll('Anchor',0)
                        This.SetAll('Anchor',2)
                    Case This.nAlignmentCnt=3	&&Dx
                    	This.SetAll('Anchor',4)
                    	This.Height = This.Height + This.Top
                    	This.SetAll('Anchor',0)
                        This.SetAll('Anchor',8)
                    Case This.nAlignmentCnt=4	&&Top
                    	This.SetAll('Anchor',8)
                    	This.Width = This.Width + This.Left
                    	This.SetAll('Anchor',0)
                        This.SetAll('Anchor',1)
                    Case This.nAlignmentCnt=5	&&Bottom
                    	This.SetAll('Anchor',8)
                    	This.Width = This.Width + This.Left
                    	This.SetAll('Anchor',0)
                        This.SetAll('Anchor',4)  
                    Otherwise            		&&Nessuno
                    	This.SetAll('Anchor',12)
                    	This.Height = This.Height + This.Top
                    	This.Width = This.Width + This.Left
                    	This.SetAll('Anchor',0)
                        This.SetAll('Anchor',3)
                Endcase
                This.bNoResize=.F.
                *--- Size contenitore a schermo pieno
                *--- Disancoro l'immagine prima di allargare il contenitore, ridimensiono entrambi e infine la riancoro
                *--- per averne una visualizzazione corretta
                This.bckImg.Anchor = 0 
                This.Move(nLeft, nTop, nWidth, nHeight)
                This.bckImg.Move(0, 0, nWidth, nHeight)
                This.bckImg.Anchor = 15
                This.Anchor = 15
            Else
				Do Case
                    Case This.nAlignmentCnt=1	&&Center
                        This.Left=Int((_Screen.Width-This.Width)/2)
                        This.Top=Int((_Screen.Height-This.Height)/2)
                        This.Anchor = 768
                    Case This.nAlignmentCnt=2	&&Sx
                        This.Left=0
                        This.Anchor = 2
                    Case This.nAlignmentCnt=3	&&Dx
                        This.Left=_Screen.Width-This.Width
                        This.Anchor = 8
                    Case This.nAlignmentCnt=4	&&Top
                        This.Top=0
                        This.Anchor = 1
                    Case This.nAlignmentCnt=5	&&Bottom
                        This.Top=_Screen.Height-This.Height
                        This.Anchor = 4
                Endcase
            Endif
			*--- Nascondo il logo adhoc se fullscreen
			IF Vartype(_Screen.BCKIMG)<>"U" AND Vartype(_Screen.ahlogo)<>"U"
				_screen.BCKIMG.Visible=!(This.cScreen='FullScreen')
				_screen.ahlogo.Visible=_screen.BCKIMG.Visible
			ENDIF
			
			*--- Controllo se il Desk Menu � visualizzato			
						
    Func GetCtrl(cName)
        Local i
        cName=Lower(cName)
        For i=1 To This.ControlCount
            If Lower(Trim(This.Controls(i).cVarName))==cName
                Return(i)
            Endif
        Next
        Return(.Null.)
    Func GetVar(cName)
        Local i, cVar
        cVar=Lower(cName)
        For i=1 To This.ControlCount
            If Lower(Trim(This.Controls(i).cVarName))==cVar
                Return(This.Controls(i).GetValue())
            Endif
        Next
        *  --- Possono essere utilizzate anche variabili di memoria (es.variabili pubbliche)
        If Type(cName) <> 'U'
            Return &cName
        Endif
        Return('')
    Proc SetVar(cName,xValue)
        Local i,objname
        cName=Lower(cName)
        For i=1 To This.ControlCount
        	If Lower(Trim(This.Controls(i).cVarName))==cName
        	    *Zucchetti Aulla Inizio - errore con check e radio box come key fixed
        		If Lower(This.Controls(i).Class)='painter_check_exec' OR Lower(This.Controls(i).Class)='painter_radio_exec' OR Lower(This.Controls(i).Class)='painter_combo_exec'
        			objname=This.Controls(i).Name
        			This.&objname..setvalue(xValue)
        	    *Zucchetti Aulla fine - errore con check e radio box come key fixed
        		Else
        			This.Controls(i).Value=xValue
        		Endif
        		Return
        	Endif
        Next
        Return
    Func IsVar(cName)
        Return(cName+'|'$This.cVars)
    Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            * --- per ritardare un po il drag e drop
            This.nMouseDownX=nXCoord
            This.nMouseDownY=nYCoord
        Endif
	Proc MouseUp(nButton, nShift, nXCoord, nYCoord)
		If nButton=1 and nXCoord>=_screen.width-30 and nYCoord>=_screen.Height-30
			SetToolBarVisible()
		Endif
	Endproc
	Proc bckImg.MouseUp(nButton, nShift, nXCoord, nYCoord)
		this.Parent.MouseUp(nButton, nShift, nXCoord, nYCoord)
	Endproc	
    Proc MouseMove(nButton, nShift, nXCoord, nYCoord)
        Local oldarea
        If !Isnull(This.oContained) And This.oContained.bLoaded And nButton=1 And (Abs(nXCoord-This.nMouseDownX)+Abs(nYCoord-This.nMouseDownY))>8
            This.cFile=This.oContained.cFile
            oldarea=Select()
            Select (This.oContained.cKeySet)
            Scatter To This.xKey
            Select (oldarea)
            This.DragIcon=i_cBmpPath+"dragmove.cur"
            This.Drag(1)
        Endif
    Proc DragDrop(oSource,nXCoord,nYCoord,nState)
        If Lower(oSource.Name)=='postitedt' And !Isnull(This.oContained)
            This.oContained.AddWarning(oSource.Parent)
        Endif
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        Do Case
            Case nState=0   && Enter
                This.cOldIcon=oSource.DragIcon
                If Lower(oSource.Name)=='postitedt' And !Isnull(This.oContained)
                    oSource.DragIcon=i_cBmpPath+"cross01.cur"
                Endif
            Case nState=1   && Leave
                oSource.DragIcon=This.cOldIcon
        Endcase
Enddefine

Define Class vm_frm_btn As CommandButton
    cVarName='.-.'
    Proc ReadVar(o)
        Return
    Proc WriteVar(o)
        Return
Enddefine

Define Class painter_lbl_exec As Label
    cVarName=''
    cAlign=''
    BackStyle=0
    Proc Serialize(xcg)
        If xcg.bIsStoring
        Else
			*--- Zucchetti Aulla inizio
*!*	            This.Left=xcg.Load('N',0)
*!*	            This.Top=xcg.Load('N',0)
*!*	            This.Width=xcg.Load('N',0)
*!*	            This.Height=xcg.Load('N',0)
			This.Move(xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0))
			*--- Zucchetti Aulla fine
            xcg.Eoc()
            This.cVarName=xcg.Load('C','NoName')
			If left(This.cVarName,1)="#" and right(This.cVarName,1)="#"
				local cVarName
				cVarName=substr(This.cVarName, 2, len(This.cVarName)-2)
				This.cVarName=&cVarName
			Endif
            This.cAlign=xcg.Load('C','')
            This.Caption=This.cVarName
            This.Alignment=Iif(This.cAlign='R',1,Iif(This.cAlign='C',2,0))
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.ForeColor = xcg.Load('N',0)
                This.FontBold = xcg.Load('L',.F.)
                This.FontItalic = xcg.Load('L',.F.)
                This.FontUnderline = xcg.Load('L',.F.)
                This.FontStrikethru = xcg.Load('L',.F.)
                l_GlbFont= xcg.Load('L',.F.)
                xcg.Eoc()
                If l_GlbFont
                    This.FontName = SetFontName('L', This.FontName, l_GlbFont)
                    This.FontSize = SetFontSize('L', This.FontSize, l_GlbFont)
                    This.FontItalic = SetFontItalic('L', This.FontItalic, l_GlbFont)
                    This.FontBold = SetFontBold('L', This.FontBold, l_GlbFont)
                    This.FontUnderline = .F.
                    This.FontStrikethru = .F.
                Endif
            Endif
        Endif
    Proc settabindex(N)
        This.TabIndex=N
        Return
    Proc ReadVar(o)
        Return
    Proc WriteVar(o)
        Return
Enddefine

Define Class painter_txt_exec As TextBox
    cVarName=''
    cType=''
    nLen=0
    nDec=0
    cPict=''
    cStato=''
    cZFill=''
    cArch=''
    cK1=''
    cK2=''
    cK3=''
    cKa=''
    cVk1=''
    cVk2=''
    cVk3=''
    cRf1=''
    cRv1=''
    bDontReportError=.F.
    oObserver=.Null.
    DisabledBackColor=Rgb(255,255,255)
    Margin=1
    Format='K'
    nSeconds=0
    Proc Serialize(xcg)
        If xcg.bIsStoring
        Else
            *--- Zucchetti Aulla inizio
*!*	            This.Left=xcg.Load('N',0)
*!*	            This.Top=xcg.Load('N',0)
*!*	            This.Width=xcg.Load('N',0)
*!*	            This.Height=xcg.Load('N',0)
			This.Move(xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0))
			*--- Zucchetti Aulla fine
            xcg.Eoc()
            This.cVarName=xcg.Load('C','NoName')
            This.cType=xcg.Load('C','Numerico')
            This.nLen=xcg.Load('N',0)
            This.nDec=xcg.Load('N',0)
            This.cPict=xcg.Load('C','')
            This.cStato=xcg.Load('C','Edit')
            This.cZFill=xcg.Load('C','N')
            This.cArch=xcg.Load('C','')
            This.cK1=xcg.Load('C','')
            This.cK2=xcg.Load('C','')
            This.cK3=xcg.Load('C','')
            This.cKa=xcg.Load('C','')
            This.cVk1=xcg.Load('C','')
            This.cVk2=xcg.Load('C','')
            This.cVk3=xcg.Load('C','')
            This.cRf1=xcg.Load('C','')
            This.cRv1=xcg.Load('C','')
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.ForeColor = xcg.Load('N',0)
                This.FontBold = xcg.Load('L',.F.)
                This.FontItalic = xcg.Load('L',.F.)
                This.FontUnderline = xcg.Load('L',.F.)
                This.FontStrikethru = xcg.Load('L',.F.)
                l_GlbFont= xcg.Load('L',.F.)
                xcg.Eoc()
                If l_GlbFont
                    This.FontName = SetFontName('T', This.FontName, l_GlbFont)
                    This.FontSize = SetFontSize('T', This.FontSize, l_GlbFont)
                    This.FontItalic = SetFontItalic('T', This.FontItalic, l_GlbFont)
                    This.FontBold = SetFontBold('T', This.FontBold, l_GlbFont)
                    This.FontUnderline = .F.
                    This.FontStrikethru = .F.
                Endif
            Endif
        Endif
        * --- ora setta le caratteristiche del control
        This.InputMask=This.cPict
        If Empty(This.cPict) And This.cType='C'
            This.InputMask=Repl('X',This.nLen)
        Endif
        If This.cStato<>'Edit'
            This.Enabled=.F.
        Endif
        Do Case
            Case This.cType='C'
                This.Value=Space(This.nLen)
            Case This.cType='N'
                This.Value=0
            Case This.cType='D'
                This.Value=Ctod("  -  -  ")
        Endcase
    Func GetValue()
        Return(Iif(This.cType='C',Trim(This.Value),This.Value))
    Proc SetValue(xValue)
        This.Value=xValue
        This.bDontReportError=.T.
        This.Valid()
    Proc ReadVar(oObj)
        * --- legge dall' oggetto che ha chiamato la maschera
        If oObj.IsVar(This.cVarName)
            This.SetValue(oObj.GetVar(This.cVarName))
        Endif
    Proc WriteVar(oObj)
        If oObj.IsVar(This.cVarName)
            oObj.SetVar(This.cVarName,This.GetValue())
        Endif
        Return
    Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            If Seconds()-0.3<This.nSeconds
                Nodefault
                This.Zoom()
            Endif
            This.nSeconds=Seconds()
        Endif
        Return
    Func Valid()
        If This.cZFill='S' And This.cType='C' And Left(This.Value,1)$"123456789"
            This.Value=Right(Repl('0',This.nLen)+Alltrim(This.Value),This.nLen)
        Endif
        If !Empty(This.cArch)
            Local i_cCmd,i_bODBC,i_xVal,N,i_va,i_NT,i_PhName,i_nConn
            If !Thisform.bFox26
                i_bODBC=i_ServerConn[1,2]<>0
                i_NT=cp_TablePropScan(This.cArch)
                i_PhName=This.cArch
                i_nConn=i_ServerConn[1,2]
                cp_OpenTable( This.cArch )
                If i_NT<>0
                    i_PhName=cp_SetAzi(i_TableProp[i_NT,2])
                    i_nConn=i_TableProp[i_NT,3]
                Endif
            Else
                i_bODBC=.F.
                i_nConn=0
                i_PhName=This.cArch
                Do cplu_opf With (This.cArch),''
            Endif
            i_cCmd='select '+This.cKa+Iif(Empty(This.cRf1),'',','+This.cRf1)+' from '+i_PhName
            i_cCmd=i_cCmd+' where '+This.cKa+'='
            i_va=This.Parent.GetVar(This.cVarName)
            If Empty(i_va)
                This.bDontReportError=.T.
                i_va=' '
            Endif
            i_cCmd=i_cCmd+Iif(i_bODBC,cp_ToStrODBC(i_va),cp_ToStr(i_va))
            If !Empty(This.cK1)
                i_cCmd=i_cCmd+' and '+This.cK1+'='
                i_cCmd=i_cCmd+Iif(i_bODBC,cp_ToStrODBC(This.Parent.GetVar(This.cVk1)),cp_ToStr(This.Parent.GetVar(This.cVk1)))
            Endif
            If !Empty(This.cK2)
                i_cCmd=i_cCmd+' and '+This.cK2+'='
                i_cCmd=i_cCmd+Iif(i_bODBC,cp_ToStrODBC(This.Parent.GetVar(This.cVk2)),cp_ToStr(This.Parent.GetVar(This.cVk2)))
            Endif
            If !Empty(This.cK3)
                i_cCmd=i_cCmd+' and '+This.cK3+'='
                i_cCmd=i_cCmd+Iif(i_bODBC,cp_ToStrODBC(This.Parent.GetVar(This.cVk3)),cp_ToStr(This.Parent.GetVar(This.cVk3)))
            Endif
            If i_bODBC
                N=cp_SQL(i_nConn,i_cCmd,'__VMTMP__')
            Else
                &i_cCmd nofilter Into Cursor __VMTMP__
            Endif
            If Used('__VMTMP__')
                If Eof()
                    If !This.bDontReportError
                        CP_MSG(cp_Translate(MSG_VALUE_MISSING),.F.)
                    Endif
                    This.Value=cp_NullValue(This.Value)
                Endif
                If !Empty(This.cRf1)
                    N=This.cRf1
                    This.Parent.SetVar(This.cRv1,__VMTMP__.&N)
                Endif
                Use
            Else
                If !This.bDontReportError
                    CP_MSG(cp_Translate(MSG_VALUE_MISSING),.F.)
                Endif
                This.Value=cp_NullValue(This.Value)
            Endif
        Endif
        Local i,o
        For i=1 To This.Parent.ControlCount
            o=This.Parent.Controls(i)
            If Lower(o.Class)='painter_txt_exec'
                If o.cVk1==This.cVarName Or o.cVk2==This.cVarName Or o.cVk3==This.cVarName
                    o.bDontReportError=.T.
                    o.Valid()
                Endif
            Endif
        Next
        If !Empty(This.cArch)
            If Thisform.bFox26
                Do cplu_clf With (This.cArch)
            Else
                cp_CloseTable( This.cArch )
            Endif
        Endif
        This.bDontReportError=.F.
        Return(.T.)
    Proc GotFocus()
        If !Empty(This.cArch)
            oCpToolBar.b9.Enabled=.T.
        Endif
    Proc LostFocus()
        If !Isnul(This.oObserver)
            *nodefault
            * --- segnala all' osservatore che il valore e' cambiato
            If This.oObserver.ObservedChanged(This.cVarName)
                Nodefault
            Endif
        Endif
        oCpToolBar.b9.Enabled=.F.
    Proc settabindex(N)
        This.TabIndex=N
    Proc Zoom()
        If !Empty(This.cArch)
            Local k,s,sf,T,i_bODBC
            If Thisform.bFox26
                i_bODBC=.F.
            Else
                i_bODBC=i_ServerConn[1,2]<>0
            Endif
            k=''
            s=''
            sf=''
            If !Empty(This.cK1)
                k=This.cK1
                T=Thisform.GetVar(This.cVk1)
                If !Empty(T)
                    s=s+Iif(Empty(s),'',' and ')+This.cK1+'='+Iif(i_bODBC,cp_ToStr(T),cp_ToStrODBC(T))
                Endif
                If Thisform.bFox26
                    T=This.cK1
                    sf=T
                Endif
            Endif
            If !Empty(This.cK2)
                k=k+Iif(Empty(k),'',',')+This.cK2
                T=Thisform.GetVar(This.cVk2)
                If !Empty(T)
                    s=s+Iif(Empty(s),'',' and ')+This.cK2+'='+Iif(i_bODBC,cp_ToStr(T),cp_ToStrODBC(T))
                Endif
                If Thisform.bFox26
                    T=This.cK2
                    sf=sf+'+'+T
                Endif
            Endif
            If !Empty(This.cK3)
                k=k+Iif(Empty(k),'',',')+This.cK3
                T=Thisform.GetVar(This.cVk3)
                If !Empty(T)
                    s=s+Iif(Empty(s),'',' and ')+This.cK3+'='+Iif(i_bODBC,cp_ToStr(T),cp_ToStrODBC(T))
                Endif
                If Thisform.bFox26
                    T=This.cK3
                    sf=sf+'+'+T
                Endif
            Endif
            k=k+Iif(Empty(k),'',',')+This.cKa
            If !Thisform.bFox26
                If Empty(s)
                    cp_zoom(This.cArch,'*',k,Thisform.cThis+'.cnt.'+This.Name)
                Else
                    cp_zoom(This.cArch,'*',k,Thisform.cThis+'.cnt.'+This.Name,s)
                Endif
            Else
                T=This.cKa
                Private w_&T,i_zoomrec
                w_&T=''
                sf=Iif(!Empty(sf),sf+'+','')+T
                i_zoomrec=0
                cpvfpzom(This.cArch,'','','vutk',sf)
                If i_zoomrec <> 0
                    Select (This.cArch)
                    w_&T = &T
                    If !Empty(This.cK1)
                        T=This.cK1
                        Thisform.SetVar(This.cVk1,&T)
                    Endif
                    If !Empty(This.cK2)
                        T=This.cK2
                        Thisform.SetVar(This.cVk2,&T)
                    Endif
                    If !Empty(This.cK3)
                        T=This.cK3
                        Thisform.SetVar(This.cVk3,&T)
                    Endif
                    T=This.cKa
                    This.Value=&T
                    This.Valid()
                Endif
            Endif
        Endif
    Proc ecpDrop(oSource)
        Local i
        i=1
        If !Empty(This.cK1)
            Thisform.SetVar(This.cVk1,oSource.xKey[i])
            i=i+1
        Endif
        If !Empty(This.cK2)
            Thisform.SetVar(This.cVk2,oSource.xKey[i])
            i=i+1
        Endif
        If !Empty(This.cK3)
            Thisform.SetVar(This.cVk2,oSource.xKey[i])
            i=i+1
        Endif
        This.Value=oSource.xKey[i]
        This.Valid()
Enddefine

Define Class painter_check_exec As Checkbox
    cVarName=''
    cType=''
    nLen=0
    nDec=0
    cPic=''
    cStato=''
    cTitle='check'
    cSelValue=''
    cUnselValue=''
    Proc Serialize(xcg)
        If xcg.bIsStoring
        Else
            *--- Zucchetti Aulla inizio
*!*	            This.Left=xcg.Load('N',0)
*!*	            This.Top=xcg.Load('N',0)
*!*	            This.Width=xcg.Load('N',0)
*!*	            This.Height=xcg.Load('N',0)
			This.Move(xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0))
			*--- Zucchetti Aulla fine
            xcg.Eoc()
            This.cVarName=xcg.Load('C','NoName')
            This.cType=xcg.Load('C','Numerico')
            This.nLen=xcg.Load('N',0)
            This.nDec=xcg.Load('N',0)
            This.cPic=xcg.Load('C','')
            This.cStato=xcg.Load('C','Edit')
            This.cTitle=xcg.Load('C','check')
            This.cSelValue=xcg.Load('C','')
            This.cUnselValue=xcg.Load('C','')
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.ForeColor = xcg.Load('N',0)
                This.FontBold = xcg.Load('L',.F.)
                This.FontItalic = xcg.Load('L',.F.)
                This.FontUnderline = xcg.Load('L',.F.)
                This.FontStrikethru = xcg.Load('L',.F.)
                l_GlbFont= xcg.Load('L',.F.)
                xcg.Eoc()
                If l_GlbFont
                    This.FontName = SetFontName('X', This.FontName, l_GlbFont)
                    This.FontSize = SetFontSize('X', This.FontSize, l_GlbFont)
                    This.FontItalic = SetFontItalic('X', This.FontItalic, l_GlbFont)
                    This.FontBold = SetFontBold('X', This.FontBold, l_GlbFont)
                    This.FontUnderline = .F.
                    This.FontStrikethru = .F.
                Endif
            Endif

            If This.cStato<>'Edit'
                This.Enabled=.F.
            Endif
            This.Caption=This.cTitle
        Endif
    Func GetValue()  
        Local N
        N=Iif(Empty(This.Value),This.cUnselValue,This.cSelValue)
        Return(&N)
    Proc settabindex(N)
        This.TabIndex=N      
    Proc SetValue(xValue)
        Local N
        N=This.cSelValue
        If Type("xValue")=Type(N)
            This.Value=Iif(xValue=&N,1,0)
        Endif
    Proc ReadVar(oObj)
        If oObj.IsVar(This.cVarName)
            This.SetValue(oObj.GetVar(This.cVarName))
        Endif
    Proc WriteVar(oObj)
        If oObj.IsVar(This.cVarName)
            oObj.SetVar(This.cVarName,This.GetValue())
        Endif
Enddefine

Define Class painter_radio_exec As OptionGroup
    BorderStyle=0
    cVarName=''
    cType=''
    nLen=0
    nDec=0
    cPict=''
    cStato=''
    cVal1=''
    cVal2=''
    cVal3=''
    cVal4=''
    cVal5=''
    cVal6=''
    cVal7=''
    cVal8=''
    cVal9=''
    cVal10=''
    cLbl1=''
    cLbl2=''
    cLbl3=''
    cLbl4=''
    cLbl5=''
    cLbl6=''
    cLbl7=''
    cLbl8=''
    cLbl9=''
    cLbl10=''

    FntName = ''
    FntSize = 10
    FntColor = 0
    FntBold = .F.
    FntItalic = .F.
    FntUnderline = .F.
    FntStrikeThru = .F.

    Proc Serialize(xcg)
    	LOCAL TestMacro
        If xcg.bIsStoring
        Else
            *--- Zucchetti Aulla inizio
*!*	            This.Left=xcg.Load('N',0)
*!*	            This.Top=xcg.Load('N',0)
*!*	            This.Width=xcg.Load('N',0)
*!*	            This.Height=xcg.Load('N',0)
			This.Move(xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0))
			*--- Zucchetti Aulla fine
            xcg.Eoc()
            This.cVarName=xcg.Load('C','NoName')
            This.cType=xcg.Load('C','Numerico')
            This.nLen=xcg.Load('N',0)
            This.nDec=xcg.Load('N',0)
            This.cPict=xcg.Load('C','')
            This.cStato=xcg.Load('C','Edit')
            This.cVal1=xcg.Load('C','')
            This.cVal2=xcg.Load('C','')
            This.cVal3=xcg.Load('C','')
            This.cVal4=xcg.Load('C','')
            This.cVal5=xcg.Load('C','')
            This.cVal6=xcg.Load('C','')
            This.cVal7=xcg.Load('C','')
            This.cVal8=xcg.Load('C','')
            This.cVal9=xcg.Load('C','')
            This.cVal10=xcg.Load('C','')
            This.cLbl1=xcg.Load('C','')
            This.cLbl2=xcg.Load('C','')
            This.cLbl3=xcg.Load('C','')
            This.cLbl4=xcg.Load('C','')
            This.cLbl5=xcg.Load('C','')
            This.cLbl6=xcg.Load('C','')
            This.cLbl7=xcg.Load('C','')
            This.cLbl8=xcg.Load('C','')
            This.cLbl9=xcg.Load('C','')
            This.cLbl10=xcg.Load('C','')
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FntName = xcg.Load('C','Arial')
                This.FntSize = Int(xcg.Load('N',9))
                This.FntColor = xcg.Load('N',0)
                This.FntBold = xcg.Load('L',.F.)
                This.FntItalic = xcg.Load('L',.F.)
                This.FntUnderline = xcg.Load('L',.F.)
                This.FntStrikeThru = xcg.Load('L',.F.)
                l_GlbFont= xcg.Load('L',.F.)
                xcg.Eoc()
                If l_GlbFont
                    This.FntName = SetFontName('R', This.FntName , l_GlbFont)
                    This.FntSize = SetFontSize('R', This.FntSize , l_GlbFont)
                    This.FntItalic = SetFontItalic('R', This.FntItalic , l_GlbFont)
                    This.FntBold = SetFontBold('R', This.FntBold , l_GlbFont)
                    This.FntUnderline = .F.
                    This.FntStrikeThru = .F.
                Endif
            Endif
        Endif
        * --- ora setta le caratteristiche del control
        If This.cStato<>'Edit'
            This.Enabled=.F.
        Endif
        Do Case
            Case This.cType='C'
                This.Value=Space(This.nLen)
            Case This.cType='N'
                This.Value=0
            Case This.cType='D'
                This.Value=Ctod("  -  -  ")
        Endcase
        Local i,N,l,j
        l=0
        For i=1 To 10
            N=Alltrim(Str(i))
            TestMacro=This.cLbl&N
            If !Empty(TestMacro)
                l=l+1
            Endif
        Next
        If l>0
            This.ButtonCount=l
            j=0
            For i=1 To 10
                N=Alltrim(Str(i))
                TestMacro=This.cLbl&N
                If !Empty(TestMacro)
                    j=j+1
                    This.Buttons(j).Caption=This.cLbl&N
                Endif
            Next
            This.SetAll("width",This.Width-33)
            *--- Font
            If ! Empty(This.FntName)
                This.SetAll("FontName", This.FntName)
                This.SetAll("FontSize", This.FntSize)
                This.SetAll("ForeColor", This.FntColor)
                This.SetAll("FontBold", This.FntBold)
                This.SetAll("FontItalic", This.FntItalic)
                This.SetAll("FontUnderline", This.FntUnderline)
                This.SetAll("FontStrikeThru", This.FntStrikeThru)
            Endif
        Endif
    Func GetValue()
        Local i,N,r
        LOCAL TestMacro
        r=This.Value
        For i=1 To 10
            N=Alltrim(Str(i))
            TestMacro=This.cLbl&N
            If This.Value==TestMacro
                N=This.cVal&N
                *--- Per non avere errore con radio in .vfm chiamate da .vqr nel caso in cui
                *--- non sia stato selezionato nessun valore, dato che non � possibile indicare un valore
                *--- di default nel radio
                If Not Empty(N)
                    r=&N
                Else
                    r=' '
                Endif
            Endif
        Next
        Return(r)
    Proc settabindex(N)
        This.TabIndex=N
    Proc SetValue(xValue)
        Local i,v,N
        LOCAL TestMacro
        For i=1 To 10
            N=Alltrim(Str(i))
            v=Trim(This.cVal&N)
            If !Empty(v)
            TestMacro=&v
                If xValue=TestMacro
                    This.Value=This.cLbl&N
                    Return
                Endif
            Endif
        Next
    Proc ReadVar(oObj)
        If oObj.IsVar(This.cVarName)
            This.SetValue(oObj.GetVar(This.cVarName))
        Endif
    Proc WriteVar(oObj)
        If oObj.IsVar(This.cVarName)
            oObj.SetVar(This.cVarName,This.GetValue())
        Endif
Enddefine

Define Class painter_combo_exec As ComboBox
    cVarName=''
    cType=''
    nLen=0
    nDec=0
    cPict=''
    cStato=''
    cVal1=''
    cVal2=''
    cVal3=''
    cVal4=''
    cVal5=''
    cVal6=''
    cVal7=''
    cVal8=''
    cVal9=''
    cVal10=''
    cLbl1=''
    cLbl2=''
    cLbl3=''
    cLbl4=''
    cLbl5=''
    cLbl6=''
    cLbl7=''
    cLbl8=''
    cLbl9=''
    cLbl10=''
    Proc Serialize(xcg)
        LOCAL TestMacro
        If xcg.bIsStoring
        Else
           	*--- Zucchetti Aulla inizio
*!*	            This.Left=xcg.Load('N',0)
*!*	            This.Top=xcg.Load('N',0)
*!*	            This.Width=xcg.Load('N',0)
*!*	            This.Height=xcg.Load('N',0)
			This.Move(xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0))
			*--- Zucchetti Aulla fine
            xcg.Eoc()
            This.cVarName=xcg.Load('C','NoName')
            This.cType=xcg.Load('C','Numerico')
            This.nLen=xcg.Load('N',0)
            This.nDec=xcg.Load('N',0)
            This.cPict=xcg.Load('C','')
            This.cStato=xcg.Load('C','Edit')
            This.cVal1=xcg.Load('C','')
            This.cVal2=xcg.Load('C','')
            This.cVal3=xcg.Load('C','')
            This.cVal4=xcg.Load('C','')
            This.cVal5=xcg.Load('C','')
            This.cVal6=xcg.Load('C','')
            This.cVal7=xcg.Load('C','')
            This.cVal8=xcg.Load('C','')
            This.cVal9=xcg.Load('C','')
            This.cVal10=xcg.Load('C','')
            This.cLbl1=xcg.Load('C','')
            This.cLbl2=xcg.Load('C','')
            This.cLbl3=xcg.Load('C','')
            This.cLbl4=xcg.Load('C','')
            This.cLbl5=xcg.Load('C','')
            This.cLbl6=xcg.Load('C','')
            This.cLbl7=xcg.Load('C','')
            This.cLbl8=xcg.Load('C','')
            This.cLbl9=xcg.Load('C','')
            This.cLbl10=xcg.Load('C','')
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.ForeColor = xcg.Load('N',0)
                This.FontBold = xcg.Load('L',.F.)
                This.FontItalic = xcg.Load('L',.F.)
                This.FontUnderline = xcg.Load('L',.F.)
                This.FontStrikethru = xcg.Load('L',.F.)
                l_GlbFont= xcg.Load('L',.F.)
                xcg.Eoc()
                If l_GlbFont
                    This.FontName = SetFontName('C', This.FontName, l_GlbFont)
                    This.FontSize = SetFontSize('C', This.FontSize, l_GlbFont)
                    This.FontItalic = SetFontItalic('C', This.FontItalic, l_GlbFont)
                    This.FontBold = SetFontBold('C', This.FontBold, l_GlbFont)
                    This.FontUnderline = .F.
                    This.FontStrikethru = .F.
                Endif
            Endif
        Endif
        * --- ora setta le caratteristiche del control
        This.InputMask=This.cPict
        If This.cStato<>'Edit'
            This.Enabled=.F.
        Endif
        Do Case
            Case This.cType='C'
                This.Value=Space(This.nLen)
            Case This.cType='N'
                This.Value=0
            Case This.cType='D'
                This.Value=Ctod("  -  -  ")
        Endcase
        Local i,N
        For i=1 To 10
            N=Alltrim(Str(i))
            TestMacro=This.cLbl&N
            If !Empty(TestMacro)
                This.AddItem(This.cLbl&N)
            Endif
        Next
    Func GetValue()
        Local i,N,r
        LOCAL TestMacro
        r=This.Value
        For i=1 To 10
            N=Alltrim(Str(i))
            TestMacro=This.cLbl&N
            If This.Value==TestMacro
                N=This.cVal&N
                r=&N
            Endif
        Next
        Return(r)
    Proc settabindex(N)
        This.TabIndex=N
    Proc SetValue(xValue)
        Local i,v,N
        LOCAL TestMacro
        For i=1 To 10
            N=Alltrim(Str(i))
            v=Trim(This.cVal&N)
            If !Empty(v)
             TestMacro=&v
                If xValue=TestMacro
                    This.Value=This.cLbl&N
                    Return
                Endif
            Endif
        Next
    Proc ReadVar(oObj)
        If oObj.IsVar(This.cVarName)
            This.SetValue(oObj.GetVar(This.cVarName))
        Endif
    Proc WriteVar(oObj)
        If oObj.IsVar(This.cVarName)
            oObj.SetVar(This.cVarName,This.GetValue())
        Endif
Enddefine

Define Class painter_btn_exec As CommandButton
    cVarName='.-.'
    cType=''
    cPrg=''
    cTitle=''
    nFormatPicture=16
    Proc Serialize(xcg)
        If xcg.bIsStoring
        Else
            *--- Zucchetti Aulla inizio
*!*	            This.Left=xcg.Load('N',0)
*!*	            This.Top=xcg.Load('N',0)
*!*	            This.Width=xcg.Load('N',0)
*!*	            This.Height=xcg.Load('N',0)
			This.Move(xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0))
			*--- Zucchetti Aulla fine
            xcg.Eoc()
            This.cVarName=xcg.Load('C','NoName')
            This.cType=xcg.Load('C','QRY')
            This.cPrg=xcg.Load('C','')
            This.cTitle=xcg.Load('C','')
            This.Picture=xcg.Load('C','')
            This.Style=xcg.Load('N',0)
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.ForeColor = xcg.Load('N',0)
                This.FontBold = xcg.Load('L',.F.)
                This.FontItalic = xcg.Load('L',.F.)
                This.FontUnderline = xcg.Load('L',.F.)
                This.FontStrikethru = xcg.Load('L',.F.)
                l_GlbFont= xcg.Load('L',.F.)
                If Not xcg.Eoc(.T.)
                    This.nFormatPicture=xcg.Load('N',16)
                    xcg.Eoc()
                ENDIF    
                If l_GlbFont
                    This.FontName = SetFontName('B', This.FontName, l_GlbFont)
                    This.FontSize = SetFontSize('B', This.FontSize, l_GlbFont)
                    This.FontItalic = SetFontItalic('B', This.FontItalic, l_GlbFont)
                    This.FontBold = SetFontBold('B', This.FontBold, l_GlbFont)
                    This.FontUnderline = .F.
                    This.FontStrikethru = .F.
                Endif
            Endif
            This.Caption=This.cTitle
            If This.Style=1
                This.MousePointer=10
            Endif
         Endif

    Procedure Picture_assign
        Lparameters cNewVal
        If Empty(m.cNewVal)
            This.Picture = ''
            Return
        Endif
        Local cFileBmp, l_Dim
        l_Dim= i_nBtnImgSize
        If Vartype(i_ThemesManager)=='O'
            m.cFileBmp  = i_ThemesManager.PutBmp(This,m.cNewVal, l_Dim)
        Else
            This.Picture = m.cNewVal
        Endif
    Endproc

    Func GetValue()
        Return('')
    Proc Click()
        Local N,oVfmManager
        If Empty(This.cPrg) And !(This.cType$'DOWN,UP,HOME')
            Return
        Endif
        *--- Zucchetti Aulla Inizio - Gestione caricamento maschera su gadget
        *--- se la maschera � caricata su un gadget uso direttamente lui come gestore
        If (This.cType$'DOWN,UP,HOME')
	        If Lower(this.Parent.Parent.class)=="cp_vfmviewer"
	        	m.oVfmManager = this.Parent.Parent
	        Else
	        	m.oVfmManager = i_oVfmManager
	        Endif
        Endif
        *--- Zucchetti Aulla Fine - Gestione caricamento maschera su gadget
        Do Case
            Case This.cType='PRG'
                N=This.cPrg
                If N='quitting'
                    Clear Events
                Else
                    &N
                Endif
            Case This.cType='MSK'
                VM_EXEC(This.cPrg,This.Parent)
            Case This.cType='QRY'
                vq_exec(This.cPrg,This.Parent)
            Case This.cType='ZOOM'
                vz_exec(This.cPrg,This.Parent)
            Case This.cType='HOME'
                m.oVfmManager.Home()
            Case This.cType='DOWN'
                m.oVfmManager.DrillDown()
            Case This.cType='UP'
                m.oVfmManager.DrillUp()
        Endcase
        Return
    Proc settabindex(N)
        This.TabIndex=N
    Proc ReadVar(o)
        Return
    Proc WriteVar(o)
        Return
Enddefine

Define Class painter_bitmap_exec As Image
    cVarName='.-.'
    cType=''
    cPrg=''
    cTitle=''
    nFormatPicture=16
    Proc Serialize(xcg)
        If xcg.bIsStoring
        Else
			*--- Zucchetti Aulla inizio
*!*	            This.Left=xcg.Load('N',0)
*!*	            This.Top=xcg.Load('N',0)
*!*	            This.Width=xcg.Load('N',0)
*!*	            This.Height=xcg.Load('N',0)
			This.Move(xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0))
			*--- Zucchetti Aulla fine
            xcg.Eoc()
            This.cVarName=xcg.Load('C','NoName')
            This.cType=xcg.Load('C','QRY')
            This.cPrg=xcg.Load('C','')
            This.cTitle=xcg.Load('C','')
            This.Picture=xcg.Load('C','')
            If Not xcg.Eoc(.T.)
                This.nFormatPicture=xcg.Load('N',16)
                xcg.Eoc()
            endif
            This.Picture = IIF(LOWER(JUSTEXT(This.Picture))='ico',FORCEEXT(i_ThemesManager.RetBmpFromIco(This.Picture, This.nFormatPicture),"bmp"),This.Picture)
            This.ZOrder(1)
        Endif
    Func GetValue()
        Return('')
    Proc Click()
        Local N,oVfmManager
        If Empty(This.cPrg) And !(This.cType$'DOWN,UP,HOME')
            Return
        Endif
        *--- Zucchetti Aulla Inizio - Gestione caricamento maschera su gadget
        *--- se la maschera � caricata su un gadget uso direttamente lui come gestore
        If (This.cType$'DOWN,UP,HOME')
	        If Lower(this.Parent.Parent.class)=="cp_vfmviewer"
	        	m.oVfmManager = this.Parent.Parent
	        Else
	        	m.oVfmManager = i_oVfmManager
	        Endif
        Endif
        *--- Zucchetti Aulla Fine - Gestione caricamento maschera su gadget
        Do Case
            Case This.cType='PRG'
                N=This.cPrg
                &N
            Case This.cType='MSK'
                VM_EXEC(This.cPrg,This.Parent)
            Case This.cType='QRY'
                vq_exec(This.cPrg,This.Parent)
            Case This.cType='ZOOM'
                vz_exec(This.cPrg,This.Parent)
            Case This.cType='HOME'
                m.oVfmManager.Home()
            Case This.cType='DOWN'
                m.oVfmManager.DrillDown()
            Case This.cType='UP'
                m.oVfmManager.DrillUp()
        Endcase
        Return
    Proc settabindex(N)
        Return
    Proc ReadVar(o)
        Return
    Proc WriteVar(o)
        Return
Enddefine

Define Class draw_LineNode_exec As Custom
    cNext=""		&&Prossimo nodo
    cRoot=""		&&Nodo padre da cui parte il connettore
    cMBtn=""		&&Media button
    cMBtnAnchor=""	&&Ancoraggio LRTB
    Visible=.F.
    cVarName=""
    nStartCap=0
    nEndCap=0
    nBorderColor=RGB(0,0,255)
    nBorderWidth=2
    nDashStyle = 0
    nCurvature = 5
    nSCAngle = 1
    nSCSize = 1
    nECAngle = 1
    nECSize = 1

    Procedure SetTabIndex(idx)
    Endproc

    Proc Serialize(xcg)
        If xcg.bIsStoring
        Else
            This.Left=xcg.Load('N',0)
            This.Top=xcg.Load('N',0)
            This.Width=xcg.Load('N',0)
            This.Height=xcg.Load('N',0)
            This.Name=xcg.Load('C',This.Name)
            xcg.Eoc()
            This.cNext=xcg.Load('C',"")
            This.cRoot=xcg.Load('C',"")
            This.cMBtn=xcg.Load('C',"")
            This.cMBtnAnchor=xcg.Load('C',"")
            This.nBorderColor = xcg.Load('N',RGB(0,0,255))  
            This.nBorderWidth = xcg.Load('N',2)
            This.nStartCap = xcg.Load('N',0)
            This.nEndCap = xcg.Load('N',0)  
            This.nDashStyle = xcg.Load('N',0) 
            This.nCurvature = xcg.Load('N',5)   
            If Not xcg.Eoc(.T.)
                This.nSCAngle= xcg.Load('N',36)
                This.nSCSize= xcg.Load('N',15)
                This.nECAngle= xcg.Load('N',36)
                This.nECSize= xcg.Load('N',15)
                xcg.Eoc()
            endif
        Endif
Enddefine

Define Class painter_mediabtn_exec As Container
    Width = 48
    Height = 48
    BackStyle = 0
    BorderWidth = 0
    MousePointer = 15

    cIcon=""
    cText=""
    nRadius = 6
    BackColor = Rgb(227,239,255)
    BorderColor = Rgb(179,212,255)
    nAlpha = 0
    nAlphaH = 100
    nAlphaD = 200
    nOffsetX = 3
    nOffsetY = 3
    cShape = "Process"
    *!*	Process
    *!*	Decision
    *!*	Document
    *!*	MultipleDocuments
    *!*	PredefinedProcess
    *!*	Terminator
    *!*	ManualInput
    *!*	Preparation
    *!*	PaperTape
    *!*	Database
    *!*	ManualOperation
    *!*	Merge
    *!*	Connector
    *!*	ArrowUp
    *!*	ArrowDown
    *!*	ArrowLeft
    *!*	ArrowRight
    *!* Cloud
    BrdWidth = 1
    nFormatPicture=0

    FontName = "Tahoma"
    FontSize = 10
    FontColor = Rgb(76,76,76)
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    FontStyle = 0

    Add Object Btn As CommandButton With Style=1, Top=-1, Left=-1, Width=50, Height = 50
    Add Object ImgBackground As Image With Top=0, Left=0, Width=48, Height = 48, BackStyle=0, MousePointer = 15
    cPictureVal=""
    cPictureValMouseHover=""
    cPictureValMouseDown=""

    cVarName='.-.'
    cType=''
    cPrg=''
    nDblClick=0
    bBtnFocus = .F.
    cEditCond = ""
    nColorDisabled = Rgb(76,76,76)

	PROCEDURE Init()
		DODEFAULT()
        If i_VisualTheme<>-1
        	This.BorderColor = i_ThemesManager.GetProp(3)
        	This.BackColor = i_ThemesManager.GetProp(7)
        ELSE
        	This.BorderColor = Rgb(179,212,255)
        	This.BackColor = Rgb(227,239,255)	                    
        Endif

    Proc Serialize(xcg)
        If xcg.bIsStoring
        Else
			*--- Zucchetti Aulla inizio
*!*	            This.Left=xcg.Load('N',0)
*!*	            This.Top=xcg.Load('N',0)
*!*	            This.Width=xcg.Load('N',0)
*!*	            This.Height=xcg.Load('N',0)
			This.Move(xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0))
			*--- Zucchetti Aulla fine
            xcg.Eoc()
            This.cVarName=xcg.Load('C','NoName')
            This.cType=xcg.Load('C','QRY')
            This.cPrg=xcg.Load('C','')
            This.cText=xcg.Load('C','')
            cProvIcon=xcg.Load('C','')
            This.nDblClick=xcg.Load('N',0)
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Tahoma')
                This.FontSize = Int(xcg.Load('N',10))
                This.FontColor = xcg.Load('N',0)
                This.FontBold = xcg.Load('L',.F.)
                This.FontItalic = xcg.Load('L',.F.)
                This.FontUnderline = xcg.Load('L',.F.)
                This.FontStrikeout = xcg.Load('L',.F.)
                This.FontStyle = FONTSTYLEREGULAR
                This.FontStyle = This.FontStyle + Iif(This.FontBold, FONTSTYLEBOLD, 0)
                This.FontStyle = This.FontStyle + Iif(This.FontItalic, FONTSTYLEITALIC, 0)
                This.FontStyle = This.FontStyle + Iif(This.FontUnderline, FONTSTYLEUNDERLINE, 0)
                This.FontStyle = This.FontStyle + Iif(This.FontStrikeout, FONTSTYLESTRIKEOUT, 0)

                l_GlbFont = xcg.Load('L',.F.)
                If Not xcg.Eoc(.T.)
                    This.cShape = xcg.Load('C','Process')
                    This.nAlpha = xcg.Load('N',0)
                    This.nAlphaH = xcg.Load('N',100)
                    This.nAlphaD = xcg.Load('N',200)
                    This.BrdWidth = xcg.Load('N',1)
                    This.cEditCond = xcg.Load('C','')
                    This.nRadius = xcg.Load('N',6)
                    If Not xcg.Eoc(.T.)
	                    If i_VisualTheme<>-1
	                    	This.BorderColor = xcg.Load('N',i_ThemesManager.GetProp(3))
	                    	This.BackColor = xcg.Load('N',i_ThemesManager.GetProp(7))
	                    ELSE
	                    	This.BorderColor = xcg.Load('N',Rgb(179,212,255))
	                    	This.BackColor = xcg.Load('N',Rgb(227,239,255))	                    	
	                    Endif
	                    If Not xcg.Eoc(.T.)
                            This.nFormatPicture=xcg.Load('N',0)
                            xcg.Eoc()
	                    Endif
	                Endif
                ENDIF
                This.cIcon = m.cProvIcon
                If l_GlbFont
                    This.FontName = SetFontName('B', This.FontName, l_GlbFont)
                    This.FontSize = SetFontSize('B', This.FontSize, l_GlbFont)
                    This.FontItalic = SetFontItalic('B', This.FontItalic, l_GlbFont)
                    This.FontBold = SetFontBold('B', This.FontBold, l_GlbFont)
                    This.FontStyle = FONTSTYLEREGULAR
                    This.FontStyle = This.FontStyle + Iif(This.FontBold, .FONTSTYLEBOLD, 0)
                    This.FontStyle = This.FontStyle + Iif(This.FontItalic, FONTSTYLEITALIC, 0)
                Endif
            Endif
            This._Init()
        Endif

    Procedure cIcon_Assign(cNewVal)
        If Empty(m.cNewVal)
            This.cIcon = ''
            Return
        Endif
        Local cFileBmp, cOrigBmp, cAttBmp
        If Vartype(i_ThemesManager)=='O'
            m.cOrigBmp = Iif(Lower(Justext(m.cNewVal)) == 'ico', Forceext(m.cNewVal, "bmp"), m.cNewVal)
            m.cAttBmp = i_ThemesManager.RetBmpFromIco(m.cOrigBmp, IIF(TYPE("This.nFormatPicture")<>"N",m.i_nbtnimgsize,This.nFormatPicture))
            This.cIcon = Forceext(m.cAttBmp, Justext(m.cOrigBmp))
        Else
            This.cIcon = m.cNewVal
        Endif
    Endproc

    Func GetValue()
        Return('')
    Proc Click()
        Local N, oVfmManager
        If Empty(This.cPrg) And !(This.cType$'DOWN,UP,HOME')
            Return
        Endif
        *--- Zucchetti Aulla Inizio - Gestione caricamento maschera su gadget
        *--- se la maschera � caricata su un gadget uso direttamente lui come gestore
        If (This.cType$'DOWN,UP,HOME')
	        If Lower(this.Parent.Parent.class)=="cp_vfmviewer"
	        	m.oVfmManager = this.Parent.Parent
	        Else
	        	m.oVfmManager = i_oVfmManager
	        Endif
        Endif
        *--- Zucchetti Aulla Fine - Gestione caricamento maschera su gadget
        Do Case
            Case This.cType='PRG'
                N=This.cPrg
                If N='quitting'
                    Clear Events
                Else
                    &N
                Endif
            Case This.cType='MSK'
                VM_EXEC(This.cPrg,This.Parent)
            Case This.cType='QRY'
                vq_exec(This.cPrg,This.Parent)
            Case This.cType='ZOOM'
                vz_exec(This.cPrg,This.Parent)
            Case This.cType='HOME'
                m.oVfmManager.Home()
            Case This.cType='DOWN'
                m.oVfmManager.DrillDown()
            Case This.cType='UP'
                m.oVfmManager.DrillUp()
        Endcase
        Return
    Proc settabindex(N)
        This.TabIndex=N
    Proc ReadVar(o)
        Return
    Proc WriteVar(o)
        Return

    Procedure Resize()
    	*--- Zucchetti Aulla inizio
*!*	        This.Btn.Width = This.Width + 2
*!*	        This.Btn.Height = This.Height + 2
		This.Btn.Move(This.Btn.Left, This.Btn.Top, This.Width + 2, This.Height + 2)
*!*	        This.ImgBackground.Width = This.Width
*!*	        This.ImgBackground.Height = This.Height
		This.ImgBackground.Move(This.ImgBackground.Left, This.ImgBackground.Top, This.Width, This.Height)
        *--- Zucchetti Aulla fine
    Endproc

    Procedure _Init()
        If !Empty(This.cEditCond)
            Local l_m
            l_m = This.cEditCond
            This.Enabled = &l_m
            This.MousePointer = 0
        Endif
        If i_VisualTheme<>-1
            *This.BackColor = i_ThemesManager.GetProp(7)
            *This.BorderColor = i_ThemesManager.GetProp(3)
            This.nColorDisabled = i_ThemesManager.GetProp(71)
        Endif
        This.DrawButton("N")&& Valorizza cPictureVal,cPictureValMouseHover,cPictureValMouseDown
        This.ImgBackground.PictureVal = This.cPictureVal
    Endproc

    Procedure Btn.GotFocus
        This.Parent.bBtnFocus = .T.
        If Empty(This.Parent.cPictureValMouseHover)
            This.Parent.DrawButton("H")
        Endif
        This.Parent.ImgBackground.PictureVal = This.Parent.cPictureValMouseHover

    Procedure Btn.LostFocus
        This.Parent.bBtnFocus = .F.
        This.Parent.ImgBackground.PictureVal = This.Parent.cPictureVal

    Procedure Btn.Click()
        If Empty(This.Parent.cPictureValMouseDown)
            This.Parent.DrawButton("D")
        Endif
        This.ImgBackground.PictureVal = This.Parent.cPictureValMouseDown
        This.ImgBackground.PictureVal = This.Parent.cPictureValMouseHover
        This.Parent.Click()

    Procedure ImgBackground.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.Parent.Enabled
            If Empty(This.Parent.cPictureValMouseHover)
                This.Parent.DrawButton("H")
            Endif
            This.PictureVal = This.Parent.cPictureValMouseHover
        Endif

    Procedure ImgBackground.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.Parent.Enabled
            If !This.Parent.bBtnFocus
                This.PictureVal = This.Parent.cPictureVal
            Endif
        Endif

    Procedure ImgBackground.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.Parent.Enabled
            If This.Parent.nDblClick=0
                If Empty(This.Parent.cPictureValMouseDown)
                    This.Parent.DrawButton("D")
                Endif
                This.PictureVal = This.Parent.cPictureValMouseDown
            Endif
        Endif

    Procedure ImgBackground.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.Parent.Enabled
            If Empty(This.Parent.cPictureValMouseHover)
                This.Parent.DrawButton("H")
            Endif
            This.PictureVal = This.Parent.cPictureValMouseHover
        Endif

    Procedure ImgBackground.Click()
        If This.Parent.Enabled
            This.Parent.Btn.SetFocus()
            If This.Parent.nDblClick=0
                This.Parent.Click()
            Endif
        Endif

    Procedure ImgBackground.DblClick()
        If This.Parent.Enabled
            This.Parent.Btn.SetFocus()
            If This.Parent.nDblClick=1
                This.Parent.Click()
            Endif
        Endif

    Procedure DrawButton(cType)
        *--- cType: "N"ormal, Mouse"H"over, Mouse"D"own

        *!*	-Process
        *!*	-Decision
        *!*	-Document
        *!*	-MultipleDocuments
        *!*	-PredefinedProcess
        *!*	-Terminator
        *!*	-ManualInput
        *!*	-Preparation
        *!*	-PaperTape
        *!*	-Database
        *!*	-ManualOperation
        *!*	-Merge
        *!*	-Connector
        *!*	-ArrowUp
        *!*	-ArrowDown
        *!*	-ArrowLeft
        *!*	-ArrowRight
        *!*	-Cloud

        Dimension aRPoints[1,1]
        Dimension aLPoints[1,1]
        Dimension aBPoints[1,1]
        Dimension aAPoints[1,1]
        bCloseFigure = .F.

        Do Case
            Case This.cShape=="Process"
                This.nOffsetX=3
                This.nOffsetY=3
                Dimension aRPoints[4,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=This.BrdWidth
                aRPoints[1,3]=.T.
                aRPoints[2,1]=This.Width-This.BrdWidth
                aRPoints[2,2]=This.BrdWidth
                aRPoints[2,3]=.T.
                aRPoints[3,1]=This.Width-This.BrdWidth
                aRPoints[3,2]=This.Height-This.BrdWidth
                aRPoints[3,3]=.T.
                aRPoints[4,1]=This.BrdWidth
                aRPoints[4,2]=This.Height-This.BrdWidth
                aRPoints[4,3]=.T.

            Case This.cShape=="Decision"
                This.nOffsetX=This.Width/4
                This.nOffsetY=This.Height/4
                Dimension aRPoints[4,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=This.Height/2
                aRPoints[1,3]=.T.
                aRPoints[2,1]=This.Width/2
                aRPoints[2,2]=This.BrdWidth
                aRPoints[2,3]=.T.
                aRPoints[3,1]=This.Width-This.BrdWidth
                aRPoints[3,2]=This.Height/2
                aRPoints[3,3]=.T.
                aRPoints[4,1]=This.Width/2
                aRPoints[4,2]=This.Height-This.BrdWidth
                aRPoints[4,3]=.T.

            Case This.cShape=="Document"
                This.nOffsetX=3
                This.nOffsetY=0.15*This.Height
                Dimension aRPoints[4,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=0.85*This.Height-This.BrdWidth
                aRPoints[1,3]=.F.
                aRPoints[2,1]=This.BrdWidth
                aRPoints[2,2]=This.BrdWidth
                aRPoints[2,3]=.T.
                aRPoints[3,1]=This.Width-This.BrdWidth
                aRPoints[3,2]=This.BrdWidth
                aRPoints[3,3]=.T.
                aRPoints[4,1]=This.Width-This.BrdWidth
                aRPoints[4,2]=This.Height-This.BrdWidth
                aRPoints[4,3]=.F.

                Dimension aBPoints[4,2]
                aBPoints[1,1]=This.Width-This.BrdWidth
                aBPoints[1,2]=This.Height-This.BrdWidth
                aBPoints[2,1]=0.75*This.Width-This.BrdWidth
                aBPoints[2,2]=0.70*This.Height-This.BrdWidth
                aBPoints[3,1]=0.25*This.Width
                aBPoints[3,2]=1.15*This.Height-This.BrdWidth
                aBPoints[4,1]=This.BrdWidth
                aBPoints[4,2]=0.85*This.Height-This.BrdWidth

            Case This.cShape=="PredefinedProcess"
                This.nOffsetX=0.1*This.Width+This.BrdWidth+3
                This.nOffsetY=3
                Dimension aRPoints[4,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=This.BrdWidth
                aRPoints[1,3]=.T.
                aRPoints[2,1]=This.Width-This.BrdWidth
                aRPoints[2,2]=This.BrdWidth
                aRPoints[2,3]=.T.
                aRPoints[3,1]=This.Width-This.BrdWidth
                aRPoints[3,2]=This.Height-This.BrdWidth
                aRPoints[3,3]=.T.
                aRPoints[4,1]=This.BrdWidth
                aRPoints[4,2]=This.Height-This.BrdWidth
                aRPoints[4,3]=.T.

                Dimension aLPoints[4,2]
                aLPoints[1,1]=0.1*This.Width+This.BrdWidth
                aLPoints[1,2]=This.BrdWidth
                aLPoints[2,1]=0.1*This.Width+This.BrdWidth
                aLPoints[2,2]=This.Height-This.BrdWidth
                aLPoints[3,1]=0.9*This.Width
                aLPoints[3,2]=This.BrdWidth
                aLPoints[4,1]=0.9*This.Width
                aLPoints[4,2]=This.Height-This.BrdWidth

            Case This.cShape=="Terminator"
                This.nOffsetX=0.15*This.Width
                This.nOffsetY=3
                Dimension aAPoints[2,6]
                aAPoints[1,1] = 0
                aAPoints[1,2] = 0
                aAPoints[1,3] = 0.3*This.Width
                aAPoints[1,4] = This.Height-This.BrdWidth
                aAPoints[1,5] = 90
                aAPoints[1,6] = 180

                aAPoints[2,1] = 0.7*This.Width
                aAPoints[2,2] = 0
                aAPoints[2,3] = 0.3*This.Width
                aAPoints[2,4] = This.Height-This.BrdWidth
                aAPoints[2,5] = 270
                aAPoints[2,6] = 180
                bCloseFigure = .T.

            Case This.cShape=="ManualInput"
                This.nOffsetX=3
                This.nOffsetY=0.1*This.Height+3
                Dimension aRPoints[4,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=0.1*This.Height
                aRPoints[1,3]=.T.
                aRPoints[2,1]=This.Width-This.BrdWidth
                aRPoints[2,2]=This.BrdWidth
                aRPoints[2,3]=.T.
                aRPoints[3,1]=This.Width-This.BrdWidth
                aRPoints[3,2]=This.Height-This.BrdWidth
                aRPoints[3,3]=.T.
                aRPoints[4,1]=This.BrdWidth
                aRPoints[4,2]=This.Height-This.BrdWidth
                aRPoints[4,3]=.T.

            Case This.cShape=="Preparation"
                This.nOffsetX=3
                This.nOffsetY=3
                Dimension aRPoints[6,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=0.5*This.Height
                aRPoints[1,3]=.T.
                aRPoints[2,1]=0.25*This.Width
                aRPoints[2,2]=This.BrdWidth
                aRPoints[2,3]=.T.
                aRPoints[3,1]=0.75*This.Width
                aRPoints[3,2]=This.BrdWidth
                aRPoints[3,3]=.T.
                aRPoints[4,1]=This.Width-This.BrdWidth
                aRPoints[4,2]=0.5*This.Height
                aRPoints[4,3]=.T.
                aRPoints[5,1]=0.75*This.Width
                aRPoints[5,2]=This.Height-This.BrdWidth
                aRPoints[5,3]=.T.
                aRPoints[6,1]=0.25*This.Width
                aRPoints[6,2]=This.Height-This.BrdWidth
                aRPoints[6,3]=.T.

            Case This.cShape=="PaperTape"
                This.nOffsetX=3
                This.nOffsetY=3

                Dimension aBPoints[8,2]
                aBPoints[1,1]=This.BrdWidth
                aBPoints[1,2]=This.BrdWidth
                aBPoints[2,1]=0.25*This.Width-This.BrdWidth
                aBPoints[2,2]=0.25*This.Height
                aBPoints[3,1]=0.75*This.Width-This.BrdWidth
                aBPoints[3,2]=-0.125*This.Height
                aBPoints[4,1]=This.Width-This.BrdWidth
                aBPoints[4,2]=0.125*This.Height

                aBPoints[8,1]=This.BrdWidth
                aBPoints[8,2]=0.875*This.Height
                aBPoints[7,1]=0.25*This.Width-This.BrdWidth
                aBPoints[7,2]=1.125*This.Height-This.BrdWidth
                aBPoints[6,1]=0.75*This.Width-This.BrdWidth
                aBPoints[6,2]=0.75*This.Height-This.BrdWidth
                aBPoints[5,1]=This.Width-This.BrdWidth
                aBPoints[5,2]=This.Height-This.BrdWidth

                bCloseFigure = .T.

            Case This.cShape=="ManualOperation"
                This.nOffsetX=3
                This.nOffsetY=3
                Dimension aRPoints[4,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=This.BrdWidth
                aRPoints[1,3]=.T.
                aRPoints[2,1]=This.Width-This.BrdWidth
                aRPoints[2,2]=This.BrdWidth
                aRPoints[2,3]=.T.
                aRPoints[3,1]=0.75*This.Width
                aRPoints[3,2]=This.Height-This.BrdWidth
                aRPoints[3,3]=.T.
                aRPoints[4,1]=0.25*This.Width
                aRPoints[4,2]=This.Height-This.BrdWidth
                aRPoints[4,3]=.T.

            Case This.cShape=="Merge"
                This.nOffsetX=0.25*This.Width
                This.nOffsetY=0.25*This.Height
                Dimension aRPoints[3,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=This.BrdWidth
                aRPoints[1,3]=.T.
                aRPoints[2,1]=This.Width-This.BrdWidth
                aRPoints[2,2]=This.BrdWidth
                aRPoints[2,3]=.T.
                aRPoints[3,1]=0.50*This.Width
                aRPoints[3,2]=This.Height-This.BrdWidth
                aRPoints[3,3]=.T.

            Case This.cShape=="Connector"
                This.nOffsetX=3
                This.nOffsetY=3
                Dimension aAPoints[1,6]
                aAPoints[1,1] = 0
                aAPoints[1,2] = 0
                aAPoints[1,3] = This.Width-This.BrdWidth
                aAPoints[1,4] = This.Height-This.BrdWidth
                aAPoints[1,5] = 0
                aAPoints[1,6] = 360

            Case This.cShape=="ArrowRight"
                This.nOffsetX=3
                This.nOffsetY=3

                If This.Height>=This.Width
                    px=0.5*This.Width
                Else
                    px=This.Width-0.5*This.Height
                Endif

                Dimension aRPoints[7,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=0.25*This.Height
                aRPoints[1,3]=.T.

                aRPoints[2,1]=This.BrdWidth
                aRPoints[2,2]=0.75*This.Height
                aRPoints[2,3]=.T.

                aRPoints[3,1]=px
                aRPoints[3,2]=0.75*This.Height
                aRPoints[3,3]=.T.

                aRPoints[4,1]=px
                aRPoints[4,2]=This.Height-This.BrdWidth
                aRPoints[4,3]=.T.

                aRPoints[5,1]=This.Width-This.BrdWidth
                aRPoints[5,2]=0.50*This.Height
                aRPoints[5,3]=.T.

                aRPoints[6,1]=px
                aRPoints[6,2]=This.BrdWidth
                aRPoints[6,3]=.T.

                aRPoints[7,1]=px
                aRPoints[7,2]=0.25*This.Height
                aRPoints[7,3]=.T.

            Case This.cShape=="ArrowLeft"
                This.nOffsetX=3
                This.nOffsetY=3

                If This.Height>=This.Width
                    px=0.5*This.Width
                Else
                    px=0.5*This.Height
                Endif

                Dimension aRPoints[7,3]
                aRPoints[1,1]=This.Width-This.BrdWidth
                aRPoints[1,2]=0.25*This.Height
                aRPoints[1,3]=.T.

                aRPoints[2,1]=This.Width-This.BrdWidth
                aRPoints[2,2]=0.75*This.Height
                aRPoints[2,3]=.T.

                aRPoints[3,1]=px
                aRPoints[3,2]=0.75*This.Height
                aRPoints[3,3]=.T.

                aRPoints[4,1]=px
                aRPoints[4,2]=This.Height-This.BrdWidth
                aRPoints[4,3]=.T.

                aRPoints[5,1]=This.BrdWidth
                aRPoints[5,2]=0.50*This.Height
                aRPoints[5,3]=.T.

                aRPoints[6,1]=px
                aRPoints[6,2]=This.BrdWidth
                aRPoints[6,3]=.T.

                aRPoints[7,1]=px
                aRPoints[7,2]=0.25*This.Height
                aRPoints[7,3]=.T.

            Case This.cShape=="ArrowUp"
                This.nOffsetX=3
                This.nOffsetY=3

                If This.Height>=This.Width
                    py=0.5*This.Width
                Else
                    py=0.5*This.Height
                Endif

                Dimension aRPoints[7,3]
                aRPoints[1,1]=0.25*This.Width
                aRPoints[1,2]=This.Height-This.BrdWidth
                aRPoints[1,3]=.T.

                aRPoints[2,1]=0.75*This.Width
                aRPoints[2,2]=This.Height-This.BrdWidth
                aRPoints[2,3]=.T.

                aRPoints[3,1]=0.75*This.Width
                aRPoints[3,2]=py
                aRPoints[3,3]=.T.

                aRPoints[4,1]=This.Width-This.BrdWidth
                aRPoints[4,2]=py
                aRPoints[4,3]=.T.

                aRPoints[5,1]=0.50*This.Width
                aRPoints[5,2]=This.BrdWidth
                aRPoints[5,3]=.T.

                aRPoints[6,1]=This.BrdWidth
                aRPoints[6,2]=py
                aRPoints[6,3]=.T.

                aRPoints[7,1]=0.25*This.Width
                aRPoints[7,2]=py
                aRPoints[7,3]=.T.

            Case This.cShape=="ArrowDown"
                This.nOffsetX=3
                This.nOffsetY=3

                If This.Height>=This.Width
                    py=This.Height-0.5*This.Width
                Else
                    py=0.5*This.Height
                Endif

                Dimension aRPoints[7,3]
                aRPoints[1,1]=0.25*This.Width
                aRPoints[1,2]=This.BrdWidth
                aRPoints[1,3]=.T.

                aRPoints[2,1]=0.75*This.Width
                aRPoints[2,2]=This.BrdWidth
                aRPoints[2,3]=.T.

                aRPoints[3,1]=0.75*This.Width
                aRPoints[3,2]=py
                aRPoints[3,3]=.T.

                aRPoints[4,1]=This.Width-This.BrdWidth
                aRPoints[4,2]=py
                aRPoints[4,3]=.T.

                aRPoints[5,1]=0.50*This.Width
                aRPoints[5,2]=This.Height-This.BrdWidth
                aRPoints[5,3]=.T.

                aRPoints[6,1]=This.BrdWidth
                aRPoints[6,2]=py
                aRPoints[6,3]=.T.

                aRPoints[7,1]=0.25*This.Width
                aRPoints[7,2]=py
                aRPoints[7,3]=.T.

            Case This.cShape=="Cloud"
                This.nOffsetX=3
                This.nOffsetY=3

                Dimension aBPoints[28,2]
                aBPoints[1,1]=This.Width/8
                aBPoints[1,2]=This.Height/3
                aBPoints[2,1]=This.BrdWidth
                aBPoints[2,2]=This.Height/3
                aBPoints[3,1]=This.BrdWidth
                aBPoints[3,2]=0.75*This.Height
                aBPoints[4,1]=This.Width/8
                aBPoints[4,2]=0.75*This.Height

                aBPoints[5,1]=This.Width/8
                aBPoints[5,2]= 0.75*This.Height
                aBPoints[6,1]= This.Width/8
                aBPoints[6,2]=This.Height
                aBPoints[7,1]= This.Width/2
                aBPoints[7,2]=This.Height
                aBPoints[8,1]= This.Width/2
                aBPoints[8,2]=0.875*This.Height

                aBPoints[9,1]=This.Width/2
                aBPoints[9,2]=0.875*This.Height
                aBPoints[10,1]=This.Width/2
                aBPoints[10,2]=This.Height
                aBPoints[11,1]=0.75*This.Width
                aBPoints[11,2]=This.Height
                aBPoints[12,1]=0.75*This.Width
                aBPoints[12,2]=0.875*This.Height

                aBPoints[13,1]=0.75*This.Width
                aBPoints[13,2]=0.875*This.Height
                aBPoints[14,1]=This.Width
                aBPoints[14,2]=This.Height
                aBPoints[15,1]=This.Width
                aBPoints[15,2]=0.6*This.Height
                aBPoints[16,1]=15/16*This.Width
                aBPoints[16,2]=0.6*This.Height

                aBPoints[17,1]=15/16*This.Width
                aBPoints[17,2]=0.6*This.Height
                aBPoints[18,1]=This.Width
                aBPoints[18,2]=0.6*This.Height
                aBPoints[19,1]=This.Width
                aBPoints[19,2]=This.Height/6
                aBPoints[20,1]=0.75*This.Width
                aBPoints[20,2]=This.Height/6

                aBPoints[21,1]=0.75*This.Width
                aBPoints[21,2]=This.Height/6
                aBPoints[22,1]=0.75*This.Width
                aBPoints[22,2]=This.BrdWidth
                aBPoints[23,1]=This.Width/2
                aBPoints[23,2]=This.BrdWidth
                aBPoints[24,1]=0.4*This.Width
                aBPoints[24,2]=0.125*This.Height

                aBPoints[25,1]=0.4*This.Width
                aBPoints[25,2]=0.125*This.Height
                aBPoints[26,1]=0.375*This.Width
                aBPoints[26,2]=This.BrdWidth
                aBPoints[27,1]=This.BrdWidth
                aBPoints[27,2]=This.Height/8
                aBPoints[28,1]=This.Width/8
                aBPoints[28,2]=This.Height/3

            Case This.cShape=="Database"
                This.nOffsetX=3
                This.nOffsetY=3
                Dimension aAPoints[4,6]
                aAPoints[1,1] = This.BrdWidth
                aAPoints[1,2] = 0.8*This.Height-This.BrdWidth
                aAPoints[1,3] = This.Width-2*This.BrdWidth
                aAPoints[1,4] = 0.2*This.Height
                aAPoints[1,5] = 0
                aAPoints[1,6] = 180

                aAPoints[2,1] = This.BrdWidth
                aAPoints[2,2] = This.BrdWidth
                aAPoints[2,3] = This.Width-2*This.BrdWidth
                aAPoints[2,4] = 0.2*This.Height
                aAPoints[2,5] = 180
                aAPoints[2,6] = 180

                aAPoints[3,1] = This.BrdWidth
                aAPoints[3,2] = This.BrdWidth
                aAPoints[3,3] = This.Width-2*This.BrdWidth
                aAPoints[3,4] = 0.2*This.Height
                aAPoints[3,5] = 0
                aAPoints[3,6] = 360

                aAPoints[4,1] = This.BrdWidth
                aAPoints[4,2] = This.BrdWidth
                aAPoints[4,3] = This.Width-2*This.BrdWidth
                aAPoints[4,4] = 0.2*This.Height
                aAPoints[4,5] = 0
                aAPoints[4,6] = 360

                bCloseFigure=.T.

            Case This.cShape=="MultipleDocuments"
                This.nOffsetX=0.1*This.Width + 3
                This.nOffsetY=0.15*This.Height
                Dimension aRPoints[26,3]
                && A
                aRPoints[1,1]=This.Width-This.BrdWidth
                aRPoints[1,2]=This.Height-This.BrdWidth
                aRPoints[1,3]=.F.
                && B
                aRPoints[2,1]=This.Width-This.BrdWidth
                aRPoints[2,2]=0.1*This.Height
                aRPoints[2,3]=.T.
                && C
                aRPoints[3,1]=0.95*This.Width-This.BrdWidth
                aRPoints[3,2]=0.1*This.Height
                aRPoints[3,3]=.F.
                && D
                aRPoints[4,1]=0.95*This.Width-This.BrdWidth
                aRPoints[4,2]=0.05*This.Height
                aRPoints[4,3]=.T.
                && E
                aRPoints[5,1]=0.9*This.Width-This.BrdWidth
                aRPoints[5,2]=0.05*This.Height
                aRPoints[5,3]=.F.
                && F
                aRPoints[6,1]=0.9*This.Width-This.BrdWidth
                aRPoints[6,2]=This.BrdWidth
                aRPoints[6,3]=.T.
                && G
                aRPoints[7,1]=This.BrdWidth
                aRPoints[7,2]=This.BrdWidth
                aRPoints[7,3]=.T.
                && H
                aRPoints[8,1]=This.BrdWidth
                aRPoints[8,2]=0.6*This.Height
                aRPoints[8,3]=.F.
                && I
                aRPoints[9,1]=0.05*This.Width-This.BrdWidth
                aRPoints[9,2]=0.65*This.Height
                aRPoints[9,3]=.F.
                && L
                aRPoints[10,1]=0.05*This.Width-This.BrdWidth
                aRPoints[10,2]=0.70*This.Height
                aRPoints[10,3]=.F.
                && M
                aRPoints[11,1]=0.1*This.Width-This.BrdWidth
                aRPoints[11,2]=0.75*This.Height
                aRPoints[11,3]=.F.
                && N
                aRPoints[12,1]=0.1*This.Width-This.BrdWidth
                aRPoints[12,2]=0.8*This.Height
                aRPoints[12,3]=.F.
                && O
                aRPoints[13,1]=0.1*This.Width-This.BrdWidth
                aRPoints[13,2]=0.1*This.Height
                aRPoints[13,3]=.T.
                && C
                aRPoints[14,1]=0.95*This.Width-This.BrdWidth
                aRPoints[14,2]=0.1*This.Height
                aRPoints[14,3]=.F.
                && D
                aRPoints[15,1]=0.95*This.Width-This.BrdWidth
                aRPoints[15,2]=0.05*This.Height
                aRPoints[15,3]=.T.
                && P
                aRPoints[16,1]=0.05*This.Width-This.BrdWidth
                aRPoints[16,2]=0.05*This.Height
                aRPoints[16,3]=.T.
                && L
                aRPoints[17,1]=0.05*This.Width-This.BrdWidth
                aRPoints[17,2]=0.70*This.Height
                aRPoints[17,3]=.F.
                && M
                aRPoints[18,1]=0.1*This.Width-This.BrdWidth
                aRPoints[18,2]=0.75*This.Height
                aRPoints[18,3]=.F.
                && N
                aRPoints[19,1]=0.1*This.Width-This.BrdWidth
                aRPoints[19,2]=0.8*This.Height
                aRPoints[19,3]=.F.
                && O
                aRPoints[20,1]=0.1*This.Width-This.BrdWidth
                aRPoints[20,2]=0.1*This.Height
                aRPoints[20,3]=.T.
                && C
                aRPoints[21,1]=0.95*This.Width-This.BrdWidth
                aRPoints[21,2]=0.1*This.Height
                aRPoints[21,3]=.F.
                && D
                aRPoints[22,1]=0.95*This.Width-This.BrdWidth
                aRPoints[22,2]=0.05*This.Height
                aRPoints[22,3]=.T.
                && P
                aRPoints[23,1]=0.05*This.Width-This.BrdWidth
                aRPoints[23,2]=0.05*This.Height
                aRPoints[23,3]=.T.
                && L
                aRPoints[24,1]=0.05*This.Width-This.BrdWidth
                aRPoints[24,2]=0.70*This.Height
                aRPoints[24,3]=.F.
                && M
                aRPoints[25,1]=0.1*This.Width-This.BrdWidth
                aRPoints[25,2]=0.75*This.Height
                aRPoints[25,3]=.F.
                && N
                aRPoints[26,1]=0.1*This.Width-This.BrdWidth
                aRPoints[26,2]=0.8*This.Height
                aRPoints[26,3]=.F.

                Dimension aBPoints[4,2]
                aBPoints[4,1]=This.Width-This.BrdWidth
                aBPoints[4,2]=This.Height-This.BrdWidth
                aBPoints[3,1]=0.75*This.Width-This.BrdWidth
                aBPoints[3,2]=0.70*This.Height
                aBPoints[2,1]=0.25*This.Width
                aBPoints[2,2]=1.15*This.Height
                aBPoints[1,1]=0.1*This.Width-This.BrdWidth
                aBPoints[1,2]=0.8*This.Height
        Endcase

        Do Case
            Case m.cType = "N"
                This.cPictureVal=cb_DrawMediaButton(This, This.nAlpha, This.nRadius, This.nOffsetX, This.nOffsetY, @aRPoints, @aLPoints, @aBPoints, @aAPoints, bCloseFigure)
            Case m.cType = "H"
                This.cPictureValMouseHover=cb_DrawMediaButton(This, This.nAlphaH, This.nRadius, This.nOffsetX, This.nOffsetY, @aRPoints, @aLPoints, @aBPoints, @aAPoints, bCloseFigure)
            Case m.cType = "D"
                This.cPictureValMouseDown=cb_DrawMediaButton(This, This.nAlphaD, This.nRadius, This.nOffsetX, This.nOffsetY, @aRPoints, @aLPoints, @aBPoints, @aAPoints, bCloseFigure)
            Case m.cType = "A"
                Return cb_DrawMediaButton(This, 255, This.nRadius, This.nOffsetX, This.nOffsetY, @aRPoints, @aLPoints, @aBPoints, @aAPoints, bCloseFigure)
        Endcase
    Endproc

    Procedure OpenGest(pCmd, cAzi, pKey1, pKey2, pKey3, pKey4, pKey5, pKey6, pKey7, pKey8, pKey9, pKey10)
        Local i_cmd
        Local i_o,i_i,i_k,i_v,i_p,l_len
        i_cmd=m.pCmd
        If Not(Empty(i_cmd))
            If Empty(cAzi) Or Trim(cAzi)=Trim(i_codazi)
                * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
                * Inserito ramo Else
                If Right(i_cmd,1)<>')'
                    * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
                    i_o=&i_cmd()
                    * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
                Else
                    i_o=&i_cmd
                Endif
                * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
                If !Isnull(i_o) And Type("i_o.cFile")="C"
                    If Used(i_o.cKeySet)
                        i_o.ecpFilter()
                        * --- carica il record specificato
                        i_k=i_o.ckeyselect
                        l_len = Alines(xKey, i_k, ",")
                        For i_i=1 To l_len
                            i_v = m.xKey[i_i]
                            i_k = "pKey"+Alltrim(Str(i_i))
                            i_o.w_&i_v = &i_k
                        Next
                        i_o.ecpSave()
                        *
                    Endif
                Endif
            Else
                cp_ErrorMsg(cp_MsgFormat(MSG_BUTTON_LINKED_TO_COMPANY__,This.cAzi),"!",cp_Translate(MSG_POSTIN_ADMIN),.F.)
            Endif
        Endif
    Endproc

Enddefine

Define Class painter_tilebtn_exec As Container
    Width = 120
    Height = 120
    BackStyle = 0
    BorderWidth = 0
    MousePointer = 15

    cIcon=""
    cText=""
    nRadius = 6
    BackColor = Rgb(227,239,255)
    BorderColor = Rgb(179,212,255)
    nAlpha = 255
    nAlphaH = 255
    nAlphaD = 240	
    nOffsetX = 3
    nOffsetY = 3
    cShape = "Square tile"
    *!*	Small tile
    *!*	Square tile
    *!*	Large tile

    BrdWidth = 1

    FontName = "Tahoma"
    FontSize = 7
    FontColor = Rgb(255,255,230)
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    FontStyle = 0

    Add Object Btn As CommandButton With Style=1, Top=-1, Left=-1, Width=120, Height = 120
    Add Object ImgBackground As Image With Top=0, Left=20, Width=80, Height = 80, BackStyle=0, MousePointer = 15
    cPictureVal=""
    cPictureValMouseHover=""
    cPictureValMouseDown=""

    cVarName='.-.'
    cType=''
    cPrg=''
    nDblClick=0
    bBtnFocus = .F.
    cEditCond = ""
    nColorDisabled = Rgb(76,76,76)

	PROCEDURE Init()
		DODEFAULT()
        If i_VisualTheme<>-1
        	This.BorderColor = i_ThemesManager.GetProp(3)
        	This.BackColor = i_ThemesManager.GetProp(7)
        ELSE
        	This.BorderColor = Rgb(179,212,255)
        	This.BackColor = Rgb(227,239,255)	                    
        Endif

    Proc Serialize(xcg)
        If xcg.bIsStoring
        Else
            This.Left=xcg.Load('N',0)
            This.Top=xcg.Load('N',0)
            This.Width=xcg.Load('N',0)
            This.Height=xcg.Load('N',0)
            xcg.Eoc()
            This.cVarName=xcg.Load('C','NoName')
            This.cType=xcg.Load('C','QRY')
            This.cPrg=xcg.Load('C','')
            This.cText=xcg.Load('C','')
            This.cIcon=xcg.Load('C','')
            This.nDblClick=xcg.Load('N',0)
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Tahoma')
                This.FontSize = Int(xcg.Load('N',10))
                This.FontColor = xcg.Load('N',0)
                This.FontBold = xcg.Load('L',.F.)
                This.FontItalic = xcg.Load('L',.F.)
                This.FontUnderline = xcg.Load('L',.F.)
                This.FontStrikeout = xcg.Load('L',.F.)
                This.FontStyle = FONTSTYLEREGULAR
                This.FontStyle = This.FontStyle + Iif(This.FontBold, FONTSTYLEBOLD, 0)
                This.FontStyle = This.FontStyle + Iif(This.FontItalic, FONTSTYLEITALIC, 0)
                This.FontStyle = This.FontStyle + Iif(This.FontUnderline, FONTSTYLEUNDERLINE, 0)
                This.FontStyle = This.FontStyle + Iif(This.FontStrikeout, FONTSTYLESTRIKEOUT, 0)

                l_GlbFont = xcg.Load('L',.F.)
                If Not xcg.Eoc(.T.)
                    This.cShape = xcg.Load('C','Process')
                    This.nAlpha = xcg.Load('N',0)
                    This.nAlphaH = xcg.Load('N',100)
                    This.nAlphaD = xcg.Load('N',200)
                    This.BrdWidth = xcg.Load('N',1)
                    This.cEditCond = xcg.Load('C','')
                    This.nRadius = xcg.Load('N',6)
                    If Not xcg.Eoc(.T.)
	                    If i_VisualTheme<>-1
	                    	This.BorderColor = xcg.Load('N',i_ThemesManager.GetProp(3))
	                    	This.BackColor = xcg.Load('N',i_ThemesManager.GetProp(7))
	                    Else
	                    	This.BorderColor = xcg.Load('N',Rgb(179,212,255))
	                    	This.BackColor = xcg.Load('N',Rgb(227,239,255))	                    	
	                    Endif
	                    xcg.Eoc()
	                Endif
                Endif
                If l_GlbFont
                    This.FontName = SetFontName('B', This.FontName, l_GlbFont)
                    This.FontSize = SetFontSize('B', This.FontSize, l_GlbFont)
                    This.FontItalic = SetFontItalic('B', This.FontItalic, l_GlbFont)
                    This.FontBold = SetFontBold('B', This.FontBold, l_GlbFont)
                    This.FontStyle = FONTSTYLEREGULAR
                    This.FontStyle = This.FontStyle + Iif(This.FontBold, .FONTSTYLEBOLD, 0)
                    This.FontStyle = This.FontStyle + Iif(This.FontItalic, FONTSTYLEITALIC, 0)
                Endif
            Endif
            This._Init()
        Endif

    Procedure cIcon_Assign(cNewVal)
        If Empty(m.cNewVal)
            This.cIcon = ''
            Return
        Endif
        Local cFileBmp, cOrigBmp, cAttBmp
        If Vartype(i_ThemesManager)=='O'
            m.cOrigBmp = Iif(Lower(Justext(m.cNewVal)) == 'ico', Forceext(m.cNewVal, "bmp"), m.cNewVal)
            m.cAttBmp = i_ThemesManager.RetBmpFromIco(m.cOrigBmp, m.i_nBtnImgSize)
            This.cIcon = Forceext(m.cAttBmp, Justext(m.cOrigBmp))
        Else
            This.cIcon = m.cNewVal
        Endif
    Endproc

    Func GetValue()
        Return('')
    Proc Click()
        Local N,oVfmManager
        If Empty(This.cPrg) And !(This.cType$'DOWN,UP,HOME')
            Return
        Endif
        *--- Zucchetti Aulla Inizio - Gestione caricamento maschera su gadget
        *--- se la maschera � caricata su un gadget uso direttamente lui come gestore
        If (This.cType$'DOWN,UP,HOME')
	        If Lower(this.Parent.Parent.class)=="cp_vfmviewer"
	        	m.oVfmManager = this.Parent.Parent
	        Else
	        	m.oVfmManager = i_oVfmManager
	        Endif
        Endif
        *--- Zucchetti Aulla Fine - Gestione caricamento maschera su gadget
        Do Case
            Case This.cType='PRG'
                N=This.cPrg
                If N='quitting'
                    Clear Events
                Else
                    &N
                Endif
            Case This.cType='MSK'
                VM_EXEC(This.cPrg,This.Parent)
            Case This.cType='QRY'
                vq_exec(This.cPrg,This.Parent)
            Case This.cType='ZOOM'
                vz_exec(This.cPrg,This.Parent)
            Case This.cType='HOME'
                m.oVfmManager.Home()
            Case This.cType='DOWN'
                m.oVfmManager.DrillDown()
            Case This.cType='UP'
                m.oVfmManager.DrillUp()
        Endcase
        Return
    Proc settabindex(N)
        This.TabIndex=N
    Proc ReadVar(o)
        Return
    Proc WriteVar(o)
        Return

    Procedure _Init()
        If !Empty(This.cEditCond)
            Local l_m
            l_m = This.cEditCond
            This.Enabled = &l_m
            This.MousePointer = 0
        Endif
        If i_VisualTheme<>-1
            *This.BackColor = i_ThemesManager.GetProp(7)
            *This.BorderColor = i_ThemesManager.GetProp(3)
            This.nColorDisabled = i_ThemesManager.GetProp(71)
        Endif
        This.DrawButton("N")&& Valorizza cPictureVal,cPictureValMouseHover,cPictureValMouseDown
        This.ImgBackground.PictureVal = This.cPictureVal
    Endproc

    Procedure Btn.GotFocus
        This.Parent.bBtnFocus = .T.
        If Empty(This.Parent.cPictureValMouseHover)
            This.Parent.DrawButton("H")
        Endif
        This.Parent.ImgBackground.PictureVal = This.Parent.cPictureValMouseHover

    Procedure Btn.LostFocus
        This.Parent.bBtnFocus = .F.
        This.Parent.ImgBackground.PictureVal = This.Parent.cPictureVal

    Procedure Btn.Click()
        If Empty(This.Parent.cPictureValMouseDown)
            This.Parent.DrawButton("D")
        Endif
        This.ImgBackground.PictureVal = This.Parent.cPictureValMouseDown
        This.ImgBackground.PictureVal = This.Parent.cPictureValMouseHover
        This.Parent.Click()

    Procedure ImgBackground.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.Parent.Enabled
            If Empty(This.Parent.cPictureValMouseHover)
                This.Parent.DrawButton("H")
            Endif
            This.PictureVal = This.Parent.cPictureValMouseHover
        Endif

    Procedure ImgBackground.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.Parent.Enabled
            If !This.Parent.bBtnFocus
                This.PictureVal = This.Parent.cPictureVal
            Endif
        Endif

    Procedure ImgBackground.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.Parent.Enabled
            If This.Parent.nDblClick=0
                If Empty(This.Parent.cPictureValMouseDown)
                    This.Parent.DrawButton("D")
                Endif
                This.PictureVal = This.Parent.cPictureValMouseDown
            Endif
        Endif

    Procedure ImgBackground.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If This.Parent.Enabled
            If Empty(This.Parent.cPictureValMouseHover)
                This.Parent.DrawButton("H")
            Endif
            This.PictureVal = This.Parent.cPictureValMouseHover
        Endif

    Procedure ImgBackground.Click()
        If This.Parent.Enabled
            This.Parent.Btn.SetFocus()
            If This.Parent.nDblClick=0
                This.Parent.Click()
            Endif
        Endif

    Procedure ImgBackground.DblClick()
        If This.Parent.Enabled
            This.Parent.Btn.SetFocus()
            If This.Parent.nDblClick=1
                This.Parent.Click()
            Endif
        Endif

    Procedure DrawButton(cType)
        *--- cType: "N"ormal, Mouse"H"over, Mouse"D"own

        *!*	-Process
        *!*	-Terminator

        Dimension aRPoints[1,1]
        Dimension aLPoints[1,1]
        Dimension aBPoints[1,1]
        Dimension aAPoints[1,1]
        bCloseFigure = .F.

		This.nOffsetX=3
		This.nOffsetY=3
		Dimension aRPoints[4,3]
                aRPoints[1,1]=This.BrdWidth
                aRPoints[1,2]=This.BrdWidth
                *aRPoints[1,3]=.T.
                aRPoints[2,1]=This.Width-This.BrdWidth
                aRPoints[2,2]=This.BrdWidth
                *aRPoints[2,3]=.T.
                aRPoints[3,1]=This.Width-This.BrdWidth
                aRPoints[3,2]=This.Height-This.BrdWidth
                *aRPoints[3,3]=.T.
                aRPoints[4,1]=This.BrdWidth
                aRPoints[4,2]=This.Height-This.BrdWidth
                *aRPoints[4,3]=.T.

        Do Case
            Case m.cType = "N"
                This.cPictureVal=cb_DrawMediaButton(This, This.nAlpha, This.nRadius, This.nOffsetX, This.nOffsetY, @aRPoints, @aLPoints, @aBPoints, @aAPoints, bCloseFigure)
            Case m.cType = "H"
                This.cPictureValMouseHover=cb_DrawMediaButton(This, This.nAlphaH, This.nRadius, This.nOffsetX, This.nOffsetY, @aRPoints, @aLPoints, @aBPoints, @aAPoints, bCloseFigure)
            Case m.cType = "D"
                This.cPictureValMouseDown=cb_DrawMediaButton(This, This.nAlphaD, This.nRadius, This.nOffsetX, This.nOffsetY, @aRPoints, @aLPoints, @aBPoints, @aAPoints, bCloseFigure)
            Case m.cType = "A"
                Return cb_DrawMediaButton(This, 255, This.nRadius, This.nOffsetX, This.nOffsetY, @aRPoints, @aLPoints, @aBPoints, @aAPoints, bCloseFigure)
        Endcase
    Endproc

    Procedure OpenGest(pCmd, cAzi, pKey1, pKey2, pKey3, pKey4, pKey5, pKey6, pKey7, pKey8, pKey9, pKey10)
        Local i_cmd
        Local i_o,i_i,i_k,i_v,i_p,l_len
        i_cmd=m.pCmd
        If Not(Empty(i_cmd))
            If Empty(cAzi) Or Trim(cAzi)=Trim(i_codazi)
                * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
                * Inserito ramo Else
                If Right(i_cmd,1)<>')'
                    * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
                    i_o=&i_cmd()
                    * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
                Else
                    i_o=&i_cmd
                Endif
                * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
                If !Isnull(i_o) And Type("i_o.cFile")="C"
                    If Used(i_o.cKeySet)
                        i_o.ecpFilter()
                        * --- carica il record specificato
                        i_k=i_o.ckeyselect
                        l_len = Alines(xKey, i_k, ",")
                        For i_i=1 To l_len
                            i_v = m.xKey[i_i]
                            i_k = "pKey"+Alltrim(Str(i_i))
                            i_o.w_&i_v = &i_k
                        Next
                        i_o.ecpSave()
                        *
                    Endif
                Endif
            Else
                cp_ErrorMsg(cp_MsgFormat(MSG_BUTTON_LINKED_TO_COMPANY__,This.cAzi),"!",cp_Translate(MSG_POSTIN_ADMIN),.F.)
            Endif
        Endif
    Endproc

Enddefine

Define Class deferredmaskexec As Timer
    Interval=100
    cFile=''
    oThis=.Null.
    Proc Init(cFile)
        This.cFile=cFile
        This.oThis=This
    Proc Timer()
        _Screen.LockScreen=.T.
        _Screen.RemoveObject('cnt')
        VM_EXEC(This.cFile)
        _Screen.LockScreen=.F.
        This.oThis=.Null.
Enddefine

Define Class VfmManager As Custom
    nIndex=0
    Add Object aVfmList As Collection
    bGoto = .F.  &&Indica se sto aprendo una vfm attraverso l'opzione Goto

    Procedure AddItem(cFile)
        With This
            If !.bGoto
                If .nIndex<.aVfmList.Count
                    Local l_nIndex
                    l_nIndex = .nIndex + 1
                    If Upper(.aVfmList.Item(m.l_nIndex))<>Upper(m.cFile)
                        For i=.aVfmList.Count To l_nIndex Step -1
                            .aVfmList.Remove(.aVfmList.Count)
                        Endfor
                        .aVfmList.Add(m.cFile)
                        .nIndex = .aVfmList.Count
                    Else
                        .nIndex	= l_nIndex
                    Endif
                Else
                    .aVfmList.Add(m.cFile)
                    .nIndex = .aVfmList.Count
                Endif
            Endif
            .bGoto=.F.
        Endwith
    Endproc
    Procedure Goto(pnIndex)
        With This
            .bGoto=.T.
            .nIndex = Min(Max(m.pnIndex,1),.aVfmList.Count)
            VM_EXEC(.aVfmList.Item(.nIndex))
        Endwith
    Endproc

    Procedure DrillUp()
        This.Goto(This.nIndex-1)
    Endproc

    Procedure DrillDown()
        This.Goto(This.nIndex+1)
    Endproc

    Procedure Home()
        This.Goto(1)
    Endproc

Enddefine

Define Class vm_frm_line_image As Image
    cVarName = '.-.'
    Func GetValue()
        Return('')
    *--- DrawLine
    Procedure DrawLines()
    	LOCAL Obj, i, l_idx
    	DIMENSION aNode[1]
        For Each Obj In This.Parent.Controls
			IF LOWER(Obj.Class)=="draw_linenode_exec" AND Obj.cRoot==Obj.Name
				aNode[ALEN(aNode,1)]=Obj
				DIMENSION aNode(ALEN(aNode,1)+1)
			Endif
        NEXT
        IF ALEN(aNode,1)>1
        	This.Width=This.Parent.Width
        	This.Height=This.Parent.Height
        	l_idx= 0
        	FOR i=1 TO ALEN(aNode,1)-1
	       		l_idx = l_idx + 1
	       		DIMENSION aMBLines(l_idx, 13) 
        		aMBLines[l_idx, 1]=aNode[i].Left+aNode[i].Width/2
        		aMBLines[l_idx, 2]=aNode[i].Top+aNode[i].Height/2
        		aMBLines[l_idx, 3]=aNode[i].nBorderColor
        		aMBLines[l_idx, 4]=aNode[i].nBorderWidth
        		aMBLines[l_idx, 5]=aNode[i].nStartCap
        		aMBLines[l_idx, 6]=aNode[i].nEndCap		
        		aMBLines[l_idx, 7]=aNode[i].nDashStyle
        		aMBLines[l_idx, 8]=aNode[i].nCurvature
        		aMBLines[l_idx, 9]=IIF(TYPE("aNode[i].nSCAngle")<>"U",aNode[i].nSCAngle,IIF(aNode[i].nStartCap=3,60,32))
        		aMBLines[l_idx, 10]=IIF(TYPE("aNode[i].nSCSize")<>"U",aNode[i].nSCSize,IIF(aNode[i].nStartCap=3,20,15))
        		aMBLines[l_idx, 11]=IIF(TYPE("aNode[i].nECAngle")<>"U",aNode[i].nECAngle,IIF(aNode[i].nEndCap=3,60,32))
        		aMBLines[l_idx, 12]=IIF(TYPE("aNode[i].nECSize")<>"U",aNode[i].nECSize,IIF(aNode[i].nEndCap=3,20,15))		
        		oNode = aNode[i]
        		DO WHILE !EMPTY(oNode.cNext)
        			cNext = oNode.cNext
        			oNode = This.Parent.&cNext
        			l_idx = l_idx + 1
					DIMENSION aMBLines(l_idx, 13)
        			aMBLines[l_idx, 1]=oNode.Left+oNode.Width/2
        			aMBLines[l_idx, 2]=oNode.Top+oNode.Height/2
	        		aMBLines[l_idx, 3]=oNode.nBorderColor
	        		aMBLines[l_idx, 4]=oNode.nBorderWidth
	        		aMBLines[l_idx, 5]=oNode.nStartCap
	        		aMBLines[l_idx, 6]=oNode.nEndCap		
	        		aMBLines[l_idx, 7]=oNode.nDashStyle
	        		aMBLines[l_idx, 8]=oNode.nCurvature
        		    aMBLines[l_idx, 9]=IIF(TYPE("aNode[i].nSCAngle")<>"U",aNode[i].nSCAngle,IIF(aNode[i].nStartCap=3,60,32))
        	    	aMBLines[l_idx, 10]=IIF(TYPE("aNode[i].nSCSize")<>"U",aNode[i].nSCSize,IIF(aNode[i].nStartCap=3,20,15))
            		aMBLines[l_idx, 11]=IIF(TYPE("aNode[i].nECAngle")<>"U",aNode[i].nECAngle,IIF(aNode[i].nEndCap=3,60,32))
        	    	aMBLines[l_idx, 12]=IIF(TYPE("aNode[i].nECSize")<>"U",aNode[i].nECSize,IIF(aNode[i].nEndCap=3,20,15))       			
        		ENDDO
       			aMBLines[l_idx, 13]=.T.	        		
        	Next
        	This.PictureVal=cb_DrawMediaButtonLines(@aMBLines, This.Width, This.Height)
        	This.Visible=.T.
        Endif
    Endproc
ENDDEFINE
