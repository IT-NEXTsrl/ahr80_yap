* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VZ_BUILD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Disegnatore di zoom
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

* --- Zucchetti aulla inizio Framework monitor
If cp_set_get_right()<2
    cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
    Return
Endif
* --- Zucchetti aulla fine Framework monitor

If At('vq_lib',Lower(Set('proc')))=0 Or At('cp_zoom',Lower(Set('proc')))=0
    Set Proc To vq_lib,cp_zoom Additive
Endif
o=Createobject('zoom_toolbar',Vartype(_Screen.cp_ThemesManager)=="O" And i_VisualTheme<>-1)
i_curform=o
* --- Zucchetti aulla inizio Framework monitor
If (Type('i_designfile')='C')
    o.cFileName = Alltrim(i_designfile)
    o.SetTitle()
    o.LoadDoc()
Endif
* --- Zucchetti aulla fine Framework monitor
o.EditZoom()
Return

Define Class zoom_toolbar As microtool_toolbar
    oForm=.Null.
    oThis=.Null.
    cExt='vzm'
    Caption=''
    cToolName='Visual Zoom'
    *
    bSec1=.T.
    bSec2=.T.
    bSec3=.T.
    bSec4=.T.
    *
    bFox26=.F.

    Proc Init(bStyle)
        DoDefault(m.bStyle)
        With This
            *--- Separatore
            .AddObject("sep1","Separator")
            #If Version(5)>=900
                .sep1.Visible=.T.
            #Endif
            *--- File Btn
            Local oBtn
            m.oBtn = .AddButton("fil")
            m.oBtn.Caption = ""&&cp_Translate(MSG_TABLE)
            m.oBtn.cImage = "data_table.bmp"
            m.oBtn._OnClick = "This.parent.fil_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_MAIN_TABLE)
            m.oBtn.Visible=.T.
            *--- Opt Btn
            m.oBtn = .AddButton("opt")
            m.oBtn.Caption = ""&&cp_Translate(MSG_OPT_F)
            m.oBtn.cImage = "opz.bmp"
            m.oBtn._OnClick = "This.parent.opt_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_OPTIONS)
            m.oBtn.Visible=.T.
            If Type("i_bFox26")='L'
                If i_bFox26
                    This.bFox26=.T.
                Endif
            Endif
            If !.bFox26
                cp_GetSecurity(This,'VisualZom')
            Endif
            .oForm=Createobject('vz_frm')
            .oForm.oTool=This
            .Dock(i_nCPToolBarPos)
            .oThis=This
            .Caption=cp_Translate(MSG_DRAW)

            .cToolName=cp_Translate(MSG_VISUAL_ZOOM)
        Endwith
    Proc EditZoom()
        If !This.bSec1
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            This.Exit()
        Else
            This.Show()
            This.oForm.Show()
        Endif
        Return
    Proc Exit()
        If i_AdvancedHeaderZoom And Vartype(This.oForm.Zoom.oRefHeaderObject)='O'
            This.oForm.RemoveObject(This.oForm.Zoom.oRefHeaderObject.Name)
        Endif
        This.oForm=.Null.
        This.oThis=.Null.
        i_curform=.Null.
        This.Visible=.F.
        This.Destroy()
        Return
    Func GetHelpTopic()
        Return('vz_build')
    Proc NewDoc()
        If i_AdvancedHeaderZoom And Vartype(This.oForm.Zoom.oRefHeaderObject)='O'
            This.oForm.RemoveObject(This.oForm.Zoom.oRefHeaderObject.Name)
        Endif
        This.oForm.New()
        This.fil.Enabled=.T.
        Return
    Proc LoadDoc()
        Local xcg,c,h,F,l
        * --- nuovo documento
        *this.oForm.New()
        If i_AdvancedHeaderZoom And Vartype(This.oForm.Zoom.oRefHeaderObject)='O'
            This.oForm.RemoveObject(This.oForm.Zoom.oRefHeaderObject.Name)
            This.oForm.RemoveObject("__unlockscreen")
            This.oForm.oTool=Null
        Endif
        c=cp_GetPFile(This.cFileName)
        xcg=Createobject('pxcg','load',c)
        This.Serialize(xcg)
        This.bModified=.F.
        This.fil.Enabled=.F.
        This.Caption=cp_Translate(MSG_DRAW)
	Proc SaveDoc()
		Local xcg,h
		cp_CreateCacheFile()
		this.oForm.zoom.SaveColumnStatus()
		this.oForm.zoom.query(this.oForm.zoom.o_initsel)
		xcg=Createobject('pxcg','save')
		This.Serialize(xcg)
		This.bModified=.F.
		h=Fcreate(This.cFileName)
		=Fwrite(h,xcg.cText)
		=Fclose(h)
    Proc Serialize(xcg)
        This.oForm.LockScreen=.T.
        This.oForm.Serialize(xcg)
        This.oForm.LockScreen=.F.
    Proc fil_click()
        Local o
        o=Createobject('dcx_select_table')
        If Vartype(i_MenuToolbar)='O' And i_VisualTheme<>-1
            *--- Disabilito il menu
            i_MenuToolbar.ShowModalWnd = i_MenuToolbar.ShowModalWnd + 1
        Endif
        o.Show()
        If Vartype(i_MenuToolbar)='O' And i_VisualTheme<>-1
            *--- Disabilito il menu
            i_MenuToolbar.ShowModalWnd = i_MenuToolbar.ShowModalWnd - 1
        Endif
        If !Isnull(o) And o.nChoosed<>0
            Local N
            This.fil.Enabled=.F.
            N=i_dcx.GetTable(o.nChoosed)
            This.oForm.Zoom.cKeyFields=cp_KeyToSQL(i_dcx.GetIdxDef(N,1))
            This.oForm.Zoom.SetFileAndFields(N,'*',.F.,.F.)
            Local cObjManyHeaderName
            If i_AdvancedHeaderZoom And Vartype(This.oForm.Zoom.oRefHeaderObject)<>"O"
                cObjManyHeaderName="oManyHeader_"+This.oForm.Zoom.Name
                This.oForm.Zoom.Parent.AddObject(cObjManyHeaderName,"ManyHeader")
                This.oForm.Zoom.oRefHeaderObject=This.oForm.Zoom.Parent.&cObjManyHeaderName
                This.oForm.Zoom.oRefHeaderObject.InitHeader(This.oForm.Zoom.grd,.T.)
            Endif
            This.oForm.Zoom.Query()
        Endif
        Return
    Proc opt_Click()
        Local o
        o=Createobject('vz_opt_form',This)
        If Vartype(i_MenuToolbar)='O' And i_VisualTheme<>-1
            *--- Disabilito il menu
            i_MenuToolbar.ShowModalWnd = i_MenuToolbar.ShowModalWnd + 1
        Endif
        o.Show()
        If Vartype(i_MenuToolbar)='O' And i_VisualTheme<>-1
            *--- Disabilito il menu
            i_MenuToolbar.ShowModalWnd = i_MenuToolbar.ShowModalWnd - 1
        Endif
    Endproc
    Proc ecpSecurity()
        Createobject('stdsProgAuth','VisualZom')
        Return
Enddefine

Define Class vz_frm As CpSysform
    Width=400
    Height=250
    Closable=.F.
    oTool=.Null.
    Caption=""
    Icon=i_cBmpPath+i_cStdIcon
    cCPPrg=''
    bOptions=.T.
    ShowTips=.T.
    Add Object __sqlx__ As sqlx
    Add Object Zoom As zoombox With Height=240,Width=390,BorderWidth=0

	*--- Zucchetti Aulla Inizio - Form decorator
	bApplyFormDecorator=.t.	
	*-- Zucchetti Aulla Fine - Form decorator    
    Proc New()
        This.Top=0
        This.Left=0
        This.Width=400
        This.Height=250
        This.Caption=cp_Translate(MSG_DRAW)
        This.Zoom.Width=390
        This.Zoom.Height=240
        This.Zoom.grd.ColumnCount=0
        This.Zoom.Destroy()
        This.Zoom.cSymFile=''
        *--- Zucchetti Aulla Inizio - Abilito bottoni stdzrep
        This.Zoom.cZoomName=''
        *--- Zucchetti Aulla Fine - Abilito bottoni stdzrep
        *proc drw.SetModified(bStatus)
        *  this.parent.oTool.bModified=bStatus
        *  DoDefault(bStatus)
    Proc Resize()
        This.Zoom.Width=This.Width
        This.Zoom.Height=This.Height
        If Vartype(This.oTool)='O'
            This.oTool.bModified=.T.
        Endif
    Proc Serialize(xcg)
        If xcg.bIsStoring
            xcg.Save('N',This.Top)
            xcg.Save('N',This.Left)
            xcg.Save('N',This.Width)
            xcg.Save('N',This.Height)
            xcg.Save('C',This.Caption)
            xcg.Save('C',This.cCPPrg)
            xcg.Save('L',This.bOptions)
            xcg.Eoc()
            This.Zoom.Serialize(xcg)
        Else
            This.Top=xcg.Load('N',0)
            This.Left=xcg.Load('N',0)
            This.Width=xcg.Load('N',0)
            This.Height=xcg.Load('N',0)
            This.Caption=xcg.Load('C','Zoom')
            This.cCPPrg=xcg.Load('C','')
            This.bOptions=xcg.Load('L',.T.)
            xcg.Eoc()
            This.Zoom.Serialize(xcg)
            *this.zoom.bOptions=this.bOptions
            *this.zoom.bAdvanced=this.bAdvOptions
            This.Zoom.Query()
        Endif
        If i_AdvancedHeaderZoom And Vartype(This.Zoom.oRefHeaderObject)<>"O"
            cObjManyHeaderName="oManyHeader_"+This.Zoom.Name
            This.AddObject(cObjManyHeaderName,"ManyHeader")
            This.Zoom.oRefHeaderObject=This.&cObjManyHeaderName
            This.Zoom.oRefHeaderObject.InitHeader(This.Zoom.grd,.T.)
        Endif
    Proc Init()
        This.Caption=cp_Translate(MSG_DRAW)
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
    	*--- Form Decorator
    	IF This.bApplyFormDecorator And i_ThemesManager.GetProp(123)=0
    		This.AddObject("oDec", "cp_FormDecorator")	
        ELSE
            This.bApplyFormDecorator = .F.    		
    	ENDIF	
        This.Zoom.oButtonOption.bVisibleOption=.F.
        This.Zoom.oButtonOption.Click()
        *--- Zucchetti Aulla Inizio - Abilito bottoni stdzrep
        This.Zoom.cZoomName = This.Zoom.cZoomName
        *--- Zucchetti Aulla Fine - Abilito bottoni stdzrep
    Endproc
Enddefine

Define Class vz_opt_form As CpSysform
    WindowType=1
    oObj=.Null.
    Top=43
    Left=130
    Width=391
    Height=105
    Caption= ""
    Add Object lbl1 As Label With Caption="",Alignment=1,Top=13,Left=42,Width=50,Height=20, BackStyle = 0
    Add Object lbl2 As Label With Caption="",Alignment=1,Top=35,Left=18,Width=74,Height=18, BackStyle = 0
    Add Object tit As TextBox With Top=8,Left=98,Width=189,Height=22
    Add Object prg As TextBox With Top=33,Left=98,Width=111,Height=22
    Add Object opt As Checkbox With Top=60,Left=98,Width=111,Height=22,Caption="", BackStyle = 0
    Add Object btnok As CommandButton With Caption="",Width=50,Height=25
    Add Object btncan As CommandButton With Caption="",Width=50,Height=25
    Proc Init(oObj)
        This.oObj=oObj
        This.LoadValues()
        This.btnok.Top=This.Height-30
        This.btnok.Left=This.Width-110
        This.btncan.Top=This.Height-30
        This.btncan.Left=This.Width-55
        This.Caption=cp_Translate(MSG_OPTIONS)
        This.lbl1.Caption=cp_Translate(MSG_TITLE+MSG_FS)
        This.lbl2.Caption=cp_Translate(MSG_PROGRAM+MSG_FS)
        This.opt.Caption=cp_Translate(MSG_OPTIONS_BUTTON)
        This.btnok.Caption=cp_Translate(MSG_OK_BUTTON)
        This.btncan.Caption=cp_Translate(MSG_CANCEL_BUTTON)
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
    Proc LoadValues()
        This.tit.Value=This.oObj.oForm.Caption
        This.prg.Value=This.oObj.oForm.cCPPrg
        This.opt.Value=This.oObj.oForm.bOptions
        Return
    Proc SaveValues()
        This.oObj.oForm.Caption=Trim(This.tit.Value)
        This.oObj.oForm.cCPPrg=Trim(This.prg.Value)
        This.oObj.oForm.bOptions=This.opt.Value
        Return
    Proc btnok.Click()
        Thisform.SaveValues()
        Thisform.Hide()
    Proc btncan.Click()
        Thisform.Hide()
Enddefine
