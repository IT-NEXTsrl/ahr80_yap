* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_DESK
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 02/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Oggetti per la gestione del desktop
* ----------------------------------------------------------------------------
* --- verifica l' esistenza di postit e persist

#include "cp_app_lang.inc"

Public odesktopbar
If At('vq_lib',Lower(Set('proc')))=0
    Set Proc To vq_lib Additive
Endif
odesktopbar=Createobject('DeskTopBar', i_VisualTheme<>-1 And Vartype(_Screen.cp_ThemesManager)=="O")
odesktopbar.Visible = i_bShowDeskTopBar and !i_bToolBarDisappear
If i_VisualTheme <> -1
    odesktopbar.Customize = i_bShowDeskTopBar
    odesktopbar.SetAfterCpToolbar()
Endif

If Vartype(i_bDisablePostIn)="U" Or !i_bDisablePostIn
    If i_DatSysPostInFilterDate
        i_dPostInFilterDate = Datetime()
    Else
        i_dPostInFilterDate = i_DatSys+Val(Sys(2))
    Endif
    Do LoadPostIT
    Public opostittimer
    opostittimer=Createobject('postitbiff')
Endif
Return

Define Class DeskTopBar As CommandBar
    cOldIcon=''
    Width=200
    heigth=30
    Visible=.F.
    Procedure Init(bStyle)
        DoDefault(m.bStyle)
        Local oBtn
        With This
            .Caption='Application bar'
            This.ControlBox = .T.
            *--- Postit Btn
            m.oBtn = .AddButton("postitbtn")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "postit.bmp"
            m.oBtn._OnClick = "This.parent.postitbtn_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_CREATE_POSTIN)
            m.oBtn.Visible=(i_ab_btnpostin And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,1,1)<>'N'
            *--- PostitFolder Btn
            m.oBtn = .AddButton("postitfolder")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "folder_postit.bmp"
            m.oBtn._OnClick = "This.parent.postitfolder_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_POSTIN_FOLDER)
            m.oBtn.Visible=(i_ab_btnpostin And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,2,1)<>'N'
            *--- PostitBiff Btn
            m.oBtn = .AddButton("postitbiff")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "maildn.bmp"
            m.oBtn._OnClick = "opostittimer.GetMail()"
            m.oBtn.ToolTipText = cp_Translate(MSG_CHECK_MAIL)
            m.oBtn.Visible=(Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,3,1)<>'N'
            *--- PostitUsers Btn
            m.oBtn = .AddButton("postitusers")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "user.bmp"
            m.oBtn._OnClick = "cp_grpusr()"
            m.oBtn.ToolTipText = cp_Translate(MSG_USERS)
            m.oBtn.Visible=(i_ab_btnuser And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,4,1)<>'N'
            * --- Zucchetti Aulla inizio bottoni personalizzati
            *--- BtnWfld Btn
            m.oBtn = .AddButton("BtnWfld")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "bmp\wfld.Bmp"
            m.oBtn._OnClick = "this.parent.BtnWfld_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_FLOW_AND_PERMISSIONS)
            m.oBtn.Visible=(g_APPLICATION <> "ADHOC REVOLUTION" And g_WFLD='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,5,1)<>'N'
            *--- AhiRevolution Btn
            m.oBtn = .AddButton("AhriRevolution")
            m.oBtn.Caption = ''
			m.oBtn.cImage = "bmp\fixinf.Bmp"
            m.oBtn._OnClick = "this.parent.ahiRevolution_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_IREVOLUTION)
            m.oBtn.Visible=(g_REVI='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,17,1)<>'N'
            *--- Ahvisualizza Btn
            m.oBtn = .AddButton("ahvisualizza")
            m.oBtn.Caption = ''
			m.oBtn.cImage = "bmp\visualicat"+iif(g_DMIP='S',"_infinityproject","")+".Bmp"
            m.oBtn._OnClick = "this.parent.ahvisualizza_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_SHOW_DOCUMENTS)
            m.oBtn.Visible=(g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,6,1)<>'N'
            *--- AhvisualizzaPratica Btn
            If Isalt()
                m.oBtn = .AddButton("ahvisualizzaprat")
                m.oBtn.Caption = ''
                m.oBtn.cImage = "bmp\VisualPra.ico"
                m.oBtn._OnClick = "this.parent.ahvisualizzaprat_click()"
                m.oBtn.ToolTipText = cp_Translate(MSG_SHOW_PRAT_DOCUMENTS)
                m.oBtn.Visible=(g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,7,1)<>'N'
            Endif
            *--- ahnuovodoc Btn
            m.oBtn = .AddButton("ahnuovodoc")
            m.oBtn.Caption = ''
			m.oBtn.cImage = "bmp\folder_edit"+iif(g_DMIP='S',"_infinityproject","")+".Bmp"
            m.oBtn._OnClick = "this.parent.ahnuovodoc_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_NEW_DOCUMENT)
			m.oBtn.Visible=(g_DMIP<>'S' and g_DOCM='S' and not ((g_JBSH='S' OR g_IZCP='S') AND g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,8,1)<>'N'
            *--- aharchivia Btn
            m.oBtn = .AddButton("aharchivia")
            m.oBtn.Caption = ''
			m.oBtn.cImage = "bmp\zarchivia"+iif(g_DMIP='S',"_infinityproject","")+".Bmp"
            m.oBtn._OnClick = "this.parent.aharchivia_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_FAST_ARCHIVING)
            m.oBtn.Visible=(g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,9,1)<>'N'
            *--- aharchivia_infinity Btn
            IF NOT isalt()
	            m.oBtn = .AddButton("aharchivia_infinity")
	            m.oBtn.Caption = ''
	            m.oBtn.cImage = "bmp\barcode"+iif(g_DMIP='S',"_infinityproject","")+".Bmp"
	            m.oBtn._OnClick = "this.parent.aharchivia_click('B')"
	            m.oBtn.ToolTipText = cp_Translate(MSG_FAST_ARCHIVING_INFINITY)
	            m.oBtn.Visible=Not((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) and SUBSTR(g_AB_VIEWBUTTON,10,1)<>'N'
            endif
            *--- ahscanner Btn ********************
            m.oBtn = .AddButton("ahscanner")
            m.oBtn.Caption = ''
            m.oBtn.ToolTipText = cp_Translate(MSG_FAST_ARCHIVING_SCANNER)
			m.oBtn.cImage = "bmp\scanner"+iif(g_DMIP='S',"_infinityproject","")+".Bmp"
			If g_DMIP='S'
				m.oBtn._OnClick="odesktopbar.aharchivia_click('U')"
			Else
				If i_VisualTheme = -1
					m.oBtn._OnClick = "this.parent.aharchivia_click('S')"
				Else
					Local oBtnMenu, oMenuItem
					m.oBtnMenu = Createobject("cbPopupMenu")
					m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
					m.oMenuItem = m.oBtnMenu.AddMenuItem()
					m.oMenuItem.Caption = Ah_Msgformat('Acquisizione singola')
					m.oMenuItem.Picture = "bmp\scanner.ico"
					m.oMenuItem.OnClick = "odesktopbar.aharchivia_click('U')"
					m.oMenuItem.Visible=.T.
					m.oMenuItem = m.oBtnMenu.AddMenuItem()
					m.oMenuItem.Caption = Ah_Msgformat('Acquisizione multipla')
					m.oMenuItem.OnClick = "odesktopbar.aharchivia_click('M')"
					m.oMenuItem.Visible=.T.
					m.oBtnMenu.InitMenu()
					m.oBtn.oPopupMenu = m.oBtnMenu
					m.oBtn.nButtonType = 1
				Endif
			Endif
            m.oBtn.Visible=(g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,11,1)<>'N'
            ************************************
            *--- attributi Btn
            m.oBtn = .AddButton("attributi")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "bmp\tattrib.Bmp"
            m.oBtn._OnClick = "this.parent.Attributi_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_ATTRIBUTE_ASSOCIATION)
            m.oBtn.Visible=(g_APPLICATION <> "ADHOC REVOLUTION"And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,12,1)<>'N'
            *--- inforeader Btn
            m.oBtn = .AddButton("inforeader")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "bmp\IReader.Bmp"
            m.oBtn._OnClick = "this.parent.inforeader_click()"
            m.oBtn.ToolTipText = 'InfoReader'
            m.oBtn.Visible=(g_IRDR='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,13,1)<>'N'
            *--- setupActLog schedjob Btn
            m.oBtn = .AddButton("stpactlogger")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "bmp\log.Bmp"
            m.oBtn._OnClick = "this.parent.stpactlogger_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_SETUP_ACT_LOGGER)
            m.oBtn.Visible=((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)
            *--- schedjob Btn
            m.oBtn = .AddButton("schedjob")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "bmp\suggerito.Bmp"
            m.oBtn._OnClick = "this.parent.schedjob_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_JOB_SCHEDULATOR)
            m.oBtn.Visible=((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)
            *--- boxsched TextBox
            .AddObject("boxsched", "TextBox")
            m.oBtn = .boxsched
            m.oBtn.Visible=((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)
            m.oBtn.ToolTipText = cp_Translate(MSG_ENABLED_SCH)
            m.oBtn.Enabled=.F.
            *--- cfgGest Btn
            m.oBtn = .AddButton("cfgGest")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "bmp\FOLDERCL.Bmp"
            m.oBtn._OnClick = "this.parent.cfgGest_click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_SETTINGS)
            m.oBtn.Visible=(Vartype(g_bDisableCfgGest)="U" Or !g_bDisableCfgGest) And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) And Not Isalt() and SUBSTR(g_AB_VIEWBUTTON,14,1)<>'N'
            *--- agenda Btn
            m.oBtn = .AddButton("agenda")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "bmp\ScadT.bmp"
            m.oBtn._OnClick = "GSAG_KAG('P')"
            m.oBtn.ToolTipText = cp_Translate(MSG_MY_AGENDA)
            m.oBtn.Visible=g_AGEN="S" And (Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,15,1)<>'N'
            m.oBtn = .Null.
            *--- Timer Btn
            m.oBtn = .AddButton("Timer")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "bmp\timer.bmp"
            m.oBtn._OnClick = "GSAG_KTM()"
            m.oBtn.ToolTipText = cp_Translate(MSG_TIMER)
            m.oBtn.Visible=g_AGEN="S" And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) And Isalt() and SUBSTR(g_AB_VIEWBUTTON,16,1)<>'N'
            m.oBtn = .Null.
            * --- Zucchetti Aulla fine bottoni personalizzati
            Local xcg, nDimension
            If i_ServerConn[1,2]<>0
                =cp_SQL(i_ServerConn[1,2],"select persist from persist where code='desktopbar' and usercode="+cp_ToStr(i_codute),'persist_c')
            Else
                Select persist From persist Where Code='desktopbar' And usercode=i_codute Into Cursor persist_c
            Endif
            If Used('persist_c')
                If Reccount()>=1
                    *--- Se ho salvato dei bottoni nella toolbar e passo da un tema std a uno non std;
                    *--- devo modificare il nome della classe
                    Local l_TextPersist
                    l_TextPersist = persist_c.persist
                    If i_VisualTheme<>-1
                        l_TextPersist = Strtran(l_TextPersist, "desktopbutton ", "cb_desktopbutton_dummy ", 1, -1, 1)
                    Else
                        l_TextPersist = Strtran(l_TextPersist, "cb_desktopbutton_dummy ", "desktopbutton ", 1, -1, 1)
                    Endif
                    xcg=Createobject('pxcg','load', m.l_TextPersist)
                    This.Serialize(xcg)
                Endif
                Use
            Endif
            This.Dock(i_nDeskTopBarPos,oCpToolBar.Width+10,10)
            If Vartype(i_bDisablePostIn)="L" And i_bDisablePostIn
                This.postitbtn.Enabled=.F.
                This.postitfolder.Enabled=.F.
                This.postitbiff.Enabled=.F.
            Endif
            *--- Zucchetti Aulla Inizio - Schedulatore
            If(g_UserScheduler)
                Public oschedTimer
                oschedTimer=Createobject('schedTimer')
            Endif
            *--- Zucchetti Aulla Fine - Schedulatore
            *--- Zucchetti Aulla Inizio - Tendina ricerca dei recenti
            If i_bRecentMenu_toolbar
                If Vartype(cCursRecen)<>"C"
                    Public cCursRecen
                    cCursRecen=""
                Endif
                If Empty(cCursRecen)
                    cCursRecen = Sys(2015)
                Endif
                .AddObject("oRecentMenu", "SearchRecent", cCursRecen, "TOOLBAR" )
                .oRecentMenu.Visible=.T.
                * CREO IL CURSORE DEI RECENTI
                If Not Used(cCursRecen) Or Recno(cCursRecen)=0
                    GSUT_BMR('C')
                Endif
            Endif
            *--- Zucchetti Aulla Fine - Tendina ricerca dei recenti
        Endwith
    Endproc
    Procedure SetAfterCpToolbar
        * --- trucco per far stare le due tool bar sulla stessa riga
        If This.Visible
			If i_nDeskTopBarPos = i_nCPToolBarPos AND i_nDeskTopBarPos<>-1
            Local nCount
            nCount = 0
            This.Dock(-1)
				if i_nDeskTopBarPos = 1 OR i_nDeskTopBarPos = 2
					This.Dock(i_nDeskTopBarPos,oCpToolBar.height,oCpToolBar.Left)
					Do While (This.Top<oCpToolBar.Height+oCpToolBar.Top Or This.Left>oCpToolBar.Left) And m.nCount <= 5
						This.Dock(i_nDeskTopBarPos,oCpToolBar.height,oCpToolBar.Left)
						nCount = m.nCount + 1
					Enddo
				Else
					This.Dock(i_nDeskTopBarPos,oCpToolBar.Width,oCpToolBar.Top)
            Do While (This.Left<oCpToolBar.Width+oCpToolBar.Left Or This.Top>oCpToolBar.Top) And m.nCount <= 5
						This.Dock(i_nDeskTopBarPos,oCpToolBar.Width,oCpToolBar.Top)
                nCount = m.nCount + 1
            Enddo
        Endif
			Else
				odesktopbar.Dock( i_nDeskTopBarPos )
			Endif
        Endif
    Endproc
	Procedure SetVisible()
        With This
            *--- Postit Btn
            .postitbtn.Visible=(i_ab_btnpostin And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,1,1)<>'N'
            *--- PostitFolder Btn
            .postitfolder.Visible=(i_ab_btnpostin And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,2,1)<>'N'
            *--- PostitBiff Btn
            .postitbiff.Visible=(Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,3,1)<>'N'
            *--- PostitUsers Btn
            .postitusers.Visible=(i_ab_btnuser And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,4,1)<>'N'
            *--- BtnWfld Btn
            .BtnWfld.Visible=(g_APPLICATION <> "ADHOC REVOLUTION" And g_WFLD='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,5,1)<>'N'
            *--- Ahvisualizza Btn
            .ahvisualizza.Visible=(g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,6,1)<>'N'
			If Isalt()
				*--- Ahvisualizzaprat Btn
				.ahvisualizzaprat.Visible=(g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,7,1)<>'N'		
			Endif
            *--- ahnuovodoc Btn
            .ahnuovodoc.Visible=(g_DMIP<>'S' and g_DOCM='S' and not ((g_JBSH='S' OR g_IZCP='S') AND g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,8,1)<>'N'
            *--- aharchivia Btn
            .aharchivia.Visible=(g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,9,1)<>'N'
            If .f. and NOT isalt()
				*--- aharchivia_infinity Btn
				.aharchivia_infinity.Visible=Not((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) and SUBSTR(g_AB_VIEWBUTTON,10,1)<>'N'	
			Endif
            *--- ahscanner Btn
            .ahscanner.Visible=(g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,11,1)<>'N'
            *--- attributi Btn
            .attributi.Visible=(g_APPLICATION <> "ADHOC REVOLUTION" And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,12,1)<>'N'
            *--- inforeader Btn
            .inforeader.Visible=(g_IRDR='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,13,1)<>'N'
            *--- cfgGest Btn
            .cfgGest.Visible=(Vartype(g_bDisableCfgGest)="U" Or !g_bDisableCfgGest) And Not((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) And Not Isalt() and SUBSTR(g_AB_VIEWBUTTON,14,1)<>'N'
            *--- agenda Btn
            .agenda.Visible=g_AGEN="S" And (Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and SUBSTR(g_AB_VIEWBUTTON,15,1)<>'N'
            *--- Timer Btn
            .Timer.Visible=g_AGEN="S" And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) And Isalt() and SUBSTR(g_AB_VIEWBUTTON,16,1)<>'N'			
        Endwith	
	Endproc
    *--- Traduzione tooltip
    Procedure LocalizeString
        With This
            *--- Postit Btn
            .postitbtn.ToolTipText = cp_Translate(MSG_CREATE_POSTIN)
            *--- PostitFolder Btn
            .postitfolder.ToolTipText = cp_Translate(MSG_POSTIN_FOLDER)
            *--- PostitBiff Btn
            .postitbiff.ToolTipText = cp_Translate(MSG_CHECK_MAIL)
            *--- PostitUsers Btn
            .postitusers.ToolTipText = cp_Translate(MSG_USERS)
            *--- BtnWfld Btn
            .BtnWfld.ToolTipText = cp_Translate(MSG_FLOW_AND_PERMISSIONS)
            *--- Ahvisualizza Btn
            .ahvisualizza.ToolTipText = cp_Translate(MSG_SHOW_DOCUMENTS)
            *--- ahnuovodoc Btn
            .ahnuovodoc.ToolTipText = cp_Translate(MSG_NEW_DOCUMENT)
            *--- aharchivia Btn
            .aharchivia.ToolTipText = cp_Translate(MSG_FAST_ARCHIVING)
            *--- ahscanner Btn
            .ahscanner.ToolTipText = cp_Translate(MSG_FAST_ARCHIVING_SCANNER)
            *--- attributi Btn
            .attributi.ToolTipText = cp_Translate(MSG_ATTRIBUTE_ASSOCIATION)
            *--- inforeader Btn
            .inforeader.ToolTipText = 'InfoReader'
            *--- schedjob Btn
            .schedjob.ToolTipText = cp_Translate(MSG_JOB_SCHEDULATOR)
            *--- boxsched TextBox
            .boxsched.ToolTipText = cp_Translate(MSG_ENABLED_SCH)
            *--- cfgGest Btn
            .cfgGest.ToolTipText = cp_Translate(MSG_SETTINGS)
            *--- agenda Btn
            .agenda.ToolTipText = cp_Translate(MSG_MY_AGENDA)
            *--- Timer Btn
            .Timer.ToolTipText = cp_Translate(MSG_TIMER)		
        Endwith
    Endproc
    Proc postitbtn_click()
        Local oPostIt
        If Vartype(g_Alertmanager)<>'O' Or i_bDisableNewPostIt
            =Createobject('postit','new',i_ServerConn[1,2])
        Else
            m.oPostIt = g_Alertmanager.AddAlert("cp_AdvPostIt", 'new', i_ServerConn[1,2])
            m.oPostIt.DoAlert()
            m.oPostIt = .Null.
        Endif
        Return
        *--- Rimuove bottoni desktpbutton nuova interfaccia
    Procedure RemoveDeskButton(pName)
        This.RemoveButton(m.pName)
        This.RemoveObject(Left(m.pName, Len(m.pName)-3))
    Endproc
    Proc postitfolder_click()
        * If VARTYPE(g_Alertmanager)<>'O' OR ( VARTYPE(i_bDisableNewPostIt)='L' And i_bDisableNewPostIt)
        *		=createobject('postitfolder')
        *	Else
        Do cp_postitfolder
        *	EndIf
        Return
    Proc Destroy()
        Local i,xcg
        * --- controlla se esiste la definizione nel database di persistenza
        If Vartype(i_codute)='N' && solo per gli errori in interattivo
            If i_ServerConn[1,2]<>0
                =cp_SQL(i_ServerConn[1,2],"select count(*) as nrec from persist where code='desktopbar' and usercode="+cp_ToStr(i_codute),"persist_c")
            Else
                Select Count(*) As nrec From persist Where Code='desktopbar' And usercode=i_codute Into Cursor persist_c
            Endif
            If Not(Used('persist_c'))
                DoDefault()
                Return
            Endif
            If Type('persist_c.nrec')='C'
                i=Val(persist_c.nrec)
            Else
                i=persist_c.nrec
            Endif
            Use
            If i=0
                If i_ServerConn[1,2]<>0
                    =cp_SQL(i_ServerConn[1,2],"insert into persist (code,usercode) values ('desktopbar',"+cp_ToStr(i_codute)+")")
                Else
                    Insert Into persist (Code,usercode) Values ('desktopbar',i_codute)
                Endif
            Endif
            xcg=Createobject('pxcg','save')
            This.Serialize(xcg)
            If i_ServerConn[1,2]<>0
                =cp_SQL(i_ServerConn[1,2],"update persist set persist="+cp_ToStr(xcg.cText)+" where code='desktopbar' and usercode="+cp_ToStr(i_codute))
            Else
                Update persist Set persist=xcg.cText Where Code='desktopbar' And usercode=i_codute
            Endif
        Endif
        *--- Zucchetti Aulla Inizio - Schedulatore
        If Type('g_userscheduler')='L' And (g_UserScheduler) And Type('oschedTimer')='O' And Type('proclogin')='U'
            Release oschedTimer
        Endif
        *--- Zucchetti Aulla Fine - Schedulatore
        *---
        If TYPE("This.oRecentMenu")="O"
            This.oRecentMenu.oSearchMenu.oToolbar=.Null.
        Endif
        *---
        DoDefault()
        Return
    Proc Serialize(xcg)
        If xcg.bIsStoring
            xcg.Container(This)
            For i=1 To This.ControlCount
                If Upper(This.Controls(i).Class)=='DESKTOPBUTTON' Or (Upper(This.Controls(i).Class)=='CB_DESKTOPBUTTON_DUMMY' And !Isnull(This.Controls(i).oBtn))
                    xcg.Contained(This,This.Controls(i))
                Endif
            Next
            xcg.EndContainer()
            xcg.Save('N',This.DockPosition)
            xcg.Save('N',This.Top)
            xcg.Save('N',This.Left)
            xcg.Save('N',This.Width)
            xcg.Save('N',This.Height)
            xcg.Save('C',Type('i_oTreeMenuForm'))
            xcg.Eoc()
        Else
            xcg.Container(This)
            i=xcg.Load('N',0)
            This.Dock(i)
            This.Top=xcg.Load('N',0)
            This.Left=xcg.Load('N',0)
            This.Width=xcg.Load('N',100)
            This.Height=xcg.Load('N',25)
            If xcg.Load('C','U')='O'
                cp_tmenu()
            Endif
            xcg.Eoc()
        Endif
        Return
    Proc AddButton_dsk(cComment,cPrg,oSource)
        Local i,j,sBtn,oBtn
        *--- Disabilito resize all'afterdock (errore c0000005)
        This.bNotifyResize = .F.
        i=0
        For j=1 To This.ControlCount
            If Type("this.Controls(j).cComment") <> 'U' And alltrim(This.Controls(j).cComment)==alltrim(cComment)
                i=j
            Endif
        Next
        If i=0
            sBtn=Sys(2015)
            If This.bnewStyle
                This.AddObject(sBtn,'cb_desktopbutton_dummy')
                oBtn=This.&sBtn
                Do Case
                    Case i_nTBTNH>30
                        m.oBtn.nOffset_H = 23
                    Case i_nTBTNH>22
                        m.oBtn.nOffset_H = 15
                    Otherwise
                        m.oBtn.nOffset_H = 6
                Endcase
                oBtn.Caption=cComment
                oBtn._OnClick = "this.OpenGest()"
                oBtn._OnRightClick = "This.MenuRightClick()"
                If cp_fileexist('bmp\tb_'+cPrg+'.bmp')
                    oBtn.cImage='bmp\tb_'+cPrg+'.bmp'
                    oBtn.Caption=''
                Endif
            Else
                This.AddObject(sBtn,'desktopbutton')
                oBtn=This.&sBtn
                oBtn.Height=i_nTBTNH
                oBtn.Caption=cComment
                If cp_fileexist('bmp\tb_'+cPrg+'.bmp')
                    oBtn.Caption=''
                    oBtn.Width=i_nTBTNW
                    oBtn.Picture='bmp\tb_'+cPrg+'.bmp'
                Endif
            Endif
            * aggiunto assegnamento ed eventuali chiavi
            If Type('oSource')<>'L'
                * valorizzo la chiave dell'oggetto
                oBtn.SetSourceKey(oSource)
            Endif
            oBtn.cPrg=cPrg
            oBtn.cComment=cComment
            oBtn.ToolTipText=cComment
            oBtn.Visible=.T.
        Else
            cp_msg(cp_Translate(MSG_COMMAND_IS_DUPLICATED))
        Endif
        *--- Ripristino resize all'afterdock (errore c00005)
        This.bNotifyResize = .T.
        Return
    Proc DragDrop(oSource, nXCoord, nYCoord)
        * --- accetta come drop un form con gestione standard
        If oSource.ParentClass='Stdcontainer'
            * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
            Local L_oBj, L_Sec, L_Param, L_Prg
            L_oBj=oSource.Parent.Parent.Parent
		    DO WHILE !ISNULL(L_oBj) AND !PEMSTATUS(L_oBj,"getSecuritycode",5) AND PEMSTATUS(L_oBj,"oParentObject",5)
			*--- Se � un figlio integrato devo risalire fino al padre		    
		      	L_oBj=L_oBj.oParentObject
		    ENDDO
            L_Sec=Alltrim(L_oBj.getSecuritycode())
            L_Prg= Alltrim(Substr(L_oBj.Class,2,Len(L_oBj.Class)-1))
            L_Param=Upper(Alltrim(Substr(L_Sec,Len(L_Prg)+2)))
            If Not Empty(Alltrim(Nvl(L_Param, ' ')))
                L_Prg = L_Prg + '("'+L_Param+'")'
            Else
                L_Prg = L_Prg
            Endif
            * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
            This.AddButton_dsk(L_oBj.cComment,L_Prg,oSource)
        Endif
        If oSource.Class='Desktopbutton'
            * Inizio (Errore se droppo un bottone di un post-in nella toolbar post-in)
            * verifico, prima di rimuovere se il bottone � nella toolbar
            Local L_Loop,L_Remove
            L_Loop=1
            L_Remove=.F.
            Do While Type('This.Objects[L_Loop]')='O' And Not L_Remove
                L_Remove=Upper(oSource.Name)=Upper(This.Objects[L_loop].Name)
                L_Loop=L_Loop+1
            Enddo
            If L_Remove
                This.RemoveObject(oSource.Name)
            Endif
        Endif
        Return
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        Do Case
            Case nState=0   && Enter
                This.cOldIcon=oSource.DragIcon
                If oSource.ParentClass='Stdcontainer'
                    oSource.DragIcon=i_cBmpPath+"cross01.cur"
                Endif
            Case nState=1   && Leave
                oSource.DragIcon=This.cOldIcon
        Endcase
        Return

        ***** Zucchetti Aulla Inizio
        * Gestione Flussi Documentali lancia la gestione documenti
    Proc BtnWfld_click
        Local l_sPrg
        l_sPrg="GSWD_KWD"
        Do (l_sPrg)
        Release l_sPrg
        Return
        ***** Zucchetti Aulla fine - Flussi documentali

        ***** Zucchetti Aulla Inizio - Document management
    Proc aharchivia_click()
        Parameters operation
        *--- Vecchia Interfaccia uso menu std
        If Vartype(m.operation)= 'C' And m.operation = 'S'
            Local nCmd
            nCmd= 0
            Define Popup popCmd From Mrow(),Mcol() SHORTCUT
            Define Bar 1 Of popCmd Prompt Ah_Msgformat('Acquisizione singola')
            On Selection Bar 1 Of popCmd nCmd = 1
            Define Bar 2 Of popCmd Prompt Ah_Msgformat('Acquisizione multipla')
            On Selection Bar 2 Of popCmd nCmd = 2
            Activate Popup popCmd
            Deactivate Popup popCmd
            Release Popups popCmd
            Do Case
                Case  nCmd = 1
                    This.aharchivia_click('U')
                Case  nCmd = 2
                    This.aharchivia_click('M')
            Endcase
            Return
        Endif
        Local l_ckeyselect,l_cfile, l_vpos, l_tmpc, l_tmpp, l_tmpv, l_cprg
        l_ckeyselect=''
        If Vartype(i_curform)='O' And Not Isnull(i_curform) And Vartype(i_curform.ckeyselect)='C' And Not Empty(i_curform.ckeyselect)
            l_tmpc = i_curform.ckeyselect
            Do While At(',',l_tmpc)>0
                l_vpos = At(',',l_tmpc)
                l_tmpp = 'i_curform.w_'+Substr(l_tmpc,1,At(',',l_tmpc)-1)
                l_tmpv = Eval(l_tmpp)
                Do Case
                    Case Vartype(l_tmpv)='C'
                        l_ckeyselect = l_ckeyselect+l_tmpv
                    Case Vartype(l_tmpv)='D'
                        l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
                    Case Vartype(l_tmpv)='N'
                        l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
                Endcase
                l_tmpc = Substr(l_tmpc,At(',',l_tmpc)+1)
            Enddo
            * Valorizza l'ultimo
            l_tmpp = 'i_curform.w_'+l_tmpc
            l_tmpv = Eval(l_tmpp)
            Do Case
                Case Vartype(l_tmpv)='C'
                    l_ckeyselect = l_ckeyselect+l_tmpv
                Case Vartype(l_tmpv)='D'
                    l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
                Case Vartype(l_tmpv)='N'
                    l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
            Endcase
            *
            l_cfile       = Upper(i_curform.cfile)
            l_cprg        = Upper(i_curform.cPrg)
            Local l_sPrg
            l_sPrg='gsut_bcv'
            ** Inizio modifica Zucchetti Aulla -- per DOCM
            If Vartype(operation)= 'C'
                * cattura da scanner
                *--- Se nuova interfaccia il campo operation vale "U" per singola "M" per multipla
                Do (l_sPrg) With (i_curform),l_cfile,l_ckeyselect,l_cprg, operation
            Else
                * cattura da file
                Do (l_sPrg) With (i_curform),l_cfile,l_ckeyselect,l_cprg,'C'
            Endif
            Release l_sPrg
        ELSE
        	If Vartype(m.operation)= 'C' And m.operation = 'B'
        		cp_ErrorMsg(MSG_NO_ENTITY_OPENED_ERROR,"!",,.T.)
        	else
	            Local l_sPrg
	            l_sPrg='GSUT_BTB'
	            If !Type('operation')='C'
	                * Nessun parametro
	                *Chiamo la routine per la gestione dell'archiviazione rapida senza form attivo
	                Do (l_sPrg) With .Null.,'AF' && l'ultimo parametro indica la tipologia di archiviazione - Tramite selezione file
	            Else
	                * Scanner
	                *Chiamo la routine per la gestione dell'archiviazione rapida senza form attivo
	                Do (l_sPrg) With .Null.,'AS', operation && l'ultimo parametro indica la tipologia di archiviazione - Tramite Scanner
	            Endif
	            Release l_sPrg
	        Endif
        Endif
        Return

    ***** Zucchetti Aulla Inizio - iRevolution
    * apre pagina principale Mydesk
    Proc ahiRevolution_click
        Local l_sPrg
        l_sPrg="=infinitylink('HOME')"
        &l_sPrg
    Endproc
    ***** Zucchetti Aulla fine - iRevolution

    Proc ahvisualizza_click()
        Local l_ckeyselect,l_cfile, l_vpos, l_tmpc, l_tmpp, l_tmpv, l_cprg, l_sPrg
        l_ckeyselect=''
        l_sPrg='gsut_bcv'
        If Vartype(i_curform)='O' And Not Isnull(i_curform) And Vartype(i_curform.ckeyselect)='C' And Not Empty(i_curform.ckeyselect)
            l_tmpc = i_curform.ckeyselect
            Do While At(',',l_tmpc)>0
                l_vpos = At(',',l_tmpc)
                l_tmpp = 'i_curform.w_'+Substr(l_tmpc,1,At(',',l_tmpc)-1)
                l_tmpv = Eval(l_tmpp)
                Do Case
                    Case Vartype(l_tmpv)='C'
                        l_ckeyselect = l_ckeyselect+l_tmpv
                    Case Vartype(l_tmpv)='D'
                        l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
                    Case Vartype(l_tmpv)='N'
                        l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
                Endcase
                l_tmpc = Substr(l_tmpc,At(',',l_tmpc)+1)
            Enddo
            * Valorizza l'ultimo
            l_tmpp = 'i_curform.w_'+l_tmpc
            l_tmpv = Eval(l_tmpp)
            Do Case
                Case Vartype(l_tmpv)='C'
                    l_ckeyselect = l_ckeyselect+l_tmpv
                Case Vartype(l_tmpv)='D'
                    l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
                Case Vartype(l_tmpv)='N'
                    l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
            Endcase
            *
            l_cfile       = Upper(i_curform.cfile)
            l_cprg        = Upper(i_curform.cPrg)
            Do (l_sPrg) With (i_curform),l_cfile,l_ckeyselect,l_cprg,'V'
        Else
            Do (l_sPrg) With Null,'GBGBGB','GBGBGB','GBGBGB','V'
        Endif
        Release l_sPrg
        Return

    Proc ahvisualizzaprat_click()
        Local l_ckeyselect,l_cfile, l_vpos, l_tmpc, l_tmpp, l_tmpv, l_cprg, l_sPrg
        l_ckeyselect=''
        l_sPrg='gsut_bcv'
        If Vartype(i_curform)='O' And Not Isnull(i_curform) And Vartype(i_curform.ckeyselect)='C' And Not Empty(i_curform.ckeyselect)
            l_tmpc = i_curform.ckeyselect
            Do While At(',',l_tmpc)>0
                l_vpos = At(',',l_tmpc)
                l_tmpp = 'i_curform.w_'+Substr(l_tmpc,1,At(',',l_tmpc)-1)
                l_tmpv = Eval(l_tmpp)
                Do Case
                    Case Vartype(l_tmpv)='C'
                        l_ckeyselect = l_ckeyselect+l_tmpv
                    Case Vartype(l_tmpv)='D'
                        l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
                    Case Vartype(l_tmpv)='N'
                        l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
                Endcase
                l_tmpc = Substr(l_tmpc,At(',',l_tmpc)+1)
            Enddo
            * Valorizza l'ultimo
            l_tmpp = 'i_curform.w_'+l_tmpc
            l_tmpv = Eval(l_tmpp)
            Do Case
                Case Vartype(l_tmpv)='C'
                    l_ckeyselect = l_ckeyselect+l_tmpv
                Case Vartype(l_tmpv)='D'
                    l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
                Case Vartype(l_tmpv)='N'
                    l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
            Endcase
            *
            l_cfile       = Upper(i_curform.cfile)
            l_cprg        = Upper(i_curform.cPrg)
            Do (l_sPrg) With (i_curform),l_cfile,l_ckeyselect,l_cprg,'V',0,0,.F.,.F.,.T.
        Else
            Do (l_sPrg) With Null,'GBGBGB','GBGBGB','GBGBGB','V',0,0,.F.,.F.,.T.
        Endif
        Release l_sPrg
        Return

    Proc ahnuovodoc_click()
        Local l_ckeyselect,l_cfile, l_vpos, l_tmpc, l_tmpp, l_tmpv, l_cprg, l_sPrg
        l_ckeyselect=''
        l_sPrg='gsut_bcv'
        If Vartype(i_curform)='O' And Not Isnull(i_curform) And Vartype(i_curform.ckeyselect)='C' And Not Empty(i_curform.ckeyselect)
            l_tmpc = i_curform.ckeyselect
            Do While At(',',l_tmpc)>0
                l_vpos = At(',',l_tmpc)
                l_tmpp = 'i_curform.w_'+Substr(l_tmpc,1,At(',',l_tmpc)-1)
                l_tmpv = Eval(l_tmpp)
                Do Case
                    Case Vartype(l_tmpv)='C'
                        l_ckeyselect = l_ckeyselect+l_tmpv
                    Case Vartype(l_tmpv)='D'
                        l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
                    Case Vartype(l_tmpv)='N'
                        l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
                Endcase
                l_tmpc = Substr(l_tmpc,At(',',l_tmpc)+1)
            Enddo
            * Valorizza l'ultimo
            l_tmpp = 'i_curform.w_'+l_tmpc
            l_tmpv = Eval(l_tmpp)
            Do Case
                Case Vartype(l_tmpv)='C'
                    l_ckeyselect = l_ckeyselect+l_tmpv
                Case Vartype(l_tmpv)='D'
                    l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
                Case Vartype(l_tmpv)='N'
                    l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
            Endcase
            *
            l_cfile       = Upper(i_curform.cfile)
            l_cprg        = Upper(i_curform.cPrg)
            Do (l_sPrg) With (i_curform),l_cfile,l_ckeyselect,l_cprg,'D'
            Release l_sPrg
        Else
            cp_ErrorMsg(MSG_NEW_DOCUMENT_ERROR,"!",,.T.)
        Endif
        Return

        ***** Zucchetti Aulla Fine - Document management

        *--- Zucchetti Aulla Inizio - Inforeader
    Proc inforeader_click()
        Local OBJ
        l_sPrg="GSIR_KSR()"
        OBJ=&l_sPrg
        Return
        *--- Zucchetti Aulla Fine - Inforeader

        *--- Zucchetti Aulla Inizio- Job Scheduler
    Proc schedjob_click()
        With This
            If .boxsched.Value=cp_Translate(MSG_DISABLED_SCH)
                *--- Disattivo schedulatore
                .schedjob.cImage='bmp\suggerito.Bmp'
                .boxsched.Value=cp_Translate(MSG_ENABLED_SCH)
                Public oschedTimer
                oschedTimer=Createobject('schedTimer')
            Else
                *--- Attivo schedulatore
                .schedjob.cImage='bmp\lanciato.Bmp'
                .boxsched.Value=cp_Translate(MSG_DISABLED_SCH)
                Release oschedTimer
            Endif
            .boxsched.ToolTipText=.boxsched.Value
        Endwith
        Return
        *--- Zucchetti Aulla Fine- Job Scheduler

        *--- Zucchetti Aulla Inizio - Setup Activity Logger (solo utente schedulatore)
    Proc stpactlogger_click()
        Local l_sPrg
        l_sPrg="do cp_SetupTrace"
        &l_sPrg
    EndProc
        *--- Zucchetti Aulla Fine - Setup Activity Logger (solo utente schedulatore)

        *--- Zucchetti Aulla Inizio- Gestione attributi
    Proc Attributi_click()
        If Vartype(i_curform)='O' And i_curform.cFunction<>'Load'
            If Vartype(i_curform.bloaded)='L' And Vartype(i_curform.bGestatt)='L' And i_curform.bloaded And i_curform.bGestatt
                * Gestione attributi
                Local L_Prg
                L_Prg="GSAR_BAR (i_curform)"
                &L_Prg
            Endif
        Endif
        Return
        *--- Zucchetti Aulla fine- Gestione attributi

        *--- Zucchetti Aulla Inizio - Configurazioni gestioni
    Procedure cfgGest_click()
        DoDefault()
        Local l_n
        If Type("i_curform.cPrg")='C' And Type("i_curform.cnt")<>"O"
            CreaMenuCfgGestione(0,'D', Mrow(),Mcol())
        Endif
        *--- Zucchetti Aulla Fine - Configurazioni gestioni
	EndProc
	
Enddefine

Define Class desktopbutton As CommandButton
    Height=23
    FontBold=.F.
    cPrg=''
    cComment=''
    Dimension xKey[1]
    xKey[1]=''
    nMouseDownX=0
    nMouseDownY=0
    cAzi=''
    AutoSize=.T.
    Proc SetSourceKey(oSource)
        Local i
        Dimension This.xKey[alen(oSource.xKey)]
        For i=1 To Alen(oSource.xKey)
            This.xKey[i]=oSource.xKey[i]
        Next
        i=cp_ascan(@i_TableProp,Lower(oSource.cfile),i_nTables)
        If i>0
            This.cAzi=Strtran(cp_SetAzi(i_TableProp[i,2]),Strtran(i_TableProp[i,2],'xxx',''),'')
        Endif
    Proc Serialize(xcg)
        Local i,j
        If xcg.bIsStoring
            xcg.Save('C',This.cPrg)
            xcg.Save('C',This.cComment)
            xcg.Save('N',This.Top)
            xcg.Save('N',Alen(This.xKey))
            For i=1 To Alen(This.xKey)
                xcg.Save('X',This.xKey[i])
            Next
            xcg.Save('C',This.cAzi)
            xcg.Save('C',This.Picture)
            xcg.Eoc()
        Else
            This.cPrg=xcg.Load('C','')
            This.cComment=xcg.Load('C','')
            * --- questa proprieta' e' R/O nelle toolbar in VFP 5.0, mentre deve essere settata
            *     negli altri casi. La on error evita l' errore.
            Local i_errsav
            i_errsav=On('ERROR')
            On Error =.F.
            This.Top=xcg.Load('N',0)
            On Error &i_errsav
            * ---
            This.Caption=This.cComment
            i=xcg.Load('N',0)
            Dimension This.xKey[max(1,i)]
            For j=1 To i
                This.xKey[j]=xcg.Load('X','')
            Next
            This.cAzi=xcg.Load('C','')
            This.Visible=Empty(This.cAzi) Or Trim(This.cAzi)=Trim(i_codazi)
            This.Picture=xcg.Load('C','')
            If !Empty(This.Picture)
                This.Caption=''
                This.Width=23
            Endif
            This.ToolTipText = This.cComment
            xcg.Eoc()
        Endif
    Endproc
    Proc Click()
        Local i_cmd
        Local i_o,i_i,i_k,i_v,i_p
        i_cmd=This.cPrg
        If Not(Empty(i_cmd))
            If Empty(This.cAzi) Or Trim(This.cAzi)=Trim(i_codazi)
                * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
                * Inserito ramo Else
                If Right(i_cmd,1)<>')'
                    * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
                    i_o=&i_cmd()
                    * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
                Else
                    i_o=&i_cmd
                Endif
                * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
                If !Isnull(i_o) And Type("i_o.cFile")="C"
                    If Used(i_o.cKeySet)
                        i_o.ecpFilter()
                        * --- carica il record specificato
                        i_k=i_o.ckeyselect
                        For i_i=1 To Alen(This.xKey)
                            If Len(i_k)>0
                                i_p=At(',',i_k)
                                If i_p=0
                                    i_v=i_k
                                    i_k=''
                                Else
                                    i_v=Left(i_k,i_p-1)
                                    i_k=Substr(i_k,i_p+1)
                                Endif
                                i_o.w_&i_v=This.xKey[i_i]
                            Endif
                        Next
                        i_o.ecpSave()
                        *
                    Endif
                Endif
            Else
                cp_ErrorMsg(cp_MsgFormat(MSG_BUTTON_LINKED_TO_COMPANY__,This.cAzi),"!",cp_Translate(MSG_POSTIN_ADMIN),.F.)
            Endif
        Endif
    Endproc
    Procedure RightClick()
        funzione=""
        Activate Screen odesktopbar
        Define Popup Finestra From Mrow(),Mcol() SHORTCUT Margin
        Define Bar 1 Of Finestra Prompt cp_Translate(MSG_OPEN_BUTTON)
        Define Bar 2 Of Finestra Prompt "\-"
        Define Bar 3 Of Finestra Prompt cp_Translate(MSG_DELETE_BUTTON)
        Define Bar 4 Of Finestra Prompt "\-"
        Define Bar 5 Of Finestra Prompt cp_Translate(MSG_CHANGE_IMAGE)
        On Sele Bar 1 Of Finestra funzione='A'
        On Sele Bar 3 Of Finestra funzione='E'
        On Sele Bar 5 Of Finestra funzione='C'
        Activ Popup Finestra
        Deactiv Popup Finestra
        If funzione='A'
            This.Click()
        Endif
        If funzione='E'
            * Di seguito il codice originale: nel caso il bottone fosse stato collegato ad un Postin
            * la removeobject dava errore poich� l'oggetto cui � collegato � oPstCnt (non pubblico).
            *ODESKTOPBAR.RemoveObject(THIS.Name)
            * La chiamata This.Parent.RemoveObject(This.Name) � pi� generica.
            This.Parent.RemoveObject(This.Name)
        Endif
        If funzione='C'
            This.Picture=Getpict("BMP|JPG|GIF|ICO",cp_Translate(MSG_IMAGE_CL))
        Endif

    Endproc

    Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            * --- per ritardare un po il drag e drop
            This.nMouseDownX=nXCoord
            This.nMouseDownY=nYCoord
        Endif
    Proc MouseMove(nButton, nShift, nXCoord, nYCoord)
        If nButton=1 And (Abs(nXCoord-This.nMouseDownX)+Abs(nYCoord-This.nMouseDownY))>8
            This.DragIcon=i_cBmpPath+"DragMove.cur"
            This.Drag(1)
        Endif
    Endproc
Enddefine

*****
Define Class cb_desktopbutton_dummy As Custom
    oBtn = .Null.
    Procedure Init()
        This.oBtn = This.Parent.AddButton(This.Name + "_cb" ,'cb_desktopbutton')
        Do Case
            Case i_nTBTNH>30
                This.oBtn.nOffset_H = 23
            Case i_nTBTNH>22
                This.oBtn.nOffset_H = 15
            Otherwise
                This.oBtn.nOffset_H = 6
        Endcase
        This.oBtn._OnClick = "this.OpenGest()"
        This.oBtn._OnRightClick = "This.MenuRightClick()"
    Endproc
    Procedure THIS_ACCESS
        Lparameter cMemberName
        If Isnull(This.oBtn) Or !Pemstatus(This.oBtn, m.cMemberName, 5) Or Inlist(Upper(m.cMemberName), 'CLASS', 'NAME')
            Return This
        Else
            Return This.oBtn
        Endif
    Endproc

    *--- E' stata definita perch� viene richiamata in caso il bottone
    *--- sia stato cancellato al momento della distruzione della toolbar
    Proc Serialize(xcg)
    Endproc
Enddefine

Define Class CB_DesktopButton As CommandBarButton
    cPrg=''
    cComment=''
    Dimension xKey[1]
    xKey[1]=''
    cAzi=''

    Proc SetSourceKey(oSource)
        Local i
        Dimension This.xKey[alen(oSource.xKey)]
        For i=1 To Alen(oSource.xKey)
            This.xKey[i]=oSource.xKey[i]
        Next
        i=cp_ascan(@i_TableProp,Lower(oSource.cfile),i_nTables)
        If m.i>0
            This.cAzi=Strtran(cp_SetAzi(i_TableProp[i,2]),Strtran(i_TableProp[i,2],'xxx',''),'')
        Endif

    Proc Serialize(xcg)
        Local i,j
        If xcg.bIsStoring
            xcg.Save('C',This.cPrg)
            xcg.Save('C',This.cComment)
            xcg.Save('N',This.Top)
            xcg.Save('N',Alen(This.xKey))
            For i=1 To Alen(This.xKey)
                xcg.Save('X',This.xKey[i])
            Next
            xcg.Save('C',This.cAzi)
            xcg.Save('C',This.cImage)
            xcg.Eoc()
        Else
            This.cPrg=xcg.Load('C','')
            This.cComment=xcg.Load('C','')
            * --- questa proprieta' e' R/O nelle toolbar in VFP 5.0, mentre deve essere settata
            *     negli altri casi. La on error evita l' errore.
            Local i_errsav
            i_errsav=On('ERROR')
            On Error =.F.
            This.Top=xcg.Load('N',0)
            On Error &i_errsav
            * ---
            This.Caption=This.cComment
            i=xcg.Load('N',0)
            Dimension This.xKey[max(1,i)]
            For j=1 To i
                This.xKey[j]=xcg.Load('X','')
            Next
            This.cAzi=xcg.Load('C','')
            This.Visible=Empty(This.cAzi) Or Trim(This.cAzi)=Trim(i_codazi)
            This.cImage=xcg.Load('C','')
            If !Empty(This.cImage)
                This.Caption=''
            Endif
            This.ToolTipText = This.cComment
            xcg.Eoc()
        Endif
    Endproc
    Proc OpenGest()
        Local i_cmd
        Local i_o,i_i,i_k,i_v,i_p
        i_cmd=This.cPrg
        If Not(Empty(i_cmd))
            If Empty(This.cAzi) Or Trim(This.cAzi)=Trim(i_codazi)
                * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
                * Inserito ramo Else
                If Right(i_cmd,1)<>')'
                    * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
                    i_o=&i_cmd()
                    * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
                Else
                    i_o=&i_cmd
                Endif
                * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
                If !Isnull(i_o)
                    If Used(i_o.cKeySet)
                        i_o.ecpFilter()
                        * --- carica il record specificato
                        i_k=i_o.ckeyselect
                        For i_i=1 To Alen(This.xKey)
                            If Len(i_k)>0
                                i_p=At(',',i_k)
                                If i_p=0
                                    i_v=i_k
                                    i_k=''
                                Else
                                    i_v=Left(i_k,i_p-1)
                                    i_k=Substr(i_k,i_p+1)
                                Endif
                                i_o.w_&i_v=This.xKey[i_i]
                            Endif
                        Next
                        i_o.ecpSave()
                    Endif
                    *
                Endif
            Else
                cp_ErrorMsg(cp_MsgFormat(MSG_BUTTON_LINKED_TO_COMPANY__,This.cAzi),"!",cp_Translate(MSG_POSTIN_ADMIN),.F.)
            Endif
        Endif
    Endproc

    *--- Modifica Immagine bottone
    Procedure ChangeImage()
        Local cImg
        cImg =	Getpict("BMP|JPG|GIF|ICO",cp_Translate(MSG_IMAGE_CL))
        With This
            If Empty(cImg)
                Do Case
                    Case i_nTBTNH>30
                        .nOffset_H = 23
                    Case i_nTBTNH>22
                        .nOffset_H = 15
                    Otherwise
                        .nOffset_H = 6
                Endcase
                .Caption = .cComment
                .cImage = ''
            Else
                .nOffset_H = 6
                .Caption = ''
                .cImage = cImg
            Endif
        Endwith
    Endproc

    Procedure MenuRightClick()
        Local oBtnMenu, oMenuItem
        m.oBtnMenu = Createobject("cbPopupMenu")
        m.oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
        m.oMenuItem = m.oBtnMenu.AddMenuItem()
        m.oMenuItem.Caption = cp_Translate(MSG_OPEN_BUTTON)
        m.oMenuItem.OnClick = "odesktopbar."+Alltrim(This.Name)+".OpenGest()"
        m.oMenuItem.Visible=.T.
        m.oMenuItem = m.oBtnMenu.AddMenuItem()
        m.oMenuItem.Caption = cp_Translate(MSG_DELETE_BUTTON)
        m.oMenuItem.OnClick = "odesktopbar.RemoveButton('"+This.Name+"')"
        m.oMenuItem.Visible=.T.
        m.oMenuItem = m.oBtnMenu.AddMenuItem()
        m.oMenuItem.Caption = cp_Translate(MSG_CHANGE_IMAGE)
        m.oMenuItem.OnClick = "odesktopbar."+Alltrim(This.Name)+'.ChangeImage()'
        m.oMenuItem.Visible=.T.
        m.oBtnMenu.InitMenu()
        Local x, Y, uFlag
        uFlag = 0
        With This
            Do Case
                Case .Parent.DockPosition=-1
                    m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+Sysmetric(3)+1
                    m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+.Height+Sysmetric(4)+Sysmetric(34) - 2
                Case .Parent.DockPosition=0 Or .Parent.DockPosition=3
                    m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+1
                    m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+.Height -2
                Case .Parent.DockPosition=1
                    m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+.Width-2
                    m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+1
                Case .Parent.DockPosition=2
                    m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+2
                    m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+1
                    m.uFlag = Bitor(m.uFlag, TPM_RIGHTALIGN)
            Endcase
        Endwith
        m.oBtnMenu.ShowMenu(m.x, m.y, m.uFlag)
    Endproc
Enddefine

*******
Define Class postit As Form
    Width=150
    Height=150
    BackColor=Rgb(255,255,0)
    Caption=''
    Icon=i_cBmpPath+i_cStdIcon
    oThis=.Null.
    oTimer=.Null.
    AlwaysOnTop=.T.
    Proc Init(op,nConn,oCalledForm, bReadOnly, bNoIntegra, bNoLink)
        Local i,fo
        If Pcount()<2
            * !!!!!
            cp_msg(cp_Translate(MSG_CONNECTION_WITH_POSTIN_MISSING_QM),.F.)
        Endif
        This.AddObject("oPstCnt","postitcontainer",op,nConn,oCalledForm, bReadOnly, bNoIntegra, bNoLink)
        *  --- la posizione ?
        Local l_top, l_left, l_found
        l_found = .F.
        l_top = 0
        l_left = 0
        For i=1 To _Screen.FormCount
            fo=_Screen.Forms(i)
            If Lower(fo.Class)=Lower(This.Class) And Lower(fo.oPstCnt.cVar)<>Lower(This.oPstCnt.cVar)
                If This.Top=fo.Top And This.Left=fo.Left
                    l_found = .T.
                Endif
                l_top = Max(l_top, fo.Top)
                l_left = Max(l_left, fo.Left)
            Endif
        Next
        If l_found
            This.Top=l_top+20
            This.Left=l_left+20
        Endif
        *    this.AddObject("oPstCnt","postitcontainer",op,nConn)
        This.oPstCnt.Visible=.T.
        This.oPstCnt.Enabled=.T.
        This.Show()
        This.oThis=This
        This.oTimer=Createobject("postitalwaysontoptimer",This)
        This.Caption=cp_Translate(MSG_POSTIN)
        Return
    Proc Resize()
        This.oPstCnt.Resize()
        *this.width=this.oPstCnt.width
        *this.height=this.oPstCnt.height
        Return
    Proc Activate()
        i_curform=This
        This.oPstCnt.Activate()
        Return
    Proc Deactivate()
        This.oPstCnt.Deactivate()
        i_curform=.Null.
        Return
    Proc QueryUnload()
        If This.oPstCnt.IsEmpty()
            * --- si distrugge
            This.oPstCnt.ecpDelete_Vuoto()
        Else
            This.oPstCnt.bQueryUnLoad=.T.
            This.oPstCnt.relasePostIT()
        Endif
        Return
    Proc Destroy()
        This.oThis=.Null.
        *--- Zucchetti Aulla Inizio - Chiusura postin vuoto
        If Not This.oPstCnt.bQueryUnLoad OR This.oPstCnt.IsEmpty()
        *--- Zucchetti Aulla Fine - Chiusura postin vuoto
            This.oPstCnt.relasePostIT()
        Endif
        This.oPstCnt.oParentForm=.Null.
        This.oPstCnt.Deactivate()
        Return
    Func HasCPEvents(i_cOp)
        Return(.T.)
    Proc ecpDelete()
        If This.oPstCnt.ecpDelete()
            This.Release()
        Endif
        Return
    Proc ecpEdit()
        This.oPstCnt.ecpEdit()
        Return
    Proc ecpF6()
        Return
    Proc ecpFilter()
        Return
    Proc ecpLoad()
        Return
    Proc ecpNext()
        Return
    Proc ecpPgUp()
        Return
    Proc ecpPgDn()
        Return
    Proc ecpPrint()
        Return
    Proc ecpPrior()
        Return
    Proc ecpQuit
        Local i_res
        If This.oPstCnt.IsEmpty()
            * --- si cancella
            This.oPstCnt.ecpDelete_Vuoto()
            This.Release() && lo rilascia
        Else
            This.oPstCnt.bQueryUnLoad=.T.
            This.oPstCnt.relasePostIT()
            This.Release()
        Endif
    Proc ecpSave()
        Return
    Proc ecpSecurity()
        Return
    Proc ecpZoom()
        Return
    Proc ecpZoomOnZoom()
        Return
    Proc ecpInfo()
        Return
    Func GetHelpFile()
        Return('postit')
    Func GetHelpTopic()
        Return('')
Enddefine

Define Class postitalwaysontoptimer As Timer
    oPostIt=.Null.
    Interval=5000
    Procedure Init(i_oPostIt)
        This.oPostIt=i_oPostIt
        Return
    Procedure Timer
        **testo se il postit � ancora aperto
        If Type("this.oPostIT")='O' And Not Isnull(This.oPostIt)
            This.oPostIt.AlwaysOnTop=.F.
            This.oPostIt.oTimer=.Null.
            This.oPostIt=.Null.
        Endif
Enddefine

*--- La classe � stata ridefinita perch� veniva utilizzata la stessa classe sia per il disegnatore di query
*--- che per i postit
Define Class ListboxPostit As ListBox
    nRow=0  && il contatore della riga passata al contenitore
    nY=0
    Proc MouseDown(nButton,nShift,nX,nY)
        This.nY=nY
        *  local oRow
        *  if this.nRow<>0
        *    oRow=this.GetRowContainer()
        *    if !isnull(oRow)
        *      oRow.SetValues(this,this.nRow)
        *      oRow.visible=.f.
        *      this.nRow=0
        *    endif
        *  endif
    Proc MouseUp(nButton,nShift,nX,nY)
        Local i,j,rh,rr,dh,dx,oo,oRow
        Nodefault
        oRow=This.GetRowContainer()
        If This.nY<>nY And This.MoverBars
            For i=1 To This.ListCount
                This.Selected(i)=.F.
            Next
        Endif
        If !Isnull(oRow) And This.nY=nY
            * --- trova la riga selezionata
            j=0
            ns=0
            For i=1 To This.ListCount
                If This.Selected(i)
                    j=i
                    ns=ns+1
                Endif
            Next
            If ns<=1
                * --- trova la riga in base alla posizione del click
                rh=Fontmetric(3,This.FontName,This.FontSize)+Fontmetric(1,This.FontName,This.FontSize)-1
                dh=0
                dx=0
                oo=This
                Do While oo.BaseClass<>'Form'
                    If !Inlist(oo.BaseClass,'Page')
                        dh=dh+oo.Top
                        dx=dx+oo.Left
                    Endif
                    If oo.BaseClass=='Pageframe'
                        dh=dh+30
                    Endif
                    oo=oo.Parent
                Enddo
                rr=Int((nY-dh)/rh)
                * --- se c'e' un container, lo attiva
                oRow.SetPos(This.Top+2+rr*rh,This.Left+Iif(This.MoverBars,20,1))
                If j<>0 And nX-dx<This.Width-20 And nY-dh>0 And rr<This.ListCount
                    oRow.GetValues(This,j)
                    This.nRow=j
                    oRow.Visible=.T.
                    oRow.ZOrder(0)
                    oRow.SetFocus()
                Else
                    This.nRow=0
                    oRow.Visible=.F.
                    *this.additem('')
                    *oRow.BlankValues()
                    *this.nRow=this.listcount
                Endif
            Endif
        Endif
    Func GetRowContainer()
        Return(.Null.)
Enddefine


Define Class postitcontainer As Container
    * nel database il campo status
    *   'F' = nel folder
    *   'P' = spedito e non ancora ricevuto
    *   'D' = nel desktop
    *   'W' = warning
    Width=150
    Height=150
    BorderWidth=0
    BackColor=Rgb(255,255,0)
    cVar=''
    nConn=0
    cOldIcon=''
    nBtn=0
    nLvw=0
    nBtnPos=130
    nTxtPos=0
    bUpdated=.F.
    bDeleted=.F.
    nOldTop=0
    nOldLeft=0
    nOldHeight=0
    cStatus=''
    cParent = ''
    dStart=Ctod('  /  /  ')
    dStop=Ctod('  /  /  ')
    nUser=1
    nCreatedBy=1
    cPageName=''
    cNewStatus=''
    nMouseDownX=0
    nMouseDownY=0
    bQueryUnLoad=.F.
    nHeight=0
    bCanResize=.T.
    nDimension=This.Height
    cItemTag=''
    Dimension cDataTag[1]
    cDataTag[1]=''
    oParentForm=.Null.
    * Zucchetti Aulla Frasi Modello
    bdisablefrasimodello=.F.
    nSeconds=0
    bReadOnly = .F.
    * Zucchetti Aulla Frasi Modello fine
	*--- Zucchetti Aulla Inizio - check cpccchk
	bEditing = .F.	&&Indica se il postit � in modalit� edit
	cCPCCCHK = ""
	bNew = .F.
	*--- Zucchetti Aulla Fine - check cpccchk
	*--- Zucchetti Aulla Inizio - Gestione Appuntamento e to do
	bNoIntegra = .F.
	bNoLink = .F.
	*--- Zucchetti Aulla Fine - Gestione Appuntamento e to do
    * Zucchetti Aulla Inizio - Colore Post-IN
    Add Object postitedt As EditBox With BackColor=Rgb(255,255,0),Width=150,Height=130,FontBold=.F.,ScrollBars=0,OLEDropMode=1,BorderStyle=1
    * Zucchetti Aulla Fine - Colore Post-IN
    *add object shp as shape with top=130,left=0,width=150,height=0,curvature=0,borderstyle=1,borderwidth=5
    Add Object lvwCtrl As lvwContainer
    Add Object lvwFile As ListboxPostit With Top=170,Width=150,OLEDropMode=1,Visible=.F.
    Proc lvwFile.GetRowContainer()
        Return(This.Parent.lvwCtrl)
    Proc Init(op,nConn,oCalledForm, bReadOnly, bNoIntegra, bNoLink))
        *  proc Init(op,nConn)
        * --- inizializza
        This.bReadOnly=m.bReadOnly
        This.nConn=nConn
        * Zucchetti Aulla Inizio - Colore Post-IN
        This.BackColor=Iif(Vartype(g_PostInColor)='N' And g_PostInColor>0,g_PostInColor,Rgb(255,255,0))
        This.postitedt.BackColor=Iif(Vartype(g_PostInColor)='N' And g_PostInColor>0,g_PostInColor,Rgb(255,255,0))
        This.postitedt.DisabledBackColor=Iif(Vartype(g_PostInColor)='N' And g_PostInColor>0,g_PostInColor,Rgb(255,255,0))
        * Zucchetti Aulla Fine - Colore Post-IN
        *--- Zucchetti Aulla Inizio - Gestione Appuntamento e to do
        This.bNoIntegra = m.bNoIntegra
        This.bNoLink = m.bNoLink
        *--- Zucchetti Aulla Fine - Gestione Appuntamento e to do
        Do Case
            Case op='new'
                This.NewPostIT()
            Case op='set'
                This.SetPostIT()
            Case op='contained'
                This.WarnInForm(oCalledForm)
        Endcase
        Return

	Procedure Destroy
	    This.oParentForm=.Null.
	Endproc

		*--- Zucchetti Aulla Inizio - check cpccchk
	Procedure bEditing_Assign(xValue)
		This.bEditing = m.xValue
		This.postitedt.ReadOnly = !This.bEditing Or This.bReadOnly
	Endproc
	Procedure SetEditing(xValue)
		If !xValue
			If cp_YesNo(MSG_DISCARD_CHANGES_QP)
				This.bEditing = m.xValue
				This.postitedt.ReadOnly = !This.bEditing Or This.bReadOnly
			Endif
		Else
			This.bEditing = m.xValue
			This.postitedt.ReadOnly = !This.bEditing Or This.bReadOnly
		Endif
	Endproc
	*--- Zucchetti Aulla Fine - check cpccchk
        *--- Zucchetti Aulla Inizio - Invia a..
    Procedure postitedt.RightClick()
        Public l_postit
        l_postit = This.Parent
        If !l_postit.bReadOnly
			*--- Zucchetti Aulla Inizio - check cpccchk
			If !l_postit.bEditing
            If i_VisualTheme<>-1
                Local oBtnMenu, oMenuItem
                m.oBtnMenu = Createobject("cbPopupMenu")
                m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
					m.oMenuItem.Caption = Ah_Msgformat("Editing mode")
					m.oMenuItem.OnClick = "l_postit.SetEditing(.T.)"
					m.oMenuItem.MarkFor = "l_postit.bEditing"
					m.oMenuItem.Visible=.T.
					m.oBtnMenu.InitMenu()
					m.oBtnMenu.ShowMenu()
				Else
					Define Popup ShortcutMenu From Mrow(), Mcol() Relative SHORTCUT
					Define Bar 1 Of ShortcutMenu Prompt ("Editing mode")
					SET MARK OF BAR 1 OF ShortcutMenu TO l_postit.bEditing
					On Selection Bar 1 Of ShortcutMenu l_postit.SetEditing(.T.)
					Activate Popup ShortcutMenu
					Clear Popups
				Endif
			Else
				If i_VisualTheme<>-1
					Local oBtnMenu, oMenuItem
					m.oBtnMenu = Createobject("cbPopupMenu")
					m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
					IF !l_postit.bNew
						m.oMenuItem = m.oBtnMenu.AddMenuItem()					
						m.oMenuItem.Caption = Ah_Msgformat("Editing mode")
						m.oMenuItem.OnClick = "l_postit.SetEditing(.F.)"
						m.oMenuItem.MarkFor = "l_postit.bEditing"
						m.oMenuItem.Visible=.T.
					EndIf
					m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = Ah_Msgformat(MSG_POSTIN_PROPERTIES)
                m.oMenuItem.OnClick = "l_postit.EcpEdit()"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = Ah_Msgformat(MSG_SENDTO)
                m.oMenuItem.OnClick = "cp_PostitSendTo(l_postit)"
                m.oMenuItem.Visible=.T.
                m.oBtnMenu.InitMenu()
                m.oBtnMenu.ShowMenu()
            Else
                Define Popup ShortcutMenu From Mrow(), Mcol() Relative SHORTCUT
					IF !l_postit.bNew
						Define Bar 1 Of ShortcutMenu Prompt ("Editing mode")
						SET MARK OF BAR 1 OF ShortcutMenu TO l_postit.bEditing
						On Selection Bar 1 Of ShortcutMenu l_postit.SetEditing(.F.)					
					EndIf
					Define Bar 2 Of ShortcutMenu Prompt (MSG_POSTIN_PROPERTIES)
					On Selection Bar 2 Of ShortcutMenu l_postit.ecpEdit()
                If l_postit.cStatus<>'W'
						Define Bar 3 Of ShortcutMenu Prompt (MSG_SENDTO)
						On Selection Bar 3 Of ShortcutMenu cp_PostitSendTo(l_postit)
                Endif
                Activate Popup ShortcutMenu
                Clear Popups
            Endif
			Endif &&!l_postit.bEditing
			*--- Zucchetti Aulla Fine - check cpccchk
		Endif &&!l_postit.bReadOnly
        Release l_postit
    Endproc
    *--- Zucchetti Aulla Fine - Invia a..

    * ---- Zucchetti Aulla Aggiunto parametro per salvare post it senza distruggerlo
    Proc relasePostIT(bNoRelease)
        Local xcg,i,fo,i_n,i_nL,i_nT
        If Not(This.bDeleted)
			If This.bEditing
            * --- salva il valore corrente nel database
            i_nL=This.nOldLeft
            i_nT=This.nOldTop
            If This.bCanResize
                i_nL=This.Parent.Left
                i_nT=This.Parent.Top
            Endif
            If This.bUpdated Or i_nT<>This.nOldTop Or i_nL<>This.nOldLeft
                xcg=Createobject('pxcg','save')
                This.Serialize(xcg,.F.)
					*--- Zucchetti Aulla Inizio - check cpccchk
					*--- rileggo cpccchk
					If This.nConn<>0
						=cp_SQL(This.nConn,"select cpccchk from postit where code="+cp_ToStrODBC(This.cVar), "postit_cpccchk")
					Else
						Select cpccchk From postit Where Code=This.cVar Into Cursor postit_cpccchk
					Endif
					If NVL(postit_cpccchk.cpccchk,"xxx") == NVL(This.cCPCCCHK, "xxx")
						This.cCPCCCHK = cp_NewCCChk()
						*--- Zucchetti Aulla Fine - check cpccchk
                If This.bCanResize
                    i_nL=This.Parent.Left
                    i_nT=This.Parent.Top
                    *--- Zucchetti Aulla Inizio - Gestione parentela post-in attivit�
                    If (This.cStatus = "A" Or This.cStatus = "TD") And !Empty(This.cParent) And cp_YesNo("Post-it legato ad un altro, togliere il vincolo?",32,.f.)
                		This.cParent = ''
	                EndIf
                    *--- Zucchetti Aulla Fine - Gestione parentela post-in attivit�
                    If This.nConn<>0
								*--- Zucchetti Aulla Inizio - check cpccchk
								=cp_SQL(This.nConn,"update postit set postit="+cp_ToStrODBC(xcg.cText)+",px="+cp_ToStrODBC(i_nL)+;
									",py="+cp_ToStrODBC(i_nT)+", wi="+cp_ToStrODBC(This.Width)+", he="+cp_ToStrODBC(This.Height)+;
									",datestart="+cp_ToStrODBC(This.dStart)+", datestop="+cp_ToStrODBC(This.dStop)+;
									",status="+cp_ToStrODBC(This.cStatus)+",parent="+cp_ToStrODBC(This.cParent)+",cpccchk="+cp_ToStrODBC(This.cCPCCCHK)+;
									" where code="+cp_ToStrODBC(This.cVar))
                    Else
                        Update postit Set postit=xcg.cText, px=i_nL, py=i_nT, wi=This.Width, he=This.Height, datestart=This.dStart, datestop=This.dStop, Status=This.cStatus, parent=This.cParent, cpccchk=This.cCPCCCHK Where Code=This.cVar
                    EndIf
                    *--- Zucchetti Aulla Inizio - Gestione parentela post-in attivit�
                    If This.cStatus = "A" Or This.cStatus = "TD"
                    	*-- Se il postit � il padre di qualcuno chiedo se aggiornare i figli o dissociarli
						If This.nConn<>0
							=cp_SQL(This.nConn,"select Code as ChildCode from postit where Parent="+cp_ToStrODBC(This.cVar)+"And (Status='A' Or Status='TD')", "postit_parent")
						Else
							Select Code as ChildCode From postit Where Parent=This.cVar And (Status="A" Or Status="TD") Into Cursor postit_parent
						EndIf
						If Reccount()>0
							Select postit_parent
							If cp_YesNo("Aggiornare i post-in associati?",32,.f.)
								Scan
									If This.nConn<>0
										=cp_SQL(This.nConn,"update postit set postit="+cp_ToStrODBC(xcg.cText)+",px="+cp_ToStrODBC(i_nL)+;
											",py="+cp_ToStrODBC(i_nT)+", wi="+cp_ToStrODBC(This.Width)+", he="+cp_ToStrODBC(This.Height)+;
											",datestart="+cp_ToStrODBC(This.dStart)+", datestop="+cp_ToStrODBC(This.dStop)+;
											",status="+cp_ToStrODBC(This.cStatus)+",parent="+cp_ToStrODBC(This.cVar)+",cpccchk="+cp_ToStrODBC(This.cCPCCCHK)+;
											" where code="+cp_ToStrODBC(ChildCode))
				                    Else
				                        Update postit Set postit=xcg.cText, px=i_nL, py=i_nT, wi=This.Width, he=This.Height, datestart=This.dStart, datestop=This.dStop, Status=This.cStatus, parent=This.cVar, cpccchk=This.cCPCCCHK Where Code=ChildCode
				                    EndIf
			                    EndScan
							Else
								Scan
									If This.nConn<>0
										=cp_SQL(This.nConn,"update postit set parent='' where code="+cp_ToStrODBC(ChildCode))
				                    Else
				                        update postit set Parent='' where Code=ChildCode
				                    EndIf
			                    EndScan
							EndIf
						EndIf
					EndIf
					*--- Zucchetti Aulla Fine - Gestione parentela post-in attivit�
					*--- Zucchetti Aulla Fine - check cpccchk
                Else &&quando il warning � integrato non aggiorno le coordinate del form
                    i_n=This.nOldHeight
                    If This.nBtn>0
                        i_n=i_n+(This.nBtn-1)*23
                    Endif
                    If This.nConn<>0
								*--- Zucchetti Aulla Inizio - check cpccchk
								=cp_SQL(This.nConn,"update postit set postit="+cp_ToStrODBC(xcg.cText)+",he="+cp_ToStrODBC(i_n)+;
									",datestart="+cp_ToStrODBC(This.dStart)+",datestop="+cp_ToStrODBC(This.dStop)+;
									",status="+cp_ToStrODBC(This.cStatus)+",cpccchk="+cp_ToStrODBC(This.cCPCCCHK)+;
									" where code="+cp_ToStrODBC(This.cVar))
							Else
								Update postit Set postit=xcg.cText, he=i_n, datestart=This.dStart, datestop=This.dStop, Status=This.cStatus,;
									cpccchk=This.cCPCCCHK Where Code=This.cVar
							Endif
						EndIf
                    Else
						cp_ErrorMsg(MSG_POSTIT_CHANGED_BY_ANOTHER_USER)
						&&Creare una copia di backup del postit per non perdere il lavoro
						LOCAL l_codeOld
						l_codeOld = This.cVar
						This.cVar = SYS(2015)
						This.cCPCCCHK = cp_NewCCChk()
						If This.nConn<>0
							=cp_SQL(This.nConn,"insert into postit (code,usercode,created,createdby,status,postit,wi,he,px,py,parent,cpccchk) values ("+cp_ToStr(This.cVar)+","+cp_ToStr(This.nUser)+","+cp_ToStrODBC(Datetime())+","+cp_ToStr(i_Codute)+",'D','*',150,150,0,0,"+cp_ToStrODBC(This.cParent)+","+cp_ToStrODBC(This.cCPCCCHK)+")")
						Else
							Insert Into postit (Code,usercode,created,createdby,Status,postit,wi,he,px,py,parent,cpccchk) Values (This.cVar,This.nUser,Datetime(),i_Codute,'D','*',150,150,0,0,This.cParent,This.cCPCCCHK)
						Endif	
						This.relasePostIT(.T.)
						if i_ServerConn[1,2]<>0
						    =cp_SQL(i_ServerConn[1,2],"select * from postit where code="+cp_ToStrODBC(This.cVar),"postit_c")
						else
						    select * from postit where code=This.cVar into cursor postit_c
						endif  
					    m.oPostIt=createobject('postit','set',i_ServerConn[1,2], .null.)
					    m.oPostIt.oPstCnt.cStatus='D'
					    use in select("postit_c")											
					    This.cVar = l_codeOld
                    Endif
					Use In Select("postit_cpccchk")
					*--- Zucchetti Aulla Fine - check cpccchk
                Endif
            Endif
			*--- Zucchetti Aulla Inizio - check cpccchk
            *---Inserisce il postit nel folder
            If This.bQueryUnLoad
                If This.cStatus = "D" Then
                    If This.nConn<>0
                        =cp_SQL(This.nConn,"update postit set status='F' where code="+cp_ToStr(This.cVar)+" and usercode="+cp_ToStr(i_codute))
                    Else
                        Update postit Set Status='F' Where Code=This.cVar And usercode=i_codute
                    Endif
				Endif
                    For i=1 To _Screen.FormCount
                        fo=_Screen.Forms(i)
                        Do Case
                            Case Lower(fo.Class)="postitfolder"
                                fo.FillPostit()
                            Case Lower(fo.Class)="tcp_postitfolder"
                                fo.NotifyEvent("ZoomRefresh")
                        Endcase
                    Next
                Endif
			*--- Zucchetti Aulla Fine - check cpccchk
        Endif
        * --- si distrugge
        * --- Zuchetti Aulla - Invio multiplo postin (aggiunto IF)
        If Not bNoRelease
            Release (This.cVar)
        Endif
    Func IsEmpty() && Object blank if no comment, buttons and icons
        Return Empty(This.postitedt.Value) And This.nBtn=0 And This.lvwFile.ListCount=0 And At("I",This.cStatus) = 0
    Proc NewPostIT()
        * --- crea la variabile globale
        Local pName
        pName=Sys(2015)
        Public &pName
        &pName=This
        This.cVar=pName
        This.bUpdated=.T.
        This.cStatus='D'
        This.dStart = Date()
        This.dStop = Ctod('  /  /  ')
        This.nUser = i_codute
        This.nCreatedBy = i_codute
        * --- inserisce nel database dei post-it
        This.nConn=i_ServerConn[1,2]
		*--- Zucchetti Aulla Inizio - check cpccchk e parent
		This.cCPCCCHK = cp_NewCCChk()
		This.cParent = ""		
		*--- Zucchetti Aulla Fine - check cpccchk e parent
        If This.nConn<>0
            *=cp_SQL(this.nConn,"insert into postit (code,usercode,created,createdby,status,postit,wi,he) values ('"+this.cVar+"',"+alltrim(str(i_codute))+",[01-01-01],"+alltrim(str(i_codute))+",'D','*',150,150)")
            *=cp_SQL(this.nConn,"insert into postit (code,usercode,createdby,status,postit,wi,he,px,py) values ("+cp_ToStr(this.cVar)+","+cp_ToStr(i_codute)+","+cp_ToStr(i_codute)+",'D','*',150,150,0,0)")
			*--- Zucchetti Aulla Inizio - check cpccchk
			*=cp_SQL(This.nConn,"insert into postit (code,usercode,created,createdby,status,postit,wi,he,px,py) values ("+cp_ToStr(This.cVar)+","+cp_ToStr(i_codute)+","+cp_ToStrODBC(Datetime())+","+cp_ToStr(i_codute)+",'D','*',150,150,0,0)")
			=cp_SQL(This.nConn,"insert into postit (code,usercode,created,createdby,status,postit,wi,he,px,py,parent,cpccchk) values ("+cp_ToStr(This.cVar)+","+cp_ToStr(i_codute)+","+cp_ToStrODBC(Datetime())+","+cp_ToStr(i_codute)+",'D','*',150,150,0,0,"+cp_ToStrODBC(This.cParent)+","+cp_ToStrODBC(This.cCPCCCHK)+")")
			*--- Zucchetti Aulla Fine - check cpccchk
        Else
			*--- Zucchetti Aulla Inizio - check cpccchk
			*Insert Into postit (Code,usercode,created,createdby,Status,postit,wi,he,px,py) Values (This.cVar,i_codute,Datetime(),i_codute,'D','*',150,150,0,0)
			Insert Into postit (Code,usercode,created,createdby,Status,postit,wi,he,px,py,parent,cpccchk) Values (This.cVar,i_codute,Datetime(),i_codute,'D','*',150,150,0,0,This.cParent,This.cCPCCCHK)
			*--- Zucchetti Aulla Fine - check cpccchk
        Endif
		*--- Zucchetti Aulla Inizio - check cpccchk
		*--- Postit editabile
		This.bEditing = .T.
		This.bNew = .T.
		*--- Zucchetti Aulla Fine - check cpccchk
        This.Resize()
    Proc SetPostIT()
        Local pName,xcg
        pName=postit_c.Code
        Public &pName
        &pName=This
        This.cStatus=postit_c.Status
        xcg=Createobject('pxcg','load',postit_c.postit)
        This.cVar=pName
        This.Parent.Top=postit_c.py
        This.nOldTop=This.Parent.Top
        This.Parent.Left=postit_c.px
        This.nOldLeft=This.Parent.Left
        This.Parent.Width=postit_c.wi
        This.Parent.Height=postit_c.he
        This.nOldHeight=postit_c.he
        This.Height=This.Parent.Height
		This.dStart = Nvl(postit_c.datestart, {})
		This.dStop = Nvl(postit_c.datestop, {})
        This.nUser = postit_c.usercode
        This.nCreatedBy = postit_c.createdby
        This.Serialize(xcg,.F.)
        This.postitedt.Width=This.Width
        *this.shp.width=this.width
        *  --- la posizione ?
        Local l_top, l_left, l_found
        l_found = .F.
        l_top = 0
        l_left = 0
        For i=1 To _Screen.FormCount
            fo=_Screen.Forms(i)
            If Lower(fo.Class)=Lower(This.Parent.Class) And Lower(fo.oPstCnt.cVar)<>Lower(This.cVar)
                If This.Parent.Top=fo.Top And This.Parent.Left=fo.Left
                    l_found = .T.
                Endif
                l_top = Max(l_top, fo.Top)
                l_left = Max(l_left, fo.Left)
            Endif
        Next
        If l_found
            This.Parent.Top=l_top+20
            This.Parent.Left=l_left+20
        Endif
        This.bUpdated=.F.
        This.postitedt.ReadOnly=This.bReadOnly
		*--- Zucchetti Aulla Inizio - check cpccchk e parent
		This.cCPCCCHK = postit_c.cpccchk
		This.bEditing = .F.
		This.cParent = postit_c.parent
		*--- Zucchetti Aulla Fine - check cpccchk e parent
        This.Resize()
    Proc WarnInForm(oForm)
        Local pName,xcg
        *txtPostit.height = oParentObject.height
        This.bCanResize = .F.
        If Isnull(This.oParentForm)
            This.oParentForm = oForm
        Endif
        pName=postit_c.Code
        Public &pName
        &pName=This
        This.cStatus=postit_c.Status
        xcg=Createobject('pxcg','load',postit_c.postit)
        This.cVar=pName
        This.nOldHeight=postit_c.he
		This.dStart = Nvl(postit_c.datestart, {})
		This.dStop = Nvl(postit_c.datestop, {})
        This.nUser = postit_c.usercode
        This.nCreatedBy = postit_c.createdby
        This.Serialize(xcg,.T.)
        This.postitedt.Width=This.Width
		*--- Zucchetti Aulla Inizio - check cpccchk e parent
		This.cCPCCCHK = postit_c.cpccchk
		This.bEditing = .F.
		This.cParent = postit_c.parent
		*--- Zucchetti Aulla Fine - check cpccchk
        This.bUpdated=.F.
        This.Visible=.F.
        This.Resize()
        Return
    Proc Resize()
        * --- riposiziona il campo di edit e la linea di separazione
        If This.bCanResize
            This.Width=This.Parent.Width
            This.Height=This.Parent.Height
        Else
            This.Width=This.Parent.Parent.Width - IIF(TYPE("thisform.oDec")<>'O',3,0)	&&Se ho il decoratore delle form sfrutto tutto lo spazio
            This.Height= This.Parent.Parent.Height - OBJTOCLIENT(This.Parent,1)	&&Se ho il decoratore delle form sfrutto tutto lo spazio
        Endif
        This.postitedt.Width=This.Width
        *this.shp.width=this.width
        This.lvwFile.Width=This.Width
        This.lvwCtrl.Width=This.Width-20
        This.lvwCtrl.txtFile.Width=This.lvwCtrl.Width-This.lvwCtrl.btnCancel.Width
        This.lvwCtrl.btnCancel.Left=This.lvwCtrl.txtFile.Width
        This.bUpdated=.T.
        If This.nBtn = 0
            If This.nLvw > 0 Then
                If This.Height > 0 Then
                    This.lvwFile.Height = This.Height/ 4 * 3
                    This.nHeight = This.lvwFile.Height
                    This.postitedt.Top = This.Height/ 4 * 3 &&+ PCTDIFF
                    This.postitedt.Height = This.Height/ 4 &&- PCTDIFF
                Endif
            Else
                This.postitedt.Height = Iif(This.Height-23>0,This.Height-23,This.Height)
            Endif
        Endif
    Proc Serialize(xcg,bLockedForm)
        Local nLstHeight,i,nItem,cKey,cIcon,nCount,cComment,cPrg
        If xcg.bIsStoring
            xcg.Save('C',This.postitedt.Value)
            This.nHeight = Iif(This.nHeight=0,This.lvwFile.Height,This.nHeight)
            nLstHeight = Iif(This.nLvw>0,This.nHeight,0)
            xcg.Save("N",nLstHeight)
            *---Scrittura degli allegati
            nItem=This.lvwFile.ListCount
            xcg.Save("N",nItem)
            For i = 1 To nItem
                xcg.Save("C",This.lvwFile.List(i)) &&Commento
                xcg.Save("C",This.cDataTag[i]) &&Nome file
                xcg.Save("C",This.lvwFile.Picture(i))  &&Tipo di icona
            Next
            *---Scrittura dei bottoni
            xcg.Save('N',This.nBtn)
            xcg.Container(This)
            For i=1 To This.ControlCount
                If This.Controls(i).Class=='Desktopbutton'
                    xcg.Contained(This,This.Controls(i))
                Endif
            Next
            xcg.EndContainer()
            xcg.Save('N',This.BackColor)
            xcg.Save("C",This.cPageName)
            xcg.Eoc()
        Else
            This.postitedt.Value=xcg.Load('C','')
            nLstHeight = xcg.Load("N",157)
            *---Lettura degli allegati
            nCount = xcg.Load("N",0)
            This.nHeight = nLstHeight
            If bLockedForm
                nLstHeight=This.Height * 3 / 4
            Endif
            For i = 1 To nCount
                cComment=xcg.Load("C","")
                cPrg=xcg.Load("C","")
                cIcon=xcg.Load("C","")
                This.AddIcon(cComment,cPrg,cIcon,nLstHeight,.T.)
            Next
            *---Lettura dei bottoni
            This.nBtn=xcg.Load('N',130)
            If This.nBtn=0
                This.nBtnPos=This.Height
            Else
                If This.bCanResize
                    This.nBtnPos=This.Height-(This.nBtn*23)
                    This.postitedt.Height = Max(This.nBtnPos - This.nTxtPos,0)
                Endif
            Endif
            nCount=0
            xcg.Container(This)
            For i=1 To This.ControlCount
                If This.Controls(i).Class=='Desktopbutton'
                    nCount=nCount+1
                    This.ButtonResize(This.Controls(i),nCount)
                Endif
            Next
            This.BackColor=xcg.Load('N',Rgb(255,255,0))
            This.postitedt.BackColor=This.BackColor
            This.postitedt.DisabledBackColor=This.BackColor
            This.cPageName=xcg.Load("C","")
            *--- Zucchetti Aulla Inizio - Nuovi postit
            If -2=xcg.Load('N',-2)
                xcg.Eoc()
            Endif
        Endif
    Proc AddButton(cComment,cPrg,oSource)
        Local cBtn,oBtn
        This.nBtn=This.nBtn+1
        cBtn=Sys(2015)
        This.AddObject(cBtn,'desktopbutton')
        oBtn=This.&cBtn
        oBtn.cPrg=cPrg
        oBtn.cComment=cComment
        oBtn.Caption=cComment
        oBtn.FontBold=.F.
        This.ButtonResize(oBtn,This.nBtn)
        *oBtn.top=this.nBtnPos
        oBtn.SetSourceKey(oSource)
        oBtn.Visible=.T.
        *this.nBtnPos=this.nBtnPos+oBtn.height
        If This.nBtnPos>This.Height
            This.Parent.Height=This.nBtnPos
            This.Height=This.Parent.Height
        Endif
    Proc ButtonResize(oBtn,i_nBtn)
        Local l
        If This.bCanResize Then
            oBtn.Top = This.nBtnPos
            This.nBtnPos = This.nBtnPos + oBtn.Height
        Else
            This.nBtnPos = This.Height
            This.nBtnPos = This.nBtnPos - oBtn.Height
            If This.nBtnPos > 0
                oBtn.Top = This.nBtnPos
                l = oBtn.Width * (i_nBtn-1)
                If l < This.Width
                    oBtn.Left = l
                Endif
            Endif
            If This.lvwFile.ListCount > 0
                nLstHeight = This.nBtnPos * 3 / 4
                This.lvwFile.Height = nLstHeight
                This.postitedt.Top = nLstHeight
                This.nTxtPos = This.postitedt.Top
            Endif
            This.postitedt.Height = Iif(This.nBtnPos - This.nTxtPos > 0, This.nBtnPos - This.nTxtPos, 0)
        Endif
    Proc DragDrop(oSource, nXCoord, nYCoord)
		*--- Zucchetti Aulla Inizio - check cpccchk e DragDrop non gestito per i gadget
		If !This.bReadOnly And This.bEditing And oSource.ParentClass='Stdcontainer' And (Type('oSource.Parent.Parent.Parent.ParentClass')='U' Or Lower(oSource.Parent.Parent.Parent.ParentClass) <> 'stdgadgetform')
			*--- Zucchetti Aulla Fine - check cpccchk e DragDrop non gestito per i gadget
            * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
            Local L_oBj, L_Sec, L_Param, L_Prg
            L_oBj=oSource.Parent.Parent.Parent
            L_Sec=Alltrim(oSource.Parent.Parent.Parent.getSecuritycode())
            L_Prg= Alltrim(Substr(L_oBj.Class,2,Len(L_oBj.Class)-1))
            L_Param=Upper(Alltrim(Substr(L_Sec,Len(L_Prg)+2)))
            If Not Empty(Alltrim(Nvl(L_Param, ' ')))
                L_Prg = L_Prg + '("'+L_Param+'")'
            Else
                L_Prg = L_Prg
            Endif
            * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
            This.AddButton(oSource.Parent.Parent.Parent.cComment,L_Prg,oSource)
            This.bUpdated=.T.
        Endif
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
		*--- Zucchetti Aulla Inizio - check cpccchk
		If !This.bReadOnly And This.bEditing
			*--- Zucchetti Aulla Fine - check cpccchk
            Do Case
                Case nState=0   && Enter
                    This.cOldIcon=oSource.DragIcon
                    *--- Zucchetti Aulla Inizio - DragDrop non gestito per i gadget
                    If Type('oSource.Parent.Parent.Parent.ParentClass')='U' Or Lower(oSource.Parent.Parent.Parent.ParentClass) <> 'stdgadgetform'
                    *--- Zucchetti Aulla Fine - DragDrop non gestito per i gadget
	                    If oSource.ParentClass='Stdcontainer' Or oSource.Name='STDSUSR' Or oSource.Name='STDSGRP'
	                        oSource.DragIcon=i_cBmpPath+"cross01.cur"
	                    EndIf
                    *--- Zucchetti Aulla Inizio - DragDrop non gestito per i gadget
                    EndIf
                    *--- Zucchetti Aulla Fine - DragDrop non gestito per i gadget
                Case nState=1   && Leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
    Proc postitedt.InteractiveChange()
        This.Parent.bUpdated=.T.
    Proc postitedt.DragDrop(oSource,nXCoord,nYCoord)
		*--- Zucchetti Aulla Inizio - check cpccchk
		If !This.Parent.bReadOnly And This.Parent.bEditing
			*--- Zucchetti Aulla Fine - check cpccchk
			*--- Zucchetti Aulla Inizio - DragDrop non gestito per i gadget
			If Type('oSource.Parent.Parent.Parent.ParentClass')='U' Or Lower(oSource.Parent.Parent.Parent.ParentClass) <> 'stdgadgetform'
			*--- Zucchetti Aulla Fine - DragDrop non gestito per i gadget
	            If oSource.ParentClass='Stdcontainer'
	                This.Value=This.Value+' '+Trim(oSource.Parent.Parent.Parent.cComment)
	                For i=1 To Alen(oSource.xKey)
	                    This.Value=This.Value+','+Trim(cp_ToStr(oSource.xKey[i],1))
	                Next
	                * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
	                Local L_oBj, L_Sec, L_Param, L_Prg
	                L_oBj=oSource.Parent.Parent.Parent
	                L_Sec=Alltrim(oSource.Parent.Parent.Parent.getSecuritycode())
	                L_Prg= Alltrim(Substr(L_oBj.Class,2,Len(L_oBj.Class)-1))
	                L_Param=Upper(Alltrim(Substr(L_Sec,Len(L_Prg)+2)))
	                If Not Empty(Alltrim(Nvl(L_Param, ' ')))
	                    L_Prg = L_Prg + '("'+L_Param+'")'
	                Else
	                    L_Prg = L_Prg
	                Endif
	                * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
	                This.Parent.AddButton(oSource.Parent.Parent.Parent.cComment,L_Prg,oSource)
	                This.Parent.bUpdated=.T.
	            EndIf
            *--- Zucchetti Aulla Inizio - DragDrop non gestito per i gadget
            EndIf
            *--- Zucchetti Aulla Fine - DragDrop non gestito per i gadget
            If oSource.Name='STDSUSR'
                *this.value=this.value+' '+trim(_cpusers_.name)
                This.Parent.SendTo(_cpusers_.Code,_cpusers_.Name)
            Endif
            If oSource.Name='STDSGRP'
                *this.value=this.value+' '+trim(_cpusers_.name)
                This.Parent.SendToGroup(_cpgroups_.Code,_cpgroups_.Name)
            Endif
        Endif
    Proc postitedt.DragOver(oSource,nXCoord,nYCoord,nState)
        This.Parent.DragOver(oSource,nXCoord,nYCoord,nState)
    Proc postitedt.OLEDragOver(oDataObject,nEffect,nButton,nShift,nXCoord,nYCoord,nState)
		*--- Zucchetti Aulla Inizio - check cpccchk
		If !This.Parent.bReadOnly And This.Parent.bEditing And (oDataObject.GetFormat(15) Or oDataObject.GetFormat(1))
			*--- Zucchetti Aulla Fine - check cpccchk
            If This.Parent.nLvw=0
                This.OLEDropHasData=1
                This.OLEDropEffects=5
            Else
                If oDataObject.GetFormat(1)
                    This.OLEDropHasData=1
                    This.OLEDropEffects=1
                Endif
            Endif
        Endif
    Proc postitedt.OLEDragDrop(oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord)
        If This.OLEDropEffects>1
            This.Parent.FormOLEDragDrop(oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord)
            This.Parent.Resize()
        Endif
        *** Zucchetti Aulla Inizio - frasi modello
        *Inserito gestione query da postit
        currentEvent=''
    Func IsVar(cVarName)
        Return(Type(cVarName)<>'U')
    Func GetVar(cVarName)
        Return(&cVarName)
    Proc SetVar(cVarName,xValue)
        &cVarName=xValue
        Return
    Procedure NotifyEvent(cEvent)
        Return
        *** Zucchetti Aulla Fine - frasi modello
    Proc postitedt.MouseDown(nButton, nShift, nXCoord, nYCoord)
        *** Zucchetti Aulla Inizio - frasi modello
        If  Not This.Parent.bdisablefrasimodello
            **Per gestire il doppio click sul campo memo e visualizzare la tabella delle frasi modello
            If nButton=1
                If Seconds()-0.3<This.Parent.nSeconds
                    ptipo='P'
                    pgest=''
                    If Type('this.Parent.Parent.alwaysontop')<>'U'
                        This.Parent.Parent.AlwaysOnTop=.F.
                    Endif
                    Do cp_zoom With 'FRA_MODE','*','FMSERIAL',"this.parent.parent.parent.oparentobject.postitedt",.F.,'GSUT_AFM',cp_Translate(MSG_PHRASE_MODEL),'GSUT_AFM.FRA_MODE_VZM',This.Parent
                Endif
                This.Parent.nSeconds=Seconds()
            Endif
            *** Zucchetti Aulla Fine - frasi modello
            If nButton=1
                Nodefault
                * --- per ritardare un po il drag e drop
                This.Parent.nMouseDownX=nXCoord
                This.Parent.nMouseDownY=nYCoord
            Endif
            *** Zucchetti Aulla Inizio - frasi modello
            oCpToolBar.b9.Enabled=.T.
        Endif

    Proc postitedt.mZoom()
        *passo propriet� per parametri query
		*--- Zucchetti Aulla Inizio - check cpccchk
		If !This.Parent.bReadOnly And This.Parent.bEditing And Not This.Parent.bdisablefrasimodello
			*--- Zucchetti Aulla Fine - check cpccchk
            ptipo='P'
            pgest=''
            If Type('this.Parent.Parent.alwaysontop')<>'U'
                This.Parent.Parent.AlwaysOnTop=.F.
            Endif
            Do cp_zoom With 'FRA_MODE','*','FMSERIAL',"this.parent.parent.parent.oparentobject.postitedt",.F.,'GSUT_AFM',cp_Translate(MSG_PHRASE_MODEL),'GSUT_AFM.FRA_MODE_VZM',This.Parent
        Endif
        Return

    Proc postitedt.Keyf9()
        **Per gestire il doppio click sul campo memo e visualizzare la tabella delle frasi modello
        Nodefault
        ptipo='P'
        pgest=''
        **I Postin integrati in un form non settano la propriet� alwaysontop (i form hanno gi� la propriet� settata)
        If (Type("this.Parent.Parent.alwaysontop")<>'U')
            This.Parent.Parent.AlwaysOnTop=.F.
        Endif
        Do cp_zoom With 'FRA_MODE','*','FMSERIAL',"this.parent.parent.parent.oparentobject.postitedt",.F.,'GSUT_AFM',cp_Translate(MSG_PHRASE_MODEL),'GSUT_AFM.FRA_MODE_VZM',This.Parent
        Return
        *** Zucchetti Aulla fine - frasi modello

    Proc postitedt.MouseMove(nButton, nShift, nXCoord, nYCoord)
		*--- Zucchetti Aulla Inizio - check cpccchk
		If !This.Parent.bReadOnly And This.Parent.bEditing And nButton=1 And (Abs(nXCoord-This.Parent.nMouseDownX)+Abs(nYCoord-This.Parent.nMouseDownY))>8
			*--- Zucchetti Aulla Fine - check cpccchk
            Nodefault
            This.DragIcon=i_cBmpPath+"DragMove.cur"
            This.Drag(1)
        Endif
    Proc lvwFile.OLEDragOver(oDataObject,nEffect,nButton,nShift,nXCoord,nYCoord,nState)
        If oDataObject.GetFormat(15) Or oDataObject.GetFormat(1)
            This.OLEDropEffects=7
            This.OLEDropHasData=1
        Endif
    Proc lvwFile.OLEDragDrop(oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord)
        This.Parent.FormOLEDragDrop(oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord)
    Proc lvwFile.DragDrop(oSource,nXCoord,nYCoord)
        If oSource.ParentClass='Stdcontainer'
            This.Parent.postitedt.Value=This.Parent.postitedt.Value+' '+Trim(oSource.Parent.Parent.Parent.cComment)
            For i=1 To Alen(oSource.xKey)
                This.Parent.postitedt.Value=This.Parent.postitedt.Value+','+Trim(cp_ToStr(oSource.xKey[i],1))
            Next
            * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
            Local L_oBj, L_Sec, L_Param, L_Prg
            L_oBj=oSource.Parent.Parent.Parent
            L_Sec=Alltrim(oSource.Parent.Parent.Parent.getSecuritycode())
            L_Prg= Alltrim(Substr(L_oBj.Class,2,Len(L_oBj.Class)-1))
            L_Param=Upper(Alltrim(Substr(L_Sec,Len(L_Prg)+2)))
            If Not Empty(Alltrim(Nvl(L_Param, ' ')))
                L_Prg = L_Prg + '("'+L_Param+'")'
            Else
                L_Prg = L_Prg
            Endif
            * Zucchetti Aulla Fine - Anche gestioni con parametri nei bottoni
            This.Parent.AddButton(oSource.Parent.Parent.Parent.cComment,L_Prg,oSource)
            This.Parent.bUpdated=.T.
        Endif
        If oSource.Name='STDSUSR'
            *this.value=this.value+' '+trim(_cpusers_.name)
            This.Parent.SendTo(_cpusers_.Code,_cpusers_.Name)
        Endif
        If oSource.Name='STDSGRP'
            *this.value=this.value+' '+trim(_cpusers_.name)
            This.Parent.SendToGroup(_cpgroups_.Code,_cpgroups_.Name)
        EndIf
    Proc lvwFile.DragOver(oSource,nXCoord,nYCoord,nState)
        This.Parent.DragOver(oSource,nXCoord,nYCoord,nState)
    Proc lvwFile.DblClick()
        If Not Empty(This.Parent.cItemTag)
            This.Parent.ItemClick(This.Parent.cItemTag)
            This.Parent.cItemTag=''
        Endif
    Proc lvwFile.MouseDown(nButton, nShift, nXCoord, nYCoord)
        If nButton=2
            This.nY=nYCoord
        Endif
    Proc lvwFile.MouseUp(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            j=This.ListIndex
            If j<>0
                This.Parent.cItemTag =This.Parent.cDataTag[j]
            Endif
        Else
            DoDefault(nButton, nShift, nXCoord, nYCoord)
        Endif
    Proc Activate()
        *i_curform=this
        oCpToolBar.Enable(.F.)
        oCpToolBar.b2.Enabled=.T.
        oCpToolBar.b5.Enabled=.T.
        *** Zucchetti Aulla inizio - frasi modello
        oCpToolBar.b9.Enabled=.T.
        *** Zucchetti Aulla Fine
        *oCpToolBar.b13.enabled=.t.
    Proc Deactivate()
        *i_curform=.NULL.
        oCpToolBar.Enable(.F.)
        * --- Zucchetti Aulla - Invio Postit a gruppi, aggiunto parametro bNoRequest
    Proc SendTo(nUsercode,cName,bNoRequest)
        If This.cStatus='D'
            If i_codute=nUsercode
                cp_msg(cp_Translate(MSG_YOU_CANNOT_SEND_MSG_TO_YOURSELF),.F.)
            Else
                * --- Zucchetti Aulla Aggiuno bNORequest or (invio postit a gruppo)
                If bNoRequest Or cp_YesNo(cp_MsgFormat(MSG_SEND_POSTIN_TO__C__,cp_ToStr(nUsercode),cName),32,.f.)
                    If This.nConn<>0
                        =cp_SQL(This.nConn,"update postit set usercode="+cp_ToStr(nUsercode)+", status='P' where code="+cp_ToStr(This.cVar))
                    Else
                        Update postit Set usercode=nUsercode,Status='P' Where Code=This.cVar
                    Endif
                    This.cStatus='P'
                    This.Parent.Release()
                Endif
            Endif
        Else
            cp_msg(cp_Translate(MSG_WHEN_A_POSTIN_IS_ADDED_TO_A_RECORD_IT_CANNOT_BE_SENT_TO_ANOTHER_USER),.F.)
        Endif
        * --- Zucchetti Aulla - Invio Postit a gruppi, aggiunto parametro bNoRequest
    Proc SendToGroup(nGroupcode,cName,bNoRequest,cCurUsers)
        If This.cStatus='D'
            * --- Zucchetti Aulla Aggiuno bNORequest or (invio postit a gruppo)
            * --- sostituita la Update..
            If bNoRequest Or cp_YesNo(cp_MsgFormat(MSG_SEND_POSTIN_TO__C__,cp_ToStr(nGroupcode),cName),32,.f.)
                * --- Zucchetti Aulla Inizio - Se trascino un gruppo sul post it invio il post it a tutti gli utenti del gruppo..
                * --- creo un temporaneo con gli utenti che appartengono al gruppo
                Private cCurs,i_OldArea, nCode, bFirst, nUsercode
                cCurs=Sys(2015)
                i_OldArea=Select()
                bFirst=.T.
                *--- Se passo io il cursore con la lista degli utenti nn leggo dal db
                If Vartype(cCurUsers)<>'C'
                    If This.nConn<>0
                        cp_sqlexec(This.nConn,"select * from CpUsrGrp Where groupcode="+cp_ToStrODBC(nGroupcode),cCurs)
                    Else
                        Select * From CpUsrGrp Where groupcode=nGroupcode Into Cursor (cCurs )nofilter
                    Endif
                Else
                    cCurs=cCurUsers
                Endif
                Select(cCurs)
                Go Top
                Scan
                    nUsercode=usercode
                    * --- Il post it aperto lo assegno al primo utente..
                    If bFirst
                        If This.nConn<>0
                            =cp_SQL(This.nConn,"update postit set usercode="+cp_ToStr(nUsercode)+", status='P' where code="+cp_ToStr(This.cVar))
                        Else
                            Update postit Set usercode=nUsercode,Status='P' Where Code=This.cVar
                        Endif
                        bFirst=.F.
                        This.bQueryUnLoad=.T.
                        This.cStatus='P'
                        This.relasePostIT(.T.)
                    Else
                        * --- Per ogni utente duplico il Postin e lo invio...
                        * --- Raddoppio il post it direttamente sul database..
                        nCode=Sys(2015)
                        If This.nConn<>0
                            =cp_SQL(This.nConn,"Insert Into Postit (code,usercode, created , createdby  , postit  , status, "+;
                                "px, py ,wi,he, datestart,  datestop ) "+;
                                " SELECT "+cp_ToStrODBC(nCode)+" As code , "+cp_ToStr(nUsercode)+" As usercode , "+;
                                "created , createdby  , postit  , "+cp_ToStr('P')+" As  status,"+;
                                "px, py ,wi,he, datestart,  datestop FROM Postit WHERE code="+cp_ToStr(This.cVar))
                        Else
                            #If Version(5)>=900
                                Insert Into postit(Code,usercode, created , createdby  , postit  , Status,;
                                    px, py ,wi,he, datestart,  datestop)  ;
                                    SELECT nCode As Code , nUsercode As usercode , created , createdby  , postit  ,  'P',;
                                    px, py ,wi,he, datestart,  datestop From postit Where Code=This.cVar
                            #Else
                                Local cCursName
                                cCursName=Sys(2015)
                                Select nCode As Code , nUsercode As usercode , created , createdby  , postit  ,  'P',;
                                    px, py ,wi,he, datestart,  datestop From postit Where Code=This.cVar Into Cursor &cCursName
                                Select (cCursName)
                                Scatter
                                Select postit
                                Append Blank
                                Gather
                                Select (cCursName)
                                Use
                                Select (postit)
                                Use
                            #Endif
                        Endif
                    Endif
                    Select(cCurs)
                Endscan
                * --- Chiudo il post it se almeno un utente nel gruppo
                If Not bFirst
                    This.cStatus='P'
					Release (This.cVar)
                    This.Parent.Release()
                Endif
                Use In (cCurs)
                Select( i_OldArea )
                * --- Zucchetti Aulla Fine - Se trascino un gruppo sul post it invio il post it a tutti gli utenti del gruppo..
                * --- vecchio codice
                *!*	       if this.nConn<>0
                *!*	          =cp_SQL(this.nConn,"update postit set groupcode="+cp_ToStr(nGroupCode)+", status='P' where code="+cp_ToStr(this.cVar))
                *!*	        else
                *!*	          update postit set groupcode=nGroupCode,status='P' where code=this.cVar
                *!*	        endif
                *!*	        this.cStatus='P'
                *!*	        this.parent.Release()
            Endif
        Else
            cp_msg(cp_Translate(MSG_WHEN_A_POSTIN_IS_ADDED_TO_A_RECORD_IT_CANNOT_BE_SENT_TO_ANOTHER_USER),.F.)
        Endif
    Func ecpDelete()
        If !This.bReadOnly And cp_YesNo(MSG_CONFIRM_DELETING_POSTIN_QP)
			*--- Zucchetti Aulla Inizio - check cpccchk
			*--- rileggo cpccchk
			If This.nConn<>0
				=cp_SQL(This.nConn,"select cpccchk from postit where code="+cp_ToStrODBC(This.cVar), "postit_cpccchk")
			Else
				Select cpccchk From postit Where Code=This.cVar Into Cursor postit_cpccchk
			Endif
			If postit_cpccchk.cpccchk == This.cCPCCCHK
				*--- Zucchetti Aulla Fine - check cpccchk
            If This.nConn<>0
                cp_SQL(This.nConn,"delete from postit where code="+cp_ToStr(This.cVar))
                cp_sqlexec(This.nConn,"delete from cpwarn where warncode='"+This.cVar+"'") && in db2 CP_SQL da un messaggio di errore
            Else
                Delete From postit Where Code=This.cVar
                Delete From cpwarn Where warncode=This.cVar
            Endif
            This.bDeleted=.T.
            *-- Se vengo da GadgetCalendar devo aggiornarlo
            If Type("This.Parent.cGadgetParent")="C" And Type("oGadgetManager")="O"
            	Local oParent, nList
            	nList = oGadgetManager.aGadget.GetKey(This.Parent.cGadgetParent)
            	If m.nList > 0
            		oParent = oGadgetManager.aGadget[This.Parent.cGadgetParent]
	            	oParent.NotifyEvent("CalendarUpdate")
	            	oParent = .Null.
            	EndIf
            EndIf
            Return(.T.)
            *this.Release()
			Else
				cp_ErrorMsg(MSG_POSTIT_CHANGED_BY_ANOTHER_USER)
				&&Creare una copia di backup del postit per non perdere il lavoro
			Endif
			Use In Select("postit_cpccchk")
			*--- Zucchetti Aulla Fine - check cpccchk
        Endif
        Return(.F.)
    Func ecpDelete_Vuoto()
		If This.bEditing
			*--- Zucchetti Aulla Inizio - check cpccchk
			*--- rileggo cpccchk
			If This.nConn<>0
				=cp_SQL(This.nConn,"select cpccchk from postit where code="+cp_ToStrODBC(This.cVar), "postit_cpccchk")
			Else
				Select cpccchk From postit Where Code=This.cVar Into Cursor postit_cpccchk
			Endif
			If postit_cpccchk.cpccchk == This.cCPCCCHK
        If This.nConn<>0
            cp_SQL(This.nConn,"delete from postit where code="+cp_ToStr(This.cVar))
            SQLExec(This.nConn,"delete from cpwarn where warncode='"+This.cVar+"'") && in db2 CP_SQL da un messaggio di errore
        Else
            Delete From postit Where Code=This.cVar
            Delete From cpwarn Where warncode=This.cVar
        Endif
        This.bDeleted=.T.
        Return(.T.)
			Else
				cp_ErrorMsg(MSG_POSTIT_CHANGED_BY_ANOTHER_USER)
			Endif
		Endif
    Proc ecpEdit()
		*--- Zucchetti Aulla Inizio - check cpccchk
		If !This.bReadOnly And This.bEditing
			*--- Zucchetti Aulla Fine - check cpccchk
            Local oPostitProp
            oPostitProp=Createobject('PostitProp',This,This.BackColor,This.dStart,This.dStop,This.nCreatedBy,This.nUser,Iif(Not Empty(This.cNewStatus),This.cNewStatus,This.cStatus),This.cPageName)
            oPostitProp.Show()
        Endif
        Return
    Proc ReportValue(i_nBkColor,i_dPstStart,i_dPstStop,i_cPostitStatus,i_cPage)
        This.BackColor=i_nBkColor
        This.postitedt.BackColor=i_nBkColor
        This.cPageName=i_cPage
        If Not Empty(i_cPostitStatus)
            If At("I",i_cPostitStatus) <> 0 And Len(i_cPostitStatus)=1
                This.cNewStatus="W"+i_cPostitStatus
            Else
                This.cNewStatus=i_cPostitStatus
            Endif
            If This.cStatus = "W" Or This.cStatus = "A" Or At("I",This.cStatus) <> 0 Then
                If Not This.cStatus==This.cNewStatus
                    This.cStatus = This.cNewStatus
                    If At("I",This.cStatus) <> 0
                        cp_msg(cp_Translate(MSG_POSTIN_WILL_BE_INTEGRATED_IN_THE_WINDOW_WHEN_NEXT_RECORD_WILL_BE_READ_F),.F.)
                    Else
                        cp_msg(cp_Translate(MSG_POSTIN_WILL_BE_DETACHED_FROM_THE_WINDOW_WHEN_NEXT_RECORD_WILL_BE_READ_F),.F.)
                    Endif
                Endif
            Endif
        Endif
        This.dStart = i_dPstStart
        This.dStop = i_dPstStop
        This.bUpdated = .T.
        Return
    Proc AddIcon(cComment, cKey, cIcon, nHeight, Block)
        Local nIdx,i,i_l,i_cExt
        This.lvwFile.AddListItem(cComment)
        If This.lvwFile.ListCount = 1
            If Empty(This.postitedt.Value)
                This.postitedt.Value= cp_MsgFormat(MSG_ENCLOSURE_A___A,cComment)
            Endif
            This.lvwFile.Left = 0
            This.lvwFile.Top = 0
            This.lvwFile.Width = This.Width
            This.lvwFile.Height = nHeight
            This.postitedt.Top = nHeight
            This.nTxtPos = This.postitedt.Top
            If Not Block
                This.postitedt.Height = Max(This.Height - nHeight,0)
            Endif
            This.nBtnPos = Iif(Block, This.nDimension, This.Height)
            *---Posiziona l'eventuale linea
            *If this.nBtn <> 0
            *this.shp.top = this.nBtnPos
            *this.shp.width = this.width
            *Endif
            *---Sposta gli eventuali bottoni
            For i=1 To This.ControlCount
                If Lower(This.Controls(i).Class)=="desktopbutton"
                    This.Controls(i).Top=This.nBtnPos
                    This.nBtnPos = This.nBtnPos + This.Controls(i).Height
                Endif
            Next
            If This.nBtnPos > This.Height And This.bCanResize
                This.Parent.Height=This.nBtnPos
                This.Height=This.Parent.Height
            Endif
            If Block Then
                This.postitedt.Height = Max(This.Height - nHeight,0)
            Endif
            This.nLvw = This.nLvw + 1
            This.lvwFile.Visible = .T.
        Endif
        *---Salvataggio della chiave e del tipo di icona
        If Vartype(i_ThemesManager)=='O'
            i_ThemesManager.PutBmp( This.lvwFile, m.cIcon , 16)
        Else
            This.lvwFile.Picture(This.lvwFile.ListCount)=m.cIcon
        Endif
        Dimension This.cDataTag[this.lvwFile.ListCount]
        *---Se 'VQR' tolgo la path
        If At(Curdir(),Upper(cKey))<>0 And At('.VQR',Upper(cKey))<>0
            cKey=Substr(cKey,Rat('\',cKey)+1)
        Endif
        This.cDataTag[this.lvwFile.ListCount]=cKey
        This.bUpdated=.T.
        Return
    Proc ItemClick(cTag)
        Local cExt,i_obj
        cExt=Upper(Substr(cTag,Rat(".",cTag)+1))
        Do Case
            Case cExt="VQR"
                *---Eseguo la Visual Query
                vx_exec(cTag,This) &&NOTE: se � integrato sarebbe bello passargli il puntatore al padre
            Otherwise
                ShellEx(cTag)
        Endcase
        Return
    Proc FormOLEDragDrop(oDataObject, nEffect, nButton, nShift, nXCoord, nYCoord)
        Local vFN, sFile, sExt, eValidExt, sIcon, sURL,i_aFiles
		*--- Zucchetti Aulla Inizio - check cpccchk
		If !This.bReadOnly And This.bEditing
			*--- Zucchetti Aulla Fine - check cpccchk
            If oDataObject.GetFormat(15)
                *---D&D iniziato da file
                Dimension i_aFiles[1]
                i_aFiles[1]=''
                oDataObject.GetData(15,@i_aFiles)
                For i=1 To Alen(i_aFiles)
                    sFile=i_aFiles[i]
                    sExt = Upper(Substr(sFile,Rat(".",sFile)))
                    Do Case
                        Case sExt=".DOC"
                            sIcon=i_cBmpPath+'psword.bmp'
                        Case sExt=".XLT" Or sExt=".XLS"
                            sIcon=i_cBmpPath+'psexcel.bmp'
                        Case sExt=".HTM" Or sExt=".HTML" Or sExt=".XML"
                            sIcon=i_cBmpPath+'psnet.bmp'
                        Case sExt=".TXT"
                            sIcon=i_cBmpPath+'pstext.bmp'
                        Case sExt=".BMP"
                            sIcon=i_cBmpPath+'psbmp.bmp'
                        Case sExt=".GIF"
                            sIcon=i_cBmpPath+'psbmp.bmp'
                        Case sExt=".JPG"
                            sIcon=i_cBmpPath+'psbmp.bmp'
                        Case sExt=".AVI"
                            sIcon=i_cBmpPath+'psavi.bmp'
                        Case sExt=".VQR"
                            sIcon=i_cBmpPath+'psvqr.bmp'
                        Case sExt=".MPP" Or sExt=".MPT"
                            sIcon=i_cBmpPath+'psPrj.bmp'
                        Case sExt=".PDF"
                            sIcon=i_cBmpPath+'psPdf.bmp'
                        Otherwise
                            sIcon=i_cBmpPath+'psUnk.bmp'
                    Endcase
                    This.AddIcon(Substr(sFile, Rat("\", sFile) + 1), sFile, sIcon,Iif(This.nBtn <> 0 Or Not This.bCanResize, This.Height/ 4 * 3, This.nDimension  / 4 * 3), .F.)
                Next
            Else
                If oDataObject.GetFormat(1)
                    sURL = oDataObject.GetData(1)
                    If At("http",Lower(sURL)) <> 0
                        *sIcon = "Internet"
                        *sIcon=i_cBmpPath+'bitmap.bmp'
                        sIcon='internet.bmp'
                        This.AddIcon(sURL, sURL, sIcon, Iif(This.nBtn <> 0 Or Not This.bCanResize, This.Height / 4 * 3, This.nDimension / 4 * 3), .F.)
                    Endif
                Endif
            Endif
        Endif
        *** Zucchetti Aulla inizio -frasi modello
        *passo il valore del campo memo al campo (simulando un link sulla maschera)
    Procedure postitedt.Ecpdrop(oSource)
        Local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt,i_n
        i_n=cp_OpenTable('FRA_MODE')
        i_nConn = i_TableProp[i_n,3]
        i_cTable = cp_SetAzi(i_TableProp[i_n,2])
        i_nArea = Select()
        i_bEmpty = .F.
        If Used("_Link_")
            Select _Link_
            Use
        Endif

        If .Not. Empty(oSource.xKey(1))
            If i_nConn<>0
                i_ret=cp_SQL(i_nConn,"select FMSERIAL,FM__MEMO,FMPSTCOL";
                    +" from "+i_cTable+" where FMSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                    ,"_Link_")
                i_reccount = Iif(i_ret=-1,0,Reccount())
            Else
                i_cWhere = cp_PKFox(i_cTable;
                    ,'FMSERIAL',oSource.xKey(1))
                Select FMSERIAL,FM__MEMO,FMPSTCOL;
                    from (i_cTable) Where &i_cWhere. Into Cursor _Link_
                i_reccount = _Tally
            Endif
        Else
            i_reccount = 0
        Endif
        If i_reccount>0 And Used("_Link_")
            This.Value=Stuff(This.Value, this.selstart+1,this.SelLength , Nvl(_Link_.FM__MEMO,Space(0)))
            *--- Colore post-in
            This.Parent.BackColor = Nvl(_Link_.FMPSTCOL, g_PostInColor)
            This.BackColor = Nvl(_Link_.FMPSTCOL, g_PostInColor)
            This.DisabledBackColor = Nvl(_Link_.FMPSTCOL, g_PostInColor)
        Else
            This.Value = Space(0)
        Endif
        cp_CloseTable('FRA_MODE')
        Select (i_nArea)
    Endproc
    *** Zucchetti Aulla Fine frasi modello
Enddefine
Proc LoadPostIT
    Local p,bCancel,dStop
    If i_ServerConn[1,2]<>0
        =cp_SQL(i_ServerConn[1,2],"select * from postit where usercode="+cp_ToStr(i_codute)+" and (status='D' or status='P' Or Status='TD' Or Status='A') and (datestart <= "+cp_ToStrODBC(i_dPostInFilterDate)+" or datestart IS NULL) and (datestop >= "+cp_ToStrODBC(i_dPostInFilterDate)+" or datestop IS NULL)","postit_c")
    Else
        *select * from postit where (usercode=i_codute OR (groupcode in (select groupcode FROM cpusrgrp WHERE usercode=i_codute))) and (status='D' or status='P') and (datestart<=Date() or datestart IS NULL) into cursor postit_c
        Select * From postit Where (usercode=i_codute) And (Status='D' Or Status='P' Or Status='TD' Or Status='A') And (TTOD(datestart)<=i_dPostInFilterDate Or datestart Is Null) And (datestop<=i_dPostInFilterDate Or datestop Is Null) Into Cursor postit_c
    Endif
    If Used('postit_c')
        Go Top
        Scan
            bCancel=.F.
            dStop = Iif(Isnull(postit_c.datestop) Or postit_c.datestop=Ctod('  /  /  '), i_dPostInFilterDate, postit_c.datestop)
            * --- Zucchetti Aulla - Revisione Postiin
            If Vartype(g_Alertmanager)<>'O' Or ( Vartype(i_bDisableNewPostIt)='L' And i_bDisableNewPostIt)
                * --- Zucchetti Aulla - Revisione Postiin
                p=Createobject('postit','set',i_ServerConn[1,2])
                * --- Zucchetti Aulla - Revisione Postiin
            Else
                p = g_Alertmanager.AddAlert("cp_AdvPostIt", 'set', i_ServerConn[1,2])
                p.DoAlert()
            Endif
            * --- Zucchetti Aulla - Revisione Postiin
            If dStop<i_dPostInFilterDate
                *---Post-IN scaduto
                If cp_YesNo(MSG_POSTIN_EXPIRED_QM+Chr(10)+Chr(13)+MSG_DELETE_IT_QP) Then
                    bCancel = .T.
                Endif
            Endif
            If Not bCancel
                If Vartype(p.oPstCnt)<>'U'
                    p.oPstCnt.cStatus='D'
                Else
                    p.cStatus = 'D'
                Endif
                If postit_c.Status='P'
                    If i_ServerConn[1,2]<>0
                        =cp_SQL(i_ServerConn[1,2],"update postit set status='D' where code='"+postit_c.Code+"'")
                    Else
                        Update postit Set Status='D' Where Code=postit_c.Code
                    Endif
                Endif
            Else
                p.Release()
                If i_ServerConn[1,2]<>0
                    =cp_SQL(i_ServerConn[1,2],"delete from postit where code="+cp_ToStr(postit_c.Code))
                Else
                    Delete From postit Where Code=postit_c.Code
                Endif
            Endif
        Endscan
        Select ('postit_c')
        Use
    Endif
Endproc

Define Class postitbiff As Timer
    *--- Zucchetti Aulla Inizio - gestione minuti Post-IN
    Interval=Iif(Vartype(g_MinutiPostIN)='N',g_MinutiPostIN,10)*60*1000
    *--- Zucchetti Aulla Fine - gestione minuti Post-IN
    Proc Timer()
        Local oldarea,p,i_cnt, nDimension
        oldarea=Select()
        If i_ServerConn[1,2]<>0
            =cp_SQL(i_ServerConn[1,2],"select count(*) as cnt from postit where usercode="+cp_ToStr(i_codute)+" and status='P'","postit_b")
        Else
            Select Count(*) As Cnt From postit Where usercode=i_codute And Status='P' Into Cursor postit_b
        Endif
        If Used('postit_b')
            If Type('postit_b.cnt')='C'
                i_cnt=Val(postit_b.Cnt)
            Else
                i_cnt=postit_b.Cnt
            Endif
            If i_cnt<>0
                *--- Zucchetti Aulla Inizio - gestione minuti Post-IN
                If Vartype(g_ModoPostIN)='N' And g_ModoPostIN=2
                    This.GetMail()
                Else
                    *--- Zucchetti Aulla Fine - gestione minuti Post-IN
                    cp_msg(cp_Translate(MSG_RECEIVING_MAIL_D))
                    If Vartype(i_ThemesManager)=='O'
                        Do Case
                            Case i_nTBTNH>30
                                m.nDimension = 32
                            Case i_nTBTNH>22
                                m.nDimension = 24
                            Otherwise
                                m.nDimension = 16
                        Endcase
                        i_ThemesManager.PutBmp ( odesktopbar.postitbiff , i_cBmpPath+'mailup.bmp' , m.nDimension )
                    Else
                        odesktopbar.postitbiff.Picture=i_cBmpPath+'mailup.bmp'
                    Endif
                    *--- Zucchetti Aulla Inizio - gestione minuti Post-IN
                Endif
                *--- Zucchetti Aulla Fine - gestione minuti Post-IN
            Endif
            Select postit_b
            Use
        Endif
        Select (oldarea)
    Proc GetMail()
        Local oldarea,p,bCancel,dStop
        oldarea=Select()
        If i_ServerConn[1,2]<>0
            =cp_SQL(i_ServerConn[1,2],"select * from postit where usercode="+cp_ToStr(i_codute)+" and (Status='P' or Status='A' or Status='TD') and (datestart >= "+cp_ToStrODBC(TTOD(i_dPostInFilterDate))+" or datestart IS NULL)","postit_c")
        Else
            *select * from postit where (usercode=i_codute OR (groupcode in (select groupcode FROM cpusrgrp WHERE usercode=i_codute))) and status='P' and (datestart<=Date() or datestart IS NULL) into cursor postit_c
            Select * From postit Where (usercode=i_codute) And (Status='P' or Status='A' or Status='TD') And (datestart<=i_dPostInFilterDate Or datestart Is Null) Into Cursor postit_c
        Endif
        If Used('postit_c')
            Go Top
            Scan
                bCancel=.F.
                dStop = Iif(Isnull(postit_c.datestop) Or postit_c.datestop=Ctod('  /  /  '), i_dPostInFilterDate, postit_c.datestop)
                * --- Zucchetti Aulla - Revisione Postiin
                If Vartype(g_Alertmanager)<>'O' Or ( Vartype(i_bDisableNewPostIt)='L' And i_bDisableNewPostIt)
                    * --- Zucchetti Aulla - Revisione Postiin
                    p=Createobject('postit','set',i_ServerConn[1,2])
                    * --- Zucchetti Aulla - Revisione Postiin
                Else
                    p = g_Alertmanager.AddAlert("cp_AdvPostIt", 'set', i_ServerConn[1,2])
                    p.DoAlert()
                Endif
                * --- Zucchetti Aulla - Revisione Postiin
                If dStop<i_dPostInFilterDate
                    *---Post-IN scaduto
                    If cp_YesNo(MSG_POSTIN_EXPIRED_QM+Chr(10)+Chr(13)+MSG_DELETE_IT_QP) Then
                        bCancel = .T.
                    Endif
                Endif
                If Not bCancel
                    p.oPstCnt.cStatus='D'
                    If i_ServerConn[1,2]<>0
                        =cp_SQL(i_ServerConn[1,2],"update postit set status='D' where code="+cp_ToStr(postit_c.Code)+" and usercode="+cp_ToStr(i_codute))
                    Else
                        Update postit Set Status='D' Where Code=postit_c.Code And usercode=i_codute
                    Endif
                Else
                    p.Release()
                    If i_ServerConn[1,2]<>0
                        =cp_SQL(i_ServerConn[1,2],"delete from postit where code="+cp_ToStr(postit_c.Code))
                    Else
                        Delete From postit Where Code=postit_c.Code
                    Endif
                Endif
            Endscan
            Select ('postit_c')
            Use
        Endif
        Select (oldarea)
        If Vartype(i_ThemesManager)=='O'
            Do Case
                Case i_nTBTNH>30
                    m.nDimension = 32
                Case i_nTBTNH>22
                    m.nDimension = 24
                Otherwise
                    m.nDimension = 16
            Endcase
            i_ThemesManager.PutBmp ( odesktopbar.postitbiff , i_cBmpPath+'maildn.bmp' , m.nDimension )
        Else
            odesktopbar.postitbiff.Picture=i_cBmpPath+'maildn.bmp'
        Endif
Enddefine

Define Class PostitProp As Form
    Top=10
    Left=10
    Width=320
    Height=290
    MinHeight=100
    BackColor=Rgb(192,192,192)
    Caption=''
    WindowType=1
    Icon=i_cBmpPath+i_cStdIcon
    bUpdated=.F.
    w_txtStart=Ctod('  /  /  ')
    w_txtStop=Ctod('  /  /  ')
    w_txtPage=''
    cPostitStatus=''
    oParentObject=.Null.
    #If Version(5)>=900
        Add Object ImgBackGround As Image With Stretch=2, Visible=.F.
    #Endif
    Add Object txtStart As TextBox With Top=12,Left=105,Height=23,Width=72,FontBold=.F.,Value=Ctod("  /  /  "),SelectOnEntry=.T.
    Add Object lbl1 As Label With Top=12,Left=0,Height=21,Width=100,FontBold=.F.,Caption="",Alignment=1, BackStyle=0
    Add Object txtStop As TextBox With Top=42,Left=105,Height=21,Width=72,FontBold=.F.,Value=Ctod("  /  /  "),SelectOnEntry=.T.
    Add Object lbl2 As Label With Top=42,Left=0,Height=21,Width=100,FontBold=.F.,Caption="",Alignment=1, BackStyle=0
    Add Object txtCreatedBy As TextBox With Top=72,Left=105,Height=21,Width=50,FontBold=.F.,Caption='',Value=0,Enabled=.F.
    Add Object txtCreatedByDes As TextBox With Top=72,Left=160,Height=21,Width=140,FontBold=.F.,Caption='',Value=0,Enabled=.F.
    Add Object lbl3 As Label With Top=72,Left=0,Height=21,Width=100,FontBold=.F.,Caption="",Alignment=1, BackStyle=0
    Add Object txtOwner As TextBox With Top=102,Left=105,Height=21,Width=50,FontBold=.F.,Caption='',Value=0,Enabled=.F.
    Add Object txtOwnerDes As TextBox With Top=102,Left=160,Height=21,Width=140,FontBold=.F.,Caption='',Value="",Enabled=.F.
    Add Object lbl4 As Label With Top=102,Left=0,Height=21,Width=100,FontBold=.F.,Caption="",Alignment=1, BackStyle=0
    Add Object chkLink As Checkbox With Top=132,Left=105,Height=21,Width=250,FontBold=.F.,Caption="", BackStyle=0
    Add Object chkIntegra As Checkbox With Top=162,Left=105,Height=21,Width=250,FontBold=.F.,Caption="", BackStyle=0
    Add Object lblPage As Label With Top=192,Left=105,Height=21,Width=100,FontBold=.F.,Caption="", BackStyle=0
    Add Object txtPageName As TextBox With Top=192,Left=190,Height=23,Width=120,FontBold=.F.,Caption='',SelectOnEntry=.T.
    Add Object txtBackColor As TextBox With Top=222,Left=105,Height=21,Width=24,FontBold=.F.,Caption=''
    Add Object lbl5 As Label With Top=222,Left=0,Height=21,Width=100,FontBold=.F.,Caption="",Alignment=1, BackStyle=0
    *add object cmdColor as commandbutton with top=222,left=135,height=21,width=20,Fontbold=.f.,caption='...',ToolTipText=MSG_BACKGROUND_COLOR
    Add Object cmdColor As CommandButton With Top=222,Left=135,Height=21,Width=20,FontBold=.F.,Caption='...'
    Add Object cmdOk As CommandButton With Top=262,Left=165,Height=21,Width=50,FontBold=.F.,Caption=""
    Add Object cmdCancel As CommandButton With Top=262,Left=225,Height=21,Width=80,FontBold=.F.,Caption=""
    Proc Init(oParent,nBkColor,dStart,dStop,nCreated,nOwner,cStatus,cPage)

        If i_VisualTheme<>-1 And i_bGradientBck
            With Thisform.ImgBackGround
                .Width = Thisform.Width
                .Height = Thisform.Height
                .Anchor = 15 && Resize Width and Height
                .Picture = i_ThemesManager.GetProp(11)
                .ZOrder(1)
                .Visible = .T.
            Endwith
        Endif
        This.oParentObject=oParent
        This.txtBackColor.BackColor = nBkColor
        This.w_txtStop = dStop
        This.txtStop.Value = This.w_txtStop
        This.w_txtStart = dStart
        This.txtStart.Value=dStart

        This.txtCreatedBy.Value= nCreated
        If i_ServerConn[1,2]<>0
            =cp_SQL(i_ServerConn[1,2],"select name from cpusers where code="+cp_ToStrODBC(nCreated),"users_c")
        Else
            Select Name From cpusers Where Code=nCreated Into Cursor users_c
        Endif
        This.txtCreatedByDes.Value = users_c.Name
        Use In Select("users_c")

        This.txtOwner.Value = nOwner
        If i_ServerConn[1,2]<>0
            =cp_SQL(i_ServerConn[1,2],"select name from cpusers where code="+cp_ToStrODBC(nOwner),"users_c")
        Else
            Select Name From cpusers Where Code=nOwner Into Cursor users_c
        Endif
        This.txtOwnerDes.Value = users_c.Name
        Use In Select("users_c")

        This.w_txtPage = cPage
        This.txtPageName.Value = cPage
        This.Caption=cp_Translate(MSG_POSTIN_PROPERTIES)
        This.lbl1.Caption=cp_Translate(MSG_IT_APPEARS_FROM)+ cp_Translate(MSG_FS)
        This.lbl2.Caption=cp_Translate(MSG_TILL)+ cp_Translate(MSG_FS)
        This.lbl3.Caption=cp_Translate(MSG_CREATED_BY)+ cp_Translate(MSG_FS)
        This.lbl4.Caption=cp_Translate(MSG_ADDRESSEE)+ cp_Translate(MSG_FS)
        This.chkLink.Caption=cp_Translate( MSG_IT_APPEARS_IN_LINKS_LB_WARNING_ONLY_RB)
        This.chkIntegra.Caption=cp_Translate(MSG_INTEGRATED_IN_FORM_LB_WARNING_ONLY_RB)
        This.lblPage.Caption=cp_Translate(MSG_PAGE_NAME)+ cp_Translate(MSG_FS)
        This.lbl5.Caption=cp_Translate(MSG_BACKGROUND_COLOR)+ cp_Translate(MSG_FS)
        This.cmdOk.Caption=cp_Translate(MSG_OK_BUTTON )
        This.cmdCancel.Caption=cp_Translate(MSG_CANCEL_BUTTON)
        *--- Zucchetti Aulla Inizio - Gestione Appuntamento e to do
        This.chkIntegra.Visible = !This.oParentObject.bNoIntegra
        This.chkLink.Visible = !This.oParentObject.bNoLink
        *--- Zucchetti Aulla Fine - Gestione Appuntamento e to do
        If cStatus <> "W" And cStatus <> "A" And At("I",cStatus) = 0 And This.chkLink.Visible &&And This.chkLink.Visible Aucchetti Aulla - Gestione Appuntamento e to do
            This.cPostitStatus = "W"
            This.chkLink.Value = 1
        Else
            This.cPostitStatus = cStatus
            If At("W",cStatus) <> 0
                This.chkLink.Value = 1
                If At("I",cStatus) <> 0
                    This.chkIntegra.Value = 1
                Endif
            Else
                This.chkLink.Value = 0
                If At("I",cStatus) <> 0
                    This.chkIntegra.Value = 0
                Endif
            Endif
        Endif
        If This.chkIntegra.Value=1
            This.lblPage.Visible = .T.
            This.txtPageName.Visible = .T.
        Else
            This.lblPage.Visible = .F.
            This.txtPageName.Visible = .F.
        Endif
        Return
    Proc Activate()
        i_curform=Thisform
        oCpToolBar.Enable(.F.)
        oCpToolBar.b13.Enabled=.T.
    Proc Deactivate()
        i_curform=.Null.
        oCpToolBar.Enable(.F.)
    Func HasCPEvents(i_cOp)
        Return(.T.)
    Proc ecpQuit()
        This.Release()
    Func GetHelpFile()
        Return('postit')
    Func GetHelpTopic()
        Return('')
    Proc Destroy()
        If This.bUpdated Then
            This.oParentObject.ReportValue(This.txtBackColor.BackColor, This.w_txtStart, This.w_txtStop, This.cPostitStatus, This.w_txtPage)
        Endif
        This.oParentObject=.Null.
    Proc cmdColor.Click()
        Local c
        c=Getcolor()
        If c<>-1
            Thisform.txtBackColor.BackColor=c
            Thisform.bUpdated=.T.
        Endif
    Proc txtStop.Valid()
        If Thisform.w_txtStop<>This.Value
            Thisform.bUpdated=.T.
            Thisform.w_txtStop=This.Value
        Endif
        Return
    Proc txtStart.Valid()
        If Thisform.w_txtStart<>This.Value
            Thisform.bUpdated=.T.
            Thisform.w_txtStart=This.Value
        Endif
        Return
    Proc txtPageName.Valid()
        If Thisform.w_txtPage<>This.Value
            Thisform.bUpdated=.T.
            Thisform.w_txtPage=This.Value
        Endif
        Return
    Proc chkIntegra.Click()
        If This.Value = 0
            Thisform.lblPage.Visible = .F.
            Thisform.txtPageName.Visible = .F.
            If Left(Thisform.cPostitStatus, 1) <> "I"
                Thisform.cPostitStatus = Left(Thisform.cPostitStatus, 1)
            Else
                Thisform.cPostitStatus = ''
            Endif
        Else
            Thisform.lblPage.Visible = .T.
            Thisform.txtPageName.Visible = .T.
            Thisform.cPostitStatus = Trim(Thisform.cPostitStatus)+"I"
        Endif
        Thisform.bUpdated = .T.
        Return
    Proc chkLink.Click()
        If This.Value = 0
            If At("I",Thisform.cPostitStatus) <> 0
                Thisform.cPostitStatus = "AI"
            Else
                Thisform.cPostitStatus = "A"
            Endif
        Else
            If At("I",Thisform.cPostitStatus) <> 0
                Thisform.cPostitStatus = "WI"
            Else
                Thisform.cPostitStatus = "W"
            Endif
        Endif
        Thisform.bUpdated = .T.
        Return
    Proc cmdCancel.Click()
        Thisform.bUpdated = .F.
        Thisform.cmdOk.Click()
        Return
    Proc cmdOk.Click()
        Thisform.Release()
    Proc ecpSave()
        Thisform.cmdOk.Click()
        Return
    Proc ecpPrint()
        Return
    Proc ecpEdit()
        Return
    Proc ecpLoad()
        Return
    Proc ecpDelete()
        Return
    Proc ecpF6()
        Return
    Proc ecpPrior()
        Return
    Proc ecpNext()
        Return
    Proc ecpZoom()
        Return
    Proc ecpZoomOnZoom()
        Return
    Proc ecpFilter()
        Return
    Proc ecpInfo()
        Return
    Proc ecpSecurity()
        Return
Enddefine

Define Class lvwContainer As ContainerForListbox
    Width=400
    Height=20
    i_nRow=0
    Add Object txtFile As TextBox With SpecialEffect=1,Width=120,Height=20,Left=1
    Add Object btnCancel As CommandButton With SpecialEffect=1,Width=30,Height=20,Left=125,Picture="canc.bmp",Caption=""

    Proc Init
        DoDefault()
        If Vartype(i_ThemesManager)=='O'
            i_ThemesManager.PutBmp( This.btnCancel , "canc.bmp", 16)
        Endif
    Endproc

    Proc btnCancel.Click()
        This.Parent.Parent.lvwFile.SetFocus()
        This.Parent.Parent.lvwFile.RemoveItem(This.Parent.i_nRow)
        *---Eliminazione file da lista postit
        Adel(This.Parent.Parent.lvwFile.Parent.cDataTag,This.Parent.i_nRow)
    Proc SetValues(oList,nRow)
        DoDefault(oList,nRow)
        oList.List(nRow)=Trim(This.txtFile.Value)
    Proc GetValues(oList,nRow)
        DoDefault(oList,nRow)
        This.txtFile.Value=oList.List(nRow)
        This.i_nRow=nRow
    Proc BlankValues()
        This.txtFile.Value=''
    Proc GetList()
        Return(This.Parent.lvwFile)
Enddefine

Func ShellEx(cLink,cAction,cParms)
    Declare Integer ShellExecute In shell32.Dll;
        integer nHwnd,;
        string cOperation,;
        string cFileName,;
        string cParms,;
        string cDir,;
        integer nShowCmd

    Declare Integer FindWindow In win32api;
        string cNull,;
        string cWinName

    cAction=Iif(Empty(cAction),"Open",cAction)
    cParms=Iif(Empty(cParms),"",cParms)
    Return(ShellExecute(FindWindow(0,_Screen.Caption),cAction,cLink,cParms,tempadhoc(),1)) && Zucchetti Aulla, gestione cartella temporanea configurabile
Endfunc

Procedure SetToolBarVisible
    Local l_i,oTb,l_bEnabledCPToolBar,l_bEnabledDeskTopBar
    l_bEnabledCPToolBar = .F.
    l_bEnabledDeskTopBar = .F.
    For l_i=1 To i_ThemesManager.aToolBar.Count
        oTb = i_ThemesManager.aToolBar.Item(l_i)
        l_bEnabledCPToolBar = l_bEnabledCPToolBar Or oTb.Customize And oTb.Caption="Toolbar"
        l_bEnabledDeskTopBar = l_bEnabledDeskTopBar Or oTb.Customize And oTb.Caption="Application bar"
    Next
    oTb = .Null.
    i_bShowCPToolBar = l_bEnabledCPToolBar And Not ocptoolbar.Visible
    ocptoolbar.Visible = i_bShowCPToolBar
    If l_bEnabledDeskTopBar
        i_bShowDeskTopBar = Iif(l_bEnabledCPToolBar, i_bShowCPToolBar, Not odesktopbar.Visible)
        odesktopbar.Visible = i_bShowDeskTopBar
    Endif
    cp_backgroundmask(.T.)
Endproc
