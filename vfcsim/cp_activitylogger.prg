* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: cp_ActivityLogger
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Leopoldo Abbateggio
* Data creazione: 30/99/08
* Aggiornato il : 30/99/08
* Translated    : 30/99/08
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Gestione traccia attivit� ad hoc
* ----------------------------------------------------------------------------
*!*	 registra log delle attivit� dell'applicazione
*!*	 riceve come parametri:
*!*	 - pTempoDiInizio : datetime() all'inizio dell'esecuzione del comando
*!*	 - pSecondiDiInizio : seconds() all'inizio dell'esecuzione del comando
*!*	 - pSecondiDiFine : seconds() alla fine dell'esecuzione del comando
*!*	 - pTipoOperazione : tipo operazione
*!*  - IUD : Insert, update o delete (SQLXInsUpdDel)
*!*	 - pFraseSQL : frase sql inviata al server
*!*	 - pTipoConnessione : tipo connessione (sincrona/asincrona)
*!*	 - pFormCorrente : form corrente
*!*	 - pConnessione : connessione utilizzata
*!*	 - pNumeroRiche : numero di righe restituito dall'istruzione corrente
*!*	 - pMessaggioDiErrore : messaggio di errore restituito dall'istruzione corrente
*!*	 - pProgramma : nome del programma che � stato avviato o chiuso
*!*	 - pCursore : nome del cursore contenente il risultato dell'istruzione SQL
*!*	 - pDeadlockORiconnessione : vale 'D' nel caso di deadlock, 'R' nel caso diriconnessione
*!*	 - pQueryAsincrona : vale .t. se la chiamata proviene da cp_SQLX2 (o da cp_SQLEXEC a sua volta chiamato da cp_SQLX2)
#include "cp_app_lang.inc"

Parameters pTempoDiInizio, pSecondiDiInizio, pSecondiDiFine, pTipoOperazione, pFraseSQL, pFormCorrente, pConnessione, pNumeroRighe, pMessaggioDiErrore, pProgramma, pCursore, pDeadlockORiconnessione, pQueryAsincrona

*!* Se non esiste, viene creato un dbf con i seguenti campi:
*!* - ALDATTIM T : datetime() inizio operazione {x}
*!* - AL____MS N(3,0) : millisecondi della funzione seconds() di inizio operazione {x}
*!*	- AL__TIPO C(3) : tipo traccia pu� valere {x}
*!*	   DBR =lettura da database
*!*	   DBW =update sul database
*!*	   DBI =inserzione sul database
*!*	   DBD =cancellazione dal database
*!*	   DBM =modifica meta dati del database
*!*	   DBT =creazione tabella temporanea sul database
*!*	   DBX =altra operazione sul database
*!*	   GSO =apertura gestione
*!*	   GSD =chiusura gestione
*!*	   BTO =lancio routine
*!*	   BTD =termina routine
*!*	   FCO =lancio funzione (ad oggi non possibile senza modifica template)
*!*	   FCD =termina funzione(ad oggi non possibile senza modifica template)
*!*	   TBT =avvio transazione
*!*	   TCT =chiusura transazione con successo
*!*	   TRT =chiusura transazione con rollback
*!*	- ALCMDSQL M : comando sql
*!*	- ALSERVER C(10) : nome del sever sul quale � stata eseguita la query
*!*	- ALTIPCON C(1) : se 'A' connessione asincrona
*!*	- ALTYPEDB C(10) : tipo database server
*!*	- ALPROCED C(50) : classe procedura
*!*	- ALCAPTIO C(150) : titolo gestione di lancio (i_curform.caption)
*!*	- ALTOTTMS N(10) : durata comando in milliseocndi
*!*	- ALTOTTSC N(10) : durata comando in secondi (coincide con il precedente per 100 arrotondato) � una chiara dipendenza funzionale ma pu� essere utile averlo per mostrare il dato in modo pi� comprensibile).
*!*	- ALROWAFF N(10) : numero di record letti/modificati/cancellati/inseriti
*!*	- ALCURSOR C(10) : nome cursore all'interno della procedura
*!*	- ALERROR M : errore ritornato dal database
*!*	- ALCODUTE N(4) : codice utente
*!*	- ALCODAZI C(10) : codice azienda (verifica lunghezza cpazi in nuovo progetto.
*!* - ALNUMRIG int : numero progressivo indicante la riga
*!*	- ALCRDATI C(10): nome DBF in contenente il risultato dell'istruzione SQL salvato localmente
*!* - ALCOLORE n(10): colore da mostrare all'interno del viewer
*!* - ALDEARIC C(1) : D (deadlock) oppure R (tentativo di riconnessione) oppure vuoto (nessuno dei due)
*!* - AL__HOST C(50) : nome PC Host che ha provocato la traccia

*!*	Nome e posizione del DBF

*--- Se DB VFP nn faccio nulla
If type("i_ServerConn[1,2]")<>"N" or i_ServerConn[1,2]=0
    Return
Endif

*--- Se bisogna usare il database e non � ancora stato caricato il dizionario dati, esce
If inlist(i_nACTIVATEPROFILER, 5, 7) And Empty(i_tableprop(1,1))
    Return
Endif

Local i_VecchiaArea, i_PosizioneTabellaLog, i_AliasTabellaLog, i_FileTabellaLog, i_NomeTabellaLog, i_DBFDati, i_DBFDatiConPath, i_bInserisceRecord, bErroreCreazioneCartella, bErroreCreazioneDBF
Local cTipoOperazione,cFraseSQL,cCursore,cTipoConnessione,i_VecchiaArea,i_RigaVecchiaArea,i_DBFDati,bErroreCreazioneDBF,bMemorizzaCursore,i_bInserisceRecord,cDeadlockORiconnessione,cTipoDatabase,cNomeServer

*!* Se il nome richiesto per il DBF di log incomincia con un numero, si hanno degli errori insaspettati
*!* In tal caso viene anteposto un carattere '_' davanti al nome del DBF
If Left( i_cACTIVATEPROFILER_NOMELOG , 1 ) $ "0123456789"
    i_cACTIVATEPROFILER_NOMELOG = "_"+i_cACTIVATEPROFILER_NOMELOG
Endif

cTipoOperazione = Iif(Empty(pTipoOperazione),"NNN",pTipoOperazione)


*!* Propriet� della connessione
If pConnessione>0
    * --- Se attivo il meccanismo di riconnessione allore testo la bont� della connessione
    If  type("i_AUTORICONNECT")<>'L' Or Not i_AUTORICONNECT Or Cp_IsConnected(pConnessione)
        cTipoConnessione = Iif(pQueryAsincrona, "A", Iif(cTipoOperazione$'SEL-IUD', "S", " "))
    Else
        cTipoConnessione =" " && Se connessione non disponibile non mi pronuncio
    Endif
    cNomeServer= cp_GetServerConn (pConnessione)
    * --- Se connessione asincrona il database � sempre quello principale...
    If cTipoConnessione ="A"
        cTipoDatabase = CP_DBTYPE
    Else
        cTipoDatabase = SQLXGetDatabaseType(pConnessione)
    Endif
Else
    cTipoConnessione = " "
    cNomeServer= ""
    cTipoDatabase = ""
Endif

i_VecchiaArea=Select()
* --- Se query asincrona eseguendo una RECNO significa portare a terminae l'esecuzione della query
If cTipoConnessione<>'A'
    i_RigaVecchiaArea=Recno(i_VecchiaArea)
Endif

i_DBFDati = Space( 10 )
bErroreCreazioneCartella=.F.
bErroreCreazioneDBF=.F.

cFraseSQL = Iif(Empty(pFraseSQL),"",pFraseSQL)
cCursore = Iif(Empty(pCursore),"",Left(pCursore+Space(10),10))
bMemorizzaCursore = .F.


* Le query asincrone vengono tracciate solo alla fine di WAITFORREPLY (quindi una sola volta,parametro ASY).
* La stessa query, al contrario, quando viene invocata dalla cp_sqlexec (parametro SEL) non viene tracciata
*!* query asincrona
If cTipoConnessione = "A"
    If cTipoOperazione = "SEL"
        * Chiamata da cp_sqlexec
        Return(.T.)
    Endif
    If cTipoOperazione = "ASY"
        * Chiamata alla fine del ciclo in WAITFORREPLY
        cTipoOperazione = "DBR"
    Endif
Endif

*!* Titolo della maschera
cTitolo = Space(150)
If Type("pFormCorrente.caption")="C"
    cTitolo = Left(pFormCorrente.Caption,150)
Endif

*!* Nome del programma
cProgramma = Space(50)
If Type("pProgramma.classlibrary")="C"
    cProgramma = Right(Upper(pProgramma.ClassLibrary),50)
Endif
If Type("pProgramma")="C"
    cProgramma = Right(Upper(pProgramma),50)
Endif

*!* Deadlock o riconnessione
cDeadlockORiconnessione = Iif(Empty(pDeadlockORiconnessione)," ",pDeadlockORiconnessione)

If pTipoOperazione = "IUD"
    * Insert, update o delete
    Do Case
        Case Left(Upper(cFraseSQL),3)="INS"
            cTipoOperazione = "DBI"
        Case  Left(Upper(cFraseSQL),3)="UPD"
            cTipoOperazione = "DBW"
        Case Left(Upper(cFraseSQL),3)="DEL"
            cTipoOperazione = "DBD"
    Endcase
Endif

If pTipoOperazione = "SEL"
    * Select (con dati), select into cursor (senza dati),
    Do Case
        Case Left(Upper(cFraseSQL),6)="SELECT" And Empty(cCursore)
            * Creazione temporaneo
            cTipoOperazione = "DBT"

        Case Left(Upper(cFraseSQL),6)="SELECT" And Not Empty(cCursore)
            cTipoOperazione = "DBR"

        Case Left(Upper(cFraseSQL),6)<>"SELECT"
            * Altre operazioni sul database
            If Left(Upper(cFraseSQL),5)="ALTER" Or Left(Upper(cFraseSQL),6)="CREATE" Or Left(Upper(cFraseSQL),4)="DROP"
                cTipoOperazione = "DBM"
            Else
                cTipoOperazione = "DBX"
            Endif
    Endcase
Endif

* Calcola i valori da inserire nella tabella
*!* Inizio
tTempoDiInizio = pTempoDiInizio
nMillisecondiDiInizio = Int(pSecondiDiInizio*1000-Int(pSecondiDiInizio)*1000)

*!* Durata
tDurataInMillisecondi = 0
If Not Empty(pSecondiDiFine)
    tDurataInMillisecondi = Int(pSecondiDiFine*1000-pSecondiDiInizio*1000)
Endif
tDurataInSecondi = Round(tDurataInMillisecondi/1000, 0)


*!* Informazioni dal database
* --- Il conteggior delle righe, in presenza di un cursore avviene attraverso il conto delle sue righe...
If Not Empty( cCursore ) And Used( cCursore )
    * --- Se query sincrona posso eseguire la reccount, se asincrona la reccount mi fa terminare la query fino all'ultimo record..
    cNumeroRighe = Iif( cTipoConnessione <> "A" ,Reccount(cCursore) , 0)
Else
    cNumeroRighe = Iif(Empty(pNumeroRighe),0,pNumeroRighe)
Endif

cMessaggioDiErrore = Iif(Empty(pMessaggioDiErrore),"",pMessaggioDiErrore)

i_bInserisceRecord = .F.
*!* Seleziona i record da inserire, in partenza non aggiungo niente...:
* --- La scelta dipende da:
* ---- cTipoOperazione (tipo operazione)
* ---- cTipoConnessione (tipo connessione, sincrona / asincrona)
* ---- cDeadlockORiconnessione (se collegato al motore di riconnessione automatica)
*!* Tipo operazione

If i_nACTIVATEPROFILER_TRACCIATURAGESTIONI<>0 And cTipoOperazione $ "GSO-GSD"
    * Log per apertura e chiusura delle gestioni
    i_bInserisceRecord = .T.
    cFraseSQL=Justfname(cProgramma)&& solo il nome della gestione nella frase per aiutare le ricerche
Endif
If i_nACTIVATEPROFILER_TRACCIATURAROUTINE<>0 And cTipoOperazione $ "BTO-BTD"
    * Log per aperura e chiusura delle routine
    i_bInserisceRecord = .T.
    cFraseSQL=Justfname(cProgramma)&& solo il nome della gestione nella frase per aiutare le ricerche
Endif
If i_nACTIVATEPROFILER_TRACCIATURAFUNZIONI<>0 And cTipoOperazione $ "FCO-FCD"
    * Log per aperura e chiusura delle funzioni
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURAQUERYASINCRONA<>0 And cTipoConnessione = "A" And (i_nACTIVATEPROFILER_RIGHE<=cNumeroRighe Or i_nACTIVATEPROFILER_RIGHE=0) And i_nACTIVATEPROFILER_MILLISECONDI<=tDurataInMillisecondi
    * Log per le query asincrone
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURASQLLETTURA<>0 And cTipoOperazione = "DBR" And (i_nACTIVATEPROFILER_RIGHE<=cNumeroRighe Or i_nACTIVATEPROFILER_RIGHE=0) And i_nACTIVATEPROFILER_MILLISECONDI<=tDurataInMillisecondi
    * Log per le istruzioni Sql di lettura
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURASQLSCRITTURA<>0 And cTipoOperazione = "DBW" And (i_nACTIVATEPROFILER_RIGHE<=cNumeroRighe Or i_nACTIVATEPROFILER_RIGHE=0) And i_nACTIVATEPROFILER_MILLISECONDI<=tDurataInMillisecondi
    * Log per le istruzioni Sql di scrittura
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURASQLINSERIMENTO<>0 And cTipoOperazione = "DBI"
    * Log per le istruzioni Sql di inserimento
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURASQLCANCELLAZIONE<>0 And cTipoOperazione = "DBD" And (i_nACTIVATEPROFILER_RIGHE<=cNumeroRighe Or i_nACTIVATEPROFILER_RIGHE=0) And i_nACTIVATEPROFILER_MILLISECONDI<=tDurataInMillisecondi
    * Log per le istruzioni Sql di cancellazione
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURASQLTEMPORANEO<>0 And cTipoOperazione = "DBT"
    * Log per le istruzioni Sql di creazione temporaneo
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURASQLMODIFICADB<>0 And cTipoOperazione = "DBM"
    * Log per le istruzioni Sql di modifica database
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURASQLALTROSUDB<>0 And cTipoOperazione = "DBX"
    * Log per le istruzioni Sql di altre operazioni su database
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURATRANSAZIONI<>0 And cTipoOperazione $ "TBT-TCT-TRT"
    * Log per le istruzioni Sql di altre operazioni su database
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURADEADLOCK<>0 And cDeadlockORiconnessione = "D"
    * Log per i deadlock
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURAVQR<>0 And cTipoOperazione = "VQR"
    * Log per i deadlock
    i_bInserisceRecord = .T.
Endif
If i_nACTIVATEPROFILER_TRACCIATURARICONNESSIONE<>0 And cDeadlockORiconnessione = "R"
    * Log per le riconnessioni
    i_bInserisceRecord = .T.
Endif
If cTipoOperazione = "BLK"
    i_bInserisceRecord = .T.
Endif

* --- gli eventi utenti iniziano tutti per U sia i tasti funzione che gli eventi.
If (Left(cTipoOperazione,1) = "U" And cTipoOperazione <> "UEV") And i_nACTIVATEPROFILER_TRACCIATURATASTIFUN=1
    * Log per gestione tasti funzione
    i_bInserisceRecord = .T.
Endif

If cTipoOperazione = "UEV" And i_nACTIVATEPROFILER_TRACCIATURAEVENTI=1
    * Log per gestione eventi
    i_bInserisceRecord = .T.
Endif

* --- non devo tracciare nulla...
If Not i_bInserisceRecord
    Return (.T.)
Endif

If Left(cTipoOperazione,1)='U'
    * scriviamo nel logo nome campo e suo valore se questo � editabile (sono editabili solo i campi
    * soggetti ad indice in prima pagina
    Local bLeggirec
    bLeggirec=.T.
    Do Case
        Case cTipoOperazione='UF3' && Modifica record
            cFraseSQL=MSG_CHANGE
        Case cTipoOperazione='UF5' && Cancellazione record
            cFraseSQL=MSG_DELETE
        Case cTipoOperazione='UF6' && Cancellazione riga
            cFraseSQL=MSG_DELETE_ROW
            bLeggirec=.F.
            * --- se elimino una riga leggo i dati sul dettaglio...
            If Type('pFormCorrente.oPgFrm.Pages(1).oPag.oBody.oBodyCol.oRow.ControlCount')='N'
                For i=1 To pFormCorrente.oPgFrm.Pages(1).oPag.oBody.oBodyCol.oRow.ControlCount
                    * MEGLIO LA VARTYPE non sporca la msg.
                    If Type('pFormCorrente.oPgFrm.Pages(1).oPag.oBody.oBodyCol.oRow.Controls(i).cformvar')='C' And pFormCorrente.oPgFrm.Pages(1).oPag.oBody.oBodyCol.oRow.Controls(i).Enabled
                        cFraseSQL=cFraseSQL+Chr(13)+Chr(10)+pFormCorrente.oPgFrm.Pages(1).oPag.oBody.oBodyCol.oRow.Controls(i).cformvar+':'+cp_tostr(pFormCorrente.oPgFrm.Pages(1).oPag.oBody.oBodyCol.oRow.Controls(i).Value)
                    Endif
                Endfor
            Endif

        Case cTipoOperazione='U10' && Conferma gestione
            cFraseSQL=MSG_SAVE
        Case cTipoOperazione='UF4' && Caricamento record
            cFraseSQL=MSG_LOAD
            bLeggirec=.F.
        Case cTipoOperazione='U12' && Inizio filtratura records
            cFraseSQL=MSG_FILTER
            bLeggirec=.F.
        Case cTipoOperazione='UES' && Uscita operazione
            cFraseSQL=MSG_EXIT
            bLeggirec=.F.	&& da valutare se leggere o meno...
        Case cTipoOperazione='UEV' && Evento
            cFraseSQL=MSG_EVENT_END+' '+cFraseSQL
            bLeggirec=.F.
    Endcase
    * solo gli eventi che hanno la maschera popolato leggono i dati abilitati sulla prima pagina
    If bLeggirec And Type('pFormCorrente.oPgFrm.Pages(1).oPag.ControlCount')='N'
        For i=1 To pFormCorrente.oPgFrm.Pages(1).oPag.ControlCount
            * MEGLIO LA VARTYPE non sporca la msg.
            If Type('pFormCorrente.oPgFrm.Pages(1).oPag.Controls(i).cformvar')='C' And pFormCorrente.oPgFrm.Pages(1).oPag.Controls(i).Enabled
                cFraseSQL=cFraseSQL+Chr(13)+Chr(10)+pFormCorrente.oPgFrm.Pages(1).oPag.Controls(i).cformvar+':'+cp_tostr(pFormCorrente.oPgFrm.Pages(1).oPag.Controls(i).Value)
            Endif
        Endfor
    Endif
Endif

* Non traccia i programmi di gestione dell'activity logger
If Not Empty(cProgramma)
    If Inlist(Upper(Justfname(cProgramma)),"CP_VIEWTRACE","CP_VIEWTRACE_B","CP_SETUPTRACE","CP_SETUPTRACE_B")
        Return (.T.)
    Endif
Endif

*!* Posizione del log e creazione della cartella, se non specificato va nella TEMP sotto la cartella Logger
If Empty(i_cACTIVATEPROFILER_POSIZIONELOG) Or type("i_cACTIVATEPROFILER_POSIZIONELOG")<>"C"
    i_nACTIVATEPROFILER_OLD = i_nACTIVATEPROFILER
    i_nACTIVATEPROFILER = 0
    i_PosizioneTabellaLog=Addbs(Addbs(tempadhoc())+'Logger')
    i_nACTIVATEPROFILER = i_nACTIVATEPROFILER_OLD
    i_cACTIVATEPROFILER_POSIZIONELOG = i_PosizioneTabellaLog
Else
    i_PosizioneTabellaLog=Addbs(i_cACTIVATEPROFILER_POSIZIONELOG)
Endif
VecchioErrore=On('error')
bErroreCreazioneCartella=.F.
On Error bErroreCreazioneCartella=.T.
If Not Directory(i_PosizioneTabellaLog)
    Md (i_PosizioneTabellaLog)
Endif
If bErroreCreazioneCartella
    i_nACTIVATEPROFILER_OLD = i_nACTIVATEPROFILER
    i_nACTIVATEPROFILER = 0
    i_PosizioneTabellaLog=Addbs(Addbs(tempadhoc())+'Logger')
    i_nACTIVATEPROFILER = i_nACTIVATEPROFILER_OLD
Endif
On Error &VecchioErrore

*!* Nome del file di log
If Empty(i_cACTIVATEPROFILER_NOMELOG) Or type("i_cACTIVATEPROFILER_NOMELOG")<>"C"
    i_FileTabellaLog="ActivityLogger"
Else
    i_FileTabellaLog=i_cACTIVATEPROFILER_NOMELOG
Endif
*!* Gestione multisessione
If i_nACTIVATEPROFILER_MULTISESSION = 1
    If Empty(i_cACTIVATEPROFILER_SESSIONNAME)
        * Assegna il nome della multisessione
        i_cACTIVATEPROFILER_SESSIONNAME = Sys( 2015 )
        i_FileTabellaLog = i_FileTabellaLog + i_cACTIVATEPROFILER_SESSIONNAME
        i_cACTIVATEPROFILER_NOMELOG = i_FileTabellaLog
    Endif
Endif
i_NomeTabellaLog=i_PosizioneTabellaLog+i_FileTabellaLog

If type("i_nACTIVATEPROFILER_ALIAS")="C"
    i_AliasTabellaLog = i_nACTIVATEPROFILER_ALIAS
Else
    i_AliasTabellaLog = ""
Endif
*!* Crea il DBF di log se non esiste
*!* i_nACTIVATEPROFILER=1 o 3 scrive su DBF, i_nACTIVATEPROFILER=5 o 7 scrive su database corrente
If inlist(i_nACTIVATEPROFILER, 1, 3)

    If Not Used(i_AliasTabellaLog)

        *!* Controlla se il nome assegnato alla tabella di log pu� essere utilizzato
        If Not File(i_NomeTabellaLog+".DBF")
            nNomeFileOK=Fcreate(i_NomeTabellaLog)
            Fclose(nNomeFileOK)
            If nNomeFileOK <> -1
                *!* Se la creazione va a buon fine il file di prova viene cancellato
                Delete File &i_NomeTabellaLog
            Else
                *!* Altrimenti utilizza nome e posizione standard
                i_FileTabellaLog="ActivityLogger"
                i_NomeTabellaLog=i_PosizioneTabellaLog+i_FileTabellaLog
            Endif

        Endif

        *!* Chiama la creazione del file di log
        *!* Se non � possibile eseguire la creazione del file di log (per esempio, � stato creato
        *!* da un'altra istanza di ad hoc sulla stessa macchina) allora esce da cp_activitylogger
        VecchioErrore=On('error')
        bErroreCreazioneDBF=.F.
        On Error bErroreCreazioneDBF=.T.

        If Not CreaFileDiLog(i_NomeTabellaLog)
            * --- se non riesco a creare/aprire il file di log esco
            Return
        Endif
        * i_nACTIVATEPROFILER_ALIAS � valorizzato in CreaFileDiLog
        i_AliasTabellaLog = i_nACTIVATEPROFILER_ALIAS
        On Error &VecchioErrore

    Endif

    If bErroreCreazioneDBF Or Not Used( i_AliasTabellaLog )
        i_bInserisceRecord=.F.
    Endif

Endif



*!* Nome per eventuale DBF con dati restituiti dall'istruzione corrente
i_DBFDati=Sys(2015)
i_DBFDatiConPath=i_PosizioneTabellaLog+i_DBFDati



* Conserva il risultato della query
If cTipoOperazione = "DBR" And (Left(Upper(cFraseSQL),6)="SELECT" And Not Empty(cCursore))
    If Used(cCursore) And (INLIST(i_nACTIVATEPROFILER,1,3,5,7))And ( (cTipoConnessione = "A" And i_nACTIVATEPROFILER_TRACCIATURAQUERYASINCRONA=1) Or (cTipoConnessione = "S" And i_nACTIVATEPROFILER_TRACCIATURASQLLETTURA=1) )
        *SELECT * FROM (cCursore) INTO TABLE (i_DBFDatiConPath)
        Select (cCursore)
        *APPEND FROM (cCursore)
        Local nPosRec
        nPosRec=Recno()
        Copy To (i_DBFDatiConPath)
        Select (cCursore)
        If Reccount() > 0 And Reccount() >= nPosRec
            Goto (nPosRec)
        Endif
        bMemorizzaCursore = .T.
        If Used(i_DBFDati)
            Select(i_DBFDati)
            Use
        Endif
    Endif
Endif


*!* Per distinguere le righe, viene assegnato un colore
cColore = Rgb(255,255,255)
If Inlist(cTipoOperazione,"DBR","DBW","DBI","DBD")
    * Rosso
    cColore = Rgb(239,89,39)
Endif
If Inlist(cTipoOperazione,"GSO","GSD","BTO","BTD","FCO","FCD")
    * Verde - Apertura e chiusura gestioni
    cColore = Rgb(41,199,49)
Endif
If Inlist(cTipoOperazione,"TBT","TCT","TRT")
    * Viola - Transazioni
    cColore = Rgb(180,41,166)
Endif
If cTipoOperazione = "DBM"
    * Giallo - Manutenzione Database
    cColore = Rgb(255,255,127)
Endif
If cTipoOperazione = "VQR"
    * Grigio - Manutenzione Database
    cColore = Rgb(190,190,190)
Endif
If cDeadlockORiconnessione $ "D-R"
    * Giallo - Manutenzione Database
    cColore = Rgb(127,171,255)
Endif

* Aggiunge la descrizione dell'operazione
cDescrizioneOperazione=cTipoOperazione
DO CASE
    CASE cTipoOperazione = "DBR"
        cDescrizioneOperazione = "SQL - SELECT"
    CASE cTipoOperazione = "DBI"
        cDescrizioneOperazione = "SQL - INSERT"
    CASE cTipoOperazione = "DBW"
        cDescrizioneOperazione = "SQL - UPDATE"
    CASE cTipoOperazione = "DBD"
        cDescrizioneOperazione = "SQL - DELETE"
    CASE cTipoOperazione = "DBT"
        cDescrizioneOperazione = "SQL - Creazione temporaneo"        
    CASE cTipoOperazione = "TBT"
        cDescrizioneOperazione = "SQL - BEGIN TRANSACTION"
    CASE cTipoOperazione = "TCT"
        cDescrizioneOperazione = "SQL - COMMIT"
    CASE cTipoOperazione = "TRT"
        cDescrizioneOperazione = "SQL - ROLLBACK"
    CASE cTipoOperazione = "DBM"
        cDescrizioneOperazione = "SQL - Modifica del database"
    CASE cTipoOperazione = "DBX"
        cDescrizioneOperazione = "SQL - Altre operazioni sul database"
    CASE cTipoOperazione = "GSO"
        cDescrizioneOperazione = "Apertura form"
    CASE cTipoOperazione = "GSD"
        cDescrizioneOperazione = "Chiusura form"
    CASE cTipoOperazione = "BTO"
        cDescrizioneOperazione = "Avvio routine"
    CASE cTipoOperazione = "BTD"
        cDescrizioneOperazione = "Fine routine"
    CASE cTipoOperazione = "FCO"
        cDescrizioneOperazione = "Chiamata di funzione"
    CASE cTipoOperazione = "FCD"
        cDescrizioneOperazione = "Chiusura funzione"
    CASE cTipoOperazione = "VQR"
        cDescrizioneOperazione = "Visual query"
    CASE cTipoOperazione = "BLK"
        cDescrizioneOperazione = "Blackbox"
    CASE cTipoOperazione = "UEV"
        cDescrizioneOperazione = "Evento"
    CASE cTipoOperazione = "UF3"
        cDescrizioneOperazione = "Modifica su form"
    CASE cTipoOperazione = "UF5"
        cDescrizioneOperazione = "Cancellazione su form"
    CASE cTipoOperazione = "UF6"
        cDescrizioneOperazione = "Cancellazione riga da dettaglio"
    CASE cTipoOperazione = "U10"
        cDescrizioneOperazione = "Conferma su form"
    CASE cTipoOperazione = "UF4"
        cDescrizioneOperazione = "Nuovo record su form"
    CASE cTipoOperazione = "U12"
        cDescrizioneOperazione = "Filtro su form"
    CASE cTipoOperazione = "ESC"
        cDescrizioneOperazione = "Tasto escape"

ENDCASE


*!* Aggiunge la riga di log nel DBF e valorizza i campi
*!* i_nACTIVATEPROFILER=0 :nessun log, =1: su DBF, =2: a video, =3: entrambi
Local nNumeroDiRiga
nNumeroDiRiga = 0
If inlist(i_nACTIVATEPROFILER, 1, 3, 5, 7)
    *!* i_nACTIVATEPROFILER=1 o 3 scrive su DBF, i_nACTIVATEPROFILER=5 o 7 scrive su database corrente
    If inlist(i_nACTIVATEPROFILER, 1, 3)
        Select(i_AliasTabellaLog)
        Append Blank
        nNumeroDiRiga = Recno()
        Replace ;
            ALDATTIM With tTempoDiInizio, AL____MS With nMillisecondiDiInizio, AL__TIPO With cTipoOperazione,;
            ALOPNAME With cDescrizioneOperazione,;
            ALCMDSQL With cFraseSQL, ALSERVER With cNomeServer, ALTIPCON With cTipoConnessione,;
            ALTYPEDB With cTipoDatabase, ALPROCED With cProgramma, ALCAPTIO With cTitolo,;
            ALTOTTMS With tDurataInMillisecondi, ALTOTTSC With tDurataInSecondi,;
            ALROWAFF With cNumeroRighe, ALCURSOR With cCursore, ALERROR  With cMessaggioDiErrore,;
            ALCODUTE With i_codute, ALCODAZI With i_codazi, ALCRDATI With Iif(bMemorizzaCursore,i_DBFDati,Space(10)),;
            ALCOLORE With cColore, ALDEARIC With cDeadlockORiconnessione,AL__HOST With Alltrim(Sys(0))

        *!* Gestisce il rollover
        Adir(aDimensioneFile,Alltrim(i_NomeTabellaLog)+".*")

        Local nDimensioneDBF, nDimensioneFPT, bEseguiRollover

        * Dimensioni dei file
        nDimensioneDBF = 0
        nDimensioneFPT = 0
        bEseguiRollover = .F.

        If Type("aDimensioneFile(1,2)") = "N"
            nDimensioneDBF = aDimensioneFile(1,2)
        Endif

        If Type("aDimensioneFile(2,2)") = "N"
            nDimensioneFPT = aDimensioneFile(2,2)
        Endif

        If nDimensioneDBF >= 1900*1000000 Or nDimensioneFPT >= 1900*1000000
            * Se uno dei file di log raggiunge i 2 gb, si esegue comunque il rollover
            bEseguiRollover = .T.
        Endif

        If i_cACTIVATEPROFILER_TIPOROLLOVER = "D" And i_nACTIVATEPROFILER_ROLLOVER > 0 And ( nDimensioneDBF >= i_nACTIVATEPROFILER_ROLLOVER * 1000000 Or nDimensioneFPT >= i_nACTIVATEPROFILER_ROLLOVER * 1000000 )
            * Se uno dei file di log raggiunge la dimensioni specificata e il log � gestito secondo dimensione,
            * si esegue comunque il rollover
            bEseguiRollover = .T.
        Endif

        If i_cACTIVATEPROFILER_TIPOROLLOVER = "R" And i_nACTIVATEPROFILER_ROLLOVER > 0 And nNumeroDiRiga >= i_nACTIVATEPROFILER_ROLLOVER
            * Se uno dei file di log raggiunge la dimensioni specificata e il log � gestito secondo dimensione,
            * si esegue comunque il rollover
            bEseguiRollover = .T.
        Endif

        If bEseguiRollover
            cVecchioNome = Alltrim(i_NomeTabellaLog)+".DBF"
            cOra=Right("00"+Alltrim(Str(Hour(tTempoDiInizio))),2)+Right("00"+Alltrim(Str(Minute(tTempoDiInizio))),2)+Right("00"+Alltrim(Str(Sec(tTempoDiInizio))),2)
            cNuovoNome = Alltrim(i_NomeTabellaLog)+"_"+Dtos(tTempoDiInizio)+"_"+cOra+".DBF"
            * Se i rollover avviene troppo presto (stessa ora, minuti e secondi del rollover precedente)
            * si attende un secondo
            If File( cNuovoNome )
                cOra_2 = cOra
                Do While cOra_2 = cOra
                    cOra = Right("00"+Alltrim(Str(Hour(Datetime()))),2)+Right("00"+Alltrim(Str(Minute(Datetime()))),2)+Right("00"+Alltrim(Str(Sec(Datetime()))),2)
                Enddo
                cNuovoNome = Alltrim(i_NomeTabellaLog)+"_"+Dtos(Datetime())+"_"+cOra+".DBF"
            Endif
            * Chiude il DBF
            Select(i_AliasTabellaLog)
            Use
            * Rinomina il file DBF e FPT
            Rename &cVecchioNome To &cNuovoNome
            cVecchioNome = Alltrim(i_NomeTabellaLog)+".FPT"
            cNuovoNome = Alltrim(i_NomeTabellaLog)+"_"+Dtos(tTempoDiInizio)+"_"+cOra+".FPT"
            If File(cVecchioNome)
                Rename &cVecchioNome To &cNuovoNome
            Endif

            * Se � stato specificato il numero di file di log da mantenere,
            * elimina i file di log in eccesso
            If i_nACTIVATEPROFILER_NUMERODIFILEDILOG > 0
                *!*   Ricava l'elenco dei file da cancellare
                Adir(aFileDaCancellare,Alltrim(i_NomeTabellaLog)+"*.DBF")
                nRigheArray = Alen(aFileDaCancellare,1)
                nColonneArray = Alen(aFileDaCancellare,2)

                *!*   Ordina per nome
                Asort(aFileDaCancellare)
                For nContatore = 1 To nRigheArray
                    * Cancella solo i file pi� vecchi
                    If i_nACTIVATEPROFILER_NUMERODIFILEDILOG <= nRigheArray - nContatore
                        * Cancella il file DBF
                        cFileDaCancellare=i_PosizioneTabellaLog+aFileDaCancellare(nContatore,1)
                        Delete File &cFileDaCancellare
                        * Cancella il file FPT
                        cFileDaCancellare=i_PosizioneTabellaLog+Left(Alltrim(aFileDaCancellare(nContatore,1)),Len(Alltrim(aFileDaCancellare(nContatore,1)))-4)+".FPT"
                        If File(cFileDaCancellare)
                            Delete File &cFileDaCancellare
                        Endif
                    Endif
                Endfor

            Endif

            * Crea un nuovo file di log
            CreaFileDiLog(i_NomeTabellaLog)

        Endif && Fine gestione scrittura su DBF
    Else
        *!* i_nACTIVATEPROFILER=1 o 3 scrive su DBF, i_nACTIVATEPROFILER=5 o 7 scrive su database corrente
        *!* caso i_nACTIVATEPROFILER=5 o 7
        *!* interrompe momentaneamente le chiamate all'activitylogger per non andare in loop
        *!*	    IF NOT "BLACKBOX" $ UPPER(cProgramma)
        i_nACTIVATEPROFILER_OLD = i_nACTIVATEPROFILER
        i_nACTIVATEPROFILER = 0
        cp_WriteLOG( tTempoDiInizio, nMillisecondiDiInizio, cTipoOperazione, cFraseSQL, cNomeServer, cTipoConnessione,;
            cTipoDatabase, cProgramma, cTitolo, tDurataInMillisecondi, tDurataInSecondi,;
            cNumeroRighe, cCursore, cMessaggioDiErrore, Iif(bMemorizzaCursore,i_DBFDati,Space(10)),;
            cColore, cDeadlockORiconnessione, cDescrizioneOperazione)
        *!* riattiva le chiamate all'activitylogger per non andare in loop

        * Riattiva il profiler
        i_nACTIVATEPROFILER = i_nACTIVATEPROFILER_OLD
        *!*	ENDIF

    Endif
Endif

*!* Aggiunge la riga di log sullo schermo
*!* i_nACTIVATEPROFILER=0 :nessun log, =1: su DBF, =2: a video, =3: entrambi
If INLIST( i_nACTIVATEPROFILER,2,3,7)
    * --- a video passo data / msec, tipo operazione, frase se c'�, errore se c'� e gestione
    cp_Dbg(Right ( Ttoc(tTempoDiInizio),8)+'ms'+Str(nMillisecondiDiInizio,3,0)+' '+cTipoOperazione+Iif(Not Empty(cFraseSQL),' '+cFraseSQL,'')+Iif(Not Empty(cMessaggioDiErrore),' '+cMessaggioDiErrore,'')+Iif( Not Empty(cProgramma  ),' '+cProgramma,'' ))
Endif

Select(i_VecchiaArea)

Return .T.

Procedure CreaFileDiLog(pNomeDelFileDaCreare)
    Local bres
    bres=.T.
    *crea alias da utilizzare per le insert
    If type("i_nACTIVATEPROFILER_ALIAS") <>"C"
        Public i_nACTIVATEPROFILER_ALIAS
        i_nACTIVATEPROFILER_ALIAS = Sys(2015)
    Endif
    If ! File(pNomeDelFileDaCreare+".DBF")

        Create Table (pNomeDelFileDaCreare)(ALDATTIM T, AL____MS N(3), AL__TIPO C(3), ALOPNAME C(100),;
            ALCMDSQL M, ALSERVER C(10), ALTIPCON C(1), ALTYPEDB C(10), ALPROCED C(50),;
            ALCAPTIO C(150), ALTOTTMS N(10), ALTOTTSC N(10), ALROWAFF N(20),;
            ALCURSOR C(10), ALERROR M, ALCODUTE N(4), ALCODAZI C(10), ALNUMRIG Int Not Null Autoinc,;
            ALCRDATI C(10), ALCOLORE N(10), ALDEARIC C(1),AL__HOST C(100))
    Else
        * --- Potrebbe essere un log del passato, quindi verifico se esiste il campo AL__HOST se no lo creo..
        Local nFld, nREc
        * --- apro la tabella in shared per determianre l'elenco dei campi, va subito richiusa...
        Use (pNomeDelFileDaCreare+".DBF") Alias (i_nACTIVATEPROFILER_ALIAS) In 0 Shared
        nREc=Reccount(i_nACTIVATEPROFILER_ALIAS)
        nFld=Afields(el_fld,i_nACTIVATEPROFILER_ALIAS)
        Use In (i_nACTIVATEPROFILER_ALIAS)
        * --- Se non trovo il campo Host lo aggiungo
        If nFld>0 And Ascan(el_fld,'AL__HOST',1,-1,1,1)=0
            Try
                * -- oltre ad aggiungere il campo mancante rendo il numero riga un AUTOINC
                Alter Table (pNomeDelFileDaCreare) Add Column AL__HOST Char(100) Alter Column ALNUMRIG Int Not Null Autoinc Nextvalue nREc+1
                * --- la alter table lascia la tabella aperta, va richiusa...
                Use In (Justfname(pNomeDelFileDaCreare))
            Catch
                * Se qualcosa va storto disabilito tutto, significa che lo stesso file � condiviso da due installazione diverse..
                cp_errormsg("Impossibile aggiornare la struttura del file di log dell'Activity logger. Funzionalit� disabilitata.")
                i_nACTIVATEPROFILER=0
                bres=.F.
            Endtry
        Endif
    Endif

    If bres
        Try
            If Used(Justfname(pNomeDelFileDaCreare))
                Use In (Justfname(pNomeDelFileDaCreare))
            Endif
            If Not Used(i_nACTIVATEPROFILER_ALIAS)
                Use (pNomeDelFileDaCreare+".DBF") Alias (i_nACTIVATEPROFILER_ALIAS) In 0 Shared
            Endif
        Catch
            bres=.F.
        Endtry
    Endif

    Return bres

Define Class ActivityLoggerSetup As GlobalVarSetup
    * Variabili di configurazione dell'activity logger
    * i_nACTIVATEPROFILER=0 :nessun log, =1: su DBF, =2: a video, =3: entrambi
    i_nACTIVATEPROFILER=0
    * i_cACTIVATEPROFILER_POSIZIONELOG specifica il percorso assoluto per una cartella dove
    * depositare il log. Se vuoto, si utilizza la cartella temporanea tempadhoc()
    i_cACTIVATEPROFILER_POSIZIONELOG=""
    * i_cACTIVATEPROFILER_NOMELOG specifica il nome del DBF dove
    * depositare il log. Se vuoto, si utilizza il nome "ActivityLogger"
    i_cACTIVATEPROFILER_NOMELOG=""
    * Il tipo rollover vale 'D' o 'R' e indica se eseguire il rollover in base al
    * numero di record o alla dimensione del file
    i_cACTIVATEPROFILER_TIPOROLLOVER="D"
    * i_nACTIVATEPROFILER_ROLLOVER specifica il numero di righe nel DBF raggiunte le
    * quali il DBF stesso viene rinominato e ne viene aperto un altro, oppure
    * la dimensione del file
    i_nACTIVATEPROFILER_ROLLOVER=0
    * i_nACTIVATEPROFILER_NUMERODIFILEDILOG specifica il numero massimo di file di log da tenere
    * quelli pi� vecchi vengono eliminati
    i_nACTIVATEPROFILER_NUMERODIFILEDILOG=5
    * Le seguenti variabili possono valere 0 o 1, Se messe a zero evitano la relitiva tracciatura su log
    i_nACTIVATEPROFILER_TRACCIATURAGESTIONI=1
    i_nACTIVATEPROFILER_TRACCIATURAROUTINE=1
    i_nACTIVATEPROFILER_TRACCIATURAFUNZIONI=1    
    i_nACTIVATEPROFILER_TRACCIATURAQUERYASINCRONA=2
    i_nACTIVATEPROFILER_TRACCIATURASQLLETTURA=1
    i_nACTIVATEPROFILER_TRACCIATURASQLSCRITTURA=1
    i_nACTIVATEPROFILER_TRACCIATURASQLINSERIMENTO=1
    i_nACTIVATEPROFILER_TRACCIATURASQLCANCELLAZIONE=1
    i_nACTIVATEPROFILER_TRACCIATURASQLTEMPORANEO=1
    i_nACTIVATEPROFILER_TRACCIATURASQLMODIFICADB=1
    i_nACTIVATEPROFILER_TRACCIATURASQLALTROSUDB=1
    i_nACTIVATEPROFILER_TRACCIATURATRANSAZIONI=1
    i_nACTIVATEPROFILER_TRACCIATURADEADLOCK=1
    i_nACTIVATEPROFILER_TRACCIATURARICONNESSIONE=1
    i_nACTIVATEPROFILER_TRACCIATURAVQR=1
    i_nACTIVATEPROFILER_MULTISESSION=1

    * Nome della multisessione. � opportuno  che sia gestito dalla procedura
    i_cACTIVATEPROFILER_SESSIONNAME=""
    * Numero minimo di millisecondi per i quali l'istruzione viene salvata
    i_nACTIVATEPROFILER_MILLISECONDI=0
    * Numero minimo di righe interessate per le quali l'istruzione viene tracciata
    i_nACTIVATEPROFILER_RIGHE=0
    i_nACTIVATEPROFILER_TRACCIATURAEVENTI = 1 && Attiva tracciatura eventi
    i_nACTIVATEPROFILER_TRACCIATURATASTIFUN = 1 && Attiva tracciatura tasti funzione (un sotto insieme F3,F4,F5,F6,F10,F1,ESC)

    * Nel caso di inserimento sul database indica il numero di riga
    i_nACTIVATEPROFILER_ROWNUMBER = 0
Enddefine
