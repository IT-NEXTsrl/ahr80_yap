* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_chpfun3                                                      *
*              Read fields multilanguage                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-31                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar,pMultiReportSplit
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_chpfun3",oParentObject,m.pPar,m.pMultiReportSplit)
return(i_retval)

define class tcp_chpfun3 as StdBatch
  * --- Local variables
  pPar = space(10)
  w_IDMSAsa = .f.
  w_bReOpen = .f.
  w_stp = space(10)
  w_SelOpt = space(10)
  w_YEAR = space(4)
  pMultiReportSplit = .NULL.
  w_MACCHINA = space(1)
  w_ini_EMAIL = space(10)
  w_ini_EMAILSUBJECT = space(10)
  w_ini_EMAILTEXT = space(10)
  w_ini_FAXSUBJECT = space(10)
  w_ini_DEST = space(10)
  w_ini_FAXNO = space(10)
  w_oReport_Text = .NULL.
  w_oReport_Graphic = .NULL.
  w_OBJ = .NULL.
  w_elemento = 0
  w_CurName = space(10)
  w_RepName = space(10)
  w_bOk = space(10)
  w_cexte = space(10)
  w_i = 0
  w_numrep = 0
  w_count = 0
  w_cType = space(10)
  w_nomefile = space(10)
  w_CURSOR = space(10)
  w_ret = 0
  w_pathhtml = space(250)
  w_HTMLVAL = .f.
  w_OPT = .NULL.
  w_OLDVAL = space(10)
  w_LC = 0
  w_pathhtmlold = space(250)
  w_maskcall = space(25)
  w_OLDSTATE = 0
  w_PRINTER_ARRAY_LINE = 0
  w_cpassworddoc = space(10)
  w_cpasswordtemp = space(10)
  w_cprotectdoc = space(10)
  w_cprotecttemp = space(10)
  w_cformat = 0
  w_cconfirmconv = 0
  w_creadonly = 0
  w_clinktosource = 0
  w_caddtofilelist = 0
  w_crevert = 0
  w_ntuple = 0
  w_cModelName = space(10)
  w_cDocName = space(10)
  w_cdsname = space(10)
  w_cwconn = space(10)
  w_csqlstatement = space(10)
  w_oManager = .NULL.
  w_oDesktop = .NULL.
  w_oDBc = .NULL.
  w_oDataSource = space(10)
  w_objMailMerge = .NULL.
  w_outType = space(10)
  w_oWriter = .NULL.
  w_oText = .NULL.
  w_oCurs = .NULL.
  w_oExcel = .NULL.
  w_NameExport = space(255)
  w_aggpath = space(254)
  w_PATH = space(10)
  w_cPath_new = space(10)
  w_bmod_exists = space(10)
  w_cPath_ok = space(10)
  w_NomeCalc = space(10)
  w_RepCodISO = space(10)
  w_oSManager = .NULL.
  w_oSDesktop = .NULL.
  w_oReflection = .NULL.
  w_oPropertyValue = .NULL.
  w_oStarCalc = .NULL.
  w_str = space(10)
  w_arr = 0
  w_curs = space(10)
  w_NomeExcel = space(10)
  w_NewExcel = space(10)
  w_NewCalc = space(10)
  w_oReflection = .NULL.
  w_oSFrame = .NULL.
  w_bOldExport = .f.
  w_cTranslate = space(20)
  w_cNameTranslate = space(100)
  w_AutoDir = .f.
  w_NameExport = space(255)
  w_bAutoOffice = .f.
  w_cOldExp_NoModel = space(254)
  w_cOldExp_OldModel = space(254)
  w_param = space(10)
  w_nOperaz = 0
  w_SERIAL = space(10)
  w_MVFLSEND = space(1)
  w_MVCLADOC = space(2)
  w_BOTMENU = .NULL.
  w_OLDFLOPEN = space(1)
  w_FILENAME = space(254)
  w_tempstamp = space(10)
  w_nro = 0
  w_oa = 0
  w_ovar = 0
  w_reportname = space(10)
  w_workstation = space(10)
  w_oggetto = .NULL.
  w_var = space(10)
  w_oldpath = space(10)
  w_path_ps = space(10)
  w_CAMPO = .NULL.
  w_cGetExtReport = .NULL.
  w_dbfname = space(10)
  w_dataname = space(10)
  w_tblname = space(10)
  w_UsrRight = 0
  w_nNumRep = 0
  w_cCurName = space(10)
  w_SelForm = .NULL.
  w_NumRow = 0
  w_i = 0
  w_bFound = .f.
  w_d = space(10)
  w_VarEnv = .NULL.
  w_paramqry = space(10)
  w_bVarEnv = .f.
  w_NumVar = 0
  w_cFilename = space(10)
  w_ENV = space(10)
  w_MONTH = space(4)
  w_DAY = space(4)
  w_HOUR = space(2)
  w_MIN = space(2)
  w_SEC = space(2)
  w_N = 0
  w_NUMCOPIE = 0
  w_DISPOSITIVO = space(10)
  * --- WorkFile variables
  STMPFILE_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- nell'anteprima pMultiReportSplit pu� essere usata per forzare/disabilitare il GDI+
    this.w_MACCHINA = "N"
    this.w_OBJ = this.oparentobject
    private L_OldCaption
    L_OldCaption = _screen.caption
    do case
      case this.pPar="init"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="AnteClick"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="AnteHTMLClick"
        this.w_OPT = this.w_OBJ.getCTRL ("w_Opt_File")
        this.w_OLDVAL = this.w_OPT.combovalues
        this.w_HTMLVAL = False
        this.w_LC = this.w_OPT.listcount
        i_nrocopie=this.w_OBJ.w_Txt_Copies
        * --- Controllo se HTML � nella lista (non usare ASCAN perch� non riconosce combovalues come array)
        do while this.w_LC>0 and not this.w_HTMLVAL
          this.w_HTMLVAL = this.w_OPT.combovalues(this.w_LC) == "HTML"
          this.w_LC = this.w_LC-1
        enddo
        if this.w_HTMLVAL
          =setvaluelinked("M",this.w_oBJ,"w_Opt_File","HTML")
          this.w_OBJ.SaveDependsOn()     
          this.w_pathhtmlold = this.w_OBJ.w_TXT_FILE
          this.w_pathhtml = TEMPADHOC()+"\"+SYS(2015)+".HTML"
          this.w_OBJ.w_TXT_FILE = this.w_pathhtml
          this.w_OBJ.mCalc(.t.)     
          this.Page_19()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          do cp_htmlpreview with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          DELETE FILE (this.w_pathhtml)
          =setvaluelinked("M",this.w_oBJ,"w_Opt_File",this.w_OLDVAL)
          this.w_OBJ.w_TXT_FILE = this.w_pathhtmlold
        else
          ah_errormsg("Il formato HTML non � stato definito")
        endif
      case this.pPar="StpClick"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="oPDBtnProcClick"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="StpOptClick"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="PDBtnStatusClick"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="WordClick"
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="ExcelClick"
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="GraphClick"
        this.Page_10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="MailClick"
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="WWP_ZCPClick" or this.pPar="WWP_ZCPClick_sa"
        this.w_IDMSAsa = this.pPar=="WWP_ZCPClick_sa"
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="PliteClick"
        this.Page_13()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="FaxClick"
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="QueryClick"
        this.Page_15()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="EdiClick" or this.pPar="FatelClick"
        this.Page_16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="EscClick"
        this.Page_17()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="StpFileOptClick"
        this.Page_18()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="StpFileClick"
        this.Page_19()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="StpArchiClick"
        this.Page_20()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="SaveIndClick"
        this.Page_21()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="AssocClick"
        this.Page_22()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="SStpClick"
        this.Page_23()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="SFileClick"
        this.Page_24()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="Txt_Copies LostFocus"
        this.Page_25()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="Opt_Language Changed"
        this.Page_26()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="BlankRecEnd"
        this.Page_27()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="SettaggiLingue" 
        this.Page_28()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="ecpSecurity" 
        this.Page_29()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="Schedulatore" 
        this.Page_30()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="NumeroCopie" 
        this.Page_31()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="CambioStampante" 
        this.Page_32()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="Select_color"
        this.Page_33()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="SelectReport"
        this.Page_34()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="ExecuteReport"
        this.Page_35()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        i_retval = this.w_RET
        return
      case this.pPar="BrowseClick"
        this.Page_36()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizizlizza i la combobox con l elenco delle estensioni file della macchina oppure dell installazione
    * --- w_OBJ=this.oparentobject.getCTRL ("w_Opt_File")   // Non funziona!!!
    this.w_OBJ = this.w_OBJ.getCTRL ("w_Opt_File")
    this.oParentObject.w_PCNAME = Left( Sys(0), Rat("#",Sys( 0 ))-1)
    * --- Select from STMPFILE
    i_nConn=i_TableProp[this.STMPFILE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SFFLGSEL  from "+i_cTable+" STMPFILE ";
          +" where SFFLGSEL = 'S' AND SFPCNAME = "+cp_ToStrODBC(this.oParentObject.w_PCNAME)+"";
           ,"_Curs_STMPFILE")
    else
      select SFFLGSEL from (i_cTable);
       where SFFLGSEL = "S" AND SFPCNAME = this.oParentObject.w_PCNAME;
        into cursor _Curs_STMPFILE
    endif
    if used('_Curs_STMPFILE')
      select _Curs_STMPFILE
      locate for 1=1
      do while not(eof())
      this.w_MACCHINA = SFFLGSEL
        select _Curs_STMPFILE
        continue
      enddo
      use
    endif
    if NVL (this.w_MACCHINA,"N")<>"S"
      this.oParentObject.w_PCNAME = "INSTALLAZIONE"
    endif
    this.oparentobject.mcalc (.t.)
    * --- Per forzare il caricamento dei dati dovpo che sono stati impostati i parametri della query di estrazione dati
    this.w_OBJ.clear()     
    this.w_OBJ.init()     
    * --- Elimina l elemento vuoto e seleziona il primo elemento della lista
    if this.w_OBJ.listcount>=1
      this.w_OBJ.removeitem(1)     
      * --- Aggiorna l array contenente le estensioni  (non bisogna considerare l elemento cancellato che nell array delle estensioni � ancora presente)
      this.w_elemento = 1
      do while this.w_elemento<= this.w_OBJ.listcount
        this.w_OBJ.combovalues[this.w_elemento] = this.w_OBJ.combovalues[this.w_elemento + 1]
        this.w_elemento = this.w_elemento + 1
      enddo
    endif
    if this.w_OBJ.listcount>=1
      this.w_OBJ.listindex = 1
    endif
    this.w_OBJ = this.oparentobject
    * --- Recupero il nome della maschera chiamante perch� nel flusso autorizzazioni il ridimensionamento delle maschera provoca la perdita del focus
    this.w_OLDSTATE = this.w_OBJ.w_OLDSTATE
    this.w_maskcall = iif( VARTYPE(this.w_OBJ.noParentObject) ="O" and VARTYPE(this.w_OBJ.noParentObject.class) ="C", this.w_OBJ.noParentObject.class," " )
    public g_maskcall 
 g_maskcall=this.w_maskcall 
 public g_oldstate 
 g_oldstate=this.w_oldstate
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invia a Video
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = LEFT( L_OldCaption, 30 )
    endif
    this.w_OBJ.alwaysontop = .F.
    if this.w_OBJ.i_FDFFile
       ShellExecute(FindWindow(0,_screen.caption),"Open",this.w_OBJ.cNomeReport.FirstReport()+".FDF","",cHomeDir,1)
    else
      * --- Invia a Video
      if this.w_OBJ.cTipoReport="S"
        * --- Invio nuovo parametro 'V' per anteprima a Video
        do Run2Rep with this.w_OBJ,this.w_OBJ.oSplitReport, this.w_OBJ.prCopie, this.w_OBJ.prStamp, "V", this.w_OBJ.cModSst , this.w_OBJ
      else
        if this.w_OBJ.w_treport="I"
          l_sPrg="gsir_bir"
          if substr(this.w_OBJ.cNomeReport.FirstReport(),len(this.w_OBJ.cNomeReport.FirstReport())-3,1)="."
             do (l_sPrg) with this.w_OBJ, left(this.w_OBJ.cNomeReport.FirstReport(),len(this.w_OBJ.cNomeReport.FirstReport())-4), noParentObject, "V", -1
          else
             do (l_sPrg) with this.w_OBJ, this.w_OBJ.cNomeReport.FirstReport(), noParentObject, "F", -1
          endif
        else
          if vartype(this.pMultiReportSplit)="N" AND INLIST(this.pMultiReportSplit, 80, 90)
             do RunRep with this.w_OBJ,this.w_OBJ.oSplitReport, "V", this.w_OBJ.prFile, this.w_OBJ.prCopie, , , this.pMultiReportSplit
          else
             do RunRep with this.w_OBJ,this.w_OBJ.oSplitReport, "V", this.w_OBJ.prFile, this.w_OBJ.prCopie
          endif
        endif
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Private PrintErr 
 PrintErr = .f.
    * --- Con ON ERROR non si possono usare propriet� di un oggetto (per esempio 'on error this.w_miavar'  v� in errore)
    on error PrintErr=.T.
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = LEFT( L_OldCaption, 30 )
    endif
    * --- Invia a Stampante senza Richiesta
    if this.w_OBJ.i_FDFFile
       ShellExecute(FindWindow(0,_screen.caption),"Print",this.w_OBJ.cNomeReport.FirstReport()+".FDF","",chomedir,0)
    else
      if not(empty(this.w_OBJ.prStamp))
         set printer to name (alltrim (this.w_OBJ.prStamp))
      else
         set printer to default
      endif
      if not PrintErr
        if this.w_OBJ.cTipoReport="S"
          * --- Modifica per stampa solo Testo (tutto il resto e' standard del metodo click)
          *     Passo nuovo parametro 'S' per Invio a stampante
          *     Passo nuovo parametro cModSst che contiene il modello di stampante su Associazione Report/Device
           do Run2Rep with this.w_OBJ, this.w_OBJ.oSplitReport, this.w_OBJ.prCopie, this.w_OBJ.prStamp, "S", this.w_OBJ.cModSst , this.w_OBJ
        else
           do RunRep with this.w_OBJ,this.w_OBJ.oSplitReport, "S", this.w_OBJ.prFile, this.w_OBJ.prCopie
        endif
      else
         cp_Errormsg(MSG_UNABLE_TO_USE_PRINTER,16)
      endif
    endif
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = L_OldCaption
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia routine di esecuzione processo documentale
    * --- Memorizzo i dati relativi a invio Email e Fax
    this.w_ini_EMAIL = i_EMAIL
    this.w_ini_EMAILSUBJECT = i_EMAILSUBJECT
    this.w_ini_EMAILTEXT = i_EMAILTEXT
    this.w_ini_FAXSUBJECT = i_FAXSUBJECT
    this.w_ini_DEST = i_DEST
    this.w_ini_FAXNO = i_FAXNO
    if this.w_OBJ.w_treport="I"
      if substr(this.w_OBJ.cNomeReport,len(this.w_OBJ.cNomeReport)-3,1)="."
         do GSIR_BIR with this.w_OBJ, left(this.w_OBJ.cNomeReport,len(this.w_OBJ.cNomeReport)-4), noParentObject, "P", -1
      else
         do GSIR_BIR with _OBJ, this.w_OBJ.cNomeReport, noParentObject, "P", -1
      endif
    else
      this.w_oReport_Text = CREATEOBJECT("MultiReport")
      this.w_oReport_Graphic = CREATEOBJECT("MultiReport")
      this.w_OBJ.oSplitReport.SplitTextGraphic(this.w_oReport_Text, this.w_oReport_Graphic)     
      do GSDM_BPD with this.w_OBJ, "EP", this.w_OBJ.pdKeyIstanza, this.w_oReport_Graphic 
      this.w_oReport_Text = .NULL.
      this.w_oReport_Graphic = .NULL.
    endif
    * --- Ripristino i dati relativi a invio Email e Fax
     i_EMAIL = this.w_ini_EMAIL 
 i_EMAILSUBJECT = this.w_ini_EMAILSUBJECT 
 i_EMAILTEXT = this.w_ini_EMAILTEXT 
 i_FAXSUBJECT =this.w_ini_FAXSUBJECT 
 i_DEST = this.w_ini_DEST 
 i_FAXNO = this.w_ini_FAXNO 
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invia a Stampante senza Richiesat
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = LEFT( L_OldCaption, 30 )
    endif
    if not(empty(this.w_OBJ.prStamp))
      * --- Controlla se sul sistema c'� la stampante richiesta
      APRINTER( L_PRINTERS_ARRAY )
      this.w_PRINTER_ARRAY_LINE = ASCAN( L_PRINTERS_ARRAY, this.w_OBJ.prStamp,1,ALEN(L_PRINTERS_ARRAY,1),1,9)
      if this.w_PRINTER_ARRAY_LINE <> 0
        set printer to name (this.w_OBJ.prStamp)
      else
        this.w_OBJ.prStamp = ""
         set printer to default
      endif
    else
       set printer to default
    endif
     do RunRep with this.w_OBJ,this.w_OBJ.oSplitReport, "O", this.w_OBJ.prFile, this.w_OBJ.prCopie
    if TYPE("g_PRINTERDRIVERPATCH")<>"U" AND g_PRINTERDRIVERPATCH
      _screen.caption = L_OldCaption
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizio DOCM definizione metodi 'Click' per i bottoni
    * --- lancia routine di lancio maschera
    this.w_oReport_Text = CREATEOBJECT("MultiReport")
    this.w_oReport_Graphic = CREATEOBJECT("MultiReport")
    this.w_OBJ.oSplitReport.SplitTextGraphic(this.w_oReport_Text, this.w_oReport_Graphic)     
     do GSDM_BPD with this.w_OBJ, "VL", this.w_OBJ.pdKeyIstanza, this.w_oReport_Graphic
    this.w_oReport_Text = .NULL.
    this.w_oReport_Graphic = .NULL.
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziono report
    Local nomefi, i_err , oDesktop , oText
    this.w_cType = "W"
    this.Page_34()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_OBJ.nIndexReport>0 AND !this.w_OBJ.cNomeReport.IsEmpty()
      this.w_CurName = this.w_OBJ.cNomeReport.GetCursorName(this.w_OBJ.nIndexReport)
      this.w_RepName = this.w_OBJ.cNomeReport.GetReport(this.w_OBJ.nIndexReport)
      SELECT(this.w_CurName)
      this.w_ntuple = RECCOUNT(this.w_CurName)
      SELECT(this.w_CurName) 
 go top
      if not(eof())
        if g_OFFICE="M" And Type("g_AUTOFFICE")<>"U" And !g_AUTOFFICE And cp_IsStdFile(this.w_RepName, "docx")
          * --- Esportazione senza automazione solo se il file docx esiste
          if i_MonitorFramework and At(MSG_NOT_FILEEXIST,i_pathFindFile)<>0
            i_pathFindFile=""
          endif
          this.w_aggpath = cp_GetStdFile(this.w_RepName, "docx")
          this.w_AutoDir = Type("g_NoAutOfficeDir")="C" And !Empty(g_NoAutOfficeDir) And Directory(g_NoAutOfficeDir)
          this.w_NameExport = IIF(this.w_AutoDir, Addbs(g_NoAutOfficeDir)+JustStem(this.w_aggpath)+Sys(2015), Putfile("Save as", Addbs(g_tempadhoc)+Justfname(this.w_aggpath), "docx"))
          if !Empty(this.w_NameExport)
            vx_build("D", this.w_aggpath, this.w_CurName, this.w_NameExport, .F., .T.)
          endif
        else
          nomefi=ADDBS(Alltrim(g_tempadhoc))+ IIF(g_OFFICE="M",Sys(2015), "__word__")
          if ! g_OFFICE="M"
            copy to (nomefi) foxplus
          endif
          if g_OFFICE="M"
            if cp_fileexist(forceext(nomefi, "xlsx"),.T.)
              nomefiext=forceext(nomefi, "xlsx") 
              erase &nomefiext
            endif
            * --- Non lascia messaggi e non rende visibile il file
            cp_cursortoxlsx( FullPath(forceext( nomefi , "xlsx")) , "Foglio1" ,SELECT(0))
          endif
          this.w_bOk = .T.
          bOk=.T. 
 i_err=on("ERROR")
          on error bOk=.f.
          if g_OFFICE="M"
            * --- Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
            DecoratorState ("D")
            this.w_OBJ.word = createobject("word.application")
            this.w_OBJ.word.Visible = .F.
          endif
          if bOk
            if g_OFFICE="M"
              this.w_cdsname = forceext(nomefi, "xlsx")
              this.w_cexte = IIF(cp_IsStdFile(this.w_RepName,"DOCX"),"DOCX","DOC")
              if i_MonitorFramework and At(MSG_NOT_FILEEXIST,i_pathFindFile)<>0
                i_pathFindFile=""
              endif
              this.w_aggpath = cp_GetStdFile(this.w_RepName,this.w_cexte)
              if Empty(this.w_aggpath)
                this.w_OBJ.word.documents.open(Forceext(cp_GetStdFile(FULLPATH(this.w_RepName),this.w_cexte),this.w_cexte))     
              else
                this.w_OBJ.word.documents.open(Forceext(FULLPATH(this.w_aggpath),this.w_cexte))     
              endif
              this.w_cModelName = this.w_OBJ.word.activedocument.name
              * --- Zucchetti Aulla Inizio
              *     Setta correttamente il datasource sul modello
              this.w_csqlstatement = "SELECT * FROM [Foglio1$]"
              this.w_OBJ.word.ActiveDocument.MailMerge.OpenDataSource(this.w_cdsname,this.w_cformat,this.w_cconfirmconv,this.w_creadonly,this.w_clinktosource,this.w_caddtofilelist,"","",this.w_crevert,this.w_cprotectdoc,this.w_cprotecttemp,"",this.w_csqlstatement)     
              this.w_OBJ.word.ActiveDocument.MailMerge.Execute()     
              this.w_cDocName = this.w_OBJ.word.activedocument.name
              * --- La close va in errore per i documenti di word 97'
              this.w_OBJ.word.documents(this.w_cModelName).Close(0)     
              this.w_OBJ.word.documents(this.w_cDocName).activate()     
              this.w_OBJ.word.visible = .t.
              if cp_fileexist(forceext(nomefi, "xlsx"),.T.)
                fileext=forceext(nomefi, "xlsx") 
                fileext='"'+fileext+'"'
                erase &fileext
              endif
            else
              * --- *** Gestione OpenOffice ***
              this.w_cdsname = nomefi +".dbf"
              oManager= CreateObject("com.sun.star.ServiceManager") 
              oDesktop=oManager.createInstance("com.sun.star.frame.Desktop") 
              comarray(oDesktop,10)
              this.w_oDBc = oManager.createInstance("com.sun.star.sdb.DatabaseContext") 
              this.w_oDataSource = this.w_oDBc.getByName("Bibliography")
              this.w_oDataSource.URL = "sdbc:dbase:file:///"+strtran(ADDBS(Alltrim(g_tempadhoc)),CHR(92),CHR(47)) 
              this.w_objMailMerge = oManager.createInstance("com.sun.star.text.MailMerge") 
              this.w_aggpath = cp_GetStdFile(this.w_RepName,"SXW")
              if Empty(this.w_aggpath)
                this.w_objMailMerge.DocumentURL="file:///"+StrTran(ALLTRIM(FULLPATH(Forceext(this.w_repname,"SXW"))),CHR(92),CHR(47)) 
              else
                this.w_objMailMerge.DocumentURL="file:///"+StrTran(ALLTRIM(Forceext(FULLPATH(this.w_aggpath),"SXW")),CHR(92),CHR(47)) 
              endif
              this.w_objMailMerge.CommandType = 0
              this.w_outType = INT(VAL(ALLTRIM(g_PRINTMERGE)))
              this.w_objMailMerge.OutputType = IIF(this.w_outType=1 or this.w_outType=2, this.w_outType, 2)
              this.w_objMailMerge.OutputURL = "file:///"+STRTRAN(ALLTRIM(tempadhoc()),CHR(92),CHR(47)) 
              DIMENSION Args(2), DocArgs(1), newDoc(1)
              Args[1] = oManager.Bridge_GetStruct("com.sun.star.beans.NamedValue") 
 Args[1].name = "DataSourceName" 
 Args[1].value = "Bibliography" 
 Args[2] = oManager.Bridge_GetStruct("com.sun.star.beans.NamedValue") 
 Args[2].name = "Command" 
 Args[2].value = "__word__" 
              this.w_objMailMerge.execute(@Args)     
              if this.w_objMailMerge.OutputType = 2
                * --- Se non mando in stampa concateno i files in un unico documento...
                DocArgs[1] = oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue") 
 newDoc[1] = oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue") 
 newDoc[1].name="AsTemplate" 
 newDoc[1].Value=".T."
                * --- Crea nuovo Documento Writer vuoto ***
                this.w_oWriter = oDesktop.loadComponentFromURL("private:factory/swriter", "_blank", 0, @newDoc)
                oText = this.w_oWriter.getText()
                this.w_oCurs = oText.createTextCursorByRange(oText.getEnd())
                this.w_oCurs.collapseToStart()     
                * --- Carica le stampe unione salvate in un unico file
                this.w_nomefile = ALLTRIM(justfname(this.w_aggpath))
                this.w_i = 0
                do while this.w_i<= this.w_ntuple
                  this.w_PATH = ALLTRIM(addbs(tempadhoc()))+this.w_nomefile+ALLTRIM(STR(INT(this.w_i)))+".sxw"
                  this.w_cPath_new = ALLTRIM(addbs(tempadhoc()))+this.w_nomefile+ALLTRIM(STR(INT(this.w_i)))+".odt"
                  this.w_bmod_exists = .f.
                  if cp_fileexist(this.w_PATH)
                    this.w_cPath_ok = this.w_PATH
                    this.w_bmod_exists = .t.
                  else
                    if cp_fileexist(this.w_cPath_new)
                      this.w_cPath_ok = this.w_cPath_new
                      this.w_bmod_exists = .t.
                    endif
                  endif
                  if this.w_bmod_exists
                    this.w_oCurs.insertDocumentFromURL("file:///"+StrTran(ALLTRIM(this.w_cPath_ok),CHR(92),CHR(47)), @DocArgs)     
                    this.w_oCurs.collapseToEnd()     
                    if this.w_i!=(this.w_ntuple-1)
                      this.w_oCurs.BreakType = 5
                      oText.insertControlCharacter ( this.w_oCurs,0,.f. ) 
                      this.w_oCurs.setString(chr(13)+chr(10))     
                      this.w_oCurs.collapseToEnd()     
                    endif
                    * --- Cancella i file temporanei
                    DELETE FILE (this.w_cPath_ok)
                  endif
                  this.w_i = this.w_i + 1
                enddo
              endif
            endif
            * --- Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
            *     perch� serve solo per decidere l'icona da utilizzare.
            if ! bOk
              if g_OFFICE="M"
                cp_ErrorMsg(MSG_CANNOT_FIND_WORD_QM,"stop",MSG_ERROR)
              else
                cp_ErrorMsg(MSG_CANNOT_FIND_WRITER,"stop",MSG_ERROR)
              endif
            else
              cp_ErrorMsg(MSG_WRITING_FINISHED_CORRECTLY)
            endif
          else
            if g_OFFICE="M"
              cp_ErrorMsg(MSG_CANNOT_OPEN_WORD_DOCUMENT_QM,"stop",MSG_ERROR) 
            else
              cp_ErrorMsg(MSG_CANNOT_OPEN_WRITER_DOCUMENT,"stop",MSG_ERROR)
            endif
          endif
          if g_OFFICE="M"
            * --- Al termine del esportazion riattiva il decorator dei form 
            DecoratorState ("A")
          endif
          * --- Zucchetti Aulla Inizio
          *     Elimina file temporaneo
          nomefi=forceext(nomefi, "dbf") 
          if cp_fileexist(nomefi,.T.)
            fileext='"'+nomefi+'"'
            erase &fileext
            * --- Cerca eventuale .fpt
            nomefi=forceext(nomefi, "fpt")
            if cp_fileexist( nomefi ,.T.)
              fileext='"'+nomefi+'"'
              erase &fileext
            endif
            * --- Cerca eventuale dbt
            n=forceext(nomefi, "dbt")
            if cp_fileexist( nomefi,.T.)
              fileext='"'+nomefi+'"'
              erase &fileext
            endif
          endif
          * --- Zucchetti Aulla Fine
           on error &i_err
        endif
      else
        cp_ErrorMsg(MSG_NO_DATA_TO_PRINT,"stop",MSG_ERROR)
      endif
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziono report
    this.w_cType = "E"
    this.Page_34()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_OBJ.nIndexReport>0
      Local i_nFile,i_err , bOk
      this.w_CurName = this.w_OBJ.cNomeReport.GetCursorName(this.w_OBJ.nIndexReport)
      this.w_RepName = this.w_OBJ.cNomeReport.GetReportOrig(this.w_OBJ.nIndexReport)
      this.w_RepCodISO = this.w_OBJ.cNomeReport.GetCodISO(this.w_OBJ.nIndexReport)
      * --- Zucchetti Aulla - Inizio Gestione Suite OpenOffice/StarOffice
      Local Array NoArgs[1]
      * --- Zucchetti Aulla - Fine Gestione Suite OpenOffice/StarOffice
      Select(this.w_CurName) 
 Go Top
      this.w_bOk = .T.
       bOk=.T.
      i_err=On("ERROR") 
 On Error bOk=.F.
      * --- Se � stato indicato un modello esplicitamente (nell output utente o da batch)  allora si utilizza tale modello altrimenti si cerca priam il modello per excel/word 97
      this.w_cexte = this.w_OBJ.cNomeReport.GetExtReport(this.w_OBJ.nIndexReport)
      this.w_cexte = IIF (cp_IsStdFile(this.w_RepName,"XLTX"), "XLTX", "XLT")
      i_Ext=this.w_cexte
      if i_MonitorFramework AND At(MSG_NOT_FILEEXIST,i_pathFindFile)!=0
        i_pathFindFile=""
      endif
      if g_OFFICE="M"
        this.w_NomeExcel = FULLPATH(forceext(cp_GetStdFile(this.w_RepName,this.w_cexte),this.w_cexte))
        * --- L'utilizzo della ve_build � condizionato dal check Automazione office all'interno della Suite Office, dalla variabile g_ExcelNoModel nel caso di assenza modello, 
        * --- oppure se non � prevista l'Automazione ma non � presente il modello XLTX (fondamentale per l'esportazione tramite XML)
        this.w_bAutoOffice = Type("g_AUTOFFICE")="U" Or g_AUTOFFICE
        this.w_cOldExp_NoModel = this.w_bAutoOffice And ((cp_IsStdFile(this.w_NomeExcel, "XLTX") Or cp_IsStdFile(this.w_NomeExcel, "XLT") Or cp_IsStdFile(this.w_NomeExcel, "XLTM")) Or (Type("g_ExcelNoModel")="U" Or g_ExcelNoModel<>1))
        this.w_cOldExp_OldModel = !this.w_bAutoOffice And !cp_IsStdFile(this.w_NomeExcel, "XLTX") And (cp_IsStdFile(this.w_NomeExcel, "XLT") Or cp_IsStdFile(this.w_NomeExcel, "XLTM"))
        this.w_bOldExport = this.w_cOldExp_NoModel Or this.w_cOldExp_OldModel
        if this.w_bOldExport AND bOk
          * --- Cmq istanzio un foglio excel che abbia o meno un modello...
          this.w_OBJ.excel = Createobject("excel.application")
          * --- Eseguo la traduzione se necessario...
          if Not(this.w_OBJ.cNomeReport.GetCodISO(this.w_OBJ.nIndexReport)==this.w_OBJ.cLanguage)
            this.w_OBJ.excel.Quit()     
            this.w_OBJ.excel = null
            this.w_NewExcel = cp_TranslateExcel(JUSTSTEM(this.w_NomeExcel),this.w_RepCodISO,this.w_OBJ.cLanguage,this.w_OBJ.cSessionCode,this.w_NomeExcel)
            if Not(Empty(this.w_NewExcel))
              this.w_NomeExcel = this.w_NewExcel
            endif
            this.w_OBJ.excel = createobject("excel.application")
          endif
          * --- Se non c � excel l esportazione senza modello pu� essere fatta  (nella ve_build c � il controllo che blocca l elaborazione nel caso in cui non sia stato istanziato l oggetto excel.applicatioon)
          bOk=.T.
        endif
        if bOk
          if this.w_bOldExport
            cp_msg(cp_Translate(MSG_BUILDING_EXCEL_WORKSHEET_D))
            ve_build(this.w_OBJ.excel, this.w_CurName, EVL((FORCEEXT(this.w_NomeExcel, this.w_cexte)),Forceext(Justfname(Alltrim(this.w_OBJ.prFile)), ".XLS")), .F., , , iif( TYPE ("this.oParentObject.noparentobject.class")="C" AND lower(this.oParentObject.noparentobject.class)="zoombox", this.oParentObject.noparentobject, .f.))
            this.w_OBJ.excel = .null.
          else
            if Empty(Justfname(this.w_NomeExcel))
              this.w_NomeExcel = ForceExt(this.w_OBJ.prFile, "xltx")
            endif
            if !Empty(this.w_RepCodISO) And !Empty(this.w_OBJ.cLanguage) And Not(this.w_RepCodISO==this.w_OBJ.cLanguage)
              this.w_cTranslate = this.w_RepCodISO+";"+this.w_OBJ.cLanguage
              this.w_cNameTranslate = Justfname( ForceExt( Addbs(JustPath(this.w_NomeExcel))+JustStem(this.w_NomeExcel)+"_"+Alltrim(this.w_OBJ.cLanguage)+Iif(Vartype(this.w_OBJ.cSessionCode)<>"C" Or Empty(this.w_OBJ.cSessionCode), Sys(2015), this.w_OBJ.cSessionCode), "xlsx"))
            else
              this.w_cTranslate = ""
              this.w_cNameTranslate = ForceExt(Justfname(this.w_NomeExcel), "xlsx")
            endif
            this.w_AutoDir = Type("g_NoAutOfficeDir")="C" And !Empty(g_NoAutOfficeDir) And Directory(g_NoAutOfficeDir)
            this.w_NameExport = IIF(this.w_AutoDir, Addbs(g_NoAutOfficeDir)+JustStem(this.w_cNameTranslate)+Sys(2015), Putfile("Save as", Addbs(g_tempadhoc)+this.w_cNameTranslate, "xlsx"))
            if !Empty(this.w_NameExport)
              vx_build("X", this.w_NomeExcel, this.w_CurName, this.w_NameExport, .F., .T., this.w_cTranslate)
            endif
          endif
        else
          * --- Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
          *      perch� serve solo per decidere l'icona da utilizzare.
          cp_ErrorMsg(MSG_CANNOT_OPEN_EXCEL_WORKSHEET_QM+Chr(13)+Message()+Message(1),"stop",MSG_ERROR)
        endif
      else
        do case
          case cp_IsStdFile(this.w_RepName,"OTS")
            this.w_cexte = "OTS"
          case cp_IsStdFile(this.w_RepName,"STC")
            this.w_cexte = "STC"
          case cp_IsStdFile(this.w_RepName,"XLT")
            this.w_cexte = "XLT"
          case cp_IsStdFile(this.w_RepName,"XLTX")
            this.w_cexte = "XLTX"
          otherwise
            this.w_cexte = ""
        endcase
        this.w_NomeCalc = FULLPATH(cp_GetStdFile(this.w_RepName,this.w_cexte))
        if Not(this.w_OBJ.cNomeReport.GetCodISO(this.w_OBJ.nIndexReport)==this.w_OBJ.cLanguage)
          this.w_NewCalc = cp_TranslateCalc(this.w_NomeCalc,this.w_RepCodISO,this.w_OBJ.cLanguage,this.w_OBJ.cSessionCode)
          if Not(Empty(this.w_NewCalc))
            this.w_NomeCalc = this.w_NewCalc
          endif
        endif
        cp_msg(cp_Translate(MSG_CREATING_SPREADSHEET_DOCUMENT))
        vo_build(this.w_CurName, ForceExt(this.w_NomeCalc,this.w_cexte), .F., , iif(TYPE ("this.oParentObject.noparentobject.class")="C" AND lower(this.oParentObject.noparentobject.class)="zoombox", this.oParentObject.noparentobject, .f.))
      endif
      Wait Clear
      On Error &i_err
    else
      this.w_OBJ.nIndexReport = 1
    endif
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziono report
    this.w_cType = "G"
    this.Page_34()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_OBJ.nIndexReport>0 AND !this.w_OBJ.cNomeReport.IsEmpty()
      this.w_CurName = this.w_OBJ.cNomeReport.GetCursorName(this.w_OBJ.nIndexReport)
      this.w_RepName = this.w_OBJ.cNomeReport.GetReport(this.w_OBJ.nIndexReport)
      SELECT(this.w_CurName) 
 go top
      if FILE( FULLPATH(forceext(cp_GetStdFile(this.w_RepName,"VFC"),"VFC")) )
        this.w_param = this.w_CurName + "," + FULLPATH(forceext(cp_GetStdFile(this.w_RepName,"VFC"),"VFC")) 
        VX_EXEC( this.w_param )
      else
        vg_build(this.w_CurName,FULLPATH(forceext(cp_GetStdFile(this.w_RepName,"VGR"),"vgr")),.t.,.t.)
      endif
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OBJ.cNomeReport.IsStdFile(iif(this.w_OBJ.i_FDFFile,"FDF","FRX"),"G")
       do GSUT_BXP with this.w_OBJ, 2, iif(this.w_OBJ.i_FDFFile,"S","N")
    else
       cp_ErrorMsg(MSG_NO_PRINTFILE_SPECIFIED,"stop")
    endif
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_IDMSAsa
        this.w_nOperaz = 6
      case g_WEENABLED="S" AND g_IZCP<>"S" and g_IZCP<>"A"
        this.w_nOperaz = 4
      otherwise
        this.w_nOperaz = IIF(g_IZCP$"SA" OR g_CPIN="S" OR g_DMIP="S",5,6)
    endcase
    if this.w_OBJ.cNomeReport.IsStdFile(iif(this.w_OBJ.i_FDFFile,"FDF","FRX"))
       local l_sPrg 
 l_sPrg="GSUT_BXP" 
 do (l_sPrg) with this.w_OBJ, this.w_nOperaz, iif(this.w_OBJ.i_FDFFile,"S","N") 
 l_sPrg=Null
    else
      cp_ErrorMsg(MSG_NO_PRINTFILE_SPECIFIED,"stop")
    endif
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    =PLiteEXE(this.w_OBJ.oSplitReport)
  endproc


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OBJ.cNomeReport.IsStdFile("FRX")
      do GSUT_BXP with this.w_OBJ, 3 
    else
      cp_ErrorMsg(MSG_NO_PRINTFILE_SPECIFIED,"stop")
    endif
  endproc


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    public i_designfile
    i_designfile=alltrim(this.w_OBJ.cNomeReport.areportlist[this.w_OBJ.w_INDICE,3].cqryname)+".VQR"
     vq_build() 
 release i_designfile
  endproc


  procedure Page_16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    LOCAL l_prg
    if TYPE("noParentObject.oParentobject.w_codstr")="C"
      if this.pPar="EdiClick"
        GSVA_BGF(.null.,noParentObject)
      else
        this.w_SERIAL = noParentObject.oParentobject.w_MVSERIAL
        this.w_MVCLADOC = noParentObject.oParentobject.w_MVCLADOC
        this.w_MVFLSEND = noParentObject.oParentobject.w_MVFLSEND
        do case
          case ! this.w_MVCLADOC $ "FA-NC"
            ah_errormsg("Approva documento per fatturazione elettronica non prevista per la tipologia documento")
          case this.w_MVFLSEND $ "S-F"
            ah_errormsg("Documento gi� approvato")
          otherwise
            * --- Try
            local bErr_05248388
            bErr_05248388=bTrsErr
            this.Try_05248388()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              ah_errormsg("Errore nell'approvazione del documento per fatturazione elettronica")
            endif
            bTrsErr=bTrsErr or bErr_05248388
            * --- End
        endcase
      endif
    else
      if this.pPar="EdiClick"
        ah_errormsg("Generazione file EDI non prevista!")
      else
        ah_errormsg("Approvazione documento per fatturazione elettronica non prevista!")
      endif
    endif
  endproc
  proc Try_05248388()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLSEND ="+cp_NullLink(cp_ToStrODBC("F"),'DOC_MAST','MVFLSEND');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MVFLSEND = "F";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_errormsg("Approvato documento da inviare a Fatel","!")
    return


  procedure Page_17
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_bReOpen = .F.
    if this.w_OBJ.pdProcesso
      * --- * Se non � richiesta la conservazione del log oppure se il processo non � stato eseguito, procede con cancellazione
      if not this.w_OBJ.pdMantieniLog or not this.w_OBJ.pdExeProcesso
        DO GSDM_BPD with this.w_OBJ, "CL", this.w_OBJ.pdKeyIstanza
      endif
      * --- Se c'� un processo ed � stato eseguito
      if this.w_OBJ.pdExeProcesso OR this.w_OBJ.pdObbligatorio
        * --- Se ho eseguito tutto il treno di stampe con il processo esco,
        *     altrimenti seleziono solo i report che non hanno partecipato al processo documentale
        this.w_numrep = this.w_OBJ.oOrigMReport.GetNumReport()
        this.w_count = this.w_numrep
        do while this.w_count>=1
          if this.w_OBJ.cNomeReport.GetIndexByKey(this.w_OBJ.oOrigMReport.GetKeyReport(this.w_count))<>-1
            * --- Se gi� processato rimuovo il report
            this.w_OBJ.oOrigMReport.RemoveReport(this.w_count)     
          endif
          this.w_count = this.w_count - 1
        enddo
        if !this.w_OBJ.oOrigMReport.IsEmpty()
          this.w_bReOpen = .T.
        endif
      else
        this.w_bReOpen = .T.
      endif
    endif
    * --- Riapertura print system dopo processo documentale  
    if this.w_bReOpen
      * --- Processo non eseguito e non obbligatorio => passa a print system standard
      this.w_OBJ.PdProcesso = .f.
      * --- Ricarica il menu 'Salva su file' 
      i_LoadMenu=.T.
      this.w_BOTMENU = this.w_OBJ.GetCtrl ("\<Salva")
      this.w_BOTMENU.ReLoadmenu()     
      this.w_BOTMENU = this.w_OBJ.GetCtrl ("S\<tampa")
      if vartype (this.w_BOTMENU)="O"
        this.w_BOTMENU.ReLoadmenu()     
      else
        this.w_BOTMENU = this.w_OBJ.GetCtrl ("S\<tampa GDI+")
        if vartype (this.w_BOTMENU)="O"
          this.w_BOTMENU.ReLoadmenu()     
        endif
      endif
      this.w_BOTMENU = this.w_OBJ.GetCtrl ("\<Anteprima")
      this.w_BOTMENU.ReLoadmenu()     
      i_LoadMenu=.F.
      this.w_OBJ.cNomeReport = this.w_OBJ.oOrigMReport
      this.w_OBJ.cNomeReport.SetAllPrinter()     
      * --- Ripopolo lo zoom con i dati del multireport originale
      this.w_CURSOR = this.w_OBJ.w_Zoom_Printer.cCursor
      if USED(this.w_CURSOR)
        DELETE FROM (this.w_CURSOR) WHERE 1=1
      endif
      LoadConfig (this.w_OBJ)
      this.w_OBJ.NotifyEvent("AggiornaZoom")     
      * --- Aggiorno il numero di report selezionati
      this.w_OBJ.w_SELECTED = this.w_OBJ.cNomeReport.GetNumReport()
      * --- Aggiorno valori stampante su print system
      this.w_OBJ.cNomeReport.IsOnePrinter()     
      this.Page_23()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Zucchetti Aulla-Schedulatore Inizio*
      if TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
        this.Page_30()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_OBJ.Refresh()     
      endif
      * --- Zucchetti Aulla-Schedulatore Fine
    else
      this.w_OBJ.CloseAllTables()     
      * --- Cancello i report tradotti
      if !(VARTYPE(g_DISABLEREPORTTRANSLATION)="L" AND g_DISABLEREPORTTRANSLATION)
        * --- Cancello FRX
        this.w_i = 1
        this.w_numrep = ADIR(aRepList, ADDBS(TEMPADHOC())+"*"+this.w_OBJ.cSessionCode+".FR?")
        do while this.w_i<=this.w_numrep
          this.w_nomefile = ADDBS(TEMPADHOC())+aRepList[this.w_i,1]
          if cp_FileExist(this.w_NomeFile)
            erase(this.w_NomeFile)
          endif
          this.w_i = this.w_i + 1
        enddo
      endif
      this.w_OBJ.release()     
      this.w_OBJ = .NULL.
    endif
  endproc


  procedure Page_18
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia maschera delle opzioni associata al formato e alla classe
    this.w_OBJ.cOption = GSUT_BSF(this.w_OBJ, "OPEN", this.w_OBJ.w_FormFile)
    * --- Stampo su file
    if NOT EMPTY(this.w_OBJ.cOption)
      * --- Sulla maschera per la stampa con opzioni c'� gi� il flag apri dopo generazione
      *     In questo caso non controllo il flag
      this.w_OLDFLOPEN = this.w_OBJ.w_SFFLOPEN
      this.w_OBJ.w_SFFLOPEN = "N"
      this.Page_19()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_OBJ.w_SFFLOPEN = this.w_OLDFLOPEN
    endif
    * --- Resetto i parametri
    this.w_OBJ.cOption = ""
  endproc


  procedure Page_19
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invia su File
    if not(empty(this.w_OBJ.w_txt_file))
      this.w_OBJ.w_txt_file = alltrim(this.w_OBJ.w_txt_file)
      if cp_fileexist(this.w_OBJ.w_txt_file)
        if !ah_yesno("File gi� esistente vuoi sovrascrivere?",64)
          i_retcode = 'stop'
          return
        endif
      endif
      if this.w_OBJ.w_treport="I"
        do gsir_bir with this.w_OBJ, this.w_OBJ.cNomeReport.FirstReport(), noParentObject, "F", -1, .null., this.w_OBJ.w_txt_file
      else
        if chknfile(this.w_OBJ.w_txt_file,"F")
          if INLIST( UPPER(ALLTRIM(this.w_OBJ.w_TIPOFILE)),"DBF","SDF","DELIMITED") Or this.w_OBJ.cNomeReport.IsStdFile("FRX", "G")
            GSUT_BXP(this.w_OBJ, 1)
          else
            ah_ErrorMsg("Funzione non disponibile. Utilizzare i formati DBF, SDF o Delimited, se disponibili","stop")
          endif
          if TYPE("This.w_OBJ.nAzAutom")="L" 
 
            do case
              case FILE(this.w_OBJ.w_txt_file)
                if this.oParentObject.w_SFFLOPEN = "S" And ah_YesNo("Visualizzare il file %1 con l'applicazione ad esso associata?", 64, this.w_OBJ.w_txt_file, , , , ,.T.)
                  StaPDF(this.w_OBJ.w_txt_file, "Open", "", .T.)
                endif
              case FILE(ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file)
                if this.oParentObject.w_SFFLOPEN = "S" And ah_YesNo("Visualizzare il file %1 con l'applicazione ad esso associata?", 64, ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file, , , , ,.T.)
                  StaPDF(ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file, "Open", "", .T.)
                endif
                this.w_OBJ.w_txt_file = ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file
              case FILE(ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file)) OR FILE(ALLTRIM(TempAdhoc () +"\"+ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file)))
                if NOT FILE(ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file)) 
                  this.w_OBJ.w_txt_file = ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file
                endif
                if FILE(ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_2."+JUSTEXT(this.w_OBJ.w_txt_file))
                  this.w_FILENAME = ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file)
                  if this.oParentObject.w_SFFLOPEN = "S" And ah_YesNo("Il salvataggio nel formato %1 ha generato pi� files%0Si vuole aprire la cartella di destinazione %2?", 64, JustExt(this.w_FILENAME), JustPath(this.w_FILENAME), , , ,.T.)
                    StaPDF("Explorer.exe","Open", '/n,/select,"'+this.w_FILENAME+'"', .T.)
                  endif
                else
                  this.w_FILENAME = ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file)
                  if this.oParentObject.w_SFFLOPEN = "S" And ah_YesNo("Visualizzare il file %1 con l'applicazione ad esso associata?", 64, this.w_FILENAME, , , , ,.T.)
                    StaPDF(this.w_FILENAME, "Open", "", .T.)
                  endif
                endif
              otherwise
                cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,upper(this.w_OBJ.w_txt_file)),"stop")
            endcase
          endif
        endif
      endif
    else
      cp_ErrorMsg(MSG_FILE_NAME_MISSING_QM,"stop")
    endif
  endproc


  procedure Page_20
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_ARCHI="S"
      this.w_tempstamp = PrinterChecked(ALLTRIM(g_PRINTERARCHI))
      if NOT EMPTY(this.w_tempStamp)
        do RunRep with this.w_OBJ,this.w_OBJ.oSplitReport, "S", this.w_OBJ.prFile, this.w_OBJ.prCopie, this.w_tempStamp, .t.
      else
        cp_ErrorMsg(MSG_PRINTER_ARCHEASY_NOT_FOUND,"stop") 
      endif
    else
      if g_CPIN="N" AND g_DMIP="N" AND g_IZCP$"SA" AND this.w_OBJ.oBtn_WWP_ZCP.enabled
        this.w_IDMSAsa = .T.
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Page_21
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invia su File
    if not(empty(this.w_OBJ.w_txt_file))
      if this.w_OBJ.w_treport="I"
        * --- Non gestito in AET
        i_retcode = 'stop'
        return
      else
        if chknfile(this.w_OBJ.w_txt_file,"F")
          if this.w_OBJ.cNomeReport.IsStdFile("FRX", "G")
            this.w_OBJ.prFile = FORCEEXT(this.w_OBJ.w_txt_file, this.w_OBJ.cExtFormat) 
            do GSUT_BXP with this.w_OBJ, 1 && Creo solo il file
          else
            cp_ErrorMsg(MSG_NO_PRINTFILE_SPECIFIED,"stop")
          endif
          * --- Se il file � stato creato nella temp aggiorna il nome del file sulla print system
          if NOT FILE(this.w_OBJ.w_txt_file) AND NOT FILE(ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file))
            if FILE(ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file)
              this.w_OBJ.w_txt_file = ALLTRIM(TempAdhoc () ) +"\"+this.w_OBJ.w_txt_file
            endif
          endif
          if NOT FILE(this.w_OBJ.w_txt_file) AND NOT FILE(ADDBS(JUSTPATH(this.w_OBJ.w_txt_file))+JUSTSTEM(this.w_OBJ.w_txt_file)+"_Pag_1."+JUSTEXT(this.w_OBJ.w_txt_file))
            cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,upper(this.w_OBJ.w_txt_file)),"stop")
          else
            do GSUT_BSO with this.w_OBJ, "GB",this.w_OBJ.w_txt_file
          endif
        endif
      endif
    else
      cp_ErrorMsg(MSG_FILE_NAME_MISSING_QM,"stop")
    endif
  endproc


  procedure Page_22
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziono repor
    if VARTYPE (this.w_OBJ.w_INDICE) ="N"
      this.w_OBJ.nIndexReport=this.w_OBJ.w_INDICE
    else
      this.w_cType = .F.
      this.Page_34()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_OBJ.nIndexReport>0
      local i_nCurs 
 i_nCurs = sys(2015)
      this.w_reportname = this.w_OBJ.cNomeReport.GetReportOrig(this.w_OBJ.nIndexReport)
      this.w_cexte = IIF(this.w_OBJ.cNomeReport.GetText(this.w_OBJ.nIndexReport), ".FXP", ".FRX")
      this.w_oa = select()
      this.w_workstation = LEFT(SYS(0), RAT(" # ",SYS(0))-1)
      * --- Zucchetti Aulla Inizio : modificata frase Sql per intercettare anche associazioni su report solo testo
      if i_ServerConn[1,2]<>0
        cp_sql(i_ServerConn[1,2],"select * from cpusrrep where repasso="+cp_ToStr(this.w_reportname+this.w_cexte)+" and (aziasso="+cp_ToStr(i_codazi)+" or aziasso='          ' or aziasso is null) and (usrasso="+cp_ToStr(i_codute)+" or usrasso=0 or usrasso is null) and (wstasso="+cp_ToStr(this.w_workstation)+" or wstasso='          ' or wstasso is null)" ,i_nCurs)
      else
        select * from cpusrrep where repasso=this.w_reportname+this.w_cexte AND (TRIM(i_codazi)==TRIM(aziasso) OR EMPTY(aziasso)) AND (usrasso=i_codute or usrasso=0) into curs (i_nCurs)
      endif
      * --- Zucchetti Aulla Fine
      if used(i_nCurs)
        * --- Ordino per utente e azienda in modo da prendere sempre prima le associazioni con utente e/o azienda valorizzate 
        *     indipendentemente dall'ordine di caricamento; ordino il cursore fox per evitare problemi con DB2/Oracle e il verso asc o desc
        SELECT * FROM (i_nCurs) ORDER BY usrasso DESC, aziasso DESC INTO CURSOR (i_nCurs)
        if not(eof())
          this.w_nro = NROASSO
          this.w_oggetto = cpusrrep()
          this.w_oggetto.w_NROASSO = this.w_nro
          * --- i_obj.QueryKeySet("NROASSO=w_OBJ.w_NROASSO","")
          *     Zucchetti Aulla Inizio : inserito IIF(tReport='S',RptTxt_Search+'.FXP',w_OBJ.cNomeReport+'.FRX'). Prima gestiva solo FXP
          *     i_obj.QueryKeySet("REPASSO="+cp_ToStr(l_reportname+l_Ext),"") 
          *     Zucchetti Aulla Fine
          *     i_obj.LoadRec()
          this.w_oggetto.ecpFilter()     
          this.w_oggetto.w_REPASSO = this.w_reportname + this.w_cexte
          this.w_oggetto.w_USRASSO = NVL(&i_nCurs..USRASSO,0)
          this.w_oggetto.w_AZIASSO = NVL(&i_nCurs..AZIASSO , "")
          this.w_oggetto.ecpSave()     
          this.w_oggetto.ecpEdit()     
        else
          this.w_oggetto = cpusrrep()
          this.w_oggetto.ecpLoad()     
          this.w_oggetto.w_USRASSO = i_codute
          this.w_oggetto.w_USRASSO = i_codute
          this.w_ovar = this.w_oggetto.GetCtrl("w_USRASSO")
          this.w_ovar.check()     
          this.w_oggetto.w_AZIASSO = i_codazi
          * --- Zucchetti Aulla Inizio : inserito IIF(tReport='S',RptTxt_Search+'.FXP',w_OBJ.parent.cNomeReport+'.FRX'). Prima gestiva solo FXP
          this.w_oggetto.w_REPASSO = this.w_reportname + this.w_cexte
          * --- Zucchetti Aulla Fine
          this.w_oggetto.w_COPASSO = 1
          if not(empty(this.w_OBJ.prStamp))
            this.w_oggetto.w_DEVASSO = this.w_OBJ.prStamp
          endif
          this.w_oggetto.mCalc(.T.)     
        endif
        USE IN SELECT(i_nCurs)
      else
         cp_msg(cp_Translate(MSG_ERROR_OPENING_PRINTING_TABLE))
      endif
       select (this.w_oa)
    endif
  endproc


  procedure Page_23
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scelta Stampante
    if this.w_OBJ.cNomeReport.GetNumReport()=1
      this.w_stp = this.w_OBJ.w_Zoom_Printer.GETVAR ("PRINTER")
      this.w_OBJ.oTxt_Device.enabled = this.w_OBJ.cNomeReport.bStampanti
      this.w_OBJ.w_DEVICE = this.w_OBJ.cNomeReport.cStampante
      this.w_OBJ.oTxt_Device.Displayvalue = UPPER (this.w_OBJ.w_DEVICE)
      this.w_OBJ.prStamp = this.w_OBJ.cNomeReport.cStampante
      if not(empty(this.w_stp))
        this.w_OBJ.cNomeReport.SetRepPrinter(this.w_stp, 1)     
        this.w_OBJ.oSplitReport.SetRepPrinter(this.w_stp, 1)     
      endif
      this.w_OBJ.w_Txt_Copies = INT(this.w_OBJ.cNomeReport.nNumCopy)
      this.w_OBJ.prCopie = INT (this.w_OBJ.cNomeReport.nNumCopy)
    else
      this.w_OBJ.oSplitReport.SetInfoFromParent(this.w_OBJ.cNomeReport)     
      this.w_OBJ.w_Txt_Copies = INT(this.w_OBJ.cNomeReport.nNumCopy)
      this.w_OBJ.prCopie = INT (this.w_OBJ.cNomeReport.nNumCopy)
      this.w_OBJ.oTxt_Device.enabled = this.w_OBJ.cNomeReport.bStampanti
      this.w_OBJ.w_DEVICE = this.w_OBJ.cNomeReport.cStampante
      this.w_OBJ.oTxt_Device.Displayvalue = UPPER (this.w_OBJ.w_DEVICE)
      this.w_OBJ.prStamp = this.w_OBJ.cNomeReport.cStampante
      * --- Numero copie
      this.w_OBJ.oSplitReport.bNumCopy = this.w_OBJ.cNomeReport.bNumCopy
      this.w_OBJ.oSplitReport.nNumCopy = INT (this.w_OBJ.cNomeReport.nNumCopy)
    endif
  endproc


  procedure Page_24
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Clicca Export
    this.w_var = ""
    this.w_oldpath = sys(5)+sys(2003)
    this.w_path_ps = path_print_system()
    if not empty(this.w_path_ps) and directory(this.w_path_ps)
      set default to (this.w_path_ps)
    else
      if DIRECTORY("C:\")
        set default to C:\
      else
        set default to (this.w_oldpath)
      endif
    endif
    this.w_var = putfile("Nome file:", this.w_OBJ.w_Txt_File, this.w_OBJ.cExtFormat)
    set default to (this.w_oldpath)
    if .not. empty(this.w_var)
      this.w_OBJ.w_Txt_File = this.w_var
      this.w_OBJ.prFile = this.w_var
    endif
  endproc


  procedure Page_25
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OBJ.w_Txt_Copies = iif(this.w_OBJ.w_Txt_Copies<=0,1,this.w_OBJ.w_Txt_Copies)
    this.w_OBJ.prCopie = this.w_OBJ.w_Txt_Copies
    * --- Zucchetti Aulla Inizio - Multireport
    this.w_OBJ.cNomeReport.SetAllNumCopy(this.w_OBJ.prCopie)     
    this.w_OBJ.oSplitReport.SetAllNumCopy(this.w_OBJ.prCopie)     
    * --- Zucchetti Aulla Fine - Multireport
  endproc


  procedure Page_26
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OBJ.cLanguage = RTRIM(STRTRAN(this.w_OBJ.oOpt_Language.Displayvalue,"<<",""))
    this.w_OBJ.w_Opt_Language = RTRIM(STRTRAN(this.w_OBJ.oOpt_Language.Displayvalue,"<<",""))
    * --- Traduco il multireprot
    if !this.w_OBJ.oSplitReport.IsEmpty()
      if this.w_OBJ.pdProcesso AND this.w_OBJ.pdISOLang AND this.w_OBJ.oOpt_Language.ListIndex=1
        this.w_OBJ.oSplitReport.TranslateReport("@@@", this.w_OBJ.cSessionCode)     
      else
        this.w_OBJ.oSplitReport.TranslateReport(this.w_OBJ.cLanguage, this.w_OBJ.cSessionCode)     
      endif
      this.w_OBJ.cNomeReport.CopyTranslateReport(this.w_OBJ.oSplitReport)     
      * --- al cambio lingua, se siamo dentro un processo documentale, risetta l'abilitazione dei bottoni
      if this.w_OBJ.pdProcesso AND this.w_OBJ.pdISOLang 
        this.w_OBJ.mEnableControls()     
        this.w_OBJ.mHideControls()     
      endif
    endif
  endproc


  procedure Page_27
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Imposta bitmap e titolo bottone CPZ/WE
    this.w_OBJ.oOpt_File = this.w_OBJ.GETCTRL ("w_Opt_File")
    this.w_OBJ.oOpt_Language = this.w_OBJ.GETCTRL ("w_Opt_Language")
    this.w_OBJ.oParentobject = oParentobject
    this.w_OBJ.oTxt_File = this.w_OBJ.GETCTRL ("w_Txt_File")
    * --- DA MODIFICARE OBTN_1_12 ALTRIMENTI AL CAMBIO SEQUENE DA ERRORE
    this.w_OBJ.oPDLbl4 = this.w_OBJ.GETCTRL (AH_MSGFORMAT(MSG_WORKING_PROGRESS))
    this.w_OBJ.oTxt_Copies = this.w_OBJ.GETCTRL ("w_Txt_Copies")
    this.w_OBJ.oOpt_File = this.w_OBJ.GETCTRL ("w_Opt_File")
    this.w_OBJ.oTxt_Device = this.w_OBJ.GETCTRL ("w_DEVICE")
    this.w_OBJ.cSessionCode = SYS(2015)
    * --- * --- Zucchetti Aulla Inizio
    *       * --- Disabilito il Resize della maschera
    *       * --- Bordi insensibili
    this.w_OBJ.BorderStyle = 2
    * --- Bottone allarga / Restore disabilitato
    this.w_OBJ.Maxbutton = .F.
    * --- * Carico i formati
    *       *CfgCBStmp(this,'CFG', 'oOpt_File')
    *     w_OBJ.oPgFrm.Zorder(1)  
    if (TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S")
      this.w_OBJ.prTyExp = "5"
      this.w_OBJ.w_Opt_File = "5"
      this.w_OBJ.cExtFormat = "5"
      * --- Zucchetti Aulla-Schedulatore Fine
    else
      this.w_OBJ.prTyExp = "1"
      this.w_OBJ.w_Opt_File = "TXT" 
      this.w_OBJ.cExtFormat = "TXT" 
      this.w_OBJ.oOpt_File.Click()     
    endif
    this.w_OBJ.prFile = iif (TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S" , FORCEEXT(this.w_OBJ.prFile, "PDF") , path_print_system()+"DEFA"+right("0000"+alltrim(str(i_codute,4,0)),4)+".TXT" )
    this.w_OBJ.w_Txt_File = this.w_OBJ.prFile
    this.w_OBJ.mcalc(.T.)     
  endproc


  procedure Page_28
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Zucchetti aulla Inizio - Sezione lingua (zzzz)
    this.w_SelOpt = CfgCBStmp(this.w_OBJ,IIF(this.w_OBJ.pdProcesso,"PDL","LAN"), "oOpt_Language")
    this.w_OBJ.w_Opt_Language = left (this.w_OBJ.oOpt_Language.DISPLAYVALUE, 3)
    * --- valore di default della combo
    if this.w_OBJ.pdProcesso AND this.w_OBJ.pdISOLang
      if this.w_OBJ.oOpt_Language.ListIndex<=1
        * --- imposta "lingua destinatario" disabilita la preview
        this.w_OBJ.oOpt_Language.ListIndex = 1
        * --- lingua destinatario, antiprima non disponibile (per ora)
        *     this.w_OBJ.oBtn_Ante.enabled=(w_SelOpt<>1) and this.w_OBJ.pdnTotDocOk>0 and not(this.w_OBJ.pdEXEProcesso)
      else
        * --- * disabilita esecuzione processo, la lingua deve essere "destinarario"
        *     *this.w_OBJ.oPDBtn_Proc.enabled=(w_SelOpt=0) and this.w_OBJ.pdnTotDocOk>0 and not(this.w_OBJ.pdEXEProcesso)
        *     
      endif
    else
      * --- * lingua del destinatario, utente, ecc
      *     *this.w_OBJ.oOpt_Language.Value = IIF(VARTYPE(g_DEMANDEDLANGUAGE)='C' and Not(EMPTY(g_DEMANDEDLANGUAGE)), g_DEMANDEDLANGUAGE, this.w_OBJ.oOpt_Language.Value)
      *     *--- Zucchetti Aulla Inizio - Correzione lingua
      this.w_OBJ.w_Opt_Language = IIF(VARTYPE(g_DEMANDEDLANGUAGE)="C" and Not(EMPTY(g_DEMANDEDLANGUAGE)) and ALLTRIM(g_DEMANDEDLANGUAGE)+","$ALLTRIM(this.w_OBJ.oOpt_Language.rowsource)+",", g_DEMANDEDLANGUAGE, IIF(Not(EMPTY(i_cLANGUAGE)) and ALLTRIM(i_cLANGUAGE)+","$ALLTRIM(this.w_OBJ.oOpt_Language.rowsource)+",", i_cLANGUAGE, g_PROJECTLANGUAGE))
      this.w_cGetExtReport = this.w_OBJ.oSplitReport.GetExtReport(1)
      this.w_OBJ.w_Opt_Language = IIF(TYPE("this.w_cGetExtReport")<>"C" or this.w_cGetExtReport="FDF", g_PROJECTLANGUAGE, this.w_OBJ.w_Opt_Language)
      this.w_OBJ.w_Opt_Language = IIF(VARTYPE(g_DEMANDEDLANGUAGE)="C" and Not(EMPTY(g_DEMANDEDLANGUAGE)) and ALLTRIM(g_DEMANDEDLANGUAGE)+","$ALLTRIM(this.w_OBJ.oOpt_Language.rowsource)+",", g_DEMANDEDLANGUAGE,IIF(Not(EMPTY(i_cLANGUAGE)) and ALLTRIM(i_cLANGUAGE)+","$ALLTRIM(this.w_OBJ.oOpt_Language.rowsource)+",", i_cLANGUAGE, g_PROJECTLANGUAGE))
      this.w_cGetExtReport = this.w_OBJ.oSplitReport.GetExtReport(1)
      this.w_OBJ.w_Opt_Language = IIF(TYPE("this.w_cGetExtReport")<>"C" or this.w_cGetExtReport="FDF", g_PROJECTLANGUAGE, this.w_OBJ.w_Opt_Language)
      this.w_OBJ.oOpt_Language.DISPLAYVALUE = this.w_OBJ.w_Opt_Language
      * --- Zucchetti Aulla Fine
    endif
    this.w_OBJ.cLanguage = RTRIM(STRTRAN(this.w_OBJ.oOpt_Language.Displayvalue,"<<",""))
    if this.w_OBJ.oOpt_Language.ListIndex<1
      * --- setta la prima lingua della lista
      this.w_OBJ.oOpt_Language.ListIndex = 1
    endif
    * --- font
    if this.w_OBJ.pdProcesso AND this.w_SelOpt=1
      this.w_OBJ.oOpt_Language.fontsize = 8
      this.w_OBJ.oOpt_Language.fontitalic = true
    else
      this.w_OBJ.oOpt_Language.fontsize = 9
      this.w_OBJ.oOpt_Language.fontitalic = false
    endif
    if ! this.w_OBJ.oSplitReport.IsEmpty()
      if this.w_OBJ.pdProcesso
        if this.w_OBJ.pdISOLang AND this.w_OBJ.oOpt_Language.ListIndex=1
          this.w_OBJ.oSplitReport.TranslateReport("@@@", this.w_OBJ.cSessionCode)     
        else
          this.w_OBJ.oSplitReport.TranslateReport(this.w_OBJ.cLanguage, this.w_OBJ.cSessionCode)     
        endif
        this.w_OBJ.cNomeReport.CopyTranslateReport(this.w_OBJ.oSplitReport)     
      else
        this.w_OBJ.oSplitReport.TranslateReport(this.w_OBJ.cLanguage, this.w_OBJ.cSessionCode)     
        this.w_OBJ.cNomeReport.CopyTranslateReport(this.w_OBJ.oSplitReport)     
      endif
    endif
    this.w_OBJ.oOpt_Language.FontItalic = .T.
  endproc


  procedure Page_29
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Zucchetti Aulla Inizio
    *     Se report solo testo non � modificabile attraverso il disegnatore di report del Fox
    this.w_dbfname = ""
    this.w_dataname = ""
    this.w_tblname = ""
    this.w_count = 0
    if Upper(this.w_OBJ.cTipoReport)<>"S"
      * --- ***** Zucchetti Aulla Fine
      *     *--- Zucchetti Aulla inizio - Sostituito check tra amministratore e gestione framework
      *     * framework monitor
      *     * Controllo se si hanno i diritti
      *     *if cp_IsAdministrator()
      this.w_UsrRight = cp_set_get_right()
      if (cp_IsAdministrator() or isalt() or i_MonitorFramework) and this.w_UsrRight>1
        * --- Zucchetti Aulla Inizio - Multireport
        this.w_NumRow = Alen(this.w_OBJ.cNomeReport.aReportList, 1)
        if (cp_IsAdministrator() or isalt() or i_MonitorFramework) And this.w_NumRow>1 And !this.w_OBJ.cNomeReport.IsEmpty()
          if vartype ( this.w_OBJ.w_INDICE ) ="N"
            * --- Sulla print system nuova il report da modificare � il report selezionato a pag 2 della print sysyem
            this.w_NumRow = this.w_OBJ.w_INDICE
            this.w_nNumRep = this.w_OBJ.w_INDICE
          else
            this.w_SelForm = Createobject("SelForm")
            this.w_SelForm.Icon = i_curform.Icon
            this.w_count = 1
            do while this.w_Count<=this.w_NumRow
              if Not Empty(this.w_OBJ.cNomeReport.GetReport(this.w_Count))
                if Type("g_MRDuplicateReport")="L" And g_MRDuplicateReport
                  this.w_SelForm.RepList.AddItem(this.w_OBJ.cNomeReport.GetReport(this.w_Count))     
                else
                  this.w_bFound = .F.
                  this.w_i = 1
                  do while this.w_i <= this.w_SelForm.RepList.ListCount
                    if this.w_OBJ.cNomeReport.GetReport(this.w_Count) == this.w_SelForm.RepList.List[this.w_i]
                      this.w_bFound = .T.
                    endif
                    this.w_i = this.w_i + 1
                  enddo
                  if !bFound
                    this.w_SelForm.RepList.AddItem(this.w_OBJ.cNomeReport.GetReport(l_Count))     
                  endif
                endif
              endif
              this.w_count = this.w_count + 1
            enddo
            * --- Ordino in modo alfabetico
            if Type("g_MROrderReport")="L" And g_MROrderReport
              this.w_SelForm.RepList.Sorted = .T.
            endif
            this.w_SelForm.Show(1)     
            * --- Se esiste la propriet� la maschera non � stata ancora distrutta
            if Type("w_SelForm.nNumReport")="N" 
              this.w_nNumRep = this.w_SelForm.nNumReport
              this.w_SelForm.Release()     
            else
              i_retcode = 'stop'
              return
            endif
            this.w_SelForm = .Null.
          endif
        else
          this.w_nNumRep = 1
        endif
        * --- Zucchetti Aulla Fine - Multireport
        if !this.w_OBJ.cNomeReport.IsEmpty() And cp_IsStdFile(Iif(this.w_nNumRep>0, this.w_OBJ.cNomeReport.GetReport(this.w_nNumRep), ""),"FRX")
          this.w_cCurName = this.w_OBJ.cNomeReport.GetCursorName(this.w_nNumRep)
          Select( this.w_cCurName )
          this.w_d = Set("DATABASE")
          if Not(this.w_OBJ.bModifiedFrx)
            this.w_dataname = Getenv("TEMP")+"\"+Sys(2015)
            this.w_tblname = Sys(2015)
            this.w_dbfname = Getenv("TEMP")+"\"+this.w_tblname
            * --- crea una tabella temporanea in un database temporaneo per la gestione dei nomi lunghi
            Crea Database (this.w_dataname) 
 Copy To (this.w_dbfname) Database (this.w_dataname) 
 Use In Select(this.w_cCurName)
            if Used("__tmp__")
               Select * From __tmp__ Into Cursor __tmp__temporaneo__ 
 Use In Select("__tmp__")
            endif
            * --- usa la tabella appena creata (in \windows\temp) con alias __tmp__
            Select 0 
 Use (this.w_dbfname) Alias __tmp__
            this.w_OBJ.bModifiedFrx = .T.
          endif
          * --- Setto i_paramqry per permettere l'uso di cp_queryparam()
          this.w_paramqry = this.w_OBJ.cNomeReport.GetParamqry(this.w_nNumRep)
          * --- Creo l'environment con le variabili l_
          this.w_VarEnv = this.w_OBJ.cNomeReport.GetVarEnv(this.w_nNumRep)
          this.w_bVarEnv = .F.
          if Not Isnull(this.w_VarEnv) And Not this.w_VarEnv.IsEmpty()
            this.w_NumVar = this.w_VarEnv.GetNumVar()
            * --- Creo le variabili
            this.w_i = 1
            do while this.w_i <= this.w_NumVar
              l_VarName=this.w_VarEnv.GetVarName(this.w_i) 
 &l_VarName=this.w_VarEnv.GetVarValue(this.w_i)
              this.w_i = this.w_i + 1
            enddo
            this.w_bVarEnv = .T.
          endif
          Public g_CurCursor
          if USED("__tmp__")
            g_CurCursor="__tmp__"
          else
            g_CurCursor = this.w_cCurName
          endif
          * --- Monitor FrameWork
          this.w_cFilename = cp_GetStdfile(this.w_OBJ.cNomeReport.GetReport(this.w_nNumRep),"FRX")
          modifyreport (this,this.w_cFilename, this.w_UsrRight)
          * --- Rivalorizzo le variabili dell' environment in modo da poterle poi interrogare alla fine della stampa
          * --- Se � true ho creato le variabili quindi le risalvo all'interno dell'oggetto
          if this.w_bVarEnv
            this.w_i = 1
            do while this.w_i <= this.w_NumVar
              l_VarName=this.w_VarEnv.GetVarName(this.w_i)
              this.w_VarEnv.SetVarValue(&l_VarName,this.w_i)     
              this.w_i = this.w_i + 1
            enddo
          endif
          Release g_CurCursor
          if Not(Empty(this.w_d))
            * --- potrebbe non esserci un database aperto
            Set Database To (this.w_d)
          endif
        else
          cp_errormsg(CP_MSGFORMAT(MSG_FILE__NOT_FOUND_QM,this.w_OBJ.cNomeReport.GetReport(this.w_nNumRep)),48,"",.F.)
        endif
        * --- Zucchetti Aulla Inizio - FrameWorkMonitor
      else
        cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN )
        * --- Zucchetti Aulla Fine - FrameWorkMonitor
      endif
    else
      cp_errormsg(MSG_REPORT_CHANGEABLE_BY_TEXTREPORTKIT)
    endif
    * --- distrugge la tabella e il database temporanei
    if !Empty(this.w_dbfname)
      this.w_ENV = Getenv("TEMP")
      this.w_d = Set("DATABASE")
      Set Database To (this.w_dataname) 
 Select __tmp__ 
 Use 
 Close Database 
 Delete Database (this.w_dataname) 
 Select * From (this.w_dbfname) Into Cursor (this.w_cCurName) 
 Use In Select(this.w_tblname) 
 Delete File (this.w_dbfname+".dbf")
      * --- Se ho dei campi memo � stato creato anche .fpt
      if cp_fileexist(this.w_dbfname+".fpt",.T.)
        Delete File (this.w_dbfname+".fpt")
      endif
      if Used("__tmp__temporaneo__")
         Select * From __tmp__temporaneo__ Into Cursor __tmp__ 
 Use In Select("__tmp__temporaneo__")
      endif
    endif
    * --- Zucchetti Aulla Fine
  endproc


  procedure Page_30
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OBJ.pdProcesso
      if cp_PrintCondition (this.w_OBJ , "PDBtn_Proc_Edit") 
        g_MSG=g_MSG+" "+ cp_Translate(MSG_EXEC_DOCUMENT_PROCESS)+CHR(13)
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
         g_MSG=g_MSG+" "+ cp_Translate(MSG_CANNOT_EXEC_DOCUMENT_PROCESS)+CHR(13)
      endif
    else
      * --- Stampa Std
      g_MSG=g_MSG+" "+ cp_Translate(MSG_EXEC_STANDARD_REPORT)+CHR(13) 
 setvaluelinked("M", this.w_OBJ, "w_opt_File", "PDF")
      this.w_MIN = SUBSTR(TIME(),4,2)
      this.w_SEC = SUBSTR(TIME(),7,2)
      this.w_YEAR = SUBSTR(DTOC(i_DATSYS),7,4)
      this.w_MONTH = SUBSTR(DTOC(i_DATSYS),4,2)
      this.w_DAY = SUBSTR(DTOC(i_DATSYS),1,2)
      this.w_HOUR = SUBSTR(TIME(),1,2)
      if left(this.w_OBJ.prfile,2)="\\" or substr(this.w_OBJ.prfile,2,1)=":"
        this.w_OBJ.prfile = right(this.w_OBJ.prfile,len(this.w_OBJ.prfile)-rat("\",this.w_OBJ.prfile))
        this.w_OBJ.prfile = g_PATHSJ+LEFT(alltrim(this.w_OBJ.prfile),RAT(".",alltrim(this.w_OBJ.prfile),1)-1)+iif(empty(this.w_OBJ.prfile) or "DEFA"$this.w_OBJ.prfile,"_"+g_JBNOME,"")+"_"+ALLTRIM(g_JBCODAZI)+"_"+ALLTRIM(this.w_YEAR)+ALLTRIM(this.w_MONTH)+ALLTRIM(this.w_DAY)+ALLTRIM(this.w_HOUR)+ALLTRIM(this.w_MIN)+ALLTRIM(this.w_SEC)+"."+RIGHT(alltrim(this.w_OBJ.prfile),LEN(alltrim(this.w_OBJ.prfile))-RAT(".",alltrim(this.w_OBJ.prfile),1))
      else
        this.w_OBJ.prfile = g_PATHSJ+LEFT(alltrim(this.w_OBJ.prfile),RAT(".",alltrim(this.w_OBJ.prfile),1)-1)+iif(empty(this.w_OBJ.prfile) or "DEFA"$this.w_OBJ.prfile,"_"+g_JBNOME,"")+"_"+ALLTRIM(g_JBCODAZI)+"_"+ALLTRIM(this.w_YEAR)+ALLTRIM(this.w_MONTH)+ALLTRIM(this.w_DAY)+ALLTRIM(this.w_HOUR)+ALLTRIM(this.w_MIN)+ALLTRIM(this.w_SEC)+"."+RIGHT(alltrim(this.w_OBJ.prfile),LEN(alltrim(this.w_OBJ.prfile))-RAT(".",alltrim(this.w_OBJ.prfile),1))
      endif
      setvaluelinked("M", this.w_OBJ, "w_Txt_File", this.w_OBJ.prfile)
      this.Page_19()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if file (this.w_OBJ.prfile)
        this.w_N = ALEN(g_STAMPJOB)
         g_MSG=g_MSG+" "+ cp_Msgformat(MSG_FILE_CREATED_CL__,this.w_OBJ.prfile)+ CHR(13)
        g_STAMPJOB(this.w_N)=this.w_OBJ.prfile 
 DIMENSION g_STAMPJOB(this.w_N+1) 
 g_FLAGSTMP=.T.
      endif
      * --- Stampa cartacea
      if (g_FLGSTC="S")
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        g_MSG=g_MSG+" "+ cp_Translate(MSG_EXEC_PAPERY_REPORT)+CHR(13)
      endif
    endif
    * --- Chiudo il form
    this.Page_17()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_31
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il numero copie nello zoom a pagina 2
    this.w_CURSOR = this.w_OBJ.w_Zoom_Printer.ccursor
    this.w_NUMCOPIE = this.w_OBJ.w_Txt_Copies
    Update (this.w_cursor) set NUMCOPY = this.w_numcopie
  endproc


  procedure Page_32
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna stampante nello zoom a pagina 2
    this.w_CURSOR = this.w_OBJ.w_Zoom_Printer.ccursor
    this.w_OBJ.w_DEVICE = this.w_OBJ.oTxt_Device.Displayvalue
    this.w_DISPOSITIVO = this.w_OBJ.w_DEVICE
    this.w_OBJ.prStamp = this.w_OBJ.w_DEVICE
    Update (this.w_cursor) set PRINTER= this.w_dispositivo
    this.w_CURSOR = this.w_OBJ.w_Zoom_Printer.ccursor
    if TYPE ("this.w_OBJ.cNomeReport")="O"
      this.w_OBJ.cNomeReport.SetInfoZoom(this.w_CURSOR)     
    endif
    if TYPE ("this.w_OBJ.oOrigMReport")="O"
      this.w_OBJ.oOrigMReport.SetInfoZoom(this.w_CURSOR)     
    endif
    this.w_OBJ.oSplitReport.SetInfoFromParent(this.w_OBJ.cNomeReport)     
    GSUT_BRL(this.w_OBJ ,"S" ,this.w_OBJ.cNomeReport )
  endproc


  procedure Page_33
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_COUNT = 0
    this.w_i = 1
    do while this.w_i <= this.w_OBJ.w_zoom_printer.nFields
      if this.w_OBJ.w_zoom_printer.cFields[this.w_i]=="EXCLWRD"
        this.w_COUNT = this.w_i
        exit
      endif
      this.w_i = this.w_i + 1
    enddo
    if this.w_count>0
      l_macro=this.w_OBJ.w_zoom_printer.grd.columns[this.w_count].dynamicforecolor
      this.w_OBJ.w_zoom_printer.grd.columns[this.w_count].text1.forecolor = &l_macro
      this.w_OBJ.w_zoom_printer.grd.columns[this.w_count].text1.SelectedForeColor = &l_macro
      l_macro=this.w_OBJ.w_zoom_printer.grd.columns[this.w_count].dynamicbackcolor
      this.w_OBJ.w_zoom_printer.grd.columns[this.w_count].text1.backcolor = &l_macro
      this.w_OBJ.w_zoom_printer.grd.columns[this.w_count].text1.SelectedBackColor = &l_macro
    endif
  endproc


  procedure Page_34
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OBJ.cNomeReport.GetNumReport()>1
      this.w_OBJ.nIndexReport = GSUT_BRL(this, "C", this.w_OBJ.cNomeReport, this.w_cType)
    else
      this.w_OBJ.nIndexReport = 1
    endif
  endproc


  procedure Page_35
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private i_oErr,i_sok 
 i_sok=.t. 
 i_oErr=on("ERROR") 
 on error i_sok=.f.
    if not(empty(this.w_OBJ.prStamp))
      set printer to name (alltrim (this.w_OBJ.prStamp))
    else
      set printer to default
    endif
    if i_sok
      do RunRep with this.w_OBJ ,this.pMultiReportSplit , "S" , this.w_OBJ.prFile , this.w_OBJ.prCopie
    endif
    * --- Ripristina stampante predefinita
    set printer to default
    * --- Ripristina on-error
    if EMPTY(i_oErr)
      on error
    else
      on error &i_oErr
    endif
    * --- Risultato
    this.w_ret = i_sok
    i_retcode = 'stop'
    i_retval = i_sok
    return
  endproc


  procedure Page_36
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OBJ.nIndexReport>0
      this.w_CurName = this.w_OBJ.cNomeReport.GetCursorName(this.w_OBJ.w_INDICE)
      SELECT(this.w_CurName) 
 go top 
 browse
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPar,pMultiReportSplit)
    this.pPar=pPar
    this.pMultiReportSplit=pMultiReportSplit
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STMPFILE'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_STMPFILE')
      use in _Curs_STMPFILE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar,pMultiReportSplit"
endproc
