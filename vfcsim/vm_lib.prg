* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VM_LIB
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Libreria per le maschere
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

#Define VFM_HGRD 20	&&Spessore sfumatura immagine

Define Class draw_LineNode As draw_Shape
    Width=8
    Height=8
    Curvature=99
    cNext=""		&&Prossimo nodo
    cRoot=""		&&Nodo padre da cui parte il connettore
    cMBtn=""		&&Media button
    cMBtnAnchor=""	&&Ancoraggio LRTB
    cLine=""		&&Oggetto linea
    nOx=0
    nOy=0
    bModified=.F.

    nStartCap = 0	&&1:Arrow, 2:Hollow Arrow, 3:Open Arrow, 0:None
    nEndCap = 0
    nDashStyle = 0	&&0:Solid, 1:Dashed, 2:Dotted
    nCurvature = 5
    nBorderColor = Rgb(0,0,255)
    nBorderWidth = 2
    nSCAngle = 1  &&Start Cap Angle
    nSCSize = 1   &&Start Cap Size
    nECAngle = 1  &&End Cap Angle
    nECSize = 1   &&End Cap Size

    BackColor=Rgb(0,0,255)
    BackStyle=1
    *Visible=.F.

    Proc RemoveShape()
    Endproc

    Proc SetSize(w,h)
    Endproc

    Proc Serialize(xcg)
        If xcg.bIsStoring
            xcg.Save('N',This.Left)
            xcg.Save('N',This.Top)
            xcg.Save('N',This.Width)
            xcg.Save('N',This.Height)
            xcg.Save('C',This.Name)
            xcg.Eoc()
            xcg.Save('C',This.cNext)
            xcg.Save('C',This.cRoot)
            xcg.Save('C',This.cMBtn)
            xcg.Save('C',This.cMBtnAnchor)
            xcg.Save('N',This.nBorderColor)
            xcg.Save('N',This.nBorderWidth)
            xcg.Save('N',This.nStartCap)
            xcg.Save('N',This.nEndCap)
            xcg.Save('N',This.nDashStyle)
            xcg.Save('N',This.nCurvature)
            xcg.Save('N',This.nSCAngle)
            xcg.Save('N',This.nSCSize)
            xcg.Save('N',This.nECAngle)
            xcg.Save('N',This.nECSize)
            xcg.Eoc()
        ELSE
        	*--- Zucchetti Aulla inizio
*!*	            This.Left=xcg.Load('N',0)
*!*	            This.Top=xcg.Load('N',0)
*!*	            This.Width=xcg.Load('N',0)
*!*	            This.Height=xcg.Load('N',0) Sostituite con la Move perch� in questo modo viene richiamato il metodo Resize una sola volta
			This.Move(xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0), xcg.Load('N',0))
			*--- Zucchetti Aulla fine
            This.Name=xcg.Load('C',This.Name)
            xcg.Eoc()
            This.cNext=xcg.Load('C',"")
            This.cRoot=xcg.Load('C',"")
            This.cMBtn=xcg.Load('C',"")
            This.cMBtnAnchor=xcg.Load('C',"")
            This.nBorderColor = xcg.Load('N',Rgb(0,0,255))
            This.nBorderWidth = xcg.Load('N',2)
            This.nStartCap = xcg.Load('N',0)
            This.nEndCap = xcg.Load('N',0)
            This.nDashStyle = xcg.Load('N',0)
            This.nCurvature = xcg.Load('N',5)
            IF Not xcg.Eoc(.T.)
                This.nSCAngle = xcg.Load('N',32)
                This.nSCSize = xcg.Load('N',15)
                This.nECAngle = xcg.Load('N',32)
                This.nECSize = xcg.Load('N',15)
                xcg.Eoc()
            ENDIF
        Endif
    Endproc

    Procedure AddNode(x, Y)
        Local cNewNode, cRoot, cLine
        With This
            *--- Nuovo nodo
            cNewNode = Sys(2015)
            .Parent.AddObject(m.cNewNode, "draw_LineNode")
            .Parent.&cNewNode..cRoot = This.cRoot
            .Parent.&cNewNode..cNext = This.cNext
            .Parent.&cNewNode..Visible=.T.
            .cNext=cNewNode
            .Parent.&cNewNode..Left=m.x-.Parent.&cNewNode..Width/2
            .Parent.&cNewNode..Top=m.y-.Parent.&cNewNode..Height/2
            *--- Aggiungo la linea
            cx = .Left + .Width/2
            cy = .Top + .Height/2
            cLine = Sys(2015)
            .Parent.&cNewNode..cLine = cLine
            .Parent.AddObject(m.cLine, "draw_Line", x, Y, cx, cy, m.cNewNode)
            *--- Seleziono il nodo appena aggiunto e ridisegno le linee
            .Parent.DeSelectAll()
            .Parent.&cNewNode..Select()
        Endwith
    Endproc

    Procedure RemoveNode()
        Local cNext, cPrev, l_oldErr
        LOCAL TestMacro
        l_oldErr = On("Error")
        On Error l_err=.T.
        With This
            If Empty(.cMBtn)
                *--- Ricavo padre e figlio del nodo che sto cancellando
                cNext = .cNext
                cPrev = .cRoot
                bTest = !Empty(.Parent.&cPrev..cNext) And .Parent.&cPrev..cNext <> .Name
                Do While bTest
                    cPrev = .Parent.&cPrev..cNext
                    bTest = !Empty(.Parent.&cPrev..cNext) And .Parent.&cPrev..cNext <> .Name
                ENDDO
                TestMacro=!Empty(.Parent.&cNext..cMBtn) And !Empty(.Parent.&cPrev..cMBtn)
                If TestMacro
                    *--- Rimuovo il connettore, � l'ultimo nodo
                    .Parent.RemoveObject(.cLine)
                    .Parent.RemoveObject(.Parent.&cPrev..cLine)
                    .Parent.RemoveObject(cNext)
                    .Parent.RemoveObject(cPrev)
                Else
                    .Parent.RemoveObject(.cLine)
                    .Parent.&cPrev..cNext = .cNext
                    .Parent.&cPrev..RedrawLines()
                Endif
            Endif
        Endwith
        On Error &l_oldErr
    Endproc

    Procedure Destroy()
        This.RemoveNode()
    Endproc

    Procedure RemoveAll()
        Local cRoot, cNext
        cRoot = This.cRoot
        cNext = This.Parent.&cRoot..cNext
        Do While Type("This.Parent."+cNext)='O'
            This.Parent.RemoveObject(cNext)
            If Type("This.Parent."+cRoot)='O'
                cNext = This.Parent.&cRoot..cNext
            Endif
        Enddo
    Endproc

    Function CheckLine(x,Y)
        Local offSet, mc, delta, N
        offSet=3
        With This
            If !Empty(.cNext)
                cx = .Left + .Width/2
                cy = .Top + .Height/2
                N = .cNext
                x1 = .Parent.&N..Left+.Parent.&N..Width/2
                y1 = .Parent.&N..Top+.Parent.&N..Height/2
                mc = (m.cy-m.y1)/(m.cx-m.x1)
                delta = Abs(-mc*m.x+m.y-m.y1+mc*m.x1)/Sqrt(mc*mc+1)
                If m.delta<m.offSet
                    Return .T.
                Endif
            Endif
        Endwith
        Return .F.
    Endfunc

    Proc MoveDelta(dx,dy)
        This.Top=This.Top+dy
        This.Left=This.Left+dx
        This.bModified=.T.
        N = This.cRoot
        This.Parent.&N..RedrawLines(.T.)
        Return
    Proc MoveTo(nx,ny)
        This.Left=nx-4
        This.Top=ny-4
        This.bModified=.T.
        N = This.cRoot
        This.Parent.&N..RedrawLines(.T.)

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This
            If m.nButton=1 And .bSelected
                Local N, dx, dy
                dx=m.nXCoord-.nOx
                dy=m.nYCoord-.nOy
                .Left=.Left+m.dx
                .Top=.Top+m.dy
                .bModified=.T.
                N = .cRoot
                .Parent.&N..RedrawLines(.T.)
            Endif
            *--- Eventuale linea
  	        This.nOx=m.nXCoord
            This.nOy=m.nYCoord
        Endwith

    Func InRect(x1,y1,x2,y2)
        Local res
        res=.F.
        If This.Top>=y1 And This.Top<=y2 And This.Left>=x1 And This.Left<=x2
            res=.T.
        Endif
        If This.Top+This.Height>=y1 And This.Top+This.Height<=y2 And This.Left+This.Width>=x1 And This.Left+This.Width<=x2
            res=.T.
        Endif
        Return(res)
    Endfunc

	PROCEDURE MouseUp
		Lparameters nButton, nShift, nXCoord, nYCoord
		LOCAL N,LeftMod ,TopMod 
		With This
			If m.nButton=1 And .bSelected
				LeftMod = MOD(.Left,.Parent.nGridHStep)
				.Left=.Left-LeftMod + ROUND(LeftMod /.Parent.nGridHStep,0)*.Parent.nGridHStep
				TopMod = MOD(.Top,.Parent.nGridHStep)
				.Top=.Top-TopMod + ROUND(TopMod /.Parent.nGridHStep,0)*.Parent.nGridHStep				
	            N = .cRoot
	            .Parent.&N..RedrawLines(.T.)			
	        Endif
		ENDWITH
		
    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If m.nButton=1
            If nShift<>1
                This.Parent.DeSelectAll()
            Endif
            This.Select()
  	        This.nOx=m.nXCoord
            This.nOy=m.nYCoord 
        Endif

    Procedure AddLine()
        Local cx, cy, x1, y1
        With This
            If !Empty(.cNext)
                cx = .Left + .Width/2
                cy = .Top + .Height/2
                .cLine = Sys(2015)
                N = .cNext
                x1 = .Parent.&N..Left+.Parent.&N..Width/2
                y1 = .Parent.&N..Top+.Parent.&N..Height/2
                .Parent.AddObject(.cLine, "draw_Line", m.x1, m.y1, cx, cy, .Name)
                *--- Ciclo al prossimo
                .Parent.&N..AddLine()
            Endif
        Endwith
    Endproc

    Procedure RemoveLines()
        If !Empty(This.cLine)
            This.Parent.RemoveObject(This.cLine)
            This.cLine=""
        Endif
    Endproc

    Procedure RedrawLines(bSelect)
        With This
            Local cx,cy,N,L,x1,y1
            If !Empty(.cNext)
                cx = .Left + .Width/2
                cy = .Top + .Height/2
                N = .cNext
                x1 = .Parent.&N..Left+.Parent.&N..Width/2
                y1 = .Parent.&N..Top+.Parent.&N..Height/2
                L = .cLine
                .Parent.&L.._Init(m.x1,m.y1,m.cx,m.cy)
                .Parent.&L..BorderWidth=Iif(m.bSelect, 2, 1)
                .Parent.&N..RedrawLines(m.bSelect)
            Endif
        Endwith

    Procedure bSelected_Assign(xValue)
        This.bSelected=m.xValue
        This.BackColor=Iif(m.xValue,Rgb(255,0,0),Rgb(0,0,255))
    Endproc

    Proc Select()
        With This
            If !.bSelected And Empty(.cMBtn)
                .bSelected=.T.
                *.BackColor=Rgb(255,0,0)
                Local N
                N = .cRoot
                If !Empty(m.N)
                    .Parent.&N..RedrawLines(.T.)
                Endif
            Endif
        Endwith

    Proc DeSelect()
        With This
            If .bSelected And Empty(.cMBtn)
                .bSelected=.F.
                *.BackColor=Rgb(0,0,255)
                Local N
                N = .cRoot
                If !Empty(m.N)
                    .Parent.&N..RedrawLines()
                Endif
            Endif
        Endwith

    Procedure MoveShapeDelta(dx,dy)
    Endproc

    Procedure DblClick()
*!*	        Local o, cRoot
*!*	        cRoot=This.cRoot
*!*	        o=Createobject('painter_node_form',This.Parent.&cRoot)
*!*	        o.Show()
            DIMENSION aParam[2]
            aParam[1]= this
            aParam[2] = 6
			Local l_batch
			l_batch = "cp_objmaskoptions(@aParam)"
			&l_batch
    Endproc

Enddefine

Define Class draw_Line As Line
    bSelected=.F.
    bIsLink=.F.
    Enabled=.T.
    Visible=.T.
    cNode=""
    mX=0
    mY=0

    Procedure Init(x1,y1,x2,y2, cNode)
        This._Init(m.x1,m.y1,m.x2,m.y2)
        This.cNode = m.cNode
    Endproc

    Func InRect(x1,y1,x2,y2)
        Return .F.

    Procedure DblClick()
        Local N, bPrev
        LOCAL TestMacro
        Amouseobj(aPos)
        N = This.cNode
        TestMacro=This.Parent.&N..CheckLine(aPos[3], aPos[4])
        If TestMacro
            This.Parent.&N..AddNode(aPos[3], aPos[4])
        Endif
    Endproc

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        Amouseobj(aPos,1)
        This.Parent.MouseMove(nButton, nShift, aPos[3], aPos[4])

    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        Local N
        LOCAL TestMacro
        N = This.cNode
        Amouseobj(aPos,1)
        If nButton=1
        TestMacro=!This.Parent.&N..CheckLine(aPos[3], aPos[4])
            If TestMacro
                This.Parent.MouseDown(nButton, nShift, aPos[3], aPos[4])
            Else
                If nShift<>1
                    This.Parent.DeSelectAll()
                ENDIF
                TestMacro=This.Parent.&N..cRoot = This.Parent.&N..Name
                If TestMacro
                    N=This.Parent.&N..cNext
                Endif
                This.Parent.&N..Select()
            Endif
        Else
            This.Parent.MouseDown(nButton, nShift, aPos[3], aPos[4])
        Endif

    Procedure MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        Local N
        N = This.cNode
        Amouseobj(aPos,1)
        This.Parent.MouseUp(nButton, nShift, aPos[3], aPos[4])

    Procedure _Init(x1,y1,x2,y2)
        Local sy,sx,dx,dy
        sy=y1
        sx=x1
        dy=y2-y1
        dx=x2-x1
        This.LineSlant=Iif(dx*dy<0,'/','\')
        If dx<0
            sx=sx+dx
            dx=-dx
        Endif
        If dy<0
            sy=sy+dy
            dy=-dy
        Endif
		*--- Zucchetti Aulla inizio
*!*	        This.Top=sy
*!*	        This.Left=sx
*!*	        This.Width=dx
*!*	        This.Height=dy Sostituite con la Move perch� in questo modo viene richiamato il metodo Resize una sola volta
		This.Move(sx, sy, dx, dy)
		*--- Zucchetti Aulla fine
        This.ZOrder(1)
    Endproc
Enddefine

Define Class draw_handlerLink As Shape
    Width=8
    Height=8
    cObj=''
    cType=''
    bSelected=.F.
    bIsLink=.F.
    nOx=0
    nOy=0
    cObjOver = ""
    nStartLeft=0
    nStartTop=0
    cNewHandler=""

    BackColor=Rgb(255,0,0)
    Proc MoveDelta(dx,dy)
        This.Top=This.Top+dy
        This.Left=This.Left+dx
        Return
    Proc MoveTo(nx,ny)
        This.Left=nx-4
        This.Top=ny-4

    Func InRect(x1,y1,x2,y2)
        Local res
        res=.F.
        If This.Top>=y1 And This.Top<=y2 And This.Left>=x1 And This.Left<=x2
            res=.T.
        Endif
        If This.Top+This.Height>=y1 And This.Top+This.Height<=y2 And This.Left+This.Width>=x1 And This.Left+This.Width<=x2
            res=.T.
        Endif
        Return(res)
    Endfunc

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        LOCAL TestMacro
        If m.nButton=1 And This.bSelected
            Local oObj,N,dx,dy
            dx=m.nXCoord-This.nOx
            dy=m.nYCoord-This.nOy
            This.Left=This.Left+m.dx
            This.Top=This.Top+m.dy
            *--- Mostra altri draw_handlerlink
            Local x1,y1,x2,y2
            N = This.cObj
            For Each oObj In This.Parent.Controls
            TestMacro=Alltrim(Lower(m.oObj.Class))=="painter_mediabtn" And Lower(m.oObj.Name)<>Lower(This.Parent.&N..Name)
                If TestMacro
                    x1=m.oObj.Left
                    y1=m.oObj.Top
                    x2=m.x1+m.oObj.Width
                    y2=m.y1+m.oObj.Height
                    If !m.oObj.bHandlers And This.InRect(m.x1, m.y1, m.x2, m.y2)
                        m.oObj.AddHandlersLink()
                        This.cObjOver=m.oObj.Name
                    Else
                        If This.cObjOver=m.oObj.Name
                            m.oObj.RemoveHandlersLink()
                        Endif
                    Endif
                Endif
            Endfor
        Endif
        *--- Eventuale linea
        This.nOx=nXCoord
        This.nOy=nYCoord

    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        Local N
        N=This.cObj
        This.bSelected = .T.
        This.ZOrder()
        Do Case
            Case This.cType="L"
                This.Parent.&N..cHandlerLinkL= This.Parent.AddHandlerLink(This.Left+4, This.Top+4, This.Parent.&N, "L")
                This.cNewHandler=This.Parent.&N..cHandlerLinkL
            Case This.cType="T"
                This.Parent.&N..cHandlerLinkT= This.Parent.AddHandlerLink(This.Left+4, This.Top+4, This.Parent.&N, "T")
                This.cNewHandler=This.Parent.&N..cHandlerLinkT
            Case This.cType="R"
                This.Parent.&N..cHandlerLinkR= This.Parent.AddHandlerLink(This.Left+4, This.Top+4, This.Parent.&N, "R")
                This.cNewHandler=This.Parent.&N..cHandlerLinkR
            Case This.cType="B"
                This.Parent.&N..cHandlerLinkB= This.Parent.AddHandlerLink(This.Left+4, This.Top+4, This.Parent.&N, "B")
                This.cNewHandler=This.Parent.&N..cHandlerLinkB
        Endcase
        This.nStartLeft=This.Left
        This.nStartTop=This.Top

    Procedure MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        *--- Test
        Local x1,y1,x2,y2, obj, cNodeRoot, cNode, cNodeLeaf, N
        LOCAL TestMacro
        x1=This.Left
        y1=This.Top
        x2=x1+This.Width
        y2=y1+This.Height
        For Each obj In This.Parent.Controls
            If Lower(obj.Class)=="draw_handlerlink" And m.obj.InRect(m.x1, m.y1, m.x2, m.y2) And Lower(m.obj.Name)<>Lower(This.Name) And Lower(m.obj.Name)<>Lower(This.cNewHandler)
                *--- Add LineConnector
                cNodeRoot = Sys(2015)
                This.Parent.AddObject(cNodeRoot, "draw_LineNode")
                This.Parent.&cNodeRoot..cRoot = cNodeRoot
                This.Parent.&cNodeRoot..cMBtn = This.cObj
                This.Parent.&cNodeRoot..cMBtnAnchor = This.cType
                This.Parent.&cNodeRoot..Left=This.nStartLeft
                This.Parent.&cNodeRoot..Top=This.nStartTop
                cNode = Sys(2015)
                This.Parent.AddObject(cNode, "draw_LineNode")
                This.Parent.&cNode..cRoot = cNodeRoot
                This.Parent.&cNodeRoot..cNext = cNode
                cNodeLeaf = Sys(2015)
                This.Parent.AddObject(cNodeLeaf, "draw_LineNode")
                This.Parent.&cNodeLeaf..cRoot = cNodeRoot
                This.Parent.&cNodeLeaf..cMBtn = m.obj.cObj
                This.Parent.&cNodeLeaf..cMBtnAnchor = m.obj.cType
                This.Parent.&cNodeLeaf..Left=m.obj.Left
                This.Parent.&cNodeLeaf..Top=m.obj.Top
                This.Parent.&cNode..cNext = cNodeLeaf
                This.Parent.&cNode..Left=(This.Parent.&cNodeRoot..Left+This.Parent.&cNodeLeaf..Left)/2
                This.Parent.&cNode..Top=(This.Parent.&cNodeRoot..Top+This.Parent.&cNodeLeaf..Top)/2
                This.Parent.&cNodeRoot..Visible=.T.
                This.Parent.&cNode..Visible=.T.
                This.Parent.&cNodeLeaf..Visible=.T.
                This.Parent.&cNodeRoot..AddLine()
                IF TYPE('this.Parent.Parent.ccnffile')<>'U'
                    i_cCnfFile=this.Parent.Parent.ccnffile
                    IF !EMPTY(i_cCnfFile) AND cp_fileexist(i_cCnfFile)
                       LOCAL i_prop, i_val, i_class
                       IF USED(JUSTSTEM(i_cCnfFile))
                           USE IN select (i_cCnfFile)
                       ENDIF 
                       i_cCnfFile=JUSTSTEM(i_cCnfFile) 
                       SELECT prop,val FROM (this.Parent.Parent.ccnffile) WHERE obj="node" INTO CURSOR SaveConf
                       SELECT SaveConf
                       GO top
                       DO WHILE NOT EOF()
                           i_prop=alltrim(prop)
                           i_val=IIF(TYPE('This.parent.&cNodeRoot..&i_prop')='N',VAL(ALLTRIM(val)),IIF(TYPE('This.parent.&cNodeRoot..&i_prop')='L',IIF(ALLTRIM(val)='.T.',.T.,.F.),ALLTRIM(val)))
                           This.parent.&cNodeRoot..&i_prop=i_val
                           skip
                       ENDDO
                       USE IN SELECT (i_cCnfFile) 
                       USE IN SELECT("SaveConf")
                    ENDIF
                ENDIF                 
                This.Parent.&cNodeRoot..Select()

                *--- Rimuovo handler
                N = m.obj.cObj
                TestMacro=!This.Parent.&N..bHandlers
                If TestMacro
                    This.Parent.&N..RemoveHandlersLink()
                Endif
                Exit
            Endif
        Endfor
        N = This.cObjOver
        If !Empty(m.N)
        TestMacro=!This.Parent.&N..bHandlers
            If TestMacro
                This.Parent.&N..RemoveHandlersLink()
            Endif
        Endif
        This.Parent.RemoveObject(This.Name)
Enddefine

* --- Classe per Label ------------------------------------------------->

Define Class painter_lbl As draw_lbl
    cVarName=MSG_STRING
    cAlign=''

    FontName = "Arial"
    FontSize = 9
    FontColor = Rgb(76,76,76)
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    GlbFont = .F.

    Proc DblClick()
*!*	        Local o
*!*	        o=Createobject('painter_lbl_form',This)
*!*	        o.Show()
        DIMENSION aParam[2]
        aParam[1]= this
        aParam[2] = 1
		Local l_batch
		l_batch = "cp_objmaskoptions(@aParam)"
		&l_batch
*!*	        This.obj.Caption=This.cVarName
*!*	        This.obj.Alignment=Iif(This.cAlign='R',1,Iif(This.cAlign='C',2,0))
*!*	        This.obj.FontName = This.FontName
*!*	        This.obj.FontSize = This.FontSize
*!*	        This.obj.ForeColor = This.FontColor
*!*	        This.obj.FontBold = This.FontBold
*!*	        This.obj.FontItalic = This.FontItalic
*!*	        This.obj.FontUnderline = This.FontUnderline
*!*	        This.obj.FontStrikethru = This.FontStrikeout
	EndProc

    Proc Serialize(xcg)
        DoDefault(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cVarName)
            xcg.Save('C',This.cAlign)
            *--- Font
            xcg.Save('C',This.FontName)
            xcg.Save('N',This.FontSize)
            xcg.Save('N',This.FontColor)
            xcg.Save('L',This.FontBold)
            xcg.Save('L',This.FontItalic)
            xcg.Save('L',This.FontUnderline)
            xcg.Save('L',This.FontStrikeout)
            xcg.Save('L',This.GlbFont)
            xcg.Eoc()
        Else
            This.cVarName=xcg.Load('C',cp_Translate(MSG_NO_NAME))
            This.cAlign=xcg.Load('C','')
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.FontColor = xcg.Load('N',0)
                This.FontBold= xcg.Load('L',.F.)
                This.FontItalic= xcg.Load('L',.F.)
                This.FontUnderline= xcg.Load('L',.F.)
                This.FontStrikeout= xcg.Load('L',.F.)
                This.GlbFont= xcg.Load('L',.F.)
                This.obj.FontName = This.FontName
                This.obj.FontSize = This.FontSize
                This.obj.ForeColor = This.FontColor
                This.obj.FontBold = This.FontBold
                This.obj.FontItalic = This.FontItalic
                This.obj.FontUnderline = This.FontUnderline
                This.obj.FontStrikethru = This.FontStrikeout
                xcg.Eoc()
            Endif
            This.obj.Caption=This.cVarName
            This.obj.Alignment=Iif(This.cAlign='R',1,Iif(This.cAlign='C',2,0))
        Endif
    Procedure Init()
        This.cVarName=cp_Translate(MSG_STRING)
        DoDefault()
    Endproc
Enddefine

* --- Classe per TextBox------------------------------------------------->

Define Class painter_txt As draw_txt
    cVarName=''
    cType='C'
    nLen=10
    nDec=0
    cPict=''
    cStato='Edit'
    cZFill='N'
    cArch=''
    cK1=''
    cK2=''
    cK3=''
    cKa=''
    cVk1=''
    cVk2=''
    cVk3=''
    cRf1=''
    cRv1=''

    FontName = "Arial"
    FontSize = 9
    FontColor = Rgb(76,76,76)
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    GlbFont = .F.

    Proc DblClick()
*!*	        Local o
*!*	        o=Createobject('painter_txt_form',This)
*!*	        o.Show()
            DIMENSION aParam[2]
            aParam[1]= this
            aParam[2] = 2
			Local l_batch
			l_batch = "cp_objmaskoptions(@aParam)"
			&l_batch
*!*	            This.obj.Value=This.cVarName

            
    Proc Serialize(xcg)
        DoDefault(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cVarName)
            xcg.Save('C',This.cType)
            xcg.Save('N',This.nLen)
            xcg.Save('N',This.nDec)
            xcg.Save('C',This.cPict)
            xcg.Save('C',This.cStato)
            xcg.Save('C',This.cZFill)
            xcg.Save('C',This.cArch)
            xcg.Save('C',This.cK1)
            xcg.Save('C',This.cK2)
            xcg.Save('C',This.cK3)
            xcg.Save('C',This.cKa)
            xcg.Save('C',This.cVk1)
            xcg.Save('C',This.cVk2)
            xcg.Save('C',This.cVk3)
            xcg.Save('C',This.cRf1)
            xcg.Save('C',This.cRv1)
            *--- Font
            xcg.Save('C',This.FontName)
            xcg.Save('N',This.FontSize)
            xcg.Save('N',This.FontColor)
            xcg.Save('L',This.FontBold)
            xcg.Save('L',This.FontItalic)
            xcg.Save('L',This.FontUnderline)
            xcg.Save('L',This.FontStrikeout)
            xcg.Save('L',This.GlbFont)
            xcg.Eoc()
        Else
            This.cVarName=xcg.Load('C',cp_Translate(MSG_NO_NAME))
            This.cType=xcg.Load('C',cp_Translate(MSG_NUMERIC))
            This.nLen=xcg.Load('N',0)
            This.nDec=xcg.Load('N',0)
            This.cPict=xcg.Load('C','')
            This.cStato=xcg.Load('C','Edit')
            This.cZFill=xcg.Load('C','N')
            This.cArch=xcg.Load('C','')
            This.cK1=xcg.Load('C','')
            This.cK2=xcg.Load('C','')
            This.cK3=xcg.Load('C','')
            This.cKa=xcg.Load('C','')
            This.cVk1=xcg.Load('C','')
            This.cVk2=xcg.Load('C','')
            This.cVk3=xcg.Load('C','')
            This.cRf1=xcg.Load('C','')
            This.cRv1=xcg.Load('C','')
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.FontColor = xcg.Load('N',0)
                This.FontBold= xcg.Load('L',.F.)
                This.FontItalic= xcg.Load('L',.F.)
                This.FontUnderline= xcg.Load('L',.F.)
                This.FontStrikeout= xcg.Load('L',.F.)
                This.GlbFont= xcg.Load('L',.F.)
                xcg.Eoc()
            Endif
            *
            This.obj.Value=This.cVarName
        Endif
Enddefine

* --- Classe per Radio ------------------------------------------------->

Define Class painter_radio As draw_radio
    cVarName=''
    cType=''
    nLen=0
    nDec=0
    cPict=''
    cStato='Edit'
    cTitle=""
    cVal1=''
    cVal2=''
    cVal3=''
    cVal4=''
    cVal5=''
    cVal6=''
    cVal7=''
    cVal8=''
    cVal9=''
    cVal10=''
    cLbl1=''
    cLbl2=''
    cLbl3=''
    cLbl4=''
    cLbl5=''
    cLbl6=''
    cLbl7=''
    cLbl8=''
    cLbl9=''
    cLbl10=''

    FontName = "Arial"
    FontSize = 9
    FontColor = 0
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    GlbFont = .F.

    Proc Init()
        This.cTitle=cp_Translate(MSG_CHECKBOX)
        DoDefault()
    Proc DblClick()
*!*	        Local o
*!*	        o=Createobject('painter_radio_form',This)
*!*	        o.Show()
            DIMENSION aParam[2]
            aParam[1]= this
            aParam[2] = 3
			Local l_batch
			l_batch = "cp_objmaskoptions(@aParam)"
			&l_batch
*!*	            This.obj.Value=This.cVarName

    Proc Serialize(xcg)
        DoDefault(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cVarName)
            xcg.Save('C',This.cType)
            xcg.Save('N',This.nLen)
            xcg.Save('N',This.nDec)
            xcg.Save('C',This.cPict)
            xcg.Save('C',This.cStato)
            xcg.Save('C',This.cVal1)
            xcg.Save('C',This.cVal2)
            xcg.Save('C',This.cVal3)
            xcg.Save('C',This.cVal4)
            xcg.Save('C',This.cVal5)
            xcg.Save('C',This.cVal6)
            xcg.Save('C',This.cVal7)
            xcg.Save('C',This.cVal8)
            xcg.Save('C',This.cVal9)
            xcg.Save('C',This.cVal10)
            xcg.Save('C',This.cLbl1)
            xcg.Save('C',This.cLbl2)
            xcg.Save('C',This.cLbl3)
            xcg.Save('C',This.cLbl4)
            xcg.Save('C',This.cLbl5)
            xcg.Save('C',This.cLbl6)
            xcg.Save('C',This.cLbl7)
            xcg.Save('C',This.cLbl8)
            xcg.Save('C',This.cLbl9)
            xcg.Save('C',This.cLbl10)
            *--- Font
            xcg.Save('C',This.FontName)
            xcg.Save('N',This.FontSize)
            xcg.Save('N',This.FontColor)
            xcg.Save('L',This.FontBold)
            xcg.Save('L',This.FontItalic)
            xcg.Save('L',This.FontUnderline)
            xcg.Save('L',This.FontStrikeout)
            xcg.Save('L',This.GlbFont)
            xcg.Eoc()
        Else
            This.cVarName=xcg.Load('C',cp_Translate(MSG_NO_NAME))
            This.cType=xcg.Load('C',cp_Translate(MSG_NUMERIC))
            This.nLen=xcg.Load('N',0)
            This.nDec=xcg.Load('N',0)
            This.cPict=xcg.Load('C','')
            This.cStato=xcg.Load('C','Edit')
            This.cVal1=xcg.Load('C','')
            This.cVal2=xcg.Load('C','')
            This.cVal3=xcg.Load('C','')
            This.cVal4=xcg.Load('C','')
            This.cVal5=xcg.Load('C','')
            This.cVal6=xcg.Load('C','')
            This.cVal7=xcg.Load('C','')
            This.cVal8=xcg.Load('C','')
            This.cVal9=xcg.Load('C','')
            This.cVal10=xcg.Load('C','')
            This.cLbl1=xcg.Load('C','')
            This.cLbl2=xcg.Load('C','')
            This.cLbl3=xcg.Load('C','')
            This.cLbl4=xcg.Load('C','')
            This.cLbl5=xcg.Load('C','')
            This.cLbl6=xcg.Load('C','')
            This.cLbl7=xcg.Load('C','')
            This.cLbl8=xcg.Load('C','')
            This.cLbl9=xcg.Load('C','')
            This.cLbl10=xcg.Load('C','')
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.FontColor = xcg.Load('N',0)
                This.FontBold= xcg.Load('L',.F.)
                This.FontItalic= xcg.Load('L',.F.)
                This.FontUnderline= xcg.Load('L',.F.)
                This.FontStrikeout= xcg.Load('L',.F.)
                This.GlbFont= xcg.Load('L',.F.)
                xcg.Eoc()
            Endif
            Local a
            Dimension a[1]
            This.AddLbl(@a,This.cLbl1)
            This.AddLbl(@a,This.cLbl2)
            This.AddLbl(@a,This.cLbl3)
            This.AddLbl(@a,This.cLbl4)
            This.AddLbl(@a,This.cLbl5)
            This.AddLbl(@a,This.cLbl6)
            This.AddLbl(@a,This.cLbl7)
            This.AddLbl(@a,This.cLbl8)
            This.AddLbl(@a,This.cLbl9)
            This.AddLbl(@a,This.cLbl10)
            This.SetRadioButtons(@a)
        Endif
        Return
    Proc AddLbl(a,cLbl)
        If !Empty(cLbl)
            Local L
            L=Alen(a)
            If L>1 Or !Empty(a[1])
                L=L+1
                Dimension a[l]
            Endif
            a[l]=cLbl
        Endif
        Return
Enddefine

* --- Classe per ComboBox ------------------------------------------------->

Define Class painter_combo As draw_combo
    cVarName=''
    cType=''
    nLen=0
    nDec=0
    cPict=''
    cStato='Edit'
    cTitle=""
    cVal1=''
    cVal2=''
    cVal3=''
    cVal4=''
    cVal5=''
    cVal6=''
    cVal7=''
    cVal8=''
    cVal9=''
    cVal10=''
    cLbl1=''
    cLbl2=''
    cLbl3=''
    cLbl4=''
    cLbl5=''
    cLbl6=''
    cLbl7=''
    cLbl8=''
    cLbl9=''
    cLbl10=''

    FontName = "Arial"
    FontSize = 9
    FontColor = 0
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    GlbFont = .F.

    Proc Init()
        This.cTitle=cp_Translate(MSG_CHECKBOX)
        DoDefault()
    Proc DblClick()
*!*	        Local o
*!*	        o=Createobject('painter_cmb_form',This)
*!*	        o.Show()
            DIMENSION aParam[2]
            aParam[1]= this
            aParam[2] = 3
			Local l_batch
			l_batch = "cp_objmaskoptions(@aParam)"
			&l_batch
*!*	            This.obj.Value=This.cVarName

    Proc Serialize(xcg)
        DoDefault(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cVarName)
            xcg.Save('C',This.cType)
            xcg.Save('N',This.nLen)
            xcg.Save('N',This.nDec)
            xcg.Save('C',This.cPict)
            xcg.Save('C',This.cStato)
            xcg.Save('C',This.cVal1)
            xcg.Save('C',This.cVal2)
            xcg.Save('C',This.cVal3)
            xcg.Save('C',This.cVal4)
            xcg.Save('C',This.cVal5)
            xcg.Save('C',This.cVal6)
            xcg.Save('C',This.cVal7)
            xcg.Save('C',This.cVal8)
            xcg.Save('C',This.cVal9)
            xcg.Save('C',This.cVal10)
            xcg.Save('C',This.cLbl1)
            xcg.Save('C',This.cLbl2)
            xcg.Save('C',This.cLbl3)
            xcg.Save('C',This.cLbl4)
            xcg.Save('C',This.cLbl5)
            xcg.Save('C',This.cLbl6)
            xcg.Save('C',This.cLbl7)
            xcg.Save('C',This.cLbl8)
            xcg.Save('C',This.cLbl9)
            xcg.Save('C',This.cLbl10)
            *--- Font
            xcg.Save('C',This.FontName)
            xcg.Save('N',This.FontSize)
            xcg.Save('N',This.FontColor)
            xcg.Save('L',This.FontBold)
            xcg.Save('L',This.FontItalic)
            xcg.Save('L',This.FontUnderline)
            xcg.Save('L',This.FontStrikeout)
            xcg.Save('L',This.GlbFont)
            xcg.Eoc()
        Else
            This.cVarName=xcg.Load('C',cp_Translate(MSG_NO_NAME))
            This.cType=xcg.Load('C',cp_Translate(MSG_NUMERIC))
            This.nLen=xcg.Load('N',0)
            This.nDec=xcg.Load('N',0)
            This.cPict=xcg.Load('C','')
            This.cStato=xcg.Load('C','Edit')
            This.cVal1=xcg.Load('C','')
            This.cVal2=xcg.Load('C','')
            This.cVal3=xcg.Load('C','')
            This.cVal4=xcg.Load('C','')
            This.cVal5=xcg.Load('C','')
            This.cVal6=xcg.Load('C','')
            This.cVal7=xcg.Load('C','')
            This.cVal8=xcg.Load('C','')
            This.cVal9=xcg.Load('C','')
            This.cVal10=xcg.Load('C','')
            This.cLbl1=xcg.Load('C','')
            This.cLbl2=xcg.Load('C','')
            This.cLbl3=xcg.Load('C','')
            This.cLbl4=xcg.Load('C','')
            This.cLbl5=xcg.Load('C','')
            This.cLbl6=xcg.Load('C','')
            This.cLbl7=xcg.Load('C','')
            This.cLbl8=xcg.Load('C','')
            This.cLbl9=xcg.Load('C','')
            This.cLbl10=xcg.Load('C','')
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.FontColor = xcg.Load('N',0)
                This.FontBold= xcg.Load('L',.F.)
                This.FontItalic= xcg.Load('L',.F.)
                This.FontUnderline= xcg.Load('L',.F.)
                This.FontStrikeout= xcg.Load('L',.F.)
                This.GlbFont= xcg.Load('L',.F.)
                xcg.Eoc()
            Endif
        Endif
Enddefine

* --- Classe per CheckBox ------------------------------------------------->

Define Class painter_check As draw_check
    cVarName=''
    cType=''
    nLen=0
    nDec=0
    cPict=''
    cStato='Edit'
    cTitle=""
    cSelValue=''
    cUnselValue=''

    FontName = "Arial"
    FontSize = 9
    FontColor = 0
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    GlbFont = .F.

    Proc Init()
        This.cTitle=cp_Translate(MSG_CHECKBOX)
        DoDefault()
    Proc DblClick()
*!*	        Local o
*!*	        o=Createobject('painter_chk_form',This)
*!*	        o.Show()
            DIMENSION aParam[2]
            aParam[1]= this
            aParam[2] = 4
			Local l_batch
			l_batch = "cp_objmaskoptions(@aParam)"
			&l_batch
*!*	            This.obj.Caption=This.cTitle


    Proc Serialize(xcg)
        DoDefault(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cVarName)
            xcg.Save('C',This.cType)
            xcg.Save('N',This.nLen)
            xcg.Save('N',This.nDec)
            xcg.Save('C',This.cPict)
            xcg.Save('C',This.cStato)
            xcg.Save('C',This.cTitle)
            xcg.Save('C',This.cSelValue)
            xcg.Save('C',This.cUnselValue)
            *--- Font
            xcg.Save('C',This.FontName)
            xcg.Save('N',This.FontSize)
            xcg.Save('N',This.FontColor)
            xcg.Save('L',This.FontBold)
            xcg.Save('L',This.FontItalic)
            xcg.Save('L',This.FontUnderline)
            xcg.Save('L',This.FontStrikeout)
            xcg.Save('L',This.GlbFont)
            xcg.Eoc()
        Else
            This.cVarName=xcg.Load('C',cp_Translate(MSG_NO_NAME))
            This.cType=xcg.Load('C',cp_Translate(MSG_NUMERIC))
            This.nLen=xcg.Load('N',0)
            This.nDec=xcg.Load('N',0)
            This.cPict=xcg.Load('C','')
            This.cStato=xcg.Load('C','Edit')
            This.cTitle=xcg.Load('C',cp_Translate(MSG_CHECKBOX))
            This.cSelValue=xcg.Load('C','')
            This.cUnselValue=xcg.Load('C','')
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.FontColor = xcg.Load('N',0)
                This.FontBold= xcg.Load('L',.F.)
                This.FontItalic= xcg.Load('L',.F.)
                This.FontUnderline= xcg.Load('L',.F.)
                This.FontStrikeout= xcg.Load('L',.F.)
                This.GlbFont= xcg.Load('L',.F.)
                xcg.Eoc()
            Endif
            This.obj.Caption=This.cTitle
        Endif
Enddefine

* --- Classe per i Commandbutton ------------------------------------------------->

Define Class painter_btn As draw_btn
    cVarName='.-.'
    cType=''
    cPrg=''
    cTitle=''
    cPicture=''
    nStyle=0
    nFormatPicture=16

    FontName = "Arial"
    FontSize = 9
    FontColor = 0
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    GlbFont = .F.

    Proc DblClick()
*!*	        Local o
*!*	        o=Createobject('painter_btn_form',This)
*!*	        o.Show()
            DIMENSION aParam[2]
            aParam[1]= this
            aParam[2] = 5
			Local l_batch
			l_batch = "cp_objmaskoptions(@aParam)"
			&l_batch
*!*	            This.obj.Caption=This.cTitle
            
            
    Proc Serialize(xcg)
        DoDefault(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cVarName)
            xcg.Save('C',This.cType)
            xcg.Save('C',This.cPrg)
            xcg.Save('C',This.cTitle)
            xcg.Save('C',This.cPicture)
            xcg.Save('N',This.nStyle)
            *--- Font
            xcg.Save('C',This.FontName)
            xcg.Save('N',This.FontSize)
            xcg.Save('N',This.FontColor)
            xcg.Save('L',This.FontBold)
            xcg.Save('L',This.FontItalic)
            xcg.Save('L',This.FontUnderline)
            xcg.Save('L',This.FontStrikeout)
            xcg.Save('L',This.GlbFont)
            *--- Nuove Propriet�
            xcg.Save('N',This.nFormatPicture)
            xcg.Eoc()
        Else
            This.cVarName=xcg.Load('C',cp_Translate(MSG_NO_NAME))
            This.cType=xcg.Load('C','QRY')
            This.cPrg=xcg.Load('C','')
            This.cTitle=xcg.Load('C','')
            This.cPicture=xcg.Load('C','')
            This.nStyle=xcg.Load('N',0)
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Arial')
                This.FontSize = Int(xcg.Load('N',9))
                This.FontColor = xcg.Load('N',0)
                This.FontBold= xcg.Load('L',.F.)
                This.FontItalic= xcg.Load('L',.F.)
                This.FontUnderline= xcg.Load('L',.F.)
                This.FontStrikeout= xcg.Load('L',.F.)
                This.GlbFont= xcg.Load('L',.F.)
                If Not xcg.Eoc(.T.)
                   *--- Nuove Propriet�
                   This.nFormatPicture=xcg.Load('N',16)
                xcg.Eoc()
                ENDIF
            Endif
            This.obj.Caption=This.cTitle
            This.obj.Picture=This.cPicture
            This.obj.Style=This.nStyle
        Endif
Enddefine


* --- Classe per i MediaButton ------------------------------------------------->

Define Class painter_mediabtn As draw_bmp
    cVarName='.-.'
    cType=''
    cPrg=''
    cTitle=''
    cPicture=''
    nStyle=0
    cShape = "Process"
    nAlpha = 50
    nAlphaH = 100
    nAlphaD = 200
    nRadius = 6
    Width = 120
    Height = 120
    BackStyle=0
    nFormatPicture=16

    FontName = "Tahoma"
    FontSize = 10
    FontColor = Rgb(76,76,76)
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    GlbFont = .F.

    cEditCond = ""
    nBrdWidth = 1
    bHandlersLink = .F.
    cHandlerLinkL=""
    cHandlerLinkT=""
    cHandlerLinkR=""
    cHandlerLinkB=""
    nBackColor = Rgb(227,239,255)
    nBorderColor = Rgb(179,212,255)

    Add Object mbtn As painter_mediabtn_exec With Visible = .F.

    Procedure Init()
        If i_VisualTheme<>-1
            This.nBorderColor = i_ThemesManager.GetProp(3)
            This.nBackColor= i_ThemesManager.GetProp(7)
        Else
            This.nBorderColor = Rgb(179,212,255)
            This.nBackColor = Rgb(227,239,255)
        Endif
        DoDefault()

    Procedure mbtn.Init()
        DoDefault()

    Procedure cShape_Assign(xValue)
        This.cShape = m.xValue
        This.Preview()
    Endproc

    Procedure Preview()
    	*--- Zucchetti Aulla inizio
*!*	        This.mbtn.Width = This.Width
*!*	        This.mbtn.Height = This.Height
		This.mbtn.Move(This.mbtn.Left, This.mbtn.Top, This.Width, This.Height)
		*--- Zucchetti Aulla fine
	
        This.mbtn.cIcon = IIF(LOWER(JUSTEXT(This.cPicture))='ico',FORCEEXT(i_ThemesManager.RetBmpFromIco(This.cPicture, this.nFormatPicture),"bmp"),This.cPicture)
        This.mbtn.cText = This.cTitle
        This.mbtn.cShape = This.cShape

        This.mbtn.FontName = This.FontName
        This.mbtn.FontSize = This.FontSize
        This.mbtn.FontColor = This.FontColor
        This.mbtn.FontStyle = 0
        This.mbtn.FontStyle = This.mbtn.FontStyle + Iif(This.FontBold, 1, 0)
        This.mbtn.FontStyle = This.mbtn.FontStyle + Iif(This.FontItalic, 2, 0)
        This.mbtn.cEditCond = This.cEditCond
        This.mbtn.brdWidth = This.nBrdWidth
        This.mbtn.nRadius = This.nRadius
        This.mbtn.BackColor = This.nBackColor
        This.mbtn.BorderColor = This.nBorderColor

        This.obj.PictureVal = This.mbtn.DrawButton("A")
    Endproc

    Procedure Destroy()
        DoDefault()
        Local obj, i
        Dimension aDelNode(1)
        For Each obj In This.Parent.Controls
            If Lower(obj.Class)=="draw_linenode" And obj.cMBtn==This.Name
                aDelNode(Alen(aDelNode,1)) = m.obj
                Dimension aDelNode(Alen(aDelNode,1)+1)
            Endif
        Next
        For i=1 To Alen(aDelNode,1)-1
            If Vartype(aDelNode[i])='O'
                aDelNode[i].RemoveAll()
            Endif
        Next
    Endproc

    Procedure Resize()
        DoDefault()
        This.Preview()
    Endproc

    Proc DblClick()
*!*	        Local o, mbtn
*!*	        o=Createobject('painter_btn_form',This)
*!*	        o.Show()
            DIMENSION aParam[2]
            aParam[1]= this
            aParam[2] = 5
			Local l_batch
			l_batch = "cp_objmaskoptions(@aParam)"
			&l_batch
*!*	        This.Preview()

    Proc Serialize(xcg)
        DoDefault(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cVarName)
            xcg.Save('C',This.cType)
            xcg.Save('C',This.cPrg)
            xcg.Save('C',This.cTitle)
            xcg.Save('C',This.cPicture)
            xcg.Save('N',This.nStyle)
            *--- Font
            xcg.Save('C',This.FontName)
            xcg.Save('N',This.FontSize)
            xcg.Save('N',This.FontColor)
            xcg.Save('L',This.FontBold)
            xcg.Save('L',This.FontItalic)
            xcg.Save('L',This.FontUnderline)
            xcg.Save('L',This.FontStrikeout)
            xcg.Save('L',This.GlbFont)
            xcg.Save('C',This.cShape)
            xcg.Save('N',This.nAlpha)
            xcg.Save('N',This.nAlphaH)
            xcg.Save('N',This.nAlphaD)
            xcg.Save('N',This.nBrdWidth)
            xcg.Save('C',This.cEditCond)
            xcg.Save('N',This.nRadius)
            xcg.Save('N',This.nBorderColor)
            xcg.Save('N',This.nBackColor)
            *-- Nuove propriet�
            xcg.Save('N',This.nFormatPicture)
            xcg.Eoc()
        Else
            This.cVarName=xcg.Load('C',cp_Translate(MSG_NO_NAME))
            This.cType=xcg.Load('C','QRY')
            This.cPrg=xcg.Load('C','')
            This.cTitle=xcg.Load('C','')
            This.cPicture=xcg.Load('C','')
            This.nStyle=xcg.Load('N',0)
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Tahoma')
                This.FontSize = Int(xcg.Load('N',10))
                This.FontColor = xcg.Load('N',0)
                This.FontBold = xcg.Load('L',.F.)
                This.FontItalic = xcg.Load('L',.F.)
                This.FontUnderline = xcg.Load('L',.F.)
                This.FontStrikeout = xcg.Load('L',.F.)
                This.GlbFont = xcg.Load('L',.F.)
                If Not xcg.Eoc(.T.)
                    This.cShape = xcg.Load('C','Process')
                    This.nAlpha = xcg.Load('N',0)
                    This.nAlphaH = xcg.Load('N',100)
                    This.nAlphaD = xcg.Load('N',200)
                    This.nBrdWidth = xcg.Load('N',1)
                    This.cEditCond = xcg.Load('C','')
                    This.nRadius = xcg.Load('N',6)
                    If Not xcg.Eoc(.T.)
                        If i_VisualTheme<>-1
                            This.nBorderColor = xcg.Load('N',i_ThemesManager.GetProp(3))
                            This.nBackColor= xcg.Load('N',i_ThemesManager.GetProp(7))
                        Else
                            This.nBorderColor = xcg.Load('N',Rgb(179,212,255))
                            This.nBackColor = xcg.Load('N',Rgb(227,239,255))
                        ENDIF
                        If Not xcg.Eoc(.T.)
                            This.nFormatPicture=xcg.Load('N',16)
                            xcg.Eoc()
                        endif
                    Endif
                Endif
            Endif
            This.Preview()
        Endif

    Proc AddHandlers()
        DoDefault()
        This.AddHandlersLink()
    Endproc

    Procedure AddHandlersLink()
        With This
            If !.bHandlersLink
                .cHandlerLinkL= .Parent.AddHandlerLink(.Left, .Top+.Height/2, This, "L")
                .cHandlerLinkT= .Parent.AddHandlerLink(.Left+.Width/2, .Top, This, "T")
                .cHandlerLinkR= .Parent.AddHandlerLink(.Left+.Width, .Top+.Height/2, This, "R")
                .cHandlerLinkB= .Parent.AddHandlerLink(.Left+.Width/2, .Top+.Height, This, "B")
                .bHandlersLink = .T.
            Endif
        Endwith

    Proc RemoveHandlers()
        DoDefault()
        This.RemoveHandlersLink()
    Endproc

    Proc RemoveHandlersLink()
        If This.bHandlersLink
            This.Parent.RemoveObject(This.cHandlerLinkL)
            This.Parent.RemoveObject(This.cHandlerLinkT)
            This.Parent.RemoveObject(This.cHandlerLinkR)
            This.Parent.RemoveObject(This.cHandlerLinkB)
            This.bHandlersLink = .F.
        Endif
        Return

    Proc MoveDelta(dx,dy)
        * --- sposta l' oggetto di dx e dy pixel
        DoDefault(dx,dy)
        Local N, obj
        If This.bHandlers
            N=This.cHandlerLinkL
            This.Parent.&N..MoveDelta(dx, dy)
            N=This.cHandlerLinkT
            This.Parent.&N..MoveDelta(dx, dy)
            N=This.cHandlerLinkR
            This.Parent.&N..MoveDelta(dx, dy)
            N=This.cHandlerLinkB
            This.Parent.&N..MoveDelta(dx, dy)
            This.CheckLines()
        Endif

    Procedure CheckLines()
        *--- Check Connector Line
        Local cNodeRoot
        For Each obj In This.Parent.Controls
            If Lower(obj.Class)=="draw_linenode"
                If obj.cMBtn==This.Name
                    Do Case
                        Case obj.cMBtnAnchor='L'
                            obj.Left= This.Left-obj.Width/2
                            obj.Top= This.Top+This.Height/2-obj.Height/2
                        Case obj.cMBtnAnchor='R'
                            obj.Left= This.Left+This.Width-obj.Width/2
                            obj.Top= This.Top+This.Height/2-obj.Height/2
                        Case obj.cMBtnAnchor='T'
                            obj.Left= This.Left+This.Width/2-obj.Width/2
                            obj.Top= This.Top-obj.Height/2
                        Case obj.cMBtnAnchor='B'
                            obj.Left= This.Left+This.Width/2-obj.Width/2
                            obj.Top= This.Top+This.Height-obj.Height/2
                    Endcase
                Endif
                cNodeRoot = obj.cRoot
                This.Parent.&cNodeRoot..RedrawLines()
            Endif
        Endfor

    Proc MoveTo(nx,ny)
        DoDefault(nx,ny)
        If This.bHandlers
            This.RedrawHandlerLink()
        Endif
    Procedure RedrawHandlerLink()
        With This
            If .bHandlers
                N=.cHandlerLinkL
                .Parent.&N..MoveTo(.Left, .Top+.Height/2)
                N=.cHandlerLinkT
                .Parent.&N..MoveTo(.Left+.Width/2, .Top)
                N=.cHandlerLinkR
                .Parent.&N..MoveTo(.Left+.Width, .Top+.Height/2)
                N=This.cHandlerLinkB
                .Parent.&N..MoveTo(.Left+.Width/2, .Top+.Height)
                This.CheckLines()
            Endif
        Endwith
    Endproc
Enddefine

* --- Classe per i TileButton ------------------------------------------------->

Define Class painter_tilebtn As draw_bmp
    cVarName='.-.'
    cType=''
    cPrg=''
    cTitle=''
    cPicture=''
    nStyle=0
    cShape = "Square tile"
    nAlpha = 255
    nAlphaH = 255
    nAlphaD = 240
    nRadius = 6
    Width = 120
    Height = 120
    BackStyle=0

    FontName = "Tahoma"
    FontSize = 7
    FontColor = Rgb(255,255,230)
    FontBold = .F.
    FontItalic = .F.
    FontUnderline = .F.
    FontStrikeout = .F.
    GlbFont = .F.

    cEditCond = ""
    nBrdWidth = 1
    bHandlersLink = .F.
    cHandlerLinkL=""
    cHandlerLinkT=""
    cHandlerLinkR=""
    cHandlerLinkB=""
    nBackColor = Rgb(227,239,255)
    nBorderColor = Rgb(179,212,255)

    Add Object mbtn As painter_tilebtn_exec With Visible = .F.

    Procedure Init()
		local nColorRandom
		nColorRandom=MOD(INT(RAND()*10),4)
		do case
			case nColorRandom=1
				This.nBorderColor = Rgb(124,50,255)
				This.nBackColor = Rgb(124,50,255)
			case nColorRandom=2
				This.nBorderColor = Rgb(255,90,50)
				This.nBackColor = Rgb(255,90,50)
			case nColorRandom=3
				This.nBorderColor = Rgb(100,200,60)
				This.nBackColor = Rgb(100,200,60)		
			otherwise
				This.nBorderColor = Rgb(0,150,255)
				This.nBackColor = Rgb(0,150,255)
        Endcase
        DoDefault()

    Procedure mbtn.Init()
        DoDefault()

    Procedure cShape_Assign(xValue)
        This.cShape = m.xValue
        This.Preview()
    Endproc

    Procedure Preview()
    *--- Zucchetti Aulla inizio
		Do case
			Case This.cShape=="Large tile"
*!*					This.Width=250
*!*					This.Height = 120
				This.Move(This.Left, This.Top, 250, 120)				
			Case This.cShape=="Small tile"
*!*					This.Width = 250
*!*					This.Height = 55	
				This.Move(This.Left, This.Top, 250, 55)			
			Otherwise
*!*					This.Width = 120
*!*					This.Height = 120	
				This.Move(This.Left, This.Top, 120, 120)		
		Endcase
*!*			This.mbtn.Width = This.Width
*!*	        This.mbtn.Height = This.Height
		This.mbtn.Move(This.mbtn.Left, This.mbtn.Top, This.Width, This.Height)
		*--- Zucchetti Aulla fine
	
        This.mbtn.cIcon = This.cPicture
        This.mbtn.cText = This.cTitle
        This.mbtn.cShape = This.cShape

        This.mbtn.FontName = This.FontName
        This.mbtn.FontSize = This.FontSize
        This.mbtn.FontColor = This.FontColor
        This.mbtn.FontStyle = 0
        This.mbtn.FontStyle = This.mbtn.FontStyle + Iif(This.FontBold, 1, 0)
        This.mbtn.FontStyle = This.mbtn.FontStyle + Iif(This.FontItalic, 2, 0)
        This.mbtn.cEditCond = This.cEditCond
        This.mbtn.brdWidth = This.nBrdWidth
        This.mbtn.nRadius = This.nRadius
        This.mbtn.BackColor = This.nBackColor
        This.mbtn.BorderColor = This.nBorderColor

        This.obj.PictureVal = This.mbtn.DrawButton("A")
    Endproc

    Procedure Destroy()
        DoDefault()
        Local obj, i
        Dimension aDelNode(1)
        For Each obj In This.Parent.Controls
            If Lower(obj.Class)=="draw_linenode" And obj.cMBtn==This.Name
                aDelNode(Alen(aDelNode,1)) = m.obj
                Dimension aDelNode(Alen(aDelNode,1)+1)
            Endif
        Next
        For i=1 To Alen(aDelNode,1)-1
            If Vartype(aDelNode[i])='O'
                aDelNode[i].RemoveAll()
            Endif
        Next
    Endproc

    Procedure Resize()
    	*--- Zucchetti Aulla inizio
		Do case
			Case this.cShape = "Small tile"
*!*					This.Width=250
*!*					This.Height=55
				This.Move(This.Left, This.Top, 250, 55)
			Case this.cShape = "Large tile"
*!*					This.Width=250
*!*					This.Height=120
				This.Move(This.Left, This.Top, 250, 120)
			Otherwise
*!*					This.Width=120
*!*					This.Height=120
				This.Move(This.Left, This.Top, 120, 120)
		Endcase
		*--- Zucchetti Aulla fine
        This.Preview()
    Endproc

    Proc DblClick()
*!*	        Local o, mbtn
*!*	        o=Createobject('painter_btn_form',This)
*!*	        o.Show()
            DIMENSION aParam[2]
            aParam[1]= this
            aParam[2] = 5
			Local l_batch
			l_batch = "cp_objmaskoptions(@aParam)"
			&l_batch
*!*	        This.Preview()

    Proc Serialize(xcg)
        DoDefault(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cVarName)
            xcg.Save('C',This.cType)
            xcg.Save('C',This.cPrg)
            xcg.Save('C',This.cTitle)
            xcg.Save('C',This.cPicture)
            xcg.Save('N',This.nStyle)
            *--- Font
            xcg.Save('C',This.FontName)
            xcg.Save('N',This.FontSize)
            xcg.Save('N',This.FontColor)
            xcg.Save('L',This.FontBold)
            xcg.Save('L',This.FontItalic)
            xcg.Save('L',This.FontUnderline)
            xcg.Save('L',This.FontStrikeout)
            xcg.Save('L',This.GlbFont)
            xcg.Save('C',This.cShape)
            xcg.Save('N',This.nAlpha)
            xcg.Save('N',This.nAlphaH)
            xcg.Save('N',This.nAlphaD)
            xcg.Save('N',This.nBrdWidth)
            xcg.Save('C',This.cEditCond)
            xcg.Save('N',This.nRadius)
            xcg.Save('N',This.nBorderColor)
            xcg.Save('N',This.nBackColor)
            xcg.Eoc()
        Else
            This.cVarName=xcg.Load('C',cp_Translate(MSG_NO_NAME))
            This.cType=xcg.Load('C','QRY')
            This.cPrg=xcg.Load('C','')
            This.cTitle=xcg.Load('C','')
            This.cPicture=xcg.Load('C','')
            This.nStyle=xcg.Load('N',0)
            *--- Font
            If Not xcg.Eoc(.T.)
                This.FontName = xcg.Load('C','Tahoma')
                This.FontSize = Int(xcg.Load('N',10))
                This.FontColor = xcg.Load('N',0)
                This.FontBold = xcg.Load('L',.F.)
                This.FontItalic = xcg.Load('L',.F.)
                This.FontUnderline = xcg.Load('L',.F.)
                This.FontStrikeout = xcg.Load('L',.F.)
                This.GlbFont = xcg.Load('L',.F.)
                If Not xcg.Eoc(.T.)
                    This.cShape = xcg.Load('C','Process')
                    This.nAlpha = xcg.Load('N',0)
                    This.nAlphaH = xcg.Load('N',100)
                    This.nAlphaD = xcg.Load('N',200)
                    This.nBrdWidth = xcg.Load('N',1)
                    This.cEditCond = xcg.Load('C','')
                    This.nRadius = xcg.Load('N',6)
                    If Not xcg.Eoc(.T.)
                        If i_VisualTheme<>-1
                            This.nBorderColor = xcg.Load('N',i_ThemesManager.GetProp(3))
                            This.nBackColor= xcg.Load('N',i_ThemesManager.GetProp(7))
                        Else
                            This.nBorderColor = xcg.Load('N',Rgb(179,212,255))
                            This.nBackColor = xcg.Load('N',Rgb(227,239,255))
                        Endif
                        xcg.Eoc()
                    Endif
                Endif
            Endif
            This.Preview()
        Endif

Enddefine


Define Class painter_bitmap As draw_bmp
    cVarName='.-.'
    cType=''
    cPrg=''
    cTitle=''
    cPicture=''
    nFormatPicture=16
    Proc DblClick()
*!*	        Local o
*!*	        o=Createobject('painter_btn_form',This)
*!*	        o.Show()
            DIMENSION aParam[2]
            aParam[1]= this
            aParam[2] = 7
			Local l_batch
			l_batch = "cp_objmaskoptions(@aParam)"
			&l_batch
    Proc Init()
        This.ZOrder(1)
    Proc Serialize(xcg)
        DoDefault(xcg)
        If xcg.bIsStoring
            xcg.Save('C',This.cVarName)
            xcg.Save('C',This.cType)
            xcg.Save('C',This.cPrg)
            xcg.Save('C',This.cTitle)
            xcg.Save('C',This.cPicture)
            *-- Nuove propriet�
            xcg.Save('N',This.nFormatPicture)
            xcg.Eoc()
        Else
            This.cVarName=xcg.Load('C',cp_Translate(MSG_NO_NAME))
            This.cType=xcg.Load('C','QRY')
            This.cPrg=xcg.Load('C','')
            This.cTitle=xcg.Load('C','')
            This.cPicture=xcg.Load('C','')
            If Not xcg.Eoc(.T.)
                This.nFormatPicture=xcg.Load('N',16)
                xcg.Eoc()
            ENDIF
            If Vartype(i_ThemesManager)=='O'
                i_ThemesManager.PutBmp(This.obj, This.cPicture,This.nFormatPicture)
            Else
                This.obj.Picture=This.cPicture
            Endif
        Endif
Enddefine

* --- form per le varie opzioni

Define Class painter_form As CpSysform
    WindowType=1
    oObj=.Null.
    Icon=i_cBmpPath+i_cStdIcon
    Add Object btnok As CommandButton With Caption="",Width=50,Height=25
    Add Object btncan As CommandButton With Caption="",Width=50,Height=25
    Proc Init(oObj)
        This.oObj=oObj
        This.LoadValues()
        This.btnok.Top=This.Height-30
        This.btnok.Left=This.Width-110
        This.btncan.Top=This.Height-30
        This.btncan.Left=This.Width-55
        This.btnok.Caption=cp_Translate(MSG_OK_BUTTON)
        This.btncan.Caption=cp_Translate(MSG_CANCEL_BUTTON)
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                	*--- Zucchetti Aulla inizio
*!*	                    .Width = Thisform.Width
*!*	                    .Height = Thisform.Height
					.Move(.Left, .Top, Thisform.Width, Thisform.Height)
					*--- Zucchetti Aulla fine
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
    Proc btnok.Click()
        Thisform.SaveValues()
        Thisform.Hide()
    Proc btncan.Click()
        Thisform.Hide()
    Proc LoadValues()
        Return
    Proc SaveValues()
        Return
Enddefine

Define Class painter_lbl_form As painter_form
    Top=13
    Left=117
    Width=399
    Height=270
    Caption=MSG_STRING_OPTIONS
    Add Object lbl1 As Label With Caption="",Alignment=1,Top=18,Left=16,Width=78,Height=16, BackStyle = 0
    Add Object lbl2 As Label With Caption="",Alignment=1,Top=46,Left=16,Width=78,Height=19, BackStyle = 0
	*** Zucchetti Aulla - portata a 254 i caratteri che possono essere inseriti come label per funzioni calcolate
    Add Object txt As TextBox With Top=15,Left=98,Width=272,Height=22,MaxLength=254
    Add Object alg As ComboBox With Top=45,Left=98,Width=89,Height=22
    Add Object fnt As fontOptCnt With Left=15, Top=75
    Add Object sizes As sizeOptContainer With Left=15, Top= 160
    Proc LoadValues()
        This.txt.Value=This.oObj.cVarName
        This.alg.AddItem(cp_Translate(MSG_LEFT))
        This.alg.AddItem(cp_Translate(MSG_CENTRED ))
        This.alg.AddItem(cp_Translate(MSG_RIGHT))
        This.alg.Value=Iif(This.oObj.cAlign='R',cp_Translate(MSG_RIGHT),Iif(This.oObj.cAlign='C',cp_Translate(MSG_CENTRED),cp_Translate(MSG_LEFT)))
        *--- Font
        This.fnt.LoadFont(This.oObj)
        *--- Dimensioni e posizione
        This.sizes.LoadSeD(This.oObj)
    Proc SaveValues()
        This.oObj.cVarName=Trim(This.txt.Value)
        This.oObj.cAlign=Iif(This.alg.Value=cp_Translate(MSG_RIGHT),'R',Iif(This.alg.Value=cp_Translate(MSG_CENTRED),'C',''))
        *--- Font
        This.fnt.SaveFont(This.oObj)
        *--- Dimensioni e posizione
        This.sizes.SaveSeD(This.oObj)
    Proc Init(oObj)
        This.Caption=cp_Translate(MSG_STRING_OPTIONS)
        This.lbl1.Caption= cp_Translate(MSG_TEXT+MSG_FS)
        This.lbl2.Caption=cp_Translate(MSG_ALIGNMENT+MSG_FS)
        DoDefault(oObj)
    Endproc
Enddefine

Define Class painter_txt_form As painter_form
    Top=16
    Left=123
    Width=404
    Height=539
    Caption=""
    Add Object lbl1 As Label With Caption="",Alignment=1,Top=13,Left=11,Width=54,Height=19, BackStyle = 0
    Add Object txt As TextBox With Top=11,Left=69,Width=100,Height=20,Margin=1
    Add Object lbl3 As Label With Caption="",Alignment=1,Top=39,Left=15,Width=50,Height=20, BackStyle = 0
    Add Object typ As ComboBox With Top=38,Left=69,Width=96,Height=20
    Add Object lbl5 As Label With Caption="",Alignment=1,Top=66,Left=35,Width=30,Height=18, BackStyle = 0
    Add Object lbl6 As Label With Caption="",Alignment=1,Top=68,Left=114,Width=32,Height=17, BackStyle = 0
    Add Object Len As TextBox With  InputMask="999",Top=65,Left=69,Width=40,Height=21,Margin=1
    Add Object dec As TextBox With  InputMask="9",Top=65,Left=149,Width=31,Height=21,Margin=1
    Add Object lbl9 As Label With Caption="",Alignment=1,Top=109,Left=15,Width=50,Height=20, BackStyle = 0
    Add Object Pict As TextBox With Top=106,Left=70,Width=287,Height=20,Margin=1
    Add Object edt As Checkbox With Caption="",Top=11,Left=196,Width=74,Height=21, BackStyle = 0
    Add Object zfill As Checkbox With Caption="",Top=35,Left=196,Width=110,Height=20, BackStyle = 0
    Add Object lbl13 As Label With Caption="",Alignment=1,Top=150,Left=12,Width=52,Height=15, BackStyle = 0
    Add Object arc As TextBox With Top=148,Left=65,Width=100,Height=20,Margin=1
    Add Object k1 As TextBox With Top=191,Left=15,Width=100,Height=20,Margin=1
    Add Object vk1 As TextBox With Top=191,Left=120,Width=100,Height=20,Margin=1
    Add Object k2 As TextBox With Top=213,Left=15,Width=100,Height=20,Margin=1
    Add Object vk2 As TextBox With Top=213,Left=120,Width=100,Height=20,Margin=1
    Add Object k3 As TextBox With Top=235,Left=15,Width=100,Height=20,Margin=1
    Add Object vk3 As TextBox With Top=236,Left=120,Width=100,Height=20,Margin=1
    Add Object lbl21 As Label With Caption="",Top=174,Left=15,Width=96,Height=14, BackStyle = 0
    Add Object ka As TextBox With Top=257,Left=15,Width=100,Height=20,Margin=1
    Add Object lbl23 As Label With Caption="",Top=174,Left=121,Width=48,Height=14, BackStyle = 0
    Add Object rf1 As TextBox With Top=300,Left=15,Width=100,Height=20,Margin=1
    Add Object rv1 As TextBox With Top=300,Left=120,Width=100,Height=20,Margin=1
    Add Object lbl26 As Label With Caption="",Top=282,Left=14,Width=118,Height=16, BackStyle = 0
    *
    Add Object arcbtn As CommandButton With Top=148,Left=165,Width=15,Height=20,Caption=""

    Add Object fnt As fontOptCnt With Left=15, Top=330

    Add Object sizes As sizeOptContainer With Left=15, Top= 415

    Proc Init(oObj)
        This.Caption=cp_Translate(MSG_VARIABLE_OPTIONS)
        This.lbl1.Caption=cp_Translate(MSG_VARIABLE+MSG_FS)
        This.lbl3.Caption=cp_Translate(MSG_TYPE+MSG_FS)
        This.lbl5.Caption=cp_Translate(MSG_LEN_F+MSG_FS)
        This.lbl6.Caption=cp_Translate(MSG_DEC_F+MSG_FS)
        This.lbl9.Caption=cp_Translate(MSG_PICTURE+MSG_FS)
        This.edt.Caption=cp_Translate(MSG_EDITABLE)
        This.zfill.Caption=cp_Translate(MSG_FILL_WITH_ZEROES)
        This.lbl13.Caption=cp_Translate(MSG_ARCHIVES+MSG_FS)
        This.lbl21.Caption=cp_Translate(MSG_FIXED_KEY)
        This.lbl23.Caption=cp_Translate(MSG_VALUES)
        This.lbl26.Caption=cp_Translate(MSG_LINKED_FIELD)
        This.arcbtn.Caption=cp_Translate(MSG_D)
        DoDefault(oObj)
    Endproc


    Proc arcbtn.Click()
        Local o
        o=Createobject('dcx_select_table')
        o.Show()
        If !Isnull(o) And o.nChoosed<>0
            Local N,k,c,i,j,v
            * --- svuota
            This.Parent.k1.Value=''
            This.Parent.k2.Value=''
            This.Parent.k3.Value=''
            This.Parent.ka.Value=''
            *
            N=i_dcx.GetTable(o.nChoosed)
            k=cp_KeyToSQL(i_dcx.GetIdxDef(N,1))
            i=At(',',k)
            j=1
            Do While i<>0
                c=Left(k,i-1)
                v=Str(j,1)
                This.Parent.k&v..Value=c
                k=Substr(k,i+1)
                i=At(',',k)
                j=j+1
            Enddo
            This.Parent.ka.Value=k
            This.Parent.arc.Value=N
        Endif
        Return
    Proc LoadValues()
        This.txt.Value=This.oObj.cVarName
        This.typ.AddItem(cp_Translate(MSG_CHARACTER))
        This.typ.AddItem(cp_Translate(MSG_NUMERIC))
        This.typ.AddItem(cp_Translate(MSG_DATE ))
        This.typ.Value=Iif(This.oObj.cType='N',cp_Translate(MSG_NUMERIC),Iif(This.oObj.cType='D',cp_Translate(MSG_DATE) ,cp_Translate(MSG_CHARACTER)))
        This.Len.Value=This.oObj.nLen
        This.dec.Value=This.oObj.nDec
        This.Pict.Value=This.oObj.cPict
        This.edt.Value=This.oObj.cStato='Edit'
        This.zfill.Value=Iif(This.oObj.cZFill='S',1,0)
        This.arc.Value=This.oObj.cArch
        This.k1.Value=This.oObj.cK1
        This.k2.Value=This.oObj.cK2
        This.k3.Value=This.oObj.cK3
        This.ka.Value=This.oObj.cKa
        This.vk1.Value=This.oObj.cVk1
        This.vk2.Value=This.oObj.cVk2
        This.vk3.Value=This.oObj.cVk3
        This.rf1.Value=This.oObj.cRf1
        This.rv1.Value=This.oObj.cRv1
        *-- Font
        This.fnt.LoadFont(This.oObj)
        *--- Dimensioni e posizione
        This.sizes.LoadSeD(This.oObj)
    Proc SaveValues()
        This.oObj.cVarName=Trim(This.txt.Value)
        This.oObj.cType=Iif(This.typ.Value=cp_Translate(MSG_DATE) ,'D',Iif(This.typ.Value=cp_Translate(MSG_NUMERIC),'N','C'))
        This.oObj.nLen=This.Len.Value
        This.oObj.nDec=This.dec.Value
        This.oObj.cPict=Trim(This.Pict.Value)
        This.oObj.cStato=Iif(This.edt.Value,'Edit','Show')
        This.oObj.cZFill=Iif(This.zfill.Value=1,'S','N')
        This.oObj.cArch=Trim(This.arc.Value)
        This.oObj.cK1=Trim(This.k1.Value)
        This.oObj.cK2=Trim(This.k2.Value)
        This.oObj.cK3=Trim(This.k3.Value)
        This.oObj.cKa=Trim(This.ka.Value)
        This.oObj.cVk1=Trim(This.vk1.Value)
        This.oObj.cVk2=Trim(This.vk2.Value)
        This.oObj.cVk3=Trim(This.vk3.Value)
        This.oObj.cRf1=Trim(This.rf1.Value)
        This.oObj.cRv1=Trim(This.rv1.Value)
        *--- Font
        This.fnt.SaveFont(This.oObj)
        *--- Dimensioni e posizione
        This.sizes.SaveSeD(This.oObj)
Enddefine

Define Class painter_chk_form As painter_form
    Top=14
    Left=43
    Width=401
    Height=421
    Caption=MSG_CHECKBOX_OPTIONS
    Add Object lbl1 As Label With Caption="",Alignment=1,Top=13,Left=11,Width=54,Height=19, BackStyle = 0
    Add Object txt As TextBox With Top=11,Left=69,Width=100,Height=20
    Add Object lbl3 As Label With Caption="",Alignment=1,Top=39,Left=15,Width=50,Height=20, BackStyle = 0
    Add Object typ As ComboBox With Top=38,Left=69,Width=96,Height=20
    Add Object lbl5 As Label With Caption="",Alignment=1,Top=66,Left=35,Width=30,Height=18, BackStyle = 0
    Add Object lbl6 As Label With Caption="",Alignment=1,Top=68,Left=114,Width=32,Height=17, BackStyle = 0
    Add Object Len As TextBox With Top=65,Left=69,Width=40,Height=21,InputMask='999'
    Add Object dec As TextBox With Top=65,Left=149,Width=31,Height=21,InputMask='9'
    Add Object lbl9 As Label With Caption="",Alignment=1,Top=114,Left=22,Width=137,Height=19, BackStyle = 0
    Add Object vsel As TextBox With Top=111,Left=163,Width=214,Height=21
    Add Object lbl11 As Label With Caption="",Alignment=1,Top=143,Left=11,Width=148,Height=18, BackStyle = 0
    Add Object vunsel As TextBox With Top=140,Left=163,Width=214,Height=21
    Add Object lbl13 As Label With Caption="",Alignment=1,Top=14,Left=185,Width=36,Height=15, BackStyle = 0
    Add Object cpt As TextBox With Top=12,Left=224,Width=159,Height=20
    Add Object edt As Checkbox With Caption=MSG_EDITABLE,Top=45,Left=205,Width=100,Height=20, BackStyle = 0
    Add Object fnt As fontOptCnt With Left=11, Top=180
    Add Object sizes As sizeOptContainer With Left=15, Top= 265

    Proc Init(oObj)
        This.Caption=cp_Translate(MSG_CHECKBOX_OPTIONS)
        This.lbl1.Caption=cp_Translate(MSG_VARIABLE+MSG_FS)
        This.lbl3.Caption=cp_Translate(MSG_TYPE+MSG_FS)
        This.lbl5.Caption=cp_Translate(MSG_LEN_F+MSG_FS)
        This.lbl6.Caption=cp_Translate(MSG_DEC_F+MSG_FS)
        This.lbl9.Caption=cp_Translate(MSG_CHECKED_VALUE+MSG_FS)
        This.lbl11.Caption=cp_Translate(MSG_UNCHECKED_VALUE+MSG_FS)
        This.lbl13.Caption=cp_Translate(MSG_TITLE+MSG_FS)
        This.edt.Caption=cp_Translate(MSG_EDITABLE)
        DoDefault(oObj)
    Endproc

    Proc LoadValues()
        This.txt.Value=This.oObj.cVarName
        This.typ.AddItem(cp_Translate(MSG_CHARACTER) )
        This.typ.AddItem(cp_Translate(MSG_NUMERIC))
        This.typ.AddItem(cp_Translate(MSG_DATE))
        This.typ.Value=Iif(This.oObj.cType='N',cp_Translate(MSG_NUMERIC),Iif(This.oObj.cType='D',cp_Translate(MSG_DATE) ,cp_Translate(MSG_CHARACTER) ))
        This.Len.Value=This.oObj.nLen
        This.dec.Value=This.oObj.nDec
        This.cpt.Value=This.oObj.cTitle
        This.vsel.Value=This.oObj.cSelValue
        This.vunsel.Value=This.oObj.cUnselValue
        This.edt.Value=This.oObj.cStato='Edit'
        *--- Font
        This.fnt.LoadFont(This.oObj)
        *--- Dimensioni e posizione
        This.sizes.LoadSeD(This.oObj)
    Proc SaveValues()
        This.oObj.cVarName=Trim(This.txt.Value)
        This.oObj.cType=Iif(This.typ.Value=cp_Translate(MSG_DATE) ,'D',Iif(This.typ.Value=cp_Translate(MSG_NUMERIC),'N','C'))
        This.oObj.nLen=This.Len.Value
        This.oObj.nDec=This.dec.Value
        This.oObj.cTitle=Trim(This.cpt.Value)
        This.oObj.cSelValue=Trim(This.vsel.Value)
        This.oObj.cUnselValue=Trim(This.vunsel.Value)
        This.oObj.cStato=Iif(This.edt.Value,'Edit','Show')
        *--- Font
        This.fnt.SaveFont(This.oObj)
        *--- Dimensioni e posizione
        This.sizes.SaveSeD(This.oObj)
Enddefine

Define Class painter_cmb_form As painter_form
    Top=25
    Left=101
    Width=391
    Height=600
    Caption=""
    Add Object lbl1 As Label With Caption="",Alignment=1,Top=13,Left=11,Width=54,Height=19, BackStyle = 0
    Add Object txt As TextBox With Top=11,Left=69,Width=100,Height=22
    Add Object lbl3 As Label With Caption="",Alignment=1,Top=39,Left=15,Width=50,Height=20, BackStyle = 0
    Add Object typ As ComboBox With Top=38,Left=69,Width=96,Height=20
    Add Object lbl5 As Label With Caption="",Alignment=1,Top=66,Left=35,Width=30,Height=18, BackStyle = 0
    Add Object lbl6 As Label With Caption="",Alignment=1,Top=68,Left=114,Width=32,Height=17, BackStyle = 0
    Add Object Len As TextBox With  InputMask="999",Top=65,Left=69,Width=40,Height=22
    Add Object dec As TextBox With  InputMask="9",Top=65,Left=149,Width=31,Height=22
    *add object lbl9 as label with caption="Titolo:",alignment=1,top=14,left=185,width=36,height=15
    *add object cpt as textbox with top=12,left=224,width=159,height=20
    Add Object edt As Checkbox With Caption="",Top=45,Left=205,Width=100,Height=20, BackStyle = 0
    Add Object lbl12 As Label With Caption="",Top=97,Left=8,Width=35,Height=15, BackStyle = 0
    Add Object lbl13 As Label With Caption="",Top=96,Left=114,Width=80,Height=15, BackStyle = 0
    Add Object val1 As TextBox With Top=115,Left=5,Width=100,Height=22
    Add Object lbl_1 As TextBox With Top=115,Left=112,Width=260,Height=22
    Add Object val2 As TextBox With Top=139,Left=5,Width=100,Height=22
    Add Object lbl_2 As TextBox With Top=139,Left=112,Width=260,Height=22
    Add Object val3 As TextBox With Top=163,Left=5,Width=100,Height=22
    Add Object lbl_3 As TextBox With Top=163,Left=112,Width=260,Height=22
    Add Object val4 As TextBox With Top=187,Left=5,Width=100,Height=22
    Add Object lbl_4 As TextBox With Top=187,Left=112,Width=260,Height=22
    Add Object val5 As TextBox With Top=211,Left=5,Width=100,Height=22
    Add Object lbl_5 As TextBox With Top=211,Left=112,Width=260,Height=22
    Add Object val6 As TextBox With Top=235,Left=5,Width=100,Height=22
    Add Object lbl_6 As TextBox With Top=235,Left=112,Width=260,Height=22
    Add Object val7 As TextBox With Top=259,Left=5,Width=100,Height=22
    Add Object lbl_7 As TextBox With Top=259,Left=112,Width=260,Height=22
    Add Object val8 As TextBox With Top=283,Left=5,Width=100,Height=22
    Add Object lbl_8 As TextBox With Top=283,Left=112,Width=260,Height=22
    Add Object val9 As TextBox With Top=307,Left=5,Width=100,Height=22
    Add Object lbl_9 As TextBox With Top=307,Left=112,Width=260,Height=22
    Add Object val10 As TextBox With Top=331,Left=5,Width=100,Height=22
    Add Object lbl_10 As TextBox With Top=331,Left=112,Width=260,Height=22
    Add Object fnt As fontOptCnt With Left=5, Top=360
    Add Object sizes As sizeOptContainer With Left=15, Top= 445

    Proc Init(oObj)
        This.Caption=cp_Translate(MSG_COMBOBOX_OPTIONS)
        This.lbl1.Caption=cp_Translate(MSG_VARIABLE+MSG_FS)
        This.lbl3.Caption=cp_Translate(MSG_TYPE+MSG_FS)
        This.lbl5.Caption=cp_Translate(MSG_LEN_F+MSG_FS)
        This.lbl6.Caption=cp_Translate(MSG_DEC_F+MSG_FS)
        This.lbl12.Caption=cp_Translate(MSG_VALUES)
        This.lbl13.Caption=cp_Translate(MSG_DESCRIPTIONS)
        This.edt.Caption=cp_Translate(MSG_EDITABLE)
        DoDefault(oObj)
    Endproc
    Proc LoadValues()
        This.txt.Value=This.oObj.cVarName
        This.typ.AddItem(cp_Translate(MSG_CHARACTER))
        This.typ.AddItem(cp_Translate(MSG_NUMERIC))
        This.typ.AddItem(cp_Translate(MSG_DATE) )
        This.typ.Value=Iif(This.oObj.cType='N',cp_Translate(MSG_NUMERIC),Iif(This.oObj.cType='D',cp_Translate(MSG_DATE) ,cp_Translate(MSG_CHARACTER) ))
        This.Len.Value=This.oObj.nLen
        This.dec.Value=This.oObj.nDec
        *this.cpt.value=this.oObj.cTitle
        This.edt.Value=This.oObj.cStato='Edit'
        This.val1.Value=This.oObj.cVal1
        This.val2.Value=This.oObj.cVal2
        This.val3.Value=This.oObj.cVal3
        This.val4.Value=This.oObj.cVal4
        This.val5.Value=This.oObj.cVal5
        This.val6.Value=This.oObj.cVal6
        This.val7.Value=This.oObj.cVal7
        This.val8.Value=This.oObj.cVal8
        This.val9.Value=This.oObj.cVal9
        This.val10.Value=This.oObj.cVal10
        This.lbl_1.Value=This.oObj.cLbl1
        This.lbl_2.Value=This.oObj.cLbl2
        This.lbl_3.Value=This.oObj.cLbl3
        This.lbl_4.Value=This.oObj.cLbl4
        This.lbl_5.Value=This.oObj.cLbl5
        This.lbl_6.Value=This.oObj.cLbl6
        This.lbl_7.Value=This.oObj.cLbl7
        This.lbl_8.Value=This.oObj.cLbl8
        This.lbl_9.Value=This.oObj.cLbl9
        This.lbl_10.Value=This.oObj.cLbl10
        *--- Font
        This.fnt.LoadFont(This.oObj)
        *--- Dimensioni e posizione
        This.sizes.LoadSeD(This.oObj)
    Proc SaveValues()
        This.oObj.cVarName=Trim(This.txt.Value)
        This.oObj.cType=Iif(This.typ.Value=cp_Translate(MSG_DATE ),'D',Iif(This.typ.Value=cp_Translate(MSG_NUMERIC),'N','C'))
        This.oObj.nLen=This.Len.Value
        This.oObj.nDec=This.dec.Value
        *this.oObj.cTitle=trim(this.cpt.value)
        This.oObj.cStato=Iif(This.edt.Value,'Edit','Show')
        This.oObj.cVal1=This.val1.Value
        This.oObj.cVal2=This.val2.Value
        This.oObj.cVal3=This.val3.Value
        This.oObj.cVal4=This.val4.Value
        This.oObj.cVal5=This.val5.Value
        This.oObj.cVal6=This.val6.Value
        This.oObj.cVal7=This.val7.Value
        This.oObj.cVal8=This.val8.Value
        This.oObj.cVal9=This.val9.Value
        This.oObj.cVal10=This.val10.Value
        This.oObj.cLbl1=This.lbl_1.Value
        This.oObj.cLbl2=This.lbl_2.Value
        This.oObj.cLbl3=This.lbl_3.Value
        This.oObj.cLbl4=This.lbl_4.Value
        This.oObj.cLbl5=This.lbl_5.Value
        This.oObj.cLbl6=This.lbl_6.Value
        This.oObj.cLbl7=This.lbl_7.Value
        This.oObj.cLbl8=This.lbl_8.Value
        This.oObj.cLbl9=This.lbl_9.Value
        This.oObj.cLbl10=This.lbl_10.Value
        *--- Font
        This.fnt.SaveFont(This.oObj)
        *--- Dimensioni e posizione
        This.sizes.SaveSeD(This.oObj)
Enddefine

Define Class painter_radio_form As painter_cmb_form
    Caption=""
    Proc SaveValues()
        DoDefault()
        Local a
        Dimension a[1]
        This.AddLbl(@a,This.lbl_1.Value)
        This.AddLbl(@a,This.lbl_2.Value)
        This.AddLbl(@a,This.lbl_3.Value)
        This.AddLbl(@a,This.lbl_4.Value)
        This.AddLbl(@a,This.lbl_5.Value)
        This.AddLbl(@a,This.lbl_6.Value)
        This.AddLbl(@a,This.lbl_7.Value)
        This.AddLbl(@a,This.lbl_8.Value)
        This.AddLbl(@a,This.lbl_9.Value)
        This.AddLbl(@a,This.lbl_10.Value)
        This.oObj.SetRadioButtons(@a)
        Return
    Proc AddLbl(a,cLbl)
        If !Empty(cLbl)
            Local L
            L=Alen(a)
            If L>1 Or !Empty(a[1])
                L=L+1
                Dimension a[l]
            Endif
            a[l]=cLbl
        Endif
        Return
    Proc Init(oObj)
        DoDefault(oObj)
        This.Caption=cp_Translate(MSG_RADIO_OPTIONS)
    Endproc
Enddefine

*--- Font Options container
Define Class fontOptCnt As Container
    Width = 229
    Height = 77
    BackStyle = 0
    BorderWidth = 0

    *---
    FntColor=0
    FntBold=.F.
    FntItalic=.F.
    FntUnderline=.F.
    FntStrikeout=.F.
    GlbFont = .F.
    *---

    Add Object fntlbl As Label With ;
        BackStyle = 0, ;
        Caption = "Font Options", ;
        Height = 17, ;
        Left = 3, ;
        Top = 1, ;
        Width = 108, ;
        Name = "FntLbl"

    Add Object fntname As TextBox With ;
        Enabled = .F., ;
        Height = 23, ;
        Left = 1, ;
        Top = 19, ;
        Width = 187, ;
        Name = "FntName"

    Add Object fntsize As TextBox With ;
        Enabled = .F., ;
        Height = 23, ;
        Left = 190, ;
        Top = 19, ;
        Width = 34, ;
        Name = "FntSize"

    Add Object flgglbfnt As Checkbox With ;
        Top = 52, ;
        Left = 1, ;
        Height = 17, ;
        Width = 120, ;
        Alignment = 0, ;
        BackStyle = 0, ;
        Caption = "Global font", ;
        Name = "FlgGlbFnt"

    Add Object fntbtn As CommandButton With ;
        Top = 48, ;
        Left = 144, ;
        Height = 27, ;
        Width = 84, ;
        Caption = "Change font", ;
        Name = "FntBtn"

    Procedure flgglbfnt.Click()
        This.Parent.fntbtn.Enabled=(This.Value=0)
    Endproc

    Procedure fntbtn.Click()
        Dimension aFnt[7]
        aFnt[1] = Alltrim(This.Parent.fntname.Value)
        aFnt[2] = This.Parent.fntsize.Value
        aFnt[3] = This.Parent.FntColor
        aFnt[4] = This.Parent.FntBold
        aFnt[5] = This.Parent.FntItalic
        aFnt[6] = This.Parent.FntUnderline
        aFnt[7] = This.Parent.FntStrikeout
        *--- Scelta font
        If cp_GetFont(@aFnt)
            This.Parent.fntname.Value = Alltrim(aFnt[1])
            This.Parent.fntsize.Value = aFnt[2]
            This.Parent.FntColor = aFnt[3]
            This.Parent.FntBold = aFnt[4]
            This.Parent.FntItalic = aFnt[5]
            This.Parent.FntUnderline = aFnt[6]
            This.Parent.FntStrikeout = aFnt[7]

            This.Parent.fntname.DisabledForeColor = This.Parent.FntColor
            This.Parent.fntname.FontBold = This.Parent.FntBold
            This.Parent.fntname.FontItalic = This.Parent.FntItalic
            This.Parent.fntname.FontUnderline = This.Parent.FntUnderline
            This.Parent.fntname.FontStrikethru = This.Parent.FntStrikeout
        Endif
    Endproc

    Procedure SaveFont(oObj)
        oObj.FontName = This.fntname.Value
        oObj.FontSize = This.fntsize.Value
        oObj.FontColor = This.FntColor
        oObj.FontBold = This.FntBold
        oObj.FontItalic = This.FntItalic
        oObj.FontUnderline = This.FntUnderline
        oObj.FontStrikeout = This.FntStrikeout
        This.GlbFont = This.flgglbfnt.Value=1
        oObj.GlbFont = This.GlbFont

    Procedure LoadFont(oObj)
        This.fntname.Value = m.oObj.FontName
        This.fntsize.Value = m.oObj.FontSize
        This.FntColor = m.oObj.FontColor
        This.FntBold = m.oObj.FontBold
        This.FntItalic = m.oObj.FontItalic
        This.FntUnderline = m.oObj.FontUnderline
        This.FntStrikeout = m.oObj.FontStrikeout
        This.GlbFont = oObj.GlbFont
        This.flgglbfnt.Value = Iif(This.GlbFont, 1, 0)

        This.fntname.DisabledForeColor = This.FntColor
        This.fntname.FontBold = This.FntBold
        This.fntname.FontItalic = This.FntItalic
        This.fntname.FontUnderline = This.FntUnderline
        This.fntname.FontStrikethru = This.FntStrikeout
    Endproc
Enddefine

Define Class sizeOptContainer As Container
    Width = 220
    Height = 110
    BackStyle = 0
    BorderWidth = 0

    Add Object headerLbl As Label With ;
        BackStyle = 0, ;
        Caption = "Size and positions", ;
        Height = 17, ;
        Left = 3, ;
        Top = 1, ;
        Width = 170, ;
        Name = "headerLbl"

    Add Object lineheader As Line With ;
        Top = 19, ;
        Left = 2, ;
        Width = 180, ;
        Height = 0

    Add Object topLbl As Label With ;
        BackStyle = 0, ;
        Caption = "Top", ;
        Height = 17, ;
        Left = 3, ;
        Top = 20, ;
        Width = 108, ;
        Name = "TopLbl"

    Add Object topText As TextBox With ;
        Enabled = .T., ;
        Height = 23, ;
        Left = 3, ;
        Top = 39, ;
        Width = 60, ;
        Name = "topText", ;
        InputMask = "9999"

    Add Object leftLbl As Label With ;
        BackStyle = 0, ;
        Caption = "Left", ;
        Height = 17, ;
        Left = 120, ;
        Top = 20, ;
        Width = 108, ;
        Name = "leftLbl"

    Add Object leftText As TextBox With ;
        Enabled = .T., ;
        Height = 23, ;
        Left = 120, ;
        Top = 39, ;
        Width = 60, ;
        Name = "leftText", ;
        InputMask = "9999"

    Add Object heightLbl As Label With ;
        BackStyle = 0, ;
        Caption = "Height", ;
        Height = 17, ;
        Left = 3, ;
        Top = 64, ;
        Width = 108, ;
        Name = "heightLbl"

    Add Object heightText As TextBox With ;
        Enabled = .T., ;
        Height = 23, ;
        Left = 3, ;
        Top = 85, ;
        Width = 60, ;
        Name = "heightText", ;
        InputMask = "9999"

    Add Object widthLbl As Label With ;
        BackStyle = 0, ;
        Caption = "Width", ;
        Height = 17, ;
        Left = 120, ;
        Top = 64, ;
        Width = 108, ;
        Name = "widthLbl"

    Add Object widthText As TextBox With ;
        Enabled = .T., ;
        Height = 23, ;
        Left = 120, ;
        Top = 85, ;
        Width = 60, ;
        Name = "widthText", ;
        InputMask = "9999"

    Procedure Init
        With This
            .headerLbl.Caption = cp_Translate(MSG_POS_AND_DIMENSION)
            .topLbl.Caption = cp_Translate(MSG_POS_TOP)
            .leftLbl.Caption = cp_Translate(MSG_POS_LEFT)
            .heightLbl.Caption = cp_Translate(MSG_DIM_HEIGHT)
            .widthLbl.Caption = cp_Translate(MSG_DIM_WIDTH)
        Endwith
    Endproc

    Procedure LoadSeD(oObj)
        This.topText.Value = m.oObj.Top
        This.leftText.Value = m.oObj.Left
        This.heightText.Value = m.oObj.Height
        This.widthText.Value = m.oObj.Width
    Endproc

    Procedure SaveSeD(oObj)
    	*--- Zucchetti Aulla inizio
*!*	        m.oObj.Top = This.topText.Value
*!*	        m.oObj.Left = This.leftText.Value
*!*	        m.oObj.Height = This.heightText.Value
*!*	        m.oObj.Width = This.widthText.Value
		m.oObj.Move(This.leftText.Value, This.topText.Value, This.widthText.Value, This.heightText.Value)
		*--- Zucchetti Aulla fine
        m.oObj.RemoveHandlers()
        m.oObj.AddHandlers()
    Endproc

Enddefine

Define Class painter_AlphaBlend_form As painter_form
    Top=73
    Left=158
    Height = 280
    Width = 230
    Caption = "Alpha Blending"
    WindowType = 1

    Add Object SpnN As Spinner With Height=24, Left=156, SpinnerHighValue=255, SpinnerLowValue=0, Top=12, Width=60, InputMask="999"
    Add Object SpnH As Spinner With Height=24, Left=156, SpinnerHighValue=255, SpinnerLowValue=0, Top=42, Width=60, InputMask="999"
    Add Object SpnD As Spinner With Height=24, Left=156, SpinnerHighValue=255, SpinnerLowValue=0, Top=72, Width=60, InputMask="999"
    Add Object brdWidth As Spinner With Height=24, Left=156, SpinnerLowValue=1, Top=102, Width=60, InputMask="999"
    Add Object Radius As Spinner With Height=24, Left=156, SpinnerLowValue=1, Top=132, Width=60, InputMask="999"
    Add Object lbl1 As Label With Alignment=1, Caption="Normal", Height=17, Left=12, Top=15, Width=136, BackStyle=0
    Add Object lbl2 As Label With Alignment=1, Caption="Mouse Hover", Height=17, Left=12, Top=45, Width=136, BackStyle=0
    Add Object lbl3 As Label With Alignment=1, Caption="Mouse Down", Height=17, Left=12, Top=75, Width=136, BackStyle=0
    Add Object Lbl4 As Label With Alignment=1, Caption="Border width", Height=17, Left=12, Top=105, Width=136, BackStyle=0
    Add Object lbl5 As Label With Alignment=1, Caption="Curvature", Height=17, Left=12, Top=135, Width=136, BackStyle=0
    Add Object lbl6 As Label With Alignment=1, Caption="Border color", Height=17, Left=12, Top=165, Width=136, BackStyle=0
    Add Object lbl7 As Label With Alignment=1, Caption="Back color", Height=17, Left=12, Top=195, Width=136, BackStyle=0
    Add Object shp1 As Shape With Top=165,Left=156,Width=16,Height=16
    Add Object shp2 As Shape With Top=195,Left=156,Width=16,Height=16

    Proc Getcolor(oShp)
        Local l_color
        l_color = Getcolor(m.oShp.BackColor)
        m.oShp.BackColor=Iif(l_color<>-1, l_color, m.oShp.BackColor)

    Proc Init(oObj)
        This.Caption=cp_Translate(MSG_ADVANCED)
        This.lbl1.Caption=cp_Translate(MSG_NOSELECTED)+MSG_FS
        This.lbl2.Caption=cp_Translate(MSG_MOUSEHOVER)+MSG_FS
        This.lbl3.Caption=cp_Translate(MSG_MOUSEDOWN)+MSG_FS
        This.Lbl4.Caption=cp_Translate(MSG_BORDERWIDTH)+MSG_FS
        This.lbl5.Caption=cp_Translate(MSG_CURVATURE)+MSG_FS
        This.lbl6.Caption=cp_Translate(MSG_BORDERCOLOR)+MSG_FS
        This.lbl7.Caption=cp_Translate(MSG_BACKGROUND_COLOR)+MSG_FS
        DoDefault(oObj)
    Endproc

    Proc LoadValues()
        This.SpnN.Value=This.oObj.nAlpha
        This.SpnH.Value=This.oObj.nAlphaH
        This.SpnD.Value=This.oObj.nAlphaD
        This.brdWidth.Value=This.oObj.nBrdWidth
        This.Radius.Value=This.oObj.nRadius
        This.shp1.BackColor = This.oObj.nBorderColor
        This.shp2.BackColor = This.oObj.nBackColor
    Endproc

    Proc SaveValues()
        This.oObj.nAlpha=This.SpnN.Value
        This.oObj.nAlphaH=This.SpnH.Value
        This.oObj.nAlphaD=This.SpnD.Value
        This.oObj.nBrdWidth=This.brdWidth.Value
        This.oObj.nRadius = This.Radius.Value
        This.oObj.nBorderColor = This.shp1.BackColor
        This.oObj.nBackColor = This.shp2.BackColor
    Endproc

    Procedure shp1.DblClick()
        This.Parent.Getcolor(This)

    Procedure shp2.DblClick()
        This.Parent.Getcolor(This)

Enddefine

*--- Line controller
Define Class painter_node_form As painter_form

    Top = 0
    Left = 0
    Height = 214
    Width = 375
    DoCreate = .T.
    Caption = "Line Option"

    Add Object lbl1 As Label With Alignment=1, BackStyle=0, Caption="Border Color:", Height=17, Left=24, Top=15, Width=84
    Add Object lbl2 As Label With Alignment=1, BackStyle=0, Caption="Border Width:", Height=17, Left=24, Top=41, Width=84
    Add Object lbl3 As Label With Alignment=1, BackStyle=0, Caption="Start Cap:", Height=17, Left=24, Top=64, Width=84
    Add Object Lbl4 As Label With Alignment=1, BackStyle=0, Caption="End Cap:", Height=17, Left=24, Top=90, Width=84
    Add Object lbl5 As Label With Alignment=1, BackStyle=0, Caption="Dash Style:", Height=17, Left=24, Top=116, Width=84
    Add Object lbl6 As Label With Alignment=1, BackStyle=0, Caption="Curvature:", Height=17, Left=24, Top=142, Width=84

    Add Object shp1 As Shape With Top=15,Left=111,Width=16,Height=16
    Add Object brdWidth As Spinner With Height=22, KeyboardHighValue=99, KeyboardLowValue=1, Left=111, Top=38, Width=121, InputMask="99"
    Add Object startcap As ComboBox With Height=22, Left=111, Top=64, Width=144
    Add Object endcap As ComboBox With Height=22, Left=111, Top=90, Width=144
    Add Object dashstyle As ComboBox With Height=22, Left=111, Top=116, Width=144
    Add Object Curvature As Spinner With Height=22, KeyboardHighValue=99, KeyboardLowValue=1, Left=111, Top=142, Width=121, InputMask="99"

    Proc Getcolor(oShp)
        Local l_color
        l_color = Getcolor(m.oShp.BackColor)
        m.oShp.BackColor=Iif(l_color<>-1, l_color, m.oShp.BackColor)

    Proc Init(oObj)
        This.lbl1.Caption=cp_Translate(MSG_BORDERCOLOR)+MSG_FS
        This.lbl2.Caption=cp_Translate(MSG_BORDERWIDTH)+MSG_FS
        This.lbl3.Caption=cp_Translate(MSG_STARTCAP)+MSG_FS
        This.Lbl4.Caption=cp_Translate(MSG_ENDCAP)+MSG_FS
        This.lbl5.Caption=cp_Translate(MSG_DASHSTYLE)+MSG_FS
        This.lbl6.Caption=cp_Translate(MSG_CURVATURE)+MSG_FS
        DoDefault(oObj)

    Proc LoadValues()
        With This
            .startcap.AddItem(cp_Translate(MSG_NONEARROW))
            .startcap.AddItem(cp_Translate(MSG_ARROW))
            .startcap.AddItem(cp_Translate(MSG_HOLLOWARROW))
            .startcap.AddItem(cp_Translate(MSG_OPENARROW))

            .endcap.AddItem(cp_Translate(MSG_NONEARROW))
            .endcap.AddItem(cp_Translate(MSG_ARROW))
            .endcap.AddItem(cp_Translate(MSG_HOLLOWARROW))
            .endcap.AddItem(cp_Translate(MSG_OPENARROW))

            .dashstyle.AddItem(cp_Translate(MSG_SOLID))
            .dashstyle.AddItem(cp_Translate(MSG_DASHED))
            .dashstyle.AddItem(cp_Translate(MSG_DOTTED))

            .shp1.BackColor = .oObj.nBorderColor
            .brdWidth.Value = .oObj.nBorderWidth

            .startcap.Value = Icase(.oObj.nStartCap=1, cp_Translate(MSG_ARROW),;
                .oObj.nStartCap=2, cp_Translate(MSG_HOLLOWARROW),;
                .oObj.nStartCap=3, cp_Translate(MSG_OPENARROW),;
                cp_Translate(MSG_NONEARROW))
            .endcap.Value = Icase(.oObj.nEndCap=1, cp_Translate(MSG_ARROW),;
                .oObj.nEndCap=2, cp_Translate(MSG_HOLLOWARROW),;
                .oObj.nEndCap=3, cp_Translate(MSG_OPENARROW),;
                cp_Translate(MSG_NONEARROW))
            .dashstyle.Value = Icase(.oObj.nDashStyle=1, cp_Translate(MSG_DASHED),;
                .oObj.nDashStyle=2, cp_Translate(MSG_DOTTED),;
                cp_Translate(MSG_SOLID))
            .Curvature.Value = .oObj.nCurvature
        Endwith

    Proc SaveValues()
        With This
            .oObj.nStartCap = Icase(.startcap.Value==cp_Translate(MSG_ARROW), 1,;
                .startcap.Value==cp_Translate(MSG_HOLLOWARROW), 2,;
                .startcap.Value==cp_Translate(MSG_OPENARROW), 3,;
                0)
            .oObj.nEndCap = Icase(.endcap.Value==cp_Translate(MSG_ARROW), 1,;
                .endcap.Value==cp_Translate(MSG_HOLLOWARROW), 2,;
                .endcap.Value==cp_Translate(MSG_OPENARROW), 3,;
                0)
            .oObj.nDashStyle = Icase(.dashstyle.Value==cp_Translate(MSG_DASHED), 1,;
                .dashstyle.Value==cp_Translate(MSG_DOTTED), 2,;
                0)
            .oObj.nCurvature = .Curvature.Value
            .oObj.nBorderColor = .shp1.BackColor
            .oObj.nBorderWidth = .brdWidth.Value
        Endwith
    Procedure shp1.DblClick()
        This.Parent.Getcolor(This)

Enddefine

Define Class painter_btn_form As painter_form
    Top=73
    Left=158
    Width=370
    Height=375
    Caption=""
    Add Object lbl1 As Label With Caption="",Alignment=1,Top=19,Left=28,Width=50,Height=20, BackStyle=0
    Add Object prg As TextBox With Top=16,Left=86,Width=240,Height=23, MaxLength=255
    Add Object lbl3 As Label With Caption="",Alignment=1,Top=45,Left=28,Width=50,Height=20, BackStyle=0
    Add Object tip As ComboBox With Top=43,Left=86,Width=150,Height=20,Margin=1
    Add Object lbl5 As Label With Caption="",Alignment=1,Top=70,Left=28,Width=50,Height=20, BackStyle=0
    Add Object tit As TextBox With Top=67,Left=86,Width=135,Height=21, MaxLength=255
    Add Object lbl7 As Label With Caption="",Alignment=1,Top=95,Left=28,Width=50,Height=20, BackStyle=0
    Add Object bmp As TextBox With Top=92,Left=86,Width=100,Height=20, MaxLength=255
    Add Object Dir As CommandButton With Caption="",Top=92,Left=190,Width=15,Height=19
    Add Object inv As Checkbox With Caption="",Top=95,Left=225,Width=140,Visible=.F., BackStyle=0

    Add Object lbl8 As Label With Caption="",Alignment=1, Top=120, Left=28, Width=50, Height=20, BackStyle=0
    Add Object shp As ComboBox With Top=117, Left=86, Width=150, Height=20, Margin=1
    Add Object btnAlpha As CommandButton With Caption="",Top=115,Left=250,Width=105,Height=25

    Add Object lbl9 As Label With Caption="",Alignment=1,Top=147,Left=28,Width=50,Height=20, BackStyle=0
    Add Object editCond As TextBox With Top=146,Left=86,Width=240,Height=21, MaxLength=255

    Add Object fnt As fontOptCnt With Left=10, Top=172
    Add Object sizes As sizeOptContainer With Left=15, Top= 253

    Proc Init(oObj)
        This.Caption=cp_Translate(MSG_BUTTON_BS_BITMAP_IMAGE_OPTIONS)
        This.lbl1.Caption=cp_Translate(MSG_EXECUTE+MSG_FS)
        This.lbl3.Caption=cp_Translate(MSG_TYPE+MSG_FS)
        This.lbl5.Caption=cp_Translate(MSG_TITLE+MSG_FS)
        This.lbl7.Caption=cp_Translate(MSG_BITMAP+MSG_FS)
        This.lbl8.Caption=cp_Translate(MSG_SHAPE+MSG_FS)
        This.lbl9.Caption=cp_Translate(MSG_EDITING+MSG_FS)
        This.Dir.Caption=cp_Translate(MSG_D)
        This.inv.Caption=cp_Translate(MSG_INVISIBLE)
        This.btnAlpha.Caption=cp_Translate(MSG_ADVANCED)
        DoDefault(oObj)
    Endproc

    Proc LoadValues()
        This.tit.Value=This.oObj.cTitle
        This.tip.AddItem(cp_Translate(MSG_PROGRAM))
        This.tip.AddItem(cp_Translate(MSG_DIALOG_WINDOW))
        This.tip.AddItem(cp_Translate(MSG_QUERY_BS_REPORT))
        This.tip.AddItem(cp_Translate(MSG_ZOOM))
        This.tip.AddItem(cp_Translate(MSG_VFM_HOME))
        This.tip.AddItem(cp_Translate(MSG_VFM_DRILLDOWN))
        This.tip.AddItem(cp_Translate(MSG_VFM_DRILLUP))
        This.tip.Value=	Iif(This.oObj.cType='PRG',cp_Translate(MSG_PROGRAM),;
            Iif(This.oObj.cType='MSK',cp_Translate(MSG_DIALOG_WINDOW),;
            Iif(This.oObj.cType='ZOOM',cp_Translate(MSG_ZOOM),;
            IIF(This.oObj.cType='DOWN', cp_Translate(MSG_VFM_DRILLDOWN),;
            IIF(This.oObj.cType='UP', cp_Translate(MSG_VFM_DRILLUP),;
            IIF(This.oObj.cType='HOME', cp_Translate(MSG_VFM_HOME),;
            cp_Translate(MSG_QUERY_BS_REPORT)))))))
        This.prg.Value=This.oObj.cPrg
        This.bmp.Value=This.oObj.cPicture
        Do Case
            Case Lower(This.oObj.Class)='painter_btn'
                This.inv.Value=This.oObj.nStyle
                This.inv.Visible=.T.
                This.shp.Visible=.F.
                This.lbl8.Visible=.F.
                This.btnAlpha.Visible=.F.
                *--- Font
                This.fnt.LoadFont(This.oObj)
                *--- Dimensioni e posizione
                This.sizes.LoadSeD(This.oObj)
                This.lbl9.Visible=.F.
                This.editCond.Visible=.F.
            Case Lower(This.oObj.Class)='painter_mediabtn'
                This.inv.Value=This.oObj.nStyle
                This.inv.Visible=.T.
                This.inv.Caption = cp_Translate(MSG_EXECUTEDBLCLICK)

                This.shp.AddItem("Process")
                This.shp.AddItem("Decision")
                This.shp.AddItem("Document")
                This.shp.AddItem("MultipleDocuments")
                This.shp.AddItem("PredefinedProcess")
                This.shp.AddItem("Terminator")
                This.shp.AddItem("ManualInput")
                This.shp.AddItem("Preparation")
                This.shp.AddItem("PaperTape")
                This.shp.AddItem("Database")
                This.shp.AddItem("ManualOperation")
                This.shp.AddItem("Merge")
                This.shp.AddItem("Connector")
                This.shp.AddItem("ArrowUp")
                This.shp.AddItem("ArrowDown")
                This.shp.AddItem("ArrowLeft")
                This.shp.AddItem("ArrowRight")
                This.shp.AddItem("Cloud")
                This.shp.Value = This.oObj.cShape
                This.editCond.Value = This.oObj.cEditCond

                *--- Font
                This.fnt.LoadFont(This.oObj)
                *--- Dimensioni e posizione
                This.sizes.LoadSeD(This.oObj)
            Case Lower(This.oObj.Class)='painter_tilebtn'
                This.inv.Value=This.oObj.nStyle
                This.inv.Visible=.T.
                This.inv.Caption = cp_Translate(MSG_EXECUTEDBLCLICK)

                This.shp.AddItem("Square tile")
                This.shp.AddItem("Large tile")
                This.shp.AddItem("Small tile")
                This.shp.Value = This.oObj.cShape
				This.shp.enabled=.f.
                This.editCond.Value = This.oObj.cEditCond

                *--- Font
                This.fnt.LoadFont(This.oObj)
                *--- Dimensioni e posizione
                This.sizes.LoadSeD(This.oObj)
				This.sizes.heightText.enabled=.f.
				This.sizes.widthText.enabled=.f.
            Case Lower(This.oObj.Class)='painter_bitmap'
                This.fnt.Visible=.F.
                This.shp.Visible=.F.
                This.lbl8.Visible=.F.
                This.btnAlpha.Visible=.F.
                This.lbl9.Visible=.F.
                This.editCond.Visible=.F.
                This.Height=260
                This.sizes.Top = 130

                *--- Dimensioni e posizione
                This.sizes.LoadSeD(This.oObj)
        Endcase

    Proc SaveValues()
        This.oObj.cTitle=Trim(This.tit.Value)
        This.oObj.cType=Iif(This.tip.Value=cp_Translate(MSG_PROGRAM),'PRG',;
            Iif(This.tip.Value=cp_Translate(MSG_DIALOG_WINDOW),'MSK',;
            Iif(This.tip.Value=cp_Translate(MSG_ZOOM),'ZOOM',;
            IIF(This.tip.Value=cp_Translate(MSG_VFM_DRILLDOWN),'DOWN',;
            IIF(This.tip.Value=cp_Translate(MSG_VFM_DRILLUP),'UP',;
            IIF(This.tip.Value=cp_Translate(MSG_VFM_HOME),'HOME',;
            'QRY'))))))
        This.oObj.cPrg=Trim(This.prg.Value)
        This.oObj.cPicture=Trim(This.bmp.Value)
        This.oObj.obj.Picture=Trim(This.bmp.Value)

        If Lower(This.oObj.Class)='painter_btn'
            This.oObj.nStyle=This.inv.Value
            This.oObj.obj.Style=This.inv.Value
        Endif
        If Lower(This.oObj.Class)='painter_mediabtn'
            This.oObj.nStyle=This.inv.Value
            This.oObj.cShape=This.shp.Value
            This.oObj.cEditCond=This.editCond.Value
        Endif
        If Lower(This.oObj.Class)='painter_tilebtn'
            This.oObj.nStyle=This.inv.Value
            This.oObj.cShape=This.shp.Value
            This.oObj.cEditCond=This.editCond.Value
        Endif

        *--- Font
        If Lower(This.oObj.Class)!='painter_bitmap'
            This.fnt.SaveFont(This.oObj)
        Endif
        *--- Dimensioni e posizione
        This.sizes.SaveSeD(This.oObj)

    Proc Dir.Click()
        This.Parent.bmp.Value=Getfile("Image Files:JPG,BMP,GIF,TIF,PCX,PNG")
        If Upper(This.Parent.bmp.Value)=Upper(Sys(5)+Curdir())
            This.Parent.bmp.Value=Substr(This.Parent.bmp.Value,Len(Upper(Sys(5)+Curdir()))+1)
        Endif

    Proc btnAlpha.Click()
        Local l_Form
        l_Form = Createobject("painter_AlphaBlend_form", This.Parent.oObj)
        l_Form.Show()

Enddefine

Define Class vm_frm_bck_image As Image
    Stretch=2
    cVarName='.-.'

    Procedure Setup(cBckImage, cGradient, nFColor, nLColor, nGradientType, nWidth, nHeight, bForced)
        *---Background Image
        Local l_Visible,l_PictureVal
        l_Visible=.F.
        l_PictureVal=""
        IF (Empty(This.PictureVal) OR bForced)
          If !Empty(m.cBckImage) And cp_FileExist(m.cBckImage)
            If m.cGradient='S'
                *--- Sfumatura verso lo sfondo
                   l_PictureVal= cb_ImageGradient(cBckImage, nWidth, nHeight, _Screen.BackColor, VFM_HGRD)
            Else
                *--- Solo immagine
                   l_PictureVal=Filetostr(m.cBckImage)
            Endif
            l_Visible=.T.
          Else
            If m.cGradient='S'
                *--- Gradiente di sfondo
                l_PictureVal=This.CreateGradient(m.nFColor, m.nLColor, m.nGradientType, m.nWidth, m.nHeight)
                l_Visible=.T.
            Endif
          ENDIF
          This.PictureVal=l_PictureVal
          This.Visible=l_Visible
        ENDIF

    Function CreateGradient(nColor1, nColor2, nType, nWidth, nHeight)
        Return cb_CreateGradient(nColor1, nColor2, nType, nWidth, nHeight)

    Func GetValue()
        Return('')
	
	PROCEDURE PictureVal_Assign(xValue)
		LockWindowUpdate(_Screen.HWnd)
		This.PictureVal = m.xValue
		LockWindowUpdate(0)
	
	*** Zucchetti Aulla - gestione resize immagine desktop
	Procedure DesktopResize()
		If lower(this.Name)='bckimg' AND NOT(Vartype(_Screen.Cnt)<>"U" AND _Screen.Cnt.cScreen='FullScreen')
			this.Move(this.Left,this.Top,_screen.Width,_screen.Height)
			this.Setup("", 'S', RGB(255,255,255), iif(_Screen.cp_Themesmanager.GetSingleProp(54, 2)<_Screen.cp_Themesmanager.GetSingleProp(54, 3),_Screen.cp_Themesmanager.GetSingleProp(54, 2),_Screen.cp_Themesmanager.GetSingleProp(54, 3)), 1, _screen.Width, _screen.Height)
			_Screen.ahlogo.Move((_screen.Width-_Screen.ahlogo.Width)/2,(_screen.Height-_Screen.ahlogo.Height)/2)
		ENDIF
	Endproc
ENDDEFINE

PROCEDURE freeScreenSpace(pLeft, pTop, pWidth, pHeight)
	*-- controllo se � presente e visualizzato il Desktop Menu, in tal caso lo spazio libero va diminuito
	IF TYPE('i_oDeskMenu')='O' AND NOT ISNULL(i_oDeskMenu) AND i_oDeskMenu.Visible
		pLeft = i_oDeskMenu.Width
		pWidth = _screen.Width - i_oDeskMenu.Width
	ELSE
		pLeft = 0
		pWidth = _screen.Width
	ENDIF
	pTop = 0
	pHeight = _screen.Height
ENDPROC 

