* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: VRT_SWITCH
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  56
* ----------------------------------------------------------------------------
* procedura per lo switch di runtime
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

If Vartype(bLoadRuntimeConfig)='U'
    Public bLoadRuntimeConfig
Else
    bLoadRuntimeConfig=!bLoadRuntimeConfig
Endif
If bLoadRuntimeConfig
    CP_MSG(cp_Translate(MSG_RUNTIME_CONFIGURATION_ENABLED),.F.)
Else
    CP_MSG(cp_Translate(MSG_RUNTIME_CONFIGURATION_DISABLED),.F.)
Endif
