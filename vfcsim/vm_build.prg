* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VM_BUILD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Disegnatore di maschere
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

#Define ID_VFM 9999999991

*---Zucchetti Aulla Inizio Monitor framework
If cp_set_get_right()<2
    cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
    Return
Endif
*---Zucchetti Aulla Fine Monitor framework

If At('vm_lib',Lower(Set('proc')))=0 Or At('cp_draw',Lower(Set('proc')))=0 Or At('vg_lib',Lower(Set('proc')))=0 Or At('vm_exec',Lower(Set('proc')))=0
    Set Proc To vm_lib,cp_draw,vq_lib,vm_exec Additive
Endif


o=Createobject('mask_toolbar', Vartype(_Screen.cp_ThemesManager)=="O" And i_VisualTheme<>-1)
i_curform=o
*---Zucchetti Aulla Inizio Monitor framework
If (Vartype(i_designfile)='C')
    o.cFileName = Alltrim(i_designfile)
    o.SetTitle()
    o.LoadDoc()
Endif
*---Zucchetti Aulla Fine Monitor framework
o.EditMask()

Define Class mask_toolbar As microtool_toolbar
    oForm=.Null.
    oThis=.Null.
    HelpContextID=2
    cToolName='Visual Mask'
    cExt='vfm'
    cClipBoard=''
    Caption=""
    *
    bSec1=.T.
    bSec2=.T.
    bSec3=.T.
    bSec4=.T.
    *
    bFox26=.F.

    Proc Init(bStyle)
        DoDefault(m.bStyle)
        Local nDimension
        With This
            If Type("i_bFox26")='L'
                If i_bFox26
                    .bFox26=.T.
                Endif
            Endif
            If !.bFox26
                cp_GetSecurity(This,'VisualMsk')
            Endif
            .oForm=Createobject('vm_frm')
            .oForm.oTool=This
            .Dock(i_nCPToolBarPos)
            .oThis=This
            .Caption=cp_Translate(MSG_DIALOG_WINDOW_PAINTER)
            *--- Separatore
            .AddObject("sep1","Separator")
            #If Version(5)>=900
                .sep1.Visible=.T.
            #Endif
            *--- Test Btn
            Local oBtn
            m.oBtn = .AddButton("test")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "run.bmp"
            m.oBtn._OnClick = "This.parent.test_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_VFM_TEST)
            m.oBtn.Visible=.T.
            *--- Option Btn
            Local oBtn
            m.oBtn = .AddButton("opt")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "opz.bmp"
            m.oBtn._OnClick = "This.parent.opt_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_DIALOG_WINDOW_OPTIONS)
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("sep4","Separator")
            #If Version(5)>=900
                .sep4.Visible=.T.
            #Endif
            *--- Label Btn
            m.oBtn = .AddButton("lbl", Iif(m.bStyle, "draw_adder_cb", "draw_adder"))
            m.oBtn.Caption = ""
            m.oBtn.cImage = "label.bmp"
            m.oBtn._OnClick = "This.parent.draw_adder_Click(this)"
            m.oBtn.ToolTipText = cp_Translate(MSG_STRING)
            m.oBtn.cType = 'painter_lbl'
            m.oBtn.Visible=.T.
            *--- Text Btn
            m.oBtn = .AddButton("txt", Iif(m.bStyle, "draw_adder_cb", "draw_adder"))
            m.oBtn.Caption = ""
            m.oBtn.cImage = "textbox.bmp"
            m.oBtn._OnClick = "This.parent.draw_adder_Click(this)"
            m.oBtn.ToolTipText = cp_Translate(MSG_VARIABLE)
            m.oBtn.cType = 'painter_txt'
            m.oBtn.Visible=.T.
            *--- Radio Btn
            m.oBtn = .AddButton("rad", Iif(m.bStyle, "draw_adder_cb", "draw_adder"))
            m.oBtn.Caption = ""
            m.oBtn.cImage = "radio.bmp"
            m.oBtn._OnClick = "This.parent.draw_adder_Click(this)"
            m.oBtn.ToolTipText = cp_Translate(MSG_RADIO)
            m.oBtn.cType = 'painter_radio'
            m.oBtn.Visible=.T.
            *--- Combo Btn
            m.oBtn = .AddButton("cmb", Iif(m.bStyle, "draw_adder_cb", "draw_adder"))
            m.oBtn.Caption = ""
            m.oBtn.cImage = "combobox.bmp"
            m.oBtn._OnClick = "This.parent.draw_adder_Click(this)"
            m.oBtn.ToolTipText = cp_Translate(MSG_COMBOBOX)
            m.oBtn.cType = 'painter_combo'
            m.oBtn.Visible=.T.
            *--- CheckBox Btn
            m.oBtn = .AddButton("chk", Iif(m.bStyle, "draw_adder_cb", "draw_adder"))
            m.oBtn.Caption = ""
            m.oBtn.cImage = "checkbox.bmp"
            m.oBtn._OnClick = "This.parent.draw_adder_Click(this)"
            m.oBtn.ToolTipText = cp_Translate(MSG_CHECKBOX)
            m.oBtn.cType = 'painter_check'
            m.oBtn.Visible=.T.
            *--- Button Btn
            m.oBtn = .AddButton("btn", Iif(m.bStyle, "draw_adder_cb", "draw_adder"))
            m.oBtn.Caption = ""
            m.oBtn.cImage = "button.bmp"
            m.oBtn._OnClick = "This.parent.draw_adder_Click(this)"
            m.oBtn.ToolTipText = cp_Translate(MSG_BUTTON)
            m.oBtn.cType = 'painter_btn'
            m.oBtn.Visible=.T.
            *--- Button MediaBtn
            m.oBtn = .AddButton("mbtn", Iif(m.bStyle, "draw_adder_cb", "draw_adder"))
            m.oBtn.Caption = ""
            m.oBtn.cImage = "mbutton.bmp"
            m.oBtn._OnClick = "This.parent.draw_adder_Click(this)"
            m.oBtn.ToolTipText = cp_Translate(MSG_MEDIABUTTON)
            m.oBtn.cType = 'painter_mediabtn'
            m.oBtn.Visible=.T.
            If i_VisualTheme <> -1
                Local oBtnMenu, oMenuItem
                m.oBtnMenu = Createobject("cbPopupMenu")
                m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("Process")
                l_tmp = '"cShape=[Process]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Picture = "process.ico"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("Decision")
                l_tmp = '"cShape=[Decision]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Picture = "Decision.ico"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("Document")
                m.oMenuItem.Picture = "Document.ico"
                l_tmp = '"cShape=[Document]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("MultipleDocuments")
                m.oMenuItem.Picture = "MultiDoc.ico"
                l_tmp = '"cShape=[MultipleDocuments]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("PredefinedProcess")
                m.oMenuItem.Picture = "PredProcess.ico"
                l_tmp = '"cShape=[PredefinedProcess]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("Terminator")
                m.oMenuItem.Picture = "Terminator.ico"
                l_tmp = '"cShape=[Terminator]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("ManualInput")
                m.oMenuItem.Picture = "ManualInput.ico"
                l_tmp = '"cShape=[ManualInput]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("Preparation")
                m.oMenuItem.Picture = "Preparation.ico"
                l_tmp = '"cShape=[Preparation]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("PaperTape")
                m.oMenuItem.Picture = "PaperTape.ico"
                l_tmp = '"cShape=[PaperTape]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("Database")
                m.oMenuItem.Picture = "Database.ico"
                l_tmp = '"cShape=[Database]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("ManualOperation")
                m.oMenuItem.Picture = "ManualOperation.ico"
                l_tmp = '"cShape=[ManualOperation]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("Merge")
                m.oMenuItem.Picture = "Merge.ico"
                l_tmp = '"cShape=[Merge]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("Connector")
                m.oMenuItem.Picture = "Connector.ico"
                l_tmp = '"cShape=[Connector]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("ArrowUp")
                m.oMenuItem.Picture = "ArrowUp.ico"
                l_tmp = '"cShape=[ArrowUp]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("ArrowDown")
                m.oMenuItem.Picture = "ArrowDown.ico"
                l_tmp = '"cShape=[ArrowDown]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("ArrowLeft")
                m.oMenuItem.Picture = "ArrowLeft.ico"
                l_tmp = '"cShape=[ArrowLeft]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("ArrowRight")
                m.oMenuItem.Picture = "ArrowRight.ico"
                l_tmp = '"cShape=[ArrowRight]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oMenuItem = m.oBtnMenu.AddMenuItem()
                m.oMenuItem.Caption = cp_Translate("Cloud")
                m.oMenuItem.Picture = "Cloud.ico"
                l_tmp = '"cShape=[Cloud]"'
                m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
                m.oMenuItem.Visible=.T.
                m.oBtnMenu.InitMenu()
                m.oBtn.oPopupMenu = m.oBtnMenu
                m.oBtn.nButtonType = 2
            Endif
            *--- Button Tile
			if .f.
				m.oBtn = .AddButton("tbtn", Iif(m.bStyle, "draw_adder_cb", "draw_adder"))
				m.oBtn.Caption = ""
				m.oBtn.cImage = "mbutton.bmp"
				m.oBtn._OnClick = "This.parent.draw_adder_Click(this)"
				m.oBtn.ToolTipText = cp_Translate(MSG_TILEBUTTON)
				m.oBtn.cType = 'painter_tilebtn'
				m.oBtn.Visible=.T.
				If i_VisualTheme <> -1
					Local oBtnMenu, oMenuItem
					m.oBtnMenu = Createobject("cbPopupMenu")
					m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
					m.oMenuItem = m.oBtnMenu.AddMenuItem()
					m.oMenuItem.Caption = cp_Translate("Square tile")
					l_tmp = '"cShape=[Square tile]"'
					m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
					m.oMenuItem.Picture = "process.ico"
					m.oMenuItem.Visible=.T.
					m.oMenuItem = m.oBtnMenu.AddMenuItem()
					m.oMenuItem.Caption = cp_Translate("Large tile")
					m.oMenuItem.Picture = "Terminator.ico"
					l_tmp = '"cShape=[Large tile]"'	
					m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
					m.oMenuItem.Visible=.T.
					m.oMenuItem = m.oBtnMenu.AddMenuItem()
					m.oMenuItem.Caption = cp_Translate("Small tile")
					m.oMenuItem.Picture = "Terminator.ico"
					l_tmp = '"cShape=[Small tile]"'	
					m.oMenuItem.OnClick = "cp_ToolBarCmd('"+.cCBName+"','draw_adder_Click(l_oToolBar."+m.oBtn.Name+","+l_tmp +")')"
					m.oMenuItem.Visible=.T.
					m.oBtnMenu.InitMenu()
					m.oBtn.oPopupMenu = m.oBtnMenu
					m.oBtn.nButtonType = 2
				Endif
			endif
            *--- Bmp Btn
            m.oBtn = .AddButton("bmp", Iif(m.bStyle, "draw_adder_cb", "draw_adder"))
            m.oBtn.Caption = ""
            m.oBtn.cImage = "bitmap.bmp"
            m.oBtn._OnClick = "This.parent.draw_adder_Click(this)"
            m.oBtn.ToolTipText = cp_Translate(MSG_BITMAP_IMAGE)
            m.oBtn.cType = 'painter_bitmap'
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("sep2","Separator")
            #If Version(5)>=900
                .sep2.Visible=.T.
            #Endif
            *--- Cut Btn
            m.oBtn = .AddButton("btncut")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "cut.bmp"
            m.oBtn._OnClick = "This.parent.btncut_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_CUT)
            m.oBtn.Visible=.T.
            *--- Copy Btn
            m.oBtn = .AddButton("btncopy")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "copy.bmp"
            m.oBtn._OnClick = "This.parent.btncopy_Click()"
            m.oBtn.ToolTipText =cp_Translate(MSG_COPY)
            m.oBtn.Visible=.T.
            *--- Paste Btn
            m.oBtn = .AddButton("btnpaste")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "paste.bmp"
            m.oBtn._OnClick = "This.parent.btnpaste_Click()"
            m.oBtn.ToolTipText =cp_Translate(MSG_PASTE)
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("sep5","Separator")
            #If Version(5)>=900
                .sep5.Visible=.T.
            #Endif
            *--- Erase Btn
            m.oBtn = .AddButton("btnerase")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "canc.bmp"
            m.oBtn._OnClick = "This.parent.btncanc_Click()"
            m.oBtn.ToolTipText =cp_Translate(MSG_ERASE)
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("sep3","Separator")
            #If Version(5)>=900
                .sep3.Visible=.T.
            #Endif
            *--- AllignLeft Btn
            m.oBtn = .AddButton("allleft")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignl.bmp"
            m.oBtn._OnClick = "This.parent.allleft_Click()"
            m.oBtn.ToolTipText =cp_Translate(MSG_ALIGN_LEFT)
            m.oBtn.Visible=.T.
            *--- AllignRight Btn
            m.oBtn = .AddButton("allright")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignr.bmp"
            m.oBtn._OnClick = "This.parent.allright_Click()"
            m.oBtn.ToolTipText =cp_Translate(MSG_ALIGN_RIGHT)
            m.oBtn.Visible=.T.
            *--- AllignTop Btn
            m.oBtn = .AddButton("alltop")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignt.bmp"
            m.oBtn._OnClick = "This.parent.alltop_Click()"
            m.oBtn.ToolTipText =cp_Translate(MSG_ALIGN_TOP)
            m.oBtn.Visible=.T.
            *--- AllignBottom Btn
            m.oBtn = .AddButton("allbottom")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignb.bmp"
            m.oBtn._OnClick = "This.parent.allbottom_Click()"
            m.oBtn.ToolTipText =cp_Translate(MSG_ALIGN_BOTTOM)
            m.oBtn.Visible=.T.
            *--- AllSameH Btn
            m.oBtn = .AddButton("allsameh")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignsh.bmp"
            m.oBtn._OnClick = "This.parent.allsameh_Click()"
            m.oBtn.ToolTipText =cp_Translate(MSG_SAME_HORIZONTAL_DISTANCE)
            m.oBtn.Visible=.T.
            *--- AllSameH Btn
            m.oBtn = .AddButton("allsamev")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignsv.bmp"
            m.oBtn._OnClick = "This.parent.allsamev_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_SAME_VERTICAL_DISTANCE)
            m.oBtn.Visible=.T.
        Endwith
    Endproc

    Procedure draw_adder_Click(btn, cMacro)
        With m.btn
            If i_VisualTheme <> -1
                .Ndx=This.MousePosX -.Left
                .nDY=This.MousePosY-.Top
                If !Empty(m.cMacro)
                    .Ndx=Int(.Width/2)
                    .nDY=Int(.Height/2)
                Endif
            Endif
            .cMacro = m.cMacro
            Local dt
            dt = Createobject("DragTimer", m.btn)
        Endwith
    Endproc

    Proc EditMask()
        If !This.bSec1
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            This.Exit()
        Else
            This.Show()
            This.oForm.Show()
        Endif
        Return
    Proc Exit()
        If !IsNull(This.oForm.oExec)
        	This.oForm.oExec.Cnt.btncan.Click()
        EndIf
        This.oForm=.Null.
        This.oThis=.Null.
        i_curform=.Null.
        This.Visible=.F.
        This.Destroy()
        Return
    Func GetHelpTopic()
        Return('vm_build')
    Proc NewDoc()
        This.oForm.New()
        Return
    Proc LoadDoc()
        Local xcg,c,h,F,l
        * --- nuovo documento
        This.oForm.New()
        c=cp_GetPFile(This.cFileName)
        xcg=Createobject('pxcg','load',c)
        This.Serialize(xcg)
        This.bModified=.F.
    Proc SaveDoc()
        Local xcg,h
        xcg=Createobject('pxcg','save')
        This.Serialize(xcg)
        This.bModified=.F.
        h=Fcreate(This.cFileName)
        =Fwrite(h,xcg.cText)
        =Fclose(h)
    Proc Serialize(xcg)
        This.oForm.LockScreen=.T.
        This.oForm.Serialize(xcg)
        This.oForm.GridSettings()
        This.oForm.LockScreen=.F.
        *
        * --- procedure dei bottoni
        *
    Proc test_Click()
        Local l_fileVfm, l_oldScreen, l_oldFileName, l_oldModified
        l_fileVfm = Addbs(Sys(2023))+Sys(2015)+".vfm"
        l_oldScreen = This.oForm.cScreen
        This.oForm.cScreen ='Form'
        l_oldFileName = This.cFileName
        This.cFileName = l_fileVfm
        l_oldModified = This.bModified
        This.SaveDoc()
        This.oForm.cScreen = m.l_oldScreen
        This.cFileName = l_oldFileName
        vm_exec(l_fileVfm, This.oForm)
        Erase (l_fileVfm)
        This.bModified = m.l_oldModified
    Proc opt_Click()
        Local o
*!*	        o=Createobject('vm_form_options',This.oForm)
*!*	        o.Show()
            l_batch="cp_maskoptions(this)"
            &l_batch
    Proc btncut_Click()
        Local xcg
        xcg=Createobject('pxcg','save')
        This.oForm.drw.Cut(xcg)
        This.cClipBoard=xcg.cText
    Proc btncopy_Click()
        Local xcg
        xcg=Createobject('pxcg','save')
        This.oForm.drw.Copy(xcg)
        This.cClipBoard=xcg.cText
    Proc btnpaste_Click()
        Local xcg
        If !Empty(This.cClipBoard)
            xcg=Createobject('pxcg','load',This.cClipBoard)
            This.oForm.drw.Paste(xcg)
            This.bModified=.T.
        Endif
    Proc btncanc_Click()
        This.oForm.drw.RemoveSelected()
    Proc allleft_click()
        Local o
        o=Createobject('draw_align')
        o.AlignLeft(This.oForm.drw)
        This.bModified=.T.
    Proc allright_click()
        Local o
        o=Createobject('draw_align')
        o.Alignright(This.oForm.drw)
        This.bModified=.T.
    Proc alltop_click()
        Local o
        o=Createobject('draw_align')
        o.AlignTop(This.oForm.drw)
        This.bModified=.T.
    Proc allbottom_click()
        Local o
        o=Createobject('draw_align')
        o.AlignBottom(This.oForm.drw)
        This.bModified=.T.
    Proc allsameh_click()
        Local o
        o=Createobject('draw_align')
        o.SameHDist(This.oForm.drw)
        This.bModified=.T.
    Proc allsamev_click()
        Local o
        o=Createobject('draw_align')
        o.SameVDist(This.oForm.drw)
        This.bModified=.T.
        *
        *
        *
    Proc MouseDown(nBtn,nSh,nX,nY)
        If nBtn=2 And nSh=1
            If !Empty(This.cFileName)
                Local o
                This.oForm.drw.DeSelectAll()
                o=Createobject('vm_code_generator',This)
                cp_msg( cp_MsgFormat(MSG__GENERATED_QM,This.cFileName),.F.)
            Else
                cp_msg(cp_Translate(MSG_FILE_NAME_MISSING_QM),.F.)
            Endif
        Endif
    Proc ecpSecurity()
        Createobject('stdsProgAuth','VisualMsk')
        Return
Enddefine

Define Class vm_frm As Form
    Width=400
    Closable=.F.
    oTool=.Null.
    Caption=""
    cModal='S'
    cScreen='Form'
    nSnapGrid=0
    nShowGrid=0
    oExec=.Null.

    idVfm = ID_VFM
    nAlignmentCnt=1
    cGradient='N'
    cBckImage=""
    nGradientType=1
    nFColor=Rgb(255,255,255)
    nLColor=Rgb(255,255,255)
	BackColor=Rgb(255,255,255)
    cOldIcon=''
    cCnfFile=iif(TYPE('g_cCnfFileOpt')<>'U' and LOWER(JUSTEXT(g_cCnfFileOpt))='dbf',g_cCnfFileOpt,'')

    KeyPreview=.T.
    Icon=i_cBmpPath+i_cStdIcon
    Add Object bckImg As vm_frm_bck_image With Top=0, Left=0, Visible=.F.,Height=This.Height,Width=This.Width
    Add Object drw As draw_board With Height=This.Height,Width=This.Width,BorderWidth=0
    Add Object dummy As CommandButton With Enable=.T., Visible=.T., Style=1, Width=1, Height=1, Top=-1

	*--- Zucchetti Aulla Inizio - Form decorator
	bApplyFormDecorator=.t.	
	*-- Zucchetti Aulla Fine - Form decorator  

    Proc Init()
        This.Caption=cp_Translate(MSG_DRAW)
        IF !EMPTY(this.cCnfFile) AND cp_fileexist(this.cCnfFile)
             this.CnfFile()
        ENDIF
        This.GridSettings()
        
    	*--- Form Decorator
    	If This.bApplyFormDecorator And i_ThemesManager.GetProp(123)=0
    		This.AddObject("oDec", "cp_FormDecorator")	
        Else
            This.bApplyFormDecorator = .F.    		
    	EndIf	
           
    Proc New()
        This.drw.RemoveAll()
        IF !EMPTY(this.cCnfFile) AND cp_fileexist(this.cCnfFile) AND LOWER(JUSTEXT(this.cCnfFile))='dbf'
             this.CnfFile()
        ELSE
        This.cBckImage=""
        This.cGradient='N'
        This.cModal='S'
        This.nAlignmentCnt=1
        This.nGradientType=1
        This.nFColor=Rgb(255,255,255)
        This.nLColor=Rgb(255,255,255)
        This.bckImg.Visible=.F.
        This.bckImg.PictureVal=""
        This.nSnapGrid=0
        This.nShowGrid=0
        ENDIF
        This.Top=0
        This.Left=0
        This.Width=400
        This.Height=250
        This.Caption=cp_Translate(MSG_DRAW)
        This.bckImg.Setup(This.cBckImage, This.cGradient, This.nFColor, This.nLColor, This.nGradientType, This.Width, This.Height, .T.)
        This.GridSettings()

    Proc drw.SetModified(bStatus)
        This.Parent.oTool.bModified=bStatus
        DoDefault(bStatus)
    Proc drw.DragDrop(oSource,x,Y)
        Local cFName,cOldFName,bOk,cDefPath
        cDefPath=Sys(5)+Sys(2003)+'\'
        Do Case
            Case Upper(oSource.Name)='STDSGRP'
                * drag iniziato nella lista dei gruppi del tool di gestione utenti
                cFName='default_g'+Alltrim(Str(_cpgroups_.Code))+'.vfm'
                cOldFName=This.Parent.oTool.cFileName
                If At(Upper(cFName),Upper(Thisform.oTool.cFileName))<>0
                    cFName=Thisform.oTool.cFileName
                Endif
                This.Parent.oTool.cFileName=cFName
                bOk=.T.
                If At("\",cFName)<>0
                    cDefPath=''
                Endif
                *--- Zucchetti Aulla Inizio - Monitor Framework
                *--- Se il monitor � attivo verifico i diritti
                Local l_UsrRight
                l_UsrRight = cp_set_get_right()
                Do Case
                    Case l_UsrRight = 2   &&Read Only
                        This.Parent.oTool.cFileName=cOldFName
                        Return
                    Case l_UsrRight = 3
                        If Ascan(i_aUsrGrps, _cpgroups_.Code) = 0
                            This.Parent.oTool.cFileName=cOldFName
                            Return
                        Endif
                        cFName = Forcepath(Upper(cFName), Fullpath(i_usrpath))
                    Case l_UsrRight = 4
                        cFName = Forcepath(Upper(cFName), Fullpath(i_usrpath))
                    Otherwise
                        cFName = Iif(Not Empty(cDefPath), Forcepath(Upper(cFName), cDefPath), Upper(cFName))
                Endcase
                This.Parent.oTool.cFileName=cFName
                If cp_fileexist(cFName)
                    bOk=cp_YesNo(cp_MsgFormat(MSG_FILE____ALREADY_EXIST_C_SUBSTITUTE_QP, cFName, ''),48,.f.,.f.,MSG_SAVE)
                Else
                    bOk=cp_YesNo(cp_MsgFormat(MSG_SAVE_FILE__QP, cFName),32,.f.,.f.,MSG_SAVE)
                Endif
                *--- Zucchetti Aulla Fine - Monitor Framework
                If bOk
                    This.Parent.oTool.SaveDoc()
                Else
                    This.Parent.oTool.bModified=.F.
                Endif
                This.Parent.oTool.cFileName=cOldFName
            Case Upper(oSource.Name)='STDSUSR'
                * drag iniziato nella lista degli utenti del tool di gestione utenti
                cFName='default_'+Alltrim(Str(_cpusers_.Code))+'.vfm'
                cOldFName=This.Parent.oTool.cFileName
                If At(Upper(cFName),Upper(Thisform.oTool.cFileName))<>0
                    cFName=Thisform.oTool.cFileName
                Endif
                This.Parent.oTool.cFileName=cFName
                bOk=.T.
                If At("\",cFName)<>0
                    cDefPath=''
                Endif
                *--- Zucchetti Aulla Inizio - Monitor Framework
                *--- Se il monitor � attivo verifico i diritti
                Local l_UsrRight
                l_UsrRight = cp_set_get_right()
                Do Case
                    Case l_UsrRight = 2   &&Read Only
                        This.Parent.oTool.cFileName=cOldFName
                        Return
                    Case l_UsrRight = 3
                        If i_CodUte<>_cpusers_.Code
                            *cp_errorMsg(MSG_ACCESS_REFUSED)
                            This.Parent.oTool.cFileName=cOldFName
                            Return
                        Endif
                        cFName = Forcepath(Upper(cFName), Fullpath(i_usrpath))
                    Case l_UsrRight = 4
                        cFName = Forcepath(Upper(cFName), Fullpath(i_usrpath))
                    Otherwise
                        cFName = Iif(Not Empty(cDefPath), Forcepath(Upper(cFName), cDefPath), Upper(cFName))
                Endcase
                This.Parent.oTool.cFileName=cFName
                *--- Zucchetti Aulla Fine - Monitor Framework
                If cp_fileexist(cFName)
                    bOk=cp_YesNo(cp_MsgFormat(MSG_FILE____ALREADY_EXIST_C_SUBSTITUTE_QP, cFName, ''),48,.f.,.f.,MSG_SAVE)
                Else
                    bOk=cp_YesNo(cp_MsgFormat(MSG_SAVE_FILE__QP, cFName),32,.f.,.f.,MSG_SAVE)
                Endif
                If bOk
                    This.Parent.oTool.SaveDoc()
                Else
                    This.Parent.oTool.bModified=.F.
                Endif
                This.Parent.oTool.cFileName=cOldFName
            Case Lower(oSource.ParentClass)=='stdcontainer'
                *--- Da gestione, aggiungo bottone
                * Zucchetti Aulla Inizio - Anche gestioni con parametri nei bottoni
                Local L_oBj, L_Sec, L_Param, L_Prg
                L_oBj=oSource.oContained
			    DO WHILE !ISNULL(L_oBj) AND !PEMSTATUS(L_oBj,"getSecuritycode",5) AND PEMSTATUS(L_oBj,"oParentObject",5)
				*--- Se � un figlio integrato devo risalire fino al padre		    
			      	L_oBj=L_oBj.oParentObject
			    ENDDO                
                L_Sec=Alltrim(L_oBj.getSecuritycode())
                L_Prg= Alltrim(Substr(L_oBj.Class,2,Len(L_oBj.Class)-1))
                L_Param=Upper(Alltrim(Substr(L_Sec,Len(L_Prg)+2)))
                If Not Empty(Alltrim(Nvl(L_Param, ' ')))
                    L_Prg = L_Prg + '("'+L_Param+'")'
                Else
                    L_Prg = L_Prg
                Endif
                *---WAIT WINDOW oSource.Parent.Parent.Parent.cComment + CHR(13) + L_Prg
                Local L_oBj
                L_oBj = "This."+This.AddItem('painter_mediabtn', x-This.Ndx, Y-This.nDY)
                &L_oBj..cType='PRG'
                If oSource.oContained.bLoaded
                    Local cKey
                    cKey = ""
                    Local i, cAzi
                    Dimension xKey[alen(oSource.xKey)]
                    For i=1 To Alen(oSource.xKey)
                        cKey = cKey + "," +  cp_ToStrODBC(oSource.xKey[i])
                    Next
                    i=cp_ascan(@i_TableProp, Lower(oSource.cfile), i_nTables)
                    If i>0
                        cAzi=Strtran(cp_SetAzi(i_TableProp[i,2]), Strtran(i_TableProp[i,2],'xxx',''),'')
                        cKey = ",'"+cAzi+"'" + cKey
                    Endif
                    &L_oBj..cPrg="This.OpenGest(["+L_Prg+"]"+cKey+")"
                Else
                    &L_oBj..cPrg="OpenGest('A', ["+L_Prg+"])"
                Endif
                &L_oBj..cTitle=oSource.oContained.cComment
                &L_oBj..Preview()
            Otherwise
                DoDefault(oSource,x,Y)
        Endcase
        Return
    Proc drw.DragOver(oSource,nXCoord,nYCoord,nState)
        Do Case
            Case nState=0   && Enter
                This.Parent.cOldIcon=oSource.DragIcon
                If oSource.ParentClass='Stdcontainer'
                    oSource.DragIcon=i_cBmpPath+"cross01.cur"
                Endif
            Case nState=1   && Leave
                oSource.DragIcon=This.Parent.cOldIcon
        Endcase
        Return
    Proc Resize()
        This.drw.Width=This.Width
        This.drw.Height=This.Height
        This.bckImg.Width=This.Width
        This.bckImg.Height=This.Height
        This.bckImg.Setup(This.cBckImage, This.cGradient, This.nFColor, This.nLColor, This.nGradientType, This.Width, This.Height)
        This.oTool.bModified=.T.
    Proc Serialize(xcg)
        Local i,j,N,c, nCtrlCount
        If xcg.bIsStoring
            *--- Salvo la versione del file, le nuove vfm hanno una propriet� che le identifica e che viene scartata
            xcg.Save('N',This.Left)
            xcg.Save('N',This.Top)
            xcg.Save('N',This.Width)
            xcg.Save('N',This.Height)
            This.drw.DeSelectAll()
            This.drw.bModified=.F.
            *--- Conto i controlli che hanno la serialize
            nCtrlCount=0
            For i=1 To This.drw.ControlCount
                If Pemstatus(This.drw.Controls(i), "Serialize", 5)
                    nCtrlCount= nCtrlCount + 1
                Endif
            Next
            xcg.Save('N',nCtrlCount)
            For i=1 To This.drw.ControlCount
                If Pemstatus(This.drw.Controls(i), "Serialize", 5)
                    xcg.Save('C',This.drw.Controls(i).Name)
                    xcg.Save('C',This.drw.Controls(i).Class)
                    This.drw.Controls(i).Serialize(xcg)
                Endif
            Next
            xcg.Save('C',This.Caption)
            xcg.Save('C',This.cModal)
            xcg.Save('C',This.cScreen)
            *--- Nuove Propriet�
            xcg.Save('N',This.nAlignmentCnt)
            xcg.Save('C',This.cGradient)
            xcg.Save('C',This.cBckImage)
            xcg.Save('N',This.nGradientType)
            xcg.Save('N',This.nFColor)
            xcg.Save('N',This.nLColor)
            xcg.Save('N',This.nSnapGrid)
            xcg.Save('N',This.nShowGrid)
            xcg.Save('N',This.drw.nGridHPoint)
            xcg.Save('N',This.drw.nGridVPoint)
            xcg.Eoc()
        Else
            This.Left=xcg.Load('N',0)
            This.Top=xcg.Load('N',0)
            This.Width=xcg.Load('N',300)
            This.Height=xcg.Load('N',200)
            j=xcg.Load('N',0)
            For i=1 To j
                N=xcg.Load('C','')
                c=xcg.Load('C','')
                This.drw.AddObject(N,c)
                This.drw.&N..Serialize(xcg)
            Next
            This.Caption=xcg.Load('C','')
            This.cModal=xcg.Load('C','S')
            This.cScreen=xcg.Load('C','Form')
            If Not xcg.Eoc(.T.)
                This.idVfm = ID_VFM
                *--- Nuove Propriet�
                This.nAlignmentCnt=xcg.Load('N',1)
                This.cGradient=xcg.Load('C','N')
                This.cBckImage=xcg.Load('C','')
                This.nGradientType=xcg.Load('N',1)
                This.nFColor=xcg.Load('N',Rgb(255,255,255))
                This.nLColor=xcg.Load('N',Rgb(255,255,255))
                This.nSnapGrid=xcg.Load('N',0)
                This.nShowGrid=xcg.Load('N',0)
                This.drw.nGridHPoint=xcg.Load('N',1)
                This.drw.nGridVPoint=xcg.Load('N',1)
                xcg.Eoc()
            Endif
            This.bckImg.Setup(This.cBckImage, This.cGradient, This.nFColor, This.nLColor, This.nGradientType, This.Width, This.Height)
            *--- Add Connector
            Local Obj
            Dimension aNodeRoot[1]
            For Each Obj In This.drw.Controls
                If Lower(Obj.Class)=="draw_linenode" And Obj.Name==Obj.cRoot
                    aNodeRoot[ALEN(aNodeRoot,1)] = Obj
                    Dimension aNodeRoot[ALEN(aNodeRoot,1)+1]
                Endif
            Next
            For i=1 To Alen(aNodeRoot,1)-1
                aNodeRoot[i].AddLine()
            Next
        Endif
        *--- Gestione keyboard
    Procedure KeyPress
        Lparameters nKeyCode, nShiftAltCtrl
        Do Case
            Case nKeyCode=7  &&Canc
                This.drw.RemoveSelected()
            Case nKeyCode=56 &&Up
                This.drw.ResizeSelected(0,-1)
            Case nKeyCode=5
                This.drw.MoveSelected(0,-1)
            Case nKeyCode=50 &&Down
                This.drw.ResizeSelected(0,1)
            Case nKeyCode=24 And nShiftAltCtrl <> 2
                This.drw.MoveSelected(0,1)
            Case nKeyCode=54 &&Right
                This.drw.ResizeSelected(1,0)
            Case nKeyCode=4
                This.drw.MoveSelected(1,0)
            Case nKeyCode=52 &&Left
                This.drw.ResizeSelected(-1,0)
            Case nKeyCode=19
                This.drw.MoveSelected(-1,0)
            Case nKeyCode=13 &&Enter
                This.drw.DblClickSelected()
            Case nKeyCode=24 And nShiftAltCtrl = 2&&Cut
                Local xcg
                xcg=Createobject('pxcg','save')
                This.drw.Cut(xcg)
                This.oTool.cClipBoard=xcg.cText
            Case nKeyCode=3 And nShiftAltCtrl = 2 &&Copy
                Local xcg
                xcg=Createobject('pxcg','save')
                This.drw.Copy(xcg)
                This.oTool.cClipBoard=xcg.cText
            Case nKeyCode=22 And nShiftAltCtrl = 2 &&Paste
                Local xcg
                If !Empty(This.oTool.cClipBoard)
                    xcg=Createobject('pxcg','load',This.oTool.cClipBoard)
                    This.drw.Paste(xcg)
                    This.oTool.bModified=.T.
                Endif
        ENDCASE
        PROC CnfFile() 
             IF USED(JUSTSTEM(this.cCnfFile))
                  USE IN SELECT (this.cCnfFile)
             ENDIF 
             i_cCnfFile=JUSTSTEM(this.cCnfFile) 
             L_Err=.f.
             L_OLDERROR=On('Error')
             on error L_err=.t.
             SELECT prop,val FROM (i_cCnfFile) WHERE obj="mask" INTO CURSOR SaveConf
             SELECT SaveConf
             GO top
             DO WHILE NOT EOF()
                  i_prop=alltrim(prop)
                  IF "point" $ i_prop
                      i_val=VAL(ALLTRIM(val))
                      This.drw.&i_prop=i_val
                  ELSE
                      i_val=IIF(TYPE('This.&i_prop')='N',VAL(ALLTRIM(val)),IIF(TYPE('This.&i_prop')='L',IIF(ALLTRIM(val)='.T.',.T.,.F.),ALLTRIM(val)))
                      This.&i_prop=i_val
                  ENDIF
                  skip
             ENDDO
             This.bckImg.Setup(This.cBckImage, This.cGradient, This.nFColor, This.nLColor, This.nGradientType, This.Width, This.Height)
             USE IN SELECT (i_cCnfFile)
             USE IN SELECT ("SaveConf")
             on error &L_OldError
             IF L_err=.t.
                cp_errormsg("File di configurazione errato")
                this.cCnfFile='' 
             ENDIF
        PROC GridSettings()
              *-- Se attivo il check inserisco la griglia
              This.picture=""
              IF This.nShowGrid<>0 AND This.drw.nGridHPoint>1 AND This.drw.nGridVPoint>1
                  L_TmpBmp = _Screen.CP_THEMESMANAGER.CreateBitmap(This.drw.nGridHPoint,This.drw.nGridVPoint,"FFFFFF")
                  m.L_oImg=LoadPicture(m.L_TmpBmp )
                  m.i_HDC = GetDC( 0)
                  m.i_HMemDC= CreateCompatibleDC(m.i_HDC)
                  DeleteObject(SelectObject(m.i_HMemDC, m.L_oImg.handle))
                  nHeight = Iif(Type('ThisForm.oDec.Height')='N',Mod(ThisForm.oDec.Height-ThisForm.oDec.BorderWidth,This.drw.nGridVPoint),0)
					        SetPixel(m.i_HMemDC, 0, m.nHeight, 0)  &&Widht, height e colore					
                  DeleteDC(m.i_HMemDC)
                  SavePicture(m.L_oImg, m.L_TmpBmp)
                  This.picture=m.L_TmpBmp
              ENDIF
				This.drw.nGridHStep=This.drw.nGridHPoint
				This.drw.nGridVStep=This.drw.nGridVPoint
Enddefine

Define Class vm_code_generator As Custom
    Proc Init(oVM)
        Local h,N,i
        N=Left(oVM.cFileName,Len(oVM.cFileName)-4)+'.p'
        h=Fcreate(N)
        Fwrite(h,'  top='+Alltrim(Str(oVM.oForm.Top))+Chr(13)+Chr(10))
        Fwrite(h,'  left='+Alltrim(Str(oVM.oForm.Left))+Chr(13)+Chr(10))
        Fwrite(h,'  width='+Alltrim(Str(oVM.oForm.Width))+Chr(13)+Chr(10))
        Fwrite(h,'  height='+Alltrim(Str(oVM.oForm.Height))+Chr(13)+Chr(10))
        Fwrite(h,'  caption="'+Alltrim(oVM.oForm.Caption)+'"'+Chr(13)+Chr(10))
        For i=1 To oVM.oForm.drw.ControlCount
            o=oVM.oForm.drw.Controls(i)
            Fwrite(h,'  add object ')
            Do Case
                Case o.Class='Painter_lbl'
                    Fwrite(h,'lbl'+Alltrim(Str(i))+' as label with caption="'+o.cVarName+'",')
                    If o.cAlign='C'
                        Fwrite(h,'alignment=2,')
                    Endif
                    If o.cAlign='R'
                        Fwrite(h,'alignment=1,')
                    Endif
                Case o.Class='Painter_txt'
                    Fwrite(h,o.cVarName+' as textbox with ')
                    If !Empty(o.cPict)
                        Fwrite(h,' inputmask="'+o.cPict+'",')
                    Endif
                Case o.Class='Painter_combo'
                    Fwrite(h,o.cVarName+' as combobox with ')
                Case o.Class='Painter_check'
                    Fwrite(h,o.cVarName+' as checkbox with caption="'+o.cTitle+'",')
                Case o.Class='Painter_button'
                    Fwrite(h,o.cVarName+' as commandbutton with caption="'+o.cTitle+'",')
            Endcase
            Fwrite(h,'top='+Alltrim(Str(o.Top))+',left='+Alltrim(Str(o.Left))+',width='+Alltrim(Str(o.Width))+',height='+Alltrim(Str(o.Height))+Chr(13)+Chr(10))
        Next
        Fclose(h)
Enddefine

DEFINE CLASS vm_grid_setting as Container
	Width = 288
	Height = 108
	BackStyle = 0
	BorderWidth = 0

	ADD OBJECT lbl1 AS label WITH ;
		BackStyle = 0, ;
		Caption = "Grid settings", ;
		Height = 17, ;
		Left = 8, ;
		Top = 12, ;
		Width = 84

	ADD OBJECT line1 AS line WITH ;
		Height = 0, ;
		Left = 8, ;
		Top = 30, ;
		Width = 252

	ADD OBJECT lbl2 AS label WITH ;
		Alignment = 1, ;
		BackStyle = 0, ;
		Caption = "Horizontal step:", ;
		Height = 17, ;
		Left = 12, ;
		Top = 43, ;
		Width = 84

	ADD OBJECT hstep AS textbox WITH ;
		Height = 22, ;
		InputMask = "999", ;
		Left = 99, ;
		Top = 41, ;
		Width = 36

	ADD OBJECT lbl3 AS label WITH ;
		Alignment = 1, ;
		BackStyle = 0, ;
		Caption = "Vertical step:", ;
		Height = 17, ;
		Left = 12, ;
		Top = 72, ;
		Width = 84

	ADD OBJECT vstep AS textbox WITH ;
		Height = 22, ;
		InputMask = "999", ;
		Left = 99, ;
		Top = 70, ;
		Width = 36

	ADD OBJECT SnapGrdChk AS checkbox WITH ;
		Top = 43, ;
		Left = 180, ;
		Height = 17, ;
		Width = 84, ;
		Alignment = 0, ;
		BackStyle = 0, ;
		Caption = "Snap to grid"

	ADD OBJECT ShowGrdChk AS checkbox WITH ;
		Top = 72, ;
		Left = 180, ;
		Height = 17, ;
		Width = 84, ;
		Alignment = 0, ;
		BackStyle = 0, ;
		Caption = "Show grid"
ENDDEFINE 
Define Class vm_form_options As painter_form
    Top=32
    Left=112
    Width=330
    Height=417
    Caption=""
    Dimension aAllCnt(6)
    Dimension aGrdType(5)

    Add Object lbl1 As Label With Caption="",Alignment=1,Top=16,Left=17,Width=38,Height=16,BackStyle=0
    Add Object Title As TextBox With Top=14,Left=59,Width=257,Height=22
    Add Object Mod As Checkbox With Caption="",Top=46,Left=17,Width=100,Height=20,BackStyle=0
    Add Object scr As Checkbox With Caption="",Top=66,Left=17,Width=100,Height=20,BackStyle=0
    Add Object lbl2 As Label With Caption="",Alignment=1,Top=68,Left=100,Width=94,Height=16,BackStyle=0
    Add Object allCnt As ComboBox With Top=66,Left=196,Width=120,Height=22,BackStyle=0,Style=2
    Add Object lbl3 As Label With Caption="",Alignment=0,Top=116,Left=17,Width=100,Height=16,BackStyle=0
    Add Object bckImg As TextBox With Top=131,Left=17,Width=275,Height=22
    Add Object BckImgBtn As CommandButton With Top=131,Left=295,Width=22,Height=22, Caption='...'
    
    Add Object lbl7 As Label With Caption="File di configurazione"+MSG_FS,Alignment=0,Top=160,Left=17,Width=130,Height=16,BackStyle=0
    Add Object CnfFile As TextBox With Top=175,Left=17,Width=275,Height=22
    Add Object CnfFileBtn As CommandButton With Top=175,Left=295,Width=22,Height=22, Caption='...'

    Add Object grd As Checkbox With Caption="",Top=207,Left=17,Width=130,Height=20,BackStyle=0
    Add Object lbl4 As Label With Caption="",Alignment=1,Top=209,Left=156,Width=38,Height=16,BackStyle=0
    Add Object grdType As ComboBox With Top=207,Left=196,Width=120,Height=22,BackStyle=0,Style=2
    Add Object shp1 As Shape With Top=235,Left=17,Width=16,Height=16
    Add Object lbl5 As Label With Caption="FColor",Alignment=0,Top=239,Left=36,Width=38,Height=16,BackStyle=0
    Add Object shp2 As Shape With Top=257,Left=17,Width=16,Height=16
    Add Object lbl6 As Label With Caption="LColor",Alignment=0,Top=259,Left=36,Width=38,Height=16,BackStyle=0
	Add Object FullScr As Checkbox With Caption="",Top=86,Left=17,Width=100,Height=20,BackStyle=0
	ADD OBJECT GrdSetting As vm_grid_setting WITH Top=280, left=17 
	

    Proc Getcolor(oShp)
        Local l_color
        l_color = Getcolor(m.oShp.BackColor)
        m.oShp.BackColor=Iif(l_color<>-1, l_color, m.oShp.BackColor)

    Procedure shp1.DblClick()
        This.Parent.Getcolor(This)

    Procedure shp2.DblClick()
        This.Parent.Getcolor(This)

    Procedure CnfFileBtn.Click()
        Local l_FileCnf
        l_FileCnf = Getfile('dbf')
        This.Parent.CnfFile.Value = Evl(Sys(2014, l_FileCnf), This.Parent.CnfFile.Value)
        This.Parent.LoadValues()

    Procedure BckImgBtn.Click()
        Local l_FileImg
        l_FileImg = Getfile()
        This.Parent.bckImg.Value = Evl(Sys(2014, l_FileImg), This.Parent.bckImg.Value)

    Proc LoadValues()
        This.Title.Value=This.oObj.Caption
        this.CnfFile.value=IIF(EMPTY(NVL(this.CnfFile.value,"")),this.oObj.cCnfFile,this.CnfFile.value)
        IF !EMPTY(this.CnfFile.value) AND cp_fileexist(this.CnfFile.value) AND This.oObj.cCnfFile<>This.CnfFile.value AND cp_yesno('File di configurazione variato, applicare le modifiche alla maschera attuale?',32,.f.)
             IF USED(JUSTSTEM(this.CnfFile.value))
                  USE (this.CnfFile.value)
             ENDIF 
             i_cCnfFile=JUSTSTEM(this.CnfFile.value)
             L_Err=.f.
             L_OLDERROR=On('Error')
             on error L_err=.t. 
             SELECT prop,val FROM (i_cCnfFile) WHERE obj="mask" INTO CURSOR SaveConf
             SELECT SaveConf
             GO top
             DO WHILE NOT EOF()
                  i_prop=alltrim(prop)
                  oObject = IIF("grdchk" $ LOWER(i_prop) OR "step" $ LOWER(i_prop),This.GrdSetting.&i_prop,This.&i_prop)
                  IF LOWER(i_prop)="shp1" OR LOWER(i_prop)="shp2"
                  i_val=IIF(EMPTY(val),0,VAL(ALLTRIM(val)))
                  oObject.BackColor= i_val
                  ELSE
                  i_val=IIF(TYPE('oObject.value')='N',VAL(ALLTRIM(val)),IIF(TYPE('oObject.value')='L',IIF(ALLTRIM(val)='.T.',.T.,.F.),ALLTRIM(val)))
                  oObject.value=i_val
                  endif
                  skip
             ENDDO
             USE IN SELECT (i_cCnfFile)
             USE IN SELECT ("SaveConf")
             on error &L_OldError
             IF L_err=.t.
                cp_errormsg("File di configurazione errato")
                this.CnfFile.value='' 
             ENDIF
        else
        This.Mod.Value=Iif(This.oObj.cModal='S',1,0)
        This.scr.Value=Iif(This.oObj.cScreen='FullScreen' or This.oObj.cScreen='OnScreen',1,0)
		This.FullScr.Value=Iif(This.oObj.cScreen='FullScreen',1,0)
        This.allCnt.Value=This.oObj.nAlignmentCnt
        This.grd.Value=Iif(This.oObj.cGradient='S',1,0)
        This.bckImg.Value=This.oObj.cBckImage
        This.grdType.Value=This.oObj.nGradientType
        This.shp1.BackColor=This.oObj.nFColor
        This.shp2.BackColor=This.oObj.nLColor
        This.GrdSetting.SnapGrdChk.value=This.oObj.nSnapGrid
        This.GrdSetting.ShowGrdChk.value=This.oObj.nShowGrid
        This.GrdSetting.hstep.value = This.oObj.drw.nGridHPoint
        This.GrdSetting.vstep.value = This.oObj.drw.nGridVPoint
        endif
        Return
    Proc SaveValues()
        LOCAL old_,old_Hstep,old_Vstep
        This.oObj.Caption=Trim(This.Title.Value)
        This.oObj.cModal=Iif(This.Mod.Value=1,'S','N')
        This.oObj.cScreen=Iif(This.FullScr.Value=1,'FullScreen',Iif(This.scr.Value=1,'OnScreen','Form'))
        This.oObj.nAlignmentCnt=This.allCnt.Value
        This.oObj.cGradient=Iif(This.grd.Value=1,'S','N')
        This.oObj.cBckImage=This.bckImg.Value
        *--- Resize form alla dimensione dell'immagine
        If !Empty(This.oObj.cBckImage) And cp_fileexist(This.oObj.cBckImage)
            Local loImg As xfcImage
            With _Screen.System.Drawing
                loImg = .Image.FromFile(This.oObj.cBckImage)
                If (loImg.Width<>This.oObj.Width Or loImg.Height<>This.oObj.Height) And cp_YesNo(cp_Translate(MSG_RESIZE_FORM_TO_IMAGE_QP))
                    This.oObj.Width=loImg.Width
                    This.oObj.Height=loImg.Height
                Endif
            Endwith
        Endif
        This.oObj.nGradientType=This.grdType.Value
        This.oObj.nFColor=This.shp1.BackColor
        This.oObj.nLColor=This.shp2.BackColor
        This.oObj.nSnapGrid=This.GrdSetting.SnapGrdChk.value
        This.oObj.nShowGrid=This.GrdSetting.ShowGrdChk.value
        This.oObj.drw.nGridHPoint=MAX(This.GrdSetting.hstep.value,1)
        This.oObj.drw.nGridVPoint=MAX(This.GrdSetting.vstep.value,1)
        This.oObj.drw.nGridHStep=IIF(This.GrdSetting.SnapGrdChk.value<>0,This.GrdSetting.hstep.value,1)
        This.oObj.drw.nGridVStep=IIF(This.GrdSetting.SnapGrdChk.value<>0,This.GrdSetting.vstep.value,1)
        This.oObj.picture=""
        *-- Se attivo il check inserisco la griglia
        IF (This.oObj.nShowGrid<>0) AND This.GrdSetting.hstep.value>1 AND This.GrdSetting.vstep.value>1
            oPicGrid=_screen.system.Drawing.Bitmap.New(This.GrdSetting.hstep.value,This.GrdSetting.vstep.value)
            oPicGrid.SetPixel(0,0, _screen.system.Drawing.Color.Black)
            cPicture=g_TEMPADHOC+"\ShowGrid.bmp"
            oPicGrid.Save(cPicture)
            CLEAR RESOURCES (cPicture)
            This.oObj.picture=cPicture
        ENDIF
        This.oObj.cCnfFile=This.CnfFile.value
        This.oObj.bckImg.Setup(This.oObj.cBckImage, This.oObj.cGradient, This.oObj.nFColor, This.oObj.nLColor, This.oObj.nGradientType, This.oObj.Width, This.oObj.Height, .T.)
        This.oObj.drw.SetModified(.T.)
        Return
    Procedure Init(oObj)
        This.Caption=cp_Translate(MSG_DIALOG_WINDOW_OPTIONS)
        This.lbl1.Caption=cp_Translate(MSG_TITLE+MSG_FS)
        This.Mod.Caption=cp_Translate(MSG_MODAL)
        This.scr.Caption=cp_Translate(MSG_BACKGROUND)
		This.FullScr.Caption=cp_Translate(MSG_FULL_SCREEN)
        This.lbl2.Caption=cp_Translate(MSG_ALIGNMENT+MSG_FS)
        This.aAllCnt[1]=cp_Translate(MSG_ALIGN_CENTER)
        This.aAllCnt[2]=cp_Translate(MSG_ALIGN_LEFT)
        This.aAllCnt[3]=cp_Translate(MSG_ALIGN_RIGHT)
        This.aAllCnt[4]=cp_Translate(MSG_ALIGN_TOP)
        This.aAllCnt[5]=cp_Translate(MSG_ALIGN_BOTTOM)
        This.aAllCnt[6]=cp_Translate(MSG_NO_ALIGN)
        This.allCnt.RowSourceType = 5
        This.allCnt.RowSource = "Thisform.aAllCnt"
        This.lbl3.Caption=cp_Translate(MSG_IMAGE_CL)
        This.grd.Caption=cp_Translate(MSG_VIS_GRAD_BACKGOUND)
        This.lbl4.Caption=cp_Translate(MSG_TYPE+MSG_FS)
        This.GrdSetting.SnapGrdChk.value=0
        This.GrdSetting.ShowGrdChk.value=0
        This.aGrdType[1]=cp_Translate(MSG_CENTRED)
        This.aGrdType[2]=cp_Translate(MSG_VERTICAL)
        This.aGrdType[3]=cp_Translate(MSG_HORIZONTAL)
        This.aGrdType[4]=cp_Translate(MSG_FORWARDDIAGONAL)
        This.aGrdType[5]=cp_Translate(MSG_BACKWARDDIAGONAL)
        This.grdType.RowSourceType = 5
        This.grdType.RowSource = "Thisform.aGrdType"
        DoDefault(oObj)
    Endproc
Enddefine

Define Class DragTimer As Timer
    Interval = 50
    Bnt=.Null.
    _This=.Null.
    Procedure Init(btn)
        This.Bnt = m.btn
        This._This=This
    Endproc
    Procedure Timer()
        This.Interval=0
        This.Bnt.Drag(1)
        This._This=.Null.
        This.Destroy()
    Endproc
Enddefine
