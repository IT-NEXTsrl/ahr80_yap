* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_DOCKWND
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Stefano D'Amico
* Data creazione: 20/03/2008
* Aggiornato il : 01/05/2010
* Traduzione    : 21/03/2008
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Classi interfaccia stile Office 2007 (toolbar / men�  )
* ----------------------------------------------------------------------------
#include "cp_app_lang.inc"

#Define DW_CAPTION 1
#Define DW_BACKIMG 2
#Define DW_DOCKPOSITION 3
#Define DW_BCKCOLOR 4
#Define DW_BORDERCOLOR 5
#Define DW_FRAMECOLOR 6
#Define DW_CLOSABLE 7
#Define DW_GRIPPER 8
#Define DW_CLOSEBTNML 9
#Define DW_CLOSEBTNMM 10
#Define DW_CLOSEBTNMD 11
#Define DW_VFPTOOLBAR 12
#Define DW_VFPMENU 13
#Define DW_ISMENU 14
#Define DW_HWNDMENUBAR 15
#Define DW_VISIBLE 16
#Define DW_RELEASECAPTURE 17

*--- Menu Item
#Define MF_SEPARATOR	0x800
#Define MF_STRING	0x0
#Define MF_POPUP	0x10
#Define MF_CHECKED	0x8
#Define MF_UNCHECKED	0x0
#Define MF_DISABLED	0x2
#Define MF_ENABLED	0x0
#Define MF_GRAYED	0x1

#Define VK_LEFT	0x25
#Define VK_RIGHT	0x27
#Define VK_DOWN	0x28
#Define VK_UP	0x26

#Define TPM_RIGHTALIGN	0x8
#Define TPM_BOTTOMALIGN	0x20
#Define TPM_CENTERALIGN 0x4
#Define TPM_VCENTERALIGN 0x10
#Define TPM_NOANIMATION	0x4000

#Define IMIMF_ICON		4
#Define IMIMF_BITMAP 	8
#Define IMIMF_NOIMAGE 	16

*--- Colori
#Define NAVBAR_BORDERCOLOR	1
#Define PANELVERTICAL_BACKCOLOR	7
#Define OVERFLOWPANELBUTTON_FOCUSEDSELECTED_PICTURE	13
#Define CLOSEBUTTON_PICTURE	38
#Define CLOSEBUTTONFOCUSED_PICTURE	39
#Define TOOLBAR_BTN_HOVER 51
#Define TOOLBAR_BTN_DOWN 52
#Define TOOLBAR_BACKGRD_V 53
#Define TOOLBAR_BACKGRD_H 54
#Define TOOLBAR_BORDERCOLOR 55
#Define TOOLBARFRAME_COLOR 56
#Define MENUBAR_BTN_COLOR_BCK 57
#Define MENUBAR_BTN_COLOR_BORDER 58
#Define TOOLBAR_BTN_TEXT 59
#Define TOOLBAR_BTN_HOVER_TEXT 60
#Define TOOLBAR_BTN_DOWN_TEXT 61
#Define TOOLBAR_BTN_BORDER	62
#Define TOOLBAR_BTN_HOVER_BORDER	62
#Define TOOLBAR_BTN_DOWN_BORDER	63
#Define SEARCHMENU_HOVER 76

***********************************************************************************************
******************* COMMAND BAR ***************************************************************
***********************************************************************************************
Define Class CommandBar As Toolbar

    bNewStyle = .F.		&&True = Nuovo stile, False = vecchie toolbar

    BackImg_V = 0
    BackImg_H = 0
    CloseBtnImgML = 0
    CloseBtnImgMM = 0
    CloseBtnImgMD = 0
    BorderColor = 0
    FrameColor = Rgb(55,100,160)
    Gripper = .T.
    oldCapture = 0
    Visible = .F.
    OldVisible = .T.
    ShowModalWnd = 0

    ControlBox=.F.

    bMouseHover = .F.
    bMouseDown = .F.
    bMouseDrag = .F.	&&Indica se sono o meno su un bottone
    bNoSubMenu = .F.	&&Indica se in una menubar ho cliccato su un bottone senza submenu
    MousePosX = 0
    MousePosY = 0
    Customize = .T.		&&Abilita/Disabilita possibilit� di nascondere la toolbar con il tasto destro

    oItemHover = .Null.
    oItemSelected = .Null.

    #If Version(5)>=900
        Add Object CommandBarButtons As Collection
    #Else
        Add Object CommandBarButtons As Cp_Collection
    #Endif
    *---Propriet� Bottoni
    cntMouseHover_BorderColor = 0&&i_ThemesManager.GetProp(TOOLBAR_BTN_BORDER)
    cntMouseDown_BorderColor = 0&&i_ThemesManager.GetProp(TOOLBAR_BTN_BORDER)
    cntMouseHover_imgBck_picture = ''&&i_ThemesManager.GetProp(TOOLBAR_BTN_HOVER)
    cntMouseDown_imgBck_picture = ''&&i_ThemesManager.GetProp(TOOLBAR_BTN_DOWN)
    cntMenuSelect_BorderColor = 0&&i_ThemesManager.GetProp(MENUBAR_BTN_COLOR_BORDER )
    cntMenuSelect_imgBck_picture = ''&&i_ThemesManager.GetProp(MENUBAR_BTN_COLOR_BCK)
    HoverTextColor =  0&&i_ThemesManager.GetProp(TOOLBAR_BTN_HOVER_TEXT)
    DownTextColor = 0&&i_ThemesManager.GetProp(TOOLBAR_BTN_DOWN_TEXT)
    TextColor = 0&&i_ThemesManager.GetProp(TOOLBAR_BTN_TEXT)

    cCBName = ''

    bNotifyResize = .T.	&&Notifica resize su afterdock

    Procedure Init(bStyle)
        With This
            .bNewStyle = m.bStyle
            If .bNewStyle
                i_ThemesManager.LockVfp(.T.)
                .cCBName = 'CB_'+Sys(2015)
                Local cMacro
                cMacro = This.cCBName
                Public &cMacro
                &cMacro = This
                #If Version(5)<700
                    * --- ricerco l'handle della toolbar attuale...
                    .HWnd = TakeWindow(This.Name)
                #Endif
                cb_HookCommandBar(.HWnd, .cCBName)
                cb_ReflectActionObj(.HWnd, DW_VFPTOOLBAR, "This")
                cb_ReflectActionBool(.HWnd, DW_ISMENU, .F.)
                *--- Aggiungo toolbar alla lista toolbar
                i_ThemesManager.AddToolbar(This, .cCBName)
                *Catturo l'evento cambio tema del ThemeManager
                i_ThemesManager.AddInstance(This, "_ChangeTheme")
                .ChangeTheme()
                .ControlBox=.T.
                i_ThemesManager.LockVfp(.F.)
            Endif
        Endwith
        DoDefault()
    Endproc

    Procedure Destroy
        If This.bNewStyle
            Local cMacro
            cMacro = This.cCBName
            &cMacro = .Null.
            Release (cMacro)
            cb_Destroy(This.HWnd)
            *Rilascio la verifica del messaggio di cambio tema
            If Vartype(i_ThemesManager)='O' And !Isnull(i_ThemesManager)
                i_ThemesManager.RemoveInstance(This,"_ChangeTheme")
                i_ThemesManager.RemoveToolbar(This.cCBName)
            Endif
        Endif
        DoDefault()
        *--- Notifico il resize
        If Vartype(i_ThemesManager) = 'O' And This.bNotifyResize
            i_ThemesManager.Resize()
        Endif
    Endproc

    Procedure _ChangeTheme
        Local bVisible, nDock
        With This
            m.bVisible = .Visible
            .Visible = .F.
            .ChangeTheme()
            *--- Refresh delle bitmap
            .Visible = m.bVisible
        Endwith
    Endproc

    Procedure ChangeTheme
		local nCheckNewTheme
        With This
        	i_ThemesManager.CreateCursorPropFromXML()
			m.nCheckNewTheme=i_ThemesManager.GetProp(TOOLBAR_BACKGRD_V)
			If vartype(m.nCheckNewTheme)='N' and m.nCheckNewTheme=-1
				Local l_OldErr, l_err
				l_err = .F.
				l_OldErr = On("error")
				On Error l_err=.T.
				L_FH = FCREATE(ADDBS(ALLTRIM(i_ThemesManager.Imgpath))+"bDelGraphics.fla" )
				FCLOSE(L_FH)
				i_ThemesManager.remove_graphics(.t.,.t.)
				i_ThemesManager.Init_ThemesArray(.T.)			
				On Error &l_OldErr
				If l_err
					cp_errormsg("Impossibile eliminare dati impostazioni grafiche")
					killprocess()
				Endif			
			Endif
            .BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
            .BorderColor = i_ThemesManager.GetProp(TOOLBAR_BORDERCOLOR)
            .FrameColor = i_ThemesManager.GetProp(TOOLBARFRAME_COLOR)
            .BackImg_V = LoadPicture(i_ThemesManager.GetProp(TOOLBAR_BACKGRD_V))
            .BackImg_H = LoadPicture(i_ThemesManager.GetProp(TOOLBAR_BACKGRD_H))
            Local cBmp, cTmpBmp
            m.cBmp = i_ThemesManager.GetProp(CLOSEBUTTON_PICTURE)
            m.cTmpBmp=Addbs(i_ThemesManager.ImgPath)+Sys(2015)+'.bmp'
            i_ThemesManager.GetThemePicture(m.cBmp, m.cTmpBmp, 16,16, .FrameColor, Rgb(255,255,255))

            .CloseBtnImgML =  LoadPicture(m.cTmpBmp)
            .CloseBtnImgMM =  LoadPicture(i_ThemesManager.GetProp(CLOSEBUTTONFOCUSED_PICTURE))
            .CloseBtnImgMD =  LoadPicture(m.cTmpBmp)
            Delete File (m.cTmpBmp)

            *---Propriet� bottoni
            .cntMouseHover_BorderColor = i_ThemesManager.GetProp(TOOLBAR_BTN_BORDER)
            .cntMouseDown_BorderColor = i_ThemesManager.GetProp(TOOLBAR_BTN_BORDER)
            .cntMouseHover_imgBck_picture = i_ThemesManager.GetProp(TOOLBAR_BTN_HOVER)
            .cntMouseDown_imgBck_picture = i_ThemesManager.GetProp(TOOLBAR_BTN_DOWN)
            .cntMenuSelect_BorderColor = i_ThemesManager.GetProp(MENUBAR_BTN_COLOR_BORDER)
            .cntMenuSelect_imgBck_picture = i_ThemesManager.GetProp(MENUBAR_BTN_COLOR_BCK)
            .HoverTextColor = i_ThemesManager.GetProp(TOOLBAR_BTN_HOVER_TEXT)
            .DownTextColor = i_ThemesManager.GetProp(TOOLBAR_BTN_DOWN_TEXT)
            .TextColor = i_ThemesManager.GetProp(TOOLBAR_BTN_TEXT)
            Local oCtrl, l_i
            For Each oCtrl In This.Controls
                If Inlist(Upper(m.oCtrl.Class), "COMMANDBARBUTTON", "COMMANDBARIMAGE")
                    m.oCtrl.ChangeTheme()
                Endif
            Next
            m.oCtrl = .Null.
            *--- Setto stile menu
            nMenuStyle = i_ThemesManager.GetProp(118)
            IF nMenuStyle=-1
            	nMenuStyle = i_ThemesManager.ActiveTheme
            endif 
            cb_SetStylePopupMenu(nMenuStyle)
        Endwith
    Endproc

    Procedure ChangeSettings()
        Local oBtn, bOldVisible, l_i
        m.oCtrl = .Null.
        bOldVisible = This.Visible
        This.Visible = .F.
        *!*			IF TYPE("This.CommandBarButtons.class")<>'U'
        *!*				FOR l_i=1 TO This.CommandBarButtons.Count
        *!*					m.oCtrl = This.CommandBarButtons.Item(m.l_i)
        *!*					m.oCtrl.cImage = m.oCtrl.cImage
        *!*					m.oCtrl.Enabled = m.oCtrl.Enabled
        *!*				Next
        *!*			EndIf
        For Each oCtrl In This.Controls
            If Inlist(Upper(m.oCtrl.Class), "COMMANDBARBUTTON", "COMMANDBARIMAGE")
                m.oCtrl.cImage = m.oCtrl.cImage
                m.oCtrl.Enabled = m.oCtrl.Enabled
            Endif
            If Upper(m.oCtrl.Class)=="SEARCHRECENT"
                m.oCtrl.Height = i_nTBTNH+6
            Endif
        Next
        m.oCtrl = .Null.
        This.Visible = m.bOldVisible
    Endproc

    *--- Aggiunge un immagine
    Function AddImage(cName, cImg)
        *--- Aggiungo nuovo bottone
        This.AddObject(m.cName, "CommandBarImage")
        This.&cName..imgBck.Stretch = 0
        If Not Empty(m.cImg)
            This.&cName..cImage = m.cImg
        Endif
        Return This.&cName
    Endfunc

    *--- Aggiunge un bottone
    Function AddButton(cName, cClass)
        Local l_cClass
        With This
            *--- Eventualmente posso decidere io quale classe usare
            If Vartype(m.cClass)='C'
                l_cClass = m.cClass
            Else
                If .bNewStyle
                    l_cClass = "CommandBarButton"
                Else
                    l_cClass = "CPToolBtn"
                Endif
            Endif
            *--- Aggiungo nuovo bottone
            .AddObject(m.cName, l_cClass)
            This.&cName..Caption = ''
            If .bNewStyle
                .CommandBarButtons.Add(This.&cName)
                *--- Imposto i colori
                This.&cName..ChangeTheme()
            Endif
        Endwith
        Return This.&cName
    Endfunc

    Procedure RemoveButton(cName)
        Local l_oldVisible, l_oldResize
        With This
            l_oldVisible = .Visible
            l_oldResize = .bNotifyResize
            .Visible = .F.
            .RemoveObject(m.cName)
            .Visible = m.l_oldVisible
            .bNotifyResize = m.l_oldResize
        Endwith
    Endproc

    *PROCEDURE bMouseHover_Assign(xValue)
    Procedure _MouseHover(xValue, x , Y)
        With This
            .bMouseHover = m.xValue
            .MousePosX = m.x
            .MousePosY = m.y
            &&Controllo se devo illuminare un bottone
            Local oBtn, l_i, nBorderW
            m.oBtn = .Null.
            l_i=0
            If Type("This.CommandBarButtons.class")<>'U'
                For l_i=1 To .CommandBarButtons.Count
                    m.oBtn = .CommandBarButtons.Item(l_i)
                    If Vartype(m.oBtn)='O' And !Isnull(m.oBtn) And m.oBtn.Visible
                        If m.xValue And m.oBtn.Enabled And m.oBtn.IsMouseHover(.MousePosX, .MousePosY)
                            m.oBtn.cntMouseDown.Visible = m.oBtn.bMouseDown
                            m.oBtn.cntMouseHover.Visible = !.bMouseDown
                        Else
                            m.oBtn.cntMouseHover.Visible = m.oBtn.bMouseDown
                            m.oBtn.cntMouseDown.Visible = .F.
                        Endif
                        *--- Cambio colore label
                        m.oBtn.lblCaption.ForeColor = Iif(m.oBtn.cntMouseDown.Visible, m.oBtn.DownTextColor, Iif(m.oBtn.cntMouseHover.Visible, m.oBtn.HoverTextColor, m.oBtn.TextColor ))
                        *--- SplitButton
                        nBorderW = Iif(m.oBtn.cntMouseHover.Visible, 1, 0)
                        If m.oBtn.cntSplit.BorderWidth<>m.nBorderW
                            m.oBtn.cntSplit.BorderWidth = m.nBorderW
                        Endif
                    Endif
                Next
            Endif
            m.oBtn = .Null.
        Endwith
    Endproc

    *PROCEDURE bMouseDown_Assign(xValue)
    Procedure _MouseDown(xValue, x, Y)
        With This
            .bMouseDown = m.xValue
            .MousePosX = m.x
            .MousePosY = m.y
            &&Controllo se devo illuminare un bottone
            Local oBtn,l_i
            l_i=0
            m.oBtn = .Null.
            .bMouseDrag =.F.
            If Type("This.CommandBarButtons.class")<>'U'
                For l_i=1 To .CommandBarButtons.Count
                    m.oBtn = .CommandBarButtons.Item(l_i)
                    If !Isnull(m.oBtn) And m.oBtn.Visible
                        If m.oBtn.Enabled And m.oBtn.IsMouseHover(.MousePosX, .MousePosY)
                            .bMouseDrag = .T.
                            .bNoSubMenu = (m.oBtn.nButtonType = 4)
                            m.oBtn.bSplitHover = (m.oBtn.nButtonType = 2 And m.oBtn.cntSplit.IsMouseHover(.MousePosX, .MousePosY))
                            If .bMouseDown		&&Bottone premuto
                                m.oBtn.cntMouseDown.Visible = .T.
                                m.oBtn.cntMouseHover.Visible = .F.
                                m.oBtn.bMouseDown= .T.
                                Do Case
                                    Case Upper(m.oBtn.Class) == 'MENUBARBUTTON'  And m.oBtn.nButtonType <> 4 &&in caso di menu faccio subito il click
                                        .bMouseDown = .F.
                                        m.oBtn.bMouseDown= .F.
                                        m.oBtn.cntMouseDown.Visible = .F.
                                        m.oBtn.cntMenuSelect.Visible = .T.
                                        m.oBtn.lblCaption.ForeColor = m.oBtn.TextColor
                                        *--- Valorizzo indice per gestire la navigabilit� con la tastiera
                                        .nIndex = m.l_i
                                        *--- Mostro il submenu
                                        .ShowPopupMenu(m.oBtn)
                                        Return
                                    Case m.oBtn.nButtonType = 1 Or (m.oBtn.nButtonType = 2 And m.oBtn.bSplitHover)
                                        m.oBtn.bMouseDown= .F.
                                        .bMouseDown = .F.
                                        m.oBtn.cntMouseDown.Visible = .F.
                                        m.oBtn.cntSplit.BorderWidth = 0
                                        m.oBtn.cntMenuSelect.Visible = .T.
                                        m.oBtn.lblCaption.ForeColor = m.oBtn.TextColor
                                        .bMouseDrag = .F.
                                        m.oBtn.OnClick()
                                        Return
                                Endcase
                            Else
                                If m.oBtn.bMouseDown &&Click
                                    m.oBtn.bMouseDown= .F.
                                    m.oBtn.cntMouseDown.Visible = .F.
                                    m.oBtn.OnClick()
                                    Return
                                Endif
                                m.oBtn.bMouseDown= .F.
                                m.oBtn.cntMouseDown.Visible = .F.
                            Endif
                        Else
                            m.oBtn.bMouseDown= .F.
                            m.oBtn.cntMouseDown.Visible = .F.
                            m.oBtn.cntMouseHover.Visible = .F.
                        Endif
                        *--- Cambio colore label
                        m.oBtn.lblCaption.ForeColor = Iif(m.oBtn.cntMouseDown.Visible, m.oBtn.DownTextColor, Iif(m.oBtn.cntMouseHover.Visible, m.oBtn.HoverTextColor, m.oBtn.TextColor ))
                        *--- SplitButton
                        m.oBtn.cntSplit.BorderWidth = Iif(m.oBtn.cntMouseHover.Visible, 1, 0)
                    Endif
                Next
            Endif
            m.oBtn = .Null.
        Endwith
    Endproc

    Procedure ControlBox_Assign(xValue)
        If This.bNewStyle
            This.ControlBox = .F.
            cb_ReflectActionBool(This.HWnd, DW_CLOSABLE, m.xValue)
        Else
            This.ControlBox = m.xValue
        Endif
    Endproc

    Procedure Caption_Assign(xValue)
        This.Caption = m.xValue
        If This.Caption <> m.xValue And This.bNewStyle
            cb_ReflectActionStr(This.HWnd, DW_CAPTION, m.xValue)
        Endif
    Endproc

    Procedure Gripper_Assign(xValue)
        This.Gripper = m.xValue
        cb_ReflectActionBool(This.HWnd, DW_GRIPPER, m.xValue)
    Endproc

    Procedure Visible_Assign(xValue)
        This.Visible = m.xValue
        If This.bNewStyle
            cb_ReflectActionBool(This.HWnd, DW_VISIBLE, m.xValue)
        Endif
        *--- Troppo lenta
        *DOEVENTS FORCE
    Endproc

    Procedure BackColor_Assign(xValue)
        This.BackColor = m.xValue
        If This.bNewStyle
            cb_ReflectActionInt(This.HWnd, DW_BCKCOLOR, m.xValue)
        Endif
    Endproc

    Procedure BorderColor_Assign(xValue)
        This.BorderColor = m.xValue
        If This.bNewStyle
            cb_ReflectActionInt(This.HWnd, DW_BORDERCOLOR, m.xValue)
        Endif
    Endproc

    Procedure FrameColor_Assign(xValue)
        This.FrameColor = m.xValue
        cb_ReflectActionInt(This.HWnd, DW_FRAMECOLOR, m.xValue)
    Endproc

    Procedure CloseBtnImgML_Assign(xValue)
        This.CloseBtnImgML = m.xValue
        If !Isnull(This.CloseBtnImgML)
            cb_ReflectActionInt(This.HWnd, DW_CLOSEBTNML , m.xValue.handle)
        Endif
    Endproc

    Procedure CloseBtnImgMM_Assign(xValue)
        This.CloseBtnImgMM = m.xValue
        If !Isnull(This.CloseBtnImgMM)
            cb_ReflectActionInt(This.HWnd, DW_CLOSEBTNMM , m.xValue.handle)
        Endif
    Endproc

    Procedure CloseBtnImgMD_Assign(xValue)
        This.CloseBtnImgMD = m.xValue
        If !Isnull(This.CloseBtnImgMD)
            cb_ReflectActionInt(This.HWnd, DW_CLOSEBTNMD , m.xValue.handle)
        Endif
    Endproc

    Procedure BeforeDock(nLoc)
        With This
            If .bNewStyle
                If  nLoc =0 Or nLoc = 3 Or nLoc =-1
                    If Vartype(This.BackImg_H)='O' And !Isnull(This.BackImg_H)
                        cb_ReflectActionInt(This.HWnd, DW_BACKIMG, This.BackImg_H.handle)
                    Endif
                Else
                    If Vartype(This.BackImg_H)='O' And !Isnull(This.BackImg_H)
                        cb_ReflectActionInt(This.HWnd, DW_BACKIMG, This.BackImg_V.handle)
                    Endif
                Endif
                cb_ReflectActionInt(This.HWnd, DW_DOCKPOSITION, m.nLoc)
            Endif
            DoDefault(m.nLoc)
        Endwith
    Endproc

    *--- Resize NavBar
    Procedure MouseUp(nButton, nShift, nXCoord, nYCoord)
        *--- Notifico il resize
        If Vartype(i_ThemesManager) = 'O' And This.bNotifyResize
            i_ThemesManager.Resize()
        Endif
    Endproc

    Procedure AfterDock
        With This
            If .bNewStyle
                If .DockPosition<>-1
                    Local oBtn
                    m.oBtn =.Null.
                    cb_HookDockArea(.HWnd, .BackColor)
                Endif
                .bMouseDown= .F.
                *--- Notifico il resize
                If Vartype(i_ThemesManager) = 'O' And .bNotifyResize
                    i_ThemesManager.Resize()
                Endif
            Endif
            DoDefault()
        Endwith
    Endproc

    Procedure UnDock
        If This.bNewStyle
            This.bMouseDown= .F.
        Endif
    Endproc

    *!*		PROCEDURE Resize
    *!*			DODEFAULT()
    *!*			IF VARTYPE(i_ThemesManager) = 'O'
    *!*				i_ThemesManager.Resize()
    *!*			ENDIF
    *!*		ENDPROC

    Procedure ShowModalWnd_Assign(xValue)
        With This
            .ShowModalWnd = Max(m.xValue, 0)
            .Enabled = (.ShowModalWnd = 0)
        Endwith
    Endproc

    Procedure Enabled_Assign(xValue)
        With This
            If .bNewStyle
                If .Enabled <> m.xValue And Type("This.CommandBarButtons.class")<>'U'
                    .Enabled = m.xValue
                    For  Each m.oBtn In .CommandBarButtons
                        If !Isnull(m.oBtn)
                            If m.xValue
                                m.oBtn.Enabled = m.oBtn.OldEnabled
                            Else
                                m.oBtn.OldEnabled = m.oBtn.Enabled
                                m.oBtn.Enabled= .F.
                            Endif
                        Endif
                    Next
                Endif
                If Vartype(.oSearchMenu)='O'
                    .oSearchMenu.Enabled = m.xValue
                Endif
            Endif
        Endwith
    Endproc

    *--- Ridefinisco l'evento DblClick i modo da poter disabilitare lo spostamento
    Procedure DblClick
        If !This.Movable
            Nodefault
        Endif
    Endproc

    *--- Customize menu
    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If m.nButton=2 And i_VisualTheme<>-1 And Vartype(_Screen.cp_ThemesManager)=="O"
            DoDefault(nButton, nShift, nXCoord, nYCoord)
            i_ThemesManager.ShowCustomizeMenu()
        Endif
    Endproc
Enddefine


***********************************************************************************************
******************* MENU BAR****************************************************************
***********************************************************************************************
Define Class MenuBar As CommandBar
    oPopupMenu = .Null.
    bMenuHover = .F.
    MaxRecursive = 0
    bSleep = .F.
    bLoop = .F.

    nIndex = 0
    nKeyDown = 0
    MousePosXOld = -1
    MousePosYOld = -1
    bMenuActivate = .F.	&&Indica se il menu � pilotato da tastiera
    cPublicMenu = ""
    nWidthItem = 12
    nHeightItem = 0

    KeyPreview = .T.

    Procedure Init(pPopupMenu)
        DoDefault(.T.)
        This.ControlBox=.F.
        This.Customize = .F.
        *--- Nessuno sfondo
        This.BackImg_V = 0
        This.BackImg_H = 0
        *--- Inizializzo menu
        If Vartype(m.pPopupMenu)='O'
            This.oPopupMenu = m.pPopupMenu
        Endif
        *--- Non faccio disegnare lo sfondo
        cb_ReflectActionBool(This.HWnd, DW_ISMENU, .T.)
        *--- Assegno ALT+F4
        On Key Label Alt+F4 Clear Events
        On Key Label CTRL+F6 Execscript("SYS(1500, '_MWI_ROTAT', '_MWINDOW')")
    Endproc

    Procedure Destroy
        Local l_nIndex, oBtn
        m.oBtn = .Null.
        m.l_nIndex=0
        *--- Elimino i tasti mnemonici del menu
        If Type("This.CommandBarButtons.class")<>'U'
            For m.l_nIndex=1 To This.CommandBarButtons.Count
                m.oBtn	= This.CommandBarButtons.Item(m.l_nIndex)
                If !Empty(m.oBtn.cMnemonicKey)
                    l_name = "ON KEY LABEL ALT+" + m.oBtn.cMnemonicKey
                    &l_name
                Endif
            Next
        Endif
        DoDefault()
        cb_KillEndMenuTimer(This.HWnd)
        If 	!Isnull(This.oPopupMenu)
            cb_RemovePopupMenu(This.oPopupMenu.hMenu, This.oPopupMenu.HWnd)
            This.oPopupMenu.oParentObject=.Null.
            This.oPopupMenu = .Null.
        Endif
        *--- Elimino ALT+F4
        On Key Label Alt+F4
        On Key Label CTRL+F6
    Endproc

    Procedure KeyPress
        Lparameters nKeyCode, nShiftAltCtrl
        With This
            If Vartype(.oSearchMenu)=='O' And .oSearchMenu.bClick And m.nKeyCode = 13
                .oSearchMenu.OnClick()
            Endif
        Endwith
    Endproc

    Function AddButton(cName)
        With This
            .AddObject(m.cName, "MenuBarButton")
            .CommandBarButtons.Add(This.&cName)
            Local btn
            btn = This.&cName
            btn.lblCaption.FontName = .oPopupMenu.FontName
            btn.lblCaption.FontSize = .oPopupMenu.FontSize
            btn.lblCaption.FontBold = .oPopupMenu.FontBold
            btn.lblCaption.FontItalic = .oPopupMenu.FontItalic
        Endwith
        Return This.&cName
    Endfunc

    Procedure oPopupMenu_Assign(xValue)
        Local l_Lvl, l_btn, l_i, l_hSubMenu, l_oldArea, l_name, l_j
        l_oldArea = Select()
        With This
            .oPopupMenu = m.xValue
            If !Isnull(.oPopupMenu)
                .oPopupMenu.oParentObject =This
                l_i = 1
                l_j = 0
                *--- Variabile pubblica del menu
                .cPublicMenu = .cCBName
                For m.l_i = 1 To .oPopupMenu.MenuItems.Count
                    m.l_Lvl = .oPopupMenu.MenuItems.GetKey(m.l_i)
                    m.l_hSubMenu = .oPopupMenu.MenuItems.Item(m.l_i)
                    Select(.oPopupMenu.cCursorName)
                    Locate For LVLKEY = m.l_Lvl
                    If Found( )
                        l_j = l_j + 1
                        l_btn = .AddButton("M_"+Strtran(m.l_Lvl,'.','_'))
                        l_btn.LVLKEY = m.l_Lvl
                        l_btn.Caption = Cp_Translate(Alltrim(VOCEMENU))
                        l_btn.Enabled = !Skip
                        l_btn._OnClick = m.l_hSubMenu
                        l_btn.nButtonType = Iif(Vartype(m.l_hSubMenu)='N', 0, 4)
                        l_btn.Visible=.T.
                        .nWidthItem = .nWidthItem + l_btn.Width
                        .nHeightItem = Max(.nHeightItem, l_btn.Height)
                        *--- Mnemonic key
                        Local l_cKey, l_nPos
                        l_nPos = At('\<', l_btn.Caption)
                        If l_nPos>0
                            l_cKey = Upper(Substr(l_btn.Caption, l_nPos+2, 1))
                            If (Asc(l_cKey)>=65 And Asc(l_cKey)<=90) Or (Asc(l_cKey)>=48 And Asc(l_cKey)<=57)
                                l_btn.cMnemonicKey = l_cKey
                                l_name = "ON KEY LABEL ALT+" + m.l_cKey + " " + .cPublicMenu + ".OnKeyLabel(" + Transform(m.l_j) + " , '" + m.l_cKey + "')"
                                &l_name
                            Endif
                        Endif	&&l_nPos>0
                    Endif	&&FOUND( )
                Endfor
                *--- Container invisibile per allungare la toolbar quando docked
                .AddObject("oMargin", "MenuBarStretcher")
                .oMargin.BackStyle = 0
                .oMargin.BorderWidth = 0
                .oMargin.BorderColor = .BackColor
                .oMargin.Visible=.F.
                *--- SearchMenuItem
                If i_bSearchMenu
                    .AddObject("oSearchMenu", "SearchMenu", .oPopupMenu.cCursorName, "CERCA")
                    .nWidthItem = .nWidthItem + .oSearchMenu.Width
                    .oSearchMenu.Visible=.T.
                Endif
		If Type("oCpToolbar.oStatusToolbar.oSearchBox.cCursorNameMenu")='C'
		  oCpToolbar.oStatusToolbar.oSearchBox.cCursorNameMenu = .oPopupMenu.cCursorName
		ENDIF
		If Type("oCpToolbar.oStatusToolbar.oSearchBox.cCursorNameRecent")='C'
		    IF VARTYPE(cCursRecen) = "U" 
	  	      Public cCursRecen
		      cCursRecen = Sys(2015)
		      GSUT_BMR('C')
		    EndIf
		    oCpToolbar.oStatusToolbar.oSearchBox.cCursorNameRecent= m.cCursRecen
		EndIF
            Endif
        Endwith
        *--- Ripristino l'area di memoria
        Select(l_oldArea)
    Endproc

    *	PROCEDURE bMouseHover_Assign(xValue)
    Procedure _MouseHover(xValue, x, Y)
        With This
            .bMouseHover = m.xValue
            .MousePosX = m.x
            .MousePosY = m.y
            &&Controllo se devo illuminare un bottone
            If Vartype(m.oBtn)='U'
                Local oBtn
            Endif
            m.oBtn = .Null.
            Local l_nIndex
            m.l_nIndex=0
            If Type("This.CommandBarButtons.class")<>'U'
                For m.l_nIndex=1 To .CommandBarButtons.Count
                    m.oBtn = .CommandBarButtons.Item(m.l_nIndex)
                    If !Isnull(m.oBtn) And m.oBtn.Visible And (.MousePosXOld<>.MousePosX Or .MousePosYOld<>.MousePosY)
                        If  m.xValue And m.oBtn.Enabled And m.oBtn.IsMouseHover(.MousePosX, .MousePosY)
                            m.oBtn.cntMouseDown.Visible = m.oBtn.bMouseDown
                            m.oBtn.cntMenuSelect.Visible = !Isnull(.oItemSelected) And m.oBtn.nButtonType<>4
                            m.oBtn.cntMouseHover.Visible = !m.oBtn.cntMenuSelect.Visible And !.bMouseDown
                            *--- mi sposto sul menu
                            If !Isnull(.oItemSelected) And Upper(.oItemSelected.Name)<>Upper(m.oBtn.Name)
                                .oItemSelected.cntMenuSelect.Visible = .F.
                                .oItemSelected=m.oBtn
                                .nIndex = m.l_nIndex
                                cb_CloseMenu()
                                .bSleep = .F.
                                .bLoop = .T.
                                m.oBtn = .Null.
                                Return
                            Endif
                        Else
                            m.oBtn.cntMenuSelect.Visible = (m.oBtn.Enabled And (m.oBtn.bMouseDown  Or (!Isnull(.oItemSelected) And Upper(.oItemSelected.Name)=Upper(m.oBtn.Name)))) And m.oBtn.nButtonType<>4
                            m.oBtn.cntMouseHover.Visible = Iif(m.oBtn.nButtonType<>4, .F., m.oBtn.bMouseDown)
                            m.oBtn.cntMouseDown.Visible = .F.
                        Endif
                        *--- Cambio colore label
                        m.oBtn.lblCaption.ForeColor = Iif(m.oBtn.cntMenuSelect.Visible, m.oBtn.TextColor, Iif(m.oBtn.cntMouseDown.Visible, m.oBtn.DownTextColor, Iif(m.oBtn.cntMouseHover.Visible, m.oBtn.HoverTextColor, m.oBtn.TextColor )))
                    Endif
                Next
                *--- Hover su SearchMenu
                If Vartype(.oSearchMenu)='O'
                    .oSearchMenu._MouseHover(m.xValue, m.x, m.y)
                Endif
                .MousePosXOld = .MousePosX
                .MousePosYOld = .MousePosY
            Endif
            m.oBtn = .Null.
        Endwith
    Endproc

    Procedure nKeyDown_Assign(xValue)
        With This
            .nKeyDown=m.xValue
            m.l_nIndex = .GetNextPrevBtn(.nIndex, .nKeyDown==VK_LEFT)
            .oItemSelected.cntMenuSelect.Visible = .F.
            .oItemSelected = .CommandBarButtons.Item(m.l_nIndex)
            cb_CloseMenu()
            .bLoop = .T.
            .nIndex = m.l_nIndex
        Endwith
    Endproc

    Procedure bMenuActivate_Assign(xValue)
        With This
            If .ShowModalWnd=0
                .bMenuActivate = Iif(.bMenuActivate<>m.xValue, m.xValue, .F.)
                If .bMenuActivate
                    If !Empty(.cPublicMenu)
                        If Upper(On('KEY', 'ESC'))= Upper(.cPublicMenu + ".OnKeyLabel('X')")
                            Pop Key
                        Endif
                    Endif
                    Push Key
                    l_name = "ON KEY LABEL LEFTARROW " + .cPublicMenu + ".OnKeyLabel('L')"
                    &l_name
                    l_name = "ON KEY LABEL RIGHTARROW " + .cPublicMenu + ".OnKeyLabel('R')"
                    &l_name
                    l_name = "ON KEY LABEL UPARROW " + .cPublicMenu + ".OnKeyLabel('U')"
                    &l_name
                    l_name = "ON KEY LABEL DNARROW " + .cPublicMenu + ".OnKeyLabel('D')"
                    &l_name
                    l_name = "ON KEY LABEL ENTER " + .cPublicMenu + ".OnKeyLabel('E')"
                    &l_name
                    l_name = "ON KEY LABEL ESC " + .cPublicMenu + ".OnKeyLabel('X')"
                    &l_name
                    l_name = "ON KEY LABEL MOUSE " + .cPublicMenu + ".OnKeyLabel('X')"
                    &l_name
                    *--- Apro il primo menu
                    .UpdateBarHover()
                Else
                    .UpdateBarHover(.T.)
                    If !Empty(.cPublicMenu)
                        If Upper(On('KEY', 'ESC')) = Upper(.cPublicMenu + ".OnKeyLabel('X')")
                            Pop Key
                        Endif
                    Endif
                Endif
            Endif	&&.ShowModalWnd=0
        Endwith
    Endproc

    Procedure UpdateBarHover(bClear)
        Local oBtn, l_i
        m.oBtn = .Null.
        For l_i=1 To .CommandBarButtons.Count
            m.oBtn = .CommandBarButtons.Item(l_i)
            m.oBtn.cntMouseDown.Visible = .F.
            m.oBtn.cntMenuSelect.Visible = .F.
            m.oBtn.cntMouseHover.Visible = .F.
        Next
        If !bClear
            m.oBtn = .CommandBarButtons.Item(.nIndex)
            m.oBtn.cntMouseDown.Visible = .F.
            m.oBtn.cntMenuSelect.Visible = .F.
            m.oBtn.cntMouseHover.Visible = .T.
        Endif
    Endproc

    Procedure OnKeyLabel(xValue, cKey)
        Local oBtn, btn
        With This
            If .Visible
                Do Case
                    Case Vartype(m.xValue)='N'
                        *--- Mnemonic Key
                        m.btn = .Null.
                        If !Isnull(i_Curform)
                            Dimension aBtnList[1]
                            m.btn = SearchButton(i_Curform, -1, @aBtnList, i_bNoChildSearch, 0, m.cKey)
                            Release aBtnList
                        Endif
                        If Isnull(m.btn)
                            *--- Apro la voce di menu, non ci sono bottoni attivi che utilizzano la solita combinazione di tasti ALT+cKey
                            .nIndex = m.xValue
                            oBtn = .CommandBarButtons.Item(.nIndex)
                            If m.oBtn.Enabled
                                .UpdateBarHover(.T.)
                                If m.oBtn.nButtonType <> 4
                                    .ShowPopupMenu(m.oBtn)
                                Else
                                    m.oBtn.OnClick()
                                Endif
                            Endif
                        Else
                            *--- Eseguo il click del bottone della maschera
                            m.btn.Click()
                        Endif
                    Case Inlist(xValue, "E", "U","D")
                        *--- Apertura menu da tastiera
                        m.oBtn = .CommandBarButtons.Item(.nIndex)
                        .UpdateBarHover(.T.)
                        If m.oBtn.nButtonType <> 4
                            .ShowPopupMenu(m.oBtn)
                        Else
                            m.oBtn.OnClick()
                        Endif
                    Case Inlist(xValue, "L", "R")
                        *--- Navigazione sinistra, destra
                        .nIndex =  .GetNextPrevBtn(.nIndex, xValue=='L')
                        .UpdateBarHover()
                    Case xValue=="X"
                        *--- Chiusura navigabilit� menu da tastiera
                        .UpdateBarHover(.T.)
                        .bMenuActivate = .F.
                Endcase
            Endif
        Endwith
    Endproc

    Function GetNextPrevBtn(pIndex, bPrev)
        Local nCount, l_nIndex
        With This
            l_nIndex = pIndex
            m.nCount = .CommandBarButtons.Count
            If bPrev
                l_nIndex = l_nIndex - 1
                If l_nIndex <= 0
                    l_nIndex = nCount
                Endif
            Else
                l_nIndex = l_nIndex + 1
                If l_nIndex > nCount
                    l_nIndex = 1
                Endif
            Endif
            Local oBtn
            m.oBtn = .CommandBarButtons.Item(l_nIndex)
            If !(m.oBtn.Enabled)
                Return .GetNextPrevBtn(l_nIndex, bPrev)
            Endif
            Return l_nIndex
        Endwith
    Endfunc

    Procedure BeforeDock(nLoc)
        DoDefault(nLoc)
        With This
            If Type("This.CommandBarButtons.class")<>'U'
                *--- Se eseguo il dock in 1 o 2 devo ruotare verticalmente il testo
                Local oBtn, l_OldWidth, l_Rotation, l_OldWidthBar, l_Offset
                l_OldWidthBar = .Width
                m.oBtn = .CommandBarButtons.Item(1)
                m.l_Rotation = Iif(Inlist(nLoc, -1,0,3), 0, 270)
                If Vartype(m.oBtn)='O' And m.oBtn.lblCaption.Rotation<>m.l_Rotation
                    m.l_Offset = Fontmetric(6, m.oBtn.lblCaption.FontName, m.oBtn.lblCaption.FontSize)
                    m.oBtn = .Null.
                    For Each m.oBtn In .CommandBarButtons
                        If !Isnull(m.oBtn) And m.oBtn.Visible
                            m.oBtn.lblCaption.Rotation = m.l_Rotation
                            l_OldWidth = m.oBtn.lblCaption.Width
                            m.oBtn.lblCaption.Width = m.oBtn.lblCaption.Height
                            m.oBtn.lblCaption.Height = l_OldWidth
                            m.oBtn.lblCaption.Visible=.F.
                            m.oBtn.lblCaption.Visible=.T.
                            If m.l_Rotation = 270
                                m.oBtn.lblCaption.Height = m.oBtn.lblCaption.Height + m.l_Offset
                            Endif
                            l_OldWidth = m.oBtn.nOffset_W
                            m.oBtn.nOffset_W = m.oBtn.nOffset_H
                            m.oBtn.nOffset_H = l_OldWidth
                            m.oBtn.Rearrange()
                        Endif
                    Next
                    *--- Resize toolbar
                    .Width = .Height
                    .Height = l_OldWidthBar
                Endif
            Endif
            If Vartype(This.oMargin)='O'
                .oMargin.Visible = (m.nLoc <> -1)
            Endif
            .Rearrange(m.nLoc)
        Endwith
    Endproc

    Procedure Rearrange(pLoc)
        With This
            Local nLoc
            nLoc = Iif(Vartype(m.pLoc)='L', .DockPosition, m.pLoc)
            .LockScreen = .T.
            If Vartype(This.oMargin)='O'
                Do Case
                    Case (m.nLoc = 0 Or m.nLoc = 3)
                        Local l_Width
                        If Vartype(This.oSearchMenu)='O'
                            .oSearchMenu.Visible=.T.
                        Endif
                        l_Width = _vfp.Width - .nWidthItem - 2*Sysmetric(3) - 3
                        .oMargin.Height = .nHeightItem
                        .oMargin.Width = Max(m.l_Width, 0)
                    Case (m.nLoc = 1 Or m.nLoc = 2)
                        If Vartype(This.oSearchMenu)='O'
                            .oSearchMenu.Visible=.F.
                            .oMargin.Height = Max(_Screen.Height - .nWidthItem - .oSearchMenu.Width, 0)
                        Else
                            .oMargin.Height = Max(_Screen.Height - .nWidthItem , 0)
                        Endif
                        .oMargin.Width = .nHeightItem
                    Otherwise
                        If Vartype(This.oSearchMenu)='O'
                            .oSearchMenu.Visible=.T.
                        Endif
                Endcase
            Endif
            .LockScreen = .F.
        Endwith
    Endproc

    Procedure ShowPopupMenu(oBtn)
        With This
            .oItemSelected = m.oBtn
            .bLoop = .T.
            Local nFlag, l_First
            m.nFlag = 0
            l_First = .T.
            Do While .bLoop
                .bLoop = .F.
                *--- Se ho clickato subito su di un MenuItem che non ha submenu, devo lanciare subito la voce
                If .oItemSelected.nButtonType = 0 Or (Empty(.oItemSelected._OnClick) And .oItemSelected.nButtonType=3)
                    If .oItemSelected.nButtonType = 0 &&MenubarButton con submenu
                        *--- In caso di Custom MenuBarButton non posso farlo ora ma devo farlo una volta creato il popup vero e proprio (viene creato al volo ogni volta tramite OnBuildCustomList)
                        cb_SetEndMenuTimer(.HWnd, .oItemSelected._OnClick)
                    Endif
                    *--- Apro il popupmenu (la procedura di frizza dopo l'apertura del menu, ci pensera il timer a chiudere il popup se il mouse si sposta su di un altro submenu)
                    .oItemSelected.OnClick(m.nFlag, m.nFlag<>0)
                    *--- Distruggo il timer, il popup si � chiuso e non ne ho pi� bisogno
                    cb_KillEndMenuTimer(.HWnd)
                    *--- Alle aperture successive tolgo l'animazione dal menu in modo che si apra in modo istantaneo
                    m.nFlag = TPM_NOANIMATION
                Else
                    *--- Disattivo SetCapture, ho clickato fuori dal popupmenu
                    Mouse Click
                Endif
            Enddo
            l_First = .F.	&&Non sono pi� al primo giro
            .oItemSelected =.Null.
        Endwith
    Endproc
Enddefine

*--- Oggetto separatore per eseguire lo stretch del menu in modo che occupi tutto lo schermo
Define Class MenuBarStretcher As Container
    *--- Customize menu
    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        DoDefault(nButton, nShift, nXCoord, nYCoord)	&&Senza dodefault ho un problema
        This.Parent.MouseDown(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure Visible_Assign(xValue)
        If This.Visible<> m.xValue
            This.Visible = m.xValue
        Endif
    Endproc
Enddefine

*---
Define Class MenuBarButton As CommandBarButton
    LVLKEY = ''
    cMnemonicKey = ''

    &&nButtonType = 3 Custom MenuBarButton, 4 MenuItem (no submenu)
    cBuildCustomList = "This.OnBuildCustomList"	&&Funzione per popmenu personalizzato, la funzione ha per parametro un array passato per riferimento

    Procedure Init()
        This.nOffset_W = 15
        DoDefault()
    Endproc

    Procedure OnBuildCustomList(pMenu)
    Endproc

    Procedure OnClick(pFlag, bSubMenu)
        Local x, Y, uFlag, l_hMenu, l_macro, l_i, MenuWidth, l_oPopupMenu
        Local l_menu
        m.x = 0
        m.y = 0
        m.uFlag = Iif(Vartype(pFlag)<>'L', pFlag, 0)
        With This
            m.l_hMenu = ._OnClick
            m.l_oPopupMenu = .Parent.oPopupMenu
            .cntMenuSelect.Visible = .T.
            If .nButtonType = 3 &&Custom MenuBarButton
                If !Empty(.cBuildCustomList)
                    m.l_menu = Createobject("cbPopupMenu")
                    *--- La funzione contenuta in cBuildCustomList costruisce il menu custom da visualizzare
                    l_macro = .cBuildCustomList+"(@m.l_menu)"
                    &l_macro
                    m.l_hMenu = m.l_menu.oPopupMenu.hMenu
                    cb_SetEndMenuTimer(.Parent.HWnd, m.l_hMenu)
                    m.l_oPopupMenu = m.l_menu.oPopupMenu
                Else
                    *--- Non ho una funzione da lanciare che mi popoli l_menu
                    Return
                Endif
            Endif &&nButtonType=3
            If Vartype(m.l_hMenu)='N'
                *--- SubMenu
                Do Case
                    Case .Parent.DockPosition=-1
                        m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+Sysmetric(3)+1
                        m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+.Height+Sysmetric(4)+Sysmetric(34) - 2
                    Case .Parent.DockPosition=0 Or .Parent.DockPosition=3
                        m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+1
                        m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+.Height -2
                    Case .Parent.DockPosition=1
                        m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+.Width-2
                        m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+1
                    Case .Parent.DockPosition=2
                        m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+2
                        m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+1
                        m.uFlag = Bitor(m.uFlag, TPM_RIGHTALIGN)
                Endcase

                *--- Usato per navigazione menu
                .Parent.oItemSelected = This

                Local wScreen, hScreen
                m.wScreen = Sysmetric(1)
                m.hScreen = Sysmetric(2)
                m.MenuWidth = m.l_oPopupMenu.GetWidthSubMenu(.LVLKEY)
                *---Intervento per correzione errore di posizionamento dei sotto men� in caso di secondo monitor

                #Define SM_CMONITORS  80  && Number of monitors

                m.MenuHeight = GetMenuItemCount(m.l_hMenu) * Sysmetric(20)

                If GetSystemMetrics(SM_CMONITORS) = 2 &&Ho due monitor
                    If !(wScreen<m.x+m.MenuWidth And .Parent.DockPosition<>2) And m.x-m.MenuWidth>0 And .Parent.DockPosition=2
                        m.uFlag = Bitand(m.uFlag, Bitnot(TPM_RIGHTALIGN))
                        m.x = m.x+.Width-1
                    Endif

                Else && Gestione classica in caso di monitor unico
                    If wScreen<m.x+m.MenuWidth And .Parent.DockPosition<>2
                        If .Parent.DockPosition=1
                            m.uFlag = Bitor(m.uFlag, TPM_RIGHTALIGN)
                            m.x = m.x-.Width+1
                        Else
                            m.uFlag = Bitor(m.uFlag, TPM_CENTERALIGN)
                            m.x = wScreen
                        Endif
                    Else
                        If m.x-m.MenuWidth<0 And .Parent.DockPosition=2
                            m.uFlag = Bitand(m.uFlag, Bitnot(TPM_RIGHTALIGN))
                            m.x = m.x+.Width-1
                        Endif
                    Endif

                Endif

                *--- Fine Intervento per correzione errore di posizionamento dei sotto men� in caso di secondo monitor

                If hScreen<m.y + MenuHeight
                    If Inlist(.Parent.DockPosition, -1,0,3)
                        m.uFlag = Bitor(m.uFlag, TPM_BOTTOMALIGN)
                        m.y = m.y-.Height+1
                    Else
                        m.uFlag = Bitor(m.uFlag, TPM_VCENTERALIGN)
                        m.y = hScreen
                    Endif

                Endif
                m.l_oPopupMenu.ShowMenu(m.x, m.y, m.l_hMenu, m.uFlag)
                If !Isnull(.Parent.oItemSelected)
                    .Parent.oItemSelected.cntMenuSelect .Visible = .F.
                Endif
            Else
                *--- No SubMenu ma ItemMenu,m.l_hMenu contiene un comando da eseguire
                *.parent.oItemSelected = this
                m.l_oPopupMenu.IDOnClick = Val(Substr(m.l_hMenu, 3))
            Endif &&Vartype(._OnClick)='N'
            *--- Spengo la voce di menu
            .cntMenuSelect.Visible = .F.
            *--- Distruggo oggetto
            If .nButtonType = 3 And !Empty(.cBuildCustomList)
                m.l_menu=.Null.
            Endif
        Endwith
    Endproc
Enddefine

***********************************************************************************************
******************* POPUP MENU ****************************************************************
***********************************************************************************************
Define Class PopupMenu As Custom
    HWnd = 0	&&Handle finestra associata al menu
    IDOnClick = 0
    hMenu = 0
    cCursorName = ""
    cCursorNameOrig = "" &&Nome cursore originale
    cOnClickProc = "ExecScript(NAMEPROC)"
    #If Version(5)>=900
        Add Object MenuItems As Collection With KeySort=2
    #Else
        Add Object MenuItems As Cp_Collection With KeySort=2
    #Endif

    FontName = 'Tahoma'
    FontSize = 8
    FontBold = .F.
    FontItalic = .F.
    oParentObject = .Null.	&&Padre
    nMenuPopupID = 0

    Procedure Destroy
        This.oParentObject=.Null.
        Use In Select(This.cCursorName)
        cb_RemovePopupMenu(This.hMenu, This.HWnd)
        For Each l_hMenu In This.MenuItems
            If Vartype(l_hMenu)<>'C'
                DestroyMenu(l_hMenu)
            Endif
        Endfor
    Endproc

    Procedure Init(pCurName)
        With This
            .cCursorName = Sys(2015)
            If Vartype(m.pCurName)='C'
                *--- Ho gi� un cursore pronto con le informazioni del menu
                .SetCursorVMN(m.pCurName)
            Else
                *--- Creo il menu utilizzando i metodi
                .Create_CurMenu(.cCursorName)
            Endif
        Endwith
        *--- Leggo font menu, valorizza FontName FontSize FontBold FontItalic
        cb_GetMenuFont(This)
    Endproc

    * --- Crea il Cursore base per il men�
    Procedure Create_CurMenu(cCursorName)
        *--- Salvo l'area di memoria corrente
        Local l_oldArea
        l_oldArea = Select()
        Create Cursor (m.cCursorName) (VOCEMENU C (60), Level N (5,0), NAMEPROC M, Directory N (1), Enabled L, MENUID N (9,0), CPBMPNAME C (254), LVLKEY C (240), Skip L, CHECKED L)
        *--- Ripristino l'area di lavoro
        Select(m.l_oldArea)
    Endproc

    Procedure SetCursorVMN(pCursor, bReload)
        *--- Salvo l'area di memoria corrente
        Local l_oldArea
        l_oldArea = Select()
        With This
            .cCursorNameOrig = m.pCursor
            If Used(.cCursorNameOrig)
                If !m.bReload
                    If Vartype(g_MenuPopupID)="U"
                        Public g_MenuPopupID
                        g_MenuPopupID = 5000
                    Endif
                    g_MenuPopupID = g_MenuPopupID + 5000
                    .nMenuPopupID = g_MenuPopupID
                Endif
                #If Version(5)>=700
                    Select VOCEMENU, Level, Cast("Do mhit In cp_menu With ["+Strtran(Trim(Alltrim(NAMEPROC)),'#',' with ')+"],["+ Alltrim(VOCEMENU)+"]" As M) As NAMEPROC, Directory, Enabled, This.nMenuPopupID+Recno() As MENUID, CPBMPNAME, LVLKEY, This.EvalModules(Alltrim(MODULO), Enabled) As Skip, .F. As CHECKED From (m.pCursor) ;
                        ORDER By LVLKEY Into Cursor (.cCursorName) NOFILTER Readwrite
                #Else
                    Select VOCEMENU, Level, Cast("Do mhit In cp_menu With ["+Strtran(Trim(Alltrim(NAMEPROC)),'#',' with ')+"],["+ Alltrim(VOCEMENU)+"]" As M) As NAMEPROC, Directory, Enabled, This.nMenuPopupID+Recno() As MENUID, CPBMPNAME, LVLKEY, This.EvalModules(Alltrim(MODULO), Enabled) As Skip, .F. As CHECKED From (m.pCursor) ;
                        ORDER By LVLKEY Into Cursor (.cCursorName) NOFILTER
                    Cp_ReadWrite(.cCursorName)
                #Endif
            Endif
        Endwith
        *--- Ripristino l'area di lavoro
        Select(m.l_oldArea)
    Endproc

    Function EvalModules(i_cMod, i_bEnabled)
        Local PosVirg, Par1, Par2, i_cSkip
        If !Empty(m.i_cMod)
            If Upper(Left(m.i_cMod,8)) = 'G_OMENU'
                i_cMod  = Strtran(m.i_cMod,Chr(253),"'")
                PosVirg = Rat(',',m.i_cMod)
                * Estraggo l'oggetto dalla stringa i_cmod
                Par1 = Substr(m.i_cMod,9,m.PosVirg - 9)
                * Estraggo la condizione da testare sull'oggetto
                Par2 = Substr(m.i_cMod,m.PosVirg+1,Len(m.i_cMod)-(m.PosVirg+1))
                * verifico la condizione sull'oggetto
                i_cSkip=!g_Omenu.ObjInfo(m.Par1,m.Par2)
            Else
                If At('(',m.i_cMod)>0
                    i_cMod  = Strtran(m.i_cMod,"*","'")
                    i_cSkip = !Eval(m.i_cMod)
                Else
                    i_cSkip=At(','+i_cMod+',',','+i_cModules+',')=0
                Endif
            Endif  &&UPPER(LEFT(m.i_cmod,8)) = 'G_OMENU'
        Endif
        Return m.i_cSkip Or !i_bEnabled
    Endfunc

    Procedure InitMenuFromCursor()
        i_ThemesManager.LockVfp(.T.)
        Local l_hMenu, l_hSubMenu, l_hMenuList, l_MENUID, l_oldArea, l_VOCEMENU
        m.l_hMenu = 0
        m.l_hSubMenu = 0
        * --- Questa gestione � attiva solo per la nuova interfaccia (la if di compilazione solo per prevenire errori di compilazione
        #If Version(5)>=900
            l_hMenuList = Createobject("Collection")
        #Else
            l_hMenuList = Createobject("Cp_Collection")
        #Endif
        *--- Salvo l'area di lavoro
        l_oldArea = Select()
        With This
            Local l_LVLKEY, l_uFlag, l_widthMenu, l_VOCEMENU, l_bTranslateVoc
            l_bTranslateVoc=Iif(Vartype(g_PROJECTLANGUAGE)<>'C' Or Iif(Empty(i_cLanguage),g_PROJECTLANGUAGE,i_cLanguage)<>g_PROJECTLANGUAGE, .T., .F.)
            l_widthMenu = 0
            Select(.cCursorName)
            Go Top
            Scan For Not Deleted()
                l_LVLKEY = Alltrim(LVLKEY)
                l_MENUID = MENUID
                *---l_hMenuList contiene il collegamento tra padre lvlkey e hmenu
                Do Case
                    Case Directory = 1	&&Menu Item
                        l_VOCEMENU = Alltrim( VOCEMENU )
                        If Left( l_VOCEMENU ,1 )="{"
                            Private l_macro, L_OLDERR, L_NewErr
                            L_OLDERR = On("ERROR")
                            On Error L_NewErr=.T.
                            l_macro = Chrtran(l_VOCEMENU,"{}","")
                            l_VOCEMENU = &l_macro
                            On Error &L_OLDERR
                            Release l_macro, L_OLDERR, L_NewErr
                        Endif
                        l_VOCEMENU = Iif(l_bTranslateVoc, Cp_Translate(l_VOCEMENU), l_VOCEMENU)
                        m.l_hMenu = l_hMenuList.Item(Evl(Left(l_LVLKEY, Len(l_LVLKEY)-4), '001'))
                        * --- Bitor annidati per compatbilit� VFp precedenti
                        m.l_uFlag = Bitor(MF_STRING, Bitor(Iif(CHECKED, MF_CHECKED, MF_UNCHECKED),Bitor( Iif(Enabled, MF_ENABLED, MF_DISABLED), Iif(Skip, MF_GRAYED, MF_ENABLED))))
                        cb_AppendMenu(l_VOCEMENU, m.l_MENUID, m.l_hMenu, m.l_uFlag)
                        If Occurs('.',l_LVLKEY)=1 And .MenuItems.GetKey(l_LVLKEY)=0
                            *--- Livello principale
                            .MenuItems.Add("M_"+Transform(m.l_MENUID), l_LVLKEY)
                        Endif
                        Select(.cCursorName)
                        Loop
                    Case Directory = 2	&&SubMenu
                        l_VOCEMENU = Alltrim( VOCEMENU )
                        If Left( l_VOCEMENU ,1 )="{"
                            Private l_macro, L_OLDERR, L_NewErr
                            L_OLDERR = On("ERROR")
                            On Error L_NewErr=.T.
                            l_macro = Chrtran(l_VOCEMENU,"{}","")
                            l_VOCEMENU = &l_macro
                            On Error &L_OLDERR
                            Release l_macro, L_OLDERR, L_NewErr
                        Endif
                        l_VOCEMENU = Iif(l_bTranslateVoc, Cp_Translate(l_VOCEMENU), l_VOCEMENU)
                        m.l_hSubMenu = CreatePopupMenu()	&&Creo menu pupoup
                        m.l_hMenu = l_hMenuList.Item(Evl(Left(l_LVLKEY, Len(l_LVLKEY)-4), '001'))
                        * --- BITOR annidati per compatibilit� VFp precedenti
                        cb_AppendMenu(l_VOCEMENU, m.l_hSubMenu, m.l_hMenu, Bitor(MF_STRING, Bitor(MF_POPUP, Bitor(Iif(Enabled, MF_ENABLED, MF_DISABLED), Iif(Skip, MF_GRAYED, MF_ENABLED)))))
                        *--- Salvo tutti i sottomenu per poi poter inserire gli item/separetor nel giusto sottomenu
                        If l_hMenuList.GetKey(l_LVLKEY)=0
                            l_hMenuList.Add(m.l_hSubMenu, l_LVLKEY)
                        Endif
                        If Occurs('.',l_LVLKEY)=1 And .MenuItems.GetKey(l_LVLKEY)=0
                            *--- Livello principale
                            .MenuItems.Add(m.l_hSubMenu, l_LVLKEY)
                        Endif
                        Select(.cCursorName)
                        Loop
                    Case Directory = 4	&&Separetor
                        m.l_hMenu = l_hMenuList.Item(Evl(Left(l_LVLKEY, Len(l_LVLKEY)-4),'001'))
                        cb_AppendMenu("", m.l_MENUID , m.l_hMenu, MF_SEPARATOR)
                        Select(.cCursorName)
                        Loop
                    Case Directory = 0	&&Menu principale
                        If l_hMenuList.GetKey(l_LVLKEY)=0
                            .hMenu = CreatePopupMenu()	&&Creo menu pupoup
                            l_hMenuList.Add(.hMenu, l_LVLKEY)
                        Endif
                Endcase
                Select(.cCursorName)
            Endscan
            l_hMenuList = .Null.
            *--- Creo la window associata al menu, non posso usare vfp perch� non funziona la colorazione tramite ImageMenu
            .HWnd = cb_CreatePopupMenuWnd()
            *---- Associo alla finestra l'oggetto VFP
            cb_ReflectActionObj(.HWnd, DW_VFPMENU, "This")
            *--- Inizializza PopupMenu (ImageMenu)
            cb_InitPopupMenu(.HWnd, .hMenu)
            *--- Carico le picture nel menu, devo farlo dopo aver inizializzato il menu
            If Vartype(g_DisableMenuImage)='U' Or !g_DisableMenuImage
                Local l_Format, l_Picture, l_PictureOrig, l_PictureToLoad
                m.l_MENUID = 0
                Select(.cCursorName)
                Go Top
                Scan For Not Deleted() And Not Empty(CPBMPNAME) And Not (Lower(Alltrim(CPBMPNAME))=Lower( g_MNODEBMP ) Or Lower(Alltrim(CPBMPNAME))=Lower( g_MLEAFBMP ))
                    If i_bEnableCustomImage
                        l_PictureOrig = Juststem(Alltrim(CPBMPNAME)) &&ADDBS(JUSTPATH(ALLTRIM(CPBMPNAME)))+JUSTSTEM(ALLTRIM(CPBMPNAME))
                        If Left( Alltrim( m.l_PictureOrig ) ,1 )="{"
                            Private l_macro, L_OLDERR, L_NewErr
                            L_OLDERR = On("ERROR")
                            On Error L_NewErr=.T.
                            l_macro = Chrtran(m.l_PictureOrig,"{}","")
                            l_PictureOrig = &l_macro
                            On Error &L_OLDERR
                            Release l_macro, L_OLDERR, L_NewErr
                        Endif
                        l_Picture = cp_GetStdImg(m.l_PictureOrig, 'ico')
                        l_PictureToLoad = m.l_Picture+'.ico'
                        l_Format = IMIMF_ICON
                        If Empty(m.l_Picture)
                            m.l_Picture = cp_GetStdImg(m.l_PictureOrig, 'bmp')
                            l_PictureToLoad = m.l_Picture+'.bmp'
                            m.l_Format = IMIMF_BITMAP
                        Endif
                    Else
                        m.l_Format = Iif(Upper(Justext(Alltrim(CPBMPNAME)))=='BMP', IMIMF_BITMAP, IMIMF_ICON)
                        l_PictureToLoad = Alltrim(CPBMPNAME)
                    Endif
                    m.l_MENUID = MENUID
                    cb_SetMenuItemImage(.hMenu, m.l_MENUID, Fullpath(m.l_PictureToLoad), m.l_Format)
                    Select(.cCursorName)
                Endscan
            Endif
        Endwith
        i_ThemesManager.LockVfp(.F.)
        *--- Ripristino l'area di lavoro
        Select(l_oldArea)
    Endproc

    *--- Mostra il menu in posizione x,y , se non specificate usa la posizione attuale del mouse
    Procedure ShowMenu(x, Y, hSubMenu, uFlag)
        Local l_hSubMenu,  l_uFlag
        l_uFlag = Iif(Vartype(m.uFlag)='N', m.uFlag, 0)
        l_hSubMenu = Iif(Vartype(m.hSubMenu)='N', m.hSubMenu, This.hMenu)
        If m.l_hSubMenu <>0
            Local l_x, l_y
            l_x=Iif(Vartype(m.x)='N', m.x, -1)
            l_y=Iif(Vartype(m.y)='N', m.y, -1)
            cb_ShowPopupMenu(m.l_hSubMenu, m.l_x, m.l_y, This.HWnd, l_uFlag)
        Endif
    Endproc

    Procedure IDOnClick_assign(xValue)
        Local l_macro, l_oldArea
        With This
            If !Isnull(.oParentObject)
                &&.oParentObject.bMouseDrag = .F.
                .oParentObject.bMenuActivate = .F.
            Endif
            *--- Salvo l'area di lavoro
            l_oldArea = Select()
            *--- Ricerco ID del menu per recuperare il comando  ad eseguire
            Select(.cCursorName)
            Locate For MENUID = m.xValue
            If Found( )
                l_macro = .cOnClickProc
                If Type("This.oParentObject.oItemSelected")='O' And !Isnull(.oParentObject.oItemSelected)
                    .oParentObject.oItemSelected.cntMenuSelect .Visible = .F.
                Endif
                If !Isnull(.oParentObject)
                    Activate Screen
                    cb_KillEndMenuTimer(.oParentObject.HWnd)
                Endif
                *--- Eseguo voce menu
                With This
                    &l_macro
                Endwith
            Else
                If !Isnull(.oParentObject)
                    .oParentObject.bMouseDrag = .T.
                Endif
            Endif
            *--- Ripristino l'area di lavoro
            Select(m.l_oldArea)
        Endwith
    Endproc

    Function GetWidthSubMenu(pLvlKey)
        Local l_widthMenu, l_oldArea
        l_widthMenu = 0
        With This
            l_oldArea = Select()
            If !Used(.cCursorName)
                If Not Empty(.cCursorNameOrig) And Used(.cCursorNameOrig)
                    .SetCursorVMN(.cCursorNameOrig, .T.)
                    Select Max(Len(Alltrim(VOCEMENU))) As Length From (.cCursorName) Where Left(Alltrim(LVLKEY), Len(Alltrim(LVLKEY))-4)==Alltrim(m.pLvlKey) Into Cursor _TMP_Length
                Endif
            Else
                Select Max(Len(Alltrim(VOCEMENU))) As Length From (.cCursorName) Where Left(Alltrim(LVLKEY), Len(Alltrim(LVLKEY))-4)==Alltrim(m.pLvlKey) Into Cursor _TMP_Length
            Endif
            If Used('_TMP_Length')
                l_widthMenu = _TMP_Length.Length * Fontmetric(6, .FontName, .FontSize, Iif(.FontBold,'B','') + Iif(.FontItalic,'I','')) + 40	&&46 o 30 dipende se submenu o item
                Use In Select('_TMP_Length')
            Endif
        Endwith
        Select(m.l_oldArea)
        Return m.l_widthMenu
    Endfunc
Enddefine

Define Class cbPopupMenu As Custom
    Add Object oPopupMenu As PopupMenu Noinit &&Oggetto PopupMenu
    Add Object oMenuItems As cbMenuItem	&&Radice del menu
    oParentObject = .Null.	&&Oggetto di riferimento da utilizzare al momento dell'OnClick

    Function AddMenuItem()
        Return This.oMenuItems.AddMenuItem()
    Endfunc

    Procedure InitMenu(pAppend)
        Local nMenuID, l_LVLKEY, l_tmpCur, l_CountLvlKey, l_oldArea
        l_LVLKEY = ''
        nMenuID = 0
        With This
            l_oldArea = Select()
            *--- pAdd ho gi� il cursore del menu valorizzato devo andare in append
            If m.pAppend And !Empty(.oPopupMenu.cCursorName)
                l_tmpCur = Sys(2015)
                Select Max(LVLKEY) As MaxLvlKey From (.oPopupMenu.cCursorName) Where Len(Alltrim(LVLKEY))=7 Into Cursor (m.l_tmpCur)
                If Used(m.l_tmpCur)
                    l_LVLKEY = Alltrim(&l_tmpCur..MaxLvlKey)
                Endif
                Use In Select(m.l_tmpCur)
            Else
                *--- Inizializzo il PopupMenu
                .oPopupMenu.Init()
            Endif
            If Vartype(g_MenuPopupID)="U"
                Public g_MenuPopupID
                g_MenuPopupID = 5000
            Endif
            g_MenuPopupID = g_MenuPopupID + 5000
            *--- MenuID da cui partire
            nMenuID = g_MenuPopupID
            l_CountLvlKey = Iif(Empty(Nvl(m.l_LVLKEY, '')),0,Val(Right(m.l_LVLKEY, 3)))
            *--- Popolo il cursore
            .oMenuItems.InitMenuItem('001', .oPopupMenu.cCursorName, m.l_CountLvlKey)
            *--- Ordino il cursore
            Select * From (.oPopupMenu.cCursorName) Order By LVLKEY Into Cursor (.oPopupMenu.cCursorName)
            #If Version(5)>=700
                Select VOCEMENU, Level, NAMEPROC, Directory, Enabled, m.nMenuID + Recno() As MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED;
                    FROM (.oPopupMenu.cCursorName) Order By LVLKEY Into Cursor (.oPopupMenu.cCursorName)	 Readwrite
            #Else
                Select VOCEMENU, Level, NAMEPROC, Directory, Enabled, m.nMenuID + Recno() As MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED;
                    FROM (.oPopupMenu.cCursorName) Order By LVLKEY Into Cursor (.oPopupMenu.cCursorName)
                Cp_ReadWrite(.oPopupMenu.cCursorName)
            #Endif
            *--- Ripristino l'area di memoria
            Select(m.l_oldArea)
            *--- Creo il menu (API)
            .oPopupMenu.InitMenuFromCursor()
        Endwith
    Endproc

    Procedure ShowMenu(x, Y, uFlag)
        This.oPopupMenu.ShowMenu(m.x, m.y, .F., m.uFlag)
    Endproc

    Procedure Destroy
        This.oPopupMenu.Destroy()
        This.oParentObject = .Null.
    Endproc
Enddefine

Define Class cbMenuItem As Custom
    Caption = ""		&&Caption del Item/SubMenu
    SkipFor = ".F."		&&Se la condizione � vera Item disabilitato
    MarkFor = ".F."	&&Se la condizione � vera Item con spunta
    Picture = ""		&&Bitmap
    Visible = .T.		&&Mostra/Nasconde la voce
    OnClick = ""		&&Operazione da eseguire al click
    BeginGroup = .F. &&Separator
    Description = ""	&&Descrizione statusbar

    cLevelKey = ''	&&Chiave menu item
    #If Version(5)>=900
        Add Object cbMenuItems As Collection	&&Item figli
    #Else
        Add Object cbMenuItems As Cp_Collection	&&Item figli
    #Endif

    *--- Aggiunge una nuova voce di emnu
    Function AddMenuItem()
        With This
            Local l_cName, l_MenuItem, l_nNumItems
            l_nNumItems = .cbMenuItems.Count
            m.l_cName = "M"+Right("0000000000"+Transform(l_nNumItems),10)	&&Nome del nuovo Item
            .AddObject(m.l_cName, "cbMenuItem")	&&Aggiunge il nuovo Item come figlio
            m.l_cName = "This."+m.l_cName
            m.l_MenuItem = &l_cName
            .cbMenuItems.Add(m.l_MenuItem)
        Endwith
        Return m.l_MenuItem
    Endfunc

    *--- Inizializza il menu API, richiamato da InitMenu del padre
    Procedure InitMenuItem(cLvlKey, cCursor, pCountLvlKey)
        Local l_LVLKEY, l_Count
        m.l_Count = Iif(Vartype(pCountLvlKey)='N', pCountLvlKey, 0)
        m.l_LVLKEY = m.cLvlKey+'.'
        With This
            .cLevelKey = m.cLvlKey
            If .cbMenuItems.Count>0
                Local l_MenuItem
                For Each m.l_MenuItem In .cbMenuItems
                    m.l_Count = m.l_Count + 1
                    If m.l_MenuItem.BeginGroup
                        .InsertMenuItem(m.cCursor, 4, m.l_LVLKEY + Right('000'+Alltrim(Str(m.l_Count)),3))
                        m.l_Count = m.l_Count + 1
                    Endif
                    m.l_MenuItem.InitMenuItem(m.l_LVLKEY + Right('000'+Alltrim(Str(m.l_Count)),3), m.cCursor)
                Next
                m.l_MenuItem = .Null.
                .InsertMenuItem(m.cCursor, Iif(m.cLvlKey=='001',0,2), m.cLvlKey)
            Else
                .InsertMenuItem(m.cCursor, Iif(m.cLvlKey=='001',0,1), m.cLvlKey)
            Endif
        Endwith
    Endproc

    Procedure InsertMenuItem(cCursor, nDir, cLvlKey)
        With This
            Local l_Skip, l_Mark
            l_Skip = Iif(Empty(.SkipFor), ".F.", .SkipFor)
            l_Mark = Iif(Empty(.MarkFor), ".F.", .MarkFor)
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (.Caption, 0, .OnClick, m.nDir, .T., 0, .Picture, m.cLvlKey, &l_Skip, &l_Mark)
        Endwith
    Endproc

    Procedure Caption_Assign(xValue)
        This.Caption = Strtran(m.xValue, '\<', "&")
    Endproc

    Procedure Picture_Assign(xValue)
        Local nDimension
        If !Empty(m.xValue)
            m.cImg = cp_GetStdImg(Forceext(m.xValue, ''), Justext(m.xValue))
            This.Picture = Forceext(m.cImg, Justext(m.xValue))
        Endif
    Endproc

Enddefine
***********************************************************************************************
******************* COMMAND BAR BUTTON ********************************************************
***********************************************************************************************
Define Class CommandBarButton As Container
    Add Object cntMouseHover As cbContainer With BorderColor = Rgb(255,189,105)
    Add Object cntMouseDown As cbContainer With BorderColor = Rgb(251,140,60)
    Add Object cntMenuSelect As cbContainer With BorderColor = Rgb(101,147,207)
    Add Object imgDeactivate As cbImage
    Add Object imgActivate As cbImage
    Add Object lblCaption As cbLabel
    *--- SplitButton
    Add Object cntSplit As cbSplit With Visible=.F., Top = 1, Width = 12

    Caption = ""
    cImage = ""
    bMouseDown = .F.
    BackStyle=0
    BorderWidth=0
    Enabled = .T.
    OldEnabled = .F.
    _OnClick=''
    _OnRightClick=''
    nOffset_W = 7
    nOffset_H = 6

    cComment = '' &&DeskToolbar
    HoverTextColor =  0
    DownTextColor = 0
    TextColor = 0

    nButtonType = 0		&& 0 = Standard, 1 =  ButtonPopup, 2 = SplitButtonPopup, 3 Custom Menu, 4 MenuItem (no submenu) (3,4 usati solo per MenuBarButton)
    oPopupMenu = .Null.
    bSplitHover = .F.

    Procedure Init
        This.ChangeTheme()
    Endproc

    Procedure Picture_Assign(xValue)
        This.cImage = m.xValue
    Endproc

    Procedure ChangeTheme()
        With This
            .cntMouseHover.BorderColor = .Parent.cntMouseHover_BorderColor
            .cntMouseDown.BorderColor = .Parent.cntMouseDown_BorderColor
            .cntMouseHover.imgBck.Picture = .Parent.cntMouseHover_imgBck_picture
            .cntMouseDown.imgBck.Picture = .Parent.cntMouseDown_imgBck_picture
            .cntMenuSelect.BorderColor = .Parent.cntMenuSelect_BorderColor
            .cntMenuSelect.imgBck.Picture = .Parent.cntMenuSelect_imgBck_picture
            .cntSplit.BorderColor = .Parent.cntMouseHover_BorderColor
            .HoverTextColor =  .Parent.HoverTextColor
            .DownTextColor = .Parent.DownTextColor
            .TextColor = .Parent.TextColor
            .lblCaption.ForeColor = .TextColor
            If !Empty(.cImage)
                .cImage = .cImage
            Endif
        Endwith
    Endproc

    Function IsMouseHover(x, Y)
        With This
            *--- SplitButton
            Return (m.x>.Left And m.x<.Left+.Width And m.y>.Top And m.y< .Top+.Height)
        Endwith
    Endfunc

    Procedure Caption_Assign(xValue)
        Local l_oldResize
        With This
            l_oldResize = .Parent.bNotifyResize
            .Parent.bNotifyResize = .F.
            .Caption = Strtran(m.xValue, '&', '\<')
            .lblCaption.Caption = .Caption
            .Rearrange()
            .Parent.bNotifyResize = m.l_oldResize
        Endwith
    Endproc

    Procedure Visible_Assign(xValue)
        With This
            .Parent.LockScreen=.T.
            .Visible=m.xValue
            .Parent.LockScreen=.F.
        Endwith
    Endproc

    Procedure cImage_Assign(xValue)
        Local nDimension, l_oldVisible, l_oldResize
        With This
            l_oldResize = .Parent.bNotifyResize
            .Parent.bNotifyResize = .F.
            l_oldVisible = .Parent.Visible
            .Parent.Visible=.F.
            .cImage = m.xValue
            If i_bMobileMode And Sysmetric(1)<=800
                i_nTBTNH = Min(i_nTBTNH, 32)
            Endif
            Do Case
                Case i_nTBTNH>40
                    m.nDimension = 48
                Case i_nTBTNH>30 And i_nTBTNH<40
                    m.nDimension = 32
                Case i_nTBTNH>22
                    m.nDimension = 24
                Otherwise
                    m.nDimension = 16
            Endcase
            Local cImg
            m.cImg = i_ThemesManager.RetBmpFromIco(.cImage, m.nDimension)
            .imgActivate.Picture = ''
            .imgDeactivate.Picture = ''
            If !Empty(m.cImg)
                .imgActivate.Picture = m.cImg+ '.bmp'
                .imgDeactivate.Picture = i_ThemesManager.ColorizeBmp(m.cImg, m.nDimension) + '.bmp'
            Endif
            .Rearrange()
            .Enabled = .Enabled
            .Parent.Visible=m.l_oldVisible
            .Parent.bNotifyResize = m.l_oldResize
        Endwith
    Endproc

    Procedure Rearrange
        With This
            .Width = Max(.lblCaption.Width, .imgActivate.Width) + .nOffset_W
            .Height = Iif(Empty(.Caption),0,.lblCaption.Height) + .imgActivate.Height + .nOffset_H
            .cntMouseHover.Rearrange()
            .cntMouseDown.Rearrange()
            .cntMenuSelect.Rearrange()
            If Empty(.cImage)
                .lblCaption.Top = Int((.Height - .lblCaption.Height+3)/2)
            Else
                .imgActivate.Top = Int((.Height - .imgActivate.Height - Iif(!Empty(.Caption), .lblCaption.Height, 0))/2)
                .imgDeactivate.Top = .imgActivate.Top
                .lblCaption.Top = .imgActivate.Top + .imgActivate.Height
            Endif
            .lblCaption.Left = Int((.Width - .lblCaption.Width+3) / 2)
            .imgActivate.Left = Int((.Width - .imgActivate.Width) / 2)
            .imgDeactivate.Left = .imgActivate.Left
            *--- ---*
            If .nButtonType = 2
                .cntSplit.Rearrange()
                .cntSplit.Left = .Width-1
                .Width = .Width + .cntSplit.Width
                .cntMouseHover.Rearrange()
                .cntMouseDown.Rearrange()
                .cntMenuSelect.Rearrange()
                .cntSplit.Visible = .T.
            Else
                .cntSplit.Visible = .F.
            Endif
        Endwith
    Endproc

    Procedure OnClick()
        With This
            Do Case
                Case .nButtonType = 0 Or (.nButtonType = 2 And !.bSplitHover)
                    If Not Empty(This._OnClick)
                        Local l_OnClick
                        m.l_OnClick = This._OnClick
                        *Createobject("tExecCmdMenu", 1, This, l_OnClick)
                        &l_OnClick
                    Endif
                Case .nButtonType = 1 Or (.nButtonType = 2 And .bSplitHover)
                    If !Isnull(.oPopupMenu)
                        Local x, Y, uFlag
                        uFlag = 0
                        Do Case
                            Case .Parent.DockPosition=-1
                                m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+Sysmetric(3)+1
                                m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+.Height+Sysmetric(4)+Sysmetric(34) - 2
                            Case .Parent.DockPosition=0 Or .Parent.DockPosition=3
                                m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+1
                                m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+.Height -2
                            Case .Parent.DockPosition=1
                                m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+.Width-2
                                m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+1
                            Case .Parent.DockPosition=2
                                m.x = cb_GetWindowLeft(.Parent.HWnd)+.Left+2
                                m.y = cb_GetWindowTop(.Parent.HWnd)+.Top+1
                                m.uFlag = Bitor(m.uFlag, TPM_RIGHTALIGN)
                        Endcase
                        .oPopupMenu.ShowMenu(m.x, m.y, m.uFlag)
                        .cntMenuSelect.Visible = .F.
                    Endif
            Endcase
        Endwith
    Endproc

    Procedure ToolTipText_Assign(xValue)
        With This
            .ToolTipText = m.xValue
            .cntMouseHover.ToolTipText = m.xValue
            .cntMouseDown.ToolTipText = m.xValue
            .imgDeactivate.ToolTipText = m.xValue
            .imgActivate.ToolTipText = m.xValue
            .lblCaption.ToolTipText = m.xValue
        Endwith
    Endproc

    Procedure Enabled_Assign(xValue)
        With This
            .Enabled = m.xValue
            If !Empty(.cImage)
                .imgDeactivate.Visible = !m.xValue
                .imgActivate.Visible = m.xValue
            Endif
            .lblCaption.Enabled = m.xValue
        Endwith
    Endproc

    Procedure Click()
        *--- Non esegue nulla
    Endproc

    Procedure RightClick()
        If Not Empty(This._OnRightClick)
            Local l_OnRightClick
            m.l_OnRightClick = This._OnRightClick
            &l_OnRightClick
        Endif
    Endproc

    Procedure nButtonType_Assign(xValue)
        Local l_oldResize, l_oldVisible
        With This
            .nButtonType = m.xValue
            l_oldResize = .Parent.bNotifyResize
            l_oldVisible = .Parent.Visible
            .Parent.Visible=.F.
            .Parent.bNotifyResize = .F.
            .Rearrange()
            .Enabled = .Enabled
            .Parent.Visible=m.l_oldVisible
            .Parent.bNotifyResize = m.l_oldResize
        Endwith
    Endproc
Enddefine

*******************************************************************************
Define Class cbLabel As Label
    AutoSize = .T.
    Caption = ""
    Visible = .F.
    Width = 0
    Height = 0
    FontName = 'Tahoma'
    FontSize = 8
    BackStyle = 0

    Procedure Caption_Assign(xValue)
        With This
            .Visible=.F.
            .AutoSize = .F.
            .Width = 0
            .Height = 0
            .Caption = m.xValue
            If !Empty(m.xValue)
                .AutoSize = .T.
                .Visible = .T.
            Endif
        Endwith
    Endproc

    *--- Se il valore � lo stesso non faccio nulla, altrimenti sul mousemove la toolbar viene ridisegnata e vedo lampeggiare le caption
    Procedure Forecolor_Assign(xValue)
        With This
            If .ForeColor <> m.xValue
                .ForeColor = m.xValue
            Endif
        Endwith
    Endproc

    Procedure RightClick()
        This.Parent.RightClick()
    Endproc
Enddefine

*******************************************************************************
Define Class cbImage As Image
    Visible = .F.
    Width = 0
    Height = 0
    Top = 3
    Left = 3
    BackStyle = 0

    Procedure Picture_Assign(xValue)
        With This
            .Visible = .F.
            .Picture = ""
            .Picture = m.xValue
            .Width = 0
            .Height = 0
            .Stretch = 0 &&Clip
            If !Empty(m.xValue)
                .Visible = .T.
                .Top = 3
            Endif
        Endwith
    Endproc

    Procedure RightClick()
        This.Parent.RightClick()
    Endproc
Enddefine

*******************************************************************************
Define Class cbContainer As Container
    Top = 1
    Left = 1
    BackStyle = 1
    BorderWidth = 1
    Visible=.F.

    Add Object imgBck As Image With Top = 1, Left = 1, Stretch = 2

    Procedure Visible_Assign(xValue)
        If m.xValue<>This.Visible
            This.Visible = m.xValue
        Endif
    Endproc

    Procedure Rearrange
        With This
            .Width = .Parent.Width - .Left-1
            .Height = .Parent.Height - .Top-1
            .imgBck.Width = .Width - 2*.BorderWidth
            .imgBck.Height = .Height - 2*.BorderWidth
        Endwith
    Endproc

    Procedure RightClick()
        This.Parent.RightClick()
    Endproc
Enddefine

Define Class cbSplit As Container
    Top = 1
    Left = 1
    BackStyle = 0
    BorderWidth = 0
    Visible=.F.

    Add Object imgPopup As Image With Top = 2, Left = -1, Stretch = 2, Width = 16, Height = 16

    Procedure Init
        With This
            i_ThemesManager.PutBmp(.imgPopup, "OVERFLOWPANEL_MENUBUTTON_PICTURE.Bmp", 16)
        Endwith
    Endproc

    Procedure Rearrange
        With This
            .Width = 12
            .Height = .Parent.Height - .Top-1
            .imgPopup.Top = Int((.Height - .imgPopup.Height)/2)
        Endwith
    Endproc

    Function IsMouseHover(x, Y)
        With This
            *--- SplitButton
            Return (m.x>.Parent.Left+.Left And m.x<.Parent.Left+.Left+.Width And m.y>.Parent.Top+.Top And m.y<.Parent.Top+.Top+.Height)
        Endwith
    Endfunc

Enddefine

*********************************************************************************
Define Class CommandBarImage As cbContainer
    cImage = ""
    nOffset_W = 7
    nOffset_H = 6
    BackStyle = 0
    BorderWidth = 0
    imgBck.Stretch = 0
    imgBck.BackStyle = 0

    Procedure Rearrange
        With This
            .Width = .imgBck.Width + .nOffset_W
            .Height = .imgBck.Height + .nOffset_H
            .imgBck.Top = Int((.Height - .imgBck.Height)/2)
            .imgBck.Left = Int((.Width - .imgBck.Width) / 2)
        Endwith
    Endproc

    Procedure cImage_Assign(xValue)
        Local nDimension, l_oldVisible, l_oldResize
        With This
            l_oldResize = .Parent.bNotifyResize
            .Parent.bNotifyResize = .F.
            l_oldVisible = .Parent.Visible
            .Parent.Visible=.F.
            .cImage = m.xValue
            Do Case
                Case i_nTBTNH>30
                    m.nDimension = 32
                Case i_nTBTNH>22
                    m.nDimension = 24
                Otherwise
                    m.nDimension = 16
            Endcase
            Local cImg
            m.cImg = i_ThemesManager.RetBmpFromIco(.cImage, m.nDimension)
            .imgBck.Picture = ''
            If !Empty(m.cImg)
                .imgBck.Picture = m.cImg+ '.bmp'
            Endif
            .Rearrange()
            .Enabled = .Enabled
            .Parent.Visible=m.l_oldVisible
            .Parent.bNotifyResize = m.l_oldResize
        Endwith
    Endproc

    Procedure Visible_Assign(xValue)
        With This
            .Parent.LockScreen=.T.
            .Visible=m.xValue
            .Parent.LockScreen=.F.
        Endwith
    Endproc

    Procedure ChangeTheme
        If !Empty(This.cImage)
            This.cImage = This.cImage
        Endif
    Endproc
Enddefine

*********************************************************************************
*--- Draw_adder
Define Class draw_adder_cb As CommandBarButton
    cName=''
    cType=''
    Ndx=0
    nDY=0
    cMacro=''  &&Macro da eseguire sul oggetto aggiunto, utilizzato per valorizzare una propriet� dell'elemento aggiunto (painter_mediabtn)
Enddefine

**********************************************************************************
*--- Btn Combo
Define Class BtnSearch As Container
    Add Object ImgBtn As Image With Top = 0, BorderStyle=0, BorderColor = Rgb(255,255,255), BackStyle = 0, Top = 3, Left = 0
Enddefine

*--- Ricerca voci di menu, textbox che apre un menu con tutte le voci di menu che hanno nella descrizione la parola indicata
Define Class SearchMenu As Container
    BackStyle = 0
    BorderWidth = 0
    oToolbar = .Null.
    EmptySearch = .F.

    Add Object oTextToSearch As TextBox With Left=0, Top = 0, Width = 130, SpecialEffect = 1, BorderColor = Rgb(255,255,255), FontItalic = .T.,;
        ForeColor = Rgb(128,128,255), Value = MSG_SEARCH, EmptySearch = .T.
    Add Object oBtnSearch As BtnSearch With Top = 0, BorderColor = Rgb(255,255,255)

    cCursorName = '' &&Nome cursore su cui ricercare le voci
    bClick = .F.
    cTipo = '' && valori possibili "CERCA" oppure "RECENTI"

    Procedure Init(pCursor,pTipo)
        With This
            If Empty(pTipo)
                *** se non passo il parametro usa la vecchia classe
                .cTipo = "CERCA"
            Else
                .cTipo = pTipo
            Endif
            .oBtnSearch.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
            .Rearrange()
            If .cTipo="RECENTI" Then
                .oTextToSearch.ToolTipText = Cp_Translate("Voce dei recenti da cercare")
                i_ThemesManager.PutBmp(.oBtnSearch.ImgBtn, "OVERFLOWPANEL_MENUBUTTON_PICTURE.Bmp", 16)
                .oBtnSearch.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
                .oBtnSearch.ToolTipText = Cp_Translate("Recenti")
                .cCursorName = Iif(Vartype(m.pCursor)='C', m.pCursor, '')
                .oTextToSearch.Value = Iif(isalt(),MSG_RECENT_PR, MSG_RECENT)
                .oToolbar = .Parent.Parent
                .EmptySearch = .T.
            Else
                .oTextToSearch.ToolTipText = Cp_Translate("Voce di menu da cercare (minimo tre caratteri)")
                i_ThemesManager.PutBmp(.oBtnSearch.ImgBtn, "OVERFLOWPANEL_MENUBUTTON_PICTURE.Bmp", 16)
                .oBtnSearch.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
                .oBtnSearch.ToolTipText = Cp_Translate("Cerca")
                .cCursorName = Iif(Vartype(m.pCursor)='C', m.pCursor, '')
                .oTextToSearch.Value = MSG_SEARCH
                .oToolbar = .Parent
            Endif
        Endwith
    Endproc

    Procedure Visible_Assign(xValue)
        If This.Visible<>m.xValue
            This.Visible = m.xValue
        Endif
    Endproc

    Function IsMouseHover(x, Y)
        With This
            Return (m.x>.Left And m.x<.Left+.Width And m.y>.Top And m.y< .Top+.Height)
        Endwith
    Endfunc

    Procedure Enabled_Assign(xValue)
        With This
            .Enabled = m.xValue
            .oTextToSearch.Enabled = m.xValue
            .oBtnSearch.Enabled = m.xValue
        Endwith
    Endproc

    Procedure Rearrange
        With This
            .oBtnSearch.Top = .oTextToSearch.Top
            .oBtnSearch.Left = .oTextToSearch.Left + .oTextToSearch.Width - 1
            .Height = .oTextToSearch.Height
            .oBtnSearch.Width = 16
            .oBtnSearch.Height = .Height
            .Width = .oTextToSearch.Width + .oBtnSearch.Width
        Endwith
    Endproc

    Procedure _MouseHover(xValue, x, Y)
        With This
            If !.bClick
                If m.xValue And .IsMouseHover(m.x, m.y) And .Enabled
                    .oTextToSearch.BorderColor = i_ThemesManager.GetProp(TOOLBAR_BTN_HOVER_BORDER)
                    .oBtnSearch.BackColor = i_ThemesManager.GetProp(SEARCHMENU_HOVER)
                    .oBtnSearch.BorderColor = .oTextToSearch.BorderColor
                Else
                    .oBtnSearch.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
                    .oTextToSearch.BorderColor = Rgb(255,255,255)
                    .oBtnSearch.BorderColor = Rgb(255,255,255)
                Endif
            Endif
        Endwith
    Endproc

    Procedure oTextToSearch.GotFocus
        This.Parent.bClick = .T.
        This.FontItalic = .F.
        This.ForeColor = Rgb(0,0,0)
        This.Value = ""
        This.EmptySearch= .F.
    Endproc
	
    Procedure oTextToSearch.LostFocus
        This.Parent.bClick = .F.
        This.Parent._MouseHover(.F.)
        This.FontItalic = .T.
        This.ForeColor = Rgb(128,128,255)
        This.Value = Iif(This.Parent.cTipo="RECENTI", Iif(isalt(),MSG_RECENT_PR, MSG_RECENT), MSG_SEARCH)
        This.EmptySearch= .T.
    Endproc
	
    Procedure oTextToSearch.valid
		If this.parent.parent.class<>'Menubar'
			this.parent.OnClick()
		endif
    Endproc	

    Procedure oTextToSearch.MouseEnter(nButton, nShift, nXCoord, nYCoord)
        With This.Parent
            If .cTipo="RECENTI"
                ._MouseHover(.T., nXCoord-.Parent.Left, nYCoord-.Parent.Top)
            Endif
        Endwith
    Endproc

    Procedure oTextToSearch.MouseLeave(nButton, nShift, nXCoord, nYCoord)
        With This.Parent
            If .cTipo="RECENTI"
                ._MouseHover(.F., nXCoord-.Parent.Left, nYCoord-.Parent.Top)
            Endif
        Endwith
    Endproc

    Procedure oBtnSearch.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.OnClick()
    Endproc

    Procedure oBtnSearch.ImgBtn.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Parent.OnClick()
    Endproc

    Procedure oBtnSearch.ImgBtn.MouseEnter(nButton, nShift, nXCoord, nYCoord)
        With This.Parent.Parent
            If .cTipo="RECENTI"
                ._MouseHover(.T., nXCoord-.Parent.Left, nYCoord-.Parent.Top)
            Endif
        Endwith
    Endproc

    Procedure oBtnSearch.ImgBtn.MouseLeave(nButton, nShift, nXCoord, nYCoord)
        With This.Parent.Parent
            If .cTipo="RECENTI"
                ._MouseHover(.F., nXCoord-.Parent.Left, nYCoord-.Parent.Top)
            Endif
        Endwith
    Endproc

    *--- Costruisce il path della vece di menu
    Function RetPath(cLvlKey, cVoceMenu,cCursor)
        Local l_cLVLKEY, l_oldArea, l_cCursor, l_Ret, l_i, l_max, l_nOccurs
        l_oldArea = Select()
        l_Ret = ""
        l_cCursor = Sys(2015)
        l_cLVLKEY = Alltrim(m.cLvlKey)
        l_cLVLKEY = Left(m.l_cLVLKEY, 7)
        l_nOccurs = Occurs('.', m.cLvlKey)-1
        For l_i = 1 To m.l_nOccurs
            Select VOCEMENU, Skip From (m.cCursor) Where Alltrim(LVLKEY) == m.l_cLVLKEY Into Cursor (m.l_cCursor)
            If &l_cCursor..Skip
                Select(m.l_oldArea)
                Return Left('@@@@@@@@@@'+Space(254), 254)
            Endif
            If m.l_i <= i_nMaxSearchMenu
                l_Ret = m.l_Ret + Cp_Translate(Alltrim(&l_cCursor..VOCEMENU))+'\'
            Endif
            If m.l_Ret=='\' Then
                l_Ret = ""
            Endif
            Use In Select(m.l_cCursor)
            m.l_cLVLKEY = Left(Alltrim(m.cLvlKey), 7+m.l_i*4)
        Next
        Select(m.l_oldArea)
        Return Left(m.l_Ret+Iif(i_nMaxSearchMenu<l_nOccurs, '..\','')+Cp_Translate(Alltrim(m.cVoceMenu))+Space(254), 254)
    Endfunc

    Procedure OnClick()
        Local bEmptySearch
        With This
            If .bClick Or .EmptySearch
                .oBtnSearch.BackColor = i_ThemesManager.GetProp(TOOLBAR_BTN_DOWN_BORDER)
                bEmptySearch = .EmptySearch And (Empty(.oTextToSearch.Value) Or .oTextToSearch.EmptySearch)
                If !Empty(.cCursorName) And Used(.cCursorName) And (bEmptySearch Or !Empty(.oTextToSearch.Value) And Len(Alltrim(.oTextToSearch.Value))>=3)
                    Local l_oldArea, l_Value, l_cCursor
                    l_oldArea = Select()
                    l_Value = Upper(Alltrim(.oTextToSearch.Value))
                    l_cCursor = .cCursorName
                    If Vartype(g_DebugSearchMenu) = 'L' And g_DebugSearchMenu
                        *--- Ricerco anche nel campo
                        #If Version(5)>=700
                            Select *, LVLKEY As LVLKEYORIG From (m.l_cCursor) Where Directory = 1 And (bEmptySearch Or Upper(NAMEPROC) Like '%'+l_Value+'%' Or Upper(Strtran(Cp_Translate(VOCEMENU),'&','')) Like '%'+l_Value+'%') ;
                                INTO Cursor __LISTMENUITEM__ Readwrite
                        #Else
                            Select *, LVLKEY As LVLKEYORIG From (m.l_cCursor) Where Directory = 1 And (bEmptySearch Or Upper(NAMEPROC) Like '%'+l_Value+'%' Or Upper(Strtran(Cp_Translate(VOCEMENU),'&','')) Like '%'+l_Value+'%') ;
                                INTO Cursor __LISTMENUITEM__
                            Cp_ReadWrite("__LISTMENUITEM__ ")
                        #Endif
                    Else
                        #If Version(5)>=700
                            Select *, LVLKEY As LVLKEYORIG From (m.l_cCursor) Where Directory = 1 And (bEmptySearch Or Upper(Strtran(Cp_Translate(VOCEMENU),'&','')) Like '%'+l_Value+'%') ;
                                INTO Cursor __LISTMENUITEM__ Readwrite
                        #Else
                            Select *, LVLKEY As LVLKEYORIG From (m.l_cCursor) Where Directory = 1 And (bEmptySearch Or Upper(Strtran(Cp_Translate(VOCEMENU),'&','')) Like '%'+l_Value+'%') ;
                                INTO Cursor  __LISTMENUITEM__
                            Cp_ReadWrite("__LISTMENUITEM__ ")
                        #Endif
                    Endif
                    If Used("__LISTMENUITEM__")
                        *--- Creo popupmenu
                        *--- Elemento radice
                        Local l_oPopupMenu, oMenuItem, l_bFindItem, l_MENUID
                        If Vartype(g_MenuPopupID)="U"
                            Public g_MenuPopupID
                            g_MenuPopupID = 5000
                        Endif
                        g_MenuPopupID = g_MenuPopupID + 5000
                        l_MENUID = g_MenuPopupID
                        *--- Inserisco voce menu principale
                        Insert Into __LISTMENUITEM__ (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED, LVLKEYORIG) Values ("",0,"",0,.T., m.l_MENUID,"","001",.F.,.F.,'')
                        l_bFindItem = .F.
                        m.l_oPopupMenu= Createobject("cbPopupMenu")
                        m.l_oPopupMenu.oPopupMenu.Init
                        *--- Cambio voce menu con il suo path
                        #If Version(5)>=700
                            Select This.RetPath(LVLKEYORIG, VOCEMENU, m.l_cCursor) As VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED From __LISTMENUITEM__;
                                INTO Cursor (m.l_oPopupMenu.oPopupMenu.cCursorName) Readwrite
                            Select * From (m.l_oPopupMenu.oPopupMenu.cCursorName) Where At('@@@@@@@@@@',VOCEMENU) = 0 ;
                                INTO Cursor (m.l_oPopupMenu.oPopupMenu.cCursorName) Readwrite
                        #Else
                            Select This.RetPath(LVLKEYORIG, VOCEMENU, m.l_cCursor) As VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED From __LISTMENUITEM__;
                                INTO Cursor (m.l_oPopupMenu.oPopupMenu.cCursorName)
                            * --- rendo scrivibile il cursore...
                            Cp_ReadWrite(m.l_oPopupMenu.oPopupMenu.cCursorName)
                            Select * From (m.l_oPopupMenu.oPopupMenu.cCursorName) Where At('@@@@@@@@@@',VOCEMENU) = 0 ;
                                INTO Cursor (m.l_oPopupMenu.oPopupMenu.cCursorName)
                            Cp_ReadWrite(m.l_oPopupMenu.oPopupMenu.cCursorName)
                        #Endif
                        *--- Aggiorno le chiavi
                        Update (m.l_oPopupMenu.oPopupMenu.cCursorName) Set LVLKEY = "001."+Right("000"+Transform(Recno()), 3), MENUID=m.l_MENUID+Recno() Where Directory<>0
                        *--- Ordino per la chiave
                        Select * From (m.l_oPopupMenu.oPopupMenu.cCursorName) Into Cursor (m.l_oPopupMenu.oPopupMenu.cCursorName) Order By LVLKEY
                        m.l_oPopupMenu.oPopupMenu.InitMenuFromCursor()
                        If Recno("__LISTMENUITEM__")>1
                            Local x, Y
                            Do Case
                                Case Isnull(.oToolbar)
                                    *controllo dentro il navbar
                                    m.x = -1
                                    m.y = -1
                                Case .oToolbar.DockPosition=-1
                                    m.x = cb_GetWindowLeft(.oToolbar.HWnd)+Sysmetric(3)+ 1
                                    m.y = cb_GetWindowTop(.oToolbar.HWnd)+Sysmetric(4)+Sysmetric(34)
                                    If .cTipo="RECENTI" Then
                                        m.x = m.x + .Parent.Left + .Parent.Width
                                        m.y = m.y + .Parent.Top + .Parent.Height
                                    Else
                                        m.x = m.x + .Left + .Width
                                        m.y = m.y + .Top + .Height
                                    Endif
                                Case .oToolbar.DockPosition=0 Or .oToolbar.DockPosition=3
                                    If .cTipo="RECENTI" Then
                                        m.x = cb_GetWindowLeft(.oToolbar.HWnd)+.Parent.Left+.Parent.Width + 1
                                        m.y = cb_GetWindowTop(.oToolbar.HWnd)+.Parent.Top+.Parent.Height
                                    Else
                                        m.x = cb_GetWindowLeft(.oToolbar.HWnd)+.Left+.Width + 1
                                        m.y = cb_GetWindowTop(.oToolbar.HWnd)+.Top+.Height
                                    Endif
                            Endcase
                            m.l_oPopupMenu.ShowMenu(m.x, m.y, TPM_RIGHTALIGN)
                        Endif
                        Use In Select("__LISTMENUITEM__")
                    Endif
                    Select(m.l_oldArea)
                Endif
            Else
                .oBtnSearch.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
            Endif
        Endwith
    Endproc
Enddefine

*--- Ricerca voci recenti, textbox che apre un menu con tutte le voci di menu che hanno nella descrizione la parola indicata
Define Class SearchRecent As Container
    BackStyle = 0
    BorderWidth = 0
    cTypeBar = ""

    Procedure Init(pCursRecen,pTypeBar,pTipo)
        Local offsetH
        With This
            .cTypeBar = pTypeBar
            .AddObject("oSearchMenu", "SearchMenu", pCursRecen , iif(pTipo,"CERCA","RECENTI"))
            .Height = .oSearchMenu.Height+1
            .Width = .oSearchMenu.Width+1
            .oSearchMenu.Top=1
            .oSearchMenu.Left=1
            Do Case
                Case pTypeBar="NAVBAR"
                    offsetH = .Height
                    .oSearchMenu.Anchor=10
                    .oSearchMenu.oBtnSearch.Anchor=8
                    .oSearchMenu.oTextToSearch.Anchor=10
                    .oSearchMenu.oToolbar = .Null.
                Case pTypeBar="TOOLBAR"
                    offsetH = 6
                    .oSearchMenu.Anchor=768
                    Do Case
                        Case i_nTBTNH>30
                            offsetH = offsetH + 32
                        Case i_nTBTNH>22
                            offsetH = offsetH + 24
                        Otherwise
                            offsetH = offsetH + 16
                    Endcase
            Endcase
            .Height = offsetH
        Endwith
    Endproc

    Procedure Visible_Assign
        Lparameters bnewval
        This.Visible=bnewval
        This.oSearchMenu.Visible=bnewval
    Endproc

Enddefine


Procedure TakeWindow
    * Author.....: Nicola Gorlandi
    * Date.......: Agosto 2008
    * Abstract...: Recupera l'handel di una fom all'interno di VFP...
    Lparameters pcWindowTitle
    Local lnWHND, lnVFP, lnRes, lnCNT, lnWinCnt

    Declare Integer FindWindow In win32api As apiFindWindow ;
        INTEGER nClass, String cName
    Declare Integer FindWindowEx In win32api As apiFindWindowEx ;
        INTEGER nParent, Integer nChildAfter, ;
        INTEGER nClass, String cName

    * First get the VFP window handle
    lnVFP = apiFindWindow( 0, _Screen.Caption )
    If lnVFP<=0
        Return -1
    Endif
    lnWinCnt = lnVFP
    lnWHND=apiFindWindowEx( lnVFP, 0, 0, pcWindowTitle ) && Look in VFP window
    If lnWHND=0 && Not in Main VFP window... This is the case if this window
        && was created by VFP
        && Container windows in VFP have no title
        lnCNT  = apiFindWindowEx( lnVFP, 0, 0, '' ) && Find in container window
        Do While lnWHND=0 And lnCNT<>0
            lnWHND = apiFindWindowEx( lnCNT, 0, 0, pcWindowTitle ) && Look in container
            lnCNT  = apiFindWindowEx( lnVFP, lnCNT, 0, '' ) && Find next container window
        Enddo
    Endif

    If lnWHND<0
        Return -1
    Endif

    Return lnWHND

Procedure Cp_ReadWrite
    Parameters cCursor
    w_NOME = Sys(2015)
    Select( m.cCursor )
    w_POSI = Select()
    Use Dbf() Again In 0 Alias ( m.w_NOME )
    Use
    Select( m.w_NOME )
    Use Dbf() Again In ( m.w_POSI ) Alias ( m.cCursor )
    Use
    Select( m.cCursor )
Endproc

*--- Ricerca ed esegue metodi delle Toolbar
Procedure cp_ToolBarCmd(cCBName, cCmd)
    If Vartype(i_ThemesManager)='O' And !Isnull(i_ThemesManager)
        Local l_oToolBar, l_Cmd
        l_oToolBar = i_ThemesManager.aToolbar.Item(m.cCBName)
        If Vartype(l_oToolBar)='O' And !Isnull(l_oToolBar)
            l_Cmd = "l_oToolBar."+Alltrim(cCmd)
            &l_Cmd
        Endif
        l_oToolBar = .Null.
    Endif
Endproc


*--- Classe timer per eseguire i comandi in modo differito
Define Class tExecCmdMenu As Timer
    nType = 0 &&0 Menu/Popup 1 Toolbar button
    Interval = 200
    cCmd = ""
    cMenuCursor = ""
    oParent = .Null.
    oThis=.Null.

    Procedure Init(nType, oParent, cCmd, cCursor)
        With This
            .oThis=This
            .nType = m.nType
            .cCmd = m.cCmd
            .cMenuCursor = m.cCursor
            .oParent = m.oParent
            .Interval = 50
            .Enabled=.T.
        Endwith

    Procedure Timer
        Local l_Cmd, l_oldArea
        This.Interval=0
        This.Enabled=.F.
        With This.oParent
            Do Case
                    *--- Menu
                Case This.nType = 0
                    l_Cmd = Strtran(This.cCmd, 'This.', 'This.oParent.',1,1,1)
                    l_oldArea = Select()
                    Select(This.cMenuCursor)
                    &l_Cmd
                    Select(l_oldArea)
                    *--- Btn
                Case This.nType = 1
                    l_Cmd = Strtran(This.cCmd, 'This.', 'This.oParent.',1,1,1)
                    &l_Cmd
            Endcase
        Endwith
        This.oParent = .Null.
        This.oThis=.Null.
        This.Destroy()
    Endproc
Enddefine
