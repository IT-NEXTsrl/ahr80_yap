* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_PPX
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Classi per la gestione della persistenza
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

#Define MAXCTEXT 10000
#Define MAXNLINE 9000

Define Class pxcg As Custom
    nLine=0
    cText=''
    bIsStoring=.F.
    nMajor=0
    nMinor=0
    *
    cText1=''
    Proc Init(mode,cText)
        This.bIsStoring=m.mode='save'
        This.nLine=0
        *
        If This.bIsStoring
            This.cText=''
        Else
            * --- ci sono i LF ma non i CR, la mline sbaglierebbe
            If At(Chr(10),m.cText)<>0 And At(Chr(13),m.cText)=0
                cText=Strtran(m.cText,Chr(10),Chr(13)+Chr(10))
            Endif
            * ---
            If Len(m.cText)>MAXCTEXT
                This.cText=Left(m.cText,MAXCTEXT)
                This.cText1=Substr(m.cText,MAXCTEXT+1)
            Else
                This.cText=m.cText
            Endif
        Endif
    Endproc
    Func Eoc(bIsEoc)
        Local oldmv
        If This.bIsStoring
            This.cText=This.cText+'*'+Chr(13)+Chr(10)
        Else
            oldmv=Set('MEMOWIDTH')
            Set Memowidth To 1024
            If Mline(This.cText,1,This.nLine)<>'*'
                If m.bIsEoc
                    *--- Se passo true nn restituisco l'errore e comunico che il file ha ulteriori proprietÓ
                    *--- restituendo false e riposizionandomi
						_Mline = This.nLine
                    Return .F.
                Else
                    * --- Zucchetti Aulla - Test automatico Visual Query (Aggiunto IF e ramo else)
                    If Vartype( g_testvar )<>'C'
                        Error cp_Translate(MSG_OBJECT_END_MISSING)
                    Else
                        g_testvar=cp_Translate(MSG_OBJECT_END_MISSING)
                    Endif
                Endif
            Endif
            Set Memowidth To (m.oldmv)
				This.nLine=_Mline
            *
        Endif
        Return .T.
    Endfunc
    Proc Save(cType,xValue)
        * --- salva/legge una variabile , assegna il default se non trovata
        * cType 'C'=char
        *       'N'=numeric
        *       'D'=date
        *       'L'=logic
        *       'X'=unk.
		This.cText=This.cText+SaveStorage(m.cType,m.xValue)
    Endproc
    Proc SaveHeader(cHeader,nMajor,nMinor)
        This.Save('H','* --- '+m.cHeader+' Vers. '+Alltrim(Str(m.nMajor))+'.'+Alltrim(Str(m.nMinor)))
        This.nMajor=m.nMajor
        This.nMinor=m.nMinor
    Endproc
    Func Load(cType,xDefault,bNoSetMemoWidth)
        * --- salva/legge una variabile , assegna il default se non trovata
        * cType 'C'=char
        *       'N'=numeric
        *       'D'=date
        *       'L'=logic
        *       'X'=unk.
        Local v,adv,oldmv
        adv=0
		if not bNoSetMemoWidth
			oldmv=Set('MEMOWIDTH')
			Set Memowidth To 1024
		endif
			v=ReadStorage(Mline(This.cText,1,This.nLine),@adv,m.cType,m.xDefault)
        Do While adv=2
            adv=0
				This.nLine=_Mline
				This.FillCText()
				v=v+' '+ReadStorage(Mline(This.cText,1,This.nLine),@adv,m.cType,m.xDefault)				
        Enddo
        If adv=0
				This.nLine=_Mline
				This.FillCText()
        Endif
		if not bNoSetMemoWidth		
			Set Memowidth To (oldmv)
		endif
        Return(v)
    Endfunc
    Proc LoadHeader(cHeader,nMajor,nMinor)
        Local v,adv,oldmv,p
        adv=0
        oldmv=Set('MEMOWIDTH')
        Set Memowidth To 1024
			v=ReadStorage(Mline(This.cText,1,This.nLine),@adv,'H','')
        * ha letto la riga, che pero' potrebbe essere nel vecchio formato, quindi senza header
        If Left(v,6)='* --- '
				This.nLine=_Mline
            p=At(' Vers. ',v)
            If p<>0
                If Left(v,p-1)<>'* --- '+m.cHeader
                    * --- Zucchetti Aulla - Test automatico Visual Query (if+ramo else)
                    If Vartype( g_testvar )<>'C'
                        Error cp_Translate(MSG_FILE_FORMAT_ERROR)
                    Else
                        g_testvar=cp_Translate(MSG_FILE_FORMAT_ERROR)
                    Endif
                Endif
                v=Substr(m.v,m.p+7)
                p=At('.',m.v)
                This.nMajor=Val(Left(m.v,m.p-1))
                This.nMinor=Val(Substr(m.v,m.p+1))
                If (This.nMajor>nMajor) Or (This.nMajor=m.nMajor And This.nMinor>m.nMinor)
                    Error cp_Translate(MSG_FILE_FORMAT_ERROR)
                Endif
                *wait window alltrim(str(this.nMajor))+'.'+alltrim(str(this.nMinor))
            Else
                * --- Zucchetti Aulla - Test automatico Visual Query (if+ramo else)
                If Vartype( g_testvar )<>'C'
                    Error cp_Translate(MSG_FILE_FORMAT_ERROR)
                Else
                    g_testvar=cp_Translate(MSG_FILE_FORMAT_ERROR)
                Endif
            Endif
        Endif
        Set Memowidth To (oldmv)
    Endproc
    Proc FillCText()
        If This.nLine>MAXNLINE And !Empty(This.cText1)
            This.nLine=This.nLine-MAXNLINE
            This.cText=Substr(This.cText,MAXNLINE+1)+Left(This.cText1,MAXNLINE)
            This.cText1=Substr(This.cText1,MAXNLINE+1)
        Endif
    Endproc
    Proc Container(oContainer)
        Local v,c,oldmv
        If This.bIsStoring
            This.cText=This.cText+'['+Chr(13)+Chr(10)
        Else
            oldmv=Set('MEMOWIDTH')
            Set Memowidth To 1024
				v=Mline(This.cText,1,This.nLine)
            If v='['
					*this.nLine=this.nLine+1
					This.nLine=_Mline
                *
                Do While This.Contained(m.oContainer)
                Enddo
					v=Mline(This.cText,1,This.nLine)
					This.nLine=_Mline
                *
                If v<>']'
                    * --- Zucchetti Aulla - Test automatico Visual Query (if+ramo else )
                    If Vartype( g_testvar )<>'C'
                        Error cp_Translate(MSG_END_CONTAINER_MISSING)
                    Else
                        g_testvar=cp_Translate(MSG_END_CONTAINER_MISSING)
                    Endif
                Endif
            Endif
            Set Memowidth To (oldmv)
        Endif
    Endproc
    Proc EndContainer()
        Local v
        If This.bIsStoring
            This.cText=This.cText+']'+Chr(13)+Chr(10)
        Endif
    Endproc
    Func Contained(oContainer,oObj)
        Local v,c,N,i,oldmv
        If This.bIsStoring
            This.cText=This.cText+m.oObj.Class+' '+m.oObj.Name+Chr(13)+Chr(10)
            oObj.Serialize(This)
            Return(.T.)
        Else
            oldmv=Set('MEMOWIDTH')
            Set Memowidth To 1024
				v=Mline(This.cText,1,This.nLine)
            If v<>']'
                i=At(' ',v)
                N=Substr(v,i+1)
                c=Left(v,i-1)
                *--- Interfaccia
                If Lower(m.oContainer.Class) == 'desktopbar' And Inlist(Lower(Alltrim(c)), 'cb_desktopbutton_dummy', 'desktopbutton')
                    If i_VisualTheme<>-1
                        c = 'cb_desktopbutton_dummy'
                    Else
                        c = 'desktopbutton'
                    Endif
                Endif
                oContainer.AddObject(m.n,m.c)
					This.nLine=_Mline
					This.FillCText()
                *
                oContainer.&N..Serialize(This)
                Return(.T.)
            Endif
            Set Memowidth To (oldmv)
            Return(.F.)
        Endif
    Endfunc
Enddefine

Func SaveStorage(cType,xVar)
    Local v
    If cType='X'
        cType=Type("xVar")
    Endif
    Do Case
        Case m.cType='C'
			xVar=iif(type("m.xVar")<>'C', '', m.xVar)
            v=xVar
            v=Strtran(m.v,"'",Chr(253))
            v=Strtran(m.v,'"',Chr(254))
            v=Strtran(m.v,Chr(13)+Chr(10),Chr(255))
            Return('"'+m.v+'"'+Chr(13)+Chr(10))
        Case m.cType='N' Or m.cType='Y'
			xVar=iif(type("m.xVar")<>'N' and type("m.xVar")<>'Y', 0, m.xVar)
            Return(' '+Alltrim(Str(m.xVar))+Chr(13)+Chr(10))
        Case m.cType='L'
            Return(Iif(xVar,'.t.','.f.')+Chr(13)+Chr(10))
        Case m.cType='D'
            Return('{'+Dtoc(m.xVar)+'}'+Chr(13)+Chr(10))
        Case m.cType='T'
            Return('#'+Ttoc(m.xVar)+'#'+Chr(13)+Chr(10))
        Case m.cType='H'
            Return(m.xVar+Chr(13)+Chr(10))
        Otherwise
            Error "???"
    Endcase
Endfunc

Func ReadStorage(v,l,cType,xDefault)
    Local T
    If m.v=='*'
        l=1
        Return(m.xDefault)
    Else
        If m.cType='X'
            T=Left(m.v,1)
            Do Case
                Case m.T='"'
                    cType='C'
                Case m.T=' '
                    cType='N'
                Case m.T='{'
                    cType='D'
                Case m.T='.'
                    cType='L'
                Case m.T='#'
                    cType='T'
            Endcase
        Endif
        Do Case
            Case m.cType='C'
                If Len(m.v)>=1 And Right(m.v,1)<>'"'
                    l=2
                Endif
                v=Strtran(m.v,'"','')
                v=Strtran(m.v,Chr(253),"'")
                v=Strtran(m.v,Chr(254),'"')
                v=Strtran(m.v,Chr(255),Chr(13)+Chr(10))
                Return(m.v)
            Case m.cType='N' Or m.cType='Y'
                Return(Val(m.v))
            Case (m.cType='D' Or m.cType='L') And Not Empty(m.v) && Test automatico VQR (Aggiunto Not Empty(v)
                Return(&v)
            Case cType='T'
                v=Substr(m.v,2,Len(m.v)-2)
                Return(Ctot(m.v))
            Case m.cType='H'
                Return(m.v)
            Otherwise
                * --- Zucchetti Aulla - Test automatico Visual Query (Aggiunto If e ramo else)
                If Vartype( g_testvar )<>'C'
                    Error "???"
                Else
                    g_testvar="???"
                Endif
        Endcase
    Endif
Endfunc
