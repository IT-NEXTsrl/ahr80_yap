* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_setfoxchartproperties                                        *
*              SetFoxChartProperties                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-07                                                      *
* Last revis.: 2010-05-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_setfoxchartproperties",oParentObject,m.pOPER)
return(i_retval)

define class tcp_setfoxchartproperties as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_oChart = .NULL.
  w_objProp = .NULL.
  w_oParentObject = .NULL.
  w_ParentClass = space(10)
  cp_foxcharteditor = .NULL.
  cp_foxchartproperties = .NULL.
  cp_foxchartfields = .NULL.
  oChart = .NULL.
  w_FIELDSCOUNT = 0
  w_i = 0
  w_Cursor = space(10)
  w_CLEGEND = space(25)
  w_CCOLOR = space(25)
  w_CCOLOR = space(25)
  w_ChartsCount = 0
  w_CURRENTROW = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definisco le variabili degli oggetti chiamanti
    this.w_ParentClass = Upper(Alltrim(this.oParentObject.Class))
    do case
      case this.w_ParentClass = "TCP_FOXCHARTEDITOR"
        this.w_oChart = this.oParentObject.w_oChart
      case this.w_ParentClass = "TCCP_FOXCHARTPROPERTIES"
        this.w_oChart = this.oParentObject.oParentObject.w_oChart
      case this.w_ParentClass = "TCCP_FOXCHARTFIELDS"
        this.w_oChart = this.oParentObject.oParentObject.oParentObject.w_oChart
    endcase
    this.oChart = this.w_oChart.oChart
    this.cp_foxcharteditor = this.w_oChart.oParentObject
    this.cp_foxchartproperties = this.cp_foxcharteditor.cp_foxchartproperties
    this.cp_foxchartfields = this.cp_foxchartproperties.cp_foxchartfields
    do case
      case this.pOper $ "SETLEGENDS-GETLEGENDS"
        this.w_oChart = this.oParentObject.w_oChart
        do case
          case this.oParentObject.w_LEGENDS=0
            this.w_objProp = this.w_oChart.oChart.Title
          case this.oParentObject.w_LEGENDS=1
            this.w_objProp = this.w_oChart.oChart.SubTitle
          case this.oParentObject.w_LEGENDS=2
            this.w_objProp = this.w_oChart.oChart.XAxis
          case this.oParentObject.w_LEGENDS=3
            this.w_objProp = this.w_oChart.oChart.AxisLegend2
          case this.oParentObject.w_LEGENDS=4
            this.w_objProp = this.w_oChart.oChart.YAxis
          case this.oParentObject.w_LEGENDS=5
            this.w_objProp = this.w_oChart.oChart.ShapeLegend
          case this.oParentObject.w_LEGENDS=6
            this.w_objProp = this.w_oChart.oChart.ScaleLegend
          case this.oParentObject.w_LEGENDS=7
            this.w_objProp = this.w_oChart.oChart.SideLegend
        endcase
        do case
          case this.pOper == "SETLEGENDS"
            this.w_objProp.Caption = Alltrim(this.oParentObject.w_Caption)
            this.w_objProp.Format = Alltrim(this.oParentObject.w_Format)
            this.w_objProp.FontName = this.oParentObject.w_FontName
            this.w_objProp.FontBold = this.oParentObject.w_FontBold
            this.w_objProp.FontItalic = this.oParentObject.w_FontItalic
            this.w_objProp.FontSize = this.oParentObject.w_FontSize
            this.w_objProp.FontUnderline = this.oParentObject.w_FontUnderline
            this.w_objProp.ForeColor = this.oParentObject.w_LForeColor
            this.w_objProp.BackColor = this.oParentObject.w_LBackColor
            this.w_objProp.ForeColorAlpha = this.oParentObject.w_LForeColorAlpha
            this.w_objProp.BackColorAlpha = this.oParentObject.w_LBackColorAlpha
            this.w_objProp.Alignment = this.oParentObject.w_Alignment
            this.w_objProp.Rotation = this.oParentObject.w_Rotation
            this.w_objProp.RotationCenter = this.oParentObject.w_RotationCenter
          case this.pOper == "GETLEGENDS"
            this.cp_foxchartproperties.SaveDependsOn()     
            this.oParentObject.w_Caption = this.w_objProp.Caption
            this.oParentObject.w_Format = this.w_objProp.Format
            this.oParentObject.w_FontName = this.w_objProp.FontName
            this.oParentObject.w_FontBold = this.w_objProp.FontBold
            this.oParentObject.w_FontItalic = this.w_objProp.FontItalic
            this.oParentObject.w_FontSize = this.w_objProp.FontSize
            this.oParentObject.w_FontUnderline = this.w_objProp.FontUnderline
            this.oParentObject.w_LForeColor = this.w_objProp.ForeColor
            this.cp_foxchartproperties.w_oForeColorChartL.Calculate("      1234567890     ",this.oParentObject.w_LFORECOLOR,this.oParentObject.w_LFORECOLOR)     
            this.oParentObject.w_LBackColor = this.w_objProp.BackColor
            this.cp_foxchartproperties.w_oBckColorChartL.Calculate("      1234567890     ",this.oParentObject.w_LBACKCOLOR,this.oParentObject.w_LBACKCOLOR)     
            this.oParentObject.w_LForeColorAlpha = this.w_objProp.ForeColorAlpha
            this.oParentObject.w_LBackColorAlpha = this.w_objProp.BackColorAlpha
            this.oParentObject.w_Alignment = this.w_objProp.Alignment
            this.oParentObject.w_Rotation = this.w_objProp.Rotation
            this.oParentObject.w_RotationCenter = this.w_objProp.RotationCenter
        endcase
      case this.pOper ="ADDFIELDSPROP"
        this.w_Cursor = SYS(2015)
        if !Empty(this.cp_foxchartproperties.w_QUERYMODEL)
          vq_exec( this.cp_foxchartproperties.w_QUERYMODEL ,this, this.w_Cursor )
          this.w_FIELDSCOUNT = AFIELDS(ArrayFields) && Create array.
          if this.w_FIELDSCOUNT > 0
            this.w_i = 1
            select( this.cp_foxchartfields.cTrsName) 
 ZAP
            do while this.w_i <= this.w_FIELDSCOUNT
              this.cp_foxchartfields.InitRow()     
              this.cp_foxchartfields.w_FD_VALUE = Alltrim(ArrayFields( this.w_i, 1))
              * --- Verifico se nel cursore esiste la colonna della leggenda relativa al fieldvalue
              this.w_CLEGEND = UPPER(Alltrim(ArrayFields( this.w_i, 1))+"_LEGEND")
              if ASCAN(ArrayFields , this.w_CLEGEND) >0
                this.cp_foxchartfields.w_FDLEGEND = this.w_CLEGEND
              else
                this.cp_foxchartfields.w_FDLEGEND = SPACE(25)
              endif
              this.cp_foxchartfields.w_FDCOLOR = RGB(INT(RAND() * 255), INT(RAND() * 255), INT(RAND() * 255))
              this.cp_foxchartfields.w_FD_SHAPE = 1
              this.cp_foxchartfields.w_FDVALONS = .T.
              this.cp_foxchartfields.SaveRow()     
              this.w_i = this.w_i + 1
            enddo
            if .f.
              this.w_oChart.SourceAlias = this.w_Cursor
              this.cp_foxchartfields.NotifyEvent("ADDFOXCHARTFIELDS")     
              this.w_oChart.DrawChart()     
            endif
          endif
        endif
      case this.pOper ="ADDFOXCHARTFIELDS"
        this.w_CHARTSCOUNT = this.cp_foxchartfields.NumRow()
        if this.w_CHARTSCOUNT > 0
          this.oChart.ChartsCount = this.w_ChartsCount
          this.cp_foxchartfields.MarkPos()     
          this.cp_foxchartfields.FirstRow()     
          do while Not this.cp_foxchartfields.Eof_Trs()
            this.cp_foxchartfields.SetRow()     
            if this.cp_foxchartfields.FullRow()
              this.w_CURRENTROW = this.w_CURRENTROW + 1
              this.oChart.Fields( this.w_CURRENTROW ).FieldValue = this.cp_foxchartfields.w_FD_VALUE
              this.oChart.Fields( this.w_CURRENTROW ).Legend = this.cp_foxchartfields.w_FDLEGEND
              this.oChart.Fields( this.w_CURRENTROW ).Color = this.cp_foxchartfields.w_FDCOLOR
              this.oChart.Fields( this.w_CURRENTROW ).Shape = this.cp_foxchartfields.w_FD_SHAPE
              this.oChart.Fields( this.w_CURRENTROW ).ShowValuesOnShape = this.cp_foxchartfields.w_FDVALONS
            endif
            this.cp_foxchartfields.NextRow()     
          enddo
          this.cp_foxchartfields.RePos(.F.)     
          this.cp_foxcharteditor.NotifyEvent("Redraw")     
        endif
        this.bUpdateParentObject = .f.
      case this.pOper ="GETTFIELDSFROMCHART"
        select (this.cp_foxchartfields.cTrsName)
        ZAP
        this.w_CHARTSCOUNT = this.oChart.ChartsCount
        if this.w_CHARTSCOUNT > 0
          this.w_CURRENTROW = 1
          do while this.w_CURRENTROW <= this.w_CHARTSCOUNT
            this.cp_foxchartfields.InitRow()     
            this.cp_foxchartfields.w_FD_VALUE = this.oChart.Fields( this.w_CURRENTROW ).FieldValue
            this.cp_foxchartfields.w_FDLEGEND = this.oChart.Fields( this.w_CURRENTROW ).Legend
            this.cp_foxchartfields.w_FDCOLOR = this.oChart.Fields( this.w_CURRENTROW ).Color
            this.cp_foxchartfields.w_FD_SHAPE = this.oChart.Fields( this.w_CURRENTROW ).Shape
            this.cp_foxchartfields.w_FDVALONS = this.oChart.Fields( this.w_CURRENTROW ).ShowValuesOnShape
            this.cp_foxchartfields.SaveDependsOn()     
            this.cp_foxchartfields.SetControlsValue()     
            this.cp_foxchartfields.SaveRow()     
            this.w_CURRENTROW = this.w_CURRENTROW + 1
          enddo
          this.cp_foxchartfields.WorkfromTrs()     
          this.cp_foxchartfields.Refresh()     
          this.cp_foxchartproperties.w_SHPMAINCOLOR = this.oChart.Fields(1).Color
          this.cp_foxchartproperties.w_REFSHPMAIN.Calculate("      1234567890     ", this.cp_foxchartproperties.w_SHPMAINCOLOR, this.cp_foxchartproperties.w_SHPMAINCOLOR)     
        endif
        this.bUpdateParentObject = .f.
      case this.pOper == "RESETCOMBOBOX"
        * --- Ho rieseguito anche la query, ora ho disponibile il cursore derivante dalla query
        *     posso ripopolare la combobox
        Local ObjCtrl
        ObjCtrl= This.cp_foxchartfields.GetbodyCtrl("w_FD_VALUE")
        ObjCtrl.Init()
        ObjCtrl=.NULL.
        if .F.
          ObjCtrl= This.cp_foxchartfields.GetbodyCtrl("w_FDLEGEND")
          ObjCtrl.Init()
          ObjCtrl=.NULL.
        endif
        select (this.cp_foxchartfields.cTrsName)
        ZAP
        this.cp_foxchartfields.InitRow()     
        * --- --
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDLEGEND")
        ObjCtrl.Init()
        ObjCtrl=.NULL.
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDDETACHSLICE")
        ObjCtrl.Init()
        ObjCtrl=.NULL.
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDHIDESLICE")
        ObjCtrl.Init()
        ObjCtrl=.NULL.
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDAXIS2")
        ObjCtrl.Init()
        ObjCtrl=.NULL.
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDCOLOR")
        ObjCtrl.Init()
        ObjCtrl=.NULL.
        this.cp_foxchartproperties.SetControlsValue()     
        this.bUpdateParentObject = .f.
      case this.pOper == "SETCURSORDATA"
        * --- Ho rieseguito anche la query, ora ho disponibile il cursore derivante dalla query
        *     posso ripopolare la combobox
        Local ObjCtrl
        ObjCtrl= This.cp_foxchartfields.GetbodyCtrl("w_FD_VALUE")
        ObjCtrl.Popola(this.oChart.SourceAlias)
        ObjCtrl=.NULL.
        * --- --
        if .F.
          ObjCtrl= This.cp_foxchartfields.GetbodyCtrl("w_FDLEGEND")
          ObjCtrl.Popola(this.oChart.SourceAlias)
          ObjCtrl=.NULL.
        endif
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDLEGEND")
        ObjCtrl.Popola(this.oChart.SourceAlias)
        ObjCtrl=.NULL.
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDDETACHSLICE")
        ObjCtrl.Popola(this.oChart.SourceAlias)
        ObjCtrl=.NULL.
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDHIDESLICE")
        ObjCtrl.Popola(this.oChart.SourceAlias)
        ObjCtrl=.NULL.
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDAXIS2")
        ObjCtrl.Popola(this.oChart.SourceAlias)
        ObjCtrl=.NULL.
        ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDCOLOR")
        ObjCtrl.Popola(this.oChart.SourceAlias)
        ObjCtrl=.NULL.
        this.cp_foxchartproperties.SetControlsValue()     
        this.bUpdateParentObject = .f.
      case this.pOper == "SETGRIDDATA"
        * --- Assegno alla griglia il cursore del grafico
        if Used(this.oChart.SourceAlias)
          this.cp_foxchartproperties.w_GraphData.cCursor = this.oChart.SourceAlias
          this.cp_foxchartproperties.w_GraphData.Browse()     
        endif
        this.bUpdateParentObject = .f.
      case this.pOper == "REDRAWFOXCHARTS"
        this.cp_foxcharteditor.NotifyEvent("Redraw")     
        this.bUpdateParentObject = .f.
      case this.pOper == "DRAWFOXCHARTS"
        this.w_oChart.UpdateFoxCharts()     
        this.bUpdateParentObject = .f.
      case this.pOper == "LOADMODELFROMFILE"
        if !Empty(this.cp_foxchartproperties.w_FILEMODEL)
          this.w_oChart.LoadModelFromFile(this.cp_foxchartproperties.w_FILEMODEL)     
          this.cp_foxchartproperties.NotifyEvent("GetValueFromChart")     
          this.bUpdateParentObject = .f.
        endif
      case this.pOper == "CONFIGLOADED"
        this.cp_foxcharteditor.w_CHANGEONFLY = .F.
        this.cp_foxchartproperties.NotifyEvent("GetValueFromChart")     
        this.cp_foxchartproperties.SaveDependsOn()     
        this.cp_foxchartproperties.SetControlsValue()     
        if Not Empty(this.cp_foxchartproperties.w_QUERYMODEL) And Empty(this.cp_foxchartproperties.w_cSource)
          this.w_oChart.cSource = this.cp_foxchartproperties.w_QUERYMODEL
        endif
        this.cp_foxchartproperties.mEnableControls()     
        this.cp_foxchartproperties.NotifyEvent("GETLEGENDS")     
        this.cp_foxchartproperties.NotifyEvent("REDRAWFOXCHARTS")     
        this.cp_foxchartproperties.NotifyEvent("SETCURSORDATA")     
        this.cp_foxchartproperties.NotifyEvent("GETTFIELDSFROMCHART")     
        this.bUpdateParentObject = .f.
      case this.pOper == "RUNBYPARAM"
        this.cp_foxcharteditor.w_CHANGEONFLY = .F.
        this.cp_foxchartproperties.NotifyEvent("GetValueFromChart")     
        this.cp_foxchartproperties.SaveDependsOn()     
        if Not Empty(this.cp_foxchartproperties.w_QUERYMODEL) And Empty(this.cp_foxchartproperties.w_cSource)
          this.w_oChart.cSource = this.cp_foxchartproperties.w_QUERYMODEL
        endif
        this.cp_foxchartproperties.SetControlsValue()     
        this.cp_foxchartproperties.mEnableControls()     
        this.cp_foxchartproperties.NotifyEvent("GETLEGENDS")     
        this.cp_foxchartproperties.NotifyEvent("REDRAWFOXCHARTS")     
        this.cp_foxchartproperties.NotifyEvent("SETCURSORDATA")     
        this.bUpdateParentObject = .f.
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
