* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VQ_EXEC
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Interprete di query
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam i_cQueryFile,i_oParentObject,i_cCursor,i_cReportFile,i_bPreview,i_bBatch,i_bAsci,i_cAsciFile,i_cTempTable,i_indexes,i_addexisting,pExcludeFromRemoveParam
Private i_paramqry
Local i_xcg,i_h,i_c,i_crs,__sqlx__,i_loader,i_qry,i_report
If Type("i_cQueryFile")<>'C' Or i_cQueryFile='?'
    i_cQueryFile=Getfile('vqr')
    i_cQueryFile=Left(i_cQueryFile,Len(i_cQueryFile)-4)
Else
    *--- Zucchetti Aulla Inizio - Gestione query InfoPublisher
    * Se lancio IRP esco senza eseguire nessuna query
    If Lower(Right(i_cQueryFile,4))='.irp'
        Return
    Endif
    *--- Zucchetti Aulla Fine
    * --- Verifica se il file inviato e' presente anche nella cartella \custom
    Private CustFile
    * --- Valuta il File di Query
    If Type("i_cQueryFile")='C'
        i_cQueryFile=Upper(Alltrim(i_cQueryFile))
		local bQueryFind
		*** Zuccehtti Aulla - gestione query gi� caricate
		If vartype(i_oCacheFile)='O'
			bQueryFind=i_oCacheFile.IsFile(forceext(i_cQueryFile, 'VQR'))
		Else
			bQueryFind=.f.
		Endif		
		If not bQueryFind
			If Not Right(i_cQueryFile, 4) = '.VQR'
				* --- se non presente, aggiunge l'estensione
				i_cQueryFile = i_cQueryFile + '.VQR'
			Endif
			CustFile = i_cQueryFile
			* --- Toglie il Path
			Do While At('\', CustFile)<>0
				CustFile=Substr(CustFile, At('\', CustFile)+1)
			Enddo
			CustFile = 'CUSTOM\' + CustFile
			* --- Se presente nella Custom riassegna i_cQueryFile
			If cp_fileexist(CustFile)
				i_cQueryFile = CustFile
			Endif
		Endif
    Endif
Endif
* --- Valuta il File di Report (Solo se specificato)
* --- non valuto pi� se report personalizzato perch� lo controllo nella ztam_class
If .F.
    If Type("i_cReportFile")='C' And Not(Empty(i_cReportFile))
        i_cReportFile=Upper(Alltrim(i_cReportFile))
        If Not Right(i_cReportFile, 4) = '.FRX'
            * --- se non presente, aggiunge l'estensione
            i_cReportFile = i_cReportFile + '.FRX'
        Endif
        CustFile = i_cReportFile
        * --- Toglie il Path
        Do While At('\', CustFile)<>0
            CustFile=Substr(CustFile, At('\', CustFile)+1)
        Enddo
        CustFile = 'CUSTOM\' + CustFile
        * --- Se presente nella Custom riassegna i_cReportFile
        If cp_fileexist(CustFile)
            i_cReportFile = CustFile
        Endif
    Endif
Endif
If Lower(Right(i_cQueryFile,4))='.vqr'
    i_cQueryFile=Left(i_cQueryFile,Len(i_cQueryFile)-4)
Endif
If Not(cp_IsPFile(i_cQueryFile+'.vqr'))
    cp_msg(cp_MsgFormat(MSG_CANNOT_FIND_QUERY__,i_cQueryFile),.F.)
    Return
Endif
If At('cp_query',Lower(Set('proc')))=0
    Set Proc To cp_query Additive
Endif
i_qry=Createobject('cpquery')
* --- Zucchetti Aulla Inizio - Gestione esclusione dalla remove filter di alcuni parametri
If Type('pExcludeFromRemoveParam')='C'
    i_qry.cExcludeFromRemoveParam=pExcludeFromRemoveParam
Endif
* --- Zucchetti Aulla Fine
* Traccia l'esecuzione di VQ_EXEC
*--- Activity log Traccia l'esecuzione di VQ_EXEC
If i_nACTIVATEPROFILER>0
    cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "VQR", i_cQueryFile, Iif(Type("i_curform")="C",i_curform,Space(10)), 0, , , "VQ_EXEC" )
Endif
*--- Activity log
If i_bBatch Or !Empty(i_cCursor) Or (Type('i_bDisableAsyncConn')='L' And i_bDisableAsyncConn)
    __sqlx__=.F.
Else
    __sqlx__=Createobject('sqlx')
Endif
* --- ora carica la query
i_loader=Createobject('cpqueryloader')
i_loader.LoadQuery(i_cQueryFile,i_qry,i_oParentObject)
* --- ora la esegue
i_crs=Iif(Type("i_cCursor")='C' And !Empty(i_cCursor),i_cCursor,"__tmp__")
If Lower(i_crs)='__tmp__' And Used('__tmp__')
    Select __tmp__
    Use
Endif
If i_qry.mDoQuery(i_crs,'Exec',__sqlx__,.F.,i_cTempTable,@i_indexes,i_addexisting)
    If Used(i_crs)
        Sele (i_crs)
        *--- Zucchetti Aulla Inizio - Multireport
        If Type('g_MultiReportParamqry')<>'U' And Isnull(g_MultiReportParamqry)
            g_MultiReportParamqry=i_qry
        Endif
        *--- Zucchetti Aulla Fine - Multireport
        i_paramqry=i_qry
        If Empty(i_cCursor)
            * --- usa il formato specificato o quello di default della query
            If Type("i_cReportFile")='C' And Not(Empty(i_cReportFile))
                i_report=i_cReportFile
            Else
                i_report=i_cQueryFile
            Endif
            * --- esegue
            If i_bPreview And cp_IsStdFile(i_report,'FRX')
                Push Key Clear
                i_curform=.Null.
                Push Menu _Msysmenu
                *set sysmenu to _mview
                Set Sysmenu To
                Repo Form (cp_GetStdFile(i_report,'FRX')) Preview
                Pop Menu _Msysmenu
                Pop Key
            Else
                If i_bAsci And cp_IsStdFile(i_report,'FRX')
                    Repo Form (cp_GetStdFile(i_report,'FRX')) To File (i_cAsciFile) Ascii
                Else
                    ***** Zucchetti Aulla
                    Local l_sPrg, l_stx
                    l_sPrg="cp_chprn"
                    IF JUSTEXT(i_report)='FXP'
                     l_stx='S'
                    else
                     l_stx='X'
                    ENDIF
                    Do (l_sPrg) With i_report,l_stx,i_oParentObject
                    Release l_sPrg
                    ***** Zucchetti Aulla
                Endif
            Endif
        Endif
    Endif
Else
    * --- Gestione errore in caso di DeadLock
    * --- Quando si verifica un deadlock su SQLSERVER, una delle query che generano lo stesso,
    * --- viene sacrificata ed interrotta. E' stato verificato che nonostante la query venga killata dal DBMS,
    * --- venga cmq inizializzato un cursore (i_cCursor) strutturalmente valido.
    * --- In questo caso, prima di presentare l'errore di impossibilit� di esecuzione query, distruggiamo l'eventuale
    * --- cursore creato.
    If Vartype(i_cCursor)='C' And Used(i_cCursor)
        Select(i_cCursor)
        Use
    Endif
    cp_msg(cp_Translate(MSG_ERROR_EXECUTING_QUERY),.F.)
Endif
Return
