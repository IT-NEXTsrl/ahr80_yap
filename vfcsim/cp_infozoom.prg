* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_infozoom                                                     *
*              ProprietÓ zoom                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-05                                                      *
* Last revis.: 2014-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_infozoom
cp_ReadXdc()
* --- Fine Area Manuale
return(createobject("tcp_infozoom",oParentObject))

* --- Class definition
define class tcp_infozoom as StdForm
  Top    = 144
  Left   = 219

  * --- Standard Properties
  Width  = 541
  Height = 146
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-19"
  HelpContextID=97872578
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_infozoom"
  cComment = "ProprietÓ zoom"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_cFisTab = space(30)
  w_cLogTab = space(30)
  w_cZoomName = space(254)
  w_cQueryName = space(254)
  w_cMenuFile = space(254)
  w_cZoomOnZoom = space(254)
  * --- Area Manuale = Declare Variables
  * --- cp_infozoom
  AutoCenter = .t.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_infozoomPag1","cp_infozoom",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.ocFisTab_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_infozoom
    * --- Translate interface...
     this.parent.cComment=cp_Translate(MSG_ZOOM_PROPERTY)
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_cFisTab=space(30)
      .w_cLogTab=space(30)
      .w_cZoomName=space(254)
      .w_cQueryName=space(254)
      .w_cMenuFile=space(254)
      .w_cZoomOnZoom=space(254)
        .w_cFisTab = .oParentObject.oContained.cSymFile
        .w_cLogTab = UPPER(ALLTRIM(i_dcx.GetTableDescr(i_dcx.GetTableIdx(upper(alltrim(.oParentObject.oContained.cSymFile))))))
        .w_cZoomName = .oParentObject.oContained.cZoomName
        .w_cQueryName = .oParentObject.oContained.ccpQueryName
        .w_cMenuFile = .oParentObject.cMenuFile
        .w_cZoomOnZoom = .oParentObject.oContained.cZoomOnZoom
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLES+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_SETUP+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_QUERY_FILE+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_CONTEXT_MENU+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ZOOM+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_SHOW_NAME_QUERY_FILE)
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_ENTITY_NAME)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLES+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_SETUP+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_QUERY_FILE+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_CONTEXT_MENU+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ZOOM+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_SHOW_NAME_QUERY_FILE)
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_ENTITY_NAME)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLES+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_SETUP+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_QUERY_FILE+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_CONTEXT_MENU+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ZOOM+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_SHOW_NAME_QUERY_FILE)
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_ENTITY_NAME)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.ocFisTab_1_1.value==this.w_cFisTab)
      this.oPgFrm.Page1.oPag.ocFisTab_1_1.value=this.w_cFisTab
    endif
    if not(this.oPgFrm.Page1.oPag.ocLogTab_1_2.value==this.w_cLogTab)
      this.oPgFrm.Page1.oPag.ocLogTab_1_2.value=this.w_cLogTab
    endif
    if not(this.oPgFrm.Page1.oPag.ocZoomName_1_3.value==this.w_cZoomName)
      this.oPgFrm.Page1.oPag.ocZoomName_1_3.value=this.w_cZoomName
    endif
    if not(this.oPgFrm.Page1.oPag.ocQueryName_1_5.value==this.w_cQueryName)
      this.oPgFrm.Page1.oPag.ocQueryName_1_5.value=this.w_cQueryName
    endif
    if not(this.oPgFrm.Page1.oPag.ocMenuFile_1_7.value==this.w_cMenuFile)
      this.oPgFrm.Page1.oPag.ocMenuFile_1_7.value=this.w_cMenuFile
    endif
    if not(this.oPgFrm.Page1.oPag.ocZoomOnZoom_1_8.value==this.w_cZoomOnZoom)
      this.oPgFrm.Page1.oPag.ocZoomOnZoom_1_8.value=this.w_cZoomOnZoom
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_infozoomPag1 as StdContainer
  Width  = 537
  height = 146
  stdWidth  = 537
  stdheight = 146
  resizeXpos=226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object ocFisTab_1_1 as StdField with uid="DJGFQBJCBL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_cFisTab", cQueryName = "cFisTab",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome fisico della tabella",;
    HelpContextID = 207699162,;
   bGlobalFont=.t.,;
    Height=21, Width=228, Left=116, Top=8, InputMask=replicate('X',30), readOnly=.T.

  add object ocLogTab_1_2 as StdField with uid="WHKUHZFYWQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_cLogTab", cQueryName = "cLogTab",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome logico della tabella",;
    HelpContextID = 100744410,;
   bGlobalFont=.t.,;
    Height=21, Width=178, Left=350, Top=8, InputMask=replicate('X',30), readOnly=.T.

  add object ocZoomName_1_3 as StdField with uid="JLOLDFOPRK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_cZoomName", cQueryName = "cZoomName",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome dello zoom",;
    HelpContextID = 189436175,;
   bGlobalFont=.t.,;
    Height=21, Width=228, Left=116, Top=35, InputMask=replicate('X',254), readOnly=.T.


  add object oBtn_1_4 as StdButton with uid="CQXXUHUYYM",left=354, top=35, width=76,height=22,;
    caption="OpenZoom", nPag=1;
    , ToolTipText = "Premere per aprire lo zoom";
    , HelpContextID = 36806924;
    , caption=MSG_OPEN_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        do OpenZoom with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object ocQueryName_1_5 as StdField with uid="ZGXJEPKUWX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_cQueryName", cQueryName = "cQueryName",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome della query",;
    HelpContextID = 149831484,;
   bGlobalFont=.t.,;
    Height=21, Width=228, Left=116, Top=62, InputMask=replicate('X',254), readOnly=.T.


  add object oBtn_1_6 as StdButton with uid="YQRXVYETMW",left=354, top=62, width=76,height=22,;
    caption="OpenQuery", nPag=1;
    , ToolTipText = "Premere per aprire la query";
    , HelpContextID = 44754787;
    , caption=MSG_OPEN_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        do OpenQuery with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object ocMenuFile_1_7 as StdField with uid="YLUBTYZYIL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_cMenuFile", cQueryName = "cMenuFile",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome del menu contestuale associato",;
    HelpContextID = 8030359,;
   bGlobalFont=.t.,;
    Height=21, Width=228, Left=116, Top=89, InputMask=replicate('X',254), readOnly=.T.

  add object ocZoomOnZoom_1_8 as StdField with uid="XBFEHCUWDM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_cZoomOnZoom", cQueryName = "cZoomOnZoom",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome dello gestione associata",;
    HelpContextID = 12358113,;
   bGlobalFont=.t.,;
    Height=21, Width=228, Left=116, Top=116, InputMask=replicate('X',254), readOnly=.T.


  add object oBtn_1_9 as StdButton with uid="YVVNOWRCVM",left=478, top=116, width=50,height=20,;
    caption="Ok", nPag=1;
    , HelpContextID = 215636674;
    , caption=MSG_OK_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_15 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=7, top=197, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tabella:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_16 as cp_setCtrlObjprop with uid="MXMKFOCINM",left=7, top=223, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Configurazione:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_17 as cp_setCtrlObjprop with uid="RUSBZEOSXF",left=7, top=249, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Query File:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_18 as cp_setCtrlObjprop with uid="IUHGJLEFDC",left=7, top=275, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Menu contestuale:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_19 as cp_setCtrlObjprop with uid="GKILVNDKTQ",left=7, top=301, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Ricerca:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_20 as cp_setobjprop with uid="SKPUZOAYUU",left=280, top=197, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_FisTab",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_21 as cp_setobjprop with uid="EHQUCINFUP",left=280, top=223, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cLogTab",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_22 as cp_setobjprop with uid="CRQJWNEWNL",left=280, top=249, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cZoomName",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_23 as cp_setobjprop with uid="RUDVSJVAGV",left=280, top=275, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cQueryName",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_24 as cp_setobjprop with uid="UCSUEVDYTN",left=280, top=301, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cMenufile",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307


  add object oObj_1_25 as cp_setobjprop with uid="BJZBTYIQBF",left=280, top=327, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cZoomOnZoom",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 162897307

  add object oStr_1_10 as StdString with uid="TQWDNGNLBG",Visible=.t., Left=13, Top=37,;
    Alignment=1, Width=98, Height=18,;
    Caption="Configurazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="FFBNAJVRKR",Visible=.t., Left=1, Top=91,;
    Alignment=1, Width=110, Height=18,;
    Caption="Menu contestuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="YMKICHSDRF",Visible=.t., Left=6, Top=64,;
    Alignment=1, Width=105, Height=18,;
    Caption="Query File:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="ZSGXKEPLFV",Visible=.t., Left=16, Top=10,;
    Alignment=1, Width=95, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KFQQAZYJHZ",Visible=.t., Left=19, Top=118,;
    Alignment=1, Width=92, Height=18,;
    Caption="Ricerca:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_infozoom','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_infozoom
proc OpenQuery(obj)
  public i_designfile
  i_designfile=FULLPATH(FORCEEXT(obj.w_cQueryName,'VQR'))
  vq_build()
  release i_designfile
endproc

proc OpenZoom(obj)
  vz_build()
  if Type('obj.opARENTOBJECT.oCONTAINED.Parent.cconfigfile')='C' and lower(right(alltrim(obj.opARENTOBJECT.oCONTAINED.Parent.cconfigfile),4))='.vzm'
    i_curform.cFilename=FULLPATH(FORCEEXT(obj.w_cZoomName,'.VZM'))
    i_curform.Loaddoc()
  else
    i_curform.oFORM.ZOOM.askfile(FULLPATH(FORCEEXT(obj.w_cZoomName,alltrim(obj.w_cFisTab)+'_VZM')))
  endif
endproc
* --- Fine Area Manuale
