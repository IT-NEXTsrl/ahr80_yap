* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VU_CONTEXT
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Giorgio Montali
* Data creazione: 15/02/2005
* Aggiornato il : 07/03/2005
* Translated    :
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Carica menu contestuale. Derivato da vu_exec.
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"


Lparam cMenuFile,oParentObject,contextmenu,j
If Vartype(cMenuFile)<>'C' Or cMenuFile='?'
    cMenuFile=Getfile('vmn')
Endif
If Lower(Right(cMenuFile,4))='.vmn'
    cMenuFile=Left(cMenuFile,Len(cMenuFile)-4)
Endif
Local xcg,h,c,l
If i_VisualTheme <> -1
    h = Sys(2015)
    * --- Zucchetti aulla inizio Framework monitor
    If i_MonitorFramework
        c = cp_getStdFile(Alltrim(cMenuFile),'vmn')
    Else
        * --- Zucchetti aulla fine Framework monitor
        c = cp_getStdFile('..\rightclick\'+cMenuFile,'vmn')
        * --- Zucchetti aulla inizio Framework monitor
    Endif
    * --- Zucchetti aulla fine Framework monitor
    Do Vmn_To_Cur In vu_exec With c+'.vmn', h
    Local loPopupMenu
    loPopupMenu = m.oParentObject
    *--- Inserisco nel popup il menu vmn
    loPopupMenu.oPopupMenu.cCursorName = Sys(2015)
    loPopupMenu.oPopupMenu.SetCursorVMN(h)
    Use In Select(h)
    Return
Else
    * --- Zucchetti aulla inizio Framework monitor
    If i_MonitorFramework
        c=cp_GetPFile(Alltrim(cMenuFile)+'.vmn')
    Else
        * --- Zucchetti aulla fine Framework monitor
        c=cp_GetPFile('..\rightclick\'+cMenuFile+'.vmn')
        * --- Zucchetti aulla inizio Framework monitor
    Endif
    * --- Zucchetti aulla fine Framework monitor
    xcg=Createobject('pxcg','load',c)
    LoadFirstLevelMenu(c, contextmenu,@j)
    LoadMenu(xcg, contextmenu)
Endif
Return

Proc LoadFirstLevelMenu(i_cText1, con_menu,i)
    Local i,k,p,c,i_nPos,i_nStart,i_nAction,i_cAction,i_cName,i_cMod,i_bEnabled
    Local i_cText,i_nPos1
    i=1
    i_nPos1=At('"'+Chr(13)+Chr(10)+' 2'+Chr(13)+Chr(10)+'"',i_cText1)
    Do While i_nPos1<>0
        i_nStart=i_nPos1-1
        Do While Substr(i_cText1,i_nStart,1)<>'"'
            i_nStart=i_nStart-1
        Enddo
        * --- separa la parte da analizzare in piccole stringhe che sono gestite piu' velocemente da VFP
        i_cText=Substr(i_cText1,i_nStart,1000)
        i_nPos=i_nPos1-i_nStart+1
        i_nStart=1
        *
        i_nAction=At('"',Substr(i_cText,i_nPos+8))
        i_cAction=Substr(i_cText,i_nPos+8,i_nAction-1)
        i_cAction=Strtran(i_cAction,Chr(253),"'")
        i_cAction=Strtran(i_cAction,Chr(254),'"')
        i_cName=Substr(i_cText,i_nStart+1,i_nPos-i_nStart-1)
        i_cName=Strtran(i_cName,Chr(253),"'")
        i_cName=Strtran(i_cName,Chr(254),'"')
        i_nAction=Val(Substr(i_cText,i_nAction+i_nPos+10,2))
        * --- controlla se l' opzione e' legata ad un modulo
        i_cMod=''
        i_bEnabled=.T.
        i_cPicture=''
        k=0
        p=0
        Do While k<=9
            * --- l'indicazione e' dopo 9 "
            p=p+1
            c=Substr(i_cText,i_nPos+p,1)
            Do Case
                Case c='*' And Substr(i_cText,i_nPos+p,3)='*'+Chr(13)+Chr(10)
                    k=10
                Case c='"'
                    k=k+1
                Case c='.' And Inlist(Substr(i_cText,i_nPos+p,3),'.t.','.f.')
                    i_bEnabled=Substr(i_cText,i_nPos+p,3)='.t.'
                    p=p+2
                Case k=5
                    i_cPicture=i_cPicture+c
                Case k=9
                    i_cMod=i_cMod+c
            Endcase
        Enddo
        *
        If i_nAction=2 && e' un submenu'
            Do LoadMenuItem In vu_exec With (con_menu), ('padF'+Alltrim(Str(i))), (i_cName), (i_nAction), ('subF'+Alltrim(Str(i))) , i, (i_cMod), (i_bEnabled), (i_cPicture) , (.T.)
            Define Popup ('subF'+Alltrim(Str(i))) Margin shortcut
        Else
            Do LoadMenuItem In vu_exec With (con_menu), ('padF'+Alltrim(Str(i))), (i_cName), (i_nAction), (i_cAction), i, (i_cMod), (i_bEnabled), (i_cPicture), (.T.)
        Endif
        i=i+1
        i_nPos1=At('"'+Chr(13)+Chr(10)+' 2'+Chr(13)+Chr(10)+'"',i_cText1,i)
    Enddo
    Return

Proc LoadMenu(xcg, con_menu)
    Local N,i,a,p,l,T,i_sp,i_stk,i_nCnt,i_cMod,i_bEnabled,i_cClose,i_cOpen
    Dimension i_stk[20,3]
    i_sp=1
    i_stk[1,1]=con_menu
    i_stk[1,2]=1
    i_stk[1,3]=0
    * --- skippa il menu principale
    xcg.LoadHeader('CodePainter Revolution Menu',4,3)
    If xcg.nMajor>=3
        N=xcg.Load('C','')
        N=xcg.Load('C','')
        N=xcg.Load('C','')
        N=xcg.Load('C','')
        N=xcg.Load('C','')
    Endif
    N=xcg.Load('N',0)
    If xcg.nMajor>=3
        xcg.Eoc()
    Endif
    T=xcg.Load('C','')
    l=xcg.Load('N',0)
    p=xcg.Load('C','')
    a=xcg.Load('N',0)
    If xcg.nMajor>=3
        xcg.Load('N',0)
        xcg.Load('N',0)
        xcg.Load('C','')
        xcg.Load('L',.T.)
        xcg.Load('N',0)
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('D',Ctod('  /  /  '))
        xcg.Load('D',Ctod('  /  /  '))
    Endif
    xcg.Eoc()
    * --- ora carica i sottolivelli del menu' principale
    i_nCnt=0
    For i=1 To N-1
        T=xcg.Load('C','')
        l=xcg.Load('N',0)
        p=xcg.Load('C','')
        a=xcg.Load('N',0)
        i_cMod=''
        i_bEnabled=.T.
        If xcg.nMajor>=3
            xcg.Load('N',0)
            xcg.Load('N',0)
            xcg.Load('C','')
            i_bEnabled=xcg.Load('L',.T.)
            xcg.Load('N',0)
            i_cClose=xcg.Load('C','')
            i_cOpen=xcg.Load('C','')
            i_cMod=xcg.Load('C','')
            xcg.Load('C','')
            xcg.Load('L',.T.)
            xcg.Load('C','')
            xcg.Load('C','')
        Endif
        xcg.Eoc()
        Do While l<i_stk[i_sp,2] && e' finito il menu, si scende
            i_sp=i_sp-1
        Enddo
        If a=2 && e' un submenu'
            If l<>2
                Do LoadMenuItem In vu_exec With (i_stk[i_sp,1]),('pad'+Alltrim(Str(i))),(T),(a),('sub'+Alltrim(Str(i))),(i_stk[i_sp,3]),(i_cMod),(i_bEnabled),(i_cClose),(.T.)
                Define Popup ('sub'+Alltrim(Str(i))) Margin shortcut
            Else
                i_nCnt=i_nCnt+1
            Endif
            i_stk[i_sp,3]=i_stk[i_sp,3]+1
            i_sp=i_sp+1
            If l<>2
                i_stk[i_sp,1]='sub'+Alltrim(Str(i))
            Else
                i_stk[i_sp,1]='subF'+Alltrim(Str(i_nCnt))
            Endif
            i_stk[i_sp,2]=l+1
            i_stk[i_sp,3]=1
        Else
            If l<>2
                Do LoadMenuItem In vu_exec With (i_stk[i_sp,1]),('pad'+Alltrim(Str(i))),(T),(a),(p),(i_stk[i_sp,3]),(i_cMod),(i_bEnabled),(i_cClose),(.T.)
            Else
                i_nCnt=i_nCnt+1
            Endif
            i_stk[i_sp,3]=i_stk[i_sp,3]+1
        Endif
    Next
    xcg.Eoc()
    Return

