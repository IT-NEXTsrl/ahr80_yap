* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: run2rep                                                         *
*              Stampa                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-10                                                      *
* Last revis.: 2013-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_nomerep,i_nrocopiepar,i_NomeStampante,i_TipoRep,i_ModSst,oPrint
* --- Area Manuale = Header
* --- run2rep
LOCAL  cFileStampa , cFileStampa_Totale
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("trun2rep",oParentObject,m.i_nomerep,m.i_nrocopiepar,m.i_NomeStampante,m.i_TipoRep,m.i_ModSst,m.oPrint)
return(i_retval)

define class trun2rep as StdBatch
  * --- Local variables
  i_nomerep = space(0)
  i_nrocopiepar = space(0)
  i_NomeStampante = space(0)
  i_TipoRep = space(0)
  i_ModSst = space(0)
  oPrint = .NULL.
  w_cFileStampa = space(10)
  w_cNomeStampante = space(10)
  w_cCmd = space(10)
  w_uRet = 0
  w_cModSst = space(10)
  w_cOk = .f.
  w_numCopie = 0
  w_i = 0
  w_VarEnv = space(10)
  w_x = 0
  w_VarValue = 0
  w_bVarEnv = 0
  w_first = .f.
  w_VarValue = 0
  w_ts_Enabled = .f.
  w_ts_10Cpi = space(10)
  w_ts_12Cpi = space(10)
  w_ts_15Cpi = space(10)
  w_ts_Comp = space(10)
  w_ts_RtComp = space(10)
  w_ts_Reset = space(10)
  w_ts_Inizia = space(10)
  w_ts_ForPag = space(10)
  w_ts_RowPag = 0
  w_ts_RowOk = 0
  w_ts_StBold = space(10)
  w_ts_StDoub = space(10)
  w_ts_StItal = space(10)
  w_ts_StUnde = space(10)
  w_ts_FiBold = space(10)
  w_ts_FiDoub = space(10)
  w_ts_FiItal = space(10)
  w_ts_FiUnde = space(10)
  w_cOk = .f.
  w_RepFxp = space(10)
  w_CurName = space(10)
  w_nNumCopy = 0
  w_cPrnText = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- * --- Parametri
    *       * i_NomeRep = nome del Report da Stampare (FRX)
    *       * i_nrocopiepar = numero di copie
    *       * i_NomeStampante = nome stampante
    *       * Zucchetti Aulla Inizio
    *       * i_TipoRep -> nuovo parametro per gestione Anteprima di stampa
    *       * i_ModSst -> nuovo parametro per modello Stampa Solo testo letto in Associazione Report/Device 
    *       * Zucchetti Aulla Fine
    *     
    this.w_numCopie = this.i_nrocopiepar
    LOCAL oReport_Text, RepOrd , oReport_Graphic , cCmd
    oReport_Text = CREATEOBJECT("MultiReport")
    oReport_Graphic = CREATEOBJECT("MultiReport")
    RepOrd=this.i_nomerep.SplitTextGraphic(oReport_Text, oReport_Graphic)
    if RepOrd = 1 AND !oReport_Graphic.IsEmpty()
      * --- Stampo i report Grafici se non vuoto
      do RunRep with this,oReport_Graphic, this.i_TipoRep, this.oPrint.prFile, this.w_numCopie
    endif
    LOCAL l_VarName
    * --- File prn globale
    local i_cpercorso, i_cpercorsodos 
 i_cperrcorso = tempadhoc() 
 i_cpercorsodos=space (300) 
 DECLARE INTEGER GetShortPathName IN Kernel32 STRING @lpszLongPath, STRING @lpszShortPath, INTEGER cchBuffer 
 GetShortPathName(@i_cperrcorso , @i_cpercorsodos, 300)
    cFileStampa_Totale= ADDBS(i_cpercorsodos)+sys(2015)
    this.w_first = .T.
    do while this.i_nrocopiepar>0
      this.w_i = 0
      i_NroCopie= this.i_nrocopiepar
      do while this.w_i<>-1
        this.w_i = oReport_Text.NextReport(this.w_i)
        if this.w_i<>-1
          * --- Setto i_paramqry per permettere l'uso di cp_queryparam()  
          i_paramqry=oReport_Text.GetParamqry(this.w_i)
          * --- Creo l'environment con le variabili l_
          this.w_VarEnv = oReport_Text.GetVarEnv(this.w_i)
          this.w_bVarEnv = .F.
          if NOT ISNULL(this.w_VarEnv) AND NOT this.w_VarEnv.IsEmpty()
            l_NumVar=this.w_VarEnv.GetNumVar()
            * --- Creo le variabili
            this.w_x = 1
            do while this.w_x <= l_NumVar
              l_VarName=this.w_VarEnv.GetVarName(this.w_x) 
 &l_VarName=this.w_VarEnv.GetVarValue(this.w_x)
              this.w_x = this.w_x + 1
            enddo
            this.w_bVarEnv = .T.
          endif
          * --- Init
          *     Nome file di stampa senza estensione
          cFileStampa = ADDBS(i_cpercorsodos)+sys(2015)
          * --- Utilizzo sempre la prima stampante trovata, non effettua cambio stampante
          this.i_NomeStampante = IIF(EMPTY(this.i_NomeStampante), oReport_Text.GetRepPrinter(this.w_i), this.i_NomeStampante)
          if not(empty(this.i_NomeStampante))
            this.w_cNomeStampante = upper(alltrim(this.i_NomeStampante))
          else
            this.w_cNomeStampante = upper(alltrim(set("PRINTER",2)))
          endif
          this.w_cModSst = Alltrim( this.i_ModSst)
          * --- Lettura impostazioni per stampa solo testo
           ts_Enabled = .t. 
 ts_10Cpi = "" 
 ts_12Cpi = "" 
 ts_15Cpi = "" 
 ts_Comp = "" 
 ts_RtComp = "" 
 ts_Reset = "" 
 ts_Inizia = "" 
 ts_ForPag = "" 
 ts_RowPag = 66 
 ts_RowOk = 64 && Numero righe utili 
 ts_StBold = "" 
 ts_StDoub = "" 
 ts_StItal = "" 
 ts_StUnde = "" 
 ts_FiBold = "" 
 ts_FiDoub = "" 
 ts_FiItal = "" 
 ts_FiUnde = "" 
 cOk = .t. && Solo testo a video non carico i set di caratteri. In questo modo non si vedono nella maschera di anteprima
          * --- Se stampa su carta leggo i set di caratteri
          if this.i_TipoRep = "S" 
            cOk = ReadChrCtrl( this.w_cNomeStampante, NVL(this.w_cModSst,""))
          else
             ts_10Cpi = "CHR(32)" 
 ts_12Cpi = "CHR(32)" 
 ts_15Cpi = "CHR(32)" 
 ts_Comp = "CHR(32)" 
 ts_RtComp = "CHR(32)" 
 ts_StBold = "CHR(32)" 
 ts_StDoub = "CHR(32)" 
 ts_StItal = "CHR(32)" 
 ts_StUnde = "CHR(32)" 
 ts_FiBold = "CHR(32)" 
 ts_FiDoub = "CHR(32)" 
 ts_FiItal = "CHR(32)" 
 ts_FiUnde = "CHR(32)" 
          endif
          * --- Quando concateno non inserisco l'inizializzazione della stampante e il formato pagina
          if !this.w_first
             ts_Inizia = "" 
 ts_ForPag = ""
          endif
          if cOk
            * --- Prepara file di stampa
            _ASCIIROWS = this.w_ts_RowOk 
 _ASCIICOLS = 162 
            this.w_RepFxp = oReport_Text.GetReport(this.w_i)
            this.w_CurName = oReport_Text.GetCursorName(this.w_i)
            if UPPER(this.w_CurName)<>"__TMP__"
               USE IN SELECT("__TMP__") 
 SELECT * FROM (this.w_CurName) INTO CURSOR __TMP__
            endif
             SELECT __TMP__ 
 PUBLIC g_CurCursor 
 g_CurCursor="__TMP__"
            * --- Lancia la gestione delle stampe Painter report
            do (this.w_RepFxp) 
            this.w_nNumCopy = IIF(oReport_Text.bNumCopy OR this.i_TipoRep="V", 1, oReport_Text.GetNumCopy(this.w_i))
            this.w_x = 1
            do while this.w_x<= this.w_nNumCopy
              * --- Concateno i file prn
              this.w_cPrnText = FILETOSTR(cFileStampa+".prn")
              STRTOFILE(this.w_cPrnText, cFileStampa_Totale+".prn",1)
              this.w_x = this.w_x + 1
            enddo
            if (this.i_nrocopiepar=1 AND oReport_Text.NextReport(this.w_i)=-1) OR (oReport_Text.NextReport(this.w_i)=-1 AND this.i_TipoRep = "V")
              * --- Stampo
              *     i_TipoRep = 'S' -> Invio A stampante; 'V' -> a Video
              if this.i_TipoRep = "S" 
                * --- Imposta la stampante selezionata  
                set printer to name (this.w_cNomeStampante)
                if cp_fileexist(cFileStampa_Totale+".prn",.T.)
                  * --- Invia il file alla stampante tramite utility Spooling       
                   cCmd = fullpath("SPOOLING.EXE") + " " + cFileStampa_Totale+ ".prn "+ this.w_cNomeStampante + " /c /j AHR_Stampa_SoloTesto /n " + alltrim(str(this.i_nrocopiepar)) 
 uRet=WinExec(cCmd,1)
                  if uRet<=31
                    cp_ErrorMsg(CP_MSGFORMAT(MSG_ERROR_EXECUTING_STATEMENT_CL__,this.w_cCmd),"stop","",.F.)
                  endif
                else
                  cp_ErrorMsg(MSG_NO_DATA_TO_PRINT,"stop")
                endif
                * --- Ripristina stampante predefinita 
                set printer to default
              else
                * --- * Zucchetti Aulla Inizio
                *     * i_PrnFile contiene il Path del File Prn creato con Stampa Solo testo nella Temp di Win
                *     * GSUT_KAS maschera di antemprima: passo come parametro il nome file Prn e nome File Report 
                *     * divisi da @ per ricostruzione variabili sulla maschera
                if cp_fileexist(cFileStampa_Totale+".prn",.T.)
                  i_PrnFile = (cFileStampa_Totale+".prn")
                  do GSUT_KAS with i_PrnFile+"@" + this.w_RepFxp
                endif
                * --- Zucchetti Aulla Fine
              endif
            endif
          endif
          this.w_ts_Enabled = .F.
          * --- Rivalorizzo le variabili dell' environment in modo da poterle poi interrogare alla fine della stampa
          if this.w_bVarEnv &&Se � true ho creato le variabili quindi le risalvo all"interno dell"oggetto
             this.w_x=1
            this.w_x = 1
            do while this.w_x <= l_NumVar
              l_VarName=this.w_VarEnv.GetVarName(this.w_x)
              this.w_VarEnv.SetVarValue(&l_VarName,this.w_x)     
              this.w_x = this.w_x + 1
            enddo
          endif
        endif
      enddo
      this.i_nrocopiepar = IIF(this.i_TipoRep="V", 0, this.i_nrocopiepar - 1)
    enddo
    * --- Stampo i report Grafici se non vuoto
    if RepOrd = 0 AND !oReport_Graphic.IsEmpty()
      * --- Stampo i report Grafici se non vuoto
      do RunRep with this,oReport_Graphic, this.i_TipoRep, this.oPrint.prFile, this.w_numCopie
    endif
    * --- Zucchetti Aulla Fine
  endproc


  proc Init(oParentObject,i_nomerep,i_nrocopiepar,i_NomeStampante,i_TipoRep,i_ModSst,oPrint)
    this.i_nomerep=i_nomerep
    this.i_nrocopiepar=i_nrocopiepar
    this.i_NomeStampante=i_NomeStampante
    this.i_TipoRep=i_TipoRep
    this.i_ModSst=i_ModSst
    this.oPrint=oPrint
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="i_nomerep,i_nrocopiepar,i_NomeStampante,i_TipoRep,i_ModSst,oPrint"
endproc
