* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_ZOOM
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Zoom "visuale"
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam i_cFilename,i_cFields,i_cKeyfld,i_cTarget,i_cInitsel,i_cZoomOnZoom,i_cZoomTitle,i_cZoomFile,i_fromobj
* --- crea un oggetto di tipo zoom
Return(Createobject('stdzoom',i_cFilename,i_cFields,i_cKeyfld,i_cTarget,i_cInitsel,i_cZoomOnZoom,i_cZoomTitle,i_cZoomFile,Iif(Type('i_fromobj')<>'O',i_curform,i_fromobj), Vartype(i_cZoomMode)<>'U' And i_cZoomMode='C'))

#Define zb_shift 0 &&30

Define Class stdzoom As CPSysForm 
	ShowTips = .T.
	Width=500
	oThisForm=.Null.
	cTarget=''
	MinHeight=150
	Icon = "zoom.ico"
	bMobilityMode=.F.
	Add Object __sqlx__ As sqlx
	** ZUCCHETTI AULLA gestione singolo click Inizio
	oCurrentCellHandle = .Null.
	Add Object oTimerSingleClick As Timer With Interval = 1, Enabled = .F.
	*--- Zucchetti Aulla Inizio - Form decorator
	bApplyFormDecorator=.t.	
	*-- Zucchetti Aulla Fine - Form decorator
	** ZUCCHETTI AULLA gestione singolo click Fine
	Proc Init(i_cFile,i_cFields,i_cKeyFields,i_cTarget,i_cInitsel,i_cZoomOnZoom,i_cZoomTitle,i_cZoomFile,i_oParentObject, bMobilityMode)
		This.oThisForm=This
		This.bMobilityMode = bMobilityMode
    	*--- Form Decorator
    	LOCAL TestMacro
    	IF This.bApplyFormDecorator And i_ThemesManager.GetProp(123)=0
    		This.AddObject("oDec", "cp_FormDecorator")	
        ELSE
            This.bApplyFormDecorator = .F.    		
    	ENDIF	
		If This.bMobilityMode
			With Thisform
				.MinWidth=150
				.BorderStyle=0
				.MaxButton=.F.
				.MinButton=.F.
				.ControlBox=.F.
				.TitleBar=0
			Endwith
		Endif
		** ZUCCHETTI AULLA GESTIONE BCON INIZIO
		If Upper(i_cFile) = 'GRUPPCON' And Type('i_oParentObject.octrlgruppo') = 'O'
			If Type('i_oParentObject.octrlgruppo.cNoStdZoom') <> 'L' Or (Type('i_oParentObject.octrlgruppo.cNoStdZoom') = 'L' And Not(i_oParentObject.octrlgruppo.cNoStdZoom))
				If Type('g_ZOOMGROUPNAME') = 'C'
					i_cZoomFile = g_ZOOMGROUPNAME
				Endif
			Endif
		Endif
		** ZUCCHETTI AULLA GESTIONE BCON FINE
		If !This.bMobilityMode
			This.Caption=Iif(Empty(i_cZoomTitle),cp_MsgFormat(Upper(Substr(i_cFile,1,1))+ Lower(Substr(i_cFile,2)) ),cp_Translate(i_cZoomTitle))
		Else
			This.AddObject('shpBorder','shape')
			With This.shpBorder
				.Top=0
				.Left=0
				.BorderWidth=2
				.BorderColor=0
				.BackStyle=0
				.Visible=.T.
			Endwith
		Endif
		This.AddObject('z1','zoombox')
		This.z1.oParentObject=i_oParentObject
		If This.bMobilityMode
			*Disabilito sempre il pulsante delle opzioni per problemi di resize e perch� la sua
			*altezza � troppo piccola per la modalit� "mobility" (touchscreen)
			This.z1.bOptions=.F.
			This.z1.Height = This.z1.Height - This.z1.oButtonOption.Height - 1
			This.z1.bMobilityMode = .T.
		Endif
		This.z1.cZoomKeyFields=Iif(Type("i_cKeyFields")='C',i_cKeyFields,'')
		If Type("this.z1.cKeyFields")='C'
			This.z1.cKeyFields=This.z1.cZoomKeyFields
		Endif
		* --- gestione dell' eventuale filtro legato alla gestione associata
		If !Empty(i_cZoomOnZoom) And Empty(i_cZoomFile)
			* --- se e' specificato un programma di zoomonzoom va a cercare il filtro associato
			Local i_cFlt
			i_cFlt=cp_QueryEntityFilter(i_cZoomOnZoom)
			If !Empty(i_cFlt)
				If Empty(i_cInitsel)
					i_cInitsel=i_cFlt
				Else
					i_cInitsel=i_cInitsel+' and (' +i_cFlt+')'
				Endif
			Endif
		Endif

		*---
		This.z1.SetFileAndFields(i_cFile,i_cFields,i_cInitsel,i_cZoomFile)
		This.z1.SetZoomOnZoom(i_cZoomOnZoom)
		This.z1.TabIndex=1
		This.cTarget=Iif(Type("i_cTarget")='C',i_cTarget,'')
		i_curform=Thisform
		Local cObjManyHeaderName
		cObjManyHeaderName="oManyHeader_"+This.z1.Name
		If (This.z1.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And Type("this."+cObjManyHeaderName)<>"O"
			This.AddObject(cObjManyHeaderName,"ManyHeader")
			This.z1.oRefHeaderObject=This.&cObjManyHeaderName
			This.&cObjManyHeaderName .InitHeader(This.z1.grd,.T.)
		Endif
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
			If This.bMobilityMode
				Thisform.shpBorder.BorderColor = i_ThemesManager.GetProp(2)
			Endif
		Endif
		If This.bMobilityMode
			This.z1.Top=-8
			This.z1.Left=-8
			This.SizeWindow()

			This.shpBorder.Width = This.Width
			This.shpBorder.Height = This.z1.Height - 6
			This.shpBorder.Anchor = 15
			If TYPE(i_cTarget)=='O'
				This.Top =  &i_cTarget..Height + Objtoclient( &i_cTarget ,1)  + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,  Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top
				This.Left = Objtoclient( &i_cTarget ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left -1
			else
				This.Top =  i_oParentObject.Parent.Height + Sysmetric(4) * 2 + Sysmetric(9) + i_oParentObject.Parent.Top + 1
				This.Left = i_oParentObject.Parent.Left
			EndIf
			If This.bApplyFormDecorator And Type("This.oDec")<>'U'
				This.Move(This.Left - Sysmetric(3), This.Top - This.oDec.Height + Sysmetric(4))
			EndIf
			*Verifiche per visualizzazione (verticali)
			*Se visualizzando lo zoom sotto il controllo non sta nello schermo provo a visualizzarlo sopra al controllo
			*Se visualizzandolo sopra al controllo non ci sta comunque verifico se lo spazio maggiore � sopra o sotto il
			*controllo e visualizzo li lo zoom ridimensionandolo
			Local BottomSpace
			BottomSpace = _Screen.Height - This.Top
			If This.Top+This.Height>_Screen.Height
				*Visualizzo lo zoom sopra al controllo
				If TYPE(i_cTarget)=='O'
					This.Top =  (Objtoclient( &i_cTarget ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top) - This.Height - 1
				else
					This.Top =  i_oParentObject.Parent.Top - This.Height
				endif
				If This.Top<0
					*Poco spazio sopra al controllo. Verifico se lo spazio � maggiore sopra o sotto
					*visualizzo nell'area maggiore lo zoom ridimensionandolo
					If TYPE(i_cTarget)=='O'
					TestMacro=(Objtoclient( &i_cTarget ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top) > BottomSpace
						If  TestMacro
							*Ridimensiono lo zoom e lo visualizzo comunque sopra al controllo
							This.Height = Max(Objtoclient( &i_cTarget ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top + 4, Thisform.MinHeight)
							This.Top = (Objtoclient( &i_cTarget ,1)+ Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) + Sysmetric(9) ) + i_oParentObject.Top) - This.Height - 1
						Else
							*Ridimensiono lo zoom e lo visualizzo sotto al controllo
							This.Height = Max(_Screen.Height - (Objtoclient( &i_cTarget ,1)  + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) ) + Sysmetric(9) + i_oParentObject.Top + 1), Thisform.MinHeight)
							This.Top =  &i_cTarget..Height + Objtoclient( &i_cTarget ,1)  + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(4) ) + Sysmetric(9) + i_oParentObject.Top + 1
						Endif
					Else
						If i_oParentObject.Parent.Top - 1 > BottomSpace
							*Ridimensiono lo zoom e lo visualizzo comunque sopra al controllo
							This.Height = Max( i_oParentObject.Parent.Top , Thisform.MinHeight)
							This.Top =  i_oParentObject.Parent.Top - This.Height + Sysmetric(4) - 1
						else
							*Ridimensiono lo zoom e lo visualizzo sotto al controllo
							This.Height = BottomSpace
							This.Top =  i_oParentObject.Parent.Height + Sysmetric(4) * 2 + Sysmetric(9) + i_oParentObject.Parent.Top + 1
						endif
					endif
					This.z1.Resize()
					This.SizeWindow()
				Endif
			Endif
			*Verifiche per visualizzazione (orizzontali)
			*Se visualizzando lo zoom allineato a sinistra del controllo non sta nello schermo provo ad allinearlo a destra
			*Se allineato a destra non ci sta comunque verifico se lo spazio maggiore � a destra o a sinistra del
			*controllo e visualizzo li lo zoom ridimensionandolo
			Local RightSpace
			RightSpace = _Screen.Width - This.Left
			Local CondBtnZoom
			If TYPE(i_cTarget)=='O'
				CondBtnZoom=((i_cZBtnShw='S' And (Vartype(&i_cTarget..bNoContxtBtn)='U' Or (Vartype(&i_cTarget..bNoContxtBtn)='N' And &i_cTarget..bNoContxtBtn=2 ))) Or;
						(i_cZBtnShw='N' And Vartype(&i_cTarget..bNoContxtBtn)='N' And &i_cTarget..bNoContxtBtn=2 ))
			else
				CondBtnZoom=((i_cZBtnShw='S' And (Vartype(i_oParentObject.Parent.bNoContxtBtn)='U' Or (Vartype(i_oParentObject.Parent.bNoContxtBtn)='N' And i_oParentObject.Parent.bNoContxtBtn=2 ))) Or;
						(i_cZBtnShw='N' And Vartype(i_oParentObject.Parent.bNoContxtBtn)='N' And i_oParentObject.Parent.bNoContxtBtn=2 ))
			endif
			If This.Left+This.Width>_Screen.Width
				*Visualizzo lo zoom allineato a destra
				If TYPE(i_cTarget)=='O'
					This.Left = Objtoclient( &i_cTarget ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left - This.Width + Objtoclient( &i_cTarget ,3) + Iif(CondBtnZoom, i_nZBtnWidth, 0) - 1
				Else
					This.Left = i_oParentObject.Parent.Left + i_oParentObject.Parent.Width + Sysmetric(3) * 2 + 1  - This.Width
				EndIf
				If This.Left<0
					*poco spazio a destra del controllo. verifico se lo spazio � maggiore a destra o a sinistra
					*visualizzo nell'area maggiore lo zoom ridimensionandolo
					If TYPE(i_cTarget)=='O'
					TestMacro=&i_cTarget
						If Objtoclient( TestMacro ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left + Iif(CondBtnZoom, i_nZBtnWidth, 0) > RightSpace
							*Ridimensiono lo zoom e lo visualizzo comunque allineato a destra
							This.Width = Max( Objtoclient( &i_cTarget ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) )+ i_oParentObject.Left + Objtoclient( &i_cTarget ,3) + Iif(CondBtnZoom, i_nZBtnWidth, 0) ,Thisform.MinWidth)
							This.Left = Objtoclient( &i_cTarget ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left - This.Width + Objtoclient( &i_cTarget ,3) + Iif(CondBtnZoom, i_nZBtnWidth * 2, 0) - 1
						Else
							*Ridimensiono lo zoom e lo visualizzo allineato a sinistra
							This.Width = Max( RightSpace  ,Thisform.MinWidth)
							This.Left = Objtoclient( &i_cTarget ,2) + Iif( Vartype(_Screen.TBDI)='O' And _Screen.TBDI.Visible, 0,Sysmetric(3) ) + i_oParentObject.Left -1
						Endif
					else
						If Objtoclient( i_oParentObject.Parent ,2) - this.Width - Sysmetric(3) * 2 + 1 +  i_oParentObject.Parent.Width > RightSpace
							*Ridimensiono lo zoom e lo visualizzo comunque allineato a destra
							This.Width = Max( RightSpace + i_oParentObject.Parent.Width  ,Thisform.MinWidth)
							This.Left =  i_oParentObject.Parent.Left
						Else
							*Ridimensiono lo zoom e lo visualizzo allineato a sinistra
							This.Width = Max( i_oParentObject.Parent.Left + i_oParentObject.Parent.Width + Sysmetric(9) + Sysmetric(3) + 1, Thisform.MinWidth)
							This.Left = i_oParentObject.Parent.Left + i_oParentObject.Parent.Width + Sysmetric(9) + Sysmetric(3) + 1 - This.Width
						EndIf
					endif
					This.z1.Resize()
					This.SizeWindow()
				Endif
			Endif
		Endif
		This.Show()
		Return

		** ZUCCHETTI AULLA gestione singolo click Inizio
	Proc oTimerSingleClick.Timer
		This.Parent.oCurrentCellHandle.ReturnSelected()
		This.Parent.oCurrentCellHandle = .Null.
	Endproc
	** ZUCCHETTI AULLA gestione singolo click fine

	Proc SizeWindow
		Local OldAnchor
		OldAnchor=This.z1.Anchor
		This.z1.Anchor=0
		This.Width=This.z1.Width -16
		This.Height=This.z1.Height-6
		This.z1.Anchor=OldAnchor
	Endproc

	Proc Destroy()
		This.oThisForm=.Null.
		If Vartype(i_curform)='O' And !Isnull(i_curform) And i_curform.Name=Thisform.Name
			i_curform=.Null.
		Endif
		** ZUCCHETTI AULLA gestione singolo click Inizio
		If Vartype(This.oCurrentCellHandle) = 'O'
			This.oCurrentCellHandle = .Null.
		Endif
		** ZUCCHETTI AULLA gestione singolo click fine
		Return
	Proc Resize()
		This.LockScreen=.T.
		This.z1.Resize()
		This.LockScreen=.F.
		Return
		
	Proc Activate()
		i_curform=Thisform
		Return
		
	Proc Deactivate()
		This.__sqlx__.CloseConn()
		If This.bMobilityMode
			Thisform.Release()
		Endif
		Return
		
	Func HasCPEvents(i_cOp)
		Return(.T.)
		
	Proc ecpDelete()
		Return
		
	Proc ecpEdit()
		Return
		
	Proc ecpF6()
		Return
		
	Proc ecpFilter()
		If (This.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And i_ServerConn[1,2]<>0
			This.z1.AskAdvancedParameters()
		Else
			This.z1.AskParameters()
		Endif
		Return
		
	Proc ecpLoad()
		Return
		
	Proc ecpNext()
		Return
		
	Proc ecpPgUp()
		Return
		
	Proc ecpPgDn()
		Return
		
	Proc ecpPrint()
		Return
		
	Proc ecpPrior()
		Return
		
        *--- Modifica per assegnare il focus al campo di partenza dello zoom anche in caso di Abort
	Proc ecpQuit()
        If Type("thisform.cTarget")<>'U' And Not(Empty(Thisform.cTarget))
            Local ot
            LOCAL TestMacro
            ot = Thisform.cTarget
	    * --- Zucchetti Aulla inizio - Esc in zoom da variabile in visual mask
	    TestMacro= Type(ot)='O' and TYPE('&ot..Parent.oContained.bDontReportError')<>'U' 
            If TestMacro
	    * --- Zucchetti Aulla fine - Esc in zoom da variabile in visual mask
                &ot..Parent.oContained.bDontReportError=.T.
                &ot..SetFocus()
            Endif
        Endif
		Thisform.Release()
		Return
        *Proc ecpQuit()
        *   Thisform.Release()
        *   Return
	Proc ecpSave()
		Return
		
	Proc ecpZoom()
		This.z1.zz.Click()
		Return
		
	Proc ecpRefresh()
		This.z1.ref.Click()
		Return
		
	Proc ecpZoomOnZoom()
		Return
	Proc ecpSecurity()
		Return
		
	Proc ecpInfo()
		Return
		
	Func GetHelpFile()
		Return(i_CpDic)
		
	Func GetHelpTopic()
		Return(This.z1.cZoomName)
Enddefine

Define Class ScrollingButton As Container
	Top = 0
	Left = 0
	Width = 40
	Height = 40
	Visible = .F.
	nDirection = 1
	nDefaultDelay=500
	BorderWidth=0
	BackStyle = 0

	Add Object oImg As Image With Top=4, Left=4, Height=32,Width=32,Visible=.F.
	Add Object oTimerScroll As Timer With Enabled=.F., Interval=1000

	Procedure Init
		*Inizializzo la bitmap del tasto
		This.SetBtnProperties()
	Endproc

	Proc SetBtnProperties
		Do Case
			Case This.nDirection=0	&&Up
				This.oImg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('.\bmp\up.bmp', 32),'bmp')
				This.oImg.ToolTipText = MGS_PREVIOUS_RECORD
			Case This.nDirection=1	&&Down
				This.oImg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('.\bmp\down.bmp', 32),'bmp')
				This.oImg.ToolTipText = MSG_NEXT_RECORD
			Case This.nDirection=4	&&Left
				This.oImg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('.\bmp\left.bmp', 32),'bmp')
				This.oImg.ToolTipText = MSG_LEFT
			Case This.nDirection=5	&&Right
				This.oImg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('.\bmp\right.bmp', 32),'bmp')
				This.oImg.ToolTipText = MSG_RIGHT
		Endcase
	Endproc

	Proc Direction_Assign
		Lparameters nNewVal
		This.nDirection = m.nNewVal
		This.SetBtnProperties()
	Endproc

	Proc oImg.MouseDown
		Lparameters nButton, nShift, nXCoord, nYCoord
		If nButton=1 And nShift=0
			This.Parent.oTimerScroll.Timer()
			This.Parent.oTimerScroll.Interval = This.Parent.nDefaultDelay
			This.Parent.oTimerScroll.Enabled=.T.
		Endif
	Endproc

	Proc oImg.MouseUp
		Lparameters nButton, nShift, nXCoord, nYCoord
		This.Parent.oTimerScroll.Enabled=.F.
	Endproc

	Proc oTimerScroll.Timer
		This.Parent.Parent.Parent.z1.grd.DoScroll(This.Parent.nDirection)
		This.Interval = Max( This.Interval/2 , 10 )
	Endproc

	Proc Visible_Assign
		Lparameters bNewVal
		This.Visible = m.bNewVal
		This.oImg.Visible = m.bNewVal
	Endproc

Enddefine

Define Class zoombox As cpzoomcontainer
	Top=0
	Left=0
	Width=500
	Height=300
	* ---
	cCursor=''
	cFile=''
	lFile=''
	cZoomName=''
	nConn=0
	cSymFile=''
	bFileError=.F.
	cZoomOnZoom=''
	cDatabaseType=''
	cStartFields='*'
	* --- campi
	nFields=0
	Dimension cFields(1)
	cFields(1)=''
	bChangedFld=.F.
	* --- condizioni di where
	nWhere=0
	Dimension cWhere(1)
	cWhere(1)=''
	cWhereCol=''
	cWhereAdvanced=''
	nWhereAtLoad=0
	Dimension  cWhereAtLoad(1)
	o_initsel=''
	* --- campi di order by
	nOrderBy=0
	Dimension cOrderBy[1]
	cOrderBy[1]=''
	Dimension bOrderDesc[1]
	bOrderDesc[1]=.F.
	nOrderByDefault=0
	Dimension cOrderByDefault[1]
	cOrderByDefault[1]=''
	Dimension bOrderDescDefault[1]
	bOrderDescDefault[1]=.F.
	bOrderQueryTransfer=.F.   && propriet� che indica se � gi� stata trasferito l'ordinamento dalla query allo zoom
	* --- i campi della chiave primaria per Drag&Drop verso altri form
	cKeyFields=''
	cZoomKeyFields='' && i campi richisti dall' oggetto di partenza per il drag & drop
	* --- lo stato delle colonne
	nColPositions=0
	Dimension nColWidth[1]
	nColWidth[1]=0
	Dimension nColOrd[1]
	nColOrd[1]=0
	Dimension cColTitle[1]
	cColTitle[1]=''
	Dimension cColFormat[1]
	cColFormat[1]=''
	Dimension nColRW[1]
	nColRW[1]=0
	Dimension nColX[1]
	nColX[1]=0
	Dimension nColY[1]
	nColY[1]=0
	Dimension nColH[1]
	nColH[1]=0
	Dimension cForeColor[1]
	cForeColor[1]=''
	Dimension cBackColor[1]
	cBackColor[1]=''
	Dimension cHyperLink[1]
	cHyperLink[1]=''
	Dimension cHyperLinkTarget[1]
	cHyperLinkTarget[1]=''
	Dimension bIsImage[1]
	bIsImage[1]=.F.
	Dimension cLayerContent[1]
	cLayerContent[1]=''
	Dimension nColumnType[1]
	nColumnType[1]=0
	Dimension cTooltip[1]
	cTooltip[1]=''
	* --- gestione carattere testata
	Dimension cLabelFont[1]
	cLabelFont[1]=''
	Dimension nDimFont[1]
	nDimFont[1]=0
	Dimension bItalicFont[1]
	bItalicFont[1]=.F.
	Dimension bBoldFont[1]
	bBoldFont[1]=.F.
	* --- gestione carattere colonne
	Dimension cLabelFontC[1]
	cLabelFontC[1]=''
	Dimension nDimFontC[1]
	nDimFontC[1]=0
	Dimension bItalicFontC[1]
	bItalicFontC[1]=.F.
	Dimension bBoldFontC[1]
	bBoldFontC[1]=.F.	
	* ---
	bSrcUpdated=.F.   && segnala se la variabile di ricerca e' stata variata, serve a per filtrare per valori nulli
	cOldIcon=''       && icona drag/drop
	bAdvanced=.F.     && segnala se e' in modo advanced
	bAskParam=.F.     && segnala se bisogna attivare la maschera di selezione al caricamento del file
	bFillFields=.F.   && segnala se bisogna riempire l' array con il nome dei campi
	bKeepSize=.F.     && segnala se deve essere mantenuta la dimensione della finestra
	bModSQL=.F.       && segnala se la frase SQL e' stata modificata
	cSQL=''           && la frase SQL modidicata
	bOptions=.T.      && segnala se la parte del form relativa alle opzioni deve essere visualizzata
	bReadOnly=.T.     && indica se il cursore e' scrivibile o meno
	bQueryOnDblClick=.T. && indica se rieseguire la query quando l' utente fa un doppio click sulla testata di colonna
	bNoMenuFunction=.F.  && Disabilita nel tasto destro sottomenu Funzionalit�
	bNoMenuAction=.F.    && Disabilita nel tasto destro sottomenu Azioni
	bNoMenuProperty=.F.  && Disabilita nel tasto destro sottomenu Propiet�
	bNoMenuHeaderProperty=.F.  && Disabilita nel tasto destro su Header il sottomenu Propiet�
	bNoDefault=.F.    && Segnala se non deve essere eseguita la ricerca dello zoom di default
	bNoZoomGridShape=.F. &&Disabilita il resize dello zoom tramite Drag&Drop (AdvOpt)
	* --- Problema colonne zoom )Se zoom con spunta segnala se la spunta � presente o meno
	bCheckBox = .F.
	bAddedCheckBox=.F.
	bMobilityMode = .F.
	bDefaultHeightGrid=.T.
	* --- gestione della query esterna
	cOldSelect=''
	oCpQuery=.Null.   && oggetto query complessa
	cCpQueryName=''
	oParentObject=.Null.
	**** ZUCCHETTI AULLA INIZIO - GESTIONE QUERY SINCRONA
	bSinc = .F.
	**** ZUCCHETTI AULLA FINE - GESTIONE QUERY SINCRONA
	* --- gestione del resize della griglia
	Resizable=.T.
	nDY=25
	nGrdW=10
	nGrdH=35
	* --- lock colonna da passare alla griglia
	nLockColumns=0
	* --- riferimento all'oggetto header
	oRefHeaderObject=Null
	* --- riferimento all'oggetto search/filter
	bSearchFilterOnClick=.T. && indica se apre maschera searchfilter al click
	oRefSearchFilter=Null
	bSaveWhere=.F.
	* --- ZUCCHETTI AULLA GESTIONE IMMAGINI ZOOM INIZIO
	nFieldsImage=0					&& Numero dei campi che contengono le immagini
	Dimension cFieldsImage(1,1)		&& Array contenente i nomi del campi costruiti aggiungendo _ZIMG
	Dimension cCursorImage(1)		&& Contiene il cursore legato alla colonna per la gestione delle immgaini
	* --- ZUCCHETTI AULLA GESTIONE IMMAGINI ZOOM FINE
	bAdvancedHeaderZoom = .F.
	* ---
	Add Object grd As stdzGrid Noinit With Top=10,Left=10,Width=This.Width-20,Height=This.Height-45,ColumnCount=0
	bVisibleOption=.F. 		&& propriet� per espandere le aprire lo zoom con option espansa
	Add Object oButtonOption As stdzButtonOption With Top=This.Height-37,Left=10,Width=This.Width-20,Height=4,FontBold=.F.,FontName='MS Sans Serif',ToolTipText=MSG_CONFIGURATION_OPTIONS,Visible=.T.
	Add Object lbl As Label   With Top=This.Height-26,Left=10,Width=95,BackStyle=0,Caption='',Alignment=1
	Add Object src As stdzSrc With Top=This.Height-30,Left=110,Width=130,Height=25,FontBold=.F.
	Add Object sel As stdzSel With Top=This.Height-30,Left=241,Width=28,Height=25,Caption='',FontBold=.F.,Picture='',ToolTipText=MSG_ASK_FOR_PARAMETERS
	Add Object Dir As stdzDir With Top=This.Height-30,Left=270,Width=28,Height=25,Caption='',FontBold=.F.,Picture='',ToolTipText=MSG_SETTINGS
	Add Object rep As stdzRep With Top=This.Height-30,Left=299,Width=28,Height=25,Caption='',FontBold=.F.,Picture='',ToolTipText=MSG_EXECUTE_REPORT
	Add Object ref As stdzRef With Top=This.Height-30,Left=328,Width=28,Height=25,Caption='',FontBold=.F.,Picture='',ToolTipText=MSG_RECALCULATE_QUERY
	Add Object adv As stdzAdv With Top=This.Height-30,Left=357,Width=60,Height=25,FontBold=.F.,FontName='MS Sans Serif',ToolTipText=MSG_CONFIGURATION_OPTIONS
	Add Object qry As stdzQry With Top=This.Height-30,Left=435,Width=60,Height=25,Caption=MSG_QUERY_BUTTON,FontBold=.F.,Visible=.F.,FontName='MS Sans Serif'
	Add Object zz As CPToolBtn With Top=This.Height-30,Left=435,Width=60,Height=25,Caption='',Picture='',ToolTipText=MSG_OPEN_FORM,Visible=.F.
	Add Object morerows As CommandButton With Top=0,Left=0,Width=16,Height=16,Caption='',FontBold=.F.,Visible=.F.,Picture='',ToolTipText=MSG_MORE_ROWS
	Add Object btnLeft As ScrollingButton With Top=0, Left=0, Width=40, Height=40, Caption='',FontBold=.F.,Visible=.F., nDirection=4
	Add Object btnRight As ScrollingButton With Top=0, Left=0, Width=40, Height=40, Caption='',FontBold=.F.,Visible=.F., nDirection=5
	Add Object btnUp As ScrollingButton With Top=0, Left=0, Width=40, Height=40, Caption='',FontBold=.F.,Visible=.F., nDirection=0
	Add Object btnDown As ScrollingButton With Top=0, Left=0, Width=40, Height=40, Caption='',FontBold=.F.,Visible=.F., nDirection=1
	nDesFlags=0  &&Se 1 0 3 non utilizza le descrizioni delle colonne salvate nello zoom, se 2 o 3 non utilizza le larghezze delle colonne salvate nello zoom
	Proc zz.Click()
		* --- Zoom On Zoom
		Local z
		z=This.Parent.cZoomOnZoom
		Do Case
			Case z='_ok_'
				* --- chiude lo zoom
				This.Parent.Parent.bOk=.T.
				This.Parent.Parent.Release()
			Case !Empty(z)
				* --- esegue lo zoom on zoom
				Do &z
		Endcase
		* --- procedure del form principale
	*--- Zucchetti Aulla Inizio - Abilito bottoni stdzrep
	PROCEDURE cZoomName_assign(xValue)
		this.cZoomName = m.xValue
		This.rep.Enabled= !EMPTY(this.cZoomName)
	ENDPROC
	*--- Zucchetti Aulla Fine - Abilito bottoni stdzrep
	Proc Init(pGlobalFont)
		With This
			.sel.ToolTipText=cp_Translate(.sel.ToolTipText)
			.Dir.ToolTipText=cp_Translate(.Dir.ToolTipText)
			.rep.ToolTipText=cp_Translate(.rep.ToolTipText)
			.ref.ToolTipText=cp_Translate(.ref.ToolTipText)
			.adv.ToolTipText=cp_Translate(.adv.ToolTipText)
			.qry.ToolTipText=cp_Translate(.qry.ToolTipText)
			.oButtonOption.ToolTipText=cp_Translate(.oButtonOption.ToolTipText)
			.qry.Caption=cp_Translate(This.qry.Caption)
			.adv.Caption=cp_Translate( MSG_OPTIONS_FR )
			.grd.GridLineColor = i_nGridLineColor
			If (This.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And i_bExpandZoomParameter Or Not (This.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And Not i_bExpandZoomParameter
				.oButtonOption.Click()
			Endif
			If Vartype(i_ThemesManager)=='O'
				i_ThemesManager.PutBmp(.sel, 'filter.bmp', 16)
				i_ThemesManager.PutBmp(.Dir, 'folder.bmp', 16)
				i_ThemesManager.PutBmp(.rep, 'print.bmp', 16)
				i_ThemesManager.PutBmp(.ref, 'new_interrog.bmp', 16)
				i_ThemesManager.PutBmp(.zz, 'zoom.bmp', 16)
				i_ThemesManager.PutBmp(.morerows, 'morerows.bmp', 16)
			Else
				.sel.Picture='filtra.bmp'
				.Dir.Picture='folder.bmp'
				.rep.Picture='print.bmp'
				.ref.Picture='new_interrog.bmp'
				.zz.Picture='tzoom.bmp'
				.morerows.Picture='morerows.bmp'			
			Endif
			#If Version(5)>=900
				.grd.HighlightStyle = i_nEviRigaZoom
				.grd.HighlightBackColor = i_nZoomColor
				.grd.HighlightForeColor=Rgb(0,0,0)
				.grd.SelectedItemBackColor = i_nZoomColor
				.grd.SelectedItemForeColor=Rgb(0,0,0)
			#Endif

			.Visible=.T.
			Local l_delta
			l_delta = 0
			If .Resizable
				If .Parent.BaseClass='Page'
					.Width=This.Parent.Parent.Width
					If Pemstatus(Thisform, "oTabMenu",5)
						l_delta = 28
					Else
						l_delta = 25
					Endif
					If Pemstatus(Thisform, "oTitleBar",5)
						l_delta = l_delta + 18
					Endif
					.Height=.Parent.Parent.Height - l_delta
				Else
					.Width=.Parent.Width
					If Pemstatus(Thisform, "oTitleBar",5)
						l_delta = 18
					Endif
					.Height=.Parent.Height - l_delta
					.nDY=0
				Endif
			Endif
			.cCursor=Sys(2015) &&thisform.name+'_cur'
			If At('cp_query',Lower(Set('proc')))=0
				Set Proc To cp_query Additive
			Endif
			.grd.RecordMark = i_bRecordMark
		Endwith
	Func OpenFile(i_cFile)
		Local N
		N=cp_OpenTable(i_cFile,.T.)
		If N=0 And Not(Used(i_cFile))
			This.RemoveObject('grd')
			This.SetAll('enabled',.F.)
			This.AddObject('errlbl','label')
			This.errlbl.Caption=cp_MsgFormat(MSG_FILE__NOT_FOUND_QM,i_cFile)
			This.errlbl.Top=10
			This.errlbl.Left=10
			This.errlbl.Width=This.Width
			This.errlbl.BackStyle=0
			This.errlbl.Visible=.T.
			This.bFileError=.T.
			Return(.F.)
		Endif
		If N=0
			This.cSymFile=i_cFile
			This.cFile=i_cFile
			*--- Se la tabella non � sul dizionario devo valorizzare l'alias della tabella con se stessa altrimenti ho un errore nella creazione della frase sql
			This.lFile=i_cFile
			This.nConn=0
			This.cDatabaseType='VFP'
		Else
			This.cSymFile=i_cFile
			*** verifico dove deve essere applicata la sicurezza sul dato
			If Empty(This.cZoomOnZoom)
				This.cFile=cp_SetAzi(i_TableProp[n,2],.T.,N)
			Else
				This.cFile=cp_SetAzi(i_TableProp[n,2])
			Endif
			*** se non ho definito lo zoom on zoom la sicurezza deve essere definita sulla tabella
			*** altrimenti � calcolata nella cp_queryenityfilter
			This.lFile=i_TableProp[n,1]
			This.nConn=i_TableProp[n,3]
			This.cDatabaseType=i_ServerConn[i_TableProp[n,5],6]
		Endif
		Return(.T.)
	Proc SetFileAndFields(i_cFile,i_cFields,i_cInitsel,i_cZoomFile)
		Local xcg,p,l,N,h,i_bFile,i_cExt
		* ---
		i_bFile=.F.
		If Vartype(i_cZoomFile)='C'
		    	i_cExt=JUSTEXT(i_cZoomFile)
			if (!Empty(i_cExt))
				i_cZoomFile=JUSTSTEM(i_cZoomFile)
			Else
				i_cExt=i_cFile+'_vzm'
			Endif
			i_cZoomFile=cp_FindNamedDefaultFile(i_cZoomFile,i_cExt)
			If !Empty(i_cZoomFile)
				* --- carica lo zoom specificato
				i_bFile=.T.
				p=cp_GetPFile(Forceext(m.i_cZoomFile,m.i_cFile+'_vzm'))
				If Not(Empty(m.p))
					This.cZoomName=m.i_cZoomFile
					xcg=Createobject('pxcg','load',m.p)
					This.Serialize(m.xcg)
				Endif
				i_cFile=This.cSymFile
			Endif
		Endif
		If !m.i_bFile
			* ---
			If !This.OpenFile(m.i_cFile)
				Return
			Endif
		Endif
		* ---
		*this.FillFields(cFields)
		This.bFillFields=Not(m.i_bFile) && alla prima query li carica effettivamente
		If Vartype(m.i_cFields)='C'
			This.cStartFields=m.i_cFields
		Endif
		* --- ora cerca il default
		If !m.i_bFile
			If !This.bNoDefault
				i_cZoomFile=cp_FindDefault(m.i_cFile+'_vzm')
			Endif
			If !Empty(m.i_cZoomFile)
				p=cp_GetPFile(Forceext(m.i_cZoomFile,m.i_cFile+'_vzm'))
				If Not(Empty(m.p))
					This.cZoomName=m.i_cZoomFile
					xcg=Createobject('pxcg','load',m.p)
					This.Serialize(m.xcg)
					This.bFillFields=.F.
					* --- trovata la configurazione
					i_bFile=.T.
					* ---
				Endif
			Endif
		Endif
		* --- Imposta le dimensioni di default se non esiste la configurazione
		If !m.i_bFile And Lower(This.Name)='autozoom'
			If Type('this.parent.parent.parent.height')='N'
				This.Parent.Parent.Parent.Height=Iif(Vartype(g_MINZOOMHEIGHT)='N',g_MINZOOMHEIGHT, 400)
				This.Parent.Parent.Parent.Width=Iif(Vartype(g_MINZOOMWIDTH)='N',g_MINZOOMWIDTH, 500)
			Endif
		Endif
		* ---
		This.Query(m.i_cInitsel)
	Proc SetZoomOnZoom(cZoomOnZoom)
		If !Empty(cZoomOnZoom)
		**** ZUCCHETTI AULLA INIZIO - nell'elenco delle gestioni lo ZoomOnZoom non ci deve essere
			This.zz.Visible=LOWER(this.parent.name)<>"autozoom"
		**** ZUCCHETTI AULLA FINE - nell'elenco delle gestioni lo ZoomOnZoom non ci deve essere
			This.cZoomOnZoom=cZoomOnZoom
		Endif
	Proc CloseFile()
		If Not(This.bFileError) And Not(Empty(This.cSymFile))
			cp_CloseTable(This.cSymFile)
		Endif
	Proc Destroy()
		* --- chiudo riferimento ad oggetto header
		This.oRefHeaderObject=Null
		* --- "chiude" il file
		This.CloseFile()
		* --- chiude i cursori
		Use In Select(This.cCursor)
	Proc FillFields(cFields)
		Local i,j
		* --- Gestione Sensitive (Problema database Case sensitive sui metadati)
		* --- Se tabella fuori dall'analisi campi in minuscolo (tabelle di sistema)
		* --- altrimenti leggo il dato dall'analisi...
		Local nIdx
		nIdx = 0
		*--- Se non ho una query recupero i nomi dei campi da XDC
		If Empty(This.cCpQueryName)
			Cp_ReadXdc()
			nIdx=i_Dcx.GetTableIdx(This.cSymFile)
		Endif
		* --- Gestione Sensitive Fine
		This.nFields=Fcount()
		Dimension This.cFields(Max(1,This.nFields))
		j=1
		* -- resetta la lista dei campi
		For i=1 To Fcount()
			If Field(i)=='CPCCCHK'
				This.nFields=This.nFields-1
				Dimension This.cFields(This.nFields)
			Else
				* --- Gestione Sensitive
				* --- se la tabella � in analisi allora leggo
				* --- il nome del campo altrimenti lascio FIELD(I) quindi maiuscolo..
				Local nIdxField
				nIdxField = i_Dcx.Getfieldidx(This.cSymFile,Field(i))
				* --- Se trovo il campo in analisi recupero la descrizione altrimenti lascio il nome del campo...
				If nIdxField>0 And nIdx<>0
					This.cFields(j)=i_Dcx.Getfieldname(This.cSymFile,nIdxField)
				Else
					This.cFields(j)= Field(i)
				Endif
				j=j+1
			Endif
		Next
		* --- toglie le colonne in posizione fissa
		i=1
		Do While i<=This.ControlCount
			If Lower(Left(This.Controls(i).Name,6))='column'
				This.RemoveObject(This.Controls(i).Name)
			Else
				i=i+1
			Endif
		Enddo
		This.bFillFields=.F.
		Return
	Proc Resize()
		Local nShift,i
		Thisform.LockScreen=.T.
		nShift=Iif(This.bOptions,Iif(This.oButtonOption.bVisibleOption,Iif(This.bAdvanced And Vartype(This.advpage)="O", This.advpage.Height, 0),-30),-35)
		If This.Resizable
			If This.Parent.BaseClass='Page'
				This.Width=This.Parent.Parent.Width
				This.Height=Max(83+nShift,This.Parent.Parent.PageHeight-This.Parent.Parent.Top)
			Else
				This.Width=This.Parent.Width
				This.Height=Max(83+nShift,This.Parent.Height)-zb_shift
			Endif
		Endif
		If Pemstatus(Thisform, "oTitleBar",5) And (This.Height - 18) > 0
			This.Height = This.Height - 18
		ENDIF
		*--- FormDecorator
		If (Pemstatus(This.Parent, "oDec",5) OR (Pemstatus(Thisform, "oDec",5) AND LOWER(this.name)="autozoom")) And (This.Height - Thisform.oDec.BorderWidth) > 0
			This.Height = This.Height - Thisform.oDec.BorderWidth - IIF(LOWER(This.Parent.baseclass)="form", Thisform.oDec.Height,0)
		Endif
		This.AdjustToSize()
		Return
	Procedure AdjustToSize()
		Local nShift, nGridResize
		nShift=Iif(This.bOptions,Iif(This.oButtonOption.bVisibleOption,Iif(This.bAdvanced And Vartype(This.advpage)="O", This.advpage.Height, 0),-30),-35)
		nGridResize = Iif(This.bMobilityMode And Vartype(i_ThemesManager)='O' And i_bMobileMode, 40, 0)
		This.grd.Width=Max(20,This.Width-This.grd.Left-This.nGrdW)
		This.grd.Height=Max(20,This.Height-This.grd.Top-This.nGrdH-nShift-nGridResize)
		If This.bMobilityMode And Vartype(i_ThemesManager)='O' And i_bMobileMode
			*this.grd.scrollbars = 0
			With This.btnLeft
				.Top=This.Height - 40
				.Left=This.grd.Left
				.Visible = .T.
			Endwith
			With This.btnRight
				.Top=This.Height - 40
				.Left=This.btnLeft.Left + This.btnLeft.Width + 3
				.Visible = .T.
			Endwith
			With This.btnDown
				.Top=This.Height - 40
				.Left=This.grd.Left + This.grd.Width - This.btnDown.Width
				.Visible = .T.
			Endwith
			With This.btnUp
				.Top=This.Height - 40
				.Left=This.btnDown.Left - This.btnDown.Width - 3
				.Visible = .T.
			Endwith
		Endif

		This.oButtonOption.Top=This.Height-35-nShift
		This.oButtonOption.Width=This.grd.Width
		This.lbl.Top=This.Height-26-nShift
		This.rep.Top=This.Height-30-nShift
		This.src.Top=This.Height-30-nShift
		This.sel.Top=This.Height-30-nShift
		This.Dir.Top=This.Height-30-nShift
		This.ref.Top=This.Height-30-nShift
		This.adv.Top=This.Height-30-nShift
		This.qry.Top=This.Height-30-nShift
		This.zz.Top=This.Height-30-nShift
		This.morerows.Top=This.Height-51-nShift
		This.morerows.Left=This.Width-26
		If This.oButtonOption.bVisibleOption And This.bAdvanced
			If  Vartype(This.advpage)="O"
				This.advpage.Top=This.Height-This.advpage.Height
			Endif
			If Type("this.grdshp_SE")='O'
				This.grdshp_SE.Top=This.grd.Top+This.grd.Height-9
				This.grdshp_SE.Left=This.grd.Left+This.grd.Width-9
				This.grdshp_NW.Top=This.grd.Top-3
				This.grdshp_NW.Left=This.grd.Left-3
			Endif
		Endif
		For i=1 To This.nColPositions
			N=Alltrim(Str(i))
			If Type('this.column'+N)='O'
				If This.nColX[i]<0
					This.Column&N..Left=This.grd.Left+This.grd.Width-This.nColX[i]
				Endif
				If This.nColY[i]<0
					This.Column&N..Top=This.grd.Top+This.grd.Height-This.nColY[i]
				Endif
				This.columnlabel&N..Top=This.Column&N..Top+2
				This.columnlabel&N..Left=This.Column&N..Left-Len(Trim(This.columnlabel&N..Caption))*7-6
			Endif
		Next
		Thisform.LockScreen=.F.
	Proc SaveColumnStatus()
		Local i,N
		This.nColPositions=Min(This.nFields,Fcount(This.cCursor))
		If This.nColPositions=0
			Return
		Endif
		Dimension This.nColWidth[this.nFields]
		Dimension This.nColOrd[this.nFields]
		Dimension This.cColTitle[this.nFields]
		Dimension This.cColFormat[this.nFields]
		Dimension This.nColRW[this.nFields]
		Dimension This.nColX[this.nFields]
		Dimension This.nColY[this.nFields]
		Dimension This.nColH[this.nFields]
		Dimension This.cForeColor[this.nFields]
		Dimension This.cBackColor[this.nFields]
		Dimension This.cHyperLink[this.nFields]
		Dimension This.cHyperLinkTarget[this.nFields]
		Dimension This.bIsImage[this.nFields]
		Dimension This.cLayerContent[this.nFields]
		Dimension This.nColumnType[this.nFields]
		Dimension This.cTooltip[this.nFields]
		Dimension This.cLabelFont[this.nFields]
		Dimension This.nDimFont[this.nFields]
		Dimension This.bItalicFont[this.nFields]
		Dimension This.bBoldFont[this.nFields]
		Dimension This.cLabelFontC[this.nFields]
		Dimension This.nDimFontC[this.nFields]
		Dimension This.bItalicFontC[this.nFields]
		Dimension This.bBoldFontC[this.nFields]
		This.nLockColumns=This.grd.LockColumns
		For i=1 To This.grd.ColumnCount
			If i<=This.nFields
				IF (This.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom)
					If Left(This.grd.Columns(i).ControlSource,17)='cp_MemoFirstLine('
						This.cFields(i)=cp_ControlSourceMemo(This.grd.Columns(i).ControlSource)
					Else
						This.cFields(i)=This.grd.Columns(i).ControlSource
					ENDIF
				endif
				N=Val(Substr(This.grd.Columns(i).Name,7))
				If N<=This.nFields
					This.nColWidth[n]=This.grd.Columns(i).Width
					This.nColOrd[n]=This.grd.Columns(i).ColumnOrder - Iif(This.bCheckBox,1,0)
					This.cColTitle[n]=Trim(This.grd.Columns(i).hdr.Caption)
					This.cColFormat[n]=This.grd.Columns(i).Tag &&this.grd.Columns(i).inputmask
					This.nColRW[n]=This.grd.Columns(i).rw &&this.grd.Columns(i).inputmask
					This.nColX[n]=0
					This.nColY[n]=0
					This.nColH[n]=This.grd.Columns(i).hdr.nHeight
					This.cForeColor[n]=This.grd.Columns(i).DynamicForeColor
					This.cBackColor[n]=This.grd.Columns(i).DynamicBackColor
					This.cHyperLink[n]=Trim(This.grd.Columns(i).hdr.cHyperLink)
					This.cHyperLinkTarget[n]=Trim(This.grd.Columns(i).hdr.cHyperLinkTarget)
					This.bIsImage[n]=This.grd.Columns(i).hdr.bIsImage
					This.cLayerContent[n]=This.grd.Columns(i).hdr.cLayerContent
					This.nColumnType[n]=This.grd.Columns(i).hdr.nColumnType
					This.cTooltip[n]=This.grd.Columns(i).hdr.cTooltip
					This.cLabelFont[n]=This.grd.Columns(i).hdr.cLabelFont
					This.nDimFont[n]=Iif(Vartype(This.grd.Columns(i).hdr.nDimFont)<>'N', 0, This.grd.Columns(i).hdr.nDimFont)
					This.bItalicFont[n]=This.grd.Columns(i).hdr.bItalicFont
					This.bBoldFont[n]=This.grd.Columns(i).hdr.bBoldFont
					This.cLabelFontC[n]=This.grd.Columns(i).cLabelFontC
					This.nDimFontC[n]=Iif(Vartype(This.grd.Columns(i).nDimFontC)<>'N', 0, This.grd.Columns(i).nDimFontC)
					This.bItalicFontC[n]=This.grd.Columns(i).bItalicFontC
					This.bBoldFontC[n]=This.grd.Columns(i).bBoldFontC
				Endif
			Endif
		Next
		Return
	Procedure SetColumnSource(i_nIdx,i_cSource,i_cTitle)
		This.grd.Columns(i_nIdx).ControlSource=i_cSource
		This.grd.Columns(i_nIdx).hdr.Caption=Iif(Type("i_cTitle")='C',i_cTitle,i_cSource)
		Return
	Procedure Visible_Assign
		Lparameters vNewVal
		*To do:
		This.Visible = m.vNewVal
		Local cObjManyHeaderName
		cObjManyHeaderName="oManyHeader_"+This.Name
		If Vartype(This.oRefHeaderObject)='O'
			This.oRefHeaderObject.Visible=This.Visible
		Endif
	Endproc
	Proc Query(initsel,bSameTable,i_bNoFilter,i_bDontFill,i_bCreateHeader,i_bNotRemoveObject)
		* --- esegue la query sull' archivio principale, creando il cursore su cui operera'
		Local fld,ord,whe,i,j,k,del,op,Key,aFlds,i_timedmsg,s,i_tmpcursor,whe_t,bReCreateHeader, bThisFormLockScreen
		LOCAL TestMacro
		Dimension aFlds[1]
		This.o_initsel=initsel
		If Empty(This.cSymFile)
			Return
		Endif
		bThisFormLockScreen=Thisform.LockScreen
		Thisform.LockScreen=.T.
		This.grd.Enabled=.F.
		* --- Zucchetti Aulla inizio - non notifico se zoom con selezione
		If Type('this.parent.oContained')='O' AND UPPER(this.ParentClass)<>"ZZBOX"
		* --- Zucchetti Aulla fine - non notifico se zoom con selezione
			* --- lasciato per compatibilita'
			This.Parent.oContained.NotifyEvent(This.Name+' before query')
			* --- forma corretta
			This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' before query')
		Endif
		If Not i_bNotRemoveObject
			Local cObjManyHeaderName
			cObjManyHeaderName="oManyHeader_"+This.Name
			If Vartype(This.oRefHeaderObject)='O' And (Vartype(i_bNoreMoveDblClick)<>'L' Or Not i_bNoreMoveDblClick)
				This.oRefHeaderObject.Parent.RemoveObject(cObjManyHeaderName)
				This.oRefHeaderObject=Null
				bReCreateHeader=.T.
			Else
				bReCreateHeader=.F.
			Endif
		Endif
		* --- se e' sulla stessa tabella, crea un cursore fasullo che ottimizza la riscrittura della griglia
		If bSameTable And Used(This.cCursor)
			Select (This.cCursor)
			=Afields(aFlds)
			i_tmpcursor=Sys(2015)
			Create Cursor (i_tmpcursor) From Array aFlds
			This.grd.RecordSource=i_tmpcursor
			Select (This.cCursor)
		Else
			bSameTable=.F.
		Endif
		* --- i campi da selezionare
		If This.bFillFields
			fld=This.cStartFields && '*'
		Else
			fld=''
			*--- Rileggo XDC
			Cp_ReadXdc()
			For i=1 To This.nFields
				If Lower(This.cFields(i))<>'xchk'
					If Isnull(This.oCpQuery) And i_Dcx.IsFieldNameTranslate(This.cSymFile,This.cFields(i))
						fld=fld+Iif(i=1,'',',')+cp_TransLinkFldName(This.cFields(i))+' as '+This.cFields(i)
					Else
						fld=fld+Iif(i=1,'',',')+This.cFields(i)
					Endif
				Endif
			Next
			If Empty(fld)
				fld='*'
			Endif
		Endif
		* -- l' ordine da utilizzare
		ord=''
		If Not Isnull(This.oCpQuery)
			If Type("This.oCpQuery.bNoBindVariable")='L'
				This.oCpQuery.bNoBindVariable=.F.
			Endif
			With This.oCpQuery
				.PrepareOrderBy()
				If .nOrderBy>0
					If Not This.bOrderQueryTransfer
						Dimension This.cOrderBy[this.nOrderBy+.nOrderBy]
						Dimension This.bOrderDesc[this.nOrderBy+.nOrderBy]
						Dimension This.cOrderByDefault[this.nOrderBy+.nOrderBy]
						Dimension This.bOrderDescDefault[this.nOrderBy+.nOrderBy]
						This.nOrderBy=This.nOrderBy+.nOrderBy
						This.nOrderByDefault=This.nOrderByDefault+.nOrderBy
						For i=1 To .nOrderBy
							Ains(This.cOrderBy, i)
							Ains(This.cOrderByDefault, i)
							This.cOrderBy[i]=.i_OrderBy[i,1]
							This.cOrderByDefault[i]=.i_OrderBy[i,1]
							* --- vado alla ricerca l'alias....
							k=Ascan(.i_Fields,This.cOrderBy[i],-1,-1,1,15)
							If k>0
								This.cOrderBy[i]=.i_Fields[k,2]
								This.cOrderByDefault[i]=.i_Fields[k,2]
							Endif
							Ains(This.bOrderDesc, i)
							Ains(This.bOrderDescDefault, i)
							This.bOrderDesc[i]=Iif(.i_OrderBy[i,2]='Ascending',.F.,.T.)
							This.bOrderDescDefault[i]=Iif(.i_OrderBy[i,2]='Ascending',.F.,.T.)
						Endfor
						This.bOrderQueryTransfer=.T.
					Endif
					.nOrderBy=0 && le order by della query vengono riportate sullo zoom
				Endif
			Endwith
		Endif
		For i=1 To This.nOrderBy
			j=0
			* e' preferibile utilizzare il numero del campo piuttosto che il nome:
			* meno problemi in query complesse con campi qualificati o con union e piu' veloce
			If Isnull(This.oCpQuery)
				k=Ascan(This.cFields,This.cOrderBy[i],-1,-1,1,15)
				If k>0
					j=k
				Endif
			Else
				* --- se ho la query vado a ricerca l'alias....
				k=Ascan(This.oCpQuery.i_Fields,This.cOrderBy[i],-1,-1,2,15)
				If k>0
					j=k
				Endif
			ENDIF
			If (j=0 And At(This.cOrderBy[i],ord)=0) Or (j<>0 And At(' '+Alltrim(Str(j))+' ',' '+ord+' ')=0)
				ord=ord+Iif(i=1,'',' , ')+Iif(j=0,This.cOrderBy[i],Alltrim(Str(j)))+Iif(This.bOrderDesc[i], +' desc', '')
			Endif
		Next
		*--- mancava il controllo della variabile w_ORERBY che modifica l'ordinamento
		If Not Isnull(This.oCpQuery) And Not Empty(This.oCpQuery.cOrderByTmp)
			k=This.oCpQuery.cOrderByTmp
			i=At(' desc',Lower(k))
			If i<>0
				k=Left(k,i-1)
			Endif
			If At(' '+k+' ',' '+ord+' ')=0
				ord = ord+Iif(Empty(ord),'',',')+This.oCpQuery.cOrderByTmp
			Endif
		Endif
		* --- costruisce la where
		whe=''
		op=''
		For i=1 To This.nWhere
			whe_t=This.cWhere(i)
			If Right(Trim(whe_t),2)=']]'
				whe_t=Left(whe_t,At('[[',whe_t)-1)
			Endif
			If !(whe_t=='OR')
				If This.nConn<>0
					whe=whe+op+Trim(This.toodbc(whe_t))
				Else
					whe=whe+op+Trim(whe_t)
				Endif
				op=' and '
			Else
				op=Iif(!Empty(op),' or ',op)  && la or non puo' essere al primo posto
			Endif
		Next
		* ---
		If Isnull(This.oCpQuery)
			If Vartype(initsel)='C' And !Empty(initsel)
				* --- caso di query inizializzata dall' esterno (zoom standard su selezioni aperte)
				whe=Iif(Empty(whe),initsel,'(('+whe+') and ('+initsel+'))')
			Endif
			* --- costruisce la frase SQL (query interna)
			If !Empty(whe)
				whe=' where '+cp_SetSQLFunctions(whe,CP_GETDATABASETYPE(This.nConn))
			Endif
			If !Empty(ord)
				ord=' order by '+ord
			Endif
			* --- crea effettivamente la frase SQL
			If This.bModSQL And !Empty(This.cSQL)
				* --- Zucchetti Aulla inizio - i_Codazi non trimmato
				s=Strtran(Strtran(This.cSQL,'xxx',Alltrim(i_codazi)),Chr(13)+Chr(10),' ')
				If !Empty(whe)
					If ' where '$s
						s=Strtran(s,' where ',whe+' and ')
					Else
						s=s+whe
					Endif
				Endif
				If !Empty(ord)
					If ' order by '$s
						s=Strtran(s,' order by ',ord+',')
					Else
						s=s+ord
					Endif
				Endif
			Else
				s="select "+fld+" from "+This.cFile+Iif(This.nConn<>0," "," as ")+This.lFile+" "+whe+" "+ord
			Endif
			* --- esegue
			If This.nConn<>0
				*=cp_SQL(this.nConn,"select "+fld+" from "+this.cFile+" "+whe+" "+ord,this.cCursor)
				Local __sqlx__
				__sqlx__=This.GetSqlx()
				If Type("__sqlx__")<>'O'
					cp_sql(This.nConn,s,This.cCursor)
				Else
					__sqlx__.SQLExec(This.nConn,s,This.cCursor,This.cDatabaseType)
					*--- Zucchetti Aulla Inizio - gestione riconessione degli zoom secondari degli elenchi
					thisform.ozoomelenco=this
					*--- Zucchetti Aulla Fine - gestione riconessione degli zoom secondari degli elenchi
				Endif
			Else
				i_timedmsg=Createobject('timedmsg',cp_Translate(MSG_EXECUTING_QUERY_D),3)
				s=s+Iif(' where '$s Or i_bNoFilter Or !This.bReadOnly,' nofilter','')
				Use In Select(This.cCursor)
				&s Into Cursor (This.cCursor)
			Endif
			This.cOldSelect=Left(s,At('FROM',Upper(s)))
		Else
			* --- utilizza la query esterna
			i_timedmsg=.Null.
			This.oCpQuery.mLoadWhereTmp(whe)
			Thisform.LockScreen=bThisFormLockScreen
			* --- Se query con filtro aggiuntivo e non trovo l'alias della tabella all'interno della
			* --- visual query allora applico una ulteriore Sub select sulla query
			* --- perch� se utilizzo alias nei query filter potrebbero essere diversi da quelli specificati nella query
			Local bSub
			bSub=.F.
			If i_bSecurityRecord
				Local nIdx
				If Not Empty( initsel )
					#If Version(5)>=900
						nIdx=Ascan(This.oCpQuery.i_Files,This.lFile,1,1,2,15)
					#Else
						Local i_i
						nIdx=0
						For i_i=1 To Alen(This.oCpQuery.i_Files,1)
							If  Not Empty( This.oCpQuery.i_Files[i_i,2])
								If Lower(This.oCpQuery.i_Files[i_i,2])==Lower(This.lFile)
									nIdx=i_i
									Exit
								Endif
							Else
								nIdx=0
								Exit
							Endif
						Next
					#Endif
					bSub= (nIdx=0 )
				Endif
			Endif

			If bSub
				This.oCpQuery.nFiles=Iif(This.oCpQuery.nFiles=0,This.oCpQuery.nFilesOld,This.oCpQuery.nFiles)
				s='select * from ('+This.oCpQuery.mDoQuery('nocursor','Get',.Null.)+') '+Iif(This.nConn<>0," "," as ")+This.lFile+' where '+initsel+Iif(Not Empty(ord)," order by "+ord,"")
				If This.nConn<>0
					Local __sqlx__
					__sqlx__=This.GetSqlx()
					If Type("__sqlx__")<>'O'
						cp_sql(This.nConn,s,This.cCursor)
					Else
						__sqlx__.SQLExec(This.nConn,s,This.cCursor,This.cDatabaseType)
						*--- Zucchetti Aulla Inizio - gestione riconessione degli zoom secondari degli elenchi
						thisform.ozoomelenco=this
						*--- Zucchetti Aulla Fine - gestione riconessione degli zoom secondari degli elenchi
					Endif
				Else
					i_timedmsg=Createobject('timedmsg',cp_Translate(MSG_EXECUTING_QUERY_D),3)
					s=s+Iif(' where '$s Or i_bNoFilter Or !This.bReadOnly,' nofilter','')
					Use In Select(This.cCursor)
					&s Into Cursor (This.cCursor)
				Endif
			Else
				If Vartype(initsel)='C' And !Empty(initsel)
					This.oCpQuery.mLoadWhereTmp(initsel)
				Endif
				This.oCpQuery.mLoadOrderByTmp(ord)
				*--- Zucchetti Aulla Inizio, query sincrona
				*this.oCpQuery.mDoQuery(this.cCursor,'Exec',this.GetSqlx(),i_bNoFilter or !this.bReadOnly)
				This.oCpQuery.mDoQuery(This.cCursor,'Exec',Iif(Type('this.bSinc') = 'L' And This.bSinc, .T.,This.GetSqlx()),i_bNoFilter Or !This.bReadOnly)
				*--- Zucchetti Aulla Fine, query sincrona
			Endif
			Thisform.LockScreen=.T.
		Endif
		*--- Rendo il cursore scrivibile se sono su dbfox (problema query semplici)
		If This.nConn=0 And !This.bAddedCheckBox And This.nFieldsImage=0
			Local l_TmpName, l_OldArea
			l_TmpName = Sys(2015)
			Select(This.cCursor)
			l_OldArea = Select()
			Use Dbf() Again In 0 Alias (m.l_TmpName)
			Use
			Select(m.l_TmpName)
			Use Dbf() Again In (m.l_OldArea) Alias (This.cCursor)
			Use
			Select(This.cCursor)
		Endif

		* --- GESTIONE IMMAGINI ZOOM INIZIO
		Local l_expr, l_SelectChk, l_SelectImage, l_ImgJoin, l_FldImage, l_AliasFieldImage, l_FindXchk
		Local l_oldext, l_dim, l_FileImage, l_StrImg
		Local l_Zolderr, l_bzerr
		m.l_FindXchk=""
		m.l_SelectChk=""
		m.l_SelectImage=""
		m.l_ImgJoin=""
		m.l_FldImage=""
		m.l_AliasFieldImage=""
		m.l_expr=""
		Dimension This.cFieldsImage(1,2)
		Dimension This.cCursorImage(1,2)
		This.nFieldsImage=0
		&& ciclo sul numero delle colonne per controllare se ce ne sono che hanno il check immagine
		For i=1 To This.nFields
			If Lower(This.cFields(i))<>'xchk' And i<=Alen(This.bIsImage)
				If This.bIsImage(i)
					&& Tipo immagine la memorizzo nell'array
					This.nFieldsImage=This.nFieldsImage+1
					Dimension This.cFieldsImage[this.nFieldsImage,2]
					Dimension This.cCursorImage[this.nFieldsImage]
					This.cFieldsImage[this.nFieldsImage,1]= Alltrim(This.cFields[i])
					This.cFieldsImage[this.nFieldsImage,2]= This.nColWidth[i] && predisposto per la gestione della dimensione delle immagini
					This.cCursorImage[this.nFieldsImage]=Sys(2015) && assegno il nome del cursore alla colonna
				Endif
			Endif
		Next
		&& Questo codice viene eseguito solo se all'interno dello zoom
		&& ho delle immagini cos� � pi� veloce
		If This.nFieldsImage>0
			For i=1 To This.nFieldsImage
				Use In Select(This.cCursorImage(i))
				&& this.cCursorImage(i) = SYS(2015)
				m.l_FldImage=This.cFieldsImage[i,1]
				m.l_AliasFieldImage=This.cFieldsImage[i,1]+"_ZIMG"
				&& Metto una Nvl altrimenti poi fallisce la Join
				Select Distinct Nvl(&l_FldImage, 'NullImage') As (m.l_FldImage) From (This.cCursor) Into Cursor (This.cCursorImage(i))
				If Used(This.cCursorImage(i))
					Select *, Cast(Space(0) As Memo) As (m.l_AliasFieldImage) From (This.cCursorImage(i)) Into Cursor (This.cCursorImage(i)) NOFILTER Readwrite				
					m.l_SelectImage = m.l_SelectImage + "," + m.l_AliasFieldImage
					m.l_ImgJoin = m.l_ImgJoin + " left outer join " + This.cCursorImage(i) + " on "
					m.l_ImgJoin = m.l_ImgJoin + This.cCursor + "." + m.l_FldImage
					m.l_ImgJoin = m.l_ImgJoin + "=" + This.cCursorImage(i)+"." + m.l_FldImage
					Select(This.cCursorImage(i))
					Go Top
					Scan
						If Vartype(i_ThemesManager)=='O'
							m.l_FileImage = Nvl(Evaluate(This.cFieldsImage[i,1]),"")
							&& per ora la dimensione � sempre 16 (icona)
							m.l_dim = 16 && This.cFieldsImage[i,2]
							m.l_oldext=Lower(Justext(m.l_FileImage))
							m.l_oldext=Iif(m.l_oldext=='ico',"bmp",m.l_oldext)
							m.l_FileImage = i_ThemesManager.RetBmpFromIco(m.l_FileImage, m.l_dim) + '.' + m.l_oldext
						Else
							l_FldImage = Nvl(Evaluate(This.cFieldsImage[i,1]),"")
							m.l_FileImage = Addbs(Justpath(m.l_FldImage))+Juststem(m.l_FldImage)
						Endif
						&& nel caso si verifichi un errore significa che non ho trovato l'immagine
						&& forzo immagine bianca white_small.bmp
						Local l_Zolderr
						l_Zolderr=On('ERROR')
						l_bzerr=.F.
						On Error l_bzerr =.T.
						m.l_StrImg = Filetostr(m.l_FileImage)
						On Error &l_Zolderr
						If l_bzerr
							m.l_FileImage = ".\bmp\white_small.bmp"
							If Vartype(i_ThemesManager)=='O'
								m.l_dim = 16
								m.l_oldext=Lower(Justext(m.l_FileImage))
								m.l_oldext=Iif(m.l_oldext=='ico',"bmp",m.l_oldext)
								m.l_FileImage = Forceext(i_ThemesManager.RetBmpFromIco(m.l_FileImage, m.l_dim),m.l_oldext)
							Else
								m.l_FileImage = Addbs(Justpath(m.l_FileImage))+Juststem(m.l_FileImage)
							Endif
							m.l_StrImg = Filetostr(m.l_FileImage)
						Endif
						Select(This.cCursorImage(i))
						Replace (m.l_AliasFieldImage) With (m.l_StrImg)
						Select(This.cCursorImage(i))
					Endscan
				Endif
			Next
		Endif
		* --- Modificata con introduzione delle immagini negli zoom
		* --- Compongo la select nel caso sia presente il check
		* --- Zucchetti Aulla inizio - introdotta variabile l_FindXchk per vedere se il campo xchk � gi� stato inserito nella query con un valore numerico
		m.l_FindXchk=This.cCursor + ".xchk"
		If This.bAddedCheckBox
		 TestMacro=TYPE('&l_FindXchk')
			if TestMacro<>'N'
				m.l_SelectChk = ", 0 as xchk"
			endif	
		Endif
		* --- Zucchetti Aulla fine - introdotta variabile l_FindXchk per vedere se il campo xchk � gi� stato inserito nella query con un valore numerico
		* --- Nel caso in cui abbia xchl oppure colonne contenenti immagini ricreo il cursore
		If This.bAddedCheckBox Or This.nFieldsImage>0
			m.l_expr = This.cCursor + ".*" + m.l_SelectChk + m.l_SelectImage
			Select &l_expr From (This.cCursor) &l_ImgJoin Into Cursor (This.cCursor)
		Endif
		* --- chiude i cursori delle colonne che sono di tipo immagine
		If This.nFieldsImage>0
			For i=1 To This.nFieldsImage
				Use In Select(This.cCursorImage(i))
			Next
		Endif
		* ---GESTIONE IMMAGINI ZOOM FINE
		If !This.bReadOnly
			Select (This.cCursor)
			Local i_tmpname
			i_tmpname=Sys(2015)
			Use Dbf() Again In 0 Alias (i_tmpname)
			Use
			Select (i_tmpname)
			Use Dbf() Again In 0 Alias (This.cCursor)
			Use
			Select (This.cCursor)
		Endif
		If !i_bDontFill Or This.nColPositions=0
			* --- riempie la lista dei campi
			If This.bFillFields And Used(This.cCursor)
				This.FillFields()
				bSameTable=.F.
				This.nColPositions=0
			Endif
			* --- riempie la griglia
			If bSameTable
				This.grd.fillagain()
			Else
				This.grd.ColumnCount=0
				If Used(This.cCursor)
					This.grd.Fill()
				Endif
			Endif
		Endif
		If This.nColPositions=0 And Used(This.cCursor)
			This.SaveColumnStatus()
		Endif
		If Not i_bNotRemoveObject And (i_AdvancedHeaderZoom Or This.bAdvancedHeaderZoom) 
			Do Case
				Case (bReCreateHeader Or i_bCreateHeader) And (i_AdvancedHeaderZoom Or This.bAdvancedHeaderZoom) And Vartype(This.oRefHeaderObject)<>"O"
					This.Parent.AddObject(cObjManyHeaderName,"ManyHeader")
					This.oRefHeaderObject=This.Parent.&cObjManyHeaderName
					This.oRefHeaderObject.InitHeader(This.grd,.T.)
				Case Not (Vartype(i_bNoreMoveDblClick)<>'L' Or Not i_bNoreMoveDblClick) And Vartype(This.oRefHeaderObject)="O"
					This.oRefHeaderObject.HeaderRedraw()
			Endcase
		Endif
		This.grd.Enabled=.T.
		This.bChangedFld=.F.
		If Len(whe)<=127
			This.src.ToolTipText=whe
		Else
			This.src.ToolTipText=Left(whe,124)+'...'
		Endif
		Wait Clear
		If bSameTable
			Select (i_tmpcursor)
			Use
			Select (This.cCursor)
		Endif
		Thisform.LockScreen=bThisFormLockScreen
		* --- gestione dell' avanzamento dello zoom nel caso di risultato troncato
		Local i_nrec
		If Type('i_nZoomMaxRows')='N' And Type('i_bDisableAsyncConn')<>'U' And i_bDisableAsyncConn And i_ServerConn[1,2]<>0
			i_nrec=Reccount(This.cCursor)
			If i_nrec>0 And i_nrec=i_nZoomMaxRows
				This.morerows.Visible=.T.
			Else
				This.morerows.Visible=.F.
			Endif
		Endif
		* --- Zucchetti Aulla inizio - non notifico se zoom con selezione
		If Type('this.parent.oContained')='O' AND UPPER(this.ParentClass)<>"ZZBOX"
			* --- lasciato per compatibilita'
			This.Parent.oContained.NotifyEvent(This.Name+' after query')
			* --- forma corretta
			This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' after query')
		Endif
		* --- Zucchetti Aulla fine - non notifico se zoom con selezione
		* selezione della prima riga
		This.grd.Refresh
		*--- Zucchetti Aulla Inizio - Errore refresh header (E18956)
		If Type("This.oRefHeaderObject")='O' And !Isnull(This.oRefHeaderObject)
		    This.oRefHeaderObject.InitHeader(This.grd,.T.)
		Endif
		*--- Zucchetti Aulla Fine - Errore refresh header (E18956)
		* ---
		* --- GESTIONE IMMAGINI ZOOM INIZIO
		&& Ho cambiato la query legata allo zoom
	Proc ResetFieldImages()	
		if this.nFields>0
			Dimension This.bIsImage[this.nFields]
			For i=1 To This.nFields
				This.bIsImage(i)=.f.
			Next
			&& Funnzione che serve per popolare i campi di tipo immagine nel caso venga riempito da batch
			&& per ora la funzione riempie solo quelli che sono vuoti o nulli
		endif
	Proc FillCursorImages(pWhere)
		Local nImgEmpty, l_FldImage, l_AliasFieldImage, l_FileImage, l_dim, l_oldext, l_FileImage, l_StrImg
		nImgEmpty=0
		If This.nFieldsImage>0
			For i=1 To This.nFieldsImage
				m.l_FldImage=This.cFieldsImage[i,1]
				m.l_AliasFieldImage=This.cFieldsImage[i,1]+"_ZIMG"
				m.nImgEmpty = 0
				Select(This.cCursor)
				Count For Empty(Nvl(&l_AliasFieldImage,"")) To nImgEmpty
				If m.nImgEmpty > 0 And Used(This.cCursor)
					Select(This.cCursor)
					Go Top
					Scan For Empty(Nvl(&l_AliasFieldImage,""))
						If Vartype(i_ThemesManager)=='O'
							m.l_FileImage = Nvl(Evaluate(This.cFieldsImage[i,1]),"")
							m.l_dim = 16 && This.cFieldsImage[i,2]
							&& Iif(This.ImgSize>0,This.ImgSize,16)
							m.l_oldext=Lower(Justext(m.l_FileImage))
							m.l_oldext=Iif(m.l_oldext=='ico',"bmp",m.l_oldext)
							m.l_FileImage = Forceext(i_ThemesManager.RetBmpFromIco(m.l_FileImage, m.l_dim),m.l_oldext)
						Else
							m.l_FldImage = Nvl(Evaluate(This.cFieldsImage[i,1]),"")
							m.l_FileImage = Addbs(Justpath(m.l_FldImage))+Juststem(m.l_FldImage)
						Endif
						&& nel caso si verifichi un errore significa che non ho trovato l'immagine
						&& forzo immagine bianca white_small.bmp
						Local l_Zolderr
						l_Zolderr=On('ERROR')
						l_bzerr=.F.
						On Error l_bzerr =.T.
						m.l_StrImg = Filetostr(m.l_FileImage)
						On Error &l_Zolderr
						If l_bzerr
							m.l_FileImage = ".\bmp\white_small.bmp"
							If Vartype(i_ThemesManager)=='O'
								m.l_dim = 16
								m.l_oldext=Lower(Justext(m.l_FileImage))
								m.l_oldext=Iif(m.l_oldext=='ico',"bmp",m.l_oldext)
								m.l_FileImage = Forceext(i_ThemesManager.RetBmpFromIco(m.l_FileImage, m.l_dim),m.l_oldext)
							Else
								m.l_FileImage = Addbs(Justpath(m.l_FileImage))+Juststem(m.l_FileImage)
							Endif
							m.l_StrImg = Filetostr(m.l_FileImage)
						Endif
						Select(This.cCursor)
						Update (This.cCursor) Set &l_AliasFieldImage = (m.l_StrImg) Where &l_AliasFieldImage=Evaluate(m.l_FldImage)
						&& REPLACE (m.l_AliasFieldImage) WITH (m.l_StrImg) FOR (m.l_FldImage)=&l_FldImage
						Select(This.cCursor)
					Endscan
				Endif
			Next
		Endif
		* --- ZUCCHETTI AULLA GESTIONE IMMAGINI ZOOM FINE
		If Type('this.parent.oContained')='O'
			* --- lasciato per compatibilita'
			This.Parent.oContained.NotifyEvent(This.Name+' after query')
			* --- forma corretta
			This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' after query')
		Endif
		* ---
	Proc Requery()
		This.Query(This.o_initsel,.T.)
	Proc GetSqlx()
		Return(Thisform.__sqlx__)
	Proc DeferredQuery()
		If This.bChangedFld
			cp_ErrorMsg(MSG_FIELD_LIST_CHANGED_QM,16)
		Else
			Local i_bNoreMoveDblClick
			i_bNoreMoveDblClick=.T.
			This.Query(This.o_initsel,.T.)
		Endif
	Proc SetWhereCaption(cs,ct)
		This.lbl.Caption=Trim(ct)
		This.cWhereCol=cs
		If At('.',cs)<>0
			This.cWhereCol=Substr(cs,At('.',cs)+1)
		Endif
		*--- Seleziono il cursore dello zoom per essere sicuro di trovare il campo
		Local l_OldArea
		l_OldArea = Select()
		Select(This.cCursor)
		This.src.Value=cp_NullValue(cs)
		Select(l_OldArea)
		*--- Metto la solita inputmask anche sul campo filtro
		Local l_nCol
		l_nCol = This.Getcol(This.cWhereCol)
		If l_nCol>0 And l_nCol<=This.grd.ColumnCount
			This.src.InputMask = This.grd.Columns[This.Getcol(This.cWhereCol)].InputMask
		ELSE
		*--- I campi memo utilizzano cp_memofirst, quindi GetCol fallisce
			This.src.InputMask = ""
		Endif

	Proc RefreshParamWnd()
		If This.bAdvanced And Vartype(This.advpage)="O"
			This.advpage.FillWhere()
			This.advpage.FillOrderBy()
			This.advpage.FillFld()
			This.advpage.FillSql()
			This.advpage.FillParam()
		Endif
	Proc DeleteColumn(nPos)
		If nPos<=This.nColPositions
			Local nColPosition
			This.nColPositions=This.nColPositions-1
			=Adel(This.cFields,nPos)
			This.nFields=This.nFields-1
			*this.bChangedFld=.t.
			=Adel(This.nColWidth,nPos)
			nColPosition=This.nColOrd[nPos]
			=Adel(This.nColOrd,nPos)
			For i=1 To This.nColPositions
				This.nColOrd[i]=Iif(This.nColOrd[i]>nColPosition, This.nColOrd[i]-1, This.nColOrd[i])
			Next
			=Adel(This.cColTitle,nPos)
			=Adel(This.cColFormat,nPos)
			=Adel(This.nColRW,nPos)
			=Adel(This.nColX,nPos)
			=Adel(This.nColY,nPos)
			=Adel(This.nColH,nPos)
			=Adel(This.cForeColor,nPos)
			=Adel(This.cBackColor,nPos)
			=Adel(This.cHyperLink,nPos)
			=Adel(This.cHyperLinkTarget,nPos)
			=Adel(This.bIsImage,nPos)
			=Adel(This.cLayerContent,nPos)
			=Adel(This.nColumnType,nPos)
			=Adel(This.cTooltip,nPos)
			=Adel(This.cLabelFont,nPos)
			=Adel(This.nDimFont,nPos)
			=Adel(This.bItalicFont,nPos)
			=Adel(This.bBoldFont,nPos)
			=Adel(This.cLabelFontC,nPos)
			=Adel(This.nDimFontC,nPos)
			=Adel(This.bItalicFontC,nPos)
			=Adel(This.bBoldFontC,nPos)			
		Endif
	Proc AddWhere(s)
           If (Not (Empty(s)))
		This.nWhere=This.nWhere+1
		Dimension This.cWhere(This.nWhere)
		This.cWhere(This.nWhere)=s
		This.RefreshParamWnd()
           Endif
	Proc SetWhere(i,s)
		This.cWhere(i)=+Iif(Left(This.cWhere(i),1)='(','(','')+s+Iif(Right(Alltrim(This.cWhere(i)),1)=')',')','')
		This.RefreshParamWnd()
	Proc DeleteWhere(i)
		=Adel(This.cWhere,i)
		This.nWhere=iif(This.nWhere=0,This.nWhere,This.nWhere-1)
		This.RefreshParamWnd()
	Proc AddOrderBy(cs)
		This.nOrderBy=This.nOrderBy+1
		Dimension This.cOrderBy[this.nOrderBy]
		This.cOrderBy[this.nOrderBy]=cs
		Dimension This.bOrderDesc[this.nOrderBy]
		This.bOrderDesc[this.nOrderBy]=.F.
		This.RefreshParamWnd()
	Proc DeleteOrderBy(i)
		=Adel(This.cOrderBy,i)
		=Adel(This.bOrderDesc,i)
		This.nOrderBy=iif(This.nOrderBy=0,This.nOrderBy,This.nOrderBy-1)
		This.RefreshParamWnd()
	Func BuildWhere(cs,v,Pop)
		Local T,w,op,i_oldarea
		i_oldarea=Select()
		If Used(This.cCursor)
			Select(This.cCursor)
		Endif
		T=Type(cs)
		Select(i_oldarea)
		op=Iif(Vartype(Pop)='C',Pop,'like')
		If This.bAdvanced And Not(Empty(This.advpage.page1.oplst.sOp))
			op=This.advpage.page1.oplst.sOp
		Endif
		Do Case
			Case T='C' .Or. T='M'
				If Isnull(v)
					v=''
				Endif
				If This.nConn=0 And At('%',v)=0 And op='like'
					* --- caso fox senza %, trasformare da like a =
					op='='
				Endif
				If This.nConn=0
					v=Strtran(v,"'","'+chr(39)+'")  && in VFP cambiamo l' apice in una espressione
				Else
					v=Strtran(v,"'","''") && in SQL ' va impostato come ''
				Endif
				If Empty(v) And Len(v)<>0
					* ------- Zucchetti Aulla Inizio - Inserimento iniziale della '%'
					*v="'"+v+iif(op='like','%','')+"'"
					v="'"+Iif(op='like' And g_RicPerCont='T', '%', '')+v+Iif(op='like','%','')+"'"
					* ------- Zucchetti Aulla Fine - Inserimento iniziale della '%'
				Else
					* ------- Zucchetti Aulla Inizio - Inserimento iniziale della '%'
					*v="'"+trim(v)+iif(op='like','%','')+"'"
					v="'"+Iif(op='like' And g_RicPerCont='T', '%', '')+Trim(v)+Iif(op='like','%','')+"'"
					* ------- Zucchetti Aulla Fine - Inserimento iniziale della '%'
				Endif
			Case T='N' Or T='Y'
				If Isnull(v)
					v=0
				Endif
				op=Iif(op='like','=',op)
				v=Strtran(Alltrim(Str(v,16,4)),',','.')
			Case T='T' Or T='D'
				op=Iif(op='like','=',op)
				If Isnull(v) Or Empty(v)
					If This.nConn=0
						v='ctod("  -  -  ")'
					Else
						op=Iif(op='<>','is not',Iif(op='=','is',op))
						v='null'
					Endif
				Else
					If Hour(v)=0 And Minute(v)=0 And Sec(v)=0
						v=Dtos(v)
						If This.nConn=0
							v='{^'+Left(v,4)+'-'+Substr(v,5,2)+'-'+Right(v,2)+'}'
						Else
							If i_ServerConn[1,6]='Interbase'
								v="'"+Left(v,4)+'-'+Substr(v,5,2)+'-'+Right(v,2)+"'"
							Else
								v="{d '"+Left(v,4)+'-'+Substr(v,5,2)+'-'+Right(v,2)+"'}"
							Endif
						Endif
					Else
						v=Ttoc(v,1)
						If This.nConn=0
							v='{^'+Left(v,4)+'-'+Substr(v,5,2)+'-'+Substr(v,7,2)+' '+ ;
								substr(v,9,2)+':'+Substr(v,11,2)+':'+Right(v,2)+'}'
						Else
							If i_ServerConn[1,6]='Interbase'
								v="'"+Left(v,4)+'-'+Substr(v,5,2)+'-'+Substr(v,7,2)+' '+Substr(v,9,2)+':'+Substr(v,11,2)+':'+Right(v,2)+"'"
							Else
								v="{ts '"+Left(v,4)+'-'+Substr(v,5,2)+'-'+Substr(v,7,2)+' '+Substr(v,9,2)+':'+Substr(v,11,2)+':'+Right(v,2)+"'}"
							Endif
						Endif
					Endif
				Endif
			Case T="L"
				op=Iif(op='like','=',op)
				v=Iif(v,'.t.','.f.')
		Endcase
		* Zucchetti Aulla inizio - sostituisco ilike con ilike
		If op='like' AND CP_DBTYPE='PostgreSQL' AND g_USE_ILIKE='S'
			op='ilike'
			* In PostgreSQL nella like e ilike il carattere escape '\' deve essere raddoppiato 
			v=STRTRAN(v,"\","\\")
		Endif
		* Zucchetti Aulla inizio - sostituisco ilike con ilike
		w=cs+' '+op+' '+v
		Return(w)
	Proc ChangeOp(i,op)
		Local j,w
		w=This.cWhere(i)
		j=At(' ',w)
		cs=Left(w,j-1)
		j=At(' ',w,2)
		v=Substr(w,j+1)
		If op='like'
			If Left(v,1)="'"
				Local p
				p=At("'",v,2)
				If Substr(v,p-1,1)<>'%'
					v=Left(v,p-1)+'%'+Substr(v,p)
				Endif
			Else
				op='='
			Endif
		Else
			v=Strtran(v,'%','')
		Endif
		* Zucchetti Aulla inizio - sostituisco ilike con ilike
		If op='like' AND CP_DBTYPE='PostgreSQL' AND g_USE_ILIKE='S'
			op='ilike'
			* In PostgreSQL nella like e ilike il carattere escape '\' deve essere raddoppiato 
			v=STRTRAN(v,"\","\\")
		Endif
		* Zucchetti Aulla inizio - sostituisco ilike con ilike
		This.cWhere(i)=cs+' '+op+' '+v
	Proc ChangeConst(i,c)
		Local j,w,p
		w=This.cWhere(i)
		j=At(' ',w,2)
		If !Inlist(Left(c,1),'"',"'")
			c=Strtran(c,',','.')
		Endif
		If Right(w,2)=']]'
			p=At('[[',w)
			This.cWhere(i)=Left(w,j)+Trim(c)+Substr(w,p-1)
		Else
			This.cWhere(i)=Left(w,j)+Trim(c)
		Endif
	Func Getcol(cn)
		Local i
		i=Ascan(This.cFields,Alltrim(cn))
		Return(Iif(i>This.nColPositions,0,i))
	Proc AskParameters()
		If This.nWhere<>0 Or !Isnull(This.oCpQuery)
			If This.nWhere<>0
				Local wnd
				wnd=Createobject('stdzParamMask',This)
				wnd.Show()
			Endif
			If !Isnull(This.oCpQuery)
				For i=1 To This.oCpQuery.nFilterParam
					This.oCpQuery.i_FilterParam[i,6]=.F.
					This.oCpQuery.i_FilterParam[i,7]=.F.
				Next
				This.oCpQuery.bAskParam=.T.
			Endif
		Else
			CP_MSG(cp_Translate(MSG_SELECTION_PARAMETERS_MISSING),.F.)
		Endif
	Proc AddFilterFast(oObj)
		Local cFieldFilter, i_nIdx, i_cursvalue, i_oldarea
		If Type("oObj")='O' And oObj.grd.nActivecolumn>0
			i_nIdx=Ascan(oObj.nColOrd, oObj.grd.nActivecolumn-Iif(oObj.bCheckBox,1,0))
			cFieldFilter=oObj.grd.Columns(i_nIdx).ControlSource
			i_oldarea=Select()
			Select (oObj.cCursor)
			i_cursvalue=&cFieldFilter
			Select(i_oldarea)
			oObj.AddWhere(oObj.BuildWhere(cFieldFilter,i_cursvalue,'='))
			oObj.RefreshParamWnd()
			oObj.DeferredQuery()
		Endif
	Endproc
	Proc AskAdvancedParameters()
		Local cTmpCurs, nNumField, op, whe_t, nPosOr, nPosAnd, cFlCommen, nCol

		If This.nWhere<>0 Or !Isnull(This.oCpQuery)
			If This.nWhere<>0
				bSaveWhere=.F.
				cTmpCurs=Sys(2015)
				Create Cursor (cTmpCurs) (TBNAME c(50), FLNAME c(50), FLTYPE c(1), FLLENGHT N(3), FLDECIMA N(1), FLCOMMEN c(50))
				Select (This.cCursor)
				nNumField=Afields(aStructCurs)
				* --- costruisce l'elenco dei campi dello zoom
				For i=1 To nNumField
					nCol=This.Getcol(aStructCurs[i,1])
					cFlCommen=Iif(nCol=0, '', This.cColTitle[nCol])
					Insert Into (cTmpCurs) Values (Space(10), aStructCurs[i,1], aStructCurs[i,2], aStructCurs[i,3], aStructCurs[i,4], cFlCommen)
				Endfor

				* --- costruisce la where
				This.cWhereAdvanced=''
				op=''
				For i=1 To This.nWhere
					whe_t=This.cWhere(i)
					If Right(Trim(whe_t),2)=']]'
						whe_t=Left(whe_t,At('[[',whe_t)-1)
					Endif
					If !(whe_t=='OR')
						If This.nConn<>0
							This.cWhereAdvanced=This.cWhereAdvanced+op+Trim(This.toodbc(whe_t))
						Else
							This.cWhereAdvanced=This.cWhereAdvanced+op+Trim(whe_t)
						Endif
						op=' and '
					Else
						op=Iif(!Empty(op),' or ',op)  && la or non puo' essere al primo posto
					Endif
				Next

				* --- apro la gestione
				Do cp_AddFilter With cTmpCurs, This.cWhereAdvanced, "cWhereAdvanced", This

				If This.bSaveWhere
					This.bSaveWhere=.F.
					* --- elimino tutti i filtri precedenti
					If This.nWhere>0
						Do While This.nWhere>0
							This.DeleteWhere(This.nWhere)
						Enddo
					Endif

					* --- filtro vuoto o pieno
					nPosOr=Atc(" OR ", This.cWhereAdvanced)
					nPosAnd=Atc(" AND ", This.cWhereAdvanced)
					Do While nPosOr+nPosAnd>0
						If nPosOr<nPosAnd And nPosOr<>0 Or nPosAnd=0
							This.AddWhere(Alltrim(Left(This.cWhereAdvanced,nPosOr-1)))
							This.AddWhere("OR")
						Else
							This.AddWhere(Alltrim(Left(This.cWhereAdvanced, nPosAnd-1)))
						Endif
						This.cWhereAdvanced=Alltrim(Substr(This.cWhereAdvanced, Iif(nPosOr<nPosAnd And nPosOr<>0 Or nPosAnd=0, nPosOr+3, nPosAnd+4)))
						nPosOr=Atc(" OR ", This.cWhereAdvanced)
						nPosAnd=Atc(" AND ", This.cWhereAdvanced)
					Enddo
					This.AddWhere(This.cWhereAdvanced)
					This.RefreshParamWnd()
				Endif
			Endif

			If !Isnull(This.oCpQuery)
				For i=1 To This.oCpQuery.nFilterParam
					This.oCpQuery.i_FilterParam[i,6]=.F.
					This.oCpQuery.i_FilterParam[i,7]=.F.
				Next
				This.oCpQuery.bAskParam=.T.
			Endif
			*This.DeferredQuery()

		Else
			CP_MSG(cp_Translate(MSG_SELECTION_PARAMETERS_MISSING),.F.)
		Endif
	Endproc
	* --- Zucchetti aulla Inizio Monitor framework (aggiunto paramentro)
	Proc AskFile(pFileName)
		Local N,h,p,l
		* --- Zucchetti aulla Inizio Monitor framework
		If Vartype(pFileName)='C'
			N = pFileName
		Else
			If cp_isdeveloper()
				* --- Zucchetti aulla fine Monitor framework
				N=Getfile(Iif(Empty(This.cSymFile),'*',This.cSymFile)+'_VZM')
				* --- Zucchetti aulla Inizio Monitor framework
			Else
				Local old_default_path
				old_default_path = Alltrim(Sys(5))+Alltrim(Sys(2003))
				Set Default To Alltrim(old_default_path)+"\custom\userdir"
				N=Getfile("*_VZM")
				Set Default To Alltrim(old_default_path)
			Endif
		Endif
		* --- Zucchetti aulla Fine Monitor framework
		If cp_IsPFile(m.N)
			p=cp_GetPFile(m.N)
			If Not(Empty(m.p))
				xcg=Createobject('pxcg','load',m.p)
				This.Serialize(xcg)
				* --- Minimun path in luogo di path assoluto in caricamento configurazione
				* --- Caricando una configurazione, nello zoom Elenco o in una maschera con zoom integrato, nella casella 'Name' del tab 'File' era memorizzato
				* --- il path assoluto della configurazione in luogo di quello relativo, inoltre tale path era 'troncato' se conteneva un punto.
				This.cZoomName=Left(N,At('.',m.N)-1)
				This.cZoomName=Sys(2014,This.cZoomName) && prendo la minimum path
				If This.bAdvanced
					This.advpage.page5.SaveAs.Value=This.cZoomName
				Endif
				This.bFillFields=.F.
				This.Query(,,,,.T.)
			Endif
		Endif
	Proc Serialize(xcg)
		Local i,N,i_gx,i_gy,i_gw,i_gh,i_oc,N,i_fw,i_fh,i_nf
		If xcg.bIsStoring
			This.SaveColumnStatus()
			* --- ora puo' salvare
			xcg.Save('C',This.cSymFile)
			xcg.Save('N',This.nFields)
			For i=1 To This.nFields
				xcg.Save('C',This.cFields[i])
			Next
			xcg.Save('N',This.nWhere)
			For i=1 To This.nWhere
				xcg.Save('C',Trim(This.cWhere[i]))
			Next
			*** Controllo campi che sono definiti in order by nello zoom e nella query
			*** Non possono essere salvati due volte e inoltre visto che vince sempre l'ordinamento della query vengono eliminati dallo zoom
			If Not Isnull(This.oCpQuery)
				Local i_bChnageOrderForQuery, i_cOrderByQuery, i_nPosition, k
				i_bChnageOrderForQuery = .F.
				If Type("This.oCpQuery.bNoBindVariable")='L'
					This.oCpQuery.bNoBindVariable=.F.
				Endif
				With This.oCpQuery
					.nOrderBy=Alen(.i_OrderBy, 1)
					If .nOrderBy>0
						i=.nOrderBy
						Do While i>0
							If Type("This.oCpQuery.i_OrderBy[i,1]")="C"
								i_cOrderByQuery=.i_OrderBy[i,1]
								* --- vado alla ricerca l'alias....
								k=Ascan(.i_Fields,i_cOrderByQuery,-1,-1,1,15)
								If k>0
									i_cOrderByQuery=.i_Fields[k,2]
								Endif
								i_nPosition=Ascan(This.cOrderBy,i_cOrderByQuery,-1,-1,1,15)
								If i_nPosition=0
									*** vuol dire che il campo ordinato nella query � stato eliminato dallo zoom
									i_bChnageOrderForQuery = .T.
								Else
									Do While i_nPosition>0
										This.nOrderBy = This.nOrderBy - 1
										Adel(This.cOrderBy, i_nPosition)
										If i<>i_nPosition
											i_bChnageOrderForQuery = .T.
										Endif
										i_nPosition=Ascan(This.cOrderBy,i_cOrderByQuery,-1,-1,1,15)
									Enddo
								Endif
							Endif
							i = i - 1
						Enddo
						If i_bChnageOrderForQuery
							cp_ErrorMsg(MSG_ZOOM_ORDER_QUERY_NOT_RESPECTED)
						Endif
					Endif
					.nOrderBy=0
				Endwith
			Endif
			***
			xcg.Save('N',This.nOrderBy)
			For i=1 To This.nOrderBy
				xcg.Save('C',This.cOrderBy[i])
			Next
			xcg.Save('C',This.cKeyFields)
			xcg.Save('N',This.nColPositions)
			For i=1 To This.nColPositions
				xcg.Save('N',This.nColWidth[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('N',This.nColOrd[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('C',This.cColTitle[i])
			Next
			xcg.Save('L',This.bAskParam)
			xcg.Save('N',This.nOrderBy)
			For i=1 To This.nOrderBy
				xcg.Save('L',This.bOrderDesc[i])
			Next
			xcg.Save('C',This.cCpQueryName)
			For i=1 To This.nColPositions
				xcg.Save('C',This.cColFormat[i])
			Next
			xcg.Save('N',This.grd.Top)
			xcg.Save('N',This.grd.Left)
			xcg.Save('N',This.nGrdH)
			xcg.Save('N',This.nGrdW)
			For i=1 To This.nColPositions
				xcg.Save('N',This.nColX[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('N',This.nColY[i])
			Next
			xcg.Save('C',This.cSymFile)
			xcg.Save('L',This.bKeepSize)
			xcg.Save('N',Thisform.Width)
			xcg.Save('N',Thisform.Height)
			For i=1 To This.nColPositions
				xcg.Save('N',This.nColH[i])
			Next
			xcg.Save('L',This.bModSQL)
			xcg.Save('C',Iif(This.bModSQL,This.cSQL,''))
			For i=1 To This.nColPositions
				xcg.Save('C',This.cForeColor[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('C',This.cBackColor[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('N',This.nColRW[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('C',This.cHyperLink[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('L',This.bIsImage[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('C',This.cLayerContent[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('N',This.nColumnType[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('C',This.cHyperLinkTarget[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('C',This.cTooltip[i])
			Next
			xcg.Save('N',This.grd.LockColumns-Iif(i_AdvancedHeaderZoom And This.bAddedCheckBox,1,0))
			* --- gestione font carattere testata			
			For i=1 To This.nColPositions
				xcg.Save('C',This.cLabelFont[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('N',This.nDimFont[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('L',This.bItalicFont[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('L',This.bBoldFont[i])
			Next
			* --- verifico se ho modificato altezza header e altezza row per salvarle nello zoom
			If Not This.bDefaultHeightGrid And This.grd.HeaderHeight<>i_nHeaderHeight
				xcg.Save('N',This.grd.HeaderHeight)
			Else
				xcg.Save('N',0)
			Endif
			If Not This.bDefaultHeightGrid And This.grd.RowHeight<>i_nRowHeight
				xcg.Save('N',This.grd.RowHeight)
			Else
				xcg.Save('N',0)
			Endif
			* --- gestione font carattere colonna			
			For i=1 To This.nColPositions
				xcg.Save('C',This.cLabelFontC[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('N',This.nDimFontC[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('L',This.bItalicFontC[i])
			Next
			For i=1 To This.nColPositions
				xcg.Save('L',This.bBoldFontC[i])
			Next			
			* ---
			xcg.Eoc()
		Else
			*** setto una volta il memowidth per non rifarlo sempre
			oldmv=Set('MEMOWIDTH')
			Set Memowidth To 1024

			i_oc=This.nFields
			i_nf=xcg.Load('C','',.T.)
			If !(Trim(i_nf)==Trim(This.cSymFile))
				This.CloseFile()
				*this.cFile=i_nf
				This.OpenFile(i_nf)
			Endif
			This.nFields=xcg.Load('N',0,.T.)
			Dimension This.cFields[max(1,this.nFields)]
			For i=1 To This.nFields
				This.cFields[i]=xcg.Load('C','',.T.)
			Next
			This.nWhere=xcg.Load('N',0,.T.)
			Dimension This.cWhere[max(1,this.nWhere)]
			For i=1 To This.nWhere
				This.cWhere[i]=xcg.Load('C','',.T.)
			Next
			This.nOrderBy=xcg.Load('N',0,.T.)
			This.nOrderByDefault=This.nOrderBy
			Dimension This.cOrderBy[max(1,this.nOrderBy)]
			Dimension This.cOrderByDefault[max(1,this.nOrderBy)]
			For i=1 To This.nOrderBy
				This.cOrderBy[i]=xcg.Load('C','',.T.)
				This.cOrderByDefault[i]=This.cOrderBy[i]
			Next
			This.cKeyFields=xcg.Load('C','',.T.)
			This.nColPositions=xcg.Load('N',0,.T.)
			Dimension This.nColWidth[max(1,this.nColPositions)]
			Dimension This.nColOrd[max(1,this.nColPositions)]
			Dimension This.cColTitle[max(1,this.nColPositions)]
			Dimension This.cColFormat[max(1,this.nColPositions)]
			Dimension This.nColRW[max(1,this.nColPositions)]
			Dimension This.nColX[max(1,this.nColPositions)]
			Dimension This.nColY[max(1,this.nColPositions)]
			Dimension This.nColH[max(1,this.nColPositions)]
			Dimension This.cForeColor[max(1,this.nColPositions)]
			Dimension This.cBackColor[max(1,this.nColPositions)]
			Dimension This.cHyperLink[max(1,this.nColPositions)]
			Dimension This.cHyperLinkTarget[max(1,this.nColPositions)]
			Dimension This.bIsImage[max(1,this.nColPositions)]
			Dimension This.cLayerContent[max(1,this.nColPositions)]
			Dimension This.nColumnType[max(1,this.nColPositions)]
			Dimension This.cTooltip[max(1,this.nColPositions)]
			Dimension This.cLabelFont[max(1,this.nColPositions)]
			Dimension This.nDimFont[max(1,this.nColPositions)]
			Dimension This.bItalicFont[max(1,this.nColPositions)]
			Dimension This.bBoldFont[max(1,this.nColPositions)]
			Dimension This.cLabelFontC[max(1,this.nColPositions)]
			Dimension This.nDimFontC[max(1,this.nColPositions)]
			Dimension This.bItalicFontC[max(1,this.nColPositions)]
			Dimension This.bBoldFontC[max(1,this.nColPositions)]			
			For i=1 To This.nColPositions
				This.nColWidth[i]=xcg.Load('N',0,.T.)
			Next
			For i=1 To This.nColPositions
				This.nColOrd[i]=xcg.Load('N',0,.T.)
			Next
			For i=1 To This.nColPositions
				This.cColTitle[i]=xcg.Load('C','',.T.)
			Next
			This.bAskParam=xcg.Load('L',.F.,.T.)
			This.nOrderBy=xcg.Load('N',0,.T.)
			This.nOrderByDefault=This.nOrderBy
			Dimension This.bOrderDesc[max(1,this.nOrderBy)]
			Dimension This.bOrderDescDefault[max(1,this.nOrderBy)]
			For i=1 To This.nOrderBy
				This.bOrderDesc[i]=xcg.Load('L',.F.,.T.)
				This.bOrderDescDefault[i]=This.bOrderDesc[i]
			Next
			This.cCpQueryName=xcg.Load('C','',.T.)
			For i=1 To This.nColPositions
				This.cColFormat[i]=xcg.Load('C','',.T.)
			Next
			i_gy=xcg.Load('N',0,.T.)
			i_gx=xcg.Load('N',0,.T.)
			i_gh=xcg.Load('N',0,.T.)
			i_gw=xcg.Load('N',0,.T.)
			If i_gw<>0 And i_gh<>0
				This.grd.Top=i_gy
				This.grd.Left=i_gx
				This.nGrdH=i_gh
				This.nGrdW=i_gw
				This.Resize()
			Endif
			For i=1 To This.nColPositions
				This.nColX[i]=xcg.Load('N',0,.T.)
			Next
			For i=1 To This.nColPositions
				This.nColY[i]=xcg.Load('N',0,.T.)
			Next
			This.cSymFile=xcg.Load('C',This.cSymFile,.T.)
			This.bKeepSize=xcg.Load('L',.F.,.T.)
			i_fw=xcg.Load('N',0,.T.)
			i_fh=xcg.Load('N',0,.T.)
			For i=1 To This.nColPositions
				This.nColH[i]=xcg.Load('N',0,.T.)
			Next
			This.bModSQL=xcg.Load('L',.F.,.T.)
			This.cSQL=xcg.Load('C','',.T.)
			For i=1 To This.nColPositions
				This.cForeColor[i]=xcg.Load('C','',.T.)
			Next
			For i=1 To This.nColPositions
				This.cBackColor[i]=xcg.Load('C','',.T.)
			Next
			For i=1 To This.nColPositions
				This.nColRW[i]=xcg.Load('N',0,.T.)
			Next
			For i=1 To This.nColPositions
				This.cHyperLink[i]=xcg.Load('C','',.T.)
			Next
			For i=1 To This.nColPositions
				This.bIsImage[i]=xcg.Load('L',.F.,.T.)
			Next
			For i=1 To This.nColPositions
				This.cLayerContent[i]=xcg.Load('C','',.T.)
			Next
			For i=1 To This.nColPositions
				This.nColumnType[i]=xcg.Load('N',0,.T.)
			Next
			For i=1 To This.nColPositions
				This.cHyperLinkTarget[i]=xcg.Load('C','',.T.)
			Next
			For i=1 To This.nColPositions
				This.cTooltip[i]=xcg.Load('C','',.T.)
			Next
			This.nLockColumns=xcg.Load('N',0,.T.)+Iif(i_AdvancedHeaderZoom And This.bAddedCheckBox,1,0)
			* --- gestione font carattere di testata
			For i=1 To This.nColPositions
				This.cLabelFont[i]=xcg.Load('C','',.T.)
			Next
			For i=1 To This.nColPositions
				This.nDimFont[i]=xcg.Load('N',0,.T.)
			Next
			For i=1 To This.nColPositions
				This.bItalicFont[i]=xcg.Load('L',.F.,.T.)
			Next
			For i=1 To This.nColPositions
				This.bBoldFont[i]=xcg.Load('L',.F.,.T.)
			Next
			* --- verifico se ho altezza header e altezza row specifiche per lo zoom
			Local nHeaderHeight, nRowHeight
			This.bDefaultHeightGrid=.T.
			nHeaderHeight=xcg.Load('N',0,.T.)
			If nHeaderHeight<>0
				This.grd.HeaderHeight=nHeaderHeight
				This.bDefaultHeightGrid=.F.
			Endif
			nRowHeight=xcg.Load('N',0,.T.)
			If nRowHeight<>0
				This.grd.RowHeight=nRowHeight
				This.bDefaultHeightGrid=.F.
			Endif
			* --- gestione font carattere di colonna
			For i=1 To This.nColPositions
				This.cLabelFontC[i]=xcg.Load('C','',.T.)
			Next
			For i=1 To This.nColPositions
				This.nDimFontC[i]=xcg.Load('N',0,.T.)
			Next
			For i=1 To This.nColPositions
				This.bItalicFontC[i]=xcg.Load('L',.F.,.T.)
			Next
			For i=1 To This.nColPositions
				This.bBoldFontC[i]=xcg.Load('L',.F.,.T.)
			Next
			* ---
			xcg.Eoc()
			* --- altre inizializzazioni
			This.lbl.Caption=''
			This.nWhereAtLoad=This.nWhere
			ACOPY(this.cwhere ,this.cWhereAtLoad)
			If This.bAskParam
				If (This.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And i_ServerConn[1,2]<>0
					This.AskAdvancedParameters()
				Else
					This.AskParameters()
				Endif
			Endif
			This.NewExtQuery(This.cCpQueryName,.T.)
			For i=This.nFields+1 To i_oc
				N=Alltrim(Str(i))
				If Type("this.column"+N)='O'
					This.RemoveObject('column'+N)
					This.RemoveObject('columnlabel'+N)
				Endif
			Next
			This.RefreshParamWnd()
			If This.bKeepSize And This.Resizable
				If Vartype(Thisform.nPageResizeContainer)='N'
					Thisform.nPageResizeContainer=Thisform.oPgFrm.ActivePage
				Endif
				Thisform.Move(Thisform.Left, Thisform.Top, i_fw, i_fh)
				This.Resize()
				If Vartype(Thisform.nPageResizeContainer)='N'
					Thisform.nPageResizeContainer=0
				Endif
			Endif
		Endif
	Proc NewExtQuery(cQueryName,bDontFill)
		Local i_oLoad
		*** Zuccehtti Aulla - gestione query gi� caricate
		If Empty(cQueryName) Or !cp_IsPFile(cQueryName+'.VQR')
			This.oCpQuery=.Null.
			cQueryName=''
			This.bFillFields=!bDontFill
		Else
			This.oCpQuery=Createobject('cpquery')
			i_oLoad=Createobject('cpqueryloader')
			i_oLoad.LoadQuery(cQueryName,This.oCpQuery,This.oParentObject)
		Endif
		This.cCpQueryName=cQueryName
		Return
	Func toodbc(i_cmd)
		Local i_pos
		i_pos=At('{^',i_cmd)
		If i_pos<>0 And Substr(i_cmd+Space(12),i_pos+12,1)='}'
			* --- e' un tipo data nel formato VFP6, bisogna gestirlo in ODBC
			i_date=Substr(i_cmd,i_pos,13)
			i_newdate=Dtos(&i_date)
			i_newdate="{d '"+Left(i_newdate,4)+"-"+Substr(i_newdate,5,2)+"-"+Substr(i_newdate,7,2)+"'}"
			i_cmd=Left(i_cmd,i_pos-1)+i_newdate
		Else
			i_pos=At('{',i_cmd)
			If i_pos<>0 And (Substr(i_cmd+Space(9),i_pos+9,1)='}' Or Substr(i_cmd+Space(11),i_pos+11,1)='}')
				* --- e' un tipo data, bisogna gestirlo in ODBC
				i_date=Substr(i_cmd,i_pos+1,Iif(Substr(i_cmd+Space(9),i_pos+9,1)='}',8,11))
				If Empty(Ctod(i_date))
					i_newdate="null"
				Else
					i_newdate=Dtos(Ctod(i_date))
					i_newdate="{d '"+Left(i_newdate,4)+"-"+Substr(i_newdate,5,2)+"-"+Substr(i_newdate,7,2)+"'}"
				Endif
				i_cmd=Left(i_cmd,i_pos-1)+i_newdate
			Endif
		Endif
		Return(i_cmd)
	Proc DragDrop(oSource,nX,nY)
		Local i,N
		* --- gestione delle shape che spostano la griglia
		Local nDecHeight
		m.nDecHeight = Iif(Type('Thisform.oDec')='O', Thisform.oDec.Height, 0)
		If Lower(oSource.Name)=='grdshp_nw'
			oSource.Drag(2)
			If This.grd.Left+This.grd.Width-20>nX
				This.grd.Width=This.grd.Width-nX+This.grd.Left
				This.grd.Left=nX
			Endif
			If This.grd.Top+This.grd.Height-20+m.nDecHeight>nY-This.nDY
				This.grd.Height=This.grd.Height-nY+This.nDY+This.grd.Top+m.nDecHeight
				This.grd.Top=nY-This.nDY-m.nDecHeight
			Endif
			This.grdshp_NW.Top=This.grd.Top-3
			This.grdshp_NW.Left=This.grd.Left-3
			Return
		Endif
		If Lower(oSource.Name)=='grdshp_se'
			oSource.Drag(2)
			If This.grd.Left+20<nX
				This.grd.Width=nX-This.grd.Left
			Endif
			If This.grd.Top+20+m.nDecHeight<nY-This.nDY And nY-This.nDY<This.adv.Top+m.nDecHeight
				This.grd.Height=nY-This.nDY-This.grd.Top-m.nDecHeight
			EndIf
			This.grdshp_SE.Top=This.grd.Top+This.grd.Height-9
			This.grdshp_SE.Left=This.grd.Left+This.grd.Width-9
			This.nGrdW=This.Width-This.grd.Width-This.grd.Left
			This.nGrdH=This.Height-This.grd.Height-This.grd.Top-This.advpage.Height
			Return
		Endif
		* --- gestione del drop di una colonna
		If oSource.Class=='Stdztextbox' And This.bAdvanced
			oSource.Drag(2)
			If This.nDY<>0
				nX=nX-3
				nY=nY-5
			Else
				nX=nX-2
				nY=nY-2
			Endif
			If Lower(oSource.Name)='text1'
				If NOT (This.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) AND cp_YesNo(MSG_PLACING_FIELD_OUT_OF_GRID_QP)
					* --- una colonna viene spostata nella parte fissa
					N=Val(Substr(oSource.Parent.Name,7))
					This.nColX[n]=Iif(nX<This.grd.Left+This.grd.Width,nX,This.grd.Left+This.grd.Width-nX)
					This.nColY[n]=Iif(nY-This.nDY<This.grd.Top+This.grd.Height,nY-This.nDY,This.grd.Top+This.grd.Height-nY+This.nDY)
					For i=N+1 To This.nColPositions
						This.nColOrd[i]=This.nColOrd[i]-1
					Next
					Createobject('zoom_deferred_gridfill',This)
				Endif
			Else
				* --- spostamento di un campo all' interno della parte fissa
				N=Val(Substr(oSource.Name,7))
				oSource.Left=nX
				oSource.Top=nY-This.nDY-m.nDecHeight
				This.nColX[n]=Iif(nX<This.grd.Left+This.grd.Width,nX,This.grd.Left+This.grd.Width-nX)
				This.nColY[n]=Iif(nY-This.nDY<This.grd.Top+This.grd.Height,nY-This.nDY,This.grd.Top+This.grd.Height-nY+This.nDY)
				N=Alltrim(Str(N))
				This.columnlabel&N..Top=nY-This.nDY+2-m.nDecHeight
				This.columnlabel&N..Left=nX-Len(This.columnlabel&N..Caption)*7-6
			Endif
		Endif
	Func GetGridHeaderClass(i)
		Return('stdzHeader')
	Func GetGridTextboxClass(i)
		Return('stdzTextbox')
	Func GetGridImageClass(i)
		Return('stdzImage')
	Proc RemoveUserFilters()
		This.nWhere=This.nWhereAtLoad
		ACOPY (this.cWhereAtLoad,this.cwhere)
		This.RefreshParamWnd()
		This.Query(This.o_initsel)
	Procedure morerows.Click()
		Local i_old,i_nrec,i_pos
		i_nrec=Reccount(This.Parent.cCursor)
		i_old=i_nZoomMaxRows
		i_nZoomMaxRows=i_nrec*2
		i_pos=Recno(This.Parent.cCursor)
		This.Parent.Requery()
		i_nZoomMaxRows=i_old
		Goto (i_pos) In (This.Parent.cCursor)
		This.Parent.grd.SetFocus()
	        *--- Notifica morerows event
	        If Type("This.Parent.Parent.oContained") = 'O'
		        This.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Name)+' morerows')		
  	        EndIf
		Return
	Proc RetriveAllRows()
		Local i_old
		If Type('i_bDisableAsyncConn')<>'U' And i_bDisableAsyncConn
			If This.morerows.Visible
				CP_MSG(cp_Translate(MSG_RETRIEVING_ALL_RECORDS_D))
				i_old=i_nZoomMaxRows
				i_nZoomMaxRows=-1
				This.Requery()
				i_nZoomMaxRows=i_old
				Wait Clear
			Endif
		Endif
		Return
	*--- Zucchetti Aulla - Inizio export su excel/calc
	Func ExcelCalcExport()
		Local L_OldPosition, L_oExcel, L_cTmpCurs
		L_cTmpCurs = SYS(2015)
		Select ( This.cCursor )
		L_OldPosition = RECNO()
		Go Bottom
		Go Top
		USE DBF() In 0 Again Alias (L_cTmpCurs)
		If g_OFFICE=='M'
			If Type('g_AUTOFFICE')='U' Or g_AUTOFFICE
				Local i_olderr, i_err
			CP_MSG( cp_Translate(MSG_BUILDING_EXCEL_WORKSHEET_D) )
				i_olderr=On('ERROR')
				On Error i_err=.T.
			L_oExcel=createobject('excel.application')
			ve_build( L_oExcel, L_cTmpCurs, "", .F., .F., "", This )
			L_oExcel = .NULL.
				On Error &i_olderr
			Else
				*--- Preparo la stringa per le intestazioni delle colonne
				*--- Esempio formato stringa: "ANTIPCON;Tipo conto;ANCODICE;Intestatario"
				Local cColTitles, cParseChar, cNameFile, bAutoDir
				m.cColTitles = ''
				m.cParseChar = ';'
				For Each oCol in This.grd.Columns
					If oCol.Visible And PemStatus(oCol,'hdr',5)
						m.cColTitles = m.cColTitles+Alltrim(oCol.ControlSource)+m.cParseChar+Alltrim(oCol.hdr.Caption)+m.cParseChar
					Endif
				Endfor
				*--- tolgo l'ultimo separatore
				m.cColTitles = Left(m.cColTitles,Len(m.cColTitles)-Len(cParseChar))
				m.cNameFile = Iif(Upper(This.Name)='AUTOZOOM', ThisForm.cComment, 'Export'+Sys(2015))
				m.bAutoDir = Type('g_NoAutOfficeDir')='C' And !Empty(g_NoAutOfficeDir) And Directory(g_NoAutOfficeDir)
				m.cNameFile = Iif(m.bAutoDir, Addbs(g_NoAutOfficeDir)+m.cNameFile, Putfile('Save as', Addbs(g_tempadhoc)+m.cNameFile, 'XLSX'))
				IF !EMPTY(m.cNameFile)
					vx_build('X', '',m.L_cTmpCurs, FORCEEXT (m.cNameFile,"XLSX"), .F., !m.bAutoDir, '', .F., .T., m.cColTitles, m.cParseChar,this)
				ENDIF
			EndIf
		Else
			CP_MSG( cp_Translate(MSG_CREATE_BS_MODIFY_SPREADSHEET) )
			vo_build( L_cTmpCurs, "", .F., .F., This )
		Endif
		Use in Select(L_cTmpCurs)
	Endfunc
	*--- Zucchetti Aulla - Fine export su excel/calc
Enddefine

Define Class timedmsg As Timer
	msg=''
	Proc Init(msg,WaitTime)
		This.msg=msg
		This.Interval=WaitTime*1000
	Proc Timer()
		This.Interval=0
		Wait Window This.msg Nowait
Enddefine

Define Class stdzGrid As Grid
	FontBold=.F.
	DeleteMark=.F.
	last2=0
	cFile=''
	Dimension xKey[1]
	HeaderHeight=i_nHeaderHeight
	RowHeight=i_nRowHeight
	HighlightStyle=2
	HighlightRowLineWidth=3
	SplitBar=Not i_AdvancedHeaderZoom
	nActivecolumn=0
	* --- procedure della griglia
	Proc AddGridColumn(i)
		Local N, objColCur
		N=Alltrim(Str(i))
		If left(This.Parent.cFields[i],17)='cp_MemoFirstLine('
			This.Parent.cFields[i]=substr(This.Parent.cFields[i],18,len(alltrim(This.Parent.cFields[i]))-18)
		Endif
		If Type("this.column"+N)='O'
			This.RemoveObject('column'+N)
		Endif
		This.AddObject('column'+N,'stdzColumn')
                objColCur = This.Column&N
		m.objColCur.ControlSource=This.Parent.cFields[i] &&field(i)
		If Type(This.Parent.cFields[i])='M'
			m.objColCur.ControlSource='cp_MemoFirstLine('+This.Parent.cFields[i]+')'
		Endif
		m.objColCur.FontBold=.F.
		m.objColCur.Visible=.T.
		*--- Descrizione colonne zoom
		*this.Column&n..Width=fsize(this.parent.cFields[i])*7+6
		*--- Descrizione colonne zoom
		m.objColCur.AddObject('hdr',This.Parent.GetGridHeaderClass(i))
		*--- Descrizione colonne zoom
		*--- Discrimino tra tabella e query
		If Isnull(This.Parent.oCpQuery)
			*--- Rileggo XDC
			Cp_ReadXdc()
			*--- Leggo da i_DCX la descrizione dei campi, come alias c'� il nome del campo
			Local nIndexDCX
			nIndexDCX = i_Dcx.Getfieldidx(This.Parent.cSymFile, This.Parent.cFields[i])
			If nIndexDCX > 0
				*--- Se la descrizione � vuota metto il nome del campo
				Local l_desc
				l_desc = cp_Translate(i_Dcx.getfielddescr(This.Parent.cSymFile, nIndexDCX))
				If !Empty(l_desc)
					m.objColCur.hdr.Caption = l_desc
				Else
					m.objColCur.hdr.Caption=This.Parent.cFields[i]
				Endif
			Else
				m.objColCur.hdr.Caption=This.Parent.cFields[i]
			Endif
		Else
			*--- Sulla query � memorizzata la descrizione del campo, utilizzo l'alias per recuperare la descrizione
			Local nIndexAlias
			#If Version(5)>=900
				nIndexAlias = Ascan(This.Parent.oCpQuery.i_Fields, This.Parent.cFields[i],1,0,2,8)
			#Else
				Local cSetExact
				cSetExact=Set("EXACT")
				Set Exact On
				nIndexAlias=Ascan(This.Parent.oCpQuery.i_Fields, This.Parent.cFields[i],1)
				nIndexAlias=Iif(nIndexAlias>0, Asubscript(This.Parent.oCpQuery.i_Fields, nIndexAlias,1) , nIndexAlias)
				Set Exact &cSetExact
			#Endif
			If nIndexAlias > 0
				m.objColCur.hdr.Caption = cp_Translate(This.Parent.oCpQuery.i_Fields[nIndexAlias,6])
			Else
				*--- Rileggo XDC
				Cp_ReadXdc()
				*--- Leggo da i_DCX la descrizione dei campi, come alias c'� il nome del campo
				Local nIndexDCX
				nIndexDCX = i_Dcx.Getfieldidx(This.Parent.cSymFile, This.Parent.cFields[i])
				If nIndexDCX > 0
					m.objColCur.hdr.Caption = cp_Translate(i_Dcx.getfielddescr(This.Parent.cSymFile, nIndexDCX))
				Else
					m.objColCur.hdr.Caption=This.Parent.cFields[i]
				Endif
			Endif
		Endif
		*    this.Column&n..hdr.caption=this.parent.cFields[i]
		*--- Ricalcolo la lunghezza in base alla descrizione, tipo, valore campo
		Local l_DesWidth, l_FldWidth, l_cGrdFontName, l_cLblFontName, l_nGrdFontSize, l_nLblFontSize
		l_nGrdFontSize = i_nGrdFontSize
		l_nLblFontSize = i_nLblFontSize
		l_cGrdFontName = i_cGrdFontName
		l_cLblFontName = i_cLblFontName
		If i<=This.Parent.nColPositions
			* --- gestione font caratteri della testata		
			m.objColCur.hdr.cLabelFont=This.Parent.cLabelFont[i]
			m.objColCur.hdr.nDimFont=This.Parent.nDimFont[i]
			m.objColCur.hdr.bItalicFont=This.Parent.bItalicFont[i]
			m.objColCur.hdr.bBoldFont=This.Parent.bBoldFont[i]
			m.objColCur.hdr.SetFont()
			If Not Empty(m.objColCur.hdr.cLabelFont)
				l_nLblFontSize=m.objColCur.hdr.nDimFont
				l_cLblFontName=m.objColCur.hdr.cLabelFont
			Endif
			* --- gestione font caratteri della colonna
			m.objColCur.cLabelFontC=This.Parent.cLabelFontC[i]
			m.objColCur.nDimFontC=This.Parent.nDimFontC[i]
			m.objColCur.bItalicFontC=This.Parent.bItalicFontC[i]
			m.objColCur.bBoldFontC=This.Parent.bBoldFontC[i]
			m.objColCur.SetColumnFont()
		Else
			m.objColCur.hdr.SetFont()
			m.objColCur.SetColumnFont()			
		Endif
		l_DesWidth = cp_LabelWidth2(m.objColCur.hdr.Caption, l_cGrdFontName, l_nGrdFontSize) + 6
		l_FldWidth = cp_LabelWidth2(Replicate('W',Fsize(This.Parent.cFields[i])), l_cLblFontName, l_nLblFontSize) + 3
		m.objColCur.Width=Max(l_DesWidth, l_FldWidth)
		*--- Descrizione colonne zoom
		m.objColCur.RemoveObject('text1')
		*--- Campo di tipo immagine nello zoom
		If i<=This.Parent.nColPositions
			m.objColCur.hdr.bIsImage=This.Parent.bIsImage[i]
		Else
			m.objColCur.hdr.bIsImage=.F.
		Endif
		If Type(This.Parent.cFields[i])="C" And m.objColCur.hdr.bIsImage
			m.objColCur.Bound = .F.
			m.objColCur.Sparse = .F.
			m.objColCur.AddObject('text1', 'stdzimage')
			m.objColCur.CurrentControl='text1'
			&& m.objColCur.AddObject('Img'+N, this.parent.GetGridImageClass(i))
			m.objColCur.text1.cPathImage = This.Parent.cFields[i]+"_ZIMG"
			&& m.objColCur.AddObject('Img'+N, 'stdzimage')
			m.objColCur.text1.Visible=.T.
			m.objColCur.text1.BackStyle=0
			&&		m.objColCur.AddObject('text1',this.parent.GetGridImageClass(i))
		ELSE
			m.objColCur.AddObject('text1',This.Parent.GetGridTextboxClass(i))
		Endif
		If i<=This.Parent.nColPositions And This.Parent.nColX[i]=0
			*--- Descrizione colonne zoom
			If Bitand(This.Parent.nDesFlags, 2) != 2
				This.Parent.nColWidth[i]=iif(type("This.Parent.nColWidth[i]")<>'N', 0, This.Parent.nColWidth[i])
				m.objColCur.Width=This.Parent.nColWidth[i]
			Endif
			*--- Se di tipo date/datetime riduco la dimensione per non vedere gli 0 dell'ora
			If m.objColCur.Width>5 And Type(This.Parent.cFields[i])="T" And This.getinputmask(This.Parent.cColFormat[i])<>i_cDateTimeMask
				Local l_DateWidth
				l_DateWidth = cp_LabelWidth2(Dtoc({^8888-08-30}), l_cGrdFontName, l_nGrdFontSize) + 3
				m.objColCur.Width = l_DateWidth
			Endif
			*--- Descrizione colonne zoom
			This.Parent.nColOrd[i]=iif(type("This.Parent.nColOrd[i]")<>'N', 0,This.Parent.nColOrd[i])
            m.objColCur.ColumnOrder=This.Parent.nColOrd[i]
			*--- Descrizione colonne zoom
			If Bitand(This.Parent.nDesFlags, 1) != 1
				m.objColCur.hdr.Caption=cp_Translate(Alltrim(Iif(Vartype(This.Parent.cColTitle[i])<>"C","",This.Parent.cColTitle[i])))
			Endif
			*--- Descrizione colonne zoom
			m.objColCur.hdr.Alignment=6
			This.Parent.cColFormat[i]=iif(TYPE("This.Parent.cColFormat[i]")<>'C', '', This.Parent.cColFormat[i])
			m.objColCur.InputMask=This.getinputmask(This.Parent.cColFormat[i])
			m.objColCur.Tag=This.Parent.cColFormat[i]
			m.objColCur.rw=This.Parent.nColRW[i]
			If This.Parent.bReadOnly
				m.objColCur.ReadOnly=.T.
			Else
				m.objColCur.ReadOnly=(Vartype(This.Parent.nColRW[i])<>'N' Or This.Parent.nColRW[i]=0)
			Endif
			m.objColCur.hdr.nHeight=This.Parent.nColH[i]
			This.Parent.cForeColor[i]=iif(type("This.Parent.cForeColor[i]")<>'C', '', This.Parent.cForeColor[i])
			This.Parent.cBackColor[i]=iif(type("This.Parent.cBackColor[i]")<>'C', '', This.Parent.cBackColor[i])
			m.objColCur.DynamicForeColor=Trim(This.Parent.cForeColor[i])
			m.objColCur.DynamicBackColor=Trim(This.Parent.cBackColor[i])
			This.Parent.cHyperLink[i]=iif(type("This.Parent.cHyperLink[i]")<>'C', '', This.Parent.cHyperLink[i])
			This.Parent.cHyperLinkTarget[i]=iif(type("This.Parent.cHyperLinkTarget[i]")<>'C', '', This.Parent.cHyperLinkTarget[i])
			m.objColCur.hdr.cHyperLink=This.Parent.cHyperLink[i]
			m.objColCur.hdr.cHyperLinkTarget=This.Parent.cHyperLinkTarget[i]
			m.objColCur.hdr.cLayerContent=This.Parent.cLayerContent[i]
			m.objColCur.hdr.nColumnType=This.Parent.nColumnType[i]
			m.objColCur.hdr.cTooltip=This.Parent.cTooltip[i]
		Endif
		Return
	Proc AddFixedColumn(i)
		Local N,T, objFld, objLbl
		N=Alltrim(Str(i))
		This.Parent.AddObject('column'+N,'stdztextbox')
		objFld = This.Parent.Column&N
		*--- Set Font
		Local l_oldFontName
		If Empty(This.Parent.cLabelFontC[m.i])
                        l_oldFontName = objFld.FontName
			If Thisform.bGlobalFont
                                SetFont('G', objFld)
			Else
				objFld.FontName = SetFontName('G', Thisform.FontName, Thisform.bGlobalFont)
				objFld.FontSize = SetFontSize('G', Thisform.FontSize, Thisform.bGlobalFont)
				objFld.FontItalic = SetFontItalic('G', Thisform.FontItalic, Thisform.bGlobalFont)
				objFld.FontBold = SetFontBold('G', Thisform.FontBold, Thisform.bGlobalFont)
			Endif
		Else
			objFld.FontName = This.Parent.cLabelFontC[m.i]
			objFld.FontSize = This.Parent.nDimFontC[m.i]
			objFld.FontItalic = This.Parent.bItalicFontC[m.i]
			objFld.FontBold = This.Parent.bBoldFontC[m.i]	
		Endif		
		*---
		objFld.Visible=.T.
		objFld.Top=Iif(This.Parent.nColY[i]>0,This.Parent.nColY[i],This.Top+This.Height-This.Parent.nColY[i])
		objFld.Left=Iif(This.Parent.nColX[i]>0,This.Parent.nColX[i],This.Left+This.Width-This.Parent.nColX[i])
		objFld.Width=This.Parent.nColWidth[i]+6
		If This.Parent.nColH[i]<>0
			objFld.Height=This.Parent.nColH[i]
		Endif
		Local cMask
		cMask=This.getinputmask(This.Parent.cColFormat[i])
		objFld.InputMask=cp_GetMask('"'+cMask+'"')
		objFld.Format=cp_GetFormat('"'+cMask+'"')
		objFld.Tag=This.Parent.cColFormat[i]
		objFld.ControlSource=This.Parent.cFields[i]
		
		This.Parent.AddObject('columnlabel'+N,'stdzlabel')
		objLbl = This.Parent.columnlabel&N
                *--- Set Font
                Local l_oldFontName
                If Empty(This.Parent.cLabelFont[m.i])
                    l_oldFontName = objLbl.FontName
                    If Thisform.bGlobalFont
                        SetFont('G', objLbl )
                    Else
                        objLbl.FontName = SetFontName('G', Thisform.FontName, Thisform.bGlobalFont)
                        objLbl.FontSize = SetFontSize('G', Thisform.FontSize, Thisform.bGlobalFont)
                        objLbl.FontItalic = SetFontItalic('G', Thisform.FontItalic, Thisform.bGlobalFont)
                        objLbl.FontBold = SetFontBold('G', Thisform.FontBold, Thisform.bGlobalFont)
                    Endif
                Else
                    objLbl.FontName = This.Parent.cLabelFont[m.i]
                    objLbl.FontSize = This.Parent.nDimFont[m.i]
                    objLbl.FontItalic = This.Parent.bItalicFont[m.i]
                    objLbl.FontBold = This.Parent.bBoldFont[m.i]
                Endif
		objLbl.Visible=.T.
		T=Trim(This.Parent.cColTitle[i])
		If !Empty(T)
			T=T+':'
		Endif
		objLbl.Caption=T
		objLbl.Left=This.Parent.Column&N..Left-Len(T)*7-6
		objLbl.Top=This.Parent.Column&N..Top+2
		objLbl.Width=Len(T)*7+6
		objLbl.Alignment=1
		Return
	Proc RightClick
		If Lower(This.column1.text1.Class)='stdzimage'
			If Lower(This.column2.text1.Class)<>'stdzimage'
				This.column2.text1.dorightclick()
			Endif
		Else
			This.column1.text1.dorightclick()
		Endif
		Return
	Proc Fill(i_bLockScreen)
		Local i,N
		If i_bLockScreen
			Thisform.LockScreen=.T.
		Endif
		This.RecordSource=This.Parent.cCursor
		For i=1 To This.Parent.nFields  && possono esserci dei campi aggiuntivi da non mostrare
			* --- aggiunge una colonna per ogni campo
			If ALEN(This.Parent.nColX)>=i
				This.Parent.nColX[i]=iif(type("This.Parent.nColX[i]")<>'N', 0, This.Parent.nColX[i])
			Endif
			If i>This.Parent.nColPositions Or This.Parent.nColX[i]=0
				This.AddGridColumn(i)
			Endif
		Next
		* --- setta le proprieta' delle colonne
		For i=1 To This.Parent.nColPositions
			N=Alltrim(Str(i))
			If This.Parent.nColX[i]=0
				If Type("this.parent.column"+N)='O'
					This.Parent.RemoveObject('column'+N)
					This.Parent.RemoveObject('columnlabel'+N)
				Endif
				*this.Column&n..width=this.parent.nColWidth[i]
				This.Column&N..ColumnOrder=This.Parent.nColOrd[i]
				*this.Column&n..hdr.Caption=this.parent.cColTitle[i]
				*this.Column&n..inputmask=this.parent.cColFormat[i]
			Else
				If Type("this.parent.column"+N)<>'O'
					This.AddFixedColumn(i)
				Endif
			Endif
		Next
		If Empty(This.Parent.lbl.Caption)
			N=1000
			For i=1 To This.ColumnCount
				If This.Columns(i).ColumnOrder<=N
					N=This.Columns(i).ColumnOrder
					If This.Columns(i).ControlSource<>"xchk"
						This.Parent.SetWhereCaption(This.Columns(i).ControlSource,This.Columns(i).hdr.Caption)
					Endif
				Endif
			Next
		Endif
		This.LockColumns=Iif(Vartype(This.Parent.nLockColumns)='N',This.Parent.nLockColumns,0)
		If i_bLockScreen
			Thisform.LockScreen=.F.
			This.Refresh()
		Endif
		Return
	Proc fillagain()
		Local i,N
		LOCAL TestMacro
		*--- Eseguo la select su cCursor, altrimenti l'assegnamento successivo genera un errore (Error 52)
		*--- nel caso in cui si faccia due volte filtro e non sono presenti record
		Select(This.Parent.cCursor)
		This.RecordSource=This.Parent.cCursor
		For i=1 To This.Parent.nColPositions  && possono esserci dei campi aggiuntivi da non mostrare
			If This.Parent.nColX[i]=0 And i<=This.Parent.nFields
				N=Alltrim(Str(i))
				TestMacro=This.Column&N..Width
				If TestMacro=0
					This.Column&N..Width=This.Parent.nColWidth[i]
				Endif
				This.Column&N..ControlSource=This.Parent.cFields[i]
				If Type(This.Parent.cFields[i])='M'
					This.Column&N..ControlSource='cp_MemoFirstLine('+This.Parent.cFields[i]+')'
				Endif
			Endif
		Next
		Return
	Proc MouseUp(btn,sh,x,Y)
		If This.last2<>0 And Used()
			Local i_olderr
			i_olderr=On('ERROR')
			On Error=.T.
			Go (This.last2)
			This.last2=0
			On Error &i_olderr
		ENDIF
		
	Proc Scrolled(nDir)
		If Used(This.parent.cCursor)
			* L' ERRORE INIZIA CON ACTIVEROW=0
			This.last2=Recno(This.parent.cCursor)
		ENDIF
		Local i_olderr
		i_olderr=On('ERROR')
		On Error =.T.
		
		This.Parent.Parent.Parent.Parent.Parent.NotifyEvent('w_'+Lower(This.Parent.Name)+' scrolled')
		* ---- Aggiunto per gestione Paint ed elenchi (effetto fastidioso sulla scroll bar della griglia che sale e scende)
		thisform._bPaint_ErrorGrid=.t.    
		*--- Introdotti Refresh per evitare problemi di duplicazione delle righe
		If m.nDir<>2 And m.nDir<>3 And Recno()>1 AND This.RelativeRow<>0
			This.Refresh()
		ENDIF
		On Error &i_olderr
		
	Proc AfterRowColChange(nIdx)
		Local i,N
		IF (This.Parent.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom)
			Thisform.LockScreen=.T.
		Endif
		For i=1 To This.Parent.nColPositions
			If This.Parent.nColX[i]<>0
				N=Alltrim(Str(i))
				If Type("this.parent.Column"+N)='O'
					x=This.Parent.Column&N..ControlSource
					This.Parent.Column&N..Value=&x
				Endif
			Endif
		Next
		This.nActivecolumn=This.ActiveColumn
		If (This.Parent.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom)
			If This.RelativeRow<>0 AND Vartype(This.oRefHeaderObject)='O'
				This.oRefHeaderObject.Refresh()
			Endif
			If Vartype(This.oRefSearchFilter)='O'
				This.oRefSearchFilter.ecpQuit()
			Endif
			Thisform.LockScreen=.F.
		Endif
		Return

		* Zucchetti Aulla Inizio (non utilizzare la vartype in alcuni casi non riesce a gestire)
	Proc BeforeRowColChange(nIdx)
		Local i_notifyevent, Old_value, cOldArea, i_OldErr,cMacro, bErr, npidx
		i_notifyevent=.F.
		If (This.Parent.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom)
			Thisform.LockScreen=.T.
			If Vartype(Thisform.__UnLockScreen)='O'
				Thisform.__UnLockScreen.Enabled=.T.
			Endif
		Endif
		i_OldErr=ON('ERROR')
		ON ERROR bErr=.t.		
		DoDefault()
		If VarType('this.ColumnS')<>'U' And Type('this.ColumnS[NIDX-1].controlsource')='C' And Type('this.parent.parent.parent.parent.parent')='O'
			* --- Confronto valore sul text associato alla griglia con valore sul cursore...
			cOldArea=SELECT()
			SELECT (this.Controlsource)
			npidx=ASCAN(this.Parent.ncolord,nidx-1)
			cMacro=This.Columns[npidx].ControlSource
			Old_value=&cMacro
			If Type("this.parent.oParentObject.w_"+This.Columns[npidx].ControlSource)<>'U'
				cMacro="this.parent.oParentObject.w_"+This.Columns[npidx].ControlSource
				Old_value=&cMacro
			Endif
			Select(cOldArea)
			If This.Columns[npidx].text1.Value<>Old_value
				* nome zoom nome colonna BeforeRowColChanged
				i_notifyevent=.T.
			Endif
		Endif
		If i_notifyevent
			* nome zoom nome colonna BeforeRowColChanged
			This.Parent.Parent.Parent.Parent.Parent.NotifyEvent(Lower('w_'+This.Parent.Name+' '+This.Columns[npidx].ControlSource+' BeforeRowColchanged'))
		Endif
		On Error &i_olderr
		Return
		* Zucchetti Aulla Fine

	Proc DragDrop(oSource,nX,nY)
		If oSource.Class='Stdztextbox' And Lower(oSource.Name)<>'text1' And This.Parent.bAdvanced
			This.Parent.nColX[oSource.nIdx]=0
			This.Parent.nColY[oSource.nIdx]=0
			Createobject('zoom_deferred_gridfill',This.Parent)
			Return
		Endif
		This.Parent.DragDrop(oSource,nX,nY)
	Func getinputmask(cMask)
		If Empty(cMask)
			Return(cMask)
		Endif
		Local cLeft1
		cLeft1=Left(cMask,1)
		If cLeft1='9' Or cLeft1='@' Or cLeft1='X'
			Return(cMask)
		Endif
		Return(Eval(cMask))
	Proc Top_Assign(nValue)
		This.Top = Max(m.nValue,0)
Enddefine

Define Class zoom_deferred_gridfill As Timer
	* --- solito trucco per poter eseguire delle operazioni in ritardo
	Interval=200
	oThis=.Null.
	oObj=.Null.
	Proc Init(oObj)
		This.oObj=oObj
		This.oThis=This
	Proc Timer()
		This.oObj.adv.SetFocus()
		Select 0
		This.oObj.grd.ColumnCount=0
		This.oObj.grd.Fill(.T.)
		This.oObj.SaveColumnStatus()
		This.oThis=.Null.
Enddefine

Define Class stdzSrc As TextBox
	Proc Valid()
		Local w,p,cs
		* --- modifica l' espressione di where
		If This.Parent.bSrcUpdated And Used(This.Parent.cCursor)
			* --- Zucchetti Aulla - Inizio: Modifica perch� se inserisco un valore come '01-01-005'
			* --- SQLServer non riesce a gestire date cos� piccole
			If (Type('this.value')='D' Or Type('this.value')='T') And (This.Value<i_INIDAT Or This.Value>i_FINDAT)
				cp_ErrorMsg(MSG_NO_VALID_DATE)
				This.Value=Iif(This.Value<i_INIDAT,i_INIDAT,i_FINDAT)
			Endif
			* --- Zucchetti Aulla - Fine

			cs=This.Parent.cWhereCol
			If cs='cp_MemoFirstLine('
				cs=cp_ControlSourceMemo(cs)
			Endif
			Select (This.Parent.cCursor)
			This.Parent.bSrcUpdated=.F.
			If !Empty(This.Value)
				w=This.Parent.BuildWhere(cs,This.Value)
				If Not(Empty(w))
					p=At(' ',w,2)
					If This.Parent.nWhere>0 And This.Parent.nWhere>This.Parent.nWhereAtLoad And Left(w,p)=Left(This.Parent.cWhere(This.Parent.nWhere),p)
						This.Parent.SetWhere(This.Parent.nWhere,w)
					Else
						This.Parent.AddWhere(w)
					Endif
					*this.value=cp_NullValue(this.parent.lbl.caption)
					This.Parent.Query(This.Parent.o_initsel)
				Endif
			Else
				This.Parent.RemoveUserFilters()
			Endif
		Endif
	Endproc
	Proc DragDrop(oSource, nXCoord, nYCoord)
		* --- se avviene il drop da un record visualizzato, lo pone nelle condizioni
		Local i_bAdd,cs,w,xx
		If oSource.Class='Stdztextbox'
			If oSource.bInGrid
				i_bAdd=This.Parent.cWhereCol<>oSource.Parent.ControlSource
				If oSource.Parent.ControlSource<>"xchk"
					This.Parent.SetWhereCaption(oSource.Parent.ControlSource,oSource.Parent.hdr.Caption)
				Endif
			Else
				i_bAdd=This.Parent.cWhereCol<>oSource.ControlSource
				If oSource.ControlSource<>"xchk"
					This.Parent.SetWhereCaption(oSource.ControlSource,This.Parent.cColTitle[oSource.nIdx])
				Endif
			Endif
			This.Value=Iif(Empty(oSource.SelText) Or Type("oSource.value")<>'C',oSource.Value,oSource.SelText)
			cs=This.Parent.cWhereCol
			If cs='cp_MemoFirstLine('
				cs=cp_ControlSourceMemo(cs)
			Endif
			If !i_bAdd And This.Parent.nWhere>0 And This.Parent.nWhere>This.Parent.nWhereAtLoad
				w=This.Parent.BuildWhere(cs,This.Value)
				xx=This.Parent.cWhereCol && altrimenti da errore!
				This.Parent.SetWhere(This.Parent.nWhere,w)
			Else
				w=This.Parent.BuildWhere(cs,This.Value)
				xx=This.Parent.cWhereCol && altrimenti da errore!
				This.Parent.AddWhere(w)
			Endif
			oSource.Drag(2)
			This.Parent.DeferredQuery()
		Endif
	Endproc
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		Do Case
			Case nState=0   && Enter
				This.Parent.cOldIcon=oSource.DragIcon
				If oSource.Class='Stdztextbox'
					oSource.DragIcon="cross01.cur"
				Endif
			Case nState=1   && Leave
				oSource.DragIcon=This.Parent.cOldIcon
		Endcase
	Endproc
	Proc InteractiveChange
		This.Parent.bSrcUpdated=.T.
	Endproc
Enddefine

Proc cp_ManageReport(oZoom,cOp,bPreview)
	Local i_rn,i_flds,i_dn,i_oDs, i_oWfile, i_oExcel
	* --- Aggiunta scelta di menu
	Define Pad Vista Of _Msysmenu Prompt cp_Translate(MSG_VIEW)
	On Pad Vista Of _Msysmenu Activate Popup _Msm_view
	* ---
	i_rn=Trim(oZoom.cZoomName)+'_'+Trim(oZoom.cSymFile)
	If Not(Empty(oZoom.cZoomName))
		* --- copia il cursore in __tmp__
		USE IN SELECT('__tmp__')
		If VARTYPE(g_OFFICE_TYPE_CONV) = 'C' AND g_OFFICE_TYPE_CONV $ "C-S"
			If !convcuraccentedchars(oZoom.cCursor, g_OFFICE_TYPE_CONV )
				cp_ErrorMsg(MSG_CURSOR_CHARS_CONVERSION_ERROR)
			EndIf
		Endif
		Select (oZoom.cCursor)
		Go Top
		If cOp='word'
			If Not(Eof())
				If Type('g_AUTOFFICE')<>'U' And !g_AUTOFFICE And cp_IsStdFile(i_rn,'docx')
					Local cNameFile, bAutoDir
					i_dn=cp_GetStdFile(i_rn,'docx')
					If At(':\',i_dn)=0
						i_dn=Sys(5)+Sys(2003)+'\'+i_dn+'.docx'
					EndIf
					m.bAutoDir = Type('g_NoAutOfficeDir')='C' And !Empty(g_NoAutOfficeDir) And Directory(g_NoAutOfficeDir)
					m.cNameFile = Iif(m.bAutoDir, Addbs(g_NoAutOfficeDir)+m.i_dn+Sys(2015), Putfile('Save as', Addbs(g_tempadhoc)+m.i_dn, 'DOCX'))
					vx_build('D', i_dn, oZoom.cCursor, m.cNameFile, .F., !m.bAutoDir)
				Else
				Local N,i_err,bOk
				i_err=On('ERROR')
				On Error bOk=.F.
					bOk=.T.
				*** Zucchetti Aulla - aggiunta possibilit� di fare esportazione per mail merge con excel
					N=Alltrim(Addbs(g_tempadhoc))+'__word__'
				Delete File (forceext(N,'xlsx'))
					If Type('g_AUTOFFICE')='U' Or g_AUTOFFICE
		        i_oExcel = Createobject("excel.application")
		        * --- Non lascia messaggi e non rende visibile il file
		        ve_build(i_oExcel,oZoom.cCursor, N,,,,,.t.,.t.)
		        i_oExcel.WorkBooks(1).SaveAs(FullPath(forceext(N, 'xlsx')))     
		        i_oExcel.Quit()     
		        i_oExcel = .Null.
				    Else
				    	vx_build('X', '', oZoom.cCursor, m.N, .F., .T., '', .T., .T.)
				    EndIf
				    If bOk
				w=Createobject('word.application')
				If bOk
					w.Visible=.T.
					If cp_IsStdFile(i_rn,'doc')
						i_dn=cp_GetStdFile(i_rn,'doc')
						If At(':\',i_dn)=0
							i_dn=Sys(5)+Sys(2003)+'\'+i_dn+'.doc'
						Endif
						i_oWfile = w.documents.Open(i_dn)
						i_oDs = i_oWfile.mailmerge.Datasource
						*** Zucchetti Aulla - aggiunta possibilit� di fare esportazione per mail merge con excel
						If Not(Upper(i_oDs.Name)==Upper(forceext(N,'xlsx')))
							i_oWfile.mailmerge.opendatasource(forceext(N,'xlsx'))
							i_oWfile.SaveAs(i_dn)
						else
							cp_errormsg(MSG_DATA_SOURCE_NAME_SETUP_FAILED,16,MSG_ERROR)
							bOk = .T.
						endif
						*--- Zucchetti Aulla Fine - Errore impossibile aprire word
					Else
						i_dn=Sys(5)+Sys(2003)+'\'+i_rn+'.doc'
						i_oWfile = w.documents.Add()
						i_oWfile.mailmerge.maindocumenttype=3
						*** Zucchetti Aulla - aggiunta possibilit� di fare esportazione per mail merge con excel
						i_oWfile.mailmerge.opendatasource(forceext(N,'xlsx'))
						i_oWfile.SaveAs(i_dn)
					Endif
					Delete File (forceext(N,'xlsx'))
					If Not bOk
						cp_ErrorMsg(MSG_CANNOT_OPEN_MSWORD,16,MSG_ERROR)
					Endif
				Else
					cp_ErrorMsg(MSG_CANNOT_OPEN_WORD_DOCUMENT_QM,16,MSG_ERROR)
				Endif
					Else
						cp_ErrorMsg(MSG_CANNOT_OPEN_MSEXCEL,16,MSG_ERROR)
					EndIf
				On Error &i_err
				Select (oZoom.cCursor)
				EndIf
			Else
				cp_ErrorMsg(MSG_DATA_MISSING_C_CANNOT_CREATE_MAILMERGE,16,MSG_ERROR)
			Endif
		Else
			* --- crea una copia del cursore
			Local N,d
			N=Getenv('TEMP')
			d=Set('DATABASE')
			Create Database (N+'\_data_')
			Copy To (N+'\__tmp__') Database (N+'\_data_')
			Select 0
			Use (N+'\__tmp__')
			* --- se non esiste, crea il report
			If Not(cp_IsStdFile(i_rn,'frx'))
				i_flds=''
				For i=1 To Fcount()
					If Field(i)<>'CPCCCHK'
						i_flds=i_flds+Iif(Empty(i_flds),'',',')+Field(i)
					Endif
				Next
				*--- Zucchetti Aulla Inizio - Report Wizard
                * --- Verifica avvio Report Wizard
				If IsAlt() Or (Vartype(g_bnousewizard) <> 'U' And  g_bnousewizard)
				Create Report (i_rn) From __tmp__ Fields &i_flds
				Else
					* --- Preparazione cursore ed avvio del Report Wizard
                    Local tabella,numpar,tabpar, nFields, cFieldDescri, nPosDescri
                    tabella = Sys(2015)
                    tabpar = Sys(2015)
                    Create Cursor (tabella) (FIELDNAME C(60), FIELDDESC C(80), FIELDTYPE C(1), FIELDUSED C(1))
                    nFields = AFIELDS(L_ArrFields, '__tmp__')
                    * --- Inserimento dati campi cursore di stampa
                    For i=1 To nFields
                    	* Verifico se nell'array dei campi � presente quello attuale, se � presente recupero la descrizione
                    	* impostata nel disegnatore di query, altrimenti metto il solito nome del campo (Es. query senza campi o con campi e *)
                    	cFieldDescri = ""
                    	IF ALEN(oZoom.Parent.cFields,1)>=i AND VARTYPE(oZoom.Parent.cFields(i))<>'L'
							nPosDescri = ASCAN(oZoom.Parent.cFields, oZoom.Parent.cFields(i), 1, -1, 1, 15)
                    		cFieldDescri = IIF( nPosDescri > 0,  oZoom.Parent.cColTitle(nPosDescri), '')
                    	ENDIF
                    	cFieldDescri = EVL(cFieldDescri, LEFT(ALLTRIM(L_ArrFields[i,1]),80) )
                        Insert Into (tabella) Values( LEFT(ALLTRIM(L_ArrFields[i,1]),60), cFieldDescri, LEFT(ALLTRIM(L_ArrFields[i,2]),1), 'N')
                    NEXT
                    RELEASE L_ArrFields
                    * --- Inserimento parametri associati alla visual query
                    Create Cursor (tabpar) (PARNAME C(20), PARDESC C(50), FIELDTYPE C(1), FIELDUSED C(1))
                    L_macro = "cPATHREPORT = GSUT_BWR(.Null.,tabella,Forceext(i_rn,'frx'), tabpar, .T.)"
                    &L_macro
                    Use In Select(tabella)
                    Use In Select(tabpar)
					i_rn = FORCEEXT(cPATHREPORT, "frx")
                Endif
                *--- Zucchetti Aulla Fine - Report Wizard
			Endif
			* --- modifica il report
			*--- Zucchetti Aulla Inizio - Report Wizard
			If !EMPTY(i_rn)
			cp_ModifyReport(cp_GetStdFile(i_rn,'FRX'))
			EndIf
			*--- Zucchetti Aulla Fine - Report Wizard
			* --- distrugge il file temporaneo creato durante la costruzione del report
			Use in Select("__tmp__")
			Close Database
			Delete Database (N+'\_data_')
			Delete File (N+'\__tmp__.dbf')
			If Not(Empty(d))
				Set Database To (d)
			Endif
			Select (oZoom.cCursor)
		Endif
	Else
		CP_MSG(cp_Translate(MSG_CONFIGURATION_FILE_MISSING_QM),.F.)
	Endif
	* --- Rimozione scelta di menu'
	Release Pad Vista Of _Msysmenu
Endproc

Define Class stdzRep As CPToolBtn
	Proc Repo(bPreview)
		Local i_rn,i_dn
		*i_rn=trim(this.parent.cZoomName)+'_'+trim(this.parent.cSymFile)
		*if not(empty(this.parent.cZoomName)) and file(i_rn+'.frx')
		*  do cp_ManageReport with (this.parent),'std',bPreview
		*else
		*  wait window "Report non definito"
		*endif
		If Not(Empty(This.Parent.cZoomName))
			If Used('__tmp__')
				Select __tmp__
				Use
			Endif
			If Type('i_bDisableAsyncConn')<>'U' And i_bDisableAsyncConn
				This.Parent.RetriveAllRows()
				Select (This.Parent.cCursor)
				Go Top
			Else
				Select (This.Parent.cCursor)
				* --- si sposta alla fine in modo da scaricare tutto il cursore in caso di query asicrone
				CP_MSG( cp_Translate(MSG_RETRIEVING_ALL_RECORDS_D) )
				Go Bottom
				Go Top
				Wait Clear
			Endif
			* ---
			Use Dbf() In 0 Again Alias __tmp__
			i_rn=This.Parent.cZoomName+'_'+This.Parent.cSymFile
			i_dn=Sys(5)+Sys(2003)
			If Left(i_rn,Len(i_dn))==i_dn
				i_rn=Substr(i_rn,Len(i_dn)+2)
			Endif
			If Upper(Left(i_rn,4))=='STD\'
				i_rn=Substr(i_rn,5)
			Endif
			* --- CP_CHPRN fuori dall'eseguibile
			Local l_sPrg
			l_sPrg="cp_chprn"
			Do (l_sPrg) With i_rn,,This.Parent
			Release l_sPrg
			USE IN SELECT('__tmp__')
		Else
			CP_MSG(cp_Translate(MSG_CONFIGURATION_FILE_MISSING_QM),.F.)
		Endif
	Proc Click()
		This.Repo(.F.)
		*proc MouseDown(nBtn,nSh,nX,nY)
		*  if nBtn=2
		*    nodefault
		*    this.Repo(.t.)
		*  endif
Enddefine

Define Class stdzSel As CPToolBtn
	Proc Click()
		If (This.Parent.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And i_ServerConn[1,2]<>0
			This.Parent.AskAdvancedParameters()
		Else
			This.Parent.AskParameters()
		Endif
		This.Parent.Query(This.Parent.o_initsel,.T.)
	Endproc
Enddefine

Define Class stdzDir As CPToolBtn
	Proc Click()
		This.Parent.AskFile()
	Endproc
Enddefine

Define Class stdzRef As CPToolBtn
	Proc Click()
		This.Parent.Requery()
		Return

	Proc MouseDown(nBtn,nSh,nX,nY)
		* --- resetta le condizioni di ricerca
		If nBtn=2
			Nodefault
			This.Parent.RemoveUserFilters()
		Endif
	Endproc
Enddefine

*--- Contenitore per visualthemes
Define Class stdzparam_c As Container
	#If Version(5)>=900
		Add Object ImgBackGround As Image With Stretch=2, Visible=.F.
	#Endif
	Add Object advpage As stdzparam With Visible=.F.
	Add Object __dummy__ As CommandButton With Enabled=.F.,Width=0,Height=0,Style=0,SpecialEffect=1
	Visible=.F.
	Procedure Init(bNoVisible)
		This.Visible=Not bNoVisible
		This.Height = This.advpage.Height
		This.Width = This.advpage.Width+2
		If i_bGradientBck
			With This.ImgBackGround
				.Width = This.Width
				.Height = This.Height
				.Anchor = 15 && Resize Width and Height
				.Picture = i_ThemesManager.GetProp(11)
				.ZOrder(1)
				.Visible = .T.
			Endwith
		Endif
		This.BackColor = i_ThemesManager.GetProp(7)
		*--- Aggiungo un bordo
		This.BorderWidth = 1
		This.BorderColor = i_ThemesManager.GetProp(55)
		This.advpage.Visible=Not bNoVisible
		This.advpage.Tabs=.F.
		This.advpage.BorderWidth=0
		This.advpage.SetFont()
		*--- Rendo trasparenti le pagine
		Local pg, l_i
		For l_i=1 To This.advpage.PageCount
			pg = This.advpage.Pages[m.l_i]
			pg.BackStyle=0
		Next
		pg=.Null.
		This.AddObject("oTabMenu", "TabMenu", This, .F., .F., "advpage")
	Endproc
	*--- Accedo a advpage anzich� a this
	Procedure THIS_ACCESS
		Lparameter cMemberName
		If !Pemstatus(This, cMemberName, 5) And !Pemstatus(This.advpage, cMemberName, 5)
			Return This.Parent
		Endif
		If !Pemstatus(This, cMemberName, 5) And Pemstatus(This.advpage, cMemberName, 5)
			Return This.advpage
		Endif
		Return This
	Endproc
Enddefine

Define Class stdzAdv As CPToolBtn
	bSec1=.T.
	bSec2=.T.
	bSec3=.T.
	bSec4=.T.
	bFox26=.F.
	Caption=""
	Proc Click(bNoVisible)
		Local i_olderr
		Private i_err
		i_olderr=On('ERROR')
		i_err=.F.
		On Error i_err=.T.
		cp_GetSecurity(This,'Z*'+Thisform.cPrg)
		On Error &i_olderr
		If i_err=.F. And Not(This.bSec1)
			cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
			Return
		Endif
		If Not(This.Parent.bAdvanced)
			If This.Parent.Height>200
				This.Caption=cp_Translate(MSG_OPTIONS_FL)
				This.Parent.bAdvanced=.T.
				If i_VisualTheme<>-1 And i_cMenuTab<>"S"
					This.Parent.AddObject('advpage','stdzparam_c',bNoVisible)
				Else
					This.Parent.AddObject('advpage','stdzparam')
					This.Parent.advpage.SetFont()
				Endif
				This.Parent.Height=This.Parent.Height+This.Parent.advpage.Height
				This.Parent.advpage.Top=This.Parent.Height-This.Parent.advpage.Height
				This.Parent.advpage.Visible=Not bNoVisible
				This.Parent.qry.Visible=.T.
				This.Parent.zz.Visible=.F.
				This.Parent.ref.Enabled=.F.
				*this.parent.advpage.fillzoomlist()
				If Used(This.Parent.cCursor)
					This.Parent.RefreshParamWnd()
					Select (This.Parent.cCursor)
				Endif
				If Not bNoVisible
					This.Parent.Resize()
				Endif
				This.Parent.AddObject('grdshp_NW','zoom_gridshape',This.Parent.grd.Left-3,This.Parent.grd.Top-3,7,7, This.Parent.bNoZoomGridShape, 0)
				This.Parent.AddObject('grdshp_SE','zoom_gridshape',This.Parent.grd.Left+This.Parent.grd.Width-9,This.Parent.grd.Top+This.Parent.grd.Height-9,7,7, This.Parent.bNoZoomGridShape, 1)
				This.Parent.grdshp_SE.CreateImg(0)
				This.Parent.grdshp_NW.CreateImg(180)
			Else
				CP_MSG(cp_Translate(MSG_WINDOW_TOO_SMALL_QM),.F.)
			Endif
		Else
			This.Caption=cp_Translate(MSG_OPTIONS_FR)
			This.Parent.bAdvanced=.F.
			This.Parent.qry.Visible=.F.
			This.Parent.ref.Enabled=.T.
			*--- ZoonOnZoon non visibile se autozoom (problema menu contestuale)
			This.Parent.zz.Visible=Not(Empty(This.Parent.cZoomOnZoom)) and LOWER(This.Parent.name)<>"autozoom"
			If Vartype(This.Parent.advpage)="O"
				This.Parent.Height=This.Parent.Height-This.Parent.advpage.Height
				This.Parent.RemoveObject('advpage')
				This.Parent.RemoveObject('grdshp_NW')
				This.Parent.RemoveObject('grdshp_SE')
			Endif
			This.Parent.Resize()
		EndIf
		ThisForm.Resize()
	Endproc
Enddefine

Define Class stdzQry As CPToolBtn
	Proc Click(bNoModSQL)
		If !bNoModSQL and This.Parent.bModSQL
			Local s
			* --- Zucchetti Aulla inizio - i_Codazi non trimmato
			s=Strtran(Strtran(This.Parent.cSQL,'xxx',Alltrim(i_codazi)),Chr(13)+Chr(10),' ')
			This.Parent.bFillFields=Upper(This.Parent.cOldSelect)<>Upper(Left(s,At('FROM',Upper(s))))
		Endif
		This.Parent.Query(This.Parent.o_initsel)
		Return
Enddefine

#Define PANELVERTICAL_BACKCOLOR	7
#Define TOOLBAR_BORDERCOLOR 55

Define Class stdzButtonOption As Container
	bVisibleOption=Not i_AdvancedHeaderZoom
	BackStyle = 1
	BorderWidth = 2
	BorderColor = i_nBorderColor
	bGlobalFont = .T.
	FontSize   = 8
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	FontName   = ""
	MousePointer=15

	Add Object oBackGround As Image With ;
		Height = 28, ;
		Left = 1, ;
		Top = 1 , ;
		Width = 0, ;
		visible=.T., ;
		Name = "oBackGround", MousePointer=15

	Add Object lineleft As Line With ;
		Height = 28, ;
		Left = 12, ;
		Top = 5, ;
		Width = 0, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineLeft", MousePointer=15

	Add Object linetop As Line With ;
		Height = 0, ;
		Left = 12, ;
		Top = 5, ;
		Width = 48, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineTop", MousePointer=15

	Add Object linebottom As Line With ;
		BorderWidth = 0, ;
		Height = 0, ;
		Left = 11, ;
		Top = 31, ;
		Width = 48, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineBottom", MousePointer=15

	Add Object lineright As Line With ;
		Height = 28, ;
		Left = 58, ;
		Top = 6, ;
		Width = 0, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineRight", MousePointer=15

	Procedure Init
		Local objcolumn
		This.bVisibleOption= This.bVisibleOption Or This.Parent.bVisibleOption
		If Val(Substr(Os(),8,Len(Os())-7))>=6
			*this.AddObject("oBackGround","image")
			This.oBackGround.Top=1
			This.oBackGround.Left=1
			This.oBackGround.Width=This.Width-2
			This.oBackGround.Height=This.Height-2
			This.oBackGround.Anchor=15
			This.oBackGround.Stretch=2
			This.oBackGround.Visible=.T.
			This.oBackGround.MousePointer=15
		Else
			This.oBackGround.Visible=.F.
		Endif
		This.ChangeTheme()
	Endproc
	Procedure ChangeTheme
		With This
			.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
			.BorderColor = i_ThemesManager.GetProp(TOOLBAR_BORDERCOLOR)
			If Val(Substr(Os(),8,Len(Os())-7))>=6
				If i_bGradientBck
					.oBackGround.Picture=i_ThemesManager.GetProp(54)
				Else
					.oBackGround.Visible=.F.
				Endif
			Else
				.Picture=i_ThemesManager.GetProp(54)
			Endif
		Endwith
	Endproc

	Proc oBackGround.Click()
		This.Parent.Click()
	Endproc

	Proc linetop.Click()
		This.Parent.Click()
	Endproc

	Proc linebottom.Click()
		This.Parent.Click()
	Endproc

	Proc lineleft.Click()
		This.Parent.Click()
	Endproc

	Proc lineright.Click()
		This.Parent.Click()
	Endproc

	Proc Click()
		If This.bVisibleOption
			This.bVisibleOption=.F.
			This.MousePointer=15
			This.linetop.MousePointer=15
			This.linebottom.MousePointer=15
			This.lineleft.MousePointer=15
			This.lineright.MousePointer=15
			This.oBackGround.MousePointer=15
			If This.Parent.bAdvanced
				This.Parent.adv.Click()
			Endif
		Else
			This.MousePointer=16
			This.linetop.MousePointer=16
			This.linebottom.MousePointer=16
			This.lineleft.MousePointer=16
			This.lineright.MousePointer=16
			This.oBackGround.MousePointer=16
			This.bVisibleOption=.T.
		Endif
		This.Parent.Resize()
		If Type('ThisForm.oPgFrm.Pages(1).oPag')<>'U' Or Type('ThisForm.Pages(1).oPag')<>'U'
			*-- Deve esistere gi� l'oPag per la cp_ResizeContainer
			ThisForm.Resize()
		EndIf
	Endproc
	Proc ToolTipText_Assign(xValue)
		With This
			.ToolTipText=m.xValue
			.oBackGround.ToolTipText=m.xValue
			.linetop.ToolTipText=m.xValue
			.linebottom.ToolTipText=m.xValue
			.lineleft.ToolTipText=m.xValue
			.lineright.ToolTipText=m.xValue
		Endwith
	Endproc
Enddefine

Define Class stdzColumn As cpzoomcolumn
	Tag=''
	rw=0
	cLabelFontC=''
	nDimFontC=0
	bItalicFontC=.F.
	bBoldFontC=.F.	
	*--- Interfaccia - Font
	Procedure Init
		DoDefault()
		This.SetColumnFont()
	Endproc
	Procedure SetColumnFont()
		Local l_oldFontName
		If Empty(This.cLabelFontC)
			l_oldFontName = This.FontName
			If Thisform.bGlobalFont
				SetFont('G', This)
			Else
				This.FontName = SetFontName('G', Thisform.FontName, Thisform.bGlobalFont)
				This.FontSize = SetFontSize('G', Thisform.FontSize, Thisform.bGlobalFont)
				This.FontItalic = SetFontItalic('G', Thisform.FontItalic, Thisform.bGlobalFont)
				This.FontBold = SetFontBold('G', Thisform.FontBold, Thisform.bGlobalFont)
			Endif
		Else
			This.FontName = This.cLabelFontC
			This.FontSize = This.nDimFontC
			This.FontItalic = This.bItalicFontC
			This.FontBold = This.bBoldFontC	
		Endif
	Endproc
	Proc Moved()
		This.Parent.Parent.SaveColumnStatus()
		Return
	Proc Resize()
		This.Parent.Parent.SaveColumnStatus()
		Return
Enddefine

**************************************************
*-- Class:        _timerdblclick
*-- Timer per evitare che al dblclick vanga aperta la maschera di click
*
Define Class _timerdblclick As Timer
	Height = 23
	Width = 23
	Enabled = .T.
	Interval = 500
	Name = "_timerdblclick"
	oRefObj=.Null.

	Procedure Timer
		Local i_olderr
                This.Enabled=.F.
		i_olderr=On('ERROR')
		On Error =.T.
		Do cp_showsearchfilterform With This.oRefObj
		This.Parent.oRefSearchFilter=i_curform
		
		On Error &i_olderr
	Endproc
Enddefine
**************************************************

Define Class stdzHeader As cpzoomHeader
	* La classe stdzHeader provvede alla gestione del click e del doppio click.
	* Con click si setta la condizione di ricerca, che avverra' sul campo clickato
	* Con doppio click si riordina per la colonna scelta.
	* Gestisce poi il tasto destro per la definizione delle condizioni di ricerca
	nHeight=0
	cHyperLink=''
	cHyperLinkTarget=''
	bIsImage=.F.
	cLayerContent=''
	nColumnType=0
	cTooltip=''
	cLabelFont=''
	nDimFont=0
	bItalicFont=.F.
	bBoldFont=.F.
	*---  Interfaccia
	bSetFont = .T.
	
	Procedure Init
		DoDefault()
	Endproc
	
	Procedure SetFont()
		Local l_oldFontName
		If Empty(This.cLabelFont)
			l_oldFontName = This.FontName
			If Thisform.bGlobalFont
				SetFont('H', This)
			Else
				This.FontName = SetFontName('H', Thisform.FontName, Thisform.bGlobalFont)
				This.FontSize = SetFontSize('H', Thisform.FontSize, Thisform.bGlobalFont)
				This.FontItalic = SetFontItalic('H', Thisform.FontItalic, Thisform.bGlobalFont)
				This.FontBold = SetFontBold('H', Thisform.FontBold, Thisform.bGlobalFont)
			Endif
		Else
			This.FontName = This.cLabelFont
			This.FontSize = This.nDimFont
			This.FontItalic = This.bItalicFont
			This.FontBold = This.bBoldFont
		Endif
	Endproc
	
	Proc Click()
		* --- setta questa colonna come controllo per la where
		This.Parent.Parent.Parent.SetWhereCaption(This.Parent.ControlSource,This.Caption)
		If (This.Parent.Parent.Parent.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And (Vartype(i_cZoomMode)='U' Or i_cZoomMode='S')
			If Vartype(This.Parent.Parent.Parent.oRefSearchFilter)='O'
				This.Parent.Parent.Parent.oRefSearchFilter.Destroy()
			Endif
			If Vartype(This.Parent.Parent.Parent.__timerdblclick)='O'
				This.Parent.Parent.Parent.RemoveObject("__timerdblclick")
			Endif
			If This.Parent.Parent.Parent.bSearchFilterOnClick
				This.Parent.Parent.Parent.AddObject("__timerdblclick", "_timerdblclick")
				This.Parent.Parent.Parent.__timerdblclick.oRefObj=This.Parent
			Endif
		Endif
	Endproc

	Proc DblClick()
		If Vartype(This.Parent.Parent.Parent.__timerdblclick)='O'
			This.Parent.Parent.Parent.RemoveObject("__timerdblclick")
		Endif
		If This.Parent.Parent.Parent.bQueryOnDblClick
			Local N
			N=This.Parent.ControlSource
			If At('.',N)<>0
				N=Substr(N,At('.',N)+1)
			Endif
			If Type(N)<>'M' And N<>'cp_MemoFirst'
				Thisform.LockScreen=.T.
				If This.Parent.Parent.Parent.nOrderBy=1 And This.Parent.Parent.Parent.cOrderBy[1]=N
					* --- era gia' ordinato su questa colonna, cambia ascending/descending
					This.Parent.Parent.Parent.bOrderDesc[1]=Not(This.Parent.Parent.Parent.bOrderDesc[1])
					This.Parent.Parent.Parent.RefreshParamWnd()
				Else
					* --- segnala al form di riordinare per questa colonna
					This.Parent.Parent.Parent.nOrderBy=0
					This.Parent.Parent.Parent.AddOrderBy(N)
				Endif
				This.Parent.Parent.Parent.DeferredQuery()
				Thisform.LockScreen=.F.
			Else
				CP_MSG(cp_Translate(MSG_YOU_CANNOT_ORDER_USING_MEMO_FIELDS),.F.)
			Endif
		Endif
	Endproc
	
	Proc RightClick
		Local oFrm
		*** creo menu per gestione nuovi comandi
		Local i_olderr,cFileMenu
		i_olderr=On('ERROR')
		On Error =.T.
		g_oMenu=Createobject("IM_ContextMenu", This, "", "", 7)
		g_oMenu.BeforeCreaMenu()
		g_oMenu=.Null.
		On Error &i_olderr
	Endproc
Enddefine

*** Procedure per la gestione a menu dei controlli dello zoom
Procedure cp_askcolumntitleproc(pobj)
	Createobject('zz_deferred_askcolumntitle',pobj)
Endproc

Define Class zz_deferred_askcolumntitle As Timer
	* --- solito trucco per poter eseguire delle operazioni in ritardo
	Interval=200
	oThis=.Null.
	oObj=.Null.
	Proc Init(oObj)
		This.oObj=oObj
		This.oThis=This
	Proc Timer()
		Do cp_askcolumntitle With This.oObj
		This.oObj=.Null.
		This.oThis=.Null.
Enddefine

Procedure cp_DeleteColumnZoom(pobj)
	Local i,ok
	ok=.T.
	If pobj.ControlSource$pobj.Parent.Parent.cKeyFields
		If Not cp_YesNo(cp_MsgFormat(cp_Translate(cp_RetrieveMacroVcx("MSG__BELONGS_TO_PRIMARY_KEY_F_DRAG_AND_DROP_WILL_BE_DISABLED_C_CONTINUE_QP")),pobj.ControlSource),,.F.)
			ok=.F.
		Endif
	Endif
	If ok
		i=pobj.Parent.Parent.Getcol(pobj.ControlSource)
		If i>0
			pobj.Parent.Parent.DeleteColumn(i)
			pobj.Parent.Parent.bChangedFld=.F.
			pobj.Parent.Parent.RefreshParamWnd()
			Createobject('zz_deferred_query',pobj.Parent.Parent)
		Endif
	Endif
Endproc

Procedure cp_BlankAllOrderZoom(pobj, bOrderDefault)
	* --- azzero tutti gli ordinamenti
	If pobj.Parent.Parent.nOrderBy>0
		Do Whil pobj.Parent.Parent.nOrderBy>0
			pobj.Parent.Parent.DeleteOrderBy(pobj.Parent.Parent.nOrderBy)
		Enddo
	Endif
	pobj.Parent.Parent.nOrderBy=0
	pobj.Parent.Parent.cOrderBy[1]=''
	pobj.Parent.Parent.bOrderDesc[1]=.F.
	If bOrderDefault And pobj.Parent.Parent.nOrderByDefault>0
		pobj.Parent.Parent.nOrderBy=pobj.Parent.Parent.nOrderByDefault
		Dimension pobj.Parent.Parent.cOrderBy[pobj.Parent.Parent.nOrderBy]
		Dimension pobj.Parent.Parent.bOrderDesc[pobj.Parent.Parent.nOrderBy]
		Local nIdx
		For nIdx=1 To pobj.Parent.Parent.nOrderBy
			pobj.Parent.Parent.cOrderBy[nIdx]=pobj.Parent.Parent.cOrderByDefault[nIdx]
			pobj.Parent.Parent.bOrderDesc[nIdx]=pobj.Parent.Parent.bOrderDescDefault[nIdx]
		Next
	Endif
	pobj.Parent.Parent.RefreshParamWnd()
	Createobject('zz_deferred_query',pobj.Parent.Parent)
Endproc

Procedure cp_BlankOrderZoom(pobj)
	* --- azzero ordinamento
	If pobj.Parent.Parent.nOrderBy>0
		Local i
		i=1
		Do While i<=pobj.Parent.Parent.nOrderBy And pobj.ControlSource<>pobj.Parent.Parent.cOrderBy[i]
			i=i+1
		Enddo
		If i<=pobj.Parent.Parent.nOrderBy
			pobj.Parent.Parent.DeleteOrderBy(i)
		Endif
	Endif
	pobj.Parent.Parent.RefreshParamWnd()
	Createobject('zz_deferred_query',pobj.Parent.Parent)
Endproc

Procedure cp_AddOrderZoom(pobj, bOrderDesc)
	Local cControlSource
	cControlSource=pobj.Parent.ControlSource
	If Type(cControlSource)<>'M' And cControlSource<>'cp_MemoFirst'
		* --- aggiungo ordinamento
		If pobj.Parent.Parent.Parent.nOrderBy>0
			Local i
			i=1
			Do While i<=pobj.Parent.Parent.Parent.nOrderBy And pobj.Parent.ControlSource<>pobj.Parent.Parent.Parent.cOrderBy[i]
				i=i+1
			Enddo
			If i<=pobj.Parent.Parent.Parent.nOrderBy
				pobj.Parent.Parent.Parent.DeleteOrderBy(i)
			Endif
		Endif
		pobj.Parent.Parent.Parent.AddOrderBy(pobj.Parent.ControlSource)
		pobj.Parent.Parent.Parent.bOrderDesc[pObj.Parent.Parent.Parent.nOrderBy]=bOrderDesc
		pobj.Parent.Parent.Parent.RefreshParamWnd()
		Createobject('zz_deferred_query',pobj.Parent.Parent.Parent)
	Else
		CP_MSG(cp_Translate(MSG_YOU_CANNOT_ORDER_USING_MEMO_FIELDS),.F.)
	Endif
Endproc

Procedure cp_AddFirstOrderZoom(pobj,bOrderDesc)
	* --- aggiungo ordinamento
	Local cOrderByNew, bOrderDescNew, i, cControlSource
	cControlSource=pobj.Parent.ControlSource
	If Type(cControlSource)<>'M' And cControlSource<>'cp_MemoFirst'
		If pobj.Parent.Parent.Parent.nOrderBy>0
			Local i
			i=1
			Do While i<=pobj.Parent.Parent.Parent.nOrderBy And pobj.Parent.ControlSource<>pobj.Parent.Parent.Parent.cOrderBy[i]
				i=i+1
			Enddo
			If i<=pobj.Parent.Parent.Parent.nOrderBy
				pobj.Parent.Parent.Parent.DeleteOrderBy(i)
			Endif
		Endif
		pobj.Parent.Parent.Parent.AddOrderBy(pobj.Parent.ControlSource)
		cOrderByNew=pobj.Parent.Parent.Parent.cOrderBy[pObj.Parent.Parent.Parent.nOrderBy]
		bOrderDescNew=bOrderDesc
		For i=pobj.Parent.Parent.Parent.nOrderBy To 2 Step -1
			pobj.Parent.Parent.Parent.cOrderBy[i]=pobj.Parent.Parent.Parent.cOrderBy[i-1]
			pobj.Parent.Parent.Parent.bOrderDesc[i]=pobj.Parent.Parent.Parent.bOrderDesc[i-1]
		Endfor
		pobj.Parent.Parent.Parent.cOrderBy[1]=cOrderByNew
		pobj.Parent.Parent.Parent.bOrderDesc[1]=bOrderDescNew
		pobj.Parent.Parent.Parent.RefreshParamWnd()
		Createobject('zz_deferred_query',pobj.Parent.Parent.Parent)
	Else
		CP_MSG(cp_Translate(MSG_YOU_CANNOT_ORDER_USING_MEMO_FIELDS),.F.)
	Endif
Endproc

Procedure cp_InvertOrderZoom(pobj, bOrderDesc)
	* --- inverti ordinamento
	Local i
	i=1
	Do While i<=pobj.Parent.Parent.Parent.nOrderBy And pobj.Parent.ControlSource<>pobj.Parent.Parent.Parent.cOrderBy[i]
		i=i+1
	Enddo
	If i<=pobj.Parent.Parent.nOrderBy
		pobj.Parent.Parent.Parent.bOrderDesc[i]=Not pobj.Parent.Parent.Parent.bOrderDesc[i]
	Endif
	pobj.Parent.Parent.Parent.RefreshParamWnd()
	Createobject('zz_deferred_query',pobj.Parent.Parent.Parent)
Endproc

Procedure cp_BlankAllWhereZoom(pobj, bLeaveWhereAtLoad)
	* --- azzero tutti i filtri
	If pobj.Parent.Parent.nWhere>0
		Do Whil pobj.Parent.Parent.nWhere>0 And (Not bLeaveWhereAtLoad Or pobj.Parent.Parent.nWhere>pobj.Parent.Parent.nWhereAtLoad)
			pobj.Parent.Parent.DeleteWhere(pobj.Parent.Parent.nWhere)
		Enddo
	Endif
	Createobject('zz_deferred_query',pobj.Parent.Parent)
Endproc

Procedure cp_BlankWhereZoom(pobj)
	* --- azzero filtro
	If pobj.Parent.Parent.nWhere>0
		Local i, bPrevDelete
		i=1
		bPrevDelete=.F.
		Do While i<=pobj.Parent.Parent.nWhere
			If pobj.ControlSource$pobj.Parent.Parent.cWhere[i] Or (bPrevDelete And pobj.Parent.Parent.cWhere[i]='OR')
				bPrevDelete=.T. And Not pobj.Parent.Parent.cWhere[i]='OR'
				pobj.Parent.Parent.DeleteWhere(i)
				i=i-1
			Else
				bPrevDelete=.F.
			Endif
			i=i+1
		Enddo
		pobj.Parent.Parent.RefreshParamWnd()
	Endif
	Createobject('zz_deferred_query',pobj.Parent.Parent)
Endproc

Procedure cp_FilterEmpty(pobj,bBlankFilter)
	* --- filtro vuoto o pieno
	Local cFilterCond, i_OldCursor
	i_OldCursor=Select ()
	cFilterCond=''
	Select (pobj.Parent.RecordSource)
	If bBlankFilter
		Do Case
			Case Type(pobj.ControlSource)='D' Or Type(pobj.ControlSource)='T'
				cFilterCond=Alltrim(pobj.ControlSource)+" is null"
			Case Type(pobj.ControlSource)='N'
				cFilterCond=Alltrim(pobj.ControlSource)+" = 0"
			Case Type(pobj.ControlSource)='C'
				cFilterCond="[NOTEMPTYSTR("+Alltrim(pobj.ControlSource)+")] = 0"
		Endcase
	Else
		Do Case
			Case Type(pobj.ControlSource)='D' Or Type(pobj.ControlSource)='T'
				cFilterCond=Alltrim(pobj.ControlSource)+" is not null"
			Case Type(pobj.ControlSource)='N'
				cFilterCond=Alltrim(pobj.ControlSource)+" <> 0"
			Case Type(pobj.ControlSource)='C'
				cFilterCond="[NOTEMPTYSTR("+Alltrim(pobj.ControlSource)+")] <> 0"
		Endcase
	Endif
	If Not Empty(cFilterCond)
		pobj.Parent.Parent.AddWhere(cFilterCond)
	Endif
	Select (i_OldCursor)
	pobj.Parent.Parent.RefreshParamWnd()
	Createobject('zz_deferred_query',pobj.Parent.Parent)
Endproc

Define Class stdzTextbox As cpzoomTextbox
	* La class stdzTextbox gestisce il doppio click sul campo, che temina lo zoom e torna
	* la chiave del record scelto al programma chiamante.
	* Inizia una operazione di Drag&Drop con il move del mouse con il tasto sinistro premuto.
	Visible=.T.
	*backcolor=rgb(0,248,248)
	*forecolor=rgb(255,255,255)
	cFile=''
	Dimension xKey[1]
	nSeconds=0
	bInGrid=.F.
	nIdx=0
	* ---- Aggiunta Notifyevent per gestire selezione valori da zoom
	cCurInfo=""
	* ---
	bSetFont = .T.
	Proc Init()
		If Lower(This.Name)=='text1'
			This.bInGrid=.T.
			This.BorderStyle=0
			This.SpecialEffect=1
		Else
			This.nIdx=Val(Substr(This.Name,7))
		Endif
		If This.bSetFont
			This.SetFont()
		Endif
		This.Margin = 0
		Return
	Procedure SetFont()
	  IF TYPE("This.parent.FontName")=='C'
            This.FontName = This.parent.FontName
            This.FontSize = This.parent.FontSize
            This.FontItalic = This.parent.FontItalic
            This.FontBold = This.parent.FontBold
          endif
	Endproc
	Proc FillDragParam()
		If This.bInGrid
			This.cFile=This.Parent.Parent.Parent.cFile
			If !Empty(This.Parent.Parent.Parent.cZoomKeyFields)
				kf=This.Parent.Parent.Parent.cZoomKeyFields
			Else
				kf=This.Parent.Parent.Parent.cKeyFields
			Endif
		Else
			This.cFile=This.Parent.cFile
			If !Empty(This.Parent.cZoomKeyFields)
				kf=This.Parent.cZoomKeyFields
			Else
				kf=This.Parent.cKeyFields
			Endif
		Endif
		err=.F.
		If Not(Empty(kf))
			* --- la scatter puo' dare errore se non ci sono i campi della chiave tra quelli selezionati
			errsav=On('ERROR')
			On Error err=.T.
			oldarea=Select()
			If This.bInGrid
				Select (This.Parent.Parent.Parent.cCursor)
			Else
				Select (This.Parent.cCursor)
			Endif
			Scatter Fields &kf To This.xKey
			Select (oldarea)
			On Error &errsav
		Endif
		If err Or Empty(kf)
			* --- un valore che non puo' essere accettato come file se non e' stata letta la chiave primaria
			*cp_errormsg("Zoom senza i campi necessari al Drag&Drop")
			This.cFile='...'
			*return(.f.)
		Endif
		Return(.T.)
		* --- ritorna i valori e chiude
	Proc ReturnSelected()
		If Type("thisform.cTarget")<>'U' And Not(Empty(Thisform.cTarget))
			If This.FillDragParam()
				Local ot
				ot=Thisform.cTarget
				If Type(ot)='O'   && --- potrebbe essere "sparito"
					*** sotto indicazione di Paolo il codice di sotto dovrebbe essere una correzione solo per build 53
					*thisform.visible=.f. && Paolo Per nacondere lo zoom: problema Licon
					&ot..ecpDrop(This)
					If Type(Thisform.cTarget+'.parent.oContained')='O'  And Type(Thisform.cTarget+'.cFormvar')='C'
						* aggiunta NotifyEvent per gestire selezione valori da zoom; non riportata nella lista.
						* Esempio d'utilizzo evento: "w_MVCODICE  SelectedFromZoom"
						&ot..Parent.oContained.NotifyEvent(&ot..cFormVar+' SelectedFromZoom')
					Endif
				Endif
				If (This.Parent.Parent.Parent.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom)
					Local cObjManyHeaderName
					cObjManyHeaderName="oManyHeader_"+Thisform.z1.Name
					If Type("Thisform."+cObjManyHeaderName)='O'
						Thisform.RemoveObject(cObjManyHeaderName)
					Endif
				Endif
				Thisform.Destroy()
				ot=Null && Paolo rilascia la variabile con il cTarget; essendo local questa potrebbe essere evitata
			Endif
		Else
			If Type("thisform.nCPPrgType")='N'
				Thisform.ecpEdit()
			Else
				If Lower(This.Parent.Name)='autozoom' Or Lower(This.Parent.Parent.Parent.Name)='autozoom'
					This.FillDragParam()
					If This.cFile<>'...'
						* Assegnamento per caricare record al doppio click
						Thisform.oPgFrm.bAutoZoomLoad=.T.
						Thisform.oPgFrm.ActivePage=1
						Thisform.oPgFrm.Click()
					Else
						cp_ErrorMsg(MSG_ZOOM_WITHOUT_NEEDED_FIELDS_FOR_RECORD_SELECTION)
					Endif
				Else
					* --- tenta di notificare l' evento selected al contenitore
					Local i_errsave
					i_errsave=On('ERROR')
					On Error =.T.
					This.Parent.Parent.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Parent.Parent.Name)+' selected')
					On Error &i_errsave
				Endif
			Endif
		Endif
		Return
	Proc KeyPress(nKeyCode, nShiftAltCtrl)
		If nKeyCode=13
			Nodefault
			This.ReturnSelected()
		Endif
		*-- Abilita il puntino sul tastierino numerico per passare ad imputare la parte decimale
		*-- Se il separatore � gi� settato sul carattere . non faccio nulla altrimenti il keypress verr� richiamato ricorsivamente
		If nKeyCode=46 And Asc(Set('point'))<>46 Then
			If Type('this.value')='N' Then
				* Sostituzione carattere '.'
				Keyboard Set('point')
			Endif
		Endif
		local i_olderr
		i_olderr=on('ERROR')
		* -- La on error potrebbe essere ommessa in questo caso (lasciata per similitudine di comportamento con la Scrolled di Zoombox
		on error =.t.
		thisform._bPaint_ErrorGrid=.t.
		on error &i_olderr 		
		Return
		* --- gestione del drag&drop
	Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
		If nButton=1
			Nodefault
			If Seconds()-0.3<This.nSeconds
				This.ReturnSelected()
				** ZUCCHETTI AULLA gestione singolo click Inizio
			Else
				If VARTYPE(i_bZoomSingleClickSelMode)<>'U' And i_bZoomSingleClickSelMode And Vartype(This.Parent.Parent.Parent.Parent.oTimerSingleClick)='O'
					This.Parent.Parent.Parent.Parent.oTimerSingleClick.Enabled = .T.
					This.Parent.Parent.Parent.Parent.oCurrentCellHandle = This
				Endif
			Endif
			** ZUCCHETTI AULLA gestione singolo click Fine
			This.nSeconds=Seconds()
		Endif
		Return

		** ZUCCHETTI AULLA gestione singolo click Inizio
	Proc MouseUp(nButton, nShift, nXCoord, nYCoord)
		If VARTYPE(i_bZoomSingleClickSelMode)<>'U' And i_bZoomSingleClickSelMode And Vartype(This.Parent.Parent.Parent.Parent.oTimerSingleClick)='O'
			This.Parent.Parent.Parent.Parent.oTimerSingleClick.Enabled = .F.
			This.Parent.Parent.Parent.Parent.oCurrentCellHandle = .Null.
		Endif
		DoDefault()
	Endproc
	** ZUCCHETTI AULLA gestione singolo click Fine

	Proc MouseMove(nButton, nShift, nXCoord, nYCoord)
		Local kf,errsav,oldarea
		Private err
		If nButton=1
			Nodefault
			If This.FillDragParam()
				This.DragIcon="DragMove.cur"
				This.Drag(1)
			Endif
		Endif
		Return
	Proc DragDrop(oSource,nX,nY)
		If oSource.Class='Stdztextbox' And !This.bInGrid
			This.Parent.DragDrop(oSource,nX,nY)
		Endif
		Return
	Proc RightClick()
		This.dorightclick()
	Endproc
	Procedure dorightclick
		Local i_olderr,cFileMenu
		i_olderr=On('ERROR')
		On Error =.T.
		This.Parent.Parent.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Parent.Parent.Name)+' MouseRightClick')
		On Error &i_olderr
		This.FillDragParam()
		cFileMenu=Iif(Type("this.parent.parent.parent.cMenuFile")<>'C','',This.Parent.Parent.Parent.cMenuFile)
		If Type("this.parent.parent.parent")<>"U"
			g_oMenu=Createobject("IM_ContextMenu" ,This,This.Parent.Parent.Parent,cFileMenu,3)
		Else
			g_oMenu=Createobject("IM_ContextMenu" ,This,This.Parent,cFileMenu,3)
		Endif
		* --- Zucchetti Aulla - Gestione men� contestuale personalizzato
		g_oMenu.BeforeCreaMenu()
		g_oMenu=.Null.
	Endproc
Enddefine

Define Class stdzLabel As Label
	BackStyle = 0
	Proc DragDrop(oSource,nX,nY)
		This.Parent.DragDrop(oSource,nX,nY)
		Return
	Procedure Init()
		DoDefault()
		This.SetFont()

	Procedure SetFont()
		If Thisform.bGlobalFont
			SetFont('L', This)
		Else
			This.FontName = SetFontName('L', Thisform.FontName, Thisform.bGlobalFont)
			This.FontSize = SetFontSize('L', Thisform.FontSize, Thisform.bGlobalFont)
			This.FontItalic = SetFontItalic('L', Thisform.FontItalic, Thisform.bGlobalFont)
			This.FontBold = SetFontBold('L', Thisform.FontBold, Thisform.bGlobalFont)
		Endif
	Endproc
Enddefine

Func cp_NullValue
	Lparam cs
	* --- risponde il valore nullo per un campo
	Local res,T
	T=Type(cs)
	res=''
	Do Case
		Case T='C'
			res=''
		Case T='N' Or T='Y'
			res=0.00
		Case T='T'
			res=Ctot("  /  /  ")
		Case T='D'
			res=Ctod("  /  /  ")
		Case T='L'
			res=.F.
	Endcase
	Return(res)

Define Class stdzParamMask As CpSysform
	WindowType=1
	oZoom=.Null.
	*backcolor=rgb(192,192,192)
	FontBold=.F.
	Caption=MSG_SELECTION_PARAMETERS
	AutoCenter=.T.
	Icon="zoom.ico"
	Add Object OkBtn As CommandButton With Caption="",FontBold=.F.,Width=60,Height=20
	Add Object CancelBtn As CommandButton With Caption="",FontBold=.F.,Width=60,Height=20
	Proc Init(oZoom)
		This.Caption=cp_Translate(MSG_SELECTION_PARAMETERS)
		This.OkBtn.Caption=cp_Translate(MSG_OK_BUTTON)
		This.CancelBtn.Caption=cp_Translate(MSG_CANCEL_BUTTON)
		This.oZoom=oZoom
		This.CreateInput()
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
	Endproc
	Func HasCPEvents(cOp)
		Return(cOp=='ecpQuit')
	Func ecpQuit()
		This.Destroy()
	Proc Activate()
		i_curform=This
	Proc CreateInput()
		Local i,h,l,m,N,o,w,fld,op,cst,j1,j2,Col,fo
		h=5
		l=0
		m=0
		fo=.Null.
		For i=1 To This.oZoom.nWhere
			N=Alltrim(Str(i))
			w=This.oZoom.cWhere[i]
			If !(w=='OR')
				j1=At(' ',w)
				j2=At(' ',w,2)
				fld=Left(w,j1-1)
				op=Substr(w,j1+1,j2-j1-1)
				cst=Substr(w,j2+1)
				Col=This.oZoom.Getcol(fld)
				This.AddObject('lbl'+N,'Label')
				o=This.lbl&N
				o.Caption=Trim(Iif(Col<>0,This.oZoom.cColTitle[col],fld))+' '+This.TransOp(Trim(op))+':'
				l=Max(l,This.TextWidth(o.Caption))
				o.BackStyle=0
				o.Top=h+2
				o.Left=5
				o.FontBold=.F.
				o.Alignment=1
				o.Visible=.T.
				This.AddObject('txt'+N,'stdzParamBox')
				o=This.txt&N
				o.Top=h
				o.Left=100
				o.Width=Iif(Col<>0,This.oZoom.nColWidth[col]+8,200)
				m=Max(m,o.Width)
				o.Value=This.TransConst(cst)
				If Type('o.value')='C' And '[GLOBALVAR(' $ o.Value
					numpos=At('[GLOBALVAR(',o.Value)
					numposfin=At(']',o.Value)
					valore=Substr(o.Value,1,numpos-1)+Substr(o.Value,numpos+11,numposfin-numpos-12)+Substr(o.Value,numposfin+1)
					o.Value=&valore
				Endif
				If Type('o.value')='C' And '[DATAVUOTA]' $ o.Value
					o.Value=Ctod('  -  -    ')
				Endif
				If Type('o.value')='N'
					o.Width=o.Width+30
					If o.Value=Round(o.Value,3)
						o.InputMask=Repl("9",Int((o.Width-20)/10))+".999"
					Endif
				Endif
				o.FontBold=.F.
				o.nIndex=i
				o.Visible=.T.
				If Isnull(fo)
					fo=o
				Endif
				h=h+27
			Endif
		Next
		For i=1 To This.ControlCount
			w=This.Controls[i].Class
			Do Case
				Case w='Label'
					This.Controls[i].Width=l
				Case w='Stdzparambox'
					This.Controls[i].Left=l+10
			Endcase
		Next
		This.Width=Max(l+m+20,250)
		This.Height=h+35
		This.OkBtn.Left=This.Width-135
		This.OkBtn.Top=h+5
		This.CancelBtn.Left=This.Width-70
		This.CancelBtn.Top=h+5
		If Not(Isnull(fo))
			fo.SetFocus()
		Endif
	Endproc
	Proc Destroy()
		Thisform.Release()
	Endproc
	Proc OkBtn.Click()
		Thisform.SaveConst()
		Thisform.Destroy()
	Endproc
	Proc CancelBtn.Click()
		Thisform.Destroy()
	Endproc
	Func TransOp(op)
		Do Case
			Case op='like'
				Return('come')
			Case Inlist(op,'=','<=','>=')
				Return(op+' a')
			Case Inlist(op,'<>')
				Return(op+' da')
			Case Inlist(op,'<','>')
				Return(op+' di')
		Endcase
		Return(op)
	Endfunc
	Func TransConst(cst)
		If Right(cst,2)=']]'
			cst=Trim(Left(cst,At('[[',cst)-1))
		Endif
		Do Case
			Case Left(cst,1)="'"
				Return(Substr(cst,2,Len(cst)-2))
			Case Left(Trim(cst),1)$"0123456789"
				Return(Val(cst))
			Case Left(cst,4)="{d '"
				cst=Strtran(Strtran(cst,"{d '",'{^'),"'}",'}')
				Return(&cst)
			Case Left(cst,1)='{' Or Left(cst,1)='.'
				Return(&cst)
		Endcase
		Return(cst)
	Endfunc
	Proc SaveConst()
		Local i
		For i=1 To This.ControlCount
			If This.Controls[i].Class='Stdzparambox'
				This.oZoom.ChangeConst(This.Controls[i].nIndex,This.Controls[i].StrValue())
			Endif
		Next
		This.oZoom.RefreshParamWnd()
		*this.oZoom.Query()
	Endproc
Enddefine

Define Class stdzParamBox As TextBox
	nIndex=0
	Height=25
	Func StrValue
		Local T
		T=Type("this.value")
		If T='C'
			This.Value=Trim(This.Value)
		Endif
		* --- Problema maschera parametri e date vuote
		If T='D' And Empty(This.Value)
			This.Value =  '[DATAVUOTA]'
			Return(This.Value)
		Endif
		Return(cp_ToStr(This.Value))
	Endfunc
Enddefine

Define Class zoom_gridshape As Shape
	Visible=.T.
	nox=0
	noy=0
	MousePointer = 8
	Dimension aPolyPoints[3,2]
	Proc Init(x,Y,w,h,v,b)
		This.Top=m.Y
		This.Left=m.x
		This.Height=m.h
		This.Width=m.w
		This.Visible = !m.v
		This.BackStyle = m.b
		This.BorderStyle = m.b
	Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
		This.DragIcon='dragmove.cur'
		This.nox=nXCoord
		This.noy=nYCoord
		This.Drag(1)
	Proc DragDrop(oSource,nX,nY)
		This.Parent.DragDrop(oSource,nX,nY)
	Proc CreateImg(nRot)
		With This
	        .aPolyPoints[1,1] = 100&&A
	        .aPolyPoints[1,2] = 1
	        .aPolyPoints[2,1] = 100&&B
	        .aPolyPoints[2,2] = 100
	        .aPolyPoints[3,1] = 1&&C
	        .aPolyPoints[3,2] = 100
	        .Polypoints = "This.aPolyPoints"
	        .Rotation = m.nRot
	        .BackStyle = 0
	        .FillStyle = 0
	        .FillColor = Rgb(161,161,161)
		EndWith
    Endproc
		
Enddefine

Define Class zzbox As zoombox
	bQueryPending=.F.
	bAddedCheckBox=.T.
	bNoMenuHeaderProperty=.F.
	Proc Query(initsel,bSameTable,i_bNoFilter,i_bDontFill,i_bCreateHeader,i_bNotRemoveObject)
		Local i_OldCursor,i_newcursor
		This.bCheckBox=.F.
		This.bReadOnly=.F.
		If Type('this.parent.oContained')='O'
			* --- lasciato per compatibilita'
			This.Parent.oContained.NotifyEvent(This.Name+' before query')
			* --- forma corretta
			This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' before query')
		Endif
		* --- controllo focus
		Local i_oCtrl,i_err
		i_oCtrl=.Null.
		i_err=On('ERROR')
		On Error =.T.
		i_oCtrl=Thisform.ActiveControl
		On Error &i_err
		If !Isnull(i_oCtrl) And i_oCtrl.Name='GRD'
			Thisform.__dummy__.Enabled=.T.
			Thisform.__dummy__.SetFocus()
		Endif
		* --- esegue la query
		Thisform.LockScreen=.T.
		DoDefault(initsel,.T.,.T.,.T.,i_bCreateHeader,i_bNotRemoveObject)
		This.grd.Enabled=.F.
		Local cObjManyHeaderName
		cObjManyHeaderName="oManyHeader_"+This.Name
		If Vartype(This.oRefHeaderObject)='O'
			This.oRefHeaderObject.Parent.RemoveObject(cObjManyHeaderName)
			This.oRefHeaderObject=Null
		Endif
		Select (This.cCursor)
		*
		Thisform.LockScreen=.T.
		This.grd.ColumnCount=0
		This.grd.RecordSource=''
		* --- riempie la griglia
		This.grd.Fill()
		This.grd.Enabled=.F.
		This.AddCheckBox()
		If (This.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom)
			This.Parent.AddObject(cObjManyHeaderName,"ManyHeader")
			This.oRefHeaderObject=This.Parent.&cObjManyHeaderName
			This.oRefHeaderObject.InitHeader(This.grd,.T.)
		Endif
		If !Isnull(i_oCtrl) And i_oCtrl.Name='GRD'
			Thisform.__dummy__.Enabled=.F.
			i_oCtrl.SetFocus()
		Endif
		i_oCtrl=.Null.
		If Type('this.parent.oContained')='O'
			* --- lasciato per compatibilita'
			This.Parent.oContained.NotifyEvent(This.Name+' after query')
			* --- forma corretta
			This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' after query')
		Endif
		This.grd.Enabled=.T.
		Thisform.LockScreen=.F.
		This.bQueryPending=.F.
	Proc DeferredQuery()
		If This.bChangedFld
			cp_ErrorMsg(MSG_FIELD_LIST_CHANGED_QM,16)
		Else
			If !This.bQueryPending
				This.grd.Enabled=.F.
				This.bQueryPending=.T.
				Createobject('zz_deferred_query',This)
			Endif
		Endif
	Proc AddCheckBox()
		Local N
		N=Alltrim(Str(This.grd.ColumnCount+1))
		If Type("This.grd.column"+N)='O'
			This.grd.RemoveObject('column'+N)
		Endif
		This.grd.AddObject('column'+N,'Column')
		This.grd.Column&N..Visible=.T.
		This.grd.Column&N..Width=16
		This.grd.Column&N..header1.Caption=''
		This.grd.Column&N..header1.FontName=SetFontName('G', Thisform.FontName, .T.)
		This.grd.Column&N..header1.FontSize=SetFontSize('G', Thisform.FontSize, .T.)
		This.grd.Column&N..header1.FontItalic=SetFontItalic('G', Thisform.FontItalic, .T.)
		This.grd.Column&N..header1.FontBold=SetFontBold('G', Thisform.FontBold, .T.)
		This.grd.Column&N..ControlSource='xchk'
		*this.grd.Column&n..RemoveObject('text1')
		This.grd.Column&N..AddObject('chk','zzboxcheckbox')
		This.grd.Column&N..chk.Visible=.T.
		This.grd.Column&N..Sparse=.F.
		This.grd.Column&N..CurrentControl='chk'
		Dimension co[this.grd.columncount-1]
		For i=1 To This.grd.ColumnCount-1
			co[i]=This.grd.Columns(i).ColumnOrder
		Next
		This.grd.Column&N..ColumnOrder=1
		For i=1 To This.grd.ColumnCount-1
			j=0
			For k=1 To This.grd.ColumnCount-1
				If co[k]=i
					j=k
				Endif
			Next
			This.grd.Columns(j).ColumnOrder=i+1
		Next
		This.bCheckBox=.T.
		This.SaveColumnStatus()
		Return
Enddefine

Define Class zz_deferred_query As Timer
	* --- solito trucco per poter eseguire delle operazioni in ritardo
	Interval=200
	oThis=.Null.
	oObj=.Null.
	Proc Init(oObj)
		This.oObj=oObj
		This.oThis=This
	Proc Timer()
		If Vartype(This.oObj.oRefHeaderObject)='O'
			This.oObj.oRefHeaderObject.bRightClick=.F.
		Endif
		This.oObj.grd.Enabled=.F.
		This.oObj.src.SetFocus()
		Local i_bNoreMoveDblClick
		i_bNoreMoveDblClick=.T.
		This.oObj.Query(This.oObj.o_initsel)
		This.oObj=.Null.
		This.oThis=.Null.
Enddefine

Define Class zzboxcheckbox As Checkbox
	nFirstSelect=0
	nShift=0
	nLastValue=0
	Proc When()
		*if lastkey() = 32 or lastkey() = 13
		*   this.click()
		*  endif
		Return(This.Value<>2)
	Proc Click()
		Local c_mCursor
		If Type('this.parent.parent.parent.parent.oContained')='O'
			This.nLastValue=This.Value
			This.MultipleSelection()
			If This.Value=1
				* --- lasciato per compatibilita'
				This.Parent.Parent.Parent.Parent.oContained.NotifyEvent(This.Parent.Parent.Parent.Name+' row checked')
				* --- forma corretta
				This.Parent.Parent.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Parent.Parent.Name)+' row checked')
			Else
				* --- lasciato per compatibilita'
				This.Parent.Parent.Parent.Parent.oContained.NotifyEvent(This.Parent.Parent.Parent.Name+' row unchecked')
				* --- forma corretta
				This.Parent.Parent.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Parent.Parent.Name)+' row unchecked')
			Endif
			c_mCursor=This.Parent.Parent.Parent.cCursor
			Select (c_mCursor)
			This.nFirstSelect=Recno()
		Endif
	Proc MouseUp(nButton, nShift, nXCoord, nYCoord)
		This.nShift=nShift
	Proc MultipleSelection()
		Local c_mCursor
		Local nLastSelect
		If This.nShift = 1
			c_mCursor=This.Parent.Parent.Parent.cCursor
			Select (c_mCursor)
			nLastSelect=Recno()
			Update (c_mCursor) Set xchk=This.nLastValue Where ;
				(Recno()>=This.nFirstSelect And Recno()<=nLastSelect) Or ;
				(Recno()<=This.nFirstSelect And Recno()>=nLastSelect)
			Select (c_mCursor)
			Goto nLastSelect
			*--- Se selezioni/deseleziono pi� righe lancio l'evento multiple rows...
			If _Tally > 1
				If This.nLastValue=1
					This.Parent.Parent.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Parent.Parent.Name)+' multiple rows checked')
				Else
					This.Parent.Parent.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Parent.Parent.Name)+' multiple rows unchecked')
				Endif
			Endif
		Endif
Enddefine

Func cp_MemoFirstLine(i_cMemo)
	Local p
	p=At(Chr(13),i_cMemo)
	* --- Zucchetti Aulla - modificata lunghezza massima gestita in visualizzazione da 50 a 254
	If Len(i_cMemo)>254 And (p=0 Or p>254)
		Return(Trim(Left(i_cMemo,254)))
	Endif
	If p<>0
		Return(Trim(Left(i_cMemo,p-1)))
	Endif
	Return(Trim(i_cMemo))

Func cp_ControlSourceMemo(i_cMemo)
	*** funzione per determinale il controlsource di un campo memo
	Local cControlSource
	cControlSource=Substr(i_cMemo,18)
	cControlSource=Left(cControlSource,Len(cControlSource)-1)
	Return cControlSource

Procedure cp_ZoomFilterParam(i_obj)
	Local i_frm
	i_frm=Createobject("zoomfilterparam_form",i_obj)
	i_frm.Show()
	Return

Define Class zoompainter_form As CpSysform
	WindowType=1
	oObj=.Null.
	Icon=i_cBmpPath+i_cStdIcon
	Add Object btnok As CommandButton With Caption=MSG_OK_BUTTON,Width=50,Height=25
	Add Object btncan As CommandButton With Caption=MSG_CANCEL_BUTTON,Width=50,Height=25
	Proc Init(oObj)
		This.oObj=oObj
		This.LoadValues()
		This.btnok.Top=This.Height-30
		This.btnok.Left=This.Width-110
		This.btncan.Top=This.Height-30
		This.btncan.Left=This.Width-55
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
	Proc btnok.Click()
		Thisform.SaveValues()
		Thisform.Hide()
	Proc btncan.Click()
		Thisform.Hide()
	Proc LoadValues()
		Return
	Proc SaveValues()
		Return
Enddefine

Define Class zoomfilterparam_form As zoompainter_form
	Top=16
	Left=123
	Width=404
	Height=440
	WindowType=1
	Caption=MSG_VARIABLE_OPTIONS
	editPos=0
	Add Object lbl1 As Label With Caption=MSG_TEXT+MSG_FS,Alignment=1,Top=13,Left=11,Width=54,Height=19
	Add Object txt As TextBox With Top=11,Left=69,Width=200,Height=20,Margin=1
	Add Object samerow As Checkbox With Top=11,Left=273,Caption='Keep with previous',Width=120
	Add Object lbl3 As Label With Caption=MSG_TYPE+MSG_FS,Alignment=1,Top=39,Left=15,Width=50,Height=20
	Add Object typ As ComboBox With Top=38,Left=69,Width=96,Height=20
	Add Object lbl5 As Label With Caption=MSG_LEN_F+MSG_FS,Alignment=1,Top=66,Left=35,Width=30,Height=18
	Add Object lbl6 As Label With Caption=MSG_DEC_F+MSG_FS,Alignment=1,Top=68,Left=114,Width=32,Height=17
	Add Object Len As TextBox With  InputMask="999",Top=65,Left=69,Width=40,Height=21,Margin=1
	Add Object dec As TextBox With  InputMask="9",Top=65,Left=149,Width=31,Height=21,Margin=1
	Add Object lbl9 As Label With Caption=MSG_PICTURE+MSG_FS,Alignment=1,Top=109,Left=15,Width=50,Height=20
	Add Object Pict As TextBox With Top=106,Left=70,Width=287,Height=20,Margin=1
	*add object edt as checkbox with caption=MSG_EDITABLE,top=11,left=196,width=74,height=21
	*add object zfill as checkbox with caption=MSG_FILL_WITH_ZEROES,top=35,left=196,width=110,height=20
	Add Object lbl13 As Label With Caption=MSG_ARCHIVES+MSG_FS,Alignment=1,Top=150,Left=12,Width=52,Height=15
	Add Object arc As TextBox With Top=148,Left=65,Width=100,Height=20,Margin=1
	Add Object k1 As TextBox With Top=191,Left=15,Width=100,Height=20,Margin=1
	Add Object vk1 As TextBox With Top=191,Left=120,Width=100,Height=20,Margin=1
	Add Object k2 As TextBox With Top=213,Left=15,Width=100,Height=20,Margin=1
	Add Object vk2 As TextBox With Top=213,Left=120,Width=100,Height=20,Margin=1
	Add Object k3 As TextBox With Top=235,Left=15,Width=100,Height=20,Margin=1
	Add Object vk3 As TextBox With Top=236,Left=120,Width=100,Height=20,Margin=1
	Add Object lbl21 As Label With Caption=MSG_FIXED_KEY,Top=174,Left=15,Width=96,Height=14
	Add Object ka As TextBox With Top=257,Left=15,Width=100,Height=20,Margin=1
	Add Object lbl23 As Label With Caption=MSG_VALUES,Top=174,Left=121,Width=48,Height=14
	Add Object rf1 As TextBox With Top=300,Left=15,Width=100,Height=20,Margin=1
	Add Object rv1 As TextBox With Top=300,Left=120,Width=100,Height=20,Margin=1
	Add Object lbl26 As Label With Caption=MSG_LINKED_FIELD,Top=282,Left=14,Width=118,Height=16
	Add Object cmb As TextBox With Top=329,Left=80,Height=23,Width=300
	Add Object cmblbl As Label With Top=333,Left=15,Caption='Combobox:',Width=65
	Add Object adv As Checkbox With Top=357,Left=15,Caption='Advanced',Width=75
	Add Object advgrp As TextBox With Top=355,Left=90,Width=180,Height=20,Margin=1
	Add Object lbl27 As Label With Caption="Query prm.:",Top=382,Left=14,Width=118,Height=16
	Add Object prm As TextBox With Top=379,Left=80,Height=23,Width=300
	*
	Add Object arcbtn As CommandButton With Top=148,Left=165,Width=15,Height=20,Caption=MSG_D
	Proc arcbtn.Click()
		Local o
		o=Createobject('dcx_select_table')
		o.Show()
		If !Isnull(o) And o.nChoosed<>0
			Local N,k,c,i,j,v
			* --- svuota
			This.Parent.k1.Value=''
			This.Parent.k2.Value=''
			This.Parent.k3.Value=''
			This.Parent.ka.Value=''
			*
			N=i_Dcx.GetTable(o.nChoosed)
			k=cp_KeyToSQL(i_Dcx.GetIdxDef(N,1))
			i=At(',',k)
			j=1
			Do While i<>0
				c=Left(k,i-1)
				v=Str(j,1)
				This.Parent.k&v..Value=c
				k=Substr(k,i+1)
				i=At(',',k)
				j=j+1
			Enddo
			This.Parent.ka.Value=k
			This.Parent.arc.Value=N
		Endif
		Return
	Proc LoadValues()
		This.typ.AddItem(MSG_CHARACTER)
		This.typ.AddItem(MSG_NUMERIC)
		This.typ.AddItem(MSG_DATE )
		Local i,j,w,i_tmp
		i=0
		For j=1 To This.oObj.Parent.WhereLst.ListCount
			If This.oObj.Parent.WhereLst.Selected(j)
				i=j
			Endif
		Next
		This.editPos=i
		w=This.oObj.Parent.Parent.Parent.cWhere(i)
		If Right(w,2)=']]'
			p=At('[[',w)
			w=Substr(w,p+2,Len(w)-p-3)
			This.txt.Value=This.GetValue(@w)
			i_tmp=This.GetValue(@w)
			This.typ.Value=Iif(i_tmp='N',MSG_NUMERIC,Iif(i_tmp='D',MSG_DATE ,MSG_CHARACTER))
			This.Len.Value=This.GetValue(@w)
			This.dec.Value=This.GetValue(@w)
			This.Pict.Value=This.GetValue(@w)
			This.arc.Value=This.GetValue(@w)
			This.k1.Value=This.GetValue(@w)
			This.k2.Value=This.GetValue(@w)
			This.k3.Value=This.GetValue(@w)
			This.ka.Value=This.GetValue(@w)
			This.vk1.Value=This.GetValue(@w)
			This.vk2.Value=This.GetValue(@w)
			This.vk3.Value=This.GetValue(@w)
			This.rf1.Value=This.GetValue(@w)
			This.rv1.Value=This.GetValue(@w)
			This.cmb.Value=This.GetValue(@w)
			This.adv.Value=(Iif(This.GetValue(@w)='S',1,0))
			This.advgrp.Value=This.GetValue(@w)
			This.samerow.Value=(Iif(This.GetValue(@w)='S',1,0))
			This.prm.Value=This.GetValue(@w)
		Endif
	Proc SaveValues()
		Local i_res
		i_res="[["+Trim(This.txt.Value)
		i_res=i_res+'���'+Iif(This.typ.Value=MSG_DATE ,'D',Iif(This.typ.Value=MSG_NUMERIC,'N','C'))
		i_res=i_res+'���'+This.Len.Value
		i_res=i_res+'���'+This.dec.Value
		i_res=i_res+'���'+Trim(This.Pict.Value)
		i_res=i_res+'���'+Trim(This.arc.Value)
		i_res=i_res+'���'+Trim(This.k1.Value)
		i_res=i_res+'���'+Trim(This.k2.Value)
		i_res=i_res+'���'+Trim(This.k3.Value)
		i_res=i_res+'���'+Trim(This.ka.Value)
		i_res=i_res+'���'+Trim(This.vk1.Value)
		i_res=i_res+'���'+Trim(This.vk2.Value)
		i_res=i_res+'���'+Trim(This.vk3.Value)
		i_res=i_res+'���'+Trim(This.rf1.Value)
		i_res=i_res+'���'+Trim(This.rv1.Value)
		i_res=i_res+'���'+Trim(This.cmb.Value)
		If This.adv.Value=1
			i_res=i_res+'���S'
		Else
			i_res=i_res+'���N'
		Endif
		i_res=i_res+'���'+Trim(This.advgrp.Value)
		If This.samerow.Value=1
			i_res=i_res+'���S'
		Else
			i_res=i_res+'���N'
		Endif
		i_res=i_res+'���'+Trim(This.prm.Value)
		i_res=i_res+']]'
		Local i_cw,p
		i_cw=This.oObj.Parent.Parent.Parent.cWhere(This.editPos)
		p=At('[[',i_cw)
		If p<>0
			i_cw=Left(i_cw,p-2)
		Endif
		This.oObj.Parent.Parent.Parent.cWhere(This.editPos)=i_cw+' '+i_res
	Function GetValue(w)
		Local p,i_res
		p=At('���',w)
		If p<>0
			i_res=Left(w,p-1)
			w=Substr(w,p+3)
		Else
			i_res=w
			w=''
		Endif
		Return i_res
Enddefine

Proc cp_showsearchfilterform
	Lparameters toHeaderObject
	If Vartype(toHeaderObject.Parent.Parent.oRefSearchFilter)='O'
		toHeaderObject.Parent.Parent.oRefSearchFilter.ecpQuit()
	Endif
	If Vartype(toHeaderObject.Parent.Parent.oRefHeaderObject)='O'
		toHeaderObject.Parent.Parent.oRefHeaderObject.InitHeader(toHeaderObject.Parent, Inlist(Lower(oHeaderObject.Parent.Parent.Class), 'cp_szoombox', 'cp_zoombox', 'stdzoom', 'zzbox'))
	Endif
	Do cp_searchfilter With toHeaderObject
	toHeaderObject.Parent.Parent.oRefSearchFilter=i_curform
Endproc
