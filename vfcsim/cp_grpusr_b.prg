* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_grpusr_b                                                     *
*              Batch gestione Utenti/Gruppi                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-09-25                                                      *
* Last revis.: 2016-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_grpusr_b",oParentObject,m.pParam)
return(i_retval)

define class tcp_grpusr_b as StdBatch
  * --- Local variables
  pParam = space(15)
  w_UN = space(20)
  w_PW = space(20)
  w_LANG = space(3)
  w_LOG = space(20)
  w_DATAAGG = ctod("  /  /  ")
  w_LRECCOUNT = 0
  w_MAXROW = 0
  w_ENABLED = space(1)
  w_PWDCHECK = .f.
  w_FIND = .f.
  w_USRGRP = space(4)
  w_NOME = space(30)
  w_BATCH = space(15)
  w_AZIENDA = space(5)
  w_TIPO = space(1)
  w_NEWNAME = space(30)
  w_GRUPPO = space(1)
  w_COUNT = 0
  w_RES = .f.
  w_CPUSRCOD = space(6)
  w_Untipo = space(1)
  * --- WorkFile variables
  UTE_AZI_idx=0
  cpusers_idx=0
  cpusrgrp_idx=0
  cpprgsec_idx=0
  cpssomap_idx=0
  cpgroups_idx=0
  POL_SIC_idx=0
  CP_PREVPWD_idx=0
  cp_prevpwd_idx=0
  POL_UTE_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiamato dalle gestioni CP_GRPUSR, CP_USERS, CP_GROUPS e GSUT_KWD(solo con paramentro UsrOk)
    * --- Batch unico per la gestione utenti/gruppi
    * --- Parametro pParam:
    *     
    *     G\U UsrCanc - Cancellazione Utente
    *     G\U GrpCanc - Cancellazione Gruppo
    *     G\U UsrMod- Modifica Utente
    *     G\U GrpMod - Modifica Gruppo
    *     G\U UsrNew - Nuovo Utente
    *     G\U GrpNew - Nuovo Gruppo
    *     CloseCursor - Chiusura Cursori
    *     
    *             ------------
    *     
    *     UsrCheck - Check Form
    *     UsrInit - Utente Inizializzazione
    *     UsrCurs - Utente Popola Cursori
    *     UsrBeforeQ - Utente Salvataggio Cursore
    *     UsrAfterQ - Utente Ripristina Cursore
    *     UsrModCode - Utente Modifica Codice
    *     UsrModLang - Utente Modifica Lingua
    *     UsrOk - Utente Bottone Ok
    *     
    *             ------------
    *     
    *     GrpInit - Gruppo Inizializzazione
    *     GrpCurs - Gruppo Popola Cursori
    *     GrpBeforeQ - Gruppo Salvataggio Cursore
    *     GrpAfterQ - Gruppo Ripristina Cursore
    *     GrpModCode - Gruppo Modifica Codice
    *     GrpOk - Gruppo Bottone Ok
    *     
    this.w_Untipo = Type("This.oparentobject.w_CODE")
    do case
      case this.w_Untipo="N"
        * --- Posgress evidenzia incongruenza tra tipo variabile numerica e campo carattere
        this.w_CPUSRCOD = Alltrim(str(this.oParentObject.w_CODE))
      case this.w_Untipo="C"
        this.w_CPUSRCOD = this.oParentObject.w_CODE
    endcase
    do case
      case this.pParam="G\U UsrCanc"
        this.w_USRGRP = this.oParentObject.w_Code
        this.w_NOME = nvl(this.oParentObject.w_Name,"")
        * --- Read from UTE_AZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UTE_AZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UACODAZI"+;
            " from "+i_cTable+" UTE_AZI where ";
                +"UACODUTE = "+cp_ToStrODBC(this.w_USRGRP);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UACODAZI;
            from (i_cTable) where;
                UACODUTE = this.w_USRGRP;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_AZIENDA = NVL(cp_ToDate(_read_.UACODAZI),cp_NullValue(_read_.UACODAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- --  Zucchetti Aulla Inizio  --
        if not empty(this.w_AZIENDA)
          cp_ErrorMsg(MSG_CANNOT_DELETE_USERCODE_LINKED_TO_COMPANY)
        else
          * --- --  Zucchetti Aulla Fine  --
          if cp_YesNo(cp_MsgFormat(MSG_DELETE_USER__C__QP,alltrim(str(this.w_USRGRP)),Trim(this.w_NOME),""),32,.f.)
            * --- Delete from cpusers
            i_nConn=i_TableProp[this.cpusers_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"code = "+cp_ToStrODBC(this.w_USRGRP);
                     )
            else
              delete from (i_cTable) where;
                    code = this.w_USRGRP;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Delete from cpusrgrp
            i_nConn=i_TableProp[this.cpusrgrp_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"usercode = "+cp_ToStrODBC(this.w_USRGRP);
                     )
            else
              delete from (i_cTable) where;
                    usercode = this.w_USRGRP;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Delete from CP_PREVPWD
            i_nConn=i_TableProp[this.CP_PREVPWD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CP_PREVPWD_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CPUSRCOD = "+cp_ToStrODBC(this.w_CPUSRCOD);
                     )
            else
              delete from (i_cTable) where;
                    CPUSRCOD = this.w_CPUSRCOD;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Delete from cpprgsec
            i_nConn=i_TableProp[this.cpprgsec_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpprgsec_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"usrcode = "+cp_ToStrODBC(this.w_USRGRP);
                     )
            else
              delete from (i_cTable) where;
                    usrcode = this.w_USRGRP;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Delete from cpssomap
            i_nConn=i_TableProp[this.cpssomap_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpssomap_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"userid = "+cp_ToStrODBC(this.w_USRGRP);
                     )
            else
              delete from (i_cTable) where;
                    userid = this.w_USRGRP;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          * --- --  Zucchetti Aulla Inizio  --
          * --- * --- Lancio il batch della cancellazione utente, questo batch serve per poter eliminare il codice utente da
          *     * --- tutte quelle gestioni che lo utilizzano
          GS___BDU(this,this.w_USRGRP)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oparentobject.NotifyEvent("Aggiorna")
          * --- --  Zucchetti Aulla Fine  --
        endif
      case this.pParam="G\U GrpCanc"
        this.w_USRGRP = this.oParentObject.w_GCODE
        this.w_NOME = this.oParentObject.w_GNAME
        this.w_TIPO = this.oParentObject.w_GRPTYPE
        if cp_YesNo(cp_MsgFormat(Iif(this.w_TIPO="R",MSG_DELETE_ROLE__C__QP,MSG_DELETE_GROUP__C__QP),alltrim(str(this.w_USRGRP)),Trim(this.w_NOME)),32,.f.)
          * --- Delete from cpgroups
          i_nConn=i_TableProp[this.cpgroups_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"code = "+cp_ToStrODBC(this.w_USRGRP);
                   )
          else
            delete from (i_cTable) where;
                  code = this.w_USRGRP;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from cpusrgrp
          i_nConn=i_TableProp[this.cpusrgrp_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"groupcode = "+cp_ToStrODBC(this.w_USRGRP);
                   )
          else
            delete from (i_cTable) where;
                  groupcode = this.w_USRGRP;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from cpprgsec
          i_nConn=i_TableProp[this.cpprgsec_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpprgsec_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"grpcode = "+cp_ToStrODBC(this.w_USRGRP);
                   )
          else
            delete from (i_cTable) where;
                  grpcode = this.w_USRGRP;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        * --- --  Zucchetti Aulla Inizio  --
        * --- * --- Lancio il batch della cancellazione del gruppo, questo batch serve per poter eliminare il codice gruppo da
        *     * --- tutte quelle gestioni che lo utilizzano
        GS___BDG(this,this.w_USRGRP)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oparentobject.NotifyEvent("Aggiorna")
        * --- --  Zucchetti Aulla Fine  --
      case this.pParam="G\U UsrMod"
        if this.oParentObject.w_ISADMIN or this.oParentObject.w_code=i_codute or i_codute=0
          * --- Indico al figlio che passo dalla modifica
          cp_users(this.oParentObject)
          * --- --  Zucchetti Aulla Inizio  --
          * --- Selezione delle aziende in cui abilitare l'utente
          GS___KNU(Alltrim(Str(this.oParentObject.w_CODE))+"*"+Alltrim(this.oParentObject.w_NAME))
          * --- --  Zucchetti Aulla Fine  --
        else
          cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
        endif
      case this.pParam="G\U GrpMod"
        if this.oParentObject.w_ISADMIN or i_codute=0
          cp_groups(this.oParentObject)
        else
          cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
        endif
      case this.pParam="G\U UsrNew"
        this.oParentObject.w_CODE = 0
        cp_users(this.oParentObject)
      case this.pParam="G\U GrpNew"
        this.oParentObject.w_GCODE = 0
        cp_groups(this.oParentObject)
      case this.pParam="G\U Init"
        Select (this.oParentObject.w_Utenti.cCursor)
        if RECCOUNT() > 0
          this.oParentObject.w_USERS = .T.
        endif
      case this.pParam="CloseCursor"
        if USED ("_cpusers_")
          USE IN SELECT ("_cpusers_")
        endif
        if USED ("_cpgroups_")
          USE IN SELECT ("_cpgroups_")
        endif
      case this.pParam="UsrCheck"
        * --- Verifico corrispondenza password corrente solo se si viene dalla gestione utenti
        if TYPE("this.oparentobject.w_SOURCE")="U" AND NOT this.oParentObject.w_NEW
          * --- Read from cpusers
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.cpusers_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "passwd"+;
              " from "+i_cTable+" cpusers where ";
                  +"code = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              passwd;
              from (i_cTable) where;
                  code = this.oParentObject.w_CODE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PW = NVL(cp_ToDate(_read_.passwd),cp_NullValue(_read_.passwd))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PWDCHECK = cp_checkpwd(this.oParentObject.w_CODE,rtrim(this.oParentObject.w_PWDO), rtrim(this.w_PW))
        endif
        if TYPE("this.oparentobject.w_SOURCE")="U" AND NOT this.w_PWDCHECK AND NOT this.oParentObject.w_NEW AND this.oParentObject.w_CODE==i_codute
          cp_errormsg(MSG_LAST_PASSWORD_NOT_MATCH)
          this.w_RES = .F.
        else
          this.w_RES = .T.
        endif
        do case
          case this.oParentObject.w_PWD<>this.oParentObject.w_PWDC AND this.w_RES
            cp_ErrorMsg(MSG_CONFIRM_PWD_DIFFERENT_FROM_PWD)
            this.w_RES = .F.
          case this.oParentObject.w_CODE=0 AND this.w_RES
            cp_errormsg(MSG_USER_CODE_MUST_BE_DIFFERENT_FROM_ZERO)
            this.w_RES = .F.
            * --- --  Zucchetti Aulla Inizio  --
          case this.oParentObject.w_VALIDPWD=2 AND this.oParentObject.w_ATTIVO="S" AND this.w_RES
            cp_errormsg(MSG_PWD_DOES_NOT_MEET_PREREQUISITES)
            this.w_RES = .F.
          case this.oParentObject.w_VALIDPWD=1 AND this.oParentObject.w_ATTIVO="S" AND this.w_RES
            if not cp_YesNo(MSG_PWD_LESS_THAN_RECOMMENDED_LENGTH_CONTINUE,32,.f.)
              this.w_RES = .F.
            endif
          case empty(nvl(this.oParentObject.w_LOGIN," ")) and this.oParentObject.w_CODUTE="S" AND this.w_RES
            cp_errormsg(MSG_SPECIFY_LOGIN)
            this.w_RES = .F.
        endcase
        do case
          case ((NOT this.oParentObject.w_UTEADMIN AND this.oParentObject.w_NORIP="S") OR (this.oParentObject.w_UTEADMIN AND this.oParentObject.w_NORIPA="S")) AND this.oParentObject.w_ATTIVO="S" AND this.w_RES
            this.w_PW = TRIM(LEFT(this.oParentObject.w_PWD,20))
            * --- * --- Se pwd non ripetibile, attiva gest. multi utenza check pwd cambiata
            *     * --- se utente non modifica la pwd assume come valore '__non_definita__' e quindi
            *     * --- il controllo scatta cmq.
            if this.oParentObject.w_CHANGE AND this.w_PW="__non_definita__"
              cp_ErrorMsg(MSG_INSERT_A_DIFFERENT_PWD_FROM_THE_PREVIOUS)
              this.w_RES = .F.
            else
              * --- Select from cp_prevpwd
              i_nConn=i_TableProp[this.cp_prevpwd_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.cp_prevpwd_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select top "+cp_ToStrODBC(this.oParentObject.w_MAXPWD)+" CPPSSWRD  from "+i_cTable+" cp_prevpwd ";
                    +" where CPUSRCOD="+cp_ToStrODBC(this.w_CPUSRCOD)+"";
                    +" order by CPROWNUM desc";
                     ,"_Curs_cp_prevpwd")
              else
                select top this.oParentObject.w_MAXPWD CPPSSWRD from (i_cTable);
                 where CPUSRCOD=this.w_CPUSRCOD;
                 order by CPROWNUM desc;
                  into cursor _Curs_cp_prevpwd
              endif
              if used('_Curs_cp_prevpwd')
                select _Curs_cp_prevpwd
                locate for 1=1
                do while not(eof())
                this.w_PWDCHECK = cp_checkpwd(this.oParentObject.w_CODE,this.w_PW, rtrim(_Curs_cp_prevpwd.CPPSSWRD))
                if this.w_PWDCHECK
                  this.w_FIND = .T.
                endif
                  select _Curs_cp_prevpwd
                  continue
                enddo
                use
              endif
              if this.w_FIND
                cp_ErrorMsg(MSG_INSERT_A_DIFFERENT_PWD_FROM_THE_PREVIOUS)
                this.w_RES = .F.
              else
                this.w_RES = .T.
              endif
            endif
            * --- --  Zucchetti Aulla Fine  --
        endcase
        i_retcode = 'stop'
        i_retval = this.w_RES
        return
      case this.pParam="UsrInit"
        if this.oParentObject.w_CODE<>0
          * --- Modifica utente
          * --- Select from cpusers
          i_nConn=i_TableProp[this.cpusers_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select name,language,enabled, userright,cp_login  from "+i_cTable+" cpusers ";
                +" where code="+cp_ToStrODBC(this.oParentObject.w_CODE)+"";
                 ,"_Curs_cpusers")
          else
            select name,language,enabled, userright,cp_login from (i_cTable);
             where code=this.oParentObject.w_CODE;
              into cursor _Curs_cpusers
          endif
          if used('_Curs_cpusers')
            select _Curs_cpusers
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_NAME = nvl(_Curs_cpusers.NAME,"")
            * --- --  Zucchetti Aulla Inizio  --
            * --- Select from POL_UTE
            i_nConn=i_TableProp[this.POL_UTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PUCHGPWD  from "+i_cTable+" POL_UTE ";
                  +" where PUCODUTE="+cp_ToStrODBC(this.oParentObject.w_CODE)+"";
                   ,"_Curs_POL_UTE")
            else
              select PUCHGPWD from (i_cTable);
               where PUCODUTE=this.oParentObject.w_CODE;
                into cursor _Curs_POL_UTE
            endif
            if used('_Curs_POL_UTE')
              select _Curs_POL_UTE
              locate for 1=1
              if not(eof())
              do while not(eof())
              this.oParentObject.w_CHANGEPWD = NVL(_Curs_POL_UTE.PUCHGPWD,"N")
                select _Curs_POL_UTE
                continue
              enddo
              else
                * --- Insert into POL_UTE
                i_nConn=i_TableProp[this.POL_UTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.POL_UTE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PUCODUTE"+",PUDATULT"+",PUNUMTEN"+",PUCHGPWD"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODE),'POL_UTE','PUCODUTE');
                  +","+cp_NullLink(cp_ToStrODBC(i_datsys),'POL_UTE','PUDATULT');
                  +","+cp_NullLink(cp_ToStrODBC(-1),'POL_UTE','PUNUMTEN');
                  +","+cp_NullLink(cp_ToStrODBC("N"),'POL_UTE','PUCHGPWD');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PUCODUTE',this.oParentObject.w_CODE,'PUDATULT',i_datsys,'PUNUMTEN',-1,'PUCHGPWD',"N")
                  insert into (i_cTable) (PUCODUTE,PUDATULT,PUNUMTEN,PUCHGPWD &i_ccchkf. );
                     values (;
                       this.oParentObject.w_CODE;
                       ,i_datsys;
                       ,-1;
                       ,"N";
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
                this.oParentObject.w_CHANGEPWD = "N"
                select _Curs_POL_UTE
              endif
              use
            endif
            this.oParentObject.w_LOGIN = _Curs_cpusers.CP_LOGIN
            this.oParentObject.w_OLDNAME = TRIM(LEFT(this.oParentObject.w_LOGIN,20))
            * --- --  Zucchetti Aulla Fine  --
            this.oParentObject.w_LANGSYS = _Curs_cpusers.LANGUAGE
            this.oParentObject.w_PWD = "__non_definita__"
            this.oParentObject.w_PWDC = "__non_definita__"
            if TYPE("ENABLED")="C"
              this.oParentObject.w_HASENABLED = .T.
            endif
            this.oParentObject.w_RIGHTS = NVL(_Curs_cpusers.USERRIGHT,1)
            this.oParentObject.w_DISABLE = IIF(NVL(_Curs_cpusers.ENABLED, "Y") = "N", "S", "N")
              select _Curs_cpusers
              continue
            enddo
            use
          endif
          * --- Select from cpusers
          i_nConn=i_TableProp[this.cpusers_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select count(*) as NumRec  from "+i_cTable+" cpusers ";
                 ,"_Curs_cpusers")
          else
            select count(*) as NumRec from (i_cTable);
              into cursor _Curs_cpusers
          endif
          if used('_Curs_cpusers')
            select _Curs_cpusers
            locate for 1=1
            do while not(eof())
            * --- Lo zoom � modificabile sono se si � amministratori oppure � presente solo un utente
            if NOT this.oParentObject.w_ISADMIN And NumRec<>1
              this.oParentObject.w_GROUP.Enabled = .F.
            endif
              select _Curs_cpusers
              continue
            enddo
            use
          endif
        else
          * --- Nuovo utente
          * --- Select from cpusers
          i_nConn=i_TableProp[this.cpusers_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MAX(code)+1 as NEWCODE  from "+i_cTable+" cpusers ";
                 ,"_Curs_cpusers")
          else
            select MAX(code)+1 as NEWCODE from (i_cTable);
              into cursor _Curs_cpusers
          endif
          if used('_Curs_cpusers')
            select _Curs_cpusers
            locate for 1=1
            do while not(eof())
            if NOT EMPTY(nvl(NEWCODE,""))
              this.oParentObject.w_CODE = NEWCODE
            else
              this.oParentObject.w_CODE = 1
            endif
              select _Curs_cpusers
              continue
            enddo
            use
          endif
          this.oParentObject.w_NEW = .T.
          * --- --  Zucchetti Aulla Inizio  --
          this.oParentObject.w_NAME = " "
          this.oParentObject.w_LOGIN = " "
          this.oParentObject.w_RIGHTS = 5
          this.oParentObject.w_UTEADMIN = .F.
          this.oParentObject.w_VALIDPWD = iif((this.oParentObject.w_COMPLEX=1 AND this.oParentObject.w_NUMLIM=0),0,2)
          this.oParentObject.w_GROUP.Enabled = .F.
          * --- --  Zucchetti Aulla Fine  --
          this.oParentObject.w_PWD = SPACE(20)
          this.oParentObject.w_PWDC = SPACE(20)
          this.oParentObject.w_LANGSYS = " "
          * --- Select from cpusers
          i_nConn=i_TableProp[this.cpusers_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select enabled  from "+i_cTable+" cpusers ";
                +" where code="+cp_ToStrODBC(this.oParentObject.w_CODE)+"";
                 ,"_Curs_cpusers")
          else
            select enabled from (i_cTable);
             where code=this.oParentObject.w_CODE;
              into cursor _Curs_cpusers
          endif
          if used('_Curs_cpusers')
            select _Curs_cpusers
            locate for 1=1
            do while not(eof())
            if TYPE("cpusers->ENABLE")="C"
              this.oParentObject.w_HASENABLED = .T.
              this.oParentObject.w_DISABLE = .F.
            endif
              select _Curs_cpusers
              continue
            enddo
            use
          endif
        endif
      case this.pParam="UsrCurs"
        * --- Select from cpusrgrp
        i_nConn=i_TableProp[this.cpusrgrp_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select groupcode  from "+i_cTable+" cpusrgrp ";
              +" where usercode="+cp_ToStrODBC(this.oParentObject.w_CODE)+"";
               ,"_Curs_cpusrgrp")
        else
          select groupcode from (i_cTable);
           where usercode=this.oParentObject.w_CODE;
            into cursor _Curs_cpusrgrp
        endif
        if used('_Curs_cpusrgrp')
          select _Curs_cpusrgrp
          locate for 1=1
          do while not(eof())
          UPDATE (this.oParentObject.w_GROUP.cCursor) SET XCHK=1 WHERE code=_Curs_cpusrgrp.groupcode
            select _Curs_cpusrgrp
            continue
          enddo
          use
        endif
        Select (this.oParentObject.w_GROUP.cCursor) 
 Go Top
      case this.pParam="UsrBeforeQ"
        * --- In caso di ordinamento da parte dell'utente, salvo le modifiche allo zoom
        if Used(this.oParentObject.w_GROUP.cCursor)
          Select * from (this.oParentObject.w_GROUP.cCursor) into cursor "SaveMod"
        endif
      case this.pParam="UsrAfterQ"
        * --- Ripristino cursore alle modifiche precedenti la riesecuzione della query
        if Used(this.oParentObject.w_GROUP.cCursor) and Used("SaveMod")
          UPDATE (this.oParentObject.w_GROUP.cCursor) SET XCHK=1 WHERE code in (select code from "SaveMod" where xchk=1)
          Update (this.oParentObject.w_GROUP.cCursor) set XCHK=0 where code in (select code from "SaveMod" where xchk=0)
          use in select ("SaveMod")
          Select (this.oParentObject.w_GROUP.cCursor) 
 Go Top
          this.oParentObject.w_GROUP.grd.Refresh()     
        endif
      case this.pParam="UsrModCode"
        * --- Select from cpusers
        i_nConn=i_TableProp[this.cpusers_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT(*) as COUNT  from "+i_cTable+" cpusers ";
              +" where code="+cp_ToStrODBC(this.oParentObject.w_CODE)+"";
               ,"_Curs_cpusers")
        else
          select COUNT(*) as COUNT from (i_cTable);
           where code=this.oParentObject.w_CODE;
            into cursor _Curs_cpusers
        endif
        if used('_Curs_cpusers')
          select _Curs_cpusers
          locate for 1=1
          do while not(eof())
          if TYPE("COUNT") = "C"
            this.w_COUNT = VAL(COUNT)
          else
            this.w_COUNT = COUNT
          endif
          this.w_RES = (this.w_COUNT=0)
            select _Curs_cpusers
            continue
          enddo
          use
        endif
        if this.w_RES
          this.oParentObject.w_NCODE = this.oParentObject.w_CODE
        else
          this.oParentObject.w_CODE = this.oParentObject.w_NCODE
        endif
      case this.pParam="UsrModLang"
        if NOT EMPTY(this.oParentObject.w_LANGSYS)
          * --- *-- Codice eseguito perch� alla creazione di un ambiente da db vuoto,
          *          la tabella LINGUE (aziendale) non � stata ancora creata
          * --- Verify that the language used is present in the languages 
 Local r, sCode 
 r=.T. 
 If Not Empty( This.oparentobject.w_LANGSYS ) 
 If i_ServerConn[1,2]<>0 
 * --- Zucchetti Aulla Inizio (controllo su tabella lingue dellapplicativo) 
 *=cp_SQL(i_ServerConn[1,2],"select code as exp from cplangs where code="+cp_ToStr(This.oparentobject.w_LANGSYS),"__tmp__") 
 If not this.oparentobject.w_pParam 
 Local i_cTable 
 i_cTable = cp_SetAzi(i_TableProp[cp_OpenTable("LINGUE") ,2]) 
 =cp_SQL(i_ServerConn[1,2],"select LUCODISO as exp from "+i_cTable+" where LUCODISO="+cp_ToStr(This.oparentobject.w_LANGSYS),"__tmp__") 
 Else 
 =cp_SQL(i_ServerConn[1,2],"select code as exp from cplangs where code="+cp_ToStr(This.oparentobject.w_LANGSYS),"__tmp__") 
 Endif 
 * --- Zucchetti Aulla Fine (controllo su tabella lingue dellapplicativo) 
 Else 
 Select Code As Exp From cplangs Where Code=Alltrim(This.oparentobject.w_LANGSYS) And Not(Deleted()) Into Cursor __tmp__ 
 Endif 
 r=(Reccount("__tmp__")>0) 
 sCode=__tmp__.Exp 
 Use In Select("__tmp__") 
 If Not r 
 cp_ErrorMsg(MSG_WRONG_LANGUAGE_CODE) 
 This.oparentobject.w_LANGSYS="" 
 Else 
 This.oparentobject.w_LANGSYS=sCode 
 Endif 
 Endif
        endif
      case this.pParam="UsrOk"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParam="GrpInit"
        if this.oParentObject.w_GCODE<>0
          * --- Read from cpgroups
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.cpgroups_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "name"+;
              " from "+i_cTable+" cpgroups where ";
                  +"code = "+cp_ToStrODBC(this.oParentObject.w_GCODE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              name;
              from (i_cTable) where;
                  code = this.oParentObject.w_GCODE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_GNAME = NVL(cp_ToDate(_read_.name),cp_NullValue(_read_.name))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if TYPE("w_GRPTYPE") = "C"
            this.oParentObject.w_HASGRPTYPE = .T.
          endif
        else
          * --- Select from cpgroups
          i_nConn=i_TableProp[this.cpgroups_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select max(code)+1 as NEWCODE  from "+i_cTable+" cpgroups ";
                 ,"_Curs_cpgroups")
          else
            select max(code)+1 as NEWCODE from (i_cTable);
              into cursor _Curs_cpgroups
          endif
          if used('_Curs_cpgroups')
            select _Curs_cpgroups
            locate for 1=1
            do while not(eof())
            if NOT EMPTY(NEWCODE)
              this.oParentObject.w_GCODE = NEWCODE
            else
              this.oParentObject.w_GCODE = 1
            endif
              select _Curs_cpgroups
              continue
            enddo
            use
          endif
          this.oParentObject.w_NEW = .T.
          this.oParentObject.w_GNAME = " "
          * --- Read from cpgroups
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.cpgroups_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "grptype"+;
              " from "+i_cTable+" cpgroups where ";
                  +"code = "+cp_ToStrODBC(this.oParentObject.w_GCODE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              grptype;
              from (i_cTable) where;
                  code = this.oParentObject.w_GCODE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPO = NVL(cp_ToDate(_read_.grptype),cp_NullValue(_read_.grptype))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if TYPE("w_TIPO") = "C"
            this.oParentObject.w_HASGRPTYPE = .T.
            this.oParentObject.w_GRPTYPE = "G"
          endif
        endif
      case this.pParam="GrpCurs"
        * --- Select from cpusrgrp
        i_nConn=i_TableProp[this.cpusrgrp_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select usercode  from "+i_cTable+" cpusrgrp ";
              +" where groupcode="+cp_ToStrODBC(this.oParentObject.w_GCODE)+"";
               ,"_Curs_cpusrgrp")
        else
          select usercode from (i_cTable);
           where groupcode=this.oParentObject.w_GCODE;
            into cursor _Curs_cpusrgrp
        endif
        if used('_Curs_cpusrgrp')
          select _Curs_cpusrgrp
          locate for 1=1
          do while not(eof())
          UPDATE (this.oParentObject.w_USER.cCursor) SET XCHK=1 WHERE code=_Curs_cpusrgrp.usercode
            select _Curs_cpusrgrp
            continue
          enddo
          use
        endif
        Select (this.oParentObject.w_USER.cCursor) 
 Go Top
      case this.pParam="GrpBeforeQ"
        * --- In caso di ordinamento da parte dell'utente, salvo le modifiche allo zoom
        if Used(this.oParentObject.w_USER.cCursor)
          Select * from (this.oParentObject.w_USER.cCursor) into cursor "SaveMod"
        endif
      case this.pParam="GrpAfterQ"
        * --- Ripristino cursore alle modifiche precedenti la riesecuzione della query
        if Used(this.oParentObject.w_USER.cCursor) and Used("SaveMod")
          UPDATE (this.oParentObject.w_USER.cCursor) SET XCHK=1 WHERE code in (select code from "SaveMod" where xchk=1)
          Update (this.oParentObject.w_USER.cCursor) set XCHK=0 where code in (select code from "SaveMod" where xchk=0)
          use in select ("SaveMod")
          Select (this.oParentObject.w_USER.cCursor) 
 Go Top
          this.oParentObject.w_USER.grd.Refresh()     
        endif
      case this.pParam="GrpModCode"
        * --- Select from cpgroups
        i_nConn=i_TableProp[this.cpgroups_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT(*) as COUNT  from "+i_cTable+" cpgroups ";
              +" where code="+cp_ToStrODBC(this.oParentObject.w_GCODE)+"";
               ,"_Curs_cpgroups")
        else
          select COUNT(*) as COUNT from (i_cTable);
           where code=this.oParentObject.w_GCODE;
            into cursor _Curs_cpgroups
        endif
        if used('_Curs_cpgroups')
          select _Curs_cpgroups
          locate for 1=1
          do while not(eof())
          if TYPE("COUNT") = "C"
            this.w_COUNT = VAL(COUNT)
          else
            this.w_COUNT = COUNT
          endif
          this.w_RES = (this.w_COUNT=0)
            select _Curs_cpgroups
            continue
          enddo
          use
        endif
        if this.w_RES
          this.oParentObject.w_NCODE = this.oParentObject.w_GCODE
        else
          this.oParentObject.w_GCODE = this.oParentObject.w_NCODE
        endif
      case this.pParam="GrpOk"
        this.w_NEWNAME = TRIM(LEFT(this.oParentObject.w_GNAME,20))
        if this.oParentObject.w_NEW
          * --- Insert into cpgroups
          i_nConn=i_TableProp[this.cpgroups_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cpgroups_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"code"+",name"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GCODE),'cpgroups','code');
            +","+cp_NullLink(cp_ToStrODBC(" "),'cpgroups','name');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'code',this.oParentObject.w_GCODE,'name'," ")
            insert into (i_cTable) (code,name &i_ccchkf. );
               values (;
                 this.oParentObject.w_GCODE;
                 ," ";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- --  Zucchetti Aulla Inizio  --
          * --- * --- Lancio il batch della creazione nuovo gruppo, questo batch serve per poter inserire il codice gruppo in
          *     * --- tutte quelle gestioni che lo utilizzano
          GS___BNG(this,this.oParentObject.w_GCODE)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- --  Zucchetti Aulla Fine  --
          this.oParentObject.w_NEW = .F.
        endif
        * --- Write into cpgroups
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.cpgroups_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.cpgroups_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"name ="+cp_NullLink(cp_ToStrODBC(this.w_NEWNAME),'cpgroups','name');
              +i_ccchkf ;
          +" where ";
              +"code = "+cp_ToStrODBC(this.oParentObject.w_GCODE);
                 )
        else
          update (i_cTable) set;
              name = this.w_NEWNAME;
              &i_ccchkf. ;
           where;
              code = this.oParentObject.w_GCODE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.oParentObject.w_HASGRPTYPE
          this.w_GRUPPO = LEFT(TRIM(this.oParentObject.w_GRPTYPE),1)
          * --- Write into cpgroups
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.cpgroups_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpgroups_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.cpgroups_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"grptype ="+cp_NullLink(cp_ToStrODBC(this.w_GRUPPO),'cpgroups','grptype');
                +i_ccchkf ;
            +" where ";
                +"code = "+cp_ToStrODBC(this.oParentObject.w_GCODE);
                   )
          else
            update (i_cTable) set;
                grptype = this.w_GRUPPO;
                &i_ccchkf. ;
             where;
                code = this.oParentObject.w_GCODE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- --  Zucchetti Aulla Inizio  --
        * --- Aggiorno tabella in base alla selezione dei gruppi
        * --- Delete from cpusrgrp
        i_nConn=i_TableProp[this.cpusrgrp_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"groupcode = "+cp_ToStrODBC(this.oParentObject.w_GCODE);
                 )
        else
          delete from (i_cTable) where;
                groupcode = this.oParentObject.w_GCODE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        select (this.oParentObject.w_USER.cCursor)
        GO TOP
        SCAN FOR XCHK=1
        * --- Insert into cpusrgrp
        i_nConn=i_TableProp[this.cpusrgrp_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cpusrgrp_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"usercode"+",groupcode"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(code),'cpusrgrp','usercode');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GCODE),'cpusrgrp','groupcode');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'usercode',code,'groupcode',this.oParentObject.w_GCODE)
          insert into (i_cTable) (usercode,groupcode &i_ccchkf. );
             values (;
               code;
               ,this.oParentObject.w_GCODE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        select (this.oParentObject.w_USER.cCursor)
        ENDSCAN
        * --- * --- Lancio il batch della creazione nuovo gruppo, questo batch serve per poter inserire il codice gruppo in
        *     * --- tutte quelle gestioni che lo utilizzano
        GS___BNG(this,this.oParentObject.w_GCODE, .T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oparentobject.oparentobject.NotifyEvent("Aggiorna")
        * --- --  Zucchetti Aulla Fine  --
        this.oparentobject.ecpquit()
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --  Zucchetti Aulla Inizio  --
    if empty(nvl(this.oParentObject.w_LOGIN," ")) and this.oParentObject.w_CODUTE<>"S"
      this.oParentObject.w_LOGIN = ALLTRIM(STR(this.oParentObject.w_CODE))
    endif
    if Not Empty(Nvl(this.oParentObject.w_LOGIN,""))
      this.w_LOG = TRIM(LEFT(this.oParentObject.w_LOGIN,20))
      * --- Select from cpusers
      i_nConn=i_TableProp[this.cpusers_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT(*) as COUNT  from "+i_cTable+" cpusers ";
            +" where cp_login="+cp_ToStrODBC(this.w_LOG)+"";
             ,"_Curs_cpusers")
      else
        select COUNT(*) as COUNT from (i_cTable);
         where cp_login=this.w_LOG;
          into cursor _Curs_cpusers
      endif
      if used('_Curs_cpusers')
        select _Curs_cpusers
        locate for 1=1
        do while not(eof())
        this.w_LRECCOUNT = NVL(COUNT,0)
          select _Curs_cpusers
          continue
        enddo
        use
      endif
      if this.w_LRECCOUNT<>0 AND (this.w_LOG<>this.oParentObject.w_OLDNAME OR this.oParentObject.w_NEW) AND this.oParentObject.w_CODUTE="S"
        cp_ErrorMsg(MSG_USER_LOGIN_ALREADY_EXISTING)
      else
        * --- --  Zucchetti Aulla Fine  --
        if this.oParentObject.w_NEW
          * --- Insert into cpusers
          i_nConn=i_TableProp[this.cpusers_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cpusers_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"code"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODE),'cpusers','code');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'code',this.oParentObject.w_CODE)
            insert into (i_cTable) (code &i_ccchkf. );
               values (;
                 this.oParentObject.w_CODE;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- --  Zucchetti Aulla Inizio  --
          *     * --- Lancio il batch della craezione nuovo utente, questo batch serve per poter inserire il codice utente in
          *     * --- tutte quelle gestioni che lo utilizzano
          GS___BNU(this,this.oParentObject.w_CODE)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- * Selezione delle aziende in cui abilitare l'utente
          GS___KNU(ALLTRIM(STR(this.oParentObject.w_CODE))+"*"+ALLTRIM(this.oParentObject.w_NAME))
          * --- --  Zucchetti Aulla Fine  --
          this.oParentObject.w_NEW = .F.
        endif
        this.w_UN = trim(left(this.oParentObject.w_NAME,20))
        this.w_PW = trim(left(this.oParentObject.w_PWD,20))
        this.w_LOG = trim(left(this.oParentObject.w_LOGIN,20))
        if TYPE("this.oparentobject.w_SOURCE")="U"
          this.w_LANG = NVL(trim(this.oParentObject.w_LANGSYS),"")
        endif
        if this.w_PW="__non_definita__" AND TYPE("this.oparentobject.w_SOURCE")="U"
          * --- * Zucchetti Aulla Monitor Framework
          * --- Write into cpusers
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.cpusers_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.cpusers_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"name ="+cp_NullLink(cp_ToStrODBC(this.w_UN),'cpusers','name');
            +",language ="+cp_NullLink(cp_ToStrODBC(this.w_LANG),'cpusers','language');
            +",cp_login ="+cp_NullLink(cp_ToStrODBC(this.w_LOG),'cpusers','cp_login');
            +",userright ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RIGHTS),'cpusers','userright');
                +i_ccchkf ;
            +" where ";
                +"code = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                   )
          else
            update (i_cTable) set;
                name = this.w_UN;
                ,language = this.w_LANG;
                ,cp_login = this.w_LOG;
                ,userright = this.oParentObject.w_RIGHTS;
                &i_ccchkf. ;
             where;
                code = this.oParentObject.w_CODE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if this.oParentObject.w_CODE=i_codute
            =cp_set_get_right(.T.)
          endif
        else
          this.w_PW = cp_CriptPwd(this.oParentObject.w_CODE,this.w_PW)
          * --- --  Zucchetti Aulla Inizio  --
          * --- Select from cp_prevpwd
          i_nConn=i_TableProp[this.cp_prevpwd_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cp_prevpwd_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select max(CPROWNUM)+1 as MAX  from "+i_cTable+" cp_prevpwd ";
                +" where CPUSRCOD="+cp_ToStrODBC(this.w_CPUSRCOD)+"";
                 ,"_Curs_cp_prevpwd")
          else
            select max(CPROWNUM)+1 as MAX from (i_cTable);
             where CPUSRCOD=this.w_CPUSRCOD;
              into cursor _Curs_cp_prevpwd
          endif
          if used('_Curs_cp_prevpwd')
            select _Curs_cp_prevpwd
            locate for 1=1
            do while not(eof())
            this.w_MAXROW = NVL(MAX,1)
              select _Curs_cp_prevpwd
              continue
            enddo
            use
          endif
          * --- Aggiorno tabella utenti
          if this.w_MAXROW>this.oParentObject.w_MAXPWD AND this.oParentObject.w_MAXPWD>0
            * --- Try
            local bErr_04C862C8
            bErr_04C862C8=bTrsErr
            this.Try_04C862C8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04C862C8
            * --- End
          endif
          * --- Insert into CP_PREVPWD
          i_nConn=i_TableProp[this.CP_PREVPWD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CP_PREVPWD_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CP_PREVPWD_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CPUSRCOD"+",CPROWNUM"+",CPPSSWRD"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CPUSRCOD),'CP_PREVPWD','CPUSRCOD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAXROW),'CP_PREVPWD','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PW),'CP_PREVPWD','CPPSSWRD');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CPUSRCOD',this.w_CPUSRCOD,'CPROWNUM',this.w_MAXROW,'CPPSSWRD',this.w_PW)
            insert into (i_cTable) (CPUSRCOD,CPROWNUM,CPPSSWRD &i_ccchkf. );
               values (;
                 this.w_CPUSRCOD;
                 ,this.w_MAXROW;
                 ,this.w_PW;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          if TYPE("this.oparentobject.w_SOURCE")<>"U"
            * --- Non sovrascrive gli userrights se viene chiamato dal conferma password all'accesso
            * --- Write into cpusers
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.cpusers_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.cpusers_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"name ="+cp_NullLink(cp_ToStrODBC(this.w_UN),'cpusers','name');
              +",passwd ="+cp_NullLink(cp_ToStrODBC(this.w_PW),'cpusers','passwd');
              +",language ="+cp_NullLink(cp_ToStrODBC(this.w_LANG),'cpusers','language');
              +",cp_login ="+cp_NullLink(cp_ToStrODBC(this.w_LOG),'cpusers','cp_login');
                  +i_ccchkf ;
              +" where ";
                  +"code = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                     )
            else
              update (i_cTable) set;
                  name = this.w_UN;
                  ,passwd = this.w_PW;
                  ,language = this.w_LANG;
                  ,cp_login = this.w_LOG;
                  &i_ccchkf. ;
               where;
                  code = this.oParentObject.w_CODE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- --  Zucchetti Aulla Fine  --
            * --- Write into cpusers
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.cpusers_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.cpusers_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"name ="+cp_NullLink(cp_ToStrODBC(this.w_UN),'cpusers','name');
              +",passwd ="+cp_NullLink(cp_ToStrODBC(this.w_PW),'cpusers','passwd');
              +",language ="+cp_NullLink(cp_ToStrODBC(this.w_LANG),'cpusers','language');
              +",cp_login ="+cp_NullLink(cp_ToStrODBC(this.w_LOG),'cpusers','cp_login');
              +",userright ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RIGHTS),'cpusers','userright');
                  +i_ccchkf ;
              +" where ";
                  +"code = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                     )
            else
              update (i_cTable) set;
                  name = this.w_UN;
                  ,passwd = this.w_PW;
                  ,language = this.w_LANG;
                  ,cp_login = this.w_LOG;
                  ,userright = this.oParentObject.w_RIGHTS;
                  &i_ccchkf. ;
               where;
                  code = this.oParentObject.w_CODE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- --  Zucchetti Aulla Inizio  --
          endif
          * --- --  Zucchetti Aulla Fine  --
          * --- Inserisco la password nella tabella contenente le password precedenti
          * --- Delete from cpssomap
          i_nConn=i_TableProp[this.cpssomap_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpssomap_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"userid = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                   )
          else
            delete from (i_cTable) where;
                  userid = this.oParentObject.w_CODE;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          if this.oParentObject.w_CODE=i_codute
            =cp_set_get_right(.T.)
          endif
        endif
        * --- --  Zucchetti Aulla Inizio  --
        if TYPE("this.oparentobject.w_SOURCE")="U"
          * --- Write into POL_UTE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.POL_UTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PUCHGPWD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CHANGEPWD),'POL_UTE','PUCHGPWD');
                +i_ccchkf ;
            +" where ";
                +"PUCODUTE = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                   )
          else
            update (i_cTable) set;
                PUCHGPWD = this.oParentObject.w_CHANGEPWD;
                &i_ccchkf. ;
             where;
                PUCODUTE = this.oParentObject.w_CODE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiorno tabella in base alla selezione dei gruppi
          * --- Delete from cpusrgrp
          i_nConn=i_TableProp[this.cpusrgrp_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"usercode = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                   )
          else
            delete from (i_cTable) where;
                  usercode = this.oParentObject.w_CODE;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          select (this.oParentObject.w_GROUP.cCursor)
          GO TOP
          SCAN FOR XCHK=1
          * --- Insert into cpusrgrp
          i_nConn=i_TableProp[this.cpusrgrp_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cpusrgrp_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"usercode"+",groupcode"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODE),'cpusrgrp','usercode');
            +","+cp_NullLink(cp_ToStrODBC(code),'cpusrgrp','groupcode');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'usercode',this.oParentObject.w_CODE,'groupcode',code)
            insert into (i_cTable) (usercode,groupcode &i_ccchkf. );
               values (;
                 this.oParentObject.w_CODE;
                 ,code;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          select (this.oParentObject.w_GROUP.cCursor)
          ENDSCAN
        endif
        * --- --  Zucchetti Aulla Fine  --
        if TYPE("w_HASENABLED")<>"U" AND this.oParentObject.w_HASENABLED
          this.w_ENABLED = IIF(this.oParentObject.w_DISABLE="S","N"," ")
          * --- Write into cpusers
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.cpusers_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.cpusers_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"enabled ="+cp_NullLink(cp_ToStrODBC(this.w_ENABLED),'cpusers','enabled');
                +i_ccchkf ;
            +" where ";
                +"code = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                   )
          else
            update (i_cTable) set;
                enabled = this.w_ENABLED;
                &i_ccchkf. ;
             where;
                code = this.oParentObject.w_CODE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- --  Zucchetti Aulla Inizio  --
        *     * --- Lancio il batch della craezione nuovo utente, questo batch serve per poter inserire il codice utente in
        *     * --- tutte quelle gestioni che lo utilizzano
        GS___BNU(this,this.oParentObject.w_CODE, .T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if TYPE("this.oparentobject.w_SOURCE")="U"
          this.oparentobject.oparentobject.NotifyEvent("Aggiorna")
        else
          this.oParentObject.w_CHANGED = .T.
          * --- * --- Vengono aggiornate le variabili dell'anchestor(GS___UTE) perch� la mReplace viene fatta prima dell'esecuzione del batch
          this.oparentobject.oparentobject.w_CHANGED=this.oParentObject.w_CHANGED
          this.oparentobject.oparentobject.w_PWD=this.oParentObject.w_PWD
        endif
        * --- --  Zucchetti Aulla Fine  --
      endif
      * --- --  Zucchetti Aulla Inizio  --
    endif
    * --- --  Zucchetti Aulla Fine  --
  endproc
  proc Try_04C862C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from cp_prevpwd
    i_nConn=i_TableProp[this.cp_prevpwd_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cp_prevpwd_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CPUSRCOD = "+cp_ToStrODBC(this.w_CPUSRCOD);
            +" and CPROWNUM <= "+cp_ToStrODBC(this.w_MAXROW-this.oParentObject.w_MAXPWD);
             )
    else
      delete from (i_cTable) where;
            CPUSRCOD = this.w_CPUSRCOD;
            and CPROWNUM <= this.w_MAXROW-this.oParentObject.w_MAXPWD;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='UTE_AZI'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='cpusrgrp'
    this.cWorkTables[4]='cpprgsec'
    this.cWorkTables[5]='cpssomap'
    this.cWorkTables[6]='cpgroups'
    this.cWorkTables[7]='POL_SIC'
    this.cWorkTables[8]='CP_PREVPWD'
    this.cWorkTables[9]='cp_prevpwd'
    this.cWorkTables[10]='POL_UTE'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_cp_prevpwd')
      use in _Curs_cp_prevpwd
    endif
    if used('_Curs_cpusers')
      use in _Curs_cpusers
    endif
    if used('_Curs_POL_UTE')
      use in _Curs_POL_UTE
    endif
    if used('_Curs_cpusers')
      use in _Curs_cpusers
    endif
    if used('_Curs_cpusers')
      use in _Curs_cpusers
    endif
    if used('_Curs_cpusers')
      use in _Curs_cpusers
    endif
    if used('_Curs_cpusrgrp')
      use in _Curs_cpusrgrp
    endif
    if used('_Curs_cpusers')
      use in _Curs_cpusers
    endif
    if used('_Curs_cpgroups')
      use in _Curs_cpgroups
    endif
    if used('_Curs_cpusrgrp')
      use in _Curs_cpusrgrp
    endif
    if used('_Curs_cpgroups')
      use in _Curs_cpgroups
    endif
    if used('_Curs_cpusers')
      use in _Curs_cpusers
    endif
    if used('_Curs_cp_prevpwd')
      use in _Curs_cp_prevpwd
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
