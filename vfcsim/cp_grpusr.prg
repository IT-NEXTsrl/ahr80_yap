* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_grpusr                                                       *
*              Gestione Gruppi/Utenti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-09-24                                                      *
* Last revis.: 2014-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_grpusr",oParentObject))

* --- Class definition
define class tcp_grpusr as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 716
  Height = 420
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-17"
  HelpContextID=59189547
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_grpusr"
  cComment = "Gestione Gruppi/Utenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ISADMIN = .F.
  w_CODE = 0
  w_NAME = space(30)
  w_NEW = .F.
  w_GCODE = 0
  w_GNAME = space(30)
  w_CHANGE = .F.
  w_GRPTYPE = space(1)
  w_pParam = .F.
  w_USERS = .F.
  w_Utenti = .NULL.
  w_Gruppi = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_grpusrPag1","cp_grpusr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Utenti = this.oPgFrm.Pages(1).oPag.Utenti
    this.w_Gruppi = this.oPgFrm.Pages(1).oPag.Gruppi
    DoDefault()
    proc Destroy()
      this.w_Utenti = .NULL.
      this.w_Gruppi = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- cp_grpusr
    BINDEVENT(this.w_Utenti.grd.column1.text1, 'Drag',this, 'cp_MouseMoveUsr' )
    BINDEVENT(this.w_Utenti.grd.column2.text1, 'Drag',this, 'cp_MouseMoveUsr' )
    BINDEVENT(this.w_Utenti.grd.column3.text1, 'Drag',this, 'cp_MouseMoveUsr' )
    BINDEVENT(this.w_Gruppi.grd.column1.text1, 'Drag',this, 'cp_MouseMoveGrp' )
    BINDEVENT(this.w_Gruppi.grd.column2.text1, 'Drag',this, 'cp_MouseMoveGrp' )
    BINDEVENT(this.w_Gruppi.grd.column3.text1, 'Drag',this, 'cp_MouseMoveGrp' )
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ISADMIN=.f.
      .w_CODE=0
      .w_NAME=space(30)
      .w_NEW=.f.
      .w_GCODE=0
      .w_GNAME=space(30)
      .w_CHANGE=.f.
      .w_GRPTYPE=space(1)
      .w_pParam=.f.
      .w_USERS=.f.
        .w_ISADMIN = cp_IsAdministrator()
        .w_CODE = .w_utenti.getvar('code')
        .w_NAME = .w_utenti.getvar('name')
      .oPgFrm.Page1.oPag.Utenti.Calculate()
      .oPgFrm.Page1.oPag.Gruppi.Calculate()
          .DoRTCalc(4,4,.f.)
        .w_GCODE = .w_gruppi.getvar('code')
        .w_GNAME = .w_gruppi.getvar('name')
        .w_CHANGE = .F.
        .w_GRPTYPE = .w_gruppi.getvar('grptype')
        .w_pParam = nvl(oparentobject,.T.)
    endwith
    this.DoRTCalc(10,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_grpusr
    endproc
    Proc CP_MouseMoveUsr(nButton, nShift, nXCoord, nYCoord)
        DIMENSION aSourceEvent(3)
        AEVENTS(aSourceEvent,0)
        if lower(aSourceEvent(1).DragIcon)==lower(fullpath(i_cBmpPath+"dragmove.cur"))
          create cursor _cpusers_ (Code N(6), Name C(30))
          insert into _cpusers_ (Code, Name) values(this.w_code, this.w_name)
        endif
        IF aSourceEvent(1).name<>'STDSUSR'
          aSourceEvent(1).name='STDSUSR'
        ENDIF
        Return
        
    Proc CP_MouseMoveGrp(nButton, nShift, nXCoord, nYCoord)
        DIMENSION aSourceEvent(3)
        AEVENTS(aSourceEvent,0)
        if lower(aSourceEvent(1).DragIcon)==lower(fullpath(i_cBmpPath+"dragmove.cur"))
          create cursor _cpgroups_ (Code N(6), Name C(30))
          insert into _cpgroups_ (Code, Name) values(this.w_gcode, this.w_gname)
        endif
        if aSourceEvent(1).name<>'STDSGRP'
          aSourceEvent(1).name='STDSGRP'  
        endif
        Return
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CODE = .w_utenti.getvar('code')
            .w_NAME = .w_utenti.getvar('name')
        .oPgFrm.Page1.oPag.Utenti.Calculate()
        .oPgFrm.Page1.oPag.Gruppi.Calculate()
        .DoRTCalc(4,4,.t.)
            .w_GCODE = .w_gruppi.getvar('code')
            .w_GNAME = .w_gruppi.getvar('name')
        .DoRTCalc(7,7,.t.)
            .w_GRPTYPE = .w_gruppi.getvar('grptype')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Utenti.Calculate()
        .oPgFrm.Page1.oPag.Gruppi.Calculate()
    endwith
  return

  proc Calculate_GKBDGBQVZL()
    with this
          * --- Chiusura cursori
          cp_grpusr_b(this;
              ,'CloseCursor';
             )
    endwith
  endproc
  proc Calculate_OGNZECPFLF()
    with this
          * --- Doppio click gruppi
          cp_grpusr_b(this;
              ,'G\U GrpMod';
             )
    endwith
  endproc
  proc Calculate_SYKTLZOTLX()
    with this
          * --- Doppio click utenti
          cp_grpusr_b(this;
              ,'G\U UsrMod';
             )
    endwith
  endproc
  proc Calculate_WINUTWVOCR()
    with this
          * --- Doppio click gruppi
          cp_grpusr_b(this;
              ,'G\U Init';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Utenti.Event(cEvent)
      .oPgFrm.Page1.oPag.Gruppi.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_GKBDGBQVZL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Gruppi selected")
          .Calculate_OGNZECPFLF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Utenti selected")
          .Calculate_SYKTLZOTLX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_WINUTWVOCR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_grpusrPag1 as StdContainer
  Width  = 712
  height = 420
  stdWidth  = 712
  stdheight = 420
  resizeXpos=338
  resizeYpos=362
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_2 as StdButton with uid="JRQUBSOMML",left=604, top=382, width=94,height=25,;
    caption="Ok", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 58729531;
    , Caption=MSG_OK_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="CZICQCULRY",left=256, top=23, width=94,height=24,;
    caption="Nuovo utente", nPag=1;
    , ToolTipText = "Creazione nuovo utente";
    , HelpContextID = 169527162;
    , Caption=MSG_NEW_USER;
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        cp_grpusr_b(this.Parent.oContained,"G\U UsrNew")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ISADMIN Or Not .w_USERS)
      endwith
    endif
  endfunc


  add object oBtn_1_4 as StdButton with uid="QNKQKINWQE",left=256, top=53, width=94,height=24,;
    caption="Modifica", nPag=1;
    , ToolTipText = "Modifica impostazioni utente";
    , HelpContextID = 109830088;
    , Caption=MSG_CHANGE_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        cp_grpusr_b(this.Parent.oContained,"G\U UsrMod")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="PPIDRPTNIN",left=256, top=84, width=94,height=24,;
    caption="Elimina", nPag=1;
    , ToolTipText = "Elimina utente";
    , HelpContextID = 54992346;
    , Caption=MSG_DELETE_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_5.Click()
      with this.Parent.oContained
        cp_grpusr_b(this.Parent.oContained,"G\U UsrCanc")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ISADMIN Or Not .w_USERS)
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="DOZBIBKOFR",left=256, top=162, width=94,height=24,;
    caption="Traduci", nPag=1;
    , ToolTipText = "Premere per gestire le traduzioni dell'installazione";
    , HelpContextID = 136401198;
    , Caption=MSG_TRANSLATE;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        cp_etrad()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ISADMIN Or Not .w_USERS)
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="TENQDVYJPU",left=604, top=23, width=94,height=24,;
    caption="Nuovo Gruppo", nPag=1;
    , ToolTipText = "Creazione nuovo gruppo";
    , HelpContextID = 179762988;
    , Caption=MSG_NEW_GROUP;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        cp_grpusr_b(this.Parent.oContained,"G\U GrpNew")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ISADMIN Or Not .w_USERS)
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="GDEEZZTZKD",left=604, top=53, width=94,height=24,;
    caption="Modifica", nPag=1;
    , ToolTipText = "Modifica impostazioni gruppo";
    , HelpContextID = 109830088;
    , Caption=MSG_CHANGE_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        cp_grpusr_b(this.Parent.oContained,"G\U GrpMod")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ISADMIN Or Not .w_USERS)
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="YPEZJTNPVG",left=604, top=84, width=94,height=24,;
    caption="Elimina", nPag=1;
    , ToolTipText = "Elimina gruppo";
    , HelpContextID = 54992346;
    , Caption=MSG_DELETE_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        cp_grpusr_b(this.Parent.oContained,"G\U GrpCanc")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ISADMIN Or Not .w_USERS)
      endwith
    endif
  endfunc


  add object Utenti as cp_zoombox with uid="DKNXILYDYH",left=5, top=13, width=241,height=401,;
    caption='Utenti',;
   bGlobalFont=.t.,;
    bNoZoomGridShape=.f.,cZoomFile="CP_GRPUSR",bOptions=.f.,bAdvOptions=.t.,bQueryOnLoad=.t.,bReadOnly=.t.,bRetriveAllRows=.f.,cTable="CPUSERS",cZoomOnZoom="",cMenuFile="",bQueryOnDblClick=.t.,;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 138055227


  add object Gruppi as cp_zoombox with uid="ADMPHRNDHF",left=357, top=13, width=241,height=401,;
    caption='Gruppi',;
   bGlobalFont=.t.,;
    bNoZoomGridShape=.f.,cZoomFile="CP_GRPUSR",bOptions=.f.,bAdvOptions=.t.,bQueryOnLoad=.t.,bReadOnly=.t.,bRetriveAllRows=.f.,cTable="CPGROUPS",cZoomOnZoom="",cMenuFile="",bQueryOnDblClick=.t.,;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 202030139

  add object oStr_1_22 as StdString with uid="OCWOZKFTPX",Visible=.t., Left=20, Top=4,;
    Alignment=0, Width=32, Height=18,;
    Caption="Utenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="XTXYLIVXNW",Visible=.t., Left=370, Top=4,;
    Alignment=0, Width=37, Height=18,;
    Caption="Gruppi"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_grpusr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
