* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_secctrl                                                      *
*              Richiesta sblocco procedura                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-12                                                      *
* Last revis.: 2011-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_secctrl
= cp_ReadXdc()
* --- Fine Area Manuale
return(createobject("tcp_secctrl",oParentObject))

* --- Class definition
define class tcp_secctrl as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 648
  Height = 132
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-11-14"
  HelpContextID=107176555
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  cpusrgrp_IDX = 0
  cPrg = "cp_secctrl"
  cComment = "Richiesta sblocco procedura"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_tablename = space(50)
  w_TBCOMMENT = space(60)
  w_CURFORM = space(50)
  * --- Area Manuale = Declare Variables
  * --- cp_secctrl
  * --- Centro la finestra
  Autocenter=.t.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_secctrlPag1","cp_secctrl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_secctrl
    * --- Translate interface...
     this.parent.cComment=cp_Translate(MSG_REQUEST_UNBLOCK_PROCEDURE)
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='cpusrgrp'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_tablename=space(50)
      .w_TBCOMMENT=space(60)
      .w_CURFORM=space(50)
        .w_tablename = iif( vartype(oparentobject)='C', left(oParentObject,at('@',oParentObject)-1) ,'')
        .w_TBCOMMENT = I_DCX.GETTABLEDEscr(I_DCX.gETTABLEIDX( Alltrim( .w_tablename ) ))
        .w_CURFORM = IIF( VARTYPE(oParentObject)='C' , substr(oParentObject, at('@',oParentObject)+1, at('@',oParentObject,2)-at('@',oParentObject)-1), '' )
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_FEATURE_NOT_ACCESSIBLE_WITH_RESTRICTED_DATA)
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_TABLE_WITH_ACCESS_FILTER_UNABLE_TO_ACCESS)
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TABLE_WITH_ACCESS_FILTER+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_MANAGEMENT_REQUEST+MSG_FS)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- cp_secctrl
    w_checkadmin=.t.
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_FEATURE_NOT_ACCESSIBLE_WITH_RESTRICTED_DATA)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_TABLE_WITH_ACCESS_FILTER_UNABLE_TO_ACCESS)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TABLE_WITH_ACCESS_FILTER+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_MANAGEMENT_REQUEST+MSG_FS)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_FEATURE_NOT_ACCESSIBLE_WITH_RESTRICTED_DATA)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_TABLE_WITH_ACCESS_FILTER_UNABLE_TO_ACCESS)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TABLE_WITH_ACCESS_FILTER+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_MANAGEMENT_REQUEST+MSG_FS)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.otablename_1_3.value==this.w_tablename)
      this.oPgFrm.Page1.oPag.otablename_1_3.value=this.w_tablename
    endif
    if not(this.oPgFrm.Page1.oPag.oTBCOMMENT_1_4.value==this.w_TBCOMMENT)
      this.oPgFrm.Page1.oPag.oTBCOMMENT_1_4.value=this.w_TBCOMMENT
    endif
    if not(this.oPgFrm.Page1.oPag.oCURFORM_1_7.value==this.w_CURFORM)
      this.oPgFrm.Page1.oPag.oCURFORM_1_7.value=this.w_CURFORM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_secctrlPag1 as StdContainer
  Width  = 644
  height = 132
  stdWidth  = 644
  stdheight = 132
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object otablename_1_3 as StdField with uid="NRBQWSBWLH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_tablename", cQueryName = "",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 204210195,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=200, Top=50, InputMask=replicate('X',50)

  add object oTBCOMMENT_1_4 as StdField with uid="GWXCAUQGAN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TBCOMMENT", cQueryName = "TBCOMMENT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 48640206,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=421, Top=50, InputMask=replicate('X',60)

  add object oCURFORM_1_7 as StdField with uid="PELIPIZVZE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CURFORM", cQueryName = "CURFORM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 183094857,;
   bGlobalFont=.t.,;
    Height=21, Width=437, Left=200, Top=75, InputMask=replicate('X',50)


  add object oBtn_1_8 as StdButton with uid="JWIBHYOYEX",left=587, top=103, width=50,height=20,;
    caption="Esci", nPag=1;
    , HelpContextID = 155470997;
    , Caption=MSG_S_EXIT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_9 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=7, top=138, width=284,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="La funzionalit� non � accessibile da utenti con restrizione nei dati.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 94629129


  add object oObj_1_10 as cp_setCtrlObjprop with uid="LIFPFMELWQ",left=7, top=164, width=284,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Sulla tabella sono stati definiti accessi con filtro: impossibile accedere.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 94629129


  add object oObj_1_11 as cp_setCtrlObjprop with uid="DXBAZXZQNK",left=7, top=190, width=284,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tabella con accesso bloccato:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 94629129


  add object oObj_1_12 as cp_setCtrlObjprop with uid="MPPQQAAGHO",left=7, top=216, width=284,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Gestione richiesta:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 94629129

  add object oStr_1_1 as StdString with uid="LJAXTAYSPR",Visible=.t., Left=14, Top=7,;
    Alignment=2, Width=621, Height=19,;
    Caption="La funzionalit� non � accessibile da utenti con restrizione nei dati."  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_2 as StdString with uid="GLROYFNAJU",Visible=.t., Left=14, Top=25,;
    Alignment=2, Width=621, Height=19,;
    Caption="Sulla tabella sono stati definiti accessi con filtro: impossibile accedere."  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="SKIKRKJNVZ",Visible=.t., Left=4, Top=50,;
    Alignment=1, Width=189, Height=18,;
    Caption="Tabella con accesso bloccato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="BSYHUWLRWV",Visible=.t., Left=14, Top=75,;
    Alignment=1, Width=179, Height=18,;
    Caption="Gestione richiesta:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_secctrl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
