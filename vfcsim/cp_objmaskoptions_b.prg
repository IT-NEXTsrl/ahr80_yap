* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_objmaskoptions_b                                             *
*              Batch opzioni maschera                                          *
*                                                                              *
*      Author: Nicol� Mercanti                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-16                                                      *
* Last revis.: 2017-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,pPage,pSelect
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_objmaskoptions_b",oParentObject,m.pOper,m.pPage,m.pSelect)
return(i_retval)

define class tcp_objmaskoptions_b as StdBatch
  * --- Local variables
  pOper = space(20)
  pPage = 0
  pSelect = space(1)
  w_COUNT = 0
  cKey = space(40)
  cName = space(40)
  cClass = space(10)
  oObject = .NULL.
  cObjectClass = space(10)
  nIndex = 0
  cArch = space(10)
  cSlot = space(10)
  oObjProp = .NULL.
  bTrovato = .f.
  bTrovato2 = .f.
  nIndClass = 0
  nIndClass2 = 0
  nCorrispPage = 0
  cClassCnf = space(10)
  cFile = space(300)
  * --- WorkFile variables
  cpttbls_idx=0
  objmaskoptions_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch opzioni disegnatore di maschera, viene lanciato da CP_OBJMASKOPTIONS
    * --- La lettura e gli assegnamenti vengono fatti tramite un cursore che contiene la corrispondenza propriet� oggetto-campo maschera
    *         Viene inizializzato nell'area manuale della maschera chiamante
    * --- Parametro operazione
    * --- Parametro pagina
    * --- Parametro selezione (utilizzato solo dal Sel.\Desel. tutti)
    * --- Array utilizzato per la creazione e la selezione degli zoom
    DIMENSION aZoom[7] 
 aZoom[1]="lbl" 
 aZoom[2]="txt" 
 aZoom[3]="combo" 
 aZoom[4]="check" 
 aZoom[5]="btn" 
 aZoom[6]="line" 
 aZoom[7]="bitmap"
    * --- Array con i nomi delle classi visualizzate nello zoom
    DIMENSION aClassInZoom[9] 
 aClassInZoom[1]="Label" 
 aClassInZoom[2]="Text" 
 aClassInZoom[3]="Radio" 
 aClassInZoom[4]="Combo" 
 aClassInZoom[5]="Check" 
 aClassInZoom[6]="Button" 
 aClassInZoom[7]="Mediabutton" 
 aClassInZoom[8]="Linenode" 
 aClassInZoom[9]="Bitmap"
    * --- Array con la corrispondenza di indici tra aClassInZoom e aZoom
    *        Se i due array vengono aggiornati, deve essere aggiornato assolutamente anche questo
    DIMENSION aCorrisp[9] 
 aCorrisp[1]=1 
 aCorrisp[2]=2 
 aCorrisp[3]=3 
 aCorrisp[4]=3 
 aCorrisp[5]=4 
 aCorrisp[6]=5 
 aCorrisp[7]=5 
 aCorrisp[8]=6 
 aCorrisp[9]=7
    do case
      case this.pOper="Init"
        For this.w_COUNT=1 to ALEN(aClassInZoom)
        this.cObjectClass = aClassInZoom[this.w_COUNT]
        this.objmaskoptions_idx=cp_gettabledefidx("objmaskoptions")
        * --- Select from objmaskoptions
        i_nConn=i_TableProp[this.objmaskoptions_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.objmaskoptions_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select count(*) as NumRec  from "+i_cTable+" objmaskoptions ";
              +" where oclass = "+cp_ToStrODBC(this.cObjectClass)+"";
               ,"_Curs_objmaskoptions")
        else
          select count(*) as NumRec from (i_cTable);
           where oclass = this.cObjectClass;
            into cursor _Curs_objmaskoptions
        endif
        if used('_Curs_objmaskoptions')
          select _Curs_objmaskoptions
          locate for 1=1
          do while not(eof())
          * --- In base alla classe dell'oggetto dello zoom corrente, recupero la pagina da attivare
          nLoop = 1
          this.bTrovato = .F.
          do while nLoop <= ALEN(aClassInZoom) AND NOT this.bTrovato
            if aClassInZoom[nLoop] = this.cObjectClass
              nNumPage=aCorrisp[nLoop]
              cNamePage=ALLTRIM(STR(nNumPage))
              this.bTrovato = .T.
            endif
            nLoop = nLoop+1
          enddo
          this.oParentObject.w_HIDE&cNamePage = IIF (this.oParentObject.w_HIDE&cNamePage = 2,0,this.oParentObject.w_HIDE&cNamePage)
          if NumRec=1
            * --- Non visualizzo i flag con un solo record
            this.oParentObject.w_HIDE&cNamePage = this.oParentObject.w_HIDE&cNamePage +1
          else
            if NumRec>1
              * --- Flag visibili
              this.oParentObject.w_HIDE&cNamePage = 3
            endif
          endif
          * --- Se non vengono rispettati gli IF la pagina � disabilitata
            select _Curs_objmaskoptions
            continue
          enddo
          use
        endif
        Next
        Use in Select ("CurAlias")
        * --- La prima pagina da visualizzare � quella relativa all'oggetto selezionato
        * --- pParam viene passato alla maschera dall'oggetto selezionato, contiene il numero della pagina da visualizzare
        cNamePage = aZoom [pParam]
        this.oObject = this.oParentObject.w_z&cNamePage
        Select (this.oObject.cCursor) 
 Go Top
        if pParam <> 1
          this.oParentObject.oPgFrm.ActivePage = pParam
        else
          this.oParentObject.NotifyEvent("ActivatePage 1")
        endif
        this.oParentObject.w_FROMPAGE = iif(empty(this.oParentObject.w_FROMPAGE),pParam,this.oParentObject.w_FROMPAGE)
        * --- Variabile caller contenente il file di configurazione 
        this.oParentObject.w_CNFFILE = this.oParentObject.oParentObject.parent.parent.cCnfFile
      case this.pOper="Zoom"
        * --- Creazione tabella temporanea in base agli oggetti selezionati
        * --- Create temporary table objmaskoptions
        i_nIdx=cp_AddTableDef('objmaskoptions') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.cpttbls_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"cast(' ' as char(10)) as objkey, cast(' ' as char(254)) as name,cast(' ' as char(15)) as oclass "," from "+i_cTable;
              +" where 1=0";
              )
        this.objmaskoptions_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Come primo record inserisco l'oggetto selezionato nella tavola di disegno
        this.cClass = this.oParentObject.oParentObject.class
        this.cClass = substr(this.cClass,rat("_",this.cClass)+1,len(this.cClass))
        * --- Modifico il nome della classe per la visualizzazione nello zoom
        this.cClass = ALLTRIM(icase(this.cClass="lbl",aClassInZoom[1],this.cClass="txt",aClassInZoom[2],this.cClass="radio",aClassInZoom[3],this.cClass="combo",aClassInZoom[4],this.cClass="check",aClassInZoom[5],this.cClass="btn",aClassInZoom[6],this.cClass="mediabtn",aClassInZoom[7],this.cClass="linenode",aClassInZoom[8],aClassInZoom[9]))
        if LOWER(this.cClass) = "linenode"
          this.cName = "Linenode"+ALLTRIM(STR(this.w_COUNT+1))
          this.cKey = ALLTRIM(this.oParentObject.oParentObject.croot)
        else
          this.cName = this.oParentObject.oParentObject.cvarname
          this.cName = iif(Empty(this.cName) OR this.cName=".-.", this.cClass+ALLTRIM(STR(1)), this.cName)
          this.cKey = ALLTRIM(this.oParentObject.oParentObject.name)
        endif
        * --- Insert into objmaskoptions
        i_nConn=i_TableProp[this.objmaskoptions_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.objmaskoptions_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.objmaskoptions_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"objkey"+",name"+",oclass"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.cKey),'objmaskoptions','objkey');
          +","+cp_NullLink(cp_ToStrODBC(this.cName),'objmaskoptions','name');
          +","+cp_NullLink(cp_ToStrODBC(this.cClass),'objmaskoptions','oclass');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'objkey',this.cKey,'name',this.cName,'oclass',this.cClass)
          insert into (i_cTable) (objkey,name,oclass &i_ccchkf. );
             values (;
               this.cKey;
               ,this.cName;
               ,this.cClass;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        for this.w_COUNT=1 to this.oParentObject.oParentObject.parent.ControlCount
        if this.oParentObject.oParentObject.parent.Controls(this.w_COUNT).bSelected
          this.cClass = this.oParentObject.oParentObject.parent.Controls(this.w_COUNT).class
          this.cClass = substr(this.cClass,rat("_",this.cClass)+1,len(this.cClass))
          * --- Modifico il nome della classe per la visualizzazione nello zoom
          this.cClass = ALLTRIM(icase(this.cClass="lbl",aClassInZoom[1],this.cClass="txt",aClassInZoom[2],this.cClass="radio",aClassInZoom[3],this.cClass="combo",aClassInZoom[4],this.cClass="check",aClassInZoom[5],this.cClass="btn",aClassInZoom[6],this.cClass="mediabtn",aClassInZoom[7],this.cClass="linenode",aClassInZoom[8],aClassInZoom[9]))
          if LOWER(this.cClass) = "linenode"
            this.cName = "Linenode"+ALLTRIM(STR(this.w_COUNT+1))
            this.cKey = ALLTRIM(this.oParentObject.oParentObject.parent.Controls(this.w_COUNT).croot)
          else
            this.cName = this.oParentObject.oParentObject.parent.Controls(this.w_COUNT).cvarname
            this.cName = iif(Empty(this.cName) OR this.cName=".-.", this.cClass+ALLTRIM(STR(this.w_COUNT+1)), this.cName)
            this.cKey = ALLTRIM(this.oParentObject.oParentObject.parent.Controls(this.w_COUNT).name)
          endif
          * --- Modifico il nome della classe per la visualizzazione nello zoom
          * --- Select from objmaskoptions
          i_nConn=i_TableProp[this.objmaskoptions_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.objmaskoptions_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) as NUMREC  from "+i_cTable+" objmaskoptions ";
                +" where objkey = "+cp_ToStrODBC(this.cKey)+"";
                 ,"_Curs_objmaskoptions")
          else
            select COUNT(*) as NUMREC from (i_cTable);
             where objkey = this.cKey;
              into cursor _Curs_objmaskoptions
          endif
          if used('_Curs_objmaskoptions')
            select _Curs_objmaskoptions
            locate for 1=1
            do while not(eof())
            if NUMREC = 0
              * --- Insert into objmaskoptions
              i_nConn=i_TableProp[this.objmaskoptions_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.objmaskoptions_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.objmaskoptions_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"objkey"+",name"+",oclass"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.cKey),'objmaskoptions','objkey');
                +","+cp_NullLink(cp_ToStrODBC(this.cName),'objmaskoptions','name');
                +","+cp_NullLink(cp_ToStrODBC(this.cClass),'objmaskoptions','oclass');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'objkey',this.cKey,'name',this.cName,'oclass',this.cClass)
                insert into (i_cTable) (objkey,name,oclass &i_ccchkf. );
                   values (;
                     this.cKey;
                     ,this.cName;
                     ,this.cClass;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
              select _Curs_objmaskoptions
              continue
            enddo
            use
          endif
        endif
        next
      case this.pOper="SelectZoom"
        * --- Lettura propriet� degli oggetti
        cNamePage = aZoom[this.pPage]
        this.oObject = this.oParentObject.w_z&cNamePage
        Select (this.oObject.cCursor)
        if EMPTY(this.oParentObject.w_CURROBJ)
          Go Top
        endif
        cNewClassObj = ALLTRIM(oclass)
        cObj = ALLTRIM(ObjKey)
        this.nIndex = 1
        if this.oParentObject.w_CURRCLAS<>cNewClassObj
          this.bTrovato = .F.
          this.bTrovato2 = .F.
          this.nIndClass = 0
          this.nIndClass2 = 0
          do while this.nIndex <= ALEN(aClassInZoom) AND (NOT this.bTrovato or NOT this.bTrovato2)
            if aClassInZoom[this.nIndex] = cNewClassObj
              this.bTrovato = .T.
              this.nIndClass = aCorrisp[this.nIndex]
            endif
            if aClassInZoom[this.nIndex] = this.oParentObject.w_CURRCLAS
              this.bTrovato2 = .T.
              this.nIndClass2 = aCorrisp[this.nIndex]
            endif
            this.nIndex = this.nIndex+1
          enddo
        endif
        * --- Se nIndex > 1 le classi sono diverse, ma vanno controllati gli indici delle pagine perch� potrebbero essere visualizzati nella stessa pagina
        if NOT EMPTY(this.oParentObject.w_CURRCLAS) AND this.nIndex > 1 AND this.nIndClass <> this.nIndClass2
          * --- Selezionato oggetto per cui � necessario cambiare pagina
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.bTrovato = .f.
          nLoop = 1
          do while nLoop <= ALEN(aClassInZoom) and NOT this.bTrovato
            if aClassInZoom[nLoop] = cNewClassObj
              * --- Attivo la pagina nuova e esco, la pagina a sua volta far� la SelectZoom ma con i suoi campi
              this.nIndex = aCorrisp[nLoop]
              this.oParentObject.w_CURRCLAS = cNewClassObj
              this.oParentObject.w_CURROBJ = cObj
              this.oParentObject.oPgFrm.ActivePage = this.nIndex
              this.bTrovato = .t.
            endif
            nLoop = nLoop+1
          enddo
        else
          cNumPage = ALLTRIM(STR(this.pPage))
          if NOT EMPTY(cObj)
            cObjClass = ALLTRIM(oclass)
            this.oParentObject.w_ZNAME = ALLTRIM(name)
            this.oParentObject.w_CLASSOBJ5 = ALLTRIM(oclass)
            * --- Modifico handlers (riquadri di selezione) in modo da indicare nella tavola di disegno quale oggetto � stato selezionato
            if NOT EMPTY(this.oParentObject.w_CURROBJ) AND this.oParentObject.w_CURROBJ <> cObj
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.oObjProp = this.oParentObject.oParentObject.parent.&cObj
            * --- Evidenzio la nuova selezione
            if LOWER(this.oObjProp.Class) <> "draw_linenode"
              cShapeNW = this.oObjProp.cHandlernw 
 cShapeNE = this.oObjProp.cHandlerne 
 cShapeSW = this.oObjProp.cHandlersw 
 cShapeSE = this.oObjProp.cHandlerse 
              oShape = this.oObjProp.parent.&cShapeNW 
 oShape.backcolor = RGB(100, 149, 237) 
 oShape = this.oObjProp.parent.&cShapeNE 
 oShape.backcolor = RGB(100, 149, 237) 
 oShape = this.oObjProp.parent.&cShapeSW 
 oShape.backcolor = RGB(100, 149, 237) 
 oShape = this.oObjProp.parent.&cShapeSE 
 oShape.backcolor = RGB(100, 149, 237) 
 oShape = .NULL.
            else
              cNextObj = this.oObjProp.cnext
              do while NOT EMPTY (cNextObj)
                oNext = this.oParentObject.oParentObject.parent.&cNextObj 
 cNextObj = oNext.cnext
                if NOT EMPTY (cNextObj)
                  oNext.BackColor = RGB(100, 149, 237)
                endif
              enddo
            endif
            this.oParentObject.w_CURROBJ = cObj
            this.oParentObject.w_CURRCLAS = cNewClassObj
            Select * from CurOptions where Type=cNamePage into cursor CurAlias
            do while not eof()
              cFlag = ALLTRIM(FlagName)
              * --- Vengono sovrascritti solo i campi senza il flag attivo (con un solo record vengono sovrascritti solo alla prima attivazione pagina)
              if (this.oParentObject.w_HIDE&cNumPage>1 OR NOT this.oparentobject.w_INIT&cNumPage) AND not this.oParentObject.w_&cFlag
                cField = ALLTRIM(FieldName) 
 cProp = ALLTRIM(PropName) 
 cPlus = ALLTRIM(FlagPlus)
                * --- Gestione campi esclusivi per i mediabtn
                if empty(nvl(cPlus,"")) OR LOWER(cObjClass)="mediabutton"
                  this.oParentObject.w_&cField = this.oObjProp.&cProp
                endif
                * --- Caricamento colore campi alla selezione
                if lower(cField) $ "brdclr5|bckclr5|color6"
                   local l_obj 
 l_obj = This.oParentObject.GetCtrl("w_"+cField) 
 l_obj.DisabledBackcolor = This.oParentObject.w_&cField 
 l_obj.DisabledForecolor = This.oParentObject.w_&cField 
 l_obj=.null.
                endif
              endif
              skip
            enddo
            this.oParentObject.w_INIT&cNumPage = .T.
            Use in Select ("CurAlias")
          endif
        endif
        this.oObject = .NULL.
      case this.pOper="SelectPage"
        if NOT EMPTY(NVL(this.oParentObject.w_FROMPAGE,""))
          cNamePage=aZoom[this.oParentObject.w_FROMPAGE]
          cNumPage = ALLTRIM(STR(this.oParentObject.w_FROMPAGE))
          this.oObject = this.oParentObject.w_z&cNamePage
          * --- Salvo selezioni zoom precedente
          if Used(this.oObject.cCursor) AND not empty(cNumPage)
            Select xchk,objkey from (this.oObject.cCursor) into cursor "Cursor_Page"
          endif
        endif
        this.oParentObject.w_FROMPAGE = this.pPage
        cNamePage = aZoom[this.pPage]
        cNumPage = ALLTRIM(STR(this.pPage))
        this.oParentObject.w_NACTIVEPAGE = IIF(EMPTY(this.oParentObject.w_NACTIVEPAGE), cNumPage,this.oParentObject.w_NACTIVEPAGE+cNumPage )
        this.oObject = this.oParentObject.w_z&cNamePage
        this.oParentObject.NotifyEvent("Pagina"+ALLTRIM(STR(this.pPage)))
        if Used(this.oObject.cCursor) and Used("Cursor_Page")
          * --- Ripristino cursore alle selezioni precedenti
          UPDATE (this.oObject.cCursor) SET XCHK=1 WHERE objkey in (select objkey from ("Cursor_Page") where xchk=1)
          Update (this.oObject.cCursor) set XCHK=0 where objkey in (select objkey from ("Cursor_Page") where xchk=0)
          use in select ("Cursor_Page")
          * --- Se apro la pagina con doppio click sullo zoom, prendo l'oggetto selezionato
          *         Se invece ho aperto la pagina dal tab, prendo il primo oggetto della classe corretta
          Select (this.oObject.cCursor)
          Go Top
          nLoop=1
          this.cName = ""
          do while nLoop<= ALEN(aClassInZoom)
            * --- Ricerco le classi relative alla pagina appena attivata
            if aCorrisp[nLoop] = this.pPage
              this.cName = this.cName+aClassInZoom[nLoop]+"-"
            endif
            nLoop = nLoop+1
          enddo
          if NOT(UPPER(this.oParentObject.w_CURRCLAS) $ UPPER(this.cName))
            do while NOT(UPPER(ALLTRIM(oclass)) $ UPPER(this.cName))
              skip
            enddo
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_CURRCLAS = ALLTRIM(oclass)
            this.oParentObject.w_CURROBJ = ALLTRIM(objkey)
          else
            do while ALLTRIM(objkey) <> this.oParentObject.w_CURROBJ
              skip
            enddo
          endif
          this.oObject.grd.Refresh()
        else
          * --- Alla prima attivazione pagina, se c'� solo un record per pagina questo viene selezionato
          this.nIndex = 1
          do while this.nIndex <= ALEN(aZoom)
            * --- Ricerco, per ogni pagina, le classi da essa contenute e faccio la COUNT sullo zoom
            this.nIndClass = 1
            cNameClass = ""
            do while this.nIndClass <= ALEN(aClassInZoom)
              this.nCorrispPage = aCorrisp[this.nIndClass]
              if this.nCorrispPage = this.nIndex
                cNameClass = cNameClass+"'"+aClassInZoom[this.nIndClass]+"',"
              endif
              this.nIndClass = this.nIndClass+1
            enddo
            cNameClass = LEFT(cNameClass,LEN(cNameClass)-1)
            Select count(*) as NUMREC from (this.oObject.cCursor) where oclass in (&cNameClass) into cursor CurCount
            if NUMREC = 1
              UPDATE (this.oObject.cCursor) SET xchk=1 where oclass in (&cNameClass)
            endif
            USE IN SELECT ("CurCount")
            this.nIndex = this.nIndex+1
          enddo
        endif
        this.oObject = .NULL.
        this.oObjProp = .NULL.
      case this.pOper="Font"
        * --- Inserimento font
        cNumPage=ALLTRIM(STR(this.pPage))
         Dimension aFnt[7] 
 aFnt[1] = Alltrim(This.oParentObject.w_fntname&cNumPage) 
 aFnt[2] = This.oParentObject.w_fntsize&cNumPage 
 aFnt[3] = This.oParentObject.w_FntColor&cNumPage 
 aFnt[4] = This.oParentObject.w_FntBold&cNumPage 
 aFnt[5] = This.oParentObject.w_FntItalic&cNumPage 
 aFnt[6] = This.oParentObject.w_FntUnder&cNumPage 
 aFnt[7] = This.oParentObject.w_FntStrike&cNumPage
        * --- - Scelta font
        if cp_GetFont(@aFnt)
          This.oParentObject.w_fntname&cNumPage = Alltrim(aFnt[1]) 
 This.oParentObject.w_fntsize&cNumPage = aFnt[2] 
 This.oParentObject.w_FntColor&cNumPage = aFnt[3] 
 This.oParentObject.w_FntBold&cNumPage = aFnt[4] 
 This.oParentObject.w_FntItalic&cNumPage = aFnt[5] 
 This.oParentObject.w_FntUnder&cNumPage= aFnt[6] 
 This.oParentObject.w_FntStrike&cNumPage = aFnt[7]
          This.oParentObject.NotifyEvent("FontColorBatch"+cNumPage)
        endif
      case this.pOper="Archive"
        * --- Gestione link chiavi alla selezione archivi
        For i=1 to 4
        cNumField = ALLTRIM(STR( i )) 
 this.oParentObject.w_FIELD&cNumField = ""
        Next
        this.cArch = ALLTRIM(this.oParentObject.w_ARCHIVE2)
        this.cKey = cp_KeyToSQL(i_dcx.GetIdxDef(this.cArch,1))
        this.nIndex = At( "," , this.cKey)
        this.w_COUNT = 1
        do while this.nIndex<>0
          this.cSlot = LEFT(this.cKey, this.nIndex-1)
          cNumField = ALLTRIM(STR(this.w_COUNT))
          this.oParentObject.w_FIELD&cNumField = this.cSlot
          this.cKey = SUBSTR(this.cKey,this.nIndex+1)
          this.nIndex = At( "," , this.cKey)
          this.w_COUNT = this.w_COUNT+1
        enddo
        this.oParentObject.w_FIELD4 = this.cKey
      case this.pOper="SelectAll"
        cNamePage=aZoom[this.pPage]
        Select distinct FlagName from CurOptions where Type=cNamePage into cursor CurAlias
        do while not eof()
          cFlag = ALLTRIM(FlagName)
          this.oParentObject.w_&cFlag = (this.pSelect="S")
          skip
        enddo
        Use in Select ("CurAlias")
      case this.pOper="Save"
        * --- Salvataggio (lanciato da Ok e Applica)
        if TYPE("pPage")="N"
          * --- Applica le modifiche, solo alla pagina corrente
          cNamePage = aZoom[this.pPage]
          cNumPage = ALLTRIM(STR(this.pPage))
          this.oObject = this.oParentObject.w_z&cNamePage
          nLoop=1
          cNameClass=""
          do while nLoop<= ALEN(aClassInZoom)
            * --- Ricerco le classi relative alla pagina appena attivata
            if aCorrisp[nLoop] = this.pPage
              cNameClass= cNameClass+"'"+aClassInZoom[nLoop]+"',"
            endif
            nLoop = nLoop+1
          enddo
          cNameClass = LEFT(cNameClass,LEN(cNameClass)-1)
          Select * from (this.oObject.cCursor) where oclass in (&cNameClass) into cursor Save 
 Go Top
          cObjClass = ALLTRIM(oclass)
          do while not eof()
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            Select ("Save")
            skip
          enddo
        else
          * --- Salva
          nNumPage = this.oParentObject.w_FROMPAGE
          cNamePage = aZoom[nNumPage]
          this.oObject = this.oParentObject.w_z&cNamePage
          Select * from (this.oObject.cCursor) into cursor Save 
 Go Top
          do while not eof()
            cObjClass = ALLTRIM(oclass)
            this.nIndClass = 1
            this.bTrovato = .F.
            do while this.nIndClass <= ALEN(aClassInZoom) and NOT this.bTrovato
              if aClassInZoom[this.nIndClass] = cObjClass
                this.nIndex = aCorrisp[this.nIndClass]
                this.bTrovato = .T.
              endif
              this.nIndClass = this.nIndClass+1
            enddo
            cNamePage = aZoom[this.nIndex]
            cNumPage=ALLTRIM(STR(this.nIndex))
            if cNumPage $ this.oParentObject.w_NACTIVEPAGE
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            Select ("Save")
            skip
          enddo
        endif
        Use in Select ("CurAlias")
        Use in Select ("Save")
        this.oObject = .NULL.
        this.oObjProp = .NULL.
        * --- Se siamo in Salva chiudo i cursori e il temporaneo
        if type("pPage")<>"N"
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOper="Done"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="MaskInit"
        * --- Init delle opzioni maschera
        this.oObjProp = this.oParentObject.oParentObject.oform
        Select * from CurOptions into cursor CurAlias
        Go Top
        do while not eof()
          cField = ALLTRIM(FieldName) 
 cProp = ALLTRIM(PropName)
          do case
            case LOWER(cField)="screen"
              this.oParentObject.w_&cField = IIF(this.oObjProp.&cProp="FullScreen" or this.oObjProp.&cProp="OnScreen",1,0)
            case LOWER(cField)="fullscreen"
              this.oParentObject.w_&cField = IIF(this.oObjProp.&cProp="FullScreen",1,0)
            case "step" $ LOWER(cField)
              this.oParentObject.w_&cField = this.oObjProp.drw.&cProp
            otherwise
              this.oParentObject.w_&cField = this.oObjProp.&cProp
          endcase
          skip
        enddo
        Use in Select ("CurAlias")
      case this.pOper="MaskConf"
        * --- Salvataggio file di configurazione con i valori dele opzioni maschera
        this.oParentObject.w_CNFFILE = Alltrim(this.oParentObject.w_CNFFILE)
        * --- cClassCnf conterr� la classe degli oggetti definita nel file DBF
        this.cClassCnf = IIF(this.pPage=5,"mediabtn",IIF(this.pPage=6,"node","mask"))
        this.cClass = IIF(this.pPage=0,"",aZoom[this.pPage])
        if not EMPTY(this.oParentObject.w_CNFFILE)
          if EMPTY(this.cClass)
            * --- Opzioni maschera
            Select * from CurOptions into cursor CurAlias
          else
            * --- Opzioni oggetti
            Select * from CurOptions where Type=this.cClass And Not(Lower(PropName)="top") And Not(Lower(PropName)="left") into cursor CurAlias
          endif
          Go Top
          if Not cp_fileexist(this.oParentObject.w_CNFFILE)
            * --- Creo il file
            Select "           " as Obj, Propname as Prop, "                                                                                                                                  " as Val from CurAlias into cursor CurAppend
            =WRCURSOR("CurAppend")
            Update CurAppend Set Obj=this.cClassCnf
            copy to (this.oParentObject.w_CNFFILE) TYPE FOX2X
            Use (this.oParentObject.w_CNFFILE) Shared In 0
            Use in Select ("CurAppend")
          else
            Select Count(*) as Count from (this.oParentObject.w_CNFFILE) where Obj=(this.cClassCnf) into cursor CurCount
            if Count = 0
              * --- La classe non � presente nel file, la inserisco
              Select "" as Obj, Propname as Prop, "" as Val from CurAlias into cursor CurAppend
              Select * from (Juststem(this.oParentObject.w_CNFFILE)) UNION Select * from CurAppend into cursor FinalCur
              Use in Select (Juststem(this.oParentObject.w_CNFFILE))
              Select ("FinalCur")
              copy to (this.oParentObject.w_CNFFILE) TYPE FOX2X
              Update (this.oParentObject.w_CNFFILE) Set Obj=(this.cClassCnf) where Empty(Obj)
              Use in Select ("FinalCur")
            endif
            Use in Select ("CurCount")
            Use in Select ("CurAppend")
            if Not cp_YesNo("Il file di configurazione verr� sovrascritto, confermare l'operazione?",32,.f.)
              Return
            endif
          endif
          Select CurAlias 
 Go Top
          this.cFile = JUSTSTEM(this.oParentObject.w_CNFFILE)
          do while not eof()
            cField = ALLTRIM(FieldName) 
 cProp = LOWER(ALLTRIM(PropName))
            * --- I valori nel dbf sono di tipo char
            FieldValue = TRANSFORM(this.oParentObject.w_&cField)
            Update (this.cFile) set val=(FieldValue) where obj=(this.cClassCnf) and Lower(prop)=cProp
            skip
          enddo
          Use in Select ("CurAlias")
          Use in Select (this.cFile)
        endif
      case this.pOper="MaskSave"
        * --- Salvataggio opzioni maschera
        this.oObjProp = this.oParentObject.oParentObject.oform
        Select * from CurOptions into cursor CurAlias
        Go Top
        do while not eof()
          cField = ALLTRIM(FieldName) 
 cProp = ALLTRIM(PropName)
          do case
            case LOWER(cProp)="cscreen"
              this.oObjProp.&cProp=Iif(this.oParentObject.w_FullScreen=1,"FullScreen",Iif(this.oParentObject.w_Screen=1,"OnScreen","Form"))
            case "step" $ LOWER(cField)
              this.oObjProp.drw.&cProp = this.oParentObject.w_&cField 
            otherwise
              if Type("this.oParentObject.w_&cField") = "C"
                this.oObjProp.&cProp = Alltrim( this.oParentObject.w_&cField )
              else
                this.oObjProp.&cProp = this.oParentObject.w_&cField 
              endif
          endcase
          skip
        enddo
        * --- Resize form alla dimensione dell'immagine
        if !Empty(this.oObjProp.cBckImage) And cp_fileexist(this.oObjProp.cBckImage)
          Local loImg As xfcImage 
 With _Screen.System.Drawing 
 loImg = .Image.FromFile(this.oObjProp.cBckImage)
          if (loImg.Width<>this.oObjProp.Width Or loImg.Height<>this.oObjProp.Height) And cp_YesNo(cp_Translate(MSG_RESIZE_FORM_TO_IMAGE_QP))
            this.oObjProp.Width = loImg.Width
            this.oObjProp.Height = loImg.Height
          endif
          Endwith
        endif
        this.oObjProp.GridSettings()     
        this.oObjProp.bckImg.Setup(this.oObjProp.cBckImage, this.oObjProp.cGradient, this.oObjProp.nFColor, this.oObjProp.nLColor, this.oObjProp.nGradientType, this.oObjProp.Width, this.oObjProp.Height, .T.) 
 this.oObjProp.drw.SetModified(.T.)
        if Used ("CurOptions")
          SELECT ("CurOptions") 
 USE
        endif
      case this.pOper="MaskDone"
        if Used ("CurOptions")
          SELECT ("CurOptions") 
 USE
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Drop temporary table objmaskoptions
    i_nIdx=cp_GetTableDefIdx('objmaskoptions')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('objmaskoptions')
    endif
    if Used ("CurOptions")
      SELECT ("CurOptions") 
 USE
    endif
    if Used("Cursor_Page")
      use in select ("Cursor_Page")
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riporto ai colori standard la vecchia selezione
    cCurrObj = this.oParentObject.w_CURROBJ
    this.oObjProp = this.oParentObject.oParentObject.parent.&cCurrObj
    if LOWER(this.oObjProp.Class) <> "draw_linenode"
      cShapeNW = this.oObjProp.cHandlernw 
 cShapeNE = this.oObjProp.cHandlerne 
 cShapeSW = this.oObjProp.cHandlersw 
 cShapeSE = this.oObjProp.cHandlerse 
      * --- Riquadro bianco
      if Type("this.oObjProp.parent.&cShapeNW")<>"U"
        oShape = this.oObjProp.parent.&cShapeNW 
 oShape.backcolor = RGB(255,255,255)
      endif
      if Type("this.oObjProp.parent.&cShapeNE")<>"U"
        oShape = this.oObjProp.parent.&cShapeNE 
 oShape.backcolor = RGB(255,255,255)
      endif
      if Type("this.oObjProp.parent.&cShapeSW")<>"U"
        oShape = this.oObjProp.parent.&cShapeSW 
 oShape.backcolor = RGB(255,255,255)
      endif
      if Type("this.oObjProp.parent.&cShapeSE")<>"U"
        oShape = this.oObjProp.parent.&cShapeSE 
 oShape.backcolor = RGB(255,255,255)
      endif
      oShape = .NULL.
    else
      * --- Nodi blu, solo quelli interni
      cNextObj = this.oObjProp.cnext
      do while NOT EMPTY (cNextObj)
        oNext = this.oParentObject.oParentObject.parent.&cNextObj 
 cNextObj = oNext.cnext
        if NOT EMPTY (cNextObj)
          oNext.BackColor = RGB(255,0,0)
        endif
        oNext = .NULL.
      enddo
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    cObj = ALLTRIM(ObjKey)
    this.oObjProp = this.oParentObject.oParentObject.parent.&cObj
    if this.oParentObject.w_HIDE&cNumPage<>0 AND xchk=1
      Select * from CurOptions where Type=cNamePage into cursor CurAlias
      do while not eof()
        cFlag = ALLTRIM(FlagName)
        * --- Vengono salvati solo i campi con il flag attivo
        if this.oParentObject.w_HIDE&cNumPage=1 OR this.oParentObject.w_&cFlag
          cField = ALLTRIM(FieldName) 
 cProp = ALLTRIM(PropName) 
 cPlus = ALLTRIM(FlagPlus)
          * --- Gestione campi esclusivi per i mediabtn
          if Type("this.oParentObject.w_&cField") = "C"
            this.oParentObject.w_&cField = Alltrim(this.oParentObject.w_&cField)
          endif
          if empty(nvl(cPlus,"")) OR LOWER(cObjClass)="mediabutton"
            this.oObjProp.&cProp = this.oParentObject.w_&cField
          endif
        endif
        skip
      enddo
      * --- Riposiziono i riquadri di selezione
      if LOWER(cObjClass) <> "linenode"
        this.oObjProp.RemoveHandlers()     
        this.oObjProp.AddHandlers()     
      endif
      * --- Imposto l'anteprima dell'assegnamento dei valori
      do case
        case LOWER(cObjClass) = "label"
          this.oObjProp.Obj.Caption = this.oObjProp.cVarName
          this.oObjProp.Obj.Alignment = Iif(this.oObjProp.cAlign="R",1,Iif(this.oObjProp.cAlign="C",2,0))
          this.oObjProp.Obj.FontName = this.oObjProp.FontName
          this.oObjProp.Obj.FontSize = this.oObjProp.FontSize
          this.oObjProp.Obj.ForeColor = this.oObjProp.FontColor
          this.oObjProp.Obj.FontBold = this.oObjProp.FontBold
          this.oObjProp.Obj.FontBold = this.oObjProp.FontBold
          this.oObjProp.Obj.FontItalic = this.oObjProp.FontItalic
          this.oObjProp.Obj.FontUnderline = this.oObjProp.FontUnderline
          this.oObjProp.Obj.FontStrikethru = this.oObjProp.FontStrikeout
        case LOWER(cObjClass) $ "text-radio-combo"
          this.oObjProp.Obj.Value = this.oObjProp.cVarName
        case LOWER(cObjClass) $ "check-button"
          this.oObjProp.Obj.Caption = this.oObjProp.cTitle
        case LOWER(cObjClass) = "mediabutton"
          this.oObjProp.Preview()     
      endcase
      if LOWER(cObjClass) $ "button-bitmap"
        * --- Aggiorno l'anteprima dell'immagine 
        this.oObjProp.obj.Picture = IIF(LOWER(JUSTEXT(this.oParentObject.w_PICTURE&cNumPage))="ico",FORCEEXT(i_ThemesManager.RetBmpFromIco(this.oParentObject.w_PICTURE&cNumPage, this.oParentObject.w_FORMAT&cNumPage),"bmp"),this.oParentObject.w_PICTURE&cNumPage)
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper,pPage,pSelect)
    this.pOper=pOper
    this.pPage=pPage
    this.pSelect=pSelect
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='cpttbls'
    this.cWorkTables[2]='*objmaskoptions'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_objmaskoptions')
      use in _Curs_objmaskoptions
    endif
    if used('_Curs_objmaskoptions')
      use in _Curs_objmaskoptions
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,pPage,pSelect"
endproc
