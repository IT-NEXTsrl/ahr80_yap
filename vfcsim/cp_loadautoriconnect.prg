* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_loadautoriconnect                                            *
*              Riconnessione automatica                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-02                                                      *
* Last revis.: 2010-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_loadautoriconnect",oParentObject)
return(i_retval)

define class tcp_loadautoriconnect as StdBatch
  * --- Local variables
  w_RCDEADRQ = space(1)
  w_RCNUMDRQ = 0
  w_RCDELAYD = 0
  w_RCAUTORC = space(1)
  w_RCNUMRIC = 0
  w_RCDELAYR = 0
  w_RCCNTIME = 0
  w_RCCONMSG = space(1)
  * --- WorkFile variables
  paramric_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riconnessione automatica
    * --- Read from paramric
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.paramric_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.paramric_idx,2],.t.,this.paramric_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "rcautorc,rcnumric,rcdelayr,rccntime,rcdeadrq,rcnumdrq,rcdelayd,rcconmsg"+;
        " from "+i_cTable+" paramric where ";
            +"rcserial = "+cp_ToStrODBC("00001");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        rcautorc,rcnumric,rcdelayr,rccntime,rcdeadrq,rcnumdrq,rcdelayd,rcconmsg;
        from (i_cTable) where;
            rcserial = "00001";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_rcautorc = NVL(cp_ToDate(_read_.rcautorc),cp_NullValue(_read_.rcautorc))
      this.w_rcnumric = NVL(cp_ToDate(_read_.rcnumric),cp_NullValue(_read_.rcnumric))
      this.w_rcdelayr = NVL(cp_ToDate(_read_.rcdelayr),cp_NullValue(_read_.rcdelayr))
      this.w_rccntime = NVL(cp_ToDate(_read_.rccntime),cp_NullValue(_read_.rccntime))
      this.w_rcdeadrq = NVL(cp_ToDate(_read_.rcdeadrq),cp_NullValue(_read_.rcdeadrq))
      this.w_rcnumdrq = NVL(cp_ToDate(_read_.rcnumdrq),cp_NullValue(_read_.rcnumdrq))
      this.w_rcdelayd = NVL(cp_ToDate(_read_.rcdelayd),cp_NullValue(_read_.rcdelayd))
      this.w_rcconmsg = NVL(cp_ToDate(_read_.rcconmsg),cp_NullValue(_read_.rcconmsg))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    Public i_DEADREQUERY , i_NUMDEADREQUERY, i_DELAYDEADREQUERY, i_CONNECTMSG, i_AUTORICONNECT, i_CONNECTLOGIN
    Public i_NUMRICONNECT, i_NUMRICONNECT, i_DELAYRICONNECT, i_ConnectTimeOutEndProc
     
 i_DEADREQUERY = (this.w_RCDEADRQ="S") 
 i_NUMDEADREQUERY = this.w_RCNUMDRQ 
 i_DELAYDEADREQUERY = this.w_RCDELAYD 
 i_AUTORICONNECT = (this.w_RCAUTORC = "S") 
 i_NUMRICONNECT = this.w_RCNUMRIC 
 i_DELAYRICONNECT = this.w_RCDELAYR 
 i_ConnectTimeOut = this.w_RCCNTIME 
 i_CONNECTMSG = this.w_RCCONMSG 
 i_CONNECTLOGIN = ""
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='paramric'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
