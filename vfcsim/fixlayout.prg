* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: fixlayout                                                       *
*              Layout pagina                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-12                                                      *
* Last revis.: 2011-02-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFileStampa
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tfixlayout",oParentObject,m.pFileStampa)
return(i_retval)

define class tfixlayout as StdBatch
  * --- Local variables
  pFileStampa = space(0)
  w_nOldArea = 0
  w_Set = space(10)
  w_SetOrientation = space(10)
  w_SetPaperSize = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ***** Zucchetti Aulla Inizio
    *     * --------------------------------------------------------------------------
    *     * Routine per impostare correttamente Layout Pagina Report/Label FOXPRO
    *     * --------------------------------------------------------------------------
    *     * Procedure : FixLayout
    *     * Ver       : 1.1
    *     * Autore    : Zucchetti Aulla (MS)
    *     * Data      : 20/04/2000
    *     * Agg       : 05/02/2001
    *     * --------------------------------------------------------------------------
    *     * Descrizione
    *     *    1) Imposta cassetto non valido per utilizzare quello della stampante
    *     *    2) Imposta formato A4 se non specificato
    *     *    3) Imposta orientamento VERTICALE se non specificato
    *     *    4) Imposta risoluzione a 0x0 per utilizzare impostazione dpi stampante.
    *     *
    *     * Parametri
    *     *    pFileStampa  : File di stampa
    *     * --------------------------------------------------------------------------
    * --- Apre il file se esiste
    if cp_fileexist(this.pFileStampa) 
      * --- Salva area di lavoro 
      this.w_nOldArea = select(0) 
      select 0 
 use (this.pFileStampa) 
 go top 
 locate for Objtype=1 and Objcode=53
      * --- Se report (come tabella) in sola lettura allora non posso modificarlo...
      if ISREADONLY()
        * --- Ripristina area di lavoro
         select (this.w_nOldArea) 
 ah_ErrorMsg("Il report %1 � in sola lettura. Impossibile modificarlo.", "stop", "", this.pFileStampa) 
 return
      endif
      if found() 
        * --- Legge impostazioni correnti
        this.w_SetOrientation = substr(mline(expr,atcline("ORIENTATION=",expr)),len("ORIENTATION=")+1) 
        this.w_SetPaperSize = substr(mline(expr,atcline("PAPERSIZE=",expr)),len("PAPERSIZE=")+1)
        * --- Prepara nuove impostazioni
        this.w_Set = ""
        this.w_Set = this.w_Set+ "ORIENTATION="+iif(empty(this.w_SetOrientation),"0",this.w_SetOrientation)+CHR(13)+CHR(10)
        this.w_Set = this.w_Set+ "PAPERSIZE="+iif(empty(this.w_SetPaperSize),"9",this.w_SetPaperSize)+CHR(13)+CHR(10)
        this.w_Set = this.w_Set+ "DEFAULTSOURCE=15"+CHR(13)+CHR(10)
        this.w_Set = this.w_Set+ "PRINTQUALITY=0"+CHR(13)+CHR(10)
        this.w_Set = this.w_Set+ "YRESOLUTION=0"
        * ---  Nuove Impostazioni
        replace EXPR with this.w_Set
        * --- Clear TAG e TAG2
      endif
       use
      * --- Ripristina area di lavoro
      select (this.w_nOldArea)
    endif
    return
  endproc


  proc Init(oParentObject,pFileStampa)
    this.pFileStampa=pFileStampa
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFileStampa"
endproc
