* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_searchfilterr                                                *
*              Routine ctrl mask filter/search                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-12-22                                                      *
* Last revis.: 2015-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCommandExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_searchfilterr",oParentObject,m.pCommandExec)
return(i_retval)

define class tcp_searchfilterr as StdBatch
  * --- Local variables
  pCommandExec = space(10)
  w_VALUEFILTERTYPE = space(1)
  w_POSITION = 0
  w_OLDPOSITION = 0
  w_FilterCond = space(254)
  w_ZOOM = .NULL.
  w_ControlSource = space(50)
  w_FIRSTREC = .f.
  w_OpLike = space(10)
  w_StrValue = space(100)
  w_EOF = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per il controllo dela maschera di ricerca e filtro
    this.w_ControlSource = this.oParentObject.oParentObject.ControlSource
    this.w_OpLike = iif(CP_DBTYPE="PostgreSQL" AND g_USE_ILIKE="S", "iLike", "Like")
    if Left(this.w_ControlSource,17)="cp_MemoFirstLine("
      this.w_ControlSource = cp_ControlSourceMemo(this.w_ControlSource)
    endif
    local i_OldCursor 
 i_OldCursor=select ()
    do case
      case this.pCommandExec="Type"
        select (this.oParentObject.oParentObject.parent.recordsource)
        do case
          case type(this.w_ControlSource)="D" or type(this.w_ControlSource)="T"
            this.w_VALUEFILTERTYPE = "D"
            this.oParentObject.w_ZoomValue.visible = .t.
            this.oParentObject.w_ZoomValueN.visible = .f. 
          case type(this.w_ControlSource)="N"
            this.w_VALUEFILTERTYPE = "N"
            this.oParentObject.w_ZoomValue.visible = .f.
            this.oParentObject.w_ZoomValueN.visible = .t.
          otherwise
            this.w_VALUEFILTERTYPE = "C"
            this.oParentObject.w_ZoomValue.visible = .t.
            this.oParentObject.w_ZoomValueN.visible = .f. 
        endcase
        select (i_OldCursor)
        i_retcode = 'stop'
        i_retval = this.w_VALUEFILTERTYPE
        return
      case this.pCommandExec="Search"
        this.oParentObject.RunDeactivateRelease = .F.
        if RECCOUNT(this.oParentObject.oParentObject.parent.recordsource)>0
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if NOT this.w_EOF and FOUND()
            this.w_POSITION = RECNO()
          else
            if this.oParentObject.w_Searchwrap="S"
              this.w_POSITION = 1
              select (this.oParentObject.oParentObject.parent.recordsource)
              GO (this.w_POSITION)
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if FOUND()
                this.w_POSITION = RECNO()
              else
                Cp_ErrorMsg("Valore non trovato")
                this.w_POSITION = this.w_OLDPOSITION
              endif
            else
              Cp_ErrorMsg("Valore non trovato")
              this.w_POSITION = this.w_OLDPOSITION
            endif
          endif
          select (this.oParentObject.oParentObject.parent.recordsource)
          GO (this.w_POSITION)
          if Type("this.oParentObject.oParentObject.Parent.Parent.Parent.Parent.Parent")="O"
            with this.oParentObject.oParentObject.Parent.Parent.Parent.Parent.Parent
            if TYPE (".ozoomelenco") ="O"
              elenco = .ozoomelenco 
 .ozoomelenco = "" 
 .briposiz = .T.
            endif
            if TYPE ("elenco") ="O"
              .ozoomelenco=elenco
            endif
            endwith
          endif
          this.oParentObject.oParentObject.parent.SetFocus()
          this.oParentObject.oParentObject.parent.Refresh()
          this.oParentObject.RunDeactivateRelease = .T.
        else
          Cp_ErrorMsg("Valore non trovato")
        endif
        if vartype(this.oParentObject)="O" 
          if TYPE ("i_curform.windowtype")="N" AND i_curform.windowtype=1
            this.oParentObject.EcpQuit()
          else
            this.oParentObject.SetFocusOnFirstControl()
            if this.oParentObject.oPgFrm.Pages[1].oPag.oStr_StringAdvanced.CAPTION = cp_Translate("Nascondi filtri avanzati")
              this.oParentObject.oPgFrm.Pages[1].oPag.oStr_StringAdvanced.Click()
            endif
          endif
        endif
      case this.pCommandExec="Filter"
        if this.oParentObject.w_RemPrevFilter="S"
          cp_BlankWhereZoom(this.oParentObject.oParentObject)
        endif
        this.w_FilterCond = ALLTRIM(this.w_ControlSource)
        do case
          case INLIST(this.oParentObject.w_TypeFilter, "=", ">=", ">", "<=", "<", "<>")
            this.w_FilterCond = this.w_FilterCond + alltrim(this.oParentObject.w_TypeFilter)
          case this.oParentObject.w_TypeFilter=="Like" OR this.oParentObject.w_TypeFilter=="Like %"
            this.w_FilterCond = this.w_FilterCond + " " +this.w_OpLike+ " "
          case this.oParentObject.w_TypeFilter=="Not Like" OR this.oParentObject.w_TypeFilter=="Not Like %"
            this.w_FilterCond = this.w_FilterCond + " not " +this.w_OpLike+ " "
        endcase
        do case
          case this.oParentObject.w_CTRLTYPE="D"
            if EMPTY(this.oParentObject.w_SearchValueD)
              if this.oParentObject.w_TypeFilter="<>"
                this.w_FilterCond = ALLTRIM(this.w_ControlSource) + " is not null"
              else
                this.w_FilterCond = ALLTRIM(this.w_ControlSource) + " is null"
              endif
            else
              this.w_FilterCond = this.w_FilterCond + cp_ToStrODBC(this.oParentObject.w_SearchValueD)
            endif
          case this.oParentObject.w_CTRLTYPE="N"
            this.w_FilterCond = this.w_FilterCond + CHRTRAN(ALLTRIM(STR(this.oParentObject.w_SearchValueN, 18, 4)), ",", ".")
          otherwise
            if EMPTY(this.oParentObject.w_SearchValue)
              do case
                case INLIST(this.oParentObject.w_TypeFilter, "Not Like", "Not Like %")
                  this.w_FilterCond = "[NOTEMPTYSTR(" + ALLTRIM(this.w_ControlSource) + ")] <> 0"
                case INLIST(this.oParentObject.w_TypeFilter, "Like", "Like %")
                  this.w_FilterCond = "[NOTEMPTYSTR(" + ALLTRIM(this.w_ControlSource) + ")] = 0"
                otherwise
                  this.w_FilterCond = "[NOTEMPTYSTR(" + ALLTRIM(this.w_ControlSource) + ")] "+ALLTRIM(this.oParentObject.w_TypeFilter)+" 0"
              endcase
            else
              if  "like" $ Lower(this.oParentObject.w_TypeFilter)
                this.w_StrValue = IIF(INLIST(this.oParentObject.w_TypeFilter, "Like %", "Not Like %"), "%", "") + ALLTRIM(this.oParentObject.w_SearchValue) + "%"
                if this.w_OpLike="iLike"
                  * --- con la iLike non entra nella cp_SQL_like, raddoppio \ (solo PostgreSQL)
                  this.w_StrValue = StrTran(this.w_StrValue, "\", "\\")
                endif
              else
                this.w_StrValue = ALLTRIM(this.oParentObject.w_SearchValue)
              endif
              this.w_FilterCond = this.w_FilterCond + cp_ToStrODBC(this.w_StrValue)
            endif
        endcase
        with this.oParentObject.oParentObject.Parent.Parent 
 .AddWhere(this.w_FilterCond) 
 .RefreshParamWnd() 
 .DeferredQuery() 
 endwith
        if TYPE ("this.oParentObject.oParentObject.Parent.Parent.parent.parent.parent.ozoomelenco") ="O"
          this.oParentObject.oParentObject.Parent.Parent.parent.parent.parent.briposiz=.F.
        endif
        if vartype(this.oParentObject)="O" 
          this.oParentObject.EcpQuit()
        endif
      case this.pCommandExec="LoadZoom"
        local w_ColumnSearch 
 w_ColumnSearch = this.w_ControlSource
        do case
          case this.oParentObject.w_CTRLTYPE="D"
            this.w_ZOOM = this.oParentObject.w_ZoomValue 
            select distinct &w_ColumnSearch as VALUEZOOM from (this.oParentObject.oParentObject.parent.recordsource) into cursor elemzoom where &w_ColumnSearch is not null order by 1 desc
          case this.oParentObject.w_CTRLTYPE="N"
            this.w_ZOOM = this.oParentObject.w_ZoomValueN 
            select distinct &w_ColumnSearch as VALUEZOOM from (this.oParentObject.oParentObject.parent.recordsource) into cursor elemzoom order by 1
          otherwise
            this.w_ZOOM = this.oParentObject.w_ZoomValue 
            select distinct ALLTRIM( &w_ColumnSearch ) as VALUEZOOM from (this.oParentObject.oParentObject.parent.recordsource) into cursor elemzoom where &w_ColumnSearch is not null and not empty( &w_ColumnSearch ) order by 1
        endcase
        if USED("elemzoom")
          * --- Cancello il contenuto dello Zoom
          Select (this.w_ZOOM.cCursor)
          Zap
          * --- Scan del cursore di appoggio e copia su cursore zoom
          Select elemzoom
          Go Top
          Scan
          Scatter Memvar
          Select (this.w_ZOOM.cCursor)
          Append blank
          Gather memvar
          Endscan
          USE IN elemzoom
          Select (this.w_ZOOM.cCursor) 
 Go Top
          this.w_zoom.grd.refresh
          this.w_ZOOM.grd.Refresh()     
        endif
      case this.pCommandExec="Select"
        if this.oParentObject.w_RemPrevFilter="S"
          cp_BlankWhereZoom(this.oParentObject.oParentObject)
        endif
        local w_ColumnSearch 
 w_ColumnSearch = this.w_ControlSource
        if this.oParentObject.w_CTRLTYPE="N"
          this.w_ZOOM = this.oParentObject.w_ZoomValueN
        else
          this.w_ZOOM = this.oParentObject.w_ZoomValue
        endif
        this.w_FIRSTREC = .t.
        Select (this.w_ZOOM.cCursor)
        SCAN FOR xchk=1
        if this.w_FIRSTREC
          this.w_FIRSTREC = .f.
          this.w_FilterCond = "("+ALLTRIM(this.w_ControlSource)
        else
          this.oParentObject.oParentObject.Parent.Parent.AddWhere(this.w_FilterCond)
          this.w_FilterCond = "OR"
          this.oParentObject.oParentObject.Parent.Parent.AddWhere(this.w_FilterCond)
          this.w_FilterCond = ALLTRIM(this.w_ControlSource)
        endif
        Select (this.w_ZOOM.cCursor)
        do case
          case this.oParentObject.w_CTRLTYPE="N"
            this.w_FilterCond = this.w_FilterCond + " = " + cp_ToStrODBC(VALUEZOOM)
          case this.oParentObject.w_CTRLTYPE="D"
            this.w_FilterCond = this.w_FilterCond + " = " + cp_ToStrODBC(CTOT(ALLTRIM(VALUEZOOM)))
          otherwise
            this.w_FilterCond = this.w_FilterCond + " = " + cp_ToStrODBC(ALLTRIM(VALUEZOOM))
        endcase
        ENDSCAN
        if NOT EMPTY(this.w_FilterCond)
          this.w_FilterCond = this.w_FilterCond + ")"
          this.oParentObject.oParentObject.Parent.Parent.AddWhere(this.w_FilterCond)
        endif
        this.oParentObject.oParentObject.Parent.Parent.RefreshParamWnd() 
 this.oParentObject.oParentObject.Parent.Parent.DeferredQuery()
        if vartype(this.oParentObject)="O" 
          this.oParentObject.EcpQuit()
        endif
      case this.pCommandExec="Checked"
        if this.oParentObject.w_CTRLTYPE="N"
          this.w_ZOOM = this.oParentObject.w_ZoomValueN
        else
          this.w_ZOOM = this.oParentObject.w_ZoomValue
        endif
        Select (this.w_ZOOM.cCursor)
        this.w_OLDPOSITION = RECNO()
        COUNT FOR xchk=1 TO this.w_POSITION
        this.oParentObject.w_CHECKED = this.w_POSITION>0
        if NOT this.oParentObject.w_CHECKED AND this.oParentObject.w_CtrlType="C"
          this.oParentObject.w_TypeFilter = IIF(g_RicPerCont="T", "Like %", "Like")
        endif
        if this.oParentObject.w_CHECKED
          this.oParentObject.w_SearchValue = ""
          this.oParentObject.w_SearchValueD = null
          this.oParentObject.w_SearchValueN = 0
        endif
        Select (this.w_ZOOM.cCursor)
        GO this.w_OLDPOSITION
    endcase
    select (i_OldCursor)
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    select (this.oParentObject.oParentObject.parent.recordsource)
    this.w_OLDPOSITION = RECNO()
    this.w_EOF = .f. 
    do case
      case this.w_OLDPOSITION>=RECCOUNT()
        this.w_EOF = .t.
      case this.w_OLDPOSITION=1 and not this.oParentObject.w_SecondSearch
        this.oParentObject.w_SecondSearch = .t. 
        GO this.w_OLDPOSITION
      otherwise
        this.oParentObject.w_SecondSearch = .f. 
        GO this.w_OLDPOSITION+1
    endcase
    if NOT EOF()
      local w_ColumnSearch, w_Operand 
 w_ColumnSearch = this.w_ControlSource 
 w_Operand=iif(this.oParentObject.w_TypeFilter="=", "==", this.oParentObject.w_TypeFilter)
      do case
        case this.oParentObject.w_CTRLTYPE="D"
          LOCATE FOR NVL( &w_ColumnSearch , cp_CharToDate("  -  -    ")) &w_Operand NVL( this.oParentObject.w_SearchValueD , cp_CharToDate("  -  -    ")) REST
        case this.oParentObject.w_CTRLTYPE="N"
          LOCATE FOR &w_ColumnSearch &w_Operand this.oParentObject.w_SearchValueN REST
        otherwise
          do case
            case this.oParentObject.w_TypeFilter="Like %"
              LOCATE FOR UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) $ UPPER(ALLTRIM( &w_ColumnSearch )) REST
            case this.oParentObject.w_TypeFilter="Like"
              LOCATE FOR UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) == UPPER(ALLTRIM( LEFT( &w_ColumnSearch , LEN(ALLTRIM(this.oParentObject.w_SearchValue))))) REST
            case this.oParentObject.w_TypeFilter="Not Like %"
              LOCATE FOR NOT(UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) $ UPPER(ALLTRIM( &w_ColumnSearch ))) REST
            case this.oParentObject.w_TypeFilter="Not Like"
              LOCATE FOR UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) <> UPPER(ALLTRIM( LEFT( &w_ColumnSearch , LEN(ALLTRIM(this.oParentObject.w_SearchValue))))) REST
            otherwise
              LOCATE FOR UPPER(ALLTRIM( &w_ColumnSearch )) &w_Operand UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) REST
          endcase
      endcase
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pCommandExec)
    this.pCommandExec=pCommandExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCommandExec"
endproc
