* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: PM_EXEC
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Zucchetti Aulla
* Data creazione: 08/08/04
* Aggiornato il : 21/09/04
* Translated    :
* #%&%#Build:  56
* ----------------------------------------------------------------------------
* Gestore Popup Menu
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Procedure CreateShortCut
    Parameters oParentObject, oObject, s_VMN
    Local gsMenu, gsPopup
    && I Paramteri Contengono il Nome delle variabili di riferimento
    && Per renderla + standard possibile
    gsMenu = Sys(2015) && 'PMENU'+
    gsPopup = Sys(2015) && 'MNU'+
    && Mi Assegno il Nome al Menu da usare per la (RELEASE MENU <Nome> EXTENDED)
    oObject.gsMenu = gsMenu
    && Mi assegno un nome al popup
    oObject.gsPopup = gsPopup
    && Creo il cursore per memorizzare i dati
    && Purtroppo i Parametri sono tanti li devo spezzare (wParam1 wParam2 wParam3)
    Create Cursor (gsPopup) (Opt_Name C(40), Opt_Pad C(50), Action N(1), nBar N(10), Opt_Proc C(40), ;
        wParam1 C(250), wParam2 C(250), wParam3 C(250))
    Select (gsPopup)
    Zap

    && Nel caso di Errore chiamo il gestore dell'errore
    On Error Do errhand In PM_EXEC ;
        WITH Sys(0), Error(), Message(), Message(1), ;
        PROGRAM(), Lineno(1), Dbf(), Date(), Time()

    && Leggo il menu
    Do vp_exec With gsMenu, gsPopup, s_VMN, oParentObject

    On Error
Endproc

Proc SelectionShortCut
    Param gnBar, gcPopup, oParentObject, nPopup, pObject
    Local N, oSelect, lnameProc, lwParam
    Local loBject
    Declare ArrProc[4]
    lObjCtrl = .Null.
    lObjCtrl = pObject
    On Key Label F1    Do cp_help
    On Key Label ESC   Do cp_DoAction With "ecpQuit"
    i_curform = oParentObject
    If Used(nPopup)
        *oSelect = Select()
        Select (nPopup)
        && Azzero il contenuto dell'array
        ArrProc[1]=''
        ArrProc[2]=''
        && Chiamo la funzione per la ricerca del menu selezionato
        && Alla funzione viene passato il nome del menu,il numero della barra
        && e un ho scelto un array di modo che mi ritornino tutti i dati necessari
        && per lanciare la procedura
        N = menu_code(gnBar, gcPopup, @ArrProc)
        && Nel caso di Errore chiamo il gestore dell'errore
        On Error Do errhand In PM_EXEC ;
            WITH Sys(0), Error(), Message(), Message(1), ;
            PROGRAM(), Lineno(1), Dbf(), Date(), Time()
        && Recupero nome della procedura, parametro oppure comando da
        && eseguire dall'array
        lnameProc = ArrProc[1] && alltrim(nvl(Opt_Proc,''))
        lwParam = ArrProc[2]+ArrProc[3]+ArrProc[4] && alltrim(nvl(wParam,''))
        && Eseguo i comandi
        If !Empty(lnameProc) And !Empty(lwParam)
            If Lower(lnameProc)='openmdw' Or Lower(lnameProc)='openmsk' Or Lower(lnameProc)='execbtcdef' Or Lower(lnameProc)='sys_func'
                && Aggiungo Padre chiamante dove recuperare i valori
                && da assegnare alle variabili
                lwParam = '(oParentObject,'+lwParam+')'
                lnameProc=lnameProc + lwParam
                With oParentObject
                    &lnameProc
                Endwith
            Else
                Do &lnameProc With &lwParam
            Endif
        Else
            If Empty(lnameProc) And !Empty(lwParam)
                && Lo utilizzo per Eseguire dei comandi Es: linkPCClick()
                &lwParam
            Else
                If !Empty(lnameProc) And Empty(lwParam)
                    && In questo caso richiamo la gestione
                    && Manca Parte (da Implementare) per la gestione delle
                    && operazioni da Effettuare ??????????????
                    Do &lnameProc With oParentObject
                Endif
            Endif
        Endif
        *select(oSelect)
        If Lower(Left(lnameProc,8))<>'sys_func'
            AggOparentObject(oParentObject)
        Endif
    Endif
    Wait Clear
    On Error
Endproc

Proc AggOparentObject
    Param oParent
    Local oParentObject, fType
    oParentObject=oParent
    fType=Upper(oParentObject.ParentClass)
    If !Isnull(oParentObject) And Inlist(oParentObject.cfunction,"Edit","Load")
        If fType = 'STDTRSFORM'
            oParentObject.bHeaderUpdated=.T.
        Endif
        If Inlist(fType, 'STDTRSFORM', 'STDCPFORM')
            Select (oParentObject.cTrsName)
            If I_SRV=" "
                Replace I_SRV With "U"
            Endif
        Endif
        oParentObject.mCalc(.T.)
    Endif
Endproc

&& -------------------------
&& Recupero le informazioni dal Cursore del menu
&& Le metto in un array nel caso ne servissero altre
&& � gi� predisposto
Function menu_code
    Parameter gnBar, gcPopup, ArrProc
    Private N
    ArrProc[1]=''
    ArrProc[2]=''
    ArrProc[3]=''
    ArrProc[4]=''
    N = Recno()
    Locate For Opt_Name=(gcPopup) And nBar=(gnBar)
    If Found()
        ArrProc[1] = Alltrim(Nvl(Opt_Proc,''))
        ArrProc[2] = Alltrim(Nvl(wParam1,''))
        ArrProc[3] = Alltrim(Nvl(wParam2,''))
        ArrProc[4] = Alltrim(Nvl(wParam3,''))
    Else
        Return 0
    Endif
    Goto N
    Return 1

    * -------------------------
    * release menu
Proc ReleaseShortCut
    Param gsMenu, gsPopup, oParentObject
    Local FirstSub, lCurs
    FirstSub=''
    lCurs=''
    If Not Empty(gsMenu) And Not Empty(gsPopup)
        FirstSub = gsPopup+'subF1'
        lCurs = gsPopup
        Deactivate Popups (FirstSub)
        Release Menu (gsMenu) Extended
    Endif
    If Used(lCurs)
        Use In (lCurs)
    Endif
    Return
Endproc

* -------------------------
* -------------------------
* -------------------------

Procedure errhand
    Parameter m.machine, m.messgnum, m.messg, m.linecode, ;
        m.callprog, m.inline, m.OPENTABL, m.errdate, ;
        m.errtime
    m.errspace=Select()      && Recupera corrente area di Lavoro
    m.errorder=Order()       && Recupera Ordine Corrente
    If Len(Alltrim(m.callprog))=0
        m.callprog="Command Line"
        Store Space(0) To m.linecode
    Endif
    outmsgline="Error ; "+m.messg+Chr(13)+"Line "+Str(m.inline)+ ;
        CHR(13)+ ;
        "program name = "+m.callprog+Chr(13)+"Syntax is  :"+m.linecode
    * Visual FoxPro users use =MESSAGEBOX(outmsgline,32+0)
    * MAC uUse the FXALERT() Function in Foxtools.mlb
    * FoxPro For Windows users use the MsgBox() Function in Foxtools.fll
    Wait Window outmsgline && TIMEOUT 5 && Tutte le versioni usano questa sintassi.
    Return
Endproc
