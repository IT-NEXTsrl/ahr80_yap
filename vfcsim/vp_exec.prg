* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VP_EXEC
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : zucchetti Aulla (Derivato da VU_EXEC di Gregorio Piccoli)
* Data creazione: 20/03/97
* Aggiornato il : 21/09/98
* Translated    :
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Interprete di menu
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam pMenu, pPopup, cMenuFile, oParent
&& Aggiunte variabili private cos' sono vilibili
&& a tutto il programma
Private lMenu, lPopup
Private oParentObject, gsMenu, gsPopup
&& Mi assegno i nomi
lMenu = pMenu
lPopup = pPopup
&& Devo definirle per passarle al menu altrimenti
&& in fase di esecuzione da errore
oParentObject = .Null.
gsMenu = ''
gsPopup = ''

If Type("cMenuFile")<>'C' Or cMenuFile='?'
    cMenuFile=Getfile('vmn')
Endif
If Lower(Right(cMenuFile,4))='.vmn'
    cMenuFile=Left(cMenuFile,Len(cMenuFile)-4)
Endif
If Not(cp_IsPFile(cMenuFile+'.vmn'))
    cp_msg( cp_MsgFormat(MSG_CANNOT_OPEN_MENU__,cMenuFile),.F.)
    Return
Endif
i_cMenuLoaded=cMenuFile

Local xcg,h,c,l
c=cp_GetPFile(cMenuFile+'.vmn')
xcg=Createobject('pxcg','load',c)

Define Menu (lMenu)

LoadFirstLevelMenu(c)
LoadMenu(xcg)

Return

Proc LoadFirstLevelMenu(i_cText1)
    Local i,k,p,c,i_nPos,i_nStart,i_nAction,i_cAction,i_cName,i_cMod,i_bEnabled
    Local i_cText,i_nPos1
    Local lFirst
    lFirst=.T.
    i=1
    i_nPos1=At('"'+Chr(13)+Chr(10)+' 2'+Chr(13)+Chr(10)+'"',i_cText1)
    Do While i_nPos1<>0
        i_nStart=i_nPos1-1
        Do While Substr(i_cText1,i_nStart,1)<>'"'
            i_nStart=i_nStart-1
        Enddo
        * --- separa la parte da analizzare in piccole stringhe che sono gestite piu' velocemente da VFP
        i_cText=Substr(i_cText1,i_nStart,1000)
        i_nPos=i_nPos1-i_nStart+1
        i_nStart=1
        *
        i_nAction=At('"',Substr(i_cText,i_nPos+8))
        i_cAction=Substr(i_cText,i_nPos+8,i_nAction-1)
        i_cAction=Strtran(i_cAction,Chr(253),"'")
        i_cAction=Strtran(i_cAction,Chr(254),'"')
        i_cName=Substr(i_cText,i_nStart+1,i_nPos-i_nStart-1)
        i_cName=Strtran(i_cName,Chr(253),"'")
        i_cName=Strtran(i_cName,Chr(254),'"')
        i_nAction=Val(Substr(i_cText,i_nAction+i_nPos+10,2))
        * --- controlla se l' opzione e' legata ad un modulo
        i_cMod=''
        i_bEnabled=.T.
        k=0
        p=0
        Do While k<=9
            * --- l'indicazione e' dopo 9 "
            p=p+1
            c=Substr(i_cText,i_nPos+p,1)
            Do Case
                Case c='*' And Substr(i_cText,i_nPos+p,3)='*'+Chr(13)+Chr(10)
                    k=10
                Case c='"'
                    k=k+1
                Case c='.' And Inlist(Substr(i_cText,i_nPos+p,3),'.t.','.f.')
                    i_bEnabled=Substr(i_cText,i_nPos+p,3)='.t.'
                    p=p+2
                Case k=9
                    i_cMod=i_cMod+c
            Endcase
        Enddo
        *
        If i_nAction=2 && e' un submenu'
            LoadMenuItem(lMenu, lPopup+'padF'+Alltrim(Str(i)),i_cName,i_nAction,lPopup+'subF'+Alltrim(Str(i)),0,i_cMod,i_bEnabled)
            && devo definire il menu nello screen e non nella finestra (m.oParentObject.Name)
            && altrimenti se � + lungo della maschera non si vede
            Define Popup (lPopup+'subF'+Alltrim(Str(i))) In Screen shortcut Margin Relative && SHADOW COLOR SCHEME 4
            && Alla scelta di una voce di menu chiamo il gestore del popup
            On Selection Popup (lPopup + 'subF'+Alltrim(Str(i))) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
            && Inserisco le Voci taglia Copia Incolla Etc...
            && quando entro la prima Volta
            If lFirst
                *Mnu_Utility((lPopup+'subF'+alltrim(str(i))))
                lFirst=.F.
            Endif
        Else
            LoadMenuItem(lMenu, lPopup+'padF'+Alltrim(Str(i)),i_cName,i_nAction,i_cAction,0,i_cMod,i_bEnabled)
        Endif
        i=i+1
        i_nPos1=At('"'+Chr(13)+Chr(10)+' 2'+Chr(13)+Chr(10)+'"',i_cText1,i)
    Enddo
    Return

Proc LoadMenu(xcg)
    Local N,i,a,p,l,T,i_sp,i_stk,i_nCnt,i_cMod,i_bEnabled
    Dimension i_stk[20,3]
    i_sp=1
    i_stk[1,1]=lMenu
    i_stk[1,2]=1
    i_stk[1,3]=0
    * --- skippa il menu principale
    xcg.LoadHeader('CodePainter Revolution Menu',4,3)
    If xcg.nMajor>=3
        N=xcg.Load('C','')
        N=xcg.Load('C','')
        N=xcg.Load('C','')
        N=xcg.Load('C','')
        N=xcg.Load('C','')
    Endif
    N=xcg.Load('N',0)
    If xcg.nMajor>=3
        xcg.Eoc()
    Endif
    T=xcg.Load('C','')
    l=xcg.Load('N',0)
    p=xcg.Load('C','')
    a=xcg.Load('N',0)
    If xcg.nMajor>=3
        xcg.Load('N',0)
        xcg.Load('N',0)
        xcg.Load('C','')
        xcg.Load('L',.T.)
        xcg.Load('N',0)
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('C','')
        xcg.Load('D',Ctod('  /  /  '))
        xcg.Load('D',Ctod('  /  /  '))
    Endif
    xcg.Eoc()
    * --- ora carica i sottolivelli del menu' principale
    i_nCnt=0
    For i=1 To N-1
        T=xcg.Load('C','')
        l=xcg.Load('N',0)
        p=xcg.Load('C','')
        a=xcg.Load('N',0)
        i_cMod=''
        i_bEnabled=.T.
        If xcg.nMajor>=3
            xcg.Load('N',0)
            xcg.Load('N',0)
            xcg.Load('C','')
            i_bEnabled=xcg.Load('L',.T.)
            xcg.Load('N',0)
            xcg.Load('C','')
            xcg.Load('C','')
            i_cMod=xcg.Load('C','')
            xcg.Load('C','')
            xcg.Load('C','')
            xcg.Load('C','')
            xcg.Load('C','')
        Endif
        xcg.Eoc()
        Do While l<i_stk[i_sp,2] && e' finito il menu, si scende
            i_sp=i_sp-1
        Enddo
        If a=2 && e' un submenu'
            If l<>2
                LoadMenuItem(i_stk[i_sp,1],lPopup+'pad'+Alltrim(Str(i)),T,a,lPopup+'sub'+Alltrim(Str(i)),i_stk[i_sp,3],i_cMod,i_bEnabled)
                Define Popup (lPopup+'sub'+Alltrim(Str(i))) In Screen shortcut Margin Relative && SHADOW COLOR SCHEME 4 && (m.oParentObject.Name)
                On Selection Popup (lPopup+'sub'+Alltrim(Str(i))) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
            Else
                i_nCnt=i_nCnt+1
            Endif
            i_stk[i_sp,3]=i_stk[i_sp,3]+1
            i_sp=i_sp+1
            If l<>2
                i_stk[i_sp,1]=lPopup+'sub'+Alltrim(Str(i))
            Else
                i_stk[i_sp,1]=lPopup+'subF'+Alltrim(Str(i_nCnt))
            Endif
            i_stk[i_sp,2]=l+1
            i_stk[i_sp,3]=1
        Else
            If l<>2
                LoadMenuItem(i_stk[i_sp,1],lPopup+'pad'+Alltrim(Str(i)),T,a,p,i_stk[i_sp,3],i_cMod,i_bEnabled)
            Else
                i_nCnt=i_nCnt+1
            Endif
            i_stk[i_sp,3]=i_stk[i_sp,3]+1
        Endif
    Next
    xcg.Eoc()
    Return

Proc LoadMenuItem(i_menu,i_padname,i_optname,i_action,i_subname,i,i_cMod,i_bEnabled)
    Local i_pos,i_evd,i_cOpz,k,i_cSkip
    Local funzione,FLGT,FLGC,FLGI, lEnabled
    i_cSkip=Iif(i_bEnabled,'',' SKIP FOR .t.')
    *****
    * --- Gestione menu su variabili globali
    If !Empty(i_cMod)
        && if At('(',i_cMod)>0
        i_cMod  = Strtran(i_cMod,"*","'")
        && Per far scrivere meno codice all'interno dello skip
        && del visual menu basta mettere il punto e il nome della variabile
        && il Punto viene tradotto con 'm.oParent'
        && Siccome la lettura avviene alla 'FormLoad' ho tutte le variabili disponibili
        i_cMod = Strtran(i_cMod, '.', 'oParentObject.')
        i_cSkip=' SKIP FOR ' + i_cMod
        * i_cSkip = iif(Eval(i_cMod), i_cSkip, ' SKIP FOR .t.')
    Else
        * if At(','+i_cMod+',',','+i_cModules+',')=0
        *    i_cSkip=' SKIP FOR .t.'
        * endif
        && endif
        i_cSkip=''
    Endif
    *****
    i_cOpz=cp_Translate(i_optname)
    i_pos = At("&",i_cOpz)
    i_evd = Iif(i_pos<>0,Subs(i_cOpz,i_pos+1,1),Left(i_cOpz,1))
    k = 'ALT+'+i_evd
    i_cOpz=Iif(i_pos<>0,Left(i_cOpz,i_pos-1)+'\<'+Trim(Right(i_cOpz,Len(i_cOpz)-i_pos)),'\<'+Alltrim(i_cOpz))
    If i_optname="------------"
        i_action=9
    Endif
    If i_menu = lMenu
        If i_action<>9
            Define Pad (i_padname) Of &i_menu Prompt i_cOpz &i_cSkip && key &k
        Else
            Define Pad (i_padname) Of &i_menu Prompt i_cOpz Skip && key &k
        Endif
        Do Case
            Case i_action=1
                i_subname = Strtran(Trim(i_subname),'#',' with ')
                If "'"$i_subname
                    On Selection Pad (i_padname) Of &i_menu Do SelectionShortCut In PM_EXEC With Pad(), Menu(), oParentObject, gsPopup
                Else
                    On Selection Pad (i_padname) Of &i_menu Do SelectionShortCut In PM_EXEC With Pad(), Menu(), oParentObject, gsPopup
                Endif
            Case i_action=2 Or i_action=3
                *on pad (i_padname) of &i_menu activate popup (i_subname)
        Endcase
        && ho messo una rem nella parte della selezione del Pad altrimenti quando attivo il popup
        && viene attivato anche il pad e non si deve vedere
    Else
        If i_action<>9
            && define bar i of (i_menu) prompt '   '+i_cOpz+'   ' &i_cSkip
            Define Bar i Of (i_menu) Prompt i_cOpz &i_cSkip
        Else
            Define Bar i Of (i_menu) Prompt "\-"
        Endif
        Do Case
            Case i_action=1
                StoreMenu(i_menu, i_action, i, i_subname, i_cOpz)
            Case i_action=2 Or i_action=3
                On Bar (i) Of (i_menu) Activate Popup (i_subname)
        Endcase
    Endif
    Return

Proc StoreMenu
    Param i_menu,i_action,i,i_subname, i_cOpz
    Local lProcName, lParam1, lParam2, lParam3, appo, appo1
    Select (lPopup)
    Scatter Memvar Blank
    i_subname=Alltrim(i_subname)
    appo = At('#', i_subname)
    appo1 = Len(i_subname) - appo
    If appo > 0
        lProcName = Left(i_subname, appo-1)
        Lparam = Right(Alltrim(i_subname), appo1)
    Else
        If appo = 0
            lProcName = i_subname
            Lparam = ''
        Endif
    Endif
    If Not(Empty(i_subname))
        m.Opt_Pad = Alltrim(i_cOpz)
        m.Opt_Name=Upper(Alltrim(i_menu))
        m.Action=i_action
        m.nBar = i
        m.Opt_Proc=lProcName
        If !Empty(Nvl(Lparam,''))
            m.wParam1 = Alltrim(Substr(Lparam, 1, 250))
            m.wParam2 = Alltrim(Substr(Lparam, 251, 500))
            m.wParam3 = Alltrim(Substr(Lparam, 501, 750))
        Else
            m.wParam1 =''
            m.wParam2 =''
            m.wParam3 =''
        Endif
        Select (lPopup)
        Append Blank
        Gather Memvar
    Endif
    Return

Proc Mnu_Utility
    Parameters pMenuName
    Local i,k,p,c,i_nPos,i_nStart,i_nAction,i_cAction,i_cName,i_cMod,i_bEnabled
    Local i_cText,i_nPos1
    Local lMenuName, i_cSkip

    lMenuName =  Upper(Alltrim(pMenuName))
    i_cMod=''
    i_bEnabled=.T.

    i=1
    i_cSkip=' SKIP FOR ' + "inlist(oParentObject.cFunction, 'Query', 'Filter')"
    Define Bar i Of (lMenuName) Prompt cp_Translate(MSG_CUT) &i_cSkip
    On Selection Bar i Of (lMenuName) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
    StoreMenu(lMenuName, 1, i,  cp_Translate(MSG_CUT))
    i=i+1
    i_cSkip=' SKIP FOR ' + "inlist(oParentObject.cFunction, 'Query', 'Filter')"
    Define Bar i Of (lMenuName) Prompt cp_Translate(MSG_COPY) &i_cSkip
    On Selection Bar i Of (lMenuName) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
    StoreMenu(lMenuName, 1, i,  cp_Translate(MSG_COPY))
    i=i+1
    i_cSkip=' SKIP FOR ' + "inlist(oParentObject.cFunction, 'Query', 'Filter')"
    Define Bar i Of (lMenuName) Prompt cp_Translate(MSG_PASTE) &i_cSkip
    On Selection Bar i Of (lMenuName) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
    StoreMenu(lMenuName, 1, i,  cp_Translate(MSG_PASTE))
    i=i+1
    i_cSkip=' SKIP FOR ' + "inlist(oParentObject.cFunction, 'Query', 'Filter')"
    Define Bar i Of (lMenuName) Prompt cp_Translate(MSG_SELECT_ALL_TEXT) &i_cSkip
    On Selection Bar i Of (lMenuName) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
    StoreMenu(lMenuName, 1, i,  cp_Translate(MSG_SELECT_ALL_TEXT))
    i=i+1
    Define Bar i Of (lMenuName) Prompt "\-"
    i=i+1
    i_cSkip=' SKIP FOR ' + "inlist(oParentObject.cFunction, 'Filter')"
    Define Bar i Of (lMenuName) Prompt cp_Translate(MSG_ZOOM) &i_cSkip
    On Selection Bar i Of (lMenuName) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
    StoreMenu(lMenuName, 1, i,  cp_Translate(MSG_ZOOM))
    i=i+1
    Define Bar i Of (lMenuName) Prompt "\-"
    i=i+1
    i_cSkip=' SKIP FOR ' + "inlist(oParentObject.cFunction, 'Query', 'Filter')"
    Define Bar i Of (lMenuName) Prompt cp_Translate(MSG_CUT_ROW) &i_cSkip
    On Selection Bar i Of (lMenuName) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
    StoreMenu(lMenuName, 1, i,  cp_Translate(MSG_CUT_ROW))
    i=i+1
    i_cSkip=' SKIP FOR ' + "inlist(oParentObject.cFunction, 'Filter')"
    Define Bar i Of (lMenuName) Prompt cp_Translate(MSG_COPY_ROW) &i_cSkip
    On Selection Bar i Of (lMenuName) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
    StoreMenu(lMenuName, 1, i,  cp_Translate(MSG_COPY_ROW))
    i=i+1
    i_cSkip=' SKIP FOR ' + "inlist(oParentObject.cFunction, 'Query', 'Filter')"
    Define Bar i Of (lMenuName) Prompt cp_Translate(MSG_PASTE_ROW) &i_cSkip
    On Selection Bar i Of (lMenuName) Do SelectionShortCut In PM_EXEC With Bar(), Popup(), oParentObject, gsPopup
    StoreMenu(lMenuName, 1, i,  cp_Translate(MSG_PASTE_ROW))
    i=i+1
    Define Bar i Of (lMenuName) Prompt "\-"
    Return
