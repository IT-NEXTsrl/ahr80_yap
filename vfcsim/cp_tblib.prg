* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_TBLIB
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Samuele Masetto
* Data creazione: 17/09/97
* Aggiornato il : 17/09/97
* Translated:     03/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Libreria di funzioni per la creazione/inserimento di una tabella non amministrata
* Utilizzata per il trasferimento (import/export) di dati tra due tabelle
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Func InsertOneTable(i_nConn,i_cTable,i_cCursor)
    * --- copia il cursore "i_cCursor" nella tabella "i_cTable" nella connessione "i_nConn"
    Local i_nErr,bFirst,cNField,cErrTable
    bFirst=.T.
    i_nErr=0
    i_bInsertError=.F.
    i_bEndInsertError=.F.
    cNField=FieldList(i_cCursor)
    cErrTable=Stuff(i_cTable,Rat('\',i_cTable)+1, 0,'E_')
    cErrTable=cErrTable+'.ERR'
    Select (i_cCursor)
    Scan
        i_nErr=InsertRowInTable(i_nConn,i_cTable,i_cCursor)
        * controllo degli errori nell'inserimento delle righe
        If i_nErr=-1
            If bFirst
                Delete File (cErrTable)
                * crea la tabella delle righe di errore
                Copy To &cErrTable Next 1
                Use (cErrTable) In 0 Alias 'tblerr'
                bFirst=.F.
            Else
                * inserisci la riga che ha generato un errore
                Set Safety Off
                Copy To DBERR1 Next 1
                Set Safety On
                Select tblerr
                Append From DBERR1
                Select (i_cCursor)
            Endif
        Else
            If i_nErr=-2
                Exit
            Endif
        Endif
    Endscan
    If Used('tblerr')
        i_bInsertError=.T.
        Select tblerr
        Use
        Delete File DBERR1.Dbf
        i_bEndInsertError=.T.
    Endif
    On Error
    Return(i_bEndInsertError)

Func InsertRowInTable(i_nConn,i_cTable,i_cCursor)
    * --- inserisce la riga corrente del cursore "i_cCursor" nella tabella
    * --- "i_cTable" nella connessione "i_nConn"
    Local i,i_cFlds,i_cValues,i_nSQLResult

    i_nSQLResult=0
    Select (i_cCursor)
    If Mod(Recno(),50)=0
        CP_MSG(cp_MsgFormat(MSG_RECORD_CL__BS__,Alltrim(Str(Recno())),Alltrim(Str(Reccount()))))
    Endif
    i_cFlds=''
    i_cValues=''
    For i=1 To Fcount()
        i_cFlds=i_cFlds+Iif(Empty(i_cFlds),'',',')+Field(i)
        If i_nConn=0
            i_cValues=i_cValues+Iif(Empty(i_cValues),'',',')+cp_ToStr(Eval(Field(i)))
        Else
            i_cValues=i_cValues+Iif(Empty(i_cValues),'',',')+cp_ToStrODBC(Eval(Field(i)))
        Endif
    Next
    If i_nConn<>0
        i_nSQLResult=cp_sqlexec(i_nConn,'insert into '+i_cTable+' ('+i_cFlds+') values ('+i_cValues+')')
    Else
        On Error i_nSQLResult=-1
        Insert Into &i_cTable ( &i_cFlds ) Values ( &i_cValues )
        On Error
    Endif
    If i_nSQLResult<0
        i_nRet=Messagebox(cp_MsgFormat(MSG_ERROR_CL_INSERT_INTO__F_ERR__VALUES__,i_cTable,i_cFlds,i_cValues),1)
        If i_nRet=2
            i_nSQLResult=-2
        Endif
    Endif
    Return(i_nSQLResult)

Func FieldList(i_cDb)
    Local i_cNField,i_acType,i_nCount,i_nfld
    Dimension i_acType[1]
    Select (i_cDb)
    i_nfld=Afields(i_acType)
    i_cNField=''
    For i_nCount=1 To i_nfld
        If i_acType[i_nCount,2] = 'N'
            i_cNField=i_cNField+' ,'+i_acType[i_nCount,1]+' '+i_acType[i_nCount,2]+'('+Alltrim(Str(i_acType[i_nCount,3]))+','+Alltrim(Str(i_acType[i_nCount,4]))+')'
        Else
            If i_acType[i_nCount,2] = 'C'
                i_cNField =i_cNField+' ,'+i_acType[i_nCount,1]+' '+i_acType[i_nCount,2]+'('+Alltrim(Str(i_acType[i_nCount,3]))+')'
            Else
                If i_acType[i_nCount,2] = 'D'
                    i_cNField =i_cNField+' ,'+i_acType[i_nCount,1]+' '+i_acType[i_nCount,2]
                Endif
            Endif
        Endif
    Next
    i_cNField=Substr(i_cNField,3)
    Return(i_cNField)

Func FieldListODBC(i_cDb,i_cDBType)
    Local i_cNField,i_acType,i_cFieldType,i_nCount,i_nfld
    Dimension i_acType[1]
    Select (i_cDb)
    i_nfld=Afields(i_acType)
    i_cNField=''
    For i_nCount=1 To i_nfld
        i_cFieldType=db_FieldType(i_cDBType,i_acType[i_nCount,2],i_acType[i_nCount,3],i_acType[i_nCount,4])
        i_cNField =i_cNField+' ,'+i_acType[i_nCount,1]+' '+i_cFieldType
    Next
    i_cNField=Substr(i_cNField,3)
    Return(i_cNField)

