* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_savefor_b                                                    *
*              Lancia cp_savefor                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-23                                                      *
* Last revis.: 2011-05-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_savefor_b",oParentObject)
return(i_retval)

define class tcp_savefor_b as StdBatch
  * --- Local variables
  w_original_file_name = space(10)
  w_NEWFILE_NAME = space(10)
  w_edit_condition = .f.
  w_vrt_condition = .f.
  w_OK = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da vq_build
    *     Lancia cp_savefor e valorizza original_file_name
    this.w_original_file_name = original_file_name
    this.w_edit_condition = .F.
    this.w_vrt_condition = .F.
    this.w_OK = .F.
    do cp_savefor with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    original_file_name = this.w_NEWFILE_NAME
    this.bUpdateParentObject=.F.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
