* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VRT_BUILD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Modificatore di formn al run-time
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

*--- Massima lunghezza espressione
#Define MAX_CTRL_LENGHT 1000

*--- Zucchetti Aulla inizio - Framework monitor
* blocca l'oggetto a chi non ha i diritti
*---
If cp_set_get_right()<2
    cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
    Return
Endif
*--- Zucchetti Aulla fine - Framework monitor

If At('vm_li',Lower(Set('proc')))=0 Or At('cp_draw',Lower(Set('proc')))=0 ;
        OR At('vq_lib',Lower(Set('proc')))=0
    Set Proc To vm_lib,cp_draw,vq_lib Additive
Endif

If Vartype(i_curform)='O' And !Isnull(i_curform)
    Local i_class
    i_class=vrtClass(i_curform)
    If Inlist(i_class,"Stdform","Stdpcform","Stdtrsform")
        Local i_oForm,i_rtform
        Do Case
            Case Inlist(i_class,"Stdform","Stdtrsform")
                i_oForm=i_curform
            Case i_class=="Stdpcform"
                i_oForm=i_curform.Cnt
        Endcase
        If Empty(i_oForm.cRuntimeConfiguration)
            i_rt=Createobject('RT_toolbar',i_oForm,i_curform,Substr(i_curform.Class,2), Vartype(_Screen.cp_ThemesManager)=="O" And i_VisualTheme<>-1)
            i_curform=i_rt
            *--- Zucchetti Aulla inizio - Framework monitor
            If (Vartype(i_designfile)='C')
                i_rt.cFileName = Alltrim(i_designfile)
                i_rt.SetTitle()
                i_rt.LoadDoc()
            Endif
            *--- Zucchetti Aulla inizio - Framework monitor
            i_rt.EditMask()
        Else
            cp_msg(cp_Translate(MSG_UNABLE_TO_EDIT_A_FORM_WITH_A_RUNTIME_CONFIGURATION),.F.)
        Endif
    Else
        cp_msg(cp_Translate(MSG_NO_VALID_FORM_SELECTED))
    Endif
Endif

Define Class RT_toolbar As microtool_toolbar
    oBaseForm=.Null.
    oFormContainer=.Null.
    cClassName=''
    oForm=.Null.
    oThis=.Null.
    cToolName='Visual RUN-TIME'
    cExt='vrt'
    cClipBoard=''
    oReEnable=.Null.
    Caption="RunTime toolbar"
    *
    bSec1=.T.
    bSec2=.T.
    bSec3=.T.
    bSec4=.T.
    *
    bFox26=.F.

    HighlightCheck = .F.

    Proc Init(i_oBaseForm,i_oFormContainer,i_cClassName,bStyle)
        DoDefault(m.bStyle)
        Local nDimension, oBtn
        With This
            If Vartype(i_bFox26)='L'
                If i_bFox26
                    .bFox26=.T.
                Endif
            Endif
            If !.bFox26
                cp_GetSecurity(This,'VisualRT')
            Endif
            .oBaseForm=i_oBaseForm
            .oFormContainer=i_oFormContainer
            .oForm=Createobject('vrt_frm',This,i_oBaseForm,i_oFormContainer,i_cClassName)
            .Dock(i_nCPToolBarPos)
            .oThis=This
            .cClassName=i_cClassName
            *--- Separatore
            .AddObject("sep1","Separator")
            .sep1.Visible=.T.
            *--- Event Btn
            m.oBtn = .AddButton("evt")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "flash.bmp"
            m.oBtn._OnClick = "This.parent.evt_Click()"
            m.oBtn.ToolTipText = "Events"
            m.oBtn.Visible=.T.
            *--- HighlightMod Btn
            m.oBtn = .AddButton("hlm")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "Highlight.bmp"
            m.oBtn._OnClick = "This.parent.HighlightMod_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_HIGHLIGHTMOD)
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("sep3","Separator")
            .sep3.Visible=.T.
            *--- Separatore
            .AddObject("sep4","Separator")
            .sep1.Visible=.T.
            *--- allleft Btn
            m.oBtn = .AddButton("allleft")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignl.bmp"
            m.oBtn._OnClick = "This.parent.allleft_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_ALIGN_LEFT)
            m.oBtn.Visible=.T.
            *--- allright Btn
            m.oBtn = .AddButton("allright")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignr.bmp"
            m.oBtn._OnClick = "This.parent.allright_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_ALIGN_RIGHT)
            m.oBtn.Visible=.T.
            *--- alltop Btn
            m.oBtn = .AddButton("alltop")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignt.bmp"
            m.oBtn._OnClick = "This.parent.alltop_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_ALIGN_TOP)
            m.oBtn.Visible=.T.
            *--- allbottom Btn
            m.oBtn = .AddButton("allbottom")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignb.bmp"
            m.oBtn._OnClick = "This.parent.allbottom_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_ALIGN_BOTTOM)
            m.oBtn.Visible=.T.
            *--- allsameh Btn
            m.oBtn = .AddButton("allsameh")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignsh.bmp"
            m.oBtn._OnClick = "This.parent.allsameh_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_SAME_HORIZONTAL_DISTANCE)
            m.oBtn.Visible=.T.
            *--- allsamev Btn
            m.oBtn = .AddButton("allsamev")
            m.oBtn.Caption = ""
            m.oBtn.cImage = "alignsv.bmp"
            m.oBtn._OnClick = "This.parent.allsamev_Click()"
            m.oBtn.ToolTipText = cp_Translate(MSG_SAME_VERTICAL_DISTANCE)
            m.oBtn.Visible=.T.
        Endwith
    Endproc
    Proc Destroy()
        If !Isnull(This.oReEnable)
            This.oReEnable.Show()
            This.oReEnable.oTool.Show()
        Endif
        DoDefault()
    Proc EditMask()
        If !This.bSec1
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            This.Exit()
        Else
            This.Show()
            This.oForm.Show()
        Endif
        Return
    Proc Exit()
        This.oForm=.Null.
        This.oThis=.Null.
        i_curform=.Null.
        This.Visible=.F.
        This.Destroy()
        Return
    Func GetHelpTopic()
        Return('vm_build')
    Proc NewDoc()
        This.oForm.New()
        Return
    Proc LoadDoc()
        Local xcg,c,h,F,l
        * --- nuovo documento
        c=cp_GetPFile(This.cFileName)
        This.oForm.New()
        xcg=Createobject('pxcg','load',c)
        This.Serialize(xcg)
        This.bModified=.F.
        This.oForm.Caption=This.oBaseForm.cComment+' ['+This.cFileName+']'
    Func GetFileName()
        If Empty(This.cFileName)
            Local fnw,i_olderr
            i_olderr=On('ERROR')
            On Error =.T.
            fnw=This.cClassName+'*.vrt'
            This.cFileName=Locfile(fnw,This.cExt)
            On Error &i_olderr
            If !Empty(This.cFileName)
                This.SetTitle()
            Endif
        Endif
        Return(Not(Empty(This.cFileName)))
    Func PutFileName()
        If Empty(This.cFileName)
            This.cFileName=Putfile('File:',This.cClassName+'*.vrt',This.cExt)
            This.SetTitle()
        Endif
        Return(Not(Empty(This.cFileName)))
    Proc SaveDoc()
        Local xcg,h
        xcg=Createobject('pxcg','save')
        This.Serialize(xcg)
        This.bModified=.F.
        h=Fcreate(This.cFileName)
        =Fwrite(h,xcg.cText)
        =Fclose(h)
        i_rtcfgs=''
    Proc Serialize(xcg)
        If xcg.bIsStoring
            xcg.SaveHeader('Visual Run-Time',1,0)
        Else
            xcg.LoadHeader('Visual Run-Time',1,0)
        Endif
        This.oForm.Serialize(xcg)
        *
        * --- procedure dei bottoni
        *
    Proc evt_Click()
        Local o
        o=Createobject('vrt_events_form',This.oForm)
        o.Show()
    Proc HighlightMod_Click()
        This.HighlightCheck = !This.HighlightCheck
        This.hlm.cImage = Iif(This.HighlightCheck, "Highlight_Check.bmp", "Highlight.bmp")
        This.oForm.HighlightMod(!This.HighlightCheck)
    Proc allleft_click()
        Local o
        o=Createobject('draw_align')
        o.AlignLeft(This.oForm.GetDrw())
        This.bModified=.T.
    Proc allright_click()
        Local o
        o=Createobject('draw_align')
        o.Alignright(This.oForm.GetDrw())
        This.bModified=.T.
    Proc alltop_click()
        Local o
        o=Createobject('draw_align')
        o.AlignTop(This.oForm.GetDrw())
        This.bModified=.T.
    Proc allbottom_click()
        Local o
        o=Createobject('draw_align')
        o.AlignBottom(This.oForm.GetDrw())
        This.bModified=.T.
    Proc allsameh_click()
        Local o
        o=Createobject('draw_align')
        o.SameHDist(This.oForm.GetDrw())
        This.bModified=.T.
    Proc allsamev_click()
        Local o
        o=Createobject('draw_align')
        o.SameVDist(This.oForm.GetDrw())
        This.bModified=.T.
    Proc ecpSecurity()
        Createobject('stdsProgAuth','VisualRT')
        Return
Enddefine

Define Class vrt_frm As CpSysform
    Width=400
    Closable=.F.
    oTool=.Null.
    oBaseForm=.Null.
    oFormContainer=.Null.
    cClassName=''
    Caption='Draw'
    cModal='S'
    cScreen='Form'
    nPages=0
    Icon=i_cBmpPath+i_cStdIcon
    bFormMoved=.F.
    Dimension aShpNames[1]
    aShpNames[1]=''
    Dimension aShpCtrls[1]
    aShpCtrls[1]=''
    Dimension aShpPages[1]
    aShpPages[1]=0
    Dimension cEvents[2]
    cEvents[1]=''
    cEvents[2]=''
    Proc Init(i_oTool,i_oBaseForm,i_oFormContainer,i_cClassName)
        This.oTool=i_oTool
        This.oBaseForm=i_oBaseForm
        This.oFormContainer=i_oFormContainer
        This.cClassName=i_cClassName
        This.FillFromBase()
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
    Proc New()
        On Error
        This.RemoveObject('oPgFrm')
        Dimension This.aShpNames[1]
        Dimension This.aShpCtrls[1]
        Dimension This.aShpPages[1]
        This.aShpPages[1]=0
        This.FillFromBase()
    Proc FillFromBase()
        Local npag,i
        npag=This.oBaseForm.oPgFrm.PageCount
        This.nPages=npag
        * ---
        This.AddObject('oPgFrm','pageframe')
        This.oPgFrm.PageCount=npag
        This.oPgFrm.TabStyle=1
        For i=1 To npag
            * --- istanzia i figli integrati di pagina
            If Type('this.oBaseForm.oPgFrm.Pages(i).autozoom')='U' And Type('this.oBaseForm.oPgFrm.Pages(i).opag')<>"U"
                This.oBaseForm.oPgFrm.Pages(i).opag.UIEnable(.T.)
            Endif
            This.oPgFrm.Pages(i).Caption=This.oBaseForm.oPgFrm.Pages(i).Caption
            This.oPgFrm.Pages(i).AddObject('drw','rtdraw_board')
            This.oPgFrm.Pages(i).drw.nDY=28
            This.oPgFrm.Pages(i).drw.Visible=.T.
            This.FillPageControls(i)
        Next
        If Type('this.oBaseForm.oPgFrm.Pages(1).oPag.oBody.oBodyCol.oRow')='O'
            * -- il form possiede il transitorio
            This.AddBodyDrawBoard()
            This.FillControls(This.oBaseForm.oPgFrm.Pages(1).opag.oBody.oBodyCol.oRow,This.oPgFrm.Pages(1).bodydrw,-1)
        Endif
        If npag=1
            This.oPgFrm.Tabs=.F.
            This.oPgFrm.SpecialEffect=2
            This.oPgFrm.Pages(1).drw.nDY=0
        Endif
        This.oPgFrm.Visible=.T.
        This.Top=This.oFormContainer.Top &&+20 sta bene proprio sopra!
        This.Left=This.oFormContainer.Left &&+20
        This.Width=This.oFormContainer.Width
        This.Height=This.oFormContainer.Height
        This.Caption=This.cClassName+' / RUN-TIME'
        This.Resize()
        This.oTool.bModified=.F.
        This.bFormMoved=.F.
    Proc FillPageControls(i_nPag)
        If Type("this.oBaseForm.oPgFrm.Pages(i_nPag).oPag")='O'
            This.FillControls(This.oBaseForm.oPgFrm.Pages(i_nPag).opag,This.oPgFrm.Pages(i_nPag).drw,i_nPag)
        Endif
        Return
    Proc AddBodyDrawBoard()
        This.oPgFrm.Pages(1).AddObject('body','container')
        This.oPgFrm.Pages(1).body.Top=This.oBaseForm.oPgFrm.Pages(1).opag.oBody.Top-1
        This.oPgFrm.Pages(1).body.Left=This.oBaseForm.oPgFrm.Pages(1).opag.oBody.Left-1
        This.oPgFrm.Pages(1).body.Width=This.oBaseForm.oPgFrm.Pages(1).opag.oBody.Width+2
        This.oPgFrm.Pages(1).body.Height=This.oBaseForm.oPgFrm.Pages(1).opag.oBody.Height
        This.oPgFrm.Pages(1).body.Visible=.T.
        This.oPgFrm.Pages(1).AddObject('bodydrw','rtdraw_board')
        This.oPgFrm.Pages(1).bodydrw.Top=This.oBaseForm.oPgFrm.Pages(1).opag.oBody.Top
        This.oPgFrm.Pages(1).bodydrw.Left=This.oBaseForm.oPgFrm.Pages(1).opag.oBody.Left
        This.oPgFrm.Pages(1).bodydrw.Width=This.oBaseForm.oPgFrm.Pages(1).opag.oBody.Width
        This.oPgFrm.Pages(1).bodydrw.Height=This.oBaseForm.oPgFrm.Pages(1).opag.oBody.oBodyCol.oRow.Height+2
        This.oPgFrm.Pages(1).bodydrw.BackColor=Rgb(255,255,255)
        This.oPgFrm.Pages(1).bodydrw.Visible=.T.
        Return
    Proc FillControls(i_oFromPage,i_oDrw,i_nPag)
        Local i,ctrl,N,ctrlclass
        For i=1 To i_oFromPage.ControlCount
            ctrl=i_oFromPage.Controls(i)
            ctrlclass=vrtClass(ctrl)
            Do Case
                Case Inlist(ctrlclass,'Stdfield','Stdmemo')
                    N=i_oDrw.AddItem('rtpainter_txt',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    i_oDrw.&N..oCtrl=ctrl
                    *this.oPgFrm.Pages(i_nPag).drw.&n..bModified=.f.
                    This.AddShp(N,i_nPag,ctrl.uid)
                Case Inlist(ctrlclass,'Stdtrsfield','Stdtrsmemo')
                    N=i_oDrw.AddItem('rtpainter_trstxt',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    i_oDrw.&N..oCtrl=ctrl
                    *this.oPgFrm.Pages(i_nPag).drw.&n..bModified=.f.
                    This.AddShp(N,i_nPag,ctrl.uid)
                Case ctrlclass='Stdstring'
                    N=i_oDrw.AddItem('rtpainter_lbl',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    i_oDrw.&N..cVarName=ctrl.Caption
                    i_oDrw.&N..cAlign=Iif(ctrl.Alignment=1,'R',Iif(ctrl.Alignment=2,'C',' '))
                    i_oDrw.&N..obj.Caption=ctrl.Caption
                    i_oDrw.&N..Alignment(ctrl.Alignment)
                    i_oDrw.&N..oCtrl=ctrl
                    *this.oPgFrm.Pages(i_nPag).drw.&n..bModified=.f.
                    This.AddShp(N,i_nPag,ctrl.uid)
                Case Inlist(ctrlclass,'Stdcheck','Stdtrscheck')
                    N=i_oDrw.AddItem('rtpainter_check',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    i_oDrw.&N..obj.Caption=ctrl.Caption
                    i_oDrw.&N..oCtrl=ctrl
                    *i_oDrw.&n..bModified=.f.
                    This.AddShp(N,i_nPag,ctrl.uid)
                Case Inlist(ctrlclass,'Stdradio','Stdtrsradio')
                    N=i_oDrw.AddItem('rtpainter_radio',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    i_oDrw.&N..oCtrl=ctrl
                    *i_oDrw.&n..bModified=.f.
                    i_oDrw.&N..SetLabels()
                    This.AddShp(N,i_nPag,ctrl.uid)
                    i_oDrw.&N..bModified=.F.
                Case Inlist(ctrlclass,'Stdcombo','Stdtrscombo')
                    N=i_oDrw.AddItem('rtpainter_combo',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    i_oDrw.&N..oCtrl=ctrl
                    *i_oDrw.&n..bModified=.f.
                    This.AddShp(N,i_nPag,ctrl.uid)
                Case ctrlclass='Stdbutton'
                    N=i_oDrw.AddItem('rtpainter_btn',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    i_oDrw.&N..obj.Picture=ctrl.Picture && PAOLO Per identificare il bottone...
                    i_oDrw.&N..obj.Caption=ctrl.Caption
                    i_oDrw.&N..oCtrl=ctrl
                    *i_oDrw.&n..bModified=.f.
                    This.AddShp(N,i_nPag,ctrl.uid)
                Case ctrlclass='Stdbox'
                    N=i_oDrw.AddItem('rtpainter_box',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    i_oDrw.&N..oCtrl=ctrl
                    *i_oDrw.&n..bModified=.f.
                    This.AddShp(N,i_nPag,ctrl.uid)
                Case Inlist(ctrlclass,"Stdpccontainer","Stdpctrscontainer")
                    N=i_oDrw.AddItem('rtpainter_pccontainer',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    i_oDrw.&N..oCtrl=ctrl
                    *i_oDrw.&n..bModified=.f.
                    i_oDrw.&N..SetPCName(Substr(ctrl.Class,3))
                    This.AddShp(N,i_nPag,ctrl.uid)
                Case Inlist(ctrlclass,"Stddynamicchildcontainer")
                    N=i_oDrw.AddItem('rtpainter_pcDynamic',ctrl.Left,ctrl.Top,ctrl.Width,ctrl.Height)
                    If Type('ctrl.objects[1].Class')='C'
                        i_oDrw.&N..oCtrl=ctrl.Objects[1]
                        *i_oDrw.&n..bModified=.f.
                        i_oDrw.&N..SetPCName(Substr(ctrl.Objects[1].Class,3))
                    Else
                        i_oDrw.&N..oCtrl=Null
                        i_oDrw.&N..SetPCName('')
                    Endif
                    This.AddShp(N,i_nPag,ctrl.uid)
            Endcase
        Next
        Return
    Func GetDrw()
        Return(This.oPgFrm.Pages(This.oPgFrm.ActivePage).drw)
    Proc Resize()
        Local i
        This.oPgFrm.Width=This.Width
        This.oPgFrm.Height=This.Height
        If This.nPages>1
            For i=1 To This.nPages
                This.oPgFrm.Pages(i).drw.Width=This.Width-4
                This.oPgFrm.Pages(i).drw.Height=This.Height-30
            Next
        Else
            This.oPgFrm.Pages(1).drw.Width=This.Width-2
            This.oPgFrm.Pages(1).drw.Height=This.Height-2
            This.oPgFrm.Pages(1).drw.BorderWidth=0
            *this.drw.width=this.width
            *this.drw.height=this.height
        Endif
        This.oTool.bModified=.T.
        This.bFormMoved=.T.
    Proc Moved()
        DoDefault()
        This.bFormMoved=.T.
    Proc AddShp(cShp,npag,cName)
        Local N
        N=Alen(This.aShpPages)
        If N<>1 Or This.aShpPages[1]<>0
            N=N+1
            Dimension This.aShpPages[n]
            Dimension This.aShpCtrls[n]
            Dimension This.aShpNames[n]
        Endif
        This.aShpPages[n]=npag
        This.aShpCtrls[n]=cShp
        This.aShpNames[n]=cName
    Proc Serialize(xcg)
        Local shp,pag,i
        If xcg.bIsStoring
            If This.bFormMoved
                If This.Top<>This.oFormContainer.Top
                    This.SaveProp(xcg,'form','top',Alltrim(Str(This.Top)))
                Endif
                If This.Left<>This.oFormContainer.Left
                    This.SaveProp(xcg,'form','left',Alltrim(Str(This.Left)))
                Endif
                If This.Width<>This.oFormContainer.Width
                    This.SaveProp(xcg,'form','width',Alltrim(Str(This.Width)))
                Endif
                If This.Height<>This.oFormContainer.Height
                    This.SaveProp(xcg,'form','height',Alltrim(Str(This.Height)))
                Endif
            Endif
            For Each pag In This.oPgFrm.Pages
                * --- salva le varie pagine
                pag.drw.DeselectAll()
                For Each shp In pag.drw.Controls
                    This.SerializeACtrl(xcg,shp)
                Next
            Next
            If Type('this.oPgFrm.Pages(1).bodydrw')='O'
                * --- salva gli elementi del corpo
                This.oPgFrm.Pages(1).bodydrw.DeselectAll()
                For Each shp In This.oPgFrm.Pages(1).bodydrw.Controls
                    This.SerializeACtrl(xcg,shp)
                Next
            Endif
            For i=1 To Alen(This.cEvents) Step 2
                If !Empty(This.cEvents[i])
                    This.SaveProp(xcg,'event',This.cEvents[i],This.cEvents[i+1])
                Endif
            Next
            xcg.Eoc()
        Else
            *this.lockscreen=.t.
            * --- sta caricando
            Local i_cCtrl,i_cName,i_cValue
            Do While This.GetProp(xcg,@i_cCtrl,@i_cName,@i_cValue)
                *wait window i_cCtrl+','+i_cName+','+i_cValue
                If Lower(i_cName)+','$'top,left,width,height,'
                    * --- ora se e' una proprieta' da applicare (top,left,width,height,forecolor,backcolor) la applica
                    This.ApplyProp(i_cCtrl,i_cName,i_cValue)
                Else
                    * -- altrimenti le gestisce, alcune in modo diretto, altre in un array di proprieta'
                    This.AddProp(i_cCtrl,i_cName,i_cValue)
                Endif
            Enddo
            *this.lockscreen=.f.
        Endif
        Return
    Proc SerializeACtrl(xcg,shp)
        If shp.bModified
            If shp.Top<>shp.oCtrl.Top
                This.SaveProp(xcg,shp.GetID(),'top',Alltrim(Str(shp.Top)))
            Endif
            If shp.Left<>shp.oCtrl.Left
                This.SaveProp(xcg,shp.GetID(),'left',Alltrim(Str(shp.Left)))
            Endif
            If shp.Width<>shp.oCtrl.Width
                This.SaveProp(xcg,shp.GetID(),'width',Alltrim(Str(shp.Width)))
            Endif
            If shp.Height<>shp.oCtrl.Height
                This.SaveProp(xcg,shp.GetID(),'height',Alltrim(Str(shp.Height)))
            Endif
            If shp.bHidden
                This.SaveProp(xcg,shp.GetID(),'visible','.f.')
            Endif
            If Inlist(shp.Class,'Rtpainter_lbl','Rtpainter_btn') And shp.obj.Caption<>shp.oCtrl.Caption
                This.SaveProp(xcg,shp.GetID(),'caption',shp.obj.Caption)
            Endif
            If shp.Class='Rtpainter_lbl' And shp.nAlignment<>shp.oCtrl.Alignment
                This.SaveProp(xcg,shp.GetID(),'alignment',Alltrim(Str(shp.nAlignment)))
                *wait window "ha salvato l' allineamento "+str(shp.nAlignment)+' '+str(shp.oCtrl.Alignment)
            Endif
            For i=1 To Alen(shp.sProps)-1 Step 2
                If !Empty(shp.sProps[i])
                    *wait window 'save prop '+shp.GetID()+' '+shp.sProps[i]+' '+shp.sProps[i+1]
                    This.SaveProp(xcg,shp.GetID(),shp.sProps[i],shp.sProps[i+1])
                Endif
            Next
        Endif
        Return
    Proc SaveProp(xcg,i_cCtrl,i_cName,i_cValue)
        xcg.Save('C',i_cCtrl)
        xcg.Save('C',i_cName)
        xcg.Save('C',i_cValue)
    Func GetProp(xcg,i_cCtrl,i_cName,i_cValue)
        i_cCtrl=xcg.Load('C','')
        If !Empty(i_cCtrl)
            i_cName=xcg.Load('C','')
            i_cValue=xcg.Load('C','')
            Return(.T.)
        Else
            Return(.F.)
        Endif
        *--- Evidenzia elementi modificati
    Procedure HighlightMod(bNoHighlight)
        Local drw,N,nn
        For N=1 To Alen(This.aShpNames)
            drw=This.GetDrwFromIdx(This.aShpPages[n])
            nn=This.aShpCtrls[n]
            drw.&nn..bHighlight= drw.&nn..bModified And !m.bNoHighlight
        Next
    Endproc

    Proc ApplyProp(i_cCtrl,i_cName,i_cValue)
        If i_cCtrl='form'
            This.&i_cName=&i_cValue
        Else
            * --- cerca rapidamente nell' array delle shapes
            Local N,nn,drw
            N=Ascan(This.aShpNames,i_cCtrl)
            * --- applica la proprieta'
            If N<>0
                nn=This.aShpCtrls[n]
                *this.oPgFrm.Pages(this.aShpPages[n]).drw.&nn..&i_cName=&i_cValue
                drw=This.GetDrwFromIdx(This.aShpPages[n])
                drw.&nn..&i_cName=&i_cValue
                drw.&nn..bModified=.T.
            Endif
        Endif
    Proc AddProp(i_cCtrl,i_cName,i_cValue)
        Local N,nn,drw
        If i_cCtrl<>'event'
            N=Ascan(This.aShpNames,i_cCtrl)
            If N<>0
                nn=This.aShpCtrls[n]
                drw=This.GetDrwFromIdx(This.aShpPages[n])
                drw.&nn..bModified=.T.
                *wait window nn+' '+i_cName+' '+i_cValue
                Do Case
                    Case i_cName='visible'
                        drw.&nn..Hide(.T.)
                    Case i_cName='alignment'
                        drw.&nn..Alignment(Val(i_cValue))
                    Case i_cName='caption'
                        *wait window nn+' '+i_cName+' '+i_cValue
                        drw.&nn..obj.Caption=i_cValue
                    Otherwise
                        *wait window nn+' '+i_cName+' '+i_cValue
                        drw.&nn..AddProp(i_cName,i_cValue)
                Endcase
            Endif
        Else
            *wait window i_cCtrl+' '+i_cName+' '+i_cValue
            * -- eventi
            N=Alen(This.cEvents)
            If N>2 Or !Empty(This.cEvents[1])
                N=N+2
                Dimension This.cEvents[n]
            Endif
            This.cEvents[n-1]=i_cName
            This.cEvents[n]=i_cValue
        Endif
        Return
    Func GetDrwFromIdx(i_n)
        If (i_n=-1)
            Return(This.oPgFrm.Pages(1).bodydrw)
        Endif
        Return(This.oPgFrm.Pages(i_n).drw)
Enddefine

Define Class rtdraw_board As draw_board
    Proc SetModified(bStatus)
        If Type("this.parent.oTool.bModified")='L'
            * -- e' inserito direttamente nel form
            This.Parent.oTool.bModified=bStatus
        Else
            * -- il form ha pagine e pageframe
            This.Parent.Parent.Parent.oTool.bModified=bStatus
        Endif
        DoDefault(bStatus)
    Proc DragDrop(oSource,x,Y)
        Local cFName,cOldFName,bOk,cDefPath
        Local oTool
        If Type("this.parent.oTool.bModified")='L'
            oTool=This.Parent.oTool
        Else
            oTool=This.Parent.Parent.Parent.oTool
        Endif
        cDefPath=Sys(5)+Sys(2003)+'\'
        Do Case
            Case Upper(oSource.Name)='STDSUSR' Or Upper(oSource.Name)='STDSGRP'
                * drag iniziato nella lista degli utenti del tool di gestione utenti
                If Upper(Left(oTool.oBaseForm.ParentClass,5))='STDPC'
                    * Se figlio la classe � prefissata da tc, per gli altri da t
                    TmpS=Substr(oTool.oBaseForm.Class,3)
                Else
                    TmpS=Substr(oTool.oBaseForm.Class,2)
                Endif
                * Run Time caso Drop utenti costruzione nome file
                cFName=TmpS+Iif(Upper(oSource.Name)='STDSGRP','_g'+Alltrim(Str(_cpgroups_.Code)),'_'+Alltrim(Str(_cpusers_.Code)))+'.vrt'
                cOldFName=oTool.cFileName
                If At(Upper(cFName),Upper(oTool.cFileName))<>0
                    cFName=oTool.cFileName
                Endif
                oTool.cFileName=cFName
                bOk=.T.
                If At("\",cFName)<>0
                    cDefPath=''
                Endif
                *--- Zucchetti Aulla Inizio - Monitor Framework
                *--- Se il monitor � attivo verifico i diritti
                Local l_UsrRight
                l_UsrRight = cp_set_get_right()
                Do Case
                    Case l_UsrRight = 2   &&Read Only
                        oTool.cFileName=cOldFName
                        Return
                    Case l_UsrRight = 3
                        If Upper(oSource.Name)='STDSUSR'
                            If i_CodUte<>_cpusers_.Code
                                oTool.cFileName=cOldFName
                                Return
                            Endif
                        Else
                            If Ascan(i_aUsrGrps, _cpgroups_.Code) = 0
                                oTool.cFileName=cOldFName
                                Return
                            Endif
                        Endif
                        cFName = Forcepath(Upper(cFName), Fullpath(i_usrpath))
                    Case l_UsrRight = 4
                        cFName = Forcepath(Upper(cFName), Fullpath(i_usrpath))
                    Otherwise
                        cFName = Iif(Not Empty(cDefPath), Forcepath(Upper(cFName), cDefPath), Upper(cFName))
                Endcase
                oTool.cFileName=cFName
                *--- Zucchetti Aulla Fine - Monitor Framework
                If cp_fileexist(cFName)
                    *bOk=cp_YesNo(cp_MsgFormat(MSG_FILE____ALREADY_EXIST_C_SUBSTITUTE_QP,cDefPath,upper(cFName)),48,cp_translate(MSG_SAVE),.f.) && Zucchetti aulla riga sostituita con quella sottostante
                    bOk=cp_YesNo(cp_MsgFormat(MSG_FILE____ALREADY_EXIST_C_SUBSTITUTE_QP, cFName, ''),48,.f.,.f.,MSG_SAVE)
                Else
                    *bOk=cp_YesNo(cp_MsgFormat(MSG_SAVE_FILE_____QP,cDefPath,upper(cFName)),32,cp_translate(MSG_SAVE),.f.)&& Zucchetti aulla riga sostituita con quella sottostante
                    bOk=cp_YesNo(cp_MsgFormat(MSG_SAVE_FILE__QP, cFName),32,.f.,.f.,MSG_SAVE)
                ENDIF
                
                If bOk
                *--- Zucchetti Aulla Inizio - Monitor Framework
                 #Define FILEDESC_IDX 2
                 Local F
		         cp_CreateCacheFile()
                   If cp_IsDeveloper()

                      If oTool.PutFileName()
                         oTool.SaveDoc()
                         oTool.SetTitle()
                  
                        If Lower(Alltrim(Justext(oTool.cFileName)))=="vrt" And i_MonitorFramework
                          Local vrt_campi
                          cp_monitor_SetFields(Alltrim(Upper(".\"+Sys(2014,oTool.cFileName))),@vrt_campi)
                          vrt_campi[FILEDESC_IDX] = "Gestione "+Alltrim(oTool.oform.oformcontainer.cComment)
                          Insert Into cp_monitor From Array vrt_campi
                        Endif
                   
                      Else
                       oTool.SetTitle()
                      Endif
                   
                  Else && non sono sviluppatore
                   edit_condition = .T.
                   vrt_caption = ""
                   Local vrt_campi
                   If oTool.MonitorPutFileName()
                      oTool.SaveDoc()
                      cp_monitor_SetFields(Alltrim(Upper(oTool.cFileName)),@vrt_campi)
                      If Len(vrt_caption)>0
                       vrt_campi[FILEDESC_IDX] = Alltrim(vrt_caption)
                       Insert Into cp_monitor From Array vrt_campi
                      Endif
                   ENDIF
                 ENDIF
                
                *--- Zucchetti Aulla fine  - Framework monitor   
                Else
                    oTool.bModified=.F.
                Endif
                oTool.cFileName=cOldFName
            Otherwise
                DoDefault(oSource,x,Y)
        Endcase
        Return
Enddefine

Define Class rtbodydraw_board As rtdraw_board
    Proc SetModified(bStatus)
        This.Parent.drw.SetModified(bStatus)
        Return
    Proc Resize()
        Return
Enddefine

Define Class rtpainter_txt As painter_txt
    oCtrl=.Null.
    bModified=.F.
    bHighlight=.F.
    bHidden=.F.
    Dimension sProps[2]
    sProps[1]=''
    sProps[2]=''

    Procedure bModified_Assign(xValue)
        This.bModified = m.xValue
        This.bHighlight = Thisform.oTool.HighlightCheck And m.xValue
    Endproc

    Procedure bHighlight_Assign(xValue)
        This.bHighlight = m.xValue
        If This.bHighlight
            *This.BorderColor=255
            *This.BorderWidth=1
            This.obj.SpecialEffect=1
            This.obj.BorderColor=255
        Else
            *This.BorderWidth=0
            This.obj.SpecialEffect=0
        Endif
    Endproc

    Proc DblClick()
        Local o
        o=Createobject('rtpainter_fldvar_form',This)
        o.varname.Value=Substr(This.oCtrl.cFormVar,3)
        o.editradio.Value=Iif(This.bHidden,1,2)
        o.Show()
    Proc Hide(bHide)
        If bHide
            This.obj.SpecialEffect=1
            This.obj.BorderColor=Rgb(255,0,0)
        Else
            This.obj.SpecialEffect=0
            This.obj.BorderColor=Rgb(0,0,0)
        Endif
        This.bHidden=bHide
    Proc Moved()
        DoDefault()
        This.bModified=.T.
    Proc Resize()
        DoDefault()
        This.bModified=.T.
    Func GetID()
        Return(This.oCtrl.uid)
    Proc AddProp(i_cName,i_cValue)
        * --- aggiunge una proprieta' all' array dell proprieta'
        Local N
        N=Alen(This.sProps)
        If N>2 Or !Empty(This.sProps[1])
            N=N+2
            Dimension This.sProps[n]
        Endif
        This.sProps[n-1]=i_cName
        This.sProps[n]=i_cValue
        *wait window "Aggiunta "+i_cName+' '+i_cValue
Enddefine

Define Class rtpainter_trstxt As rtpainter_txt
    Proc Init()
        DoDefault()
        This.obj.SpecialEffect=1
        Return
    Proc Hide(bHide)
        If bHide
            *this.obj.specialeffect=1
            This.obj.BorderColor=Rgb(255,0,0)
        Else
            *this.obj.specialeffect=1
            This.obj.BorderColor=Rgb(0,0,0)
        Endif
        This.bHidden=bHide
Enddefine

Define Class rtpainter_lbl As painter_lbl
    oCtrl=.Null.
    bModified=.F.
    bHighlight=.F.
    bHidden=.F.
    nAlignment=0
    Dimension sProps[2]
    sProps[1]=''
    sProps[2]=''

    Procedure bModified_Assign(xValue)
        This.bModified = m.xValue
        This.bHighlight = Thisform.oTool.HighlightCheck And m.xValue
    Endproc

    Procedure bHighlight_Assign(xValue)
        This.bHighlight = m.xValue
        If This.bHighlight
            This.BorderColor=255
            This.BorderWidth=1
        Else
            This.BorderWidth=0
        Endif
    Endproc
    Proc DblClick()
        Local o
        o=Createobject('rtpainter_lbl_form',This)
        o.Text.Value=This.obj.Caption
        o.editradio.Value=Iif(This.bHidden,1,2)
        o.Align.Value=This.obj.Alignment+1
        o.Show()
    Proc Hide(bHide)
        If bHide
            This.BorderWidth=1
            This.BorderColor=Rgb(255,0,0)
        Else
            This.BorderWidth=0
            This.BorderColor=Rgb(0,0,0)
        Endif
        This.bHidden=bHide
    Proc Alignment(nValue)
        This.nAlignment=nValue
        This.obj.Alignment=nValue
    Proc Moved()
        DoDefault()
        This.bModified=.T.
    Proc Resize()
        DoDefault()
        This.bModified=.T.
    Func GetID()
        Return(This.oCtrl.uid)
    Proc AddProp(i_cName,i_cValue)
        * --- aggiunge una proprieta' all' array dell proprieta'
        Local N
        N=Alen(This.sProps)
        If N>2 Or !Empty(This.sProps[1])
            N=N+2
            Dimension This.sProps[n]
        Endif
        This.sProps[n-1]=i_cName
        This.sProps[n]=i_cValue
        *wait window "Aggiunta "+i_cName+' '+i_cValue
Enddefine

Define Class rtpainter_check As painter_check
    oCtrl=.Null.
    bModified=.F.
    bHighlight=.F.
    bHidden=.F.
    Dimension sProps[2]
    sProps[1]=''
    sProps[2]=''

    Procedure bModified_Assign(xValue)
        This.bModified = m.xValue
        This.bHighlight = Thisform.oTool.HighlightCheck And m.xValue
    Endproc

    Procedure bHighlight_Assign(xValue)
        This.bHighlight = m.xValue
        If This.bHighlight
            This.BorderColor=255
            This.BorderWidth=1
            This.obj.BackStyle=0
        Else
            This.BorderWidth=0
        Endif
    Endproc
    Proc DblClick()
        Local o
        o=Createobject('rtpainter_fldvar_form',This)
        o.varname.Value=Substr(This.oCtrl.cFormVar,3)
        o.editradio.Value=Iif(This.bHidden,1,2)
        o.Show()
    Proc Hide(bHide)
        If bHide
            This.obj.BackColor=Rgb(255,0,0)
        Else
            This.obj.BackColor=Rgb(192,192,192)
        Endif
        This.bHidden=bHide
    Proc Moved()
        DoDefault()
        This.bModified=.T.
    Proc Resize()
        DoDefault()
        This.bModified=.T.
    Func GetID()
        Return(This.oCtrl.uid)
    Proc AddProp(i_cName,i_cValue)
        * --- aggiunge una proprieta' all' array dell proprieta'
        Local N
        N=Alen(This.sProps)
        If N>2 Or !Empty(This.sProps[1])
            N=N+2
            Dimension This.sProps[n]
        Endif
        This.sProps[n-1]=i_cName
        This.sProps[n]=i_cValue
Enddefine

Define Class rtpainter_radio As painter_radio
    oCtrl=.Null.
    bModified=.F.
    bHighlight=.F.
    bHidden=.F.
    Dimension sProps[2]
    sProps[1]=''
    sProps[2]=''

    Procedure bModified_Assign(xValue)
        This.bModified = m.xValue
        This.bHighlight = Thisform.oTool.HighlightCheck And m.xValue
    Endproc

    Procedure bHighlight_Assign(xValue)
        This.bHighlight = m.xValue
        If This.bHighlight
            This.BorderColor=255
            This.BorderWidth=1
            This.obj.BackStyle=0
            *This.obj.BorderColor=255
        Else
            This.BorderWidth=0
            This.obj.BackStyle=1
        Endif
    Endproc
    Proc DblClick()
        Local o
        o=Createobject('rtpainter_fldvar_form',This)
        o.varname.Value=Substr(This.oCtrl.cFormVar,3)
        o.editradio.Value=Iif(This.bHidden,1,2)
        o.Show()
    Proc Hide(bHide)
        If bHide
            This.obj.SpecialEffect=1
            This.obj.BorderStyle=1
            This.obj.BorderColor=Rgb(255,0,0)
        Else
            This.obj.SpecialEffect=0
            This.obj.BorderStyle=0
        Endif
        This.bHidden=bHide
    Proc Moved()
        DoDefault()
        This.bModified=.T.
    Proc Resize()
        DoDefault()
        This.bModified=.T.
    Func GetID()
        Return(This.oCtrl.uid)
    Proc AddProp(i_cName,i_cValue)
        * --- aggiunge una proprieta' all' array dell proprieta'
        Local N
        N=Alen(This.sProps)
        If N>2 Or !Empty(This.sProps[1])
            N=N+2
            Dimension This.sProps[n]
        Endif
        This.sProps[n-1]=i_cName
        This.sProps[n]=i_cValue
        Return
    Proc SetLabels()
        Local a,i,h
        h=This.Height
        Dimension a[1]
        For i=1 To This.oCtrl.ButtonCount
            This.AddLbl(@a,Trim(This.oCtrl.Buttons(i).Caption))
        Next
        This.SetRadioButtons(@a)
        This.AdjustRadioButtons()
        This.Height=h
        Return
    Proc AdjustRadioButtons()
        Local i
        For i=1 To This.oCtrl.ButtonCount
            This.obj.Buttons(i).Left=This.oCtrl.Buttons(i).Left
            This.obj.Buttons(i).Top=This.oCtrl.Buttons(i).Top
        Next
    Proc AddLbl(a,cLbl)
        If !Empty(cLbl)
            Local l
            l=Alen(a)
            If l>1 Or !Empty(a[1])
                l=l+1
                Dimension a[l]
            Endif
            a[l]=cLbl
        Endif
        Return
Enddefine

Define Class rtpainter_combo As painter_combo
    oCtrl=.Null.
    bModified=.F.
    bHidden=.F.
    bHighlight=.F.
    Dimension sProps[2]
    sProps[1]=''
    sProps[2]=''

    Procedure bModified_Assign(xValue)
        This.bModified = m.xValue
        This.bHighlight = Thisform.oTool.HighlightCheck And m.xValue
    Endproc

    Procedure bHighlight_Assign(xValue)
        This.bHighlight = m.xValue
        If This.bHighlight
            This.BorderColor=255
            This.BorderWidth=1
            This.obj.SpecialEffect= 1
            This.obj.BorderColor = This.BorderColor
        Else
            This.BorderWidth=0
            This.obj.SpecialEffect= 0
        Endif
    Endproc
    Proc DblClick()
        Local o
        o=Createobject('rtpainter_fldvar_form',This)
        o.varname.Value=Substr(This.oCtrl.cFormVar,3)
        o.editradio.Value=Iif(This.bHidden,1,2)
        o.Show()
    Proc Hide(bHide)
        If bHide
            This.obj.SpecialEffect=1
            This.obj.BorderColor=Rgb(255,0,0)
        Else
            This.obj.SpecialEffect=0
            This.obj.BackColor=Rgb(0,0,0)
        Endif
        This.bHidden=bHide
    Proc Moved()
        DoDefault()
        This.bModified=.T.
    Proc Resize()
        DoDefault()
        This.bModified=.T.
    Func GetID()
        Return(This.oCtrl.uid)
    Proc AddProp(i_cName,i_cValue)
        * --- aggiunge una proprieta' all' array dell proprieta'
        Local N
        N=Alen(This.sProps)
        If N>2 Or !Empty(This.sProps[1])
            N=N+2
            Dimension This.sProps[n]
        Endif
        This.sProps[n-1]=i_cName
        This.sProps[n]=i_cValue
Enddefine

Define Class rtpainter_btn As painter_btn
    oCtrl=.Null.
    bModified=.F.
    bHighlight=.F.
    bHidden=.F.
    Dimension sProps[2]
    sProps[1]=''
    sProps[2]=''

    Procedure bModified_Assign(xValue)
        This.bModified = m.xValue
        This.bHighlight = Thisform.oTool.HighlightCheck And m.xValue
    Endproc

    Procedure bHighlight_Assign(xValue)
        This.bHighlight = m.xValue
        If This.bHighlight
            This.BorderColor=255
            This.BorderWidth=1
        Else
            This.BorderWidth=0
        Endif
    Endproc
    Proc DblClick()
        Local o
        o=Createobject('rtpainter_btn_form',This,.F.)
        o.Text.Value=This.obj.Caption
        o.editradio.Value=Iif(This.bHidden,1,2)
        o.Show()
    Proc Hide(bHide)
        If bHide
            This.BorderWidth=1
            This.SpecialEffect=2
            This.BorderColor=Rgb(255,0,0)
            This.obj.Top=1
            This.obj.Left=1
        Else
            This.obj.Top=0
            This.obj.Left=0
        Endif
        This.bHidden=bHide
    Proc Moved()
        DoDefault()
        This.bModified=.T.
    Proc Resize()
        DoDefault()
        This.bModified=.T.
    Func GetID()
        Return(This.oCtrl.uid)
    Proc AddProp(i_cName,i_cValue)
        * --- aggiunge una proprieta' all' array dell proprieta'
        Local N
        N=Alen(This.sProps)
        If N>2 Or !Empty(This.sProps[1])
            N=N+2
            Dimension This.sProps[n]
        Endif
        This.sProps[n-1]=i_cName
        This.sProps[n]=i_cValue
Enddefine


Define Class rtpainter_box As draw_stditem
    oCtrl=.Null.
    bModified=.F.
    bHighlight=.F.
    bHidden=.F.
    Dimension sProps[2]
    sProps[1]=''
    sProps[2]=''
    nOldBorderColor=Rgb(0,192,0)
    Add Object obj As Shape With SpecialEffect=0,BackStyle=0

    Procedure bModified_Assign(xValue)
        This.bModified = m.xValue
        This.bHighlight = Thisform.oTool.HighlightCheck And m.xValue
    Endproc

    Procedure bHighlight_Assign(xValue)
        If This.bHighlight <> m.xValue
            This.bHighlight = m.xValue
            If This.bHighlight
                This.nOldBorderColor= This.obj.BorderColor
                This.obj.BorderColor=255
                *This.BorderWidth=1
            Else
                This.obj.BorderColor=This.nOldBorderColor
            Endif
        Endif
    Endproc
    Proc Moved()
        DoDefault()
        This.bModified=.T.
    Proc Resize()
        DoDefault()
        This.bModified=.T.
    Func GetID()
        Return(This.oCtrl.uid)
    Proc obj.MouseDown(nBtn,nSh,nX,nY)
        This.Parent.MouseDown(nBtn,nSh,nX,nY)
        Return
    Proc obj.MouseMove(nBtn,nSh,nX,nY)
        This.Parent.MouseMove(nBtn,nSh,nX,nY)
        Return
    Proc obj.MouseUp(nBtn,nSh,nX,nY)
        This.Parent.MouseUp(nBtn,nSh,nX,nY)
        Return
Enddefine

Define Class rtpainter_pccontainer As rtpainter_box
    oCtrl=.Null.
    bModified=.F.
    bHidden=.F.
    Dimension sProps[2]
    sProps[1]=''
    sProps[2]=''
    Add Object lbl As CommandButton With Caption='P/C',Top=2,Left=2,AutoSize=.T.
    Proc Hide(bHide)
        If bHide
            This.obj.BorderColor=Rgb(255,0,0)
        Else
            This.obj.BorderColor=Rgb(0,192,0)
        Endif
        This.bHidden=bHide
    Proc SetPCName(cName)
        This.lbl.Caption=cName
        This.obj.BorderColor=Rgb(0,192,0)
        This.obj.SpecialEffect=1
    Proc obj.DblClick()
        This.Parent.ShowForm()
    Proc ShowForm()
        Local o
        o=Createobject('rtpainter_btn_form',This,.T.)
        o.Text.Value=''
        o.editradio.Value=Iif(This.bHidden,1,2)
        o.Show()
        Return
    Proc AddProp(i_cName,i_cValue)
        * --- aggiunge una proprieta' all' array dell proprieta'
        Local N
        N=Alen(This.sProps)
        If N>2 Or !Empty(This.sProps[1])
            N=N+2
            Dimension This.sProps[n]
        Endif
        This.sProps[n-1]=i_cName
        This.sProps[n]=i_cValue
    Proc lbl.Click()
        Thisform.oTool.Hide()
        Thisform.Hide()
        i_rt=Createobject('RT_toolbar',This.Parent.oCtrl,This.Parent.oCtrl,Substr(This.Parent.oCtrl.Class,3), Vartype(_Screen.cp_ThemesManager)=="O" And i_VisualTheme<>-1)
        i_rt.EditMask()
        i_rt.oReEnable=Thisform
Enddefine

* --- Correzione Piccoli su problema RUN-TIME nel caso di figli integrati
* --- Problema Figli integrati
Define Class rtpainter_pcDynamic As rtpainter_pccontainer
    Func GetID()
        Return(This.oCtrl.Parent.uid)
Enddefine

Define Class rtpainter_fldvar_form As CpSysform
    WindowType=1
    Caption = "Field / Variable Options"
    Icon=i_cBmpPath+i_cStdIcon
    oShp=.Null.
    oCurrForm=.Null.
    * ---
    Top = 55
    Left = 60
    Height = 380
    Width = 492
    Add Object label1 As Label With Alignment = 1, Caption = "Name:", Height = 17, Left = 15, Top = 16, Width = 40, Name = "Label1", BackStyle=0
    Add Object varname As TextBox With 	Enabled = .F., Height = 23, Left = 60, Top = 12, Width = 100, Name = "varname"
    Add Object editradio As OptionGroup With ButtonCount = 2, Value = 1, Height = 48, Left = 396, Top = 12, Width = 83, TabIndex = 4, Name = "editradio", BackStyle=0, ;
        Option1.Caption = "Hide", Option1.Value = 1, Option1.Height = 17, Option1.Left = 5, Option1.Top = 5, Option1.Width = 61,Option1.BackStyle=0, Option1.Name = "Hide", ;
        Option2.Caption = "Show/Edit", Option2.Height = 17, Option2.Left = 5, Option2.Top = 24, Option2.Width = 79,Option2.BackStyle=0, Option2.Name = "Show"
    Add Object commandgroup1 As CommandGroup With ButtonCount = 0, Value = 0, Height = 120, Left = 12, Top = 84, Width = 469, Name = "Commandgroup1", BackStyle=0
    Add Object check1 As Checkbox With Top = 156, Left = 192, Height = 17, Width = 75, Caption = "Zero filling", TabIndex = 9, Name = "Check1", BackStyle=0
    Add Object check2 As Checkbox With Top = 156, Left = 336, Height = 17, Width = 72, Caption = "Obligatory", TabIndex = 10, Name = "Check2", BackStyle=0
    Add Object check3 As Checkbox With Top = 180, Left = 192, Height = 17, Width = 132, Caption = "Edit under condition", TabIndex = 11, Name = "Check3", BackStyle=0
    Add Object check4 As Checkbox With Top = 180, Left = 336, Height = 17, Width = 132, Caption = "Hide under condition", TabIndex = 12, Name = "Check4", BackStyle=0
    Add Object calcradio As OptionGroup With ButtonCount = 4, Value = 1, Height = 85, Left = 24, Top = 108, Width = 78, TabIndex = 5, Name = "calcradio", BackStyle=0, ;
        Option1.Caption = "No", Option1.Value = 1, Option1.Height = 17, Option1.Left = 5, Option1.Top = 5, Option1.Width = 61,Option1.BackStyle=0, Option1.Name = "Option1", ;
        Option2.Caption = "Calculate", Option2.Height = 17, Option2.Left = 5, Option2.Top = 24, Option2.Width = 67,Option2.BackStyle=0, Option2.Name = "Option2", ;
        Option3.Caption = "Init", Option3.Height = 17, Option3.Left = 5, Option3.Top = 43, Option3.Width = 61,Option3.BackStyle=0, Option3.Name = "Option3", ;
        Option4.Caption = "Default", Option4.Height = 17, Option4.Left = 5, Option4.Top = 62, Option4.Width = 61,Option4.BackStyle=0, Option4.Name = "Option4"
    Add Object label2 As Label With Caption = "Options", Height = 17, Left = 24, Top = 77, Width = 44, ColorSource = 4, Name = "Label2", BackStyle=0
    Add Object label3 As Label With Alignment = 1, Caption = "Help:", Height = 17, Left = 14, Top = 40, Width = 40, Name = "Label3", BackStyle=0
    Add Object varhelp As TextBox With Height = 23, Left = 60, TabIndex = 3, Top = 36, Width = 324, Name = "varhelp"
    Add Object label4 As Label With Caption = "Evaluate", Height = 17, Left = 35, Top = 100, Width = 48, Name = "Label4", BackStyle=0
    Add Object label5 As Label With Alignment = 1, Caption = "Get picture:", Height = 17, Left = 222, Top = 100, Width = 76, Name = "Label5", BackStyle=0
    Add Object label6 As Label With Alignment = 1, Caption = "Display picture:", Height = 17, Left = 211, Top = 121, Width = 88, Name = "Label6", BackStyle=0
    Add Object vargetpict As TextBox With Height = 23, Left = 300, TabIndex = 7, Top = 96, Width = 168, Name = "vargetpict"
    Add Object varsaypict As TextBox With Height = 23, Left = 300, TabIndex = 8, Top = 120, Width = 168, Name = "varsaypict"
    Add Object commandgroup2 As CommandGroup With ButtonCount = 0, Value = 0, Height = 120, Left = 12, Top = 216, Width = 468, Name = "Commandgroup2", BackStyle=0
    Add Object label7 As Label With Caption = "Expressions", Height = 17, Left = 24, Top = 210, Width = 72, Name = "Label7", BackStyle=0
    Add Object label8 As Label With Alignment = 1, Caption = "Calc/Init/Def:", Height = 17, Left = 22, Top = 232, Width = 72, Name = "Label8", BackStyle=0
    * --- Modificata la definizione del calcexpr da textbox a editbox e partato il limite della lunghezza a 1000 caratteri
    Add Object calcexpr As EditBox With Height = 23, Left = 96, TabIndex = 13, Top = 228, Width = 372, Name = "calcexpr",MaxLength=MAX_CTRL_LENGHT

    Add Object label9 As Label With Alignment = 1, Caption = "Checking:", Height = 17, Left = 22, Top = 255, Width = 72, Name = "Label9", BackStyle=0
    * --- Modificata la definizione del checkexpr da textbox a editbox e partato il limite della lunghezza a 1000 caratteri
    Add Object checkexpr As EditBox With Height = 23, Left = 96, TabIndex = 14, Top = 252, Width = 372, Name = "checkexpr",MaxLength=MAX_CTRL_LENGHT
    Add Object label10 As Label With Alignment = 1, Caption = "Editing:", Height = 17, Left = 47, Top = 279, Width = 47, Name = "Label10", BackStyle=0
    * --- Modificata la definizione del editexpr da textbox a editbox e partato il limite della lunghezza a 1000 caratteri
    Add Object editexpr As EditBox With Height = 23, Left = 96, TabIndex = 15, Top = 276, Width = 372, Name = "editexpr",MaxLength=MAX_CTRL_LENGHT
    Add Object checkradio As OptionGroup With ButtonCount = 2, Value = 1, Height = 46, Left = 105, Top = 109, Width = 71, TabIndex = 6, Name = "checkradio", BackStyle=0, ;
        Option1.Caption = "No", Option1.Value = 1, Option1.Height = 17, Option1.Left = 5, Option1.Top = 5, Option1.Width = 61,Option1.BackStyle=0, Option1.Name = "Option1", ;
        Option2.Caption = "Check", Option2.Height = 17, Option2.Left = 5, Option2.Top = 24, Option2.Width = 61,Option2.BackStyle=0, Option2.Name = "Option2"
    Add Object label11 As Label With Alignment = 1, Caption = "Hiding:", Height = 17, Left = 47, Top = 303, Width = 47, Name = "Label11", BackStyle=0
    * --- Modificata la definizione del hideexpr da textbox a editbox e partato il limite della lunghezza a 1000 caratteri
    Add Object hideexpr As EditBox With Height = 23, Left = 96, TabIndex = 16, Top = 300, Width = 372, Name = "hideexpr",MaxLength=MAX_CTRL_LENGHT
    Add Object btnok As CommandButton With Top = 348, Left = 348, Height = 27, Width = 48, Caption = "Ok", TabIndex = 18, Name = "btnok"
    Add Object btncancel As CommandButton With Top = 348, Left = 408, Height = 27, Width = 60, Caption = "Cancel", TabIndex = 19, Name = "btncancel"
    Add Object resetbtn As CommandButton With Top = 348, Left = 24, Height = 24, Width = 72, Caption = "Reset", TabIndex = 17, Name = "resetbtn"
    Add Object label12 As Label With Caption = "Checked", Height = 17, Left = 117, Top = 101, Width = 50, Name = "Label12", BackStyle=0
    Add Object label13 As Label With Caption = "Editing", Height = 17, Left = 413, Top = 4, Width = 40, Name = "Label13", BackStyle=0
    * ---
    Proc check3.Click()
        Thisform.editexpr.Enabled=(This.Value=1)
    Proc check4.Click()
        Thisform.hideexpr.Enabled=(This.Value=1)
    Proc checkradio.Click()
        Thisform.checkexpr.Enabled=(Thisform.checkradio.Value<>1)
    Proc calcradio.Click()
        Thisform.calcexpr.Enabled=(Thisform.calcradio.Value<>1)
    Proc Init(oShp)
        This.oShp=oShp
        Thisform.GetStockProps()
        Thisform.calcexpr.Enabled=(This.calcradio.Value<>1)
        Thisform.checkexpr.Enabled=(This.checkradio.Value<>1)
        Thisform.editexpr.Enabled=(This.check3.Value=1)
        Thisform.hideexpr.Enabled=(This.check4.Value=1)
        This.oCurrForm=i_curform
        i_curform=This
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
    Proc btnok.Click()
        Thisform.oShp.Hide(Thisform.editradio.Value=1)
        Thisform.SetStockProps()
        Thisform.oShp.bModified=.T.
        Thisform.oShp.Parent.SetModified(.T.)
        Thisform.Release()
    Proc btncancel.Click()
        Thisform.Release()
    Proc GetStockProps()
        Local i,N,v
        For i=1 To Alen(This.oShp.sProps) Step 2
            N=This.oShp.sProps[i]
            v=This.oShp.sProps[i+1]
            *wait window "GetStockProps "+n
            Do Case
                Case N='nZero'
                    This.check1.Value=1
                Case N='bObbl'
                    This.check2.Value=1
                Case N='cGetPict'
                    This.vargetpict.Value=v
                Case N='cSayPict'
                    This.varsaypict.Value=v
                Case N='tooltiptext'
                    This.varhelp.Value=v
                Case N='cfgCond'
                    This.check3.Value=1
                    This.editexpr.Value=v
                Case N='cfgHide'
                    This.check4.Value=1
                    This.hideexpr.Value=v
                Case N='cfgCheck'
                    This.checkradio.Value=2
                    This.checkexpr.Value=v
                Case N='cfgCalc'
                    This.calcradio.Value=2
                    This.calcexpr.Value=v
                Case N='cfgInit'
                    This.calcradio.Value=3
                    This.calcexpr.Value=v
                Case N='cfgDefault'
                    This.calcradio.Value=4
                    This.calcexpr.Value=v
            Endcase
        Next
    Proc SetStockProps()
        This.ResetShpStockProps()
        * --- calc
        If This.calcradio.Value=2
            This.SetShpStockProp('cfgCalc',Trim(This.calcexpr.Value))
        Endif
        If This.calcradio.Value=3
            This.SetShpStockProp('cfgInit',Trim(This.calcexpr.Value))
        Endif
        If This.calcradio.Value=4
            This.SetShpStockProp('cfgDefault',Trim(This.calcexpr.Value))
        Endif
        * --- check
        If This.checkradio.Value<>1
            This.SetShpStockProp('cfgCheck',Trim(This.checkexpr.Value))
        Endif
        * --- editcond
        If This.check3.Value<>0
            This.SetShpStockProp('cfgCond',Trim(This.editexpr.Value))
        Endif
        * --- hidecond
        If This.check4.Value<>0
            This.SetShpStockProp('cfgHide',Trim(This.hideexpr.Value))
        Endif
        * --- zerofill
        If This.check1.Value<>0
            *wait window "zerofill"
            This.SetShpStockProp('nZero','-1')
        Endif
        * --- obbl
        If This.check2.Value<>0
            *wait window "obbl"
            This.SetShpStockProp('bObbl','.t.')
        Endif
        * --- saypict
        If !Empty(This.varsaypict.Value)
            *wait window "saypict "+this.varsaypict.value
            This.SetShpStockProp('cSayPict',Trim(This.varsaypict.Value))
        Endif
        * --- getpict
        If !Empty(This.vargetpict.Value)
            *wait window "getpict "+this.vargetpict.value
            This.SetShpStockProp('cGetPict',Trim(This.vargetpict.Value))
        Endif
        * --- tooltiptext
        If !Empty(This.varhelp.Value)
            *wait window "tooltip "+this.varhelp.value
            This.SetShpStockProp('tooltiptext',Trim(This.varhelp.Value))
        Endif
    Proc ResetShpStockProps()
        Dimension This.oShp.sProps[2]
        This.oShp.sProps[1]=''
        This.oShp.sProps[2]=''
    Proc SetShpStockProp(cProp,cValue)
        Local N
        N=Alen(This.oShp.sProps)
        If N<>2 Or !Empty(This.oShp.sProps[1])
            Dimension This.oShp.sProps[n+2]
            N=N+2
        Endif
        This.oShp.sProps[n-1]=cProp
        This.oShp.sProps[n]=cValue
    Proc resetbtn.Click()
        Thisform.ResetShpStockProps()
        Thisform.oShp.Top=Thisform.oShp.oCtrl.Top
        Thisform.oShp.Left=Thisform.oShp.oCtrl.Left
        Thisform.oShp.Height=Thisform.oShp.oCtrl.Height
        Thisform.oShp.Width=Thisform.oShp.oCtrl.Width
        If Thisform.oShp.bHandlers
            Thisform.oShp.RemoveHandlers()
            Thisform.oShp.AddHandlers()
        Endif
        Thisform.oShp.bModified=.T.
        Thisform.Release()
    Proc Release()
        DoDefault()
        i_curform=This.oCurrForm
    Func HasCPEvents(i_cOp)
        Return(i_cOp='ecpQuit' Or i_cOp='ecpSave')
    Proc ecpQuit()
        This.btncancel.Click()
    Proc EcpSave()
        This.btnok.Click()
Enddefine

Define Class rtpainter_lbl_form As CpSysform
    WindowType=1
    Caption = "String options"
    Icon=i_cBmpPath+i_cStdIcon
    oShp=.Null.
    oCurrForm=.Null.
    Top = 0
    Left = 0
    Height = 159
    Width = 463

    Add Object label1 As Label With Alignment = 1, Caption = "Text:", Height = 17, Left = 12, Top = 12, Width = 36, BackStyle=0
    Add Object Text As TextBox With Height = 25, Left = 51, Top = 8, Width = 321, Name = "text"
    Add Object Align As ComboBox With Height = 25, Left = 51, Top = 36, Width = 85
    Add Object editradio As OptionGroup With ButtonCount = 2, Value = 1, Height = 48, Left = 376, Top = 9, Width = 83, Name = "editradio", BackStyle=0, ;
        Option1.Caption = "Hide", Option1.Value = 1, Option1.Height = 17, Option1.Left = 5, Option1.Top = 5, Option1.Width = 61,Option1.BackStyle=0, Option1.Name = "Hide", ;
        Option2.Caption = "Show/Edit", Option2.Height = 17, Option2.Left = 5, Option2.Top = 24, Option2.Width = 79,Option2.BackStyle=0, Option2.Name = "Show"
    Add Object label13 As Label With Caption = "Editing", Height = 17, Left = 393, Top = 1, Width = 40, BackStyle=0
    Add Object hidecheck As Checkbox With Top = 72, Left = 12, Height = 17, Width = 132, Caption = "Hide under condition" , BackStyle=0
    Add Object hideexpr As TextBox With Height = 23, Left = 12, Top = 90, Width = 372
    Add Object btnok As CommandButton With Top = 122, Left = 336, Height = 27, Width = 48, Caption = "Ok"
    Add Object btncancel As CommandButton With Top = 122, Left = 396, Height = 27, Width = 60, Caption = "Cancel"
    Add Object resetbtn As CommandButton With Top = 122, Left = 12, Height = 24, Width = 72, Caption = "Reset"
    Add Object label2 As Label With Alignment = 1, Caption = "Align:", Height = 17, Left = 8, Top = 40, Width = 40, BackStyle=0

    Proc Init(oShp)
        This.oShp=oShp
        This.Align.AddItem(cp_Translate(MSG_LEFT))
        This.Align.AddItem(cp_Translate(MSG_RIGHT))
        This.Align.AddItem(cp_Translate(MSG_CENTRED))
        This.oCurrForm=i_curform
        i_curform=This
        This.GetStockProps()
        This.hideexpr.Enabled=(This.hidecheck.Value=1)
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
    Proc Release()
        DoDefault()
        i_curform=This.oCurrForm
    Proc hidecheck.Click()
        Thisform.hideexpr.Enabled=(This.Value=1)
    Proc btnok.Click()
        Thisform.SetStockProps()
        Thisform.oShp.Hide(Thisform.editradio.Value=1)
        Thisform.oShp.obj.Caption=Trim(Thisform.Text.Value)
        Thisform.oShp.Alignment(Thisform.Align.Value-1)
        Thisform.oShp.bModified=.T.
        Thisform.oShp.Parent.SetModified(.T.)
        Thisform.Release()
    Proc btncancel.Click()
        Thisform.Release()
    Proc resetbtn.Click()
        Thisform.oShp.Top=Thisform.oShp.oCtrl.Top
        Thisform.oShp.Left=Thisform.oShp.oCtrl.Left
        Thisform.oShp.Height=Thisform.oShp.oCtrl.Height
        Thisform.oShp.Width=Thisform.oShp.oCtrl.Width
        Thisform.oShp.obj.Caption=Thisform.oShp.oCtrl.Caption
        Thisform.oShp.Alignment(Thisform.oShp.oCtrl.Alignment)
        If Thisform.oShp.bHandlers
            Thisform.oShp.RemoveHandlers()
            Thisform.oShp.AddHandlers()
        Endif
        Thisform.ResetShpStockProps()
        Thisform.oShp.bModified=.T.
        Thisform.Release()
    Proc GetStockProps()
        Local i,N,v
        For i=1 To Alen(This.oShp.sProps) Step 2
            N=This.oShp.sProps[i]
            v=This.oShp.sProps[i+1]
            *wait window "GetStockProps "+n
            Do Case
                Case N='cfgHide'
                    This.hidecheck.Value=1
                    This.hideexpr.Value=v
            Endcase
        Next
    Proc SetStockProps()
        This.ResetShpStockProps()
        * --- hidecond
        If This.hidecheck.Value<>0
            This.SetShpStockProp('cfgHide',Trim(This.hideexpr.Value))
        Endif
    Proc ResetShpStockProps()
        Dimension This.oShp.sProps[2]
        This.oShp.sProps[1]=''
        This.oShp.sProps[2]=''
    Func HasCPEvents(i_cOp)
        Return(i_cOp='ecpQuit' Or i_cOp='ecpSave')
    Proc ecpQuit()
        This.btncancel.Click()
    Proc EcpSave()
        This.btnok.Click()
    Proc SetShpStockProp(cProp,cValue)
        Local N
        N=Alen(This.oShp.sProps)
        If N<>2 Or !Empty(This.oShp.sProps[1])
            Dimension This.oShp.sProps[n+2]
            N=N+2
        Endif
        This.oShp.sProps[n-1]=cProp
        This.oShp.sProps[n]=cValue
Enddefine

Define Class rtpainter_btn_form As CpSysform
    WindowType=1
    Caption = "Button Options"
    Icon=i_cBmpPath+i_cStdIcon
    oShp=.Null.
    oCurrForm=.Null.
    Top = 0
    Left = 0
    Height = 159
    Width = 463
    bIntegratedPC=.F.
    Add Object label1 As Label With Alignment = 1, Caption = "Text:", Height = 17, Left = 12, Top = 12, Width = 36, BackStyle=0
    Add Object Text As TextBox With Height = 25, Left = 51, Top = 8, Width = 321, Name = "text"
    Add Object editradio As OptionGroup With ButtonCount = 2, Value = 1, Height = 48, Left = 376, Top = 9, Width = 83, Name = "editradio", ;
        Option1.Caption = "Hide", Option1.Value = 1, Option1.Height = 17, Option1.Left = 5, Option1.Top = 5, Option1.Width = 61,Option1.BackStyle=0, Option1.Name = "Hide", ;
        Option2.Caption = "Show/Edit", Option2.Height = 17, Option2.Left = 5, Option2.Top = 24, Option2.Width = 79,Option2.BackStyle=0, Option2.Name = "Show", BackStyle=0
    Add Object label13 As Label With Caption = "Editing", Height = 17, Left = 393, Top = 1, Width = 40, BackStyle=0
    Add Object hidecheck As Checkbox With Top = 72, Left = 12, Height = 17, Width = 132, Caption = "Hide under condition" , BackStyle=0
    Add Object hideexpr As TextBox With Height = 23, Left = 12, Top = 90, Width = 372
    Add Object btnok As CommandButton With Top = 122, Left = 336, Height = 27, Width = 48, Caption = "Ok"
    Add Object btncancel As CommandButton With Top = 122, Left = 396, Height = 27, Width = 60, Caption = "Cancel"
    Add Object resetbtn As CommandButton With Top = 122, Left = 12, Height = 24, Width = 72, Caption = "Reset"

    Proc Init(oShp,bIntegratedPC)
        This.oShp=oShp
        This.oCurrForm=i_curform
        i_curform=This
        This.GetStockProps()
        * --- Aggiunta visualizzazione del nome del BMP associato a un bottone nel titolo
        If !Empty(Sys(2014,This.oShp.oCtrl.Picture))
            This.Caption=This.Caption+'  ('+Allt(Sys(2014,This.oShp.oCtrl.Picture))+')'
        Endif

        This.hideexpr.Enabled=(This.hidecheck.Value=1)
        This.bIntegratedPC=bIntegratedPC
        If bIntegratedPC
            This.Text.Enabled=.F.
        Endif
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
    Proc Release()
        DoDefault()
        i_curform=This.oCurrForm
    Proc hidecheck.Click()
        Thisform.hideexpr.Enabled=(This.Value=1)
    Proc btnok.Click()
        Thisform.SetStockProps()
        Thisform.oShp.Hide(Thisform.editradio.Value=1)
        If !Thisform.bIntegratedPC
            Thisform.oShp.obj.Caption=Trim(Thisform.Text.Value)
        Endif
        Thisform.oShp.bModified=.T.
        Thisform.oShp.Parent.SetModified(.T.)
        Thisform.Release()
    Proc btncancel.Click()
        Thisform.Release()
    Proc resetbtn.Click()
        Thisform.oShp.Top=Thisform.oShp.oCtrl.Top
        Thisform.oShp.Left=Thisform.oShp.oCtrl.Left
        Thisform.oShp.Height=Thisform.oShp.oCtrl.Height
        Thisform.oShp.Width=Thisform.oShp.oCtrl.Width
        If !Thisform.bIntegratedPC
            Thisform.oShp.obj.Caption=Thisform.oShp.oCtrl.Caption
        Endif
        If Thisform.oShp.bHandlers
            Thisform.oShp.RemoveHandlers()
            Thisform.oShp.AddHandlers()
        Endif
        Thisform.ResetShpStockProps()
        Thisform.oShp.bModified=.T.
        Thisform.Release()
    Proc GetStockProps()
        Local i,N,v
        For i=1 To Alen(This.oShp.sProps) Step 2
            N=This.oShp.sProps[i]
            v=This.oShp.sProps[i+1]
            *wait window "GetStockProps "+n
            Do Case
                Case N='cfgHide'
                    This.hidecheck.Value=1
                    This.hideexpr.Value=v
            Endcase
        Next
    Proc SetStockProps()
        This.ResetShpStockProps()
        * --- hidecond
        If This.hidecheck.Value<>0
            This.SetShpStockProp('cfgHide',Trim(This.hideexpr.Value))
        Endif
    Proc ResetShpStockProps()
        Dimension This.oShp.sProps[2]
        This.oShp.sProps[1]=''
        This.oShp.sProps[2]=''
    Func HasCPEvents(i_cOp)
        Return(i_cOp='ecpQuit' Or i_cOp='ecpSave')
    Proc ecpQuit()
        This.btncancel.Click()
    Proc EcpSave()
        This.btnok.Click()
    Proc SetShpStockProp(cProp,cValue)
        Local N
        N=Alen(This.oShp.sProps)
        If N<>2 Or !Empty(This.oShp.sProps[1])
            Dimension This.oShp.sProps[n+2]
            N=N+2
        Endif
        This.oShp.sProps[n-1]=cProp
        This.oShp.sProps[n]=cValue
Enddefine

Define Class vrt_events_form As CpSysForm
    WindowType=1
    Caption="Events"
    Width=410
    Height=265-170
    Top=50
    Left=50
    Icon=i_cBmpPath+i_cStdIcon
    oForm=.Null.
    oCurrForm=.Null.
    Dimension cEvents[1]
    cEvents[1]=''
    Dimension cEventsCode[1]
    cEventsCode[1]=''
    cOldEventName=''
    Add Object eventcombo As ComboBox With Top=10,Left=10,Height=20,Width=200

    * --- Modificata la definizione del hideexpr da textbox a editbox e partato il limite della lunghezza a 1000 caratteri
    * --- add object eventcode as editbox with top=35,left=5,width=400,height=200
    Add Object eventcode As EditBox With Top=35,Left=5,Width=400,Height=25,MaxLength=MAX_CTRL_LENGHT

    Add Object btnok As CommandButton With Caption='Ok',Top=240-170,Left=300,Height=25,Width=50
    Add Object btncancel As CommandButton With Caption='Cancel',Top=240-170,Left=350,Height=25,Width=50
    Proc Init(oForm)
        This.oForm=oForm
        This.eventcombo.AddItem('Blank')
        This.eventcombo.AddItem('Delete end')
        This.eventcombo.AddItem('Delete start')
        This.eventcombo.AddItem('Done')
        This.eventcombo.AddItem('Init')
        This.eventcombo.AddItem('Insert end')
        This.eventcombo.AddItem('Insert started')
        This.eventcombo.AddItem('Load')
        This.eventcombo.AddItem('Record inserted')
        This.eventcombo.AddItem('Record updated')
        This.eventcombo.AddItem('Update end')
        This.eventcombo.AddItem('Update start')
        Local i,j
        j=1
        For i=1 To Alen(This.oForm.cEvents) Step 2
            If !Empty(This.oForm.cEvents[i])
                If Alen(This.cEvents)<j
                    Dimension This.cEvents[j]
                    Dimension This.cEventsCode[j]
                Endif
                This.cEvents[j]=This.oForm.cEvents[i]
                This.cEventsCode[j]=This.oForm.cEvents[i+1]
                j=j+1
            Endif
        Next
        This.oCurrForm=i_curform
        i_curform=This
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
    Proc Release()
        DoDefault()
        i_curform=This.oCurrForm
    Proc eventcombo.Click()
        Local N
        If !Empty(Thisform.cOldEventName)
            N=Ascan(Thisform.cEvents,Thisform.cOldEventName)
            If N<>0
                Thisform.cEventsCode[n]=Thisform.eventcode.Value
            Else
                N=Alen(Thisform.cEvents)+1
                Dimension Thisform.cEvents[n]
                Dimension Thisform.cEventsCode[n]
                Thisform.cEvents[n]=Thisform.cOldEventName
                Thisform.cEventsCode[n]=Thisform.eventcode.Value
            Endif
        Endif
        N=Ascan(Thisform.cEvents,This.Value)
        If N<>0
            Thisform.eventcode.Value=Thisform.cEventsCode[n]
        Else
            Thisform.eventcode.Value=''
        Endif
        Thisform.cOldEventName=This.Value
    Proc btnok.Click()
        Thisform.oForm.oTool.bModified=.T.
        Thisform.eventcombo.Click()
        Local i,j
        j=1
        For i=1 To Alen(Thisform.cEvents)
            If !Empty(Thisform.cEventsCode[i])
                Dimension Thisform.oForm.cEvents[j+1]
                Thisform.oForm.cEvents[j]=Thisform.cEvents[i]
                Thisform.oForm.cEvents[j+1]=Thisform.cEventsCode[i]
                j=j+2
            Endif
        Next
        Thisform.Release()
    Proc btncancel.Click()
        Thisform.Release()
    Func HasCPEvents(i_cOp)
        Return(i_cOp='ecpQuit' Or i_cOp='ecpSave')
    Proc ecpQuit()
        This.btncancel.Click()
    Proc EcpSave()
        This.btnok.Click()
Enddefine

Define Class rtpainter_pcDynamic As rtpainter_pccontainer
    Func GetID()
        Return(This.oCtrl.Parent.uid)
Enddefine
