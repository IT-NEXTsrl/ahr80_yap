* --- START CP_WRITELOG
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_writelog                                                     *
*              Scrive log                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-08-02                                                      *
* Last revis.: 2012-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cp_writelog
param w_aldattim,w_al____ms,w_al__tipo,w_alcmdsql,p_alserver,w_altipcon,w_altypedb,w_alproced,w_alcaptio,w_altottms,w_altottsc,w_alrowaff,w_alcursor,w_alerror,w_alcrdati,w_alcolore,w_aldearic,w_alopname

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_alcodute
  m.w_alcodute=0
  private w_alcodazi
  m.w_alcodazi=space(10)
  private w_alnumrig
  m.w_alnumrig=0
  private w_alnumber
  m.w_alnumber=0
  private w_alsqlchr
  m.w_alsqlchr=space(254)
  private w_alerrchr
  m.w_alerrchr=space(254)
  private w_allibnam
  m.w_allibnam=space(50)
  private w_al__host
  m.w_al__host=space(100)
  private w_alpostaz
  m.w_alpostaz=space(100)
  private w_alaziend
  m.w_alaziend=space(10)
  private w_alcmdsql_total
  m.w_alcmdsql_total=space(0)
  private w_alcmdsql_from_pos
  m.w_alcmdsql_from_pos=0
  private w_alcmdsql_to_pos
  m.w_alcmdsql_to_pos=0
  private w_alcmdsql_len
  m.w_alcmdsql_len=0
* --- WorkFile variables
  private show_log_idx
  show_log_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cp_writelog"
if vartype(__cp_writelog_hook__)='O'
  __cp_writelog_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cp_writelog('+Transform(w_aldattim)+','+Transform(w_al____ms)+','+Transform(w_al__tipo)+','+Transform(w_alcmdsql)+','+Transform(p_alserver)+','+Transform(w_altipcon)+','+Transform(w_altypedb)+','+Transform(w_alproced)+','+Transform(w_alcaptio)+','+Transform(w_altottms)+','+Transform(w_altottsc)+','+Transform(w_alrowaff)+','+Transform(w_alcursor)+','+Transform(w_alerror)+','+Transform(w_alcrdati)+','+Transform(w_alcolore)+','+Transform(w_aldearic)+','+Transform(w_alopname)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_writelog')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if cp_writelog_OpenTables()
  cp_writelog_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_show_log')
  use in _Curs_show_log
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cp_writelog('+Transform(w_aldattim)+','+Transform(w_al____ms)+','+Transform(w_al__tipo)+','+Transform(w_alcmdsql)+','+Transform(p_alserver)+','+Transform(w_altipcon)+','+Transform(w_altypedb)+','+Transform(w_alproced)+','+Transform(w_alcaptio)+','+Transform(w_altottms)+','+Transform(w_altottsc)+','+Transform(w_alrowaff)+','+Transform(w_alcursor)+','+Transform(w_alerror)+','+Transform(w_alcrdati)+','+Transform(w_alcolore)+','+Transform(w_aldearic)+','+Transform(w_alopname)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_writelog')
Endif
*--- Activity log
if vartype(__cp_writelog_hook__)='O'
  __cp_writelog_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cp_writelog_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Chiamato da activitylogger per inserire nella tabella SHOW_LOG col comando
  * --- Esempio di chiamata
  * --- WriteLOG( tTempoDiInizio, nMillisecondiDiInizio, cTipoOperazione, cFraseSQL, cNomeServer, cTipoConnessione,;
  *            cTipoDatabase, cProgramma, cTitolo, tDurataInMillisecondi, tDurataInSecondi,;
  *            cNumeroRighe, cCursore, cMessaggioDiErrore, iif(bMemorizzaCursore,i_DBFDati,Space(10)),;
  *            cColore, cDeadlockORiconnessione, cDescrizioneOperazione)
  i_nACTIVATEPROFILER_ROWNUMBER = NVL( i_nACTIVATEPROFILER_ROWNUMBER, 0 )
  if i_nACTIVATEPROFILER_ROWNUMBER = 0
    * --- Select from show_log
    i_nConn=i_TableProp[show_log_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[show_log_idx,2],.t.,show_log_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(alnumber) AS ALNUMBER from "+i_cTable+" show_log ";
           ,"_Curs_show_log")
    else
      select MAX(alnumber) AS ALNUMBER from (i_cTable);
        into cursor _Curs_show_log
    endif
    if used('_Curs_show_log')
      select _Curs_show_log
      locate for 1=1
      do while not(eof())
      i_nACTIVATEPROFILER_ROWNUMBER = NVL( ALNUMBER, 0 )
        select _Curs_show_log
        continue
      enddo
      use
    endif
  endif
  i_nACTIVATEPROFILER_ROWNUMBER = i_nACTIVATEPROFILER_ROWNUMBER + 1
  m.w_alcodute = i_CODUTE
  m.w_alcodazi = i_CODAZI
  m.w_alserver = LEFT( m.p_ALSERVER, 10 )
  m.w_alnumrig = i_nACTIVATEPROFILER_ROWNUMBER
  m.w_alnumber = i_nACTIVATEPROFILER_ROWNUMBER
  m.w_alsqlchr = cast( m.w_ALCMDSQL as char(254))
  m.w_alerrchr = cast( m.w_ALERROR as char(254))
  m.w_allibnam = LEFT(JUSTFNAME( m.w_ALPROCED )+SPACE(50),50)
  m.w_al__host = SYS( 0 )
  m.w_alpostaz = SYS( 0 )
  m.w_alaziend = i_CODAZI
  if upper( cp_dbtype )= "ORACLE" and LEN( m.w_ALCMDSQL ) > 3600
    m.w_alcmdsql_from_pos = 1
    m.w_alcmdsql_to_pos = 3600
    m.w_alcmdsql_len = LEN( m.w_ALCMDSQL )
    m.w_alcmdsql_total = m.w_alcmdsql
    * --- Spezza la frase in parti consecutive di lunghezza inferiore o uguale a 3600
    m.w_alcmdsql = substr( m.w_alcmdsql_total, m.w_alcmdsql_from_pos, m.w_alcmdsql_to_pos + 1 - m.w_alcmdsql_from_pos )
    do while m.w_alcmdsql_to_pos < m.w_alcmdsql_len
      * --- Determina una porzione della frase di lunghezza inferiore o uguale a 3600 in modo da non spezzare le parole
      * --- Se la posizione finale non � uno spazio, prende una parte di stringa pi� piccola in modo da non spezzare le parole
      if substr( m.w_alcmdsql_total, m.w_alcmdsql_to_pos, 1 ) <> " "
        if rat( " ", left( m.w_alcmdsql_total, m.w_alcmdsql_to_pos) ) > m.w_alcmdsql_from_pos
          m.w_alcmdsql_to_pos = rat( " ", left( m.w_alcmdsql_total, m.w_alcmdsql_to_pos) )
          m.w_alcmdsql = substr( m.w_alcmdsql_total, m.w_alcmdsql_from_pos, m.w_alcmdsql_to_pos + 1 - m.w_alcmdsql_from_pos )
        endif
      endif
      cp_writelog_Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Nuove posizioni iniziale e finale della parte di stringa da scrivere
      m.w_alcmdsql_from_pos = m.w_alcmdsql_to_pos + 1
      m.w_alcmdsql_to_pos = m.w_alcmdsql_to_pos + 3600
      * --- Parte di stringa da scrivere sul log. Se � di lunghezza inferiore a 3600 caratteri viene scritta fuori dal ciclo while
      m.w_alcmdsql = substr( m.w_alcmdsql_total, m.w_alcmdsql_from_pos, m.w_alcmdsql_to_pos + 1 - m.w_alcmdsql_from_pos )
      * --- Nuovo numero di riga da scrivere nel log
      i_nACTIVATEPROFILER_ROWNUMBER = i_nACTIVATEPROFILER_ROWNUMBER + 1
      m.w_alnumrig = i_nACTIVATEPROFILER_ROWNUMBER
      m.w_alnumber = i_nACTIVATEPROFILER_ROWNUMBER
    enddo
  endif
  cp_writelog_Pag2()
  if i_retcode='stop' or !empty(i_Error)
    return
  endif
endproc


procedure cp_writelog_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Se non � gi� stato fatto, crea una nuova connessione da utilizzare
  *     appositamente per le operazioni sulla tabella "show_log"
  if TYPE("L_show_log_connection") = "U"
    public L_show_log_connection
    L_show_log_connection = 0
  endif
  if L_show_log_connection = 0
    * --- Apre una nuova connessione
    L_show_log_connection = sqlstringconnect( SQLGETPROP(i_serverconn(1,2),"ConnectString") )
  endif
  * --- Assegna la nuova connessione alla tabella 'show_log'
  i_tableprop[ show_log_idx, 3] = L_show_log_connection
   i_nConn=i_TableProp[show_log_idx,3]
   i_Rows=SQLEXEC(i_nConn,"insert into show_log "+; 
 " ("+"aldattim"+",al____ms"+",al__tipo"+",alcmdsql"+",alserver"+",altipcon"+",altypedb"+",alproced"+",alcaptio"+",altottms"+",altottsc"+",alrowaff"+",alcursor"+",alerror"+",alcodute"+",alcodazi"+",alnumrig"+",alcrdati"+",alcolore"+",aldearic"+",alsqlchr"+",alerrchr"+",allibnam"+",al__host"+",alpostaz"+",alaziend"+",alnumber"+",alopname"+",cpccchk"+") values ("+; 
 cp_ToStrODBC(m.w_aldattim); 
 +","+cp_ToStrODBC(m.w_al____ms); 
 +","+cp_ToStrODBC(m.w_al__tipo); 
 +","+cp_ToStrODBC(m.w_alcmdsql); 
 +","+cp_ToStrODBC(m.w_alserver); 
 +","+cp_ToStrODBC(m.w_altipcon); 
 +","+cp_ToStrODBC(m.w_altypedb); 
 +","+cp_ToStrODBC(m.w_alproced); 
 +","+cp_ToStrODBC(m.w_alcaptio); 
 +","+cp_ToStrODBC(m.w_altottms); 
 +","+cp_ToStrODBC(m.w_altottsc); 
 +","+cp_ToStrODBC(m.w_alrowaff); 
 +","+cp_ToStrODBC(m.w_alcursor); 
 +","+cp_ToStrODBC(m.w_alerror); 
 +","+cp_ToStrODBC(m.w_alcodute); 
 +","+cp_ToStrODBC(m.w_alcodazi); 
 +","+cp_ToStrODBC(m.w_alnumrig); 
 +","+cp_ToStrODBC(m.w_alcrdati); 
 +","+cp_ToStrODBC(m.w_alcolore); 
 +","+cp_ToStrODBC(m.w_aldearic); 
 +","+cp_ToStrODBC(m.w_alsqlchr); 
 +","+cp_ToStrODBC(m.w_alerrchr); 
 +","+cp_ToStrODBC(m.w_allibnam); 
 +","+cp_ToStrODBC(m.w_al__host); 
 +","+cp_ToStrODBC(m.w_alpostaz); 
 +","+cp_ToStrODBC(m.w_alaziend); 
 +","+cp_ToStrODBC(m.w_alnumber); 
 +","+cp_ToStrODBC(m.w_alopname); 
 +", 'show_log__' )")
endproc


  function cp_writelog_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='show_log'
    return(cp_OpenFuncTables(1))
* --- END CP_WRITELOG
* --- START CP_PRINTCONDITION
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_printcondition                                               *
*              cp_PrintCondition                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-08                                                      *
* Last revis.: 2013-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cp_printcondition
param pParobj,pPar

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RESULT
  m.w_RESULT=.f.
  private w_BTNANTE
  m.w_BTNANTE=.f.
  private w_OBJ
  m.w_OBJ = .null.
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cp_printcondition"
if vartype(__cp_printcondition_hook__)='O'
  __cp_printcondition_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cp_printcondition('+Transform(pParobj)+','+Transform(pPar)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_printcondition')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
cp_printcondition_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cp_printcondition('+Transform(pParobj)+','+Transform(pPar)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_printcondition')
Endif
*--- Activity log
if vartype(__cp_printcondition_hook__)='O'
  __cp_printcondition_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cp_printcondition_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  m.w_OBJ = m.pParobj
  do case
    case m.pPar="Btn_Ante_Edit"
      m.w_RESULT = (NOT bReportVuoto AND m.w_OBJ.cNomeReport.GetText(m.w_OBJ.nIndexReport) AND cp_IsStdFile(m.w_OBJ.cNomeReport.GetReport(m.w_OBJ.nIndexReport),"FXP") ) OR ( NOT m.w_OBJ.cNomeReport.GetText(m.w_OBJ.nIndexReport) AND m.w_OBJ.i_FDFFile) OR (NOT m.w_OBJ.cNomeReport.GetText(m.w_OBJ.nIndexReport) AND NOT m.w_OBJ.i_FDFFile AND ((! bReportVuoto AND m.w_OBJ.w_FRXFILE ) OR m.w_OBJ.w_treport="I" )AND iif( m.w_OBJ.pdProcesso, m.w_OBJ.cAnteprimaSN,.T.) and iif(m.w_OBJ.pdProcesso, m.w_OBJ.pdnTotDocOk>0 and not( m.w_OBJ.pdEXEProcesso),.T.) ) AND m.w_OBJ.w_SELECTED>0 AND m.w_OBJ.w_BTNANTE
      i_retcode = 'stop'
      i_retval = m.w_RESULT
      return
    case m.pPar="PDBtn_Proc_Edit"
      i_retcode = 'stop'
      i_retval = NOT bReportVuoto AND m.w_OBJ.pdProcesso and m.w_OBJ.pdnTotDocOk>0 and not( m.w_OBJ.pdEXEProcesso) AND m.w_OBJ.w_SELECTED>0 AND m.w_OBJ.w_PDBTN
      return
    case m.pPar="Btn_Mail_Edit"
      m.w_BTNANTE = cp_PrintCondition (m.w_OBJ, "Btn_Ante_Edit") 
      m.w_RESULT = m.w_OBJ.w_treport<>"I" AND (NOT bReportVuoto AND m.w_OBJ.cNomeReport.GetText( m.w_OBJ.nIndexReport) AND m.w_OBJ.w_FRXGRPFILE AND m.w_OBJ.w_SELECTED>0 OR ( m.w_OBJ.w_SELECTED>0 AND (NOT( m.w_OBJ.cNomeReport.GetText( m.w_OBJ.nIndexReport)) ) OR (NOT( m.w_OBJ.cNomeReport.GetText( m.w_OBJ.nIndexReport)) AND (NOT m.w_OBJ.cNomeReport.GetText( m.w_OBJ.nIndexReport) AND m.w_BTNANTE )))) AND m.w_OBJ.w_FRXGRPFILE AND NOT bReportVuoto 
      i_retcode = 'stop'
      i_retval = m.w_RESULT
      return
    case m.pPar="Btn_Fax_Edit"
      m.w_RESULT = !bReportVuoto AND m.w_OBJ.w_treport<>"I" AND !m.w_OBJ.cNomeReport.GetText( m.w_OBJ.nIndexReport) AND m.w_OBJ.w_SELECTED>0 AND ( m.w_OBJ.i_FDFFile OR ((m.w_OBJ.cNomeReport.GetText(m.w_OBJ.nIndexReport) AND cp_IsStdFile(m.w_OBJ.cNomeReport.GetReport(m.w_OBJ.nIndexReport),"FXP") ) OR ( NOT m.w_OBJ.cNomeReport.GetText(m.w_OBJ.nIndexReport) AND m.w_OBJ.i_FDFFile) OR (NOT m.w_OBJ.cNomeReport.GetText(m.w_OBJ.nIndexReport) AND NOT m.w_OBJ.i_FDFFile AND ! bReportVuoto AND m.w_OBJ.w_FRXFILE AND iif( m.w_OBJ.pdProcesso, m.w_OBJ.cAnteprimaSN,.T.) and iif(m.w_OBJ.pdProcesso, m.w_OBJ.pdnTotDocOk>0 and not( m.w_OBJ.pdEXEProcesso),.T.) ) AND m.w_OBJ.w_SELECTED>0 ) )
      i_retcode = 'stop'
      i_retval = m.w_RESULT
      return
    case m.pPar="Btn_WWP_ZCP_Edit"
      m.w_RESULT =  m.w_OBJ.w_treport<>"I" AND ( iif(g_WEENABLED="S" and g_IZCP<>"S" and g_IZCP<>"A", g_WEENABLED="S" and g_WEUTEENABLED="S", (g_IZCP="S" or g_IZCP="A" or g_CPIN="S") and g_ZCPUENABLED="S") AND m.w_OBJ.w_SELECTED>0 AND m.w_OBJ.cNomeReport.GetText( m.w_OBJ.nIndexReport) AND m.w_OBJ.w_FRXGRPFILE AND m.w_OBJ.w_SELECTED>0 OR ( m.w_OBJ.w_SELECTED>0 AND (NOT( m.w_OBJ.cNomeReport.GetText( m.w_OBJ.nIndexReport)) ) OR (NOT( m.w_OBJ.cNomeReport.GetText( m.w_OBJ.nIndexReport)) AND (NOT m.w_OBJ.cNomeReport.GetText( m.w_OBJ.nIndexReport) OR m.w_OBJ.oBtn_Ante.enabled )))) AND m.w_OBJ.w_FRXPDFFILE
      i_retcode = 'stop'
      i_retval = m.w_RESULT
      return
    case m.pPar="Btn_Plite_Edit"
      m.w_RESULT =  m.w_OBJ.w_treport<>"I" AND NOT bReportVuoto AND !m.w_OBJ.cNomeReport.GetText(m.w_OBJ.nIndexReport) AND (chkplite(oRepPlite)=0) AND m.w_OBJ.w_SELECTED>0
      i_retcode = 'stop'
      i_retval = m.w_RESULT
      return
    case m.pPar="Btn_Word_Edit"
      m.w_RESULT = m.w_OBJ.w_treport<>"I" AND !bReportVuoto AND m.w_OBJ.w_SELECTED>0 AND m.w_OBJ.cNomeReport.CheckWord() AND not m.w_OBJ.pdProcesso
      i_retcode = 'stop'
      i_retval = m.w_RESULT
      return
    case m.pPar="Btn_Excel_Edit"
      m.w_RESULT = m.w_OBJ.w_treport<>"I" AND NOT bReportVuoto AND m.w_OBJ.w_tReport <> "I" AND (g_OFFICE="O" OR g_OFFICE="M" ) AND m.w_OBJ.w_SELECTED>0
      i_retcode = 'stop'
      i_retval = m.w_RESULT
      return
    case m.pPar="Btn_Graph_Edit"
      m.w_RESULT = m.w_OBJ.w_treport<>"I" AND (!bReportVuoto AND m.w_OBJ.cNomeReport.CheckGraphVer() ="G" AND not m.w_OBJ.pdProcesso) OR (m.w_OBJ.cNomeReport.CheckGraphVer() <>"G" AND m.w_OBJ.cNomeReport.CheckGraphVer() <>"N" AND not m.w_OBJ.pdProcesso)
      i_retcode = 'stop'
      i_retval = m.w_RESULT
      return
  endcase
endproc


* --- END CP_PRINTCONDITION
* --- START GETSQLSTATEMENT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: getsqlstatement                                                 *
*              Ottiene frase sql da query                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-29                                                      *
* Last revis.: 2012-02-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func getsqlstatement
param pFileVqr

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_ALCMDSQL
  m.w_ALCMDSQL=space(0)
  private w_FILE
  m.w_FILE=space(254)
  private w_Qry
  m.w_Qry = .null.
  private w_QryLoader
  m.w_QryLoader = .null.
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "getsqlstatement"
if vartype(__getsqlstatement_hook__)='O'
  __getsqlstatement_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'getsqlstatement('+Transform(pFileVqr)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getsqlstatement')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
getsqlstatement_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'getsqlstatement('+Transform(pFileVqr)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getsqlstatement')
Endif
*--- Activity log
if vartype(__getsqlstatement_hook__)='O'
  __getsqlstatement_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure getsqlstatement_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Questa funzione restituisce la statement SQL data una visual query
  * --- pFileVqr nome della visual query da interpretare 
  * --- --
  * --- --
  m.w_ALCMDSQL = ""
  if VARTYPE(m.pFileVqr)="C" AND NOT EMPTY (m.pFileVqr)
    m.w_FILE = ALLTRIM(m.pFileVqr)
  else
    m.w_FILE = GetFile("vqr")
  endif
  if NOT EMPTY (m.w_FILE) AND CP_FILEEXIST(m.w_FILE)
    w_Qry=createobject("cpquery")
    w_QryLoader=createobject("cpqueryloader")
    if m.w_QryLoader.loadquery(FORCEEXT(m.w_FILE,""), m.w_Qry, .null.)
      m.w_ALCMDSQL = m.w_Qry.mdoquery("","Get")
    endif
    m.w_Qry = .null.
    m.w_QryLoader = .null.
  endif
  i_retcode = 'stop'
  i_retval = m.w_ALCMDSQL
  return
endproc


* --- END GETSQLSTATEMENT
* --- START CP_SHOWDBF_COMMIT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_showdbf_commit                                               *
*              Esegue commit dalla maschera CP_SHOWDBF                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-08                                                      *
* Last revis.: 2012-03-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cp_showdbf_commit

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cp_showdbf_commit"
if vartype(__cp_showdbf_commit_hook__)='O'
  __cp_showdbf_commit_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cp_showdbf_commit()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_showdbf_commit')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
cp_showdbf_commit_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cp_showdbf_commit()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_showdbf_commit')
Endif
*--- Activity log
if vartype(__cp_showdbf_commit_hook__)='O'
  __cp_showdbf_commit_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cp_showdbf_commit_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Esegue commit dalla maschera CP_SHOWDBF
  * --- commit
  cp_EndTrs(.t.)
  i_retcode = 'stop'
  i_retval = .f.
  return
endproc


* --- END CP_SHOWDBF_COMMIT
* --- START CP_SHOWDBF_BEGIN
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_showdbf_begin                                                *
*              Esegue begin transaction dalla maschera CP_SHOWDBF              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-08                                                      *
* Last revis.: 2012-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cp_showdbf_begin

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cp_showdbf_begin"
if vartype(__cp_showdbf_begin_hook__)='O'
  __cp_showdbf_begin_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cp_showdbf_begin()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_showdbf_begin')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
cp_showdbf_begin_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cp_showdbf_begin()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_showdbf_begin')
Endif
*--- Activity log
if vartype(__cp_showdbf_begin_hook__)='O'
  __cp_showdbf_begin_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cp_showdbf_begin_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Esegue begin transaction dalla maschera CP_SHOWDBF
  * --- begin transaction
  cp_BeginTrs()
  i_retcode = 'stop'
  i_retval = .t.
  return
endproc


* --- END CP_SHOWDBF_BEGIN
* --- START CP_SHOWDBF_ROLLBACK
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_showdbf_rollback                                             *
*              Esegue rollback dalla maschera CP_SHOWDBF                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-08                                                      *
* Last revis.: 2012-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cp_showdbf_rollback

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cp_showdbf_rollback"
if vartype(__cp_showdbf_rollback_hook__)='O'
  __cp_showdbf_rollback_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cp_showdbf_rollback()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_showdbf_rollback')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
cp_showdbf_rollback_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cp_showdbf_rollback()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_showdbf_rollback')
Endif
*--- Activity log
if vartype(__cp_showdbf_rollback_hook__)='O'
  __cp_showdbf_rollback_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cp_showdbf_rollback_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Esegue rollback dalla maschera CP_SHOWDBF
  * --- rollback
  bTrsErr=.t.
  cp_EndTrs(.t.)
  i_retcode = 'stop'
  i_retval = .f.
  return
endproc


* --- END CP_SHOWDBF_ROLLBACK
* --- START CP_SHOWDBF_SQL
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_showdbf_sql                                                  *
*              Esegue comandi sql dalla maschera CP_SHOWDBF                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-08                                                      *
* Last revis.: 2012-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cp_showdbf_sql
param pCMDSQL,pCPSHOWDBF

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
* --- WorkFile variables
  private AZIENDA_idx
  AZIENDA_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cp_showdbf_sql"
if vartype(__cp_showdbf_sql_hook__)='O'
  __cp_showdbf_sql_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cp_showdbf_sql('+Transform(pCMDSQL)+','+Transform(pCPSHOWDBF)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_showdbf_sql')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if cp_showdbf_sql_OpenTables()
  cp_showdbf_sql_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cp_showdbf_sql('+Transform(pCMDSQL)+','+Transform(pCPSHOWDBF)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_showdbf_sql')
Endif
*--- Activity log
if vartype(__cp_showdbf_sql_hook__)='O'
  __cp_showdbf_sql_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cp_showdbf_sql_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Esegue comandi sql dalla maschera CP_SHOWDBF
  if m.pCPSHOWDBF.w_ISTRSEL="S" AND NOT UPPER( ALLTRIM (m.pCMDSQL) ) = "SELECT"
    CP_ERRORMSG("Sono consentite solo istruzioni di tipo Select",64)
    i_retcode = 'stop'
    return
  endif
  i_nConn = i_TableProp[AZIENDA_idx,3]
  if UPPER( ALLTRIM (m.pCMDSQL) ) = "SELECT"
    w_result = CP_SQLEXEC(i_nConn, m.pCMDSQL, m.pCPSHOWDBF.oPARENTOBJECT.w_SHOWDBF_CURS)
  else
    w_result = cp_TrsSQL(i_nConn, m.pCMDSQL )
  endif
  * --- Memorizza nel cursore dei comandi il comando eseguito nella maschera CP_SHOWDBF
  SELECT (m.pCPSHOWDBF.w_CURS_SQL)
  APPEND BLANK
  REPLACE COMANDO WITH m.pCMDSQL
  if w_result=-1
    CP_ERRORMSG(message(),16)
    bTrsErr=.f.
  else
    pCPSHOWDBF.w_BROWSEOBJ.BROWSE()
    pCPSHOWDBF.SETCONTROLSVALUE()
  endif
  i_retcode = 'stop'
  i_retval = .t.
  return
endproc


  function cp_showdbf_sql_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='AZIENDA'
    return(cp_OpenFuncTables(1))
* --- END CP_SHOWDBF_SQL
* --- START CP_WRCURSOR
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_wrcursor                                                     *
*              Rende il cursore scrivibile                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-26                                                      *
* Last revis.: 2013-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cp_wrcursor
param cCursor

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_NOME
  m.w_NOME=space(10)
  private w_POSI
  m.w_POSI=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cp_wrcursor"
if vartype(__cp_wrcursor_hook__)='O'
  __cp_wrcursor_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cp_wrcursor('+Transform(cCursor)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_wrcursor')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
cp_wrcursor_Page_1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cp_wrcursor('+Transform(cCursor)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_wrcursor')
Endif
*--- Activity log
if vartype(__cp_wrcursor_hook__)='O'
  __cp_wrcursor_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cp_wrcursor_Page_1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Rende il cursore passato scrivibile - utilizzato per scrivere negli oggetti Zoom
  m.w_NOME = Sys(2015)
  * --- Accedo direttamente al temporaneo in cui sono contenute le informazioni del cursore
  Select(m.cCursor)
  m.w_POSI = Select()
  * --- Faccio una copia
  Use Dbf() Again In 0 Alias (m.w_NOME)
  Use In Select(m.cCursor)
  Select(m.w_NOME)
  * --- Ricopio la copia nel cursore originale, precedentemente chiuso
  Use Dbf() Again In (m.w_POSI) Alias (m.cCursor)
  Use In Select(m.w_NOME)
  Select(m.cCursor)
endproc


* --- END CP_WRCURSOR
* --- START CP_VMNMEM
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_vmnmem                                                       *
*              Carica/Salva Menu su database                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-05-25                                                      *
* Last revis.: 2015-08-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cp_vmnmem
param pVmnFile

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_MenuKey
  m.w_MenuKey=space(20)
  private w_Timestamp
  m.w_Timestamp=space(20)
  private w_DBTimestamp
  m.w_DBTimestamp=space(20)
  private w_bUpdate
  m.w_bUpdate=.f.
  private w_Cursor
  m.w_Cursor=space(10)
  private w_Temp
  m.w_Temp=space(254)
  private w_InfinityProc
  m.w_InfinityProc=space(254)
  private w_InfinityMod
  m.w_InfinityMod=.f.
  private w_CODUTE
  m.w_CODUTE=0
  private w_CODAZI
  m.w_CODAZI=space(5)
  private w_CACHEOK
  m.w_CACHEOK=.f.
* --- WorkFile variables
  private cpvmnmem_idx
  cpvmnmem_idx=0
  private cpusrvmn_idx
  cpusrvmn_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 2
  private i_func_name
  i_func_name = "cp_vmnmem"
if vartype(__cp_vmnmem_hook__)='O'
  __cp_vmnmem_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cp_vmnmem('+Transform(pVmnFile)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_vmnmem')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if cp_vmnmem_OpenTables()
  cp_vmnmem_Page_1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cp_vmnmem('+Transform(pVmnFile)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cp_vmnmem')
Endif
*--- Activity log
if vartype(__cp_vmnmem_hook__)='O'
  __cp_vmnmem_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cp_vmnmem_Page_1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Carica/Salva Menu su database
  m.w_CODUTE = i_CODUTE
  m.w_CODAZI = Alltrim(i_CODAZI)
  m.w_MenuKey = ""
  m.w_Timestamp = ""
  m.w_CACHEOK = .t.
  For m.l_i=1 To Alen(g_aMenuAsMem,1)
  m.w_MenuKey = m.w_MenuKey+Alltrim(g_aMenuAsMem(m.l_i,1))
  m.w_Temp = g_aMenuAsMem(m.l_i,2)
  m.w_Temp = Strtran(m.w_Temp,"/")
  m.w_Temp = Strtran(m.w_Temp," ")
  m.w_Temp = Strtran(m.w_Temp,":")
  m.w_Timestamp = m.w_Timestamp+m.w_Temp
  Endfor
  * --- *--- Aggiungo temporaneamente la cp_sec per poter richiamare la funzione di criptazione
  m.w_MenuKey = StrConv(Hash(m.w_MenuKey,5),15)
  m.w_Timestamp = StrConv(Hash(m.w_Timestamp,5),15)
  if Vartype(m.pVmnFile)=="C" And !Empty(m.pVmnFile)
    m.w_Cursor = Sys(2015)
    m.w_DBTimestamp = ""
    * --- Read from cpvmnmem
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[cpvmnmem_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[cpvmnmem_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MMLSTUPD"+;
        " from "+i_cTable+" cpvmnmem where ";
            +"MMMENUID = "+cp_ToStrODBC(m.w_MenuKey);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MMLSTUPD;
        from (i_cTable) where;
            MMMENUID = m.w_MenuKey;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_DBTimestamp = NVL(cp_ToDate(_read_.MMLSTUPD),cp_NullValue(_read_.MMLSTUPD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if !Empty(m.w_DBTimestamp)
      * --- Verifico se deve essere aggiornato
      if m.w_DBTimestamp<>m.w_Timestamp
        * --- Delete from cpvmnmem
        i_nConn=i_TableProp[cpvmnmem_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[cpvmnmem_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MMMENUID = "+cp_ToStrODBC(m.w_MenuKey);
                 )
        else
          delete from (i_cTable) where;
                MMMENUID = m.w_MenuKey;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        m.w_bUpdate = .T.
      else
        * --- Carico menu da db
        Local l_oldArea, cMenuCursor 
 m.cMenuCursor = Sys(2015) 
 m.l_oldArea = Select() 
 do vq_exec with "..\vfcsim\cp_vmnmem.vqr",createobject("cp_getfuncvar"),m.cMenuCursor,"",.f.,.t. 
 Select * from (m.cMenuCursor) into Cursor (i_CUR_MENU) ReadWrite 
 Use in Select(m.cMenuCursor) 
 Select(i_CUR_MENU) 
 Go Top 
 
        * --- Nel caso in cui ENABLED sia numerico devo travasarlo in una nuova colonna per farlo tornare logico
        *     Il problema si presenta con Oracle
        if Vartype(Enabled)=="N"
          Alter Table (i_CUR_MENU) Add Column Enabled2 L 
 Update (i_CUR_MENU) set Enabled2=(Enabled==1) 
 Alter Table (i_CUR_MENU) Drop Column Enabled 
 Alter Table (i_CUR_MENU) Rename Column Enabled2 To Enabled
        endif
        * --- Risetto MODULO a stringa vuota e cambio il nome della colonna LEVEL
        *     Oracle restituisce Null per le stringhe vuote e non accetta alias di nome "Level"
        Update (i_CUR_MENU) set Modulo="" Where isNull(Modulo) 
 Alter Table (i_CUR_MENU) Rename Column Qrylevel to Level 
 Index On LVLKEY Tag LVLKEY COLLATE "MACHINE" 
 Select(m.l_oldArea)
        m.w_bUpdate = .F.
      endif
    else
      * --- Nuovo menu, va caricato e poi salvato su db
      m.w_bUpdate = .T.
    endif
    * --- Se necessario faccio un nuovo inserimento su db
    if m.w_bUpdate
      * --- Carica Menu da FIle
      DO Vmn_To_Cur IN vu_exec WITH ForceExt(Alltrim(m.pVmnFile),".VMN"), i_CUR_MENU 
 DO merge_all IN vu_exec WITH i_CUR_MENU, i_cModules
      cp_vmnmem_Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  else
    * --- Cancello il contenuto delle tabelle
    * --- Delete from cpusrvmn
    i_nConn=i_TableProp[cpusrvmn_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[cpusrvmn_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where 1=1")
    else
      delete from (i_cTable) where 1=1
      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from cpvmnmem
    i_nConn=i_TableProp[cpvmnmem_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[cpvmnmem_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where 1=1")
    else
      delete from (i_cTable) where 1=1
      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    cp_vmnmem_Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_errormsg("Pulizia salvataggio menu su database completata")
  endif
  if m.w_CACHEOK
    * --- Se necessario associo questo menu alla coppia utente-azienda utilizzati per la login
    m.w_Temp = ""
    * --- Read from cpusrvmn
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[cpusrvmn_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[cpusrvmn_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "id_menu"+;
        " from "+i_cTable+" cpusrvmn where ";
            +"cod_ute = "+cp_ToStrODBC(m.w_CODUTE);
            +" and cod_azi = "+cp_ToStrODBC(m.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        id_menu;
        from (i_cTable) where;
            cod_ute = m.w_CODUTE;
            and cod_azi = m.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_Temp = NVL(cp_ToDate(_read_.id_menu),cp_NullValue(_read_.id_menu))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case Empty(m.w_Temp)
        * --- Insert into cpusrvmn
        i_nConn=i_TableProp[cpusrvmn_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[cpusrvmn_idx,2])
        i_commit = .f.
        local bErr_026AF6C0
        bErr_026AF6C0=bTrsErr
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        func_SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,cpusrvmn_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"cod_ute"+",cod_azi"+",id_menu"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(m.w_CODUTE),'cpusrvmn','cod_ute');
          +","+cp_NullLink(cp_ToStrODBC(m.w_CODAZI),'cpusrvmn','cod_azi');
          +","+cp_NullLink(cp_ToStrODBC(m.w_MenuKey),'cpusrvmn','id_menu');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'cod_ute',m.w_CODUTE,'cod_azi',m.w_CODAZI,'id_menu',m.w_MenuKey)
          insert into (i_cTable) (cod_ute,cod_azi,id_menu &i_ccchkf. );
             values (;
               m.w_CODUTE;
               ,m.w_CODAZI;
               ,m.w_MenuKey;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          bTrsErr=bErr_026AF6C0
        ah_errormsg("Associazione menu per (%1-%2) non riuscita",,,m.w_CODUTE,m.w_CODAZI)
        endif
      case m.w_Temp<>m.w_MenuKey
        * --- Write into cpusrvmn
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[cpusrvmn_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[cpusrvmn_idx,2])
        i_ccchkf=''
        func_SetCCCHKVarsWrite(@i_ccchkf,cpusrvmn_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"id_menu = "+cp_NullLink(cp_ToStrODBC(m.w_MenuKey),'cpusrvmn','id_menu');
              +i_ccchkf ;
          +" where ";
              +"cod_ute = "+cp_ToStrODBC(m.w_CODUTE);
              +" and cod_azi = "+cp_ToStrODBC(m.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              id_menu = m.w_MenuKey;
              &i_ccchkf. ;
           where;
              cod_ute = m.w_CODUTE;
              and cod_azi = m.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if i_Rows=0
        ah_errormsg("Associazione menu per (%1-%2) non riuscita",,,m.w_CODUTE,m.w_CODAZI)
        endif
    endcase
  endif
  if Type("i_bFox26")<>"L" And Vartype(m.pVmnFile)=="C" And !Empty(m.pVmnFile)
    DO CaricaMenu IN vu_exec WITH i_CUR_MENU,.F.
  endif
endproc


procedure cp_vmnmem_Page_2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Salvataggio del menu su db
  * --- begin transaction
  cp_BeginTrs()
  * --- Try
  cp_vmnmem_Try_026B2120()
  * --- Catch
  if !empty(i_Error)
    i_ErrMsg=i_Error
    i_Error=''
    ah_errormsg("Caching menu su db non riuscito")
    m.w_CACHEOK = .f.
    m.w_bUpdate = .F.
    * --- rollback
    bTrsErr=.t.
    cp_EndTrs(.t.)
  endif
  * --- End
endproc
  proc cp_vmnmem_Try_026B2120()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  Local l_oldArea 
 m.l_oldArea = Select() 
 Select(i_CUR_MENU) 
 Locate for 1=1 
 Scan
  m.w_InfinityProc = Iif(DIRECTORY==1, 'function:openAhr("menu='+StrTran(Alltrim(INSERTION),Chr(172),"||")+'")', "")
  m.w_InfinityMod = ENABLED
  m.w_Temp = Upper(Alltrim(MODULO))
  if !Empty(m.w_Temp)
    if At("(",m.w_Temp)>0
      m.w_Temp = Strtran(m.w_Temp,"*","'")
      m.w_Temp = Strtran(m.w_Temp,"�","'")
      m.w_InfinityMod = Eval(m.w_Temp)
    else
      if At(","+m.w_Temp+",",","+i_cModules+",")=0
        m.w_InfinityMod = .F.
      endif
    endif
  endif
  * --- Insert into cpvmnmem
  i_nConn=i_TableProp[cpvmnmem_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[cpvmnmem_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  i_ccchkf=''
  i_ccchkv=''
  func_SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,cpvmnmem_idx,i_nConn)
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                " ("+"MMMENUID"+",MMLSTUPD"+",MM__VOCE"+",MM_LEVEL"+",MMPROCED"+",MMINFPRO"+",MMDIRECT"+",MMRIFPAD"+",MMELEMEN"+",MM_ROWID"+",MMENABLE"+",MM_PROGR"+",MM_IMAGE"+",MMMODULO"+",MMINFMOD"+",MMINSPNT"+",MM_POSIT"+",MMDELETE"+",MMLVLKEY"+",MMINSERT"+",MM__NOTE"+",MM___MRU"+",MMTEAROF"+i_ccchkf+") values ("+;
    cp_NullLink(cp_ToStrODBC(m.w_MenuKey),'cpvmnmem','MMMENUID');
    +","+cp_NullLink(cp_ToStrODBC(m.w_Timestamp),'cpvmnmem','MMLSTUPD');
    +","+cp_NullLink(cp_ToStrODBC(VoceMenu),'cpvmnmem','MM__VOCE');
    +","+cp_NullLink(cp_ToStrODBC(Level),'cpvmnmem','MM_LEVEL');
    +","+cp_NullLink(cp_ToStrODBC(NameProc),'cpvmnmem','MMPROCED');
    +","+cp_NullLink(cp_ToStrODBC(m.w_InfinityProc),'cpvmnmem','MMINFPRO');
    +","+cp_NullLink(cp_ToStrODBC(Directory),'cpvmnmem','MMDIRECT');
    +","+cp_NullLink(cp_ToStrODBC(RifPadre),'cpvmnmem','MMRIFPAD');
    +","+cp_NullLink(cp_ToStrODBC(Elementi),'cpvmnmem','MMELEMEN');
    +","+cp_NullLink(cp_ToStrODBC(Id),'cpvmnmem','MM_ROWID');
    +","+cp_NullLink(cp_ToStrODBC(Enabled),'cpvmnmem','MMENABLE');
    +","+cp_NullLink(cp_ToStrODBC(Progress),'cpvmnmem','MM_PROGR');
    +","+cp_NullLink(cp_ToStrODBC(CpBmpName),'cpvmnmem','MM_IMAGE');
    +","+cp_NullLink(cp_ToStrODBC(Modulo),'cpvmnmem','MMMODULO');
    +","+cp_NullLink(cp_ToStrODBC(m.w_InfinityMod),'cpvmnmem','MMINFMOD');
    +","+cp_NullLink(cp_ToStrODBC(Ins_Point),'cpvmnmem','MMINSPNT');
    +","+cp_NullLink(cp_ToStrODBC(Position),'cpvmnmem','MM_POSIT');
    +","+cp_NullLink(cp_ToStrODBC(Deleted),'cpvmnmem','MMDELETE');
    +","+cp_NullLink(cp_ToStrODBC(LvlKey),'cpvmnmem','MMLVLKEY');
    +","+cp_NullLink(cp_ToStrODBC(Insertion),'cpvmnmem','MMINSERT');
    +","+cp_NullLink(cp_ToStrODBC(Note),'cpvmnmem','MM__NOTE');
    +","+cp_NullLink(cp_ToStrODBC(Mru),'cpvmnmem','MM___MRU');
    +","+cp_NullLink(cp_ToStrODBC(TearOff),'cpvmnmem','MMTEAROF');
         +i_ccchkv+")")
  else
    cp_CheckDeletedKey(i_cTable,0,'MMMENUID',m.w_MenuKey,'MMLSTUPD',m.w_Timestamp,'MM__VOCE',VoceMenu,'MM_LEVEL',Level,'MMPROCED',NameProc,'MMINFPRO',m.w_InfinityProc,'MMDIRECT',Directory,'MMRIFPAD',RifPadre,'MMELEMEN',Elementi,'MM_ROWID',Id,'MMENABLE',Enabled,'MM_PROGR',Progress)
    insert into (i_cTable) (MMMENUID,MMLSTUPD,MM__VOCE,MM_LEVEL,MMPROCED,MMINFPRO,MMDIRECT,MMRIFPAD,MMELEMEN,MM_ROWID,MMENABLE,MM_PROGR,MM_IMAGE,MMMODULO,MMINFMOD,MMINSPNT,MM_POSIT,MMDELETE,MMLVLKEY,MMINSERT,MM__NOTE,MM___MRU,MMTEAROF &i_ccchkf. );
       values (;
         m.w_MenuKey;
         ,m.w_Timestamp;
         ,VoceMenu;
         ,Level;
         ,NameProc;
         ,m.w_InfinityProc;
         ,Directory;
         ,RifPadre;
         ,Elementi;
         ,Id;
         ,Enabled;
         ,Progress;
         ,CpBmpName;
         ,Modulo;
         ,m.w_InfinityMod;
         ,Ins_Point;
         ,Position;
         ,Deleted;
         ,LvlKey;
         ,Insertion;
         ,Note;
         ,Mru;
         ,TearOff;
         &i_ccchkv. )
    i_Rows=iif(bTrsErr,0,1)
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if i_Rows<0 or bTrsErr
    * --- Error: insert not accepted
    i_Error=MSG_INSERT_ERROR
    return
  endif
  EndScan
  * --- commit
  cp_EndTrs(.t.)
    return


  function cp_vmnmem_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='cpvmnmem'
    i_cWorkTables[2]='cpusrvmn'
    return(cp_OpenFuncTables(2))
* --- END CP_VMNMEM
