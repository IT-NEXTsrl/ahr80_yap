* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_monitor                                                      *
*              Framework Monitor                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-29                                                      *
* Last revis.: 2009-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_monitor
*--- Indici colonne cp_monitor
#Define FILENAME_IDX 1
#Define FILEDESC_IDX 2
#Define FLSTATUS_IDX 3
#Define FLCUSTOM_IDX 4
#Define FILETYPE_IDX 5
#Define FLDESUTE_IDX 6
#Define FLDESGRP_IDX 7
#Define FLDESAZI_IDX 8
#Define FLORGNAM_IDX 9
#Define FLCODUTE_IDX 10
#Define FLCODGRP_IDX 11   
#Define FLCODAZI_IDX 12
#Define FLREFFIL_IDX 13
#Define FL_ERASE_IDX 14
#Define FLMODFIL_IDX 15
#Define FLNEWFIL_IDX 16
#Define FLREVFIL_IDX 17
#Define FLAUTHOR_IDX 18
#Define FL_NOTES_IDX 19

*--- Blocca l'oggetto a chi non ha i diritti
IF cp_set_get_right()<3 Or !i_MonitorFramework
   cp_errormsg(MSG_ACCESS_DENIED,'stop')
   RETURN 
ENDIF 

*--- Controllo apertura di pi� di 1 monitor
local oForm
For Each oForm in _screen.Forms
   If vartype(oForm.cPrg)='C' And Upper(oForm.cPrg) = 'CP_MONITOR'
     return
   endif
Next

*--- Apertura Framework Monitor
*oFrmwkMonitor = CREATEOBJECT("FrmwkMon_Toolbar", i_VisualTheme<>-1 And Vartype(_Screen.cp_ThemesManager)=="O")
*i_curform = oFrmwkMonitor
*oFrmwkMonitor.Start()
return(createobject("tcp_monitor",oParentObject))

DEFINE CLASS FrmwkMon_Toolbar AS CommandBar
   oForm=.NULL.
   w_modifiche = .F.

  PROCEDURE INIT(bStyle)
     DoDefault(m.bStyle)
		 With this
			.caption='Framework monitor'
			*--- Save Btn
			m.oBtn = .AddButton("btnsave")
			m.oBtn.Caption = ''
			m.oBtn.cImage = "save.bmp"
			m.oBtn._OnClick = "cp_monitor_save(this.Parent.oForm)"
			m.oBtn.TooltipText = cp_Translate(MSG_SAVE)
			m.oBtn.Visible= .t.    
 			*--- Print Btn
			m.oBtn = .AddButton("btnprint")
			m.oBtn.Caption = ''
			m.oBtn.cImage = "print.bmp"
			m.oBtn._OnClick = "this.Parent.btnprint_Click()"
			m.oBtn.TooltipText = cp_Translate(MSG_PRINT)
			m.oBtn.Visible= .t. 
   		*--- Exit Btn
			m.oBtn = .AddButton("btnexit")
			m.oBtn.Caption = ''
			m.oBtn.cImage = "Esc.bmp"
			m.oBtn._OnClick = "this.Parent.btnprint_Click()"
			m.oBtn.TooltipText = cp_Translate(MSG_EXIT_1)
			m.oBtn.Visible= .t.     
   		*--- Cancel Btn
			m.oBtn = .AddButton("btncancel")
			m.oBtn.Caption = ''
			m.oBtn.cImage = "canc.bmp"
			m.oBtn._OnClick = "this.Parent.btncancel_Click()"
			m.oBtn.TooltipText = cp_Translate(MSG_CANCEL)
			m.oBtn.Visible= .t. 
   		*--- Export Btn
			m.oBtn = .AddButton("btnexp")
			m.oBtn.Caption = ''
			m.oBtn.cImage = "expo.bmp"
			m.oBtn._OnClick = "cp_exportfile(This.Parent.oForm)"
			m.oBtn.TooltipText = cp_Translate(MSG_CANCEL)
			m.oBtn.Visible= .t.

      .oForm=.NULL.
      .Dock(0)
     EndWith
   Endproc  
   
   PROCEDURE Start()
      This.oForm = CREATEOBJECT("tcp_monitor", this)
      This.Show()
   EndProc
   
   * Bottone di stampa
   PROCEDURE btnprint_Click()
      LOCAL ToPrint
      ToPrint=This.oForm.w_zoom.cCursor
      SELECT * FROM (ToPrint) ORDER BY FLCUSTOM INTO CURSOR __tmp__
      SELECT('__TMP__')
      cp_chprn("cp_monitor_rep.frx")
      USE IN SELECT('__TMP__')
   ENDPROC

   * Bottone di uscita
   PROCEDURE btnexit_Click()
      cp_uscita(This.oform)
      monitor_count = 0
      This.oForm.Release()
      This.oForm = .null.
      This.Release()
   ENDPROC
   
   * tasto escape
   PROC ecpQuit()
      This.btnexit_Click()
   ENDPROC
   
   FUNC HasCPEvents(i_cOp)
      RETURN(i_cOp='ecpQuit' OR i_cOp='ecpSecurity')

   * Bottone di cancellazione
   PROCEDURE btncancel_Click()
      cp_annulla(This.oForm)
      This.oForm.NotifyEvent("aggiornaZoom")
   ENDPROC
ENDDEFINE

* Men� con il tasto destro
PROCEDURE cp_MonitorRightClick(pParent)
   LOCAL cVis , record_count
   cVis = ""
   *---
   * controllo se c'� almeno un record nello zoom
   *---
   * metodo 1 tramiche una query
   *SELECT * FROM (pParent.w_zoom.ccursor) INTO CURSOR _record_count_
   *record_count = IIF(USED('_record_count_'),RECCOUNT(),0)
   *USE IN SELECT('_record_count_')
   *non visualizzo il menu contestuale se non c'� nessun record
   *IF record_count==0
   * metodo 2 controllo la variabile w_CURFLORGNAM
   * se � vuota significa che non c'� nulla nello zoom
   IF EMPTY(pParent.w_CURFLORGNAM) 
      RETURN 
   ENDIF 
   *---
   DEFINE POPUP popmonvis FROM MROW(),MCOL() SHORTCUT MARGIN
   DEFINE BAR 1 OF popmonvis PROMPT '\<Info'
   DEFINE BAR 2 OF popmonvis PROMPT '\<Open Obj.'
   DEFINE BAR 3 OF popmonvis PROMPT '\<Difference'
   DEFINE BAR 4 OF popmonvis PROMPT '\<TextFormat'
   ON SELECTION BAR 1 OF popmonvis cVis = 'I'
   ON SELECTION BAR 2 OF popmonvis cVis = 'O'
   ON SELECTION BAR 3 OF popmonvis cVis = 'D'
   ON SELECTION BAR 4 OF popmonvis cVis = 'T'
   ACTIVATE POPUP popmonvis
   DEACTIVATE POPUP popmonvis
   RELEASE POPUPS popmonvis
   * Apro la maschera Info
   DO CASE
      CASE cVis == 'I'
         DO cp_infoMon WITH pParent
   * Apro l'oggetto selezionato
      CASE cVis == 'O'
         cp_Open_Obj(.w_CURFLORGNAM, pParent)
   * Apro la maschera delle differenze
      CASE cVis == 'D'
         DO cp_differences WITH pParent
   * Apro la maschera del formato testo
      CASE cVis == 'T'
         DO cp_textfile WITH pParent
   ENDCASE
   cVis=''   
ENDPROC
* --- per evitare errori di compilazione
Procedure NoCoompileErr()
* --- Fine Area Manuale
return(createobject("tcp_monitor",oParentObject))

* --- Class definition
define class tcp_monitor as StdForm
  Top    = 2
  Left   = 3

  * --- Standard Properties
  Width  = 814
  Height = 459
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-07-21"
  HelpContextID=262783502
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "cp_monitor"
  cComment = "Framework Monitor"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CURFLORGNAM = space(254)
  o_CURFLORGNAM = space(254)
  w_CODUTE = 0
  w_FLDESUTE = space(20)
  w_CODGRP = 0
  w_FLDESGRP = space(20)
  w_FLCODAZI = space(10)
  w_FLDESAZI = space(40)
  w_FILETYPE = space(1)
  w_FLCUSTOM = space(1)
  w_SELECTALL = space(10)
  o_SELECTALL = space(10)
  w_MEMO = space(0)
  w_SELECTRECORD = .F.
  w_CURFILENAME = space(254)
  w_CURFILETYPE = space(4)
  w_CURFILEDESC = space(80)
  w_CURFLSTATUS = space(1)
  w_CURFLCODUTE = 0
  w_CURFLDESUTE = space(20)
  w_CURFLCODGRP = 0
  w_CURFLDESGRP = space(20)
  w_CURFLCODAZI = space(5)
  w_CURFLDESAZI = space(20)
  w_CURFLREVFIL = 0
  w_CURFLAUTHOR = space(10)
  w_CURFL_NOTES = space(0)
  w_CURFLCUSTOM = space(1)
  w_CURFLNEWFIL = space(1)
  w_OPEN_OBJ_VAR = .F.
  w_NUM_MODIFIED_RECORD = 0
  w_REFRESHZOOMVAR = .F.
  w_RET = space(10)
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_monitor
  Closable = .f.
  AutoCenter = .t.
  *--- Eseguo il refresh dello zoom all'activate per problema con zoom e tasto dx
  PROCEDURE Activate()
    IF this.w_REFRESHZOOMVAR
     this.notifyevent("Search")
     this.w_REFRESHZOOMVAR=.f.
    endif
    dodefault()
  endproc
  
  *--- Array per il salvataggio della tabella cp_monitor, da usare in caso di annullamento delle modifiche
  Dimension cp_monitor_backup(1)
  
  *--- Procedura di caricamento file all'interno dello zoom del monitor*
  PROCEDURE LoadFile()
     LOCAL l_azidir, l_numfilazi, l_filazi, l_i, l_k, l_numfile, l_usrgrp, l_nDirs, l_numdir, l_countDir, l_j
     LOCAL l_aFields, l_Filename
     
     *--- Controllo esistenza cartella azienda
     cp_Monitor_Check_DirAzi()
     
     *--- Definizione array delle cartelle utenti
     PUBLIC ARRAY i_usrDir(4,2)  &&1->Cartella , 2->Cartella attiva?
     i_usrDir[1,1] = i_CustomPath
     i_usrDir[1,2] = .T.
     i_usrDir[2,1] = i_UsrPath
     i_usrDir[2,2] = .T.
     i_usrDir[3,1] = i_UsrPath+"RightClick\"
     i_usrDir[3,2] = .T.
     i_usrDir[4,1] = i_UsrPath+"Deskmenu\"
     i_usrDir[4,2] = .T.
  
     l_countDir = ALEN(i_usrDir, 1) 
     
     *--- Riempio array con cartelle trovate
     l_nDirs = ADIR(l_aUsrDirs, i_UsrPath+"*.*", "D")   && leggo il contenuto di i_UsrPath
     FOR l_j=1 TO m.l_nDirs
        * Controllo il 5� campo di ogni file se � uguale a 'D', escluse le cartelle . e ..
        IF RIGHT(Upper(l_aUsrDirs[l_j,5]), 1)=="D" AND l_aUsrDirs[l_j,1]!='.' AND l_aUsrDirs[l_j,1]!='..'
           l_countDir = m.l_countDir + 1
           DIMENSION i_usrDir(m.l_countDir,2)
           i_usrDir[m.l_countDir,1] = ADDBS(i_UsrPath + l_aUsrDirs[l_j,1])
           i_usrDir[m.l_countDir,2] = .T.
        ENDIF
     ENDFOR
  
     *--- Imposto il flag erase a 'S' per tutti i record della tabella, tutti gli elementi contrasseganti con 'S' saranno cancellati,
     *--- faccio questo per sincronizzare la tabella con il contenuto della cartella, i file presenti precedentemente in tabella e non nella cartella saranno cancellati dal cursore
     UPDATE cp_monitor SET cp_monitor.FL_ERASE="S" 
  
     *--- Lettura file nelle cartelle
     USE IN SELECT('_checkExist_')
     FOR l_i=1 TO m.l_countDir
        IF(i_usrDir[l_i,2])   && se la cartella non � attiva non leggo i suoi files
           l_numfile = ADIR(l_aUsrFile, i_usrDir[l_i,1]+"*.*")
           FOR l_k=1 TO m.l_numfile
              *Controllo con una select se il file � gia in tabella
              *Se lo trovo non lo inserisco e modifico il flag di erase a "N"
              * Se non lo trovo eseguo la funzione Setcampi e lo inserisco
              l_Filename = UPPER(ALLTRIM(i_usrDir[l_i,1])+ALLTRIM(l_aUsrFile[l_k,1]))
              SELECT FLORGNAM FROM cp_monitor WHERE UPPER(ALLTRIM(FLORGNAM))==ALLTRIM(m.l_Filename) INTO CURSOR _checkExist_
              IF USED('_checkExist_') And RECCOUNT('_checkExist_')>0 && il file cercato � gi� presente in tabella
                 cp_Monitor_SetFields(m.l_Filename, @l_aFields)
                 UPDATE cp_monitor SET cp_monitor.FL_ERASE="N" , cp_monitor.FLNEWFIL=l_aFields[FLNEWFIL_IDX] ;
                    WHERE UPPER(ALLTRIM(FLORGNAM))==ALLTRIM(m.l_Filename)
              ELSE && il file non � presente in tabella quindi lo inserisco
                 IF cp_Monitor_SetFields(m.l_Filename, @l_aFields)
                    INSERT INTO cp_monitor FROM ARRAY l_aFields
                 ENDIF
              ENDIF
              USE IN SELECT('_checkExist_')
           ENDFOR
        ENDIF
     ENDFOR
  
     * Elimino i file dalla tabella che non sono pi� presenti sul disco (FL_ERASE='S')
     DELETE FROM cp_monitor WHERE FL_ERASE ='S'
     
     *--- Creo copia di backup per annullamento delle modifiche
     SELECT * FROM cp_monitor INTO ARRAY This.cp_monitor_backup
  ENDPROC
  
  *--- Procedura di aggiornamento zoom lanciata a seguito dell'evento 'RefreshZoom'
  PROC RefreshZoom(bZoomSetup)
     *----- Parametri della finestra del monitor
     *!*	   pParent.w_zoom.grd.scrollbars = 3
     *!*	   pParent.borderstyle = 1
     *!*	   pParent.w_zoom.nfields = 7
     *!*	   pParent.w_zoom.nColpositions=7
     With This
         *---
         * ZoomParam contiene i dati dei parametri dei campi dello zoom
         * ZoomParam[x,y]
         * x = uno dei campi della tabella(zoom)
         *
         * y=1: Nome Colonna
         * y=2: Larghezza colonna (width)
         * y=3: dynamicforecolor
         * y=4: dynamicbackcolor
         *---
         if bZoomSetup
           LOCAL l_Option
           l_Option = 4
           DIMENSION ZoomParam(FCOUNT('cp_monitor'), l_Option)
           LOCAL l_color_, l_backcolor_
           l_color_ = "IIF(FLNEWFIL=='S', RGB(0,160,0), RGB(0,0,200))"
           l_backcolor_ = "IIF(FLSTATUS=='E', RGB(255,255,255), RGB(210,210,210))"
  
           ZoomParam[FILENAME_IDX,1]= "File Name"
           ZoomParam[FILENAME_IDX,2]= 160
           ZoomParam[FILENAME_IDX,3]= l_color_
           ZoomParam[FILENAME_IDX,4]= l_backcolor_
  
           ZoomParam[FILEDESC_IDX,1]= "Description"
           ZoomParam[FILEDESC_IDX,2]= 150
           ZoomParam[FILEDESC_IDX,3]= l_color_
           ZoomParam[FILEDESC_IDX,4]= l_backcolor_
  
           ZoomParam[FLSTATUS_IDX,1]= "Status"
           ZoomParam[FLSTATUS_IDX,2]= 40
           ZoomParam[FLSTATUS_IDX,3]= l_color_
           ZoomParam[FLSTATUS_IDX,4]= l_backcolor_
  
           ZoomParam[FLCUSTOM_IDX,1]= "Folder"
           ZoomParam[FLCUSTOM_IDX,2]= 50
           ZoomParam[FLCUSTOM_IDX,3]= l_color_
           ZoomParam[FLCUSTOM_IDX,4]= l_backcolor_
  
           ZoomParam[FILETYPE_IDX,1]= "Type"
           ZoomParam[FILETYPE_IDX,2]= 40
           ZoomParam[FILETYPE_IDX,3]= l_color_
           ZoomParam[FILETYPE_IDX,4]= l_backcolor_
  
           ZoomParam[FLDESUTE_IDX,1]= "User"
           ZoomParam[FLDESUTE_IDX,2]= 90
           ZoomParam[FLDESUTE_IDX,3]= l_color_
           ZoomParam[FLDESUTE_IDX,4]= l_backcolor_
  
           ZoomParam[FLDESGRP_IDX,1]= "Group"
           ZoomParam[FLDESGRP_IDX,2]= 90
           ZoomParam[FLDESGRP_IDX,3]= l_color_
           ZoomParam[FLDESGRP_IDX,4]= l_backcolor_
  
           ZoomParam[FLDESAZI_IDX,1]= "Company"
           ZoomParam[FLDESAZI_IDX,2]= 90
           ZoomParam[FLDESAZI_IDX,3]= l_color_
           ZoomParam[FLDESAZI_IDX,4]= l_backcolor_
  
           ZoomParam[FLORGNAM_IDX,2]= 0
           ZoomParam[FLCODUTE_IDX,2]= 0
           ZoomParam[FLCODGRP_IDX,2]= 0
           ZoomParam[FLCODAZI_IDX,2]= 0
           ZoomParam[FLREFFIL_IDX,2]= 0
           ZoomParam[FL_ERASE_IDX,2]= 0
           ZoomParam[FLMODFIL_IDX,2]= 0
           *ZoomParam[FLCUSTOM_IDX,2]= 0
           ZoomParam[FLNEWFIL_IDX,2]= 0
           ZoomParam[FLREVFIL_IDX,2]= 0
           ZoomParam[FLAUTHOR_IDX,2]= 0
           ZoomParam[FL_NOTES_IDX,2]= 0
  
           *--- Setto propriet� delle colonne dello zoom
           LOCAL l_i
           FOR l_i = 1 TO FCOUNT('cp_monitor')
              IF !EMPTY(ZoomParam[m.l_i,1])
                 .w_zoom.cColTitle[m.l_i] = ZoomParam[m.l_i,1]
                 .w_zoom.GRD.COLUMNS[m.l_i].HDR.CAPTION = ZoomParam[m.l_i,1]
              ENDIF
              .w_zoom.nColWidth[m.l_i] = ZoomParam[m.l_i,2]
              *.w_zoom.grd.columns[m.l_i].resizable = .f.
              .w_zoom.GRD.COLUMNS[m.l_i].WIDTH = ZoomParam[m.l_i,2]
              IF !EMPTY(ZoomParam[m.l_i,3])
                 .w_zoom.cForecolor[m.l_i] = ZoomParam[m.l_i,3]
                 .w_zoom.GRD.COLUMNS[m.l_i].DYNAMICFORECOLOR = ZoomParam[m.l_i,3]
              ENDIF
              IF !EMPTY(ZoomParam[m.l_i,4])
                 .w_zoom.cbackcolor[m.l_i] = ZoomParam[m.l_i,4]
                 .w_zoom.GRD.COLUMNS[m.l_i].DYNAMICBACKCOLOR = ZoomParam[m.l_i,4]
              ENDIF
           ENDFOR
           
           *--- Nome zoom
           .w_Zoom.cZoomName = 'DEFAULT'
         Endif  &&bZoomSetup
         *--- Se sono state fatte delle modifiche appare uno * accanto al nome della maschera
         IF cp_Monitor_modified_records()>0
            IF RIGHT(ALLTRIM(.CAPTION),1)!="*"
               .CAPTION=ALLTRIM(.CAPTION)+"*"
            ENDIF
         ENDIF
  
         *--- Filtri
         .w_zoom.cwhere[1] = ALLTRIM("FL_ERASE='N'") && flag di erase
  
         * Filtro codice utente
         IF .w_CODUTE<>0
            .w_zoom.cwhere[1]= .w_zoom.cwhere[1] + ALLTRIM("AND FLCODUTE="+STR(.w_CODUTE))
         ENDIF
  
         * Filtro codice gruppo
         IF .w_CODGRP<>0
            .w_zoom.cwhere[1]=.w_zoom.cwhere[1] + ALLTRIM("AND FLCODGRP="+STR(.w_CODGRP))
         ENDIF
  
         * Filtro codice azienda
         IF !EMPTY(.w_FLCODAZI)
            .w_zoom.cwhere[1]=.w_zoom.cwhere[1] + ALLTRIM("AND FLCODAZI='"+.w_FLCODAZI+"'")
         ENDIF
  
         * Filtro estensione
         IF !EMPTY(.w_FILETYPE)
            .w_zoom.cwhere[1]=.w_zoom.cwhere[1] + ALLTRIM("AND FILETYPE='"+.w_FILETYPE+"'")
         ENDIF
  
         * Filtro custom
         IF .w_FLCUSTOM != 'A'
            .w_zoom.cwhere[1]=.w_zoom.cwhere[1] + ALLTRIM("AND FLCUSTOM='"+.w_FLCUSTOM+"'")
         ENDIF
         
         .w_zoom.nwhere = 1
         IF !.w_REFRESHZOOMVAR
            .NotifyEvent("Search")
         ENDIF
         .w_zoom.REFRESH()
     EndWith
  ENDPROC
  
  *--- Seleziona/Deseleziona record zoom
  PROC CheckUncheck()
     LOCAL l_cursor, l_OldArea, l_NumRec
     l_NumRec = 0
     l_OldArea = SELECT()
     With This
       l_cursor = .w_zoom.cCursor
       UPDATE (l_cursor) SET xchk=IIF(This.w_SELECTALL = 'S', 1, 0)
       .w_SELECTRECORD = This.w_SELECTALL = 'S'
       .w_zoom.REFRESH()
     EndWith
     SELECT(l_OldArea)
  ENDPROC
  
  *---Funzioni per bottoni monitor
  * Bottone activate/disactivate
  PROCEDURE EnableDisable()
     LOCAL l_OldArea, l_Key, l_table_, FLORGNAM_new, PATH_new, l_RepName, l_rowcounts
     l_OldArea = SELECT()
     l_table_ = This.w_zoom.cCursor
     SELECT * FROM (l_table_) WHERE xchk=1 INTO CURSOR _cursore_
     l_rowcounts=IIF(USED('_cursore_'),RECCOUNT(),0)
     if l_rowcounts==0
        cp_errormsg("Nessun file selezionato",'x')
        USE IN SELECT('_cursore_')
        return
     endif 
     SELECT('_cursore_')
     GO TOP
     SCAN
        FLORGNAM_new = ""
        PATH_new = ""
        l_Key = _cursore_.FLORGNAM
        *--- Modifico il nome del file, lo stato, il  flag modificato e il reffil
        IF _cursore_.FLSTATUS=="E" && devo aggiungere la @
           PATH_new = ALLTRIM(JUSTPATH(l_Key)+"\@")
           FLORGNAM_new = ALLTRIM(PATH_new+ALLTRIM(JUSTFNAME(l_Key)))
        ELSE && devo levare la @
           PATH_new = ALLTRIM(JUSTPATH(l_Key)+"\")
           FLORGNAM_new = ALLTRIM(PATH_new+ALLTRIM(SUBSTR(JUSTFNAME(l_Key),2)))
        ENDIF
        *--- Controllo esistenza record con lo stesso nuovo nome
        Select('cp_Monitor')
        l_RepName = 0
        COUNT FOR UPPER(ALLTRIM(FLORGNAM)) = UPPER(ALLTRIM(FLORGNAM_new)) TO l_RepName
        SELECT('_cursore_')
        *--- Se ho i diritti e non ci sono nomi ripetuti
        IF l_RepName=0 AND (cp_Monitor_UserRight()>3 OR (cp_Monitor_UserRight()==3 AND _cursore_.FLCODUTE==i_codute))
           UPDATE cp_monitor ;
              SET FLSTATUS=IIF(FLSTATUS=="E","D","E"), FLMODFIL="M", FLORGNAM = FLORGNAM_new ;
              WHERE FLORGNAM=l_Key
        ELSE && c'� un nome ripetuto
           cp_errormsg("File: "+ALLTRIM(l_Key)+CHR(13)+"Abilitazione o disabilitazione non consentita"+CHR(13)+"File destinazione gi� esistente o diritti insufficienti",'x')
        ENDIF
        SELECT('_cursore_')
     ENDSCAN
     USE IN SELECT('_cursore_')
     *--- Restore Area
     SELECT(l_OldArea)
  ENDPROC
  
  *--- Bottone Erase
  PROCEDURE LogicErase()
     LOCAL l_OldArea, l_key, l_table_ , l_rowcounts
     l_OldArea = SELECT()
     l_table_ = This.w_zoom.cCursor
     SELECT * FROM (l_table_) WHERE xchk=1 INTO CURSOR _cursore_
     l_rowcounts=IIF(USED('_cursore_'),RECCOUNT(),0)
     if l_rowcounts=0
        cp_errormsg("Nessun file selezionato",'x')
        USE IN SELECT('_cursore_')
        return
     endif 
     IF l_rowcounts>0 AND cp_yesno(MSG_CONFIRM_DELETING_QP)
        SELECT('_cursore_')
        GO TOP
        SCAN
           l_key = _cursore_.FLORGNAM
           *--- Controllo diritti
           IF (cp_Monitor_UserRight()>3 OR (cp_Monitor_UserRight()==3 AND _cursore_.FLCODUTE==i_codute))
              UPDATE cp_monitor ;
                 SET FL_ERASE="S" ;
                 WHERE FLORGNAM=l_key
              SELECT('_cursore_')
           ELSE
              cp_errormsg("File: "+ALLTRIM(l_key)+CHR(13)+"Eliminazione non consentita"+CHR(13)+"Diritti insufficienti","x")
           ENDIF 
        ENDSCAN
     ENDIF
     USE IN SELECT('_cursore_')
     *--- Restore Area
     SELECT(l_OldArea)   
  ENDPROC
  
  *--- Bottone Open Obj.
  FUNCTION OpenObj(pFileName)
     Local l_OldArea
     l_OldArea = Select()
     
     PUBLIC i_designfile
     LOCAL extens, old_bLoadRuntimeConfig, mask, obj_array
     IF !cp_fileexist(m.pFileName,.T.)
        IF cp_yesno("Occorre salvare per aprire il file"+CHR(13)+"Salvare?")
           This.Save()
        ENDIF
     ENDIF
  
     * Controllo se esiste il file
     IF cp_fileexist(m.pFileName,.T.)
        *--- Assegno a i_designfile il nome del file completo di estensione
        i_designfile = ALLTRIM(m.pFileName)
  
        extens= UPPER(RIGHT(JUSTEXT(m.pFileName),4))
        *--- Controllo se devo aprire un disegnatore
        DO CASE
           CASE extens=='VQR' && query
              DO vq_build
           CASE extens=='VFM' && maschera
              DO vm_build
           CASE extens=='VZM' && zoom
              DO vz_build
           CASE extens=='_VZM' && zoom
              RELEASE i_designfile
              DO vz_build
              i_curform.oFORM.ZOOM.askfile(m.pFileName)
           CASE extens=='VMN' && men�
              DO vu_build
           CASE extens=='FRX' && report
              cp_ModifyReport(m.pFileName)
           CASE extens=='VRT' && run-time
              old_bLoadRuntimeConfig = bLoadRuntimeConfig
              bLoadRuntimeConfig =.F.
              IF AT('vrt_build ',LOWER(SET('proc')))=0 OR AT('vm_li',LOWER(SET('proc')))=0 OR AT('cp_draw',LOWER(SET('proc')))=0 ;
                    OR AT('vq_lib',LOWER(SET('proc')))=0
                 SET PROC TO vrt_build,vm_lib,cp_draw,vq_lib ADDITIVE
              ENDIF
              LOCAL oo, i_rt, i_class
              oo=cp_monitor_vrtPrg(cp_Monitor_GetFilename(m.pFileName))
              IF !ISNULL(oo)
                i_class=vrtClass(oo)
  			        if inlist(i_class,"Stdform","Stdpcform","Stdtrsform")
  			           local i_oForm
  			           do case
  			             case inlist(i_class,"Stdform","Stdtrsform")
  			               i_oForm=oo 
  			             case i_class=="Stdpcform"
  			               i_oForm=oo.cnt 
  			           endcase
                  i_rt=CREATEOBJECT('RT_toolbar',i_oForm,oo,SUBSTR(oo.CLASS,2))
                ELSE
                  i_rt=CREATEOBJECT('RT_toolbar',oo,oo,SUBSTR(oo.CLASS,3))
                ENDIF
                i_rt.cFileName = ALLTRIM(i_designfile)
                i_rt.SetTitle()
                i_rt.LoadDoc()
                i_rt.EditMask()
              ELSE
                 cp_errormsg("Occorre aprire la gestione")
              ENDIF
              bLoadRuntimeConfig = old_bLoadRuntimeConfig
           CASE extens=='XDC' && aggiunta campi
              DO cp_monitoraddfield
           OTHERWISE
              cp_errormsg("Nessun disegnatore disponibile",'!')
        ENDCASE
     ENDIF  &&cp_fileexist(m.pFileName,.T.)
  
     IF VARTYPE(i_designfile)='C'
        RELEASE i_designfile
     ENDIF
     
      *--- Restore Area
     Select(l_OldArea)  
     RETURN .T.
  ENDPROC
  
  *--- Funzione di salvataggio dei files presenti in tabella cp_monitor su disco
  PROCEDURE Save()
     LOCAL l_OldArea, _savecur_, error_msg, l_errsav
     l_OldArea = Select()
     *--- Gestione errori
     l_errsav=on('ERROR')
     error_msg = ""
     SELECT * FROM cp_monitor INTO CURSOR _savecur_
     SELECT('_savecur_')
     GO TOP
     SCAN && per ogni record della tabella
        DO CASE
           CASE ALLTRIM(UPPER(_savecur_.FLMODFIL))=="N"
              * Nessuna modifica da salvare su disco
           CASE ALLTRIM(UPPER(_savecur_.FLMODFIL))=="M"
              * Occorre rinominare il file
              ON ERROR error_msg= ALLTRIM(error_msg + cp_Translate("Errore rinominazione file ") + ALLTRIM(_savecur_.FLREFFIL) + CHR(10)+CHR(13))
              * Verifico l'esistenza della cartella di destinazione
              cp_makedir(ALLTRIM(JUSTPATH(_savecur_.FLORGNAM)))
              RENAME (_savecur_.FLREFFIL) TO (_savecur_.FLORGNAM)
              *--- Se FRX devo rinominare anche FRT
              If UPPER(ALLTRIM(_savecur_.FILETYPE)) == 'FRX'
                RENAME FORCEEXT(_savecur_.FLREFFIL, 'FRT') TO FORCEEXT(_savecur_.FLORGNAM, 'FRT')
              Endif
              UPDATE cp_monitor ;
                 SET FLREFFIL=(_savecur_.FLORGNAM) , FL_ERASE="N" , FLMODFIL="N" ;
                 WHERE FLORGNAM=(_savecur_.FLORGNAM)
           CASE ALLTRIM(UPPER(_savecur_.FLMODFIL))=="D"
              * Occorre duplicare il file
              ON ERROR error_msg= ALLTRIM(error_msg + cp_Translate("Errore duplicazione file ") + ALLTRIM(_savecur_.FLREFFIL) + CHR(10)+CHR(13))
              * Se il file orig esiste e non esiste il file dest lo copio
              * Controllo esistenza cartella destinazione
              cp_makedir(ALLTRIM(JUSTPATH(_savecur_.FLORGNAM)))
              COPY FILE (_savecur_.FLREFFIL) TO (_savecur_.FLORGNAM)
              *--- Se FRX devo rinominare anche FRT
              If UPPER(ALLTRIM(_savecur_.FILETYPE)) == 'FRX'
                COPY FILE  FORCEEXT(_savecur_.FLREFFIL, 'FRT') TO FORCEEXT(_savecur_.FLORGNAM, 'FRT')
              Endif               
              * Imposto alcuni flag a valori di default
              UPDATE cp_monitor ;
                 SET FLREFFIL=(_savecur_.FLORGNAM) , FL_ERASE="N" , FLMODFIL="N" ;
                 WHERE FLORGNAM=(_savecur_.FLORGNAM)
        ENDCASE
     ENDSCAN
     GO TOP
     SCAN
        IF ALLTRIM(UPPER(_savecur_.FL_ERASE))=="S" && se lo devo cancellare
           * Cancellazione
           ON ERROR error_msg= ALLTRIM(error_msg + cp_Translate("Errore cancellazione file ") + ALLTRIM(_savecur_.FLORGNAM) + CHR(10)+CHR(13))
           DELETE FILE (_savecur_.FLORGNAM)
           IF ALLTRIM(UPPER(_savecur_.FILETYPE))=="FRX"
              DELETE FILE FORCEEXT(_savecur_.FLORGNAM, 'FRT')
           ENDIF
           * Elimino il record anche dalla tabella
           DELETE FROM cp_monitor WHERE FL_ERASE ='S' AND FLORGNAM=(_savecur_.FLORGNAM)
        ENDIF
     ENDSCAN
     USE IN SELECT('_savecur_')
  
     *--- Controllo errori
     If !Empty(m.error_msg)
       *--- Ho degli errori
       cp_ErrorMsg(m.error_msg)
     Endif
     
     * Elimino l'eventuale asterisco in fondo al tirolo del monitor
     IF RIGHT(ALLTRIM(This.CAPTION),1)=="*"
        This.CAPTION=SUBSTR(This.CAPTION,1,LEN(This.CAPTION)-1)
     ENDIF
  
     * Aggiorno la copia di backup
     SELECT * FROM cp_monitor INTO ARRAY This.cp_monitor_backup
     
     * azzero il contatore di record modificati
     This.w_NUM_MODIFIED_RECORD = 0
     *--- Restore error
     on error &l_errsav
     *--- Restore Area
     Select(l_OldArea)
  ENDPROC
  
  *--- Ricarica la tabella cp_monitor
  PROCEDURE Reload()
     Local l_OldArea
     l_OldArea = Select()
     With This
        IF .w_NUM_MODIFIED_RECORD>0 AND cp_yesno("Salvare le modifiche apportate prima di ricaricare?")
          .Save()
        ENDIF 
        IF .w_NUM_MODIFIED_RECORD!=0
          .Cancel(.T.)
        ENDIF
        *--- Ricarico dal disco
        .LoadFile()
        *--- Eseguo Setup dello zoom
        .RefreshZoom(.T.)
     EndWith
     *--- Restore area
     Select(m.l_OldArea) 
  ENDPROC
  
  *--- Procedura che annulla tutte le modifiche fatte durante l'ultima sessione del monitor
  PROCEDURE Cancel(bForce)
     Local l_OldArea
     l_OldArea = Select()
     With This
       IF m.bForce Or (cp_monitor_Modified_records()>0 AND cp_yesno("Annullare modifiche?"))
          DELETE FROM cp_monitor
          INSERT INTO cp_monitor FROM ARRAY .cp_monitor_backup
          * Elimino l'eventuale asterisco in fondo al titolo del monitor
          IF RIGHT(ALLTRIM(.CAPTION),1)=="*"
             .CAPTION=SUBSTR(.CAPTION,1,LEN(.CAPTION)-1)
          ENDIF
       ENDIF
     EndWith
     *--- Restore area
     Select(m.l_OldArea)   
  ENDPROC
  
  *--- Procedura di uscita
  *--- Se riscontra delle modifiche le salva ed esce, altrimenti esce soltanto
  PROCEDURE Exit()
     Local l_OldArea
     l_OldArea = Select()
     With This
       IF cp_monitor_Modified_records()>0
          IF cp_yesno("Salvare le modifiche effettuate?")&& se � stato modificato qualcosa
             * Eseguo il salvataggio
             .Save()
          ELSE
             .Cancel(.t.)
          ENDIF
       ENDIF
     EndWith
     *--- Restore area
     Select(m.l_OldArea) 
     This.EcpQuit()
  ENDPROC
  
  *--- Procedura di esportazione files selezionati
  PROCEDURE ExportFile()
     Local l_OldArea
     l_OldArea = Select()
     LOCAL _table_ , rowcounts , rowcounts_itr, l_DestPath, l_BadPath, old_time_set, l_notsavefile
     With This
       *--- Recupero le chiavi dei record selezionati (da esportare)
       IF cp_monitor_SelectRec(.w_zoom.cCursor)
         *--- Select per contare quanti file sono stati modificati fra quelli selezionati
        SELECT * FROM (.w_zoom.cCursor) WHERE FLMODFIL!="N" AND xchk=1 INTO CURSOR _export_cursor_
        l_notsavefile = IIF(USED('_export_cursor_'),RECCOUNT(),-1)
        USE IN SELECT('_export_cursor_')
        * Se ci sono dei file modificati, e si vogliono salvare, li salvo
        IF m.l_notsavefile>0 AND cp_yesno("Alcuni file non sono stati salvati."+CHR(13)+"Salvarli?")
           .Save()
        ENDIF
        l_BadPath = .t.
        DO WHILE l_BadPath
           * Scelta da parte dell'utente del percorso di salvataggio
           l_DestPath = ADDBS(cp_GetDir())
           * Se cliccato su annulla esco
           IF EMPTY(m.l_DestPath)
              *--- Restore area
              Select(m.l_OldArea)
              RETURN
           ELSE
              IF at(SYS(5)+SYS(2003), m.l_DestPath)!=0
                 cp_errormsg("Percorso di destinazione non valido!","!")
              ELSE
                 l_BadPath = .f.
              ENDIF 
           ENDIF
        ENDDO 
  
        *--- Creo una cartella con nome la data e l'ora attuale
        *m.l_DestPath = ADDBS(ALLTRIM(m.l_DestPath))+ADDBS(ALLTRIM(TTOC(DATETIME(),1)))
        SELECT * FROM (.w_zoom.cCursor) WHERE xchk=1 INTO CURSOR _export_cursor_
        SELECT('_export_cursor_')
        GO TOP
        SCAN
           IF cp_fileexist(_export_cursor_.florgnam,.T.)
              cp_makedir(ALLTRIM(l_DestPath+SYS(2014,(JUSTPATH(_export_cursor_.florgnam)))))
              COPY FILE ALLTRIM(_export_cursor_.florgnam) TO ALLTRIM(l_DestPath+SYS(2014,(_export_cursor_.florgnam)))
              IF ALLTRIM(UPPER(_export_cursor_.FILETYPE))=="FRX"
                COPY FILE ALLTRIM(FORCEEXT(_export_cursor_.florgnam, 'FRT')) TO ALLTRIM(FORCEEXT(l_DestPath+SYS(2014,_export_cursor_.florgnam), 'FRT'))
              ENDIF            
           ENDIF
        ENDSCAN
        USE IN SELECT('_export_cursor_')
        cp_errormsg("File Esportati nella cartella:"+CHR(13)+ALLTRIM(l_DestPath),'i')
       ELSE
        cp_errormsg("Nessun file selezionato",'!')
       ENDIF
     EndWith
     *--- Restore area
     Select(m.l_OldArea)
  ENDPROC
  
  *--- Bottone di stampa
  PROCEDURE Print()
     LOCAL ToPrint
     ToPrint=This.w_zoom.cCursor
     SELECT * FROM (ToPrint) ORDER BY FLCUSTOM INTO CURSOR __tmp__
     SELECT('__TMP__')
     cp_chprn("cp_monitor_rep.frx")
     USE IN SELECT('__TMP__')
  ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_monitorPag1","cp_monitor",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODUTE_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_monitor
    this.Visible=.f.
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CPGROUPS'
    this.cWorkTables[3]='AZIENDA'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CURFLORGNAM=space(254)
      .w_CODUTE=0
      .w_FLDESUTE=space(20)
      .w_CODGRP=0
      .w_FLDESGRP=space(20)
      .w_FLCODAZI=space(10)
      .w_FLDESAZI=space(40)
      .w_FILETYPE=space(1)
      .w_FLCUSTOM=space(1)
      .w_SELECTALL=space(10)
      .w_MEMO=space(0)
      .w_SELECTRECORD=.f.
      .w_CURFILENAME=space(254)
      .w_CURFILETYPE=space(4)
      .w_CURFILEDESC=space(80)
      .w_CURFLSTATUS=space(1)
      .w_CURFLCODUTE=0
      .w_CURFLDESUTE=space(20)
      .w_CURFLCODGRP=0
      .w_CURFLDESGRP=space(20)
      .w_CURFLCODAZI=space(5)
      .w_CURFLDESAZI=space(20)
      .w_CURFLREVFIL=0
      .w_CURFLAUTHOR=space(10)
      .w_CURFL_NOTES=space(0)
      .w_CURFLCUSTOM=space(1)
      .w_CURFLNEWFIL=space(1)
      .w_OPEN_OBJ_VAR=.f.
      .w_NUM_MODIFIED_RECORD=0
      .w_REFRESHZOOMVAR=.f.
      .w_RET=space(10)
      .oPgFrm.Page1.oPag.Zoom.Calculate()
        .w_CURFLORGNAM = .w_Zoom.GetVar("FLORGNAM")
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODUTE))
          .link_1_5('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_CODGRP))
          .link_1_8('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_FLCODAZI))
          .link_1_11('Full')
        endif
          .DoRTCalc(7,8,.f.)
        .w_FLCUSTOM = "U"
          .DoRTCalc(10,10,.f.)
        .w_MEMO = .w_ZOOM.GetVar("FL_NOTES")
          .DoRTCalc(12,12,.f.)
        .w_CURFILENAME = .w_Zoom.GetVar("FILENAME")
        .w_CURFILETYPE = .w_Zoom.GetVar("FILETYPE")
        .w_CURFILEDESC = .w_Zoom.GetVar("FILEDESC")
        .w_CURFLSTATUS = .w_Zoom.GetVar("FLSTATUS")
        .w_CURFLCODUTE = .w_ZOOM.GetVar("FLCODUTE")
        .w_CURFLDESUTE = .w_Zoom.GetVar("FLDESUTE")
        .w_CURFLCODGRP = .w_Zoom.GetVar("FLCODGRP")
        .w_CURFLDESGRP = .w_Zoom.GetVar("FLDESGRP")
        .w_CURFLCODAZI = .w_Zoom.GetVar("FLCODAZI")
        .w_CURFLDESAZI = .w_Zoom.GetVar("FLDESAZI")
        .w_CURFLREVFIL = .w_Zoom.GetVar("FLREVFIL")
        .w_CURFLAUTHOR = .w_Zoom.GetVar("FLAUTHOR")
        .w_CURFL_NOTES = .w_Zoom.GetVar("FL_NOTES")
        .w_CURFLCUSTOM = .w_Zoom.GetVar("FLCUSTOM")
        .w_CURFLNEWFIL = .w_Zoom.GetVar("FLNEWFIL")
    endwith
    this.DoRTCalc(28,31,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_monitor
    this.oPgFrm.visible=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
            .w_CURFLORGNAM = .w_Zoom.GetVar("FLORGNAM")
        .DoRTCalc(2,10,.t.)
        if .o_CURFLORGNAM<>.w_CURFLORGNAM
            .w_MEMO = .w_ZOOM.GetVar("FL_NOTES")
        endif
        if .o_SELECTALL<>.w_SELECTALL
          .Calculate_RPPVJLPQBD()
        endif
        .DoRTCalc(12,12,.t.)
            .w_CURFILENAME = .w_Zoom.GetVar("FILENAME")
            .w_CURFILETYPE = .w_Zoom.GetVar("FILETYPE")
            .w_CURFILEDESC = .w_Zoom.GetVar("FILEDESC")
            .w_CURFLSTATUS = .w_Zoom.GetVar("FLSTATUS")
            .w_CURFLCODUTE = .w_ZOOM.GetVar("FLCODUTE")
            .w_CURFLDESUTE = .w_Zoom.GetVar("FLDESUTE")
            .w_CURFLCODGRP = .w_Zoom.GetVar("FLCODGRP")
            .w_CURFLDESGRP = .w_Zoom.GetVar("FLDESGRP")
            .w_CURFLCODAZI = .w_Zoom.GetVar("FLCODAZI")
            .w_CURFLDESAZI = .w_Zoom.GetVar("FLDESAZI")
            .w_CURFLREVFIL = .w_Zoom.GetVar("FLREVFIL")
            .w_CURFLAUTHOR = .w_Zoom.GetVar("FLAUTHOR")
            .w_CURFL_NOTES = .w_Zoom.GetVar("FL_NOTES")
            .w_CURFLCUSTOM = .w_Zoom.GetVar("FLCUSTOM")
            .w_CURFLNEWFIL = .w_Zoom.GetVar("FLNEWFIL")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
    endwith
  return

  proc Calculate_AHXJGITLMA()
    with this
          * --- LoadFile/Zoom Setup
          .w_RET = .LoadFile()
          .w_RET = .RefreshZoom(.T.)
    endwith
  endproc
  proc Calculate_AHNAGSWYRV()
    with this
          * --- Refresh zoom
          .w_SELECTRECORD = .F.
          .w_RET = .RefreshZoom()
    endwith
  endproc
  proc Calculate_RPPVJLPQBD()
    with this
          * --- Check/Uncheck
          .w_RET = .CheckUncheck()
    endwith
  endproc
  proc Calculate_XWXMMPTIVC()
    with this
          * --- Enable/Disable
          .w_RET = .EnableDisable()
          .w_RET = .RefreshZoom()
    endwith
  endproc
  proc Calculate_RBZJWJDDCD()
    with this
          * --- Erase
          .w_RET = .LogicErase()
          .w_RET = .RefreshZoom()
    endwith
  endproc
  proc Calculate_OXEHGNFTVI()
    with this
          * --- Open on DblClick
          .w_RET = .OpenObj(.w_CURFLORGNAM)
    endwith
  endproc
  proc Calculate_HZARFYTBQY()
    with this
          * --- Reload
          .w_num_modified_record = cp_Monitor_modified_records()
          .w_RET = .Reload()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODUTE_1_5.enabled = this.oPgFrm.Page1.oPag.oCODUTE_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODGRP_1_8.enabled = this.oPgFrm.Page1.oPag.oCODGRP_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_AHXJGITLMA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("RefreshZoom")
          .Calculate_AHNAGSWYRV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Enable_Disable")
          .Calculate_XWXMMPTIVC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Erase")
          .Calculate_RBZJWJDDCD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Zoom selected")
          .Calculate_OXEHGNFTVI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("reload")
          .Calculate_HZARFYTBQY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODUTE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCODUTE_1_5'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.CODE,0)
      this.w_FLDESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_FLDESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRP
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODGRP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODGRP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRP) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oCODGRP_1_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODGRP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRP = NVL(_Link_.CODE,0)
      this.w_FLDESGRP = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRP = 0
      endif
      this.w_FLDESGRP = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FLCODAZI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FLCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_FLCODAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_FLCODAZI))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FLCODAZI)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FLCODAZI) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oFLCODAZI_1_11'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FLCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_FLCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_FLCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FLCODAZI = NVL(_Link_.AZCODAZI,space(10))
      this.w_FLDESAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FLCODAZI = space(10)
      endif
      this.w_FLDESAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FLCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_5.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_5.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESUTE_1_6.value==this.w_FLDESUTE)
      this.oPgFrm.Page1.oPag.oFLDESUTE_1_6.value=this.w_FLDESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRP_1_8.value==this.w_CODGRP)
      this.oPgFrm.Page1.oPag.oCODGRP_1_8.value=this.w_CODGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESGRP_1_9.value==this.w_FLDESGRP)
      this.oPgFrm.Page1.oPag.oFLDESGRP_1_9.value=this.w_FLDESGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCODAZI_1_11.value==this.w_FLCODAZI)
      this.oPgFrm.Page1.oPag.oFLCODAZI_1_11.value=this.w_FLCODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESAZI_1_12.value==this.w_FLDESAZI)
      this.oPgFrm.Page1.oPag.oFLDESAZI_1_12.value=this.w_FLDESAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oFILETYPE_1_14.RadioValue()==this.w_FILETYPE)
      this.oPgFrm.Page1.oPag.oFILETYPE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCUSTOM_1_16.RadioValue()==this.w_FLCUSTOM)
      this.oPgFrm.Page1.oPag.oFLCUSTOM_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECTALL_1_22.RadioValue()==this.w_SELECTALL)
      this.oPgFrm.Page1.oPag.oSELECTALL_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMEMO_1_26.value==this.w_MEMO)
      this.oPgFrm.Page1.oPag.oMEMO_1_26.value=this.w_MEMO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CURFLORGNAM = this.w_CURFLORGNAM
    this.o_SELECTALL = this.w_SELECTALL
    return

enddefine

* --- Define pages as container
define class tcp_monitorPag1 as StdContainer
  Width  = 810
  height = 459
  stdWidth  = 810
  stdheight = 459
  resizeXpos=276
  resizeYpos=262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Zoom as cp_szoombox with uid="IANLWBZAXC",left=13, top=75, width=784,height=242,;
    caption='MonitorZoom',;
   bGlobalFont=.t.,;
    cTable="cp_monitor",cZoomFile="",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bNoRightClick=.t.,;
    cEvent = "Search",;
    nPag=1;
    , HelpContextID = 133217921

  add object oCODUTE_1_5 as StdField with uid="IIJYJPNYOV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "User code",;
    HelpContextID = 166317367,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=64, Top=10, cSayPict='"99"', cGetPict='"99"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODGRP==0)
    endwith
   endif
  endfunc

  func oCODUTE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCODUTE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oFLDESUTE_1_6 as StdField with uid="JVAZTKZPOK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FLDESUTE", cQueryName = "FLDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 166152071,;
   bGlobalFont=.t.,;
    Height=21, Width=157, Left=127, Top=10, InputMask=replicate('X',20)

  add object oCODGRP_1_8 as StdField with uid="UHNOZGVPQV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODGRP", cQueryName = "CODGRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Group code",;
    HelpContextID = 199871810,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=64, Top=38, cSayPict='"99"', cGetPict='"99"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_CODGRP"

  func oCODGRP_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODUTE==0)
    endwith
   endif
  endfunc

  func oCODGRP_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRP_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRP_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oCODGRP_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oFLDESGRP_1_9 as StdField with uid="OTRKEFSLDU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FLDESGRP", cQueryName = "FLDESGRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 166154841,;
   bGlobalFont=.t.,;
    Height=21, Width=157, Left=127, Top=38, InputMask=replicate('X',20)

  add object oFLCODAZI_1_11 as StdField with uid="UCWKKGIWDN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FLCODAZI", cQueryName = "FLCODAZI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Company code",;
    HelpContextID = 64441299,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=349, Top=10, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_FLCODAZI"

  func oFLCODAZI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oFLCODAZI_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFLCODAZI_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oFLCODAZI_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oFLDESAZI_1_12 as StdField with uid="MFOORKQOHG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FLDESAZI", cQueryName = "FLDESAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 166153171,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=405, Top=10, InputMask=replicate('X',40)


  add object oFILETYPE_1_14 as StdCombo with uid="CRPNSGRUUP",value=1,rtseq=8,rtrep=.f.,left=349,top=38,width=143,height=21;
    , ToolTipText = "File type";
    , HelpContextID = 174344011;
    , cFormVar="w_FILETYPE",RowSource=""+"ALL,"+"Add Fields,"+"Graphics,"+"Module Excel,"+"Module Word,"+"OpenOffice Calc,"+"OpenOffice Writer,"+"Report,"+"Visual Mask,"+"Visual Men�,"+"Visual Query,"+"Visual Run-Time,"+"Visual Zoom,"+"Visual Zoom  _vzm", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFILETYPE_1_14.RadioValue()
    return(iif(this.value =1,'',;
    iif(this.value =2,'XDC',;
    iif(this.value =3,'VGR',;
    iif(this.value =4,'XLT',;
    iif(this.value =5,'DOC',;
    iif(this.value =6,'STC',;
    iif(this.value =7,'SXW',;
    iif(this.value =8,'FRX',;
    iif(this.value =9,'VFM',;
    iif(this.value =10,'VMN',;
    iif(this.value =11,'VQR',;
    iif(this.value =12,'VRT',;
    iif(this.value =13,'VZM',;
    iif(this.value =14,'_VZM',;
    space(1))))))))))))))))
  endfunc
  func oFILETYPE_1_14.GetRadio()
    this.Parent.oContained.w_FILETYPE = this.RadioValue()
    return .t.
  endfunc

  func oFILETYPE_1_14.SetRadio()
    this.Parent.oContained.w_FILETYPE=trim(this.Parent.oContained.w_FILETYPE)
    this.value = ;
      iif(this.Parent.oContained.w_FILETYPE=='',1,;
      iif(this.Parent.oContained.w_FILETYPE=='XDC',2,;
      iif(this.Parent.oContained.w_FILETYPE=='VGR',3,;
      iif(this.Parent.oContained.w_FILETYPE=='XLT',4,;
      iif(this.Parent.oContained.w_FILETYPE=='DOC',5,;
      iif(this.Parent.oContained.w_FILETYPE=='STC',6,;
      iif(this.Parent.oContained.w_FILETYPE=='SXW',7,;
      iif(this.Parent.oContained.w_FILETYPE=='FRX',8,;
      iif(this.Parent.oContained.w_FILETYPE=='VFM',9,;
      iif(this.Parent.oContained.w_FILETYPE=='VMN',10,;
      iif(this.Parent.oContained.w_FILETYPE=='VQR',11,;
      iif(this.Parent.oContained.w_FILETYPE=='VRT',12,;
      iif(this.Parent.oContained.w_FILETYPE=='VZM',13,;
      iif(this.Parent.oContained.w_FILETYPE=='_VZM',14,;
      0))))))))))))))
  endfunc


  add object oFLCUSTOM_1_16 as StdCombo with uid="EFALAKMMFW",rtseq=9,rtrep=.f.,left=570,top=38,width=118,height=21;
    , ToolTipText = "Folder filter";
    , HelpContextID = 165105462;
    , cFormVar="w_FLCUSTOM",RowSource=""+"Userdir,"+"Custom,"+"Custom+Userdir", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLCUSTOM_1_16.RadioValue()
    return(iif(this.value =1,"U",;
    iif(this.value =2,"C",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oFLCUSTOM_1_16.GetRadio()
    this.Parent.oContained.w_FLCUSTOM = this.RadioValue()
    return .t.
  endfunc

  func oFLCUSTOM_1_16.SetRadio()
    this.Parent.oContained.w_FLCUSTOM=trim(this.Parent.oContained.w_FLCUSTOM)
    this.value = ;
      iif(this.Parent.oContained.w_FLCUSTOM=="U",1,;
      iif(this.Parent.oContained.w_FLCUSTOM=="C",2,;
      iif(this.Parent.oContained.w_FLCUSTOM=="A",3,;
      0)))
  endfunc


  add object oBtn_1_17 as StdButton with uid="WSZYFDNWEJ",left=696, top=10, width=93,height=20,;
    caption="Filter", nPag=1;
    , ToolTipText = "Search";
    , HelpContextID = 193199460;
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      with this.Parent.oContained
        .Notifyevent('RefreshZoom')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="ZTRBYYVOOP",left=696, top=38, width=93,height=20,;
    caption="ReLoad", nPag=1;
    , ToolTipText = "ReLoad";
    , HelpContextID = 75545942;
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      with this.Parent.oContained
        .Notifyevent('reload')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELECTALL_1_22 as StdRadio with uid="BHGULJYDAA",rtseq=10,rtrep=.f.,left=13, top=320, width=151,height=35;
    , cFormVar="w_SELECTALL", ButtonCount=2, bObbl=.f., nPag=1;
    , FntName="Arial", FntSize=8, FntBold=.f., FntItalic=.t., FntUnderline=.f., FntStrikeThru=.f.

    proc oSELECTALL_1_22.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Select All"
      this.Buttons(1).HelpContextID = 174447958
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deselect All"
      this.Buttons(2).HelpContextID = 174447958
      this.Buttons(2).Top=16
      this.SetAll("Width",149)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECTALL_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(10))))
  endfunc
  func oSELECTALL_1_22.GetRadio()
    this.Parent.oContained.w_SELECTALL = this.RadioValue()
    return .t.
  endfunc

  func oSELECTALL_1_22.SetRadio()
    this.Parent.oContained.w_SELECTALL=trim(this.Parent.oContained.w_SELECTALL)
    this.value = ;
      iif(this.Parent.oContained.w_SELECTALL=='S',1,;
      iif(this.Parent.oContained.w_SELECTALL=='N',2,;
      0))
  endfunc


  add object oBtn_1_23 as StdButton with uid="WBNLILDHYF",left=290, top=323, width=105,height=20,;
    caption="\<Print", nPag=1;
    , ToolTipText = "Print";
    , HelpContextID = 141007712;
  , bGlobalFont=.t.

    proc oBtn_1_23.Click()
      with this.Parent.oContained
        .Print()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CURFLORGNAM))
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="YADSGMMTKH",left=414, top=324, width=105,height=20,;
    caption="E\<xport", nPag=1;
    , ToolTipText = "Export file to disk";
    , HelpContextID = 209152129;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        .ExportFile()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CURFLORGNAM))
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="TTYWXFQUBU",left=538, top=324, width=105,height=20,;
    caption="\<Exit", nPag=1;
    , ToolTipText = "Exit";
    , HelpContextID = 51880602;
  , bGlobalFont=.t.

    proc oBtn_1_25.Click()
      with this.Parent.oContained
        .Exit()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMEMO_1_26 as StdMemo with uid="CICWDCJXNO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MEMO", cQueryName = "MEMO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 74476786,;
   bGlobalFont=.t.,;
    Height=49, Width=784, Left=13, Top=356, readonly=.t.


  add object oBtn_1_29 as StdButton with uid="ATMCPAIUQR",left=42, top=423, width=105,height=20,;
    caption="\<Info", nPag=1;
    , ToolTipText = "Detailed information";
    , HelpContextID = 215458463;
  , bGlobalFont=.t.

    proc oBtn_1_29.Click()
      do cp_infomon with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CURFLORGNAM))
      endwith
    endif
  endfunc


  add object oBtn_1_30 as StdButton with uid="DLCKCRPZIE",left=166, top=423, width=105,height=20,;
    caption="E\<nable/Disable", nPag=1;
    , ToolTipText = "Enable / Disable";
    , HelpContextID = 61950666;
  , bGlobalFont=.t.

    proc oBtn_1_30.Click()
      this.parent.oContained.NotifyEvent("Enable_Disable")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CURFLORGNAM))
      endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="PHFMNFFXFB",left=290, top=423, width=105,height=20,;
    caption="D\<uplicate", nPag=1;
    , ToolTipText = "Duplicate";
    , HelpContextID = 4282094;
  , bGlobalFont=.t.

    proc oBtn_1_31.Click()
      do cp_duplicate with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CURFLORGNAM))
      endwith
    endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="XVCMTYKHGX",left=414, top=423, width=105,height=20,;
    caption="\<Open Obj", nPag=1;
    , ToolTipText = "Open object";
    , HelpContextID = 100191840;
  , bGlobalFont=.t.

    proc oBtn_1_32.Click()
      with this.Parent.oContained
        .OpenObj(.w_CURFLORGNAM)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CURFLORGNAM))
      endwith
    endif
  endfunc


  add object oBtn_1_33 as StdButton with uid="WXJNXEBVZP",left=538, top=423, width=105,height=20,;
    caption="\<Erase", nPag=1;
    , ToolTipText = "Erase";
    , HelpContextID = 115893173;
  , bGlobalFont=.t.

    proc oBtn_1_33.Click()
      this.parent.oContained.NotifyEvent("Erase")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CURFLORGNAM))
      endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="GSDNZQAARW",left=662, top=423, width=105,height=20,;
    caption="\<Difference", nPag=1;
    , ToolTipText = "Difference";
    , HelpContextID = 109238872;
  , bGlobalFont=.t.

    proc oBtn_1_34.Click()
      do cp_differences with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Zoom.GetVar("FLMODFIL")!="D" and !EMPTY(.w_CURFLORGNAM))
      endwith
    endif
  endfunc


  add object oBtn_1_61 as StdButton with uid="ATIIFQSBYR",left=166, top=323, width=105,height=20,;
    caption="\<Save", nPag=1;
    , ToolTipText = "Apply changes to disk";
    , HelpContextID = 113794391;
  , bGlobalFont=.t.

    proc oBtn_1_61.Click()
      with this.Parent.oContained
        .Save()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_61.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CURFLORGNAM))
      endwith
    endif
  endfunc

  add object oStr_1_4 as StdString with uid="USNSTLJBVM",Visible=.t., Left=18, Top=12,;
    Alignment=1, Width=44, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="OPBNQBQRKF",Visible=.t., Left=18, Top=38,;
    Alignment=1, Width=44, Height=18,;
    Caption="Group:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="IFMMUVRUDC",Visible=.t., Left=285, Top=12,;
    Alignment=1, Width=61, Height=18,;
    Caption="Company:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="UZCSDPJXSA",Visible=.t., Left=305, Top=38,;
    Alignment=1, Width=40, Height=18,;
    Caption="Type:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="IVFORMDCVZ",Visible=.t., Left=515, Top=38,;
    Alignment=1, Width=53, Height=18,;
    Caption="Folder:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PDRBBMNYAO",Visible=.t., Left=658, Top=322,;
    Alignment=0, Width=120, Height=17,;
    Caption="Modified Standard"    , forecolor=RGB(0,0,200);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="IEXUNUHWIT",Visible=.t., Left=658, Top=337,;
    Alignment=0, Width=58, Height=17,;
    Caption="New File"    , forecolor = rgb(0,200,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_3 as StdBox with uid="NEXBRCEGCB",left=13, top=3, width=784,height=66

  add object oBox_1_19 as StdBox with uid="GBCEXBRQUD",left=652, top=319, width=145,height=35

  add object oBox_1_28 as StdBox with uid="ZLXZKZOAOO",left=13, top=411, width=784,height=43
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_monitor','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
