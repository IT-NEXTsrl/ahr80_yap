* ----------------------------------------------------------------------------
* Programma: cp_monlib
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Alessio Coli, Gabriele Pieretti
* Data creazione: 14/03/07
* ----------------------------------------------------------------------------
* Libreria di funzioni monitor
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

*--- Indici colonne cp_monitor
#Define FILENAME_IDX 1
#Define FILEDESC_IDX 2
#Define FLSTATUS_IDX 3
#Define FLCUSTOM_IDX 4
#Define FILETYPE_IDX 5
#Define FLDESUTE_IDX 6
#Define FLDESGRP_IDX 7
#Define FLDESAZI_IDX 8
#Define FLORGNAM_IDX 9
#Define FLCODUTE_IDX 10
#Define FLCODGRP_IDX 11
#Define FLCODAZI_IDX 12
#Define FLREFFIL_IDX 13
#Define FL_ERASE_IDX 14
#Define FLMODFIL_IDX 15
#Define FLNEWFIL_IDX 16
#Define FLREVFIL_IDX 17
#Define FLAUTHOR_IDX 18
#Define FL_NOTES_IDX 19

*--- Controlla l'esistenza della cartella aziendale
*--- Se non c'� la crea
Procedure cp_Monitor_Check_DirAzi()
    If Len(i_codazi)>0
        If Not Directory(Alltrim(i_usrPath+i_codazi+"\"))
            Md Alltrim(i_usrPath)+ Alltrim(i_codazi)
        Endif
    Endif
Endproc

*--- Controllo estensione file
*--- Ritorna .t. se l'estensione appartiene all'elenco di quelle da gestire
Function cp_Monitor_CheckExtension(pFileName)
    Local l_ext, l_x
    l_ext = Justext(m.pFileName)
    For l_x=1 To Alen(i_ext,1)
        If i_ext[l_x,2] And (Lower(i_ext[l_x,1])==Lower(l_ext) Or Lower(i_ext[l_x,1])==Lower(Right(l_ext,4)))
            Return .T.
        Endif
    Endfor
    Return .F.
Endfunc

*--- Funzione di estrazione filename senza personalizzazioni ne estensione
Function cp_monitor_GetFilename(pFull_name)
    Local l_file_name, l_name, l_cod
    l_name = Juststem(m.pFull_name)
    m.pFull_name = Alltrim(m.pFull_name)
    l_file_name = ""
    Do Case
        Case cp_monitor_GetCodute(m.pFull_name)<>0 Or cp_monitor_GetCodgrp(m.pFull_name)<>0
            m.l_file_name = Left(Juststem(m.pFull_name), Rat('_', Juststem(m.pFull_name))-1)
        Otherwise
            m.l_file_name = Juststem(m.pFull_name)
    Endcase
    *---- File disattivo
    l_file_name = Iif(Left(m.l_file_name, 1)=='@',Substr(m.l_file_name, 2), m.l_file_name)
    Return m.l_file_name
Endfunc

*--- Funzione di estrazione codice utente
*--- Se ritorna 0 significa nessun utente
Function cp_monitor_GetCodute(pFull_name)
    Local l_name
    l_name = Juststem(m.pFull_name)
    * Controllo presenza numero e un _ in fondo al nome file
    Local l_cod
    l_cod = Substr(m.l_name, Rat('_', m.l_name)+1)
    If Isdigit(l_cod)
        Return Val(l_cod)
    Else
        Return 0
    Endif
Endfunc

*--- Funzione di estrazione codice gruppo
*--- Se ritorna 0 significa nessun gruppo
Function cp_monitor_GetCodgrp(pFull_name)
    Local l_name
    l_name = Juststem(m.pFull_name)
    * Controllo presenza numero e un _G in fondo al nome file
    Local l_cod
    l_cod = Substr(m.l_name, Rat('_G', m.l_name)+2)
    If Isdigit(l_cod)
        Return Val(l_cod)
    Else
        Return 0
    Endif
Endfunc

*--- Funzione di estrazione codice azienda
Function cp_monitor_GetCodazi(pFull_name)
    * Impostazione codice azienda
    * Occorre controllare se il file si trova dentro una directory che sta dentro userdir
    * Cerchiamo \userdir\ all'interno del path dei file, estraiamo il nome della cartella dopo userdir e leviamo gli \
    Local l_name
    l_name = Justpath(Upper(m.pFull_name))
    If At(Upper("\rightclick\"),Upper(Alltrim(pFull_name))) != 0 Or At(Upper("\deskmenu\"),Upper(Alltrim(pFull_name))) != 0
        Return ""
    Else
        Return Strextract(Substr(Addbs(m.l_name), (At("\USERDIR\", Addbs(m.l_name))+8)),"\","\")
    Endif
Endfunc

*---
*Ritorna un array con i valori di tutti i campi della tabella settati per il file 'filename'
*---
Function cp_monitor_SetFields(pFullName,pFields)
    Local  l_FilenameWithExt, l_varPathOld
    Dimension pFields(Fcount('cp_monitor'))
    pFields=''

    *--- Controllo se l'estensione corrisponde all'elenco
    If (cp_Monitor_CheckExtension(m.pFullName))

        pFields[FILENAME_IDX] = cp_monitor_GetFilename(m.pFullName)
        Do Case
            Case At(Upper("\rightclick\"),Upper(Alltrim(m.pFullName)))!=0
                pFields[FILEDESC_IDX] = "Menu RightClick"
            Case At(Upper("\deskmenu\"),Upper(Alltrim(m.pFullName)))!=0
                pFields[FILEDESC_IDX] = "Deskmenu"
            Otherwise
                pFields[FILEDESC_IDX] = "" && descrizione
        Endcase
        * Controllo abilitazione file (@)
        If Left(Justfname(m.pFullName),1)=="@"
            pFields[FLSTATUS_IDX]= 'D'
            * m.pFullName = Substr(m.pFullName,2)
        Else
            pFields[FLSTATUS_IDX] = 'E'
        Endif
        pFields[FILETYPE_IDX] = Right(Justext(m.pFullName),4) &&flag tipo/estensione
        pFields[FLCODUTE_IDX] = cp_monitor_GetCodute(m.pFullName) && codice utente
        pFields[FLCODGRP_IDX] = cp_monitor_GetCodgrp(m.pFullName) && codice gruppo
        * Select per la descrizione utente
        pFields[FLDESUTE_IDX] = LookTab('cpusers', 'NAME', 'CODE', m.pFields[FLCODUTE_IDX])
        * Select per la descrizione gruppo
        pFields[FLDESGRP_IDX] = LookTab('cpgroups', 'NAME', 'CODE', m.pFields[FLCODGRP_IDX])
        * Impostazione codice azienda
        * Occorre controllare se il file si trova dentro una directory che sta dentro userdir
        * Cerchiamo \userdir\ all'interno del path dei file estraiamo il nome della cartella dopo userdir e leviamo gli \
        pFields[FLCODAZI_IDX] = cp_monitor_GetCodazi(m.pFullName)
        * Select per la descrizione dell'azienda
        pFields[FLDESAZI_IDX] = LookTab('AZIENDA', 'AZRAGAZI', 'AZCODAZI', m.pFields[FLCODAZI_IDX])
        pFields[FLORGNAM_IDX] = Alltrim(m.pFullName)
        pFields[FLREFFIL_IDX] = Alltrim(m.pFullName) && il padre
        pFields[FL_ERASE_IDX]="N"
        pFields[FLMODFIL_IDX]="N"
        * Controllo file custom
        If (Upper(Right(Justpath(m.pFullName),7))=="\CUSTOM")  && siamo dentro custom
            pFields[FLCUSTOM_IDX]="C"
        Else
            pFields[FLCUSTOM_IDX]="U"
        Endif
        * Setto flag FLNEWFIL (ricerca eventuale file std)
        pFields[FLNEWFIL_IDX]='S'
        l_FilenameWithExt = Iif(Len(Justext(m.pFields[FLORGNAM_IDX]))>3,m.pFields[FILENAME_IDX]+"."+Justext(m.pFields[FLORGNAM_IDX]),m.pFields[FILENAME_IDX]+"."+m.pFields[FILETYPE_IDX])
        * modifica del path per la ricerca dei file std, i_varPathNew popolato in ingresso
        l_varPathOld = Set('path')
        Set Path To &i_varPathNew
        * Ricerco il file dentro le cartelle standard tramite la funzione file
        pFields[FLNEWFIL_IDX] = Iif(File(l_FilenameWithExt),'N','S')
        Set Path To &l_varPathOld
        *--- Info
        pFields[FLREVFIL_IDX]=1.0
        pFields[FLAUTHOR_IDX]=""
        pFields[FL_NOTES_IDX]=""
        Return .T.
    Endif
    Return .F.
Endfunc

*--- Creazione e\o recupero diritti per l'utente corrente
* Ritorna il livello di diritto dell'utente
Function cp_monitor_UserRight(pForce)
    * Se il controllo � forzato o se non esiste la creo
    If pForce Or Vartype(i_userRight)=='U'
        Public i_userRight
        i_userRight = 1
        i_userRight = Min(Max(LookTab("cpusers", "userright", "code", i_codute), 1), 5)
    Endif
    Return i_userRight
Endfunc

*--- Funzione di ricerca di un file dentro le cartelle
Function cp_monitor_SearchFile(pFileToSearch, pDirs)
    Local l_foundFile,l_z, l_zz, l_numFile
    l_foundFile = ""
    For l_z=1 To Alen(pDirs, 1)
        If (pDirs[m.l_z, 2]) && se la cartella � da controllare
            l_numFile = Adir(l_aFile, pDirs[l_z,1]+"*.*")
            For l_zz = 1 To m.l_numFile
                If (Lower(m.pFileToSearch) == Lower(l_aFile[l_zz,1]))
                    l_foundFile = Alltrim(pDirs[l_z, 1]) + Alltrim(l_aFile[l_zz, 1])
                Endif
            Endfor
        Endif
    Endfor
    Return l_foundFile
Endfunc

*--- Funzione di ricerca di un file dentro le cartelle standard
*--- pFileToSearch: nome del file compresa estensione senza percorso
*--- Ritorna il percorso completo del file corrispondente se lo trova
*--- Altrimenti ritorna una stringa vuota
Function cp_monitor_SearchStd(pFileToSearch)
    If Type("i_stddir")='U'
        Public Array i_stddir(1,2)
        * popolo l'array stddir con le cartelle standard e anche la variabile var_path_new
        i_varPathNew = cp_monitor_GetStdDir(@i_stddir)
    Endif

    Return cp_monitor_SearchFile(m.pFileToSearch, @i_stddir)
Endfunc

*--- Funzione di ricerca di un file dentro la cartella custom
*--- pFileToSearch: nome del file compresa estensione senza percorso
*--- Ritorna il percorso completo del file corrispondente se lo trova
*--- Altrimenti ritorna una stringa vuota
Function cp_monitor_SearchCustom(pFileToSearch)
    Local l_foundFile, l_zz, l_numFile
    l_foundFile = ""
    l_numFile = Adir(l_cusfile,Alltrim(i_customPath)+"*.*")
    For l_zz = 1 To m.l_numFile
        If (Lower(m.pFileToSearch) == Lower(l_cusfile[l_zz,1]))
            l_foundFile = Alltrim(i_customPath)+Alltrim(l_cusfile[l_zz,1])
        Endif
    Endfor
    Return m.l_foundFile
Endfunc

*--- Funzione di ricerca di un file dentro la cartella userdir
*--- pFileToSearch: nome del file compresa estensione senza percorso
*--- Ritorna il percorso completo del file corrispondente se lo trova
*--- Altrimenti ritorna una stringa vuota
Function cp_monitor_SearchUserDir(pFileToSearch)
    Return cp_monitor_SearchFile(m.pFileToSearch, @i_usrDir)
Endfunc

*--- Funzione di appoggio per aprire la visual run-time
Function cp_monitor_vrtPrg(cPrgName)
    Local i_i, i_o, i_j, nProp
    i_o = .Null.
    *--- Cerco tra i form aperti
    For i_i = 1 To _Screen.FormCount
        i_o=_Screen.Forms(i_i)
        If Vartype(i_o.cPrg)<>'U'
            If Alltrim(Upper(i_o.cPrg)) == Alltrim(Upper(cPrgName))
                Return i_o
            Endif
            *--- Cerco se � un figlio
            nProp = Amembers(aProp, i_o)
            For i_j = 1 To nProp
                If Alltrim(Upper(aProp[i_j])) == Alltrim(Upper(cPrgName))
                    Return i_o.&aProp[i_j]
                Endif
            Endfor
        Endif
    Endfor
    *-- Non trovato
    Return .Null.
Endfunc

Function cp_monitor_buildname(oldfullname, newcodute, newcodgrp, newcodazi, newstatus, newcustom)
    Local filename, new_FLORGNAM
    filename = Alltrim(cp_monitor_GetFilename(m.oldfullname))
    new_FLORGNAM=""

    new_FLORGNAM = Iif(newstatus=="E",Alltrim(filename),"@"+Alltrim(filename)) && inizio con il nome
    If newcodute != 0 And newcodgrp == 0 && se ho selezionato un utente
        new_FLORGNAM = Alltrim(new_FLORGNAM+"_"+Alltrim(Str(newcodute)))
    Endif
    If newcodgrp != 0 And newcodute == 0&& se ho selezionato un gruppo
        new_FLORGNAM = Alltrim(new_FLORGNAM+"_G"+Alltrim(Str(newcodgrp)))
    Endif
    new_FLORGNAM = Alltrim(new_FLORGNAM+"."+Alltrim(Justext(oldfullname))) && aggiungo l'estensione
    * Controllo rightclick/deskmenu: se il vecchio percorso contiene la cartella rightclick/deskmenu
    * Ignoriamo la cartella aziendate newcodazi e ci forziamo la cartella rightclick/deskmenu
    Do Case
        Case At(Upper("\rightclick\"),Alltrim(Upper(oldfullname)))!=0 && il file � dentro rightclick
            new_FLORGNAM = Alltrim("rightclick\"+new_FLORGNAM)
        Case At(Upper("\deskmenu\"),Alltrim(Upper(oldfullname)))!=0 && il file � dentro deskmenu
            new_FLORGNAM = Alltrim("deskmenu\"+new_FLORGNAM)
        Otherwise
            If !Empty(Alltrim(newcodazi)) And newcustom != "C" && se ho selezionato una azienda
                new_FLORGNAM = Alltrim(Addbs(Alltrim(newcodazi))+new_FLORGNAM)
            Endif
    Endcase
    If newcustom=="C" && se deve andare in custom
        new_FLORGNAM = Alltrim(".\custom\"+Alltrim(new_FLORGNAM))
    Else && se deve andare in user
        new_FLORGNAM = Alltrim(Alltrim(i_usrPath)+Alltrim(new_FLORGNAM))
    Endif
    Return new_FLORGNAM
Endfunc

*--- Cancella una cartella con tutte le sue sottodirectory
*--- Se da errore restituisce .f.
*--- Utilizzo oggetto script
Procedure cp_monitor_DeleteFolder(w_PATH)
    Local L_OldErr, l_bErr
    l_bErr=.T.
    If Directory( Justpath(Addbs(m.w_PATH)) )
        L_OldErr=On("Error")
        On Error l_bErr=.F.
        w_Shell=Createobject("Scripting.FileSystemObject")
        w_Shell.DeleteFolder(Justpath(Addbs(m.w_PATH)))
        w_Shell = .Null.
        On Error &L_OldErr
    Endif
    Return l_bErr
Endproc

*---
*   Ritorna true se c'� almeno un record selezionato con la checkbox
*   Altrimenti ritorna false
*---
Function cp_monitor_SelectRec(_table_cursor_)
    Local numero_records
    Select * From (_table_cursor_) Where xchk==1 Into Cursor _table_cursor2_
    numero_records = Iif(Used('_table_cursor2_'),Reccount(),-1)
    Use In Select("_table_cursor2_")
    Return numero_records>0
Endfunc

*--- Cartelle std
Function cp_monitor_GetStdDir(dir_list)
    Dimension dir_list(1,2)
    Local l_next_dir, l_elem, l_new_path, l_i, l_j
    dir_list[1,1]=Addbs(Sys(2014,Sys(5)+Sys(2003) + "\..\"))
    dir_list[1,2]=.T.
    l_next_dir = Alen(m.dir_list,1)
    l_i = 1
    *---Riempio array con cartelle trovate
    Do While l_i <= Alen(m.dir_list, 1)
        Dimension actual_folder(1)
        l_elem = Adir(actual_folder, dir_list[l_i, 1]+"*.*","D")   && leggo il contenuto
        For l_j = 1 To l_elem
            If Right(actual_folder[l_j, 5], 1)=="D" And actual_folder[l_j, 1]!='.' And actual_folder[l_j, 1]!='..';
                    AND At("\exe\custom",Lower(dir_list[l_i, 1]+actual_folder[l_j, 1]))==0
                l_next_dir = m.l_next_dir + 1
                Dimension dir_list(m.l_next_dir, 2)
                dir_list[m.l_next_dir, 1] = Addbs(dir_list[l_i, 1] + actual_folder[l_j, 1])
                dir_list[m.l_next_dir, 2]=.T.
            Endif
        Endfor
        l_i = l_i + 1
    Enddo
    Asort(dir_list)
    l_new_path= ""
    For l_i=1 To Alen(dir_list, 1)
        l_new_path = Upper(Alltrim( l_new_path)+";"+Alltrim(dir_list[l_i, 1]))
    Endfor
    Return l_new_path
Endfunc

*--- Funzione che ritorna il numero di record modificati della tabella cp_monitor
Function cp_monitor_Modified_records()
    Local l_NumMod
    Select * From cp_monitor Where FLMODFIL!="N" Or FL_ERASE=="S" Into Cursor _ChkChange_
    l_NumMod= Iif(Used('_ChkChange_'),Reccount('_ChkChange_'),0)
    Use In Select('_ChkChange_')
    Return l_NumMod
Endfunc

*--- Apertura oggetto
Function cp_monitor_OpenObject()
    i_curform.OpenObj(i_curform.w_CURFLORGNAM)
Endfunc

