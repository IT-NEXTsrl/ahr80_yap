* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: LOOKTAB
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  56
* ----------------------------------------------------------------------------
* Cerca un campo in una tabella (per report)
* ----------------------------------------------------------------------------

*********** NON CI SONO MACRO DA TRADURRE
*Zucchetti Aulla Inizio
* Se viene modificata la looktab occorre modificare anche la
* adhoc_looktab presente nella gestione gsar_klt (Area manuale Functions & Procedures)
*Zucchetti Aulla Fine
*#include "cp_app_lang.inc"

Lparam i_cTable,i_cField,i_cK1,i_xV1,i_cK2,i_xV2,i_cK3,i_xV3,i_cK4,i_xV4,i_cK5,i_xV5
Local i_NT,i_nConn,i_cPhTable,i_cWhere,i,N,i_xRes,i_nOldArea,i_cLoTable
i_nOldArea=Select()
i_NT=cp_OpenTable(i_cTable)
i_xRes=.Null.
If i_NT<>0
    i_nConn = i_TableProp[i_NT,3]
    i_cLoTable = Upper(i_TableProp[i_NT,1])
    i_cPhTable = cp_SetAzi(i_TableProp[i_NT,2],.T.,i_NT)
    i_cWhere=''
    For i=1 To 5
        N=Alltrim(Str(i))
        If Type("i_xv"+N)='D' And Empty(i_xv&N)
            i_xv&N=Null
        Endif
        If Type("i_cK"+N)='C' And Not(Isnull(i_xv&N))
            i_cWhere=i_cWhere+Iif(Empty(i_cWhere),'',' and ')+Upper(i_cK&N)+'='+Iif(i_nConn=0,cp_ToStr(i_xv&N),cp_ToStrODBC(i_xv&N))
        Endif
    Next
    If Not(Empty(i_cWhere))
        If i_nConn<>0
            cp_SQL(i_nConn,"select "+Upper(i_cField)+" from "+i_cPhTable+" "+i_cLoTable+" where "+i_cWhere,'__looktab__')
        Else
            Select &i_cField From &i_cPhTable As &i_cLoTable Where &i_cWhere nofilter Into Cursor __looktab__
        Endif
        If Used('__looktab__')
            i_xRes = cp_ToDate(&i_cField) && trasforma l' eventuale datetime in date
            Use
        Endif
    Endif
    cp_CloseTable(i_cTable)
Endif
Select (i_nOldArea)
Return(i_xRes)
