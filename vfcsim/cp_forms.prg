* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_FORMS
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated:     03/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Definizione del form standard  e Page Frame standard
* ----------------------------------------------------------------------------
#include "cp_app_lang.inc"

#Define GWL_WNDPROC	-4
#Define TOOLBAR_BORDERCOLOR 55
*--- Utilizzate per disabilitare la LoadConfig del Runtime CTRL+SHIFT
#Define VK_CONTROL 0x11
#Define VK_SHIFT 0x10
#Define WM_EXITSIZEMOVE				0x232
#Define WM_ENTERSIZEMOVE			0x231
#Define BROADCAST_QUERY_DENY 		0x424D5144

Define Class CPForm As Form
    *backcolor=rgb(128,128,0)
    bGlobalFont = .T.	&&Global font indica se la gestione eredita o meno il font del progetto
    *--- Zucchetti Aulla Inizio - gestione riconessione degli zoom secondari degli elenchi
    ozoomelenco =.F. &&riferimento allo zoom dell elenco
    briposiz=.F.     &&indica sw riposizionare il cursore dopo aver ripristinato la conessione asincrona
    *--- Zucchetti Aulla Fine - gestione riconessione degli zoom secondari degli elenchi
    #If Version(5)>=900
        Add Object ImgBackGround As Image With Stretch=2, Visible=.F.
    #Endif
    bLockVfp=.F.

    Procedure Init
        If Vartype(i_cWarnType)='C' And i_cWarnType='B' And Type ("this.oBalloon")<>'O'
            This.AddObject("oBalloon","cp_balloontip")
        Endif
        DoDefault()
    Endproc

    Procedure Show(bModal)
        DoDefault(m.bModal)
        bModal = Iif(Vartype(m.bModal)='N', m.bModal, 0)
        If i_VisualTheme<>-1 And (This.WindowType = 1 Or m.bModal=1) And !(Vartype(g_SCHEDULER)='C' And g_SCHEDULER='S') && Zucchetti Aulla Aggiunto And
            If Vartype(i_MenuToolbar)='O'
                *--- Disabilito il menu
                i_MenuToolbar.ShowModalWnd = i_MenuToolbar.ShowModalWnd + 1
            Endif
            If Vartype(i_ThemesManager)='O'
                This.bLockVfp=i_ThemesManager.nLockVfp>0
                i_ThemesManager.LockVfp(.F., .T.)
            Endif
        	*--- Notifico all'interfaccia Gadget l'apertura di una maschera modale
        	*--- Le maschera modali devono rimanere sempre sopra l'interfaccia dei gadget
            If Vartype(oGadgetManager)='O'
        		If oGadgetManager.nModalHWnd=0
		        	Local cCmd
		        	oGadgetManager.nModalHWnd = This.HWnd
		        	*--- Sposto il focus tra interfaccia e form per farli posizionare correttamente
		        	SetWindowPos(oGadgetManager.HWnd, 1, 0, 0, 0, 0, 0x0010+0x0001+0x0002) &&NoActivate, NoSize e NoMove
	        	Endif
	        	oGadgetManager.NotifyEvent("GadgetShowModalForm")
	        Endif
        Endif
    	&& Notifico l'apertura di una maschera NON modale, escludendo i gadget e la library per aggiungerli
    	If Vartype(oGadgetManager)='O' And !(Lower(This.ParentClass)=="stdgadgetform") And !(Lower(This.Class)=="tgsut_kgs");
    	   And i_VisualTheme<>-1 And This.WindowType=0 And m.bModal=0 And !(Vartype(g_SCHEDULER)='C' And g_SCHEDULER='S')
	    	oGadgetManager.NotifyEvent("GadgetShowForm")
        Endif
    Endproc

    Procedure Unload
        If This.WindowType = 1 And i_VisualTheme<>-1 And !(Vartype(g_SCHEDULER)='C' And g_SCHEDULER='S')&& Zucchetti Aulla Aggiunto And
            If Vartype(i_MenuToolbar)='O'
                *--- Disabilito il menu
                i_MenuToolbar.ShowModalWnd = i_MenuToolbar.ShowModalWnd - 1
            Endif
            If Vartype(i_ThemesManager)='O' And This.bLockVfp
                i_ThemesManager.LockVfp(.T., .T.)
            Endif
        	*--- Notifico all'interfaccia Gadget la chiusura della maschera modale
        	*--- se � quella a cui faceva riferimento deve nuovamente mettersi sopra la toolbar
            If Vartype(oGadgetManager)='O' And oGadgetManager.nModalHWnd==This.HWnd
	        	Local cCmd
	        	oGadgetManager.nModalHWnd = 0
	        	SetWindowPos(oGadgetManager.HWnd, 1, 0, 0, 0, 0, 0x0010+0x0001+0x0002) &&NoActivate, NoSize e NoMove
            Endif
        Endif
        DoDefault()
    Endproc
    Function msgFmt(i_msg)
        Local i_p1,i_p2,i_i,i_expr,i_val
        i_i = 1
        i_p1 = At('%',i_msg)
        Do While i_p1<>0
            If Lower(Substr(i_msg,i_p1,3))='%w_' Or Lower(Substr(i_msg,i_p1,7))='%format' Or Lower(Substr(i_msg,i_p1,4))='%str' Or Lower(Substr(i_msg,i_p1,5))='%ttoc' Or Lower(Substr(i_msg,i_p1,5))='%dtoc' Or Lower(Substr(i_msg,i_p1,11))='%datetochar'
                i_p2 = At('%',i_msg,i_i+1)
                If i_p2<>0
                    i_expr = Strtran(Substr(i_msg,i_p1+1,i_p2-i_p1-1),'w_','this.w_')
                    If Lower(Left(i_expr,7))='format('
                        i_expr='transform'+Substr(i_expr,7)
                    Endif
                    i_val = &i_expr
                    i_msg = Left(i_msg,i_p1-1)+i_val+Substr(i_msg,i_p2+1)
                Else
                    i_i = i_i+1
                Endif
            Else
                * % isolato
                i_i = i_i+1
            Endif
            i_p1 = At('%',i_msg,i_i)
        Enddo
        Return  i_msg
    Endfunc
Enddefine

Define Class CPSysForm As CPForm
    *backcolor=rgb(128,128,0)
Enddefine
Define Class CPPageFrame As PageFrame
    *backcolor=rgb(128,128,0)
    nPageActive = 1	&&Pagina attiva, necessaria per non far scattare pi� volte la click
    *--- Effettuo la click al cambio pagina
    Procedure ActivePage_Assign(xValue)
        If This.ActivePage <> m.xValue
            This.ActivePage = m.xValue
            This.Click()
            This.nPageActive = This.ActivePage
        Endif
    Endproc
Enddefine
Define Class CPContainer As Container
    BackStyle=0
    bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
    bNoMenuAction=.F.         	&& Disabilita nel tasto destro sottomenu Azioni
    bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
    bNoMenuAutorizaction=.F.		&& Disabilita nel tasto destro sottomenu Autorizzazioni
Enddefine
Define Class cpzoomcontainer As Container
    *backcolor=rgb(192,192,192)
    BackStyle=0	&& Container zoom trasparente per eliminare alone grigio se stile XP attivo
    BorderWidth=0
    *fontname='Arial'
    *fontsize=9
Enddefine

*--- Zucchetti Aulla Inizio - Gadget
#Define WM_MOVING 0x0216
#Define WM_MOUSEMOVE 0x0200

Define Class cp_GadgetGripper As Container
    BorderWidth = 3
    BackStyle=0
    MousePointer=0
    Width=150
    Height=150
    Dimension aPolyPointsTriangle[3,2]
    Dimension aPolyPointsClose[12,2]
    Dimension aPolyPointsResizer[6,2]
    Dimension aPolyPointsOption[12,2]
    Dimension aPolyPointsSelected[6,2]
    nTriangleHeight = 20

    Add Object oCornerTopLeft As Shape With MousePointer = 0
    Add Object oCornerTopRight As Shape With MousePointer = 0
    Add Object oCornerBottomRight As Shape With MousePointer = 0
    Add Object oCornerBottomLeft As Shape With MousePointer = 0

    Add Object oCloseShape As Shape With MousePointer=15, ToolTipText="Chiudi"
    Add Object oResizer As Shape
    Add Object oOptShape As Shape With MousePointer=15, ToolTipText="Opzioni"
    Add Object oSelShape As Shape With MousePointer=0, ToolTipText="Selezionato"

    Procedure ChangeTheme()
        With This
            Local nColor, nForeColor
            nColor = Iif(Thisform.nBorderColor=-1, RGBAlphaBlending(Thisform.BackColor, Rgb(255,255,255), 0.15), Thisform.nBorderColor)
            .BorderColor= m.nColor
            .oCornerTopLeft.BackColor = .BorderColor
            .oCornerTopLeft.BorderColor = .BorderColor
            .oCornerTopRight.BackColor = .BorderColor
            .oCornerTopRight.BorderColor = .BorderColor
            .oCornerBottomRight.BackColor = .BorderColor
            .oCornerBottomRight.BorderColor = .BorderColor
            .oCornerBottomLeft.BackColor = .BorderColor
            .oCornerBottomLeft.BorderColor = .BorderColor

            *--- Colore dei controlli; devono essere sempre visibili a prescindere dal colore del bordo
            nForeColor = GetBestForeColor(m.nColor)

            .oCloseShape.BackColor = m.nForeColor
            .oCloseShape.BorderColor = m.nForeColor

            .oResizer.BackColor = m.nForeColor
            .oResizer.BorderColor = m.nForeColor

            .oOptShape.BackColor = m.nForeColor
            .oOptShape.BorderColor = m.nForeColor
            .oOptShape.FillColor = m.nForeColor

            .oSelShape.BackColor = m.nForeColor
            .oSelShape.BorderColor = m.nForeColor
            .oSelShape.FillColor = m.nForeColor

        Endwith
    Endproc

    Procedure Init()
        With This
            *--- Triangolo
            .aPolyPointsTriangle[1,1]=1
            .aPolyPointsTriangle[1,2]=1
            .aPolyPointsTriangle[2,1]=99
            .aPolyPointsTriangle[2,2]=1
            .aPolyPointsTriangle[3,1]=99
            .aPolyPointsTriangle[3,2]=99

            .oCornerTopLeft.Polypoints = "This.Parent.aPolyPointsTriangle"
            .oCornerTopRight.Polypoints = "This.Parent.aPolyPointsTriangle"
            .oCornerBottomRight.Polypoints = "This.Parent.aPolyPointsTriangle"
            .oCornerBottomLeft.Polypoints = "This.Parent.aPolyPointsTriangle"

            .oCornerTopLeft.Rotation = 90
            .oCornerBottomRight.Rotation = 270
            .oCornerBottomLeft.Rotation = 180

            .oCornerTopLeft.Move(1,3,.nTriangleHeight,.nTriangleHeight)
            .oCornerTopRight.Move(.Width-.nTriangleHeight-2,3,.nTriangleHeight,.nTriangleHeight)
            .oCornerBottomRight.Move(.Width-.nTriangleHeight-2,.Height-.nTriangleHeight-2,.nTriangleHeight,.nTriangleHeight)
            .oCornerBottomLeft.Move(1,.Height-.nTriangleHeight-2,.nTriangleHeight,.nTriangleHeight)

            .oCornerTopRight.Anchor = 9
            .oCornerBottomRight.Anchor = 12
            .oCornerBottomLeft.Anchor = 6
            *--- Close shape
            .aPolyPointsClose[1,1] = 1&&A
            .aPolyPointsClose[1,2] = 1
            .aPolyPointsClose[2,1] = 30&&B
            .aPolyPointsClose[2,2] = 1
            .aPolyPointsClose[3,1] = 50&&C
            .aPolyPointsClose[3,2] = 30
            .aPolyPointsClose[4,1] = 60&&D
            .aPolyPointsClose[4,2] = 1
            .aPolyPointsClose[5,1] = 80&&E
            .aPolyPointsClose[5,2] = 1
            .aPolyPointsClose[6,1] = 50&&F
            .aPolyPointsClose[6,2] = 40
            .aPolyPointsClose[7,1] = 80&&H
            .aPolyPointsClose[7,2] = 70
            .aPolyPointsClose[8,1] = 60&&I
            .aPolyPointsClose[8,2] = 70
            .aPolyPointsClose[9,1] = 50&&L
            .aPolyPointsClose[9,2] = 50
            .aPolyPointsClose[10,1] = 30&&M
            .aPolyPointsClose[10,2] = 70
            .aPolyPointsClose[11,1] = 1&&N
            .aPolyPointsClose[11,2] = 70
            .aPolyPointsClose[12,1] = 40&&P
            .aPolyPointsClose[12,2] = 40

            .oCloseShape.Polypoints ="This.Parent.aPolyPointsClose"
            .oCloseShape.Move(.Width-12, 4, 10,10)
            .oCloseShape.Anchor=9
            .oCloseShape.BackColor = Rgb(255,255,255)
            .oCloseShape.BorderColor = Rgb(255,255,255)
            *--- Resize shape
            .aPolyPointsResizer[1,1]=80
            .aPolyPointsResizer[1,2]=0
            .aPolyPointsResizer[2,1]=100
            .aPolyPointsResizer[2,2]=0
            .aPolyPointsResizer[3,1]=100
            .aPolyPointsResizer[3,2]=100
            .aPolyPointsResizer[4,1]=0
            .aPolyPointsResizer[4,2]=100
            .aPolyPointsResizer[5,1]=0
            .aPolyPointsResizer[5,2]=80
            .aPolyPointsResizer[6,1]=80
            .aPolyPointsResizer[6,2]=80

            .oResizer.Polypoints = "This.Parent.aPolyPointsResizer"
            .oResizer.Move(.Width-10, .Height-10, 6,6)
            .oResizer.Anchor = 12
            .oResizer.BackColor = Rgb(255,255,255)
            .oResizer.BorderColor = Rgb(255,255,255)
            .oResizer.MousePointer = 8
            *--- Option shape (menu)
            .aPolyPointsOption[1,1] = 1&&A
            .aPolyPointsOption[1,2] = 1
            .aPolyPointsOption[2,1] = 100&&B
            .aPolyPointsOption[2,2] = 1
            .aPolyPointsOption[3,1] = 100&&C
            .aPolyPointsOption[3,2] = 20
            .aPolyPointsOption[4,1] = 1&&D
            .aPolyPointsOption[4,2] = 20
            .aPolyPointsOption[5,1] = 1&&E
            .aPolyPointsOption[5,2] = 30
            .aPolyPointsOption[6,1] = 100&&F
            .aPolyPointsOption[6,2] = 30
            .aPolyPointsOption[7,1] = 100&&H
            .aPolyPointsOption[7,2] = 50
            .aPolyPointsOption[8,1] = 1&&I
            .aPolyPointsOption[8,2] = 50
            .aPolyPointsOption[9,1] = 1&&H
            .aPolyPointsOption[9,2] = 60
            .aPolyPointsOption[10,1] = 100&&I
            .aPolyPointsOption[10,2] = 60
            .aPolyPointsOption[11,1] = 100&&I
            .aPolyPointsOption[11,2] = 80
            .aPolyPointsOption[12,1] = 1&&I
            .aPolyPointsOption[12,2] = 80

            .oOptShape.Polypoints = "This.Parent.aPolyPointsOption"
            .oOptShape.Move(3, 3, 9, 11)
            .oOptShape.BorderStyle=0
            .oOptShape.FillStyle=0
            .oOptShape.BackColor = Rgb(255,255,255)
            .oOptShape.BorderColor = Rgb(255,255,255)
            .oOptShape.FillColor = Rgb(255,255,255)
            *--- Selected shape
            .aPolyPointsSelected[1,1]=1
            .aPolyPointsSelected[1,1] = 30&&A
            .aPolyPointsSelected[1,2] = 50
            .aPolyPointsSelected[2,1] = 50&&B
            .aPolyPointsSelected[2,2] = 70
            .aPolyPointsSelected[3,1] = 90&&C
            .aPolyPointsSelected[3,2] = 30
            .aPolyPointsSelected[4,1] = 90&&D
            .aPolyPointsSelected[4,2] = 50
            .aPolyPointsSelected[5,1] = 50&&E
            .aPolyPointsSelected[5,2] = 90
            .aPolyPointsSelected[6,1] = 30&&F
            .aPolyPointsSelected[6,2] = 70

            .oSelShape.Polypoints = "This.Parent.aPolyPointsSelected"
            .oSelShape.Move(2, .Height-12, 10, 10)
            .oSelShape.BackColor = Rgb(255,255,255)
            .oSelShape.BorderColor = Rgb(255,255,255)
            .oSelShape.BorderStyle=1
            .oSelShape.BackStyle=1
            .oSelShape.FillColor = Rgb(255,255,255)
            .oSelShape.FillStyle=0
            .oSelShape.Anchor=6

            .ShowGripper(.F.)
            .ChangeTheme()
        Endwith

    Endproc

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        Thisform.oParentObject.MouseHover(Thisform)

    Procedure ShowGripper(bShow)
        With This
            If .oCornerTopLeft.Visible # m.bShow &&And Thisform.bOptions
                .oCornerTopLeft.Visible = m.bShow &&And Thisform.bOptions
            Endif
            If .oOptShape.Visible # .oCornerTopLeft.Visible
                .oOptShape.Visible = .oCornerTopLeft.Visible
            Endif
            If .oCornerTopRight.Visible # m.bShow And Thisform.Closable
                .oCornerTopRight.Visible = m.bShow And Thisform.Closable
            Endif
            If .oCloseShape.Visible #.oCornerTopRight.Visible
                .oCloseShape.Visible = .oCornerTopRight.Visible
            Endif
            If .oCornerBottomRight.Visible # m.bShow And Thisform.bSizable
                .oCornerBottomRight.Visible = m.bShow And Thisform.bSizable
            Endif
            If .oResizer.Visible # .oCornerBottomRight.Visible
                .oResizer.Visible = .oCornerBottomRight.Visible
            Endif
            If .oCornerBottomLeft.Visible # Thisform.bSelected
                .oCornerBottomLeft.Visible = Thisform.bSelected
            Endif
            If .oSelShape.Visible # .oCornerBottomLeft.Visible
                .oSelShape.Visible = .oCornerBottomLeft.Visible
            Endif
            If .BorderWidth # Iif(m.bShow Or Thisform.bSelected, 3, 1)
                .BorderWidth = Iif(m.bShow Or Thisform.bSelected, 3, 1)
            Endif
        Endwith
    Endproc

    Procedure ShowSelGripper()
        With This
            If .oCornerBottomLeft.Visible # Thisform.bSelected
                .oCornerBottomLeft.Visible = Thisform.bSelected
            Endif
            If .oSelShape.Visible # .oCornerBottomLeft.Visible
                .oSelShape.Visible = .oCornerBottomLeft.Visible
            Endif
            If .BorderWidth # Iif(Thisform.bSelected, 3, 1)
                .BorderWidth = Iif(Thisform.bSelected, 3, 1)
            Endif
        Endwith
    Endproc

    Procedure oOptShape.Click()
        *--- Show menu
        *--- per maggiore velocit� carico il menu da cursore
        Local oldarea
        oldarea = Select()
        With Thisform
            Local cCursor, l_MenuID, l_i, l_lvlKey, l_cCaption, l_cProgram, l_bSkip, l_bCheck, l_nDirectory, l_cBmp, l_lvlGrp, l_lvlCustom
            Local loPopupMenu, cnCmd
            cnCmd = Sys(2015)
            Public &cnCmd	&&Contiene il comando da eseguire alla selezione voce di menu
            &cnCmd = ""
            *--- Nuovo menu contestuale
            loPopupMenu = Createobject("cbPopupMenu")
            loPopupMenu.oPOPUPMENU.Init()
            cCursor = loPopupMenu.oPOPUPMENU.CCURSORNAME

            If Vartype(g_MenuPopupID)="U"
                Public g_MenuPopupID
                g_MenuPopupID = 5000
            Endif
            g_MenuPopupID = m.g_MenuPopupID + 5000
            l_MenuID = m.g_MenuPopupID
            l_i=1

            *--- Livello 0
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                ("",0,"",0,.T., m.l_MenuID,"","001",.F.,.F.)
            *--- Se opzioni
            If .bOptions And .bOptionsAccess
                l_MenuID = m.l_MenuID + 1
                l_lvlKey = '001.'+Padl(m.l_i, 3, "0")
                l_i = m.l_i + 1
                l_cCaption = cp_Translate("Opzioni...")
                l_cProgram = cnCmd+"='.NotifyEvent([GadgetOption])'"
                l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
                l_bCheck = .F.  &&.T. checked
                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
                l_cBmp = "" &&".\bmp\miobitmap.bmp"
                Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                    (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
            Endif
            *--- Submenu Personalizza
            Local l_c
            l_c = 1
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = '001.'+Padl(m.l_i, 3, "0")
            l_lvlCustom = l_lvlKey+"." &&mi salvo il livello
            l_i = m.l_i + 1
            l_cCaption = cp_Translate("Personalizza")
            l_cProgram = ""
            l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
            l_cBmp = "" &&".\bmp\miobitmap.bmp"
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
            *--- Submenu Personalizza/Tema
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = l_lvlCustom+Padl(m.l_c, 3, "0")
            l_lvlTheme = l_lvlKey+"." &&mi salvo il livello
            l_c = m.l_c + 1
            l_cCaption = cp_Translate("Tema")
            l_cProgram = ""
            l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
            l_cBmp = "" &&".\bmp\miobitmap.bmp"
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)

            *--- Submenu Personalizza/Aggiornamento
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = l_lvlCustom+Padl(m.l_c, 3, "0")
            l_lvlUpdate = l_lvlKey+"." &&mi salvo il livello
            l_c = m.l_c + 1
            l_cCaption = cp_Translate("Aggiorna ogni...")
            l_cProgram = ""
            l_bSkip = !.bUpdateOnTimer	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
            l_cBmp = "" &&".\bmp\miobitmap.bmp"
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)


            *--- Vai alla library
            If cp_IsAdministrator()
	            l_MenuID = m.l_MenuID + 1
	            l_lvlKey = l_lvlCustom+Padl(m.l_c, 3, "0")
	            l_c = m.l_c + 1
	            l_cCaption = cp_Translate('Vai alla library...')
	            l_cProgram = cnCmd+[='OpenGest("A","gsut_mga", "GACODICE", "]+Thisform.cKeyGadget +[")']
	            l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
	            l_bCheck = .F.  &&.T. checked
	            l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
	            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
	                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
            Endif

            *--- Sposta nell'area    
            *--- Solo se sono in gruppo home e il gadget ha un'area assegnata diversa da HOME
            IF !EMPTY(NVL(.cSourceGroup,""))
	            l_MenuID = m.l_MenuID + 1
	            l_lvlKey = '001.'+Padl(m.l_i, 3, "0")
	            l_i = m.l_i + 1
	            IF ThisForm.oParentObject.nActivePage=1
		            l_cCaption = cp_msgformat("Sposta nell'area (%1)", .cSourceGroupDes)
		            l_cProgram = cnCmd+"='.oParentObject.MoveInGroup(Thisform, 2)'"
		        ELSE
			        l_cCaption = cp_Translate("Sposta nell'area (Home)")
		        	l_cProgram = cnCmd+"='.oParentObject.MoveInGroup(Thisform, 1)'"
		        ENDIF 
	            l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
	            l_bCheck = .F.  &&.T. checked
	            l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
	            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
	                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)				            	
            EndIf    

            *--- Separatore
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = '001.'+Padl(m.l_i, 3, "0")
            l_i = m.l_i + 1
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                ("", 0, "", 4, .T., m.l_MenuID, "", m.l_lvlKey, .F., .F.)

            *--- Seleziona (Area)
            Local l_g
            l_g = 1
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = '001.'+Padl(m.l_i, 3, "0")
            l_lvlGrp = l_lvlKey+"." &&mi salvo il livello
            l_i = m.l_i + 1
            l_cCaption = cp_msgformat("Seleziona (%1)", Alltrim(.oParentObject.aPage(.oParentObject.nActivePage)))
            l_cProgram = ""
            l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
            l_cBmp = "" &&".\bmp\miobitmap.bmp"
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)

            *--- Separatore
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = '001.'+Padl(m.l_i, 3, "0")
            l_i = m.l_i + 1
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                ("", 0, "", 4, .T., m.l_MenuID, "", m.l_lvlKey, .F., .F.)

            *--- Salva modifiche
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = '001.'+Padl(m.l_i, 3, "0")
            l_i = m.l_i + 1
            l_cCaption = cp_Translate("Salva modifiche")
            l_cProgram = cnCmd+"='.oParentObject.oCharmBar.ExitModify()'"
            l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)

            *--- Submenu Gruppo/Tema
            l_bIsSelectedGadget = .oParentObject.GetLogicProp("bSelected", "OR")	&&Selezionati solo del gruppo attuale            
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = l_lvlGrp+Padl(m.l_g, 3, "0")
            l_lvlTheme_grp = l_lvlKey+"." &&mi salvo il livello
            l_g = m.l_g + 1
            l_cCaption = cp_Translate("Tema")
            l_cProgram = ""
            l_bSkip = !m.l_bIsSelectedGadget 	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
            l_cBmp = "" &&".\bmp\miobitmap.bmp"
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
            *--- Submenu Gruppo/Aggiornamento
            l_bIsSelectedUpdateOnTimer = .oParentObject.GetLogicProp("bUpdateOnTimer", "OR", .T.)
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = l_lvlGrp+Padl(m.l_g, 3, "0")
            l_lvlUpdate_grp = l_lvlKey+"." &&mi salvo il livello
            l_g = m.l_g + 1
            l_cCaption = cp_Translate("Aggiorna ogni...")
            l_cProgram = ""
            l_bSkip = !m.l_bIsSelectedGadget Or !l_bIsSelectedUpdateOnTimer 	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
            l_cBmp = "" &&".\bmp\miobitmap.bmp"
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
            *--- Rimuovi tutti
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = l_lvlGrp+Padl(m.l_g, 3, "0")
            l_g = m.l_g + 1
            l_cCaption = cp_Translate("Rimuovi selezionati")
            l_cProgram = cnCmd+"='.oParentObject.RemoveGadgetSelected()'"
            l_bSkip = !m.l_bIsSelectedGadget	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
            l_cBmp = "" &&".\bmp\miobitmap.bmp"
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
            *--- Separatore
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = l_lvlGrp+Padl(m.l_g, 3, "0")
            l_g = m.l_g + 1
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                ("", 0, "", 4, .T., m.l_MenuID, "", m.l_lvlKey, .F., .F.)
            *--- Seleziona/Deseleziona
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = l_lvlGrp+Padl(m.l_i, 3, "0")
            l_g = m.l_g + 1
            If .bSelected
                l_cCaption = cp_Translate("Deseleziona")
                *l_cBmp = ".\bmp\uncheck.bmp"
            Else
                l_cCaption = cp_Translate("Seleziona")
                *l_cBmp = ".\bmp\check.bmp"
            Endif
            l_cProgram = cnCmd+"='.bSelected=!.bSelected'"
            l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
            *--- Seleziona tutti
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = l_lvlGrp+Padl(m.l_g, 3, "0")
            l_g = m.l_g + 1
            l_cCaption = cp_Translate("Seleziona tutti")
            l_cProgram = cnCmd+"='.oParentObject.SetAllProp([bSelected], .T.)'"
            l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
            l_cBmp = "" &&".\bmp\miobitmap.bmp"
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
            *--- Deseleziona tutti
            l_MenuID = m.l_MenuID + 1
            l_lvlKey = l_lvlGrp+Padl(m.l_g, 3, "0")
            l_g = m.l_g + 1
            l_cCaption = cp_Translate("Deseleziona tutti")
            l_cProgram = cnCmd+"='.oParentObject.SetAllProp([bSelected], .F.)'"
            l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
            l_bCheck = .F.  &&.T. checked
            l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
            l_cBmp = "" &&".\bmp\miobitmap.bmp"
            Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)

            *--- Riempio i submenu condivisi tra gadget corrente e gruppo
            *--- Submenu "Temi"
            tmpCur = Sys(2015)
            vq_exec("QUERY\GSUT_MGC.VQR", .Null., m.tmpCur)
            If Used(m.tmpCur)
                Local l_t,l_st,tmpSubCur,aSubLvlKey
                l_t = 0
                l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
                l_bCheck = .F.  &&.T. checked
                l_cBmp = "" &&".\bmp\miobitmap.bmp"
                
                *--- Cerco eventuali sottomenu e li inserisco (Raggruppo i temi se trovo un '-' nella descrizione)
                m.tmpSubCur = Sys(2015)
                Select Distinct Padr(GetWordNum(GCDESCRI,1,'-'),50) as GCDESCRI from (m.tmpCur) Where At('-',GCDESCRI)>0 into cursor (m.tmpSubCur)
                l_nDirectory = 2 &&1 = voce, 2 = submenu, 4 = separatore
                l_cProgram = ""
                Select(m.tmpSubCur)
                Locate For 1=1
                Do While !Eof()
                    *--- se ho almeno una configurazione attivo la voce "Temi"
                    l_MenuID = m.l_MenuID + 1
                    l_t = m.l_t + 1
                    Dimension aSubLvlKey(m.l_t,3) && matrice con le chiavi di livello dei sottomenu
                    l_cCaption = Alltrim(&tmpSubCur..GCDESCRI)
                    aSubLvlKey(m.l_t,1) = Alltrim(m.l_cCaption)
                    *--- Gadget corrente
                    l_lvlKey = l_lvlTheme+Padl(m.l_t, 3, "0")
                    aSubLvlKey(m.l_t,2) = m.l_lvlKey+'.'
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                    *--- Gadget selezionati
                    If m.l_bIsSelectedGadget
                    	l_MenuID = m.l_MenuID + 1
                        l_lvlKey = l_lvlTheme_grp+Padl(m.l_t, 3, "0")
                        aSubLvlKey(m.l_t,3) = m.l_lvlKey+'.'
                        Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                            (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                    Endif
                    Select(m.tmpSubCur)
                    Continue
                Enddo
                Use In Select(m.tmpSubCur)
                
                *--- Inserisco le voci dei temi distribuendole nei sottomenu
                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
                Select(m.tmpCur)
                Locate For 1=1
                Do While !Eof()
                    *--- se ho almeno una configurazione attivo la voce "Temi"
                    l_MenuID = m.l_MenuID + 1
                    l_t = m.l_t + 1
                    l_cCaption = Evl(Alltrim(&tmpCur..GCDESCRI), Alltrim(&tmpCur..GCCODICE))
                    l_i = Iif(At('-',l_cCaption)>0, Ascan(aSubLvlKey,Alltrim(GetWordNum(l_cCaption,1,'-')),1,Alen(aSubLvlKey,1),1,14), 0)
                    *--- Voce da inserire in un sottomenu
                    If l_i>0
                    	m.l_cCaption = Alltrim(SubStr(m.l_cCaption,At('-',m.l_cCaption)+1,Len(m.l_cCaption)))
                    Endif
                    *--- Gadget corrente
                    l_lvlKey = Iif(l_i>0, aSubLvlKey(l_i,2), l_lvlTheme)+Padl(m.l_t, 3, "0")
                    l_cProgram = cnCmd+"=[GSUT_BGG(ThisForm.oParentObject, 'ApplyTheme', ThisForm.Name, .F., '"+&tmpCur..GCCODICE+"')]"
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                    *--- Gadget selezionati
                    If m.l_bIsSelectedGadget
                        l_lvlKey = Iif(l_i>0, aSubLvlKey(l_i,3), l_lvlTheme_grp)+Padl(m.l_t, 3, "0")
                        l_cProgram = cnCmd+"=[GSUT_BGG(ThisForm.oParentObject, 'ApplyTheme', '', .F., '"+&tmpCur..GCCODICE+"')]"
                        l_MenuID = m.l_MenuID + 1
                        Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                            (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                    Endif
                    Select(m.tmpCur)
                    Continue
                Enddo
            Endif
            Use In Select(m.tmpCur)
            Release aSubLvlKey

            *--- Submenu "Aggiorna ogni..."
            *--- Se il gadget corrente oppure uno dei selezionati ha bUpdateOnTimer=.T. aggiungo le voci altrimenti sono disabilitate
            If .bUpdateOnTimer Or m.l_bIsSelectedUpdateOnTimer
                Local l_u
                l_u = 0
                *--- Mai
                l_MenuID = m.l_MenuID + 1
                l_u = m.l_u + 1
                l_cCaption = cp_Translate("Mai")
                l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
                If .bUpdateOnTimer
                    l_lvlKey = l_lvlUpdate+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.nUpdateInterval = 0'"
                    l_bCheck = .nUpdateInterval = 0  &&.T. checked
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                If m.l_bIsSelectedUpdateOnTimer
                    l_MenuID = m.l_MenuID + 1
                    l_lvlKey = l_lvlUpdate_grp+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.oParentObject.SetAllProp([nUpdateInterval], 0, .T.)'"
                    l_bCheck = .F.
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                *-- 5 minuti
                l_MenuID = m.l_MenuID + 1
                l_u = m.l_u + 1
                l_cCaption = cp_Translate("5 Minuti")
                l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
                If .bUpdateOnTimer
                    l_lvlKey = l_lvlUpdate+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.nUpdateInterval = 5*60'"
                    l_bCheck = .nUpdateInterval = 5*60  &&.T. checked
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                If m.l_bIsSelectedUpdateOnTimer
                    l_MenuID = m.l_MenuID + 1
                    l_lvlKey = l_lvlUpdate_grp+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.oParentObject.SetAllProp([nUpdateInterval], 5*60, .T.)'"
                    l_bCheck = .F.
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                *-- 15 minuti
                l_MenuID = m.l_MenuID + 1
                l_u = m.l_u + 1
                l_cCaption = cp_Translate("15 Minuti")
                l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
                If .bUpdateOnTimer
                    l_lvlKey = l_lvlUpdate+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.nUpdateInterval = 15*60'"
                    l_bCheck = .nUpdateInterval = 15*60  &&.T. checked
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                If m.l_bIsSelectedUpdateOnTimer
                    l_MenuID = m.l_MenuID + 1
                    l_lvlKey = l_lvlUpdate_grp+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.oParentObject.SetAllProp([nUpdateInterval], 15*60, .T.)'"
                    l_bCheck = .F.
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                *-- 30 minuti
                l_MenuID = m.l_MenuID + 1
                l_u = m.l_u + 1
                l_cCaption = cp_Translate("30 Minuti")
                l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
                If .bUpdateOnTimer
                    l_lvlKey = l_lvlUpdate+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.nUpdateInterval = 30*60'"
                    l_bCheck = .nUpdateInterval = 30*60  &&.T. checked
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                If m.l_bIsSelectedUpdateOnTimer
                    l_MenuID = m.l_MenuID + 1
                    l_lvlKey = l_lvlUpdate_grp+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.oParentObject.SetAllProp([nUpdateInterval], 30*60, .T.)'"
                    l_bCheck = .F.
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                *-- 60 minuti
                l_MenuID = m.l_MenuID + 1
                l_u = m.l_u + 1
                l_cCaption = cp_Translate("1 Ora")
                l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
                If .bUpdateOnTimer
                    l_lvlKey = l_lvlUpdate+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.nUpdateInterval = 60*60'"
                    l_bCheck = .nUpdateInterval = 60*60  &&.T. checked
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                If m.l_bIsSelectedUpdateOnTimer
                    l_MenuID = m.l_MenuID + 1
                    l_lvlKey = l_lvlUpdate_grp+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.oParentObject.SetAllProp([nUpdateInterval], 60*60, .T.)'"
                    l_bCheck = .F.
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                *-- 240 minuti
                l_MenuID = m.l_MenuID + 1
                l_u = m.l_u + 1
                l_cCaption = cp_Translate("4 Ore")
                l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
                If .bUpdateOnTimer
                    l_lvlKey = l_lvlUpdate+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.nUpdateInterval = 240*60'"
                    l_bCheck = .nUpdateInterval = 240*60  &&.T. checked
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                If m.l_bIsSelectedUpdateOnTimer
                    l_MenuID = m.l_MenuID + 1
                    l_lvlKey = l_lvlUpdate_grp+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.oParentObject.SetAllProp([nUpdateInterval], 240*60, .T.)'"
                    l_bCheck = .F.
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                *-- 480 minuti
                l_MenuID = m.l_MenuID + 1
                l_u = m.l_u + 1
                l_cCaption = cp_Translate("8 Ore")
                l_bSkip = .F.	&&.F. disabilitato, .T. abilitato
                l_nDirectory = 1 &&1 = voce, 2 = submenu, 4 = separatore
                If .bUpdateOnTimer
                    l_lvlKey = l_lvlUpdate+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.nUpdateInterval = 480*60'"
                    l_bCheck = .nUpdateInterval = 480*60  &&.T. checked
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
                If m.l_bIsSelectedUpdateOnTimer
                    l_MenuID = m.l_MenuID + 1
                    l_lvlKey = l_lvlUpdate_grp+Padl(m.l_u, 3, "0")
                    l_cProgram = cnCmd+"='.oParentObject.SetAllProp([nUpdateInterval], 480*60, .T.)'"
                    l_bCheck = .F.
                    Insert Into (m.cCursor) (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ;
                        (m.l_cCaption, 0, m.l_cProgram,m.l_nDirectory, .T., m.l_MenuID, m.l_cBmp, m.l_lvlKey, m.l_bSkip ,m.l_bCheck)
                Endif
            Endif	&&.bUpdateOnTimer OR m.l_bIsSelectedUpdateOnTimer

            loPopupMenu.oPOPUPMENU.InitMenuFromCursor()
            loPopupMenu.oPOPUPMENU.cOnClickProc = "ExecScript(NAMEPROC)"
            loPopupMenu.ShowMenu(cb_GetWindowLeft(.HWnd), cb_GetWindowTop(.HWnd))

            *--- Azioni
  	   LOCAL TestMacro
  	   TestMacro=&cnCmd
            If !Empty(TestMacro)
                Local cmd
                cmd = Evaluate(m.cnCmd)
                &cmd
            Endif

        Endwith
        Release cnCmd
        Select(m.oldarea)

    Endproc

    Procedure oCloseShape.Click()
        Thisform.oParentObject.RemoveGadget(Thisform)

    Procedure oCornerTopLeft.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseDown(m.nButton, m.nShift, m.nXCoord, m.nYCoord)

    Procedure oCornerTopLeft.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseUp(m.nButton, m.nShift, m.nXCoord, m.nYCoord)

    Procedure oCornerTopRight.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseDown(m.nButton, m.nShift, m.nXCoord, m.nYCoord)

    Procedure oCornerTopRight.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseUp(m.nButton, m.nShift, m.nXCoord, m.nYCoord)

    Procedure oCornerBottomRight.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseDown(m.nButton, m.nShift, m.nXCoord, m.nYCoord)

    Procedure oCornerBottomRight.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseUp(m.nButton, m.nShift, m.nXCoord, m.nYCoord)

    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        #Define WM_SYSCOMMAND 0x112
        #Define WM_LBUTTONUP 0x202
        #Define MOUSE_MOVE 0xf012

        If m.nButton = 1&& LMB
            If m.nShift#2
                With Thisform
                    This.MousePointer=5
                    .bMoveMode = .T.
                    .AlwaysOnTop=.T.
                    .oParentObject.OnMouseDownGadget(Thisform)
                    = ReleaseCapture()
                    * Complete left click by sending 'left button up' message
                    = SendMessage(.HWnd, WM_LBUTTONUP, 0x0, 0x0)
                    * Initiate Window Move
                    = SendMessage(.HWnd, WM_SYSCOMMAND, MOUSE_MOVE, 0x0)
                Endwith
            Endif
            If m.nShift=2
                Thisform.bSelected=!Thisform.bSelected
            Endif
        Else
            DoDefault(nButton, nShift, nXCoord, nYCoord)
        Endif
    Endproc

    Procedure MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If m.nButton = 1 		&& LMB
            With Thisform
                This.MousePointer=0
                .oParentObject.OnMouseUpGadget(Thisform)
                .AlwaysOnTop=.F.
                .bMoveMode = .F.
            Endwith
        Else
            DoDefault(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
        Endif
    Endproc

    Procedure RightClick()
    Endproc

    Procedure MouseWheel
        Lparameters nDirection, nShift, nXCoord, nYCoord
		Thisform.oParentObject.MouseWheel(m.nDirection, m.nShift, m.nXCoord, m.nYCoord)
    ENDPROC
    
    Procedure oResizer.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton=1
            With Thisform
                .AlwaysOnTop=.T.
                .oParentObject.OnMouseDownResizeGadget(Thisform)
                .nXCoord=m.nXCoord
                .nYCoord=m.nYCoord
            Endwith
        Endif
    Endproc

    Procedure oResizer.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton=1
            With Thisform
                .AlwaysOnTop=.F.
                .oParentObject.OnMouseUpResizeGadget(Thisform)
            Endwith
        Endif
    Endproc

    Procedure oResizer.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        With Thisform
            If nButton=1
                .Move(.Left,.Top,.Width + (nXCoord - .nXCoord),.Height + (nYCoord - .nYCoord))
                .oParentObject.OnMouseResizeGadget(Thisform)
                .Draw()
                .nXCoord=m.nXCoord
                .nYCoord=m.nYCoord
            Endif
        Endwith
Enddefine

*--- Macro per effetti transizione (le tipologie si possono trovare nella cp_SingleTransition presente nella cp_lib)
#Define TRSDESCRI "Linear"
#Define TRSSELECT "SineEaseOut"

Define Class StdGadgetForm As StdForm
    *--- Form di sistema non modificabile da window menu
    bSysForm = .T.

    TitleBar=0
    BorderStyle=0

	showTips = .F.
	oToolTipTimer = .null. && Timer per la visualizzazione del tooltip sull'header
	cShowToolTipName = ""

    nOldX=0
    nOldY=0
    bMoveMode = .F.
    nOldProc = 0

    nXCoord=0
    nYCoord=0

    nCoordX1 = 0
    nCoordY1 = 0
    nCoordX2 = 0
    nCoordY2 = 0

    Add Object oMoveShape As cp_GadgetGripper
    *---Bordi
    Add Object oLineTop As Line With BorderWidth=1
    Add Object oLineBottom As Line With BorderWidth=1
    Add Object oLineLeft As Line With BorderWidth=1
    Add Object oLineRight As Line With BorderWidth=1

    *--- Dummy
    Add Object _oDummy As CommandButton With Top=-1, Left=-1, Width=1, Height=1, Style=1

    *--- Alert per messaggistica di anomalie di configurazione
    cAlertMsg = ""
    Add Object oAlert As cp_AlertGadget With Top=2, Left=2, Visible = .F.

    *--- Attributi std
    nBackColor = -1	&&Colore sfondo gadget
    nHeaderColor = -1 &&Colore sfondo header gadget
    nBorderColor = -1 &&Colore bordo gadget
    nFooterColor =  -1 &&Colore sfondo footer gadget
    nFontColor =  -1 &&Colore testo sul gadget

    *--- Header
    cHeaderFontName = "Open Sans"
    nHeaderFontSize = 10
    nHeaderFontColor = Rgb(255,255,255)
    bHeaderUpper = .T.
    bHeaderPlus = .T.
    bHeaderValue = .T.

    *-- Footer
    cFooterFontName = "Open Sans"
    nFooterFontSize = 10
    nFooterFontColor = Rgb(255,255,255)

    bSizable = .T.	&&Il gadget pu� essere ridimensionato
    bMouseHover = .T. && Il gadget cambia colore se il mouse ci passa sopra

	bOptionsAccess = .T.	&& Il gadget ha accesso alle opzioni (bottone Accessi su anagrafica gadget)
    bOptions = .T.	&& Il gadget non ha opzioni
    cMskOptions = "" && Se il gadget ha delle opzioni, questo attributo indica l'azione da fare al click del triangolino (top dx)

    bUpdateOnTimer = .F. && Indica se il gadget deve essere aggiornato tramite timer
    nUpdateInterval = 0	&&0 Nessun aggiornamento, 5,10,15,30 min, 1,4,8 ore (sempre espresso in secondi)

    cContextMenu = "" &&Menu contestuale del gadget !!!!

    nPgFrmTimeSwap = 0  &&Intervallo in millisecondi per lo swap automatico delle pagine
    *---*

    *--- Le form devono essere di tipo child (In Top-Level Form)
    ShowWindow = 1

    *--- Propriet� StdForm da disattivare
    *--- Non salvo la posizione della form con il metodo std delle altre form
    bChkPositionForm =.F.
    *--- Nessun menu contestuale
    bNoRightClick = .T.

    *--- Indica se effettuare lo swap delle pagine in modo automatico se nPgFrmTimeSwap<>0, utilizzato dal menu contestuale option
    bAutoPageSwap = .T.

    *--- Propriet� ad uso interno
    *--- Chiave identificativa ad uso interno
    cKeyGadget = ""
    *--- Testo da mostrare come tooltip sul titolo del gadget
    cToolTipGadget = ""
    *--- Datetime ultimo aggiornamento
    tLastUpdate = {//,}
    *--- Gestione collapse/expand gadget
    nCollapsedHeight = 24 &&Altezza gadget quando � collassato
    bExpanded = .T. &&Indica se il gadget � espanso, variando la propriet� il gadget viene ridimensionato
    nExpandedHeight = This.Height &&Indica l'altezza del gadget espanso
    bExpandedOld = .T.	&&Salva lo stato di bExpanded quando si entra in modifica
    bDisabledResize = .F. &&Disabilita il resize durante Expanded/Collapsed
    bInitCollapsed = .F. && Indica che il gadget deve essere collassato all'apertura

    *--- Gadget aziendale
    bCompany = .F.	&&Se true il gadget � aziendale quindi gli attributi sono personalizzabile per azienda

    nPage = 1 	&&Pagina del gadget
	cSourceGroup = "" &&Codice gruppo di originale, utilizzato nello "sposta nell'area"
	cSourceGroupDes = "" &&Descrizione gruppo di originale, utilizzato nello "sposta nell'area"
	
    bEventArranged = .F.	&&Forza l'evento GadgetArranged quando il gadget � al suo posto

    bSelected=.F.	&&Indica se il gadget � selezionato (in modalit� modifica)
    
    bMaximize = .F. && indica se il gadget pu� essere massimizzato
    nMaximizePage = 0 && indica la pagina del gadget da mostrare quando viene massimizzato
    nMinimizePage = 0 && indica la pagina del gadget da mostrare quando viene minimizzato
    nMaximizeState = 0 && 0: Minimizzato - 1: Massimizzato ma mouse non ancora sul gadget - 2: Massimizzato e mouse sul gadget

    *--- Eventi
    Procedure Moved
        If This.bMoveMode
            This.oParentObject.OnMouseMoveGadget(This.Left, This.Top, This)
        Endif
    Endproc

    Proc Activate()
        *--- Ridefinita per non essere eseguita

    Proc Deactivate()
        *--- Ridefinita per non essere eseguita

    Procedure Resize()
        If !This.bDisabledResize
            This.oPgFrm.Width = This.Width
            This.oPgFrm.Height = This.Height - Iif(Pemstatus(This, "oHeader",5), This.oHeader.Height,0)
            cp_ResizeContainer(This.oPgFrm, This.nPageResizeContainer)
        Endif
    Endproc

    Proc nPgFrmTimeSwap_Assign(nValue)
        If Pemstatus(This, 'oPgFrm', 5) And !Isnull(This.oPgFrm) And Pemstatus(This.oPgFrm, 'nTimeSwap', 5)
            This.nPgFrmTimeSwap = m.nValue
        Endif
    Endproc

    Procedure bAutoPageSwap_Assign(nValue)
        This.bAutoPageSwap = m.nValue
        If m.nValue
            This.oPgFrm.nTimeSwap = This.nPgFrmTimeSwap
        Else
            This.oPgFrm.nTimeSwap = 0
        Endif
    Endproc

    *--- Metodi
    Procedure ModifyMode(bActive)
        *--- Notifico il passaggio in modalit� di modifica
        If m.bActive
            This.NotifyEvent('GadgetModifyModeON Init')
        Else
            This.NotifyEvent('GadgetModifyModeOFF Init')
            *--- Se esco dalla modifica rimetto non selezionato
            This.bSelected = .F.
        Endif

        This.oMoveShape.Visible=m.bActive
        This.oMoveShape.ShowGripper(.F.)
        Local nNewBackColor
        If m.bActive
            If GetCapture() == This.HWnd
                ReleaseCapture()
            Endif
            nNewBackColor = RGBAlphaBlending(This.nBackColor, Rgb(255,255,255), 0.2)
            *--- Gestione bExpanded
            This.bExpandedOld = This.bExpanded
            This.bExpanded = .T.
            If This.oPgFrm.nTimeSwap # 0
                This.oPgFrm.oTimer.Interval = 0
                This.oPgFrm.oTimer.Enabled = .F.
            Endif
        Else
            nNewBackColor = This.nBackColor
            This.bExpanded = This.bExpandedOld
            If This.oPgFrm.nTimeSwap # 0 AND This.bExpanded
                *--- Faccio ripartire il timer per swap delle pagine
                This.bAutoPageSwap = This.bAutoPageSwap
            Endif
        Endif
        *--- Transizione colore
        *cp_SingleTransition(Thisform, "BackColor", m.nNewBackColor, "C", 200, "Linear")
        Thisform.BackColor=m.nNewBackColor

        *--- Notifico il passaggio in modalit� di modifica
        If m.bActive
            This.NotifyEvent('GadgetModifyModeON End')
        Else
            This.NotifyEvent('GadgetModifyModeOFF End')
        Endif
    Endproc

    Procedure ShowGripper(bShow)
        This.oMoveShape.ShowGripper(m.bShow)

    Procedure Declare()
        Declare Integer GetWindowLong In Win32API ;
            integer HWnd, Integer nIndex
        Declare Integer CallWindowProc In Win32API ;
            integer lpPrevWndFunc, Integer HWnd, Integer Msg, ;
            integer wParam, Integer Lparam
        Declare RtlMoveMemory In WIN32API As Heap2String;
            String @ Destination, Integer Source,;
            Integer nLength
        Declare SetRect In WIN32API Integer lpRect, ;
            Integer X1, Integer Y1, Integer X2,Integer Y2
        Declare Integer GetCapture In WIN32API
    Endproc


    *--- Effettua una transizione del colore di sfondo del gadget per attirare l'attenzione dell'utente
    Procedure Highlight(nColor, nNum, nTransitionTime, bChange, cTmethod)
        *--- nColor: Nuovo colore
        *--- nNum: Numero di flash
        *--- nTransitionTime: Durata flash
        With This
            Local nOldBackColor, i, nTime
            cTmethod = Evl(m.cTmethod, "QuarticEaseOut")
            nNum = Evl(m.nNum, 1)
            nTime = Evl(m.nTransitionTime, 200)/(2*(m.nNum-1) + Iif(!m.bChange, 1,2))
            nOldBackColor = .BackColor
            For m.i = 1 To m.nNum-1
                cp_SingleTransition(Thisform, "BackColor", m.nColor , "C", m.nTime, m.cTmethod, .T.)
                cp_SingleTransition(Thisform, "BackColor", m.nOldBackColor , "C", m.nTime, m.cTmethod, .T.)
            Next
            cp_SingleTransition(Thisform, "BackColor", m.nColor , "C", m.nTime, m.cTmethod, !m.bChange)
            If !m.bChange
                cp_SingleTransition(Thisform, "BackColor", m.nOldBackColor, "C", m.nTime, m.cTmethod)
            Endif
        Endwith
    Endproc

    *--- Collapse/Expand gadget
    Procedure bExpanded_Assign(xValue)
        With This
            If .bExpanded # m.xValue
                Local nNewHeight
                .bExpanded = m.xValue
                If .bExpanded
                    *--- Expand
                    m.nNewHeight = .nExpandedHeight
		            If .oPgFrm.nTimeSwap # 0
		                *--- Faccio ripartire il timer per swap delle pagine
		                .bAutoPageSwap = .bAutoPageSwap
		            Endif                    
                Else
                    *--- Collapse
		            If .oPgFrm.nTimeSwap # 0
		                .oPgFrm.oTimer.Interval = 0
		                .oPgFrm.oTimer.Enabled = .F.
		            Endif                    
                    .nExpandedHeight = .Height
                    m.nNewHeight = .nCollapsedHeight
                Endif
                cParam = Transform(.Height)+","+Transform(m.nNewHeight)+",.T."
                cp_SingleTransition(Thisform, "_Accordion", m.cParam , "M", 300, "QuarticEaseOut", .T.)
            Endif
        Endwith
    Endproc

    *--- Gestione animazione del collassamento/espansione
    Procedure _Accordion(oTrs, nPercentage, nHeightStart, nHeightEnd, bNoResize)
        Local nHeight, oldMoveMode
        nHeight = m.oTrs.Interpolate(m.nHeightStart, m.nHeightEnd, m.nPercentage)
        With This
            oldMoveMode = .bMoveMode
            .bMoveMode = .T.
            .bDisabledResize = m.bNoResize
            .Move(.Left, .Top, .Width, m.nHeight)
            .oParentObject.OnMouseResizeGadget(Thisform)
            .oParentObject.Arrangegadget(Thisform.Name, .T.)
            .bMoveMode = m.oldMoveMode
            .bDisabledResize = .F.
        Endwith
        oTrs = .Null.
    Endproc
    *---*

    Procedure bSelected_Assign(xValue)
        With This
            .bSelected = m.xValue
            .oMoveShape.ShowSelGripper()
        Endwith
    Endproc

	*--- Triangoli sull'header per massimizzazione
	Procedure bMaximize_Assign(xValue)
        With This
            .bMaximize = m.xValue
            If PemStatus(This,'oHeader',5)
            	.oHeader.setMaximize(m.xValue)
            Endif
        Endwith
    Endproc

    Proc Init()
        With This
            If Not(.OpenWorkTables())
                cp_msg(cp_Translate(MSG_CANNOT_OPEN_ALL_TABLES),.F.)
            Else
                If Type("i_bFox26")='L'
                    If i_bFox26
                        .bFox26=.T.
                    Endif
                Endif
                cp_GetSecurity(This, .getSecurityCode())
                If !(.bSec1)
                    cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
                    .CloseAllTables()
                    .Release()
                    Return
                Endif
                If !.CanAccess()
                    .CloseAllTables()
                    .Release()
                    Return
                Endif
                .Name=.Name+Sys(2015)
                basename=.Name                        && alternativamente si puo' usare 'sys(2015)'
                Public frm&basename
                frm&basename = This
                * --- definisce il nome per il suo cursore
                .cCursor=Sys(2015) &&'cur'+basename
                .cKeySet=Sys(2015) &&'Key'+basename
                * ---
                .GetFirstControl()
                Local i
                For i=1 To .oPgFrm.PageCount-1
                    .oPgFrm.Pages(m.i).oPag.oContained=This
                Next
                * --- run-time configuration
                .oRunTime=Createobject('RunTimeConfigurationAgent',This)
                If Vartype(bLoadRuntimeConfig)='U' Or m.bLoadRuntimeConfig
                    .oRunTime.LoadConfig()
                Endif
                * ---
                .NotifyEvent('FormLoad')
                .CreateChildren()
                *-- Zucchetti Aulla Inizio: Gestione ancoraggio oggetti
                Cp_SetAnchorForm(This)
                *-- Zucchetti Aulla Fine: Gestione ancoraggio oggetti
                *  --- la posizione ?
                *This.SetPosition()
                If i_VisualTheme<>-1
                    Local pg, l_i
                    For l_i=1 To .oPgFrm.PageCount
                        pg = .oPgFrm.Pages[m.l_i]
                        pg.oPag.Move(pg.oPag.Left, pg.oPag.Top, pg.Width, pg.Height)
                    Next
                    pg = .Null.
                Endif
                *--- Problemi con alcuni resize e zoom integrati
                .Resize()
                .mHideControls()
                If .WindowType=0
                    .ScrollBars=0
                Endif
                .QueryKeySet("1=0","")          && Where inizialmente nulla

                *--- Colore random
                .nBackColor = m.oParentObject.GetColorRandom()

                .ecpQuery()
                .NotifyEvent('Init')
            Endif

            .Declare()

            *--- Posso spostare il gadget solo all'interno della gadgetmanager
            .nOldProc = GetWindowLong(.HWnd, GWL_WNDPROC)
            Bindevent(.HWnd, WM_MOVING, This, "EventHandler")
            Bindevent(.HWnd, WM_MOUSEMOVE, This, "EventHandler")

            CPForm::Init()&&DoDefault()
            .SetBorderLine()
            .SetMoveShape()
            .MinWidth = .oParentObject.nMinGadgetWidth
            .MinHeight = .oParentObject.nMinGadgetHeight
        Endwith
        Return

    Procedure SetMoveShape()
        With This
            .oMoveShape.Anchor=0
            .oMoveShape.ChangeTheme()
            .oMoveShape.ZOrder()
            .oMoveShape.Move(0, 0, .Width, .Height)
            .oMoveShape.Anchor=15
        Endwith
    Endproc

    Procedure SetBorderLine()
        With This
            Local nColor
            nColor = Iif(Thisform.nBorderColor=-1, RGBAlphaBlending(Thisform.BackColor, Rgb(255,255,255), 0.15), Thisform.nBorderColor)
            .oLineTop.BorderColor = m.nColor
            .oLineBottom.BorderColor = .oLineTop.BorderColor
            .oLineRight.BorderColor = .oLineTop.BorderColor
            .oLineLeft.BorderColor = .oLineTop.BorderColor

            .oLineTop.Move(0, 0, .Width, 0)
            .oLineBottom.Move(0, .Height-1, .Width, 0)
            .oLineRight.Move(.Width-1, 0, 0, .Height)
            .oLineLeft.Move(0, 0, 0, .Height)
            .oLineTop.Anchor = 10
            .oLineBottom.Anchor = 14
            .oLineRight.Anchor = 13
            .oLineLeft.Anchor = 7

            .oLineTop.ZOrder()
            .oLineBottom.ZOrder()
            .oLineRight.ZOrder()
            .oLineLeft.ZOrder()
        Endwith
    Endproc

    Procedure Eventhandler(HWnd, Msg, wParam, Lparam)
        Do Case
            Case m.Msg = WM_MOUSEMOVE
                With This
                    If  This.bMouseHover And !This.oMoveShape.Visible And !Wexist("Debugger") And !Wvisible("Command")
                        Local nTime
                        nTime = 200
                        SetCapture(m.HWnd)
                        If Mcol(.Name, 3)=-1 Or Mrow(.Name,3) = -1
                            *--- Fuori dalla form
                            If GetCapture() == m.HWnd
                                ReleaseCapture()
                            Endif
                            This.oParentObject.oHighLightGadget.Visible=.F.
                        Else
                            *--- Dentro il form
                            This.oParentObject.oHighLightGadget.Move(.Left-3,.Top-3,.Width+6, .Height+6)
                            This.oParentObject.oHighLightGadget.Visible=.T.
                        Endif
                    Endif
                    If Type("This.nPgFrmTimeSwap")='N' And .nPgFrmTimeSwap # 0 And !This.oMoveShape.Visible
                        SetCapture(m.HWnd)
                        If Mcol(.Name, 3)=-1 Or Mrow(.Name,3) = -1
                            *--- Fuori dalla form
                            If GetCapture() == m.HWnd
                                ReleaseCapture()
                            Endif
                            *--- Faccio ripartire il timer, se � espanso
                            IF .bExpanded
	                            .bAutoPageSwap = .bAutoPageSwap
	                        EndIf
                        Else
                            *--- Dentro il form
                            .oPgFrm.oTimer.Interval = 0
                            .oPgFrm.oTimer.Enabled = .F.
                        Endif
                    Endif
                	*--- Massimizzazione
                	If .bMaximize And .nMaximizeState>0
                        SetCapture(m.HWnd)
                        If Mcol(.Name, 3)=-1 Or Mrow(.Name,3) = -1
                            *--- Fuori dalla form
                            If GetCapture() == m.HWnd
                                ReleaseCapture()
                            Endif
                            *--- Minimizzo se massimizzato e sono uscito dal gadget
                            If .nMaximizeState=2
	                            .onMaximize(.F.)
                            Endif
                        Else
                            *--- Dentro il form
							.nMaximizeState = 2
                        Endif
                    Endif
                	*--- ToolTip
                    cBuffer = Replicate(Chr(0), 8)
        			GetCursorPos(@cBuffer)
                    If WindowFromPoint(CToBin(Substr(m.cBuffer,1,4), "4RS"), CToBin(Substr(m.cBuffer,5,4), "4RS")) # m.HWnd
                            *--- Fuori dalla form
	                        If GetCapture() == m.HWnd
	                            ReleaseCapture()
	                        Endif
	                        *--- Disattivo il timer per il tooltip
	                        If Vartype(This.oToolTipTimer)='O'
	                        	This.oToolTipTimer.Enabled = .F.
	                        Endif
	                        This.oToolTipTimer = .null.
                    Else
                        *--- Dentro il form
						*--- Attivo il timer per il tooltip se non � attualmente mostrato
                        If !This.oParentObject.oBalloon.ctlVisible
                            If Vartype(This.oToolTipTimer)='O'
                            	This.oToolTipTimer.Enabled = .T.
                            Else
                            	This.oToolTipTimer = Createobject("ToolTipGadgetTimer")
                            Endif
                        Endif
                    EndIf
                Endwith
            Case m.Msg = WM_MOVING
                If This.bMoveMode
                    lpRect= Space(16)
                    Heap2String(@lpRect, m.lParam, 16)
                    X1 = CToBin(Substr(m.lpRect, 1,4), "4SR")
                    Y1 = CToBin(Substr(m.lpRect, 5,4), "4SR")
                    X2 = CToBin(Substr(m.lpRect, 9,4), "4SR")
                    Y2 = CToBin(Substr(m.lpRect, 13,4), "4SR")
                    lpRectParent = Space(16)
                    GetWindowRect(This.oParentObject.HWnd, @lpRectParent)
                    ParentX1 = CToBin(Substr(m.lpRectParent, 1,4), "4SR")
                    ParentY1 = CToBin(Substr(m.lpRectParent, 5,4), "4SR")
                    ParentX2 = CToBin(Substr(m.lpRectParent, 9,4), "4SR")
                    ParentY2 = CToBin(Substr(m.lpRectParent, 13,4), "4SR")
                    X1=Min(Max(m.X1, m.ParentX1), m.ParentX2-This.Width)
                    SetRect(m.lParam, m.X1, m.Y1, m.X1+This.Width, m.Y2)
                Endif
        Endcase
        Return CallWindowProc(This.nOldProc, m.HWnd, m.Msg, m.wParam, m.lParam)
    Endproc

    *--- Apertura Opzioni
    Proc GadgetOption()
        Local cPrg,nExprIdx,cProp,cExprProp
        With This
            If !Empty(.cMskOptions)
                cPrg = .cMskOptions
                &cPrg(This)
            	*--- Rivaluto eventuali propriet� parametriche
            	AMembers(aProps,This)
            	m.nExprIdx = Ascan(aProps,'EXPR_',1,Alen(aProps),1,1)
            	Do while m.nExprIdx<>0
            		m.cExprProp = aProps(m.nExprIdx)
            		m.cProp = Strtran(m.cExprProp,"EXPR_","",1,1,1)
            		.&cProp = Eval(.&cExprProp )
            		aProps(m.nExprIdx) = ""
            		m.nExprIdx = Ascan(aProps,'EXPR_',1,Alen(aProps),1,1)
            	Enddo
            Endif
        Endwith
    Endproc

    *--- Applica modifiche definite dagli attributi
    Proc ApplyTheme()
        Local cPrg
        With This
            *--- Standard
            .BackColor = .nBackColor
            .SetBorderLine()
            .SetMoveShape()

            *--- Header
            If Pemstatus(This, "oHeader", 5)
                .oHeader.nBackColor = .nHeaderColor
                .oHeader.FontColor = .nHeaderFontColor
                .oHeader.FontName = .cHeaderFontName
                .oHeader.FontSize = .nHeaderFontSize
                .oHeader.bUpper = .bHeaderUpper
                .oHeader.bPlus = .bHeaderPlus
                .oHeader.bShowValue = .bHeaderValue
            Endif

            *--- Footer
            If Pemstatus(This, "w_oFooter", 5)
                .w_oFooter.nBackColor = .nFooterColor
                .w_oFooter.FontColor = .nFooterFontColor
                .w_oFooter.FontName = .cFooterFontName
                .w_oFooter.FontSize = .nFooterFontSize
            Endif
        Endwith
    Endproc

    *--- Gestione eventi
    Procedure NotifyEvent(cEvent)
        *DODEFAULT(cEvent)
    	*--- Zucchetti Aulla - Unbind resize FoxChart
        IF !Thisform.bNoFoxCharts AND m.cEvent=='Done'
			UnbindEventsEx(Thisform.HWnd, WM_ENTERSIZEMOVE)
		    UnbindEventsEx(Thisform.HWnd, WM_EXITSIZEMOVE)  
		EndIf        	
	    *--- Zucchetti Aulla - Unbind resize FoxChart        
        If m.cEvent=="GadgetArranged"
            *--- Alla gadget arranged faccio partire l'eventuale swap delle pagine
            This.bAutoPageSwap = This.bAutoPageSwap
            If !Isnull(This.oParentObject.oGadgetLibrary)
                *--- Se le library � aperta fermo i timer
                This.oPgFrm.oTimer.Interval = 0
                This.oPgFrm.oTimer.Enabled = .F.
            Endif
        	*--- Metto l'Alert sopra tutto
        	This.oAlert.ZOrder()
        	This.SetMoveShape()
        Endif
    Endproc

	PROCEDURE SetStatus()
	  *--- Ridefinita per non essere eseguita
	EndProc

	Proc onMaximize(bOpen)
		With This
			If .bMaximize And Vartype(m.bOpen)='L' And (.nMaximizeState=0 and m.bOpen Or !m.bOpen)
				Local l_oldArea,l_Left,l_Top,l_Width,l_Height,cEvent 
				*--- Gadget minimizzato e richiesta di massimizzazione
				If .nMaximizeState=0 and m.bOpen
					.NotifyEvent("GadgetMaximize Init")
					Local lpRect,lpRectParent,Y1,ParentY1,Y2,ParentY2
					*--- Left
					m.l_Left = .oParentObject.nMarginLeft
					*--- Top
					m.l_Top = .oParentObject.nMarginTop
                	lpRect= Space(16)
	                GetWindowRect(.oParentObject.nHwndClientScroll, @lpRect)
	                Y1 = CToBin(Substr(m.lpRect, 5,4), "4SR")
	                Y2 = CToBin(Substr(m.lpRect, 13,4), "4SR")
	                lpRectParent = Space(16)
	                GetWindowRect(.oParentObject.nHwndClient, @lpRectParent)
	                ParentY1 = CToBin(Substr(m.lpRectParent, 5,4), "4SR")
	                ParentY2 = CToBin(Substr(m.lpRectParent, 13,4), "4SR")
	                *--- Se il Cnt � stato scrollato devo abbassare il gadget
	                If m.ParentY1>m.Y1
	                	m.l_Top = m.l_Top+(m.ParentY1-m.Y1)
	                Endif
					*--- Width
					m.l_Width = .oParentObject.Width-(2*.oParentObject.nMarginLeft)
					If !Isnull(.oParentObject.oCharmBar)
						If .oParentObject.oCharmBar.bPin
							m.l_Width = m.l_Width-.oParentObject.oCharmBar.oPgFrm.Page1.oPag.oBox_1_2.Width
						Else
							m.l_Width = m.l_Width-.oParentObject.oCharmBar.oPgFrm.Page1.oPag.oBox_1_1.Width+1
						Endif
					Endif
					*--- Height
					m.l_Height = .oParentObject.Height-(2*.oParentObject.nMarginTop)
					m.l_Height = m.l_Height+Iif(!Isnull(.oParentObject.oTabPage), .oParentObject.oTabPage.Height, 0)
					
					*--- Se il gadget non deve andare a tutto schermo lo centro
					If .MaxWidth>=0
						m.l_Left = Max(m.l_Left, m.l_Left + (m.l_Width-.MaxWidth)*0.5)
					Endif
					If .MaxHeight>=0
						m.l_Top = Max(m.l_Top, m.l_Top + (m.l_Height-.MaxHeight)*0.5)
					Endif
					
					.AlwaysOnTop = .T.
					m.cEvent = "GadgetMaximize End"
				Else&& Gadget massimizzato e richiesta di minimizzazione
					.NotifyEvent("GadgetMinimize Init")
					*--- Recupero posizione e valori originari dal cursore
					m.l_oldArea = Select()
					Select(.oParentObject.cCursorGadget)
					Locate for Alltrim(GadgetID)=Alltrim(.Name)
					m.l_Left = X1
					m.l_Top = Y1
					m.l_Width = X2-X1
					m.l_Height = Y2-Y1
					Select(m.l_oldArea)
					
					.AlwaysOnTop = .F.
					m.cEvent = "GadgetMinimize End"
				Endif
				If !.oParentObject.bArrangedFast
					Local nTime
					m.nTime = 350
					*--- Fermo l'eventuale swap delle pagine
					If .oPgFrm.nTimeSwap # 0
                		.oPgFrm.oTimer.Interval = 0
                		.oPgFrm.oTimer.Enabled = .F.
            		Endif
					*--- transizioni per massimizzazione
	                tm = Createobject("cp_TransitionManager")
	                tm.Add(This, "Left", m.l_Left, 'N', m.nTime, TRSSELECT)
	                tm.Add(This, "Top", m.l_Top, 'N', m.nTime, TRSSELECT)
	                tm.Add(This, "Width", m.l_Width, 'N', m.nTime, TRSSELECT)
	                tm.Add(This, "Height", m.l_Height, 'N', m.nTime, TRSSELECT)
	                tm.Start(.T.)
				Else
					.Move(m.l_Left,m.l_Top,m.l_Width,m.l_Height)
				Endif
				*--- Nuovo stato del gadget
				*--- Cambio della pagina
				*--- Disattivo lo swap automatico se massimizzato e lo riattivo se minimizzato
				If .nMaximizeState=0
					.nMaximizeState=1
					.nMinimizePage = .oPgFrm.ActivePage
					If .nMaximizePage>0 And .nMaximizePage<=.oPgFrm.PageCount And .nMaximizePage<>.oPgFrm.ActivePage
						.oPgFrm.ActivePage = .nMaximizePage
					Endif
				Else
					.nMaximizeState=0
					If .nMinimizePage>0 And .nMinimizePage<=.oPgFrm.PageCount And .nMinimizePage<>.oPgFrm.ActivePage
						.oPgFrm.ActivePage = .nMinimizePage
					Endif
					.bAutoPageSwap = .bAutoPageSwap
				Endif
				*--- Problema resize label su header e footer: forzo il resize del gadget
				.Width = .Width + 1
				.Width = .Width - 1
				.NotifyEvent(m.cEvent)
			Endif
		Endwith
	EndProc
	
	Proc AddMenu(loPopupMenu)
		*-- Aggiunge voci specifiche per gadget al men� header
	EndProc

	*--- Alert
	Proc cAlertMsg_Assign(xValue)
		If Vartype(m.xValue)='C' And !(Alltrim(This.cAlertMsg)==Alltrim(cp_Translate(m.xValue)))
			This.cAlertMsg = Alltrim(cp_Translate(m.xValue))
		This.oAlert.Visible = !Empty(This.cAlertMsg)
		This.oAlert.ToolTipText = This.cAlertMsg
		If PemStatus(This,"oHeader",5)
			This.oHEader.AdjustLabel()
			Endif
		Endif
	EndProc

Enddefine

*--- Zucchetti Aulla Fine - Gadget

Define Class StdForm As CPForm
    *
    * --- PROPRIETA' VFP
    *
    WindowType = 0                    && 1 = Modale
    *BackColor = rgb(192,192,192)
    ShowTips=.T.
    *scrollbars=IIF(TYPE('i_nFormScrollBars')='U',3,i_nFormScrollBars)
    cOldBtn=""
    * --- Zucchetti Aulla Inizio Gestione dati caricato da, alla data..
    cCalUtd='A'
    * --- Zucchetti Aulla Inizio Gestione dati caricato da, alla data..
    bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
    bNoMenuAction=.F.         	&& Disabilita nel tasto destro sottomenu Azioni
    bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
    bNoMenuAutorizaction=.F.		&& Disabilita nel tasto destro sottomenu Autorizzazioni
    bTrsDontDisplayErr=.F.        && Permette di non mostrare messaggi di eerore se fallisce transazione
    bNoRightClick = .F. && Zucchetti Aulla disabilita tasto dx
    *** Zucchetti Aulla - Modulo BCON
    oCtrlGruppo = .Null.
    oCtrlImpresa = .Null.
    *** Zucchetti Aulla - Modulo controllo flussi
    cFlowStamp = Space(10)			  && sys(2015) aggiornato su F10 e F5
    bFlowNoRules = .F.				  && se .t. esce subito dalla chiamata
    *** Zucchetti Aulla Fine
    *
    * --- PROPRIETA' CODEPAINTER
    *
    cPrg = ""
    cComment = ""
    *
    cFunction = ""                    && Funzione corrente: Query,Filter,Edit,Load
    cFile = ""
    cCursor = ""                      && nome del cursore con i risultati della query
    cCursorTrs = ""                   && nome del cursore con le righe ottenute dalla query
    *cSaveCursor=""
    *cSaveCursorTrs=""
    cTrsName=""                       && nome del cursore transitorio
    cRowID=""                         && identificativo della riga del padre a cui e' associata questa gestione (se figlia)
    *bCreateSaveCursor=.t.
    cKeySet = ""                      && Nome del Cursore di sole chiavi
    cKeySelect = ""                   && Nomi dei campi della chiave 1 per select
    cKeyWhere = ""                    && Nomi dei campi della chiave 1 per where
    cKeyDetail = ""                   && Nomi dei campi della chiave 1 per where del detail
    cLastWhere = ""                   && Salvataggio ultima where
    cLastOrderBy = ""                 && Salvataggio ultimo Order By
    cQueryFilter = ""                 && Filtro in fase di query
    bLoadRecFilter=.F.                && Se attivo negli oggetti di tipo detail e master/detail viene applicato il filtro sicurezza record anche in fase di load
    *
    bSec1=.T.                         && primo parametro di sicurezza (accesso)
    bSec2=.T.                         && secondo parametro di sicurezza (inserimento)
    bSec3=.T.                         && terzo parametro di sicurezza (modifica)
    bSec4=.T.                         && quarto parametro di sicurezza (cancellazione)
    *
    bUpdated=.F.                      && indica se il form e' stato modificato
    bHeaderUpdated=.F.                && indica se la testata del form e' stata modificata
    Dimension xKey[1]
    Dimension cWorkTables[1]
    nOpenedTables=0
    bLoaded = .F.                     && indica se i dati sono stati caricati da tabelle nel database
    bOkToTerminate=.F.
    bDontReportError=.F.              && indica di non visualizzare l' eventuale messaggio di errore (es. lancio di uno zoom, o ESC)
    oFirstControl = .Null.            && il control su cui portare il focus all' uscita dall' Editing
    bPostIt=.T.                       && indica se sono gestiti i post-in
    nPostItConn=0                     && la connessione dove trovare i post-in
    nWarn=0                           && il numero di post-in associati al record corrente
    Dimension cWarn[1]
    bFirstQuery=.T.                   && e' la prima query?
    nFirstRow=0                       && indica la prima riga del tranditorio (puo' non essere 1 se vengono cancellate le prime righe)
    nLastRow=0                        && ultima riga del transitorio
    i_nRowNum=0                       && ultimo numero utilizzato per CPROWNUM (non coincide con il numero di riga se sono state cancellate delle righe)
    cAutoZoom=''                      && configurazione di zoom da usarsi in autozoom
    Caption=''
    infodaterev='?'
    _paint_error_s=0                  && VFP a volte scatena il metodo PAINT di continuo, rallentando la gestione (segnalato a Microsoft, ma non corretto)
    _paint_error_c=0                  && queste due variabili forzano una Refresh() che risolve il problema
    _bPaint_ErrorGrid=.F.			  && Gestione Paint ed elenchi
    nLoadTime=0                       && timestamp di quando e' stato caricato un record, per ricaricare i dati troppo vecchi
    bOnScreen=.T.                     && indica se un figlio e' visibile o meno
    bSaving=.F.                       && indica che e' in corso una operazione di salvataggio
    bDeleting=.F.                     && indica che e' in corso una operazione di cancellazione
    bCalculating=.F.                  && indica che e' in corso la routine di ricalcolo
    nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
    * Zucchetti Aulla gestione visualizzazione dimensione fissa form inizio
    bOnInit = 0					&& Maschera in inizializzazione
    bSizeForm = .F.				&& Indica se la form � sizabile
    bCheckSize = .F.			&& Indica se � gi� stato verificato che la form � sizabile
    * Zucchetti Aulla gestione visualizzazione dimensione fissa form fine

    * --- oggetto fittizio su cui viene portato il focus all' F10, per essere certi di aver scatenato la Valid() del campo corrente
    Add Object __dummy__ As CommandButton With Enabled=.F.,Width=0,Height=0,Style=0,SpecialEffect=1
    *ADD OBJECT __tb__ as commandbutton WITH visible=.f.,width=20,height=10,caption="..."
    Add Object __sqlx__ As sqlx       && oggetto di gestione della query asincrona in zoom
    * --- configurazione al run-rime
    cRuntimeConfiguration=''          && configurazione di run-time corrente
    oRunTime=.Null.                   && gestore della configurazione
    bCurPos=0 && identifica il numero record da "marcare" sul quale riposionarsi
    bAbsRow=0 && identifica la variabile di stato corrispondente sul body
    bRelRow=0 && identifica la variabile di stato corrispondente sul body
    nMarkPos=0 && Conta i Markpos per evitare di riposizionarsi se gia invocato MarkPos senza corrispondente Repos
    currentEvent=''
    bFox26=.F.
    * Zucchetti Aulla - Inizio - Visualizzazione query InfoPublisher su gestione
    * --- Nome del cursore che contiene l'elenco delle query infopublisher associate alla gestione
    cCurInfo=""
    bCurForm=.T.	&&.T. = il cursore � generato dalla form; .F. = il cursore � generato dallo zoom
    *
    *---  Configurazione gestione
    nCfgGest=0
    *--- Zucchetti Aulla Fine - Configurazione gestione

    bTBMDI = .F.	&&Indica se il form � soggetto a TBMDI
    nPrevWndFunc = 0		&&Memorizzo handle per usarlo in CallWindowProc
    bSecDataFilter = .F. && Se .t. attiva la modalit� di raccolta informazioni per costruire il filtro

    *--- Zucchetti Aulla Inizio - Refresh controlli in apertura maschera
    nOldLeft = 0
    *--- Zucchetti Aulla Fine - Refresh controlli in apertura maschera
    *--- Disabilita il gotfocus dei tab delle pagine, evita anomalie con CTRL+F6
    bGotFocusTab=.T.
    * --- COSTRUTTORE/DISTRUTTORE
    * --- unica pagina su cui fare resizecontainer
    nPageResizeContainer=0
    * --- Posizione originale della form
    TopOrig=0
    HeightOrig=0
    LeftOrig=0
    WidthOrig=0
    bPostionChange=.F.
    bChkPositionForm = .T.

    *--- Attiva/Disattiva il decoratore delle form
    bApplyFormDecorator = .T.
    *--- Serve per norificare l'evento resize sui grafici solo al termine della resize della form
    bEndResize=.t.
    bNoExistFoxCharts=.t.
    bNoFoxCharts=.t.
    * --- Inizializzazione
    Proc Init()
        Local basename,i,fo
        If Val(Os(3))>5
            This.Icon=This.Icon
        Endif
        * --- Ripristino valori Label Key per maschere modali lanciate da tasto destro
        If Type('g_Omenu.oKey')<>'U'
            g_oMenu.gestPopup('A')
            g_oMenu.bRecLabel=.F.
        Endif
        If Not(This.OpenWorkTables())
            cp_msg(cp_Translate(MSG_CANNOT_OPEN_ALL_TABLES),.F.)
        Else
            If Type("i_bFox26")='L'
                If i_bFox26
                    This.bFox26=.T.
                Endif
            Endif
            * --- Zucchetti Aulla Segnalo attivit� sessione
            If Vartype(g_CHKACTFORM)='C' And g_CHKACTFORM='S' And Vartype(g_LASTVERACT)='N'
                i_nChkSeconds=Seconds()
                If (i_nChkSeconds-g_LASTVERACT<0 Or i_nChkSeconds-g_LASTVERACT>10*60)
                    chkzuusr(.Null.,'ACTIVE')
                Endif
            Endif
            * ---
            cp_GetSecurity(This,This.getSecurityCode())
            If !(This.bSec1)
                cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
                This.CloseAllTables()
                This.Release()
                Return
            Endif
            If !This.CanAccess()
                This.CloseAllTables()
                This.Release()
                Return
            Endif
            If Type('i_oTreeMenuForm')='O' And i_oTreeMenuForm.Left<=0
                This.Top=0
                This.Left=i_oTreeMenuForm.Width+5
            Endif
            This.Name=This.Name+Sys(2015)
            basename=This.Name                        && alternativamente si puo' usare 'sys(2015)'
            Public frm&basename
            frm&basename = This
            * --- definisce il nome per il suo cursore
            This.cCursor=Sys(2015) &&'cur'+basename
            *this.cSaveCursor=this.cCursor
            This.cKeySet=Sys(2015) &&'Key'+basename
            * ---
            If i_VisualTheme=-1
                This.MDIForm = i_cViewMode="A" Or i_cViewMode="M"
            Else
                This.nPrevWndFunc  = GetWindowLong(This.HWnd, GWL_WNDPROC)
                If i_cViewMode <> "S" And Vartype(_Screen.TBMDI)<>"U"
                    *--- WM_SYSCOMMAND
                    Bindevent(This.HWnd, 0x0112, _Screen.TBMDI, "OnSysCommandForm")
                    *--- WM_NCLBUTTONDBLCLK 0xA3
                    Bindevent(This.HWnd, 0xA3, _Screen.TBMDI, "OnNCButtonDblClickForm")
                Endif
            Endif
            *--- Zucchetti Aulla - Inizio
            This.cCurInfo=Sys(2015) && Cursore InfoPublisher
            * Gestione calcolo data modificato / caricato
            This.cCalUtd=Iif(Vartype(g_CALUTD)='C' ,g_CALUTD , 'A')
            *--- Zucchetti Aulla Fine -
            This.GetFirstControl()
            For i=1 To This.oPgFrm.PageCount-1
                This.oPgFrm.Pages(m.i).oPag.oContained=This
            Next
            * --- run-time configuration
            This.oRunTime=Createobject('RunTimeConfigurationAgent',This)
            If Vartype(bLoadRuntimeConfig)='U' Or bLoadRuntimeConfig
                *--- Se CTRL+SHIFT sono premuti non carico il runtime
                If (Bitand(GetKeyState(VK_CONTROL), GetKeyState(VK_SHIFT), 128) <> 128)
                    This.oRunTime.LoadConfig()
                Endif
            Endif
            * ---
            This.NotifyEvent('FormLoad')
            This.CreateChildren()

            *--- Zucchetti Aulla FormDecorator Inizio
            If This.bApplyFormDecorator And i_ThemesManager.GetProp(123)=0
                This.AddObject("oDec", "cp_FormDecorator")
            Else
                This.bApplyFormDecorator = .F.
            Endif
            *--- Zucchetti Aulla FormDecorator Fine
            *-- Zucchetti Aulla Inizio: Gestione ancoraggio oggetti
            Cp_SetAnchorForm(This)
            *-- Zucchetti Aulla Fine: Gestione ancoraggio oggetti
            *  --- la posizione ?
            This.SetPosition()
            If i_VisualTheme<>-1
                If i_cMenuTab<>"S"
                    This.oPgFrm.Tabs = .F.
                    This.oPgFrm.BorderWidth=0
                    If i_bGradientBck
                        With Thisform.ImgBackGround
                            .Width = Thisform.Width
                            .Height = Thisform.Height
                            .Anchor = 15 && Resize Width and Height
                            .Picture = i_ThemesManager.GetProp(11)
                            .ZOrder(1)
                            .Visible = .T.
                        Endwith
                    Endif
                    Thisform.BackColor = i_ThemesManager.GetProp(7)
                    *--- Rendo trasparenti le pagine
                    Local pg, l_i
                    For l_i=1 To This.oPgFrm.PageCount
                        pg = This.oPgFrm.Pages[m.l_i]
                        pg.BackStyle=0
                    Next
                    pg=.Null.
                    If Vartype(This.oTabmenu)<>'O'
                        This.AddObject("oTabMenu","TabMenu", This, i_cMenuTab="C")
                    Endif
                Endif
            Endif
            *---- Zucchetti Aulla Inizio Job Scheduler
            If Vartype(g_SCHEDULER)<>'C' Or g_SCHEDULER<>'S'
                *---- Zucchetti Aulla fine Job Scheduler
                Do Case
                    Case i_cViewMode = "I" And This.MaxButton
                        This.Left = 0
                        This.WindowState = 2
                    Case i_cViewMode = "A"
                        If i_bClientAreaMode
                            This.Left = 0
                            This.WindowState = 2
                        Endif
                    Case i_cViewMode = "M" And This.MaxButton
                        This.Left = 0
                        This.WindowState = 2
                Endcase
                *---- Zucchetti Aulla Inizio Job Scheduler
            Endif
            *---- Zucchetti Aulla fine Job Scheduler
            *--- Aggiunge la form al WindowsManager
            If Vartype(i_oDeskmenu)='O'
                i_oDeskmenu.olistWnd.AddFormItem(This.HWnd)
            Endif
			*--- Aggiorna l'interfaccia gadget nel caso in cui ci sia un gadget
	        *--- di tipo WindowManager a cui deve essere aggiunta la form
	        If Vartype(oGadgetmanager)='O'
	        	oGadgetManager.Notifyevent("GadgetWindowsManager")
	        Endif

            *--- Problemi con alcuni resize e zoom integrati
            This.bEndResize=.T.
            This.Resize()
            *--- Zucchetti Aulla Inizio - Refresh controlli in apertura maschera
            *--- Sposto la maschera fuori schermo per evitare l'effetto a scomparsa dei controlli in hide
            This.nOldLeft = This.Left
            If Vartype(g_bNoHideForm)='L' And Not g_bNoHideForm
                *--- Zucchetti Aulla impostare la variabile quando siamo su terminal da MAC
                This.Left = _Screen.Width + 100
            Else
                This.mHideControls()
            Endif
            This.bNoFoxCharts=This.bNoExistFoxCharts 
            *--- Zucchetti Aulla Fine - Refresh controlli in apertura maschera
            If This.WindowType=0
                This.ScrollBars=Iif(Type('i_nFormScrollBars')='U',3,i_nFormScrollBars)
                This.Show()
            Endif
            This.QueryKeySet("1=0","")          && Where inizialmente nulla
            This.ecpQuery()
            This.NotifyEvent('Init')

            *--- Zucchetti Aulla Inizio - Refresh controlli in apertura maschera
            This.Left = This.nOldLeft
            *--- Zucchetti Aulla Fine - Refresh controlli in apertura maschera

            * --- Activity log
            *--- Tracciatura dell'apertura del form
            If  i_nACTIVATEPROFILER>0
                cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "GSO", "", This, 0, 0, "", This)
            Endif
            * --- Activity log
            If This.WindowType<>0
                If This.WindowType = 1
                    This.ScrollBars=0
                Endif
                * --- Zucchetti Aulla Inizio
                If Not(Type('g_SCHEDULER')='C' And g_SCHEDULER='S')
                    * --- Zucchetti Aulla Fine
                    This.Show()
                Endif

            Endif
            * --- Zucchetti Aulla Inizio, Activity log
        Endif
        * --- Zucchetti Aulla Fine
        DoDefault()
        Return

    Procedure bNoFoxCharts_assign
        Lparameters xNewVal
        
        IF This.bNoFoxCharts <> xNewVal
	        This.bNoFoxCharts = xNewVal
        	IF !m.xNewVal
				BindEventsEx(This.HWnd, WM_ENTERSIZEMOVE, This, [HandleWinEvent])
		        BindEventsEx(This.HWnd, WM_EXITSIZEMOVE, This, [HandleWinEvent])        
	       	EndIf
        EndIf
		RETURN This.bNoFoxCharts
	
    Proc HandleWinEvent
        Lparameters HWnd, nMsg, nwParam, nlParam

		*--- Determino se devo lanciare l'evento resize
       	This.bEndResize = (nMsg = WM_EXITSIZEMOVE)
        IF !This.bNoFoxCharts AND This.bEndResize
        	This.NotifyEvent("EndResize")
        EndIf
        
        Return BROADCAST_QUERY_DENY

    Procedure windowstate_assign
        Lparameters xNewVal
        If i_VisualTheme<>-1 And m.xNewVal=2 And i_cViewMode <> "S"  And Vartype(_Screen.TBMDI)<>"U"
            *--- #define SC_MAXIMIZE  		  0xF030
            _Screen.TBMDI.OnSysCommandForm(Thisform.HWnd, 0, 0xF030)
        Else
            If i_VisualTheme<>-1 And i_cViewMode <> "S" And m.xNewVal=1  And Vartype(_Screen.TBMDI)<>"U" And _Screen.TBMDI.Visible And _Screen.TBMDI.oForm = This
                _Screen.TBMDI.RestoreStd("M")
            Else
                If i_VisualTheme<>-1 And i_cViewMode <> "S" And m.xNewVal=0  And Vartype(_Screen.TBMDI)<>"U" And _Screen.TBMDI.Visible And _Screen.TBMDI.oForm = This
                    This.WindowState = m.xNewVal
                    _Screen.TBMDI.RestoreStd("R")
                Else
                    This.WindowState = m.xNewVal
                Endif
            Endif
        ENDIF
		if This.WindowState = 0 OR This.WindowState = 2
           This.NotifyEvent("EndResize")
		ENDIF
		
    Endproc

    Procedure icon_assign
        Lparameters xNewVal
        This.Icon=Forceext(cp_GetStdImg(m.xNewVal,Justext(m.xNewVal)),Justext(m.xNewVal))
    Endproc


    Proc SetPosition()
        Local i,fo
        *--- Controllo se esiste una posizione salvata par la form
        If i_cConfSavePosForm<>'D' And This.bChkPositionForm And This.WindowState<>1 And This.Left<_Screen.Width And This.Left+This.Width>0 And This.Top<_Screen.Height And This.Top+This.Height>0 And Not This.AutoCenter
            oMemoryFormPosition.SearchPosition(This)
        Endif
        *--- Se DeskMenu aperto posiziono lo form in modo che nn si sovrappongano
        If i_VisualTheme<>-1 And Vartype(i_oDeskmenu)<>"U" And i_oDeskmenu.Visible And This.Left<i_oDeskmenu.Width + 5
            This.Left = This.Left + i_oDeskmenu.Width + 5
        Endif
        For i=_Screen.FormCount To 1 Step -1
            fo=_Screen.Forms(i)
            If fo.Class=This.Class And fo.Name<>This.Name And This.Top=fo.Top And This.Left=fo.Left
                This.Top=This.Top+20
                This.Left=This.Left+20
            Endif
        Next
        Return
    Proc QueryUnload()
        If Inlist(This.cFunction,'Edit','Load')
            If This.IsAChildUpdated()
                If !cp_YesNo(MSG_DISCARD_CHANGES_QP)
                    Nodefault
                    Return .F.
                Endif
            Endif
            This.NotifyEvent('Edit Aborted')
        Endif
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        This.Hide()
        This.NotifyEvent('Done')
        DoDefault()
        Return .T.
        * --- Distruzione
    Proc Destroy()
        *
        *this.NotifyEvent('Done')
        This.LockScreen=.T.
        *--- Zucchetti Aulla Inizio - riposizionare la curform se non accede
        If This.bSec1
            i_curform = .Null.
        Endif
        *--- Zucchetti Aulla Fine - riposizionare la curform se non accede
        If Used(This.cCursor)
            Select (This.cCursor)
            Use
        Endif
        If Used(This.cKeySet)
            Select (This.cKeySet)
            Use
        Endif
        * Zucchetti Aulla - Inizio - Visualizzazione query InfoPublisher su gestione
        If Used(This.cCurInfo)
            Select (This.cCurInfo)
            Use
        Endif
        * Zucchetti Aulla - Fine
        *--- Salvo le informazioni di dove era aperta la form
        *--- se all'interno dello schermo e se non minimizzata
        If Type("oMemoryFormPosition")="O" And i_cConfSavePosForm=' ' And This.WindowState<>1 And !This.MDIForm And This.Left<_Screen.Width And This.Left+This.Width>0 And This.Top<_Screen.Height And This.Top+This.Height>0 And Not This.AutoCenter
            oMemoryFormPosition.InsertPosition(This)
        Endif
        This.ReleaseWarn()
        This.CloseAllTables()
        If Vartype(oCPToolBar)<>'U'
            oCPToolBar.Enable(.F.)
        Endif
        *--- Zucchetti Aulla Inizio - gestione riconessione degli zoom secondari degli elenchi
        ozoomelenco=.Null.
        *--- Zucchetti Aulla Fine - gestione riconessione degli zoom secondari degli elenchi
        This.oFirstControl=.Null.
        This.DestroyChildren()
        *createobject('TimedFreeResources',this.class,this.oPgFrm.PageCount)
        Local basename
        basename=This.Name
        Release frm&basename
        DoDefault()
        If i_VisualTheme<>-1
            If Vartype(i_cViewMode)='C' And i_cViewMode <> "S"
                *--- WM_SYSCOMMAND
                Unbindevent(This.HWnd, 0x0112)
            Endif
        Endif
        This.__sqlx__.CloseConn()
        * --- Activity log
        *--- Tracciatura della chiusura del form
        If  i_nACTIVATEPROFILER>0
            cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "GSD", "", This, 0, 0, "", This)
        Endif
        *--- Activity log
        Return
    Proc SetFocusOnFirstControl()
        *!*	        If !Isnull(This.oFirstControl) And !This.oFirstControl.Enabled
        *!*	            Local l_obj, i
        *!*	            For i=1 To This.oPgFrm.Page1.oPag.ControlCount
        *!*	                With This.oPgFrm.Page1.oPag.Controls(i)
        *!*	                    If Upper(.BaseClass)='TEXTBOX' And .Visible And .Enabled
        *!*	                        l_obj=This.oPgFrm.Page1.oPag.Controls(i)
        *!*	                        l_obj.SetFocus()
        *!*	                        Return
        *!*	                    Endif
        *!*	                Endwith
        *!*	            Next
        *!*	            l_obj=This.oPgFrm.Page1
        *!*	            l_obj.SetFocus()
        *!*	        Else
        *!*	            * Zucchetti Aulla set focus  inizio
        *!*	            If Not Empty(This.cFunction)
        This.oFirstControl.SetFocus()
        *!*	            Endif
        *!*	            * Zucchetti Aulla set focus  fine
        *!*	        Endif
    Endproc

    Proc GetFirstControl()
        If Isnull(This.oFirstControl)
            For i=1 To This.oPgFrm.Page1.oPag.ControlCount
                With This.oPgFrm.Page1.oPag.Controls(i)
                    If Upper(.BaseClass)='TEXTBOX' And .Visible And .Enabled
                        This.oFirstControl=This.oPgFrm.Page1.oPag.Controls(i)
                        Return
                    Endif
                Endwith
            Next
            This.oFirstControl=This.oPgFrm.Page1
        Endif
        Return
        *
        * --- EVENTI/METODI VFP
        *
        * --- Attivazione (in i_curform il puntatore all'oggetto corrente)
        *     se clicco dentro il form o chiamo il form dal menu
    Proc Activate()
        i_curform = This
        This.SelectCursor()
        This.SetCPToolBar()
        *--- Zucchetti Aulla Inizio - gestione riconessione degli zoom secondari degli elenchi
        *riattiva la conessione degli zoom asincroni
        If !This.__sqlx__.bWaitforreply
            If Type ("This.__sqlx__.o_ccursor")='C' And Used (This.__sqlx__.o_ccursor) And Type ("This.__sqlx__.nconn")='N' And This.__sqlx__.nconn<=0  And Type ("this.ozoomelenco")='O'
                Local posiz
                Select (This.__sqlx__.o_ccursor)
                posiz=Recno()
                This.__sqlx__.Requery()
                Select (This.__sqlx__.o_ccursor)
                If  posiz>0 And This.briposiz
                    Goto posiz
                    This.briposiz=.F.
                Endif
            Endif
        Endif
        *--- Zucchetti Aulla Fine - gestione riconessione degli zoom secondari degli elenchi
        DoDefault()
        Return
    Endproc

    Proc SetCPToolBar()
        Do Case
            Case This.cFunction="Load"
                oCPToolBar.SetEdit()
            Case This.cFunction="Edit"
                oCPToolBar.SetEdit()
            Case This.cFunction="Filter"
                oCPToolBar.SetFilter()
            Case This.cFunction="Query"
                oCPToolBar.SetQuery()
        Endcase
        Return
    Proc SetCaption()
        Local l_cType
        l_cType = cp_getEntityType(This.cPrg)
        If l_cType == "Dialog Window" Or l_cType == "unkown"
            This.Caption = This.cComment
        Else
            Do Case
                Case This.cFunction='Edit'
                    This.Caption = This.cComment+" / "+cp_Translate(MSG_CHANGE)
                Case This.cFunction='Load'
                    This.Caption = This.cComment+" / "+cp_Translate(MSG_LOAD)
                Case This.cFunction='Query'
                    This.Caption = This.cComment+" / "+cp_Translate(MSG_QUERY)
                Case This.cFunction='Filter'
                    This.Caption = This.cComment+" / "+cp_Translate(MSG_FILTER)+" "+Iif( This.bSecDataFilter ,MSG_AUTORITY  ,'')
            Endcase
        Endif
        Return
        * --- Disattivazione
    Proc Deactivate()
        oCPToolBar.Enable(.F.)
        If !This.__sqlx__.bWaitforreply
            This.__sqlx__.CloseConn()
        Endif
        DoDefault()
        If Type("This.oBalloon")='O'
            This.oBalloon.CTLVISIBLE = .F.
            Raiseevent(This.oBalloon, "ctlHide", 0)
        Endif
        Return
    Endproc


    * --- Resize
    Proc Resize()
        Local a, i_i
        *--- Gadget
        If Type("oGadgetManager")="O" And !Isnull(oGadgetManager)
            With oGadgetManager
                Local nCharmWidth
                If .Visible And !.bFullScreen
                    This.MaxWidth= _Screen.Width - .Width
                Else
                    If !Isnull(.oCharmBar)
                        If .oCharmBar.bPin
                            nCharmWidth = .oCharmBar.oPgFrm.Page1.oPag.oBox_1_2.Width + 1
                        Else
                            nCharmWidth = .oCharmBar.oPgFrm.Page1.oPag.oBox_1_1.Width - 1
                        Endif
                        This.MaxWidth = _Screen.Width -  m.nCharmWidth
                    Endif
                Endif

            Endwith
        Endif
        *--- Gadget
        a=This.oPgFrm.ActivePage
        If Type('this.oPgFrm.Pages(m.a).oPag')<>'O'
            This.ScrollBars=0
            * Zucchetti Aulla gestione visualizzazione dimensione fissa form inizio
            This.oPgFrm.Width=Iif( Vartype(g_cMaskStartupDimension)<>"C" Or g_cMaskStartupDimension='S' Or This.bOnInit>0, This.Width, Max(This.Width, g_nMaskFixedWidth))
            This.oPgFrm.Height=Iif( Vartype(g_cMaskStartupDimension)<>"C" Or g_cMaskStartupDimension='S' Or This.bOnInit>0, Max(This.Height, 30), Max( Max( This.Height, g_nMaskFixedHeight), 30) )
            * Zucchetti Aulla gestione visualizzazione dimensione fissa form fine
        Else
            If !This.LockScreen
                This.ScrollBars=0
                This.oPgFrm.Width=Max(This.Width,Iif(This.oPgFrm.Pages(a).oPag.stdwidth<>-1,This.oPgFrm.Pages(a).oPag.stdwidth,This.oPgFrm.Pages(a).oPag.Width))
                This.oPgFrm.Height=Max(25,Max(This.Height,Iif(This.oPgFrm.Pages(a).oPag.stdheight<>-1,This.oPgFrm.Pages(a).oPag.stdheight,This.oPgFrm.Pages(a).oPag.Height)))
                * Zucchetti Aulla gestione visualizzazione dimensione fissa form inizio
                * Verifico che sia impostato il resize nella prima pagina, sono in inizializzazione e quindi sar� sicuramente la prima pagina ad essere visualizzata
                * Se non � presente il resize � innutile redimentsionare la maschera
                If Not This.bCheckSize
                    This.bCheckSize = .T.
                    i_i=1
                    Do While i_i<=This.oPgFrm.PageCount And Not This.bSizeForm And Type('This.oPgFrm.Pages(i_i).autozoom')='U' And Type('This.oPgFrm.Pages(i_i).oPag')<>'U'
                        This.bSizeForm=This.oPgFrm.Pages(i_i).oPag.resizeXpos<>-1 Or  This.oPgFrm.Pages(i_i).oPag.resizeYpos<>-1
                        i_i=i_i+1
                    Enddo
                Endif
                If Vartype(g_cMaskStartupDimension)="C" And g_cMaskStartupDimension='D' And (This.bOnInit=1 Or cp_getEntityType(This.cPrg)="Dialog Window" And This.bOnInit=0) And (This.bSizeForm)
                    This.oPgFrm.Width = Max(This.oPgFrm.Width, g_nMaskFixedWidth)
                    This.oPgFrm.Height = Max( This.oPgFrm.Height, g_nMaskFixedHeight)
                    This.Width = Max( This.Width, g_nMaskFixedWidth)
                    This.Height = Max( This.Height, g_nMaskFixedHeight)
                Endif
                * Zucchetti Aulla gestione visualizzazione dimensione fissa form fine
                If This.oPgFrm.Width>This.Width Or This.oPgFrm.Height>This.Height
                    If This.WindowType = 1
                        This.ScrollBars=0
                    Else
                        This.ScrollBars=Iif(Type('i_nFormScrollBars')='U',3,i_nFormScrollBars)
                    Endif
                Endif
            Endif
        Endif
        This.oPgFrm.SetZoomSize()

        *--- Zucchetti FormDecorator
        If !This.LockScreen
        This.oPgFrm.Height = This.oPgFrm.Height - Iif(Pemstatus(This, "oDec",5), This.oDec.Height,0)
        Endif
        *--- Zucchetti FormDecorator

        cp_ResizeContainer(This.oPgFrm, This.nPageResizeContainer)
        DoDefault()
        *--- Zucchetti Aulla Inizio - Document Management - Attributi
        This.Attach_Resize()
        *--- Zucchetti Aulla Fine - Document Management - Attributi
        * Zucchetti Aulla gestione visualizzazione dimensione fissa form inizio
        This.bOnInit = This.bOnInit + 1
        * Zucchetti Aulla gestione visualizzazione dimensione fissa form fine
        IF !This.bNoFoxCharts AND This.bEndResize
	      *--- Lancia l'evento EndResize per i grafici solo al termine della resize della form
	      This.NotifyEvent("EndResize")
	    EndIf
        Return
        *--- Zucchetti Aulla Inizio - Document Management - Attributi
    Procedure Attach_Resize
        Local l_Alleg
        l_Alleg = .F.
        With This
            If Vartype(.oAlleg)<>"U"
                l_Alleg = .T.
                .oAlleg.Left = .Width - 25
                .oAlnum.Left = .Width - 45
            Endif
            If Vartype(.oAttrib)<>"U"
                .oAttrib.Left = .Width - (25+Iif(l_Alleg,40,0))
                .oAtnum.Left = .Width - (55+Iif(l_Alleg,40,0))
            Endif
            If Vartype(.oRevInfSync)<>"U"
	            .oRevInfSync.Left=.Width-(28+Iif(PemStatus(This,"oAlleg",5) and this.oAlleg.Visible,40,0)+Iif(PemStatus(This,"oAttrib",5) and this.oAttrib.Visible,40,0))
	    EndIf
        Endwith
    Endproc
    *--- Zucchetti Aulla Fine - Document Management - Attributi
    *
    * ------ METODI CODEPAINTER
    *
    * --- Termina con "sicurezza" un editing
    Proc __dummy__.GotFocus()
        * --- si piu' uscire in sicurezza perche' il control e' riuscito a ricevere il focus, quindi e' scattata con successo la
        *     valid del control precedentemente attivo
        This.Parent.bOkToTerminate=.T.
        Return
    Func TerminateEdit()
        Local bRes,ac
        *ac=this.activeControl
        ****************************************************************************
        * Se salvo quando sono posizionato su un bottone
        * invece che su un control non avveniva il salvataggio
        If Type("this.activeControl.class")='C' And Lower(This.ActiveControl.Class)='stdbutton'
            This.bOkToTerminate=.T.
        Else
            This.bOkToTerminate=.F.
            This.__dummy__.Enabled=.T.
            This.__dummy__.SetFocus()
        Endif
        Return(This.bOkToTerminate)
    Proc CreateChildren()
        Return
    Proc DestroyChildren()
        Return
    Proc ChildrenNewDocument()
        Return
    Func CheckChildrenForDelete()
        Return(.T.)
    Func IsAChildUpdated()
        Return(This.bUpdated)
    Proc ChildrenChangeRow()
        Return
    Proc SelectCursor()
        If Used(This.cKeySet)
            Select (This.cKeySet)
        Endif
        Return
    Proc LoadRec()
        Return
    Proc LoadRecWarn()
        If Pemstatus(This,'SetWorkFromKeySet',5)
            This.SetWorkFromKeySet()
        Endif
        *--- Zucchetti Aulla Inizio - Memorizza gli ultimi record aperti
        This.SetDataHistory()
        *--- Zucchetti Aulla Fine - Memorizza gli ultimi record aperti
        Local LockScr
        LockScr=This.LockScreen
        This.LockScreen=.T.
        If Type('i_dcx')='U'
            cp_ReadXdc()
        Endif
        This.LoadRec()
        This.LockScreen=LockScr
        oCPToolBar.SetQuery()
        If This.CanView()
            This.LoadWarn()
        Else
            This.bLoaded=.F.
        Endif
        Return
    Proc LoadWarn()
        * --- carica i postit legati al record
        Local i,p,bCancel,dStop,i_cDatabaseType
        This.nLoadTime=Seconds()
        * --- cancella i warn precedenti
        This.ReleaseWarn()
        * --- carica i nuovi warn
        If This.bPostIt And This.bLoaded
            This.GetXKey()
            p=This.KeyToStr()
            If This.nPostItConn<>0
                i_cDatabaseType=cp_GetDatabaseType(This.nPostItConn)
                If i_cDatabaseType="SQLServer"
                    cp_SQL(This.nPostItConn,"select * from postit where code in (select warncode from cpwarn(nolock) where tablecode="+cp_ToStrODBC(p)+") and (datestart <= "+cp_ToStrODBC(i_dPostInFilterDate)+" or datestart IS NULL) and (datestop >= "+cp_ToStrODBC(i_dPostInFilterDate)+" or datestop IS NULL)","postit_c")
                Else
                    cp_SQL(This.nPostItConn,"select * from postit where code in (select warncode from cpwarn where tablecode="+cp_ToStrODBC(p)+") and (datestart <= "+cp_ToStrODBC(i_dPostInFilterDate)+" or datestart IS NULL) and (datestop >= "+cp_ToStrODBC(i_dPostInFilterDate)+" or datestop IS NULL)","postit_c")
                Endif
            Else
                *select * from postit where code in (select warncode from cpwarn where tablecode=p) and (datestart <=i_dPostInFilterDate or datestart IS NULL) and (datestop >=i_dPostInFilterDate or datestop IS NULL) into cursor postit_c
                Select * From postit Where Code In (Select warncode From cpwarn Where tablecode=p) And (datestart <=i_dPostInFilterDate Or (datestart Is Null Or Empty(datestart))) And (datestop >=i_dPostInFilterDate Or (datestop Is Null Or Empty(datestop))) Into Cursor postit_c
            Endif
            Dimension This.cWarn[max(1,reccount())]
            i=0
            If Used('postit_c')
                Scan
                    If Not(cp_InScreenPostIT(postit_c.Code))
                        i=i+1
                        bCancel=.F.
                        dStop = Iif(Isnull(postit_c.datestop) Or postit_c.datestop=Ctod('  /  /  '), i_dPostInFilterDate, postit_c.datestop)
                        This.cWarn[i]=postit_c.Code
                        If At("I",postit_c.Status)<>0 And ;
                                type('this.oPgFrm.Pages(this.oPgFrm.activepage).oWarning')='U'
                            *---Postin integrato dentro l'oggetto
                            This.AddTab(This.nPostItConn)
                        Else
                            * ---- Zucchetti Aulla inizio - revisione postin
                            If Vartype(g_Alertmanager)<>'O' Or ( Vartype(i_bDisableNewPostIt)='L' And i_bDisableNewPostIt)
                                * ---- Zucchetti Aulla fine - revisione postin
                                p=Createobject('postit','set',This.nPostItConn)
                                * ---- Zucchetti Aulla inizio - revisione postin
                            Else
                                p = g_Alertmanager.AddAlert("cp_AdvPostIt", 'set', This.nPostItConn)
                                This.cWarn[i] = p
                                p.DoAlert()
                            Endif
                            * ---- Zucchetti Aulla fine - revisione postin
                        Endif
                        If dStop<i_dPostInFilterDate
                            *---Post-IN scaduto
                            If cp_YesNo(MSG_POSTIN_EXPIRED_QM+Chr(10)+Chr(13)+MSG_DELETE_IT_QP) Then
                                bCancel = .T.
                            Endif
                        Endif
                        If bCancel
                            If This.nPostItConn<>0
                                =cp_SQL(This.nPostItConn,"delete from postit where code="+cp_ToStr(postit_c.Code))
                                =cp_SQL(This.nPostItConn,"delete from cpwarn where warncode='"+postit_c.Code+"'")
                            Else
                                Delete From postit Where Code=postit_c.Code
                                Delete From cpwarn Where warncode=postit_c.Code
                            Endif
                            If At("I",postit_c.Status)<>0
                                *---Rimozione dell'eventuale tabstrip degli allegati
                                This.RemoveTab()
                            Else
                                p.Release()
                            Endif
                        Endif
                    Endif
                Endscan
                Use In Select('postit_c')
            Endif
            This.nWarn=i
        Endif
        *this.Show()
        This.SetFocusOnFirstControl()
        * --- Zucchetti Aulla - Inizio Document Management - Attributi
        This.LoadAttach()
        * --- Zucchetti Aulla - Fine Document Management - Attributi
        *--- Zucchetti Aulla Inizio - iRevolution 
        This.CheckiRevolutionSync()
        *--- Zucchetti Aulla Fine - iRevolution 
        Return
    *--- Zucchetti Aulla Inizio - iRevolution    
    Procedure CheckiRevolutionSync()
        If Type("g_REVI")='C' And g_REVI='S'
            Local cState, l_macro, l_unknown
            l_unknown = Not Pemstatus(This,"bHasActions",5)
            If l_unknown
                This.AddProperty("bHasActions",.F.)
            Endif
            If l_unknown Or This.bHasActions
                l_macro = 'cState = GSRV_BIQ(.null., This, "CheckSyncroState")'
                &l_macro
                If !Empty(m.cState )
                    Do Case
                        Case cState="S"	&&Sync
                            If Vartype(i_ThemesManager)=='O'
                                This.oRevInfSync.Picture = Forceext(i_ThemesManager.RetBmpFromIco('bmp\RevInfSync.bmp', 24),'bmp')
                            Else
                                This.oRevInfSync.Picture='bmp\RevInfSync.bmp'
                            Endif
                            This.oRevInfSync.ToolTipText = cp_Translate("Sincronizzato")
                        Case cState="W" Or cState="." &&Waiting
                            If Vartype(i_ThemesManager)=='O'
                                This.oRevInfSync.Picture = Forceext(i_ThemesManager.RetBmpFromIco('bmp\RevInf2Sync.bmp', 24),'bmp')
                            Else
                                This.oRevInfSync.Picture='bmp\RevInf2Sync.bmp'
                            Endif
                            This.oRevInfSync.ToolTipText = cp_Translate("In attesa di sincronizzazione")
                        Case cState="R"&&Running
                            If Vartype(i_ThemesManager)=='O'
                                This.oRevInfSync.Picture = Forceext(i_ThemesManager.RetBmpFromIco('bmp\RevInf2Sync.bmp', 24),'bmp')
                            Else
                                This.oRevInfSync.Picture='bmp\RevInf2Sync.bmp'
                            Endif
                            This.oRevInfSync.ToolTipText = cp_Translate("Sincronizzazione in corso...")
                        Case cState="N" &&Never Sync
                            If Vartype(i_ThemesManager)=='O'
                                This.oRevInfSync.Picture = Forceext(i_ThemesManager.RetBmpFromIco('bmp\RevInfNeverSync.bmp', 24),'bmp')
                            Else
                                This.oRevInfSync.Picture='bmp\RevInfNeverSync.bmp'
                            Endif
                            This.oRevInfSync.ToolTipText = cp_Translate("Informazioni di sincronizzazione non presenti")
                        Case cState="E" &&Error
                            If Vartype(i_ThemesManager)=='O'
                                This.oRevInfSync.Picture = Forceext(i_ThemesManager.RetBmpFromIco('bmp\RevInfSyncErr.bmp', 24),'bmp')
                            Else
                                This.oRevInfSync.Picture='bmp\RevInfSyncErr.bmp'
                            Endif
                            This.oRevInfSync.ToolTipText = cp_Translate("Errore durante la sincronizzazione")
                        Case cState="X" &&Blocco
                            If Vartype(i_ThemesManager)=='O'
                                This.oRevInfSync.Picture = Forceext(i_ThemesManager.RetBmpFromIco('bmp\RevInfSyncBlk.bmp', 24),'bmp')
                            Else
                                This.oRevInfSync.Picture='bmp\RevInfSyncBlk.bmp'
                            Endif
                            This.oRevInfSync.ToolTipText = cp_Translate("Sincronizzazione sospesa")
                    Endcase
                    This.oRevInfSync.Top = Objtoclient(This.oPgFrm.Page1,1) - This.oRevInfSync.Height - 1
                    This.oRevInfSync.BackStyle=0
                    This.oRevInfSync.Left=This.Width-(28+Iif(Pemstatus(This,"oAlleg",5) And This.oAlleg.Visible,40,Iif(Pemstatus(This,"oAttrib",5) And This.oAttrib.Visible,40,0)))
                    This.oRevInfSync.Visible=.T.
                Else
                    If Type("This.oRevInfSync") = 'O'
                        This.oRevInfSync.Visible=.F.
                    Endif
                Endif
            Endif
        Endif
    Endproc
    *--- Zucchetti Aulla Fine - iRevolution    	
    *--- Zucchetti Aulla Inizio - Memorizza gli ultimi record aperti
    Procedure SetDataHistory()
        Local i, myfld, myval, cmyval, cmd, prg, par, nparam, gest, cKeySelect, vocname
        If Lower(This.BaseClass)="form" And Not Empty(This.cPrg)
            cKeySelect=This.cKeySelect
            prg=Upper(This.getSecurityCode())
            nparam = At(",", prg)
            vocname=""
            If nparam=0
                gest = prg
            Else
                par = "'" + Rtrim(Substr(prg, nparam+1)) + "'"
                prg = Left(prg, nparam-1)
                gest = prg + " with " + par
                par = '(' + par + ')'
                prg = prg + par
            Endif
            cmd = 'OPENGEST WITH "A", "' + prg + '"'
            Do While Not Empty(cKeySelect)
                i=At(",", cKeySelect)
                If i>0 Then
                    myfld=Left(cKeySelect,i-1)
                    cKeySelect=Substr(cKeySelect,i+1)
                Else
                    myfld=cKeySelect
                    cKeySelect=""
                Endif
                myval=Evaluate(myfld)
                Do Case
                    Case Vartype(myval)="C"
                        cmyval=myval
                    Case Vartype(myval)$"DT"
                        cmyval=Dtoc(myval)
                        myval=cp_todate(myval)
                    Otherwise
                        cmyval=cp_ToStrODBC(myval)
                Endcase
                cmyval=Strtran(Alltrim(cmyval),"'", "")
                If Empty(vocname) Then
                    vocname=cmyval
                Else
                    vocname=vocname + "/" + cmyval
                Endif
                If Vartype(myval)="D"
                    myval=cp_ToStrODBC(myval)
                    myval=Strtran(myval,"{d '","{^")
                    myval=Strtran(myval,"'}","}")
                    cmd = cmd + ', "' + myfld + '", ' + myval
                Else
                    cmd = cmd + ', "' + myfld + '", ' + cp_ToStrODBC(myval)
                Endif
            Enddo
            gsut_bmr('D', vocname, cmd, gest)
        Endif
    Endproc
    *--- Zucchetti Aulla Fine - Memorizza gli ultimi record aperti

    * --- Zucchetti Aulla - Inizio Document Management - Attributi
    Proc LoadAttach()
        * Test presenza allegati
        Local l_ckeyselect,l_cfile, l_vpos, l_tmpc, l_tmpp, l_tmpv, l_cRec, l_Alleg

        If Vartype(This.cKeySelect)='C' And Not Empty(This.cKeySelect)
        	l_tmpc = This.cKeySelect
        	l_ckeyselect=''
        	Do While At(',',l_tmpc)>0
        		l_vpos = At(',',l_tmpc)
        		l_tmpp = 'this.w_'+Substr(l_tmpc,1,At(',',l_tmpc)-1)
        		l_tmpv = Eval(l_tmpp)
        		Do Case
        			Case Vartype(l_tmpv)='C'
        				l_ckeyselect = l_ckeyselect+l_tmpv
        			Case Vartype(l_tmpv)='D'
        				l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
        			Case Vartype(l_tmpv)='N'
        				l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
        		Endcase
        		l_tmpc = Substr(l_tmpc,At(',',l_tmpc)+1)
        	Enddo
        	* Valorizza l'ultimo
        	l_tmpp = 'this.w_'+l_tmpc
        	l_tmpv = Eval(l_tmpp)
        	Do Case
        		Case Vartype(l_tmpv)='C'
        			l_ckeyselect = l_ckeyselect+l_tmpv
        		Case Vartype(l_tmpv)='D'
        			l_ckeyselect = l_ckeyselect+Dtos(l_tmpv)
        		Case Vartype(l_tmpv)='N'
        			l_ckeyselect = l_ckeyselect+Alltrim(Str(l_tmpv))
        	Endcase
        	l_cfile = Upper(This.cFile)
        	If ! Empty(l_ckeyselect) And !(Vartype(g_DMIP_no_chkalleg)='L' AND g_DMIP_no_chkalleg)
        		Local l_Task,l_cBack,l_Key,i_AsyncTask, i_SyncroDLL
        		If Vartype(This.oAlleg) <> 'O'
        			This.AddObject('oAlleg','Image')
        			If Vartype(i_ThemesManager)=='O'
        				This.oAlleg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('bmp\attach.bmp', 24),'bmp')
        			Else
        				This.oAlleg.Picture='bmp\attach.bmp'
        			Endif
        			This.oAlleg.Top = Objtoclient(This.oPgFrm.Page1, 1) - This.oAlleg.Height - 1
        			This.oAlleg.Left=This.Width-25
        			This.oAlleg.BackStyle=0
        			This.AddObject('oAlnum','Label')
        			This.oAlnum.Top=This.oAlleg.Top
        			This.oAlnum.Left=This.Width-45
        			This.oAlnum.Width=25
        			This.oAlnum.BackStyle=0
        			This.oAlnum.Alignment=1
        			This.oAlnum.ToolTipText=cp_Translate(MSG_RETRIEVING_ALL_RECORDS_D)
        			This.oAlnum.ForeColor = i_ThemesManager.GetProp(98)
        		Endif
        		*--- Zucchetti Aulla Inizio - iRevolution
        		*--- ripristino l'immagine della graffetta e il tooltip
        		*--- nel tentativo precedente potrebbe caso ci sia stato un errore in un precedente caricamento
        		If Vartype(i_ThemesManager)=='O'
    				This.oAlleg.Picture = Forceext(i_ThemesManager.RetBmpFromIco('bmp\attach.bmp', 24),'bmp')
    			Else
    				This.oAlleg.Picture='bmp\attach.bmp'
    			Endif
        		This.oAlleg.ToolTipText=cp_Translate(MSG_RETRIEVING_ALL_RECORDS_D)
        		This.oAlleg.Visible=.T.
        		This.oAlnum.Caption='?'
        		This.oAlnum.Visible=.T.
        		*--- Preparo la stringa della chiave per poterla passare come parametro al task
        		Do Case
        			Case !([']$m.l_ckeyselect)
        				m.l_Key = [']+m.l_ckeyselect+[']
        			Case !(["]$m.l_ckeyselect)
        				m.l_Key = ["]+m.l_ckeyselect+["]
        			Otherwise
        				m.l_Key = '['+m.l_ckeyselect+']'
        		Endcase
        		m.i_AsyncTask = .F.
        		If g_DMIP='S'
        		    i_SyncroDLL = chkSyncroCRC()
        		    If i_SyncroDLL
        		        m.l_Task = Textmerge("chkalleg_async(.null.,<<cp_toStrODBC(m.l_cfile)>>,<<m.l_Key>>)")
        		        m.l_cBack = Textmerge("chkalleg_cback(.null.,<<cp_toStrODBC(This.Name)>>)")
        		        m.i_AsyncTask = ah_AsyncTask(m.l_Task,m.l_cBack,15)
        		    Endif
       			Else
        		    i_SyncroDLL = .T.
        		Endif
        		If !i_AsyncTask
        			If i_SyncroDLL
        			    l_sPrg='chkalleg( l_cfile, l_ckeyselect )'
        			    l_cRec  = &l_sPrg
        			Else
        			    l_cRec  = ""
        			Endif
        			If ! Empty( l_cRec )
        				l_Alleg=.T.
        				This.oAlleg.ToolTipText=cp_Translate(MSG_ARC_DOC)
        				This.oAlleg.Visible=.T.
        				This.oAlnum.Caption=l_cRec
        				This.oAlnum.ToolTipText=cp_Translate(MSG_NUM_ARC_DOC)
        				This.oAlnum.Visible=.T.
        			Else
        				* Rilascia flags allegati
        				If Vartype(This.oAlleg) = 'O'
        					This.oAlleg.Visible=.F.
        					This.oAlnum.Visible=.F.
        				Endif
        				*--- Zucchetti Aulla Inizio - iRevolution
        				If Type("g_REVI")='C' And g_REVI='S' And Pemstatus(This,"oRevInfSync",5)
        					This.oRevInfSync.Visible=.F.
        				Endif
        				*--- Zucchetti Aulla Fine - iRevolution
        			Endif
        			Select (This.cCursor)
        			Go Top
        			If Type('GESTGUID')<>'U'
        				_GESTG=Nvl(GESTGUID,'')
        				If !Empty(_GESTG) And g_APPLICATION <> "ADHOC REVOLUTION"
        					l_sPrg='gsma_bta( this, _GESTG )'
        					l_cRec = &l_sPrg
        					If ! Empty(l_cRec)
        						If Vartype(This.oAttrib) <> 'O'
        							This.AddObject('oAttrib','Image')
        							If Vartype(i_ThemesManager)=='O'
        								This.oAttrib.Picture = Forceext(i_ThemesManager.RetBmpFromIco('BMP\tattrib.bmp', 24),'bmp')
        							Else
        								This.oAttrib.Picture ='BMP\tattrib.bmp'
        							Endif
        							This.oAttrib.ToolTipText=cp_Translate(MSG_ATTRIBUTE)
        							This.oAttrib.Top = Objtoclient(This.oPgFrm.Page1, 1) - This.oAttrib.Height - 1
        							This.oAttrib.BackStyle=0
        							This.AddObject('oAtnum','Label')
        							This.oAtnum.Top=This.oAttrib.Top
        							This.oAtnum.Width=25
        							This.oAtnum.BackStyle=0
        							This.oAtnum.Alignment=1
        						Endif
        						This.oAttrib.Left=This.Width-(25+Iif(l_Alleg,40,0))
        						This.oAtnum.Left=This.Width-(55+Iif(l_Alleg,40,0))
        						This.oAttrib.Visible=.T.
        						This.oAtnum.Caption=l_cRec
        						This.oAtnum.ToolTipText=cp_Translate(MSG_NUM_ATTRIBUTE)
        						This.oAtnum.Visible=.T.
        						This.oAtnum.ForeColor = i_ThemesManager.GetProp(98)
        					Else
        						If Vartype(This.oAttrib) = 'O'
        							This.oAttrib.Visible=.F.
        							This.oAtnum.Visible=.F.
        						Endif
        						*--- Zucchetti Aulla Inizio - iRevolution
        						If Type("g_REVI")='C' And g_REVI='S' And Pemstatus(This,"oRevInfSync",5)
        							This.oRevInfSync.Visible=.F.
        						Endif
        						*--- Zucchetti Aulla Fine - iRevolution
        					Endif
        				Endif
        			Endif
        		Endif
        		
        		*--- Zucchetti Aulla Fine - iRevolution
        	Endif
        Endif
    Proc ReleaseAttach()
        * Rilascia flags allegati
        If Vartype(This.oAlleg) = 'O'
            This.oAlleg.Visible=.F.
            This.oAlnum.Visible=.F.
        Endif
        * Rilascia flags attributi
        If Vartype(This.oAttrib) = 'O'
            This.oAttrib.Visible=.F.
            This.oAtnum.Visible=.F.
        Endif

        * --- Zucchetti Aulla - Fine Document Management - Attributi

    Proc ReleaseWarn()
        Local i,p
        * --- cancella i warn precedenti
        If This.nWarn<>0
            *---Rimozione dell'eventuale tabstrip degli allegati
            For i=1 To This.nWarn
            	p=This.cWarn[i]
                If TYPE("&p..Name")="C" AND &p..Name="OWARNING"
                    This.RemoveTab()
                Else
                    If Vartype(p)<>"X"
                        If Vartype(p)="O"
                            p.oPostItContainer.relasePostIT()
                        Else
                            If Type(p)='O'
                                &p..Parent.Release()
                            Endif
                        Endif
                    Endif
                Endif
            Next
        Endif
        This.nWarn=0
        * --- Zucchetti Aulla - Inizio Document Management
        This.ReleaseAttach()
        * --- Zucchetti Aulla - Fine Document Management
        *--- Zucchetti Aulla Inizio - iRevolution
        IF TYPE("g_REVI")='C' AND g_REVI='S' AND pemstatus(This,"oRevInfSync",5)
        	This.RemoveObject("oRevInfSync")
        Endif
        *--- Zucchetti Aulla Fine - iRevolution
        Return
    Func CanView()
        Return(.T.)
    Func CanAccess()
        Return(.T.)
    Proc RemoveTab()
        *---Rimuove il tabstrip degli allegati
        With This.oPgFrm
        	LOCAL i_nPostitPage
        	i_nPostitPage = .PageCount
            Do While Type('this.oPgFrm.Pages(m.i_nPostitPage).oWarning')='O' And m.i_nPostitPage>0
	        	.Pages(m.i_nPostitPage).oWarning.relasePostIT()
	            .Pages(m.i_nPostitPage).RemoveObject("oWarning")
	            .PageCount=.PageCount-1            
                i_nPostitPage= m.i_nPostitPage-1
            Enddo
        Endwith
        Return
    Proc AddTab(i_nPostitConn)
        Local i_cPageName
        *---Aggiunte il tabstrip degli allegati
        i_cPageName=cp_Translate(MSG_ENCLOSURES)
        With This.oPgFrm
            .PageCount=.PageCount+1
            * ---- Zucchetti Aulla inizio - revisione postin
            If Vartype(g_Alertmanager)<>'O' Or ( Vartype(i_bDisableNewPostIt)='L' And i_bDisableNewPostIt)
                * ---- Zucchetti Aulla fine - revisione postin
                .Pages(.PageCount).AddObject("oWarning","postitcontainer","contained",i_nPostitConn,.Parent)
                * ---- Zucchetti Aulla inizio - revisione postin
            Else
                .Pages(.PageCount).AddObject("oWarning","cp_AdvPostItContainer")
                .Pages(.PageCount).oWarning.SetUpPostIt("contained",i_nPostitConn,.Parent)
            Endif
            * ---- Zucchetti Aulla fine - revisione postin
            .Pages(.PageCount).oWarning.Visible=.T.
            i_cPageName=Iif(Empty(.Pages(.PageCount).oWarning.cPageName),i_cPageName,Trim(.Pages(.PageCount).oWarning.cPageName))
            .Pages(.PageCount).Caption=cp_Translate(i_cPageName)
            .Height=.Parent.Height
        Endwith
        Return
    Func OpenWorkTables()
        * --- "apre" le tabelle, in realta' controlla solo che esistano
        This.nOpenedTables=0
        Return(.T.)
    Func OpenAllTables(nTables)
        * --- "apre" tutte le tabelle della procedura
        Local i,i_ok,i_wrk,i_nidx, cProgram, bSecurityRec
        This.nOpenedTables=0
        i_ok=.T.
        *** se sicurezza al livello di record
        bSecurityRec=i_bSecurityRecord And Not cp_IsAdministrator(.T.)
        cp_LoadRuntimeUserFilters()
        For i=1 To nTables
            If i_ok
                If bSecurityRec
                    *** Sicurezza al livello di record: verifico se per la gestione aperta posso entrare nella tabella
                    i_queryfilter=i_TablePropSec[cp_GetTableDefIdx(this.cWorkTables[i]),1]
                    If Vartype(i_queryfilter)='C' And Not Empty(i_queryfilter)
                        If Type("this.oParentObject.cPrg")='C' And Vartype(i_curform)='O' And Vartype(i_curform.cPrg)='C'
                            cProgram=i_curform.cPrg
                        Else
                            cProgram=This.cPrg
                        Endif
                        cp_AppQueryFilter(cProgram,This.cWorkTables[i])
                    Endif
                    ***
                Endif
                i_nidx=cp_OpenTable(This.cWorkTables[i])
                If i_nidx<>0
                    This.nOpenedTables=This.nOpenedTables+1
                    i_wrk=This.cWorkTables[i]
                    This.&i_wrk._IDX=i_nidx
                Else
                    i_ok=.F.
                Endif
            Endif
        Next
        If i_ok
            This.SetPostItConn()
        Endif
        Return(i_ok)
    Proc SetPostItConn()
        Return
    Proc CloseAllTables()
        * --- "chiude" tutte le tabelle, in relta decrementa un contatore d' uso
        Local i
        For i=1 To This.nOpenedTables
            cp_CloseTable(This.cWorkTables[i])
        Next
        Return
    Proc BlankRec()
        Return
    Proc SetStatus()
        This.SetCaption()
        This.SetCPToolBar()
        This.SetEnabled(This.cFunction)
        Return
        *proc SetChildrenStatus(i_cOp)
        *  return
    Proc SetEnabled(i_cOp)
        Return
    Func BuildFilter()
        Return('')
    Proc mDelete()
        Return
    Proc mDeleteWarnings()
        Local cKey
        If Not(bTrsErr) And This.bPostIt
            * --- cancella i warn legati a questo record
            cKey=This.KeyToStr()
            If This.nPostItConn<>0
                cp_SQL(This.nPostItConn,"delete from postit where code in (select warncode from cpwarn where tablecode="+cp_ToStrODBC(cKey)+")")
                cp_SQL(This.nPostItConn,"delete from cpwarn where tablecode="+cp_ToStrODBC(cKey))
            Else
                Delete From postit Where Code In (Select warncode From cpwarn Where tablecode==cKey)
                Delete From cpwarn Where tablecode==cKey
            Endif
        Endif
        Return
    Func mInsert()
        Return(.F.)
    Proc mReplace(i_bUpd)
        Return
    Proc mCalc(i_bUpd)
        Return
    Proc mCalledBatchEnd(i_bUpd)
        If !This.bCalculating
            This.mCalc(i_bUpd)
        Endif
        Return
    Proc DoRTCalc(i_nFrom,i_nTo,i_bIsCalc)
        If Vartype(bLoadRuntimeConfig)='U' Or bLoadRuntimeConfig
            This.oRunTime.DoRTCalc(i_nFrom,i_nTo,i_bIsCalc)
        Endif
        Return
    Proc mEnableControls()
        This.oRunTime.mEnableControls()
        *** Zucchetti Aulla inizio - Modulo BCON
        ** Disabilita il gruppo e l'impresa per il modulo di preconsolidato
        ** Sulla maschera deve essere stato definito un parametro pTipoMod
        If g_APPLICATION = "ad hoc ENTERPRISE" And Vartype(g_BCON)="C" And g_BCON = "S" And Atc('GSBC', This.cPrg)<> 0
            If Type("this.pTipoMod") = 'C'
                If This.pTipoMod = 'P'
                    If Not(Isnull(This.oCtrlImpresa))
                        This.oCtrlImpresa.Enabled = .F.
                    Endif
                Endif
            Endif
        Endif
        *** Zucchetti Aulla fine - Modulo BCON
        Return
    Proc mEnableControlsFixed()
        Return
    Proc mObblControls()
        Return
    Proc mHideControls()
        This.oRunTime.mHideControls()
        Return
    Proc mHideControlsFixed()
        This.oRunTime.mHideControlsFixed()
        Return
    Proc mHideRowControls()
        This.oRunTime.mHideRowControls()
        Return
    Proc Dep()
        Return
    Func CheckForm()
        Return(This.oRunTime.CheckForm())
    Func CheckRow()
        Return(This.oRunTime.CheckRow())
    Proc SaveDependsOn()
        Return
    Proc mUpdateTrs(i_bCanSkip)
        Return
    Proc mRestoreTrs(i_bCanSkip)
        Return
    Proc NotifyEvent(cEvent)
    	*--- Zucchetti Aulla - Unbind resize FoxChart
        IF !Thisform.bNoFoxCharts AND m.cEvent=='Done'
			UnbindEventsEx(Thisform.HWnd, WM_ENTERSIZEMOVE)
		    UnbindEventsEx(Thisform.HWnd, WM_EXITSIZEMOVE)  
		EndIf        	
	    *--- Zucchetti Aulla - Unbind resize FoxChart
	    
        *--- Activity log - tracciatura eventi
        If  i_nACTIVATEPROFILER>0
            cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UEV", cEvent, This, 0, 0, "", This)
        Endif
        * --- Activity log - tracciatura eventi

        * Zucchetti Aulla Inizio--- Gestione dati caricato da, alla data..
        If Lower(cEvent)='update start'
            If Vartype(This.w_UTDV)<>'U'
                This.w_UTDV=SetInfoDate(This.cCalUtd)
                If Vartype(This.w_UTCV)<>'U'
                    This.w_UTCV=i_CoduTe
                Endif
            Endif
        Endif

        If Lower(cEvent)='insert start'
            If Vartype(This.w_UTDC)<>'U'
                This.w_UTDC=SetInfoDate(This.cCalUtd)
                If Vartype(This.w_UTCC)<>'U'
                    This.w_UTCC=i_CoduTe
                Endif
            Endif
        Endif
        * Zucchetti Aulla Fine--- Gestione dati caricato da, alla data..

        *--- Zucchetti Aulla Inizio - Configurazione gestione
        If (Vartype(g_bDisableCfgGest)="U" Or !g_bDisableCfgGest)
            Do Case
                Case Upper(cEvent)=='NEW RECORD' Or (Upper(cEvent)=='INIT' And cp_getEntityType(This.cPrg) == "Dialog Window")
                    *--- Carico senza salvare, a F4 delle gestioni e alla init delle Dlg
                    If ! Used("_Curs_PRCFGGES")
                        * Non ho ancora finito l'applicazione di una configurazione
                        This.LoadCfg(.T.)
                    Endif
                Case Inlist(Upper(cEvent), 'EDIT ABORTED', 'DONE', 'RECORD UPDATED')
                    *--- Salvo e resetto nCfgGest perch� posso tornare il Query
                    This.SaveCfg()
                    This.nCfgGest=0
                Case Upper(cEvent)=='RECORD INSERTED'
                    *--- Salvo la cfg corrente
                    This.SaveCfg()
                Case Upper(cEvent)=='EDIT STARTED'
                    *--- Resetto la cfg se sono in edit, altrimenti vedrei la cfg selezionata nel menu
                    This.nCfgGest=0
            Endcase
        Endif
        * Zucchetti Aulla fine
        Local i,c,N,i_cParam
        cEvent=cEvent+','
        If !Isnull(This.oRunTime)
            N=Alen(This.oRunTime.RT_cEventName)
            For i=1 To N
                If cEvent=This.oRunTime.RT_cEventName[i]+','
                    c=This.oRunTime.RT_cEventCode[i]
                    &c
                Endif
            Next
        Endif
        **** Zucchetti Aulla inizio - Sincronizzazione
        cEvent=Left(Alltrim(cEvent), Len(Alltrim(cEvent))-1)
        If g_APPLICATION = "ad hoc ENTERPRISE" And Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_SINC)="C" And g_SINC='S'
            Local i_cParam
            Do Case
                Case cEvent='Delete end'
                    i_cParam='D'
                Case cEvent='Insert end'
                    i_cParam='I'
                Case cEvent='Update end'
                    i_cParam='U'
                Case cEvent='Delete start'
                    i_cParam='S'
                Case cEvent='Insert start'
                    i_cParam='K'
                Case cEvent='Update start'
                    i_cParam='V'
                Case cEvent='Delete row end'
                    i_cParam='DR'
                Case cEvent='Insert row end'
                    i_cParam='IR'
                Case cEvent='Update row end'
                    i_cParam='UR'
                Case cEvent='Delete row start'
                    i_cParam='SR'
                Case cEvent='Insert row start'
                    i_cParam='KR'
                Case cEvent='Update row start'
                    i_cParam='VR'
            Endcase
            If !Empty(i_cParam) And Type("g_SINC") = "C"
                If g_SINC = "S"
                    l_sPrg="GSSI_BSD"
                    Do (l_sPrg) With This,i_cParam
                Endif
            Endif
        Endif

        **** Zucchetti Aulla fine - Sincronizzazione

        **** Zucchetti Aulla inizio - Controllo flussi
        If Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And IsFlus(This)
            Local i_cParam
            Do Case
                Case cEvent='Delete start'
                    i_cParam='DS'
                Case cEvent='Insert start'
                    i_cParam='IS'
                Case cEvent='Update start'
                    i_cParam='US'
                Case cEvent='Delete row start'
                    i_cParam='DR'
                Case cEvent='Insert row start'
                    i_cParam='IR'
                Case cEvent='Update row start'
                    i_cParam='UR'
                Case cEvent='Record Deleted'
                    i_cParam='ET'
                Otherwise
                    i_cParam=''
            Endcase
            If !Empty(i_cParam)
                l_sPrg="GSCF_BCF"
                Do (l_sPrg) With This,i_cParam
            Endif
        Endif
        **** Zucchetti Aulla fine - Controllo flussi

        **** Zucchetti Aulla inizio - Modulo BCON
        If g_APPLICATION = "ad hoc ENTERPRISE" And ((Vartype(g_BCON)="C" And g_BCON = 'S') Or (Vartype(g_BPRC)="C" And g_BPRC='S')) And Atc('GSBC', This.cPrg)<> 0
            If  'NEW RECORD' $ Upper(cEvent) Or (Upper(cEvent)=='EDIT STARTED' And Type("this.pTipoMod") = 'C' And This.pTipoMod = 'P')
                This.InitializeCtrl(This.oPgFrm.PageCount-1)
            Endif
            If 'Blank'$ cEvent And (Type("this.cKeySelect") = 'U' Or Empty(This.cKeySelect))
                This.InitializeCtrl(This.oPgFrm.PageCount)
            Endif
            If Type('this.oCtrlGruppo') = 'O'
                If Upper(cEvent) = Upper(This.oCtrlGruppo.cFormVar) + ' CHANGED'
                    If Not(CheckGruppo(This.oCtrlGruppo, This.oCtrlImpresa))
                        This.oCtrlGruppo.Value = Space(15)
                        This.oCtrlGruppo.Valid()
                    Endif
                Endif
            Endif

        Endif

        **** Zucchetti Aulla fine - Modulo BCON

        If Inlist(m.cEvent,'Update row start','Insert row start','Delete row start')
            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
            Endif
        Endif

        Return

        **** Zucchetti Aulla Inizio - Modulo BCON
    Proc InitializeCtrl(pNumPage)
        ** Inizializza ed effettua il link per gruppo di consolidato ed impresa
        ** Il gruppo � inizializzato per i control con propriet� cLinkFile="GRUPPCON"
        ** L'impresa � inizializzata per i control con propriet� cLinkFile="SOCIETA"
        ** L'esercizio � inizializzato per i control con propriet� cLinkFile="ES_CONS"
        ** Il periodo � inizializzato per i control con propriet� cLinkFile = "PER_ESI"
        Local j,i,l_Link, obj, l_Value, l_oSource, l_OldPostin, l_NumPage, l_NumCtrl, l_nTab, l_nObj, l_DimArr
        Dimension obj_Array(20,3)
        l_nObj = 1
        l_NumPage = pNumPage
        For j=1 To l_NumPage
            l_NumCtrl = This.oPgFrm.Pages(j).oPag.ControlCount
            For i=1 To l_NumCtrl
                ** Inizializza il gruppo solo se non specificatamente espresso con la userdef sul campo bNoLink = .t.
                If Type('this.oPgFrm.Pages(j).oPag.Controls(i).clinkfile')='C' ;
                        AND Not(Type('this.oPgFrm.Pages(j).oPag.Controls(i).bNoLink') = 'L')
                    obj=This.oPgFrm.Pages(j).oPag.Controls(i)
                    l_Link = Upper(obj.clinkfile)
                    ** creo un array con l'ordine di tabulazione. Non eseguo subito il link per evitare che le calculate definite
                    ** sulla maschera rieffettuino i calcoli
                    If l_Link == "GRUPPCON" Or l_Link == "SOCIETA" Or l_Link == "ES_CONS" Or l_Link == "PER_ESI"
                        l_nTab = obj.TabIndex
                        obj_Array(l_nObj,1) = l_nTab
                        obj_Array(l_nObj,2) = i
                        obj_Array(l_nObj,3) = j
                        l_nObj = l_nObj + 1
                    Endif
                Endif
            Next
        Next
        ** Se � stato trovato almeno un control
        If l_nObj <> 1
            Dimension obj_Array(l_nObj-1,3)
            ** ordino l'array in base all'ordine di tabulazione
            If Asort(obj_Array,1) <> -1
                l_DimArr = l_nObj-1
                For i=1 To l_DimArr
                    obj=This.oPgFrm.Pages(obj_Array(i,3)).oPag.Controls(obj_Array(i,2))
                    l_Link = Upper(obj.clinkfile)
                    l_oSource = Createobject("_Osource")
                    Do Case
                        Case l_Link == "GRUPPCON"
                            l_Value = g_GRUPPO
                            l_oSource.xKey(1) = l_Value
                            If Not(Empty(l_Value))
                                This.oCtrlGruppo = obj
                            Endif
                        Case l_Link == "SOCIETA"
                            l_Value = g_IMPRESA
                            l_oSource.xKey(1) = g_GRUPPO
                            l_oSource.xKey(2) = l_Value
                            If Not(Empty(l_Value))
                                This.oCtrlImpresa = obj
                            Endif
                        Case l_Link == "ES_CONS"
                            l_Value = g_ESECONS
                            l_oSource.xKey(1) = l_Value
                        Case l_Link == "PER_ESI"
                            l_Value = g_PERCONS
                            l_oSource.xKey(1) = g_ESECONS
                            l_oSource.xKey(2) = l_Value
                    Endcase
                    If Not(Empty(l_Value))
                        obj.Value= l_Value
                        l_OldPostin = i_ServerConn[1,7]
                        i_ServerConn[1,7] = .F.
                        obj.ecpDrop(l_oSource)
                        i_ServerConn[1,7] = l_OldPostin
                        This.SaveDependsOn()
                    Endif
                Next
            Endif
        Endif
        Return

        **** Zucchetti Aulla fine - Modulo BCON

    Proc AddWarning(oPostIT)
        * --- aggiunge un post-in
        Local oldarea,kk,i
        If Not(This.bPostIt)
            cp_msg(cp_Translate(MSG_TABLE_DOES_NOT_ALLOW_POSTIN),.F.)
            Return
        Endif
        If Not(This.bLoaded)
            cp_msg(cp_Translate(MSG_SELECT_A_RECORD_QM),.F.)
            Return
        Endif
        i=Ascan(This.cWarn,oPostIT.CVar)
        If i<>0
            cp_msg(cp_Translate(MSG_POSTIN_ALREADY_LINKED_TO_A_RECORD),.F.)
            Return
        Endif
        oldarea=Select()
        kk=This.KeyToStr()
        * --- aggiunge il postit
        This.nWarn=This.nWarn+1
        Dimension This.cWarn[this.nWarn]
        This.cWarn[this.nWarn]=oPostIT.CVar
        * --- setta il postit come warn
        If i_ServerConn[1,2]=This.nPostItConn
            * --- il postit cambia stato, resta nello stesso server
            If i_ServerConn[1,2]<>0
                =cp_SQL(i_ServerConn[1,2],"update postit set status='W' where code="+cp_ToStrODBC(oPostIT.CVar))
            Else
                Update postit Set Status='W' Where Code=oPostIT.CVar
            Endif
        Else
            * --- il postit viene spostato di server
            If i_ServerConn[1,2]<>0
                =cp_SQL(i_ServerConn[1,2],"delete from postit where code="+cp_ToStrODBC(oPostIT.CVar))
            Else
                Delete From postit Where Code=oPostIT.CVar
            Endif
            If This.nPostItConn<>0
                =cp_SQL(This.nPostItConn,"insert into postit (code,usercode,createdby,status) values ("+cp_ToStrODBC(oPostIT.CVar)+","+cp_ToStrODBC(i_CoduTe)+","+cp_ToStrODBC(i_CoduTe)+",'W')")
            Else
                Insert Into postit (Code,usercode,createdby,Status) Values (oPostIT.CVar,i_CoduTe,i_CoduTe,'W')
            Endif
            oPostIT.bUpdated=.T.
            oPostIT.nconn=This.nPostItConn
        Endif
        * --- aggiunge il warning
        If Empty(oPostIT.cNewStatus)
            oPostIT.cStatus='W'
        Else
            oPostIT.cStatus=oPostIT.cNewStatus
            oPostIT.cNewStatus=''
        Endif
        If This.nPostItConn<>0
            cp_SQL(This.nPostItConn,"insert into cpwarn (tablecode,warncode) values ("+cp_ToStrODBC(kk)+","+cp_ToStrODBC(oPostIT.CVar)+")")
        Else
            Insert Into cpwarn (tablecode,warncode) Values (kk,oPostIT.CVar)
        Endif
        * ---
        Select (oldarea)
        If At("I",oPostIT.cStatus)=0
            cp_msg(cp_Translate(MSG_WARNING_ADDED),.F.)
        Else
            cp_msg(cp_Translate(MSG_WARNING_ADDED)+Chr(13)+Chr(10)+cp_Translate(MSG_IT_WILL_BE_INTEGRATED_IN_THE_WINDOW_WHEN_NEXT_RECORD_WILL_BE_READ_F),.F.)
        Endif
        Return
    Proc GetXKey()
        Local i_oldarea,i_pk
        i_oldarea=Select()
        Select (This.cKeySet)
        i_pk=This.cKeySelect
        Scatter Fields &i_pk To This.xKey
        Select (i_oldarea)
        Return
    Func KeyToStr()
        Local kk,i
        kk=''
        For i=1 To Alen(This.xKey)
            kk=kk+'\'+cp_ToStr(This.xKey[i],1)
        Next
        i='this.'+This.cFile+'_IDX'
        Return(cp_SetAzi(i_TableProp[&i,2])+kk)
    Proc InitAutonumber()
        Return
        *
        * --- EVENTI CODEPAINTER (generati da ToolBar o tasti funzione)
        *
        *--- Zucchetti Aulla Inizio - Controllo tasti funzione
    Function ah_HasCPEvents(i_cOp)
        Return .T.
    Func HasCPEvents(i_cOp)
        *--- Tramite il metodo ah_HasCPEvents(i_cOp) � possibile pilotare le funzioni di sistema, viene utilizzato un altro metodo per permette di inserire operazioni di default
        *--- come ad esempio la cancellazione di postin integrati
        *--- Controllo postin
        If Upper(This.cFunction)="QUERY" And Upper(i_cOp) = 'ECPEDIT' And Type('this.oPgFrm.Pages(this.oPgFrm.activepage).oWarning')<>'U'
            This.oPgFrm.Pages(This.oPgFrm.ActivePage).oWarning.ecpEdit()
            Return .F.
        Endif
        If Upper(i_cOp) = 'ECPDELETE' And Type('this.oPgFrm.Pages(this.oPgFrm.activepage).oWarning')<>'U'
            This.oPgFrm.Pages(This.oPgFrm.ActivePage).oWarning.ecpDelete()
            This.ReleaseWarn()
            This.LoadWarn()
            Return .F.
        Endif
        Return This.ah_HasCPEvents(m.i_cOp)
    Endfunc
    *--- Zucchetti Aulla Fine - Controllo tasti funzione
    * --- Cancellazione
    Proc ecpDelete()
        Local bRes,nRec,bErr
        If Type('this.oPgFrm.Pages(this.oPgFrm.activepage).oWarning')<>'U'
            This.oPgFrm.Pages(This.oPgFrm.ActivePage).oWarning.ecpDelete()
            This.ReleaseWarn()
            This.LoadWarn()
            Return
        Endif
        *--- Zucchetti Aulla Inizio - Disabilitata cancellazione da scheda Elenco
        If This.oPgFrm.ActivePage=This.oPgFrm.PageCount And Type('this.oPgFrm.Pages(this.oPgFrm.activepage).AutoZoom')='O'
            Return
        Endif
        *--- Zucchetti Aulla Fine - Disabilitata cancellazione da scheda Elenco
        If !This.bSec4
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            Return
        Endif
        If !This.CanDelete()
            Return
        Endif
        If !This.deletewarn()
            Return
        Endif
        If This.bDeleting
            Return
        Endif
        This.bDeleting=.T.
        If This.bLoaded And This.cFunction='Query'
            * --- se il dato e' vecchio deve essere riletto per avere i timestamp aggiornati
            If This.nLoadTime=0
                This.LoadRecWarn()  && rilettura se si cancella direttamente dopo un editing senza eseguire una query
            Endif
            * --- Richiesta conferma Cancellazione Record
            bRes=cp_YesNo(MSG_CONFIRM_DELETING_QP)
            If bRes And !This.CheckChildrenForDelete()
                bRes=cp_YesNo(MSG_DATA_EXIST_IN_CHILD_ENTITY_C_PROCEED_QP)
            Endif
            If bRes
                *--- Activity logger traccio la modifica di un record
                If i_nACTIVATEPROFILER>0
                    cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UF5", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
                Endif
                *--- Activity logger traccio la modifica di un record
                Thisform.LockScreen=.T.
                Select (This.cKeySet)
                nRec=Recno()
                cp_BeginTrs()
                **** Zucchetti Aulla inizio - controllo flussi
                This.cFlowStamp = Sys(2015)
                This.bFlowNoRules = .F.
                **** Zucchetti Aulla fine - controllo flussi
                This.LockScreen=.T.
                * Zucchetti aulla inizio - Gestione attributi
                If Type("this.bGestAtt")='O' And Vartype(i_curform.w_GESTGUID)='C'
                    * Elimino l'attributo associato (di fatto la funzione � una DELETE (utilizziamo un comando Painter per gestire bTrsErr in automatico e la connessione...)
                    ELIMINATT( i_curform.w_GESTGUID)
                Endif
                * Zucchetti aulla fine - Gestione attributi
                *--- Zucchetti Aulla Inizio - iRevolution
                If TYPE("g_REVI")="C" AND g_REVI=='S' AND This.bHasActions
                	Local l_ASSERIAL
                	m.l_ASSERIAL = cp_NewCCChk()
                	*--- Inserimento in coda
                	l_sPrg="GSRV_BIQ"
                 	Do (l_sPrg) with .null., This, "InsertQueue", m.l_ASSERIAL, .F., .F., .F., 'W', .T.
                EndIf
                *--- Zucchetti Aulla Fine - iRevolution                
                This.mDelete()
                bErr=bTrsErr
                This.LockScreen=.F.
                cp_EndTrs()
                *--- Zucchetti Aulla Inizio - iRevolution
                If Type("g_REVI")='C' And g_REVI='S' And This.bHasActions And Type("This.oRevInfSync") = 'O'
                	This.oRevInfSync.Visible=.F.
                Endif
                *--- Zucchetti Aulla Fine - iRevolution
                If Not(bErr)
                    This.NotifyEvent("Record Deleted")
                Endif
                This.QueryKeySet(This.cLastWhere,This.cLastOrderBy)
                If Reccount()>=nRec
                    Goto nRec
                Endif
                This.LoadRecWarn()
                **** Zucchetti Aulla inizio - controllo flussi
                If bTrsErr And Used("tmplog")
                    Use In tmplog
                Endif
                **** Zucchetti Aulla fine - controllo flussi
                **** Zucchetti Aulla inizio - Sincronizzazione
                *** Delete
                If g_APPLICATION = "ad hoc ENTERPRISE" And Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_SINC)="C" And g_SINC='S'
                    Local i_cParam
                    i_cParam='DM'
                    If !Empty(i_cParam) And Type("g_SINC") = "C"
                        If g_SINC = "S"
                            l_sPrg="GSSI_BSD"
                            Do (l_sPrg) With This,i_cParam
                        Endif
                    Endif
                Endif

                **** Zucchetti Aulla fine - Sincronizzazione
                Thisform.LockScreen=.F.
                This.Refresh()
            Endif
        Endif
        This.bDeleting=.F.
        Return
        * --- Variazione
    Proc ecpEdit()
        *--- controlla di essere in query
        If This.cFunction<>"Query"
            Return
        Endif
        *--- Vedo se la pagina attiva � un allegato
        If Type('this.oPgFrm.Pages(this.oPgFrm.activepage).oWarning')<>'U'
            This.oPgFrm.Pages(This.oPgFrm.ActivePage).oWarning.ecpEdit()
            Return
        Endif
        If !This.bSec3
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            Return
        Endif
        If This.bLoaded
            * --- Aggiunto IF per permettere il mantenimento della pagina attuale anche in F3
            This.oPgFrm.Pages[iif(VARTYPE(i_bFirstPage)='U' Or !i_bFirstPage,1,this.oPgFrm.activepage)].SetFocus() && forza una rilettura se l' utente ha modificato senza aver attivato la ricerca
        Endif
        If This.bLoaded
            If Seconds()>This.nLoadTime+60 Or This.bUpdated
                This.LoadRecWarn()  && rilettura se sono passati piu' di 60 secondi dall' ultima lettura o il record � stato variato in Interroga
            Endif
            If !This.CanEdit()
                Return
            Endif
            If !This.editwarn()
                Return
            Endif
            If This.oPgFrm.Pages(This.oPgFrm.ActivePage).Caption=MSG_LIST
                This.oPgFrm.ActivePage=1
                This.oPgFrm.Click()
            Endif
            This.cFunction="Edit"
            If This.oPgFrm.ActivePage=1 && Solo se sono gia in prima pagina eseguo il setfocus del primo control
                This.SetFocusOnFirstControl()
            Endif
            *--- Activity Logger traccio la modifica di un record
            If i_nACTIVATEPROFILER>0
                cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UF3", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
            Endif
            *--- Activity Logger traccio la modifica di un record
            This.SetStatus()
            If Type("this.ActiveControl.Name")='C' And Lower(This.ActiveControl.Name)=='obody'
                This.__dummy__.Enabled=.T.
                This.__dummy__.SetFocus()
            Endif
            This.mEnableControls()
            oCPToolBar.SetEdit()
            This.SetFocusOnFirstControl()
            This.__dummy__.Enabled=.F.
            This.NotifyEvent("Edit Started")
            This.Refresh()
        Endif
        Return
        * --- Filtro
    Proc ecpFilter()
        If This.cFunction="Query"
            This.cFunction = "Filter"
            *--- Activity Logger traccio la ricerca di un record
            If i_nACTIVATEPROFILER>0
                cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "U12", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
            Endif
            *--- Activity Logger traccio la ricerca di un record
            This.SetStatus()
            * --- Dall'ultima pagina (autozoom) non e' possibile fare filtro
            If This.oPgFrm.ActivePage = This.oPgFrm.PageCount
                This.oPgFrm.ActivePage=1
                This.oPgFrm.Click()
            Endif
            This.BlankRec()
            This.Refresh()
        Endif
        Return
        * --- Caricamento
    Proc ecpLoad()
        * --- Zucchetti Aulla Inizio, impedisco F4 se scaduta chiave di attivazione
        If Type( 'g_SCALIC' )='L' And g_SCALIC
            If Vartype(g_MSG_ACTIVATION_EXPIRED_CL_CANNOT_LOAD_NEW_DATA)='C' And Not Empty(g_MSG_ACTIVATION_EXPIRED_CL_CANNOT_LOAD_NEW_DATA)
                cp_ErrorMsg(g_MSG_ACTIVATION_EXPIRED_CL_CANNOT_LOAD_NEW_DATA,16,i_msgtitle)
            Else
                cp_ErrorMsg(MSG_ACTIVATION_EXPIRED_CL_CANNOT_LOAD_NEW_DATA,16,i_msgtitle)
            Endif
            Return
        Endif
        * --- Zucchetti Aulla Inizio
        If !This.bSec2
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            Return
        Endif
        If !This.CanAdd()
            Return
        Endif
        If !This.addwarn()
            Return
        Endif
        If This.cFunction='Query'
            If This.oPgFrm.ActivePage<>1
                This.oPgFrm.ActivePage=1
                This.oPgFrm.Click()
            Endif
            This.cFunction = "Load"
            *--- Activity Logger traccio caricamento di un record
            If i_nACTIVATEPROFILER>0
                cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UF4", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
            Endif
            *--- Activity Logger traccio caricamento di un record
            This.ReleaseWarn()
            This.SetStatus()
            This.BlankRec()
            This.InitAutonumber()
            This.NotifyEvent('New record')
            This.mEnableControls()
            oCPToolBar.SetEdit()
            This.cFunction="Filter"
            This.bDontReportError=.T.
            This.SetFocusOnFirstControl()
            This.cFunction = "Load"
            This.Refresh()
            This.bDontReportError=.F.
        Endif
        Return
        * --- F6
    Proc ecpF6()
        If Inlist(This.cFunction,'Edit','Load') And Type('this.ActiveControl.parent.oContained')='O'
            This.ActiveControl.Parent.oContained.F6()
        Endif
        Return
    Proc F6()
        Return
    Function FullRow()
        Return(.T.)
        * --- Record Precedente
    Proc ecpPrior()
        If This.cFunction='Query'
            This.SelectCursor()
            If .Not. Bof()
                This.LockScreen=.T.
                Skip -1
                This.LoadRecWarn()
                This.Refresh()
                This.LockScreen=.F.
            Endif
        Endif
        Return
        * --- Record Seguente
    Proc ecpNext()
        If This.cFunction='Query'
            This.SelectCursor()
            If .Not. Eof()
                This.LockScreen=.T.
                Skip 1
                This.LoadRecWarn()
                This.Refresh()
                This.LockScreen=.F.
            Endif
        Endif
        Return
        * --- Pagina Su
    Proc ecpPgUp()
        If Inlist(This.cFunction,'Edit','Load')
            If Type('this.ActiveControl')='O' And This.ActiveControl.BaseClass<>'Page'
                **** Zucchetti Aulla inizio - Gestione Buttonmenu
                **** If This.ActiveControl.BaseClass='Optionbutton' Or This.ActiveControl.Name='GRD'
                If (Type("This.ActiveControl.Parent.Parent.oContained")="O" And This.ActiveControl.BaseClass='Commandbutton') Or This.ActiveControl.BaseClass='Optionbutton' Or This.ActiveControl.Name='GRD'
                    **** Zucchetti Aulla fine   - Gestione Buttonmenu
                    This.ActiveControl.Parent.Parent.oContained.PgUp()
                Else
                    Local i_olderr
                    i_olderr=On('ERROR')
                    On Error =.T.
                    This.ActiveControl.Parent.oContained.PgUp()
                    On Error &i_olderr
                Endif
            Endif
        Else
            If This.oPgFrm.Pages(This.oPgFrm.ActivePage).Caption=MSG_LIST And Type('this.oPgFrm.Pages(this.oPgFrm.ActivePage).autozoom.Grd')='O'
                This.oPgFrm.Pages(This.oPgFrm.ActivePage).autozoom.Grd.DoScroll(2)
            Else
                This.PgUp()
            Endif
        Endif
        Return
    Proc PgUp()
        If This.oPgFrm.ActivePage>1
            This.oPgFrm.ActivePage=This.oPgFrm.ActivePage-1
            This.oPgFrm.Click()
        Endif
        Return
        * --- Pagina Giu'
    Proc ecpPgDn()
        If Inlist(This.cFunction,'Edit','Load')
            If Type('this.ActiveControl')='O' And This.ActiveControl.BaseClass<>'Page'
                **** Zucchetti Aulla inizio - Gestione Buttonmenu
                **** If This.ActiveControl.BaseClass='Optionbutton' Or This.ActiveControl.Name='GRD'
                If (Type("This.ActiveControl.Parent.Parent.oContained")="O" And This.ActiveControl.BaseClass='Commandbutton') Or This.ActiveControl.BaseClass='Optionbutton' Or This.ActiveControl.Name='GRD'
                    **** Zucchetti Aulla fine   - Gestione Buttonmenu
                    This.ActiveControl.Parent.Parent.oContained.PgDn()
                Else
                    Local i_olderr
                    i_olderr=On('ERROR')
                    On Error =.T.
                    This.ActiveControl.Parent.oContained.PgDn()
                    On Error &i_olderr
                Endif
            Endif
        Else
            If This.oPgFrm.Pages(This.oPgFrm.ActivePage).Caption=MSG_LIST And Type('this.oPgFrm.Pages(this.oPgFrm.ActivePage).autozoom.Grd')='O'
                This.oPgFrm.Pages(This.oPgFrm.ActivePage).autozoom.Grd.DoScroll(3)
            Else
                This.PgDn()
            Endif
        Endif
        Return
    Proc PgDn()
        If This.oPgFrm.ActivePage<This.oPgFrm.PageCount
            This.oPgFrm.ActivePage=This.oPgFrm.ActivePage+1
            This.oPgFrm.Click()
        Endif
        Return
        * --- Stampa
    Proc ecpPrint()
        Return
        * --- ecpQuery
    Procedure ecpQuery()
        This.LockScreen=.T.
        * ---
        This.cFunction = "Filter"  && possono esserci degli spostamenti, evita query spurie
        *this.SetEnabled('Query')
        If !This.bFirstQuery
            This.oPgFrm.ActivePage=1
            This.__dummy__.SetFocus()
            This.oFirstControl.SetFocus()
            This.__dummy__.Enabled=.F.
        Endif
        * ---
        This.cFunction = "Query"
        This.SetStatus()
        *this.BlankRec()
        This.SetFocusOnFirstControl()
        If !This.bFirstQuery
            This.mHideControls()
            * --- rilegge
            *this.LoadRecWarn()
            *this.Refresh()
            *this.nLoadTime=0
        Else
            This.BlankRec()
        Endif
        This.LockScreen=.F.
        This.bFirstQuery=.F.
        Return
        * --- Esc
    Proc ecpQuit()
        Local i_oldfunc
        i_oldfunc=This.cFunction
        If Inlist(This.cFunction,'Edit','Load')
            If This.IsAChildUpdated()
                If !cp_YesNo(MSG_DISCARD_CHANGES_QP)
                    Return
                Endif
            Endif
            This.bUpdated=.F.
            This.NotifyEvent('Edit Aborted')
        Endif
        *--- Activity Logger traccio ESC
        If i_nACTIVATEPROFILER>0
            cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UES", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
        Endif
        *--- Activity Logger traccio ESC
        * --- si sposta senza attivare i controlli
        This.cFunction='Filter'
        Local i_err
        i_err=On('error')
        On Error =.T.
        This.ActiveControl.Parent.oContained.bDontReportError=.T.
        On Error &i_err
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        * ---
        Do Case
            Case i_oldfunc="Query"
                This.NotifyEvent("Done")
                This.Hide()
                This.Release()
            Case i_oldfunc="Load"
                *this.QueryKeySet(this.cLastWhere,this.cLastOrderBy)
                *this.ecpQuery()
                *this.nLoadTime=0
                This.cFunction="Query"
                This.BlankRec()
                This.bLoaded=.F.
                This.SetStatus()
                This.SetFocusOnFirstControl()
            Case i_oldfunc="Edit"
                This.ecpQuery()
                This.nLoadTime=0
            Otherwise
                This.ecpQuery()
        Endcase
        Return
        * --- Salva
    Proc ecpSave()
        If This.bSaving
            Return
        Endif
        This.bSaving=.T.
        *--- Activity Logger traccio la cancellazione di un record
        If i_nACTIVATEPROFILER>0
            cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "U10",This.cFunction , This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
        Endif
        *--- Activity Logger traccio la cancellazione di un record
        Do Case
            Case This.cFunction = "Filter"
                If i_bSecurityRecord  And This.bSecDataFilter
                    Local i_SecFilter, w_ObjMask
                    This.bSecDataFilter=.F.
                    i_SecFilter=This.AddSonsFilter(This.BuildFilter(),This)
                    This.ecpQuery() && La Ecpquery deve andare prima dell'apertura della maschera con la sicurezza sul record...
                    w_ObjMask= Cp_recsec()
                    If Vartype(w_ObjMask)='O'
                        w_ObjMask.ecpLoad()
                        * valrizzo testata
                        setvaluelinked('ML',w_ObjMask,'w_TABLENAME', This.cFile)
                        setvaluelinked('DL',w_ObjMask,'w_DESCRI', This.cComment)
                        setvaluelinked('DL',w_ObjMask,'w_QUERYFILTER', i_SecFilter)
                    Endif
                Else
                    This.QueryKeySet(This.AddSonsFilter(This.BuildFilter(),This),"")
                    This.LoadRecWarn()
                    This.ecpQuery()
                Endif
            Case This.cFunction = "Edit"
                If This.TerminateEdit() And This.CheckForm()
                    cp_BeginTrs()
                    *  Zucchetti Aulla Inizio: modifica gestione padre anche se modificato solo un child

                    * --- Gestione dati caricato da, alla data..
                    If Vartype(This.w_UTDV)<>'U'
                        * --- Se presente nella gestione la data di caricamento
                        * --- allora forzo la scrittura, per contemplare anche
                        * --- eventuali modifiche ai figli
                        This.bUpdated=.T.
                        This.w_UTDV=SetInfoDate(This.cCalUtd)
                        If Vartype(This.w_UTCV)<>'U'
                            This.w_UTCV=i_CoduTe
                        Endif
                    Endif

                    *  Solo se attiva logistica remota, se la gestione non � gi� vista come modificata, se il figlio � modificato e se
                    *  la tabella relativa alla gestione � presente nelle entit� della logistica
                    If This.bUpdated=.F. And Vartype(g_LORE)='C' And g_LORE='S' And This.IsAChildUpdated()
                        * solo se � una gestione e quindi ha una tabella
                        If Type('this.cFile')='C'
                            If TISINENT(This.cFile)
                                * Forzo l'aggiornamento della gestione
                                This.bUpdated=.T.
                            Endif
                        Endif
                    Endif
                    **** Zucchetti Aulla inizio - Gestione modulo controllo flussi
                    This.cFlowStamp = Sys(2015)
                    This.bFlowNoRules = .F.
                    *  Zucchetti Aulla Fine - Gestione modulo controllo flussi
                    
                    *--- Zucchetti Aulla Inizio - iRevolution
                    If TYPE("g_REVI")="C" AND g_REVI=='S' And This.bHasActions
                    	LOCAL l_ASSERIAL
                    	l_ASSERIAL = cp_NewCCChk()
                    	*--- Inserimento in coda, azione ancora da non eseguire
                    	l_sPrg="GSRV_BIQ"
                      	Do (l_sPrg) with .null., This, "InsertQueue", m.l_ASSERIAL, .F., .F., .F., '.'
                    	*--- Teoricamente dovrei ripulire la coda nel caso l'update non vada a buon fine
                    	*--- ma non � un grosso problema, non essendo variato il crc il record non verr� re-inviato
                    EndIf
                    *--- Zucchetti Aulla Fine - iRevolution                    
                    
                    This.LockScreen=.T.
                    This.mReplace(.T.)
                    **** Zucchetti Aulla inizio - Sincronizzazione
                    *** Update
                    If g_APPLICATION = "ad hoc ENTERPRISE" And Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_SINC)="C" And g_SINC='S'
                        Local i_cParam
                        i_cParam='UM'
                        If !Empty(i_cParam) And Type("g_SINC") = "C"
                            If g_SINC = "S"
                                l_sPrg="GSSI_BSD"
                                Do (l_sPrg) With This,i_cParam
                            Endif
                        Endif
                    Endif

                    **** Zucchetti Aulla fine - Sincronizzazione
                    
                    **** Zucchetti Aulla inizio - Regole di controllo
                    If  Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_REVI)="C" And g_REVI='S' AND ALLTRIM(i_CODAZI) $ AssocLst()
                                l_sPrg="GSRV_BCS"
                                Do (l_sPrg) With This
                    Endif
                    **** Zucchetti Aulla fine - Regole di controllo
                    
                    This.LockScreen=.F.
                    cp_EndTrs(This.bTrsDontDisplayErr)
                    **** Zucchetti Aulla inizio - Gestione modulo controllo flussi
                    If Type("this.cFile")="C" And Not Empty(This.cFile) And IsFlus(This)
                        If bTrsErr
                            Use In Select("tmplog")
                        Else
                            Local l_sPrg,i_cParam
                            l_sPrg="GSCF_BCF"
                            i_cParam='ET'
                            Do (l_sPrg) With This,i_cParam
                        Endif
                    Endif
                    ****  Zucchetti Aulla Fine - Gestione modulo controllo flussi
                    *--- Zucchetti Aulla Inizio - iRevolution
                    If .Not. bTrsErr AND TYPE("g_REVI")="C" AND g_REVI=='S' And This.bHasActions
                    	*--- Aggiornamento coda, azione in attesa, viene verificata l'esecuzione immediata
                    	l_sPrg="GSRV_BIQ"
                      	Do (l_sPrg) with .null., This, "InsertQueue", m.l_ASSERIAL, .T.
                    	This.CheckiRevolutionSync()
                    EndIf
                    *--- Zucchetti Aulla Fine - iRevolution
                    
                    If .Not. bTrsErr
                        This.NotifyEvent("Record Updated")
                        This.ecpQuery()
                        This.nLoadTime=0
                        This.bUpdated=.F.
                    Else
                        If !Empty(This.cTrsName)
                            Select (This.cTrsName)
                            Go Top
                            This.WorkFromTrs()
                            * ---Se ho problemi dopo la replace end raddoppia le righe
                            This.SetControlsValue()
                            This.ChildrenChangeRow()
                            This.oPgFrm.Page1.oPag.oBody.nAbsRow=1
                            This.oPgFrm.Page1.oPag.oBody.nRelRow=1
                        Endif
                        This.NotifyEvent("Edit Restarted")
                    Endif
                Endif
            Case This.cFunction = "Load"
                If This.TerminateEdit() And This.CheckForm()
                    cp_BeginTrs()
                    **** Zucchetti Aulla inizio - controllo flussi
                    This.cFlowStamp = Sys(2015)
                    This.bFlowNoRules = .F.
                    **** Zucchetti Aulla fine - controllo flussi
                    This.LockScreen=.T.
                    If This.mInsert()
                        This.mReplace(.F.)
                    Endif
                    **** Zucchetti Aulla inizio - Sincronizzazione
                    *** Insert
                    If g_APPLICATION = "ad hoc ENTERPRISE" And Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_SINC)="C" And g_SINC='S'
                        Local i_cParam
                        i_cParam='IM'
                        If !Empty(i_cParam) And Type("g_SINC") = "C"
                            If g_SINC = "S"
                                l_sPrg="GSSI_BSD"
                                Do (l_sPrg) With This,i_cParam
                            Endif
                        Endif
                    Endif

                    **** Zucchetti Aulla fine - Sincronizzazione
                    
                                        *--- Zucchetti Aulla Fine - iRevolution  
                    
                                        **** Zucchetti Aulla inizio - Regole di controllo
                    If  Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_REVI)="C" And g_REVI='S' AND ALLTRIM(i_CODAZI) $ AssocLst()
                                l_sPrg="GSRV_BCS"
                                Do (l_sPrg) With This
                    Endif
                    **** Zucchetti Aulla fine - Regole di controllo
                    
                    This.LockScreen=.F.
                    cp_EndTrs(This.bTrsDontDisplayErr)
                    **** Zucchetti Aulla inizio - Gestione modulo controllo flussi
                    If Type("this.cFile")="C" And Not Empty(This.cFile) And IsFlus(This)
                        If bTrsErr
                            Use In Select("tmplog")
                        Else
                            Local l_sPrg,i_cParam
                            l_sPrg="GSCF_BCF"
                            i_cParam='ET'
                            Do (l_sPrg) With This,i_cParam
                        Endif
                    Endif
                    ****  Zucchetti Aulla Fine - Gestione modulo controllo flussi
                    *--- Zucchetti Aulla Inizio - iRevolution
                    If .Not. bTrsErr AND TYPE("g_REVI")="C" AND g_REVI=='S'
                    	Local l_unknown
                    	l_unknown = Not Pemstatus(This,"bHasActions",5)
                    	If l_unknown
                    		This.AddProperty("bHasActions",.F.)
                    	Endif
                    	If l_unknown Or This.bHasActions
                    		*--- Inserimento coda, azione in attesa, non deve esserne verificata l'esecuzione immediata
                    		l_sPrg="GSRV_BIQ"
                    		Do (l_sPrg) with .null., This, "InsertQueue", .F., .F., .F., .F., 'W', .T.
	                    EndIf
                    EndIf
                                     
                    If .Not. bTrsErr
                        This.NotifyEvent("Record Inserted")
                        This.BlankRec()
                        This.InitAutonumber()
                        This.NotifyEvent('New record')
                        This.bUpdated=.F.
                    Else
                        If !Empty(This.cTrsName)
                            Select (This.cTrsName)
                            Go Top
                            This.WorkFromTrs()
                            * ---Se ho problemi dopo la replace end raddoppia le righe
                            This.SetControlsValue()
                            This.ChildrenChangeRow()
                            This.oPgFrm.Page1.oPag.oBody.nAbsRow=1
                            This.oPgFrm.Page1.oPag.oBody.nRelRow=1
                        Endif
                    Endif
                    This.cFunction='Filter'  && disabilita when e valid
                    * !!! greg 26/6/96
                    Local b
                    b=This.oFirstControl.Enabled
                    This.oFirstControl.Enabled=.T.
                    This.SetFocusOnFirstControl()
                    This.__dummy__.Enabled=.F.
                    This.oPgFrm.ActivePage=1
                    This.SetFocusOnFirstControl()
                    If !Empty(This.cTrsName)
                        Select (This.cTrsName)
                        Go Top
                    Endif
                    * !!!
                    This.Refresh()
                    This.cFunction='Load'
                    This.oFirstControl.Enabled=m.b
                    This.mEnableControls()
                Endif
        Endcase
        This.bTrsDontDisplayErr=.F.
        This.bSaving=.F.
        Return
        * Attivo la modalit� di raccolta informazioni filtro....
    Procedure ecpSecurityRecord()
        If i_bSecurityRecord  And cp_IsAdministrator(.T.) And This.cFunction = "Query"
            This.bSecDataFilter=.T.
            This.ecpFilter()
        Endif
    Endproc

    Proc ecpSecurity()
        *if this.oPgFrm.ActivePage=this.oPgFrm.pagecount
        If Type('this.oPgFrm.Pages(this.oPgFrm.ActivePage).caption')='C' And This.oPgFrm.Pages(This.oPgFrm.ActivePage).Caption=MSG_LIST
            Createobject('stdsProgAuth','Z*'+This.getSecurityCode())
        Else
            Createobject('stdsProgAuth',This.getSecurityCode())
        Endif
        Return
    Func getSecurityCode()
        * --- Zucchetti Aulla Inizio aggiungo parametri al controllo della sicurezza
        Local nameprg
        nameprg=This.cPrg
        If Type("oParentObject")='C'
            nameprg=nameprg+','+oParentObject
        Else
            If Type("this.oParentObject")='C'
                nameprg=nameprg+','+This.oParentObject
            Endif
        Endif
        Return(nameprg) && Era solo return(this.cPrg)
        * --- Zucchetti Aulla Fine aggiungo parametri al controllo della sicurezza

    Proc ecpInfo()
        Local i_olderr,i_a,i_n,i_fxpdate,i_fxpsize
        Dimension i_a[1]
        Private i_err
        i_olderr=On('ERROR')
        i_err=.F.
        On Error i_err=.T.
        cp_info(This)
        If i_err
            *wait window this.class+' '+i_cBmpPath
            i_n=Substr(This.Class,2)
            i_fxpsize=0
            i_fxpdate={//}
            Do Case
                Case Adir(i_a,i_n+'.fxp')=1
                    i_fxpsize=i_a[2]
                    i_fxpdate=i_a[3]
                Case Adir(i_a,'..\vfcssrc\'+i_n+'.fxp')=1
                    i_fxpsize=i_a[2]
                    i_fxpdate=i_a[3]
                Case Adir(i_a,i_cBmpPath+i_n+'.fxp')=1
                    i_fxpsize=i_a[2]
                    i_fxpdate=i_a[3]
            Endcase
            cp_ErrorMsg(cp_Translate(MSG_NAME)+MSG_FS+Upper(i_n)+Chr(13)+Chr(10)+;
                cp_Translate(MSG_VERSION)+MSG_FS+This.infodaterev+Chr(13)+Chr(10)+;
                cp_Translate(MSG_FXP_SIZE)+MSG_FS+Alltrim(Str(i_fxpsize))+Chr(13)+Chr(10)+;
                cp_Translate(MSG_FXP_DATE)+MSG_FS+Dtoc(i_fxpdate);
                ,48,MSG_PROGRAM_INFO,.F.)
        Endif
        On Error &i_olderr
        Return
        * --- Zoom
    Proc ecpZoom()
        Local i_cErrSav,i_nZoomPage
        If This.cFunction='Load' Or This.cFunction='Edit'
            i_cErrSav=On('ERROR')
            On Error =.T.  && potrebbe non esistere il metodo per il control attivo
            This.ActiveControl.Parent.oContained.bDontReportError=.T.
            This.ActiveControl.mZoom()
            On Error &i_cErrSav
        Else
            If This.cFunction='Query'
                i_nZoomPage=This.oPgFrm.PageCount
                Do While Type('this.oPgFrm.Pages(i_nZoomPage).autozoom')='U' And i_nZoomPage>0
                    i_nZoomPage=i_nZoomPage-1
                Enddo
                If i_nZoomPage>0
                    This.oPgFrm.ActivePage=i_nZoomPage
                    This.oPgFrm.Click()
                Endif
            Endif
        Endif
        Return
    Proc ecpZoomOnZoom()
        Local i_cErrSav,i_nZoomPage
        If This.cFunction='Load' Or This.cFunction='Edit'
            i_cErrSav=On('ERROR')
            On Error =.T.  && potrebbe non esistere il metodo per il control attivo
            This.ActiveControl.Parent.oContained.bDontReportError=.T.
            This.ActiveControl.mZoomOnZoom()
            On Error &i_cErrSav
        Endif
        Return
        * --- Refresh form
    Proc ecpRefresh()
        If This.oPgFrm.ActivePage=This.oPgFrm.PageCount And Vartype(This.oPgFrm.Pages(This.oPgFrm.PageCount).autozoom)="O"
            This.oPgFrm.Pages(This.oPgFrm.PageCount).autozoom.qry.Click()
        Endif
        If This.cFunction = "Query"
            This.LoadRecWarn()
        Endif
        This.NotifyEvent("ecpRefresh")
        This.Refresh()
        Return
    Func IsVar(cVarName)
        Return(Type("this."+cVarName)<>'U')
    Func GetVar(cVarName)
        Return(This.&cVarName)
    Proc SetVar(cVarName,xValue)
        This.&cVarName=m.xValue
    Func GetHelpFile()
        Return(i_CpDic)
    Func GetHelpTopic()
        *--- Zucchetti aulla La gestione dell'Help sempre in Upper per mantenere il collegamento
        * --- tra gli help attuali e i titoli normalizzati delle maschere.
        Return(Upper(This.cComment))
    Func CanEdit()
        Return(.T.)
    Func CanAdd()
        Return(.T.)
    Func CanAddRow()
        Return(.T.)
    Func CanDelete()
        Return(.T.)
    Proc GetCtrl(i_cName, nOccurrence, nPag, cClassName, cPropName)
        Local i,e,c,j, nCount
        LOCAL TestMacro
        nCount=0 &&Contatore delle occorrenze
        nOccurrence=Iif(Vartype(m.nOccurrence)="N", m.nOccurrence, 1)
        nPagInit=Iif(Vartype(m.nPag)="N", m.nPag, 1)
        nPagFin=Iif(Vartype(m.nPag)="N", m.nPag , This.oPgFrm.PageCount)
        cClassName=Iif(Vartype(m.cClassName)='C' And !Empty(m.cClassName), Alltrim(m.cClassName), "" )
        cPropName=Iif(Vartype(m.cPropName)='C' And !Empty(m.cPropName), Alltrim(m.cPropName), "")
        Private i_bErr
        If Type("this."+i_cName)='O'
            Return(This.&i_cName)
        Endif
        i_cName=Upper(i_cName)
        e=On('ERROR')
        On Error i_bErr=.T.
        For j=m.nPagInit To m.nPagFin
            For i=1 To This.oPgFrm.Pages(j).oPag.ControlCount
                i_bErr=.F.
                If !Empty(m.cPropName)
                TestMacro=This.oPgFrm.Pages(j).oPag.Controls(i).&cPropName
                    If Upper(TestMacro)==i_cName
                        If !i_bErr
                            nCount = nCount+1
                            If nCount = nOccurrence
                                On Error &e
                                Return(This.oPgFrm.Pages(j).oPag.Controls(i))
                            Endif
                        Endif
                    Endif
                Else
                    If Upper(This.oPgFrm.Pages(j).oPag.Controls(i).Name)==i_cName
                        If !i_bErr
                            nCount = nCount+1
                            If nCount = nOccurrence
                                On Error &e
                                Return(This.oPgFrm.Pages(j).oPag.Controls(i))
                            Endif
                        Endif
                    Endif
                Endif
                i_bErr=.F.
                If Empty(m.cClassName) Or Empty(m.cPropName)
                    c=Upper(This.oPgFrm.Pages(j).oPag.Controls(i).Class)
                    If Left(c,3)='STD' And !Inlist(c,'STDBUTTON','STDBODY','STDBOX','STDSTRING') And Upper(This.oPgFrm.Pages(j).oPag.Controls(i).cFormVar)==i_cName
                        If !i_bErr
                            nCount = nCount+1
                            If nCount = nOccurrence
                                On Error &e
                                Return(This.oPgFrm.Pages(j).oPag.Controls(i))
                            Endif
                        Endif
                    Endif
                    i_bErr=.F.
                    If (c='STDSTRING' Or c='STDBUTTON' Or c='CP_CALCLBL'  Or c='ADVBUTTONMENU' ) And Upper(This.oPgFrm.Pages(j).oPag.Controls(i).Caption)==Upper(i_cName)
                        If !i_bErr
                            nCount = nCount+1
                            If nCount = nOccurrence
                                On Error &e
                                Return(This.oPgFrm.Pages(j).oPag.Controls(i))
                            Endif
                        Endif
                    Endif
                    i_bErr=.F.
                    If (c='STDBOX'Or c='STDSTRING' Or c='STDBUTTON' Or c='CP_CALCLBL'  Or c='BUTTONMENU' Or c='ADVBUTTONMENU' ) And Upper(This.oPgFrm.Pages(j).oPag.Controls(i).uid)=Upper(i_cName)
                        If !i_bErr
                            nCount = nCount+1
                            If nCount = nOccurrence
                                On Error &e
                                Return(This.oPgFrm.Pages(j).oPag.Controls(i))
                            Endif
                        Endif
                    Endif
                ELSE
                TestMacro=Upper(This.oPgFrm.Pages(j).oPag.Controls(i).Class)=m.cClassName And Upper(This.oPgFrm.Pages(j).oPag.Controls(i).&cPropName)==i_cName
                    If TestMacro
                        If !i_bErr
                            nCount = nCount+1
                            If nCount = nOccurrence
                                On Error &e
                                Return(This.oPgFrm.Pages(j).oPag.Controls(i))
                            Endif
                        Endif
                    Endif
                Endif	&&EMPTY(m.cClassName)
            Next
        Next
        On Error &e
        Return(.Null.)
        *--- Ricerca nOccurrence-esima ricorrenza del controllo
    Proc GetBodyCtrl(i_cName, nOccurrence)
        Local i,e,c, nCount
        nCount=0 &&Contatore delle occorrenze
        nOccurrence=Iif(Type("nOccurrence")="N", nOccurrence, 1)
        Private i_bErr
        If Type("this."+i_cName)='O'
            Return(This.&i_cName)
        Endif
        e=On('ERROR')
        On Error i_bErr=.T.
        For i=1 To This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.ControlCount
            c=Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(i).Class)
            i_bErr=.F.
            If  Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(i).Name)==i_cName
                nCount = nCount+1
                If nCount = nOccurrence
                    On Error &e
                    Return(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(i))
                Endif
            Endif
            If Left(c,3)='STD' And !Inlist(c,'STDBUTTON','STDBODY','STDBOX','STDSTRING') And Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(i).cFormVar)==Upper(i_cName)
                If !i_bErr
                    nCount = nCount+1
                    If nCount = nOccurrence
                        On Error &e
                        Return(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(i))
                    Endif
                Endif
            Endif
            i_bErr=.F.
            If (c='STDBUTTON' Or c='BUTTONMENU'  Or c='ADVBUTTONMENU') And (Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(i).Caption)==Upper(i_cName) Or Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(i).uid)==Upper(i_cName))
                If !i_bErr
                    nCount = nCount+1
                    If nCount = nOccurrence
                        On Error &e
                        Return(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(i))
                    Endif
                Endif
            Endif
        Next
        On Error &e
        Return(.Null.)
    Proc Paint()
        * In alcune situazioni, con programmi molto carichi e soprattutto con tanti control
        * dentro le griglie, viene chiamato ripetutamente la funzione di Paint.
        * Il sistema non si accorge di aver gia' ridisegnato la finestra e continua a chiamare
        * la funzione di Paint.
        * Con questa patch, dopo 6 chiamate consecutive, forziamo una refresh che blocca il
        * firing continuo dell' evento Paint
        If Not This._bPaint_ErrorGrid
            Local seconds_
            seconds_=Int(Seconds())
            If seconds_=This._paint_error_s
                This._paint_error_c=This._paint_error_c+1
            Else
                This._paint_error_c=0
            Endif
            This._paint_error_s=seconds_
            If This._paint_error_c>=6
                This.Refresh()
                This._paint_error_c=0
            Endif
        Else
            This._bPaint_ErrorGrid=.F.
        Endif
        Return
    Proc TrsFromWork()
        Return
    Func AddSonsFilter(i_cFlt,i_oTopObject)
        Return i_cFlt
        ***** Zucchetti Aulla Inizio - Schedulatore
    Proc ecpReadPar()
        If g_JBSH='S' Or g_IZCP='S'
            l_sPrg="gsjb_bgs"
            Do (l_sPrg) With This
        Endif
        Return
        ***** Zucchetti Aulla Fine - Schedulatore

        ***** Zucchetti Aulla Inizio - Configurazione gestione
    Procedure SaveCfg()
        *--- Salvo cfg
        If This.nCfgGest<>0
            Local l_sPrg
            l_sPrg="GSUT_BCG(This, 'S')"
            &l_sPrg
        Endif
    Endproc

    Procedure LoadCfg(bNoSave)
        *--- Creo cursore con le configurazioni, la prima di default � quella che devo utilizzare (query ordinata)
        If bNoSave
            *--- Svuoto la vecchia cfg affinch� non venga salvata
            This.nCfgGest=0
        Endif
        Local l_sPrg
        l_sPrg="GSUT_BCG(This, 'T')"
        &l_sPrg
        Local l_rownum
        If Used("CURSCFGGEST")
            Select("CURSCFGGEST")
            Go Top
            *--- Se di default
            If CURSCFGGEST.CGDEFCFG='S'
                l_sPrg="GSUT_BCG(This, 'L', CURSCFGGEST.CPROWNUM)"
                &l_sPrg
            Endif
            Use In Select("CURSCFGGEST")
        Endif
        This.NotifyEvent('CFGLoaded')
    Endproc
    *--- Zucchetti Aulla Fine - Configurazione gestione


    Procedure SetCCCHKVarInsert(i_ccchkf,i_ccchkv,i_nTblIdx,i_nConn)
        If i_TableProp[i_nTblIdx,7]=1
            i_ccchkf=',CPCCCHK'
            i_ccchkv=','+cp_ToStrODBC(cp_NewCCChk())
        Else
            i_ccchkf=''
            i_ccchkv=''
        Endif
        *ACTIVATE SCREEN
        *? '-->',i_ccchkf,i_ccchkv
        Return

    Procedure SetCCCHKVarReplace(i_ccchkf,i_ccchkw,i_bEditing,i_nTblIdx,i_nConn,i_TrsName)
        Local i_NF
        If i_TableProp[i_nTblIdx,7]=1
            If Not i_TrsName
                i_NF=This.cCursor
            Else
                i_NF=This.cTrsName
            Endif
            i_OldCCCHK = Iif(i_bEditing,&i_NF..CPCCCHK,'')
            i_ccchkf=',CPCCCHK='+cp_ToStrODBC(cp_NewCCChk())
            If i_nConn<>0
                i_ccchkw=' and CPCCCHK='+cp_ToStrODBC(i_OldCCCHK)
            Else
                i_ccchkw=' and CPCCCHK=='+cp_ToStrODBC(i_OldCCCHK)
            Endif
        Else
            i_ccchkf=''
            i_ccchkw=''
        Endif
        *ACTIVATE SCREEN
        *? '-->',i_ccchkf,i_ccchkw
        Return

    Procedure SetCCCHKCursor(i_ccchkf,i_nTblIdx)
        Local i_NF
        If i_TableProp[i_nTblIdx,7]=1
            i_NF=This.cTrsName
            Select (i_NF)
            Replace CPCCCHK With Substr(i_ccchkf,11,10)
        Endif
        Return

    Procedure SetCCCHKVarDelete(i_ccchkw,i_nTblIdx,i_nConn)
        Local i_NF
        If i_TableProp[i_nTblIdx,7]=1
            i_NF=This.cCursor
            If i_nConn<>0
                i_ccchkw=' and CPCCCHK='+cp_ToStrODBC(&i_NF..CPCCCHK)
            Else
                i_ccchkw=' and CPCCCHK=='+cp_ToStrODBC(&i_NF..CPCCCHK)
            Endif
        Else
            i_ccchkw=''
        Endif
        *ACTIVATE SCREEN
        *? '-->',i_ccchkw
        Return

    Function editwarn()
        Return .T.

    Function addwarn()
        Return .T.

    Function deletewarn()
        Return .T.

    Function GetcKeyWhereODBC()
        Local i_cKeyWhereODBC
        i_cKeyWhereODBC="'1=0'"
        If Vartype(This.cKeyWhereODBC)='C'
            i_cKeyWhereODBC=This.cKeyWhereODBC
        Endif
        Return &i_cKeyWhereODBC

    Function GetcKeyDetailWhereODBC()
        Local i_cKeyWhereODBC, i_TN
        i_cKeyWhereODBC="'1=0'"
        i_TN=This.cTrsName
        If Vartype(This.cKeyDetailWhereODBC)='C'
            i_cKeyWhereODBC=Strtran(This.cKeyDetailWhereODBC, 'i_TN.->', '&i_TN.->')
        Endif
        Return &i_cKeyWhereODBC

        ***** Zucchetti Aulla Inizio - Sincronizzatore
    Proc ecpSinc()
        If Type('g_SINC')="C" And g_SINC='S'
            l_sPrg="GSSI_BCR"
            Do (l_sPrg) With This
        Endif
        Return
        ***** Zucchetti Aulla Fine - Sincronizzatore
        ***** Zucchetti Aulla Inizio - Controllo flussi
    Function FormFlus()
        Local actctrl, actform, cFile
        actctrl=Iif(Type('this.ActiveControl')='O', This.ActiveControl, This)
        If Pemstatus(actctrl,"cTrsName", 5) And Pemstatus(actctrl,"cFile", 5) And Not Empty(actctrl.cFile)
            actform=This
            cFile=actform.cFile
        Else
            If Vartype(actctrl)="O"
                actform=actctrl
                Do While Empty(cFile) And Type("actform.parent")="O"
                    actform=actform.Parent
                    cFile=Iif(Pemstatus(actform,"cFile", 5) And Pemstatus(actform,"cTrsName", 5), actform.cFile , "")
                Enddo
                If Empty(cFile)
                    * NON HO TROVATO NULLA, USO THIS (LA FORM ATTIVA)
                    actform=This
                Endif
            Else
                actform=This
            Endif
        Endif
        Return Iif(Empty(cFile), .Null., actform)
    Endfunc
    Proc ecpLoadFlus()
        Local actform
        actform=This.FormFlus()
        If Vartype(actform)="O" And IsFlus(actform)
            l_sPrg="GSCF_BCF"
            Do (l_sPrg) With actform, "RE+"
        Endif
        Return
    Endproc

    Proc ecpCtrlFlus()
        Local actform
        actform=This.FormFlus()
        If Vartype(actform)="O" And IsFlus(actform)
            l_sPrg="GSCF_BCF"
            Do (l_sPrg) With actform, "RE"
        Endif
        Return
    Endproc

    Proc ecpViewFlus()
        Local actform
        actform=This.FormFlus()
        If Vartype(actform)="O" And IsFlus(actform)
            l_sPrg="GSCF_BCF"
            Do (l_sPrg) With actform, "VW"
        Endif
        Return
    Endproc
    ***** Zucchetti Aulla Fine - Controllo flussi
Enddefine

* === PageFrame Standard
Define Class StdPageFrame As CPPageFrame
    bInAutoZoom=.F.
    * disabilita loadrec nel caso di spostamento da zoom di elenco a gestione
    bAutoZoomLoad=.F.
    TabStyle=1
    nOldWs=0
    nOldHs=0
    nOldWz=0
    nOldHz=0
    ExtFldsPage=-1
    bSetFont = .T.
    oRefHeaderObject=Null
    Proc Init()
        * --- aggiunge la pagina di autozoom, con dentro un autozoom fasullo.
        *     il vero zoom verra' creato solo clickkandoci sopra
        This.Pages(This.PageCount).AddObject("autozoom","commandbutton")
        This.Pages(This.PageCount).Caption=cp_Translate(MSG_LIST)
        This.Height=This.Parent.Height
        If This.bSetFont
            This.SetFont()
        Endif
    Procedure SetFont()
        Local oPage, l_i
        If i_VisualTheme=-1 Or i_cMenuTab="S"
            For l_i=1 To This.PageCount
                oPage = This.Pages[m.l_i]
                oPage.FontName = SetFontName('P', Thisform.FontName, Thisform.bGlobalFont)
                oPage.FontSize = SetFontSize('P', Thisform.FontSize, Thisform.bGlobalFont)
                oPage.FontItalic = SetFontItalic('P', Thisform.FontItalic, Thisform.bGlobalFont)
                oPage.FontBold = SetFontBold('P', Thisform.FontBold, Thisform.bGlobalFont)
            Endfor
        Endif
    Endproc
    Proc Click()
        *cp_dbg('click'+str(this.ActivePage,3,0)+str(this.PageCount,3,0))
        *thisform.lockscreen=.t.
        *--- Controllo cambio pagina, evita il rilancio del evento ActivatePage quando si preme ripetutamente sul tab
        If This.nPageActive <> This.ActivePage
            Local i_nZoomPage,i_nWarningPage
            i_nZoomPage=This.PageCount
            i_nWarningPage=0
            This.Parent.NotifyEvent('ActivatePage '+Ltrim(Str(This.ActivePage,2,0)))
            Do While Type('this.pages(i_nZoomPage).autozoom')='U' And i_nZoomPage>0
                i_nZoomPage=i_nZoomPage-1
            Enddo
            If Type('this.pages(this.PageCount).oWarning')<>'U'
                i_nWarningPage=This.PageCount
            Endif
            If This.ActivePage=i_nZoomPage
		if Vartype(g_AutoZoomLoad)='L' and g_AutoZoomLoad
			This.Parent.ReleaseWarn()
		endif
                If This.nOldWz<>0
                    Thisform.LockScreen=.T.
                    This.nOldWs=Thisform.Width
                    This.nOldHs=Thisform.Height
                    Thisform.Width=This.nOldWz
                    Thisform.Height=This.nOldHz
                    Thisform.LockScreen=.F.
                Endif
                This.bInAutoZoom=.T.
                If This.Pages(i_nZoomPage).autozoom.Class='Commandbutton'
                    This.nOldWs=Thisform.Width
                    This.nOldHs=Thisform.Height
                    * --- c'era l' autozoom fasullo, quindi ci mette quello vero
                    This.Pages(i_nZoomPage).RemoveObject('autozoom')
                    This.Pages(i_nZoomPage).AddObject("autozoom","zoombox", This.Parent.bGlobalFont)
                    This.Pages(i_nZoomPage).autozoom.cKeyFields=This.Parent.cKeySelect
                    *--- Valorizzo cZoomOnZoom in modo che si valorizzi anche il campo "Ricerca" nelle propriet� dello zoom
                    This.Pages(i_nZoomPage).autozoom.cZoomOnZoom = Thisform.cPrg
                    If Empty(This.Parent.cAutoZoom)
                        This.Pages(i_nZoomPage).autozoom.SetFileAndFields(This.Parent.cFile,'*',This.Parent.cQueryFilter)
                    Else
                        This.Pages(i_nZoomPage).autozoom.SetFileAndFields(This.Parent.cFile,'*',This.Parent.cQueryFilter,This.Parent.cAutoZoom+'.'+This.Parent.cFile+'_VZM')
                    Endif
                    If i_VisualTheme <> -1  And Vartype(_Screen.TBMDI)<>"U" And Thisform.bTBMDI And _Screen.TBMDI.Visible
                        Thisform.Width=This.nOldWs
                        Thisform.Height=This.nOldHs
                    Endif
                    If i_AdvancedHeaderZoom
                        Local cObjManyHeaderName
                        cObjManyHeaderName="oManyHeader_"+This.Pages(i_nZoomPage).autozoom.Name
                        This.Pages(i_nZoomPage).AddObject(cObjManyHeaderName,"ManyHeader")
                        This.oRefHeaderObject=This.Pages(i_nZoomPage).&cObjManyHeaderName
                        This.Pages(i_nZoomPage).autozoom.oRefHeaderObject=This.Pages(i_nZoomPage).&cObjManyHeaderName
                        This.oRefHeaderObject.InitHeader(This.Pages(i_nZoomPage).autozoom.Grd)
                    Endif
                    This.Pages(i_nZoomPage).autozoom.Visible=.T.
                Endif
                This.Parent.nPageResizeContainer=This.ActivePage
                This.Parent.Resize()
                This.Parent.nPageResizeContainer=0
            Else
                If This.bInAutoZoom
                    This.bInAutoZoom=.F.
                    This.nOldWz=Thisform.Width
                    This.nOldHz=Thisform.Height
                    If This.nOldWs<>0
                        Thisform.LockScreen=.T.
                        Thisform.Width=This.nOldWs
                        Thisform.Height=This.nOldHs
                        Thisform.LockScreen=.F.
                    Endif
                    This.bAutoZoomLoad = Iif( Vartype(g_AutoZoomLoad)='L',g_AutoZoomLoad,This.bAutoZoomLoad)
                    If  This.bAutoZoomLoad
                        This.bAutoZoomLoad=.F.
                        * --- copia il cursore di zoom nel cursore del keyset
                        Local i_c,i_a,i,j
                        Dimension i_a[1]
                        Private i_err,i_errsav
                        i_err=.F.
                        i_errsav=On('ERROR')
                        On Error i_err=.T.
                        * ---- crea nuovo keyset
                        Select (This.Parent.cKeySet)
                        Afields(i_a)
                        For i=1 To Alen(i_a,1)
                            For j=7 To 16
                                i_a[i,j]='' && buco della afields(): se il cursore era un "filtro" inserisce i trigger della tabella principale
                            Next
                        Next
                        Use
                        Create Cursor (This.Parent.cKeySet) From Array i_a
                        * --- lo riempie
                        Select (This.Pages(i_nZoomPage).autozoom.cCursor)
                        i_c=This.Parent.cKeySelect
                        Dimension i_a[1]
                        Scatter Fields &i_c To i_a
                        Select (This.Parent.cKeySet)
                        Append From Array i_a
                        This.Parent.SetWorkFromKeySet()
                        If !i_err
                            This.Parent.LoadRecWarn()
                        Endif
                        On Error &i_errsav
                    Endif
                    * ---
                Else
                Endif
                If This.ActivePage=i_nWarningPage
                    This.Pages(This.ActivePage).oWarning.Resize()
                Endif
                This.Parent.Resize()
            Endif
            This.Parent.Refresh()
            * --- colori delle pagine
            *this.pages(this.activepage).forecolor=rgb(255,255,255)
            *this.pages(this.activepage).backcolor=rgb(192,192,192)
            *for i=1 to this.PageCount
            *  if i<>this.ActivePage
            *    this.pages(i).forecolor=rgb(255,255,0)
            *    this.pages(i).backcolor=rgb(128,128,128)
            *  endif
            *next
            *thisform.lockscreen=.f.  && togliere anche sopra!
            * ---
            *this.Pages(this.ActivePage).refresh()
            *this.Pages(this.ActivePage).Controls(1).SetFocus()
            *this.parent.oFirstControl.SetFocus()
            *this.parent.refresh()
            *--- Ho cambiato pagina
            This.nPageActive = This.ActivePage
        Endif
    Proc SetZoomSize()
        If Type('this.Pages(this.ActivePage).Autozoom')='O' And This.Pages(This.ActivePage).autozoom.Class='Zoombox'
            This.Pages(This.ActivePage).autozoom.Resize()
        Else
            If Type('this.Pages(this.ActivePage).oWarning')='O' And This.Pages(This.ActivePage).oWarning.Class='Postitcontainer'
                This.Pages(This.ActivePage).oWarning.Resize()
            Endif
        Endif
Enddefine

Define Class StdPCPageFrame As CPPageFrame
    BorderWidth=0
    ExtFldsPage=-1
    Proc Init()
        If This.PageCount=1
            This.Tabs=.F.
            This.SpecialEffect=2
        Endif
        This.Height=This.Parent.Height
        Return
    Proc SetZoomSize()
        Return
    Proc Click()
        *--- Controllo cambio pagina, evita il rilancio del evento ActivatePage quando si preme ripetutamente sul tab
        If This.nPageActive <> This.ActivePage
            This.Parent.NotifyEvent('ActivatePage '+Ltrim(Str(This.ActivePage,2,0)))
            This.nPageActive = This.ActivePage
        Endif
        Return
    Proc UIEnable(lEnabled)
        * --- con vfp 7 i figli integrati non vengono rinfrescati se non sono visibili
        *     quindi i campi restando disbilitati quando si passa da query a edit
        If lEnabled And Thisform.Visible
            * ---- Gestisco l'errore nel caso in cui premo Esc in un Post-In Integrato
            Local i_olderr
            i_olderr=On('ERROR')
            On Error =.T.
            Thisform.Refresh()
            On Error &i_olderr
        Endif
        Return
Enddefine

Define Class stdcontainer As CPContainer
    * --- proprieta' VFP
    BorderWidth=0
    *backstyle=1
    FontName='Arial'
    FontSize=9
    FontBold=.F.
    FontItalic=.F.
    FontUnderline=.F.
    FontStrikethru=.F.
    * --- Proprieta' CP
    cFile = ''
    Dimension xKey[1]
    cOldIcon=''
    oContained=.Null.
    nMouseDownX=0
    nMouseDownY=0
    CreateChildren=.T.
    stdwidth=-1
    stdheight=-1
    resizeXpos=-1
    resizeYpos=-1
    Add Object pagebmp As Image With Top=0,Left=0,Width=0,Height=0,Visible=.F.

    *--- Zucchetti Aulla Inizio - SetFocus se nessun controllo � attivo
	Add Object __dummy__ As CommandButton With Enabled=.T.,Width=0,Height=0,Left=-1, Top=-1, Style=0,SpecialEffect=1, nCountGotFocus=0

	Procedure __dummy__.GotFocus
		*--- Faccio al massimo 10 tentativi, evito di andare in loop
	    If This.nCountGotFocus<10 &&AND !Isnull(Thisform.oFirstControl) AND Thisform.oFirstControl.Enabled AND This.parent.parent.parent.ActivePage=1
	        KEYBOARD '{TAB}'
	        This.nCountGotFocus = This.nCountGotFocus + 1 
	    Else
		    This.nCountGotFocus = 0
		ENDIF
	Endproc
    *--- Zucchetti Aulla Fine - SetFocus se nessun controllo � attivo    
    
    Proc Init(cPrgName,nPag)
        This.LoadBackgroundImage(cPrgName,nPag)
        Return
    Procedure LoadBackgroundImage(cPrgName,nPag)
        If Type("i_bDisableBackgroundImage")="U" Or !i_bDisableBackgroundImage
            If Type('cPrgName')='C'
                Do Case
                    Case cp_fileexist('bmp\'+cPrgName+Alltrim(Str(nPag))+'.bmp')
                        This.pagebmp.Picture='bmp\'+cPrgName+Alltrim(Str(nPag))+'.bmp'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\'+cPrgName+Alltrim(Str(nPag))+'.gif')
                        This.pagebmp.Picture='bmp\'+cPrgName+Alltrim(Str(nPag))+'.gif'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\'+cPrgName+Alltrim(Str(nPag))+'.jpg')
                        This.pagebmp.Picture='bmp\'+cPrgName+Alltrim(Str(nPag))+'.jpg'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\'+cPrgName+'.bmp')
                        This.pagebmp.Picture='bmp\'+cPrgName+'.bmp'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\'+cPrgName+'.gif')
                        This.pagebmp.Picture='bmp\'+cPrgName+'.gif'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\'+cPrgName+'.jpg')
                        This.pagebmp.Picture='bmp\'+cPrgName+'.jpg'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\default.bmp')
                        This.pagebmp.Picture='bmp\default.bmp'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\default.gif')
                        This.pagebmp.Picture='bmp\default.gif'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\default.jpg')
                        This.pagebmp.Picture='bmp\default.jpg'
                        This.pagebmp.Visible=.T.
                Endcase
            Else
                Do Case
                    Case cp_fileexist('bmp\default.bmp')
                        This.pagebmp.Picture='bmp\default.bmp'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\default.gif')
                        This.pagebmp.Picture='bmp\default.gif'
                        This.pagebmp.Visible=.T.
                    Case cp_fileexist('bmp\default.jpg')
                        This.pagebmp.Picture='bmp\default.jpg'
                        This.pagebmp.Visible=.T.
                Endcase
            Endif
        Endif
        Return
    Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            * --- per ritardare un po il drag e drop
            This.nMouseDownX=nXCoord
            This.nMouseDownY=nYCoord
        Endif
    Proc pagebmp.MouseDown(nButton, nShift, nXCoord, nYCoord)
        This.Parent.MouseDown(nButton, nShift, nXCoord, nYCoord)
    Proc MouseMove(nButton, nShift, nXCoord, nYCoord)
        Local oldarea
        *--- _Screen.LOCKSCREEN inserito per evitare problemi con il resize della cp_navbar
        *        If !_Screen.LockScreen And !Isnull(This.oContained) And This.oContained.bLoaded And nButton=1 And (Abs(nXCoord-This.nMouseDownX)+Abs(nYCoord-This.nMouseDownY))>8
        If !_Screen.LockScreen And !Isnull(This.oContained) And nButton=1 And (Abs(nXCoord-This.nMouseDownX)+Abs(nYCoord-This.nMouseDownY))>8
            If Used(This.oContained.cKeySet)
                This.cFile=This.oContained.cFile
                oldarea=Select()
                Select (This.oContained.cKeySet)
                Scatter To This.xKey
                Select (oldarea)
            Endif
            This.DragIcon=i_cBmpPath+"dragmove.cur"
            This.Drag(1)
        Endif
    Proc pagebmp.MouseMove(nButton, nShift, nXCoord, nYCoord)
        This.Parent.MouseMove(nButton, nShift, nXCoord, nYCoord)
    Proc DragDrop(oSource,nXCoord,nYCoord,nState)
        *--- Zucchetti Aulla Inizio - Nuovi Postit
        *if lower(oSource.name)=='postitedt'
        If Lower(oSource.Name)=='postitedt' Or Lower(oSource.Name)=='textarea'
            *--- Zucchetti Aulla Fine - Nuovi Postit
            This.oContained.AddWarning(oSource.Parent)
        Endif
    Proc pagebmp.DragDrop(oSource,nXCoord,nYCoord,nState)
        This.Parent.DragDrop(oSource,nXCoord,nYCoord,nState)
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        Do Case
            Case nState=0   && Enter
                This.cOldIcon=oSource.DragIcon
                *--- Zucchetti Aulla Inizio - Nuovi Postit
                *if lower(oSource.name)=='postitedt'
                If Lower(oSource.Name)=='postitedt' Or Lower(oSource.Name)=='textarea'
                    *--- Zucchetti Aulla Fine - Nuovi Postit
                    oSource.DragIcon=i_cBmpPath+"cross01.cur"
                Endif
            Case nState=1   && Leave
                oSource.DragIcon=This.cOldIcon
        Endcase
    Proc pagebmp.DragOver(oSource,nXCoord,nYCoord,nState)
        This.Parent.DragOver(oSource,nXCoord,nYCoord,nState)
        Return
    Procedure pagebmp.RightClick
        This.Parent.oContained.NotifyEvent('FormMouseRightClick')
        If Vartype(This.Parent.bNoRightClick)<>'U' And This.Parent.bNoRightClick
            Return
        Endif
        If Vartype(This.Parent.Parent.bNoRightClick)<>'U' And This.Parent.Parent.bNoRightClick
            Return
        Endif
        If Vartype(This.Parent.Parent.Parent.bNoRightClick)<>'U' And This.Parent.Parent.Parent.bNoRightClick
            Return
        Endif
        g_oMenu=Createobject("IM_ContextMenu" ,.Null.,This.Parent,'',2)
        * --- Zucchetti Aulla Inizio - Gestione presonalizzata tasto destro su gestione
        g_oMenu.BeforeCreaMenu()
        * --- Zucchetti Aulla Fine - Gestione presonalizzata tasto destro su gestione
        g_oMenu=.Null.
        Return
        * Procedura che crea il menu contestuale speculare al menu della toolbar
        * Aggiunge, come prime voci, i dati letti da un file di menu
    Procedure RightClick
        This.oContained.NotifyEvent('FormMouseRightClick')
        If Vartype(This.Parent.bNoRightClick)<>'U' And This.Parent.bNoRightClick
            Return
        Endif
        If Vartype(This.Parent.Parent.bNoRightClick)<>'U' And This.Parent.Parent.bNoRightClick
            Return
        Endif
        If Vartype(This.Parent.Parent.Parent.bNoRightClick)<>'U' And This.Parent.Parent.Parent.bNoRightClick
            Return
        Endif
        g_oMenu=Createobject("IM_ContextMenu" ,.Null.,This,'',2)
        * --- Zucchetti Aulla Inizio - Gestione presonalizzata tasto destro su gestione
        g_oMenu.BeforeCreaMenu()
        * --- Zucchetti Aulla Fine - Gestione presonalizzata tasto destro su gestione
        g_oMenu=.Null.
        Return
Enddefine

Define Class StdTrsForm As StdForm
    i_nRowPerPage=5
    *nRelRow=0
    oNewFocus=.Null.
    * ---
    Proc Init()
        This.CreateTrs()
        This.cCursorTrs=Sys(2015) &&'crt'+this.name
        This.oPgFrm.Page1.oPag.oBody.RecordMark = i_bRecordMark
        *--- Se nascondo il recordmark devo ridimensionare obody
        If !i_bRecordMark
            This.oPgFrm.Page1.oPag.oBody.Left =  This.oPgFrm.Page1.oPag.oBody.Left + 10
            This.oPgFrm.Page1.oPag.oBody.Width =  This.oPgFrm.Page1.oPag.oBody.Width - 10
            This.oPgFrm.Page1.oPag.obody3D.Left =  This.oPgFrm.Page1.oPag.obody3D.Left + 10
            This.oPgFrm.Page1.oPag.obody3D.Width =  This.oPgFrm.Page1.oPag.obody3D.Width - 10
        Endif
        StdForm::Init()
        *this.cSaveCursorTrs=this.cCursorTrs
        Return
    Proc Destroy()
        *--- Salvo le informazioni di dove era aperta la form
        *--- se all'interno dello schermo e se non minimizzata
        If Type("oMemoryFormPosition")="O" And i_cConfSavePosForm=' ' And This.WindowState<>1 And !This.MDIForm And This.Left<_Screen.Width And This.Left+This.Width>0 And This.Top<_Screen.Height And This.Top+This.Height>0 And Not This.AutoCenter
            oMemoryFormPosition.InsertPosition(This)
        Endif
        This.DeleteTrs()
        This.oPgFrm.Page1.oPag.oBody.oFirstControl=.Null.
        StdForm::Destroy()
        Return
    Proc CreateTrs()
        Return
    Proc DeleteTrs()
        If Used(This.cTrsName)
            Select (This.cTrsName)
            Use
        Endif
        If Used(This.cTrsName+'_bis')
            Select (This.cTrsName+'_bis')
            Use
        Endif
        If Used(This.cCursorTrs)
            Select (This.cCursorTrs)
            Use
        Endif
        *if used(this.cSaveCursorTrs)
        *  select (this.cSaveCursorTrs)
        *  use
        *endif
        Return
    Proc mRestoreTrsDetail(i_bCanSkip)
        Return
    Proc mUpdateTrsDetail(i_bCanSkip)
        Return
        * --- Variazione
    Proc ecpEdit()
        If This.bLoaded
            StdForm::ecpEdit()
            *select (this.cCursorTrs)
            *go top
            *this.WorkFromTrs()
        Endif
        Return
    Proc ecpF6()
        If Inlist(This.cFunction,'Edit','Load') And Type('this.ActiveControl.parent.oContained')='O'
            This.bDontReportError=.T.
            This.ActiveControl.Parent.oContained.F6()
        Endif
        Return
    Proc F6()
        Local i_r,i_del
        If This.cFunction='Edit' Or This.cFunction='Load'
            If This.ActiveControl.Name='OBODY' And This.FullRow()
                If !This.CanDeleteRow()
                    Return
                Endif
                *--- Activity log - tracciatura F6
                If  i_nACTIVATEPROFILER>0
                    cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UF6", "", This, 0, 0, "", This)
                Endif
                *--- Activity log - tracciatura F6
                This.NotifyEvent('Before row delete')
                This.__dummy__.Enabled=.T.
                This.__dummy__.SetFocus()
                This.SubtractTotals()
                Select (This.cTrsName)
                Delete
                i_r=Recno()
                i_del=Set('DELETED')
                Set Deleted Off
                Go Top
                Do While Deleted() And !Eof()
                    Skip
                Enddo
                This.nFirstRow=Recno()
                Go Bottom
                Do While Deleted() And !Bof()
                    Skip -1
                Enddo
                This.nLastRow=Recno()
                Set Deleted &i_del
                Go i_r
                This.oPgFrm.Page1.oPag.oBody.nAbsRow=0
                This.oPgFrm.Page1.oPag.oBody.Refresh()
                This.bUpdated=.T.
                This.__dummy__.Enabled=.F.
                If This.nFirstRow>This.nLastRow
                    This.InitRow()
                Endif
                This.oPgFrm.Page1.oPag.oBody.SetFullFocus()
                This.mCalc(.T.)
                This.NotifyEvent('Row deleted')
            Endif
        Endif
        Return
    Function CanDeleteRow()
        Return(.T.)
    Proc SubtractTotals()
        Return
    Proc ChildrenNewDocument()
        Return
        * --- Pagina Su
    Proc ecpPgUp()
        If This.oPgFrm.ActivePage=1
            This.ScrollBody(-1,This.cFunction='Query')
        Else
            StdForm::ecpPgUp()
        Endif
        Return
        * --- Pagina Giu'
    Proc ecpPgDn()
        If This.oPgFrm.ActivePage=1
            This.ScrollBody(1,This.cFunction='Query')
        Else
            StdForm::ecpPgDn()
        Endif
        Return
    Proc ScrollBody(nDirection,bIsQuery)
        Local i_n,i_errsav
        Private i_err
        If bIsQuery
            If Type('This.ActiveControl')<>'O' Or This.ActiveControl.Name<>'OBODY'
                This.oPgFrm.Page1.oPag.oBody.SetFocus()
            Else
                Select (This.cTrsName)
                Do Case
                    Case Recno()>=This.nLastRow And nDirection=1
                        If Type('this.oFirstControl')='O'
                            This.SetFocusOnFirstControl()
                        Endif
                        StdForm::ecpPgDn()
                    Case Recno()=1 And nDirection=-1
                        If Type('this.oFirstControl')='O'
                            This.SetFocusOnFirstControl()
                        Endif
                    Otherwise
                        Skip nDirection*This.i_nRowPerPage
                        If Recno()>=This.nLastRow Or Eof()
                            This.oPgFrm.Page1.oPag.oBody.Refresh()
                            cp_msg(cp_Translate(MSG_END_OF_THE_DOCUMENT),.F.)
                        Endif
                Endcase
            Endif
        Else
            i_errsav=On('ERROR')
            i_n=2
            i_err=.F.
            On Error i_err=.T.
            If Type('This.ActiveControl')='O' And This.ActiveControl.Name='OBODY'
                i_n=2
            Else
                i_n=This.ActiveControl.nPag
                If i_err
                    i_n=This.ActiveControl.Parent.nPag
                Endif
            Endif
            On Error &i_errsav
            Do Case
                Case i_n=1
                    If nDirection=1
                        This.oPgFrm.Page1.oPag.oBody.SetFullFocus()
                    Endif
                Case i_n=2
                    Do Case
                        Case (Recno()=1 Or Bof()) And nDirection=-1
                            This.oPgFrm.Page1.oPag.oEndHeader.MoveFocus()
                        Case (Recno()>=This.nLastRow Or Eof()) And nDirection=1
                            This.oPgFrm.Page1.oPag.oBeginFooter.MoveFocus()
                        Otherwise
                            Skip nDirection*This.i_nRowPerPage
                            If Type('This.ActiveControl')<>'O' Or This.ActiveControl.Name<>'OBODY'
                                This.oPgFrm.Page1.oPag.oBody.SetFullFocus()
                            Endif
                    Endcase
                Case i_n=3
                    If nDirection=1
                        StdForm::ecpPgDn()
                    Else
                        This.oPgFrm.Page1.oPag.oBody.SetFullFocus()
                    Endif
            Endcase
        Endif
        Return
    Procedure AddVline(x)
        If i_ThemesManager.GetProp(120)=-1
            Local N
            N='_vl_'+Alltrim(Str(x))
            This.oPgFrm.Page1.oPag.AddObject(N,'shape') &&Line(150,00,150,00)
            This.oPgFrm.Page1.oPag.&N..Visible=.T.
            This.oPgFrm.Page1.oPag.&N..Top=This.oPgFrm.Page1.oPag.obody3D.Top+1
            This.oPgFrm.Page1.oPag.&N..Left=x
            This.oPgFrm.Page1.oPag.&N..Width=1
            This.oPgFrm.Page1.oPag.&N..Height=This.oPgFrm.Page1.oPag.obody3D.Height-2
            This.oPgFrm.Page1.oPag.&N..ZOrder(0)
            This.oPgFrm.Page1.oPag.&N..BorderColor = i_ThemesManager.GetProp(TOOLBAR_BORDERCOLOR)
            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.&N..Top = This.oPgFrm.Page1.oPag.&N..Top - This.oPgFrm.Page1.oPag.oHeaderDetail.Height - 1
                This.oPgFrm.Page1.oPag.&N..Height = This.oPgFrm.Page1.oPag.&N..Height + This.oPgFrm.Page1.oPag.oHeaderDetail.Height + 1
            Endif
        Endif
    Endproc
    * Metodo per posizionare i temporanei in modo corretto
    * Utilizza propriet�:
    * bCurPos: definita sulla classe base identifica il numero record da "marcare" sul quale riposionarsi
    * bAbsRow: definita sulla classe base identifica la variabile di stato corrispondente sul body
    * bRelRow: definita sulla classe base identifica la variabile di stato corrispondente sul body
    * Memorizza le variabili di stato per rispristinare in modo corretto il transitorio
    * Se passato a .t. il parametro non svolge la TrsFormWork (nel caso di utilizzo all'interno di una CheckRow)
    Proc MarkPos(NoTrsFromWork)
        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
            This.nMarkPos=0
        Endif

        If This.nMarkPos=0
            If Used(This.cTrsName)
                If Not NoTrsFromWork
                    This.TrsFromWork()
                Endif
                This.bCurPos=Iif( Eof(This.cTrsName) , Recno(This.cTrsName)-1 ,  Recno(This.cTrsName) )
                This.bAbsRow=This.oPgFrm.Page1.oPag.oBody.nAbsRow
                This.bRelRow=This.oPgFrm.Page1.oPag.oBody.nRelRow
            Else
                This.bCurPos=0
                This.bAbsRow=0
                This.bRelRow=0
            Endif
        Endif
        This.nMarkPos=This.nMarkPos+1
    Endproc

    * Per rispristinare la posizione sul transitorio (previa chiamata al metodo MarkPos)
    * Se passato NoSavedependsOn a .t. non esegue la SaveDependsON
    * Nel caso di un batch lanciato da una Changed questa non va svolta
    Proc RePos(NoSaveDependsOn)
        Local L_OldArea,i_r,i_del,L_NumRec
        If Used(This.cTrsName)
            * memorizzo l'area di lavoro per riposizionarmici
            L_OldArea=Select()

            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
            Endif

            Select (This.cTrsName)
            Count For Not Deleted() To L_NumRec
            If L_NumRec=0
                Go Top
                * --- Se la Top mi porta oltre l'ultimo record se tutti cancellati la Count va oltre..
                If Recno(This.cTrsName)>Reccount(This.cTrsName)
                    Go 1
                Endif
            Else
                Go This.bCurPos
            Endif
            With This
                .WorkFromTrs()
                If Not NoSaveDependsOn
                    .SaveDependsOn()
                Endif
                .SetControlsValue()
                .ChildrenChangeRow()
                .mHideControls()
                .oPgFrm.Page1.oPag.oBody.Refresh()
                .oPgFrm.Page1.oPag.oBody.nAbsRow= This.bAbsRow
                .oPgFrm.Page1.oPag.oBody.nRelRow= This.bRelRow
                * --- Inserito pezzo di codice prelevato dalla F6 per gestire
                * --- i record cancellati con DELETE dal temporaneo.
                i_r=Recno()
                i_del=Set('DELETED')
                Set Deleted Off
                Go Top
                Do While Deleted() And !Eof()
                    Skip
                Enddo
                .nFirstRow=Recno()
                Set Deleted &i_del
                Go i_r
            Endwith
            * Mi riposiziono sull'area di lavoro precedente
            Select( L_OldArea )
        Endif
        This.nMarkPos=This.nMarkPos-1
    Endproc

    * Per aggiungere una riga al temporaneo cTrsName
    * Si riposiziona anche sull'area di lavoro in cui si era in precedenza
    * se per esempio si richiama all'interno di una Scan su un Cursore
    Proc AddRow()
        Local L_OldArea, bNewRow
        If Used(This.cTrsName)
            * memorizzo l'area di lavoro per riposizionarmici
            L_OldArea=Select()

            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
            Endif

            Select (This.cTrsName)
            Go Bottom
            * Per Controllare se riga � piena devo prima verificare
            * se la riga esiste. Questa condizione � determinata
            * dal fatto che sia EOF che BOF sono .t. (funziona perch�
            * abbiamo svolto una go Bottom)
            If Eof( This.cTrsName ) And Bof( This.cTrsName )
                * Cursore vuoto inserisco sempre
                bNewRow=.T.
            Else
                * Controllo condizione di riga piena
                bNewRow=This.FullRow()
            Endif
            * Controllo condizione di riga piena
            If bNewRow
                * Se l'ultima riga del transitorio � piena ne aggiungo una nuova....
                This.InitRow()
            Else
                * ...altrimenti aggiorno le variabili di lavoro dai campi del cursore (w_...= t_...)
                This.WorkFromTrs()
            Endif
            * Mi riposiziono sull'area di lavoro precedente
            Select( L_OldArea )
        Endif
    Endproc
    * Riceve come parametro l'espressione di ricerca e ritorna il primo numero record che la
    * soddisfa. Il secondo parametro, se impostato, continua la ricerca a partire
    * dal numero record specificato.
    * L'implementazione considera il numero record una visione alta di RecNo()
    * ATTENZIONE per scelta implementativa considera anche le righe cancellate
    * x poter dare pi� libert� di utilizzo
    Function Search(Criterio,StartFrom)
        Local cOldArea,cName_1,cName_2, cCursName,nResult,cCond
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        cName_1=This.cTrsName
        cName_2=This.cTrsName+'_bis'
        cCursName=Sys(2015)
        cCond=' 1=1 '
        * Mi chiedo se attiva o meno la presenza del doppio transitorio

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        If Used(cName_2)
            * Se ricerca a partire da un certo record aggiungo la condizione..
            If Type('StartFrom')='N' And StartFrom<>0
                cCond=' Curs1.I_RECNO>'+Alltrim(Str(StartFrom))
            Endif
            Select Min(Curs1.I_RECNO) As Riga From (cName_1) Curs1 ,(cName_2) Curs2 Where ( Curs1.I_RECNO= Curs2.I_RECNO And &cCond) And;
                &Criterio Into Cursor &cCursName
        Else
            * Se ricerca a partire da un certo record aggiungo la condizione..
            If Type('StartFrom')='N' And StartFrom<>0
                cCond=' I_RECNO>'+Alltrim(Str(StartFrom))
            Endif
            Select Min(I_RECNO) As Riga From (cName_1) Where  &cCond And ;
                &Criterio Into Cursor &cCursName
        Endif
        * leggo il risultato
        Select(cCursName)
        Go Top
        nResult=Nvl(&cCursName..Riga,-1)
        nResult=Iif(nResult=0,-1, nResult)
        * rimuovo il cursore di appoggio
        If Used(cCursName)
            Select (cCursName)
            Use
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
        * restituisco il risultato
        Return nResult

    Endfunc


    * Crea un temporaneo Fox basato esclusivamente sui dati presenti nei transitori...
    * Di fatto costrusce una Query di selezione sul temporaneo rendendo trasparente l'implementazione
    * dell'eventuale doppio transitorio..
    * A T T E N Z I O N E  la routine lascia volutamente l'area di lavoro sul temporaneo appena creato
    * al primo record..
    * parametri
    * cTmp, nome del temporaneo Fox da restituire
    * cFields, elenco dei campi
    * cWhere, condizioni di where da applicare
    * cOrder, ordinamenti
    * cGroup, eventuali raggruppamenti
    * cHaving, eventuali filtri su raggruppamenti
    * bDeleted se non valorizzata esclude i record cancellati (default), se a .t. li include
    *            (il test Deleted non si pu� utilizzare all'interno di una select
    *             VFP prende quello del record corrente)
    Procedure Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving,bDeleted)
        Local cName_1,cName_2,cCmdSql,cOlderr,cMsgErr
        * Assegnamenti..
        cOlderr=On('error')
        cMsgErr=''
        On Error cMsgErr=Message(1)+': '+Message()

        cTmp=   Iif( Empty(cTmp)   , '__Tmp__', cTmp )
        cFields=Iif( Empty(cFields), '*'      , cFields )
        cWhere= Iif( Empty(cWhere) , '1=1'    , cWhere )
        cOrder= Iif( Empty(cOrder) , '1'      , cOrder )
        cOrder=Iif( Empty(cGroupBy),'',' GROUP BY '+(cGroupBy)+Iif( Empty(cHaving),'',' HAVING '+(cHaving) ) )+' Order By '+cOrder

        cName_1=This.cTrsName
        cName_2=This.cTrsName+'_bis'

        cCmdSql='Select '+cFields+' From '+cName_1

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        * Mi chiedo se attivo il doppio transitorio...
        If Used(This.cTrsName+'_bis')
            cCmdSql=cCmdSql+' Curs1,'+cName_2+' Curs2 Where Curs1.I_RECNO=Curs2.I_RECNO And '
        Else
            cCmdSql=cCmdSql+' Curs1 Where '
        Endif

        * Gestione cancellati
        Local cOldDeleted
        cOldDeleted=Set("Deleted")
        If bDeleted
            * Considero anche i cancellati
            Set Deleted Off
        Else
            * di default escludo i cancellati
            Set Deleted On
        Endif

        cCmdSql=cCmdSql+cWhere+' &cOrder Into Cursor '+cTmp+' Nofilter '
        * Eseguo la frase..
        Select (This.cTrsName)
        &cCmdSql

        * Ripristino la vecchia gestione errori
        On Error &cOlderr

        * Ripristino la gestione dei cancellati..
        Set Deleted &cOldDeleted

        * Se Qualcosa va storto lo segnalo ed esco...
        If Not Empty(cMsgErr)
            cp_ErrorMsg(cMsgErr,'stop')
            * memorizzo nella clipboard la frase generata in caso di errore
            _Cliptext=cCmdSql
        Else
            * mi posiziono sul temporaneo al primo record..
            Select (cTmp)
            Go Top
        Endif

    Endproc

    ************************************************************
    *Cancellazione righe su transitorio che verificano la condizione passata nel parametro cWhere
    Procedure Exec_Delete(cWhere,bNoInitrow)
        Local m.ctmpCursor, cName_1,cName_2,cCmdSql,cOlderr,cMsgErr, L_OldArea,bDeleted
        L_OldArea = Select()
        * Assegnamenti..
        cOlderr=On('error')
        cMsgErr=''
        On Error cMsgErr=Message(1)+': '+Message()
        cWhere= Evl(m.cWhere , '1=1')
        bNoInitrow=Evl(bNoInitrow , .F.)
        ctmpCursor='__Select__'
        cName_1=This.cTrsName
        cName_2=This.cTrsName+'_bis'
        This.Exec_Select(m.ctmpCursor,'Curs1.I_RECNO',m.cWhere)

        If Recno(ctmpCursor)>0 And Used(ctmpCursor)
            *all'interno del cursore _Select_ sono contenute le righe del transitorio da cancellare
            If Used(m.cName_1)
                cCmdSql='Delete ' +m.cName_1 +' from '+m.cName_1+' where '+ m.cName_1+'.I_RECNO  in ( Select I_RECNO from '+ m.ctmpCursor+' )'
                Select (m.cName_1)
                &cCmdSql
            Endif
            * Mi chiedo se attivo il doppio transitorio, se lo � cancello...
            If Used(m.cName_2)
                cCmdSql='Delete ' +m.cName_2 +' from '+m.cName_2+' where ' + m.cName_2+'.I_RECNO in ( Select I_RECNO from '+ m.ctmpCursor+' )'
                Select (m.cName_2)
                &cCmdSql
            Endif
            * Ripristino la vecchia gestione errori
            On Error &cOlderr
            * Se Qualcosa va storto lo segnalo ed esco...
            If Not Empty(m.cMsgErr)
                cp_ErrorMsg(m.cMsgErr,'stop')
                * memorizzo nella clipboard la frase generata in caso di errore
                _Cliptext=m.cCmdSql
            Else
                bDeleted=.T.
                This.bUpdated=.T.
            Endif
            * Se il transitorio, a seguito della cancellazione
            * rimane vuoto allora si esegue una AddRow
            If bDeleted And Not bNoInitrow And This.NumRow()=0
                This.AddRow()
            Endif
            * ripristino la vecchia area
            Select(m.L_OldArea)
            *chiudo il cursore
            Use In Select (m.ctmpCursor)
        Endif
    Endproc

    ******************************************************************************
    * Pemette la modifica massiva di 3 valori del transitorio rendendo trasparente la gestione del doppio transitorio
    * viene sfruttata la exec_select per la determinazione delle righe da modificare e per la valorizzazione del campo e del valore oggetto dell'update
    *cWhere contiene la condizione di where da applicare per la determinazione delle righe da modificare
    *cField1 contiene l'identificativo del primo,campo del transitorio da modificare
    *cValue1 contiene il valore da attribuire al campo cField1
    *cField2 contiene l'identificativo del secondo campo del transitorio da modificare
    *cValue2 contiene il valore da attribuire al campo cField2
    *cField3 contiene l'identificativo del terzo campo del transitorio da modificare
    *cValue3 contiene il valore da attribuire al campo cField3
    * bNoUpd non necessario, se passato a .t. non valorizza i_srv sul transitorio
    Procedure Exec_Update(cWhere,cField1,cValue1,cField2,cValue2,cField3,cValue3,bNoUpd)

        Local cOlderr,L_OldArea,cName_1,cName_2,cCmdSql,cUpdate,cSet,cSet1,cSet2,cSet3,cSet4

        L_OldArea = Select()

        * Assegnamenti..
        cOlderr=On('error')
        cMsgErr=''
        On Error cMsgErr=Message(1)+': '+Message()

        *passaggio parametri
        cWhere= Evl(m.cWhere , '1=1')
        cField1 = Evl(m.cField1 , '')
        cValue1 = Evl(m.cValue1 , "''")
        cField2 = Evl(m.cField2 , '')
        cValue2 = Evl(m.cValue2 , "''")
        cField3 = Evl(m.cField3 , '')
        cValue3 = Evl(m.cValue3 , "''")
        bNoUpd=Evl(bNoUpd , .F.)
        ctmpCursor='__Select__'

        cName_1=This.cTrsName
        cName_2=This.cTrsName+'_bis'
        *condizione

        cUpdate="iif ("+cName_1+".i_Srv<>'A' And "+cName_1+".i_Srv<>'U' And Not Deleted(),'U',"+cName_1+".i_Srv)"

        *vengono selezionate le righe che devono essere modificate con i valori per l'update
        This.Exec_Select(m.ctmpCursor,'Curs1.I_RECNO, '+m.cValue1+' as  r1,'+m.cValue2+' as r2,'+m.cValue3+' as r3', m.cWhere,'','')

        If Used(m.ctmpCursor) And Reccount(m.ctmpCursor)>0

            *Costruzione comando update per il primo transitorio
            cSet=''
            cSet1=Iif(Type(m.cName_1+'.'+m.cField1)<>'U',m.cName_1+'.'+m.cField1+'= '+m.ctmpCursor+'.r1 ','')
            cSet2=Iif(Not Empty(m.cField2) And Type(m.cName_1+'.'+m.cField2)<>'U',m.cName_1+'.'+m.cField2+'= '+m.ctmpCursor+'.r2 ','')
            cSet3=Iif(Not Empty(m.cField3) And Type(m.cName_1+'.'+m.cField3)<>'U',m.cName_1+'.'+m.cField3+'= '+m.ctmpCursor+'.r3 ','')
            cSet4=Iif(m.bNoUpd,'',m.cName_1+'.i_Srv='+m.cUpdate)

            cSet=m.cSet1
            If !Empty(m.cSet2)
                cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet2
            Endif

            If !Empty(m.cSet3)
                cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet3
            Endif

            If !Empty(m.cSet4)
                cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet4
            Endif

            If !Empty(cSet)
                cCmdSql='Update '+m.cName_1 +' set '+m.cSet+' from '+m.cName_1+','+m.ctmpCursor+' where '+m.cName_1+'.I_RECNO in ( Select I_RECNO from '+ m.ctmpCursor+' ) and '+m.cName_1+'.I_RECNO=' +m.ctmpCursor+'.I_RECNO'
                Select (m.cName_1)
                &cCmdSql
            Endif

            If Used(m.cName_2) And Not Empty(m.cMsgErr)
                *Costruzione comando update per il transitorio_bis

                cSet=''
                cSet1=''
                cSet2=''
                cSet3=''
                cSet1=Iif(Type(m.cName_2+'.'+m.cField1)<>'U',m.cName_2+'.'+m.cField1+'= '+m.ctmpCursor+'.r1 ','')
                cSet2=Iif(Not Empty(m.cField2) And Type(m.cName_2+'.'+m.cField2)<>'U',m.cName_2+'.'+m.cField2+'= '+m.ctmpCursor+'.r2 ','')
                cSet3=Iif(Not Empty(m.cField3) And Type(m.cName_2+'.'+m.cField3)<>'U',m.cName_2+'.'+m.cField3+'= '+m.ctmpCursor+'.r3 ','')


                cSet=m.cSet1

                If !Empty(m.cSet2)
                    cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet2
                Endif

                If !Empty(m.cSet3)
                    cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet3
                Endif
                If !Empty(cSet)
                    cCmdSql='Update '+m.cName_2 +' set '+m.cSet+' from '+m.cName_2+','+m.ctmpCursor+' where '+m.cName_2+'.I_RECNO in ( Select I_RECNO from '+ m.ctmpCursor+' ) and '+m.cName_2+'.I_RECNO=' +ctmpCursor+'.I_RECNO'
                    Select (m.cName_2)
                    &CmdSql
                Endif

            Endif

            * Se Qualcosa va storto lo segnalo ed esco...
            If Not Empty(m.cMsgErr)
                cp_ErrorMsg(m.cMsgErr,'stop')
                * memorizzo nella clipboard la frase generata in caso di errore
                _Cliptext=m.cCmdSql
            Else
                This.bUpdated=.T.
            Endif

            *chiudo il cursore
            Use In Select (m.ctmpCursor)
            * Ripristino la vecchia gestione errori
            On Error &cOlderr
            Select (L_OldArea)
        Endif
    Endproc

    * Si posiziona all'ennesima riga del transitorio (id_row coincide con la Recno)
    * ATTENZIONE il metodo � stato costruito per utilizzo all'interno di batch non
    * utilizzare per riposizionarsi, utilizzare per questo scopo la Repos abbinata alla MarkPos
    * Se parametro non passato allora aggiorna la riga corrente...
    * Aggiunto un nuovo parametro per rendere opzionabile la WorkFromTrs
    Procedure SetRow(id_row, bnoWorkFromTrs)
        Local cOldArea
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Se paramento valorizzato mi posiziono sulla riga passata
        * altrimenti sulla riga attuale...
        If Type( 'id_row' )='N'
            Select( This.cTrsName )
            Goto (id_row)
            * gestisco anche il transitorio doppio
            If Used(This.cTrsName+'_bis')
                Goto (id_row)
            Endif
        Endif
        If Not bnoWorkFromTrs
            This.WorkFromTrs()
            This.ChildrenChangeRow()
        Endif

        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Si posiziona sulla prima riga non cancellata
    * Prima Routine da invocare per iniziare una scan del transitorio
    * A questa routine, di norma, si deve far seguire una SetRow per valorizzare
    * le variabili di Work..
    Procedure FirstRow()
        This._EndsRow(.F.,.F.,.F.)
    Endproc

    * Si posiziona sulla prima riga cancellata
    * Prima Routine da invocare per iniziare una scan del transitorio
    * A questa routine, di norma, si deve far seguire una SetRow per valorizzare
    * le variabili di Work..
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure FirstRowDel(bAdded)
        This._EndsRow(.T.,.F.,bAdded)
    Endproc

    * Si posiziona sulla prima riga non cancellata
    * Prima Routine da invocare per iniziare una scan del transitorio
    * A questa routine, di norma, si deve far seguire una SetRow per valorizzare
    * le variabili di Work..
    Procedure LastRow()
        This._EndsRow(.F.,.T.,.F.)
    Endproc

    * Si posiziona sulla prima riga cancellata
    * Prima Routine da invocare per iniziare una scan del transitorio
    * A questa routine, di norma, si deve far seguire una SetRow per valorizzare
    * le variabili di Work..
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure LastRowDel(bAdded)
        This._EndsRow(.T.,.T.,bAdded)
    Endproc


    * Metodo di base utilizzato per accedere al primo o ultimo record
    * bLast a .t. va all'ultimo...
    * Se Del_Row a .t. recupera la prima riga cancellata..
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    * 			ha senso solo se si gestiscono le cancellate
    Procedure _EndsRow(Del_Row,bLast,bAdded)
        Local cOldArea,cOldFilter,cOldDeleted,nTrs_Pos
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Memorizzo il vecchio deleted..
        cOldDeleted=Set('Deleted')
        * Rendo visibili o meno i record cancellati...
        If Del_Row
            Set Deleted Off
        Else
            Set Deleted On
        Endif

        Select( This.cTrsName )

        cOldFilter=Filter(This.cTrsName)

        * Se desidero solo le cancellate imposto il filtro su queste...
        If Del_Row
            If bAdded
                Set Filter To Deleted()
            Else
                Set Filter To Deleted() And i_Srv<>'A'
            Endif
        Else
            Set Filter To Not Deleted()
        Endif

        If bLast
            Goto Bottom
        Else
            Goto Top
        Endif

        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            nTrs_Pos=Recno(This.cTrsName)
        Endif

        * Rispristino la Filter precedente...
        Select (This.cTrsName)
        Set Filter To &cOldFilter
        Set Deleted &cOldDeleted

        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            Select(This.cTrsName+'_bis')
            If Reccount(This.cTrsName+'_bis')>=nTrs_Pos
                Goto nTrs_Pos
            Else
                Goto Bottom
            Endif
        Endif

        * ripristino la vecchia area
        Select(cOldArea)

    Endproc


    * Di norma dopo questi metodi occorre lanciare la SetRow..

    * Passa alla riga succesiva sul transitorio non cancellata
    * Arrivato all'ultima riga va alla successiva per far scattare l'eof...
    Procedure NextRow()
        This._MoveRow(1,.F.,.F.)
    Endproc

    * Passa alla riga succesiva sul transitorio cancellata
    * Arrivato all'ultima riga va alla successiva per far scattare l'eof...
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure NextRowDel(bAdded)
        This._MoveRow(1,.T.,bAdded)
    Endproc

    * Passa alla riga succesiva sul transitorio non cancellata
    * Arrivato all'ultima riga va alla successiva per far scattare l'bof...
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure PriorRow()
        This._MoveRow(-1,.F.,.F.)
    Endproc

    * Passa alla riga succesiva sul transitorio cancellata
    * Arrivato all'ultima riga va alla successiva per far scattare l'bof...
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure PriorRowDel(bAdded)
        This._MoveRow(-1,.T.,bAdded)
    Endproc


    * Metodo di base x implementare lo spostamento di riga a partire dalla riga corrente
    *(ascesa e discesa, cancellati o non cancellati)
    * nStep 1,-1 indica se andare al record successivo o al precedente (ammessi anche passi diversi da 1/-1)
    * Del_Row a .t. per scorrere solo le cancellate...
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    * 			ha senso solo se si gestiscono le cancellate
    Procedure _MoveRow(nStep,Del_Row, bAdded)
        Local cOldArea,cOldFilter,cOldDeleted
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Memorizzo il vecchio deleted..
        cOldDeleted=Set('Deleted')
        * Rendo visibili o meno i record cancellati...
        If Del_Row
            Set Deleted Off
        Else
            Set Deleted On
        Endif

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Select( This.cTrsName )

        cOldFilter=Filter(This.cTrsName)
        * Se desidero solo le cancellate imposto il filtro su queste...
        If Del_Row
            If bAdded
                Set Filter To Deleted()
            Else
                Set Filter To Deleted() And i_Srv<>'A'
            Endif
        Else
            Set Filter To Not Deleted()
        Endif

        * Passo la record adiacente...
        Skip nStep

        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            * Se non sono arrivato alla fine del file mi
            * posiziono al solito record
            Select ( This.cTrsName+'_bis' )
            If (Not Eof(This.cTrsName) And nStep>0) Or (Not Bof(This.cTrsName) And nStep<0)
                Goto Recno(This.cTrsName)
            Else
                * altrimenti vado all'ultimo record e passo al successivo..
                If nStep>0
                    Goto Bottom
                Else
                    Goto Top
                Endif

                Skip nStep

            Endif
        Endif

        * Rispristino la Filter precedente...
        Select (This.cTrsName)
        Set Filter To &cOldFilter
        Set Deleted &cOldDeleted
        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Restituisce true se raggiungo l'eof del transitorio...
    Function Eof_Trs()
        Return (Eof(This.cTrsName))
    Endfunc

    * Restituisce true se raggiungo l'Bof del transitorio...
    Function Bof_Trs()
        Return (Bof(This.cTrsName))
    Endfunc

    * Restituisce il numero di righe piene (non cancellate)
    Function NumRow()
        Local cOldArea,nResult,nPos
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        nPos=Recno( This.cTrsName )
        nResult=0
        This.FirstRow()
        Do While Not This.Eof_Trs()
            * Scorro il transitorio e se la riga � piena la considero
            If This.FullRow()
                nResult = nResult + 1
            Endif
            This.NextRow()
        Enddo
        * Mi rimetto sul record dov'ero..
        Select( This.cTrsName )
        If Reccount(This.cTrsName)>=nPos
            Goto nPos
        Else
            Goto Bottom
        Endif
        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            Select(This.cTrsName+'_bis')
            If Reccount(This.cTrsName+'_bis')>=Recno(This.cTrsName)
                Goto Recno(This.cTrsName)
            Else
                Goto Bottom
            Endif
        Endif

        * ripristino la vecchia area
        Select(cOldArea)
        Return nResult
    Endfunc


    * Restituisce il numero di righe cancellate
    Function NumRow_Del()
        Local cOldArea,nResult,nPos
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        nPos=Recno( This.cTrsName )
        nResult=0
        * Scorro le righe cancellate...
        This.FirstRowDel()
        Do While Not This.Eof_Trs()
            nResult = nResult + 1
            This.NextRowDel()
        Enddo
        * Mi rimetto sul record dov'ero..
        Select( This.cTrsName )
        If Reccount(This.cTrsName)>=nPos
            Goto nPos
        Else
            Goto Bottom
        Endif

        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            Select(This.cTrsName+'_bis')
            If Reccount(This.cTrsName+'_bis')>=Recno(This.cTrsName)
                Goto Recno(This.cTrsName)
            Else
                Goto Bottom
            Endif
        Endif

        * ripristino la vecchia area
        Select(cOldArea)
        Return nResult
    Endfunc

    * Restituisce lo stato della riga
    * 'A' Agginunta
    * Empty - Non modifica
    * 'U' MOdificata
    * 'D' deleted
    Function RowStatus()
        Local cOldArea,cResult
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Select ( This.cTrsName )
        If Deleted()
            cResult='D'
        Else
            cResult=i_Srv
        Endif

        * ripristino la vecchia area
        Select(cOldArea)
        Return cResult
    Endfunc

    * Scrive sui temporanei le variabili di Work..
    Procedure SaveRow(bChangeRow)
        This.TrsFromWork()
        If bChangeRow
            This.ChildrenChangeRow()
        Endif
        * marca la riga come modificata..
        This.SetupdateRow()
    Endproc

    * Valorizza sul transitorio il valore per il determinato campo sulla riga attuale
    * corrisponde ad una Replace sul corretto transitorio (non marca la riga come modificata)
    * bNoUpd non necessario, se passato a .t. non valorizza i_srv sul transitorio
    * BupdPro aggiorna anche la corrispondente variabile w_ (Se presente)
    Procedure Set(cItem,vValue,bNoUpd,bUpdProp)
        Local cOldArea,cFldName,cTest,bResult, bNoSrv, sAssign
        LOCAL TestMacro
        * Se specifico il terzo parametro ed � .t. allora non aggiorno i_SRV..
        bNoSrv=Pcount()>=3 And Type("bNoUpd")='L' And bNoUpd
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        * Se le prima due lettere sono w_ le sostituisco con t_
        * altrimenti lascio quanto trovo...
        If Lower(Left(cItem,2))='w_'
            cFldName='t_'+Substr(cItem,3,Len(cItem))
        Else
            cFldName=cItem
        Endif

        cTest=This.cTrsName+'_bis'+'.'+cFldName
        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis') And Type( cTest )<>'U'
            * ricerco il campo nel transitorio doppio
            * se lo trovo aggiorno il due altrimenti l'uno...
            Select( This.cTrsName+'_Bis' )
        Else
            * Aggiorno il transitorio normale...
            Select( This.cTrsName )
        Endif

        * Se cambio allora svolgo la Replace..
        * controllo valori null- inzio
        TestMacro=(&cFldName<>vValue Or ( Lower(Left(cItem,2))='w_' And bUpdProp And This.&cItem <> vValue )) Or (Isnull(&cFldName)And Not Isnull(&cFldName)) Or (Isnull(&cFldName)And Not Isnull(&cFldName))
        If TestMacro
            Replace &cFldName With vValue
            * Se richiesto e corrisponde ad una propriet� aggiorno anch'essa
            If Lower(Left(cItem,2))='w_' And bUpdProp
                This.&cItem = vValue
            Endif

            * Ho cambiato qualcosa e non � esplicitamente richiesto di non aggiornare i_SRV ?
            * i_Srv sempre sul temporaneo principale...
            If Not bNoSrv
                This.SetupdateRow()
            Endif
        Endif

        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Restituisce il valore combo/radio per la riga attuale
    * per un determinato item.
    Function GetRadioValue(Item, nOccurrence)
        Local cOldArea,cFldName, oCtrl, Result
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Se le prima due lettere sono t_ le sostituisco con w_
        * altrimenti lascio quanto trovo...
        If Lower(Left(m.Item,2))='t_'
            cFldName='w_'+Substr(m.Item,3,Len(m.Item))
        Else
            cFldName=m.Item
        Endif
        nOccurrence=Iif(Type("nOccurrence")="N", m.nOccurrence, 1)
        oCtrl = This.GetBodyCtrl(m.cFldName, m.nOccurrence)
        If !Isnull(m.oCtrl) And Pemstatus(m.oCtrl, "RadioValue", 5)
            Result = m.oCtrl.RadioValue()
        Endif
        oCtrl = .Null.
        * ripristino la vecchia area
        Select(m.cOldArea)

        * Restituisco il valore recuperato..
        Return(m.Result)
    Endfunc

    * Restituisce il valore sul transitorio per la riga attuale
    * per un determinato item..
    Function Get(Item)
        Local cOldArea,cFldName,cTest
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        * Se le prima due lettere sono w_ le sostituisco con t_
        * altrimenti lascio quanto trovo...
        If Lower(Left(Item,2))='w_'
            cFldName='t_'+Substr(Item,3,Len(Item))
        Else
            cFldName=Item
        Endif

        cTest=This.cTrsName+'_bis'+'.'+cFldName
        * gestisco anche il transitorio doppio
        * A seconda di dove � il campo mi seleziono sul temporaneo corretto...
        If Used(This.cTrsName+'_bis') And Type(cTest)<>'U'
            Select( This.cTrsName+'_Bis' )
        Else
            Select( This.cTrsName )
        Endif
        * Leggo il campo...
        Result=Eval(cFldName)

        * ripristino la vecchia area
        Select(cOldArea)

        * Restituisco il valore recuperato..
        Return(Result)
    Endfunc

    * Ritorna il tipo di un campo sul transitorio
    * Riceve una stringa contenente il nome del campo (Es. w_MVCODICE)
    * restituisce stesso risultato di una Type Vfp
    Function GetType(cFieldsName)
        Local cOldArea,cFldName,cTest
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Se le prima due lettere sono w_ le sostituisco con t_
        * altrimenti lascio quanto trovo...
        If Lower(Left(cFieldsName,2))='w_'
            cFldName='t_'+Substr(cFieldsName,3,Len(cFieldsName))
        Else
            cFldName=cFieldsName
        Endif

        cTest=This.cTrsName+'_bis'+'.'+cFldName
        * gestisco anche il transitorio doppio
        * A seconda di dove � il campo mi seleziono sul temporaneo corretto...
        If Used(This.cTrsName+'_bis') And Type( cTest )<>'U'
            Select( This.cTrsName+'_Bis' )
        Else
            Select( This.cTrsName )
        Endif
        * Leggo il campo...
        Result=Type(cFldName)

        * ripristino la vecchia area
        Select(cOldArea)
        Return( Result )
    Endfunc

    * Marca la riga attuale come modificata
    Procedure SetupdateRow()
        Local cOldArea
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Select( This.cTrsName )

        If i_Srv<>'A' And i_Srv<>'U' And Not Deleted()
            Replace i_Srv With 'U'
        Endif
        This.bUpdated=.T.

        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Marca la riga attuale come cancellata
    * Se parametro a .t. non svolge la Initrow
    * a seguito della cancellazione dell'ultimo record
    Procedure DeleteRow(bNoInitrow)
        Local cOldArea, bDeleted
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Select( This.cTrsName )
        bDeleted=.F.
        * Se non � gia cancellata...
        If Not Deleted()
            Delete
            bDeleted=.T.
            This.bUpdated=.T.
        Endif

        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            Select(This.cTrsName+'_bis')
            Delete
        Endif

        * Se il transitorio, a seguito della cancellazione
        * rimane vuoto allora si esegue una AddRow
        If bDeleted And Not bNoInitrow And This.NumRow()=0
            This.AddRow()
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Restituisce l'indice del record corrente
    * sul transitorio
    Function RowIndex()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Return ( Recno( This.cTrsName ) )
    Endfunc

    *--- Se utilizzo SetVar devo segnalare che la riga � aggiornata
    Proc SetVar(cVarName,xValue)
        DoDefault(m.cVarName,m.xValue)
        If This.GetType(m.cVarName)<>'U'
            This.SetupdateRow()
        Endif
    Endproc

Enddefine

Define Class stdPCForm As StdForm
    Closable=.F.
    bSaveContext = .T.
    Proc Init()
        Local basename,i,fo
        This.Name=This.Name+Sys(2015)
        basename=This.Name                        && alternativamente si puo' usare 'sys(2015)'
        Public frm&basename
        frm&basename = This
        * --- definisce il nome per il suo cursore
        *this.cCursor='cur'+basename
        *this.cKeySet='Key'+basename
        *--- Zucchetti Aulla FormDecorator Inizio
        If This.bApplyFormDecorator And i_ThemesManager.GetProp(123)=0
            This.AddObject("oDec", "cp_FormDecorator")
        Else
            This.bApplyFormDecorator = .F.
        Endif
        *--- Zucchetti Aulla FormDecorator Fine
        *-- Zucchetti Aulla Inizio: Gestione ancoraggio oggetti
        Cp_SetAnchorForm(This.Cnt)
        *-- Zucchetti Aulla Fine: Gestione ancoraggio oggetti
        *  --- la posizione ?
        If i_cConfSavePosForm<>'D'
            This.SetPosition()
        Endif
        * ---
        For i=1 To _Screen.FormCount
            fo=_Screen.Forms(m.i)
            If fo.Class=This.Class And fo.Name<>This.Name And This.Top=fo.Top And This.Left=fo.Left
                This.Top=This.Top+20
                This.Left=This.Left+20
            Endif
        Next
        * ---
        *this.OpenWorkTables()
        *this.CreateChildren()
        This.Cnt.NotifyEvent("FormLoad")
        This.cFunction='Query'
        This.Icon=This.Cnt.Icon
        This.cComment=This.Cnt.cComment
        *--- Aggiunge la form al WindowsManager
        If Vartype(i_oDeskmenu)='O'
            i_oDeskmenu.olistWnd.AddFormItem(This.HWnd)
        Endif
        Return
    Proc Destroy()
        *--- Salvo le informazioni di dove era aperta la form
        *--- se all'interno dello schermo e se non minimizzata
        If Type("oMemoryFormPosition")="O" And i_cConfSavePosForm=' ' And This.bChkPositionForm And This.WindowState<> 1 And !This.MDIForm And This.Left<_Screen.Width And This.Left+This.Width>0 And This.Top<_Screen.Height And This.Top+This.Height>0 And Not This.AutoCenter
            oMemoryFormPosition.InsertPosition(This)
        Endif
        *wait window "Destroy del form PC"
        This.RemoveObject('cnt')
        basename = This.Name
        Release frm&basename
        Return
    Proc Resize()
        This.Cnt.oPgFrm.Width=This.Width
        This.Cnt.Width=This.Width
        *--- Zucchetti FormDecorator
        This.Cnt.oPgFrm.Height=Max(25,This.Height - Iif(Pemstatus(This, "oDec",5), This.oDec.Height,0))
        This.Cnt.Height=Max(25,This.Height - Iif(Pemstatus(This, "oCaptionBar",5) And This.oCaptionBar.Visible, This.oCaptionBar.Height,0))
        *--- Zucchetti FormDecorator
        cp_ResizeContainer(This.Cnt.oPgFrm)
        Return
    Proc SelectCursor()
        Return
    Proc QueryKeySet()
        Return
    Procedure SetKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        This.Cnt.SetKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        Return
    Proc LinkPCClick(bFromBatch) && Gestione figli da routine se passato il parametro a .t.
        If This.Cnt.bOnScreen
            This.HideChildrenChain()
        Else
            *** Zucchetti Aulla - Modulo controllo flussi
            This.cFlowStamp = Space(10)			  && sbiancato per inizializzarlo con un nuovo valore all'F10 e F5
            *** Zucchetti Aulla Fine - Modulo controllo flussi
            cp_GetSecurity(This,This.getSecurityCode())
            If !(This.bSec1)
                cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
                Return
            Endif
            This.ShowChildrenChain(bFromBatch) && Gestione figli da routine
            This.Cnt.SetStatus(This.cFunction)  &&problema valorizzazione cfunction dei nipoti
            This.Cnt.FillRec()
            This.Cnt.SetEnabled(This.cFunction)
            If Inlist(This.cFunction,'Edit','Load')
                This.Cnt.mEnableControls()
            Endif
            This.Cnt.Refresh()
        Endif
        Return
    Function getSecurityCode()
        Return This.Cnt.cPrg
    Proc SetStatus(cFunction)
        This.cFunction=cFunction
        This.Cnt.SetStatus(cFunction)
        This.SetCaption()
        Return
    Func IsAChildUpdated(bDetail)
        If Vartype(i_bOldIsAChildUpdated)='L' And i_bOldIsAChildUpdated
            Return(This.Cnt.IsAChildUpdated())
        Else
            Return(This.Cnt.IsAChildUpdated(m.bDetail))
        Endif
    Proc DestroyChildrenChain()
        If Vartype(This.Cnt.w_oHeaderDetail)='O'
            This.Cnt.w_oHeaderDetail.Destroy()
            This.Cnt.w_oHeaderDetail=.Null.
        Endif
        This.Cnt.DestroyChildrenChain()
        Local basename
        basename = This.Name
        Release frm&basename
        Return
    Proc ShowChildrenChain(bFromBatch) && Gestione figli da routine
        This.Cnt.ShowChildrenChain()
        * --- Se figlio aperto per popolarlo da routine posso dedidere di non visualizzarlo
        If !bFromBatch
            This.Show()
        Endif
        Return
    Proc HideChildrenChain()
        *this.cnt.bOnScreen=.f.
        This.Cnt.HideChildrenChain()
        This.Hide()
        Return
    Func CheckChildrenForDelete()
        Return(This.Cnt.CheckChildrenForDelete())
    Func CanDelete()
        Return(This.Cnt.CanDelete())
        * ---
    Proc ecpQuit()
        * Zucchetti Aulla - Inizio
        * Eliminato IF esterno per consentire gestine F10 non bloccante su figli da bottone
        * if !this.cnt.bF10
        If This.IsAChildUpdated()
            This.Cnt.bUpdated=!cp_YesNo(MSG_DISCARD_CHANGES_RECOVERY_DB_QP)
            * Zucchetti Aulla - Inizio - Premendo Esc su figli da bottone e rispondendo Abbandoni
            * le modifiche al successivo rientro l'applicazione visualizza i dati abbandonati
            * Se abbandono le modifiche mi preparo per ricaricare i dati
            * dal database alla prossima apertura
            If Not This.Cnt.bUpdated
                This.Cnt.nDeferredFillRec=1
            Endif
            * Zucchetti Aulla - Fine
        Endif
        If !This.Cnt.bUpdated
            This.Cnt.bDontReportError=.T.
            This.TerminateEdit()
            This.Cnt.bUpdated=.F.
        Endif
        *endif
        *this.cnt.bUpdated=iif(this.cnt.bF10,this.cnt.bUpdated,.f.)
        This.LinkPCClick()
        Return
    Proc ecpSave()
        If This.Cnt.bOnScreen And Inlist(This.cFunction,'Load','Edit')
            * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            If Vartype(This.Cnt.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.Cnt.oPgFrm.Page1.oPag.oHeaderDetail.BlankFilter()
                Select (This.Cnt.cTrsName)
                Set Filter To
            Endif
            If !This.Cnt.CheckForm()
                This.Cnt.SetFocus()
                Return
            Endif
        Endif
        This.Cnt.bF10=.T.
        This.LinkPCClick()
        Return
    Proc ecpEdit()
        cp_msg(cp_Translate(MSG_CALL_THIS_FUNCTION_FROM_PARENT_ENTITY),.F.)
        Return
    Proc ecpLoad()
        cp_msg(cp_Translate(MSG_CALL_THIS_FUNCTION_FROM_PARENT_ENTITY),.F.)
        Return
    Proc ecpNext()
        *wait window "Funzione da chiamare dal padre"
        If Not(Isnull(This.Cnt.oParentObject))
            This.Cnt.oParentObject.ecpNext()
        Endif
        Return
    Proc ecpPrior()
        *wait window "Funzione da chiamare dal padre"
        If Not(Isnull(This.Cnt.oParentObject))
            This.Cnt.oParentObject.ecpPrior()
        Endif
        Return
    Proc ecpPgUp()
        This.Cnt.PgUp()
        Return
    Proc ecpPgDn()
        This.Cnt.PgDn()
        Return
    Proc ecpDelete()
        Local bRes,nRec
        If !This.bSec4
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            Return
        Endif
        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.Cnt.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.Cnt.oPgFrm.Page1.oPag.oHeaderDetail.BlankFilter()
            Select (This.Cnt.cTrsName)
            Set Filter To
        Endif
        If !This.CanDelete()
            Return
        Endif
        If This.Cnt.bLoaded And This.Cnt.cFunction='Query'
            bRes=cp_YesNo(MSG_CONFIRM_DELETING_QP)
            If bRes And !This.CheckChildrenForDelete()
                bRes=cp_YesNo(MSG_DATA_EXIST_IN_CHILD_ENTITY_C_PROCEED_QP)
            Endif
            If bRes
                *--- Activity Logger traccio la cancellazione di un record
                If i_nACTIVATEPROFILER>0
                    cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UF5", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
                Endif
                *--- Activity Logger traccio la cancellazione di un record
                Local bErr
                *wait window "Delete del figlio?"
                cp_BeginTrs()
                Thisform.LockScreen=.T.
                This.Cnt.mDelete()
                bErr=bTrsErr
                Thisform.LockScreen=.F.
                cp_EndTrs()
                If Not(bErr)
                    This.NotifyEvent("Record Deleted")
                    This.Cnt.BlankRec()
                    This.bUpdated=.F.
                Endif
            Endif
        Endif
        Return
    Proc ecpFilter()
        Return
    Proc ecpZoom()
        If This.Cnt.cFunction='Load' Or This.Cnt.cFunction='Edit'
            i_cErrSav=On('ERROR')
            On Error =.T.  && potrebbe non esistere il metodo per il control attivo
            This.Cnt.ActiveControl.Parent.oContained.bDontReportError=.T.
            This.Cnt.ActiveControl.mZoom()
            On Error &i_cErrSav
        Endif
        Return
    Proc ecpZoomOnZoom()
        If This.Cnt.cFunction='Load' Or This.Cnt.cFunction='Edit'
            i_cErrSav=On('ERROR')
            On Error =.T.  && potrebbe non esistere il metodo per il control attivo
            This.Cnt.ActiveControl.Parent.oContained.bDontReportError=.T.
            This.Cnt.ActiveControl.mZoomOnZoom()
            On Error &i_cErrSav
        Endif
        Return
        * --- Refresh form
    Proc ecpRefresh()
        This.NotifyEvent("ecpRefresh")
        This.Refresh()
        Return
    Proc ecpF6()
        If Inlist(This.Cnt.cFunction,'Edit','Load') And Type('this.ActiveControl.parent.oContained')='O'
            This.Cnt.bDontReportError=.T.
            This.ActiveControl.Parent.oContained.F6()
        Endif
        Return
        * ---
    Func BlankRec()
        Return(This.Cnt.BlankRec())
    Func LoadRec()
        Return(This.Cnt.LoadRec())
    Func mInsert()

        * --- Zucchetti Aulla Inizio Gestione dati caricato da, alla data..
        If Vartype(This.Cnt.w_UTDC)<>'U'
            This.Cnt.w_UTDC=SetInfoDate(This.Cnt.cCalUtd)
            If Vartype(This.Cnt.w_UTCC)<>'U'
                This.Cnt.w_UTCC=i_CoduTe
            Endif
        Endif
        * --- Zucchetti Aulla Fine, Gestione dati caricato da, alla data..

        Return(This.Cnt.mInsert())
    Func mReplace(i_bUpd)
        * --- Zucchetti Aulla Inizio -Gestione dati caricato da, alla data..
        If Vartype(This.Cnt.w_UTDV)<>'U'
            * --- Se presente nella gestione la data di caricamento
            * --- allora forzo la scrittura, per contemplare anche
            * --- eventuali modifiche ai figli
            This.Cnt.bUpdated=.T.
            This.Cnt.w_UTDV=SetInfoDate(This.Cnt.cCalUtd)
            If Vartype(This.Cnt.w_UTCV)<>'U'
                This.Cnt.w_UTCV=i_CoduTe
            Endif
        Endif
        * --- Zucchetti Aulla Fine -Gestione dati caricato da, alla data..
        Return(This.Cnt.mReplace(i_bUpd))
    Func mDelete()
        Return(This.Cnt.mDelete())
    Proc NewDocument()
        This.Cnt.NewDocument()
        Return
    Proc ChangeRow(i_cRowID,i_nCallChildren,i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        This.Cnt.ChangeRow(i_cRowID,i_nCallChildren,i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        Return
    Proc SetRow(i_cRowID)
        This.Cnt.SetRow(i_cRowID)
        Return
    Func IsVar(cVarName)
        Return(This.Cnt.IsVar(cVarName))
    Func GetVar(cVarName)
        Return(This.Cnt.GetVar(cVarName))
    Proc SetVar(cVarName,xValue)
        This.Cnt.SetVar(cVarName,m.xValue)
        Return
    Func CheckForm()
        Return(This.Cnt.CheckForm())
    Func CheckRow()
        Return(This.Cnt.CheckRow())
Enddefine

Define Class stdPCContainer As CPContainer
    BorderWidth=0
    * --- variabili proprie
    oParentObject=.Null.
    bOnScreen=.F.
    nDeferredFillRec=0
    bUpdated=.F.
    bHeaderUpdated=.F.
    bF10=.F.
    * --- variabili da stdform
    cCursor=''
    cCursorTrs=''
    *cSaveCursor=''
    *cSaveCursorTrs=''
    cTrsName=''
    cRowID=""
    cQueryFilter=""
    *bCreateSaveCursor=.t.
    cFunction=''
    bLoaded=.F.
    Dimension cWorkTables[1]
    nOpenedTables=0
    bPostIt=.T.
    nPostItConn=0
    bDontReportError=.F.
    oFirstControl=.Null.
    cFile=''
    cComment=''
    cKeySet=''
    bCalculating=.F.                  && indica che e' in corso la routine di ricalcolo
    Dimension xKey[1]
    *** Zucchetti Aulla - Modulo controllo flussi
    cFlowStamp = Space(10)			  && sys(2015) aggiornato su F10 e F5
    bFlowNoRules = .F.				  && se .t. esce subito dalla chiamata
    *** Zucchetti Aulla Fine
    Add Object __dummy__ As CommandButton With Enabled=.F.,Width=0,Height=0,Style=0,SpecialEffect=1
    * --- variabili per la gestione dei figli delle righe di movimentazioni
    Dimension oSaveStatus[1]
    Dimension cSaveStatusKey[1]
    oSaveStaus[1]=.Null.
    nCurrentRow=0
    nFirstRow=0
    nLastRow=0
    i_nRowNum=0
    currentEvent=''
    bLoadRecFilter=.F.                && Se attivo negli oggetti di tipo detail e master/detail viene applicato il filtro sicurezza record anche in fase di load
    * --- configurazione al run-rime
    cRuntimeConfiguration=''
    oRunTime=.Null.
    *--- Zucchetti Aulla Inizio - Caricato il / modificato da
    cCalUtd=Iif(Vartype(g_CALUTD)='C' ,g_CALUTD , 'A')
    *--- Zucchetti Aulla Fine  - Caricato il / modificato da
    bSetFont = .T.

    bSaveContext=.T.
    nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
    * ---
    Proc Init()
        *!!!wait window "Init di STDPCCONTAINER per "+this.class
        Local basename,i,j
        basename=Sys(2015)
        * --- definisce il nome per il suo cursore
        This.cCursor=Sys(2015) &&'cur'+basename
        *this.cSaveCursor=this.cCursor
        This.cKeySet=This.cCursor
        This.OpenWorkTables()
        This.CreateChildren()
        Cp_SetAnchorForm(This)
        If !This.bOnScreen
            This.HideChildrenChain()
        Endif
        This.cFunction='Query'
        * --- !!!
        For i=1 To This.oPgFrm.PageCount
            This.oPgFrm.Pages(m.i).oPag.oContained=This
        Next
        If This.Parent.BaseClass<>'Form'
            This.oParentObject=This.Parent.Parent.Parent.Parent
        Endif
        * --- run-time configuration
        This.oRunTime=Createobject('RunTimeConfigurationAgent',This)
        If Vartype(bLoadRuntimeConfig)='U' Or bLoadRuntimeConfig
            If (Bitand(GetKeyState(VK_CONTROL), GetKeyState(VK_SHIFT), 128) <> 128)
                This.oRunTime.LoadConfig()
            Endif
        Endif

        If i_VisualTheme<>-1 And i_cMenuTab<>"S"
            This.oPgFrm.Tabs=.F.
            This.oPgFrm.BorderWidth=0
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
            *--- Aggiungo un bordo
            This.BorderWidth = 1
            This.BorderColor = i_ThemesManager.GetProp(55)

            *--- Rendo trasparenti le pagine
            Local pg, l_i
            For l_i=1  To This.oPgFrm.PageCount
                pg = This.oPgFrm.Pages[m.l_i]
                pg.BackStyle=0
            Next
            pg=.Null.
            If Vartype(This.oTabmenu)<>'O'
                This.AddObject("oTabMenu","TabMenu", This, .F., .T.)
            Endif
        Endif &&i_VisualTheme<>-1 AND i_cMenuTab<>"S"


        If Vartype(This.oPgFrm)<>"U" And Type("this.oPgFrm.PAGE1.OPAG.ObODY")<>"U"
            This.oPgFrm.Page1.oPag.oBody.RecordMark = i_bRecordMark
            If !i_bRecordMark
                This.oPgFrm.Page1.oPag.oBody.Left = This.oPgFrm.Page1.oPag.oBody.Left + 10
                This.oPgFrm.Page1.oPag.oBody.Width = This.oPgFrm.Page1.oPag.oBody.Width - 10
                This.oPgFrm.Page1.oPag.obody3D.Left = This.oPgFrm.Page1.oPag.obody3D.Left + 10
                This.oPgFrm.Page1.oPag.obody3D.Width = This.oPgFrm.Page1.oPag.obody3D.Width - 10
            Endif
        Endif
        If Vartype("this.oPgFrm")="U" And Type("this.OPAG.ObODY")<>"U"
            This.oPag.oBody.RecordMark = i_bRecordMark
            If !i_bRecordMark
                This.oPag.oBody.Left = This.oPag.oBody.Left + 10
                This.oPag.oBody.Width = This.oPag.oBody.Width - 10
                This.oPag.obody3D.Left = This.oPag.obody3D.Left + 10
                This.oPag.obody3D.Width = This.oPag.obody3D.Width - 10
            Endif
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
        Return
    Procedure SetFont
        Local oPage
        For Each oPage In This.oPgFrm.Pages
            oPage.FontName = SetFontName('P', Thisform.FontName, Thisform.bGlobalFont)
            oPage.FontSize = SetFontSize('P', Thisform.FontSize, Thisform.bGlobalFont)
            oPage.FontItalic = SetFontItalic('P', Thisform.FontItalic, Thisform.bGlobalFont)
            oPage.FontBold = SetFontBold('P', Thisform.FontBold, Thisform.bGlobalFont)
        Endfor
    Endproc

    Proc SetFocusOnFirstControl()
        If !Isnull(This.oFirstControl) And !This.oFirstControl.Enabled
            Local l_obj, i
            For i=1 To This.oPgFrm.Page1.oPag.ControlCount
                With This.oPgFrm.Page1.oPag.Controls(i)
                    If Upper(.BaseClass)='TEXTBOX' And .Visible And .Enabled
                        l_obj=This.oPgFrm.Page1.oPag.Controls(i)
                        l_obj.SetFocus()
                        Return
                    Endif
                Endwith
            Next
            l_obj=This.oPgFrm.Page1
            l_obj.SetFocus()
        Else
            *--- Zucchetti Aulla Inizio -* per evitare esecuzione metodo con oggetto nullo
            If !Isnull(This.oFirstControl)
                This.oFirstControl.SetFocus()
            Endif
            *--- Zucchetti Aulla Fine -* per evitare esecuzione metodo con oggetto nullo
        Endif
    Endproc

    Procedure cpResize(i_dx,i_dy)
        This.oPgFrm.Width=This.Width
        This.oPgFrm.Height=This.Height
        cp_ResizeContainer(This.oPgFrm)
    Proc Destroy()
        This.Erase_Cur()
        Dimension This.oSaveStatus[1]
        This.oSaveStatus[1]=.Null.
        If Used(This.cCursor)
            Select (This.cCursor)
            Use
        Endif
        *if used(this.cSaveCursor)
        *  select (this.cSaveCursor)
        *  use
        *endif
        This.CloseAllTables()
        This.oParentObject=.Null.
        This.oFirstControl=.Null.
        This.DestroyChildren()
        *createobject('TimedFreeResources',this.class,this.oPgFrm.PageCount,'t'+substr(this.class,3),'ts'+substr(this.class,3))
        Return
    Proc SetStatus(cFunction)
        This.cFunction=cFunction
		This.SetEnabled(This.cFunction)
        If This.bOnScreen
            If Inlist(This.cFunction,'Edit','Load')
                This.mEnableControls()
                *this.mHideControls()
            Endif
            This.Refresh()
        Endif
        Return
    Proc FillRec()
        Do Case
            Case This.nDeferredFillRec=0
            Case This.nDeferredFillRec=1
                This.LoadRec()
            Case This.nDeferredFillRec=2
                This.BlankRec()
        Endcase
        Return
    Func CheckForm()
        Return(This.oRunTime.CheckForm())
    Func CheckRow()
        Return(This.oRunTime.CheckRow())
    Procedure mCalc(i_bUpd)
        Return
    Proc mCalledBatchEnd(i_bUpd)
        If Inlist(This.cFunction,"Edit","Load") And !This.bCalculating
            This.mCalc(i_bUpd)
        Endif
        Return
    Proc DoRTCalc(nFrom,nTo,bIsCalc)
        If Vartype(bLoadRuntimeConfig)='U' Or bLoadRuntimeConfig
            This.oRunTime.DoRTCalc(nFrom,nTo,bIsCalc)
        Endif
        Return
    Proc mEnableControls()
        This.oRunTime.mEnableControls()
        Return
    Proc mEnableControlsFixed()
        This.oRunTime.mEnableControls()
        Return
    Proc mObblControls()
        Return
    Proc mHideControls()
        This.oRunTime.mHideControls()
        Return
    Proc mHideControlsFixed()
        Return
    Proc mHideRowControls()
        Return
    Proc mCalcObjs()
        Return
    Proc CreateChildren()
        Return
    Proc DestroyChildren()
        Return
    Proc DestroyChildrenChain()
        This.oFirstControl=.Null.
        This.oParentObject=.Null.
        If Vartype(This.w_oHeaderDetail)='O'
            This.w_oHeaderDetail.Destroy()
            This.w_oHeaderDetail=.Null.
        Endif
        Return
    Func IsAChildUpdated(bDetail)
        Local i, nCount, lbupdated, oCnt
        lbupdated=This.bUpdated
        If m.bDetail
            nCount=Alen(This.oSaveStatus)
            For i=1 To m.nCount
                oCnt=This.oSaveStatus[m.i]
                If Vartype(m.oCnt)='O'
                    lbupdated=m.lbupdated Or m.oCnt.bUpdated
                Endif
            Endfor
        Endif
        Return(m.lbupdated)
    Proc ChildrenNewDocument()
        Return
    Func CheckChildrenForDelete()
        Return(.T.)
    Proc ChildrenChangeRow()
        Return
    Func CanDelete()
        If This.nDeferredFillRec<>0  && record non caricato
            *i_bLoadNow=.t.
            This.bOnScreen=.T.         && forza la loadrec ad avvenire
            This.LoadRec()             && caricato il record, timestamp corretto
            *i_cCurs=this.cSaveCursor
            *this.cSaveCursor=this.cCursor  && setta il cursore da cui prendere i dati per lo storno
        Endif
        Return(!This.bLoaded)
    Func CanAddRow()
        Return(.T.)
    Proc SelectCursor()
        Select (This.cKeySet)
        Return
    Proc QueryKeySet(cWhere,cOrderBy)
        Return
    Proc LoadRecWarn()
        Return
    Proc mUpdateTrs(i_bCanSkip)
        Return
    Proc mRestoreTrs(i_bCanSkip)
        Return
    Func OpenWorkTables()
        This.nOpenedTables=0
        Return(.T.)
    Func OpenAllTables(nTables)
        Local i,ok,Wrk,nidx
        This.nOpenedTables=0
        ok=.T.
        For i=1 To nTables
            If ok
                nidx=cp_OpenTable(This.cWorkTables[i])
                If nidx<>0
                    This.nOpenedTables=This.nOpenedTables+1
                    Wrk=This.cWorkTables[i]
                    This.&Wrk._IDX=nidx
                Else
                    ok=.F.
                Endif
            Endif
        Next
        If ok
            This.SetPostItConn()
        Endif
        Return(ok)
    Proc SetPostItConn()
        Return
    Proc CloseAllTables()
        Local i
        For i=1 To This.nOpenedTables
            Do cp_CloseTable With This.cWorkTables[i]
        Next
        Return
    Proc BlankRec()
        Return
    Proc SaveDependsOn()
        Return
        * ---
    Proc F6()
        *local i_r,i_del
        *if INLIST(this.cFunction,'Edit','Load')
        *  if this.ActiveControl.name='OBODY' and this.FullRow()
        *    this.__dummy__.enabled=.t.
        *    this.__dummy__.SetFocus()
        *    this.SubtractTotals()
        *    delete
        *    i_r=recno()
        *    i_del=set('DELETED')
        *    set deleted off
        *    go top
        *    do while deleted() and !eof()
        *      skip
        *    enddo
        *    this.nFirstRow=recno()
        *    go bottom
        *    do while deleted() and !bof()
        *      skip -1
        *    enddo
        *    this.nLastRow=recno()
        *    set deleted &i_del
        *    go i_r
        *    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
        *    this.oPgFrm.Page1.oPag.oBody.Refresh()
        *    this.bUpdated=.t.
        *    this.__dummy__.enabled=.f.
        *    if this.nFirstRow>this.nLastRow
        *      this.InitRow()
        *    endif
        *    this.oPgFrm.Page1.oPag.oBody.SetFullFocus()
        *    this.mCalc(.t.)
        *    this.NotifyEvent('Row deleted')
        *  endif
        *endif
        Return
    Function FullRow()
        Return(.T.)
        * --- Pagina Su
    Proc PgUp()
        If This.oPgFrm.ActivePage>1
            This.oPgFrm.ActivePage=This.oPgFrm.ActivePage-1
            This.oPgFrm.Click()
        Endif
        Return
        * --- Pagina Giu'
    Proc PgDn
        If This.oPgFrm.ActivePage<This.oPgFrm.PageCount
            This.oPgFrm.ActivePage=This.oPgFrm.ActivePage+1
            This.oPgFrm.Click()
        Endif
        Return
        * --- Procedure per i figli intergrati nel corpo di movimentazioni
    Proc SetKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        Local i_fld,i_v,i
        LOCAL TestMacro
        For i=1 To 12
            i_v = Alltrim(Str(i))
            TestMacro= Type("i_k&i_v.f")
            If TestMacro='C'
                i_fld = 'this.w_'+i_k&i_v.F
                &i_fld = i_k&i_v.v
            Endif
        Next
        Return
    Proc KeyKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        * --- produce la chiave di riga come stringa
        Local i_cKey,i_v,i
        i_cKey=''
        For i=1 To 12
            i_v = Alltrim(Str(i))
            *if type("i_k&i_v.f")='C'
            i_cKey=i_cKey+'\'+cp_ToStr(i_k&i_v.v)
            *endif
        Next
        Return(i_cKey)
        * Rimuove i cursori e gli oggetti per gestire i figli...
    Proc Erase_Cur()
        Local i,i_curs
        For i=1 To Alen(This.oSaveStatus)
            If Type('this.oSaveStatus[i].xSavecursor')='C'
                i_curs=This.oSaveStatus[i].xSavecursor
                If Used(i_curs)
                    Select(i_curs)
                    Use
                Endif
                i_curs=This.oSaveStatus[i].xSavecursortrs
                If Used(i_curs)
                    Select(i_curs)
                    Use
                Endif
                i_curs=This.oSaveStatus[i].xSavetrs
                If Used(i_curs)
                    Select(i_curs)
                    Use
                Endif
            Endif
            This.oSaveStatus[i]=.Null.
        Next
    Endproc

    Proc NewDocument()
        *cp_dbg('New document '+this.Class)
        This.Erase_Cur()
        Dimension This.oSaveStatus[1]
        This.oSaveStatus[1]=.F.
        Dimension This.cSaveStatusKey[1]
        This.cSaveStatusKey[1]=.F.
        This.nCurrentRow=0
        This.ChildrenNewDocument()
        Return
    Proc ChildrenNewDocument()
        Return
    Func GetKeyPos(i_cRowID)
        Local i_nKey,i_nLen
        If Type("this.cSaveStatusKey[1]")='L'
            This.cSaveStatusKey[1]=i_cRowID
            Return(1)
        Endif
        i_nKey=Ascan(This.cSaveStatusKey,i_cRowID)
        If i_nKey=0
            i_nLen=Alen(This.cSaveStatusKey)+1
            Dimension This.cSaveStatusKey[i_nLen]
            This.cSaveStatusKey[i_nLen]=i_cRowID
            Return(i_nLen)
        Endif
        Return(i_nKey)
    Proc ChangeRow(i_cRowID,i_nCallChildren,i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        Local i_cSel,i_nNextRow
        *cp_dbg('change to row code = '+alltrim(i_cRowID))
        i_cSel=Select()
        i_nNextRow=This.GetKeyPos(i_cRowID)
        If i_nNextRow=This.nCurrentRow And This.nDeferredFillRec=0
            * --- cambio riga fittizzio: non e' necessario salvare lo stato, ma e' necessario cambiare la chiave
            This.SetKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
            Return
        Endif
        * --- salva il context corrente
        If This.nCurrentRow<>0 And This.nDeferredFillRec=0 And This.bSaveContext
            If Alen(This.oSaveStatus)<This.nCurrentRow
                Dimension This.oSaveStatus[this.nCurrentRow]
            Endif
            If Type("this.oSaveStatus[this.nCurrentRow]")<>'O'
                This.oSaveStatus[this.nCurrentRow]=This.NewContext()
                *cp_dbg('Creato nuovo contesto in '+alltrim(str(this.nCurrentRow)))
            Endif
            * --- all' inizio c'e' uno stato vuoto!
            This.oSaveStatus[this.nCurrentRow].Save(This)
        Endif
        *!
        *this.oSaveStatus[this.nCurrentRow].cRowID=this.cRowID
        *cp_dbg('Salva il contesto in '+this.cRowID)
        *!
        * --- seleziona il nuovo context
        This.nCurrentRow=i_nNextRow &&this.GetKeyPos(i_cRowID)
        *cp_dbg('for code '+i_cRowID+' index '+str(this.nCurrentRow))
        This.cRowID=i_cRowID
        * --- carica il nuovo  context
        If Type("this.oSaveStatus[this.nCurrentRow]")='O'
            * --- gia' caricato
            *cp_dbg('carica dal contesto '+alltrim(str(this.nCurrentRow)))
            This.oSaveStatus[this.nCurrentRow].Load(This)
            This.SetKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        Else
            *cp_dbg('carica dal file')
            * --- nuovo elemento
            This.bLoaded=.F.
            This.nDeferredFillRec=0
            This.bF10=.F.
            This.bUpdated=.F.
            This.SetKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
            This.LoadRec()
        Endif
        If This.bOnScreen
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.mEnableControls()
                *this.mHideControls()
            Else
                This.SetEnabled(This.cFunction)
                This.mHideControls()
            Endif
            This.Refresh()
        Endif
        * --- chiama anche i figli
        If i_nCallChildren<>0
            This.ChildrenChangeRow()
        Endif
        Select (i_cSel)
        Return
    Proc ChildrenChangeRow()
        Return
    Proc SetRow(i_cRowID)
        This.nCurrentRow=This.GetKeyPos(i_cRowID)
        Return
    Proc NewContext()
        Error cp_Translate(MSG_ABSTRACT_FUNCTION_QM)
        Return
    Proc LocateDeletedKey(i_nRowNum)
        Local i_cKey
        i_cKey=This.cKeyWhere
        If i_nRowNum=0
            Locate For &i_cKey And Deleted()
        Else
            Locate For &i_cKey And CPROWNUM=i_nRowNum And Deleted()
        Endif
        Return
    Proc LocateDelKeyDetail(i_nRowNum)
        Local i_cKey
        i_cKey=This.cKeyDetail
        If i_nRowNum=0
            Locate For &i_cKey And Deleted()
        Else
            Locate For &i_cKey And CPROWNUM=i_nRowNum And Deleted()
        Endif
        Return
    Proc NotifyEvent(cEvent)
        Return
        * --- metodi di codepainter, che possono essere chiamati dai bottoni con funzioni di sistema
    Func HasCPEvents(i_cOp)
        Return(.T.)
    Proc ecpQuit()
        If This.Parent.BaseClass='Form'
            This.Parent.ecpQuit()
        Else
            i_curform.ecpQuit()
        Endif
        Return
    Proc ecpSave()
        If This.Parent.BaseClass='Form'
            This.Parent.ecpSave()
        Else
            i_curform.ecpSave()
        Endif
        Return
    Proc ecpEdit()
        cp_msg(cp_Translate(MSG_CALL_THIS_FUNCTION_FROM_PARENT_ENTITY),.F.)
        Return
    Proc ecpLoad()
        cp_msg(cp_Translate(MSG_CALL_THIS_FUNCTION_FROM_PARENT_ENTITY),.F.)
        Return
    Proc ecpNext()
        *wait window "Funzione da chiamare dal padre"
        If Not(Isnull(This.oParentObject))
            This.oParentObject.ecpNext()
        Endif
        Return
    Proc ecpPrior()
        *wait window "Funzione da chiamare dal padre"
        If Not(Isnull(This.oParentObject))
            This.oParentObject.ecpPrior()
        Endif
        Return
    Proc ecpPgUp()
        This.PgUp()
        Return
    Proc ecpPgDn()
        This.PgDn()
        Return
    Proc ecpDelete()
        If This.bLoaded
            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.oHeaderDetail.BlankFilter()
                Select (This.cTrsName)
                Set Filter To
            Endif
            *--- Acivity Logger traccio la modifica di un record
            If i_nACTIVATEPROFILER>0
                cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UF5", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
            Endif
            *--- Acivity Logger traccio la modifica di un record
            Local bErr
            *wait window "Delete del figlio?"
            cp_BeginTrs()
            Thisform.LockScreen=.T.
            This.mDelete()
            bErr=bTrsErr
            Thisform.LockScreen=.F.
            cp_EndTrs()
            If Not(bErr)
                This.NotifyEvent("Record Deleted")
                This.BlankRec()
            Endif
        Endif
        Return
    Proc ecpFilter()
        Return
    Proc ecpZoom()
        Return
    Proc ecpZoomOnZoom()
        Return
    Proc ecpF6()
        This.F6()
        Return
    Func IsVar(cVarName)
        Return(Type("this."+cVarName)<>'U')
    Func GetVar(cVarName)
        Return(This.&cVarName)
    Proc SetVar(cVarName,xValue)
        This.&cVarName=m.xValue
        Return
        * ---
    Proc AddWarning(oPostIT)
        cp_msg(cp_Translate(MSG_POSTIN_CANNOT_BE_ADDED_TO_CHILD_ENTITY_TABLE),.F.)
        Return
    Proc mDeleteWarnings()
        Return
        *--- Ricerca nOccurrence-esima ricorrenza del controllo
    Proc GetCtrl(i_cName, nOccurrence)
        Local i,e,c,j, nCount
        nCount=0 &&Contatore delle occorrenze
        nOccurrence=Iif(Type("nOccurrence")="N", nOccurrence, 1)
        Private i_bErr
        If Type("this."+i_cName)='O'
            Return(This.&i_cName)
        Endif
        i_cName=Upper(i_cName)
        e=On('ERROR')
        On Error i_bErr=.T.
        For j=1 To This.oPgFrm.PageCount
            For i=1 To This.oPgFrm.Pages(m.j).oPag.ControlCount
                c=Upper(This.oPgFrm.Pages(m.j).oPag.Controls(m.i).Class)
                i_bErr=.F.
                If Upper(This.oPgFrm.Pages(m.j).oPag.Controls(m.i).Name)==m.i_cName
                    If !i_bErr
                        nCount = m.nCount+1
                        If nCount = m.nOccurrence
                            On Error &e
                            Return(This.oPgFrm.Pages(m.j).oPag.Controls(m.i))
                        Endif
                    Endif
                Endif
                i_bErr=.F.
                If Left(m.c,3)='STD' And !Inlist(c,'STDBUTTON','STDBODY','STDBOX','STDSTRING') And Upper(This.oPgFrm.Pages(m.j).oPag.Controls(m.i).cFormVar)==m.i_cName
                    If !i_bErr
                        nCount = m.nCount+1
                        If nCount = m.nOccurrence
                            On Error &e
                            Return(This.oPgFrm.Pages(m.j).oPag.Controls(m.i))
                        Endif
                    Endif
                Endif
                i_bErr=.F.
                If (c='STDBOX' Or c='STDSTRING' Or c='STDBUTTON' Or c='CP_CALCLBL' ) And Upper(This.oPgFrm.Pages(j).oPag.Controls(i).Caption)==Upper(i_cName)
                    If !i_bErr
                        nCount = m.nCount+1
                        If nCount = m.nOccurrence
                            On Error &e
                            Return(This.oPgFrm.Pages(m.j).oPag.Controls(m.i))
                        Endif
                    Endif
                Endif
                i_bErr=.F.
                If (c='STDBOX'Or c='STDSTRING' Or c='STDBUTTON' Or c='CP_CALCLBL'  Or c='BUTTONMENU' Or c='ADVBUTTONMENU') And Upper(This.oPgFrm.Pages(j).oPag.Controls(i).uid)=Upper(i_cName)
                    If !i_bErr
                        nCount = m.nCount+1
                        If nCount = m.nOccurrence
                            On Error &e
                            Return(This.oPgFrm.Pages(m.j).oPag.Controls(m.i))
                        Endif
                    Endif
                Endif
            Next
        Next
        On Error &e
        Return(.Null.)


        *--- Ricerca nOccurrence-esima ricorrenza del controllo
    Proc GetBodyCtrl(i_cName, nOccurrence)
        Local i,e,c, nCount
        nCount=0 &&Contatore delle occorrenze
        nOccurrence=Iif(Type("nOccurrence")="N", m.nOccurrence, 1)
        Private i_bErr
        If Type("this."+m.i_cName)='O'
            Return(This.&i_cName)
        Endif
        e=On('ERROR')
        On Error i_bErr=.T.
        For i=1 To This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.ControlCount
            c=Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(m.i).Class)
            i_bErr=.F.
            If  Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(m.i).Name)==m.i_cName
                nCount = m.nCount+1
                If nCount = m.nOccurrence
                    On Error &e
                    Return(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(m.i))
                Endif
            Endif
            If Left(m.c,3)='STD' And !Inlist(m.c,'STDBUTTON','STDBODY','STDBOX','STDSTRING') And Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(m.i).cFormVar)==Upper(m.i_cName)
                If !i_bErr
                    nCount = m.nCount+1
                    If nCount = m.nOccurrence
                        On Error &e
                        Return(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(m.i))
                    Endif
                Endif
            Endif
            i_bErr=.F.
            If (m.c='STDBUTTON'   Or m.c='BUTTONMENU'  Or m.c='ADVBUTTONMENU') And (Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(m.i).Caption)==Upper(m.i_cName) Or Upper(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(m.i).uid)==Upper(m.i_cName))
                If !i_bErr
                    nCount = m.nCount+1
                    If nCount = m.nOccurrence
                        On Error &e
                        Return(This.oPgFrm.Pages(1).oPag.oBody.Columns(1).oRow.Controls(m.i))
                    Endif
                Endif
            Endif
        Next
        On Error &e
        Return(.Null.)
    Proc WorkFromTrs()
        Return
    Proc ShowChildrenChain()
        This.bOnScreen=.T.
        This.WorkFromTrs() && Valorizzo le variabili di work prima di aggiornare i controls.
        This.SetControlsValue()
        If Inlist(This.cFunction,'Edit','Load')
            This.mEnableControls()
            *this.mHideControls()
        Else
            This.SetEnabled(This.cFunction)
            This.mHideControls()
        Endif
        This.Refresh()
        Return
    Proc HideChildrenChain()
        This.bOnScreen=.F.
        Return
        *proc LostFocus()
        *  if inlist(this.cFunction,'Load','Edit') and this.bOnScreen and i_lastindirectaction<>'ecpZoom' &&!isnull(i_curform) and i_curform.class<>'Stdzoom'
        *    if !this.CheckForm()
        *      this.opgfrm.page1.opag.SetFocus()
        *    endif
        *  else
        *    i_lastindirectaction=''
        *  endif
        *  DoDefault()
        *  return
    Proc TrsFromWork()
        Return
    Function CreateRealChild()
        Return .T.
    Func AddSonsFilter(i_cFlt,i_oTopObject)
        Return i_cFlt

    Procedure SetCCCHKVarInsert(i_ccchkf,i_ccchkv,i_nTblIdx,i_nConn)
        If i_TableProp[i_nTblIdx,7]=1
            i_ccchkf=',CPCCCHK'
            i_ccchkv=','+cp_ToStrODBC(cp_NewCCChk())
        Else
            i_ccchkf=''
            i_ccchkv=''
        Endif
        *ACTIVATE SCREEN
        *? '-->',i_ccchkf,i_ccchkv
        Return

    Procedure SetCCCHKVarReplace(i_ccchkf,i_ccchkw,i_bEditing,i_nTblIdx,i_nConn,i_TrsName)
        Local i_NF
        If i_TableProp[i_nTblIdx,7]=1
            If Not i_TrsName
                i_NF=This.cCursor
            Else
                i_NF=This.cTrsName
            Endif
            i_OldCCCHK = Iif(i_bEditing,&i_NF..CPCCCHK,'')
            i_ccchkf=',CPCCCHK='+cp_ToStrODBC(cp_NewCCChk())
            If i_nConn<>0
                i_ccchkw=' and CPCCCHK='+cp_ToStrODBC(i_OldCCCHK)
            Else
                i_ccchkw=' and CPCCCHK=='+cp_ToStrODBC(i_OldCCCHK)
            Endif
        Else
            i_ccchkf=''
            i_ccchkw=''
        Endif
        *ACTIVATE SCREEN
        *? '-->',i_ccchkf,i_ccchkw
        Return

    Procedure SetCCCHKCursor(i_ccchkf,i_nTblIdx)
        Local i_NF
        If i_TableProp[i_nTblIdx,7]=1
            i_NF=This.cTrsName
            Select (i_NF)
            Replace CPCCCHK With Substr(i_ccchkf,11,10)
        Endif
        Return

    Procedure SetCCCHKVarDelete(i_ccchkw,i_nTblIdx,i_nConn)
        Local i_NF
        If i_TableProp[i_nTblIdx,7]=1
            i_NF=This.cCursor
            If i_nConn<>0
                i_ccchkw=' and CPCCCHK='+cp_ToStrODBC(&i_NF..CPCCCHK)
            Else
                i_ccchkw=' and CPCCCHK=='+cp_ToStrODBC(&i_NF..CPCCCHK)
            Endif
        Else
            i_ccchkw=''
        Endif
        *ACTIVATE SCREEN
        *? '-->',i_ccchkw
        Return

        *-- Aggiunta funzione utilizzata se abilito check di traduzione su un campo del master
    Function GetcKeyWhereODBC()
        Local i_cKeyWhereODBC
        i_cKeyWhereODBC="'1=0'"
        If Vartype(This.cKeyWhereODBC)='C'
            i_cKeyWhereODBC=This.cKeyWhereODBC
        Endif
        Return &i_cKeyWhereODBC

    Function GetcKeyDetailWhereODBC()
        Local i_cKeyWhereODBC, i_TN
        i_cKeyWhereODBC="'1=0'"
        i_TN=This.cTrsName
        If Vartype(This.cKeyDetailWhereODBC)='C'
            i_cKeyWhereODBC=Strtran(This.cKeyDetailWhereODBC, 'i_TN.->', '&i_TN.->')
        Endif
        Return &i_cKeyWhereODBC

        *--- Activity log - tracciatura eventi
    Proc NotifyEvent(cEvent)
        If  i_nACTIVATEPROFILER>0
            cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UEV", cEvent, This, 0, 0, "", This)
        Endif
        *--- Activity log - tracciatura eventi

        * --- Gestione dati caricato da, alla data..
        If Lower(cEvent)='update row start' Or Lower(cEvent)='update start'
            If Vartype(This.w_UTDV)<>'U'
                This.w_UTDV=SetInfoDate(This.cCalUtd)
                If Vartype(This.w_UTCV)<>'U'
                    This.w_UTCV=i_CoduTe
                Endif
            Endif
        Endif

        If Lower(cEvent)='insert row start' Or Lower(cEvent)='insert start'
            If Vartype(This.w_UTDC)<>'U'
                This.w_UTDC=SetInfoDate(This.cCalUtd)
                If Vartype(This.w_UTCC)<>'U'
                    This.w_UTCC=i_CoduTe
                Endif
            Endif
        Endif

        **** Zucchetti Aulla inizio - Sincronizzazione
        Local i_cParam
        If g_APPLICATION = "ad hoc ENTERPRISE" And Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And g_SINC='S'
            Do Case
                Case cEvent='Delete end'
                    i_cParam='D'
                Case cEvent='Insert end'
                    i_cParam='I'
                Case cEvent='Update end'
                    i_cParam='U'
                Case cEvent='Delete start'
                    i_cParam='S'
                Case cEvent='Insert start'
                    i_cParam='K'
                Case cEvent='Update start'
                    i_cParam='V'
                Case cEvent='Delete row end'
                    i_cParam='DR'
                Case cEvent='Insert row end'
                    i_cParam='IR'
                Case cEvent='Update row end'
                    i_cParam='UR'
                Case cEvent='Delete row start'
                    i_cParam='SR'
                Case cEvent='Insert row start'
                    i_cParam='KR'
                Case cEvent='Update row start'
                    i_cParam='VR'
            Endcase
            If !Empty(i_cParam) And Type("g_SINC") = "C"
                If g_SINC = "S" And "\SINC\VFCSSRC" $ Upper(Set("path"))
                    l_sPrg="GSSI_BSD"
                    Do (l_sPrg) With This,i_cParam
                Endif
            Endif
        Endif
        **** Zucchetti Aulla fine - Sincronizzazione

        **** Zucchetti Aulla inizio - Controllo flussi
        If Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And IsFlus(This)
            Local i_cParam
            Do Case
                Case cEvent='Delete start'
                    i_cParam='DS'
                Case cEvent='Insert start'
                    i_cParam='IS'
                Case cEvent='Update start'
                    i_cParam='US'
                Case cEvent='Delete row start'
                    i_cParam='DR'
                Case cEvent='Insert row start'
                    i_cParam='IR'
                Case cEvent='Update row start'
                    i_cParam='UR'
                Otherwise
                    i_cParam=''
            Endcase
            If !Empty(i_cParam)
                l_sPrg="GSCF_BCF"
                Do (l_sPrg) With This,i_cParam
            Endif
        Endif
        **** Zucchetti Aulla fine - Controllo flussi

        If Inlist(m.cEvent,'Update row start','Insert row start','Delete row start')
            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
            Endif
        Endif

        Return
    Procedure bOnScreen_assign
        Lparameters xNewVal
        This.bOnScreen = m.xNewVal
        *--- Aggiunge la form al WindowsManager
        If Vartype(i_oDeskmenu)='O' And Type("This.parent.hWnd")<>'U' And This.bOnScreen
            i_oDeskmenu.olistWnd.AddFormItem(This.Parent.HWnd)
        Endif
        If Vartype(i_oDeskmenu)='O' And Type("This.parent.hWnd")<>'U' And !This.bOnScreen
            i_oDeskmenu.olistWnd.OnDestroyForm(This.Parent.HWnd)
        Endif
    Endproc

Enddefine

Define Class stdPCTrsContainer As stdPCContainer
    i_nRowPerPage=5
    oNewFocus=.Null.
    bCurPos=0 && identifica il numero record da "marcare" sul quale riposionarsi
    bAbsRow=0 && identifica la variabile di stato corrispondente sul body
    bRelRow=0 && identifica la variabile di stato corrispondente sul body
    nMarkPos=0 && identifica la variabile di stato corrispondente sul body
    * ---
    * Memorizza le variabili di stato per rispristinare in modo corretto il transitorio
    * Se passato a .t. il parametro non svolge la TrsFormWork (nel caso di utilizzo all'interno di una CheckRow)
    Proc MarkPos(NoTrsFromWork)
        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
            This.nMarkPos=0
        Endif

        If This.nMarkPos=0
            If Used(This.cTrsName)
                If Not NoTrsFromWork
                    This.TrsFromWork()
                Endif
                This.bCurPos=Iif( Eof(This.cTrsName) , Recno(This.cTrsName)-1 ,  Recno(This.cTrsName) )
                This.bAbsRow=This.oPgFrm.Page1.oPag.oBody.nAbsRow
                This.bRelRow=This.oPgFrm.Page1.oPag.oBody.nRelRow
            Else
                This.bCurPos=0
                This.bAbsRow=0
                This.bRelRow=0
            Endif
        Endif
        This.nMarkPos=This.nMarkPos+1
    Endproc

    * Per rispristinare la posizione sul transitorio (previa chiamata al metodo MarkPos)
    * Se passato NoSavedependsOn a .t. non esegue la SaveDependsON
    * Nel caso di un batch lanciato da una Changed questa non va svolta
    Proc RePos(NoSaveDependsOn)
        Local L_OldArea,i_r,i_del,L_NumRec
        If Used(This.cTrsName)
            * memorizzo l'area di lavoro per riposizionarmici
            L_OldArea=Select()

            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
            Endif

            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
            Endif

            Select (This.cTrsName)
            Count For Not Deleted() To L_NumRec
            If L_NumRec<=1
                Go Top
                * --- Se la Top mi porta oltre l'ultimo record se tutti cancellati la Count va oltre..
                If Recno(This.cTrsName)>Reccount(This.cTrsName)
                    Go 1
                Endif
            Else
                Go This.bCurPos
            Endif
            With This
                .WorkFromTrs()
                If Not NoSaveDependsOn
                    .SaveDependsOn()
                Endif
                .SetControlsValue()
                .ChildrenChangeRow()
                .mHideControls()
                .oPgFrm.Page1.oPag.oBody.Refresh()
                .oPgFrm.Page1.oPag.oBody.nAbsRow= This.bAbsRow
                .oPgFrm.Page1.oPag.oBody.nRelRow= This.bRelRow
                * --- Inserito pezzo di codice prelevato dalla F6 per gestire
                * --- i record cancellati con DELETE dal temporaneo.
                i_r=Recno()
                i_del=Set('DELETED')
                Set Deleted Off
                Go Top
                Do While Deleted() And !Eof()
                    Skip
                Enddo
                .nFirstRow=Recno()
                Set Deleted &i_del
                Go i_r
            Endwith
            * Mi riposiziono sull'area di lavoro precedente
            Select( L_OldArea )
        Endif
        This.nMarkPos=This.nMarkPos-1
    Endproc

    * Per aggiungere una riga al temporaneo cTrsName
    * Si riposiziona anche sull'area di lavoro in cui si era in precedenza
    * se per esempio si richiama all'interno di una Scan su un Cursore
    Proc AddRow()
        Local L_OldArea, bNewRow
        If Used(This.cTrsName)
            * memorizzo l'area di lavoro per riposizionarmici
            L_OldArea=Select()

            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
            Endif

            Select (This.cTrsName)
            Go Bottom
            * Per Controllare se riga � piena devo prima verificare
            * se la riga esiste. Questa condizione � determinata
            * dal fatto che sia EOF che BOF sono .t. (funziona perch�
            * abbiamo svolto una go Bottom)
            If Eof( This.cTrsName ) And Bof( This.cTrsName )
                * Cursore vuoto inserisco sempre
                bNewRow=.T.
            Else
                * Controllo condizione di riga piena
                bNewRow=This.FullRow()
            Endif
            * Controllo condizione di riga piena
            If bNewRow
                * Se l'ultima riga del transitorio � piena ne aggiungo una nuova....
                This.InitRow()
            Else
                * ...altrimenti aggiorno le variabili di lavoro dai campi del cursore (w_...= t_...)
                This.WorkFromTrs()
            Endif
            * Mi riposiziono sull'area di lavoro precedente
            Select( L_OldArea )
        Endif
    Endproc
    * Riceve come parametro l'espressione di ricerca e ritorna il primo numero record che la
    * soddisfa. Il secondo parametro, se impostato, continua la ricerca a partire
    * dal numero record specificato.
    * L'implementazione considera il numero record una visione alta di RecNo()
    * ATTENZIONE per scelta implementativa considera anche le righe cancellate
    * x poter dare pi� libert� di utilizzo
    Function Search(Criterio,StartFrom)
        Local cOldArea,cName_1,cName_2, cCursName,nResult,cCond
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        cName_1=This.cTrsName
        cName_2=This.cTrsName+'_bis'
        cCursName=Sys(2015)
        cCond=' 1=1 '

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        * Mi chiedo se attiva o meno la presenza del doppio transitorio
        If Used(cName_2)
            * Se ricerca a partire da un certo record aggiungo la condizione..
            If Type('StartFrom')='N' And StartFrom<>0
                cCond=' Curs1.I_RECNO>'+Alltrim(Str(StartFrom))
            Endif
            Select Min(Curs1.I_RECNO) As Riga From (cName_1) Curs1 ,(cName_2) Curs2 Where ( Curs1.I_RECNO= Curs2.I_RECNO And &cCond) And;
                &Criterio Into Cursor &cCursName
        Else
            * Se ricerca a partire da un certo record aggiungo la condizione..
            If Type('StartFrom')='N' And StartFrom<>0
                cCond=' I_RECNO>'+Alltrim(Str(StartFrom))
            Endif
            Select Min(I_RECNO) As Riga From (cName_1) Where  &cCond And ;
                &Criterio Into Cursor &cCursName
        Endif
        * leggo il risultato
        Select(cCursName)
        Go Top
        nResult=Nvl(&cCursName..Riga,-1)
        nResult=Iif(nResult=0,-1, nResult)
        * rimuovo il cursore di appoggio
        If Used(cCursName)
            Select (cCursName)
            Use
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
        * restituisco il risultato
        Return nResult
    Endfunc


    * Crea un temporaneo Fox basato esclusivamente sui dati presenti nei transitori...
    * Di fatto costrusce una Query di selezione sul temporaneo rendendo trasparente l'implementazione
    * dell'eventuale doppio transitorio..
    * A T T E N Z I O N E  la routine lascia volutamente l'area di lavoro sul temporaneo appena creato
    * al primo record..
    * parametri
    * cTmp, nome del temporaneo Fox da restituire
    * cFields, elenco dei campi
    * cWhere, condizioni di where da applicare
    * cOrder, ordinamenti
    * cGroup, eventuali raggruppamenti
    * cHaving, eventuali filtri su raggruppamenti
    * bDeleted se non valorizzata esclude i record cancellati (default), se a .t. li include
    *            (il test Deleted non si pu� utilizzare all'interno di una select
    *             VFP prende quello del record corrente)
    Procedure Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving,bDeleted)
        Local cName_1,cName_2,cCmdSql,cOlderr,cMsgErr
        * Assegnamenti..
        cOlderr=On('error')
        cMsgErr=''
        On Error cMsgErr=Message(1)+': '+Message()

        cTmp=   Iif( Empty(cTmp)   , '__Tmp__', cTmp )
        cFields=Iif( Empty(cFields), '*'      , cFields )
        cWhere= Iif( Empty(cWhere) , '1=1'    , cWhere )
        cOrder= Iif( Empty(cOrder) , '1'      , cOrder )
        cOrder=Iif( Empty(cGroupBy),'',' GROUP BY '+(cGroupBy)+Iif( Empty(cHaving),'',' HAVING '+(cHaving) ) )+' Order By '+cOrder

        cName_1=This.cTrsName
        cName_2=This.cTrsName+'_bis'

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        cCmdSql='Select '+cFields+' From '+cName_1

        * Mi chiedo se attivo il doppio transitorio...
        If Used(This.cTrsName+'_bis')
            cCmdSql=cCmdSql+' Curs1,'+cName_2+' Curs2 Where Curs1.I_RECNO=Curs2.I_RECNO And '
        Else
            cCmdSql=cCmdSql+' Curs1 Where '
        Endif

        * Gestione cancellati
        Local cOldDeleted
        cOldDeleted=Set("Deleted")
        If bDeleted
            * Considero anche i cancellati
            Set Deleted Off
        Else
            * di default escludo i cancellati
            Set Deleted On
        Endif

        cCmdSql=cCmdSql+cWhere+' &cOrder Into Cursor '+cTmp+' Nofilter '
        * Eseguo la frase..
        Select (This.cTrsName)
        &cCmdSql
        * Ripristino la vecchia gestione errori
        On Error &cOlderr

        * Ripristino la gestione dei cancellati..
        Set Deleted &cOldDeleted


        * Se Qualcosa va storto lo segnalo ed esco...
        If Not Empty(cMsgErr)
            cp_ErrorMsg(cMsgErr,'stop')
            * memorizzo nella clipboard la frase generata in caso di errore
            _Cliptext=cCmdSql
        Else
            * mi posiziono sul temporaneo al primo record..
            Select (cTmp)
            Go Top
        Endif
    Endproc

    ************************************************************
    *Cancellazione righe su transitorio che verificano la condizione passata nel parametro cWhere
    Procedure Exec_Delete(cWhere,bNoInitrow)

        Local m.ctmpCursor, cName_1,cName_2,cCmdSql,cOlderr,cMsgErr, L_OldArea,bDeleted
        L_OldArea = Select()

        * Assegnamenti..
        cOlderr=On('error')
        cMsgErr=''
        On Error cMsgErr=Message(1)+': '+Message()
        cWhere= Evl(m.cWhere , '1=1')
        bNoInitrow=Evl(bNoInitrow , .F.)
        ctmpCursor='__Select__'

        cName_1=This.cTrsName
        cName_2=This.cTrsName+'_bis'

        This.Exec_Select(m.ctmpCursor,'Curs1.I_RECNO',m.cWhere)

        If Recno(ctmpCursor)>0 And Used(ctmpCursor)
            *all'interno del cursore _Select_ sono contenute le righe del transitorio da cancellare
            If Used(m.cName_1)
                cCmdSql='Delete ' +m.cName_1 +' from '+m.cName_1+' where '+ m.cName_1+'.I_RECNO  in ( Select I_RECNO from '+ m.ctmpCursor+' )'
                Select (m.cName_1)
                &cCmdSql
            Endif

            * Mi chiedo se attivo il doppio transitorio, se lo � cancello...
            If Used(m.cName_2)
                cCmdSql='Delete ' +m.cName_2 +' from '+m.cName_2+' where ' + m.cName_2+'.I_RECNO in ( Select I_RECNO from '+ m.ctmpCursor+' )'
                Select (m.cName_2)
                &cCmdSql
            Endif

            * Ripristino la vecchia gestione errori
            On Error &cOlderr

            * Se Qualcosa va storto lo segnalo ed esco...
            If Not Empty(m.cMsgErr)
                cp_ErrorMsg(m.cMsgErr,'stop')
                * memorizzo nella clipboard la frase generata in caso di errore
                _Cliptext=m.cCmdSql
            Else
                bDeleted=.T.
                This.bUpdated=.T.
            Endif

            * Se il transitorio, a seguito della cancellazione
            * rimane vuoto allora si esegue una AddRow
            If bDeleted And Not bNoInitrow And This.NumRow()=0
                This.AddRow()
            Endif
            * ripristino la vecchia area
            Select(m.L_OldArea)
            *chiudo il cursore
            Use In Select (m.ctmpCursor)
        Endif
    Endproc

    ******************************************************************************
    * Pemette la modifica massiva di 3 valori del transitorio rendendo trasparente la gestione del doppio transitorio
    * viene sfruttata la exec_select per la determinazione delle righe da modificare e per la valorizzazione del campo e del valore oggetto dell'update
    *cWhere contiene la condizione di where da applicare per la determinazione delle righe da modificare
    *cField1 contiene l'identificativo del primo,campo del transitorio da modificare
    *cValue1 contiene il valore da attribuire al campo cField1
    *cField2 contiene l'identificativo del secondo campo del transitorio da modificare
    *cValue2 contiene il valore da attribuire al campo cField2
    *cField3 contiene l'identificativo del terzo campo del transitorio da modificare
    *cValue3 contiene il valore da attribuire al campo cField3
    * bNoUpd non necessario, se passato a .t. non valorizza i_srv sul transitorio
    Procedure Exec_Update(cWhere,cField1,cValue1,cField2,cValue2,cField3,cValue3,bNoUpd)

        Local cOlderr,L_OldArea,cName_1,cName_2,cCmdSql,cUpdate,cSet,cSet1,cSet2,cSet3,cSet4

        L_OldArea = Select()

        * Assegnamenti..
        cOlderr=On('error')
        cMsgErr=''
        On Error cMsgErr=Message(1)+': '+Message()

        *passaggio parametri
        cWhere= Evl(m.cWhere , '1=1')
        cField1 = Evl(m.cField1 , '')
        cValue1 = Evl(m.cValue1 , "''")
        cField2 = Evl(m.cField2 , '')
        cValue2 = Evl(m.cValue2 , "''")
        cField3 = Evl(m.cField3 , '')
        cValue3 = Evl(m.cValue3 , "''")
        bNoUpd=Evl(bNoUpd , .F.)
        ctmpCursor='__Select__'

        cName_1=This.cTrsName
        cName_2=This.cTrsName+'_bis'
        *condizione

        cUpdate="iif ("+cName_1+".i_Srv<>'A' And "+cName_1+".i_Srv<>'U' And Not Deleted(),'U',"+cName_1+".i_Srv)"

        *vengono selezionate le righe che devono essere modificate con i valori per l'update
        This.Exec_Select(m.ctmpCursor,'Curs1.I_RECNO, '+m.cValue1+' as  r1,'+m.cValue2+' as r2,'+m.cValue3+' as r3', m.cWhere,'','')

        If Used(m.ctmpCursor) And Reccount(m.ctmpCursor)>0
            *Costruzione comando update per il primo transitorio
            cSet=''
            cSet1=Iif(Type(m.cName_1+'.'+m.cField1)<>'U',m.cName_1+'.'+m.cField1+'= '+m.ctmpCursor+'.r1 ','')
            cSet2=Iif(Not Empty(m.cField2) And Type(m.cName_1+'.'+m.cField2)<>'U',m.cName_1+'.'+m.cField2+'= '+m.ctmpCursor+'.r2 ','')
            cSet3=Iif(Not Empty(m.cField3) And Type(m.cName_1+'.'+m.cField3)<>'U',m.cName_1+'.'+m.cField3+'= '+m.ctmpCursor+'.r3 ','')
            cSet4=Iif(m.bNoUpd,'',m.cName_1+'.i_Srv='+m.cUpdate)

            cSet=m.cSet1
            If !Empty(m.cSet2)
                cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet2
            Endif

            If !Empty(m.cSet3)
                cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet3
            Endif

            If !Empty(m.cSet4)
                cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet4
            Endif

            If !Empty(cSet)
                cCmdSql='Update '+m.cName_1 +' set '+m.cSet+' from '+m.cName_1+','+m.ctmpCursor+' where '+m.cName_1+'.I_RECNO in ( Select I_RECNO from '+ m.ctmpCursor+' ) and '+m.cName_1+'.I_RECNO=' +m.ctmpCursor+'.I_RECNO'
                Select (m.cName_1)
                &cCmdSql
            Endif

            If Used(m.cName_2) And Not Empty(m.cMsgErr)
                *Costruzione comando update per il transitorio_bis
                cSet=''
                cSet1=''
                cSet2=''
                cSet3=''
                cSet1=Iif(Type(m.cName_2+'.'+m.cField1)<>'U',m.cName_2+'.'+m.cField1+'= '+m.ctmpCursor+'.r1 ','')
                cSet2=Iif(Not Empty(m.cField2) And Type(m.cName_2+'.'+m.cField2)<>'U',m.cName_2+'.'+m.cField2+'= '+m.ctmpCursor+'.r2 ','')
                cSet3=Iif(Not Empty(m.cField3) And Type(m.cName_2+'.'+m.cField3)<>'U',m.cName_2+'.'+m.cField3+'= '+m.ctmpCursor+'.r3 ','')

                cSet=m.cSet1
                If !Empty(m.cSet2)
                    cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet2
                Endif

                If !Empty(m.cSet3)
                    cSet=m.cSet+Iif(!Empty(m.cSet),',','')+m.cSet3
                Endif
                If !Empty(cSet)
                    cCmdSql='Update '+m.cName_2 +' set '+m.cSet+' from '+m.cName_2+','+m.ctmpCursor+' where '+m.cName_2+'.I_RECNO in ( Select I_RECNO from '+ m.ctmpCursor+' ) and '+m.cName_2+'.I_RECNO=' +ctmpCursor+'.I_RECNO'
                    Select (m.cName_2)
                    &CmdSql
                Endif
            Endif

            * Se Qualcosa va storto lo segnalo ed esco...
            If Not Empty(m.cMsgErr)
                cp_ErrorMsg(m.cMsgErr,'stop')
                * memorizzo nella clipboard la frase generata in caso di errore
                _Cliptext=m.cCmdSql
            Else
                This.bUpdated=.T.
            Endif

            *chiudo il cursore
            Use In Select (m.ctmpCursor)
            * Ripristino la vecchia gestione errori
            On Error &cOlderr
            Select (L_OldArea)
        Endif
    Endproc

    * Si posiziona all'ennesima riga del transitorio (id_row coincide con la Recno)
    * ATTENZIONE il metodo � stato costruito per utilizzo all'interno di batch non
    * utilizzare per riposizionarsi, utilizzare per questo scopo la Repos abbinata alla MarkPos
    * Se parametro non passato allora aggiorna la riga corrente...
    * Aggiunto un nuovo parametro per rendere opzionabile la WorkFromTrs
    Procedure SetRow(id_row, bnoWorkFromTrs)
        Local cOldArea
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Se paramento valorizzato mi posiziono sulla riga passata
        * altrimenti sulla riga attuale...
        If Type( 'id_row' )='N'
            Select( This.cTrsName )
            Goto (id_row)
            * gestisco anche il transitorio doppio
            If Used(This.cTrsName+'_bis')
                Goto (id_row)
            Endif
        Endif
        If Not bnoWorkFromTrs
            This.WorkFromTrs()
            This.ChildrenChangeRow()
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Si posiziona sulla prima riga non cancellata
    * Prima Routine da invocare per iniziare una scan del transitorio
    * A questa routine, di norma, si deve far seguire una SetRow per valorizzare
    * le variabili di Work..
    Procedure FirstRow()
        This._EndsRow(.F.,.F.,.F.)
    Endproc

    * Si posiziona sulla prima riga cancellata
    * Prima Routine da invocare per iniziare una scan del transitorio
    * A questa routine, di norma, si deve far seguire una SetRow per valorizzare
    * le variabili di Work..
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure FirstRowDel(bAdded)
        This._EndsRow(.T.,.F.,bAdded)
    Endproc

    * Si posiziona sulla prima riga non cancellata
    * Prima Routine da invocare per iniziare una scan del transitorio
    * A questa routine, di norma, si deve far seguire una SetRow per valorizzare
    * le variabili di Work..
    Procedure LastRow()
        This._EndsRow(.F.,.T.,.F.)
    Endproc

    * Si posiziona sulla prima riga cancellata
    * Prima Routine da invocare per iniziare una scan del transitorio
    * A questa routine, di norma, si deve far seguire una SetRow per valorizzare
    * le variabili di Work..
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure LastRowDel(bAdded)
        This._EndsRow(.T.,.T.,bAdded)
    Endproc

    * Metodo di base utilizzato per accedere al primo o ultimo record
    * bLast a .t. va all'ultimo...
    * Se Del_Row a .t. recupera la prima riga cancellata..
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    * 			ha senso solo se si gestiscono le cancellate
    Procedure _EndsRow(Del_Row,bLast,bAdded)
        Local cOldArea,cOldFilter,cOldDeleted,nTrs_Pos
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Memorizzo il vecchio deleted..
        cOldDeleted=Set('Deleted')
        * Rendo visibili o meno i record cancellati...
        If Del_Row
            Set Deleted Off
        Else
            Set Deleted On
        Endif
        Select( This.cTrsName )
        cOldFilter=Filter(This.cTrsName)
        * Se desidero solo le cancellate imposto il filtro su queste...
        If Del_Row
            If bAdded
                Set Filter To Deleted()
            Else
                Set Filter To Deleted() And i_Srv<>'A'
            Endif
        Else
            Set Filter To Not Deleted()
        Endif
        If bLast
            Goto Bottom
        Else
            Goto Top
        Endif
        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            nTrs_Pos=Recno(This.cTrsName)
        Endif
        * Rispristino la Filter precedente...
        Select (This.cTrsName)
        Set Filter To &cOldFilter
        Set Deleted &cOldDeleted
        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            Select(This.cTrsName+'_bis')
            If Reccount(This.cTrsName+'_bis')>=nTrs_Pos
                Goto nTrs_Pos
            Else
                Goto Bottom
            Endif
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Di norma dopo questi metodi occorre lanciare la SetRow..
    * Passa alla riga succesiva sul transitorio non cancellata
    * Arrivato all'ultima riga va alla successiva per far scattare l'eof...
    Procedure NextRow()
        This._MoveRow(1,.F.,.F.)
    Endproc

    * Passa alla riga succesiva sul transitorio cancellata
    * Arrivato all'ultima riga va alla successiva per far scattare l'eof...
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure NextRowDel(bAdded)
        This._MoveRow(1,.T.,bAdded)
    Endproc

    * Passa alla riga succesiva sul transitorio non cancellata
    * Arrivato all'ultima riga va alla successiva per far scattare l'bof...
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure PriorRow()
        This._MoveRow(-1,.F.,.F.)
    Endproc

    * Passa alla riga succesiva sul transitorio cancellata
    * Arrivato all'ultima riga va alla successiva per far scattare l'bof...
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    Procedure PriorRowDel(bAdded)
        This._MoveRow(-1,.T.,bAdded)
    Endproc

    * Metodo di base x implementare lo spostamento di riga a partire dalla riga corrente
    *(ascesa e discesa, cancellati o non cancellati)
    * nStep 1,-1 indica se andare al record successivo o al precedente (ammessi anche passi diversi da 1/-1)
    * Del_Row a .t. per scorrere solo le cancellate...
    * bAdded, nel caso si gestisca le cancellate considera anche le righe aggiunte e cancellate (i_SRV='A' And Deleted())
    * 			ha senso solo se si gestiscono le cancellate
    Procedure _MoveRow(nStep,Del_Row, bAdded)
        Local cOldArea,cOldFilter,cOldDeleted
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Memorizzo il vecchio deleted..
        cOldDeleted=Set('Deleted')
        * Rendo visibili o meno i record cancellati...
        If Del_Row
            Set Deleted Off
        Else
            Set Deleted On
        Endif

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Select( This.cTrsName )
        cOldFilter=Filter(This.cTrsName)
        * Se desidero solo le cancellate imposto il filtro su queste...
        If Del_Row
            If bAdded
                Set Filter To Deleted()
            Else
                Set Filter To Deleted() And i_Srv<>'A'
            Endif
        Else
            Set Filter To Not Deleted()
        Endif
        * Passo la record adiacente...
        Skip nStep
        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            * Se non sono arrivato alla fine del file mi
            * posiziono al solito record
            Select ( This.cTrsName+'_bis' )
            If (Not Eof(This.cTrsName) And nStep>0) Or (Not Bof(This.cTrsName) And nStep<0)
                Goto Recno(This.cTrsName)
            Else
                * altrimenti vado all'ultimo record e passo al successivo..
                If nStep>0
                    Goto Bottom
                Else
                    Goto Top
                Endif
                Skip nStep
            Endif
        Endif
        * Rispristino la Filter precedente...
        Select (This.cTrsName)
        Set Filter To &cOldFilter
        Set Deleted &cOldDeleted
        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Restituisce true se raggiungo l'eof del transitorio...
    Function Eof_Trs()
        Return (Eof(This.cTrsName))
    Endfunc

    * Restituisce true se raggiungo l'Bof del transitorio...
    Function Bof_Trs()
        Return (Bof(This.cTrsName))
    Endfunc

    * Restituisce il numero di righe piene (non cancellate)
    Function NumRow()
        Local cOldArea,nResult,nPos
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        nPos=Recno( This.cTrsName )
        nResult=0
        This.FirstRow()
        Do While Not This.Eof_Trs()
            * Scorro il transitorio e se la riga � piena la considero
            If This.FullRow()
                nResult = nResult + 1
            Endif
            This.NextRow()
        Enddo
        * Mi rimetto sul record dov'ero..
        Select( This.cTrsName )
        If Reccount(This.cTrsName)>=nPos
            Goto nPos
        Else
            Goto Bottom
        Endif
        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            Select(This.cTrsName+'_bis')
            If Reccount(This.cTrsName+'_bis')>=Recno(This.cTrsName)
                Goto Recno(This.cTrsName)
            Else
                Goto Bottom
            Endif
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
        Return nResult
    Endfunc

    * Restituisce il numero di righe cancellate
    Function NumRow_Del()
        Local cOldArea,nResult,nPos
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        nPos=Recno( This.cTrsName )
        nResult=0
        * Scorro le righe cancellate...
        This.FirstRowDel()
        Do While Not This.Eof_Trs()
            nResult = nResult + 1
            This.NextRowDel()
        Enddo
        * Mi rimetto sul record dov'ero..
        Select( This.cTrsName )
        If Reccount(This.cTrsName)>=nPos
            Goto nPos
        Else
            Goto Bottom
        Endif
        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            Select(This.cTrsName+'_bis')
            If Reccount(This.cTrsName+'_bis')>=Recno(This.cTrsName)
                Goto Recno(This.cTrsName)
            Else
                Goto Bottom
            Endif
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
        Return nResult
    Endfunc

    * Restituisce lo stato della riga
    * 'A' Agginunta
    * Empty - Non modifica
    * 'U' MOdificata
    * 'D' deleted
    Function RowStatus()
        Local cOldArea,cResult
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Select ( This.cTrsName )
        If Deleted()
            cResult='D'
        Else
            cResult=i_Srv
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
        Return cResult
    Endfunc

    * Scrive sui temporanei le variabili di Work..
    Procedure SaveRow(bChangeRow)
        This.TrsFromWork()
        If bChangeRow
            This.ChildrenChangeRow()
        Endif
        * marca la riga come modificata..
        This.SetupdateRow()
    Endproc

    * Valorizza sul transitorio il valore per il determinato campo sulla riga attuale
    * corrisponde ad una Replace sul corretto transitorio (non marca la riga come modificata)
    * bNoUpd non necessario, se passato a .t. non valorizza i_srv sul transitorio
    * BupdPro aggiorna anche la corrispondente variabile w_ (Se presente)
    Procedure Set(cItem,vValue,bNoUpd,bUpdProp)
        Local cOldArea,cFldName,cTest,bResult, bNoSrv, sAssign
        LOCAL TestMacro
        * Se specifico il terzo parametro ed � .t. allora non aggiorno i_SRV..
        bNoSrv=Pcount()>=3 And Type("bNoUpd")='L' And bNoUpd
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        * Se le prima due lettere sono w_ le sostituisco con t_
        * altrimenti lascio quanto trovo...
        If Lower(Left(cItem,2))='w_'
            cFldName='t_'+Substr(cItem,3,Len(cItem))
        Else
            cFldName=cItem
        Endif
        cTest=This.cTrsName+'_bis'+'.'+cFldName
        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis') And Type( cTest )<>'U'
            * ricerco il campo nel transitorio doppio
            * se lo trovo aggiorno il due altrimenti l'uno...
            Select( This.cTrsName+'_Bis' )
        Else
            * Aggiorno il transitorio normale...
            Select( This.cTrsName )
        Endif
        * Se cambio allora svolgo la Replace..
        * controllo valori null
        TestMacro=(&cFldName<>vValue Or ( Lower(Left(cItem,2))='w_' And bUpdProp And This.&cItem <> vValue )) Or (Isnull(&cFldName)And Not Isnull(&cFldName)) Or (Isnull(&cFldName)And Not Isnull(&cFldName))
        If TestMacro
            Replace &cFldName With vValue
            * Se richiesto e corrisponde ad una propriet� aggiorno anch'essa
            If Lower(Left(cItem,2))='w_' And bUpdProp
                This.&cItem = vValue
            Endif
            * Ho cambiato qualcosa e non � esplicitamente richiesto di non aggiornare i_SRV ?
            * i_Srv sempre sul temporaneo principale...
            If Not bNoSrv
                This.SetupdateRow()
            Endif
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Restituisce il valore combo/radio per la riga attuale
    * per un determinato item.
    Function GetRadioValue(Item, nOccurrence)
        Local cOldArea,cFldName, oCtrl, Result
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Se le prima due lettere sono t_ le sostituisco con w_
        * altrimenti lascio quanto trovo...
        If Lower(Left(m.Item,2))='t_'
            cFldName='w_'+Substr(m.Item,3,Len(m.Item))
        Else
            cFldName=m.Item
        Endif
        nOccurrence=Iif(Type("nOccurrence")="N", m.nOccurrence, 1)
        oCtrl = This.GetBodyCtrl(m.cFldName, m.nOccurrence)
        If !Isnull(m.oCtrl) And Pemstatus(m.oCtrl, "RadioValue", 5)
            Result = m.oCtrl.RadioValue()
        Endif
        oCtrl = .Null.
        * ripristino la vecchia area
        Select(m.cOldArea)

        * Restituisco il valore recuperato..
        Return(m.Result)
    Endfunc

    * Restituisce il valore sul transitorio per la riga attuale
    * per un determinato item..
    Function Get(Item)
        Local cOldArea,cFldName,cTest
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        * Se le prima due lettere sono w_ le sostituisco con t_
        * altrimenti lascio quanto trovo...
        If Lower(Left(Item,2))='w_'
            cFldName='t_'+Substr(Item,3,Len(Item))
        Else
            cFldName=Item
        Endif
        cTest=This.cTrsName+'_bis'+'.'+cFldName
        * gestisco anche il transitorio doppio
        * A seconda di dove � il campo mi seleziono sul temporaneo corretto...
        If Used(This.cTrsName+'_bis') And Type(cTest)<>'U'
            Select( This.cTrsName+'_Bis' )
        Else
            Select( This.cTrsName )
        Endif
        * Leggo il campo...
        Result=Eval(cFldName)
        * ripristino la vecchia area
        Select(cOldArea)
        * Restituisco il valore recuperato..
        Return(Result)
    Endfunc

    * Ritorna il tipo di un campo sul transitorio
    * Riceve una stringa contenente il nome del campo (Es. w_MVCODICE)
    * restituisce stesso risultato di una Type Vfp
    Function GetType(cFieldsName)
        Local cOldArea,cFldName,cTest
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        * Se le prima due lettere sono w_ le sostituisco con t_
        * altrimenti lascio quanto trovo...
        If Lower(Left(cFieldsName,2))='w_'
            cFldName='t_'+Substr(cFieldsName,3,Len(cFieldsName))
        Else
            cFldName=cFieldsName
        Endif
        cTest=This.cTrsName+'_bis'+'.'+cFldName
        * gestisco anche il transitorio doppio
        * A seconda di dove � il campo mi seleziono sul temporaneo corretto...
        If Used(This.cTrsName+'_bis') And Type( cTest )<>'U'
            Select( This.cTrsName+'_Bis' )
        Else
            Select( This.cTrsName )
        Endif
        * Leggo il campo...
        Result=Type(cFldName)
        * ripristino la vecchia area
        Select(cOldArea)
        Return( Result )
    Endfunc

    * Marca la riga attuale come modificata
    Procedure SetupdateRow()
        Local cOldArea
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Select( This.cTrsName )
        If i_Srv<>'A' And i_Srv<>'U' And Not Deleted()
            Replace i_Srv With 'U'
        Endif
        This.bUpdated=.T.
        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Marca la riga attuale come cancellata
    Procedure DeleteRow(bNoInitrow)
        Local cOldArea, bDeleted
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()

        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Select( This.cTrsName )
        bDeleted=.F.
        * Se non � gia cancellata...
        If Not Deleted()
            Delete
            bDeleted=.T.
            This.bUpdated=.T.
        Endif
        * gestisco anche il transitorio doppio
        If Used(This.cTrsName+'_bis')
            Select(This.cTrsName+'_bis')
            Delete
        Endif

        * Se il transitorio, a seguito della cancellazione
        * rimane vuoto alloro svolgo una AddRow
        If bDeleted And Not bNoInitrow And This.NumRow()=0
            This.AddRow()
        Endif

        * ripristino la vecchia area
        Select(cOldArea)
    Endproc

    * Restituisce l'indice del record corrente
    * sul transitorio
    Function RowIndex()
        *** Zucchetti aulla - gestione oggetto Hedaer nei detail
        * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
        If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
            This.oPgFrm.Page1.oPag.oHeaderDetail.RemoveFilter()
        Endif

        Return ( Recno( This.cTrsName ) )
    Endfunc

    Proc Init()
        *!!!wait window "Init di STDPCTRSCONTAINER "+this.Class
        This.CreateTrs()
        This.cCursorTrs=Sys(2015) &&'crt'+this.name
        stdPCContainer::Init()
        *this.cSaveCursorTrs=this.cCursorTrs
        Return
    Proc Destroy()
        This.DeleteTrs()
        This.oPgFrm.Page1.oPag.oBody.oFirstControl=.Null.
        stdPCContainer::Destroy()
        Return
    Proc CreateTrs()
        Return
    Proc DeleteTrs()
        If Used(This.cTrsName)
            Select (This.cTrsName)
            Use
        Endif
        If Used(This.cCursorTrs)
            Select (This.cCursorTrs)
            Use
        Endif
        *if used(this.cSaveCursorTrs)
        *  select (this.cSaveCursorTrs)
        *  use
        *endif
        Return
    Proc mRestoreTrsDetail(i_bCanSkip)
        Return
    Proc mUpdateTrsDetail(i_bCanSkip)
        Return
    Proc SetStatus(cFunction)
        stdPCContainer::SetStatus(cFunction)
        If cFunction='Edit' And This.bOnScreen
            This.WorkFromTrs()
        Endif
        Return
    Proc F6()
        Local i_r,i_del
        If This.cFunction='Edit' Or This.cFunction='Load'
            If This.ActiveControl.Name='OBODY' And This.FullRow()
                If !This.CanDeleteRow()
                    Return
                Endif
                *--- Activity log - tracciatura F6
                If  i_nACTIVATEPROFILER>0
                    cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UF6", "", This, 0, 0, "", This)
                Endif
                *--- Activity log - tracciatura F6
                This.NotifyEvent('Before row delete')
                This.__dummy__.Enabled=.T.
                This.__dummy__.SetFocus()
                This.SubtractTotals()
                Select (This.cTrsName)
                Delete
                i_r=Recno()
                i_del=Set('DELETED')
                Set Deleted Off
                Go Top
                Do While Deleted() And !Eof()
                    Skip
                Enddo
                This.nFirstRow=Recno()
                Go Bottom
                Do While Deleted() And !Bof()
                    Skip -1
                Enddo
                This.nLastRow=Recno()
                Set Deleted &i_del
                Go i_r
                This.oPgFrm.Page1.oPag.oBody.nAbsRow=0
                This.oPgFrm.Page1.oPag.oBody.Refresh()
                This.bUpdated=.T.
                This.__dummy__.Enabled=.F.
                If This.nFirstRow>This.nLastRow
                    This.InitRow()
                Endif
                This.oPgFrm.Page1.oPag.oBody.SetFullFocus()
                This.mCalc(.T.)
                This.NotifyEvent('Row deleted')
            Endif
        Endif
        Return
    Function CanDeleteRow()
        Return(.T.)
    Proc SubtractTotals()
        Return
    Proc ChildrenNewDocument()
        Return
        * --- Pagina Su
    Proc PgUp()
        If This.oPgFrm.ActivePage=1
            This.ScrollBody(-1,This.cFunction='Query')
        Else
            stdPCContainer::PgUp()
        Endif
        Return
        * --- Pagina Giu'
    Proc PgDn()
        If This.oPgFrm.ActivePage=1
            This.ScrollBody(1,This.cFunction='Query')
        Else
            stdPCContainer::PgDn()
        Endif
        Return
    Proc ScrollBody(nDirection,bIsQuery)
        Local i_n,i_errsav
        Private i_err
        If bIsQuery
            If Type('This.ActiveControl')<>'O' Or This.ActiveControl.Name<>'OBODY'
                This.oPgFrm.Page1.oPag.oBody.SetFocus()
            Else
                Select (This.cTrsName)
                Do Case
                    Case Recno()=This.nLastRow And nDirection=1
                        If Type('this.oFirstControl')='O'
                            This.SetFocusOnFirstControl()
                        Endif
                        stdPCContainer::PgDn()
                    Case Recno()=1 And nDirection=-1
                        If Type('this.oFirstControl')='O'
                            This.SetFocusOnFirstControl()
                        Endif
                    Otherwise
                        Skip nDirection*This.i_nRowPerPage
                        If Recno()=This.nLastRow Or Eof()
                            This.oPgFrm.Page1.oPag.oBody.Refresh()
                            cp_msg(cp_Translate(MSG_END_OF_THE_DOCUMENT),.F.)
                        Endif
                Endcase
            Endif
        Else
            i_errsav=On('ERROR')
            i_n=2
            i_err=.F.
            On Error i_err=.T.
            If Type('This.ActiveControl')='O' And This.ActiveControl.Name='OBODY'
                i_n=2
            Else
                i_n=This.ActiveControl.nPag
                If i_err
                    i_n=This.ActiveControl.Parent.nPag
                Endif
            Endif
            On Error &i_errsav
            Do Case
                Case i_n=1
                    If nDirection=1
                        This.oPgFrm.Page1.oPag.oBody.SetFullFocus()
                    Endif
                Case i_n=2
                    Do Case
                        Case (Recno()=1 Or Bof()) And nDirection=-1
                            This.oPgFrm.Page1.oPag.oEndHeader.MoveFocus()
                        Case (Recno()=This.nLastRow Or Eof()) And nDirection=1
                            This.oPgFrm.Page1.oPag.oBeginFooter.MoveFocus()
                        Otherwise
                            Skip nDirection*This.i_nRowPerPage
                            If Type('This.ActiveControl')<>'O' Or This.ActiveControl.Name<>'OBODY'
                                This.oPgFrm.Page1.oPag.oBody.SetFullFocus()
                            Endif
                    Endcase
                Case i_n=3
                    If nDirection=1
                        stdPCContainer::PgDn()
                    Else
                        This.oPgFrm.Page1.oPag.oBody.SetFullFocus()
                    Endif
            Endcase
        Endif
        Return
    Procedure AddVline(x)
        If i_ThemesManager.GetProp(120)=-1
            Local N
            N='_vl_'+Alltrim(Str(x))
            This.oPgFrm.Page1.oPag.AddObject(N,'shape') &&Line(150,00,150,00)
            This.oPgFrm.Page1.oPag.&N..Visible=.T.
            This.oPgFrm.Page1.oPag.&N..Top=This.oPgFrm.Page1.oPag.obody3D.Top+1
            This.oPgFrm.Page1.oPag.&N..Left=x
            This.oPgFrm.Page1.oPag.&N..Width=1
            This.oPgFrm.Page1.oPag.&N..Height=This.oPgFrm.Page1.oPag.obody3D.Height-2
            This.oPgFrm.Page1.oPag.&N..ZOrder(0)
            This.oPgFrm.Page1.oPag.&N..BorderColor = i_ThemesManager.GetProp(TOOLBAR_BORDERCOLOR)
            *** Zucchetti aulla - gestione oggetto Hedaer nei detail
            If Vartype(This.oPgFrm.Page1.oPag.oHeaderDetail)='O'
                This.oPgFrm.Page1.oPag.&N..Top = This.oPgFrm.Page1.oPag.&N..Top - This.oPgFrm.Page1.oPag.oHeaderDetail.Height - 1
                This.oPgFrm.Page1.oPag.&N..Height = This.oPgFrm.Page1.oPag.&N..Height + This.oPgFrm.Page1.oPag.oHeaderDetail.Height + 1
            Endif
        Endif
    Endproc
    * Per aggiungere una riga al temporaneo cTrsName
    * Si riposiziona anche sull'area di lavoro in cui si era in precedenza
    * se per esempio si richiama all'interno di una Scan su un Cursore
    Proc AddRow()
        Local L_OldArea,bNewRow
        If Used(This.cTrsName)
            * memorizzo l'area di lavoro per riposizionarmici
            L_OldArea=Select()
            Select (This.cTrsName)
            Go Bottom
            * Per Controllare se riga � piena devo prima verificare
            * se la riga esiste. Questa condizione � determinata
            * dal fatto che sia EOF che BOF sono .t. (funziona perch�
            * abbiamo svolto una go Bottom)
            If Eof( This.cTrsName ) And Bof( This.cTrsName )
                * Cursore vuoto inserisco sempre
                bNewRow=.T.
            Else
                * Controllo condizione di riga piena
                bNewRow=This.FullRow()
            Endif
            * Controllo condizione di riga piena
            If bNewRow
                * Se l'ultima riga del transitorio � piena ne aggiungo una nuova....
                This.InitRow()
            Else
                * ...altrimenti aggiorno le variabili di lavoro dai campi del cursore (w_...= t_...)
                This.WorkFromTrs()
            Endif
            * Mi riposiziono sull'area di lavoro precedente
            Select( L_OldArea )
        Endif
    Endproc

    *--- Se utilizzo SetVar devo segnalare che la riga � aggiornata
    Proc SetVar(cVarName,xValue)
        DoDefault(m.cVarName,m.xValue)
        If This.GetType(m.cVarName)<>'U'
            This.SetupdateRow()
        Endif
    Endproc
Enddefine

Define Class PCContext As Custom
    bLoaded = .F.
    nDeferredFillRec=0
    bF10=.F.
    bUpdated=.F.
    bHeaderUpdated=.F.
    nFirstRow=0
    nLastRow=0
    i_nRowNum=0
    xSavecursor=''
    xSavecursortrs=''
    xSavetrs=''
    *dimension xSaveCursor[1]
    *dimension xSaveCursorTrs[1]
    *dimension xSaveTrs[1]
    cRowID=''
    * --- Zucchetti Aulla Inizio Gestione dati caricato da, alla data..
    cCalUtd=Iif(Vartype(g_CALUTD)='C' ,g_CALUTD , 'A')
    * --- Zucchetti Aulla Inizio Gestione dati caricato da, alla data..
    *proc Destroy()
    *  cp_dbg('distrugge contesto '+this.cRowId)
    Proc Save(oFrom)
        Local i_cSel,i_del
        i_cSel=Select()
        This.bLoaded = oFrom.bLoaded
        This.nDeferredFillRec = oFrom.nDeferredFillRec
        This.bF10 = oFrom.bF10
        This.bUpdated = oFrom.bUpdated
        This.bHeaderUpdated = oFrom.bHeaderUpdated
        This.nFirstRow=oFrom.nFirstRow
        This.nLastRow=oFrom.nLastRow
        This.i_nRowNum=oFrom.i_nRowNum
        i_del=Set('DELETED')
        Set Deleted On
        If This.nDeferredFillRec=0
            If Used(oFrom.cCursor)
                * --- salva il cursore
                *select * from (oFrom.cCursor) into array this.xSaveCursor
                Select (oFrom.cCursor)
                This.SaveCursor(1,oFrom.cCursor)
            Endif
            * --- salva il cursore del detail
            If Not(Empty(oFrom.cCursorTrs)) And Used(oFrom.cCursorTrs)
                *select * from (oFrom.cCursorTrs) into array this.xSaveCursorTrs
                Select (oFrom.cCursorTrs)
                This.SaveCursor(2,oFrom.cCursorTrs)
            Endif
            If Not(Empty(oFrom.cTrsName)) And Used(oFrom.cTrsName)
                * --- salva il transitorio
                Select (oFrom.cTrsName)
                Set Deleted Off
                Replace i_trsdel With 'D' For Deleted()
                *select * from (oFrom.cTrsName) into array this.xSaveTrs
                This.SaveCursor(3,oFrom.cTrsName)
            Endif
        Endif
        Set Deleted &i_del
        Select (i_cSel)
        Return
    Proc Load(oTo)
        Local i_n,i_curs,i_cSel,i_del
        Dimension i_curs[1]
        i_cSel=Select()
        oTo.bLoaded = This.bLoaded
        oTo.nDeferredFillRec = This.nDeferredFillRec
        oTo.bF10 = This.bF10
        oTo.bUpdated = This.bUpdated
        oTo.bHeaderUpdated = This.bHeaderUpdated
        oTo.nFirstRow=This.nFirstRow
        oTo.nLastRow=This.nLastRow
        oTo.i_nRowNum=This.i_nRowNum
        i_del=Set('DELETED')  && questa funzione viene chiamata anche durante la mReplace
        Set Deleted On        && dove il deleted puo' essere OFF per identificare le righe
        && cancellate del transitorio
        If This.nDeferredFillRec=0
            If Used(oTo.cCursor)
                * --- ripristina il cursore
                Select (oTo.cCursor)
                Use Dbf(oTo.cCursor) Again In 0 Alias '__RW__tmp__'
                Select ('__RW__tmp__')
                Delete All
                *append from array this.xSaveCursor
                *this.LoadCursor(1,oTo.cCursor)
                This.LoadCursor(1,'__RW__tmp__')
                Use
                Select (oTo.cCursor)
                Go Top
            Endif
            * --- ripristina il cursore del detail
            If Not(Empty(oTo.cCursorTrs)) And Used(oTo.cCursorTrs)
                Use Dbf(oTo.cCursorTrs) Again In 0 Alias '__RW__tmp__'
                Select ('__RW__tmp__')
                Delete All
                *append from array this.xSaveCursorTrs
                *this.LoadCursor(2,oTo.cCursorTrs)
                This.LoadCursor(2,'__RW__tmp__')
                Use
                Select (oTo.cCursorTrs)
                Go Top
            Endif
            If Not(Empty(oTo.cTrsName)) And Used(oTo.cTrsName)
                * --- ripristina il transitorio
                Select (oTo.cTrsName)
                Zap
                *append from array this.xSaveTrs
                This.LoadCursor(3,oTo.cTrsName)
                Delete For i_trsdel='D'
                Go Top
                oTo.WorkFromTrs()
            Endif
        Endif
        If oTo.bOnScreen And This.nDeferredFillRec<>0
            * --- un figlio che sta' nelle righe potrebbe essere stato portato a video
            *     in una riga precedente. Entrando in questa riga il caricamento non
            *     e' stato fatto e quindi apparirebbe con dei dati non validi
            Do Case
                Case This.nDeferredFillRec=1
                    oTo.LoadRec()
                Case This.nDeferredFillRec=2
                    oTo.BlankRec()
            Endcase
        Endif
        *--- Richiamo la SaveDependsOn in questo modo sono sicuro che la o_ sono valorizzate con le informazini del record corrente
        oTo.SaveDependsOn()
        oTo.mCalcObjs()
        Set Deleted &i_del
        Select (i_cSel)
        Return

    Proc SaveCursor(i_nWhere,i_Curs_Name)
        * --- salva un cursore in un array, anche con i campi MEMO!
        Local i_curs,bNew,i_old
        i_old=Select()
        Go Top
        bNew=.F.
        Do Case
            Case i_nWhere=1
                bNew=Empty(This.xSavecursor)
                This.xSavecursor=Iif(Empty(This.xSavecursor),Sys(2015),This.xSavecursor)
                i_curs=This.xSavecursor
            Case i_nWhere=2
                bNew=Empty(This.xSavecursortrs)
                This.xSavecursortrs=Iif(Empty(This.xSavecursortrs),Sys(2015),This.xSavecursortrs)
                i_curs=This.xSavecursortrs
            Case i_nWhere=3
                bNew=Empty(This.xSavetrs)
                This.xSavetrs=Iif(Empty(This.xSavetrs),Sys(2015),This.xSavetrs)
                i_curs=This.xSavetrs
        Endcase

        * Se non � la prima valorizzazione lo chiudo...
        If Not bNew
            Select(i_curs)
            Use
        Endif
        * Travaso il contenuto nel cursore d'appoggio
        If Version(5)>=700
            Select * From &i_Curs_Name Into Cursor &i_curs Readwrite Nofilter
        Else
            Local w_nome
            w_nome = Sys(2015)
            Select * From &i_Curs_Name Into Cursor (w_nome) Nofilter
            Use Dbf() Again In 0 Alias (i_curs)
            Use
            Select(i_curs)
        Endif
        Select(i_old)

        Return

    Proc LoadCursor(i_nWhere,i_Curs_Tmp)
        * Salva nel cursore associato il contenuto del figlio
        Local i_tmp,i_old
        i_old=Select()
        Do Case
            Case i_nWhere=1
                i_tmp=This.xSavecursor
            Case i_nWhere=2
                i_tmp=This.xSavecursortrs
            Case i_nWhere=3
                i_tmp=This.xSavetrs
        Endcase

        * Select * From i_tmp Into cursor i_Curs_Tmp
        Select (i_tmp)
        Scan
            Scatter Memvar Memo
            Select (i_Curs_Tmp)
            Append Blank
            Gather Memvar Memo
            Select (i_tmp)
        Endscan

        Select( i_old )
        Return

Enddefine

Define Class stdLazyChild As Custom
    oParentObject=.Null.
    cName=''
    i_cRowID=''
    i_nCallChildren=0
    i_k1v=.F.
    i_k1f=.F.
    i_k2v=.F.
    i_k2f=.F.
    i_k3v=.F.
    i_k3f=.F.
    i_k4v=.F.
    i_k4f=.F.
    i_k5v=.F.
    i_k5f=.F.
    i_k6v=.F.
    i_k6f=.F.
    i_k7v=.F.
    i_k7f=.F.
    i_k8v=.F.
    i_k8f=.F.
    i_k9v=.F.
    i_k9f=.F.
    i_k10v=.F.
    i_k10f=.F.
    i_k11v=.F.
    i_k11f=.F.
    i_k12v=.F.
    i_k12f=.F.
    i_cOp=''
    bLoaded=.F.
    cQueryFilter=''
    currentEvent=''
    bSaveContext = .T.
    Procedure Init(i_oParentObject,i_cName)
        This.oParentObject=i_oParentObject
        This.cName=i_cName
    Procedure SaveDependsOn()
        Return
    Procedure SetStatus(i_cOp)
        This.i_cOp=i_cOp
        Return
    Procedure NewDocument()
        Return
    Procedure DestroyChildrenChain()
        If Vartype(This.w_oHeaderDetail)='O'
            This.w_oHeaderDetail.Destroy()
            This.w_oHeaderDetail=.Null.
        Endif
        Return
    Procedure HideChildrenChain()
        Return
    Procedure ChangeRow(i_cRowID,i_nCallChildren,i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        This.i_cRowID=i_cRowID
        This.i_nCallChildren=i_nCallChildren
        This.SetKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        Return
    Procedure SetKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f,i_k11v,i_k11f,i_k12v,i_k12f)
        This.i_k1v=i_k1v
        This.i_k1f=i_k1f
        This.i_k2v=i_k2v
        This.i_k2f=i_k2f
        This.i_k3v=i_k3v
        This.i_k3f=i_k3f
        This.i_k4v=i_k4v
        This.i_k4f=i_k4f
        This.i_k5v=i_k5v
        This.i_k5f=i_k5f
        This.i_k6v=i_k6v
        This.i_k6f=i_k6f
        This.i_k7v=i_k7v
        This.i_k7f=i_k7f
        This.i_k8v=i_k8v
        This.i_k8f=i_k8f
        This.i_k9v=i_k9v
        This.i_k9f=i_k9f
        This.i_k10v=i_k10v
        This.i_k10f=i_k10f
        This.i_k11v=i_k11v
        This.i_k11f=i_k11f
        This.i_k12v=i_k12v
        This.i_k12f=i_k12f
        Return
    Procedure OnScreen
        Local Array aEv[1]
        Local oForm, cDlgBar, l_DlgBar

        Aevents(aEv,0)
        m.oForm = aEv[1]
        cDlgBar="_screen."+m.oForm.Name
        l_DlgBar = &cDlgBar
        l_DlgBar.Visible = m.oForm.bOnScreen
    Endproc
    Procedure OnDlgShow
        Local Array aEv[1]
        Local oForm, cDlgBar, l_DlgBar

        Aevents(aEv,0)
        m.oForm = aEv[1]

    Endproc
    Procedure OnCaption
        Local Array aEv[1]
        Local oForm, cDlgBar, l_DlgBar

        Aevents(aEv,0)
        m.oForm = aEv[1]
        cDlgBar="_screen."+m.oForm.Name
        l_DlgBar = &cDlgBar
        l_DlgBar.Caption = m.oForm.Caption

    Endproc
    Procedure LinkPCClick(bFromBatch) && bFromBatch a .t. se Gestione figli da routine
        Local o
        o=This.CreateRealChild()
        o.LinkPCClick(bFromBatch) && Gestione figli da routine
        Return
    Function CheckForm()
        Return(.T.)
    Function IsAChildUpdated(bDetail)
        Return(.F.)
    Function mReplace(i_bUpd)
        Return(.T.)
    Function CanDelete()
        Local o
        o=This.CreateRealChild()
        Return o.CanDelete()
    Function mDelete()
        Local o
        o=This.CreateRealChild()
        o.mDelete()
        Return
    Function CreateRealChild()
        Local N,o
        N=This.cName
        Set Procedure To &N Additive
        o=Createobject("t"+N)
        This.oParentObject.&N.=o
        o.Cnt.oParentObject=This.oParentObject
        o.ChangeRow(This.i_cRowID,This.i_nCallChildren,This.i_k1v,This.i_k1f,This.i_k2v,This.i_k2f,This.i_k3v,This.i_k3f,This.i_k4v,This.i_k4f,This.i_k5v,This.i_k5f,This.i_k6v,This.i_k6f,;
            this.i_k7v,This.i_k7f,This.i_k8v,This.i_k8f,This.i_k9v,This.i_k9f,This.i_k10v,This.i_k10f,This.i_k11v,This.i_k11f,This.i_k12v,This.i_k12f)
        o.SetStatus(This.i_cOp)
        Release Procedure &N
        Return(o)
    Func AddSonsFilter(i_cFlt,i_oTopObject)
        Return i_cFlt
    Func BuildFilter()
        Return ''
Enddefine

Define Class stdDynamicChild As stdLazyChild
    oCnt=.Null.
    Enabled=.T.
    Procedure Init(i_oParentObject,i_cName,i_oCnt)
        DoDefault(i_oParentObject,i_cName)
        This.oCnt=i_oCnt
        Return
    Function CreateRealChild()
        Local N,o,i_olderr
        N=This.cName
        * --- crea il figlio
        Private i_err
        i_err=.F.
        i_olderr=On('error')
        On Error i_err=.T.
        Do _AddObject With This.oCnt,"child" In &N
        On Error &i_olderr
        If i_err
            Set Procedure To &N Additive
            i_olderr=On('error')
            On Error i_err=.T.
            This.oCnt.AddObject("child","tc"+N)
            On Error &i_olderr
            Release Procedure &N
        Endif
        * ---
        o=This.oCnt.Child
        This.oCnt.Width=o.Width
        This.oCnt.Height=o.Height
        o.Visible=.T.
        o.bOnScreen=.T.
        o.oParentObject=This.oParentObject
        o.NotifyEvent("FormLoad")
        This.oParentObject.&N.=o
        *--- Eliminazione bordo figli integrati - Inizio
        If Type("This.oCnt.bNoBorder")='L' And This.oCnt.bNoBorder
            o.BorderWidth=0
        Endif
        *--- Eliminazione bordo figli integrati - Fine
        o.ShowChildrenChain()
        o.ChangeRow(This.i_cRowID,This.i_nCallChildren,This.i_k1v,This.i_k1f,This.i_k2v,This.i_k2f,This.i_k3v,This.i_k3f,This.i_k4v,This.i_k4f,This.i_k5v,This.i_k5f,This.i_k6v,This.i_k6f,;
            this.i_k7v,This.i_k7f,This.i_k8v,This.i_k8f,This.i_k9v,This.i_k9f,This.i_k10v,This.i_k10f,This.i_k11v,This.i_k11f,This.i_k12v,This.i_k12f)
        o.SetStatus(This.i_cOp)
        o.ShowChildrenChain()
        *o.SetEnabled(o.cFunction)
        o.Enabled=This.Enabled
        If This.oCnt.dx<>0 Or This.oCnt.dy<>0
            This.oCnt.Width=This.oCnt.Width+This.oCnt.dx
            This.oCnt.Height=This.oCnt.Height+This.oCnt.dy
            This.oCnt.cpResize(This.oCnt.dx,This.oCnt.dy)
        Endif
        Return(o)
    Proc ShowChildrenChain()
        Return
Enddefine

Define Class StdDynamicChildContainer As Container
    BorderWidth=0
    BorderColor=Rgb(169,189,224)
    BackStyle=0
    dx=0
    dy=0
    cfghide=''
    
	* --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
	bNoSetAnchorForm = .T.
	* --- Zucchetti Aulla Fine  - Gestione SetAnchorForm
	
    * --- Procedura di Hide standard del campo
    Func mHide()
        Return (.F.)

    Proc Destroy()
        If Type('this.child')='O'
            This.RemoveObject('child')
        Endif
        Return
    Procedure cpResize(i_dx,i_dy)
        If Vartype(This.Child)='O'
            This.Child.Width=This.Width
            This.Child.Height=This.Height
            This.Child.cpResize(i_dx,i_dy)
        Else
            This.dx=This.dx+i_dx
            This.dy=This.dy+i_dy
        Endif
Enddefine

Define Class StdBatch As Custom
    oParentObject=.Null.
    bUpdateParentObject=.T.
    Dimension cWorkTables[1]
    nOpenedTables=0
    nTrs=0
    bOldTrsErr=.F.
    currentEvent=''
    runtime_filters=0
    *
    bFox26=.F.
    *
    bSec1=.T.                         && primo parametro di sicurezza (accesso)
    bSec2=.T.                         && secondo parametro di sicurezza (inserimento)
    bSec3=.T.                         && terzo parametro di sicurezza (modifica)
    bSec4=.T.                         && quarto parametro di sicurezza (cancellazione)
    * ---
    Proc Init(oParentObject)
        If Pcount()>=1 And Type("oParentObject")='O' And !Isnull(oParentObject)
            This.oParentObject=oParentObject
        Endif
        Local i_oldarea
        i_oldarea=Select()
        If This.OpenTables()
            This.nTrs=Iif(Type("nTrsConnCnt")="U",0,Iif(nTrsConnCnt=0,0,1))
            If Type('bTrsErr')='U'
                Public bTrsErr
            Endif
            * --- Zucchetti Aulla Segnalo attivit� sessione
            If Vartype(g_CHKACTBTC)='C' And g_CHKACTBTC='S' And Vartype(g_LASTVERACT)='N'
                g_CHKACTBTC='N'
                i_nChkSeconds=Seconds()
                If (i_nChkSeconds-g_LASTVERACT<0 Or i_nChkSeconds-g_LASTVERACT>10*60)
                    chkzuusr(.Null.,'ACTIVE')
                Endif
                g_CHKACTBTC='S'
            Endif
            * ---
            *** Zucchetti Aulla inizio - debug
            If Vartype(G_DEBUGMODE)='N' And G_DEBUGMODE=2
                cp_dbg('Start running '+This.Name)
            Endif
            *** Zucchetti Aulla fine - debug
            If Vartype(i_CoduTe)='N' And Type("oParentObject")<>'O'
                cp_GetSecurity(This,This.getSecurityCode())
                If !(This.bSec1)
                    cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
                    This.CloseAllTables()
                    Return
                Endif
            Endif
            This.bOldTrsErr=bTrsErr
            *** Zucchetti Aulla inizio - Modifica per attivare la clessidra quando lancio una routine
            If Type('oParentObject.mousepointer')='N'
                oParentObject.MousePointer=11
            Endif
            *--- Zucchetti Aulla Inizio - MousePointer Page
            Local bMousePointerPage
            bMousePointerPage = .F.
            *--- Zucchetti Aulla Fine - MousePointer Page
            If Type('oParentObject.oPgfrm.Pages[oParentObject.oPgfrm.ActivePage].oPag.mousepointer')='N'
                *--- Zucchetti Aulla Inizio - MousePointer Page
                bMousePointerPage = .T.
                *--- Zucchetti Aulla Fine - MousePointer Page
                oParentObject.oPgFrm.Pages[oParentObject.oPgfrm.ActivePage].oPag.MousePointer=11
            Endif
            Local nOldScreenMousePointer
            If Type('_screen.MousePointer')='N'
                nOldScreenMousePointer=_Screen.MousePointer
                _Screen.MousePointer=11
            Endif
            *** Zucchetti Aulla fine - clessidra
            *--- Blocco cp_DockWnd C000000005
            If i_VisualTheme <>-1 And Vartype(i_ThemesManager) ='O' And Not(Vartype(i_DisableLockScreen)='L' And i_DisableLockScreen)
                i_ThemesManager.LockVfp(.T.)
            Endif
            *--- Activity log Tracciatura dell'apertura del batch
            If  i_nACTIVATEPROFILER>0
                cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "BTO", "", Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", This)
            Endif
            *--- Activity log
            This.Pag1()
            bTrsErr=bTrsErr Or This.bOldTrsErr
            This.CloseTransactions()
            This.CloseAllTables()
            This.Done()
            *** Zucchetti Aulla inizio - Modifica per attivare la clessidra quando lancio una routine
            If Type('oParentObject.mousepointer')='N'
                oParentObject.MousePointer=0
            Endif
            *--- Zucchetti Aulla Inizio - MousePointer Page
            *IF TYPE('oParentObject.oPgfrm.Pages[oParentObject.oPgfrm.ActivePage].oPag.mousepointer')='N'
            If bMousePointerPage And Type('oParentObject.oPgfrm.Pages[oParentObject.oPgfrm.ActivePage].oPag.mousepointer')='N'
                *oParentObject.oPgfrm.Pages[oParentObject.oPgfrm.ActivePage].oPag.mousepointer=0
                Local l_i
                For l_i = 1 To oParentObject.oPgFrm.PageCount
                    If Type('oParentObject.oPgfrm.Pages['+Str(l_i)+'].oPag.mousepointer')='N'
                        oParentObject.oPgFrm.Pages[l_i].oPag.MousePointer=0
                    Endif
                Endfor
            Endif
            *--- Zucchetti Aulla Fine - MousePointer Page
            If Type('_screen.MousePointer')='N'
                _Screen.MousePointer=nOldScreenMousePointer
            Endif
            *** Zucchetti Aulla fine
            * --- Zucchetti Aulla Inizio, Activity log
            *--- Tracciatura della chiusura del batch
            If  i_nACTIVATEPROFILER>0
                cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "BTD", "", Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", This)
            Endif
            *--- Activity log
            *--- Blocco cp_DockWnd C000000005
            If i_VisualTheme <>-1 And Vartype(i_ThemesManager) ='O' And Not(Vartype(i_DisableLockScreen)='L' And i_DisableLockScreen)
                i_ThemesManager.LockVfp(.F.)
            Endif
        Endif
        Select(i_oldarea)
        Return
    Proc Done()
        If !Empty(i_Error)
            cp_ErrorMsg(i_Error)
        Endif
        If !Isnull(This.oParentObject) And This.bUpdateParentObject
            *this.oParentObject.SetControlsValue()
            This.oParentObject.mCalledBatchEnd(.T.) && sara' da cambiare in modo da segnalare se il batch ha settato variabili del "caller"
        Endif
        *createobject('TimedFreeResources',this.class,0)
        This.RemoveMemoryCursors()
        Return
        * ---
    Proc RemoveMemoryCursors()
    Endproc
    Func OpenAllTables(i_nTables,i_chkFilter)
        Local i,w,N
        This.nOpenedTables=0
        If This.runtime_filters=0
            cp_LoadRuntimeUserFilters()
        Endif
        For i=1 To i_nTables
            If Left(This.cWorkTables[i],1)<>'*'
                N=cp_OpenTable(This.cWorkTables[i])
                If N<>0
                    If i_bSecurityRecord And This.runtime_filters=0 And Type("i_TablePropSec[n,1]")='C'
                        Do cp_secctrl With Alltrim(This.cWorkTables[i])+'@'+Alltrim(This.getSecurityCode())+'@'
                        Return .F.
                    Else
                        This.nOpenedTables=This.nOpenedTables+1
                        w=This.cWorkTables[i]
                        This.&w._IDX=N
                    Endif
                Else
                    Return(.F.)
                Endif
            Else
                This.nOpenedTables=This.nOpenedTables+1
            Endif
        Next
        Return(.T.)
    Proc CloseAllTables()
        Local i_i,i_n
        For i_i=1 To This.nOpenedTables
            i_n=This.cWorkTables[i_i]
            If Left(i_n,1)='*'
                * --- caso di tabella creata sul server
                i_n=Substr(i_n,2)
            Endif
            cp_CloseTable(i_n)
        Next
        This.CloseCursors()
        Return
    Proc CloseTransactions()
        If Iif(Type("nTrsConnCnt")="U",0,Iif(nTrsConnCnt=0,0,1))>This.nTrs
            cp_msg(cp_Translate(MSG_ROLLBACK_FORCED),.F.)
            bTrsErr=.T.
            cp_EndTrs(.T.)
        Endif
        Return
    Proc CloseCursors()
        Return
    Proc SetControlsValue()
        Return
    Proc mCalc(cVal)
        Return
    Proc mCalledBatchEnd(i_bUpb)
        Return
    Procedure NotifyEvent(cEvent)
        *--- Activity log - tracciatura eventi
        If  i_nACTIVATEPROFILER>0
            cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UEV", cEvent, This, 0, 0, "", This)
        Endif
        *--- Activity log - tracciatura eventi
        Return
    Func IsVar(cVarName)
        Return(Type("this."+cVarName)<>'U' Or Type("this.oParentObject."+cVarName)<>'U')
    Func GetVar(cVarName)
        If Type("this."+cVarName)<>'U'
            Return(This.&cVarName)
        Endif
        Return(This.oParentObject.&cVarName)
    Func SetVar(cVarName,xValue)
        This.&cVarName=m.xValue
        Return
    Procedure SetCCCHKVarsInsert(i_ccchkf,i_ccchkv,i_nTblIdx,i_nConn)
        Local i_NF
        If i_TableProp[i_nTblIdx,7]=1
            i_ccchkf=',CPCCCHK'
            i_ccchkv=','+cp_ToStrODBC(cp_NewCCChk())
        Else
            i_ccchkf=''
            i_ccchkv=''
        Endif
        *ACTIVATE SCREEN
        *? '-->',i_ccchkf,i_ccchkv
        * --- Zucchetti Aulla Inizio - gestione campo UTCC e UTDC
        If i_TableProp[i_nTblIdx,8]=1
            Local bUTFLD
            * Nel comportamento standard (g_UPDSYSFLD="DEF" oppure g_UPDSYSFLD non definita) l'utente schedulatore non aggiorna i campi
            bUTFLD = Type("g_SCHEDULER")<>"C" Or g_SCHEDULER<>"S"
            bUTFLD = Iif(Type("g_UPDSYSFLD")="C", g_UPDSYSFLD="ON" Or g_UPDSYSFLD="DEF" And m.bUTFLD, m.bUTFLD)
            If m.bUTFLD
                i_ccchkf=i_ccchkf+',UTCC,UTDC'
                i_ccchkv=i_ccchkv+','+cp_ToStrODBC(i_CoduTe)+','+cp_ToStrODBC(SetInfoDate(Iif(Vartype(g_CALUTD)='C' ,g_CALUTD , 'A')))
            Endif
        Endif
        * --- Zucchetti Aulla Fine - gestione campo UTCC e UTDC
        Return

    Procedure SetCCCHKVarsWrite(i_ccchkf,i_nTblIdx,i_nConn)
        Local i_NF
        If i_TableProp[i_nTblIdx,7]=1
            i_ccchkf=',CPCCCHK='+cp_ToStrODBC(cp_NewCCChk())
        Else
            i_ccchkf=''
        Endif
        *ACTIVATE SCREEN
        *? '-->',i_ccchkf
        * --- Zucchetti Aulla Inizio - gestione campo UTCC e UTDC
        If i_TableProp[i_nTblIdx,8]=1
            Local bUTFLD
            * Nel comportamento standard (g_UPDSYSFLD="DEF" oppure g_UPDSYSFLD non definita) l'utente schedulatore non aggiorna i campi
            bUTFLD = Type("g_SCHEDULER")<>"C" Or g_SCHEDULER<>"S"
            bUTFLD = Iif(Type("g_UPDSYSFLD")="C", g_UPDSYSFLD="ON" Or g_UPDSYSFLD="DEF" And m.bUTFLD, m.bUTFLD)
            If m.bUTFLD
                i_ccchkf=i_ccchkf+',UTCV ='+cp_ToStrODBC(i_CoduTe)+',UTDV ='+cp_ToStrODBC(SetInfoDate(Iif(Vartype(g_CALUTD)='C' ,g_CALUTD , 'A')))
            Endif
        Endif
        * --- Zucchetti Aulla Fine - gestione campo UTCC e UTDC
        Return
    Func getSecurityCode() && Zucchetti aulla gestito check con parametri (se batch lanciato da men� ha senso applicare la sicurezza anche con il parametro (e necessariamente invariante rispetto alla voce di men� che lo lancia)
        Local nameprg
        nameprg=Upper(Substr(This.Class,2))
        If Type("oParentObject")='C'
            nameprg=nameprg+','+Alltrim(oParentObject)
        Endif
        Return(nameprg)
Enddefine

Define Class cp_getfuncvar As Custom
    currentEvent=''
    Func IsVar(cVarName)
        Return(Type(cVarName)<>'U')
    Func GetVar(cVarName)
        Return(&cVarName)
    Proc SetVar(cVarName,xValue)
        &cVarName=m.xValue
        Return
    Procedure NotifyEvent(cEvent)
        Return
Enddefine

Define Class TimedFreeResources As Timer  && libera risorse
    oThis=.Null.
    cClassName=''
    cClassCnt=''
    cClassCtx=''
    nPag=0
    Interval=500
    Proc Init(cClassName,nPag,cClassCnt,cClassCtx)
        *wait window "set class="+cClassName
        This.cClassName=cClassName
        This.cClassCnt=cClassCnt
        This.cClassCtx=cClassCtx
        This.nPag=nPag
        This.oThis=This
        Return
    Proc Timer()
        This.oThis=.Null.
        Clear Reso
        Clear Class cp_zoombox
        Clear Class cp_szoombox
        Clear Class zzbox
        Clear Class zoombox
        Local N,i,ii
        If Not(Empty(This.cClassCnt))
            N=This.cClassCnt
            Clear Class &N
            *wait window "clear class "+n
        Endif
        If Not(Empty(This.cClassCtx))
            N=This.cClassCtx
            Clear Class &N
            *wait window "clear class "+n
        Endif
        If Not(Empty(This.cClassName))
            N=This.cClassName
            Clear Class &N
            *wait window "clear class "+n
            For i=1 To This.nPag
                ii=N+'Pag'+Alltrim(Str(i))
                Clear Class &ii
                *wait window "clear class "+ii
            Next
            Clear Class &N.BodyRow
            *wait window "clear class "+n+'BodyRow'
        Endif
        If Vartype(maxmemused)='U'
            Public maxmemused
            maxmemused=0
        Endif
        Local um
        um=Sys(1016)
        If Val(um)>maxmemused
            maxmemused=Val(um)
        Endif
        *activate screen
        *@ 1,1 say 'max='+alltrim(transform(maxmemused,"99999999"))+' curr='+alltrim(um)+' '
        Return
Enddefine

Define Class RunTimeConfigurationAgent As Custom
    oObj=.Null.
    Dimension RT_cEventName[1]
    RT_cEventName[1]=''
    Dimension RT_cEventCode[1]
    RT_cEventCode[1]=''
    Dimension RT_cObbl[1]
    RT_cObbl[1]=''
    Dimension RT_cCheck[1]
    RT_cCheck[1]=''
    Dimension RT_cEnable[1]
    RT_cEnable[1]=''
    Dimension RT_cHide[1]
    RT_cHide[1]=''
    Dimension RT_cCalc[1]
    RT_cCalc[1]=''
    Dimension RT_cInit[1]
    RT_cInit[1]=''
    Proc Init(oObj)
        This.oObj=oObj
        Return
    Proc LoadConfig()
        Local fn,clname
        LOCAL TestMacro
        clname=Substr(This.oObj.Class,2)
        If Inlist(vrtClass(This.oObj),'Stdpccontainer','Stdpctrscontainer')
            clname=Substr(clname,2)
        Endif
        If Type('i_rtcfgs')='U'
            * --- cache dei filename gia' controllati
            Public i_rtcfgs
            i_rtcfgs=""
        Endif
        If At('!'+clname+'!',i_rtcfgs)=0
            fn=cp_FindNamedDefaultFile('custom\'+clname,'vrt')
            If Empty(fn)
                fn=cp_FindNamedDefaultFile(clname,'vrt')
                If Empty(fn)
                    fn=cp_FindNamedDefaultFile('std\'+clname,'vrt')
                Endif
            Endif
            If Empty(fn)
                i_rtcfgs=i_rtcfgs+'!'+clname+'!'
            Endif
        Else
            * --- configurazione gia' cercata e non trovata precedentemente
            fn=''
        Endif
        If !Empty(fn)
            Local i_olderr
            This.oObj.cRuntimeConfiguration=fn
            i_olderr=On('error')
            Private i_err
            i_err=.F.
            On Error i_err=.T.
            * --- crea la lista dei control che hanno una workvar o che sono in quelche modo raggiungibili
            Local ii,jj,j,jjj,p,pwd,cpfx
            Local w_vars,w_ctrl
            Dimension w_vars[1],w_ctrl[1]
            j=1
            p=0
            pwd=Lower(Sys(5)+Curdir())
            For jjj=1 To This.oObj.oPgFrm.PageCount+1
                * --- definisce la pagina in cui cercare
                If jjj<=This.oObj.oPgFrm.PageCount
                    i_err=.F.
                    jj=This.oObj.oPgFrm.Pages(jjj).oPag
                    cpfx='this.oObj.oPgFrm.Pages('+Alltrim(Str(jjj))+').oPag.'
                Else
                    i_err=.F.
                    jj=This.oObj.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
                    cpfx='this.oObj.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.'
                Endif
                p=p+1
                If !i_err
                    * --- ora trova i controls
                    For Each ii In jj.Controls
                        If Inlist(vrtClass(ii),'Stdfield','Stdtrsfield','Stdmemo','Stdtrsmemo','Stdcheck','Stdtrscheck','Stdradio','Stdtrsradio','Stdcombo','Stdtrscombo','Stdtablecombo','Stdtrstablecombo','Stdstring','Stdbutton','Stdbox','Stdpccontainer','Stddynamicchildcontainer')
                            Dimension w_vars[j]
                            Dimension w_ctrl[j]
                            w_vars[j]=Lower(ii.uid)
                            w_ctrl[j]=cpfx+ii.Name
                            j=j+1
                        Endif
                    Next
                Endif
            Next
            * --- ora legge le varie configurazioni
            Local h,s,ctrl,Prop,Value
            h=Fopen(fn+'.vrt')
            s=Fgets(h) && skip della prima riga
            If Left(s,21)="* --- Visual Run-Time"
                Do While Not(Feof(h))
                    s=Fgets(h)
                    If s<>'*'
                        s=Strtran(s,'"','')
                        s=Strtran(s,Chr(253),"'")
                        s=Strtran(s,Chr(254),'"')
                        ctrl=s
                        s=Fgets(h)
                        s=Strtran(s,'"','')
                        s=Strtran(s,Chr(253),"'")
                        s=Strtran(s,Chr(254),'"')
                        Prop=s
                        s=Fgets(h)
                        s=Strtran(s,'"','')
                        s=Strtran(s,Chr(253),"'")
                        s=Strtran(s,Chr(254),'"')
                        Value=s
                        ctrl=This.GetConfigCtrl(Lower(ctrl),@w_ctrl,@w_vars)
                        *wait window ctrl+'.'+prop+'='+value
                        If Inlist(Lower(Prop),'tooltiptext','caption','cgetpict','csaypict','cfgcond','cfghide','cfgcheck','cfgdefault','cfgcalc','cfginit')
                            If Type(ctrl)='O' && bisogna controllare se esiste il control: una nuova versione potrebbe averlo cancellato
                                If Inlist(Lower(Prop),'tooltiptext','caption')
                                    &ctrl..&Prop.=cp_Translate(Value)
                                    TestMacro=Lower(Prop)='tooltiptext' And vrtClass(&ctrl.)='Stdradio'
                                    If TestMacro
                                        &ctrl..SetAll("ToolTipText",cp_Translate(Value))
                                    Endif
                                Else
                                    &ctrl..&Prop.=Value
                                Endif
                                Do Case
                                    Case Prop='cfgCheck'
                                        * --- abilita il controllo alla pressione dell' F10
                                        jj=Alen(This.RT_cCheck)
                                        If jj>1 Or !Empty(This.RT_cCheck)
                                            jj=jj+1
                                            Dimension This.RT_cCheck[jj]
                                        Endif
                                        This.RT_cCheck[jj]=ctrl
                                    Case Prop='cfgCond'
                                        jj=Alen(This.RT_cEnable)
                                        If jj>1 Or !Empty(This.RT_cEnable)
                                            jj=jj+1
                                            Dimension This.RT_cEnable[jj]
                                        Endif
                                        This.RT_cEnable[jj]=ctrl
                                    Case Prop='cfgHide'
                                        jj=Alen(This.RT_cHide)
                                        If jj>1 Or !Empty(This.RT_cHide)
                                            jj=jj+1
                                            Dimension This.RT_cHide[jj]
                                        Endif
                                        This.RT_cHide[jj]=ctrl
                                    Case Prop='cfgCalc'
                                        jj=Alen(This.RT_cCalc)
                                        If jj>1 Or !Empty(This.RT_cCalc)
                                            jj=jj+1
                                            Dimension This.RT_cCalc[jj]
                                        Endif
                                        This.RT_cCalc[jj]=ctrl
                                    Case Prop='cfgInit'
                                        jj=Alen(This.RT_cInit)
                                        If jj>1 Or !Empty(This.RT_cInit)
                                            jj=jj+1
                                            Dimension This.RT_cInit[jj]
                                        Endif
                                        This.RT_cInit[jj]=ctrl
                                Endcase
                            Endif
                        Else
                            If ctrl<>'event'
                                If Type(ctrl)='O' && bisogna controllare se esiste il control: una nuova versione potrebbe averlo cancellato
                                    *wait window ctrl+'.'+prop+'='+value
                                    &ctrl..&Prop.=&Value
                                    Do Case
                                        Case Prop='bObbl'
                                            * --- abilita il controllo dei campi obbligatori alla pressione dell' F10
                                            jj=Alen(This.RT_cObbl)
                                            If jj>1 Or !Empty(This.RT_cObbl)
                                                jj=jj+1
                                                Dimension This.RT_cObbl[jj]
                                            Endif
                                            This.RT_cObbl[jj]=ctrl
                                        Case Prop=='height' And  ctrl=='this.oObj'
                                            This.oObj.oPgFrm.Height=This.oObj.Height
                                            This.oObj.oPgFrm.Pages(1).oPag.Height=This.oObj.Height
                                        Case Prop=='width' And  ctrl=='this.oObj'
                                            This.oObj.oPgFrm.Width=This.oObj.Width
                                            This.oObj.oPgFrm.Pages(1).oPag.Width=This.oObj.Width
                                    Endcase
                                Endif
                            Else
                                *wait window "Event "+prop+'='+strtran(value,chr(255),chr(13)+chr(10))
                                jj=Alen(This.RT_cEventName)
                                If jj>1 Or !Empty(This.RT_cEventName[1])
                                    jj=jj+1
                                    Dimension This.RT_cEventName[jj]
                                    Dimension This.RT_cEventCode[jj]
                                Endif
                                This.RT_cEventName[jj]=Prop
                                This.RT_cEventCode[jj]=Value
                            Endif
                        Endif
                    Endif
                Enddo
            Endif
            Fclose(h)
            On Error &i_olderr
        Endif
        Return
    Function GetConfigCtrl(s,c,v)
        Do Case
            Case s='form'
                s='this.oObj'
            Case Left(s,4)='page'
                s='this.oObj.oPgFrm.Pages('+Substr(s,5)+').oPag'
            Otherwise
                Local i
                i=Ascan(v,s)
                If i<>0
                    s=c[i]
                Endif
        Endcase
        Return(s)
    Proc DoRTCalc(i_nFrom,i_nTo,i_bIsCalc)
        Local i_i,i_n,i_e,i_var,i_olderr
        LOCAL TestMacro
        * --- abilita i campi condizionati al run-time
        For i_i=1 To Alen(This.RT_cCalc)
            If !Empty(This.RT_cCalc[i_i])
                i_n=This.RT_cCalc[i_i]
                TestMacro=i_nFrom<=&i_n..rtseq And &i_n..rtseq<=i_nTo
                If TestMacro
                    i_e=Strtran(&i_n..cfgCalc,'w_','this.oObj.w_')
                    i_var=&i_n..cFormVar
                    This.oObj.&i_var=&i_e
                    If i_bIsCalc
                        i_e=Rat('_',i_n,2)
                        i_e='this.link'+Substr(i_n,i_e)+'("Full")'
                        i_olderr=On('ERROR')
                        On Error =.T.
                        &i_e
                        On Error &i_olderr
                    Endif
                Endif
            Endif
        Next
        * --- abilita i campi condizionati al run-time
        If !i_bIsCalc
            For i_i=1 To Alen(This.RT_cInit)
                If !Empty(This.RT_cInit[i_i])
                    i_n=This.RT_cInit[i_i]
                    TestMacro=i_nFrom<=&i_n..rtseq And &i_n..rtseq<=i_nTo
                    If TestMacro
                        i_e=Strtran(&i_n..cfgInit,'w_','this.oObj.w_')
                        i_var=&i_n..cFormVar
                        This.oObj.&i_var=&i_e
                    Endif
                Endif
            Next
        Endif
        Return
    Proc mEnableControls()
        Local i_i,i_n,i_e
        * --- abilita i campi condizionati al run-time
        For i_i=1 To Alen(This.RT_cEnable)
            If !Empty(This.RT_cEnable[i_i])
                i_n=This.RT_cEnable[i_i]
                i_e=Strtran(&i_n..cfgCond,'w_','this.oObj.w_')
                &i_n..Enabled=&i_e And &i_n..mCond()
            Endif
        Next
        * --- disabilita i campi calcolati al run-time
        For i_i=1 To Alen(This.RT_cCalc)
            If !Empty(This.RT_cCalc[i_i])
                i_n=This.RT_cCalc[i_i]
                &i_n..Enabled=.F.
            Endif
        Next
        Return
    Proc mEnableControlsFixed()
        This.mEnableControls()
        Return
    Proc mHideControls()
        Local i_i,i_n,i_e
        * --- abilita i campi condizionati al run-time
        For i_i=1 To Alen(This.RT_cHide)
            If !Empty(This.RT_cHide[i_i])
                i_n=This.RT_cHide[i_i]
                i_e=Strtran(&i_n..cfghide,'w_','this.oObj.w_')
                &i_n..Visible=!&i_e And !&i_n..mHide()
            Endif
        Next
        Return
    Proc mHideControlsFixed()
        This.mHideControls()
        Return
    Proc mHideRowControls()
        This.mHideControls()
        Return
    Func CheckRunTimeCtrls(i_bRepOnly)
        Local i_bRes,i_i,i_n,i_e,i_var
        LOCAL TestMacro
        i_bRes=.T.
        * --- campi obbligatori da run-time
        For i_i=1 To Alen(This.RT_cObbl)
            If i_bRes And !Empty(This.RT_cObbl[i_i])
                i_n=This.RT_cObbl[i_i]
                TestMacro=!i_bRepOnly Or Type(Alltrim(i_n)+".rtrep")<>'L' Or &i_n..rtrep
                If TestMacro
                TestMacro=&i_n..Value
                    If Empty(TestMacro)
                        i_bRes=.F.
                        This.SetFocusToCtrl(i_n)
                        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
                    Endif
                Endif
            Endif
        Next
        * --- campi controllati da run-time
        For i_i=1 To Alen(This.RT_cCheck)
            If i_bRes And !Empty(This.RT_cCheck[i_i])
                i_n=This.RT_cCheck[i_i]
                TestMacro=!i_bRepOnly Or Type(Alltrim(i_n)+".rtrep")<>'L' Or &i_n..rtrep
                If TestMacro
                    i_e=Strtran(&i_n..cfgCheck,'w_','this.oObj.w_')
                    i_bRes=&i_e
                    If !i_bRes
                        Do cp_ErrorMsg With &i_n..sErrorMsg
                        i_var=&i_n..cFormVar
                        This.oObj.&i_var=cp_NullValue(This.oObj.&i_var)
                        &i_n..SetControlValue()
                        This.SetFocusToCtrl(i_n)
                    Endif
                Endif
            Endif
        Next
        Return(i_bRes)
    Func CheckForm()
        Return(This.CheckRunTimeCtrls(.F.))
    Func CheckRow()
        *activate screen
        *? 'run-time checkrow'
        If !This.oObj.FullRow()
            Return(.T.)
        Endif
        Return(This.CheckRunTimeCtrls(.T.))
    Proc SetFocusToCtrl(cCtrl)
        Local ap,apn
        ap=At('Pages(',cCtrl)
        If m.ap<>0
            apn=Substr(cCtrl,m.ap+6)
            ap=At(')',m.apn)
            apn=Left(m.apn,m.ap-1)
            This.oObj.oPgFrm.ActivePage=&apn
        Endif
        &cCtrl..SetFocus()
        Return
Enddefine

Procedure deferred_cp_zoom(i_p1,i_p2,i_p3,i_p4,i_p5,i_p6,i_p7,i_p8,i_p9)
    Createobject('_cp_dfz_',i_p1,i_p2,i_p3,i_p4,i_p5,i_p6,i_p7,i_p8,i_p9)
    Return

Define Class _cp_dfz_ As Timer
    Interval=100
    oThis=.Null.
    oForm=.Null.
    i_p1=''
    i_p2=''
    i_p3=''
    i_p4=''
    i_p5=''
    i_p6=''
    i_p7=''
    i_p8=''
    i_p9=.F.
    Procedure Init(i_p1,i_p2,i_p3,i_p4,i_p5,i_p6,i_p7,i_p8,i_p9)
        This.oThis=This
        This.oForm=i_curform
        This.i_p1=i_p1
        This.i_p2=i_p2
        This.i_p3=i_p3
        This.i_p4=i_p4
        This.i_p5=i_p5
        This.i_p6=i_p6
        This.i_p7=i_p7
        This.i_p8=i_p8
        This.i_p9=i_p9
        Return
    Procedure Timer()
        If Type('This.i_p9.bDontReportError')='L'
            This.i_p9.bDontReportError=.T.
        Else
            This.oForm.bDontReportError=.T.
        Endif
        cp_zoom(This.i_p1,This.i_p2,This.i_p3,This.i_p4,This.i_p5,This.i_p6,This.i_p7,This.i_p8,This.i_p9)
        This.oThis=.Null.
        Return
Enddefine

Define Class cp_MemoryCursor As Custom
    cCursor=''
    cTable=''
    cIndex=''
    oParentObject=.Null.
    bAutoDestroy=.T. && Se impostato a .f. non distrugge il cursore

    Procedure Init(i_cTable,oParent)
        This.cTable=i_cTable
        Local i_cName, i_fn, i_area
        i_area=Select()
        This.oParentObject=oParent
        * --- Interrogo il database per determinare l'elenco
        * --- dei campi che compongono il memorycursor
        * --- La query � per costruzione vuota (Tabella / Visual Query)
        i_cName = This.CreateCursor(This.cTable, .T.)
        * --- Le propriet� di un memory cursor sono definite
        * --- alla sua creazione.
        If Used(i_cName)
            Select (i_cName)
            For i_i=1 To Fcount()
                i_fn=Field(i_i)
                This.AddProperty(i_fn)
                This.&i_fn.=&i_fn
            Next
            This.cCursor=i_cName
        Else
            This.cCursor=''
            This.cTable=''
        Endif
        Select(i_area)
        Return
    Function CreateCursor(i_cTable, i_bEmpty)
        Local i_cName, i_cNewName, i_area, i_nConn, i_loader, i_qry
        i_area=Select()
        i_cName=Sys(2015)
        i_cNewName=Sys(2015)
        If Lower(Right(i_cTable,4))='.vqr'
            i_nConn=i_ServerConn[1,2]
            If At('cp_query',Lower(Set('proc')))=0
                Set Proc To cp_query Additive
            Endif
            i_cTable=Left(i_cTable,Len(i_cTable)-4)
            i_qry=Createobject('cpquery')
            i_loader=Createobject('cpqueryloader')
            i_loader.LoadQuery(i_cTable,i_qry, This.oParentObject)
            If i_bEmpty
                i_qry.mLoadWhereTmp('1=0')  &&Creo il cursore con la sola struttura della query
            Endif
            i_qry.mDoQuery(i_cName,'Exec',.Null.,.F.,.F.,.F.,.F.)
        Else
            i_nidx=cp_OpenTable(i_cTable)
            i_nConn=i_TableProp[i_nIdx,3]
            i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
            If i_nConn<>0
                cp_SQL(i_nConn,"SELECT * from "+i_cTable+Iif(i_bEmpty," WHERE 1=0",""),i_cName)
            Else
                If i_bEmpty
                    Select * From (i_cTable) Where 1=0 Into Cursor (i_cName) &&Creo il cursore con la sola struttura della query
                Else
                    Select * From (i_cTable) Into Cursor (i_cName)
                Endif
            Endif
        Endif
        If Used(i_cName)
            Use Dbf() In 0 Again Alias (i_cNewName)
            Use In Select(i_cName)
        Endif
        Select(i_area)
        Return i_cNewName
    Procedure FillFromQuery(cVqr)
        Local i_area,i_cName
        i_area=Select()
        If !Empty(This.cCursor) And Used(This.cCursor)
            Use In Select(This.cCursor)
        Endif
        * --- Se ricevo una query / tabella come parametro
        * --- utilizzo un'altra query / tabella per
        * --- popolare il cursore
        * --- la tabella / query utilizzata deve avere la medesima struttura
        * --- di quella associata al memory cursor in definizione, pena errori
        * --- al momento dell'utilizzo dei campi del memory cursor (questo metodo non
        * --- ridefinisce le propriet�)
        cVqr=Iif(Empty(cVqr), This.cTable , cVqr)
        i_cName=This.CreateCursor(cVqr, .F.)
        If Used(i_cName)
            This.cCursor=i_cName
            Select (This.cCursor)
            Go Top
            This.ReadCurrentRecord()
        Else
            This.cCursor=''
        Endif
        Select(i_area)
        Return
    Procedure Destroy()
        This.Done()
    Endproc
    Procedure Done()
        This.oParentObject=.Null.
        * --- All'uscita posso decidere di mantenere il cursore
        If This.bAutoDestroy
            If !Empty(This.cCursor) And Used(This.cCursor)
                Use In (This.cCursor)
            Endif
        Endif
        Return
    Procedure SaveRow()
        This.SaveCurrentRecord()
        Return
    Procedure SaveCurrentRecord()
        Local i_i,i_fn
        * selezione cursore
        i_area=Select()
        If !Empty(This.cCursor) And Used(This.cCursor)
            Select (This.cCursor)
        Endif
        If !Eof()
            For i_i=1 To Fcount()
                i_fn=Field(i_i)
                Replace &i_fn With This.&i_fn.
            Next
        Endif
        * mi rimetto nell'area precedente..
        Select(i_area)
        Return
    Procedure ReadCurrentRecord()
        Local i_i,i_fn, i_area
        If !Empty(This.cCursor) And Used(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            For i_i=1 To Fcount()
                i_fn=Field(i_i)
                This.&i_fn.=&i_fn
            Next
            Select(i_area)
        Endif
        Return
    Procedure CurrentRecordToArray(RetArray)
        Local i_i,i_fn, i_area
        If !Empty(This.cCursor) And Used(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            For i_i=1 To Fcount()
                i_fn=Field(i_i)
                Dimension RetArray( i_i, 2)
                RetArray(i_i, 1) = i_fn
                RetArray(i_i, 2) = &i_fn
            Next
            Select(i_area)
        Endif
        Return
    Procedure SetPropertyValues(oChildren, cDest, cOrig)
        Local i_i,i_fn, i_area, cMacro
        If !Empty(This.cCursor) And Used(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            For i_i=1 To Fcount()
                i_fn=Field(i_i)
                cMacro = Alltrim(m.cDest)+Alltrim(m.i_fn)+"="+Alltrim(m.cOrig)+Alltrim(m.i_fn)
                &cMacro
            Next
            Select(i_area)
        Endif
        Return
    Procedure SetLocalValues(oChildren)
        This.SetPropertyValues(m.oChildren, "oChildren.w_","this.")
    Procedure SetObjectValues(oChildren)
        This.SetPropertyValues(m.oChildren, "this.","oChildren.w_")
    Procedure AppendBlank()
        Local i_area,i_i,i_fn
        If !Empty(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            This.SaveCurrentRecord()
            Append Blank
            This.ReadCurrentRecord()
            Select(i_area)
        Endif
        Return
    Procedure Skip(i_nRec)
        Local i_area
        If !Empty(This.cCursor) And Used(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            This.SaveCurrentRecord()
            Skip (i_nRec)
            This.ReadCurrentRecord()
            Select(i_area)
        Endif
        Return
    Procedure Next()
        This.Skip(1)
        Return
    Procedure Prev()
        This.Skip(-1)
        Return
    Procedure gotop()
        Local i_area
        If !Empty(This.cCursor) And Used(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            This.SaveCurrentRecord()
            Go Top
            This.ReadCurrentRecord()
            Select(i_area)
        Endif
        Return
    Procedure gobottom()
        If !Empty(This.cCursor) And Used(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            This.SaveCurrentRecord()
            Go Bottom
            This.ReadCurrentRecord()
            Select(i_area)
        Endif
        Return
    Procedure Goto(i_nRec)
        If !Empty(This.cCursor) And Used(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            This.SaveCurrentRecord()
            Go (i_nRec)
            This.ReadCurrentRecord()
            Select(i_area)
        Endif
        Return
    Procedure Delete()
        If !Empty(This.cCursor) And Used(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            Delete
            This.ReadCurrentRecord()
            Select(i_area)
        Endif
        Return
    Procedure Zap()
        If !Empty(This.cCursor) And Used(This.cCursor)
            i_area=Select()
            Select (This.cCursor)
            Zap
            This.ReadCurrentRecord()
            Select(i_area)
        Endif
        Return
    Function Eof()
        Return Iif(Used(This.cCursor),Eof(This.cCursor),.T.)
    Function Bof()
        Return Iif(Used(This.cCursor),Bof(This.cCursor),.T.)
    Function Recno()
        Return Iif(Used(This.cCursor),Recno(This.cCursor),0)
    Function IsEmpty()
        Return This.Reccount()=0
    Function Reccount()
        Return Iif(Used(This.cCursor),Reccount(This.cCursor),0)
    Procedure OrderBy(i_cExp, i_nSortOrder)
        Local i_area, i_cSortOrder
        i_area=Select()
        Select(This.cCursor)
        If Empty(This.cIndex)
            This.cIndex=Sys(2015)
        Else
            Delete Tag (This.cIndex)
        Endif
        i_cSortOrder=Iif(Type('i_nSortOrder')='N' And i_nSortOrder<>0, 'DESCENDING', 'ASCENDING')
        Index On &i_cExp Tag (This.cIndex)  &i_cSortOrder
        Select(i_area)
    Endproc
    Procedure Exec_MCSelect(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
        Local cName_1,cName_2,cCmdSql,cOlderr,cMsgErr
        * Assegnamenti..
        cOlderr=On('error')
        cMsgErr=''
        On Error cMsgErr=Message(1)+': '+Message()
        cTmp=   Iif( Empty(cTmp)   , '__Tmp__', cTmp )
        cFields=Iif( Empty(cFields), '*'      , cFields )
        cWhere= Iif( Empty(cWhere) , '1=1'    , cWhere )
        cOrder= Iif( Empty(cOrder) , '1'      , cOrder )
        cOrder=Iif( Empty(cGroupBy),'',' GROUP BY '+(cGroupBy)+Iif( Empty(cHaving),'',' HAVING '+(cHaving) ) )+' Order By '+cOrder
        cName_1=This.cCursor
        cCmdSql='Select '+cFields+' From '+cName_1
        cCmdSql=cCmdSql+' Where '
        cCmdSql=cCmdSql+cWhere+' &cOrder Into Cursor '+cTmp+' Nofilter '
        * Eseguo la frase..
        &cCmdSql
        * Ripristino la vecchia gestione errori
        On Error &cOlderr
        * Se Qualcosa va storto lo segnalo ed esco...
        If Not Empty(cMsgErr)
            cp_ErrorMsg(cMsgErr,'stop')
            * memorizzo nella clipboard la frase generata in caso di errore
            _Cliptext=cCmdSql
        Else
            * mi posiziono sul temporaneo al primo record..
            Select (cTmp)
            Go Top
        Endif
    Endproc
    * Esegue ricerca nel cursore del dettaglio documenti
    Function Exec_MCSearch(Criterio,StartFrom)
        Local cOldArea,cName_1, cCursName,nResult,cCond
        * Memorizzo l'area corrente per ripristino
        cOldArea=Select()
        cName_1=This.cCursor
        cCursName=Sys(2015)
        cCond=' 1=1 '

        * Se ricerca a partire da un certo record aggiungo la condizione..
        If Type('StartFrom')='N' And StartFrom<>0
            cCond=' I_RECNO>'+Alltrim(Str(StartFrom))
        Endif
        Select Min(I_RECNO) As Riga From (cName_1) Where  &cCond And ;
            &Criterio Into Cursor &cCursName
        * leggo il risultato
        Select(cCursName)
        Go Top
        nResult=Nvl(&cCursName..Riga,-1)
        nResult=Iif(nResult=0,-1, nResult)
        * rimuovo il cursore di appoggio
        If Used(cCursName)
            Select (cCursName)
            Use
        Endif
        * ripristino la vecchia area
        Select(cOldArea)
        * restituisco il risultato
        Return nResult
    Endfunc

Enddefine

Define Class TabMenu As Container
    oParentObject=.Null.
    BackStyle=0
    BorderWidth = 0
    nPageCount = 0

    OffSet = 25  &&Offset pagine

    Procedure Init(oParentObject, bCmdBar, bChild, cPgFrm, bPage1)
        With This
            Local l_oPgfrm, l_cPgFrm
            .oParentObject = m.oParentObject
            If Vartype(m.cPgFrm) = 'C'
                l_cPgFrm = "This.oParentObject."+m.cPgFrm
            Else
                l_cPgFrm = "This.oParentObject.oPgFrm"
            Endif
            l_oPgfrm = &l_cPgFrm
            .nPageCount = m.l_oPgfrm.PageCount
            If m.l_oPgfrm.PageCount>1 Or m.bPage1
                .AddObject("oTabMenu", "_TabMenu", bChild, m.cPgFrm)
                If bChild
                    .Move(1, 1, .oTabmenu.Width, .oTabmenu.Height)
                Else
                    .Move(0, 0, .oTabmenu.Width, .oTabmenu.Height)
                Endif
                .Anchor = 11
                .oTabmenu.Visible=.T.
                .Visible=.T.
            Endif
        Endwith
    Endproc
    Procedure Destroy()
        This.oParentObject = .Null.
    Endproc
Enddefine

*--- TabMenu Tematizzati
Define Class _TabMenu As Container

    #If Version(5)>=900
        Add Object aMenuItem As Collection 	&&Collezione di MenuItem
    #Else
        Add Object aMenuItem As cp_collection 	&&Collezione di MenuItem
    #Endif

    Height = 26
    Width = 0
    Top = 0
    Left = 0
    BorderWidth = 0
    Picture = ""			&&Immagine di sfondo

    Selected = 0
    nPageCount = 0
    oPgFrm = .Null.
    oParentObject = .Null.

    bSeparator = .T.	&&Indica se � presente il separatore tra i tab

    Add Object Line_T As Line With ;
        Top = 0, ;
        Left = 0, ;
        Width = 0, ;
        Visible = .T., ;
        Height = 0

    Add Object Line_B As Line With ;
        Top = 25, ;
        Left = 0, ;
        Width = 0, ;
        Visible = .T., ;
        Height = 0

    Procedure Init(bChild, cPgFrm)
        Local l_i
        Local l_oPgfrm, l_cPgFrm
        If Vartype(m.cPgFrm) = 'C'
            l_cPgFrm = "This.Parent.oParentObject."+m.cPgFrm
        Else
            l_cPgFrm = "This.Parent.oParentObject.oPgFrm"
        Endif
        With This
            .oPgFrm = &l_cPgFrm
            .oParentObject = .Parent.oParentObject
            .Width = .oParentObject.Width
            .oPgFrm.Top = .oPgFrm.Top + .Height &&Abbasso il page frame
            If m.bChild
                .oParentObject.BorderWidth = 1
                .oParentObject.BorderColor = i_ThemesManager.GetProp(55)
                .Width = .oParentObject.Width - 2
                .Left = 1
            Endif
            .Line_T.Width = .Width
            .Line_T.Anchor = 11
            .Line_B.Width = .Width
            .Line_B.Anchor = 11
            .Anchor = 11
            .bSeparator = i_ThemesManager.GetProp(110)=-1
            For l_i = 1 To .oPgFrm.PageCount
                .AddMenuItem(l_i, .oPgFrm.Pages[l_i].Caption)
            Next
            *--- Se gradiente abbasso il tutto
            If i_bGradientBck And Type('this.oParentObject.ImgBackGround')='O'
                .oParentObject.ImgBackGround.Anchor = 0
                .oParentObject.ImgBackGround.Top = This.Height
                .oParentObject.ImgBackGround.Height=.oParentObject.ImgBackGround.Height - This.Height
                .oParentObject.ImgBackGround.Anchor = 15
            Endif
            .nPageCount = .oPgFrm.PageCount
            .OnVisualThemeChanged()
            .SelectMenuItem(1, .T.)
            *Catturo l'evento cambio tema del ThemeManager
            i_ThemesManager.AddInstance(This, "OnVisualThemeChanged")
        Endwith
    Endproc

    Procedure Destroy
        *Rilascio la verifica del messaggio di cambio tema
        If Type('i_ThemesManager')='O'
            i_ThemesManager.RemoveInstance(This,"OnVisualThemeChanged")
        Endif
        This.oPgFrm = .Null.
        This.oParentObject = .Null.
    Endproc

    Procedure AddMenuItem(pNum, cCaption)
        With This
            Local l_MenuItem, lnLeft, l_PrevMenuItem, lnOffset
            l_MenuItem = "MenuItem" + Strtran(Str(m.pNum,3,0), " ", "0")
            .AddObject(l_MenuItem, "_TabMenuItem", m.cCaption)
            .aMenuItem.Add(This.&l_MenuItem., Str(m.pNum))
            If This.bSeparator
                lnLeft = 5
                lnOffset=0
            Else
                lnLeft = 0
                lnOffset=1
            Endif
            If .aMenuItem.Count > 1
                l_PrevMenuItem = This.aMenuItem[.aMenuItem.Count-1]
                lnLeft = l_PrevMenuItem.Left + l_PrevMenuItem.Width+lnOffset
            Endif
            .oPgFrm.Pages[m.pNum].Tag = "MenuItem"+Strtran(Str(m.pNum,3,0), " ", "0")
            Bindevent(.oPgFrm.Pages[m.pNum],"Caption", This, "OnCaption",1)
            Bindevent(.oPgFrm.Pages[m.pNum],"Enabled", This, "OnEnabled",1)
            Bindevent(.oPgFrm.Pages[m.pNum],"SetFocus", This, "OnSetFocus",1)
            Bindevent(.oPgFrm.Pages[m.pNum],"Tooltiptext", This, "OnToolTipText",1)
            Bindevent(.oPgFrm,"ActivePage", This, "OnActivatePage",1)
            Bindevent(.oPgFrm,"PageCount", This, "OnPageCount",1)
        Endwith
        With This.&l_MenuItem.
            .Top = 0
            .Left = lnLeft
            .ItemIndex = This.aMenuItem.Count
            .Visible=.T.
            .Enabled = This.oPgFrm.Pages[m.pNum].Enabled
        Endwith
    Endproc

    Procedure OnActivatePage
        Local Array aEv[1]
        Local oPage, l_MenuItem, l_cMenuItem

        Aevents(aEv,0)
        m.oPage = aEv[1]
        l_cMenuItem = "This."+m.oPage.Pages[m.oPage.ActivePage].Tag
        l_MenuItem = &l_cMenuItem
        l_MenuItem.CmdMenuItem.SetFocus()
        This.SelectMenuItem(l_MenuItem.ItemIndex, .T.)
    Endproc

    Procedure GotFocus
        Local l_cMenuItem,l_MenuItem
        l_cMenuItem = "This."+This.oPgFrm.Pages[this.selected].Tag
        l_MenuItem = &l_cMenuItem
        l_MenuItem.CmdMenuItem.SetFocus()
    Endproc

    Procedure OnSetFocus
        Local Array aEv[1]

        Aevents(aEv,0)
        m.oPage = aEv[1]
        l_cMenuItem = "This."+m.oPage.Tag
        l_MenuItem = &l_cMenuItem
        This.SelectMenuItem(l_MenuItem.ItemIndex, .F.)
    Endproc

    Procedure OnToolTipText
        Local Array aEv[1]
        Local oPage, l_MenuItem, l_cMenuItem

        Aevents(aEv,0)
        m.oPage = aEv[1]
        l_cMenuItem = "This."+oPage.Tag
        l_MenuItem = &l_cMenuItem
        l_MenuItem.ItemCaption.ToolTipText = m.oPage.ToolTipText
    Endproc

    Procedure OnCaption
        Local Array aEv[1]
        Local oPage, l_MenuItem, l_cMenuItem

        Aevents(aEv,0)
        m.oPage = aEv[1]
        l_cMenuItem = "This."+oPage.Tag
        l_MenuItem = &l_cMenuItem
        l_MenuItem.Caption = m.oPage.Caption
    Endproc

    Procedure OnEnabled
        Local Array aEv[1]
        Local oPage, l_MenuItem, l_cMenuItem

        Aevents(aEv,0)
        m.oPage = aEv[1]
        l_cMenuItem = "This."+oPage.Tag
        l_MenuItem = &l_cMenuItem
        l_MenuItem.Enabled = m.oPage.Enabled
    Endproc

    Procedure OnPageCount
        Local l_bAdd, l_i, l_MenuItem, l_Start, l_End
        With This
            If Type("This.oPgFrm.Pagecount")<>"U"
                If .oPgFrm.PageCount > .nPageCount
                    l_Start = .nPageCount + 1
                    l_End = .oPgFrm.PageCount
                    l_bAdd = .T.
                Else
                    l_Start = .oPgFrm.PageCount + 1
                    l_End = .nPageCount
                    l_bAdd = .F.
                Endif
                For l_i = l_Start To l_End
                    l_MenuItem = "MenuItem" + Strtran(Str(m.l_i,3,0), " ", "0")
                    If l_bAdd
                        .AddMenuItem(l_i, "Page"+Strtran(Str(m.l_i,3,0), " ", "0"))
                    Else
                        .RemoveObject(l_MenuItem)
                    Endif
                Next
                .nPageCount = .oPgFrm.PageCount
                .OnVisualThemeChanged()
            Endif
        Endwith
    Endproc

    Procedure SetUpItems
        Local oTab, lnLeft, l_i, lnOffset
        If This.bSeparator
            lnLeft = 5
            lnOffset = 0
        Else
            lnLeft=-1
            lnOffset=1
        Endif
        For l_i=1 To This.aMenuItem.Count
            oTab = This.aMenuItem.Item(m.l_i)
            oTab.Left = lnLeft
            lnLeft = lnLeft +Iif(oTab.Visible, oTab.Width + lnOffset, 0)
            oTab.Refresh()
        Next
        oTab = .Null.
    Endproc
    Procedure BeginVisualThemeChange
        Lparameters obj
        With This
            .Visible = .F.
            .Picture = ""
            Local l_TabItem, l_i
            For l_i=1 To .aMenuItem.Count
                l_TabItem = .aMenuItem.Item(m.l_i)
                l_TabItem.BeginVisualThemeChange()
            Next
            l_TabItem = .Null.
        Endwith
    Endproc

    Procedure OnVisualThemeChanged
        With This
            .Visible = .F.
            .Picture = ""
            .Picture = i_ThemesManager.GetProp(119)

            .bSeparator = i_ThemesManager.GetProp(110)=-1
            .Line_T.BorderColor = i_ThemesManager.GetProp(7)

            .Line_B.BorderColor = i_ThemesManager.GetProp(111)
            Local l_TabItem, l_i
            For l_i=1 To .aMenuItem.Count
                l_TabItem = .aMenuItem.Item(m.l_i)
                l_TabItem.OnVisualThemeChanged()
            Next
            .Visible = .T.
        Endwith
    Endproc

    Procedure HighlightMenuItem(numItem)
        For Each l_TabItem In This.aMenuItem
            If l_TabItem.Enabled
                If l_TabItem.ItemIndex<>m.numItem
                    l_TabItem.ItemHighlight.Visible=.F.
                    If This.Selected == l_TabItem.ItemIndex
			l_nForeColor = IIF(l_TabItem.ItemCaption.nForeColor==-1, i_ThemesManager.GetProp(112), l_TabItem.ItemCaption.nForeColor)
                        l_TabItem.ItemCaption.ForeColor = m.l_nForeColor                    
                    Else
			l_nForeColor = IIF(l_TabItem.ItemCaption.nForeColor==-1, i_ThemesManager.GetProp(113), l_TabItem.ItemCaption.nForeColor)
                        l_TabItem.ItemCaption.ForeColor = m.l_nForeColor
                    Endif
                Else
                    l_nForeColor = IIF(l_TabItem.ItemCaption.nForeColor==-1, i_ThemesManager.GetProp(114), l_TabItem.ItemCaption.nForeColor)
                    l_TabItem.ItemCaption.ForeColor = m.l_nForeColor
                Endif
            Endif
        Next
    Endproc

    Procedure SelectMenuItem(numItem, bNoActivate, nNoForce)
        Local l_i, l_TabItem, l_SelTabItem, bNext, l_nForeColor
        numItem = Mod(m.numItem-1, This.aMenuItem.Count)+1
        If m.numItem <> This.Selected
            If Vartype(m.nNoForce)<>'L'
                *--- Controllo se la pagina � visibile altrimenti cerco la prima visibile
                bNext = .T.
                l_i=1
                Do While bNext
                    l_i = Mod(m.l_i-1, This.aMenuItem.Count)+1
                    l_TabItem = This.aMenuItem.Item(m.l_i)
                    If l_TabItem.ItemIndex == m.numItem
                        If !l_TabItem.Visible Or !l_TabItem.Enabled
                            bNext = .T.
                            m.numItem = m.numItem + m.nNoForce
                            numItem = Mod(m.numItem-1, This.aMenuItem.Count)+1
                        Else
                            bNext = .F.
                        Endif
                    Else
                        bNext = .T.
                    Endif
                    l_i = l_i + m.nNoForce
                Enddo
            Endif
            For l_i=1 To This.aMenuItem.Count
                l_TabItem = This.aMenuItem.Item(m.l_i)
                If l_TabItem.ItemIndex <> m.numItem
                    l_TabItem.ItemSelected.Visible = .F.
                    l_TabItem.ItemHighlight.Visible=.F.
                    If i_ThemesManager.GetProp(100)>0
                        If l_TabItem.Enabled
                            l_nForeColor = IIF(l_TabItem.ItemCaption.nForeColor==-1, i_ThemesManager.GetProp(113), l_TabItem.ItemCaption.nForeColor)
                            l_TabItem.ItemCaption.ForeColor = m.l_nForeColor
                        Else
			    l_nForeColor = IIF(l_TabItem.ItemCaption.nForeColor==-1, i_ThemesManager.GetProp(115), l_TabItem.ItemCaption.nForeColor)
                            l_TabItem.ItemCaption.ForeColor = m.l_nForeColor
                        Endif
                    Endif
                Else
                    l_TabItem.ItemSelected.Visible = .T.
                    l_TabItem.ItemHighlight.Visible=.F.
                    l_SelTabItem = l_TabItem
                    If i_ThemesManager.GetProp(100)>0
                        l_TabItem.ItemCaption.ForeColor=i_ThemesManager.GetProp(112)
                    Endif
                Endif
            Next
            This.Selected = m.numItem
            If !bNoActivate
                This.oParentObject.__dummy__.Enabled=.T.
                This.oParentObject.__dummy__.SetFocus()
                This.oPgFrm.ActivePage = m.numItem
                This.oPgFrm.Click()
                This.oParentObject.__dummy__.Enabled=.F.
            Endif
            l_TabItem = .Null.
            *--- Controllo se il tab � visibile
            If This.Selected <> 1
                If This.Left + l_SelTabItem.Left <= This.Parent.OffSet
                    *--- Sposto avanti
                    This.Left = This.Left + Abs(This.Left + l_SelTabItem.Left) + This.Parent.OffSet
                Endif
                If This.Left + l_SelTabItem.Left + l_SelTabItem.Width >= This.Parent.Width - This.Parent.OffSet
                    *--- Sposto indietro
                    This.Left = ((This.Parent.Width - This.Parent.OffSet) - (l_SelTabItem.Left + l_SelTabItem.Width))
                Endif
            Else
                *--- Ripristino
                This.Left = 0
            Endif
            If !bNoActivate
                l_SelTabItem.CmdMenuItem.Enabled=.T.
                l_SelTabItem.CmdMenuItem.SetFocus()
            Endif
            l_SelTabItem = .Null.
        Endif
    Endproc
Enddefine

Define Class _TabMenuItem As Container

    Width = 84
    Height = 26
    BackStyle = 0
    BorderWidth = 0
    Name = "_TabMenuItem"
    bSetFont=.T.
    Caption = ""
    ItemIndex = 0

    Add Object ItemSelected As _TabMenuItemSelected With ;
        Top = 0, ;
        Left = 2, ;
        Width = 74, ;
        Visible = .F., ;
        Name = "ItemSelected"

    Add Object ItemHighlight As _TabMenuItemHighlight With ;
        Top = 2, ;
        Left = 2, ;
        Width = 74, ;
        Height = 22, ;
        Picture = "", ;
        Visible = .F., ;
        BorderWidth = 1, ;
        Name = "ItemHighlight"

    Add Object CmdMenuItem As CommandButton With ;
        Top = 5, ;
        Left = 3, ;
        Height = 20, ;
        Width = 84, ;
        Caption = "", ;
        Style = 1, ;
        Enabled=.F., ;
        Name = "CmdMenuItem"

    Add Object ItemCaption As Label With ;
        AutoSize = .T., ;
        FontName = "Arial", ;
        FontSize = 9, ;
        Alignment = 0, ;
        BackStyle = 0, ;
        Height = 15, ;
        Left = 6, ;
        Top = 6, ;
        Width = 0, ;
        Name = "ItemCaption", ;
        nForeColor = -1

    Add Object ItemSeparator As _TabMenuItemSeparator

    Procedure Init
        Lparameters vcCaption
        If This.bSetFont
            This.SetFont()
        Endif
        This.Caption = vcCaption
        If !This.Parent.bSeparator
            This.ItemSelected.Left=0
            This.ItemHighlight.Left=0
            This.ItemHighlight.Top=0
            This.ItemHighlight.Height=26
        Endif
    Endproc

    Procedure SetFont()
        Local l_oldFontName
        l_oldFontName = This.ItemCaption.FontName
        This.ItemCaption.FontName = SetFontName('P', Thisform.FontName, Thisform.bGlobalFont)
        This.ItemCaption.FontSize = SetFontSize('P', Thisform.FontSize, Thisform.bGlobalFont)
        This.ItemCaption.FontItalic = SetFontItalic('P', Thisform.FontItalic, Thisform.bGlobalFont)
        This.ItemCaption.FontBold = SetFontBold('P', Thisform.FontBold, Thisform.bGlobalFont)
    Endproc

    Procedure Caption_assign
        Lparameters vNewVal
        With This
            If Empty(m.vNewVal)
                .Visible=.F.
            Else
                .Visible=.T.
                .Caption = m.vNewVal
                .ItemCaption.Caption = .Caption
                .CmdMenuItem.Caption = .Caption
                .ItemCaption.Visible = .T.
                If .Parent.bSeparator
                    .ItemCaption.Left = 8
                    .Width = .ItemCaption.Width + 14
                    .ItemHighlight.Width = .ItemCaption.Width + 8
                    .ItemSelected.Width = .ItemHighlight.Width
                    .ItemSeparator.Left = .ItemHighlight.Width + 4
                Else
                    .ItemCaption.Left = 8
                    .Width = .ItemCaption.Width + 10
                    .ItemHighlight.Width = .ItemCaption.Width + 10
                    .ItemSelected.Width = .ItemHighlight.Width
                    .ItemSeparator.Visible=.F.
                Endif
            Endif
            .Parent.SetUpItems()
        Endwith
    Endproc

    Procedure Enabled_assign
        Lparameters vNewVal
        With This
            .Enabled = m.vNewVal
            If i_ThemesManager.GetProp(100)=1
                .ItemCaption.Enabled=.T.
                If Not .Enabled
                     l_nForeColor = IIF(.ItemCaption.nForeColor==-1, i_ThemesManager.GetProp(115), .ItemCaption.nForeColor)
                    .ItemCaption.ForeColor= m.l_nForeColor
                ELSE
                	IF .ItemIndex <> .Parent.Selected
			            l_nForeColor = IIF(.ItemCaption.nForeColor==-1, i_ThemesManager.GetProp(113), .ItemCaption.nForeColor)
    	                .ItemCaption.ForeColor= m.l_nForeColor 
    	            ENDIF
                Endif
            Else
                .ItemCaption.Enabled = m.vNewVal
            Endif
        Endwith
    Endproc

    Procedure HotTrackOn
        With This
            If .Enabled And .ItemIndex <> .Parent.Selected
                .Parent.HighlightMenuItem(.ItemIndex)
                .ItemHighlight.Visible = .T.
            Endif
        Endwith
    Endproc

    Procedure HotTrackOff
        With This
            .ItemHighlight.Visible = .F.
            If .Enabled And .ItemIndex <> .Parent.Selected
		l_nForeColor = IIF(.ItemCaption.nForeColor==-1, i_ThemesManager.GetProp(113), .ItemCaption.nForeColor)
                .ItemCaption.ForeColor = m.l_nForeColor
            Endif
        Endwith
    Endproc

    Procedure ItemCaption.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.HotTrackOn()
    Endproc

    Procedure ItemCaption.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.HotTrackOff()
    Endproc

    Procedure Click
        With This
            If .Enabled
                .Parent.SelectMenuItem(.ItemIndex)
                .ItemSelected.Visible = .T.
		l_nForeColor = IIF(.ItemCaption.nForeColor==-1, i_ThemesManager.GetProp(112), .ItemCaption.nForeColor)
                .ItemCaption.ForeColor = m.l_nForeColor
                This.SetFocus()
            Endif
        Endwith
    Endproc

    Procedure ItemCaption.Click
        #If Version(5)>=900
            Raiseevent(This.Parent,"Click")
        #Endif
    Endproc

    Procedure BeginVisualThemeChange
        With This
            .ItemSelected.BeginVisualThemeChange()
            .ItemHighlight.BeginVisualThemeChange()
        Endwith
    Endproc

    Procedure OnVisualThemeChanged
        With This
            nBackColor = i_ThemesManager.GetProp(116)
            If nBackColor<>-1
                .BackStyle = 1
                .BackColor = nBackColor
            Endif

            .ItemSelected.OnVisualThemeChanged()
            .ItemHighlight.OnVisualThemeChanged()
            .ItemSeparator.OnVisualThemeChanged()
        Endwith
    Endproc

    Procedure Resize
        With This
            If .Parent.bSeparator
                .CmdMenuItem.Width = This.Width - 8
            Else
                .CmdMenuItem.Width = This.Width - 4
            Endif
            .ItemSelected.Resize()
            .ItemHighlight.Resize()
            .ItemSeparator.Resize()
        Endwith
    Endproc

    Procedure CmdMenuItem.Click
        Raiseevent(This.Parent,"Click")
    Endproc

    Procedure CmdMenuItem.KeyPress(nKeyCode, nShiftAltCtrl)
        Do Case
            Case nKeyCode = 19 &&Left
                This.Enabled=.F.
                This.Parent.Parent.SelectMenuItem(This.Parent.ItemIndex-1, .F., -1)
                Nodefault
            Case nKeyCode = 4 &&Right
                This.Enabled=.F.
                This.Parent.Parent.SelectMenuItem(This.Parent.ItemIndex+1, .F., 1)
                Nodefault
            Case nKeyCode = 24 &&Down
        Endcase
    Endproc

Enddefine
*
*-- EndDefine: tabmenuitem
**************************************************

Define Class _TabMenuItemHighlight As Container
    Height = 22
    BackStyle = 0
    BorderWidth = 1

    Add Object Background As Container With ;
        Top = 1 , ;
        Left = 1, ;
        BackStyle = 1, ;
        BorderWidth = 0

    Procedure BeginVisualThemeChange
        With This
            .Background.Picture = ""
        Endwith
    Endproc

    Procedure OnVisualThemeChanged
        With This
            .Background.Picture = ""
            nColor = i_ThemesManager.GetProp(117)
            If nColor<>-1
                .Background.BackStyle = 0
                .BorderColor = m.nColor
                .BackColor = m.nColor
                .BackStyle = 1
            Else
                .Background.Picture = i_ThemesManager.GetProp(51)
                .BorderColor = i_ThemesManager.GetProp(62)
            Endif
        Endwith
    Endproc

    Procedure Resize
        This.Background.Move(1, 1, This.Width - 2, This.Height - 2)
    Endproc
Enddefine

Define Class _TabMenuItemSelected As Container

    Height = 26
    BackStyle = 0
    BorderWidth = 0

    Add Object Background As Container With ;
        Top = 2, ;
        Left = 3, ;
        Width = 68, ;
        Height = 24, ;
        BorderWidth = 0, ;
        Backstyle=1,;
        Picture = "", ;
        Name = "Background"


    Add Object ImgLeft As Image With ;
        Height = 26, ;
        Left = 0, ;
        Top = 0, ;
        Width = 2, ;
        Picture = "", ;
        Backstyle=0,;
        Name = "ImgLeft"


    Add Object ImgRight As Image With ;
        Height = 26, ;
        Left = 72, ;
        Top = 0, ;
        Width = 2, ;
        Picture = "", ;
        Backstyle=0,;
        Name = "ImgRight"

    Procedure Resize
        With This
            .Background.Move(2,2, .Width - 4)
            .ImgRight.Left = .Width - 2
        Endwith
    Endproc

    Procedure BeginVisualThemeChange
        Local l_OldPic
        With This
            .Background.Picture = ""
            .ImgLeft.Picture = ""
            .ImgRight.Picture = ""
        Endwith
    Endproc

    Procedure OnVisualThemeChanged
        Local l_OldPic
        With This
            .Background.Picture = ""
            nColor = i_ThemesManager.GetProp(117)
            If nColor<>-1
                .Background.BackStyle=0
                .ImgLeft.Visible = .F.
                .ImgRight.Visible = .F.
                .BackColor= m.nColor
                .BackStyle=1
                .BorderWidth = 1
                .BorderColor = m.nColor
            Else
                .Background.Picture = i_ThemesManager.TabMenuItemSel
                .ImgLeft.Picture = ""
                .ImgLeft.Picture = i_ThemesManager.TabMenuItemSelLeft
                .ImgRight.Picture = ""
                .ImgRight.Picture = i_ThemesManager.TabMenuItemSelRight
            Endif
        Endwith
    Endproc
Enddefine

Define Class _TabMenuItemSeparator As Container
    Top = 6
    Left = 0
    Width = 2
    Height = 15
    BackStyle = 0
    BorderWidth = 0
    Name = "_TabMenuItemSeparator"


    Add Object Line1 As Line With ;
        Height = 14, ;
        Left = 0, ;
        Top = 0, ;
        Width = 0, ;
        Name = "Line1"


    Add Object Line2 As Line With ;
        Height = 14, ;
        Left = 1, ;
        Top = 1, ;
        Width = 0, ;
        BorderColor = Rgb(255,255,255), ;
        Name = "Line2"

    Procedure OnVisualThemeChanged
        This.Visible=This.Parent.Parent.bSeparator And (i_ThemesManager.GetProp(100)=0)
        This.Line1.BorderColor = i_ThemesManager.GetProp(2)
    Endproc
Enddefine

Define Class cp_StatusBar As Custom
    hwndProxy = 0 && Handle della finestra che contiene la status bar
    hwndVFPStatusbar = -1 &&Handle della status bar di VFP
    oStatusBarForm = -1 &&Form che conterr� i panel, che sar� inserita al posto della status bar
    cOldStatusBar = '' &&Stato precedente statusbar
    _WindowProcedure = 0
    nWidth = 0
    nHeight = 0
    nOffset = 3
    *--- Panels
    Dimension aPanels[1]
    nCount = 0
    hTooltipText = 0
    oBrush = .Null.
    pPen = .Null.
    gfxHwnd =.Null.
    gfxZBuffer = .Null.
    loBitmapZBuffer = .Null.

    Add Object TimerRefresh As Timer With Interval=200, Enabled = .F.

    Procedure DeclareApi()
        #Define GW_CHILD 5
        #Define GWL_STYLE -16
        #Define SW_HIDE 0
        #Define WM_WINDOWPOSCHANGED	0x0047
        #Define GWL_WNDPROC -4
        #Define WM_DESTROY	0X2
        #Define WM_THEMECHANGED	0x031A
        #Define WS_VISIBLE	0x10000000
        #Define WS_EX_NOPARENTNOTIFY	0x4
        #Define WS_CLIPSIBLINGS	0x4000000
        #Define WS_CHILD		0x40000000
        #Define WS_CLIPCHILDREN	0x2000000
        #Define WM_PAINT	0xF
        *#Define WM_MOUSEMOVE	0x200
        #Define WM_LBUTTONDBLCLK	0x203
        #Define WM_NOTIFY	0x4E
        #Define WS_GROUP	0x20000
        #Define WM_SETCURSOR	0x20

    Endproc

    Procedure TimerRefresh.Timer()
        *--- Controllo esistenza della statusbar per possibile cambio tema
        If IsWindow(This.Parent.hwndVFPStatusbar)=0
            This.Parent.Initialize()
        Endif
        *--- Se il debug � attivo aumento l'intervallo del timer
        If Wexist("Debugger")
            This.Interval=120000
            Return
        Else
            This.Interval=200
        Endif
        This.Parent.RefreshStatusBar()
    Endproc

    Procedure Init
        This.DeclareApi()
        *--- E' necessario sia presente la status bar per poter catturare il testo
        This.cOldStatusBar = Set("Status Bar")
        If This.cOldStatusBar = 'OFF'
            Set Status Bar On
        Endif
        This.Initialize()
    Endproc

    Procedure Initialize()
        If This.oStatusBarForm<>-1
            Unbindevent(This.hwndVFPStatusbar, WM_WINDOWPOSCHANGED)
            DestroyWindow(This.oStatusBarForm)
            This.oStatusBarForm=-1
        Endif

        *--- Utilizzo un toolbar per individuare la finestra della status bar
        Local oToolBar
        oToolBar = Createobject("Toolbar")
        oToolBar.Dock(3)
        oToolBar.Show()

        This.hwndProxy = apiGetParent(oToolBar.HWnd)
        *--- La toolbar non � pi� necessaria
        oToolBar = .Null.
        *--- Questo trucco funziona solo se nn sono presenti altre toolbar con DOCK(3) TOOL_BOTTOM
        This.hwndVFPStatusbar = apiGetWindow(This.hwndProxy, GW_CHILD)
        *--- Posso nascondere la StatusBar
        apiShowWindow(This.hwndVFPStatusbar, SW_HIDE)
        *--- Aggiungo il FormContainer
        Local l_hwnd, lcClass, lnSize, dwExStyle, dwStyle
        l_hwnd = _Screen.HWnd&&GetActiveWindow()
        lcClass = Space(250)
        lnSize = RealGetWindowClass (l_hwnd, @lcClass , Len(lcClass))
        lcClass = Strtran (Substr(lcClass, 1,lnSize), Chr(0),"")

        dwExStyle = WS_EX_NOPARENTNOTIFY
        dwStyle   = WS_VISIBLE+WS_CLIPSIBLINGS+WS_CHILD+WS_CLIPCHILDREN+WS_GROUP
        *--- Creo la nuova statusbar, nuova finestra figlia di hwndProxy
        This.oStatusBarForm = CreateWindowEx(dwExStyle, lcClass, "cp_StatusBar"+Sys(2015),;
            dwStyle, 0, 0, 800, 30, This.hwndProxy, 0, 0, 0)
        If This.oStatusBarForm = 0
            cp_ErrorMsg("Impossibile creare cp_StatusBar: " + Transform(GetLastError()))
            This.TimerRefresh.Enabled = .F.
            This.TimerRefresh.Interval = 0
            Return
        Endif
        This._WindowProcedure = GetWindowLong(This.hwndVFPStatusbar, GWL_WNDPROC)
        Bindevent(This.hwndVFPStatusbar, WM_WINDOWPOSCHANGED, This, "_WndProc", 5)	&&Resize StatusBar
        Bindevent(This.oStatusBarForm, WM_SETCURSOR, This, "_WndProc", 5)	&&Imposto il mousepointer a IDC_ARROW quando sono sulla StatusBar

        *--- Resize
        This.Resize()
        Local l_bitMapBck
        With _Screen.System.Drawing
            l_bitMapBck = .Bitmap.FromFile(i_ThemesManager.GetProp(20))
            l_bitMapBck = l_bitMapBck.GetThumbnailImage(1, This.nHeight)
            This.oBrush = .TextureBrush.New(l_bitMapBck)
            This.pPen = .Pen.New(.Color.FromRgb(i_ThemesManager.GetProp(91)))
            This.gfxHwnd = .Graphics.FromHWnd(This.oStatusBarForm)
        Endwith
        This.TimerRefresh.Enabled = .T.
    Endproc

    Procedure Destroy
        This.TimerRefresh.Enabled = .F.
        This.TimerRefresh.Interval = 0
        Unbindevent(This.hwndVFPStatusbar, WM_WINDOWPOSCHANGED)
        DestroyWindow(This.oStatusBarForm)
        This.oStatusBarForm=.Null.
        Local l_cOldStatusBar
        l_cOldStatusBar = This.cOldStatusBar
        Set Status Bar On
        Set Status Bar &l_cOldStatusBar
    Endproc

    Procedure RefreshStatusBar
        With _Screen.System.Drawing
            Local ImgBck,loBitmap, gfxHwnd, l_bitMapBck
            If Vartype(This.gfxZBuffer)='O' And !Isnull(This.gfxZBuffer)
                This.gfxZBuffer.FillRectangle(This.oBrush, 0,0, This.nWidth, This.nHeight)
                This.gfxZBuffer.DrawLine(This.pPen, 0, 0, This.nWidth, 0)
                *--- Draw panels
                This.DrawPanels(This.gfxZBuffer)
            Endif
            *--- Copio il buffer sul dc della finestra
            If Vartype(This.gfxHwnd)='O' And !Isnull(This.gfxHwnd)
                This.gfxHwnd.DrawImage(This.loBitmapZBuffer, 0, 0)
            Endif
        Endwith
    Endproc

    Procedure DrawPanels(pGfx)
        Local oPanel, l_Left, l_i
        Local l_oPanel, l_width, l_CountAutosize
        l_Left = This.nOffset
        *--- Calcolo autosize
        l_width = 2*This.nOffset
        l_CountAutosize = 0
        For l_i = 1 To This.nCount
            If Vartype(This.aPanels[l_i]) = 'O' And This.aPanels[l_i].Visible
                If This.aPanels[l_i].bAutosize
                    l_CountAutosize = l_CountAutosize + 1
                Else
                    l_width = l_width + This.aPanels[l_i].Width + This.nOffset
                Endif
            Endif
        Next
        If l_CountAutosize<>0
            l_widthAvg = Int((This.nWidth - l_width) / l_CountAutosize)
            l_delta = Mod((This.nWidth - l_width), l_CountAutosize)
        Endif
        Local l_Panel_Left, l_Panel_Top, l_Panel_Right, l_Panel_Bottom
        l_Left = This.nOffset
        With _Screen.System.Drawing
            For l_i=1 To This.nCount
                oPanel = This.aPanels[l_i]
                If oPanel.Visible
                    Do Case
                        Case oPanel.nType = 0
                            oPanel.cText = _vfp.StatusBar
                        Case oPanel.nType = 1 &&NUM
                            oPanel.bActive = Numlock( )
                        Case oPanel.nType = 2	&&CAPS
                            oPanel.bActive = Capslock( )
                        Case oPanel.nType = 3	&&INS
                            oPanel.bActive = Insmode( )
                        Case oPanel.nType = 4	&&Date
                            oPanel.cText = Alltrim(Dtoc(i_datsys))
                    Endcase
                    oPanel.nLeft = l_Left
                    If oPanel.bShowIcon
                        If oPanel.bActive
                            If !Empty(oPanel.cBmpIco)
                                pGfx.DrawImage(oPanel.oBmpIco, l_Left, 5)
                            Endif
                        Else
                            If !Empty(oPanel.cBmpIcoDisable)
                                pGfx.DrawImage(oPanel.oBmpIcoDisable, l_Left, 5)
                            Endif
                        Endif
                        l_Left = l_Left + oPanel.nIcoWidth
                    Endif
                    If !Empty(oPanel.cText)
                        pGfx.DrawString(oPanel.cText, oPanel.oFont, .Brushes.Black, l_Left, 5)
                    Endif
                    If !Inlist(oPanel.nType,1,2,3)
                        If oPanel.bAutosize
                            l_Left = l_Left + l_widthAvg+l_delta
                            l_delta = 0	&&l_delta � aggiunto solo la prima volta
                        Else
                            l_Left = l_Left + Max(oPanel.Width, oPanel.nMinWidth)
                        Endif
                    Endif
                    oPanel.nRight = l_Left
                Else
                    *--- Non � visibile elimino il tooltip
                    cb_RemoveToolTip(This.oStatusBarForm, oPanel.hTooltipText)
                Endif
            Next
            oPanel = .Null.
        Endwith
    Endproc

    Procedure _WndProc
        Lparameters tnHWnd As Integer, tnMsg As Integer, tnwParam As Integer, tnLparam As Integer
        Do Case
            Case tnMsg = WM_WINDOWPOSCHANGED
                This.Resize()
            Case tnMsg = WM_SETCURSOR
                cb_SetCursor(0)
        Endcase
    Endproc

    Procedure Resize()
        Local sRect
        Local nLeft, nTop, nRight, nBottom, nWidth, nHeight
        #Define RECT_Size		16  && (DWORD_Size*4)

        sRect = Space(RECT_Size)

        *--- Dimensione StatusBar di VFP
        GetWindowRect(This.hwndVFPStatusbar, @sRect)
        nLeft = This.Buff2Num( sRect, 1, .T. )
        nTop = This.Buff2Num( sRect, 5, .T. )
        nRight = This.Buff2Num( sRect, 9, .T. )
        nBottom = This.Buff2Num( sRect, 13, .T. )
        This.nWidth = (nRight - nLeft)+1
        This.nHeight = (nBottom - nTop)

        *--- Posizione Proxy
        GetWindowRect(This.hwndProxy, @sRect)
        *--- Faccio la differenza per trovare le posizioni relative
        nLeft = Abs(This.Buff2Num( sRect, 1, .T. ) - nLeft )
        nTop = Abs(This.Buff2Num( sRect, 5, .T. ) - nTop )
        *--- Sposto il FormContainer
        SetWindowPos(This.oStatusBarForm, 1, nLeft, nTop, This.nWidth, This.nHeight, 0x40)
        *--- Buffer video
        This.loBitmapZBuffer = _Screen.System.Drawing.Bitmap.New(This.nWidth, This.nHeight)
        This.gfxZBuffer = _Screen.System.Drawing.Graphics.FromImage(This.loBitmapZBuffer)
        This.gfxHwnd = _Screen.System.Drawing.Graphics.FromHWnd(This.oStatusBarForm)
    Endproc

    Procedure Buff2Num
        Lparameters tc_Buffer, tn_Pos, tl_Signed
        Local ln_Result
        If tl_Signed
            ln_Result = CToBin( Substr( tc_Buffer, tn_Pos, 4 ), '4rs' )
        Else
            ln_Result = Asc(Substr( tc_Buffer, tn_Pos+0, 1 )) + ;
                asc(Substr( tc_Buffer, tn_Pos+1, 1 )) * 0x100 + ;
                asc(Substr( tc_Buffer, tn_Pos+2, 1 )) * 0x10000 + ;
                asc(Substr( tc_Buffer, tn_Pos+3, 1 )) * 0x1000000
        Endif
        Return ln_Result
    Endproc

    *--- Aggiunta nuovo panel
    Function AddPanel(pType, pMinWidth, pAutoSize, pName, pPosition)
        This.nCount = This.nCount + 1
        Dimension This.aPanels[This.nCount]
        Local l_PanelName, l_oPanel, l_Position
        l_PanelName = "oPanel"+Right("00"+Alltrim(Str(This.nCount)), 2)
        If Vartype(m.pName)='C' And !Empty(m.pName)
            l_PanelName = m.pName
        Endif
        Local l_err
        l_err = .F.
        l_oldErr = On("error")
        On Error l_err = .T.
        This.AddObject(l_PanelName, "cp_StatusBarPanel", Iif(Type("pType") = 'N', m.pType, 0))
        On Error &l_oldErr
        If l_err
            Return .Null.
        Endif
        l_oPanel = This.&l_PanelName
        This.aPanels[This.nCount] = l_oPanel
        l_Position = Iif(Vartype(pPosition)='N',Min(pPosition, This.nCount), This.nCount)
        l_oPanel.ChangePos(l_Position)
        l_oPanel.nMinWidth = Iif(Type("pMinWidth")='N', m.pMinWidth, 16)
        l_oPanel.bAutosize = m.pAutoSize
        l_oPanel.cFontName=SetFontName("L", l_oPanel.cFontName, .T.)
        l_oPanel.nFontSize=SetFontSize("L", l_oPanel.nFontSize, .T.)
        l_oPanel.uid = This.nCount
        Return l_oPanel
    Endfunc

    Function RemovePanel(nNum)
        If m.nNum<=This.nCount And m.nNum>0
            Local l_name
            l_name = This.aPanels[m.nNum].Name
            If This.aPanels[m.nNum].hTooltipText<>0
                DestroyWindow(This.aPanels[m.nNum].hTooltipText)
            Endif
            Adel(This.aPanels, m.nNum)
            This.RemoveObject(l_name)
            This.nCount = Max(This.nCount - 1,0)
            Dimension This.aPanels(This.nCount)
            This.RefreshStatusBar()
        Endif
    Endfunc

Enddefine

Define Class cp_StatusBarPanel As Custom
    cBmpIcoDisable = ""
    cBmpIco = ""
    cText = ''
    nType = 0 &&0 Text statusbar, 1 Num, 2 Caps, 3 Ins, 4 i_Datsys, 5 Endless loop, 6 Text
    bActive = .T.
    bAutosize = .F.
    cTooltiptext = ""
    nMinWidth = 16
    Width = 16
    bShowIcon = .F.
    cCommand = ""
    bSeparator = .F.
    cFontName = i_cProjectFontName
    nFontSize = i_nProjectFontSize
    oBmpIco = .Null.
    oBmpIcoDisable = .Null.
    nIcoWidth = 0
    Visible = .T.
    nLeft=0
    nTop=0
    nRight=0
    nBottom=30
    uid = 0
    oFont = .Null.

    Procedure Init(nType)
        Do Case
            Case m.nType = 0	&&StatusBar Text
            Case m.nType = 1	&&NUM
                This.bActive = Numlock( )
                This.cBmpIco = "light.bmp"
                This.bShowIcon = .T.
                This.Width = 16
                This.cTooltiptext = "NUM"
            Case m.nType = 2	&&CAPS
                This.bActive = Capslock( )
                This.cBmpIco = "light.bmp"
                This.bShowIcon = .T.
                This.Width = 16
                This.cTooltiptext = "CAPS"
            Case m.nType = 3	&&INS
                This.bActive = Insmode( )
                This.cBmpIco = "light.bmp"
                This.bShowIcon = .T.
                This.Width = 16
                This.cTooltiptext = "OVR"
            Case m.nType = 4	&&Date
                This.cBmpIco = "calendar.bmp"
                This.bShowIcon = .T.
                This.cText = Alltrim(Dtoc(i_datsys))
        Endcase
        This.oFont= _Screen.System.Drawing.Font.New(This.cFontName, This.nFontSize)
        This.nType = m.nType
    Endproc

    Procedure nLeft_Assign(nValue)
        If This.nLeft <> m.nValue
            This.nLeft = m.nValue
            If !Empty(This.cTooltiptext)
                cb_RemoveToolTip(This.Parent.oStatusBarForm, This.Parent.hTooltipText, This.uid)
                This.Parent.hTooltipText = cb_SetToolTip(This.Parent.oStatusBarForm, This.cTooltiptext, This.nLeft,This.nTop, This.nRight, This.nBottom, This.Parent.hTooltipText, This.uid)
            Endif
        Endif
    Endproc

    Procedure nTop_Assign(nValue)
        If This.nTop <> m.nValue
            This.nTop = m.nValue
            If !Empty(This.cTooltiptext)
                cb_RemoveToolTip(This.Parent.oStatusBarForm, This.Parent.hTooltipText, This.uid)
                This.Parent.hTooltipText = cb_SetToolTip(This.Parent.oStatusBarForm, This.cTooltiptext, This.nLeft,This.nTop, This.nRight, This.nBottom, This.Parent.hTooltipText, This.uid)
            Endif
        Endif
    Endproc

    Procedure nRight_Assign(nValue)
        If This.nRight <> m.nValue
            This.nRight = m.nValue
            If !Empty(This.cTooltiptext)
                cb_RemoveToolTip(This.Parent.oStatusBarForm, This.Parent.hTooltipText, This.uid)
                This.Parent.hTooltipText = cb_SetToolTip(This.Parent.oStatusBarForm, This.cTooltiptext, This.nLeft,This.nTop, This.nRight, This.nBottom, This.Parent.hTooltipText, This.uid)
            Endif
        Endif
    Endproc

    Procedure nBottom_Assign(nValue)
        If This.nBottom <> m.nValue
            This.nBottom = m.nValue
            If !Empty(This.cTooltiptext)
                cb_RemoveToolTip(This.Parent.oStatusBarForm, This.Parent.hTooltipText, This.uid)
                This.Parent.hTooltipText = cb_SetToolTip(This.Parent.oStatusBarForm, This.cTooltiptext, This.nLeft,This.nTop, This.nRight, This.nBottom, This.Parent.hTooltipText, This.uid)
            Endif
        Endif
    Endproc

    Procedure cTooltiptext_Assign(cValue)
        This.cTooltiptext = Alltrim(cValue)
        If !Empty(This.cTooltiptext)
            cb_UpdateToolTip(This.Parent.oStatusBarForm, This.cTooltiptext, This.Parent.hTooltipText, This.uid)
        Else
            cb_RemoveToolTip(This.Parent.oStatusBarForm, This.Parent.hTooltipText, This.uid)
        Endif
    Endproc

    Procedure cBmpIco_Assign(cValue)
        With _Screen.System.Drawing
            This.cBmpIco = Forceext(i_ThemesManager.RetBmpFromIco(m.cValue, 16), 'bmp')
            This.oBmpIco = .Bitmap.FromFile(This.cBmpIco)
            This.oBmpIco.MakeTransparent(.Color.White)
            This.cBmpIcoDisable = Forceext(i_ThemesManager.GrayScaleBmp(Forceext(This.cBmpIco, ''), 16), 'bmp')
            This.oBmpIcoDisable = .Bitmap.FromFile(This.cBmpIcoDisable)
            This.oBmpIcoDisable.MakeTransparent(.Color.White)
            This.nIcoWidth = This.oBmpIco.Width
        Endwith
    Endproc

    Procedure DblClick()
        Execscript(This.cCommand)

    Procedure cText_Assign(cValue)
        Local new_cText
        new_cText = Evl(Nvl(m.cValue,''),' ')
        If This.cText <> m.new_cText
            This.cText = m.new_cText
            This.Width = This.MeasureString(This.cText) + Iif(This.bShowIcon And !Isnull(This.oBmpIco), 19, 0)
        Endif
    Endproc

    Function MeasureString(cValue)
        Local l_gfx, sizeF
        With _Screen.System.Drawing
            If Vartype(This.Parent.gfxHwnd)='O' And !Isnull(This.Parent.gfxHwnd)
                sizeF = This.Parent.gfxHwnd.MeasureString(m.cValue, This.oFont)
                Return Int(m.sizeF.Width)
            Endif
            Return 0
        Endwith
    Endfunc

    Procedure ChangePos(pPos)
        Local l_i, l_elem, l_OldPos
        With This.Parent
            If m.pPos<=.nCount And m.pPos>0
                For l_i = 1 To .nCount
                    If .aPanels[l_i] == This
                        l_OldPos = l_i
                        Exit
                    Endif
                Next
                If l_OldPos <> pPos
                    If l_OldPos > pPos
                        *--- Sposto avanti gli altri controlli
                        For l_i = l_OldPos-1 To pPos Step -1
                            .aPanels[l_i+1]	= .aPanels[l_i]
                        Next
                    Else
                        *--- Sposto indietro gli altri controlli
                        For l_i = l_OldPos To pPos
                            .aPanels[l_i]	= .aPanels[l_i+1]
                        Next
                    Endif
                    .aPanels[pPos] = This
                Endif
            Endif
        Endwith
    Endproc
Enddefine

*--- cp_WaitWindow
Define Class cp_WaitWindow As Custom
    WWhWnd = 0	&&Handle wait window
    WWDefWndProc = 0	&&WndProc WaitWindow
    bStopBindCreate = .T.
    nWidth = 0
    nHeight =0
    rectClip = .Null.
    oGradBrush = .Null.
    nLeft = 0
    nTop = 0
    nRight = 0
    nBottom = 0
    TopColor = .Null.
    BottomColor = .Null.
    BorderColor = .Null.
    BorderColor2 = .Null.
    bOldLockScreen = .F.

    Procedure Init()
        This.DeclareApi()
        With _Screen.System.Drawing
            This.TopColor = .Color.FromRgb(i_ThemesManager.GetProp(92))
            This.BottomColor = .Color.FromRgb(i_ThemesManager.GetProp(93))
            This.BorderColor = .Color.FromRgb(i_ThemesManager.GetProp(94))
            This.BorderColor2 = .Color.FromRgb(i_ThemesManager.GetProp(95))
        Endwith
    Endproc

    Procedure DeclareApi()

        If !Pemstatus(_Screen, "System", 5)
            Do System.App
        Endif

        #Define WM_CREATE	0X1
        #Define WM_ERASEBKGND	0x14
        #Define GWL_EXSTYLE	-20
        #Define GWL_STYLE	-16
        #Define WS_POPUP	0x80000000
        #Define WIN_2K_XP  (Os(3) >= '5')
        #Define WS_BORDER	0x800000
    Endproc

    Procedure Initialize(pText)
        If This.WWhWnd=0 Or IsWindow(This.WWhWnd)==0
            Bindevent(0, WM_CREATE, This, "_WndProc")
        Endif
        This.bOldLockScreen = _Screen.LockScreen
        _Screen.LockScreen = .F.
    Endproc

    Procedure _WndProc
        Lparameters th_Wnd, tn_Msg, t_wParam, t_lParam
        Do Case
            Case tn_Msg==WM_CREATE
                Local ln_Style, ll_Found, lc_ClassName, lc_ClassNameWW, ln_Len
                ll_Found = .F.
                lc_ClassName = Space(32)
                ln_Len = GetClassName(_vfp.HWnd, @lc_ClassName, 32)
                If WIN_2K_XP
                    lc_ClassName = Left(m.lc_ClassName, ln_Len) + '3'
                Else
                    lc_ClassName = Left(m.lc_ClassName, ln_Len) + '2'
                Endif
                lc_ClassNameWW = Space(32)
                ln_Len = GetClassName(m.th_Wnd, @lc_ClassNameWW, 32)
                lc_ClassNameWW = Left(m.lc_ClassNameWW, ln_Len)
                ln_Style = GetWindowLong( m.th_Wnd, GWL_STYLE)

                ll_Found = (Bitand( m.ln_Style, WS_POPUP ) != 0) And Upper(m.lc_ClassName)==Upper(m.lc_ClassNameWW)
                If m.ll_Found
                    If This.bStopBindCreate
                        Unbindevents(0, WM_CREATE)
                    Endif
                    m.ln_Style = Bitand(m.ln_Style, Bitnot(WS_BORDER))
                    SetWindowLong( m.th_Wnd, GWL_STYLE, m.ln_Style)
                    *--- Handle della WaitWindow std vfp
                    This.WWhWnd = th_Wnd
                    This.WWDefWndProc = GetWindowLong(This.WWhWnd, GWL_WNDPROC)
                    *--- BindEvent WaitWindow Std
                    This.BindEvents()
                Endif
                Return 0
            Case tn_Msg==WM_DESTROY
                Local l_ret
                This._UnBindEvents()
                This.WWhWnd=0
                l_ret = CallWindowProc(This.WWDefWndProc, m.th_Wnd, m.tn_Msg, m.t_wParam, m.t_lParam)
                This.WWDefWndProc = 0
                Return l_ret
            Case tn_Msg==WM_PAINT
                l_ret = CallWindowProc(This.WWDefWndProc, m.th_Wnd, m.tn_Msg, m.t_wParam, m.t_lParam)
                This.DrawWaitWind()
                Return l_ret
            Case tn_Msg==WM_ERASEBKGND
                l_ret = CallWindowProc(This.WWDefWndProc, m.th_Wnd, m.tn_Msg, m.t_wParam, m.t_lParam)
                This.DrawWaitWind()
                Return l_ret
        Endcase
    Endproc

    Procedure DrawWaitWind()
        With _Screen.System.Drawing
            Local sRect
            Local nLeft, nTop, nRight, nBottom, l_reg, l_rectClip
            #Define RECT_Size	16  && (DWORD_Size*4)
            If 	This.nWidth=0
                sRect = Space(RECT_Size)
                *--- Dimensione WaitWindow di VFP
                GetWindowRect(This.WWhWnd, @sRect)
                nLeft = This.Buff2Num( m.sRect, 1, .T. )
                nTop = This.Buff2Num( m.sRect, 5, .T. )
                nRight = This.Buff2Num( m.sRect, 9, .T. )
                nBottom = This.Buff2Num( m.sRect, 13, .T. )
                This.nWidth = (m.nRight - m.nLeft)
                This.nHeight = (m.nBottom - m.nTop)
                l_reg=CreateRoundRectRgn(4, 7, This.nWidth-4, This.nHeight-6,2,2)
                SetWindowRgn(This.WWhWnd, l_reg, 1)
                This.rectClip = .Rectangle.New(4, 7, This.nWidth-10, This.nHeight-15)
                This.oGradBrush = .Drawing2D.LinearGradientBrush.New(This.rectClip, ;
                    This.TopColor, ;
                    This.BottomColor, ;
                    .Drawing2D.LineargradientMode.Vertical)
                This.nLeft = This.rectClip.x + 1
                This.nTop = This.rectClip.Y + 1
                This.nRight = This.rectClip.Width + 3
                This.nBottom = This.rectClip.Height + 6
            Endif
            Local gfx, l_bitmap, gfxBmpWnd, oBrush, BmpHwnd, gfxBmp
            l_bitmap=.Bitmap.New(This.nWidth, This.nHeight)
            gfxBmp=.Graphics.FromImage(l_bitmap)
            gfxBmp.FillRectangle(.Brushes.Red, 0, 0, This.nWidth, This.nHeight)
            gfxBmpWnd =.Graphics.FromHWnd(This.WWhWnd)
            BmpHwnd = .Bitmap.FromScreen(This.WWhWnd, ;
                0, ;
                0, ;
                This.nWidth-1, ;
                This.nHeight-1)
            If Vartype(m.BmpHwnd)='O' And !Isnull(m.BmpHwnd)
                Local l_Pixel, l_color
                l_Pixel = BmpHwnd.GetPixel(7,10)
                gfxBmpWnd = .Graphics.FromImage(BmpHwnd)
                If Vartype(m.gfxBmpWnd)='O' And !Isnull(m.gfxBmpWnd)
                    gfxBmpWnd.DrawRectangle(.Pen.New(.Color.Red, 6), 4, 5,This.nWidth-8,This.nHeight-10)
                Endif
                BmpHwnd.MakeTransparent(l_Pixel)
                gfxBmp.DrawImage(m.BmpHwnd, 0, 0)
                gfxBmp.FillRectangle(This.oGradBrush, 0, 0, This.nWidth, This.nHeight)
                BmpHwnd.MakeTransparent(.Color.Red)
                gfxBmp.DrawImage(m.BmpHwnd, 0, 0)

                gfx=.Graphics.FromHWnd(This.WWhWnd)
                gfx.DrawImage(m.l_bitmap, 0, 0)
                gfx.DrawRectangle(.Pen.New(This.BorderColor, 1), This.rectClip)
                oBrush = .SolidBrush.New(This.BorderColor2)
                gfx.FillRectangle(oBrush, This.nLeft, This.nTop,1,1)&&LeftTopCorner
                gfx.FillRectangle(oBrush, This.nRight, This.nTop,1,1)&&RightTopCorner
                gfx.FillRectangle(oBrush, This.nLeft, This.nBottom,1,1)&&LeftBottomCorner
                gfx.FillRectangle(oBrush, This.nRight, This.nBottom,1,1)&&RightBottomCorner
            Endif
        Endwith
    Endproc

    Procedure Buff2Num
        Lparameters tc_Buffer, tn_Pos, tl_Signed
        Local ln_Result
        If tl_Signed
            ln_Result = CToBin( Substr( tc_Buffer, tn_Pos, 4 ), '4rs' )
        Else
            ln_Result = Asc(Substr( tc_Buffer, tn_Pos+0, 1 )) + ;
                asc(Substr( tc_Buffer, tn_Pos+1, 1 )) * 0x100 + ;
                asc(Substr( tc_Buffer, tn_Pos+2, 1 )) * 0x10000 + ;
                asc(Substr( tc_Buffer, tn_Pos+3, 1 )) * 0x1000000
        Endif
        Return ln_Result
    Endproc

    Procedure _UnBindEvents()
        Unbindevent(This.WWhWnd, WM_DESTROY)
        Unbindevent(This.WWhWnd, WM_PAINT)
        Unbindevent(This.WWhWnd, WM_ERASEBKGND)
    Endproc

    Procedure BindEvents()
        Bindevent(This.WWhWnd, WM_DESTROY, This, "_WndProc")
        Bindevent(This.WWhWnd, WM_PAINT, This, "_WndProc", 5)
        Bindevent(This.WWhWnd, WM_ERASEBKGND, This, "_WndProc", 5)
    Endproc

    Procedure Destroy
        If This.WWhWnd<>0
            This._UnBindEvents()
        Endif
        Unbindevents(0, WM_CREATE)
        _Screen.LockScreen = This.bOldLockScreen
    Endproc
Enddefine


*--- cp_FormDecorator
Define Class cp_FormDecorator As Custom
    bNCLButtonDownVFP = .T.	&&.T. Vfp NCLBUTTONDOWN - .F. StdWin NCLBUTTONDOWN
    hOrigProc = 0
    hWindow = 0
    BorderWidth = 5
    bResizable = .T.
    nTitlebar = -1

    hChildWindow = 0
    hChildProc = 0

    hCur_SIZENS = 0
    hCur_SIZEWE = 0
    hCur_SIZENESW = 0
    hCur_SIZENWSE = 0
    bSetCursorDefault = .T.

    Procedure Init()
        With This.Parent
            *--- Caption
            This.nTitlebar = .TitleBar
            .TitleBar = 0
            .AddObject("oCaptionBar", "cp_Caption")
            This.Height = Iif(This.nTitlebar=0,This.BorderWidth, This.BorderWidth + .oCaptionBar.Height)
            .Height = .Height + This.Height
            .AddObject("oGripper", "cp_gripper")
            *--- Border resize
            .AddObject("oBorderTop", "cp_Border", 0)
            .AddObject("oBorderLeft", "cp_Border", 1)
            .AddObject("oBorderRight", "cp_Border", 2)
            .AddObject("oBorderBottom", "cp_Border", 3)
            This.ApplyDecoration()
        Endwith
    Endproc

    Procedure ApplyDecoration()
        With This
            Local ctrl
            *--- Modifico form
            .Parent.LockScreen = .T.

            *--- Abbasso tutto dell'altezza della nuova caption
            Local nAnchor_old
            For Each ctrl In .Parent.Controls
                If !Inlist(Lower(m.ctrl.Class), "cp_border", "cp_caption", "cp_gripper") And Pemstatus(m.ctrl, "Top", 5)
                    If Pemstatus(m.ctrl, "Anchor", 5)
                        nAnchor_old = m.ctrl.Anchor
                        m.ctrl.Anchor = 0
                    Endif
                    m.ctrl.Top = m.ctrl.Top + Iif(.nTitlebar=1, .Parent.oCaptionBar.Height, 2)
                    If Pemstatus(m.ctrl, "Anchor", 5)
                        m.ctrl.Anchor = m.nAnchor_old
                    Endif
                Endif
            Endfor
            m.ctrl = .Null.
            .OnCaption()
            .OnMinButton()
            .OnMaxButton()
            .OnClosable()
            .OnControlBox()
            .OnBorderStyle()

            .Parent.oCaptionBar.Visible = .nTitlebar=1
            .Parent.oGripper.Visible = .bResizable


            #Define GW_CHILD 5
            .DeclareApi()
            *--- hWindow
            .hWindow = .Parent.HWnd
            If GetWindow(.Parent.HWnd, GW_CHILD)<>0 &&.Parent.ShowWindow= 2
                .hChildWindow = GetWindow(.Parent.HWnd, GW_CHILD)
                .hChildProc = GetWindowLong(.hChildWindow, -4)
                #Define WM_NCHITTEST 0x0084
                Bindevent(.hChildWindow, WM_NCHITTEST, This, 'NCHitTestChildCallback', 6)
            Endif

            #Define GWL_STYLE -16
            nStyle = GetWindowLong(.hWindow, GWL_STYLE)
            #Define WS_SYSMENU 0x00080000
            = SetWindowLong(.hWindow, GWL_STYLE , Bitor(m.nStyle, WS_SYSMENU))

            *--- Set Cursor
            .hCur_SIZENS = LoadCursor(0,32645)
            .hCur_SIZEWE = LoadCursor(0,32644)
            .hCur_SIZENESW = LoadCursor(0,32643)
            .hCur_SIZENWSE = LoadCursor(0,32642)

            *--- BindEvent Form
            .BindEventForm()
            .Parent.LockScreen = .F.
        Endwith
    Endproc

    Procedure BindEventForm()
        With This
            #Define GWL_WNDPROC -4
            .hOrigProc = GetWindowLong(.hWindow, GWL_WNDPROC)

            *#Define WM_NCHITTEST 0x0084
            *#define WM_SETCURSOR 0x0020
            Bindevent(.hWindow , WM_NCHITTEST, This, 'NCHitTestCallback', 6)
            Bindevent(.hWindow , WM_SETCURSOR, This, 'SetCursorCallback', 6)

            #Define WM_NCLBUTTONDOWN 0x00A1
            #Define WM_NCLBUTTONDBLCLK 0x00A3
            Bindevent(.hWindow, WM_NCLBUTTONDOWN, This, 'NCLButtonDownCallback', 4)
            If !(i_cViewMode <> "S" And Vartype(_Screen.TBMDI)<>"U" And !Pemstatus(.Parent,"LinkPCClick",5) And ;
                 Type('.Parent.oPgFrm.Pages(1).oPag.resizeYpos')='N' And Type('.Parent.oPgFrm.Pages(1).oPag.resizeYpos')='N' And ;
                 .Parent.oPgFrm.Pages(1).oPag.resizeYpos<>-1 And .Parent.oPgFrm.Pages(1).oPag.resizeYpos<>-1 AND .Parent.WindowType=0) 
                    
                Bindevent(.hWindow, WM_NCLBUTTONDBLCLK, This, 'NCLButtonDblClkCallback', 4)
            Endif

            Bindevent(.Parent, "AddObject", This, "OnAddObject",1)
            Bindevent(.Parent, "Caption", This, "OnCaption",1)
            Bindevent(.Parent, "Activate", This, "OnActivate")
            Bindevent(.Parent, "Deactivate", This, "OnDeactivate")
            Bindevent(.Parent, "MinButton", This, "OnMinButton",1)
            Bindevent(.Parent, "MaxButton", This, "OnMaxButton",1)
            Bindevent(.Parent, "ControlBox", This, "OnControlBox",1)
            Bindevent(.Parent, "Closable", This, "OnClosable",1)
            Bindevent(.Parent, "TitleBar", This, "OnTitleBar",1)
            Bindevent(.Parent, "BorderStyle", This, "OnBorderStyle",1)
            Bindevent(.Parent, "Resize", This, "OnResize",1)
        Endwith
    Endproc

    Procedure UnBindEventForm()
        With This
            Unbindevent(.hWindow, WM_NCHITTEST)
            Unbindevent(.hWindow, WM_SETCURSOR)
            Unbindevent(.hWindow, WM_NCLBUTTONDOWN)
            Unbindevent(.hWindow, WM_NCLBUTTONDBLCLK)
            If .hChildWindow<>0
                Unbindevent(.hChildWindow, WM_NCHITTEST)
            Endif
            Unbindevent(.Parent, "AddObject", This, "OnAddObject")
            Unbindevent(.Parent, "Caption", This, "OnCaption")
            Unbindevent(.Parent, "Activate", This, "OnActivate")
            Unbindevent(.Parent, "Deactivate", This, "OnDeactivate")
            Unbindevent(.Parent, "MinButton", This, "OnMinButton")
            Unbindevent(.Parent, "MaxButton", This, "OnMaxButton")
            Unbindevent(.Parent, "ControlBox", This, "OnControlBox")
            Unbindevent(.Parent, "Closable", This, "OnClosable")
            Unbindevent(.Parent, "TitleBar", This, "OnTitleBar")
            Unbindevent(.Parent, "BorderStyle", This, "OnBorderStyle")
            Unbindevent(.Parent, "Resize", This, "OnResize")            
        Endwith
    Endproc

    Procedure Destroy()
        This.UnBindEventForm()
    Endproc

    *--- Callback function
    Function NCHitTestChildCallback(HWnd, uMsg, wParam, Lparam)
        Local ret
        If This.Parent.WindowState<>1
            Return -1  &&HTTRANSPARENT
        Endif
        Return CallWindowProc(This.hChildProc, m.HWnd, m.uMsg, m.wParam, m.lParam)
    Endfunc

    *--- WM_SETCURSOR
    Function SetCursorCallback(HWnd, uMsg, wParam, Lparam)
        *--- Se il cursore � stato modificato dalla NCHitTestCallback non richiamo il default
        With This
            If .bSetCursorDefault
                Return CallWindowProc(.hOrigProc, m.HWnd, m.uMsg, m.wParam, m.lParam)
            Else
                Return .T.
            Endif
        Endwith
    Endfunc

    *--- WM_NCHITTEST
    Function NCHitTestCallback(HWnd, uMsg, wParam, Lparam)
    	*--- Problema del focus tenuto dall'interfaccia dei gadget
    	If Vartype(oGadgetManager)='O' And Thisform.WindowType=1 And Vartype(i_curform)<>'O' And GetForegroundWindow()==_vfp.hwnd
    		oGadgetManager.bActivateScreen = .T.
    		*--- Click sulla finestra del Fox
        	SendMessage(_vfp.HWnd, 0x00A1, 2, 0) && WM_NCLBUTTONDOWN
    	    SendMessage(_vfp.HWnd, 0x00A2, 2, 0) && WM_NCLBUTTONUP
    	Endif
    	
        *--- Modifica il cursore del mouse e ritorna la giusta nchittest
        Local nXCoord, nYCoord, nRet, cBuffer
        cBuffer = Replicate(Chr(0), 16)
        GetWindowRect(m.HWnd, @cBuffer)
        * low-order word: cursor x-coordinate
        nXCoord = Get_X_LParam(m.lParam) - CToBin(Substr(m.cBuffer, 1, 4), "SR")
        * high-order word: cursor y-coordinate
        nYCoord =  Get_Y_LParam(m.lParam) - CToBin(Substr(m.cBuffer, 5, 4), "SR")

        With This
            .bSetCursorDefault = .T.
            If .Parent.WindowState<>1
                If (!.Parent.oCaptionBar.oCtrlBox.PointInRect(m.nXCoord, m.nYCoord))
                    .Parent.oCaptionBar.oCtrlBox.oShapeHover.Visible=.F.
                Endif
                If .bResizable And .Parent.WindowState=0
                    *--- TopLeft corner
                    If (m.nYCoord>=0 And m.nYCoord<=3*.BorderWidth And m.nXCoord>=0 And m.nXCoord<=.BorderWidth) Or ;
                            (m.nYCoord>=0 And m.nYCoord<=.BorderWidth And m.nXCoord>=0 And m.nXCoord<=3*.BorderWidth)
                        SetCursor(.hCur_SIZENWSE)
                        .bSetCursorDefault = .F.
                        Return 13 &&HTTOPLEFT
                    Endif
                    *--- TopRight corner
                    If (m.nYCoord>=0 And m.nYCoord<=3*.BorderWidth And m.nXCoord>=.Parent.Width-.BorderWidth And m.nXCoord<=.Parent.Width) Or ;
                            (m.nYCoord>=0 And m.nYCoord<=.BorderWidth And m.nXCoord>=.Parent.Width-3*.BorderWidth And m.nXCoord<=.Parent.Width)
                        SetCursor(.hCur_SIZENESW)
                        .bSetCursorDefault = .F.
                        Return 14 &&HTTOPRIGHT
                    Endif
                    *--- BottomLeft corner
                    If (m.nYCoord>=.Parent.oBorderBottom.Top-3*.BorderWidth  And m.nYCoord<=.Parent.oBorderBottom.Top And m.nXCoord>=0 And m.nXCoord<=.BorderWidth) Or ;
                            (m.nYCoord>=.Parent.oBorderBottom.Top-.BorderWidth  And m.nYCoord<=.Parent.oBorderBottom.Top And m.nXCoord>=0 And m.nXCoord<=3*.BorderWidth)
                        SetCursor(.hCur_SIZENESW)
                        .bSetCursorDefault = .F.
                        Return 16&&HTBOTTOMLEFT
                    Endif
                    *--- BottomRight corner
                    If (m.nYCoord>=.Parent.Height-3*.BorderWidth  And m.nYCoord<=.Parent.oBorderBottom.Top And m.nXCoord>=.Parent.Width-.BorderWidth And m.nXCoord<=.Parent.Width) Or ;
                            (m.nYCoord>=.Parent.Height-.BorderWidth  And m.nYCoord<=.Parent.oBorderBottom.Top And m.nXCoord>=.Parent.Width-3*.BorderWidth And m.nXCoord<=.Parent.Width)
                        SetCursor(.hCur_SIZENWSE)
                        .bSetCursorDefault = .F.
                        Return 17 &&HTBOTTOMRIGHT
                    Endif

                    *--- Top Border
                    If m.nYCoord>=0 And m.nYCoord<=.BorderWidth
                        SetCursor(.hCur_SIZENS)
                        .bSetCursorDefault = .F.
                        Return 12 &&HTTOP
                    Endif
                    *--- Left Border
                    If m.nXCoord>=0 And m.nXCoord<=.BorderWidth
                        SetCursor(.hCur_SIZEWE)
                        .bSetCursorDefault = .F.
                        Return 10 &&HTLEFT
                    Endif
                    *--- Bottom Border
                    If m.nYCoord>=.Parent.oBorderBottom.Top-.BorderWidth And m.nYCoord<=.Parent.oBorderBottom.Top
                        SetCursor(.hCur_SIZENS)
                        .bSetCursorDefault = .F.
                        Return 15 &&HTBOTTOM
                    Endif
                    *--- Right Border
                    If m.nXCoord>=.Parent.Width-.BorderWidth And m.nXCoord<=.Parent.Width
                        SetCursor(.hCur_SIZEWE)
                        .bSetCursorDefault = .F.
                        .Parent.oCaptionBar.oCtrlBox.oShapeHover.Visible=.F.
                        Return 11 &&HTRIGHT
                    Endif
                Endif
                *--- Caption
                If .Parent.oCaptionBar.PointInRect(m.nXCoord, m.nYCoord) And ;
                        (!.Parent.oCaptionBar.oCtrlBox.PointInRect(m.nXCoord, m.nYCoord))
                    Return 2 &&HTCAPTION
                Endif
            Endif
            *--- Call std vfp WndProc
            Return CallWindowProc(.hOrigProc, m.HWnd, m.uMsg, m.wParam, m.lParam)
        Endwith
    Endfunc

    *--- WM_NCLBUTTONDOWN
    Func NCLButtonDownCallback
        *--- Se non intercetto WM_NCLBUTTONDOWN le form non attive non prendono il focus quando clicco sulla No Client Area
        Lparameters HWnd, uMsg, wParam, Lparam
        Local nRet
        If This.bNCLButtonDownVFP
            nRet = CallWindowProc(This.hOrigProc, m.HWnd, m.uMsg, m.wParam, m.lParam)
            If This.bResizable
                This.bNCLButtonDownVFP = .F.
                *= ReleaseCapture()
                * Complete left click by sending 'left NC button up' message
                *#Define WM_LBUTTONUP 0x202
                = SendMessage(m.HWnd, WM_LBUTTONUP, m.wParam, m.lParam)
                *--- Metto nella coda nuovamente WM_NCLBUTTONDOWN per eseguire la DefWindowProc
                = PostMessage(m.HWnd, m.uMsg, m.wParam, m.lParam)
            Endif
        Else
            This.bNCLButtonDownVFP = .T.
            If GetFocus() == Thisform.HWnd Or (Type("_Screen.ActiveForm")='O' And !IsNull(_Screen.ActiveForm) AND _Screen.ActiveForm.hwnd == Thisform.HWnd)
            nRet = DefWindowProc(m.HWnd, m.uMsg, m.wParam, m.lParam)
            Else
            	nRet = CallWindowProc(This.hOrigProc, m.HWnd, m.uMsg, m.wParam, m.lParam)
            Endif
        Endif
        Return nRet
    Endfunc

    *--- WM_NCLBUTTONDBLCLK
    Function NCLButtonDblClkCallback
        *--- Se non intercetto WM_NCLBUTTONDBLCLK le form non si massimizzano al DblClk sulla titlebar
        Lparameters HWnd, uMsg, wParam, Lparam
        If GetFocus() == Thisform.HWnd Or (Type("_Screen.ActiveForm")='O' And !IsNull(_Screen.ActiveForm) And _Screen.ActiveForm.hwnd == Thisform.HWnd)
        	Return DefWindowProc(m.HWnd, m.uMsg, m.wParam, m.lParam)
        Else
        	Return CallWindowProc(This.hOrigProc, m.HWnd, m.uMsg, m.wParam, m.lParam)
        Endif
    Endfunc

    *----*

    Procedure DeclareApi()
        Declare Long ReleaseCapture In WIN32API

        Declare Long SendMessage In WIN32API ;
            Long HWnd, Long wMsg, Long wParam, Long Lparam

        Declare SHORT PostMessage In WIN32API;
            INTEGER   HWnd,;
            Integer   Msg,;
            Long      wParam,;
            Long      Lparam

        Declare Integer DefWindowProc In WIN32API;
            LONG HWnd,;
            LONG Msg,;
            INTEGER wParam,;
            INTEGER Lparam

        Declare Integer GetWindowRect In WIN32API;
            INTEGER hWindow,;
            STRING @lpRect

        Declare Integer CallWindowProc In WIN32API;
            INTEGER lpPrevWndFunc, Integer hWindow, Long Msg,;
            INTEGER wParam, Integer Lparam

        Declare Integer GetWindowLong In WIN32API;
            INTEGER HWnd, Integer nIndex

        Declare Integer GetWindow In WIN32API Integer HWnd, Integer wFlag

        Declare Integer GetFocus In WIN32API

        Declare Integer GetSystemMenu In WIN32API;
            INTEGER hWindow, Integer bRevert

        Declare Integer TrackPopupMenuEx In WIN32API;
            LONG hmenu,;
            LONG fuFlags,;
            INTEGER x,;
            INTEGER Y,;
            LONG HWnd,;
            INTEGER lptpm

        Declare Integer SetWindowLong In WIN32API;
            INTEGER HWnd,;
            INTEGER nIndex,;
            INTEGER dwNewLong

        Declare Integer SetCursor In WIN32API Integer
        Declare Integer LoadCursor In WIN32API Integer, Integer

    Endproc

    Procedure RemoveDecoration()
        This.UnBindEventForm()
    Endproc

    *--- BindEvents
    Procedure OnAddObject(cName, cClass, cOLEClass, aInit1, aInit2, aInit3, aInit4, aInit5, aInit6, aInit7)
        Local oCtrl,old_Anchor
        With This
            oCtrl = .Parent.&cName
            If Pemstatus(oCtrl, "Top", 5)
                If Pemstatus(oCtrl, "Anchor", 5)
                    old_Anchor = oCtrl.Anchor
                    oCtrl.Anchor = 0
                    oCtrl.Top = oCtrl.Top + Iif(.nTitlebar=1, .Parent.oCaptionBar.Height, 2)
                    oCtrl.Anchor = m.old_Anchor
                Else
                    oCtrl.Top = oCtrl.Top + Iif(.nTitlebar=1, .Parent.oCaptionBar.Height, 2)
                Endif
            Endif
            .Parent.oBorderLeft.ZOrder(0)
            .Parent.oBorderRight.ZOrder(0)
        Endwith
    Endproc

    Procedure OnCaption()
        Local cStatus
        m.cStatus = ''
        If Pemstatus(This.Parent, "cPrg",5) And !Inlist(cp_getEntityType(This.Parent.cPrg),"Dialog Window","unkown")
            Do Case &&Query,Filter,Edit,Load
                Case This.Parent.cFunction = "Query"
                    m.cStatus = Iif(g_Revi='S', "bmp\revi_view_mode.png", cp_Translate(MSG_QUERY))
                Case This.Parent.cFunction = "Filter"
                    m.cStatus = Iif(g_Revi='S', "bmp\revi_query_mode.png", cp_Translate(MSG_FILTER)+" "+Iif( This.Parent.bSecDataFilter ,MSG_AUTORITY  ,''))
                Case This.Parent.cFunction = "Edit"
                    m.cStatus = Iif(g_Revi='S', "bmp\revi_edit_mode.png", cp_Translate(MSG_CHANGE))
                Case This.Parent.cFunction = "Load"
                    m.cStatus = Iif(g_Revi='S', "bmp\revi_new_mode.png", cp_Translate(MSG_LOAD))
            Endcase
        Endif
        This.Parent.oCaptionBar.SetCaption(m.cStatus, Iif(Pemstatus(This.Parent, "cPrg", 5), This.Parent.cComment, This.Parent.Caption))
    Endproc

    Procedure OnActivate()
        This.Parent.oCaptionBar.ActivateCaption(.T.)
    Endproc

    Procedure OnDeactivate()
        This.Parent.oCaptionBar.ActivateCaption(.F.)
    Endproc

    Procedure OnMinButton()
        This.Parent.oCaptionBar.oCtrlBox.oBtnMin.Enabled = This.Parent.MinButton
    Endproc

    Procedure OnMaxButton()
        This.Parent.oCaptionBar.oCtrlBox.oBtnMax.Enabled = This.Parent.MaxButton
    Endproc

    Procedure OnControlBox()
        This.Parent.oCaptionBar.oCtrlBox.Visible = This.Parent.ControlBox
    Endproc

    Procedure OnClosable()
        This.Parent.oCaptionBar.oCtrlBox.oBtnClose.Enabled = This.Parent.Closable
    Endproc

    Procedure OnTitleBar()
        With This
            Local nDelta
            If .nTitlebar # .Parent.TitleBar
                Do Case
                    Case .nTitlebar=-1
                        nDelta = 0
                    Case .nTitlebar=0
                        nDelta = 2
                    Case .nTitlebar=1
                        nDelta = .Parent.oCaptionBar.Height
                Endcase
                .nTitlebar = .Parent.TitleBar
                .Parent.oCaptionBar.Visible = (.nTitlebar=1)
                
                *--- Abbasso/Alzo tutto dell'altezza della nuova caption
                Local nAnchor_old
                For Each ctrl In .Parent.Controls
                    If !Inlist(Lower(m.ctrl.Class), "cp_border", "cp_caption", "cp_gripper") And Pemstatus(m.ctrl, "Top", 5)
                        If Pemstatus(m.ctrl, "Anchor", 5)
                            nAnchor_old = m.ctrl.Anchor
                            m.ctrl.Anchor = 0
                        Endif
                        m.ctrl.Top = m.ctrl.Top + Iif(.nTitlebar=1, .Parent.oCaptionBar.Height-m.nDelta, 2-m.nDelta)
                        If Pemstatus(m.ctrl, "Anchor", 5)
                            m.ctrl.Anchor = m.nAnchor_old
                        Endif
                    Endif
                Endfor
            Endif
            *--- Elimino titlebar std vfp
            .Parent.TitleBar = 0
        Endwith
    Endproc

    Procedure OnBorderStyle()
        With This.Parent
            This.bResizable = (.BorderStyle = 3)
            *--- Gripper per indicare la possibilit� di resize
            .oGripper.Visible = This.bResizable
            .oBorderTop.Visible = (.BorderStyle # 0) Or This.nTitlebar=1
            .oBorderTop.Anchor=0
            If .BorderStyle = 3 Or !This.nTitlebar=0
                .oBorderTop.BorderWidth = 3
                .oBorderTop.Top = 1
            Else
                .oBorderTop.BorderWidth = 1
                .oBorderTop.Top = 0
            Endif
            .oBorderTop.Anchor=11
            .oBorderLeft.Visible = (.BorderStyle # 0) Or This.nTitlebar=1
            .oBorderRight.Visible = (.BorderStyle # 0) Or This.nTitlebar=1
            .oBorderBottom.Visible = (.BorderStyle # 0) Or This.nTitlebar=1
            If .BorderStyle=3
                nStyle = GetWindowLong(.HWnd, -16)
                #Define WS_SYSMENU 0x00080000
                = SetWindowLong(.HWnd, -16 , Bitand(Bitand(m.nStyle, Bitnot(0x00040000)), Bitnot(0x00800000)))
                nStyle = GetWindowLong(.HWnd, -20)
                = SetWindowLong(.HWnd, -20 , Bitand(m.nStyle, Bitnot(0x00000001)))
                *--- non uso la move
                .Height=.Height-2*Sysmetric(4)
                .Width=.Width-2*Sysmetric(3)
            Else
                .BorderStyle = 0
            Endif
        Endwith
    Endproc

PROCEDURE Set_WM_CAPTION(bCaptionOn)
           LOCAL nStyle
           With This.Parent
           nStyle = GetWindowLong(.HWnd, -16)
                IF bCaptionOn
                   = SetWindowLong(.HWnd, -16 , Bitor(m.nStyle, (0x00C00000)))
                Else
                   = SetWindowLong(.HWnd, -16 , Bitand(m.nStyle, Bitnot(0x00C00000)))
                ENDIF 
           EndWith
 endproc

    Procedure OnResize()
        With This.Parent
                        This.Set_WM_CAPTION(.windowstate==1)
            .oCaptionBar.oCtrlBox.oBtnMax.SetMaximizeBtn()
            *-- Riduco il titolo se la maschera si stringe
            Local l_title,nLeft, nWidth
            m.l_title = .oCaptionBar.cFormCaption
            m.nLeft= Len(m.l_title)
            m.nWidth = .oCaptionBar.Width-(.oCaptionBar.oLblCaption.Left+.oCaptionBar.oCtrlBox.Width)
            Do While m.nLeft>1 And m.nWidth<cp_LabelWidth2(m.l_title, .oCaptionBar.oLblCaption.FontName,.oCaptionBar.oLblCaption.FontSize)
                m.nLeft = m.nLeft-1
                m.l_title = Left(m.l_title,m.nLeft)+'...'
            Enddo
            .oCaptionBar.oLblCaption.Caption = m.l_title
        Endwith
    Endproc

Enddefine

*--- cp_Caption
Define Class cp_LblSeparator As Container
    BorderWidth = 0
    BackStyle = 0
    Width = 2
    Height = 16
    nMargin = 15
    Add Object oLine1 As Line With Top=0,Width=0,Height=16,Left=0
    Add Object oLine2 As Line With Top=0,Width=0,Height=16,Left=1

    Procedure BackColor_Assign(xValue)
        This.BackColor = m.xValue
        This.oLine1.BorderColor = RGBAlphaBlending(m.xValue, Rgb(255,255,255), 0.40)
        This.oLine2.BorderColor = RGBAlphaBlending(m.xValue, Rgb(255,255,255), 0.60)
    Endproc
Enddefine

Define Class cp_ControlBoxButton As Shape
    Visible=.T.
    nType = 0
    Dimension aPolyPoints[1,2]
    Procedure Init(pType)
        With This
            .nType = m.pType
            Do Case
                Case pType=0	&&Close X
                    Dimension .aPolyPoints[15,2]
                    .aPolyPoints[1,1] = 10&&A
                    .aPolyPoints[1,2] = 10

                    .aPolyPoints[2,1] = 20&&B
                    .aPolyPoints[2,2] = 10

                    .aPolyPoints[3,1] = 40&&C
                    .aPolyPoints[3,2] = 30

                    .aPolyPoints[4,1] = 50&&D
                    .aPolyPoints[4,2] = 30

                    .aPolyPoints[5,1] = 70&&E
                    .aPolyPoints[5,2] = 10

                    .aPolyPoints[6,1] = 80&&F
                    .aPolyPoints[6,2] = 10

                    .aPolyPoints[7,1] = 50&&G
                    .aPolyPoints[7,2] = 40

                    .aPolyPoints[8,1] = 50&&H
                    .aPolyPoints[8,2] = 50

                    .aPolyPoints[9,1] = 80&&I
                    .aPolyPoints[9,2] = 80

                    .aPolyPoints[10,1] = 70&&L
                    .aPolyPoints[10,2] = 80

                    .aPolyPoints[11,1] = 40&&M
                    .aPolyPoints[11,2] = 50

                    .aPolyPoints[12,1] = 10&&N
                    .aPolyPoints[12,2] = 80

                    .aPolyPoints[13,1] = 20&&O
                    .aPolyPoints[13,2] = 80

                    .aPolyPoints[14,1] = 40&&P
                    .aPolyPoints[14,2] = 60

                    .aPolyPoints[15,1] = 40&&Q
                    .aPolyPoints[15,2] = 40

                    .Polypoints ="This.aPolyPoints"
                    .Move(.Parent.Width-20, 9, 10,10)
                Case pType=1	&&MaxButton
                    .SetMaximizeBtn()
                Case pType=2	&&MinButton
                    Dimension .aPolyPoints[4,2]
                    .aPolyPoints[1,1] = 1&&A
                    .aPolyPoints[1,2] = 80

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 80

                    .aPolyPoints[3,1] = 100&&C
                    .aPolyPoints[3,2] = 100

                    .aPolyPoints[4,1] = 1&&D
                    .aPolyPoints[4,2] = 100
                    .Move(.Parent.Width-68, 7, 9,11)
                    .Polypoints ="This.aPolyPoints"
                    .BorderStyle=0
                    .BackStyle=1
                    .FillStyle=1
                Case pType=3	&&MenuButton
                    Dimension .aPolyPoints[12,2]
                    .aPolyPoints[1,1] = 1&&A
                    .aPolyPoints[1,2] = 1

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 1

                    .aPolyPoints[3,1] = 100&&C
                    .aPolyPoints[3,2] = 20

                    .aPolyPoints[4,1] = 1&&D
                    .aPolyPoints[4,2] = 20

                    .aPolyPoints[5,1] = 1&&E
                    .aPolyPoints[5,2] = 30

                    .aPolyPoints[6,1] = 100&&F
                    .aPolyPoints[6,2] = 30

                    .aPolyPoints[7,1] = 100&&H
                    .aPolyPoints[7,2] = 50

                    .aPolyPoints[8,1] = 1&&I
                    .aPolyPoints[8,2] = 50

                    .aPolyPoints[9,1] = 1&&H
                    .aPolyPoints[9,2] = 60

                    .aPolyPoints[10,1] = 100&&I
                    .aPolyPoints[10,2] = 60

                    .aPolyPoints[11,1] = 100&&I
                    .aPolyPoints[11,2] = 80
                    .aPolyPoints[12,1] = 1&&I
                    .aPolyPoints[12,2] = 80

                    .Polypoints ="This.aPolyPoints"
                    .BorderStyle=0
                    .FillStyle=0
                    .Move(.Parent.Width-92, 9, 10,11)
                Case pType=4	&&RestoreButton
                    Dimension .aPolyPoints[12,2]
                    .aPolyPoints[1,1] = 20&&A
                    .aPolyPoints[1,2] = 50

                    .aPolyPoints[2,1] = 30&&B
                    .aPolyPoints[2,2] = 40

                    .aPolyPoints[3,1] = 30&&C
                    .aPolyPoints[3,2] = 20

                    .aPolyPoints[4,1] = 100&&D
                    .aPolyPoints[4,2] = 20

                    .aPolyPoints[5,1] = 100&&E
                    .aPolyPoints[5,2] = 70

                    .aPolyPoints[6,1] = 90&&F
                    .aPolyPoints[6,2] = 80

                    .aPolyPoints[7,1] = 90&&G
                    .aPolyPoints[7,2] = 50

                    .aPolyPoints[8,1] = 20&&H
                    .aPolyPoints[8,2] = 50

                    .aPolyPoints[9,1] = 20&&I
                    .aPolyPoints[9,2] = 100

                    .aPolyPoints[10,1] = 90&&L
                    .aPolyPoints[10,2] = 100

                    .aPolyPoints[11,1] = 90&&M
                    .aPolyPoints[11,2] = 90

                    .aPolyPoints[12,1] = 20&&N
                    .aPolyPoints[12,2] = 90
                    .Polypoints ="This.aPolyPoints"
                    .Move(.Parent.Width-44, 11, 10,10)
                    .BackStyle = 0
                    .Visible=.F.
            Endcase
            .BackColor = RGBAlphaBlending(.Parent.Parent.nBackColor, Rgb(255,255,255), 0.9)
            .BorderColor = .BackColor
            .FillColor = .BackColor
        Endwith
    Endproc

    Procedure SetMaximizeBtn()
        With This
            If .Parent.Parent.Parent.WindowState=2
                &&Restore Icon
                Dimension .aPolyPoints[12,2]
                .aPolyPoints[1,1] = 10&&A
                .aPolyPoints[1,2] = 50

                .aPolyPoints[2,1] = 20&&B
                .aPolyPoints[2,2] = 40

                .aPolyPoints[3,1] = 20&&C
                .aPolyPoints[3,2] = 20

                .aPolyPoints[4,1] = 90&&D
                .aPolyPoints[4,2] = 20

                .aPolyPoints[5,1] = 90&&E
                .aPolyPoints[5,2] = 70

                .aPolyPoints[6,1] = 80&&F
                .aPolyPoints[6,2] = 80

                .aPolyPoints[7,1] = 80&&G
                .aPolyPoints[7,2] = 50

                .aPolyPoints[8,1] = 10&&H
                .aPolyPoints[8,2] = 50

                .aPolyPoints[9,1] = 10&&I
                .aPolyPoints[9,2] = 100

                .aPolyPoints[10,1] = 80&&L
                .aPolyPoints[10,2] = 100

                .aPolyPoints[11,1] = 80&&M
                .aPolyPoints[11,2] = 90

                .aPolyPoints[12,1] = 10&&N
                .aPolyPoints[12,2] = 90
                .Move(.Parent.Width-45, 8, 10,10)
            Else
                &&Maximize Icon
                Dimension .aPolyPoints[7,2]
                .aPolyPoints[1,1] = 1&&A
                .aPolyPoints[1,2] = 100
                .aPolyPoints[2,1] = 100&&B
                .aPolyPoints[2,2] = 100
                .aPolyPoints[3,1] = 100&&C
                .aPolyPoints[3,2] = 80
                .aPolyPoints[4,1] = 1&&D
                .aPolyPoints[4,2] = 80
                .aPolyPoints[5,1] = 1&&E
                .aPolyPoints[5,2] = 1
                .aPolyPoints[6,1] = 100&&F
                .aPolyPoints[6,2] = 1
                .aPolyPoints[7,1] = 100&&H
                .aPolyPoints[7,2] = 100
                .Move(.Parent.Width-44, 11, 9,6)
            Endif
            .Polypoints ="This.aPolyPoints"
            .BackStyle = 0

        Endwith
    Endproc

    Procedure Enabled_assign(xValue)
        With This
            .Enabled = m.xValue
            If m.xValue
                .BackColor = RGBAlphaBlending(.Parent.Parent.nBackColor, Rgb(255,255,255), 0.9)
                .BorderColor = .BackColor
                .FillColor = .BackColor
            Else
                .BackColor = RGBAlphaBlending(.Parent.Parent.nBackColor, Rgb(255,255,255), 0.1)
                .BorderColor = .BackColor
                .FillColor = .BackColor
            Endif
        Endwith
    Endproc

    Procedure MouseEnter(nButton, nShift, nXCoord, nYCoord)
        If m.nButton=0 And This.Enabled
            This.Parent.oShapeHover.Move(This.Left-Ceiling(This.Parent.oShapeHover.Width/4))
            This.Parent.oShapeHover.Visible=.T.
        Endif
    Endproc

    Procedure Click
        With This
            Do Case
                Case .nType=0
                    #Define SC_CLOSE 0xF060
                    *RAISEEVENT(This.Parent.Parent.Parent, "Activate")
                    *PostMessage(This.Parent.Parent.Parent.oDec.hWindow, WM_SYSCOMMAND, SC_CLOSE, 0)
                    *!*	                    LOCAL oldCursor
                    *!*	                    oldCursor = SET("Cursor")
                    *!*	                    SET CURSOR OFF
                    *!*	                    INKEY(.151)
                    *!*	                    SET CURSOR &oldCursor
                
                    SendMessage(Thisform.HWnd, WM_SYSCOMMAND, SC_CLOSE, 0)
                Case .nType=1
                    If .Parent.Parent.Parent.WindowState=2
                        #Define SC_RESTORE 0xF120
                        SendMessage(Thisform.HWnd, WM_SYSCOMMAND, SC_RESTORE, 0)
                    Else
                        #Define SC_MAXIMIZE 0xF030
                        SendMessage(Thisform.HWnd, WM_SYSCOMMAND, SC_MAXIMIZE, 0)
                    Endif
                Case .nType=2
                    #Define SC_MINIMIZE 0xF020
                    PostMessage(Thisform.HWnd, WM_SYSCOMMAND, SC_MINIMIZE, 0)
                Case .nType=3
                    Local hmenu, cmd, cBuffer
                    hmenu = GetSystemMenu(.Parent.Parent.Parent.oDec.hWindow, 0)
                    cBuffer = Replicate(Chr(0), 16)
                    GetWindowRect(.Parent.Parent.Parent.oDec.hWindow, @cBuffer)
                    cmd = TrackPopupMenuEx(m.hmenu, 0x100,  CToBin(Substr(cBuffer, 1, 4), "SR")+Objtoclient(.Parent.oShapeHover,2), ;
                        CToBin(Substr(cBuffer, 5, 4), "SR")+Objtoclient(.Parent.oShapeHover,1)+.Parent.oShapeHover.Height, ;
                        .Parent.Parent.Parent.oDec.hWindow, 0)
                    If m.cmd > 0
                        SendMessage(Thisform.HWnd, WM_SYSCOMMAND, m.cmd, 0)
                    Endif
            Endcase
        Endwith
    Endproc
Enddefine

Define Class cp_ControlBox As Container
    Width = 104
    Height = 29
    BackStyle=1
    BorderWidth=0
    Add Object oShapeHover As Shape With Width=21,Height=21,Top=3, BackStyle=0,BorderColor=Rgb(255,255,255), Visible=.F.

    Procedure Init()
        With This
            .AddObject("oBtnClose", "cp_ControlBoxButton" ,0)
            .AddObject("oBtnMax", "cp_ControlBoxButton" ,1)
            .AddObject("oBtnMin", "cp_ControlBoxButton" ,2)
            .AddObject("oBtnMenu", "cp_ControlBoxButton" ,3)
        Endwith
    Endproc

    Func PointInRect(nXCoord, nYCoord)
        Local nTop, nLeft
        nLeft = Objtoclient(This,2) &&Left
        nTop = Objtoclient(This,1) &&Top
        Return This.Visible And !(m.nXCoord < m.nLeft Or m.nXCoord > m.nLeft+This.Width Or m.nYCoord < m.nTop Or m.nYCoord > m.nTop+This.Height)
    Endfunc

    Function CollisionObject(nXCoord, nYCoord)
        With This
            Local obj, nTop, nLeft
            For Each obj In .Controls
                If Lower(obj.Class) == "cp_controlboxbutton" And obj.Visible And obj.Enabled
                    nLeft = Objtoclient(m.obj,2) - Ceiling(.oShapeHover.Width/4)&&Left
                    nTop = .oShapeHover.Top &&Objtoclient(Obj,1) &&Top
                    If !(m.nXCoord < m.nLeft Or m.nXCoord > m.nLeft+.oShapeHover.Width Or m.nYCoord < m.nTop Or m.nYCoord > m.nTop+.oShapeHover.Height)
                        Return m.obj
                    Endif
                Endif
            Next
            Return .Null.
        Endwith
    Endfunc

    Procedure MouseMove(nButton, nShift, nXCoord, nYCoord)
        If m.nButton=0
            With This
                Local oObj
                oObj = .CollisionObject(m.nXCoord, m.nYCoord)
                If !Isnull(m.oObj)
                    .oShapeHover.Move(m.oObj.Left-Ceiling(.oShapeHover.Width/4))
                    .oShapeHover.Visible=.T.
                Else
                    .oShapeHover.Visible=.F.
                Endif
            Endwith
        Endif
    Endproc

    Procedure MouseLeave(nButton, nShift, nXCoord, nYCoord)
        oObj = Sys(1270)
        If Type('m.oObj')='O' And !Isnull(m.oObj)
            This.oShapeHover.Visible= Inlist(Lower(m.oObj.Class), "cp_controlboxbutton", "shape")
        Else
            This.oShapeHover.Visible=.F.
        Endif
    Endproc

    Procedure oShapeHover.Click()
        Local oObj
        Amouseobj(aArrayMouse,1)
        oObj = This.Parent.CollisionObject(aArrayMouse[3], aArrayMouse[4])
        If !Isnull(m.oObj)
            oObj.Click()
        Endif
    Endproc

Enddefine

*--- Caption Class
Define Class cp_Caption As Container
    bHalfCaption = .F.	&&Child
    Height = 32
    BorderWidth=0
    Add Object oImgStatus As Image With Top=9, Left=10,Width=18, Height=18, Stretch=1, Visible=.F.
    Add Object oLblStatus As Label With BackStyle=0, AutoSize=.T., Top = 9, Left = 16, FontSize = 10, FontName = "Open Sans"
    Add Object oSepLbl As cp_LblSeparator With Top = 11
    Add Object oLblCaption As Label With BackStyle=0, AutoSize=.T., Top = 7, FontSize = 11, FontName = "Open Sans"
    Add Object oCtrlBox As cp_ControlBox
    nBackColor = Rgb(40,40,40)
    cFormCaption = '' &&Titolo della form

    Procedure Init()
        With This
            .oCtrlBox.Move(.Width-.oCtrlBox.Width,5)
            .oCtrlBox.Anchor = 8
            .Move(0,0,.Parent.Width)
            .Anchor = 11
            .ChangeTheme()
        Endwith
    Endproc

    Func PointInRect(nXCoord, nYCoord)
        Local nTop, nLeft
        nLeft = Objtoclient(This, 2) &&Left
        nTop = Objtoclient(This, 1) &&Top
        Return This.Visible And !(m.nXCoord < m.nLeft Or m.nXCoord > m.nLeft+This.Width Or m.nYCoord < m.nTop Or m.nYCoord > m.nTop+This.Height)
    Endfunc

    Procedure SetCaption(cStatus, cCaption)
        With This
            .oLblCaption.Caption = Iif(g_Revi='S', Upper(m.cCaption), m.cCaption)
            .cFormCaption = .oLblCaption.Caption
            If !Empty(m.cStatus)
            	If !Empty(JustExt(m.cStatus)) && Immagine
            		.oImgStatus.Picture = Alltrim(m.cStatus) &&Forceext(i_ThemesManager.RetBmpFromIco(Alltrim(m.cStatus), 16),"bmp")
            		.oImgStatus.Visible = .T.
            		.oLblStatus.Visible = .F.
            	Else
            		.oLblStatus.Caption = m.cStatus
            		.oLblStatus.Visible = .T.
            		.oImgStatus.Visible = .F.
            	Endif
                If .oLblStatus.Visible
                	.oSepLbl.Left = .oLblStatus.Left + .oLblStatus.Width + .oSepLbl.nMargin
	                .oLblCaption.Left = .oSepLbl.Left + .oSepLbl.Width + .oSepLbl.nMargin
	                .oSepLbl.Visible = .T.
              	Else
              		.oLblCaption.Left = 2*.oImgStatus.Left + .oImgStatus.Width
              		.oSepLbl.Visible = .F.
                Endif
            Else
                .oLblCaption.Left = .oLblStatus.Left
                .oImgStatus.Visible = .F.
                .oLblStatus.Visible = .F.
                .oSepLbl.Visible = .F.
            Endif
        Endwith
    Endproc

    *--- Da definire
    Procedure ChangeTheme()
        This.BackColor = This.nBackColor
        This.oSepLbl.BackColor = This.BackColor
        This.oLblStatus.ForeColor = Rgb(255, 255, 255)
        This.oLblStatus.FontName = "Open Sans"
        This.oLblStatus.FontSize = 11
        This.oLblStatus.Caption = This.oLblStatus.Caption
        This.oLblCaption.FontName = "Open Sans"
        This.oLblCaption.FontSize = 13
        This.oLblCaption.ForeColor = Rgb(255, 255, 255)
        This.oCtrlBox.BackColor = This.nBackColor
        This.oCtrlBox.oBtnMax.Enabled = This.oCtrlBox.oBtnMax.Enabled
        This.oCtrlBox.oBtnMin.Enabled = This.oCtrlBox.oBtnMin.Enabled
        This.oCtrlBox.oBtnClose.Enabled = This.oCtrlBox.oBtnClose.Enabled
        This.oCtrlBox.oBtnMenu.Enabled = This.oCtrlBox.oBtnMenu.Enabled
    Endproc

    Procedure ActivateCaption(bActive)
        If This.Visible
            If m.bActive
                *cp_SingleTransition(This, "BackColor", This.nBackColor, "C", 150)
                This.BackColor = This.nBackColor
                *This.Parent.oBorderTop.BorderColor = i_ThemesManager.GetProp(109)
            Else
                *cp_SingleTransition(This, "BackColor", RGBAlphaBlending(This.nBackColor, Rgb(255,255,255), 0.3), "C", 150)
                This.BackColor = RGBAlphaBlending(This.nBackColor, Rgb(255,255,255), 0.3)
                *This.Parent.oBorderTop.BorderColor = RGBAlphaBlending(i_ThemesManager.GetProp(109), Rgb(255,255,255), 0.3)
                This.oCtrlBox.oShapeHover.Visible=.F.
            Endif
            This.oCtrlBox.BackColor = This.BackColor
        Endif
    Endproc
Enddefine

*--- cp_Border
Define Class cp_Border As Line
    BorderWidth = 1
    nType = 0
    Procedure Init(pType)
        With This
            .nType= m.pType
            Do Case
                Case m.pType = 0
                    *--- Top
                    .Move(0, 2, .Parent.Width, 0)
                    .BorderWidth = 4
                    .Anchor = 11
                Case m.pType = 1
                    *--- Left
                    .Move(0, 0, 0, .Parent.Height)
                    .Anchor = 7
                Case m.pType = 2
                    *--- Right
                    .Move(.Parent.Width-.BorderWidth, 0, 0, .Parent.Height)
                    .Anchor = 13
                Case m.pType = 3
                    *--- Bottom
                    .Move(0, .Parent.Height-.BorderWidth, .Parent.Width, 0)
                    .Anchor = 14
            Endcase
            .ChangeTheme()
        Endwith
    Endproc

    Procedure ChangeTheme()
        If This.nType = 0
            This.BorderColor = i_ThemesManager.GetProp(109)
        Else
            This.BorderColor = Rgb(161,161,161)
        Endif
    Endproc
Enddefine

*--- cp_gripper
Define Class cp_Gripper As Shape
    Dimension aPolyPoints[3,2]
    Width=7
    Height=7
    Visible=.F.
    Procedure Init()
        With This
            .aPolyPoints[1,1] = 100&&A
            .aPolyPoints[1,2] = 1
            .aPolyPoints[2,1] = 100&&B
            .aPolyPoints[2,2] = 100
            .aPolyPoints[3,1] = 1&&C
            .aPolyPoints[3,2] = 100
            .Polypoints ="This.aPolyPoints"
            .BorderStyle=1
            .FillStyle=0
            .BackStyle=0
            .BorderColor=Rgb(161,161,161)
            .FillColor = Rgb(161,161,161)
            .Move(.Parent.Width-9, .Parent.Height-9)
            .Anchor = 12
        Endwith
    Endproc
Enddefine


*-- Zucchetti Aulla Inizio
*-- Pageframe per Gadget
Define Class cp_oPgFrm As Container

    BackStyle=0
    BorderWidth=0
    PageCount=1
    ActivePage=1
    Tabs=.F.
    TabOrientation=0
    ExtFldsPage=-1
    cEvent=''
    nPageActive=0
    bSetFont=.T.
    cTypeTransition = 'H'
    nTimeTransition = 300
    cTransitionMethod = "Linear"
    cTransitionId = ""	&&Id sessione transizione

    *!*		bDialogWindow=.F.
    Dimension aPageOrder[1]

    *-- Timer per ActivePage automatica
    nTimeSwap = 0 &&Timer disattivato con valore a 0
    Add Object oTimer As Timer With Enabled=.F., Interval=0

    *-- Copiato da StdPageFrame - Inizio
    bInAutoZoom=.F.
    * disabilita loadrec nel caso di spostamento da zoom di elenco a gestione
    bAutoZoomLoad=.F.
    TabStyle=1
    nOldWz=0
    nOldWs=0
    nOldHz=0
    nOldHs=0
    oRefHeaderObject=Null
    *-- Copiato da StdPageFrame - Fine

    Add Object Pages As Collection

    Proc Init()
        With This
            .PageCount=.PageCount
            * --- aggiunge la pagina di autozoom, con dentro un autozoom fasullo.
            *     il vero zoom verra' creato solo cliccandoci sopra
            *!*				IF Not .bDialogWindow
            *!*					.Pages(This.PageCount).AddObject("autozoom","commandbutton")
            *!*					.Pages(This.PageCount).Caption=cp_Translate(MSG_LIST)
            *!*				ENDIF
            If .bSetFont
                .SetFont()
            Endif
        Endwith
    Endproc

    Proc Calculate()
    Endproc

    Proc Event(cEvent)
    Endproc

    Proc PageCount_Assign(nValue)
        With This
            Local l_Count
            l_Count=1
            Do While l_Count <= m.nValue
                If Type("This.Pages(l_Count)")='U'
                    .AddPage(l_Count)
                Endif
                l_Count = l_Count+1
            Enddo
            .PageCount = m.nValue
        Endwith
    Endproc

    Proc ActivePage_Assign(nValue)
        With This
            Local l_Count,l_nLastActivePage, bTrovato, l_cPage
            If .ActivePage <> m.nValue
                l_nLastActivePage = .ActivePage
                l_Count = 1
                Do While l_Count <= .PageCount And Not bTrovato
                    If .aPageOrder[l_Count] = m.nValue
                        bTrovato = .T.
                    Endif
                    l_Count=l_Count+1
                Enddo
                .ActivePage = l_Count-1
                l_cPage = Transform(.ActivePage)
                Thisform.NotifyEvent('ActivatePage '+m.l_cPage)
                .PageTransition(l_nLastActivePage,.ActivePage,.cTypeTransition)
            Endif
        Endwith
    Endproc

    Proc AddPage(nNum)
        With This
            Local l_cNum,l_obj
            m.nNum = Iif(Type("m.nNum")<>'N',.PageCount,m.nNum)
            l_cNum = Alltrim(Str(m.nNum))
            .AddObject("Page"+l_cNum,"cp_Page")
            l_obj = .Page&l_cNum
            Dimension .aPageOrder[m.nNum]
            .aPageOrder[m.nNum] = m.nNum
            l_obj.Caption = l_obj.Name
            l_obj.PageOrder = m.nNum
            l_obj.BackStyle = 0
            l_obj.Visible = (m.nNum = 1)
            *-- L'elenco deve lasciare spazio per la scrollbar orizzontale e le opzioni di configurazione
            *l_Obj.Move(0,0,.Width,IIF(m.nNum < .PageCount,.Height,.Height-25))
            *-- Coi Gadget non c'� l'elenco finale, vado alla lunghezza della PgFrm
            l_obj.Move(0,0,.Width,.Height)
            l_obj.Anchor=15
            .Pages.Add(l_obj)
        Endwith
    Endproc

    Proc PageTransition(nOldPage,nNewPage,cType)
        With This
            Local l_cNumOld, l_cNumNew, l_OldPage, l_NewPage, l_OutForm,l_oTransit, nWidth, nHeight
            l_oTransit = Createobject("cp_TransitionManager")
            l_cNumOld = Alltrim(Str(m.nOldPage))
            l_OldPage = .Page&l_cNumOld
            l_cNumNew = Alltrim(Str(m.nNewPage))
            l_NewPage = .Page&l_cNumNew
            l_OldPage.Anchor=0
            l_NewPage.Anchor=0            
            Do Case
                Case cType=="H"  &&Transizione orizzontale
                    nWidth = Max(l_OldPage.Width, Thisform.Width)
                    l_OutForm = Iif(m.nNewPage>m.nOldPage ,l_OldPage.Left - m.nWidth , m.nWidth )
                    l_oTransit.Add(l_OldPage,"Left",l_OutForm,"N",.nTimeTransition, .cTransitionMethod )

                    nWidth = Max(l_NewPage.Width, Thisform.Width)
                    l_NewPage.Left = Iif(m.nNewPage>m.nOldPage ,m.nWidth ,l_NewPage.Left - m.nWidth)
                    l_NewPage.Enabled = .T.
                    l_NewPage.Visible = .T.
                    l_oTransit.Add(l_NewPage,"Left",0,"N", .nTimeTransition, .cTransitionMethod )
                Case cType=="V"  &&Transizione verticale
                    nHeight = Max(l_OldPage.Height, Thisform.Height)
                    l_OutForm = Iif(m.nNewPage>m.nOldPage ,l_OldPage.Top - m.nHeight ,m.nHeight )
                    l_oTransit.Add(l_OldPage,"Top",l_OutForm,"N",.nTimeTransition,.cTransitionMethod )
                    nHeight = Max(l_NewPage.Height, Thisform.Height)
                    l_NewPage.Top = Iif(m.nNewPage>m.nOldPage, m.nHeight, l_NewPage.Top-m.nHeight)
                    l_NewPage.Enabled = .T.
                    l_NewPage.Visible = .T.
                    l_oTransit.Add(l_NewPage,"Top",0,"N", .nTimeTransition,.cTransitionMethod )
            ENDCASE
            l_oTransit.OnCompleted(This, "OnCompletedTransition('"+l_cNumOld+"','"+l_cNumNew+"')")
            .cTransitionId = l_oTransit.Start(.F.)	&&Salvo l'Id della transizione
        Endwith
    Endproc

    *--- Operazioni finali dopo la transizione
    Procedure OnCompletedTransition(cNumOld, cNumNew)
        Local l_OldPage, l_NewPage
        With This
            l_OldPage = .Page&cNumOld
            l_OldPage.Enabled = .F.
            l_OldPage.Visible = .F.
            l_OldPage.Anchor=15

            l_NewPage = .Page&cNumNew
            l_NewPage.Anchor=15

            l_NewPage = .Null.
            l_OldPage = .Null.
        Endwith
    Endproc

    Proc SetPageOrder(OldValue,NewValue)
        With This
            Local l_Count, l_Num, l_obj, l_Start, l_Final
            If m.OldValue < m.NewValue
                l_Start = m.OldValue+1
                l_Final = m.NewValue
            Else
                l_Start = m.NewValue
                l_Final = m.OldValue-1
            Endif
            Do While m.l_Start <= m.l_Final
                l_Num = .aPageOrder[m.l_Start]
                .aPageOrder[m.l_Start] = Iif(m.OldValue < m.NewValue,Max(l_Num-1,1),l_Num+1)
                m.l_Start = m.l_Start+1
            Enddo
        Endwith
    Endproc

    *--- Swap page on timer
    Proc oTimer.Timer()
        With This.Parent
            Local nNextPage, cPage, L_OldArea
            If .ActivePage <> .PageCount
                nNextPage = .ActivePage+1
            Else
                nNextPage = 1
            Endif
            If Type("Thisform.oHeader")="O"
                cPage = Alltrim(Str(m.nNextPage))
                .Parent.oHeader.oLblPages.Caption = m.cPage+"/"+Alltrim(Str(.Parent.oHeader.nNumPages))
            Endif
            *--- sicurezza per cambio cursore
            L_OldArea = Select()
            *--- Cambio pagina
            .ActivePage = m.nNextPage
            .Click()
            *--- sicurezza per cambio cursore
            Select(m.L_OldArea)
        Endwith
    Endproc

    Proc nTimeSwap_Assign(nValue)
        This.nTimeSwap = m.nValue
        This.oTimer.Interval = m.nValue + Iif(m.nValue>0,This.nTimeTransition,0)
        This.oTimer.Enabled = ( m.nValue<>0 )
    Endproc

    Proc Destroy()
        This.oTimer.Interval = 0
        This.oTimer.Enabled = .F.
    Endproc

    *-- Copiato da StdPageFrame - Inizio

    Procedure SetFont()
        Local oPage, l_i
        If i_VisualTheme=-1 Or i_cMenuTab="S"
            For l_i=1 To This.PageCount
                oPage = This.Pages[m.l_i]
                oPage.FontName = SetFontName('P', Thisform.FontName, Thisform.bGlobalFont)
                oPage.FontSize = SetFontSize('P', Thisform.FontSize, Thisform.bGlobalFont)
                oPage.FontItalic = SetFontItalic('P', Thisform.FontItalic, Thisform.bGlobalFont)
                oPage.FontBold = SetFontBold('P', Thisform.FontBold, Thisform.bGlobalFont)
            Endfor
        Endif
    Endproc

    Proc Click()
        *cp_dbg('click'+str(this.ActivePage,3,0)+str(this.PageCount,3,0))
        *thisform.lockscreen=.t.
        *--- Controllo cambio pagina, evita il rilancio del evento ActivatePage quando si preme ripetutamente sul tab
        If This.nPageActive <> This.ActivePage
            Local i_nZoomPage,i_nWarningPage
            i_nZoomPage=This.PageCount
            i_nWarningPage=0
            This.Parent.NotifyEvent('ActivatePage '+Ltrim(Str(This.ActivePage,2,0)))
            Do While Type('this.pages(i_nZoomPage).autozoom')='U' And i_nZoomPage>0
                i_nZoomPage=i_nZoomPage-1
            Enddo
            If Type('this.pages(this.PageCount).oWarning')<>'U'
                i_nWarningPage=This.PageCount
            Endif
            If This.ActivePage=i_nZoomPage
                This.Parent.ReleaseWarn()
                If This.nOldWz<>0
                    Thisform.LockScreen=.T.
                    This.nOldWs=Thisform.Width
                    This.nOldHs=Thisform.Height
                    Thisform.Width=This.nOldWz
                    Thisform.Height=This.nOldHz
                    Thisform.LockScreen=.F.
                Endif
                This.bInAutoZoom=.T.
                If This.Pages(i_nZoomPage).autozoom.Class='Commandbutton'
                    This.nOldWs=Thisform.Width
                    This.nOldHs=Thisform.Height
                    * --- c'era l' autozoom fasullo, quindi ci mette quello vero
                    This.Pages(i_nZoomPage).RemoveObject('autozoom')
                    This.Pages(i_nZoomPage).AddObject("autozoom","zoombox", This.Parent.bGlobalFont)
                    This.Pages(i_nZoomPage).autozoom.cKeyFields=This.Parent.cKeySelect
                    *--- Valorizzo cZoomOnZoom in modo che si valorizzi anche il campo "Ricerca" nelle propriet� dello zoom
                    This.Pages(i_nZoomPage).autozoom.cZoomOnZoom = Thisform.cPrg
                    If Empty(This.Parent.cAutoZoom)
                        This.Pages(i_nZoomPage).autozoom.SetFileAndFields(This.Parent.cFile,'*',This.Parent.cQueryFilter)
                    Else
                        This.Pages(i_nZoomPage).autozoom.SetFileAndFields(This.Parent.cFile,'*',This.Parent.cQueryFilter,This.Parent.cAutoZoom+'.'+This.Parent.cFile+'_VZM')
                    Endif
                    If i_VisualTheme <> -1  And Vartype(_Screen.TBMDI)<>"U" And Thisform.bTBMDI And _Screen.TBMDI.Visible
                        Thisform.Width=This.nOldWs
                        Thisform.Height=This.nOldHs
                    Endif
                    If i_AdvancedHeaderZoom
                        Local cObjManyHeaderName
                        cObjManyHeaderName="oManyHeader_"+This.Pages(i_nZoomPage).autozoom.Name
                        This.Pages(i_nZoomPage).AddObject(cObjManyHeaderName,"ManyHeader")
                        This.oRefHeaderObject=This.Pages(i_nZoomPage).&cObjManyHeaderName
                        This.Pages(i_nZoomPage).autozoom.oRefHeaderObject=This.Pages(i_nZoomPage).&cObjManyHeaderName
                        This.oRefHeaderObject.InitHeader(This.Pages(i_nZoomPage).autozoom.Grd)
                    Endif
                    This.Pages(i_nZoomPage).autozoom.Visible=.T.
                Endif
                This.Parent.nPageResizeContainer=This.ActivePage
                This.Parent.Resize()
                This.Parent.nPageResizeContainer=0
            Else
                If This.bInAutoZoom
                    This.bInAutoZoom=.F.
                    This.nOldWz=Thisform.Width
                    This.nOldHz=Thisform.Height
                    If This.nOldWs<>0
                        Thisform.LockScreen=.T.
                        Thisform.Width=This.nOldWs
                        Thisform.Height=This.nOldHs
                        Thisform.LockScreen=.F.
                    Endif
                    This.bAutoZoomLoad = Iif( Vartype(g_AutoZoomLoad)='L',g_AutoZoomLoad,This.bAutoZoomLoad)
                    If  This.bAutoZoomLoad
                        This.bAutoZoomLoad=.F.
                        * --- copia il cursore di zoom nel cursore del keyset
                        Local i_c,i_a,i,j
                        Dimension i_a[1]
                        Private i_err,i_errsav
                        i_err=.F.
                        i_errsav=On('ERROR')
                        On Error i_err=.T.
                        * ---- crea nuovo keyset
                        Select (This.Parent.cKeySet)
                        Afields(i_a)
                        For i=1 To Alen(i_a,1)
                            For j=7 To 16
                                i_a[i,j]='' && buco della afields(): se il cursore era un "filtro" inserisce i trigger della tabella principale
                            Next
                        Next
                        Use
                        Create Cursor (This.Parent.cKeySet) From Array i_a
                        * --- lo riempie
                        Select (This.Pages(i_nZoomPage).autozoom.cCursor)
                        i_c=This.Parent.cKeySelect
                        Dimension i_a[1]
                        Scatter Fields &i_c To i_a
                        Select (This.Parent.cKeySet)
                        Append From Array i_a
                        This.Parent.SetWorkFromKeySet()
                        If !i_err
                            This.Parent.LoadRecWarn()
                        Endif
                        On Error &i_errsav
                    Endif
                    * ---
                Else
                Endif
                If This.ActivePage=i_nWarningPage
                    This.Pages(This.ActivePage).oWarning.Resize()
                Endif
                This.Parent.Resize()
            Endif
            This.Parent.Refresh()
            * --- colori delle pagine
            *this.pages(this.activepage).forecolor=rgb(255,255,255)
            *this.pages(this.activepage).backcolor=rgb(192,192,192)
            *for i=1 to this.PageCount
            *  if i<>this.ActivePage
            *    this.pages(i).forecolor=rgb(255,255,0)
            *    this.pages(i).backcolor=rgb(128,128,128)
            *  endif
            *next
            *thisform.lockscreen=.f.  && togliere anche sopra!
            * ---
            *this.Pages(this.ActivePage).refresh()
            *this.Pages(this.ActivePage).Controls(1).SetFocus()
            *this.parent.oFirstControl.SetFocus()
            *this.parent.refresh()
            *--- Ho cambiato pagina
            This.nPageActive = This.ActivePage
        Endif

    Proc SetZoomSize()
        With This
            If Type('this.Pages(this.ActivePage).Autozoom')='O' And .Pages(.ActivePage).autozoom.Class='Zoombox'
                .Pages(.ActivePage).autozoom.Resize()
            Else
                If Type('this.Pages(this.ActivePage).oWarning')='O' And .Pages(.ActivePage).oWarning.Class='Postitcontainer'
                    .Pages(.ActivePage).oWarning.Resize()
                Endif
            Endif
        Endwith
    Endproc

    *-- Copiato da StdPageFrame - Fine
Enddefine


*--- Oggetto Header per Gadget
*--- cp_gripper
Define Class cp_HeaderMenuBtn As Shape
    Dimension aPolyPoints[12,2]
    Width=10
    Height=11
    Visible=.F.
    Procedure Init()
        With This
            Dimension .aPolyPoints[12,2]
            .aPolyPoints[1,1] = 1&&A
            .aPolyPoints[1,2] = 1

            .aPolyPoints[2,1] = 100&&B
            .aPolyPoints[2,2] = 1

            .aPolyPoints[3,1] = 100&&C
            .aPolyPoints[3,2] = 20

            .aPolyPoints[4,1] = 1&&D
            .aPolyPoints[4,2] = 20

            .aPolyPoints[5,1] = 1&&E
            .aPolyPoints[5,2] = 30

            .aPolyPoints[6,1] = 100&&F
            .aPolyPoints[6,2] = 30

            .aPolyPoints[7,1] = 100&&H
            .aPolyPoints[7,2] = 50

            .aPolyPoints[8,1] = 1&&I
            .aPolyPoints[8,2] = 50

            .aPolyPoints[9,1] = 1&&H
            .aPolyPoints[9,2] = 60

            .aPolyPoints[10,1] = 100&&I
            .aPolyPoints[10,2] = 60

            .aPolyPoints[11,1] = 100&&I
            .aPolyPoints[11,2] = 80
            .aPolyPoints[12,1] = 1&&I
            .aPolyPoints[12,2] = 80
            .Polypoints ="This.aPolyPoints"
            .BorderStyle=0
            .FillStyle=0
            .BorderColor=Rgb(255,255,255)
            .FillColor = Rgb(255,255,255)
            .Move(.Left, .Top, 10,11)
        Endwith
    Endproc
Enddefine

Define Class cp_HeaderPlusBtn As Shape
    Dimension aPolyPoints[12,2]
    Width=11
    Height=11
    Visible=.F.

    Procedure Init()
        This._Draw("M")
    Endproc

    Procedure _Draw(cType)
        With This
            If m.cType="P"  &&Plus
                Dimension .aPolyPoints[12,2]
                .aPolyPoints[1,1] = 40&&A
                .aPolyPoints[1,2] = 10

                .aPolyPoints[2,1] = 50&&B
                .aPolyPoints[2,2] = 10

                .aPolyPoints[3,1] = 50&&C
                .aPolyPoints[3,2] = 40

                .aPolyPoints[4,1] = 80&&D
                .aPolyPoints[4,2] = 40

                .aPolyPoints[5,1] = 80&&E
                .aPolyPoints[5,2] = 50

                .aPolyPoints[6,1] = 50&&F
                .aPolyPoints[6,2] = 50

                .aPolyPoints[7,1] = 50&&G
                .aPolyPoints[7,2] = 80

                .aPolyPoints[8,1] = 40&&H
                .aPolyPoints[8,2] = 80

                .aPolyPoints[9,1] = 40&&I
                .aPolyPoints[9,2] = 50

                .aPolyPoints[10,1] = 10&&L
                .aPolyPoints[10,2] = 50

                .aPolyPoints[11,1] = 10&&M
                .aPolyPoints[11,2] = 40

                .aPolyPoints[12,1] = 40&&N
                .aPolyPoints[12,2] = 40
                .ToolTipText = cp_Translate("Espandi")
            Else
                Dimension .aPolyPoints[4,2]
                .aPolyPoints[1,1] = 10&&M
                .aPolyPoints[1,2] = 40

                .aPolyPoints[2,1] = 80&&D
                .aPolyPoints[2,2] = 40

                .aPolyPoints[3,1] = 80&&E
                .aPolyPoints[3,2] = 50

                .aPolyPoints[4,1] = 10&&L
                .aPolyPoints[4,2] = 50
                .ToolTipText = cp_Translate("Riduci")
            Endif
            .Polypoints ="This.aPolyPoints"
            .BorderStyle=1
            .BackStyle=0
            .Move(.Left, .Top, 11,11)
        Endwith
    Endproc
Enddefine

*--- Triangolini per massimizzazione
Define Class cp_ShpTrngl As Shape
    Dimension aPolyPoints[3,2]
    cType = "U"
    Proc Init
        With This
            Do Case
                Case Empty(.cType) Or .cType=='U'
                    .aPolyPoints[1,1] = 0&&A
                    .aPolyPoints[1,2] = 100

                    .aPolyPoints[2,1] = 50&&B
                    .aPolyPoints[2,2] = 0

                    .aPolyPoints[3,1] = 100&&C
                    .aPolyPoints[3,2] = 100

                Case .cType=='D'
                    .aPolyPoints[1,1] = 0&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 50&&B
                    .aPolyPoints[2,2] = 100

                    .aPolyPoints[3,1] = 100&&C
                    .aPolyPoints[3,2] = 0

                Case .cType=='R'
                    .aPolyPoints[1,1] = 0&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 50

                    .aPolyPoints[3,1] = 0&&C
                    .aPolyPoints[3,2] = 100

                Case .cType=='L'
                    .aPolyPoints[1,1] = 100&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 100

                    .aPolyPoints[3,1] = 0&&C
                    .aPolyPoints[3,2] = 50

            	Case .cType=='UL'
                    .aPolyPoints[1,1] = 0&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 0

                    .aPolyPoints[3,1] = 0&&C
                    .aPolyPoints[3,2] = 100

                Case .cType=='UR'
                    .aPolyPoints[1,1] = 0&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 0

                    .aPolyPoints[3,1] = 100&&C
                    .aPolyPoints[3,2] = 100

                Case .cType=='DL'
                    .aPolyPoints[1,1] = 0&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 100

                    .aPolyPoints[3,1] = 0&&C
                    .aPolyPoints[3,2] = 100

                Case .cType=='DR'
                    .aPolyPoints[1,1] = 100&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 100

                    .aPolyPoints[3,1] = 0&&C
                    .aPolyPoints[3,2] = 100

            Endcase
            .Polypoints = "This.aPolyPoints"
            .BorderStyle = 1
        Endwith
    Endproc
Enddefine

Define Class cp_HeaderGadget As Container
    cEvent=''
    cTitle = ''
    nBackColor = -1
    bPlus = .T.
    bShowValue = .T.
    bRefresh = .T.
    nNumPages = 1

    bBinded = .F. &&Eseguo la BindEvent alla Calculate solo una volta
    nAlphaColor = Rgb(0,0,0)

    FontName="Arial"
    FontSize=12
    FontBold=.F.
    FontItalic=.F.

    Width = 120
    Height = 24
    BorderWidth = 0
    Anchor=10

    bUpper = .T.
    FontColor = Rgb(255,255,255)

    Add Object oBtnPlusMinus As cp_HeaderPlusBtn With bPlus = .T., Top=6, Left = 5, MousePointer=15, BorderColor=Rgb(255,255,255)
    Add Object oLblTitle As Label With Top=0, Left = 26, Width = 68, Anchor = 10, BackStyle=0, ForeColor=Rgb(255,255,255), Height=24
    Add Object oLblValue As Label With Top=0, Left = 26, Width = 68, Height=24, AutoSize = .T., Anchor = 10, BackStyle=0, ForeColor=Rgb(255,255,255), FontSize = 12
    Add Object oLblPrevPage As Label With Caption="<",Top=5, Left = 57, Width = 6, BackStyle=0, ForeColor=Rgb(255,255,255),Height=24, AutoSize=.T., Visible=.F., MousePointer=15, ToolTipText="Pagina precedente"
    Add Object oLblPages As Label With Top=5, Left = 63, Width = 25, BackStyle=0, ForeColor=Rgb(255,255,255),Height=24,AutoSize=.T., Visible=.F., Alignment=2
    Add Object oLblNextPage As Label With Caption=">",Top=5, Left = 88, Width = 6, BackStyle=0, ForeColor=Rgb(255,255,255),Height=24,AutoSize=.T., Visible=.F., MousePointer=15, ToolTipText="Pagina successiva"
    Add Object oBtnShape As Shape With Top=0, Left = 99, Width = 16, Height = 24, Visible = .F.
    Add Object oBtnUpdate As cp_HeaderMenuBtn With Top=7, Left = 99, MousePointer=15, ToolTipText="Opzioni"
	Add Object oULShape As cp_ShpTrngl With Visible=.f., cType='UL'
	Add Object oURShape As cp_ShpTrngl With Visible=.f., cType='UR'
	Add Object oDLShape As cp_ShpTrngl With Visible=.f., cType='DL'
	Add Object oDRShape As cp_ShpTrngl With Visible=.f., cType='DR'
	
    Procedure Init()
        Declare Integer SetCursor In WIN32API Integer
        Declare Integer LoadCursor In WIN32API Integer, Integer
        With This
            .oLblValue.Visible = .F.

            .bPlus = .bPlus
            .bRefresh = .bRefresh

            .oBtnShape.BackColor = Rgb(224,224,224)
            .oBtnShape.BorderColor = Rgb(224,224,224)
            .nBackColor = .nBackColor
            If .nNumPages>1
                *-- La freccia oLblPrevPage rimane invisibile perch� siamo a pagina 1
                .oLblNextPage.Visible=.T.
                .oLblPages.Visible=.T.
                .oLblPages.Caption = "1/"+Alltrim(Str(.nNumPages))
                .oLblNextPage.Left = .oLblNextPage.Left + Int(.nNumPages/10)*5
            Endif
        Endwith
    Endproc

    Proc Calculate(cNewTitle)
        This.cTitle = Alltrim(m.cNewTitle)
        If !Empty(This.cTitle)
            This.AdjustLabel()
        Endif
        If !This.bBinded
            Bindevent(Thisform.oPgFrm,"ActivePage",This,"SetPage",1)
            This.bBinded = .T.
        Endif
    Endproc

    *--- Event
    Proc Event(cEvent)
    Endproc

    Procedure FontColor_Assign(xValue)
        With This
            .FontColor = m.xValue
            .oLblTitle.ForeColor = m.xValue
            .oLblValue.ForeColor = m.xValue
            .oLblNextPage.ForeColor = m.xValue
            .oLblPages.ForeColor = m.xValue
            .oLblPrevPage.ForeColor = m.xValue
            .oBtnPlusMinus.BorderColor = m.xValue
            .oBtnUpdate.BorderColor = m.xValue
            .oBtnUpdate.FillColor = m.xValue
            .oULShape.BackColor = m.xValue
            .oULShape.BorderColor = m.xValue
        	.oURShape.BackColor = m.xValue
        	.oURShape.BorderColor = m.xValue
        	.oDLShape.BackColor = m.xValue
        	.oDLShape.BorderColor = m.xValue
        	.oDRShape.BackColor = m.xValue
        	.oDRShape.BorderColor = m.xValue
        Endwith
    Endproc

    Procedure FontName_Assign(xValue)
        With This
            .FontName = m.xValue
            .oLblTitle.FontName = m.xValue
            .oLblValue.FontName = m.xValue
            .oLblNextPage.FontName = m.xValue
            .oLblPages.FontName = m.xValue
            .oLblPrevPage.FontName = m.xValue
        Endwith
    Endproc

    Procedure FontSize_Assign(xValue)
        With This
            .FontSize=m.xValue
            .oLblTitle.FontSize = m.xValue
            .oLblValue.FontSize = m.xValue+1
            .oLblNextPage.FontSize = m.xValue
            .oLblPages.FontSize = m.xValue
            .oLblPrevPage.FontSize = m.xValue
        Endwith
    Endproc

    Procedure AdjustLabel()
        With This
            .oLblTitle.Anchor=0
            .oLblTitle.Height=0
            .oLblTitle.Visible = .T.
            .oLblTitle.AutoSize = .T.
            .oLblTitle.Caption = Iif(.bUpper, Upper(.cTitle), .cTitle)
            .oLblTitle.Visible = .F.
            If .oLblTitle.Height=0
                Thisform.Draw()
            Endif
            .oLblTitle.AutoSize = .F.
            .oLblTitle.Width = Max(Iif(.nNumPages>1, .oLblPrevPage.Left, .oBtnUpdate.Left) - .oLblTitle.Left - 5,0)
            .oLblTitle.Top = Bitrshift(Max(.Height - .oLblTitle.Height,0),1)
            Do case
            	Case .Parent.oAlert.Visible
            If .oBtnPlusMinus.Visible
            			.Parent.oAlert.Left = .oBtnPlusMinus.Left + .oBtnPlusMinus.Width + 3
            		Endif
            		.oLblTitle.Left = .Parent.oAlert.Left + .Parent.oAlert.Width + 3
            	Case .oBtnPlusMinus.Visible
                	.oLblTitle.Left = .oBtnPlusMinus.Left + .oBtnPlusMinus.Width + 3
            	Otherwise
                .oLblTitle.Left = 5
            Endcase
            *--- Visualizzazione del w_VALUE eventualmente presente sul gadget
            If .oLblValue.Visible
                Local l_Left
                .oLblValue.Anchor = 0
                *--- Calcolo la left della label in base a quanto gi� presente (la metto a destra prima di pagine e/o menu)
                Do Case
                    Case .nNumPages>1 && label per la gestioen delle pagine
                        m.l_Left = Max(.oLblPrevPage.Left - .oLblValue.Width - 5, 0)
                    Case .oBtnUpdate.Visible && label per il menu
                        m.l_Left = Max(.oBtnUpdate.Left - .oLblValue.Width - 5, 0)
                    Otherwise && nessuna label alla destra del titolo
                        m.l_Left = Max(.Width - .oLblValue.Width - 5, 0)
                Endcase
                .oLblValue.Move(m.l_Left, .Height*0.5-.oLblValue.Height*0.5)
                .oLblValue.Anchor = 8
                *--- accorcio la label del titolo per fare spazio alla label del valore
                .oLblTitle.Width = Max(.oLblTitle.Width - .oLblValue.Width - 1, 0)
            Endif
            *---
            Local l_title,nLeft
            m.l_title = .oLblTitle.Caption
            m.nLeft= Len(m.l_title)
            
            Do While m.nLeft>1 And .oLblTitle.Width<cp_LabelWidth2(m.l_title, .oLblTitle.FontName,.oLblTitle.FontSize)
                m.nLeft = m.nLeft-1
                m.l_title = Left(.oLblTitle.Caption,m.nLeft)+'...'
            Enddo
            If m.nLeft>1
                .oLblTitle.Caption = m.l_title
                .oLblTitle.Visible = .T.
            Else
                .oLblTitle.Visible = .F.
            Endif
            .oLblTitle.Anchor=10
        Endwith
    Endproc

    Proc AdjustLblPage()
        With This
            .oLblNextPage.Move(Max(0,  .oBtnUpdate.Left - .oLblNextPage.Width - 5),Bitrshift(Max(.Height - .oLblNextPage.Height, 0), 1))
            .oLblPages.Move(Max(0,.oLblNextPage.Left - .oLblPages.Width),Bitrshift(Max(.Height - .oLblPages.Height,0),1))
            .oLblPrevPage.Move(Max(0,.oLblPages.Left - .oLblPrevPage.Width),Bitrshift(Max(.Height - .oLblPrevPage.Height,0),1))
        Endwith
    Endproc

    Proc Resize()
        This.AdjustLblPage()
        This.AdjustLabel()
    Endproc

    Procedure FontBold_Assign(xValue)
        This.FontBold=m.xValue
        This.oLblTitle.FontBold=m.xValue
    Endproc

    Procedure FontItalic_Assign(xValue)
        This.FontItalic=m.xValue
        This.oLblTitle.FontItalic=m.xValue
        This.oLblValue.FontItalic=m.xValue
    Endproc

    Procedure nBackColor_Assign(xValue)
        Local nColor
        This.nBackColor = Iif(m.xValue=-1, RGBAlphaBlending(Thisform.nBackColor, This.nAlphaColor, 0.1),m.xValue)
        This.BackColor = This.nBackColor
    Endproc

	Procedure bUpper_Assign(xValue)
        With This
            If .bUpper<>m.xValue
            	.bUpper = m.xValue
            	If !Empty(.cTitle)
            	.AdjustLabel()
            Endif
            Endif
        Endwith
    Endproc

    Procedure bPlus_Assign(xValue)
        With This
            .bPlus = m.xValue
            .oBtnPlusMinus.Visible = m.xValue
            .AdjustLabel()
            .oBtnUpdate.Anchor = 0
            .oBtnUpdate.Left= .Width - 5 - .oBtnUpdate.Width
            .oBtnUpdate.Anchor = 8
            .oBtnShape.Anchor = 0
            .oBtnShape.Move( .oBtnUpdate.Left-5, 0, .Width-.oBtnUpdate.Left+5, .Height)
            .oBtnShape.Anchor = 8
        Endwith
    Endproc
		
	Procedure setMaximize(bSet)
        With This
            If m.bSet
	            .oULshape.Anchor = 0
	            .oULshape.Move(2,2,3,3)
	            .oULshape.Anchor = 3
	            .oULshape.Visible = .t.
	        	
	        	.oURshape.Anchor = 0
	            .oURshape.Move(.Width-5,2,3,3)
	            .oURshape.Anchor = 9
	            .oURshape.Visible = .t.
	            
	            .oDLshape.Anchor = 0
	            .oDLshape.Move(2,.Height-5,3,3)
	            .oDLshape.Anchor = 6
	            .oDLshape.Visible = .t.
	            
	            .oDRshape.Anchor = 0
	            .oDRshape.Move(.Width-5,.Height-5,3,3)
	            .oDRshape.Anchor = 12
	            .oDRshape.Visible = .t.
	    	Else
	    		.oULshape.Visible = .f.
	    		.oURshape.Visible = .f.
	    		.oDLshape.Visible = .f.
	    		.oDRshape.Visible = .f.
	    	Endif
        Endwith
    Endproc
	
    Proc bRefresh_Assign(xValue)
        This.oBtnUpdate.Visible = m.xValue
        This.AdjustLabel()
    Endproc

    Procedure DblClick()
        This.OnMaximize()
    Endproc

	*--- oLblTitle
    Procedure oLblTitle.DblClick()
        This.Parent.OnMaximize()
    Endproc

	Procedure oLblTitle.MouseEnter(nButton, nShift, nXCoord, nYCoord)
		Dodefault()
		If m.nButton = 0
			This.Parent.SetToolTipTimer()
		Endif
	Endproc
	
	Procedure oLblTitle.MouseLeave(nButton, nShift, nXCoord, nYCoord)
		Dodefault()
		This.Parent.ReleaseToolTipTimer()
	Endproc
	
	*--- oLblValue
    Procedure oLblValue.DblClick()
        This.Parent.OnMaximize()
    Endproc

	Procedure oLblValue.MouseEnter(nButton, nShift, nXCoord, nYCoord)
		Dodefault()
		If m.nButton = 0
			This.Parent.SetToolTipTimer()
		Endif
	Endproc
	
	Procedure oLblValue.MouseLeave(nButton, nShift, nXCoord, nYCoord)
		Dodefault()
		This.Parent.ReleaseToolTipTimer()
	Endproc

	*--- oBtnPlusMinus
    Proc oBtnPlusMinus.Click()
        This.Parent.PlusMinus_Click()
    Endproc

	Proc oBtnPlusMinus.MouseEnter(nButton, nShift, nXCoord, nYCoord)
        If m.nButton = 0
			This.Parent.SetToolTipTimer()
		Endif
    Endproc

    Proc oBtnPlusMinus.MouseLeave(nButton, nShift, nXCoord, nYCoord)
        This.Parent.ReleaseToolTipTimer()
    Endproc
	
    Proc OnMaximize()
        With Thisform
	        If .bMaximize And .bExpanded
				*--- Nascondo l'eventuale ToolTip
		        If Vartype(oGadgetManager)='O' And PemStatus(oGadgetManager,'oBalloon',5)
		        	oGadgetManager.oBalloon.ctlHide(2)
		        	oGadgetManager.oBalloon.ctlVisible = .F.
		        Endif
				This.ReleaseToolTipTimer() && chiudo il tooltip
				Local bMaximizing
				*--- Cambio il valore di nMaximizeState per evitare che la EventHandler interagisca
				*--- durante la minimizzazione per doppio click (causerebbe un'anomalia nel cambio pagina)
				m.bMaximizing = .nMaximizeState==0
				.nMaximizeState = Iif(.nMaximizeState==2, -1, .nMaximizeState)
				.OnMaximize(m.bMaximizing)
	        Endif
    	Endwith
    Endproc
    
    Proc PlusMinus_Click()
        With This.oBtnPlusMinus
            If Thisform.nMaximizeState==0
	            *--- Nascondo l'eventuale ToolTip
		        If Vartype(oGadgetManager)='O' And PemStatus(oGadgetManager,'oBalloon',5)
		        	oGadgetManager.oBalloon.ctlHide(2)
		        	oGadgetManager.oBalloon.ctlVisible = .F.
		        Endif
		        .Parent.ReleaseToolTipTimer()
	            .bPlus = !.bPlus
	            If .bPlus
	                ._Draw("M")
	                m.cEvent = " Expanded"
	                Thisform.NotifyEvent(Lower(.Parent.Name)+ m.cEvent + ' Init')
	                *--- Se serve nascondo il w_VALUE
	                If .Parent.bShowValue And .Parent.oLblValue.Visible
	                    .Parent.oLblValue.Visible = .F.
	                    This.AdjustLabel()
	                Endif
	            Else
	                ._Draw("P")
	                m.cEvent = " Collapsed"
	                Thisform.NotifyEvent(Lower(.Parent.Name)+ m.cEvent + ' Init')
	            	*--- mostro il w_VALUE nell'header se presente sul gadget
	                If .Parent.bShowValue
	                	Do case
	                		Case Pemstatus(Thisform,'w_PERCENTVALUE',5)
	                		*-- Nelle bar si usa il valore visualizzato dall'oggetto
							        .Parent.oLblValue.Caption = '['+Trans(Thisform.w_PERCENTVALUE)+']'	                		
	                		Case Pemstatus(Thisform,'w_VALUE',5) And !Empty(Thisform.w_VALUE)
	                    .Parent.oLblValue.Caption = '['+Trans(Thisform.w_VALUE)+']'
	                    	Otherwise
	                    		.Parent.oLblValue.Caption = ""
	                    Endcase
	                    .Parent.oLblValue.Visible = !Empty(.Parent.oLblValue.Caption)
	                    This.AdjustLabel()
	                Endif
	            Endif
	            Thisform.bExpanded=.bPlus
	            Thisform.NotifyEvent(Lower(.Parent.Name)+ m.cEvent + ' End')
        	Endif
        Endwith
    Endproc

	*--- oBtnUpdate
    Proc oBtnUpdate.Click()
        Local nLeft, nTop
        *--- Nascondo l'eventuale ToolTip
        If Vartype(oGadgetManager)='O' And PemStatus(oGadgetManager,'oBalloon',5)
        	oGadgetManager.oBalloon.ctlHide(2)
        	oGadgetManager.oBalloon.ctlVisible = .F.
        Endif
        This.Parent.ReleaseToolTipTimer()
        nLeft = cb_GetWindowLeft(Thisform.HWnd)
        nTop = cb_GetWindowTop(Thisform.HWnd)
        nLeft = m.nLeft + This.Left-5
        nTop = m.nTop + This.Parent.Height - 1
        This.Parent.oBtnShape.Visible = .T.
        This.Parent.CreateMenu(m.nLeft, m.nTop)
        This.Parent.oBtnShape.Visible = .F.
    Endproc

	Proc oBtnUpdate.MouseEnter(nButton, nShift, nXCoord, nYCoord)
        If m.nButton = 0
			This.Parent.SetToolTipTimer()
		Endif
    Endproc

    Proc oBtnUpdate.MouseLeave(nButton, nShift, nXCoord, nYCoord)
        This.Parent.ReleaseToolTipTimer()
    Endproc

	*--- oLblPrevPage
	Proc oLblPrevPage.Click()
        Local cPage, nPrevPage
        *--- Nascondo l'eventuale ToolTip
        If Vartype(oGadgetManager)='O' And PemStatus(oGadgetManager,'oBalloon',5)
        	oGadgetManager.oBalloon.ctlHide(2)
        	oGadgetManager.oBalloon.ctlVisible = .F.
        Endif
        This.Parent.ReleaseToolTipTimer()
        nPrevPage = Max(Thisform.oPgFrm.ActivePage-1,1)
        Thisform.oPgFrm.ActivePage = m.nPrevPage
    Endproc
    
    Proc oLblPrevPage.MouseEnter(nButton, nShift, nXCoord, nYCoord)
        This.BackStyle = 1
        This.BackColor = This.Parent.FontColor
        This.ForeColor = This.Parent.BackColor
        If m.nButton = 0
			This.Parent.SetToolTipTimer()
		Endif
    Endproc

    Proc oLblPrevPage.MouseLeave(nButton, nShift, nXCoord, nYCoord)
        This.BackStyle= 0
        This.ForeColor = This.Parent.FontColor
        This.Parent.ReleaseToolTipTimer()
    Endproc

	*--- oLblPges
	Procedure oLblPages.MouseLeave
		Lparameters nButton, nShift, nXCoord, nYCoord
		Dodefault()
		This.Parent.ReleaseToolTipTimer()
	Endproc

	*--- oLblNextPage
    Proc oLblNextPage.Click()
        Local cPage, nNextPage
        *--- Nascondo l'eventuale ToolTip
        If Vartype(oGadgetManager)='O' And PemStatus(oGadgetManager,'oBalloon',5)
        	oGadgetManager.oBalloon.ctlHide(2)
        	oGadgetManager.oBalloon.ctlVisible = .F.
        Endif
        This.Parent.ReleaseToolTipTimer()
        nNextPage = Min(Thisform.oPgFrm.ActivePage+1,This.Parent.nNumPages)
        Thisform.oPgFrm.ActivePage = m.nNextPage
    Endproc

    Proc oLblNextPage.MouseEnter(nButton, nShift, nXCoord, nYCoord)
        This.BackStyle = 1
        This.BackColor = This.Parent.FontColor
        This.ForeColor = This.Parent.BackColor
        If m.nButton = 0
			This.Parent.SetToolTipTimer()
		Endif
    Endproc

    Proc oLblNextPage.MouseLeave(nButton, nShift, nXCoord, nYCoord)
        This.BackStyle= 0
        This.ForeColor = This.Parent.FontColor
        This.Parent.ReleaseToolTipTimer()
    Endproc
	
	Procedure MouseLeave(nButton, nShift, nXCoord, nYCoord)
		Dodefault()
		This.ReleaseToolTipTimer()
	Endproc
	
	Procedure SetToolTipTimer
		*--- Attivo il timer per la visualizzazione del tooltip
		If Vartype(This.Parent.oToolTipTimer)='O'
			This.Parent.oToolTipTimer.Enabled = .T.
		Else
			This.Parent.oToolTipTimer = Createobject("ToolTipGadgetTimer")
		Endif
	Endproc
    
    Procedure ReleaseToolTipTimer
		*--- Disattivo il timer per la visualizzazione del tooltip
		If Vartype(This.Parent.oToolTipTimer)='O'
			This.Parent.oToolTipTimer.Enabled = .F.
		Endif
		This.Parent.oToolTipTimer = .null.
		
    Endproc

    Procedure SetPage()
        With This
            Local cPage
            cPage = Transform(Thisform.oPgFrm.ActivePage)
            .oLblPages.Caption = m.cPage+"/"+Transform(.nNumPages)
            .oLblPrevPage.Visible = (Thisform.oPgFrm.ActivePage <> 1)
            .oLblNextPage.Visible = (Thisform.oPgFrm.ActivePage <> .nNumPages)
        Endwith
    Endproc

    Proc CreateMenu(l_x, l_y)
        With This.Parent
            Local loPopupMenu, BeginGroup,loMenuItem
            Public i_PopupCurForm
            *-- Assegno la ThisForm perch� dal Popup non � visibile
            i_PopupCurForm = Thisform
            loPopupMenu = Createobject("cbPopupMenu")
            If Not Empty(.cContextMenu)
                vu_context(.cContextMenu, m.loPopupMenu)
                BeginGroup=.T.
            Endif

            *-- Voci di men� del gadget
            i_PopupCurForm.AddMenu(m.loPopupMenu)

            *--- Aggiorna
            loMenuItem = m.loPopupMenu.AddMenuItem()
            loMenuItem.Caption = cp_Translate("Aggiorna")
            loMenuItem.BeginGroup = m.BeginGroup
            loMenuItem.Visible = .T.
            loMenuItem.OnClick = "i_PopupCurForm.NotifyEvent('oheader RefreshOnDemand')"
            
            *--- Massimizza
            If Thisform.bMaximize And Thisform.bExpanded
                loMenuItem = m.loPopupMenu.AddMenuItem()
                loMenuItem.Caption = cp_Translate(Iif(Thisform.nMaximizeState=0, "Massimizza", "Minimizza"))
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = "i_PopupCurForm.onMaximize(i_PopupCurForm.nMaximizeState==0)"
            Endif

            *--- Vai a...
            If Pemstatus(Thisform, "w_PROGRAM", 5) And !Empty(Thisform.w_PROGRAM) And (!Pemstatus(Thisform, "w_NOGOTOMENU", 5) Or !Thisform.w_NOGOTOMENU)
                Local l_Cmd
                loMenuItem = m.loPopupMenu.AddMenuItem()
                loMenuItem.Caption = Iif(Pemstatus(Thisform, "w_PRGDESCRI", 5) And !Empty(Thisform.w_PRGDESCRI),Thisform.w_PRGDESCRI, cp_Translate('Vai a...'))
                loMenuItem.Visible = .T.
                m.l_Cmd = "l_oldError = On('Error')"
                m.l_Cmd = ah_MsgFormat("%1%0%2", m.l_Cmd, "On Error frm"+Alltrim(ThisForm.Name)+".cAlertMsg = Message()")
                m.l_Cmd = ah_MsgFormat("%1%0%2", m.l_Cmd, "Eval(["+Thisform.w_PROGRAM+"])")
                m.l_Cmd = ah_MsgFormat("%1%0%2", m.l_Cmd, "On Error &l_oldError")
                loMenuItem.OnClick = m.l_Cmd
            Endif

            *--- Blocca/sblocca scorrimento automatico
            If Thisform.nPgFrmTimeSwap#0
                loMenuItem = m.loPopupMenu.AddMenuItem()
                loMenuItem.Caption = cp_Translate('Cambio pagina automatico')
                loMenuItem.MarkFor = "i_PopupCurForm.bAutoPageSwap"
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = "i_PopupCurForm.bAutoPageSwap=!i_PopupCurForm.bAutoPageSwap"
            Endif

            *--- Modifica
            loMenuItem = m.loPopupMenu.AddMenuItem()
            loMenuItem.Caption = cp_Translate('Modifica/Sposta')
            loMenuItem.BeginGroup = .T.
            loMenuItem.Visible = .T.
            loMenuItem.OnClick = "oGadgetManager.oCharmBar.OnModify()"

			*--- Visualizza opzioni
            If Thisform.bOptions And Thisform.bOptionsAccess
	            loMenuItem = m.loPopupMenu.AddMenuItem()
	            loMenuItem.Caption = cp_Translate('Visualizza opzioni')
	            loMenuItem.BeginGroup = .T.
	            loMenuItem.Visible = .F.
	            loMenuItem.OnClick = "frm"+Alltrim(Thisform.Name)+".NotifyEvent([GadgetOption])"
            Endif

            *--- Info
            loMenuItem = m.loPopupMenu.AddMenuItem()
            loMenuItem.Caption = cp_Translate('Info')
            loMenuItem.BeginGroup = !(Thisform.bOptions And Thisform.bOptionsAccess)
            loMenuItem.Visible = .T.
            loMenuItem.OnClick = "ah_ErrorMsg(frm"+Alltrim(Thisform.Name)+".cToolTipGadget,[i])"

            loPopupMenu.InitMenu(.T.)
            loPopupMenu.ShowMenu(l_x, l_y)

            *--- Se ho cliccato fuori dal menu faccio ripartire eventualmente il time dello swap pagine
            If !Thisform.oParentObject.oCntResizer.Visible	&&Non sono in modifica dei gadget
                Thisform.bAutoPageSwap=Thisform.bAutoPageSwap
            Endif
            *--- Se ho cliccato fuori dal menu spengo MouseHover
            Thisform.oParentObject.oHighLightGadget.Visible=.F.

            *--- Release public var
            Release i_PopupCurForm
        Endwith
    Endproc
Enddefine
*--- Zucchetti Aulla - Fine

*--- Zucchetti Aulla - Inizio
*--- Timer per la visualizzazione della descrizione del Gadget nel tooltip
Define Class ToolTipGadgetTimer As Timer
    Enabled = .F.
	Interval = 100
	
	nTmrInterval = 100
    nLoop = 0
    nXMouse = 0
    nYMouse = 0
    cShowTipName = ""

    Proc Init
        If Vartype(oGadgetManager)='O'
	        This.nXMouse = Mcol(oGadgetManager.Name,3)
	        This.nYMouse = Mrow(oGadgetManager.Name,3)
	        This.Enabled=.T.
	        This.Interval = This.nTmrInterval
        Endif
    Endproc

	*--- Se scatta 5 volte senza che venga mosso il mouse mostro il tooltip e disattivo il timer
    Proc Timer()
        With This
        	*--- Verifico che l'interfaccia sia effettivamente aperta
	        If Vartype(oGadgetManager)='O' And PemStatus(oGadgetManager,"oBalloon",5)
	        	Local l_points, nTmrX, nTmrY, l_Title, l_Text, l_Name
	        	m.l_points = Replicate(Chr(0), 8)
    			GetCursorPos(@l_points)
    			ScreenToClient(_Screen.hwnd,@l_points)
    			m.nTmrX = CToBin(Substr(l_points,1,4), "4RS")
    			m.nTmrY = CToBin(Substr(l_points,5,4), "4RS")
	        	Do Case						
	        		*--- il mouse non si � mosso per un secondo: mostro il tooltip
	        		Case .nLoop=5
	        			Local l_FocusObj
	        			m.l_FocusObj = Sys(1270)
	        			If Vartype(m.l_FocusObj)='O' And PemStatus(m.l_FocusObj,"ToolTipText",5) And .nXMouse==m.nTmrX And .nYMouse==m.nTmrY;
	        				And (Empty(.cShowTipName) Or m.l_FocusObj.Name<>.cShowTipName)
		        			oGadgetManager.oBalloon.ctlHide(2)
							oGadgetManager.oBalloon.ctlVisible = .F.
			    			*--- Allineamento del tooltip in base al "quadrante" dello screen
			    			m.l_points = Replicate(Chr(0), 8)
			    			Do case
			    				Case m.nTmrX<=(_Screen.Width*0.5) And m.nTmrY<=(_Screen.Height*0.5) && top-sx
			    					oGadgetManager.oBalloon.ctlAlignment = 3
			    				Case m.nTmrX<=(_Screen.Width*0.5) And m.nTmrY>(_Screen.Height*0.5) && bottom-sx
			    					oGadgetManager.oBalloon.ctlAlignment = 5
			    				Case m.nTmrX>(_Screen.Width*0.5) And m.nTmrY<=(_Screen.Height*0.5) && top-dx
			    					oGadgetManager.oBalloon.ctlAlignment = 7
			    				Case m.nTmrX>(_Screen.Width*0.5) And m.nTmrY>(_Screen.Height*0.5) && bottom-dx
			    					oGadgetManager.oBalloon.ctlAlignment = 4
			    			Endcase
			    			*--- Se sono sulla label del titolo o del valore devo fare il tooltip dell'intero gadget
			    			If Upper(m.l_FocusObj.Parent.Class)=="CP_HEADERGADGET" And (Upper(m.l_FocusObj.Name)=="OLBLTITLE" Or Upper(m.l_FocusObj.Name)=="OLBLVALUE")
			    				m.l_Title = Alltrim(m.l_FocusObj.Parent.cTitle)
			    				m.l_Text = Alltrim(m.l_FocusObj.Parent.Parent.cToolTipGadget)
			    				m.l_Name = 'frm'+Alltrim(m.l_FocusObj.Parent.Parent.Name)
			    				If Len(m.l_Text)>150
			    					oGadgetManager.oBalloon.ctlShow(6 , Left(m.l_Text,150)+'...'+oGadgetManager.oBalloon.ctlMakeLink(cp_Translate('Altro'), 'ah_ErrorMsg('+m.l_Name+'.cToolTipGadget,[i])'), m.l_Title,0)
			    				Else
			    					oGadgetManager.oBalloon.ctlShow(6 , m.l_Text, m.l_Title,0)
			    				Endif
			    			Else
			    				oGadgetManager.oBalloon.ctlShow(6 , m.l_FocusObj.ToolTipText , "",0)
			    			Endif
		    				.cShowTipName = m.l_FocusObj.Name
		    				.Enabled = .F.
		    			Else
		    				.nXMouse = m.nTmrX
			        		.nYMouse = m.nTmrY
			        		.nLoop=0
		    			Endif
	        		 *--- il mouse � ancora fermo
	        		Case .nXMouse==m.nTmrX And .nYMouse==m.nTmrY
	        			.nLoop = .nLoop + 1 
	        		*--- il mouse � stato mosso
	        		Otherwise
	        			.nXMouse = m.nTmrX
		        		.nYMouse = m.nTmrY
		        		.nLoop=0
	        	Endcase
        		*--- Se il debug � attivo aumento l'intervallo del timer
		        If Wexist("Debugger")
		            .Interval = 120000
		            Return
		        Else
		            This.Interval = .nTmrInterval
		        Endif
        	Endif
    	Endwith
    Endproc

	Proc Enabled_Assign(xValue)
		If Vartype(m.xValue)=='L' And This.Enabled<>m.xValue
			This.Enabled = m.xValue
			This.nLoop = 0
		Endif
	Endproc
Enddefine
*--- Zucchetti Aulla - Fine

*--- Zucchetti Aulla - Inizio
*--- Triangolini per massimizzazione
Define Class cp_AlertGadget As cp_showImage
	
    Width=22
    Height=22
    Default = "bmp\gd_alert.bmp"
    ToolTipText = ""
    BackColor = Rgb(255,255,255)
    BackStyle=1
    
    
    Procedure ToolTipText_Assing(xValue)
    	If Vartype(m.xValue)='C' And !(Alltrim(This.ToolTipText)==Alltrim(m.xValue))
    		This.ToolTipText = cp_Translate(Alltrim(m.xValue))
    	Endif
    Endproc
    
    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        *--- Attivo il timer per la visualizzazione del tooltip
		If Vartype(This.Parent.oToolTipTimer)='O'
			This.Parent.oToolTipTimer.Enabled = .T.
		Else
			This.Parent.oToolTipTimer = Createobject("ToolTipGadgetTimer")
		Endif
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        *--- Disattivo il timer per la visualizzazione del tooltip
		If Vartype(This.Parent.oToolTipTimer)='O'
			This.Parent.oToolTipTimer.Enabled = .F.
		Endif
		This.Parent.oToolTipTimer = .null.
    Endproc

	Procedure Click
        *--- Disattivo il timer per la visualizzazione del tooltip
		If Vartype(This.Parent.oToolTipTimer)='O'
			*--- Nascondo l'eventuale ToolTip
	        If Vartype(oGadgetManager)='O' And PemStatus(oGadgetManager,'oBalloon',5)
	        	oGadgetManager.oBalloon.ctlHide(2)
	        	oGadgetManager.oBalloon.ctlVisible = .F.
	        Endif
			This.Parent.oToolTipTimer.Enabled = .F.
		Endif
		This.Parent.oToolTipTimer = .null.
    	*--- Mostro il contenuto dell'alert in una MessageBox
    	If !Empty(This.ToolTipText)
    		ah_ErrorMsg(This.ToolTipText)
    	Endif
    Endproc
Enddefine
*--- Zucchetti Aulla - Fine

*--- Zucchetti Aulla - Fine
** Attiva/disattiva decorator
** In alcuni casi bisogna disattivare il decorator (l esportazione su word dalle offerte v� in errore nel caso in cui � atttivo il deocrator)
Procedure DecoratorState 
Lparameters stato
LOCAL numform, totform
*stato='A' Attiva il decorator
*stato='D' Attiva il decorator
numform=1
totform=_Screen.FormCount
Do While numform<=totform
	IF TYPE("_Screen.Forms(numform).oDec")='O'
		IF stato='A'
			Bindevent(_Screen.Forms(numform).oDec.hWindow , WM_NCHITTEST, _Screen.Forms(numform).oDec, 'NCHitTestCallback', 6)
			If _Screen.Forms(numform).oDec.hChildWindow<>0
                Bindevent(_Screen.Forms(numform).oDec.hChildWindow, WM_NCHITTEST, _Screen.Forms(numform).oDec, 'NCHitTestChildCallback', 6)
            Endif
		ELSE
			Unbindevent(_Screen.Forms(numform).oDec.hWindow, WM_NCHITTEST)
			If _Screen.Forms(numform).oDec.hChildWindow<>0
                Unbindevent(_Screen.Forms(numform).oDec.hChildWindow, WM_NCHITTEST)
            Endif
		ENDIF
	ENDIF
	numform=numform + 1
ENDDO
Endproc
*--- Zucchetti Aulla - Fine
