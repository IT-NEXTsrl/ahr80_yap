* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_mngserver                                                    *
*              Configurazione Server                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-10-11                                                      *
* Last revis.: 2015-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_mngserver",oParentObject))

* --- Class definition
define class tcp_mngserver as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 358
  Height = 220
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-05-12"
  HelpContextID=101924043
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_mngserver"
  cComment = "Configurazione Server"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ADDSRV = .F.
  w_NAME = space(10)
  w_DESCRI = space(200)
  w_CHECKODBC = space(1)
  w_TEXTODBC = space(254)
  w_ODBC = space(30)
  w_TYPE = space(30)
  w_CONNECT = space(1)
  o_CONNECT = space(1)
  w_POSTIT = space(1)
  w_SERVER = space(10)
  * --- Area Manuale = Declare Variables
  * --- cp_mngserver
  dimension aDataSources[1,2]
  oODBC = .Null.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_mngserverPag1","cp_mngserver",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNAME_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_mngserver
    thisform.backcolor=RGB(192,192,192)
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        cp_mngdatabase_b(this,"Srv Ok")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ADDSRV=.f.
      .w_NAME=space(10)
      .w_DESCRI=space(200)
      .w_CHECKODBC=space(1)
      .w_TEXTODBC=space(254)
      .w_ODBC=space(30)
      .w_TYPE=space(30)
      .w_CONNECT=space(1)
      .w_POSTIT=space(1)
      .w_SERVER=space(10)
      .w_ADDSRV=oParentObject.w_ADDSRV
      .w_SERVER=oParentObject.w_SERVER
          .DoRTCalc(1,1,.f.)
        .w_NAME = ' '
        .w_DESCRI = ' '
          .DoRTCalc(4,7,.f.)
        .w_CONNECT = 'S'
        .w_POSTIT = 'S'
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_NAME))
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate(cp_translate(MSG_DESCR_F))
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_ODBC))
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate(cp_translate(MSG_TYPE))
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate(cp_Translate(MSG_IT_CONNECTS_ON_PROGRAM_START))
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate(cp_Translate(MSG_IT_MANAGES_POSTIN))
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate(cp_Translate(MSG_IT_DSN_LESS))
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
    endwith
    this.DoRTCalc(10,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_mngserver
    this.oODBC = this.GetCtrl("w_ODBC", 1)
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ADDSRV=.w_ADDSRV
      .oParentObject.w_SERVER=.w_SERVER
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_NAME))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(cp_translate(MSG_DESCR_F))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_ODBC))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(cp_translate(MSG_TYPE))
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(cp_Translate(MSG_IT_CONNECTS_ON_PROGRAM_START))
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(cp_Translate(MSG_IT_MANAGES_POSTIN))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(cp_Translate(MSG_IT_DSN_LESS))
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_NAME))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(cp_translate(MSG_DESCR_F))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_ODBC))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(cp_translate(MSG_TYPE))
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(cp_Translate(MSG_IT_CONNECTS_ON_PROGRAM_START))
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(cp_Translate(MSG_IT_MANAGES_POSTIN))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(cp_Translate(MSG_IT_DSN_LESS))
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
    endwith
  return

  proc Calculate_VFZLZFCNHX()
    with this
          * --- Init
          cp_mngdatabase_b(this;
              ,'Srv Init';
             )
    endwith
  endproc
  proc Calculate_ELFCFZKVFD()
    with this
          * --- Done
          .oODBC = .Null.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNAME_1_4.enabled = this.oPgFrm.Page1.oPag.oNAME_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTEXTODBC_1_10.visible=!this.oPgFrm.Page1.oPag.oTEXTODBC_1_10.mHide()
    this.oPgFrm.Page1.oPag.oODBC_1_11.visible=!this.oPgFrm.Page1.oPag.oODBC_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_VFZLZFCNHX()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_ELFCFZKVFD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNAME_1_4.value==this.w_NAME)
      this.oPgFrm.Page1.oPag.oNAME_1_4.value=this.w_NAME
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_5.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_5.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCHECKODBC_1_6.RadioValue()==this.w_CHECKODBC)
      this.oPgFrm.Page1.oPag.oCHECKODBC_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTEXTODBC_1_10.value==this.w_TEXTODBC)
      this.oPgFrm.Page1.oPag.oTEXTODBC_1_10.value=this.w_TEXTODBC
    endif
    if not(this.oPgFrm.Page1.oPag.oODBC_1_11.RadioValue()==this.w_ODBC)
      this.oPgFrm.Page1.oPag.oODBC_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTYPE_1_12.RadioValue()==this.w_TYPE)
      this.oPgFrm.Page1.oPag.oTYPE_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONNECT_1_14.RadioValue()==this.w_CONNECT)
      this.oPgFrm.Page1.oPag.oCONNECT_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOSTIT_1_16.RadioValue()==this.w_POSTIT)
      this.oPgFrm.Page1.oPag.oPOSTIT_1_16.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CONNECT = this.w_CONNECT
    return

enddefine

* --- Define pages as container
define class tcp_mngserverPag1 as StdContainer
  Width  = 354
  height = 220
  stdWidth  = 354
  stdheight = 220
  resizeXpos=170
  resizeYpos=181
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNAME_1_4 as StdField with uid="KRFEJTCKYG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_NAME", cQueryName = "NAME",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 265567366,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=12, Top=25, InputMask=replicate('X',10)

  func oNAME_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ADDSRV)
    endwith
   endif
  endfunc

  add object oDESCRI_1_5 as StdField with uid="SVOUQQSAUZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 59511192,;
   bGlobalFont=.t.,;
    Height=21, Width=217, Left=121, Top=25, InputMask=replicate('X',200)

  add object oCHECKODBC_1_6 as StdCheck with uid="EGRWNHYJRU",rtseq=4,rtrep=.f.,left=12, top=56, caption="                                                  ",;
    HelpContextID = 183654184,;
    cFormVar="w_CHECKODBC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHECKODBC_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCHECKODBC_1_6.GetRadio()
    this.Parent.oContained.w_CHECKODBC = this.RadioValue()
    return .t.
  endfunc

  func oCHECKODBC_1_6.SetRadio()
    this.Parent.oContained.w_CHECKODBC=trim(this.Parent.oContained.w_CHECKODBC)
    this.value = ;
      iif(this.Parent.oContained.w_CHECKODBC=='S',1,;
      0)
  endfunc

  add object oTEXTODBC_1_10 as StdField with uid="QJMTJKBZTH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TEXTODBC", cQueryName = "TEXTODBC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 187487111,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=12, Top=99, InputMask=replicate('X',254)

  func oTEXTODBC_1_10.mHide()
    with this.Parent.oContained
      return (.w_CHECKODBC<>'S')
    endwith
  endfunc


  add object oODBC_1_11 as stdcmbDataSource with uid="RJIVASHABF",rtseq=6,rtrep=.f.,left=12,top=99,width=153,height=22;
    , HelpContextID = 54248312;
    , cFormVar="w_ODBC",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(30);
  , bGlobalFont=.t.


  func oODBC_1_11.mHide()
    with this.Parent.oContained
      return (.w_CHECKODBC='S')
    endwith
  endfunc


  add object oTYPE_1_12 as StdCombo with uid="SFVTBXSIXT",rtseq=7,rtrep=.f.,left=185,top=99,width=153,height=22;
    , ToolTipText = "Tipo database di destinazione";
    , HelpContextID = 143377274;
    , cFormVar="w_TYPE",RowSource=""+"SQLServer,"+"Oracle,"+"PostgreSQL,"+"MySQL,"+"FoxPro (DBC),"+"Access,"+"DB2,"+"Adabas,"+"SAPDB,"+"Informix", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTYPE_1_12.RadioValue()
    return(iif(this.value =1,'SQLServer',;
    iif(this.value =2,'Oracle',;
    iif(this.value =3,'PostgreSQL',;
    iif(this.value =4,'MySQL',;
    iif(this.value =5,'VFP',;
    iif(this.value =6,'Access',;
    iif(this.value =7,'DB2',;
    iif(this.value =8,'Adabas',;
    iif(this.value =9,'SAPDB',;
    iif(this.value =10,'Informix',;
    space(30))))))))))))
  endfunc
  func oTYPE_1_12.GetRadio()
    this.Parent.oContained.w_TYPE = this.RadioValue()
    return .t.
  endfunc

  func oTYPE_1_12.SetRadio()
    this.Parent.oContained.w_TYPE=trim(this.Parent.oContained.w_TYPE)
    this.value = ;
      iif(this.Parent.oContained.w_TYPE=='SQLServer',1,;
      iif(this.Parent.oContained.w_TYPE=='Oracle',2,;
      iif(this.Parent.oContained.w_TYPE=='PostgreSQL',3,;
      iif(this.Parent.oContained.w_TYPE=='MySQL',4,;
      iif(this.Parent.oContained.w_TYPE=='VFP',5,;
      iif(this.Parent.oContained.w_TYPE=='Access',6,;
      iif(this.Parent.oContained.w_TYPE=='DB2',7,;
      iif(this.Parent.oContained.w_TYPE=='Adabas',8,;
      iif(this.Parent.oContained.w_TYPE=='SAPDB',9,;
      iif(this.Parent.oContained.w_TYPE=='Informix',10,;
      0))))))))))
  endfunc

  proc oTYPE_1_12.mAfter
    with this.Parent.oContained
      this.Parent.oContained.oODBC.Populate(.w_TYPE)
    endwith
  endproc

  add object oCONNECT_1_14 as StdCheck with uid="HCLYPQOQCX",rtseq=8,rtrep=.f.,left=12, top=138, caption="                                              ",;
    HelpContextID = 41858349,;
    cFormVar="w_CONNECT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONNECT_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCONNECT_1_14.GetRadio()
    this.Parent.oContained.w_CONNECT = this.RadioValue()
    return .t.
  endfunc

  func oCONNECT_1_14.SetRadio()
    this.Parent.oContained.w_CONNECT=trim(this.Parent.oContained.w_CONNECT)
    this.value = ;
      iif(this.Parent.oContained.w_CONNECT=='S',1,;
      0)
  endfunc

  add object oPOSTIT_1_16 as StdCheck with uid="IOGTBHHBNG",rtseq=9,rtrep=.f.,left=12, top=161, caption="                            ",;
    HelpContextID = 239868953,;
    cFormVar="w_POSTIT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOSTIT_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPOSTIT_1_16.GetRadio()
    this.Parent.oContained.w_POSTIT = this.RadioValue()
    return .t.
  endfunc

  func oPOSTIT_1_16.SetRadio()
    this.Parent.oContained.w_POSTIT=trim(this.Parent.oContained.w_POSTIT)
    this.value = ;
      iif(this.Parent.oContained.w_POSTIT=='S',1,;
      0)
  endfunc


  add object oBtn_1_18 as StdButton with uid="ELSFVWSBLF",left=197, top=187, width=70,height=25,;
    caption="Ok", nPag=1;
    , HelpContextID = 171688757;
    , Caption=MSG_OK_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="WQUVOQPINF",left=275, top=187, width=70,height=25,;
    caption="Annulla", nPag=1;
    , HelpContextID = 207767146;
    , Caption=MSG_CANCEL_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_20 as cp_setCtrlObjprop with uid="STMQUKDDXO",left=372, top=-22, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nome",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 247710262


  add object oObj_1_21 as cp_setCtrlObjprop with uid="JSGEXNVRKN",left=372, top=4, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 247710262


  add object oObj_1_22 as cp_setCtrlObjprop with uid="SCTQHMIYEX",left=372, top=30, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="ODBC",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 247710262


  add object oObj_1_23 as cp_setCtrlObjprop with uid="VLHZKBYKFX",left=372, top=56, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tipo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 247710262


  add object oObj_1_24 as cp_setCtrlObjprop with uid="VMLHLREVJM",left=460, top=-22, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Connette a inizio programma",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 247710262


  add object oObj_1_25 as cp_setCtrlObjprop with uid="CFRVFCVMDG",left=460, top=4, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Gestisce Post-IN",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 247710262


  add object oObj_1_26 as cp_setCtrlObjprop with uid="PIUPGMWPVN",left=460, top=30, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Usa una stringa di connessione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 247710262


  add object oObj_1_28 as cp_runprogram with uid="BYZBNUAOYQ",left=374, top=122, width=167,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="cp_mngdatabase_b('Srv ChangeODBC')",;
    cEvent = "w_ODBC Changed",;
    nPag=1;
    , HelpContextID = 247710262

  add object oStr_1_2 as StdString with uid="SPGUWKPWCS",Visible=.t., Left=12, Top=8,;
    Alignment=0, Width=40, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="WIMJVIPRVI",Visible=.t., Left=121, Top=8,;
    Alignment=0, Width=76, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ADEBGLOWVY",Visible=.t., Left=29, Top=58,;
    Alignment=0, Width=176, Height=18,;
    Caption="Usa una stringa di connessione"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="STXQQYOIGH",Visible=.t., Left=12, Top=81,;
    Alignment=0, Width=60, Height=18,;
    Caption="ODBC"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="OGTDBEDRAD",Visible=.t., Left=185, Top=81,;
    Alignment=0, Width=33, Height=18,;
    Caption="Tipo"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="KYOZQRVRVZ",Visible=.t., Left=29, Top=139,;
    Alignment=0, Width=159, Height=18,;
    Caption="Connette a inizio programma"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NIXBUYBZUX",Visible=.t., Left=29, Top=163,;
    Alignment=0, Width=93, Height=18,;
    Caption="Gestisce Post-IN"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_mngserver','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_mngserver
* ---------------------------------------
#Define SQL_MAX_DSN_LENGTH               50
#Define SQL_FETCH_NEXT                    1
#Define SQL_SUCCESS                       0
#Define SQL_SUCCESS_WITH_INFO             1

Define Class stdcmbDataSource as StdTableCombo

	Proc Init()
		This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
		This.ToolTipText=cp_Translate(This.ToolTipText)  

		This._declareSQLApi()
		*--- Inizialmente popolo con 'SQL Server'	
		This.Populate('SQL Server')

		If This.bSetFont
			This.SetFont()
		Endif		
	EndProc

	Proc Populate(parType)
		Local i_cDSN, i_nDSNSize, i_cDescription, i_nDescriptionSize, i_nCont, i_nHenv, i, xDataSource
		Local bAll
		bAll = Empty(parType)
		This.Clear()
		If (parType=='SQLServer')
			parType = 'SQL Server'
		EndIf
		*** Initialitation variables
		i_cDSN = REPLICATE( chr(0), SQL_MAX_DSN_LENGTH )
		i_nDSNSize = 0
		i_cDescription = REPLICATE( chr(0), SQL_MAX_DSN_LENGTH )
		i_nDescriptionSize = 0
		*** ODBC Environment Handle
		i_nHenv = VAL( SYS(3053) )
		This.nValues = 0
		*** ODBC API for get Datasources
		Do While INLIST( SQLDataSources( i_nHenv, ;
									 SQL_FETCH_NEXT, ;
									 @i_cDSN, ;
									 LEN( i_cDSN ), ;
									 @i_nDSNSize, ;
									 @i_cDescription, ;
									 LEN( i_cDescription ), ;
									 @i_nDescriptionSize ), ;
					 SQL_SUCCESS, SQL_SUCCESS_WITH_INFO )
			xDataSource = SUBSTR( i_cDSN, 1, i_nDSNSize )
      dimension thisform.aDataSources[This.nValues + 1,2]
      thisform.aDataSources[This.nValues + 1, 1] = SUBSTR( i_cDSN, 1, i_nDSNSize )
      thisform.aDataSources[This.nValues + 1, 2] = SUBSTR(i_cDescription, 1, i_nDescriptionSize)
			If (bAll or (parType $ SUBSTR(i_cDescription, 1, i_nDescriptionSize)))
				This.AddItem(xDataSource)
				This.nValues = This.nValues + 1			
				Dimension This.combovalues[This.nValues]
				This.combovalues[This.nValues] = xDataSource
			EndIf		
		EndDo
		* -- Se non ho inserito nessun elemento per via del filtro metto un elemento dummy
		If This.ListCount=0
		   This.AddItem(" ")
		   This.nValues = 1
		   Dimension This.combovalues[1]
		   This.comboValues[1]=" "
		EndIf
		This.DisplayValue = this.comboValues[1]
    ThisForm.w_ODBC = ""
	EndProc

	*--- Declare funzioni odbc32.dll
	Proc _declareSQLApi()
		DECLARE SHORT SQLDataSources IN odbc32.dll ;
			INTEGER  nHenv, ;
			INTEGER  nDirection, ;
			STRING  @cDSN, ;
			INTEGER  nDSNMax, ;
			INTEGER @nDSNSize, ;
			STRING  @cDescription, ;
			INTEGER  nDescriptionMax, ;
			INTEGER @nDescription
	EndProc
EndDefine

* --- Fine Area Manuale
