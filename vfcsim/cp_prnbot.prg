* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_prnbot                                                       *
*              Funzionalita dei bottoni della  print system                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-07                                                      *
* Last revis.: 2011-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar1
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_prnbot",oParentObject,m.pPar1)
return(i_retval)

define class tcp_prnbot as StdBatch
  * --- Local variables
  pPar1 = space(10)
  w_STP = space(10)
  w_VAR = space(1)
  w_Type = space(1)
  w_OBJ = .NULL.
  w_NRO = 0
  w_OA = 0
  w_OVAR = .NULL.
  w_nCurs = 0
  w_nome = space(10)
  w_err = space(1)
  w_bOK = .f.
  w_nfile = space(10)
  cDocName = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pPar1="SStpClick"
        this.w_STP = getPrinter()
        if NOT EMPTY (this.w_STP)
          this.oParentObject.w_DEVICE = this.w_STP
        endif
      case this.pPar1="SFileClick"
        * --- * --- Clicca Export
        if this.oParentObject.w_Opt_File= "DBF"
          this.w_VAR = getfile("DBF","Table/DBF:", "Seleziona")
        else
          this.w_VAR = getfile("TXT","File/TXT:", "Seleziona")
        endif
        if NOT EMPTY (this.w_VAR)
          this.oParentObject.w_Txt_File = this.w_VAR
        endif
      case this.pPar1="AssocClick"
        this.w_OA = select()
        this.w_nCurs = sys(2015)
        if i_ServerConn[1,2]<>0
           cp_sql(i_ServerConn[1,2],"select * from cpusrrep where REPASSO="+cp_ToStr(this.oparentobject.cNomeReport+".FRX")+" and USRASSO="+cp_ToStr(i_codute)+" and AZIASSO="+cp_ToStr(i_codazi),this.w_nCurs)
        else
           select * from cpusrrep where REPASSO=this.oparentobject.cNomeReport+".FRX" and USRASSO=i_codute and AZIASSO=i_codazi into curs (this.w_nCurs)
        endif
        if used(this.w_nCurs)
          if not(eof())
            this.w_NRO = NROASSO
            this.w_OBJ = cpusrrep()
            this.w_OBJ.w_NROASSO = this.w_NRO
            this.w_OBJ.QueryKeySet("REPASSO="+cp_ToStr(this.oparentobject.cNomeReport+".FRX"),"")     
            this.w_OBJ.LoadRec()     
            this.w_OBJ.ecpEdit()     
          else
            this.w_OBJ = cpusrrep()
            this.w_OBJ.ecpLoad()     
            this.w_OBJ.w_USRASSO = i_codute
            this.w_OVAR = this.w_OBJ.GetCtrl("w_USRASSO")
            this.w_OVAR.check()     
            this.w_OBJ.w_AZIASSO = i_codazi
            this.w_OBJ.w_REPASSO = this.oparentobject.cNomeReport+".FRX"
            this.w_OBJ.w_COPASSO = 1
            if not(empty(this.oParentObject.w_DEVICE))
              this.w_OBJ.w_DEVASSO = this.oParentObject.w_DEVICE
            endif
            this.w_OBJ.mcalc(.T.)     
          endif
          USE
        else
           wait window cp_Translate(MSG_ERROR_OPENING_PRINTING_TABLE)
        endif
        select (this.w_oa)
      case this.pPar1="StpClick"
        * --- * --- Invia a Stampante senza RichiestA
        if not(empty(this.oParentObject.w_DEVICE))
          set printer to name (this.oParentObject.w_DEVICE)
        else
          set printer to default
        endif
        do cp_RunRep with this.oparentobject,cp_GetStdFile(this.oParentObject.cNomeReport,"FRX"), "S", this.oParentObject.w_Txt_File, this.oParentObject.prCopie
      case this.pPar1="StpOptClick"
        * --- * --- Invia a Stampante senza Richiesat
        if not(empty(this.oParentObject.w_DEVICE))
          set printer to name (this.oParentObject.w_DEVICE)
        else
          set printer to default
        endif
         do cp_RunRep with this.oparentobject,cp_GetStdFile(this.oParentObject.cNomeReport,"FRX"), "O", this.oParentObject.w_Txt_File, this.oParentObject.prCopie
      case this.pPar1="StpFileClick"
        * --- * --- Invia su File
        if not(empty(this.oParentObject.w_Txt_File))
          if this.oParentObject.w_Opt_File = "TXT"
            if cp_IsStdFile(this.oparentobject.cNomeReport,"FRX")
               do cp_RunRep with this.oparentobject,cp_GetStdFile(this.oparentobject.cNomeReport,"FRX"), "F", this.oParentObject.w_Txt_File, this.oParentObject.prCopie
            else
              * --- **** Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
              *        *                perch� serve solo per decidere l'icona da utilizzare.
              do cp_ErrorMsg with MSG_FUNCTION_NOT_AVALIABLE_F_USE_DBF_C_SDF_OR_DELIMITED_INSTEAD_QM,"stop",MSG_WARNING
            endif
          else
            this.w_Type = iif(this.oParentObject.w_Opt_File="DBF","DBF",iif(this.oParentObject.w_Opt_File="SDF","SDF","DLM"))
            do cp_ExpRep with this.oparentobject , this.w_Type , this.oParentObject.w_Txt_File
          endif
        else
          * --- **** Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
          *        *                perch� serve solo per decidere l'icona da utilizzare.
           do cp_ErrorMsg with MSG_FILE_NAME_MISSING_QM,"stop",MSG_WARNING
        endif
      case this.pPar1="WordClick"
        cModelName=""
        select __tmp__ 
 go top
        if not(eof())
          i_err=0
          this.w_nome = Alltrim(ADDBS(g_tempadhoc))+"__word__"
          copy to (this.w_nome) fox2x
          if g_OFFICE="M" and NOT(VARTYPE(g_ODBC64DBF)="L" and g_ODBC64DBF)
            if cp_fileexist(forceext(this.w_nome, "xls"),.T.)
              nomefiext=forceext(this.w_nome, "xls") 
              erase &nomefiext
            endif
            copy to (this.w_nome) xl5
          endif
          this.w_bOK = .T.
          i_err=on("ERROR")
          on error this.w_bOk=.f.
          this.oParentObject.word = getobject("","word.application")
          if this.w_bOK
            this.oParentObject.word.documents.open(sys(5)+sys(2003)+"\"+cp_GetStdFile(this.oParentObject.cNomeReport,"DOC")+".DOC")     
            cModelName=this.oParentObject.word.activedocument.name
            if g_OFFICE="M" and NOT(VARTYPE(g_ODBC64DBF)="L" and g_ODBC64DBF)
              nomefiext=forceext(this.w_nome, "xls") 
            else
              nomefiext=forceext(this.w_nome, "dbf")
            endif
            if not(upper(this.oParentObject.word.activedocument.mailmerge.datasource.name)==upper(nomefiext))
              this.oParentObject.word.activedocument.mailmerge.opendatasource(nomefiext)     
            endif
            this.oParentObject.word.activedocument.mailmerge.execute()     
            this.cDocName = this.oParentObject.word.activedocument.name
            this.oparentobject.word.documents(cModelName).close(0)
            this.oParentObject.word.visible = .T.
            * --- **** Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
            *        *                perch� serve solo per decidere l'icona da utilizzare.
            if NOT this.w_bOK
              cp_ErrorMsg(MSG_CANNOT_FIND_WORD_QM,"stop",MSG_ERROR)
            endif
          else
            cp_ErrorMsg(MSG_CANNOT_OPEN_WORD_DOCUMENT_QM,"stop",MSG_ERROR)
          endif
          on error &i_err
        else
          cp_ErrorMsg(MSG_NO_DATA_TO_PRINT_QM,"stop",MSG_ERROR)
        endif
      case this.pPar1="ExcelClick"
        select __tmp__ 
 go top
        this.w_bOK = .T.
        i_err=on("ERROR") 
 on error bOk=.f.
        this.oParentObject.excel = Createobject("excel.application")
        if this.w_bOK
          if cp_IsStdFile(this.cNomeReport,"XLT")
            wait window cp_Translate(MSG_BUILDING_EXCEL_WORKSHEET_D) nowait
            ve_build(this.oParentObject.excel,"__tmp__",sys(5)+sys(2003)+"\"+cp_GetStdFile(this.oParentObject.cNomeReport,"XLT")+".XLT",.f.)
            wait clear
          else
            this.w_nfile = getenv("TEMP")+"\"+strtran(this.oParentObject.w_Txt_File,iif(this.oParentObject.w_Opt_File="DBF",".DBF",".TXT"),".XLS")
            on error cp_ErrorMsg(cp_Translate("Non e' possibile creare il FILE: ")+upper(this.w_nFile)) 
 copy to (i_nFile) TYPE XL5
            this.oParentObject.excel.workbooks.open(this.w_nFile)     
            this.oParentObject.excel.excel.visible = .T.
            on error
          endif
        else
          * --- **** Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
          *        *                perch� serve solo per decidere l'icona da utilizzare.
          cp_ErrorMsg(MSG_CANNOT_OPEN_EXCEL_WORKSHEET_QM,"stop",MSG_ERROR)
        endif
        on error &i_err
      case this.pPar1="GraphClick"
        if FILE( FULLPATH(cp_GetStdFile(this.oParentObject.cNomeReport,"VFC")+".VFC") )
          local l_param
          l_param = "__tmp__" + "," + FULLPATH(cp_GetStdFile(this.oParentObject.cNomeReport ,"VFC")+".VFC") 
          VX_EXEC( l_param )
        else
          if FILE( FULLPATH(cp_GetStdFile(this.oParentObject.cNomeReport,"VGR")+".VFC") ) 
             select __tmp__ 
 go top 
 vg_build("__tmp__",sys(5)+sys(2003)+"\"+cp_GetStdFile(this.oParentObject.cNomeReport,"VGR")+".vgr",.t.,.t.)
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pPar1)
    this.pPar1=pPar1
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar1"
endproc
