* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_QUERY
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Classi per la gestione delle query
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

* --- i_Files: Array con i Files
*     1 = Nome
*     2 = Nome Logico
*     3 = Nome Rinominato
*     4 = Nro. Connessione (0 = Database Locale Visual Foxpro)
* --- i_Fields: Array con i Campi
*     1 = Expression
*     2 = Nome Campo
*     3 = Tipo         (C=Char, N=Numeric, D=Date)
*     4 = Lunguezza
*     5 = Decimali
*---  Descrizione colonne zoom
*     6 = Descrizione
* --- i_Join: Array con le Join
*     1 = Nome File 1
*     2 = Nome File 2
*     3 = Nome File 3
*     4 = Expr
*     5 = Tipo = Outer/Where
* --- i_Where: Array con le Where
*     1 = Expr
* --- i_OrderBy: Array con gli Order By
*     1 = Field
*     2 = Ascending/Discending
* --- i_GroupBy: Array con i Group By
*     1 = Expr
* --- i_WhareTmp: Array con le Where temporanee
*     1 = Expr

#Define FILEDIM     4
#Define aF_Nome     1
#Define aF_NomeL    2
#Define aF_NomeF    3
#Define aF_Conn     4
#Define FIELDDIM    6
#Define aC_Expr     1
#Define aC_Nome     2
#Define aC_Tipo     3
#Define aC_Len      4
#Define aC_Dec      5
#Define aC_Des      6
#Define JOINDIM     6
#Define aJ_NomeF1   1
#Define aJ_NomeF2   2
#Define aJ_NomeF3   3
#Define aJ_Expr     4
#Define aJ_Type     5
#Define aJ_Used     6

#Define WHEREDIM    4
#Define aW_Expr     1
#Define aW_Param    2
#Define aW_SubQuery 3
#Define aW_oQuery   4

#Define ORDERDIM    2
#Define aO_Field    1
#Define aO_AscDsc   2

#Define GROUPDIM    1
#Define aG_Expr     1

#Define WHERETMPDIM 1
#Define aWT_Expr    1

#Define FILTERPARAMDIM 8
#Define aFP_Name    1
#Define aFP_Descr   2
#Define aFP_Type    3
#Define aFP_Len     4
#Define aFP_Dec     5
#Define aFP_Value   6
#Define aFP_HasValue 7
#Define aFP_Remove  8

Define Class CPQuery As Custom
    Declare i_Files[1,FILEDIM]
    Declare i_Fields[1,FIELDDIM]
    Declare i_Joins[1,JOINDIM]
    Declare i_Where[1,WHEREDIM]
    Declare i_Having[1,WHEREDIM]
    Declare i_OrderBy[1,ORDERDIM]
    Declare i_GroupBy[1,GROUPDIM]
    Declare i_WhereTmp[1,WHERETMPDIM]
    Declare i_FilterParam[1,FILTERPARAMDIM]
    Declare i_PivotFlds[1]
    Declare i_PivotData[1]
    nFiles = 0
    nFilesOld = 0 && Nuova Variabile (Gestione Tabelle Aperte)
    nFields = 0
    nJoins = 0
    nWhere = 0
    nHaving = 0
    nOrderBy = 0
    nGroupBy = 0
    nWhereTmp = 0
    nFilterParam = 0
    nPivotFlds = 0
    nPivotData = 0
    nPivotType = 0
    cPivotListQry = ""
    cFields = ""
    cFrom = ""
    cJoin = ""
    cWhere = ""
    cHaving = ""
    cDistinct = ""
    cOrderBy = ""
    cGroupBy = ""
    cOrderByTmp = ""
    cExtMask = ""
    bRemoveWhereOnEmptyParam=.F.
    cSingleParametersToBeRemoved='?'
    cUnion= ""
    bUnionAll=.F.
    bOrderByParm=.T.
    nVersion=0
    oUnion=.Null.
    oParentObject=.Null.
    runtime_filters=1
    *  variabili di appoggio
    Declare i_FileInUse[1]
    i_cntf1=0
    *
    nConn=-1
    cDatabaseType=''
    bAskParam=.T.
    bFox26=.F.
    * --- Zucchetti Aulla inzio
    * --- Contiene l'elenco dei parametri divisi da ; per i quali
    * --- anche se il parametro � vuoto va comunque passato alla query
    * --- Utilizzato nel Faz_Engine
    cExcludeFromRemoveParam=''
    * --- Zucchetti Aulla fine
    *
    bMultiCompany=.F.
    cCompanyList=''
    cCompanyAlias='cp_company'
    cRoles=''
    *
    * --- Zucchetti Aulla - bind variable di oracle
    * --- propriet� che definisce nella sezione fields se c'� una costante e quindi non si pu� utilizzare
    * --- bind variable perch� il tipo del cursore sarebbe sbagliato
    bNoBindVariable=.F.
    * --- Zucchetti Aulla fine - bind variable di oracle
    * --- Zucchetti Aulla inizio - vero tipo del parametro per postgres
    i_FilterParamRealType = .NULL.
    * --- Zucchetti Aulla fine - vero tipo del parametro per postgres
    bPublic=.T.
    bBatch=.F.
    cSecKey=''
    cQryName = ''
    Proc Init()
        If Type("i_bFox26")='L'
            This.bFox26=.T.
            If At('cp_pfile',Lower(Set('proc')))=0
                Set Proc To cp_pfile Additive
            Endif
        Else
            * --- IL TIPO DI DATABASE DELLA QUERY DI DEFAULT � LA CONNESSIONE PRINCIPALE
            This.cDatabaseType=CP_GETDATABASETYPE(I_SERVERCONN[1,2])
        Endif
    * --- Zucchetti Aulla inizio - vero tipo del parametro per postgres
    If This.cDatabaseType="PostgreSQL"
        This.i_FilterParamRealType=Createobject("collection")
    Endif
    * --- Zucchetti Aulla fine - vero tipo del parametro per postgres
    Endproc
    Proc Destroy(i_oper)
        Local i_cNomeF,i
        If !Isnull(This.oUnion)
            * Le destroy lanciate dalle union
            * devono verificare quando sono in stato exec per lanciare le eventuali tabelle temporanee
            *this.oUnion.destroy()
            This.oUnion.Destroy(i_oper)
        Endif
        This.oUnion=.Null.
        For i=1 To This.nWhere
            If !Isnull(This.i_Where[i,aW_oQuery])
                This.i_Where[i,aW_oQuery].Destroy()
            Endif
            This.i_Where[i,aW_oQuery]=.Null.
        Next
        For i=1 To This.nHaving
            If !Isnull(This.i_Having[i,aW_oQuery])
                This.i_Having[i,aW_oQuery].Destroy()
            Endif
            This.i_Having[i,aW_oQuery]=.Null.
        Next

        * Se non sono in Get devo cancellare le tabelle temporanee
        If Type('i_oper')='C' And i_oper<>'Get' And !This.bFox26
            This.nFiles=Iif(This.nFiles=0,This.nFilesOld,This.nFiles)
        Endif

        For i=1 To This.nFiles
            i_cNomeF=This.i_Files[i,aF_NomeF]
            If This.bFox26
                If Used(i_cNomeF)
                    If i_dcx.istabella(i_cNomeF)
                        Select (i_cNomeF)
                        Use
                    Else
                        Do cplu_clf With (i_cNomeF)
                    Endif
                Endif
            Else
                If Left(i_cNomeF,1)<>'('
                    cp_CloseTable(i_cNomeF)
                Endif
            Endif
            If Upper(Right(This.i_Files[i,aF_Nome],4))='.VQR' And Left(i_cNomeF,1)<>'('
                This.DropTemporaryTable(i_cNomeF)
            Endif
        Next
        This.nFiles=0  && non ha piu' file aperti
        * --- Zucchetti Aulla inizio - vero tipo del parametro per postgres
        This.i_FilterParamRealType=.Null.
        * --- Zucchetti Aulla fine - vero tipo del parametro per postgres
        Return

    Func ADim(N)
        Return(Int(N/10)*10+10)

    Procedure SetParentObject(i_oParentObject)
        If Vartype(i_oParentObject)='O'
            This.oParentObject=i_oParentObject
            If Vartype(i_oParentObject.runtime_filters)='N'
                This.runtime_filters=i_oParentObject.runtime_filters
            Else
                If i_oParentObject.isVar('i_runtime_filters')
                    This.runtime_filters=i_oParentObject.GetVar('i_runtime_filters')
                Endif
            Endif
        Endif
        Return

    Proc mLoadFile(i_cNome,i_cNomeL,i_cNomeF,i_nConn)
        Local i_NT,i_res,i
        If Type('i_oParentObject')='L'
            i_oParentObject=.Null.
        Endif
        If Upper(Right(i_cNome,4))='.VQR'
            If Right(i_cNomeF,1)='*'
                If I_SERVERCONN[1,2]<>0
                    If Not This.bMultiCompany
                        Local oTT,i_l
                        oTT=Createobject('cpquery')
                        oTT.bFox26=.F.
                        i_l=Createobject('cpqueryloader',.F.)
                        i_l.LoadQuery(i_cNomeL,oTT,This,.T.) && Se query di query
                        oTT.cDatabaseType=CP_GETDATABASETYPE(Iif(i_nConn=0,I_SERVERCONN[1,2],i_nConn))
                        i_cNomeF='('+oTT.mDoQuery('nocursor','Get',.Null.)+')'
                        * --- Rimuovo gli oggetti dalla memoria
                        oTT.Destroy()
                        oTT=.Null.
                        i_l.Destroy()
                        i_l=.Null.
                    Else
                        i_cNomeF='(*'+i_cNomeL+')'
                    Endif
                Else
                    i_cNomeF=Left(i_cNome,Len(i_cNomeF)-1)
                    This.CreateTemporaryTable(i_cNomeL)
                Endif
            Else
                If Right(i_cNomeF,1)='+'
                    Local i_batch
                    i_batch=Left(i_cNomeF,Len(i_cNomeF)-1)
                    *WAIT window "chiama il batch "+i_batch
                    This.CallBatch(i_batch)
                    i_cNomeF=i_batch
                    i_cNomeL=i_batch
                Else
                    This.CreateTemporaryTable(i_cNomeL)
                Endif
            Endif
        Endif
        With This
            .nFiles   = .nFiles+1
            Dimension .i_Files[this.ADim(.nFiles),FILEDIM]
            .i_Files[.nFiles,aF_Nome]  = i_cNome
            .i_Files[.nFiles,aF_NomeL] = i_cNomeL
            .i_Files[.nFiles,aF_NomeF] = i_cNomeF
            .i_Files[.nFiles,aF_Conn]  = i_nConn
            *
            If This.bFox26
                If i_dcx.istabella(i_cNomeF)
                    cp_seltb(i_cNomeF)
                Else
                    Do cplu_opf With (i_cNomeF),''
                Endif
            Else
                If Left(i_cNomeF,1)<>'('
                    i_NT=cp_OpenTable(Trim(i_cNomeF))
                    If i_NT<>0
                        * --- trovato nell' elenco dei file
                        If .nConn=-1
                            .nConn=i_TableProp[i_NT,3]
                            * --- cerca il server
                            i_res = 0
                            For i=1 To i_nServers
                                If I_SERVERCONN[i,2]=.nConn
                                    i_res=i
                                Endif
                            Next
                            If i_res<>0
                                .cDatabaseType=I_SERVERCONN[i_res,6]
                            Endif
                        Else
                            If .nConn<>i_TableProp[i_NT,3]
                                .nConn=-2
                            Endif
                        Endif
                    Endif
                Else
                    If .nConn=-1
                        .nConn=I_SERVERCONN[1,2]
                    Endif
                Endif
            Endif
            *
        Endwith
        Return

    Proc CreateSubQueryTables()
        Local i,i_cNomeL,i_cNomeF
        Local oTT,i_l
        Local i_oldazi
        For i=1 To This.nFiles
            i_cNomeF=This.i_Files[i,aF_NomeF]
            If Left(i_cNomeF,2)='(*'
                * -- la sintassi
                i_cNomeL=Substr(i_cNomeF,3,Len(i_cNomeF)-3)
                oTT=Createobject('cpquery')
                oTT.bFox26=.F.
                i_l=Createobject('cpqueryloader',.F.)
                i_l.LoadQuery(i_cNomeL,oTT,This,.T.) &&  Se multi company rimuovo le order by
                If This.bMultiCompany
                    i_oldazi=i_codazi
                    i_codazi='@@@###!!!^^^@@@'
                Endif
                oTT.cDatabaseType=CP_GETDATABASETYPE(Iif(This.nConn=0,I_SERVERCONN[1,2],This.nConn))
                i_cNomeF='('+oTT.mDoQuery('nocursor','Get',.Null.)+')'
                This.i_Files[i,aF_NomeF]=i_cNomeF
                If This.bMultiCompany
                    i_codazi=i_oldazi
                Endif
                * --- Rimuovo gli oggetti dalla memoria
                oTT.Destroy()
                oTT=.Null.
                i_l.Destroy()
                i_l=.Null.
            Endif
        Next
        Return

    Procedure CallBatch(i_batch)
        Private i_pl,i_prm,i_p,i_n
        Local i_olderr,bIsVar, bRightParent, cParent
        i_pl=""
        i_prm=""
        bRightParent=.T.
        i_olderr=On('ERROR')
        On Error i_pl=""
        Do getEntityParmList In &i_batch. With i_pl
        On Error &i_olderr
        If !Empty(i_pl)
            i_pl=i_pl+','
            i_p=At(',',i_pl)
            Do While i_p<>0
                i_n=Left(i_pl,i_p-1)
                i_pl=Substr(i_pl,i_p+1)
                * --- Batch come Query per leggere i parametri
                * --- se lanciato da funzione
                On Error bRightParent=.F.
                bIsVar=This.isVar(i_n)
                If Not bRightParent
                    bRightParent=.T.
                    bIsVar=This.oParentObject.isVar(i_n)
                    If Not bRightParent
                        *--- Non so cosa fare...
                        bIsVar=.F.
                    Else
                        cParent='this.oParentobject'
                    Endif
                Else
                    cParent='this'
                Endif
                On Error &i_olderr
                * --- Batch come Query non legge i parametri
                If bIsVar
                    i_prm=i_prm+','+cParent+'.getvar("'+i_n+'")'
                Else
                    i_prm=i_prm+',.f.'
                Endif
                i_p=At(',',i_pl)
            Enddo
        Endif
        *WAIT WINDOW 'chiama il batch '+i_batch+' con parametri '+i_prm
        &i_batch.(This &i_prm.)
        Return

        *--- Descrizione colonne zoom
    Proc mLoadField(i_cExpr,i_cNome,i_cTipo,i_nLen,i_ndec, i_cDes)
        With This
            .nFields  = .nFields+1
            Dimension .i_Fields[this.ADim(.nFields),FIELDDIM]
            .i_Fields[.nFields,aC_Expr] = i_cExpr
            .i_Fields[.nFields,aC_Nome] = i_cNome
            .i_Fields[.nFields,aC_Tipo] = i_cTipo
            .i_Fields[.nFields,aC_Len]  = i_nLen
            .i_Fields[.nFields,aC_Dec]  = i_ndec
            If Type("i_cDes")='C'
                .i_Fields[.nFields,aC_Des]  = i_cDes
            Else
                .i_Fields[.nFields,aC_Des]  = i_cNome
            Endif
        Endwith
        Return

    Proc mLoadJoin(i_cNome1,i_cNome2,i_cNome3,i_cExpr,i_cType)
        With This
            .nJoins = .nJoins+1
            Dimension .i_Joins[this.ADim(.nJoins),JOINDIM]
            .i_Joins[.nJoins,aJ_NomeF1] = i_cNome1
            .i_Joins[.nJoins,aJ_NomeF2] = i_cNome2
            .i_Joins[.nJoins,aJ_NomeF3] = i_cNome3
            .i_Joins[.nJoins,aJ_Expr]   = i_cExpr
            .i_Joins[.nJoins,aJ_Type]   = i_cType
            .i_Joins[.nJoins,aJ_Used]   = .F.
        Endwith
        Return

    Proc mLoadWhere(i_cExpr,i_cParam,i_bSubQuery)
        With This
            .nWhere = .nWhere+1
            Dimension .i_Where[this.ADim(.nWhere),WHEREDIM]
            .i_Where[.nWhere,aW_Expr] = i_cExpr
            .i_Where[.nWhere,aW_Param] = i_cParam
            .i_Where[.nWhere,aW_SubQuery] = i_bSubQuery
            .i_Where[.nWhere,aW_oQuery] = .Null.
        Endwith
        Return

    Proc mLoadHaving(i_cExpr,i_cParam,i_bSubQuery)
        With This
            .nHaving = .nHaving+1
            Dimension .i_Having[this.ADim(.nHaving),WHEREDIM]
            .i_Having[.nHaving,aW_Expr] = i_cExpr
            .i_Having[.nHaving,aW_Param] = i_cParam
            .i_Having[.nHaving,aW_SubQuery] = i_bSubQuery
            .i_Having[.nHaving,aW_oQuery] = .Null.
        Endwith
        Return

    Proc mLoadOrder(i_cField,i_cAscDsc)
        With This
            .nOrderBy = .nOrderBy+1
            Dimension .i_OrderBy[this.ADim(.nOrderBy),ORDERDIM]
            .i_OrderBy[.nOrderBy,aO_Field] = i_cField
            .i_OrderBy[.nOrderBy,aO_AscDsc] = i_cAscDsc
        Endwith
        Return

    Proc mLoadGroupBy(i_cExpr)
        With This
            .nGroupBy = .nGroupBy+1
            Dimension .i_GroupBy[this.ADim(.nGroupBy),GROUPDIM]
            .i_GroupBy[.nGroupBy,aG_Expr] = i_cExpr
        Endwith
        Return

    Proc mClearWhereTmp()
        This.nWhereTmp = 0
        Dimension This.i_WhereTmp[1,WHERETMPDIM]
        Return

    Proc mLoadWhereTmp(i_cExpr)
        If !Empty(i_cExpr)
            This.nWhereTmp = This.nWhereTmp+1
            Dimension This.i_WhereTmp[this.ADim(this.nWhereTmp),WHERETMPDIM]
            This.i_WhereTmp[this.nWhereTmp,aWT_Expr] = i_cExpr
        Endif
        Return

    Proc mClearOrderByTmp()
        This.cOrderByTmp=''
        Return

    Proc mLoadOrderByTmp(i_cExpr)
        This.cOrderByTmp=i_cExpr
        Return

    Proc mLoadCompanyList(i_cCompanyList)
        This.cCompanyList=i_cCompanyList
        Return

    Proc mLoadCompanyAlias(i_cCompanyAlias)
        This.cCompanyAlias=i_cCompanyAlias
        Return

    Proc mLoadValuedFilterParam(cName,cDescr,cType,nLen,nDec,xVal,cRemove)
        This.mLoadFilterParam(cName,cDescr,cType,nLen,nDec,cRemove)
        This.i_FilterParam[this.nFilterParam,aFP_Value]=xVal
        This.i_FilterParam[this.nFilterParam,aFP_HasValue]=.T.
        Return

    Proc mLoadFilterParam(cName,cDescr,cType,nLen,nDec,cRemove)
        If Type("cRemove")='L'
            cRemove=''
        Endif
        With This
            .nFilterParam = .nFilterParam+1
            Dimension .i_FilterParam[.nFilterParam,FILTERPARAMDIM]
            .i_FilterParam[.nFilterParam,aFP_Name]=cName
            .i_FilterParam[.nFilterParam,aFP_Descr]=cDescr
            .i_FilterParam[.nFilterParam,aFP_Type]=cType
            .i_FilterParam[.nFilterParam,aFP_Len]=nLen
            .i_FilterParam[.nFilterParam,aFP_Dec]=nDec
            .i_FilterParam[.nFilterParam,aFP_HasValue]=.F.
            .i_FilterParam[.nFilterParam,aFP_Remove]=cRemove
            If cRemove='Remove'
                This.cSingleParametersToBeRemoved=This.cSingleParametersToBeRemoved+cName+'?'
            Endif
        Endwith
        Return

    Proc mLoadExtMask(cExtMask)
        This.cExtMask=cExtMask
        Return

    Proc mLoadPivotFlds(i_cAlias)
        With This
            .nPivotFlds = .nPivotFlds+1
            Dimension .i_PivotFlds[this.ADim(.nPivotFlds)]
            .i_PivotFlds[.nPivotFlds] = i_cAlias
        Endwith

    Proc mLoadPivotData(i_cAlias)
        With This
            .nPivotData = .nPivotData+1
            Dimension .i_PivotData[this.ADim(.nPivotData)]
            .i_PivotData[.nPivotData] = i_cAlias
        Endwith

    Func OracleOuterJoinSub(i_cExpr,i_cType,i_t1,i_t2)
        Local i_p,i_bSameOrd
        i_p=At('=',i_cExpr)
        i_s1=Left(i_cExpr,i_p-1)
        i_s2=Substr(i_cExpr,i_p+1)
        i_bSameOrd=At(i_t1+'.',Lower(i_s1))<>0
        Do Case
            Case i_cType='left outer'
                If i_bSameOrd
                    * le espressioni sono nello stesso ordine di t1 e t2
                    i_s2=i_s2+'(+)'
                Else
                    * le espressioni sono in ordine inverso rispetto t1 e t2
                    i_s1=i_s1+'(+)'
                Endif
            Case i_cType='right outer'
                If i_bSameOrd
                    * le espressioni sono nello stesso ordine di t1 e t2
                    i_s1=i_s1+'(+)'
                Else
                    * le espressioni sono in ordine inverso rispetto t1 e t2
                    i_s2=i_s2+'(+)'
                Endif
            Case i_cType='full'
                i_s1=i_s1+'(+)'
                i_s2=i_s2+'(+)'
        Endcase
        Return(i_s1+'='+i_s2)

    Func OracleOuterJoin(i_cExpr,i_cType,i_t1,i_t2)
        Local i_p,i_res
        i_cType=Lower(i_cType)
        i_t1=Lower(i_t1)
        i_t2=Lower(i_t2)
        *wait window i_cType+' '+i_t1+' '+i_t2+' '+i_cExpr
        i_res=''
        i_p=At(' AND ',Upper(i_cExpr))
        Do While i_p<>0
            i_res=i_res+This.OracleOuterJoinSub(Trim(Left(i_cExpr,i_p-1)),i_cType,i_t1,i_t2)+' AND '
            i_cExpr=Substr(i_cExpr,i_p+5)
            i_p=At(' AND ',Upper(i_cExpr))
        Enddo
        i_res=i_res+This.OracleOuterJoinSub(i_cExpr,i_cType,i_t1,i_t2)
        *wait window i_cType+' '+i_t1+' '+i_t2+' '+i_res
        Return(i_res)

    Proc mCreateSelect()
        Local i,j,xxx,cPar,i_nf,cJ,p,cFld,cExp,w,i_dbtype,c,cCurrentTable,nCurrentTableSP,nCurrentSubCnt,cNewTable,outerpos,N
        Dimension cCurrentTable[40]
        Dimension nCurrentSubCnt[40]
        i_dbtype=This.cDatabaseType
        With This
            * --- Preparo la variabile per i campo cFields
            If .nFields=0
                .cFields = "*"
            Else
                c = ""
                For i=1 To .nFields
                    p = .i_Fields[i,aC_Expr]
                    *** Zucchetti Aulla Inizio - Su database PostgreSQL le costanti stringa restituiscono il tipo unknown se la query non restituisce righe
                    If i_dbtype="PostgreSQL"
                        cPgrExpr=Alltrim(.i_Fields[i,aC_Expr])
                        Do Case
                            Case (Left(cPgrExpr,1)="'" OR INLIST(Left(cPgrExpr,5),"MIN('","MAX('")) And Right(cPgrExpr,1)="'" And Occurs("'",cPgrExpr)=2
                                If INLIST(Left(cPgrExpr,5),"MIN('","MAX('")
                                    p = Substr(m.p,5)
                                Endif
                                If .i_Fields[i,aC_Tipo]="M"
                                    p = "CAST(" +m.p+ " AS TEXT)"
                                Else
                                    p = "CAST(" +m.p+ " AS CHAR(" + Alltrim(Str(Max(1,Len(cPgrExpr)-2),4,0)) + "))"
                                Endif
                            Case cPgrExpr="?" And .i_FilterParamRealType.GetKey(Substr(cPgrExpr,2))>0 And .i_FilterParamRealType.Item(Substr(cPgrExpr,2))="C"
                                p = "''||"+m.p
                            Otherwise
                                If cPgrExpr="[GLOBALVAR(" And Right(cPgrExpr,2)=")]"
                                    cPgrSpace = Substr(cPgrExpr,12,Len(cPgrExpr)-13)
                                    Local i_errval
                                    i_errval=On('ERROR')
                                    On Error =.F.
                                    cPgrSpace = Evaluate(cPgrSpace)
                                    On Error &i_errval
                                    
                                    If Vartype(cPgrSpace)="C"
                                        cPgrLen = Len(cPgrSpace)
                                        If cPgrLen>0 And cPgrLen<255 AND .i_Fields[i,aC_Tipo]="C"
                                            p = "CAST(" +m.p+ " AS CHAR(" + ALLTRIM(STR(cPgrLen,20,0)) + "))"
                                        Else
                                            p = "CAST(" +m.p+ " AS TEXT)"
                                        Endif
                                    Endif
                                Else
                                    cPgrSpace=Chrtran(Upper(cPgrExpr),'1234567890 ','')
                                    If cPgrSpace=='[SPACES()]' Or (cPgrSpace=='MIN([SPACES()])' Or cPgrSpace=='MAX([SPACES()])')
                                        cPgrLen=Chrtran(Upper(cPgrExpr),'MINX[SPACE()]','')
                                        If .i_Fields[i,aC_Tipo]="M"
                                            p = "CAST('" +Space(Val(cPgrLen))+ "' AS TEXT)"
                                        Else
                                            p = "CAST('" +Space(Val(cPgrLen))+ "' AS CHAR(" + cPgrLen + "))"
                                        Endif
                                    Endif
                                Endif
                        Endcase
                        *** Preparo la lista dei campi Memo
                        If .i_Fields[i,aC_Tipo]="M"
                            i_cMemoFieldsPostgreSQL=i_cMemoFieldsPostgreSQL+.i_Fields[i,aC_Nome]+","
                        Endif
                        *** Preparo la lista dei campi Char(254)
                        If .i_Fields[i,aC_Tipo]="C" And .i_Fields[i,aC_Len]=254
                            i_cC254FieldsPostgreSQL = i_cC254FieldsPostgreSQL + .i_Fields[i,aC_Nome]+","
                        Endif
                    Endif
                    *** Zucchetti Aulla Fine - Su database PostgreSQL le costanti stringa restituiscono il tipo unknown se la query non restituisce righe
                    c = m.c+Iif(i=1,'',',')+p
                    xxx=Trim(.i_Fields[i,aC_Nome])
                    If !Empty(xxx)
                        *** Zucchetti Aulla - Modifica perch� nel caso di utilizzo di funzione di calcolo non metteva alias il codice finiva con il nome dell'alias
                        *** Esempio CONTI.ANTIPCON[CONCAT]CONTI.ANCODICE as ANCODICE
                        *if not(right(.i_Fields[i,aC_Expr],len(xxx)+1)=='.'+xxx)
                        If '['$.i_Fields[i,aC_Expr] Or Not(Right(.i_Fields[i,aC_Expr],Len(xxx)+1)=='.'+xxx)
                            p =.i_Fields[i,aC_Nome]
                            c = m.c+" as "+p
                        Endif
                    Endif
                Next
                If Not(Empty(.cDistinct))
                    c = .cDistinct+' '+m.c
                Endif
                .cFields = m.c
            Endif
            If This.bMultiCompany
            	If !Empty(.cCompanyAlias)
	                .cFields=.cFields+",[CAST('@@@###!!!^^^@@@' as CHAR(5))] as " + .cCompanyAlias
	        Endif
            Endif
            * --- Completo la FROM e la WHERE con le eventuali Join
            .cFrom=''
            .cWhere=''
            This.i_cntf1=0
            For N=1 To .nJoins
                .i_Joins[n,aJ_Used]=.F.
            Next
            Do While .T.
                * --- trova il primo file non usato
                N=.FirstUnusedFile()
                If N=0
                    Exit
                Endif
                .UsedFile(.i_Files[n,aF_NomeL])
                cPar=''
                cJ=.SearchFile(.i_Files[n,aF_NomeL])+.i_Files[n,aF_NomeL]
                cJ1=''
                cCurrentTable[1]=.i_Files[n,aF_NomeL]
                nCurrentSubCnt[1]=0
                nCurrentTableSP=1
                * --- ci appiccica tutte le outer join
                Do While nCurrentTableSP>0
                    i=.FindAJoin(cCurrentTable[nCurrentTableSP])
                    If i<>0
                        nCurrentSubCnt[nCurrentTableSP]=nCurrentSubCnt[nCurrentTableSP]+1
                        If nCurrentSubCnt[nCurrentTableSP]=1
                            If i_dbtype='Informix'
                                outerpos=Rat('outer ',cJ)
                                If outerpos<>0
                                    cJ=Substr(cJ,1,outerpos+5)+'('+Substr(cJ,outerpos+5)
                                Endif
                            Endif
                        Endif
                        Do Case
                            Case .IsUsed(.i_Joins[i,aJ_NomeF2]) And !.IsUsed(.i_Joins[i,aJ_NomeF3]) And .i_Joins[i,aJ_Type]<>'Where' &&'Inner'
                                * --- Il primo c'e', il secondo no
                                .UsedFile(.i_Joins[i,aJ_NomeF3])
                                cNewTable=.i_Joins[i,aJ_NomeF3]
                                If .i_Joins[i,aJ_Type]='Right outer'
                                    CP_MSG(cp_Translate(MSG_QUERY_MAY_BE_WRONG_TWO_TABLES_OUTER_WITH_TABLE))
                                Endif
                                Do Case
                                        * --- Zucchetti Aulla, Compatibilit� Oracle 8
                                    Case Inlist(i_dbtype,'Adabas','SAPDB') Or (i_dbtype='Oracle' And ((Type('i_OldOracleJoinSyntax')<>'U' And i_OldOracleJoinSyntax) Or (Type('g_ORACLEVERS')='C' And g_ORACLEVERS = '8')))
                                        * --- sintassi ORACLE per le outer join
                                        cJ1=cJ1+','+.SearchFile(.i_Joins[i,aJ_NomeF3])+.i_Joins[i,aJ_NomeF3]
                                        .cWhere=.cWhere+Iif(Empty(.cWhere),'',' and ')+' ('+This.OracleOuterJoin(.i_Joins[i,aJ_Expr],.i_Joins[i,aJ_type],.i_Joins[i,aJ_NomeF2],.i_Joins[i,aJ_NomeF3])+') '
                                    Case i_dbtype='Informix'
                                        cJ=cJ+',outer '+.SearchFile(.i_Joins[i,aJ_NomeF3])+.i_Joins[i,aJ_NomeF3]
                                        .cWhere=.cWhere+Iif(Empty(.cWhere),'',' and ')+' ('+.i_Joins[i,aJ_Expr]+') '
                                    Otherwise && sintassi SQL92 per le outer join
                                        If .i_Joins[i,aJ_Type]='Right outer' And i_dbtype='DB2'
                                            * --- DB2 su AS/400 non ha RIGHT OUTER JOIN
                                            cJ=.SearchFile(.i_Joins[i,aJ_NomeF3])+.i_Joins[i,aJ_NomeF3]+' Left Join '+cPar+cJ+;
                                                ' on '+.i_Joins[i,aJ_Expr]+')'
                                            cPar='('
                                        Else
                                            cPar=cPar+'('
                                            cJ=cJ+' '+.i_Joins[i,aJ_Type]+' Join '+.SearchFile(.i_Joins[i,aJ_NomeF3])+.i_Joins[i,aJ_NomeF3]+;
                                                ' on '+.i_Joins[i,aJ_Expr]+')'
                                        Endif
                                Endcase
                                .i_Joins[i,aJ_Used]=.T.
                            Case !.IsUsed(.i_Joins[i,aJ_NomeF2]) And .IsUsed(.i_Joins[i,aJ_NomeF3]) And .i_Joins[i,aJ_Type]<>'Where' &&'Inner'
                                * --- Il primo non c'e', il secondo c'e'
                                .UsedFile(.i_Joins[i,aJ_NomeF2])
                                cNewTable=.i_Joins[i,aJ_NomeF2]
                                If .i_Joins[i,aJ_Type]='Left outer'
                                    CP_MSG(cp_Translate(MSG_QUERY_MAY_BE_WRONG_TWO_TABLES_OUTER_WITH_TABLE))
                                Endif
                                Do Case
                                        * --- Zucchetti Aulla, Compatibilit� Oracle 8
                                    Case Inlist(i_dbtype,'Adabas','SAPDB') Or (i_dbtype='Oracle' And ((Type('i_OldOracleJoinSyntax')<>'U' And i_OldOracleJoinSyntax) Or (Type('g_ORACLEVERS')='C' And g_ORACLEVERS = '8')))
                                        * --- proviamo a spostare la Inner Join nella parte di Where
                                        xxx=.i_Joins[i,aJ_Type]
                                        cJ1=cJ1+','+.SearchFile(.i_Joins[i,aJ_NomeF2])+.i_Joins[i,aJ_NomeF2]
                                        .cWhere=.cWhere+Iif(Empty(.cWhere),'',' and ')+' ('+This.OracleOuterJoin(.i_Joins[i,aJ_Expr],xxx,.i_Joins[i,aJ_NomeF2],.i_Joins[i,aJ_NomeF3])+') '
                                    Case i_dbtype='Informix'
                                        cJ=cJ+',outer '+.SearchFile(.i_Joins[i,aJ_NomeF2])+.i_Joins[i,aJ_NomeF2]
                                        .cWhere=.cWhere+Iif(Empty(.cWhere),'',' and ')+' ('+.i_Joins[i,aJ_Expr]+') '
                                    Otherwise && sintassi SQL92 per le outer join
                                        xxx=Iif(.i_Joins[i,aJ_Type]='Left outer','Right outer',Iif(.i_Joins[i,aJ_Type]='Right outer','Left outer',.i_Joins[i,aJ_Type]))
                                        If xxx='Right outer' And i_dbtype='DB2'
                                            * --- DB2 su AS/400 non ha RIGHT OUTER JOIN
                                            cJ=.SearchFile(.i_Joins[i,aJ_NomeF2])+.i_Joins[i,aJ_NomeF2]+' Left Join '+cPar+cJ+;
                                                ' on '+.i_Joins[i,aJ_Expr]+')'
                                            cPar='('
                                        Else
                                            cPar=cPar+'('
                                            cJ=cJ+' '+xxx+' Join '+.SearchFile(.i_Joins[i,aJ_NomeF2])+.i_Joins[i,aJ_NomeF2]+;
                                                ' on '+.i_Joins[i,aJ_Expr]+')'
                                        Endif
                                Endcase
                                .i_Joins[i,aJ_Used]=.T.
                            Case .IsUsed(.i_Joins[i,aJ_NomeF2]) And .IsUsed(.i_Joins[i,aJ_NomeF3]) And .i_Joins[i,aJ_Type]<>'Where' &&'Inner'
                                * Il primo c'e', il secondo c'e'
                                .i_Joins[i,aJ_Used]=.T.
                                CP_MSG(cp_MsgFormat(MSG_REDUNDANT_JOIN_NO_F__ ,Alltrim(Str(i))), .F.)
                                Exit
                        Endcase
                        * --- ora setto il nuovo file corrente
                        nCurrentTableSP=nCurrentTableSP+1
                        cCurrentTable[nCurrentTableSP]=cNewTable
                        nCurrentSubCnt[nCurrentTableSP]=0
                    Else
                        If nCurrentSubCnt[nCurrentTableSP]<>0 And nCurrentTableSP>1
                            If i_dbtype='Informix'
                                cJ=cJ+')'
                            Endif
                        Endif
                        nCurrentTableSP=nCurrentTableSP-1
                    Endif
                Enddo
                * --- ci appiccica tutte le inner join
                bStop=.F.
                Do While Not(bStop)
                    bStop=.T.
                    For i=1 To .nJoins
                        If Not(.i_Joins[i,aJ_Used])
                            Do Case
                                Case .IsUsed(.i_Joins[i,aJ_NomeF2]) And !.IsUsed(.i_Joins[i,aJ_NomeF3])
                                    * Il primo c'e', il secondo no
                                    .UsedFile(.i_Joins[i,aJ_NomeF3])
                                    * --- proviamo a spostare la Inner Join nella parte di Where
                                    cJ1=cJ1+','+.SearchFile(.i_Joins[i,aJ_NomeF3])+.i_Joins[i,aJ_NomeF3]
                                    .cWhere=.cWhere+Iif(Empty(.cWhere),'',' and ')+' ('+.i_Joins[i,aJ_Expr]+') '
                                    .i_Joins[i,aJ_Used]=.T.
                                    bStop=.F.
                                Case !.IsUsed(.i_Joins[i,aJ_NomeF2]) And .IsUsed(.i_Joins[i,aJ_NomeF3])
                                    * --- Il primo non c'e', il secondo c'e'
                                    .UsedFile(.i_Joins[i,aJ_NomeF2])
                                    * --- proviamo a spostare la Inner Join nella parte di Where
                                    cJ1=cJ1+','+.SearchFile(.i_Joins[i,aJ_NomeF2])+.i_Joins[i,aJ_NomeF2]
                                    .cWhere=.cWhere+Iif(Empty(.cWhere),'',' and ')+' ('+.i_Joins[i,aJ_Expr]+') '
                                    .i_Joins[i,aJ_Used]=.T.
                                    bStop=.F.
                                Case .IsUsed(.i_Joins[i,aJ_NomeF2]) And .IsUsed(.i_Joins[i,aJ_NomeF3])
                                    * --- Il primo c'e', il secondo c'e'
                                    .i_Joins[i,aJ_Used]=.T.
                                    bStop=.F.
                                    If .i_Joins[i,aJ_Type]=='Where' &&'Inner'
                                        .cWhere=.cWhere+Iif(Empty(.cWhere),'',' and ')+' ('+.i_Joins[i,aJ_Expr]+') '
                                    Else
                                        CP_MSG(cp_MsgFormat(MSG_REDUNDANT_JOIN_NO_F__ ,Alltrim(Str(i))),.F.)
                                    Endif
                            Endcase
                        Endif
                    Next
                Enddo
                * ---
                .cFrom=.cFrom+Iif(Empty(.cFrom),'',',')+cPar+cJ+cJ1
            Enddo
            * --- Preparo la Where con le condizioni di "filtro"
            .cWhere=.cWhere+Iif(Empty(.cWhere),'(',' AND (')
            For i=1 To .nWhere
                If !This.WhereWithEmptyParam(i)
                    w=.i_Where[i,aW_Expr]
                    If .i_Where[i,aW_SubQuery]
                        Local i_sfn,i_ssql
                        i_sfn=.i_Where[i,aW_Param]
                        If Right(i_sfn,1)=')'
                            i_sfn=Left(i_sfn,Len(i_sfn)-1)
                        Endif
                        i_ssql=This.MakeSubQuery("i_where",i,i_sfn)
                        w=Strtran(w,i_sfn,'('+i_ssql+')')
                    Endif
                    .cWhere = .cWhere+w
                Else
                    Local jj,i_lpar,i_rpar,pp
                    i_lpar=0
                    i_rpar=0
                    For jj=1 To Len(.i_Where[i,aW_Expr])
                        pp=Substr(.i_Where[i,aW_Expr],jj,1)
                        If pp='('
                            i_lpar=i_lpar+1
                        Endif
                        If pp=')'
                            i_rpar=i_rpar+1
                        Endif
                    Next
                    If i_lpar>i_rpar
                        .cWhere=.cWhere+'('
                    Endif
                    If i_lpar<i_rpar
                        .cWhere=.cWhere+')'
                        If Right(.i_Where[i,aW_Expr],4)=' OR '
                            .cWhere=.cWhere+' OR '
                        Endif
                        If Right(.i_Where[i,aW_Expr],5)=' AND '
                            .cWhere=.cWhere+' AND '
                        Endif
                    Endif
                    *if left(.i_Where[i,aW_Expr],1)='('
                    *  .cWhere=.cWhere+'('
                    *endif
                    *if right(.i_Where[i,aW_Expr],5)=') OR ' or right(.i_Where[i,aW_Expr],6)=') AND '
                    *  if right(.cWhere,1)='('
                    *    .cWhere=left(.cWhere,len(.cWhere)-1)
                    *  else
                    *    .cWhere=left(.cWhere,len(.cWhere)-iif(right(.cWhere,4)=' OR ',4,5))+')'
                    *  endif
                    *endif
                Endif
            Next
            .cWhere=.cWhere+')'
            * --- toglie l' ultima operazione logica
            *i=rat(' ',.cWhere)
            *if i<>0 and inlist(upper(trim(substr(.cWhere,i+1))),'AND','OR')
            *  .cWhere=left(.cWhere,i)
            *endif
            * --- elimina le AND ), OR ) e () che possono essere rimaste dall' eliminazione di parametri vuoti
            .cWhere=Strtran(.cWhere,' AND )',')')
            .cWhere=Strtran(.cWhere,' OR )',')')
            .cWhere=Strtran(.cWhere,' AND ()','')
            .cWhere=Strtran(.cWhere,' OR ()','')
            .cWhere=Strtran(.cWhere,'( AND ','(')
            .cWhere=Strtran(.cWhere,'( OR ','(')
            .cWhere=Strtran(.cWhere,'() AND ','')
            .cWhere=Strtran(.cWhere,'() OR ','')

            Do While At('()',.cWhere)<>0
                .cWhere=Strtran(.cWhere,'()','')
                .cWhere=Alltrim(.cWhere)
            Enddo
            If Left(.cWhere,4)='AND '
                .cWhere=Substr(.cWhere,4)
            Endif
            If Left(.cWhere,3)='OR '
                .cWhere=Substr(.cWhere,4)
            Endif
            If Right(.cWhere,4)=' AND'
                .cWhere=Left(.cWhere,Len(.cWhere)-4)
            Endif
            If Right(.cWhere,3)=' OR'
                .cWhere=Left(.cWhere,Len(.cWhere)-3)
            Endif
            If .cWhere=='AND' Or .cWhere=='OR'
                .cWhere=''
            Endif
            * --- Where temporanee (quando la query e' usata in uno zoom)
            If .nWhereTmp>0
                If !Empty(.cWhere)
                    .cWhere='('+.cWhere+')'
                Endif
                For i=1 To .nWhereTmp
                    * --- scompone l' espressione
                    cExp=This.GetWhereTmpExpr(Alltrim(.i_WhereTmp[i,aWT_Expr]))
                    .cWhere = .cWhere+Iif(Empty(.cWhere),'',' and ')+Ltrim(cExp)
                Next
                .cWhere='('+.cWhere+')'
            Endif
            * --- Preparo la Having
            .cHaving=''
            For i=1 To .nHaving
                If !This.HavingWithEmptyParam(i)
                    If .cDatabaseType="VFP" Or .bFox26
                        w = .i_Having[i,aW_Expr]
                    Else
                        w = This.GetHavingExpr(.i_Having[i,aW_Expr])
                    Endif
                    If .i_Having[i,aW_SubQuery]
                        Local i_sfn,i_ssql
                        i_sfn=.i_Having[i,aW_Param]
                        If Right(i_sfn,1)=')'
                            i_sfn=Left(i_sfn,Len(i_sfn)-1)
                        Endif
                        i_ssql=This.MakeSubQuery("i_having",i,i_sfn)
                        w=Strtran(w,i_sfn,'('+i_ssql+')')
                    Endif
                    .cHaving = .cHaving+w
                Endif
            Next
            * --- toglie l' ultima operazione logica
            .cHaving=Trim(.cHaving)
            i=Rat(' ',.cHaving)
            If i<>0 And Inlist(Upper(Trim(Substr(.cHaving,i+1))),'AND','OR')
                .cHaving=Left(.cHaving,i)
            Endif
            * --- Preparo la Order By
            .PrepareOrderBy()
            If !Empty(.cOrderByTmp)
                k=.cOrderByTmp
                i=At(' desc',Lower(k))
                If i<>0
                    k=Left(k,i-1)
                Endif
                If At(' '+k+' ',.cOrderBy)=0
                    .cOrderBy = .cOrderBy+Iif(Empty(.cOrderBy),'',',')+.cOrderByTmp
                Endif
            Endif
            * --- Preparo la Group By
            .cGroupBy = ""
            For i=1 To .nGroupBy
                If .cDatabaseType="VFP" Or .bFox26
                    .cGroupBy = .cGroupBy+Iif(i=1,'',',')+.i_GroupBy[i,aG_Expr]
                Else
                    *--- Zucchetti Aulla inizio - Aggiunto il secondo parametro alla GetGroupByExpr
                    .cGroupBy = .cGroupBy+Iif(i=1,'',',')+This.GetGroupByExpr(.i_GroupBy[i,aG_Expr],.cDatabaseType="PostgreSQL")
                    *--- Zucchetti Aulla fine - Aggiunto il secondo parametro alla GetGroupByExpr
                Endif
            Next
        Endwith
        Return
    Function PrepareOrderBy
        * --- Preparo la Order By
        Local i,k,j
        With This
            .cOrderBy = ""
            For i=1 To .nOrderBy
                * !!! versione che mette i campi
                *.cOrderBy = .cOrderBy+iif(i=1,'',',')+.i_OrderBy[i,aO_Field]+iif(.i_OrderBy[i,aO_AscDsc]='Ascending','',' desc')
                * !!! versione che mette 1,2 desc,3
                k=0
                For j=1 To .nFields
                    If .i_OrderBy[i,aO_Field]==.i_Fields[j,aC_Expr] Or .i_OrderBy[i,aO_field]==.i_Fields[j,aC_Nome]
                        k=j
                    Endif
                Next
                If k=0
                    .cOrderBy = .cOrderBy+Iif(i=1,'',',')+.i_OrderBy[i,aO_Field]+Iif(.i_OrderBy[i,aO_AscDsc]='Ascending','',' desc')
                Else
                    .cOrderBy = .cOrderBy+Iif(i=1,'',',')+' '+Alltrim(Str(k))+' '+Iif(.i_OrderBy[i,aO_AscDsc]='Ascending','',' desc')
                Endif
                * !!!
            Next
        Endwith
        Return
    Function MakeSubQuery(i_array,i,i_sfn)
        This.&i_array.[i,aW_oQuery]=Createobject('cpquery')
        This.&i_array.[i,aW_oQuery].bFox26=This.bFox26
        Local i_ssql,i_sloader
        i_sloader=Createobject('cpqueryloader',This.bFox26)
        i_sloader.LoadQuery(Substr(i_sfn,2),This.&i_array.[i,aW_oQuery],This,.T.)&& Zse Subquery in filtro non metto order by
        i_ssql=This.&i_array.[i,aW_oQuery].mDoQuery('nocursor','Get',.Null.)
        Return(i_ssql)
    Func FindAJoin(cTable)
        Local i
        For i=1 To This.nJoins
            If Not(This.i_Joins[i,aJ_Used]) And (This.i_Joins[i,aJ_NomeF2]==cTable Or This.i_Joins[i,aJ_NomeF3]==cTable) And This.i_Joins[i,aJ_Type]<>'Where'
                Return(i)
            Endif
        Next
        Return(0)
    Func GetGroupByExpr(cExp,bUseNumberCol)
        Local i,res
        For i=1 To This.nFields
            If cExp==This.i_Fields[i,aC_Nome]
            *--- Zucchetti Aulla inizio - su database PostGreSQL metto il numero nelle espressioni
                res=This.i_Fields[i,aC_Expr]
                If bUseNumberCol AND ("["$res Or "'"$res Or "?"$res)
                    res=Alltrim(Str(i,3,0))
                Endif
                Return(res)
            *--- Zucchetti Aulla fine - metto il numero nelle espressioni
            Endif
        Next
        Return(cExp)
    Function GetWhereTmpExpr(i_cExp)
        Local l_char, l_pos, l_ret, l_SetCharKey, l_StrTest, l_nField, l_i, l_bApice, l_bDblApice, l_nExp
        l_SetCharKey = ' (),[]+-/*=<>'		&&Elenco caratteri chiave, utilizzati per l'identificazione degli alias
        l_StrTest = ''	&&Stringa contenente l'alias da ricercare in i_Fields
        l_ret = ''		&&Nuova where da ritornare
        l_bApice = .F.	&&Ho incontrato un apice?
        l_bDblApice = .F.	&&Ho incontrato un doppio apice?
        l_nExp = Len(m.i_cExp)
        *--- Ciclo su ogni singolo carattere della where, aggiungo 1 perch� devo fare un ultimo controllo sull'ultima porzione di stringa, l'ultima parte potrebbe essere un alias da sostituire
        For l_pos = 1 To m.l_nExp + 1
            *--- Leggo un carattere, se ultimo inserisco ''
            l_char = Iif(l_pos<=m.l_nExp, Substr(m.i_cExp, m.l_pos, 1), '')
            *--- Ho incontrato un '/" in precedenza? In questo caso sono in una stringa
            If  m.l_bApice Or m.l_bDblApice
                m.l_ret = m.l_ret + m.l_char
                *--- Se rincontro un ' o " non sono pi� in una stringa
                m.l_bApice = m.l_bApice And !(m.l_bApice And m.l_char == ['])
                m.l_bDblApice= m.l_bDblApice And !(m.l_bDblApice And m.l_char == ["])
            Else
                Do Case
                        *--- Se incontro ' o " entro in una stringa
                    Case m.l_char $ ['"]
                        m.l_bApice = m.l_char == [']
                        m.l_bDblApice = m.l_char == ["]
                        m.l_ret = m.l_ret + m.l_char
                        *--- Se incontro un carattere chiave devo cercare l_StrTest in i_Fields
                    Case m.l_char $ m.l_SetCharKey Or Empty(m.l_char)
                        l_nField = 0
                        For l_i = 1 To .nFields
                            If Upper(Alltrim(.i_Fields[m.l_i, aC_Nome])) == Upper(Alltrim(m.l_StrTest))
                                l_nField = m.l_i
                                Exit
                            Endif
                        Next
                        If m.l_nField > 0 And !Empty(l_StrTest)
                            *--- Se trovo una corrispondenza faccio la sostituzione
                            m.l_ret = m.l_ret + .i_Fields[m.l_nField, aC_Expr] + m.l_char
                        Else
                            *--- Se non trovo una corrispondenza concateno l_StrTest al risultato
                            m.l_ret = m.l_ret + m.l_StrTest + m.l_char
                        Endif
                        *--- Svuoto l_StrTest per riniziare con una nuova porzione di where
                        l_StrTest = ''
                    Otherwise
                        *--- Se l_char non � ', " o un carattere chiave devo concatenare, sto costruendo l'alias da ricercare in i_Fields
                        l_StrTest = m.l_StrTest + m.l_char
                Endcase
            Endif	&&m.l_bApice OR m.l_bDblApice
        Next
        *--- Ritorno il risultato
        Return(Alltrim(m.l_ret))
    Function GetHavingExpr(i_cExp)
        Local i_i,i_cExp1,i_bGroupExp,i_bNot
        i_cExp1=''
        i_bGroupExp=.F.
        i_bNot=.F.
        If Upper(Left(i_cExp,4))='NOT('
            i_bNot=.T.
            i_cExp=Substr(i_cExp,5,Len(i_cExp)-5)
        Endif
        * --- elimina l' eventuale nome del file nella sintassi a.b creata dal drag&drop
        i_i=At(' ',i_cExp)
        If i_i<>0
            i_cExp1=Substr(i_cExp,i_i)
            i_cExp=Left(i_cExp,i_i-1)
            i_i=At('(',i_cExp)
            If i_i=0
                i_i=At('.',i_cExp)
                If i_i<>0
                    i_cExp=Substr(i_cExp,i_i+1)
                Endif
            Else
                i_bGroupExp=.T.
            Endif
        Endif
        * --- sostituisce il nome del campo con l' espressione
        If !i_bGroupExp
            For i_i=1 To This.nFields
                i_cExp=Strtran(i_cExp,This.i_Fields[i_i,aC_Nome],This.i_Fields[i_i,aC_Expr])
            Next
        Endif
        If i_bNot
            i_cExp='NOT('+i_cExp
            i_cExp1=i_cExp1+')'
        Endif
        Return(i_cExp+i_cExp1)
        * Zucchetti Aulla Inizio - Problema Having rimessa la vecchia funzione - NG
    Function GetHavingExpr(cExp)
        Local i
        For i=1 To This.nFields
            cExp=Strtran(cExp,This.i_Fields[i,aC_Nome],This.i_Fields[i,aC_Expr])
        Next
        Return(cExp)
        * Zucchetti Aulla Fine - Problema Having rimessa la vecchia funzione - NG
    Func FirstUnusedFile()
        Local i,N
        For i=1 To This.nFiles
            If !This.IsUsed(This.i_Files[i,aF_NomeL]) And This.IsMasterOfOuterJoin(This.i_Files[i,aF_NomeL])
                Return(i)
            Endif
        Next
        For i=1 To This.nFiles
            If !This.IsUsed(This.i_Files[i,aF_NomeL]) And This.HasOuterJoin(This.i_Files[i,aF_NomeL])
                Return(i)
            Endif
        Next
        For i=1 To This.nFiles
            If !This.IsUsed(This.i_Files[i,aF_NomeL])
                Return(i)
            Endif
        Next
        Return(0)
    Func IsMasterOfOuterJoin(cAlias)
        Local i,j,tj
        j=0 && conta in quante Left o Right join e' incluso questo alias
        For i=1 To This.nJoins
            If !This.i_Joins[i,aJ_used]
                tj=Left(This.i_Joins[i,aJ_Type],4)
                If Inlist(tj,'Left','Righ') And ((This.i_Joins[i,aJ_NomeF2]==cAlias) Or (This.i_Joins[i,aJ_NomeF3]==cAlias))
                    If (This.i_Joins[i,aJ_NomeF2]==cAlias And tj<>'Left') Or (This.i_Joins[i,aJ_NomeF3]==cAlias And tj<>'Righ')
                        *? cAlias+' fallisce perche '+this.i_Joins[i,aJ_NomeF2]+' '+this.i_Joins[i,aJ_NomeF3]+' '+this.i_Joins[i,aJ_Type]
                        Return(.F.)
                    Endif
                    j=j+1
                Endif
            Endif
        Next
        * una tabella e' master se lo e' in tutte le join in cui e' inclusa ed e' inclusa almeno in una Left o in una Right
        Return(j>0)
    Func HasOuterJoin(cAlias)
        Local i
        For i=1 To This.nJoins
            If !This.i_Joins[i,aJ_used]
                If (This.i_Joins[i,aJ_NomeF1]==cAlias Or This.i_Joins[i,aJ_NomeF2]==cAlias) And This.i_Joins[i,aJ_Type]<>'Where' &&'Inner'
                    Return(.T.)
                Endif
            Endif
        Next
        Return(.F.)
    Proc UsedFile(cAlias)
        This.i_cntf1=This.i_cntf1+1
        Dimension This.i_FileInUse[this.i_cntf1]
        This.i_FileInUse[this.i_cntf1]=cAlias
        Return
    Func IsUsed(cAlias)
        Local i
        For i=1 To This.i_cntf1
            If This.i_FileInUse[i]==cAlias
                Return(.T.)
            Endif
        Next
        Return(.F.)
    Func mDoQuery(i_cCursor,i_oper,i_sqlx,i_bNoFilter,i_cTempTable,i_indexes,i_addexisting,i_cError)
        Local i_cSelect,i_cFields,i_cWhere,i_cHaving,i_cFrom,i_cOrder,i_cGroup,i_cUnion,i_timedmsg,i_cNoFilter
        Local i_p,i_batch,i_oldazi
        i_cSelect=''
        i_cWhere=''
        i_cHaving=''
        i_cFrom=''
        i_cOrder=''
        i_cGroup=''
        i_cFields=''
        i_cUnion=''
        i_cNoFilter=Iif(i_bNoFilter Or This.nPivotFlds<>0,'nofilter','')
        *--- Zucchetti Aulla inizio (test automatico VQR)
        Local i_bTestVqr
        i_bTestVqr=Vartype( g_testvar )='C'
        *--- Zucchetti Aulla fine (test automatico VQR)
        *--- Zucchetti Aulla inizio - campi Memo per PostgreSQL
        Private i_cMemoFieldsPostgreSQL,i_cC254FieldsPostgreSQL
        i_cMemoFieldsPostgreSQL=''
        i_cC254FieldsPostgreSQL=''
        *--- Zucchetti Aulla fine - campi Memo per PostgreSQL
        If This.nFilterParam<>0 And This.bAskParam
            *--- Zucchetti Aulla inizio (Gestione parametri Portale)
            If  g_IZCP='S' And  Vartype(g_ZCPSETPARAM)='C' And Not(Empty(g_ZCPSETPARAM))
                * Prova ad assegnare ai parametri
                This.bAskParam = ZSETPARAM(This,g_ZCPSETPARAM,'VQR')
            Endif
            If This.bAskParam
                *--- Zucchetti Aulla fine (Gestione parametri Portale)
                This.ExtParamMask()
                This.ParamMask()
                This.bAskParam=.F.
            Endif
            *--- Zucchetti Aulla inizio (Gestione parametri Portale)
        Endif
        *--- Zucchetti Aulla fine (Gestione parametri Portale)
        * --- Zucchetti Aulla inizio - vero tipo del parametro per postgres
        If This.nFilterParam<>0 And This.cDatabaseType="PostgreSQL"
            For iFilp=1 To This.nFilterParam
                If This.i_FilterParamRealType.GetKey(This.i_FilterParam[iFilp,aFP_Name])>0
                    This.i_FilterParamRealType.Remove(This.i_FilterParam[iFilp,aFP_Name])
                Endif
                This.i_FilterParamRealType.Add(Vartype(This.i_FilterParam[iFilp,aFP_Value]),This.i_FilterParam[iFilp,aFP_Name])
            Next
        Endif
        * --- Zucchetti Aulla fine - vero tipo del parametro per postgres
        If !This.bFox26
            i_timedmsg=Createobject('timedmsg',cp_Translate(MSG_EXECUTING_QUERY_D),3)
        Endif
        If This.bMultiCompany
            i_oldazi=i_codazi
            i_codazi='@@@###!!!^^^@@@'
        Endif
        This.mCreateSelect()
        i_cFields = This.cFields
        * --- Zucchetti Aulla - bindvariable di oracle
        If Type("this.oParentObject.bNoBindVariable")='L'
            This.oParentObject.bNoBindVariable=(Upper(CP_DBTYPE)='ORACLE' And (Type("g_BINDVARIABLE")<>'L' Or g_BINDVARIABLE) And (Atc("'", i_cFields)>0 Or Atc("[SPACES(", i_cFields)>0)) Or This.oParentObject.bNoBindVariable
        Else
            This.bNoBindVariable=(Upper(CP_DBTYPE)='ORACLE' And (Type("g_BINDVARIABLE")<>'L' Or g_BINDVARIABLE) And (Atc("'", i_cFields)>0 Or Atc("[SPACES(", i_cFields)>0)) Or This.bNoBindVariable
        Endif
        * --- Zucchetti Aulla fine - bindvariable di oracle
        i_cFrom   = "from "+This.cFrom
        i_cSelect = "select "+i_cFields+" "+i_cFrom
        If Len(Trim(This.cJoin))<>0
            i_cSelect = i_cSelect+" where "+This.cJoin
            i_cWhere = "where "+This.cJoin
            If Len(Trim(This.cWhere))<>0
                i_cSelect = i_cSelect+" and "+This.cWhere
                i_cWhere = i_cWhere+" and "+This.cWhere
            Endif
        Else
            If Len(Trim(This.cWhere))<>0
                i_cSelect = i_cSelect+" where "+This.cWhere
                i_cWhere = i_cWhere+"where "+This.cWhere
            Endif
        Endif
        If Len(Trim(This.cGroupBy))<>0
            i_cSelect = i_cSelect+" group by "+This.cGroupBy
            i_cGroup = "group by "+This.cGroupBy
        Endif
        If Len(Trim(This.cHaving))<>0
            i_cSelect = i_cSelect+' having '+This.cHaving
            i_cHaving = ' having '+This.cHaving
        Endif
        * --- gestione della Union
        If !Empty(This.cUnion)
            Local i_l,i_s,i_i
            This.oUnion=Createobject('cpquery')
            This.oUnion.bFox26=This.bFox26
            i_l=Createobject('cpqueryloader',This.bFox26)
            i_l.LoadQuery(Trim(This.cUnion),This.oUnion,This,.T.,.T.)
            This.oUnion.cDatabaseType=This.cDatabaseType
            For i_i=1 To This.nWhereTmp
                This.oUnion.mLoadWhereTmp(This.i_WhereTmp[i_i])
            Next
            i_s=This.oUnion.mDoQuery('nocursor','Get',.Null.)
            i_cSelect=i_cSelect+' union '+Iif(This.bUnionAll,'all ','')+i_s
            i_cUnion=' union '+Iif(This.bUnionAll,'all ','')+i_s
            * --- Zucchetti Aulla - bindvariable di oracle
            If Type("this.oParentObject.bNoBindVariable")='L' And This.bNoBindVariable
                This.oParentObject.bNoBindVariable=This.oParentObject.bNoBindVariable Or This.bNoBindVariable
            Endif
            * --- Zucchetti Aulla fine - bindvariable di oracle
        Endif
        If Len(Trim(This.cOrderBy))<>0
            i_cSelect = i_cSelect+" order by "+This.cOrderBy
            i_cOrder = "order by "+This.cOrderBy
        Endif
        If i_cCursor<>'nocursor' && per la union
            i_cSelect = i_cSelect+" into cursor "+i_cCursor
        Endif
        i_cFields=cp_SetSQLFunctions(i_cFields,This.cDatabaseType,This)
        i_cFrom=cp_SetSQLFunctions(i_cFrom,This.cDatabaseType,This)
        i_cWhere=cp_SetSQLFunctions(i_cWhere,This.cDatabaseType,This)
        i_cGroup=cp_SetSQLFunctions(i_cGroup,This.cDatabaseType,This)
        i_cHaving=cp_SetSQLFunctions(i_cHaving,This.cDatabaseType,This)
        * i_cSelect possiamo calcolarla sulla base degli altri token
        *i_cSelect=cp_SetSQLFunctions(i_cSelect,this.cDatabaseType)
        If This.nFilterParam<>0
            i_cFields=This.FillParam(i_cFields)
            i_cFrom=This.FillParam(i_cFrom)
            i_cWhere=This.FillParam(i_cWhere)
            i_cGroup=This.FillParam(i_cGroup)
            i_cHaving=This.FillParam(i_cHaving)
            * i_cSelect possiamo calcolarla sulla base degli altri token
            *i_cSelect=this.FillParam(i_cSelect)
        Endif
        * --- cambia gli eventuali =NULL in IS NULL
        i_cFields=Strtran(i_cFields,' = NULL',' IS NULL')
        i_cFrom=Strtran(i_cFrom,' = NULL',' IS NULL')
        i_cWhere=Strtran(i_cWhere,' = NULL',' IS NULL')
        i_cHaving=Strtran(i_cHaving,' = NULL',' IS NULL')
        * i_cSelect possiamo calcolarla sulla base degli altri token
        *i_cSelect=Strtran(i_cSelect,' = NULL',' IS NULL')
        ****** Zucchetti Aulla Inizio
        ****** Gestione delle date NULL
        If CP_DBTYPE='DB2'
            Private filnul, nn
            Dimension filnul[6,2]
            filnul[1,1]='<= NULL'
            filnul[1,2]=" <{d '1753-01-01'} "
            filnul[2,1]='>= NULL'
            filnul[2,2]=" <{d '1753-01-01'} "
            filnul[3,1]='<> NULL'
            filnul[3,2]=' IS NOT NULL'
            filnul[4,1]='> NULL'
            filnul[4,2]=" <{d '1753-01-01'} "
            filnul[5,1]='< NULL'
            filnul[5,2]=" <{d '1753-01-01'} "
            filnul[6,1]='between NULL AND NULL'
            filnul[6,2]="between {d '1753-01-01'} AND {d '1753-01-01'} "
            For nn= 1 To 6
                i_cFields=Strtran(i_cFields, filnul[nn,1], filnul[nn,2])
                i_cWhere=Strtran(i_cWhere, filnul[nn,1], filnul[nn,2])
                i_cHaving=Strtran(i_cHaving, filnul[nn,1], filnul[nn,2])
                *** Zucchetti Aulla - inutile comporre nuovamente la i_cSelect perch� costruita dagli altri assegnamenti
                *     i_cSelect=Strtran(i_cSelect, filnul[nn,1], filnul[nn,2])
            Endfor
        Endif
        ****** Zucchetti Aulla Fine

        i_cSelect="select "+i_cFields+" "+i_cFrom+" "+i_cWhere+" "+i_cGroup+" "+i_cHaving+" "+i_cUnion+" "

        * --- gestione della multicompany
        If This.bMultiCompany
            i_codazi=i_oldazi
            Local i_azi,i_p,i_newsel
            i_newsel=''
            i_azi=Iif(Empty(This.cCompanyList),i_codazi,This.cCompanyList)
            * --- con l' asterisco mette tutte le aziende
            If Left(i_azi,1)='*'
                i_nConn=I_SERVERCONN[1,2]
                i_cTable='cpazi'
                If i_nConn<>0
                    cp_sqlexec(i_nConn,"select * from "+i_cTable+" cpazi ","_Curs_cpazi")
                Else
                    Select * From (i_cTable)  Into Cursor _Curs_cpazi
                Endif
                If Used('_Curs_cpazi')
                    Select _Curs_cpazi
                    Locate For 1=1
                    Do While Not(Eof())
                        If Empty(i_newsel)
                            i_newsel=i_newsel+Alltrim(codazi)
                        Else
                            i_newsel=i_newsel+","+Alltrim(codazi)
                        Endif
                        Select _Curs_cpazi
                        Continue
                    Enddo
                    Use
                Endif
                i_azi=i_newsel
                i_newsel=''
            Endif
            i_p=At(',',i_azi)
            Do While i_p>0
                i_newsel=i_newsel+Iif(Empty(i_newsel),'',' UNION ALL ')+Strtran(i_cSelect,'@@@###!!!^^^@@@',Left(i_azi,i_p-1))
                i_azi=Substr(i_azi,i_p+1)
                i_p=At(',',i_azi)
            Enddo
            i_newsel=i_newsel+Iif(Empty(i_newsel),'',' UNION ALL ')+Strtran(i_cSelect,'@@@###!!!^^^@@@',Alltrim(i_azi))
            i_cSelect=i_newsel
        Endif

        i_cSelect=i_cSelect+i_cOrder

        *--- Zucchetti Aulla inizio - se stringa vuota restituisce memo anzich� varchar
        If Upper(CP_DBTYPE)='POSTGRESQL' 
            i_cSelect=STRTRAN(i_cSelect, "''||'' as ", "''||' ' as ")
        Endif
        *--- Zucchetti Aulla fine - se stringa vuota restituisce memo anzich� varchar

        * --
        Do Case
            Case i_oper='View'
                *cp_ErrorMsg(i_cSelect,64,'Select Expression')
                Public msgwnd
                msgwnd=Createobject('mkdbmsg_noerr')
                msgwnd.Caption='SQL Statement'
                msgwnd.AlwaysOnTop=.T.
                msgwnd.Show()
                msgwnd.Add(i_cSelect)
                msgwnd.End('')
            Case i_oper='Get'
                This.ClearQueryObjects(i_oper)
                Return(i_cSelect)
            Otherwise
                If This.nConn<0 And !This.bFox26
                    If This.nFiles>0
                        *--- Zucchetti Aulla inizio - Debug query
                        If Not i_bTestVqr
                            *--- Zucchetti Aulla fine - Debug query
							if Type("i_cError")='C'
								i_cError=cp_Translate(MSG_TABLES_CONNECTED_TO_DIFFERENT_SERVERS)
							Else
								cp_ErrorMsg(Iif(Not Empty(This.cQryName),This.cQryName+" - ","")+cp_Translate(MSG_TABLES_CONNECTED_TO_DIFFERENT_SERVERS),16,MSG_ERROR_EXECUTING_QUERY,.f.)
							EndIf
                            *--- Zucchetti Aulla inizio - Debug query
                        Else
                            g_testvar=cp_Translate(MSG_TABLES_CONNECTED_TO_DIFFERENT_SERVERS)
                        Endif
                        *--- Zucchetti Aulla fine - Debug query
                    Else
                        *--- Zucchetti Aulla inizio - Debug query
                        If Not i_bTestVqr
                            *--- Zucchetti Aulla fine - Debug query
							if Type("i_cError")='C'
								i_cError=cp_Translate(MSG_EMPTY_QUERY_QM)
							Else							
								cp_ErrorMsg(Iif(Not Empty(This.cQryName),This.cQryName+" - ","")+cp_Translate(MSG_EMPTY_QUERY_QM),16,MSG_ERROR_EXECUTING_QUERY,.f.)
							EndIf
                            *--- Zucchetti Aulla inizio - Debug query
                        Else
                            g_testvar=cp_Translate(MSG_EMPTY_QUERY_QM)
                        Endif
                        *--- Zucchetti Aulla fine - Debug query
                    Endif
                    This.ClearQueryObjects(i_oper)
                    Return(.F.)
                Endif
                i_cOldError=On("ERROR")
                Private i_err
                i_err=.F.
                On Error i_err=.T.
                If Used(i_cCursor)
                    Select (i_cCursor)
                    Use
                Endif
                *cp_ErrorMsg("select "+i_cFields+" "+i_cFrom+" "+i_cWhere+" "+i_cGroup+" "+i_cHaving+" "+i_cUnion+" "+i_cOrder,48,'',.f.)
                If This.nConn=0 Or This.bFox26
                    *i_cSelect=i_cFields+' '+i_cFrom+' '+i_cWhere+' '+i_cGroup+' '+i_cHaving+' '+i_cUnion+' '+i_cOrder+' '+i_cNoFilter
                    * i_cSelect=cp_SetSQLFunctions(i_cSelect,'VFP')
                    i_cSelect=i_cSelect+' '+i_cNoFilter
                    If Type("i_cTempTable")='C'
                        &i_cSelect Into Table &i_cTempTable
                    Else
                        &i_cSelect Into Cursor &i_cCursor
                        Locate For 1=1 && stabilizza i cursori "finti"
                    Endif
                Else
                    ***** Modifica per multi company
                    If Type("i_cTempTable")='C'
                        If This.bMultiCompany
                            i_azi=Iif(Empty(This.cCompanyList),i_codazi,This.cCompanyList)
                            i_p=At(',',i_azi)
                            If i_p>0
                                i_cSelect=Right(i_cSelect,Len(i_cSelect)-Len("select "+Strtran(i_cFields,'@@@###!!!^^^@@@',Left(i_azi,i_p-1))))
                                i_cFields=Strtran(i_cFields,'@@@###!!!^^^@@@',Left(i_azi,i_p-1))
                            Else
                                i_cSelect=Right(i_cSelect,Len(i_cSelect)-Len("select "+Strtran(i_cFields,'@@@###!!!^^^@@@',Alltrim(i_azi))))
                                i_cFields=Strtran(i_cFields,'@@@###!!!^^^@@@',Alltrim(i_azi))
                            Endif
                        Else
                            *****************************************************************
                            * La creazione  della tabella temporanea con order by va in errore su db oracle
                            If CP_GETDATABASETYPE(This.nConn)="Oracle" And Not(Type("g_OracleOrderBy")='L' And g_OracleOrderBy)
                                i_cSelect=' '+i_cFrom+' '+i_cWhere+' '+i_cGroup+' '+i_cHaving+' '+i_cUnion
                            Else
                                i_cSelect=' '+i_cFrom+' '+i_cWhere+' '+i_cGroup+' '+i_cHaving+' '+i_cUnion+' '+i_cOrder
                            Endif
                        Endif
                        If i_addexisting
                            * --- Zucchetti Aulla - Bind variable Oracle
                            If !cp_InsertIntoTempTable(This.nConn,i_cTempTable,Iif(Upper(CP_DBTYPE)='ORACLE' And (Type("g_BINDVARIABLE")<>'L' Or g_BINDVARIABLE) And This.bNoBindVariable,'/*+ CURSOR_SHARING_EXACT */ ','')+i_cFields,i_cSelect,i_sqlx)
                                i_err=.T.
                            Endif
                        Else
                            ****** Zucchetti Aulla Inizio - per la creazione della tabella temporanea semplifico la query di creazione
                            * Zucchetti Aulla Fine (aggiunto parametro alla chiamata sotto)
                            *if !cp_CreateTempTable(this.nConn,i_cTempTable,i_cFields,i_cSelect,i_sqlx,@i_indexes)
                            If !cp_CreateTempTable(This.nConn,i_cTempTable,Iif(Upper(CP_DBTYPE)='ORACLE' And (Type("g_BINDVARIABLE")<>'L' Or g_BINDVARIABLE) And This.bNoBindVariable,'/*+ CURSOR_SHARING_EXACT */ ','')+i_cFields,i_cSelect,i_sqlx,@i_indexes,Iif(CP_GETDATABASETYPE(This.nConn)="Oracle",Iif(This.bMultiCompany, Strtran(" "+i_cFrom + " where 1=0 " + i_cGroup,'@@@###!!!^^^@@@',Iif(At(',',i_azi)-1>0,Alltrim(Left(i_azi,At(',',i_azi)-1)),Alltrim(i_azi)))," "+i_cFrom + " where 1=0 " + i_cGroup),""))
                                i_err=.T.
                            Endif
                        Endif
                    Else
                        *i_cSelect="select "+i_cFields+i_cSelect
                        * --- Zucchetti Aulla - se sono su oracle con bind variable per la creazione del cursore
                        * --- metto un hint per non utilizzarle perch� il cursore che verrebbe creato nel caso
                        * --- di valori costanti mi restituirebbe un valore errato
                        If Upper(CP_DBTYPE)='ORACLE' And (Type("g_BINDVARIABLE")<>'L' Or g_BINDVARIABLE) And This.bNoBindVariable
                            i_cSelect=Stuff(i_cSelect, At('select ',Lower(i_cSelect)), 7, 'select /*+ CURSOR_SHARING_EXACT */ ')
                        Endif
                        * --- Zucchetti Aulla Aggiunta Or per svolgere in
                        * --- modo sincrono le query su tabelle temporanee
                        If Type("i_sqlx")='L' Or At('cptmp_',i_cSelect)<> 0
                            If cp_sqlexec(This.nConn,i_cSelect,i_cCursor)=-1
                                i_err=.T.
                            Endif
                        Else
                            If i_sqlx.SQLExec(This.nConn,i_cSelect,i_cCursor,This.cDatabaseType)=-1
                                i_err=.T.
                            Endif
                        Endif
                    Endif
                Endif
                On Error &i_cOldError
                If i_err
                    *--- Zucchetti Aulla inizio - Debug query
                    If Not i_bTestVqr
                        *--- Zucchetti Aulla fine - Debug query
						if Type("i_cError")='C'
							i_cError="/* "+Message()+" */"+Chr(13)+Chr(10)+Chr(13)+Chr(10)+i_cSelect
						Else						
							* --- In caso di errore con la Right vedo il msg a discapito della frase
							If cp_YesNo(Right(cp_MsgFormat(MSG_SELECT__ ,i_cSelect)+Chr(13)+Chr(10)+cp_MsgFormat(MSG_ERROR_CL__ ,Message())+cp_Translate(MSG_SAVE_TO_CLIPBOARD_QP),254),16,.f.,.f.,MSG_ERROR_EXECUTING_QUERY)
								_Cliptext=Iif(Not Empty(This.cQryName),This.cQryName+" - ","")+i_cSelect+Chr(13)+Chr(10)+Message()
								*--- Zucchetti Aulla inizio - Debug query
							Endif
						EndIf
                    Else
                        g_testvar=i_cSelect+Chr(13)+Chr(10)+Message()
                        *--- Zucchetti Aulla fine - Debug query
                    Endif
                    This.ClearQueryObjects(i_oper)
                    Return(.F.)
                Endif
                *--- Zucchetti Aulla inizio - Converto a char i campi Memo non previsti sulla visual query
                If NOT i_err AND Upper(CP_DBTYPE)='POSTGRESQL' AND USED(i_cCursor) 
                    This.CastMemoToChar(i_cCursor,i_cMemoFieldsPostgreSQL,i_cC254FieldsPostgreSQL)
                Endif
                *--- Zucchetti Aulla fine - Converto a char i campi Memo non previsti sulla visual query
                If not i_bTestVqr AND This.nPivotFlds<>0
                    This.DoPivot(i_cCursor)
                Endif
        Endcase
        This.ClearQueryObjects(i_oper)
        Return(.T.)
    Func ClearQueryObjects(i_oper)
        This.mClearWhereTmp()
        This.mClearOrderByTmp()
        If i_oper<>'Get'
            Local i_i
            For i_i=1 To This.nWhere
                If !Isnull(This.i_Where[i_i,aW_oQuery])
                    This.i_Where[i_i,aW_oQuery].Destroy()
                Endif
                This.i_Where[i_i,aW_oQuery]=.Null.
            Next
            If !Isnull(This.oUnion)
                * Le destroy lanciate dalle union
                * devono verificare quando sono in stato exec per lanciare le eventuali tabelle temporanee
                This.oUnion.Destroy(i_oper)
                This.oUnion=.Null.
            Endif
        Else
            * --- tolta chiusura tabelle in fase di get: problema AHW su union.
            * --- Le tabelle vengono chiuse alla destroy della query
            ***************************************************************************
            * Metto in disuso le tabelle anche nel caso di delete da query
            For i=1 To This.nFiles
                i_cNomeF=This.i_Files[i,aF_NomeF]
                If This.bFox26
                    *    if used(i_cNomeF)
                    *      if i_dcx.istabella(i_cNomeF)
                    *        select (i_cNomeF)
                    *        use
                    *      else
                    *        do cplu_clf with (i_cNomeF)
                    *      endif
                    *    endif
                Else
                    cp_CloseTable(i_cNomeF)
                Endif
            Next
            * ---
            * Nuova variabile per salvataggio numero
            * tabelle aperte da chiudere nella destroy della union quando siamo in stato exec
            This.nFilesOld=This.nFiles
            This.nFiles=0  && non ha piu' file aperti
            ***************************************************************************
        Endif
        Wait Clear
        Return(.T.)
    Func SearchFile(i_cAlias)
        Local i,j,i_NT,F
        j=0
        For i=1 To This.nFiles
            If This.i_Files[i,aF_NomeL]==i_cAlias
                j=i
                Exit
            Endif
        Next
        If j=0
            Return('')
        Endif
        F=This.i_Files[j,aF_NomeF]
        If !This.bFox26
            i_NT=cp_TablePropScan(F)
            If i_NT<>0
                Do Case
                    Case i_bSecurityRecord And Vartype(This.runtime_filters)='N' And This.runtime_filters=1
                        F=cp_SetAzi(i_TableProp[i_NT,2],.T.,i_NT,This.cQryName)
                    Case i_bSecurityRecord And Not cp_IsAdministrator(.T.) And (Vartype(This.runtime_filters)<>'N' Or This.runtime_filters=0) And Type("i_TablePropSec[n,1]")='C'
                        Do cp_secctrl With Alltrim(i_TableProp[i_NT,1])+'@'+Alltrim(This.oParentObject.getSecurityCode())+'@'
                        Clear Events
                        On Shutdown
                        Quit
                    Otherwise
                        F=cp_SetAzi(i_TableProp[i_NT,2])
                Endcase
            Endif
        Endif
        If F==This.i_Files[j,aF_NomeL]
            Return('')
        Else
            Return(F+' ')
        Endif
    Proc ParamMask()
        Local oM,i,j,o
        j=0
        *oM=createobject('query_param_mask',this,this.bFox26) && Zucchetti Aulla Test automatico VQR
        *--- Zucchetti Aulla Inizio - se query lanciata da Gadget non mostro richiesta parametri
        m.o = This.oParentObject
        If Vartype(m.o)='O' And Lower(m.o.Class)="cpquery" And PemStatus(m.o,"oParentObject",5) && query di query
        	m.o = m.o.oParentObject
        Endif
        *--- Zucchetti Aulla Fine - se query lanciata da Gadget non mostro richiesta parametri
        For i=1 To This.nFilterParam
            If !This.i_FilterParam[i,aFP_HasValue]
                *- Zucchetti Aulla Inizio Test automatico Visual Query...
                *--- Zucchetti Aulla - se query lanciata da Gadget non mostro richiesta parametri
                If Vartype( g_testvar )='C' Or (Vartype(m.o)='O' And PemStatus(m.o,"ParentClass",5) And Lower(m.o.ParentClass)=="stdgadgetform")
                    * --- se parametro non definito lo definisco in base
                    * --- al tipo..
                    Local i_res
                    i_res=""
                    Do Case
                        Case This.i_FilterParam[i,aFP_Type]='C'
                            i_res=Repl('1',This.i_FilterParam[this.nFilterParam,aFP_Len])
                        Case This.i_FilterParam[i,aFP_Type]='M'
                            i_res='1'
                        Case This.i_FilterParam[i,aFP_Type]='N' Or This.i_FilterParam[i,aFP_Type]='Y'
                            i_res=1.00
                        Case This.i_FilterParam[i,aFP_Type]='D' Or This.i_FilterParam[i,aFP_Type]='T'
                            i_res=i_datsys
                        Case This.i_FilterParam[i,aFP_Type]='L'
                            i_res=.F.
                    Endcase
                    This.i_FilterParam[i,aFP_HasValue]=.T.
                    This.i_FilterParam[i,aFP_Value]=i_res
                    
                    *--- Zucchetti Aulla Inizio - valorizzo l'alert del Gadget
                    If Vartype(m.o)='O' And PemStatus(m.o,"ParentClass",5) And Lower(m.o.ParentClass)=="stdgadgetform" And PemStatus(m.o,"cAlertMsg",5)
                    	If Empty(m.o.cAlertMsg)
                    		m.o.cAlertMsg = ah_MsgFormat("Parametro non trovato: %1",This.i_FilterParam[i,aFP_Name])
                    	Else
                    		m.o.cAlertMsg = ah_MsgFormat("%1%0Parametro non trovato: %2",m.o.cAlertMsg,This.i_FilterParam[i,aFP_Name])
                    	Endif
                    Endif
                    *--- Zucchetti Aulla Fine - valorizzo l'alert del Gadget
                Else
                    * --- commentata CreateObject sopra..
                    If j=0
                        *--- Zucchetti Aulla inizio - Debug query
                        oM=Createobject('query_param_mask',This,This.bFox26, This.cQryName)
                        *--- Zucchetti Aulla fine - Debug query
                    Endif
                    *- Zucchetti Aulla Fine Test automatico Visual Query...
                    oM.Add(i,This.i_FilterParam[i,aFP_Name],This.i_FilterParam[i,aFP_Descr],This.i_FilterParam[i,aFP_Type],This.i_FilterParam[i,aFP_Len],This.i_FilterParam[i,aFP_Dec])
                    j=j+1
                Endif
                *- Zucchetti Aulla inizio Test automatico Visual Query...
            Endif
            *- Zucchetti Aulla Fine Test automatico Visual Query...
        Next
        If j>0
            If This.bFox26
                Read When param_mask_read(oM)
            Else
                oM.Show()
            Endif
        Endif
        * ---Gestione parametri VFPMACRO, aggiunto parametro bVFP
    Func FillParam(i_cSelect,bVFP)
        Local N,i
        If '?' $ i_cSelect
            * --- Gestione parametri VFPMACRO, aggiunto <or bVFP> alla condizione sotto (La cp_valuetostr devo comportarmi come se il database fosse VFP)
            N=Iif(This.bFox26 Or bVFP,0,This.nConn)
            i_cSelect=i_cSelect+' '
            For i=1 To This.nFilterParam
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+' ',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+' ')
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+')',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+')')
                * --- Se dopo il parametro c'� una virgola o una parentesi quadra aperta lo riconosco cmq
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+',',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+',')
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+'[',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+'[')
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+'+',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+'+')
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+'-',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+'-')
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+'/',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+'/')
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+'*',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+'*')
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+'=',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+'=')
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+'<',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+'<')
                i_cSelect=Strtran(i_cSelect,'?'+This.i_FilterParam[i,aFP_Name]+'>',cp_ValueToStr(N,This.i_FilterParam[i,aFP_Value],This.i_FilterParam[i,aFP_Type])+'>')
            Next
        Endif
        Return(i_cSelect)
    Proc ExtParamMask()
        Local o,i
        If !Empty(This.cExtMask)
            If cp_IsPFile(This.cExtMask+'.vfm')
                o=vm_exec(This.cExtMask)
            Else
                i=This.cExtMask
                o=&i
            Endif
            If !Isnull(o)
                For i=1 To This.nFilterParam
                    If !This.i_FilterParam[i,aFP_HasValue] And o.isVar(This.i_FilterParam[i,aFP_Name])
                        This.i_FilterParam[i,aFP_Value]=o.GetVar(This.i_FilterParam[i,aFP_Name])
                        This.i_FilterParam[i,aFP_HasValue]=.T.
                    Endif
                Next
                If o.isVar('OrderBy')
                    This.mLoadOrderByTmp(o.GetVar('OrderBy'))
                Endif
            Endif
        Endif
    Func WhereWithEmptyParam(p)
        Local i,j,k,r
        *if !this.bRemoveWhereOnEmptyParam
        *  return(.f.)
        *endif
        r=.F.
        i=This.i_Where[p,aW_Param]
        If Type("i")='C'
            i=Trim(i)
            If Right(i,1)=')'
                i=Left(i,Len(i)-1)
            Endif
            * --- ora in i c'e' il nome del parametro da controllare se e' vuoto
            If !This.bRemoveWhereOnEmptyParam And At(i+'?',This.cSingleParametersToBeRemoved)=0
                * --- non si devono rimuovere ne tutti i parametri vuoti ne questo in particolare
                Return(.F.)
            Endif
            k=0
            For j=1 To This.nFilterParam
                If Trim('?'+This.i_FilterParam[j,aFP_Name])== i &&trim(this.i_Where[p,aW_Param])
                    k=j
                Endif
            Next
            If k<>0 And Empty(This.i_FilterParam[k,aFP_Value])
                r=.T.
            Endif
        Endif
        Return(r)
    Func HavingWithEmptyParam(p)
        Local i,j,k,r
        If !This.bRemoveWhereOnEmptyParam
            Return(.F.)
        Endif
        r=.F.
        i=This.i_Having[p,aW_Param]
        If Type("i")='C'
            k=0
            For j=1 To This.nFilterParam
                If Trim('?'+This.i_FilterParam[j,aFP_Name])==Trim(This.i_Having[p,aW_Param])
                    k=j
                Endif
            Next
            If k<>0 And Empty(This.i_FilterParam[k,aFP_Value])
                r=.T.
            Endif
        Endif
        Return(r)
    Func isVar(cName)
        Local i
        cName=Lower(cName)
        For i=1 To This.nFilterParam
            If cName==Lower(This.i_FilterParam[i,aFP_Name])
                Return(.T.)
            Endif
        Next
        If !Isnull(This.oParentObject)
            Return(This.oParentObject.isVar(cName))
        Endif
        Return(.F.)
    Func GetVar(cName)
        Local i,k
        k=0
        cName=Lower(cName)
        For i=1 To This.nFilterParam
            If cName==Lower(This.i_FilterParam[i,aFP_Name])
                Return(This.i_FilterParam[i,aFP_Value])
            Endif
        Next
        If !Isnull(This.oParentObject)
            Return(This.oParentObject.GetVar(cName))
        Endif
        Return('')

    *--- Zucchetti Aulla inizio - Converto a char i campi Memo non previsti sulla visual query
    Proc CastMemoToChar(i_cCursor,i_cMemoFields,i_cC254Fields)
        Local i,N,i_cpgrfld,i_cpgrtyp,i_cpgrlen,i_cPrgFldList,bDoCast,bEmptyCurs
        i_cPrgFldList=''
        bDoCast=.F.
        bEmptyCurs=Reccount(i_cCursor)=0
        Select(i_cCursor)
        N=Afields(PGRSTRUCT)
        For i=1 To N
            i_cpgrfld=PGRSTRUCT[i,1]
            i_cpgrtyp=PGRSTRUCT[i,2]
            i_cpgrlen=PGRSTRUCT[i,3]
            If (i_cpgrtyp="M" And Not Upper(i_cpgrfld) $ Upper(i_cMemoFields)) ;
            Or (i_cpgrtyp="C" And i_cpgrlen=254 And Not Upper(i_cpgrfld) $ Upper(i_cC254Fields))
                If bEmptyCurs
                    i_cPrgFldList=i_cPrgFldList+"' ' as "
                    bDoCast=.T.
                Else
                    Calculate Max(Len(Trim( &i_cpgrfld ))) To fieldlen In (i_cCursor)
                    If fieldlen<=254
                        i_cPrgFldList=i_cPrgFldList+"left("+ i_cpgrfld + ","+Str(fieldlen,3,0)+") as "
                        bDoCast=.T.
                    Endif
                Endif
            Endif
            i_cPrgFldList=i_cPrgFldList+i_cpgrfld+","
        Next
        If bDoCast
            i_cPrgFldList="Select "+Left(i_cPrgFldList,Len(i_cPrgFldList)-1)+" from "+i_cCursor+" into cursor "+i_cCursor+" readwrite"
            &i_cPrgFldList
        Endif
	Select (i_cCursor)
        Go Top
        Release PGRSTRUCT
    *--- Zucchetti Aulla fine - Converto a char i campi Memo non previsti sulla visual query

    Proc DoPivot(i_cCursor)
        Local i_oPivot,i,i_cTemp
        i_cTemp=Sys(2015)
        i_oPivot=Createobject('cp_pivot')
        For i=1 To This.nPivotFlds-1
            i_oPivot.AddFld(This.i_PivotFlds[i])
        Next
        i_oPivot.SetPivotFld(This.i_PivotFlds[this.nPivotFlds])
        For i=1 To This.nPivotData
            i_oPivot.AddObserved(This.i_PivotData[i])
        Next
        Do Case
            Case This.nPivotType=2
                i_oPivot.SetFldNumber()
            Case This.nPivotType=3
                i_oPivot.SetMonth()
            Case This.nPivotType=4
                i_oPivot.SetDay()
            Case This.nPivotType=5
                i_oPivot.SetList(This.cPivotListQry)
        Endcase
        i_oPivot.SetTable(i_cCursor)
        i_oPivot.SetCursor(i_cTemp)
        i_oPivot.Pivot()
        Select (i_cCursor)
        Use
        Select (i_cTemp)
        Use Dbf() Again In 0 Alias (i_cCursor)
        Select (i_cTemp)
        Use
        Return
    Proc CreateTemporaryTable(i_cName)
        Local i_nIdx,i_cTempTable
        i_nIdx=cp_AddTableDef(i_cName) && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        Do vq_exec With i_cName,This.oParentObject,Null,'',.F.,.T.,.F.,.F.,i_cTempTable
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        Return
    Proc DropTemporaryTable(i_cName)
        Local i_nIdx
        i_nIdx=cp_GetTableDefIdx(i_cName)
        If i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef(i_cName)
        Endif
        Return
    Procedure mCalledBatchEnd(__dummy__)
        Return
Enddefine

Func param_mask_read(oM)
    oM.Show()
    Clear Read
    Return

Define Class query_param_mask As CpSysform
    WindowType=1
    AutoCenter=.T.
    ScrollBars = 2
    oQ=.Null.
    nCtrls=0
    nY=5
    Caption=""
    oOldForm=.Null.
    bFox26=.F.
    *** Errore chiusura maschera parametri se questi vuoti all'apertura
    showmask=.F.
    Dimension Ctrls[1],Names[1],Types[1],nIdx[1]
    Icon=i_cBmpPath+i_cStdIcon
    Add Object ok As CommandButton With Top=5,Left=310,Width=50,Height=25,Caption="",FontBold=.F.
    Add Object Cancel As CommandButton With Top=35,Left=310,Width=50,Height=25,Caption="",FontBold=.F.
    Proc ok.Click()
        Local i
        For i=2 To This.Parent.nCtrls Step 2
            If This.Parent.Types[i]='C'
                This.Parent.oQ.i_FilterParam[this.parent.nIdx[i],aFP_Value]=Trim(This.Parent.Ctrls[i].Value)
            Else
                This.Parent.oQ.i_FilterParam[this.parent.nIdx[i],aFP_Value]=This.Parent.Ctrls[i].Value
            Endif
        Next
        i_curform=This.Parent.oOldForm
        This.Parent.oQ=.Null.
        Thisform.Hide()
        Thisform.Release()
    Proc Cancel.Click()
        Local i
        For i=2 To This.Parent.nCtrls Step 2
            This.Parent.oQ.i_FilterParam[this.parent.nIdx[i],aFP_Value]=cp_NullValue(This.Parent.Types[i])
        Next
        i_curform=This.Parent.oOldForm
        This.Parent.oQ=.Null.
        Thisform.Hide()
        Thisform.Release()
    Proc Init(oQ,bFox26, cQryName)
        This.oQ=oQ
        This.bFox26=bFox26
        This.Caption=Iif(Type('m.cQryName')='C' And Not Empty(m.cQryName), m.cQryName + ' - ', '')+cp_Translate(MSG_SELECTION_PARAMETERS)
        This.ok.Caption=cp_Translate(MSG_OK_BUTTON)
        This.Cancel.Caption=cp_Translate(MSG_CANCEL_BUTTON)
        If !this.bFox26 and i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif

    Proc Add(nIdx,cName,cLabel,cType,nLen,nDec)
        Local i
        This.nCtrls=This.nCtrls+2
        i=This.nCtrls-1
        Dimension This.Ctrls[i+1],This.Names[i+1],This.Types[i+1],This.nIdx[i+1]
        This.AddObject('this.Ctrls[i]','Label')
        This.Ctrls[i].Move(5,This.nY+3)
        This.Ctrls[i].Caption=Trim(cLabel)
        This.Ctrls[i].Width=100
        This.Ctrls[i].FontBold=.F.
        This.Ctrls[i].Alignment=1
        This.Ctrls[i].BackStyle=0
        This.Ctrls[i].Visible=.T.
        i=i+1
        This.nIdx[i]=nIdx
        This.Names[i]=cName
        *This.AddObject('this.Ctrls[i]','TextBox')
        This.AddObject('this.Ctrls[i]','cp_textcombo', m.cName, m.cType, m.nLen, m.nDec)

        This.Ctrls[i].Move(110,This.nY)
        This.Ctrls[i].FontBold=.F.
        This.Ctrls[i].Width=MIN(Max(Int((nLen+1.5)*8), 20)+ This.Ctrls[i].oCombo.Width, _screen.width-65-This.Ctrls[i].Left-SYSMETRIC(5)-2*SYSMETRIC(3)) 
        This.Ctrls[i].Height=22
        *This.Ctrls[i].Value=cp_NullValue(cType,nLen,nDec)
        This.Ctrls[i].TabIndex=(i-1)/2+1
        This.Ctrls[i].Visible=.T.
        Local cType1
        cType1=Left(cType,1)
        Do Case
            Case cType1='C'
                This.Ctrls[i].InputMask=Repl('X',nLen)
            Case cType1='N' And nDec=0
                This.Ctrls[i].InputMask=Repl('9',nLen)
            Case cType1='N' Or cType1='Y'
                This.Ctrls[i].InputMask=Repl('9',nLen-nDec)+'.'+Repl('9',nDec)
        Endcase
        This.Types[i]=cType
        This.nY=This.nY+25
        This.Height = Min(MAX(This.nY, 65), _screen.height-100)
        This.Width = MIN(Max(This.Ctrls[i].Left+This.Ctrls[i].Width+65+SYSMETRIC(5), This.Width), _screen.width-2*SYSMETRIC(3))
        This.ok.Left=This.Width-60-SYSMETRIC(5)
        This.Cancel.Left=This.ok.Left
    Proc Show()
        If !This.bFox26
            This.oOldForm=i_curform
            i_curform=This
            *** Errore chiusura maschera parametri se questi vuoti all'apertura
            This.showmask=.T.
        Endif
        This.AutoCenter=.T.
        DoDefault()
    Proc ecpQuit()
        This.Cancel.Click()
    Proc ecpSave()
        This.ok.Click()
        ***Errore chiusura maschera parametri se questi vuoti all'apertura
    Proc Destroy()
        Local i
        If Type("this")='O' And Type("this.oQ")='O' And Not Isnull(This.oQ) And This.showmask
            For i=2 To This.nCtrls Step 2
                This.oQ.i_FilterParam[this.nIdx[i],aFP_Value]=cp_NullValue(This.Types[i])
            Next
            i_curform=This.oOldForm
            This.oQ=.Null.
        Endif
    Func HasCPEvents(i_cOp)
        Return(i_cOp$"ecpSave,ecpQuit")
Enddefine

Func cp_NullValue(pType,nLen,nDec)
    LOCAL cType
    cType=Upper(Left(pType,1))
    Do Case
        Case cType='C' Or cType='M'
            Return('')
        Case cType='N' Or cType='Y'
            Return(0)
        Case cType='T' Or UPPER(pType)=='DATETIME'
            Return(Ctot("  -  -  "))
        Case cType='D'
            Return(Ctod("  -  -  "))
        Case cType='L'
            Return(.F.)
    Endcase
    Return('')

Func cp_ValueToStr(i_nConn,i_cs,i_ty)
    Local i_res
    If i_nConn=0
        If Vartype(i_cs)='D'
            i_res="ctod('"+Dtoc(i_cs)+"')"
        Else
            If Vartype(i_cs)='T'
                i_res="ctot('"+Ttoc(i_cs)+"')"
            Else
                i_res=cp_ToStr(i_cs)
            Endif
        Endif
    Else
        i_res=cp_ToStrODBC(i_cs)
    Endif
    *do case
    *  case ty='C' or ty='M'
    *    if "'"$cs
    *      res='"'+trim(cs)+'"'
    *    else
    *      res="'"+trim(cs)+"'"
    *    endif
    *  case ty='N'
    *    res=alltrim(str(cs))
    *  case ty='D'
    *    res='ctod('+dtoc(cs)+')'
    *  case ty='L'
    *    res=iif(cs,'.T.','.F.')
    *  case ty='T'
    *    res='ctot('+ttoc(cs)+')'
    *endcase
    Return(i_res)

Func cp_TranslateSQLFunctions(cIn, cTipoDB, cNestedFuncs,obj)
    Local cParAp,cParCh,cVir,cIn,nPA,cFunc,cParam
    Local cApi,nCases,cFirstParam,cIIf,nCountIter,cParamINI,cParamFIN
    cParAp = '('
    cParCh = ')'
    cVir = ','
    cApi = "'"
    cIn = Alltrim(cIn)
    *IsolaFunzione
    nPA = At(cParAp, cIn)
    If nPA>0
        cFunc = Upper(Substr(cIn, 1, nPA-1))
        *IsolaParametri
        cParam = Substr(cIn, nPA+1, Len(cIn)-nPA-1)
    Else && � un operatore
        cFunc = Upper(cIn)
    Endif
    *Funzioni SQL
    cTipoDB=Upper(cTipoDB)
    cFunc=Alltrim(cFunc)
    Do Case
        Case cTipoDB = 'ACCESS'
            Do Case
                Case cFunc = 'VAL'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc == 'STR'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc = 'LTRIM'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc = 'RTRIM'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc = 'TRIM'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc = 'SUBSTR'
                    Return 'MID' + cParAp + cParam + cParCh
                Case cFunc = 'YEAR'
                    Return 'DATEPART' + cParAp + cApi + 'YYYY' + cApi + cVir + cParam + cParCh
                Case cFunc = 'MONTH'
                    Return 'DATEPART' + cParAp + cApi + 'M' + cApi + cVir + cParam + cParCh
                Case cFunc == 'DAY'
                    Return 'DATEPART' + cParAp + cApi + 'D' + cApi + cVir + cParam + cParCh
                Case cFunc = 'DATE'
                    Return Strtran(cParam, cApi, '#')
                Case cFunc = 'NVL'
                    Return 'IIF' + cParAp + 'NOT ISNULL' + cParAp + ;
                        SubStr(cParam, 1, At(cVir, cParam)-1) + cParCh + cVir + cParam + cParCh
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'IIF' + cParAp + 'NOT ISNULL' + cParAp + cParam + cParCh + cVir + ;
                        'LEN' + cParAp + 'TRIM' + cParAp + cParam + cParCh + cParCh + cVir + ;
                        '0' + cParCh
                Case cFunc = 'LEN'
                    Return 'IIF' + cParAp + 'NOT ISNULL' + cParAp + cParam + cParCh + cVir + ;
                        'LEN' + cParAp + cParam + cParCh + cVir + '0' + cParCh
                Case cFunc = 'NOTEMPTYDATE'
                    Return 'IIF' + cParAp + 'NOT ISNULL' + cParAp + cParam + cParCh + cVir + ;
                        'LEN' + cParAp + cParam + cParCh + cVir + '0' + cParCh
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'IIF' + cParAp + 'NOT ISNULL' + cParAp + cParam + cParCh + cVir + ;
                        cParam + cVir + '0' + cParCh
                Case cFunc == 'CASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    nVir = At(cVir, cParam)
                    cFirstParam = Left(cParam, nVir-1)
                    cIIf = 'IIF' + cParAp + cFirstParam + ' = '
                    cParam = cIIf + Substr(cParam, nVir+1)
                    For nCountIter = 1 To nCases-1
                        cParam = Stuff(cParam, At(cVir, cParam, 2*nCountIter)+1, 0, cIIf)
                    Endfor
                    For nCountIter = 1 To nCases
                        cParam = cParam + cParCh
                    Endfor
                    Return cParam
                Case cFunc == 'CASE2'
                    nCases = (Occurs(cVir, cParam))/2
                    nVir = At(cVir, cParam)
                    cFirstParam = Left(cParam, nVir-1)
                    cIIf = 'IIF' + cParAp + cFirstParam  + cVir
                    cParam = cIIf + Substr(cParam, nVir+1)
                    cIIf = 'IIF' + cParAp
                    For nCountIter = 1 To nCases-1
                        cParam = Stuff(cParam, At(cVir, cParam, 2*nCountIter)+1, 0, cIIf)
                    Endfor
                    For nCountIter = 1 To nCases
                        cParam = cParam + cParCh
                    Endfor
                    Return cParam                    
                Case cFunc = 'CONCAT'
                    Return '&'
                Case cFunc = 'DIFFDAY'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return '('+cParamFIN + '-' + cParamINI + ')'
                Case cFunc == 'ADDDATE'
                    * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return 'DATEADD'+ cParAp + 'd' + cVir + cParamFIN + cVir +  cParamINI + cParCh

            Endcase

        Case cTipoDB = 'DB2'
            Do Case
                Case cFunc = 'VAL'
                    Return 'DOUBLE('+cParam+')'
                Case cFunc == 'STR'
                    Return 'CHAR('+cParam+')'
                Case cFunc = 'LTRIM'
                    Return 'LTRIM('+cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'RTRIM('+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'LTRIM(RTRIM('+cParam+'))'
                Case cFunc = 'SUBSTR'
                    * --- In DB2 occorre tipare il secondo e terzo parametro (se non sono costanti va in errore)
                    Local sString, nIdx,nLen,nSVir
                    nVir = At(cVir,cParam)
                    sString = Substr(cParam, 1, nVir-1)
                    nSVir = Rat(cVir,cParam)
                    If nSVir=nVir
                        * --- ho due parametri
                        nIdx = Substr(cParam, nVir+1)
                        Return 'SUBSTR('+sString+', CAST('+nIdx+' As Integer ))'
                    Else
                        * --- ho tre parametri
                        nIdx = Substr(cParam, nVir+1, nSVir-nVir-1)
                        nLen = Substr(cParam, nSVir+1)
                        Return 'SUBSTR('+sString+', CAST('+nIdx+' As Integer ), CAST('+nLen+' As Integer ))'
                    Endif
                Case cFunc = 'YEAR'
                    Return 'YEAR('+cParam+')'
                Case cFunc = 'MONTH'
                    Return 'MONTH('+cParam+')'
                Case cFunc == 'DAY'
                    Return 'DAY('+cParam+')'
                Case cFunc = 'DATE'
                    Return 'DATE('+cp_NormalizeDateConst(cParam)+')'
                Case cFunc = 'NVL'
                    Return 'COALESCE('+cParam+')'
                    *return 'VALUE' + cParAp + cParam + cParCh
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'COALESCE(LENGTH(LTRIM(RTRIM('+cParam+'))),0)'
                Case cFunc = 'LEN'
                    Return 'COALESCE(LENGTH('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYDATE'
                    Return 'COALESCE(LENGTH('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'COALESCE('+cParam+',0)'
                Case cFunc == 'CASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases > 0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE ' + cParam + ' END'
                Case cFunc = 'CONCAT'
                    Return '||'
                Case cFunc = 'DIFFDAY'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return 'DAYS' + cParAp + cParamFIN + cParCh + '- DAYS' + cParAp + cParamINI + cParCh
                Case cFunc = 'ROUND'
                    Local i_nRound
                    i_nRound=At(cVir,cParam)
                    Return 'CAST'+cParAp+cFunc+cParAp+Left(cParam,i_nRound-1)+cVir+'CAST'+cParAp+Substr(cParam,i_nRound+1,Len(cParam))+' AS Int'+cParCh+cParCh+' AS DECIMAL'+cParAp+'18'+cVir+'5'+cParCh+cParCh
                    *--- Zucchetti Aulla inizio (funzioni ad hoc)
                Case cFunc == 'ADDDATE'
                    * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return 'DATE'+ cParAp + 'DAYS' + cParAp + cParamINI + cParCh + '+' + cParamFIN + cParCh
                Case cFunc = 'ABS'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc = 'DATAVUOTA'
                    * Restituisce un valore Null di tipo Date
                    If Vartype(g_ORACLEVERS)='C' And g_ORACLEVERS = '8'
                        If cTipoDB = 'VFP'
                            Return(cp_ToStr(i_INIDAT))
                        Else
                            Return(cp_ToStrODBC(i_INIDAT))
                        Endif
                    Else
                        Return 'CAST' + cParAp + 'NULL As Date' + cParCh
                    Endif
                Case cFunc = 'CASTD'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return 'CAST'+cParAp+cParamINI+' AS DECIMAL'+cParAp+Left(cParamFIN,At(cVir,cParamFIN)-1)+cVir+Substr(cParamFIN,At(cVir,cParamFIN)+1,Len(cParam))+cParCh+cParCh
                Case cFunc='BCASE'
                    Local nCases
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases>0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE '+cParam+' END'
                    *  Funzioni Settimana/quadrimestre/semestre
                Case cFunc='WEEK'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc='QUARTER'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc='SEMESTER'
                    Return 'CASE WHEN MONTH'+ cParAp + cParam + cParCh + '<=6 THEN 1  ELSE 2 END'
                    *  Funzioni Settimana/quadrimestre/semestre
                    *  Funzione Day of Week (Numerico)
                Case cFunc='DOW'
                    Return 'DAYOFWEEK' + cParAp + cParam + cParCh
                    *  Funzione Day of Week (Numerico)
                Case cFunc='RIGHT'
                    * --- in DB2 la right accetta come secondo parametro solo un Integer o SmallInt
                    Local i_nRight
                    i_nRight=At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, i_nRight-1)
                    If Occurs(cVir,cParam)= 2
                        * Se presenti pi� di due parametri, significa che intendo fare la CAST
                        cParamFIN = Substr(cParam, i_nRight+1)
                        cParamFIN = Left(cParamFIN, At(cVir,cParamFIN)-1)
                        cParamCast = Substr(cParam, Rat(cVir, cParam)+1)
                    Else
                        * Non faccio la cast as Varchar nel caso di Right normale
                        cParamCast = "0"
                        cParamFIN = Substr(cParam, i_nRight+1)
                    Endif
                    If Alltrim(cParamCast) = "1"
                        **Aggiunta 'CAST' per risolvere l'errore in caso di stringhe concatenate
                        **passate come primo parametro
                        Return 'CAST'+ cParAp + 'RIGHT' + cParAp + cParamINI +cVir+'CAST'+cParAp +cParamFIN + ' As Integer' + cParCh + cParCh+  ' As Varchar '+cParAp+ cParamFIN  + cParCh + cParCh
                    Else
                        Return 'RIGHT' + cParAp + cParamINI +cVir+'CAST'+cParAp +cParamFIN + ' As Integer' + cParCh + cParCh
                    Endif
                    *  AT(cSearchExpression, cExpressionSearched [, nStart_location ])
                    * Restituisce la prima posizione di una specifica espressione (cSearchExpression) all'interno di una stringa (cExpressionSearched)
                    * tramite l'ultimo parametro � possibile specificare la posizione da cui partire per effettuare la ricerca (da sinistra a destra)
                    * Restituisce 0 se non viene trovata nessuna occorenza
                Case cFunc = 'AT'
                    Return 'LOCATE'+ cParAp + cParam + cParCh
                    *--- Zucchetti Aulla fine (funzioni ad hoc)
            Endcase

        Case cTipoDB = 'INFORMIX'
            Do Case
                Case cFunc = 'VAL'
                    Return cParam
                Case cFunc == 'STR'
                    Return cParam
                Case cFunc = 'LTRIM'
                    Return 'TRIM(LEADING FROM '+cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'TRIM(TRAILING FROM '+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'TRIM('+cParam+')'
                Case cFunc = 'SUBSTR'
                    Return 'SUBSTR('+cParam+')'
                Case cFunc = 'YEAR'
                    Return 'YEAR('+cParam+')'
                Case cFunc = 'MONTH'
                    Return 'MONTH('+cParam+')'
                Case cFunc == 'DAY'
                    Return 'DAY('+cParam+')'
                Case cFunc = 'DATE'
                    Return "TO_DATE("+cp_NormalizeDateConst(cParam)+",'%Y-%m-%d')"
                    *If Left(Right(cParam, 4), 1) = '-' && cParam = "'...-98'"
                    *  return 'TO_DATE' + cParAp + cParam + cVir + '%d-%m-%y' + cParCh
                    *Else && cParam = "'...-1998'"
                    *  return 'TO_DATE' + cParAp + cParam + cVir + '%d-%m-%Y' + cParCh
                    *EndIf
                Case cFunc = 'NVL'
                    Return 'NVL('+cParam+')'
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'NVL(CHAR_LENGTH(TRIM('+cParam+')),0)'
                Case cFunc = 'LEN'
                    Return 'NVL(CHAR_LENGTH('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYDATE'
                    Return 'NVL(CHAR_LENGTH(TRIM(TO_CHAR('+cParam+'))),0)'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'NVL('+cParam+',0)'
                Case cFunc == 'CASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases > 0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE ' + cParam + ' END'
                Case cFunc = 'CONCAT'
                    Return '||'
                Case cFunc == 'ADDDATE'
                    * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return cParamINI+'+'+cParamFIN+' UNITS DAY '
            Endcase

        Case cTipoDB = 'ORACLE'
            Do Case
                Case cFunc = 'VAL'
                    Return 'TO_NUMBER('+cParam+')'
                Case cFunc == 'STR'
                    Return 'TO_CHAR('+cParam+')'
                Case cFunc = 'LTRIM'
                    Return 'LTRIM('+ cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'RTRIM('+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'LTRIM(RTRIM('+cParam+'))'
                Case cFunc = 'SUBSTR'
                    Return 'SUBSTR('+cParam+')'
                Case cFunc = 'YEAR'
                    Return 'TO_NUMBER(TO_CHAR('+cParam+",'YYYY'))"
                Case cFunc = 'MONTH'
                    Return 'TO_NUMBER(TO_CHAR('+cParam+",'MM'))"
                Case cFunc == 'DAY'
                    Return 'TO_NUMBER(TO_CHAR('+cParam+",'DD'))"
                Case cFunc == "DAYOFWEEK"
                    Return 'TO_NUMBER(TO_CHAR('+cParam+",'D'))"
                Case cFunc = 'DATE'
                    Return 'TO_DATE('+cp_NormalizeDateConst(cParam)+",'YYYY-MM-DD')"
                Case cFunc = 'NVL'
                    Return 'NVL('+cParam+')'
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'NVL(LENGTH(LTRIM(RTRIM('+cParam+'))),0)'
                Case cFunc = 'LEN'
                    Return 'NVL(LENGTH('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYDATE'
                    Return 'NVL(LENGTH('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'NVL('+cParam+',0)'
                Case cFunc == 'CASE'
                    Return 'DECODE('+cParam+')'
                Case cFunc = 'CONCAT'
                    Return '||'
                Case cFunc = 'DIFFDAY'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return '('+cParamFIN + '-' + cParamINI + ')'
                    *--- Zucchetti Aulla inizio (funzioni ad hoc)
                Case cFunc == 'ADDDATE'
                    * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    Return Strtran(cParam, cVir, '+')
                Case cFunc = 'DATAVUOTA'
                    If Vartype(g_ORACLEVERS)='C' And g_ORACLEVERS = '8'
                        If cTipoDB = 'VFP'
                            Return(cp_ToStr(i_INIDAT))
                        Else
                            Return(cp_ToStrODBC(i_INIDAT))
                        Endif
                    Else
                        Return 'CAST' + cParAp + 'NULL As Date' + cParCh
                    Endif
                Case cFunc = 'ABS'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc = 'CASTD'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    Return cParamINI
                Case cFunc='BCASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases>0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE '+cParam+' END'
                Case cFunc='WEEK'
                    Return 'to_number '+cParAp+'to_char '+ cParAp + cParam + cVir + cApi+ 'WW'+ cApi +cParCh + cParCh
                Case cFunc='QUARTER'
                    Return 'to_number '+ cParAp+'to_char '+ cParAp + cParam + cVir + cApi+ 'Q'+ cApi+ cParCh + cParCh
                Case cFunc='SEMESTER'
                    Return 'CASE  WHEN TO_NUMBER' + cParAp +'TO_CHAR'+ cParAp + cParam + cVir + cApi + 'MM' + cApi + cParCh + cParCh + '<=6 THEN 1  ELSE 2 END'
                    * Funzione Day of Week (Numerico)
                Case cFunc='DOW'
                    Return 'to_number '+cParAp+'to_char '+ cParAp + cParam + cVir + cApi+ 'D'+ cApi +cParCh + cParCh
                    * Funzione Day of Week (Numerico)
                    * AT(cSearchExpression, cExpressionSearched [, nStart_location ])
                    * Restituisce la prima posizione di una specifica espressione (cSearchExpression) all'interno di una stringa (cExpressionSearched)
                    * tramite l'ultimo parametro � possibile specificare la posizione da cui partire per effettuare la ricerca (da sinistra a destra)
                    * Restituisce 0 se non viene trovata nessuna occorenza
                Case cFunc = 'AT'
                    Dimension aParam[1]
                    nNumParam = ParamParsing(@aParam, cParam, cVir)
                    If nNumParam = 3
                        Return 'INSTR'+ cParAp + aParam[2] + cVir + aParam[1] + cVir + aParam[3] + cParCh
                    Else
                        Return 'INSTR'+ cParAp + aParam[2] + cVir + aParam[1] + cParCh
                    Endif
                    * Zucchetti Aulla Fine - AT(cSearchExpression, cExpressionSearched [, nStart_location ])
                Case cFunc='RIGHT'
                    * Funzione Right per Oracle. Si traduce con na SUBSTR con il secondo parametro negativo
                    * E' possibile usare la Right con 3 parametri perch� in DB2 a volte � necessario effettuare una cast sul secondo parametro
                    Local i_nRight
                    i_nRight=At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, i_nRight-1)
                    If Occurs(cVir,cParam)= 2
                        * Se presenti pi� di due parametri, significa che intendo fare la CAST
                        cParamFIN = Substr(cParam, i_nRight+1)
                        cParamFIN = Left(cParamFIN, At(cVir,cParamFIN)-1)
                        cParamCast = Substr(cParam, Rat(cVir, cParam)+1)
                    Else
                        * Non faccio la cast as Varchar nel caso di Right normale
                        cParamCast = "0"
                        cParamFIN = Substr(cParam, i_nRight+1)
                    Endif
                    Return 'SUBSTR' + cParAp + 'LTRIM(RTRIM(' + cParamINI + '))' + cVir + '-' + cParamFIN + cParCh
                Case cFunc = 'DTOC' And g_APPLICATION <> "ADHOC REVOLUTION"
                    *** Zucchetti Aulla Inizio DTOC
                    cParam=Iif(At(cVir,cParam)=0,cParam+cVir+'105',cParam)
                    nVir=At(cVir,cParam)
                    cParamINI=Left(cParam,nVir-1)
                    Do Case
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='101'
                            cParamTip="'MM/DD/YYYY'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='102'
                            cParamTip="'YYYY.MM.DD'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='103'
                            cParamTip="'DD/MM/YYYY'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='105'
                            cParamTip="'DD-MM-YYYY'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='110'
                            cParamTip="'MM-DD-YYYY'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='112'
                            cParamTip="'YYYYMMDD'"
                        Otherwise
                            cParamTip="'DD-MM-YYYY'"
                    Endcase
                    Return 'LTRIM' + cParAp + 'RTRIM' + cParAp + 'TO_CHAR' + cParAp + cParamINI + cVir + cParamTip + cParCh + cParCh + cParCh
                    *** Zucchetti Aulla Fine DTOC
                    *** Zucchetti Aulla Inizio funzioni orario
				Case cFunc == 'SECOND'
					Return "EXTRACT(SECOND FROM CAST("+cParam+" AS TIMESTAMP))"
				Case cFunc == 'MINUTE'
					Return "EXTRACT(MINUTE FROM CAST("+cParam+" AS TIMESTAMP))"
				Case cFunc == 'HOUR'
	                Return "EXTRACT(HOUR FROM CAST("+cParam+" AS TIMESTAMP))"
	                *** Zucchetti Aulla Fine funzioni orario
                Case cFunc='UPPER'
                    Return 'UPPER' + cParAp + cParam + cParCh
                Case cFunc=='ASCII'
                    Return 'ASCII' + cParAp + cParam + cParCh
                Case cFunc=='CHR'
                    Return 'CHR' + cParAp + cParam + cParCh
                Case cFunc = 'MOD'
                    Return 'MOD('+cParam+')'
            Endcase
        Case cTipoDB = 'SAPDB'
            Do Case
                Case cFunc = 'VAL'
                    Return 'NUM('+cParam+')'
                Case cFunc == 'STR'
                    Return 'CHR('+cParam+')'
                Case cFunc = 'LTRIM'
                    Return 'LTRIM('+cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'RTRIM('+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'LTRIM(RTRIM('+cParam+'))'
                Case cFunc = 'SUBSTR'
                    Return 'SUBSTR('+cParam+')'
                Case cFunc = 'YEAR'
                    Return 'YEAR('+ cParam+')'
                Case cFunc = 'MONTH'
                    Return 'MONTH('+cParam+')'
                Case cFunc == 'DAY'
                    Return 'DAY('+ cParam+')'
                Case cFunc = 'DATE'
                    Return 'DATE('+cp_NormalizeDateConst(cParam)+')'
                Case cFunc = 'NVL'
                    Return 'IFNULL('+cParam+','+Substr(cParam,1,Rat(cVir, cParam)-1)+')'
                Case cFunc = 'LEN'
                    Return 'IFNULL(LENGTH('+cParam+'),0,LENGTH('+cParam+'))'
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'IFNULL(LENGTH('+cParam+'),0,LENGTH(LTRIM(RTRIM('+cParam+'))))'
                Case cFunc = 'NOTEMPTYDATE'
                    Return 'IFNULL(LENGTH('+cParam+'),0,LENGTH('+cParam+'))'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'IFNULL('+cParam+',0,'+cParam+')'
                Case cFunc == 'CASE'
                    Return 'DECODE('+cParam+')'
                Case cFunc = 'CONCAT'
                    Return '||'
                Case cFunc = 'DIFFDAY'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return 'CASE WHEN '+cParamINI + '<' + cParamFIN+' THEN DATEDIFF(' + cParamINI + ',' + cParamFIN + ') ELSE 0-DATEDIFF(' + cParamINI + ',' + cParamFIN +') END'
                Case cFunc == 'ADDDATE'
                    * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return 'ADDATE'+ cParAp + cParamINI + cVir + cParamFIN + cParCh
            Endcase
        Case cTipoDB = 'SQLSERVER'
            Do Case
                Case cFunc = 'VAL'
                    Return 'CONVERT(FLOAT,'+cParam+')'
                Case cFunc == 'STR'
                    Return 'CONVERT(CHAR,'+cParam+')'
                Case cFunc = 'LTRIM'
                    Return 'LTRIM('+cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'RTRIM('+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'LTRIM(RTRIM('+cParam+'))'
                Case cFunc = 'SUBSTR'
                    Return 'SUBSTRING('+cParam+')'
                Case cFunc = 'YEAR'
                    Return 'DATEPART(YEAR,'+cParam+')'
                Case cFunc = 'MONTH'
                    Return 'DATEPART(MONTH,'+cParam+')'
                Case cFunc == 'DAY'
                    Return 'DATEPART(DAY,'+cParam+')'
                Case cFunc == "DAYOFWEEK"
                    Return "DATEPART(DW,"+cParam+")"
                Case cFunc = 'DATE'
                    * --- Problema date per SQL in italiano o in inglese
                    Return 'CONVERT' + cParAp + 'DATETIME' + cVir + cp_NormalizeDateConst(cParam) + cVir + '102' + cParCh
                Case cFunc = 'NVL'
                    Return 'COALESCE('+cParam+')'
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'COALESCE(DATALENGTH(LTRIM(RTRIM('+cParam+'))),0)'
                Case cFunc = 'LEN'
                    Return 'COALESCE(DATALENGTH('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYDATE'
                    Return 'COALESCE(DATALENGTH('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'COALESCE('+cParam+',0)'
                Case cFunc == 'CASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases > 0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE ' + cParam + ' END'
                Case cFunc = 'CONCAT'
                    Return '+'
                Case cFunc = 'DIFFDAY'
                    Return 'DATEDIFF(day,'+cParam+')'
                    *--- Zucchetti Aulla inizio (funzioni ad hoc)
                Case cFunc == 'ADDDATE'
                    * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    Dimension L_ArrParams(2)
                    Local L_Result
                    ParamParsing(@L_ArrParams, cParam, cVir)
                    L_Result = 'DATEADD'+cParAp+'day'+cVir+L_ArrParams(2)+cVir+L_ArrParams(1)+cParCh
                    Release L_ArrParams
                    Return L_Result
                    *return StrTran(cParam, cVir, '+')
                Case cFunc = 'DATAVUOTA'
                    If Vartype(g_ORACLEVERS)='C' And g_ORACLEVERS = '8'
                        If cTipoDB = 'VFP'
                            Return(cp_ToStr(i_INIDAT))
                        Else
                            Return(cp_ToStrODBC(i_INIDAT))
                        Endif
                    Else
                        Return 'CAST' + cParAp + 'NULL As DateTime' + cParCh
                    Endif
                Case cFunc = 'ABS'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc = 'CASTD'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    Return cParamINI
                Case cFunc='BCASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases>0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE '+cParam+' END'
                Case cFunc='WEEK'
                    Return 'datepart'+ cParAp + 'week'+ cVir + cParam + cParCh
                Case cFunc='QUARTER'
                    Return 'datepart'+ cParAp + 'quarter'+ cVir + cParam + cParCh
                Case cFunc='SEMESTER'
                    Return 'CASE WHEN MONTH'+ cParAp + cParam + cParCh + '<=6 THEN 1  ELSE 2 END'
                Case cFunc='DOW'
                    Return 'datepart'+ cParAp + 'dw'+ cVir + cParam + cParCh
                    * AT(cSearchExpression, cExpressionSearched [, nStart_location ])
                    * Restituisce la prima posizione di una specifica espressione (cSearchExpression) all'interno di una stringa (cExpressionSearched)
                    * tramite l'ultimo parametro � possibile specificare la posizione da cui partire per effettuare la ricerca (da sinistra a destra)
                    * Restituisce 0 se non viene trovata nessuna occorenza
                Case cFunc = 'AT'
                    Return 'CHARINDEX'+ cParAp + cParam + cParCh
                Case cFunc = 'RIGHT'
                    * Per problema Right su DB2 � stato inserito un terzo parametro nella Right.
                    * E' possibile usare la Right con 3 parametri perch� in DB2 a volte � necessario effettuare una cast sul secondo parametro
                    Local i_nRight
                    i_nRight=At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, i_nRight-1)
                    If Occurs(cVir,cParam)= 2
                        * Se presenti pi� di due parametri, significa che intendo fare la CAST
                        cParamFIN = Substr(cParam, i_nRight+1)
                        cParamFIN = Left(cParamFIN, At(cVir,cParamFIN)-1)
                        cParamCast = Substr(cParam, Rat(cVir, cParam)+1)
                    Else
                        * Non faccio la cast as Varchar nel caso di Right normale
                        cParamCast = "0"
                        cParamFIN = Substr(cParam, i_nRight+1)
                    Endif
                    Return cFunc + cParAp + cParamINI + cVir + cParamFIN + cParCh

                    *** Zucchetti Aulla Inizio DTOC
                Case cFunc = 'DTOC' And g_APPLICATION <> "ADHOC REVOLUTION"
                    cParam=Iif(At(cVir,cParam)=0,cParam+cVir+'105',cParam)
                    nVir=At(cVir,cParam)
                    cParamINI=Left(cParam,nVir-1)
                    Do Case
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='101'
                            cParamTip='101'
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='102'
                            cParamTip='102'
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='103'
                            cParamTip='103'
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='105'
                            cParamTip='105'
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='110'
                            cParamTip='110'
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='112'
                            cParamTip='112'
                        Otherwise
                            cParamTip='105'
                    Endcase
                    Return 'LTRIM' + cParAp + 'RTRIM' + cParAp + 'CONVERT'+cParAp+'CHAR'+cVir+cParamINI+cVir+cParamTip+cParCh + cParCh + cParCh
                    *** Zucchetti Aulla Fine TODC
                    *** Zucchetti Aulla Inizio funzioni orario
				Case cFunc == 'SECOND'
					Return "DATEPART(SECOND ,"+cParam+")"
				Case cFunc == 'MINUTE'
					Return "DATEPART(MINUTE ,"+cParam+")"
				Case cFunc == 'HOUR'
	                Return "DATEPART(HOUR ,"+cParam+")"
	                *** Zucchetti Aulla Fine funzioni orario
                Case cFunc='UPPER'
                    Return 'UPPER' + cParAp + cParam + cParCh
                Case cFunc=='ASCII'
                    Return 'ASCII' + cParAp + cParam + cParCh
                Case cFunc=='CHR'
                    Return 'CHAR' + cParAp + cParam + cParCh
                    *--- Zucchetti Aulla - introdotto per poter avere una funzione dipendente da un campo della query
                Case cFunc == 'SPACES'
                    Return 'SPACE('+cParam+')'
                    *--- Zucchetti Aulla fine (funzioni ad hoc)
                Case cFunc = 'MOD'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return '('+cParamINI+' % '+cParamFIN+')'
            Endcase
        Case cTipoDB = "INTERBASE"
            Do Case
                Case cFunc = 'VAL'
                    Return 'CAST('+cParam+' AS NUMERIC)'
                Case cFunc == 'STR'
                    Return 'CAST('+cParam+' AS CHARACTER)'
                Case cFunc = 'LTRIM'
                    Return 'LTRIM('+cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'RTRIM('+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'LTRIM(RTRIM('+cParam+'))'
                Case cFunc = 'SUBSTR'
                    Return 'SUBSTRLEN('+cParam+')'
                Case cFunc = 'YEAR'
                    Return 'EXTRACT(YEAR FROM '+cParam+')'
                Case cFunc = 'MONTH'
                    Return 'EXTRACT(MONTH FROM '+cParam+')'
                Case cFunc == 'DAY'
                    Return 'EXTRACT(DAY FROM '+cParam+')'
                Case cFunc = 'DATE'
                    *RETURN 'CAST('+cp_NormalizeDateConst(cParam)+' AS DATE)'
                    Return 'CONVERT' + cParAp + 'DATETIME' + cVir + cParam + cVir + '105' + cParCh
                Case cFunc = 'NVL'
                    Return 'COALESCE('+cParam+')'
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'COALESCE(STRLEN(LTRIM(RTRIM('+cParam+'))),0)'
                Case cFunc = 'LEN'
                    Return 'COALESCE(STRLEN('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYDATE'
                    Return 'COALESCE(STRLEN('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'COALESCE('+cParam+',0)'
                Case cFunc == 'CASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases > 0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE ' + cParam + ' END'
                Case cFunc = 'CONCAT'
                    Return '||'
                Case cFunc='DIFFDAY'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return '('+cParamFIN +'-'+cParamINI+')'
                Case cFunc == 'ADDDATE'
                    * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return 'DATEADD('+cParamFIN+' DAY TO '+cParamINI +')'
            Endcase
        Case cTipoDB = 'VFP'
            Do Case
                Case cFunc = 'VAL'
                    Return 'VAL('+cParam+')'
                Case cFunc == 'STR'
                    Return 'STR('+cParam+',19,6)'
                Case cFunc = 'LTRIM'
                    Return 'LTRIM('+cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'RTRIM('+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'ALLTRIM('+cParam+')'
                Case cFunc = 'SUBSTR'
                    Return 'SUBSTR('+cParam+')'
                Case cFunc = 'YEAR'
                    Return 'YEAR('+cParam+')'
                Case cFunc = 'MONTH'
                    Return 'MONTH('+cParam+')'
                Case cFunc == 'DAY'
                    Return 'DAY('+cParam+')'
                Case cFunc = 'DATE'
                    Return '{^'+Strtran(cp_NormalizeDateConst(cParam),cApi,'')+'}'
                Case cFunc = 'NVL'
                    Return 'NVL('+cParam+')'
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'NVL(LEN(ALLTRIM('+cParam+')),0)'
                Case cFunc = 'LEN'
                    Return 'NVL(LEN('+cParam+'),0)'
                Case cFunc = 'NOTEMPTYDATE'
                    Return 'IIF(EMPTY('+cParam+'),0,1)'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'NVL('+cParam+',0)'
                Case cFunc == 'CASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    nVir = At(cVir, cParam)
                    cFirstParam = Left(cParam, nVir-1)
                    cIIf = 'IIF' + cParAp + cFirstParam + ' = '
                    cParam = cIIf + Substr(cParam, nVir+1)
                    For nCountIter = 1 To nCases-1
                        cParam = Stuff(cParam, At(cVir, cParam, 2*nCountIter)+1, 0, cIIf)
                    Endfor
                    For nCountIter = 1 To nCases
                        cParam = cParam + cParCh
                    Endfor
                    Return cParam
                Case cFunc = 'CONCAT'
                    Return '+'
                Case cFunc == 'ADDDATE'
                    * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    Return Strtran(cParam, cVir, '+')
            Endcase

        Case cTipoDB='POSTGRESQL' 
            Do Case
                Case cFunc = 'VAL'
                    Return 'TO_NUMBER('+cParam+",'999999999999999.999999')"
                Case cFunc == 'STR'
                    Return 'CAST('+cParam+' AS VARCHAR(20))'
                Case cFunc = 'LTRIM'
                    Return 'LTRIM('+cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'RTRIM('+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'TRIM('+cParam+')'
                Case cFunc = 'SUBSTR'
                    Return 'SUBSTRING('+cParam+')'
                Case cFunc = 'YEAR'
                    Return 'EXTRACT(YEAR FROM CAST('+cParam+' AS TIMESTAMP))'
                Case cFunc = 'MONTH'
                    Return 'EXTRACT(MONTH FROM CAST('+cParam+' AS TIMESTAMP))'
                Case cFunc == 'DAY'
                    Return 'EXTRACT(DAY FROM CAST('+cParam+' AS TIMESTAMP))'
                Case cFunc == "DAYOFWEEK"
                    Return "CASE EXTRACT(DOW FROM "+cParam+") WHEN 0 THEN 7 ELSE EXTRACT(DOW FROM "+cParam+") END "
                Case cFunc = 'DATE'
                    Return 'CAST(' + cp_NormalizeDateConst(cParam) + ' AS DATE)'
                Case cFunc = 'NVL'
                    Return 'COALESCE('+cParam+')'
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'COALESCE(CHAR_LENGTH(TRIM('+cParam+')),0)'
                Case cFunc = 'LEN'
                    Return 'COALESCE(CHAR_LENGTH('+cParam+'),0)'
                Case cFunc='NOTEMPTYDATE'
                    Return 'COALESCE(CHAR_LENGTH(TRIM(CAST('+cParam+' AS CHAR(20)))),0)'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'COALESCE('+cParam+',0)'
                Case cFunc == 'CASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases > 0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE ' + cParam + ' END'
                Case cFunc='CONCAT'
                    Return '||'
                Case cFunc='FCONCAT'
                    Dimension L_ArrParams(2)
                    Local L_Result
                    ParamParsing(@L_ArrParams, cParam, cVir)
                    L_Result = 'CONCAT'+ cParAp+ L_ArrParams(1)+ cVir +L_ArrParams(2)+ cParCh
                    Release L_ArrParams
                    Return L_Result
                Case cFunc='DIFFDAY'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return '('+cParamFIN +'-'+cParamINI+')'
                Case cFunc == 'ADDDATE'
                * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    Dimension L_ArrParams(2)
                    Local L_Result
                    ParamParsing(@L_ArrParams, cParam, cVir)
                    L_Result = cParAp+'CAST('+L_ArrParams(1)+' AS DATE) + CAST(' + L_ArrParams(2)+ ' AS INTEGER)'+ cParCh
                    Release L_ArrParams
                    Return L_Result
                Case cFunc == 'ADDDATETIME'
                * Restituisce una data ottenuta aggiungendo (sottraendo) un intervallo di giorni ad una data di partenza
                    Dimension L_ArrParams(2)
                    Local L_Result
                    ParamParsing(@L_ArrParams, cParam, cVir)
                    L_Result = "(CAST("+L_ArrParams(1)+" as timestamp) + CAST(" +L_ArrParams(2)+ " as decimal(12,3))*interval '1 day')"
                    Release L_ArrParams
                    Return L_Result
                Case cFunc = 'DATAVUOTA'
                    If Vartype(g_ORACLEVERS)='C' And g_ORACLEVERS = '8'
                        Return(cp_ToStrODBC(i_INIDAT))
                    Else
                        Return 'CAST' + cParAp + 'NULL As Date' + cParCh
                    Endif
                Case cFunc = 'CASTD'
                    Dimension L_ArrParams(3)
                    Local L_Result
                    ParamParsing(@L_ArrParams, cParam, cVir)
                    L_Result = 'CAST'+cParAp+L_ArrParams[1]+' AS DECIMAL'+cParAp+L_ArrParams[2]+cVir+L_ArrParams[3]+cParCh+cParCh
                    Release L_ArrParams
                    Return L_Result
                Case cFunc = 'CASTP'
                    Return 'CAST('+cParam+')'
                Case cFunc='BCASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases>0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE '+cParam+' END'
                Case cFunc='WEEK'
                    Return 'EXTRACT(WEEK FROM '+cParam+')'
                Case cFunc='QUARTER'
                    Return 'EXTRACT(QUARTER FROM '+cParam+')'
                Case cFunc='SEMESTER'
                    Return 'CAST(.5*(.9+EXTRACT(QUARTER FROM '+cParam+')) as DECIMAL(4,0))'
                Case cFunc='DOW'
                    Return 'EXTRACT(DOW FROM '+cParam+')'
                Case cFunc = 'AT'
                    Dimension L_ArrParams(2)
                    Local L_Result
                    ParamParsing(@L_ArrParams, cParam, cVir)
                    L_Result = 'POSITION'+ cParAp+ L_ArrParams(1)+' IN '+L_ArrParams(2)+ cParCh
                    Release L_ArrParams
                    Return L_Result
                Case cFunc = 'RIGHT'
                    * Per problema Right su DB2 � stato inserito un terzo parametro nella Right.
                    * E' possibile usare la Right con 3 parametri perch� in DB2 a volte � necessario effettuare una cast sul secondo parametro
                    Local i_nRight
                    i_nRight=At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, i_nRight-1)
                    If Occurs(cVir,cParam)= 2
                        * Se presenti pi� di due parametri, significa che intendo fare la CAST
                        cParamFIN = Substr(cParam, i_nRight+1)
                        cParamFIN = Left(cParamFIN, At(cVir,cParamFIN)-1)
                        cParamCast = Substr(cParam, Rat(cVir, cParam)+1)
                    Else
                        * Non faccio la cast as Varchar nel caso di Right normale
                        cParamCast = "0"
                        cParamFIN = Substr(cParam, i_nRight+1)
                    Endif
                    Return cFunc + cParAp + cParamINI + cVir + cParamFIN + cParCh
                Case cFunc = 'DTOC' And g_APPLICATION <> "ADHOC REVOLUTION"
                    *** Zucchetti Aulla Inizio DTOC
                    cParam=Iif(At(cVir,cParam)=0,cParam+cVir+'105',cParam)
                    nVir=At(cVir,cParam)
                    cParamINI=Left(cParam,nVir-1)
                    Do Case
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='101'
                            cParamTip="'MM/DD/YYYY'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='102'
                            cParamTip="'YYYY.MM.DD'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='103'
                            cParamTip="'DD/MM/YYYY'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='105'
                            cParamTip="'DD-MM-YYYY'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='110'
                            cParamTip="'MM-DD-YYYY'"
                        Case Alltrim(Right(cParam,Len(cParam)-nVir))='112'
                            cParamTip="'YYYYMMDD'"
                        Otherwise
                            cParamTip="'DD-MM-YYYY'"
                    Endcase
                    Return 'TO_CHAR' + cParAp + cParamINI + cVir + cParamTip + cParCh
                    *** Zucchetti Aulla Fine DTOC
                    *** Return "TO_CHAR(" + cParam + ",'DD-MM-YYYY')"
                    *** Zucchetti Aulla Inizio funzioni orario
				Case cFunc == 'SECOND'
					Return "EXTRACT(SECOND FROM CAST("+cParam+" AS TIMESTAMP))"
				Case cFunc == 'MINUTE'
					Return "EXTRACT(MINUTE FROM CAST("+cParam+" AS TIMESTAMP))"
				Case cFunc == 'HOUR'
	                Return "EXTRACT(HOUR FROM CAST("+cParam+" AS TIMESTAMP))"
	                *** Zucchetti Aulla Fine funzioni orario
                Case cFunc=='ABS' OR cFunc=='UPPER' OR cFunc=='ASCII' OR cFunc=='CHR' OR cFunc=='ABS'
                    Return cFunc + cParAp + cParam + cParCh
                Case cFunc='TTOC'
                    Local ppp,cP2
                    ppp=At(',',cParam)
                    cP2=Substr(cParam,ppp+1)
                    cParam=Iif(ppp=0,cParam,Left(cParam,ppp-1))
                    If ppp=0 Or cP2<>"'R'"
                        Return "TO_CHAR(CAST("+cParam+" AS TIMESTAMP),CAST('DD-MM-YYYY HH24:MI:SS' AS TEXT))"
                    Else
                        Return "TO_CHAR(CAST("+cParam+" AS TIMESTAMP),CAST('YYYY-MM-DD HH24:MI:SS' AS TEXT))"
                    Endif
                    *--- Zucchetti Aulla - introdotto per poter avere una funzione dipendente da un campo della query
                Case cFunc = 'MOD'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
*                    Return cParAp + cParamINI + ' % ' + cParamFIN + cParCh
                    Return '('+cParamINI+' % '+cParamFIN+')'                    
            Endcase

        Case cTipoDB='MYSQL' And cFunc='VAL'
            Return 'CAST('+cParam+' AS SIGNED)'
        Case cTipoDB='MYSQL' And cFunc = 'NVL' And !Empty(cNestedFuncs) And 'AS DATE)'$Upper(cNestedFuncs)
            Return 'CAST(COALESCE('+cParam+') AS DATE)'
        Case cTipoDB='MYSQL' And cFunc='DIFFDAY'
            nVir = At(cVir,cParam)
            cParamINI = Substr(cParam, 1, nVir-1)
            cParamFIN = Substr(cParam, nVir+1)
            Return 'TO_DAYS('+cParamFIN +')-TO_DAYS('+cParamINI+')'
        Case cTipoDB='MYSQL' And cFunc='DAYOFWEEK'
            Return "weekday("+cParam+")+1"

        Case cTipoDB='DBMAKER'
        
            Do Case
                Case cFunc = 'VAL'
                    Return 'CAST('+cParam+' AS FLOAT)'
                Case cFunc == 'STR'
                    Return 'CAST('+cParam+' AS CHAR)'
                Case cFunc = 'LTRIM'
                    Return 'LTRIM('+cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'RTRIM('+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'RTRIM(LTRIM('+cParam+'))'
                Case cFunc = 'SUBSTR'
                    If Occurs(cVir, cParam) > 1
                        cParam = Stuff(cParam, Rat(cVir, cParam), 1, ' FOR ')
                    Endif
                    Return 'SUBSTRING('+Stuff(cParam, Rat(cVir, cParam), 1, ' FROM ') + ')'
                Case cFunc = 'YEAR'
                    Return 'YEAR('+cParam+')'
                Case cFunc = 'MONTH'
                    Return 'MONTH('+cParam+')'
                Case cFunc == 'DAY'
                    Return 'DAYOFMONTH('+cParam+')'
                Case cFunc = 'DATE'
                    Return 'CAST('+cp_NormalizeDateConst(cParam)+' AS DATE)'
                Case cFunc = 'NVL'
                    Return 'COALESCE('+cParam+')'
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'LENGTH('+cParam+')'
                Case cFunc = 'LEN'
                    Return 'CASE WHEN '+cParam+' IS NULL THEN 0 ELSE LENGTH('+cParam+') END'
                Case cFunc='NOTEMPTYDATE'
                    Return 'CASE WHEN '+cParam+' IS NULL THEN 0 ELSE 1 END'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'COALESCE('+cParam+ ',0)'
                Case cFunc == 'CASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases > 0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE ' + cParam + ' END'
                Case cFunc = 'CONCAT'
                    Return '||'
                Case cFunc='DIFFDAY'
                    nVir = At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, nVir-1)
                    cParamFIN = Substr(cParam, nVir+1)
                    Return 'CASE WHEN '+cParamINI + '<' + cParamFIN+' THEN DAYS_BETWEEN(' + cParamINI + ',' + cParamFIN + ') ELSE 0-DAYS_BETWEEN(' + cParamINI + ',' + cParamFIN +') END'
            Endcase
        Otherwise && SQL Standard
            Do Case
                Case cFunc = 'VAL'
                    Return 'CAST('+cParam+' AS FLOAT)'
                Case cFunc == 'STR'
                    Return 'CAST('+cParam+' AS CHAR)'
                Case cFunc = 'LTRIM'
                    Return 'TRIM(LEADING FROM '+cParam+')'
                Case cFunc = 'RTRIM'
                    Return 'TRIM(TRAILING FROM '+cParam+')'
                Case cFunc = 'TRIM'
                    Return 'TRIM('+cParam+')'
                Case cFunc = 'SUBSTR'
                    If Occurs(cVir, cParam) > 1
                        cParam = Stuff(cParam, Rat(cVir, cParam), 1, ' FOR ')
                    Endif
                    Return 'SUBSTRING('+Stuff(cParam, Rat(cVir, cParam), 1, ' FROM ') + ')'
                Case cFunc = 'YEAR'
                    Return 'EXTRACT(YEAR FROM '+cParam+')'
                Case cFunc = 'MONTH'
                    Return 'EXTRACT(MONTH FROM '+cParam+')'
                Case cFunc == 'DAY'
                    Return 'EXTRACT(DAY FROM '+cParam+')'
                Case cFunc = 'DATE'
                    Return 'CAST('+cp_NormalizeDateConst(cParam)+' AS DATE)'
                Case cFunc = "DAYOFWEEK"
                    Return "EXTRACT(WEEKDAY FROM "+cParam+")"
                Case cFunc = 'NVL'
                    Return 'COALESCE('+cParam+')'
                Case cFunc = 'NOTEMPTYSTR'
                    Return 'COALESCE(CHAR_LENGTH(TRIM('+cParam+')),0)'
                Case cFunc = 'LEN'
                    Return 'COALESCE(CHAR_LENGTH('+cParam+'),0)'
                Case cFunc='NOTEMPTYDATE'
                    Return 'COALESCE(CHAR_LENGTH(TRIM(CAST('+cParam+' AS CHAR))),0)'
                Case cFunc = 'NOTEMPTYNUM'
                    Return 'COALESCE('+cParam+ ',0)'
                Case cFunc == 'CASE'
                    nCases = (Occurs(cVir, cParam)-1)/2
                    Do While nCases > 0
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                        cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                        nCases = nCases-1
                    Enddo
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                    Return 'CASE ' + cParam + ' END'
                Case cFunc = 'CONCAT'
                    Return '||'
                * --- Zucchetti Aulla inizio: parser_concat
                Case cFunc='FCONCAT'
                    * --- FCONCAT serve solo per postgres. Altrimenti [FCONCAT(a,b)]=a[CONCAT]b
                    Dimension L_ArrParams(2)
                    Local L_Result
                    ParamParsing(@L_ArrParams, cParam, cVir)
                    L_Result = ' '+ L_ArrParams(1)+ '[CONCAT]' +L_ArrParams(2)+ ' '
                    Release L_ArrParams
                    Return L_Result
                * --- Zucchetti Aulla fine: parser_concat
                Case cFunc = 'RIGHT'
                    * Per problema Right su DB2 � stato inserito un terzo parametro nella Right.
                    * E' possibile usare la Right con 3 parametri perch� in DB2 a volte � necessario effettuare una cast sul secondo parametro
                    Local i_nRight
                    i_nRight=At(cVir,cParam)
                    cParamINI = Substr(cParam, 1, i_nRight-1)
                    If Occurs(cVir,cParam)= 2
                        * Se presenti pi� di due parametri, significa che intendo fare la CAST
                        cParamFIN = Substr(cParam, i_nRight+1)
                        cParamFIN = Left(cParamFIN, At(cVir,cParamFIN)-1)
                        cParamCast = Substr(cParam, Rat(cVir, cParam)+1)
                    Else
                        * Non faccio la cast as Varchar nel caso di Right normale
                        cParamCast = "0"
                        cParamFIN = Substr(cParam, i_nRight+1)
                    Endif
                    Return cFunc + cParAp + cParamINI + cVir + cParamFIN + cParCh
            Endcase
    Endcase
    *Funzioni generiche
    Do Case
        Case cFunc='ABS'
            Return 'ABS('+cParam+')'
        Case cFunc == 'CASTP'
            *** CAST PER POSTGRES: negli altri DB deve essere rimossa
            nASCH = At(" AS CHAR(",cParam)
            cParam = Substr(cParam, 1, nASCH-1)
            Return cParam
        Case cFunc = 'GLOBALVAR'
            If cTipoDB = 'VFP'
                Return(cp_ToStr(&cParam))
            Else
                Return(cp_ToStrODBC(&cParam))
            Endif
        Case cFunc = 'SPACES'
            Return cApi + Space(Val(cParam)) + cApi
            * ---- Funzione equipollente a DTOC Vfp converte un campo data
            * ---- in una stringa gg-mm-yyyy. Se campo Null cambia a seconda delle funzioni
            * ---- DAY,MONTH e YEAR sul tipo di server.
            * ---- se passata con ulteriore parametro 'R' trasforma in yyyy-mm-gg
        Case cFunc = 'DTOC' And g_APPLICATION = "ADHOC REVOLUTION" && Zucchetti Aulla aggiunto AND
            Local ppp,cP2
            ppp=At(',',cParam)
            cP1=Left(cParam,ppp-1)
            cP2=Substr(cParam,ppp+1)
            cParam=Iif(ppp=0,cParam,Left(cParam,ppp-1))
            If ppp=0 Or cP2<>"'R'"
                Return  "[SUBSTR"+cParAp+"[ZEROFILL"+cParAp+"[DAY"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"'-'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[MONTH"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"'-'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[YEAR"+cParAp +cParam + cParCh+"],4" +cParCh+"],1,10"+ cParCh+"]"
            Else
                Return  "[SUBSTR"+cParAp+"[ZEROFILL"+cParAp+"[YEAR"+cParAp +cParam + cParCh+"],4"+ cParCh+"]"+"[CONCAT]"+"'-'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[MONTH"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"'-'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[DAY"+cParAp +cParam + cParCh+"],2" + cParCh+"],1,10"+ cParCh+"]"
            Endif
        Case cFunc = 'FDAYMONTH'
            * Restituisce la data del primo giorno del mese di una specifica data
            Return "[ADDDATE"+cParAp+cParam+cVir+"-[DAY"+cParAp+cParam+cParCh+"] + 1"+cParCh+"]"
        Case cFunc = 'LDAYMONTH'
            * Restituisce la data dell'ultimo giorno del mese di una specifica data
            Return "[ADDDATE"+cParAp+cParam+cVir+cParAp+"32 - [DAY"+cParAp+"[ADDDATE"+cParAp+"[FDAYMONTH"+cParAp+cParam+cParCh+"]"+cVir+" 31"+cParCh+"]"+cParCh+"]"+cParCh+" - [DAY"+cParAp+cParam+cParCh+"]"+cParCh+"]"
        Case cFunc = 'NDAYMONTH'
            * Restituisce il numero di giorni del mese di una specifica data
            Return cParAp+"32 - [DAY"+cParAp+"[ADDDATE"+cParAp+"[FDAYMONTH"+cParAp+cParam+cParCh+"]"+cVir+" 31"+cParCh+"]"+cParCh+"]"+cParCh
        Case cFunc = 'BEGINWEEK'
            * Restituisce la data del primo giorno della settimana di una specifica data
            Return "[BCASE"+cParAp+" "+cVir+" [DOW"+cParAp+cParam+cParCh+"]<>1"+cVir+;
                "[ADDDATE"+cParAp+cParam+cVir+"-[DOW"+cParAp+cParam+cParCh+"]+2"+cParCh+"]"+cVir+;
                "[ADDDATE"+cParAp+cParam+cVir+"-6"+cParCh+"]"+cParCh+"]"
        Case cFunc = 'ENDWEEK'
            * Restituisce la data dell'ultimo giorno della settimana di una specifica data
            Return "[BCASE"+cParAp+" "+cVir+" [DOW"+cParAp+cParam+cParCh+"]<>1"+cVir+;
                "[ADDDATE"+cParAp+cParam+cVir+"-[DOW"+cParAp+cParam+cParCh+"]+8"+cParCh+"]"+cVir+;
                cParam+cParCh+"]"
        Case cFunc = 'ZEROFILL'
            Local ppp,cP1,cP2,cR1,cRep
            ppp=At(',',cParam)
            cP1=Iif(ppp=0,cParam,Left(cParam,ppp-1))
            cP2=Iif(ppp=0,'10',Substr(cParam,ppp+1))
            cRep = "'"+Replicate('0', Val(cP2))+"'"
            Do Case
                Case cTipoDB='VFP'
                    Return "right("+cRep+"alltrim(str("+cP1+','+cP2+',0)),'+cP2+')'
                Case cTipoDB = 'DB2'
                    cR1=+cRep+" [CONCAT] [TRIM( [STR(CAST ( "+cP1+" as INTEGER) )] )]"
                    Return "[SUBSTR("+cR1+","+"[LEN("+cR1+")]"+"-"+cP2+"+1,"+cP2+")]"
                Otherwise
                    cR1=cRep+" [CONCAT] [TRIM( [STR("+cP1+")] )]"
                    Return "[SUBSTR("+cR1+","+"[LEN("+cR1+")]"+"-"+cP2+"+1,"+cP2+")]"
            Endcase
        Case cFunc='MACRO'
            If Left(cParam,1)="'" And Right(cParam,1)="'"
                Return (" " + Alltrim(Substr(cParam,2,Len(cParam)-2)) + " ")
            Else
                Return(cParam)
            Endif
        Case cFunc="CURRENTDATE"
            If cTipoDB='VFP'
                Return " "+Dtoc(Date())+" "
            Else
                Return " "+cp_ToStrODBC(Date())+" "
            Endif
        Case cFunc="CURRENTTIME"
            If cTipoDB='VFP'
                Return " "+Ttoc(Datetime())+" "
            Else
                Return " "+cp_ToStrODBC(Datetime())+" "
            Endif
        Case cFunc="USERCODE"
            Return " "+Alltrim(Str(i_codute,10,0))+" "
        Case cFunc="USERGROUPS"
            Return " "+cp_getGroupsArray()+" "
        Case cFunc="USERROLES"
            Return " "+cp_getRolesArray()+" "
        Case cFunc="COMPANYCODE"
            Return " '"+i_codazi+"' "
        Case cFunc = 'CASE2'
            If cTipoDB='VFP'
                Return 'iif'+cParAp+cParam+cParCh
            Else
                cParam=','+cParam
                Local nPA, cOldParam
                nPA=0
                cOldParam = cParam
                cParam = ""
                For l_i = 1 To Len(cOldParam)
                    cChar = Substr(cOldParam, l_i , 1)
                    If cChar  = cParAp
                        nPA = nPA + 1
                    Endif
                    If cChar = cParCh
                        nPA = nPA - 1
                    Endif
                    If cChar = cVir And nPA > 0
                        cChar = '��'
                    Endif
                    cParam = cParam + cChar
                Endfor
                nCases = (Occurs(cVir, cParam))/2 - 1
                Do While nCases > 0
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' WHEN ')
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' THEN ')
                    nCases = nCases-1
                Enddo
                If At(cVir,cParam)<>0
                    cParam = Stuff(cParam, At(cVir, cParam), 1, ' ELSE ')
                Endif
                Return 'CASE ' + Strtran(cParam, "��", cVir) + ' END'
            Endif
        Case cFunc="TRANSLATE"
            * --- Aggiunto parametro per restituire
            * --- a seguito della traduzione al massimo quel numero di caratteri
            * --- per evitare problemi se query per popolare strutture gia presenti
            * --- come temporanei
            Local ppp,cP1,cP2
            ppp=At(',',cParam)
            If ppp=0
                cP1=Substr(cParam, At("'",cParam)+1, Rat("'",cParam)-2)
            Else
                cP1=Substr(Left(cParam,ppp-1), At("'",cParam)+1, Rat("'",cParam)-2)
            Endif
            cP2=Iif(ppp=0,Len(cP1),Val(Substr(cParam,ppp+1)))
            cParam=cp_Translate(cP1)
            cP2=Iif(ppp=0,Max(Len(cParam), cP2), cP2)
            cParam=Padr(cParam, cP2)
            Return cp_ToStrODBC(cParam)
        Case cFunc == 'T'
            Return Iif(Not Empty(i_cLanguageData), 'COALESCE('+cParam+'_'+i_cLanguageData+','+cParam+')',cParam)
        Case cFunc == 'T2'
            Return Iif(Not Empty(i_cLanguageData), cParam+'_'+i_cLanguageData, cParam)
        Case cFunc == 'T!'
            * --- Funzione per visualizzare un campo in lingua in funzione di una lingua presente su un campo
            * --- [T!(FieldTranslate,FieldLanguage)]
            * --- il primo campo � il campo da tradurre e il secondo campo � il campo che contiene la lingua in cui tradurre
            If Type("i_aCpLangs")='U'
                cp_LanguageTranslateData()
            Endif
            Local i_result,ppp,cP1,cP2
            ppp=At(',',cParam)
            cP1=Left(cParam,ppp-1)
            cP2=Substr(cParam,ppp+1)
            If Empty(i_aCpLangs[1])
                i_result=Alltrim(cP1)
            Else
                i_result="[CASE("+cP2+","
                For i=1 To Alen(i_aCpLangs)
                    i_result=i_result+cp_ToStrODBC(i_aCpLangs[i])+",[NVL("+Alltrim(cP1)+"_"+i_aCpLangs[i]+","+Alltrim(cP1)+")],"
                Endfor
                i_result=i_result+Alltrim(cP1)+")]"
            Endif
            Return i_result
            *--- Zucchetti Aulla inizio (obsoleta usare NULLSTRING)
        Case cFunc = 'STRINGAVUOTA'
            Return 'CAST(NULL as '+db_fieldtype(cTipoDB,'C',Max(Int(Val(cParam)),1),0)+')'
            *--- Zucchetti Aulla fine (funzioni ad hoc)
        Case cFunc = 'VFPMACRO'
            * Funzione per eseguire la macro del comando senza trasform arlo in stringa come la GLOBALVAR
            * Serve nel caso viene utilizzata una funzione che crea gi� una stringa da applicare alla frase Sql generata
            * Nel caso in cui siano contenuti dei paramentri ?w_Param occorre tradurli con la FillParam
            If(At('?',cParam)>0)
                * La traduzione va operata sempre come se il database fosse VFP
                cParam=obj.FillParam(cParam,.T.)
            Endif
            Return(&cParam)
        Case cFunc = 'NUMBER'
            * Funzione per applicare un tipo numerico, utile nel caso di calcoli tra campi numerici
            * --- Primo parametro il valore al quale applicare la CAST...
            Local sImporto, sType
            nVir = At(cVir,cParam)
            sImporto = Left(cParam,nVir-1)
            cParam=Substr(cParam,nVir+1)
            If Empty(Nvl(cParam, ' '))
                cParam="1,0"
            Endif
            nVir = At(cVir,cParam)
            cParLung = Substr(cParam, 1, nVir-1)
            cParPrec = Substr(cParam, nVir+1)

            Do Case
                Case cTipoDB='VFP' Or cTipoDB='DBF'
                    sType='N'
                Case cTipoDB='ORACLE' Or At('DB2',cTipoDB)=1 Or cTipoDB='INFORMIX' Or cTipoDB='POSTGRESQL'
                    sType='Numeric'
                Case cTipoDB='Adabas' Or cTipoDB='SAPDB'
                    sType='Fixed'
                Case cTipoDB='Interbase'
                    sType='Float'
                Otherwise
                    sType='Decimal'
            Endcase
            Return 'CAST('+sImporto+' AS '+sType+'('+cParLung+','+cParPrec+'))'
            *--- Restiruisce il maggiore tra due valori
        Case cFunc = 'MAX2'
            Local nPar, nPA, cNewParam, cChar
            nPA=0
            cNewParam = ""
            cChar = ""
            For l_i = 1 To Len(cParam)
                cChar = Substr(cParam, l_i, 1)
                If cChar = cParAp
                    nPA = nPA + 1
                Endif
                If cChar = cParCh
                    nPA = nPA - 1
                Endif
                If cChar = cVir And nPA>0
                    cChar = "��"
                Endif
                cNewParam = cNewParam + cChar
            Endfor
            nPar = Alines(aParam, cNewParam, cVir)
            Return "[CASE2"+cParAp+" "+Strtran(aParam[1], "��", cVir)+" > "+Strtran(aParam[2], "��", cVir)+" "+cVir+" "+Strtran(aParam[1], "��", cVir)+" "+cVir+" "+Strtran(aParam[2], "��", cVir)+" "+cParCh+"]"
            *--- Restituisce il maggiore tra n valori
        Case cFunc = 'MAXN'
            Local nPar, cRet, l_i, l_j, nItem
            Local nPA, cNewParam, cChar
            nPA=0
            cNewParam = ""
            cChar = ""
            For l_i = 1 To Len(cParam)
                cChar = Substr(cParam, l_i, 1)
                If cChar = cParAp
                    nPA = nPA + 1
                Endif
                If cChar = cParCh
                    nPA = nPA - 1
                Endif
                If cChar = cVir And nPA>0
                    cChar = "��"
                Endif
                cNewParam = cNewParam + cChar
            Endfor
            nPar = Alines(aParam, cNewParam, cVir)
            l_j = 1
            l_i = 1
            Do While l_i <= Alen(aParam)
                l_j = l_i + 1
                If l_j <= Alen(aParam)
                    cRet = cp_Max2(Strtran(aParam[l_i],"��", cVir), Strtran(aParam[l_j],"��", cVir))
                    nItem = Alen(aParam) + 1
                    Dimension aParam[nItem]
                    aParam[nItem] = cRet
                Endif
                l_i = l_i + 2
            Enddo
            Return aParam[ALEN(aParam)]
        Case cFunc = 'NULLNUMBER'
            * Funzione per creare un campo numerico vuoto con la struttura desiderata
            If Empty(Nvl(cParam, ' '))
                cParam="1,0"
            Endif
            nVir = At(cVir,cParam)
            cParLung = Substr(cParam, 1, nVir-1)
            cParPrec = Substr(cParam, nVir+1)
            Return 'CAST(NULL AS '+db_fieldtype(cTipoDB,'N',Max(Int(Val(cParLung)),1),Max(Int(Val(cParPrec)),1))+')'
        Case cFunc = 'NULLSTRING'
            * Funzione per creare un campo stringa vuoto con la lunghezza desiderata
            If Empty(Nvl(cParam, ' '))
                cParam="1"
            Endif
            Return 'CAST(NULL as '+db_fieldtype(cTipoDB,'C',Max(Int(Val(cParam)),1),0)+')'
        Case cFunc = 'NULLBOOL'
            Return 'CAST(NULL as '+db_fieldtype(cTipoDB,'L')+')'
        Case cFunc = 'NULLMEMO'
            Return 'CAST(NULL as '+db_fieldtype(cTipoDB,'M')+')'
        Case cFunc = 'LOWER'
            If cTipoDB='ACCESS'
                Return 'LCASE('+cParam+')'
            Endif
        Case cFunc = 'UPPER'
            If cTipoDB='ACCESS'
                Return 'UCASE('+cParam+')'
            Endif
            *--- Zucchetti Aulla inizio (funzioni ad hoc)
        Case cFunc == 'ADDDATETIME'
            If At('DB2',cTipoDB)=1
                cParam = Alltrim(cParam)
                If Left(cParam,1)=='?'
                    nVir = At(cVir,cParam)
                    cFirstParam = Left(cParam, nVir-1)
                    cLastParam = Right(cParam, Len(cParam)-nVir-1)
                    Return 'TIMESTAMP(date('+cFirstParam+'),time('+cFirstParam+')) + ' + cLastParam + ' days'
                Else
                    Return Strtran(cParam,cVir,'+') +' days'
                Endif
            Else
                Return '[ADDDATE'+cParAp+cParam+cParCh+']'
            Endif
        Case cFunc == 'EMPTYDATETIME'
            If At('DB2',cTipoDB)=1 OR cTipoDB="POSTGRESQL"
                Return "CAST(NULL AS TIMESTAMP)"
            Else
                Return "[DATAVUOTA]"
            EndIf
        Case cFunc = 'TTOC' &&And g_APPLICATION = "ADHOC REVOLUTION"
            Local ppp,cP2
            ppp=At(',',cParam)
*!*	            cP1=Left(cParam,ppp-1)
            cP2=Substr(cParam,ppp+1)
            cParam=Iif(ppp=0,cParam,Left(cParam,ppp-1))
            If ppp=0 Or cP2<>"'R'"
                Return  "CAST([SUBSTR"+cParAp+"[ZEROFILL"+cParAp+"[DAY"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"'-'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[MONTH"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"'-'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[YEAR"+cParAp +cParam + cParCh+"],4" +cParCh+"]"+"[CONCAT]"+"' '"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[HOUR"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"':'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[MINUTE"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"':'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[SECOND"+cParAp +cParam + cParCh+"],2" +cParCh+"],1,19"+ cParCh+"] AS VARCHAR(19))"
            Else
                Return  "CAST([SUBSTR"+cParAp+"[ZEROFILL"+cParAp+"[YEAR"+cParAp +cParam + cParCh+"],4"+ cParCh+"]"+"[CONCAT]"+"'-'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[MONTH"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"'-'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[DAY"+cParAp +cParam + cParCh+"],2" + cParCh+"]"+"[CONCAT]"+"':'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[HOUR"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"':'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[MINUTE"+cParAp +cParam + cParCh+"],2"+ cParCh+"]"+"[CONCAT]"+"':'"+"[CONCAT]"+;
                    "[ZEROFILL"+cParAp+"[SECOND"+cParAp +cParam + cParCh+"],2" +cParCh+"],1,19"+ cParCh+"] AS VARCHAR(19))"
            Endif
            *--- Zucchetti Aulla fine (funzioni ad hoc)
    Endcase
    Return(cIn)

    *--- Funzione utilizzata per la creazione del comando MAXN
Function cp_Max2(cFirst, cSecond)
    Return "[MAX2( " + cFirst + " , " + cSecond + " )]"

Function cp_NormalizeDateConst(cDate)
    Local i_res,occur1,occur2,lencDate
    i_res=Alltrim(cDate)
    If Left(i_res,1)="'" And Right(i_res,1)="'" And At('[',i_res)=0
        i_res=Strtran(i_res,'/','-')
        occur1 = At('-',i_res)
        occur2 = Rat('-',i_res)
        lencDate=Len(cDate)
        If lencDate-occur2-1 = 4 && e' del tipo '2000-...'
            i_res="'"+Substr(i_res,occur2+1,4)+"-"+Right("0"+Substr(i_res,occur1+1,occur2-occur1-1),2)+"-"+Right("0"+Substr(i_res,2,occur1-2),2)+"'" && e' del tipo '...-2000'
        Else
            i_res="'20"+Substr(i_res,occur2+1,2)+"-"+Right("0"+Substr(i_res,occur1+1,occur2-occur1-1),2)+"-"+Right("0"+Substr(i_res,2,occur1-2),2)+"'" && e' del tipo '...-00'
        Endif
    Else
        i_res=cDate
    Endif
    Return(i_res)

* --- Zucchetti Aulla inizio: parser_concat
Function parser_concat(cexpr As String, nLevel As Integer) As String
    * questa funzione trasforma l'operatore arg1[CONCAT]arg2 nella funzione [FCONCAT(arg1,arg2)]
    * ci� serve in PostgreSQL quando si concatenano due campi di tipo char, altrimenti gli spazi sarebbero trimmati
    * il problema non si presenta con le costanti, il tipo varchar e text (quindi anche tutte le funzioni sulle stringhe)
    * in questo caso viene mantenuto l'operatore [CONCAT]
    Local i_char,i_conc,i_cwork,csxarg,cdxarg,i_cSearch,i_pc,i_pl,i_pr,plen,i_np,i_cOK,i_nAp,i_pa,i_paa,nindc,nOccur,i_cCost,nindc,i_pfi
    i_conc="[CONCAT]"
    nindc=0
    i_cwork=Strtran(cexpr,i_conc,i_conc,-1,-1,1)
    i_pc=At(i_conc,i_cwork)
    If i_pc=0
        Return cexpr
    Endif
    If nLevel=0
        i_nAp=Occurs("'",i_cwork)
        Do Case
            Case i_nAp=1
                * errore nella stringa
                Return cexpr
            Case i_nAp>1
                Dimension cConst(Int(i_nAp/2))
                i_pa=At("'",i_cwork)
                Do While i_pa>0
                    i_cCost="'"
                    crest=Substr(i_cwork,i_pa+1)
                    i_paa=At("''",crest)
                    i_pa=At("'",crest)
                    Do While i_paa>0 And i_paa<=i_pa
                        i_cCost=i_cCost+Left(crest,i_paa)+"'"
                        crest=Substr(crest,i_paa+2)
                        i_paa=At("''",crest)
                        i_pa=At("'",crest)
                    Enddo
                    i_pfi=At("'",crest)
                    If i_pfi=0
                        * errore nella stringa
                        Return cexpr
                    Else
                        i_cCost=i_cCost+Left(crest,i_pfi)
                        nindc=nindc+1
                        cConst[nindc]=i_cCost
                        i_cwork=Strtran(i_cwork,i_cCost,Chr(254)+Alltrim(Str(nindc,3,0))+Chr(254))
                    Endif
                    i_pa=At("'",i_cwork)
                Enddo
        Endcase
        i_nAp=Occurs("?",i_cwork)
        If i_nAp>0
            Dimension cConst(nindc+i_nAp)
            i_nAp=At("?",i_cwork)
            Do While i_nAp>0
                i_pr=i_nAp+1
                plen=Len(i_cwork)
                i_char=Substr(i_cwork,i_pr,1)
                Do While i_pr<plen And Between(Upper(i_char),"A","Z") Or i_char $ "01234567890._"
                    i_pr=i_pr+1
                    i_char=Substr(i_cwork,i_pr,1)
                Enddo
                i_cCost=Substr(i_cwork,i_nAp,i_pr-i_nAp)
                nindc=nindc+1
                cConst[nindc]=i_cCost
                i_cwork=Strtran(i_cwork,i_cCost,Chr(254)+Alltrim(Str(nindc,3,0))+Chr(254))
                i_nAp=At("?",i_cwork)
            Enddo
        Endif
    Endif
    i_pc=At(i_conc,i_cwork)
    nOccur=1
    Do While i_pc>0
        * cerco l'espressione a sinistra
        i_pl=i_pc-1
        * elimino gli spazi
        Do While i_pl>1 And Empty(Substr(i_cwork,i_pl,1))
            i_pl=i_pl-1
        Enddo
        csxarg=""
        i_char=Substr(i_cwork,i_pl,1)
        Do Case
            Case Between(Upper(i_char),"A","Z") Or i_char $ "01234567890._"
                Do While i_pl>=1 And Between(Upper(i_char),"A","Z") Or i_char $ "01234567890._"
                    i_pl=i_pl-1
                    i_char=Substr(i_cwork,i_pl,1)
                Enddo
                i_pl=i_pl+1
            Case i_char="]"
                * cerco la parentesi di apertura
                i_np=1
                Do While i_np>0 And i_pl>1
                    i_pl=i_pl-1
                    i_char=Substr(i_cwork,i_pl,1)
                    i_np=Icase(i_char="[",i_np-1,i_char="]",i_np+1,i_np)
                Enddo
            Case i_char=Chr(254) OR i_char=Chr(255)
                * cerco l'apice di apertura oppure l'apertura della concat gi� risolta
                i_pl=Rat(i_char,Left(i_cwork,i_pl-1))
            Otherwise
                * carattere non atteso
                Return cexpr
        Endcase
        csxarg=Substr(i_cwork,i_pl,i_pc-i_pl)
        If csxarg="[" And At(i_conc,csxarg)>0
            csxarg=parser_concat(csxarg,nLevel+1)
        Endif
        If Empty(csxarg)
            csxarg=Left(i_cwork,i_pc-1)
        Endif
        If Empty(csxarg)
            * errore nella stringa
            Return cexpr
        Endif
        * cerco l'espressione a destra
        i_pr=i_pc+Len(i_conc)
        plen=Len(i_cwork)
        * elimino gli spazi
        Do While i_pr<plen And Empty(Substr(i_cwork,i_pr,1))
            i_pr=i_pr+1
        Enddo
        cdxarg=""
        i_char=Substr(i_cwork,i_pr,1)
        Do Case
            Case Between(Upper(i_char),"A","Z") Or i_char $ "01234567890._"
                Do While i_pr<=plen And Between(Upper(i_char),"A","Z") Or i_char $ "01234567890._"
                    i_pr=i_pr+1
                    i_char=Substr(i_cwork,i_pr,1)
                Enddo
                i_pr=i_pr-1
            Case i_char="["
                * cerco la parentesi di chiusura
                i_np=1
                Do While i_np>0 And i_pr<plen
                    i_pr=i_pr+1
                    i_char=Substr(i_cwork,i_pr,1)
                    i_np=Icase(i_char="]",i_np-1,i_char="[",i_np+1,i_np)
                Enddo
            Case i_char=Chr(254) OR i_char=Chr(255)
                * cerco l'apice di chiusura o la chiusura della concat gi� risolta
                i_pr=i_pr+At(i_char,Substr(i_cwork,i_pr+1))
            Otherwise
                * carattere non atteso
                Return cexpr
        Endcase
        cdxarg=Substr(i_cwork,i_pc+Len(i_conc),i_pr-(i_pc+Len(i_conc))+1)
        If cdxarg="[" And At(i_conc,cdxarg)>0
            cdxarg=parser_concat(cdxarg,nLevel+1)
        Endif
        If Empty(cdxarg)
            cdxarg=Substr(i_cwork,i_pc+Len(i_conc))
        Endif
        If Empty(cdxarg)
            * errore nella stringa
            Return cexpr
        Endif
        i_cSearch=Substr(i_cwork,i_pl,i_pr-i_pl+1)
        If Not Empty(i_cSearch)
            i_cOK=Chr(255)+Alltrim(Str(nLevel,2,0))+Chr(255)
            i_cwork=Strtran(i_cwork,i_cSearch,i_cOK)
            i_pr=At(i_cOK,i_cwork)+Len(i_cOK)
            If i_conc $ Substr(i_cwork,i_pr)
                i_pr=Max(1,Rat(i_conc,Left(i_cwork,i_pr)))
                i_cwork=Left(i_cwork,i_pr-1) + parser_concat(Substr(i_cwork,i_pr),nLevel+1)
            Endif
            i_cwork=Strtran(i_cwork,i_cOK,"[FCONCAT("+csxarg+","+cdxarg+")]")
        Endif
        * Cerco la prossima concat
        nOccur=nOccur+1
        i_pc=At(i_conc,i_cwork,nOccur)
    Enddo
    If nindc>0
        For i_pa=1 To nindc
            i_cwork=Strtran(i_cwork,Chr(254)+Alltrim(Str(i_pa,3,0))+Chr(254),cConst[i_pa])
        Next
    Endif
    Return i_cwork
Endfunc
* --- Zucchetti Aulla fine: parser_concat

Func cp_ParseSQLFunctions(cString, cTipoDB, nLevel, bOK, cNestedFuncs,obj)
    Local cLeftDelimiter, cRightDelimiter, nLenLDel, nLenRDel, nRD, nLD, nIterLevel
    cLeftDelimiter = '['
    cRightDelimiter = ']'
    nLenLDel = Len(cLeftDelimiter)
    nLenRDel = Len(cRightDelimiter)
    nIterLevel=0
    *-  Non itero se sono al livello diverso da 0
    Do While bOK  And nIterLevel=0
        * Cerca il primo cRightDelimiter da sinistra
        nRD = At(cRightDelimiter, cString)
        If nRD > 0 && Trovato
            * Cerca il primo cLeftDelimiter che precede il cRightDelimiter appena trovato
            nLD = Rat(cLeftDelimiter, Left(cString, nRD-1))
            If nLD > 0 && Trovato
                Local cFunc, cPre, cPost, cPlaceHolder
                * Effettua la traduzione della funzione appena individuata
                cFunc = cp_TranslateSQLFunctions(Substr(cString, nLD+nLenLDel, nRD-(nLD+nLenLDel)), cTipoDB, cNestedFuncs,obj)
                * Aggiorna la stringa da analizzare
                cPre = Left(cString, nLD-1)
                cPost = Substr(cString, nRD+nLenRDel)
                If nLD = At(cLeftDelimiter, cString)
                    * La funzione tradotta era la pi� a sinistra
                    cString = cPre + cFunc + cPost
                Else
                    * La funzione tradotta era nidificata
                    cPlaceHolder = Chr(255) + Ltrim(Str(nLevel)) + Chr(255)
                    cString = cp_ParseSQLFunctions(cPre+cPlaceHolder+cPost, cTipoDB, nLevel+1, @bOK, cFunc,obj)
                    cString = Strtran(cString, cPlaceHolder, cFunc)
                Endif
            Else
                * Errore di parsing
                cString = Stuff(cString, nRD, 0, 'ERROR: unmatched ')
                bOK = .F.
            Endif
        Else
            * Controlla che non ci sia un cLeftDelimiter disaccoppiato
            nLD = At(cLeftDelimiter, cString)
            If nLD > 0 && Trovato
                * Errore di parsing
                cString = Stuff(cString, nLD, 0, 'ERROR: unmatched ')
                bOK = .F.
            Endif
            Exit
        Endif
        nIterLevel=nLevel
    Enddo
    Return cString

Func cp_SetSQLFunctions(cString, cTipoDB,obj)
    Local bOK, l_cRet, l_cString , l_i
    bOK = .T.
    Dimension aEscapeChars[2,3] && EscapeChar, TmpChar, Char
    aEscapeChars[1,1]="\["
    aEscapeChars[1,2]=Chr(157)
    aEscapeChars[1,3]="["
    aEscapeChars[2,1]="\]"
    aEscapeChars[2,2]=Chr(158)
    aEscapeChars[2,3]="]"

    l_cString = m.cString
    For l_i=1 To 2
        l_cString=Strtran(l_cString, aEscapeChars[l_i,1], aEscapeChars[l_i,2])
    Endfor
    * --- Zucchetti Aulla inizio: parser_concat
    If cTipoDB="PostgreSQL"
        l_cString=parser_concat(l_cString,0)
    endif
    * --- Zucchetti Aulla fine: parser_concat
    l_cRet = cp_ParseSQLFunctions(l_cString, cTipoDB, 0, @bOK,"",obj)
    For l_i=1 To 2
        l_cRet=Strtran(l_cRet, aEscapeChars[l_i,2], aEscapeChars[l_i,3])
    Endfor
    Return l_cRet

Procedure cp_SetSQLFunc(i_cInOut,i_cTipoDB)
    Local i_res,i_sql
    i_sql=i_cInOut
    i_res=cp_SetSQLFunctions(i_sql,i_cTipoDB)
    i_cInOut=i_res
    Return

Define Class CPQueryLoader As Custom
    bFox26=.F.
    cNote=''
    Proc Init(bFox26)
        This.bFox26=bFox26
    Proc LoadQuery(cQueryFile,qry,oParentObject,bNoOrderBy,bNoMultiCompany)
		Local xcg,c,h,l,nQueryLoad
		*--- Salvo il nome della query cos� quando si verifica un errore posso restituire il nome della query che � andata in errore
		qry.cQryName = cQueryFile
		qry.SetParentObject(oParentObject)
		qry.bAskParam=.T.
		c=cp_GetPFile(Trim(cQueryFile)+'.vqr')

        *** Zucchetti Aulla - controllo se query infobusiness
        If Empty(Nvl(m.c,''))
            Return
        Endif
        *** Zucchetti Aulla

        xcg=Createobject('pxcg','load',m.c)
        * --- ora carica la query
        This.SerializeTables(xcg,qry)
        This.SerializeFields(xcg,qry)
        This.SerializeRelations(xcg,qry)
        This.SerializeWhere(xcg,qry)
        This.SerializeOrderBy(xcg,qry)
        If bNoOrderBy
            qry.nOrderBy=0
        Endif
        This.SerializeGroupBy(xcg,qry)
        This.SerializeDistinct(xcg,qry)
        This.SerializeParam(xcg,qry,oParentObject)
        If !xcg.bIsStoring
            qry.cExtMask=xcg.Load('C','')
            qry.bRemoveWhereOnEmptyParam=xcg.Load('L',.F.)
            qry.cUnion=xcg.Load('C','')
        Endif
        This.SerializePivot(xcg,qry)
        If !xcg.bIsStoring
            This.cNote=xcg.Load('C','')
            qry.bUnionAll=xcg.Load('L',.F.)
            qry.bOrderByParm=xcg.Load('L',.T.)
            qry.nVersion=xcg.Load('N',0)
            qry.bMultiCompany=xcg.Load('L',.F.)
            qry.cRoles=xcg.Load('C','')
            qry.bPublic=xcg.Load('L',.T.)
            qry.bBatch=xcg.Load('L',.F.)
            qry.cSecKey=xcg.Load('C','')
        Endif
        xcg.Eoc()
        If qry.bOrderByParm And Not bNoOrderBy
            * --- cerca la variabile che aggiunge una condizione di order by dinamica
            If Vartype(oParentObject)='O'
                Local i_olderr
                i_olderr=On('ERROR')
                On Error =.T.
                * --- Zucchetti Aulla inizio - Variabile w_ORDERBY con il nome della query
                Local cMyOrderbyName
                cMyOrderbyName = 'w_OrderBy_'+juststem(qry.cQRYNAME)
                do case
                  case oParentObject.IsVar(cMyOrderbyName )
                    qry.mLoadOrderByTmp(oParentObject.GetVar(cMyOrderbyName))
                  case oParentObject.IsVar('w_OrderBy')  && parte standard
                    qry.mLoadOrderByTmp(oParentObject.GetVar('w_OrderBy'))
                  otherwise
                    If Type('oParentObject.parent.parent.parent.parent')='O'
                      if oParentObject.parent.parent.parent.parent.IsVar(cMyOrderbyName)
                        qry.mLoadOrderByTmp(oParentObject.parent.parent.parent.parent.GetVar(cMyOrderbyName))
                      else
                        if oParentObject.parent.parent.parent.parent.IsVar('w_OrderBy')  && parte standard
                            qry.mLoadOrderByTmp(oParentObject.Parent.Parent.Parent.Parent.GetVar('w_OrderBy'))
                        Endif
                    Endif
                Endif
                Endcase
                * --- Zucchetti Aulla fine - Variabile w_ORDERBY con il nome della query
                On Error &i_olderr
            Else
                If This.bFox26 And Type('w_OrderBy')='C'
                    qry.mLoadOrderByTmp(w_OrderBy)
                Endif
            Endif
        Endif
        If qry.bMultiCompany And Not bNoMultiCompany
            * --- ora deve creare lo statemente per le tabelle definite da altree query
            qry.CreateSubQueryTables()
            * --- cerca la variabile che contiene la lista delle company con cui fare la query
            If Vartype(oParentObject)='O'
                Local i_olderr
                i_olderr=On('ERROR')
                On Error =.T.
                If oParentObject.isVar('w_CompanyList')
                    qry.mLoadCompanyList(oParentObject.GetVar('w_CompanyList'))
                Else
                    If Type('oParentObject.parent.parent.parent.parent')='O'
                        If oParentObject.Parent.Parent.Parent.Parent.isVar('w_CompanyList')
                            qry.mLoadCompanyList(oParentObject.Parent.Parent.Parent.Parent.GetVar('w_CompanyList'))
                        Endif
                    Endif
                Endif
                On Error &i_olderr
            Else
                If This.bFox26 And Type('w_CompanyList')='C'
                    qry.mLoadCompanyList(w_CompanyList)
                Endif
            ENDIF
            *--- Zucchetti Aulla Inizio - CP_COMPANY Alias
	    *---CP_COMPANY Alias
            If Vartype(oParentObject)='O'
                Local i_olderr
                i_olderr=On('ERROR')
                On Error =.T.
                If oParentObject.isVar('w_CompanyAlias')
                    qry.mLoadCompanyAlias(oParentObject.GetVar('w_CompanyAlias'))
                Else
                    If Type('oParentObject.parent.parent.parent.parent')='O'
                        If oParentObject.Parent.Parent.Parent.Parent.isVar('w_CompanyAlias')
                            qry.mLoadCompanyAlias(oParentObject.Parent.Parent.Parent.Parent.GetVar('w_CompanyAlias'))
                        Endif
                    Endif
                Endif
                On Error &i_olderr
            Else
                If This.bFox26 And Type('w_CompanyAlias')='C'
                    qry.mLoadCompanyAlias(w_CompanyAlias)
                Endif
            Endif
            *--- Zucchetti Aulla Fine - CP_COMPANY Alias
        Endif		
        Return
    Proc SerializeTables(xcg,qry)
        Local i,j,o,cDescr,cName,cAlias,nX,nY
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_READ_ONLY_QM),.F.)
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cAlias=xcg.Load('C','')
                cName=xcg.Load('C','')
                nX=xcg.Load('N',0)
                nY=xcg.Load('N',0)
                xcg.Eoc()
                qry.mLoadFile(cDescr,cAlias,cName,0)
            Next
        Endif
        Return
    Proc SerializeFields(xcg,qry)
        Local i,j,o,cDescr,cExp,cAlias,cType,nLen,nDec
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_READ_ONLY_QM),.F.)
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cExp=xcg.Load('C','')
                cAlias=xcg.Load('C','')
                cType=xcg.Load('C','')
                nLen=xcg.Load('N',0)
                nDec=xcg.Load('N',0)
                qry.mLoadField(cExp,cAlias,cType,nLen,nDec, cDescr)
                xcg.Eoc()
            Next
        Endif
        Return
    Proc SerializeRelations(xcg,qry)
        Local i,j,o,cDescr,cExp,cAlias1,cAlias2,cType
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_READ_ONLY_QM),.F.)
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cExp=xcg.Load('C','')
                cType=xcg.Load('C','')
                cAlias1=xcg.Load('C','')
                cAlias2=xcg.Load('C','')
                qry.mLoadJoin(cDescr,cAlias1,cAlias2,cExp,cType)
                xcg.Eoc()
            Next
        Endif
        Return
    Proc SerializeWhere(xcg,qry)
        Local i,j,o,cFld,cNot,cOp,cConst,cConst2,cLOp,cHaving,i_bSubQuery
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_READ_ONLY_QM),.F.)
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cFld=xcg.Load('C','')
                cNot=xcg.Load('C','')
                cOp=xcg.Load('C','')
                cConst=xcg.Load('C','')
                cConst2=cConst
                cLOp=xcg.Load('C','')
                cHaving=xcg.Load('C','')
                xcg.Eoc()
                i_bSubQuery=Inlist(cOp,'in','not in','exists','not exists') Or 'any'$cOp Or 'all'$cOp
                If cOp='like'
                    cConst='('+cConst+')'
                Endif
                If i_bSubQuery
                    If Inlist(cOp,'in','not in') And Left(cConst,1)='('
                        i_bSubQuery=.F.
                    Else
                        cConst='!'+cConst
                        cConst2=cConst
                    Endif
                Endif
                If cHaving='Having'
                    If Empty(cNot)
                        qry.mLoadHaving(cFld+' '+cOp+' '+cConst+' '+cLOp+' ',Iif(Left(cConst2,1)='?',cConst2,.F.),i_bSubQuery)
                    Else
                        qry.mLoadHaving('not('+cFld+' '+cOp+' '+cConst+') '+cLOp+' ',Iif(Left(cConst2,1)='?',cConst2,.F.),i_bSubQuery)
                    Endif
                Else
                    If Empty(cNot)
                        qry.mLoadWhere(cFld+' '+cOp+' '+cConst+' '+cLOp+' ',Iif(Left(cConst2,1)='?' Or i_bSubQuery,cConst2,.F.),i_bSubQuery)
                    Else
                        qry.mLoadWhere('not('+cFld+' '+cOp+' '+cConst+') '+cLOp+' ',Iif(Left(cConst2,1)='?' Or i_bSubQuery,cConst2,.F.),i_bSubQuery)
                    Endif
                Endif
            Next
        Endif
        Return
    Proc SerializeOrderBy(xcg,qry)
        Local i,j,o,cFld,cDescr,cAscDesc
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_READ_ONLY_QM),.F.)
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cFld=xcg.Load('C','')
                cAscDesc=xcg.Load('C','')
                xcg.Eoc()
                qry.mLoadOrder(cFld,cAscDesc)
            Next
        Endif
        Return
    Proc SerializeGroupBy(xcg,qry)
        Local i,j,o,cFld,cDescr
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_READ_ONLY_QM),.F.)
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                cDescr=xcg.Load('C','')
                cFld=xcg.Load('C','')
                xcg.Eoc()
                qry.mLoadGroupBy(cFld)
            Next
        Endif
        Return
    Proc SerializeDistinct(xcg,qry)
        Local N
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_READ_ONLY_QM),.F.)
        Else
            N=xcg.Load('N',0)
            If N<>0
                qry.cDistinct="distinct"
            Endif
        Endif
        Return
    Proc SerializeParam(xcg,qry,oParentObject)
        Local i,j,o,cVar,cDescr,cType,cLen,cDec,bGetObjVar
        Local bGetObjFatherVar
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_READ_ONLY_QM),.F.)
        Else
            bGetObjVar=Vartype(oParentObject)='O' And !Isnull(oParentObject)
            j=xcg.Load('N',0)
            For i=1 To j
                cVar=xcg.Load('C','')
                cDescr=xcg.Load('C','')
                cType=xcg.Load('C','C')
                cLen=xcg.Load('C','10')
                cDec=xcg.Load('C','0')
                cRemove=xcg.Load('C','')
                * --- Zucchetti Aulla Inizio - Gestione parametrica filtri vuoti FazEngine
                If Type('qry.cExcludeFromRemoveParam')='C' And Lower(cVar)$ Lower(qry.cExcludeFromRemoveParam)
                    cRemove=''
                Endif
                * --- Zucchetti Aulla Fine - FazEngine
                xcg.Eoc()
                If This.bFox26
                    If Type(cVar)<>"U"
                        qry.mLoadValuedFilterParam(cVar,cDescr,cType,Val(cLen),Val(cDec),&cVar,cRemove)
                    Else
                        qry.mLoadFilterParam(cVar,cDescr,cType,Val(cLen),Val(cDec),cRemove)
                    Endif
                Else
                    If bGetObjVar And oParentObject.isVar(cVar)
                        qry.mLoadValuedFilterParam(cVar,cDescr,cType,Val(cLen),Val(cDec),oParentObject.GetVar(cVar),cRemove)
                    Else
                        If Type("oParentObject.parent.parent.parent.parent")='O'
                            If oParentObject.Parent.Parent.Parent.Parent.BaseClass='Pageframe'
                                bGetObjFatherVar=Type("oParentObject.parent.parent.parent.parent.parent")='O' And !Isnull("oParentObject.parent.parent.parent.parent.parent")
                                If bGetObjFatherVar And oParentObject.Parent.Parent.Parent.Parent.Parent.isVar(cVar)
                                    qry.mLoadValuedFilterParam(cVar,cDescr,cType,Val(cLen),Val(cDec),oParentObject.Parent.Parent.Parent.Parent.Parent.GetVar(cVar),cRemove)
                                Else
                                    qry.mLoadFilterParam(cVar,cDescr,cType,Val(cLen),Val(cDec),cRemove)
                                Endif
                            Else
                                bGetObjFatherVar=Type("oParentObject.parent.parent.parent.parent")='O' And !Isnull("oParentObject.parent.parent.parent.parent")
                                If bGetObjFatherVar And oParentObject.Parent.Parent.Parent.Parent.isVar(cVar)
                                    qry.mLoadValuedFilterParam(cVar,cDescr,cType,Val(cLen),Val(cDec),oParentObject.Parent.Parent.Parent.Parent.GetVar(cVar),cRemove)
                                Else
                                    qry.mLoadFilterParam(cVar,cDescr,cType,Val(cLen),Val(cDec),cRemove)
                                Endif
                            Endif
                        Else
                            qry.mLoadFilterParam(cVar,cDescr,cType,Val(cLen),Val(cDec),cRemove)
                        Endif
                    Endif
                Endif
            Next
            *IF qry.bOrderByParm
            *  * --- cerca la variabile che aggiunge una condizione di order by dinamica
            *if type('oParentObject')='O'
            *local i_olderr
            *i_olderr=on('ERROR')
            *on error =.t.
            *if oParentObject.IsVar('w_OrderBy')
            *  qry.mLoadOrderByTmp(oParentObject.GetVar('w_OrderBy'))
            *ELSE
            *  IF TYPE('oParentObject.parent.parent.parent.parent')='O'
            *    if oParentObject.parent.parent.parent.parent.IsVar('w_OrderBy')
            *      qry.mLoadOrderByTmp(oParentObject.parent.parent.parent.parent.GetVar('w_OrderBy'))
            *    endif
            *  endif
            *endif
            *on error &i_olderr
            *else
            * if this.bFox26 and type('w_OrderBy')='C'
            *  qry.mLoadOrderByTmp(w_OrderBy)
            *endif
            *ENDIF
            *endif
        Endif
        Return
    Proc SerializePivot(xcg,qry)
        Local i,j,c
        If xcg.bIsStoring
            CP_MSG(cp_Translate(MSG_READ_ONLY_QM),.F.)
        Else
            j=xcg.Load('N',0)
            For i=1 To j
                c=xcg.Load('C','')
                c=xcg.Load('C','')
                qry.mLoadPivotFlds(xcg.Load('C',''))
            Next
            j=xcg.Load('N',0)
            For i=1 To j
                c=xcg.Load('C','')
                c=xcg.Load('C','')
                qry.mLoadPivotData(xcg.Load('C',''))
            Next
            qry.nPivotType=xcg.Load('N',0)
            qry.cPivotListQry=xcg.Load('C','')
        Endif
Enddefine

Define Class cp_pivot As Custom
    cTable=''
    cCursor=''
    nFlds=0
    Dimension cFlds[1]
    nObserved=0
    Dimension cObserved[1]
    cPivotFld=''
    Dimension xTitles[1]
    bMonth=.F.
    bDay=.F.
    bFldNumber=.F.
    bList=.F.
    cList=""
    Proc Clear()
        This.nFlds=0
        This.nObserved=0
        This.cPivotFld=''
        This.cTable=''
        This.cCursor=''
        This.bMonth=.F.
        This.bDay=.F.
    Proc SetTable(cTable)
        This.cTable=cTable
    Proc SetCursor(cCursor)
        This.cCursor=cCursor
    Proc SetMonth()
        This.bMonth=.T.
    Proc SetDay()
        This.bDay=.T.
    Proc SetList(cList)
        This.bList=.T.
        This.cList=cList
    Proc SetFldNumber()
        This.bDay=.F.
        This.bMonth=.F.
        This.bFldNumber=.T.
    Proc AddFld(cFldName)
        This.nFlds=This.nFlds+1
        Dimension This.cFlds[this.nFlds]
        This.cFlds[this.nFlds]=cFldName
        Return
    Proc AddObserved(cFldName)
        This.nObserved=This.nObserved+1
        Dimension This.cObserved[this.nObserved]
        This.cObserved[this.nObserved]=cFldName
        Return
    Proc SetPivotFld(cPivotFld)
        This.cPivotFld=cPivotFld
        Return
    Proc Pivot()
        Local a[1],r[1],N,i,c,p,m,T,v,i_cOldExact
        i_cOldExact=Set('EXACT')
        Set Exact On
        c=This.cPivotFld
        T=This.cTable
        Do Case
            Case This.bMonth Or This.bDay
                m=Iif(This.bMonth,12,31)
                Dimension This.xTitles[m]
                For i=1 To m
                    This.xTitles[i]=i
                Next
                N=Alen(This.xTitles)
            Case This.bList
                v=This.cList
                i=Occurs(',',v)
                Dimension This.xTitles[i+1]
                p=At(',',v)
                i=1
                Do While p<>0
                    This.xTitles[i]=Left(v,p-1)
                    i=i+1
                    v=Substr(v,p+1)
                    p=At(',',v)
                Enddo
                This.xTitles[i]=v
                N=Alen(This.xTitles)
            Otherwise
                Select Distinct &c From (T) Order By 1 Into Array This.xTitles
                If _Tally=0
                    N=0
                Else
                    N=Alen(This.xTitles)
                Endif
        Endcase
        If N*This.nObserved+This.nFlds<=256
            * --- puo' creare il pivot perche' non ci sono troppi campi
            Select (This.cTable)
            Afields(a)
            * --- prepara il cursore per la risposta
            Dimension r[n*this.nObserved+this.nFlds,16]
            For i=1 To N*This.nObserved+This.nFlds
                For j=1 To 16
                    r[i,j]=''
                Next
                r[i,3]=0
                r[i,4]=0
                r[i,5]=.T.
                r[i,6]=.F.
            Next
            m=0
            For i=1 To This.nFlds
                * --- aggiunge i campi "normali" dalla tabella originale
                p=Ascan(a,Upper(This.cFlds[i]))
                m=m+1
                r[m,1]=This.cFlds[i]
                r[m,2]=a(p+1)
                r[m,3]=a(p+2)
                r[m,4]=a(p+3)
            Next
            For i=1 To N
                * --- aggiunge i campi "pivot"
                For j=1 To This.nObserved
                    p=Ascan(a,Upper(This.cObserved[j]))
                    m=m+1
                    r[m,1]=This.cObserved[j]+'_'+Iif(This.bFldNumber,Alltrim(Str(i)),This.ToStr(This.xTitles[i]))
                    * Zucchetti Aulla Inizio
                    r[m,2]=a(p+1) &&'N'
                    r[m,3]=a(p+2) &&12+a(p+3)
                    * Zucchetti Aulla Fine
                    r[m,4]=0+a(p+3)   &&a(p+3)
                Next
            Next
            * --- e' pronto a creare il cursore
            Create Cursor (This.cCursor) From Array r
            * --- ora travasa i dati da dalla tabella originale
            c=''
            For i=1 To This.nFlds
                c=c+Iif(Empty(c),'',' and ')+This.cTable+'.'+This.cFlds[i]+'=='+This.cCursor+'.'+This.cFlds[i]
            Next
            Select (This.cTable)
            Scan
                Select (This.cCursor)
                If Empty(c)
                    Go Top
                    If Eof()
                        Append Blank
                    Endif
                Else
                    Locate For &c
                    If !Found()
                        Append Blank
                        For i=1 To This.nFlds
                            m=This.cFlds[i]
                            Replace &m With &T..&m
                        Next
                    Endif
                Endif
                m=This.cTable+'.'+This.cPivotFld
                p=Ascan(This.xTitles,&m)
                If p=0 And This.bList
                    p=Ascan(This.xTitles,cp_ToStr(&m))
                Endif
                If p<>0
                    Select (This.cTable)
                    For i=1 To This.nObserved
                        Select (This.cTable)
                        m=This.cObserved[i]
                        v=&m
                        Select (This.cCursor)
                        N=m+'_'+Iif(This.bFldNumber,Alltrim(Str(p)),This.ToStr(This.xTitles[p]))
                        Replace &N With v
                    Next
                Endif
            Endscan
        Endif
        Set Exact &i_cOldExact
        Return
    Func ToStr(xValue)
        Local T
        T=Type('xValue')
        Do Case
            Case T='C'
                Return(xValue)
            Case T='N' Or T='Y'
                Return(Strtran(Alltrim(Str(xValue)),'.','_'))
            Case T='D'
                Return(Dtos(xValue))
            Case T='T'
                Return(Ttoc(xValue,1))
        Endcase
Enddefine

*--- ParamParsing(@aValues, cParam, cVir)
*--- Separa i parametri contenuti nella stringa cParam delimitati da cVir, restituisce in
*--- aValues (passato per riferimento) i parametri
*--- Ritorna il numero di parametri trovati
Function ParamParsing
    Parameters aValues, cParam, cVir
    *--- Inizializzo le variabili
    nCnt = 0
    cTmpParam = cParam+','
    cNewParam = ''
    nItems = 0
    nLen = Len(cTmpParam)
    cStr = ''
    nSkip = 0
    *--- Ciclo fino alla fine della stringa
    Do While nCnt < nLen
        nCnt = nCnt + 1
        cChar = Substr(cTmpParam, nCnt, 1)
        bAdd = .F.
        Do Case
            Case cChar == '('
                If Empty(cStr)
                    nSkip = nSkip + 1
                Endif
            Case cChar == ')'
                If Empty(cStr)
                    nSkip = nSkip - 1
                Endif
            Case cChar == '"'
                If Empty(cStr)
                    cStr = '"'
                Else
                    If cStr = '"'
                        *--- Fine Stringa
                        cStr = ''
                    Endif
                Endif
            Case cChar == "'"
                If Empty(cStr)
                    cStr = "'"
                Else
                    If cStr = "'"
                        cStr = ''
                    Endif
                Endif
            Case cChar == cVir
                If Empty(cStr)
                    bAdd = (nSkip=0)
                Endif
        Endcase
        If bAdd
            nItems = nItems + 1
            Dimension aValues[nItems]
            aValues[nItems] = cNewParam
            cNewParam = ''
        Else
            cNewParam = cNewParam + cChar
        Endif
    Enddo
    Return nItems
Endproc

Define Class cp_AutoCompManager As Custom
    Add Object oData As Collection

    Procedure AddItem(cKey, eValue, cText)
        Local l_Item
        If Empty(m.cText) Or Empty(m.eValue)
            m.cText = "<Empty>"
        Endif
        If This.oData.GetKey(m.cKey)<>0
            l_Item=This.oData.Item(m.cKey)
            If l_Item.GetKey(Alltrim(m.cText))=0
                l_Item.Add(m.eValue, Alltrim(m.cText),1)
            Else
                *--- Porto al primo posto
                If l_Item.Count>1
                    l_Item.Remove(Alltrim(m.cText))
                    l_Item.Add(m.eValue, Alltrim(m.cText),1)
                Endif
            Endif
        Else
            l_Item = Createobject("Collection")
            l_Item.Add(m.eValue, Alltrim(m.cText))
            This.oData.Add(m.l_Item, m.cKey)
        Endif
    Endproc
    Function GetValue(cKey, cText, cType, nLen, nDec)
        Local l_Item, l_eValue
        If This.oData.GetKey(m.cKey)<>0
            l_Item=This.oData.Item(m.cKey)
            If l_Item.GetKey(Alltrim(m.cText))<>0
                *--- Porto al primo posto
                l_eValue = l_Item.Item(Alltrim(m.cText))
                If l_Item.Count>1
                    l_Item.Remove(Alltrim(m.cText))
                    l_Item.Add(m.l_eValue, Alltrim(m.cText),1)
                Endif
                Return l_eValue
            Endif
        Endif
        Return cp_NullValue(m.cType, m.nLen, m.nDec)
    Endproc
    Function GetDefault(cKey, cType, nLen, nDec)
        Local l_Item
        If This.oData.GetKey(m.cKey)<>0
            l_Item=This.oData.Item(m.cKey)
            If l_Item.Count>0
                Return l_Item.Item(1)
            Endif
        Endif
        Return cp_NullValue(m.cType, m.nLen, m.nDec)
    Endproc
    Procedure FillCombo(cKey, oCombo)
        Local l_Item, l_nCnt
        If This.oData.GetKey(m.cKey)<>0
            l_Item=This.oData.Item(m.cKey)
            m.oCombo.Clear()
            For l_nCnt=1 To l_Item.Count
                m.oCombo.AddItem(l_Item.GetKey(m.l_nCnt))
            Endfor
        Endif
    Endproc
Enddefine

Define Class cp_textcombo As Container

    Width = 272
    Height = 23
    BackStyle = 0
    BorderWidth = 0
    Name = "cp_textcombo"

    cKey = Sys(2015)
    cType = 'C'
    nLen = 10
    nDec = 0
    Value=""
    InputMask=""
    Format=""
    FontBold=.F.

    Add Object otext As TextBox With ;
        Anchor = 15, ;
        Height = 23, ;
        Left = 0, ;
        Top = 0, ;
        Width = 252, ;
        Name = "oText"

    Add Object oCombo As ComboBox With ;
        Anchor = 13, ;
        Height = 23, ;
        Left = 251, ;
        Top = 0, ;
        Width = 21, ;
        Name = "oCombo"

    Procedure Init(cKey, cType, nLen, nDec)
        DoDefault()
        This.cKey = Evl(m.cKey, Sys(2015))
        This.cType = Evl(m.cType, "C")
        This.nLen = Evl(m.nLen, 15)
        This.nDec = Evl(m.nDec, 0)
        This.InitAutoComp()
        This.Value = oAutoCompParamQry.GetDefault(This.cKey, This.cType, This.nLen, This.nDec)
    Endproc

    Procedure InitAutoComp()
        If Vartype(m.oAutoCompParamQry)='U' Or Isnull(m.oAutoCompParamQry)
            Public oAutoCompParamQry
            oAutoCompParamQry = Createobject("cp_AutoCompManager")
        Endif
    Endproc

    Procedure Value_Assign(xValue)
        This.Value = m.xValue
        This.otext.Value = m.xValue
    Endproc

    Procedure Value_Access()
        Return This.otext.Value
    Endproc

    Procedure InputMask_Assign(xValue)
        This.InputMask = m.xValue
        This.otext.InputMask = m.xValue
    Endproc

    Procedure Format_Assign(xValue)
        This.Format = m.xValue
        This.otext.Format = m.xValue
    Endproc

    Procedure FontBold_Assign(xValue)
        This.FontBold = m.xValue
        This.otext.FontBold = m.xValue
    Endproc

    Procedure otext.Valid()
        This.Parent.InitAutoComp()
        *--- Necessario per le date e datetime
        This.Value = This.Value
        m.oAutoCompParamQry.AddItem(This.Parent.cKey, This.Value, This.Text)
    Endproc

    Procedure oCombo.DropDown
        This.Parent.InitAutoComp()
        m.oAutoCompParamQry.FillCombo(This.Parent.cKey, This)
    Endproc

    Procedure oCombo.InteractiveChange
        This.Parent.InitAutoComp()
        This.Parent.otext.Value = m.oAutoCompParamQry.GetValue(This.Parent.cKey, This.Value, This.Parent.cType, This.Parent.nLen, This.Parent.nDec)
    Endproc
Enddefine
