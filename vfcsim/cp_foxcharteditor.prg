* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_foxcharteditor                                               *
*              FoxChart editor                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-07-27                                                      *
* Last revis.: 2010-05-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_foxcharteditor",oParentObject))

* --- Class definition
define class tcp_foxcharteditor as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 629
  Height = 598
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-05-26"
  HelpContextID=99652182
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cpfchart_IDX = 0
  cPrg = "cp_foxcharteditor"
  cComment = "FoxChart editor"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FDSERIAL = space(3)
  o_FDSERIAL = space(3)
  w_CHANGEONFLY = .F.
  w_ISVQR = .F.
  w_ISCURS = .F.

  * --- Children pointers
  cp_FoxChartProperties = .NULL.
  w_oChart = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_foxcharteditor
  * --- Disabilito la possibilitÓ di chiudere la maschera con la crocetta
  * --- in alto a destra
  closable=.f.
  autocenter=.t.
  AlwaysOnTop=.t.
  * --- Disabilito la Toolbar all'attivarsi della maschera
  Proc SetStatus()
   DoDefault()
   oCpToolBar.Enable(.f.) 
  EndProc
  
  * --- Disabilito la Toolbar all'attivarsi della maschera
  * --- inoltre valorizzo i_curform facendola puntare alla toolbar
  * --- che ha lanciato la maschera
  Proc Activate()
    Dodefault()
    oCpToolBar.Enable(.f.) 
    *--- tasto destro non funziona, lo devo disabilitare
    ON KEY LABEL RIGHTMOUSE wait window ""  
  EndProc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to cp_FoxChartProperties additive
    with this
      .Pages(1).addobject("oPag","tcp_foxcharteditorPag1","cp_foxcharteditor",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCHANGEONFLY_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure cp_FoxChartProperties
    * --- Area Manuale = Init Page Frame
    * --- cp_foxcharteditor
     * --- Translate interface...
     this.parent.cComment=cp_Translate(MSG_FOXCHARTS) 
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oChart = this.oPgFrm.Pages(1).oPag.oChart
    DoDefault()
    proc Destroy()
      this.w_oChart = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='cpfchart'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.cp_FoxChartProperties = CREATEOBJECT('stdDynamicChild',this,'cp_FoxChartProperties',this.oPgFrm.Page1.oPag.oLinkPC_1_3)
    this.cp_FoxChartProperties.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.cp_FoxChartProperties)
      this.cp_FoxChartProperties.DestroyChildrenChain()
      this.cp_FoxChartProperties=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.cp_FoxChartProperties.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.cp_FoxChartProperties.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.cp_FoxChartProperties.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.cp_FoxChartProperties.SetKey(;
            .w_FDSERIAL,"code";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .cp_FoxChartProperties.ChangeRow(this.cRowID+'      1',1;
             ,.w_FDSERIAL,"code";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.cp_FoxChartProperties)
        i_f=.cp_FoxChartProperties.BuildFilter()
        if !(i_f==.cp_FoxChartProperties.cQueryFilter)
          i_fnidx=.cp_FoxChartProperties.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.cp_FoxChartProperties.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.cp_FoxChartProperties.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.cp_FoxChartProperties.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.cp_FoxChartProperties.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_cp_FoxChartProperties(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.cp_FoxChartProperties.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_cp_FoxChartProperties(i_ask)
    if this.cp_FoxChartProperties.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.cp_FoxChartProperties.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FDSERIAL=space(3)
      .w_CHANGEONFLY=.f.
      .w_ISVQR=.f.
      .w_ISCURS=.f.
      .oPgFrm.Page1.oPag.oChart.Calculate()
      .cp_FoxChartProperties.NewDocument()
      .cp_FoxChartProperties.ChangeRow('1',1,.w_FDSERIAL,"code")
      if not(.cp_FoxChartProperties.bLoaded)
        .cp_FoxChartProperties.SetKey(.w_FDSERIAL,"code")
      endif
          .DoRTCalc(1,1,.f.)
        .w_CHANGEONFLY = .T.
    endwith
    this.DoRTCalc(3,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_foxcharteditor
     
      endproc
      
          Proc ecpQuit1()
    	    * --- Move without activating controls
    	    this.cFunction='Filter'
    	    this.__dummy__.enabled=.t.
    	    this.__dummy__.Setfocus()
    	    * ---
    	    this.Hide()
    	    this.NotifyEvent("Edit Aborted")
    	    this.NotifyEvent("Done")
    	    this.Release()
    	  EndProc
    
      Proc ecpQuit()
        i_curform.oParentObject.ecpquit() 
    	  EndProc
    
      Proc ecpSave()
        Return
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.cp_FoxChartProperties.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.cp_FoxChartProperties.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oChart.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_FDSERIAL<>.o_FDSERIAL
          .Save_cp_FoxChartProperties(.t.)
          .cp_FoxChartProperties.NewDocument()
          .cp_FoxChartProperties.ChangeRow('1',1,.w_FDSERIAL,"code")
          if not(.cp_FoxChartProperties.bLoaded)
            .cp_FoxChartProperties.SetKey(.w_FDSERIAL,"code")
          endif
        endif
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oChart.Calculate()
    endwith
  return

  proc Calculate_BYKEQGZGCK()
    with this
          * --- AFTERCHART
          cp_setfoxchartproperties(this;
              ,"SETGRIDDATA";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oChart.Event(cEvent)
        if lower(cEvent)==lower("w_ochart afterchart")
          .Calculate_BYKEQGZGCK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- cp_foxcharteditor
            IF lower(cEvent)='getvaluefromchart'
               this.cp_foxchartproperties.NotifyEvent("GetValueFromChart")
            EndIf     
            IF lower(cEvent)='w_ochart config loaded'
               this.cp_foxchartproperties.NotifyEvent("CONFIGLOADED")
            EndIf
            IF lower(cEvent)='redrawfoxcharts'
               this.cp_foxchartproperties.NotifyEvent("REDRAWFOXCHARTS")
            EndIf
            IF lower(cEvent)='setcursordata'
               this.cp_foxchartproperties.NotifyEvent("SETCURSORDATA")
            EndIf
            IF lower(cEvent)='gettfieldsfromchart'
               this.cp_foxchartproperties.NotifyEvent("GETTFIELDSFROMCHART")
            EndIf
            IF lower(cEvent)='getlegends'
               this.cp_foxchartproperties.NotifyEvent("GETLEGENDS")
            EndIf
            IF lower(cEvent)='runbyparam'
               this.cp_foxchartproperties.NotifyEvent("RUNBYPARAM")
            EndIf                
    
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCHANGEONFLY_1_6.RadioValue()==this.w_CHANGEONFLY)
      this.oPgFrm.Page1.oPag.oCHANGEONFLY_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .cp_FoxChartProperties.CheckForm()
      if i_bres
        i_bres=  .cp_FoxChartProperties.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FDSERIAL = this.w_FDSERIAL
    * --- cp_FoxChartProperties : Depends On
    this.cp_FoxChartProperties.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tcp_foxcharteditorPag1 as StdContainer
  Width  = 625
  height = 598
  stdWidth  = 625
  stdheight = 598
  resizeXpos=480
  resizeYpos=224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oChart as cp_FoxCharts with uid="ZPUJQCCDHG",left=5, top=1, width=616,height=305,;
    caption='Object',;
   bGlobalFont=.t.,;
    cLogoFileName="",cSource="",cFileCfg="",Anchor=0,bDisplayLogo=.f.,bQueryOnLoad=.t.,cMenuFile="",bReadOnly=.t.,;
    cEvent = "Redraw",;
    nPag=1;
    , HelpContextID = 5175482


  add object oLinkPC_1_3 as stdDynamicChildContainer with uid="MPPJVAAMEA",left=4, top=337, width=612, height=255, bOnScreen=.t.;



  add object oBtn_1_4 as StdButton with uid="XEHWPCHNML",left=10, top=307, width=77,height=25,;
    caption="Redraw", nPag=1;
    , HelpContextID = 242956870;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .cp_foxchartproperties.cp_foxchartfields.NotifyEvent("REDRAWFOXCHARTS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="PZEOMMSRXV",left=91, top=307, width=77,height=25,;
    caption="Update", nPag=1;
    , HelpContextID = 7073094;
  , bGlobalFont=.t.

    proc oBtn_1_5.Click()
      with this.Parent.oContained
        .w_oChart.oChart.DrawChart()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCHANGEONFLY_1_6 as StdCheck with uid="EAPFEMDGJM",rtseq=2,rtrep=.f.,left=175, top=311, caption="Apply change on the fly",;
    HelpContextID = 227252619,;
    cFormVar="w_CHANGEONFLY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHANGEONFLY_1_6.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oCHANGEONFLY_1_6.GetRadio()
    this.Parent.oContained.w_CHANGEONFLY = this.RadioValue()
    return .t.
  endfunc

  func oCHANGEONFLY_1_6.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHANGEONFLY==.T.,1,;
      0)
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_foxcharteditor','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
