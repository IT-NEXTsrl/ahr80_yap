* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_MENU
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 02/03/93
* Aggiornato il : 20/03/97
* Translated    : 01/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Definisce il menu' principale
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam bStdMenu
Private maxitem, padname, m ,k ,cOpz ,i_evd ,i_pos,i_err,i_olderr
Public i_cMenuLoaded

*--- Zucchetti Aulla Inizio - Window Menu
If g_userscheduler
    On Key Label CTRL+T
    On Key Label CTRL+m
Else
    On Key Label CTRL+T cp_tmenu()
    On Key Label CTRL+m ShowMenu()
Endif
*--- Zucchetti Aulla Fine - Window Menu

* --- Contiene il nome del cursore che contiene il men� in alto..
If Vartype(i_CUR_MENU)='U'
    Public i_CUR_MENU
    i_CUR_MENU=''
Endif

* --- Separatore voci di men� (�)
If Vartype(g_MENUSEP)='U'
    Public g_MENUSEP
    g_MENUSEP=Chr(172)
Endif

Local F, bOld_menu
bOld_menu=bStdMenu
If !bOld_menu
    F=cp_FindDefault('vmn')
    If Empty(F)
        * --- Carico Plan.Vmn
        F=cp_FindNamedDefaultFile(i_CpDic,'vmn')
        If Empty(F)
            * --- Non trovo nessun VMN da caricare ne PLAN e DEFAULT...
            bOld_menu=.T.
        Endif
    Endif
    *--- Zucchetti Aulla inizio - Monitor Framework
    If i_MonitorFramework
        * potenziamento framework
        * evita di ripetere due volte la stessa ricerca di un file
        *---
        Private framework_file_menu
        If At("|",i_pathFindFile)==0
            framework_file_menu = Alltrim(i_pathFindFile)
        Else
            If At(Upper(MSG_FILEEXIST),Upper(i_pathFindFile))!=0
                framework_file_menu = Strextract(Alltrim(i_pathFindFile),"|")
            Else
                framework_file_menu = ""
            Endif
        Endif
    Endif
    *--- Zucchetti Aulla Fine - Monitor Framework
Endif
If !bOld_menu
    i_cMenuLoaded=F
    * --- Contiene il nome del cursore che contiene il men� in alto..
    If Used(i_CUR_MENU)
        Select &i_CUR_MENU
        Use
    Endif
    i_CUR_MENU=Sys(2015)
	
    Do Case
	    Case Type('g_LoadMenuMode')='N' And (g_LoadMenuMode==1 Or (g_LoadMenuMode==2 And cp_UpdatedMenuCheck()))
	        Do Vmn_To_Cur In vu_exec With ForceExt(F,'.VMN'), i_CUR_MENU
			Do merge_all In vu_exec With i_CUR_MENU , i_cmodules
			If Type("i_bFox26")<>'L'
			    Do CaricaMenu In vu_exec With i_CUR_MENU,.F.
			Endif
	        * --- Salvataggio DBF
	        If g_LoadMenuMode==2
	            cp_SaveMenu(g_MenuMemName, i_CUR_MENU)
	        Endif
	    Case Type('g_LoadMenuMode')='N' And g_LoadMenuMode==2
	        * --- Caricamento DBF
	        cp_LoadMenu(g_MenuMemName, i_CUR_MENU)
	        If Type("i_bFox26")<>'L'
	            Do CaricaMenu In vu_exec With i_CUR_MENU,.T.
	        Endif
    	Case Type('g_LoadMenuMode')<>'N' Or g_LoadMenuMode==0
			*--- Zucchetti Aulla Inizio - Memorizzazione dei menu su db
			Public g_aMenuAsMem
			cp_CreateMenuAsMem(@g_aMenuAsMem)
			cp_vmnmem(F)
			*--- Zucchetti Aulla Fine - Memorizzazione dei menu su db
    Endcase
Else
    If Type("i_bFox26")<>'L'
        If i_VisualTheme<>-1
            Use In Select(i_CUR_MENU)
            i_CUR_MENU=Sys(2015)
            cp__Menu2Std(i_CUR_MENU)
            Do CaricaMenu In vu_exec With i_CUR_MENU,.F.
        Else
            * --- Gestione menu da cp_menu.dbf (old)
            i_cMenuLoaded=''
            If Not(Used('cp__menu'))
                i_olderr=On('error')
                i_err=.F.
                On Error i_err=.T.
                Use cp__menu In 0 Shared
                On Error &i_olderr
            Endif

            Select cp__menu
            ResetMenu()
            *set sysmenu to
            Go Top
            maxitem = tot_opt
            For i=1 To maxitem
                Skip
                padname = 'pad_1_'+Ltrim(Str(i,3,0))
                m = '_msysmenu'
                cOpz=cp_Translate(opt_name)
                i_pos = At("&",cOpz)
                i_evd = Iif(i_pos<>0,Subs(cOpz,i_pos+1,1),Left(cOpz,1))
                k = 'ALT+'+i_evd
                If _Mac
                    cOpz=Iif(i_pos<>0,Left(cOpz,i_pos-1)+Trim(Right(cOpz,Len(cOpz)-i_pos)),Alltrim(cOpz))
                Else
                    cOpz=Iif(i_pos<>0,Left(cOpz,i_pos-1)+'\<'+Trim(Right(cOpz,Len(cOpz)-i_pos)),'\<'+Alltrim(cOpz))
                Endif
                If action<>9
                    Define Pad (padname) Of &m Prompt cOpz Key &k
                Else
                    Define Pad (padname) Of &m Prompt cOpz Key &k Skip
                Endif
                Do Case
                    Case action=1
                        subname = Strtran(Trim(opt_proc),'#',' with ')
                        If "'"$subname
                            On Selection Pad (padname) Of &m Do mhit In CP_MENU With "&subname"
                        Else
                            On Selection Pad (padname) Of &m Do mhit In CP_MENU With '&subname'
                        Endif
                    Case action=2 .Or. action=3
                        subname = menu_code(opt_proc)
                        On Pad (padname) Of &m Activate Popup (subname)
                Endcase
            Next
            * --- ora seguono tutti gli altri menu!
            Do While .Not. Eof()
                Skip
                Do create_sub
            Enddo
            * ---
            Release Pad _Msm_edit Of _Msysmenu
            Release Pad _mview Of _Msysmenu
            Set Sysmenu Automatic
        Endif &&i_VisualTheme<>-1
    Endif
Endif

**Rilascia il menu visualizzato con il CTRL-T in modo che al cambio azienda venga ricreato
If Vartype(OCpToolMenu)<>'U'
	If Vartype(OCpToolMenu)='O' and (OCpToolMenu.Visible)
		OCpToolMenu.Visible=.F.
		oCpToolMenu.release()
		Release OCpToolMenu
	Else
		Release OCpToolMenu
	Endif
ENDIF
IF i_bShowToolMenu
   cp_tmenu()
ENDIF

*--- Apertura Gadget
IF Type("oGadgetManager") = 'O' And !IsNull(oGadgetManager)
	oGadgetManager._Release(.T.)
	RELEASE oGadgetManager
EndIF
IF Type("i_bGadgetStartup")='L' AND i_bGadgetStartup
	i_themesmanager.LockVfp(.F., .T.)
	cp_Gadget()
EndIf

If Vartype(i_oDeskmenu)="O" And i_cDeskMenu="O"
    * --- Lo lancio due volte, la prima crea l'oggetto la seconda carica il menu
    Do cp_NavBar
    i_oDeskmenu.Visible=.T.
Endif
Return

*--- Converte il dbf cp__menu nel cursore std
Procedure cp__Menu2Std(pCursor, pLvlKey, pSubMenu)
    If !Used(pCursor)
        *--- creo il cursore del menu
        Do cp_Create_Cur_Menu In vu_exec With m.pCursor
    Endif
    Local l_LvlKey, l_count, l_pLvlKey
    l_pLvlKey = Iif(Vartype(m.pLvlKey)='C', m.pLvlKey, '')
    l_count = 0
    l_LvlKey = '000'
    If Not(Used('cp__menu'))
        i_olderr=On('error')
        i_err=.F.
        On Error i_err=.T.
        Use cp__menu In 0 Shared
        On Error &i_olderr
    Endif
    Select cp__menu
    If Vartype(m.pSubMenu)='C' And !Empty(m.pSubMenu)
        Locate For Alltrim(opt_name) == Alltrim(m.pSubMenu)
    Else
        Go Top
    Endif
    Local l_numItem, l_i, l_opt_name, l_opt_proc, l_Action, l_tot_opt, l_oldRecNo
    l_numItem = tot_opt + 1
    For l_i = 1 To l_numItem
        l_count = l_count + 1
        l_LvlKey = Iif(!Empty(m.l_pLvlKey),m.l_pLvlKey+'.', '')+ Right('000' + Alltrim(Str(l_count)), 3)
        l_opt_name = opt_name
        l_opt_proc = opt_proc
        l_Action = action
        l_tot_opt = tot_opt
        If l_Action <> 0
            Insert Into (m.pCursor) (VOCEMENU, LVLKEY, NAMEPROC, Directory, elementi, Enabled) Values (l_opt_name, l_LvlKey, l_opt_proc, l_Action, l_tot_opt, l_Action<>9)
        Else
            If Recno()=1
                Insert Into (m.pCursor) (VOCEMENU, LVLKEY, NAMEPROC, Directory) Values (l_opt_name, '001', l_opt_proc, l_Action)
                l_pLvlKey = '001'
            Endif
            l_count = 0
        Endif
        If action=2
            l_oldRecNo = Recno()
            cp__Menu2Std(m.pCursor, l_LvlKey, opt_proc)
            Go (l_oldRecNo)
        Endif
        If !Eof()
            Skip
        Endif
    Next
Endproc

* --------------------------------
Procedure create_sub

    Private curmenu, maxitem, padname
    If opt_name="Menu' Utility"
        CreateMenuUtility()
        Return
    Endif
    curmenu = 'sub_'+Ltrim(Str(Recno(),5,0))
    maxitem = tot_opt
    Define Popup (curmenu) In Screen
    For i=1 To maxitem
        Skip
        m = '_msysmenu'
        cOpz=cp_Translate(opt_name)
        i_pos = At("&",cOpz)
        If _Mac
            cOpz=Iif(i_pos<>0,Left(cOpz,i_pos-1)+Trim(Right(cOpz,Len(cOpz)-i_pos)),Alltrim(cOpz))
        Else
            cOpz=Iif(i_pos<>0,Left(cOpz,i_pos-1)+'\<'+Trim(Right(cOpz,Len(cOpz)-i_pos)),'\<'+Alltrim(cOpz))
        Endif
        If action<>9
            Define Bar i Of (curmenu) Prompt '   '+cOpz+'   '
        Else
            If Left(Alltrim(opt_name),5)="�����"
                Define Bar i Of (curmenu) Prompt "\-"
            Else
                Define Bar i Of (curmenu) Prompt '   '+cOpz+'   ' Skip
            Endif
        Endif
        Do Case
            Case action=1
                subname = Strtran(Trim(opt_proc),'#',' with ')
                If .Not. Empty(subname)
                    If "'"$subname
                        On Selection Bar (i) Of (curmenu) Do mhit In CP_MENU With "&subname"
                    Else
                        On Selection Bar (i) Of (curmenu) Do mhit In CP_MENU With '&subname'
                    Endif
                Endif
            Case action=2 .Or. action=3
                subname = menu_code(opt_proc)
                On Bar (i) Of (curmenu) Activate Popup (subname)
        Endcase
    Next
    Return

Proc CreateMenuUtility(bNoDBF)
    Local i
    If bNoDBF
        Define Pad utility Of _Msysmenu Prompt "Utility"
        On Pad utility Of _Msysmenu Activate Popup sub_utility
        curmenu='sub_utility'
    Else
        curmenu = 'sub_'+Ltrim(Str(Recno(),5,0))
        maxitem = tot_opt
        For i=1 To maxitem
            Skip
        Next
    Endif
    Define Popup (curmenu) In Screen
    * ---
    i=1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_DATABASE_ADMIN)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "cp_managedb"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_QUERY_PAINTER_BUTTON)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "vq_build"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_DIALOG_WINDOW_PAINTER_BUTTON)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "vm_build"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_ZOOM_PAINTER)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "vz_build"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_MENU_PAINTER)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "vu_build"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_USER_ADMIN_BUTTON)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "cp_MangageUsrGrp"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_PROC_SECURITY_BUTTON)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "cp_ProgAuth"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_PROGRESSIVE_TABLE_ADMIN)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "cp_chpro"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_LANGUAGES)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "cplangs"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_VIEW_ACCESS_FILTER_RULE)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "cp_recsec"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_VIEW_ACCESS_FILTER_RULE)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "cp_recseczoom"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_SETTING_GUI)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "cp_setupgui"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_CONNECT_REPORT_TO_PRINTER_BUTTON)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "cpusrrep"
    i=i+1
    Define Bar i Of (curmenu) Prompt '   '+cp_Translate(MSG_EXIT_BUTTON)+'   '
    On Selection Bar i Of (curmenu) Do mhit In CP_MENU With "quitting"
    * ---
    Return


    * -------------------------
Function menu_code
    Parameter Name
    Private N
    N = Recno()
    Locate For opt_name=Name
    r = 'sub_'+Ltrim(Str(Recno(),5,0))
    Goto N
    Return r

Proc mhit
    Lparam act, cprompt
    If act='quitting'
        Clear Events
    Else
        *--- Zucchetti Aulla Inizio - Window Menu
        *--- Aggiungo la voce nel menu recenti
        If (i_bRecentMenu_navbar or i_bRecentMenu_toolbar or i_bRecentMenu_windows) And Type('cprompt') = 'C'
            gsut_bmr('A', cprompt, act)
        Endif
        *--- Zucchetti Aulla Fine - Window Menu
        *  Voce di men� che inizia con g_OMENU ( allora eseguo un'azione )
        *  La chiamata deve essere del tipo g_OMENU('oParentObject', 'ecpedit') o
        *  g_oMenu('nomebottone', 'Click')
        If Upper(Left(act,8)) = 'G_OMENU('
            PosVirg = Rat(',',act)
            * Estraggo l'oggetto
            Par1 = Substr(act,9,PosVirg - 9)
            * Estraggo l'azione da eseguire
            Par2 = Substr(act,PosVirg+1,Len(act)-(PosVirg+1))
            g_Omenu.doObjAction(Par1,Par2)
        Else
            Do &act
        Endif
    Endif
Endproc

Procedure ResetMenu()
    * Problema comb.tasti Taglia/Copia/incolla disabilitati
    *SET SYSMENU to
    * --- Zucchetti Aulla Inizio - Abilito Window Menu
    If i_bWindowMenu
        Set Sysmenu To _Msm_edit, _Mwindow
        Define Pad _Mwindow Of _Msysmenu Prompt cp_Translate(MSG_WINDOW)  && Modifico il nome della voce di menu
    Else
        ***** Zucchetti Aulla Fine - Abilito Window Menu
        Set Sysmenu To _Msm_edit
        * --- Zucchetti Aulla Inizio - Abilito Window Menu
    Endif
    ***** Zucchetti Aulla Fine - Abilito Window Menu
    Return
    *set sysmenu to _msm_edit,_mview
    *release pad _msm_edit of _msysmenu
    *release pad _mview of _msysmenu
    *return
    *
    *set sysmenu to _msm_systm,_msm_edit,_msm_view
    *release pad _msm_edit of _msysmenu
    *release pad _msm_systm of _msysmenu
    *release pad _msm_view of _msysmenu
    *return
    *
    *release pad _mview of _msysmenu
    *release pad _msm_edit of _msysmenu
    *release pad _msm_systm of _msysmenu
    *release pad _medit of _msm_edit
    *release pad _med_undo of _msm_edit
    *release pad _med_redo of _msm_edit
    *release pad _med_sp100 of _msm_edit
    *release pad _med_pstlk of _msm_edit
    *release pad _med_clear of _msm_edit
    *release pad _med_sp200 of _msm_edit
    *release pad _med_insob of _msm_edit
    *release pad _med_obj of _msm_edit
    *release pad _med_link of _msm_edit
    *release pad _med_cvtst of _msm_edit
    *release pad _med_sp300 of _msm_edit
    *release pad _med_slcta of _msm_edit
    *release pad _med_sp400 of _msm_edit
    *release pad _med_goto of _msm_edit
    *release pad _med_finda of _msm_edit
    *release pad _med_repl of _msm_edit
    *release pad _med_repla of _msm_edit
    *release pad _med_sp500 of _msm_edit
    *release pad _med_pref  of _msm_edit
    *release pad _med_find  of _msm_edit
    *release pad _mst_help  of _msm_systm
    *release pad _mst_hpsch of _msm_systm
    *release pad _mst_hphow of _msm_systm
    *release pad _mst_sp100 of _msm_systm
    *release pad _mst_about of _msm_systm
    *release pad _mst_sp200 of _msm_systm
    *release pad _mst_filer of _msm_systm
Endproc

Func cp_UpdatedMenuCheck()
    Local bReloadMenu,l_aMenuAsMem
    * --- Costruisco MenuAsMem
    cp_CreateMenuAsMem(@l_aMenuAsMem)
    * --- Se non � presente il menu devo ricaricarlo
    bReloadMenu = Not cp_MenuCheck(g_MenuMemName)
    * --- Carico MenuAsMem gi� presente
    If cp_ReadMenuAsMem(g_MenuMemName) And Not bReloadMenu
        If  Alen(l_aMenuAsMem, 1) <> Alen(g_aMenuAsMem, 1)
            bReloadMenu = .T.
        Else
            For nCount = 1 To Alen(l_aMenuAsMem, 1)
                cMenuToFind = l_aMenuAsMem(nCount, 1)
                #If Version(5)>=900
                    nPos = Ascan(g_aMenuAsMem, cMenuToFind, -1, -1, 1, 8)
                #Else
                    nPos = Ascan(g_aMenuAsMem, cMenuToFind)
                    * --- Devo calcolare il numero di riga, ed il match �
                    * --- possibile solo sulla prima colonna per cui
                    nPos = Int((nPos+1) / 2	)
                #Endif
                If nPos = 0 Or g_aMenuAsMem(nPos, 2) <> l_aMenuAsMem(nCount, 2)
                    bReloadMenu = .T.
                    Exit
                Endif
            Endfor
        Endif
    Else
        bReloadMenu = .T.
    Endif
    * ---
    If bReloadMenu
        Release g_aMenuAsMem
        Public g_aMenuAsMem
        Dimension g_aMenuAsMem(Alen(l_aMenuAsMem,1),2)
        Acopy(l_aMenuAsMem, g_aMenuAsMem)
        cp_SaveMenuAsMem(g_MenuMemName)
    Endif
    Return bReloadMenu

Function cp_MenuCheck(i_MenuMemName)
    Return cp_fileexist(i_MenuMemName + ".dbf",.T.)

Func cp_GetMenuDate(i_cMenu)
    private i_f,i_d,oVBS,i_errsav,i_err
    oVBS=Createobject("Scripting.FileSystemObject")
    i_d='?'
    i_errsav=On('ERROR')
    i_err=.F.
    On Error i_err=.T.	
    i_f=oVBS.OpenTextFile(cp_GetStdFile(i_cMenu,'vmn')+'.vmn', 1)
	On Error &i_errsav
	If i_err
		i_f=fopen(cp_GetStdFile(i_cMenu,'vmn')+'.vmn')
		if i_f<>-1
		  i_s=fgets(i_f)
		  if AT("* --- CodePainter Revolution Menu",i_s)<>0
			FOR i_i = 1 TO 24
				i_d=fgets(i_f)
			ENDFOR
			i_d=substr(i_d,2,len(i_d)-2)
		  endif
		  fclose(i_f)
		endif
	Else
		If i_f.ReadLine()="* --- CodePainter Revolution Menu"
			For i_i = 1 To 23
				i_f.SkipLine()
			Endfor
			i_d=i_f.ReadLine()
			i_d=Substr(i_d,2,Len(i_d)-2)
		Endif
		i_f.Close()
	Endif
    Return(i_d)

Function cp_CreateMenuAsMem(i_MenuMem ,bNoCustom)
    Local cMenuFile, nCount, i_p, i_m, MenuMemLength, n_Ext, cPathTemp
    &&nNumMenu = OCCURS(',',cModules) + 3
    n_Ext='vmn'
    MenuMemLength = 1
    Dimension i_MenuMem(MenuMemLength, 2)
    * --- Default
    *--- Zucchetti Aulla inizio Monitor framework
    * modifica per ottimizzazione framework
    * evita di ripetere due volte la stessa ricerca di un file
    *---
    If Vartype(framework_file_menu)=="U" Or Empty(Alltrim(framework_file_menu))
        *--- Zucchetti Aulla fine
        cMenuFile = cp_FindDefault(n_Ext)
        If Empty(cMenuFile)
            cMenuFile=cp_FindNamedDefaultFile(i_CpDic,n_Ext)
        Endif
        *---Zucchetti Aulla inizio Monitor framework
    Else
        cMenuFile = Addbs(Alltrim(Justpath(framework_file_menu)))+Alltrim(Juststem(framework_file_menu))
        framework_file_menu = ""
    Endif
    *---Zucchetti Aulla fine
    i_MenuMem(1, 1) = Iif(Type('g_LoadMenuMode')='N' And g_LoadMenuMode=2,cp_GetStdFile(cMenuFile,'vmn'),cMenuFile)
    i_MenuMem(1, 2) = cp_GetMenuDate(cMenuFile)
    i_p = Rat('_', cMenuFile)
    If i_p > 0
        cExtDef = Alltrim(Right(cMenuFile, Len(cMenuFile) - i_p+1))
    Else
        cExtDef = ''
    Endif
    * --- Moduli
    cPathTemp = cHomeDir+'..\'
    && ###start remove from setup###
    *---Zucchetti Aulla inizio - Gestione ambientone
    Local cPathTempAHO
    cPathTempAHO = cHomeDir+'..\..\'
    *---Zucchetti Aulla fine
    && ###end remove from setup###
    i_m=i_cmodules
    i_p=At(',',i_m)
    MenuMemLength = 2
    Do While i_p<>0
        cModulo = Left(i_m, i_p-1)
		*** Zucchetti Aulla - inverto ricerca menu modulo perch� per noi � sempre in exe
        *cMenuFile=cPathTemp+cModulo+'\'+cModulo+'_default'
		cMenuFile=cPathTemp+cModulo+'\EXE\'+cModulo+'_default'
        bExistMenu=.T.
        If Not cp_fileexist(cMenuFile+'.'+n_Ext)
			*** Zucchetti Aulla - inverto ricerca menu modulo perch� per noi � sempre in exe
            *cMenuFile=cPathTemp+cModulo+'\EXE\'+cModulo+'_default'
			cMenuFile=cPathTemp+cModulo+'\'+cModulo+'_default'
            If Not cp_fileexist(cMenuFile+'.'+n_Ext)
                && ###start remove from setup###
                * --- Zucchetti Aulla Inizio, ricerca anche in cartella superiore alla cartella che
                * --- contiene l'applicativo (era bExistMenu=.f.)
                If Not Empty( p_Application )
                    cMenuFile=cPathTempAHO+cModulo+'\EXE\'+cModulo+'_default'
                    bExistMenu=cp_fileexist(cMenuFile+'.'+n_Ext,.T.)
                Else
                    && ###end remove from setup###
                    * ---- Gestione eccezione (da rendere opzionale ?)
                    bExistMenu=.F.
                    && ###start remove from setup###
                Endif
                * --- Zucchetti Aulla Fine
                && ###end remove from setup###
            Endif
        Endif
        If bExistMenu
            Dimension i_MenuMem(MenuMemLength,2)
            i_MenuMem(MenuMemLength, 1) = Sys(2014, cMenuFile, Curdir()) &&Path Relativo
            i_MenuMem(MenuMemLength, 2) = cp_GetMenuDate(cMenuFile)
            MenuMemLength = MenuMemLength + 1
        Endif
        i_m=Substr(i_m,i_p+1)
        i_p=At(',',i_m)
    Enddo
    cModulo = i_m
    cMenuFile=cPathTemp+cModulo+'\'+cModulo+'_default'
    bExistMenu=.T.
    If Not cp_fileexist(cMenuFile+'.'+n_Ext)
        cMenuFile=cPathTemp+cModulo+'\EXE\'+cModulo+'_default'
        If Not cp_fileexist(cMenuFile+'.'+n_Ext)
            * --- Zucchetti Aulla Inizio, ricerca anche in cartella superiore alla cartella che
            * --- contiene l'applicativo (era bExistMenu=.f.)
            If Not Empty( p_Application )
                cPathTemp = cHomeDir+'..\..\'
                cMenuFile=cPathTemp+cModulo+'\EXE\'+cModulo+'_default'
                bExistMenu=cp_fileexist(cMenuFile+'.'+n_Ext,.T.)
            Else
                * --- Zucchetti Aulla Fine
                * ---- Gestione eccezione (da rendere opzionale ?)
                bExistMenu=.F.
                * --- Zucchetti Aulla inizio
            Endif
            * --- Zucchetti Aulla Fine
        Endif
    Endif
    If bExistMenu
        Dimension i_MenuMem(MenuMemLength,2)
        i_MenuMem(MenuMemLength, 1) = Sys(2014, cMenuFile, Curdir()) &&Path Relativo
        i_MenuMem(MenuMemLength, 2) = cp_GetMenuDate(cMenuFile)
        MenuMemLength = MenuMemLength + 1
    Endif
    * --- Custom
    cExtCust = ''
    If Not bNoCustom
        * --- legge il men� di applicazione
        cMenuFile = cp_FindNamedDefaultFile('Application',n_Ext)
        If Not Empty(cMenuFile)
            Dimension i_MenuMem(MenuMemLength,2)
            i_MenuMem(MenuMemLength, 1) = cMenuFile
            i_MenuMem(MenuMemLength, 2) = cp_GetMenuDate(cMenuFile)
            i_p = Rat('_', i_MenuMem(MenuMemLength, 1))
            If i_p > 0
                cExtCust = Alltrim(Right(cMenuFile, Len(cMenuFile) - i_p + 1))
            Endif
            MenuMemLength = MenuMemLength + 1
        Endif

        cMenuFile = cp_FindNamedDefaultFile('Application_'+Trim(i_codazi),n_Ext)
        If Not Empty(cMenuFile)
            Dimension i_MenuMem(MenuMemLength,2)
            i_MenuMem(MenuMemLength, 1) = cMenuFile
            i_MenuMem(MenuMemLength, 2) = cp_GetMenuDate(cMenuFile)
            i_p = Rat('_', i_MenuMem(MenuMemLength, 1))
            If i_p > 0
                cExtCust = Alltrim(Right(cMenuFile, Len(cMenuFile) - i_p + 1))
            Endif
            MenuMemLength = MenuMemLength + 1
        Endif

        cMenuFile = cp_FindNamedDefaultFile('custom',n_Ext)
        If Not Empty(cMenuFile)
            Dimension i_MenuMem(MenuMemLength,2)
            i_MenuMem(MenuMemLength, 1) = cMenuFile
            i_MenuMem(MenuMemLength, 2) = cp_GetMenuDate(cMenuFile)
            i_p = Rat('_', i_MenuMem(MenuMemLength, 1))
            If i_p > 0
                cExtCust = Alltrim(Right(cMenuFile, Len(cMenuFile) - i_p + 1))
            Endif
        Endif
    Endif
    * --- costruisco nome file mem
    Public g_MenuMemName
    * --- Zucchetti Aulla - Il file di men� � salvato nella temp...
    g_MenuMemName = tempadhoc()+"\MenuAsMem"+cExtDef+cExtCust
    * --- ritorno l'array contenente l'elenco dei menu con le rispettive date di creazione che compongono il menu finale


Function cp_ReadMenuAsMem(i_cMemFile)
    Public g_aMenuAsMem
    i_cMemFile = Forceext(i_cMemFile,"mem")
    If cp_fileexist(i_cMemFile,.T.)
        Restore From (i_cMemFile) Additive
        Dimension g_aMenuAsMem(Alen(i_aMenuAsMem,1),2)
        Acopy(i_aMenuAsMem, g_aMenuAsMem)
        Return .T.
    Else
        Return .F.
    Endif

Procedure cp_SaveMenuAsMem(i_cMemFile)
    Private i_aMenuAsMem
    Dimension i_aMenuAsMem(Alen(g_aMenuAsMem,1),2)
    i_cMemFile = Forceext(i_cMemFile,"mem")
    Acopy(g_aMenuAsMem, i_aMenuAsMem)
    Save All Like i_aMenuAsMem To (i_cMemFile)
Endproc

Procedure cp_SaveMenu(i_MenuMemName, i_CUR_MENU)
    Select (i_CUR_MENU)
    Copy To (Forceext(i_MenuMemName,"dbf"))
Endproc

Procedure cp_LoadMenu(i_MenuMemName, i_CUR_MENU)
    #If Version(5)>=700
        Select * From (Forceext(i_MenuMemName,"dbf")) Into Cursor (i_CUR_MENU) Readwrite
    #Else
        Local w_nome
        w_nome = Sys(2015)
        Select * From (Forceext(i_MenuMemName,"dbf") ) Into Cursor (w_nome) NOFILTER
        Use Dbf() Again In 0 Alias  (i_CUR_MENU)
        Use
        Select (i_CUR_MENU)
    #Endif
    * --- Aggiunto JUSTFNAME per gestione cartella temporanea
    Use In Select(Justfname(i_MenuMemName))
    Select (i_CUR_MENU)
    Index On LVLKEY Tag LVLKEY COLLATE "MACHINE"
Endproc
