* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_postitfolder                                                 *
*              "+MSG_POSTIN_FOLDER+"                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-06                                                      *
* Last revis.: 2014-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_postitfolder",oParentObject))

* --- Class definition
define class tcp_postitfolder as StdForm
  Top    = 6
  Left   = 13

  * --- Standard Properties
  Width  = 622
  Height = 292+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-20"
  HelpContextID=194929609
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_postitfolder"
  cComment = ""+MSG_POSTIN_FOLDER+""
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODEINBOX = space(10)
  w_CODESENT = space(10)
  w_CODEREM = space(10)
  w_CODETODO = space(10)
  w_ZOOMINBOX = .NULL.
  w_ZOOMSENT = .NULL.
  w_ZOOMREM = .NULL.
  w_ZOOMTODO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_postitfolder
  Proc Delete(pZoom)
    local l_c,bFirst,bDelete,cCursor
    bFirst=.t.
    bDelete=.f.
      cCursor = This.w_&pZoom..cCursor
    Select (m.cCursor)
    Scan For XCHK=1
        if m.bFirst 
          if cp_ConfermicancellazionePostINselezionati()
            bDelete=.t.
          endif
          bFirst=.f.
        endif
        if m.bDelete
          l_c = &cCursor->CODE
          if i_ServerConn[1,2]<>0
            =cp_SQL(i_ServerConn[1,2],"delete from postit where code="+cp_ToStrODBC(m.l_c))
            =cp_SQL(i_ServerConn[1,2],"delete from cpwarn where warncode='"+m.l_c+"'")
          else
            delete from postit where code=m.l_c
            delete from cpwarn where warncode=m.l_c
          endif
        endif
    EndScan
    This.NotifyEvent("ZoomRefresh")
  Endproc
  
  Proc FillPostit
    This.NotifyEvent("ZoomRefresh")
    This.NotifyEvent("RemovePostitExpired")
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_postitfolderPag1","cp_postitfolder",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate(""+MSG_INBOX+"")
      .Pages(2).addobject("oPag","tcp_postitfolderPag2","cp_postitfolder",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate(""+MSG_SENT+"")
      .Pages(3).addobject("oPag","tcp_postitfolderPag3","cp_postitfolder",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate(""+MSG_REMINDER+"")
      .Pages(4).addobject("oPag","tcp_postitfolderPag4","cp_postitfolder",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate(""+MSG_TO_DO+"")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMINBOX = this.oPgFrm.Pages(1).oPag.ZOOMINBOX
    this.w_ZOOMSENT = this.oPgFrm.Pages(2).oPag.ZOOMSENT
    this.w_ZOOMREM = this.oPgFrm.Pages(3).oPag.ZOOMREM
    this.w_ZOOMTODO = this.oPgFrm.Pages(4).oPag.ZOOMTODO
    DoDefault()
    proc Destroy()
      this.w_ZOOMINBOX = .NULL.
      this.w_ZOOMSENT = .NULL.
      this.w_ZOOMREM = .NULL.
      this.w_ZOOMTODO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODEINBOX=space(10)
      .w_CODESENT=space(10)
      .w_CODEREM=space(10)
      .w_CODETODO=space(10)
      .oPgFrm.Page1.oPag.ZOOMINBOX.Calculate()
      .oPgFrm.Page2.oPag.ZOOMSENT.Calculate()
        .w_CODEINBOX = .w_ZOOMINBOX.GetVar("CODE")
        .w_CODESENT = .w_ZOOMSENT.GetVar("CODE")
      .oPgFrm.Page3.oPag.ZOOMREM.Calculate()
      .oPgFrm.Page4.oPag.ZOOMTODO.Calculate()
        .w_CODEREM = .w_ZOOMREM.GetVar("CODE")
        .w_CODETODO = .w_ZOOMTODO.GetVar("CODE")
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_3.enabled = this.oPgFrm.Page3.oPag.oBtn_3_3.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_3.enabled = this.oPgFrm.Page4.oPag.oBtn_4_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMINBOX.Calculate()
        .oPgFrm.Page2.oPag.ZOOMSENT.Calculate()
            .w_CODEINBOX = .w_ZOOMINBOX.GetVar("CODE")
            .w_CODESENT = .w_ZOOMSENT.GetVar("CODE")
        .oPgFrm.Page3.oPag.ZOOMREM.Calculate()
        .oPgFrm.Page4.oPag.ZOOMTODO.Calculate()
            .w_CODEREM = .w_ZOOMREM.GetVar("CODE")
            .w_CODETODO = .w_ZOOMTODO.GetVar("CODE")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMINBOX.Calculate()
        .oPgFrm.Page2.oPag.ZOOMSENT.Calculate()
        .oPgFrm.Page3.oPag.ZOOMREM.Calculate()
        .oPgFrm.Page4.oPag.ZOOMTODO.Calculate()
    endwith
  return

  proc Calculate_DYNIJYNIAA()
    with this
          * --- Open Postit
          cp_OpenPostit(this;
              ,.w_CODEINBOX;
              ,"ZOOMINBOX";
              ,"D";
             )
    endwith
  endproc
  proc Calculate_CRVFETRXNV()
    with this
          * --- Open Postit
          cp_OpenPostit(this;
              ,.w_CODESENT;
              ,"ZOOMSENT";
              ,"D";
             )
    endwith
  endproc
  proc Calculate_PHQHHOHSCP()
    with this
          * --- Rimuovo i postit scaduti
          cp_RemovePostitExpired(this;
             )
    endwith
  endproc
  proc Calculate_JMMDXBAAFN()
    with this
          * --- Oggetto vuoto
          cp_PostitFolderRefreshZoom(this;
              ,"ZOOMINBOX";
             )
    endwith
  endproc
  proc Calculate_DHXXLFATEV()
    with this
          * --- Oggetto vuoto, inserisco prima riga del memo
          cp_PostitFolderRefreshZoom(this;
              ,"ZOOMSENT";
             )
    endwith
  endproc
  proc Calculate_XNOBSXAEZT()
    with this
          * --- Open Postit
          cp_OpenPostit(this;
              ,.w_CODEREM;
              ,"ZOOMREM";
              ,"A";
             )
    endwith
  endproc
  proc Calculate_KIUQBOBMME()
    with this
          * --- Oggetto vuoto, inserisco prima riga del memo
          cp_PostitFolderRefreshZoom(this;
              ,"ZOOMREM";
             )
    endwith
  endproc
  proc Calculate_YZKXJOFRKS()
    with this
          * --- Open Postit
          cp_OpenPostit(this;
              ,.w_CODETODO;
              ,"ZOOMTODO";
              ,"TD";
             )
    endwith
  endproc
  proc Calculate_RDZNZQKUKK()
    with this
          * --- Oggetto vuoto, inserisco prima riga del memo
          cp_PostitFolderRefreshZoom(this;
              ,"ZOOMTODO";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMINBOX.Event(cEvent)
      .oPgFrm.Page2.oPag.ZOOMSENT.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOMINBOX Selected")
          .Calculate_DYNIJYNIAA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMSENT Selected")
          .Calculate_CRVFETRXNV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("RemovePostitExpired")
          .Calculate_PHQHHOHSCP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ZOOMINBOX after query")
          .Calculate_JMMDXBAAFN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("ZoomRefresh")
          .Calculate_DHXXLFATEV()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.ZOOMREM.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOMREM Selected")
          .Calculate_XNOBSXAEZT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("ZoomRefresh")
          .Calculate_KIUQBOBMME()
          bRefresh=.t.
        endif
      .oPgFrm.Page4.oPag.ZOOMTODO.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOMTODO Selected")
          .Calculate_YZKXJOFRKS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("ZoomRefresh")
          .Calculate_RDZNZQKUKK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_postitfolderPag1 as StdContainer
  Width  = 618
  height = 292
  stdWidth  = 618
  stdheight = 292
  resizeXpos=409
  resizeYpos=193
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMINBOX as cp_szoombox with uid="LADZWMWQBC",left=9, top=10, width=605,height=234,;
    caption='ZOOMINBOX',;
   bGlobalFont=.t.,;
    cZoomFile="cp_PostitInbox",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cTable="postit",cZoomOnZoom="",;
    cEvent = "Init,ZoomRefresh",;
    nPag=1;
    , HelpContextID = 74465063


  add object oBtn_1_2 as StdButton with uid="EULBDNIKYH",left=558, top=247, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 155525703;
    , Caption=MSG_S_EXIT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="ZMIZZNTUDJ",left=18, top=247, width=48,height=45,;
    CpPicture="BMP\elimina2.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per cancellare i post-in selezionati";
    , HelpContextID = 244669606;
    , Caption= MSG_DELETE_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        .Delete("ZOOMINBOX")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine
define class tcp_postitfolderPag2 as StdContainer
  Width  = 618
  height = 292
  stdWidth  = 618
  stdheight = 292
  resizeXpos=404
  resizeYpos=166
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMSENT as cp_szoombox with uid="HSOZGLJXGM",left=9, top=10, width=605,height=234,;
    caption='ZOOMSENT',;
   bGlobalFont=.t.,;
    cZoomFile="cp_PostitSent",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cTable="postit",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.f.,;
    cEvent = "Init,ZoomRefresh",;
    nPag=2;
    , HelpContextID = 118079617


  add object oBtn_2_4 as StdButton with uid="BXTWKLZQHK",left=17, top=247, width=48,height=45,;
    CpPicture="BMP\elimina2.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per cancellare i post-in selezionati";
    , HelpContextID = 244669606;
    , Caption= MSG_DELETE_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_4.Click()
      with this.Parent.oContained
        .Delete("ZOOMSENT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_6 as StdButton with uid="TWMJATGPOO",left=558, top=247, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 155525703;
    , Caption=MSG_S_EXIT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine
define class tcp_postitfolderPag3 as StdContainer
  Width  = 618
  height = 292
  stdWidth  = 618
  stdheight = 292
  resizeXpos=410
  resizeYpos=199
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMREM as cp_szoombox with uid="AIMUVZNOLC",left=9, top=10, width=605,height=234,;
    caption='ZOOMSENT',;
   bGlobalFont=.t.,;
    cZoomFile="cp_PostitREM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cTable="postit",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Init,ZoomRefresh",;
    nPag=3;
    , HelpContextID = 118079617


  add object oBtn_3_3 as StdButton with uid="QEUDUAEWEA",left=17, top=247, width=48,height=45,;
    CpPicture="BMP\elimina2.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per cancellare i post-in selezionati";
    , HelpContextID = 244669606;
    , Caption= MSG_DELETE_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_3.Click()
      with this.Parent.oContained
        .Delete("ZOOMREM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_5 as StdButton with uid="VEZYEKTMOF",left=558, top=247, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 155525703;
    , Caption=MSG_S_EXIT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine
define class tcp_postitfolderPag4 as StdContainer
  Width  = 618
  height = 292
  stdWidth  = 618
  stdheight = 292
  resizeXpos=454
  resizeYpos=178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMTODO as cp_szoombox with uid="NUOVTUPLTU",left=9, top=10, width=605,height=234,;
    caption='ZOOMSENT',;
   bGlobalFont=.t.,;
    cZoomFile="cp_PostitToDo",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cTable="postit",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Init,ZoomRefresh",;
    nPag=4;
    , HelpContextID = 118079617


  add object oBtn_4_3 as StdButton with uid="SOUTWDVDAE",left=17, top=247, width=48,height=45,;
    CpPicture="BMP\elimina2.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per cancellare i post-in selezionati";
    , HelpContextID = 244669606;
    , Caption= MSG_DELETE_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_3.Click()
      with this.Parent.oContained
        .Delete("ZOOMTODO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_5 as StdButton with uid="ZITWUBOBTG",left=558, top=247, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 155525703;
    , Caption=MSG_S_EXIT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_postitfolder','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_postitfolder
*--- Apertura postit al dblClick
Proc cp_OpenPostit(pParent, pCode, pZoom, pStatus)
*--- Rimuovo il postit dallo zoom
  local l_Zoom, oPostIt, l_bIntegraELink
  l_Zoom = pParent.w_&pZoom
  Delete from (l_Zoom.cCursor) where CODE = pCode
  l_Zoom.Refresh()

  if i_ServerConn[1,2]<>0
    =cp_SQL(i_ServerConn[1,2],"select * from postit where code="+cp_ToStrODBC(pCode),"postit_c")
  else
    select * from postit where code=pCode into cursor postit_c
  endif
  *-- I postit appuntamento e to do non posso essere integrati o apparire nei link
  l_bIntegraELink = Upper(pZoom)="ZOOMREM" or Upper(pZoom)="ZOOMTODO"
  If VARTYPE(g_Alertmanager)<>'O' OR ( VARTYPE(i_bDisableNewPostIt)='L' And i_bDisableNewPostIt) or i_VisualTheme=-1
    m.oPostIt=createobject('postit','set',i_ServerConn[1,2], .null., Upper(pZoom)="ZOOMSENT", l_bIntegraELink, l_bIntegraELink)
    m.oPostIt.oPstCnt.cStatus=pStatus
  Else
    m.oPostIt = g_Alertmanager.AddAlert("cp_AdvPostIt", 'set', i_ServerConn[1,2], Upper(pZoom)="ZOOMSENT")
    m.oPostIt.DoAlert()
    m.oPostIt = .Null.
  Endif  
  use in select("postit_c")
  if i_ServerConn[1,2]<>0
    =cp_SQL(i_ServerConn[1,2],"update postit set status="+cp_ToStrODBC(pStatus)+" where code="+cp_ToStrODBC(pCode))
  else
    update postit set status=pStatus where code=pCode
  endif  
Endproc

*--- Rimuove i postit scaduti
Proc cp_RemovePostitExpired(pParent)
  if i_ServerConn[1,2]<>0
    =cp_SQL(i_ServerConn[1,2],"select code, count(*) as NumPostit from postit where status='F' and usercode="+cp_ToStrODBC(i_codute)+" and datestop is not null and datestop<"+cp_ToStrODBC(date()) +" group by code","postit_expired")
  else
    select code, count(*) as NumPostit from postit where status='F' and usercode=i_codute and IIf(IsNull(datestop) Or datestop=ctod('  /  /  '), Date(), datestop)<Date() group by code into cursor postit_expired
  endif 
  If used("postit_expired") and postit_expired.NumPostit>0 and cp_YesNo(MSG_POSTITEXPIRED)
     Local l_c
     Select postit_expired
     Scan
        l_c = postit_expired.code
        if i_ServerConn[1,2]<>0
          =cp_SQL(i_ServerConn[1,2],"delete from postit where code="+cp_ToStr(m.l_c))
        else
          delete from postit where code=m.l_c
        endif      
     EndScan
  Endif
  use in select("postit_expired")  
  pParent.notifyevent('ZoomRefresh')
EndProc

Proc cp_PostitFolderRefreshZoom(pParent, pZoom)
  local l_Zoom, oPostIt
  l_Zoom = pParent.w_&pZoom
  Update (l_Zoom.cCursor) Set subject =left(ChrTran(substr(postit,2,at('�',postit,1)-1),Chr(255)+Chr(253)," '"),250) where Empty(Nvl(subject,''))
  *Substr(mLine(postit,1,1), 2, Len(mLine(postit,1,1))-2) 
  l_Zoom.Refresh()
EndProc
* --- Fine Area Manuale
