* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_showpreview                                                  *
*              Anteprima di stampa                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-15                                                      *
* Last revis.: 2013-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_showpreview
KeyPreview=.T.
* --- Fine Area Manuale
return(createobject("tcp_showpreview",oParentObject))

* --- Class definition
define class tcp_showpreview as StdForm
  Top    = 2
  Left   = 7

  * --- Standard Properties
  Width  = 609
  Height = 574
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-08-26"
  HelpContextID=163863771
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_showpreview"
  cComment = "Anteprima di stampa"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CURRENTPAGE = 0
  w_TOTALPAGES = 0
  w_EXECZOOM = .F.
  w_XFRXBOOKWIDTH = 0
  w_ZOOMBOOKVISIBLE = .F.
  w_TREEBOOKVISIBLE = .F.
  w_XFRXPREVIEW = .NULL.
  w_ZOOMBOOK = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_showpreview
  maxbutton = IIF(i_VisualTheme=-1 AND (i_cViewMode="A" Or i_cViewMode="M"),.f.,.t.)
  
  PROCEDURE Show(bModal)
    	  DoDefault(m.bModal)
        this.Move(0,0,_screen.width,_screen.height-50)
        this.resize()
        this.w_XFRXPREVIEW.zoom(-2)
        this.w_XFRXPREVIEW.Resize()
  Endproc
  
  PROCEDURE HideZoomBook
     
     IF this.w_ZOOMBOOKVISIBLE
       this.w_ZOOMBOOKVISIBLE=.F.
       this.w_ZOOMBOOK.visible=.F.
     
       IF this.w_XFRXBOOKWIDTH = 0
         this.w_XFRXBOOKWIDTH=this.w_ZOOMBOOK.width
       ENDIF
       * Nasconde il bookmark e allarga la preview
       *this.w_ZOOMBOOK.width = 41
       
       this.w_ZOOMBOOK.left = this.width - this.w_XFRXBOOKWIDTH
       
       this.w_XFRXPREVIEW.width = this.width - 12
     
       this.mhidecontrols()
     ENDIF
  ENDPROC
  
  PROCEDURE ShowZoomBook
     IF NOT this.w_ZOOMBOOKVISIBLE
       this.w_ZOOMBOOKVISIBLE=.T.
       this.w_ZOOMBOOK.visible=.T.
     
       * Restringe la preview e visualizza il bookmark
       this.w_XFRXPREVIEW.width = this.w_XFRXPREVIEW.width - this.w_XFRXBOOKWIDTH
       
       this.w_ZOOMBOOK.left = this.width - this.w_XFRXBOOKWIDTH
       *this.w_ZOOMBOOK.width = this.w_XFRXBOOKWIDTH
  
       this.mhidecontrols()
     ENDIF
  ENDPROC
  
  PROCEDURE HideTreeBook
     
     this.w_TREEBOOKVISIBLE=.F.
     this.w_TREEBOOK.visible=.F.
     
     IF this.w_XFRXBOOKWIDTH = 0
       this.w_XFRXBOOKWIDTH=this.w_ZOOMBOOK.width
     ENDIF
     * Nasconde il bookmark e allarga la preview
     *this.w_TREEBOOK.width = 41
  
     this.w_TREEBOOK.left = this.width - this.w_XFRXBOOKWIDTH
  
     this.w_XFRXPREVIEW.width = this.width - 12
     
     this.mhidecontrols()
  
  ENDPROC
  
  PROCEDURE ShowTreeBook
     
     this.w_ZOOMBOOKVISIBLE=.T.
     this.w_TREEBOOK.visible=.T.
     
     * Restringe la preview e visualizza il bookmark
     this.w_XFRXPREVIEW.width = this.w_XFRXPREVIEW.width - this.w_XFRXBOOKWIDTH
  
     this.w_TREEBOOK.left = this.width - this.w_XFRXBOOKWIDTH
     *this.w_TREEBOOK.width = this.w_XFRXBOOKWIDTH
  
     this.mhidecontrols()
  
  ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_showpreviewPag1","cp_showpreview",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_XFRXPREVIEW = this.oPgFrm.Pages(1).oPag.XFRXPREVIEW
    this.w_ZOOMBOOK = this.oPgFrm.Pages(1).oPag.ZOOMBOOK
    DoDefault()
    proc Destroy()
      this.w_XFRXPREVIEW = .NULL.
      this.w_ZOOMBOOK = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CURRENTPAGE=0
      .w_TOTALPAGES=0
      .w_EXECZOOM=.f.
      .w_XFRXBOOKWIDTH=0
      .w_ZOOMBOOKVISIBLE=.f.
      .w_TREEBOOKVISIBLE=.f.
      .oPgFrm.Page1.oPag.XFRXPREVIEW.Calculate()
        .w_CURRENTPAGE = .w_XFRXPREVIEW.nCURRENTPAGENUMBER
        .w_TOTALPAGES = .w_XFRXPREVIEW.nTOTALPAGES
      .oPgFrm.Page1.oPag.ZOOMBOOK.Calculate()
          .DoRTCalc(3,4,.f.)
        .w_ZOOMBOOKVISIBLE = .F.
        .w_TREEBOOKVISIBLE = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.XFRXPREVIEW.Calculate()
            .w_CURRENTPAGE = .w_XFRXPREVIEW.nCURRENTPAGENUMBER
            .w_TOTALPAGES = .w_XFRXPREVIEW.nTOTALPAGES
        .oPgFrm.Page1.oPag.ZOOMBOOK.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.XFRXPREVIEW.Calculate()
        .oPgFrm.Page1.oPag.ZOOMBOOK.Calculate()
    endwith
  return

  proc Calculate_LOTXZWOKFK()
    with this
          * --- Aggancia oggetto embeddedtoolbar e bookmark con la preview
          .w_XFRXPREVIEW.oZoomBook = .w_ZOOMBOOK
    endwith
  endproc
  proc Calculate_IFZHHXBAEM()
    with this
          * --- Apre la toolbar con i bottoni
          .w_XFRXPREVIEW.sToolbarClass = "zxfrxToolbar"
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- cp_showpreview
    if lower(cEvent)=='done'
      pop key
      this.w_XFRXPREVIEW.Quit()
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.XFRXPREVIEW.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_LOTXZWOKFK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_IFZHHXBAEM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZOOMBOOK.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- cp_showpreview
    if lower(cEvent)=='init'
       push key
       on key label HOME i_curform.w_XFRXPREVIEW.pages.page1.CNTPREVIEWER.GotoFirstPage()
       on key label PGUP i_curform.w_XFRXPREVIEW.pages.page1.CNTPREVIEWER.GotoPreviousPage()
       on key label PGDN i_curform.w_XFRXPREVIEW.pages.page1.CNTPREVIEWER.GotoNextPage()
       on key label END i_curform.w_XFRXPREVIEW.pages.page1.CNTPREVIEWER.GotoLastPage()
       on key label ESC i_curform.ecpQuit()
       this.w_ZOOMBOOKVISIBLE=.T.
       this.HideZoomBook()
       this.w_XFRXPREVIEW.PREVIEWXFF(this.oParentObject.oParentObject.w_RUNREP_SESSION.GetXfDocument())
       this.w_XFRXPREVIEW.Resize()
       
       * Assegna il titolo alla maschera
       IF TYPE("this.oParentObject.oParentObject.w_NOMEREP") = "C"
         this.caption = AH_MSGFORMAT("Anteprima di stampa - Report %1.frx", LOWER( ALLTRIM( this.oParentObject.oParentObject.w_NOMEREP ) ) )
       ENDIF
       
       this.w_XFRXPREVIEW.resize()
       
    endif
    
    if cEvent="BookmarkOn"
       this.ShowZoomBook()
       * this.ShowTreeBook()
    endif
    
    if cEvent="BookmarkOff"
       this.HideZoomBook()   
       * this.HideTreeBook()   
    endif
    
    if cEvent="w_zoombook selected"
       this.w_XFRXPREVIEW.pages.page1.CNTPREVIEWER.activateobject( this.w_ZoomBook.GetVar( "FND_PAG"), this.w_ZoomBook.GetVar( "FND_NAM"), .T. )
    endif
    
    if cEvent="DblClickOnPreview"
        this.w_XFRXPREVIEW.DblClickOnPreview()
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_showpreviewPag1 as StdContainer
  Width  = 605
  height = 574
  stdWidth  = 605
  stdheight = 574
  resizeXpos=119
  resizeYpos=330
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object XFRXPREVIEW as zcntxfrxmultipage with uid="DNRLSCYUXL",left=5, top=2, width=418,height=569,;
    caption='XFRXPREVIEW',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 266536512


  add object ZOOMBOOK as cp_zoombox with uid="ZVATDAMBPL",left=425, top=29, width=168,height=542,;
    caption='ZOOMBOOK',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",bQueryOnLoad=.f.,cTable="cpusers",cMenuFile="",bReadOnly=.t.,bRetriveAllRows=.f.,cZoomFile="CP_ZOOMBOOK",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.f.,bNoZoomGridShape=.f.,;
    nPag=1;
    , HelpContextID = 176220828


  add object oBtn_1_9 as StdButton with uid="XWVWSOJBAF",left=464, top=3, width=27,height=25,;
    CpPicture="ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Chiude il risultato della ricerca";
    , HelpContextID = 87794469;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        .NotifyEvent("BookmarkOff")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_ZOOMBOOKVISIBLE)
     endwith
    endif
  endfunc

  add object oStr_1_10 as StdString with uid="JDAKBWFSCM",Visible=.t., Left=494, Top=7,;
    Alignment=0, Width=104, Height=18,;
    Caption="Chiude ricerca"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (NOT .w_ZOOMBOOKVISIBLE)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_showpreview','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
