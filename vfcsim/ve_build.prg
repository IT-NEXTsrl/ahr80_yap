* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VE_BUILD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------  
* Autore        : Samuele Masetto
* Data creazione: 02/07/98
* Aggiornato il : 02/07/98
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Costruttore foglio Excel
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam i_Excl,i_cCursor,i_cFileName,i_bCreateModel,i_bDisplayModel,i_ex,i_oQry, i_bNoMessage, i_bNoVisible, i_bNoTot
Return(Createobject('excel',i_Excl,i_cCursor,i_cFileName,i_bCreateModel,i_bDisplayModel,i_ex,i_oQry,i_bNoMessage,i_bNoVisible,i_bNoTot))

#Define C_MAXIMIZED -4137

Define Class excel As Custom
	i_cCursor=''
	i_cModel=''
	i_bCreateModel=.F.
	i_bDisplayModel=.F.
	oExcel=.Null.
	oExcelData=.Null.
	i_nFile_xls=''
	i_nMaxCopyRows = 1048000
	i_bNoPic=.F.

	*-- Valori della variabile g_EXLEXPFAS
	*-- Maggiore � il valore, maggiore � la velocit� di esportazione ma anche il rischio di ottenere un file non corretto
	*-- 0 : Esportazione cella per cella
	*-- 1 : Esportazione solo delle righe piene
	*-- 2 : Esportazione per colonne con messaggi
	*-- 3 : Esportazione per colonne senza messaggi, le colonne a rischio di errore vengono copiate cella per cella
	*-- 4 : Esportazione per colonne senza messaggi, le colonne a rischio di errore non vengono copiate ulteriormente

	Proc Init(i_Excl,i_cCursor,i_cModel,i_bCreateModel,i_bDisplayModel,i_ex,i_oQry, i_bNoMessage,i_bNoVisible, i_bNoTot)
		Local i, k, i_nRow, i_cModelName, i_cOpen, i_Ext, i_ntimeexec, i_npercent, i_npercentmsg, i_nFilecWrkExl, i_cFileToDel, i_bIsxlt, i_bFromWord
		Local oObj
    **Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
		DecoratorState ("D")
        This.i_nMaxCopyRows=IIF(TYPE("m.i_nMaxCopyRows")='N' , m.i_nMaxCopyRows , This.i_nMaxCopyRows )
		i_ntimeexec=Seconds()
		i_npercent=0
		i_npercentmsg=1
		This.i_cCursor=i_cCursor
		This.i_cModel=i_cModel
		This.i_bCreateModel=i_bCreateModel
		This.i_bDisplayModel=i_bDisplayModel
		This.oExcel=Createobject('cp_excel',i_Excl, This.i_nMaxCopyRows)
		If (Vartype(i_ex)='C' And Upper (i_ex)='XLTX') Or Empty(Justext(This.i_cModel))
			Do Case
				Case cp_IsStdFile(This.i_cModel,'XLTX')
					i_Ext='XLTX'
				Case cp_IsStdFile(This.i_cModel,'XLTM')
					i_Ext='XLTM'				
				Otherwise
					i_Ext='XLT'
			Endcase
		Else
			i_Ext=Lower(Justext(This.i_cModel))
		Endif
		*-- I file e i messaggi non vengono visualizzati solo nel caso di stampa unione Word
		i_bFromWord = m.i_bNoMessage And m.i_bNoVisible
		Dimension i_aFld[1,5]
		i_nRow=0
		If Type('this.i_cModel')='C'
			If Lower(Right(This.i_cModel,4))='.xlt' Or Lower(Right(This.i_cModel,5))='.xltx' Or Lower(Right(This.i_cModel,5))='.xltm'
				This.i_cModel=Left(This.i_cModel,Len(This.i_cModel)-Len(i_Ext)-1)
			Endif
			i_cModelName=This.i_cModel
		ENDIF	
		If !Empty(i_cModelName)
			Do Case
				Case This.i_bCreateModel And !cp_IsStdFile(i_cModelName,i_Ext)
					*-- Verifico esistenza componente excel
					oObj=Createobject("excel.application")
					If Isnull(m.oObj)
						cp_ErrorMsg(MSG_CANNOT_OPEN_EXCEL_WORKSHEET_QM,16,MSG_ERROR)
						Return
					ENDIF
					Release m.oObj					
					i_cModelName=Forceext(i_cModelName,i_Ext)
					This.oExcel.CreateModel(i_cModelName,This.i_cCursor,i_oQry,m.i_bNoTot)
					i_cOpen=This.oExcel.Open(i_cModelName,This.i_bDisplayModel)
					This.CreateTableExcel(@i_nFilecWrkExl, i_cCursor,.T.)
				Case cp_IsStdFile(i_cModelName,i_Ext) Or (!m.i_bFromWord And Type("g_ExcelNoModel")<>"U" And g_ExcelNoModel=2)
					*-- Se esiste il modello o viene prevista l'esportazione con la PasteSpecial nel caso non esista
					*-- Verifico esistenza componente excel
					oObj=Createobject("excel.application")
					If Isnull(m.oObj)
						cp_ErrorMsg(MSG_CANNOT_OPEN_EXCEL_WORKSHEET_QM,16,MSG_ERROR)
						Return
					ENDIF
					Release m.oObj
					If cp_IsStdFile(i_cModelName,i_Ext)
						i_cModelName=cp_GetStdFile(i_cModelName,i_Ext)
						i_cModelName=Forceext(i_cModelName,i_Ext)
						i_cOpen=This.oExcel.Open(i_cModelName,This.i_bDisplayModel)
					Endif
					This.CreateTableExcel(@i_nFilecWrkExl, i_cCursor,.T.)
				Otherwise
					i_cOpen='NOBODY'
			Endcase
		Else
			i_cOpen='NOBODY'
		Endif
		If Upper(Justext(i_cModelName))=='XLT'
			m.i_bIsxlt=.T.
		Endif
		If Type("this.oExcel.o.ActiveSheet.pictures")="O" And This.oExcel.o.ActiveSheet.PictureS.Count=0
			This.i_bNoPic=.T.
		Endif
		Select (This.i_cCursor)
		Dimension i_aFld[fcount(),5]
		For i=1 To Fcount()
			i_aFld[i,1]=Field(i)
			Do Case
				Case i_cOpen=='BODY'
					This.oExcel.GetFieldPos(Upper(Field(i)),@i_aFld,i)
				Case Vartype(i_oQry)='O' And (Lower(i_oQry.Class)='zoombox' Or Lower(i_oQry.Class)='cp_szoombox' Or Lower(i_oQry.Class)='cp_zoombox') And (Vartype(i_oQry.ocpquery)<>'O' Or i_oQry.ocpquery.nfields>0)
					i_aFld[i,2]=2
					k=i_oQry.Getcol(i_aFld[i,1])
					If k>0
						i_aFld[i,3]=i_oQry.grd.Columns(k).ColumnOrder - Iif( Lower(i_oQry.Class)='cp_szoombox', 1, 0) &&Elimino la colonna XCHK
						i_aFld[i,4]=1
					Else
						i_aFld[i,3]=0
						i_aFld[i,4]=0
					Endif

			Endcase
		Next
		Do Case
			Case i_cOpen=='BODY'
				i_nRow=This.oExcel.i_nEndBody
                If (Vartype(g_EXLEXPFAS)<>'N' Or g_EXLEXPFAS>=2) And This.oExcel.i_nDsp=0 And Not Empty(This.i_cModel)
					Select (This.i_cCursor)
					Go Top
					This.oExcel.AddDataFixed(This.i_cCursor,@i_aFld,i_nRow, m.i_bIsxlt, m.i_bNoMessage)
					This.oExcel.PasteDataBody(This.i_cCursor,@i_aFld,i_nRow,This.oExcelData,i_nFilecWrkExl, This.i_bNoPic, m.i_bNoMessage)
					This.oExcelData.DisplayAlerts=.F.
					*This.oExcelData.workbooks(i_nFilecWrk).Close(.F.)
					This.oExcelData.workbooks(i_nFilecWrkExl).Close(.F.)
					i_nRow=i_nRow+This.oExcel.i_nNumbRow+1+This.oExcel.i_nDsp
					* Chiudo tutti i fogli di lavoro aperti
					For i=1 To This.oExcelData.workbooks.Count
						This.oExcelData.workbooks.Item(1).Close(.F.)
						i_cFileToDel = Iif(i=1,This.i_nFile_xls, Forceext(Addbs(Justpath(This.i_nFile_xls))+Juststem(This.i_nFile_xls)+"_"+Alltrim(Str(i)),"XLSX"))
						Delete File (i_cFileToDel)
					Next
					This.oExcelData.Quit()
				Else
					i=0
					Scan
						If i=0
							This.oExcel.AddDataFixed(This.i_cCursor,@i_aFld,i_nRow, m.i_bNoMessage)
							i=1
						Endif
						This.oExcel.AddDataBody(This.i_cCursor,@i_aFld,i_nRow)
						i_nRow=i_nRow+1+This.oExcel.i_nDsp
						i_npercent=Round(Recno()*100/This.oExcel.i_nNumbRow,2)
						If i_npercentmsg<Round(i_npercent/10,0)
							i_npercentmsg=i_npercentmsg+1
							If !m.i_bNoMessage
								cp_Msg(ah_msgformat("Esecuzione esportazione al %1 %",Alltrim(Str(i_npercent,2))),.T.,.T.,,.F.)
							Endif
						Endif
					Endscan
				Endif
				If!m.i_bNoMessage
					cp_Msg(ah_msgformat("Esecuzione esportazione al 100%"),.T.,.F.,,.F.)
				Endif
				This.oExcel.DeleteModelRow(i_nRow)
				This.oExcel.o.Goto(This.oExcel.o.Range("A1"))
				If This.i_bDisplayModel
					This.oExcel.o.workbooks(This.oExcel.i_cWrkModel).Activate()
				Endif
			Case i_cOpen=='NOBODY'
				*-- Utilizzo la Copy To per fare l'esportazione senza modello
				If !i_bFromWord
				    **Copia dati senza Modello
				    This.i_nFile_xls = Addbs(Getenv("TEMP"))+ALLTRIM(IIF (EMPTY (This.i_cModel) ,"",JUSTSTEM(This.i_cModel)))+ ".XLSX"
				    **recupera i titoli delle colonne
				    Select (This.i_cCursor)
					Dimension i_aTitoliCol[fcount(),2]
				    IF TYPE("i_oQry") = 'O'
				    	**per le esportazioni da zoom crea il file excel con il nome della tabella associata allo zoom
				    	IF EMPTY (This.i_cModel)
				    		This.i_nFile_xls = Addbs(Getenv("TEMP"))+ALLTRIM(IIF (EMPTY (i_oQry.lfile) ,"",JUSTSTEM(i_oQry.lfile)))+ ".XLSX"
				    	ENDIF
				       Local i, k, frase, ind
				       ind=1
				       frase ="SELECT "
			        For i=1 To Alen(i_aTitoliCol,1)             
							if vartype(i_oQry)='O' and (lower(i_oQry.class)='zoombox' or lower(i_oQry.class)='cp_szoombox' Or lower(i_oQry.class)='cp_zoombox') and (VARTYPE(i_oQry.ocpquery)<>'O' OR i_oQry.ocpquery.nfields>0)
							    If i_aFld[i,4] = 1
									k=i_oQry.GetCol(i_aFld[i,1])
									if k>0
										i_aTitoliCol [ind,1]=i_oQry.cColTitle(k)
										i_aTitoliCol [ind,2]=i_oQry.ncolord(k)	
										frase=frase+ i_aFld[i,1]+ " ,"	
										ind=ind+1								
									endif	
								endif	
							endif
			        NEXT			        
			        frase=LEFT(frase,LEN(frase)-1) + " FROM " +ALIAS() + " INTO CURSOR CURTEMPZOOM "
			        IF NOT (TYPE ("g_EXPZOOMALL")='L' AND g_EXPZOOMALL)
			        &frase
			        ENDIF
			         if TYPE ("g_EXCELCOPYTO") = "C"
					   This.i_nFile_xls=FORCEEXT(This.i_nFile_xls,"XLS")
					ENDIF 
			            cursortoxlsx(This.i_nFile_xls ,IIF (EMPTY (This.i_cModel) ,  "Foglio1",JUSTSTEM(This.i_cModel)),ALIAS(),@i_aTitoliCol )
			            IF USED ("CURTEMPZOOM")
			            SELECT CURTEMPZOOM
			            USE
			            ENDIF
			        ELSE 
					    if TYPE ("g_EXCELCOPYTO") = "C"
					        This.i_nFile_xls=FORCEEXT(This.i_nFile_xls,"XLS") 					    
					  	    EXCELCOPYTO( This.i_nFile_xls , ALIAS())
					    Else
			        		cp_cursortoxlsx(This.i_nFile_xls ,IIF (EMPTY (This.i_cModel) ,  "Foglio1",JUSTSTEM(This.i_cModel)),SELECT(0))
			        	endif
        			ENDIF		
        			STAPDF(This.i_nFile_xls ,"OPEN"," ")		    
        			    **Al termine del esportazion riattiva il decorator dei form 
						DecoratorState ("A")
						If !m.i_bNoMessage
							ah_errormsg("Esportazione completata.","i")
						Endif
						Return	    
*!*						This.oExcelData=Createobject("excel.application")
*!*						This.oExcelData.workbooks.Open(This.i_nFile_xls)
*!*		            	This.oExcelData.Visible=!i_bNoVisible
				Else
					*-- Esportazione Word con stampa unione
					This.oExcel.Open('')
					If !Empty(i_cModelName) And !( '\' $ i_cModelName Or '/' $ i_cModelName Or '?' $ i_cModelName Or '*' $ i_cModelName Or '[' $ i_cModelName Or ']' $ i_cModelName ) &&Escludo i nomi con questi caratteri speciali
						This.oExcel.o.ActiveWorkBook.ActiveSheet.Name = Left(i_cModelName, 30)
					Endif
					This.oExcel.AddColumnName(@i_aFld, 1, i_oQry)
					i_nRow=2
					Select (This.i_cCursor)
					Go Top
					Scan
						This.oExcel.AddDataCatalog(This.i_cCursor,@i_aFld,i_nRow)
						i_nRow=i_nRow+1
					Endscan
					If Vartype(i_oQry)='O' And (Lower(i_oQry.Class)='zoombox' Or Lower(i_oQry.Class)='cp_szoombox' Or Lower(i_oQry.Class)='cp_zoombox') And (Vartype(i_oQry.ocpquery)<>'O' Or i_oQry.ocpquery.nfields>0)
						This.oExcel.o.Cells.EntireColumn.AutoFit
					Endif
				Endif
		ENDCASE
		IF TYPE("This.oExcel")='O'
		    This.oExcel.o.Visible = !m.i_bNoVisible
			If This.i_bDisplayModel And !Empty(i_cModelName) 
				This.oExcel.OpenWrk(i_cModelName)
				This.oExcel.o.workbooks(This.oExcel.cWrk).Activate()
			Endif
			*This.oExcel.o.ActiveWorkbook.Savecopyas()
		ENDIF
    **Al termine del esportazion riattiva il decorator dei form 
		DecoratorState ("A")
		If !m.i_bNoMessage
			ah_errormsg("Esportazione completata.","i")
		Endif
		Return
	Proc CreateTableExcel(i_nFilecWrkExl, i_cCursor, bModelExist)
		*** Costruisco il foglio excel con tutti i dati per fare copia e incolla
        If (Vartype(g_EXLEXPFAS)<>'N' Or g_EXLEXPFAS>=2 Or !m.bModelExist) And This.oExcel.i_nDsp=0
        	This.i_nFile_xls = Addbs(Getenv("TEMP"))+Forceext(Sys(2015), ".XLSX")
			Select (i_cCursor)
			*** Se i record da esportare sono minori del limite massimo allora esporto un solo cursore
			*** altrimenti suddivido i record in n fogli xls ognuno avente un numero di record minore
			*** o uguale del numero massimo esportabile
			If Reccount(This.i_cCursor) <= This.i_nMaxCopyRows
				cp_cursortoxlsx(This.i_nFile_xls ,IIF (EMPTY (This.i_cModel) ,  "Foglio1",JUSTSTEM(This.i_cModel)),SELECT(0))
			Else
				Local i, cTmpCursor
				cTmpCursor=Sys(2015)
				For i=1 To Reccount(This.i_cCursor) Step This.i_nMaxCopyRows
					Select * From (This.i_cCursor) Where Recno() >= i And Recno() < i + This.i_nMaxCopyRows Into Cursor (cTmpCursor)
					cp_cursortoxlsx(Forceext(Addbs(Justpath(This.i_nFile_xls))+Juststem(This.i_nFile_xls)+Iif(Ceiling(i/This.i_nMaxCopyRows )>1,"_"+Alltrim(Str(Ceiling(i/This.i_nMaxCopyRows ))),""),"XLSX") ,IIF (EMPTY (This.i_cModel) ,  "Foglio1",JUSTSTEM(This.i_cModel)),SELECT(0))
					Use In Select(cTmpCursor)
				Next
			Endif
			This.oExcelData=Createobject("excel.application")
			*** Apro xl5
			If m.bModelExist
				If Reccount(This.i_cCursor) <= This.i_nMaxCopyRows
					This.oExcelData.workbooks.Open(This.i_nFile_xls)
					i_nFilecWrkExl=This.oExcelData.ActiveWorkBook.Name
					This.oExcelData.workbooks(i_nFilecWrkExl).Activate()
					This.oExcelData.Cells.EntireColumn.AutoFit
				Else
					For i=1 To Reccount(This.i_cCursor) Step This.i_nMaxCopyRows
						This.oExcelData.workbooks.Open(Forceext(Addbs(Justpath(This.i_nFile_xls))+Juststem(This.i_nFile_xls)+Iif(Ceiling(i/This.i_nMaxCopyRows )>1,"_"+Alltrim(Str(Ceiling(i/This.i_nMaxCopyRows ))),""),"XLSX"))
						i_nFilecWrkExl = Iif(i=1, This.oExcelData.ActiveWorkBook.Name, i_nFilecWrkExl)
						This.oExcelData.workbooks(i_nFilecWrkExl).Activate()
						This.oExcelData.Cells.EntireColumn.AutoFit
					Next
				Endif
				***
				This.oExcelData.activewindow.WindowState = C_MAXIMIZED
			Endif
		Endif
		***
		Return
Enddefine

Define Class cp_excel As Custom
	o=.Null.
	i_nBody=0
	i_nEndBody=0
	i_nDsp=0
	i_cSel=''
	i_cWrkModel=''
	i_nNumbRow=0
	cWrk=''
	i_nMaxCopyRows = 1048000  &&numero massimo di righe gestite da Excel (nMaxCopyRows determina quanti fogli excel di appoggio vengono creati in base ai dati cotnenuti nel cursore)
	Proc Init(i_oExcl, nMaxCopyRows)
		This.o = m.i_oExcl
		This.i_nMaxCopyRows = m.nMaxCopyRows
		*-- Zucchetti Aulla Inizio
		*-- Dichiarazione funzione Sleep
		Declare Sleep In kernel32 Integer
		*-- Zucchetti Aulla Fine
	Func Open(i_cFile,i_bDisplayModel,i_bCreateModel,i_oQry)
		Local i_errsav,i
		Private i_err1,i_err2
		If Empty(i_cFile)
			This.o.workbooks.Add
			This.cWrk=This.o.ActiveWorkBook.Name
		Else
			This.o.workbooks.Add(i_cFile)
			This.cWrk=This.o.ActiveWorkBook.Name
			*!*	            If i_bDisplayModel
			*!*	                This.OpenWrk(i_cFile)
			*!*	                This.o.workbooks(This.cWrk).Activate()
			*!*	            Endif
		Endif
		This.o.workbooks(This.cWrk).Activate()
		This.o.activewindow.WindowState = C_MAXIMIZED
		i_errsav=On('ERROR')
		On Error i_err1=.T.
		i_err1=.F.
		i_err2=.F.
		This.o.Goto("BODY")
		On Error &i_errsav
		If !i_err1
			This.i_nBody=This.o.activecell.Row
		Endif
		On Error i_err2=.T.
		This.o.Goto("ENDBODY")
		On Error &i_errsav
		If !i_err2
			This.i_nEndBody=This.o.activecell.Row
			This.i_nDsp=This.i_nEndBody-2-This.i_nBody
		Endif
		If i_err1 Or i_err2
			If i_err1 And i_err2
				*non ci sono i campi di corpo: visualizzazione dell'elenco
				Return("NOBODY")
			Else
				*modello non corretto
				Return("ERROR")
			Endif
		Endif
		i_err1=.F.
		i_err2=.F.
		Return("BODY")
	Proc Save(i_cFile)
		If Type('i_cFile')='C'
			This.o.ActiveWorkBook.SaveAs(i_cFile)
		Else
			This.o.ActiveWorkBook.Save
		Endif
	Proc Destroy()
		This.o=.Null.
	Proc ApplyColor()
		Local r,j
		r=This.o.Selection
		For i=1 To r.Rows.Count
			For j=1 To r.Columns.Count
				r.Cells(i,j).interior.colorindex=27
			Next
		Next
		Return
	Func GetFieldPos(i_cField,i_aFld,i_nIdx)
		Local i_errsav
		Private i_err
		i_errsav=On('ERROR')
		i_err=.F.
		On Error i_err=.T.
		This.o.Goto(i_cField)
		If i_err
			i_err=.F.
			This.o.Goto(Lower(i_cField))
		Endif
		On Error &i_errsav
		If !i_err
			i_aFld[i_nIdx,2]=This.o.activecell.Row
			i_aFld[i_nIdx,3]=This.o.activecell.Column
			If This.o.activecell.Row>This.i_nBody And This.o.activecell.Row<This.i_nEndBody
				i_aFld[i_nIdx,4]=1
			Else
				i_aFld[i_nIdx,4]=0
			Endif
		Else
			Return(.F.)
		Endif
		i_err=.F.
		Return
	Proc AddDataBody(i_cCursor,i_aFld,i_nFirstRow,i_bNoCopyBody)
		Local i,i_cSel,i_cValue
        If NOT i_bNoCopyBody AND Not((Vartype(g_EXLEXPFAS)='N' And g_EXLEXPFAS=1) And This.i_nDsp=0)
			This.CopyBody()
			i_cSel='"'+Alltrim(Str(i_nFirstRow))+":"+Alltrim(Str(i_nFirstRow+This.i_nDsp))+'"'
			This.o.Rows(&i_cSel).Select
			This.o.Selection.Insert(-4121)
		Endif
		For i=1 To Alen(i_aFld,1)
			If Vartype(i_aFld[i,1])='C' And Vartype(i_aFld[i,4])='N' And i_aFld[i,4]=1
				i_cValue=&i_cCursor..&i_aFld[i,1]
				If Vartype(i_cValue)<>'D' Or !Empty(i_cValue)
					Do Case
						Case Vartype(i_cValue)="C"
                            If !Empty(i_cValue) Or (Vartype(g_EXLEXPFAS)<>'N' OR g_EXLEXPFAS<>1)
								This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).Value="'"+Alltrim(i_cValue)
							Endif
						Case Vartype(i_cValue)="N"
                            If Not i_aFld[i,5] And (Vartype(g_EXLEXPFAS)='N' And g_EXLEXPFAS=1) And This.i_nDsp=0
								i_aFld[i,5]=.T.
								This.o.Range(Chr(i_aFld[i,3]+64)+Alltrim(Str(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)))+":"+Chr(i_aFld[i,3]+64)+Alltrim(Str(This.i_nNumbRow+i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)))).Value=0
							Endif
                            If i_cValue<>0 Or Not(Vartype(g_EXLEXPFAS)='N' And g_EXLEXPFAS=1) Or This.i_nDsp>0
								This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).Value=i_cValue
							Endif
						Otherwise
							This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).Value=i_cValue
					Endcase
				Endif
			Endif
		Next
		Return
	Proc PasteDataBody(i_cCursor,i_aFld,i_nFirstRow,oExcelData,cDataWrkXls, i_bNoPic, i_bNoMessage)
		Private i, j, k,i_cSel, i_cCharPosition, i_cCharPosition2, i_nPosition, nAttLastCell, i_cOrigin, i_cDestination, i_nIdxPaste, i_err, i_errsav,i_bYesNo,  i_nCompare, i_nRowAddData, i_cEmptyRows, nLen, nInd
		Local bMemoField
		*** variabile per contare i campi memo da andare a recuperare dal cursore
		*** mentre gli altri sono in cDataWrkXls per problema lettere accentate
		j=0
		i_cEmptyRows=""
		For i=1 To Alen(i_aFld,1)
			m.bMemoField = .F.
			If Type(Alltrim(i_cCursor)+'.'+i_aFld[i,1])="M"
				j=j+1
			Endif
			If Vartype(i_aFld[i,1])='C' And Vartype(i_aFld[i,4])='N' And i_aFld[i,4]=1
				If Type(Alltrim(i_cCursor)+'.'+i_aFld[i,1])="M"
					m.bMemoField = .T.
					i_nPosition=i
				Else
					oExcelData.workbooks(cDataWrkXls).Activate()
					i_nPosition=i-j
				Endif
				i_cCharPosition=This.ConvertToLetter(i_nPosition)
				If m.bMemoField
					*-- I campi Memo non vengono riportati perch� hanno problemi di esportazione sia con XLS che DBF (lettura della struttura in FPT)
					*-- Esporto cella per cella
					Dimension i_xFld[1,5]
					Local i_nRowAddData
					i_xFld[1,1]=i_aFld[i,1]
					i_xFld[1,2]=i_aFld[i,2]
					i_xFld[1,3]=i_aFld[i,3]
					i_xFld[1,4]=i_aFld[i,4]
					i_xFld[1,5]=i_aFld[i,5]
					i_nRowAddData=i_nFirstRow
					Scan
						This.AddDataBody(i_cCursor,@i_xFld,i_nRowAddData, .T.)
						i_nRowAddData=i_nRowAddData+1
					Endscan
				Else
					If This.i_nNumbRow > This.i_nMaxCopyRows
						*** Prelevo i dati della colonna da tutti gli eventuali folgi xl5 aperti in modo
						*** da copiare anche un numero di record maggiore del limite del formato xl5
						For k=1 To This.i_nNumbRow Step This.i_nMaxCopyRows
							If k > 1
								oExcelData.workbooks(Forceext(Juststem(cDataWrkXls)+"_"+Alltrim(Str(Ceiling(k/This.i_nMaxCopyRows))),"XLSX")).Activate()
							Endif
							nAttLastCell = k - 1 + Min(k + This.i_nMaxCopyRows - 1, This.i_nNumbRow)
							i_cOrigin='A'
							i_cDestination='B'
							i_nIdxPaste=1
							i_nCompare=1
							Do While (i_nIdxPaste=1 Or Type("i_cOrigin")<>Type("i_cDestination") Or (Empty(Type("i_cDestination")) And Not Empty(Type("i_cOrigin"))) Or Not(i_cOrigin==i_cDestination)) And i_nIdxPaste<=3 And i_nCompare<=3
								oExcelData.Range(i_cCharPosition+Alltrim(Str(2+(i_aFld[i,2]-This.i_nBody-1)))+":"+i_cCharPosition+Alltrim(Str(This.i_nMaxCopyRows + 2 + (i_aFld[i,2]-This.i_nBody-1)))).Select()
								oExcelData.Range(i_cCharPosition+Alltrim(Str(2+(i_aFld[i,2]-This.i_nBody-1)))+":"+i_cCharPosition+Alltrim(Str(This.i_nMaxCopyRows + 2 + (i_aFld[i,2]-This.i_nBody-1)))).Copy()
								This.o.workbooks(This.cWrk).Activate()
								This.o.Cells(k - 1 + i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).Select
								i_err=.F.
								i_errsav=On('ERROR')
								On Error i_err=.T.
								*if type(alltrim(i_cCursor)+'.'+i_aFld[i,1])='C' OR type(alltrim(i_cCursor)+'.'+i_aFld[i,1])='M'
								*-- Zucchetti Aulla Inizio
								*-- In caso di Windows 8, Excel a vole gira troppo veloce, si usa una sleep progressiva in base ai tentativi di PasteSpecial
								If Type('g_nSleepSys')=='U'
									g_nSleepSys=100
								Endif
								Sleep(g_nSleepSys*i_nIdxPaste)
								*-- Zucchetti Aulla Fine
								This.o.Cells(k - 1 + i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).PasteSpecial(-4163)
								* copio la cella
								This.o.Cells(k - 1 + i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)-1,i_aFld[i,3]).Copy()
								* incollo il formato
								*i_cCharPosition=This.ConvertToLetter(i_aFld[i,3])
								This.o.Range(i_cCharPosition+Alltrim(Str(k - 1 + i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)))+":"+i_cCharPosition+Alltrim(Str(m.nAttLastCell + i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)))).PasteSpecial(-4122)
								*else
								*	this.o.Activesheet.Pastespecial("HTML",,,,,,.t.)
								*endif
								On Error &i_errsav
								If Type(Alltrim(i_cCursor)+'.'+i_aFld[i,1])='T' And Upper(This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).NumberFormatLocal)="STANDARD"
									This.o.Columns(i_aFld[i,3]).NumberFormatLocal="gg/mm/aaaa hh:mm"
								Endif
								If Type(Alltrim(i_cCursor)+'.'+i_aFld[i,1])='D' And Upper(This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).NumberFormatLocal)="STANDARD"
									This.o.Columns(i_aFld[i,3]).NumberFormatLocal="gg/mm/aaaa"
								Endif
								i_nIdxPaste=i_nIdxPaste+1
								i_cOrigin=Nvl(oExcelData.Cells(2,i_nPosition).Value, "")
								i_cDestination=Nvl(This.o.Cells(k - 1 + i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).Value, "")
								*-- Zucchetti Aulla Inizio
								*-- Nel caso ci siano colonne con prima riga vuota, si fa un controllo sulle prime 3 righe
								*-- Il controllo viene fatto solo al primo tentativo di pastespecial
								Do While Empty(Nvl(i_cOrigin,"")) And i_nCompare<=3 And i_nIdxPaste=2
									i_cOrigin=Nvl(oExcelData.Cells(2+i_nCompare,i_nPosition).Value, "")
									i_nCompare=i_nCompare+1
								Enddo
								If i_nCompare=4
									i_cEmptyRows=Iif(Empty(i_cEmptyRows),Alltrim(Str(i)),i_cEmptyRows+","+Alltrim(Str(i)))
								Endif
								*-- Zucchetti Aulla Fine
								If i_err Or (i_nIdxPaste=4 And (Type("i_cOrigin")<>Type("i_cDestination") Or (Empty(Type("i_cDestination")) And Not Empty(Type("i_cOrigin"))) Or Not(i_cOrigin==i_cDestination)))
									i_cOrigin='@@'
									i_cDestination='��'
									i_err=.F.
									If i_nIdxPaste=4
										i_bYesNo= (TYPE ("g_EXLEXPFAS")<>"N" OR g_EXLEXPFAS<>4) And (m.i_bNoMessage Or ( TYPE ("g_EXLEXPFAS")="N" AND g_EXLEXPFAS=3 ) Or ah_YesNo("Errore nella formattazione della colonna %1%0Continuare comunque l'esecuzione?",,i_aFld[i,1])) && (Vartype(g_EXLEXPFAS)<>'N' Or g_EXLEXPFAS<>4) - (Vartype(g_EXLEXPFAS)='N' And g_EXLEXPFAS=3)
										If Not i_bYesNo
											Return
										Endif
										*Se la PasteSpecial non copia correttamente, si copiano singolarmente i campi della colonna specifica
										Dimension i_xFld[1,5]
										Local i_nRowAddData
										i_xFld[1,1]=i_aFld[i,1]
										i_xFld[1,2]=i_aFld[i,2]
										i_xFld[1,3]=i_aFld[i,3]
										i_xFld[1,4]=i_aFld[i,4]
										i_xFld[1,5]=i_aFld[i,5]
										i_nRowAddData=i_nFirstRow
										Scan
											This.AddDataBody(i_cCursor,@i_xFld,i_nRowAddData, .T.)
											i_nRowAddData=i_nRowAddData+1
										Endscan
									Endif
								Endif
							Enddo
						Next &&k=1 TO this.i_nNumbRow STEP this.i_nMaxCopyRows
					Else
						i_cOrigin='A'
						i_cDestination='B'
						i_nIdxPaste=1
						i_nCompare=1
						Do While (i_nIdxPaste=1 Or Type("i_cOrigin")<>Type("i_cDestination") Or (Empty(Type("i_cDestination")) And Not Empty(Type("i_cOrigin"))) Or Not(i_cOrigin==i_cDestination)) And i_nIdxPaste<=3 And i_nCompare<=3
							oExcelData.Range(i_cCharPosition+Alltrim(Str(2+(i_aFld[i,2]-This.i_nBody-1)))+":"+i_cCharPosition+Alltrim(Str(This.i_nNumbRow+2+(i_aFld[i,2]-This.i_nBody-1)))).Select()
							oExcelData.Range(i_cCharPosition+Alltrim(Str(2+(i_aFld[i,2]-This.i_nBody-1)))+":"+i_cCharPosition+Alltrim(Str(This.i_nNumbRow+2+(i_aFld[i,2]-This.i_nBody-1)))).Copy()
							This.o.workbooks(This.cWrk).Activate()
							This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).Select
							i_err=.F.
							i_errsav=On('ERROR')
							On Error i_err=.T.
							*if type(alltrim(i_cCursor)+'.'+i_aFld[i,1])='C' OR type(alltrim(i_cCursor)+'.'+i_aFld[i,1])='M' OR i_nIdxPaste>1
							*-- Zucchetti Aulla Inizio
							*-- In caso di Windows 8, Excel a volte gira troppo veloce, si usa una sleep progressiva in base ai tentativi di PasteSpecial
							If Type('g_nSleepSys')=='U'
								g_nSleepSys=100
							Endif
							Sleep(g_nSleepSys*i_nIdxPaste)
							*-- Zucchetti Aulla Fine
							This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).PasteSpecial(-4163)
							* copio la cella
							This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)-1,i_aFld[i,3]).Copy()
							* incollo il formato
							i_cCharPosition2=This.ConvertToLetter(i_aFld[i,3])
							This.o.Range(i_cCharPosition2+Alltrim(Str(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)))+":"+i_cCharPosition2+Alltrim(Str(This.i_nNumbRow+i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)))).PasteSpecial(-4122)
							*else
							*	this.o.Activesheet.Pastespecial("HTML",,,,,,.t.)
							*endif
							On Error &i_errsav
							If Type(Alltrim(i_cCursor)+'.'+i_aFld[i,1])='T' And Upper(This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).NumberFormatLocal)="STANDARD"
								This.o.Columns(i_aFld[i,3]).NumberFormatLocal="gg/mm/aaaa hh:mm"
							Endif
							If Type(Alltrim(i_cCursor)+'.'+i_aFld[i,1])='D' And Upper(This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).NumberFormatLocal)="STANDARD"
								This.o.Columns(i_aFld[i,3]).NumberFormatLocal="gg/mm/aaaa"
							Endif
							i_nIdxPaste=i_nIdxPaste+1
							i_cOrigin=Nvl(oExcelData.Cells(2,i_nPosition).Value, "")
							i_cDestination=Nvl(This.o.Cells(i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1),i_aFld[i,3]).Value, "")
							*-- Zucchetti Aulla Inizio
							*-- Nel caso ci siano colonne con prima riga vuota, si fa un controllo sulle prime 3 righe
							*-- Il controllo viene fatto solo al primo tentativo di pastespecial
							Do While Empty(Nvl(i_cOrigin,"")) And i_nCompare<=3 And i_nIdxPaste=2
								i_cOrigin=Nvl(oExcelData.Cells(2+i_nCompare,i_nPosition).Value, "")
								i_nCompare=i_nCompare+1
							Enddo
							If i_nCompare=4
								i_cEmptyRows=Iif(Empty(i_cEmptyRows),Alltrim(Str(i)),i_cEmptyRows+","+Alltrim(Str(i)))
							Endif
							*-- Zucchetti Aulla Fine
							If i_err Or (i_nIdxPaste=4 And (Type("i_cOrigin")<>Type("i_cDestination") Or (Empty(Type("i_cDestination")) And Not Empty(Type("i_cOrigin"))) Or Not(i_cOrigin==i_cDestination)))
								i_cOrigin='@@'
								i_cDestination='��'
								i_err=.F.
								If i_nIdxPaste=4
									i_bYesNo= ( TYPE ("g_EXLEXPFAS")<>"N" OR g_EXLEXPFAS<>4 ) And (m.i_bNoMessage Or (TYPE ("g_EXLEXPFAS")="N" AND g_EXLEXPFAS=3 )Or ah_YesNo("Errore nella formattazione della colonna %1%0Continuare comunque l'esecuzione?",,i_aFld[i,1])) && (Vartype(g_EXLEXPFAS)<>'N' Or g_EXLEXPFAS<>4) - (Vartype(g_EXLEXPFAS)='N' And g_EXLEXPFAS=3)
									If Not i_bYesNo
										Return
									Endif
									*Se la PasteSpecial non copia correttamente, si copiano singolarmente i campi della colonna specifica 
									Dimension i_xFld[1,5]
									Local i_nRowAddData
									i_xFld[1,1]=i_aFld[i,1]
									i_xFld[1,2]=i_aFld[i,2]
									i_xFld[1,3]=i_aFld[i,3]
									i_xFld[1,4]=i_aFld[i,4]
									i_xFld[1,5]=i_aFld[i,5]
									i_nRowAddData=i_nFirstRow
									Scan
										This.AddDataBody(i_cCursor,@i_xFld,i_nRowAddData, .T.)
										i_nRowAddData=i_nRowAddData+1
									Endscan
								Endif
							Endif
						Enddo
					Endif &&this.i_nNumbRow > this.i_nMaxCopyRows
				Endif &&m.bMemoField
			Endif &&vartype(i_aFld[i,1])='C' and vartype(i_aFld[i,4])='N' and i_aFld[i,4]=1
		Next &&i=1 to alen(i_aFld,1)
		If Not Empty(i_cEmptyRows)
			i_bYesNo= (TYPE ("g_EXLEXPFAS")<>"N" OR g_EXLEXPFAS<>4) And (m.i_bNoMessage Or  ( TYPE ("g_EXLEXPFAS")="N" AND  g_EXLEXPFAS=3) Or ah_YesNo("Non � possibile rilevare la corretta esportazione di alcune colonne.%0Verr� eseguita l'esportazione cella per cella di queste colonne.%0Continuare comunque l'esportazione?",)) && (Vartype(g_EXLEXPFAS)<>'N' Or g_EXLEXPFAS<>4) - (Vartype(g_EXLEXPFAS)='N' And g_EXLEXPFAS=3)
			If Not i_bYesNo
				Return
			Endif
			Dimension aRow[1]
			nLen=Alines(aRow,i_cEmptyRows,",")
			For i=1 To nLen
				Dimension i_xFld[1,5]
				nInd=Val(aRow[i])
				i_xFld[1,1]=i_aFld[nInd,1]
				i_xFld[1,2]=i_aFld[nInd,2]
				i_xFld[1,3]=i_aFld[nInd,3]
				i_xFld[1,4]=i_aFld[nInd,4]
				i_xFld[1,5]=i_aFld[nInd,5]
				i_nRowAddData=i_nFirstRow
				Scan
					This.AddDataBody(i_cCursor,@i_xFld,i_nRowAddData, .T.)
					i_nRowAddData=i_nRowAddData+1
				Endscan
			Next
		Endif
		If This.i_nNumbRow>This.i_nMaxCopyRows
			This.o.Rows(i_nFirstRow+This.i_nNumbRow).Delete
		Endif
		*-- Se la PasteSpecial ha copiato in formato immagine e non c'erano immagini nel modello, vengono cancellate tutte le immagini
		If i_bNoPic And Type("this.oExcel.o.ActiveSheet.pictures")="O" And This.o.ActiveSheet.PictureS.Count<>0
			This.o.ActiveSheet.PictureS.Delete
		Endif
		Return
	Func ConvertToLetter(i_nPosition)
		Local i_cCharPosition
		i_cCharPosition=Iif(Mod(i_nPosition,26)=0,'Z', Chr(Mod(i_nPosition,26)+64))
		If Iif(Mod(i_nPosition,26)=0, Int(i_nPosition/26)-1, Int(i_nPosition/26))>0
			i_cCharPosition=Chr(Mod(Iif(Mod(i_nPosition,26)=0, Int(i_nPosition/26)-1, Int(i_nPosition/26)),26)+64)+i_cCharPosition
		Endif
		Return i_cCharPosition
	Proc AddDataFixed(i_cCursor,i_aFld,i_nFirstRow,i_bIsxlt, i_bNoMessage)
		Local i,i_cValue,i_cSel
		This.i_nNumbRow=Reccount(i_cCursor)
		This.CopyBody()
        If (Vartype(g_EXLEXPFAS)<>'N' Or g_EXLEXPFAS>0) And This.i_nDsp=0
            if i_bIsxlt
				If This.i_nNumbRow>This.i_nMaxCopyRows
					This.i_nNumbRow=This.i_nMaxCopyRows
					If !m.i_bNoMessage
						cp_ErrorMsg("Attenzione numero di righe eccessivo per il formato xlt%0L'esportazione verr� ridotta a %1 righe",,This.i_nMaxCopyRows)
					Endif
				Endif
				i_cSel='"'+Alltrim(Str(i_nFirstRow))+":"+Alltrim(Str(i_nFirstRow+This.i_nNumbRow-1))+'"'
				This.o.Rows(&i_cSel).Insert(-4121)
			Else
				For k=1 To This.i_nNumbRow Step This.i_nMaxCopyRows
					i_cSel='"'+Alltrim(Str(k-1+i_nFirstRow))+":"+Alltrim(Str(i_nFirstRow+Iif(This.i_nNumbRow > k+This.i_nMaxCopyRows,k+This.i_nMaxCopyRows,This.i_nNumbRow)-1))+'"'
					This.o.Rows(&i_cSel).Insert(-4121)
				Next
			Endif
		Endif

		For i=1 To Alen(i_aFld,1)
			If Vartype(i_aFld[i,1])='C' And Vartype(i_aFld[i,4])='N' And i_aFld[i,4]=0
				i_cValue=&i_cCursor..&i_aFld[i,1]
				If Vartype(i_cValue)<>'D' Or !Empty(i_cValue)
					Do Case
						Case Vartype(i_cValue)="C"
                            If !Empty(i_cValue) Or Not(Vartype(g_EXLEXPFAS)='N' And g_EXLEXPFAS=1)
								This.o.Cells(i_aFld[i,2],i_aFld[i,3]).Value="'"+Alltrim(i_cValue)
							Endif
						Case Vartype(i_cValue)="N"
                            If Not i_aFld[i,5] And (Vartype(g_EXLEXPFAS)='N' And g_EXLEXPFAS=1) And This.i_nDsp=0
								i_aFld[i,5]=.T.
								This.o.Range(Chr(i_aFld[i,3]+64)+Alltrim(Str(i_aFld[i,2]))+":"+Chr(i_aFld[i,3]+64)+Alltrim(Str(This.i_nNumbRow+i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)))).Value=0
							Endif
                            If i_cValue<>0 Or Not(Vartype(g_EXLEXPFAS)='N' And g_EXLEXPFAS=1) Or This.i_nDsp>0
								This.o.Cells(i_aFld[i,2],i_aFld[i,3]).Value=i_cValue
							Endif
						Otherwise
							This.o.Cells(i_aFld[i,2],i_aFld[i,3]).Value=i_cValue
					Endcase
				Endif
			Endif
		Next
		Return
	Proc AddDataCatalog(i_cCursor,i_aFld,i_nRow)
		Local i,i_cSel,i_cValue
		For i=1 To Alen(i_aFld,1)
			If Type('i_aFld[i,1]')='C'
				i_cValue=&i_cCursor..&i_aFld[i,1]
				If Type('i_cValue')<>'D' Or !Empty(i_cValue)
					If Type('i_aFld[i,4]')<>'N' Or i_aFld[i,4] > 0
						If Type('i_cValue')='C'
							If Type('i_aFld[i,3]')='N' And i_aFld[i,3] > 0
								This.o.Cells(i_nRow,i_aFld[i,3]).Value="'"+Alltrim(i_cValue)
							Else
								This.o.Cells(i_nRow,i).Value="'"+Alltrim(i_cValue)
							Endif
						Else
							If Type('i_aFld[i,3]')='N' And i_aFld[i,3] > 0
								This.o.Cells(i_nRow,i_aFld[i,3]).Value=i_cValue
							Else
								This.o.Cells(i_nRow,i).Value=i_cValue
							Endif
						Endif
					Endif
				Endif
			Endif
		Next
		Return
	Proc AddColumnName(i_aFld, i_nRow, i_oQry)
		Local i, k
		For i=1 To Alen(i_aFld,1)
			If Type('i_aFld[i,1]')='C'
				If Vartype(i_oQry)='O' And (Lower(i_oQry.Class)='zoombox' Or Lower(i_oQry.Class)='cp_szoombox' Or Lower(i_oQry.Class)='cp_zoombox') And (Vartype(i_oQry.ocpquery)<>'O' Or i_oQry.ocpquery.nfields>0)
					If i_aFld[i,4] = 1
						k=i_oQry.Getcol(i_aFld[i,1])
						If k>0
							This.o.Cells(i_nRow,i_aFld[i,3]).Value=i_oQry.cColTitle(k)
						Endif
						This.o.Cells(i_nRow,i_aFld[i,3]).Font.bold=.T.
						This.o.Cells(i_nRow,i_aFld[i,3]).interior.Color=i_ThemesManager.GetProp(7)
					Endif
				Else
					This.o.Cells(i_nRow,i).Value=i_aFld[i,1]
					This.o.Cells(i_nRow,i).Font.bold=.T.
					This.o.Cells(i_nRow,i).interior.Color=i_ThemesManager.GetProp(7)
				Endif
			Endif
		Next
		Return
	Proc CopyBody()
		Local i_cSel
		If Empty(This.i_cSel)
			i_cSel='"'+Alltrim(Str(This.i_nBody+1))+":"+Alltrim(Str(This.i_nEndBody-1))+'"'
			This.i_cSel=i_cSel
		Else
			i_cSel=This.i_cSel
		Endif
		This.o.Rows(&i_cSel).Select
		This.o.Selection.Copy
		Return
	Proc DeleteModelRow(i_nRow)
		Local i_cSel, oUnion,i_c, i_nEndB, i_errsav, i_err2
		i_c='"'+Alltrim(Str(This.i_nBody))+':'+Alltrim(Str(This.i_nBody+1+This.i_nDsp))+'"'
		i_errsav=On('ERROR')
		i_err2=.F.
		On Error i_err2=.T.
		This.o.Goto("ENDBODY")
		On Error &i_errsav
		If !i_err2
			i_nEndB=This.o.activecell.Row
		Endif
		i_nEndB=Iif(Empty(i_nEndB),i_nRow,i_nEndB)
		oUnion= This.o.Union(This.o.Rows(&i_c), This.o.Rows(This.i_nBody+1),This.o.Rows(i_nEndB))
		oUnion.Delete
		Return
	Proc OpenWrk(i_cFile)
		If At('\',i_cFile)=0
			i_cFile=Sys(5)+Sys(2003)+'\'+i_cFile
		Endif
		*wait window "sto aprendo "+i_cFile
		This.o.workbooks.Open(i_cFile)
		This.i_cWrkModel=This.o.ActiveWorkBook.Name
		Return
	Proc CreateModel(i_cModel,i_cCursor,i_oQry, i_NoTot)
		Local i,j,cLetter,cFld,cSheet,bTot,i_nNumberRow
		This.Open('')
		cSheet=This.o.ActiveWorkBook.ActiveSheet.Name
		*** Se provengo da query aggiungo il nome del modello con la descrizione della prima tabella
		If Vartype(i_oQry)='O'
			i_nNumberRow=2
			This.o.Cells(1,1).Value=i_oQry.oPage.Page1.opag.TblTables.List(1,1)
		Else
			i_nNumberRow=1
		Endif
		This.o.ActiveWorkBook.Names.Add("BODY","="+cSheet+"!$A$"+Alltrim(Str(i_nNumberRow+1))+":$A$"+Alltrim(Str(i_nNumberRow+1)))
		This.o.Cells(i_nNumberRow+1,1).Value='BODY'
		* la prima lettera che precede la 'A'
		cLetter=Chr(64) && '@'
		i_cNum=''
		bTot= !m.i_NoTot
		cFld=Field(1)
		If Inlist(Type('&i_cCursor..&cFld'),'N','Y')
			bTot=.F.
		Endif
		j=65
		For i=1 To Fcount()
			If i<=26
				cLetter=Chr(Asc(cLetter)+1)
			Else
				cLetter=Chr(j)+Chr(Mod(i,26)+Iif(Mod(i,26)=0,90,64))
				If Mod(i,26)=0
					j=j+1
				Endif
			Endif
			cFld=Field(i)
			This.o.ActiveWorkBook.Names.Add(Upper(cFld),"="+cSheet+"!$"+cLetter+"$"+Alltrim(Str(i_nNumberRow+2))+":$"+cLetter+"$"+Alltrim(Str(i_nNumberRow+2)))
			*** Cerco di impostare le label delle colonne secondo le descrizioni per avere un modello pi� comprensibile
			If Vartype(i_oQry)='O' And i_oQry.oPage.page2.opag.fdlfields.ListCount>0
				This.o.Cells(i_nNumberRow,i).Value=i_oQry.oPage.page2.opag.fdlfields.List(i,1)
			Else
				This.o.Cells(i_nNumberRow,i).Value=Upper(cFld)
			Endif
			If Inlist(Type('&i_cCursor..&cFld'),'N','Y')
				This.o.Cells(i_nNumberRow+4,i).Value="=sum("+cLetter+Alltrim(Str(i_nNumberRow+2))+":"+cLetter+Alltrim(Str(i_nNumberRow+3))+")"
			Endif
		Next
		This.o.Rows(1).Font.bold=.T.
		*** Definisco il font ingrandito per i dati di testata
		If Vartype(i_oQry)='O'
			This.o.Rows(1).Font.Size=This.o.Rows(1).Font.Size+5
			This.o.Rows(2).Font.bold=.T.
			This.o.Rows(2).interior.Color=i_ThemesManager.GetProp(7)
			This.o.Cells.EntireColumn.AutoFit
		Endif
		This.o.ActiveWorkBook.Names.Add("ENDBODY","="+cSheet+"!$A$"+Alltrim(Str(i_nNumberRow+3))+":$A$"+Alltrim(Str(i_nNumberRow+3)))
		This.o.Cells(i_nNumberRow+3,1).Value='ENDBODY'
		If bTot
			This.o.Cells(i_nNumberRow+4,1).Value=cp_Translate(MSG_TOTAL)
		Endif
		This.Save(i_cModel)
		This.o.ActiveWorkBook.Close()
		Return
	Proc CloseObject()
		Local i_errsav,i
		Private i_err
		i_err=.F.
		i_errsav=On('ERROR')
		On Error i_err=.T.
		For i=1 To This.o.workbooks.Count
			If At("Object",This.o.workbooks(i).Name)<>0
				This.o.workbooks(i).Close
				Exit
			Endif
		Next
		On Error &i_errsav
		Return
Enddefine
