**************************************************
*-- Class Library:  cp_gridex
**************************************************

#INCLUDE "..\vfcsim\win32api.h"
#INCLUDE "cp_app_lang.inc"

#Define PANELVERTICAL_BACKCOLOR	7
#Define TOOLBAR_BORDERCOLOR 55
#Define TOOLBAR_BACKGRD_H 54

**************************************************
*-- Class:        _do_controls
*-- ParentClass:  container
*-- BaseClass:    container
*-- Time Stamp:   05/22/08 04:35:14 PM
*
*
Define Class _do_controls As Container

	Width = 200
	Height = 7
	BorderWidth = 0
	BackColor = Rgb(255,255,230)
	npanel = 1
	nlock = 0
	nwidthset = 4
	nlockwidth = 0
	ldrag = .F.
	mousedownx = 0
	mousedowny = 0
	nrangeminx = 0
	nrangemaxx = 0
	lTheme="5.01" $ Os(1) Or "5.02" $ Os(1)
	Name = "_do_controls"

	Add Object imglock As Image With ;
		Picture = "columnlock.bmp", ;
		Height = 7, ;
		Left = 0, ;
		Top = -1, ;
		Width = 9, ;
		ToolTipText = "", ;
		Name = "ImgLock"

	Add Object shape1 As Shape With ;
		Top = 0, ;
		Left = 60, ;
		Height = 13, ;
		Width = 37, ;
		BorderStyle = 0, ;
		BorderWidth = 0, ;
		DrawMode = 6, ;
		Visible = .F., ;
		Name = "Shape1"

	Add Object imglockdrag As Image With ;
		Picture = "columnlockdrag.bmp", ;
		Height = 7, ;
		Left = 9, ;
		Top = -1, ;
		Visible = .F., ;
		Width = 9, ;
		Name = "ImgLockDrag"

	Procedure lock_mouseup
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.BackStyle = 0
		This.shape1.Visible = .F.
		This.imglockdrag.Visible = .F.
		This.Parent.oRefGrid.Panel = This.npanel

		If This.ldrag
			This.ldrag = .F.
			This.SetWidth(This.nlockwidth)
			This.Parent.HeaderLock(This.nlock)
		Endif
	Endproc

	Procedure lock_mousedown
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.BackStyle = 1
		This.shape1.Visible = .T.
		This.imglock.ZOrder(0)
		This.Width = This.Parent.Width - 1
		This.npanel = This.Parent.npanel
		This.Parent.oRefGrid.Panel = This.Parent.npanel

		This.ldrag = .F.
		This.mousedownx = m.nXCoord
		This.mousedowny = m.nYCoord
		This.nrangeminx = Objtoclient(This, 2)
		This.nrangemaxx = This.nrangeminx + This.Width
	Endproc

	Procedure lock_mousemove
		Lparameters nButton, nShift, nXCoord, nYCoord

		Private loGrid, lnWidth, loColumn, lnLock, lnMinLock
		Local loGrid, lnWidth, loColumn, lnLock, lnMinLock

		This.imglockdrag.Left = Min(Max(m.nXCoord - This.nrangeminx - This.imglockdrag.Width/2, 0), This.Width - This.imglockdrag.Width)
		If Not This.ldrag And Abs(This.mousedownx - m.nXCoord) > 5
			This.ldrag = .T.
			This.imglockdrag.Visible = .T.
			This.imglockdrag.ZOrder(0)
		Endif

		If (Not This.ldrag) Or (Not Between(m.nXCoord, This.nrangeminx, This.nrangemaxx))
			Return
		Endif
		lnMinLock = This.Parent.nMinLock
		loGrid = This.Parent.oRefGrid
		lnLock = 0
		lnWidth = 0
		For lnIndex = loGrid.LeftColumn To loGrid.ColumnCount
			loColumn = This.Parent.GetColumn(m.lnIndex)
			If Not This.Parent.GetColumnVisible(m.loColumn)
				Loop
			Endif
			If This.nrangeminx + m.lnWidth + loColumn.Width + 1 >= m.nXCoord
				Exit For
			Endif
			lnLock = m.lnLock + 1
			lnWidth = lnWidth + loColumn.Width + 1
		Endfor

		If m.lnLock < m.lnMinLock
			This.nlock = m.lnMinLock
			Return
		Endif

		If This.nlock <> m.lnLock ;
				Or This.nlockwidth <> m.lnWidth
			This.nlock = m.lnLock
			This.nlockwidth = m.lnWidth
			This.SetWidth(m.lnWidth)
		Endif
	Endproc

	Procedure SetWidth
		Lparameters tnWidth

		This.nlockwidth = m.tnWidth
		If Not This.ldrag
			This.Width = Max(This.imglock.Width, This.nlockwidth + This.imglock.Width/2)
		Endif

		This.Resize()
	Endproc

	Procedure Init
		This.Width = This.imglock.Width
		This.BackStyle = 0

		This.imglock.ToolTipText = cp_Translate(MSG_ManyHeader_Controls_imgLock)
	Endproc

	Procedure Resize
		This.shape1.Move(0, 0, This.nlockwidth, This.Height)
		If This.nlockwidth > This.Parent.Width
			This.imglock.Move(This.Parent.Width-This.imglock.Width - 1)
		Else
			This.imglock.Move(Iif(This.nlockwidth = 0, 0, This.nlockwidth-This.imglock.Width/2))
		Endif
	Endproc

	Procedure imglock.MouseUp
		Lparameters nButton, nShift, nXCoord, nYCoord

		If m.nButton = 1
			This.Tag = ""
			This.Parent.lock_mouseup(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure imglock.MouseDown
		Lparameters nButton, nShift, nXCoord, nYCoord

		If m.nButton = 1
			This.Tag = "Y"
			This.Parent.lock_mousedown(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure imglock.MouseMove
		Lparameters nButton, nShift, nXCoord, nYCoord

		If m.nButton = 1 And This.Tag = "Y"
			This.Parent.lock_mousemove(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure imglockdrag.MouseMove
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.lock_mousemove(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure imglockdrag.MouseDown
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.lock_mousedown(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure imglockdrag.MouseUp
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.lock_mouseup(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

Enddefine
*
*-- EndDefine: _do_controls
**************************************************

**************************************************
*-- Class:        _do_headerbase
*-- ParentClass:  container
*-- BaseClass:    container
*-- Time Stamp:   07/10/08 05:09:10 PM
*
*
Define Class _do_headerbase As Container

	Width = 200
	Height = 35
	BackStyle = 0
	BorderWidth = 0
	orefup = .Null.
	nmergecount = 0
	ntype = 0
	lmovable = .F.
	lresizable = .F.
	Hidden mousedownx
	mousedownx = 0
	Hidden mousedowny
	mousedowny = 0
	Hidden orefline
	orefline = .Null.
	Hidden orefshadow
	orefshadow = .Null.
	Hidden ndragmin
	ndragmin = 0
	Hidden ndragmax
	ndragmax = 0
	Hidden ldrag
	ldrag = .F.
	linit = .F.
	nalignment = 3
	orefheader = .Null.
	orefform = .Null.
	nheight = -1
	Name = "_do_headerbase"
	Caption = ""
	Dimension arefcolumns[1]
	*** parametri per controllare se necessario effettuare gettxtsize
	oGttnWidth=0
	oGttnHeight=0
	oGtWidth=0
	oGtHeight=0
	oGtCaption=''
	oGtFontName=''
	oGtFontSize=0
	oGtFontBold=.f.
	oGtFontItalic=.f.
	oGtFontUnderline=.f.
	oGtFontStrikethru=.f.
	*
	bInitOnStyle=.f.
	bModifyOnStyle=.f.

	Add Object hmovebar As Container With ;
		Top = 0, ;
		Left = 193, ;
		Width = 4, ;
		Height = 34, ;
		BorderWidth = 0, ;
		Visible = .F., ;
		MousePointer = 9, ;
		BackColor = Rgb(255,128,0), ;
		BackStyle = 0, ;
		Name = "hMoveBar"

	Add Object header1 As Label With ;
		WordWrap = .T., ;
		BackStyle = 0, ;
		Caption = "Header1", ;
		Height = 16, ;
		Left = 75, ;
		Top = 9, ;
		Width = 42, ;
		Name = "Header1"

	Add Object lineleft As Line With ;
		Height = 28, ;
		Left = 12, ;
		Top = 5, ;
		Width = 0, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineLeft"

	Add Object linetop As Line With ;
		Height = 0, ;
		Left = 12, ;
		Top = 5, ;
		Width = 48, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineTop"

	Add Object shape1 As Shape With ;
		Top = 12, ;
		Left = 21, ;
		Height = 13, ;
		Width = 23, ;
		BorderStyle = 0, ;
		BorderWidth = 0, ;
		DrawMode = 6, ;
		MousePointer = 16, ;
		BackStyle = 0, ;		
		Name = "Shape1"

	Add Object linebottom As Line With ;
		BorderWidth = 0, ;
		Height = 0, ;
		Left = 11, ;
		Top = 31, ;
		Width = 48, ;
		Name = "LineBottom"

	Add Object lineright As Line With ;
		Height = 28, ;
		Left = 58, ;
		Top = 6, ;
		Width = 0, ;
		Name = "LineRight"

	Add Object imgtheme As themeimage With ;
		Height = 17, ;
		Left = 169, ;
		Top = 8, ;
		Visible = .F., ;
		Width = 17, ;
		themeclass = "Header", ;
		themeparid = 1, ;
		themestate = 1, ;
		Name = "imgTheme"

	Add Object vmovebar As Container With ;
		Top = 30, ;
		Left = 0, ;
		Width = 12, ;
		Height = 4, ;
		BorderWidth = 0, ;
		Visible = .F., ;
		MousePointer = 7, ;
		BackColor = Rgb(255,128,0), ;
		BackStyle = 0, ;		
		Name = "vMoveBar", ;
		CurrentTop=0, mousedownx=0, mousedowny=0, orefline=0

	Hidden Procedure caption_access
		Return This.header1.Caption
	Endproc

	Hidden Procedure caption_assign
		Lparameters vNewVal

		vNewVal = Strtran(m.vNewVal, "\\", Chr(0))
		vNewVal = Strtran(m.vNewVal, "\n", Chr(13)+Chr(10))
		vNewVal = Strtran(m.vNewVal, Chr(0), "\")

		This.header1.Caption = m.vNewVal
		This.imgtheme.Font.Caption = m.vNewVal

		This.Refresh()
	Endproc

	Hidden Procedure lresizable_assign
		Lparameters vNewVal
		This.lresizable = m.vNewVal
		This.hmovebar.Visible = m.vNewVal
	Endproc

	Procedure Init
		Do Case
			Case Vartype(This.Parent.orefform)="O"
				This.orefform=This.Parent.orefform
			Otherwise
				This.orefform=Thisform
		Endcase
	Endproc

	*-- MouseDown
	Procedure bar_mousedown
		Lparameters nButton, nShift, nXCoord, nYCoord

		Private loGrid, lnIndex, loColumn, loColTmp, lnLock
		Local loGrid, lnIndex, loColumn, loColTmp, lnLock

		loGrid = This.Parent.oRefGrid
		If Not Vartype(m.loGrid) = "O" Or Isnull(m.loGrid)
			Return
		Endif

		This.mousedownx	= m.nXCoord
		This.mousedowny	= m.nYCoord

		Do Case
			Case m.nButton = 1
				If Vartype(This.orefform)="O"
					This.orefform.Newobject("_" + Sys(2015), "Line")
					This.orefline = This.orefform.Controls(This.orefform.ControlCount)

					With This.orefline
						.DrawMode = This.shape1.DrawMode
						.AddProperty("nRangeMinX", Objtoclient(This, 2))
						.AddProperty("nRangeMaxX", Objtoclient(This.Parent, 2) + This.Parent.Width)
						.Move(Objtoclient(This, 2) + This.Width - 1, ;
							ObjToClient(This.Parent, 1), ;
							0, ;
							Max(m.loGrid.Height - Iif(Inlist(m.loGrid.ScrollBars, 1, 3), Sysmetric(8), 0) - 2, 0))
						.Visible = .T.
					Endwith
				Endif

			Case m.nButton = 2
				lnLock = 0
				loColumn = This.Parent.GetColumn(This.Parent.GetGroupMax(This))
				For lnIndex = loGrid.LeftColumn To loColumn.ColumnOrder
					loColTmp = This.Parent.GetColumn(m.lnIndex)
					If loColTmp.Visible And loColTmp.ControlCount > 0 And Objtoclient(loColTmp.Controls(1), 2) > 0
						lnLock = m.lnLock + 1
					Endif
				Endfor
				If This.Parent.nlock <> m.lnLock
					This.Parent.HeaderLock(Max(m.lnLock, This.Parent.nMinLock))
				Else
					This.Parent.HeaderLock(This.Parent.nMinLock)
				Endif
		Endcase
	Endproc

	*-- MouseMove
	Procedure bar_mousemove
		Lparameters nButton, nShift, nXCoord, nYCoord

		*-- MooseMove

		Private lnLeft
		Local lnLeft
		If Not Vartype(This.orefline) = "O" Or Isnull(This.orefline)
			Return
		Endif

		With This.orefline
			lnLeft = Objtoclient(This.hmovebar, 2) - This.mousedownx + m.nXCoord
			.Left = Min(.nrangemaxx, Max(m.lnLeft, .nrangeminx))
			*!*		Do Case
			*!*		Case m.lnLeft < .Min
			*!*			m.lnLeft = .Min
			*!*		Case m.lnLeft > .Max
			*!*			* m.lnLeft = .Max
			*!*		EndCase

			*!*		.Left = m.lnLeft
		Endwith
	Endproc

	*-- MouseUp
	Procedure bar_mouseup
		Lparameters nButton, nShift, nXCoord, nYCoord

		*-- MooseUp

		If Not Isnull(This.orefline)
			If Vartype(This.orefform)="O"
				This.orefform.RemoveObject(This.orefline.Name)
			Endif
			This.orefline = Null
		Endif
		If m.nXCoord <> This.mousedownx
			This.ColumnResizable(m.nXCoord - This.mousedownx)
		Endif
	Endproc

	Procedure ColumnResizable
		Lparameters tnValue

		Private lnCell, loColumn, lnCellWidth, lnMod
		Local lnCell, loColumn, lnCellWidth, lnMod

		lnCell = 0
		*-- Resizable
		For Each loColumn In This.arefcolumns
			lnCell = m.lnCell + Iif(loColumn.Resizable, 1, 0)
		Endfor

		*-- Resizable
		lnCellWidth = Int(m.tnValue/m.lnCell)
		lnMod = Mod(m.tnValue, m.lnCell)
		For Each loColumn In This.arefcolumns
			If loColumn.Resizable
				lnCell = m.lnCell - 1
				If lnCell <> 0
					loColumn.Width = Max(loColumn.Width + m.lnCellWidth, 0)
				Else
					loColumn.Width = Max(loColumn.Width + m.lnCellWidth + m.lnMod, 0)
				Endif
			Endif
		Endfor

		This.Parent.HeaderRedraw()
		If Not Isnull(This.Parent.oRefPanel)
			This.Parent.oRefPanel.HeaderRedraw()
		Endif
	Endproc

	Procedure drag_shadowdestory
		If Not Isnull(This.orefshadow)
			If Vartype(This.orefform)="O"
				This.orefform.RemoveObject(This.orefshadow.Name)
			Endif
		Endif
		This.orefshadow = Null
	Endproc

	*-- RefShadow
	Procedure drag_shadowcreate
		Privat lnScroll, loGrid
		Local lnScroll, loGrid
		loGrid = This.Parent.oRefGrid
		If Isnull(This.orefshadow) And Vartype(This.orefform)="O"
			This.orefform.Newobject("_" + Sys(2015), "Shape")
			This.orefshadow = This.orefform.Controls(This.orefform.ControlCount)
			With This.orefshadow
				lnScroll = Iif(Inlist(loGrid.ScrollBars, 1, 3), Sysmetric(8), 0) + 1
				If This.ntype  = 2
					.Top = Objtoclient(This, 1) + This.Height
					.Height = Max(loGrid.Height - This.Height - m.lnScroll, 0)
				Else
					.Top = Objtoclient(This, 1) + This.Height - 2
					.Height = Max(loGrid.Height - This.Parent.Height - m.lnScroll, 0)
				Endif
				.BorderStyle = 0
				.BorderWidth = 0
				.DrawMode = 6
				.ZOrder(0)
			Endwith
		Endif
		If Not Isnull(This.orefshadow)
			With This.orefshadow
				.Move(Objtoclient(m.This, 2), .Top, m.This.Width)
				.Visible = This.Visible
			Endwith
		Endif
	Endproc

	*-- MouseDown
	Procedure drag_mousedown
		Lparameters nButton, nShift, nXCoord, nYCoord

		*-- MooseDown

		This.mousedownx	= m.nXCoord
		This.mousedowny	= m.nYCoord

		If Isnull(This.orefup)
			This.ndragmin = Objtoclient(This.Parent, 2)
			This.ndragmax = This.ndragmin + This.Parent.Width
		Else
			This.ndragmin = Objtoclient(This.orefup, 2)
			This.ndragmax = This.ndragmin + This.orefup.Width
		Endif

		If m.nButton = 1
			If Not This.Parent.lTheme Or Not This.imgtheme.lThemeDraw
				This.shape1.BackStyle = 1
			Endif
		Endif
	Endproc

	*-- MouseMove
	Procedure drag_mousemove
		Lparameters nButton, nShift, nXCoord, nYCoord
		*-- MooseMove
		Private lnLeft, loBorderHeader, lnMinLock
		Local lnLeft, loBorderHeader, lnMinLock

		If Not This.ldrag And Abs(This.mousedownx - m.nXCoord) > 5
			This.drag_shadowcreate()
			This.ldrag = .T.
		Endif

		If This.ldrag And Between(m.nXCoord, This.ndragmin, This.ndragmax)
			m.lnLeft = Objtoclient(This, 2)
			Do Case
				Case (m.nXCoord < This.mousedownx) And (m.nXCoord < m.lnLeft)
					loBorderHeader = This.Parent.GetHeaderLeft(This)
					If Not Isnull(m.loBorderHeader) ;
							And loBorderHeader.Visible ;
							And m.nXCoord < Objtoclient(m.loBorderHeader, 2) + This.Width
						#If Version(5) > 600
							lnMinLock = This.Parent.nMinLock
							Do Case
								Case loBorderHeader.ntype = 0 And loBorderHeader.arefcolumns[1].ColumnOrder <= m.lnMinLock
									Return
								Case loBorderHeader.ntype = 1 And loBorderHeader.arefcolumns[1].ColumnOrder <= m.lnMinLock
									Return
								Case loBorderHeader.ntype = 2 And This.Parent.GetGroupMin(m.loBorderHeader) <= m.lnMinLock
									Return
							Endcase
						#Endif

						This.mousedownx = m.nXCoord
						This.Parent.HeaderSwap(This, m.loBorderHeader)
						This.drag_shadowcreate()
						This.Parent.OnDrag(This, 0)
					Endif

				Case (m.nXCoord > This.mousedownx) And (m.nXCoord > m.lnLeft + This.Width)
					loBorderHeader = This.Parent.GetHeaderRight(This)
					If Not Isnull(m.loBorderHeader) ;
							And loBorderHeader.Visible ;
							And  m.nXCoord > Objtoclient(m.loBorderHeader, 2) + loBorderHeader.Width - This.Width

						This.mousedownx = m.nXCoord
						This.Parent.HeaderSwap(m.loBorderHeader, This)
						This.drag_shadowcreate()
						This.Parent.OnDrag(This, 1)
					Endif
			Endcase
		Endif
	Endproc

	Procedure drag_mouseup
		Lparameters nButton, nShift, nXCoord, nYCoord
		This.ldrag = .F.
		This.shape1.BackStyle = 0
		This.drag_shadowdestory()
	Endproc

	Procedure headersort
		Lparameters tnSortOrder

		Private loColumn, lnIndex, laColumn[1, 2]
		Local loColumn, lnIndex, laColumn[1, 2]

		If This.ntype = 2
			Dimension laColumn[This.nMergeCount, 2]
			For lnIndex = 1 To This.nmergecount
				loColumn = This.arefcolumns[m.lnIndex]
				laColumn[m.lnIndex, 1] = loColumn
				laColumn[m.lnIndex, 2] = loColumn.ColumnOrder
			Endfor
			Asort(laColumn, 2, -1, Iif(Vartype(m.tnSortOrder) = "N" And m.tnSortOrder <> 0, 1, 0))
			For lnIndex = 1 To This.nmergecount
				This.arefcolumns[m.lnIndex] = laColumn[m.lnIndex, 1]
			Endfor
		Endif
	Endproc

	Procedure onmouseenter
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.onmouseenter(This, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure b_updateheadercaption
		this.b_updateheader('Caption')
	EndProc
	Procedure b_updateheaderForeColor
		this.b_updateheader('ForeColor')
	EndProc
	Procedure b_updateheaderFontName
		this.b_updateheader('FontName')
	EndProc
	Procedure b_updateheaderFontSize
		this.b_updateheader('FontSize')
	EndProc
	Procedure b_updateheaderFontBold
		this.b_updateheader('FontBold')
	EndProc
	Procedure b_updateheaderFontItalic
		this.b_updateheader('FontItalic')
	EndProc
	Procedure b_updateheaderFontStrikethru
		this.b_updateheader('FontStrikethru')
	EndProc	
	Procedure b_updateheaderFontUnderline
		this.b_updateheader('FontUnderline')
	EndProc	
	
	Procedure b_updateheader(cParamChenged)
		Private llModify, lcCaption, laArray
		Local llModify, lcCaption, laArray[1]
		Dimension laArray[1]

		If (Not Isnull(This.orefheader))
			If (Aevents(m.laArray, 0) > 0) And (m.laArray[1] = This.orefheader)
				lcCaption = This.orefheader.Caption
				lcCaption = Strtran(lcCaption, "\\", Chr(0))
				lcCaption = Strtran(lcCaption, "\n", Chr(13)+Chr(10))
				lcCaption = Strtran(lcCaption, Chr(0), "\")
				This.header1.Caption = m.lcCaption
				This.header1.FontName = This.orefheader.FontName
				This.header1.FontSize = This.orefheader.FontSize
				This.header1.ForeColor = This.orefheader.ForeColor
				This.header1.FontBold = This.orefheader.FontBold
				This.header1.FontItalic = This.orefheader.FontItalic
				This.header1.FontStrikethru = This.orefheader.FontStrikethru
				This.header1.FontUnderline = This.orefheader.FontUnderline
			Endif
		Else
			lcCaption = This.header1.Caption
			If Not Chr(13) + Chr(10) $ m.lcCaption
				lcCaption = Strtran(lcCaption, "\\", Chr(0))
				lcCaption = Strtran(lcCaption, "\n", Chr(13)+Chr(10))
				lcCaption = Strtran(lcCaption, Chr(0), "\")
				This.header1.Caption = m.lcCaption
			Endif
		Endif

		llModify = .F.
		With This.imgtheme.Font
			do case
				case cParamChenged='Caption'
					If (Not .Caption == This.header1.Caption)
						llModify = .T.
						.Caption = This.header1.Caption
					Endif
				case cParamChenged='ForeColor'
					If (Not .ForeColor == This.header1.ForeColor)
						llModify = .T.
						.ForeColor = This.header1.ForeColor
					Endif
				case cParamChenged='FontName'
					If (Not .FontName == This.header1.FontName)
						llModify = .T.
						.FontName = This.header1.FontName
					Endif
				case cParamChenged='FontSize'
					If (Not .FontSize = This.header1.FontSize)
						llModify = .T.
						.FontSize = This.header1.FontSize
					Endif
				case cParamChenged='FontBold'
					If (Not .FontBold = This.header1.FontBold)
						llModify = .T.
						.FontBold = This.header1.FontBold
					Endif
				case cParamChenged='FontItalic'
					If (Not .FontItalic = This.header1.FontItalic)
						llModify = .T.
						.FontItalic = This.header1.FontItalic
					Endif
				case cParamChenged='FontStrikethru'
					If (Not .FontStrikethru = This.header1.FontStrikethru)
						llModify = .T.
						.FontStrikethru = This.header1.FontStrikethru
					Endif
				case cParamChenged='FontUnderline'
					If (Not .FontUnderline = This.header1.FontUnderline)
						llModify = .T.
						.FontUnderline = This.header1.FontUnderline
					Endif
			endcase
		Endwith
		If this.bInitOnStyle
			This.bModifyOnStyle=This.bModifyOnStyle or llModify
		Else
			If (llModify or This.bModifyOnStyle)
				This.imgtheme.themeclass = This.imgtheme.themeclass
				This.ResetCaption()
				This.bModifyOnStyle=.f.
			Endif		
		Endif
	Endproc

	Hidden Procedure orefheader_assign
		Lparameters vNewVal
		*To do: Assign
		This.orefheader = m.vNewVal
		If Vartype(m.vNewVal) = "O" And Not Isnull(m.vNewVal)
			Bindevent(m.vNewVal, "Caption", This, "b_UpdateHeaderCaption", 1)
			Bindevent(m.vNewVal, "FontName", This, "b_UpdateHeaderFontName", 1)
			Bindevent(m.vNewVal, "FontSize", This, "b_UpdateHeaderFontSize", 1)
			Bindevent(m.vNewVal, "ForeColor", This, "b_UpdateHeaderForeColor", 1)
			Bindevent(m.vNewVal, "FontBold", This, "b_UpdateHeaderFontBold", 1)
			Bindevent(m.vNewVal, "FontItalic", This, "b_UpdateHeaderFontItalic", 1)
			Bindevent(m.vNewVal, "FontStrikethru", This, "b_UpdateHeaderFontStrikethru", 1)
			Bindevent(m.vNewVal, "FontUnderline", This, "b_UpdateHeaderFontUnderline", 1)
		Endif
	Endproc

	Hidden Procedure gettxtsize
		Lparameters tnWidth, tnHeight

		Private lhDC, lhFont, lhOldFont, lcFontName, lnFontSize, lsRect

		If This.oGtWidth=This.Width and This.oGtHeight=This.Height and This.oGtCaption=This.header1.Caption and  This.oGtFontName=This.header1.FontName and This.oGtFontSize=This.header1.FontSize and  This.oGtFontBold=This.header1.FontBold and This.oGtFontItalic=This.header1.FontItalic and This.oGtFontUnderline=This.header1.FontUnderline and This.oGtFontStrikethru=This.header1.FontStrikethru
			tnWidth = This.oGttnWidth
			tnHeight = This.oGttnHeight
			Return
		Endif
		
*!*			lsRect = BinToC(0, "4rs") + BinToC(0, "4rs")  ;
*!*				+ BinToC(This.Width, "4rs") + BinToC(This.Height, "4rs")

*!*			lhDC = GetDC(_vfp.HWnd)

*!*			lcCaption = This.header1.Caption
*!*			lcFontName = This.header1.FontName
*!*			lnFontSize = This.header1.FontSize
*!*			lnFontSize = -(m.lnFontSize * GetDeviceCaps(m.lhDC, LOGPIXELSX) / 72)
*!*			lhFont = CreateFont (m.lnFontSize, 0, 0, 0, ;
*!*				Iif(This.header1.FontBold, FW_BOLD, 0), ;
*!*				Iif(This.header1.FontItalic, 1, 0), ;
*!*				Iif(This.header1.FontUnderline, 1, 0), ;
*!*				Iif(This.header1.FontStrikethru, 1, 0), ;
*!*				DEFAULT_CHARSET, 0, 0, 0, 0, m.lcFontName)

*!*			lhOldFont = SelectObject(m.lhDC, m.lhFont)

*!*			lsRect = BinToC(0, "4rs") + BinToC(0, "4rs") + BinToC(This.Width, "4rs") + + BinToC(This.Height, "4rs")
*!*			DrawText(m.lhDC, m.lcCaption, Len(m.lcCaption), @m.lsRect, DT_CALCRECT + DT_WORDBREAK)

*!*			lhFont = SelectObject(m.lhDC, m.lhOldFont)
*!*			DeleteObject(m.lhFont)
*!*			tnWidth = CToBin(Substr(m.lsRect, 9, 4), "4rs")+10
*!*			tnHeight = CToBin(Substr(m.lsRect, 13, 4), "4rs")
		local lcFontFlag
		lcFontFlag = Iif(This.header1.FontBold, "B", "")+Iif(This.header1.FontItalic, "I", "")+Iif(This.header1.FontUnderline, "U", "")+Iif(This.header1.FontStrikethru, "-","")
		cp_GetTextSize(@tnWidth, @tnHeight, This.header1.Caption,This.header1.FontName, This.header1.FontSize, lcFontFlag , This.Width)
		
		*--- assegno variabili per velocizzare successivi ingressi
		This.oGttnWidth=tnWidth
		This.oGttnHeight=tnHeight
		This.oGtWidth=This.Width
		This.oGtHeight=This.Height
		This.oGtCaption=This.header1.Caption
		This.oGtFontName=This.header1.FontName
		This.oGtFontSize=This.header1.FontSize
		This.oGtFontBold=This.header1.FontBold
		This.oGtFontItalic=This.header1.FontItalic
		This.oGtFontUnderline=This.header1.FontUnderline
		This.oGtFontStrikethru=This.header1.FontStrikethru
	Endproc

	Hidden Procedure ResetCaption
		Private lnTxtW, lnTxtH, lnTop, lnLeft, lnAlign
		Local lnTxtW, lnTxtH, lnTop, lnLeft, lnAlign

		If This.oGtWidth=This.Width and This.oGtHeight=This.Height and This.oGtCaption=This.header1.Caption and  This.oGtFontName=This.header1.FontName and This.oGtFontSize=This.header1.FontSize and  This.oGtFontBold=This.header1.FontBold and This.oGtFontItalic=This.header1.FontItalic and This.oGtFontUnderline=This.header1.FontUnderline and This.oGtFontStrikethru=This.header1.FontStrikethru
			Return
		Endif
		
		If This.Parent.lTheme And Val(Substr(Os(),8,Len(Os())-7))>=6
			This.imgtheme.Draw()
			Return
		Endif

		Store 0 To lnTop, lnLeft, lnTxtW, lnTxtH, lnAlign
		This.gettxtsize(@m.lnTxtW, @m.lnTxtH)

		Do Case
			Case Inlist(This.nalignment, 0, 1, 2, 3)
				lnTop = Max((This.Height - m.lnTxtH) / 2, 1)
			Case Inlist(This.nalignment, 4, 5, 6)
				lnTop = 1
			Case Inlist(This.nalignment, 7, 8, 9)
				lnTop = Max(This.Height - m.lnTxtH, 1)
		Endcase
		Do Case
			Case Inlist(This.nalignment, 2, 3, 6, 9)
				lnLeft = Max((This.Width - m.lnTxtW) / 2, 1)
				If This.Width > m.lnTxtW
					lnAlign = 2
				Endif
			Case Inlist(This.nalignment, 0, 4, 7)
				lnLeft = 1
			Case Inlist(This.nalignment, 1, 5, 8)
				lnLeft = This.Width - m.lnTxtW
				lnAlign = 1
		Endcase

		This.header1.Alignment = m.lnAlign
		This.header1.Move(m.lnLeft, m.lnTop, m.lnTxtW, m.lnTxtH)
	Endproc

	Procedure Destroy
		If Vartype(This.orefheader) = "O" And Not Isnull(This.orefheader)
			Unbindevent(This.orefheader)
			This.orefheader=Null
		Endif
		If Vartype(This.orefform) = "O" And Not Isnull(This.orefform)
			This.orefform=Null
		Endif
	Endproc

	Procedure MouseWheel
		Lparameters nDirection, nShift, nXCoord, nYCoord

		This.Parent.OnMouseWheel(This, m.nDirection, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure MouseEnter
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.onmouseenter(This, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure MouseDown
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.OnMouseDown(This, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure MouseLeave
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.OnMouseLeave(This, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure RightClick
		This.Parent.OnRightClick(This)
	Endproc

	Procedure Resize
		With This
			.imgtheme.ZOrder(1)
			.imgtheme.Move(0, 0, This.Width, This.Height)

			.hmovebar.Move(.Width - .hmovebar.Width, 0, .hmovebar.Width, .Height)
			.vmovebar.Move(0, This.Height - .vmovebar.Height)

			.linetop.Move(0, 0, Max(.Width - 1, 0), 0)
			.linebottom.Move(0, .Height-1, .Width, 0)
			.lineleft.Move(0, 0, 0, .Height)
			.lineright.Move(.Width - 1, 0, 0, .Height)

			.shape1.Move(0, 0, This.Width, This.Height)
			.shape1.ZOrder(0)

			.hmovebar.ZOrder(0)
			.vmovebar.ZOrder(0)
		Endwith

		If This.linit
			This.ResetCaption()
			This.Parent.OnResize(This)
		Endif
	Endproc

	Procedure Click
		This.Parent.OnClick(This)
	Endproc

	Procedure DblClick
		This.Parent.OnDblClick(This)
	Endproc

	Procedure MouseMove
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.OnMouseMove(This, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure MouseUp
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.OnMouseUp(This, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure MiddleClick
		This.Parent.OnMiddleClick(This)
	Endproc

	Procedure Init
		Bindevent(This.header1, "Caption", This, "b_UpdateHeaderCaption", 1)
		Bindevent(This.header1, "FontName", This, "b_UpdateHeaderFontName", 1)
		Bindevent(This.header1, "FontSize", This, "b_UpdateHeaderFontSize", 1)
		Bindevent(This.header1, "ForeColor", This, "b_UpdateHeaderForeColor", 1)
		Bindevent(This.header1, "FontBold", This, "b_UpdateHeaderFontBold", 1)
		Bindevent(This.header1, "FontItalic", This, "b_UpdateHeaderFontItalic", 1)
		Bindevent(This.header1, "FontStrikethru", This, "b_UpdateHeaderFontStrikethru", 1)
		Bindevent(This.header1, "FontUnderline", This, "b_UpdateHeaderFontUnderline", 1)
	Endproc

	Procedure hmovebar.MouseLeave
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.MouseLeave(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure hmovebar.MouseEnter
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Parent.MouseEnter(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure hmovebar.MouseUp
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.Tag = ""
		If m.nButton = 1
			This.Parent.bar_mouseup(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure hmovebar.MouseMove
		Lparameters nButton, nShift, nXCoord, nYCoord

		If m.nButton = 1 And This.Tag = "Y"
			This.Parent.bar_mousemove(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure hmovebar.MouseDown
		Lparameters nButton, nShift, nXCoord, nYCoord
		Local i_err, bOk
		i_err=On("ERROR")
		On Error bOk=.F.
		This.Tag = "Y"
		This.Parent.bar_mousedown(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		On Error &i_err
	Endproc

	Procedure hmovebar.DblClick
		*-- AutoFit
		Private loColumn, llLockScreen
		Local loColumn, llLockScreen

		If This.Parent.Parent.oRefGrid.AllowAutoColumnFit < 2
			If Vartype(This.Parent.orefform)='O'
				llLockScreen = This.Parent.orefform.LockScreen
				This.Parent.orefform.LockScreen = .T.
			Endif
			For Each loColumn In This.Parent.arefcolumns
				loColumn.AutoFit()
			Endfor
			This.Parent.Parent.HeaderRedraw()
			If Vartype(This.Parent.orefform)='O'
				This.Parent.orefform.LockScreen = m.llLockScreen
			Endif
		Endif
	Endproc

	Procedure shape1.MouseWheel
		Lparameters nDirection, nShift, nXCoord, nYCoord

		This.Parent.MouseWheel(m.nDirection, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure shape1.MouseLeave
		Lparameters nButton, nShift, nXCoord, nYCoord

		If This.Parent.Parent.lTheme
			This.Parent.imgtheme.themestate = 1
			This.Parent.imgtheme.Draw()
		Endif

		This.Parent.MouseLeave(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure shape1.MouseEnter
		Lparameters nButton, nShift, nXCoord, nYCoord

		If This.Parent.Parent.lTheme
			This.Parent.imgtheme.themestate = 2
			This.Parent.imgtheme.Draw()
		Endif

		* This.Parent.OnMouseEnter(m.nButton, m.nShift, m.nXcoord, m.nYcoord)
	Endproc

	Procedure shape1.RightClick
		This.Parent.RightClick()
	Endproc

	Procedure shape1.MouseMove
		Lparameters nButton, nShift, nXCoord, nYCoord

		If m.nButton = 1 And This.Tag = "Y" And This.Parent.lmovable
			This.Parent.drag_mousemove(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif

		This.Parent.MouseMove(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure shape1.MouseDown
		Lparameters nButton, nShift, nXCoord, nYCoord

		If This.Parent.lmovable
			If This.Parent.Parent.lTheme
				This.Parent.imgtheme.themestate = 3
				This.Parent.imgtheme.Draw()
			Endif

			This.Tag = "Y"
			This.Parent.drag_mousedown(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif

		This.Parent.MouseDown(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure shape1.MouseUp
		Lparameters nButton, nShift, nXCoord, nYCoord

		If This.Parent.Parent.lTheme
			This.Parent.imgtheme.themestate = 2
			This.Parent.imgtheme.Draw()
		Endif

		This.Tag = ""
		If This.Parent.lmovable
			This.Parent.drag_mouseup(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif

		This.Parent.MouseUp(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
	Endproc

	Procedure shape1.Click
		This.Parent.Click()
	Endproc

	Procedure shape1.DblClick
		This.Parent.DblClick()
	Endproc

	Procedure shape1.MiddleClick
		This.Parent.MiddleClick()
	Endproc

	Procedure vmovebar.MouseDown
		Lparameters nButton, nShift, nXCoord, nYCoord

		Private loGrid, lnIndex, loColumn, loColTmp, lnLock, loManyH
		Local loGrid, lnIndex, loColumn, loColTmp, lnLock, loManyH

		loGrid = This.Parent.Parent.oRefGrid
		If Not Vartype(m.loGrid) = "O" Or Isnull(m.loGrid)
			Return
		Endif

		loManyH = This.Parent.Parent

		If m.nButton = 1
			This.CurrentTop = Objtoclient(This, 1)
			This.mousedownx	= m.nXCoord
			This.mousedowny	= m.nYCoord

			This.Tag = "Y"
			If Vartype(This.orefform)="O"
				This.orefform.Newobject("_" + Sys(2015), "Line")
				This.orefline = This.orefform.Controls(This.orefform.ControlCount)
			Endif
			If Vartype(This.orefline)="O"
				With This.orefline
					.DrawMode = This.Parent.shape1.DrawMode
					.AddProperty("nRangeMinY", Objtoclient(loManyH, 1))
					.AddProperty("nRangeMaxY", Objtoclient(loManyH, 1) + loManyH.Height)

					.Move(Objtoclient(m.loManyH, 2), ;
						ObjToClient(This, 1) + This.Height / 2, ;
						loManyH.Width, 0)

					.Visible = .T.
				Endwith
			Endif
		Endif
	Endproc

	Procedure vmovebar.MouseMove
		Lparameters nButton, nShift, nXCoord, nYCoord

		Private lnTop
		Local lnTop

		If m.nButton = 1 And This.Tag = "Y"
			If Not Vartype(This.orefline) = "O" Or Isnull(This.orefline)
				Return
			Endif

			With This.orefline
				lnTop = This.CurrentTop - This.mousedowny + m.nYCoord
				.Top = Min(.nRangeMaxY, Max(m.lnTop, .nRangeMinY))
			Endwith
		Endif
	Endproc

	Procedure vmovebar.MouseUp
		Lparameters nButton, nShift, nXCoord, nYCoord

		Private lnHeight, loHeader
		Local lnHeight, loHeader

		This.Tag = ""
		If Not Isnull(This.orefline)
			If Vartype(This.orefform)="O"
				This.orefform.RemoveObject(This.orefform.orefline.Name)
			Endif
			This.orefline = Null
		Endif

		If m.nYCoord <> This.mousedownx
			lnHeight = This.Parent.Height - This.mousedowny + m.nYCoord
			This.Parent.nheight = This.Parent.Height - This.mousedowny + m.nYCoord
			If Bittest(GetKeyState(17), 15)
				For Each loHeader In This.Parent.Parent.Controls
					If loHeader.Class == This.Parent.Class
						loHeader.nheight = m.lnHeight
					Endif
				Endfor
				loHeader = Null
			Else
				This.Parent.nheight = m.lnHeight
			Endif

			This.Parent.Parent.HeaderRedraw()
		Endif
	Endproc

Enddefine
*
*-- EndDefine: _do_headerbase
**************************************************


**************************************************
*-- Class:        _unlockscreen
*-- ParentClass:  timer
*-- BaseClass:    timer
*-- Time Stamp:   04/20/08 02:41:03 PM
*
Define Class _unlockscreen As Timer
	Height = 23
	Width = 23
	Enabled = .F.
	Interval = 50
	Name = "_unlockscreen"
	oThis=.Null.

	Proc Init
		This.oThis=This	
	Endproc
	
	Procedure Timer
		This.Enabled = .F.
		This.Parent.LockScreen = .F.
		This.oThis=.Null.		
	Endproc
Enddefine
*
*-- EndDefine: _unlockscreen
**************************************************


**************************************************
*-- Class:        manyheader
*-- ParentClass:  container
*-- BaseClass:    container
*-- Time Stamp:   07/09/08 10:22:04 AM
*
*
Define Class manyheader As Container

	Width = 200
	Height = 28
	BorderWidth = 0
	Visible = .F.
	nleftoffset = 0
	ntopoffset = 0
	ndeletemark = 8
	nrecordmark = 10
	oRefGrid = .Null.
	nleftcolumn = 0
	Hidden oreftimer
	oreftimer = .Null.
	npanel = 1
	nwhere_out = 0
	lsplitbar = .T.
	nref = -1
	oRefPanel = .Null.
	lmaster = .T.
	Hidden orefcontrol
	orefcontrol = .Null.
	orefform = .Null.
	nlock = 0
	llock = .T.
	Hidden nclicksecond
	nclicksecond = 0
	lalignment = .F.
	nMinLock = 0
	classlibraryroot = ""
	Hidden llockscreen
	llockscreen = .F.
	*-- xp
	lTheme = "5.01" $ Os(1) Or "5.02" $ Os(1)
	*-- call proc handle
	Hidden hproc
	hproc = 0
	wm_code = 0
	*-- Default merge header font name, If the value is equal to empty, Get grid FontName.
	_fontname = ""
	*-- Default merge header font size.If the value is equal to -1, Get grid FontSize
	_fontsize = -1
	*-- Default merge header font Style(Sample: "BIDU")
	_fontstyle = ""
	*-- Default merge header fore color.If the value is equal to -1, Get system color.(Note:When lTheme = .T., this value there is no role.)
	_forecolor = -1
	*-- Default merge header back color.If the value is equal to -1, Get system color.(Note:When lTheme = .T., this value there is no role.)
	_backcolor = -1
	*-- Default merge header align ment.
	_Align = 3
	Name = "manyheader"
	*** Property Zoom Ingtegrate
	lZoomIntegrate=.F.
	*** property fro problem destroy on rightclick
	bRightClick=.F.

	*-- Column[lnIndex]
	Dimension acolumn[1]

	Dimension areflocks[1]

	* --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
	bNoSetAnchorForm = .T.
	* --- Zucchetti Aulla Fine  - Gestione SetAnchorForm

	Procedure mergeheader
		Lparameters tnStart, tnEnd, tcCaption, tnHeight

		Private llError, lnIndex, lnCount, lnOrder, loMergeHeader, loColumn, loHeader, ;
			llResizable, llMovable, lnHeight
		Local llError, lnIndex, lnCount, lnOrder, loMergeHeader, loColumn, loHeader, ;
			llResizable, llMovable, lnHeight

		llError = .T.
		Do Case
			Case Vartype(This.oRefGrid) <> "O" Or Isnull(This.oRefGrid)
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_MergeHeader_1), 16)
			Case Not Vartype(m.tcCaption) = "C"
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_MergeHeader_2), 16)
			Case Not Vartype(m.tnStart) = "N" Or Not Vartype(m.tnEnd) = "N"
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_MergeHeader_3), 16)
			Case m.tnStart > m.tnEnd
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_MergeHeader_4), 16)
			Case Not m.tnStart > 0
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_MergeHeader_5), 16)
			Case m.tnEnd > This.oRefGrid.ColumnCount
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_MergeHeader_6), 16)
			Otherwise
				llError = .F.
		Endcase

		If m.llError
			Return
		Endif

		This.MergeClearTry(tnStart, tnEnd)

		loMergeHeader = Null
		This.Newobject("_" + Sys(2015), "_Do_HeaderBase", This.classlibraryroot)
		loMergeHeader = This.Controls(This.ControlCount)
		lnCount = m.tnEnd - m.tnStart + 1
		loMergeHeader.nheight = Iif(Vartype(m.tnHeight) = "N" And m.tnHeight > 0, m.tnHeight, -1)
		Dimension loMergeHeader.arefcolumns[m.lnCount]

		llResizable = .F.
		llMovable = .F.

		For m.lnIndex = m.lnCount To 1 Step - 1
			loColumn = This.GetColumn(m.lnIndex + m.tnStart - 1)
			llResizable = Iif(loColumn.Resizable, .T., m.llResizable)
			llMovable = Iif(loColumn.Movable, .T., m.llMovable)
			loHeader = This.GetRefHeader(m.loColumn)

			loMergeHeader.arefcolumns[m.lnIndex] = m.loColumn

			If loHeader.ntype = 1
				This.HeaderDel(This.GetRefHeader(m.loColumn))
			Endif
			loHeader.ntype = 1
			loHeader.orefup = m.loMergeHeader
		Endfor

		loHeader = This.GetColumnHeader(loMergeHeader.arefcolumns[1])
		With loMergeHeader
			.Caption = m.tcCaption
			.nalignment = This._Align
			.lresizable = m.llResizable
			.lmovable = m.llMovable
			.ntype = 2
			.vmovebar.Visible = .T.
			.nmergecount = m.lnCount

			If This.lmaster
				This.OnStyle(m.loMergeHeader, m.loHeader)
			Else
				If Not Isnull(This.oRefPanel)
					This.oRefPanel.OnStyle(m.loMergeHeader, This.GetColumnHeader(.arefcolumns[1]))
				Endif
			Endif
			.linit = .T.
		Endwith

		This.HeaderRedraw()
		If This.lmaster And Not Isnull(This.oRefPanel)
			With This.oRefPanel
				.mergeheader(m.tnStart, m.tnEnd, m.tcCaption)
				.GetGridColumn()
				.HeaderRedraw()
			Endwith
		Endif

		Return loMergeHeader
	Endproc

	Procedure ChangeTheme
		With This
			.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
			.BorderColor = i_ThemesManager.GetProp(TOOLBAR_BORDERCOLOR)
			If Val(Substr(Os(),8,Len(Os())-7))>=6
				If i_bGradientBck
					.oBackGround.Picture=i_ThemesManager.GetProp(TOOLBAR_BACKGRD_H)
				Else
					.oBackGround.Visible=.F.
				Endif
			Else
				.Picture=i_ThemesManager.GetProp(TOOLBAR_BACKGRD_H)
			Endif
		Endwith
	Endproc

	Procedure initheader
		Lparameters toGrid, toZoomIntegrate

		Do Case
			Case Vartype(Thisform)="O"
				This.orefform=Thisform
			Case Vartype(i_curform)="O"
				This.orefform=i_curform
		Endcase

		Private llLockScreen, lnIndex, loColumn, loHeader, loColHeader, lnPanel
		Local llLockScreen, lnIndex, loColumn, loHeader, loColHeader, lnPanel

		This.lZoomIntegrate=toZoomIntegrate

		If Not Vartype(m.toGrid) = "O" Or Isnull(m.toGrid) Or Not Upper(m.toGrid.BaseClass) = "GRID"
			cp_ErrorMsg(cp_Translate(MSG_ManyHeader_InitHeader_1), 64)
			Return
		Endif

		If Not toGrid.ColumnCount > 0
			Return
		Endif
		If Not toGrid.Columns(1).ColumnOrder > 0
			Return
		Endif

		Local i_olderr
		i_olderr=On('ERROR')
		On Error =.T.

		If _vfp.StartMode = 0 And Pemstatus(toGrid, "AllowCellSelection", 5)
			If toGrid.AllowCellSelection = .F. And Not Pemstatus(toGrid, "Valid", 0)
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_InitHeader_2), 16)
			Endif
		Endif

		This._fontname = Iif(Empty(This._fontname), toGrid.FontName, This._fontname)
		This._fontsize = Iif(This._fontsize < 0, toGrid.FontSize, This._fontsize)
		This._backcolor = Iif(This._backcolor < 0, GetSysColor(COLOR_BTNFACE), This._backcolor)
		This._forecolor = Iif(This._forecolor < 0, GetSysColor(COLOR_BTNTEXT), This._forecolor)

		#If Not Version(5) > 600
			This.nMinLock = 0
		#Endif

		llLockScreen = This.orefform.LockScreen
		This.orefform.LockScreen = .T.

		If Not Type("this.oRefForm.__UnLockScreen.Name") = "C"
			This.orefform.Newobject("__UnLockScreen", "_UnLockScreen", This.classlibraryroot)
		Endif

		If Type("this.oRefForm.__UnLockScreen.Name") = "C"
			This.oreftimer = This.orefform.__UnLockScreen
		Else
			This.oreftimer = Null
		Endif

		If Not Pemstatus(toGrid, "LockColumns", 5)
			toGrid.AddProperty("LockColumns", 0)
		Endif
		If Not Pemstatus(toGrid, "LockColumnsLeft", 5)
			toGrid.AddProperty("LockColumnsLeft", 0)
		Endif

		lnPanel = toGrid.Panel
		If toGrid.SplitBar
			toGrid.PanelLink = .F.
			toGrid.Panel = 0
			toGrid.ScrollBars = Iif(Inlist(toGrid.ScrollBars, 0, 2), 0, 1)

			toGrid.Panel = m.lnPanel
			toGrid.PanelLink = .T.
		Endif

		This.oRefGrid = m.toGrid
		This.GetGridColumn()

		This.HeaderClear()
		If This.npanel = 1
			toGrid.LockColumns = Max(toGrid.LockColumns, This.nMinLock)
			This.nlock = toGrid.LockColumns
		Else
			toGrid.LockColumnsLeft = Max(toGrid.LockColumnsLeft, This.nMinLock)
			This.nlock = toGrid.LockColumnsLeft
		Endif

		*-- Column
		loColumn = toGrid.Columns(1)
		This.nref = 1
		For m.lnIndex = 1 To toGrid.ColumnCount
			loColumn = toGrid.Columns(This.acolumn[m.lnIndex])
			#If Version(5) > 600
				Bindevent(m.loColumn, "Width", This, "Ev_HeaderRedraw", 1)
			#Endif
			*--
			This.Newobject(loColumn.Name, "_Do_HeaderBase", This.classlibraryroot)
			m.loHeader = This.Controls(This.ControlCount)
			loColHeader = This.GetColumnHeader(m.loColumn)

			m.loHeader.arefcolumns[1] = m.loColumn
			If Vartype(loColumn.aRefHeaders)='O'
				loColumn.aRefHeaders=Null
			Endif
			loColumn.AddProperty("aRefHeaders[1]", Null)
			loColumn.aRefHeaders[This.nRef] = m.loHeader

			m.loHeader.orefheader = m.loColHeader
			m.loHeader.bInitOnStyle=.t.
			
			m.loHeader.Caption = loColHeader.Caption
			m.loHeader.nalignment = Iif(This.lalignment, loColHeader.Alignment, loHeader.nalignment)
			m.loHeader.lresizable = loColumn.Resizable
			m.loHeader.lmovable = loColumn.Movable
			m.loColumn.AddProperty("nIndex", This.acolumn[m.lnIndex])

			If This.lmaster
				This.OnStyle(m.loHeader, m.loColHeader)
			Else
				If Not Isnull(This.oRefPanel)
					This.oRefPanel.OnStyle(m.loHeader, m.loColHeader)
				Endif
			Endif
			m.loHeader.linit = .T.
		Endfor
		This.nleftcolumn = toGrid.LeftColumn

		This.HeaderResize(.t.)
		If This.lmaster
			If toGrid.Partition > 0
				If Isnull(This.oRefPanel)
					This.PanelCreate()
				Endif
				This.oRefPanel.initheader(m.toGrid)
			Else
				This.PanelDestroy()
			Endif
		Endif
		If Val(Substr(Os(),8,Len(Os())-7))>=6
			This.AddObject("oBackGround","image")
			This.oBackGround.Top=0
			This.oBackGround.Left=0
			This.oBackGround.Width=This.Width
			This.oBackGround.Height=This.Height
			This.oBackGround.Anchor=15
			This.oBackGround.Stretch=2
			This.oBackGround.Visible=.T.
			This.oBackGround.ZOrder(1)
		Endif
		This.BindEvents()
		This.ZOrder(0)
		This.ChangeTheme()

		If toGrid.LockColumns<>0
			toGrid.DoScroll(4)
		Endif

		This.Visible = m.toGrid.Visible
		This.orefform.LockScreen = m.llLockScreen

		On Error &i_olderr
	Endproc

	Procedure ev_scrolled
		Lparameters nDirection

		*-- Scrolled

		If Not Vartype(This.oRefGrid) = "O" Or Isnull(This.oRefGrid)
			Return
		Endif

		If Vartype(m.nDirection) = "N" And m.nDirection >= 4
			Do Case
				Case This.oRefGrid.Panel = This.npanel
					This.nleftcolumn = This.GetLeftColumn()
					This.HeaderRedraw()
				Case Not Isnull(This.oRefPanel)
					This.oRefPanel.nleftcolumn = This.oRefPanel.GetLeftColumn()
					This.oRefPanel.HeaderRedraw()
			Endcase
		Endif
	Endproc

	Procedure HeaderRedraw(bNoRefresh, bNolInit)
		Private loLockup, lnIndex, loColumn, loHeader, lnLeft, lnTop, lnWidth, lnHeight, llLockScreen, lnPanel
		Local loLockup, lnIndex, loColumn, loHeader, lnLeft, lnTop, lnWidth, lnHeight, llLockScreen, lnPanel

		If Not Vartype(This.oRefGrid) = "O" Or Isnull(This.oRefGrid)
			Return
		Endif

		If Not Vartype(This.orefform) = "O" Or Isnull(This.orefform)
			Return
		Endif

		Local i_olderr
		i_olderr=On('ERROR')
		On Error =.T.

		llLockScreen = This.orefform.LockScreen
		This.orefform.LockScreen = .T.

		lnPanel = This.oRefGrid.Panel
		This.oRefGrid.Panel = This.npanel

		This.SetAll("Visible", .F., "_Do_HeaderBase")

		lnLeft = 0
		loLockup = Null
		loHeader = Null
		loColumn = Null
		For lnIndex = This.oRefGrid.LeftColumn To This.oRefGrid.ColumnCount
			If Not Isnull(This.orefcontrol)
				If This.npanel = 1
					If This.oRefGrid.LockColumns > 0 ;
							And m.lnIndex = This.oRefGrid.LockColumns + 1
						This.orefcontrol.SetWidth(m.lnLeft)
						lnLeft = m.lnLeft + 1
						If Not Isnull(loHeader) And Not Isnull(loHeader.orefup)
							loLockup = loHeader.orefup
						Endif
					Endif
				Else
					If This.oRefGrid.LockColumnsLeft > 0 And m.lnIndex = This.oRefGrid.LockColumnsLeft + 1
						This.orefcontrol.SetWidth(m.lnLeft)
						lnLeft = m.lnLeft + 1
						If Not Isnull(loHeader) And Not Isnull(loHeader.orefup)
							loLockup = loHeader.orefup
						Endif
					Endif
				Endif
			Endif

			loColumn = This.GetColumn(m.lnIndex)
			If Not This.GetColumnVisible(m.loColumn)
				Loop
			Endif

			loHeader = This.GetRefHeader(m.loColumn)
			lnTop = 0
			lnWidth = loColumn.Width + 1
			lnHeight = This.Height && + 1

			If m.lnLeft + m.lnWidth > This.Width
				lnWidth = This.Width - m.lnLeft + 0
			Endif

			If Vartype(loHeader)="O"
				If Not Isnull(loHeader.orefup)
					If loHeader.orefup.nheight > 0
						lnHeight = Min(m.lnHeight, loHeader.orefup.nheight)
					Else
						lnHeight = Int(m.lnHeight/2)
					Endif

					lnTop = m.lnHeight
					If Not loHeader.orefup.Visible
						*-- Left Width
						loHeader.orefup.Visible = .T.
						loHeader.orefup.Move(m.lnLeft, 0, m.lnWidth, lnHeight)
					Else
						loHeader.orefup.Width = loHeader.orefup.Width + m.lnWidth
						If Not Isnull(m.loLockup) And m.loLockup = loHeader.orefup
							m.loLockup = Null
							loHeader.orefup.Width = loHeader.orefup.Width + 1
						Endif
					Endif
					lnHeight = This.Height - m.lnHeight
					* lnHeight = m.lnHeight + Mod(This.Height, 2)
				Endif
				loHeader.Move(m.lnLeft, m.lnTop, m.lnWidth, m.lnHeight)
				loHeader.Visible = .T.
				lnLeft = m.lnLeft + loHeader.Width
				If not bNolInit
					This.HeaderAssignOrder(loHeader)
					This.HeaderAssignWhere(loHeader)				
				Endif								
			Endif

			If m.lnLeft >= This.Width
				Exit
			Endif
		Endfor

		If Not Isnull(This.orefcontrol)
			This.orefcontrol.Resize()
		Endif
		This.oRefGrid.Panel = m.lnPanel
		If Not bNoRefresh
			This.Refresh()
		Endif
		This.orefform.LockScreen = m.llLockScreen

		On Error &i_olderr
	Endproc

	*-- Assign property objects order
	Procedure HeaderAssignOrder
		Lparameters toHeader

		Local nOrderBy
		If Left(toHeader.arefcolumns(1).ControlSource,17)='cp_MemoFirstLine('
			nOrderBy=Ascan(toHeader.Parent.oRefGrid.Parent.cOrderby, cp_ControlSourceMemo(toHeader.arefcolumns(1).ControlSource), 1, 0, 0, 7)
		Else
			nOrderBy=Ascan(toHeader.Parent.oRefGrid.Parent.cOrderby, toHeader.arefcolumns(1).ControlSource, 1, 0, 0, 7)
		Endif
		If nOrderBy>0
			*** bmp order
			If Vartype(toHeader.oOrderImage)<>"O"
				toHeader.Newobject("oOrderImage", "Image")
			Endif
			If toHeader.Parent.oRefGrid.Parent.bOrderDesc[nOrderBy]
				toHeader.oOrderImage.Picture=cp_GetBtnOrderImageDesc()				
			else
				toHeader.oOrderImage.Picture=cp_GetBtnOrderImageAsc()
			Endif
			With toHeader.Controls(toHeader.ControlCount)
				.BackStyle = 0
				.Picture = toHeader.oOrderImage.Picture
				.Visible = .T.				
			Endwith
			toHeader.oOrderImage.Visible=.T.
			toHeader.oOrderImage.Left = toHeader.Width - toHeader.oOrderImage.Width - 1
			toHeader.oOrderImage.Top = toHeader.Height - toHeader.oOrderImage.Height
			*** number order
			If Vartype(toHeader.oOrderNum)<>"O"
				toHeader.Newobject("oOrderNum", "Label")
			Endif
			With toHeader.oOrderNum
				.Visible=.T.
				.WordWrap = .T.
				.BackStyle = 0
				.Height = 12
				.Name = "oOrderNum"				
				.Left = 0
				.Top = 0
				.Width = 12
				.FontName='Arial'
				.FontSize=6
				.Caption=Alltrim(Str(nOrderBy))
				toHeader.imgtheme.oOrderNum.Caption = .Caption
				.Left = toHeader.oOrderImage.Left+1					
			Endwith
		Else
			*** bmp order
			If Vartype(toHeader.oOrderImage)="O"
				toHeader.RemoveObject("oOrderImage")
			Endif
			*** number order
			If Vartype(toHeader.oOrderNum)="O"
				toHeader.oOrderNum.Visible=.F.
				toHeader.oOrderNum.Caption='0'
			Endif
		Endif
	Endproc

	*-- Assign property objects filter
	Procedure HeaderAssignWhere
		Lparameters toHeader

		If toHeader.Parent.oRefGrid.Parent.nWhere>0
			Local i,nWhere
			m.i=1
			nWhere=0
			Do While m.i<=toHeader.Parent.oRefGrid.Parent.nWhere And nWhere=0
				If Left(toHeader.arefcolumns(1).ControlSource,17)='cp_MemoFirstLine('
					nWhere=Iif(cp_ControlSourceMemo(toHeader.arefcolumns(1).ControlSource) $ toHeader.Parent.oRefGrid.Parent.cWhere[m.i], m.i, 0)
				Else
					nWhere=Iif(toHeader.arefcolumns(1).ControlSource $ toHeader.Parent.oRefGrid.Parent.cWhere[m.i], m.i, 0)
				Endif
				m.i = m.i + 1
			Enddo
			If nWhere>0
				*** bmp filter
				If Vartype(toHeader.oWhereImage)<>"O"
					toHeader.Newobject("oWhereImage", "Image")
				Endif
				toHeader.oWhereImage.Picture=cp_GetBtnWhereImage()
				With toHeader.Controls(toHeader.ControlCount)
					.Picture = toHeader.oWhereImage.Picture
					.Visible = .T.
				Endwith
				toHeader.oWhereImage.Left = 0
				toHeader.oWhereImage.Top = 1				
				toHeader.oWhereImage.Visible=.T.
			Else
				*** bmp filter
				If Vartype(toHeader.oWhereImage)="O"
					toHeader.RemoveObject("oWhereImage")
				Endif
			Endif
		Endif
	Endproc

	*-- MouseDown
	Procedure ev_mousedown
		Lparameters nButton, nShift, nXCoord, nYCoord

		*-- MouseDown

		Private lnWhere_Out
		Local lnWhere_Out

		If Not Vartype(This.oRefGrid) = "O" Or Isnull(This.oRefGrid)
			Return
		Endif

		lnWhere_Out = 0
		This.oRefGrid.GridHitTest(m.nXCoord, m.nYCoord, @lnWhere_Out)
		This.nwhere_out = m.lnWhere_Out

		Do Case
			Case m.lnWhere_Out = 11
				If Second() <= This.nclicksecond + _Dblclick
					If Vartype(This.orefform)="O"
						This.nclicksecond = Iif(This.orefform.LockScreen = .F., -1, -2)
						This.orefform.LockScreen = .T.
					Endif
				Else
					This.nclicksecond = Second()
				Endif

			Case m.lnWhere_Out = 5 Or m.lnWhere_Out = 16
				This.UnLockScreen()
		Endcase
	Endproc

	Hidden Procedure UnLockScreen
		If Vartype(This.oreftimer) = "O" And Not Isnull(This.oreftimer)
			If Vartype(This.orefform)="O"
				This.orefform.LockScreen = .T.
			Endif
			This.oreftimer.Enabled = .T.
		Endif
	Endproc

	*-- AfterRowColChange
	Procedure ev_afterrowcolchange
		Lparameters nColIndex

		Private lnLeftColumn, llLockScreen
		Local lnLeftColumn, llLockScreen

		*-- AfterRowColChange
		If Not Vartype(This.oRefGrid) = "O" Or Isnull(This.oRefGrid)
			Return
		Endif
		If Not Vartype(This.orefform)='O'
			Return
		Endif

		llLockScreen = This.orefform.LockScreen
		This.orefform.LockScreen = .T.

		Do Case
			Case This.oRefGrid.Panel = This.npanel
				lnLeftColumn = This.GetLeftColumn()
				If m.lnLeftColumn <> This.nleftcolumn
					This.nleftcolumn = m.lnLeftColumn
					This.HeaderLockEx()
					This.HeaderRedraw()
				Endif

			Case Not Isnull(This.oRefPanel)
				lnLeftColumn = This.oRefPanel.GetLeftColumn()
				If m.lnLeftColumn <> This.oRefPanel.nleftcolumn
					This.oRefPanel.nleftcolumn = m.lnLeftColumn
					This.oRefPanel.HeaderLockEx()
					This.oRefPanel.HeaderRedraw()
				Endif
		Endcase

		This.orefform.LockScreen = m.llLockScreen
	Endproc

	*-- GridValid
	Procedure ev_valid
		This.UnLockScreen()
	Endproc

	*-- Order Column(lnIndex)
	Procedure GetGridColumn
		*-- Columns(Index) Column. ColumnOrder
		*-- ColumnOrder Index
		*-- ColumnOrder
		*-- Columns(nIndex)

		Private lnIndex, loGrid
		Local lnIndex, loGrid

		loGrid = This.oRefGrid
		If loGrid.ColumnCount > 0
			Dimension This.acolumn[loGrid.ColumnCount]
			For m.lnIndex = 1 To loGrid.ColumnCount
				This.acolumn[m.lnIndex] = loGrid.Columns(m.lnIndex).ColumnOrder
			Endfor
		Else
			Dimension This.acolumn[1]
			This.acolumn = Null
		Endif
	Endproc

	Procedure HeaderClear
		Do While This.ControlCount > 0
			This.RemoveObject(This.Controls(1).Name)
		Enddo
		This.ControlCreate()
	Endproc

	*-- Header
	Procedure GetColumnHeader
		Lparameters toColumn
		* toColumn
		* toColumn.Header

		Private loHeader
		Local loHeader
		For Each m.loHeader In toColumn.Controls
			If Upper(m.loHeader.BaseClass) = "HEADER"
				Return m.loHeader
			Endif
		Endfor

		Return Null
	Endproc

	Procedure GetColumn
		Lparameters tnOrder
		* tnOrder ColumnOrder
		* ColumnOrder Column

		Return This.oRefGrid.Columns(Ascan(This.acolumn, m.tnOrder))
	Endproc

	Procedure OnDrag
		Lparameters toHeader, tnType
		* toHeader
		* tnType
		If Not This.lmaster
			This.oRefPanel.OnDrag(m.toHeader, m.tnType)
		Endif
	Endproc

	Procedure about
		*!*******************************************************
		* Vfp Grid ManyHeader
		* 2.1.0.0
		*!*******************************************************
		***
	Endproc

	*-- Header
	Procedure GetHeaderLeft
	    Lparameters toHeader

	    *-- Visible = .F.
	    * toHeader
	    * NULL toHeader

	    Local loColumn, lnMinOrder, loHeader

	    If Vartype(toHeader.arefcolumns[1])='O'
	        Do Case
	            Case toHeader.ntype = 0
	                If toHeader.arefcolumns[1].ColumnOrder = 1
	                    Return Null
	                Endif

	                loColumn = This.GetColumn(toHeader.arefcolumns[1].ColumnOrder - 1)
	                loHeader = This.GetRefHeader(m.loColumn)
	                If Not Isnull(loHeader.orefup)
	                    Return loHeader.orefup
	                Else
	                    Return m.loHeader
	                Endif

	            Case toHeader.ntype = 1
	                lnMinOrder = This.GetGroupMin(toHeader.orefup)
	                If m.lnMinOrder = toHeader.arefcolumns[1].ColumnOrder
	                    Return Null
	                Else
	                    loColumn = This.GetColumn(toHeader.arefcolumns[1].ColumnOrder - 1)
	                    Return This.GetRefHeader(m.loColumn)
	                Endif

	            Case toHeader.ntype = 2
	                lnMinOrder = This.GetGroupMin(m.toHeader)
	                If m.lnMinOrder = 1
	                    Return Null
	                Else
	                    loColumn = This.GetColumn(m.lnMinOrder - 1)
	                    loHeader = This.GetRefHeader(m.loColumn)
	                    If Not Isnull(loHeader.orefup)
	                        Return loHeader.orefup
	                    Else
	                        Return loHeader
	                    Endif
	                Endif

	            Otherwise
	                cp_ErrorMsg(cp_Translate(MSG_ManyHeader_GetHeaderLeft_1) + Transform(toHeader.ntype) + " ?", 64)
	        Endcase
	    Endif
	    Return Null
	Endproc

	Procedure OnClick
	    Lparameters toHeader

	    * toHeader
	    *-- Click

	    If This.lmaster
	        If Inlist(toHeader.ntype, 0, 1) And Vartype(toHeader.orefheader.Name) = "C"
	            toHeader.orefheader.Click()
	        Endif
	    Else
	        This.oRefPanel.OnClick(m.toHeader)
	    Endif
	Endproc

	*-- Header
	Procedure GetHeaderRight
	    Lparameters toHeader

	    * toHeader

	    Local loColumn, lnMaxOrder, loHeader

	    If Vartype(toHeader.arefcolumns[1])='O'
	        Do Case
	            Case toHeader.ntype = 0
	                If toHeader.arefcolumns[1].ColumnOrder >= This.oRefGrid.ColumnCount
	                    Return Null
	                Endif

	                loColumn = This.GetColumn(toHeader.arefcolumns[1].ColumnOrder + 1)
	                loHeader = This.GetRefHeader(m.loColumn)
	                If Not Isnull(loHeader.orefup)
	                    Return loHeader.orefup
	                Else
	                    Return m.loHeader
	                Endif

	            Case toHeader.ntype = 1

	                lnMaxOrder = This.GetGroupMax(toHeader.orefup)
	                If m.lnMaxOrder = toHeader.arefcolumns[1].ColumnOrder
	                    Return Null
	                Else
	                    loColumn = This.GetColumn(toHeader.arefcolumns[1].ColumnOrder + 1)
	                    loHeader = This.GetRefHeader(m.loColumn)
	                    Return m.loHeader
	                Endif

	            Case toHeader.ntype = 2
	                lnMaxOrder = This.GetGroupMax(m.toHeader)
	                If m.lnMaxOrder >= This.oRefGrid.ColumnCount
	                    Return Null
	                Else
	                    loColumn = This.GetColumn(m.lnMaxOrder + 1)
	                    loHeader = This.GetRefHeader(m.loColumn)
	                    If Not Isnull(loHeader.orefup)
	                        Return loHeader.orefup
	                    Else
	                        Return m.loHeader
	                    Endif
	                Endif

	            Otherwise
	                cp_ErrorMsg(cp_Translate(MSG_ManyHeader_GetHeaderLeft_1) + Transform(toHeader.ntype) + " ?", 64)
	        Endcase
	    Endif
	    Return Null
	Endproc

	*-- Click
	Procedure OnDblClick
		Lparameters toHeader
		i_bNoreMoveDblClick=.T.
		* toHeader
		*-- DbltClick
		If This.lmaster
			If Inlist(toHeader.ntype, 0, 1) And Type("toHeader.oRefHeader.Name") = "C"
				toHeader.orefheader.DblClick()
			Endif
		Else
			This.oRefPanel.OnDblClick(m.toHeader)
		Endif
	Endproc

	*-- RightClick
	Procedure OnRightClick
		Lparameters toHeader
		This.bRightClick=.T.
		* toHeader
		*-- RightClick

		If This.lmaster
			If Inlist(toHeader.ntype, 0, 1) And Type("toHeader.oRefHeader.Name") = "C"
				toHeader.orefheader.RightClick()
			Endif
		Else
			This.oRefPanel.RightClick(m.toHeader)
		Endif
		This.bRightClick=.F.
	Endproc

	Procedure BindEvents
		*-- BindEvents vfp BindEvent

		#If Version(5) > 600
			If Vartype(This.orefform)="O"
				Bindevent(This.orefform, "LostFocus", This, "ev_FormLostFocus")
			Endif

			Bindevent(This.oRefGrid, "AfterRowColChange", This, "Ev_AfterRowColChange")
			Bindevent(This.oRefGrid, "MouseDown", This, "Ev_MouseDown")
			Bindevent(This.oRefGrid, "MouseUp", This, "Ev_MouseUp")
			Bindevent(This.oRefGrid, "KeyPress", This, "Ev_KeyPress")
			Bindevent(This.oRefGrid, "Scrolled", This, "Ev_Scrolled")
			Bindevent(This.oRefGrid, "Valid", This, "Ev_Valid")
			Bindevent(This.oRefGrid, "AutoFit", This, "HeaderRedraw", 1)
			Bindevent(This.oRefGrid, "Resize", This, "HeaderResize")
			Bindevent(This.oRefGrid, "Moved", This, "HeaderResize")

			*-- Events for redraw header
			If Vartype(This.oRefGrid.Parent)="O"
				Bindevent(This.oRefGrid.Parent, "GotFocus", This, "ev_formunlock")
			Endif
			If Vartype(This.oRefGrid.Parent.adv)="O"
				Bindevent(This.oRefGrid.Parent.adv, "GotFocus", This, "ev_formunlock")
			Endif

			*-- 2008.06.17
			Bindevent(This.oRefGrid, "HeaderHeight", This, "Ev_HeaderRedraw", 1)

			Bindevent(This.oRefGrid, "Visible", This, "Ev_Visible", 1)

			*-- vfp bug
			If Upper(This.oRefGrid.Parent.BaseClass) == "CONTAINER"
				Bindevent(This.oRefGrid.Parent, "Resize", This, "Ev_ParentResize", 1)
				Bindevent(This.oRefGrid.Parent, "Moved", This, "HeaderResize")
			Endif
		#Endif
		
	Endproc

	Procedure HeaderResize(bNoInit)
		Private loGrid, lnLeft, lnTop, lnWidth, lnHeight, lnScroll, lnPanel, llLockScreen
		Local loGrid, lnLeft, lnTop, lnWidth, lnHeight, lnScroll, lnPanel, llLockScreen

		If Not Vartype(This.oRefGrid) = "O" Or Isnull(This.oRefGrid)
			This.Visible = .F.
			Return
		Endif

		Local i_olderr
		i_olderr=On('ERROR')
		On Error =.T.

        	If Vartype(This.oRefGrid.Parent.advpage)="O" And Pemstatus(This.oRefGrid.Parent.advpage, "GotFocus", 5)
			Bindevent(This.oRefGrid.Parent.advpage, "GotFocus", This, "ev_formunlock")
		Endif

		llLockScreen = This.orefform.LockScreen
		This.orefform.LockScreen = .T.

		loGrid = This.oRefGrid
		lnLeft = This.nleftoffset + 1
		lnTop = This.ntopoffset + 1

		lnPanel = loGrid.Panel
		If This.npanel = 1
			loGrid.Panel = 1
			lnLeft = lnLeft + loGrid.Partition
			lnWidth = loGrid.Width
		Else
			loGrid.Panel = 0
			lnWidth = loGrid.Partition + 1
		Endif

		lnLeft = lnLeft + Iif(loGrid.RecordMark, This.nrecordmark, 0)
		lnLeft = lnLeft + Iif(loGrid.DeleteMark, This.ndeletemark, 0)
		lnScroll = Iif(loGrid.ScrollBars > 1, Sysmetric(5), 0)

		lnWidth = m.lnWidth - m.lnLeft - m.lnScroll - 1
		lnHeight = loGrid.HeaderHeight - m.lnTop + 1
		*lnScroll = Iif(InList(loGrid.ScrollBars, 1, 3), SysMetric(7), 0)
		If lnHeight > loGrid.Height - m.lnScroll
			lnHeight = Max(0, loGrid.Height - m.lnScroll - 1)
		Endif

		If This.lZoomIntegrate
			This.Move(loGrid.Parent.Left+loGrid.Left + m.lnLeft, loGrid.Parent.Top+loGrid.Top + m.lnTop, Max(m.lnWidth, 0), Max(m.lnHeight, 0))
		Else
			This.Move(loGrid.Left + m.lnLeft, loGrid.Top + m.lnTop, Max(m.lnWidth, 0), Max(m.lnHeight, 0))
		Endif

		If This.lsplitbar And This.lmaster And loGrid.Partition > 0
			This.PanelCreate()
		Endif

		loGrid.Panel = m.lnPanel
		This.HeaderRedraw(.f., bNoInit)
		This.orefform.LockScreen = m.llLockScreen

		On Error &i_olderr
	Endproc

	*-- ColumnOrder
	Procedure GetGroupMax
		Lparameters toHeader

		* toHeader
		* ColumnOrder
		*-- GetColumn(GetGroupMax())

		Private lnMaxOrder, loColumn
		Local lnMaxOrder, loColumn

		lnMaxOrder = 0
		For Each loColumn In toHeader.arefcolumns
			lnMaxOrder = Max(loColumn.ColumnOrder, m.lnMaxOrder)
		Endfor

		Return m.lnMaxOrder
	Endproc

	*-- ColumnOrder
	Procedure GetGroupMin
		Lparameters toHeader

		*-- ColumnOrder
		* toHeader
		* ColumnOrder
		*-- GetColumn(GetGroupMin())

		Private lnMinOrder, loColumn
		Local lnMinOrder, loColumn

		lnMinOrder = This.oRefGrid.ColumnCount
		For Each loColumn In toHeader.arefcolumns
			lnMinOrder = Min(loColumn.ColumnOrder, lnMinOrder)
		Endfor

		Return m.lnMinOrder
	Endproc

	*-- ColumnOrder
	Hidden Procedure MergeClearTry
		Lparameters tnStart, tnEnd


		*-- tnStart tnEnd
		* tnStart 	ColumnOrder
		* tnEnd  	ColumnOrder

		Private loHeaderA, loHeaderB, loHeader, loColumn, lnMinOrder, lnMaxOrder
		Local loHeaderA, loHeaderB, loHeader, loColumn, lnMinOrder, lnMaxOrder

		loColumn = This.GetColumn(m.tnStart)
		loHeaderA = This.GetRefHeader(m.loColumn)
		loColumn = This.GetColumn(m.tnEnd)
		loHeaderB = This.GetRefHeader(m.loColumn)

		Do Case
			Case Isnull(loHeaderA.orefup) And Isnull(loHeaderB.orefup)
				For lnIndex = m.tnStart To m.tnEnd
					This.HeaderDel(m.lnIndex)
				Endfor

			Case Isnull(loHeaderA.orefup) And Not Isnull(loHeaderB.orefup)
				For lnIndex = m.tnStart To m.tnEnd
					This.HeaderDel(m.lnIndex)
				Endfor

			Case Not Isnull(loHeaderA.orefup) And Isnull(loHeaderB.orefup)
				For lnIndex = m.tnEnd To m.tnStart
					This.HeaderDel(m.lnIndex)
				Endfor

			Case loHeaderA.orefup = loHeaderB.orefup
				lnMinOrder = This.GetGroupMin(loHeaderA.orefup)
				lnMaxOrder = This.GetGroupMax(loHeaderA.orefup)

				Do Case
					Case m.lnMinOrder = m.tnStart
						For lnIndex = m.tnStart To m.tnEnd
							This.HeaderDel(m.lnIndex)
						Endfor

					Case m.lnMaxOrder = m.tnEnd
						For lnIndex = m.tnEnd To m.tnStart Step - 1
							This.HeaderDel(m.lnIndex)
						Endfor

					Otherwise
				Endcase

			Case loHeaderA.orefup <> loHeaderB.orefup
				lnMaxOrder = This.GetGroupMax(loHeaderA.orefup)
				For lnIndex = m.lnMaxOrder To m.tnStart Step -1
					This.HeaderDel(m.lnIndex)
				Endfor
				lnMinOrder = This.GetGroupMin(loHeaderB.orefup)
				For lnIndex = m.lnMinOrder To m.tnEnd
					This.HeaderDel(m.lnIndex)
				Endfor
				For lnIndex = m.lnMaxOrder To m.lnMinOrder
					This.HeaderDel(m.lnIndex)
				Endfor
		Endcase
	Endproc

	Hidden Procedure HeaderDel
		Lparameters tvHeader

		Private loHeader, loRefColumn, loRefHeader, lnIndex, loRefup, loColumn, llMovable, llResizable, lnMinOrder, lnMaxOrder
		Local loHeader, loRefColumn, loRefHeader, lnIndex, loRefup, loColumn, llMovable, llResizable, lnMinOrder, lnMaxOrder

		Do Case
			Case Vartype(m.tvHeader) = "N" And Between(m.tvHeader, 1, This.oRefGrid.ColumnCount)
				loHeader = This.GetRefHeader(This.GetColumn(m.tvHeader))
			Case Vartype(m.tvHeader) = "O"
				loHeader = m.tvHeader
			Otherwise
				Return
		Endcase

		Do Case
			Case m.loHeader.ntype = 0

			Case m.loHeader.ntype = 1 And Not Isnull(m.loHeader.orefup)
				loRefup = m.loHeader.orefup
				If Between(loRefup.nmergecount, 1, 2)
					This.HeaderDel(m.loRefup)
				Else
					loRefColumn = loHeader.arefcolumns[1]
					loRefHeader = This.GetRefHeader(m.loRefColumn)
					For lnIndex = 1 To loRefup.nmergecount
						If loRefup.arefcolumns[m.lnIndex] = m.loRefColumn
							Adel(loRefup.arefcolumns, m.lnIndex)
							Exit For
						Endif
					Endfor
					loRefup.nmergecount = loRefup.nmergecount - 1
					Dimension loRefup.arefcolumns[loRefup.nMergeCount]
					loRefHeader.ntype = 0
					loRefHeader.orefup = Null

					*-- Movable, Resizable
					llMoveable = .F.
					llResizable = .F.
					For Each loColumn In loRefup.arefcolumns
						llMovable = Iif(m.loColumn.Movable, .T., m.llMoveable)
						llResizable = Iif(m.loColumn.Movable, .T., m.llResizable)
					Endfor
					loRefup.lmovable = m.llMovable
					loRefup.lresizable = m.llResizable

					lnMinOrder = This.GetGroupMin(m.loRefup)
					lnMaxOrder = This.GetGroupMax(m.loRefup)
					If Between(loRefColumn.ColumnOrder, m.lnMinOrder + 1,  m.lnMaxOrder - 1)
						Do While loRefColumn.ColumnOrder < m.lnMaxOrder
							loRefColumn.ColumnOrder = loRefColumn.ColumnOrder + 1
						Enddo
						This.GetGridColumn()
					Endif
				Endif

			Case loHeader.ntype = 2
				For Each loColumn In loHeader.arefcolumns
					loRefHeader = This.GetRefHeader(m.loColumn)
					loRefHeader.ntype = 0
					loRefHeader.orefup = Null
				Endfor
				This.RemoveObject(loHeader.Name)
		Endcase
	Endproc

	Procedure headerdelete
		Lparameters tvHeader

		* tvHeader ColumnOrder
		Private loColumn, loHeader
		Local loColumn, loHeader

		If This.lmaster And Not Isnull(This.oRefPanel)
			If Vartype(m.tvHeader) = "O"
				If m.tvHeader.ntype = 2
					loColumn = tvHeader.arefcolumns[1]
				Else
					loColumn = tvHeader.arefcolumns[1]
				Endif
				loHeader = loColumn.aRefHeaders[This.oRefPanel.nRef]
				If tvHeader.ntype = 2
					This.oRefPanel.headerdelete(loHeader.orefup)
				Else
					This.oRefPanel.headerdelete(m.loHeader)
				Endif
			Else
				This.oRefPanel.headerdelete(m.tvHeader)
			Endif
			This.oRefPanel.HeaderRedraw()
		Endif

		This.GetGridColumn()
		This.HeaderDel(m.tvHeader)
		This.HeaderRedraw()
	Endproc

	*-- MouseUp
	Procedure ev_mouseup
		Lparameters nButton, nShift, nXCoord, nYCoord

		Private llLokScreen
		Local llLockScreen

		If This.lmaster
			llLockScreen = This.orefform.LockScreen
			This.orefform.LockScreen = .T.

			If Inlist(This.nwhere_out, 5, 11)
				This.HeaderResize()

				If This.oRefGrid.Partition > 0
					If Not Isnull(This.oRefPanel)
						This.oRefPanel.HeaderResize()
					Endif
				Else
					This.PanelDestroy()
				Endif
			Endif

			Do Case
				Case This.nclicksecond >= 0
					This.orefform.LockScreen = m.llLockScreen
				Case This.nclicksecond = -2
					This.orefform.LockScreen = .T.
				Otherwise
					This.orefform.LockScreen = .F.
			Endcase
		Endif
	Endproc

	Procedure OnStyle
		Lparameters toHeader, toColumnHeader
		With toHeader
			.bInitOnStyle=.t.
			If .ntype = 2
				.header1.ForeColor	= This._forecolor
				.header1.FontName	= This._fontname
				.header1.FontSize	= This._fontsize

				.header1.FontBold	= "B" $ This._fontstyle
				.header1.FontItalic = "I" $ This._fontstyle
				.header1.FontStrikethru = "D" $ This._fontstyle
				.bInitOnStyle=.f.
				.header1.FontUnderline = "U" $ This._fontstyle

				If .BackColor # This._backcolor
					.BackStyle		= 1
					.BackColor		= This._backcolor
				Endif
			Else
				.header1.ForeColor	= toColumnHeader.ForeColor
				.header1.FontName	= toColumnHeader.FontName
				.header1.FontSize	= toColumnHeader.FontSize

				.header1.FontBold	= toColumnHeader.FontBold
				.header1.FontItalic = toColumnHeader.FontItalic
				.header1.FontStrikethru = toColumnHeader.FontStrikethru
				.bInitOnStyle=.f.
				.header1.FontUnderline = toColumnHeader.FontUnderline


				If .BackColor # m.toColumnHeader.BackColor
					.BackStyle		= 1
					.BackColor		= m.toColumnHeader.BackColor
				Endif
			Endif

			.imgtheme.nalignment = toHeader.nalignment
		Endwith
		If Val(Substr(Os(),8,Len(Os())-7))<6
			If m.toHeader.ntype = 2
				If TYPE("m.toHeader.Parent.Image1") <> 'O'
					toHeader.Parent.Newobject("Image1", "Image")				
					toHeader.BackStyle = 0
					toHeader.Parent.Image1.Visible = .T.
					toHeader.Parent.Image1.Stretch = 2
					toHeader.Parent.Image1.Height = m.toHeader.Parent.Height
					toHeader.Parent.Image1.Width = m.toHeader.Parent.Width
					toHeader.Parent.Image1.Picture = i_ThemesManager.GetProp(TOOLBAR_BACKGRD_H)
					toHeader.Parent.Image1.ZOrder(1)
				Endif
			Endif
		Else
			This.OnThemeStyle(toHeader)
		Endif
	Endproc

	Procedure GetRefHeader
		Lparameters toColumn

		Return toColumn.aRefHeaders[This.nRef]
	Endproc

	Procedure PanelCreate
		Lparameters toGrid

		Private loHeader, loSubHeader
		Local loHeader, loSubHeader

		If Isnull(This.oRefPanel)
			This.Parent.Newobject(Sys(2015), This.Class, This.classlibraryroot)
			This.oRefPanel = This.Parent.Controls(This.Parent.ControlCount)
			This.oRefPanel.npanel = Iif(This.npanel = 1, 0, 1)
			This.oRefPanel.lsplitbar = .F.
			This.oRefPanel.lmaster = .F.
			This.oRefPanel.lTheme = This.lTheme
			This.oRefPanel.llock = This.llock
			This.oRefPanel.Picture = This.Picture
			This.oRefPanel.oRefPanel = This

			This.oRefPanel.initheader(This.oRefGrid)
			For Each loHeader In This.Controls
				If Upper(loHeader.Class) = Upper("_Do_HeaderBase") ;
						And loHeader.ntype = 2
					loSubHeader = This.oRefPanel.mergeheader( ;
						This.GetGroupMin(m.loHeader), ;
						This.GetGroupMax(m.loHeader), ;
						loHeader.header1.Caption, loHeader.nheight)

					loSubHeader.Name = loHeader.Name
					loSubHeader.ForeColor = loHeader.ForeColor
					loSubHeader.header1.FontName = loHeader.header1.FontName
					loSubHeader.header1.FontSize = loHeader.header1.FontSize
					loSubHeader.header1.FontBold = loHeader.header1.FontBold
					loSubHeader.header1.FontItalic = loHeader.header1.FontItalic
					loSubHeader.header1.FontStrikethru = loHeader.header1.FontStrikethru
					loSubHeader.header1.FontUnderline = loHeader.header1.FontUnderline
				Endif
			Endfor
		Endif
	Endproc

	Procedure visible_assign
		Lparameters vNewVal
		*To do:
		This.Visible = m.vNewVal

		If This.lmaster And Not Isnull(This.oRefPanel)
			This.oRefPanel.Visible = m.vNewVal
		Endif
		If Vartype(This.oRefGrid)='O'
			This.Visible=This.oRefGrid.Parent.Visible
		Endif
	Endproc

	*-- LeftColumn
	Procedure HeaderLock
		Lparameters tnVal
		*-- LeftColumn tnVal
		* tnVal
		* LeftColumn
		* vfp9 LockColumn
		Private laColumns
		Private lnPanel, loGrid, lnLock, lnOrder, loColumn, loHeader, lnIndex, lnLoop, loRightColumn, llLockScreen
		Local Array laColumns[1]
		Local lnPanel, loGrid, lnLock, lnOrder, loColumn, loHeader, lnIndex, lnLoop, loRightColumn, llLockScreen

		If Isnull(This.oRefGrid) Or Vartype(This.oRefGrid) <> "O" Or This.nlock = m.tnVal Or Isnull(This.orefcontrol)
			Return
		Endif

		loGrid = This.oRefGrid
		lnPanel = loGrid.Panel
		loGrid.Panel = This.npanel

		If Not Isnull(This.orefcontrol)
			This.orefcontrol.SetWidth(0)
		Endif

		llLockScreen = This.orefform.LockScreen
		This.orefform.LockScreen = .T.
		lnLock = 1
		lnOrder = 0
		For lnIndex = loGrid.LeftColumn To loGrid.ColumnCount
			loColumn = This.GetColumn(m.lnIndex)
			If Not This.GetColumnVisible(m.loColumn)
				Loop
			Endif

			lnOrder = lnOrder + 1
			Dimension laColumns[m.lnOrder]
			laColumns[m.lnOrder] = m.loColumn
			lnLock = m.lnLock + 1
			If m.lnLock > m.tnVal
				Exit For
			Endif
		Endfor

		If loColumn.ColumnOrder <> loGrid.ColumnCount
			loRightColumn = This.GetColumn(loColumn.ColumnOrder + 1)
		Else
			loRightColumn = Null
		Endif

		loHeader = This.GetRefHeader(m.loColumn)
		If Vartype(This.oRefGrid) = "O" And loHeader.ntype = 1
			loHeader = This.GetHeaderRight(m.loHeader)
			Do While Not Isnull(loHeader)
				lnOrder = m.lnOrder + 1
				Dimension laColumns[m.lnOrder]
				laColumns[m.lnOrder] = loHeader.arefcolumns
				loHeader = This.GetHeaderRight(m.loHeader)
			Enddo
		Endif

		lnLoop = 0
		For lnIndex = 1 To m.lnOrder
			loColumn = laColumns[m.lnIndex]
			Do While loColumn.ColumnOrder > m.lnIndex
				lnLoop = lnLoop + 1
				loColumn.ColumnOrder = loColumn.ColumnOrder - 1
			Enddo
		Endfor
		If This.npanel = 1
			loGrid.LockColumns = Max(m.tnVal, 0)
		Else
			loGrid.LockColumnsLeft = Max(m.tnVal, 0)
		Endif

		*-- ColumnCount
		If Not Isnull(m.loRightColumn) And m.tnVal > 0
			For lnIndex = 1 To loGrid.ColumnCount
				If This.GetColumnVisible(m.loRightColumn)
					Exit For
				Endif
				loGrid.DoScroll(5)
			Endfor
			For lnIndex = 1 To loGrid.ColumnCount
				If Not This.GetColumnVisible(m.loRightColumn)
					Exit For
				Endif
				loGrid.DoScroll(5)
			Endfor
			If Not This.GetColumnVisible(m.loRightColumn)
				loGrid.DoScroll(4)
			Endif
		Endif

		loGrid.Panel = m.lnPanel
		This.GetGridColumn()
		This.HeaderRedraw()

		If Not Isnull(This.oRefPanel)
			This.oRefPanel.GetGridColumn()
			This.oRefPanel.HeaderRedraw()
		Endif

		This.orefform.LockScreen = m.llLockScreen
		This.nlock = m.tnVal
		This.OnLock(m.tnVal)
	Endproc

	*-- Vfp6
	Procedure HeaderLockEx

		Private loColumn
		Local loColumn
		If .F. And This.nlock > 0
			loColumn = This.oRefGrid.Column1
			Do Case
				Case loColumn.ColumnOrder < This.nleftcolumn
					Do While loColumn.ColumnOrder < This.nleftcolumn
						loColumn.ColumnOrder = loColumn.ColumnOrder + 1
					Enddo
					This.GetGridColumn()

				Case loColumn.ColumnOrder > This.nleftcolumn
					Do While loColumn.ColumnOrder > This.nleftcolumn
						loColumn.ColumnOrder = loColumn.ColumnOrder - 1
					Enddo
					This.GetGridColumn()
					*!*			loColumn = This.GetColumn(This.nLeftColumn + 1)
					*!*			loColumn.SetFocus()
			Endcase

		Endif
	Endproc

	*-- Grid.LeftColumn
	Procedure GetLeftColumn
		Private lnIndex, loColumn
		Local lnIndex, loColumn

		Do Case
			Case This.oRefGrid.Panel = 1
				If This.oRefGrid.LockColumns = 0
					Return This.oRefGrid.LeftColumn
				Else
					For lnIndex = This.oRefGrid.LockColumns + 1 To This.oRefGrid.ColumnCount
						loColumn = This.GetColumn(m.lnIndex)
						If This.GetColumnVisible(m.loColumn)
							Return m.lnIndex
						Endif
					Endfor
				Endif
			Case This.oRefGrid.Panel = 0
				If This.oRefGrid.LockColumnsLeft = 0
					Return This.oRefGrid.LeftColumn
				Else
					For lnIndex = This.oRefGrid.LockColumnsLeft + 1 To This.oRefGrid.ColumnCount
						loColumn = This.GetColumn(m.lnIndex)
						If This.GetColumnVisible(m.loColumn)
							Return m.lnIndex
						Endif
					Endfor
				Endif
		Endcase

		Return 0
	Endproc

	Procedure PanelDestroy
		If Not Isnull(This.oRefPanel)
			This.oRefPanel.Destroy()
			This.Parent.RemoveObject(This.oRefPanel.Name)
			This.oRefPanel = Null
		Endif
	Endproc

	Procedure OnLock
		Lparameters tnVal

		If Not This.lmaster
			This.oRefPanel.OnLock(m.tnVal)
		Endif
	Endproc

	Procedure HeaderSwap
		Lparameters toHeaderA, toHeaderB

		*-- toHeader

		Private loColumn, loHeaderA, loHeaderB, lnMinOrderA, lnMaxOrderA, lnMinOrderB, lnMaxOrderB
		Local loColumn, loHeaderA, loHeaderB, lnMinOrderA, lnMaxOrderA, lnMinOrderB, lnMaxOrderB

		loHeaderA = Iif(Isnull(toHeaderA.orefup), toHeaderA, toHeaderA.orefup)
		loHeaderB = Iif(Isnull(toHeaderB.orefup), toHeaderB, toHeaderB.orefup)

		Do Case
			Case toHeaderA.ntype = 2 And toHeaderB.ntype = 1 And toHeaderB.orefup = m.toHeaderA
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_HeaderSwap_1), 16)
			Case toHeaderB.ntype = 2 And toHeaderA.ntype = 1 And toHeaderA.orefup = m.toHeaderA
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_HeaderSwap_1), 16)
			Case toHeaderA.ntype = 0 And toHeaderB.ntype = 0
				toHeaderA.arefcolumns[1].ColumnOrder = toHeaderB.arefcolumns[1].ColumnOrder
			Case toHeaderA.ntype = 1 And toHeaderB.ntype = 1 And toHeaderA.orefup = toHeaderB.orefup
				toHeaderA.arefcolumns[1].ColumnOrder = toHeaderB.arefcolumns[1].ColumnOrder
			Case loHeaderA.ntype = 0 And loHeaderB.ntype = 2
				lnOrder = loHeaderA.arefcolumns[1].ColumnOrder
				lnMinOrderB = This.GetGroupMin(m.loHeaderB)
				lnMaxOrderB = This.GetGroupMax(m.loHeaderB)
				If m.lnOrder < m.lnMinOrderB
					For lnLoop = 1 To m.lnMaxOrderB - m.lnOrder
						loHeaderA.arefcolumns[1].ColumnOrder = loHeaderA.arefcolumns[1].ColumnOrder + 1
					Endfor
					loHeaderB.headersort(0)
					For lnLoop = 1 To m.lnMinOrderB - m.lnOrder - 1
						For Each loColumn In loHeaderB.arefcolumns
							loColumn.ColumnOrder = loColumn.ColumnOrder - 1
						Endfor
					Endfor
				Else
					This.HeaderSwap(m.loHeaderB, m.loHeaderA)
				Endif

			Case loHeaderA.ntype = 2 And loHeaderB.ntype = 0
				lnMinOrderA = This.GetGroupMin(m.loHeaderA)
				lnMaxOrderA = This.GetGroupMax(m.loHeaderA)
				loHeaderB.headersort()
				lnOrder = loHeaderB.arefcolumns[1].ColumnOrder
				If m.lnMaxOrderA < m.lnOrder
					loHeaderA.headersort(1)
					For Each loColumn In loHeaderA.arefcolumns
						For lnLoop = 1 To m.lnOrder - m.lnMaxOrderA
							loColumn.ColumnOrder = loColumn.ColumnOrder + 1
						Endfor
					Endfor
					loHeaderA.headersort(0)
					For lnLoop = 1 To m.lnOrder - m.lnMaxOrderA - 1
						loHeaderB.arefcolumns[1].ColumnOrder = loHeaderB.arefcolumns[1].ColumnOrder - 1
					Endfor
				Else
					This.HeaderSwap(m.loHeaderB, m.loHeaderA)
				Endif


			Case loHeaderA.ntype = 2 And loHeaderB.ntype = 2
				lnMinOrderA = This.GetGroupMin(m.loHeaderA)
				lnMaxOrderA = This.GetGroupMax(m.loHeaderA)
				lnMinOrderB = This.GetGroupMin(m.loHeaderB)
				lnMaxOrderB = This.GetGroupMax(m.loHeaderB)
				If m.lnMinOrderA < m.lnMinOrderB
					loHeaderA.headersort(1)
					For Each loColumn In loHeaderA.arefcolumns
						For lnLoop = 1 To m.lnMaxOrderB - m.lnMaxOrderA
							loColumn.ColumnOrder = loColumn.ColumnOrder + 1
						Endfor
					Endfor
					loHeaderB.headersort(0)
					For Each loColumn In loHeaderB.arefcolumns
						For lnLoop = 1 To m.lnMinOrderB - m.lnMaxOrderA - 1
							loColumn.ColumnOrder = loColumn.ColumnOrder - 1
						Endfor
					Endfor
				Else
					This.HeaderSwap(m.loHeaderB, m.loHeaderA)
				Endif

			Otherwise
				*!*		Set Assert On
				*!*		Assert .F. Message "Can not solve the problems!"
				cp_ErrorMsg(cp_Translate(MSG_ManyHeader_HeaderSwap_2),16)

		Endcase

		This.GetGridColumn()
		This.HeaderRedraw()
		If Not Isnull(This.oRefPanel)
			This.oRefPanel.GetGridColumn()
			This.oRefPanel.HeaderRedraw()
		Endif
	Endproc

	Hidden Procedure HeaderLockCheck
		Lparameters m.tnVal

		Private loCurColumn, loColumn, loHeader, nlock, lnOrder
		Local loCurColumn, loColumn, loHeader, lnLock, lnOrder

		If m.tnVal > 0
			lnLock = 0
			lnOrder = 1
			Do While m.lnLock < m.tnVal And m.lnOrder < This.oRefGrid.ColumnCount
				loCurColumn = This.GetColumn(m.lnOrder)
				loHeader = loCurColumn.aRefHeaders[This.nRef]

				If m.loHeader.ntype = 0 ;
						And Not This.GetColumnVisible(m.loCurColumn)
					m.lnOrder = m.lnOrder + 1
					Loop
				Endif

				If m.loHeader.ntype = 0
					m.lnOrder = m.lnOrder + 1
					m.lnLock = m.lnLock + 1
					Loop
				Endif

				m.loHeader = m.loHeader.orefup
				If Not m.loHeader.Visible
					m.lnOrder = m.lnOrder + m.loHeader.nmergecount
					Loop
				Endif

				m.loHeader.headersort(0)
				For Each loColumn In loHeader.arefcolumns
					If m.lnLock > m.tnVal
						Return .T.
					Endif
					If Not This.GetColumnVisible(m.loColumn)
						Return .F.
					Endif
					m.lnOrder = m.lnOrder + 1
					m.lnLock = m.lnLock + 1
				Endfor
			Enddo
		Endif

		Return .T.
	Endproc

	Hidden Procedure llock_assign
		Lparameters vNewVal
		*To do: Modify this routine for the Assign method
		This.llock = m.vNewVal

		If m.vNewVal
			This.ControlCreate()
		Else
			This.ControlDestory()
		Endif


		If This.lmaster And Not Isnull(This.oRefPanel)
			This.oRefPanel.llock = m.vNewVal
		Endif
	Endproc

	Hidden Procedure ControlCreate
		#If Version(5) > 600
			If This.llock
				This.Newobject(Sys(2015), "_do_Controls", This.classlibraryroot)
				This.orefcontrol = This.Controls(This.ControlCount)
				This.orefcontrol.Move(0, 0)
				This.orefcontrol.Visible = .T.
				If This.ControlCount - 1 > 0
					This.HeaderRedraw()
				Endif
			Endif
		#Endif
	Endproc

	Procedure ControlDestory
		If Not Isnull(This.orefcontrol)
			This.RemoveObject(This.orefcontrol.Name)
			This.orefcontrol = Null
		Endif
	Endproc

	Procedure OnResize
		Lparameters toHeader
		If Val(Substr(Os(),8,Len(Os())-7))<6
			If toHeader.ntype = 2 And Pemstatus(toHeader.Parent, "Image1", 5)
				* toHeader.Image1.Left = toHeader.Header1.Left - toHeader.Image1.Width - 3
				*toHeader.Image1.Left = (toHeader.Width - Txtwidth(toHeader.Caption) * 6.0006)/2 - toHeader.Image1.Width
				*toHeader.Image1.Top = (toHeader.Height- toHeader.Image1.Height) / 2
				toHeader.Parent.Image1.Height = toHeader.Parent.height
				toHeader.Parent.Image1.Width = toHeader.Parent.Width
			Endif
		Else
			If Not This.lmaster And Not Isnull(This.oRefPanel)
				This.oRefPanel.OnResize(m.toHeader)
			Endif
		Endif

		This.HeaderAssignOrder(toHeader)
		This.HeaderAssignWhere(toHeader)
	Endproc

	Procedure OnMouseLeave
		Lparameters toHeader, nButton, nShift, nXCoord, nYCoord

		If This.lmaster
			If Inlist(toHeader.ntype, 0, 1) And Type("toHeader.oRefHeader.Name") = "C"
				toHeader.orefheader.MouseLeave(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
			Endif
		Else
			This.oRefPanel.OnMouseLeave(m.toHeader, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure onmouseenter
		Lparameters toHeader, nButton, nShift, nXCoord, nYCoord

		If This.lmaster
			If Inlist(toHeader.ntype, 0, 1) And Type("toHeader.oRefHeader.Name") = "C"
				toHeader.orefheader.MouseEnter(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
			Endif
		Else
			This.oRefPanel.onmouseenter(m.toHeader, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure ev_keypress
		Lparameters nKeyCode, nShiftAltCtrl

		If Not Isnull(This.oRefGrid) And Not This.oRefGrid.AllowCellSelection
			This.UnLockScreen()
		Endif
	Endproc

	*-- Vfp9 sp2
	Procedure GetColumnVisible
		Lparameters toColumn

		*--
		*-- vfp ObjToClient(m.toColumn, 2) = 0
		*-- vfp sp2 ObjToClient(m.toColumn, 2) = ObjToClient(m.Grid, 2)
		*--

		If toColumn.Visible
			If m.toColumn.ControlCount > 0
				Return Objtoclient(m.toColumn.Controls(1), 2) > 0
			Else
				Return Not Objtoclient(m.toColumn, 2) = Objtoclient(m.toColumn.Parent, 2)
			Endif
		Else
			#If Not Version(5) > 600
				Return .T.
			#Else
				Return .F.
			#Endif
		Endif
	Endproc

	Procedure ev_formlostfocus
		Private llLockScreen
		Local llLockScreen
		If Vartype(This.orefform)="O"
			llLockScreen = This.orefform.LockScreen
			This.orefform.LockScreen = .T.
		Endif
		If Not Isnull(This.oRefGrid) And Vartype(This.oRefGrid) = "O"
			This.HeaderRedraw()
		Endif
		If Vartype(This.orefform)="O"
			This.orefform.LockScreen = .F.
		Endif
	Endproc

	Procedure ev_formunlock
		If Vartype(This.orefform)="O"
			This.orefform.LockScreen = .F.
		Endif
	Endproc

	Procedure classlibraryroot_access
		*To do: Access
		If Empty(This.classlibraryroot)
			Private lcClass, lcClassLibrary
			Local lcClass, lcClassLibrary
			lcClass = This.Class
			lcClassLibrary = This.ClassLibrary
			This.classlibraryroot = m.lcClassLibrary

			Do While Not Empty(m.lcClassLibrary)
				lcClass = Getpem(m.lcClass, "ParentClass")
				lcClassLibrary = Getpem(m.lcClass, "ClassLibrary")
				If Not Empty(m.lcClassLibrary)
					This.classlibraryroot = m.lcClassLibrary
				Endif
			Enddo
		Endif
		Return This.classlibraryroot
	Endproc

	Hidden Procedure ev_visible
		If This.lmaster
			If Vartype(This.oRefGrid) = "O" And Not Isnull(This.oRefGrid)
				If This.oRefGrid.Visible = .F.
					This.Visible = .F.
				Else
					This.HeaderRedraw()
					This.Visible = .T.
					This.ZOrder(0)
				Endif
			Endif

			This.orefform.LockScreen = This.llockscreen
		Endif
	Endproc

	*-- _Assign
	Hidden Procedure ev_lockscreenbefore
		If This.lmaster
			This.llockscreen = This.orefform.LockScreen
			This.orefform.LockScreen = .T.
		Endif
	Endproc

	Hidden Procedure ev_headerredraw
		If This.lmaster And Vartype(This.oRefGrid) = "O" And Not Isnull(This.oRefGrid)
			This.Visible = (This.oRefGrid.Visible And This.oRefGrid.HeaderHeight > 0)
			This.HeaderResize()
			If Not Isnull(This.oRefPanel)
				This.oRefPanel.HeaderResize()
			Endif
		Endif

		This.orefform.LockScreen = This.llockscreen
	Endproc

	Hidden Procedure ev_parentresize
		If Vartype(This.oRefGrid) = "O" And Not Isnull(This.oRefGrid)
			If This.oRefGrid.Parent.Height = 0
				This.Visible = .F.
			Else
				This.Visible = This.oRefGrid.Visible
			Endif
		Endif

		This.orefform.LockScreen = This.llockscreen
	Endproc

	*-- xp
	Procedure callwindowproc
		Lparameters thWnd, tnMsg, twParam, tlParam

		If m.tnMsg = WM_THEMECHANGED And This.lTheme
			For Each loHeader In This.Controls
				If Upper(loHeader.Class) == Upper("_Do_HeaderBase")
					loHeader.imgtheme.themeclass = loHeader.imgtheme.themeclass
					This.OnThemeStyle(loHeader)
				Endif
			Endfor
		Endif

		Return .T.
	Endproc

	*-- Theme
	Procedure OnThemeStyle
		Lparameters toHeader
		If This.lTheme
			toHeader.imgtheme.Draw()
			Store toHeader.imgtheme.lThemeDraw To ;
				toHeader.imgtheme.Visible

			Store (Not toHeader.imgtheme.lThemeDraw) To ;
				toHeader.lineleft.Visible, ;
				toHeader.linetop.Visible, ;
				toHeader.lineright.Visible, ;
				toHeader.linebottom.Visible, ;
				toHeader.header1.Visible
		Endif
	Endproc

	Procedure OnMouseMove
		Lparameters toHeader, nButton, nShift, nXCoord, nYCoord

		If This.lmaster
			If Inlist(toHeader.ntype, 0, 1) ;
					And Type("toHeader.oRefHeader.Name") = "C"
				toHeader.orefheader.MouseMove(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
			Endif
		Else
			This.oRefPanel.OnMouseMove(m.toHeader, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure OnMouseDown
		Lparameters toHeader, nButton, nShift, nXCoord, nYCoord

		If This.lmaster
			If Inlist(toHeader.ntype, 0, 1) ;
					And Type("toHeader.oRefHeader.Name") = "C"
				toHeader.orefheader.MouseDown(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
			Endif
		Else
			This.oRefPanel.OnMouseDown(m.toHeader, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure OnMouseUp
		Lparameters toHeader, nButton, nShift, nXCoord, nYCoord

		If This.lmaster
			If Inlist(toHeader.ntype, 0, 1) ;
					And Type("toHeader.oRefHeader.Name") = "C"
				toHeader.orefheader.MouseUp(m.nButton, m.nShift, m.nXCoord, m.nYCoord)
			Endif
		Else
			This.oRefPanel.OnMouseUp(m.toHeader, m.nButton, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure OnMiddleClick
		Lparameters toHeader

		If This.lmaster
			If Inlist(toHeader.ntype, 0, 1) ;
					And Type("toHeader.oRefHeader.Name") = "C"
				toHeader.orefheader.MiddleClick()
			Endif
		Else
			This.oRefPanel.OnMiddleClick(m.toHeader)
		Endif
	Endproc

	Procedure OnMouseWheel
		Lparameters toHeader, nDirection, nShift, nXCoord, nYCoord

		If This.lmaster
			If Inlist(toHeader.ntype, 0, 1) ;
					And Type("toHeader.oRefHeader.Name") = "C"
				toHeader.orefheader.MouseWheel(m.nDirection, m.nShift, m.nXCoord, m.nYCoord)
			Endif
		Else
			This.oRefPanel.OnMouseWheel(m.toHeader, m.nDirection, m.nShift, m.nXCoord, m.nYCoord)
		Endif
	Endproc

	Procedure Init
		*!*	case 3.10
		*!*	ls_WinVer = "Windows 3.x"
		*!*	case 4
		*!*	ls_WinVer = "Windows NT 4.0"
		*!*	case 4.10
		*!*	ls_WinVer = "Windows 98"
		*!*	case 5
		*!*	ls_WinVer = "Windows 2000"
		*!*	case 5.01
		*!*	ls_WinVer = "Windows XP"
		*!*	case 5.02
		*!*	ls_WinVer = "Windows 2003"

		This.orefform=Thisform
	Endproc

	Procedure Destroy
		Local i, loColumn, lnRef, lnMax
		If This.bRightClick
			Nodefault
			This.bRightClick=.F.
			Return
		Endif

		If Type("this.oRefForm.Wm_ThemeChanged_Proxy.Name") = "C"
			Try
				For m.i=1 To This.orefform.Wm_ThemeChanged_Proxy.Count
					This.orefform.Wm_ThemeChanged_Proxy.Remove(m.i)
				Endfor
			Catch
			Endtry
		Endif

		If Not Isnull(This.oRefGrid) And Vartype(This.oRefGrid) = "O"
			If Vartype(This.orefform)="O"
				Unbindevent(This.orefform, "LostFocus", This, "ev_FormLostFocus")
			Endif

			Unbindevent(This.oRefGrid, "AfterRowColChange", This, "Ev_AfterRowColChange")
			Unbindevent(This.oRefGrid, "MouseDown", This, "Ev_MouseDown")
			Unbindevent(This.oRefGrid, "MouseUp", This, "Ev_MouseUp")
			Unbindevent(This.oRefGrid, "KeyPress", This, "Ev_KeyPress")
			Unbindevent(This.oRefGrid, "Scrolled", This, "Ev_Scrolled")
			Unbindevent(This.oRefGrid, "Valid", This, "Ev_Valid")
			Unbindevent(This.oRefGrid, "Resize", This, "HeaderResize")
			Unbindevent(This.oRefGrid, "Moved", This, "HeaderResize")
			Unbindevent(This.oRefGrid, "AutoFit", This, "HeaderRedraw")

			*-- Events for redraw header
			If Vartype(This.oRefGrid.Parent)="O"
				Unbindevent(This.oRefGrid.Parent, "GotFocus", This, "ev_formunlock")
			Endif
			If Vartype(This.oRefGrid.Parent.adv)="O"
				Unbindevent(This.oRefGrid.Parent.adv, "GotFocus", This, "ev_formunlock")
			Endif
            		If Vartype(This.oRefGrid.Parent.advpage)="O" And Pemstatus(This.oRefGrid.Parent.advpage, "GotFocus", 5)
				Unbindevent(This.oRefGrid.Parent.advpage, "GotFocus", This, "ev_formunlock")
			Endif

			Unbindevent(This.oRefGrid, "HeaderHeight", This, "Ev_HeaderRedraw")

			Unbindevent(This.oRefGrid, "Visible", This, "Ev_Visible")

			If Upper(This.oRefGrid.Parent.BaseClass) = "CONTAINER"
				Unbindevent(This.oRefGrid.Parent, "Resize", This, "Ev_ParentResize")
				Unbindevent(This.oRefGrid.Parent, "Moved", This, "HeaderResize")
			Endif

			lnRef = This.nref
			lnMax = 0
			For Each loColumn In This.oRefGrid.Columns
				If m.lnMax = 0 And Vartype(loColumn.aRefHeaders)="O"
					lnMax = Alen(loColumn.aRefHeaders) - 1
				Endif
				Unbindevent(m.loColumn, "Width", This, "Ev_HeaderRedraw")
				If Vartype(loColumn.aRefHeaders)="O"
					If m.lnRef = m.lnMax
						Adel(loColumn.aRefHeaders, m.lnRef)
						Dimension loColumn.aRefHeaders[m.lnMax]
					Else
						loColumn.aRefHeaders[m.lnRef] = Null
					Endif
				Endif
			Endfor

			This.oRefGrid = Null
		Endif
		This.oreftimer = Null
		This.orefform = Null
	Endproc

	Procedure Refresh
		If Not Isnull(This.orefcontrol)
			This.orefcontrol.ZOrder(0)
		Endif
	Endproc
Enddefine
*
*-- EndDefine: manyheader
**************************************************


**************************************************
*-- Class:        themeimage
*-- ParentClass:  image
*-- BaseClass:    image
*-- Time Stamp:   07/09/08 07:08:08 PM
*
*
Define Class themeimage As Image


	Height = 17
	Width = 100
	*-- Theme Class Name��
	themeclass = ""
	*-- Theme Parid
	themeparid = 0
	*-- Theme State
	themestate = 0
	lcustomw = .T.
	lcustomh = .T.
	Hidden nbeforew
	nbeforew = -1
	Hidden nbeforeh
	nbeforeh = -1
	Hidden nbeforestate
	nbeforestate = -1
	cfontname = ""
	nalignment = 0
	lThemeDraw = .F.
	Font = .Null.
	oOrderNum = .Null.
	Name = "themeimage"


	Procedure Draw
		*** draw controls
		Private lhWnd, lhTheme, lhDC, lhMemDC, lhOldBmp, lpMemory, ;
			lsRect, lsSize, lsBitmapInfo, lsBitmapFile, lsFontRect, ;
			lnBmpH, lnBmpW, lnBits, lnPaletteSize, lnBmpFSize, lcBmpValue, ;
			lcCaption, lcOrderNumCaption, lnFontSize, lhFont, lhOldFont, lnTxtW, lnTxtH, lnTop, lnLeft, lnAlign

		Local lhWnd, lhTheme, lhDC, lhMemDC, lhOldBmp, lpMemory, ;
			lsRect, lsSize, lsBitmapInfo, lsBitmapFile, lsFontRect, ;
			lnBmpH, lnBmpW, lnBits, lnPaletteSize, lnBmpFSize, lcBmpValue, ;
			lcCaption, lcOrderNumCaption, lnFontSize, lhFont, lhOldFont, lnTxtW, lnTxtH, lnTop, lnLeft, lnAlign

		If Not This.Parent.Parent.lTheme
			Return
		Endif
		If This.nbeforew = This.Width And This.nbeforeh = This.Height And This.nbeforestate = This.themestate
			Return
		Endif
		If This.Width = 0 Or This.Height = 0
			Return
		Endif

		* This.PictureVal = ""
		This.lThemeDraw = .F.
		If Empty(This.themeclass)
			Return
		Endif

		lhWnd = 0
		lhTheme = OpenThemeData(0, Strconv(This.themeclass + Chr(0), 5))
		If m.lhTheme = 0
			This.PictureVal = ""
			Return
		Endif

		lhDC = GetDC(m.lhWnd)
		If m.lhDC = 0
			Return
		Endif

		lsRect = Replicate(Chr(0), RECT_SIZE)
		lsSize = Replicate(Chr(0), POINT_SIZE)
		GetThemePartSize(m.lhTheme, m.lhDC, This.themeparid, This.themestate, @m.lsRect, TS_TRUE, @m.lsSize)
		lnBmpW = CToBin(Substr(m.lsSize, 1, 4), "4rs")
		lnBmpH = CToBin(Substr(m.lsSize, 5, 4), "4rs")

		lnBmpW = Max(Iif(This.lcustomw, This.Width, m.lnBmpW), 1)
		lnBmpH = Max(Iif(This.lcustomh, This.Height, m.lnBmpH), 1)

		*--
		lhMemDC = CreateCompatibleDC(m.lhDC)
		lhBitmap = CreateCompatibleBitmap(m.lhDC, m.lnBmpW, m.lnBmpH)
		lhOldBmp = SelectObject(m.lhMemDC, m.lhBitmap)

		lsRect = BinToC(0, "4rs") + BinToC(0, "4rs") + BinToC(m.lnBmpW, "4rs") + BinToC(m.lnBmpH, "4rs")
		DrawThemeParentBackground(m.lhWnd, m.lhMemDC, @m.lsRect)
		DrawThemeBackground(m.lhTheme, m.lhMemDC, This.themeparid, This.themestate, @m.lsRect, @m.lsRect)

		* --- Create text caption
		lnFontSize = -(This.Font.FontSize * GetDeviceCaps(m.lhMemDC, LOGPIXELSX) / 72)
		lhFont = CreateFont (m.lnFontSize, 0, 0, 0, ; &&
		Iif(This.Font.FontBold, FW_BOLD, 0), ;
			Iif(This.Font.FontItalic, 1, 0), ;
			Iif(This.Font.FontUnderline, 1, 0), ;
			Iif(This.Font.FontStrikethru, 1, 0), ;
			DEFAULT_CHARSET, 0, 0, 0, 0, This.Font.FontName)

		lhOldFont = SelectObject(m.lhMemDC, m.lhFont)

		lcCaption = This.Font.Caption
		*!*	lcCaption = StrTran(m.lcCaption, "\\", Chr(0))
		*!*	lcCaption = StrTran(m.lcCaption, "\n", CRLF)
		*!*	lcCaption = StrTran(m.lcCaption, Chr(0), "\")
		lcCaption = Strconv(m.lcCaption, 5) + Chr(0)

		lsFontRect = Replicate(Chr(0), 16)
		GetThemeTextExtent(m.lhTheme, m.lhMemDC, This.themeparid, This.themestate, m.lcCaption, -1, DT_WORDBREAK + DT_CENTER, @m.lsRect, @m.lsFontRect)
		lnTxtW = CToBin(Substr(lsFontRect, 9, 4), "4rs")
		lnTxtH = CToBin(Substr(lsFontRect, 13, 4), "4rs")

		lnTop = 0
		lnLeft = 0
		lnAlign = DT_WORDBREAK

		Do Case
			Case Inlist(This.nalignment, 0, 1, 2, 3)
				lnTop = Max((m.lnBmpH - m.lnTxtH) / 2, 0)
			Case Inlist(This.nalignment, 4, 5, 6)
				lnTop = 0
			Case Inlist(This.nalignment, 7, 8, 9)
				lnTop = Max(m.lnBmpH - m.lnTxtH, 0)
		Endcase
		Do Case
			Case Inlist(This.nalignment, 2, 3, 6, 9)
				lnLeft = Max((m.lnBmpW - m.lnTxtW) / 2, 0)
				If m.lnBmpW > m.lnTxtW
					lnAlign = m.lnAlign + DT_CENTER
				Endif
			Case Inlist(This.nalignment, 0, 4, 7)
				lnLeft = 0
			Case Inlist(This.nalignment, 1, 5, 8)
				lnLeft = m.lnBmpW - m.lnTxtW
				lnAlign = m.lnAlign + DT_RIGHT
		Endcase


		m.lsRect = BinToC(m.lnLeft, "4rs") + BinToC(m.lnTop, "4rs");
			+ BinToC(m.lnLeft + m.lnTxtW, "4rs") + BinToC(m.lnTop + m.lnTxtH, "4rs")
		DrawThemeText(m.lhTheme, m.lhMemDC, This.themeparid, This.themestate, m.lcCaption, -1, m.lnAlign, 0, m.lsRect)

		* --- Create text order
		If vartype(This.oOrderNum)='O'
			lhOrderNum = CreateFont (6, 0, 0, 0, 0, 0, 0, 0, DEFAULT_CHARSET, 0, 0, 0, 0, This.Font.FontName)

			lcOrderNumCaption = This.oOrderNum.Caption

			lsFontRect = Replicate(Chr(0), 16)
			GetThemeTextExtent(m.lhTheme, m.lhMemDC, This.themeparid, This.themestate, m.lcOrderNumCaption, -1, DT_WORDBREAK + DT_CENTER, @m.lsRect, @m.lsFontRect)
			lnTxtW = CToBin(Substr(lsFontRect, 9, 4), "4rs")
			lnTxtH = CToBin(Substr(lsFontRect, 5, 4), "4rs")

			lnTop = 0
			lnLeft = This.oOrderNum.Left
			lnAlign = DT_WORDBREAK

			m.lsRect = BinToC(m.lnLeft, "4rs") + BinToC(m.lnTop, "4rs");
				+ BinToC(m.lnLeft + m.lnTxtW, "4rs") + BinToC(m.lnTop + m.lnTxtH, "4rs")
			DrawThemeText(m.lhTheme, m.lhMemDC, This.themeparid, This.themestate, m.lcOrderNumCaption, -1, 5, 0, m.lsRect)

			lhFont = SelectObject(m.lhMemDC, m.lhOldFont)
			DeleteObject(m.lhFont)
		Endif
		
		*--
		lnBits = GetDeviceCaps(m.lhDC, BITSPIXEL) * GetDeviceCaps(m.lhDC, PLANES)
		lnBmpFSize = Int(((m.lnBmpW * m.lnBits + 31)/32) * 4 * m.lnBmpH)

		m.lnPaletteSize = Iif(m.lnBits <= 8, (m.lnBits^2) * 4, 0)
		lsBitmapFile = "BM";						&& WORD bfType
		+ BinToC(m.lnBmpFSize, "4rs") ;			&& DWORD bfSize
		+ BinToC(0, "2rs") ;					&& WORD bfReserved1
		+ BinToC(0, "2rs") ;					&& WORD bfReserved2
		+ BinToC(54 + m.lnPaletteSize, "4rs")	&& DWORD bfOffBits BITMAPFILEHEADER + BITMAPINFOHEADER

		lsBitmapInfo = BinToC(40 ,"4rs") ;			&& DWORD biSize BITMAPINFOHEADER
		+ BinToC(m.lnBmpW,"4rs") ;				&& LONG biWidth
		+ BinToC(m.lnBmpH, "4rs") ;				&& LONG biHeight
		+ Chr(Mod(1, 256)) + Chr(Int(1/256)) ;	&& WORD biPlanes
		+ Chr(Mod(m.lnBits, 256)) + Chr(Int(m.lnBits/256)) ;	&& WORD biBitCount
		+ BinToC(BI_RGB, "4rs") ;				&& DWORD  biCompression
		+ Replicate(Chr(0), 20)

		lpMemory = LocalAlloc(LMEM_FIXED, m.lnBmpFSize)
		GetDIBits(m.lhMemDC, m.lhBitmap, 0, m.lnBmpH, m.lpMemory, @m.lsBitmapInfo, DIB_RGB_COLORS)
		lcBmpValue = m.lsBitmapFile + m.lsBitmapInfo + Sys(2600, m.lpMemory, m.lnBmpFSize)

		LocalFree(m.lpMemory)

		lhBitmap = SelectObject(m.lhMemDC, m.lhOldBmp)
		DeleteObject(m.lhBitmap)

		DeleteDC(m.lhMemDC)
		ReleaseDC(m.lhWnd, m.lhDC)

		CloseThemeData(m.lhTheme)

		This.nbeforew = This.Width
		This.nbeforeh = This.Height
		This.nbeforestate = This.themestate

		This.PictureVal = m.lcBmpValue

		This.lThemeDraw = .T.
		Return .T.
	Endproc


	Hidden Procedure themeclass_assign
		Lparameters vNewVal
		*To do: Assign
		This.themeclass = m.vNewVal
		This.nbeforew = -1
		This.nbeforeh = -1
		This.nbeforestate = -1
	Endproc


	Procedure MouseLeave
		Lparameters nButton, nShift, nXCoord, nYCoord

		This.themestate = 0
		This.Draw()
	Endproc


	Procedure MouseEnter
		Lparameters nButton, nShift, nXCoord, nYCoord
		This.themestate = 2
		This.Draw()
	Endproc


	Procedure MouseUp
		Lparameters nButton, nShift, nXCoord, nYCoord
		This.themestate = 2
		This.Draw()
	Endproc


	Procedure MouseDown
		Lparameters nButton, nShift, nXCoord, nYCoord
		If m.nButton = 1
			This.themestate = 3
			This.Draw()
		Endif
	Endproc


	Procedure Init
		This.Font = Newobject("Label")
	Endproc


Enddefine
*
*-- EndDefine: themeimage
**************************************************

Procedure declare_dlls
	Declare Integer GetKeyState In "User32" Long nVirtKey

	Declare Long GetDeviceCaps In "Gdi32" Long hdc, Long nIndex

	Declare Long CreateFont In "Gdi32" ;
		Long nHeight, Long nWidth, Long nEscapement, Long nOrientation, Long fnWeight, ;
		Long fdwItalic, Long fdwUnderline, Long fdwStrikeOut, Long fdwCharSet, ;
		Long fdwOutputPrecision, Long fdwClipPrecision, Long fdwQuality, ;
		Long fdwPitchAndFamily,	String lpszFace

	Declare Long DrawText In "User32" ;
		Long hDC, String lpString, Long nCount, ;
		String @lpRect, Long uFormat

	Declare Long CreateCompatibleDC In "Gdi32" Long hDC

	Declare Long CreateCompatibleBitmap In "Gdi32" Long hdc, Long nWidth, Long nHeight

	Declare Long OpenThemeData In "Uxtheme" Long HWnd, String pszClassList

	Declare Long DrawThemeParentBackground In "Uxtheme" Long HWnd, Long hdc, String @pRect

	Declare Long DrawThemeBackground In "Uxtheme" ;
		Long hTheme, Long hdc, Long iPartId, Long iStateId, String @pRect, String @pClipRect

	Declare Long DrawThemeText In "Uxtheme" ;
		Long hTheme, Long hdc, Long iPartId, Long iStateId, ;
		String pszText, Long iCharCount, Long dwTextFlag, ;
		Long dwTextFlags2, String pRect

	Declare Long GetThemeTextExtent In "Uxtheme" ;
		Long hTheme, Long hdc, Long iPartId, Long iStateId, ;
		String pszText, Long iCharCount, ;
		Long dwTextFlags, String @pBoundingRect, String @pExtentRect

	Declare Long CloseThemeData In "Uxtheme" Long hTheme

	Declare Long GetThemePartSize In "Uxtheme" ;
		Long hTheme, Long hdc, Long iPartId, ;
		Long iStateId, String @prc, Long eSize, String @psz

	Declare Long GetDeviceCaps In "Gdi32" Long hdc, Long nIndex

	Declare Long GetDIBits In "Gdi32" Long hdc, Long hbmp, Long uStartScan, Long cScanLines, Long lpvBits, String @lpbi, Long uUsage

	Declare Long LocalAlloc In "Kernel32" Long uFlags, Long dwBytes

	Declare Long LocalFree In "Kernel32" Long hlocMem

	Declare Long GetDC In "User32" Long HWnd

	Declare Long SelectObject In "Gdi32" Long hDC, Long hObject

	Declare Long DeleteDC In "Gdi32" Long hDC

	Declare Long DeleteObject In "Gdi32" Long hDC

	Declare Long ReleaseDC In "User32" Long nhWnd, Long hDC

	Declare Integer GetWindowLong In "User32" Long nhWnd, Integer nIndex

	Declare Integer CallWindowProc In "User32" Long lpPrevWndFunc, Long nhWnd, Long uMsg, Long wParam, Long Lparam

    Declare Integer LockWindowUpdate In "User32" Integer hWndLock  &&LockScreen per media button

	Declare SHORT GetClientRect In "User32" INTEGER  HWnd, STRING @ lpRect

	Declare Integer SetWindowPos In "User32" INTEGER HWnd, INTEGER hWndInsertAfter, INTEGER x, INTEGER Y, INTEGER cx, INTEGER cy, INTEGER wFlags

	Declare Integer LoadLibrary In kernel32 As "LoadLibraryA" STRING lpLibFileName
		
	Declare Integer GetProcAddress In kernel32 INTEGER hModule, STRING lpProcName		
	
	Declare Integer ChooseFont In comdlg32 String @lpcf
	
	Declare Integer GlobalFree In kernel32 Integer Hmem
	
	Declare Integer GlobalAlloc In kernel32 Integer wFlags, Integer dwBytes

	Declare Long GlobalLock In WIN32API Long Hmem
	
	Declare Long GlobalUnlock In WIN32API Long Hmem
	
	Declare Integer PrintDlg In comdlg32.Dll Long lppd

	Declare Integer PrintDlgEx In comdlg32.Dll Long lppd
		
	Declare Integer GetDeviceCaps In gdi32 INTEGER hdc, INTEGER nIndex
		
	Declare RtlMoveMemory In kernel32 As String2Heap INTEGER Destination, String @ Source, INTEGER nLength

	Declare RtlMoveMemory In kernel32 As Heap2String STRING @Dest, Integer Source, Integer nLength

	* --- Dichiarazione API Win32
	Declare Integer RegOpenKey In ADVAPI32.Dll Integer, String, Integer @nKeyHandle
	
	Declare Integer RegEnumValue In ADVAPI32.Dll ;
		Integer nKeyHandle, Integer iValue,	STRING @lpszValue, Integer @lpcchValue, Integer, String @lpdwType, STRING @lpbData, Integer @lpcbData	
	
	Declare Long RegSetValueEx In ADVAPI32.Dll ;
		Integer hKey,String @lpValueName,Long Reserved,Long dwType,String @lpData, Long cbData
		
	Declare Long RegQueryValueEx In ADVAPI32.Dll ;
		Integer hKey, String @lpValueName, INTEGER lpReserved, Integer @lpType, String @lpData, Integer @lpcbData		
	
	Declare Integer RegCloseKey In win32api Integer hKey
	
	Declare Integer RegCreateKeyEx In advapi32;
		INTEGER hKey, STRING lpSubKey, INTEGER Reserved,	STRING lpClass, INTEGER dwOptions, INTEGER samDesired, INTEGER lpSecurityAttributes, INTEGER @ phkResult, INTEGER @ lpdwDisposition
		
	Declare Integer GetWindowsDirectory In win32api String @ WinDir,Integer Len
	
	Declare Long GetVersion In win32api
	
	Declare Integer WinExec In KERNEL32.Dll String @ lpCmdLine, Integer uCmdShow
	
	Declare Integer VFPFileExist In VFPFileExist.Dll String filename
	Public i_initvfpfileexist
	i_initvfpfileexist=.T.
	
	Declare Integer GetMenuItemCount In "User32" Integer hMenu
	
	Declare Integer GetSystemMetrics In "User32" Integer nIndex
	
	Declare Integer CreatePopupMenu In "User32"

	Declare Integer DestroyMenu In "User32" Integer hMenu	
	
    DECLARE INTEGER SetFocus IN user32 AS SetFocusAPI INTEGER hWindow

    DECLARE INTEGER SetForegroundWindow IN user32 INTEGER hWindow
 	
 	DECLARE INTEGER FindWindow IN user32 STRING lpClassName, STRING lpWindowName
 	
 	DECLARE Sleep IN kernel32 INTEGER
Endproc


* ===================================================================================================
*** Zucchetti Aulla
* Classe per la definizione dell'oggetto testata dei Detail
* ===================================================================================================

Define Class cp_DetailHeader As Container
	* --- Classe per testata dei detail
	* --- numero di colonne del contenitore
	nNumberColumn=0
	BackStyle = 1
	BorderWidth = 2
	BorderColor = i_nBorderColor
	bGlobalFont = .T.
	FontSize   = 8
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	FontName   = ""
	LeftoBody=0
	*--- riferimento alla form
	orefform = Null
	*--- riferimento alla maschera per la ricerca nel detail
	oRefSearchDetail = Null
	*--- Se c'� un controllo obbligatorio setto la propriet� per non fare comparire la maschera di ricerca
	bCtrlObbl=.F.
	*--- array per la gestione degli ordinamenti
	nOrderField=0
	Dimension aOrderField(1,2)
	aOrderField(1,1)=''
	aOrderField(1,2)=''
	cTableName=''
	bAssignOrderByDettCustom=.F.

	* --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
	bNoSetAnchorForm = .T.
	* --- Zucchetti Aulla Fine  - Gestione SetAnchorForm

	Add Object lineleft As Line With ;
		Height = 28, ;
		Left = 12, ;
		Top = 5, ;
		Width = 0, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineLeft"

	Add Object linetop As Line With ;
		Height = 0, ;
		Left = 12, ;
		Top = 5, ;
		Width = 48, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineTop"

	Add Object linebottom As Line With ;
		BorderWidth = 0, ;
		Height = 0, ;
		Left = 11, ;
		Top = 31, ;
		Width = 48, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineBottom"

	Add Object lineright As Line With ;
		Height = 28, ;
		Left = 58, ;
		Top = 6, ;
		Width = 0, ;
		BorderColor = i_nBorderColor, ;
		Name = "LineRight"

	Procedure Init
		Local objcolumn, nNumbField, cOrderByDett
		If i_VisualTheme<>-1 and Val(Substr(Os(),8,Len(Os())-7))>=6
			This.AddObject("oBackGround","image")
			This.oBackGround.Top=1
			This.oBackGround.Left=1
			This.oBackGround.Width=This.Width-2
			This.oBackGround.Height=This.Height-2
			This.oBackGround.Anchor=15
			This.oBackGround.Stretch=2
			This.oBackGround.Visible=.T.
		Endif
		*** Costruisco array ordinamenti detail
		This.CreateOrdArray()
		This.ChangeTheme()
	Endproc

	Proc CreateOrdArray()
		If Type("this.parent.parent.parent.parent.cOrderByDett")="C" And Not Empty(This.Parent.Parent.Parent.Parent.cOrderByDett)
			local cOrderByDett, nNumbField
			This.nOrderField=1
			cOrderByDett=This.Parent.Parent.Parent.Parent.cOrderByDett
			nNumbField=Atc(',',cOrderByDett)
			Dimension This.aOrderField[this.nOrderField, 2]
			Do While nNumbField>0
				This.aOrderField[this.nOrderField,1]=Left(cOrderByDett,nNumbField-1)
				This.aOrderField[this.nOrderField,2]=Substr(Strtran(This.aOrderField[this.nOrderField,1], ' DESC', ''),Atc('.',This.aOrderField[this.nOrderField,1])+1)
				This.nOrderField=This.nOrderField+1
				Dimension This.aOrderField[this.nOrderField, 2]
				cOrderByDett=Right(cOrderByDett,Len(cOrderByDett)-nNumbField)
				nNumbField=Atc(',',cOrderByDett)
			Enddo
			This.aOrderField[this.nOrderField,1]=cOrderByDett
			This.aOrderField[this.nOrderField,2]=Substr(Strtran(This.aOrderField[this.nOrderField,1], ' DESC', ''),Atc('.',This.aOrderField[this.nOrderField,1])+1)
		Endif
	Endproc

	Procedure ChangeTheme
		If i_VisualTheme<>-1
			With This
				.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
				.BorderColor = i_ThemesManager.GetProp(TOOLBAR_BORDERCOLOR)
				If Val(Substr(Os(),8,Len(Os())-7))>=6
					If i_bGradientBck
						.oBackGround.Picture=i_ThemesManager.GetProp(TOOLBAR_BACKGRD_H)
					Else
						.oBackGround.Visible=.F.
					Endif
				Else
					.Picture=i_ThemesManager.GetProp(TOOLBAR_BACKGRD_H)
				ENDIF
				IF i_ThemesManager.GetProp(121)=0
					.lineTop.BorderColor =i_ThemesManager.GetProp(TOOLBAR_BORDERCOLOR)
					.lineLeft.BorderColor =i_ThemesManager.GetProp(TOOLBAR_BORDERCOLOR)
					.lineBottom.BorderColor =i_ThemesManager.GetProp(109)
					.lineRight.BorderColor =i_ThemesManager.GetProp(TOOLBAR_BORDERCOLOR)
				Endif
			Endwith
		Endif
	Endproc

	Procedure Event(cEvent)
		Local i, cFunctionLabel, objcolumn, nOrdField, bIsDetailFile, N
		LOCAL TestMacro
		If cEvent='FormLoad'
			This.Height=Max(19,This.Height)
			This.LeftoBody=This.Parent.oBody.Left
			This.Left=Max(This.Parent.oBody.Left-1,0)
			This.Top=This.Parent.oBody.Top-This.Height-1
			This.Width=This.Parent.oBody.Width+1+(This.Parent.oBody.Left-1-This.Left)
			IF i_ThemesManager.GetProp(121)=0
			    *oBody3d
			    This.Parent.obody3D.visible= .f.
			    This.Left=This.Left+1
			    This.Top=This.Top+2
			    This.zOrder(0)
			endif
			
			bIsDetailFile=cp_getEntityType(This.Parent.Parent.Parent.Parent.cPrg)="Detail File" Or not PemStatus(this.parent.parent.parent.parent,"cFileDetail",5)
			For m.i=1 To This.nNumberColumn
				m.N=Alltrim(Str(m.i))
				objcolumn="hdr_"+This.Field&N
				If Not Pemstatus(This,objcolumn,5)
					This.AddObject(objcolumn, "cp_DetailHeaderColumn")
				Endif
				This. &objcolumn .cNameColumn=This.Field&N
				This. &objcolumn .nNumberPosition=m.i
				If bIsDetailFile
					This.cTableName=This.Parent.Parent.Parent.Parent.cFile
				Else
					This.cTableName=This.Parent.Parent.Parent.Parent.cFileDetail
				Endif
				This. &objcolumn .bIsField=i_dcx.GetFieldIdx(This.cTableName, This. &objcolumn .cNameColumn)>0
				TestMacro=This.Label&N
				If "ah_msgformat"$Lower(TestMacro)
					cFunctionLabel=This.Label&N
					cFunctionLabel=Strtran(cFunctionLabel,"w_","this.Parent.oContained.w_",1)
					This. &objcolumn .header1.Caption=&cFunctionLabel
				Else
					This. &objcolumn .header1.Caption=cp_Translate( This.Label&N )
				Endif
				This. &objcolumn .header1.bGlobalFont = .T.
				This. &objcolumn .header1.SetFont()
				*** controllo se sul campo c'� un ordinamento
				If This.nOrderField>0
					nOrdField=Ascan(This.aOrderField,This. &objcolumn .cNameColumn,-1,-1,2,15)
					If nOrdField>0
					TestMacro=vartype(This. &objcolumn .oOrderImage)
						If TestMacro<>"O"
							This. &objcolumn .Newobject("oOrderImage", "Image")
							This. &objcolumn .oOrderImage.Name="oOrderImage"
							This. &objcolumn .oOrderImage.BackStyle = 0
							If Upper(Right(This.aOrderField[nOrdField,1], 5))=" DESC"
								This. &objcolumn .oOrderImage.Picture = cp_GetBtnOrderImageDesc()
							Else
								This. &objcolumn .oOrderImage.Picture = cp_GetBtnOrderImageAsc()						
							Endif							
							This. &objcolumn .oOrderImage.Left = This. &objcolumn .Width - This. &objcolumn .oOrderImage.Width - 1
							This. &objcolumn .oOrderImage.Top = This. &objcolumn .Height - This. &objcolumn .oOrderImage.Height
						Endif
						This. &objcolumn .oOrderImage.Visible=.T.
						***
						TestMacro=vartype(This. &objcolumn .oOrderNum)
						If TestMacro<>"O"
							This. &objcolumn .Newobject("oOrderNum", "Label")
							This. &objcolumn .oOrderNum.BackStyle = 0
							This. &objcolumn .oOrderNum.Left = This. &objcolumn .oOrderImage.Left+1
							This. &objcolumn .oOrderNum.Top = 0
							This. &objcolumn .oOrderNum.FontName="Arial"
							This. &objcolumn .oOrderNum.FontSize=6
							This. &objcolumn .oOrderNum.Name = "oOrderNum"							
						Endif						
						This. &objcolumn .oOrderNum.Caption=Alltrim(Str(nOrdField))
						This. &objcolumn .oOrderNum.Visible=.T.
					Endif
				Endif
			Endfor
			This.Resize()
		Endif
	Endproc

	Procedure Calculate(xValue)
		Local i,cFunctionLabel,objcolumn,N
		LOCAL TestMacro
		For m.i=1 To This.nNumberColumn
			m.N=Alltrim(Str(m.i))
			TestMacro=This.Label&N
			If "ah_msgformat"$Lower(TestMacro)
				objcolumn="hdr_"+This.Field&N
				cFunctionLabel=This.Label&N
				cFunctionLabel=Strtran(cFunctionLabel,"w_","this.Parent.oContained.w_",1)
				This. &objcolumn .header1.Caption=&cFunctionLabel
				This. &objcolumn .header1.bGlobalFont = .T.
				This. &objcolumn .header1.SetFont()
				This. &objcolumn .gettxtsize()
			Endif
		Endfor
	Endproc

	Procedure Resize
		DoDefault()
		Local i,N
		For m.i=1 To This.nNumberColumn
			m.N=Alltrim(Str(m.i))
			objcolumn="hdr_"+This.Field&N
			If Type("this."+objcolumn)='O'
				This. &objcolumn .gettxtsize()
			Endif
		Endfor
		With This
			.linetop.Move(0, 0, Max(.Width - 1, 0), 0)
			.linebottom.Move(0, .Height-1, .Width, 0)
			.lineleft.Move(0, 0, 0, .Height)
			.lineright.Move(.Width - 1, 0, 0, .Height)
		Endwith
	Endproc

	Procedure Destroy
		If Vartype(This.oRefSearchDetail)='O'
			This.oRefSearchDetail.EcpQuit()
		Endif
		This.oRefSearchDetail=.Null.
		This.orefform=.Null.
	Endproc

	Procedure BlankFilter
		Local objcolumn,i,N
		LOCAL TestMacro
		For m.i=1 To This.nNumberColumn
			N=Alltrim(Str(m.i))
			objcolumn="hdr_"+This.Field&N
			TestMacro=vartype(This. &objcolumn .oWhereFilter)
			If TestMacro='O'
				This. &objcolumn .oWhereFilter.Visible=.F.
			Endif
		Endfor
	Endproc

	Procedure BlankOrder
		Local objcolumn,i,N
		LOCAL TestMacro
		For m.i=1 To This.nNumberColumn
			N=Alltrim(Str(m.i))
			objcolumn="hdr_"+This.Field&N
			TestMacro=Vartype(This. &objcolumn .oOrderImage)
			If TestMacro="O"
				This. &objcolumn .oOrderImage.Visible=.F.
			ENDIF
			TestMacro=Vartype(This. &objcolumn .oOrderNum)
			If TestMacro="O"
				This. &objcolumn .oOrderNum.Caption='0'
				This. &objcolumn .oOrderNum.Visible=.F.
			Endif
			If This.nOrderField>0
				nOrdField=Ascan(This.aOrderField,This. &objcolumn .cNameColumn,-1,-1,2,15)
				If nOrdField>0
				TestMacro=vartype(This. &objcolumn .oOrderImage)
					If TestMacro<>"O"
						This. &objcolumn .Newobject("oOrderImage", "Image")
						This. &objcolumn .oOrderImage.Name = "oOrderImage"							
						This. &objcolumn .oOrderImage.BackStyle = 0
						If Upper(Right(This.aOrderField[nOrdField,1], 5))=" DESC"
							This. &objcolumn .oOrderImage.Picture = cp_GetBtnOrderImageDesc()					
						Else
							This. &objcolumn .oOrderImage.Picture = cp_GetBtnOrderImageAsc()					
						Endif						
						This. &objcolumn .oOrderImage.Left = This. &objcolumn .Width - This. &objcolumn .oOrderImage.Width - 1
						This. &objcolumn .oOrderImage.Top = This. &objcolumn .Height - This. &objcolumn .oOrderImage.Height
					Endif		
					This. &objcolumn .oOrderImage.Visible=.T.	
					TestMacro=	vartype(This. &objcolumn .oOrderNum)			
					If TestMacro<>"O"
						This. &objcolumn .Newobject("oOrderNum", "Label")
						This. &objcolumn .oOrderNum.BackStyle = 0
						This. &objcolumn .oOrderNum.Left = This. &objcolumn .oOrderImage.Left+1
						This. &objcolumn .oOrderNum.Top = 0
						This. &objcolumn .oOrderNum.FontName="Arial"
						This. &objcolumn .oOrderNum.FontSize=6
						This. &objcolumn .oOrderNum.Name = "oOrderNum"							
					Endif						
					This. &objcolumn .oOrderNum.Caption=Alltrim(Str(nOrdField))
					This. &objcolumn .oOrderNum.Visible=.T.
				Endif
			Endif
		Endfor
	Endproc

	Procedure RemoveFilter
		Local cCondFilter, i, cOldArea
		cOldArea=Select()
		cCondFilter=Filter(This.Parent.Parent.Parent.Parent.cTrsName)
		If Not Empty(cCondFilter)
			Select (This.Parent.Parent.Parent.Parent.cTrsName)
			Set Filter To
			This.BlankFilter()
		Endif
		* ripristino la vecchia area
		Select(cOldArea)
	Endproc

	Procedure GoPosition(cParam)
		Local cOldArea
		* Memorizzo l'area corrente per ripristino
		cOldArea=Select()
		This.Parent.oContained.__dummy__.Enabled=.T.
		This.Parent.oContained.__dummy__.SetFocus()
		This.Parent.oContained.__dummy__.Enabled=.F.
		Select( This.Parent.Parent.Parent.Parent.cTrsName )
		If Upper(cParam)='TOP'
			This.Parent.Parent.Parent.Parent.FirstRow()
		Else
			This.Parent.Parent.Parent.Parent.LastRow()
		Endif
		This.Parent.Parent.Parent.Parent.oPgFrm.Page1.oPag.oBody.SetCurrentRow()
		This.Parent.Parent.Parent.Parent.oPgFrm.Page1.oPag.oBody.SetFullFocus()
		* ripristino la vecchia area
		Select(cOldArea)
	Endproc
Enddefine

Define Class cp_DetailHeader_deferred As Timer
	* --- solito trucco per poter eseguire delle operazioni in ritardo
	Interval=100
	oThis=.Null.
	oObj=.Null.
	Proc Init(oObj)
		This.oObj=oObj
		*--- Decremento i riferimenti all'oggetto HeaderDetail (C000005 in caso di F10)
		cb_DecrementRef(m.oObj)
		This.oThis=This
	Endproc
	Proc Timer()
		*--- Non uso la  vartype perch� This.oObj potrebbe gi� essere stato distrutto
		If Type("This.oObj.oRefSearchDetail")='O'
			This.oObj.oRefSearchDetail.EcpQuit()
		Endif
		If Type("This.oObj")='O' AND NOT ISNULL(This.oObj)
			*--- Re-incremento i riferimenti all'oggetto HeaderDetail altrimenti This.oObj=.NULL. distrugerebbe l'oggetto troppo presto
			cb_IncrementRef(This.oObj)
			This.oObj=.NULL.
		EndIf
		This.oThis=.Null.
	Endproc
Enddefine

* ===================================================================================================
* Classe per la definizione dell'oggetto testata dei Detail
* ===================================================================================================
Define Class cp_DetailHeaderColumn As Container
	* --- Classe per label delle colonne
	BackStyle = 0
	BorderWidth = 0
	bGlobalFont = .T.
	FontSize   = 8
	FontBold   = .F.
	FontItalic = .F.
	FontUnderline  = .F.
	FontStrikethru = .F.
	FontName   = ""
	Visible=.T.
	* --- Riferimento al nome variabile e posizione nella colonna
	cNameColumn=''
	nNumberPosition=0
	* --- Ogetto colonna del detail
	oObjColumn=Null
	* --- Proprit� che indica se l'oggetto si riferisce ad una variabile o a un campo
	bIsField=.F.

	Add Object header1 As StdString With ;
		WordWrap = .T., ;
		BackStyle = 0, ;
		Alignment = 2, ;
		Caption = "Header1", ;
		Height = 17, ;
		Left = 1, ;
		Top = 1, ;
		Width = 42, ;
		Name = "Header1"

	Procedure Click
		If Type("this.parent.__timerdblclickdetail")='O'
			This.Parent.RemoveObject("__timerdblclickdetail")
		Endif
		If Vartype(i_curform)='O' And (Not(Lower(This.oObjColumn.Class)="stdtrsmemo" Or Lower(This.oObjColumn.BaseClass)="checkbox" Or Lower(This.oObjColumn.BaseClass)="combobox" Or Lower(This.oObjColumn.BaseClass)="optiongroup"))
			This.Parent.orefform=i_curform
			If Vartype(This.oRefSearchDetail)='O'
				This.Parent.oRefSearchDetail.EcpQuit()
			Endif
			Local i_olderr
			i_olderr=On('ERROR')
			On Error =.T.
			This.Parent.AddObject("__timerdblclickdetail", "zz_deferred_searchdetail",This)
			On Error &i_olderr
		Endif
	Endproc

	Procedure DblClick
		Local nOrdField
                * --- Zucchetti Aulla inizio - L ordinamento del dettaglio di una griglia facendo il doppio click sulla colonna sbianca la griglia se il focus non � su un campo di testata
		This.Parent.Parent.Parent.Parent.Parent.SetFocusOnFirstControl()
                * --- Zucchetti Aulla fine
		If Type("this.parent.__timerdblclickdetail")='O'
			This.Parent.RemoveObject("__timerdblclickdetail")
		Endif
		If Upper(This.Parent.Parent.Parent.Parent.Parent.cFunction)="QUERY" And Type("this.Parent.Parent.Parent.Parent.Parent.cOrderByDett")="C" And This.bIsField And This.Parent.Parent.Parent.Parent.Parent.bApplyOrderDetailCustom
			nOrdField=Ascan(This.Parent.aOrderField,This.cNameColumn,-1,-1,2,15)
			If nOrdField>0
				cp_InvertOrderDetail(This)
			Else
				cp_AddFirstOrderDetail(This, .F.)
			Endif
		Endif
	Endproc

	Proc RightClick
		Local oFrm
		If Not isalt()
			*** creo menu per gestione nuovi comandi
			Local i_olderr,cFileMenu
			i_olderr=On('ERROR')
			On Error =.T.
			g_oMenu=Createobject("IM_ContextMenu", This, "", "", 8)
			g_oMenu.BeforeCreaMenu()
			g_oMenu=.Null.
			On Error &i_olderr
		Endif
	Endproc

	Procedure header1.Click
		This.Parent.Click()
	Endproc

	Procedure header1.DblClick()
		This.Parent.DblClick()
	Endproc

	Procedure header1.RightClick
		This.Parent.RightClick()
	Endproc

	Procedure Destroy
		This.oObjColumn=.Null.
	Endproc

	Procedure gettxtsize
		With This
			If Vartype(.oObjColumn)<>'O'
				.oObjColumn=.Parent.Parent.Parent.Parent.Parent.GetBodyCtrl("w_"+.cNameColumn)
				If Vartype(.oObjColumn)<>'O'
					.oObjColumn=.Parent.Parent.Parent.Parent.Parent.GetBodyCtrl(.cNameColumn)
				Endif
			Endif
			If Vartype(.oObjColumn)='O'
				.Left = Iif(i_bRecordMark,12,1) + .oObjColumn.Left + (.Parent.LeftoBody-.Parent.Left)
				.Top = .oObjColumn.Top+1
				.Height = .Parent.Height-2
				.header1.Height = .Height
				If Empty(.header1.Caption)
					.Width=0
				Else
					If .nNumberPosition = .Parent.nNumberColumn
						.Width = .Parent.Width-.Left-2
					Else
						.Width = .oObjColumn.Width-2+Iif(.oObjColumn.bhaszoom,14,0)
					Endif
				Endif
				If Txtwidth(.header1.Caption,.header1.FontName,.header1.FontSize)*Fontmetric(6,.header1.FontName,.header1.FontSize)+1>.Width
					.header1.Alignment = 0
					.header1.Left = 2
				Else
					.header1.Alignment = 2
				Endif
				.header1.Width = .Width
				.ToolTipText = .oObjColumn.ToolTipText
				.header1.ToolTipText = .ToolTipText
				If vartype(.oOrderImage)="O"				
					.oOrderImage.Left = .Width - .oOrderImage.Width - 1
					.oOrderImage.Top = .Height - .oOrderImage.Height
				Endif
				If vartype(.oOrderNum)="O"
					.oOrderNum.Left = .oOrderImage.Left+1
				Endif
			Else
				.Visible=.F.
			Endif
		Endwith
	Endproc
Enddefine

Define Class zz_deferred_searchdetail As Timer
	* --- solito trucco per poter eseguire delle operazioni in ritardo
	Interval=200
	oThis=.Null.
	oObj=.Null.
	Proc Init(oObj)
		This.oObj=oObj
		This.oThis=This
	Proc Timer()
		This.Enabled=.F.
		Local i_errsav, i_bError
		i_errsav=On('ERROR')
		i_bError=.f.
		On Error i_bError =.t.
		This.oObj.Parent.oRefSearchDetail=cp_searchdetail( This.oObj )
		On Error &i_errsav
		If i_bError
			This.oObj.Parent.oRefSearchDetail.ecpquit()
		endif
		This.oObj.Parent.bCtrlObbl=.F.
		*This.oObj=.Null.
		This.oThis=.Null.
Enddefine

*** Procedure per gestione menu ricerca
Procedure cp_DetailAddFilter(pobj)
	pobj.Click()
	pobj.Parent.Parent.Parent.Parent.Parent.Parent.Refresh()
Endproc

Procedure cp_DetailRemoveFilter(pobj)
	pobj.RemoveFilter()
	pobj.Parent.Parent.Parent.Parent.Refresh()
Endproc

Procedure cp_GoPosition(pobj, pParam)
	pobj.GoPosition(pParam)
	pobj.Parent.Parent.Parent.Parent.Refresh()
Endproc

*** Procedure per gestione menu ordinamento
Proc cp_AddOrderDetail(pobj, bOrderDesc)
	Local nOrdField
	* --- Zucchetti Aulla inizio - L ordinamento del dettaglio di una griglia facendo il doppio click sulla colonna sbianca la griglia se il focus non � su un campo di testata
	pobj.Parent.Parent.Parent.Parent.Parent.SetFocusOnFirstControl()
	* --- Zucchetti Aulla fine
	nOrdField=Ascan(pobj.Parent.aOrderField,pobj.cNameColumn,-1,-1,2,15)
	If nOrdField<>0
		cp_BlankOrderDetail(pobj)
	Endif
	pobj.Parent.nOrderField=pobj.Parent.nOrderField+1
	Dimension pobj.Parent.aOrderField[pobj.Parent.nOrderField,2]
	pobj.Parent.aOrderField[pobj.Parent.nOrderField,1]=pobj.Parent.cTableName+'.'+pobj.cNameColumn+Iif(bOrderDesc,' DESC','')
	pobj.Parent.aOrderField[pobj.Parent.nOrderField,2]=pobj.cNameColumn
	With pobj.Parent.Parent.Parent.Parent.Parent
		If !pobj.Parent.bAssignOrderByDettCustom And Empty(.cOrderByDettCustom) And Not Empty(.cOrderByDett) And .bApplyOrderDetail
			pobj.Parent.bAssignOrderByDettCustom=.T.
			.cOrderByDettCustom=.cOrderByDett
		Endif
		.bApplyOrderDetail=.T.
		.cOrderByDettCustom=.cOrderByDettCustom+Iif(Not Empty(.cOrderByDettCustom),',','')+pobj.Parent.aOrderField[pobj.Parent.nOrderField,1]
		If vartype(pobj.oOrderImage)<>"O"
			pobj.Newobject("oOrderImage", "Image")
			pobj.oOrderImage.Name="oOrderImage"
			pobj.oOrderImage.BackStyle = 0
			If bOrderDesc
				pobj.oOrderImage.Picture = cp_GetBtnOrderImageDesc()
			Else
				pobj.oOrderImage.Picture = cp_GetBtnOrderImageAsc()
			Endif			
			pobj.oOrderImage.Left = pobj.Width - pobj.oOrderImage.Width - 1
			pobj.oOrderImage.Top = pobj.Height - pobj.oOrderImage.Height
		Else
			If bOrderDesc
				pobj.oOrderImage.Picture = cp_GetBtnOrderImageDesc()
			Else
				pobj.oOrderImage.Picture = cp_GetBtnOrderImageAsc()
			Endif		
		Endif		
		pobj.oOrderImage.Visible=.T.
		If vartype(pobj.oOrderNum)<>"O"
			pobj.Newobject("oOrderNum", "Label")
			pobj.oOrderNum.BackStyle = 0
			pobj.oOrderNum.Left = pobj.oOrderImage.Left+1
			pobj.oOrderNum.Top = 0
			pobj.oOrderNum.FontName="Arial"
			pobj.oOrderNum.FontSize=6
			pobj.oOrderNum.Name = "oOrderNum"							
		Endif			
		pobj.oOrderNum.Caption=Alltrim(Str(pobj.Parent.nOrderField))
		pobj.oOrderNum.Visible=.T.
		.LoadRec()
		.Refresh()
	Endwith
Endproc

Proc cp_AddFirstOrderDetail(pobj, bOrderDesc)
	Local i, nOrdField
	* --- Zucchetti Aulla inizio - L ordinamento del dettaglio di una griglia facendo il doppio click sulla colonna sbianca la griglia se il focus non � su un campo di testata
	pobj.Parent.Parent.Parent.Parent.Parent.SetFocusOnFirstControl()
	* --- Zucchetti Aulla fine
	nOrdField=Ascan(pobj.Parent.aOrderField,pobj.cNameColumn,-1,-1,2,15)
	If nOrdField<>0
		cp_BlankOrderDetail(pobj)
	Endif
	pobj.Parent.nOrderField=pobj.Parent.nOrderField+1
	Dimension pobj.Parent.aOrderField[pobj.Parent.nOrderField,2]
	For m.i=pobj.Parent.nOrderField To 2 Step -1
		pobj.Parent.aOrderField[m.i,1]=pobj.Parent.aOrderField[m.i-1,1]
		pobj.Parent.aOrderField[m.i,2]=pobj.Parent.aOrderField[m.i-1,2]
	Endfor

	With pobj.Parent.Parent.Parent.Parent.Parent
		pobj.Parent.aOrderField[1,1]=pobj.Parent.cTableName+'.'+pobj.cNameColumn+Iif(bOrderDesc,' DESC','')
		pobj.Parent.aOrderField[1,2]=pobj.cNameColumn
		If !pobj.Parent.bAssignOrderByDettCustom And Empty(.cOrderByDettCustom) And Not Empty(.cOrderByDett) And .bApplyOrderDetail
			pobj.Parent.bAssignOrderByDettCustom=.T.
			.cOrderByDettCustom=.cOrderByDett
		Endif
		.bApplyOrderDetail=.T.
		.cOrderByDettCustom=pobj.Parent.aOrderField[1,1]+Iif(Not Empty(.cOrderByDettCustom),',','')+.cOrderByDettCustom
		If vartype(pobj.oOrderImage)<>"O"
			pobj.Newobject("oOrderImage", "Image")
			pobj.oOrderImage.Name="oOrderImage"
			pobj.oOrderImage.BackStyle = 0
			pobj.oOrderImage.Picture = iif( bOrderDesc, cp_GetBtnOrderImageDesc(), cp_GetBtnOrderImageAsc())			
			pobj.oOrderImage.Left = pobj.Width - pobj.oOrderImage.Width - 1
			pobj.oOrderImage.Top = pobj.Height - pobj.oOrderImage.Height
		Else
			pobj.oOrderImage.Picture = iif( bOrderDesc, cp_GetBtnOrderImageDesc(), cp_GetBtnOrderImageAsc())
		Endif		
		pobj.Parent.BlankOrder()
		.LoadRec()
		.Refresh()
	Endwith
Endproc

Proc cp_InvertOrderDetail(pobj)
	Local nOrdField
	* --- Zucchetti Aulla inizio - L ordinamento del dettaglio di una griglia facendo il doppio click sulla colonna sbianca la griglia se il focus non � su un campo di testata
	pobj.Parent.Parent.Parent.Parent.Parent.SetFocusOnFirstControl()
	* --- Zucchetti Aulla fine
	nOrdField=Ascan(pobj.Parent.aOrderField,pobj.cNameColumn,-1,-1,2,15)
	If nOrdField>0
		With pobj.Parent.Parent.Parent.Parent.Parent
			If !pobj.Parent.bAssignOrderByDettCustom And Empty(.cOrderByDettCustom) And Not Empty(.cOrderByDett) And .bApplyOrderDetail
				pobj.Parent.bAssignOrderByDettCustom=.T.
				.cOrderByDettCustom=.cOrderByDett
			Endif
			.bApplyOrderDetail=.T.
			If vartype(pobj.oOrderImage)<>"O"
				pobj.Newobject("oOrderImage", "Image")
				pobj.oOrderImage.Name="oOrderImage"
				pobj.oOrderImage.Picture = cp_GetBtnOrderImageAsc()
				pobj.oOrderImage.BackStyle = 0
				pobj.oOrderImage.Left = pobj.Width - pobj.oOrderImage.Width - 1
				pobj.oOrderImage.Top = pobj.Height - pobj.oOrderImage.Height
			Else
				pobj.oOrderImage.Picture = cp_GetBtnOrderImageAsc()
			Endif			
			If Upper(Right(pobj.Parent.aOrderField[nOrdField,1], 5))=" DESC"
				pobj.Parent.aOrderField[nOrdField,1]=pobj.Parent.cTableName+'.'+pobj.cNameColumn
				.cOrderByDettCustom=Strtran(.cOrderByDettCustom,pobj.Parent.cTableName+'.'+pobj.cNameColumn+" DESC",pobj.Parent.cTableName+'.'+pobj.cNameColumn)
			Else
				pobj.oOrderImage.Picture = cp_GetBtnOrderImageDesc()				
				pobj.Parent.aOrderField[nOrdField,1]=pobj.Parent.cTableName+'.'+pobj.cNameColumn+' DESC'
				.cOrderByDettCustom=Strtran(.cOrderByDettCustom,pobj.Parent.cTableName+'.'+pobj.cNameColumn,pobj.Parent.cTableName+'.'+pobj.cNameColumn+" DESC")
			Endif
			pobj.oOrderImage.visible=.t.			
			.LoadRec()
			.Refresh()
		Endwith
	Endif
Endproc

Proc cp_BlankOrderDetail(pobj)
	Local nOrdField
	* --- Zucchetti Aulla inizio - L ordinamento del dettaglio di una griglia facendo il doppio click sulla colonna sbianca la griglia se il focus non � su un campo di testata
	pobj.Parent.Parent.Parent.Parent.Parent.SetFocusOnFirstControl()
	* --- Zucchetti Aulla fine
	nOrdField=Ascan(pobj.Parent.aOrderField,pobj.cNameColumn,-1,-1,2,15)
	If nOrdField>0
		With pobj.Parent.Parent.Parent.Parent.Parent
			If !pobj.Parent.bAssignOrderByDettCustom And Empty(.cOrderByDettCustom) And Not Empty(.cOrderByDett) And .bApplyOrderDetail
				pobj.Parent.bAssignOrderByDettCustom=.T.
				.cOrderByDettCustom=.cOrderByDett
			Endif
			.bApplyOrderDetail=.T.
			If Upper(Right(pobj.Parent.aOrderField[nOrdField,1], 5))=" DESC"
				.cOrderByDettCustom=Strtran(.cOrderByDettCustom,pobj.Parent.cTableName+'.'+pobj.cNameColumn+" DESC"+Iif(nOrdField=pobj.Parent.nOrderField,"",","),"")
			Else
				.cOrderByDettCustom=Strtran(.cOrderByDettCustom,pobj.Parent.cTableName+'.'+pobj.cNameColumn+Iif(nOrdField=pobj.Parent.nOrderField,"",","),"")
			Endif
			If Right(Alltrim(.cOrderByDettCustom),1)=','
				.cOrderByDettCustom=Left(Alltrim(.cOrderByDettCustom),Len(Alltrim(.cOrderByDettCustom))-1)
			Endif
			=Adel(pobj.Parent.aOrderField, nOrdField)
			pobj.Parent.nOrderField=pobj.Parent.nOrderField-1
			Dimension pobj.Parent.aOrderField[pobj.Parent.nOrderField,2]
			pobj.Parent.BlankOrder()
			.LoadRec()
			.Refresh()
		Endwith
	Endif
Endproc

Proc cp_DefaultOrderDetail(pobj)
	Local nOrdField	
	With pobj.Parent.Parent.Parent.Parent
    * --- Zucchetti Aulla inizio - L ordinamento del dettaglio di una griglia facendo il doppio click sulla colonna sbianca la griglia se il focus non � su un campo di testata
	  .SetFocusOnFirstControl()
	* --- Zucchetti Aulla fine		
		.bApplyOrderDetail=.T.
		.cOrderByDettCustom=''
		pobj.bAssignOrderByDettCustom=.F.
		pobj.CreateOrdArray()
		pobj.BlankOrder()
		.LoadRec()
		.Refresh()
	Endwith
Endproc

Proc cp_BlankAllOrderDetail(pobj)
	Local nOrdField
	With pobj.Parent.Parent.Parent.Parent
    * --- Zucchetti Aulla inizio - L ordinamento del dettaglio di una griglia facendo il doppio click sulla colonna sbianca la griglia se il focus non � su un campo di testata
	  .SetFocusOnFirstControl()
	* --- Zucchetti Aulla fine		
		.bApplyOrderDetail=.F.
		pobj.bAssignOrderByDettCustom=.F.
		.cOrderByDettCustom=''
		pobj.nOrderField=0
		Dimension pobj.aOrderField[1,2]
		pobj.aOrderField[1,1]=""
		pobj.aOrderField[1,2]=""
		pobj.BlankOrder()
		.LoadRec()
		.Refresh()
	Endwith
Endproc

Proc cp_GetBtnOrderImageDesc
	if vartype(g_BtnOrderImageDesc)<>'C' or empty(g_BtnOrderImageDesc)
		If empty(cPathVfcsim)
				cPathVfcsim=chomedir+"..\vfcsim"
		Endif	
		public g_BtnOrderImageDesc
		If Vartype(i_ThemesManager)=='O'
			g_BtnOrderImageDesc = forceext(i_ThemesManager.RetBmpFromIco(cPathVfcsim+"\order_desc.bmp", 8),'bmp')
		Else
			g_BtnOrderImageDesc = cPathVfcsim+"\order_desc.bmp"
		Endif
	endif			
	return(g_BtnOrderImageDesc)
Endproc

Proc cp_GetBtnOrderImageAsc
	if vartype(g_BtnOrderImageAsc)<>'C' or empty(g_BtnOrderImageAsc)
		If empty(cPathVfcsim)
				cPathVfcsim=chomedir+"..\vfcsim"
		Endif	
		public g_BtnOrderImageAsc
		If Vartype(i_ThemesManager)=='O'
			g_BtnOrderImageAsc = forceext(i_ThemesManager.RetBmpFromIco(cPathVfcsim+"\order_asc.bmp", 8),'bmp')
		Else
			g_BtnOrderImageAsc = cPathVfcsim+"\order_asc.bmp"
		Endif
	endif
	return(g_BtnOrderImageAsc)
Endproc

Proc cp_GetBtnWhereImage
	if vartype(g_BtnWhereImage)<>'C' or empty(g_BtnWhereImage)
		If empty(cPathVfcsim)
				cPathVfcsim=chomedir+"..\vfcsim"
		Endif		
		public g_BtnWhereImage
		If Vartype(i_ThemesManager)=='O'
			g_BtnWhereImage  = forceext(i_ThemesManager.RetBmpFromIco(cPathVfcsim+"\where_filter.bmp", 10),'bmp')
		Else
			g_BtnWhereImage = cPathVfcsim+"\where_filter.bmp"
		Endif
	endif
	return(g_BtnWhereImage)
Endproc
* ===================================================================================================
