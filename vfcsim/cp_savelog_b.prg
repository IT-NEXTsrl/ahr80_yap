* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_savelog_b                                                    *
*              Union dei file di log                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-07-29                                                      *
* Last revis.: 2012-04-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_savelog_b",oParentObject)
return(i_retval)

define class tcp_savelog_b as StdBatch
  * --- Local variables
  * --- WorkFile variables
  show_log_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    i_cACTIVATEPROFILER_POSIZIONELOG = this.oParentObject.w_POSIZIONELOG
    i_cACTIVATEPROFILER_NOMELOG = this.oParentObject.w_NOMELOG
    * --- Eventualmente limina i record gi� presenti
    if TYPE( "i_nACTIVATEPROFILER_ALIAS" ) = "C"
      if USED( i_nACTIVATEPROFILER_ALIAS )
        DELETE FROM ( i_nACTIVATEPROFILER_ALIAS )
      endif
    endif
    * --- Per creare il dbf, inserire un record, gestire il rollover si utilizza una chiamata all'activitylogger in modalit� di tracciatura eventi,
    *     cio� con parametro pTipoOperazione='UEV'
    i_nACTIVATEPROFILER_TRACCIATURAEVENTI_OLD = i_nACTIVATEPROFILER_TRACCIATURAEVENTI
    i_nACTIVATEPROFILER_TRACCIATURAEVENTI=1
    * --- Si utilizza la modalit� di scrittura su DBF
    i_nACTIVATEPROFILER_OLD = i_nACTIVATEPROFILER
    i_nACTIVATEPROFILER = 1
    * --- Select from show_log
    i_nConn=i_TableProp[this.show_log_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.show_log_idx,2],.t.,this.show_log_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" show_log ";
           ,"_Curs_show_log")
    else
      select * from (i_cTable);
        into cursor _Curs_show_log
    endif
    if used('_Curs_show_log')
      select _Curs_show_log
      locate for 1=1
      do while not(eof())
      * --- Inserisce un record nel dbf di log con dei dati provvisori, perch� alcuni di essi sono ricalcolati all'interno dell'activitylogger
      cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UEV", "Provvisorio", This, 0, 0, "", This)
      * --- Scrive i dati effettivi
      AH_MSG( "Esportazione del record n. %1", .T., , , _Curs_show_log.ALNUMBER)
      SELECT( i_nACTIVATEPROFILER_ALIAS )
      GO BOTTOM
      Replace ; 
 ALDATTIM With NVL(_Curs_show_log.ALDATTIM,CTOT("  -  -  ")), AL____MS with _Curs_show_log.AL____MS, AL__TIPO with _Curs_show_log.AL__TIPO,; 
 ALCMDSQL with NVL(_Curs_show_log.ALCMDSQL,""), ALSERVER with NVL(_Curs_show_log.ALSERVER,SPACE(1)), ALTIPCON with _Curs_show_log.ALTIPCON,; 
 ALTYPEDB with NVL(_Curs_show_log.ALTYPEDB,SPACE(1)), ALPROCED with _Curs_show_log.ALPROCED, ALCAPTIO with NVL(_Curs_show_log.ALCAPTIO,SPACE(1)),; 
 ALTOTTMS with _Curs_show_log.ALTOTTMS, ALTOTTSC with _Curs_show_log.ALTOTTSC,; 
 ALROWAFF with _Curs_show_log.ALROWAFF, ALCURSOR with NVL(_Curs_show_log.ALCURSOR,SPACE(1)), ALERROR with NVL(_Curs_show_log.ALERROR,""),; 
 ALCODUTE with _Curs_show_log.ALCODUTE, ALCODAZI with _Curs_show_log.ALCODAZI, ALCRDATI with _Curs_show_log.ALCRDATI,; 
 ALCOLORE with _Curs_show_log.ALCOLORE, ALDEARIC with _Curs_show_log.ALDEARIC,AL__HOST with _Curs_show_log.AL__HOST
        select _Curs_show_log
        continue
      enddo
      use
    endif
    i_nACTIVATEPROFILER = i_nACTIVATEPROFILER_OLD
    i_nACTIVATEPROFILER_TRACCIATURAEVENTI = i_nACTIVATEPROFILER_TRACCIATURAEVENTI_OLD
    CP_ERRORMSG("Operazione terminata")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='show_log'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_show_log')
      use in _Curs_show_log
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
