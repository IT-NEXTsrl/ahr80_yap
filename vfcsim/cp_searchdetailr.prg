* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_searchdetailr                                                *
*              Routine ctrl mask detail search                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-12-22                                                      *
* Last revis.: 2012-08-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCommandExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_searchdetailr",oParentObject,m.pCommandExec)
return(i_retval)

define class tcp_searchdetailr as StdBatch
  * --- Local variables
  pCommandExec = space(10)
  w_VALUEFILTERTYPE = space(1)
  w_POSITION = 0
  w_OLDPOSITION = 0
  w_FilterCond = space(254)
  w_ZOOM = .NULL.
  w_OBJFORM = .NULL.
  w_FIRSTREC = .f.
  w_NUMREC = 0
  w_EOF = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per il controllo dela maschera di ricerca e filtro
    local i_OldCursor 
 i_OldCursor=select ()
    this.w_OBJFORM = this.oParentObject.oParentObject.oObjColumn.Parent.Parent.Parent.Parent.Parent.Parent.Parent
    do case
      case this.pCommandExec="Type"
        WITH this.oParentObject.oParentObject.oObjColumn
        do case
          case vartype(.Value)="D" or vartype(.Value)="T"
            this.w_VALUEFILTERTYPE = "D"
            this.oParentObject.w_ZoomValue.visible = .t.
            this.oParentObject.w_ZoomValueN.visible = .f.
          case vartype(.Value)="N"
            this.w_VALUEFILTERTYPE = "N"
            this.oParentObject.w_ZoomValue.visible = .f.
            this.oParentObject.w_ZoomValueN.visible = .t.
          otherwise
            this.w_VALUEFILTERTYPE = "C"
            this.oParentObject.w_ZoomValue.visible = .t.
            this.oParentObject.w_ZoomValueN.visible = .f.
        endcase
        ENDWITH
        select (i_OldCursor)
        i_retcode = 'stop'
        i_retval = this.w_VALUEFILTERTYPE
        return
      case this.pCommandExec="Search"
        this.oParentObject.RunDeactivateRelease = .F.
        this.w_OBJFORM.MarkPos()     
        if this.w_OBJFORM.NumRow()>0
          this.w_OBJFORM.__dummy__.SetFocus()     
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if NOT this.w_EOF and FOUND()
            this.w_POSITION = RECNO()
          else
            if this.oParentObject.w_Searchwrap="S"
              this.w_POSITION = 1
              select (this.w_OBJFORM.cTrsName)
              GO (this.w_POSITION)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if FOUND()
                this.w_POSITION = RECNO()
              else
                Cp_ErrorMsg("Valore non trovato")
                this.w_POSITION = this.w_OLDPOSITION
              endif
            else
              Cp_ErrorMsg("Valore non trovato")
              this.w_POSITION = this.w_OLDPOSITION
            endif
          endif
          select (this.w_OBJFORM.cTrsName)
          GO (this.w_POSITION)
          this.w_OBJFORM.oPgFrm.Page1.oPag.oBody.SetCurrentRow()     
          this.w_OBJFORM.oPgFrm.Page1.oPag.oBody.SetFullFocus()     
          this.w_OBJFORM.oPgFrm.Page1.oPag.oBody.Refresh()     
          this.oParentObject.RunDeactivateRelease = .T.
        else
          Cp_ErrorMsg("Valore non trovato")
        endif
        if vartype(this.oParentObject)="O"
          if i_curform.windowtype=1
            this.oParentObject.EcpQuit()
          else
            this.oParentObject.SetFocusOnFirstControl()
            if this.oParentObject.oPgFrm.Pages[1].oPag.oStr_StringAdvanced.CAPTION = cp_Translate("Nascondi filtri avanzati")
              this.oParentObject.oPgFrm.Pages[1].oPag.oStr_StringAdvanced.Click()
            endif
            this.OparentObject.oTimerInform.Enabled=.t.
          endif
        endif
      case this.pCommandExec="Filter"
        this.w_OBJFORM.MarkPos()     
        if this.oParentObject.w_RemPrevFilter="S"
          this.oParentObject.oParentObject.Parent.BlankFilter()
          select (this.w_OBJFORM.cTrsName)
          SET FILTER TO
        endif
        local w_ColumnSearch, w_Operand, w_SearchValueFilter 
 w_ColumnSearch = this.oParentObject.oParentObject.oObjColumn.ControlSource 
 w_Operand=iif(this.oParentObject.w_TypeFilter="=" and this.oParentObject.w_CTRLTYPE="C", "==", this.oParentObject.w_TypeFilter)
        select (this.w_OBJFORM.cTrsName)
        do case
          case this.oParentObject.w_CTRLTYPE="D"
            w_SearchValueFilter = "CTOD("+cp_ToStrODBC(DTOC(CP_TODATE(this.oParentObject.w_SearchValueD)))+")"
            SET FILTER TO &w_ColumnSearch &w_Operand &w_SearchValueFilter
          case this.oParentObject.w_CTRLTYPE="N"
            w_SearchValueFilter = cp_TOSTRODBC(this.oParentObject.w_SearchValueN)
            SET FILTER TO &w_ColumnSearch &w_Operand &w_SearchValueFilter
          otherwise
            w_SearchValueFilter = cp_TOSTRODBC(UPPER(ALLTRIM(this.oParentObject.w_SearchValue)))
            do case
              case this.oParentObject.w_TypeFilter="Like %"
                SET FILTER TO LIKE("*"+&w_SearchValueFilter+"*",UPPER(ALLTRIM( &w_ColumnSearch )))
              case this.oParentObject.w_TypeFilter="Like"
                SET FILTER TO &w_SearchValueFilter == UPPER(ALLTRIM( LEFT( &w_ColumnSearch , LEN(&w_SearchValueFilter))))
              case this.oParentObject.w_TypeFilter="Not Like %"
                SET FILTER TO NOT( &w_SearchValueFilter $ UPPER(ALLTRIM( &w_ColumnSearch )))
              case this.oParentObject.w_TypeFilter="Not Like"
                SET FILTER TO &w_SearchValueFilter <> UPPER(ALLTRIM( LEFT( &w_ColumnSearch , LEN(&w_SearchValueFilter))))
              otherwise
                SET FILTER TO ALLTRIM( &w_ColumnSearch ) &w_Operand UPPER( &w_SearchValueFilter )
            endcase
        endcase
        this.w_POSITION = RECNO()
        COUNT TO this.w_NUMREC
        if this.w_NUMREC=0
          SET FILTER TO
          this.w_OBJFORM.RePos()     
          Ah_ErrorMsg("Non sono presenti righe di dettaglio per il filtro selezionato")
        else
          if this.w_POSITION>RECCOUNT(this.w_OBJFORM.cTrsName)
            this.w_OBJFORM.SetRow(RECCOUNT(this.w_OBJFORM.cTrsName))     
          else
            this.w_OBJFORM.SetRow(this.w_POSITION)     
          endif
          if vartype(this.oParentObject.oParentObject.oWhereFilter)<>"O"
            this.oParentObject.oParentObject.Newobject("oWhereFilter", "Image")
            this.oParentObject.oParentObject.oWhereFilter.Name="oWhereFilter"
            this.oParentObject.oParentObject.oWhereFilter.BackStyle = 0
            this.oParentObject.oParentObject.oWhereFilter.Left = 0
            this.oParentObject.oParentObject.oWhereFilter.Top = 1
            this.oParentObject.oParentObject.oWhereFilter.Picture = cp_GetBtnWhereImage()
          endif
          this.oParentObject.oParentObject.oWhereFilter.visible=.t.
        endif
        this.w_OBJFORM.Refresh()     
        select (i_OldCursor)
        if vartype(this.oParentObject)="O"
          this.oParentObject.EcpQuit()
        endif
      case this.pCommandExec="LoadZoom"
        local w_ColumnSearch 
 w_ColumnSearch = this.oParentObject.oParentObject.oObjColumn.ControlSource
        this.w_OBJFORM.MarkPos()     
        do case
          case this.oParentObject.w_CTRLTYPE="D"
            this.w_ZOOM = this.oParentObject.w_ZoomValue
            select distinct &w_ColumnSearch as VALUEZOOM from (this.w_OBJFORM.cTrsName) into cursor elemzoom where &w_ColumnSearch is not null order by 1 desc
          case this.oParentObject.w_CTRLTYPE="N"
            this.w_ZOOM = this.oParentObject.w_ZoomValueN
            select distinct &w_ColumnSearch as VALUEZOOM from (this.w_OBJFORM.cTrsName) into cursor elemzoom order by 1
          otherwise
            this.w_ZOOM = this.oParentObject.w_ZoomValue
            select distinct ALLTRIM( &w_ColumnSearch ) as VALUEZOOM from (this.w_OBJFORM.cTrsName) into cursor elemzoom where &w_ColumnSearch is not null and not empty( &w_ColumnSearch ) order by 1
        endcase
        if USED("elemzoom")
          * --- Cancello il contenuto dello Zoom
          Select (this.w_ZOOM.cCursor)
          Zap
          * --- Scan del cursore di appoggio e copia su cursore zoom
          Select elemzoom
          Go Top
          Scan
          Scatter Memvar
          Select (this.w_ZOOM.cCursor)
          Append blank
          Gather memvar
          Endscan
          USE IN elemzoom
          Select (this.w_ZOOM.cCursor) 
 Go Top
          this.w_zoom.grd.refresh
          this.w_ZOOM.grd.Refresh()     
        endif
        this.w_OBJFORM.RePos()     
      case this.pCommandExec="Select"
        this.w_OBJFORM.MarkPos()     
        if this.oParentObject.w_RemPrevFilter="S"
          this.oParentObject.oParentObject.Parent.BlankFilter()
          select (this.w_OBJFORM.cTrsName)
          SET FILTER TO
        endif
        local w_ColumnSearch, w_Operand, w_FilterSearch 
 w_ColumnSearch = this.oParentObject.oParentObject.oObjColumn.ControlSource 
 w_FilterSearch=""
        if this.oParentObject.w_CTRLTYPE="N"
          this.w_ZOOM = this.oParentObject.w_ZoomValueN
        else
          this.w_ZOOM = this.oParentObject.w_ZoomValue
        endif
        this.w_FIRSTREC = .t.
        Select (this.w_ZOOM.cCursor)
        SCAN FOR xchk=1
        if this.w_FIRSTREC
          this.w_FIRSTREC = .f.
          w_FilterSearch = "("
        else
          w_FilterSearch = w_FilterSearch + " OR "
        endif
        Select (this.w_ZOOM.cCursor)
        do case
          case this.oParentObject.w_CTRLTYPE="D"
            w_FilterSearch = w_FilterSearch + ALLTRIM(this.oParentObject.oParentObject.oObjColumn.ControlSource) + " = CTOD(" + cp_ToStrODBC(CP_TODATE(LEFT(VALUEZOOM,10))) + ")"
          case this.oParentObject.w_CTRLTYPE="N"
            w_FilterSearch = w_FilterSearch + ALLTRIM(this.oParentObject.oParentObject.oObjColumn.ControlSource) + " = " + cp_ToStrODBC(VALUEZOOM)
          otherwise
            w_FilterSearch = w_FilterSearch + "ALLTRIM(" + ALLTRIM(this.oParentObject.oParentObject.oObjColumn.ControlSource) + ") == CHRTRAN(" + cp_ToStrODBC(CHRTRAN(ALLTRIM(VALUEZOOM),CHR(39),CHR(255)))+",CHR(255),CHR(39))"
        endcase
        ENDSCAN
        select (this.w_OBJFORM.cTrsName)
        if NOT EMPTY(w_FilterSearch)
          w_FilterSearch = w_FilterSearch + ")"
          SET FILTER TO &w_FilterSearch
        endif
        this.w_POSITION = RECNO()
        COUNT TO this.w_NUMREC
        if this.w_NUMREC=0
          SET FILTER TO
          this.w_OBJFORM.RePos()     
          Ah_ErrorMsg("Non sono presenti righe di dettaglio per il filtro selezionato")
        else
          this.w_OBJFORM.SetRow(this.w_POSITION)     
          if vartype(this.oParentObject.oParentObject.oWhereFilter)<>"O"
            this.oParentObject.oParentObject.Newobject("oWhereFilter", "Image")
            this.oParentObject.oParentObject.oWhereFilter.Name="oWhereFilter"
            this.oParentObject.oParentObject.oWhereFilter.BackStyle = 0
            this.oParentObject.oParentObject.oWhereFilter.Left = 0
            this.oParentObject.oParentObject.oWhereFilter.Top = 1
            this.oParentObject.oParentObject.oWhereFilter.Picture = cp_GetBtnWhereImage()
          endif
          this.oParentObject.oParentObject.oWhereFilter.visible=.t.
        endif
        this.w_OBJFORM.Refresh()     
        select (i_OldCursor)
        if vartype(this.oParentObject)="O"
          this.oParentObject.EcpQuit()
        endif
      case this.pCommandExec="Checked"
        if this.oParentObject.w_CTRLTYPE="N"
          this.w_ZOOM = this.oParentObject.w_ZoomValueN
        else
          this.w_ZOOM = this.oParentObject.w_ZoomValue
        endif
        Select (this.w_ZOOM.cCursor)
        this.w_OLDPOSITION = RECNO()
        COUNT FOR xchk=1 TO this.w_POSITION
        this.oParentObject.w_CHECKED = this.w_POSITION>0
        if NOT this.oParentObject.w_CHECKED AND this.oParentObject.w_CtrlType="C"
          this.oParentObject.w_TypeFilter = IIF(g_RicPerCont="T", "Like %", "Like")
        endif
        if this.oParentObject.w_CHECKED
          this.oParentObject.w_SearchValue = ""
          this.oParentObject.w_SearchValueD = null
          this.oParentObject.w_SearchValueN = 0
        endif
        Select (this.w_ZOOM.cCursor)
        GO this.w_OLDPOSITION
    endcase
    this.w_OBJFORM = .null.
    select (i_OldCursor)
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    select (this.w_OBJFORM.cTrsName)
    this.w_OLDPOSITION = RECNO()
    this.w_EOF = .f.
    do case
      case this.w_OLDPOSITION=RECCOUNT()
        this.w_EOF = .t.
      case this.w_OLDPOSITION=1 and not this.oParentObject.w_SecondSearch
        this.oParentObject.w_SecondSearch = .t. 
        GO this.w_OLDPOSITION
      otherwise
        this.oParentObject.w_SecondSearch = .f. 
        GO this.w_OLDPOSITION+1
    endcase
    if NOT EOF()
      local w_ColumnSearch, w_Operand 
 w_ColumnSearch = this.oParentObject.oParentObject.oObjColumn.ControlSource 
 w_Operand=iif(this.oParentObject.w_TypeFilter="=", "==", this.oParentObject.w_TypeFilter)
      do case
        case this.oParentObject.w_CTRLTYPE="D"
          LOCATE FOR NVL( &w_ColumnSearch , cp_CharToDate("  -  -    ")) &w_Operand NVL( this.oParentObject.w_SearchValueD , cp_CharToDate("  -  -    ")) REST
        case this.oParentObject.w_CTRLTYPE="N"
          LOCATE FOR &w_ColumnSearch &w_Operand this.oParentObject.w_SearchValueN REST
        otherwise
          do case
            case this.oParentObject.w_TypeFilter="Like %"
              LOCATE FOR UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) $ UPPER(ALLTRIM( &w_ColumnSearch )) REST
            case this.oParentObject.w_TypeFilter="Like"
              LOCATE FOR UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) == UPPER(ALLTRIM( LEFT( &w_ColumnSearch , LEN(ALLTRIM(this.oParentObject.w_SearchValue))))) REST
            case this.oParentObject.w_TypeFilter="Not Like %"
              LOCATE FOR NOT(UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) $ UPPER(ALLTRIM( &w_ColumnSearch ))) REST
            case this.oParentObject.w_TypeFilter="Not Like"
              LOCATE FOR UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) <> UPPER(ALLTRIM( LEFT( &w_ColumnSearch , LEN(ALLTRIM(this.oParentObject.w_SearchValue))))) REST
            otherwise
              LOCATE FOR UPPER(ALLTRIM( &w_ColumnSearch )) &w_Operand UPPER(ALLTRIM(this.oParentObject.w_SearchValue)) REST
          endcase
      endcase
    endif
  endproc


  proc Init(oParentObject,pCommandExec)
    this.pCommandExec=pCommandExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCommandExec"
endproc
