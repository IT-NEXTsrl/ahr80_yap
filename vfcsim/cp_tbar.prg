* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_TBAR
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Toolbar standard
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

* --- Definizione classe ToolBar
Define Class CPToolBar As CommandBar

    oStatusToolbar = .Null.
	bOnDestroy = .f.
	
    Procedure Init(bStyle)
        DoDefault(m.bStyle And i_ThemesManager.IsInThemes(i_VisualTheme,This))
        Local oBtn
        With This
            .Height=10

            *---Prior Btn
            m.oBtn = .AddButton("b7")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "prior.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpPrior'"
            m.oBtn.ToolTipText = cp_Translate(MGS_PREVIOUS_RECORD)+" (F7)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *---Next Btn
            m.oBtn = .AddButton("b8")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "next.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpNext'"
            m.oBtn.ToolTipText = cp_Translate(MSG_NEXT_RECORD)+" (F8)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Separatore
            .AddObject("s5","Separator")
            #If Version(5)>=900
                .s5.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            #Endif

            *--- Edit Btn
            m.oBtn = .AddButton("b2")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "modiftbl.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpEdit'"
            m.oBtn.ToolTipText = cp_Translate(MSG_CHANGE)+" (F3)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Load Btn
            m.oBtn = .AddButton("b3")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "addtabl.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpLoad'"
            m.oBtn.ToolTipText = cp_Translate(MSG_LOAD)+" (F4)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Save Btn
            m.oBtn = .AddButton("b4")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "save.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpSave'"
            m.oBtn.ToolTipText = cp_Translate(MSG_SAVE)+" (F10)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Separatore
            .AddObject("s2","Separator")
            #If Version(5)>=900
                .s2.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            #Endif
            *---Delete Btn
            m.oBtn = .AddButton("b5")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "canc.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpDelete'"
            m.oBtn.ToolTipText = cp_Translate(MSG_DELETE)+" (F5)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *---Delete Row Btn
            m.oBtn = .AddButton("b5b")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "canc_riga.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpF6'"
            m.oBtn.ToolTipText = cp_Translate(MSG_DELETE)+" (F6)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Separatore
            .AddObject("s3","Separator")
            #If Version(5)>=900
                .s3.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            #Endif

            *---Print Btn
            m.oBtn = .AddButton("b6")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "print.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpPrint'"
            m.oBtn.ToolTipText = cp_Translate(MSG_PRINT) +" (F2)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Separatore
            .AddObject("s4","Separator")
            #If Version(5)>=900
                .s4.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            #Endif

            *---Zoom Btn
            m.oBtn = .AddButton("b9")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "zoom.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpZoom'"
            m.oBtn.ToolTipText = cp_Translate(MSG_ZOOM)+" (F9)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Separatore
            .AddObject("s6","Separator")
            #If Version(5)>=900
                .s6.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            #Endif

            *---PgUp Btn
            m.oBtn = .AddButton("b10")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "PgUp.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpPgUp'"
            m.oBtn.ToolTipText = cp_Translate(MSG_PAGE_UP)+" (PgUp)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *---PgDn Btn
            m.oBtn = .AddButton("b11")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "PgDn.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpPgDn'"
            m.oBtn.ToolTipText = cp_Translate(MSG_PAGE_DOWN)+" (PgDn)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Separatore
            .AddObject("s7","Separator")
            #If Version(5)>=900
                .s7.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            #Endif
            *---Filter Btn
            m.oBtn = .AddButton("b12")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "filter.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpFilter'"
            m.oBtn.ToolTipText = cp_Translate(MSG_FILTER)+" (F12)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Separatore
            .AddObject("s8","Separator")
            #If Version(5)>=900
                .s8.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            #Endif

            *---Exit Btn
            m.oBtn = .AddButton("b13")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "escTB.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpQuit'"
            m.oBtn.ToolTipText = cp_Translate(MSG_EXIT)+" (Esc)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *-* *--- Separatore
            .AddObject("s9","Separator")
            #If Version(5)>=900
                .s9.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            #Endif

            *--- Help Btn
            m.oBtn = .AddButton("b1")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "help.bmp"
            m.oBtn._OnClick = "do cp_help"
            m.oBtn.ToolTipText = cp_Translate(MSG_HELP)+" (F1)"
            m.oBtn.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            *--- Separatore
            .AddObject("s1","Separator")
            #If Version(5)>=900
                .s1.Visible=Vartype(g_Service)<>'C' Or g_Service='N'
            #Endif

			*--- Close Service Btn
            m.oBtn = .AddButton("bcs")
            m.oBtn.Caption = ""
            m.oBtn.cImage =  "escTB.bmp"
            m.oBtn._OnClick = [p_ServiceArg=""]
            m.oBtn.ToolTipText = cp_Translate(MSG_PROCEDURE_EXIT)
            m.oBtn.Enabled=.T.
            m.oBtn.Visible=Vartype(g_Service)='C' And g_Service='S'

            Local nPixel, cChr
            nPixel = 6
            If i_nTBTNH>16
                cChr = Chr(10)
            Else
                cChr = ' '
            Endif
            *-* * --- Box Utente
            *-* .Addobject("boxute","CpEditBox")
            *-* .Boxute.SpecialEffect=1
            *-* .boxute.alignment=0
            *-* .boxute.value=MSG_TB_USER+cChr+alltrim(str(i_codute))
            *-* .boxute.enabled=.f.
            *-* .boxute.disabledforecolor=rgb(157,157,161)
            *-* .boxute.Bordercolor=RGB(165,172,178)
            *-* .boxute.fontname="Arial"
            *-* .boxute.fontsize=7
            *-* .boxute.width=40
            *-* .boxute.height=i_nTBTNH+nPixel
            *-* .boxute.DisabledBackColor=rgb(255,255,255)
            *-* .boxute.fontbold=.f.
            *-* .boxute.Visible=.t.
            *-* * --- Box Azienda
            *-* .AddObject("boxazi","CpEditBox")
            *-* .boxazi.alignment=0
            *-* .boxazi.value=MSG_TB_COMPANY+cChr+alltrim(i_codazi)
            *-* .boxazi.enabled=.f.
            *-* .boxazi.SpecialEffect=1
            *-* .boxazi.disabledforecolor=rgb(157,157,161)
            *-* .boxazi.Bordercolor=RGB(165,172,178)
            *-* .boxazi.fontname="Arial"
            *-* .boxazi.fontsize=7
            *-* .boxazi.width=45
            *-* .boxazi.height=i_nTBTNH+nPixel
            *-* .boxazi.DisabledBackColor=rgb(255,255,255)
            *-* .boxazi.fontbold=.f.
            *-* .boxazi.visible=.t.
            *--- Box Data
            *-* .addobject("boxdata","CpEditBox")
            *-* .boxdata.alignment=0
            *-* .boxdata.value=MSG_TB_DATE+cChr+Alltrim(dtoc(i_datsys))
            *-* .boxdata.enabled=.f.
            *-* .boxdata.SpecialEffect=1
            *-* .boxdata.disabledforecolor=rgb(157,157,161)
            *-* .boxdata.Bordercolor=RGB(165,172,178)
            *-* .boxdata.fontname="Arial"
            *-* .boxdata.fontsize=7
            *-* .boxdata.width=55
            *-* .boxdata.height=i_nTBTNH+nPixel
            *-* .boxdata.DisabledBackColor=rgb(255,255,255)
            *-* .boxdata.visible=.t.
            .Caption=cp_Translate(MSG_TOOLBAR)
            .ControlBox=.F.

            *--- Se non ho il ThemesManager devo utilizzare le vecchie bmp
            If !(Vartype(i_ThemesManager)=='O')
                .b1.Picture="help.bmp"
                .b2.Picture="modiftbl.bmp"
                .b3.Picture="addtabl.bmp"
                .b4.Picture="save.bmp"
                .b5.Picture="canc.bmp"
                .b5b.Picture="canc_riga.bmp"
                .b6.Picture="print.bmp"
                .b7.Picture="prior.bmp"
                .b8.Picture="next.bmp"
                .b9.Picture="zoom.bmp"
                .b10.Picture="PgUp.bmp"
                .b11.Picture="PgDn.bmp"
                .b12.Picture="filter.bmp"
                .b13.Picture="esc.bmp"
            Endif
            *---- Status Toolbar
            *.Dock(i_nCPToolBarPos)
        Endwith
        m.oBtn = .Null.
    Endproc

	PROCEDURE Destroy()
		DODEFAULT()
		this.bOnDestroy = .t.
		IF !ISNULL(This.oStatusToolbar)
			This.oStatusToolbar.Release()
		endif
		This.oStatusToolbar = .Null.
	EndPRoc

    Procedure Dock(nDock, nLeft, nTop)
        If Isnull(This.oStatusToolbar)
        	If i_VisualTheme<>-1 AND !This.bOnDestroy AND i_ThemesManager.GetProp(124)=0 And !(Vartype(g_Service)='C' And g_Service='S')
	            This.oStatusToolbar = Createobject("cpStatusToolbar")
	            DoDefault(nDock, -1, This.oStatusToolbar.Height+10)
	        EndIf
        Else
            DoDefault(nDock, -1, This.oStatusToolbar.Height+10)
        Endif
    Endproc

    Procedure ChangeSettings()
        DoDefault()
        IF !ISNULL(This.oStatusToolbar)
        	This.oStatusToolbar.ChangeSettings()
        EndIf
        *-*        Local cChr, nPixel
        *-*        nPixel = 4
        *-*        With This
        *-*            If i_nTBTNH>16
        *-*                cChr = Chr(10)
        *-*                .Boxute.Width=40
        *-*                .boxazi.Width=45
        *-*                .boxdata.Width=55
        *-*                .Boxute.Value=MSG_TB_USER+cChr+Alltrim(Str(i_codute))
        *-*                .boxazi.Value=MSG_TB_COMPANY+cChr+Alltrim(i_codazi)
        *-*                .boxdata.Value=MSG_TB_DATE+cChr+Alltrim(Dtoc(i_datsys))
        *-*            Else
        *-*                cChr = ' '
        *-*                .Boxute.Value=MSG_TB_USER+cChr+Alltrim(Str(i_codute))
        *-*                .boxazi.Value=MSG_TB_COMPANY+cChr+Alltrim(i_codazi)
        *-*                .boxdata.Value=MSG_TB_DATE+cChr+Alltrim(Dtoc(i_datsys))
        *-*                .Boxute.Width=20+Fontmetric(6, .Boxute.FontName, .Boxute.FontSize)*Len(.Boxute.Value)
        *-*                .boxazi.Width=20+Fontmetric(6, .boxazi.FontName, .boxazi.FontSize)*Len(.boxazi.Value)
        *-*                .boxdata.Width=20+Fontmetric(6, .boxdata.FontName, .boxdata.FontSize)*Len(.boxdata.Value)
        *-*            Endif
        *-*            .Boxute.Height=i_nTBTNH+nPixel
        *-*            .boxazi.Height=i_nTBTNH+nPixel
        *-*            .boxdata.Height=i_nTBTNH+nPixel
        *-*        Endwith
    Endproc

    * --- Disabilita o abilita tutti i tasti
    Proc Enable
        Parameter m.Enable
        This.b2.Enabled = m.Enable
        This.b3.Enabled = m.Enable
        This.b4.Enabled = m.Enable
        This.b5.Enabled = m.Enable
        This.b5b.Enabled = m.Enable
        This.b6.Enabled = m.Enable
        This.b7.Enabled = m.Enable
        This.b8.Enabled = m.Enable
        This.b9.Enabled = m.Enable
        This.b10.Enabled = m.Enable
        This.b11.Enabled = m.Enable
        This.b12.Enabled = m.Enable
        This.b13.Enabled = m.Enable
    Endproc
    * --- Attivazione tasti Query
    Proc SetQuery(p_exist,p_print,p_zoom)
        This.b2.Enabled  = .T. && p_exist
        This.b3.Enabled  = .T.
        This.b4.Enabled  = .F.
        This.b5.Enabled  = .T. && p_exist
        This.b5b.Enabled  = .F. && p_exist
        This.b6.Enabled  = .T. && p_print
        This.b7.Enabled  = .T.
        This.b8.Enabled  = .T.
        This.b9.Enabled  = .T. && p_zoom
        This.b10.Enabled = .T.
        This.b11.Enabled = .T.
        This.b12.Enabled = .T.
        This.b13.Enabled = .T.
    Endproc
    * --- Attivazione tasti Edit
    Proc SetEdit()
        This.b2.Enabled=.F.
        This.b3.Enabled=.F.
        This.b4.Enabled=.T.
        This.b5.Enabled=.F.
        This.b5b.Enabled=.T.
        This.b6.Enabled=.F.
        This.b7.Enabled=.F.
        This.b8.Enabled=.F.
        This.b9.Enabled=.F.
        This.b10.Enabled=.T.
        This.b11.Enabled=.T.
        This.b12.Enabled=.F.
        This.b13.Enabled=.T.
    Endproc
    * --- Attivazione tasti Filtro
    Proc SetFilter()
        This.b2.Enabled=.F.
        This.b3.Enabled=.F.
        This.b4.Enabled=.T.
        This.b5.Enabled=.F.
        This.b6.Enabled=.F.
        This.b7.Enabled=.F.
        This.b8.Enabled=.F.
        This.b9.Enabled=.F.
        This.b10.Enabled=.T.
        This.b11.Enabled=.T.
        This.b12.Enabled=.F.
        This.b13.Enabled=.T.
    Endproc
    *--- Traduzione tooltip
    Procedure LocalizeString
        With This
            *--- Help Btn
            .b1.ToolTipText = cp_Translate(MSG_HELP)+" (F1)"
            *--- Edit Btn
            .b2.ToolTipText = cp_Translate(MSG_CHANGE)+" (F3)"
            *--- Load Btn
            .b3.ToolTipText = cp_Translate(MSG_LOAD)+" (F4)"
            *--- Save Btn
            .b4.ToolTipText = cp_Translate(MSG_SAVE)+" (F10)"
            *---Delete Btn
            .b5.ToolTipText = cp_Translate(MSG_DELETE)+" (F5)"
            *---Delete row Btn
            .b5b.ToolTipText = cp_Translate(MSG_DELETE_ROW)+" (F6)"
            *---Print Btn
            .b6.ToolTipText = cp_Translate(MSG_PRINT) +" (F2)"
            *---Prior Btn
            .b7.ToolTipText = cp_Translate(MGS_PREVIOUS_RECORD)+" (F7)"
            *---Next Btn
            .b8.ToolTipText = cp_Translate(MSG_NEXT_RECORD)+" (F8)"
            *---Zoom Btn
            .b9.ToolTipText = cp_Translate(MSG_ZOOM)+" (F9)"
            *---PgUp Btn
            .b10.ToolTipText = cp_Translate(MSG_PAGE_UP)+" (PgUp)"
            *---PgDn Btn
            .b11.ToolTipText = cp_Translate(MSG_PAGE_DOWN)+" (PgDn)"
            *---Filter Btn
            .b12.ToolTipText = cp_Translate(MSG_FILTER)+" (F12)"
            *---Filter Btn
            .b13.ToolTipText = cp_Translate(MSG_EXIT)+" (Esc)"
        Endwith
    Endproc
Enddefine

Define Class StdTableGroupCombo As StdCombo
    Dimension combovalues[1]
    nValues=0
    RowSourceType=0
    cTable='GROUPSROLE.VQR'
    cKey='code'
    cValue='name'
    cOrderBy='code'
    tablefilter=''
    xDefault=0
    bSetFont = .T.
    Proc Init()
        If Vartype(This.bNoBackColor)='U'
            This.BackColor=i_nEBackColor
        Endif
        This.ToolTipText=cp_Translate(This.ToolTipText)
        Local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
        Local i_fk,i_fd
        i_curs=Sys(2015)
        If Not Empty(This.cTable) And Lower(i_codazi)<>'xxx'
            vq_exec(This.cTable,.Null.,i_curs)
        Endif
        i_fk=This.cKey
        i_fd=This.cValue
        If Used(i_curs)
            Select (i_curs)
            This.nValues=Iif(Reccount()>1,Reccount()+1,1)
            For i_j=Alen(This.combovalues) To 1 Step -1
                Adel(This.combovalues,i_j)
                If This.ListCount>0 And i_j <= This.ListCount
                    This.RemoveItem(i_j)
                Endif
            Endfor
            Dimension This.combovalues[MAX(1,this.nValues)]
            i_bCharKey=Type(i_fk)='C'
            If This.nValues>1
                This.AddItem(' ')
                This.combovalues[1]=Iif(i_bCharKey,Space(1),0)
            Else
                i_GROUPROLE=Code
            Endif
            Do While !Eof()
                This.AddItem(Iif(Type(i_fd)='C',Alltrim(&i_fd),Alltrim(Str(&i_fd))))
                If i_bCharKey
                    This.combovalues[iif(this.nValues=1,0,1)+recno()]=Trim(&i_fk)
                Else
                    This.combovalues[iif(this.nValues=1,0,1)+recno()]=&i_fk
                Endif
                Skip
            Enddo
            This.SetRadio()
            This.Enabled=(This.nValues<>1)
            Select (i_curs)
            Use
        Else
            This.Enabled=.F.
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
    Func RadioValue()
        If This.Value>0 And This.Value<=This.nValues
            Return(This.combovalues[this.value])
        Endif
        Return(This.xDefault)
    Func SetRadio()
        #If Version(5)>=900
            This.Value=Ascan(This.combovalues,i_GROUPROLE, -1, -1, -1, 6)
        #Else
            Local cSetExact
            cSetExact=Set("EXACT")
            Set Exact On
            This.Value=Ascan(This.combovalues,i_GROUPROLE)
            Set Exact &cSetExact
        #Endif
        Return
    Func GetRadio()
        Local i_n
        i_n=This.cFormvar
        i_GROUPROLE=This.RadioValue()
        Return(.T.)
        *
        * --- EVENTI/METODI VFP
        *
        * --- Quando prende il Focus
    Proc GotFocus()
        Local i_var
        Local i_oldvalue
        * --- il valore puo' essere cambiato dalla funzione di "Before"
        i_var=This.cFormvar
        If This.RadioValue()<>i_GROUPROLE
            i_GROUPROLE=This.Value
        Endif
        If Vartype(This.bNoBackColor)='U'
            This.BackColor = i_nBackColor
        Endif
        If Vartype(g_DispCnt)='N'
            This.DisplayCount = g_DispCnt
        Endif
        Return
    Proc LostFocus()
        If Vartype(This.bNoBackColor)='U'
            This.BackColor=i_nEBackColor
        Endif
        Return
    Func When()
        Local i_bRes
        * --- Procedure standard del campo
        i_bRes=.T.
        i_bRes = This.mCond()
        Return(i_bRes)
    Func Valid()
        i_GROUPROLE=This.Value
        cp_menu()
        cp_LoadTableSecArray()
        Return(.T.)
Enddefine

*--- Box di ricerca
#Define PANELVERTICAL_BACKCOLOR	7
#Define TOOLBAR_BTN_HOVER_BORDER	62
#Define TOOLBAR_BTN_DOWN_BORDER	63
#Define SEARCHMENU_HOVER 76

#Define TPM_RIGHTALIGN	0x8
Define Class cpSearchStatusToolbar As Container
    BorderWidth=0
    BackStyle=0
    Height=24
    Width = 290

    bClick = .F.
    cCursorNameMenu = ''
    cCursorNameRecent = ''
    oToolbar = .Null.
    EmptySearch = .F.

    Add Object oImgSearch As Image With Width=22,Height=22, Top=1, Left=1
    Add Object oTextToSearch As TextBox With Width=234, Height=24, Top=0, Left=30, FontName="Open Sans", FontSize=9,;
        bordercolor=Rgb(217,217,217), ForeColor=Rgb(69,69,69), Value = MSG_SEARCH, EmptySearch = .T., SpecialEffect = 1
    Add Object oBtnSearch As BtnSearch With Width=16, Height=24, Top=1, Left=1, BorderColor=Rgb(217,217,217)

    Procedure Init
        With This
            .oTextToSearch.ToolTipText = cp_Translate("Voce di menu da cercare (minimo tre caratteri)")
            .oImgSearch.Picture = Forceext(cp_GetStdImg("searchbox", 'png'), 'png')
            .oBtnSearch.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
            .oBtnSearch.ToolTipText = cp_Translate("Cerca")
            i_ThemesManager.PutBmp(.oBtnSearch.ImgBtn, "OVERFLOWPANEL_MENUBUTTON_PICTURE.Bmp", 16)
            .oBtnSearch.ImgBtn.ToolTipText = cp_Translate("Cerca")
            .oBtnSearch.Move(.oTextToSearch.Left + .oTextToSearch.Width - 1,.oTextToSearch.Top)
            .oToolbar=Thisform
        Endwith
    Endproc

    Procedure Enabled_Assign(xValue)
        With This
            .Enabled = m.xValue
            .oTextToSearch.Enabled = m.xValue
            .oBtnSearch.Enabled = m.xValue
        Endwith
    Endproc

    Function IsMouseHover(x, Y)
        With This
            Return (m.x>.Left And m.x<.Left+.Width And m.y>.Top And m.y< .Top+.Height)
        Endwith
    Endfunc

    Procedure _MouseHover(xValue, x, Y)
        With This
            If !.bClick
                If m.xValue And .IsMouseHover(m.x, m.y) And .Enabled
                    *.oTextToSearch.BorderColor = i_ThemesManager.GetProp(TOOLBAR_BTN_HOVER_BORDER)
                    .oBtnSearch.BackColor = i_ThemesManager.GetProp(SEARCHMENU_HOVER)
                    *.oBtnSearch.BorderColor = .oTextToSearch.BorderColor
                Else
                    .oBtnSearch.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
                    *.oTextToSearch.BorderColor = Rgb(255,255,255)
                    *.oBtnSearch.BorderColor = Rgb(255,255,255)
                Endif
            Endif
        Endwith
    Endproc

    Procedure oTextToSearch.GotFocus
        This.Parent.bClick = .T.
        This.ForeColor = Rgb(0,0,0)
        This.Parent.oBtnSearch.BackColor = i_ThemesManager.GetProp(SEARCHMENU_HOVER)
        This.Value = ""
        This.EmptySearch= .F.
    Endproc

    Procedure oTextToSearch.LostFocus
        This.Parent.bClick = .F.
        This.Parent._MouseHover(.F.)
        This.ForeColor = Rgb(69,69,69)
        This.Parent.oBtnSearch.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
        This.Value =  MSG_SEARCH
        This.EmptySearch= .T.
    Endproc

    Procedure oBtnSearch.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.OnClick()
    Endproc

    Procedure oBtnSearch.ImgBtn.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Parent.OnClick()
    Endproc

    Procedure oBtnSearch.ImgBtn.MouseEnter(nButton, nShift, nXCoord, nYCoord)
        With This.Parent.Parent
            ._MouseHover(.T., nXCoord, nYCoord)
        Endwith
    Endproc

    Procedure oBtnSearch.ImgBtn.MouseLeave(nButton, nShift, nXCoord, nYCoord)
        With This.Parent.Parent
            ._MouseHover(.F., nXCoord, nYCoord)
        Endwith
    Endproc

    *--- Costruisce il path della vece di menu
    Function RetPath(cLvlKey, cVoceMenu,cCursor)
        Local l_cLVLKEY, l_oldArea, l_cCursor, l_Ret, l_i, l_max, l_nOccurs
        l_oldArea = Select()
        l_Ret = ""
        l_cCursor = Sys(2015)
        l_cLVLKEY = Alltrim(m.cLvlKey)
        l_cLVLKEY = Left(m.l_cLVLKEY, 7)
        l_nOccurs = Occurs('.', m.cLvlKey)-1
        For l_i = 1 To m.l_nOccurs
            Select VOCEMENU, Skip From (m.cCursor) Where Alltrim(LVLKEY) == m.l_cLVLKEY Into Cursor (m.l_cCursor)
            If &l_cCursor..Skip
                Select(m.l_oldArea)
                Return Left('@@@@@@@@@@'+Space(254), 254)
            Endif
            If m.l_i <= i_nMaxSearchMenu
                l_Ret = m.l_Ret + cp_Translate(Alltrim(&l_cCursor..VOCEMENU))+'\'
            Endif
            If m.l_Ret=='\' Then
                l_Ret = ""
            Endif
            Use In Select(m.l_cCursor)
            m.l_cLVLKEY = Left(Alltrim(m.cLvlKey), 7+m.l_i*4)
        Next
        Select(m.l_oldArea)
        Return Left(m.l_Ret+Iif(i_nMaxSearchMenu<l_nOccurs, '..\','')+cp_Translate(Alltrim(m.cVoceMenu))+Space(254), 254)
    Endfunc

    Procedure OnClick()
        Local bEmptySearch
        With This
            If .bClick Or .EmptySearch
                .oBtnSearch.BackColor = i_ThemesManager.GetProp(TOOLBAR_BTN_DOWN_BORDER)
                bEmptySearch = .EmptySearch And (Empty(.oTextToSearch.Value) Or .oTextToSearch.EmptySearch)
                If !Empty(.cCursorNameMenu) And Used(.cCursorNameMenu) And (bEmptySearch Or !Empty(.oTextToSearch.Value) And Len(Alltrim(.oTextToSearch.Value))>=3)
                    *--- 1. Voci di menu
                    Local l_oldArea, l_Value, l_cCursor
                    l_oldArea = Select()
                    l_Value = Upper(Alltrim(.oTextToSearch.Value))
                    l_cCursor = .cCursorNameMenu
                    If Vartype(g_DebugSearchMenu) = 'L' And g_DebugSearchMenu
                        *--- Ricerco anche nel campo
                        Select *, LVLKEY As LVLKEYORIG From (m.l_cCursor) Where Directory = 1 And (bEmptySearch Or Upper(NAMEPROC) Like '%'+l_Value+'%' Or Upper(Strtran(cp_Translate(VOCEMENU),'&','')) Like '%'+l_Value+'%') ;
                            INTO Cursor __LISTMENUITEM__ Readwrite
                    Else
                        Select *, LVLKEY As LVLKEYORIG From (m.l_cCursor) Where Directory = 1 And (bEmptySearch Or Upper(Strtran(cp_Translate(VOCEMENU),'&','')) Like '%'+l_Value+'%') ;
                            INTO Cursor __LISTMENUITEM__ Readwrite
                    Endif
                    *--- 2. Recenti
                    Local l_oldArea, l_Value, l_cCursor
                    l_oldArea = Select()
                    l_Value = Upper(Alltrim(.oTextToSearch.Value))
                    l_cCursor = .cCursorNameRecent
                    If Vartype(g_DebugSearchMenu) = 'L' And g_DebugSearchMenu
                        *--- Ricerco anche nel campo
                        Select *, LVLKEY As LVLKEYORIG From (m.l_cCursor) Where Directory = 1 And (bEmptySearch Or Upper(NAMEPROC) Like '%'+l_Value+'%' Or Upper(Strtran(cp_Translate(VOCEMENU),'&','')) Like '%'+l_Value+'%') ;
                            INTO Cursor __LISTMENUITEM_RECENT__ Readwrite
                    Else
                        Select *, LVLKEY As LVLKEYORIG From (m.l_cCursor) Where Directory = 1 And (bEmptySearch Or Upper(Strtran(cp_Translate(VOCEMENU),'&','')) Like '%'+l_Value+'%') ;
                            INTO Cursor __LISTMENUITEM_RECENT__ Readwrite
                    Endif

                    If Used("__LISTMENUITEM__") Or Used("__LISTMENUITEM_RECENT__")
                        *--- Creo popupmenu
                        *--- Elemento radice
                        Local l_oPopupMenu, oMenuItem, l_bFindItem, l_MENUID
                        If Vartype(g_MenuPopupID)="U"
                            Public g_MenuPopupID
                            g_MenuPopupID = 5000
                        Endif
                        g_MenuPopupID = g_MenuPopupID + 5000
                        l_MENUID = g_MenuPopupID

                        If Used("__LISTMENUITEM__") And Used("__LISTMENUITEM_RECENT__")
                            *--- Inserisco separatore tra menu e recenti
                            If Reccount("__LISTMENUITEM__")>0 And Reccount("__LISTMENUITEM_RECENT__")>0
                                Insert Into __LISTMENUITEM__ (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ("",0,"",4,.T.,m.l_MENUID,"","",.F.,.F.)
                            Endif
                            *Insert Into __LISTMENUITEM__ (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED) Values ("Recenti",2,"",1,.T., m.l_MENUID,"","999",.T.,.F.)
                            Select VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED, LVLKEYORIG From __LISTMENUITEM_RECENT__ Where VOCEMENU Not In (Select Chrtran(VOCEMENU,"&","") As VOCEMENU From __LISTMENUITEM__) Into Cursor __LISTMENUITEM_RECENT__ Readwrite
                            Select * From __LISTMENUITEM__ Union All Select * From __LISTMENUITEM_RECENT__ Into Cursor __LISTMENUITEM__ Readwrite
                        Endif

                        *--- Inserisco voce menu principale
                        Insert Into __LISTMENUITEM__ (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED, LVLKEYORIG) Values ("",0,"",0,.T., m.l_MENUID,"","001",.F.,.F.,'')
                        *Insert Into __LISTMENUITEM__ (VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED, LVLKEYORIG) Values ("Voci menu",2,"",1,.T., m.l_MENUID,"","001.002",.T.,.F.,'')
                        *!*	                        SELECT "Voci menu" as VOCEMENU,2 as Level,"" as NAMEPROC,1 as Directory,.T. as Enabled, m.l_MENUID as MENUID,"" as CPBMPNAME,"001.002" as LVLKEY,.T. as Skip,.F. as CHECKED,'' as LVLKEYORIG FROM __LISTMENUITEM__ where Directory=4 UNION ALL SELECT * FROM __LISTMENUITEM__ into CURSOR __LISTMENUITEM__TMP
                        *!*	                        SELECT * FROM __LISTMENUITEM__TMP INTO CURSOR __LISTMENUITEM__ readwrite
                        *!*	                        USE IN SELECT("__LISTMENUITEM__TMP")

                        l_bFindItem = .F.
                        m.l_oPopupMenu= Createobject("cbPopupMenu")
                        m.l_oPopupMenu.oPopupMenu.Init
                        *--- Cambio voce menu con il suo path
                        l_cCursor = .cCursorNameMenu
                        Select This.RetPath(LVLKEYORIG, VOCEMENU, m.l_cCursor) As VOCEMENU, Level, NAMEPROC, Directory, Enabled, MENUID, CPBMPNAME, LVLKEY, Skip, CHECKED From __LISTMENUITEM__;
                            INTO Cursor (m.l_oPopupMenu.oPopupMenu.cCursorName) Readwrite
                        Select * From (m.l_oPopupMenu.oPopupMenu.cCursorName) Where At('@@@@@@@@@@',VOCEMENU) = 0 ;
                            INTO Cursor (m.l_oPopupMenu.oPopupMenu.cCursorName) Readwrite
                        *--- Aggiorno le chiavi
                        Update (m.l_oPopupMenu.oPopupMenu.cCursorName) Set LVLKEY = "001."+Right("000"+Transform(Recno()), 3), MENUID=m.l_MENUID+Recno() Where Directory<>0
                        *--- Ordino per la chiave
                        Select * From (m.l_oPopupMenu.oPopupMenu.cCursorName) Into Cursor (m.l_oPopupMenu.oPopupMenu.cCursorName) Order By LVLKEY
                        m.l_oPopupMenu.oPopupMenu.InitMenuFromCursor()
                        If Recno("__LISTMENUITEM__")>1
                            Local x, Y
                            m.x = cb_GetWindowLeft(.oToolbar.HWnd)
                            m.y = cb_GetWindowTop(.oToolbar.HWnd)
                            m.x = m.x + .Left + .Width - Sysmetric(3) - 1
                            m.y = m.y + .Top + .Height - 1
                            m.l_oPopupMenu.ShowMenu(m.x, m.y, TPM_RIGHTALIGN)
                        Endif
                        Use In Select("__LISTMENUITEM__")
                        Use In Select("__LISTMENUITEM_RECENT__")
                    Endif
                    Select(m.l_oldArea)
                Endif
            Else
                .oBtnSearch.BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
            Endif
        Endwith
    Endproc


Enddefine

#Define WM_WINDOWPOSCHANGING 0x0046
#Define GWL_WNDPROC -4
*--- Status window
Define Class cpStatusToolbar As Form
    bSysForm = .T.

	showwindow = 2
	*dockable=2
	Height = 44
	
    BorderStyle=0
    TitleBar=0
    
    Enabled=.T.
    MinWidth = 800

    ShowTips = .T.
    
    Add Object oLineTop As Line With Top=1, Left=0, Height=0, BorderWidth=3

    Add Object oLblUser As Label With AutoSize=.T., BackStyle=1,FontName="Open Sans", FontSize=11, Top=14, Left=15, MousePointer=15, tooltiptext="Login", Caption=""

    Add Object oLblAzi As Label With AutoSize=.T., BackStyle=1,FontName="Open Sans", FontSize=11, Top=14, Left=250, MousePointer=15, tooltiptext="Cambio contesto", Caption=""

    Add Object oSearchBox As cpSearchStatusToolbar With Top=15

    Add Object oLogo As Image With Picture=".\BMP\logo.png", Width=121, Height=32, Top = 5, tooltiptext="Informazioni"
    *--- Gestione Dock wnd
    hOrigProcParentDockTop = 0
    hParentDockTop = -1

    hOrigProcParentDockLeft = 0
    hParentDockLeft = -1

    hOrigProcParentDockRight = 0
    hParentDockRight = -1

    hOrigProcScreen = 0
    hOrigProcVfp = 0

    KeyPreview=.T.

    Procedure Init
        With This
            .Declare()
            *--- Tiro fuori la form dallo _screen
            SetParent(.HWnd, _vfp.HWnd)

			nStyle = GetWindowLong(.HWnd, -16)
 			= SetWindowLong(.HWnd, -16, BITAND(m.nStyle, BITNOT(0x00800000), BITNOT(0x00C00000)))
            
            .ChangeTheme()
            .Show()
            *--- BindEvent su _screen per gestire il resize e poter abbassare il tutto dell'altezza della form
            .hOrigProcScreen = GetWindowLong(_Screen.HWnd, GWL_WNDPROC)
            Bindevent(_Screen.HWnd, WM_WINDOWPOSCHANGING, This, "OnResizeDock",4)
			BINDEVENT(_screen, "Deactivate", This, "OnDeActivate") 
            ._Resize()
        Endwith
    Endproc

    Procedure QueryUnload
        Nodefault

        *--- unbindevent
    Procedure Destroy
        With This
            If IsWindow(.hParentDockTop)<>0
                Unbindevents(.hParentDockTop, WM_WINDOWPOSCHANGING)
            Endif
            If IsWindow(.hParentDockLeft)<>0
                Unbindevents(.hParentDockLeft, WM_WINDOWPOSCHANGING)
            Endif
            If IsWindow(.hParentDockRight)<>0
                Unbindevents(.hParentDockRight, WM_WINDOWPOSCHANGING)
            Endif
            Unbindevents(_Screen.HWnd, WM_WINDOWPOSCHANGING)
            RAISEEVENT(_screen, "Resize")
        Endwith
    Endproc

    *--- Aggiorno Label
    Procedure ChangeSettings
    	WITH This
	        .oLblUser.Caption = Iif(i_codute>0, cp_msgformat("Ut. %1", Transform(i_codute)+ " - " + LookTab("cpusers", Proper("name"), "code", i_codute)), cp_Translate("Ut."))
	        .oLblUser.tooltiptext = cp_Translate("Login")

	        .oLblAzi.Caption = cp_msgformat("Azienda %1",i_codazi)
	        .oLblAzi.tooltiptext = cp_Translate("Cambio contesto")
	        
	        .oLogo.tooltiptext = cp_Translate("Informazioni")

	        .ChangeTheme()
	    EndWith
    Endproc

	PROCEDURE ChangeTheme()
		WITH This
			If i_VisualTheme<>-1
				.BackColor = i_ThemesManager.GetProp(7)
	            .oLblUser.BackColor = .BackColor
	            .oLblAzi.BackColor = .BackColor	
	            .oLineTop.BorderColor = i_ThemesManager.GetProp(109) 
	            .oLblUser.ForeColor = .oLineTop.BorderColor
           		.oLblAzi.ForeColor = .oLineTop.BorderColor
			EndIf
		EndWith
	EndProc

    *--- Declare API
    Procedure Declare()
        Declare Integer ShowWindow In user32;
            INTEGER HWnd,;
            INTEGER nCmdShow

        Declare Integer SetParent In user32;
            INTEGER hWndChild,;
            INTEGER hWndNewParent

        Declare Integer GetParent In user32 As apiGetParent ;
            INTEGER HWnd

        Declare SHORT GetWindowRect In user32 Integer HWnd, String @ lpRect

        Declare Integer IsWindow In user32 Integer HWnd

        Declare Integer CallWindowProc In user32;
            INTEGER lpPrevWndFunc, Integer hWindow, Long Msg,;
            INTEGER wParam, Integer Lparam
        Declare Integer GetWindowLong In user32;
            INTEGER hWindow, Integer nIndex
		DECLARE INTEGER GetWindow IN user32 INTEGER hwnd, INTEGER wFlag            
    Endproc

	PROCEDURE Activate()
		This.OnDeActivate()

	PROCEDURE DeActivate()
		This.OnDeActivate()

	PROCEDURE OnDeActivate
		*--- Attiva la titlebar di VFP
		SendMessage(_vfp.hWnd, 0x0086, 1,0)


	#DEFINE GW_HWNDLAST 1
	#DEFINE GW_HWNDNEXT 2
	#DEFINE GW_CHILD 5

	PROCEDURE FindDockArea()
		LOCAL hParentWnd,hFirstChild, hLastChild, hCurrent  
		hParentWnd = _vfp.hwnd
		
		hFirstChild = GetWindow(m.hParentWnd, GW_CHILD)
		hLastChild = GetWindow(m.hFirstChild, GW_HWNDLAST)
		
		WITH This
		    rc = Repli(Chr(0),16)
		    = GetWindowRect(.hWnd, @rc)
	        nLeftSF = CToBin(Substr(m.rc, 1,4), "4SR")
	        nTopSF = CToBin(Substr(m.rc, 5,4), "4SR")
	        nRightSF = CToBin(Substr(m.rc, 9,4), "4SR")
	        nBottomSF = CToBin(Substr(m.rc, 13,4), "4SR")
			 
			hCurrent = m.hFirstChild
			DO WHILE .T.
				If !INLIST(m.hCurrent, .hwnd, _screen.hwnd, _vfp.hwnd)
				    rc = Repli(Chr(0),16)
				    = GetWindowRect(m.hCurrent, @rc)
		            nLeft = CToBin(Substr(m.rc, 1,4), "4SR")
		            nTop = CToBin(Substr(m.rc, 5,4), "4SR")
		            nRight = CToBin(Substr(m.rc, 9,4), "4SR")
		            nBottom = CToBin(Substr(m.rc, 13,4), "4SR")		    
					
					IF IsWindow(.hParentDockTop)=0 AND m. nRight - m.nLeft = 32767 AND (m.nTop<=m.nBottomSF OR m.nTop = m.nBottomSF + 1)
						.hParentDockTop = m.hCurrent
					EndIf

					IF IsWindow(.hParentDockLeft)=0 AND m.nLeft = m.nLeftSF
						.hParentDockLeft = m.hCurrent
					EndIf

					IF IsWindow(.hParentDockRight)=0 AND m.nRight = m.nRightSF AND m.hCurrent#.hwnd
						.hParentDockRight = m.hCurrent
					EndIf			 
				 endif				 
			    IF m.hCurrent = m.hLastChild
			        EXIT
			    ENDIF
			    *--- Next window
			    hCurrent = GetWindow(m.hCurrent, GW_HWNDNEXT)
			ENDDO	
			*--- Effettuo bindevent
            IF IsWindow(.hParentDockTop)#0
                *--- Top
                .hOrigProcParentDockTop = GetWindowLong(.hParentDockTop, GWL_WNDPROC)
                Bindevent(.hParentDockTop, WM_WINDOWPOSCHANGING, This, "OnResizeDock",4)
            ELSE
            	IF .hParentDockTop#-1
                	Unbindevents(.hParentDockTop, WM_WINDOWPOSCHANGING)
                	.hParentDockTop=-1
            	Endif                                                            	
            ENDIF
            IF IsWindow(.hParentDockLeft)#0
            	*--- Left
                .hOrigProcParentDockLeft = GetWindowLong(.hParentDockLeft, GWL_WNDPROC)
                Bindevent(.hParentDockLeft, WM_WINDOWPOSCHANGING, This, "OnResizeDock",4)
            ELSE
            	IF .hParentDockLeft#-1
                	Unbindevents(.hParentDockLeft, WM_WINDOWPOSCHANGING)
                	.hParentDockLeft=-1
            	Endif
            ENDIF
            IF IsWindow(.hParentDockRight)#0
            	*--- Right
                .hOrigProcParentDockRight = GetWindowLong(.hParentDockRight, GWL_WNDPROC)
                Bindevent(.hParentDockRight, WM_WINDOWPOSCHANGING, This, "OnResizeDock",4)
            ELSE
            	IF .hParentDockRight#-1
                	Unbindevents(.hParentDockRight, WM_WINDOWPOSCHANGING)
                	.hParentDockRight=-1
            	Endif                                
            ENDIF			
		endWith
	EndProc

    *--- Gestione Evento WM_WINDOWPOSCHANGING
    Procedure OnResizeDock
        Lparameters HWnd, nMsg, wParam, Lparam
        Local nRet
        nRet = 0
        With This
            If m.nMsg==WM_WINDOWPOSCHANGING And m.lParam>0
                *--- _Screen
                Do Case
                    Case m.hWnd==_Screen.HWnd
                        *--- Richiamo lo std VFP
                        nRet = CallWindowProc(.hOrigProcScreen, m.hWnd, m.nMsg, m.wParam, m.lParam)
                        lcBuffer = Sys(2600, m.lParam, 24)
                        This.Move(0,0,_vfp.Width-2*Sysmetric(3), 44)
                        *--- Cerco le dockarea
                        .FindDockArea()
                       
                        *--- Se lo screen � stato abbassato
                        If 0 <> CToBin(Substr(m.lcBuffer , 13,4),'4RS')
                            Sys(2600, m.lParam+12, 4, BinToC(CToBin(Substr(m.lcBuffer , 13,4),'4RS')+This.Height,'4RS'))
                            Sys(2600, m.lParam+20, 4, BinToC(CToBin(Substr(m.lcBuffer , 21,4),'4RS')-This.Height, '4RS'))
                         Else
                            Sys(2600, m.lParam+12, 4, BinToC(CToBin(Substr(m.lcBuffer , 13,4),'4RS')+This.Height,'4RS'))
                        Endif
                    Case m.hWnd==.hParentDockTop
                        lcBuffer = Sys(2600, m.lParam, 24)
                        *--- Cambio WINDOWPOS structure y prop
                        Sys(2600, m.lParam+12, 4, BinToC(This.Height,'4RS'))
                        *--- Richiamo std VFP
                        nRet = CallWindowProc(.hOrigProcParentDockTop , m.hWnd, m.nMsg, m.wParam, m.lParam)
                    Case m.hWnd==.hParentDockLeft
                        *--- Richiamo std VFP
                        nRet = CallWindowProc(.hOrigProcParentDockLeft, m.hWnd, m.nMsg, m.wParam, m.lParam)
                        lcBuffer = Sys(2600, m.lParam, 24)
                        *--- Cambio WINDOWPOS structure y e cy prop
                        Sys(2600, m.lParam+12, 4, BinToC(CToBin(Substr(m.lcBuffer , 13,4),'4RS')+This.Height,'4RS'))
                        Sys(2600, m.lParam+20, 4, BinToC(CToBin(Substr(m.lcBuffer , 21,4),'4RS')-This.Height, '4RS'))
                    Case m.hWnd==.hParentDockRight
                        *--- Richiamo std VFP
                        nRet = CallWindowProc(.hOrigProcParentDockRight, m.hWnd, m.nMsg, m.wParam, m.lParam)
                        lcBuffer = Sys(2600, m.lParam, 24)
                        *--- Cambio WINDOWPOS structure y e cy prop
                        Sys(2600, m.lParam+12, 4, BinToC(CToBin(Substr(m.lcBuffer , 13,4),'4RS')+This.Height,'4RS'))
                        Sys(2600, m.lParam+20, 4, BinToC(CToBin(Substr(m.lcBuffer , 21,4),'4RS')-This.Height, '4RS'))
                Endcase
            Endif
        Endwith
        Return nRet
    Endproc

    Procedure _Resize
        With This
            .oLineTop.Anchor=0
            .oLineTop.Width=.Width
            .oLineTop.Anchor=10
            .oLogo.Anchor=0
            .oLogo.Left = .Width- .oLogo.Width - 10
            .oLogo.Anchor=8
            .oSearchBox.Anchor=0
            .oSearchBox.Left= .oLogo.Left - .oSearchBox.Width - 15
            .oSearchBox.Anchor=8
        Endwith
    Endproc

    Procedure oLblUser.Click()
        *--- Login
        LOCAL l_Ret
        l_Ret = "CP_LOGIN(.F., 'TIM', .T.)"
        &l_Ret
    Endproc

    Procedure oLblAzi.Click()
        *--- Cambio contesto
        LOCAL l_Ret
        l_Ret = "GS___BAE()"
        &l_Ret
    Endproc


    Procedure KeyPress
        Lparameters nKeyCode, nShiftAltCtrl
        With This
            If Vartype(.oSearchBox)=='O' And .oSearchBox.bClick And m.nKeyCode = 13
                .oSearchBox.OnClick()
            Endif
        Endwith
    Endproc

Enddefine
