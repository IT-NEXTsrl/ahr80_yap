* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_CLASS
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 09/09/99
* Translated:     02/03/2000  (Max Vizzini)
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Definizione delle classi per "oggettini"
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Define Class cp_cruscotto As Container
    Min=0
    Max=100
    BorderWidth=0
    cEvent=''
    Add Object img As Image With Picture=i_cBmpPath+'cruscott.bmp',Top=1,Left=1
    Add Object lin As Line With Top=10,Left=37,BorderWidth=2,BorderColor=Rgb(128,0,0),Width=0,Height=40
    Add Object lblmax As Label With Top=45,Left=50,Caption=Alltrim(Str(This.Max)),FontName='MS Sans Serif',FontSize=8,BackStyle=0,AutoSize=.T.
    Add Object lblmin As Label With Top=45,Left=5,Caption=Alltrim(Str(This.Min)),FontName='MS Sans Serif',FontSize=8,BackStyle=0,AutoSize=.T.
    Proc Init()
        Local w
        This.lblmin.Caption=Alltrim(Str(This.Min))
        This.lblmax.Caption=Alltrim(Str(This.Max))
        w=Txtwidth(Alltrim(Str(This.Max)),'MS Sans Serif',8)*Fontmetric(6,'MS Sans Serif',8)
        This.lblmax.Left=This.Width-w-3
    Proc Calculate(nValue)
        If Vartype(nValue)<>'N'
            Return
        Endif
        If nValue>This.Max
            nValue=This.Max
        Endif
        If nValue<This.Min
            nValue=This.Min
        Endif
        nValue=nValue-This.Min
        a=Dtor(145-(nValue*110/(This.Max-This.Min)))
        dx=40*Cos(a)
        dy=40*Sin(a)
        This.lin.Top=10+(40-dy)
        This.lin.Height=dy
        If a<Dtor(90)
            This.lin.Left=37
            This.lin.Width=dx
            This.lin.LineSlant='/'
        Else
            This.lin.Left=37+dx
            This.lin.Width=-dx
            This.lin.LineSlant='\'
        Endif
        Return
    Proc Event(cEvent)
Enddefine

Define Class cp_runprogram As Custom
    cEvent=''
    prg=''
    Enabled=.T.
    Proc Calculate(xValue)
    Proc Event(cEvent)
        Local p,N,l_Event
        l_Event=''
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(l_Event, ' ,', ',')
        If Lower(','+Alltrim(cEvent)+',')$Lower(','+Alltrim(l_Event)+',')
            If Not(Empty(This.prg))
                N=This.prg
                p=At('(',N)
                If p<>0
                    N=Stuff(N,p,1,'(this.parent.oContained,')
                    N=Strtran(N,'w_','.w_')
                    With This.Parent.oContained
                        &N
                    Endwith
                Else
                    &N(This.Parent.oContained)
                Endif
            Endif
        Endif
Enddefine

Proc cp_UpdatedFromObj(i_oObj,i_nPag)
    i_oObj.bUpdated=.T.
    If Not(Empty(i_oObj.cTrsName))
        If i_nPag=2
            Local i_oa
            i_oa=Select()
            Select (i_oObj.cTrsName)
            If i_SRV<>'A'
                Replace i_SRV With 'U'
            Endif
            Select (i_oa)
        Else
            i_oObj.bHeaderUpdated=.T.
        Endif
    Endif
    i_oObj.mCalc(.T.)
    Return

Define Class cp_calendar As Container
    cEvent=''
    Var=''
    Add Object cal As datepicker With Width=187,Height=151, Visible=.F.
    Proc Init()
        This.Width = This.cal.Width
        This.Height = This.cal.Height
        If This.nPag=1
            This.cal.Visible=.T.
        Endif
    Proc Calculate(xValue)
        If Vartype(m.xValue)='D'
            This.cal.Value=m.xValue
        Endif
    Proc Event(cEvent)
    Proc DblClick()
        If Not(Empty(This.Var)) And This.Enabled
            Local N
            N=This.Var
            d=This.cal.Value
            If !Empty(d)
                This.Parent.oContained.&N=d
            Else
                This.Parent.oContained.&N={}
            Endif
            cp_UpdatedFromObj(This.Parent.oContained,This.nPag)
        Endif
Enddefine

*!*	define class cp_calendar as container
*!*	  cEvent=''
*!*	  var=''
*!*	  add object cal as olecontrol with oleclass="MSACAL.MSACALCtrl.7",width=50,height=50,visible=.f.
*!*	  proc Init()
*!*	    this.cal.width=this.width
*!*	    this.cal.height=this.height
*!*	    if this.nPag=1
*!*	      this.cal.visible=.t.
*!*	    endif
*!*	  proc cal.UIEnable(bEnable)
*!*	    this.visible=bEnable
*!*	  proc Calculate(xValue)
*!*	    if vartype(xValue)='D'
*!*	      this.cal.year=year(xValue)
*!*	      this.cal.month=month(xValue)
*!*	      this.cal.day=day(xValue)
*!*	    endif
*!*	  proc Event(cEvent)
*!*	  proc cal.DblClick()
*!*	    if not(empty(this.parent.var)) and this.parent.enabled
*!*	      local n
*!*	      n=this.parent.var
*!*	      d='{^'+alltrim(str(this.year))+'-'+alltrim(str(this.month))+'-'+alltrim(str(this.day))+'}'
*!*	      if d<>'{^0-0-0}'
*!*	        this.parent.parent.oContained.&n=&d
*!*	      else
*!*	        this.parent.parent.oContained.&n={}
*!*	      endif
*!*	      cp_UpdatedFromObj(this.parent.parent.oContained,this.parent.nPag)
*!*	    endif
*!*	enddefine
*--- Zucchetti Aulla Fine - Nuova cp_calendar

Define Class cp_trafficlight As Image
    cEvent=''
    Picture=i_cBmpPath+'trfgo.bmp'
    Proc Calculate(xValue)
        If Vartype(m.xValue)='L'
            If Vartype(i_ThemesManager)=='O'
                If m.xValue
                    i_ThemesManager.PutBmp(This, i_cBmpPath+'trfGo.bmp',16)
                Else
                    i_ThemesManager.PutBmp(This, i_cBmpPath+'trfstop.bmp',16)
                Endif
            Else
                If m.xValue
                    This.Picture=i_cBmpPath+'trfGo.bmp'
                Else
                    This.Picture=i_cBmpPath+'trfstop.bmp'
                Endif
            Endif
        Endif

    Proc Event(cEvent)
Enddefine

Define Class cp_askfile As cpcommandbutton
    cEvent=''
    Var=''
    cExt='*'
    Proc Calculate(xValue)
    Proc Event(cEvent)
    Proc Click()
        Local l_a,l_N
        If Not(Empty(This.Var))
            l_a=Getfile(This.cExt)
            l_N=This.Var
            This.Parent.oContained.&l_N=m.l_a
            cp_UpdatedFromObj(This.Parent.oContained,This.nPag)
        Endif
Enddefine

Define Class cp_askpictfile As cpcommandbutton
    cEvent=''
    Var=''
    Proc Calculate(xValue)
    Proc Event(cEvent)
    Proc Click()
        Local l_a,l_N
        If Not(Empty(This.Var))
            l_a=Getpict()
            l_N=This.Var
            This.Parent.oContained.&l_N=m.l_a
            cp_UpdatedFromObj(This.Parent.oContained,This.nPag)
        Endif
Enddefine

Define Class cp_showimage As Image
    cEvent=''
    Default=''
    ImgSize=0
    Proc Init()
        If !Empty(This.Default)
            This.Picture=This.Default
        Endif
    Proc Calculate(xValue)
        If Vartype(m.xValue)='C'
            m.xValue=Trim(m.xValue)
            If Empty(m.xValue) And !Empty(This.Default) And This.Picture<>This.Default
                This.Picture=This.Default
            Else
                If This.Picture<>m.xValue
                    This.Picture=m.xValue
                Endif
            Endif
        Endif
    Proc Event(cEvent)
    Endproc
    Proc picture_Assign(xValue)
        Local l_oldext
        If Vartype(i_ThemesManager)=='O'
            l_Dim = Iif(This.ImgSize>0,This.ImgSize,16)
            l_oldext=Lower(Justext(m.xValue))
            l_oldext=Iif(l_oldext=='ico',"bmp",l_oldext)
            * --- Zucchetti Aulla - Sostituisco la Forceext perch� nel caso in cui avessimo un immagine
            * --- di questo tipo (prova.2.bmp) non riesce a fare la concatenazione e riporta prova.bmp
            This.Picture = i_ThemesManager.RetBmpFromIco(m.xValue, m.l_Dim) + '.' + l_oldext
        Else
            This.Picture=m.xValue
        Endif
    Endproc
Enddefine

Define Class cp_worddocument As CommandButton
    cEvent=''
    cDocName=''
    oWord=.Null.
    Picture=i_cBmpPath+'word.bmp'

    Proc Init
        Local nOutDim
        DoDefault()
        If Vartype(i_ThemesManager)=='O'
            With This
                If .Height>=.Width
                    Do Case
                        Case .Height>50
                            m.nOutDim = 32
                        Case .Height>40
                            m.nOutDim = 24
                        Otherwise
                            m.nOutDim = 16
                    Endcase
                Else
                    Do Case
                        Case .Width>50
                            m.nOutDim = 32
                        Case .Width>40
                            m.nOutDim = 24
                        Otherwise
                            m.nOutDim = 16
                    Endcase
                Endif
            Endwith
            i_ThemesManager.PutBmp(This,i_cBmpPath+'word.bmp',m.nOutDim)
        Endif
    Endproc

    Proc Calculate(xValue)
        If Vartype(m.xValue)='C'
            This.cDocName=m.xValue
        Endif
    Proc Event(cEvent)
    Proc Click()
        Local bNew,cOldErr
        Private err
        err=.F.
        bNew=.F.
        If Not(Empty(This.cDocName))
            cOldErr=On('ERROR')
            On Error err=.T.
            This.oWord.appshow
            On Error &cOldErr
            If err
                This.oWord=Createobject('word.basic')
                This.oWord.appshow
                bNew=.T.
            Endif
            If !bNew
                This.oWord.filecloseall(1)
            Endif
            If At(".doc",Lower(This.cDocName))>0
                This.cDocName=Trim(This.cDocName)
            Else
                This.cDocName=Trim(This.cDocName)+".doc"
            Endif
            If cp_fileexist(Trim(This.cDocName))
                This.oWord.fileopen(Trim(This.cDocName))
            Else
                This.oWord.filenewdefault
                This.oWord.filesaveas(Trim(This.cDocName))
            Endif
        Endif
Enddefine


Define Class cp_avi As Container
    BorderWidth=0
    *cFileName='\vfp\samples\solution\ole\FoxRain.avi'
    cFileName=''
    cEvent=''
    Add Object btn As CommandButton With Caption='play',Width=30,Height=20
    Add Object Paper As OleControl With Top=20,Width=150,Height=150,OleClass='FOXHWND.FOXHWNDCtrl.1',Enabled=.F.,Visible=.T.
    Add Object vcr As OleControl With OleClass='MCI.MMControl',Visible=.F.
    Proc btn.Click()
        This.Parent.Paper.Visible=.T.
        This.Parent.vcr.Object.From = 0
        This.Parent.vcr.Object.Command = "SEEK"
        This.Parent.vcr.Object.Command = "PLAY"
    Proc Init()
        This.Paper.Width=This.Width
        This.Paper.Height=This.Height-20
        This.vcr.hWndDisplay=This.Paper.HWnd
        This.vcr.UpdateInterval = 200
        This.vcr.TimeFormat = 3
        This.vcr.DeviceType = 'AVIVideo'
    Proc Paper.UIEnable(bEnable)
        This.Visible=bEnable
    Proc Calculate(xValue)
        If Vartype(m.xValue)='C'
            m.xValue=Trim(m.xValue)
            If Not(This.cFileName==m.xValue)
                If !Empty(m.xValue)
                    If !Empty(This.cFileName)
                        This.vcr.Object.Command = 'Close'
                    Endif
                    Set Palette Off
                    This.cFileName = m.xValue
                    This.vcr.FileName = m.xValue
                    This.vcr.Shareable = .F.
                    This.vcr.Object.Command = 'Open'
                Endif
            Endif
        Endif
    Proc Event(cEvent)
        If cEvent='FormLoad'
            This.Paper.Visible=.F.
        Endif
Enddefine

Define Class cp_calclbl As Label
    BackStyle=0
    Caption=''
    cEvent=''
    bSetFont = .T.
    bGlobalFont=.F.
    Procedure Init()
        This.Caption=cp_Translate(This.Caption)
        If This.bSetFont
            This.SetFont()
        Endif
        DoDefault()
        Return
    Procedure SetFont()
        If This.bGlobalFont
            SetFont('L', This)
        Endif
    Endproc
    Proc Calculate(xValue,xForeColor,xBackColor)
        This.Caption=m.xValue
        If Vartype(m.xForeColor)='N'
            This.ForeColor=m.xForeColor
        Endif
        If Vartype(m.xBackColor)='N'
            This.BackColor=m.xBackColor
            This.BackStyle=1
        Else
            This.BackStyle=0
        Endif
    Proc Event(cEvent)
    Procedure RightClick
        This.Parent.RightClick()
        * --- Zucchetti Aulla - non visualizza i due punti finali per le label
    Procedure caption_assign( Value )
        If Right(Alltrim(Value),1)==':' And !g_Colon And !Alltrim(Value)==':'
            Value = Alltrim(Left(Alltrim(Value),Len(Alltrim(Value))-1))
        Endif
        This.Caption = Value
    Endproc
    * ---- Zucchetti Aulla - - non visualizza i due punti finali per le label
Enddefine

Define Class cp_setobjprop As Custom
    cEvent=''
    nObj=0
    cObj=''
    cProp=''
    Enabled=.F.
    bFind=.T.
    xValue=.Null.
    Proc Calculate(xValue)
        *if type("xValue")='C'
        *this.cProp=xValue
        This.SetProp(m.xValue)
        This.xValue=m.xValue
        *endif
    Proc Event(cEvent)
        If This.bFind &&this.cEvent="Init"
            * --- cerca il control nel form
            This.bFind=.F.
            * ---- Zucchetti Aulla inizio - ricerca control anche per UID
            Local i,c,o
            With This.Parent
                For i=1 To .ControlCount
                    c=Left(Upper(.Controls(i).Class),3)
                    Do Case
                        Case c='STD' And Pemstatus(.Controls(i),"cFormVar",5)
                            o=Upper(.Controls(i).cFormVar)
                        Case c='STD' And Pemstatus(.Controls(i),"Caption",5)
                            o=Upper(.Controls(i).Caption)
                        Case c='CP_' And Pemstatus(.Controls(i),"Name",5)
                            o=Upper(.Controls(i).Name)
                        Otherwise
                            o=""
                    Endcase
                    If o==Upper(This.cObj)
                        This.nObj=i
                        Exit
                    Endif
                    If Pemstatus(.Controls(i),"uid",5) And Upper(.Controls(i).uid)==Upper(This.cObj)
                        This.nObj=i
                        Exit
                    Endif
                Next
            Endwith
            * ---- Zucchetti Aulla fine - ricerca control anche per UID
            If !Isnull(This.xValue)
                This.SetProp(This.xValue)
            Endif
        Endif
    Proc SetProp(xValue)
        If This.nObj<>0 And !Empty(This.cProp)
            Local p,i
            p=This.cProp
            This.Parent.Controls(This.nObj).&p=m.xValue
        Endif
Enddefine

Define Class cp_setCtrlObjprop As Custom
    cEvent=''
    nObj=0
    cObj=''
    bRepetead=.F.
    oObj= .Null.
    cProp=''
    Enabled=.F.
    bFind=.T.
    xValue=.Null.
    Proc Calculate(xValue)
        *if type("xValue")='C'
        *this.cProp=xValue
        This.SetProp(m.xValue)
        This.xValue=m.xValue
        *endif
    Proc Event(cEvent)
        If This.bFind &&this.cEvent="Init"
            * --- cerca il control nel form
            This.bFind=.F.
            If This.bRepetead
                This.oObj=This.Parent.oContained.GetBodyCtrl( This.cObj )
            Else
                This.oObj=This.Parent.oContained.GetCtrl( This.cObj )
            Endif
            If !Isnull(This.xValue)
                This.SetProp(This.xValue)
            Endif
        Endif
    Proc SetProp(xValue)
        If Vartype(This.oObj)='O' And !Empty(This.cProp)
            Local p
            p='this.oObj.'+Alltrim(This.cProp)+'='+'m.xvalue'
            &p
        Endif
Enddefine


Define Class cp_zoombox As zoombox && ATTENZIONE: questo codice deve essere duplicato nella classe cp_szoombox
    oContained=.Null.
    Resizable=.F.
    cZoomFile=.F.
    cTable=''
    bOptions=.T.
    bAdvOptions=.T.
    bNoDefault=.F. && Se .t. non ricerca la configurazione di default
    xOldValue=.Null.
    cEvent=''
    bQueryOnLoad=.T.
    cHeight=0
    * Zucchetti Aulla - Impostato a .t. (caso disattivazione query asincrone)
    bRetriveAllRows=g_APPLICATION="ADHOC REVOLUTION"
    
	* --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
	bNoSetAnchorForm = .T.
	* --- Zucchetti Aulla Fine  - Gestione SetAnchorForm
	
    Add Object __sqlx__ As sqlx With bDuplicateConnection=.T.
    Proc Init()
        DoDefault()
        This.Resize()
        This.cHeight=This.Height
        If !This.bOptions
            This.lbl.Visible=.F.
            This.src.Visible=.F.
            This.sel.Visible=.F.
            This.Dir.Visible=.F.
            This.rep.Visible=.F.
            This.ref.Visible=.F.
            This.adv.Visible=.F.
            This.qry.Visible=.F.
            This.zz.Visible=.F.
        Endif
        If !This.bAdvOptions
            This.adv.Visible=.F.
        Endif
    Proc Query(initsel,bSameTable,i_bNoFilter,i_bDontFill,i_bCreateHeader,i_bNotRemoveObject)
        Thisform.LockScreen=.T.
        DoDefault(initsel,bSameTable,i_bNoFilter,i_bDontFill,i_bCreateHeader,i_bNotRemoveObject)
        If This.bRetriveAllRows
            This.RetriveAllRows()
        Endif
        Thisform.LockScreen=.F.
        Return
    Proc Event(cEvent)
        If cEvent='FormLoad'
            This.oParentObject=This.Parent.oContained
            This.SetFileAndFields(This.cTable,'*',Iif(This.bQueryOnLoad,.F.,'1=0'),This.cZoomFile)
            Local cObjManyHeaderName
            cObjManyHeaderName="oManyHeader_"+This.Name
            If (This.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And Vartype(This.oRefHeaderObject)<>"O"
                This.Parent.Parent.AddObject(cObjManyHeaderName,"ManyHeader")
                This.oRefHeaderObject=This.Parent.Parent.&cObjManyHeaderName
                This.oRefHeaderObject.InitHeader(This.grd,.T.)
            Endif
        Endif
        If Lower(cEvent+',')$Lower(This.cEvent+',')
            If Not(Empty(This.cCpQueryName))
                This.NewExtQuery(This.cCpQueryName)
            Endif
            This.Query('',.T.,.F.,.F.,.F.,.T.)
        Endif
    Proc Calculate(xValue)
        Local bRedo, i_oldarea
        bRedo=.T.
        If Isnull(This.xOldValue) And !This.bQueryOnLoad
            This.xOldValue=m.xValue
            Return
        Endif
        If Vartype(m.xValue)=Vartype(This.xOldValue)
            bRedo=Not(m.xValue==This.xOldValue)
        Endif
        If bRedo
            i_oldarea=Select()
            If Not(Empty(This.cCpQueryName))
                This.NewExtQuery(This.cCpQueryName)
            Endif
            This.Query('',.T.,.F.,.F.,.F.,.T.)
            Select (i_oldarea)
        Endif
        This.xOldValue=m.xValue
    Func GetVar(cName)
        Local N,v,i_olderr
        i_olderr=On('ERROR')
        Private i_err
        i_err=.F.
        On Error i_err=.T.
        N=This.cCursor
        v=&N->&cName
        If Isnull(v)
            If Type('v')='C' Or Type('v')='M'
                v=Space(1)
            Endif
            If Type('v')='T' Or Type('v')='D'
                v=Ctod(" - - ")
            Endif
            If Type('v')='N'
                v=0
            Endif
        Endif
        On Error &i_olderr
        If i_err
            v=Null
        Endif
        Return(v)
    Proc grd.AfterRowColChange(nColIndex)
        DoDefault(nColIndex)
        If !Isnull(This.Parent.Parent.oContained)
            This.Parent.Parent.oContained.mCalc(.T.)
            This.Parent.Parent.oContained.SaveDependsOn()
        Endif
    Proc Resize()
        Local nShift,i
        Thisform.LockScreen=.T.
        nShift=Iif(This.bOptions,Iif(This.bAdvanced And Vartype(This.advpage)="O", This.advpage.Height, 0),-35)
        If This.cHeight<>0
            This.Height=Max(83+nShift,This.cHeight)-0
        Endif
        DoDefault()
    Func GetSqlx()
        Return(This.__sqlx__)
Enddefine

Define Class cp_szoombox As zzbox && ATTENZIONE: questo codice deve essere duplicato nella classe cp_zoombox
    oContained=.Null.
    Resizable=.F.
    cZoomFile=.F.
    cTable=''
    bOptions=.T.
    bAdvOptions=.T.
    xOldValue=.Null.
    cEvent=''
    bQueryOnLoad=.T.
    cHeight=0
    DisableSelMenu=.F.   &&DisableSelMenu=.T. disattiva le voci di selezione/deselezione dei menu contestuali
    *add object __sqlx__ as sqlx with bDuplicateConnection=.t.
    
	* --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
	bNoSetAnchorForm = .T.
	* --- Zucchetti Aulla Fine  - Gestione SetAnchorForm
    
    Proc Init()
        DoDefault()
        This.Resize()
        This.cHeight=This.Height
        If !This.bOptions
            This.lbl.Visible=.F.
            This.src.Visible=.F.
            This.sel.Visible=.F.
            This.Dir.Visible=.F.
            This.rep.Visible=.F.
            This.ref.Visible=.F.
            This.adv.Visible=.F.
            This.qry.Visible=.F.
            This.zz.Visible=.F.
        Endif
        If !This.bAdvOptions
            This.adv.Visible=.F.
        Endif
    Proc Event(cEvent)
        If cEvent='FormLoad'
            This.oParentObject=This.Parent.oContained
            This.SetFileAndFields(This.cTable,'*',Iif(This.bQueryOnLoad,.F.,'1=0'),This.cZoomFile)
            Local cObjManyHeaderName
            cObjManyHeaderName="oManyHeader_"+This.Name
            If (This.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And Vartype(This.oRefHeaderObject)<>"O"
                This.Parent.Parent.AddObject(cObjManyHeaderName,"ManyHeader")
                This.oRefHeaderObject=This.Parent.Parent.&cObjManyHeaderName
                This.oRefHeaderObject.InitHeader(This.grd,.T.)
            Endif
        Endif
        If ','+cEvent+','$','+This.cEvent+','
            If Not(Empty(This.cCpQueryName))
                This.NewExtQuery(This.cCpQueryName)
            Endif
            This.Query('',.T.,.F.,.F.,.F.,.T.)
        Endif
    Proc Calculate(xValue)
        Local bRedo, i_oldarea
        bRedo=.T.
        If Isnull(This.xOldValue) And !This.bQueryOnLoad
            This.xOldValue=m.xValue
            Return
        Endif
        If Vartype(m.xValue)=Vartype(This.xOldValue)
            bRedo=Not(m.xValue==This.xOldValue)
        Endif
        If bRedo
            i_oldarea=Select()
            If Not(Empty(This.cCpQueryName))
                This.NewExtQuery(This.cCpQueryName)
            Endif
            This.Query('',.T.,.F.,.F.,.F.,.T.)
            Select (i_oldarea)
        Endif
        This.xOldValue=m.xValue
    Func GetVar(cName)
        Local N,v,i_olderr
        * --- Se il cursore non � in uso non ritorno nulla
        If Not Used(This.cCursor)
            Return(.Null.)
        Endif
        i_olderr=On('ERROR')
        Private i_err
        i_err=.F.
        On Error i_err=.T.
        N=This.cCursor
        v=&N->&cName
        If Isnull(v)
            If Type('v')='C' Or Type('v')='M'
                v=Space(1)
            Endif
            If Type('v')='T' Or Type('v')='D'
                v=Ctod(" - - ")
            Endif
            If Type('v')='N'
                v=0
            Endif
        Endif
        On Error &i_olderr
        If i_err
            v=Null
        Endif
        Return(v)
    Proc grd.AfterRowColChange(nColIndex)
        DoDefault(nColIndex)
        If !Isnull(This.Parent.Parent.oContained)
            This.Parent.Parent.oContained.mCalc(.T.)
            This.Parent.Parent.oContained.SaveDependsOn()
        Endif
    Proc Resize()
        Local nShift,i
        Thisform.LockScreen=.T.
        nShift=Iif(This.bOptions,Iif(This.bAdvanced And Vartype(This.advpage)="O", This.advpage.Height, 0),-35)
        If This.cHeight<>0
            This.Height=Max(83+nShift,This.cHeight)-0
        Endif
        DoDefault()
        *proc Query(initsel,bSameTable)
        *  DoDefault(initsel,bSameTable)
        *  if this.__sqlx__.nConn>0
        *    sqlcancel(this.__sqlx__.nConn)
        *  endif
    Func GetSqlx()
        * la query principale deve essere eseguita su una connessione sincrona, altrimenti la query che aggiunge il checkbox
        * si ferma dopo i primi record scaricati e non dopo tutti quelli presenti nella query di base
        Return .Null.
        *return(this.__sqlx__)
    Func CheckAll()
        Local i_position
        Select ( This.cCursor )
        i_position=Recno()
	* --- Zucchetti Aulla - si escludono le righe con xchk=2, check non editabile, dalla selezione
        Update ( This.cCursor ) Set XCHK = 1  Where XCHK <> 2
        *--- Zucchetti Aulla
        *--- Lascio per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" rowcheckall")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" rowcheckall")
        *--- Lasciato per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" menucheck")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" menucheck")
        *--- Zucchetti Aulla
        Select ( This.cCursor )
        If Between(i_position,1,Reccount(This.cCursor))
            Go (i_position)
        Endif
        This.grd.Refresh
        Return
    Endfunc
    Func CheckFrom()
        Local i_position
        Select ( This.cCursor )
        i_position=Recno()
	* --- Zucchetti Aulla - si escludono le righe con xchk=2, check non editabile, dalla selezione
        Update ( This.cCursor ) Set XCHK = 1 Where Recno(This.cCursor)>=i_position and XCHK <> 2
        *--- Zucchetti Aulla
        *--- Lasciato per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" rowcheckfrom")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" rowcheckfrom")
        *--- Lasciato per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" menucheck")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" menucheck")
        *--- Zucchetti Aulla
        Select ( This.cCursor )
        If Between(i_position,1,Reccount(This.cCursor))
            Go (i_position)
        Endif
        This.grd.Refresh
        Return
    Endfunc
    Func CheckTo()
        Local i_position
        Select ( This.cCursor )
        i_position=Recno()
	* --- Zucchetti Aulla - si escludono le righe con xchk=2, check non editabile, dalla selezione
        Update ( This.cCursor ) Set XCHK = 1 Where Recno(This.cCursor)<=i_position and XCHK <> 2
        *--- Zucchetti Aulla
        *--- Lasciato per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" rowcheckto")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" rowcheckto")
        *--- Lasciato per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" menucheck")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" menucheck")
        *--- Zucchetti Aulla
        Select ( This.cCursor )
        If Between(i_position,1,Reccount(This.cCursor))
            Go (i_position)
        Endif
        This.grd.Refresh
        Return
    Endfunc
    Func UncheckAll()
        Local i_position
        Select ( This.cCursor )
        i_position=Recno()
	* --- Zucchetti Aulla - si escludono le righe con xchk=2, check non editabile, dalla selezione
        Update ( This.cCursor ) Set XCHK = 0 Where XCHK <> 2
        *--- Zucchetti Aulla
        *--- Lasciato per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" rowuncheckall")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" rowuncheckall")
        *--- Lasciato per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" menucheck")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" menucheck")
        *--- Zucchetti Aulla
        Select ( This.cCursor )
        If Between(i_position,1,Reccount(This.cCursor))
            Go (i_position)
        Endif
        This.grd.Refresh
        Return
    Endfunc
    Func InvertSelection()
        Local i_position
        Select ( This.cCursor )
        i_position=Recno()
	* --- Zucchetti Aulla - si escludono le righe con xchk=2, check non editabile, dalla selezione
        Update ( This.cCursor ) Set XCHK = Iif(XCHK=0, 1, 0) Where XCHK <> 2
        *--- Zucchetti Aulla
        *--- Lasciato per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" rowinvertselection")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" rowinvertselection")
        *--- Lasciato per compatibilit�
        This.Parent.oContained.NotifyEvent(This.Name+" menucheck")
        * --- forma corretta
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" menucheck")
        *--- Zucchetti Aulla
        Select ( This.cCursor )
        If Between(i_position,1,Reccount(This.cCursor))
            Go (i_position)
        Endif
        This.grd.Refresh
        Return
    Endfunc
Enddefine

Define Class cp_graph As Container
    Add Object m_graph As Graph  && olecontrol with oleclass='MCI.MMControl',visible=.f.
    cEvent=''
    cProp=''
    cQuery=''
    cCursor=''
    cExclCol=''
    cTitle=''
    cModel=''
    bMod=.T.
    bByCol=.F.
    xOldValue=.Null.
    bError=.F.
    Proc Init()
        This.m_graph.Width=This.Width
        This.m_graph.Height=This.Height
        If This.bMod
            This.m_graph.AutoActivate=2
        Else
            This.m_graph.AutoActivate=0
        Endif
    Proc Calculate(xValue)
        Local bRedo,bErr,bCloseCursor
        bCloseCursor=.T.
        If Pcount()=0
            bRedo=.F.
        Else
            If Isnull(This.xOldValue)
                bRedo=.T.
            Else
                bRedo=Not(This.xOldValue==m.xValue)
            Endif
        Endif
        If bRedo And !This.bError
            If At('.VQR',Upper(This.cQuery))<>0
                This.cCursor='__graph__'
                vq_exec(This.cQuery,This.Parent.oContained,This.cCursor)
            Else
                If !Used(This.cQuery)
                    This.cCursor='__graph__'
                    vq_exec(This.cQuery,This.Parent.oContained,This.cCursor)
                Else
                    This.cCursor=This.cQuery
                    bCloseCursor=.F.
                Endif
            Endif
            If Used(This.cCursor)
                bErr=This.m_graph.CreateDataSheet(This.cCursor,This.cExclCol)
                If !bErr
                    If !Empty(This.cTitle)
                        This.m_graph.SetTitle(This.cTitle)
                    Endif
                Endif
                If bCloseCursor
                    Select (This.cCursor)
                    Use
                Endif
            Endif
        Endif
        This.xOldValue=m.xValue
    Proc Event(cEvent)
        Local i_cMod
        i_cMod=This.cModel
        If cEvent='FormLoad' Or Lower(','+cEvent+',')$Lower(','+This.cEvent+',')
            If Empty(This.cModel)
                i_cMod=Iif(At('.vqr',This.cQuery)<>0,Left(This.cQuery,Len(This.cQuery)-4),This.cQuery)
            Endif
            If Lower(Justext(i_cMod))='vgr'
                i_cMod=Left(i_cMod,Len(i_cMod)-4)
            Endif
            If cp_IsStdFile(i_cMod,'vgr')
                i_cMod=Forceext(cp_GetStdFile(i_cMod,'vgr'),'vgr')
                If This.m_graph.Open(i_cMod)
                    Local i_old
                    i_old=This.xOldValue
                    This.xOldValue=1
                    This.Calculate(2)
                    This.xOldValue=i_old
                Else
                    This.bError=.T.
                Endif
            Else
                This.m_graph.i_bNew=.T.
                If This.bByCol
                    This.m_graph.i_nPlotBy=2
                Endif
                If !Empty(This.cModel)
                    cp_ErrorMsg(cp_MsgFormat(MSG_MODEL_A__A_IS_MISSING_F,i_cMod),"!",'',.F.)
                    This.bError=.T.
                Endif
            Endif
        Endif
        This.cModel=i_cMod
        Return
    Proc cpResize(i_dx,i_dy)
        * dimensionamento
        This.m_graph.Width=This.Width
        This.m_graph.Height=This.Height
    Endproc
Enddefine

Define Class Graph As OleControl
    OleClass='MSGraph.Chart.8'
    AutoSize=.T.
    i_bNew=.F.
    i_nPlotBy=1
    Proc CreateDataSheet(i_cCursor,i_cNoCol)
        * i_cCursor=nome cursore
        * i_cNoCol=stringa dei numeri di colonna del cursore che non devono comparire nel datasheet
        Local i,j,z,nColCount,cError,s
        Private i_bErr
        Dimension a[1]
        If Type("This.Object.Application.chart")="O"
            This.Object.Application.chart.setechoon(.F.)
        Endif
        i_bErr=.F.
        Dimension i_aFld[1]
        Select (i_cCursor)
        i=1
        If !This.i_bNew
            Do While !Empty(This.Object.Application.datasheet.cells(1,i+1).Value) And !Isnull(This.Object.Application.datasheet.cells(1,i+1).Value)
                Dimension a[i]
                a[i]=This.Object.Application.datasheet.cells(1,i+1).Value
                i=i+1
            Enddo
        Endif
        s=''
        For i=1 To 8
            s=s+Chr(Int(Rand()*26+97))
        Next
        s=s+'.xls'
        Copy To &s Type Xl5
        This.Object.Application.FileImport(Sys(5)+Sys(2003)+'\'+s)
        This.Object.Application.PlotBy=This.i_nPlotBy
        If Empty(Nvl(This.Object.Application.datasheet.cells(2,1).Value,''))
            This.Object.Application.datasheet.Columns(1).Delete()
        Endif
        If !This.i_bNew
            For i=1 To Alen(a)
                This.Object.Application.datasheet.cells(1,i+1).Value=a[i]
            Next
        Endif
        Delete File &s
        cOldErr=On('ERROR')
        On Error i_bErr=.T.
        This.Object.Application.chart.walls.interior.Color=Rgb(255,255,255)
        On Error &cOldErr
        This.Object.Application.chart.setechoon(.T.)
        Return(.F.)
    Func Open(i_cFile)
        Local cOldErr
        Private i_bErr
        i_bErr=.F.
        cOldErr=On('ERROR')
        On Error i_bErr=.T.
        If Substr(i_cFile,2,1)=':'
            This.Object.Application.FileImport(i_cFile)
        Else
            This.Object.Application.FileImport(Sys(5)+Sys(2003)+'\'+i_cFile)
        Endif
        On Error &cOldErr
        If !i_bErr
            This.i_nPlotBy=This.Object.Application.PlotBy
        Else
            cp_ErrorMsg(cp_MsgFormat(MSG_ERROR_OPENING_GRAPH__,i_cFile),"!",'',.F.)
        Endif
        Return(!i_bErr)
    Proc Save(i_cFile)
        This.Object.Application.SaveAs(i_cFile)
        Return
    Proc OpenDesign(i_nType)
        This.AutoActivate=i_nType
    Proc SetTitle(i_cTitle)
        This.Object.Application.chart.hastitle=.T.
        This.Object.Application.chart.charttitle.Text=i_cTitle
        Return
    Proc IsValidColumn(i_nCol,i_cNoCol)
        If At(Alltrim(Str(i_nCol))+',',i_cNoCol)<>0
            Return(.F.)
        Else
            If At(Alltrim(Str(i_nCol)),i_cNoCol)<>0 And At(Alltrim(Str(i_nCol)),i_cNoCol)=Len(i_cNoCol)
                Return(.F.)
            Endif
        Endif
        Return(.T.)
Enddefine

Define Class cp_Treeview As Container
    oContained=.Null.
    Resizable=.F.
    BorderWidth=0
    cEvent=''
    cCursor=''
    cShowFields=''
    cNodeShowField=''
    cLeafShowField=''
    cNodeBmp=''
    cLeafBmp=''
    bHasCheckBox=.F.
    nCount=0
    bHasElementBmp=.F.
    bRightMouse=.F.
    nDblClick=0
    nNoExecFill=.F.
    nIndent=20
    nNodeExpanded=0
    nKeyExpanded=''
    nNodeCollapsed=0
    nKeyCollapsed=''
    * --- Zucchetti Aulla Inizio Personalizzazione menu contestuale
    cMenufile=''
    bNoMenuProperty=.F.
    bNoContextMenu=.T.
    * --- Zucchetti Aulla Fine
    * --- Zucchetti Aulla . come separatore piuttosto che chr(255) come default
    cLvlSep=Chr(46)
    * ---
    Dimension aBmpIndex[1,2]
    aBmpIndex[1,1]=''
    BorderColor=0
    BorderWidth=1
    Add Object ImgList As OLEImageList
    Add Object oTree As OLETree With Top=0,Left=0,Visible=.F.
    Proc Init()
        This.Width=This.Width
        This.Height=This.Height
        This.oTree.Width=This.Width
        This.oTree.Height=This.Height
        This.oTree.LabelEdit=1  && editing dei nodi disabilitati
        This.oTree.Font.Size=8
        This.oTree.LineStyle=1
        This.oTree.Style=7
        This.oTree.Indentation=This.nIndent
        Dimension i_aImage[1]
        If This.bHasCheckBox
            *this.oTree.Checkboxes=.t.
        Endif
        This.oTree.Visible=.T.
        Return
    Proc oTree.UIEnable(bEnable)
        This.Visible=bEnable
    Proc Calculate(xValue)
        If (Vartype(m.xValue)<>'L' Or This.nCount<2) And Not This.nNoExecFill &&solo se cambiato o la prima volta
            This.nCount=This.nCount+1
            This.InitTree()
        Endif
        Return
    Proc Event(cEvent)
        If Lower(cEvent+',')$Lower(This.cEvent+',')
            This.nCount=This.nCount+1
            This.InitTree()
        Endif
    Proc InitTree()
        Local cOldErr,cLev,cCursor,cKey
        Private err
        * Se il nome del cursore non � fisso posso richiederlo all'oggetto in cui e' contenuto il treeview
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" SetCursorName")
        If Used(This.cCursor)
            Select (This.cCursor)
            err=.F.
            cCursor=This.cCursor
            cOldErr=On('ERROR')
            On Error err=.T.
            cLev=&cCursor->LvlKey
            On Error &cOldErr
            If Not err
                * --  inserita inizializzazione oggetto ImgList per prevenuire ultima bitmap foglia errata
                This.ImgList.ListImages.Clear
                This.ImgList.Init
                This.FillTree()
                This.nNoExecFill=.T.
                This.Parent.oContained.mCalc(.T.)
                This.Parent.oContained.SaveDependsOn()
                This.nNoExecFill=.F.
            Else
                cp_ErrorMsg(cp_MsgFormat(MSG_A__A_STRUCTURE_IS_NOT_CORRECT,This.cCursor),"!",'',.F.)
            Endif
        Endif
    Proc oTree.NodeClick(oNode)
        This.Parent.nNoExecFill=.T.
        This.Parent.Parent.oContained.mCalc(.T.)
        This.Parent.Parent.oContained.SaveDependsOn()
        This.Parent.nNoExecFill=.F.
        If This.Parent.bRightMouse And This.Parent.nDblClick<>1
            This.Parent.bRightMouse=.F.
            * --- Zucchetti Aulla Inizio - Personalizzazione Menu Treeview
            * --- this.parent.parent.oContained.NotifyEvent("w_"+lower(this.parent.Name)+" NodeRightClick")
            This.DoNodeRightClick()
            * --- Zucchetti Aulla Fine - Personalizzazione Menu Treeview
        Else
            If This.Parent.nDblClick=1
                This.Parent.nDblClick=2
            Endif
        Endif
        This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" NodeSelected")
    Proc oTree.Click()
        * NB: Questo evento non viene scatenato se si si scatena l'evento "NodeClick"
        If This.Parent.bRightMouse And This.Parent.nDblClick<>1
            * --- Zucchetti Aulla Inizio - Personalizzazione Menu Treeview
            * --- this.parent.parent.oContained.NotifyEvent("w_"+lower(this.parent.Name)+" MouseRightClick")
            This.DoRightClick()
            * --- Zucchetti Aulla Fine - Personalizzazione Menu Treeview
        Endif
    Proc oTree.MouseDown(nButton, nShift, nXCoord, nYCoord)
        *---La pressione del tasto destro scatena l'evento "NodeRightClick"
        If nButton=2
            This.Parent.bRightMouse=.T.
            This.Parent.nDblClick=0
        Else
            If nButton=1
                This.Parent.nDblClick=1
                This.Parent.bRightMouse=.F.
            Endif
        Endif
    Proc oTree.DblClick()
        Local cKey,cCursor,cOldErr
        Private err
        err=.F.
        If Not Isnull(This.SelectedItem) And This.SelectedItem.Index > 0
            If This.SelectedItem.Children = 0
                cCursor=This.Parent.cCursor
                cOldErr=On('ERROR')
                On Error err=.T.
                cKey=This.SelectedItem.Tag
                On Error &cOldErr
                If err
                    cKey=This.Nodes.Item(1).Tag
                Endif
                Select (This.Parent.cCursor)
                Go (cKey)
                cOldErr=On('ERROR')
                On Error err=.T.
                i_Value=&cCursor->cpexec
                On Error &cOldErr
                If Not err
                    *---Esiste il campo "cpExec": eseguo la procedura contenuta nel campo
                    Do &i_Value
                    If err
                        cp_ErrorMsg(cp_MsgFormat(MSG_ERROR_WHILE_CALLING_PROCEDURE__,Upper(i_Value)),"!",'',.F.)
                    Endif
                Endif
            Endif
            If This.Parent.nDblClick=2
                This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" NodeClick")
                This.Parent.nDblClick=0
            Endif
            This.Parent.bRightMouse=.F.
        Endif
        *endIf
        Return
        * --- Zucchetti Aulla Inizio - Personalizzazione Menu Treeview
    Proc oTree.DoRightClick()
        Local i_olderr,cFileMenu
        i_olderr=On('ERROR')
        On Error =.T.
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" MouseRightClick")
        On Error &i_olderr
        If Not(This.Parent.bNoContextMenu)
            cFileMenu=Iif(Type("this.parent.cMenuFile")<>'C','',This.Parent.cMenufile)
            g_oMenu=Createobject("IM_ContextMenu" ,This.Parent,This.Parent.Parent.oContained,cFileMenu,4)
            * --- Zucchetti Aulla - Gestione men� contestuale personalizzato
            g_oMenu.BeforeCreaMenu()
            g_oMenu=.Null.
        Endif
        Return
    Proc oTree.DoNodeRightClick()
        Local i_olderr,cFileMenu
        i_olderr=On('ERROR')
        On Error =.T.
        This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" NodeRightClick")
        On Error &i_olderr
        If Not(This.Parent.bNoContextMenu)
            cFileMenu=Iif(Type("this.parent.cMenuFile")<>'C','',This.Parent.cMenufile)
            g_oMenu=Createobject("IM_ContextMenu" ,This.Parent,This.Parent.Parent.oContained,cFileMenu,5)
            * --- Zucchetti Aulla - Gestione men� contestuale personalizzato
            g_oMenu.BeforeCreaMenu()
            g_oMenu=.Null.
        Endif
        Return
        * --- Zucchetti Aulla Fine - Personalizzazione Menu Treeview
    Func GetVar(i_cName,i_xDefault)
        Local i_Value,cKey,cCursor,cOldErr,i_Blank
        Local i_oldarea
        i_oldarea=Select()
        Private err
        err=.F.
        If Vartype(i_xDefault)<>'L'
            i_Blank=i_xDefault
        Else
            *i_Blank=""
            i_Blank=.Null.
        Endif
        cCursor=This.cCursor
        If Not Used(This.cCursor) Or This.oTree.Nodes.Count=0
            Return(i_Blank)
        Endif
        cOldErr=On('ERROR')
        On Error err=.T.
        cKey=This.oTree.SelectedItem.Tag
        On Error &cOldErr
        If err
            cKey=This.oTree.Nodes.Item(1).Tag
        Endif
        Select (This.cCursor)
        Go (cKey)
        cOldErr=On('ERROR')
        err=.F.
        On Error err=.T.
        i_Value=&cCursor->&i_cName
        On Error &cOldErr
        Select(i_oldarea)
        If err Or Isnull(i_Value)
            Return(i_Blank)
        Else
            Return(i_Value)
        Endif
    Func GetIndex()
        Local nIdx
        nIdx=0
        Private err
        err=.F.
        cOldErr=On('ERROR')
        On Error err=.T.
        nIdx=This.oTree.SelectedItem.Index
        On Error &cOldErr
        Return(nIdx)
    Proc FillTree(pbAccKey)
        Local oNodX,cFathKey,aLev,cLevel,bIsContained,i,nIdx,cCursor,cTmp
        Local nBmpIndex,nPrevIdx,cNodeFields,cLeafFields,cPrevLeafText,bBmp,cOldErr
        LOCAL TestMacro
        Private err
        cFathKey=''
        bIsContained=.F.
        Dimension aLev[1]
        Dimension cNodeFields[1]
        Dimension cLeafFields[1]
        aLev[1]=''
        cNodeFields[1]=''
        cLeafFields[1]=''
        bBmp=.T.
        *---Compongo la lista dei campi da visualizzare nei nodi
        cTmp=This.cNodeShowField+Iif(Empty(This.cNodeShowField) Or Empty(This.cShowFields),'','+')+This.cShowFields+','
        i=0
        Do While At(",",cTmp)<>0
            i=i+1
            Dimension cNodeFields[i]
            cNodeFields[i]=Iif(pbAccKey,'strtran('+Left(cTmp,At(",",cTmp)-1)+',"&")',Left(cTmp,At(",",cTmp)-1))
            cTmp=Substr(cTmp,At(",",cTmp)+1)
        Enddo
        *---Compongo la lista dei campi da visualizzare nelle foglie
        cTmp=This.cLeafShowField+Iif(Empty(This.cLeafShowField) Or Empty(This.cShowFields),'','+')+This.cShowFields+','
        i=0
        Do While At(",",cTmp)<>0
            i=i+1
            Dimension cLeafFields[i]
            cLeafFields[i]=Left(cTmp,At(",",cTmp)-1)
            cTmp=Substr(cTmp,At(",",cTmp)+1)
        Enddo
        cCursor=This.cCursor
        *--- Cancella tutti i nodi esistenti
        This.oTree.Nodes.Clear
        Select (cCursor)
        Set Order To LvlKey
        nPrevIdx=1
        cPrevLeafText=''
        *--- Verifica se esiste il campo "CPBmpName" dei bitmap parametrici
        err=.F.
        cOldErr=On('ERROR')
        On Error err=.T.
        cLevel=&cCursor..CPBmpName
        On Error &cOldErr
        If err
            bBmp=.F.
        Endif
        Scan
            cLevel="k"+Trim(&cCursor->LvlKey)
            nIdx=0
            For i=Alen(aLev) To 1 Step -1
                If This.cLvlSep+aLev[i]+This.cLvlSep$This.cLvlSep+cLevel
                    nIdx=i
                    Exit For
                Endif
            Next
            Dimension aLev[nIdx+1]
            aLev[nIdx+1]=cLevel
            *---Conta il nodo per decidere quale campo visualizzare
            i=Len(cLevel)-Len(Strtran(cLevel,This.cLvlSep,''))+1
            If i>Alen(cNodeFields)
                i=1
            Endif
            cTmp=''
            TestMacro=!bBmp Or Empty(&cCursor..CPBmpName)
            If TestMacro
                If nIdx<=nPrevIdx And This.oTree.Nodes.Count>0  &&devo aggiornare il precedente perch� non � un nodo con subitems
                    If This.bHasElementBmp And This.ImgList.ListImages.Count>0
                        This.oTree.Nodes(This.oTree.Nodes.Count).Image=This.ImgList.ListImages.Count
                    Endif
                    This.oTree.Nodes(This.oTree.Nodes.Count).Text=cPrevLeafText
                Endif
                If This.ImgList.ListImages.Count>0
                    nBmpIndex=nIdx%This.ListImageCount()+1
                    cTmp=","+Alltrim(Str(nBmpIndex))
                Else
                    nBmpIndex=1
                Endif
            Else
                * --- Bitmap paramentrici
                cTmp=","+Alltrim(Str(This.GetBitmapIndex(&cCursor..CPBmpName)))
            Endif
            If nIdx=0
                oNodX=This.oTree.Nodes.Add(,,aLev[nIdx+1],Iif(Isnull(&cNodeFields[i]),'',cp_ToStr(&cNodeFields[i],1))&cTmp)
            Else
                oNodX=This.oTree.Nodes.Add(aLev[nIdx],4,aLev[nIdx+1],Iif(Isnull(&cNodeFields[i]),'',cp_ToStr(&cNodeFields[i],1))&cTmp)
            Endif
            oNodX.Tag=Recno()
            nPrevIdx=nIdx
            cPrevLeafText=Iif(Isnull(&cLeafFields[i]),'',cp_ToStr(&cLeafFields[i],1))
            *onodx.ensurevisible
        Endscan
        If This.oTree.Nodes.Count>0  &&devo aggiornare il precedente perch� non � un nodo con subitems
            If This.bHasElementBmp
                This.oTree.Nodes(This.oTree.Nodes.Count).Image=This.ImgList.ListImages.Count
            Endif
            This.oTree.Nodes(This.oTree.Nodes.Count).Text=cPrevLeafText
        Endif
        Return
    Func GetBitmapIndex(i_cBmpName)
        Local i_Idx,i, cFileBmp
        i_Idx=0
        For i=1 To Alen(This.aBmpIndex,1)
            If This.aBmpIndex[i,1]==Trim(i_cBmpName)
                i_Idx=This.aBmpIndex[i,2]
            Endif
        Next
        If i_Idx=0
            If Vartype(i_ThemesManager)=='O'
                m.cFileBmp = i_ThemesManager.RetBmpFromIco(m.i_cBmpName, 16)
                *!*			LOCAL l_bmpName, l_bmpNoExt
                *!*			l_bmpNoExt = ADDBS(JUSTPATH(ALLTRIM(i_cBmpName))) + JUSTSTEM(ALLTRIM(i_cBmpName))
                *!*			l_bmpName = cp_GetStdImg(m.l_bmpNoExt, 'ico')
                *!*			IF EMPTY(m.l_bmpName)
                *!*				m.cFileBmp = cp_GetStdImg(m.l_bmpNoExt, 'bmp')
                *!*			ELSE
                *!*				m.cFileBmp = i_ThemesManager.RetBmpFromIco(l_bmpName+'.ico', 16)
                *!*			ENDIF
            Else
                m.cFileBmp = Addbs(Justpath(m.i_cBmpName))+Juststem(m.i_cBmpName)
            Endif
            If cp_fileexist( m.cFileBmp+".bmp" )
                fn = Forceext(Addbs(Justpath(m.cFileBmp))+Sys(2015),'bmp')
                cb_GetBmpFromImage(m.cFileBmp+".bmp", m.fn)
                This.ImgList.ListImages.Add(,Trim(m.i_cBmpName),LoadPicture(fn))
                Try
                    Erase(fn)
                Catch
                Endtry
                Dimension This.aBmpIndex[alen(this.aBmpIndex,1)+1,2]
                This.aBmpIndex[alen(this.aBmpIndex,1),1]=Trim(m.i_cBmpName)
                This.aBmpIndex[alen(this.aBmpIndex,1),2]=This.ImgList.ListImages.Count
                i_Idx=This.aBmpIndex[alen(this.aBmpIndex,1),2]
            Else
                i_Idx=0
            Endif
        Endif
        Return(i_Idx)
    Func ListImageCount()
        Local nIdx
        nIdx=0
        If This.bHasElementBmp
            nIdx=This.ImgList.ListImages.Count-1
        Else
            nIdx=This.ImgList.ListImages.Count
        Endif
        Return(nIdx)
    Proc ExpandAll(bExpand)
        Local i
        If Pcount()=0
            bExpand=.T.
        Endif
        If This.oTree.Nodes.Count>0
            For i=1 To This.oTree.Nodes.Count
                This.oTree.Nodes(i).Expanded=bExpand
            Next
        Endif
        Return
    Proc ExpandElement(nIndex,bExpand)
        If Pcount()=1
            bExpand=.T.
        Endif
        If nIndex>0
            This.oTree.Nodes(nIndex).Expanded=bExpand
        Endif
    Proc oTree.expand(Node)
        This.Parent.nNodeExpanded=Node.Index
        This.Parent.nKeyExpanded=Node.Key
        This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" Expanded")
    Endproc
    Proc oTree.collapse(Node)
        This.Parent.nNodeCollapsed=Node.Tag
        This.Parent.nKeyCollapsed=Node.Key
        This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" Collapsed")
    Endproc
    Proc AddElement
        Parameters cPrefisso,cIndice
        Local cCursor,oNodX
        *
        cCursor=This.cCursor
        Select (cCursor)
        Set Order To LvlKey
        Seek Substr(cIndice,2)
        If Found()
            * Da completare - gestisce solo bitmap parametrici
            cTmp=This.cShowFields+","+Alltrim(Str(This.GetBitmapIndex(&cCursor..CPBmpName)))
            oNodX=This.oTree.Nodes.Add(cPrefisso,4,cIndice, &cTmp )
            oNodX.Tag=Recno()
        Endif
        *
        Return
    Proc DelElement
        Parameter cIndice
        This.oTree.Nodes.Remove(cIndice)
        Return
    Proc cpResize(i_dx,i_dy)
        This.oTree.Width=This.Width
        This.oTree.Height=This.Height
        Return
Enddefine

Define Class OLETree As OleControl
    Top=10
    Left=10
    Width=200
    Height=280
    Caption='OLETree'
    OLETypeAllowed=-2
    OleClass="COMCTL.TreeCtrl.1"
    ScaleMode=1
    HideSelection=.T.
    Proc Init()
        If This.Parent.ImgList.ListImages.Count>0
            This.Object.ImageList=This.Parent.ImgList.Object
        Endif
    Endproc
    * --- Zucchetti Aulla Inizio - Personalizzazione Menu Treeview
    Proc DoRightClick()
        Return
    Endproc
    Proc DoNodeRightClick()
        Return
    Endproc
    * --- Zucchetti Aulla Fine - Personalizzazione Menu Treeview
Enddefine

Define Class OLEImageList As OleControl
    OLETypeAllowed=-2
    OleClass="COMCTL.ImageListCtrl.1"
    Style=4
    Proc Init()
        Local i_aImage,cTmp,i,cTmpImage,cFile
        *dimension i_aImage[1]
        *---Costruisce l'array delle immagini
        cTmp=This.Parent.cNodeBmp
        If Not Empty(This.Parent.cLeafBmp)
            This.Parent.bHasElementBmp=.T.
            cTmp=cTmp+Iif(Empty(cTmp),'',',')+This.Parent.cLeafBmp
        Endif
        cTmp=cTmp+','
        i=0
        Do While At(",",cTmp)<>0
            i=i+1
            cTmpImage=Left(cTmp,At(",",cTmp)-1)
            *---Aggiungo il bitmap solo se esiste: verifico prima se esiste nella directory dell'applicazione
            *---poi "\bmp"\, poi in "i_cBmpPath"
            cFile=''
            If cp_fileexist("bmp\"+cTmpImage) Or (Vartype(i_ThemesManager)=='O' And cp_fileexist("bmp\"+Juststem(cTmpImage)+".ico"))
                cFile='bmp\'+cTmpImage
            Else
                If cp_fileexist(cTmpImage) Or (Vartype(i_ThemesManager)=='O' And cp_fileexist(Juststem(cTmpImage)+".ico"))
                    cFile=cTmpImage
                Else
                    If cp_fileexist(i_cBmpPath+cTmpImage) Or (Vartype(i_ThemesManager)=='O' And cp_fileexist(i_cBmpPath+Juststem(cTmpImage)+".ico"))
                        cFile=i_cBmpPath+cTmpImage
                    Endif
                Endif
            Endif
            If Empty(cFile)
                i=i-1
            Else
                Dimension i_aImage[i]
                *!* dimension L_aTmpImage[i]
                Local cTmpFile
                If Vartype(i_ThemesManager)=='O'
                    m.cTmpFile = i_ThemesManager.RetBmpFromIco(Alltrim(cFile), 16)
                    i_aImage[i]=m.cTmpFile + ".bmp"
                    *!* L_aTmpImage[i] = IIF(m.cTmpFile<>cFile, m.cTmpFile, "")
                Else
                    i_aImage[i]=cFile
                    *!* L_aTmpImage[i] = ""
                Endif
            Endif
            cTmp=Substr(cTmp,At(",",cTmp)+1)
        Enddo
        If Type('i_aImage')<>'L'
            This.LoadImage(@i_aImage)
        Endif
    Endproc
    Proc LoadImage(i_aImage)
        Local i,fn
        If Not Empty(i_aImage[1])
            For i=1 To Alen(i_aImage)
                fn = Forceext(Addbs(Justpath(i_aImage[i]))+Sys(2015),'bmp')
                cb_GetBmpFromImage(i_aImage[i], m.fn)
                This.Object.ListImages.Add(,i_aImage[i],LoadPicture(fn))
                Try
                    Erase(fn)
                Catch
                Endtry
                Dimension This.Parent.aBmpIndex(i,2)
                This.Parent.aBmpIndex[i,1]=i_aImage[i]
                This.Parent.aBmpIndex[i,2]=This.Object.ListImages.Count
            Next
        Endif
    Endproc
Enddefine

Define Class cp_Timer As Timer
    nSeconds=300
    cStartAt=''
    cStopAt=''
    nTimes=0
    nExecuted=0
    cEvent=''
    *enabled=.f.
    Enabled=.T.
    Proc Init()
        This.Interval=This.nSeconds*1000
    Proc Timer()
        Local Start,stop,dt
        Start=.Null.
        stop=.Null.
        dt=Datetime()
        * --- da calcolare qui per il cambio di data
        If !Empty(This.cStartAt)
            Start=This.ToTime(This.cStartAt)
        Endif
        If !Empty(This.cStopAt)
            stop=This.ToTime(This.cStopAt)
        Endif
        If (!Isnull(stop) And stop<dt)
            This.nExecuted=0
        Endif
        *wait window iif(!isnull(start),ttoc(start),'null')+' '+iif(!isnull(stop),ttoc(stop),'null')+str(this.nExecuted)+this.cEvent nowait
        If Not(Empty(This.cEvent)) And (This.nExecuted<This.nTimes Or This.nTimes=0) And (Isnull(Start) Or Start<=dt) And (Isnull(stop) Or dt<=stop)
            *wait window this.cEvent+' at '+ttoc(datetime())+str(this.nExecuted+1) nowait
            This.nExecuted=This.nExecuted+1
            This.Parent.oContained.NotifyEvent(This.cEvent)
        Endif
    Proc Event(cEvent)
    Proc Calculate(xValue)
    Func ToTime(cTime)
        Local T,c
        c=Set('century')
        Set Century On
        T=cTime
        If At(' ',T)=0
            T=Dtoc(Date())+' '+T
        Endif
        Set Century &c
        Return(Ctot(T))
Enddefine

Define Class cp_Browser As Container
    cEvent = ''
    BorderWidth=0
    cURL=''
    xOldValue=.Null.
    bPreviewMsg = .f.
    bAddressBar = .f.
    bSilent = .f.
    
	* --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
	bNoSetAnchorForm = .T.
	* --- Zucchetti Aulla Fine  - Gestione SetAnchorForm
    
    Add Object oBrowser As OLEBrowser With Top=0,Left=0,Visible=.F.
    Add Object oPreviewMsg As Label With Caption='Anteprima non disponibile',Alignment=2,Visible=.F.
    
    Proc Init()
        This.AdjustToSize()
        This.oBrowser.AddressBar = This.bAddressBar
    	This.oBrowser.Silent = This.bSilent
        If This.nPag=1
            This.oBrowser.Visible=.T.
        Endif
    	
    	This.oPreviewMsg.Caption = cp_translate(This.oPreviewMsg.Caption)
        This.oPreviewMsg.ForeColor = GetBestForeColor(This.BackColor)
    	If This.bPreviewMsg
            This.oPreviewMsg.Visible=.T.
        Endif
    Endproc
    Procedure AdjustToSize()
        This.oBrowser.Width=This.Width
        This.oBrowser.Height=This.Height
        
        This.oPreviewMsg.Anchor = 0
        This.oPreviewMsg.Move(0, This.Height*0.5-This.oPreviewMsg.Height*0.5, This.Width)
        This.oPreviewMsg.Anchor = 522
    Endproc
    Proc oBrowser.UIEnable(bEnable)
        This.Visible=bEnable
    Proc Event(cEvent)
    	Local l_Event
        l_Event=''
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(l_Event, ' ,', ',')
    	If Lower(','+Alltrim(cEvent)+',')$Lower(','+Alltrim(l_Event)+',')
            If Empty(This.cURL) Or This.cURL<>This.xOldValue
            	This.oBrowser.Navigate(This.xOldValue)
            Else
            	This.oBrowser.Navigate(This.cURL)
            Endif
        Endif
    Proc Calculate(xValue)
        Local cOldErr,bRedo
        Private err
        err=.F.
        bRedo=.T.
        If Vartype(m.xValue)='C'
            If Vartype(m.xValue)=Vartype(This.xOldValue)
                bRedo=Not(Trim(m.xValue)==This.xOldValue) And Not(Trim(m.xValue)==This.cURL)
            Endif
            If bRedo And Not Empty(m.xValue) &&and not this.bNoNavigate
                cOldErr=On('ERROR')
                On Error err=.T.
                This.oBrowser.Navigate(m.xValue)
                On Error &cOldErr
            Endif
        Endif
        This.xOldValue=m.xValue
    Proc oBrowser.DocumentComplete(pDisp, cURL)
        If Not This.Parent.cURL==cURL
            This.Parent.cURL=cURL
            This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" NavigationComplete")
        Endif
	Proc bPreviewMsg_Assign(bValue)
		If Vartype(m.bValue)='L' And m.bValue<>This.bPreviewMsg
			This.bPreviewMsg = m.bValue
			This.oPreviewMsg.Visible = m.bValue
		Endif
	Endproc
Enddefine

Define Class OLEBrowser As OleControl
    OLETypeAllowed=-2
    OleClass="Shell.Explorer.2"
    Proc Refresh()
        Nodefault
    Endproc
Enddefine

Define Class cp_BrowserButtons As Container
    BorderWidth=0
    cBrowserName=''
    Add Object btnBack As CommandButton With Top=0,Left=0,Width=32,Height=30,Picture=i_cBmpPath+"goback.bmp",Caption=""
    Add Object btnForward As CommandButton With Top=0,Left=32,Width=32,Height=30,Picture=i_cBmpPath+"goforw.bmp",Caption=""
    Add Object btnStop As CommandButton With Top=0,Left=64,Width=32,Height=30,Picture=i_cBmpPath+"stop.bmp",Caption=""
    Add Object btnUpdate As CommandButton With Top=0,Left=96,Width=32,Height=30,Picture=i_cBmpPath+"refresh.bmp",Caption=""
    Proc Init()
        With This
            .btnBack.ToolTipText=cp_Translate(MSG_BACK)
            .btnForward.ToolTipText=cp_Translate(MSG_FORWARD)
            .btnStop.ToolTipText=cp_Translate(MSG_EXIT)
            .btnUpdate.ToolTipText=cp_Translate(MSG_UPDATE)
            If Vartype(i_ThemesManager)=='O'
                i_ThemesManager.PutBmp(.btnBack,i_cBmpPath+"goback.bmp",24)
                i_ThemesManager.PutBmp(.btnForward,i_cBmpPath+"goforw.bmp",24)
                i_ThemesManager.PutBmp(.btnStop,i_cBmpPath+"stop.bmp",24)
                i_ThemesManager.PutBmp(.btnUpdate,i_cBmpPath+"refresh.bmp",24)
            Endif
        Endwith
    Proc Event(cEvent)
    Proc Calculate(xValue)
    Proc btnBack.Click()
        Local i_oExplorer,cOldErr
        Private err
        err=.F.
        i_oExplorer=This.Parent.cBrowserName
           LOCAL TestMacro
  	   TestMacro=Type('this.parent.parent.oContained.&i_oExplorer')
        If TestMacro='O'
            cOldErr=On('ERROR')
            On Error err=.T.
            This.Parent.Parent.oContained.&i_oExplorer..oBrowser.GoBack()
            On Error &cOldErr
        Endif
    Proc btnForward.Click()
        Local i_oExplorer,cOldErr
        LOCAL TestMacro
        Private err
        err=.F.
        i_oExplorer=This.Parent.cBrowserName
        TestMacro=Type('this.parent.parent.oContained.&i_oExplorer')
        If TestMacro='O'
            cOldErr=On('ERROR')
            On Error err=.T.
            This.Parent.Parent.oContained.&i_oExplorer..oBrowser.GoForward()
            On Error &cOldErr
        Endif
    Proc btnStop.Click()
        Local i_oExplorer,cOldErr
        Private err
        err=.F.
        i_oExplorer=This.Parent.cBrowserName
        LOCAL TestMacro
  	   TestMacro=Type('this.parent.parent.oContained.&i_oExplorer')
        If TestMacro='O'
            cOldErr=On('ERROR')
            On Error err=.T.
            This.Parent.Parent.oContained.&i_oExplorer..oBrowser.stop()
            On Error &cOldErr
        Endif
    Proc btnUpdate.Click()
        Local i_oExplorer,cOldErr
        Private err
        err=.F.
        i_oExplorer=This.Parent.cBrowserName
        LOCAL TestMacro
        TestMacro=Type('this.parent.parent.oContained.&i_oExplorer')
        If TestMacro='O'
            cOldErr=On('ERROR')
            On Error err=.T.
            This.Parent.Parent.oContained.&i_oExplorer..oBrowser.Refresh2()
            On Error &cOldErr
        Endif
Enddefine

Define Class screen_image As Image

    Proc screen_resize
        If Isnull(_Screen.oImgLogo)
            Unbindevent(This)
        Else
            _Screen.oImgLogo.Top = _Screen.Height - (_Screen.oImgLogo.Height) - 100
            _Screen.oImgLogo.Left = (_Screen.Width - _Screen.oImgLogo.Width)/2
        Endif
        Return

Enddefine

* ===================================================================================================
* Classe di integrazione con FoxCharts
* ===================================================================================================
Define Class cp_FoxCharts As Container
    Top=0
    Left=0
    BackStyle = 0
    BorderWidth=0
    oContained=.Null.
    oParentObject=.Null.
    cEvent = ''
    bChartUpdate = .T.
    bDesignMode=.F.
    bReadOnly = .T.
    bLoaded = .F.
    bQueryOnLoad=.F.
    cSource = ''
    cFileCfg = ''
    QueryModel=''
    FileModel=''
    FileReport=''
    cCursor = ''
    * --- Display FoxCharts logo
    bDisplayLogo=.F.
    cLogoFileName=''
    * --- Menu properties
    cMenufile=''
    bNoMenuProperty=.F.
    bNoContextMenu=.T.
    tErrMsg=''
    xOldValue=.Null.
    * --- Sclice/bar/Point/Line - Click/DblClick
    CurrValue = 0
    CurrLegend = ""
    CurrIndex  = 0
    CurrObjType = ""
    CurrRecno = 0
    CurrColumn = 0
    * --- Funzione Getvar
    GetFldVal=''
    Var=""
    nPag=1
    _xCursorPos=0
    _yCursorPos=0

    Add Object oChart As FoxCharts

    Proc Init()

        This.oChart.Anchor = 0

        This.oChart.Left = 0
        This.oChart.Top = 0
        This.oChart.Width = This.Width
        This.oChart.Height = This.Height

        This.oChart.Anchor = 15

        This.oChart.ChartCanvas.Anchor = 15
        This.oChart.ChartCanvas.Stretch = 2

        && Add Custom properties to a FoxCharts
        If Not Pemstatus(This.oChart, "QueryModel", 5)
            This.oChart.AddProperty("QueryModel", "")
        Endif

        && Add Custom properties to a FoxCharts
        If Not Pemstatus(This.oChart, "FileModel", 5)
            This.oChart.AddProperty("FileModel", "")
        Endif

        && Add Custom properties to a FoxCharts
        If Not Pemstatus(This.oChart, "FileReport", 5)
            This.oChart.AddProperty("FileReport", "")
        Endif

        && Add Custom properties to a FoxCharts
        If Not Pemstatus(This.oChart, "NumSlice", 5)
            This.oChart.AddProperty("NumSlice", 0)
        Endif

        && Add Custom properties to a FoxCharts
        If Not Pemstatus(This.oChart, "CHKMINVALUE", 5)
            This.oChart.AddProperty("CHKMINVALUE", .F.)
        Endif

        && Add Custom properties to a FoxCharts
        If Not Pemstatus(This.oChart, "CHKMAXVALUE", 5)
            This.oChart.AddProperty("CHKMAXVALUE", .F.)
        Endif

        Use In Select(This.oChart._DataCursor)
        This.oChart._DataCursor=""

        Use In Select(This.oChart.SourceAlias)
        This.oChart.SourceAlias=""

        This.oChart._3D = .F.
        This.oChart.BackColorAlpha=0

        This.oChart.SubTitle.Caption = ""

        If Type('g_FOXCHARTSRENDERMODE')='N' And g_FOXCHARTSRENDERMODE >= 0
            This.oChart.ChartCanvas.RenderMode = g_FOXCHARTSRENDERMODE
        Endif

        This.oChart._InitChart()
        
		*--- Mi serve per norificare l'evento resize sui grafici solo al termine della resize della form 
		ThisForm.bNoExistFoxCharts = .f.

    Endproc

    Proc Destroy()
        Use In Select(This.oChart.SourceAlias)
        Use In Select(This.cCursor)
        This.oParentObject=.Null.
    Endproc

    *--- InitFoxCharts
    Procedure InitFoxCharts
        && Ricerco il menu per utente/Gruppo
        If Type('This.cFileCfg')='C' And !Empty(This.cFileCfg) And cp_fileexist(Forceext(This.cFileCfg, 'VFC'))
            This.cFileCfg= cp_FindNamedDefaultFile(This.cFileCfg, "vfc")
        Endif
        && Carico la configurazione
        This.LoadFoxChartsFromFile(This.cFileCfg)
        Use In Select(This.oChart._DataCursor)
        This.oChart._DataCursor=""
        * --- This.oChart.DrawChart()
    Endproc

    Proc ReadData()
        Local l_olderr, bErr, l_oldcursor, l_newcursor,bQuery, l_cProg
        Local l_NumFields, l_Count, l_sFields, l_sFields1, l_FieldsChart
        Local l_ChartsCount, l_CurrCharts
        Dimension ArrField[1]
        l_cProg=''
        bQuery=''
        l_Count = 0
        l_NumFields = 0
        m.l_sFields=''
        m.l_sFields1=''
        m.l_FieldsChart=''
        If Type('this.parent.oContained')='O'
            * --- lasciato per compatibilita'
            This.Parent.oContained.NotifyEvent(This.Name+' before query')
            * --- forma corretta
            This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' before query')
        Endif

        Use In Select(This.cCursor)
        This.cCursor = Sys(2015)

        If Empty(This.cSource) And Not Empty(This.QueryModel)
            This.cSource = This.QueryModel
        Endif

        If Upper(Right(Alltrim(This.cSource),4))='.VQR'
            * --- devo eseguire una query
            bQuery = Upper(Alltrim(This.cSource))
            If Not cp_fileexist(Forceext(bQuery, 'vqr'))
                This.tErrMsg=cp_MsgFormat(MSG_CANNOT_FIND_QUERY__,bQuery)
                cp_msg(This.tErrMsg)
            Else
                vq_exec(bQuery,This.Parent.oContained, This.cCursor)
            Endif
        Else
            If !Empty(This.cSource) And Used(This.cSource)
                Select * From (This.cSource) Into Cursor (This.cCursor) Readwrite
            Endif
        Endif

        l_olderr=On('error')
        bErr=.F.
        On Error bErr=.T.
        m.l_ChartsCount = This.oChart.ChartsCount
        If Used(This.cCursor) And This.oChart.NumSlice > 0 And m.l_ChartsCount > 0
            m.l_CurrCharts=1
            If m.l_ChartsCount > 0
                Do While l_CurrCharts <= l_ChartsCount
                    m.l_FieldsChart = m.l_FieldsChart + Upper(Alltrim(This.oChart.Fields( m.l_CurrCharts ).FieldValue)) + "-"
                    l_CurrCharts = l_CurrCharts + 1
                Enddo
            Endif

            l_NumFields = Afields(ArrField, This.cCursor)

            m.l_Count = 1
            Do While m.l_Count <= m.l_NumFields
                m.l_sFields = m.l_sFields + ArrField[m.l_Count, 1] + ","
                If Upper(Alltrim(ArrField,[m.l_Count, 1])) $ m.l_FieldsChart
                    m.l_sFields1 = m.l_sFields1 + "SUM("+Upper(Alltrim(ArrField[m.l_Count, 1])) + "),"
                Else
                    m.l_sFields1 = m.l_sFields1 + "MAX("+Upper(Alltrim(ArrField[m.l_Count, 1])) + "),"
                Endif
                m.l_Count = m.l_Count + 1
            Enddo
            m.l_sFields= Substr(m.l_sFields, 1, Len(m.l_sFields)-1)
            m.l_sFields1 = Substr(m.l_sFields1 , 1, Len(m.l_sFields1)-1)

            Select &l_sFields, 'N' As vfcRagg From (This.cCursor) Where Recno() <= This.oChart.NumSlice Union All Select &l_sFields1, 'S' As vfcRagg From (This.cCursor) Where Recno() > This.oChart.NumSlice Into Cursor "__FoxCharts__"
            Use In Select(This.cCursor)
            Select * From "__FoxCharts__" Into Cursor (This.cCursor) Readwrite
            Use In Select("__FoxCharts__")

            If !Empty(This.oChart.FieldLegend)
                Local l_FieldLegend
                l_FieldLegend = This.cCursor + '.' + This.oChart.FieldLegend
                Update (This.cCursor) Set &l_FieldLegend = cp_Translate("Altre") Where vfcRagg='S'
            Endif

        Endif
        On Error &l_olderr

        If Type('this.parent.oContained')='O'
            * --- lasciato per compatibilita'
            This.Parent.oContained.NotifyEvent(This.Name+' after query')
            * --- forma corretta
            This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' after query')
        Endif

        If !Empty(This.cCursor)
            Use In Select(This.oChart.SourceAlias)
            This.oChart.SourceAlias=Sys(2015)
            If !Empty(This.cSource) And Used(This.cCursor)
                *--- Rendo il cursore scrivibile
                Local l_TmpName, l_OldArea
                l_TmpName = Sys(2015)
                Select(This.cCursor)
                l_OldArea = Select()
                Use Dbf() Again In 0 Alias (m.l_TmpName)
                Use
                Select(m.l_TmpName)
                Use Dbf() Again In (m.l_OldArea) Alias (This.cCursor)
                Use
                Select(This.cCursor)

                Select * From (This.cCursor) Into Cursor (This.oChart.SourceAlias) Readwrite
            Endif
            Use In Select(This.cCursor)
            &&			    this.oChart.SourceAlias=this.cCursor
        Endif

        This.UpdateFoxCharts()
    Endproc

    Proc UpdateFoxCharts()
        This.oChart.DrawChart()
    Endproc

    Procedure GetChartDataFromRows
        *** JRN 07/30/2009
        * 	Takes chart data that is stored in rows, rather than columns
        *	and transforms it so that it is usable by FoxCharts:
        *
        *	Two cursors (tables) are used:
        *		<tcDataAlias>:
        *			Each record corresponds to one data series (that it, what would normally appear as a field in <SourceAlias>)
        *			Thus, the 'Browse' of this cursor lays out the data much as it would appear in a line or bar chart.
        *			Other (optional) fields in this cursor:
        *				- side legend names <tcFieldSideLegend>			= .Legends(N).Legend
        *				- custom colors <tcFieldColor>					= .Legends(N).Color
        *				- line cap shapes <tcFieldShape)				= .Legends(N).Shape
        *		<tcRowAlias>
        *			Each record is used to select a field from <tcDataAlias> for data.  Two fields in this cursor:
        *				- the name of a data field in <tcDataAlias> ... <tcFieldRow>
        *				- the text of the X-Axis legend corresponding to the data field ... <tcFieldAxisLegend>

        Lparameters 				;
            tcDataAlias,			;
            tcFieldSideLegend,		;
            ;
            tcRowAlias,				;
            tcFieldRow,				;
            tcFieldAxisLegend,		;
            ;
            tcFieldColor,			;
            tcFieldShape

        ****************************************************************
        * Parameters
        *	toFoxChart			= the FoxCharts container

        *	tcDataAlias			= the name of the cursor containing the data to be charted
        *	tcFieldSideLegend	= the name of the field in <tcDataAlias> containing the side legend name (optional)

        *	tcRowAlias			= the name of the cursor containing info about what columns from <tcDataAlias> contain data
        *	tcFieldRow			= the name of the field in <tcRowAlias> containing the name of the field in <tcDataAlias> that has chart data
        *	tcFieldAxisLegend	= the name of the field in <tcRowAlias> containing the text used as X-Axis legends (optional)

        *	tcFieldColor		= the name of the field in <tcDataAlias> containing the custom color (optional)
        *	tcFieldShape		= the name of the field in <tcDataAlias> containing the line cap shape (optional)

        Local Array laFields[1]
        Local lcSourceAlias, lnRow, lcDataType, lcFieldName, toFoxChart

        m.toFoxChart = This.oChart

        With toFoxChart

            lcSourceAlias = tcDataAlias + "_Transposed"
            If Used (lcSourceAlias)
                Use In &lcSourceAlias
            Endif

            Select * 									;
                From (tcRowAlias)						;
                Into Cursor (lcSourceAlias) Readwrite

            .SourceAlias = lcSourceAlias
            If Not Empty (tcFieldAxisLegend)
                .FieldAxis2  = tcFieldAxisLegend
            Endif

            Afields(laFields, tcDataAlias)
            lnRow = Ascan(laFields, Trim(&tcFieldRow), -1, -1, 1, 15)
            lcDataType = laFields(lnRow,2) + "(" + Transform(laFields(lnRow,3)) + Iif(laFields(lnRow,2) = 'N', "," + Transform(laFields(lnRow,4)), '') + ")"

            Select (tcDataAlias)
            .ChartsCount = Reccount()
            Scan
                With .Fields(Recno())
                    lcFieldName 	= 'Value' + Transform(Recno())
                    .FieldValue 	= lcFieldName
                    Alter Table (lcSourceAlias) Add Column (lcFieldName) &lcDataType Null

                    If Not Empty (tcFieldSideLegend)
                        .Legend		= Alltrim ( Transform(&tcFieldSideLegend) )
                    Endif

                    If Not Empty (tcFieldColor)
                        .Color		= &tcFieldColor
                    Endif

                    If Not Empty (tcFieldShape)
                        .Shape		= &tcFieldShape
                    Endif

                    Select (lcSourceAlias)
                    Scan
                        lcSourceField = &tcFieldRow
                        Replace &lcFieldName With &tcDataAlias..&lcSourceField
                    Endscan

                Endwith
            Endscan

        Endwith
        Return
    Endproc

    Proc Calculate(xValue)
        Local bRedo
        If Pcount()=0
            bRedo=.F.
        Else
            If Isnull(This.xOldValue)
                bRedo=.T.
            Else
                bRedo=Not(This.xOldValue==m.xValue)
            Endif
        Endif
        If bRedo
            This.ReadData()
        Endif
        This.xOldValue=m.xValue
    Endproc

    Proc Event(cEvent)
        Local p,N,l_Event
        l_Event=''
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(l_Event, ' ,', ',')
        If cEvent='FormLoad'
            If This.bQueryOnLoad
                This.oParentObject=This.Parent.oContained
                This.InitFoxCharts()
                This.ReadData()
            Else
                This.oParentObject=This.Parent.oContained
                This.InitFoxCharts()
            Endif
        Endif

        If cEvent=='EndResize'

            * --- ZUCCHETTI AULLA INIZIO
            * --- Nel caso il grafico non � nella pagina attiva non lo ridisegno
            * --- l'immagine ha impostato Stretch = 2
            Local l_ActivePage, l_nActivePage
            l_ActivePage = .F.
            If This.oChart._InsideForm
                If Thisform.Visible And This.oChart.Visible && AND This.Parent.Parent.Parent.BaseClass = "Page"
                    l_nActivePage = This.Parent.Parent.Parent.ActivePage
                    If This.Parent.Parent.Parent.Pages(l_nActivePage).Name == This.Parent.Parent.Name
                        l_ActivePage = .T.
                    Endif
                Endif
            Endif

            If This.oChart._Started And ((This.oChart.ChartCanvas.Stretch = 0 And This.oChart.ChartCanvas.Anchor > 0) Or l_ActivePage)
                This.oChart.ChartCanvas.lRedrawNow = .T.
            Endif
        Endif

        If Lower(','+Alltrim(cEvent)+',')$Lower(','+Alltrim(l_Event)+',')
            This.ReadData()
        Endif
    Endproc

    Procedure Click
        If !Empty(This.oChart.SourceAlias) And Used(This.oChart.SourceAlias)
            Select (This.oChart.SourceAlias)
            If Not(Empty(This.Var)) And Not(Empty(This.GetFldVal))
                N=This.Var
                a=This.GetVar(Alltrim(This.GetFldVal))
                This.Parent.oContained.&N=a
                cp_UpdatedFromObj(This.Parent.oContained,This.nPag)
            Endif
        Endif
    Endproc

    Procedure DblClick
        If !Empty(This.oChart.SourceAlias) And Used(This.oChart.SourceAlias)
            Select (This.oChart.SourceAlias)
            If Not(Empty(This.Var)) And Not(Empty(This.GetFldVal))
                N=This.Var
                a=This.GetVar(Alltrim(This.GetFldVal))
                This.Parent.oContained.&N=a
                cp_UpdatedFromObj(This.Parent.oContained,This.nPag)
            Endif
        Endif
        * --- Notifica Evento
        This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+ ' SelectedFromFoxCharts')
    Endproc


    Proc LoadFoxChartsFromFile(pFile, pSource)
        If Type("This.Parent.oContained") <> 'U' Or Type("This.Parent.oContained") = 'O' And !Isnull(This.Parent.oContained)
            If Type('m.pFile')='C' And !Empty(m.pFile) And cp_fileexist(Forceext(m.pFile, 'VFC'))
                This.cFileCfg= pFile
                &&
                This.LoadPropFormFile(This.cFileCfg)
                &&
                && Eredito le propriet�
                This.QueryModel = This.oChart.QueryModel
                This.FileModel = This.oChart.FileModel
                This.FileReport = This.oChart.FileReport

                This.cSource = Iif(Vartype(m.pSource)='C' And !Empty(m.pSource), m.pSource, This.cSource)
                && Assign FixCharts properties value to a form's variable
                * --- lasciato per compatibilita'
                This.Parent.oContained.NotifyEvent(This.Name+' config loaded')
                * --- forma corretta
                This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' config loaded')
            Endif
        Endif
    Endproc

    Proc LoadModelFromFile(pFile)
        If Type("This.Parent.oContained") <> 'U' Or Type("This.Parent.oContained") = 'O' And !Isnull(This.Parent.oContained)
            If Type('m.pFile')='C' And !Empty(m.pFile) And cp_fileexist(Forceext(m.pFile, 'VFC'))
                This.LoadPropFormFile(m.pFile)
                && Assign FixCharts properties value to a form's variable
                * --- lasciato per compatibilita'
                This.Parent.oContained.NotifyEvent(This.Name+' model loaded')
                * --- forma corretta
                This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' model loaded')
            Endif
        Endif
    Endproc

    Proc LoadPropFormFile(cFileName)
        Local cProps, l_i, l_Count, l_Prop, l_Err, l_Errp
        l_Err=''
        l_Errp=''
        If Type("This.Parent.oContained") <> 'U' Or Type("This.Parent.oContained") = 'O' And !Isnull(This.Parent.oContained)
            If Type('m.cFileName')='C' And !Empty(m.cFileName) And cp_fileexist(Forceext(m.cFileName, 'VFC'))
                cProps = Filetostr(Forceext(cFileName, 'VFC'))
                l_Count = Alines(aProp, cProps)
                &&  This.oChart.DrawChart()
                For l_i=1 To l_Count
                    l_Prop = "This.oChart"+aProp[l_i]
                    Try
                        &l_Prop
                    Catch
                        If This.oChart.Debug
                            l_Err = l_Err + l_Prop + Chr(13)
                        Endif
                    Endtry
                Next

                && Eredito le propriet�
                *!*					This.QueryModel = This.oChart.QueryModel
                *!*					This.FileModel = This.oChart.FileModel
                *!*					This.FileReport = This.oChart.FileReport


                If !Empty(l_Prop) And This.oChart.Debug
                    Strtofile(l_Prop, Forceext(Addbs(Sys(2023))+"FoxChartsErrorLog","Log"))
                Endif
            Endif
        Endif
    Endproc

    Proc SaveFoxChartsToFile(pFile)
        Local l_Props, l_Count, l_text
        l_Props = This.oChart.GetChartProperties(1)
        l_Count = Alines(aProp, l_Props)
        l_text = ''
        l_this = ""
        For l_i = 1 To l_Count
            Do Case
                Case At("WITH .", Upper(aProp[l_i]))=1
                    l_this = Alltrim(Substr(aProp[l_i], 6))
                Case At("ENDWITH", Upper(aProp[l_i]))=1
                    *-- non inserisco endwith
                    l_this = ""
                Case At(".WIDTH", Upper(aProp[l_i]))=1
                    *-- non inserisco .width altrimenti non fa il resize
                    l_this = ""
                Case At(".HEIGHT", Upper(aProp[l_i]))=1
                    *-- non inserisco .width altrimenti non fa il resize
                    l_this = ""
                Case At(".SOURCEALIAS", Upper(aProp[l_i]))=1
                    *-- non inserisco .width altrimenti non fa il resize
                    l_this = ""
                Otherwise
                    && l_text = l_text + l_this + CHRTRAN(ALLTRIM(aProp[l_i]),',','.') + CHR(13)
                    l_text = l_text + Alltrim(l_this) + Alltrim(aProp[l_i]) + Chr(13)
            Endcase
        Next
        Strtofile(l_text, Forceext(pFile,"VFC"))
    Endproc

    * --- Get the stream of the image (non salva alcun file su disco)
    * --- Aggiungendo un parametro es: RenderMode si pu� aggiungere anche la possibilit� di
    * --- scegliere il formato bmp, jpg, gif, png, emf
    Function GetMemoryStream()
        Local lPictureVal
        Local loStream As xfcMemoryStream
        loStream = _Screen.System.IO.MemoryStream.New()
        Do Case
            Case This.oChart.ChartCanvas.RenderMode = 0	&& Memory BITMAP
                m.lPictureVal = This.oChart.oBmp.GetPictureValfromHBitmap()
            Case This.oChart.ChartCanvas.RenderMode= 1	&& File BITMAP
                This.oChart.oBmp.Save(m.loStream, _Screen.System.Drawing.Imaging.ImageFormat.Bmp)
                m.lPictureVal = m.loStream.GetBuffer()
            Case This.oChart.ChartCanvas.RenderMode= 4	&& Memory PNG
                ** PNG PictureVal is only supported in SP2 and later
                If Version(4) >=  "09.00.0000.5411"   && "09.00.0000.5815"	&& VFP 9.0 SP2
                    m.lPictureVal = This.oChart.oBmp.GetPictureVal(_Screen.System.Drawing.Imaging.ImageFormat.Png)
                Else
                    ** Default to BMP if SP1 or earlier
                    m.lPictureVal = This.oChart.oBmp.GetPictureValfromHBitmap()
                Endif
            Case This.oChart.ChartCanvas.RenderMode= 5	&& File PNG
                This.oChart.oBmp.Save(m.loStream, _Screen.System.Drawing.Imaging.ImageFormat.Png)
                m.lPictureVal = loStream.GetBuffer()
            Otherwise && CASE This.RenderMode = 8	&& File EMF
                This.oChart.oBmp.Save(m.loStream, _Screen.System.Drawing.Imaging.ImageFormat.emf)
                m.lPictureVal = loStream.GetBuffer()
        Endcase
        Return m.lPictureVal
    Endfunc

    * --- Send FoxCharts image into into a clipboard
    Function SendToClipboard()
        This.oChart.oBmp.ToClipboard()
    Endfunc

    * --- Save the graph's image inside the cursor
    * --- For now, the structure of the cursor is (Grafico M ,Nome C(50),Pagina I)
    Proc SaveToCursor(cCursorName, bZap, nPage)
        If !Used(cCursorName)
            Create Cursor (cCursorName) (Grafico M, Nome c(50), Page i)
            =cp_wrcursor(cCursorName)
        Endif
        If Used(cCursorName) And bZap
            Select(cCursorName)
            Zap
        Endif
        && Inserisco all'interno del cursore i dati che servono per preparare i report
        Insert Into (cCursorName) Values (This.GetMemoryStream(), This.oChart.Title, Iif(Vartype(nPage)='N', nPage, Recno()) )
    Endproc

    Proc PrintFoxCharts(cCursorName, bZap, nPage)
        Local l_FileReport
        If !Used("__tmp__")
            Select * From (This.oChart.SourceAlias) Into Cursor "__tmp__" Readwrite
        Endif
        Private oFoxChart, oChartCanvas
        oFoxChart = This.oChart
        oChartCanvas = This.oChart.ChartCanvas
        l_FileReport = Forceext("FOXCHARTSREPORT.FRX", "FRX")
        && Se ho un report personalizzato associato al grafico utilizzo quello
        If !Empty(This.FileReport) And cp_fileexist(Forceext(This.FileReport, "FRX"))
            l_FileReport = Forceext(This.FileReport, "FRX")
        Endif
        &&Lancia la cp_chprn con una macro altrimenti la cp_chprn viene incorporata automaticamente nell eseguibile
        Local l_sPrg
        l_sPrg="cp_chprn"
        Do (l_sPrg) With l_FileReport
        Release l_sPrg
        oFoxChart=.Null.
        oChartCanvas = .Null.
        Release oFoxChart
        Release oChartCanvas
    Endproc

    Func GetVar(cName)
        Local N,v,i_olderr, i_oldarea
        i_olderr=On('ERROR')
        i_oldarea=Select()
        Private i_err
        i_err=.F.
        On Error i_err=.T.
        N=This.oChart.SourceAlias
        Select(N)
        Go This.CurrRecno
        v=&N->&cName
        If Isnull(v)
            If Type('v')='C' Or Type('v')='M'
                v=Space(1)
            Endif
            If Type('v')='T' Or Type('v')='D'
                v=Ctod(" - - ")
            Endif
            If Type('v')='N'
                v=0
            Endif
        Endif
        Select (i_oldarea)
        On Error &i_olderr
        If i_err
            v=Null
        Endif
        Return(v)
    Endfunc

    *--- Eventi
    Procedure oChart.shapelegend._updatemeasures
        Lparameters tlNumeric

        If Vartype(This.Parent.oGfx) <> "O"
            Return
        Endif

        * Measure the Legend height
        If Vartype(This._oBrush) + Vartype(This._oFont) <> "OO" Or (This._initialized = .F.)
            This._Setup()
        Endif

        Local lcCaption, lcFormat

        * _ForceFormat is a forced formatting managed directly from FoxCharts
        If This.Parent.PieShowPercent And tlNumeric
            This._Value = Round(This._Value,2)
            m.lcFormat = "999.99%"
        Else
            m.lcFormat = Iif(Empty(Alltrim(This._ForceFormat)), This.Format, Alltrim(This._ForceFormat))
            If Empty(lcFormat)
                m.lcFormat = v_PU(38+VVP)
            Endif
        Endif

        If tlNumeric
            m.lcCaption = Alltrim(Transform(This._Value, m.lcFormat))
        Else
            m.lcCaption = This.Caption
        Endif

        Local lcFont
        m.lcFont = Iif(Empty(This.FontName), This.Parent.FontName, This.FontName)

        * If no font was selected, then take the _Screen one!
        If Empty(m.lcFont)
            m.lcFont = _Screen.FontName
        Endif

        If Empty(m.lcFont)
            m.lcFont = "Arial"
        Endif

        Local liStyle, l_cStyle
        liStyle = 0 && Regular
        m.l_cStyle=""

        If This.FontBold
            liStyle = liStyle + 1
            m.l_cStyle = m.l_cStyle + "B"
        Endif

        If This.FontItalic
            liStyle = liStyle + 2
            m.l_cStyle = m.l_cStyle + "I"
        Endif

        If This.FontStrikethru
            liStyle = liStyle + 8
            m.l_cStyle = m.l_cStyle + "-"
        Endif

        If This.FontUnderline
            liStyle = liStyle + 4
            m.l_cStyle = m.l_cStyle + "U"
        Endif

        Local lnWidth, lnHeight
        m.lnWidth = 0
        m.lnHeight = 0
        m.lnWidth = Txtwidth(lcCaption, m.lcFont, This.FontSize, m.l_cStyle)
        m.lnWidth = m.lnWidth * Fontmetric(6, m.lcFont, This.FontSize, m.l_cStyle)

        lnHeight = Fontmetric(1, m.lcFont , This.FontSize )
        This._Height = m.lnHeight
        This._Width = m.lnWidth


        *!*	        Local loSize As xfcSize
        *!*	        loSize = This.Parent.oGfx.MeasureString(lcCaption, This._oFont) &&, This._oStringFormat)
        *!*	        This._Height = loSize.Height
        *!*	        This._Width = loSize.Width
        This._TransfCaption = m.lcCaption
    Endproc

    Proc oChart.ChartCanvas.BeforeDraw
        Local l_olderr, bErr
        *_vfp.AutoYield = .T.
        l_olderr=On('error')
        bErr=.F.
        On Error bErr=.T.
        If Type('g_FOXCHARTSRENDERMODE')='N' And g_FOXCHARTSRENDERMODE >= 0
            This.RenderMode = g_FOXCHARTSRENDERMODE
        Endif
        DoDefault()
        On Error &l_olderr
    Endproc

    Proc oChart.AfterDraw()
        Local l_olderr, bErr
        l_olderr=On('error')
        bErr=.F.
        On Error bErr=.T.
        DoDefault()
        On Error &l_olderr
        *_vfp.AutoYield = .F.
    Endproc

    Proc oChart.AfterChart
        If Type('this.parent.Parent.oContained')='O'
            This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" AfterChart")
        Endif

        If This.Parent.bDisplayLogo And Not(Empty(This.Parent.cLogoFileName))
            * --- Visualizzo il logo se presente
            Local cFileName
            m.cFileName = Forceext(This.Parent.cLogoFileName, 'jpg')

            If cp_fileexist(m.cFileName)
                Local loGfx As xfcGraphics
                loGfx = This.oGfx

                With _Screen.System.Drawing
                    loGfx.DrawImage(.Bitmap.FromFile(m.cFileName),0,0)
                Endwith
            Endif
        Endif
    Endproc

    Procedure RightClick()
        Local i_olderr,cFileMenu
        i_olderr=On('ERROR')
        On Error =.T.
        This.Parent.oContained.NotifyEvent("w_"+Lower(This.Name)+" MouseRightClick")
        On Error &i_olderr
        cFileMenu=Iif(Type("this.parent.cMenuFile")<>'C','',This.cMenufile)
        *--- Zucchetti Aulla
        *g_oMenu=Createobject("IM_ContextMenu" ,This,This.Parent.oContained,cFileMenu,4)
        g_oMenu=Createobject("IM_ContextMenu" ,This,This.Parent.oContained,cFileMenu,6)
        *--- Zucchetti Aulla
        * --- Zucchetti Aulla - Gestione men� contestuale personalizzato
        g_oMenu.creaMenu()
        g_oMenu=.Null.
    Endproc

    Procedure oChart.ChartCanvas.RightClick()
        This.Parent.RightClick()
    Endproc

    Procedure oChart.RightClick()
        This.Parent.RightClick()
    Endproc

    Procedure oChart.ChartCanvas.MouseMove
    	Lparameters nButton, nShift, nXCoord, nYCoord

    	DoDefault(nButton, nShift, nXCoord, nYCoord)

    	This.Parent.Parent._xCursorPos = nXCoord
    	This.Parent.Parent._yCursorPos = nYCoord
    Endproc

    Procedure oChart.ChartCanvas.MouseDown
    	Lparameters nButton, nShift, nXCoord, nYCoord

    	DoDefault(nButton, nShift, nXCoord, nYCoord)

    	This.Parent.Parent._xCursorPos = nXCoord
    	This.Parent.Parent._yCursorPos = nYCoord
    Endproc

    Procedure oChart.ChartCanvas.MouseUp
    	Lparameters nButton, nShift, nXCoord, nYCoord

    	DoDefault(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure oChart.ChartCanvas.Click()
    	Local lnValue, lcLegend, lcObjType, lnCoordIndex
    	lnCoordIndex = 0
		
    	This.Parent._CheckMousePosition(0, 0, This.Parent.Parent._xCursorPos , This.Parent.Parent._yCursorPos, @lnValue, @lcLegend, @lnCoordIndex, @lcObjType)

    	This.Parent.CurrValue = lnValue
    	This.Parent.CurrLegend = lcLegend
    	This.Parent.CurrIndex = lnCoordIndex
    	This.Parent.CurrObjType = lcObjType

        This.Parent.Parent.CurrValue = lnValue
        This.Parent.Parent.CurrLegend = lcLegend
        This.Parent.Parent.CurrIndex  = lnCoordIndex
        This.Parent.Parent.CurrObjType = lcObjType
        This.Parent.Parent.CurrRecno = This.Parent.CurrRecno
        This.Parent.Parent.CurrColumn = This.Parent.CurrColumn
    	This.Parent.Click()
    	Return
    Endproc

    Procedure oChart.ChartCanvas.DblClick()
    	Local lnValue, lcLegend, lcObjType, lnCoordIndex
    	lnCoordIndex = 0

    	This.Parent._CheckMousePosition(0, 0, This.Parent.Parent._xCursorPos , This.Parent.Parent._yCursorPos, @lnValue, @lcLegend, @lnCoordIndex, @lcObjType)

    	This.Parent.CurrValue = lnValue
    	This.Parent.CurrLegend = lcLegend
    	This.Parent.CurrIndex = lnCoordIndex
    	This.Parent.CurrObjType = lcObjType

        This.Parent.Parent.CurrValue = lnValue
        This.Parent.Parent.CurrLegend = lcLegend
        This.Parent.Parent.CurrIndex  = lnCoordIndex
        This.Parent.Parent.CurrObjType = lcObjType
        This.Parent.Parent.CurrRecno = This.Parent.CurrRecno
        This.Parent.Parent.CurrColumn = This.Parent.CurrColumn
    	This.Parent.DblClick()
    	Return
    Endproc
    
    Procedure oChart.Click()
        This.Parent.Click()
    Endproc

    Procedure oChart.DblClick()
        This.Parent.DblClick()
        * --- This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" DblClick")
    Endproc

    Procedure oChart.ShapeToolTip
        Lparameters nButton, nShift, nXCoord, nYCoord, tnValue, tcLegend, tnCoordIndex, tcObjType

        With This
            Local lcCaption, lcFormat
            If tcObjType = "Legend"
                lcCaption = ""
            Else

                lcFormat = .shapelegend.Format
                If Empty(lcFormat) && AND NOT EMPTY(This.ShapeLegendExpression)
                    lcFormat = "999,999,999,999" + ;
                        IIF(This._ValueDecimals > 0, "." + Replicate("9", This._ValueDecimals), "")
                Endif

                lcCaption = Alltrim(Transform(tnValue, lcFormat))

                * -- Zucchetti Modificato formato tooltip
                lcFormat = v_PU(38+VVP)
                lcCaption = Alltrim(Transform(tnValue, lcFormat))

                lcCaption = Alltrim(lcCaption + ;
                    IIF(Empty(tcLegend),"", Chr(13) + tcLegend) + ;
                    IIF(tcObjType = "Pie", Chr(13) + Transform(Round((tnValue / .ChartSum)*100,2), "999.99%"), ""))
                * -- Zucchetti Modificato formato tooltip
            Endif

            .ToolTip.Caption = lcCaption

            .MousePointer = .ShapeMousePointer

            If Not Empty(lcCaption) And ;
                    Thisform.FoxChartsToolTip.Width = 0
                Thisform.FoxChartsToolTip.Width = 10
            Endif

        Endwith
        This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" ShapeToolTip")
    Endproc

    Func ShellEx(cLink,cAction,cParms)
        Declare Integer ShellExecute In shell32.Dll;
            integer nHwnd,;
            string cOperation,;
            string cFileName,;
            string cParms,;
            string cDir,;
            integer nShowCmd

        Declare Integer FindWindow In win32api;
            string cNull,;
            string cWinName

        cAction=Iif(Empty(cAction),"Open",cAction)
        cParms=Iif(Empty(cParms),"",cParms)
        Return(ShellExecute(FindWindow(0,_Screen.Caption),cAction,cLink,cParms,tempadhoc(),1)) && Zucchetti Aulla, gestione cartella temporanea configurabile
    Endfunc

Enddefine

Define Class cp_clock As Container
    *--- Oggetto orologio
    Add Object oTimer As Timer With Enabled = .F., Interval = 1000
    nSeconds = 0
    tStartTime = Datetime(2000,1,1,0,0,0)
    bShowSecond = .T.
    BackStyle = 0
    BorderWidth = 0
    Visible=.T.
    FontName = "Arial"
    FontSize = 32
    Fontcolor = 0
    cEvent = ""
    Add Object oLblTime As Label With FontName = 'Arial', FontSize = 32, AutoSize=.T.,;
        Top=1, Left = 1 , BackStyle = 0, Caption = 'XX:XX', Visible = .T.
    Add Object oLblSecond As Label With FontName = 'Arial', FontSize = 24, AutoSize=.T.,;
        Top=11, Left = 1 , BackStyle = 0, Caption = 'XX', Visible = .T.

    Procedure Init(bStart)
        With This
            .oTimer.Enabled = m.bStart
            .RefreshTime()
            .FontSize = .FontSize
            .FontName = .FontName
            .Fontcolor = .Fontcolor
            .bShowSecond = .bShowSecond
        Endwith
    Procedure Resize()
        With This
            .oLblTime.Left = 1
            .oLblTime.Visible=.T.  &&Forzo autosize
            .oLblSecond.Left = .oLblTime.Left + .oLblTime.Width
            .oLblSecond.Top = .oLblTime.Top + .oLblTime.FontSize -.oLblSecond.FontSize + 2
            .Width = .oLblTime.Width+ Iif(.bShowSecond, .oLblSecond.Width, 0)
            .Height = .oLblTime.Height
        Endwith
    Procedure RefreshTime()
        Local nSec
        With This
            nSec = .tStartTime + .nSeconds
            .oLblTime.Caption = Alltrim(Right("00"+Alltrim(Str(Hour(m.nSec))),2)+Iif(.bShowSecond Or Mod(Sec(m.nSec),2)=0,':'," ")+Right("00"+Alltrim(Str(Minute(m.nSec))),2))
            .oLblSecond.Caption = Alltrim(Right("00"+Alltrim(Str(Sec(m.nSec))),2))
            .Resize()
        Endwith
    Procedure bShowSecond_Assign(xValue)
        With This
            .bShowSecond = m.xValue
            .oLblSecond.Visible = .bShowSecond
            .RefreshTime()
        Endwith

    Proc Event(cEvent)
        If Lower(m.cEvent+',')$Lower(This.cEvent+',')
            This.PlayStopTime()
        Endif

    Proc Calculate(xValue)

    Procedure oTimer.Timer()
        With This.Parent
            .nSeconds = .nSeconds + 1
            .RefreshTime()
        Endwith

    Procedure ResetTime()
        This.nSeconds = 0
        This.RefreshTime()
    Procedure PlayStopTime()
        This.oTimer.Enabled = !This.oTimer.Enabled

    Function GetTime()
        Return This.tStartTime + This.nSeconds
    Function GetHour()
        Return Hour(This.GetTime())
    Function GetMinute()
        Return Minute(This.GetTime())
    Function GetSec()
        Return Sec(This.GetTime())

    Procedure SetTime(tDt)
        This.tStartTime = m.tDt
        This.nSeconds = 0

    Procedure SetHour(nSec)
        With This
            .tStartTime = Datetime(2000, 1, 1, Min(Max(m.nSec,0),23), Minute(.tStartTime), Sec(.tStartTime))
            .nSeconds = 0
            .RefreshTime()
        Endwith

    Procedure SetMinute(nMin)
        With This
            .tStartTime = Datetime(2000, 1, 1, Hour(.tStartTime), Min(Max(m.nMin,0),59), Sec(.tStartTime))
            .nSeconds = 0
            .RefreshTime()
        Endwith

    Procedure SetSec(nSec)
        With This
            .tStartTime = Datetime(2000, 1, 1, Hour(.tStartTime), Minute(.tStartTime), Min(Max(m.nSec,0),59))
            .nSeconds = 0
            .RefreshTime()
        Endwith

    Procedure FontName_Assign(xValue)
        Dimension l_aFont[1]
        With This
            If Afont(l_aFont, m.xValue)
                .FontName = m.xValue
                .oLblTime.FontName = m.xValue
                .oLblSecond.FontName = m.xValue
                .RefreshTime()
            Endif
        Endwith
    Procedure FontSize_Assign(xValue)
        With This
            .FontSize = m.xValue
            .oLblTime.FontSize = m.xValue
            .oLblSecond.FontSize = m.xValue*.75
            .RefreshTime()
        Endwith
    Procedure Fontcolor_Assign(xValue)
        With This
            .Fontcolor = m.xValue
            .oLblTime.ForeColor = m.xValue
            .oLblSecond.ForeColor = m.xValue
            .RefreshTime()
        Endwith
Enddefine

* ===================================================================================================
* Classe di integrazione con FoxCharts per cruscotto cp_speedometer
* ===================================================================================================
Define Class cp_speedometer As Container
    Top=0
    Left=0
    BackStyle = 0
    BorderWidth=0
    oContained=.Null.
    oParentObject=.Null.
    cEvent = ''
    bChartUpdate = .T.
    bDesignMode=.F.
    bReadOnly = .T.
    bLoaded = .F.
    bQueryOnLoad=.F.
    cSource = ''
    cFileCfg = ''
    QueryModel=''
    FileModel=''
    FileReport=''
    cCursor = ''
    * --- Display FoxCharts logo
    bDisplayLogo=.F.
    cLogoFileName=''
    * --- Menu properties
    cMenufile=''
    bNoMenuProperty=.F.
    bNoContextMenu=.T.
    tErrMsg=''


    ChartsCount = 0
    * Inform the class where the data will be found
    SourceAlias = ""
    FieldHideSlice = ""
    FieldColor = ""
    FieldValue = "" && This is the name of the 1st cursor field
    * Initial Settings
    Title_Caption = ""
    SubTitle_Caption = ""
    MeterFontName = "Tahoma"
    * Adjust BackGround Colors
    MeterBackColor = Rgb(255,255,255)
    MeterBackColor2 = Rgb(255,255,255)
    GradientLevel = 5
    * Adjust the chart colors
    ColorType = 1 && Custom
    BrushType = 2 && GradientBrush
    AlphaChannel = 255 && Opaque
    * Set the initial chart style
    ChartType = 2 && Doughnut Chart
    * Adjust the depth (3d effect)
    Depth = 0
    PieCompensateAngles = .F. && only for pie charts
    PieForceCircle = .T.
    PieDetachSliceonClick = .F.
    MeterShowTips = .F.
    * Margins
    MarginBottom = 10
    MarginTop    = 10
    MarginLeft   = 10
    MarginRight  = 10
    * ---
    Ticks = 20
    MinorTicks = 4

    Angle=0
    Value=0
    MinValue = 0
    MaxValue = 100

    StylePointer=1

    TopWidth  = 6
    BottomWidth = 12

    HeightPercent = 105
    PointerColor = Rgb(96,96,96)

    MajorTickColor = Rgb(96,96,96)
    MinorTickColor = Rgb(159,159,159)

    TextDist = 1.15

    NeedleCaps = .F.

    MajorTickWidth = 1
    MinorTickWidth = 1

    MajorTickHeight = 10
    MinorTickHeight = 5

    Add Object oChart As FoxCharts

    Proc Init()

        This.oChart.Anchor = 0

        This.oChart.Left = 0
        This.oChart.Top = 0
        This.oChart.Width = This.Width
        This.oChart.Height = This.Height

        This.oChart.Anchor = 15

        This.oChart._3D = .F.
        This.oChart.BackColorAlpha=0

        This.oChart.Title.Caption = ""
        This.oChart.SubTitle.Caption = ""

        This.oChart._DataCursor = ""
        This.oChart.SourceAlias = ""

        This.oChart.Init()
        This.oChart.SaveChartProperties()

    Endproc

    Proc CreateDefCursor()
        Local lcCursor, lnColor1, lnColor2, lnColor3, lnColor4
        lcCursor = This.SourceAlias

        Use In Select(lcCursor)
        Create Cursor (lcCursor) (Chart1 N(8,2) Null, Color i, Hide_Slice l)

        lnColor1 = Rgb(240,240,240) && Grey 1
        lnColor2 = Rgb(245,245,245) && Grey 2

        lnColor3 = Rgb(255,255,160) && Yellow 1
        lnColor4 = Rgb(255,255,145) && Yellow 2

        lnColor5 = Rgb(255,160,160) && Red 1
        lnColor6 = Rgb(255,145,145) && Red 2

        Insert Into (lcCursor) Values ( 5, lnColor3 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor4 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor3 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor4 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor5 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor6 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor5 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor6 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor5 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor6 , .F.)

        * This is the hidden slice
        Insert Into (lcCursor) Values ( 20, 0, .T.)

        Insert Into (lcCursor) Values ( 5, lnColor1 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor2 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor1 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor2 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor1 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor2 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor1 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor2 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor1 , .F.)
        Insert Into (lcCursor) Values ( 5, lnColor2 , .F.)

    Endproc

    Proc Value_assign(xValue)
        Local lnAngle, lnMin, lnMax, lnRatio
        This.Value = m.xValue
        lnMin = This.MinValue
        lnMax = This.MaxValue
        lnRatio = (lnMax - lnMin) / 100
        lnValue = (m.xValue - lnMin)/ lnRatio &&+ (lnMin ) && / lnRatio)
        lnAngle = ((lnValue * 3) + 210)
        This.Angle = lnAngle
    Endproc
    Proc MinValue_assign(xValue)
        Local lnMin, lnMax
        This.MinValue = m.xValue
        lnMin = This.MinValue
        lnMax = This.MaxValue
        This.Value = This.Value  + (lnMax - lnMin) / This.Ticks
    Endproc
    Proc MaxValue_assign(xValue)
        This.MaxValue = m.xValue
        This.Value = This.MinValue
    Endproc

    Proc Destroy()
        Use In Select(This.SourceAlias)
        Use In Select(This.cCursor)
        This.oParentObject=.Null.
    Endproc

    Proc UpdateFoxCharts()
        This.oChart.DrawChart()
    Endproc


    *--- InitFoxCharts
    Procedure InitFoxCharts
        This.ChartsCount = 1

        Use In Select(This.SourceAlias)
        This.SourceAlias = Sys(2015)

        && Aggiungo le propriet� Custom
        This.CreateDefCursor()
        && Aggiungo le propriet� per la gestione del Meter
        && This.InitDefValueProp()

        This.FieldHideSlice = "Hide_Slice"
        This.FieldColor = "Color"
        This.FieldValue = "Chart1" && This is the name of the 1st cursor field

        With This.oChart
            * Adjust the chart colors
            .ColorType = 1 && Custom
            .BrushType = 2 && GradientBrush
            This.AlphaChannel = 255 && Opaque
            * Set the initial chart style
            .ChartType = 2 && Doughnut Chart
            * Adjust the depth (3d effect)
            .Depth = 0
            .PieCompensateAngles = .F. && only for pie charts
            .PieForceCircle = .T.
            .PieDetachSliceonClick = .F.
            .ShowTips = .F.

            * Margins
            .MarginBottom = This.MarginBottom
            .MarginTop = This.MarginTop
            .MarginLeft = This.MarginLeft
            .MarginRight = This.MarginRight

            .ChartsCount = This.ChartsCount
            .SourceAlias = This.SourceAlias
            .FieldHideSlice = This.FieldHideSlice
            .FieldColor = This.FieldColor
            .Fields(1).FieldValue = This.FieldValue
            .Title.Caption = This.Title_Caption
            .SubTitle.Caption = This.SubTitle_Caption
            .FontName = This.MeterFontName
            .BackColor = This.MeterBackColor
            .BackColor2 = This.MeterBackColor2
            .GradientLevel = This.GradientLevel
            .ColorType = This.ColorType
            .BrushType = This.BrushType
            .AlphaChannel = This.AlphaChannel
            .ChartType = This.ChartType
            .Depth = This.Depth
            .PieCompensateAngles = This.PieCompensateAngles
            .PieForceCircle = This.PieForceCircle
            .PieDetachSliceonClick = This.PieDetachSliceonClick
            .ShowTips = This.MeterShowTips
            .MarginBottom = This.MarginBottom
            .MarginTop = This.MarginTop
            .MarginLeft = This.MarginLeft
            .MarginRight = This.MarginRight

            .ShowScale =.T.
            .ShowSideLegend =.F.
            .PieDetachPixels = 0

            .Title.FontSize = 7

            Local lnAngle, lnMin, lnMax, lnRatio
            && Min Value
            lnMin = This.MinValue
            && Max Value
            lnMax = This.MaxValue
            && Value
            lxValue = This.Value
            && Angle
            lnRatio = (lnMax - lnMin) / 100
            lnValue = (lxValue - lnMin)/ lnRatio &&+ (lnMin ) && / lnRatio)
            lnAngle = ((lnValue * 3) + 210)
            This.Angle = lnAngle

            .SaveChartProperties()
            .DrawChart()
        Endwith

    Endproc

    Proc Calculate(xValue,xMinValue,xMaxValue)
        Local bModified
        bModified=.F.
        If Vartype(m.xValue)='N' And This.Value <> m.xValue
            bModified = .T.
            This.Value = m.xValue
        Endif
        If Vartype(m.xMinValue)='N' And  This.MinValue <> m.xMinValue
            bModified = .T.
            This.MinValue = m.xMinValue
        Endif
        If Vartype(m.xMaxValue)='N' And This.MaxValue <> m.xMaxValue
            bModified = .T.
            This.MaxValue = m.xMaxValue
        Endif
        If bModified
            This.oChart.SaveChartProperties()
            This.oChart.DrawChart()
        Endif
    Endproc

    Proc Event(cEvent)
        If cEvent='FormLoad'
            This.oParentObject=This.Parent.oContained
            This.InitFoxCharts()
        Endif

        If Lower(cEvent+',')$Lower(This.cEvent+',')
            This.UpdateFoxCharts()
        Endif
    Endproc

    Proc oChart.ChartCanvas.BeforeDraw
        Local l_olderr, bErr
        _vfp.AutoYield = .T.
        l_olderr=On('error')
        bErr=.F.
        On Error bErr=.T.
        DoDefault()
        On Error &l_olderr
    Endproc

    Proc oChart.AfterDraw()
        Local l_olderr, bErr
        l_olderr=On('error')
        bErr=.F.
        On Error bErr=.T.
        DoDefault()
        On Error &l_olderr
        _vfp.AutoYield = .F.
    Endproc

    Proc oChart.AfterChart
        If Type('this.parent.Parent.oContained')='O'
            This.Parent.Parent.oContained.NotifyEvent("w_"+Lower(This.Parent.Name)+" AfterChart")
        Endif

        * --- Nel caso stia facendo un oggettino di tipo meter devo disegnare la lancetta
        If Type('this.parent.Parent.oContained')='O'
            This.Parent.AfterChartMeter()
        Endif
    Endproc

    Proc AfterChartMeter()
        Local lnAngle, lnBaseW, lnBaseX, lnBaseY, lnTopW, lnHeightPercent, lnTopY
        Local lnType, lnPointClr, lnPointClr2, lnBackClr, lnTicks, x, Y, w, h
        Local lnTicks, lnMinorTicks, lnStartAngle, lnEndAngle, lnTextDist
        Local lnMajorTickHeight, lnMinorTickHeight, llNeedleCaps
        Local lnMajorTickWidth, lnMinorTickWidth
        Local lnMajorTickColor, lnMinorTickColor

        Private oFoxChart
        oFoxChart = This.oChart
        * Get the needed coordinate information from the current FoxCHarts object
        x = oFoxChart.aCoord(1, 1)
        Y = oFoxChart.aCoord(1, 2)
        w = oFoxChart.aCoord(1, 3)
        h = oFoxChart.aCoord(1, 4)

        * Get the parameters from the form controls
        lnTicks = This.Ticks
        lnMinorTicks = This.MinorTicks
        lnType  = This.StylePointer
        lnAngle = This.Angle
        lnBaseW = This.BottomWidth
        lnTopW  = This.TopWidth
        lnHeightPercent = This.HeightPercent / 100
        lnPointClr = This.PointerColor
        lnPointClr2 = oFoxChart.ChangeColor(lnPointClr, 60)

        lnMajorTickColor = This.MajorTickColor
        lnMinorTickColor = This.MinorTickColor

        lnTextDist = This.TextDist

        lnMinValue = This.MinValue
        lnMaxValue = This.MaxValue
        llNeedleCaps = This.NeedleCaps

        lnMajorTickHeight = This.MajorTickHeight
        lnMinorTickHeight = This.MinorTickHeight

        lnMajorTickWidth = This.MajorTickWidth
        lnMinorTickWidth = This.MinorTickWidth


        lnBaseX = x + w / 2 -lnBaseW / 2
        lnBaseY = Y + h / 2
        lnTopY  = lnBaseY - (lnBaseY * lnHeightPercent) + lnTopW / 2


        lnStartAngle = 210
        lnEndAngle = 360 + 150

        Local loGfx As xfcGraphics
        loGfx = oFoxChart.oGfx

        With _Screen.System.Drawing
            * Create a brush for the needle
            Local loBrush As xfcSolidBrush
            loBrush = .SolidBrush.New(.Color.FromRGB(lnPointClr))
            loBrush2 = .SolidBrush.New(.Color.FromRGB(lnPointClr2))

            * Create a pen for the Major ticks
            Local loMajorPen As xfcPen
            loMajorPen = .Pen.New(.Color.FromRGB(lnMajorTickColor), lnMajorTickWidth)

            * Create a pen for the Minor ticks
            Local loMinorPen As xfcPen
            loMinorPen = .Pen.New(.Color.FromRGB(lnMinorTickColor), lnMinorTickWidth)


            * Draw the major ticks
            If lnTicks > 0
                lnStep = (360 - 60)/ lnTicks
                For lnTickAngle = lnStartAngle To lnEndAngle - lnStep Step lnStep
                    * Rotate the Gfx
                    loGfx.TranslateTransform(x + w/2, Y + h/2)
                    loGfx.RotateTransform(lnTickAngle)
                    loGfx.TranslateTransform(- (x + w/2), -(Y + h/2))

                    * Draw the ticks
                    loGfx.DrawLine(loMajorPen, x + w/2, Y+0, x + w/2, Y + lnMajorTickHeight)

                    * Restore the original Gfx rotation state
                    loGfx.ResetTransform()


                    If lnMinorTicks > 0

                        lnMinorStep = lnStep / (lnMinorTicks + 1)
                        For N = lnTickAngle + lnMinorStep ;
                                TO lnTickAngle + lnStep - lnMinorStep ;
                                STEP lnMinorStep

                            * Rotate the Gfx
                            loGfx.TranslateTransform(x + w/2, Y + h/2)
                            loGfx.RotateTransform(N)
                            loGfx.TranslateTransform(- (x + w/2), -(Y + h/2))

                            * Draw the ticks
                            loGfx.DrawLine(loMinorPen, x + w/2, Y+0, x + w/2, Y + lnMinorTickHeight)

                            * Restore the original Gfx rotation state
                            loGfx.ResetTransform()

                        Endfor
                    Endif

                Endfor
            Endif


            If llNeedleCaps
                loGfx.FillEllipse(loBrush2, ;
                    lnBaseX - lnBaseW *.2, ;
                    y + h/2 -lnBaseW/2 - lnBaseW *.2, ;
                    lnBaseW * 1.4, ;
                    lnBaseW * 1.4)
            Endif


            * Create a Shape for the pointer
            Local loPath As xfcGraphicsPath
            loPath = .Drawing2D.GraphicsPath.New()
            loPath.StartFigure()

            *!*		loGfx.Clear(.Color.FromRGB(Thisform.BackColor))
            *!*		loGfx.FillEllipse(.SolidBrush.New(.Color.FromRGB(lnBackClr)), ;
            *!*			.Rectangle.New(x,y,w,h))

            If lnType = 1
                loPath.AddArc(lnBaseX, Y + h/2 -lnBaseW/2, lnBaseW, lnBaseW, 0, 180)
            Endif

            loPath.AddLine(lnBaseX, lnBaseY, x + w/2 - lnTopW/2, Y + lnTopY)

            If lnType = 1
                Local laPoints(3)
                laPoints(1) = .Point.New(x + w/2 - lnTopW/2, Y + lnTopY)
                laPoints(2) = .Point.New(x + w/2           , Y + lnTopY - lnTopW / 2)
                laPoints(3) = .Point.New(x + w/2 + lnTopW/2, Y + lnTopY)
                loPath.AddCurve(@laPoints)
            Endif

            loPath.AddLine( x + w/2 + lnTopW/2, Y + lnTopY, lnBaseX + lnBaseW, lnBaseY)
            loPath.CloseFigure()

            * Rotate the shape pointer
            loGfx.TranslateTransform(x + w/2, Y + h/2)
            loGfx.RotateTransform(lnAngle)
            loGfx.TranslateTransform(-x - w/2, -Y - h/2)

            * Draw the pointer
            loGfx.FillPath(loBrush, loPath)
            loGfx.DrawPath(.Pen.New(.Color.FromRGB(lnPointClr2),1), loPath)

            * Restore the original Gfx rotation state
            loGfx.ResetTransform()


            If llNeedleCaps
                If lnType = 1
                    loGfx.FillEllipse(loBrush2, ;
                        lnBaseX + lnBaseW *.3, ;
                        y + h/2 -lnBaseW/2 + lnBaseW *.3, ;
                        lnBaseW * .4, ;
                        lnBaseW * .4)
                Else
                    loGfx.FillEllipse(loBrush2, ;
                        lnBaseX - lnBaseW *.2, ;
                        y + h/2 -lnBaseW/2 - lnBaseW *.2, ;
                        lnBaseW * 1.4, ;
                        lnBaseW * 1.4)
                Endif
            Endif

            oFoxChart.SideLegend._Value = 0
            Local lnAdjH, lnPercent
            lnAdjH = oFoxChart.SideLegend._Height / 2

            oFoxChart.SideLegend.Format = "999"
            oFoxChart.SideLegend.Alignment = 2
            *LPARAMETERS tnX, tnY, tnWidth, tnHeight, tnStart, tnSweep, tnValue, tnDistance
            For N = lnStartAngle To lnEndAngle Step (360 - 60)/ lnTicks * 2
                x2 = x
                y2 = Y
                oFoxChart._PrepareLegendinSlice(@x2, @y2, w, h, N - 90, 0, N, lnTextDist)

                lnPercent = ((N - 210) / 3) / 100
                oFoxChart.SideLegend._Value = lnMinValue + (lnPercent * (lnMaxValue - lnMinValue))
                oFoxChart.SideLegend._DrawString(x2, y2 - lnAdjH)
                loGfx.DrawLine(.Pens.Green, x2, y2, x2+1, y2+1)
            Endfor

        Endwith

    Endproc

Enddefine

Define Class stdzImage As Container
    * --- Classe immagine per zoom
    BorderWidth=0
    *	BackStyle = 1
    *	stretch=1
    Top=0
    Left=0
    *	Anchor=15
    Picture=""
    cPathImage=""
    _Controlsource = "''"

    Add Object Img1 As Image With Top=0, Left=0, Anchor=15

    Procedure BackStyle_Access
        Local l_oldext, cFileBmp, oPict, cCursor, cCOntrolSource, cFileBmpOrig, cFileCompare, l_file
        m.cCursor = This.Parent.Parent.Parent.cCursor
        m.cCOntrolSource = This.cPathImage && Parent.ControlSource
        LOCAL TestMacro
  	   TestMacro=Type('&ccursor..&ccontrolsource')
        If TestMacro="M"
            This.Img1.PictureVal = Nvl(&cCursor..&cCOntrolSource, "")
        Endif
        This.BackStyle = 0
        Return This.BackStyle
    Endproc

Enddefine

* ===================================================================================================
* Classe per effettuare cache dei file come query menu e zoom
* ===================================================================================================
Define Class cp_CacheFile As Custom
    Declare i_acCacheFile[1,2]
    Declare i_acNameFile[1,2]
    i_nNumbCacheFile=1000
    Proc Init(nNumbCacheFile)
        If Type("nNumbCacheFile")='N'
            This.i_nNumbCacheFile=nNumbCacheFile
        Endif
        Dimension This.i_acCacheFile[this.i_nNumbCacheFile, 2]
        Dimension This.i_acNameFile[this.i_nNumbCacheFile, 2]
    Endproc
    Proc Getfile(cNameFile)
        Local nCacheFile,cFileResult
        cFileResult=''
        If Vartype(i_NoCacheFile)<>'L' Or !i_NoCacheFile
            nCacheFile=Ascan(This.i_acCacheFile, cNameFile, -1, -1, 1, 15)
            If nCacheFile>0
                cFileResult=This.i_acCacheFile[nCacheFile, 2]
                If nCacheFile>Int(This.i_nNumbCacheFile/2)
                    Adel(This.i_acCacheFile, nCacheFile)
                    nCacheFile=1
                    Ains(This.i_acCacheFile, 1)
                    This.i_acCacheFile[1,1]=Alltrim(cNameFile)
                    This.i_acCacheFile[1,2]=cFileResult
                Endif
            Endif
        Endif
        Return(cFileResult)
    Endproc
    Proc Putfile(cNameFile, cContentFile)
        Ains(This.i_acCacheFile, 1)
        This.i_acCacheFile[1,1]=Alltrim(cNameFile)
        This.i_acCacheFile[1,2]=cContentFile
    Endproc
    Proc IsFile(cNameFile)
        Local nCacheFile
        nCacheFile=Ascan(This.i_acCacheFile, cNameFile, -1, -1, 1, 15)
        Return(nCacheFile>0)
    Endproc
    Proc GetNameFile(cNameFile)
        Local nCacheFile,cNameFileResult
        cNameFileResult=''
        If Vartype(i_NoCacheFile)<>'L' Or !i_NoCacheFile
            nCacheFile=Ascan(This.i_acNameFile, cNameFile, -1, -1, 1, 15)
            If nCacheFile>0
                cNameFileResult=This.i_acNameFile[nCacheFile, 2]
                If nCacheFile>Int(This.i_nNumbCacheFile/2)
                    Adel(This.i_acNameFile, nCacheFile)
                    nCacheFile=1
                    Ains(This.i_acNameFile, 1)
                    This.i_acNameFile[1,1]=Alltrim(cNameFile)
                    This.i_acNameFile[1,2]=cNameFileResult
                Endif
                If Empty(cNameFileResult)
                    cNameFileResult="##FileNotFound##"
                Endif
            Endif
        Endif
        Return(cNameFileResult)
    Endproc
    Proc PutNameFile(cNameFile, cContentFile)
        Ains(This.i_acNameFile, 1)
        This.i_acNameFile[1,1]=Alltrim(cNameFile)
        This.i_acNameFile[1,2]=cContentFile
    Endproc
    Proc RemoveFile(cNameFile)
        Local nCacheFile
        nCacheFile=Ascan(This.i_acCacheFile, cNameFile, -1, -1, 1, 15)
        If nCacheFile>0
            Adel(This.i_acCacheFile, nCacheFile)
        Endif
    Endproc
Enddefine
*** Creazione oggetto Cache File
Proc cp_CreateCacheFile
    If Vartype(i_NoCacheFile)<>'L' Or !i_NoCacheFile
        If Vartype(i_oCacheFile)='O'
            Release i_oCacheFile
        Endif
        Public i_oCacheFile
        If Vartype(i_nNumbCacheFile)='N'
            i_oCacheFile=Createobject("cp_CacheFile",i_nNumbCacheFile)
        Else
            i_oCacheFile=Createobject("cp_CacheFile")
        Endif
    Endif
Endproc

* ===================================================================================================
* Classe per la gestione della memorizzazione della posizione delle form
* ===================================================================================================
Define Class cp_MemoryFormPosition As Custom
    i_cMemoryFormName="PositionForm"
    i_cMemoryFormPath=Addbs(g_TEMPADHOC)
    Proc Init()
        Local i_OldCursor
        i_OldCursor=Select ()
        This.i_cMemoryFormPath=Addbs(tempadhoc())
        If Not cp_fileexist(Forceext(This.i_cMemoryFormPath+This.i_cMemoryFormName,"dbf"))
            Create Table (This.i_cMemoryFormPath+This.i_cMemoryFormName) Free (CODUTE N(6), NAMEPROC c(100), POSTOP N(10), POSHEIGHT N(10), POSLEFT N(10), POSWIDTH N(10))
            Index On Str(CODUTE)+NAMEPROC Tag "PForm_Idx" Of (This.i_cMemoryFormPath+"PForm_Idx")
            Use In (This.i_cMemoryFormName)
        Endif
        Use (This.i_cMemoryFormPath+This.i_cMemoryFormName) Index (This.i_cMemoryFormPath+"PForm_Idx") Order "PForm_Idx" Shared In 0
        Select (i_OldCursor)
    Endproc
    Proc SearchPosition(oObjForm)
        Local i_OldCursor
        i_OldCursor=Select ()
        Select (This.i_cMemoryFormName)
        Go Top
        Locate For CODUTE=i_CODUTE And NAMEPROC=oObjForm.getSecurityCode()
        If Found() And POSLEFT<_Screen.Width And POSLEFT+POSWIDTH>0 And POSTOP<_Screen.Height And POSTOP+POSHEIGHT>0
            oObjForm.TopOrig=oObjForm.Top
            oObjForm.HeightOrig=oObjForm.Height
            oObjForm.LeftOrig=oObjForm.Left
            oObjForm.WidthOrig=oObjForm.Width
            oObjForm.bPostionChange=.T.
            oObjForm.Top=POSTOP
            oObjForm.Height=Iif(POSHEIGHT=0, oObjForm.Height, POSHEIGHT)
            oObjForm.Left=POSLEFT
            oObjForm.Width=Iif(POSWIDTH=0, oObjForm.Width, POSWIDTH)
        Endif
        Select (i_OldCursor)
    Endproc
    Proc InsertPosition(oObjForm)
        Local i_OldCursor, nPosHeight, nPosWidth
        nPosHeight = Iif(Type("oObjForm.oPgFrm.nOldHs")='N' And oObjForm.oPgFrm.nOldHs<>0, oObjForm.oPgFrm.nOldHs, oObjForm.Height)
        nPosWidth = Iif(Type("oObjForm.oPgFrm.nOldWs")='N' And oObjForm.oPgFrm.nOldWs<>0, oObjForm.oPgFrm.nOldWs, oObjForm.Width)
        i_OldCursor=Select ()
        Select (This.i_cMemoryFormName)
        Go Top
        Locate For CODUTE=i_CODUTE And NAMEPROC=oObjForm.getSecurityCode()
        If Found()
            Replace POSTOP With oObjForm.Top
            Replace POSHEIGHT With Iif(nPosHeight=oObjForm.HeightOrig, 0, nPosHeight)
            Replace POSLEFT With oObjForm.Left
            Replace POSWIDTH With Iif(nPosWidth=oObjForm.WidthOrig, 0, nPosWidth)
        Else
            Insert Into (This.i_cMemoryFormName) Values (i_CODUTE, oObjForm.getSecurityCode(), oObjForm.Top, nPosHeight, oObjForm.Left, nPosWidth)
        Endif
        Select (i_OldCursor)
    Endproc
    Proc RestorePosition(oObjForm)
        If oObjForm.bPostionChange
            oObjForm.Top=oObjForm.TopOrig
            oObjForm.Height=oObjForm.HeightOrig
            oObjForm.Left=oObjForm.LeftOrig
            oObjForm.Width=oObjForm.WidthOrig
            oObjForm.bPostionChange=.F.
            oObjForm.Refresh()
        Endif
    Endproc
    Proc DeletePosition()
        Local i_OldCursor
        i_OldCursor=Select ()
        Delete From (This.i_cMemoryFormName) Where CODUTE=i_CODUTE
        Select (i_OldCursor)
    Endproc
    Proc Destroy()
        Use In Select(i_cMemoryFormName)
    Endproc
Enddefine

*--- Zucchetti Aulla - Inizio
*--- Oggetto VirtualGrid

#Define	SBC_HORS			1
#Define	SBC_VERT			2

#Define	SB_LINEUP			0
#Define	SB_LINELEFT			0
#Define	SB_LINEDOWN			1
#Define	SB_LINERIGHT		1
#Define	SB_PAGEUP			2
#Define	SB_PAGELEFT			2
#Define	SB_PAGEDOWN			3
#Define	SB_PAGERIGHT		3
#Define	SB_THUMBPOSITION	4
#Define	SB_THUMBTRACK		5

*--- Oggetti per la definizione di righe di dimensioni diverse
Define Class VirtualGridRow As Custom

    ControlSource = ""

    ValueAlignment = 0
    OddValueBackColor = -1
    OddValueForeColor = -1
    EvenValueBackColor = -1
    EvenValueForeColor = -1
Enddefine

*--- Oggetti per la definizione di colonne di dimensioni diverse
Define Class VirtualGridColumn As Custom

    Visible = .T.
    ControlSource = ""
    cColType = ""
    bOrdering = .F. && .t. se si sta facendo ordinamento su questa colonna
    nOrdering = 0 && indice di ordinamento

    HeaderTitle = ""
    HeaderAlignment = 2
    HeaderFontSize = 10
    HeaderFontName = ""
    HeaderFontBold = .F.
    HeaderFontItalic = .F.

    HeaderBackColor = -1
    HeaderForeColor = -1

    ValueAlignment = 0
    ValueFormat = ""
    ValueFontSize = 10
    ValueFontName = ""
    ValueFontBold = .F.
    ValueFontItalic = .F.

    OddValueBackColor = -1
    OddValueForeColor = -1
    EvenValueBackColor = -1
    EvenValueForeColor = -1

    DynamicBackColor = ""
    DynamicForeColor = ""
    DynamicFontBold = ""
    DynamicFontItalic = ""
Enddefine

*--- ViewFrame, mostra la porzione di griglia
Define Class VirtualGridViewFrame As Container
    BackStyle = 0
    BorderWidth = 0
    Add Object oCntCell As VitualGridCntCell With Top=0, Left=0

    Procedure MouseWheel
        Lparameters tnDirection, tnShift, tnXCoord, tnYCoord
        This.Parent.MouseWheel(m.tnDirection, m.tnShift, m.tnXCoord, m.tnYCoord)
    Endproc
Enddefine

*--- Oggetto Griglia Vituale
Define Class VirtualGrid As Container
    BackColor = Rgb(255,255,255)
    BorderColor = Rgb(241,241,241)
    nRowMinHeight = 16
    RowHeight = 22	&&Se <1 le righe hanno altezza variabile
    nColMinWidth = 70

    Add Object Columns As Collection
    Add Object Rows As Collection

    nHOffset = -1	&&Spazio tra colonne
    nVOffset = -1	&&Spazio tra righe

    ActiveRow = 0	&& Posizione assoluta
    ActiveColumn = 0 && Posizione assoluta
    RelativeRow = 0 && Posizione della cella attiva relativa alla prima riga visibile
    *RelativeColumn = 0 &&Posizione della cella attiva relativa alla prima colonna visibile

    AllowCellSelection = .T.

    AllowRowSizing = .F.

    ColumnCount = 1
    RowCount = 0

    _nMinTop = 0

    GridLineColor = Rgb(192,192,192)
    GridLines = 3 &&0 None 1 Horizontal 2 Vertical 3 (Default) Both
    GridLineWidth = 1

    ScrollBars = 3 &&0 None 1 Horizontal 2 Vertical 3 Both vertical and horizontal (Default)

    *-- Specifies the horizontal scrolling increment for a control's horizontal scroll bar.
    HscrollSmallChange = 8
    *-- Specifies the vertical scrolling increment for a controls vertical scroll bar.
    VscrollSmallChange = 8
    nHSBH = 16 && Height of the Horizontal ScrollBar
    nVSBW = 16	&& Width of the Vertical ScrollBar
    nOldHeight=0

    bDoScroll = .F.

    HotTracking = 0	&&0, 1 Row ,2 Cell

    *-- The number of lines to scroll when the mouse wheel is rotated. If this number is less than 0 control will use system default value (3 lines). If this number is equal to 0 control will not support mouse wheel.
    WheelScrollLines = -1

    Add Object oViewFrame As VirtualGridViewFrame With Top=0, Left=0

    Add Object oHScrollbar As VirtualGridScrollBar_H
    Add Object oVScrollbar As VirtualGridScrollBar_V

    Procedure Goto(nRow)
        Local nTop,nDeltaY
        With This
            .oVScrollbar.nPosition = .oViewFrame.oCntCell._CalculateTopFromRow(m.nRow)
        Endwith
    Endproc

    Protected bscrollby
    bscrollby = .F.

    bUpdatingScrollBars = .F.

    Procedure Redraw()
        This.oViewFrame.oCntCell.Redraw()
    Endproc

    Procedure Scrolled(nScrollDir)
        *--- Evento scrolled
    Endproc

    Procedure ScrollBy(tnDeltaX, tnDeltaY)
        Thisform.LockScreen=.T.
        Local nMinTop
        With This
            If !.bscrollby
                .bscrollby = .T.
                .oViewFrame.oCntCell.Move(.oViewFrame.oCntCell.Left + m.tnDeltaX, .oViewFrame.oCntCell.Top + m.tnDeltaY)
                .oViewFrame.oCntCell.Redraw()
                .bscrollby = .F.
            Endif
        Endwith
        Thisform.LockScreen=.F.
    Endproc


    *-- Updates scroll bars.
    Procedure UpdateScrollBars
        With This
            If !.bUpdatingScrollBars
                .bUpdatingScrollBars = .T.
                If Bitand( .ScrollBars, SBC_VERT) != 0
                    .oVScrollbar.Visible = .T.
                    .oVScrollbar.Update()
                Else
                    .oVScrollbar.Visible = .F.
                Endif
                If Bitand( .ScrollBars, SBC_HORS) != 0
                    .oHScrollbar.Visible = .T.
                    .oHScrollbar.Update()
                Else
                    .oHScrollbar.Visible = .F.
                Endif
                .bUpdatingScrollBars = .F.
            Endif
        Endwith
    Endproc

    Hidden Procedure scrollbars_assign
        Lparameters tnNewVal
        This.ScrollBars = m.tnNewVal
        This.Resize
    Endproc

    Procedure MouseWheel
        Lparameters tnDirection, tnShift, tnXCoord, tnYCoord

        #Define WHEEL_DELTA		120

        With This
            If .WheelScrollLines > 0
                If Bitand( .ScrollBars, SBC_VERT) != 0 And .oVScrollbar.Enabled
                    .oVScrollbar.nPosition = .oVScrollbar.nPosition - ;
                        (.VscrollSmallChange * .WheelScrollLines * Round( m.tnDirection / WHEEL_DELTA, 0))
                Else
                    If Bitand( .ScrollBars, SBC_HORS) != 0 And .oHScrollbar.Enabled
                        .oHScrollbar.nPosition = .oHScrollbar.nPosition - ;
                            (.HscrollSmallChange * .WheelScrollLines * Round( m.tnDirection / WHEEL_DELTA, 0))
                    Endif
                Endif
            Endif
        Endwith
    Endproc

    Procedure Init
        With This
            #Define SM_MOUSEWHEELPRESENT		75
            #Define SPI_GETWHEELSCROLLLINES		0x0068
            Declare Integer GetSystemMetrics In Win32API Integer

            If GetSystemMetrics( SM_MOUSEWHEELPRESENT) != 1
                && Mouse with a wheel isn't installed
                .WheelScrollLines = 0
            Else
                If Type( ".WheelScrollLines") != "N" Or .WheelScrollLines < 0
                    && retrieve number of scroll lines

                    Declare Integer SystemParametersInfo In Win32API Integer, Integer, Integer @, Integer

                    Local lnScrollLines
                    lnScrollLines = 3		&& default value

                    If SystemParametersInfo( SPI_GETWHEELSCROLLLINES, 0, @m.lnScrollLines, 0) != 1
                        .WheelScrollLines = 3
                    Else
                        .WheelScrollLines = m.lnScrollLines
                    Endif
                    *!*				Clear Dlls SystemParametersInfo
                Else
                    .WheelScrollLines = Int( .WheelScrollLines)
                Endif
            Endif
            .Resize()
        Endwith

    Endproc


    Procedure Resize
        Local lOldAutoYield
        lOldAutoYield = _vfp.AutoYield
        _vfp.AutoYield = .F.

        Local lnMargin, lnVPWidth, lnVPHeight, lOldLockScreen
        lOldLockScreen = Thisform.LockScreen
        Thisform.LockScreen = .T.
        With This

            lnMargin = .BorderWidth

            If This.SpecialEffect != 2
                lnMargin = m.lnMargin + 1
            Endif

            lnVPWidth = Max( 0, .Width - ( .nVSBW + m.lnMargin * 2))
            lnVPHeight = Max( 0, .Height - ( .nHSBH + m.lnMargin * 2))

            If Bitand( .ScrollBars, SBC_VERT) = 0
                lnVPWidth = m.lnVPWidth + .nVSBW
            Endif

            If Bitand( .ScrollBars, SBC_HORS) = 0
                lnVPHeight = m.lnVPHeight + .nHSBH
            Endif

            If Bitand( .ScrollBars, SBC_VERT) != 0 And .oVScrollbar.NeedsScrollbarVisible( m.lnVPHeight)
                .oVScrollbar.Visible = .T.
                .oVScrollbar.Enabled = .T.
            Else
                .oVScrollbar.Visible = .F.
                .oVScrollbar.Enabled = .F.
                If Bitand( .ScrollBars, SBC_VERT) != 0
                    lnVPWidth = m.lnVPWidth + .nVSBW
                Endif
            Endif

            If Bitand( .ScrollBars, SBC_HORS) != 0 And .oHScrollbar.NeedsScrollbarVisible( m.lnVPWidth)
                .oHScrollbar.Visible = .T.
                .oHScrollbar.Enabled = .T.
            Else
                .oHScrollbar.Visible = .F.
                .oHScrollbar.Enabled = .F.
                If Bitand( .ScrollBars, SBC_HORS) != 0
                    lnVPHeight = m.lnVPHeight + .nHSBH
                Endif
            Endif

            .oViewFrame.Move( m.lnMargin, m.lnMargin, m.lnVPWidth, m.lnVPHeight)

            .oHScrollbar.Move( m.lnMargin, lnVPHeight + m.lnMargin, m.lnVPWidth, .nHSBH)
            .oVScrollbar.Move( m.lnVPWidth + m.lnMargin, m.lnMargin, .nVSBW, m.lnVPHeight)
            .oHScrollbar.CalcAutoRange()
            .oVScrollbar.CalcAutoRange()
        Endwith
        Thisform.LockScreen = m.lOldLockScreen
        _vfp.AutoYield = m.lOldAutoYield
    Endproc

    Proc SetCell(oCtrl, nRow, nCol)
        *!*	        m.oCtrl.Value = Alltrim(Str(m.nRow))+","+Alltrim(Str(m.nCol))
        *!*	        m.oCtrl.BackColor = Iif(Mod(nRow,2)=0, Rgb(0,200,200),Rgb(255,255,255))
        *!*	        *m.oCtrl.ForeColor =
        oCtrl = .Null.
    Endproc

    Function GetCellClass(nRow, nCol)
        Return "VirtualGridTextBox"
    Endfunc

    Function GetCellSize(nRow, nCol, nRowRet As Integer @, nColRet As Integer @, nSpanTypeCtrl As Integer @)
        *--- 0 Cella 1x1 : nRowRet, nColRet invariati
        *--- 1 Cella padre NxM : nRowRet, nColRet Span
        *--- 2 Cella figlia : nRowRet, nColRet Cella padre
        *!*	        Do Case
        *!*	            Case (nRow=2 And nCol=3) Or (nRow=3 And nCol=2) Or (nRow=3 And nCol=3)
        *!*	                nRowRet = 2
        *!*	                nColRet = 2
        *!*	                nSpanTypeCtrl = 2
        *!*	            Case nRow=2 And nCol=2
        *!*	                nRowRet = 1
        *!*	                nColRet = 1
        *!*	                nSpanTypeCtrl = 1
        *!*	            Case (nRow=10 And nCol=3) Or (nRow=11 And nCol=2) Or (nRow=11 And nCol=3)
        *!*	                nRowRet = 10
        *!*	                nColRet = 2
        *!*	                nSpanTypeCtrl = 2
        *!*	            Case nRow=10 And nCol=2
        *!*	                nRowRet = 1
        *!*	                nColRet = 1
        *!*	                nSpanTypeCtrl = 1
        *!*	            Otherwise
        *!*	                nRowRet = m.nRow
        *!*	                nColRet = m.nCol
        *!*	                nSpanTypeCtrl = 0
        *!*	        Endcase
        nRowRet = m.nRow
        nColRet = m.nCol
        nSpanTypeCtrl = 0
    Endfunc

    Function _GetRowSize(nRow)
        With This
            If .RowHeight>1
                Return .RowHeight
            Else
                Return .GetRowSize(m.nRow)
            Endif
        Endwith

        *--- Pu� essere ridefinito
    Function GetRowSize(nRow)
        Return This.nRowMinHeight
    Endfunc

    Function _GetColSize(nCol)
        With This
            If Not Empty(.Columns.GetKey(m.nCol))
                Return .Columns[m.nCol].Width
            Else
                Return .GetColSize(m.nCol)
            Endif
        Endwith
    Endfunc

    Function GetColSize(nCol)
        Return This.nColMinWidth
    Endfunc

    Function GetNumColumn()
        Return This.ColumnCount
    Endfunc

    Function GetNumRow()
        Return This.RowCount
    Endfunc

    Function GetCellEnabled(nRow, nCol)
        Return .T.
    Endfunc

    Procedure RowHeight_assign(nValue)
        With This
            .RowHeight = m.nValue
            .RowCount = .RowCount
        Endwith
    Endproc

    Procedure RowCount_assign(nValue)
        With This
            .RowCount = m.nValue
            ._nMinTop = -.oViewFrame.oCntCell._CalculateTopFromRow(m.nValue+1)
            .UpdateScrollBars()
        Endwith
    Endproc

    Procedure nColMinWidth_Assign(nValue)
        With This
            .nColMinWidth = m.nValue
            Dimension .oViewFrame.oCntCell.aColumnLeft[1]
        Endwith
    Endproc
Enddefine

Define Class VitualGridCntCell As Container

    BorderWidth = 0
    BackStyle = 0

    nFirstRow = 1	&&Prima riga visibile nel viewframe
    nLastRow = 0	&&Ultima riga visibile nel viewframe

    nFirstCol = 1	&&Prima colonna visibile nel viewframe
    nLastCol = 0	&&Ultima colonna visibile nel viewframe

    Add Object oMouseEventShape As Shape With BackStyle=0, BorderStyle=0
    oCtrlMouseEvent = .Null.

    Add Object oSelectedRowShape As Shape With BackStyle=0, BorderColor=255, BorderWidth=2

    Dimension aColumnLeft[1]

    Procedure Init
        *--- Colonna 1 posizione 0
        This.aColumnLeft[1]=0
    Endproc

    Procedure oMouseEventShape.MouseWheel
        Lparameters nDirection, nShift, nXCoord, nYCoord
        This.Parent.Parent.Parent.MouseWheel(m.nDirection, m.nShift, m.nXCoord, m.nYCoord)
    Endproc

    Procedure oMouseEventShape.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This.Parent
            If .Parent.Parent.HotTracking<>0
                Thisform.LockScreen=.T.
                This.Visible=.F.
                .oSelectedRowShape.Visible=.F.
                .oCtrlMouseEvent = Sys(1270)
                If Type("This.Parent.oCtrlMouseEvent")='O' &&And Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                    Do While Type("This.Parent.oCtrlMouseEvent.Parent")='O' And Upper(Left(.oCtrlMouseEvent.Name,5)) # "CTRL_"
                        .oCtrlMouseEvent = .oCtrlMouseEvent.Parent
                    Enddo
                    If Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                        If .Parent.Parent.HotTracking = 1
                            .oSelectedRowShape.Move(1,.oCtrlMouseEvent.Top,.Width-2,.oCtrlMouseEvent.Height)
                        Else
                            .oSelectedRowShape.Move(.oCtrlMouseEvent.Left,.oCtrlMouseEvent.Top,.oCtrlMouseEvent.Width,.oCtrlMouseEvent.Height)
                        Endif
                        .oSelectedRowShape.ZOrder(0)
                    Endif
                Endif
                .oSelectedRowShape.Visible=.T.
                This.Visible = .T.
                This.ZOrder(0)
                Thisform.LockScreen = .F.
            Endif
        Endwith

    Procedure oMouseEventShape.Click
        With This.Parent
            Thisform.LockScreen=.T.
            This.Visible=.F.
            .oSelectedRowShape.Visible=.F.
            .oCtrlMouseEvent = Sys(1270)
            This.Visible=.T.
            .oSelectedRowShape.Visible=.T.
            This.ZOrder(0)
            If Type("This.Parent.oCtrlMouseEvent")='O' And !IsNull(.oCtrlMouseEvent) And Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                .oCtrlMouseEvent.ZOrder(0)
                .oCtrlMouseEvent.SetFocus
            Endif
            Thisform.LockScreen=.F.
        Endwith
    Endproc

    Procedure Resize()
        This.oMouseEventShape.Move(0,0,This.Width,This.Height)
    Endproc

    *--- Calcola la posizione della nRow-esima riga
    Function _CalculateTopFromRow(nRow)
        Local nIndex, nTop, oGrid
        With This
            m.oGrid = This.Parent.Parent
            *--- Se altezza fissa per tutte le righe
            If m.oGrid.RowHeight>1
                nTop = (m.nRow - 1) * (m.oGrid.RowHeight + m.oGrid.nVOffset)
            Else
                nTop = 0
                For nIndex=1 To m.nRow-1
                    nTop = m.nTop + m.oGrid.GetRowSize(m.nIndex) + m.oGrid.nVOffset
                Endfor
            Endif
            Return m.nTop
        Endwith
        m.oGrid = .Null.
    Endfunc

    *--- Calcola la posizione della nCol-esima colonna
    Function _CalculateLeftFromCol(nCol)
        With This
            Local nIndex, nLeft, oGrid
            Try
                m.nLeft = .aColumnLeft[m.nCol]
            Catch
                oGrid = This.Parent.Parent
                nCount = Alen(.aColumnLeft)
                nLeft = .aColumnLeft[m.nCount]
                Dimension .aColumnLeft[m.nCol]
                For nIndex = m.nCount To m.nCol-1
                    nLeft = m.nLeft + m.oGrid._GetColSize(m.nIndex) + m.oGrid.nHOffset
                    .aColumnLeft[m.nIndex+1] = m.nLeft
                Endfor
                m.oGrid = .Null.
            Endtry
            Return m.nLeft
        Endwith
    Endfunc

    *--- Gestione righe
    Function _GetRowVisible(nPosition, nViewFrameHeight, nFirstRow, nLastRow, nTopFirst, nTopLast)
        Local nRow, nTop, nPosition_Height, nTop_Prev, oGrid
        nRow = 1
        nTop = 0
        nPosition_Height = m.nPosition + m.nViewFrameHeight
        nFirstRow = -1
        nLastRow = -1
        With This
            oGrid = This.Parent.Parent
            *--- Se altezza fissa per tutte le righe evito il ciclo
            If m.oGrid.RowHeight>1
                nFirstRow = Int(m.nPosition / (m.oGrid.RowHeight + m.oGrid.nVOffset))+1
                nTopFirst = (m.nFirstRow - 1) * (m.oGrid.RowHeight + m.oGrid.nVOffset)
                nLastRow = Ceiling(m.nPosition_Height / (m.oGrid.RowHeight + m.oGrid.nVOffset))
                nTopLast = (m.nLastRow - 1) * (m.oGrid.RowHeight + m.oGrid.nVOffset)
            Else
                Do While .T.
                    nTop_Prev = m.nTop
                    nTop = m.nTop + m.oGrid.GetRowSize(m.nRow) + m.oGrid.nVOffset
                    Do Case
                        Case m.nTop >= m.nPosition And nFirstRow = -1
                            nFirstRow = m.nRow
                            nTopFirst = m.nTop_Prev
                        Case m.nTop >= m.nPosition_Height
                            nLastRow = m.nRow
                            nTopLast = m.nTop_Prev
                            Exit
                    Endcase
                    nRow = m.nRow + 1
                Enddo
            Endif
            nFirstRow = Max(m.nFirstRow, 1)
            nLastRow = Min(m.nLastRow, m.oGrid.GetNumRow())
        Endwith
        m.oGrid = .Null.
    Endfunc

    *--- Gestione colonne
    Function _GetColVisible(nPosition, nViewFrameWidth, nFirstCol, nLastCol, nLeftFirst, nLeftLast)
        Local nCol, nLeft, nPosition_Width, nLeft_Prev, nTestPos, nSub
        With This
            nPosition_Width = m.nPosition + m.nViewFrameWidth
            nNumColumn = .Parent.Parent.GetNumColumn()
            *--- Calcolo array
            =._CalculateLeftFromCol(m.nNumColumn)
            nCount = Alen(.aColumnLeft)
            nIndex_1 = 1
            nIndex_2 = m.nNumColumn
            nOld_Left = 0
            nSub = m.nIndex_2 - m.nIndex_1
            Do While m.nSub>1
                nCenter = m.nIndex_1 + Bitrshift(m.nSub,1)
                nOld_Left = ._CalculateLeftFromCol(m.nCenter)
                If m.nOld_Left > m.nPosition
                    nIndex_2 = m.nCenter
                Else
                    nIndex_1 = m.nCenter
                Endif
                nSub = m.nIndex_2 - m.nIndex_1
            Enddo
            nFirstCol = m.nIndex_1
            nCol = m.nFirstCol
            nLeftFirst = ._CalculateLeftFromCol(m.nFirstCol)
            nLeft = nLeftFirst
            nPosition_Width = m.nPosition + m.nViewFrameWidth
            Do While .T.
                nLeft_Prev = m.nLeft
                m.nLeft = ._CalculateLeftFromCol(m.nCol+1)
                If m.nLeft >= m.nPosition_Width
                    nLastCol = Min(m.nCol, m.nNumColumn)
                    nLeftLast = m.nLeft_Prev
                    Exit
                Endif
                nCol = m.nCol + 1
            Enddo
        Endwith
    Endfunc

    *--- Aggiunge cella con span
    Proc AddCellSpan(tnRow, tnCol, tnTop, tnLeft)
        Local nWidth, nHeight, nTop, nLeft, nRowRet, nColRet, nRow, nCol, cNameCtrl, oGrid, nSpanTypeCtrl
        With This
            oGrid = This.Parent.Parent
            nTop = Iif(Type("tnTop")="N", m.tnTop, ._CalculateTopFromRow(m.tnRow))
            nLeft = Iif(Type("tnLeft")="N", m.tnLeft, ._CalculateLeftFromCol(m.tnCol))
            nHeight = 0
            nWidth = 0
            m.oGrid.GetCellSize(m.tnRow, m.tnCol, @nRowRet, @nColRet, @nSpanTypeCtrl)
            cNameCtrl = "Ctrl_"+Alltrim(Str(m.tnRow))+"_"+Alltrim(Str(m.tnCol))
            .AddObject(m.cNameCtrl, m.oGrid.GetCellClass(m.tnRow, m.tnCol))
            oCtrl = .&cNameCtrl
            m.oCtrl.Visible=.T.
            *--- imposto Valori, editabilit�, colori, ecc...
            m.oGrid.SetCell(m.oCtrl, m.tnRow, m.tnCol)
            *--- Calcolo lunghezza e larghezza
            For nRow = m.tnRow To m.nRowRet+m.tnRow
                nHeight = m.nHeight + m.oGrid._GetRowSize(m.nRow) + m.oGrid.nVOffset
            Endfor
            For nCol = m.tnCol To m.nColRet+m.tnCol
                nWidth = m.nWidth + m.oGrid._GetColSize(m.nCol) + m.oGrid.nHOffset
            Endfor
            m.nWidth = m.nWidth - m.oGrid.nHOffset
            nHeight = nHeight - m.oGrid.nVOffset
            m.oCtrl.Move(m.nLeft, m.nTop, m.nWidth, m.nHeight)
        Endwith
        m.oGrid = .Null.
    Endproc

    *--- Aggiunge nuove righe, ritorna l' altezza totale di tutte le righe aggiunte
    Proc AddRows(nFirstRow, nLastRow, nTopFirst, nFirstCol, nLastCol, nLeftFirst, bResize)
        With This
            Local nRow, nCol, nWidth, nHeight, nTop, nLeft, nRowRet, nColRet, nSpanTypeCtrl, oCtrl, oGrid, cNameCtrl
            nTop = m.nTopFirst
            nLeft = m.nLeftFirst
            oGrid = This.Parent.Parent
            For nRow = m.nFirstRow To m.nLastRow
                nLeft = m.nLeftFirst
                nHeight = m.oGrid._GetRowSize(m.nRow)
                For nCol = m.nFirstCol To m.nLastCol
                    m.oGrid.GetCellSize(m.nRow, m.nCol, @nRowRet, @nColRet, @nSpanTypeCtrl)
                    nWidth = m.oGrid._GetColSize(m.nCol)
                    Do Case
                        Case nSpanTypeCtrl = 1
                            *--- Cella nRowRet X nColRet
                            cNameCtrl = "Ctrl_"+Alltrim(Str(m.nRow))+"_"+Alltrim(Str(m.nCol))
                            If !Pemstatus(This, m.cNameCtrl,5)
                                .AddCellSpan(m.nRow, m.nCol, m.nTop, m.nLeft)
                            Endif
                        Case nSpanTypeCtrl = 2
                            *--- Cella figlia
                            nParentRow = m.nRowRet
                            nParentCol = m.nColRet
                            *--- Verifico se il padre � gi� stato creato
                            cNameCtrl = "Ctrl_"+Alltrim(Str(m.nParentRow))+"_"+Alltrim(Str(m.nParentCol))
                            If !Pemstatus(This, m.cNameCtrl,5)
                                .AddCellSpan(m.nParentRow, m.nParentCol)
                            Endif
                        Otherwise
                            *--- Cella 1x1
                            cNameCtrl = "Ctrl_"+Alltrim(Str(m.nRow))+"_"+Alltrim(Str(m.nCol))
                            If !Pemstatus(This, m.cNameCtrl,5)
                                .AddObject(m.cNameCtrl, m.oGrid.GetCellClass(m.nRow, m.nCol))
                                oCtrl = .&cNameCtrl
                                *--- imposto Valori, editabilit�, colori, ecc...
                                m.oGrid.SetCell(m.oCtrl, m.nRow, m.nCol)
                                m.oCtrl.Move(m.nLeft, m.nTop, m.nWidth, m.nHeight)
                            Endif
                    Endcase
                    nLeft = m.nLeft + m.nWidth + m.oGrid.nHOffset
                Endfor
                nTop = m.nTop + (m.nHeight + m.oGrid.nVOffset)
            Endfor
            nWidth = m.nLeft - m.oGrid.nHOffset
            nHeight = m.nTop - m.oGrid.nVOffset
            If m.bResize And nHeight>0 And nWidth>0
                .Move(.Left, .Top, m.nWidth , m.nHeight)
            Endif
            oCtrl = .Null.
            m.oGrid = .Null.
        Endwith
    Endproc

    *--- Cancella vecchie righe
    Procedure RemoveRows(nFirstRow, nLastRow, nFirstCol, nLastCol, nNewFirstRow, nNewLastRow, nNewFirstCol, nNewLastCol)
        With This
            Local cNameCtrl , nRow, nCol, nSpanTypeCtrl, nRowRet, nColRet, nNewRow, nNewCol, oGrid
            oGrid = This.Parent.Parent
            For nRow = m.nFirstRow To m.nLastRow
                For nCol = m.nFirstCol To m.nLastCol
                    m.oGrid.GetCellSize(m.nRow, m.nCol, @nRowRet, @nColRet, @nSpanTypeCtrl)
                    Do Case
                        Case nSpanTypeCtrl = 1
                            *--- Cella nRowRet X nColRet
                            cNameCtrl = "Ctrl_"+Alltrim(Str(m.nRow))+"_"+Alltrim(Str(m.nCol))
                            If m.nRow+m.nRowRet<m.nNewFirstRow And Pemstatus(This, m.cNameCtrl, 5)
                                .RemoveObject(m.cNameCtrl)
                            Endif
                            If m.nCol+m.nColRet<m.nNewFirstCol And Pemstatus(This, m.cNameCtrl, 5)
                                .RemoveObject(m.cNameCtrl)
                            Endif
                        Case nSpanTypeCtrl = 2
                            *--- Cella figlia
                            cNameCtrl = "Ctrl_"+Alltrim(Str(m.nRowRet))+"_"+Alltrim(Str(m.nColRet))
                            If m.nRowRet>m.nNewLastRow And Pemstatus(This, m.cNameCtrl, 5)
                                .RemoveObject(m.cNameCtrl)
                            Endif
                            If m.nColRet>m.nNewLastCol And Pemstatus(This, m.cNameCtrl, 5)
                                .RemoveObject(m.cNameCtrl)
                            Endif
                        Otherwise
                            cNameCtrl = "Ctrl_"+Alltrim(Str(m.nRow))+"_"+Alltrim(Str(m.nCol))
                            If Pemstatus(This, m.cNameCtrl, 5)
                                .RemoveObject(m.cNameCtrl)
                            Endif
                    Endcase
                Endfor
            Endfor
            oGrid = .Null.
        Endwith
    Endproc


    *--- Reimposta oggetti, elimina e ricrea oggetti
    Procedure _Clean()
        Local l_Index, l_NumCtrl
        With This
            Thisform.LockScreen = .T.
            l_NumCtrl = .ControlCount
            For m.l_Index = m.l_NumCtrl To 1 Step -1
                If Left(Lower(.Controls[m.l_Index].Name),5) = "ctrl_"
                    .RemoveObject(.Controls[m.l_Index].Name)
                Endif
            Endfor
            .nFirstRow = 1	&&Prima riga visibile nel viewframe
            .nLastRow = 0	&&Ultima riga visibile nel viewframe

            .nFirstCol = 1	&&Prima colonna visibile nel viewframe
            .nLastCol = 0	&&Ultima colonna visibile nel viewframe
            .Redraw()
            Thisform.LockScreen = .F.
        Endwith

    Endproc

    *--- Aggiunge celle visibili / rimuove celle fuori viewframe
    Procedure Redraw()
        With This
            Local nNewFirstRow , nNewLastRow, nTopFirst, nTopLast, nWidth, nHeigth
            Local nNewFirstCol, nNewLastCol, nLeftFirst, nLeftLast, nRow, nCol
            = ._GetRowVisible(-.Top, .Parent.Height, @nNewFirstRow, @nNewLastRow, @nTopFirst, @nTopLast)
            = ._GetColVisible(-.Left, .Parent.Width, @nNewFirstCol, @nNewLastCol, @nLeftFirst, @nLeftLast)
            Thisform.LockScreen = .T.
            *--- test colonne, verifico se devo aggiungere o rimuovere colonne (scroll orizzontale)
            If .nFirstCol > m.nNewFirstCol
                *--- Devo aggiungere colonne a sinistra
                nWidth = 0
                nHeigth = 0
                nCol = Min(.nFirstCol-1, m.nNewLastCol)
                .AddRows(.nFirstRow, .nLastRow, ._CalculateTopFromRow(.nFirstRow), m.nNewFirstCol, m.nCol, m.nLeftFirst, .F.)
            Endif
            If .nLastCol < m.nNewLastCol
                *--- Devo aggiungere colonne a sinistra
                nWidth = 0
                nHeigth = 0
                nCol = Max(.nLastCol+1, m.nNewFirstCol)
                .AddRows(.nFirstRow, .nLastRow , ._CalculateTopFromRow(.nFirstRow), m.nCol, m.nNewLastCol, ._CalculateLeftFromCol(m.nCol), .T.)
            Endif

            *--- Aggiungo righe visibili
            If .nFirstRow > m.nNewFirstRow
                *--- Devo aggiungere righe in testa
                nWidth = 0
                nHeigth = 0
                nRow = Min(.nFirstRow-1, m.nNewLastRow)
                .AddRows(m.nNewFirstRow, m.nRow, m.nTopFirst, m.nNewFirstCol, m.nNewLastCol, m.nLeftFirst, .F.)
            Endif
            If .nLastRow < m.nNewLastRow
                *--- Devo aggiungere righe in coda, li aggiungo in senso contrario
                ********* Si potrebbe fare qualcosa per evitare di utilizzare la _CalculateTopFromRow *******
                nWidth = 0
                nHeigth = 0
                nRow = Max(.nLastRow+1, m.nNewFirstRow)
                .AddRows(m.nRow, m.nNewLastRow, ._CalculateTopFromRow(m.nRow), m.nNewFirstCol, m.nNewLastCol, m.nLeftFirst, .T.)
            Endif

            *--- Cancello righe non visibili
            If .nFirstRow < m.nNewFirstRow
                .RemoveRows(.nFirstRow, Min(m.nNewFirstRow-1, .nLastRow), .nFirstCol, .nLastCol, m.nNewFirstRow, m.nNewLastRow, m.nNewFirstCol, m.nNewLastCol)
            Endif
            If .nLastRow > m.nNewLastRow
                .RemoveRows(Max(m.nNewLastRow+1, .nFirstRow), .nLastRow, .nFirstCol, .nLastCol, m.nNewFirstRow, m.nNewLastRow, m.nNewFirstCol, m.nNewLastCol)
            Endif

            *--- Cancello righe non visibili
            If .nFirstCol < m.nNewFirstCol
                .RemoveRows(.nFirstRow, .nLastRow, .nFirstCol, Min(m.nNewFirstCol-1, .nLastCol), m.nNewFirstRow, m.nNewLastRow, m.nNewFirstCol, m.nNewLastCol)
            Endif
            If .nLastCol > m.nNewLastCol
                .RemoveRows(.nFirstRow, .nLastRow, Max(m.nNewLastCol+1, .nFirstCol), .nLastCol, m.nNewFirstRow, m.nNewLastRow, m.nNewFirstCol, m.nNewLastCol)
            Endif

            .nFirstRow = m.nNewFirstRow
            .nLastRow = m.nNewLastRow
            .nFirstCol = m.nNewFirstCol
            .nLastCol = m.nNewLastCol
            .oMouseEventShape.ZOrder(0)
            Thisform.LockScreen = .F.
        Endwith
    Endproc
Enddefine

*--- Scrollbar
Define Class VirtualGridScrollBar_H As VirtualGridScrollBar_V
    nKind = 0
    ContinuousScroll = .F.
Enddefine

Define Class VirtualGridScrollBar_V As Container
    Width = 16
    BackColor=Rgb(241,241,241)
    BorderColor=Rgb(219,219,219)
    Add Object oScrollThumb As Shape With Width=10, Height=10, Curvature=2, Top=3, Left=3, BackColor=Rgb(217,217,217), BorderColor=Rgb(189,189,189)
    nPosition = 0
    nCalcRange = 0
    nPage = 0
    Range = 0
    nMargin = 3	&&Margine finale della scrollbar
    nThumbArea = 0
    nThumbSize = 0
    nMax = 0
    nKind = 1	&&1 Vertical 2 Horizontal
    *-- Specifies the increment a control scrolls when you click on a scroll arrow. Available at design time and run time.
    nSmallChange = 1
    nDelay = 0.0001

    Protected bthumbmove
    Protected bthumbmoving
    Protected bCalcAutoRange
    Protected nOffset
    nOffset = 0
    ContinuousScroll = .F.

    Function ControlSize()
        If This.nKind = 1
            Return This.Height
        Else
            Return This.Width
        Endif
    Endfunc

    Procedure ScrollMessage(tnMessage, tnPos)
        Local lnKind, lnOldPos, lnScrollDir
        With This
            lnScrollDir = -1
            lnKind = .nKind
            lnOldPos = .nPosition
            Do Case
                Case m.tnMessage = SB_LINEUP
                    lnScrollDir = Iif( m.lnKind = 1, 0, 4)
                    .nPosition = ( .nPosition - .nSmallChange)
                Case m.tnMessage = SB_LINEDOWN
                    lnScrollDir = Iif( m.lnKind = 1, 1, 5)
                    .nPosition = ( .nPosition + .nSmallChange)
                Case m.tnMessage = SB_PAGEUP
                    lnScrollDir = Iif( m.lnKind = 1, 2, 6)
                    .nPosition = ( .nPosition - Int( .ControlSize() / .nSmallChange) * .nSmallChange)
                Case m.tnMessage = SB_PAGEDOWN
                    lnScrollDir = Iif( m.lnKind = 1, 3, 7)
                    .nPosition = ( .nPosition + Int( .ControlSize() / .nSmallChange) * .nSmallChange)
                Case m.tnMessage = SB_THUMBPOSITION
                    .nPosition = ( m.tnPos)
                    lnScrollDir = Iif( m.lnKind = 1, ;
                        IIF( m.lnOldPos = 0 Or m.lnOldPos > m.tnPos, 0, 1), ;
                        IIF( m.lnOldPos = 0 Or m.lnOldPos > m.tnPos, 4, 5))
                Case m.tnMessage = SB_THUMBTRACK
                    .nPosition = ( m.tnPos)
                    lnScrollDir = Iif( m.lnKind = 1, ;
                        IIF( m.lnOldPos = 0 Or m.lnOldPos > m.tnPos, 0, 1), ;
                        IIF( m.lnOldPos = 0 Or m.lnOldPos > m.tnPos, 4, 5))
            Endcase
            .Parent.Scrolled( m.lnScrollDir)
        Endwith
    Endproc

    Protected Procedure SetScrollInfo()
        With This
            If .nMax > 0
                Local lnControlSize, lnThumbArea, lnThumbSize
                lnControlSize = .ControlSize()

                If .nKind = 1	&& Vertical
                    lnThumbArea = .Height - .nMargin * 2
                    lnThumbSize = Max( 26, ;
                        Min( m.lnThumbArea, ;
                        ( m.lnThumbArea) * m.lnControlSize / .nMax))
                    .oScrollThumb.Height = m.lnThumbSize
                    .nThumbArea = .Height - .oScrollThumb.Height - .nMargin * 2
                Else
                    lnThumbArea = .Width - .nMargin * 2
                    lnThumbSize = Max( 26, Min( m.lnThumbArea, ( m.lnThumbArea) * m.lnControlSize / .nMax))
                    .oScrollThumb.Width = m.lnThumbSize
                    .nThumbArea = .Width - .oScrollThumb.Width - .nMargin * 2
                Endif

                If m.lnThumbArea >= 26
                    .oScrollThumb.Visible = .T.
                Else
                    .oScrollThumb.Visible = .F.
                Endif

                .Enabled = .T.
            Else
                If .nKind = 1	&& Vertical
                    .oScrollThumb.Height = 0
                    .nThumbArea = .Height - .oScrollThumb.Height - .nMargin * 2
                Else
                    .oScrollThumb.Width = 0
                    .nThumbArea = .Width - .oScrollThumb.Width - .nMargin * 2
                Endif

                .oScrollThumb.Visible = .F.

                .Enabled = .F.
            Endif
        Endwith
    Endproc

    Procedure Resize
        With This
            .Update()
            .nPosition = .nPosition
        Endwith
    Endproc

    Procedure Update
        With This
            Local lnControlSize
            lnControlSize = .ControlSize()
            .nCalcRange = Max( 0, .Range - m.lnControlSize)
            .nPage = m.lnControlSize + 1
            .nMax = Iif( .nCalcRange > 0, .Range, 0)

            .SetScrollInfo()
            *.nPosition = .nPosition
        Endwith
    Endproc

    Procedure CalcAutoRange
        With This
            If !.bCalcAutoRange
                .bCalcAutoRange = .T.
                .Range = Iif( .nKind = 1, ;
                    .Parent.oViewFrame.oCntCell._CalculateTopFromRow(.Parent.GetNumRow()+1),;
                    .Parent.oViewFrame.oCntCell._CalculateLeftFromCol(.Parent.GetNumColumn()+1))
                .bCalcAutoRange = .F.
            Endif
        Endwith
    Endproc

    Procedure NeedsScrollbarVisible
        Lparameters tnSize
        If Pcount() = 0 Or Type( "m.tnSize") != "N"
            Return This.Range > This.ControlSize()
        Endif
        Return This.Range > m.tnSize
    Endproc

    Hidden Procedure range_assign
        Lparameters tnNewRange
        With This
            .Range = Max( 0, m.tnNewRange)
            .Parent.UpdateScrollBars()
        Endwith
    Endproc

    Hidden Procedure nPosition_assign
        Lparameters tnNewPosition

        With This
            tnNewPosition = Max( 0, Min( .nCalcRange, Int( m.tnNewPosition)))

            Local lnOldPos, lnNewThumbPos
            lnOldPos = .nPosition
            .nPosition = m.tnNewPosition

            lnNewThumbPos = Min( .nThumbArea, Max( 0, Iif( .nCalcRange = 0, 0, (.nPosition / .nCalcRange) * .nThumbArea))) + .nMargin

            If .nKind = 1	&& Vertical
                .oScrollThumb.Top = m.lnNewThumbPos
                .Parent.ScrollBy( 0, m.lnOldPos - m.tnNewPosition)
            Else
                .oScrollThumb.Left = m.lnNewThumbPos
                .Parent.ScrollBy( m.lnOldPos - m.tnNewPosition, 0)
            Endif
        Endwith
    Endproc


    Procedure Thumb_MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If m.nButton = 1
            With This
                .nOffset = Iif( .nKind = 1, m.nYCoord - .oScrollThumb.Top, m.nXCoord - .oScrollThumb.Left) + .nMargin
                .bthumbmove = .T.
            Endwith
        Endif
    Endproc


    Procedure Thumb_MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord

        If Bitand( m.nButton, 1) = 1 And This.bthumbmove And !This.bthumbmoving
            With This
                .bthumbmoving = .T.
                Local lnNewPos
                If .nKind = 1	&& Vertical
                    lnNewPos = Max( 0, Min( m.nYCoord - .nOffset, .nThumbArea))
                Else
                    lnNewPos = Max( 0, Min( m.nXCoord - .nOffset, .nThumbArea))
                Endif
                *--- Per migliori performace
                If .ContinuousScroll
                    .ScrollMessage( SB_THUMBTRACK, ( m.lnNewPos / .nThumbArea) * .nCalcRange)
                Else
                    If .nKind = 1	&& Vertical
                        .oScrollThumb.Top = Max(m.lnNewPos, .nMargin)
                    Else
                        .oScrollThumb.Left =Max(m.lnNewPos, .nMargin)
                    Endif
                Endif
                .bthumbmoving = .F.
            Endwith
        Endif
    Endproc


    Procedure Thumb_MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord

        If m.nButton = 1
            With This
                .bthumbmove = .F.
                Local lnNewPos, lnNewPos
                If .nKind = 1	&& Vertical
                    lnNewPos = Max( 0, Min( m.nYCoord - .nOffset, .nThumbArea))
                Else
                    lnNewPos = Max( 0, Min( m.nXCoord - .nOffset, .nThumbArea))
                Endif
                .ScrollMessage( SB_THUMBPOSITION, ( m.lnNewPos / .nThumbArea) * .nCalcRange)
            Endwith
        Endif
    Endproc

    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        Local loCurObject, lnSec, lnDelay, oldCursor
        oldCursor = Set("Cursor")
        Set Cursor Off
        With This
            lnDelay = This.nDelay
            nYCoord = m.nYCoord - Objtoclient( This, 1)
            nXCoord = m.nXCoord - Objtoclient( This, 2)
            If .Enabled And m.nButton = 1 And .oScrollThumb.Visible
                If ( .nKind = 1 And m.nYCoord < .oScrollThumb.Top) Or ( .nKind = 0 And m.nXCoord < .oScrollThumb.Left)
                    Nodefault
                    Do While .T.

                        *lnSec = Seconds() + lnDelay
                        .ScrollMessage( SB_PAGEUP)
                        *Wait Window "" Timeout m.lnDelay
                        Inkey(m.lnDelay)
                        loCurObject = Sys( 1270)
                        If !Mdown() Or Type( "m.loCurObject") # "O"
                            Exit
                        Endif
                    Enddo
                Else
                    If ( .nKind = 1 And m.nYCoord > .oScrollThumb.Top + .oScrollThumb.Height) Or ( .nKind = 0 And m.nXCoord > .oScrollThumb.Left + .oScrollThumb.Width)
                        Nodefault
                        Do While .T.
                            *lnSec = Seconds() + m.lnDelay
                            .ScrollMessage( SB_PAGEDOWN)
                            *                            Wait Window "" Timeout m.lnDelay
                            Inkey(m.lnDelay)
                            loCurObject = Sys( 1270)
                            If !Mdown() Or Type( "m.loCurObject") # "O"
                                Exit
                            Endif
                        Enddo
                    Endif
                Endif
            Endif
        Endwith
        Set Cursor &oldCursor
    Endproc

    Procedure oScrollThumb.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Thumb_MouseUp( nButton, nShift, nXCoord, nYCoord)
    Endproc


    Procedure oScrollThumb.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Thumb_MouseDown( nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure oScrollThumb.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Thumb_MouseMove( nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure MouseWheel
        Lparameters nDirection, nShift, nXCoord, nYCoord
        This.Parent.MouseWheel( nDirection, nShift, nXCoord, nYCoord)
    Endproc


    Procedure oScrollThumb.MouseWheel
        Lparameters nDirection, nShift, nXCoord, nYCoord
        This.Parent.MouseWheel( nDirection, nShift, nXCoord, nYCoord)
    Endproc

Enddefine

Define Class VirtualGridTextBox As TextBox
    Margin = 0
    Themes=.F.
    Visible=.T.
    SpecialEffect=1
Enddefine
*--- Zucchetti Aulla - Fine

*--- Zucchetti Aulla Inizio - cp_MenuGrid
*--- Visualizzazione Menu con controllo Virtual Grid
Define Class IndentShp As Shape
    Width = 22
    Height = 22
    BorderStyle = 0
    BackStyle=0

    Procedure SetValue(cCursor)
        This.Visible=.T.
    Endproc
Enddefine

Define Class MenuNode As Container
    BorderWidth=0
    BackStyle=0
    Height =22
    Add Object oImg As Image With Top=1, Left=1,Width=18, Height=18
    Add Object oLbl As Label With Top=2, Left=22, Height=18, BackStyle=0

    Procedure SetValue(cCursor)
        Local oMenu
        With This
	        m.oMenu = .Parent.Parent.Parent
	        .oImg.Picture = cp_TmpColorizeImg(ForceExt(Alltrim(&cCursor..cImage),'bmp'), m.oMenu.ForeColor)
	        .oLbl.Caption = Alltrim(&cCursor..VoceMenu)
	        .oLbl.AutoSize = .T.
	        
	        .oLbl.Anchor = 0
	        .oLbl.Top = 1
	        .oLbl.Anchor = 3
	        
	        *-- Style
	        .oLbl.FontName = m.oMenu.FontName
	        .oLbl.ForeColor = m.oMenu.ForeColor
	        .oLbl.FontBold = m.oMenu.FontBold
	        .oLbl.FontItalic = m.oMenu.FontItalic
	        
	        *--- ToolTip
	        .ToolTipText = Alltrim(&cCursor..ToolTip)
	        .oImg.ToolTipText = Alltrim(&cCursor..ToolTip)
	        .oLbl.ToolTipText = Alltrim(&cCursor..ToolTip)
	        .Visible=.T.
        Endwith
    Endproc

Enddefine

* --- Macro per oggetto cp_MenuGrid
#Define UNDERLINE 1		&& tipo hottracking: sottolinea voce menu
#Define RECTANGLE 2		&& tipo hottracking: evidenzia voce menu con un rettangolo
#Define SHAPELIGHT 3	&& tipo hottracking: evidenzia voce menu con uno shape colorato

Define Class cp_MenuGrid As VirtualGrid
    cVmnFile = ""	&& File .vmn del menu da visualizzare
    cCursor = ""  && Cursore contenete i dati da visualizzare
    cViewCursor = ""
    cEvent = ""
    SelBackColor = Rgb(0,105,155)
    SelBorderColor = Rgb(0,105,155)
    
    ScrollBarsWidth = 6 && Larghezza scrollbars
    
    FontName = "Open Sans"
    ForeColor = Rgb(255,255,255)
    FontBold = .f.
    FontItalic = .f.

    Add Object _oDummy As CommandButton With Top=-1, Left=-1, Width=1, Height=1, Style=1

    Procedure Init()
        With This
            *.Move(.Left, .Top, .Width, .Height)
            .RowHeight = 18
            .nColMinWidth = 18
            .nRowMinHeight = 18
            .nHOffset = 1
            .nVOffset = 2
            .ColumnCount = 50
            .HotTracking = 3
            .BackStyle=0
            .BorderWidth=0
            .oViewFrame.AddProperty('oSelNode',.Null.) && nodo attualmente selezionato
            .oViewFrame.oCntCell.oSelectedRowShape.BackColor = .SelBackColor
            .oViewFrame.oCntCell.oSelectedRowShape.BorderColor = .SelBorderColor
            
            *--- grafica Scrollbar
            .oVScrollbar.BackStyle = 0
            .oVScrollbar.BorderWidth = 0
            .nVSBW = .ScrollBarsWidth
            .oVScrollbar.oScrollThumb.Move(1, .oVScrollbar.oScrollThumb.Top, .nVSBW-1)
            .oVScrollbar.oScrollThumb.BackColor = .ForeColor
            .oVScrollbar.oScrollThumb.BorderColor = .ForeColor

            .oHScrollbar.BackStyle = 0
            .oHScrollbar.BorderWidth = 0
            .nHSBH = .ScrollBarsWidth
            .oHScrollbar.oScrollThumb.Move(.oHScrollbar.oScrollThumb.Left, 1, .oHScrollbar.oScrollThumb.Width, .nHSBH-1)
            .oHScrollbar.oScrollThumb.BackColor = .ForeColor
            .oHScrollbar.oScrollThumb.BorderColor = .ForeColor      
            
            .InitMenu()
        Endwith
        DoDefault()
    Endproc

    Proc InitMenu()
        Local l_oldArea,cOldErr,cLev,cTempCursor,cKey
        Private err
        * --- Se ho un file vmn chiudo il vecchio cursore e me ne faccio uno nuovo
        m.l_oldArea = Select()
        If Not Empty(This.cVmnFile) And File(This.cVmnFile)
            If Used(This.cCursor)
                Use In Select(This.cCursor)
            Endif
            This.cCursor = 'MenuGrid'+Sys(2015)
            cTempCursor = Sys(2015)
            Do vmn_to_cur With This.cVmnFile,cTempCursor In vu_exec
            Select VoceMenu, Level, NAMEPROC, Directory, Enabled, CPBmpName As cImage, Modulo, LvlKey, Space(254) As ToolTip, 'C' As State, .F. As Visible From (cTempCursor) Into Cursor (This.cCursor) Readwrite
            Use In Select(cTempCursor)
        Endif
        * --- Se non ho un vmnFile n� un cursore uso i_cur_menu
        If Empty(This.cCursor) And Used(i_CUR_MENU)
            This.cCursor = 'MenuGrid'+Sys(2015)
            Select VoceMenu, Level, NAMEPROC, Directory, Enabled, CPBmpName As cImage, Modulo, LvlKey, Space(254) As ToolTip, 'C' As State, .F. As Visible From (i_CUR_MENU) Into Cursor (This.cCursor) Readwrite
        Endif

        If Used(This.cCursor)
            * --- Elimino la voce principale e alzo di un livello tutte le altre voci
            cTempCursor = Sys(2015)
            Select * From (This.cCursor) Where Lower(VoceMenu)=Lower(cp_Translate('Men� principale')) Into Cursor (cTempCursor)
            If Reccount(cTempCursor)=1
                Select (This.cCursor)
                Go Top
                Delete From (This.cCursor) Where Lower(VoceMenu)=Lower(cp_Translate('Men� principale'))
                Update (This.cCursor) Set LvlKey = Substr(LvlKey,5)
                Go Top
            Endif
            Use In Select(cTempCursor)
            Select (This.cCursor)
            err=.F.
            cTempCursor=This.cCursor
            cOldErr=On('ERROR')
            On Error err=.T.
            cLev=&cTempCursor->LvlKey
            On Error &cOldErr
            If Not err
                * --- Elimino dal Menu i separatori
                Delete From (This.cCursor) Where Directory = 4
                * --- Elimino dal Menu i nodi non abilitati
                Select (cTempCursor)
                Go top
                Scan For Not Enabled Or Not Empty(Modulo)
                    i_bModuleInstalled=Enabled
                    i_cMod=Upper(Alltrim(Modulo))
                    If !Empty(i_cMod)
                        If At('(',i_cMod)>0
                            i_cMod  = Strtran(i_cMod,"*","'")
                            i_bModuleInstalled=Eval(i_cMod)
                        Else
                            If At(','+i_cMod+',',','+i_cModules+',')=0
                                i_bModuleInstalled=.F.
                            Endif
                        Endif
                    Endif
                    If Not i_bModuleInstalled
                        cKey = Alltrim(LvlKey)
                        Delete From (This.cCursor) Where Like(cKey+'.*', LvlKey)
                        Delete From (This.cCursor) Where LvlKey==cKey
                        Go top
                    Endif
                    Select(cTempCursor )
                Endscan
                * --- Elimino la '&' dalle voci, aggiorno il livello e setto l'immagine
                Update (This.cCursor) Set VoceMenu = Strtran(VoceMenu,'&'), Level = Occurs('.',LvlKey)+1
                Update (This.cCursor) Set Visible = Iif(Level=1,.T.,.F.), cImage = Iif(Directory=1,'mmaster.bmp','mfolder.bmp')
            Else
                cp_ErrorMsg(cp_MsgFormat(MSG_A__A_STRUCTURE_IS_NOT_CORRECT,This.cCursor),"!",'',.F.)
            Endif
            This.ViewMenu()
        Endif
    	Select(m.l_oldArea)
    Endproc

    Proc ViewMenu()
        Local l_oldArea
        With This
	        If Used(.cCursor)
		        m.l_oldArea = Select()
		        Use In Select(.cViewCursor)
		        .cViewCursor = 'ViewMenuGrid'+Sys(2015)
		        Select * From (.cCursor) Into Cursor (.cViewCursor) Where Visible Order By LvlKey
		        .RowCount = Reccount(.cViewCursor)
		        .ColumnCount = .GetNumColBySize()
		        .oViewFrame.oCntCell._Clean()
		        .Resize()
		        Select(m.l_oldArea)
	        Endif
        	.oViewFrame.oCntCell.oSelectedRowShape.Visible=.F.
        Endwith
    Endproc

    * --- Effettua una ricerca all'interno del menu visualizzando solo le foglie
    Proc Search(cSearching)
        m.cSearching = Alltrim(m.cSearching)
        If Not Empty(m.cSearching)
            If Len(m.cSearching)>=3
                Use In Select(This.cViewCursor)
                This.cViewCursor = 'ViewMenuGrid'+Sys(2015)
                *--- prendo solo le foglie che hanno il valore ricercato nel nome
                Select * From (This.cCursor) Where Directory=1 And Lower(VoceMenu) Like Lower('%'+m.cSearching+'%') Into Cursor (This.cViewCursor) Readwrite
                Update (This.cViewCursor) Set Level=1
                *--- Tooltip
                Update (This.cViewCursor) Set ToolTip=This.GetToolTipText(LvlKey)
                This.RowCount = Reccount(This.cViewCursor)
                This.ColumnCount = This.GetNumColBySize()
                This.oViewFrame.oCntCell.oSelectedRowShape.Visible=.F.
                This.oViewFrame.oCntCell._Clean()
                This.Resize()
            Endif
        Else
            Update (This.cCursor) Set Visible = Iif(Level=1,.T.,.F.), State='C'
            This.ViewMenu()
        Endif
    Endproc

    * --- Compone il ToolTip delle voci risultanti da una ricerca con il loro percorso all'interno del menu
    Function GetToolTipText(cLvlKey)
        Local l_OldArea, cToolTipText, nPoint, l_i
        m.cLvlKey = Alltrim(m.cLvlKey)
        If Not Empty(m.cLvlKey)
            l_OldArea = Select()
            cToolTipText = ''
            l_i = 1
            m.nPoint = At('.',cLvlKey, l_i)-1
            Select(This.cCursor)
            Do While m.nPoint<>-1
                Locate For LvlKey == Left(m.cLvlKey,m.nPoint)
                cToolTipText = cToolTipText+Iif(Not Empty(cToolTipText),'/','')+Alltrim(VoceMenu)
                l_i = l_i+1
                m.nPoint = At('.',cLvlKey, l_i)-1
            Enddo
            Select(l_OldArea)
        Endif
        Return cToolTipText
    Endfunc

    *--- Click
    Proc Click(nRow)
        Local l_OldArea,cTempCursor,cRowKey,nLevel,nDir,cState,cProc,bMacro
        If Vartype(m.nRow)<>'L'
            This._oDummy.SetFocus()
            l_OldArea = Select()
            If Used(This.cViewCursor) And m.nRow<=Reccount(This.cViewCursor) And Used(This.cCursor)
                Select(This.cViewCursor)
                Go Record m.nRow
                m.cRowKey = Alltrim(LvlKey)
                m.nLevel = Level
                cTempCursor = This.cCursor
                Select(cTempCursor)
                Locate For Alltrim(LvlKey) == m.cRowKey
                If Found()
	                m.nDir = &cTempCursor..Directory
		            m.cState = &cTempCursor..State
	                Do Case
	                    Case m.nDir=1 && nodo foglia
	                        m.cProc = Alltrim(&cTempCursor..NAMEPROC)
	                        m.cProc = Strtran(Trim(cProc),'#',' with ')
	                        If Not Empty(cProc)
	                            Do mhit In cp_menu With cProc, Alltrim(Strtran(cp_Translate(&cTempCursor->VoceMenu),'&'))
	                        Endif
	                    Case m.nDir<>1 And m.cState='E' && nodo da collassare
	                        Update (This.cCursor) Set State = 'C', cImage = 'mfolder.bmp' Where LvlKey==cRowKey
	                        Update (This.cCursor) Set cImage = 'mfolder.bmp' Where Like(m.cRowKey+'.*', LvlKey) And State='E'
	                        Update (This.cCursor) Set Visible = .F., State = 'C' Where Like(m.cRowKey+'.*', LvlKey)
	                        This.ViewMenu()
	                    Case m.nDir<>1 And m.cState='C' && nodo da espandere
	                        Update (This.cCursor) Set State = 'E', cImage = 'mfolderopen.bmp' Where LvlKey==cRowKey
	                        Update (This.cCursor) Set Visible = .T. Where Like(m.cRowKey+'.*', LvlKey) And Level=m.nLevel+1
	                        This.ViewMenu()
	                Endcase
                Endif
                Select(l_OldArea)
            Endif
        Endif
    Endproc

    Function GetCellClass(nRow, nCol)
        Local l_OldArea, cCursor, nLevel
        l_OldArea = Select()
        cCursor = This.cViewCursor
        Select (This.cViewCursor)
        Go Record m.nRow
        nLevel = &cCursor..Level
        Select(l_OldArea)
        If nCol>=nLevel
            Return "MenuNode"
        Else
            Return "IndentShp"
        Endif
    Endfunc

    Function GetCellSize(nRow, nCol, nRowRet As Integer @, nColRet As Integer @, nSpanTypeCtrl As Integer @)
        *--- 0 Cella 1x1 : nRowRet, nColRet invariati
        *--- 1 Cella padre NxM : nRowRet, nColRet Span
        *--- 2 Cella figlia : nRowRet, nColRet Cella padre
        Local l_OldArea, cCursor, nLevel
        l_OldArea = Select()
        cCursor = This.cViewCursor
        Select (cCursor)
        Go Record m.nRow
        nLevel = &cCursor..Level
        Select(m.l_OldArea)
        Do Case
            Case m.nCol=1 && nodo padre
                If nLevel=1  && MenuNode
                    nRowRet = 0
                    nColRet = This.ColumnCount-1
                    nSpanTypeCtrl = 1
                Else        && IndentShp
                    If nLevel=2
                        nRowRet = m.nRow
                        nColRet = m.nCol
                        nSpanTypeCtrl = 0
                    Else
                        nRowRet = 0
                        nColRet = m.nLevel-2
                        nSpanTypeCtrl = 1
                    Endif
                Endif
            Case m.nCol<>1 And nCol<nLevel && IndentShp figlio
                nRowRet = m.nRow
                nColRet = 1
                nSpanTypeCtrl = 2
            Case m.nCol<>1 And nCol=nLevel && MenuNode padre
                nRowRet = 0
                nColRet = This.ColumnCount - m.nLevel
                nSpanTypeCtrl = 1
            Case m.nCol<>1 And m.nCol>m.nLevel && MenuNode figlio
                nRowRet = m.nRow
                nColRet = m.nLevel
                nSpanTypeCtrl = 2
        Endcase
    Endfunc

    Proc SetCell(oCtrl, nRow, nCol)
        Local l_OldArea
        l_OldArea = Select()
        If Used(This.cViewCursor) And m.nRow<=Reccount(This.cViewCursor)
            Select(This.cViewCursor)
            Go Record m.nRow
            oCtrl.SetValue(This.cViewCursor)
            Select(l_OldArea)
        Endif
    Endproc

    Function _GetColSize(nCol)
        Local scrollWidth
        With This
            If Not Empty(.Columns.GetKey(m.nCol))
                Return .Columns[m.nCol].Width
            Else
                Return .nColMinWidth
            Endif
        Endwith
    Endfunc

    *--- Calc
    Proc Calculate(xValue)
    	With This
    		If !Empty(m.xValue) And File(m.xValue) And Alltrim(.cVmnFile)<>Alltrim(m.xValue)
    			.cVmnFile = Alltrim(m.xValue)
    			.InitMenu()
    		Endif
    	Endwith
    Endproc

    *--- Event
    Proc Event(cEvent)
        Local l_Event
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(m.l_Event, ' ,', ',')
        If Lower(','+Alltrim(m.cEvent)+',')$Lower(','+Alltrim(m.l_Event)+',')
            This.InitMenu()
        Endif
    Endproc

    Procedure oViewFrame.oCntCell.oMouseEventShape.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        Local cCtrlName, nStartCol, nFind
        With This.Parent
            If .Parent.Parent.HotTracking<>0
                Thisform.LockScreen=.T.
                This.Visible=.F.
                .oCtrlMouseEvent = Sys(1270)
                If Type("This.Parent.oCtrlMouseEvent")='O'
                    Do While Type("This.Parent.oCtrlMouseEvent.Parent")='O' And Type("This.Parent.oCtrlMouseEvent")='O' And Upper(Left(.oCtrlMouseEvent.Name,5)) # "CTRL_"
                        .oCtrlMouseEvent = .oCtrlMouseEvent.Parent
                    Enddo
                    If Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                        Do Case
                            Case .Parent.Parent.HotTracking=UNDERLINE && riga
                                * --- Deseleziono il precedente
                                If Vartype(.Parent.oSelNode) = 'O'
                                    .Parent.oSelNode.oLbl.FontUnderline = .F.
                                Endif
                                m.nStartCol = Val(Right(.oCtrlMouseEvent.Name,1))
                                Do While Upper(.oCtrlMouseEvent.Class)=='INDENTSHP' And m.nStartCol<=.nLastCol
                                    m.cCtrlName = Left(.oCtrlMouseEvent.Name,Rat('_',.oCtrlMouseEvent.Name))+Alltrim(Str(m.nStartCol))
                                    If Pemstatus(This.Parent,m.cCtrlName,5)
                                        .oCtrlMouseEvent = .&cCtrlName
                                    Endif
                                    m.nStartCol = m.nStartCol+1
                                Enddo
                                If m.nStartCol<=.nLastCol
                                    .oCtrlMouseEvent.oLbl.FontUnderline = .T.
                                    .Parent.oSelNode = .oCtrlMouseEvent
                                    This.ToolTipText = .Parent.oSelNode.ToolTipText
                                Endif
                            Case .Parent.Parent.HotTracking=RECTANGLE Or .Parent.Parent.HotTracking=SHAPELIGHT
                                .oSelectedRowShape.Visible=.F.
                                .oSelectedRowShape.Move(0,.oCtrlMouseEvent.Top,.Parent.Width-1,.oCtrlMouseEvent.Height)
                                .oSelectedRowShape.ZOrder(Iif(.Parent.Parent.HotTracking=RECTANGLE, 0, 1))
                                .oSelectedRowShape.Visible=.T.
                        Endcase
                        * --- Visualizzazione ToolTip
                        m.nStartCol = Val(Right(.oCtrlMouseEvent.Name,1))
                        Do While Upper(.oCtrlMouseEvent.Class)=='INDENTSHP' And m.nStartCol<=.nLastCol
                            m.cCtrlName = Left(.oCtrlMouseEvent.Name,Rat('_',.oCtrlMouseEvent.Name))+Alltrim(Str(m.nStartCol))
                            If Pemstatus(This.Parent,m.cCtrlName,5)
                                .oCtrlMouseEvent = .&cCtrlName
                            Endif
                            m.nStartCol = m.nStartCol+1
                        Enddo
                        If m.nStartCol<=.nLastCol
                            If .Parent.Parent.HotTracking=1
                                .oCtrlMouseEvent.oLbl.FontUnderline = .T.
                                .Parent.oSelNode = .oCtrlMouseEvent
                                This.ToolTipText = .Parent.oSelNode.ToolTipText
                            Else
                                This.ToolTipText = .oCtrlMouseEvent.ToolTipText
                            Endif
                        Endif
                    Endif
                Endif
                This.Visible = .T.
                This.ZOrder(0)
                Thisform.LockScreen = .F.
            Endif
        Endwith
    Endproc

    Procedure oViewFrame.oCntCell.oMouseEventShape.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        Local cCtrlName
        With This.Parent
            If .Parent.Parent.HotTracking<>0
                Thisform.LockScreen=.T.
                .oSelectedRowShape.Visible=.F.
                This.Visible=.F.
                Do Case
                    Case .Parent.Parent.HotTracking=UNDERLINE && riga
                        * --- Deseleziono il precedente
                        If Vartype(.Parent.oSelNode) = 'O'
                            .Parent.oSelNode.oLbl.FontUnderline = .F.
                        Endif
                    Case .Parent.Parent.HotTracking=RECTANGLE && rettangolo
                        .oSelectedRowShape.Visible=.F.
                    Case .Parent.Parent.HotTracking=SHAPELIGHT && highlight
                        * --- Deseleziono il precedente
                        If Vartype(.Parent.oSelNode) = 'O'
                            .Parent.oSelNode.BackStyle = 0
                            m.cCtrlName = Left(.Parent.oSelNode.Name,Rat('_',.Parent.oSelNode.Name))+'1'
                            .Parent.oSelNode = .&cCtrlName
                            .Parent.oSelNode.BackStyle = 0
                        Endif
                Endcase
                This.ZOrder(0)
                This.Visible = .T.
                Thisform.LockScreen = .F.
            Endif
        Endwith
    Endproc

    Procedure oViewFrame.oCntCell.oMouseEventShape.Click
        Local nRow
        With This.Parent
            Thisform.LockScreen=.T.
            This.Visible=.F.
            .oSelectedRowShape.Visible=.F.
            .oCtrlMouseEvent = Sys(1270)
            This.Visible=.T.
            .oSelectedRowShape.Visible=.T.
            This.ZOrder(0)
            Do While Type("This.Parent.oCtrlMouseEvent")='O' And Type("This.Parent.oCtrlMouseEvent.Parent")='O' And Upper(Left(.oCtrlMouseEvent.Name,5)) # "CTRL_"
                .oCtrlMouseEvent = .oCtrlMouseEvent.Parent
            Enddo
            Thisform.LockScreen=.F.
            If Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                .oCtrlMouseEvent.ZOrder(0)
                m.nRow = Substr(.oCtrlMouseEvent.Name,6,At('_',.oCtrlMouseEvent.Name,2)-6)
                .Parent.Parent.Click(Val(nRow))
            Endif
            Thisform.Draw()
            .Parent.Parent.Resize()
        Endwith
    Endproc

    *--- Determina il numero di colonne necessario per contenere le label delle voci
    Function GetNumColBySize()
        Return Int(This.oViewFrame.Width/(This.nColMinWidth+This.nHOffset))-1
    Endfunc

    *--- SetColor
    Proc setColor(nBackColor, nForeColor)
    	Local bReDraw
    	With This
    		m.bReDraw = .f.
    		If Vartype(m.nBackColor)=='N' And m.nBackColor>=0 And .BackColor<>m.nBackColor
    			m.bReDraw = .t.
	            
	            .SelBackColor = RGBAlphaBlending(m.nBackColor, m.nForeColor, 0.3)
	            
	            .oViewFrame.oCntCell.BackColor = m.nBackColor
	            .oViewFrame.BackColor = m.nBackColor
	            .oViewFrame.BorderColor = m.nBackColor
	            .BackColor = m.nBackColor
            Endif
        	If Vartype(m.nForeColor)=='N' And m.nForeColor>=0 And .ForeColor<>m.nForeColor
        		m.bReDraw = .t.
	            .ForeColor = m.nForeColor
				
				.SelBackColor = RGBAlphaBlending(m.nBackColor, m.nForeColor, 0.3)
	            .SelBorderColor = m.nForeColor
	            
	            .oViewFrame.oCntCell.oSelectedRowShape.BorderColor = .SelBorderColor 
	            
	            .oVScrollbar.oScrollThumb.BackColor = .ForeColor
	            .oVScrollbar.oScrollThumb.BorderColor = .ForeColor
	            .oHScrollbar.oScrollThumb.BackColor = .ForeColor
	            .oHScrollbar.oScrollThumb.BorderColor = .ForeColor
            Endif
        	If m.bReDraw
	            .oViewFrame.oCntCell.oSelectedRowShape.BackColor = .SelBackColor
	            .ViewMenu()
            Endif
        Endwith
    Endproc

    *--- HotTracking
    Proc HotTracking_Assign(xValue)
        With This.oViewFrame.oCntCell.oSelectedRowShape
            Do Case
                Case m.xValue=UNDERLINE && riga
                    .BackStyle = 0
                    .BorderStyle = 0
                Case m.xValue=RECTANGLE && rettangolo
                    .BackStyle = 0
                    .BorderStyle = 1
                    .BorderWidth = 2
                Case m.xValue=SHAPELIGHT && highlight
                    .BackStyle = 1
                    .BorderStyle = 1
                    .BorderWidth = 1
            Endcase
        Endwith
        This.HotTracking = m.xValue
    Endproc

    *--- Quando collasso una voce e la scrollbar non � pi� necessaria, devo riposizionare il contenitore
    *--- in modo tale che si vedano tutte le voci

    *--- Vertical Scrollbar Visible Assign
    Proc oVScrollbar.Visible_Assign(xValue)
        This.Visible = m.xValue
        If !This.Visible And This.Parent.oViewFrame.oCntCell.Top<0
            This.Parent.Goto(1)
        Endif
    Endproc

    *--- Horizontal Scrollbar Visible Assign
    Proc oHScrollbar.Visible_Assign(xValue)
        This.Visible = m.xValue
        If !This.Visible And This.Parent.oViewFrame.oCntCell.Left<0
            This.Parent.Goto(1)
        Endif
    Endproc

    *--- Resize
    Proc Resize()
        DoDefault()
        This.ColumnCount = This.GetNumColBySize()
    Endproc

    *--- oViewFrame Resize
    Proc oViewFrame.Resize
        DoDefault()
        This.oCntCell.Width = This.Width
    Endproc

    *--- oCntCell Resize
    Proc oViewFrame.oCntCell.Resize
        DoDefault()
        This.Width = This.Parent.Width
    Endproc

    *--- Destroy
    Proc Destroy()
        Use In Select(This.cViewCursor)
        Use In Select(This.cCursor)
        DoDefault()
    Endproc
Enddefine
*--- Zucchetti Aulla Fine - cp_MenuGrid

*--- Zucchetti Aulla - Inizio

*--- Macro per effetti transizione (le tipologie si possono trovare nella cp_SingleTransition presente nella cp_lib)
#Define TRSDESCRI "Linear"
#Define TRSSELECT "SineEaseOut"

#Define DROPZONECODE "0000000000"

*--- Timer per la visualizzazione della descrizione e dei tooltip del Gadget
Define Class LibraryGridCtrlTimer As Timer
    Interval = 1000
    Enabled = .F.

    oCtrl = .Null. && riferimento all'oggetto di cui visualizzre la descrizione
    oNextCtrl = .Null. && riferimento all'oggetto successivo da elaborare
    bExecuting = .F. && indica se il timer � scattato (sto eseguendo la Timer())

    Proc Init(oCtrl, nInterval)
        This.Interval = Evl(m.nInterval, This.Interval)
        This.oCtrl= m.oCtrl
        This.Enabled=.T.
    Endproc


    Proc Timer()
        With This
	        .bExecuting = .T.
	        If !Isnull(.oCtrl)
	        	Do case
	        		Case Upper(Left(.oCtrl.Name,5))=="CTRL_"
		        		If .oCtrl.oCntInfo.Top=0
	            cp_SingleTransition(.oCtrl.oCntInfo, "Top", -(.oCtrl.Parent.Parent.Parent.RowHeight), 'N', 700, TRSDESCRI, .T.)
	            *---nascondo la linea
	            If PemStatus(This,'oCtrl',5) And !IsNull(This.oCtrl)
	            	.oCtrl.oCntInfo.oDivLine.Move(.oCtrl.oCntInfo.oDivLine.Left, .oCtrl.oCntInfo.oDivLine.Top-5)
				            Endif
				        Endif
	        		Case InList(Upper(Left(.oCtrl.Name,5)),"OCOPY","OINFO","OGOTO") And !.oCtrl.Parent.Parent.Parent.Parent.ToolTipBln.ctlVisible
	        			.oCtrl.Parent.Parent.Parent.Parent.ToolTipBln.ctlShow(6 , .oCtrl.ToolTipText, "", 0)
	        	Endcase
            	If IsNull(.oNextCtrl)
	            	.Enabled=.F.
            	Else
            		.oCtrl = .oNextCtrl
            		.oNextCtrl = .null.
            	Endif
	        Endif
    		.bExecuting = .F.
    	Endwith
    Endproc
Enddefine

*--- LibraryGrid

*--- Oggetto Custom per il mantenimento delle informazioni degli elementi della griglia selezionati
*--- anche quando quest'ultimi vengono distrutti (non visibili in griglia a seguito dello scrolling)
Define Class LibraryGridSelectedCtrl As Custom
    cKeyGadget = ''
    cGadGroup = ''
    
    Proc Init(cKeyGadget, cGadGroup)
    	This.cKeyGadget = m.cKeyGadget
    	This.cGadGroup = m.cGadGroup
    Endproc

Enddefine

*--- Contenitore con le informazioni del singolo elemento della griglia
Define Class LibraryGridCtrlCnt As Container
    BackStyle = 0
    BorderWidth = 0

    Add Object oImg As Image With Top=4, Left=36,Width=48, Height=48, Stretch=1
    Add Object oLblTitle As Label With Top = 56, Left =5, Width=110, Height=60,Alignment=2,FontSize=8,WordWrap=.T.,BackStyle=0
    Add Object oDivLine As Line With Top=121,Left=3,Width=114,Height=0,BorderWidth=1
    Add Object oLblDescri As Label With Top = 125, Left =5, Width=95, Height=110,WordWrap=.T.,BackStyle=0,FontItalic=.T.,ToolTipText=""

Enddefine

*--- Singolo elemento della grliglia
Define Class LibraryGridCtrl As Container
    Width = 120
    Height = 120
    BackStyle = 0
    BorderWidth = 0
    BorderStyle = 0
	
    Add Object oCntInfo As LibraryGridCtrlCnt With Top=0,Left=0,Width=120,Height=241
    Add Object oTopLine As Line With Top=0,Left=0,Width=120,Height=0,BorderWidth=1
    Add Object oBottomLine As Line With Top=120,Left=0,Width=120,Height=0,BorderWidth=1
    Add Object oBtnHoverShp As Shape With Top=4,Left=98,Width=18,Height=19,Curvature=4,Visible=.F.
    Add Object oCopyShp As Shape With Top=7,Left=102,Width=10,Height=14,ToolTipText='Duplica',Visible=.F.
    Add Object oInfoShp As Shape With Top=28,Left=102,Width=10,Height=14,ToolTipText='Info',Visible=.F.
    Add Object oGoToShp As Shape With Top=7,Left=6,Width=12,Height=12,ToolTipText='Vai alla library...',Visible=.F.

	Dimension aCopyPP(10,2)
	Dimension aInfoPP(14,2)
	Dimension aGoToPP(8,2)

    bSelected = .F.   && gadget selezionato
    bExclusive = .F.  && gadget per unico utente
    bAddable = .T.	  && gadget inseribile nell'interfaccia
    cKeyGadget = ''
    cGadGroup = ''
    BaseColor = Rgb(255,255,255) && colore base
    HighLightColor = 0 && colore degli item selezionati
    BaseFontColor = Rgb(69,69,69)
    HighLightFontColor = Rgb(255,255,255)
    cFont = "Open Sans"

    Procedure SetValue(cCursor, HighLightColor)
        Local cTitle,cDescri
        With This
            .HighLightColor = m.HighLightColor
            .BackColor = Rgb(236,233,216) &&gadget disabled color
            .oCntInfo.BackColor = .BaseColor
            .oCntInfo.BorderColor = RGBAlphaBlending(.HighLightColor,Rgb(255,255,255), 0.5)

            .oCntInfo.oImg.Picture = &cCursor..cImage

            * --- Taglio la stringa del titolo se troppo lunga
            m.cTitle = Alltrim(&cCursor..cTitle)
            If Len(m.cTitle)>65
            	m.cTitle = Left(m.cTitle,65)+'...'
            Endif
            .oCntInfo.oLblTitle.Caption = m.cTitle
            .oCntInfo.oLblTitle.ForeColor = .BaseFontColor
            .oCntInfo.oLblTitle.FontName = .cFont
            
            * --- Taglio la stringa di descrizione se troppo lunga
            m.cDescri = Alltrim(&cCursor..cDescri)
            If Len(m.cDescri)>70
            	.oCntInfo.oLblDescri.ToolTipText = m.cDescri
            	m.cDescri = Left(m.cDescri,70)+'...'
            Endif
            .oCntInfo.oLblDescri.Caption = m.cDescri
            .oCntInfo.oLblDescri.ForeColor = .BaseFontColor
            .oCntInfo.oLblDescri.FontName = .cFont
            .cKeyGadget = &cCursor..cKeyGadget
            *--- Drop Zone
            If .cKeyGadget==DROPZONECODE
            	.oCntInfo.oLblTitle.ForeColor = Rgb(40,40,40)
                .oCntInfo.oLblDescri.ForeColor = .oCntInfo.oLblTitle.ForeColor
                .BorderColor = Rgb(180,180,180)
                .BackStyle = 1
             Else
             	.BorderColor = Rgb(0,0,255)
             Endif
            .cGadGroup = &cCursor..cGadGroup

            *--- Controllo per Gadget gi� aggiunto
            .bAddable = &cCursor..cAddable=='S'
            
            *--- Utente Esclusivo
            .bExclusive = &cCursor..nUteEsc>0
            
            .BorderWidth = Iif(.bExclusive Or .cKeyGadget==DROPZONECODE, 1, 0)

            .oCntInfo.oDivLine.BorderColor = RGBAlphaBlending(.BaseFontColor,Rgb(255,255,255), 0.5)

            *---Linee in alto e in basso per visualizzare sempre correttamente il bordo
            .oTopLine.BorderColor = .oCntInfo.BorderColor
            .oTopLine.ZOrder(0)
            .oTopLine.Visible = .F.
            .oBottomLine.BorderColor = .oCntInfo.BorderColor
            .oBottomLine.ZOrder(0)
            .oBottomLine.Visible = .F.
            
			*--- Verifico se l'elemento � selezionato
			If .Parent.Parent.Parent.aSelected.GetKey(.Name)<>0
				.Select(.t.)
			Endif
			 
			*--- Shape "bottone" per la duplicazione dei gadget
			Dimension .aCopyPP(10,2)
			.aCopyPP[1,1] = 25
	        .aCopyPP[1,2] = 75
	        .aCopyPP[2,1] = 0
	        .aCopyPP[2,2] = 75
	        .aCopyPP[3,1] = 0
	        .aCopyPP[3,2] = 0
	        .aCopyPP[4,1] = 75
	        .aCopyPP[4,2] = 0
			.aCopyPP[5,1] = 75
	        .aCopyPP[5,2] = 25
	        .aCopyPP[6,1] = 25
	        .aCopyPP[6,2] = 25
	        .aCopyPP[7,1] = 25
	        .aCopyPP[7,2] = 100
	        .aCopyPP[8,1] = 100
	        .aCopyPP[8,2] = 100
			.aCopyPP[9,1] = 100
	        .aCopyPP[9,2] = 25
	        .aCopyPP[10,1] = 25
	        .aCopyPP[10,2] = 25
	        
	        .oCopyShp.PolyPoints = "This.Parent.aCopyPP"
	        .oCopyShp.BorderColor = Rgb(80,80,80)
	        .oCopyShp.BackStyle = 1
	        .oCopyShp.BackColor = RGBAlphaBlending(.oCopyShp.BorderColor ,GetBestForeColor(.oCopyShp.BorderColor), 0.8)
	        .oCopyShp.ToolTipText = cp_Translate(.oCopyShp.ToolTipText)
                   
             *--- Shape "bottone" per le Info dei gadget
			Dimension .aInfoPP(14,2)
			.aInfoPP[1,1] = 45
	        .aInfoPP[1,2] = 0
	        .aInfoPP[2,1] = 45
	        .aInfoPP[2,2] = 20
	        .aInfoPP[3,1] = 70
	        .aInfoPP[3,2] = 20
	        .aInfoPP[4,1] = 70
	        .aInfoPP[4,2] = 35
			.aInfoPP[5,1] = 15
	        .aInfoPP[5,2] = 35
	        .aInfoPP[6,1] = 15
	        .aInfoPP[6,2] = 50
	        .aInfoPP[7,1] = 45
	        .aInfoPP[7,2] = 50
	        .aInfoPP[8,1] = 45
	        .aInfoPP[8,2] = 85
			.aInfoPP[9,1] = 15
	        .aInfoPP[9,2] = 85
	        .aInfoPP[10,1] = 15
	        .aInfoPP[10,2] = 100
	        .aInfoPP[11,1] = 100
	        .aInfoPP[11,2] = 100
	        .aInfoPP[12,1] = 100
	        .aInfoPP[12,2] = 85
	        .aInfoPP[13,1] = 70
	        .aInfoPP[13,2] = 85
	        .aInfoPP[14,1] = 70
	        .aInfoPP[14,2] = 0
	        
	        .oInfoShp.PolyPoints = "This.Parent.aInfoPP"
	        .oInfoShp.BorderStyle = 0
	        .oInfoShp.BackStyle = 1
	        .oInfoShp.BackColor = RGB(80,80,80)
	        .oInfoShp.ToolTipText = cp_Translate(.oInfoShp.ToolTipText)
             
             *--- Shape "bottone" per l'apertura dell'anagrafica dei gadget
			Dimension .aGoToPP(8,2)
			.aGoToPP[1,1] = 0&&A
            .aGoToPP[1,2] = 0
            .aGoToPP[2,1] = 100&&B
            .aGoToPP[2,2] = 0
            .aGoToPP[3,1] = 100&&C
            .aGoToPP[3,2] = 100
            .aGoToPP[4,1] = 0&&D
            .aGoToPP[4,2] = 100
            .aGoToPP[5,1] = 0&&E
            .aGoToPP[5,2] = 16
            .aGoToPP[6,1] = 93&&F
            .aGoToPP[6,2] = 16
            .aGoToPP[7,1] = 93&&G
            .aGoToPP[7,2] = 8
            .aGoToPP[8,1] = 0&&H
            .aGoToPP[8,2] = 8
	        
	        .oGoToShp.PolyPoints = "This.Parent.aGoToPP"
	        .oGoToShp.BorderColor = Rgb(80,80,80)
	        .oGoToShp.BackStyle = 1
	        .oGoToShp.BackColor = RGBAlphaBlending(.oGoToShp.BorderColor ,GetBestForeColor(.oGoToShp.BorderColor), 0.8)
	        .oGoToShp.ToolTipText = cp_Translate(.oGoToShp.ToolTipText)
	        
	        .oBtnHoverShp.BorderColor = .oGoToShp.BorderColor
	        .oBtnHoverShp.BackStyle = 0
			            
            .Visible=.T.
        Endwith
    Endproc

    * --- Seleziona o deselezione l'oggetto se necessario (e se � aggiungibile)
    * --- bSelection: .T. Seleziona l'elemento
    * --- bSelection: .F. Deseleziona l'elemento
    Proc Select(bSelection)
        Local tm, cSelName
        With This
            If .bAddable
                Thisform.LockScreen=.F.
                Thisform.Refresh()
                * --- Testo propriet� bSelected per evitare di riselezionare l'elemento gi� selezionat
                Do Case
                    Case .bSelected And Not m.bSelection && Deseleziona
                        .BackStyle = 0
                        .oCntInfo.BackStyle = 0
                        .oCntInfo.BorderWidth = 0
                        .oCntInfo.oDivLine.BorderColor = RGBAlphaBlending(.BaseFontColor,Rgb(255,255,255), 0.5)
                        *--- transizioni per il cambio di colore
                        tm = Createobject("cp_TransitionManager")
                        tm.Add(.oCntInfo, "BackColor", .BaseColor, 'C', 300, TRSSELECT)
                        tm.Add(.oCntInfo.oLblTitle, "ForeColor", .BaseFontColor, 'C', 300, TRSSELECT)
                        tm.Add(.oCntInfo.oLblDescri, "ForeColor", .BaseFontColor, 'C', 300, TRSSELECT)
                        tm.Start()
                        *---
                        .oTopLine.Visible = .F.
                        .oBottomLine.Visible = .F.
                        *--- Controllo se sto deselezionando un oggetto che � stato distrutto dallo scrolling
                        If .Parent.Parent.Parent.aSelected.GetKey(.Name)>0
	                        m.cSelName = .Parent.Parent.Parent.aSelected.Item(.Name)
	                        .Parent.Parent.Parent.aSelected.Remove(.Name)
	                        .Parent.Parent.Parent.RemoveObject(m.cSelName.Name)
                        Endif
                        .bSelected = .F.
                    Case Not .bSelected And m.bSelection && Seleziona
                        .BackStyle = 1
                        .oCntInfo.BackStyle = 1
                        .oCntInfo.BorderWidth = 1
                        .oCntInfo.oDivLine.BorderColor = .oCntInfo.BorderColor
                        *--- transizioni per il cambio di colore
                        tm = Createobject("cp_TransitionManager")
                        tm.Add(.oCntInfo, "BackColor", .HighLightColor, 'C', 300, TRSSELECT)
                        tm.Add(.oCntInfo.oLblTitle, "ForeColor", .HighLightFontColor, 'C', 300, TRSSELECT)
                        tm.Add(.oCntInfo.oLblDescri, "ForeColor", .HighLightFontColor, 'C', 300, TRSSELECT)
                        tm.Start()
                        *---
                        .oTopLine.Visible = .T.
                        .oBottomLine.Visible = .T.
                        *--- Controllo se sto riselezionando un oggetto che era stato distrutto dallo scrolling
                        If .Parent.Parent.Parent.aSelected.GetKey(.Name)==0
							m.cSelName = 'SEL'+Sys(2015)
                        	.Parent.Parent.Parent.AddObject(m.cSelName, 'LibraryGridSelectedCtrl', .cKeyGadget,.cGadGroup )
                        	.Parent.Parent.Parent.aSelected.Add(.Parent.Parent.Parent.&cSelName, .Name)
						Endif
                        .bSelected = .T.
                Endcase
            Endif
        Endwith
    Endproc

    Proc bAddable_Assign(xValue)
        With This
            .bAddable= m.xValue
            If .bAddable
	                .oCntInfo.oLblTitle.ForeColor = .BaseFontColor
	                .oCntInfo.oLblDescri.ForeColor = .BaseFontColor
	                .BackStyle = 0
	        Else
	                .oCntInfo.oLblTitle.ForeColor = RGBAlphaBlending(.oCntInfo.oLblTitle.ForeColor, Rgb(255,255,255), 0.5)
	                .oCntInfo.oLblDescri.ForeColor = .oCntInfo.oLblTitle.ForeColor
	                .BackStyle = 1
	       	Endif
        Endwith
    Endproc

	Proc oCopyShp.Click(cKey)
		DoDefault()
		Local l_Macro, l_oGadMng
		m.l_oGadMng = This.Parent.Parent.Parent.Parent.Parent.oContained.oParentObject
		l_Macro = [GSUT_BGG(m.l_oGadMng, "Duplicate", m.cKey)]
        &l_Macro
	Endproc

	Proc oInfoShp.Click(cKey)
		DoDefault()
		Local l_Title,l_Descri,l_Msg
		*--- Titolo
		m.l_Title = Alltrim(LookTab("GAD_MAST", "GA__NOME", "GACODICE", Alltrim(m.cKey)))
		*--- Descrizione
		m.l_Descri = Alltrim(LookTab("GAD_MAST", "GADESCRI", "GACODICE", Alltrim(m.cKey)))
		m.l_Descri = Iif(Empty(m.l_Descri), cp_Translate("Nessuna descrizione"), m.l_Descri)
		*--- Mostra Titolo + Descrizione
		m.l_Msg = ah_MsgFormat("%1%0%0%2",m.l_Title, m.l_Descri)
		ah_ErrorMsg(Iif(Len(m.l_Msg)<1025, Padr(m.l_Msg,1025,' '),m.l_Msg),'i')
	Endproc

	Proc oGoToShp.Click(cKey)
		DoDefault()
		Local l_Macro
		l_Macro = [OpenGest("A","GSUT_MGA","GACODICE","]+Alltrim(m.cKey)+[")]
		*--- Uso la cp_CallAfter perch� altrimenti l'anagrafica gadget non terrebbe il focus
        cp_CallAfter(l_Macro,50)
	Endproc
	
	Proc oBtnHoverShp.Visible_Assign(xValue)
		This.Visible = Iif(Vartype(m.xValue)='L',m.xValue,This.Visible)
		If This.Visible
			This.Parent.Parent.oMouseEventShape.MousePointer = 15
		Else
			This.Parent.Parent.oMouseEventShape.MousePointer = 1
		Endif
	Endproc
Enddefine

#Define GADGET_DESCRI_TOOLTIPDELAY 1000
#Define GADGET_BTN_TOOLTIPDELAY 500

Define Class LibraryGrid As VirtualGrid
    cCursor = ""  && Cursore contenete i dati da visualizzare
    cQuery = "" && Query che riempie il cursore
    oTimer = .Null. && Oggetto Timer per la visualizzazione della descrizione del gadget
    oCurrentCtrl = .Null. && Elemento della griglia che � attualmente evidenziato (mouse Hover)
    cEvent = ""
    bInit = .F. && indica se l'oggetto � stato inizializzato, per evitare l'esecuzione della query troppo presto
    bDropZone = .F. && attiva una cella dedicata alla gestione del drop
    
    DragIcon = i_cBmpPath+"add_grd.cur"
    cOldIcon = '' && cambia cursore per le gestioni
    
    HighLightColor = Rgb(64,159,255)
    
    cExecOnEnter = '' && comando da eseguire quando viene premuto il tasto Enter
    cExecOnCanc = '' && comando da eseguire quando viene premuto il tasto Canc
    
    ToolTipBln = .null.
    
    Add Object aSelected As Collection
  
    Add Object _oDummy As CommandButton With Top=-1, Left=-1, Width=1, Height=1, Style=1

    Procedure Init()
        With This
            .bInit = .F.
            .cCursor = 'LibraryGrid'+Sys(2015)
            .GridLines  = 0
            .ColumnCount = 3
            .RowHeight = 120	&&Se <1 le righe hanno altezza variabile
            .nColMinWidth = 120
            .nHOffset = 1
            .nVOffset = 1

            * --- Selected Row Shape
            .oViewFrame.oCntCell.oSelectedRowShape.BackColor = RGBAlphaBlending(.HighLightColor, Rgb(255,255,255), 0.65)
            .oViewFrame.oCntCell.oSelectedRowShape.BorderColor = .oViewFrame.oCntCell.oSelectedRowShape.BackColor
            .oViewFrame.oCntCell.oSelectedRowShape.BackStyle = 1
            .oViewFrame.oCntCell.oSelectedRowShape.BorderWidth = 1
            .oViewFrame.oCntCell.oSelectedRowShape.Visible = .F.
            * ---

            .BackColor = Rgb(255,255,255)
            .BorderWidth = 0
            .oViewFrame.BackColor = .BackColor
            .oViewFrame.BorderWidth = 0
            .oViewFrame.oCntCell.BackColor = .BackColor
            .oViewFrame.oCntCell.BorderWidth = 0
            
            If !PemStatus(Thisform,'oBalloon',5)
            	Thisform.AddObject('oBalloon','cp_balloontip')
            Endif
            .ToolTipBln = Thisform.oBalloon
            .ToolTipBln.ctlStyle=2
            .ToolTipBln.ctlHideDelay=5000
            
            
            DoDefault()
            .bInit = .T.
        Endwith
    Endproc

    Function GetCellClass(nRow, nCol)
        Return "LibraryGridCtrl"
    Endfunc

    Proc SetCell(oCtrl, nRow, nCol)
        Local l_RowNum,l_OldArea
        l_RowNum = This.ColumnCount*(m.nRow-1) + m.nCol
        l_OldArea = Select()
        If Used(This.cCursor) And m.l_RowNum<=Reccount(This.cCursor)
            Select(This.cCursor)
            Go Record m.l_RowNum
            oCtrl.SetValue(This.cCursor, This.HighLightColor)
            Select(l_OldArea)
        Endif
    Endproc

    *--- Ridefinita
    Function GetNumRow()
        Return Ceiling(This.RowCount/This.ColumnCount)
    Endfunc

    *--- Calc
    Proc Calculate(nValue)
    Endproc

    *--- Event
    Proc Event(cEvent)
        Local l_Event
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(m.l_Event, ' ,', ',')
        If Lower(','+Alltrim(m.cEvent)+',')$Lower(','+Alltrim(m.l_Event)+',')
            This._Redraw()
        Endif
    Endproc

    Proc _Redraw(bNoMsg) && Se bNoMsg=.T. non vengono visualizzati messaggi di errore
        With This
            .ClearSelected()
            .Query(m.bNoMsg)
            .oVScrollbar.nPosition = 0
            .oViewFrame.oCntCell._Clean()
            .Resize()
            If Pemstatus(.Parent,'oContained',5)
                .Parent.oContained.Height = .Parent.oContained.Height+1
                .Parent.oContained.Height = .Parent.oContained.Height-1
            Endif
        Endwith
    Endproc

    Procedure Query(bNoMsg)
        Local l_oldErr,l_errMsg,l_EvalMsg,l_oldArea,cTempCursor
        With This
            If !Empty(.cQuery)
            	m.l_oldArea = Select()
                If Type('this.parent.oContained')='O'
                    This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' before query')
                Endif

                m.cTempCursor = Sys(2015)
                vq_exec(.cQuery, .Parent.oContained, m.cTempCursor)

                *--- cancello i Gadget per cui la condizione di attivazione risulta falsa
                *--- segnalo all'utente eventuali errori di valutazione
                Use In Select(.cCursor)
                m.l_EvalMsg = ''
                Select(m.cTempCursor)
                Go Top
                m.l_oldErr = On("Error") && Modifico 
                On Error l_errMsg=Message()
                Scan for !Empty(Nvl(cCondAtt,''))
                	Local l_cKeyGadget, l_cCondAtt
                	m.l_cKeyGadget = cKeyGadget
                	m.l_cCondAtt = Alltrim(cCondAtt)
                	l_errMsg = ''
                	Replace cCondAtt with Iif(Eval(Nvl(cCondAtt,'')),'',cCondAtt)
                	If !m.bNoMsg And !Empty(m.l_errMsg) && Errore durante la valutazione della condizione
                		m.l_errMsg = ah_MsgFormat("Codice: %1%0Condizione: %2%0Errore: %3",m.l_cKeyGadget,m.l_cCondAtt,m.l_errMsg)
                		m.l_EvalMsg = Iif(Empty(m.l_EvalMsg),m.l_errMsg,ah_MsgFormat("%1%0------------------------------%0%2",m.l_EvalMsg,m.l_errMsg))
                	Endif
                Endscan
                On Error &l_oldErr
                If !m.bNoMsg And !Empty(m.l_EvalMsg)
                	Ah_ErrorMsg("Errore durante la valutazione della condizione di attivazione di alcuni gadget:%0%0%1",.F.,.F., m.l_EvalMsg)
                Endif
                Select * From (m.cTempCursor) Where Empty(Nvl(cCondAtt,'')) Into Cursor (.cCursor) Readwrite
                Use In Select(m.cTempCursor)
                
                *--- Setto cAddable sulla base del cursore cCursorModify
                If Type("oGadgetManager")='O' And !Isnull(m.oGadgetManager)
                    Local l_cKeyGadget, l_cAction
                    m.l_oldArea = Select()
                    Select(m.oGadgetManager.cCursorModify)
                    Locate For 1=1
                    Do While Not(Eof())
                        m.l_cKeyGadget = cKeyGadget
                        m.l_cAction = cAction
                        Update (.cCursor) Set cAddable=Iif(m.l_cAction=="I",'N', 'S') Where cKeyGadget=m.l_cKeyGadget
                        Select(m.oGadgetManager.cCursorModify)
                        Continue
                    Enddo
                    Select(m.l_oldArea)
                Endif

				*--- Aggiungo la cella DropZone se richiesto
				If .bDropZone
					Select * from (.cCursor) Where 1=0 into cursor (m.cTempCursor) Readwrite
					Insert into (m.cTempCursor) Values(DROPZONECODE,'Shortcut'+Chr(10)+'Drop Zone',cp_translate('Cella su cui � possibile trascinare una gestione per creare un collegamento'),'./bmp/tb_dropzone.bmp','','','N',0)
					Insert into (m.cTempCursor) Select * from (.cCursor)
					Use in Select(.cCursor)
					.cCursor = m.cTempCursor
				Endif

                If Type('this.parent.oContained')='O'
                    This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' after query')
                Endif
                .RowCount = Reccount(.cCursor)
                .ColumnCount = .GetNumColBySize()
            	Select(m.l_oldArea)
            Endif
        Endwith
    Endproc

    Procedure oViewFrame.oCntCell.oMouseEventShape.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This.Parent
        	Thisform.LockScreen=.T.
            This.Visible=.F.
            .oCtrlMouseEvent = Sys(1270)
            If Type("This.Parent.oCtrlMouseEvent")='O'
            	Do While Type("This.Parent.oCtrlMouseEvent.Parent")='O' And Type("This.Parent.oCtrlMouseEvent")='O' And !InList(Upper(Left(.oCtrlMouseEvent.Name,5)),"CTRL_","OCOPY","OINFO","OGOTO")
                    .oCtrlMouseEvent = .oCtrlMouseEvent.Parent
                EndDo
            Endif
            If m.nButton==0
            	If Type("This.Parent.oCtrlMouseEvent")='O' And InList(Upper(Left(.oCtrlMouseEvent.Name,5)),"CTRL_","OCOPY","OINFO","OGOTO")
            		If Upper(Left(.oCtrlMouseEvent.Name,5))=="CTRL_"
            			*--- Nascondo un eventuale vecchio tooltip
            			.Parent.Parent.ToolTipBln.ctlHide(2)
            			.Parent.Parent.ToolTipBln.ctlVisible = .F.
            			*--- Nascondo lo shape per l'Hover sui bottoni
						.oCtrlMouseEvent.oBtnHoverShp.Visible = .F.
						*--- Disattivo il timer
						If Vartype(.Parent.Parent.oTimer)='O'
							.Parent.Parent.oTimer.Enabled = .F.
						Endif
						.Parent.Parent.oTimer = .null.
            		Endif
            		If InList(Upper(Left(.oCtrlMouseEvent.Name,5)),"OCOPY","OINFO","OGOTO")
            			Local nBtnTop,nBtnLeft,nBtnWidth,bBtnVisible
            			*--- puntatore su uno dei bottoni (Duplica, Info, Vai alla alibrary)
            			*--- aspetta mezzo secondo e se il puntatore non viene mosso mostra il tooltip del bottone
            			Do Case
            				Case Vartype(.Parent.Parent.oTimer)<>'O'
            					.Parent.Parent.oTimer = Createobject("LibraryGridCtrlTimer", .oCtrlMouseEvent, GADGET_BTN_TOOLTIPDELAY)
	            			Case IsNull(.Parent.Parent.oTimer.oCtrl) Or .Parent.Parent.oTimer.oCtrl<>.oCtrlMouseEvent
	            				*--- Se il timer sta attualmente scattando (animazione descrizione) gli segnalo di mostrare il tooltip dopo
	            				If .Parent.Parent.oTimer.bExecuting
	            					.Parent.Parent.oTimer.oNextCtrl = .oCtrlMouseEvent
	            				Else
		            				.Parent.Parent.oTimer.Enabled = .F.
		            				.Parent.Parent.oTimer.oCtrl = .oCtrlMouseEvent
		            				.Parent.Parent.oTimer.Enabled = .T.
	            				Endif
            					.Parent.Parent.oTimer.Interval = GADGET_BTN_TOOLTIPDELAY
            			Endcase
            			m.nBtnTop = .oCtrlMouseEvent.Top
            			m.nBtnLeft = .oCtrlMouseEvent.Left
            			m.nBtnWidth = .oCtrlMouseEvent.Width
            			m.bShpVisible = .oCtrlMouseEvent.Visible
            			*--- Aggiorno il riferimento all'elemento della griglia per le successive istruzioni
            			.oCtrlMouseEvent = .oCtrlMouseEvent.Parent
            			*--- Hover sul bottone
            			.oCtrlMouseEvent.oBtnHoverShp.Top = m.nBtnTop - 3
            			.oCtrlMouseEvent.oBtnHoverShp.Left = m.nBtnLeft - (.oCtrlMouseEvent.oBtnHoverShp.Width-m.nBtnWidth)*0.5
            			.oCtrlMouseEvent.oBtnHoverShp.Visible = m.bShpVisible
            		Endif
            		If .Parent.Parent.HotTracking<>0 && HotTracking
	            		.oSelectedRowShape.Visible=.F.
	            		*--- sposto lo shape per l'hoover
	            		.oSelectedRowShape.Move(.oCtrlMouseEvent.Left, .oCtrlMouseEvent.Top, .oCtrlMouseEvent.Width, .oCtrlMouseEvent.Height)
	            		.oSelectedRowShape.ZOrder(1)
	            		.oSelectedRowShape.Visible=.T.
            		Endif
            		*--- Mostro i bottoni sul gadget (copia, vai a, info)
            		.oCtrlMouseEvent.oCopyShp.Visible = .T. And .oCtrlMouseEvent.cKeyGadget<>DROPZONECODE
            		.oCtrlMouseEvent.oInfoShp.Visible = .T. And .oCtrlMouseEvent.cKeyGadget<>DROPZONECODE
            		.oCtrlMouseEvent.oGoToShp.Visible = .T. And .oCtrlMouseEvent.cKeyGadget<>DROPZONECODE And cp_IsAdministrator()
            	Else
            		.oSelectedRowShape.Visible=.F.
            		.Parent.Parent.oTimer = .Null.
            		.oCtrlMouseEvent = .Null.
            	Endif
            	*--- Riporta l'elemento precedente alla schermata "nome+anteprima" (se necessario)
            	If !Isnull(.Parent.Parent.oCurrentCtrl) And (Isnull(.oCtrlMouseEvent) Or .oCtrlMouseEvent.Name<>.Parent.Parent.oCurrentCtrl.Name )
            		If .Parent.Parent.oCurrentCtrl.oCntInfo.Top<0
	            		Thisform.LockScreen = .F.
	            		cp_SingleTransition(.Parent.Parent.oCurrentCtrl.oCntInfo, "Top", 0, 'N', 700, TRSDESCRI, .F.)
	            		*--- nascondo la linea
	            		.Parent.Parent.oCurrentCtrl.oCntInfo.oDivLine.Move(.Parent.Parent.oCurrentCtrl.oCntInfo.oDivLine.Left, .Parent.Parent.oCurrentCtrl.oCntInfo.oDivLine.Top+5)
            		Endif
            		*--- Nascondo i bottoni sul gadget (copia, vai a, info)
            		.Parent.Parent.oCurrentCtrl.oCopyShp.Visible = .F.
            		.Parent.Parent.oCurrentCtrl.oInfoShp.Visible = .F.
            		.Parent.Parent.oCurrentCtrl.oGoToShp.Visible = .F.
            		.Parent.Parent.oCurrentCtrl.oBtnHoverShp.Visible = .F.
            	Endif
            	*--- Aggiorno elemento corrente
           		.Parent.Parent.oCurrentCtrl = .oCtrlMouseEvent
            Endif
        	*--Drag&Drop
        	If m.nButton==1 And Type("This.Parent.oCtrlMouseEvent")='O' And Upper(Left(.oCtrlMouseEvent.Name,5))=="CTRL_" And .oCtrlMouseEvent.bSelected
        		.Parent.Parent.oTimer = .Null.
        		.Parent.Parent.Drag(1)
        	Endif
        	This.Visible = .T.
            This.ZOrder(0)
            Thisform.LockScreen = .F.
        Endwith
    Endproc

	Proc oViewFrame.oCntCell.oMouseEventShape.DragOver(oSource,nXCoord,nYCoord,nState)
        Local oOverCtrl
        Thisform.LockScreen = .t.
        This.Visible = .f.
        With This.Parent.Parent.Parent
	        If Vartype(m.oSource)=='O' And m.oSource.ParentClass='Stdcontainer' And (Type("oSource.oContained.bSysForm")='U' Or !oSource.oContained.bSysForm) And .bDropZone
	        	If nState=0 Or nState=2 && Enter or Over
	            	*--- Focus alla library
	            	._oDummy.SetFocus()
	            	*--- Verifico di essere sulla Drop Zone
	            	m.oOverCtrl = Sys(1270)
		            If Type("m.oOverCtrl")=='O'
		            	Do While Type("m.oOverCtrl")=='O' And Type("m.oOverCtrl.Parent")=='O' And Upper(Left(m.oOverCtrl.Name,5))#"CTRL_"
		                    m.oOverCtrl= m.oOverCtrl.Parent
		                Enddo
		            	If Vartype(m.oOverCtrl)=='O' And Upper(Left(m.oOverCtrl.Name,5))=="CTRL_" And m.oOverCtrl.cKeyGadget==DROPZONECODE
		            		.cOldIcon = Iif(Empty(.cOldIcon), m.oSource.DragIcon, .cOldIcon)
		            		m.oSource.DragIcon=i_cBmpPath+"cross01.cur"
		            	Else
		            		m.oSource.DragIcon=Iif(Empty(.cOldIcon), m.oSource.DragIcon, .cOldIcon)
		            		.cOldIcon = ''
		            	Endif
		            Endif
		        Else && Leave
		           	m.oSource.DragIcon=Iif(Empty(.cOldIcon), m.oSource.DragIcon, .cOldIcon)
		           	.cOldIcon = ''
	           	Endif  
	        Endif
    	Endwith
    	This.Visible = .t.
        This.ZOrder(0)
        Thisform.LockScreen = .f.
    Endproc
    
    Proc oViewFrame.oCntCell.oMouseEventShape.DragDrop(oSource,nXCoord,nYCoord)
        Local oOverCtrl,l_Macro
        Thisform.LockScreen = .t.
        This.Visible = .f.
        With This.Parent.Parent.Parent
	    	m.oSource.Drag(0)
	    	ReleaseCapture()
	    	If Vartype(m.oSource)=="O" And oSource.ParentClass='Stdcontainer' And (Type("oSource.oContained.bSysForm")='U' Or !oSource.oContained.bSysForm) And .bDropZone
	        	*--- Verifico di essere sulla Drop Zone
	        	m.oOverCtrl = Sys(1270)
	            If Type("m.oOverCtrl")=='O'
	            	Do While Type("m.oOverCtrl")=='O' And Type("m.oOverCtrl.Parent")=='O' And Upper(Left(m.oOverCtrl.Name,5))#"CTRL_"
	                    m.oOverCtrl= m.oOverCtrl.Parent
	                Enddo
	            	If Vartype(m.oOverCtrl)=='O' And Upper(Left(m.oOverCtrl.Name,5))=="CTRL_" And m.oOverCtrl.cKeyGadget==DROPZONECODE
	            		m.oSource.Drag(2)
	            		*--- accetta come drop un form con gestione standard
			            Local L_oBj, l_Macro
			            L_oBj=oSource.Parent.Parent.Parent
					    Do While !IsNull(L_oBj) And !PemStatus(L_oBj,"getSecuritycode",5) And PemStatus(L_oBj,"oParentObject",5)
							*--- Se � un figlio integrato devo risalire fino al padre		    
					      	L_oBj=L_oBj.oParentObject
					    Enddo 
			            l_Macro = [GSUT_BGG(L_Obj, "ShortcutWizard")]
	    				&l_Macro
	    				*--- Focus di nuovo alla form che � stata trascinata
	    				If Vartype(m.L_oBj)='O' And PemStatus(m.L_oBj,"SetFocusOnFirstControl",5)
	    					m.L_oBj.SetFocusOnFirstControl()
	    				EndIf
	    				m.L_oBj = .null.
	            	Endif
	            Endif
			Endif
        Endwith
    	This.Visible = .t.
        This.ZOrder(0)
        Thisform.LockScreen = .f.
    Endproc

    Procedure oViewFrame.oCntCell.oMouseEventShape.MouseLeave
		Lparameters nButton, nShift, nXCoord, nYCoord
        With This.Parent.Parent.Parent
        	*--- Sistema l'elemento precedente
            If !IsNull(.oCurrentCtrl)
            	If .oCurrentCtrl.oCntInfo.Top<0 && Riporta l'elemento precedente alla schermata "nome+anteprima" (se necessario)
	                Thisform.LockScreen = .F.
	                cp_SingleTransition(.oCurrentCtrl.oCntInfo, "Top", 0, 'N', 700, TRSDESCRI, .F.)
	                *--- nascondo la linea
	                .oCurrentCtrl.oCntInfo.oDivLine.Move(.oCurrentCtrl.oCntInfo.oDivLine.Left, .oCurrentCtrl.oCntInfo.oDivLine.Top+5)
                Endif
                *--- Nascondo i bottoni sul gadget (copia, vai a, info)
                .oCurrentCtrl.oCopyShp.Visible = .F.
                .oCurrentCtrl.oInfoShp.Visible = .F.
                .oCurrentCtrl.oGoToShp.Visible = .F.
                .oCurrentCtrl.oBtnHoverShp.Visible = .F.
                .oCurrentCtrl = .Null.
            Endif  	
        	.oViewFrame.oCntCell.oSelectedRowShape.Visible=.F.
        	*--- Spengo il timer per la visualizzazione della descrizione o del tooltip
        	.oTimer = .Null.
        	*--- Nascondo un eventuale vecchio tooltip
            .ToolTipBln.ctlHide(2)
            .ToolTipBln.ctlVisible = .F.
        Endwith
    Endproc

    *--- Gestione Click per la selezione
    Procedure oViewFrame.oCntCell.oMouseEventShape.MouseDown (nButton, nShift, nXCoord, nYCoord)
        Local tempCtrl, l_i, nTemp, nLastRow, nLastCol, nClickRow, nClickCol, l_Visible
        LOCAL TestMacro
        With This.Parent
            Thisform.LockScreen=.T.
            This.Visible=.F.
            m.l_Visible = .oSelectedRowShape.Visible
            .oSelectedRowShape.Visible=.F.
            .oCtrlMouseEvent = Sys(1270)
            This.Visible=.T.
            .oSelectedRowShape.Visible=m.l_Visible
            This.ZOrder(0)
            Do While Type("This.Parent.oCtrlMouseEvent")='O' And Type("This.Parent.oCtrlMouseEvent.Parent")='O' And !InList(Upper(Left(.oCtrlMouseEvent.Name,5)),"CTRL_","OCOPY","OINFO","OGOTO")
                .oCtrlMouseEvent = .oCtrlMouseEvent.Parent
            Enddo
            Do Case
            Case Type("This.Parent.oCtrlMouseEvent")='O' And Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                .oCtrlMouseEvent.ZOrder(0)
                Do Case
                    Case nShift=1 && SHIFT
                        If .Parent.Parent.aSelected.Count==0
                            .oCtrlMouseEvent.Select(Not .oCtrlMouseEvent.bSelected)
                        Else
                            m.tempCtrl = .Parent.Parent.aSelected.GetKey(.Parent.Parent.aSelected.Count)
                            .Parent.Parent.Select(m.tempCtrl, .oCtrlMouseEvent.Name)
                        Endif
                    Case nShift=2 && CTRL
                        .oCtrlMouseEvent.Select(Not .oCtrlMouseEvent.bSelected)
                    Otherwise
                    	*-- Gestione alla MouseDown se ho al massimo un elemento selezionato oppure
                    	*--- l'elemento su cui sto cliccando non � gi� fra quelli selezionati
                    	If .Parent.Parent.aSelected.Count<2 Or .Parent.Parent.aSelected.GetKey[.oCtrlMouseEvent.Name]==0
	                    	For l_i = .Parent.Parent.aSelected.Count To 1 Step -1
	                        	If .Parent.Parent.aSelected.GetKey[l_i]<> .oCtrlMouseEvent.Name
	                            	m.tempCtrl = .Parent.Parent.aSelected.GetKey[l_i]
	                            	TestMacro=Vartype(.&tempCtrl)
	                            	If TestMacro=='O'
	                            		.&tempCtrl..Select(.F.)
	                            	Else && l'elemeno � stato distrutto a causa dello scrolling (non pi� visibile nella griglia)
	                            		m.tempCtrl = .Parent.Parent.aSelected.Item[l_i]
						                .Parent.Parent.aSelected.Remove(l_i)
						                .Parent.Parent.RemoveObject(m.tempCtrl.Name)
	                            	Endif
	                        	Endif
	                    	Endfor
	                		.oCtrlMouseEvent.Select(.T.)
                		Endif
                Endcase
            Case Type("This.Parent.oCtrlMouseEvent")='O' And InList(Upper(Left(.oCtrlMouseEvent.Name,5)),"OCOPY","OINFO","OGOTO")
            	Thisform.LockScreen=.F.
            	.oCtrlMouseEvent.Click(.oCtrlMouseEvent.Parent.cKeyGadget)
            	Thisform.LockScreen=.T.
            Otherwise
                If nShift=0
                    For l_i = .Parent.Parent.aSelected.Count To 1 Step -1
                        m.tempCtrl = .Parent.Parent.aSelected.GetKey[l_i]
                        TestMacro=Vartype(.&tempCtrl)
                        If TestMacro=='O'
                    		.&tempCtrl..Select(.F.)
                    	Else && l'elemeno � stato distrutto a causa dello scrolling (non pi� visibile nella griglia)
                    		m.tempCtrl = .Parent.Parent.aSelected.Item[l_i]
			                .Parent.Parent.aSelected.Remove(l_i)
			                .Parent.Parent.RemoveObject(m.tempCtrl.Name)
                    	Endif
                    Endfor
                Endif
            Endcase
            .Parent.Parent._oDummy.SetFocus()
            This.ZOrder(0)
            Thisform.LockScreen=.F.
        Endwith
    Endproc

	Procedure oViewFrame.oCntCell.oMouseEventShape.MouseUp (nButton, nShift, nXCoord, nYCoord)
        Local tempCtrl, l_i, nTemp, nLastRow, nLastCol, nClickRow, nClickCol
        LOCAL TestMacro
        With This.Parent
           If VarType(.oCtrlMouseEvent)=='O' And Upper(Left(.oCtrlMouseEvent.Name,5))=="CTRL_"
              	If m.nShift==0 And .Parent.Parent.aSelected.Count>=2 And .Parent.Parent.aSelected.GetKey[.oCtrlMouseEvent.Name]<>0
                	Thisform.LockScreen=.T.
                	.oCtrlMouseEvent.ZOrder(0)
                	For l_i = .Parent.Parent.aSelected.Count To 1 Step -1
                    	If .Parent.Parent.aSelected.GetKey[l_i]<> .oCtrlMouseEvent.Name
                        	m.tempCtrl = .Parent.Parent.aSelected.GetKey[l_i]
                        	TestMacro=Vartype(.&tempCtrl)
                        	If TestMacro=='O'
                        		.&tempCtrl..Select(.F.)
                        	Else && l'elemeno � stato distrutto a causa dello scrolling (non pi� visibile nella griglia)
                        		m.tempCtrl = .Parent.Parent.aSelected.Item[l_i]
				                .Parent.Parent.aSelected.Remove(l_i)
				                .Parent.Parent.RemoveObject(m.tempCtrl.Name)
                        	Endif
                    	Endif
                	Endfor
            		.oCtrlMouseEvent.Select(.T.)
        			This.ZOrder(0)
            		Thisform.LockScreen=.F.
        		Endif
            Endif
        Endwith
    Endproc

    Procedure oViewFrame.oCntCell.oMouseEventShape.DblClick
        Local tempCtrl,l_r, l_c, nTemp, nLastRow, nLastCol, nClickRow, nClickCol
        With This.Parent
            Thisform.LockScreen=.T.
            This.Visible=.F.
            .oSelectedRowShape.Visible=.F.
            .oCtrlMouseEvent = Sys(1270)
            This.Visible=.T.
            .oSelectedRowShape.Visible=.T.
            This.ZOrder(0)
            Do While Type("This.Parent.oCtrlMouseEvent")='O' And Type("This.Parent.oCtrlMouseEvent.Parent")='O' And Upper(Left(.oCtrlMouseEvent.Name,5)) # "CTRL_"
                .oCtrlMouseEvent = .oCtrlMouseEvent.Parent
            Enddo
            If Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                .oCtrlMouseEvent.ZOrder(0)
                .Parent.Parent.Parent.oContained.addGadget()
            Endif
            This.ZOrder(0)
            Thisform.LockScreen=.F.
        Endwith
    Endproc

    Procedure _oDummy.KeyPress(nKeyCode, nShiftAltCtrl)
    	Local l_Macro
    	Do Case
            Case nKeyCode = 13  And !Empty(This.Parent.cExecOnEnter) && Enter
                Eval(Strtran(This.Parent.cExecOnEnter, 'this', 'this.Parent',1, Occurs('this',Lower(This.Parent.cExecOnEnter)), 3))
            Case nKeyCode =7 And !Empty(This.Parent.cExecOnCanc) && Canc
                Eval(Strtran(This.Parent.cExecOnCanc, 'this', 'this.Parent',1, Occurs('this',Lower(This.Parent.cExecOnCanc)), 3))
        Endcase
    Endproc
    
    Procedure oViewFrame.MouseDown (nButton, nShift, nXCoord, nYCoord)
        Local l_i, tempCtrl
        LOCAL TestMacro
        With This
	        If nShift=0
	            For l_i = .Parent.aSelected.Count To 1 Step -1
	                m.tempCtrl = .Parent.aSelected.GetKey[l_i]
	                TestMacro= Vartype(.&tempCtrl)
	                If TestMacro=='O'
	            		.&tempCtrl..Select(.F.)
	            	Else && l'elemeno � stato distrutto a causa dello scrolling (non pi� visibile nella griglia)
	            		m.tempCtrl = .Parent.aSelected.Item[l_i]
		                .Parent.aSelected.Remove(l_i)
		                .Parent.RemoveObject(m.tempCtrl.Name)
	            	Endif
	            Endfor
	        Endif
    	Endwith
    Endproc

    *--- Resize
    Proc Resize
        DoDefault()
        With This.oViewFrame
            *--- Se la largezza della finestra � tale da accogliere pi� o meno Gadget degli attuali, ridisegno
            If .Parent.bInit And (.Width<.oCntCell.Width Or .Width>(.oCntCell.Width+.Parent.nHOffset+.Parent.nColMinWidth))
                .Parent._Redraw(.T.) && Non mi interessano i messaggi sugli errori di valutazione delle condizioni di attivazione
            Endif
        Endwith
    Endproc

    *--- Determina il numero di colonne utilizzabili a seconda della dimensione del ViewFrame
    Function GetNumColBySize()
        Return Max(Int(This.oViewFrame.Width/(This.nColMinWidth+This.nHOffset)),1)
    Endfunc

    *--- Seleziona uno o pi� elementi della griglia
    Function Select(cStartItem, cEndItem)
        Local l_oldArea, cCursor, tempCtrl, cSelName, nRow, nCol, nStart, nEnd, l_i, l_step
        LOCAL TestMacro
        With This	        
	        If Vartype(cEndItem)='L' Or cStartItem==cEndItem
	            *--- Verifico se l'elemento di partenza � attualmente presente o � stato distrutto
	            *--- perch� non � pi� visibile sulla griglia a causa dello scrolling
	            TestMacro=PemStatus(.oViewFrame.oCntCell, m.cStartItem, 5) And Vartype(.oViewFrame.oCntCell.&cStartItem)=='O'
	            If TestMacro
                	m.cStartItem = .oViewFrame.oCntCell.&cStartItem
                	cStartItem.Select(.T.)
                Else 
                	*--- altrimenti inserisco nella collection l'oggetto Custom senza passare dalla Select dell'elemento
                	m.nRow = Val(Substr(m.cStartItem,6,At('_',m.cStartItem,2)-6))
            		m.nCol = Val(Substr(m.cStartItem,At('_',m.cStartItem,2)+1,Len(m.cStartItem)))
            		m.tempCtrl="Ctrl_"+Alltrim(Str(m.nRow))+"_"+Alltrim(Str(m.nCol))
		            *--- Controllo se sto riselezionando un oggetto che era stato distrutto dallo scrolling
                    If .aSelected.GetKey(m.tempCtrl)==0
						m.l_oldArea = Select()
	                	m.cCursor = .cCursor
	                	Select(m.cCursor)
			            Go Record (m.nRow-1)*.ColumnCount+m.nCol
						m.cSelName = 'SEL'+Sys(2015)
                    	.AddObject(m.cSelName, 'LibraryGridSelectedCtrl', &cCursor..cKeyGadget, &cCursor..cGadGroup )
                    	.aSelected.Add(.&cSelName, m.tempCtrl)
                    	Select(l_OldArea)
					Endif
                Endif
	        Else
	            m.l_step = Iif(m.cStartItem<m.cEndItem, 1, -1)
	            m.nRow = Val(Substr(m.cStartItem,6,At('_',m.cStartItem,2)-6))
	            m.nCol = Val(Substr(m.cStartItem,At('_',m.cStartItem,2)+1,Len(m.cStartItem)))
	            m.nStart = Int((m.nRow-1)*.ColumnCount)+m.nCol
	            m.nRow = Val(Substr(m.cEndItem,6,At('_',m.cEndItem,2)-6))
	            m.nCol = Val(Substr(m.cEndItem,At('_',m.cEndItem,2)+1,Len(m.cEndItem)))
	            m.nEnd = Int((m.nRow-1)*.ColumnCount)+m.nCol
	            For l_i=m.nStart To m.nEnd Step m.l_step
	                m.nRow = Int((l_i-1)/.ColumnCount)+1
	                m.nCol = Int((l_i-1)%.ColumnCount)+1
	                m.tempCtrl="Ctrl_"+Alltrim(Str(m.nRow))+"_"+Alltrim(Str(m.nCol))
	                *--- Verifico se l'elemento di partenza � attualmente presente o � stato distrutto
	                *--- perch� non � pi� visibile sulla griglia a causa dello scrolling
	                TestMacro=PemStatus(.oViewFrame.oCntCell, m.tempCtrl, 5) And Vartype(.oViewFrame.oCntCell.&tempCtrl)=='O'
	                If TestMacro
	                	m.tempCtrl = .oViewFrame.oCntCell.&tempCtrl
	                	m.tempCtrl.Select(.T.)
	                Else 
	                	*--- altrimenti inserisco nella collection gli oggetti Custom senza passare dalla Select dell'elemento
                        If .aSelected.GetKey(m.tempCtrl)==0 && se non � gi� selezionato
							m.l_oldArea = Select()
		                	m.cCursor = .cCursor
		                	Select(m.cCursor)
				            Go Record (m.nRow-1)*.ColumnCount+m.nCol
							m.cSelName = 'SEL'+Sys(2015)
                        	.AddObject(m.cSelName, 'LibraryGridSelectedCtrl', &cCursor..cKeyGadget, &cCursor..cGadGroup )
                        	.aSelected.Add(.&cSelName, m.tempCtrl)
                        	Select(l_OldArea)
						Endif
	                Endif
	            Endfor
	        Endif
    	Endwith
    Endfunc
	
	Proc ClearSelected()
		Local tempCtrl,l_i
		With This
			For l_i = .aSelected.Count To 1 Step -1
	            m.tempCtrl = .aSelected.Item[l_i]
                .aSelected.Remove(l_i)
                .RemoveObject(m.tempCtrl.Name)
	        Endfor
		Endwith
	Endproc
	
	Proc Destroy()
		This.ClearSelected()
		Use In Select(This.cCursor)
		This.ToolTipBln = .null.
		DoDefault()
	Endproc
Enddefine

*-- Classe per la gestione delle maschere di sfondo nei gadgets
Define Class cp_vfmviewer As Container
    cEvent=''
    cFile=''

    *--- propriet� per gestione navigazione tra maschere
    nIndex=0
    Add Object aVfmList As Collection
    bGoto = .F.  &&Indica se sto aprendo una vfm attraverso l'opzione Goto

    Enabled=.T.
    BackStyle=0
    BorderWidth=0
    Proc Calculate(xValue)
        If Type("m.xValue")='C'
            m.xValue=Trim(m.xValue)
            If Not Empty(m.xValue) And File(ForceExt(m.xValue,'vfm'))
                This.cFile=Forceext(m.xValue,'')
            Endif
        Endif
    Proc Event(cEvent)
        Local l_Event
        l_Event=''
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(l_Event, ' ,', ',')
        If Lower(','+Alltrim(cEvent)+',')$Lower(','+Alltrim(l_Event)+',')
            This.LoadVFM()
        Endif

    *--- carica la vfm specificata nella propriet� cFile
    Proc LoadVFM()
        If !Empty(This.cFile) And cp_FileExist(FullPath(ForceExt(This.cFile,'vfm')))
            This.Visible = .T.
            If Type("This.Cnt")='O'
                This.RemoveObject(This.Cnt.Name)
            Endif
            VM_EXEC(This.cFile, This)
            This.CntAdjust()
        Else
            This.Visible = .F.
        Endif
    Endproc

    Proc Show()
        *-- Richiamata dalla vm_exec
    Endproc

    *-- Riposiziona e ancora la maschera
    Proc CntAdjust()
        With This
            If Type("This.Cnt")='O'
                .Cnt.Anchor=0
                .Cnt.Left = (.Width - .Cnt.Width)*0.5
                .Cnt.Top = (.Height - .Cnt.Height)*0.5
                .Cnt.Anchor=768
            Endif
        Endwith
    Endproc

    *--- funzioni per gestione navigazione tra maschere (richiamate dalla VM_EXEC)
    Procedure AddItem(cFile)
        With This
            If !.bGoto
                If .nIndex<.aVfmList.Count
                    Local l_nIndex
                    l_nIndex = .nIndex + 1
                    If Upper(.aVfmList.Item(m.l_nIndex))<>Upper(m.cFile)
                        For i=.aVfmList.Count To l_nIndex Step -1
                            .aVfmList.Remove(.aVfmList.Count)
                        Endfor
                        .aVfmList.Add(m.cFile)
                        .nIndex = .aVfmList.Count
                    Else
                        .nIndex	= l_nIndex
                    Endif
                Else
                    .aVfmList.Add(m.cFile)
                    .nIndex = .aVfmList.Count
                Endif
            Endif
            .bGoto=.F.
        Endwith
    Endproc
    Procedure Goto(pnIndex)
        Local cVfmFile
        With This
            .bGoto=.T.
            .nIndex = Min(Max(m.pnIndex,1),.aVfmList.Count)
            m.cVfmFile = .aVfmList.Item(.nIndex)
            If m.cVfmFile<>.cFile
                .cFile = m.cVfmFile
                .LoadVFM()
            Endif
        Endwith
    Endproc

    Procedure DrillUp()
        This.Goto(This.nIndex-1)
    Endproc

    Procedure DrillDown()
        This.Goto(This.nIndex+1)
    Endproc

    Procedure Home()
        This.Goto(1)
    Endproc
Enddefine

*-- Triangolo laterale
DEFINE CLASS SideTriangle as Shape
	Visible=.t.
	Dimension aPolyPoints[4,2]
	BorderWidth=1
	BorderStyle=1
	BackColor=Rgb(255,255,255)
	BackStyle=1
	BorderColor=Rgb(255,255,255)
	
	nBase = 37
	nAngle = 62
	
	Width = 21
	Height = 21
	
	Procedure Init()
	EndProc
	
	Procedure Draw(nRotation,nBasePoint,nAnglePoint)
		With this
			.Rotation = m.nRotation
			nBasePoint = Iif(Empty(m.nBasePoint), .nBase , m.nBasePoint)
			nAnglePoint= Iif(Empty(m.nAnglePoint), .nAngle, m.nAnglePoint)
			
            Dimension .aPolyPoints[4,2]
            .aPolyPoints[1,1] = m.nBasePoint&&A
            .aPolyPoints[1,2] = 25

            .aPolyPoints[2,1] = m.nBasePoint&&B
            .aPolyPoints[2,2] = 75

            .aPolyPoints[3,1] = m.nAnglePoint&&C
            .aPolyPoints[3,2] = 50

            .aPolyPoints[4,1] = m.nBasePoint&&D
            .aPolyPoints[4,2] = 25
							
			.Polypoints ="This.aPolyPoints"
		EndWith
	EndProc
	
EndDefine

*-- Contenitore del triangolo laterale
Define Class SideTriangleCnt as Container
	Visible=.t.
	Dimension aPolyPoints[4,2]
	BorderWidth=1
	BorderStyle=1
	BackStyle=0
	BorderColor=Rgb(255,255,255)
	
	nBase = 37
	nAngle = 62
	nRotation = 0
	
	Width = 18
	Height = 18
	
	Add Object oTriangle As SideTriangle With Width=18, Height=18
	
	Procedure Init()
		With This
			.oTriangle.Draw(.nRotation,.nBase,.nAngle)
			.oTriangle.Move(Round((.Width-.oTriangle.Width)/2,0),Round((.Height-.oTriangle.Height)/2,0))
		EndWith
	EndProc
	
	Procedure Click()
		This.Parent.Click()
	EndProc
	
	Procedure oTriangle.Click()
		This.Parent.Click()
	EndProc
	
EndDefine

*-- Contenitore per il mese e l'anno del calendario
Define Class MonthYearCnt As Container
    Height = 30
    Width =  123
    Add Object MonthLbl As Label With Top=0,Left=0,Width=60,Height=24
    Add Object YearLbl As Label With Top=0,Left=63,Width=40,Height=24

Enddefine

*--Header del calendario
Define Class cp_HeaderGadgetCalendar As Container
    dDate=Ctod('  /  /  ')
    Dimension aMonth[12]
    nMon=0
    nYear=0
    nDay=0
    Height = 35
    Width = 235
    BorderWidth = 0
    bJustYear=.F. &&Gestione calendario: .T. annuale, .F. mensile
    bYearMoved=.F. &&Se vengo dalla vista del mese di oggi
	nFontColor = Rgb(255,255,255)

	*-- I bottoni delle frecce sono Shape per colorarli in base al tema
    Add Object BackArrowBtn As SideTriangleCnt With Top=8,Left=10,Width=18,Height=18,Caption="", nRotation=180, MousePointer=15, Anchor=2
    Add Object HeaderDate As MonthYearCnt With Top=7,Left=50,Width=121,Height=30,Anchor=256, BackStyle=0,BorderWidth=0
    Add Object NextArrowBtn As SideTriangleCnt With Top=8,Left=198,Width=18,Height=18,Caption="", nRotation=0, MousePointer=15, Anchor=8

    Proc Init()
        With This
            .aMonth[1] = cp_Translate("Gennaio ")
            .aMonth[2] = cp_Translate("Febbraio ")
            .aMonth[3] = cp_Translate("Marzo ")
            .aMonth[4] = cp_Translate("Aprile ")
            .aMonth[5] = cp_Translate("Maggio ")
            .aMonth[6] = cp_Translate("Giugno ")
            .aMonth[7] = cp_Translate("Luglio ")
            .aMonth[8] = cp_Translate("Agosto ")
            .aMonth[9] = cp_Translate("Settembre ")
            .aMonth[10] = cp_Translate("Ottobre ")
            .aMonth[11] = cp_Translate("Novembre ")
            .aMonth[12] = cp_Translate("Dicembre ")
            .dDate = Date()
            .nMon = Month(.dDate)
            .nYear = Year(.dDate)

            .HeaderDate.MonthLbl.Caption = .aMonth[.nMon]
            .HeaderDate.MonthLbl.FontBold = .T.
            .HeaderDate.MonthLbl.MousePointer = 15
            .HeaderDate.YearLbl.Caption = Alltrim(Str(.nYear))
            .HeaderDate.YearLbl.FontBold = .T.
            .HeaderDate.YearLbl.MousePointer = 15
            .BackArrowBtn.oTriangle.MousePointer = 15
            .BackArrowBtn.oTriangle.ToolTipText="Precedente"
            .NextArrowBtn.Left = .Width-.NextArrowBtn.Width-10
            .NextArrowBtn.oTriangle.Left = .NextArrowBtn.oTriangle.Left+1
            .NextArrowBtn.oTriangle.MousePointer = 15
            .NextArrowBtn.oTriangle.ToolTipText="Successivo"
            .AdjustDate()
        Endwith
    Endproc

    Proc Calculate(xValue)
    Endproc

    Proc Event(cEvent)
    EndProc
    
    Procedure nFontColor_Assign(nValue)
    	This.nFontColor = m.nValue
    	This.SetColor()
    EndProc

    Proc AdjustDate()
    	Local nYearWidth
    	nYearWidth = cp_LabelWidth2(This.HeaderDate.YearLbl.Caption,This.HeaderDate.YearLbl.FontName,This.HeaderDate.YearLbl.FontSize, "B")
    	This.HeaderDate.YearLbl.Width = m.nYearWidth
        If Not This.bJustYear
        	Local nMonthWidth
        	nMonthWidth = cp_LabelWidth2(This.HeaderDate.MonthLbl.Caption,This.HeaderDate.MonthLbl.FontName,This.HeaderDate.MonthLbl.FontSize, "B")
        	This.HeaderDate.MonthLbl.Width = m.nMonthWidth
            This.HeaderDate.Left = Int((This.Width - m.nMonthWidth - m.nYearWidth - 2)/2)
            This.HeaderDate.YearLbl.Left = This.HeaderDate.MonthLbl.Left + m.nMonthWidth -1
            This.HeaderDate.MonthLbl.ToolTipText="Vista mensile: selezionare le frecce per cambiare mese"
            This.HeaderDate.YearLbl.ToolTipText=This.HeaderDate.MonthLbl.ToolTipText
        Else
            This.HeaderDate.Left = Int((This.Width - m.nYearWidth)/2)
            This.HeaderDate.YearLbl.Left = 0
            This.HeaderDate.YearLbl.ToolTipText="Vista annuale: selezionare le frecce per cambiare anno"
        EndIf
        This.SetColor()
    EndProc
    
    Proc SetColor()
    	With This
		    .HeaderDate.YearLbl.ForeColor = .nFontColor
		    .HeaderDate.MonthLbl.ForeColor = .nFontColor
		    .BackArrowBtn.BorderColor = .nFontColor
		    .BackArrowBtn.oTriangle.BackColor = .nFontColor
		    .BackArrowBtn.oTriangle.BorderColor = .nFontColor
		    .NextArrowBtn.BorderColor = .nFontColor
		    .NextArrowBtn.oTriangle.BackColor = .nFontColor
		    .NextArrowBtn.oTriangle.BorderColor = .nFontColor
        EndWith
    EndProc

    Proc BackArrowBtn.Click(nDaySelected)
        *-- Parametro utilizzato dalla griglia
        With This.Parent
            If Not .bJustYear
                .nMon=Iif(.nMon = 1,12,.nMon-1)
                .HeaderDate.MonthLbl.Caption = .aMonth[.nMon]
            Endif
            .nYear=Iif(.nMon = 12 Or .bJustYear,.nYear-1,.nYear)
            .HeaderDate.YearLbl.Caption = Alltrim(Str(.nYear))
            .AdjustDate()
        Endwith
    EndProc
    
    Proc BackArrowBtn.oTriangle.Click(nDaySelected)
        *-- Parametro utilizzato dalla griglia
		This.Parent.Click(nDaySelected)
	Endproc

    Proc NextArrowBtn.Click(nDaySelected)
        *-- Parametro utilizzato dalla griglia
        With This.Parent
            If Not .bJustYear
                .nMon=Iif(.nMon = 12,1,.nMon+1)
                .HeaderDate.MonthLbl.Caption = .aMonth[.nMon]
            Endif
            .nYear=Iif(.nMon = 1 Or .bJustYear,.nYear+1,.nYear)
            .HeaderDate.YearLbl.Caption = Alltrim(Str(.nYear))
            .AdjustDate()
        Endwith
    Endproc

    Proc NextArrowBtn.oTriangle.Click(nDaySelected)
        *-- Parametro utilizzato dalla griglia
		This.Parent.Click(nDaySelected)
	Endproc

    Proc ArrowBtn(nDaySelected,nValue)
    Endproc

    Proc HeaderDate.YearLbl.Click()
        This.Parent.Click()
    Endproc

    Proc HeaderDate.MonthLbl.Click()
        This.Parent.Click()
    Endproc

    Proc HeaderDate.Click()
        With This
            .Parent.bJustYear=.T.
            .MonthLbl.Visible=.F.
            .Parent.AdjustDate()
        Endwith
    Endproc
Enddefine

*-- Celle del calendario
Define Class DayDataCnt As Container
    BorderWidth=0
    BackStyle=0
    bSelected=.F.
    bToday=.F.
    bTodayMonth=.F.
    nInMonth=0 && 0 mese precedente, 1 mese corrente, 2 mese successivo
    dDateCell=Ctod('  /  /  ')
    nActivityColor=0  &&Colore degli Shape
    nRelMonth=0
    cActivity=""
    nMonthLabelSize = 63  &&Dimensione della cella nella vista dei mesi

    Add Object sDayShape As Shape With BackColor=Rgb(255,255,255),Visible=.F. ,Curvature=0 ,BorderWidth=2  &&Shape di sfondo selezione
    Add Object cNumDay As Label With BackStyle=0,Top=0,Left=0,Alignment=2, ForeColor=Rgb(255,255,255), FontName="Open Sans"
    Add Object sPartDayShape As Shape With BackColor=Rgb(255,255,255),Visible=.F. ,Curvature=0 ,BorderWidth=2  &&Shape di attivit� giornaliera
    Add Object sAllDayShape As Shape With BackColor=Rgb(255,255,255),Visible=.F. ,Curvature=0 ,BorderWidth=2  &&Shape di attivit� di giornata piena

    Procedure Resize()
        If Not This.Parent.Parent.Parent.bYearView
            This.cNumDay.Move(0, Int((This.Height-This.cNumDay.Height)/2), This.Width)
            This.sPartDayShape.Move(This.Width-7,3,4,4)
            This.sAllDayShape.Move(This.Width-5,3,2,This.Height-3)
        Else
            This.Move(This.Left,This.Top,This.nMonthLabelSize,This.nMonthLabelSize)
            This.cNumDay.Move(0, 0, This.Width,This.Height)
            This.sPartDayShape.Move(This.Width-12,3,9,9)
        Endif
        This.sDayShape.Move(0,1,This.Width-1,This.Height-1)
    Endproc

Enddefine

*-- Griglia del calendario
Define Class cp_CalendarVirtualGrid As VirtualGrid
    Dimension aDayOfWeek[7]
    cEvent=""
    nFirstDay=Ctod('  /  /  ') &&Data per la costruzione corretta del mese
    dToday=Ctod('  /  /  ')  &&Data di oggi
    nCurrMonth=1  &&Numero del mese corrente
    nCurrYear=0  &&Numero dell'anno corrente
    dDateSelected=Ctod('  /  /  ')  &&Data selezionata
    cQuery=''  &&Query per le attivit�
    cQueryCursor=''  &&Nome del cursore temporaneo
    bYearView=.F.   && .F. vista del mese, .T. vista dell'anno
    ScrollBars = 0
    oTimer=.Null.
    nFontColor = Rgb(255,255,255)
    nAlphaFontColor = -1
    
    Proc Init()
        With This
            .RowHeight = 24
            .nColMinWidth = 24
            .nRowMinHeight = 24
            .RowCount = 7
            .ColumnCount = 7
            .HotTracking = 2
            .oViewFrame.oCntCell.oSelectedRowShape.Visible = .F.
            .oViewFrame.oCntCell.oSelectedRowShape.Curvature = 0
            .dToday = Date()
            .nFirstDay = Date(Year(.dToday),Month(.dToday),1) - Dow(Date(Year(.dToday),Month(.dToday),1),2) + 1
            .nFirstDay = Iif(Day(.nFirstDay) = 1,.nFirstDay-7,.nFirstDay)
            .nCurrMonth = Month(.dToday)
            .nCurrYear = Year(.dToday)

            .aDayOfWeek[1] = cp_Translate("LU")
            .aDayOfWeek[2] = cp_Translate("MA")
            .aDayOfWeek[3] = cp_Translate("ME")
            .aDayOfWeek[4] = cp_Translate("GI")
            .aDayOfWeek[5] = cp_Translate("VE")
            .aDayOfWeek[6] = cp_Translate("SA")
            .aDayOfWeek[7] = cp_Translate("DO")
        Endwith
    Endproc

    Proc SetCell(oCtrl, nRow, nCol)
        Local l_SearchInCursor, l_dDate, l_ActivityColor, l_cToolTip, l_Cursor, l_SubCursor, nRecCount
        l_dDate = This.nFirstDay+(nCol-1)+(nRow-2)*7
        If Not This.bYearView
            *-- Costruzione calendario del mese
            If nRow=1
                *-- Giorni della settimana
                oCtrl.cNumDay.Caption=This.aDayOfWeek[nCol]
                oCtrl.cNumDay.FontBold=.T.
                oCtrl.cNumDay.FontSize=10
                oCtrl.cNumDay.ForeColor = This.nFontColor
            Else
                oCtrl.cNumDay.Caption=Transform(Day(l_dDate))
                oCtrl.cNumDay.ForeColor=Iif(Month(l_dDate)=This.nCurrMonth ,This.nFontColor , This.nAlphaFontColor)
                oCtrl.cNumDay.FontSize=10
                oCtrl.dDateCell=l_dDate
                If l_dDate = This.dToday
                    oCtrl.sDayShape.Visible=.T.
                    oCtrl.sDayShape.BorderColor = This.nFontColor
                    oCtrl.sDayShape.BackColor = This.nFontColor
                    oCtrl.cNumDay.ForeColor = This.Parent.Color
                    oCtrl.cNumDay.FontBold=.T.
                    oCtrl.bToday = .T.
                    oCtrl.bSelected = .T.
                    This.dDateSelected = oCtrl.dDateCell
                Endif
                Do Case
                    Case (Month(l_dDate) < This.nCurrMonth And Year(l_dDate) = This.nCurrYear) Or Year(l_dDate) < This.nCurrYear
                        oCtrl.nInMonth=0  &&Mese precedente
                    Case Month(l_dDate) = This.nCurrMonth
                        oCtrl.nInMonth=1  &&Mese corrente
                    Otherwise
                        oCtrl.nInMonth=2  &&Mese successivo
                Endcase
                l_SearchInCursor=Sys(2015)
                If Not Empty(This.cQueryCursor) And Used(This.cQueryCursor)
                	l_Cursor = This.cQueryCursor
                	Select (l_Cursor)
                	If Type("&l_Cursor..DateStop")<>"U"
                		Select * From (l_Cursor) Where (CalDate<=oCtrl.dDateCell And DateStop=>oCtrl.dDateCell) or (Empty(Nvl(DateStop,'')) And CalDate=oCtrl.dDateCell) Into Cursor (l_SearchInCursor)
                	Else
                    	Select * From (l_Cursor) Where CalDate=oCtrl.dDateCell Into Cursor (l_SearchInCursor)
                    EndIf
                    If Reccount()>0
	                    l_cToolTip = Iif(Type("ToolTip")='C',ToolTip,'')
	                    l_SubCursor = Sys(2015)
	                    If Type("&l_Cursor..Status")<>"U"
	                    	Select Count(*) as Conta, Status From (l_SearchInCursor) Group by Status Into Cursor (l_SubCursor)
	                    	Scan
		                    	l_cToolTip = Strtran(Alltrim(Nvl(l_cToolTip,"")),"%"+Alltrim(Transform(Status)),Alltrim(Transform(Conta)))
	                    	EndScan
	                    	Do While At("%",l_cToolTip)<>0
	                    		*-- Sostituisco i valori non presenti con 0
	                    		l_SubToolTip = Substr(l_cToolTip,At("%",l_cToolTip),Len(l_cToolTip))
	                    		l_cToolTip = Strtran(l_cToolTip,Substr(l_cToolTip,At("%",l_cToolTip),At(" ",l_SubToolTip)-1),"0")
	                    	EndDo
	                    Else
	                    	Select Count(*) as Conta From (l_SearchInCursor) Into Cursor (l_SubCursor)
	                    	l_cToolTip = Strtran(Alltrim(Nvl(l_cToolTip,"")),"%T",Transform(Conta))
	                    EndIf
	                    Use In Select(l_SubCursor)
	                    Select * From (l_SearchInCursor) Into Cursor (l_SubCursor)
	                    Iif(Type("CalDate")="T",Ttod(CalDate),CalDate)
	                    *-- Se nel giorno � presente almeno una attivit� giornaliera, mostro lo sPartDayShape 
	                    Count For Iif(Type("CalDate")="T",Ttod(CalDate),CalDate)=oCtrl.dDateCell Or (!Empty(Nvl(DateStop,'')) And Iif(Type("DateStop")="T",Ttod(DateStop),DateStop)=oCtrl.dDateCell) To nRecCount
                    	oCtrl.sPartDayShape.Visible = m.nRecCount>0
                        *-- Se il giorno � contenuto tra l'inizio e la fine di almeno una attivit�, mostro lo sPartDayShape 
                        Count For Iif(Type("CalDate")="T",Ttod(CalDate),CalDate)<oCtrl.dDateCell And !Empty(Nvl(DateStop,'')) And Iif(Type("DateStop")="T",Ttod(DateStop),DateStop)>oCtrl.dDateCell To nRecCount
                    	oCtrl.sAllDayShape.Visible = m.nRecCount>0
                        Use In Select(l_SubCursor)
                        Select (l_SearchInCursor)
                        *-- Attivit� del giorno
                        oCtrl.cActivity=Strtran(Alltrim(Nvl(l_cToolTip,"")),"chr(13)",Chr(10),-1,-1,1)
                        If Type("ActvColor")='N'
                        	Local l_nDifferent
                        	l_ActivityColor = ActvColor
                        	Count For l_ActivityColor = ActvColor to l_nDifferent
                        	l_ActivityColor = Iif(Reccount() = l_nDifferent And l_ActivityColor>-1 ,l_ActivityColor , This.nFontColor)
                        Else
                        	l_ActivityColor = This.nFontColor
                        EndIf
                        oCtrl.nActivityColor = l_ActivityColor 
                        oCtrl.sPartDayShape.BackColor =Iif(oCtrl.bToday And (oCtrl.nActivityColor=-1 Or oCtrl.nActivityColor=oCtrl.sDayShape.BackColor),This.Parent.Color,oCtrl.nActivityColor)
                        oCtrl.sPartDayShape.BorderColor=oCtrl.sPartDayShape.BackColor
                        oCtrl.sAllDayShape.BackColor=oCtrl.sPartDayShape.BackColor
                        oCtrl.sAllDayShape.BorderColor=oCtrl.sAllDayShape.BackColor
                    Endif
                    Use In Select(l_SearchInCursor)
                Endif
            Endif
        Else
            *-- Costruzione calendario dell'anno
            oCtrl.cNumDay.Caption=Alltrim(Str(((nRow-1)*4)+nCol))
            oCtrl.nRelMonth=((nRow-1)*4)+nCol
            oCtrl.cNumDay.FontBold=.T.
            oCtrl.cNumDay.FontSize=32
            oCtrl.cNumDay.ForeColor=This.nFontColor
            If oCtrl.nRelMonth=Month(This.dToday) And This.nCurrYear=Year(This.dToday)
                oCtrl.sDayShape.Visible=.T.
                oCtrl.sDayShape.BorderColor=This.nFontColor
                oCtrl.sDayShape.BackColor=This.nFontColor
                oCtrl.cNumDay.ForeColor=This.Parent.Color
                oCtrl.bTodayMonth=.T.
            Endif
            l_SearchInCursor=Sys(2015)
            If Not Empty(This.cQueryCursor) And Used(This.cQueryCursor)
                l_olderr=On('error')
                bErr=.F.
                On Error bErr=.T.
                Select ActvColor From (This.cQueryCursor) Where Month(CalDate)=oCtrl.nRelMonth And Year(CalDate)=This.nCurrYear Into Cursor (l_SearchInCursor)
                If bErr
                    bErr = .F.
                	l_ActivityColor = This.nFontColor
                Else
                	Local l_nDifferent
                	l_ActivityColor = ActvColor
                	Count For l_ActivityColor = ActvColor to l_nDifferent
                	l_ActivityColor = Iif(Reccount() = l_nDifferent And l_ActivityColor>-1 ,l_ActivityColor , This.nFontColor)
                EndIf
                Use In Select(l_SearchInCursor)
                    Select Count(CalDate) As Conta From (This.cQueryCursor) Where Month(CalDate)=oCtrl.nRelMonth And Year(CalDate)=This.nCurrYear Into Cursor (l_SearchInCursor)
                If !bErr And Conta>0
                    oCtrl.sPartDayShape.Visible=.T.
                    *-- Attivit� del mese
                    oCtrl.cActivity=Alltrim(Str(Conta))
                    Select (l_SearchInCursor)
                    oCtrl.nActivityColor = l_ActivityColor
                    oCtrl.sPartDayShape.BackColor =Iif(oCtrl.bTodayMonth And (oCtrl.nActivityColor=-1 Or oCtrl.nActivityColor=oCtrl.sDayShape.BackColor),This.Parent.Color,oCtrl.nActivityColor)
                    oCtrl.sPartDayShape.BorderColor=oCtrl.sPartDayShape.BackColor
                Endif
                Use In Select(l_SearchInCursor)
                On Error &l_olderr
            Endif
        Endif
        oCtrl.Visible=.T.
    Endproc

    Procedure Event(cEvent)
    Endproc

    Proc _Clean()
        This.oViewFrame.oCntCell._Clean()
    Endproc

    Function GetCellClass(nRow, nCol)
        Return "DayDataCnt"
    Endfunc

    Proc oViewFrame.oCntCell.oMouseEventShape.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        Local oOldCtrl, l_Date, l_cToolTip
        *-- Tengo conto dell'oggetto corrente
        oOldCtrl = This.Parent.oCtrlMouseEvent
        DoDefault()
        If Type("This.Parent.oCtrlMouseEvent")='O'
            Do While Type("This.Parent.oCtrlMouseEvent.Parent")='O' And Upper(Left(This.Parent.oCtrlMouseEvent.Name,5)) # "CTRL_"
                This.Parent.oCtrlMouseEvent = This.Parent.oCtrlMouseEvent.Parent
            Enddo
            If Upper(Left(This.Parent.oCtrlMouseEvent.Name,6)) == "CTRL_1" And Not This.Parent.Parent.Parent.bYearView
                This.Parent.oSelectedRowShape.Visible=.F.
                If Type("ThisForm.oBalloon")='O' And Not Isnull(Thisform.oBalloon) And Thisform.oBalloon.ctlVisible
                    Thisform.oBalloon.ctlhide(2)
                    Thisform.oBalloon.ctlVisible = .F.
                Endif
            Else
                If Upper(Left(This.Parent.oCtrlMouseEvent.Name,5)) == "CTRL_"
                    This.Parent.oSelectedRowShape.Move(This.Parent.oCtrlMouseEvent.Left,This.Parent.oCtrlMouseEvent.Top+1,This.Parent.oCtrlMouseEvent.Width-1,This.Parent.oCtrlMouseEvent.Height-1)
                    If Type("m.oOldCtrl")="O" And m.oOldCtrl#This.Parent.oCtrlMouseEvent
                        *-- Se siamo su un oggetto diverso da quello precedente mostriamo di nuovo il ToolTip (in caso contrario si riesegue da solo e "lampeggia")
                        If Not This.Parent.Parent.Parent.bYearView
                            l_Date =  This.Parent.oCtrlMouseEvent.dDateCell
                            l_cToolTip = This.Parent.oCtrlMouseEvent.cActivity
                        Else
                            l_Date = This.Parent.Parent.Parent.Parent.CalHeader.aMonth[This.Parent.oCtrlMouseEvent.nRelMonth]+Alltrim(Str(This.Parent.Parent.Parent.nCurrYear))
                            l_cToolTip = Iif(Empty(This.Parent.oCtrlMouseEvent.cActivity), "", This.Parent.oCtrlMouseEvent.cActivity +" attivit� presente\i")
                        Endif
                        *-- Il vecchio balloon viene distrutto
                        If Type("ThisForm.oBalloon")='O' And Thisform.oBalloon.ctlVisible
                            Thisform.oBalloon.ctlhide(2)
                            Thisform.oBalloon.ctlVisible = .F.
                        Endif
                        If Type("This.Parent.Parent.Parent.oTimer")="O" And Not Isnull(This.Parent.Parent.Parent.oTimer)
                            This.Parent.Parent.Parent.oTimer.Enabled = .F.
                        Endif
                        This.Parent.Parent.Parent.oTimer = Createobject("CalBalloonTimer", Thisform, l_Date, l_cToolTip, This.Parent.oCtrlMouseEvent)
                    Endif
                Else
                    This.Parent.oSelectedRowShape.Visible=.F.
                    *-- Commentato per lasciare entrare il cursore del mouse nel balloon
                    *!*			                   IF Type("ThisForm.oBalloon")='O' AND NOT ISNULL(ThisForm.oBalloon) AND ThisForm.oBalloon.ctlVisible
                    *!*			                       		ThisForm.oBalloon.ctlhide(2)
                    *!*			                       		ThisForm.oBalloon.ctlVisible = .F.
                    *!*			                   ENDIF
                Endif
            Endif
        Else
        Endif
    Endproc

    Procedure oViewFrame.oCntCell.oMouseEventShape.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This.Parent
            .Parent.Parent.oTimer = .Null.
            If .Parent.Parent.HotTracking<>0
                Thisform.LockScreen=.T.
                .oSelectedRowShape.Visible=.F.
                This.ZOrder(0)
                This.Visible = .T.
                Thisform.LockScreen = .F.
            Endif
        Endwith
    Endproc

    Proc oViewFrame.oCntCell.oMouseEventShape.DblClick()
        *-- Lancio evento al doppio click
        With This.Parent
            *Thisform.LockScreen=.T.
            If Type("This.Parent.oCtrlMouseEvent")='O' And Not Isnull(This.Parent.oCtrlMouseEvent) And Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                .oCtrlMouseEvent.ZOrder(0)
                .Parent.Parent.Parent.Parent.oContained.NotifyEvent("w_"+Upper(.Parent.Parent.Parent.Name)+" DoubleClickSelect")
                If Type("ThisForm.oBalloon")='O' And Thisform.oBalloon.ctlVisible
                    Thisform.oBalloon.ctlhide(2)
                    Thisform.oBalloon.ctlVisible = .F.
                Endif
            Endif
            This.ZOrder(0)
            *Thisform.LockScreen=.F.
        Endwith

    Proc oViewFrame.oCntCell.oMouseEventShape.Click()
        With This.Parent
            Thisform.LockScreen=.T.
            This.Visible=.F.
            .oSelectedRowShape.Visible=.F.
            This.Visible=.T.
            .oSelectedRowShape.Visible=.T.
            If Type("This.Parent.oCtrlMouseEvent")='O' And Not Isnull(This.Parent.oCtrlMouseEvent) And Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                .oCtrlMouseEvent.ZOrder(0)
                If Not .Parent.Parent.bYearView
                    If Not Upper(Left(.oCtrlMouseEvent.Name,6)) == "CTRL_1"
                        *-- Selezione data
                        .Parent.Parent.SelectDate(.oCtrlMouseEvent)
                        .Parent.Parent.Parent.Parent.oContained.NotifyEvent("w_"+Upper(.Parent.Parent.Parent.Name)+" SingleSelectDay")
                    Endif
                Else
                    *-- Selezione del mese, riporto la gestione del calendario mensile
                    .Parent.Parent.FromYearToMonth(.oCtrlMouseEvent)
                    .Parent.Parent.Parent.Parent.oContained.NotifyEvent("w_"+Upper(.Parent.Parent.Parent.Name)+" SingleSelectMonth")
                Endif
            Endif
            This.ZOrder(0)
            Thisform.LockScreen=.F.
        Endwith
    Endproc

    Proc SelectDate(oCtrl,nDayToSelect)
    LOCAL TestMacro
        With This
            l_nRow=2
            If Type("oCtrl")='O'
                *-- Selezione semplice, viene deselezionato il vecchio contenitore e selezionato quello nuovo
                Do While l_nRow <= .RowCount
                    l_nCol=1
                    Do While l_nCol <= .ColumnCount
                        l_cControl="CTRL_"+Alltrim(Str(l_nRow))+"_"+Alltrim(Str(l_nCol))
                        TestMacro=Vartype(oCtrl.Parent.&l_cControl)<>"U" And !Isnull(oCtrl.Parent.&l_cControl)
                        If TestMacro
                            l_oCtrlObj = oCtrl.Parent.&l_cControl
                            If l_oCtrlObj.bSelected
                                l_oCtrlObj.bSelected = .F.
                                l_oCtrlObj.sDayShape.Visible = .F.
                                *-- Il colore dello shape � valorizzato nella SetCell
                                l_oCtrlObj.sPartDayShape.BorderColor = l_oCtrlObj.nActivityColor
                                l_oCtrlObj.sPartDayShape.BackColor = l_oCtrlObj.nActivityColor
                                l_oCtrlObj.sAllDayShape.BackColor = l_oCtrlObj.nActivityColor
                                l_oCtrlObj.sAllDayShape.BorderColor = l_oCtrlObj.nActivityColor
                                l_oCtrlObj.cNumDay.FontBold = .F.
                                l_oCtrlObj.cNumDay.ForeColor = .nFontColor
                            Endif
                            *-- Nel giorno di oggi rimane il bordo
                            If l_oCtrlObj.bToday
                                l_oCtrlObj.bSelected = .F.
                                l_oCtrlObj.sDayShape.Visible = .T.
                                l_oCtrlObj.sDayShape.BackColor = .Parent.Color
                            Endif
                        Endif
                        l_nCol=l_nCol+1
                    Enddo
                    l_nRow=l_nRow+1
                Enddo
                oCtrl.bSelected = .T.
                Do Case
                    Case oCtrl.nInMonth=0 &&Selezionato giorno mese precedente
                        .Parent.CalHeader.BackArrowBtn.Click(Int(Val(oCtrl.cNumDay.Caption)))
                    Case oCtrl.nInMonth=2 &&Selezionato giorno mese successivo
                        .Parent.CalHeader.NextArrowBtn.Click(Int(Val(oCtrl.cNumDay.Caption)))
                    Otherwise
                        oCtrl.sDayShape.Visible=.T.
                        oCtrl.sDayShape.BorderColor = .nFontColor
                        oCtrl.sDayShape.BackColor = .nFontColor
                        oCtrl.sPartDayShape.BorderColor = Iif(oCtrl.nActivityColor=-1 Or oCtrl.nActivityColor=oCtrl.sDayShape.BackColor,.Parent.Color,oCtrl.nActivityColor)
                        oCtrl.sPartDayShape.BackColor = Iif(oCtrl.nActivityColor=-1 Or oCtrl.nActivityColor=oCtrl.sDayShape.BackColor,.Parent.Color,oCtrl.nActivityColor)
                        oCtrl.sAllDayShape.BackColor = Iif(oCtrl.nActivityColor=-1 Or oCtrl.nActivityColor=oCtrl.sDayShape.BackColor,.Parent.Color,oCtrl.nActivityColor)
                        oCtrl.sAllDayShape.BorderColor = Iif(oCtrl.nActivityColor=-1 Or oCtrl.nActivityColor=oCtrl.sDayShape.BackColor,.Parent.Color,oCtrl.nActivityColor)
                        oCtrl.cNumDay.ForeColor = .Parent.Color
                        oCtrl.cNumDay.FontBold = .T.
                        This.dDateSelected = oCtrl.dDateCell
                Endcase
            Else
                *-- Se si viene da un cambio di data (click su giorno fuori mese corrente) viene selezionato il campo specifico
                l_nFounded=0
                Do While l_nRow <= .RowCount And l_nFounded<3
                    l_nCol=1
                    Do While l_nCol <= .ColumnCount And l_nFounded<3
                        l_cControl="CTRL_"+Alltrim(Str(l_nRow))+"_"+Alltrim(Str(l_nCol))
                        TestMacro=Vartype(This.oViewFrame.oCntCell.&l_cControl)<>"U" And !Isnull(This.oViewFrame.oCntCell.&l_cControl)
                        If TestMacro
                            l_oCtrlObj = .oViewFrame.oCntCell.&l_cControl
                            *-- Se si viene da sTodayShape � possibile che ci sia un giorno gi� selezionato nel mese, lo deseleziono
                            If l_oCtrlObj.bSelected
                                l_oCtrlObj.bSelected = .F.
                                l_oCtrlObj.sDayShape.Visible = .F.
                                *-- Il colore dello shape � valorizzato nella SetCell
                                l_oCtrlObj.sPartDayShape.BorderColor = l_oCtrlObj.nActivityColor
                                l_oCtrlObj.sPartDayShape.BackColor = l_oCtrlObj.nActivityColor
                                l_oCtrlObj.sAllDayShape.BackColor = l_oCtrlObj.nActivityColor
                                l_oCtrlObj.sAllDayShape.BorderColor = l_oCtrlObj.nActivityColor
                                l_oCtrlObj.cNumDay.FontBold = .F.
                                l_oCtrlObj.cNumDay.ForeColor = .nFontColor
                                l_nFounded = l_nFounded+1
                            Endif
                            *-- Ricerca fatta solo per il mese corrente
                            l_bSearch = l_oCtrlObj.nInMonth=1 And l_oCtrlObj.cNumDay.Caption==Alltrim(Str(nDayToSelect))
                            If l_bSearch
                                l_oCtrlObj.bSelected = .T.
                                l_oCtrlObj.sDayShape.Visible = .T.
                                l_oCtrlObj.sDayShape.BorderColor=.nFontColor
                                l_oCtrlObj.sDayShape.BackColor=.nFontColor
                                l_oCtrlObj.sPartDayShape.BorderColor=Iif(l_oCtrlObj.nActivityColor=-1 Or l_oCtrlObj.nActivityColor=l_oCtrlObj.sDayShape.BackColor,.Parent.Color,l_oCtrlObj.nActivityColor)
                                l_oCtrlObj.sPartDayShape.BackColor=Iif(l_oCtrlObj.nActivityColor=-1 Or l_oCtrlObj.nActivityColor=l_oCtrlObj.sDayShape.BackColor,.Parent.Color,l_oCtrlObj.nActivityColor)
                                l_oCtrlObj.sAllDayShape.BackColor = Iif(l_oCtrlObj.nActivityColor=-1 Or l_oCtrlObj.nActivityColor=l_oCtrlObj.sDayShape.BackColor,.Parent.Color,l_oCtrlObj.nActivityColor)
                                l_oCtrlObj.sAllDayShape.BorderColor = Iif(l_oCtrlObj.nActivityColor=-1 Or l_oCtrlObj.nActivityColor=l_oCtrlObj.sDayShape.BackColor,.Parent.Color,l_oCtrlObj.nActivityColor)
                                l_oCtrlObj.cNumDay.ForeColor= .Parent.Color
                                l_oCtrlObj.cNumDay.FontBold = .T.
                                This.dDateSelected = l_oCtrlObj.dDateCell
                                l_nFounded = l_nFounded+1
                            Endif
                            If l_oCtrlObj.bToday
                                l_oCtrlObj.sDayShape.Visible = .T.
                                l_oCtrlObj.sDayShape.BackColor=Iif(Not l_bSearch,.Parent.Color,.nFontColor)
                                l_oCtrlObj.cNumDay.ForeColor=Iif(l_bSearch,.Parent.Color, .nFontColor)
                                l_oCtrlObj.cNumDay.FontBold = l_bSearch
                                l_nFounded = l_nFounded+1
                            Endif
                        Endif
                        l_nCol=l_nCol+1
                    Enddo
                    l_nRow=l_nRow+1
                Enddo
            Endif
        Endwith
    Endproc

    Proc CleanGrid(bResize)
        If !m.bResize And Not Empty(This.cQuery) 
        	If cp_fileexist(This.cQuery)
	            If Used(This.cQueryCursor)
	                Use In Select(This.cQueryCursor)
	            Endif
	            This.cQueryCursor="Calendar"+Sys(2015)
	            vq_exec(This.cQuery,Thisform,This.cQueryCursor)
	            Update (This.cQueryCursor) Set CalDate=Evl(cp_ToDate(CalDate),Ctod('  /  /  '))
	            ThisForm.NotifyEvent("w_"+Lower(This.Parent.Name)+" AfterQuery")
	            Go Top
        	Else
        		If PemStatus(ThisForm, 'cAlertMsg', 5)
    				ThisForm.cAlertMsg = ah_MsgFormat('Query non passata o inesistente:%0"%1"', Alltrim(This.cQuery))
        		Else
        		cp_msg(cp_MsgFormat(MSG_CANNOT_FIND_QUERY__,This.cQuery),.F.)
        		EndIf
        		This.cQuery=''
        	EndIf
        Endif
        This._Clean()
    Endproc

    Proc FromYearToMonth(oCtrl, bTodayShape)
        *-- Chiamato dal click sul mese (oCtrl object),dal TodayShape o dalla selezione dell'Header(Torna a mese precedente)
        With This
            Local nTop
            nTop = .Top
            *-- Transizione verso il basso, sposto la griglia fuori vista
            cp_SingleTransition(This, "Top", .Height+.Top, "N", 300, "Linear", .T.)

            .bYearView = .F.
            *-- Se si viene dall'Header prendo il mese precedentemente visualizzato, altrimenti visualizzo il mese di oggi
            .dDateSelected = Iif(m.bTodayShape,.dToday,.dDateSelected)
            .Parent.dCalDate = Evl(.Parent.dCalDate, .dDateSelected)
            .Parent.dCalDate=Iif(Type("oCtrl")='O', Date(.Parent.CalHeader.nYear,oCtrl.nRelMonth,Day(.Parent.dCalDate)), .dDateSelected)
            .nCurrMonth=Month(.Parent.dCalDate)
            .nCurrYear=Year(.Parent.dCalDate)
            *-- Data per la ricostruzione del calendario
            .nFirstDay=Date(Year(.Parent.dCalDate),Month(.Parent.dCalDate),1) - Dow(Date(Year(.Parent.dCalDate),Month(.Parent.dCalDate),1),2) + 1
            *--- Tolgo 7 gg per centrare il calendario e lasciare sempre una riga con il mese precedente e quello successivo
            .nFirstDay=Iif(Day(.nFirstDay) = 1,.nFirstDay-7,.nFirstDay)
            *-- Ridimensionamento della testata
            .Parent.CalHeader.bJustYear=.F.
            .Parent.CalHeader.HeaderDate.MonthLbl.Visible = .T.
            .Parent.CalHeader.HeaderDate.MonthLbl.Caption = .Parent.CalHeader.aMonth[.nCurrMonth]
            .Parent.CalHeader.HeaderDate.MonthLbl.MousePointer=15
            .Parent.CalHeader.nMon=.nCurrMonth
            .Parent.CalHeader.nYear=.nCurrYear
            .Parent.CalHeader.HeaderDate.YearLbl.Caption = Alltrim(Str(Year(.Parent.dCalDate)))
            .Parent.CalHeader.HeaderDate.YearLbl.MousePointer=15
            .Parent.CalHeader.AdjustDate()
            .RowCount = 7
            .ColumnCount = 7
            .nColMinWidth = 24
            .nRowMinHeight = 24
            .Resize()
            .SelectDate(.F.,Iif(Type("oCtrl")='O',1, Day(.Parent.dCalDate)))
            *-- Transizione verso l'alto, sposto la griglia in vista
            cp_SingleTransition(This, "Top", m.nTop, "N", 300, "Linear", .T.)

        Endwith
    Endproc

    Proc Destroy()
        Use In Select(This.cQueryCursor)
        DoDefault()
    Endproc

Enddefine

*--Timer per il tooltip balloon
Define Class CalBalloonTimer As Timer
    Enabled=.F.
    cBalloon=""
    nSec=1.2
    oForm = .Null.
    oCtrl = .Null.
    cFormName = ''

    Proc Init(pParent, cDate, cString, oCtrl)
        *-- Vengono passati il riferimento alla Form, la stringa da visualizzare e il controllo su cui fare il ToolTip
        Local l_OldSetDate
        This.oForm= m.pParent
        This.cFormName = m.pParent.Name
        This.oCtrl = m.oCtrl
        If Type("cDate")="D"
            l_OldSetDate = Set("Date")
            Set Date Long
            *-- Data per esteso
            This.cBalloon = Dtoc(m.cDate)
            Set Date &l_OldSetDate
        Else
            This.cBalloon = m.cDate
        Endif
        This.cBalloon = Proper(This.cBalloon)
        If Not Empty(m.cString)
            This.cBalloon = Proper(This.cBalloon)+Chr(10)+m.cString
        Endif
        This.Interval=This.nSec*1000
        This.Enabled=.T.
    Endproc

    Proc Timer()
        Local l_MouseCtrl, cName
        AMouseObj(aMouseCtrl,1)
        If Type("aMouseCtrl[1]")="O"
	        l_MouseCtrl = aMouseCtrl[1] &&Oggetto = Sys(1270)
	        l_Form = aMouseCtrl[2] &&Form
	        *-- Verifico che il mouse sia ancora sull'oggetto che ha lanciato il timer
	        If Lower(l_MouseCtrl.Name) = "omouseeventshape"
	        	If Type("l_Form.Name")<>"U"
		            cName = l_Form.Name
		            If This.cFormName==m.cName
		                If Type("This.oForm.oBalloon")='O' And Type("This.oCtrl")='O' And Not Isnull(This.oCtrl)
		                    This.oForm.oBalloon.ctltop = Objtoclient(This.oCtrl, 1) + This.oCtrl.Height
		                    This.oForm.oBalloon.ctlleft = Objtoclient(This.oCtrl, 2) + This.oCtrl.Width
		                    This.oForm.oBalloon.ctlShow(1 , This.cBalloon ,"", 0)
		                Endif
		            EndIf
	            EndIf
	        EndIf
        EndIf
        This.oForm = .Null.
        This.oCtrl = .Null.
        l_MouseCtrl= .Null.
        This.Enabled=.F.
    Endproc
Enddefine

*-- Shape triangolare all'ingi�
Define Class TriShape As Shape
    Visible=.T.
    Dimension aPolyPoints[4,2]
    BorderWidth=1
    Procedure Init
        With This

            Dimension .aPolyPoints[4,2]
            .aPolyPoints[1,1] = 0&&A
            .aPolyPoints[1,2] = 0

            .aPolyPoints[2,1] = 50&&B
            .aPolyPoints[2,2] = 100

            .aPolyPoints[3,1] = 100&&C
            .aPolyPoints[3,2] = 0

            .aPolyPoints[4,1] = 0&&D
            .aPolyPoints[4,2] = 0


            .Polypoints ="This.aPolyPoints"
            .BorderStyle=1
        Endwith
Enddefine


*-- Classe del gadget per la gestione del calendario
Define Class cp_GadgetCalendar As Container

    dCalDate = Ctod('  /  /  ')  &&Data per la costruzione del calendario
    nBorder = 10  &&Bordo lasciato alle estremit� della griglia
    Height = 300
    Width = 250
    Color = Rgb(232,115,82)
    BorderWidth = 0
    nFontColor = Rgb(255,255,255)
    
	* --- Zucchetti Aulla Inizio - Gestione SetAnchorForm
	bNoSetAnchorForm = .T.
	* --- Zucchetti Aulla Fine  - Gestione SetAnchorForm

    *-- Testata del calendario
    Add Object CalHeader As cp_HeaderGadgetCalendar With Top=2,Left=0,Width=221,Height=33, Anchor=10
    *-- Linee
    Add Object DivLine1 As Line With Top=2,Left=0,Width=245,Height=0, Anchor = 10
    Add Object DivLine2 As Line With Top=35,Left=0,Width=245,Height=0, Anchor = 10
    *-- Griglia del calendario
    Add Object CalGrid As cp_CalendarVirtualGrid With Top=40,Left=10,Width=230,Height=255, BorderWidth = 0
    *-- Shape shortcut al mese di oggi
    Add Object sTodayShape As Shape With Top=33,Left=0,Height=5,Width=40,ToolTipText="Oggi", Anchor = 160, MousePointer=15

    Procedure Init()
        With This
            *.Color = .Color
            .CalHeader.HeaderDate.MonthLbl.FontSize=13
            .CalHeader.HeaderDate.MonthLbl.FontBold=.T.
            .CalHeader.HeaderDate.YearLbl.FontSize=13
            .CalHeader.HeaderDate.YearLbl.FontBold=.T.
            .CalGrid.dToday=Date()
            .DivLine1.Width = .Width
            .DivLine2.Width = .Width
        Endwith
    Endproc

    Proc Calculate(xDate,xQuery)
        This.CalGrid.dToday=Iif(Empty(m.xDate),Date(),m.xDate)
        This.CalGrid.cQuery = Iif(Upper(Justext(m.xQuery))=="VQR",m.xQuery,"")
    Endproc

    Procedure Event(cEvent)
        Local l_Event
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(m.l_Event, ' ,', ',')
        If Lower(','+Alltrim(m.cEvent)+',')$Lower(','+Alltrim(m.l_Event)+',')
            This.CalGrid.CleanGrid()
            This.CalHeader.AdjustDate()
        Endif
    Endproc

    Proc CalGrid.CleanGrid(bResize)
        *-- sTodayShape viene posizionato al centro della maschera in larghezza
        This.Parent.sTodayShape.Move(This.Parent.CalHeader.Left+Int((This.Parent.CalHeader.Width-This.Parent.sTodayShape.Width)/2))
        DoDefault(m.bResize)
    Endproc

    Proc CalHeader.BackArrowBtn.Click(nDaySelected)
        DoDefault()
        This.Parent.ArrowBtn(m.nDaySelected,-1)
        ThisForm.NotifyEvent("w_"+This.Parent.Parent.Name+" ArrowButtonBack")
    Endproc

    Proc CalHeader.NextArrowBtn.Click(nDaySelected)
        DoDefault()
        This.Parent.ArrowBtn(m.nDaySelected,1)
        ThisForm.NotifyEvent("w_"+This.Parent.Parent.Name+" ArrowButtonNext")
    EndProc
    
    Proc nFontColor_Assign(nValue)
    	If This.nFontColor <> m.nValue
	    	This.nFontColor = m.nValue
	    	This.CalGrid.nFontColor = m.nValue
	    	This.CalHeader.nFontColor = m.nValue
    	EndIf
    EndProc

    Proc CalHeader.ArrowBtn(nDaySelected,nValue)
        *-- nValue sar� -1 per la BackArrowBtn e 1 per la NextArrowBtn
        With This
            Local nLeft
            *--- Sposto il calendario fuori vista
            m.nLeft = .Parent.CalGrid.Left
            *-- Va moltiplicato per l'opposto di nValue
            cp_SingleTransition(.Parent.CalGrid, "Left", (.Parent.CalGrid.Width+.Parent.nBorder)*(m.nValue*-1), "N", 300, "Linear", .T.)

            .bYearMoved= .bJustYear And .nYear<>Year(.Parent.CalGrid.dDateSelected)
            .HeaderDate.MonthLbl.MousePointer=Iif(.bJustYear And .nYear<>Year(.Parent.CalGrid.dDateSelected),1,15)
            .HeaderDate.YearLbl.MousePointer=Iif(.bJustYear And .nYear<>Year(.Parent.CalGrid.dDateSelected),1,15)

            *-- Tolgo 7 giorni per avere la certezza che la data sia corretta (es. 30/02 restituisce data vuota)
			.Parent.dCalDate = Evl(.Parent.dCalDate, .Parent.CalGrid.dToday)
            .Parent.dCalDate=Iif(Not .bJustYear,Gomonth(.Parent.dCalDate,m.nValue),Date(.nYear,.nMon,Iif(Day(.Parent.dCalDate)<=7,Day(.Parent.dCalDate),Day(.Parent.dCalDate)-7)))

            .Parent.CalGrid.nCurrMonth=.nMon
            .Parent.CalGrid.nCurrYear=.nYear
            *-- Data per la ricostruzione del calendario
            .Parent.CalGrid.nFirstDay=Date(Year(.Parent.dCalDate),Month(.Parent.dCalDate),1) - Dow(Date(Year(.Parent.dCalDate),Month(.Parent.dCalDate),1),2) + 1
            *--- Tolgo 7 gg per centrare il calendario e lasciare sempre una riga con il mese precedente e quello successivo
            .Parent.CalGrid.nFirstDay=Iif(Day(.Parent.CalGrid.nFirstDay) = 1,.Parent.CalGrid.nFirstDay-7,.Parent.CalGrid.nFirstDay)
            *--- Ridisegno il calendario utilizzando il nuovo gg (nFirstDay)
            .Parent.CalGrid.CleanGrid()
            *-- Forzo la Move del contenitore di celle perch� verrebbero tagliati i numeri finali
            .Parent.CalGrid.oViewFrame.oCntCell.Move(0,0,.Parent.CalGrid.oViewFrame.Width,.Parent.CalGrid.oViewFrame.Height)
            *--- Se ho fatto click su un gg del calendario seleziono quella data dopo il refresh, altrimenti seleziono il giorno 1
            nDaySelected = Iif(Type("nDaySelected")='N',nDaySelected,1)
            .Parent.CalGrid.SelectDate(.F.,nDaySelected)
            *--- Valore per transizione
            .Parent.CalGrid.Left=(.Parent.CalGrid.Width+.Parent.nBorder)*m.nValue
            *--- Sposto il calendario in vista
            cp_SingleTransition(.Parent.CalGrid, "Left", m.nLeft, "N", 300, "Linear", .T.)
        Endwith
    Endproc

    Procedure CalGrid.Resize()
        DoDefault()
        With This
            If Not .bYearView
                .Move(.Left,.Top,.Parent.Width-.Left-.Parent.nBorder,.Parent.Height-.Parent.CalHeader.Top-.Parent.CalHeader.Height-.Parent.nBorder)
                .oViewFrame.Move(0,0,.Width,.Height)
                .nHOffset=Int((.oViewFrame.Width-(.nColMinWidth*.ColumnCount))/(.ColumnCount-1))
                .nVOffset=Int((.oViewFrame.Height-(.nRowMinHeight*.RowCount))/(.RowCount-1))
                .oViewFrame.oCntCell.oSelectedRowShape.Visible=.F.
                *--- Svuoto la cache delle Left delle colonne
                .nColMinWidth = .nColMinWidth
                .CleanGrid(.T.)
            Else
                .Move(.Left,.Top,.Parent.Width-.Left-.Parent.nBorder,.Parent.Height-.Parent.CalHeader.Top-.Parent.CalHeader.Height-.Parent.nBorder)
                .oViewFrame.Move(0,0,.Width,.Height)
                .nHOffset=Int((.oViewFrame.Width-(.nColMinWidth*.ColumnCount))/(.ColumnCount-1))
                .nVOffset=Int((.oViewFrame.Height-(.nRowMinHeight*.RowCount))/(.RowCount-1))
                .oViewFrame.oCntCell.oSelectedRowShape.Visible=.F.
                *--- Svuoto la cache delle Left delle colonne
                .nColMinWidth = .nColMinWidth
                .CleanGrid(.T.)
                *-- Necessario per ridimensionamento finale
                .oViewFrame.oCntCell.Move(0,0,.oViewFrame.Width,.oViewFrame.Height)
            Endif
        Endwith
    Endproc

    Procedure Color_Assign(nValue)
        With This
            *-- Uniforma il colore di tutti gli oggetti in base a quello della form
            *If .Color <> m.nValue
            	Local l_nAlphaColor, WhiteDiff, BlackDiff
	            .Color = m.nValue
	            .BackColor = .Color
	            .CalHeader.BackColor = .Color
	            l_nAlphaColor = RGBAlphaBlending(.Color,GetBestForeColor(.Color),0.5)
	            .DivLine1.BorderColor = l_nAlphaColor 
	            .DivLine2.BorderColor = l_nAlphaColor 
	            .sTodayShape.BackColor = .nFontColor
	            .sTodayShape.BorderColor = .nFontColor
	            .CalHeader.HeaderDate.MonthLbl.BackColor = .Color
	            .CalHeader.HeaderDate.YearLbl.BackColor = .Color
				*WhiteDiff = GetBrightnessDiff(.Color, RGB(243,243,243))
				*BlackDiff = GetBrightnessDiff(.Color, RGB(40,40,40))
				*-- Utilizzo il colore 'peggiore' per avvicinare il font allo sfondo
	            *.CalGrid.nAlphaFontColor = RGBAlphaBlending(.nFontColor,Iif(m.WhiteDiff >= m.BlackDiff, RGB(40,40,40), RGB(243,243,243)),0.5)
	            .CalGrid.nAlphaFontColor = RGBAlphaBlending(.nFontColor,.Color,0.5)
	            .CalGrid.BackColor = .Color
	            .CalGrid.oViewFrame.oCntCell.oSelectedRowShape.BorderColor = .nFontColor
	            .CalGrid.CleanGrid()
            *EndIf
        Endwith
    Endproc

    Proc GetVar(cColRet,dDateRet)
        *-- Funzione che restituisce una colonna specifica della query in base alla data selezionata (o specificata)
        xDate = Iif(Type("dDateRet")="D",dDateRet,This.CalGrid.dDateSelected)
        l_SearchInCursor=Sys(2015)
        Select &cColRet As ColResult From (This.CalGrid.cQueryCursor) Where CalDate=xDate Into Cursor (l_SearchInCursor)
        l_Result = ColResult
        Use In Select(l_SearchInCursor)
        Return l_Result
    Endproc

    Proc CalHeader.HeaderDate.Click()
        *-- Passaggio a visione dei mesi nella griglia
        With This.Parent
            If Not .Parent.CalGrid.bYearView
                *-- Da vista giorni a vista mesi
                DoDefault()
                *-- Transizione verso il basso, sposto la griglia fuori vista
                Local nTop
                nTop = .Parent.CalGrid.Top
                cp_SingleTransition(.Parent.CalGrid, "Top", .Parent.CalGrid.Height+.Parent.CalGrid.Top, "N", 300, "Linear", .T.)
                *-- Il click diventer� uno shortcut al mese da cui vengo
                .bYearMoved = .F.
                This.MonthLbl.MousePointer=15
                This.YearLbl.MousePointer=15
                .Parent.CalGrid.bYearView = .T.
                .Parent.CalGrid.nCurrMonth = 0
                .Parent.CalGrid.RowCount = 3
                .Parent.CalGrid.ColumnCount = 4
                .Parent.CalGrid.nColMinWidth = 63
                .Parent.CalGrid.nRowMinHeight = 38
                .Parent.CalGrid.Resize()
                *-- Transizione verso l'alto, sposto la griglia in vista
                cp_SingleTransition(.Parent.CalGrid, "Top", m.nTop, "N", 300, "Linear", .T.)
            Else
                *-- Da vista mesi a vista giorni
                *-- Se sono rimasto al solito mese riapro la vista sul mese di oggi
                If  Not .bYearMoved
                    .Parent.CalGrid.FromYearToMonth(.F., .bYearMoved)
                Endif
            Endif
        Endwith
    Endproc

    Proc sTodayShape.Click()
        *-- Shortcut alla data di oggi, utilizzato se la data di oggi non � selezionata
        With This.Parent
            Local bCondRightMonth, bCondNotYearView
            *-- Se siamo nel mese giusto ma non nella data seleziona la data di oggi
            m.bCondRightMonth = Month(.CalGrid.dToday)==.CalGrid.nCurrMonth And Year(.CalGrid.dToday)==.CalGrid.nCurrYear And Not Day(.CalGrid.dToday)==Day(.CalGrid.dDateSelected)
            *-- Se siamo nella vista mensile e il mese o l'anno non sono quelli di oggi
            m.bCondNotYearView = Not (Month(.CalGrid.dToday)==.CalGrid.nCurrMonth And Year(.CalGrid.dToday)==.CalGrid.nCurrYear)
            *-- Alle precedenti condizioni � aggiunta la condizione che siamo sulla vista annuale
            If m.bCondRightMonth Or m.bCondNotYearView Or .CalGrid.bYearView
                If m.bCondRightMonth
                    *-- Se siamo nel mese giusto seleziono la data di oggi
                    .CalGrid.SelectDate(.F., Day(.CalGrid.dToday))
                Else
                    .CalGrid.FromYearToMonth(.F., .T.)
                Endif
            Endif
        Endwith
    Endproc

Enddefine
*--- Zucchetti Aulla - Fine

*-- Shape circolare
Define Class CircleShape As Shape

    * add a shape object to the current form
    * and set some basic properties
    Width = 200
    Height = 80
    BackColor = Rgb(128,0,0)
    StartPoint = 0
    FinalPoint = 0

    Dimension aPolyPoints[4,2]
    BorderWidth=1
    Proc Init(nStart, nFinal)
    	If !Empty(m.nStart)
    		This.StartPoint = m.nStart
    	EndIf 
        This._Draw(This.StartPoint,Iif(!Empty(m.nStart),m.nFinal,This.FinalPoint))
    Endproc

    Proc _Draw(nStart,nFinal)
        With This

            * defining the polypoints array
            * change the values of lnstart and lnfinal to determine
            * the angle of the pie slice
            Local lnsweep, N, lnradius, lnAngle

            lnsweep = Abs(m.nFinal - m.nStart)
            lnradius = 50
            .StartPoint = nStart
            .FinalPoint = nFinal
            Dimension .aPolyPoints(lnsweep + 2, 2)
            For N = 1 To lnsweep + 1
                lnAngle = .StartPoint + N - 1 - 90
                .aPolyPoints(N,1) = (lnradius * Cos(Dtor(lnAngle)) + lnradius)
                .aPolyPoints(N,2) = (lnradius * Sin(Dtor(lnAngle)) + lnradius)
            Endfor

            If lnsweep = 360 && closed ellipse, so dont draw the center point
                .aPolyPoints(N,1) = .aPolyPoints(N-1,1)
                .aPolyPoints(N,2) = .aPolyPoints(N-1,2)
            Else
                * determine the center point
                .aPolyPoints(N,1) = lnradius
                .aPolyPoints(N,2) = lnradius
            Endif


            .Polypoints ="This.aPolyPoints"
            .BorderStyle=1
        Endwith
    Endproc

    Proc FinalPoint_Assign(nValue)
        This.FinalPoint=nValue
        This._Draw(This.StartPoint,This.FinalPoint)
    Endproc

Enddefine

*-- Progress bar
Define Class cp_ProgressBar As Container
    cEvent=''
    cValBar = '' && Se 'V' viene visualizzato il valore, altrimento viene visualizzata la percentuale nella barra singola
    Min = 0
    Max = 0
    Value = 0
	nSourceType = 0
    cQuery = ''
    
    nValue = 0
    
    cStringFormat = "999999999"
    nCurrMin = 0
    nCurrMax = 0
    nCurrValue = 0
    Visible = .F.
    bVisibleOnResize = .F.

    cSymbol = ''
    nProgressBarHeight = 50 &&Rapporto tra dimensione Barra di sfondo e ProgressBar (0-100 %)

    bTransition = .T. &&Se .F. non vengono eseguite le transizioni
    nTimeTransition = 300 &&Tempo delle transizioni
    bWaitTransition = .F.  &&Parametro bWait della transizione
    oCtrlMouse=.Null.
    nProgressBarColor = -1 &&Colore della barra del progresso
    nProgressBarBorderColor = -1 &&Colore del bordo della barra del progresso
    nMainBarColor = -1 &&Colore della barra principale
    nMainBarBorderColor = -1 &&Colore del bordo della barra principale
    nPerCentFontColor=0 && Colore della label percentuale
    nPerCentFontSize = 9 &&Dimensione della label percentuale
    cPerCentFontName = "Arial" &&Nome font della label percentuale
    bPerCentFontBold = .F.
    nMainBarBackstyle = 1
    nFormColor = 0
    cBitmapFile=''

    Old_Value =0

    Dimension aQueryValues[1] &&Array dei valori dalla query (barra orizzontale)

    Proc Init()
        Bindevent(Thisform,"BackColor",This,"ModifyBackColor",1)
        With This
            .BackStyle=0
            .BorderWidth=0
			*-- Valorizzazione fasulla per far scattare l'Assign
            .nPerCentFontSize = .nPerCentFontSize
            .nPerCentFontColor = .nPerCentFontColor
        Endwith
    Endproc

    Proc ModifyBackColor()
        This.nFormColor = Thisform.BackColor
    Endproc

	PROCEDURE Calculate(xVal, nMin, nMax, bByEvent)
        With This
            *-- Setta i valori della barra
            .SetProperty(m.xVal, m.nMin, m.nMax)
            *-- Se bByEvent = .T. l'operazione verr� eseguita solo alla chiamata dell'evento
            IF !m.bByEvent
            	*--- Prepara al draw
            	.PrepareBar()
            	*--- Disegna la barra
            	.ReDraw()
            EndIf
        Endwith
	ENDPROC 

    Proc Event(cEvent)
        Local l_Event
        l_Event=''
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(l_Event, ' ,', ',')
        If Lower(','+Alltrim(cEvent)+',')$Lower(','+Alltrim(l_Event)+',')
        	*--- Prepara al draw
        	This.PrepareBar()
        	*--- Disegna la barra
        	This.ReDraw()
        Endif
    Endproc

    Procedure Value_Access()
		Return This.nValue
    EndProc
    
    Procedure Value_Assign(nValue)
    	*-- Setta i valori della barra
        This.SetProperty(This.Value, This.Min, This.Max, This.cQueryBar)
    	*--- Prepara al draw
    	This.PrepareBar()
    	*--- Disegna la barra
    	This.ReDraw()
    Endproc

	*--- Esegue tutte le operazioni per preparate i dati finali da disegnare
	PROCEDURE PrepareBar()
		WITH this 
			LOCAL l_i
			*--- Processo cQuery
			LOCAL  m.l_TmpQueryCursor, oldArea
			 m.l_TmpQueryCursor = "TmpProg"+SYS(2015)
			oldArea= SELECT()
			If .nSourceType = 1 && Progress da query
				IF !EMPTY(.cQuery) And cp_FileExist(ForceExt(.cQuery,'VQR'))
					vq_exec(.cQuery, Thisform, m.l_TmpQueryCursor)
					IF USED(m.l_TmpQueryCursor)			
						.nValue = Nvl(&l_TmpQueryCursor..xValue,0)
						.Max = IIF(Type("&l_TmpQueryCursor..nMax")='N' And !IsNull(&l_TmpQueryCursor..nMax), &l_TmpQueryCursor..nMax, .Max)
						.Min = IIF(Type("&l_TmpQueryCursor..nMin")='N' And !IsNull(&l_TmpQueryCursor..nMin), &l_TmpQueryCursor..nMin, .Min)
						USE IN SELECT(m.l_TmpQueryCursor)
					ENDIF
				Else
					If PemStatus(ThisForm, 'cAlertMsg', 5)
						ThisForm.cAlertMsg = ah_MsgFormat('Query non definita o inesistente:%0"%1"', Alltrim(.cQuery))
					Else
						cp_msg(cp_MsgFormat(MSG_CANNOT_FIND_QUERY__, .cQuery))
					EndIf
				EndIf
			EndIf
			SELECT(m.oldArea)
	        .SymbolAssign()
		EndWith
	EndProc

	PROCEDURE SetProperty(xVal, nMin, nMax)
		With this
			Local l_nMin, l_nMax
			l_nMin = Iif(Type("m.nMin") = 'N', m.nMin, .Min)
			l_nMax = Iif(Type("m.nMax") = 'N', m.nMax, .Max)
			
			Do Case
				*--- Progress da valore numerico							
				Case .nSourceType = 0
					.cQuery = ''
					If Type("m.xVal")='N'
						.nValue = m.xVal
					Else
						.nValue = Val(m.xVal)
					Endif
					.Min = m.l_nMin
					.Max = m.l_nMax
				*--- Progress da query (xValue, nMax, nMin)								
				Case .nSourceType = 1
					.cQuery = m.xVal
					.nValue = 0
					.Min = m.l_nMin
					.Max = m.l_nMax
			EndCase
		EndWith
	EndProc
	
	Procedure ReDraw()
		this.RemoveObjects()
        this.Draw()
        this.Resize()
	EndProc

	Procedure RemoveObjects()
	EndProc

	*-- Procedura lanciata al completamento delle transizioni
	Proc OnCompletedTransition()
		This.PerCentLbl_Resize()
		Thisform.NotifyEvent("BarUpdated")
	Endproc

	Procedure StrTranLabel(cString, nTagValue, nQueryBarValue)
		With this
			If !Empty(cString)
				Local l_nDelta, l_value 
				l_nDelta = MAX(.Max-.Min, 1)
				l_value = Min(MAX(.nValue - .Min, 0)/m.l_nDelta,1)
				cString = Strtran(m.cString,'%V',Transform(.nValue,.cStringFormat),-1,-1,1)
				cString = Strtran(m.cString,'%P',Transform(l_value *100,.cStringFormat)+"%",-1,-1,1)
				cString = Strtran(m.cString,'%B',Transform(m.nTagValue,.cStringFormat)+"%",-1,-1,1)
				cString = Strtran(m.cString,'%T',Transform(.Max,.cStringFormat),-1,-1,1)
				cString = Strtran(m.cString,"%S",Transform(m.nQueryBarValue,.cStringFormat)+" "+.cTagSymbol,-1,-1,1)
				Return m.cString
			Else
				Return ''
			EndIf
		EndWith
	EndProc

    Proc cValBar_Assign(bValue)
        With This
		    .cValBar = m.bValue
		    .PerCentLbl.Visible= !Empty(.cValBar)
			.Symbol.Visible= !Empty(.cValBar)
        Endwith
    Endproc

    *-- Modifica colore della Label percentuale
    Proc nPerCentFontColor_Assign(nValue)
        With This
            .nPerCentFontColor = m.nValue
            .PerCentLbl.ForeColor=.nPerCentFontColor
            .Symbol.ForeColor=.nPerCentFontColor
        Endwith
    Endproc

    *-- Modifica dimensione della Label percentuale
    Proc nPerCentFontSize_Assign(nValue)
        With This
            .nPerCentFontSize = m.nValue
            .PerCentLbl.FontSize = m.nValue
            .Symbol.FontSize = m.nValue
            .PerCentLbl_Resize()
        Endwith
    EndProc
    
    *-- Modifica font della Label percentuale
    Proc cPerCentFontName_Assign(cValue)
        With This
            .cPerCentFontName = m.cValue
            .PerCentLbl.FontName = m.cValue
            .Symbol.FontName = m.cValue
            .PerCentLbl_Resize()
        Endwith
    EndProc
    
    *-- Modifica grassetto della Label percentuale
    Proc bPerCentFontBold_Assign(bValue)
        With This
            .bPerCentFontBold = m.bValue
            .PerCentLbl.FontBold = m.bValue
            .Symbol.FontBold = m.bValue
            .PerCentLbl_Resize()
        Endwith
    EndProc

    Proc SymbolAssign()
        With This
            .Symbol.Caption = IIF( Not(.cValBar=='V'),"%",.cSymbol)
        Endwith
    Endproc

	Procedure SaveBitmap(cPath)
		If Empty(m.cPath) And !Empty(This.cBitmapFile)
			Delete File (This.cBitmapFile)
        Endif
        #Define SRCCOPY 0xCC0020
        Declare Integer PrintWindow In user32 Integer HWnd, Integer hdcBlt, Integer nFlags

        Local hdcForm,hdcMem ,hbitmap ,L_TmpBmp ,hbitmap2,hDC ,hdcMem2, cFile, nTop, nHeight
        hdcForm = GetWindowDC(Thisform.HWnd)
        hdcMem = CreateCompatibleDC(hdcForm)
        hbitmap = CreateCompatibleBitmap(hdcForm ,  Thisform.Width, Thisform.Height)
        SelectObject(hdcMem, hbitmap)

        PrintWindow(Thisform.HWnd, hdcMem, 1)

		L_TmpBmp = _Screen.CP_THEMESMANAGER.CreateBitmap(This.Width,This.Height,"FFFFFF")
		hbitmap2=LoadPicture(m.L_TmpBmp )
		hDC = GetDC(0)
		hdcMem2= CreateCompatibleDC(hDC)
		DeleteObject(SelectObject(hdcMem2, hbitmap2.handle))
		m.nTop = This.Top+Iif(Type("Thisform.oHeader.Height")<>"U",Thisform.oHeader.Height,0)+Iif(Type("Thisform.oTabMenu.Top")<>"U",Thisform.oTabMenu.Top,0)
		BitBlt(hdcMem2,0,0, This.Width,This.Height, hdcMem, This.Left,m.nTop,SRCCOPY)
		m.cFile = Iif(!Empty(m.cPath),m.cPath,m.L_TmpBmp)
		SavePicture(hbitmap2, m.cFile)
		If Empty(m.cPath)
			This.cBitmapFile=m.cFile
		EndIf

		DeleteDC(hdcMem2)
		DeleteDC(hdcMem)
		DeleteDC(hDC)
		ReleaseDC(ThisForm.hwnd, hdcForm)

        DeleteObject(hbitmap)

    Endproc

    Proc Destroy()
        Unbindevents(Thisform,"BackColor",This,"ModifyBackColor")
        DoDefault()
    Endproc

Enddefine

Define Class cp_HorizontalBar As cp_ProgressBar

    bLeftObj=.F. && indica se � stato cambiato oggetto alla MouseMove
    
    bCurved= .F. &&	Curvatura barra .F.: curvatura 0 ; .T.: curvatura 15
    
    cQueryBar = ''
    oTimer=.Null.
    bOnBarTransition = .F.
    bOnWindowsTransition = .F.
    nProgressBarHeight = 100
    
    cTagSymbol = ''

    nBarVersion = 1 &&1: barra orizzontale standard, 2: barra windows composta da shape
    nMaxWindowsShapes=20 &&Numero massimo di windows shape visibili
    cToolTip=''
    *-- Allineamento della percentuale: 0 a sinistra, 1 a destra e 2 al centro della barra progressiva, 3 al centro della barra principale
    nAlignPerCent=0
    nWindowsShapes = 0 &&Numero di windows shapes attualmente visibili
    nBookMarksFontColor = Rgb(255,255,255)
    cBookMarksFontName = "Arial"
    nBookMarksFontSize = 9

    nAlignPerCent = 3

	nBookMark = 0
	nBarHeight = 20
	
	Dimension aQueryBar[1,4]
	
*-- ColorShape serve per invertire il colore della Percentuale al sovrapposizione della ProgressBar senza modificare il colore di questa (Merge Pen Not con la MainBar)
    Add Object MainBar As Shape With Left=15,Top=5,Width=350,Height=20, BorderWidth=2
    Add Object ColorShape As Shape With Left=15,Top=5,Width=10,Height=20, BackColor=Rgb(255,255,255), BorderWidth=2, BorderColor=Rgb(255,255,255), Visible = .f.
    Add Object PerCentLbl As Label With Width=350, Caption="0",BackStyle=0, FontSize=9
    Add Object Symbol As Label With Width=4,BackStyle=0, Caption=""
    Add Object ProgressBar As Shape With Left=15,Top=5,Width=10,Height=20,BorderWidth=2, DrawMode=14, Visible = .f.
    Add Object oEventShape As Shape With BackStyle=0, BorderStyle=0,Left=15,Top=5,Width=350,Height=20

    Proc Init()
        With This
            DoDefault()
            .Symbol.Caption = Iif( Not(.cValBar=='V'),"%",.cSymbol)
            .nMainBarColor=Iif(.nMainBarColor<>-1,.nMainBarColor,Iif(i_ThemesManager.GetProp(127)<> -1,i_ThemesManager.GetProp(127),Rgb(255,255,255)))
            .nMainBarBorderColor=Iif(.nMainBarBorderColor<>-1,.nMainBarBorderColor,Iif(i_ThemesManager.GetProp(128)<> -1,i_ThemesManager.GetProp(128),Rgb(255,255,255)))
            .nProgressBarColor=Iif(.nProgressBarColor<>-1,.nProgressBarColor,Iif(i_ThemesManager.GetProp(125)<> -1,i_ThemesManager.GetProp(125),Rgb(0,200,62)))
            .nProgressBarBorderColor=Iif(.nProgressBarBorderColor<>-1,.nProgressBarBorderColor,Iif(i_ThemesManager.GetProp(126)<> -1,i_ThemesManager.GetProp(126),Rgb(0,128,62)))
            .oEventShape.ZOrder(0)
            .Resize()
        Endwith
    Endproc

	PROCEDURE Calculate(xVal, cQueryBar, nMin, nMax, bByEvent)
        With This
            *-- Setta i valori della barra
            .SetProperty(m.xVal, m.nMin, m.nMax, m.cQueryBar)
            *-- Se bByEvent = .T. l'operazione verr� eseguita solo alla chiamata dell'evento
            IF !m.bByEvent
            	*--- Prepara al draw
            	.PrepareBar()
            	*--- Disegna la barra
            	.ReDraw()
            EndIf
        Endwith
	ENDPROC 

    Proc Event(cEvent)
        Local l_Event
        l_Event=''
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(l_Event, ' ,', ',')
        If Lower(','+Alltrim(cEvent)+',')$Lower(','+Alltrim(l_Event)+',')
        	*--- Prepara al draw
        	This.PrepareBar()
        	*--- Disegna la barra
        	This.ReDraw()        
        Endif
    Endproc

    Proc SetStyle()
        With This
            *-- Stile: 0 = Curvatura 0, 1 = Curvatura 15
            Local l_nCurv
            l_nCurv = Iif(.bCurved, 15,0)
            .MainBar.Curvature=l_nCurv
            .ProgressBar.Curvature = l_nCurv
            .ColorShape.Curvature=.ProgressBar.Curvature
            .ProgressBar.Width = .ProgressBar.Width
            .ProgressBar.Left =.MainBar.Left
            *-- E' stata eseguita la query
            If !Empty(.cQueryBar)
                Local l_olderr, bErr, l_Count,l_cNum, l_objShape
                l_Count=2 && Solo il primo non � sovrapposto ad altri
                l_olderr=On('error')
                bErr=.F.
                On Error bErr=.T.
                l_objShape = .oShape_1
                l_objShape.Curvature=l_nCurv
                Do While Not bErr
                    l_cNum=Alltrim(Str(l_Count))
                    l_objShape = .oShape_&l_cNum
                    l_objShape.Curvature=l_nCurv
                    l_objShape.Move(Round(l_objShape.Left+(10*(Iif(.bCurved, -1,1))),0),l_objShape.Top,l_objShape.Width-(10*(Iif(.bCurved, -1,1))))
                    l_Count=l_Count+1
                EndDo
                *-- L'ultimo valore non va ridimensionato, dato che non posso predire quale sia lo correggo alla fine con l'operazione inversa
                l_objShape.Move(Round(l_objShape.Left-(10*(Iif(.bCurved, -1,1))),0),l_objShape.Top,l_objShape.Width+(10*(Iif(.bCurved, -1,1))))
                On Error &l_olderr
            Endif
        Endwith
    Endproc
	
	*--- Esegue tutte le operazioni per preparate i dati finali da disegnare
	Procedure PrepareBar()
	With This
		DoDefault()
		.SetStyle()
		If !Empty(.cQueryBar)
			If Upper(Justext(.cQueryBar))="VQR"
				*--- Se query
				m.l_TmpQueryCursor = "TmpProg"+Sys(2015)
				oldArea= Select()
				If cp_FileExist(Forceext(.cQueryBar,'VQR'))
					vq_exec(.cQueryBar, Thisform, m.l_TmpQueryCursor)
					If Used(m.l_TmpQueryCursor)
						l_i=1
						Select(m.l_TmpQueryCursor)
						Locate For 1=1
						Do While !Eof()
							Dimension .aQueryBar[m.l_i,4]
							.aQueryBar[m.l_i,1] = &l_TmpQueryCursor..xValue
							.aQueryBar[m.l_i,2]	= &l_TmpQueryCursor..nColor
							*--- se presenti
							.aQueryBar[m.l_i,3] = Iif(Type("&l_TmpQueryCursor..cCaption")<>'U' And !Isnull(&l_TmpQueryCursor..cCaption), &l_TmpQueryCursor..cCaption, "")
							.aQueryBar[m.l_i,4]	= Iif(Type("&l_TmpQueryCursor..cTooltip")<>'U' And !Isnull(&l_TmpQueryCursor..cTooltip), &l_TmpQueryCursor..cTooltip, "")
							If .nSourceType = 2
								*-- Il Max viene calcolato con la somma dei valori dell'array
								.Max = .Max + .aQueryBar[m.l_i,1]
							Endif
							Select(m.l_TmpQueryCursor)
							Continue
							l_i = m.l_i + 1
						Enddo
						Use In Select(m.l_TmpQueryCursor)
					Endif
				Else
					If Pemstatus(Thisform, 'cAlertMsg', 5)
						Thisform.cAlertMsg = ah_MsgFormat('Query non definita o inesistente:%0"%1"', Alltrim(.cQueryBar))
					Else
						cp_msg(cp_MsgFormat(MSG_CANNOT_FIND_QUERY__,.cQueryBar))
					Endif
				Endif
				Select(m.oldArea)
			Else
				*--- Tuple
				* 'Valore;Colore;Caption;Tooltip | Valore2;Colore2;Caption2;Tooltip2 | ...'
				* Valore = Numero | Numero%
				* Colore = Numero | RGB(...)
				* Caption = "..." (%V Valore, %P Percentuale, %T Max) Esempio: "%V/%T" (10/152) dove 152 � il massimo e 10 il valore
				* Tooltip = "..." (%V Valore, %P Percentuale, %T Max)
				Local l_NumTarget, l_j
				l_NumTarget = Alines(l_aTarget, .cQueryBar, "|")
				l_i=0
				For l_i=1 To m.l_NumTarget
					Dimension .aQueryBar[m.l_i,4]
					l_NumProp = Alines(l_aProp, l_aTarget[l_i], ";")
					For l_j = 1 To m.l_NumProp
						l_aProp[m.l_j] = Alltrim(l_aProp[m.l_j])
						If !Empty(l_aProp[m.l_j])
							If l_j == 1 And Right(l_aProp[m.l_j], 1)='%' &&Valore
								*--- calcolo da percentuale
								l_Percent = Evaluate(Left(l_aProp[m.l_j], Len(l_aProp[m.l_j])-1))
								.aQueryBar[m.l_i,l_j] = (l_Percent/100*(.Max-.Min)) + .Min
							Else
								.aQueryBar[m.l_i,l_j] = Evaluate(l_aProp[m.l_j])
							Endif
						Endif
					Endfor
					*-- Sostituisco i possibili parametri passati a Caption e Tooltip
					If .nSourceType = 2
						*-- Il Max viene calcolato con la somma dei valori dell'array
						.Max = .Max + .aQueryBar[m.l_i,1]
					Endif
				Endfor
			Endif
			*--- Ordino per valore
			=Asort(.aQueryBar,1)
		Endif
	Endwith
	Endproc

	Procedure SetProperty(xVal, nMin, nMax, cQueryBar)
		With this
			Local l_nMin, l_nMax, l_cQueryBar
			l_nMin = Iif(Type("m.nMin") = 'N', m.nMin, .Min)
			l_nMax = Iif(Type("m.nMax") = 'N', m.nMax, .Max)
			l_cQueryBar = Iif(Type("m.cQueryBar") = 'C', m.cQueryBar, '')		
			
			Do Case
				*--- Progress da valore numerico
				* ------------
				* |||||      |
				* ------------						
				*--- Progress con target
				* -------------
				* |XXX|YYY|ZZZ|
				* ||||||      |
				* |XXX|YYY|ZZZ|
				* -------------									
				Case .nSourceType = 0
					.cQuery = ''
					If Type("m.xVal")='N'
						.nValue = m.xVal
					Else
						.nValue = Val(m.xVal)
					Endif
					.cQueryBar = m.l_cQueryBar
					.Min = m.l_nMin
					.Max = m.l_nMax
				*--- Progress da query (xValue, nMax, nMin)
				* ------------
				* |||||      |
				* ------------	
				*--- Progress con target
				* -------------
				* |XXX|YYY|ZZZ|
				* ||||||      |
				* |XXX|YYY|ZZZ|
				* -------------										
				Case .nSourceType = 1
					.cQuery = m.xVal
					.nValue = 0
					.cQueryBar = m.l_cQueryBar
					.Min = m.l_nMin
					.Max = m.l_nMax
				*---Se passato xVal Progress da query (xValue, nMax, nMin), altrimenti non gestito
				* -------------
				* |XXX|YYY|ZZZ|
				* -------------								
				Case .nSourceType = 2
					.cQuery = ''
					.nValue = 0
					.cQueryBar = m.xVal
					.Min = 0
					.Max = 0
			EndCase
		EndWith
	EndProc

	Procedure RemoveObjects()
        With This
        	If !.bOnBarTransition
	        	Local l_Count, l_Index
			    l_Count=AMembers(aObjects, this,2)
			    For l_Index=1 to l_count
			        If Lower(Left(aObjects[m.l_Index], 7)) = "oshape_" Or Lower(Left(aObjects[m.l_Index], 9)) = "lpercent_"
			            This.RemoveObject(aObjects[m.l_Index])
			        EndIf
			    EndFor
            EndIf
        EndWith
	Endproc

	Procedure RemoveWindowShapes()
		With this
			*-- Distruggo i vecchi shape creati
			If !.bOnWindowsTransition
				Local l_Count, l_Index
				l_Count=AMembers(aObjects, This,2)
				For l_Index=1 to l_count
				    If Lower(Left(aObjects[m.l_Index], 14)) = "oshapewindows_"
				        .RemoveObject(aObjects[m.l_Index])
				        .nWindowsShapes=0
				    EndIf
				EndFor
            EndIf
		EndWith
	EndProc

	PROCEDURE Draw()
		With this
            Local l_nWidth, l_nDelta, l_value
            .MainBar.Visible = EMPTY(.aQueryBar[1, 1])
            .ProgressBar.Visible = !EMPTY(.nValue) and (.nBarVersion=1)
            .ColorShape.Visible = .ProgressBar.Visible
			.PerCentLbl.Visible = !Empty(.cValBar)
			.Symbol.Visible = .PerCentLbl.Visible
			IF !EMPTY(.aQueryBar[1, 1]) And !.bOnBarTransition
				*-- Finche non termino la transizione blocco tutte le prossime
				.ProgressBar.DrawMode = 13
				.bOnBarTransition = .bTransition
				.CreateBar(1,0)
				.oEventShape.ZOrder(0)
			Else
				.Draw_Value()
			EndIf
		EndWith
	EndProc
	
	Procedure Draw_Value()
		With this
			IF Type("this.nValue")='N'
				l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
				l_value = Min(MAX(.nValue - .Min, 0)/m.l_nDelta,1)
				IF .nBarVersion=1
					*-- Barra semplice
					.Old_Value = l_value
					l_nWidth = INT(.width * l_value)
					If .bTransition
			            *-- Transizione dello shape corrente
			            l_oTransit = Createobject("cp_TransitionManager")
			            l_oTransit.Add(.ProgressBar, "Width" ,m.l_nWidth, "N", .nTimeTransition, "Linear")
			            l_oTransit.Add(.ColorShape, "Width" ,m.l_nWidth, "N", .nTimeTransition, "Linear")
						l_oTransit.OnCompleted(This,"OnCompletedTransition()")
			            l_oTransit.Start(.bWaitTransition)
					Else
						.ProgressBar.Move(.ProgressBar.Left,.ProgressBar.Top,m.l_nWidth)
						.ColorShape.Move(.ProgressBar.Left,.ProgressBar.Top,m.l_nWidth)
						Thisform.NotifyEvent("BarUpdated")
					EndIf
				Else
					*-- Barra windows
					If !.bOnWindowsTransition
						.bOnWindowsTransition = .bTransition
			            If .nWindowsShapes>0
			            	If .Old_Value > l_value
			            		.RemoveWindowShapes()
			            		.Old_Value = l_value
			            		.NextWindowShape(1,0)
			            	Else
			            		.Old_Value = l_value
		                        *-- Shape presenti, va proseguita la barra gi� in vista
		                        l_num = Alltrim(Str(.nWindowsShapes))
		                        l_oObjShape = .oShapeWindows_&l_num
		                        *-- La Left non serve, viene usata quella dell'oggetto
		                       	.NextWindowShape(.nWindowsShapes,0,l_oObjShape)
			            	EndIf
			            	
	                    Else
	                    	.Old_Value = l_value
			                .NextWindowShape(1,0)
			            EndIf
		            EndIf
				EndIf
        	.PercentLbl.Caption = Iif( Not(.cValBar=='V'),Alltrim(Transform(l_value*100,.cStringFormat)),Alltrim(Transform(.nValue,.cStringFormat)))
			EndIf
		EndWith
	EndProc
	
	Procedure CreateBar(nIter,nLeft)
		With This
			LOCAL l_nLeft, l_nWidth, l_nDelta, l_oObj, l_oPerCent, l_value, l_oTransit,l_ProgressHeight, l_nPerCentWidth
			*--- Nuovo shape
			*-- Prendo la Left dello shape
			l_nLeft = Max(m.nLeft-Iif(.bCurved,20,0),0)
			l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
			l_value = Min(MAX(.aQueryBar[m.nIter, 1] - .Min, 0)/m.l_nDelta,1)
			*-- Width dello shape
			If .nSourceType = 2
				l_nWidth = INT(.width * m.l_value)
				If Type("This.aQueryBar[m.nIter+1, 1]")="U"
					l_nWidth = .width - m.l_nLeft
				endIf		
			Else
				l_nWidth = INT(.width * m.l_value) - m.l_nLeft
				If Type("This.aQueryBar[m.nIter+1, 1]")="U" And .bCurved
					l_nWidth = .width - m.l_nLeft
				endIf
			EndIf
			l_num = Transform(m.nIter)
            .AddObject("oShape_"+l_num,"Shape")
            .AddObject("lPerCent_"+m.l_num,"Label")
			l_oObj = .oShape_&l_num
			*-- Posizionamento con width 0
			l_oObj.Move(l_nLeft,.MainBar.Top,0, .nBarHeight)
			l_oObj.BackColor = .aQueryBar[m.nIter, 2]
			l_oObj.BorderColor = .aQueryBar[m.nIter, 2]
			l_oObj.BorderWidth = .MainBar.BorderWidth
			l_oObj.Curvature = .MainBar.Curvature
			l_oObj.Visible = .T.
			l_oPerCent = .lPerCent_&l_num
			l_oPerCent.BackStyle = 0
			*-- Sostituisco i possibili parametri passati a Caption e Tooltip
			.aQueryBar[m.nIter,3] = .StrTranLabel(.aQueryBar[m.nIter,3], l_value, .aQueryBar[m.nIter, 1])
			.aQueryBar[m.nIter,4] = .StrTranLabel(.aQueryBar[m.nIter,4], l_value, .aQueryBar[m.nIter, 1])
			l_oPerCent.Caption = Evl(.aQueryBar[m.nIter, 3],'')
			l_oPerCent.ForeColor = .nBookMarksFontColor
			l_oPerCent.FontSize = .nBookMarksFontSize
			l_oPerCent.FontName = .cBookMarksFontName
			*--- La visualizzazione, la posizione e caption della percentuale dipendono dal valore di nBookMark
			l_oPerCent.Visible = (Bitand(.nBookMark,1)=1)
			IF Empty(l_oPerCent.Caption)
				If Bitand(.nBookMark,4)=4
					*-- Visualizza il valore
					l_oPerCent.Caption = Transform(.aQueryBar[m.nIter, 1],.cStringFormat)
				Else
					*-- Visualizza la percentuale
					l_oPerCent.Caption =  Transform(INT(100*m.l_value))+"%"
				EndIf
			Else 
				l_oPerCent.Caption = Strtran(l_oPerCent.Caption,"%S", Transform(.aQueryBar[m.nIter, 1],.cStringFormat)+" "+.cTagSymbol)
			EndIf
			l_oPerCent.Caption = Alltrim(l_oPerCent.Caption)
			l_nPerCentWidth = cp_LabelWidth2(l_oPerCent.Caption, l_oPerCent.FontName, l_oPerCent.FontSize)
			If Bitand(.nBookMark,2)=2
				*-- Percentuale visible dentro lo shape
				l_oPerCent.Move(Max(m.l_nLeft+(m.l_nWidth-Iif(.bCurved,10,0)-m.l_oPerCent.Width)/2,0), m.l_oObj.Top + Max((m.l_oObj.Height-m.l_oPerCent.Height)/2,0), l_nPerCentWidth )
			Else
				*-- Percentuale visible sopra lo shape
				l_oPerCent.Move(Max(Min(l_nLeft+m.l_nWidth-Iif(.bCurved,10,0)-(m.l_oPerCent.Width/2),.Width-m.l_oPerCent.Width),0), m.l_oObj.Top-m.l_oPerCent.Height, l_nPerCentWidth )
			EndIf
			If .bTransition
	            *-- Transizione dello shape corrente
	            l_oTransit = Createobject("cp_TransitionManager")
	            l_oTransit.Add(l_oObj, "Width" ,m.l_nWidth, "N", Round(.nTimeTransition / Alen(.aQueryBar,1),0), "Linear")
	            If Type("This.aQueryBar[m.nIter+1, 1]")<>"U"
	         		l_oTransit.OnCompleted(This,"CreateBar("+ Transform(m.nIter+1) +","+ Strtran(Transform(m.l_nLeft+m.l_nWidth),',','.') +")")
	            Else
	            	l_oTransit.OnCompleted(This,"Draw_Value()")
	            EndIf
	            l_oTransit.Start(.bWaitTransition)
			Else
				l_oObj.Move(l_oObj.Left,l_oObj.Top,m.l_nWidth)
				If Type("This.aQueryBar[m.nIter+1, 1]")<>"U"
					.CreateBar(m.nIter+1 , m.l_nLeft+m.l_nWidth)
				Else
					.Draw_Value()
				EndIf
			EndIf
			.ProgressBar.zOrder(0)
		EndWith
	Endproc
	
	Procedure NextWindowShape(l_i,l_LastStep,oObject)
		With This
			Local l_nGap,l_nDelta, l_value, nWidth, l_ProgressHeight
			l_nGap = Round(Int((.MainBar.Width-.MainBar.Left)/.nMaxWindowsShapes)+iif(.bCurved, 15/.nMaxWindowsShapes, 0), 0)
			l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
			l_value = Min(MAX(.nValue - .Min, 0)/m.l_nDelta,1)
			If Type("m.oObject")="O"
				l_oObjShape = m.oObject
                m.nWidth = Min(m.l_nGap,(.MainBar.Width*l_value)-m.l_nGap*(.nWindowsShapes-1))
                m.nWidth = Max(Round(m.nWidth, 0), 0)
                l_LastStep = .ProgressBar.Left + m.l_nGap*(m.l_i-1)
                m.l_oObjShape.nFull = Int((m.nWidth/m.l_nGap)*100)
			Else
            	Local l_num, l_oObjShape,l_oTransit
                l_num = Alltrim(Str( m.l_i))
                .AddObject("oShapeWindows_"+m.l_num,"Shape")
                l_oObjShape = .oShapeWindows_&l_num
                l_oObjShape.Visible=.F.
                l_ProgressHeight = (.nProgressBarHeight/100) * .nBarHeight
                l_oObjShape.Move(m.l_LastStep,.ProgressBar.Top,0, Iif(!Empty(.aQueryBar[1,1]),.nBarHeight/2, l_ProgressHeight ) )
                l_oObjShape.Top= .MainBar.Top+(.MainBar.Height-m.l_oObjShape.Height)/2
                l_oObjShape.BackColor=.ProgressBar.BackColor
                l_oObjShape.BorderColor=.MainBar.BackColor
                l_oObjShape.DrawMode = .ProgressBar.DrawMode
                m.nWidth = Min(m.l_nGap,(.MainBar.Width*m.l_value)-m.l_nGap*(m.l_i-1))
                m.nWidth = Max(Round(m.nWidth, 0), 0)
                *-- Salvo la proporzione tra la massima dimensione dello shape e la sua effettiva Width
                AddProperty(m.l_oObjShape,"nFull",Int((m.nWidth/m.l_nGap)*100))
                l_oObjShape.Visible= .T.
                .nWindowsShapes = .nWindowsShapes+1
            EndIf
            If !.bTransition
                l_oObjShape.Move(m.l_oObjShape.Left,m.l_oObjShape.Top,m.nWidth)
                If (1/.nMaxWindowsShapes)*(m.l_i) < m.l_value
	                .NextWindowShape(m.l_i+1, m.l_LastStep + m.nWidth)
                EndIf
            Else
                *-- Transizione dello shape corrente
	            l_oTransit = Createobject("cp_TransitionManager")
	            l_oTransit.Add(l_oObjShape, "Width",m.nWidth, "N", Round(.nTimeTransition / .nMaxWindowsShapes,0), "Linear")
	            If (1/.nMaxWindowsShapes)*(m.l_i) < m.l_value
	            	l_oTransit.OnCompleted(This,"NextWindowShape("+ Alltrim(Str(m.l_i+1)) +","+ Alltrim(Str(m.l_LastStep + m.nWidth)) +")")
	            Else
	            	l_oTransit.OnCompleted(This,"OnCompletedTransition()")
	            EndIf
	            l_oTransit.Start(.bWaitTransition)
            EndIf
            l_oObjShape.zOrder(0)
        Endwith
    EndProc

	Procedure OnCompletedTransition()
		This.Resize()
   		Thisform.NotifyEvent("BarUpdated")
		This.bOnBarTransition = .F.
    EndProc

    Proc Resize()
        With This
        	Local l_nDelta,l_nVal, l_ProgressHeight
            l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
            l_nVal = Min(Max(.nValue - .Min,0)/m.l_nDelta,1)
            l_nVal = Nvl(l_nVal,0)
            .MainBar.Move(0,(.Height-.MainBar.Height)/2,.Width,.nBarHeight)
            l_ProgressHeight = (.nProgressBarHeight/100) * .nBarHeight
            .ProgressBar.Move(.MainBar.Left,.ProgressBar.Top,Min(Int(.MainBar.Width*l_nVal),.MainBar.Width), l_ProgressHeight)

            *-- La Top viene modificata dopo per rispettare le modifiche della Height
            .ProgressBar.Top = .MainBar.Top+(.MainBar.Height-.ProgressBar.Height)/2
            .ColorShape.Move(.ProgressBar.Left,.ProgressBar.Top,.ProgressBar.Width,.ProgressBar.Height)
            .oEventShape.Move(.MainBar.Left,.MainBar.Top,.MainBar.Width,.MainBar.Height)

			*-- Resize degli shape e delle loro label
            If Type("This.oShape_1")='O'
                Local l_olderr, bErr, l_Count,l_cNum, l_Width,l_oShape
                l_Count=1
                l_olderr=On('error')
                bErr=.F.
                l_LastLeft = .MainBar.Left
                l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
                On Error bErr=.T.
                Do While Not bErr
                    l_cNum=Alltrim(Str(l_Count))
                    l_oShape=.oShape_&l_cNum
                    l_oPerCent=.lPerCent_&l_cNum
                    If Not bErr
                    	l_LastLeft = Max(m.l_LastLeft-Iif(.bCurved,20,0),0)
                        l_nVal = Min(Max(.aQueryBar[l_count,1] - .Min,0)/m.l_nDelta,1)
                        l_Width = Max(Round(.Width*l_nVal,0)-l_LastLeft,0)
						If .nSourceType = 2
							l_Width = INT(.width * m.l_nVal)
							If Type("This.aQueryBar[m.l_Count+1, 1]")="U"
								l_Width = .width - m.l_LastLeft 
							endIf		
						Else
							l_Width = INT(.width * m.l_nVal) - m.l_LastLeft 
							If Type("This.aQueryBar[m.l_Count+1, 1]")="U" And .bCurved
								l_Width = .width - m.l_LastLeft 
							endIf
						EndIf
                        l_oShape.Move(Max(l_LastLeft,0),.MainBar.Top,Min(l_Width,.Width))
						If Bitand(.nBookMark,2)=2
							*-- Percentuale visible dentro lo shape
							l_oPerCent.Move(Max(l_LastLeft+(l_Width-Iif(.bCurved,10,0)-l_oPerCent.Width)/2,0), l_oShape.Top + Max((l_oShape.Height-l_oPerCent.Height)/2,0))
						Else
							*-- Percentuale visible sopra lo shape
							l_oPerCent.Move(Max(Min(l_LastLeft+l_Width-Iif(.bCurved,10,0)-(l_oPerCent.Width/2),.Width-l_oPerCent.Width),0), l_oShape.Top-l_oPerCent.Height)
						EndIf
                        l_LastLeft = l_LastLeft + l_Width
                        l_Count=l_Count+1
                    Endif
                EndDo
                On Error &l_olderr
            EndIf
            
            *-- Resize degli Shape con nBarVersion = 2
            If Type("This.oShapeWindows_1")='O'
                Local l_olderr, bErr, l_Count,l_cNum, l_oShape, l_nGap, nWidth, nLeft
                l_Count=1
                nLeft = .MainBar.Left
                l_olderr=On('error')
                bErr=.F.
                On Error bErr=.T.
                Do While Not bErr
                    l_cNum=Alltrim(Str(l_Count))
                    l_oShape = .oShapeWindows_&l_cNum
                    If !bErr
	              		l_nGap = Round(Int((.MainBar.Width-.MainBar.Left)/.nMaxWindowsShapes)+iif(.bCurved, 15/.nMaxWindowsShapes, 0), 0)
		                *-- In base alla proporzione gestisco la Resize
		                If l_oShape.nFull=100
			            	nWidth = l_nGap
			        	Else
		            		nWidth = Int(l_nGap*(l_oShape.nFull/100))
                        EndIf
	                	l_oShape.Move(m.nLeft, .ProgressBar.Top, m.nWidth, Iif(!Empty(.aQueryBar[1,1]),.nBarHeight/2, l_ProgressHeight ))
	                	l_oShape.Top= .MainBar.Top+(.MainBar.Height-l_oShape.Height)/2
	                	l_oShape.zOrder(0)
	                	l_Count=l_Count+1
	                	m.nLeft =  m.nLeft+m.nWidth
                    Endif
                Enddo
                On Error &l_olderr
            Endif
            *-- Resize di PerCentLbl
            .PerCentLbl_Resize()
            .Visible = .Visible Or .bVisibleOnResize
        Endwith
    Endproc

    *-- Posizionamento della PerCentLbl in base al tipo di barra
    Proc PerCentLbl_Resize()
        With This
        	.PerCentLbl.Width = cp_LabelWidth2(.PerCentLbl.Caption, .PerCentLbl.FontName, .PerCentLbl.FontSize)
        	.Symbol.FontSize = .PerCentLbl.FontSize
        	.Symbol.Width = cp_LabelWidth2(.Symbol.Caption, .Symbol.FontName, .Symbol.FontSize)
            If Empty(.aQueryBar[1,1])
				Do Case
				    Case .nAlignPerCent = 4
						*-- Allineato a destra della mainbar
						.PerCentLbl.Move(.MainBar.Left+.MainBar.Width-.PerCentLbl.Width-.Symbol.Width,.MainBar.Top+(.MainBar.Height-.PerCentLbl.Height)/2)
						.PerCentLbl.Alignment = 2
				    Case .nAlignPerCent = 3
						*-- Allineato al centro della mainbar
						.PerCentLbl.Move(.MainBar.Left+(.MainBar.Width-.PerCentLbl.Width)/2,.MainBar.Top+(.MainBar.Height-.PerCentLbl.Height)/2)
						.PerCentLbl.Alignment = 2
			    	Case .nAlignPerCent = 2
						*-- Allineato al centro della progress
						.PerCentLbl.Visible = !Empty(.cValBar) And (.ProgressBar.Width >= 32)
						.PerCentLbl.Move(.ProgressBar.Left+(.ProgressBar.Width-.PerCentLbl.Width)/2,.MainBar.Top+(.MainBar.Height-.PerCentLbl.Height)/2)
						.PerCentLbl.Alignment = 2
				    Case .nAlignPerCent = 1
						*-- Allineato a destra della progress
						.PerCentLbl.Visible = !Empty(.cValBar) And (.ProgressBar.Width >= 32)
						.PerCentLbl.Move(.ProgressBar.Left+.ProgressBar.Width-.PerCentLbl.Width-.Symbol.Width,.MainBar.Top+(.MainBar.Height-.PerCentLbl.Height)/2)
						.PerCentLbl.Alignment = 1
				    Case .nAlignPerCent = 0
						*-- Allineato a sinistra della progress
						.PerCentLbl.Visible = !Empty(.cValBar) And (.ProgressBar.Width >= 32)
						.PerCentLbl.Move(.ProgressBar.Left,.MainBar.Top+(.MainBar.Height-.PerCentLbl.Height)/2)
						.PerCentLbl.Alignment = 0
			    Endcase
			Else
				Do Case
				    Case .nAlignPerCent = 4
						*-- Allineato a destra della mainbar
						.PerCentLbl.Move(.MainBar.Left+.MainBar.Width-.PerCentLbl.Width-.Symbol.Width,.MainBar.Top+.MainBar.Height+5)
						.PerCentLbl.Alignment = 2
				    Case .nAlignPerCent = 3
						*-- Allineato al centro della mainbar
						.PerCentLbl.Move(.MainBar.Left+(.MainBar.Width-.PerCentLbl.Width)/2,.MainBar.Top+.MainBar.Height+5)
						.PerCentLbl.Alignment = 2
				    Case .nAlignPerCent = 2
						*-- Allineato al centro della progress
						.PerCentLbl.Move(.ProgressBar.Left+(.ProgressBar.Width-.PerCentLbl.Width)/2,.MainBar.Top+.MainBar.Height+5)
						.PerCentLbl.Alignment = .nAlignPerCent
				    Case .nAlignPerCent = 1
						*-- Allineato a destra della progress
						.PerCentLbl.Move(.ProgressBar.Left+.ProgressBar.Width-.PerCentLbl.Width-.Symbol.Width,.MainBar.Top+.MainBar.Height+5)
						.PerCentLbl.Alignment = 1
				    Case .nAlignPerCent = 0
						*-- Allineato a sinistra della progress
						.PerCentLbl.Move(.ProgressBar.Left,.MainBar.Top+.MainBar.Height+5)
						.PerCentLbl.Alignment = .nAlignPerCent
				Endcase
            Endif
		    .Symbol.Visible = .PerCentLbl.Visible
		    .Symbol.Move(.PerCentLbl.Left+.PerCentLbl.Width,.PerCentLbl.Top)
        Endwith
    Endproc

    *-- Modifica al colore delle ProgressBar
    Proc nProgressBarColor_Assign(nValue)
        With This
            .nProgressBarColor = nValue
            .ProgressBar.BackColor=.nProgressBarColor
        Endwith
    Endproc

    Proc nProgressBarBorderColor_Assign(nValue)
        With This
            .nProgressBarBorderColor = nValue
            .ProgressBar.BorderColor=.nProgressBarColor
        Endwith
    Endproc

    *-- Modifica al colore delle barre principali
    Proc nMainBarColor_Assign(nValue)
        With This
            .nMainBarColor = nValue
            .MainBar.BackColor=.nMainBarColor
        Endwith
    Endproc

    Proc nMainBarBorderColor_Assign(nValue)
        With This
            .nMainBarBorderColor = nValue
            .MainBar.BorderColor=.nMainBarBorderColor
        Endwith
    Endproc

    Proc nMainBarBackstyle_Assign(nValue)
        With This
            .nMainBarBackstyle = nValue
            .MainBar.BackStyle=.nMainBarBackstyle
        Endwith
    Endproc

    *-- Colore BookMarks
    Proc nBookMarksFontColor_Assign(nValue)
        With This
            .nBookMarksFontColor = m.nValue
            
	    	Local l_Count, l_Index
		    l_Count=AMembers(aObjects, This,2)
		    For l_Index=1 to l_count
		        If Lower(Left(aObjects[m.l_Index], 9)) = "lpercent_"
		            .&aObjects[m.l_Index]..ForeColor = m.nValue
		        EndIf
		    EndFor
	    Endwith
    EndProc
    
    *-- Dimensione BookMarks
    Proc nBookMarksFontSize_Assign(nValue)
        With This
            .nBookMarksFontSize= m.nValue

	    	Local l_Count, l_Index
		    l_Count=AMembers(aObjects, This,2)
		    For l_Index=1 to l_count
		        If Lower(Left(aObjects[m.l_Index], 9)) = "lpercent_"
		            .&aObjects[m.l_Index]..FontSize = m.nValue
		        EndIf
		    EndFor
	    Endwith
    EndProc
    
    *-- Font BookMarks
    Proc cBookMarksFontName_Assign(cValue)
        With This
            .cBookMarksFontName = m.cValue

	    	Local l_Count, l_Index
		    l_Count=AMembers(aObjects, This,2)
		    For l_Index=1 to l_count
		        If Lower(Left(aObjects[m.l_Index], 9)) = "lpercent_"
		            .&aObjects[m.l_Index]..FontName = m.cValue
		        EndIf
		    EndFor
	    Endwith
    EndProc


    *-- Allineamento della percentuale
    Proc nAlignPerCent_Assign(nValue)
        With This
            .nAlignPerCent = nValue
			.PerCentLbl_Resize()
		Endwith
	Endproc

    Proc oEventShape.MouseMove()
        Lparameters nButton, nShift, nXCoord, nYCoord
        Local oOldCtrl, l_cObj, l_nOldNum, l_nNum
        With This.Parent
            oOldCtrl= .oCtrlMouse
            *Thisform.LockScreen=.T.
            This.Visible=.F.
            .oCtrlMouse = Sys(1270)
            This.Visible=.T.
            This.ZOrder(0)
            If Type("This.Parent.oCtrlMouse")='O'
                Do While Type("This.Parent.oCtrlMouse")='O' And Type("This.Parent.oCtrlMouse.Parent")='O' And Lower(Left(.oCtrlMouse.Name,7))#"oshape_" And Lower(Left(.oCtrlMouse.Name,9))#"lpercent_"
                    .oCtrlMouse = .oCtrlMouse.Parent
                Enddo
                l_nOldNum=Iif(Type("oOldCtrl")='O' And Not Isnull(oOldCtrl),Int(Val(Substr(oOldCtrl.Name,At("oShape_",oOldCtrl.Name)+7,Len(oOldCtrl.Name)))+Val(Substr(oOldCtrl.Name,At("lPerCent_",oOldCtrl.Name)+9,Len(oOldCtrl.Name)))),0)
                l_nNum=Int(Val(Substr(.oCtrlMouse.Name,At("oShape_",.oCtrlMouse.Name)+7,Len(.oCtrlMouse.Name)))+Val(Substr(.oCtrlMouse.Name,At("lPerCent_",.oCtrlMouse.Name)+9,Len(.oCtrlMouse.Name))))
                *-- Se si � cambiato oggetto
                If Not Empty(l_nNum) And (l_nOldNum <> l_nNum Or .bLeftObj)
                    .bLeftObj=.F.
                    If Lower(Left(.oCtrlMouse.Name,7))="oshape_" Or Lower(Left(.oCtrlMouse.Name,9))="lpercent_"
                        If Type("ThisForm.oBalloon")='O' And Thisform.oBalloon.ctlVisible
                            Thisform.oBalloon.ctlhide(2)
                            Thisform.oBalloon.ctlVisible = .F.
                        Endif
                        If Type("This.Parent.oTimer")="O" And Not Isnull(.oTimer)
                            .oTimer.Enabled = .F.
                        Endif
                        l_cObj=Int(Val(Iif(Lower(Left(.oCtrlMouse.Name,7))="oshape_",Substr(.oCtrlMouse.Name,At("oShape",.oCtrlMouse.Name)+7,Len(.oCtrlMouse.Name)),Substr(.oCtrlMouse.Name,At("lPerCent_",.oCtrlMouse.Name)+9,Len(.oCtrlMouse.Name)))))
                        .cToolTip = .aQueryBar[l_cObj,4]
                        .oTimer = Createobject("ProgBarBalloonTimer", Thisform, .cToolTip, .oCtrlMouse)
                    Else
                        If Type("ThisForm.oBalloon")='O' And Thisform.oBalloon.ctlVisible
                            Thisform.oBalloon.ctlhide(2)
                            Thisform.oBalloon.ctlVisible = .F.
                        Endif
                        .oTimer = .Null.
                    Endif
                Endif
            Endif
            *Thisform.LockScreen=.F.
        Endwith
    Endproc

    Procedure oEventShape.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This.Parent
            .bLeftObj=.T.
            .oTimer = .Null.
            If Type("ThisForm.oBalloon")='O' And Thisform.oBalloon.ctlVisible
                Thisform.oBalloon.ctlhide(2)
                Thisform.oBalloon.ctlVisible = .F.
            Endif
            *Thisform.LockScreen=.T.
            This.ZOrder(0)
            This.Visible = .T.
            *Thisform.LockScreen = .F.
        Endwith
    Endproc

    Proc Destroy()
        Unbindevents(Thisform,"BackColor",This,"ModifyBackColor")
        DoDefault()
    Endproc

Enddefine

Define Class MarqueeShapes As Container

    Width=60
    Height=20

    Proc Init()
        With This
            *-- Barra tratteggiata
            Local l_LastStep,l_nGap,nWidth,l_oObjShape
            l_i=1
            l_LastStep=0
            Do While l_i < 4
                l_num = Alltrim(Str( l_i))
                .AddObject("oShapeWindows_"+l_num,"Shape")
                l_oObjShape = .oShapeWindows_&l_num
                l_oObjShape.Visible=.F.
                l_oObjShape.Move(l_LastStep,0,0,.Height)
                l_oObjShape.BackColor=Rgb(0,200,62)
                l_oObjShape.BorderColor=Rgb(255,255,255)
                l_oObjShape.Visible= .T.
                m.nWidth = .Width/3
                l_oObjShape.Move(l_oObjShape.Left,l_oObjShape.Top,m.nWidth)
                l_LastStep = l_LastStep + m.nWidth
                l_i = l_i+1
            Enddo
        Endwith
    Endproc

Enddefine

*-- Barra di caricamento
Define Class cp_MarqueeBar As cp_HorizontalBar

    nTime=3600

    oTransit = .Null.
    bFirstTransition= .T.
    bExitCondition=.F. &&Condizione di uscita dalla transizione (pu� essere il nome della variabile pubblica di condizione)
    bInterrupted=.F. &&Indica se la transizione � stata interrotta

    Add Object LoadBar As MarqueeShapes With Width=60,Height=20

    Proc Init()
        With This
            DoDefault()
            .ProgressBar.Visible=.F.
            .PerCentLbl.Visible=.F.
            .Symbol.Visible=.F.
            .bCurved = .F.
            .setColor()
        Endwith
    Endproc

    *-- Tempo di caricamento infinito
    Proc InfiniteLoading()
        With This
            Local l_i, cParam
            .oTransit = Createobject("cp_TransitionManager")
            m.cParam = Transform(.MainBar.Width+.LoadBar.Width)+","+Transform(.MainBar.Left-.LoadBar.Width)+","+Transform(.MainBar.Width)
            .oTransit.Add(This,"LoadMove",cParam,"M",.nTime*1000, "Linear")
            *-- Decremento il riferimento alla maschera alla prima esecuzione della transizione
            If .bFirstTransition
                cb_DecrementRef(This)
                .bFirstTransition = .F.
            Endif
            *-- non rimangono appesi gli oggetti della cp_TransitionManager (prima di tutto il timer)
            .oTransit.Start(.F.)
        Endwith
    Endproc

    Proc Resize()
        With This
            DoDefault()
            .LoadBar.Move(.MainBar.Left,.MainBar.Top,.LoadBar.Width,.MainBar.Height)
        Endwith
    Endproc

    Proc setColor()
        With This
            DoDefault()
            
			Local l_Count, l_Index
			l_Count=AMembers(aObjects, This.LoadBar,2)
			For l_Index=1 to l_count
			    If Lower(Left(aObjects[m.l_Index], 14)) = "oshapewindows_"
			        .LoadBar.&aObjects[m.l_Index]..BackColor = .ProgressBar.BackColor
			        .LoadBar.&aObjects[m.l_Index]..BorderColor = .MainBar.BackColor
			    EndIf
			EndFor
        Endwith
    Endproc

    *-- Modifica al colore delle barre
    Proc nProgressBarColor_Assign(nValue)
        With This
            DoDefault(nValue)
            .setColor()
        Endwith
    Endproc

    *-- Modifica al colore della barra principale
    Proc nMainBarColor_Assign(nValue)
        With This
            DoDefault(nValue)
            .setColor()
        Endwith
    Endproc

    *-- Procedura della transizione, muove il contenitore lungo la barra in base alla lunghezza
    Proc LoadMove(oTrs,nPercentage, nLeft, nBackLeft, nWidth, bExit)
        With This
            Local nSingleTrs,nTimeSection, SectionPerCentage
            If (Type("This.bExitCondition")="L" And Not .bExitCondition) Or Not Evaluate(.bExitCondition)
                *-- Esecuzione della transizione
                *-- Tempo della singola transizione, arbitrariamente uguale a  m.nWidth*2.5
                m.nSingleTrs = m.nWidth*2.5
                *-- Sezione
                m.nTimeSection = Int(oTrs.nTransitionTime*nPercentage / m.nSingleTrs)
                m.SectionPerCentage = Mod(oTrs.nTransitionTime*nPercentage,m.nSingleTrs)/m.nSingleTrs
                If Mod(m.nTimeSection,2)=0
                    *-- Spostamento in avanti
                    .LoadBar.Left = oTrs.Interpolate(m.nBackLeft,m.nLeft,m.SectionPerCentage)
                Else
                    *-- Spostamento indietro
                    .LoadBar.Left = oTrs.Interpolate(m.nLeft,m.nBackLeft,m.SectionPerCentage)
                Endif
            Else
                *-- Si � verificata la condizione di uscita
                oTrs.bCompleted=.T.
                .bInterrupted=.T.
            Endif
        Endwith
    Endproc

    Proc Destroy()
        This.oTransit=.Null.
    Endproc

Enddefine


Define Class cp_CircleBar As cp_ProgressBar

	nProgressBarHeight = 50
    nPerCentFontSize = 15

    Add Object MainCircle As CircleShape With StartPoint=0, FinalPoint=360,Left=0,Top=5,Width=140,Height=140
    Add Object ProgCircle As CircleShape With StartPoint=0, FinalPoint=0,Left=0,Top=5,Width=140,Height=140
    Add Object FrontCircle As CircleShape With StartPoint=0, FinalPoint=360,Left=0,Top=5,Width=100,Height=100
    Add Object PerCentLbl As Label With Width=350,Alignment=2,Caption="0",BackStyle=0
    Add Object Symbol As Label With Width=4,Alignment=2,BackStyle=0, Caption=""

    Proc Init()
        With This
            DoDefault()
            .Symbol.Caption = Iif( Not(.cValBar=='V'),"%",.cSymbol)
            .nProgressBarColor=Iif(.nProgressBarColor<>-1,.nProgressBarColor,Rgb(255,130,0))
            .nProgressBarBorderColor=Iif(.nProgressBarBorderColor<>-1,.nProgressBarBorderColor,.nProgressBarColor)
            .nMainBarColor=Iif(.nMainBarColor<>-1,.nMainBarColor,RGBAlphaBlending(.nProgressBarColor, Thisform.BackColor, 0.6))
            .nMainBarBorderColor=Iif(.nMainBarBorderColor<>-1,.nMainBarBorderColor,.nMainBarColor)
            .FrontCircle.BackColor=Thisform.BackColor
            .FrontCircle.BorderColor=.FrontCircle.BackColor
            .Resize()
        Endwith
    Endproc
    
	PROCEDURE Draw()
		With this
            Local l_nWidth, l_nDelta, l_value
			IF Type("this.nValue")='N'
				l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
				l_value = Min(MAX(.nValue - .Min, 0)/m.l_nDelta,1)
                *-- Barra Circolare
                If !.bTransition
                    .ProgCircle.FinalPoint=Round(l_value*360,0)
                    Thisform.NotifyEvent("BarUpdated")
                Else
                    l_oTransit = Createobject("cp_TransitionManager")
                    *--- Sposto la barra con la transizione
                    l_oTransit.Add(.ProgCircle, "FinalPoint", Round(l_value*360,0), "N", .nTimeTransition, "Linear")
                    l_oTransit.OnCompleted(This, "OnCompletedTransition()")
                    l_oTransit.Start(.bWaitTransition)
                EndIf
				.PerCentLbl.Caption = Iif( Not(.cValBar=='V'),Alltrim(Transform(l_value*100,.cStringFormat)), Alltrim(Transform(.nValue,.cStringFormat)))
			EndIf
		ENDWITH
	EndProc
	
    Proc Resize()
        With This
            Local l_Offset, nWidth
            nWidth = Min(.Width,.Height)
            *-- La grandezza della barra dipende da ci� che non viene coperto dalla FrontCircle
            l_Offset = 1 - (.nProgressBarHeight/100)
        	.MainCircle.Move(.MainCircle.Left,.MainCircle.Top,m.nWidth,m.nWidth)
            .MainCircle.Top = (.Height-.MainCircle.Height)/2
            .MainCircle.Left = (.Width-.MainCircle.Width)/2
            .ProgCircle.Move(.MainCircle.Left,.MainCircle.Top,.MainCircle.Width,.MainCircle.Height)
            .FrontCircle.Move(.FrontCircle.Left,.FrontCircle.Top,Int(.MainCircle.Width*l_Offset),Int(.MainCircle.Height*l_Offset))
            .FrontCircle.Left = .MainCircle.Left+Int((.MainCircle.Width-.FrontCircle.Width)/2)
            .FrontCircle.Top =.MainCircle.Top+Int((.MainCircle.Height-.FrontCircle.Height)/2)
            *-- Resize di PerCentLbl a parte
            .PerCentLbl_Resize()
            .Visible = .Visible Or .bVisibleOnResize
        Endwith
    Endproc

    *-- Posizionamento della PerCentLbl in base al tipo di barra
    Proc PerCentLbl_Resize()
        With This
            .PerCentLbl.Move(.FrontCircle.Left+Int((.FrontCircle.Width-.PerCentLbl.Width-.Symbol.Width)/2),.FrontCircle.Top+Int((.FrontCircle.Height-.PerCentLbl.Height)/2),cp_LabelWidth2(.PerCentLbl.Caption, .PerCentLbl.FontName, .PerCentLbl.FontSize))
            .Symbol.FontSize = .PerCentLbl.FontSize
            .Symbol.Move(.PerCentLbl.Left+.PerCentLbl.Width,.PerCentLbl.Top,cp_LabelWidth2(.Symbol.Caption, .Symbol.FontName, .Symbol.FontSize))
            .PerCentLbl.ZOrder(0)
            .Symbol.ZOrder(0)
        Endwith
    Endproc
    
    *-- Modifica al colore delle ProgressBar
    Proc nProgressBarColor_Assign(nValue)
        With This
            .nProgressBarColor = nValue
            .ProgCircle.BackColor=.nProgressBarColor
            .ProgCircle.BorderColor=.nProgressBarColor
        Endwith
    Endproc

    *-- Modifica al colore delle barre principali
    Proc nMainBarColor_Assign(nValue)
        With This
            .nMainBarColor = nValue
            .MainCircle.BackColor=.nMainBarColor
        Endwith
    Endproc

    Proc nMainBarBorderColor_Assign(nValue)
        With This
            .nMainBarBorderColor = nValue
            .MainCircle.BorderColor=.nMainBarBorderColor
        Endwith
    Endproc

    Proc nMainBarBackstyle_Assign(nValue)
        With This
            .nMainBarBackstyle = nValue
            .MainCircle.BackStyle=.nMainBarBackstyle
        Endwith
    Endproc

    *-- Modifico colore degli Shape in base al colore della Form
    Proc nFormColor_Assign(nValue)
        With This
            .nFormColor = nValue
            .FrontCircle.BackColor=.nFormColor
            .FrontCircle.BorderColor=.FrontCircle.BackColor
        Endwith
    Endproc

Enddefine


Define Class cp_GaugeBar As cp_ProgressBar

    nProgressBarHeight = 33
    nGaugeShapes = 1 &&Numero degli Shape in cui si vuole dividere la barra gauge
    Old_NumShape =1
    
    bOnBarTransition = .F.
    nPerCentFontSize = 20

    Add Object MainCircleGauge As CircleShape With StartPoint=-135, FinalPoint=135,Left=0,Top=5,Width=140,Height=140
    Add Object FrontCircleGauge As CircleShape With StartPoint=0, FinalPoint=360,Left=0,Top=5,Width=60,Height=60
    Add Object PerCentLbl As Label With Width=350,Alignment=2,Caption="0",BackStyle=0
    Add Object Symbol As Label With Width=4,Alignment=2,BackStyle=0, Caption=""

    Proc Init()
        With This
            DoDefault()
            .Symbol.Caption = Iif( Not(.cValBar=='V'),"%",.cSymbol)
            .nMainBarColor = Iif(.nMainBarColor <> -1,.nMainBarColor ,RGBAlphaBlending(Rgb(95,158,160) ,Thisform.BackColor,0.6))
            .nMainBarBorderColor = Iif(.nMainBarBorderColor <> -1,.nMainBarBorderColor,.nMainBarColor)
            .nProgressBarColor = Iif(.nProgressBarColor <> -1,.nProgressBarColor,Thisform.BackColor)
            .nProgressBarBorderColor = Thisform.BackColor
            *-- La TargetLine rimane sotto a tutto, ristabilisco l'ordine
            .FrontCircleGauge.ZOrder(0)
            .PerCentLbl.ZOrder(0)
            .Symbol.ZOrder(0)
            .Resize()
        Endwith
    Endproc

	PROCEDURE Draw()
		With this
            Local l_nWidth, l_nDelta, l_value, l_bInNext
			IF Type("this.nValue")='N' And !.bOnBarTransition
				l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
				l_value = Min(MAX(.nValue - .Min, 0)/m.l_nDelta,1)
				*-- Finche non termino la transizione blocco tutte le prossime
				.bOnBarTransition = .bTransition
				If .Old_NumShape == .nGaugeShapes And .nGaugeShapes=1 And Type("This.oShapeGauge_1")='O'
	                *-- Se il numero di shape non � variato, non distruggo quello esistente ma lo allungo
	                l_oObjShape = .oShapeGauge_1
	                If !.bTransition
	                    l_oObjShape.FinalPoint = Min(270,Round(l_value*270,0))-135
	                Else
	                    *-- Transizione dello shape gi� presente
	                    l_oTransit = Createobject("cp_TransitionManager")
	                    l_oTransit.Add(l_oObjShape, "FinalPoint",Min(270,Round(l_value*270,0))-135, "N", .nTimeTransition, "Linear")
	                    *-- Sblocco transizioni
						l_oTransit.OnCompleted(This, "TransitionEnd()")
                    	l_oTransit.Start(.bWaitTransition)
	                Endif
                Else
                	l_bInNext = .T.
                    .Old_NumShape = .nGaugeShapes
                    l_i=1
                    l_nGap = 270 / .nGaugeShapes
					.NextShape(l_i,l_nGap)
                Endif
            	.PerCentLbl.Caption = Iif( Not(.cValBar=='V'),Alltrim(Transform(l_value *100,.cStringFormat)), Alltrim(Transform(.nValue,.cStringFormat)))
            	.PerCentLbl_Resize()
            	Thisform.NotifyEvent("BarUpdated")
				If .bTransition And !m.l_bInNext
					*-- Se � attiva la transizione della NextShapes, la TransitionEnd la far� quest'ultimo
					.TransitionEnd()
                EndIf
            Endif
		ENDWITH
	EndProc

	Proc NextShape(l_i,l_nGap)
        With This
        	Local l_num, l_oObjShape, l_oTransit,l_nDelta, l_value
			l_num = Alltrim(Str( m.l_i))
			l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
			l_value = Min(MAX(.nValue - .Min, 0)/m.l_nDelta,1)
            Thisform.LockScreen =.T.
            .AddObject("oShapeGauge_"+m.l_num,"CircleShape")
            .FrontCircleGauge.ZOrder(0)
            .PerCentLbl.ZOrder(0)
            .Symbol.ZOrder(0)
            Thisform.LockScreen =.F.
	        l_oObjShape = .oShapeGauge_&l_num
	        l_oObjShape.Visible=.F.
	        l_oObjShape.Move(.MainCircleGauge.Left,.MainCircleGauge.Top,.MainCircleGauge.Width,.MainCircleGauge.Height)
	        l_oObjShape.BackColor=.nProgressBarColor
	        l_oObjShape.BorderColor=Thisform.BackColor
	        l_oObjShape.StartPoint = (m.l_nGap * (m.l_i-1))-135
	        l_oObjShape.FinalPoint = m.l_oObjShape.StartPoint
	        l_oObjShape.Visible= .T.
	        If !.bTransition
	            l_oObjShape.FinalPoint = Min(m.l_nGap * l_i,Round(m.l_value*270,0))-135
	            If (1/.nGaugeShapes)*(m.l_i) < l_value
	                .NextShape(m.l_i+1,m.l_nGap)
                Endif
            Else
                *-- Transizione dello shape corrente
                l_oTransit = Createobject("cp_TransitionManager")
                l_oTransit.Add(l_oObjShape, "FinalPoint",Min(l_nGap * m.l_i,Round(m.l_value*270,0))-135, "N", Round(.nTimeTransition / .nGaugeShapes,0), "Linear")
	            If (1/.nGaugeShapes)*(m.l_i) < m.l_value
	            	l_oTransit.OnCompleted(This,"NextShape("+Alltrim(Str(m.l_i+1))+","+Alltrim(Str(m.l_nGap))+")")
	            Else
	            	*-- Sblocco transizioni
	            	l_oTransit.OnCompleted(This,"TransitionEnd()")
	            EndIf
            	l_oTransit.Start(.bWaitTransition)
            Endif
        Endwith
    EndProc
    
    Procedure TransitionEnd()
    	This.bOnBarTransition = .F.
    EndProc

	Procedure RemoveObjects()
        *-- Distruggo i vecchi shape creati
        If !This.bOnBarTransition
		    Local l_Count, l_Index
		    l_Count=AMembers(aObjects, this,2)
		    For l_Index=1 to l_count
		        If Lower(Left(aObjects[m.l_Index], 12)) = "oshapegauge_"
		            This.RemoveObject(aObjects[m.l_Index])
		        EndIf 
		    EndFor
	    EndIf
	EndProc

    Proc Resize()
        With This
            Local l_Offset, nWidth
            nWidth = Min(.Width,.Height)
            *-- La grandezza della barra dipende da ci� che non viene coperto dalla FrontCircleGauge
            l_Offset = 1 - (.nProgressBarHeight/100)
            .MainCircleGauge.Move(.MainCircleGauge.Left,.MainCircleGauge.Top, m.nWidth-6, m.nWidth-6)
            .MainCircleGauge.Left = (.Width-.MainCircleGauge.Width)/2
            .MainCircleGauge.Top = (.Height-.MainCircleGauge.Height)/2
            .FrontCircleGauge.Move(.FrontCircleGauge.Left,.FrontCircleGauge.Top,Int(.MainCircleGauge.Width*l_Offset),Int(.MainCircleGauge.Height*l_Offset))
            .FrontCircleGauge.Left =.MainCircleGauge.Left+Int((.MainCircleGauge.Width-.FrontCircleGauge.Width)/2)
            .FrontCircleGauge.Top =.MainCircleGauge.Top+Int((.MainCircleGauge.Height-.FrontCircleGauge.Height)/2)

			Local l_Count, l_Index
			l_Count=AMembers(aObjects, This,2)
			For l_Index=1 to l_count
			    If Lower(Left(aObjects[m.l_Index], 12)) = "oshapegauge_"
			        .&aObjects[m.l_Index]..Move(.MainCircleGauge.Left,.MainCircleGauge.Top,.MainCircleGauge.Width,.MainCircleGauge.Height)
			    EndIf
			EndFor
            *-- Resize di PerCentLbl a parte
            .PerCentLbl_Resize()
            .Visible = .Visible Or .bVisibleOnResize
        Endwith
    Endproc

    *-- Posizionamento della PerCentLbl in base al tipo di barra
    Proc PerCentLbl_Resize()
        With This
            .PerCentLbl.Move(.MainCircleGauge.Left+Int((.MainCircleGauge.Width-.PerCentLbl.Width-.Symbol.Width)/2),.Height-.PerCentLbl.Height, cp_LabelWidth2(.PerCentLbl.Caption, .PerCentLbl.FontName, .PerCentLbl.FontSize))
            .Symbol.FontSize = .PerCentLbl.FontSize
            .Symbol.Move(.PerCentLbl.Left+.PerCentLbl.Width,.PerCentLbl.Top, cp_LabelWidth2(.Symbol.Caption, .Symbol.FontName, .Symbol.FontSize))
            .PerCentLbl.ZOrder(0)
            .Symbol.ZOrder(0)
        Endwith
    Endproc

    *-- Modifica al colore delle ProgressBar
    Proc nProgressBarColor_Assign(nValue)
        With This
            .nProgressBarColor = m.nValue
            .nProgressBarBorderColor = Thisform.BackColor
            
			Local l_Count, l_Index
			l_Count=AMembers(aObjects, This,2)
			For l_Index=1 to l_count
			    If Lower(Left(aObjects[m.l_Index], 12)) = "oshapegauge_"
			        .&aObjects[m.l_Index]..BackColor = .nProgressBarColor
			        .&aObjects[m.l_Index]..BorderColor = .nProgressBarBorderColor
			    EndIf
			EndFor
        Endwith
    Endproc

    *-- Modifica al colore delle barre principali
    Proc nMainBarColor_Assign(nValue)
        With This
            .nMainBarColor = nValue
            .MainCircleGauge.BackColor = .nMainBarColor
        Endwith
    Endproc

    Proc nMainBarBorderColor_Assign(nValue)
        With This
            .nMainBarBorderColor = nValue
            .MainCircleGauge.BorderColor = .nMainBarBorderColor
        Endwith
    Endproc

    Proc nMainBarBackstyle_Assign(nValue)
        With This
            .nMainBarBackstyle = nValue
            .MainCircleGauge.BackStyle=.nMainBarBackstyle
        Endwith
    Endproc

    *-- Modifico colore degli Shape in base al colore della Form
    Proc nFormColor_Assign(nValue)
        With This
            .nFormColor = nValue
            .FrontCircleGauge.BackColor=.nFormColor
            .FrontCircleGauge.BorderColor=.FrontCircleGauge.BackColor
        Endwith
    Endproc

Enddefine


*--Timer per il tooltip balloon
Define Class ProgBarBalloonTimer As Timer
    Enabled=.F.
    cBalloon=""
    nSec=1.2
    oForm = .Null.
    oCtrl = .Null.

    Proc Init(pParent, cString, oCtrl)
        *-- Vengono passati il riferimento alla Form, la stringa da visualizzare e il controllo su cui fare il ToolTip
        Local l_OldSetDate
        This.oForm= m.pParent
        This.oCtrl = m.oCtrl
        m.cString = Iif(Type('m.cString')='L', '', m.cString)
        This.cBalloon = Strtran(m.cString,"|",Chr(10))
        This.Interval=This.nSec*1000
        This.Enabled=.T.
    Endproc

    Proc Timer()
        Local l_MouseCtrl
        l_MouseCtrl = Sys(1270)
        *-- Verifico che il mouse sia ancora sull'oggetto che ha lanciato il timer
        If Type("m.l_MouseCtrl")='O' And !IsNull(m.l_MouseCtrl)
            If Type("This.oForm.oBalloon")='O' And Type("This.oCtrl")='O' And Not Isnull(This.oCtrl)
                This.oForm.oBalloon.ctltop = Objtoclient(This.oCtrl, 1) + This.oCtrl.Height
                This.oForm.oBalloon.ctlleft = Objtoclient(This.oCtrl, 2) + This.oCtrl.Width
                This.oForm.oBalloon.ctlShow(1 , This.cBalloon ,"", 0)
            Endif
        Endif
        This.oForm = .Null.
        This.oCtrl = .Null.
        l_MouseCtrl= .Null.
        This.Enabled=.F.
    Endproc
EndDefine


*-- Shape freccia SpeedMeter
Define Class ArrowShape As Shape

    Width = 200
    Height = 80
    BackColor = Rgb(255,255,255)
    nArrowAngle = 0
    nBaseRadius = 15

    Visible=.T.
    Dimension aPolyPoints[4,2]
    BorderWidth=1
    Proc Init()
        This._Draw()
    Endproc

    Proc _Draw(nAngle,nMiniRadius)
        With This

            Dimension .aPolyPoints[4,2]
            lnradius = 50
            m.nAngle = Iif(Type("m.nAngle")<>'N',.nArrowAngle,m.nAngle)
            m.nMiniRadius = Iif(Type("m.nMiniRadius")<>'N',.nBaseRadius,m.nMiniRadius)
            .nArrowAngle =  m.nAngle
            .nBaseRadius = m.nMiniRadius
            m.nAngle = m.nAngle-180&&-nMiniRadius-2 &&il -2 � arbitrario, preferito a -1 perch� funziona per i casi preferiti (10<nMinRadius<24)
            .aPolyPoints(1,1) = (lnradius * Cos(Dtor(m.nAngle)) + lnradius)
            .aPolyPoints(1,2) = (lnradius * Sin(Dtor(m.nAngle)) + lnradius)

            .aPolyPoints[2,1] = (m.nMiniRadius * Cos(Dtor(m.nAngle+90))+ lnradius)
            .aPolyPoints[2,2] = (m.nMiniRadius * Sin(Dtor(m.nAngle+90))+ lnradius)

            .aPolyPoints[3,1] = (m.nMiniRadius * Cos(Dtor(m.nAngle-90))+ lnradius)
            .aPolyPoints[3,2] = (m.nMiniRadius * Sin(Dtor(m.nAngle-90))+ lnradius)

            .aPolyPoints(4,1) = (lnradius * Cos(Dtor(m.nAngle)) + lnradius)
            .aPolyPoints(4,2) = (lnradius * Sin(Dtor(m.nAngle)) + lnradius)

            .Polypoints ="This.aPolyPoints"
            .BorderStyle=1
        Endwith
    Endproc

    Proc nArrowAngle_Assign(nValue)
        This.nArrowAngle=m.nValue
        This._Draw(This.nArrowAngle)
    Endproc

    Proc nBaseRadius_Assign(nValue)
        This.nBaseRadius=m.nValue
        This._Draw(This.nArrowAngle,This.nBaseRadius)
    Endproc

Enddefine

Define Class SpeedMeter As Container

    Height = 200
    Width = 300
    Dimension aTargets[1]
    nStartValue =0
    nFinalValue =0
    nTargetValue = ""
    ArrowColor=Rgb(255,255,255)
    nShapeLblNumber=1
    cTypeValue = ""
    nProgressBarHeight = 33
    Dimension aValues[1]

    Add Object MainSemiC As CircleShape With StartPoint=-135, FinalPoint=135,Left=0,Top=5,Width=200,Height=200
    Add Object TargetLine As Line With BorderWidth=2, BorderColor=Rgb(128,0,0), LineSlant='/', Visible=.F.
    Add Object Arrow As ArrowShape With Left=0,Top=5,Width=180,Height=180
    Add Object FrontSemiC As CircleShape With StartPoint=-90, FinalPoint=90,Left=0,Top=5,Width=80,Height=80
    Add Object PerCentLbl As Label With Alignment=2,Width=70,Caption="0"
    Add Object Symbol As Label With Width=4,Alignment=2,BackStyle=0, Caption=""
    Add Object BookMarks0 As Label With Caption="0%", BackStyle=0, Visible =.F.
    Add Object BookMarks25 As Label With Caption="25%", BackStyle=0, Visible =.F.
    Add Object BookMarks50 As Label With Caption="50%", BackStyle=0, Visible =.F.
    Add Object BookMarks75 As Label With Caption="75%", BackStyle=0, Visible =.F.
    Add Object BookMarks100 As Label With Caption="100%", BackStyle=0, Visible =.F.

    Proc Init()
        With This
            .BackStyle=0
            .BorderWidth=0
            .MainSemiC.BackColor=Rgb(65,140,250)
            .MainSemiC.BorderColor=.MainSemiC.BackColor
            .FrontSemiC.BackColor=Thisform.BackColor
            .FrontSemiC.BorderColor=.FrontSemiC.BackColor
            .Arrow.BackColor=.ArrowColor
            .Arrow.BorderColor=.Arrow.BackColor
            .PerCentLbl.BackStyle=0
            .PerCentLbl.ForeColor=.MainSemiC.BackColor
            .Symbol.ForeColor=.PerCentLbl.BackColor
            .Arrow._Draw(0,.FrontSemiC.Height/8)
        Endwith
    Endproc

    Proc Resize(bNoLabel)
        With This
            Local l_Offset, l_LabelWidth100
            *-- La grandezza della barra dipende da ci� che non viene coperto dalla FrontCircleGauge
            l_Offset = 1 - (.nProgressBarHeight/100)
            l_LabelWidth100 = cp_LabelWidth2(.BookMarks100.Caption, .BookMarks100.FontName, .BookMarks100.FontSize)
            .MainSemiC.Move(.Left,.Top,Min(Max(.Width-m.l_LabelWidth100*2,0),(Max(.Height-.BookMarks50.Height,0))*2),Min(Max(.Width-m.l_LabelWidth100*2,0),(Max(.Height-.BookMarks50.Height,0))*2))
            .MainSemiC.Left = (.Width-.MainSemiC.Width)/2
            .MainSemiC.Top = .Height-.MainSemiC.Height/2
            .FrontSemiC.Move(.FrontSemiC.Left,.FrontSemiC.Top,.MainSemiC.Width*l_Offset,.MainSemiC.Height*l_Offset)
            .FrontSemiC.Move(Round(.MainSemiC.Left+Int((.MainSemiC.Width-.FrontSemiC.Width)/2),0),Round(.MainSemiC.Top+(.MainSemiC.Height/2-.FrontSemiC.Height/2),0)+3) &&Sommo 3 per correggere la discrepanza della base in alcuni resize
            .Arrow.Move(.Arrow.Left,.Arrow.Top,Round((.MainSemiC.Width/5)*4,0),Round((.MainSemiC.Height/5)*4,0))
            .Arrow.Move(.MainSemiC.Left+Int((.MainSemiC.Width-.Arrow.Width)/2),.Height - (.Arrow.Height/2))
            If !m.bNoLabel
            	.PerCentLbl_Resize()
            EndIf
            *-- Ridimensiono le etichette fisse
            .MoveMarks(.BookMarks0,0)
            .MoveMarks(.BookMarks25,25)
            .MoveMarks(.BookMarks50,50)
            .MoveMarks(.BookMarks75,75)
            .MoveMarks(.BookMarks100,100)
            
            *-- Ridimensiono gli shape e gli indicatori
            
			Local l_Count, l_Index, l_nValue, l_ArrayVal
			l_Count=AMembers(aObjects, This,2)
			l_nDelta = MAX(Nvl(.Parent.Max,0)-.Parent.Min, 1)
			For l_Index=1 to l_count
			    If Lower(Left(aObjects[m.l_Index], 7)) = "oshape_"
			        .&aObjects[m.l_Index]..Move(.MainSemiC.Left,.MainSemiC.Top,.MainSemiC.Width,.MainSemiC.Height)
			    EndIf
			    If Lower(Left(aObjects[m.l_Index], 9)) = "lpercent_"
			    	l_ArrayVal = Val(Substr(aObjects[m.l_Index],At("_",aObjects[m.l_Index])+1,Len(aObjects[m.l_Index])))
			    	l_nValue = Min(MAX(.Parent.aQueryBar[l_ArrayVal, 1] - .Parent.Min, 0)/m.l_nDelta,1)*100
			    	.MoveMarks(.&aObjects[m.l_Index],l_nValue)
			    EndIf
			EndFor
        Endwith
    Endproc

	Procedure PerCentLbl_Resize()
		With this
			Local l_nPerCentWidth, l_nSymbolWidth
			l_nPerCentWidth = cp_LabelWidth2(.PerCentLbl.Caption, .PerCentLbl.FontName, .PerCentLbl.FontSize)
			l_nSymbolWidth = cp_LabelWidth2(.Symbol.Caption, .Symbol.FontName, .Symbol.FontSize)
			.PerCentLbl.Move(.FrontSemiC.Left+Round((.FrontSemiC.Width- l_nPerCentWidth - l_nSymbolWidth)/2,0),.FrontSemiC.Top+.FrontSemiC.Height/2-.PerCentLbl.Height, l_nPerCentWidth )
	        .Symbol.Move(.PerCentLbl.Left+.PerCentLbl.Width,.PerCentLbl.Top, l_nSymbolWidth )
        EndWith
	EndProc 
	
	Procedure MoveMarks(oObj,nVal)
		With this
			Local l_Point,l_TargetLeft ,l_TargetTop, l_nWidth, l_nHeight, l_nCoeff, l_nSin
            l_Point = Dtor(180*(m.nVal/100)-90)
            l_nWidth = cp_LabelWidth2(oObj.Caption, oObj.FontName, oObj.FontSize)
            l_nHeight = Fontmetric(1,oObj.Fontname,oObj.FontSize)
            l_nSin = Sin(l_Point)
            l_nCoeff = Iif(l_nSin<-0.2,-1,Iif(l_nSin>0.2,0,-0.5))
            l_TargetLeft = .Width/2+((.MainSemiC.Width/2)*Sin(l_Point))+(l_nWidth*l_nCoeff)
            l_TargetTop = .Height- (.MainSemiC.Height/2)*Cos(l_Point)
            m.oObj.Move(Min(m.l_TargetLeft,.Width- l_nWidth),Max(m.l_TargetTop - l_nHeight,0), m.l_nWidth, l_nHeight)
		EndWith
	EndProc

    Proc ArrowColor_Assign(nValue)
        With This
            .ArrowColor = Iif(Type("m.nValue")='N',m.nValue,Iif(Isdigit(m.nValue),Int(Val(Alltrim(m.nValue))),Evaluate(Alltrim(m.nValue))))
            .Arrow.BackColor = .ArrowColor
            .Arrow.BorderColor = .ArrowColor
        Endwith
    Endproc

EndDefine

Define Class cp_SpeedMeterBar As cp_ProgressBar

    nArrowColor=0
    bBookMarksVisible = .T.
    nBookMarksFontColor = Rgb(255,255,255)
    cBookMarksFontName = "Arial"
    nBookMarksFontSize = 9
    bOnTarget = .F.
    nBookMark = 0
    cQueryBar = ''
    Dimension aQueryBar[1,4]
	bOnBarTransition = .F.
	cTagSymbol = ''
	
    Add Object SpeedBar As SpeedMeter With StartPoint=-90, FinalPoint=90,Left=0,Top=0,Width=300,Height=200&&Width pi� grande per contenere i BookMarks

    Proc Init()
    	With This
	        DoDefault()
	        .SpeedBar.Symbol.Caption = Iif( Not(.cValBar=='V'),"%",.cSymbol)
	        .nMainBarColor = Iif(.nMainBarColor <> -1,.nMainBarColor ,RGBAlphaBlending(Rgb(95,158,160) ,Thisform.BackColor,0.6))
	        .nMainBarBorderColor = Iif(.nMainBarBorderColor <> -1,.nMainBarBorderColor,.nMainBarColor)
	        .nProgressBarColor = Iif(.nProgressBarColor <> -1,.nProgressBarColor,Thisform.BackColor)
	        .nProgressBarBorderColor = Iif(.nProgressBarBorderColor <> -1,.nProgressBarBorderColor,.nProgressBarBorderColor)
	        .nBookMarksFontColor = Rgb(255,255,255)
		    .cBookMarksFontName = "Arial"
		    .nBookMarksFontSize = 9
	        .Resize()
        Endwith
    Endproc

	PROCEDURE Calculate(xVal, cQueryBar, nMin, nMax, bByEvent)
        With This
            *-- Setta i valori della barra
            .SetProperty(m.xVal, m.nMin, m.nMax, m.cQueryBar)
            *-- Se bByEvent = .T. l'operazione verr� eseguita solo alla chiamata dell'evento
            IF !m.bByEvent
            	*--- Prepara al draw
            	.PrepareBar()
            	*--- Disegna la barra
            	.ReDraw()
            EndIf
        Endwith
	EndProc

*-- Ridefinita perch� la Resize viene fatto nella Draw()
	Procedure ReDraw()
		This.RemoveObjects()
        This.Draw()
	EndProc

	*-- Impostazioni propriet� della SpeedBar
	Procedure SetProperty(xVal, nMin, nMax, cQueryBar)
		With this
			Local l_nMin, l_nMax, l_cQueryBar
			l_nMin = Iif(Type("m.nMin") = 'N', m.nMin, .Min)
			l_nMax = Iif(Type("m.nMax") = 'N', m.nMax, .Max)
			l_cQueryBar = Iif(Type("m.cQueryBar") = 'C', m.cQueryBar, '')		
			
			Do Case
				*--- Progress da valore numerico
				* ------------
				* |||||      |
				* ------------						
				*--- Progress con target
				* -------------
				* |XXX|YYY|ZZZ|
				* ||||||      |
				* |XXX|YYY|ZZZ|
				* -------------									
				Case .nSourceType = 0
					.cQuery = ''
					If Type("m.xVal")='N'
						.nValue = m.xVal
					Else
						.nValue = Val(m.xVal)
					Endif
					.cQueryBar = m.l_cQueryBar
					.Min = m.l_nMin
					.Max = m.l_nMax
				*--- Progress da query (xValue, nMax, nMin)
				* ------------
				* |||||      |
				* ------------	
				*--- Progress con target
				* -------------
				* |XXX|YYY|ZZZ|
				* ||||||      |
				* |XXX|YYY|ZZZ|
				* -------------										
				Case .nSourceType = 1
					.cQuery = m.xVal
					.nValue = 0
					.cQueryBar = m.l_cQueryBar
					.Min = m.l_nMin
					.Max = m.l_nMax
			EndCase
		EndWith
	ENDPROC 

	*--- Esegue tutte le operazioni per preparate i dati finali da disegnare nella SpeedBar
	Procedure PrepareBar()
	With This
		DoDefault()
		If !Empty(.cQueryBar)
			If Upper(Justext(.cQueryBar))="VQR"
				*--- Se query
				m.l_TmpQueryCursor = "TmpProg"+Sys(2015)
				oldArea= Select()
				If cp_FileExist(Forceext(.cQueryBar,'VQR'))
					vq_exec(.cQueryBar, Thisform, m.l_TmpQueryCursor)
					If Used(m.l_TmpQueryCursor)
						l_i=1
						Select(m.l_TmpQueryCursor)
						Locate For 1=1
						Do While !Eof()
							Dimension .aQueryBar[m.l_i,4]
							.aQueryBar[m.l_i,1] = &l_TmpQueryCursor..xValue
							.aQueryBar[m.l_i,2]	= &l_TmpQueryCursor..nColor
							*--- se presenti
							.aQueryBar[m.l_i,3] = Iif(Type("&l_TmpQueryCursor..cCaption")<>'U' And !Isnull(&l_TmpQueryCursor..cCaption), &l_TmpQueryCursor..cCaption, "")
							.aQueryBar[m.l_i,4]	= Iif(Type("&l_TmpQueryCursor..cTooltip")<>'U' And !Isnull(&l_TmpQueryCursor..cTooltip), &l_TmpQueryCursor..cTooltip, "")
							*-- Sostituisco i possibili parametri passati a Caption e Tooltip
							If .nSourceType = 2
								*-- Il Max viene calcolato con la somma dei valori dell'array
								.Max = .Max + .aQueryBar[m.l_i,1]
							Endif
							Select(m.l_TmpQueryCursor)
							Continue
						Enddo
						Use In Select(m.l_TmpQueryCursor)
					Endif
				Else
					If Pemstatus(Thisform, 'cAlertMsg', 5)
						Thisform.cAlertMsg = ah_MsgFormat('Query non passata o inesistente:%0"%1"', Alltrim(.cQueryBar))
					Else
						cp_msg(cp_MsgFormat(MSG_CANNOT_FIND_QUERY__,.cQueryBar))
					Endif
				Endif
				Select(m.oldArea)
			Else
				*--- Tuple
				* 'Valore;Colore;Caption;Tooltip | Valore2;Colore2;Caption2;Tooltip2 | ...'
				* Valore = Numero | Numero%
				* Colore = Numero | RGB(...)
				* Caption = "..." (%V Valore, %P Percentuale, %T Max) Esempio: "%V/%T" (10/152) dove 152 � il massimo e 10 il valore
				* Tooltip = "..." (%V Valore, %P Percentuale, %T Max)
				Local l_NumTarget, l_j
				l_NumTarget = Alines(l_aTarget, .cQueryBar, "|")
				l_i=0
				For l_i=1 To m.l_NumTarget
					Dimension .aQueryBar[m.l_i,4]
					l_NumProp = Alines(l_aProp, l_aTarget[l_i], ";")
					For l_j = 1 To m.l_NumProp
						l_aProp[m.l_j] = Alltrim(l_aProp[m.l_j])
						If !Empty(l_aProp[m.l_j])
							If l_j == 1 And Right(l_aProp[m.l_j], 1)='%' &&Valore
								*--- calcolo da percentuale
								l_Percent = Evaluate(Left(l_aProp[m.l_j], Len(l_aProp[m.l_j])-1))
								.aQueryBar[m.l_i,l_j] = (l_Percent/100*(.Max-.Min)) + .Min
							Else
								.aQueryBar[m.l_i,l_j] = Evaluate(l_aProp[m.l_j])
							Endif
						Endif
					Endfor
					*-- Sostituisco i possibili parametri passati a Caption e Tooltip
					If .nSourceType = 2
						*-- Il Max viene calcolato con la somma dei valori dell'array
						.Max = .Max + .aQueryBar[m.l_i,1]
					Endif
				Endfor
			Endif
			*--- Ordino per valore
			=Asort(.aQueryBar,1)
		Endif
	Endwith
	Endproc

    Proc Draw()
        With This
            *-- Eseguo solo se ci sono state modifiche al valore o al target
            *-- Barra Semicircolare
            Local l_nDelta, l_value, l_oTransit, cCaption
            If Type("this.nValue")='N'
                .SpeedBar.nStartValue = .Min
                .SpeedBar.nFinalValue = Nvl(.Max,0)
				l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
				l_value = Min(MAX(.nValue - .Min, 0)/m.l_nDelta,1)
                *-- Visibilit� target se impostata
				.BMVisibility(EMPTY(.aQueryBar[1, 1]))
                If !EMPTY(.aQueryBar[1, 1]) And !.bOnBarTransition
                	.bOnBarTransition = .bTransition
					.CreateBar(1,-90)
                Else
                    *-- Label indicatori
                    .RemoveObjects()
					IF Bitand(.nBookMark,4)=4
						*-- Visualizza il valore
						Local l_BM50
	                    .SpeedBar.BookMarks0.Caption = Transform(.Min,.cStringFormat)
	                    .SpeedBar.BookMarks100.Caption = Transform(Nvl(.Max,0),.cStringFormat)
	                    l_BM50 = .Min+Int((Nvl(.Max,0)-.Min)/2)
	                    .SpeedBar.BookMarks50.Caption = Transform(l_BM50,.cStringFormat)
	                    .SpeedBar.BookMarks25.Caption = Transform(.Min+Int((l_BM50-.Min)/2),.cStringFormat)
	                    .SpeedBar.BookMarks75.Caption = Transform(l_BM50+Int((Nvl(.Max,0)-l_BM50)/2),.cStringFormat)
					ELSE
						*-- Visualizza la percentuale
						.SpeedBar.BookMarks0.Caption = "0%"
	                    .SpeedBar.BookMarks25.Caption = "25%"
	                    .SpeedBar.BookMarks50.Caption = "50%"
	                    .SpeedBar.BookMarks75.Caption = "75%"
	                    .SpeedBar.BookMarks100.Caption = "100%"
					ENDIF
                EndIf
                cCaption = Iif( Not(.cValBar=='V'),Alltrim(Transform(l_value*100,.cStringFormat)), Alltrim(Transform(.nValue,.cStringFormat)))
                If !.bTransition
                    .SpeedBar.Arrow.nArrowAngle = Round(l_value*(180),0)
                    .SpeedBar.PerCentLbl.Caption = m.cCaption
                    .SpeedBar.Resize()
                    Thisform.NotifyEvent("BarUpdated")
                Else
                    l_oTransit = Createobject("cp_TransitionManager")
                    *--- Eseguo la transizione sulla freccia e la percentuale
                    l_oTransit.Add(.SpeedBar.Arrow, "nArrowAngle", Round(l_value*(180),0), "N", .nTimeTransition, "Linear")
                    .SpeedBar.PerCentLbl.Caption = m.cCaption
                    .SpeedBar.PerCentLbl_Resize()
                    If EMPTY(.aQueryBar[1, 1])
	                    l_oTransit.OnCompleted(This,"OnCompletedTransition()")
                    EndIf
                    l_oTransit.Start(.bWaitTransition)
                EndIf
            EndIf
        Endwith
    Endproc
	
	Proc BMVisibility(bNoTarget)
		With This.SpeedBar
			.BookMarks0.Visible = m.bNoTarget And .Parent.bBookMarksVisible
			.BookMarks25.Visible = m.bNoTarget And .Parent.bBookMarksVisible
			.BookMarks50.Visible = m.bNoTarget And .Parent.bBookMarksVisible
			.BookMarks75.Visible = m.bNoTarget And .Parent.bBookMarksVisible
			.BookMarks100.Visible = m.bNoTarget And .Parent.bBookMarksVisible
		EndWith
	EndProc
	
	Procedure OnCompletedTransition()
		This.Resize(.T.)
		Thisform.NotifyEvent("BarUpdated")
		This.bOnBarTransition = .F.
	EndProc

	*-- Create barra della SpeedBar
	Procedure CreateBar(nIter, nStartPoint)
		With This
			LOCAL l_nStartPoint, l_nFinalPoint, l_nWidth, l_nDelta, l_oObj, l_oPerCent, l_value, l_oTransit, l_TargetLeft, l_TargetTop,l_nFPoint, l_nPerCentWidth
			*--- Nuovo shape
			*-- Prendo l'angolo iniziale dello shape
			l_nStartPoint = m.nStartPoint
			*-- Nascondo la barra di sfondo
			.SpeedBar.MainSemiC.Visible=.F.
			l_nDelta = MAX(Nvl(.Max,0)-.Min, 1)
			l_value = Min(MAX(.aQueryBar[m.nIter, 1] - .Min, 0)/m.l_nDelta,1)
			l_value = l_value*100
			l_num = TRANSFORM(m.nIter)
            .SpeedBar.AddObject("oShape_"+l_num,"CircleShape",m.nStartPoint,m.nStartPoint)
			.SpeedBar.AddObject("lPerCent_"+m.l_num,"Label")
			l_oObj = .SpeedBar.oShape_&l_num
			*-- Riordino le sovrapposizioni
			.SpeedBar.Arrow.ZOrder(0)
			.SpeedBar.FrontSemiC.ZOrder(0)
			.SpeedBar.PerCentLbl.ZOrder(0)
			.SpeedBar.Symbol.ZOrder(0)
			l_oObj.Move(.SpeedBar.MainSemiC.Left,.SpeedBar.MainSemiC.Top,.SpeedBar.MainSemiC.Width,.SpeedBar.MainSemiC.Height)
			*l_oObj.StartPoint = m.l_nStartPoint
			l_oObj.BackColor = .aQueryBar[m.nIter, 2]
			l_oObj.BorderColor = .aQueryBar[m.nIter, 2]
			*-- Imposto l'angolo finale dello shape
			If Type("This.cTypeValue")='C' And Upper(.cTypeValue)="V"
				l_nFPoint = Iif(Nvl(.Max,0)-.Min = 0 Or l_value-.Min < 0,0,(Min((l_value-.Min)/(Nvl(.Max,0)-.Min),1)))
				l_nFinalPoint = 180*l_nFPoint-90
			Else
				l_nFinalPoint = 180*(Min(l_value/100,1))-90
			Endif
			l_oPerCent = .SpeedBar.lPerCent_&l_num
			l_oPerCent.BackStyle = 0
			*-- Sostituisco i possibili parametri passati a Tooltip
			.aQueryBar[m.nIter,4] = .StrTranLabel(.aQueryBar[m.nIter,4], l_value, .aQueryBar[m.nIter, 1])
			l_oPerCent.Caption = EVL(.aQueryBar[m.nIter, 3],'')
			l_oPerCent.ForeColor = .nBookMarksFontColor
			l_oPerCent.FontSize = .nBookMarksFontSize
			l_oPerCent.FontName = .cBookMarksFontName
			*--- La visualizzazione e caption della percentuale dipendono dal valore di nBookMark
			l_oPerCent.Visible = (Bitand(.nBookMark,1)=1)
			If Empty(l_oPerCent.Caption)
				If Bitand(.nBookMark,4)=4
					*-- Visualizza il valore
					l_oPerCent.Caption = "%S"
				Else
					*-- Visualizza la percentuale
					l_oPerCent.Caption =  "%B"
				EndIf
			EndIf
			*-- Eseguo le StrStran sui parametri passati nella aQueryBar
			l_oPerCent.Caption = Alltrim(.StrTranLabel(l_oPerCent.Caption, l_value, .aQueryBar[m.nIter, 1]))
			*-- Percentuale visible fuori lo shape
			.SpeedBar.MoveMarks(l_oPerCent, l_value)
			l_oObj.Visible = .T.
			If .bTransition
	            *-- Transizione dello shape corrente
	            l_oTransit = Createobject("cp_TransitionManager")
	            l_oTransit.Add(l_oObj, "FinalPoint" ,m.l_nFinalPoint, "N", Round(.nTimeTransition / Alen(.aQueryBar,1),0), "Linear")
	            If Type("This.aQueryBar[m.nIter+1, 1]")<>"U"
	         		l_oTransit.OnCompleted(This,"CreateBar("+ Transform(m.nIter+1) +","+ Strtran(Transform(m.l_nFinalPoint),',','.') +")")
	            Else
	            	l_oTransit.OnCompleted(This,"OnCompletedTransition()")
	            EndIf
	            l_oTransit.Start(.bWaitTransition)
			Else
				l_oObj.FinalPoint = l_nFinalPoint 
				If Type("This.aQueryBar[m.nIter+1, 1]")<>"U"
					.CreateBar(m.nIter+1,m.l_nFinalPoint)
				EndIf
			EndIf
		EndWith
	Endproc

	*-- Pulizia oggetti
    Procedure RemoveObjects()
        With This
            *-- Pulisco gli shape
        	If !.bOnBarTransition
	        	Local l_Count, l_Index
			    l_Count=AMembers(aObjects, This.SpeedBar,2)
			    For l_Index=1 to l_count
			        If Lower(Left(aObjects[m.l_Index], 7)) = "oshape_" Or Lower(Left(aObjects[m.l_Index], 9)) = "lpercent_"
			            .SpeedBar.RemoveObject(aObjects[m.l_Index])
			        EndIf
			    EndFor
            EndIf
            .SpeedBar.MainSemiC.Visible=.T.
        Endwith
    Endproc

    Proc Resize(bNoLabel)
        With This
            .SpeedBar.Move(0,0,.Width,.Height)
            .SpeedBar.Resize(m.bNoLabel)
            .Visible = .Visible Or .bVisibleOnResize
        Endwith
    Endproc
    
	Proc cValBar_Assign(bValue)
        With This
		    .cValBar = m.bValue
		    .SpeedBar.PerCentLbl.Visible = !Empty(.cValBar)
			.SpeedBar.Symbol.Visible = !Empty(.cValBar)
        Endwith
    Endproc

    *-- Colore BookMarks
    Proc nBookMarksFontColor_Assign(nValue)
        With This
            .nBookMarksFontColor = m.nValue
            .SpeedBar.BookMarks0.ForeColor = m.nValue
            .SpeedBar.BookMarks25.ForeColor = m.nValue
            .SpeedBar.BookMarks50.ForeColor = m.nValue
            .SpeedBar.BookMarks75.ForeColor = m.nValue
            .SpeedBar.BookMarks100.ForeColor = m.nValue
	        *-- Colore delle Label degli Shapes
	        
	    	Local l_Count, l_Index
		    l_Count=AMembers(aObjects, This.SpeedBar,2)
		    For l_Index=1 to l_count
		        If Lower(Left(aObjects[m.l_Index], 9)) = "lpercent_"
		            .SpeedBar.&aObjects[m.l_Index]..ForeColor = m.nValue
		        EndIf
		    EndFor
	    Endwith
    EndProc
    
    *-- Dimensione BookMarks
    Proc nBookMarksFontSize_Assign(nValue)
        With This
            .nBookMarksFontSize= m.nValue
            .SpeedBar.BookMarks0.FontSize = m.nValue
            .SpeedBar.BookMarks25.FontSize = m.nValue
            .SpeedBar.BookMarks50.FontSize = m.nValue
            .SpeedBar.BookMarks75.FontSize = m.nValue
            .SpeedBar.BookMarks100.FontSize = m.nValue
        *-- Colore delle Label degli Shapes
        
    	Local l_Count, l_Index
	    l_Count=AMembers(aObjects, This.SpeedBar,2)
	    For l_Index=1 to l_count
	        If Lower(Left(aObjects[m.l_Index], 9)) = "lpercent_"
	            .SpeedBar.&aObjects[m.l_Index]..FontSize = m.nValue
	        EndIf
	    EndFor
	    Endwith
    EndProc
    
    *-- Font BookMarks
    Proc cBookMarksFontName_Assign(cValue)
        With This
            .cBookMarksFontName = m.cValue
            .SpeedBar.BookMarks0.FontName = m.cValue
            .SpeedBar.BookMarks25.FontName = m.cValue
            .SpeedBar.BookMarks50.FontName = m.cValue
            .SpeedBar.BookMarks75.FontName = m.cValue
            .SpeedBar.BookMarks100.FontName = m.cValue
        *-- Colore delle Label degli Shapes
        
    	Local l_Count, l_Index
	    l_Count=AMembers(aObjects, This.SpeedBar,2)
	    For l_Index=1 to l_count
	        If Lower(Left(aObjects[m.l_Index], 9)) = "lpercent_"
	            .SpeedBar.&aObjects[m.l_Index]..FontName = m.cValue
	        EndIf
	    EndFor
	    Endwith
    EndProc

    *-- Modifica colore della Label percentuale
    Proc nPerCentFontColor_Assign(nValue)
        With This
            .nPerCentFontColor = m.nValue
            .SpeedBar.PerCentLbl.ForeColor=.nPerCentFontColor
            .SpeedBar.Symbol.ForeColor=.nPerCentFontColor
            .SpeedBar.PerCentLbl_Resize()
        Endwith
    Endproc

    *-- Modifica dimensione della Label percentuale
    Proc nPerCentFontSize_Assign(nValue)
        With This
            .nPerCentFontSize = m.nValue
            .SpeedBar.PerCentLbl.FontSize=m.nValue
            .SpeedBar.Symbol.FontSize=m.nValue
            .SpeedBar.PerCentLbl_Resize()
        Endwith
    Endproc

    *-- Modifica font della Label percentuale
    Proc cPerCentFontName_Assign(cValue)
        With This
            .cPerCentFontName = m.cValue
            .SpeedBar.PerCentLbl.FontName = m.cValue
            .SpeedBar.Symbol.FontName = m.cValue
            .SpeedBar.PerCentLbl_Resize()
        Endwith
    EndProc
    
    *-- Modifica grassetto della Label percentuale
    Proc bPerCentFontBold_Assign(bValue)
        With This
            .bPerCentFontBold = m.bValue
            .SpeedBar.PerCentLbl.FontBold = m.bValue
            .SpeedBar.Symbol.FontBold = m.bValue
            .SpeedBar.PerCentLbl_Resize()
        Endwith
    EndProc

    *-- Modifica al colore delle barre principali
    Proc nMainBarColor_Assign(nValue)
        With This
            .nMainBarColor = nValue
            .SpeedBar.MainSemiC.BackColor=.nMainBarColor
        Endwith
    Endproc

    Proc nMainBarBorderColor_Assign(nValue)
        With This
            .nMainBarBorderColor = nValue
            .SpeedBar.MainSemiC.BorderColor=.nMainBarBorderColor
        Endwith
    Endproc

    Proc nMainBarBackstyle_Assign(nValue)
        With This
            .nMainBarBackstyle = nValue
            .SpeedBar.MainSemiC.BackStyle=.nMainBarBackstyle
        Endwith
    Endproc

    Proc nArrowColor_Assign(nValue)
        With This
            .nArrowColor = nValue
            .SpeedBar.ArrowColor = .nArrowColor
        Endwith
    Endproc

    *-- Modifico colore degli Shape in base al colore della Form
    Proc nFormColor_Assign(nValue)
        With This
            .nFormColor = nValue
            .SpeedBar.FrontSemiC.BackColor=.nFormColor
            .SpeedBar.FrontSemiC.BorderColor=.SpeedBar.FrontSemiC.BackColor
        Endwith
    Endproc

    Proc SymbolAssign()
        With This
            .SpeedBar.Symbol.Caption = IIF( Not(.cValBar=='V'),"%",.cSymbol)
        Endwith
    Endproc

Enddefine


Define Class cp_Page As Container

    Caption=''
    BorderWidth=0
    PageOrder = 0
    Fontname = 'Open Sans'
    Fontsize = 9
    Fontitalic = .F.
    Fontbold = .F.

    Proc Init()
    Endproc

    Proc Calculate()
    Endproc

    Proc Event(cEvent)
    Endproc

    Proc PageOrder_Assign(nValue)
        With This
            Local l_OldValue
            l_OldValue = .PageOrder
            .PageOrder = m.nValue
            If l_OldValue<>m.nValue And l_OldValue>0 && .PageOrder a 0 alla Init
                .Parent.SetPageOrder(m.l_OldValue,m.nValue)
            Endif
        Endwith
    Endproc

Enddefine


*--- Oggetto per la visualizzazione del contenuto di un cursore su una griglia
Define Class cp_CBrowse As Grid

    cDataSource=""
    cCursor=""
    bDescriptionOnHeader=.F.

    Proc Init()
        With This
            * Propriet� Standard
            .RecordSource=""
            .RecordSourceType=1   && ALIAS
            .ColumnCount=0
            .ReadOnly=.T.
            .RecordMark=.F.
            .DeleteMark=.F.
            .Themes=.F.
            .HighlightStyle=1
            .HighlightBackColor=RGBAlphaBlending(Rgb(255,255,0),Rgb(255,255,255),0.5)
            .HighlightForeColor=0
            .HighlightRow=.F.
        Endwith
    Endproc

    Proc Calculate(xValue)
        This.cDataSource= m.xValue
        This.Browse()
    Endproc

    Proc Event(cEvent)
        If Lower(cEvent+',')$Lower(This.cEvent+',')
            This.Browse()
        Endif
    Endproc

    Proc Browse
        Local nI,cI,oCol,cExt,oQry,oQryLdr
        With This
            *--- Grafica griglia
            .BackColor = .Parent.oContained.BackColor
            .GridLineColor = .Parent.oContained.BackColor
            *---
            .RecordSource=''
            .ColumnCount=0
            If !Empty(.cDataSource)
                m.cExt = Justext(.cDataSource)
                *--- Chiudo il vecchio cursore (a meno che non sia ancora lo stesso)
                If !Empty(.cCursor) And Upper(.cDataSource)<>Upper(.cCursor) And Used(.cCursor)
                    Use In Select(.cCursor)
                Endif
                Do Case
                    Case Upper(cExt)=='VQR'
                        .cCursor = Iif(Empty(.cCursor), Sys(2015), .cCursor)
                        vq_exec(.cDataSource, .Parent.oContained, .cCursor)
                    Case Empty(cExt)
                        .cCursor = .cDataSource
                Endcase
                If Used(.cCursor)
                    .ColumnCount=Fcount(.cCursor)
                    .RecordSource=.cCursor
                    *--- Controllo se la sorgente � una VQR e la uso per leggere le descrizioni dei campi
                    If Upper(cExt)=='VQR' And .bDescriptionOnHeader
                        oQry = Createobject('cpquery')
                        oQryLdr = Createobject('cpqueryloader')
                        oQryLdr.LoadQuery(Forceext(.cDataSource,''), oQry, .Null.)
                    Endif
                    For nI=1 To .ColumnCount
                        cI=Alltrim(Str(nI))
                        oCol=.Columns(nI)
                        oCol.DynamicBackColor = "Iif(RECNO()%2=0,this.Parent.oContained.BackColor,IIF(this.Parent.oContained.BackColor=RGB(255,255,255),RGB(210,210,210),RGBAlphaBlending(this.Parent.oContained.BackColor,RGB(255,255,255),0.5)))"
                        If Upper(cExt)=='VQR' And .bDescriptionOnHeader And Vartype(oQry)='O'
                            oCol.Header1.Caption=oQry.i_Fields(nI,6)
                        Else
                            oCol.Header1.Caption=Field(nI,.cCursor)
                        Endif
                        oCol.Bound=.F.
                        oCol.ReadOnly=.ReadOnly
                        oCol.ControlSource=Alltrim(.cCursor)+'.'+Field(nI,.cCursor)
                        oCol.SelectOnEntry=.F.
                        oCol.Text1.Enabled = .F.
                        oCol.Text1.DisabledBackColor = .HighlightBackColor
                        oCol.Text1.DisabledForeColor = .HighlightForeColor
                    Next
                    If Vartype(oQry)='O'
                        oQry.Destroy()
                        oQry = .Null.
                    Endif
                    If Vartype(oQryLdr)='O'
                        oQryLdr.Destroy()
                        oQryLdr = .Null.
                    Endif
                Endif
            Endif
        Endwith
        Grid::Refresh()
    Endproc

    Proc Resize
        Return
    Endproc
Enddefine

Define Class GlobalVarContainer As Custom
    Proc RetrieveGV
        Local FileName, fLine, fLine_old, varName
        FileName = Forceext(Addbs(g_TEMPADHOC)+Sys(2015),'txt')
        *--- devo generare il nome di un file che non esista nella cartella temporanea
        Do While File(FileName)
            FileName = Forceext(Addbs(g_TEMPADHOC)+Sys(2015),'txt')
        Enddo
        List Memory Like 'G_*' Noconsole To File (FileName)
        List Memory Like 'I_*' Noconsole To File (FileName) Additive
        ff = Fopen(FileName)
        varName = ''
        If ff<>-1
            Do While !Feof(ff)
                fLine = Alltrim(Fgets(ff))
                If !Empty(fLine) && riga piena
                    fLine_old = fLine
                    fLine = Strtran(fLine,'  ',' ')
                    *--- cancello gli spazi intermedi
                    Do While fLine<>fLine_old
                        fLine_old = fLine
                        fLine = Strtran(fLine,'  ',' ')
                    Enddo
                    *--- creo array con valori letti dalla riga
                    Alines(ainfo,fLine,' ')
                    *--- Se necessario aggiungo la propriet�
                    Do Case Alen(ainfo)=1 And !Empty(ainfo)
                            varName = ainfo
                        Case Alen(ainfo)>=4 And !Pemstatus(This,ainfo(1),5)
                            This.AddProperty(ainfo(1),Eval(ainfo(4)))
                    Endcase
                Endif
            Enddo
            Fclose(ff)
            Delete File (FileName)
        Endif
    Endproc

    Proc DeclareGV
        Local l_cVarName, l_numVarGlobal
        l_numVarGlobal = Amembers(l_aVarGlobal, This)
        For l_i=1 To l_numVarGlobal
            l_cVarName = l_aVarGlobal[l_i]
            If Type(l_cVarName)='U'
                *--- dichiaro la variabile
                Public &l_cVarName
                *--- le assegno il valore
                &l_cVarName = This.&l_cVarName
            Endif
        Endfor
    Endproc
Enddefine


*--- Ctrl gadget grid
Define Class GadgetGridCtrl As TextBox
    Height =22
    FontName="Open Sans"
    FontSize=9
    Themes = .F.
    SpecialEffect = 1
    Enabled=.F.
    BorderStyle = 0
    DisabledBackColor = Rgb(255,255,255)
    DisabledForeColor = 0

    Procedure SetValue(cCursor, cField, nRow)
        This.Value = &cCursor..&cField
        This.DisabledBackColor=Iif(Mod(m.nRow,2)#0, Rgb(255,255,255), Rgb(228,228,228))
        This.Visible=.T.
    Endproc
Enddefine

*--- Gadget grid
Define Class GadgetGrid As VirtualGrid
    cCursor = ""  && Cursore contenete i dati da visualizzare
    cQuery = "" && Query che riempie il cursore
    cEvent = ""
    bInit = .F. && indica se l'oggetto � stato inizializzato, per evitare l'esecuzione della query troppo presto
    Dimension arFields[1]

    Procedure Init()
        With This
            .bInit = .F.
            .cCursor = Sys(2015)
            .GridLines  = 0
            .ColumnCount = 1
            .RowHeight = 22	&&Se <1 le righe hanno altezza variabile
            .nColMinWidth = 120

            .BackColor = Rgb(255,255,255)
            .BorderWidth = 0
            .oViewFrame.BackColor = .BackColor
            .oViewFrame.BorderWidth = 0
            .oViewFrame.oCntCell.BackColor = .BackColor
            .oViewFrame.oCntCell.BorderWidth = 0
            DoDefault()
            .bInit = .T.
        Endwith
    Endproc

    Function GetCellClass(nRow, nCol)
        Return "GadgetGridCtrl"
    Endfunc

    Proc SetCell(oCtrl, nRow, nCol)
        Local l_OldArea
        l_RowNum = This.ColumnCount*(m.nRow-1) + m.nCol
        l_OldArea = Select()
        If Used(This.cCursor) And m.nRow<=Reccount(This.cCursor)
            Select(This.cCursor)
            Go Record m.nRow
            oCtrl.SetValue(This.cCursor, This.arFields[m.nCol,1], m.nRow)
            Select(l_OldArea)
        Endif
    Endproc

    Function GetColSize(nCol)

        If This.bInit And Alen(This.arFields, 1)>=m.nCol
            Return This.arFields[m.nCol,3]*18
        Else
            Return This.nColMinWidth
        Endif
    Endfunc

    *--- Ridefinita
    Function GetNumRow()
        Return This.RowCount
    Endfunc

    *--- Calc
    Proc Calculate(nValue)
    Endproc

    *--- Event
    Proc Event(cEvent)
        Local l_Event
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(m.l_Event, ' ,', ',')
        If Lower(','+Alltrim(m.cEvent)+',')$Lower(','+Alltrim(m.l_Event)+',')
            This._Redraw()
        Endif
    Endproc

    Proc _Redraw()
        With This
            .Query()
            .oViewFrame.oCntCell._Clean()
            .Resize()
        Endwith
    Endproc

    Procedure Query()
        Local tempCursor
        With This
            If !Empty(.cQuery)
                If Type('this.parent.oContained')='O'
                    This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' before query')
                Endif

                Use In Select(.cCursor)

                .cCursor = Sys(2015)
                vq_exec(.cQuery, .Parent.oContained, .cCursor)

                If Type('this.parent.oContained')='O'
                    This.Parent.oContained.NotifyEvent('w_'+Lower(This.Name)+' after query')
                Endif

                .RowCount = Reccount(.cCursor)
                .ColumnCount = Afields(This.arFields, .cCursor)
            Endif
        Endwith
    Endproc

Enddefine


*----   ---------***
*--- Zucchetti Aulla - Inizio GRIGLIA VIRTUALE DI VISUALIZZAZIONE DATI (Zoom like)
#Define TYPE_QUERY "VQR"
#Define TYPE_CUR "CUR"

*--- Triangolino per l'ordinamento su un campo dell'header
Define Class ShpTrngl As Shape
    BackColor = 0
    BorderStyle = 1
    cType = "U"
    Dimension aPolyPoints[4,2]
    Proc Init
        With This
            Do Case
                Case .cType='U'
                    .aPolyPoints[1,1] = 0&&A
                    .aPolyPoints[1,2] = 100

                    .aPolyPoints[2,1] = 50&&B
                    .aPolyPoints[2,2] = 0

                    .aPolyPoints[3,1] = 100&&C
                    .aPolyPoints[3,2] = 100

                    .aPolyPoints[4,1] = 0&&D
                    .aPolyPoints[4,2] = 100
                Case .cType='D'
                    .aPolyPoints[1,1] = 0&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 50&&B
                    .aPolyPoints[2,2] = 100

                    .aPolyPoints[3,1] = 100&&C
                    .aPolyPoints[3,2] = 0

                    .aPolyPoints[4,1] = 0&&D
                    .aPolyPoints[4,2] = 0
                Case .cType='R'
                    .aPolyPoints[1,1] = 0&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 50

                    .aPolyPoints[3,1] = 0&&C
                    .aPolyPoints[3,2] = 100

                    .aPolyPoints[4,1] = 0&&D
                    .aPolyPoints[4,2] = 0
                Case .cType='L'
                    .aPolyPoints[1,1] = 100&&A
                    .aPolyPoints[1,2] = 0

                    .aPolyPoints[2,1] = 100&&B
                    .aPolyPoints[2,2] = 100

                    .aPolyPoints[3,1] = 0&&C
                    .aPolyPoints[3,2] = 50

                    .aPolyPoints[4,1] = 100&&D
                    .aPolyPoints[4,2] = 0
            Endcase
            .Polypoints ="This.aPolyPoints"
        Endwith
    Endproc

    Proc BackColor_Assing(xValue)
        This.BackColor = m.xValue
        This.BorderColor = m.xValue
    Endproc
Enddefine

*--- Shape di divisione tra le colonne per gestirne il resize
Define Class ShpDivColumn As Shape
    BackStyle = 0
    BackColor = Rgb(200,200,200)
    BorderStyle = 0
    BorderWidth = 0
    DrawMode = 6
    MousePointer = 9
    nOldXCoord = 0

    Procedure MouseDown (nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            This.BackStyle = 1
            This.nOldXCoord = m.nXCoord
        Endif
    Endproc

    Proc MouseMove(nButton, nShift, nXCoord, nYCoord)
        Local nCol, nDelta, nNewWidth
        If nButton=1
            With This
                m.nCol = Val(Substr(.Name,At('_',.Name)+1))
                m.nDelta = m.nXCoord - This.nOldXCoord
                m.nNewWidth = .Parent.grd._GetColSize(m.nCol)+m.nDelta
                If m.nNewWidth>0
                    This.Move(m.nXCoord-.Parent.Left,0)
                Endif
            Endwith
        Endif
    Endproc

    Procedure MouseUp (nButton, nShift, nXCoord, nYCoord)
        Local nCol, nDelta, nNewWidth
        If nButton=1
            With This
                .BackStyle = 0
                m.nCol = Val(Substr(.Name,At('_',.Name)+1))
                m.nDelta = This.Left - This.nOldXCoord
                m.nNewWidth = .Parent.grd._GetColSize(m.nCol)+m.nDelta
                If m.nNewWidth>0
                    .Parent.grd.Columns.Item(Trans(m.nCol)).Width = m.nNewWidth
                    .Parent._Refresh()
                Else
                    This.Move(This.nOldXCoord,0)
                Endif
            Endwith
        Endif
    Endproc

Enddefine

*--- elemento dell'header
Define Class cp_ZoomHeaderItem As Container
    BorderWidth = 0
    BorderColor = Rgb(255,0,0)

    Add Object oLblTitle As Label With BackStyle=0,BorderStyle=0,Visible=.F.
    Add Object oCheck As Checkbox With Width=15,Height=15,Caption="",AutoSize=.T.,BackStyle=0,Visible=.F.

    Add Object oShpUp As ShpTrngl With cType='U',Visible=.F.
    Add Object oShpDown As ShpTrngl With cType='D',Visible=.F.
    Add Object oLblNum As Label With BackStyle=0,Caption="",Width=10,Height=10,Alignment=2,FontSize=6,Visible=.F.
    Add Object oShpDx As ShpTrngl With cType='R',Visible=.F.
    Add Object oShpSx As ShpTrngl With cType='L',Visible=.F.

    Add Object oMouseEventShape As Shape With BackStyle=0,BorderStyle=0
    oCtrlMouseEvent = .Null.
    nShift = 0 && eventuale ultimo bottone premuto al momento del click
    nSeconds = 0 && gestione intervallo di tempo per doppio click
    bOrdering = .F. && colonna su cui � attivo l'ordinamento

    Proc Init
        With This
            *--- frecce ordinamento in fondo a destra
            .oLblNum.Anchor = 0
            .oLblNum.Move(.Width-10,0)
            .oLblNum.Anchor = 9
            .oShpUp.Anchor = 0
            .oShpUp.Move(.Width-8,.Height-9,6,6)
            .oShpUp.Anchor = 12
            .oShpDown.Anchor = 0
            .oShpDown.Move(.Width-8,.Height-9,6,6)
            .oShpDown.Anchor = 12
            *--- frecce spostamento colonne centrate a sinistra e destra
            .oShpDx.Anchor = 0
            .oShpDx.Move(.Width-7,(.Height-6)*0.5,6,6)
            .oShpDx.Anchor = 520
            .oShpDx.BackColor = Rgb(0,0,255)
            .oShpSx.Anchor = 0
            .oShpSx.Move(1,(.Height-6)*0.5,6,6)
            .oShpSx.Anchor = 514
            .oShpSx.BackColor = Rgb(0,0,255)

            .oMouseEventShape.Move(0,0,This.Width,This.Height)
            .oMouseEventShape.ZOrder(0)

            .Visible = .T.
        Endwith
    Endproc

    Proc SetHeader
        Local l_title, cCol
        With This.Parent.Parent.grd
            m.cCol = Substr(This.Name,At("_",This.Name)+1,Len(This.Name)-At("_",This.Name))
            If Upper(.Columns.Item(m.cCol).ControlSource)=="XCHK" && colonna check per zoom con selezione
                This.oCheck.Visible = .T.
                This.oCheck.Value = Iif(This.Parent.bAllSelected,1,0)
            Else
                If Vartype(.Columns.Item(m.cCol).HeaderTitle)=='C' And !Empty(.Columns.Item(m.cCol).HeaderTitle)
                    m.l_title = .Columns.Item(m.cCol).HeaderTitle
                Else
                    m.l_title = .Columns.Item(m.cCol).ControlSource
                Endif
                This.oLblTitle.FontName = Iif(Empty(.Columns.Item(m.cCol).HeaderFontName),This.oLblTitle.FontName,.Columns.Item(m.cCol).HeaderFontName)
                This.oLblTitle.FontSize = Iif(.Columns.Item(m.cCol).HeaderFontSize==0,This.oLblTitle.FontSize,.Columns.Item(m.cCol).HeaderFontSize)
                This.oLblTitle.FontItalic = .Columns.Item(m.cCol).HeaderFontItalic
                This.oLblTitle.FontBold = .Columns.Item(m.cCol).HeaderFontBold
                This.oLblTitle.Alignment = .Columns.Item(m.cCol).HeaderAlignment
                This.oLblTitle.ForeColor = .Columns.Item(m.cCol).HeaderForeColor
                This.oLblTitle.Caption = Icase(This.oLblTitle.Alignment=0, ' '+Alltrim(m.l_title), This.oLblTitle.Alignment=1, Alltrim(m.l_title)+' ', Alltrim(m.l_title))
                This.oLblTitle.Visible = .T.
                This.oLblTitle.Anchor = 0
                This.oLblTitle.AutoSize = .T.
                This.oLblTitle.AutoSize = .F.
            	This.oLblTitle.Move(0,This.Height*0.5-This.oLblTitle.Height*0.5,This.Width)
            	This.oLblTitle.Anchor = 522
            Endif
            *--- Ordinamento
            This.bOrdering = .Columns.Item(m.cCol).bOrdering
            This.oShpUp.BackColor = .Columns.Item(m.cCol).HeaderForeColor
            This.oShpUp.BorderColor = .Columns.Item(m.cCol).HeaderForeColor
            This.oShpDown.BackColor = .Columns.Item(m.cCol).HeaderForeColor
            This.oShpDown.BorderColor = .Columns.Item(m.cCol).HeaderForeColor
            This.oShpDx.BackColor = .Columns.Item(m.cCol).HeaderForeColor
            This.oShpDx.BorderColor = .Columns.Item(m.cCol).HeaderForeColor
            This.oShpSx.BackColor = .Columns.Item(m.cCol).HeaderForeColor
            This.oShpSx.BorderColor = .Columns.Item(m.cCol).HeaderForeColor
            This.oLblNum.ForeColor = .Columns.Item(m.cCol).HeaderForeColor
            This.BackColor = .Columns.Item(m.cCol).HeaderBackColor
        Endwith
    Endproc

    *--- Gestione Click per la selezione
    Procedure oMouseEventShape.MouseDown (nButton, nShift, nXCoord, nYCoord)
        Local nClickSeconds, oCtrlClick
        With This.Parent
            If nButton=1
                Nodefault
                *--- valori per gestione doppio click
                m.nClickSeconds = Seconds()-0.3
                .nShift = m.nShift
                *--- Guardo se ho cliccato proprio sul check
                This.Visible = .F.
                m.oCtrlClick = Sys(1270)
                This.Visible = .T.
                Do Case
                    Case m.nClickSeconds<.nSeconds And Vartype(m.oCtrlClick)="O" And Upper(m.oCtrlClick.Name)<>"OCHECK"&& doppio click
                        ._Dblclick()
                    Case m.nClickSeconds>=.nSeconds And Vartype(m.oCtrlClick)="O" And Upper(m.oCtrlClick.Name)=="OCHECK" And m.oCtrlClick.Value=0
                        .oCheck.Value=1
                        .Parent.bAllSelected=.T.
                        .Parent.Parent.grd.SelectAll()
                    Case m.nClickSeconds>=.nSeconds And Vartype(m.oCtrlClick)="O" And Upper(m.oCtrlClick.Name)=="OCHECK" And m.oCtrlClick.Value=1
                        .oCheck.Value=0
                        .Parent.bAllSelected=.F.
                        .Parent.Parent.grd.DeselectAll()
                Endcase
                .nSeconds=Seconds()
            Endif
        Endwith
        This.ZOrder(0)
    Endproc

    *--- DblClick
    Procedure _Dblclick
        Local nCol
        With This
            m.nCol = (Val(Right(.Name,Len(.Name)-Rat('_',.Name))))
            If .nShift=2 && CTRL (tolgo il campo dall'ordinamento)
                .Parent.Parent.grd.OrderBy(m.nCol,.T.)
            Else && aggiungo il campo all'ordinamento
                .Parent.Parent.grd.OrderBy(m.nCol)
            Endif
        Endwith
    Endproc

    *--- Visualizzazione shape sull'header relativi all'ordinamento
    Proc bOrdering_Assign(xValue)
        Local cColKey, cColName, l_i
        With This.Parent.Parent
            m.cColKey = Substr(This.Name,At("_",This.Name)+1,Len(This.Name)-At("_",This.Name))
            m.cColName = .grd.Columns.Item(m.cColKey).ControlSource
            This.bOrdering = m.xValue
            .grd.Columns.Item(m.cColKey).bOrdering = m.xValue
            Do Case
                Case This.bOrdering And (Upper(m.cColName)+" DESC"$Upper(.cOrderBy))
                    This.oShpUp.Visible = .F.
                    This.oShpDown.Visible = .T.
                Case This.bOrdering And (Upper(m.cColName)$Upper(.cOrderBy))
                    This.oShpUp.Visible = .T.
                    This.oShpDown.Visible = .F.
                    If Empty(This.oLblNum.Caption)
                        If Empty(.grd.Columns.Item(m.cColKey).nOrdering)
                            This.Parent.nOrdering = Min(This.Parent.nOrdering+1,.ColumnCount)
                            This.oLblNum.Caption = Trans(This.Parent.nOrdering)
                            *--- Aggiorno la struttura delle colonne e l'array degli indici di ordinamento
                            .grd.Columns.Item(m.cColKey).nOrdering = This.Parent.nOrdering
                            Dimension This.Parent.aOrdering(This.Parent.nOrdering)
                            This.Parent.aOrdering(This.Parent.nOrdering) = m.cColKey
                        Else
                            This.oLblNum.Caption = Trans(.grd.Columns.Item(m.cColKey).nOrdering)
                            *--- Aggiorno l'array degli indici di ordinamento
                            This.Parent.nOrdering = Max(Alen(This.Parent.aOrdering),.grd.Columns.Item(m.cColKey).nOrdering)
                            Dimension This.Parent.aOrdering(This.Parent.nOrdering)
                            This.Parent.aOrdering(.grd.Columns.Item(m.cColKey).nOrdering) = m.cColKey
                        Endif
                    Endif
                Otherwise
                    *--- Se la caption non � vuota sto effettivamente cancellando una colonna su cui stavo ordinando
                    If !Empty(This.oLblNum.Caption)
                        This.Parent.nOrdering = Max(This.Parent.nOrdering-1,0)
                        *--- Aggiorno i valori della struttura delle colonne e nell'array per far scalare gli indici
                        For l_i=Val(This.oLblNum.Caption) To Alen(This.Parent.aOrdering)-1
                            This.Parent.aOrdering(l_i) = This.Parent.aOrdering(l_i+1)
                            m.cColName = "HEADER_"+This.Parent.aOrdering(l_i)
                            This.Parent.&cColName..oLblNum.Caption = Trans(l_i)
                            .grd.Columns.Item(This.Parent.aOrdering(l_i)).nOrdering = l_i
                        Endfor
                        *--- Elimino un elemento
                        Dimension This.Parent.aOrdering(Max(This.Parent.nOrdering,1))
                        This.oShpUp.Visible = .F.
                        This.oShpDown.Visible = .F.
                        This.oLblNum.Caption = ""
                        .grd.Columns.Item(m.cColKey).nOrdering = 0
                    Endif
            Endcase
        Endwith
    Endproc

    Procedure oMouseEventShape.MouseMove(nButton, nShift, nXCoord, nYCoord)
        If m.nButton=1
            This.Drag(1)
        Endif
    Endproc

    Proc oMouseEventShape.DragOver(oSource,nXCoord,nYCoord,nState)
        Local nStartCol, nEndCol
        With This.Parent
            .oCtrlMouseEvent = This.Parent
            If Vartype(m.oSource.Parent)="O" And Upper(m.oSource.Parent.Class)=="CP_ZOOMHEADERITEM" And m.oSource.Parent.Name<>.oCtrlMouseEvent.Name
                m.nStartCol = Val(Substr(m.oSource.Parent.Name,8,Len(m.oSource.Parent.Name)))
                m.nEndCol = Val(Substr(.oCtrlMouseEvent.Name,8,Len(.oCtrlMouseEvent.Name)))
                Do Case
                    Case nState=0   && Enter
                        If m.nStartCol>m.nEndCol
                            .oCtrlMouseEvent.oShpSx.Visible = .T.
                        Else
                            .oCtrlMouseEvent.oShpDx.Visible = .T.
                        Endif
                    Case nState=1   && Leave
                        If m.nStartCol>m.nEndCol
                            .oCtrlMouseEvent.oShpSx.Visible = .F.
                        Else
                            .oCtrlMouseEvent.oShpDx.Visible = .F.
                        Endif
                Endcase
            Endif
        Endwith
    Endproc

    Proc oMouseEventShape.DragDrop(oSource,nXCoord,nYCoord)
        Local nStartCol, nEndCol
        With This.Parent
            .oCtrlMouseEvent = This.Parent
            If Vartype(m.oSource.Parent)="O" And Upper(m.oSource.Parent.Class)=="CP_ZOOMHEADERITEM" And m.oSource.Parent.Name<>.oCtrlMouseEvent.Name
                .oCtrlMouseEvent.oShpSx.Visible = .F.
                .oCtrlMouseEvent.oShpDx.Visible = .F.
                m.nStartCol = Val(Substr(m.oSource.Parent.Name,8,Len(m.oSource.Parent.Name)))
                m.nEndCol = Val(Substr(.oCtrlMouseEvent.Name,8,Len(.oCtrlMouseEvent.Name)))
                .Parent.Parent.grd.Swap(m.nStartCol, m.nEndCol)
            Endif
        Endwith
    Endproc

    Proc oLblNum.caption_assign(xValue)
        This.Caption = m.xValue
        This.Visible = !Empty(m.xValue)
    Endproc

    Proc Resize
        DoDefault()
        With This
	        .oMouseEventShape.Move(0,0,.Width,.Height)
	        .oMouseEventShape.ZOrder(0)
	        If .oCheck.Visible
	        	.oCheck.Move(1,Max(1,.Height*0.5-.oCheck.Height*0.5))
	        Endif
    	Endwith
    Endproc
Enddefine

*--- Header dello Zoom
Define Class cp_ZoomHeader As Container
    BackStyle = 0
    BorderWidth = 0
    bInitialized = .F.
    nOldLeft = 0 && indice per misurare lo spostamento dell'header e muovere di conseguenza anche gli shape tra le colonne
    bAllSelected = .F. &&in caso di colonna XCHK indica se le righe sono tutte selezionate o meno
    nOrdering = 0 && gestione indice di ordinamento
    Dimension aOrdering(1)

    *--- InitHeader
    Proc InitHeader
        Local nLeft, cItemName, cShpDivColName, nShpDivColLeft, nScrollW
        With This
            If Not .bInitialized
                m.l_Count = .Parent.ColumnCount
                m.nLeft = 0
                For l_i=1 To m.l_Count
                    m.cItemName = "HEADER_"+Trans(l_i)
                    .AddObject(m.cItemName, "cp_ZoomHeaderItem")
                    .&cItemName..SetHeader()
                    .&cItemName..Move(m.nLeft, 0, .Parent.grd._GetColSize(l_i),.Height)
                    .Parent.grd.Columns.Item(Trans(l_i)).Left = m.nLeft
                    *--- inserisco shape di divisione per la gestione della larghezza delle colonne
                    m.cShpDivColName = "SHPDIVCOL_"+Trans(l_i)
                    .Parent.AddObject(m.cShpDivColName,"ShpDivColumn")
                    *--- aggiorno Left
                    m.nLeft = m.nLeft+.Parent.grd._GetColSize(l_i)+.Parent.grd.nHOffset
                    *--- posiziono lo shape
                    m.nScrollW = Iif(.Parent.grd.oHScrollbar.Visible And .Parent.grd.oHScrollbar.Enabled,.Parent.grd.oHScrollbar.Height,0)
                    m.nShpDivColLeft = (m.nLeft-.Parent.grd.nHOffset-1)+.Left
                    If m.l_i<m.l_Count
                        .Parent.&cShpDivColName..Move(m.nShpDivColLeft, 0, .Parent.grd.nHOffset+2, .Parent.Height-m.nScrollW)
                    Else
                        .Parent.&cShpDivColName..Move(m.nShpDivColLeft, 0, 3, .Parent.Height-m.nScrollW)
                    Endif
                    .Parent.&cShpDivColName..Visible = .T.
                    .Parent.&cShpDivColName..ZOrder(0)
                Endfor
                .bInitialized =.T.
                .Resize()
            Endif
        Endwith
    Endproc

    *--- Cancella tutti gli item dell'header e reimposta bInitialized a .f.
    Proc _Clean
        Local cItemName, l_Count
        With This
            l_i = 1
            m.cItemName = "HEADER_"+Trans(l_i)
            Do While Pemstatus(This,m.cItemName,5)
                .RemoveObject(m.cItemName)
                m.cItemName = "SHPDIVCOL_"+Trans(l_i)
                .Parent.RemoveObject(m.cItemName)
                l_i = l_i+1
                m.cItemName = "HEADER_"+Trans(l_i)
            Enddo
            .nOrdering = 0
            Dimension .aOrdering(1)
            .bInitialized =.F.
        Endwith
    Endproc

    *--- Salvo il vecchio valore
    Procedure Left_Assign(xValue)
        Local l_i,bStop,cItemName,nItemLeft,nItemWidth,nOffset
        With This
	        .nOldLeft = .Left
	        .Left = m.xValue
	        m.l_i = 1
	        m.bStop = .f.
	        Do While !m.bStop And m.l_i<=.ControlCount
	    		m.cItemName = "HEADER_"+Trans(l_i)
	    		m.nItemLeft = .&cItemName..Left
	    		m.nItemWidth = .&cItemName..Width
	    		Do Case
	    			Case .Left<0 And m.nItemLeft<Abs(.Left) And m.nItemLeft+m.nItemWidth>Abs(.Left) && Intestazione incompleta a sinistra
	    				m.nOffset = Abs(.Left)-m.nItemLeft
	    				.&cItemName..oLblTitle.Move(m.nOffset,.&cItemName..oLblTitle.Top,m.nItemWidth-m.nOffset)
	    			Case m.nItemLeft+.Left<.Parent.Width And m.nItemLeft+m.nItemWidth+.Left>.Parent.Width && Intestazione incompleta a destra
	    				.&cItemName..oLblTitle.Width = m.nItemWidth-(m.nItemLeft+m.nItemWidth+.Left-.Parent.Width)
	    				m.bStop = .t.
	    			Case m.nItemLeft+.Left>=.Parent.Width && Intestazione oltre la parte visibile, devo fermarmi
	    				m.bStop = .t.
	    			Otherwise && elemento centrale, allargo l'etichetta sull'intera intestazione
	    				.&cItemName..oLblTitle.Move(0,.&cItemName..oLblTitle.Top,m.nItemWidth)
	    				
	    		Endcase
	    		*--- Se la stringa non sta tutta nell'intestazione allora la allineo a sinistra
	    		If cp_LabelWidth2(.&cItemName..oLblTitle.Caption, .&cItemName..oLblTitle.FontName,.&cItemName..oLblTitle.FontSize)<.&cItemName..oLblTitle.Width
	    			.&cItemName..oLblTitle.Alignment = .Parent.grd.Columns.Item(Trans(m.l_i)).HeaderAlignment
	    			.&cItemName..oMouseEventShape.ToolTipText = ''
	    		Else
	    			.&cItemName..oLblTitle.Alignment = 0 && Left
	    			.&cItemName..oMouseEventShape.ToolTipText = Alltrim(.&cItemName..oLblTitle.Caption)
	    		Endif
	    		m.l_i = m.l_i + 1
			Enddo
    	Endwith
    Endproc

    *--- a seconda del valore metto o tolgo il check dall'intestazione di colonna
    Procedure bAllSelected_Assign(xValue)
        Local nCol, cItemName
        This.bAllSelected = m.xValue
        m.nCol = 1
        m.cItemName = "HEADER_"+Trans(m.nCol)
        If Type('This.&cItemName')<>'U'
	        This.&cItemName..oCheck.Value = Iif(This.bAllSelected,1,0)
	    EndIf
    Endproc
    
    *--- Resize
    Proc Resize
    	Local l_i, nLeft, nWidth, cItemName
    	LOCAL TestMacro
    	DoDefault()
    	With This
    		m.nLeft = .Parent.grd.oViewFrame.oCntCell.Left+.Parent.grd.Left
    		If .ControlCount>0
    			m.cItemName = "HEADER_"+Trans(.ControlCount)
    			m.nWidth = .&cItemName..Left+.&cItemName..Width
    			TestMacro=.&cItemName..Height
    			If .Height<>TestMacro
	    			For l_i=1 To .ControlCount
	            		m.cItemName = "HEADER_"+Trans(l_i)
	            		.&cItemName..Height = .Height
	        		Endfor
    			Endif
    		Else
    			m.nWidth = .Parent.Width
    		Endif
    		.Move(m.nLeft,1,m.nWidth)
    	Endwith
    Endproc
    Enddefine

*--- Cella con check dello zoom con selezione
Define Class cp_ZoomCheckSelect As Container
    BackStyle = 1
    BorderWidth = 0

    bSelected = .F. && elemento facente parte di riga selezionata

    Add Object oCheck As Checkbox With Width=15,Height=15,Caption="",AutoSize=.T.,BackStyle=0

    Proc SetValue(nRow, nCol, xFieldValue)
        With This.Parent.Parent.Parent
            *--- BackColor
            If !Empty(.Columns.Item(Trans(m.nCol)).DynamicBackColor)
                This.BackColor = Eval(.Columns.Item(Trans(m.nCol)).DynamicBackColor)
            Else
                If .Parent.bAlternate And (m.nRow%2)=0
                    This.BackColor = .Columns.Item(Trans(m.nCol)).EvenValueBackColor
                Else
                    This.BackColor = .Columns.Item(Trans(m.nCol)).OddValueBackColor
                Endif
            Endif
            *--- Elemento selezionato?
            If .isSelected(m.nRow)
                This.Select(.T., .T.)
            Endif
        Endwith
        This.Visible = .T.
    Endproc

    * --- Seleziona o deselezione l'oggetto se necessario
    * --- bSelecting: .T. Seleziona l'elemento
    * --- bSelecting: .F. Deseleziona l'elemento
    * --- bNoTransition: .T. Seleziona l'elemento con effetto sul cambio di colore della riga
    * --- bNoTransition: .F. Seleziona l'elemento senza effetto sul cambio di colore della riga
    Proc Select(bSelecting, bNoTransition)
        Local nRow, nCol, nBackColor
        With This.Parent.Parent.Parent
            Thisform.LockScreen=.F.
            *--- Calcolo indici di riga e di colonna
            m.nRow = Val(Substr(This.Name,6,At('_',This.Name,2)-6))
            m.nCol = Val(Substr(This.Name,At('_',This.Name,2)+1,Len(This.Name)))
            * --- Testo propriet� bSelected per evitare di riselezionare l'elemento gi� selezionato
            Do Case
                Case This.bSelected And Not m.bSelecting && Deseleziona
                    *--- BackColor
                    If !Empty(.Columns.Item(Trans(m.nCol)).DynamicBackColor)
                        m.nBackColor = Eval(.Columns.Item(Trans(m.nCol)).DynamicBackColor)
                    Else
                        If .Parent.bAlternate And (m.nRow%2)=0
                            m.nBackColor = .Columns.Item(Trans(m.nCol)).EvenValueBackColor
                        Else
                            m.nBackColor = .Columns.Item(Trans(m.nCol)).OddValueBackColor
                        Endif
                    Endif
                    This.oCheck.Value = 0
                    This.bSelected = .F.
                Case Not This.bSelected And m.bSelecting && Seleziona
                    m.nBackColor = RGBAlphaBlending(This.BackColor, .Parent.HighLightColor, 0.4)
                    This.oCheck.Value = 1
                    This.bSelected = .T.
            Endcase
            *--- Transizione per il cambio di colore
            If m.bNoTransition
                This.BackColor = m.nBackColor
            Else
                cp_SingleTransition(This, "BackColor", m.nBackColor, 'C', 500, TRSSELECT)
            Endif
        Endwith
    Endproc

    Proc Resize
        Local nLeft, nTop
        DoDefault()
        This._Resize()
    Endproc

    Proc _Resize
        Local nLeft, nTop
        m.nTop = (This.Height*0.5)-(This.oCheck.Height*0.5)
        This.oCheck.Move(1,Iif(m.nTop=0,1,m.nTop))
    Endproc
Enddefine

*--- Cella dello zoom - Label
Define Class cp_LblZoomField As Container
    BackStyle = 1
    BorderWidth = 0

    bSelected = .F. && elemento facente parte di riga selezionata

    Add Object oLblValue As Label With Top=2, Left=2, BackStyle=0, FontName="Open Sans"

    Proc SetValue(nRow, nCol, xFieldValue)
        With This.Parent.Parent.Parent
            This.oLblValue.FontName = Iif(Empty(.Columns.Item(Trans(m.nCol)).ValueFontName),This.oLblValue.FontName,.Columns.Item(Trans(m.nCol)).ValueFontName)
            This.oLblValue.FontSize = Iif(Empty(.Columns.Item(Trans(m.nCol)).ValueFontSize),This.oLblValue.FontSize,.Columns.Item(Trans(m.nCol)).ValueFontSize)
            *--- FontItalic
            If !Empty(.Columns.Item(Trans(m.nCol)).DynamicFontItalic)
            	This.oLblValue.FontItalic = Eval(.Columns.Item(Trans(m.nCol)).DynamicFontItalic)
            Else
            This.oLblValue.FontItalic = Iif(.Columns.Item(Trans(m.nCol)).ValueFontItalic,.T.,This.oLblValue.FontItalic)
            Endif
            *--- FontBold
            If !Empty(.Columns.Item(Trans(m.nCol)).DynamicFontBold)
            	This.oLblValue.FontBold = Eval(.Columns.Item(Trans(m.nCol)).DynamicFontBold)
            Else
            This.oLblValue.FontBold = Iif(.Columns.Item(Trans(m.nCol)).ValueFontBold,.T.,This.oLblValue.FontBold)
            Endif
            *--- Limite per campo Memo
            If .Columns.Item(Trans(m.nCol)).cColType=='M'
                m.xFieldValue = Substr(m.xFieldValue,1,.Parent.nMemoLimit)
            Endif
            This.oLblValue.Caption = Trans(m.xFieldValue,.Columns.Item(Trans(m.nCol)).ValueFormat)
            *--- setto le propriet�
            *--- BackColor
            If !Empty(.Columns.Item(Trans(m.nCol)).DynamicBackColor)
                This.BackColor = Eval(.Columns.Item(Trans(m.nCol)).DynamicBackColor)
            Else
                If .Parent.bAlternate And (m.nRow%2)=0
                    This.BackColor = .Columns.Item(Trans(m.nCol)).EvenValueBackColor
                Else
                    This.BackColor = .Columns.Item(Trans(m.nCol)).OddValueBackColor
                Endif
            Endif
            *--- ForeColor
            If !Empty(.Columns.Item(Trans(m.nCol)).DynamicForeColor)
                This.oLblValue.ForeColor = Eval(.Columns.Item(Trans(m.nCol)).DynamicForeColor)
            Else
                If .Parent.bAlternate And (m.nRow%2)=0
                    This.oLblValue.ForeColor = .Columns.Item(Trans(m.nCol)).EvenValueForeColor
                Else
                    This.oLblValue.ForeColor = .Columns.Item(Trans(m.nCol)).OddValueForeColor
                Endif
            Endif
            *!*	            If .Parent.bAlternate And (m.nRow%2)=0
            *!*	                This.BackColor = .Columns.Item(Trans(m.nCol)).EvenValueBackColor
            *!*	                This.oLblValue.ForeColor = .Columns.Item(Trans(m.nCol)).EvenValueForeColor
            *!*	            Else
            *!*	                This.BackColor = .Columns.Item(Trans(m.nCol)).OddValueBackColor
            *!*	                This.oLblValue.ForeColor = .Columns.Item(Trans(m.nCol)).OddValueForeColor
            *!*	            Endif
            This.oLblValue.Alignment = .Columns.Item(Trans(m.nCol)).ValueAlignment
            *--- Elemento selezionato?
            If .isSelected(m.nRow)
                This.Select(.T., .T.)
            Endif
        Endwith
        This.Visible = .T.
    Endproc

    * --- Seleziona o deselezione l'oggetto se necessario
    * --- bSelecting: .T. Seleziona l'elemento
    * --- bSelecting: .F. Deseleziona l'elemento
    * --- bNoTransition: .T. Seleziona l'elemento con effetto sul cambio di colore della riga
    * --- bNoTransition: .F. Seleziona l'elemento senza effetto sul cambio di colore della riga
    Proc Select(bSelecting, bNoTransition)
        Local nRow, nCol, nBackColor
        With This.Parent.Parent.Parent
            Thisform.LockScreen=.F.
            *--- Calcolo indici di riga e di colonna
            m.nRow = Val(Substr(This.Name,6,At('_',This.Name,2)-6))
            m.nCol = Val(Substr(This.Name,At('_',This.Name,2)+1,Len(This.Name)))
            * --- Testo propriet� bSelected per evitare di riselezionare l'elemento gi� selezionato
            Do Case
                Case This.bSelected And Not m.bSelecting && Deseleziona
                    m.nBackColor = Iif(.Parent.bAlternate And (m.nRow%2)=0, .Columns.Item(Trans(m.nCol)).EvenValueBackColor, .Columns.Item(Trans(m.nCol)).OddValueBackColor)
                    This.bSelected = .F.
                Case Not This.bSelected And m.bSelecting && Seleziona
                    m.nBackColor = RGBAlphaBlending(This.BackColor, .Parent.HighLightColor, 0.4)
                    This.bSelected = .T.
            Endcase
            *--- Transizione per il cambio di colore
            If m.bNoTransition
                This.BackColor = m.nBackColor
            Else
                cp_SingleTransition(This, "BackColor", m.nBackColor, 'C', 500, TRSSELECT)
            Endif
        Endwith
    Endproc

    Proc Resize
        DoDefault()
        If This.Width>10
            This.oLblValue.Move(5,0,This.Width-10,This.Height)
        Else
            This.oLblValue.Move(0,0,This.Width,This.Height)
        Endif
    Endproc

Enddefine

Define Class cp_ZoomGrid As VirtualGrid
    Dimension aCurFields(1) && informazioni colonne cursore
    Dimension aHeaderTitles(1) && intestazioni colonne header
    Dimension aColumns(1) && array che mantiene oggetti VirtualGridColumn per la gestione delle colonne
    Dimension aRows(1) && array che mantiene oggetti VirtualGridRow per le gestione delle righe
    cEvent = ""

    Add Object aSelected As Collection
    Add Object _oDummy As CommandButton With Top=-1, Left=-1, Width=1, Height=1, Style=1

    Procedure Init()
        With This
            .HotTracking = 1
            .nHOffset = 1
            .nVOffset = 0
            .BackStyle=1
            .BorderWidth=0

            .oViewFrame.oCntCell.AddProperty("nSeconds",0) && gestione doppio click nella MouseDown

            .oViewFrame.oCntCell.oSelectedRowShape.BorderColor = .Parent.HotTrackingColor
            .oViewFrame.oCntCell.oSelectedRowShape.BackStyle = 0
            .oViewFrame.oCntCell.oSelectedRowShape.BorderStyle = 1
            .oViewFrame.oCntCell.oSelectedRowShape.BorderWidth = 2
            .oViewFrame.oCntCell.oSelectedRowShape.Visible = .F.

            *--- grafica Scrollbar
            .oVScrollbar.BackStyle = 0
            .oVScrollbar.BorderWidth = 0
            .nVSBW = .Parent.ScrollBarsWidth
            *.oVScrollbar.oScrollThumb.Move(.oVScrollbar.oScrollThumb.Left+3, .oVScrollbar.oScrollThumb.Top, .oVScrollbar.oScrollThumb.Width-5)
            .oVScrollbar.oScrollThumb.Move(1, .oVScrollbar.oScrollThumb.Top, .nVSBW-1)
            .oVScrollbar.oScrollThumb.BackColor = .Parent.ScrollBarsColor
            .oVScrollbar.oScrollThumb.BorderColor = .Parent.ScrollBarsColor

            .oHScrollbar.BackStyle = 0
            .oHScrollbar.BorderWidth = 0
            .nHSBH = .Parent.ScrollBarsWidth
            *.oHScrollbar.oScrollThumb.Move(.oHScrollbar.oScrollThumb.Left, .oHScrollbar.oScrollThumb.Top+3, .oHScrollbar.oScrollThumb.Width, .oHScrollbar.oScrollThumb.Height-5)
            .oHScrollbar.oScrollThumb.Move(.oHScrollbar.oScrollThumb.Left, 1, .oHScrollbar.oScrollThumb.Width, .nHSBH-2)
            .oHScrollbar.oScrollThumb.BackColor = .Parent.ScrollBarsColor
            .oHScrollbar.oScrollThumb.BorderColor = .Parent.ScrollBarsColor

            *--- Init strutture righe e colonne
            .aColumns(1) = .Null.
            .aRows(1) = .Null.
        Endwith
        DoDefault()
    Endproc

    *--- Calc
    Proc Calculate(nValue)
    Endproc

    *--- Event
    Proc Event(cEvent)
        Local l_Event
        l_Event = Strtran(This.cEvent, ', ', ',')
        l_Event = Strtran(m.l_Event, ' ,', ',')
        If Lower(','+Alltrim(m.cEvent)+',')$Lower(','+Alltrim(m.l_Event)+',')
            This.Parent.cWhere =""
            This.Parent.cOrderBy =""
            This._Refresh()
        Endif
    Endproc

    Function GetCellClass(nRow, nCol)
        Local cField
        m.cField = This.Columns.Item(Trans(m.nCol)).ControlSource
        If Upper(m.cField)=="XCHK"
            Return "cp_ZoomCheckSelect"
        Else
            Return "cp_LblZoomField"
        Endif
    Endfunc

    Function GetCellSize(nRow, nCol, nRowRet As Integer @, nColRet As Integer @, nSpanTypeCtrl As Integer @)
        *--- 0 Cella 1x1 : nRowRet, nColRet invariati
        *--- 1 Cella padre NxM : nRowRet, nColRet Span
        *--- 2 Cella figlia : nRowRet, nColRet Cella padre
        nRowRet = m.nRow
        nColRet = m.nCol
        nSpanTypeCtrl = 0
    Endfunc

    Function _GetColSize(nCol)
        Local nIndex
        With This
            m.nIndex = .Columns.GetKey(Trans(m.nCol))
            If m.nIndex<>0
                Return .Columns.Item(m.nIndex).Width
            Else
                Return .GetColSize(m.nCol)
            Endif
        Endwith
    Endfunc

    Proc SetCell(oCtrl, nRow, nCol)
        Local l_OldArea, l_recno, cFieldName, xFieldValue, cCursorAssign
        With This
            If Used(.Parent.cCursor) And m.nRow<=.RowCount And m.nCol<=.ColumnCount
                m.cFieldName = .Columns.Item(Trans(m.nCol)).ControlSource
                m.l_OldArea = Select()
                Select(.Parent.cCursor)
                m.l_recno = Recno()
                Go Record m.nRow
                m.cCursorAssign = .Parent.cCursor+'.'+m.cFieldName
                m.xFieldValue = &cCursorAssign
                oCtrl.SetValue(m.nRow, m.nCol, m.xFieldValue)
                oCtrl.ZOrder(1)
                Go Record m.l_recno
                Select(m.l_OldArea)
            Endif
        Endwith
    Endproc

    Function SetCursorBySource()
        Local cResults, cExt
        With This
            If !Empty(.Parent.cDataSourceType) And !Empty(.Parent.cDataSource) And .Parent.bReQuery
                Use In Select(.Parent.cCursor)
                If Empty(.Parent.cCursor)
                    .Parent.cCursor = Sys(2015)
                Endif
                *--- Notifica prima di recuperare i dati
                If Type('this.parent.parent.oContained')='O'
                    This.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Name)+' before query')
                Endif
                Do Case
                    Case Alltrim(Upper(.Parent.cDataSourceType)) = TYPE_CUR
                        m.cResults = "Select * From (.Parent.cDataSource) Into Cursor (.Parent.cCursor)"
                        &cResults
                        .Parent.bReQuery = .F. && se la sorgente dati � un curosore non devo ricopiarmela ogni volta
                    Case Alltrim(Upper(.Parent.cDataSourceType)) = TYPE_QUERY
                        m.cExt = Justext(.Parent.cDataSource)
                        If Empty(m.cExt) Or Upper(m.cExt)<>TYPE_QUERY
                            .Parent.cDataSource= Forceext(.Parent.cDataSource,TYPE_QUERY)
                        Endif
                        vq_exec(.Parent.cDataSource, .Parent.Parent.oContained, .Parent.cCursor)
                Endcase
                *--- Controllo se va aggiunta la colonna col check per la selezione
                If .Parent.bCheckSelect
                    m.cResults = "Select 0 as XCHK, * From (.Parent.cCursor) Into Cursor (.Parent.cCursor)"
                    &cResults
                    wrcursor(.Parent.cCursor)
                Endif
                *--- Notifica dopo aver recuperato i dati
                If Type('this.parent.parent.oContained')='O'
                    This.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Name)+' after query')
                Endif
                Return .T.
            Else
                Return !.Parent.bReQuery
            Endif
        Endwith
    Endfunc

    Procedure Query()
        Local cResults,cTempCursor,cExt,oCol,nHide,l_i,oQry,oQryLdr
        With This
            If  .SetCursorBySource()
                *--- Ordino e/o filtro se necessario
                If !Empty(.Parent.cOrderBy) Or !Empty(.Parent.cWhere)
                    m.cTempCursor = Sys(2015)
                    m.cResults = "Select * From (.Parent.cCursor)"+.Parent.cWhere+.Parent.cOrderBy+" Into Cursor (m.cTempCursor) ReadWrite"
                    &cResults
                    Use In Select(.Parent.cCursor)
                    .Parent.cCursor = m.cTempCursor
                Endif
                .RowCount = Reccount(.Parent.cCursor)
                .ColumnCount = Afields(.aCurFields, .Parent.cCursor)
                *--- Se la DataSource � un VQR leggo la descrizione e larghezza dei campi
                Dimension .aHeaderTitles(.ColumnCount)
                If .Parent.cDataSourceType == TYPE_QUERY
                    m.oQry = Createobject("cpquery")
                    m.oQryLdr = Createobject("cpqueryloader")
                    m.oQryLdr.LoadQuery(Forceext(.Parent.cDataSource,''), m.oQry, .Parent.Parent.oContained)
                    For l_i=1 To m.oQry.nFields
                        If !Empty(m.oQry.i_Fields(l_i,6))
                            .aHeaderTitles(l_i) = Alltrim(m.oQry.i_Fields(l_i,6))
                        Endif
                    	If Upper(m.oQry.i_Fields(l_i,3))=="CHAR"
                            .aCurFields(l_i,3) = m.oQry.i_Fields(l_i,4)
                        Endif
                    	If InList(Upper(m.oQry.i_Fields(l_i,3)),"DATE","DATETIME")
                            .aCurFields(l_i,2) = Iif(Upper(m.oQry.i_Fields(l_i,3))=="DATE","D","T")
                        Endif
                    Endfor
                    m.oQry = .Null.
                    m.oQryLdr = .Null.
                Endif
                *--- Strutture per le colonne
                If .Parent.bUpdated
                    *--- se avevo dei vecchi valori li cancello
                    If .Columns.Count>0
                        For l_i=.Columns.Count To 1 Step -1
                            .Columns.Remove(l_i)
                        Endfor
                        *--- cancello anche il relativo oggetto header
                        .Parent.hdr._Clean()
                    Endif
                    Dimension .aColumns(1)
                    Dimension .aColumns(.ColumnCount)
                    For l_i=1 To .ColumnCount
                        .aColumns(l_i) = Createobject("VirtualGridColumn")
                        .Columns.Add(.aColumns(l_i), Trans(l_i))
                        .SetColumnProp(l_i)
                    Endfor
                    *--- Strutture per le righe
                    .Parent.bUpdated = .F.
                    *--- se avevo dei vecchi valori li cancello
                    If .Rows.Count>0
                        For l_i=.Rows.Count To 1 Step -1
                            .Rows.Remove(l_i)
                        Endfor
                    Endif
                    Dimension .aRows(1)
                    If .RowCount>0
                        Dimension .aRows(.RowCount)
                    Endif
                    For l_i=1 To .RowCount
                        .aRows(l_i) = Createobject("VirtualGridRow")
                        .Rows.Add(.aRows(l_i), Trans(l_i))
                    Endfor
                Endif
                *---Controllo le colonne che non devono essere visualizzate
                m.nHide = 0
                For l_i=1 To .ColumnCount
                    If l_i<=.ColumnCount-m.nHide And !.Columns.Item(Trans(l_i)).Visible
                        .Swap(l_i,.ColumnCount,.T.)
                        m.nHide = m.nHide+1
                    Endif
                Endfor
                *--- L'array per i titoli non mi serve pi�
                Dimension .aHeaderTitles(1)
                .ColumnCount = .ColumnCount-m.nHide
                *--- Svuoto la cache delle Left delle colonne
                .nColMinWidth = .nColMinWidth
                *--- Inizializzo l'header e gli shape fra le colonne
                This.Parent.hdr.InitHeader()
                This.Parent.CurrentRow = 0
                This.Parent.CurrentCol = 0
                This.Resize() && visualizza scroolbar
            Endif
        Endwith
    Endproc

    Proc _Refresh()
        With This
            .Query()
            .oViewFrame.oCntCell._Clean()
            .Resize() && visualizza scroolbar
            .oViewFrame.oCntCell.oSelectedRowShape.Visible=.F.
        Endwith
    Endproc

    Proc RePos()
        With This
            If !Empty(.Parent.cCursor)
                .oVScrollbar.nPosition = .RowHeight*(Recno(.Parent.cCursor)-1)
            Endif
        Endwith
    Endproc

    *--- Setta i valori della colonna nel relativo oggetto VirtualGridColumn
    *--- nCol : indice della colonna (su cursore)
    *--- cKey : chiave della colonna da valorizzare (nella collection)
    Proc SetColumnProp(nCol)
        Local cKey,cField,cType,cHeaderTitle,cTrsField,nHeaderW,nLenFieldW
        With This

            m.cKey = Trans(m.nCol)
            m.cField = Alltrim(.aCurFields(m.nCol,1))
            m.cType = .aCurFields(m.nCol,2)
            *--- Trascodifica Alias campi
            cTrsField = 'w_TRS_'+m.cField
            If .Parent.bTrsHeaderTitle And Pemstatus(.Parent.Parent,'oContained',5) And;
                    PemStatus(.Parent.Parent.oContained,m.cTrsField,5) And Vartype(m.cTrsField)=='C' And !Empty(m.cTrsField)
                m.cHeaderTitle = .Parent.Parent.oContained.&cTrsField
            Else
                m.cHeaderTitle = .aHeaderTitles(m.nCol)
            Endif
            .Columns.Item(m.cKey).Visible = .T.&&(m.cType<>"M")
            *--- Alias campo
            .Columns.Item(m.cKey).ControlSource = m.cField
            *--- Nome colonna
            .Columns.Item(m.cKey).HeaderTitle = m.cHeaderTitle
            *--- Tipologia dati
            .Columns.Item(m.cKey).cColType = m.cType
            
            *--- HeaderBackColor
            .Columns.Item(m.cKey).HeaderBackColor = .Parent.HeaderBackColor
            *--- HeaderForeColor
            .Columns.Item(m.cKey).HeaderForeColor = .Parent.HeaderForeColor
            *--- HeaderFontName
            .Columns.Item(m.cKey).HeaderFontName = .Parent.HeaderFontName
            *--- HeaderFontSize
            .Columns.Item(m.cKey).HeaderFontSize = .Parent.HeaderFontSize
            *--- HeaderFontItalic
            .Columns.Item(m.cKey).HeaderFontItalic = .Parent.HeaderFontItalic
            *--- HeaderFontBold
            .Columns.Item(m.cKey).HeaderFontBold = .Parent.HeaderFontBold
            *--- HeaderAlignment
            .Columns.Item(m.cKey).HeaderAlignment = .Parent.HeaderAlignment

            *--- ValueFontName
            .Columns.Item(m.cKey).ValueFontName = .Parent.ValueFontName
            *--- ValueFontSize
            .Columns.Item(m.cKey).ValueFontSize = .Parent.ValueFontSize
            *--- ValueFontItalic
            .Columns.Item(m.cKey).ValueFontItalic = .Parent.ValueFontItalic
            *--- ValueFontBold
            .Columns.Item(m.cKey).ValueFontBold = .Parent.ValueFontBold
            *--- Alignment
            Do Case
                Case .aCurFields(m.nCol,2)='N' Or .aCurFields(m.nCol,2)='I'
                    .Columns.Item(m.cKey).ValueAlignment = 1
                Case Upper(m.cField)=="XCHK" And (.aCurFields(m.nCol,2)='N' Or .aCurFields(m.nCol,2)='I')
                    .Columns.Item(m.cKey).ValueAlignment = 2
                Otherwise
                    .Columns.Item(m.cKey).ValueAlignment = 0
            Endcase
             *--- Format
            If .aCurFields(m.nCol,2)='N' Or .aCurFields(m.nCol,2)='I'
            	.Columns.Item(m.cKey).ValueFormat = Replicate('9',.aCurFields(m.nCol,3))
            	If .aCurFields(m.nCol,4)>0 && cifre decimali
            		.Columns.Item(m.cKey).ValueFormat = .Columns.Item(m.cKey).ValueFormat+'.'+Replicate('9',.aCurFields(m.nCol,4))
            	Endif
            Endif
             *--- Width
            Do Case
                Case Upper(m.cField)=="XCHK"
                    .Columns.Item(m.cKey).Width = 15
                Case Upper(m.cType)=="M"
                    .Columns.Item(m.cKey).Width = 300
                Otherwise
                    m.nHeaderW = cp_LabelWidth2('88'+Iif(!Empty(m.cHeaderTitle), m.cHeaderTitle, m.cField), .Parent.HeaderFontName,Iif(.Parent.HeaderFontSize<>0, .Parent.HeaderFontSize, 10))
                    If Upper(m.cType)=="T" Or Upper(m.cType)=="D" &&Date o DateTime
                    	m.nLenFieldW = cp_LabelWidth2(Iif(Upper(m.cType)=="D",' 8888888888 ',' 8888888888 88888888 '), .Parent.ValueFontName,.Parent.ValueFontSize)
                    Else
                    	m.nLenFieldW = cp_LabelWidth2(Replicate('8', .aCurFields(m.nCol,3)+2), .Parent.ValueFontName,.Parent.ValueFontSize)
                    Endif
                    .Columns.Item(m.cKey).Width = Min(Max(m.nLenFieldW,m.nHeaderW),500)
            Endcase
            
            *--- DynamicBackColor
            .Columns.Item(m.cKey).DynamicBackColor = .Parent.DynamicBackColor
            *--- DynamicForeColor
            .Columns.Item(m.cKey).DynamicForeColor = .Parent.DynamicForeColor
            *--- DynamicFontBold
            .Columns.Item(m.cKey).DynamicFontBold = .Parent.DynamicFontBold
            *--- DynamicFontItalic
            .Columns.Item(m.cKey).DynamicFontItalic = .Parent.DynamicFontItalic
            *--- OddValueBackColor
            .Columns.Item(m.cKey).OddValueBackColor = .Parent.OddBackColor
            *--- OddValueForeColor
            .Columns.Item(m.cKey).OddValueForeColor = .Parent.OddForeColor
            *--- EvenValueBackColor
            .Columns.Item(m.cKey).EvenValueBackColor = .Parent.EvenBackColor
            *--- EvenValueForeColor
            .Columns.Item(m.cKey).EvenValueForeColor = .Parent.EvenForeColor

        Endwith
    Endproc

    Proc GetColIndexByName(cColName)
        Local l_i, nIndex
        With This
            l_i = 1
            m.nIndex = .F.
            Do While l_i<=.Parent.ColumnCount And Vartype(m.nIndex)='L'
                If Upper(.Columns.Item(l_i).ControlSource)==Upper(m.cColName)
                    m.nIndex = .Columns.GetKey(l_i)
                Endif
                l_i = l_i+1
            Enddo
            Return Iif(Vartype(m.nIndex)='L',.F.,Val(m.nIndex))
        Endwith
    Endproc

    Proc oViewFrame.oCntCell.oMouseEventShape.MouseMove(nButton, nShift, nXCoord, nYCoord)
        Local nRow
        With This.Parent
            If .Parent.Parent.HotTracking<>0
                Thisform.LockScreen=.T.
                This.Visible=.F.
                .oSelectedRowShape.Visible=.F.
                .oCtrlMouseEvent = Sys(1270)
                This.Visible=.T.
                Do While Type("This.Parent.oCtrlMouseEvent")='O' And Type("This.Parent.oCtrlMouseEvent.Parent")='O' And Upper(Left(.oCtrlMouseEvent.Name,5)) # "CTRL_"
                    .oCtrlMouseEvent = .oCtrlMouseEvent.Parent
                Enddo
                If Type("This.Parent.oCtrlMouseEvent")='O' And !IsNull(.oCtrlMouseEvent) And Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                    .oSelectedRowShape.Move(0,.oCtrlMouseEvent.Top,.Width,.oCtrlMouseEvent.Height)
                    .oSelectedRowShape.Visible=.T.
                    .oSelectedRowShape.ZOrder(0)
                Endif
                This.ZOrder(0)
                Thisform.LockScreen=.F.
            Endif
        Endwith
    Endproc

    *--- Ordinamento sul nCol-esima colonna
    *--- nCol = nome o indice della colonna
    *--- bCancel = .t., il campo viene aggiunto all'ordinamento
    *--- bCancel = .f., il campo viene tolto dall'ordinamento
    *--- Return .t. se � stato possibile processare la richiesta
    *--- Return .f. se non � stato possibile processare la richiesta (indice colonna sbagliato o campo Memo)
    Procedure OrderBy(nCol, bCancel)
        Local cOrdered, cCol, cHeaderName
        With This
            *--- se viene passato il nome della colonna mi procuro l'indice
            m.cCol = ""
            If Vartype(m.nCol)='C'
                m.cCol = m.nCol
                m.nCol = .GetColIndexByName(m.cCol)
            Endif
            If Vartype(m.nCol)='N' And (m.nCol>=1 And m.nCol<=.ColumnCount) And (.Columns.Item(Trans(m.nCol)).cColType<>"M") And Used(.Parent.cCursor)
                m.cCol = Iif(Empty(m.cCol),.Columns.Item(Trans(m.nCol)).ControlSource,m.cCol)
                m.cHeaderName = "HEADER_"+Trans(m.nCol)
                Do Case
                    Case Empty(.Parent.cOrderBy) && Ordinamento su campo nuovo
                        .Parent.cOrderBy = " ORDER BY "+ m.cCol+" "
                        .Parent.hdr.&cHeaderName..bOrdering = .T.
                    Case Not Empty(.Parent.cOrderBy) And Not Upper(m.cCol)$Upper(.Parent.cOrderBy) && campo non ancora presente nell'ordinamento
                        If !m.bCancel
                            .Parent.cOrderBy = .Parent.cOrderBy+", "+m.cCol+" "
                            .Parent.hdr.&cHeaderName..bOrdering = .T.
                        Endif
                    Case Not Empty(.Parent.cOrderBy) And Upper(m.cCol)$Upper(.Parent.cOrderBy) && campo gi� presente nell'ordinamento
                        If !m.bCancel  && inverte l'ordinamento
                            If " "+Upper(m.cCol)+" DESC"$Upper(.Parent.cOrderBy)
                                .Parent.cOrderBy = Strtran(Upper(.Parent.cOrderBy)," "+Upper(m.cCol)+" DESC"," "+m.cCol+" ")
                            Else
                                .Parent.cOrderBy = Strtran(Upper(.Parent.cOrderBy)," "+Upper(m.cCol)+" "," "+m.cCol+" DESC")
                            Endif
                            .Parent.hdr.&cHeaderName..bOrdering = .T.
                        Else
                            *--- tolgo il campo (scrivo tutte le possibili combinazioni, solo una matcher�)
                            *--- NON MODIFICARE L'ORDINE
                            .Parent.cOrderBy = Strtran(Upper(.Parent.cOrderBy),", "+Upper(m.cCol)+" DESC","")
                            .Parent.cOrderBy = Strtran(Upper(.Parent.cOrderBy),", "+Upper(m.cCol)+" ","")
                            .Parent.cOrderBy = Strtran(Upper(.Parent.cOrderBy)," "+Upper(m.cCol)+" DESC, "," ")
                            .Parent.cOrderBy = Strtran(Upper(.Parent.cOrderBy)," "+Upper(m.cCol)+" , "," ")
                            .Parent.cOrderBy = Strtran(Upper(.Parent.cOrderBy)," "+Upper(m.cCol)+" DESC","")
                            .Parent.cOrderBy = Strtran(Upper(.Parent.cOrderBy)," "+Upper(m.cCol)+" ","")
                            *--- se era l'unico campo azzero la stringa
                            If Upper(Alltrim(.Parent.cOrderBy))=="ORDER BY"
                                .Parent.cOrderBy = ""
                            Endif
                            .Parent.hdr.&cHeaderName..bOrdering = .F.
                        Endif
                Endcase
                If !.Parent.bCheckSelect Or (.Parent.bCheckSelect And .Parent.bReQuery)
                    .DeselectAll()
                Endif
                ._Refresh()
                Return .T.
            Else
                Return .F.
            Endif
        Endwith
    Endproc

    Proc oViewFrame.oCntCell.MouseLeave(nButton, nShift, nXCoord, nYCoord)
        This.oSelectedRowShape.Visible = .F.
        This.oMouseEventShape.ZOrder(0)
    Endproc

    *--- Gestione Click per la selezione
    Procedure oViewFrame.oCntCell.oMouseEventShape.MouseDown (nButton, nShift, nXCoord, nYCoord)
        Local tempCtrl,l_i, nFirstRow, nLastRow, nClickSeconds
        With This.Parent
            If nButton=1
                Nodefault
                m.nClickSeconds = Seconds()-0.3
                Thisform.LockScreen=.T.
                This.Visible=.F.
                .oSelectedRowShape.Visible=.F.
                .oCtrlMouseEvent = Sys(1270)
                This.Visible=.T.
                .oSelectedRowShape.Visible=.T.
                This.ZOrder(0)
                Do While Type("This.Parent.oCtrlMouseEvent")='O' And Type("This.Parent.oCtrlMouseEvent.Parent")='O' And Upper(Left(.oCtrlMouseEvent.Name,5)) # "CTRL_"
                    .oCtrlMouseEvent = .oCtrlMouseEvent.Parent
                Enddo
                m.nLastRow = .Parent.Parent.Parent.CurrentRow
                *--- Aggiorno CurrentRow e CurrentCol
                If Type("This.Parent.oCtrlMouseEvent")='O' And Upper(Left(.oCtrlMouseEvent.Name,5)) == "CTRL_"
                    .Parent.Parent.Parent.CurrentCol = Val(Substr(.oCtrlMouseEvent.Name,At('_',.oCtrlMouseEvent.Name,2)+1,Len(.oCtrlMouseEvent.Name)))
                    .Parent.Parent.Parent.CurrentRow = Val(Substr(.oCtrlMouseEvent.Name,6,At('_',.oCtrlMouseEvent.Name,2)-6))
                    Do Case
                        Case (m.nClickSeconds<.nSeconds And .Parent.Parent.Parent.CurrentRow=m.nLastRow) && doppio click
                            .Parent.Parent._Dblclick(.oCtrlMouseEvent)
                        Case (m.nClickSeconds>=.nSeconds Or .Parent.Parent.Parent.CurrentRow<>m.nLastRow) && singolo click
                            Do Case
                                Case nShift=1 && SHIFT (abilitato se in modalit� multi selezione)
                                    If .Parent.Parent.Parent.bMultiSelect Or Upper(.oCtrlMouseEvent.Class)=="CP_ZOOMCHECKSELECT"
                                        m.nFirstRow = Val(.Parent.Parent.aSelected.GetKey[.Parent.Parent.aSelected.Count])
                                        m.nLastRow = Val(Substr(.oCtrlMouseEvent.Name,6,At('_',.oCtrlMouseEvent.Name,2)-6))
                                        .Parent.Parent.RowSelect(.T., m.nFirstRow, m.nLastRow)
                                    Endif
                                Case nShift=2 && CTRL (abilitato se in modalit� multi selezione)
                                    If .Parent.Parent.Parent.bMultiSelect Or Upper(.oCtrlMouseEvent.Class)=="CP_ZOOMCHECKSELECT"
                                        m.nFirstRow = Val(Substr(.oCtrlMouseEvent.Name,6,At('_',.oCtrlMouseEvent.Name,2)-6))
                                        .Parent.Parent.RowSelect(Not .Parent.Parent.isSelected(m.nFirstRow), m.nFirstRow)
                                    Endif
                                Otherwise && nessun tasto premuto
                                    m.nFirstRow = Val(Substr(.oCtrlMouseEvent.Name,6,At('_',.oCtrlMouseEvent.Name,2)-6))
                                    If .Parent.Parent.Parent.bMultiSelect Or Upper(.oCtrlMouseEvent.Class)=="CP_ZOOMCHECKSELECT"
                                        .Parent.Parent.RowSelect(Not .Parent.Parent.isSelected(m.nFirstRow), m.nFirstRow)
                                    Else
                                        For l_i = .Parent.Parent.aSelected.Count To 1 Step -1
                                            m.nLastRow = Val(.Parent.Parent.aSelected.GetKey[l_i])
                                            .Parent.Parent.RowSelect(.F., m.nLastRow)
                                        Endfor
                                        .Parent.Parent.RowSelect(.T., m.nFirstRow)
                                    Endif
                            Endcase
                    Endcase
                Else
                    *--- click non su una riga
                    .Parent.Parent.Parent.CurrentRow = 0
                    .Parent.Parent.Parent.CurrentCol = 0
                Endif
                .Parent.Parent.Parent.hdr.bAllSelected = .F.
                .Parent.Parent._oDummy.SetFocus()
                This.ZOrder(0)
                Thisform.LockScreen=.F.
                .nSeconds=Seconds()
            Endif
        Endwith
    Endproc

    Proc _Dblclick(oCtrl)
        *--- Notifica il doppio click
        If Type('this.parent.parent.oContained')='O'
            This.Parent.Parent.oContained.NotifyEvent('w_'+Lower(This.Parent.Name)+' DblClick')
        Endif
    Endproc

    *--- Seleziona tutte le righe della griglia
    Proc SelectAll()
        This.RowSelect(.T.,1,This.RowCount)
    Endproc

    *--- Deseleziona tutte le righe della griglia
    Proc DeselectAll()
        This.RowSelect(.F.,1,This.RowCount)
    Endproc

    *--- Seleziona (bSelect = .t.) o deseleziona (bAction = .f.) una o pi� righe della griglia
    Proc RowSelect(bSelect, nStartRow, nEndRow)
        Local tempCtrl, nRow, nCol, nStart, nEnd, l_i, l_step
        If Vartype(m.nEndRow)='L' Or m.nStartRow==m.nEndRow
            This._SingleRowSelect(m.bSelect, m.nStartRow)
        Else
            l_step = Iif(m.nStartRow<m.nEndRow, 1, -1)
            For l_i=m.nStartRow To m.nEndRow Step l_step
                This._SingleRowSelect(m.bSelect, l_i)
            Endfor
        Endif
    Endproc

    *--- Seleziona (bSelect = .t.) o deseleziona (bAction = .f.) una singola riga
    Proc _SingleRowSelect(bSelect, nRow)
        Local tempCtrl, nRowRet, nColRet, nSpanTypeCtrl, l_i
        With This
            *--- Controllo se la riga � gi� selezionata/deselezionata
            If (m.bSelect And Not .isSelected(m.nRow)) Or (Not m.bSelect And .isSelected(m.nRow))
                If Vartype(nRow)<>'L' And m.nRow>=.oViewFrame.oCntCell.nFirstRow And m.nRow<=.oViewFrame.oCntCell.nLastRow
                    For l_i=.oViewFrame.oCntCell.nFirstCol To .oViewFrame.oCntCell.nLastCol
                        .GetCellSize(m.nRow, m.l_i, @nRowRet, @nColRet, @nSpanTypeCtrl)
                        If nSpanTypeCtrl <> 2
                            *--- Cella non figlia
                            m.tempCtrl = "CTRL_"+Trans(m.nRow)+"_"+Trans(m.l_i)
                            m.tempCtrl = .oViewFrame.oCntCell.&tempCtrl
                            m.tempCtrl.Select(m.bSelect,.T.) && senza transizione
                        Endif
                    Endfor
                Endif
                *--- Aggiorno il valore sulla riga selezionata
                *--- Se hola colonna coi check di selezione se presente aggiorno quella altrimenti l'apposita collezione
                If .Parent.bCheckSelect
                    Update (.Parent.cCursor) Set XCHK=Iif(m.bSelect,1,0) Where Recno()=m.nRow
                Endif
                If m.bSelect
                    .aSelected.Add(.aRows(m.nRow), Trans(m.nRow))
                Else
                    .aSelected.Remove(Trans(m.nRow))
                Endif
            Endif
        Endwith
    Endproc

    *--- Verifica se nRow � selezionata
    Procedure isSelected(nRow)
        Local l_OldArea, cCursorAssign, nSelect
        With This
            If .Parent.bCheckSelect
                m.l_OldArea = Select()
                Select(.Parent.cCursor)
                Go Record m.nRow
                m.cCursorAssign = .Parent.cCursor+'.XCHK'
                m.nSelect = &cCursorAssign
                Select(m.l_OldArea)
                Return !Empty(m.nSelect)
            Else
                Return Not Empty(.aSelected.GetKey(Trans(m.nRow)))
            Endif
        Endwith
    Endproc

    *--- Inverte la posizione delle due colonne passate
    *--- (gli indici si riferiscono all'ordine attuale di visualizzazione)
    *--- bNoRefresh : se .t. non ridisegna la griglia
    Proc Swap(nStartCol, nEndCol, bNoRefresh)
        Local bLeft, l_col, l_bOldReQuery
        With This
            If (m.nStartCol>=1 And m.nStartCol<=.ColumnCount) And (m.nEndCol>=1 And m.nEndCol<=.ColumnCount)
                m.bLeft = (m.nStartCol>m.nEndCol)
                For l_col=m.nStartCol To (m.nEndCol-Iif(m.bLeft,-1,1)) Step Iif(m.bLeft,-1,1)
                    ._Shift(m.l_col,m.bLeft)
                Endfor
                If !bNoRefresh && ridisegno la griglia senza rieffettuare la query (non necessario)
                    m.l_bOldReQuery = .Parent.bReQuery
                    .Parent.bReQuery = .F.
                    .Parent._Refresh()
                    .Parent.bReQuery = m.l_bOldReQuery
                Endif
            Endif
        Endwith
    Endproc

    *--- Sposta la colonna attualmente in posizione nCol di una posizione
    *--- bLeft=.f. colonna shiftata a destra
    *--- bLeft=.t. colonna shiftata a sinistra
    Proc _Shift(nCol, bLeft)
        Local l_this, l_other, n_other
        With This
            If (m.nCol>0 And m.nCol<=.ColumnCount) And (m.bLeft And m.nCol>1 Or !m.bLeft And m.nCol<.ColumnCount)
                m.n_other = Iif(m.bLeft, m.nCol-1, m.nCol+1)
                m.l_this = .Columns.Item(Trans(m.nCol))
                m.l_other = .Columns.Item(Trans(m.n_other))
                .Columns.Remove(Trans(m.nCol))
                .Columns.Remove(Trans(m.n_other))
                .Columns.Add(m.l_other,Trans(m.nCol))
                .Columns.Add(m.l_this,Trans(m.n_other))
            Endif
        Endwith
    Endproc

    Procedure _oDummy.GotFocus
        Local l_OldArea
        With This.Parent.Parent
            If .CurrentRow>0 And .CurrentRow<=This.Parent.RowCount
                *--- Aggiorno la posizione sul cursore
                m.l_OldArea = Select()
                Select(This.Parent.Parent.cCursor)
                Go Record This.Parent.Parent.CurrentRow
                Select(m.l_OldArea)
            Endif
        Endwith
    Endproc

    Procedure _oDummy.KeyPress(nKeyCode, nShiftAltCtrl)
        Local nCol, nFirstLeft, l_OldArea
        With This.Parent.oViewFrame.oCntCell.oSelectedRowShape
            If This.Parent.Parent.CurrentRow>0 And This.Parent.Parent.CurrentRow<=This.Parent.RowCount
                Do Case
                    Case nKeyCode = 5 && Up
                        Do Case
                            Case Not This.Parent.Parent.bMultiSelect And This.Parent.Parent.CurrentRow>1
                                This.Parent.RowSelect(.F.,This.Parent.Parent.CurrentRow)
                                This.Parent.RowSelect(.T.,This.Parent.Parent.CurrentRow-1)
                                *--- Aggiorno CurrentRow
                                This.Parent.Parent.CurrentRow = This.Parent.Parent.CurrentRow-1
                                *--- Scrollo se necessario
                                If This.Parent.Parent.CurrentRow<=.Parent.nFirstRow
                                    This.Parent.oVScrollbar.nPosition = This.Parent.oVScrollbar.nPosition-This.Parent.RowHeight
                                Endif
                            Case This.Parent.Parent.bMultiSelect
                                This.Parent.oVScrollbar.nPosition = This.Parent.oVScrollbar.nPosition-This.Parent.RowHeight
                        Endcase
                        *--- Aggiorno la posizione sul cursore
                        m.l_OldArea = Select()
                        Select(This.Parent.Parent.cCursor)
                        Go Record This.Parent.Parent.CurrentRow
                        Select(m.l_OldArea)
                        .Visible = .F.
                        Nodefault
                    Case nKeyCode = 24 && Down
                        Do Case
                            Case Not This.Parent.Parent.bMultiSelect And This.Parent.Parent.CurrentRow<This.Parent.RowCount
                                This.Parent.RowSelect(.F.,This.Parent.Parent.CurrentRow)
                                This.Parent.RowSelect(.T.,This.Parent.Parent.CurrentRow+1)
                                *--- Aggiorno CurrentRow
                                This.Parent.Parent.CurrentRow = This.Parent.Parent.CurrentRow+1
                                *--- Scrollo se necessario
                                If This.Parent.Parent.CurrentRow>=.Parent.nLastRow
                                    This.Parent.oVScrollbar.nPosition = This.Parent.oVScrollbar.nPosition+This.Parent.RowHeight
                                Endif
                            Case This.Parent.Parent.bMultiSelect
                                This.Parent.oVScrollbar.nPosition = This.Parent.oVScrollbar.nPosition+This.Parent.RowHeight
                        Endcase
                        *--- Aggiorno la posizione sul cursore
                        m.l_OldArea = Select()
                        Select(This.Parent.Parent.cCursor)
                        Go Record This.Parent.Parent.CurrentRow
                        Select(m.l_OldArea)
                        .Visible = .F.
                        Nodefault
                    Case nKeyCode = 4 && Right
                        m.nCol = Min(This.Parent.ColumnCount,.Parent.nFirstCol+1)
                        This.Parent.oHScrollbar.nPosition = This.Parent.Columns.Item(Trans(m.nCol)).Left
                        *--- Aggiorno CurrentCol
                        This.Parent.Parent.CurrentCol = Min(This.Parent.ColumnCount,This.Parent.Parent.CurrentCol+1)
                        Nodefault
                    Case nKeyCode = 19 && Left
                        m.nCol = Max(1,.Parent.nFirstCol-1)
                        m.nFirstLeft = This.Parent.Columns.Item(Trans(.Parent.nFirstCol)).Left
                        *--- se la First Col non � perfettamente allineata a sinistra allineo quella...
                        If This.Parent.oHScrollbar.nPosition>m.nFirstLeft
                            This.Parent.oHScrollbar.nPosition = m.nFirstLeft
                        Else &&...altrimenti visualizzo quella precedente
                            This.Parent.oHScrollbar.nPosition = This.Parent.Columns.Item(Trans(m.nCol)).Left
                        Endif
                        *--- Aggiorno CurrentCol
                        This.Parent.Parent.CurrentCol = Max(.Parent.nFirstCol,This.Parent.Parent.CurrentCol-1)
                        Nodefault
                    Case nKeyCode = 13 && Enter
                        Nodefault
                    Case nKeyCode = 9 && Tab
                        Nodefault
                Endcase
            Endif
        Endwith
    Endproc

    *--- BackColor
    Proc BackColor_Assign(xValue)
        With This
            .oViewFrame.oCntCell.BackColor = m.xValue
            .oViewFrame.BackColor = m.xValue
            .oViewFrame.BorderColor = m.xValue
            .BackColor = m.xValue
            *--- Colore Scrollbar
            .oVScrollbar.oScrollThumb.BackColor = .Parent.ScrollBarsColor
            .oVScrollbar.oScrollThumb.BorderColor = .Parent.ScrollBarsColor
            .oHScrollbar.oScrollThumb.BackColor = .Parent.ScrollBarsColor
            .oHScrollbar.oScrollThumb.BorderColor = .Parent.ScrollBarsColor
        Endwith
    Endproc

    *--- ColumnCount
    Proc ColumnCount_Assign(xValue)
        This.ColumnCount = m.xValue
        This.Parent.ColumnCount = m.xValue
    Endproc

    *--- oCntCell Resize
    Proc oViewFrame.oCntCell.Resize
        DoDefault()
        This.Parent.Parent.Parent.hdr.Resize()
    Endproc

    *--- oCntCell Moved
    Proc oViewFrame.oCntCell.Moved
        DoDefault()
        With This.Parent.Parent.Parent
            .hdr.Left = This.Left+.grd.Left
        Endwith
    Endproc

	*--- Zucchetti Aulla - Inizio export grid
	*-- RightClick
    Procedure oViewFrame.oCntCell.oMouseEventShape.RightClick()
    	Public loPopupMenu
    	Local loMenuItem
        loPopupMenu = Createobject("cbPopupMenu")
        *--- Esporta su Excel/Calc
        loPopupMenu.oParentObject = This.Parent.Parent.Parent
        loMenuItem = m.loPopupMenu.AddMenuItem()
        loMenuItem.Caption = cp_Translate(MSG_EXCEL_EXPORT)
        loMenuItem.BeginGroup = .F.
        loMenuItem.Visible = .T.
        loMenuItem.OnClick = "loPopupMenu.oParentObject.CursorExport('E')"
        loMenuItem = m.loPopupMenu.AddMenuItem()
        loMenuItem.Caption = cp_Translate(MSG_DBF_EXPORT)
        loMenuItem.BeginGroup = .F.
        loMenuItem.Visible = .T.
        loMenuItem.OnClick = "loPopupMenu.oParentObject.CursorExport('D')"
        loMenuItem = m.loPopupMenu.AddMenuItem()
        loMenuItem.Caption = cp_Translate(MSG_CSV_EXPORT)
        loMenuItem.BeginGroup = .F.
        loMenuItem.Visible = .T.
        loMenuItem.OnClick = "loPopupMenu.oParentObject.CursorExport('C')"
        loPopupMenu.InitMenu(.T.)
        loPopupMenu.ShowMenu()
        Release loPopupMenu
    EndProc

	Func CursorExport(pParam)
		If Used(This.Parent.cCursor) And InList(pParam,'E','D','C')
			*--- Preparo il nome del file
			Local l_cName, l_cCursor, l_oldArea
			m.l_oldArea = Select()
			If Type('This.Parent.parent.ocontained.w_title')='C' And !Empty(This.Parent.parent.ocontained.w_title)
				m.l_cName = This.Parent.parent.ocontained.w_title
				m.l_cName = Chrtran(m.l_cName,'\/:*?"<>| ','--________')
			Else
				m.l_cName = '*'
			EndIf
			If pParam='E' And Type("g_AUTOFFICE")<>"U" And !g_AUTOFFICE
				m.l_cName = Iif(Type('g_NoAutOfficeDir')<>'U', Addbs(g_NoAutOfficeDir)+m.l_cName, Putfile('Save as', Addbs(g_tempadhoc)+m.l_cName, 'XLSX'))
			Else
				m.l_cName = Putfile('Save as', m.l_cName,  ICase(pParam='E' ,'XLSX',pParam='D','DBF',pParam='C','CSV'))
			EndIf
			If !Empty(m.l_cName)
				*--- Faccio una copia del cursore da esportare
				m.l_cCursor = Sys(2015)
				Select * From (This.Parent.cCursor) Into Cursor (m.l_cCursor) ReadWrite
				m.l_cName = ForceExt(m.l_cName,ICase(pParam='E' ,'XLSX',pParam='D','DBF',pParam='C','CSV'))
				If cp_FileExist(m.l_cName)
					Local l_Err, l_errOld
					m.l_Err = .F.
					l_errOld = On("error")
               		On Error m.l_Err = .T.

					Delete File (m.l_cName)
					
					If m.l_Err
						ah_errormsg(cp_MsgFormat("Accesso negato al file %1", m.l_cName),'!','')
						Return .T.
					EndIf
					On Error &l_errOld
				Endif
				Select(m.l_cCursor)
				Do case					
					Case pParam='E'
						If Type("g_AUTOFFICE")="U" Or g_AUTOFFICE
							cp_cursortoxlsx(m.l_cName ,IIF (EMPTY ( m.l_cName ) , "Foglio1" ,JUSTSTEM(m.l_cName)),SELECT(0))
						Else
							*--- Preparo la stringa per le intestazioni delle colonne
							*--- Esempio formato stringa: "ANTIPCON;Tipo conto;ANCODICE;Intestatario"
							Local cColTitles, cParseChar
							m.cColTitles = ''
							m.cParseChar = ';'
							For Each oCol in This.Columns
								m.cColTitles = m.cColTitles+Alltrim(oCol.ControlSource)+m.cParseChar+Alltrim(oCol.HeaderTitle)+m.cParseChar
							Endfor
							*--- tolgo l'ultimo separatore
							m.cColTitles = Left(m.cColTitles,Len(m.cColTitles)-Len(cParseChar))
							vx_build('X', '', m.l_cCursor, m.l_cName, .F., .T., '', .F., .T., m.cColTitles, m.cParseChar)
						ENDIF
						STAPDF(m.l_cName ,"OPEN"," ")
					Case pParam='D'
						Copy To (m.l_cName)
					Case pParam='C'
						Copy To (m.l_cName) Type CSV
				EndCase
				*--- Chiudo il cursore usato per l'esportazione
				Use In Select(m.l_cCursor)
			Endif
			Select(m.l_oldArea)
		EndIf
	Endfunc
	*--- Zucchetti Aulla - Fine export su grid

Enddefine

Define Class cp_VirtualZoom As Container
    BackStyle = 1
    BorderWidth = 1
    BorderColor = Rgb(92,92,92)
    ColumnCount = 0

	ForeColor = -1
    
    DynamicBackColor = ""
    DynamicForeColor = ""
    DynamicFontBold = ""
    DynamicFontItalic = ""

    *--- HEADER
    nHeaderHeight = 20 && Altezza dell'header
    HeaderBackColor = 0
    HeaderForeColor = -1
    HeaderFontName = "Open Sans"
    HeaderFontSize = 10
    HeaderFontItalic = .F.
    HeaderFontBold = .T.
    HeaderAlignment = 2
    *--- Gestione trascodifica campo-header
    *--- se attiva controlla se nella maschera in cui si trova la griglia sono presenti variabili con il nome nel formato 'w_TRS_'+Alias campo
    *--- e utilizza il valore di tale variabile come titolo della colonna (visualizzato nell'header)
    bTrsHeaderTitle = .F.

	*--- VALORI CELLE
	ValueFontName = "Open Sans"
    ValueFontSize = 10
    ValueFontItalic = .F.
    ValueFontBold = .F.
    
    ScrollBarsWidth = 6 && Larghezza scrollbars

    cZoomFile = ""	&& File dello zoom da visualizzare
    cDataSource = ""  && Sorgente dei dati da visualizzare
    cDataSourceType = ""  && Tipologia della sorgente dei dati da visualizzare
    bReQuery = .T. && se .t. riesegue la query ad ogni refresh dell'oggetto
    bUpdated = .T. && se .t. sono state modificate la sorgente e/o la tipologia dei dati, le strutture verranno reinizializzate dalla _Refresh()

    cCursor = ""  && Cursore contenete i dati da visualizzare
    cWhere = ""  && Filtri sulla query definiti tramite header
    cOrderBy = ""  && Ordinamento sulla query definito tramite header

    bEditable = .F.
    bMultiSelect = .F.
    bCheckSelect = .F.

    nMemoLimit = 300 && caratteri che vengono visualizzati per i campi Memo

    *--- Colori
    bAlternate = .T.
    OddBackColor = Rgb(255,255,255)
    EvenBackColor = Rgb(210,210,210)
    AlphaColor = -1
    EvenForeColor = -1
    OddForeColor = -1
    HotTrackingColor = Rgb(92,92,92)
    HighLightColor =  Rgb(255,255,0)
    ScrollBarsColor = 0


    CurrentRow = 0
    CurrentCol = 0
    
    bListenEvents = .t.

    nPadding = 1 && padding degli elementi interni allo zoom (header e griglia)
    Add Object hdr As cp_ZoomHeader
    Add Object grd As cp_ZoomGrid With RowHeight=18, nRowMinHeight=18, nColMinWidth=150
    Add Object BorderSx As Line With Width=0,Height=0
    Add Object BorderDx As Line With Width=0,Height=0

	cEvent = ''

    Proc Init()
        With This
            .hdr.Move(.nPadding,.nPadding,.grd.oViewFrame.oCntCell.Width,.nHeaderHeight)
            .grd.Move(.nPadding,.hdr.Top+.hdr.Height+.grd.nHOffset,.Width-2,(.Height-.nHeaderHeight-(2*.nPadding)))
            .BorderSx.BorderWidth = .BorderWidth
            .BorderSx.BorderColor = .BorderColor
            .BorderSx.Move(0,0,0,.Height)
            .BorderDx.BorderWidth = .BorderWidth
            .BorderDx.BorderColor = .BorderColor
            .BorderDx.Move(.Width-1,0,0,.Height)
            Bindevent(Thisform,"BackColor",This,"setColor",1)
        Endwith
    Endproc

    *--- Calc
    Proc Calculate(cDataSource)
    	If This.bListenEvents
	        This.cDataSource = m.cDataSource   && Sorgente dei dati da visualizzare
	        This._Refresh()
	    EndIf
    Endproc

    *--- Event
    Proc Event(cEvent)
        Local l_Event
        If This.bListenEvents
	        l_Event = Strtran(This.cEvent, ', ', ',')
	        l_Event = Strtran(m.l_Event, ' ,', ',')
	        If Lower(','+Alltrim(m.cEvent)+',')$Lower(','+Alltrim(m.l_Event)+',')
	            This.bUpdated = .T.
	            This.bReQuery = .T.
	            This._Refresh()
	        Endif
    	Endif
    Endproc

    *--- GetVar
    Proc GetVar(cFieldName)
        Local l_i, l_OldArea, l_recno, nIndex, xFieldValue, cCursorAssign
        With This
            If .CurrentRow<>0 And Not Empty(.cCursor)
                l_i = 1
                m.nIndex = .grd.GetColIndexByName(m.cFieldName)
                If Vartype(m.nIndex)<>'L'
                    l_OldArea = Select()
                    Select(This.cCursor)
                    m.cCursorAssign = .cCursor+'.'+m.cFieldName
                    m.xFieldValue = &cCursorAssign
                    Select(l_OldArea)
                    Return m.xFieldValue
                Else
                    Return .Null.
                Endif
            Else
                Return .Null.
            Endif
        Endwith
    Endproc

    Proc CurrentRow_Assign(xValue)
        Local l_olArea, l_bListenEvents
        With This
            If .CurrentRow <> m.xValue
                .CurrentRow = m.xValue
                If !Empty(.CurrentRow)
                    l_OldArea = Select()
                    Select(.cCursor)
                    Go Record .CurrentRow
                    Select(l_OldArea)
                Endif
                m.l_bListenEvents = .bListenEvents
                .bListenEvents = .f.
                .Parent.oContained.mCalc(.T.)
                .Parent.oContained.SaveDependsOn()
				.bListenEvents = m.l_bListenEvents
            Endif
        Endwith
    Endproc

    *--- se viene scrollato l'header devo scrollare anche gli shape per il resize delle colonne
    Proc hdr.Moved
        Local l_i, cShpDivColName, nDelta
        With This.Parent
            m.nDelta = .hdr.Left - .hdr.nOldLeft
            For l_i = 1 To .ColumnCount-1
                m.cShpDivColName = "SHPDIVCOL_"+Trans(l_i)
                m.cHeaderItemName= "HEADER_"+Trans(l_i)
                .&cShpDivColName..Left = .&cShpDivColName..Left+m.nDelta
                .&cShpDivColName..ZOrder(0)
            Endfor
        Endwith
    Endproc

    Proc nHeaderHeight_Assign(xValue)
        With This
            If .nHeaderHeight<> m.xValue
                .nHeaderHeight= m.xValue
                .hdr.Height = .nHeaderHeight
                .grd.Top = .hdr.Top+.hdr.Height+.grd.nHOffset
            Endif
        Endwith
    Endproc

    Proc bAlternate_Assign(xValue)
        If This.bAlternate <> m.xValue
            This.bAlternate = m.xValue
            This.bUpdated = .T.
            This._Refresh()
        Endif
    Endproc

    Proc bMultiSelect_Assign(xValue)
        If This.bMultiSelect <> m.xValue
            This.bMultiSelect = m.xValue
            If !This.bMultiSelect
                This.grd.DeselectAll()
            Endif
            This._Refresh()
        Endif
    Endproc

    Proc bCheckSelect_Assign(xValue)
        If This.bCheckSelect <> m.xValue
            This.bCheckSelect = m.xValue
            This.bUpdated = .T.
            This.bReQuery = .T.
            This.grd.DeselectAll()
            This._Refresh()
        Endif
    Endproc

    Proc cDataSource_Assign(xValue)
        Local cType
        With This
            If .cDataSource<>m.xValue
                *--- Modifico di conseguenza il cDataSourceType
                m.cType = Upper(Justext(m.xValue))
                Do Case
                    Case m.cType==TYPE_QUERY
                        .cDataSourceType = TYPE_QUERY
                    Case Empty(m.cType) And Used(m.xValue)
                        .cDataSourceType = TYPE_CUR
                Endcase
                .cDataSource = Alltrim(m.xValue)
                .bUpdated = .T.
                .bReQuery = .T.
	            If (!Empty(.cOrderBy) Or !Empty(.cWhere)) And ah_yesno(cp_Translate("Cancellare i criteri di filtro e ordinamento?"),.T.)
	                .cOrderBy = ""
	                .cWhere = ""
	            Endif
            Endif
        Endwith
    Endproc

    Proc cDataSourceType_Assign(xValue)
        With This
            If .cDataSourceType <> m.xValue
                .cDataSourceType = Alltrim(Upper(m.xValue))
                .bUpdated = .T.
                .bReQuery = .T.
                If (!Empty(.cOrderBy) Or !Empty(.cWhere)) And ah_yesno(cp_Translate("Cancellare i criteri di filtro e ordinamento?"),.T.)
                    .cOrderBy = ""
                    .cWhere = ""
                Endif
            Endif
        Endwith
    Endproc

    *--- Rifaccio il colore delle righe pari in base al colore di alpha
    Proc AlphaColor_Assign(xValue)
    	With This
    		.AlphaColor = m.xValue
    		Do Case
    			Case .AlphaColor<>-1
    				.HeaderBackColor = RGBAlphaBlending(Thisform.BackColor, 0, 0.3)
    				.OddBackColor = RGBAlphaBlending(Thisform.BackColor, 0, 0.15)
    				.EvenBackColor = RGBAlphaBlending(.OddBackColor, .AlphaColor, 0.15)
    			Case .AlphaColor==-1 And Vartype(i_ThemesManager)=='O' And Thisform.BackColor==i_ThemesManager.GetProp(7)
    				.HeaderBackColor = Rgb(180,180,180)
    				.OddBackColor = Rgb(250,250,250)
    				.EvenBackColor = Rgb(210,210,210)
    				.HeaderForeColor = Iif(.HeaderForeColor==-1, 0, .HeaderForeColor)
    				.EvenForeColor = Iif(.EvenForeColor==-1, 0, .EvenForeColor)
    				.OddForeColor = Iif(.OddForeColor==-1, 0, .OddForeColor)
    			Otherwise
    				.HeaderBackColor = RGBAlphaBlending(Thisform.BackColor, 0, 0.3)
    				.OddBackColor = RGBAlphaBlending(Thisform.BackColor, 0, 0.15)
    				.EvenBackColor = RGBAlphaBlending(Thisform.BackColor, Rgb(255,255,255), 0.15)
    				.HeaderForeColor = Iif(.HeaderForeColor==-1, Rgb(255,255,255), .HeaderForeColor)
    				.EvenForeColor = Iif(.EvenForeColor==-1, Rgb(255,255,255), .EvenForeColor)
    				.OddForeColor = Iif(.OddForeColor==-1, Rgb(255,255,255), .OddForeColor)
    		Endcase
    	Endwith
    Endproc

	Proc HotTrackingColor_Assign(xValue)
		With This
			If Vartype(m.xValue)='N' And m.xValue>=0 And .HotTrackingColor<>m.xValue
				.HotTrackingColor = m.xValue
				.grd.oViewFrame.oCntCell.oSelectedRowShape.BorderColor = .HotTrackingColor
			Endif
		Endwith
	Endproc

    *--- BackColor
    Proc setColor
        With This
            .AlphaColor = .AlphaColor
            .BackColor = Iif(Thisform.BackColor=Rgb(255,255,255), Rgb(92,92,92), RGBAlphaBlending(Thisform.BackColor, Rgb(255,255,255), 0.7))            
            .grd.BackColor = .BackColor
            .BorderColor = .grd.BackColor
            .ScrollBarsColor = Thisform.BackColor
            .bUpdated = .T.
            ._Refresh()
        Endwith
    Endproc

    Proc BorderWidth_Assign(xValue)
        With This
            .BorderWidth = m.xValue
            .BorderSx.BorderWidth = m.xValue
            .BorderDx.BorderWidth = m.xValue
        Endwith
    Endproc

    Proc BorderColor_Assign(xValue)
        With This
            .BorderColor = m.xValue
            .BorderSx.BorderColor = m.xValue
            .BorderDx.BorderColor = m.xValue
        Endwith
    Endproc

	Proc ForeColor_Assign(xValue)
    	With This
    		If .ForeColor<>m.xValue And m.xValue>-1
	    		.ForeColor = m.xValue
	    		.HotTrackingColor = m.xValue
	    		.HighLightColor =  m.xValue
	    		.HeaderForeColor = m.xValue
	    		.OddForeColor = m.xValue
	    		.EvenForeColor = m.xValue
	    		.bUpdated = .T.
	    		._Refresh()
    		Endif
    	Endwith
    Endproc

    *--- Resize
    Proc Resize
        Local l_i, cShpDivColName, nScrollW
        With This
            .hdr.Resize()
            *.grd.Move(1,.hdr.Height,.Width-2,(.Height-.hdr.Height-2))
            .grd.Move(.nPadding,.hdr.Top+.hdr.Height+.grd.nHOffset,.Width-2,(.Height-.nHeaderHeight-(2*.nPadding)))
            If .hdr.bInitialized
                m.nScrollW = Iif(.grd.oHScrollbar.Visible And .grd.oHScrollbar.Enabled,.grd.oHScrollbar.Height,0)
                For l_i = 1 To .ColumnCount-1
                    m.cShpDivColName = "SHPDIVCOL_"+Trans(l_i)
                    .&cShpDivColName..Move(.&cShpDivColName..Left, 0, 3, .Height-m.nScrollW)
                    .&cShpDivColName..ZOrder(0)
                Endfor
            Endif
            .BorderSx.Move(0,0,0,.Height)
            .BorderDx.Move(.Width-1,0,0,.Height)
        Endwith
    Endproc

    Proc _Refresh()
        With This
            If !Empty(.cDataSource)
            	If (.cDataSourceType==TYPE_CUR And Used(.cDataSource)) Or;
            	   (.cDataSourceType==TYPE_QUERY And cp_FileExist(.cDataSource))
                This.hdr._Clean()
                This.grd._Refresh()
	            Else
	            	If Lower(.Parent.oContained.ParentClass)=="stdgadgetform" And PemStatus(.Parent.oContained,"cAlertMsg",5)
	            		Local l_AlertMsg
	            		m.l_AlertMsg = ah_MsgFormat('Sorgente dati "%1" non definita',.cDataSource)
	            		.Parent.oContained.cAlertMsg = Iif(Empty(.Parent.oContained.cAlertMsg),m.l_AlertMsg,ah_MsgFormat("%1%0%2",.Parent.oContained.cAlertMsg,m.l_AlertMsg))
	            	Endif
	            Endif
            Endif
        Endwith
    Endproc

    *--- Destroy
    Proc Destroy()
        Use In Select(This.cCursor)
        Unbindevents(Thisform,"BackColor",This,"setColor")
        DoDefault()
    Endproc
Enddefine
*--- Zucchetti Aulla - Fine

*--- cp_FooterGadget
Define Class cp_FooterImgGoTo As Container
    BackStyle=0
    BorderWidth = 0
    Width=13
    Height=14
    Add Object Shape1 As Shape With Top=0, Left=0, Width=13, Height=14
    Add Object Shape2 As Shape
    Dimension aShape1[8,2]
    Dimension aShape2[6,2]
    
    ForeColor = Rgb(255,255,255)
    
    Procedure Init
        With This
            *--- Shape1
            .aShape1[1,1] = 0&&A
            .aShape1[1,2] = 0
            .aShape1[2,1] = 100&&B
            .aShape1[2,2] = 0
            .aShape1[3,1] = 100&&C
            .aShape1[3,2] = 100
            .aShape1[4,1] = 0&&D
            .aShape1[4,2] = 100
            .aShape1[5,1] = 0&&E
            .aShape1[5,2] = 16
            .aShape1[6,1] = 93&&F
            .aShape1[6,2] = 16
            .aShape1[7,1] = 93&&G
            .aShape1[7,2] = 8
            .aShape1[8,1] = 0&&H
            .aShape1[8,2] = 8
            .Shape1.Polypoints = "This.Parent.aShape1"
            .Shape1.BackStyle = 0
            .Shape1.BorderColor = This.ForeColor 
            
            .Shape1.MousePointer = 15

            *--- Shape2
            .aShape2[1,1]=80
            .aShape2[1,2]=0
            .aShape2[2,1]=100
            .aShape2[2,2]=0
            .aShape2[3,1]=100
            .aShape2[3,2]=100
            .aShape2[4,1]=0
            .aShape2[4,2]=100
            .aShape2[5,1]=0
            .aShape2[5,2]=80
            .aShape2[6,1]=80
            .aShape2[6,2]=80

            .Shape2.Polypoints = "This.Parent.aShape2"
            .Shape2.Move(5, 6, 5,5)
            .Shape2.BackColor = This.ForeColor 
            .Shape2.BorderColor = This.ForeColor 
            .Shape2.MousePointer = 15
        Endwith
    Endproc

    Procedure mousepointer_assign(xValue)
        This.MousePointer = m.xValue
        This.Shape1.MousePointer = m.xValue
        This.Shape2.MousePointer = m.xValue
    Endproc

	PROCEDURE Shape1.Click()
		This.Parent.Click()
	ENDProc
	
	PROCEDURE Shape2.Click()
		This.Parent.Click()
	EndProc	
	
	PROCEDURE ForeColor_Assign(nValue)
		WITH This
			.ForeColor = m.nValue
			.Shape1.BorderColor = m.nValue
            .Shape2.BackColor = m.nValue
            .Shape2.BorderColor = m.nValue		
		EndWith
	EndProc
	
	PROCEDURE Tooltiptext_Assign(cValue)
		WITH This
			.Tooltiptext = m.cValue
			.Shape1.Tooltiptext = m.cValue
            .Shape2.Tooltiptext = m.cValue
		EndWith
	EndProc	
	
Enddefine

Define Class cp_FooterGadget As Container
    Width = 120
    nHeight = 24
    BorderWidth = 0
    BackStyle=0
    cEvent=''
    MousePointer=15

    cProgram=''
    FontName="Open Sans"
    FontSize=8
    Fontcolor=Rgb(255,255,255)
    nBackColor=-1

    Add Object oLblUpdateDate As Label With Width = 68, BackStyle=0, ForeColor=Rgb(255,255,255),;
        Height=24, FontName="Open Sans",FontSize=8,FontItalic=.T.,MousePointer=15,Caption = '', AutoSize=.T.
    Add Object oImgGoTo As cp_FooterImgGoTo With Width = 14, Height=14, Visible=.F., MousePointer=15

    Procedure Init
        With This
            *.oImgGoTo.Picture = Forceext(i_ThemesManager.RetBmpFromIco("gd_Goto.bmp", 16),"bmp")
            .Anchor=0
            .Move(1,.Parent.Height-.nHeight, .Parent.Width-2, .nHeight)
            .Anchor=14
            .oLblUpdateDate.Anchor=0
            .oLblUpdateDate.Move(5, .Height*0.5-.oLblUpdateDate.Height*0.5)
            .oLblUpdateDate.Anchor=514
            .oImgGoTo.Anchor=0
            .oImgGoTo.Move(.Width-.oImgGoTo.Width-5, .Height*0.5-.oImgGoTo.Height*0.5)
            .oImgGoTo.Anchor=520
        Endwith
    Endproc

    *--- Calc
    Proc Calculate(cDate, cProgram, cToolTip)
        With This
	        cToolTip = EVL(m.cToolTip, "")	&&Pu� essere false
            .oLblUpdateDate.Caption = Alltrim(m.cDate)
            .cProgram = m.cProgram
            If !Empty(m.cProgram)
                .oImgGoTo.Visible=.T.
                .oImgGoTo.MousePointer=15
                .MousePointer=15
                .oLblUpdateDate.MousePointer=15
                .Tooltiptext = m.cToolTip
                .oLblUpdateDate.Tooltiptext = m.cToolTip
                .oImgGoTo.Tooltiptext = m.cToolTip
            Else
                .oImgGoTo.Visible=.F.
                .oImgGoTo.MousePointer=0
                .MousePointer=0
                .oLblUpdateDate.MousePointer=0
                .Tooltiptext = ""
                .oLblUpdateDate.Tooltiptext = ""
                .oImgGoTo.Tooltiptext = ""               
            Endif
        Endwith
    Endproc

    *--- Event
    Proc Event(cEvent)
    Endproc

*!*	    Procedure Resize()
*!*	        This.oImgGoTo.Left=This.Width - 20
*!*	    Endproc

    Procedure Click()
        If !Empty(This.cProgram)
            Local l_oldError,ggname
            *--- Nascondo l'eventuale ToolTip
	        If Vartype(oGadgetManager)='O' And PemStatus(oGadgetManager,'oBalloon',5)
	        	oGadgetManager.oBalloon.ctlHide(2)
	        	oGadgetManager.oBalloon.ctlVisible = .F.
	        Endif
	        This.ReleaseToolTipTimer(This.Parent.oContained)
	        *--- Catturo l'eventuale errore per inserirlo nell'alert del Gadget
            l_cProgram = This.cProgram
            With Thisform
            	m.ggname = 'frm'+Alltrim(.Name)
		       	m.l_oldError = On("Error")
		       	On Error &ggname..cAlertMsg=Iif(Empty(&ggname..cAlertMsg),'Footer: '+Message(),ah_MsgFormat("%1%0%2",&ggname..cAlertMsg,'Footer: '+Message()))
                &l_cProgram
                On error &l_oldError
            Endwith
        Endif
    Endproc

    Procedure oImgGoTo.Click()
        This.Parent.Click()
    Endproc

    Procedure oLblUpdateDate.Click()
        This.Parent.Click()
    Endproc

    Procedure FontName_Assign(xValue)
        This.oLblUpdateDate.FontName = m.xValue
    Endproc

    Procedure FontSize_Assign(xValue)
        This.oLblUpdateDate.FontSize = m.xValue
    Endproc

    Procedure Fontcolor_Assign(xValue)
        This.oLblUpdateDate.ForeColor = m.xValue
        This.oImgGoTo.ForeColor = m.xValue
    Endproc

    Procedure nBackColor_Assign(xValue)
        This.nBackColor = m.xValue
        If m.xValue<>-1
            This.BackColor = m.xValue
            This.BackStyle= 1
        Else
            This.BackStyle= 0
        Endif
    Endproc

	Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
		This.SetToolTipTimer(This.Parent.oContained)
	Endproc

	Procedure oLblUpdateDate.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
		This.Parent.SetToolTipTimer(This.Parent.Parent.oContained)
	Endproc

	Procedure oImgGoTo.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
		This.Parent.SetToolTipTimer(This.Parent.Parent.oContained)
	Endproc
	
	Procedure SetToolTipTimer(oGadget)
		*--- Attivo il timer per la visualizzazione del tooltip
		If Vartype(m.oGadget.oToolTipTimer)='O'
			m.oGadget.oToolTipTimer.Enabled = .T.
		Else
			m.oGadget.oToolTipTimer = Createobject("ToolTipGadgetTimer")
		Endif
	Endproc

	Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
		This.ReleaseToolTipTimer(This.Parent.oContained)
	Endproc

	Procedure oLblUpdateDate.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
		This.Parent.ReleaseToolTipTimer(This.Parent.Parent.oContained)
	Endproc

	Procedure oImgGoTo.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
		This.Parent.ReleaseToolTipTimer(This.Parent.Parent.oContained)
	Endproc
	
	Procedure ReleaseToolTipTimer(oGadget)
		*--- Disattivo il timer per la visualizzazione del tooltip
		If Vartype(m.oGadget.oToolTipTimer)='O'
			m.oGadget.oToolTipTimer.Enabled = .F.
		Endif
		m.oGadget.oToolTipTimer = .null.
	Endproc
Enddefine

*--- Zucchetti Aulla Inizio - AsyncTaskManager
Define Class AsyncTaskManager As Custom
	cTaskID = ""
	cTaskCmd = ""
	cCallBackCmd = ""
	nTimeOut = 0
	nDefaultTimeOut = 15*60 && 15 minuti
	oWaitingTimer = .null.
	
	Proc Init
		This.oWaitingTimer = CreateObject("WaitingTaskTImer",This)
		*--- Eseguo il primo Task
		This.ExecTask()
	Endproc
	
	*--- Esegue il primo task nella coda
	Proc ExecTask()
		Local l_DDEserver,l_TaskCursor,l_oldArea
		m.l_DDEserver = This.GetDDEServerName()
		m.l_TaskCursor = "Async"+Sys(2015)
		This.QueryExec("select * from ASYNTASK where ATSRVDDE="+cp_ToStrODBC(m.l_DDEserver)+" And AT_STATO='W' order by AT_DTINS",m.l_TaskCursor)
		If Empty(This.cTaskCmd) And Reccount(m.l_TaskCursor)>0 && Ci sono task in coda
			*--- Salvo la vecchia area di lavoro
			m.l_oldArea = Select()
			*--- Recupero i dati del task da eseguire
			Select(m.l_TaskCursor)
			Go Top
			This.cTaskID = Alltrim(Nvl(&l_TaskCursor..ATCODICE,''))
			This.cTaskCmd = Alltrim(Nvl(&l_TaskCursor..AT__TASK,''))
			This.nTimeOut = Max(Nvl(&l_TaskCursor..ATTIMOUT,This.nDefaultTimeOut),0)
			This.cCallBackCmd = Alltrim(Nvl(&l_TaskCursor..AT_CBACK,''))
			*--- Ripristino la vecchia area di lavoro
			Select(m.l_oldArea)
			
			If !Empty(This.cTaskID) And !Empty(This.cTaskCmd)
				Local bService,nPArAt,l_Task,nParCount,l_Params
				Local l_Par1,l_Par2,l_Par3,l_Par4,l_Par5,l_Par6,l_Par7,l_Par8,l_Par9,l_Par10,l_ParIdx,l_OldPar
				Local l_oldErr, bEvalErr, nEvalErr
				m.bService = Vartype(i_DDEServiceName)='C' And !Empty(i_DDEServiceName)
				*--- Se disponibile passo il task al servizio
				If m.bService
					m.nParAt = At('(',This.cTaskCmd)
					Do case 
						Case m.nParAt=0 && Nessuna parentesi
							m.l_Task = This.cTaskCmd
							m.nParCount = 1
							m.l_Par1 = This.cTaskID
						Case Right(This.cTaskCmd,2)='()' && Parentesi vuota, nessun parametro
							m.l_Task = Left(This.cTaskCmd,Len(This.cTaskCmd)-2)
							m.nParCount = 1
							m.l_Par1 = This.cTaskID
						Otherwise && Sono presenti dei parametri
							m.l_Task = Left(This.cTaskCmd,m.nParAt-1)
							m.l_Params = SubStr(This.cTaskCmd,m.nParAt)
							m.l_Params = SubStr(m.l_Params,2,Len(m.l_Params)-2) && Tolgo le parentesi							
							m.nParCount = GetWordCount(m.l_Params, ',')
							m.l_OldPar = ''
							m.nEvalErr = 0
							For l_idx=1 To m.nParCount
								m.l_ParIdx = "l_Par"+Tran(m.l_idx-m.nEvalErr)
								
								m.bEvalErr = .F.
								m.l_oldErr = On("Error")
								On Error bEvalErr=.T.
								&l_ParIdx = Evaluate(m.l_OldPar+GetWordNum(m.l_Params, m.l_idx, ','))
								On Error &l_oldErr
								
								*--- Se c'� stato un errore durante la valutazione potrei avere una virgola all'interno di un parametro
								*--- di tipo stringa, quindi provo a concatenare il pezzo successivo e rifare la valutazione
								If m.bEvalErr
									m.l_OldPar = m.l_OldPar+GetWordNum(m.l_Params, m.l_idx, ',') + ','
									m.nEvalErr = m.nEvalErr + 1 
								Else
									m.l_OldPar = ''
								Endif
							EndFor
							
							If !m.bEvalErr
								*--- Nessun errore: aggiungo l'ID del Task come ultimo parametro
								m.nParCount = m.nParCount - m.nEvalErr + 1
								m.l_ParIdx = "l_Par"+Tran(m.nParCount)
								&l_ParIdx = This.cTaskID
							Else
								*--- Errore: non passo il task al servizio
								m.bService = .F.
							Endif
					Endcase
					If m.bService
						m.bService = Ah_TaskExec(m.l_Task,m.nParCount,m.l_Par1,m.l_Par2,m.l_Par3,m.l_Par4,m.l_Par5,m.l_Par6,m.l_Par7,m.l_Par8,m.l_Par9,m.l_Par10)==0
						*--- Avvio il timer per attendere il completamento
						This.oWaitingTimer.Enabled = .T.
					Endif
				Endif
				
				If !m.bService
					*--- Se non � stato possibile inoltrare il task al servizio disattivo il timer
					*--- e tento di farlo eseguire direttamente al processo principale
					This.oWaitingTimer.Enabled = .F.
					*--- Aggiungo l'ID del Task come ultimo parametro
					If Right(This.cTaskCmd,1)=')'
						m.l_Task = Left(This.cTaskCmd,Len(This.cTaskCmd)-1)+",'"+This.cTaskID+"')"
					Else
						m.l_Task = This.cTaskCmd+"('"+This.cTaskID+"')"
					Endif
					ExecScript(m.l_Task)
					This.ExecCallBackCmd()
				Endif
			Else
				If Empty(This.cTaskID)
					This.QueryExec("update ASYNTASK set;
					 AT_STATO='X',;
					 ATRETURN='Il campo ATCODICE non deve essere vuoto o nullo';
					 where ATCODICE="+cp_ToStrODBC(This.cTaskID))
				Else
					This.QueryExec("update ASYNTASK set;
					 AT_STATO='X',;
					 ATRETURN='Il campo AT__TASK non deve essere vuoto o nullo';
					 where ATCODICE="+cp_ToStrODBC(This.cTaskID))
				Endif
				*--- Sbianco le variabili relative al Task corrente
				This.cTaskID = ""
				This.cTaskCmd = ""
				This.cCallBackCmd = ""
				This.nTimeOut = This.nDefaultTimeOut
			Endif
		Endif
		Use in (m.l_TaskCursor)
	Endproc
	
	*--- Esegue la CallBack del task corrente
	Proc ExecCallBackCmd()
		Local l_oldArea,l_CallBack
		If !Empty(This.cCallBackCmd)
			*--- Salvo la vecchia area di lavoro
			m.l_oldArea = Select()
			*--- Eseguo la CallBack
			If At('(',This.cCallBackCmd)>0
				m.l_CallBack = Strtran(This.cCallBackCmd,')',','+cp_ToStrODBC(This.cTaskID)+')')
			Else
				m.l_CallBack = This.cCallBackCmd+'('+cp_ToStrODBC(This.cTaskID)+')'
			Endif
			ExecScript(m.l_CallBack)
			*--- Ripristino la vecchia area di lavoro
			Select(m.l_oldArea)
		Endif
		*--- Cancello il record dalla tabella di confine
		This.RemoveTask()
		*--- Sbianco le variabili relative al Task corrente
		This.cTaskID = ""
		This.cTaskCmd = ""
		This.cCallBackCmd = ""
		This.nTimeOut = This.nDefaultTimeOut
		*--- Eseguo il prossimo Task
		This.ExecTask()
	Endproc
	
	*--- Inserisce il task corrente nella coda
	Proc AddTask(cTaskCmd,cCallBackCmd,nTimeOut)
		Local l_Task,l_DDEserver,l_TaskCmd,Cl_TimeOut,l_CallBack
		If Vartype(m.cTaskCmd)='C' And !Empty(m.cTaskCmd)
			m.cTaskCmd = Alltrim(m.cTaskCmd)
			m.l_TaskID = Sys(2015)
			m.l_DDEserver = This.GetDDEServerName()
			m.l_TimeOut = Iif(Vartype(nTimeOut)='N', m.nTimeOut, This.nDefaultTimeOut)
			m.l_CallBack = Iif(Vartype(cCallBackCmd)='C', Alltrim(m.cCallBackCmd), "")
			This.QueryExec(ah_MsgFormat("insert into ASYNTASK (ATCODICE,ATSRVDDE,AT__TASK,ATTIMOUT,AT_CBACK,AT_DTINS) values(%1,%2,%3,%4,%5,%6)",;
				cp_ToStrODBC(m.l_TaskID),cp_ToStrODBC(m.l_DDEserver),cp_ToStrODBC(m.cTaskCmd),;
				cp_ToStrODBC(m.l_TimeOut),cp_ToStrODBC(m.l_CallBack),cp_ToStrODBC(Datetime())))
		*--- Provo ad eseguire il Task
		This.ExecTask()
		Endif
	Endproc

	*--- Rimuove il task dalla coda
	Proc RemoveTask(cTaskId)
		Local l_TaskId
		*--- Se non � stato passato alcun task utilizzo quello corrente
		m.l_TaskId = Iif(Vartype(m.cTaskId)<>'C', This.cTaskID, Alltrim(m.cTaskId))
		If!Empty(m.l_TaskId)
			This.QueryExec("delete from ASYNTASK where ATCODICE="+cp_ToStrODBC(m.l_TaskId))
		Endif
	Endproc

	*--- Restituisce il nome del server DDE utilizzato per fare filtro sulla cosa
	Proc GetDDEServerName()
		Return Iif(Vartype(i_DDEServiceName)='C' And !Empty(i_DDEServiceName), i_DDEServiceName, "*!*"+Tran(i_codute)+Alltrim(i_codazi)+Dtoc(i_datsys,1)+"*!*")
	Endproc

	*--- Restituisce lo stato del task corrente
	Proc ReadState(cTaskId)
		Local l_Stato,l_TaskId,l_ReadCursor,l_oldArea
		*--- Se non � stato passato alcun task utilizzo quello corrente
		m.l_TaskId = Iif(Vartype(m.cTaskId)<>'C', This.cTaskID, Alltrim(m.cTaskId))
		m.l_ReadCursor = "Async"+Sys(2015)
		This.QueryExec("select AT_STATO from ASYNTASK where ATCODICE="+cp_ToStrODBC(m.l_TaskId),m.l_ReadCursor)
		If Reccount(m.l_ReadCursor)==1
			*--- Salvo la vecchia area di lavoro
			m.l_oldArea = Select()
			Select(m.l_ReadCursor)
			m.l_Stato = Nvl(&l_ReadCursor..AT_STATO,'')
			*--- Ripristino la vecchia area di lavoro
			Select(m.l_oldArea)
		Else
			m.l_Stato = ''
		Endif
		Use in (m.l_ReadCursor)
		Return m.l_Stato
	Endproc
	
	*--- Modifica il record del task corrente per indicare il timeout
	Proc SetTimeOut(cTaskId)
		Local l_TaskId
		*--- Se non � stato passato alcun task utilizzo quello corrente
		m.l_TaskId = Iif(Vartype(m.cTaskId)<>'C', This.cTaskID, Alltrim(m.cTaskId))
		If!Empty(m.l_TaskId)
			This.QueryExec("update ASYNTASK set AT_STATO='T' where ATCODICE="+cp_ToStrODBC(m.l_TaskId))
			*--- Riavvio un nuovo servizio per evitare che resti bloccato su questo task
			Release i_DDEchannel && resetto il canale per essere sicuro che il servizio venga terminato immediatamente
			ah_StartService(i_DDEServiceName)
		Endif
	Endproc
	
	*--- Esegue una query e ne inserisce il contenuto nel cursore
	Proc QueryExec(cQuery, cCursor)
		Local l_oldArea, bCursor
		If Vartype(m.cQuery)='C' And !Empty(m.cQuery)
			*--- Se non � stato passato un cursore ne creo uno io e lo cancello prima di uscire
			If Vartype(m.cCursor)='C' And !Empty(m.cCursor)
				m.bCursor = .T.
				m.cCursor = Alltrim(m.cCursor)
			Else
				m.bCursor = .F.
				m.cCursor = "Tmp"+Sys(2015)
			Endif
			m.cQuery = Alltrim(m.cQuery)
			*--- Salvo la vecchia area di lavoro
			m.l_oldArea = Select()
			If i_ServerConn[1,2]<>0
		        cp_sqlexec(i_ServerConn[1,2],m.cQuery, m.cCursor)
		    Else
		        Local l_Query
		        m.l_Query = ah_MsgFormat([%1 Into Cursor "%2" nofilter],m.cQuery,m.cCursor)
		        &l_Query
		    Endif
			*--- Se non � stato passato un cursore ne creo uno io e lo cancello prima di uscire
			If Used(m.cCursor) And !m.bCursor
				Use in (m.cCursor)
			Endif
			*--- Ripristino la vecchia area di lavoro
			Select(m.l_oldArea)
		Endif
	Endproc
	
	*--- Modifica il record del task corrente per indicare il timeout
	Proc Destroy()
		DoDefault()
		This.oWaitingTimer.Enabled = .F.
		This.oWaitingTimer.oManager = .null.
		This.oWaitingTimer = .null.
	Endproc
Enddefine

Define Class WaitingTaskTImer As Timer
	Interval = 200
	oManager = .null.
	nStartSeconds = 0
	
	Proc Init(oManager,nInterval)
		This.oManager = m.oManager
		This.Interval = Iif(Vartype(m.nInterval)='N' And m.nInterval>0, m.nInterval, This.Interval)
		This.Enabled = .F.
	Endproc
	
	Proc Timer
		With This
		If Vartype(.oManager)='O' && verifico che il Manager sia ancora presente
			Local l_Stato
			m.l_Stato = .oManager.ReadState()
			If m.l_Stato<>'W'
				.Enabled = .F.
			Else
				*--- Verifico se � stato seperato il timeout
				If .oManager.nTimeOut>0 And seconds()-.nStartSeconds>=.oManager.nTimeOut
					.Enabled = .F.
					.oManager.SetTimeOut()
				Endif
			Endif
			*--- Se ho disattivato il Timer devo eseguire la funzione di CallBack
			If !.Enabled
				.oManager.ExecCallBackCmd()
			Endif
		Else
			.Enabled = .F.
		Endif
		Endwith
	Endproc

	Proc Enabled_Assign(xValue)
		This.Enabled = m.xValue
		If This.Enabled
			This.nStartSeconds = Seconds()
		Else
			This.nStartSeconds = 0
		Endif
	Endproc
Enddefine
*--- Zucchetti Aulla Fine - AsyncTaskCmdManager
