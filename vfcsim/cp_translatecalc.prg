* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_TRANSLATECALC
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Zucchetti Spa (EO)
* Data creazione: 06/01/2006
* Aggiornato il :
* Translated    : 06/01/2006
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Traduzione a runtime di un file STC
* ----------------------------------------------------------------------------
#include "cp_app_lang.inc"
Parameters p_ExcelName,p_cOrigLang,p_cDestLang,p_cSessionCode
*
p_cSessionCode=Iif(Vartype(p_cSessionCode)<>'C' Or Empty(p_cSessionCode), Sys(2015), p_cSessionCode)
*
Local i_cExpr, i_cNewExcel, i_cTmp1, i_cTmp2, o_cExpr, i_cLanguageReport, i_cOldError, i_lErr, i_nErr, i_cOldArea, i_whileOk
Local loSManager, loSDesktop, loReflection, loPropertyValue, loStarCalc, enumSheets, curSheet
Local Array noArgs[2]

i_whileOk=.T.
i_firstSheet=.T.
i_cExpr=""
i_cNewExcel=""
i_cOldArea=Alias()
i_cOldError=On('ERROR')
i_lErr=.F.
i_nErr=0
p_ExcelName = Alltrim(p_ExcelName)

* Check parametri
If Empty(p_cOrigLang) Or Empty(p_cDestLang) Or Empty(p_ExcelName)
    * Errore, restituisce nome Calc vuoto !
    i_cNewExcel=""
    i_lErr = .T.
Endif
* Controllo preliminare sulla lingua del foglio Calc
If Not(i_lErr) And p_cOrigLang=p_cDestLang
    * Il foglio � gi� nella lingua desiderata, va bene cos� !
    i_cNewExcel=p_ExcelName
    i_lErr = .T.
Endif
* Esegue traduzione del foglio Calc
If Not(i_lErr)
    * Nuovo nome foglio Excel
    i_cNewExcel = TEMPADHOC()+'\'+Substr(p_ExcelName, Rat('\',p_ExcelName)+1)+'_'+Alltrim(p_cDestLang)+'_'+Alltrim(p_cSessionCode)
    If Not(cp_fileexist(i_cNewExcel+'.stc'))
        * Esegue copia del file originale
        i_cTmp1 = p_ExcelName+'.stc'
        i_cTmp2 = i_cNewExcel+'.*'
        * Set errori
        On Error i_lErr=.T.
        * Copia il file originale
        Copy File "&i_cTmp1" To "&i_cTmp2"
        * Apro il nuovo foglio Calc ed eseguo le traduzioni delle labels e delle celle di tipo stringa...
        If Not(i_lErr) And cp_fileexist(i_cNewExcel+".stc")
            * Apro il foglio...
            OpenFile = "file:///"+Strtran(i_cNewExcel+".stc",Chr(92),Chr(47))
            loSManager = Createobject("Com.Sun.Star.ServiceManager")
            loSDesktop = loSManager.createInstance("com.sun.star.frame.Desktop")
            loSFrame = loSManager.createInstance("com.sun.star.frame")
            Comarray(loSDesktop, 10)
            loReflection = loSManager.createInstance("com.sun.star.reflection.CoreReflection" )
            Comarray( loReflection, 10 )
            noArgs[1] = createStruct( @loReflection,"com.sun.star.beans.PropertyValue" )
            noArgs[1].Name = "ReadOnly"
            noArgs[1].Value = .F.
            noArgs[2] = createStruct(@loReflection,"com.sun.star.beans.PropertyValue" )
            noArgs[2].Name = "MacroExecutionMode"
            noArgs[2].Value = 4
            noArgs[3] = createStruct(@loReflection,"com.sun.star.beans.PropertyValue" )
            noArgs[3].Name = "Hidden"
            noArgs[3].Value = .T.
            loStarCalc = loSDesktop.loadComponentFromUrl( OpenFile, "_blank", 0, @noArgs )
            oSheets = loStarCalc.Sheets
            * Calcolo il numero di fogli
            nSheets = loStarCalc.Sheets.Count
            * Ciclo su tutti i fogli
            For s=0 To nSheets-1
                curSheet = oSheets.getByIndex(s)
                Wait Window "Translating Calc [ Sheet("+Alltrim(Str(s+1,5,0))+") ] ..." Nowait
                * Calcolo il max del numero di colonne e di righe non vuote
                oCell = curSheet.GetCellbyPosition( 0, 0 )
                oCursor = curSheet.createCursorByRange(oCell)
                oCursor.GotoEndOfUsedArea(True)
                aAddress = oCursor.RangeAddress
                MaxRow = aAddress.EndRow
                MaxColumn = aAddress.EndColumn
                *** Ciclo sulle celle...
                For i=0 To MaxColumn
                    For r=0 To MaxRow
                        i_cExpr=curSheet.GetCellbyPosition(i,r).String
                        if type('i_cExpr')='C' AND !((i_cExpr=='BODY') OR (i_cExpr=='ENDBODY') OR (LEFT(i_cExpr,1)=='=')) AND i_cExpr = curSheet.getCellByPosition(i,r).formula
                            curSheet.GetCellbyPosition(i,r).String=cp_Translate(i_cExpr,p_cOrigLang,p_cDestLang)
                        Endif
                    Endfor
                Endfor
            Endfor
            noArgs[1].Name = "Overwrite"
            noArgs[1].Value = .T.
            loStarCalc.StoreAsUrl( OpenFile, @noArgs )
            loStarCalc.dispose()
        Else
            * Svuota nome del foglio calc
            i_cNewExcel=""
            Do cp_ErrorMsg With cp_MsgFormat(MSG_CANNOT_COPY_REPORT,Chr(10),i_cTmp1,i_cTmp2),,48,"",.F.
        Endif
    Endif
Endif
* Check Error
On Error &i_cOldError
* Riposiziona area
If Not(Empty(i_cOldArea))
    Select &i_cOldArea
Endif
Wait Clear
* Ritorna eventuale nome del foglio Excel tradotto
Return(i_cNewExcel)
Function createStruct( toReflection, tcTypeName )
    Local loPropertyValue, loTemp
    loPropertyValue = Createobject( "relation")
    toReflection.forName( tcTypeName ).Createobject( @loPropertyValue )
    Return( loPropertyValue )
Endfunc
