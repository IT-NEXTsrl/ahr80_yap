* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_writemult                                                    *
*              Write fields multilanguage                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-31                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_writemult",oParentObject)
return(i_retval)

define class tcp_writemult as StdBatch
  * --- Local variables
  w_idxlang = 0
  w_NT = 0
  w_nConn = 0
  w_cTable = space(100)
  w_cUpd = space(200)
  w_nRes = 0
  w_GESTDEF = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- scrivo nella tabella i valori dei campi multilingua
    this.w_idxlang = alen(i_aCpLangs)+1
    this.w_NT = cp_TablePropScan(this.oParentObject.w_NameTable)
    if this.w_NT<>0
      this.w_GESTDEF = this.oParentObject.oParentObject.parent.ocontained
      this.w_nConn = i_TableProp[this.w_NT,3]
      this.w_cTable = cp_SetAzi(i_TableProp[this.w_NT,2])
      this.w_cUpd = ""
      for i=1 to this.w_idxlang
      if i<>1
        this.w_cUpd = this.w_cUpd+","
      endif
      this.w_cUpd = this.w_cUpd+alltrim(this.oParentObject.w_VARIABLE)
      if i<>1
        this.w_cUpd = this.w_cUpd+"_"+trim(i_aCpLangs(i-1))
      endif
      if empty( this.oParentObject.Ctrls[i*2].value )
        this.w_cUpd = this.w_cUpd+"=NULL"
      else
        this.w_cUpd = this.w_cUpd+"="+cp_ToStrODBC( this.oParentObject.Ctrls[i*2].value )
      endif
      endfor
      * --- Try
      local bErr_04D7C4D0
      bErr_04D7C4D0=bTrsErr
      this.Try_04D7C4D0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04D7C4D0
      * --- End
    endif
    this.oParentObject.ecpQuit()
  endproc
  proc Try_04D7C4D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    local i_ccchkf, i_ccchkw 
 i_ccchkf="" 
 i_ccchkw="" 
 this.w_GESTDEF.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,.t.,this.w_NT,this.w_nConn,("Detail"$cp_getEntityType(this.w_GESTDEF.cprg) and this.oParentObject.oParentObject.nPag=2))
    this.w_nRes = cp_TrsSQL(this.w_nConn,"UPDATE "+this.w_cTable+" SET "+this.w_cUpd+i_ccchkf+" where "+this.oParentObject.w_cWhere+i_ccchkw)
    * --- commit
    cp_EndTrs(.t.)
    if "Detail"$cp_getEntityType(this.w_GESTDEF.cprg) and this.oParentObject.oParentObject.nPag=2
      this.w_GESTDEF.SetCCCHKCursor(i_ccchkf,this.w_NT)
    else
      this.w_GESTDEF.loadRecWarn() 
 this.w_GESTDEF.Refresh()
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
