* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_setuptrace                                                   *
*              Activity logger setting                                         *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-23                                                      *
* Last revis.: 2012-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_setuptrace
If i_ServerConn[1,2]=0
   cp_ErrorMsg(cp_translate("Activity Logger non disponibile per database Visual FoxPro"))
   return
EndIf
* --- Fine Area Manuale
return(createobject("tcp_setuptrace",oParentObject))

* --- Class definition
define class tcp_setuptrace as StdForm
  Top    = 17
  Left   = 10

  * --- Standard Properties
  Width  = 732
  Height = 521
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-09"
  HelpContextID=219688695
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_setuptrace"
  cComment = "Activity logger setting"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ATTIVATO = 0
  o_ATTIVATO = 0
  w_POSIZIONELOG = space(250)
  w_NOMELOG = space(250)
  w_MULTISESSION = 0
  w_TIPOROLLOVER = space(1)
  o_TIPOROLLOVER = space(1)
  w_ROLLOVER = 0
  w_NUMERODIFILEDILOG = 0
  w_CONFIGURAZIONI = space(1)
  w_TRACCIATURASQLLETTURA = 0
  w_TRACCIATURASQLLETTURA = 0
  w_TRACCIATURASQLSCRITTURA = 0
  w_TRACCIATURASQLINSERIMENTO = 0
  w_TRACCIATURASQLCANCELLAZIONE = 0
  w_TRACCIATURAVQR = 0
  w_TRACCIATURAGESTIONI = 0
  w_TRACCIATURAROUTINE = 0
  w_TRACCIATURAFUNZIONI = 0
  w_TRACCIATURATASTIFUN = 0
  w_TRACCIATURAEVENTI = 0
  w_RIGHE = 0
  w_MILLISECONDI = 0
  w_TRACCIATURASQLMODIFICADB = 0
  w_TRACCIATURASQLTEMPORANEO = 0
  w_TRACCIATURASQLALTROSUDB = 0
  w_TRACCIATURATRANSAZIONI = 0
  w_TRACCIATURAQUERYASINCRONA = 0
  w_TRACCIATURADEADLOCK = 0
  w_TRACCIATURARICONNESSIONE = 0
  w_DEBUG = 0
  w_FILECNF = space(250)
  w_SESSIONNAME = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_setuptracePag1","cp_setuptrace",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATTIVATO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ATTIVATO=0
      .w_POSIZIONELOG=space(250)
      .w_NOMELOG=space(250)
      .w_MULTISESSION=0
      .w_TIPOROLLOVER=space(1)
      .w_ROLLOVER=0
      .w_NUMERODIFILEDILOG=0
      .w_CONFIGURAZIONI=space(1)
      .w_TRACCIATURASQLLETTURA=0
      .w_TRACCIATURASQLLETTURA=0
      .w_TRACCIATURASQLSCRITTURA=0
      .w_TRACCIATURASQLINSERIMENTO=0
      .w_TRACCIATURASQLCANCELLAZIONE=0
      .w_TRACCIATURAVQR=0
      .w_TRACCIATURAGESTIONI=0
      .w_TRACCIATURAROUTINE=0
      .w_TRACCIATURAFUNZIONI=0
      .w_TRACCIATURATASTIFUN=0
      .w_TRACCIATURAEVENTI=0
      .w_RIGHE=0
      .w_MILLISECONDI=0
      .w_TRACCIATURASQLMODIFICADB=0
      .w_TRACCIATURASQLTEMPORANEO=0
      .w_TRACCIATURASQLALTROSUDB=0
      .w_TRACCIATURATRANSAZIONI=0
      .w_TRACCIATURAQUERYASINCRONA=0
      .w_TRACCIATURADEADLOCK=0
      .w_TRACCIATURARICONNESSIONE=0
      .w_DEBUG=0
      .w_FILECNF=space(250)
      .w_SESSIONNAME=space(10)
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
          .DoRTCalc(1,4,.f.)
        .w_TIPOROLLOVER = 'D'
        .w_ROLLOVER = iif( .w_TIPOROLLOVER='D' , 1900, 10000 )
          .DoRTCalc(7,7,.f.)
        .w_CONFIGURAZIONI = "0"
          .DoRTCalc(9,28,.f.)
        .w_DEBUG = 0
      .oPgFrm.Page1.oPag.oObj_1_57.Calculate(iif( .w_TIPOROLLOVER='D' , 'Raggiunta la dimensione in Megabyte specificata  la traccia viene rinominata e ne viene creata una nuova' ,  'Raggiunto il numero di record indicato la traccia viene rinominata e ne viene creata una nuova' ))
      .oPgFrm.Page1.oPag.oObj_1_58.Calculate(IIF( INLIST( .w_ATTIVATO, 1, 3 ), iif( .w_TIPOROLLOVER='D' , 'Dimensione (MB):', 'Numero record per log:' ),''))
      .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
    endwith
    this.DoRTCalc(30,31,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .DoRTCalc(1,5,.t.)
        if .o_TIPOROLLOVER<>.w_TIPOROLLOVER
            .w_ROLLOVER = iif( .w_TIPOROLLOVER='D' , 1900, 10000 )
        endif
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(iif( .w_TIPOROLLOVER='D' , 'Raggiunta la dimensione in Megabyte specificata  la traccia viene rinominata e ne viene creata una nuova' ,  'Raggiunto il numero di record indicato la traccia viene rinominata e ne viene creata una nuova' ))
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(IIF( INLIST( .w_ATTIVATO, 1, 3 ), iif( .w_TIPOROLLOVER='D' , 'Dimensione (MB):', 'Numero record per log:' ),''))
        if .o_ATTIVATO<>.w_ATTIVATO
          .Calculate_MDPKYCXTRM()
        endif
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(iif( .w_TIPOROLLOVER='D' , 'Raggiunta la dimensione in Megabyte specificata  la traccia viene rinominata e ne viene creata una nuova' ,  'Raggiunto il numero di record indicato la traccia viene rinominata e ne viene creata una nuova' ))
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(IIF( INLIST( .w_ATTIVATO, 1, 3 ), iif( .w_TIPOROLLOVER='D' , 'Dimensione (MB):', 'Numero record per log:' ),''))
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
    endwith
  return

  proc Calculate_WEJHQQFRIN()
    with this
          * --- Configura il log
          .w_TRACCIATURAGESTIONI = IIF( .w_CONFIGURAZIONI $ "T-E" , 1 , 0 )
          .w_TRACCIATURAROUTINE = IIF( .w_CONFIGURAZIONI $ "T-E" , 1 , 0 )
          .w_TRACCIATURASQLLETTURA = IIF( .w_CONFIGURAZIONI $ "T-S-Q" , IIF( .w_ATTIVATO=2, 2,1) , 0 )
          .w_TRACCIATURASQLSCRITTURA = IIF( .w_CONFIGURAZIONI $ "T-Q" , 1 , 0 )
          .w_TRACCIATURASQLINSERIMENTO = IIF( .w_CONFIGURAZIONI $ "T-Q" , 1 , 0 )
          .w_TRACCIATURASQLCANCELLAZIONE = IIF( .w_CONFIGURAZIONI $ "T-Q" , 1 , 0 )
          .w_TRACCIATURASQLMODIFICADB = IIF( .w_CONFIGURAZIONI $ "T" , 1 , 0 )
          .w_TRACCIATURASQLTEMPORANEO = IIF( .w_CONFIGURAZIONI $ "T-Q" , 1 , 0 )
          .w_TRACCIATURASQLALTROSUDB = IIF( .w_CONFIGURAZIONI $ "T" , 1 , 0 )
          .w_TRACCIATURATRANSAZIONI = IIF( .w_CONFIGURAZIONI $ "T" , 1 , 0 )
          .w_TRACCIATURAQUERYASINCRONA = IIF( .w_CONFIGURAZIONI $ "T-S-Q" ,  2 , 0 )
          .w_TRACCIATURADEADLOCK = IIF( .w_CONFIGURAZIONI $ "T-D" , 1 , 0 )
          .w_TRACCIATURARICONNESSIONE = IIF( .w_CONFIGURAZIONI $ "T-D" , 1 , 0 )
          .w_TRACCIATURAVQR = IIF( .w_CONFIGURAZIONI $ "T" , 1 , 0 )
          .w_TRACCIATURATASTIFUN = IIF( .w_CONFIGURAZIONI $ "T-E" , 1 , 0 )
          .w_TRACCIATURAEVENTI = IIF( .w_CONFIGURAZIONI $ "T-E" , 1 , 0 )
    endwith
  endproc
  proc Calculate_MDPKYCXTRM()
    with this
          * --- Aggiorna SQL lettura e Query asincrona
          .w_TRACCIATURASQLLETTURA = IIF (.w_ATTIVATO=2 ,  IIF (.w_TRACCIATURASQLLETTURA>0,2,0)  , IIF( INLIST( .w_ATTIVATO, 1, 3, 5, 7 ),1 , .w_TRACCIATURASQLLETTURA ) )
          .w_TRACCIATURAQUERYASINCRONA = IIF (.w_ATTIVATO=2 ,  IIF (.w_TRACCIATURAQUERYASINCRONA>0,2,0)  , .w_TRACCIATURAQUERYASINCRONA)
          .w_POSIZIONELOG = IIF (.w_ATTIVATO=0 OR .w_ATTIVATO=2,'',.w_POSIZIONELOG)
          .w_NOMELOG = IIF (.w_ATTIVATO=0 OR .w_ATTIVATO=2,'',.w_NOMELOG)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.enabled = this.oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.mCond()
    this.oPgFrm.Page1.oPag.oNOMELOG_1_7.enabled = this.oPgFrm.Page1.oPag.oNOMELOG_1_7.mCond()
    this.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.enabled = this.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.mCond()
    this.oPgFrm.Page1.oPag.oROLLOVER_1_10.enabled = this.oPgFrm.Page1.oPag.oROLLOVER_1_10.mCond()
    this.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.enabled = this.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCONFIGURAZIONI_1_15.enabled = this.oPgFrm.Page1.oPag.oCONFIGURAZIONI_1_15.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLSCRITTURA_1_20.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURASQLSCRITTURA_1_20.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLINSERIMENTO_1_22.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURASQLINSERIMENTO_1_22.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLCANCELLAZIONE_1_24.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURASQLCANCELLAZIONE_1_24.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURAVQR_1_25.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURAVQR_1_25.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURAGESTIONI_1_27.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURAGESTIONI_1_27.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURAROUTINE_1_28.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURAROUTINE_1_28.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURAFUNZIONI_1_29.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURAFUNZIONI_1_29.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURATASTIFUN_1_31.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURATASTIFUN_1_31.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURAEVENTI_1_32.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURAEVENTI_1_32.mCond()
    this.oPgFrm.Page1.oPag.oRIGHE_1_33.enabled = this.oPgFrm.Page1.oPag.oRIGHE_1_33.mCond()
    this.oPgFrm.Page1.oPag.oMILLISECONDI_1_35.enabled = this.oPgFrm.Page1.oPag.oMILLISECONDI_1_35.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLMODIFICADB_1_36.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURASQLMODIFICADB_1_36.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLTEMPORANEO_1_37.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURASQLTEMPORANEO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLALTROSUDB_1_38.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURASQLALTROSUDB_1_38.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURATRANSAZIONI_1_40.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURATRANSAZIONI_1_40.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURAQUERYASINCRONA_1_41.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURAQUERYASINCRONA_1_41.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURADEADLOCK_1_42.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURADEADLOCK_1_42.mCond()
    this.oPgFrm.Page1.oPag.oTRACCIATURARICONNESSIONE_1_43.enabled = this.oPgFrm.Page1.oPag.oTRACCIATURARICONNESSIONE_1_43.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oNOMELOG_1_7.visible=!this.oPgFrm.Page1.oPag.oNOMELOG_1_7.mHide()
    this.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.visible=!this.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.mHide()
    this.oPgFrm.Page1.oPag.oROLLOVER_1_10.visible=!this.oPgFrm.Page1.oPag.oROLLOVER_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.visible=!this.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.mHide()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.visible=!this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.mHide()
    this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.visible=!this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
        if lower(cEvent)==lower("w_CONFIGURAZIONI Changed")
          .Calculate_WEJHQQFRIN()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATTIVATO_1_2.RadioValue()==this.w_ATTIVATO)
      this.oPgFrm.Page1.oPag.oATTIVATO_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.value==this.w_POSIZIONELOG)
      this.oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.value=this.w_POSIZIONELOG
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMELOG_1_7.value==this.w_NOMELOG)
      this.oPgFrm.Page1.oPag.oNOMELOG_1_7.value=this.w_NOMELOG
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.RadioValue()==this.w_TIPOROLLOVER)
      this.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oROLLOVER_1_10.value==this.w_ROLLOVER)
      this.oPgFrm.Page1.oPag.oROLLOVER_1_10.value=this.w_ROLLOVER
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.value==this.w_NUMERODIFILEDILOG)
      this.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.value=this.w_NUMERODIFILEDILOG
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFIGURAZIONI_1_15.RadioValue()==this.w_CONFIGURAZIONI)
      this.oPgFrm.Page1.oPag.oCONFIGURAZIONI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.RadioValue()==this.w_TRACCIATURASQLLETTURA)
      this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.RadioValue()==this.w_TRACCIATURASQLLETTURA)
      this.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURASQLSCRITTURA_1_20.RadioValue()==this.w_TRACCIATURASQLSCRITTURA)
      this.oPgFrm.Page1.oPag.oTRACCIATURASQLSCRITTURA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURASQLINSERIMENTO_1_22.RadioValue()==this.w_TRACCIATURASQLINSERIMENTO)
      this.oPgFrm.Page1.oPag.oTRACCIATURASQLINSERIMENTO_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURASQLCANCELLAZIONE_1_24.RadioValue()==this.w_TRACCIATURASQLCANCELLAZIONE)
      this.oPgFrm.Page1.oPag.oTRACCIATURASQLCANCELLAZIONE_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURAVQR_1_25.RadioValue()==this.w_TRACCIATURAVQR)
      this.oPgFrm.Page1.oPag.oTRACCIATURAVQR_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURAGESTIONI_1_27.RadioValue()==this.w_TRACCIATURAGESTIONI)
      this.oPgFrm.Page1.oPag.oTRACCIATURAGESTIONI_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURAROUTINE_1_28.RadioValue()==this.w_TRACCIATURAROUTINE)
      this.oPgFrm.Page1.oPag.oTRACCIATURAROUTINE_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURAFUNZIONI_1_29.RadioValue()==this.w_TRACCIATURAFUNZIONI)
      this.oPgFrm.Page1.oPag.oTRACCIATURAFUNZIONI_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURATASTIFUN_1_31.RadioValue()==this.w_TRACCIATURATASTIFUN)
      this.oPgFrm.Page1.oPag.oTRACCIATURATASTIFUN_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURAEVENTI_1_32.RadioValue()==this.w_TRACCIATURAEVENTI)
      this.oPgFrm.Page1.oPag.oTRACCIATURAEVENTI_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRIGHE_1_33.value==this.w_RIGHE)
      this.oPgFrm.Page1.oPag.oRIGHE_1_33.value=this.w_RIGHE
    endif
    if not(this.oPgFrm.Page1.oPag.oMILLISECONDI_1_35.value==this.w_MILLISECONDI)
      this.oPgFrm.Page1.oPag.oMILLISECONDI_1_35.value=this.w_MILLISECONDI
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURASQLMODIFICADB_1_36.RadioValue()==this.w_TRACCIATURASQLMODIFICADB)
      this.oPgFrm.Page1.oPag.oTRACCIATURASQLMODIFICADB_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURASQLTEMPORANEO_1_37.RadioValue()==this.w_TRACCIATURASQLTEMPORANEO)
      this.oPgFrm.Page1.oPag.oTRACCIATURASQLTEMPORANEO_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURASQLALTROSUDB_1_38.RadioValue()==this.w_TRACCIATURASQLALTROSUDB)
      this.oPgFrm.Page1.oPag.oTRACCIATURASQLALTROSUDB_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURATRANSAZIONI_1_40.RadioValue()==this.w_TRACCIATURATRANSAZIONI)
      this.oPgFrm.Page1.oPag.oTRACCIATURATRANSAZIONI_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURAQUERYASINCRONA_1_41.RadioValue()==this.w_TRACCIATURAQUERYASINCRONA)
      this.oPgFrm.Page1.oPag.oTRACCIATURAQUERYASINCRONA_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURADEADLOCK_1_42.RadioValue()==this.w_TRACCIATURADEADLOCK)
      this.oPgFrm.Page1.oPag.oTRACCIATURADEADLOCK_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRACCIATURARICONNESSIONE_1_43.RadioValue()==this.w_TRACCIATURARICONNESSIONE)
      this.oPgFrm.Page1.oPag.oTRACCIATURARICONNESSIONE_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEBUG_1_44.RadioValue()==this.w_DEBUG)
      this.oPgFrm.Page1.oPag.oDEBUG_1_44.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY( .w_POSIZIONELOG ) OR ( NOT EMPTY( JUSTDRIVE( .w_POSIZIONELOG ) ) AND NOT " " $ ALLTRIM( .w_POSIZIONELOG ) ))  and (INLIST( .w_ATTIVATO, 1, 3, 5, 7))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Controllare se il percorso selezionato fa uso di una unit� disco o contiene spazi")
          case   not(NOT " " $ ALLTRIM( .w_NOMELOG ))  and not(INLIST( .w_ATTIVATO, 5, 7 ))  and (INLIST( .w_ATTIVATO, 1, 3, 5, 7))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMELOG_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un un nome di file che non contenga spazi")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ATTIVATO = this.w_ATTIVATO
    this.o_TIPOROLLOVER = this.w_TIPOROLLOVER
    return

enddefine

* --- Define pages as container
define class tcp_setuptracePag1 as StdContainer
  Width  = 728
  height = 521
  stdWidth  = 728
  stdheight = 521
  resizeXpos=355
  resizeYpos=168
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oATTIVATO_1_2 as StdCombo with uid="JWOMUWHYQF",value=1,rtseq=1,rtrep=.f.,left=180,top=8,width=165,height=22;
    , ToolTipText = "Attiva o disattiva il log";
    , HelpContextID = 156277211;
    , cFormVar="w_ATTIVATO",RowSource=""+"No,"+"Su DBF,"+"Su database", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATTIVATO_1_2.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,5,;
    0))))
  endfunc
  func oATTIVATO_1_2.GetRadio()
    this.Parent.oContained.w_ATTIVATO = this.RadioValue()
    return .t.
  endfunc

  func oATTIVATO_1_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ATTIVATO==0,1,;
      iif(this.Parent.oContained.w_ATTIVATO==1,2,;
      iif(this.Parent.oContained.w_ATTIVATO==5,3,;
      0)))
  endfunc


  add object oObj_1_3 as cp_runprogram with uid="RAHAXRFAOG",left=745, top=56, width=192,height=50,;
    caption='cp_setuptrace_b I',;
   bGlobalFont=.t.,;
    prg="cp_setuptrace_b('I')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 51562729

  add object oPOSIZIONELOG_1_5 as StdField with uid="AZIHLBPRBL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_POSIZIONELOG", cQueryName = "POSIZIONELOG",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    sErrorMsg = "Controllare se il percorso selezionato fa uso di una unit� disco o contiene spazi",;
    ToolTipText = "Cartella in cui vengono posti i file di log",;
    HelpContextID = 46390631,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=180, Top=32, InputMask=replicate('X',250), bHasZoom = .t. 

  func oPOSIZIONELOG_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 1, 3, 5, 7))
    endwith
   endif
  endfunc

  func oPOSIZIONELOG_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY( .w_POSIZIONELOG ) OR ( NOT EMPTY( JUSTDRIVE( .w_POSIZIONELOG ) ) AND NOT " " $ ALLTRIM( .w_POSIZIONELOG ) ))
    endwith
    return bRes
  endfunc

  proc oPOSIZIONELOG_1_5.mZoom
    cp_setuptrace_b(this.parent.oContained,"G")
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOMELOG_1_7 as StdField with uid="UTYWSTQQBT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NOMELOG", cQueryName = "NOMELOG",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un un nome di file che non contenga spazi",;
    ToolTipText = "Nome assegnato al file di log",;
    HelpContextID = 246124652,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=180, Top=57, InputMask=replicate('X',250)

  func oNOMELOG_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 1, 3, 5, 7))
    endwith
   endif
  endfunc

  func oNOMELOG_1_7.mHide()
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 5, 7 ))
    endwith
  endfunc

  func oNOMELOG_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT " " $ ALLTRIM( .w_NOMELOG ))
    endwith
    return bRes
  endfunc


  add object oTIPOROLLOVER_1_9 as StdCombo with uid="YEFWGKZIKD",rtseq=5,rtrep=.f.,left=180,top=81,width=165,height=21;
    , ToolTipText = "Per default il rollover viene attivato al raggiungimento di 2Gb";
    , HelpContextID = 186219420;
    , cFormVar="w_TIPOROLLOVER",RowSource=""+"Dimensione della traccia,"+"Numero di record", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOROLLOVER_1_9.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPOROLLOVER_1_9.GetRadio()
    this.Parent.oContained.w_TIPOROLLOVER = this.RadioValue()
    return .t.
  endfunc

  func oTIPOROLLOVER_1_9.SetRadio()
    this.Parent.oContained.w_TIPOROLLOVER=trim(this.Parent.oContained.w_TIPOROLLOVER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOROLLOVER=='D',1,;
      iif(this.Parent.oContained.w_TIPOROLLOVER=='R',2,;
      0))
  endfunc

  func oTIPOROLLOVER_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 1, 3, 5, 7))
    endwith
   endif
  endfunc

  func oTIPOROLLOVER_1_9.mHide()
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 5, 7 ))
    endwith
  endfunc

  add object oROLLOVER_1_10 as StdField with uid="WMRRZSXSDW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ROLLOVER", cQueryName = "ROLLOVER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Raggiunto il numero di record indicato o la dimensione, la traccia viene rinominata e ne viene creata una nuova",;
    HelpContextID = 93134077,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=515, Top=81, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

  func oROLLOVER_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 1, 3, 5, 7))
    endwith
   endif
  endfunc

  func oROLLOVER_1_10.mHide()
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 5, 7 ))
    endwith
  endfunc

  add object oNUMERODIFILEDILOG_1_12 as StdField with uid="JXANHGTAJG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMERODIFILEDILOG", cQueryName = "NUMERODIFILEDILOG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cancella le tracce pi� vecchie mantenendo le pi� recenti",;
    HelpContextID = 136465968,;
   bGlobalFont=.t.,;
    Height=20, Width=165, Left=180, Top=108, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

  func oNUMERODIFILEDILOG_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 1, 3, 5, 7))
    endwith
   endif
  endfunc

  func oNUMERODIFILEDILOG_1_12.mHide()
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 5, 7 ))
    endwith
  endfunc


  add object oCONFIGURAZIONI_1_15 as StdCombo with uid="GJWYOEGPUA",rtseq=8,rtrep=.f.,left=180,top=147,width=165,height=21;
    , ToolTipText = "Imposta i filtri";
    , HelpContextID = 58273826;
    , cFormVar="w_CONFIGURAZIONI",RowSource=""+"Seleziona tutto,"+"Esecuzione programmi,"+"Solo select SQL,"+"Solo istruzioni SQL,"+"Deadlock o riconnessioni,"+"Deseleziona tutto,"+"Selezione configurazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCONFIGURAZIONI_1_15.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'E',;
    iif(this.value =3,'S',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'D',;
    iif(this.value =6,'N',;
    iif(this.value =7,'0',;
    space(1)))))))))
  endfunc
  func oCONFIGURAZIONI_1_15.GetRadio()
    this.Parent.oContained.w_CONFIGURAZIONI = this.RadioValue()
    return .t.
  endfunc

  func oCONFIGURAZIONI_1_15.SetRadio()
    this.Parent.oContained.w_CONFIGURAZIONI=trim(this.Parent.oContained.w_CONFIGURAZIONI)
    this.value = ;
      iif(this.Parent.oContained.w_CONFIGURAZIONI=='T',1,;
      iif(this.Parent.oContained.w_CONFIGURAZIONI=='E',2,;
      iif(this.Parent.oContained.w_CONFIGURAZIONI=='S',3,;
      iif(this.Parent.oContained.w_CONFIGURAZIONI=='Q',4,;
      iif(this.Parent.oContained.w_CONFIGURAZIONI=='D',5,;
      iif(this.Parent.oContained.w_CONFIGURAZIONI=='N',6,;
      iif(this.Parent.oContained.w_CONFIGURAZIONI=='0',7,;
      0)))))))
  endfunc

  func oCONFIGURAZIONI_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURASQLLETTURA_1_16 as StdCombo with uid="MFWYCHOFQW",value=1,rtseq=9,rtrep=.f.,left=180,top=177,width=165,height=21;
    , ToolTipText = "Traccia le istruzioni select";
    , HelpContextID = 170877774;
    , cFormVar="w_TRACCIATURASQLLETTURA",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURASQLLETTURA_1_16.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oTRACCIATURASQLLETTURA_1_16.GetRadio()
    this.Parent.oContained.w_TRACCIATURASQLLETTURA = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURASQLLETTURA_1_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURASQLLETTURA==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURASQLLETTURA==2,2,;
      0))
  endfunc

  func oTRACCIATURASQLLETTURA_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc

  func oTRACCIATURASQLLETTURA_1_16.mHide()
    with this.Parent.oContained
      return (.w_ATTIVATO<>2)
    endwith
  endfunc


  add object oTRACCIATURASQLLETTURA_1_18 as StdCombo with uid="XVTJZYNMAO",value=1,rtseq=10,rtrep=.f.,left=180,top=177,width=165,height=21;
    , ToolTipText = "Istruzioni select";
    , HelpContextID = 170877774;
    , cFormVar="w_TRACCIATURASQLLETTURA",RowSource=""+"No,"+"S� con DBF del risultato,"+"S� senza DBF del risultato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURASQLLETTURA_1_18.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    0))))
  endfunc
  func oTRACCIATURASQLLETTURA_1_18.GetRadio()
    this.Parent.oContained.w_TRACCIATURASQLLETTURA = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURASQLLETTURA_1_18.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURASQLLETTURA==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURASQLLETTURA==1,2,;
      iif(this.Parent.oContained.w_TRACCIATURASQLLETTURA==2,3,;
      0)))
  endfunc

  func oTRACCIATURASQLLETTURA_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc

  func oTRACCIATURASQLLETTURA_1_18.mHide()
    with this.Parent.oContained
      return (.w_ATTIVATO = 2)
    endwith
  endfunc


  add object oTRACCIATURASQLSCRITTURA_1_20 as StdCombo with uid="QNSXGAXUZP",value=1,rtseq=11,rtrep=.f.,left=180,top=205,width=165,height=21;
    , ToolTipText = "Traccia le istruzioni update";
    , HelpContextID = 140285805;
    , cFormVar="w_TRACCIATURASQLSCRITTURA",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURASQLSCRITTURA_1_20.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURASQLSCRITTURA_1_20.GetRadio()
    this.Parent.oContained.w_TRACCIATURASQLSCRITTURA = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURASQLSCRITTURA_1_20.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURASQLSCRITTURA==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURASQLSCRITTURA==1,2,;
      0))
  endfunc

  func oTRACCIATURASQLSCRITTURA_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURASQLINSERIMENTO_1_22 as StdCombo with uid="SEEIRHXYQR",value=1,rtseq=12,rtrep=.f.,left=180,top=233,width=165,height=21;
    , ToolTipText = "Traccia le istruzioni insert";
    , HelpContextID = 240041659;
    , cFormVar="w_TRACCIATURASQLINSERIMENTO",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURASQLINSERIMENTO_1_22.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURASQLINSERIMENTO_1_22.GetRadio()
    this.Parent.oContained.w_TRACCIATURASQLINSERIMENTO = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURASQLINSERIMENTO_1_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURASQLINSERIMENTO==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURASQLINSERIMENTO==1,2,;
      0))
  endfunc

  func oTRACCIATURASQLINSERIMENTO_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURASQLCANCELLAZIONE_1_24 as StdCombo with uid="NHRNQINKTD",value=1,rtseq=13,rtrep=.f.,left=180,top=261,width=165,height=21;
    , ToolTipText = "Traccia le  istruzioni delete";
    , HelpContextID = 131366435;
    , cFormVar="w_TRACCIATURASQLCANCELLAZIONE",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURASQLCANCELLAZIONE_1_24.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURASQLCANCELLAZIONE_1_24.GetRadio()
    this.Parent.oContained.w_TRACCIATURASQLCANCELLAZIONE = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURASQLCANCELLAZIONE_1_24.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURASQLCANCELLAZIONE==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURASQLCANCELLAZIONE==1,2,;
      0))
  endfunc

  func oTRACCIATURASQLCANCELLAZIONE_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURAVQR_1_25 as StdCombo with uid="HARCTXOMJN",value=1,rtseq=14,rtrep=.f.,left=180,top=289,width=165,height=21;
    , ToolTipText = "Traccia le chiamate di visual query";
    , HelpContextID = 26443017;
    , cFormVar="w_TRACCIATURAVQR",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURAVQR_1_25.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURAVQR_1_25.GetRadio()
    this.Parent.oContained.w_TRACCIATURAVQR = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURAVQR_1_25.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURAVQR==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURAVQR==1,2,;
      0))
  endfunc

  func oTRACCIATURAVQR_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURAGESTIONI_1_27 as StdCombo with uid="GPZIRJZKDJ",value=1,rtseq=15,rtrep=.f.,left=180,top=317,width=165,height=21;
    , ToolTipText = "Traccia apertura e chiusura di form a video";
    , HelpContextID = 176717726;
    , cFormVar="w_TRACCIATURAGESTIONI",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURAGESTIONI_1_27.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURAGESTIONI_1_27.GetRadio()
    this.Parent.oContained.w_TRACCIATURAGESTIONI = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURAGESTIONI_1_27.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURAGESTIONI==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURAGESTIONI==1,2,;
      0))
  endfunc

  func oTRACCIATURAGESTIONI_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURAROUTINE_1_28 as StdCombo with uid="GDLUQCNJQY",value=1,rtseq=16,rtrep=.f.,left=180,top=345,width=165,height=21;
    , ToolTipText = "Traccia avvio e chiusura delle routine";
    , HelpContextID = 74951369;
    , cFormVar="w_TRACCIATURAROUTINE",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURAROUTINE_1_28.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURAROUTINE_1_28.GetRadio()
    this.Parent.oContained.w_TRACCIATURAROUTINE = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURAROUTINE_1_28.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURAROUTINE==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURAROUTINE==1,2,;
      0))
  endfunc

  func oTRACCIATURAROUTINE_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURAFUNZIONI_1_29 as StdCombo with uid="XWDOEMXFOF",value=1,rtseq=17,rtrep=.f.,left=180,top=373,width=165,height=21;
    , ToolTipText = "Traccia le chiamate di funzione";
    , HelpContextID = 176340910;
    , cFormVar="w_TRACCIATURAFUNZIONI",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURAFUNZIONI_1_29.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURAFUNZIONI_1_29.GetRadio()
    this.Parent.oContained.w_TRACCIATURAFUNZIONI = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURAFUNZIONI_1_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURAFUNZIONI==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURAFUNZIONI==1,2,;
      0))
  endfunc

  func oTRACCIATURAFUNZIONI_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURATASTIFUN_1_31 as StdCombo with uid="TERNEMWZCZ",value=1,rtseq=18,rtrep=.f.,left=180,top=400,width=165,height=21;
    , ToolTipText = "Traccia tasti funzioni F3,F4,F5,F6,F10,F12 ed ESC";
    , HelpContextID = 209157431;
    , cFormVar="w_TRACCIATURATASTIFUN",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURATASTIFUN_1_31.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURATASTIFUN_1_31.GetRadio()
    this.Parent.oContained.w_TRACCIATURATASTIFUN = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURATASTIFUN_1_31.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURATASTIFUN==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURATASTIFUN==1,2,;
      0))
  endfunc

  func oTRACCIATURATASTIFUN_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURAEVENTI_1_32 as StdCombo with uid="YRPSPERZEO",value=1,rtseq=19,rtrep=.f.,left=180,top=428,width=165,height=21;
    , ToolTipText = "Traccia la notifica degli eventi";
    , HelpContextID = 266292999;
    , cFormVar="w_TRACCIATURAEVENTI",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURAEVENTI_1_32.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURAEVENTI_1_32.GetRadio()
    this.Parent.oContained.w_TRACCIATURAEVENTI = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURAEVENTI_1_32.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURAEVENTI==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURAEVENTI==1,2,;
      0))
  endfunc

  func oTRACCIATURAEVENTI_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc

  add object oRIGHE_1_33 as StdField with uid="ZYQSVUKXYD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_RIGHE", cQueryName = "RIGHE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo di righe interessate per cui l'istruzione SQLviene tracciata",;
    HelpContextID = 84441278,;
   bGlobalFont=.t.,;
    Height=21, Width=165, Left=180, Top=458, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

  func oRIGHE_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO<>0)
    endwith
   endif
  endfunc

  add object oMILLISECONDI_1_35 as StdField with uid="FAHYKCYVRA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MILLISECONDI", cQueryName = "MILLISECONDI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata minima in millisecondi per cui l'istruzione SQLviene tracciata",;
    HelpContextID = 209538601,;
   bGlobalFont=.t.,;
    Height=21, Width=165, Left=180, Top=485, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

  func oMILLISECONDI_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO<>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURASQLMODIFICADB_1_36 as StdCombo with uid="OZVKJLRYBM",value=1,rtseq=22,rtrep=.f.,left=553,top=177,width=165,height=21;
    , ToolTipText = "Ricostruzione database";
    , HelpContextID = 255358127;
    , cFormVar="w_TRACCIATURASQLMODIFICADB",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURASQLMODIFICADB_1_36.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURASQLMODIFICADB_1_36.GetRadio()
    this.Parent.oContained.w_TRACCIATURASQLMODIFICADB = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURASQLMODIFICADB_1_36.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURASQLMODIFICADB==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURASQLMODIFICADB==1,2,;
      0))
  endfunc

  func oTRACCIATURASQLMODIFICADB_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURASQLTEMPORANEO_1_37 as StdCombo with uid="KOWHCWYDNN",value=1,rtseq=23,rtrep=.f.,left=553,top=205,width=165,height=21;
    , ToolTipText = "Creazioni temporanei su server";
    , HelpContextID = 126794936;
    , cFormVar="w_TRACCIATURASQLTEMPORANEO",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURASQLTEMPORANEO_1_37.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURASQLTEMPORANEO_1_37.GetRadio()
    this.Parent.oContained.w_TRACCIATURASQLTEMPORANEO = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURASQLTEMPORANEO_1_37.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURASQLTEMPORANEO==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURASQLTEMPORANEO==1,2,;
      0))
  endfunc

  func oTRACCIATURASQLTEMPORANEO_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURASQLALTROSUDB_1_38 as StdCombo with uid="LBIHXUKAPM",value=1,rtseq=24,rtrep=.f.,left=553,top=233,width=165,height=21;
    , ToolTipText = "Altre istruzioni su database";
    , HelpContextID = 86329512;
    , cFormVar="w_TRACCIATURASQLALTROSUDB",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURASQLALTROSUDB_1_38.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURASQLALTROSUDB_1_38.GetRadio()
    this.Parent.oContained.w_TRACCIATURASQLALTROSUDB = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURASQLALTROSUDB_1_38.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURASQLALTROSUDB==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURASQLALTROSUDB==1,2,;
      0))
  endfunc

  func oTRACCIATURASQLALTROSUDB_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURATRANSAZIONI_1_40 as StdCombo with uid="KLQBURDHVZ",value=1,rtseq=25,rtrep=.f.,left=553,top=261,width=165,height=21;
    , ToolTipText = "Begin transaction - commit - rollback";
    , HelpContextID = 135614754;
    , cFormVar="w_TRACCIATURATRANSAZIONI",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURATRANSAZIONI_1_40.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURATRANSAZIONI_1_40.GetRadio()
    this.Parent.oContained.w_TRACCIATURATRANSAZIONI = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURATRANSAZIONI_1_40.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURATRANSAZIONI==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURATRANSAZIONI==1,2,;
      0))
  endfunc

  func oTRACCIATURATRANSAZIONI_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURAQUERYASINCRONA_1_41 as StdCombo with uid="WNNERTPZLR",value=1,rtseq=26,rtrep=.f.,left=553,top=289,width=165,height=21;
    , ToolTipText = "Traccia query asincrone";
    , HelpContextID = 245962978;
    , cFormVar="w_TRACCIATURAQUERYASINCRONA",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURAQUERYASINCRONA_1_41.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oTRACCIATURAQUERYASINCRONA_1_41.GetRadio()
    this.Parent.oContained.w_TRACCIATURAQUERYASINCRONA = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURAQUERYASINCRONA_1_41.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURAQUERYASINCRONA==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURAQUERYASINCRONA==2,2,;
      0))
  endfunc

  func oTRACCIATURAQUERYASINCRONA_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURADEADLOCK_1_42 as StdCombo with uid="TPTSJDDDWM",value=1,rtseq=27,rtrep=.f.,left=553,top=371,width=165,height=21;
    , ToolTipText = "Traccia i deadlock";
    , HelpContextID = 174694348;
    , cFormVar="w_TRACCIATURADEADLOCK",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURADEADLOCK_1_42.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURADEADLOCK_1_42.GetRadio()
    this.Parent.oContained.w_TRACCIATURADEADLOCK = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURADEADLOCK_1_42.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURADEADLOCK==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURADEADLOCK==1,2,;
      0))
  endfunc

  func oTRACCIATURADEADLOCK_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oTRACCIATURARICONNESSIONE_1_43 as StdCombo with uid="APMEYUEVOV",value=1,rtseq=28,rtrep=.f.,left=553,top=399,width=165,height=21;
    , ToolTipText = "Traccia le riconnessioni in seguito a caduta di connessione";
    , HelpContextID = 157598540;
    , cFormVar="w_TRACCIATURARICONNESSIONE",RowSource=""+"No,"+"S�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRACCIATURARICONNESSIONE_1_43.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oTRACCIATURARICONNESSIONE_1_43.GetRadio()
    this.Parent.oContained.w_TRACCIATURARICONNESSIONE = this.RadioValue()
    return .t.
  endfunc

  func oTRACCIATURARICONNESSIONE_1_43.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TRACCIATURARICONNESSIONE==0,1,;
      iif(this.Parent.oContained.w_TRACCIATURARICONNESSIONE==1,2,;
      0))
  endfunc

  func oTRACCIATURARICONNESSIONE_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIVATO>0)
    endwith
   endif
  endfunc


  add object oDEBUG_1_44 as StdCombo with uid="YTDTIWLLGZ",value=1,rtseq=29,rtrep=.f.,left=553,top=429,width=165,height=21;
    , ToolTipText = "Attiva la variabile globale g_debugmode";
    , HelpContextID = 117996425;
    , cFormVar="w_DEBUG",RowSource=""+"Disattiva,"+"No wait,"+"Wait", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDEBUG_1_44.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    0))))
  endfunc
  func oDEBUG_1_44.GetRadio()
    this.Parent.oContained.w_DEBUG = this.RadioValue()
    return .t.
  endfunc

  func oDEBUG_1_44.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DEBUG==0,1,;
      iif(this.Parent.oContained.w_DEBUG==1,2,;
      iif(this.Parent.oContained.w_DEBUG==2,3,;
      0)))
  endfunc


  add object oBtn_1_46 as StdButton with uid="TPPZXKGHKR",left=507, top=469, width=48,height=45,;
    CpPicture="NewOffe.bmp", caption="", nPag=1;
    , ToolTipText = "Ripulisce l'output su schermo";
    , HelpContextID = 167235766;
    , caption='C\<LS';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      with this.Parent.oContained
        _screen.cls()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_48 as StdButton with uid="MEVYVBNJJZ",left=562, top=469, width=48,height=45,;
    CpPicture="CONTRATT.BMP", caption="", nPag=1;
    , ToolTipText = "Scrive le impostazioni nel file CNF";
    , HelpContextID = 167235779;
    , caption='\<Cnf';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        cp_setuptrace_b(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_50 as StdButton with uid="KCCEPGRZSR",left=617, top=469, width=48,height=45,;
    CpPicture="APPLICA.BMP", caption="", nPag=1;
    , ToolTipText = "Applica le impostazioni alla sessione di lavoro corrente";
    , HelpContextID = 193999065;
    , caption='\<Applica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_50.Click()
      with this.Parent.oContained
        cp_setuptrace_b(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_52 as StdButton with uid="HKYUBHHNJZ",left=671, top=469, width=48,height=45,;
    CpPicture="ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Chiude la maschera";
    , HelpContextID = 133679638;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_57 as cp_setobjprop with uid="SKPUZOAYUU",left=744, top=114, width=124,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_ROLLOVER",;
    cEvent = "w_TIPOROLLOVER changed",;
    nPag=1;
    , HelpContextID = 233842511


  add object oObj_1_58 as cp_calclbl with uid="JWKRNWDWTT",left=375, top=85, width=136,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Record per log",;
    cEvent = "w_TIPOROLLOVER changed",;
    nPag=1;
    , HelpContextID = 233842511


  add object oObj_1_64 as cp_runprogram with uid="FOSXEOKNMH",left=744, top=148, width=192,height=50,;
    caption='cp_setuptrace_b D',;
   bGlobalFont=.t.,;
    prg="cp_setuptrace_b('D')",;
    cEvent = "w_POSIZIONELOG Changed",;
    nPag=1;
    , HelpContextID = 135448809

  add object oStr_1_1 as StdString with uid="ASYEXESRCO",Visible=.t., Left=32, Top=9,;
    Alignment=1, Width=143, Height=18,;
    Caption="Attivazione log:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="EWYERTEVMX",Visible=.t., Left=34, Top=33,;
    Alignment=1, Width=141, Height=18,;
    Caption="Posizione log:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VTGXORRIZR",Visible=.t., Left=47, Top=57,;
    Alignment=1, Width=128, Height=18,;
    Caption="Nome log:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 5, 7 ))
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="DJSWDSEWWZ",Visible=.t., Left=43, Top=112,;
    Alignment=1, Width=132, Height=18,;
    Caption="Log da mantenere:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 5, 7 ))
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="EZWUNVAJOO",Visible=.t., Left=25, Top=320,;
    Alignment=1, Width=150, Height=18,;
    Caption="Gestioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="TIHXOUMEKY",Visible=.t., Left=25, Top=348,;
    Alignment=1, Width=150, Height=18,;
    Caption="Routine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ZUVUGXVQLW",Visible=.t., Left=392, Top=289,;
    Alignment=1, Width=155, Height=18,;
    Caption="Query asincrona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="CTTANLJVIY",Visible=.t., Left=25, Top=177,;
    Alignment=1, Width=150, Height=18,;
    Caption="SQL lettura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="YKUFMJSPWU",Visible=.t., Left=25, Top=205,;
    Alignment=1, Width=150, Height=18,;
    Caption="SQL aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VYDSTWEYQX",Visible=.t., Left=15, Top=233,;
    Alignment=1, Width=160, Height=18,;
    Caption="SQL inserimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="XNUKEVYSSZ",Visible=.t., Left=6, Top=261,;
    Alignment=1, Width=169, Height=18,;
    Caption="SQL cancellazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="RJBTKIHVSD",Visible=.t., Left=357, Top=205,;
    Alignment=1, Width=190, Height=18,;
    Caption="Creazione temporaneo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="CVTCQVIYCZ",Visible=.t., Left=397, Top=177,;
    Alignment=1, Width=150, Height=18,;
    Caption="Modifica DB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="BJUCAPEUKT",Visible=.t., Left=367, Top=233,;
    Alignment=1, Width=180, Height=18,;
    Caption="Altre istruzioni su DB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="GCUVXGWWJM",Visible=.t., Left=397, Top=261,;
    Alignment=1, Width=150, Height=18,;
    Caption="Transazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="PXAQHCUQKM",Visible=.t., Left=43, Top=147,;
    Alignment=1, Width=132, Height=19,;
    Caption="Configurazioni:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="XEVUUSCNYD",Visible=.t., Left=367, Top=371,;
    Alignment=1, Width=180, Height=18,;
    Caption="Deadlock:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="SZMMKKYOJR",Visible=.t., Left=397, Top=399,;
    Alignment=1, Width=150, Height=18,;
    Caption="Riconnessione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="LSWBMIKZMI",Visible=.t., Left=90, Top=85,;
    Alignment=1, Width=85, Height=18,;
    Caption="Rollover su:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (INLIST( .w_ATTIVATO, 5, 7 ))
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="PRJBNTFPCN",Visible=.t., Left=17, Top=627,;
    Alignment=0, Width=457, Height=18,;
    Caption="Attenzione le combo relative a SQL lettura e Asincrona sono sovrapposte"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="WGBEEEDINI",Visible=.t., Left=39, Top=459,;
    Alignment=1, Width=136, Height=18,;
    Caption="Nro minimo righe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="WCWIWEYKKB",Visible=.t., Left=37, Top=486,;
    Alignment=1, Width=138, Height=18,;
    Caption="Durata minima:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="UUEQFBGCZT",Visible=.t., Left=25, Top=401,;
    Alignment=1, Width=150, Height=18,;
    Caption="Tasti funzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="LAGBPFRSBS",Visible=.t., Left=25, Top=428,;
    Alignment=1, Width=150, Height=18,;
    Caption="Eventi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="TWKNMRVKVZ",Visible=.t., Left=18, Top=288,;
    Alignment=1, Width=157, Height=18,;
    Caption="Chiamata di visual query:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="THZHJLIYZP",Visible=.t., Left=475, Top=431,;
    Alignment=1, Width=72, Height=18,;
    Caption="Debugmode:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="HWJRVHPVQE",Visible=.t., Left=25, Top=375,;
    Alignment=1, Width=150, Height=18,;
    Caption="Funzioni:"  ;
  , bGlobalFont=.t.

  add object oBox_1_54 as StdBox with uid="FSFCXWUHTK",left=10, top=138, width=705,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_setuptrace','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
