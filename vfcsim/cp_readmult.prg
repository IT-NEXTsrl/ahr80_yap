* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_readmult                                                     *
*              Read fields multilanguage                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-31                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_readmult",oParentObject)
return(i_retval)

define class tcp_readmult as StdBatch
  * --- Local variables
  w_idxlang = 0
  w_NT = 0
  w_nConn = 0
  w_cTable = space(100)
  w_cSel = space(200)
  w_CURSOR = space(50)
  w_nRes = 0
  w_CPROWNUM = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Leggo dalla tabella i valori dei campi multilingua
    this.w_CURSOR = SYS(2015)
    this.w_idxlang = alen(i_aCpLangs)+1
    this.w_NT = cp_TablePropScan(this.oParentObject.w_NameTable)
    if this.w_NT<>0
      this.w_nConn = i_TableProp[this.w_NT,3]
      this.w_cTable = cp_SetAzi(i_TableProp[this.w_NT,2])
      for i=1 to this.w_idxlang
      if i=1
        this.w_cSel = alltrim(this.oParentObject.w_VARIABLE)
      else
        this.w_cSel = this.w_cSel+","+alltrim(this.oParentObject.w_VARIABLE)+"_"+trim(i_aCpLangs(i-1))
      endif
      endfor
      this.w_cSel = this.w_cSel+",CPCCCHK"
      * --- Try
      local bErr_04CC86E0
      bErr_04CC86E0=bTrsErr
      this.Try_04CC86E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04CC86E0
      * --- End
    endif
  endproc
  proc Try_04CC86E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_nRes = cp_SQL(this.w_nConn,"select "+this.w_cSel+" from "+this.w_cTable+" where "+this.oParentObject.w_cWhere, this.w_CURSOR)
    if this.w_nRes>0
      Select (this.w_CURSOR)
      for i=1 to this.w_idxlang
      local cVariable 
 cVariable = alltrim(this.oParentObject.w_VARIABLE)+iif(i=1, "", "_"+trim(i_aCpLangs(i-1))) 
 this.oParentObject.Ctrls[i*2].value = &cVariable
      endfor
      this.oParentObject.w_CPCCCHK = CPCCCHK
      Select (this.w_CURSOR) 
 use
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
