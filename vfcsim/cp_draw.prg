* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_DRAW
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Oggetti per i disegnatori interattivi (maschere, query)
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Define Class draw_linker As Shape
    Width=15
    Height=10
    cName=''
    Proc Init(nx,ny)
        This.Top=ny
        This.Left=nx
Enddefine

Define Class draw_adder As CPToolBtn
    cMacro=''  &&Macro da eseguire sul oggetto aggiunto, utilizzato per valorizzare una proprietÓ dell'elemento aggiunto (painter_mediabtn)
    Proc Click()
        This.Drag(1)
    Proc MouseDown(nBtn,nShft,nx,ny)
        This.Ndx=nx-This.Left
        This.nDY=ny-This.Top
Enddefine

*
* --- la classe che implementa la lavagna di disegno
*
Define Class draw_board As Container
    bTracking=.F.
    bLinking=.F.
    cStartLink=''
    cEndLink=''
    *backcolor=rgb(255,255,255)
    BackColor=Rgb(192,192,192)
    BackStyle=0
    nTrackX=0
    nTrackY=0
    nSx=-1
    nSy=-1
    nOx=-1
    nOy=-1
    Ndx=0  && bisogna assolutamente dare le coordinate dell' angolo superiore sinistro
    nDY=0  && per la routine di tracciamento
    bModified=.F.
    bVFP6Err=.F.
    nVFP6ErrX=0
    nVFP6ErrY=0
   	&&Grid Snapin option
    nGridHStep=1
    nGridVStep=1
    nGridHPoint=1
    nGridVPoint=1
    bCnfMess = .F.
        
    Proc MouseDown(nBtn,nSh,nx,ny)
	    This.nDY = This.Top
        If This.bLinking
            This.bLinking=.F.
        Endif
        If nBtn=1 And Not(This.bTracking)
            If nSh<>1
                This.DeSelectAll()
            Endif
            This.bTracking=.T.
            This.AddObject('tracker','shape')
            This.tracker.Top=ny-This.nDY
            This.tracker.Left=nx-This.Ndx
            This.tracker.Width=0
            This.tracker.Height=0
            This.tracker.BackStyle=0
            This.tracker.BorderStyle=3
            This.tracker.Visible=.T.
            This.nTrackX=nx-This.Ndx
            This.nTrackY=ny-This.nDY
*!*	            nNewLeft = ROUND(oObj.Left/This.Parent.nGridHStep,0)*This.Parent.nGridHStep
*!*		       	nNewTop = ROUND(oObj.Top/This.Parent.nGridVStep,0)*This.Parent.nGridVStep  
*!*		       	nLDelta = m.nNewLeft - oObj.Left   	
*!*		       	nTDelta = m.nNewTop - oObj.Top
*!*	            N=oObj.cHandlerNE
*!*	            This.Parent.&N..Move(This.Parent.&N..Left+nLDelta , This.Parent.&N..Top+nTDelta)
        Endif
        If nBtn=2
            This.Parent.MouseDown(nBtn,nSh,nx,ny)
        Endif
        Return
    Proc MouseMove(nBtn,nSh,nx,ny)
        If This.bVFP6Err
            This.bVFP6Err=.F.
            This.EndMove(This.nVFP6ErrX,This.nVFP6ErrY)
            Return
        Endif
        If This.bTracking
            *wait window str(nX,5,0)+str(nY,5,0) nowait
            nx=nx-This.Ndx
            ny=ny-This.nDY
            If This.nTrackX<=nx
                This.tracker.Left=This.nTrackX
                This.tracker.Width=nx-This.nTrackX
            Else
                This.tracker.Left=nx
                This.tracker.Width=This.nTrackX-nx
            Endif
            If This.nTrackY<=ny
                This.tracker.Top=This.nTrackY
                This.tracker.Height=ny-This.nTrackY
            Else
                This.tracker.Top=ny
                This.tracker.Height=This.nTrackY-ny
            Endif
        Endif
        Return
    Proc MouseUp(nBtn,nSh,nx,ny)
        Local x1,y1,x2,y2
        If This.bTracking
            This.bTracking=.F.
            x1=This.tracker.Left
            y1=This.tracker.Top
            x2=x1+This.tracker.Width
            y2=y1+This.tracker.Height
            This.RemoveObject('tracker')
            For i=1 To This.ControlCount
                If This.Controls(i).InRect(x1,y1,x2,y2)
                    This.Controls(i).Select()
                Endif
            Next
        Endif
        Return
    Func AddItem(cClass,nx,ny,nW,nH,cMacro)
        Local N, i_cCnfFile,nxMod,nyMod,nGridx,nGridy
        N=Sys(2015)
        This.AddObject(N,cClass)
        nW=IIF(Vartype(nW)='N',nW,This.&N..Width)
        nH=IIF(Vartype(nH)='N',nH,This.&N..Height)
        WITH this
        nxMod = MOD(nx,.nGridHStep)
	    nyMod = MOD(ny,.nGridVStep)
		nGridx=nx-nxMod + ROUND(nxMod /.nGridHStep,0)*.nGridHStep 	
		nGridy=ny-nyMod + (ROUND(nyMod /.nGridVStep,0)-iif(Type('ThisForm.oDec')='O',1,0))*.nGridVStep
		ENDWITH    
        This.&N..Top=nGridy
        This.&N..Left=nGridx
        This.&N..Width=nW
        This.&N..Height=nH
		If Vartype(cMacro)='C' AND !EMPTY(cMacro)
			LOCAL l_macro
			l_macro = "This."+N+"."+cMacro
            &l_macro
        ENDIF
        IF TYPE('this.Parent.cCnfFile')='C' And !EMPTY(this.Parent.cCnfFile)
            i_cCnfFile=this.Parent.ccnffile
            *--Al primo utilizzo della configurazione chiedo se si vuole continuare
            IF cp_fileexist(i_cCnfFile) AND (LOWER(cClass)="painter_mediabtn")
               If !This.bCnfMess
	               ah_errormsg(cp_msgformat("E' presente un file di configurazione nelle opzioni della maschera, le proprietÓ del bottone potrebbero essere differenti da quelle selezionate"),64)
	               This.bCnfMess = .T.
               EndIf
               LOCAL i_prop, i_val, i_class
               IF USED(JUSTSTEM(i_cCnfFile))
                   USE IN SELECT (i_cCnfFile)
               ENDIF 
               i_cCnfFile=JUSTSTEM(i_cCnfFile) 
               SELECT prop,val FROM (this.Parent.ccnffile) WHERE obj="mediabtn" INTO CURSOR SaveConf
               SELECT SaveConf
               GO top
               DO WHILE NOT EOF()
                  i_prop=alltrim(prop)
                  i_val=IIF(TYPE('This.&N..&i_prop')='N',VAL(ALLTRIM(val)),IIF(TYPE('This.&N..&i_prop')='L',IIF(ALLTRIM(val)='.T.',.T.,.F.),ALLTRIM(val)))
                  This.&N..&i_prop=i_val
                  skip
               ENDDO
               USE IN SELECT (i_cCnfFile) 
               USE IN SELECT("SaveConf")
            EndIf
        ENDIF 
        &&Lo metto a false per evitare che la func HighlightMod evidenzi tutti i campi
        If Type("This."+N+".bModified") <> 'U'
        This.&N..bModified = .F.
        Endif
        This.SetModified(.T.)
        Return(N)

    Func _AddHandler(nx,ny,oObj,cType, cClass)
        Local N,o
        N='handle'+Sys(2015)
        This.AddObject(N, m.cClass)
        o=This.&N
        o.Top=ny-4
        o.Left=nx-4
        o.cObj=oObj.Name
        o.cType=cType
        o.Visible=.T.
        Return(N)

    Func AddHandler(nx,ny,oObj,cType)
        RETURN This._AddHandler(m.nx,m.ny,m.oObj,m.cType, 'draw_handler')
        
    Func AddHandlerLink(nx,ny,oObj,cType)
        RETURN This._AddHandler(m.nx,m.ny,m.oObj,m.cType, 'draw_handlerLink')
     
    Proc StartMove(nx,ny)
        This.nSx=nx
        This.nSy=ny
        This.nOx=nx
        This.nOy=ny
        Return
    Proc MoveShapes(nx,ny)
        Local dx,dy,i
        If This.nOx=-1
            This.StartMove(nx,ny)
            Return
        Endif
        Thisform.LockScreen=.T.
        dx=nx-This.nOx
        dy=ny-This.nOy
        For i=1 To This.ControlCount
            If This.Controls(i).bSelected
                This.Controls(i).MoveShapeDelta(dx,dy)
            Endif
        Next
        This.nOx=nx
        This.nOy=ny
        Thisform.LockScreen=.F.
        Return
    Proc EndMove(nx,ny)
        Local dx,dy,i
        If This.nOx=-1
            Return && ignora se non c'e' realmente stato un movimento
        Endif
        dx=nx-This.nSx
        dy=ny-This.nSy
        This.nOx=-1
        This.nOy=-1
        i=1
        Do While i<=This.ControlCount
            If This.Controls(i).bSelected
                This.Controls(i).RemoveShape()
                This.Controls(i).MoveDelta(dx,dy)
            Endif
            i=i+1
        Enddo
        This.RecalcLinkCoords()
        This.SetModified(.T.)
    Proc RecalcLinkCoords()
        Local i,n1,n2
        i=1
        For i=1 To This.ControlCount
            If This.Controls(i).bIsLink
                n1=This.Controls(i).cStart
                n2=This.Controls(i).cEnd
                If This.&n1..bSelected Or This.&n2..bSelected
                    This.Controls(i).SetCoords()
                Endif
            Endif
        Next
        *
    Proc DeSelectAll()
        Local i
        * --- deseleziona tutti gli oggetti. Viene usata una while perche'
        *     quando si deseleziona un oggetto spariscono i sui handler,
        *     rendendo non piu' valido il ControlCount
        i=1
        Do While i<=This.ControlCount
            If This.Controls(i).bSelected
                This.Controls(i).DeSelect()
            Endif
            i=i+1
        Enddo
        Return
    Proc RemoveAll()
        Do While This.ControlCount>0
            This.RemoveObject(This.Controls(1).Name)
        Enddo
    Proc RemoveSelected()
        Local i
        i=1
        DIMENSION aToDelete[1]
        Do While i<=This.ControlCount
            If This.Controls(i).bSelected
                This.Controls(i).DeSelect()
                aToDelete[ALEN(aToDelete,1)]=This.Controls(i)
                DIMENSION aToDelete[ALEN(aToDelete,1)+1]
            Else
                i=i+1
            Endif
        ENDDO
        FOR i=1 TO ALEN(aToDelete,1)-1
        	IF VARTYPE(aToDelete(i)) = 'O'
        		This.RemoveObject(aToDelete(i).Name)
        	Endif
        next
    Proc DblClickSelected()
        Local i
        i=1
        Do While i<=This.ControlCount
            If This.Controls(i).bSelected
                This.Controls(i).DblClick()
                Exit
            Endif
            i=i+1
        Enddo
    Proc MoveSelected(dx,dy)
        Local i,nx,ny
        i=1
        Do While i<=This.ControlCount
            If This.Controls(i).bSelected
                nx=This.Controls(i).Left+5
                ny=This.Controls(i).Top+5
                This.StartMove(nx,ny)
                This.EndMove(nx+dx,ny+dy)
                Exit
            Endif
            i=i+1
        Enddo
    Proc ResizeSelected(dx,dy)
        Local i,nx,ny
        i=1
        Do While i<=This.ControlCount
            If This.Controls(i).bSelected
                nx=This.Controls(i).Width
                ny=This.Controls(i).Height
                This.Controls(i).SetSize(Max(nx+dx,0),Max(ny+dy,0))
            Endif
            i=i+1
        Enddo
    Proc LockScreen(bLockMode)
        Thisform.LockScreen=bLockMode
    Proc SetModified(bStatus)
        * --- routine per segnare che il disegno e' stato modificato
        This.bModified=bStatus
        * --- gestione dei link
    Proc StartLink()
        Local i,j,n1
        j=0
        For i=1 To This.ControlCount
            If This.Controls(i).bSelected
                j=j+1
                n1=This.Controls(i).Name
            Endif
        Next
        If j=1
            This.bLinking=.T.
            This.cStartLink=n1
        Endif
    Proc EndLink(oEnd)
        This.bLinking=.F.
        This.cEndLink=oEnd.Name
        *---
        Local N
        N=Sys(2015)
        This.AddObject(N,'draw_link')
        This.&N..cStart=This.cStartLink
        This.&N..cEnd=oEnd.Name
        This.&N..SetCoords()
        This.&N..Visible=.T.
        *proc Destroy()
        * --- ????
        *this.DeSelectAll()
        *this.RemoveAll()
        * --- ????
        *
        * --- gestione del Drag&Drop da parte di un 'adder'
        *
    Proc DragDrop(oSource,nx,ny)
        If oSource.Class=='Draw_adder' Or oSource.Class=='Draw_adder_cb'
	        This.nDY = This.Top
        	This.AddItem(oSource.cType, nx-This.Ndx-oSource.Ndx, ny-This.nDY-oSource.nDY, .F., .F., oSource.cMacro)
        Endif
        *
        * --- cut,copy e paste
        *
    Proc Cut(xcg)
        Local i
        This.Copy(xcg)
        This.RemoveSelected()
    Proc Copy(xcg)
        Local i,j
        j=0
        For i=1 To This.ControlCount
            If UPPER(This.Controls(i).Class)<>"DRAW_LINENODE" AND This.Controls(i).bSelected
                j=j+1
            Endif
        Next
        xcg.Save('N',j)
        For i=1 To This.ControlCount
            If UPPER(This.Controls(i).Class)<>"DRAW_LINENODE" AND This.Controls(i).bSelected
                xcg.Save('C',This.Controls(i).Name)
                xcg.Save('C',This.Controls(i).Class)
                This.Controls(i).Serialize(xcg)
            Endif
        Next
        xcg.Eoc()
    Proc Paste(xcg)
        Local i,j,N,c
        This.DeSelectAll()
        j=xcg.Load('N',0)
        For i=1 To j
            N=xcg.Load('C','')
            c=xcg.Load('C','')
            N=Sys(2015) && !!! altrimenti esiste gia' se faccio due volte paste! problema dei link
            This.AddObject(N,c)
            This.&N..Serialize(xcg)
            This.&N..Select()
        Next
        xcg.Eoc()
        This.SetModified(.T.)
Enddefine

*
* --- la classe che implementa l' handle degli oggetti
*
Define Class draw_handler As Shape
    Width=8
    Height=8
    cObj=''
    cType=''
    bSelected=.F.
    bIsLink=.F.
    nOx=0
    nOy=0
    BackColor=Rgb(255,255,255)
    Proc MouseDown(nBtn,nSh,nx,ny)
       	LOCAL N, oObj
       	LOCAL nGridx,nGridy, nNewLeft, nNewTop, nLDelta, nTDelta
	       	N=This.cObj
            oObj=This.Parent.&N
            *-- Ridimensiono gli handlers e le dimensioni dell'oggetto in base agli step della griglia
            nNewLeft = ROUND(oObj.Left/This.Parent.nGridHStep,0)*This.Parent.nGridHStep
	       	nNewTop = ROUND(oObj.Top/This.Parent.nGridVStep,0)*This.Parent.nGridVStep  
            nNewHeight = IIF(ROUND(oObj.Height/This.Parent.nGridVStep,0)=0,1,ROUND(oObj.Height/This.Parent.nGridVStep,0))*This.Parent.nGridVStep
	       	nNewWidth = IIF(ROUND(oObj.Width/This.Parent.nGridHStep,0)=0,1,ROUND(oObj.Width/This.Parent.nGridHStep,0))*This.Parent.nGridHStep  
	       	nLDelta = m.nNewLeft - oObj.Left   	
	       	nTDelta = m.nNewTop - oObj.Top
	       	nHDelta = m.nNewHeight - oObj.Height  	
	       	nWDelta = m.nNewWidth - oObj.Width
            N=oObj.cHandlerNE
            This.Parent.&N..Move(This.Parent.&N..Left+nWDelta+nLDelta , This.Parent.&N..Top+nTDelta)
            N=oObj.cHandlerSW
            This.Parent.&N..Move(This.Parent.&N..Left+nLDelta , This.Parent.&N..Top+nHDelta+nTDelta)
            N=oObj.cHandlerNW
            This.Parent.&N..Move(This.Parent.&N..Left+nLDelta , This.Parent.&N..Top+nTDelta)
            N=oObj.cHandlerSE
            This.Parent.&N..Move(This.Parent.&N..Left+nWDelta+nLDelta , This.Parent.&N..Top+nHDelta+nTDelta)
            IF TYPE("oObj.cHandlerLinkL")<>"U"
                *-- Ridimesiono anche gli handlerlink dei mediabutton
                N=oObj.cHandlerLinkL
                This.Parent.&N..Move(This.Parent.&N..Left+nLDelta , This.Parent.&N..Top+(nHDelta+nTDelta)/2)
                N=oObj.cHandlerLinkR
                This.Parent.&N..Move(This.Parent.&N..Left+nWDelta+nLDelta, This.Parent.&N..Top+(nHDelta+nTDelta)/2)
                N=oObj.cHandlerLinkT
                This.Parent.&N..Move(This.Parent.&N..Left+(nWDelta+nLDelta)/2 , This.Parent.&N..Top+nTDelta)
                N=oObj.cHandlerLinkB
                This.Parent.&N..Move(This.Parent.&N..Left+(nWDelta+nLDelta)/2 , This.Parent.&N..Top+nHDelta+nTDelta)
            ENDIF
	       	oObj.Move(m.nNewLeft,m.nNewTop)
	       	oObj.Width = oObj.Width + nWDelta
	       	oObj.Height = oObj.Height + nHDelta
	       	ny = MROW(0,3)+nTDelta
	       	nx = MCOL(0,3)+nLDelta
	       	nxMod = MOD(nx,This.Parent.nGridHStep)
	       	nyMod = MOD(ny,This.Parent.nGridVStep)
		   	nGridx=nx-nxMod + ROUND(nxMod /This.Parent.nGridHStep,0)*This.Parent.nGridHStep 	
		   	nGridy=ny-nyMod + ROUND(nyMod /This.Parent.nGridVStep,0)*This.Parent.nGridVStep  
	        This.nOx=nGridx
	        This.nOy=nGridy
    Proc MouseMove(nBtn,nSh,nx,ny)
        Local oObj,N,dx,dy
        If nBtn=1
            N=This.cObj
            oObj=This.Parent.&N
	       	LOCAL nGridx,nGridy
	       	nxMod = MOD(nx,This.Parent.nGridHStep)
	       	nyMod = MOD(ny,This.Parent.nGridVStep)	       	
		   	nGridx=m.nx-m.nxMod + ROUND(m.nxMod /This.Parent.nGridHStep,0)*This.Parent.nGridHStep 	
		   	nGridy=m.ny-m.nyMod + ROUND(m.nyMod /This.Parent.nGridVStep,0)*This.Parent.nGridVStep
		   	dx=m.nGridx-This.nOx
            dy=m.nGridy-This.nOy            
            Do Case
                Case This.cType="nw" And oObj.Width-dx>=0 And oObj.Height-dy>=0
                    This.Left=This.Left+dx
                    This.Top=This.Top+dy
                    oObj.Left=oObj.Left+dx
                    oObj.Width=oObj.Width-dx
                    oObj.Height=oObj.Height-dy
                    oObj.Top=oObj.Top+dy
                    N=oObj.cHandlerNE
                    This.Parent.&N..MoveDelta(0,dy)
                    N=oObj.cHandlerSW
                    This.Parent.&N..MoveDelta(dx,0)
                Case This.cType="ne" And oObj.Width+dx>=0 And oObj.Height-dy>=0
                    This.Left=This.Left+dx
                    This.Top=This.Top+dy
                    oObj.Width=oObj.Width+dx
                    oObj.Top=oObj.Top+dy
                    oObj.Height=oObj.Height-dy
                    N=oObj.cHandlerNW
                    This.Parent.&N..MoveDelta(0,dy)
                    N=oObj.cHandlerSE
                    This.Parent.&N..MoveDelta(dx,0)
                Case This.cType="se" And oObj.Width+dx>=0 And oObj.Height+dy>=0
                    This.Left=This.Left+dx
                    This.Top=This.Top+dy
                    oObj.Width=oObj.Width+dx
                    oObj.Height=oObj.Height+dy
                    N=oObj.cHandlerNE
                    This.Parent.&N..MoveDelta(dx,0)
                    N=oObj.cHandlerSW
                    This.Parent.&N..MoveDelta(0,dy)
                Case This.cType="sw" And oObj.Width-dx>=0 And oObj.Height+dy>=0
                    This.Left=This.Left+dx
                    This.Top=This.Top+dy
                    oObj.Left=oObj.Left+dx
                    oObj.Width=oObj.Width-dx
                    oObj.Height=oObj.Height+dy
                    N=oObj.cHandlerSE
                    This.Parent.&N..MoveDelta(0,dy)
                    N=oObj.cHandlerNW
                    This.Parent.&N..MoveDelta(dx,0)
            ENDCASE
   	        This.nOx=nGridx
	        This.nOy=nGridy
            oObj.RedrawHandlerLink()
        Endif
        Return
    Proc MouseUp(nBtn,nSh,nx,ny)
        This.Parent.RecalcLinkCoords()
    Proc MoveDelta(dx,dy)
        This.Top=This.Top+dy
        This.Left=This.Left+dx
        Return
    Proc MoveTo(nx,ny)
        This.Left=nx-4
        This.Top=ny-4
    Proc Select()
        Return
    Proc DeSelect()
        Return
    Func InRect(x1,y1,x2,y2)
        Return(.F.)
Enddefine

*
* --- la classe che implementa i rettangolini che mostrano gli spostamenti
*
Define Class draw_shape As Shape
    BackStyle=0
    bSelected=.F.
    bIsLink=.F.
    Visible=.T.
    Proc MouseMove(b,s,x,Y)
        If This.Parent.bVFP6Err
            This.Parent.bVFP6Err=.F.
            This.Parent.EndMove(This.Parent.nVFP6ErrX,This.Parent.nVFP6ErrY)
            Return
        Endif
Enddefine

*
* --- la classe degli elementi che vengono disegnati
*
Define Class draw_item As Container
    Visible=.T.
    bHandlers=.F.
    bSelected=.F.
    bIsLink=.F.
    bToBeMoved=.F.
    BackColor=Rgb(255,0,0)
    cHandlerNW=''
    cHandlerNE=''
    cHandlerSE=''
    cHandlerSW=''
    cHandlerTypeNW='nw'
    cHandlerTypeNE='ne'
    cHandlerTypeSE='se'
    cHandlerTypeSW='sw'
    oShape=.Null.
    nSec=0
    Proc Select()
        If !This.bSelected
            This.bSelected=.T.
            This.AddHandlers()
        Endif
        Return
    Proc DeSelect()
        If This.bSelected
            This.bSelected=.F.
            This.RemoveHandlers()
        Endif
        Return
    Proc AddHandlers()
        If !This.bHandlers
            This.cHandlerNW=This.Parent.AddHandler(This.Left,This.Top,This,This.cHandlerTypeNW)
            This.cHandlerNE=This.Parent.AddHandler(This.Left+This.Width,This.Top,This,This.cHandlerTypeNE)
            This.cHandlerSE=This.Parent.AddHandler(This.Left+This.Width,This.Top+This.Height,This,This.cHandlerTypeSE)
            This.cHandlerSW=This.Parent.AddHandler(This.Left,This.Top+This.Height,This,This.cHandlerTypeSW)
            This.bHandlers=.T.
        Endif
        Return
    Proc RemoveHandlers()
        If This.bHandlers
            This.Parent.RemoveObject(This.cHandlerNW)
            This.Parent.RemoveObject(This.cHandlerNE)
            This.Parent.RemoveObject(This.cHandlerSE)
            This.Parent.RemoveObject(This.cHandlerSW)
            This.bHandlers=.F.
        Endif
        Return
    Proc MoveDelta(dx,dy)
        * --- sposta l' oggetto di dx e dy pixel
        Local N
        This.Top=This.Top+dy
        This.Left=This.Left+dx
        If This.bHandlers
            N=This.cHandlerNW
            This.Parent.&N..MoveDelta(dx,dy)
            N=This.cHandlerNE
            This.Parent.&N..MoveDelta(dx,dy)
            N=This.cHandlerSE
            This.Parent.&N..MoveDelta(dx,dy)
            N=This.cHandlerSW
            This.Parent.&N..MoveDelta(dx,dy)
        Endif
        This.Parent.SetModified(.T.)
        Return
    Proc MoveTo(nx,ny)
        This.Left=nx
        This.Top=ny
        If This.bHandlers
            N=This.cHandlerNW
            This.Parent.&N..MoveTo(This.Left,This.Top)
            N=This.cHandlerNE
            This.Parent.&N..MoveTo(This.Left+This.Width,This.Top)
            N=This.cHandlerSE
            This.Parent.&N..MoveTo(This.Left+This.Width,This.Top+This.Height)
            N=This.cHandlerSW
            This.Parent.&N..MoveTo(This.Left,This.Top+This.Height)
        Endif
        This.Parent.SetModified(.T.)
        Return
    Proc SetSize(w,h)
        Local dx,dy,N
        dx=w-This.Width
        dy=h-This.Height
        This.Width=w
        This.Height=h
        If This.bHandlers
            N=This.cHandlerNE
            This.Parent.&N..MoveDelta(dx,0)
            N=This.cHandlerSE
            This.Parent.&N..MoveDelta(dx,dy)
            N=This.cHandlerSW
            This.Parent.&N..MoveDelta(0,dy)
            This.RedrawHandlerLink()
        Endif
        This.Parent.SetModified(.T.)
        Return
    Proc MoveShapeDelta(dx,dy)
        Local N
        If Isnull(This.oShape)
            N=Sys(2015)
            This.Parent.AddObject(N,'draw_shape')
            This.oShape=This.Parent.&N
            This.oShape.Top=This.Top
            This.oShape.Left=This.Left
            This.oShape.Width=This.Width
            This.oShape.Height=This.Height
        Endif
        This.oShape.Left=This.oShape.Left+dx
        This.oShape.Top=This.oShape.Top+dy
    Proc RemoveShape()
        If !Isnull(This.oShape)
            This.Parent.RemoveObject(This.oShape.Name)
            This.oShape=.Null.
        Endif
    Proc MouseDown(nBtn,nSh,nx,ny)
        If nBtn=1
            If This.nSec+0.5>Seconds()
                This.DblClick()
                Return
            Endif
            This.nSec=Seconds()
            If This.Parent.bLinking
                This.Parent.EndLink(This)
            Else
                If This.bSelected
			    	LOCAL nGridx,nGridy
	   		       	nxMod = MOD(nx,This.Parent.nGridHStep)
			       	nyMod = MOD(ny,This.Parent.nGridVStep)
			    	nGridx=nx-nxMod + ROUND(nxMod /This.Parent.nGridHStep,0)*This.Parent.nGridHStep
			    	nGridy=ny-nyMod + ROUND(nyMod /This.Parent.nGridVStep,0)*This.Parent.nGridVStep
                    This.Parent.StartMove(nGridx,nGridy)
                    nNewLeft = ROUND(This.Left/This.Parent.nGridHStep,0)*This.Parent.nGridHStep
	               	nNewTop = ROUND(This.Top/This.Parent.nGridVStep,0)*This.Parent.nGridVStep  
                    nNewHeight = IIF(ROUND(This.Height/This.Parent.nGridVStep,0)=0,1,ROUND(This.Height/This.Parent.nGridVStep,0))*This.Parent.nGridVStep
	               	nNewWidth = IIF(ROUND(This.Width/This.Parent.nGridHStep,0)=0,1,ROUND(This.Width/This.Parent.nGridHStep,0))*This.Parent.nGridHStep  
	               	nLDelta = m.nNewLeft - This.Left   	
	               	nTDelta = m.nNewTop - This.Top
	               	nHDelta = m.nNewHeight - This.Height
	               	nWDelta = m.nNewWidth - This.Width
                    N=This.cHandlerNE
                    This.Parent.&N..Move(This.Parent.&N..Left+nWDelta+nLDelta , This.Parent.&N..Top+nTDelta)
                    N=This.cHandlerSW
                    This.Parent.&N..Move(This.Parent.&N..Left+nLDelta , This.Parent.&N..Top+nHDelta+nTDelta)
                    N=This.cHandlerNW
                    This.Parent.&N..Move(This.Parent.&N..Left+nLDelta , This.Parent.&N..Top+nTDelta)
                    N=This.cHandlerSE
                    This.Parent.&N..Move(This.Parent.&N..Left+nWDelta+nLDelta , This.Parent.&N..Top+nHDelta+nTDelta)
                    IF TYPE("This.cHandlerLinkL")<>"U"
                        *-- Ridimesiono anche gli handlerlink dei mediabutton
                        N=This.cHandlerLinkL
                        This.Parent.&N..Move(This.Parent.&N..Left+nLDelta , This.Parent.&N..Top+(nHDelta+nTDelta)/2)
                        N=This.cHandlerLinkR
                        This.Parent.&N..Move(This.Parent.&N..Left+nWDelta+nLDelta, This.Parent.&N..Top+(nHDelta+nTDelta)/2)
                        N=This.cHandlerLinkT
                        This.Parent.&N..Move(This.Parent.&N..Left+(nWDelta+nLDelta)/2 , This.Parent.&N..Top+nTDelta)
                        N=This.cHandlerLinkB
                        This.Parent.&N..Move(This.Parent.&N..Left+(nWDelta+nLDelta)/2 , This.Parent.&N..Top+nHDelta+nTDelta)
                    ENDIF
	               	This.Move(m.nNewLeft,m.nNewTop)
	               	This.Width = This.Width + nWDelta
	               	This.Height = This.Height + nHDelta
                ELSE
                    If nSh<>1
                        This.Parent.DeSelectAll()
                    Endif
                    This.Select()
                Endif
            Endif
        Endif
        Return
    Proc MouseMove(nBtn,nSh,nx,ny)
     	&&Gestione Grid
    	LOCAL nGridx,nGridy
   		nxMod = MOD(nx,This.Parent.nGridHStep)
       	nyMod = MOD(ny,This.Parent.nGridVStep)
    	nGridx=nx-nxMod + ROUND(nxMod /This.Parent.nGridHStep,0)*This.Parent.nGridHStep
    	nGridy=ny-nyMod + ROUND(nyMod /This.Parent.nGridVStep,0)*This.Parent.nGridVStep   	
        If nBtn=1 And This.bSelected
            This.Parent.MoveShapes(nGridx,nGridy)
            This.Parent.bVFP6Err=.T.
            This.Parent.nVFP6ErrX=nGridx
            This.Parent.nVFP6ErrY=nGridy
        Else
            If This.Parent.bVFP6Err
                This.Parent.bVFP6Err=.F.
                This.Parent.EndMove(This.Parent.nVFP6ErrX,This.Parent.nVFP6ErrY)
                Return
            Endif
        Endif
        Return
    Proc MouseUp(nBtn,nSh,nx,ny)
    	LOCAL nGridx,nGridy
   		nxMod = MOD(nx,This.Parent.nGridHStep)
       	nyMod = MOD(ny,This.Parent.nGridVStep)
    	nGridx=nx-nxMod + ROUND(nxMod /This.Parent.nGridHStep,0)*This.Parent.nGridHStep
    	nGridy=ny-nyMod + ROUND(nyMod /This.Parent.nGridVStep,0)*This.Parent.nGridVStep     
        If nBtn=1 And This.bSelected
            This.Parent.EndMove(nGridx,nGridy)
        EndIf
    Func InRect(x1,y1,x2,y2)
        Local res
        res=.F.
        If This.Top>=y1 And This.Top<=y2 And This.Left>=x1 And This.Left<=x2
            res=.T.
        Endif
        If This.Top+This.Height>=y1 And This.Top+This.Height<=y2 And This.Left+This.Width>=x1 And This.Left+This.Width<=x2
            res=.T.
        Endif
        Return(res)
    Proc Serialize(xcg)
        If xcg.bIsStoring
            xcg.Save('N',This.Left)
            xcg.Save('N',This.Top)
            xcg.Save('N',This.Width)
            xcg.Save('N',This.Height)
            xcg.Eoc()
        Else
            This.Left=xcg.Load('N',0)
            This.Top=xcg.Load('N',0)
            This.Width=xcg.Load('N',0)
            This.Height=xcg.Load('N',0)
            xcg.Eoc()
        Endif

	PROCEDURE RedrawHandlerLink()
	endproc
Enddefine

Define Class draw_stditem As draw_item
    BorderWidth=0
    Height=20
    Width=50
    BackColor=Rgb(236,233,216)
    Proc Init()
        This.Resize()
    Proc Resize()
        This.obj.Width=This.Width
        This.obj.Height=This.Height
    Proc DragDrop(oSource,nx,ny)
        Wait Window oSource.Class
Enddefine

Define Class draw_btn As draw_stditem
    Add Object obj As CommandButton With Width=This.Width,Height=This.Height,Caption="",Enabled=.F.

    Procedure Init()
        This.obj.Caption=cp_Translate(MSG_BTN)
        DoDefault()
    Endproc

Enddefine

Define Class draw_txt As draw_stditem
    Height=20
    Width=100
    Add Object obj As TextBox With Width=This.Width,Height=This.Height,Enabled=.F.,DisabledBackColor=Rgb(255,255,255)
    *add object obj as textbox with width=this.width,height=this.height,enabled=.t.,disabledbackcolor=rgb(255,255,255)
    *proc obj.mousedown(b,s,x,y)
    *  nodefault
    *  this.parent.mousedown(b,s,x,y)
    *proc obj.mouseup(b,s,x,y)
    *  nodefault
    *  this.parent.mouseup(b,s,x,y)
    *proc obj.mousemove(b,s,x,y)
    *  nodefault
    *  this.parent.mousemove(b,s,x,y)
Enddefine

Define Class draw_lbl As draw_stditem
    *backcolor=rgb(192,192,192)
    BackStyle=0
    Add Object obj As Label With Width=This.Width,Height=This.Height,Enabled=.F.,Caption="",BackStyle=0
    Procedure Init()
        This.obj.Caption=cp_Translate(MSG_STRING)
        DoDefault()
    Endproc

Enddefine

Define Class draw_bmp As draw_stditem
    Height=50
    Width=50
    Add Object obj As Image With Width=This.Width,Height=This.Height,Enabled=.F.,Picture=''
Enddefine

Define Class draw_line As draw_stditem
    Height=50
    Width=50
    Add Object obj As Line With Width=This.Width,Height=This.Height,Enabled=.F.
Enddefine

Define Class draw_list As draw_stditem
    Height=100
    Width=100
    Add Object obj As ListBox With Width=This.Width,Height=This.Height,Enabled=.F.
Enddefine

Define Class draw_radio As draw_stditem
    Height=40
    Width=70
    Add Object obj As OptionGroup With Width=This.Width,Height=This.Height,Enabled=.F.,ButtonCount=2,BorderStyle=0
    Proc Init()
        This.obj.Buttons(1).Caption='Radio1'
        This.obj.Buttons(2).Caption='Radio2'
        This.obj.SetAll("width",70)
        DoDefault()
        Return
    Proc SetRadioButtons(acLbls)
        Local l,i,j,h
        l=Alen(acLbls)
        j=0
        For i=1 To l
            If !Empty(acLbls[i])
                j=j+1
            Endif
        Next
        If j>0
            This.obj.ButtonCount=j
            j=0
            For i=1 To l
                If !Empty(acLbls[i])
                    j=j+1
                    This.obj.Buttons(j).Caption=acLbls[i]
                Endif
            Next
        Else
            This.obj.ButtonCount=2
            This.obj.Buttons(1).Caption='Radio1'
            This.obj.Buttons(2).Caption='Radio2'
        Endif
        This.Height=This.obj.ButtonCount*20
        If This.bHandlers
            This.RemoveHandlers()
            This.AddHandlers()
        Endif
        Return
Enddefine

Define Class draw_combo As draw_stditem
    Height=20
    Width=100
    Add Object obj As ComboBox With Width=This.Width,Height=This.Height,Enabled=.F.
Enddefine

Define Class draw_check As draw_stditem
    Height=20
    Width=100
    Add Object obj As Checkbox With Width=This.Width,Height=This.Height,Enabled=.F.
Enddefine

Define Class draw_cal As draw_item
    BorderWidth=1
    Height=120
    Width=150
    ScaleMode=3
    Add Object obj As OleControl With OleClass="MSACAL.MSACALCtrl.7",Width=250,Height=200,Enabled=.F.
    *add object obj as olecontrol with width=150,height=150,oleclass='FOXHWND.FOXHWNDCtrl.1',enabled=.f.
    Proc Init()
        This.Resize()
        This.ZOrder(1)
    Proc Resize()
        This.obj.Width=This.Width
        This.obj.Height=This.Height
Enddefine

Define Class draw_link As Line
    cStart=''
    cEnd=''
    bCenterToCenter=.T.
    bSelected=.F.
    bIsLink=.T.
    Enabled=.F.
    Proc SetCoords()
        Local sx,sy,dx,dy,oStart,oEnd,i_n
        i_n=This.cStart
        oStart=This.Parent.&i_n
        i_n=This.cEnd
        oEnd=This.Parent.&i_n
        If This.bCenterToCenter
            sy=oStart.Top+oStart.Height/2 && centro con centro
        Else
            sy=oStart.Top+oStart.Height && come analisi di codepainter
        Endif
        sx=oStart.Left+oStart.Width/2
        dy=oEnd.Top+oEnd.Height/2-sy
        *dy=this.oEnd.top+this.oEnd.height-sy && link contrari rispetto codepainter
        dx=oEnd.Left+oEnd.Width/2-sx && come analisi di codepainter
        This.LineSlant=Iif(dx*dy<0,'/','\')
        If dx<0
            sx=sx+dx
            dx=-dx
        Endif
        If dy<0
            sy=sy+dy
            dy=-dy
        Endif
        This.Top=sy
        This.Left=sx
        This.Width=dx
        This.Height=dy
        This.ZOrder(1)
    Func InRect(x1,y1,x2,y2)
        Return(.F.)
Enddefine

Define Class draw_align As Custom
    * --- gestione degli allineamenti
    Proc AlignLeft(oDraw)
        Local i,nx
        nx=10000
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected
                nx=Min(nx,oDraw.Controls(i).Left)
            Endif
        Next
        oDraw.LockScreen(.T.)
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected
                oDraw.Controls(i).MoveTo(nx,oDraw.Controls(i).Top)
            Endif
        Next
        oDraw.LockScreen(.F.)
    Proc Alignright(oDraw)
        Local i,nx
        nx=0
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected
                nx=Max(nx,oDraw.Controls(i).Left+oDraw.Controls(i).Width)
            Endif
        Next
        oDraw.LockScreen(.T.)
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected
                oDraw.Controls(i).MoveTo(nx-oDraw.Controls(i).Width,oDraw.Controls(i).Top)
            Endif
        Next
        oDraw.LockScreen(.F.)
    Proc AlignTop(oDraw)
        Local i,ny
        ny=10000
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected
                ny=Min(ny,oDraw.Controls(i).Top)
            Endif
        Next
        oDraw.LockScreen(.T.)
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected
                oDraw.Controls(i).MoveTo(oDraw.Controls(i).Left,ny)
            Endif
        Next
        oDraw.LockScreen(.F.)
    Proc AlignBottom(oDraw)
        Local i,ny
        ny=0
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected
                ny=Max(ny,oDraw.Controls(i).Top)
            Endif
        Next
        oDraw.LockScreen(.T.)
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected
                oDraw.Controls(i).MoveTo(oDraw.Controls(i).Left,ny)
            Endif
        Next
        oDraw.LockScreen(.F.)
    Proc SameHDist(oDraw)
        Local i,nx,dx,px,c1,c2
        c1=0
        c2=0
        nx=10000
        * --- cerca l' elemento piu' a sinistra
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected And oDraw.Controls(i).Left<nx
                c1=i
                nx=oDraw.Controls(i).Left
            Endif
        Next
        * --- cerca il secondo elemento piu' a sinistra
        nx=10000
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected And i<>c1 And oDraw.Controls(i).Left<nx
                c2=i
                nx=oDraw.Controls(i).Left
            Endif
        Next
        If c1<>0 And c2<>0
            dx=oDraw.Controls(c2).Left-(oDraw.Controls(c1).Left+oDraw.Controls(c1).Width)
            If dx>0
                oDraw.LockScreen(.T.)
                px=oDraw.Controls(c1).Left+oDraw.Controls(c1).Width+dx
                For i=1 To oDraw.ControlCount
                    If oDraw.Controls(i).bSelected And i<>c1
                        oDraw.Controls(i).bToBeMoved=.T.
                    Endif
                Next
                Do While .T.
                    c2=0
                    nx=10000
                    For i=1 To oDraw.ControlCount
                        If oDraw.Controls(i).bSelected And oDraw.Controls(i).bToBeMoved And oDraw.Controls(i).Left<nx
                            nx=oDraw.Controls(i).Left
                            c2=i
                        Endif
                    Next
                    If c2<>0
                        oDraw.Controls(c2).MoveTo(px,oDraw.Controls(c2).Top)
                        px=px+oDraw.Controls(c2).Width+dx
                        oDraw.Controls(c2).bToBeMoved=.F.
                    Else
                        Exit
                    Endif
                Enddo
                oDraw.LockScreen(.F.)
            Endif
        Endif
    Proc SameVDist(oDraw)
        Local i,ny,dy,py,c1,c2
        c1=0
        c2=0
        ny=10000
        * --- cerca l' elemento piu' in alto
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected And oDraw.Controls(i).Top<ny
                c1=i
                ny=oDraw.Controls(i).Top
            Endif
        Next
        * --- cerca il secondo elemento piu' in alto
        ny=10000
        For i=1 To oDraw.ControlCount
            If oDraw.Controls(i).bSelected And i<>c1 And oDraw.Controls(i).Top<ny
                c2=i
                ny=oDraw.Controls(i).Top
            Endif
        Next
        If c1<>0 And c2<>0
            dy=oDraw.Controls(c2).Top-(oDraw.Controls(c1).Top+oDraw.Controls(c1).Height)
            If dy>0
                oDraw.LockScreen(.T.)
                py=oDraw.Controls(c1).Top+oDraw.Controls(c1).Height+dy
                For i=1 To oDraw.ControlCount
                    If oDraw.Controls(i).bSelected And i<>c1
                        oDraw.Controls(i).bToBeMoved=.T.
                    Endif
                Next
                Do While .T.
                    c2=0
                    ny=10000
                    For i=1 To oDraw.ControlCount
                        If oDraw.Controls(i).bSelected And oDraw.Controls(i).bToBeMoved And oDraw.Controls(i).Top<ny
                            ny=oDraw.Controls(i).Top
                            c2=i
                        Endif
                    Next
                    If c2<>0
                        oDraw.Controls(c2).MoveTo(oDraw.Controls(c2).Left,py)
                        py=py+oDraw.Controls(c2).Height+dy
                        oDraw.Controls(c2).bToBeMoved=.F.
                    Else
                        Exit
                    Endif
                Enddo
                oDraw.LockScreen(.F.)
            Endif
        ENDIF       
Enddefine        
