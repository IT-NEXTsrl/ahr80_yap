* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_infotreeview                                                 *
*              ProprietÓ treeview                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-03-02                                                      *
* Last revis.: 2009-08-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_infotreeview
cp_ReadXdc()
* --- Fine Area Manuale
return(createobject("tcp_infotreeview",oParentObject))

* --- Class definition
define class tcp_infotreeview as StdForm
  Top    = 144
  Left   = 219

  * --- Standard Properties
  Width  = 634
  Height = 133
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-08-04"
  HelpContextID=60146213
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_infotreeview"
  cComment = "ProprietÓ treeview"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_cNodeCount = space(30)
  w_cTreeviewName = space(30)
  w_cMenuFile = space(30)
  w_cSelectedNode = space(200)
  w_cSelectedNodeIndex = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_infotreeviewPag1","cp_infotreeview",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.ocNodeCount_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_infotreeview
    * --- Translate interface...
     this.parent.cComment=cp_Translate(MSG_TREEVIEW_PROPERTY)
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_cNodeCount=space(30)
      .w_cTreeviewName=space(30)
      .w_cMenuFile=space(30)
      .w_cSelectedNode=space(200)
      .w_cSelectedNodeIndex=space(30)
        .w_cNodeCount = alltrim(str(.oParentObject.oCtrl.oTree.Nodes.Count()-1 ))
        .w_cTreeviewName = .oParentObject.oCtrl.Name
        .w_cMenuFile = .oParentObject.cMenuFile
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_SHOW_NODE_COUNT)
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_SHOW_SELECTED_NODE)
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_NAME+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_CONTEXT_MENU+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_SHOW_NODE_COUNT)
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_SHOW_SELECTED_NODE)
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_SHOW_SELECTED_NODE)
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_SHOW_TREEVIEW_NAME)
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
        .w_cSelectedNode = alltrim(iif(type('this.oParentObject.oCtrl.oTree.selectedItem.Text')='C', .oParentObject.oCtrl.oTree.selectedItem.Text , ""))
        .w_cSelectedNodeIndex = alltrim(str(iif(type('this.oParentObject.oCtrl.oTree.selectedItem.Index()')='N' , .oParentObject.oCtrl.oTree.selectedItem.Index() , 0 )))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_SHOW_NODE_COUNT)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_SHOW_SELECTED_NODE)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_NAME+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_CONTEXT_MENU+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_SHOW_NODE_COUNT)
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_SHOW_SELECTED_NODE)
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_SHOW_SELECTED_NODE)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_SHOW_TREEVIEW_NAME)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_SHOW_NODE_COUNT)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_SHOW_SELECTED_NODE)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_NAME+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_CONTEXT_MENU+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_SHOW_NODE_COUNT)
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_SHOW_SELECTED_NODE)
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_SHOW_SELECTED_NODE)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_SHOW_TREEVIEW_NAME)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.ocNodeCount_1_1.value==this.w_cNodeCount)
      this.oPgFrm.Page1.oPag.ocNodeCount_1_1.value=this.w_cNodeCount
    endif
    if not(this.oPgFrm.Page1.oPag.ocTreeviewName_1_2.value==this.w_cTreeviewName)
      this.oPgFrm.Page1.oPag.ocTreeviewName_1_2.value=this.w_cTreeviewName
    endif
    if not(this.oPgFrm.Page1.oPag.ocMenuFile_1_3.value==this.w_cMenuFile)
      this.oPgFrm.Page1.oPag.ocMenuFile_1_3.value=this.w_cMenuFile
    endif
    if not(this.oPgFrm.Page1.oPag.ocSelectedNode_1_17.value==this.w_cSelectedNode)
      this.oPgFrm.Page1.oPag.ocSelectedNode_1_17.value=this.w_cSelectedNode
    endif
    if not(this.oPgFrm.Page1.oPag.ocSelectedNodeIndex_1_19.value==this.w_cSelectedNodeIndex)
      this.oPgFrm.Page1.oPag.ocSelectedNodeIndex_1_19.value=this.w_cSelectedNodeIndex
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_infotreeviewPag1 as StdContainer
  Width  = 630
  height = 133
  stdWidth  = 630
  stdheight = 133
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object ocNodeCount_1_1 as StdField with uid="DJGFQBJCBL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_cNodeCount", cQueryName = "cNodeCount",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome fisico della tabella",;
    HelpContextID = 147751402,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=135, Top=11, InputMask=replicate('X',30), readOnly=.T.

  add object ocTreeviewName_1_2 as StdField with uid="JLOLDFOPRK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_cTreeviewName", cQueryName = "cTreeviewName",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome dello zoom",;
    HelpContextID = 187842529,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=135, Top=60, InputMask=replicate('X',30), readOnly=.T.

  add object ocMenuFile_1_3 as StdField with uid="YLUBTYZYIL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_cMenuFile", cQueryName = "cMenuFile",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome del menu contestuale associato",;
    HelpContextID = 24251581,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=135, Top=87, InputMask=replicate('X',30), readOnly=.T.


  add object oBtn_1_4 as StdButton with uid="YVVNOWRCVM",left=571, top=105, width=50,height=20,;
    caption="Ok", nPag=1;
    , HelpContextID = 60148004;
    , caption=MSG_OK_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_8 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=7, top=164, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Numero nodi:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 188711572


  add object oObj_1_9 as cp_setCtrlObjprop with uid="MXMKFOCINM",left=7, top=190, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nodo selezionato:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 188711572


  add object oObj_1_10 as cp_setCtrlObjprop with uid="RUSBZEOSXF",left=7, top=216, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nome:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 188711572


  add object oObj_1_11 as cp_setCtrlObjprop with uid="IUHGJLEFDC",left=7, top=242, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Menu contestuale:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 188711572


  add object oObj_1_12 as cp_setobjprop with uid="SKPUZOAYUU",left=280, top=164, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cNodeCount",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 188711572


  add object oObj_1_13 as cp_setobjprop with uid="EHQUCINFUP",left=280, top=190, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cSelectedNode",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 188711572


  add object oObj_1_14 as cp_setobjprop with uid="CRQJWNEWNL",left=280, top=216, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cSelectedNodeIndex",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 188711572


  add object oObj_1_15 as cp_setobjprop with uid="RUDVSJVAGV",left=280, top=242, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cTreeviewName",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 188711572


  add object oObj_1_16 as cp_setobjprop with uid="UCSUEVDYTN",left=280, top=268, width=168,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_cMenufile",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 188711572

  add object ocSelectedNode_1_17 as StdField with uid="QRMVFDBRET",rtseq=4,rtrep=.f.,;
    cFormVar = "w_cSelectedNode", cQueryName = "cSelectedNode",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome fisico della tabella",;
    HelpContextID = 23224068,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=135, Top=35, InputMask=replicate('X',200), readOnly=.T.

  add object ocSelectedNodeIndex_1_19 as StdField with uid="KYIFLQSXIL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_cSelectedNodeIndex", cQueryName = "cSelectedNodeIndex",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza il nome fisico della tabella",;
    HelpContextID = 248665887,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=573, Top=35, InputMask=replicate('X',30), readOnly=.T.

  add object oStr_1_5 as StdString with uid="TQWDNGNLBG",Visible=.t., Left=32, Top=62,;
    Alignment=1, Width=98, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="FFBNAJVRKR",Visible=.t., Left=20, Top=87,;
    Alignment=1, Width=110, Height=18,;
    Caption="Menu contestuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ZSGXKEPLFV",Visible=.t., Left=35, Top=11,;
    Alignment=1, Width=95, Height=18,;
    Caption="Numero nodi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="PUBHYIXGFV",Visible=.t., Left=8, Top=35,;
    Alignment=1, Width=122, Height=18,;
    Caption="Nodo selezionato:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_infotreeview','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
