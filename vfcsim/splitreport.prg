* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: splitreport                                                     *
*              Split report                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-12                                                      *
* Last revis.: 2011-06-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,oMultiReport,oRetMultiReport
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tsplitreport",oParentObject,m.oMultiReport,m.oRetMultiReport)
return(i_retval)

define class tsplitreport as StdBatch
  * --- Local variables
  oMultiReport = .NULL.
  oRetMultiReport = .NULL.
  w_COUNT = 0
  w_i = 0
  w_nMainIndex = 0
  w_cMainCursor = space(10)
  w_old_area = 0
  w_StmpOrderListStr = space(10)
  w_nIndex = 0
  w_cIndexOrder = space(10)
  w_SplitCursor = space(10)
  w_tempCursor = space(10)
  w_nIndex = 0
  w_cSplitField = space(10)
  w_child = space(10)
  w_cDettOrder = space(10)
  w_cOldMainCursor = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione errori
     PRIVATE Messaggio,L_errsav, l_splitExpr , l_RowNumName 
 L_errsav=on("ERROR") 
 Messaggio="" 
 ON ERROR Messaggio=Message()+" "+Message(1)
    * --- Controllo se ho almeno un report principale
    if !this.oMultiReport.IsEmptyCursor() 
      this.w_old_area = SELECT()
      * --- Ciclo sulle categorie
      ah_Msg("Fascicolazione stampa in corso...")
      l_RowNumName=SYS(2015)
      this.w_SplitCursor = SYS(2015)
      this.w_StmpOrderListStr = this.oMultiReport.GetStmpOrderList()
      this.w_COUNT = ALINES(w_StmpOrderList, this.w_StmpOrderListStr, ",")
      this.w_i = 1
      do while this.w_i <= this.w_COUNT
        * --- *--- Report principale della categoria corrente
        *     *--- Ciclo su tutti i report principali della categoria (ristampa documenti)
        this.w_nMainIndex = 0
        do while .T.
          this.w_nMainIndex = this.oMultiReport.GetMainReport(w_StmpOrderList(this.w_i), this.w_nMainIndex)
          if this.w_nMainIndex<>-1
            this.w_child = this.oMultiReport.GetChild(this.w_nMainIndex)
            if  NOT EMPTY(this.w_child)
              l_splitExpr = this.oMultiReport.GetSplit(this.w_nMainIndex)
              this.w_cMainCursor = this.oMultiReport.GetCursorName(this.w_nMainIndex)
              this.w_cOldMainCursor = "" 
              if NOT EMPTY(l_splitExpr)
                this.w_tempCursor = SYS(2015)
                SELECT *, RECNO() AS (l_RowNumName) FROM (this.w_cMainCursor) INTO CURSOR (this.w_tempCursor)
                * ---  l_SplitCursor contiene le chiavi di rottura
                SELECT DISTINCT &l_splitExpr AS SPLITFIELD FROM (this.w_tempCursor) ORDER BY &l_RowNumName INTO CURSOR (this.w_SplitCursor) READWRITE 
 USE IN SELECT(this.w_tempCursor) 
 SELECT(this.w_SplitCursor) 
 GO TOP 
 SCAN
                this.w_tempCursor = SYS(2015)
                this.w_cSplitField = SPLITFIELD
                SELECT * FROM (this.w_cMainCursor) WHERE &l_splitExpr=this.w_cSplitField INTO CURSOR (this.w_tempCursor) READWRITE 
 *--- Split dei report secondari 
 SplitSecReport(null,this.oMultiReport, this.oRetMultiReport, this.w_tempCursor, this.w_nMainIndex, w_StmpOrderList(this.w_i), this.w_cSplitField, this.w_cOldMainCursor) 
 SELECT(this.w_SplitCursor) 
 ENDSCAN 
 USE IN SELECT(this.w_SplitCursor)
              else
                * --- Split dei report secondari
                SplitSecReport(this, this.oMultiReport, this.oRetMultiReport, this.w_cMainCursor, this.w_nMainIndex, w_StmpOrderList(this.w_i), this.w_cOldMainCursor)
              endif
            else
              * --- Ricopio tutti i dati dal multireport sorgente a quello destinatario
              this.w_nIndex = this.oRetMultiReport.CopyReport(this.oMultiReport, this.w_nMainIndex,, .t.)
              * --- Cambio il cursore
              this.oRetMultiReport.SetCursor(this.oMultiReport.GetCursorName(this.w_nMainIndex), this.w_nIndex) 
 this.oRetMultiReport.SetIndex(this.w_nMainIndex, this.w_nIndex)
              this.w_cIndexOrder = RIGHT("00000"+ALLTRIM(TRANSFORM(this.w_nIndex)),5)
              this.w_cDettOrder = RIGHT("00000"+ALLTRIM(TRANSFORM(this.oRetMultiReport.GetDettOrder(this.w_nIndex))),5)
              this.oRetMultiReport.SetOrder(this.oRetMultiReport.GetStmpOrder(this.w_nIndex)+"_"+this.w_cIndexOrder+"_"+this.w_cDettOrder, this.w_nIndex) 
 LOOP
            endif
          else
            EXIT
          endif
        enddo
        this.w_i = this.w_i + 1
      enddo
      * --- Ordino per STMPORDER e DETTORDER
      this.oRetMultiReport.OrderReport()
      * --- Valorizza reportbehavior
      this.oMultiReport.CopyHeader(this.oRetMultiReport) 
 WAIT CLEAR 
 SELECT(this.w_old_area)
    endif
    on error &L_errsav
    if not empty(Messaggio)
      ah_ErrorMsg("Si � verificato un errore durante la fascicolazione%0%1","","",Messaggio)
      * --- ripulisco il MultiReport
      this.oRetMultiReport.ResetReport() 
 i_retval=.F. 
 RETURN .F.
    endif
    i_retval=.T. 
 RETURN .T.
  endproc


  proc Init(oParentObject,oMultiReport,oRetMultiReport)
    this.oMultiReport=oMultiReport
    this.oRetMultiReport=oRetMultiReport
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="oMultiReport,oRetMultiReport"
endproc
