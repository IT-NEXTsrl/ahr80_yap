* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_setgui                                                       *
*              Configurazione interfaccia                                      *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_132]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-04                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tcp_setgui")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tccp_setgui")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tccp_setgui")
  return

* --- Class definition
define class tcp_setgui as StdPCForm
  Width  = 535
  Height = 343+35
  Top    = 6
  Left   = 9
  cComment = "Configurazione interfaccia"
  cPrg = "cp_setgui"
  HelpContextID=22243636
  add object cnt as tccp_setgui
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tscp_setgui as PCContext
  w_usrcode = 0
  w_CIVTHEME = 0
  w_CIREPBEH = space(1)
  w_CICPTBAR = space(1)
  w_CIDSKBAR = space(1)
  w_CISTABAR = space(1)
  w_CISHWBTN = space(1)
  w_CITBSIZE = 0
  w_CIMDIFRM = space(1)
  w_CITABMEN = space(1)
  w_CIXPTHEM = 0
  w_CIMENFIX = space(1)
  w_CISEARMN = space(1)
  w_CITOOLMN = space(1)
  w_CIBCKGRD = space(1)
  w_CIMRKGRD = space(1)
  w_CISELECL = 0
  w_CIEDTCOL = 0
  w_CIGRIDCL = 0
  w_CIDTLCLR = 0
  w_CIDSBLBC = 0
  w_CIDSBLFC = 0
  w_CISCRCOL = 0
  w_COLORCTSEL = space(10)
  w_CIEVIZOM = 0
  w_COLORCTED = space(10)
  w_COLORFCTDSBL = space(10)
  w_COLORBCTDSBL = space(10)
  w_COLORGRD = space(10)
  w_COLORSCREEN = space(10)
  w_COLORROWDTL = space(10)
  w_CILBLFNM = space(50)
  w_CITXTFNM = space(50)
  w_CICBXFNM = space(50)
  w_CIBTNFNM = space(50)
  w_CIGRDFNM = space(50)
  w_CIPAGFNM = space(50)
  w_CILBLFSZ = 0
  w_CITXTFSZ = 0
  w_CICBXFSZ = 0
  w_CIBTNFSZ = 0
  w_CIGRDFSZ = 0
  w_CIPAGFSZ = 0
  w_CIDSKMEN = space(1)
  w_CINAVSTA = space(1)
  w_CINAVNUB = 0
  w_CINAVDIM = 0
  w_CIMNAFNM = space(50)
  w_CIMNAFSZ = 0
  w_CIWMAFNM = space(50)
  w_CIWMAFSZ = 0
  w_RET = space(1)
  w_CIOBLCOL = 0
  w_COLORCTOB = space(10)
  w_ciobl_on = space(1)
  w_COLORZOM = space(10)
  w_CICOLZOM = 0
  w_ciwaitwd = space(1)
  w_cilblfit = space(1)
  w_citxtfit = space(1)
  w_cicbxfit = space(1)
  w_cibtnfit = space(1)
  w_cigrdfit = space(1)
  w_cipagfit = space(1)
  w_cilblfbo = space(1)
  w_citxtfbo = space(1)
  w_cicbxfbo = space(1)
  w_cibtnfbo = space(1)
  w_cigrdfbo = space(1)
  w_cipagfbo = space(1)
  proc Save(oFrom)
    this.w_usrcode = oFrom.w_usrcode
    this.w_CIVTHEME = oFrom.w_CIVTHEME
    this.w_CIREPBEH = oFrom.w_CIREPBEH
    this.w_CICPTBAR = oFrom.w_CICPTBAR
    this.w_CIDSKBAR = oFrom.w_CIDSKBAR
    this.w_CISTABAR = oFrom.w_CISTABAR
    this.w_CISHWBTN = oFrom.w_CISHWBTN
    this.w_CITBSIZE = oFrom.w_CITBSIZE
    this.w_CIMDIFRM = oFrom.w_CIMDIFRM
    this.w_CITABMEN = oFrom.w_CITABMEN
    this.w_CIXPTHEM = oFrom.w_CIXPTHEM
    this.w_CIMENFIX = oFrom.w_CIMENFIX
    this.w_CISEARMN = oFrom.w_CISEARMN
    this.w_CITOOLMN = oFrom.w_CITOOLMN
    this.w_CIBCKGRD = oFrom.w_CIBCKGRD
    this.w_CIMRKGRD = oFrom.w_CIMRKGRD
    this.w_CISELECL = oFrom.w_CISELECL
    this.w_CIEDTCOL = oFrom.w_CIEDTCOL
    this.w_CIGRIDCL = oFrom.w_CIGRIDCL
    this.w_CIDTLCLR = oFrom.w_CIDTLCLR
    this.w_CIDSBLBC = oFrom.w_CIDSBLBC
    this.w_CIDSBLFC = oFrom.w_CIDSBLFC
    this.w_CISCRCOL = oFrom.w_CISCRCOL
    this.w_COLORCTSEL = oFrom.w_COLORCTSEL
    this.w_CIEVIZOM = oFrom.w_CIEVIZOM
    this.w_COLORCTED = oFrom.w_COLORCTED
    this.w_COLORFCTDSBL = oFrom.w_COLORFCTDSBL
    this.w_COLORBCTDSBL = oFrom.w_COLORBCTDSBL
    this.w_COLORGRD = oFrom.w_COLORGRD
    this.w_COLORSCREEN = oFrom.w_COLORSCREEN
    this.w_COLORROWDTL = oFrom.w_COLORROWDTL
    this.w_CILBLFNM = oFrom.w_CILBLFNM
    this.w_CITXTFNM = oFrom.w_CITXTFNM
    this.w_CICBXFNM = oFrom.w_CICBXFNM
    this.w_CIBTNFNM = oFrom.w_CIBTNFNM
    this.w_CIGRDFNM = oFrom.w_CIGRDFNM
    this.w_CIPAGFNM = oFrom.w_CIPAGFNM
    this.w_CILBLFSZ = oFrom.w_CILBLFSZ
    this.w_CITXTFSZ = oFrom.w_CITXTFSZ
    this.w_CICBXFSZ = oFrom.w_CICBXFSZ
    this.w_CIBTNFSZ = oFrom.w_CIBTNFSZ
    this.w_CIGRDFSZ = oFrom.w_CIGRDFSZ
    this.w_CIPAGFSZ = oFrom.w_CIPAGFSZ
    this.w_CIDSKMEN = oFrom.w_CIDSKMEN
    this.w_CINAVSTA = oFrom.w_CINAVSTA
    this.w_CINAVNUB = oFrom.w_CINAVNUB
    this.w_CINAVDIM = oFrom.w_CINAVDIM
    this.w_CIMNAFNM = oFrom.w_CIMNAFNM
    this.w_CIMNAFSZ = oFrom.w_CIMNAFSZ
    this.w_CIWMAFNM = oFrom.w_CIWMAFNM
    this.w_CIWMAFSZ = oFrom.w_CIWMAFSZ
    this.w_RET = oFrom.w_RET
    this.w_CIOBLCOL = oFrom.w_CIOBLCOL
    this.w_COLORCTOB = oFrom.w_COLORCTOB
    this.w_ciobl_on = oFrom.w_ciobl_on
    this.w_COLORZOM = oFrom.w_COLORZOM
    this.w_CICOLZOM = oFrom.w_CICOLZOM
    this.w_ciwaitwd = oFrom.w_ciwaitwd
    this.w_cilblfit = oFrom.w_cilblfit
    this.w_citxtfit = oFrom.w_citxtfit
    this.w_cicbxfit = oFrom.w_cicbxfit
    this.w_cibtnfit = oFrom.w_cibtnfit
    this.w_cigrdfit = oFrom.w_cigrdfit
    this.w_cipagfit = oFrom.w_cipagfit
    this.w_cilblfbo = oFrom.w_cilblfbo
    this.w_citxtfbo = oFrom.w_citxtfbo
    this.w_cicbxfbo = oFrom.w_cicbxfbo
    this.w_cibtnfbo = oFrom.w_cibtnfbo
    this.w_cigrdfbo = oFrom.w_cigrdfbo
    this.w_cipagfbo = oFrom.w_cipagfbo
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_usrcode = this.w_usrcode
    oTo.w_CIVTHEME = this.w_CIVTHEME
    oTo.w_CIREPBEH = this.w_CIREPBEH
    oTo.w_CICPTBAR = this.w_CICPTBAR
    oTo.w_CIDSKBAR = this.w_CIDSKBAR
    oTo.w_CISTABAR = this.w_CISTABAR
    oTo.w_CISHWBTN = this.w_CISHWBTN
    oTo.w_CITBSIZE = this.w_CITBSIZE
    oTo.w_CIMDIFRM = this.w_CIMDIFRM
    oTo.w_CITABMEN = this.w_CITABMEN
    oTo.w_CIXPTHEM = this.w_CIXPTHEM
    oTo.w_CIMENFIX = this.w_CIMENFIX
    oTo.w_CISEARMN = this.w_CISEARMN
    oTo.w_CITOOLMN = this.w_CITOOLMN
    oTo.w_CIBCKGRD = this.w_CIBCKGRD
    oTo.w_CIMRKGRD = this.w_CIMRKGRD
    oTo.w_CISELECL = this.w_CISELECL
    oTo.w_CIEDTCOL = this.w_CIEDTCOL
    oTo.w_CIGRIDCL = this.w_CIGRIDCL
    oTo.w_CIDTLCLR = this.w_CIDTLCLR
    oTo.w_CIDSBLBC = this.w_CIDSBLBC
    oTo.w_CIDSBLFC = this.w_CIDSBLFC
    oTo.w_CISCRCOL = this.w_CISCRCOL
    oTo.w_COLORCTSEL = this.w_COLORCTSEL
    oTo.w_CIEVIZOM = this.w_CIEVIZOM
    oTo.w_COLORCTED = this.w_COLORCTED
    oTo.w_COLORFCTDSBL = this.w_COLORFCTDSBL
    oTo.w_COLORBCTDSBL = this.w_COLORBCTDSBL
    oTo.w_COLORGRD = this.w_COLORGRD
    oTo.w_COLORSCREEN = this.w_COLORSCREEN
    oTo.w_COLORROWDTL = this.w_COLORROWDTL
    oTo.w_CILBLFNM = this.w_CILBLFNM
    oTo.w_CITXTFNM = this.w_CITXTFNM
    oTo.w_CICBXFNM = this.w_CICBXFNM
    oTo.w_CIBTNFNM = this.w_CIBTNFNM
    oTo.w_CIGRDFNM = this.w_CIGRDFNM
    oTo.w_CIPAGFNM = this.w_CIPAGFNM
    oTo.w_CILBLFSZ = this.w_CILBLFSZ
    oTo.w_CITXTFSZ = this.w_CITXTFSZ
    oTo.w_CICBXFSZ = this.w_CICBXFSZ
    oTo.w_CIBTNFSZ = this.w_CIBTNFSZ
    oTo.w_CIGRDFSZ = this.w_CIGRDFSZ
    oTo.w_CIPAGFSZ = this.w_CIPAGFSZ
    oTo.w_CIDSKMEN = this.w_CIDSKMEN
    oTo.w_CINAVSTA = this.w_CINAVSTA
    oTo.w_CINAVNUB = this.w_CINAVNUB
    oTo.w_CINAVDIM = this.w_CINAVDIM
    oTo.w_CIMNAFNM = this.w_CIMNAFNM
    oTo.w_CIMNAFSZ = this.w_CIMNAFSZ
    oTo.w_CIWMAFNM = this.w_CIWMAFNM
    oTo.w_CIWMAFSZ = this.w_CIWMAFSZ
    oTo.w_RET = this.w_RET
    oTo.w_CIOBLCOL = this.w_CIOBLCOL
    oTo.w_COLORCTOB = this.w_COLORCTOB
    oTo.w_ciobl_on = this.w_ciobl_on
    oTo.w_COLORZOM = this.w_COLORZOM
    oTo.w_CICOLZOM = this.w_CICOLZOM
    oTo.w_ciwaitwd = this.w_ciwaitwd
    oTo.w_cilblfit = this.w_cilblfit
    oTo.w_citxtfit = this.w_citxtfit
    oTo.w_cicbxfit = this.w_cicbxfit
    oTo.w_cibtnfit = this.w_cibtnfit
    oTo.w_cigrdfit = this.w_cigrdfit
    oTo.w_cipagfit = this.w_cipagfit
    oTo.w_cilblfbo = this.w_cilblfbo
    oTo.w_citxtfbo = this.w_citxtfbo
    oTo.w_cicbxfbo = this.w_cicbxfbo
    oTo.w_cibtnfbo = this.w_cibtnfbo
    oTo.w_cigrdfbo = this.w_cigrdfbo
    oTo.w_cipagfbo = this.w_cipagfbo
    PCContext::Load(oTo)
enddefine

define class tccp_setgui as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 535
  Height = 343+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=22243636
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=70

  * --- Constant Properties
  cpsetgui_IDX = 0
  CPUSERS_IDX = 0
  cFile = "cpsetgui"
  cKeySelect = "usrcode"
  cKeyWhere  = "usrcode=this.w_usrcode"
  cKeyWhereODBC = '"usrcode="+cp_ToStrODBC(this.w_usrcode)';

  cKeyWhereODBCqualified = '"cpsetgui.usrcode="+cp_ToStrODBC(this.w_usrcode)';

  cPrg = "cp_setgui"
  cComment = "Configurazione interfaccia"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_usrcode = 0
  w_CIVTHEME = 0
  o_CIVTHEME = 0
  w_CIREPBEH = space(1)
  w_CICPTBAR = space(1)
  w_CIDSKBAR = space(1)
  w_CISTABAR = space(1)
  w_CISHWBTN = space(1)
  w_CITBSIZE = 0
  w_CIMDIFRM = space(1)
  w_CITABMEN = space(1)
  o_CITABMEN = space(1)
  w_CIXPTHEM = 0
  w_CIMENFIX = space(1)
  w_CISEARMN = space(1)
  w_CITOOLMN = space(1)
  w_CIBCKGRD = space(1)
  w_CIMRKGRD = space(1)
  w_CISELECL = 0
  w_CIEDTCOL = 0
  w_CIGRIDCL = 0
  w_CIDTLCLR = 0
  w_CIDSBLBC = 0
  w_CIDSBLFC = 0
  w_CISCRCOL = 0
  w_COLORCTSEL = space(10)
  w_CIEVIZOM = 0
  w_COLORCTED = space(10)
  w_COLORFCTDSBL = space(10)
  w_COLORBCTDSBL = space(10)
  w_COLORGRD = space(10)
  w_COLORSCREEN = space(10)
  w_COLORROWDTL = space(10)
  w_CILBLFNM = space(50)
  w_CITXTFNM = space(50)
  w_CICBXFNM = space(50)
  w_CIBTNFNM = space(50)
  w_CIGRDFNM = space(50)
  w_CIPAGFNM = space(50)
  w_CILBLFSZ = 0
  w_CITXTFSZ = 0
  w_CICBXFSZ = 0
  w_CIBTNFSZ = 0
  w_CIGRDFSZ = 0
  w_CIPAGFSZ = 0
  w_CIDSKMEN = space(1)
  w_CINAVSTA = space(1)
  w_CINAVNUB = 0
  w_CINAVDIM = 0
  w_CIMNAFNM = space(50)
  w_CIMNAFSZ = 0
  w_CIWMAFNM = space(50)
  w_CIWMAFSZ = 0
  w_RET = space(1)
  w_CIOBLCOL = 0
  w_COLORCTOB = space(10)
  w_ciobl_on = space(1)
  w_COLORZOM = space(10)
  w_CICOLZOM = 0
  w_ciwaitwd = space(1)
  w_cilblfit = space(1)
  w_citxtfit = space(1)
  w_cicbxfit = space(1)
  w_cibtnfit = space(1)
  w_cigrdfit = space(1)
  w_cipagfit = space(1)
  w_cilblfbo = space(1)
  w_citxtfbo = space(1)
  w_cicbxfbo = space(1)
  w_cibtnfbo = space(1)
  w_cigrdfbo = space(1)
  w_cipagfbo = space(1)
  * --- Area Manuale = Declare Variables
  * --- cp_setgui
  Proc SetDefault
     local l_usercode, l_bLoaded
     With This
       l_usercode = .w_usrcode
       l_bLoaded = .bLoaded
       .BlankRec()
       .w_usrcode = m.l_usercode
       .bUpdated = .t.
       .bLoaded = m.l_bLoaded
     Endwith
  EndProc
  
  Proc SetColor(pVar)
     Local L_ExecCMD, l_Color
     L_ExecCMD= 'this.w_'+pVar
     l_Color = GetColor(&L_ExecCMD)
     If l_Color <> -1
       L_ExecCMD= L_ExecCMD+'= m.l_COLOR'
       &L_ExecCMD
     EndIf
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_setguiPag1","cp_setgui",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Impostazioni")
      .Pages(1).HelpContextID = 266791235
      .Pages(2).addobject("oPag","tcp_setguiPag2","cp_setgui",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Colori")
      .Pages(2).HelpContextID = 133181508
      .Pages(3).addobject("oPag","tcp_setguiPag3","cp_setgui",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Font")
      .Pages(3).HelpContextID = 161361220
      .Pages(4).addobject("oPag","tcp_setguiPag4","cp_setgui",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Desktop men�")
      .Pages(4).HelpContextID = 165343276
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCIVTHEME_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_setgui
    this.Pages(1).Caption=cp_Translate(MSG_SETTINGS_LABEL)
    this.Pages(2).Caption=cp_Translate(MSG_COLOR)
    this.Pages(3).Caption=cp_Translate(MSG_FONT)
    this.Pages(4).Caption=cp_Translate(MSG_DESKTOP_MENU)
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='cpsetgui'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.cpsetgui_IDX,5],7]
    this.nPostItConn=i_TableProp[this.cpsetgui_IDX,3]
  return

  procedure NewContext()
    return(createobject('tscp_setgui'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from cpsetgui where usrcode=KeySet.usrcode
    *
    i_nConn = i_TableProp[this.cpsetgui_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('cpsetgui')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "cpsetgui.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' cpsetgui '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'usrcode',this.w_usrcode  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_COLORCTSEL = space(10)
        .w_COLORCTED = space(10)
        .w_COLORFCTDSBL = space(10)
        .w_COLORBCTDSBL = space(10)
        .w_COLORGRD = space(10)
        .w_COLORSCREEN = space(10)
        .w_COLORROWDTL = space(10)
        .w_RET = space(1)
        .w_COLORCTOB = space(10)
        .w_COLORZOM = space(10)
        .w_usrcode = NVL(usrcode,0)
        .w_CIVTHEME = NVL(CIVTHEME,0)
        .w_CIREPBEH = NVL(CIREPBEH,space(1))
        .w_CICPTBAR = NVL(CICPTBAR,space(1))
        .w_CIDSKBAR = NVL(CIDSKBAR,space(1))
        .w_CISTABAR = NVL(CISTABAR,space(1))
        .w_CISHWBTN = NVL(CISHWBTN,space(1))
        .w_CITBSIZE = NVL(CITBSIZE,0)
        .w_CIMDIFRM = NVL(CIMDIFRM,space(1))
        .w_CITABMEN = NVL(CITABMEN,space(1))
        .w_CIXPTHEM = NVL(CIXPTHEM,0)
        .w_CIMENFIX = NVL(CIMENFIX,space(1))
        .w_CISEARMN = NVL(CISEARMN,space(1))
        .w_CITOOLMN = NVL(CITOOLMN,space(1))
        .w_CIBCKGRD = NVL(CIBCKGRD,space(1))
        .w_CIMRKGRD = NVL(CIMRKGRD,space(1))
        .w_CISELECL = NVL(CISELECL,0)
        .w_CIEDTCOL = NVL(CIEDTCOL,0)
        .w_CIGRIDCL = NVL(CIGRIDCL,0)
        .w_CIDTLCLR = NVL(CIDTLCLR,0)
        .w_CIDSBLBC = NVL(CIDSBLBC,0)
        .w_CIDSBLFC = NVL(CIDSBLFC,0)
        .w_CISCRCOL = NVL(CISCRCOL,0)
        .w_CIEVIZOM = NVL(CIEVIZOM,0)
        .oPgFrm.Page2.oPag.oObj_2_19.Calculate(.w_CIEDTCOL)
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate(.w_CISELECL)
        .oPgFrm.Page2.oPag.oObj_2_24.Calculate(.w_CIDSBLFC)
        .oPgFrm.Page2.oPag.oObj_2_25.Calculate(.w_CIDSBLBC)
        .oPgFrm.Page2.oPag.oObj_2_43.Calculate(.w_CISCRCOL)
        .oPgFrm.Page2.oPag.oObj_2_44.Calculate(.w_CIDTLCLR)
        .oPgFrm.Page2.oPag.oObj_2_45.Calculate(.w_CIGRIDCL)
        .w_CILBLFNM = NVL(CILBLFNM,space(50))
        .w_CITXTFNM = NVL(CITXTFNM,space(50))
        .w_CICBXFNM = NVL(CICBXFNM,space(50))
        .w_CIBTNFNM = NVL(CIBTNFNM,space(50))
        .w_CIGRDFNM = NVL(CIGRDFNM,space(50))
        .w_CIPAGFNM = NVL(CIPAGFNM,space(50))
        .w_CILBLFSZ = NVL(CILBLFSZ,0)
        .w_CITXTFSZ = NVL(CITXTFSZ,0)
        .w_CICBXFSZ = NVL(CICBXFSZ,0)
        .w_CIBTNFSZ = NVL(CIBTNFSZ,0)
        .w_CIGRDFSZ = NVL(CIGRDFSZ,0)
        .w_CIPAGFSZ = NVL(CIPAGFSZ,0)
        .w_CIDSKMEN = NVL(CIDSKMEN,space(1))
        .w_CINAVSTA = NVL(CINAVSTA,space(1))
        .w_CINAVNUB = NVL(CINAVNUB,0)
        .w_CINAVDIM = NVL(CINAVDIM,0)
        .w_CIMNAFNM = NVL(CIMNAFNM,space(50))
        .w_CIMNAFSZ = NVL(CIMNAFSZ,0)
        .w_CIWMAFNM = NVL(CIWMAFNM,space(50))
        .w_CIWMAFSZ = NVL(CIWMAFSZ,0)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_THEME_APPLIC_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_THEME)
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_VIS_TOOL_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_VIS_APP_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_VIS_STATUS_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_VIS_CONT_BUTTON_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_DIM_IMM_TOOLBAR)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_ICON_TOOLBAR)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_VIS_MENUBAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_VIS_CTRL_FIND_MENU_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_VIS_TOOLMENU_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate(MSG_VIS_GRAD_BACKGOUND_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate(MSG_VIS_POINT_REC_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(MSG_VIS_MODE)
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(MSG_TOOLBAR_STATUSBAR)
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(MSG_FORM_CONTROL)
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(MSG_TAB)
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate(MSG_THEME_XP)
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate(MSG_MENU)
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate(MSG_VIS_MASK_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(MSG_VIS_TOOL_BAR)
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(MSG_VIS_APP_BAR)
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(MSG_VIS_STATUS_BAR)
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate(MSG_VIS_CONT_BUTTON)
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate(MSG_VIS_MENUBAR)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(MSG_VIS_CTRL_FIND_MENU)
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(MSG_VIS_TOOLMENU)
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(MSG_VIS_GRAD_BACKGOUND)
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(MSG_VIS_POINT_REC)
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(MSG_THEME_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(MSG_VIS_MASK_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(MSG_THEME_XP_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate(MSG_THEME_XP_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(MSG_TAB_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate(MSG_TAB_VALUE)
        .oPgFrm.Page2.oPag.oObj_2_48.Calculate(MSG_ENABLED)
        .oPgFrm.Page2.oPag.oObj_2_49.Calculate(MSG_DISABLED)
        .oPgFrm.Page2.oPag.oObj_2_50.Calculate(MSG_LISTS_ZOOM)
        .oPgFrm.Page2.oPag.oObj_2_51.Calculate(MSG_DESKTOP)
        .oPgFrm.Page2.oPag.oObj_2_52.Calculate(MSG_SELECTED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_53.Calculate(MSG_EDITABLE_LABEL+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate(MSG_BACKGROUND+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_55.Calculate(MSG_CHARACTER+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_56.Calculate(MSG_GREED_LINES+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_57.Calculate(MSG_LINE_SELECTED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_58.Calculate(MSG_GREED_DATA)
        .oPgFrm.Page2.oPag.oObj_2_59.Calculate(MSG_BACKGROUND_DESKTOP+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_60.Calculate(MSG_SELECTED_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_61.Calculate(MSG_EDITABLE_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_62.Calculate(MSG_GREED_LINES_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_63.Calculate(MSG_BACKGROUND_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate(MSG_CHARACTER_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_65.Calculate(MSG_BACKGROUND_DESKTOP_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_66.Calculate(MSG_LINE_SELECTED_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_20.Calculate(MSG_FONT_LABELS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_21.Calculate(MSG_FONT_FIELDS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_22.Calculate(MSG_FONT_COMBOBOX+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_23.Calculate(MSG_FONT_BUTTONS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_24.Calculate(MSG_FONT_ZOOM+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_25.Calculate(MSG_FONT_TAB+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_26.Calculate(MSG_DIMENSION)
        .oPgFrm.Page3.oPag.oObj_3_27.Calculate(MSG_FONT_LABELS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_28.Calculate(MSG_FONT_FIELDS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_29.Calculate(MSG_FONT_BUTTONS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_30.Calculate(MSG_FONT_ZOOM_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_31.Calculate(MSG_FONT_TAB_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_32.Calculate(MSG_FONT_COMBOBOX_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_33.Calculate(MSG_DIMENSION_LABELS)
        .oPgFrm.Page3.oPag.oObj_3_34.Calculate(MSG_DIMENSION_FIELDS)
        .oPgFrm.Page3.oPag.oObj_3_35.Calculate(MSG_DIMENSION_BUTTONS)
        .oPgFrm.Page3.oPag.oObj_3_36.Calculate(MSG_DIMENSION_ZOOM)
        .oPgFrm.Page3.oPag.oObj_3_37.Calculate(MSG_DIMENSION_TAB)
        .oPgFrm.Page3.oPag.oObj_3_38.Calculate(MSG_DIMENSION_COMBOBOX)
        .oPgFrm.Page4.oPag.oObj_4_17.Calculate(MSG_DESKTOP_MENU+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_18.Calculate(MSG_DESKTOP_MENU_STATUS)
        .oPgFrm.Page4.oPag.oObj_4_19.Calculate(MSG_NUMBER_BUTTON_VIS)
        .oPgFrm.Page4.oPag.oObj_4_20.Calculate(MSG_INITIAL_DIMENSION_MENU )
        .oPgFrm.Page4.oPag.oObj_4_21.Calculate(MSG_FONT_MENU_NAV)
        .oPgFrm.Page4.oPag.oObj_4_22.Calculate(MSG_DIMENSION+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_23.Calculate(MSG_DIMENSION+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_24.Calculate(MSG_FONT_WIN_MAN)
        .oPgFrm.Page4.oPag.oObj_4_25.Calculate(MSG_DESKTOP_MENU_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_26.Calculate(MSG_DESKTOP_MENU_STATUS_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_27.Calculate(MSG_NUMBER_BUTTON_VIS_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_28.Calculate(MSG_INITIAL_DIMENSION_MENU_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_29.Calculate(MSG_FONT_MENU_NAV_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_30.Calculate(MSG_FONT_WIN_MAN_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_31.Calculate(MSG_FONT_MENU_NAV_DIM_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_32.Calculate(MSG_FONT_WIN_MAN_DIM_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_33.Calculate(MSG_DESKTOP_MENU_VALUE)
        .oPgFrm.Page4.oPag.oObj_4_34.Calculate(MSG_DESKTOP_MENU_STATUS_VALUE)
        .w_CIOBLCOL = NVL(CIOBLCOL,0)
        .oPgFrm.Page2.oPag.oObj_2_69.Calculate(.w_CIOBLCOL)
        .oPgFrm.Page2.oPag.oObj_2_72.Calculate(MSG_HIGHLIGHT_REQUIRED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_73.Calculate(MSG_REQUIRED_TOOLTIPTEXT)
        .w_ciobl_on = NVL(ciobl_on,space(1))
        .w_CICOLZOM = NVL(CICOLZOM,0)
        .oPgFrm.Page2.oPag.oObj_2_78.Calculate(.w_CICOLZOM)
        .oPgFrm.Page2.oPag.oObj_2_79.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_80.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM)
        .oPgFrm.Page2.oPag.oObj_2_81.Calculate(MSG_SELECTED_ROW_COLOUR+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_82.Calculate(MSG_SELECTED_ROW_COLOUR_TOOLTIP)
        .w_ciwaitwd = NVL(ciwaitwd,space(1))
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate(MSG_VIS_WAITWND_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(MSG_VIS_WAITWND)
        .w_cilblfit = NVL(cilblfit,space(1))
        .w_citxtfit = NVL(citxtfit,space(1))
        .w_cicbxfit = NVL(cicbxfit,space(1))
        .w_cibtnfit = NVL(cibtnfit,space(1))
        .w_cigrdfit = NVL(cigrdfit,space(1))
        .w_cipagfit = NVL(cipagfit,space(1))
        .w_cilblfbo = NVL(cilblfbo,space(1))
        .w_citxtfbo = NVL(citxtfbo,space(1))
        .w_cicbxfbo = NVL(cicbxfbo,space(1))
        .w_cibtnfbo = NVL(cibtnfbo,space(1))
        .w_cigrdfbo = NVL(cigrdfbo,space(1))
        .w_cipagfbo = NVL(cipagfbo,space(1))
        cp_LoadRecExtFlds(this,'cpsetgui')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_usrcode = 0
      .w_CIVTHEME = 0
      .w_CIREPBEH = space(1)
      .w_CICPTBAR = space(1)
      .w_CIDSKBAR = space(1)
      .w_CISTABAR = space(1)
      .w_CISHWBTN = space(1)
      .w_CITBSIZE = 0
      .w_CIMDIFRM = space(1)
      .w_CITABMEN = space(1)
      .w_CIXPTHEM = 0
      .w_CIMENFIX = space(1)
      .w_CISEARMN = space(1)
      .w_CITOOLMN = space(1)
      .w_CIBCKGRD = space(1)
      .w_CIMRKGRD = space(1)
      .w_CISELECL = 0
      .w_CIEDTCOL = 0
      .w_CIGRIDCL = 0
      .w_CIDTLCLR = 0
      .w_CIDSBLBC = 0
      .w_CIDSBLFC = 0
      .w_CISCRCOL = 0
      .w_COLORCTSEL = space(10)
      .w_CIEVIZOM = 0
      .w_COLORCTED = space(10)
      .w_COLORFCTDSBL = space(10)
      .w_COLORBCTDSBL = space(10)
      .w_COLORGRD = space(10)
      .w_COLORSCREEN = space(10)
      .w_COLORROWDTL = space(10)
      .w_CILBLFNM = space(50)
      .w_CITXTFNM = space(50)
      .w_CICBXFNM = space(50)
      .w_CIBTNFNM = space(50)
      .w_CIGRDFNM = space(50)
      .w_CIPAGFNM = space(50)
      .w_CILBLFSZ = 0
      .w_CITXTFSZ = 0
      .w_CICBXFSZ = 0
      .w_CIBTNFSZ = 0
      .w_CIGRDFSZ = 0
      .w_CIPAGFSZ = 0
      .w_CIDSKMEN = space(1)
      .w_CINAVSTA = space(1)
      .w_CINAVNUB = 0
      .w_CINAVDIM = 0
      .w_CIMNAFNM = space(50)
      .w_CIMNAFSZ = 0
      .w_CIWMAFNM = space(50)
      .w_CIWMAFSZ = 0
      .w_RET = space(1)
      .w_CIOBLCOL = 0
      .w_COLORCTOB = space(10)
      .w_ciobl_on = space(1)
      .w_COLORZOM = space(10)
      .w_CICOLZOM = 0
      .w_ciwaitwd = space(1)
      .w_cilblfit = space(1)
      .w_citxtfit = space(1)
      .w_cicbxfit = space(1)
      .w_cibtnfit = space(1)
      .w_cigrdfit = space(1)
      .w_cipagfit = space(1)
      .w_cilblfbo = space(1)
      .w_citxtfbo = space(1)
      .w_cicbxfbo = space(1)
      .w_cibtnfbo = space(1)
      .w_cigrdfbo = space(1)
      .w_cipagfbo = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_CIVTHEME = -1
        .w_CIREPBEH = '8'
        .w_CICPTBAR = 'S'
        .w_CIDSKBAR = 'S'
        .w_CISTABAR = ' '
        .w_CISHWBTN = ' '
        .w_CITBSIZE = 24
        .w_CIMDIFRM = 'S'
        .w_CITABMEN = 'T'
        .w_CIXPTHEM = 1
        .w_CIMENFIX = 'S'
        .w_CISEARMN = 'S'
        .w_CITOOLMN = 'S'
        .w_CIBCKGRD = 'S'
        .w_CIMRKGRD = 'N'
        .w_CISELECL = RGB(255,231,162)
        .w_CIEDTCOL = RGB(246,246,246)
        .w_CIGRIDCL = RGB(208, 215, 229)
        .w_CIDTLCLR = .w_CISELECL
        .w_CIDSBLBC = Rgb(233,238,238)
        .w_CIDSBLFC = Rgb(0,41,91)
        .w_CISCRCOL = Rgb(255,255,255)
          .DoRTCalc(24,24,.f.)
        .w_CIEVIZOM = 0
        .oPgFrm.Page2.oPag.oObj_2_19.Calculate(.w_CIEDTCOL)
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate(.w_CISELECL)
        .oPgFrm.Page2.oPag.oObj_2_24.Calculate(.w_CIDSBLFC)
        .oPgFrm.Page2.oPag.oObj_2_25.Calculate(.w_CIDSBLBC)
        .oPgFrm.Page2.oPag.oObj_2_43.Calculate(.w_CISCRCOL)
        .oPgFrm.Page2.oPag.oObj_2_44.Calculate(.w_CIDTLCLR)
        .oPgFrm.Page2.oPag.oObj_2_45.Calculate(.w_CIGRIDCL)
          .DoRTCalc(26,31,.f.)
        .w_CILBLFNM = i_cProjectFontName
        .w_CITXTFNM = i_cProjectFontName
        .w_CICBXFNM = i_cProjectFontName
        .w_CIBTNFNM = i_cProjectFontName
        .w_CIGRDFNM = i_cProjectFontName
        .w_CIPAGFNM = i_cProjectFontName
        .w_CILBLFSZ = i_nProjectFontSize
        .w_CITXTFSZ = i_nProjectFontSize
        .w_CICBXFSZ = i_nProjectFontSize
        .w_CIBTNFSZ = i_nProjectFontSize
        .w_CIGRDFSZ = i_nProjectFontSize
        .w_CIPAGFSZ = i_nProjectFontSize
        .w_CIDSKMEN = 'S'
        .w_CINAVSTA = 'A'
        .w_CINAVNUB = 7
        .w_CINAVDIM = 179
        .w_CIMNAFNM = i_cProjectFontName
        .w_CIMNAFSZ = i_nProjectFontSize
        .w_CIWMAFNM = i_cProjectFontName
        .w_CIWMAFSZ = i_nProjectFontSize
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_THEME_APPLIC_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_THEME)
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_VIS_TOOL_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_VIS_APP_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_VIS_STATUS_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_VIS_CONT_BUTTON_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_DIM_IMM_TOOLBAR)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_ICON_TOOLBAR)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_VIS_MENUBAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_VIS_CTRL_FIND_MENU_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_VIS_TOOLMENU_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate(MSG_VIS_GRAD_BACKGOUND_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate(MSG_VIS_POINT_REC_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(MSG_VIS_MODE)
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(MSG_TOOLBAR_STATUSBAR)
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(MSG_FORM_CONTROL)
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(MSG_TAB)
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate(MSG_THEME_XP)
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate(MSG_MENU)
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate(MSG_VIS_MASK_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(MSG_VIS_TOOL_BAR)
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(MSG_VIS_APP_BAR)
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(MSG_VIS_STATUS_BAR)
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate(MSG_VIS_CONT_BUTTON)
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate(MSG_VIS_MENUBAR)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(MSG_VIS_CTRL_FIND_MENU)
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(MSG_VIS_TOOLMENU)
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(MSG_VIS_GRAD_BACKGOUND)
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(MSG_VIS_POINT_REC)
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(MSG_THEME_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(MSG_VIS_MASK_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(MSG_THEME_XP_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate(MSG_THEME_XP_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(MSG_TAB_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate(MSG_TAB_VALUE)
        .oPgFrm.Page2.oPag.oObj_2_48.Calculate(MSG_ENABLED)
        .oPgFrm.Page2.oPag.oObj_2_49.Calculate(MSG_DISABLED)
        .oPgFrm.Page2.oPag.oObj_2_50.Calculate(MSG_LISTS_ZOOM)
        .oPgFrm.Page2.oPag.oObj_2_51.Calculate(MSG_DESKTOP)
        .oPgFrm.Page2.oPag.oObj_2_52.Calculate(MSG_SELECTED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_53.Calculate(MSG_EDITABLE_LABEL+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate(MSG_BACKGROUND+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_55.Calculate(MSG_CHARACTER+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_56.Calculate(MSG_GREED_LINES+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_57.Calculate(MSG_LINE_SELECTED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_58.Calculate(MSG_GREED_DATA)
        .oPgFrm.Page2.oPag.oObj_2_59.Calculate(MSG_BACKGROUND_DESKTOP+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_60.Calculate(MSG_SELECTED_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_61.Calculate(MSG_EDITABLE_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_62.Calculate(MSG_GREED_LINES_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_63.Calculate(MSG_BACKGROUND_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate(MSG_CHARACTER_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_65.Calculate(MSG_BACKGROUND_DESKTOP_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_66.Calculate(MSG_LINE_SELECTED_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_20.Calculate(MSG_FONT_LABELS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_21.Calculate(MSG_FONT_FIELDS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_22.Calculate(MSG_FONT_COMBOBOX+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_23.Calculate(MSG_FONT_BUTTONS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_24.Calculate(MSG_FONT_ZOOM+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_25.Calculate(MSG_FONT_TAB+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_26.Calculate(MSG_DIMENSION)
        .oPgFrm.Page3.oPag.oObj_3_27.Calculate(MSG_FONT_LABELS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_28.Calculate(MSG_FONT_FIELDS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_29.Calculate(MSG_FONT_BUTTONS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_30.Calculate(MSG_FONT_ZOOM_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_31.Calculate(MSG_FONT_TAB_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_32.Calculate(MSG_FONT_COMBOBOX_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_33.Calculate(MSG_DIMENSION_LABELS)
        .oPgFrm.Page3.oPag.oObj_3_34.Calculate(MSG_DIMENSION_FIELDS)
        .oPgFrm.Page3.oPag.oObj_3_35.Calculate(MSG_DIMENSION_BUTTONS)
        .oPgFrm.Page3.oPag.oObj_3_36.Calculate(MSG_DIMENSION_ZOOM)
        .oPgFrm.Page3.oPag.oObj_3_37.Calculate(MSG_DIMENSION_TAB)
        .oPgFrm.Page3.oPag.oObj_3_38.Calculate(MSG_DIMENSION_COMBOBOX)
        .oPgFrm.Page4.oPag.oObj_4_17.Calculate(MSG_DESKTOP_MENU+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_18.Calculate(MSG_DESKTOP_MENU_STATUS)
        .oPgFrm.Page4.oPag.oObj_4_19.Calculate(MSG_NUMBER_BUTTON_VIS)
        .oPgFrm.Page4.oPag.oObj_4_20.Calculate(MSG_INITIAL_DIMENSION_MENU )
        .oPgFrm.Page4.oPag.oObj_4_21.Calculate(MSG_FONT_MENU_NAV)
        .oPgFrm.Page4.oPag.oObj_4_22.Calculate(MSG_DIMENSION+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_23.Calculate(MSG_DIMENSION+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_24.Calculate(MSG_FONT_WIN_MAN)
        .oPgFrm.Page4.oPag.oObj_4_25.Calculate(MSG_DESKTOP_MENU_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_26.Calculate(MSG_DESKTOP_MENU_STATUS_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_27.Calculate(MSG_NUMBER_BUTTON_VIS_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_28.Calculate(MSG_INITIAL_DIMENSION_MENU_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_29.Calculate(MSG_FONT_MENU_NAV_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_30.Calculate(MSG_FONT_WIN_MAN_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_31.Calculate(MSG_FONT_MENU_NAV_DIM_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_32.Calculate(MSG_FONT_WIN_MAN_DIM_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_33.Calculate(MSG_DESKTOP_MENU_VALUE)
        .oPgFrm.Page4.oPag.oObj_4_34.Calculate(MSG_DESKTOP_MENU_STATUS_VALUE)
        .oPgFrm.Page2.oPag.oObj_2_69.Calculate(.w_CIOBLCOL)
        .oPgFrm.Page2.oPag.oObj_2_72.Calculate(MSG_HIGHLIGHT_REQUIRED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_73.Calculate(MSG_REQUIRED_TOOLTIPTEXT)
          .DoRTCalc(52,54,.f.)
        .w_ciobl_on = 'N'
          .DoRTCalc(56,56,.f.)
        .w_CICOLZOM = RGB(255,231,162)
        .oPgFrm.Page2.oPag.oObj_2_78.Calculate(.w_CICOLZOM)
        .oPgFrm.Page2.oPag.oObj_2_79.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_80.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM)
        .oPgFrm.Page2.oPag.oObj_2_81.Calculate(MSG_SELECTED_ROW_COLOUR+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_82.Calculate(MSG_SELECTED_ROW_COLOUR_TOOLTIP)
        .w_ciwaitwd = 'N'
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate(MSG_VIS_WAITWND_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(MSG_VIS_WAITWND)
        .w_cilblfit = 'N'
        .w_citxtfit = 'N'
        .w_cicbxfit = 'N'
        .w_cibtnfit = 'N'
        .w_cigrdfit = 'N'
        .w_cipagfit = 'N'
        .w_cilblfbo = 'N'
        .w_citxtfbo = 'N'
        .w_cicbxfbo = 'N'
        .w_cibtnfbo = 'N'
        .w_cigrdfbo = 'N'
        .w_cipagfbo = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'cpsetgui')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCIVTHEME_1_2.enabled = i_bVal
      .Page1.oPag.oCIREPBEH_1_3.enabled = i_bVal
      .Page1.oPag.oCICPTBAR_1_4.enabled = i_bVal
      .Page1.oPag.oCIDSKBAR_1_5.enabled = i_bVal
      .Page1.oPag.oCISTABAR_1_6.enabled = i_bVal
      .Page1.oPag.oCISHWBTN_1_7.enabled = i_bVal
      .Page1.oPag.oCITBSIZE_1_8.enabled = i_bVal
      .Page1.oPag.oCIMDIFRM_1_9.enabled = i_bVal
      .Page1.oPag.oCITABMEN_1_10.enabled = i_bVal
      .Page1.oPag.oCIXPTHEM_1_11.enabled = i_bVal
      .Page1.oPag.oCIMENFIX_1_12.enabled = i_bVal
      .Page1.oPag.oCISEARMN_1_13.enabled = i_bVal
      .Page1.oPag.oCITOOLMN_1_14.enabled = i_bVal
      .Page1.oPag.oCIBCKGRD_1_15.enabled = i_bVal
      .Page1.oPag.oCIMRKGRD_1_16.enabled = i_bVal
      .Page2.oPag.oCIEVIZOM_2_16.enabled = i_bVal
      .Page3.oPag.oCILBLFNM_3_1.enabled = i_bVal
      .Page3.oPag.oCITXTFNM_3_2.enabled = i_bVal
      .Page3.oPag.oCICBXFNM_3_3.enabled = i_bVal
      .Page3.oPag.oCIBTNFNM_3_4.enabled = i_bVal
      .Page3.oPag.oCIGRDFNM_3_5.enabled = i_bVal
      .Page3.oPag.oCIPAGFNM_3_6.enabled = i_bVal
      .Page3.oPag.oCILBLFSZ_3_13.enabled = i_bVal
      .Page3.oPag.oCITXTFSZ_3_15.enabled = i_bVal
      .Page3.oPag.oCICBXFSZ_3_16.enabled = i_bVal
      .Page3.oPag.oCIBTNFSZ_3_17.enabled = i_bVal
      .Page3.oPag.oCIGRDFSZ_3_18.enabled = i_bVal
      .Page3.oPag.oCIPAGFSZ_3_19.enabled = i_bVal
      .Page4.oPag.oCIDSKMEN_4_1.enabled = i_bVal
      .Page4.oPag.oCINAVSTA_4_2.enabled = i_bVal
      .Page4.oPag.oCINAVNUB_4_3.enabled = i_bVal
      .Page4.oPag.oCINAVDIM_4_4.enabled = i_bVal
      .Page4.oPag.oCIMNAFNM_4_5.enabled = i_bVal
      .Page4.oPag.oCIMNAFSZ_4_6.enabled = i_bVal
      .Page4.oPag.oCIWMAFNM_4_7.enabled = i_bVal
      .Page4.oPag.oCIWMAFSZ_4_8.enabled = i_bVal
      .Page2.oPag.ociobl_on_2_74.enabled = i_bVal
      .Page1.oPag.ociwaitwd_1_70.enabled = i_bVal
      .Page3.oPag.ocilblfit_3_39.enabled = i_bVal
      .Page3.oPag.ocitxtfit_3_40.enabled = i_bVal
      .Page3.oPag.ocicbxfit_3_41.enabled = i_bVal
      .Page3.oPag.ocibtnfit_3_42.enabled = i_bVal
      .Page3.oPag.ocigrdfit_3_43.enabled = i_bVal
      .Page3.oPag.ocipagfit_3_44.enabled = i_bVal
      .Page3.oPag.ocilblfbo_3_45.enabled = i_bVal
      .Page3.oPag.ocitxtfbo_3_46.enabled = i_bVal
      .Page3.oPag.ocicbxfbo_3_47.enabled = i_bVal
      .Page3.oPag.ocibtnfbo_3_48.enabled = i_bVal
      .Page3.oPag.ocigrdfbo_3_49.enabled = i_bVal
      .Page3.oPag.ocipagfbo_3_50.enabled = i_bVal
      .Page2.oPag.oBtn_2_9.enabled = i_bVal
      .Page2.oPag.oBtn_2_10.enabled = i_bVal
      .Page2.oPag.oBtn_2_11.enabled = i_bVal
      .Page2.oPag.oBtn_2_12.enabled = i_bVal
      .Page2.oPag.oBtn_2_13.enabled = i_bVal
      .Page2.oPag.oBtn_2_14.enabled = i_bVal
      .Page2.oPag.oBtn_2_15.enabled = i_bVal
      .Page2.oPag.oBtn_2_17.enabled = i_bVal
      .Page2.oPag.oBtn_2_68.enabled = i_bVal
      .Page2.oPag.oObj_2_19.enabled = i_bVal
      .Page2.oPag.oObj_2_20.enabled = i_bVal
      .Page2.oPag.oObj_2_24.enabled = i_bVal
      .Page2.oPag.oObj_2_25.enabled = i_bVal
      .Page2.oPag.oObj_2_43.enabled = i_bVal
      .Page2.oPag.oObj_2_44.enabled = i_bVal
      .Page2.oPag.oObj_2_45.enabled = i_bVal
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_32.enabled = i_bVal
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oObj_1_35.enabled = i_bVal
      .Page1.oPag.oObj_1_36.enabled = i_bVal
      .Page1.oPag.oObj_1_37.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oObj_1_42.enabled = i_bVal
      .Page1.oPag.oObj_1_43.enabled = i_bVal
      .Page1.oPag.oObj_1_44.enabled = i_bVal
      .Page1.oPag.oObj_1_45.enabled = i_bVal
      .Page1.oPag.oObj_1_46.enabled = i_bVal
      .Page1.oPag.oObj_1_47.enabled = i_bVal
      .Page1.oPag.oObj_1_48.enabled = i_bVal
      .Page1.oPag.oObj_1_49.enabled = i_bVal
      .Page1.oPag.oObj_1_50.enabled = i_bVal
      .Page1.oPag.oObj_1_51.enabled = i_bVal
      .Page1.oPag.oObj_1_52.enabled = i_bVal
      .Page1.oPag.oObj_1_53.enabled = i_bVal
      .Page1.oPag.oObj_1_54.enabled = i_bVal
      .Page1.oPag.oObj_1_55.enabled = i_bVal
      .Page1.oPag.oObj_1_56.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.oObj_1_58.enabled = i_bVal
      .Page1.oPag.oObj_1_59.enabled = i_bVal
      .Page1.oPag.oObj_1_60.enabled = i_bVal
      .Page1.oPag.oObj_1_61.enabled = i_bVal
      .Page1.oPag.oObj_1_62.enabled = i_bVal
      .Page1.oPag.oObj_1_63.enabled = i_bVal
      .Page1.oPag.oObj_1_64.enabled = i_bVal
      .Page2.oPag.oObj_2_48.enabled = i_bVal
      .Page2.oPag.oObj_2_49.enabled = i_bVal
      .Page2.oPag.oObj_2_50.enabled = i_bVal
      .Page2.oPag.oObj_2_51.enabled = i_bVal
      .Page2.oPag.oObj_2_52.enabled = i_bVal
      .Page2.oPag.oObj_2_53.enabled = i_bVal
      .Page2.oPag.oObj_2_54.enabled = i_bVal
      .Page2.oPag.oObj_2_55.enabled = i_bVal
      .Page2.oPag.oObj_2_56.enabled = i_bVal
      .Page2.oPag.oObj_2_57.enabled = i_bVal
      .Page2.oPag.oObj_2_58.enabled = i_bVal
      .Page2.oPag.oObj_2_59.enabled = i_bVal
      .Page2.oPag.oObj_2_60.enabled = i_bVal
      .Page2.oPag.oObj_2_61.enabled = i_bVal
      .Page2.oPag.oObj_2_62.enabled = i_bVal
      .Page2.oPag.oObj_2_63.enabled = i_bVal
      .Page2.oPag.oObj_2_64.enabled = i_bVal
      .Page2.oPag.oObj_2_65.enabled = i_bVal
      .Page2.oPag.oObj_2_66.enabled = i_bVal
      .Page3.oPag.oObj_3_20.enabled = i_bVal
      .Page3.oPag.oObj_3_21.enabled = i_bVal
      .Page3.oPag.oObj_3_22.enabled = i_bVal
      .Page3.oPag.oObj_3_23.enabled = i_bVal
      .Page3.oPag.oObj_3_24.enabled = i_bVal
      .Page3.oPag.oObj_3_25.enabled = i_bVal
      .Page3.oPag.oObj_3_26.enabled = i_bVal
      .Page3.oPag.oObj_3_27.enabled = i_bVal
      .Page3.oPag.oObj_3_28.enabled = i_bVal
      .Page3.oPag.oObj_3_29.enabled = i_bVal
      .Page3.oPag.oObj_3_30.enabled = i_bVal
      .Page3.oPag.oObj_3_31.enabled = i_bVal
      .Page3.oPag.oObj_3_32.enabled = i_bVal
      .Page3.oPag.oObj_3_33.enabled = i_bVal
      .Page3.oPag.oObj_3_34.enabled = i_bVal
      .Page3.oPag.oObj_3_35.enabled = i_bVal
      .Page3.oPag.oObj_3_36.enabled = i_bVal
      .Page3.oPag.oObj_3_37.enabled = i_bVal
      .Page3.oPag.oObj_3_38.enabled = i_bVal
      .Page4.oPag.oObj_4_17.enabled = i_bVal
      .Page4.oPag.oObj_4_18.enabled = i_bVal
      .Page4.oPag.oObj_4_19.enabled = i_bVal
      .Page4.oPag.oObj_4_20.enabled = i_bVal
      .Page4.oPag.oObj_4_21.enabled = i_bVal
      .Page4.oPag.oObj_4_22.enabled = i_bVal
      .Page4.oPag.oObj_4_23.enabled = i_bVal
      .Page4.oPag.oObj_4_24.enabled = i_bVal
      .Page4.oPag.oObj_4_25.enabled = i_bVal
      .Page4.oPag.oObj_4_26.enabled = i_bVal
      .Page4.oPag.oObj_4_27.enabled = i_bVal
      .Page4.oPag.oObj_4_28.enabled = i_bVal
      .Page4.oPag.oObj_4_29.enabled = i_bVal
      .Page4.oPag.oObj_4_30.enabled = i_bVal
      .Page4.oPag.oObj_4_31.enabled = i_bVal
      .Page4.oPag.oObj_4_32.enabled = i_bVal
      .Page4.oPag.oObj_4_33.enabled = i_bVal
      .Page4.oPag.oObj_4_34.enabled = i_bVal
      .Page2.oPag.oObj_2_69.enabled = i_bVal
      .Page2.oPag.oObj_2_72.enabled = i_bVal
      .Page2.oPag.oObj_2_73.enabled = i_bVal
      .Page2.oPag.oObj_2_78.enabled = i_bVal
      .Page2.oPag.oObj_2_79.enabled = i_bVal
      .Page2.oPag.oObj_2_80.enabled = i_bVal
      .Page2.oPag.oObj_2_81.enabled = i_bVal
      .Page2.oPag.oObj_2_82.enabled = i_bVal
      .Page1.oPag.oObj_1_71.enabled = i_bVal
      .Page1.oPag.oObj_1_72.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'cpsetgui',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.cpsetgui_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_usrcode,"usrcode",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIVTHEME,"CIVTHEME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIREPBEH,"CIREPBEH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICPTBAR,"CICPTBAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSKBAR,"CIDSKBAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISTABAR,"CISTABAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISHWBTN,"CISHWBTN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITBSIZE,"CITBSIZE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMDIFRM,"CIMDIFRM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITABMEN,"CITABMEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIXPTHEM,"CIXPTHEM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMENFIX,"CIMENFIX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISEARMN,"CISEARMN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITOOLMN,"CITOOLMN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIBCKGRD,"CIBCKGRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMRKGRD,"CIMRKGRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISELECL,"CISELECL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIEDTCOL,"CIEDTCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGRIDCL,"CIGRIDCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDTLCLR,"CIDTLCLR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSBLBC,"CIDSBLBC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSBLFC,"CIDSBLFC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISCRCOL,"CISCRCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIEVIZOM,"CIEVIZOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CILBLFNM,"CILBLFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITXTFNM,"CITXTFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICBXFNM,"CICBXFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIBTNFNM,"CIBTNFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGRDFNM,"CIGRDFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAGFNM,"CIPAGFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CILBLFSZ,"CILBLFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITXTFSZ,"CITXTFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICBXFSZ,"CICBXFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIBTNFSZ,"CIBTNFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGRDFSZ,"CIGRDFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAGFSZ,"CIPAGFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSKMEN,"CIDSKMEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINAVSTA,"CINAVSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINAVNUB,"CINAVNUB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINAVDIM,"CINAVDIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMNAFNM,"CIMNAFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMNAFSZ,"CIMNAFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIWMAFNM,"CIWMAFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIWMAFSZ,"CIWMAFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIOBLCOL,"CIOBLCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ciobl_on,"ciobl_on",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICOLZOM,"CICOLZOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ciwaitwd,"ciwaitwd",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cilblfit,"cilblfit",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_citxtfit,"citxtfit",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cicbxfit,"cicbxfit",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cibtnfit,"cibtnfit",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cigrdfit,"cigrdfit",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cipagfit,"cipagfit",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cilblfbo,"cilblfbo",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_citxtfbo,"citxtfbo",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cicbxfbo,"cicbxfbo",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cibtnfbo,"cibtnfbo",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cigrdfbo,"cigrdfbo",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_cipagfbo,"cipagfbo",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.cpsetgui_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.cpsetgui_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into cpsetgui
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'cpsetgui')
        i_extval=cp_InsertValODBCExtFlds(this,'cpsetgui')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(usrcode,CIVTHEME,CIREPBEH,CICPTBAR,CIDSKBAR"+;
                  ",CISTABAR,CISHWBTN,CITBSIZE,CIMDIFRM,CITABMEN"+;
                  ",CIXPTHEM,CIMENFIX,CISEARMN,CITOOLMN,CIBCKGRD"+;
                  ",CIMRKGRD,CISELECL,CIEDTCOL,CIGRIDCL,CIDTLCLR"+;
                  ",CIDSBLBC,CIDSBLFC,CISCRCOL,CIEVIZOM,CILBLFNM"+;
                  ",CITXTFNM,CICBXFNM,CIBTNFNM,CIGRDFNM,CIPAGFNM"+;
                  ",CILBLFSZ,CITXTFSZ,CICBXFSZ,CIBTNFSZ,CIGRDFSZ"+;
                  ",CIPAGFSZ,CIDSKMEN,CINAVSTA,CINAVNUB,CINAVDIM"+;
                  ",CIMNAFNM,CIMNAFSZ,CIWMAFNM,CIWMAFSZ,CIOBLCOL"+;
                  ",ciobl_on,CICOLZOM,ciwaitwd,cilblfit,citxtfit"+;
                  ",cicbxfit,cibtnfit,cigrdfit,cipagfit,cilblfbo"+;
                  ",citxtfbo,cicbxfbo,cibtnfbo,cigrdfbo,cipagfbo "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_usrcode)+;
                  ","+cp_ToStrODBC(this.w_CIVTHEME)+;
                  ","+cp_ToStrODBC(this.w_CIREPBEH)+;
                  ","+cp_ToStrODBC(this.w_CICPTBAR)+;
                  ","+cp_ToStrODBC(this.w_CIDSKBAR)+;
                  ","+cp_ToStrODBC(this.w_CISTABAR)+;
                  ","+cp_ToStrODBC(this.w_CISHWBTN)+;
                  ","+cp_ToStrODBC(this.w_CITBSIZE)+;
                  ","+cp_ToStrODBC(this.w_CIMDIFRM)+;
                  ","+cp_ToStrODBC(this.w_CITABMEN)+;
                  ","+cp_ToStrODBC(this.w_CIXPTHEM)+;
                  ","+cp_ToStrODBC(this.w_CIMENFIX)+;
                  ","+cp_ToStrODBC(this.w_CISEARMN)+;
                  ","+cp_ToStrODBC(this.w_CITOOLMN)+;
                  ","+cp_ToStrODBC(this.w_CIBCKGRD)+;
                  ","+cp_ToStrODBC(this.w_CIMRKGRD)+;
                  ","+cp_ToStrODBC(this.w_CISELECL)+;
                  ","+cp_ToStrODBC(this.w_CIEDTCOL)+;
                  ","+cp_ToStrODBC(this.w_CIGRIDCL)+;
                  ","+cp_ToStrODBC(this.w_CIDTLCLR)+;
                  ","+cp_ToStrODBC(this.w_CIDSBLBC)+;
                  ","+cp_ToStrODBC(this.w_CIDSBLFC)+;
                  ","+cp_ToStrODBC(this.w_CISCRCOL)+;
                  ","+cp_ToStrODBC(this.w_CIEVIZOM)+;
                  ","+cp_ToStrODBC(this.w_CILBLFNM)+;
                  ","+cp_ToStrODBC(this.w_CITXTFNM)+;
                  ","+cp_ToStrODBC(this.w_CICBXFNM)+;
                  ","+cp_ToStrODBC(this.w_CIBTNFNM)+;
                  ","+cp_ToStrODBC(this.w_CIGRDFNM)+;
                  ","+cp_ToStrODBC(this.w_CIPAGFNM)+;
                  ","+cp_ToStrODBC(this.w_CILBLFSZ)+;
                  ","+cp_ToStrODBC(this.w_CITXTFSZ)+;
                  ","+cp_ToStrODBC(this.w_CICBXFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIBTNFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIGRDFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIPAGFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIDSKMEN)+;
                  ","+cp_ToStrODBC(this.w_CINAVSTA)+;
                  ","+cp_ToStrODBC(this.w_CINAVNUB)+;
                  ","+cp_ToStrODBC(this.w_CINAVDIM)+;
                  ","+cp_ToStrODBC(this.w_CIMNAFNM)+;
                  ","+cp_ToStrODBC(this.w_CIMNAFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIWMAFNM)+;
                  ","+cp_ToStrODBC(this.w_CIWMAFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIOBLCOL)+;
                  ","+cp_ToStrODBC(this.w_ciobl_on)+;
                  ","+cp_ToStrODBC(this.w_CICOLZOM)+;
                  ","+cp_ToStrODBC(this.w_ciwaitwd)+;
                  ","+cp_ToStrODBC(this.w_cilblfit)+;
                  ","+cp_ToStrODBC(this.w_citxtfit)+;
                  ","+cp_ToStrODBC(this.w_cicbxfit)+;
                  ","+cp_ToStrODBC(this.w_cibtnfit)+;
                  ","+cp_ToStrODBC(this.w_cigrdfit)+;
                  ","+cp_ToStrODBC(this.w_cipagfit)+;
                  ","+cp_ToStrODBC(this.w_cilblfbo)+;
                  ","+cp_ToStrODBC(this.w_citxtfbo)+;
                  ","+cp_ToStrODBC(this.w_cicbxfbo)+;
                  ","+cp_ToStrODBC(this.w_cibtnfbo)+;
                  ","+cp_ToStrODBC(this.w_cigrdfbo)+;
                  ","+cp_ToStrODBC(this.w_cipagfbo)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'cpsetgui')
        i_extval=cp_InsertValVFPExtFlds(this,'cpsetgui')
        cp_CheckDeletedKey(i_cTable,0,'usrcode',this.w_usrcode)
        INSERT INTO (i_cTable);
              (usrcode,CIVTHEME,CIREPBEH,CICPTBAR,CIDSKBAR,CISTABAR,CISHWBTN,CITBSIZE,CIMDIFRM,CITABMEN,CIXPTHEM,CIMENFIX,CISEARMN,CITOOLMN,CIBCKGRD,CIMRKGRD,CISELECL,CIEDTCOL,CIGRIDCL,CIDTLCLR,CIDSBLBC,CIDSBLFC,CISCRCOL,CIEVIZOM,CILBLFNM,CITXTFNM,CICBXFNM,CIBTNFNM,CIGRDFNM,CIPAGFNM,CILBLFSZ,CITXTFSZ,CICBXFSZ,CIBTNFSZ,CIGRDFSZ,CIPAGFSZ,CIDSKMEN,CINAVSTA,CINAVNUB,CINAVDIM,CIMNAFNM,CIMNAFSZ,CIWMAFNM,CIWMAFSZ,CIOBLCOL,ciobl_on,CICOLZOM,ciwaitwd,cilblfit,citxtfit,cicbxfit,cibtnfit,cigrdfit,cipagfit,cilblfbo,citxtfbo,cicbxfbo,cibtnfbo,cigrdfbo,cipagfbo  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_usrcode;
                  ,this.w_CIVTHEME;
                  ,this.w_CIREPBEH;
                  ,this.w_CICPTBAR;
                  ,this.w_CIDSKBAR;
                  ,this.w_CISTABAR;
                  ,this.w_CISHWBTN;
                  ,this.w_CITBSIZE;
                  ,this.w_CIMDIFRM;
                  ,this.w_CITABMEN;
                  ,this.w_CIXPTHEM;
                  ,this.w_CIMENFIX;
                  ,this.w_CISEARMN;
                  ,this.w_CITOOLMN;
                  ,this.w_CIBCKGRD;
                  ,this.w_CIMRKGRD;
                  ,this.w_CISELECL;
                  ,this.w_CIEDTCOL;
                  ,this.w_CIGRIDCL;
                  ,this.w_CIDTLCLR;
                  ,this.w_CIDSBLBC;
                  ,this.w_CIDSBLFC;
                  ,this.w_CISCRCOL;
                  ,this.w_CIEVIZOM;
                  ,this.w_CILBLFNM;
                  ,this.w_CITXTFNM;
                  ,this.w_CICBXFNM;
                  ,this.w_CIBTNFNM;
                  ,this.w_CIGRDFNM;
                  ,this.w_CIPAGFNM;
                  ,this.w_CILBLFSZ;
                  ,this.w_CITXTFSZ;
                  ,this.w_CICBXFSZ;
                  ,this.w_CIBTNFSZ;
                  ,this.w_CIGRDFSZ;
                  ,this.w_CIPAGFSZ;
                  ,this.w_CIDSKMEN;
                  ,this.w_CINAVSTA;
                  ,this.w_CINAVNUB;
                  ,this.w_CINAVDIM;
                  ,this.w_CIMNAFNM;
                  ,this.w_CIMNAFSZ;
                  ,this.w_CIWMAFNM;
                  ,this.w_CIWMAFSZ;
                  ,this.w_CIOBLCOL;
                  ,this.w_ciobl_on;
                  ,this.w_CICOLZOM;
                  ,this.w_ciwaitwd;
                  ,this.w_cilblfit;
                  ,this.w_citxtfit;
                  ,this.w_cicbxfit;
                  ,this.w_cibtnfit;
                  ,this.w_cigrdfit;
                  ,this.w_cipagfit;
                  ,this.w_cilblfbo;
                  ,this.w_citxtfbo;
                  ,this.w_cicbxfbo;
                  ,this.w_cibtnfbo;
                  ,this.w_cigrdfbo;
                  ,this.w_cipagfbo;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.cpsetgui_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.cpsetgui_IDX,i_nConn)
      *
      * update cpsetgui
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'cpsetgui')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CIVTHEME="+cp_ToStrODBC(this.w_CIVTHEME)+;
             ",CIREPBEH="+cp_ToStrODBC(this.w_CIREPBEH)+;
             ",CICPTBAR="+cp_ToStrODBC(this.w_CICPTBAR)+;
             ",CIDSKBAR="+cp_ToStrODBC(this.w_CIDSKBAR)+;
             ",CISTABAR="+cp_ToStrODBC(this.w_CISTABAR)+;
             ",CISHWBTN="+cp_ToStrODBC(this.w_CISHWBTN)+;
             ",CITBSIZE="+cp_ToStrODBC(this.w_CITBSIZE)+;
             ",CIMDIFRM="+cp_ToStrODBC(this.w_CIMDIFRM)+;
             ",CITABMEN="+cp_ToStrODBC(this.w_CITABMEN)+;
             ",CIXPTHEM="+cp_ToStrODBC(this.w_CIXPTHEM)+;
             ",CIMENFIX="+cp_ToStrODBC(this.w_CIMENFIX)+;
             ",CISEARMN="+cp_ToStrODBC(this.w_CISEARMN)+;
             ",CITOOLMN="+cp_ToStrODBC(this.w_CITOOLMN)+;
             ",CIBCKGRD="+cp_ToStrODBC(this.w_CIBCKGRD)+;
             ",CIMRKGRD="+cp_ToStrODBC(this.w_CIMRKGRD)+;
             ",CISELECL="+cp_ToStrODBC(this.w_CISELECL)+;
             ",CIEDTCOL="+cp_ToStrODBC(this.w_CIEDTCOL)+;
             ",CIGRIDCL="+cp_ToStrODBC(this.w_CIGRIDCL)+;
             ",CIDTLCLR="+cp_ToStrODBC(this.w_CIDTLCLR)+;
             ",CIDSBLBC="+cp_ToStrODBC(this.w_CIDSBLBC)+;
             ",CIDSBLFC="+cp_ToStrODBC(this.w_CIDSBLFC)+;
             ",CISCRCOL="+cp_ToStrODBC(this.w_CISCRCOL)+;
             ",CIEVIZOM="+cp_ToStrODBC(this.w_CIEVIZOM)+;
             ",CILBLFNM="+cp_ToStrODBC(this.w_CILBLFNM)+;
             ",CITXTFNM="+cp_ToStrODBC(this.w_CITXTFNM)+;
             ",CICBXFNM="+cp_ToStrODBC(this.w_CICBXFNM)+;
             ",CIBTNFNM="+cp_ToStrODBC(this.w_CIBTNFNM)+;
             ",CIGRDFNM="+cp_ToStrODBC(this.w_CIGRDFNM)+;
             ",CIPAGFNM="+cp_ToStrODBC(this.w_CIPAGFNM)+;
             ",CILBLFSZ="+cp_ToStrODBC(this.w_CILBLFSZ)+;
             ",CITXTFSZ="+cp_ToStrODBC(this.w_CITXTFSZ)+;
             ",CICBXFSZ="+cp_ToStrODBC(this.w_CICBXFSZ)+;
             ",CIBTNFSZ="+cp_ToStrODBC(this.w_CIBTNFSZ)+;
             ",CIGRDFSZ="+cp_ToStrODBC(this.w_CIGRDFSZ)+;
             ",CIPAGFSZ="+cp_ToStrODBC(this.w_CIPAGFSZ)+;
             ",CIDSKMEN="+cp_ToStrODBC(this.w_CIDSKMEN)+;
             ",CINAVSTA="+cp_ToStrODBC(this.w_CINAVSTA)+;
             ",CINAVNUB="+cp_ToStrODBC(this.w_CINAVNUB)+;
             ",CINAVDIM="+cp_ToStrODBC(this.w_CINAVDIM)+;
             ",CIMNAFNM="+cp_ToStrODBC(this.w_CIMNAFNM)+;
             ",CIMNAFSZ="+cp_ToStrODBC(this.w_CIMNAFSZ)+;
             ",CIWMAFNM="+cp_ToStrODBC(this.w_CIWMAFNM)+;
             ",CIWMAFSZ="+cp_ToStrODBC(this.w_CIWMAFSZ)+;
             ",CIOBLCOL="+cp_ToStrODBC(this.w_CIOBLCOL)+;
             ",ciobl_on="+cp_ToStrODBC(this.w_ciobl_on)+;
             ",CICOLZOM="+cp_ToStrODBC(this.w_CICOLZOM)+;
             ",ciwaitwd="+cp_ToStrODBC(this.w_ciwaitwd)+;
             ",cilblfit="+cp_ToStrODBC(this.w_cilblfit)+;
             ",citxtfit="+cp_ToStrODBC(this.w_citxtfit)+;
             ",cicbxfit="+cp_ToStrODBC(this.w_cicbxfit)+;
             ",cibtnfit="+cp_ToStrODBC(this.w_cibtnfit)+;
             ",cigrdfit="+cp_ToStrODBC(this.w_cigrdfit)+;
             ",cipagfit="+cp_ToStrODBC(this.w_cipagfit)+;
             ",cilblfbo="+cp_ToStrODBC(this.w_cilblfbo)+;
             ",citxtfbo="+cp_ToStrODBC(this.w_citxtfbo)+;
             ",cicbxfbo="+cp_ToStrODBC(this.w_cicbxfbo)+;
             ",cibtnfbo="+cp_ToStrODBC(this.w_cibtnfbo)+;
             ",cigrdfbo="+cp_ToStrODBC(this.w_cigrdfbo)+;
             ",cipagfbo="+cp_ToStrODBC(this.w_cipagfbo)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'cpsetgui')
        i_cWhere = cp_PKFox(i_cTable  ,'usrcode',this.w_usrcode  )
        UPDATE (i_cTable) SET;
              CIVTHEME=this.w_CIVTHEME;
             ,CIREPBEH=this.w_CIREPBEH;
             ,CICPTBAR=this.w_CICPTBAR;
             ,CIDSKBAR=this.w_CIDSKBAR;
             ,CISTABAR=this.w_CISTABAR;
             ,CISHWBTN=this.w_CISHWBTN;
             ,CITBSIZE=this.w_CITBSIZE;
             ,CIMDIFRM=this.w_CIMDIFRM;
             ,CITABMEN=this.w_CITABMEN;
             ,CIXPTHEM=this.w_CIXPTHEM;
             ,CIMENFIX=this.w_CIMENFIX;
             ,CISEARMN=this.w_CISEARMN;
             ,CITOOLMN=this.w_CITOOLMN;
             ,CIBCKGRD=this.w_CIBCKGRD;
             ,CIMRKGRD=this.w_CIMRKGRD;
             ,CISELECL=this.w_CISELECL;
             ,CIEDTCOL=this.w_CIEDTCOL;
             ,CIGRIDCL=this.w_CIGRIDCL;
             ,CIDTLCLR=this.w_CIDTLCLR;
             ,CIDSBLBC=this.w_CIDSBLBC;
             ,CIDSBLFC=this.w_CIDSBLFC;
             ,CISCRCOL=this.w_CISCRCOL;
             ,CIEVIZOM=this.w_CIEVIZOM;
             ,CILBLFNM=this.w_CILBLFNM;
             ,CITXTFNM=this.w_CITXTFNM;
             ,CICBXFNM=this.w_CICBXFNM;
             ,CIBTNFNM=this.w_CIBTNFNM;
             ,CIGRDFNM=this.w_CIGRDFNM;
             ,CIPAGFNM=this.w_CIPAGFNM;
             ,CILBLFSZ=this.w_CILBLFSZ;
             ,CITXTFSZ=this.w_CITXTFSZ;
             ,CICBXFSZ=this.w_CICBXFSZ;
             ,CIBTNFSZ=this.w_CIBTNFSZ;
             ,CIGRDFSZ=this.w_CIGRDFSZ;
             ,CIPAGFSZ=this.w_CIPAGFSZ;
             ,CIDSKMEN=this.w_CIDSKMEN;
             ,CINAVSTA=this.w_CINAVSTA;
             ,CINAVNUB=this.w_CINAVNUB;
             ,CINAVDIM=this.w_CINAVDIM;
             ,CIMNAFNM=this.w_CIMNAFNM;
             ,CIMNAFSZ=this.w_CIMNAFSZ;
             ,CIWMAFNM=this.w_CIWMAFNM;
             ,CIWMAFSZ=this.w_CIWMAFSZ;
             ,CIOBLCOL=this.w_CIOBLCOL;
             ,ciobl_on=this.w_ciobl_on;
             ,CICOLZOM=this.w_CICOLZOM;
             ,ciwaitwd=this.w_ciwaitwd;
             ,cilblfit=this.w_cilblfit;
             ,citxtfit=this.w_citxtfit;
             ,cicbxfit=this.w_cicbxfit;
             ,cibtnfit=this.w_cibtnfit;
             ,cigrdfit=this.w_cigrdfit;
             ,cipagfit=this.w_cipagfit;
             ,cilblfbo=this.w_cilblfbo;
             ,citxtfbo=this.w_citxtfbo;
             ,cicbxfbo=this.w_cicbxfbo;
             ,cibtnfbo=this.w_cibtnfbo;
             ,cigrdfbo=this.w_cigrdfbo;
             ,cipagfbo=this.w_cipagfbo;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.cpsetgui_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.cpsetgui_IDX,i_nConn)
      *
      * delete cpsetgui
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'usrcode',this.w_usrcode  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.cpsetgui_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page2.oPag.oObj_2_19.Calculate(.w_CIEDTCOL)
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate(.w_CISELECL)
        .oPgFrm.Page2.oPag.oObj_2_24.Calculate(.w_CIDSBLFC)
        .oPgFrm.Page2.oPag.oObj_2_25.Calculate(.w_CIDSBLBC)
        .oPgFrm.Page2.oPag.oObj_2_43.Calculate(.w_CISCRCOL)
        .oPgFrm.Page2.oPag.oObj_2_44.Calculate(.w_CIDTLCLR)
        .oPgFrm.Page2.oPag.oObj_2_45.Calculate(.w_CIGRIDCL)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_THEME_APPLIC_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_THEME)
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_VIS_TOOL_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_VIS_APP_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_VIS_STATUS_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_VIS_CONT_BUTTON_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_DIM_IMM_TOOLBAR)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_ICON_TOOLBAR)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_VIS_MENUBAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_VIS_CTRL_FIND_MENU_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_VIS_TOOLMENU_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate(MSG_VIS_GRAD_BACKGOUND_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate(MSG_VIS_POINT_REC_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(MSG_VIS_MODE)
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(MSG_TOOLBAR_STATUSBAR)
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(MSG_FORM_CONTROL)
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(MSG_TAB)
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate(MSG_THEME_XP)
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate(MSG_MENU)
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate(MSG_VIS_MASK_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(MSG_VIS_TOOL_BAR)
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(MSG_VIS_APP_BAR)
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(MSG_VIS_STATUS_BAR)
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate(MSG_VIS_CONT_BUTTON)
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate(MSG_VIS_MENUBAR)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(MSG_VIS_CTRL_FIND_MENU)
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(MSG_VIS_TOOLMENU)
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(MSG_VIS_GRAD_BACKGOUND)
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(MSG_VIS_POINT_REC)
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(MSG_THEME_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(MSG_VIS_MASK_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(MSG_THEME_XP_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate(MSG_THEME_XP_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(MSG_TAB_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate(MSG_TAB_VALUE)
        .oPgFrm.Page2.oPag.oObj_2_48.Calculate(MSG_ENABLED)
        .oPgFrm.Page2.oPag.oObj_2_49.Calculate(MSG_DISABLED)
        .oPgFrm.Page2.oPag.oObj_2_50.Calculate(MSG_LISTS_ZOOM)
        .oPgFrm.Page2.oPag.oObj_2_51.Calculate(MSG_DESKTOP)
        .oPgFrm.Page2.oPag.oObj_2_52.Calculate(MSG_SELECTED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_53.Calculate(MSG_EDITABLE_LABEL+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate(MSG_BACKGROUND+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_55.Calculate(MSG_CHARACTER+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_56.Calculate(MSG_GREED_LINES+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_57.Calculate(MSG_LINE_SELECTED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_58.Calculate(MSG_GREED_DATA)
        .oPgFrm.Page2.oPag.oObj_2_59.Calculate(MSG_BACKGROUND_DESKTOP+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_60.Calculate(MSG_SELECTED_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_61.Calculate(MSG_EDITABLE_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_62.Calculate(MSG_GREED_LINES_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_63.Calculate(MSG_BACKGROUND_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate(MSG_CHARACTER_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_65.Calculate(MSG_BACKGROUND_DESKTOP_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_66.Calculate(MSG_LINE_SELECTED_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_20.Calculate(MSG_FONT_LABELS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_21.Calculate(MSG_FONT_FIELDS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_22.Calculate(MSG_FONT_COMBOBOX+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_23.Calculate(MSG_FONT_BUTTONS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_24.Calculate(MSG_FONT_ZOOM+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_25.Calculate(MSG_FONT_TAB+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_26.Calculate(MSG_DIMENSION)
        .oPgFrm.Page3.oPag.oObj_3_27.Calculate(MSG_FONT_LABELS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_28.Calculate(MSG_FONT_FIELDS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_29.Calculate(MSG_FONT_BUTTONS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_30.Calculate(MSG_FONT_ZOOM_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_31.Calculate(MSG_FONT_TAB_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_32.Calculate(MSG_FONT_COMBOBOX_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_33.Calculate(MSG_DIMENSION_LABELS)
        .oPgFrm.Page3.oPag.oObj_3_34.Calculate(MSG_DIMENSION_FIELDS)
        .oPgFrm.Page3.oPag.oObj_3_35.Calculate(MSG_DIMENSION_BUTTONS)
        .oPgFrm.Page3.oPag.oObj_3_36.Calculate(MSG_DIMENSION_ZOOM)
        .oPgFrm.Page3.oPag.oObj_3_37.Calculate(MSG_DIMENSION_TAB)
        .oPgFrm.Page3.oPag.oObj_3_38.Calculate(MSG_DIMENSION_COMBOBOX)
        .oPgFrm.Page4.oPag.oObj_4_17.Calculate(MSG_DESKTOP_MENU+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_18.Calculate(MSG_DESKTOP_MENU_STATUS)
        .oPgFrm.Page4.oPag.oObj_4_19.Calculate(MSG_NUMBER_BUTTON_VIS)
        .oPgFrm.Page4.oPag.oObj_4_20.Calculate(MSG_INITIAL_DIMENSION_MENU )
        .oPgFrm.Page4.oPag.oObj_4_21.Calculate(MSG_FONT_MENU_NAV)
        .oPgFrm.Page4.oPag.oObj_4_22.Calculate(MSG_DIMENSION+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_23.Calculate(MSG_DIMENSION+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_24.Calculate(MSG_FONT_WIN_MAN)
        .oPgFrm.Page4.oPag.oObj_4_25.Calculate(MSG_DESKTOP_MENU_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_26.Calculate(MSG_DESKTOP_MENU_STATUS_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_27.Calculate(MSG_NUMBER_BUTTON_VIS_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_28.Calculate(MSG_INITIAL_DIMENSION_MENU_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_29.Calculate(MSG_FONT_MENU_NAV_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_30.Calculate(MSG_FONT_WIN_MAN_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_31.Calculate(MSG_FONT_MENU_NAV_DIM_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_32.Calculate(MSG_FONT_WIN_MAN_DIM_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_33.Calculate(MSG_DESKTOP_MENU_VALUE)
        .oPgFrm.Page4.oPag.oObj_4_34.Calculate(MSG_DESKTOP_MENU_STATUS_VALUE)
        .oPgFrm.Page2.oPag.oObj_2_69.Calculate(.w_CIOBLCOL)
        .oPgFrm.Page2.oPag.oObj_2_72.Calculate(MSG_HIGHLIGHT_REQUIRED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_73.Calculate(MSG_REQUIRED_TOOLTIPTEXT)
        .oPgFrm.Page2.oPag.oObj_2_78.Calculate(.w_CICOLZOM)
        .oPgFrm.Page2.oPag.oObj_2_79.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_80.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM)
        .oPgFrm.Page2.oPag.oObj_2_81.Calculate(MSG_SELECTED_ROW_COLOUR+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_82.Calculate(MSG_SELECTED_ROW_COLOUR_TOOLTIP)
        if .o_CIVTHEME<>.w_CIVTHEME.or. .o_CITABMEN<>.w_CITABMEN
          .Calculate_DBYVFFNWVV()
        endif
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate(MSG_VIS_WAITWND_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(MSG_VIS_WAITWND)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,70,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_19.Calculate(.w_CIEDTCOL)
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate(.w_CISELECL)
        .oPgFrm.Page2.oPag.oObj_2_24.Calculate(.w_CIDSBLFC)
        .oPgFrm.Page2.oPag.oObj_2_25.Calculate(.w_CIDSBLBC)
        .oPgFrm.Page2.oPag.oObj_2_43.Calculate(.w_CISCRCOL)
        .oPgFrm.Page2.oPag.oObj_2_44.Calculate(.w_CIDTLCLR)
        .oPgFrm.Page2.oPag.oObj_2_45.Calculate(.w_CIGRIDCL)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_THEME_APPLIC_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_THEME)
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_VIS_TOOL_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_VIS_APP_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_VIS_STATUS_BAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_VIS_CONT_BUTTON_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_DIM_IMM_TOOLBAR)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_ICON_TOOLBAR)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_VIS_MENUBAR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_VIS_CTRL_FIND_MENU_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_VIS_TOOLMENU_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate(MSG_VIS_GRAD_BACKGOUND_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate(MSG_VIS_POINT_REC_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(MSG_VIS_MODE)
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(MSG_TOOLBAR_STATUSBAR)
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(MSG_FORM_CONTROL)
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(MSG_TAB)
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate(MSG_THEME_XP)
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate(MSG_MENU)
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate(MSG_VIS_MASK_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(MSG_VIS_TOOL_BAR)
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(MSG_VIS_APP_BAR)
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(MSG_VIS_STATUS_BAR)
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate(MSG_VIS_CONT_BUTTON)
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate(MSG_VIS_MENUBAR)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(MSG_VIS_CTRL_FIND_MENU)
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(MSG_VIS_TOOLMENU)
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(MSG_VIS_GRAD_BACKGOUND)
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(MSG_VIS_POINT_REC)
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(MSG_THEME_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(MSG_VIS_MASK_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(MSG_THEME_XP_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate(MSG_THEME_XP_VALUE)
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(MSG_TAB_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate(MSG_TAB_VALUE)
        .oPgFrm.Page2.oPag.oObj_2_48.Calculate(MSG_ENABLED)
        .oPgFrm.Page2.oPag.oObj_2_49.Calculate(MSG_DISABLED)
        .oPgFrm.Page2.oPag.oObj_2_50.Calculate(MSG_LISTS_ZOOM)
        .oPgFrm.Page2.oPag.oObj_2_51.Calculate(MSG_DESKTOP)
        .oPgFrm.Page2.oPag.oObj_2_52.Calculate(MSG_SELECTED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_53.Calculate(MSG_EDITABLE_LABEL+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate(MSG_BACKGROUND+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_55.Calculate(MSG_CHARACTER+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_56.Calculate(MSG_GREED_LINES+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_57.Calculate(MSG_LINE_SELECTED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_58.Calculate(MSG_GREED_DATA)
        .oPgFrm.Page2.oPag.oObj_2_59.Calculate(MSG_BACKGROUND_DESKTOP+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_60.Calculate(MSG_SELECTED_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_61.Calculate(MSG_EDITABLE_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_62.Calculate(MSG_GREED_LINES_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_63.Calculate(MSG_BACKGROUND_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate(MSG_CHARACTER_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_65.Calculate(MSG_BACKGROUND_DESKTOP_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_66.Calculate(MSG_LINE_SELECTED_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_20.Calculate(MSG_FONT_LABELS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_21.Calculate(MSG_FONT_FIELDS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_22.Calculate(MSG_FONT_COMBOBOX+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_23.Calculate(MSG_FONT_BUTTONS+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_24.Calculate(MSG_FONT_ZOOM+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_25.Calculate(MSG_FONT_TAB+MSG_FS)
        .oPgFrm.Page3.oPag.oObj_3_26.Calculate(MSG_DIMENSION)
        .oPgFrm.Page3.oPag.oObj_3_27.Calculate(MSG_FONT_LABELS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_28.Calculate(MSG_FONT_FIELDS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_29.Calculate(MSG_FONT_BUTTONS_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_30.Calculate(MSG_FONT_ZOOM_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_31.Calculate(MSG_FONT_TAB_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_32.Calculate(MSG_FONT_COMBOBOX_TOOLTIP)
        .oPgFrm.Page3.oPag.oObj_3_33.Calculate(MSG_DIMENSION_LABELS)
        .oPgFrm.Page3.oPag.oObj_3_34.Calculate(MSG_DIMENSION_FIELDS)
        .oPgFrm.Page3.oPag.oObj_3_35.Calculate(MSG_DIMENSION_BUTTONS)
        .oPgFrm.Page3.oPag.oObj_3_36.Calculate(MSG_DIMENSION_ZOOM)
        .oPgFrm.Page3.oPag.oObj_3_37.Calculate(MSG_DIMENSION_TAB)
        .oPgFrm.Page3.oPag.oObj_3_38.Calculate(MSG_DIMENSION_COMBOBOX)
        .oPgFrm.Page4.oPag.oObj_4_17.Calculate(MSG_DESKTOP_MENU+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_18.Calculate(MSG_DESKTOP_MENU_STATUS)
        .oPgFrm.Page4.oPag.oObj_4_19.Calculate(MSG_NUMBER_BUTTON_VIS)
        .oPgFrm.Page4.oPag.oObj_4_20.Calculate(MSG_INITIAL_DIMENSION_MENU )
        .oPgFrm.Page4.oPag.oObj_4_21.Calculate(MSG_FONT_MENU_NAV)
        .oPgFrm.Page4.oPag.oObj_4_22.Calculate(MSG_DIMENSION+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_23.Calculate(MSG_DIMENSION+MSG_FS)
        .oPgFrm.Page4.oPag.oObj_4_24.Calculate(MSG_FONT_WIN_MAN)
        .oPgFrm.Page4.oPag.oObj_4_25.Calculate(MSG_DESKTOP_MENU_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_26.Calculate(MSG_DESKTOP_MENU_STATUS_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_27.Calculate(MSG_NUMBER_BUTTON_VIS_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_28.Calculate(MSG_INITIAL_DIMENSION_MENU_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_29.Calculate(MSG_FONT_MENU_NAV_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_30.Calculate(MSG_FONT_WIN_MAN_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_31.Calculate(MSG_FONT_MENU_NAV_DIM_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_32.Calculate(MSG_FONT_WIN_MAN_DIM_TOOLTIP)
        .oPgFrm.Page4.oPag.oObj_4_33.Calculate(MSG_DESKTOP_MENU_VALUE)
        .oPgFrm.Page4.oPag.oObj_4_34.Calculate(MSG_DESKTOP_MENU_STATUS_VALUE)
        .oPgFrm.Page2.oPag.oObj_2_69.Calculate(.w_CIOBLCOL)
        .oPgFrm.Page2.oPag.oObj_2_72.Calculate(MSG_HIGHLIGHT_REQUIRED+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_73.Calculate(MSG_REQUIRED_TOOLTIPTEXT)
        .oPgFrm.Page2.oPag.oObj_2_78.Calculate(.w_CICOLZOM)
        .oPgFrm.Page2.oPag.oObj_2_79.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM_TOOLTIP)
        .oPgFrm.Page2.oPag.oObj_2_80.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM)
        .oPgFrm.Page2.oPag.oObj_2_81.Calculate(MSG_SELECTED_ROW_COLOUR+MSG_FS)
        .oPgFrm.Page2.oPag.oObj_2_82.Calculate(MSG_SELECTED_ROW_COLOUR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate(MSG_VIS_WAITWND_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(MSG_VIS_WAITWND)
    endwith
  return

  proc Calculate_ZSLVHQZHHK()
    with this
          * --- Set default
          .w_RET = .SetDefault()
    endwith
  endproc
  proc Calculate_DDOFRCSJOB()
    with this
          * --- Inizializza combo...
          .w_CIVTHEME = -1
    endwith
  endproc
  proc Calculate_DBYVFFNWVV()
    with this
          * --- Flag gradiente
          .w_CIBCKGRD = IIF(.w_CIVTHEME <> -1 And .w_CITABMEN <> 'S', .w_CIBCKGRD, 'N')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCIVTHEME_1_2.enabled = this.oPgFrm.Page1.oPag.oCIVTHEME_1_2.mCond()
    this.oPgFrm.Page1.oPag.oCITABMEN_1_10.enabled = this.oPgFrm.Page1.oPag.oCITABMEN_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCIMENFIX_1_12.enabled = this.oPgFrm.Page1.oPag.oCIMENFIX_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCISEARMN_1_13.enabled = this.oPgFrm.Page1.oPag.oCISEARMN_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCIBCKGRD_1_15.enabled = this.oPgFrm.Page1.oPag.oCIBCKGRD_1_15.mCond()
    this.oPgFrm.Page2.oPag.oCIEVIZOM_2_16.enabled = this.oPgFrm.Page2.oPag.oCIEVIZOM_2_16.mCond()
    this.oPgFrm.Page4.oPag.oCIDSKMEN_4_1.enabled = this.oPgFrm.Page4.oPag.oCIDSKMEN_4_1.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_15.enabled = this.oPgFrm.Page2.oPag.oBtn_2_15.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_68.enabled = this.oPgFrm.Page2.oPag.oBtn_2_68.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oBtn_2_15.visible=!this.oPgFrm.Page2.oPag.oBtn_2_15.mHide()
    this.oPgFrm.Page2.oPag.oCOLORZOM_2_75.visible=!this.oPgFrm.Page2.oPag.oCOLORZOM_2_75.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_76.visible=!this.oPgFrm.Page2.oPag.oStr_2_76.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_19.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_20.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_24.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_25.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_43.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_44.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_45.Event(cEvent)
        if lower(cEvent)==lower("SetDefault")
          .Calculate_ZSLVHQZHHK()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_48.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_49.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_50.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_51.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_52.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_53.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_54.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_55.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_56.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_57.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_58.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_59.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_60.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_61.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_62.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_63.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_64.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_65.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_66.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_20.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_21.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_22.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_23.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_24.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_25.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_26.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_27.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_28.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_29.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_30.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_31.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_32.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_33.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_34.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_35.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_36.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_37.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_38.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_17.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_18.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_19.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_20.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_21.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_22.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_23.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_24.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_25.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_26.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_27.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_28.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_29.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_30.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_31.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_32.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_33.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_34.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_DDOFRCSJOB()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_69.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_72.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_73.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_78.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_79.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_80.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_81.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_82.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_71.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCIVTHEME_1_2.RadioValue()==this.w_CIVTHEME)
      this.oPgFrm.Page1.oPag.oCIVTHEME_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIREPBEH_1_3.RadioValue()==this.w_CIREPBEH)
      this.oPgFrm.Page1.oPag.oCIREPBEH_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCICPTBAR_1_4.RadioValue()==this.w_CICPTBAR)
      this.oPgFrm.Page1.oPag.oCICPTBAR_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIDSKBAR_1_5.RadioValue()==this.w_CIDSKBAR)
      this.oPgFrm.Page1.oPag.oCIDSKBAR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISTABAR_1_6.RadioValue()==this.w_CISTABAR)
      this.oPgFrm.Page1.oPag.oCISTABAR_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISHWBTN_1_7.RadioValue()==this.w_CISHWBTN)
      this.oPgFrm.Page1.oPag.oCISHWBTN_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCITBSIZE_1_8.RadioValue()==this.w_CITBSIZE)
      this.oPgFrm.Page1.oPag.oCITBSIZE_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMDIFRM_1_9.RadioValue()==this.w_CIMDIFRM)
      this.oPgFrm.Page1.oPag.oCIMDIFRM_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCITABMEN_1_10.RadioValue()==this.w_CITABMEN)
      this.oPgFrm.Page1.oPag.oCITABMEN_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIXPTHEM_1_11.RadioValue()==this.w_CIXPTHEM)
      this.oPgFrm.Page1.oPag.oCIXPTHEM_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMENFIX_1_12.RadioValue()==this.w_CIMENFIX)
      this.oPgFrm.Page1.oPag.oCIMENFIX_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISEARMN_1_13.RadioValue()==this.w_CISEARMN)
      this.oPgFrm.Page1.oPag.oCISEARMN_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCITOOLMN_1_14.RadioValue()==this.w_CITOOLMN)
      this.oPgFrm.Page1.oPag.oCITOOLMN_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIBCKGRD_1_15.RadioValue()==this.w_CIBCKGRD)
      this.oPgFrm.Page1.oPag.oCIBCKGRD_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMRKGRD_1_16.RadioValue()==this.w_CIMRKGRD)
      this.oPgFrm.Page1.oPag.oCIMRKGRD_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLORCTSEL_2_8.value==this.w_COLORCTSEL)
      this.oPgFrm.Page2.oPag.oCOLORCTSEL_2_8.value=this.w_COLORCTSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oCIEVIZOM_2_16.RadioValue()==this.w_CIEVIZOM)
      this.oPgFrm.Page2.oPag.oCIEVIZOM_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLORCTED_2_21.value==this.w_COLORCTED)
      this.oPgFrm.Page2.oPag.oCOLORCTED_2_21.value=this.w_COLORCTED
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLORFCTDSBL_2_26.value==this.w_COLORFCTDSBL)
      this.oPgFrm.Page2.oPag.oCOLORFCTDSBL_2_26.value=this.w_COLORFCTDSBL
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLORBCTDSBL_2_27.value==this.w_COLORBCTDSBL)
      this.oPgFrm.Page2.oPag.oCOLORBCTDSBL_2_27.value=this.w_COLORBCTDSBL
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLORGRD_2_34.value==this.w_COLORGRD)
      this.oPgFrm.Page2.oPag.oCOLORGRD_2_34.value=this.w_COLORGRD
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLORSCREEN_2_37.value==this.w_COLORSCREEN)
      this.oPgFrm.Page2.oPag.oCOLORSCREEN_2_37.value=this.w_COLORSCREEN
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLORROWDTL_2_42.value==this.w_COLORROWDTL)
      this.oPgFrm.Page2.oPag.oCOLORROWDTL_2_42.value=this.w_COLORROWDTL
    endif
    if not(this.oPgFrm.Page3.oPag.oCILBLFNM_3_1.RadioValue()==this.w_CILBLFNM)
      this.oPgFrm.Page3.oPag.oCILBLFNM_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCITXTFNM_3_2.RadioValue()==this.w_CITXTFNM)
      this.oPgFrm.Page3.oPag.oCITXTFNM_3_2.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCICBXFNM_3_3.RadioValue()==this.w_CICBXFNM)
      this.oPgFrm.Page3.oPag.oCICBXFNM_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCIBTNFNM_3_4.RadioValue()==this.w_CIBTNFNM)
      this.oPgFrm.Page3.oPag.oCIBTNFNM_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCIGRDFNM_3_5.RadioValue()==this.w_CIGRDFNM)
      this.oPgFrm.Page3.oPag.oCIGRDFNM_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCIPAGFNM_3_6.RadioValue()==this.w_CIPAGFNM)
      this.oPgFrm.Page3.oPag.oCIPAGFNM_3_6.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCILBLFSZ_3_13.value==this.w_CILBLFSZ)
      this.oPgFrm.Page3.oPag.oCILBLFSZ_3_13.value=this.w_CILBLFSZ
    endif
    if not(this.oPgFrm.Page3.oPag.oCITXTFSZ_3_15.value==this.w_CITXTFSZ)
      this.oPgFrm.Page3.oPag.oCITXTFSZ_3_15.value=this.w_CITXTFSZ
    endif
    if not(this.oPgFrm.Page3.oPag.oCICBXFSZ_3_16.value==this.w_CICBXFSZ)
      this.oPgFrm.Page3.oPag.oCICBXFSZ_3_16.value=this.w_CICBXFSZ
    endif
    if not(this.oPgFrm.Page3.oPag.oCIBTNFSZ_3_17.value==this.w_CIBTNFSZ)
      this.oPgFrm.Page3.oPag.oCIBTNFSZ_3_17.value=this.w_CIBTNFSZ
    endif
    if not(this.oPgFrm.Page3.oPag.oCIGRDFSZ_3_18.value==this.w_CIGRDFSZ)
      this.oPgFrm.Page3.oPag.oCIGRDFSZ_3_18.value=this.w_CIGRDFSZ
    endif
    if not(this.oPgFrm.Page3.oPag.oCIPAGFSZ_3_19.value==this.w_CIPAGFSZ)
      this.oPgFrm.Page3.oPag.oCIPAGFSZ_3_19.value=this.w_CIPAGFSZ
    endif
    if not(this.oPgFrm.Page4.oPag.oCIDSKMEN_4_1.RadioValue()==this.w_CIDSKMEN)
      this.oPgFrm.Page4.oPag.oCIDSKMEN_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCINAVSTA_4_2.RadioValue()==this.w_CINAVSTA)
      this.oPgFrm.Page4.oPag.oCINAVSTA_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCINAVNUB_4_3.RadioValue()==this.w_CINAVNUB)
      this.oPgFrm.Page4.oPag.oCINAVNUB_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCINAVDIM_4_4.value==this.w_CINAVDIM)
      this.oPgFrm.Page4.oPag.oCINAVDIM_4_4.value=this.w_CINAVDIM
    endif
    if not(this.oPgFrm.Page4.oPag.oCIMNAFNM_4_5.RadioValue()==this.w_CIMNAFNM)
      this.oPgFrm.Page4.oPag.oCIMNAFNM_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIMNAFSZ_4_6.value==this.w_CIMNAFSZ)
      this.oPgFrm.Page4.oPag.oCIMNAFSZ_4_6.value=this.w_CIMNAFSZ
    endif
    if not(this.oPgFrm.Page4.oPag.oCIWMAFNM_4_7.RadioValue()==this.w_CIWMAFNM)
      this.oPgFrm.Page4.oPag.oCIWMAFNM_4_7.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIWMAFSZ_4_8.value==this.w_CIWMAFSZ)
      this.oPgFrm.Page4.oPag.oCIWMAFSZ_4_8.value=this.w_CIWMAFSZ
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLORCTOB_2_71.value==this.w_COLORCTOB)
      this.oPgFrm.Page2.oPag.oCOLORCTOB_2_71.value=this.w_COLORCTOB
    endif
    if not(this.oPgFrm.Page2.oPag.ociobl_on_2_74.RadioValue()==this.w_ciobl_on)
      this.oPgFrm.Page2.oPag.ociobl_on_2_74.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLORZOM_2_75.value==this.w_COLORZOM)
      this.oPgFrm.Page2.oPag.oCOLORZOM_2_75.value=this.w_COLORZOM
    endif
    if not(this.oPgFrm.Page1.oPag.ociwaitwd_1_70.RadioValue()==this.w_ciwaitwd)
      this.oPgFrm.Page1.oPag.ociwaitwd_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocilblfit_3_39.RadioValue()==this.w_cilblfit)
      this.oPgFrm.Page3.oPag.ocilblfit_3_39.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocitxtfit_3_40.RadioValue()==this.w_citxtfit)
      this.oPgFrm.Page3.oPag.ocitxtfit_3_40.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocicbxfit_3_41.RadioValue()==this.w_cicbxfit)
      this.oPgFrm.Page3.oPag.ocicbxfit_3_41.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocibtnfit_3_42.RadioValue()==this.w_cibtnfit)
      this.oPgFrm.Page3.oPag.ocibtnfit_3_42.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocigrdfit_3_43.RadioValue()==this.w_cigrdfit)
      this.oPgFrm.Page3.oPag.ocigrdfit_3_43.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocipagfit_3_44.RadioValue()==this.w_cipagfit)
      this.oPgFrm.Page3.oPag.ocipagfit_3_44.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocilblfbo_3_45.RadioValue()==this.w_cilblfbo)
      this.oPgFrm.Page3.oPag.ocilblfbo_3_45.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocitxtfbo_3_46.RadioValue()==this.w_citxtfbo)
      this.oPgFrm.Page3.oPag.ocitxtfbo_3_46.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocicbxfbo_3_47.RadioValue()==this.w_cicbxfbo)
      this.oPgFrm.Page3.oPag.ocicbxfbo_3_47.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocibtnfbo_3_48.RadioValue()==this.w_cibtnfbo)
      this.oPgFrm.Page3.oPag.ocibtnfbo_3_48.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocigrdfbo_3_49.RadioValue()==this.w_cigrdfbo)
      this.oPgFrm.Page3.oPag.ocigrdfbo_3_49.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ocipagfbo_3_50.RadioValue()==this.w_cipagfbo)
      this.oPgFrm.Page3.oPag.ocipagfbo_3_50.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'cpsetgui')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(!Empty(.w_CILBLFNM) and TestFont('L', .w_CILBLFNM, .w_CILBLFSZ)<>-1 And !Empty(.w_CILBLFSZ))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCILBLFSZ_3_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CITXTFNM) and TestFont('T', .w_CITXTFNM, .w_CITXTFSZ)<>-1 And !Empty(.w_CITXTFSZ))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCITXTFSZ_3_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CICBXFNM) and TestFont('C', .w_CICBXFNM, .w_CICBXFSZ)<>-1 And !Empty(.w_CICBXFSZ))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCICBXFSZ_3_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIBTNFNM) and TestFont('B', .w_CIBTNFNM, .w_CIBTNFSZ)<>-1 And !Empty(.w_CIBTNFSZ))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCIBTNFSZ_3_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIGRDFNM) and TestFont('G', .w_CIGRDFNM, .w_CIGRDFSZ)<>-1 And !Empty(.w_CIGRDFSZ))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCIGRDFSZ_3_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIPAGFNM) and TestFont('P', .w_CIPAGFNM, .w_CIPAGFSZ)<>-1 And !Empty(.w_CIPAGFSZ))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCIPAGFSZ_3_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIMNAFNM) and TestFont('L', .w_CIMNAFNM, .w_CIMNAFSZ)<>-1 And !Empty(.w_CIMNAFSZ))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCIMNAFSZ_4_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIWMAFNM) and TestFont('L', .w_CIWMAFNM, .w_CIWMAFSZ)<>-1 And !Empty(.w_CIWMAFSZ))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCIWMAFSZ_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- cp_setgui
      If i_bres
         cp_ErrorMsg(cp_translate(MSG_SETTING_ON_NEXT_RESTART))
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CIVTHEME = this.w_CIVTHEME
    this.o_CITABMEN = this.w_CITABMEN
    return

enddefine

* --- Define pages as container
define class tcp_setguiPag1 as StdContainer
  Width  = 531
  height = 343
  stdWidth  = 531
  stdheight = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCIVTHEME_1_2 as StdCombo with uid="KKYNAIXTLY",value=2,rtseq=2,rtrep=.f.,left=32,top=33,width=187,height=21;
    , RowSourceType = 1;
    , ToolTipText = "Tema applicato al gestionale";
    , HelpContextID = 206009433;
    , cFormVar="w_CIVTHEME",RowSource=""+"Standard,"+"Sistema Operativo,"+"2003 Blue,"+"2003 Oliva,"+"2003 Silver,"+"2007 Black,"+"2007 Blue,"+"2007 Silver", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCIVTHEME_1_2.RadioValue()
    return(iif(this.value =1,-1,;
    iif(this.value =2,0,;
    iif(this.value =3,1,;
    iif(this.value =4,2,;
    iif(this.value =5,3,;
    iif(this.value =6,4,;
    iif(this.value =7,5,;
    iif(this.value =8,6,;
    0)))))))))
  endfunc
  func oCIVTHEME_1_2.GetRadio()
    this.Parent.oContained.w_CIVTHEME = this.RadioValue()
    return .t.
  endfunc

  func oCIVTHEME_1_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIVTHEME==-1,1,;
      iif(this.Parent.oContained.w_CIVTHEME==0,2,;
      iif(this.Parent.oContained.w_CIVTHEME==1,3,;
      iif(this.Parent.oContained.w_CIVTHEME==2,4,;
      iif(this.Parent.oContained.w_CIVTHEME==3,5,;
      iif(this.Parent.oContained.w_CIVTHEME==4,6,;
      iif(this.Parent.oContained.w_CIVTHEME==5,7,;
      iif(this.Parent.oContained.w_CIVTHEME==6,8,;
      0))))))))
  endfunc

  func oCIVTHEME_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Version(5)>=900)
    endwith
   endif
  endfunc


  add object oCIREPBEH_1_3 as StdCombo with uid="PWIWMOGOMA",rtseq=3,rtrep=.f.,left=286,top=56,width=187,height=21;
    , HelpContextID = 55800961;
    , cFormVar="w_CIREPBEH",RowSource=""+"80 (Retrocompatibile),"+"90 (GDI+)", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCIREPBEH_1_3.RadioValue()
    return(iif(this.value =1,'8',;
    iif(this.value =2,'9',;
    space(1))))
  endfunc
  func oCIREPBEH_1_3.GetRadio()
    this.Parent.oContained.w_CIREPBEH = this.RadioValue()
    return .t.
  endfunc

  func oCIREPBEH_1_3.SetRadio()
    this.Parent.oContained.w_CIREPBEH=trim(this.Parent.oContained.w_CIREPBEH)
    this.value = ;
      iif(this.Parent.oContained.w_CIREPBEH=='8',1,;
      iif(this.Parent.oContained.w_CIREPBEH=='9',2,;
      0))
  endfunc

  add object oCICPTBAR_1_4 as StdCheck with uid="OJRJRQJXNJ",rtseq=4,rtrep=.f.,left=32, top=113, caption="Visualizza barra degli strumenti",;
    ToolTipText = "Se attivo visualizza la barra degli strumenti",;
    HelpContextID = 133461277,;
    cFormVar="w_CICPTBAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCICPTBAR_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCICPTBAR_1_4.GetRadio()
    this.Parent.oContained.w_CICPTBAR = this.RadioValue()
    return .t.
  endfunc

  func oCICPTBAR_1_4.SetRadio()
    this.Parent.oContained.w_CICPTBAR=trim(this.Parent.oContained.w_CICPTBAR)
    this.value = ;
      iif(this.Parent.oContained.w_CICPTBAR=='S',1,;
      0)
  endfunc

  add object oCIDSKBAR_1_5 as StdCheck with uid="ZXPARNXJCJ",rtseq=5,rtrep=.f.,left=32, top=134, caption="Visualizza barra delle applicazioni",;
    ToolTipText = "Se attivo visualizza la barra delle applicazioni",;
    HelpContextID = 254113053,;
    cFormVar="w_CIDSKBAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIDSKBAR_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIDSKBAR_1_5.GetRadio()
    this.Parent.oContained.w_CIDSKBAR = this.RadioValue()
    return .t.
  endfunc

  func oCIDSKBAR_1_5.SetRadio()
    this.Parent.oContained.w_CIDSKBAR=trim(this.Parent.oContained.w_CIDSKBAR)
    this.value = ;
      iif(this.Parent.oContained.w_CIDSKBAR=='S',1,;
      0)
  endfunc

  add object oCISTABAR_1_6 as StdCheck with uid="ERRTOGTWXP",rtseq=6,rtrep=.f.,left=32, top=155, caption="Visualizza barra di stato",;
    ToolTipText = "Se attivo visualizza la barra di stato (in basso)",;
    HelpContextID = 88372509,;
    cFormVar="w_CISTABAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISTABAR_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCISTABAR_1_6.GetRadio()
    this.Parent.oContained.w_CISTABAR = this.RadioValue()
    return .t.
  endfunc

  func oCISTABAR_1_6.SetRadio()
    this.Parent.oContained.w_CISTABAR=trim(this.Parent.oContained.w_CISTABAR)
    this.value = ;
      iif(this.Parent.oContained.w_CISTABAR=='S',1,;
      0)
  endfunc

  add object oCISHWBTN_1_7 as StdCheck with uid="FQMHCYRRPS",rtseq=7,rtrep=.f.,left=32, top=176, caption="Visualizza bottone contestuale",;
    ToolTipText = "Se attivo visualizza il bottone contestuale accanto ai campi con link",;
    HelpContextID = 176452848,;
    cFormVar="w_CISHWBTN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISHWBTN_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCISHWBTN_1_7.GetRadio()
    this.Parent.oContained.w_CISHWBTN = this.RadioValue()
    return .t.
  endfunc

  func oCISHWBTN_1_7.SetRadio()
    this.Parent.oContained.w_CISHWBTN=trim(this.Parent.oContained.w_CISHWBTN)
    this.value = ;
      iif(this.Parent.oContained.w_CISHWBTN=='S',1,;
      0)
  endfunc


  add object oCITBSIZE_1_8 as StdCombo with uid="TKEQGLEPQU",rtseq=8,rtrep=.f.,left=32,top=219,width=76,height=21;
    , ToolTipText = "Dimensione icone toolbar";
    , HelpContextID = 165317530;
    , cFormVar="w_CITBSIZE",RowSource=""+"16,"+"24,"+"32", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCITBSIZE_1_8.RadioValue()
    return(iif(this.value =1,16,;
    iif(this.value =2,24,;
    iif(this.value =3,32,;
    0))))
  endfunc
  func oCITBSIZE_1_8.GetRadio()
    this.Parent.oContained.w_CITBSIZE = this.RadioValue()
    return .t.
  endfunc

  func oCITBSIZE_1_8.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CITBSIZE==16,1,;
      iif(this.Parent.oContained.w_CITBSIZE==24,2,;
      iif(this.Parent.oContained.w_CITBSIZE==32,3,;
      0)))
  endfunc


  add object oCIMDIFRM_1_9 as StdCombo with uid="BQFRHJEJMG",rtseq=9,rtrep=.f.,left=286,top=128,width=166,height=21;
    , RowSourceType = 1;
    , ToolTipText = "Modalit� di visualizzazione delle maschere";
    , HelpContextID = 205419742;
    , cFormVar="w_CIMDIFRM",RowSource=""+"Classica,"+"Integrata,"+"Classica / Integrata,"+"Integrata / Classica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCIMDIFRM_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'I',;
    iif(this.value =3,'A',;
    iif(this.value =4,'M',;
    space(1))))))
  endfunc
  func oCIMDIFRM_1_9.GetRadio()
    this.Parent.oContained.w_CIMDIFRM = this.RadioValue()
    return .t.
  endfunc

  func oCIMDIFRM_1_9.SetRadio()
    this.Parent.oContained.w_CIMDIFRM=trim(this.Parent.oContained.w_CIMDIFRM)
    this.value = ;
      iif(this.Parent.oContained.w_CIMDIFRM=='S',1,;
      iif(this.Parent.oContained.w_CIMDIFRM=='I',2,;
      iif(this.Parent.oContained.w_CIMDIFRM=='A',3,;
      iif(this.Parent.oContained.w_CIMDIFRM=='M',4,;
      0))))
  endfunc


  add object oCITABMEN_1_10 as StdCombo with uid="UGMBXROSQM",rtseq=10,rtrep=.f.,left=286,top=172,width=166,height=21;
    , RowSourceType = 1;
    , ToolTipText = "Modalit� di visualizzazione delle tab";
    , HelpContextID = 183143199;
    , cFormVar="w_CITABMEN",RowSource=""+"Standard,"+"A tema", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCITABMEN_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oCITABMEN_1_10.GetRadio()
    this.Parent.oContained.w_CITABMEN = this.RadioValue()
    return .t.
  endfunc

  func oCITABMEN_1_10.SetRadio()
    this.Parent.oContained.w_CITABMEN=trim(this.Parent.oContained.w_CITABMEN)
    this.value = ;
      iif(this.Parent.oContained.w_CITABMEN=='S',1,;
      iif(this.Parent.oContained.w_CITABMEN=='T',2,;
      0))
  endfunc

  func oCITABMEN_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME <> -1)
    endwith
   endif
  endfunc


  add object oCIXPTHEM_1_11 as StdCombo with uid="AMVFHFREXI",value=1,rtseq=11,rtrep=.f.,left=286,top=219,width=108,height=21;
    , RowSourceType = 1;
    , ToolTipText = "Tema XP attivo o meno";
    , HelpContextID = 133597999;
    , cFormVar="w_CIXPTHEM",RowSource=""+"Disattivo,"+"Attivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCIXPTHEM_1_11.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    -1)))
  endfunc
  func oCIXPTHEM_1_11.GetRadio()
    this.Parent.oContained.w_CIXPTHEM = this.RadioValue()
    return .t.
  endfunc

  func oCIXPTHEM_1_11.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIXPTHEM==0,1,;
      iif(this.Parent.oContained.w_CIXPTHEM==1,2,;
      0))
  endfunc

  add object oCIMENFIX_1_12 as StdCheck with uid="QKUNFLMZFJ",rtseq=12,rtrep=.f.,left=32, top=276, caption="Men� in posizione fissa",;
    ToolTipText = "Se attivo visualizza la barra del menu in posizione fissa",;
    HelpContextID = 21919109,;
    cFormVar="w_CIMENFIX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIMENFIX_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCIMENFIX_1_12.GetRadio()
    this.Parent.oContained.w_CIMENFIX = this.RadioValue()
    return .t.
  endfunc

  func oCIMENFIX_1_12.SetRadio()
    this.Parent.oContained.w_CIMENFIX=trim(this.Parent.oContained.w_CIMENFIX)
    this.value = ;
      iif(this.Parent.oContained.w_CIMENFIX=='S',1,;
      0)
  endfunc

  func oCIMENFIX_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME<>-1)
    endwith
   endif
  endfunc

  add object oCISEARMN_1_13 as StdCheck with uid="RWWFHXELEL",rtseq=13,rtrep=.f.,left=32, top=297, caption="Abilita ricerca voci men�",;
    ToolTipText = "Se attivo visualizza il controllo per la ricerca di voci di men�",;
    HelpContextID = 72643817,;
    cFormVar="w_CISEARMN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISEARMN_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCISEARMN_1_13.GetRadio()
    this.Parent.oContained.w_CISEARMN = this.RadioValue()
    return .t.
  endfunc

  func oCISEARMN_1_13.SetRadio()
    this.Parent.oContained.w_CISEARMN=trim(this.Parent.oContained.w_CISEARMN)
    this.value = ;
      iif(this.Parent.oContained.w_CISEARMN=='S',1,;
      0)
  endfunc

  func oCISEARMN_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME<>-1)
    endwith
   endif
  endfunc

  add object oCITOOLMN_1_14 as StdCheck with uid="RKRMTWUXJM",rtseq=14,rtrep=.f.,left=32, top=318, caption="Abilita tool men�",;
    ToolTipText = "Se attivo abilita il tool menu (visualizzabile tramite ctrl+t)",;
    HelpContextID = 218794775,;
    cFormVar="w_CITOOLMN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCITOOLMN_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCITOOLMN_1_14.GetRadio()
    this.Parent.oContained.w_CITOOLMN = this.RadioValue()
    return .t.
  endfunc

  func oCITOOLMN_1_14.SetRadio()
    this.Parent.oContained.w_CITOOLMN=trim(this.Parent.oContained.w_CITOOLMN)
    this.value = ;
      iif(this.Parent.oContained.w_CITOOLMN=='S',1,;
      0)
  endfunc

  add object oCIBCKGRD_1_15 as StdCheck with uid="NNTWTSEYPQ",rtseq=15,rtrep=.f.,left=286, top=276, caption="Gradiente di sfondo",;
    ToolTipText = "Se attivo mostra un gradiente di sfondo",;
    HelpContextID = 237204558,;
    cFormVar="w_CIBCKGRD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIBCKGRD_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIBCKGRD_1_15.GetRadio()
    this.Parent.oContained.w_CIBCKGRD = this.RadioValue()
    return .t.
  endfunc

  func oCIBCKGRD_1_15.SetRadio()
    this.Parent.oContained.w_CIBCKGRD=trim(this.Parent.oContained.w_CIBCKGRD)
    this.value = ;
      iif(this.Parent.oContained.w_CIBCKGRD=='S',1,;
      0)
  endfunc

  func oCIBCKGRD_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME <> -1 And .w_CITABMEN <> 'S')
    endwith
   endif
  endfunc

  add object oCIMRKGRD_1_16 as StdCheck with uid="FPPQZCEDUQ",rtseq=16,rtrep=.f.,left=286, top=297, caption="Puntatore del record",;
    ToolTipText = "Se attivo, visualizza il puntatore del record negli oggetti di tipo elenco e griglie di dati",;
    HelpContextID = 253654094,;
    cFormVar="w_CIMRKGRD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIMRKGRD_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIMRKGRD_1_16.GetRadio()
    this.Parent.oContained.w_CIMRKGRD = this.RadioValue()
    return .t.
  endfunc

  func oCIMRKGRD_1_16.SetRadio()
    this.Parent.oContained.w_CIMRKGRD=trim(this.Parent.oContained.w_CIMRKGRD)
    this.value = ;
      iif(this.Parent.oContained.w_CIMRKGRD=='S',1,;
      0)
  endfunc


  add object oObj_1_30 as cp_setobjprop with uid="SKPUZOAYUU",left=656, top=25, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIVTHEME",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_31 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=656, top=4, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tema",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_32 as cp_setobjprop with uid="SEWJLTLJEG",left=656, top=93, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CICPTBAR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_33 as cp_setobjprop with uid="TDZPUMQCSE",left=656, top=114, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIDSKBAR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_34 as cp_setobjprop with uid="OKPCDXPXJV",left=656, top=135, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CISTABAR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_35 as cp_setobjprop with uid="FPJAZRRGED",left=656, top=156, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CISHWBTN",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_36 as cp_setCtrlObjprop with uid="ICHWMLTXJJ",left=656, top=186, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Dimensione immagini toolbar",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_37 as cp_setobjprop with uid="AZRSZKUMQF",left=656, top=199, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CITBSIZE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_38 as cp_setobjprop with uid="GGASNOPFMI",left=656, top=264, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIMENFIX",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_39 as cp_setobjprop with uid="ZBBJCLDDXQ",left=656, top=285, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CISEARMN",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_40 as cp_setobjprop with uid="YZKVDVLWAI",left=656, top=306, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CITOOLMN",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_41 as cp_setobjprop with uid="NXFBOCAKBY",left=985, top=268, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIBCKGRD",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_42 as cp_setobjprop with uid="GMXJXNFRNC",left=985, top=288, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIMRKGRD",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_43 as cp_setCtrlObjprop with uid="RLJPSBDGYN",left=985, top=92, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Modalit� visualizzazione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_44 as cp_setCtrlObjprop with uid="GGTKXJGACH",left=656, top=69, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tool bar / status bar",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_45 as cp_setCtrlObjprop with uid="CJDZYWXBKS",left=985, top=69, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Form / Control",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_46 as cp_setCtrlObjprop with uid="YXBVBJYCQP",left=985, top=136, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tab",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_47 as cp_setCtrlObjprop with uid="XETOLEHYXC",left=985, top=183, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tema XP",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_48 as cp_setCtrlObjprop with uid="GERYBVQHBS",left=656, top=240, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Men�",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_49 as cp_setobjprop with uid="OQZMUDFGMA",left=985, top=108, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIMDIFRM",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_50 as cp_setobjprop with uid="ZDVHIOYSCI",left=772, top=93, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CICPTBAR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_51 as cp_setobjprop with uid="PQJGABBAFV",left=772, top=114, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CIDSKBAR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_52 as cp_setobjprop with uid="RMZCHLBOAN",left=772, top=135, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CISTABAR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_53 as cp_setobjprop with uid="ECIZDLTPHW",left=772, top=156, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CISHWBTN",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_54 as cp_setobjprop with uid="XKAOLCKGJH",left=772, top=264, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CIMENFIX",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_55 as cp_setobjprop with uid="NGHDEKYRRB",left=772, top=285, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CISEARMN",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_56 as cp_setobjprop with uid="CIFRAIAOPW",left=772, top=306, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CITOOLMN",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_57 as cp_setobjprop with uid="BCBWBMGOJK",left=1109, top=268, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CIBCKGRD",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_58 as cp_setobjprop with uid="OMNSWJZCEO",left=1109, top=288, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CIMRKGRD",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_59 as cp_setobjprop with uid="XJQGLQKXUE",left=772, top=25, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="RowSource",cObj="w_CIVTHEME",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_60 as cp_setobjprop with uid="FVTYCQKXJF",left=1109, top=108, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="RowSource",cObj="w_CIMDIFRM",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_61 as cp_setobjprop with uid="LGHFTQEFAG",left=985, top=199, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIXPTHEM",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_62 as cp_setobjprop with uid="OIADGBQAUV",left=1109, top=199, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="RowSource",cObj="w_CIXPTHEM",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_63 as cp_setobjprop with uid="QHXRDJZXKH",left=985, top=152, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CITABMEN",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_64 as cp_setobjprop with uid="XQXNIRDDGA",left=1109, top=152, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="RowSource",cObj="w_CITABMEN",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156

  add object ociwaitwd_1_70 as StdCheck with uid="VUZWGYUVMR",rtseq=58,rtrep=.f.,left=286, top=318, caption="Abilita tema wait window",;
    HelpContextID = 238720627,;
    cFormVar="w_ciwaitwd", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func ociwaitwd_1_70.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ociwaitwd_1_70.GetRadio()
    this.Parent.oContained.w_ciwaitwd = this.RadioValue()
    return .t.
  endfunc

  func ociwaitwd_1_70.SetRadio()
    this.Parent.oContained.w_ciwaitwd=trim(this.Parent.oContained.w_ciwaitwd)
    this.value = ;
      iif(this.Parent.oContained.w_ciwaitwd=='S',1,;
      0)
  endfunc


  add object oObj_1_71 as cp_setobjprop with uid="GIUJXRAIJC",left=986, top=308, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_ciwaitwd",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156


  add object oObj_1_72 as cp_setobjprop with uid="QQOHFRKHXV",left=1110, top=308, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_ciwaitwd",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 141364156

  add object oStr_1_17 as StdString with uid="LMQOAPKKXG",Visible=.t., Left=285, Top=203,;
    Alignment=0, Width=110, Height=18,;
    Caption="Tema XP"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="EGTKSXWAQH",Visible=.t., Left=26, Top=89,;
    Alignment=0, Width=124, Height=18,;
    Caption="Tool bar / status bar"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="TQMNZARRNJ",Visible=.t., Left=283, Top=89,;
    Alignment=0, Width=99, Height=18,;
    Caption="Form / Control"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="ZPUXLISGUW",Visible=.t., Left=32, Top=13,;
    Alignment=0, Width=130, Height=18,;
    Caption="Tema"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="MDCYWKOLAP",Visible=.t., Left=286, Top=112,;
    Alignment=0, Width=165, Height=18,;
    Caption="Modalit� visualizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="BSYVOZDLMT",Visible=.t., Left=286, Top=156,;
    Alignment=0, Width=94, Height=18,;
    Caption="Tab"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="JOSOXDUYZH",Visible=.t., Left=32, Top=203,;
    Alignment=0, Width=227, Height=18,;
    Caption="Dimensione immagini toolbar"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="IFRGPEXQCQ",Visible=.t., Left=26, Top=252,;
    Alignment=0, Width=124, Height=18,;
    Caption="Men�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_67 as StdString with uid="GYRYYVDMTN",Visible=.t., Left=283, Top=17,;
    Alignment=0, Width=99, Height=18,;
    Caption="Report"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="DRXCKNONKD",Visible=.t., Left=286, Top=39,;
    Alignment=0, Width=130, Height=18,;
    Caption="Motore report"  ;
  , bGlobalFont=.t.

  add object oBox_1_19 as StdBox with uid="TLVIIGADYM",left=8, top=106, width=223,height=1

  add object oBox_1_21 as StdBox with uid="IEBPSKRUQN",left=264, top=106, width=240,height=1

  add object oBox_1_27 as StdBox with uid="FXMQMUYWXW",left=8, top=269, width=223,height=1

  add object oBox_1_68 as StdBox with uid="BPIUPEJFUE",left=264, top=34, width=240,height=1
enddefine
define class tcp_setguiPag2 as StdContainer
  Width  = 531
  height = 343
  stdWidth  = 531
  stdheight = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOLORCTSEL_2_8 as StdField with uid="MOGOFUELKX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_COLORCTSEL", cQueryName = "COLORCTSEL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo dei campi selezionati",;
    HelpContextID = 99801664,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=56, InputMask=replicate('X',10)


  add object oBtn_2_9 as StdButton with uid="PEBTFBBJMK",left=234, top=56, width=22,height=19,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 19027268;
  , bGlobalFont=.t.

    proc oBtn_2_9.Click()
      with this.Parent.oContained
        .SetColor('CISELECL')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_10 as StdButton with uid="NXWXMXHDJW",left=234, top=85, width=22,height=19,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 19027268;
  , bGlobalFont=.t.

    proc oBtn_2_10.Click()
      with this.Parent.oContained
        .SetColor('CIEDTCOL')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_11 as StdButton with uid="UNCGHMEXBP",left=497, top=56, width=22,height=19,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 19027268;
  , bGlobalFont=.t.

    proc oBtn_2_11.Click()
      with this.Parent.oContained
        .SetColor('CIDSBLBC')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_12 as StdButton with uid="ZFYVKJJAHZ",left=497, top=85, width=22,height=19,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 19027268;
  , bGlobalFont=.t.

    proc oBtn_2_12.Click()
      with this.Parent.oContained
        .SetColor('CIDSBLFC')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_13 as StdButton with uid="FTMRQULGFT",left=234, top=202, width=22,height=19,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 19027268;
  , bGlobalFont=.t.

    proc oBtn_2_13.Click()
      with this.Parent.oContained
        .SetColor('CIGRIDCL')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_14 as StdButton with uid="RBEAVUZWHM",left=497, top=202, width=22,height=19,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 19027268;
  , bGlobalFont=.t.

    proc oBtn_2_14.Click()
      with this.Parent.oContained
        .SetColor('CISCRCOL')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_15 as StdButton with uid="KIEABAIYOW",left=234, top=230, width=22,height=19,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 19027268;
  , bGlobalFont=.t.

    proc oBtn_2_15.Click()
      with this.Parent.oContained
        .SetColor('CICOLZOM')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CIEVIZOM = 2 And version(5)>=900)
      endwith
    endif
  endfunc

  func oBtn_2_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CIEVIZOM <> 2)
     endwith
    endif
  endfunc

  add object oCIEVIZOM_2_16 as StdCheck with uid="ORRAUIDZIS",rtseq=25,rtrep=.f.,left=42, top=260, caption="Evidenzia la riga selezionata",;
    ToolTipText = "Se attivo, la riga selezionata negli oggetti di tipo elenco verr� evidenziata con un colore",;
    HelpContextID = 44665637,;
    cFormVar="w_CIEVIZOM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIEVIZOM_2_16.RadioValue()
    return(iif(this.value =1,2,;
    0))
  endfunc
  func oCIEVIZOM_2_16.GetRadio()
    this.Parent.oContained.w_CIEVIZOM = this.RadioValue()
    return .t.
  endfunc

  func oCIEVIZOM_2_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIEVIZOM==2,1,;
      0)
  endfunc

  func oCIEVIZOM_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (version(5)>=900)
    endwith
   endif
  endfunc


  add object oBtn_2_17 as StdButton with uid="FAQVHSXMZR",left=497, top=295, width=22,height=19,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 19027268;
  , bGlobalFont=.t.

    proc oBtn_2_17.Click()
      with this.Parent.oContained
        .SetColor('CIDTLCLR')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_19 as cp_setobjprop with uid="EQRJTNAASA",left=10, top=382, width=184,height=24,;
    caption='Sfondo Control editabili',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORCTED",;
    nPag=2;
    , ToolTipText = "Sfondo control editabili";
    , HelpContextID = 153658488


  add object oObj_2_20 as cp_setobjprop with uid="QXICVSSUMR",left=10, top=359, width=184,height=24,;
    caption='Sfondo Control Selezionato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORCTSEL",;
    nPag=2;
    , ToolTipText = "Sfondo control selezionato";
    , HelpContextID = 240044070

  add object oCOLORCTED_2_21 as StdField with uid="NPBORGGMIE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_COLORCTED", cQueryName = "COLORCTED",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo dei campi editabili",;
    HelpContextID = 99489888,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=85, InputMask=replicate('X',10)


  add object oObj_2_24 as cp_setobjprop with uid="UEBWXYJNIO",left=10, top=474, width=184,height=24,;
    caption='Colore Font control disabilitato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORFCTDSBL",;
    nPag=2;
    , ToolTipText = "Colore font control disabilitato";
    , HelpContextID = 251759825


  add object oObj_2_25 as cp_setobjprop with uid="EKKBIWECLA",left=10, top=451, width=184,height=24,;
    caption='Colore sfondo control disabilitato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORBCTDSBL",;
    nPag=2;
    , ToolTipText = "Colore sfondo control disabilitato";
    , HelpContextID = 70178519

  add object oCOLORFCTDSBL_2_26 as StdField with uid="KEPBBFTUIZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_COLORFCTDSBL", cQueryName = "COLORFCTDSBL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per il carattere dei campi disabilitati",;
    HelpContextID = 183847231,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=407, Top=85, InputMask=replicate('X',10)

  add object oCOLORBCTDSBL_2_27 as StdField with uid="HAWELLGLKM",rtseq=28,rtrep=.f.,;
    cFormVar = "w_COLORBCTDSBL", cQueryName = "COLORBCTDSBL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo dei campi disabilitati",;
    HelpContextID = 183847231,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=407, Top=56, InputMask=replicate('X',10)

  add object oCOLORGRD_2_34 as StdField with uid="KXBYKVRCAE",rtseq=29,rtrep=.f.,;
    cFormVar = "w_COLORGRD", cQueryName = "COLORGRD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per le orizzontali e verticali",;
    HelpContextID = 168962994,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=202, InputMask=replicate('X',10)

  add object oCOLORSCREEN_2_37 as StdField with uid="QHTHYIIPFX",rtseq=30,rtrep=.f.,;
    cFormVar = "w_COLORSCREEN", cQueryName = "COLORSCREEN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo del desktop",;
    HelpContextID = 104884767,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=407, Top=202, InputMask=replicate('X',10)

  add object oCOLORROWDTL_2_42 as StdField with uid="ZTFNGYYBDO",rtseq=31,rtrep=.f.,;
    cFormVar = "w_COLORROWDTL", cQueryName = "COLORROWDTL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo della riga selezionata in una griglia di dati",;
    HelpContextID = 104814971,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=407, Top=295, InputMask=replicate('X',10)


  add object oObj_2_43 as cp_setobjprop with uid="HTUAIRSZBU",left=10, top=497, width=184,height=24,;
    caption='Colore Screen',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORSCREEN",;
    nPag=2;
    , ToolTipText = "Colore screen";
    , HelpContextID = 53716980


  add object oObj_2_44 as cp_setobjprop with uid="HQPKMNSDWL",left=10, top=428, width=184,height=24,;
    caption='Colore Riga Selezionata Dtl',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORROWDTL",;
    nPag=2;
    , ToolTipText = "Colore screen";
    , HelpContextID = 163494631


  add object oObj_2_45 as cp_setobjprop with uid="SJXRMMHHHD",left=10, top=405, width=184,height=24,;
    caption='Colore linee griglie',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORGRD",;
    nPag=2;
    , ToolTipText = "Setta colore linee griglia";
    , HelpContextID = 93080990


  add object oObj_2_48 as cp_setCtrlObjprop with uid="LBGOBDWLGG",left=563, top=33, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Abilitati",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_49 as cp_setCtrlObjprop with uid="BRNYYLGECG",left=919, top=33, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Disabilitati",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_50 as cp_setCtrlObjprop with uid="FMTHXQBUNN",left=563, top=172, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Elenchi /zoom",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_51 as cp_setCtrlObjprop with uid="QHVGKEWOCK",left=919, top=172, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Desktop",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_52 as cp_setCtrlObjprop with uid="NVEMDKDGBL",left=563, top=95, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Selezionato:",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_53 as cp_setCtrlObjprop with uid="ZFKSCPIZYA",left=563, top=124, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Editabile:",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_54 as cp_setCtrlObjprop with uid="MQZMSZNUKL",left=919, top=95, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Sfondo:",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_55 as cp_setCtrlObjprop with uid="FXOZUDEZEV",left=919, top=124, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Carattere:",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_56 as cp_setCtrlObjprop with uid="PZYWEXURWC",left=563, top=202, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Linee griglie:",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_57 as cp_setCtrlObjprop with uid="FRYXCYQANL",left=919, top=295, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Riga selezionata:",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_58 as cp_setCtrlObjprop with uid="WAGTPYKNJL",left=919, top=265, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Griglie di dati",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_59 as cp_setCtrlObjprop with uid="JGSNFRQEXW",left=919, top=202, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Sfondo desktop:",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_60 as cp_setobjprop with uid="QDSZCFUUKG",left=681, top=95, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_COLORCTSEL",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_61 as cp_setobjprop with uid="IJQIHULLVL",left=681, top=124, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_COLORCTED",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_62 as cp_setobjprop with uid="BBABGKXHCK",left=681, top=202, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_COLORGRD",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_63 as cp_setobjprop with uid="CRGSPUXOWD",left=1038, top=95, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_COLORBCTDSBL",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_64 as cp_setobjprop with uid="TKDBGOMIXZ",left=1038, top=124, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_COLORFCTDSBL",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_65 as cp_setobjprop with uid="MXHBLEHCOL",left=1038, top=202, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_COLORSCREEN",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_66 as cp_setobjprop with uid="LLOTTPAQGN",left=1038, top=295, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_COLORROWDTL",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oBtn_2_68 as StdButton with uid="KJTASBKMYO",left=283, top=132, width=22,height=22,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 19027268;
  , bGlobalFont=.t.

    proc oBtn_2_68.Click()
      with this.Parent.oContained
        .SetColor('CIOBLCOL')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_68.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ciobl_on='S')
      endwith
    endif
  endfunc


  add object oObj_2_69 as cp_setobjprop with uid="HKZXMTAGRT",left=10, top=519, width=184,height=24,;
    caption='Campi obbligatori',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORCTOB",;
    nPag=2;
    , ToolTipText = "Campi obbligatori";
    , HelpContextID = 150959910

  add object oCOLORCTOB_2_71 as StdField with uid="XURQDVYPNZ",rtseq=54,rtrep=.f.,;
    cFormVar = "w_COLORCTOB", cQueryName = "COLORCTOB",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per evidenziare i campi obbligatori",;
    HelpContextID = 99489536,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=196, Top=132, InputMask=replicate('X',10)


  add object oObj_2_72 as cp_setCtrlObjprop with uid="MUTAYCSXTC",left=562, top=144, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Evidenzia obbligatori:",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_73 as cp_setobjprop with uid="RFPQEWZFUF",left=680, top=144, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_COLORCTSEL",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object ociobl_on_2_74 as StdCombo with uid="YGCPCFYLBX",rtseq=55,rtrep=.f.,left=144,top=133,width=47,height=21;
    , HelpContextID = 21141259;
    , cFormVar="w_ciobl_on",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func ociobl_on_2_74.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func ociobl_on_2_74.GetRadio()
    this.Parent.oContained.w_ciobl_on = this.RadioValue()
    return .t.
  endfunc

  func ociobl_on_2_74.SetRadio()
    this.Parent.oContained.w_ciobl_on=trim(this.Parent.oContained.w_ciobl_on)
    this.value = ;
      iif(this.Parent.oContained.w_ciobl_on=='S',1,;
      iif(this.Parent.oContained.w_ciobl_on=='N',2,;
      0))
  endfunc

  add object oCOLORZOM_2_75 as StdField with uid="RIGFTLDQIM",rtseq=56,rtrep=.f.,;
    cFormVar = "w_COLORZOM", cQueryName = "COLORZOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo della riga selezionata in un elenco",;
    HelpContextID = 168962853,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=230, InputMask=replicate('X',10)

  func oCOLORZOM_2_75.mHide()
    with this.Parent.oContained
      return (.w_CIEVIZOM <> 2)
    endwith
  endfunc


  add object oObj_2_78 as cp_setobjprop with uid="RAKYNHALNR",left=10, top=542, width=184,height=24,;
    caption='Evidenza riga selezionata negli zoom',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORZOM",;
    nPag=2;
    , ToolTipText = "Evidenza riga selezionata negli zoom";
    , HelpContextID = 44752877


  add object oObj_2_79 as cp_setobjprop with uid="ZCCGKRCAEV",left=563, top=288, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIEVIZOM",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_80 as cp_setobjprop with uid="QTQZNCCEFV",left=679, top=288, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_CIEVIZOM",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_81 as cp_setCtrlObjprop with uid="CDNLIVIKKB",left=563, top=318, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Colore riga selezionata:",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156


  add object oObj_2_82 as cp_setobjprop with uid="JEDBAOICUP",left=679, top=318, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_COLORZOM",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 141364156

  add object oStr_2_18 as StdString with uid="CEKXRNXBRY",Visible=.t., Left=36, Top=33,;
    Alignment=0, Width=177, Height=18,;
    Caption="Abilitati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_22 as StdString with uid="DOZEKZBHRE",Visible=.t., Left=5, Top=85,;
    Alignment=1, Width=134, Height=18,;
    Caption="Editabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="SIJNQEVDJS",Visible=.t., Left=5, Top=56,;
    Alignment=1, Width=134, Height=18,;
    Caption="Selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="WBTKYRDXRM",Visible=.t., Left=271, Top=85,;
    Alignment=1, Width=134, Height=18,;
    Caption="Carattere:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="KZYMQJXXIO",Visible=.t., Left=271, Top=56,;
    Alignment=1, Width=134, Height=18,;
    Caption="Sfondo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="KOJIFVHWEB",Visible=.t., Left=282, Top=33,;
    Alignment=0, Width=177, Height=18,;
    Caption="Disabilitati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_33 as StdString with uid="AHEXUSEGBY",Visible=.t., Left=5, Top=202,;
    Alignment=1, Width=134, Height=18,;
    Caption="Linee griglie:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="OYNODWFTEC",Visible=.t., Left=36, Top=172,;
    Alignment=0, Width=177, Height=18,;
    Caption="Elenchi /zoom"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_38 as StdString with uid="WVSMJOVFHN",Visible=.t., Left=271, Top=202,;
    Alignment=1, Width=134, Height=18,;
    Caption="Sfondo desktop:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="MRONQBDVIX",Visible=.t., Left=282, Top=172,;
    Alignment=0, Width=177, Height=18,;
    Caption="Desktop"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_41 as StdString with uid="KAFSEBDEFM",Visible=.t., Left=271, Top=295,;
    Alignment=1, Width=134, Height=18,;
    Caption="Riga selezionata:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="TZEEBVPTMD",Visible=.t., Left=282, Top=265,;
    Alignment=0, Width=177, Height=18,;
    Caption="Griglie di dati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_70 as StdString with uid="QZMKQOYXLQ",Visible=.t., Left=5, Top=135,;
    Alignment=1, Width=134, Height=18,;
    Caption="Evidenzia obbligatori:"  ;
  , bGlobalFont=.t.

  add object oStr_2_76 as StdString with uid="YGHAYYNNKF",Visible=.t., Left=1, Top=230,;
    Alignment=1, Width=138, Height=18,;
    Caption="Colore riga selezionata:"  ;
  , bGlobalFont=.t.

  func oStr_2_76.mHide()
    with this.Parent.oContained
      return (.w_CIEVIZOM <> 2)
    endwith
  endfunc

  add object oBox_2_31 as StdBox with uid="UVDBTBFTNC",left=20, top=50, width=248,height=1

  add object oBox_2_32 as StdBox with uid="FMNXZMMLUY",left=278, top=50, width=248,height=1

  add object oBox_2_36 as StdBox with uid="WBEAFAEBLG",left=20, top=189, width=248,height=1

  add object oBox_2_40 as StdBox with uid="TPWVWCSGLL",left=278, top=189, width=248,height=1

  add object oBox_2_47 as StdBox with uid="GKWHLPOCVK",left=278, top=282, width=248,height=1
enddefine
define class tcp_setguiPag3 as StdContainer
  Width  = 531
  height = 343
  stdWidth  = 531
  stdheight = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCILBLFNM_3_1 as StdTableComboFont with uid="XEWFUYKPVZ",rtseq=32,rtrep=.f.,left=127,top=48,width=164,height=21;
    , ToolTipText = "Font da utilizzare per la etichette, checkbox, radiobox";
    , HelpContextID = 253588698;
    , cFormVar="w_CILBLFNM",tablefilter="", bObbl = .f. , nPag = 3;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCITXTFNM_3_2 as StdTableComboFont with uid="AEBOTFGNNC",rtseq=33,rtrep=.f.,left=127,top=82,width=164,height=21;
    , ToolTipText = "Font da utilizzare per textbox, memo";
    , HelpContextID = 142963930;
    , cFormVar="w_CITXTFNM",tablefilter="", bObbl = .f. , nPag = 3;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCICBXFNM_3_3 as StdTableComboFont with uid="NXNLBIHBGE",rtseq=34,rtrep=.f.,left=127,top=116,width=164,height=21;
    , ToolTipText = "Font da utilizzare per combobox";
    , HelpContextID = 185890010;
    , cFormVar="w_CICBXFNM",tablefilter="", bObbl = .f. , nPag = 3;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCIBTNFNM_3_4 as StdTableComboFont with uid="FYKEPEUGQU",rtseq=35,rtrep=.f.,left=127,top=150,width=164,height=21;
    , ToolTipText = "Font da utilizzare per i bottoni";
    , HelpContextID = 36926682;
    , cFormVar="w_CIBTNFNM",tablefilter="", bObbl = .f. , nPag = 3;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCIGRDFNM_3_5 as StdTableComboFont with uid="JJSHBWWONC",rtseq=36,rtrep=.f.,left=127,top=184,width=164,height=21;
    , ToolTipText = "Font da utilizzare per gli zoom";
    , HelpContextID = 135820506;
    , cFormVar="w_CIGRDFNM",tablefilter="", bObbl = .f. , nPag = 3;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCIPAGFNM_3_6 as StdTableComboFont with uid="LCIYMQPBZY",rtseq=37,rtrep=.f.,left=127,top=218,width=164,height=21;
    , ToolTipText = "Font da utilizzare per le tab";
    , HelpContextID = 168916186;
    , cFormVar="w_CIPAGFNM",tablefilter="", bObbl = .f. , nPag = 3;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  add object oCILBLFSZ_3_13 as StdField with uid="KTPMHIBSVZ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CILBLFSZ", cQueryName = "CILBLFSZ",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font etichette",;
    HelpContextID = 253588911,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=308, Top=48, cSayPict='"999"', cGetPict='"999"'

  func oCILBLFSZ_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CILBLFNM) and TestFont('L', .w_CILBLFNM, .w_CILBLFSZ)<>-1 And !Empty(.w_CILBLFSZ))
      if bRes and !(TestFont('L', .w_CILBLFNM, .w_CILBLFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCITXTFSZ_3_15 as StdField with uid="MZYXYCYXKG",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CITXTFSZ", cQueryName = "CITXTFSZ",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font campi",;
    HelpContextID = 142964143,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=308, Top=82, cSayPict='"999"', cGetPict='"999"'

  func oCITXTFSZ_3_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CITXTFNM) and TestFont('T', .w_CITXTFNM, .w_CITXTFSZ)<>-1 And !Empty(.w_CITXTFSZ))
      if bRes and !(TestFont('T', .w_CITXTFNM, .w_CITXTFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCICBXFSZ_3_16 as StdField with uid="ELFXAWJCPG",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CICBXFSZ", cQueryName = "CICBXFSZ",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font combobox",;
    HelpContextID = 185890223,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=308, Top=116, cSayPict='"999"', cGetPict='"999"'

  func oCICBXFSZ_3_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CICBXFNM) and TestFont('C', .w_CICBXFNM, .w_CICBXFSZ)<>-1 And !Empty(.w_CICBXFSZ))
      if bRes and !(TestFont('C', .w_CICBXFNM, .w_CICBXFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCIBTNFSZ_3_17 as StdField with uid="CDYSADIXOB",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CIBTNFSZ", cQueryName = "CIBTNFSZ",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font bottoni",;
    HelpContextID = 36926895,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=308, Top=150, cSayPict='"999"', cGetPict='"999"'

  func oCIBTNFSZ_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIBTNFNM) and TestFont('B', .w_CIBTNFNM, .w_CIBTNFSZ)<>-1 And !Empty(.w_CIBTNFSZ))
      if bRes and !(TestFont('B', .w_CIBTNFNM, .w_CIBTNFSZ)=1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCIGRDFSZ_3_18 as StdField with uid="HAYREGIFUQ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CIGRDFSZ", cQueryName = "CIGRDFSZ",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font zoom",;
    HelpContextID = 135820719,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=308, Top=184, cSayPict='"999"', cGetPict='"999"'

  func oCIGRDFSZ_3_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIGRDFNM) and TestFont('G', .w_CIGRDFNM, .w_CIGRDFSZ)<>-1 And !Empty(.w_CIGRDFSZ))
      if bRes and !(TestFont('G', .w_CIGRDFNM, .w_CIGRDFSZ)=1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCIPAGFSZ_3_19 as StdField with uid="TZTVIGUUPA",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CIPAGFSZ", cQueryName = "CIPAGFSZ",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font tab",;
    HelpContextID = 168916399,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=308, Top=218, cSayPict='"999"', cGetPict='"999"'

  func oCIPAGFSZ_3_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIPAGFNM) and TestFont('P', .w_CIPAGFNM, .w_CIPAGFSZ)<>-1 And !Empty(.w_CIPAGFSZ))
      if bRes and !(TestFont('P', .w_CIPAGFNM, .w_CIPAGFSZ)=1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc


  add object oObj_3_20 as cp_setCtrlObjprop with uid="ZMYOFSIQGW",left=543, top=48, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Font etichette:",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_21 as cp_setCtrlObjprop with uid="AYHZAWCQDZ",left=543, top=82, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Font campi:",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_22 as cp_setCtrlObjprop with uid="IOTZXVKKVG",left=543, top=116, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Font combobox:",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_23 as cp_setCtrlObjprop with uid="FMFTEUHYJN",left=543, top=150, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Font bottoni:",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_24 as cp_setCtrlObjprop with uid="MDQQBYPDUK",left=543, top=184, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Font zoom:",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_25 as cp_setCtrlObjprop with uid="ZDTSAPRYON",left=543, top=218, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Font tab:",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_26 as cp_setCtrlObjprop with uid="XWXGWGZIMD",left=846, top=28, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Dimensione",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_27 as cp_setobjprop with uid="IQEMRMQANG",left=657, top=48, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CILBLFNM",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_28 as cp_setobjprop with uid="QEHANLBBKT",left=657, top=82, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CITXTFNM",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_29 as cp_setobjprop with uid="DZTCUEIASV",left=657, top=150, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIBTNFNM",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_30 as cp_setobjprop with uid="EMCYNRGSCM",left=657, top=184, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIGRDFNM",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_31 as cp_setobjprop with uid="HDTOVOLKON",left=657, top=218, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIPAGFNM",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_32 as cp_setobjprop with uid="BEJKRFLNNW",left=657, top=116, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CICBXFNM",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_33 as cp_setobjprop with uid="PNHQAUNKJM",left=845, top=51, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CILBLFSZ",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_34 as cp_setobjprop with uid="ITZDGQAKSA",left=845, top=85, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CITXTFSZ",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_35 as cp_setobjprop with uid="EFHWJCDOWQ",left=845, top=153, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIBTNFSZ",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_36 as cp_setobjprop with uid="DBJTCIMGIM",left=845, top=187, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIGRDFSZ",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_37 as cp_setobjprop with uid="GLONJNIPXT",left=845, top=221, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIPAGFSZ",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156


  add object oObj_3_38 as cp_setobjprop with uid="BDNVGTJPYL",left=845, top=119, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CICBXFSZ",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 141364156

  add object ocilblfit_3_39 as StdCheck with uid="ZVWDSPGSBR",rtseq=59,rtrep=.f.,left=396, top=48, caption="Italic",;
    HelpContextID = 247490715,;
    cFormVar="w_cilblfit", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocilblfit_3_39.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocilblfit_3_39.GetRadio()
    this.Parent.oContained.w_cilblfit = this.RadioValue()
    return .t.
  endfunc

  func ocilblfit_3_39.SetRadio()
    this.Parent.oContained.w_cilblfit=trim(this.Parent.oContained.w_cilblfit)
    this.value = ;
      iif(this.Parent.oContained.w_cilblfit=='S',1,;
      0)
  endfunc

  add object ocitxtfit_3_40 as StdCheck with uid="PCDBEISXPW",rtseq=60,rtrep=.f.,left=396, top=82, caption="Italic",;
    HelpContextID = 89680027,;
    cFormVar="w_citxtfit", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocitxtfit_3_40.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocitxtfit_3_40.GetRadio()
    this.Parent.oContained.w_citxtfit = this.RadioValue()
    return .t.
  endfunc

  func ocitxtfit_3_40.SetRadio()
    this.Parent.oContained.w_citxtfit=trim(this.Parent.oContained.w_citxtfit)
    this.value = ;
      iif(this.Parent.oContained.w_citxtfit=='S',1,;
      0)
  endfunc

  add object ocicbxfit_3_41 as StdCheck with uid="PUGVECLQRN",rtseq=61,rtrep=.f.,left=396, top=116, caption="Italic",;
    HelpContextID = 46753947,;
    cFormVar="w_cicbxfit", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocicbxfit_3_41.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocicbxfit_3_41.GetRadio()
    this.Parent.oContained.w_cicbxfit = this.RadioValue()
    return .t.
  endfunc

  func ocicbxfit_3_41.SetRadio()
    this.Parent.oContained.w_cicbxfit=trim(this.Parent.oContained.w_cicbxfit)
    this.value = ;
      iif(this.Parent.oContained.w_cicbxfit=='S',1,;
      0)
  endfunc

  add object ocibtnfit_3_42 as StdCheck with uid="RSYBGVOCWW",rtseq=62,rtrep=.f.,left=396, top=150, caption="Italic",;
    HelpContextID = 195717275,;
    cFormVar="w_cibtnfit", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocibtnfit_3_42.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocibtnfit_3_42.GetRadio()
    this.Parent.oContained.w_cibtnfit = this.RadioValue()
    return .t.
  endfunc

  func ocibtnfit_3_42.SetRadio()
    this.Parent.oContained.w_cibtnfit=trim(this.Parent.oContained.w_cibtnfit)
    this.value = ;
      iif(this.Parent.oContained.w_cibtnfit=='S',1,;
      0)
  endfunc

  add object ocigrdfit_3_43 as StdCheck with uid="XXQSRCESWM",rtseq=63,rtrep=.f.,left=396, top=184, caption="Italic",;
    HelpContextID = 96823451,;
    cFormVar="w_cigrdfit", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocigrdfit_3_43.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocigrdfit_3_43.GetRadio()
    this.Parent.oContained.w_cigrdfit = this.RadioValue()
    return .t.
  endfunc

  func ocigrdfit_3_43.SetRadio()
    this.Parent.oContained.w_cigrdfit=trim(this.Parent.oContained.w_cigrdfit)
    this.value = ;
      iif(this.Parent.oContained.w_cigrdfit=='S',1,;
      0)
  endfunc

  add object ocipagfit_3_44 as StdCheck with uid="NWDWWQMMDF",rtseq=64,rtrep=.f.,left=396, top=218, caption="Italic",;
    HelpContextID = 63727771,;
    cFormVar="w_cipagfit", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocipagfit_3_44.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocipagfit_3_44.GetRadio()
    this.Parent.oContained.w_cipagfit = this.RadioValue()
    return .t.
  endfunc

  func ocipagfit_3_44.SetRadio()
    this.Parent.oContained.w_cipagfit=trim(this.Parent.oContained.w_cipagfit)
    this.value = ;
      iif(this.Parent.oContained.w_cipagfit=='S',1,;
      0)
  endfunc

  add object ocilblfbo_3_45 as StdCheck with uid="YLOPRWXCNT",rtseq=65,rtrep=.f.,left=456, top=48, caption="Bold",;
    HelpContextID = 247490802,;
    cFormVar="w_cilblfbo", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocilblfbo_3_45.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocilblfbo_3_45.GetRadio()
    this.Parent.oContained.w_cilblfbo = this.RadioValue()
    return .t.
  endfunc

  func ocilblfbo_3_45.SetRadio()
    this.Parent.oContained.w_cilblfbo=trim(this.Parent.oContained.w_cilblfbo)
    this.value = ;
      iif(this.Parent.oContained.w_cilblfbo=='S',1,;
      0)
  endfunc

  add object ocitxtfbo_3_46 as StdCheck with uid="JCOBAWCLQV",rtseq=66,rtrep=.f.,left=456, top=82, caption="Bold",;
    HelpContextID = 89680114,;
    cFormVar="w_citxtfbo", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocitxtfbo_3_46.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocitxtfbo_3_46.GetRadio()
    this.Parent.oContained.w_citxtfbo = this.RadioValue()
    return .t.
  endfunc

  func ocitxtfbo_3_46.SetRadio()
    this.Parent.oContained.w_citxtfbo=trim(this.Parent.oContained.w_citxtfbo)
    this.value = ;
      iif(this.Parent.oContained.w_citxtfbo=='S',1,;
      0)
  endfunc

  add object ocicbxfbo_3_47 as StdCheck with uid="CAVJHIUWRT",rtseq=67,rtrep=.f.,left=456, top=116, caption="Bold",;
    HelpContextID = 46754034,;
    cFormVar="w_cicbxfbo", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocicbxfbo_3_47.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocicbxfbo_3_47.GetRadio()
    this.Parent.oContained.w_cicbxfbo = this.RadioValue()
    return .t.
  endfunc

  func ocicbxfbo_3_47.SetRadio()
    this.Parent.oContained.w_cicbxfbo=trim(this.Parent.oContained.w_cicbxfbo)
    this.value = ;
      iif(this.Parent.oContained.w_cicbxfbo=='S',1,;
      0)
  endfunc

  add object ocibtnfbo_3_48 as StdCheck with uid="JZCIWULNQJ",rtseq=68,rtrep=.f.,left=456, top=150, caption="Bold",;
    HelpContextID = 195717362,;
    cFormVar="w_cibtnfbo", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocibtnfbo_3_48.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocibtnfbo_3_48.GetRadio()
    this.Parent.oContained.w_cibtnfbo = this.RadioValue()
    return .t.
  endfunc

  func ocibtnfbo_3_48.SetRadio()
    this.Parent.oContained.w_cibtnfbo=trim(this.Parent.oContained.w_cibtnfbo)
    this.value = ;
      iif(this.Parent.oContained.w_cibtnfbo=='S',1,;
      0)
  endfunc

  add object ocigrdfbo_3_49 as StdCheck with uid="RIPYOINPFE",rtseq=69,rtrep=.f.,left=456, top=184, caption="Bold",;
    HelpContextID = 96823538,;
    cFormVar="w_cigrdfbo", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocigrdfbo_3_49.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocigrdfbo_3_49.GetRadio()
    this.Parent.oContained.w_cigrdfbo = this.RadioValue()
    return .t.
  endfunc

  func ocigrdfbo_3_49.SetRadio()
    this.Parent.oContained.w_cigrdfbo=trim(this.Parent.oContained.w_cigrdfbo)
    this.value = ;
      iif(this.Parent.oContained.w_cigrdfbo=='S',1,;
      0)
  endfunc

  add object ocipagfbo_3_50 as StdCheck with uid="PCCERGZQZJ",rtseq=70,rtrep=.f.,left=456, top=218, caption="Bold",;
    HelpContextID = 63727858,;
    cFormVar="w_cipagfbo", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ocipagfbo_3_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocipagfbo_3_50.GetRadio()
    this.Parent.oContained.w_cipagfbo = this.RadioValue()
    return .t.
  endfunc

  func ocipagfbo_3_50.SetRadio()
    this.Parent.oContained.w_cipagfbo=trim(this.Parent.oContained.w_cipagfbo)
    this.value = ;
      iif(this.Parent.oContained.w_cipagfbo=='S',1,;
      0)
  endfunc

  add object oStr_3_7 as StdString with uid="ADMMUSPOPS",Visible=.t., Left=22, Top=48,;
    Alignment=1, Width=100, Height=18,;
    Caption="Font etichette:"  ;
  , bGlobalFont=.t.

  add object oStr_3_8 as StdString with uid="BNIFNBVKCO",Visible=.t., Left=22, Top=82,;
    Alignment=1, Width=100, Height=17,;
    Caption="Font campi:"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="CQKRHDMKUH",Visible=.t., Left=22, Top=150,;
    Alignment=1, Width=100, Height=17,;
    Caption="Font bottoni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="XKMJQKCKEI",Visible=.t., Left=10, Top=116,;
    Alignment=1, Width=112, Height=17,;
    Caption="Font combobox:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="UUIYYLBVDV",Visible=.t., Left=22, Top=184,;
    Alignment=1, Width=100, Height=17,;
    Caption="Font zoom:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="TNZYLLZWTX",Visible=.t., Left=22, Top=218,;
    Alignment=1, Width=100, Height=17,;
    Caption="Font tab:"  ;
  , bGlobalFont=.t.

  add object oStr_3_14 as StdString with uid="EYUYQDCQFF",Visible=.t., Left=280, Top=29,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione"  ;
  , bGlobalFont=.t.
enddefine
define class tcp_setguiPag4 as StdContainer
  Width  = 531
  height = 343
  stdWidth  = 531
  stdheight = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCIDSKMEN_4_1 as StdCombo with uid="SLYGQQBJHE",rtseq=44,rtrep=.f.,left=238,top=39,width=186,height=21;
    , RowSourceType = 1;
    , ToolTipText = "Se attivo abilita il desktop menu (visualizzabile tramite ctrl+d)";
    , HelpContextID = 14322463;
    , cFormVar="w_CIDSKMEN",RowSource=""+"Abilita desktop menu,"+"Disabilita desktop menu,"+"Apri desktop menu all'ingresso", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCIDSKMEN_4_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'H',;
    iif(this.value =3,'O',;
    space(1)))))
  endfunc
  func oCIDSKMEN_4_1.GetRadio()
    this.Parent.oContained.w_CIDSKMEN = this.RadioValue()
    return .t.
  endfunc

  func oCIDSKMEN_4_1.SetRadio()
    this.Parent.oContained.w_CIDSKMEN=trim(this.Parent.oContained.w_CIDSKMEN)
    this.value = ;
      iif(this.Parent.oContained.w_CIDSKMEN=='S',1,;
      iif(this.Parent.oContained.w_CIDSKMEN=='H',2,;
      iif(this.Parent.oContained.w_CIDSKMEN=='O',3,;
      0)))
  endfunc

  func oCIDSKMEN_4_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME <> -1)
    endwith
   endif
  endfunc


  add object oCINAVSTA_4_2 as StdCombo with uid="QUKGWVIOIL",rtseq=45,rtrep=.f.,left=238,top=68,width=186,height=21;
    , RowSourceType = 1;
    , ToolTipText = "Stato del DeskMenu all'avvio";
    , HelpContextID = 152007712;
    , cFormVar="w_CINAVSTA",RowSource=""+"Aperta,"+"Chiusa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCINAVSTA_4_2.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oCINAVSTA_4_2.GetRadio()
    this.Parent.oContained.w_CINAVSTA = this.RadioValue()
    return .t.
  endfunc

  func oCINAVSTA_4_2.SetRadio()
    this.Parent.oContained.w_CINAVSTA=trim(this.Parent.oContained.w_CINAVSTA)
    this.value = ;
      iif(this.Parent.oContained.w_CINAVSTA=='A',1,;
      iif(this.Parent.oContained.w_CINAVSTA=='C',2,;
      0))
  endfunc


  add object oCINAVNUB_4_3 as StdCombo with uid="AJAKYKZJWK",rtseq=46,rtrep=.f.,left=239,top=97,width=62,height=21;
    , RowSourceType = 1;
    , ToolTipText = "Importa il numero massimo di pulsanti visualizzati dal desktop menu";
    , HelpContextID = 116427727;
    , cFormVar="w_CINAVNUB",RowSource=""+"1,"+"2,"+"3,"+"4,"+"5,"+"6,"+"7,"+"8,"+"9,"+"10,"+"11,"+"12", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCINAVNUB_4_3.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    iif(this.value =7,7,;
    iif(this.value =8,8,;
    iif(this.value =9,9,;
    iif(this.value =10,10,;
    iif(this.value =11,11,;
    iif(this.value =12,12,;
    0)))))))))))))
  endfunc
  func oCINAVNUB_4_3.GetRadio()
    this.Parent.oContained.w_CINAVNUB = this.RadioValue()
    return .t.
  endfunc

  func oCINAVNUB_4_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CINAVNUB==1,1,;
      iif(this.Parent.oContained.w_CINAVNUB==2,2,;
      iif(this.Parent.oContained.w_CINAVNUB==3,3,;
      iif(this.Parent.oContained.w_CINAVNUB==4,4,;
      iif(this.Parent.oContained.w_CINAVNUB==5,5,;
      iif(this.Parent.oContained.w_CINAVNUB==6,6,;
      iif(this.Parent.oContained.w_CINAVNUB==7,7,;
      iif(this.Parent.oContained.w_CINAVNUB==8,8,;
      iif(this.Parent.oContained.w_CINAVNUB==9,9,;
      iif(this.Parent.oContained.w_CINAVNUB==10,10,;
      iif(this.Parent.oContained.w_CINAVNUB==11,11,;
      iif(this.Parent.oContained.w_CINAVNUB==12,12,;
      0))))))))))))
  endfunc

  add object oCINAVDIM_4_4 as StdField with uid="MUFZHXFLFE",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CINAVDIM", cQueryName = "CINAVDIM",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione iniziale in pixel del desktop menu",;
    HelpContextID = 152007893,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=239, Top=127, cSayPict='"9999999999"', cGetPict='"9999999999"'


  add object oCIMNAFNM_4_5 as StdTableComboFont with uid="VDGVFJSWLZ",rtseq=48,rtrep=.f.,left=168,top=184,width=164,height=21;
    , ToolTipText = "Font da utilizzare per la sezione menu navigator";
    , HelpContextID = 81687770;
    , cFormVar="w_CIMNAFNM",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  add object oCIMNAFSZ_4_6 as StdField with uid="BFGXYNJXAE",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CIMNAFSZ", cQueryName = "CIMNAFSZ",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font per la sezione menu navigator",;
    HelpContextID = 81687983,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=444, Top=184, cSayPict='"999"', cGetPict='"999"'

  func oCIMNAFSZ_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIMNAFNM) and TestFont('L', .w_CIMNAFNM, .w_CIMNAFSZ)<>-1 And !Empty(.w_CIMNAFSZ))
      if bRes and !(TestFont('L', .w_CIMNAFNM, .w_CIMNAFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc


  add object oCIWMAFNM_4_7 as StdTableComboFont with uid="CEJELALOQK",rtseq=50,rtrep=.f.,left=168,top=218,width=164,height=21;
    , ToolTipText = "Font da utilizzare per la sezione Windows manager";
    , HelpContextID = 81294554;
    , cFormVar="w_CIWMAFNM",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  add object oCIWMAFSZ_4_8 as StdField with uid="LNZPGHJJVM",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CIWMAFSZ", cQueryName = "CIWMAFSZ",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font per la sezione Windows manager",;
    HelpContextID = 81294767,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=444, Top=218, cSayPict='"999"', cGetPict='"999"'

  func oCIWMAFSZ_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIWMAFNM) and TestFont('L', .w_CIWMAFNM, .w_CIWMAFSZ)<>-1 And !Empty(.w_CIWMAFSZ))
      if bRes and !(TestFont('L', .w_CIWMAFNM, .w_CIWMAFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc


  add object oObj_4_17 as cp_setCtrlObjprop with uid="JCLGXQXYZA",left=553, top=37, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Desktop men�:",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_18 as cp_setCtrlObjprop with uid="DWHFTOLGSG",left=553, top=69, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Stato Desktop menu all'avvio:",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_19 as cp_setCtrlObjprop with uid="QYHFNOKUDE",left=553, top=100, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Numero di bottoni visualizzabili:",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_20 as cp_setCtrlObjprop with uid="KZRRRZPBOU",left=553, top=129, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Dimensione iniziale desktop menu:",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_21 as cp_setCtrlObjprop with uid="EHWSUFKYBO",left=553, top=184, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Font menu navigator:",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_22 as cp_setCtrlObjprop with uid="HGYCWMGPUG",left=906, top=184, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Dimensione:",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_23 as cp_setCtrlObjprop with uid="AYTLKQLVQP",left=906, top=218, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Dimensione:",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_24 as cp_setCtrlObjprop with uid="WEXSRJXEXQ",left=553, top=217, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Font windows manager:",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_25 as cp_setobjprop with uid="JCDJAXNMHH",left=667, top=37, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIDSKMEN",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_26 as cp_setobjprop with uid="EHRUQXQPWU",left=667, top=68, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CINAVSTA",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_27 as cp_setobjprop with uid="UBOXHLHCUC",left=667, top=97, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CINAVNUB",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_28 as cp_setobjprop with uid="UJELONDHMM",left=667, top=127, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CINAVDIM",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_29 as cp_setobjprop with uid="TCTABLVLRN",left=667, top=184, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIMNAFNM",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_30 as cp_setobjprop with uid="SKQFBTJCXH",left=667, top=218, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIWMAFNM",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_31 as cp_setobjprop with uid="MWCNVEAGCR",left=1021, top=184, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIMNAFSZ",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_32 as cp_setobjprop with uid="MNPOQPMLDY",left=1021, top=218, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CIWMAFSZ",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_33 as cp_setobjprop with uid="QCZBORRIKB",left=786, top=37, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="RowSource",cObj="w_CIDSKMEN",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156


  add object oObj_4_34 as cp_setobjprop with uid="QLGPJBERJD",left=786, top=68, width=107,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="RowSource",cObj="w_CINAVSTA",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 141364156

  add object oStr_4_9 as StdString with uid="RBCVPKTCZB",Visible=.t., Left=7, Top=38,;
    Alignment=1, Width=226, Height=18,;
    Caption="Desktop men�:"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="JMOSAGWQOA",Visible=.t., Left=4, Top=184,;
    Alignment=1, Width=159, Height=18,;
    Caption="Font menu navigator:"  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="UBKQDBPFVW",Visible=.t., Left=4, Top=217,;
    Alignment=1, Width=159, Height=18,;
    Caption="Font windows manager:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="YVNYKNFGIQ",Visible=.t., Left=336, Top=184,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_13 as StdString with uid="ESWVGHVNDV",Visible=.t., Left=336, Top=218,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="UZCLKWTABU",Visible=.t., Left=7, Top=100,;
    Alignment=1, Width=226, Height=18,;
    Caption="Numero di bottoni visualizzabili:"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="VRGRVVMDTK",Visible=.t., Left=7, Top=69,;
    Alignment=1, Width=226, Height=18,;
    Caption="Stato Desktop menu all'avvio:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="HQOOUXWMFP",Visible=.t., Left=7, Top=129,;
    Alignment=1, Width=226, Height=18,;
    Caption="Dimensione iniziale desktop menu:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_setgui','cpsetgui','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".usrcode=cpsetgui.usrcode";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_setgui
define class StdTableComboFont as StdTableCombo
  proc Init
  	IF VARTYPE(this.bNoBackColor)='U'
		  This.backcolor = i_nEBackColor
	  ENDIF
    this.ToolTipText=cp_Translate(this.ToolTipText)
    local l_numf, l_i
    AFONT(this.combovalues)
    This.nValues = ALEN(this.combovalues)
    l_i = 1
    do while l_i <= This.nValues
        this.AddItem(this.combovalues[l_i])
        l_i = l_i + 1
    enddo
    This.SetFont()
  endproc
enddefine
* --- Fine Area Manuale
