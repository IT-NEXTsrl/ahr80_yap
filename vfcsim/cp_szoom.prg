* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_ZOOM
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 01/09/97
* Aggiornato il : 01/09/97
* Translated    : 03/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Zoom "visuale" con selezione
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam i_cCursor,i_cFilename,i_cZoomTitle,i_cZoomFile
* --- i_cCursor: creera' un cursore con questo nome con i record selezionati
*     i_cFileName: la tabella su cui opera lo zoom
*     i_cZoomTitle: titolo della finestra di zoom
*     i_cZoomFile: configurazione, viene aperta <i_cZoomFile>.<i_cFileName>_vzm, se
*                  non specificato cerca default.<i_cFileName>_vzm,
*                  se non c'e' mostra tutti i campi della tabella
Local i_obj
i_obj=Createobject('stdzz',i_cCursor,i_cFilename,'*',.F.,.F.,.F.,.F.,i_cZoomTitle,i_cZoomFile)
i_obj.Show()
Select (i_cCursor)
Return

Define Class stdzz As stdzoom
    WindowType=1
    cOutCursor=''
    bOk=.F.
    Proc Init(i_cOutCursor,i_cFile,i_cFields,i_cKeyFields,i_cTarget,i_cInitSel,i_cZoomOnZoom,i_cZoomTitle,i_cZoomFile)
        This.cOutCursor=i_cOutCursor
        This.Caption=Iif(Empty(i_cZoomTitle),cp_MsgFormat(MSG_ZOOM_CL__,i_cFile),i_cZoomTitle)
        This.AddObject('z1','zzbox')
        This.z1.cKeyFields=Iif(Type("i_cKeyFields")='C',i_cKeyFields,'')
        This.z1.SetFileAndFields(i_cFile,i_cFields,i_cInitSel,i_cZoomFile)
        This.z1.SetZoomOnZoom(i_cZoomOnZoom)
        This.z1.TabIndex=1
        This.cTarget=Iif(Vartype(i_cTarget)='C',i_cTarget,'')
        This.z1.zz.Visible=.T.
        This.z1.zz.Picture=''
        This.z1.zz.Caption=cp_Translate(MSG_OK)
        This.z1.cZoomOnZoom='_ok_'
    Proc Destroy()
        If !Empty(This.cOutCursor)
            If This.bOk
                Select * From (This.z1.cCursor) Where xchk=1 Into Cursor (This.cOutCursor)
            Else
                Select * From (This.z1.cCursor) Where .F. Into Cursor (This.cOutCursor)
            Endif
        Endif
        DoDefault()
Enddefine
