* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_objmaskoptions                                               *
*              Opzioni oggetti                                                 *
*                                                                              *
*      Author: Nicol� Mercanti                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-10                                                      *
* Last revis.: 2015-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_objmaskoptions
Dimension pParam[2]
= ACOPY(oParentObject, pParam)
pParam = pParam[2]
* --- Fine Area Manuale
return(createobject("tcp_objmaskoptions",oParentObject))

* --- Class definition
define class tcp_objmaskoptions as StdForm
  Top    = 6
  Left   = 16

  * --- Standard Properties
  Width  = 717
  Height = 512+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-05-15"
  HelpContextID=99300103
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=271

  * --- Constant Properties
  _IDX = 0
  cpttbls_IDX = 0
  cPrg = "cp_objmaskoptions"
  cComment = "Opzioni oggetti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FLTTL1 = .F.
  w_TITLE1 = space(254)
  w_FLALG1 = .F.
  w_ALIGN1 = space(1)
  w_FLFNT1 = .F.
  w_FNTNAME1 = space(180)
  w_FNTSIZE1 = 0
  w_FLGLF1 = .F.
  w_GLBFONT1 = .F.
  w_FLTOP1 = .F.
  w_TOP1 = 0
  w_FLLEF1 = .F.
  w_LEFT1 = 0
  w_FLHGT1 = .F.
  w_HEIGHT1 = 0
  w_FLWDT1 = .F.
  w_WIDTH1 = 0
  w_SELECT1 = space(10)
  o_SELECT1 = space(10)
  w_FLVAR2 = .F.
  w_VAR2 = space(25)
  w_FLTYP2 = .F.
  w_TYPE_V = space(1)
  w_FLLEN2 = .F.
  w_LEN2 = 0
  w_DEC2 = 0
  w_FLEDI2 = .F.
  w_EDIT2 = space(4)
  w_FLZER2 = .F.
  w_ZERO2 = space(1)
  w_FLFOR2 = .F.
  w_PICTURE2 = space(30)
  w_FLARC2 = .F.
  w_ARCHIVE2 = space(10)
  o_ARCHIVE2 = space(10)
  w_FLFVL1 = .F.
  w_FIELD1 = space(10)
  w_VALUE1_V = space(40)
  w_FLFVL2 = .F.
  w_FIELD2 = space(10)
  w_VALUE2_V = space(40)
  w_FLFVL3 = .F.
  w_FIELD3 = space(10)
  w_VALUE3_V = space(40)
  w_FLFVL4 = .F.
  w_FIELD4 = space(10)
  w_FLFVL5 = .F.
  w_FIELD5 = space(10)
  w_VALUE5_V = space(40)
  w_FLFNT2 = .F.
  w_FNTNAME2 = space(180)
  w_FNTSIZE2 = 0
  w_FLGLF2 = .F.
  w_GLBFONT2 = .F.
  w_FLTOP2 = .F.
  w_TOP2 = 0
  w_FLLEF2 = .F.
  w_LEFT2 = 0
  w_FLHGT2 = .F.
  w_HEIGHT2 = 0
  w_FLWDT2 = .F.
  w_WIDTH2 = 0
  w_SELECT2 = space(10)
  o_SELECT2 = space(10)
  w_FLVAR3 = .F.
  w_VAR3 = space(25)
  w_FLTYP3 = .F.
  w_TYPE_CB = space(1)
  w_FLLEN3 = .F.
  w_LEN3 = 0
  w_DEC3 = 0
  w_FLEDI3 = .F.
  w_EDIT3 = space(4)
  w_FLVDS1 = .F.
  w_VALUE1_C = space(30)
  w_DESCRI1_C = space(60)
  w_FLVDS2 = .F.
  w_VALUE2_C = space(30)
  w_DESCRI2_C = space(60)
  w_FLVDS3 = .F.
  w_VALUE3_C = space(30)
  w_DESCRI3_C = space(60)
  w_FLVDS4 = .F.
  w_VALUE4_C = space(30)
  w_DESCRI4_C = space(60)
  w_FLVDS5 = .F.
  w_VALUE5_C = space(30)
  w_DESCRI5_C = space(60)
  w_FLVDS6 = .F.
  w_VALUE6_C = space(30)
  w_DESCRI6_C = space(60)
  w_FLVDS7 = .F.
  w_VALUE7_C = space(30)
  w_DESCRI7_C = space(60)
  w_FLVDS8 = .F.
  w_VALUE8_C = space(30)
  w_DESCRI8_C = space(60)
  w_FLVDS9 = .F.
  w_VALUE9_C = space(30)
  w_DESCRI9_C = space(60)
  w_FLVDS10 = .F.
  w_VALUE10_C = space(30)
  w_DESCRI10_C = space(60)
  w_FLFNT3 = .F.
  w_FNTNAME3 = space(180)
  w_FNTSIZE3 = 0
  w_FLGLF3 = .F.
  w_GLBFONT3 = .F.
  w_FLTOP3 = .F.
  w_TOP3 = 0
  w_FLLEF3 = .F.
  w_LEFT3 = 0
  w_FLHGT3 = .F.
  w_HEIGHT3 = 0
  w_FLWDT3 = .F.
  w_WIDTH3 = 0
  w_SELECT3 = space(10)
  o_SELECT3 = space(10)
  w_FLVAR4 = .F.
  w_VAR4 = space(25)
  w_FLTTL4 = .F.
  w_TITLE4 = space(100)
  w_FLTYP4 = .F.
  w_TYPE_CH = space(1)
  w_FLLEN4 = .F.
  w_LEN4 = 0
  w_DEC4 = 0
  w_FLEDI4 = .F.
  w_EDIT4 = space(4)
  w_FLSEL4 = .F.
  w_SELECTED4 = space(30)
  w_FLUNS4 = .F.
  w_UNSELECTED4 = space(30)
  w_FLFNT4 = .F.
  w_FNTNAME4 = space(180)
  w_FNTSIZE4 = 0
  w_FLGLF4 = .F.
  w_GLBFONT4 = .F.
  w_FLTOP4 = .F.
  w_TOP4 = 0
  w_FLLEF4 = .F.
  w_LEFT4 = 0
  w_FLHGT4 = .F.
  w_HEIGHT4 = 0
  w_FLWDT4 = .F.
  w_WIDTH4 = 0
  w_SELECT4 = space(10)
  o_SELECT4 = space(10)
  w_FLCLR6 = .F.
  w_FLEXE5 = .F.
  w_EXEC5 = space(50)
  w_FLTYP5 = .F.
  w_TYPE_BTN = space(4)
  w_FLTTL5 = .F.
  w_TITLE5 = space(100)
  w_FLPIC5 = .F.
  w_PICTURE5 = space(150)
  w_FLFOR5 = .F.
  w_FORMAT5 = 0
  w_FLINV5 = .F.
  w_INVIS5 = 0
  w_DCLICK5 = 0
  w_FLSHP5 = .F.
  w_SHAPE5 = space(20)
  w_FLEDI5 = .F.
  w_EDITABLE5 = space(30)
  w_FLUNS5 = .F.
  w_UNSEL5 = 0
  w_FLPAS5 = .F.
  w_PASMOU5 = 0
  w_FLPRS5 = .F.
  w_PRESSMOU5 = 0
  w_FLLBR5 = .F.
  w_BRDWID5 = 0
  w_FLCRV5 = .F.
  w_RADIUS5 = 0
  w_FLCBR5 = .F.
  w_FLCSF5 = .F.
  w_FLFNT5 = .F.
  w_FNTNAME5 = space(180)
  w_FNTSIZE5 = 0
  w_FLGLF5 = .F.
  w_GLBFONT5 = .F.
  w_FLTOP5 = .F.
  w_TOP5 = 0
  w_FLLEF5 = .F.
  w_LEFT5 = 0
  w_FLHGT5 = .F.
  w_HEIGHT5 = 0
  w_FLWDT5 = .F.
  w_WIDTH5 = 0
  w_COLOR6 = 0
  w_FLBRW6 = .F.
  w_BRDWID6 = 0
  w_FLINI6 = .F.
  w_INILINE6 = 0
  w_FLFIN6 = .F.
  w_FINLINE6 = 0
  w_FLSTY6 = .F.
  w_STYLINE6 = 0
  w_FLCUR6 = .F.
  w_CURVE6 = 0
  w_FLEXE7 = .F.
  w_EXEC7 = space(50)
  w_FLTYP7 = .F.
  w_TYPE_BMP = space(4)
  w_FLTTL7 = .F.
  w_TITLE7 = space(100)
  w_FLPIC7 = .F.
  w_PICTURE7 = space(300)
  w_FLTOP7 = .F.
  w_TOP7 = 0
  w_FLLEF7 = .F.
  w_LEFT7 = 0
  w_FLHGT7 = .F.
  w_HEIGHT7 = 0
  w_FLWDT7 = .F.
  w_WIDTH7 = 0
  w_SELECT5 = space(10)
  o_SELECT5 = space(10)
  w_CLASSOBJ5 = space(10)
  w_BRDCLR5 = 0
  w_BCKCLR5 = 0
  w_SELECT6 = space(10)
  o_SELECT6 = space(10)
  w_SELECT7 = space(10)
  o_SELECT7 = space(10)
  w_HIDE1 = 0
  w_HIDE2 = 0
  w_HIDE3 = 0
  w_HIDE4 = 0
  w_HIDE5 = 0
  w_HIDE6 = 0
  w_HIDE7 = 0
  w_FNTCOLOR1 = 0
  w_FNTBOLD1 = space(1)
  w_FNTITALIC1 = space(1)
  w_FNTUNDER1 = space(1)
  w_FNTSTRIKE1 = space(1)
  w_FNTCOLOR2 = 0
  w_FNTBOLD2 = space(1)
  w_FNTITALIC2 = space(1)
  w_FNTUNDER2 = space(1)
  w_FNTSTRIKE2 = space(1)
  w_FNTCOLOR3 = 0
  w_FNTBOLD3 = space(1)
  w_FNTITALIC3 = space(1)
  w_FNTUNDER3 = space(1)
  w_FNTSTRIKE3 = space(1)
  w_FNTCOLOR4 = 0
  w_FNTBOLD4 = space(1)
  w_FNTITALIC4 = space(1)
  w_FNTUNDER4 = space(1)
  w_FNTSTRIKE4 = space(1)
  w_FNTCOLOR5 = 0
  w_FNTBOLD5 = space(1)
  w_FNTITALIC5 = space(1)
  w_FNTUNDER5 = space(1)
  w_FNTSTRIKE5 = space(1)
  w_FROMPAGE = 0
  w_ZNAME = space(40)
  w_ZCLASS3 = space(10)
  w_CURROBJ = space(10)
  w_CURRCLAS = space(10)
  w_INIT1 = .F.
  w_INIT2 = .F.
  w_INIT3 = .F.
  w_INIT4 = .F.
  w_INIT5 = .F.
  w_INIT6 = .F.
  w_INIT7 = .F.
  w_NACTIVEPAGE = space(20)
  w_CNFFILE = space(100)
  w_FLFOR7 = .F.
  w_FORMAT7 = 0
  w_INIANGLE6 = 0
  w_INISIZE6 = 0
  w_FINANGLE6 = 0
  w_FINSIZE6 = 0
  w_zBtn = .NULL.
  w_zLbl = .NULL.
  w_zTxt = .NULL.
  w_zCombo = .NULL.
  w_zCheck = .NULL.
  w_zLine = .NULL.
  w_zBitmap = .NULL.
  w_lbl = .NULL.
  w_txt = .NULL.
  w_combo = .NULL.
  w_check = .NULL.
  w_line = .NULL.
  w_bitmap = .NULL.
  w_btn = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=7, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_objmaskoptionsPag1","cp_objmaskoptions",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Stringa")
      .Pages(2).addobject("oPag","tcp_objmaskoptionsPag2","cp_objmaskoptions",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Variabile")
      .Pages(3).addobject("oPag","tcp_objmaskoptionsPag3","cp_objmaskoptions",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Radio/ComboBox")
      .Pages(4).addobject("oPag","tcp_objmaskoptionsPag4","cp_objmaskoptions",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("CheckBox")
      .Pages(5).addobject("oPag","tcp_objmaskoptionsPag5","cp_objmaskoptions",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Button/MediaButton")
      .Pages(6).addobject("oPag","tcp_objmaskoptionsPag6","cp_objmaskoptions",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Line")
      .Pages(7).addobject("oPag","tcp_objmaskoptionsPag7","cp_objmaskoptions",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Immagine")
      .Pages(7).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLTTL1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_objmaskoptions
    *-- Disabilito Funzionalit� del tasto destro
    thisform.bNoMenuFunction= .T.
    
    *-- Cursore per inserimento e salvataggio della maschera
    *-- Contiene tutti i flag della maschera e la corrispondenza tra propriet�-campo della maschera
    *-- Il campo FlagPlus indica i campi che differiscono i button dai mediabutton
    
    CREATE CURSOR CurOptions (Type c(5), PropName c (15), FieldName c(15), FlagName c(10), FlagPlus c(1))
    
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','cVarName' ,'title1' , 'flttl1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','cAlign','align1','flalg1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Fontname','fntname1','flfnt1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Fontsize','fntsize1','flfnt1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Fontcolor','fntcolor1','flfnt1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Fontbold','fntbold1','flfnt1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Fontitalic','fntitalic1','flfnt1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Fontunderline','fntunder1', 'flfnt1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Fontstrikeout','fntstrike1','flfnt1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','GlbFont','glbfont1', 'flglf1' )
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Top','top1', 'fltop1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Left','left1','fllef1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Height','height1','flhgt1' )
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('lbl','Width','width1','flwdt1')
    
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cVarName','var2','flvar2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cType','type_v','fltyp2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','nLen','len2','fllen2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','nDec','dec2','fllen2' )
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cPict','picture2','flfor2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cStato','edit2','fledi2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cZFill','zero2','flzer2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cArch','archive2','flarc2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cK1','field1','flfvl1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cK2','field2','flfvl2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cK3','field3','flfvl3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cKa','field4','flfvl4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cVk1','value1_v','flfvl1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cVk2','value2_v','flfvl2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cVk3','value3_v','flfvl3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cRf1','field5','flfvl5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','cRv1','value5_v','flfvl5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Fontname','fntname2','flfnt2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Fontsize','fntsize2','flfnt2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Fontcolor','fntcolor2','flfnt2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Fontbold','fntbold2','flfnt2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Fontitalic','fntitalic2','flfnt2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Fontunderline','fntunder2','flfnt2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Fontstrikeout','fntstrike2','flfnt2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','GlbFont','glbfont2','flglf2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Top','top2','fltop2' )
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Left','left2','fllef2' )
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Height','height2','flhgt2' )
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('txt','Width','width2','flwdt2' )
    
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVarName','var3','flvar3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cType','type_cb','fltyp3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','nLen','len3','fllen3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','nDec','dec3','fllen3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cStato','edit3','fledi3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal1','value1_c','flvds1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal2','value2_c','flvds2' )
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal3','value3_c','flvds3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal4','value4_c','flvds4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal5','value5_c','flvds5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal6','value6_c','flvds6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal7','value7_c','flvds7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal8','value8_c','flvds8')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal9','value9_c','flvds9')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cVal10','value10_c','flvds10')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl1','descri1_c','flvds1')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl2','descri2_c','flvds2')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl3','descri3_c','flvds3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl4','descri4_c','flvds4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl5','descri5_c','flvds5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl6','descri6_c','flvds6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl7','descri7_c','flvds7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl8','descri8_c','flvds8')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl9','descri9_c','flvds9')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','cLbl10','descri10_c','flvds10')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Fontname','fntname3','flfnt3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo', 'Fontsize','fntsize3','flfnt3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Fontcolor','fntcolor3','flfnt3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Fontbold','fntbold3','flfnt3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Fontitalic','fntitalic3','flfnt3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Fontunderline','fntunder3','flfnt3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Fontstrikeout','fntstrike3','flfnt3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','GlbFont','glbfont3','flglf3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Top','top3','fltop3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Left','left3','fllef3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Height','height3','flhgt3')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('combo','Width','width3','flwdt3')
    
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','cVarName','var4','flvar4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','cType','type_ch','fltyp4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','cTitle','title4','flttl4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','nLen','len4','fllen4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','nDec','dec4','fllen4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','cSelValue','selected4','flsel4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','cUnselValue','unselected4','fluns4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','cStato','edit4','fledi4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Fontname','fntname4', 'flfnt4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Fontcolor','fntcolor4','flfnt4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Fontbold','fntbold4','flfnt4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Fontitalic','fntitalic4','flfnt4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Fontunderline','fntunder4', 'flfnt4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Fontstrikeout','fntstrike4','flfnt4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','GlbFont','glbfont4','flglf4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Top','top4','fltop4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Left','left4','fllef4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Height','height4','flhgt4')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('check','Width','width4', 'flwdt4')
    
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','cPrg','exec5','flexe5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','cTitle','title5','flttl5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','cType','type_btn','fltyp5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','cPicture','picture5','flpic5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','nFormatPicture','format5','flfor5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','nStyle','invis5','flinv5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','nStyle','dclick5','flinv5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName,FlagPlus) values ('btn','cShape','shape5','flshp5', 'M')
    Insert into CurOptions (Type,PropName,FieldName,FlagName,FlagPlus) values ('btn','cEditCond','editable5','fledi5', 'M')
    Insert into CurOptions (Type,PropName,FieldName,FlagName,FlagPlus) values ('btn','nAlpha','unsel5','fluns5', 'M')
    Insert into CurOptions (Type,PropName,FieldName,FlagName,FlagPlus) values ('btn','nAlphaH','pasmou5','flpas5', 'M')
    Insert into CurOptions (Type,PropName,FieldName,FlagName,FlagPlus) values ('btn','nAlphaD','pressmou5','flprs5', 'M')
    Insert into CurOptions (Type,PropName,FieldName,FlagName,FlagPlus) values ('btn','nBrdWidth','brdwid5','fllbr5', 'M')
    Insert into CurOptions (Type,PropName,FieldName,FlagName,FlagPlus) values ('btn','nRadius','radius5','flcrv5', 'M')
    Insert into CurOptions (Type,PropName,FieldName,FlagName,FlagPlus) values ('btn','nBorderColor','brdclr5','flcbr5', 'M')
    Insert into CurOptions (Type,PropName,FieldName,FlagName,FlagPlus) values ('btn','nBackColor','bckclr5','flcsf5', 'M')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Fontname','fntname5', 'flfnt5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Fontsize','fntsize5','flfnt5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Fontcolor','fntcolor5','flfnt5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Fontbold','fntbold5','flfnt5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Fontitalic','fntitalic5','flfnt5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Fontunderline','fntunder5','flfnt5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Fontstrikeout','fntstrike5','flfnt5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','GlbFont','glbfont5','flglf5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Top','top5', 'fltop5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Left','left5','fllef5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Height','height5','flhgt5')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('btn','Width','width5','flwdt5')
    
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nBorderColor','color6','flclr6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nBorderWidth','brdwid6','flbrw6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nStartCap','iniline6','flini6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nEndCap','finline6','flfin6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nDashStyle','styline6','flsty6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nCurvature','curve6','flcur6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nSCAngle','iniangle6','flini6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nSCSize','inisize6','flini6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nECAngle','finangle6','flfin6')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('line','nECSize','finsize6','flfin6')
    
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('bitmap','cPrg','exec7', 'flexe7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('bitmap','cTitle','title7', 'flttl7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('bitmap','cType','type_bmp','fltyp7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('bitmap','cPicture','picture7','flpic7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('bitmap','nFormatPicture','format7','flfor7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('bitmap','Top','top7','fltop7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('bitmap','Left','left7','fllef7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('bitmap','Height','height7','flhgt7')
    Insert into CurOptions (Type,PropName,FieldName,FlagName) values ('bitmap','Width','width7','flwdt7')
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_zBtn = this.oPgFrm.Pages(5).oPag.zBtn
    this.w_zLbl = this.oPgFrm.Pages(1).oPag.zLbl
    this.w_zTxt = this.oPgFrm.Pages(2).oPag.zTxt
    this.w_zCombo = this.oPgFrm.Pages(3).oPag.zCombo
    this.w_zCheck = this.oPgFrm.Pages(4).oPag.zCheck
    this.w_zLine = this.oPgFrm.Pages(6).oPag.zLine
    this.w_zBitmap = this.oPgFrm.Pages(7).oPag.zBitmap
    this.w_lbl = this.oPgFrm.Pages(1).oPag.lbl
    this.w_txt = this.oPgFrm.Pages(2).oPag.txt
    this.w_combo = this.oPgFrm.Pages(3).oPag.combo
    this.w_check = this.oPgFrm.Pages(4).oPag.check
    this.w_line = this.oPgFrm.Pages(6).oPag.line
    this.w_bitmap = this.oPgFrm.Pages(7).oPag.bitmap
    this.w_btn = this.oPgFrm.Pages(5).oPag.btn
    DoDefault()
    proc Destroy()
      this.w_zBtn = .NULL.
      this.w_zLbl = .NULL.
      this.w_zTxt = .NULL.
      this.w_zCombo = .NULL.
      this.w_zCheck = .NULL.
      this.w_zLine = .NULL.
      this.w_zBitmap = .NULL.
      this.w_lbl = .NULL.
      this.w_txt = .NULL.
      this.w_combo = .NULL.
      this.w_check = .NULL.
      this.w_line = .NULL.
      this.w_bitmap = .NULL.
      this.w_btn = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='cpttbls'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        cp_objmaskoptions_b(this,"Save")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLTTL1=.f.
      .w_TITLE1=space(254)
      .w_FLALG1=.f.
      .w_ALIGN1=space(1)
      .w_FLFNT1=.f.
      .w_FNTNAME1=space(180)
      .w_FNTSIZE1=0
      .w_FLGLF1=.f.
      .w_GLBFONT1=.f.
      .w_FLTOP1=.f.
      .w_TOP1=0
      .w_FLLEF1=.f.
      .w_LEFT1=0
      .w_FLHGT1=.f.
      .w_HEIGHT1=0
      .w_FLWDT1=.f.
      .w_WIDTH1=0
      .w_SELECT1=space(10)
      .w_FLVAR2=.f.
      .w_VAR2=space(25)
      .w_FLTYP2=.f.
      .w_TYPE_V=space(1)
      .w_FLLEN2=.f.
      .w_LEN2=0
      .w_DEC2=0
      .w_FLEDI2=.f.
      .w_EDIT2=space(4)
      .w_FLZER2=.f.
      .w_ZERO2=space(1)
      .w_FLFOR2=.f.
      .w_PICTURE2=space(30)
      .w_FLARC2=.f.
      .w_ARCHIVE2=space(10)
      .w_FLFVL1=.f.
      .w_FIELD1=space(10)
      .w_VALUE1_V=space(40)
      .w_FLFVL2=.f.
      .w_FIELD2=space(10)
      .w_VALUE2_V=space(40)
      .w_FLFVL3=.f.
      .w_FIELD3=space(10)
      .w_VALUE3_V=space(40)
      .w_FLFVL4=.f.
      .w_FIELD4=space(10)
      .w_FLFVL5=.f.
      .w_FIELD5=space(10)
      .w_VALUE5_V=space(40)
      .w_FLFNT2=.f.
      .w_FNTNAME2=space(180)
      .w_FNTSIZE2=0
      .w_FLGLF2=.f.
      .w_GLBFONT2=.f.
      .w_FLTOP2=.f.
      .w_TOP2=0
      .w_FLLEF2=.f.
      .w_LEFT2=0
      .w_FLHGT2=.f.
      .w_HEIGHT2=0
      .w_FLWDT2=.f.
      .w_WIDTH2=0
      .w_SELECT2=space(10)
      .w_FLVAR3=.f.
      .w_VAR3=space(25)
      .w_FLTYP3=.f.
      .w_TYPE_CB=space(1)
      .w_FLLEN3=.f.
      .w_LEN3=0
      .w_DEC3=0
      .w_FLEDI3=.f.
      .w_EDIT3=space(4)
      .w_FLVDS1=.f.
      .w_VALUE1_C=space(30)
      .w_DESCRI1_C=space(60)
      .w_FLVDS2=.f.
      .w_VALUE2_C=space(30)
      .w_DESCRI2_C=space(60)
      .w_FLVDS3=.f.
      .w_VALUE3_C=space(30)
      .w_DESCRI3_C=space(60)
      .w_FLVDS4=.f.
      .w_VALUE4_C=space(30)
      .w_DESCRI4_C=space(60)
      .w_FLVDS5=.f.
      .w_VALUE5_C=space(30)
      .w_DESCRI5_C=space(60)
      .w_FLVDS6=.f.
      .w_VALUE6_C=space(30)
      .w_DESCRI6_C=space(60)
      .w_FLVDS7=.f.
      .w_VALUE7_C=space(30)
      .w_DESCRI7_C=space(60)
      .w_FLVDS8=.f.
      .w_VALUE8_C=space(30)
      .w_DESCRI8_C=space(60)
      .w_FLVDS9=.f.
      .w_VALUE9_C=space(30)
      .w_DESCRI9_C=space(60)
      .w_FLVDS10=.f.
      .w_VALUE10_C=space(30)
      .w_DESCRI10_C=space(60)
      .w_FLFNT3=.f.
      .w_FNTNAME3=space(180)
      .w_FNTSIZE3=0
      .w_FLGLF3=.f.
      .w_GLBFONT3=.f.
      .w_FLTOP3=.f.
      .w_TOP3=0
      .w_FLLEF3=.f.
      .w_LEFT3=0
      .w_FLHGT3=.f.
      .w_HEIGHT3=0
      .w_FLWDT3=.f.
      .w_WIDTH3=0
      .w_SELECT3=space(10)
      .w_FLVAR4=.f.
      .w_VAR4=space(25)
      .w_FLTTL4=.f.
      .w_TITLE4=space(100)
      .w_FLTYP4=.f.
      .w_TYPE_CH=space(1)
      .w_FLLEN4=.f.
      .w_LEN4=0
      .w_DEC4=0
      .w_FLEDI4=.f.
      .w_EDIT4=space(4)
      .w_FLSEL4=.f.
      .w_SELECTED4=space(30)
      .w_FLUNS4=.f.
      .w_UNSELECTED4=space(30)
      .w_FLFNT4=.f.
      .w_FNTNAME4=space(180)
      .w_FNTSIZE4=0
      .w_FLGLF4=.f.
      .w_GLBFONT4=.f.
      .w_FLTOP4=.f.
      .w_TOP4=0
      .w_FLLEF4=.f.
      .w_LEFT4=0
      .w_FLHGT4=.f.
      .w_HEIGHT4=0
      .w_FLWDT4=.f.
      .w_WIDTH4=0
      .w_SELECT4=space(10)
      .w_FLCLR6=.f.
      .w_FLEXE5=.f.
      .w_EXEC5=space(50)
      .w_FLTYP5=.f.
      .w_TYPE_BTN=space(4)
      .w_FLTTL5=.f.
      .w_TITLE5=space(100)
      .w_FLPIC5=.f.
      .w_PICTURE5=space(150)
      .w_FLFOR5=.f.
      .w_FORMAT5=0
      .w_FLINV5=.f.
      .w_INVIS5=0
      .w_DCLICK5=0
      .w_FLSHP5=.f.
      .w_SHAPE5=space(20)
      .w_FLEDI5=.f.
      .w_EDITABLE5=space(30)
      .w_FLUNS5=.f.
      .w_UNSEL5=0
      .w_FLPAS5=.f.
      .w_PASMOU5=0
      .w_FLPRS5=.f.
      .w_PRESSMOU5=0
      .w_FLLBR5=.f.
      .w_BRDWID5=0
      .w_FLCRV5=.f.
      .w_RADIUS5=0
      .w_FLCBR5=.f.
      .w_FLCSF5=.f.
      .w_FLFNT5=.f.
      .w_FNTNAME5=space(180)
      .w_FNTSIZE5=0
      .w_FLGLF5=.f.
      .w_GLBFONT5=.f.
      .w_FLTOP5=.f.
      .w_TOP5=0
      .w_FLLEF5=.f.
      .w_LEFT5=0
      .w_FLHGT5=.f.
      .w_HEIGHT5=0
      .w_FLWDT5=.f.
      .w_WIDTH5=0
      .w_COLOR6=0
      .w_FLBRW6=.f.
      .w_BRDWID6=0
      .w_FLINI6=.f.
      .w_INILINE6=0
      .w_FLFIN6=.f.
      .w_FINLINE6=0
      .w_FLSTY6=.f.
      .w_STYLINE6=0
      .w_FLCUR6=.f.
      .w_CURVE6=0
      .w_FLEXE7=.f.
      .w_EXEC7=space(50)
      .w_FLTYP7=.f.
      .w_TYPE_BMP=space(4)
      .w_FLTTL7=.f.
      .w_TITLE7=space(100)
      .w_FLPIC7=.f.
      .w_PICTURE7=space(300)
      .w_FLTOP7=.f.
      .w_TOP7=0
      .w_FLLEF7=.f.
      .w_LEFT7=0
      .w_FLHGT7=.f.
      .w_HEIGHT7=0
      .w_FLWDT7=.f.
      .w_WIDTH7=0
      .w_SELECT5=space(10)
      .w_CLASSOBJ5=space(10)
      .w_BRDCLR5=0
      .w_BCKCLR5=0
      .w_SELECT6=space(10)
      .w_SELECT7=space(10)
      .w_HIDE1=0
      .w_HIDE2=0
      .w_HIDE3=0
      .w_HIDE4=0
      .w_HIDE5=0
      .w_HIDE6=0
      .w_HIDE7=0
      .w_FNTCOLOR1=0
      .w_FNTBOLD1=space(1)
      .w_FNTITALIC1=space(1)
      .w_FNTUNDER1=space(1)
      .w_FNTSTRIKE1=space(1)
      .w_FNTCOLOR2=0
      .w_FNTBOLD2=space(1)
      .w_FNTITALIC2=space(1)
      .w_FNTUNDER2=space(1)
      .w_FNTSTRIKE2=space(1)
      .w_FNTCOLOR3=0
      .w_FNTBOLD3=space(1)
      .w_FNTITALIC3=space(1)
      .w_FNTUNDER3=space(1)
      .w_FNTSTRIKE3=space(1)
      .w_FNTCOLOR4=0
      .w_FNTBOLD4=space(1)
      .w_FNTITALIC4=space(1)
      .w_FNTUNDER4=space(1)
      .w_FNTSTRIKE4=space(1)
      .w_FNTCOLOR5=0
      .w_FNTBOLD5=space(1)
      .w_FNTITALIC5=space(1)
      .w_FNTUNDER5=space(1)
      .w_FNTSTRIKE5=space(1)
      .w_FROMPAGE=0
      .w_ZNAME=space(40)
      .w_ZCLASS3=space(10)
      .w_CURROBJ=space(10)
      .w_CURRCLAS=space(10)
      .w_INIT1=.f.
      .w_INIT2=.f.
      .w_INIT3=.f.
      .w_INIT4=.f.
      .w_INIT5=.f.
      .w_INIT6=.f.
      .w_INIT7=.f.
      .w_NACTIVEPAGE=space(20)
      .w_CNFFILE=space(100)
      .w_FLFOR7=.f.
      .w_FORMAT7=0
      .w_INIANGLE6=0
      .w_INISIZE6=0
      .w_FINANGLE6=0
      .w_FINSIZE6=0
        .w_FLTTL1 = .F.
          .DoRTCalc(2,2,.f.)
        .w_FLALG1 = .F.
          .DoRTCalc(4,4,.f.)
        .w_FLFNT1 = .F.
          .DoRTCalc(6,7,.f.)
        .w_FLGLF1 = .F.
        .w_GLBFONT1 = .F.
        .w_FLTOP1 = .F.
          .DoRTCalc(11,11,.f.)
        .w_FLLEF1 = .F.
          .DoRTCalc(13,13,.f.)
        .w_FLHGT1 = .F.
          .DoRTCalc(15,15,.f.)
        .w_FLWDT1 = .F.
          .DoRTCalc(17,18,.f.)
        .w_FLVAR2 = .F.
          .DoRTCalc(20,20,.f.)
        .w_FLTYP2 = .F.
        .w_TYPE_V = "C"
        .w_FLLEN2 = .F.
          .DoRTCalc(24,25,.f.)
        .w_FLEDI2 = .F.
          .DoRTCalc(27,27,.f.)
        .w_FLZER2 = .F.
          .DoRTCalc(29,29,.f.)
        .w_FLFOR2 = .F.
          .DoRTCalc(31,31,.f.)
        .w_FLARC2 = .F.
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_ARCHIVE2))
          .link_2_15('Full')
        endif
        .w_FLFVL1 = .F.
          .DoRTCalc(35,36,.f.)
        .w_FLFVL2 = .F.
          .DoRTCalc(38,39,.f.)
        .w_FLFVL3 = .F.
          .DoRTCalc(41,42,.f.)
        .w_FLFVL4 = .F.
          .DoRTCalc(44,44,.f.)
        .w_FLFVL5 = .F.
          .DoRTCalc(46,47,.f.)
        .w_FLFNT2 = .F.
          .DoRTCalc(49,50,.f.)
        .w_FLGLF2 = .F.
          .DoRTCalc(52,52,.f.)
        .w_FLTOP2 = .F.
          .DoRTCalc(54,54,.f.)
        .w_FLLEF2 = .F.
          .DoRTCalc(56,56,.f.)
        .w_FLHGT2 = .F.
          .DoRTCalc(58,58,.f.)
        .w_FLWDT2 = .F.
          .DoRTCalc(60,61,.f.)
        .w_FLVAR3 = .F.
          .DoRTCalc(63,63,.f.)
        .w_FLTYP3 = .F.
        .w_TYPE_CB = "C"
        .w_FLLEN3 = .F.
          .DoRTCalc(67,68,.f.)
        .w_FLEDI3 = .F.
          .DoRTCalc(70,70,.f.)
        .w_FLVDS1 = .F.
          .DoRTCalc(72,73,.f.)
        .w_FLVDS2 = .F.
          .DoRTCalc(75,76,.f.)
        .w_FLVDS3 = .F.
          .DoRTCalc(78,79,.f.)
        .w_FLVDS4 = .F.
          .DoRTCalc(81,82,.f.)
        .w_FLVDS5 = .F.
          .DoRTCalc(84,85,.f.)
        .w_FLVDS6 = .F.
          .DoRTCalc(87,88,.f.)
        .w_FLVDS7 = .F.
          .DoRTCalc(90,91,.f.)
        .w_FLVDS8 = .F.
          .DoRTCalc(93,94,.f.)
        .w_FLVDS9 = .F.
          .DoRTCalc(96,97,.f.)
        .w_FLVDS10 = .F.
          .DoRTCalc(99,100,.f.)
        .w_FLFNT3 = .F.
          .DoRTCalc(102,103,.f.)
        .w_FLGLF3 = .F.
          .DoRTCalc(105,105,.f.)
        .w_FLTOP3 = .F.
          .DoRTCalc(107,107,.f.)
        .w_FLLEF3 = .F.
          .DoRTCalc(109,109,.f.)
        .w_FLHGT3 = .F.
          .DoRTCalc(111,111,.f.)
        .w_FLWDT3 = .F.
          .DoRTCalc(113,114,.f.)
        .w_FLVAR4 = .F.
          .DoRTCalc(116,116,.f.)
        .w_FLTTL4 = .F.
          .DoRTCalc(118,118,.f.)
        .w_FLTYP4 = .F.
        .w_TYPE_CH = "C"
        .w_FLLEN4 = .F.
          .DoRTCalc(122,123,.f.)
        .w_FLEDI4 = .F.
          .DoRTCalc(125,125,.f.)
        .w_FLSEL4 = .F.
          .DoRTCalc(127,127,.f.)
        .w_FLUNS4 = .F.
          .DoRTCalc(129,129,.f.)
        .w_FLFNT4 = .F.
          .DoRTCalc(131,132,.f.)
        .w_FLGLF4 = .F.
          .DoRTCalc(134,134,.f.)
        .w_FLTOP4 = .F.
          .DoRTCalc(136,136,.f.)
        .w_FLLEF4 = .F.
          .DoRTCalc(138,138,.f.)
        .w_FLHGT4 = .F.
          .DoRTCalc(140,140,.f.)
        .w_FLWDT4 = .F.
          .DoRTCalc(142,143,.f.)
        .w_FLCLR6 = .F.
        .w_FLEXE5 = .F.
          .DoRTCalc(146,146,.f.)
        .w_FLTYP5 = .F.
          .DoRTCalc(148,148,.f.)
        .w_FLTTL5 = .F.
          .DoRTCalc(150,150,.f.)
        .w_FLPIC5 = .F.
      .oPgFrm.Page5.oPag.oObj_5_11.Calculate()
          .DoRTCalc(152,152,.f.)
        .w_FLFOR5 = .F.
          .DoRTCalc(154,154,.f.)
        .w_FLINV5 = .F.
          .DoRTCalc(156,157,.f.)
        .w_FLSHP5 = .F.
          .DoRTCalc(159,159,.f.)
        .w_FLEDI5 = .F.
          .DoRTCalc(161,161,.f.)
        .w_FLUNS5 = .F.
          .DoRTCalc(163,163,.f.)
        .w_FLPAS5 = .F.
          .DoRTCalc(165,165,.f.)
        .w_FLPRS5 = .F.
          .DoRTCalc(167,167,.f.)
        .w_FLLBR5 = .F.
          .DoRTCalc(169,169,.f.)
        .w_FLCRV5 = .F.
          .DoRTCalc(171,171,.f.)
        .w_FLCBR5 = .F.
        .w_FLCSF5 = .F.
        .w_FLFNT5 = .F.
          .DoRTCalc(175,176,.f.)
        .w_FLGLF5 = .F.
          .DoRTCalc(178,178,.f.)
        .w_FLTOP5 = .F.
          .DoRTCalc(180,180,.f.)
        .w_FLLEF5 = .F.
          .DoRTCalc(182,182,.f.)
        .w_FLHGT5 = .F.
          .DoRTCalc(184,184,.f.)
        .w_FLWDT5 = .F.
          .DoRTCalc(186,187,.f.)
        .w_FLBRW6 = .F.
          .DoRTCalc(189,189,.f.)
        .w_FLINI6 = .F.
          .DoRTCalc(191,191,.f.)
        .w_FLFIN6 = .F.
          .DoRTCalc(193,193,.f.)
        .w_FLSTY6 = .F.
          .DoRTCalc(195,195,.f.)
        .w_FLCUR6 = .F.
          .DoRTCalc(197,197,.f.)
        .w_FLEXE7 = .F.
          .DoRTCalc(199,199,.f.)
        .w_FLTYP7 = .F.
          .DoRTCalc(201,201,.f.)
        .w_FLTTL7 = .F.
          .DoRTCalc(203,203,.f.)
        .w_FLPIC7 = .F.
      .oPgFrm.Page7.oPag.oObj_7_11.Calculate()
          .DoRTCalc(205,205,.f.)
        .w_FLTOP7 = .F.
          .DoRTCalc(207,207,.f.)
        .w_FLLEF7 = .F.
          .DoRTCalc(209,209,.f.)
        .w_FLHGT7 = .F.
          .DoRTCalc(211,211,.f.)
        .w_FLWDT7 = .F.
      .oPgFrm.Page5.oPag.zBtn.Calculate()
      .oPgFrm.Page1.oPag.zLbl.Calculate()
          .DoRTCalc(213,219,.f.)
        .w_HIDE1 = 2
        .w_HIDE2 = 2
        .w_HIDE3 = 2
        .w_HIDE4 = 2
        .w_HIDE5 = 2
        .w_HIDE6 = 2
        .w_HIDE7 = 2
      .oPgFrm.Page2.oPag.zTxt.Calculate()
      .oPgFrm.Page3.oPag.zCombo.Calculate()
      .oPgFrm.Page4.oPag.zCheck.Calculate()
      .oPgFrm.Page6.oPag.zLine.Calculate()
      .oPgFrm.Page7.oPag.zBitmap.Calculate()
      .oPgFrm.Page1.oPag.lbl.Calculate(ALLTRIM(.w_ZNAME))
          .DoRTCalc(227,253,.f.)
        .w_ZCLASS3 = .w_zCombo.getvar("oclass")
      .oPgFrm.Page2.oPag.txt.Calculate(.w_ZNAME)
      .oPgFrm.Page3.oPag.combo.Calculate(.w_ZNAME)
      .oPgFrm.Page4.oPag.check.Calculate(.w_ZNAME)
      .oPgFrm.Page6.oPag.line.Calculate(.w_ZNAME)
      .oPgFrm.Page7.oPag.bitmap.Calculate(.w_ZNAME)
      .oPgFrm.Page5.oPag.btn.Calculate(.w_ZNAME)
          .DoRTCalc(255,265,.f.)
        .w_FLFOR7 = .F.
    endwith
    this.DoRTCalc(267,271,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_objmaskoptions
    with this
          .w_lbl.bglobalfont = .f.
          .w_lbl.fontbold = .t.
          .w_lbl.fontsize = 10
          
          .w_txt.bglobalfont = .f.
          .w_txt.fontbold = .t.
          .w_txt.fontsize = 10
          
          .w_combo.bglobalfont = .f.
          .w_combo.fontbold = .t.
          .w_combo.fontsize = 10
          
          .w_check.bglobalfont = .f.
          .w_check.fontbold = .t.
          .w_check.fontsize = 10
          
          .w_btn.bglobalfont = .f.
          .w_btn.fontbold = .t.
          .w_btn.fontsize = 10
          
          .w_line.bglobalfont = .f.
          .w_line.fontbold = .t.
          .w_line.fontsize = 10
          
          .w_bitmap.bglobalfont = .f.
          .w_bitmap.fontbold = .t.
          .w_bitmap.fontsize = 10      
    endwith
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page5.oPag.oObj_5_11.Calculate()
        .oPgFrm.Page7.oPag.oObj_7_11.Calculate()
        .oPgFrm.Page5.oPag.zBtn.Calculate()
        .oPgFrm.Page1.oPag.zLbl.Calculate()
        .oPgFrm.Page2.oPag.zTxt.Calculate()
        .oPgFrm.Page3.oPag.zCombo.Calculate()
        .oPgFrm.Page4.oPag.zCheck.Calculate()
        .oPgFrm.Page6.oPag.zLine.Calculate()
        .oPgFrm.Page7.oPag.zBitmap.Calculate()
        if .o_SELECT2<>.w_SELECT2
          .Calculate_VVPMARUIYD()
        endif
        if .o_SELECT3<>.w_SELECT3
          .Calculate_QTCVQHMESM()
        endif
        if .o_SELECT4<>.w_SELECT4
          .Calculate_YAZSACAQCX()
        endif
        if .o_SELECT5<>.w_SELECT5
          .Calculate_IMIMOZGFDD()
        endif
        if .o_SELECT6<>.w_SELECT6
          .Calculate_SXODNLCOBY()
        endif
        if .o_SELECT7<>.w_SELECT7
          .Calculate_OCTAXWJRBB()
        endif
        if .o_SELECT1<>.w_SELECT1
          .Calculate_NJWWJQTPLT()
        endif
        if .o_ARCHIVE2<>.w_ARCHIVE2
          .Calculate_LKIUSGHEKY()
        endif
        .oPgFrm.Page1.oPag.lbl.Calculate(ALLTRIM(.w_ZNAME))
        .DoRTCalc(1,253,.t.)
            .w_ZCLASS3 = .w_zCombo.getvar("oclass")
        .oPgFrm.Page2.oPag.txt.Calculate(.w_ZNAME)
        .oPgFrm.Page3.oPag.combo.Calculate(.w_ZNAME)
        .oPgFrm.Page4.oPag.check.Calculate(.w_ZNAME)
        .oPgFrm.Page6.oPag.line.Calculate(.w_ZNAME)
        .oPgFrm.Page7.oPag.bitmap.Calculate(.w_ZNAME)
        .oPgFrm.Page5.oPag.btn.Calculate(.w_ZNAME)
        * --- Area Manuale = Calculate
        * --- cp_objmaskoptions
        *-- Nelle Page Options la condizione di Hide della prima pagina non viene tradotta dal Painter
        .oPgFrm.Page1.enabled=not(.w_HIDE1=0)
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(255,271,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page5.oPag.oObj_5_11.Calculate()
        .oPgFrm.Page7.oPag.oObj_7_11.Calculate()
        .oPgFrm.Page5.oPag.zBtn.Calculate()
        .oPgFrm.Page1.oPag.zLbl.Calculate()
        .oPgFrm.Page2.oPag.zTxt.Calculate()
        .oPgFrm.Page3.oPag.zCombo.Calculate()
        .oPgFrm.Page4.oPag.zCheck.Calculate()
        .oPgFrm.Page6.oPag.zLine.Calculate()
        .oPgFrm.Page7.oPag.zBitmap.Calculate()
        .oPgFrm.Page1.oPag.lbl.Calculate(ALLTRIM(.w_ZNAME))
        .oPgFrm.Page2.oPag.txt.Calculate(.w_ZNAME)
        .oPgFrm.Page3.oPag.combo.Calculate(.w_ZNAME)
        .oPgFrm.Page4.oPag.check.Calculate(.w_ZNAME)
        .oPgFrm.Page6.oPag.line.Calculate(.w_ZNAME)
        .oPgFrm.Page7.oPag.bitmap.Calculate(.w_ZNAME)
        .oPgFrm.Page5.oPag.btn.Calculate(.w_ZNAME)
    endwith
  return

  proc Calculate_NUJMZIXJLT()
    with this
          * --- Caricamento tab temporanea
          cp_objmaskoptions_b(this;
              ,"Zoom";
             )
    endwith
  endproc
  proc Calculate_XPFMZBSXHT()
    with this
          * --- Init - Gestione pagine
          cp_objmaskoptions_b(this;
              ,"Init";
             )
    endwith
  endproc
  proc Calculate_VVPMARUIYD()
    with this
          * --- Sel./Desel. tutti
          cp_objmaskoptions_b(this;
              ,"SelectAll";
              ,2;
              ,.w_SELECT2;
             )
    endwith
  endproc
  proc Calculate_QTCVQHMESM()
    with this
          * --- Sel./Desel. tutti
          cp_objmaskoptions_b(this;
              ,"SelectAll";
              ,3;
              ,.w_SELECT3;
             )
    endwith
  endproc
  proc Calculate_YAZSACAQCX()
    with this
          * --- Sel./Desel. tutti
          cp_objmaskoptions_b(this;
              ,"SelectAll";
              ,4;
              ,.w_SELECT4;
             )
    endwith
  endproc
  proc Calculate_IMIMOZGFDD()
    with this
          * --- Sel./Desel. tutti
          cp_objmaskoptions_b(this;
              ,"SelectAll";
              ,5;
              ,.w_SELECT5;
             )
    endwith
  endproc
  proc Calculate_SXODNLCOBY()
    with this
          * --- Sel./Desel. tutti
          cp_objmaskoptions_b(this;
              ,"SelectAll";
              ,6;
              ,.w_SELECT6;
             )
    endwith
  endproc
  proc Calculate_OCTAXWJRBB()
    with this
          * --- Sel./Desel. tutti
          cp_objmaskoptions_b(this;
              ,"SelectAll";
              ,7;
              ,.w_SELECT7;
             )
    endwith
  endproc
  proc Calculate_KTLLUFMFPX()
    with this
          * --- Cambio pagina
          cp_objmaskoptions_b(this;
              ,"SelectPage";
              ,2;
             )
    endwith
  endproc
  proc Calculate_YNJQIYAHFD()
    with this
          * --- Selezione zoom
     if this.oPgFrm.ActivePage=2
          cp_objmaskoptions_b(this;
              ,"SelectZoom";
              ,2;
             )
     endif
    endwith
  endproc
  proc Calculate_XAWJQPDYDV()
    with this
          * --- Cambio pagina
          cp_objmaskoptions_b(this;
              ,"SelectPage";
              ,3;
             )
    endwith
  endproc
  proc Calculate_ISVLGAUKIU()
    with this
          * --- Selezione zoom
     if this.oPgFrm.ActivePage=3
          cp_objmaskoptions_b(this;
              ,"SelectZoom";
              ,3;
             )
     endif
    endwith
  endproc
  proc Calculate_MGOVIGEAYP()
    with this
          * --- Cambio pagina
          cp_objmaskoptions_b(this;
              ,"SelectPage";
              ,5;
             )
    endwith
  endproc
  proc Calculate_KJMNNUFXYO()
    with this
          * --- Selezione zoom
     if this.oPgFrm.ActivePage=5
          cp_objmaskoptions_b(this;
              ,"SelectZoom";
              ,5;
             )
     endif
    endwith
  endproc
  proc Calculate_GUQYYQOICD()
    with this
          * --- Cambia colore bordo
          .w_BRDCLR5 = getcolor()
    endwith
  endproc
  proc Calculate_RPKADVNESJ()
    with this
          * --- Cambia colore sfondo
          .w_BCKCLR5 = getcolor()
    endwith
  endproc
  proc Calculate_LFRATFCOYG()
    with this
          * --- Cambio Pagina
          cp_objmaskoptions_b(this;
              ,"SelectPage";
              ,1;
             )
    endwith
  endproc
  proc Calculate_GTCHNDARTF()
    with this
          * --- Selezione zoom
     if this.oPgFrm.ActivePage=1
          cp_objmaskoptions_b(this;
              ,"SelectZoom";
              ,1;
             )
     endif
    endwith
  endproc
  proc Calculate_NJWWJQTPLT()
    with this
          * --- Sel./Desel. tutti
          cp_objmaskoptions_b(this;
              ,"SelectAll";
              ,1;
              ,.w_SELECT1;
             )
    endwith
  endproc
  proc Calculate_OYTJXLDJXK()
    with this
          * --- Cambio pagina
          cp_objmaskoptions_b(this;
              ,"SelectPage";
              ,4;
             )
    endwith
  endproc
  proc Calculate_HQOLEKQACJ()
    with this
          * --- Selezione zoom
     if this.oPgFrm.ActivePage=4
          cp_objmaskoptions_b(this;
              ,"SelectZoom";
              ,4;
             )
     endif
    endwith
  endproc
  proc Calculate_LIZFGPQURJ()
    with this
          * --- Cambio pagina
          cp_objmaskoptions_b(this;
              ,"SelectPage";
              ,6;
             )
    endwith
  endproc
  proc Calculate_VJNUCAVWXJ()
    with this
          * --- Selezione zoom
     if this.oPgFrm.ActivePage=6
          cp_objmaskoptions_b(this;
              ,"SelectZoom";
              ,6;
             )
     endif
    endwith
  endproc
  proc Calculate_BNBRUKSHYG()
    with this
          * --- Cambio pagina
          cp_objmaskoptions_b(this;
              ,"SelectPage";
              ,7;
             )
    endwith
  endproc
  proc Calculate_DDMKRMMBEC()
    with this
          * --- Selezione zoom
     if this.oPgFrm.ActivePage=7
          cp_objmaskoptions_b(this;
              ,"SelectZoom";
              ,7;
             )
     endif
    endwith
  endproc
  proc Calculate_XVYPUDPXDQ()
    with this
          * --- Cambia colore sfondo
          .w_COLOR6 = getcolor()
    endwith
  endproc
  proc Calculate_LKIUSGHEKY()
    with this
          * --- Selezione archivio
          cp_objmaskoptions_b(this;
              ,"Archive";
             )
    endwith
  endproc
  proc Calculate_UJQLSMFITJ()
    with this
          * --- Done
          cp_objmaskoptions_b(this;
              ,"Done";
             )
    endwith
  endproc
  proc Calculate_XGLJGBAIUV()
    with this
          * --- INIANGLE6 mai a 0
          .w_INIANGLE6 = iif(.w_INIANGLE6=0,1,.w_INIANGLE6)
    endwith
  endproc
  proc Calculate_UFCQTVZLWL()
    with this
          * --- INISIZE6 mai a 0
          .w_INISIZE6 = iif(.w_INISIZE6=0,1,.w_INISIZE6)
    endwith
  endproc
  proc Calculate_NQBHWBSREA()
    with this
          * --- FINANGLE6 mai a 0
          .w_FINANGLE6 = iif(.w_FINANGLE6=0,1,.w_FINANGLE6)
    endwith
  endproc
  proc Calculate_SOPDXSNRJO()
    with this
          * --- FINSIZE6 mai a 0
          .w_FINSIZE6 = iif(.w_FINSIZE6=0,1,.w_FINSIZE6)
    endwith
  endproc
  proc Calculate_DXEIVZCXFY()
    with this
          * --- Cambia colore font
          .w_FNTCOLOR1 = getcolor()
    endwith
  endproc
  proc Calculate_WWGSZOPOUF()
    with this
          * --- Cambia colore font
          .w_FNTCOLOR2 = getcolor()
    endwith
  endproc
  proc Calculate_SSXFEMLQXX()
    with this
          * --- Cambia colore font
          .w_FNTCOLOR3 = getcolor()
    endwith
  endproc
  proc Calculate_FXVCCFFSJJ()
    with this
          * --- Cambia colore font
          .w_FNTCOLOR4 = getcolor()
    endwith
  endproc
  proc Calculate_IASTFIZVEA()
    with this
          * --- Cambia colore font
          .w_FNTCOLOR5 = getcolor()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oDEC2_2_7.enabled = this.oPgFrm.Page2.oPag.oDEC2_2_7.mCond()
    this.oPgFrm.Page2.oPag.oZERO2_2_11.enabled = this.oPgFrm.Page2.oPag.oZERO2_2_11.mCond()
    this.oPgFrm.Page5.oPag.oFLSHP5_5_22.enabled = this.oPgFrm.Page5.oPag.oFLSHP5_5_22.mCond()
    this.oPgFrm.Page5.oPag.oSHAPE5_5_23.enabled = this.oPgFrm.Page5.oPag.oSHAPE5_5_23.mCond()
    this.oPgFrm.Page5.oPag.oFLEDI5_5_25.enabled = this.oPgFrm.Page5.oPag.oFLEDI5_5_25.mCond()
    this.oPgFrm.Page5.oPag.oEDITABLE5_5_26.enabled = this.oPgFrm.Page5.oPag.oEDITABLE5_5_26.mCond()
    this.oPgFrm.Page5.oPag.oFLUNS5_5_27.enabled = this.oPgFrm.Page5.oPag.oFLUNS5_5_27.mCond()
    this.oPgFrm.Page5.oPag.oUNSEL5_5_28.enabled = this.oPgFrm.Page5.oPag.oUNSEL5_5_28.mCond()
    this.oPgFrm.Page5.oPag.oFLPAS5_5_29.enabled = this.oPgFrm.Page5.oPag.oFLPAS5_5_29.mCond()
    this.oPgFrm.Page5.oPag.oPASMOU5_5_30.enabled = this.oPgFrm.Page5.oPag.oPASMOU5_5_30.mCond()
    this.oPgFrm.Page5.oPag.oFLPRS5_5_31.enabled = this.oPgFrm.Page5.oPag.oFLPRS5_5_31.mCond()
    this.oPgFrm.Page5.oPag.oPRESSMOU5_5_32.enabled = this.oPgFrm.Page5.oPag.oPRESSMOU5_5_32.mCond()
    this.oPgFrm.Page5.oPag.oFLLBR5_5_33.enabled = this.oPgFrm.Page5.oPag.oFLLBR5_5_33.mCond()
    this.oPgFrm.Page5.oPag.oBRDWID5_5_34.enabled = this.oPgFrm.Page5.oPag.oBRDWID5_5_34.mCond()
    this.oPgFrm.Page5.oPag.oFLCRV5_5_35.enabled = this.oPgFrm.Page5.oPag.oFLCRV5_5_35.mCond()
    this.oPgFrm.Page5.oPag.oRADIUS5_5_36.enabled = this.oPgFrm.Page5.oPag.oRADIUS5_5_36.mCond()
    this.oPgFrm.Page5.oPag.oFLCBR5_5_37.enabled = this.oPgFrm.Page5.oPag.oFLCBR5_5_37.mCond()
    this.oPgFrm.Page5.oPag.oFLCSF5_5_39.enabled = this.oPgFrm.Page5.oPag.oFLCSF5_5_39.mCond()
    this.oPgFrm.Page6.oPag.oINIANGLE6_6_34.enabled = this.oPgFrm.Page6.oPag.oINIANGLE6_6_34.mCond()
    this.oPgFrm.Page6.oPag.oINISIZE6_6_35.enabled = this.oPgFrm.Page6.oPag.oINISIZE6_6_35.mCond()
    this.oPgFrm.Page6.oPag.oFINANGLE6_6_36.enabled = this.oPgFrm.Page6.oPag.oFINANGLE6_6_36.mCond()
    this.oPgFrm.Page6.oPag.oFINSIZE6_6_37.enabled = this.oPgFrm.Page6.oPag.oFINSIZE6_6_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_33.enabled = this.oPgFrm.Page2.oPag.oBtn_2_33.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_43.enabled = this.oPgFrm.Page3.oPag.oBtn_3_43.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_19.enabled = this.oPgFrm.Page4.oPag.oBtn_4_19.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_44.enabled = this.oPgFrm.Page5.oPag.oBtn_5_44.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_93.enabled = this.oPgFrm.Page5.oPag.oBtn_5_93.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_33.enabled = this.oPgFrm.Page6.oPag.oBtn_6_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(this.w_HIDE2=0)
    this.oPgFrm.Pages(3).enabled=not(this.w_HIDE3=0)
    this.oPgFrm.Pages(4).enabled=not(this.w_HIDE4=0)
    this.oPgFrm.Pages(5).enabled=not(this.w_HIDE5=0)
    this.oPgFrm.Pages(6).enabled=not(this.w_HIDE6=0)
    this.oPgFrm.Pages(7).enabled=not(this.w_HIDE7=0)
    this.oPgFrm.Page1.oPag.oFLTTL1_1_1.visible=!this.oPgFrm.Page1.oPag.oFLTTL1_1_1.mHide()
    this.oPgFrm.Page1.oPag.oFLALG1_1_3.visible=!this.oPgFrm.Page1.oPag.oFLALG1_1_3.mHide()
    this.oPgFrm.Page1.oPag.oFLFNT1_1_5.visible=!this.oPgFrm.Page1.oPag.oFLFNT1_1_5.mHide()
    this.oPgFrm.Page1.oPag.oFLGLF1_1_9.visible=!this.oPgFrm.Page1.oPag.oFLGLF1_1_9.mHide()
    this.oPgFrm.Page1.oPag.oFLTOP1_1_11.visible=!this.oPgFrm.Page1.oPag.oFLTOP1_1_11.mHide()
    this.oPgFrm.Page1.oPag.oFLLEF1_1_13.visible=!this.oPgFrm.Page1.oPag.oFLLEF1_1_13.mHide()
    this.oPgFrm.Page1.oPag.oFLHGT1_1_15.visible=!this.oPgFrm.Page1.oPag.oFLHGT1_1_15.mHide()
    this.oPgFrm.Page1.oPag.oFLWDT1_1_17.visible=!this.oPgFrm.Page1.oPag.oFLWDT1_1_17.mHide()
    this.oPgFrm.Page1.oPag.oSELECT1_1_19.visible=!this.oPgFrm.Page1.oPag.oSELECT1_1_19.mHide()
    this.oPgFrm.Page2.oPag.oFLVAR2_2_1.visible=!this.oPgFrm.Page2.oPag.oFLVAR2_2_1.mHide()
    this.oPgFrm.Page2.oPag.oFLTYP2_2_3.visible=!this.oPgFrm.Page2.oPag.oFLTYP2_2_3.mHide()
    this.oPgFrm.Page2.oPag.oFLLEN2_2_5.visible=!this.oPgFrm.Page2.oPag.oFLLEN2_2_5.mHide()
    this.oPgFrm.Page2.oPag.oFLEDI2_2_8.visible=!this.oPgFrm.Page2.oPag.oFLEDI2_2_8.mHide()
    this.oPgFrm.Page2.oPag.oFLZER2_2_10.visible=!this.oPgFrm.Page2.oPag.oFLZER2_2_10.mHide()
    this.oPgFrm.Page2.oPag.oFLFOR2_2_12.visible=!this.oPgFrm.Page2.oPag.oFLFOR2_2_12.mHide()
    this.oPgFrm.Page2.oPag.oFLARC2_2_14.visible=!this.oPgFrm.Page2.oPag.oFLARC2_2_14.mHide()
    this.oPgFrm.Page2.oPag.oFLFVL1_2_16.visible=!this.oPgFrm.Page2.oPag.oFLFVL1_2_16.mHide()
    this.oPgFrm.Page2.oPag.oFLFVL2_2_19.visible=!this.oPgFrm.Page2.oPag.oFLFVL2_2_19.mHide()
    this.oPgFrm.Page2.oPag.oFLFVL3_2_22.visible=!this.oPgFrm.Page2.oPag.oFLFVL3_2_22.mHide()
    this.oPgFrm.Page2.oPag.oFLFVL4_2_25.visible=!this.oPgFrm.Page2.oPag.oFLFVL4_2_25.mHide()
    this.oPgFrm.Page2.oPag.oFLFVL5_2_27.visible=!this.oPgFrm.Page2.oPag.oFLFVL5_2_27.mHide()
    this.oPgFrm.Page2.oPag.oFLFNT2_2_30.visible=!this.oPgFrm.Page2.oPag.oFLFNT2_2_30.mHide()
    this.oPgFrm.Page2.oPag.oFLGLF2_2_34.visible=!this.oPgFrm.Page2.oPag.oFLGLF2_2_34.mHide()
    this.oPgFrm.Page2.oPag.oFLTOP2_2_36.visible=!this.oPgFrm.Page2.oPag.oFLTOP2_2_36.mHide()
    this.oPgFrm.Page2.oPag.oFLLEF2_2_38.visible=!this.oPgFrm.Page2.oPag.oFLLEF2_2_38.mHide()
    this.oPgFrm.Page2.oPag.oFLHGT2_2_40.visible=!this.oPgFrm.Page2.oPag.oFLHGT2_2_40.mHide()
    this.oPgFrm.Page2.oPag.oFLWDT2_2_42.visible=!this.oPgFrm.Page2.oPag.oFLWDT2_2_42.mHide()
    this.oPgFrm.Page2.oPag.oSELECT2_2_44.visible=!this.oPgFrm.Page2.oPag.oSELECT2_2_44.mHide()
    this.oPgFrm.Page3.oPag.oFLVAR3_3_1.visible=!this.oPgFrm.Page3.oPag.oFLVAR3_3_1.mHide()
    this.oPgFrm.Page3.oPag.oFLTYP3_3_3.visible=!this.oPgFrm.Page3.oPag.oFLTYP3_3_3.mHide()
    this.oPgFrm.Page3.oPag.oFLLEN3_3_5.visible=!this.oPgFrm.Page3.oPag.oFLLEN3_3_5.mHide()
    this.oPgFrm.Page3.oPag.oFLEDI3_3_8.visible=!this.oPgFrm.Page3.oPag.oFLEDI3_3_8.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS1_3_10.visible=!this.oPgFrm.Page3.oPag.oFLVDS1_3_10.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS2_3_13.visible=!this.oPgFrm.Page3.oPag.oFLVDS2_3_13.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS3_3_16.visible=!this.oPgFrm.Page3.oPag.oFLVDS3_3_16.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS4_3_19.visible=!this.oPgFrm.Page3.oPag.oFLVDS4_3_19.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS5_3_22.visible=!this.oPgFrm.Page3.oPag.oFLVDS5_3_22.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS6_3_25.visible=!this.oPgFrm.Page3.oPag.oFLVDS6_3_25.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS7_3_28.visible=!this.oPgFrm.Page3.oPag.oFLVDS7_3_28.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS8_3_31.visible=!this.oPgFrm.Page3.oPag.oFLVDS8_3_31.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS9_3_34.visible=!this.oPgFrm.Page3.oPag.oFLVDS9_3_34.mHide()
    this.oPgFrm.Page3.oPag.oFLVDS10_3_37.visible=!this.oPgFrm.Page3.oPag.oFLVDS10_3_37.mHide()
    this.oPgFrm.Page3.oPag.oFLFNT3_3_40.visible=!this.oPgFrm.Page3.oPag.oFLFNT3_3_40.mHide()
    this.oPgFrm.Page3.oPag.oFLGLF3_3_44.visible=!this.oPgFrm.Page3.oPag.oFLGLF3_3_44.mHide()
    this.oPgFrm.Page3.oPag.oFLTOP3_3_46.visible=!this.oPgFrm.Page3.oPag.oFLTOP3_3_46.mHide()
    this.oPgFrm.Page3.oPag.oFLLEF3_3_48.visible=!this.oPgFrm.Page3.oPag.oFLLEF3_3_48.mHide()
    this.oPgFrm.Page3.oPag.oFLHGT3_3_50.visible=!this.oPgFrm.Page3.oPag.oFLHGT3_3_50.mHide()
    this.oPgFrm.Page3.oPag.oFLWDT3_3_52.visible=!this.oPgFrm.Page3.oPag.oFLWDT3_3_52.mHide()
    this.oPgFrm.Page3.oPag.oSELECT3_3_54.visible=!this.oPgFrm.Page3.oPag.oSELECT3_3_54.mHide()
    this.oPgFrm.Page4.oPag.oFLVAR4_4_1.visible=!this.oPgFrm.Page4.oPag.oFLVAR4_4_1.mHide()
    this.oPgFrm.Page4.oPag.oFLTTL4_4_3.visible=!this.oPgFrm.Page4.oPag.oFLTTL4_4_3.mHide()
    this.oPgFrm.Page4.oPag.oFLTYP4_4_5.visible=!this.oPgFrm.Page4.oPag.oFLTYP4_4_5.mHide()
    this.oPgFrm.Page4.oPag.oFLLEN4_4_7.visible=!this.oPgFrm.Page4.oPag.oFLLEN4_4_7.mHide()
    this.oPgFrm.Page4.oPag.oFLEDI4_4_10.visible=!this.oPgFrm.Page4.oPag.oFLEDI4_4_10.mHide()
    this.oPgFrm.Page4.oPag.oFLSEL4_4_12.visible=!this.oPgFrm.Page4.oPag.oFLSEL4_4_12.mHide()
    this.oPgFrm.Page4.oPag.oFLUNS4_4_14.visible=!this.oPgFrm.Page4.oPag.oFLUNS4_4_14.mHide()
    this.oPgFrm.Page4.oPag.oFLFNT4_4_16.visible=!this.oPgFrm.Page4.oPag.oFLFNT4_4_16.mHide()
    this.oPgFrm.Page4.oPag.oFLGLF4_4_20.visible=!this.oPgFrm.Page4.oPag.oFLGLF4_4_20.mHide()
    this.oPgFrm.Page4.oPag.oFLTOP4_4_22.visible=!this.oPgFrm.Page4.oPag.oFLTOP4_4_22.mHide()
    this.oPgFrm.Page4.oPag.oFLLEF4_4_24.visible=!this.oPgFrm.Page4.oPag.oFLLEF4_4_24.mHide()
    this.oPgFrm.Page4.oPag.oFLHGT4_4_26.visible=!this.oPgFrm.Page4.oPag.oFLHGT4_4_26.mHide()
    this.oPgFrm.Page4.oPag.oFLWDT4_4_28.visible=!this.oPgFrm.Page4.oPag.oFLWDT4_4_28.mHide()
    this.oPgFrm.Page4.oPag.oSELECT4_4_30.visible=!this.oPgFrm.Page4.oPag.oSELECT4_4_30.mHide()
    this.oPgFrm.Page6.oPag.oFLCLR6_6_1.visible=!this.oPgFrm.Page6.oPag.oFLCLR6_6_1.mHide()
    this.oPgFrm.Page5.oPag.oFLEXE5_5_1.visible=!this.oPgFrm.Page5.oPag.oFLEXE5_5_1.mHide()
    this.oPgFrm.Page5.oPag.oFLTYP5_5_4.visible=!this.oPgFrm.Page5.oPag.oFLTYP5_5_4.mHide()
    this.oPgFrm.Page5.oPag.oFLTTL5_5_7.visible=!this.oPgFrm.Page5.oPag.oFLTTL5_5_7.mHide()
    this.oPgFrm.Page5.oPag.oFLPIC5_5_10.visible=!this.oPgFrm.Page5.oPag.oFLPIC5_5_10.mHide()
    this.oPgFrm.Page5.oPag.oFLFOR5_5_13.visible=!this.oPgFrm.Page5.oPag.oFLFOR5_5_13.mHide()
    this.oPgFrm.Page5.oPag.oFORMAT5_5_14.visible=!this.oPgFrm.Page5.oPag.oFORMAT5_5_14.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_15.visible=!this.oPgFrm.Page5.oPag.oStr_5_15.mHide()
    this.oPgFrm.Page5.oPag.oFLINV5_5_17.visible=!this.oPgFrm.Page5.oPag.oFLINV5_5_17.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_18.visible=!this.oPgFrm.Page5.oPag.oStr_5_18.mHide()
    this.oPgFrm.Page5.oPag.oINVIS5_5_19.visible=!this.oPgFrm.Page5.oPag.oINVIS5_5_19.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_20.visible=!this.oPgFrm.Page5.oPag.oStr_5_20.mHide()
    this.oPgFrm.Page5.oPag.oDCLICK5_5_21.visible=!this.oPgFrm.Page5.oPag.oDCLICK5_5_21.mHide()
    this.oPgFrm.Page5.oPag.oFLSHP5_5_22.visible=!this.oPgFrm.Page5.oPag.oFLSHP5_5_22.mHide()
    this.oPgFrm.Page5.oPag.oFLEDI5_5_25.visible=!this.oPgFrm.Page5.oPag.oFLEDI5_5_25.mHide()
    this.oPgFrm.Page5.oPag.oFLUNS5_5_27.visible=!this.oPgFrm.Page5.oPag.oFLUNS5_5_27.mHide()
    this.oPgFrm.Page5.oPag.oFLPAS5_5_29.visible=!this.oPgFrm.Page5.oPag.oFLPAS5_5_29.mHide()
    this.oPgFrm.Page5.oPag.oFLPRS5_5_31.visible=!this.oPgFrm.Page5.oPag.oFLPRS5_5_31.mHide()
    this.oPgFrm.Page5.oPag.oFLLBR5_5_33.visible=!this.oPgFrm.Page5.oPag.oFLLBR5_5_33.mHide()
    this.oPgFrm.Page5.oPag.oFLCRV5_5_35.visible=!this.oPgFrm.Page5.oPag.oFLCRV5_5_35.mHide()
    this.oPgFrm.Page5.oPag.oFLCBR5_5_37.visible=!this.oPgFrm.Page5.oPag.oFLCBR5_5_37.mHide()
    this.oPgFrm.Page5.oPag.oFLCSF5_5_39.visible=!this.oPgFrm.Page5.oPag.oFLCSF5_5_39.mHide()
    this.oPgFrm.Page5.oPag.oFLFNT5_5_41.visible=!this.oPgFrm.Page5.oPag.oFLFNT5_5_41.mHide()
    this.oPgFrm.Page5.oPag.oFLGLF5_5_47.visible=!this.oPgFrm.Page5.oPag.oFLGLF5_5_47.mHide()
    this.oPgFrm.Page5.oPag.oFLTOP5_5_52.visible=!this.oPgFrm.Page5.oPag.oFLTOP5_5_52.mHide()
    this.oPgFrm.Page5.oPag.oFLLEF5_5_54.visible=!this.oPgFrm.Page5.oPag.oFLLEF5_5_54.mHide()
    this.oPgFrm.Page5.oPag.oFLHGT5_5_56.visible=!this.oPgFrm.Page5.oPag.oFLHGT5_5_56.mHide()
    this.oPgFrm.Page5.oPag.oFLWDT5_5_58.visible=!this.oPgFrm.Page5.oPag.oFLWDT5_5_58.mHide()
    this.oPgFrm.Page6.oPag.oFLBRW6_6_7.visible=!this.oPgFrm.Page6.oPag.oFLBRW6_6_7.mHide()
    this.oPgFrm.Page6.oPag.oFLINI6_6_9.visible=!this.oPgFrm.Page6.oPag.oFLINI6_6_9.mHide()
    this.oPgFrm.Page6.oPag.oFLFIN6_6_12.visible=!this.oPgFrm.Page6.oPag.oFLFIN6_6_12.mHide()
    this.oPgFrm.Page6.oPag.oFLSTY6_6_16.visible=!this.oPgFrm.Page6.oPag.oFLSTY6_6_16.mHide()
    this.oPgFrm.Page6.oPag.oFLCUR6_6_18.visible=!this.oPgFrm.Page6.oPag.oFLCUR6_6_18.mHide()
    this.oPgFrm.Page7.oPag.oFLEXE7_7_1.visible=!this.oPgFrm.Page7.oPag.oFLEXE7_7_1.mHide()
    this.oPgFrm.Page7.oPag.oFLTYP7_7_4.visible=!this.oPgFrm.Page7.oPag.oFLTYP7_7_4.mHide()
    this.oPgFrm.Page7.oPag.oFLTTL7_7_7.visible=!this.oPgFrm.Page7.oPag.oFLTTL7_7_7.mHide()
    this.oPgFrm.Page7.oPag.oFLPIC7_7_10.visible=!this.oPgFrm.Page7.oPag.oFLPIC7_7_10.mHide()
    this.oPgFrm.Page7.oPag.oFLTOP7_7_16.visible=!this.oPgFrm.Page7.oPag.oFLTOP7_7_16.mHide()
    this.oPgFrm.Page7.oPag.oFLLEF7_7_18.visible=!this.oPgFrm.Page7.oPag.oFLLEF7_7_18.mHide()
    this.oPgFrm.Page7.oPag.oFLHGT7_7_20.visible=!this.oPgFrm.Page7.oPag.oFLHGT7_7_20.mHide()
    this.oPgFrm.Page7.oPag.oFLWDT7_7_22.visible=!this.oPgFrm.Page7.oPag.oFLWDT7_7_22.mHide()
    this.oPgFrm.Page5.oPag.oSELECT5_5_60.visible=!this.oPgFrm.Page5.oPag.oSELECT5_5_60.mHide()
    this.oPgFrm.Page6.oPag.oSELECT6_6_20.visible=!this.oPgFrm.Page6.oPag.oSELECT6_6_20.mHide()
    this.oPgFrm.Page7.oPag.oSELECT7_7_28.visible=!this.oPgFrm.Page7.oPag.oSELECT7_7_28.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_93.visible=!this.oPgFrm.Page5.oPag.oBtn_5_93.mHide()
    this.oPgFrm.Page7.oPag.oFLFOR7_7_40.visible=!this.oPgFrm.Page7.oPag.oFLFOR7_7_40.mHide()
    this.oPgFrm.Page7.oPag.oFORMAT7_7_41.visible=!this.oPgFrm.Page7.oPag.oFORMAT7_7_41.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_42.visible=!this.oPgFrm.Page7.oPag.oStr_7_42.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page5.oPag.oObj_5_11.Event(cEvent)
      .oPgFrm.Page7.oPag.oObj_7_11.Event(cEvent)
        if lower(cEvent)==lower("FormLoad")
          .Calculate_NUJMZIXJLT()
          bRefresh=.t.
        endif
      .oPgFrm.Page5.oPag.zBtn.Event(cEvent)
      .oPgFrm.Page1.oPag.zLbl.Event(cEvent)
      .oPgFrm.Page2.oPag.zTxt.Event(cEvent)
      .oPgFrm.Page3.oPag.zCombo.Event(cEvent)
      .oPgFrm.Page4.oPag.zCheck.Event(cEvent)
      .oPgFrm.Page6.oPag.zLine.Event(cEvent)
      .oPgFrm.Page7.oPag.zBitmap.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_XPFMZBSXHT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_KTLLUFMFPX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 2") or lower(cEvent)==lower("w_zTxt selected")
          .Calculate_YNJQIYAHFD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 3")
          .Calculate_XAWJQPDYDV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 3") or lower(cEvent)==lower("w_zCombo selected")
          .Calculate_ISVLGAUKIU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 5")
          .Calculate_MGOVIGEAYP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 5") or lower(cEvent)==lower("w_zBtn selected")
          .Calculate_KJMNNUFXYO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ChangeColor1")
          .Calculate_GUQYYQOICD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ChangeColor2")
          .Calculate_RPKADVNESJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 1")
          .Calculate_LFRATFCOYG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 1") or lower(cEvent)==lower("w_zLbl selected")
          .Calculate_GTCHNDARTF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 4")
          .Calculate_OYTJXLDJXK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 4") or lower(cEvent)==lower("w_zCheck selected")
          .Calculate_HQOLEKQACJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 6")
          .Calculate_LIZFGPQURJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 6") or lower(cEvent)==lower("w_zLine selected")
          .Calculate_VJNUCAVWXJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 7")
          .Calculate_BNBRUKSHYG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 7") or lower(cEvent)==lower("w_zBitmap selected")
          .Calculate_DDMKRMMBEC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ChangeColor3")
          .Calculate_XVYPUDPXDQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.lbl.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_UJQLSMFITJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.txt.Event(cEvent)
      .oPgFrm.Page3.oPag.combo.Event(cEvent)
      .oPgFrm.Page4.oPag.check.Event(cEvent)
      .oPgFrm.Page6.oPag.line.Event(cEvent)
      .oPgFrm.Page7.oPag.bitmap.Event(cEvent)
      .oPgFrm.Page5.oPag.btn.Event(cEvent)
        if lower(cEvent)==lower("w_INIANGLE6 Changed")
          .Calculate_XGLJGBAIUV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_INISIZE6 Changed")
          .Calculate_UFCQTVZLWL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_FINANGLE6 Changed")
          .Calculate_NQBHWBSREA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_FINSIZE6 Changed")
          .Calculate_SOPDXSNRJO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FontColor1")
          .Calculate_DXEIVZCXFY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FontColor2")
          .Calculate_WWGSZOPOUF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FontColor3")
          .Calculate_SSXFEMLQXX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FontColor4")
          .Calculate_FXVCCFFSJJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FontColor5")
          .Calculate_IASTFIZVEA()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- cp_objmaskoptions
    IF cEvent=='ChangeColor1' or cEvent=='Init'
       local l_obj
       l_obj = This.GetCtrl("w_BRDCLR5")
       l_obj.DisabledBackcolor = This.w_BRDCLR5
       l_obj.DisabledForecolor = This.w_BRDCLR5
       l_obj=.null.
    ENDIF
    
    IF cEvent=='ChangeColor2' or cEvent=='Init'
       local l_obj
       l_obj = This.GetCtrl("w_BCKCLR5")
       l_obj.DisabledBackcolor = This.w_BCKCLR5
       l_obj.DisabledForecolor = This.w_BCKCLR5
       l_obj=.null.
    ENDIF
    
    IF cEvent=='ChangeColor3' or cEvent=='Init'
       local l_obj
       l_obj = This.GetCtrl("w_COLOR6")
       l_obj.DisabledBackcolor = This.w_COLOR6
       l_obj.DisabledForecolor = This.w_COLOR6
       l_obj=.null.
    ENDIF
    
    IF Left(cEvent,9)=='FontColor' or cEvent=='Init'
       local l_cNum
       If cEvent=='Init'
          local l_i
          l_i = 1
          Do While l_i <= 5
            l_cNum = Transform(l_i)
            cp_FontColor(This,l_cNum)
            l_i = l_i +1
          EndDo
       Else
         l_cNum = Right(cEvent,1)
         cp_FontColor(This,l_cNum)
       EndIf
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ARCHIVE2
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpttbls_IDX,3]
    i_lTable = "cpttbls"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpttbls_IDX,2], .t., this.cpttbls_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpttbls_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCHIVE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpttbls')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FileName like "+cp_ToStrODBC(trim(this.w_ARCHIVE2)+"%");

          i_ret=cp_SQL(i_nConn,"select FileName";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FileName","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FileName',trim(this.w_ARCHIVE2))
          select FileName;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FileName into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCHIVE2)==trim(_Link_.FileName) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCHIVE2) and !this.bDontReportError
            deferred_cp_zoom('cpttbls','*','FileName',cp_AbsName(oSource.parent,'oARCHIVE2_2_15'),i_cWhere,'',"Tabelle",'cp_objmaskoptions.cpttbls_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FileName";
                     +" from "+i_cTable+" "+i_lTable+" where FileName="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FileName',oSource.xKey(1))
            select FileName;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCHIVE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FileName";
                   +" from "+i_cTable+" "+i_lTable+" where FileName="+cp_ToStrODBC(this.w_ARCHIVE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FileName',this.w_ARCHIVE2)
            select FileName;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCHIVE2 = NVL(_Link_.FileName,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ARCHIVE2 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpttbls_IDX,2])+'\'+cp_ToStr(_Link_.FileName,1)
      cp_ShowWarn(i_cKey,this.cpttbls_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCHIVE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLTTL1_1_1.RadioValue()==this.w_FLTTL1)
      this.oPgFrm.Page1.oPag.oFLTTL1_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTITLE1_1_2.value==this.w_TITLE1)
      this.oPgFrm.Page1.oPag.oTITLE1_1_2.value=this.w_TITLE1
    endif
    if not(this.oPgFrm.Page1.oPag.oFLALG1_1_3.RadioValue()==this.w_FLALG1)
      this.oPgFrm.Page1.oPag.oFLALG1_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oALIGN1_1_4.RadioValue()==this.w_ALIGN1)
      this.oPgFrm.Page1.oPag.oALIGN1_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLFNT1_1_5.RadioValue()==this.w_FLFNT1)
      this.oPgFrm.Page1.oPag.oFLFNT1_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFNTNAME1_1_6.value==this.w_FNTNAME1)
      this.oPgFrm.Page1.oPag.oFNTNAME1_1_6.value=this.w_FNTNAME1
    endif
    if not(this.oPgFrm.Page1.oPag.oFNTSIZE1_1_7.value==this.w_FNTSIZE1)
      this.oPgFrm.Page1.oPag.oFNTSIZE1_1_7.value=this.w_FNTSIZE1
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGLF1_1_9.RadioValue()==this.w_FLGLF1)
      this.oPgFrm.Page1.oPag.oFLGLF1_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGLBFONT1_1_10.RadioValue()==this.w_GLBFONT1)
      this.oPgFrm.Page1.oPag.oGLBFONT1_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTOP1_1_11.RadioValue()==this.w_FLTOP1)
      this.oPgFrm.Page1.oPag.oFLTOP1_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOP1_1_12.value==this.w_TOP1)
      this.oPgFrm.Page1.oPag.oTOP1_1_12.value=this.w_TOP1
    endif
    if not(this.oPgFrm.Page1.oPag.oFLLEF1_1_13.RadioValue()==this.w_FLLEF1)
      this.oPgFrm.Page1.oPag.oFLLEF1_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLEFT1_1_14.value==this.w_LEFT1)
      this.oPgFrm.Page1.oPag.oLEFT1_1_14.value=this.w_LEFT1
    endif
    if not(this.oPgFrm.Page1.oPag.oFLHGT1_1_15.RadioValue()==this.w_FLHGT1)
      this.oPgFrm.Page1.oPag.oFLHGT1_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oHEIGHT1_1_16.value==this.w_HEIGHT1)
      this.oPgFrm.Page1.oPag.oHEIGHT1_1_16.value=this.w_HEIGHT1
    endif
    if not(this.oPgFrm.Page1.oPag.oFLWDT1_1_17.RadioValue()==this.w_FLWDT1)
      this.oPgFrm.Page1.oPag.oFLWDT1_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oWIDTH1_1_18.value==this.w_WIDTH1)
      this.oPgFrm.Page1.oPag.oWIDTH1_1_18.value=this.w_WIDTH1
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECT1_1_19.RadioValue()==this.w_SELECT1)
      this.oPgFrm.Page1.oPag.oSELECT1_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLVAR2_2_1.RadioValue()==this.w_FLVAR2)
      this.oPgFrm.Page2.oPag.oFLVAR2_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVAR2_2_2.value==this.w_VAR2)
      this.oPgFrm.Page2.oPag.oVAR2_2_2.value=this.w_VAR2
    endif
    if not(this.oPgFrm.Page2.oPag.oFLTYP2_2_3.RadioValue()==this.w_FLTYP2)
      this.oPgFrm.Page2.oPag.oFLTYP2_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTYPE_V_2_4.RadioValue()==this.w_TYPE_V)
      this.oPgFrm.Page2.oPag.oTYPE_V_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLLEN2_2_5.RadioValue()==this.w_FLLEN2)
      this.oPgFrm.Page2.oPag.oFLLEN2_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oLEN2_2_6.value==this.w_LEN2)
      this.oPgFrm.Page2.oPag.oLEN2_2_6.value=this.w_LEN2
    endif
    if not(this.oPgFrm.Page2.oPag.oDEC2_2_7.value==this.w_DEC2)
      this.oPgFrm.Page2.oPag.oDEC2_2_7.value=this.w_DEC2
    endif
    if not(this.oPgFrm.Page2.oPag.oFLEDI2_2_8.RadioValue()==this.w_FLEDI2)
      this.oPgFrm.Page2.oPag.oFLEDI2_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEDIT2_2_9.RadioValue()==this.w_EDIT2)
      this.oPgFrm.Page2.oPag.oEDIT2_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLZER2_2_10.RadioValue()==this.w_FLZER2)
      this.oPgFrm.Page2.oPag.oFLZER2_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oZERO2_2_11.RadioValue()==this.w_ZERO2)
      this.oPgFrm.Page2.oPag.oZERO2_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLFOR2_2_12.RadioValue()==this.w_FLFOR2)
      this.oPgFrm.Page2.oPag.oFLFOR2_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPICTURE2_2_13.value==this.w_PICTURE2)
      this.oPgFrm.Page2.oPag.oPICTURE2_2_13.value=this.w_PICTURE2
    endif
    if not(this.oPgFrm.Page2.oPag.oFLARC2_2_14.RadioValue()==this.w_FLARC2)
      this.oPgFrm.Page2.oPag.oFLARC2_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARCHIVE2_2_15.value==this.w_ARCHIVE2)
      this.oPgFrm.Page2.oPag.oARCHIVE2_2_15.value=this.w_ARCHIVE2
    endif
    if not(this.oPgFrm.Page2.oPag.oFLFVL1_2_16.RadioValue()==this.w_FLFVL1)
      this.oPgFrm.Page2.oPag.oFLFVL1_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIELD1_2_17.value==this.w_FIELD1)
      this.oPgFrm.Page2.oPag.oFIELD1_2_17.value=this.w_FIELD1
    endif
    if not(this.oPgFrm.Page2.oPag.oVALUE1_V_2_18.value==this.w_VALUE1_V)
      this.oPgFrm.Page2.oPag.oVALUE1_V_2_18.value=this.w_VALUE1_V
    endif
    if not(this.oPgFrm.Page2.oPag.oFLFVL2_2_19.RadioValue()==this.w_FLFVL2)
      this.oPgFrm.Page2.oPag.oFLFVL2_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIELD2_2_20.value==this.w_FIELD2)
      this.oPgFrm.Page2.oPag.oFIELD2_2_20.value=this.w_FIELD2
    endif
    if not(this.oPgFrm.Page2.oPag.oVALUE2_V_2_21.value==this.w_VALUE2_V)
      this.oPgFrm.Page2.oPag.oVALUE2_V_2_21.value=this.w_VALUE2_V
    endif
    if not(this.oPgFrm.Page2.oPag.oFLFVL3_2_22.RadioValue()==this.w_FLFVL3)
      this.oPgFrm.Page2.oPag.oFLFVL3_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIELD3_2_23.value==this.w_FIELD3)
      this.oPgFrm.Page2.oPag.oFIELD3_2_23.value=this.w_FIELD3
    endif
    if not(this.oPgFrm.Page2.oPag.oVALUE3_V_2_24.value==this.w_VALUE3_V)
      this.oPgFrm.Page2.oPag.oVALUE3_V_2_24.value=this.w_VALUE3_V
    endif
    if not(this.oPgFrm.Page2.oPag.oFLFVL4_2_25.RadioValue()==this.w_FLFVL4)
      this.oPgFrm.Page2.oPag.oFLFVL4_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIELD4_2_26.value==this.w_FIELD4)
      this.oPgFrm.Page2.oPag.oFIELD4_2_26.value=this.w_FIELD4
    endif
    if not(this.oPgFrm.Page2.oPag.oFLFVL5_2_27.RadioValue()==this.w_FLFVL5)
      this.oPgFrm.Page2.oPag.oFLFVL5_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIELD5_2_28.value==this.w_FIELD5)
      this.oPgFrm.Page2.oPag.oFIELD5_2_28.value=this.w_FIELD5
    endif
    if not(this.oPgFrm.Page2.oPag.oVALUE5_V_2_29.value==this.w_VALUE5_V)
      this.oPgFrm.Page2.oPag.oVALUE5_V_2_29.value=this.w_VALUE5_V
    endif
    if not(this.oPgFrm.Page2.oPag.oFLFNT2_2_30.RadioValue()==this.w_FLFNT2)
      this.oPgFrm.Page2.oPag.oFLFNT2_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFNTNAME2_2_31.value==this.w_FNTNAME2)
      this.oPgFrm.Page2.oPag.oFNTNAME2_2_31.value=this.w_FNTNAME2
    endif
    if not(this.oPgFrm.Page2.oPag.oFNTSIZE2_2_32.value==this.w_FNTSIZE2)
      this.oPgFrm.Page2.oPag.oFNTSIZE2_2_32.value=this.w_FNTSIZE2
    endif
    if not(this.oPgFrm.Page2.oPag.oFLGLF2_2_34.RadioValue()==this.w_FLGLF2)
      this.oPgFrm.Page2.oPag.oFLGLF2_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGLBFONT2_2_35.RadioValue()==this.w_GLBFONT2)
      this.oPgFrm.Page2.oPag.oGLBFONT2_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLTOP2_2_36.RadioValue()==this.w_FLTOP2)
      this.oPgFrm.Page2.oPag.oFLTOP2_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTOP2_2_37.value==this.w_TOP2)
      this.oPgFrm.Page2.oPag.oTOP2_2_37.value=this.w_TOP2
    endif
    if not(this.oPgFrm.Page2.oPag.oFLLEF2_2_38.RadioValue()==this.w_FLLEF2)
      this.oPgFrm.Page2.oPag.oFLLEF2_2_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oLEFT2_2_39.value==this.w_LEFT2)
      this.oPgFrm.Page2.oPag.oLEFT2_2_39.value=this.w_LEFT2
    endif
    if not(this.oPgFrm.Page2.oPag.oFLHGT2_2_40.RadioValue()==this.w_FLHGT2)
      this.oPgFrm.Page2.oPag.oFLHGT2_2_40.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oHEIGHT2_2_41.value==this.w_HEIGHT2)
      this.oPgFrm.Page2.oPag.oHEIGHT2_2_41.value=this.w_HEIGHT2
    endif
    if not(this.oPgFrm.Page2.oPag.oFLWDT2_2_42.RadioValue()==this.w_FLWDT2)
      this.oPgFrm.Page2.oPag.oFLWDT2_2_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWIDTH2_2_43.value==this.w_WIDTH2)
      this.oPgFrm.Page2.oPag.oWIDTH2_2_43.value=this.w_WIDTH2
    endif
    if not(this.oPgFrm.Page2.oPag.oSELECT2_2_44.RadioValue()==this.w_SELECT2)
      this.oPgFrm.Page2.oPag.oSELECT2_2_44.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVAR3_3_1.RadioValue()==this.w_FLVAR3)
      this.oPgFrm.Page3.oPag.oFLVAR3_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVAR3_3_2.value==this.w_VAR3)
      this.oPgFrm.Page3.oPag.oVAR3_3_2.value=this.w_VAR3
    endif
    if not(this.oPgFrm.Page3.oPag.oFLTYP3_3_3.RadioValue()==this.w_FLTYP3)
      this.oPgFrm.Page3.oPag.oFLTYP3_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTYPE_CB_3_4.RadioValue()==this.w_TYPE_CB)
      this.oPgFrm.Page3.oPag.oTYPE_CB_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFLLEN3_3_5.RadioValue()==this.w_FLLEN3)
      this.oPgFrm.Page3.oPag.oFLLEN3_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oLEN3_3_6.value==this.w_LEN3)
      this.oPgFrm.Page3.oPag.oLEN3_3_6.value=this.w_LEN3
    endif
    if not(this.oPgFrm.Page3.oPag.oDEC3_3_7.value==this.w_DEC3)
      this.oPgFrm.Page3.oPag.oDEC3_3_7.value=this.w_DEC3
    endif
    if not(this.oPgFrm.Page3.oPag.oFLEDI3_3_8.RadioValue()==this.w_FLEDI3)
      this.oPgFrm.Page3.oPag.oFLEDI3_3_8.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oEDIT3_3_9.RadioValue()==this.w_EDIT3)
      this.oPgFrm.Page3.oPag.oEDIT3_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS1_3_10.RadioValue()==this.w_FLVDS1)
      this.oPgFrm.Page3.oPag.oFLVDS1_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE1_C_3_11.value==this.w_VALUE1_C)
      this.oPgFrm.Page3.oPag.oVALUE1_C_3_11.value=this.w_VALUE1_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI1_C_3_12.value==this.w_DESCRI1_C)
      this.oPgFrm.Page3.oPag.oDESCRI1_C_3_12.value=this.w_DESCRI1_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS2_3_13.RadioValue()==this.w_FLVDS2)
      this.oPgFrm.Page3.oPag.oFLVDS2_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE2_C_3_14.value==this.w_VALUE2_C)
      this.oPgFrm.Page3.oPag.oVALUE2_C_3_14.value=this.w_VALUE2_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI2_C_3_15.value==this.w_DESCRI2_C)
      this.oPgFrm.Page3.oPag.oDESCRI2_C_3_15.value=this.w_DESCRI2_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS3_3_16.RadioValue()==this.w_FLVDS3)
      this.oPgFrm.Page3.oPag.oFLVDS3_3_16.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE3_C_3_17.value==this.w_VALUE3_C)
      this.oPgFrm.Page3.oPag.oVALUE3_C_3_17.value=this.w_VALUE3_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI3_C_3_18.value==this.w_DESCRI3_C)
      this.oPgFrm.Page3.oPag.oDESCRI3_C_3_18.value=this.w_DESCRI3_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS4_3_19.RadioValue()==this.w_FLVDS4)
      this.oPgFrm.Page3.oPag.oFLVDS4_3_19.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE4_C_3_20.value==this.w_VALUE4_C)
      this.oPgFrm.Page3.oPag.oVALUE4_C_3_20.value=this.w_VALUE4_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI4_C_3_21.value==this.w_DESCRI4_C)
      this.oPgFrm.Page3.oPag.oDESCRI4_C_3_21.value=this.w_DESCRI4_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS5_3_22.RadioValue()==this.w_FLVDS5)
      this.oPgFrm.Page3.oPag.oFLVDS5_3_22.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE5_C_3_23.value==this.w_VALUE5_C)
      this.oPgFrm.Page3.oPag.oVALUE5_C_3_23.value=this.w_VALUE5_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI5_C_3_24.value==this.w_DESCRI5_C)
      this.oPgFrm.Page3.oPag.oDESCRI5_C_3_24.value=this.w_DESCRI5_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS6_3_25.RadioValue()==this.w_FLVDS6)
      this.oPgFrm.Page3.oPag.oFLVDS6_3_25.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE6_C_3_26.value==this.w_VALUE6_C)
      this.oPgFrm.Page3.oPag.oVALUE6_C_3_26.value=this.w_VALUE6_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI6_C_3_27.value==this.w_DESCRI6_C)
      this.oPgFrm.Page3.oPag.oDESCRI6_C_3_27.value=this.w_DESCRI6_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS7_3_28.RadioValue()==this.w_FLVDS7)
      this.oPgFrm.Page3.oPag.oFLVDS7_3_28.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE7_C_3_29.value==this.w_VALUE7_C)
      this.oPgFrm.Page3.oPag.oVALUE7_C_3_29.value=this.w_VALUE7_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI7_C_3_30.value==this.w_DESCRI7_C)
      this.oPgFrm.Page3.oPag.oDESCRI7_C_3_30.value=this.w_DESCRI7_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS8_3_31.RadioValue()==this.w_FLVDS8)
      this.oPgFrm.Page3.oPag.oFLVDS8_3_31.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE8_C_3_32.value==this.w_VALUE8_C)
      this.oPgFrm.Page3.oPag.oVALUE8_C_3_32.value=this.w_VALUE8_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI8_C_3_33.value==this.w_DESCRI8_C)
      this.oPgFrm.Page3.oPag.oDESCRI8_C_3_33.value=this.w_DESCRI8_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS9_3_34.RadioValue()==this.w_FLVDS9)
      this.oPgFrm.Page3.oPag.oFLVDS9_3_34.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE9_C_3_35.value==this.w_VALUE9_C)
      this.oPgFrm.Page3.oPag.oVALUE9_C_3_35.value=this.w_VALUE9_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI9_C_3_36.value==this.w_DESCRI9_C)
      this.oPgFrm.Page3.oPag.oDESCRI9_C_3_36.value=this.w_DESCRI9_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVDS10_3_37.RadioValue()==this.w_FLVDS10)
      this.oPgFrm.Page3.oPag.oFLVDS10_3_37.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVALUE10_C_3_38.value==this.w_VALUE10_C)
      this.oPgFrm.Page3.oPag.oVALUE10_C_3_38.value=this.w_VALUE10_C
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI10_C_3_39.value==this.w_DESCRI10_C)
      this.oPgFrm.Page3.oPag.oDESCRI10_C_3_39.value=this.w_DESCRI10_C
    endif
    if not(this.oPgFrm.Page3.oPag.oFLFNT3_3_40.RadioValue()==this.w_FLFNT3)
      this.oPgFrm.Page3.oPag.oFLFNT3_3_40.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFNTNAME3_3_41.value==this.w_FNTNAME3)
      this.oPgFrm.Page3.oPag.oFNTNAME3_3_41.value=this.w_FNTNAME3
    endif
    if not(this.oPgFrm.Page3.oPag.oFNTSIZE3_3_42.value==this.w_FNTSIZE3)
      this.oPgFrm.Page3.oPag.oFNTSIZE3_3_42.value=this.w_FNTSIZE3
    endif
    if not(this.oPgFrm.Page3.oPag.oFLGLF3_3_44.RadioValue()==this.w_FLGLF3)
      this.oPgFrm.Page3.oPag.oFLGLF3_3_44.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGLBFONT3_3_45.RadioValue()==this.w_GLBFONT3)
      this.oPgFrm.Page3.oPag.oGLBFONT3_3_45.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFLTOP3_3_46.RadioValue()==this.w_FLTOP3)
      this.oPgFrm.Page3.oPag.oFLTOP3_3_46.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTOP3_3_47.value==this.w_TOP3)
      this.oPgFrm.Page3.oPag.oTOP3_3_47.value=this.w_TOP3
    endif
    if not(this.oPgFrm.Page3.oPag.oFLLEF3_3_48.RadioValue()==this.w_FLLEF3)
      this.oPgFrm.Page3.oPag.oFLLEF3_3_48.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oLEFT3_3_49.value==this.w_LEFT3)
      this.oPgFrm.Page3.oPag.oLEFT3_3_49.value=this.w_LEFT3
    endif
    if not(this.oPgFrm.Page3.oPag.oFLHGT3_3_50.RadioValue()==this.w_FLHGT3)
      this.oPgFrm.Page3.oPag.oFLHGT3_3_50.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oHEIGHT3_3_51.value==this.w_HEIGHT3)
      this.oPgFrm.Page3.oPag.oHEIGHT3_3_51.value=this.w_HEIGHT3
    endif
    if not(this.oPgFrm.Page3.oPag.oFLWDT3_3_52.RadioValue()==this.w_FLWDT3)
      this.oPgFrm.Page3.oPag.oFLWDT3_3_52.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oWIDTH3_3_53.value==this.w_WIDTH3)
      this.oPgFrm.Page3.oPag.oWIDTH3_3_53.value=this.w_WIDTH3
    endif
    if not(this.oPgFrm.Page3.oPag.oSELECT3_3_54.RadioValue()==this.w_SELECT3)
      this.oPgFrm.Page3.oPag.oSELECT3_3_54.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFLVAR4_4_1.RadioValue()==this.w_FLVAR4)
      this.oPgFrm.Page4.oPag.oFLVAR4_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oVAR4_4_2.value==this.w_VAR4)
      this.oPgFrm.Page4.oPag.oVAR4_4_2.value=this.w_VAR4
    endif
    if not(this.oPgFrm.Page4.oPag.oFLTTL4_4_3.RadioValue()==this.w_FLTTL4)
      this.oPgFrm.Page4.oPag.oFLTTL4_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTITLE4_4_4.value==this.w_TITLE4)
      this.oPgFrm.Page4.oPag.oTITLE4_4_4.value=this.w_TITLE4
    endif
    if not(this.oPgFrm.Page4.oPag.oFLTYP4_4_5.RadioValue()==this.w_FLTYP4)
      this.oPgFrm.Page4.oPag.oFLTYP4_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTYPE_CH_4_6.RadioValue()==this.w_TYPE_CH)
      this.oPgFrm.Page4.oPag.oTYPE_CH_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFLLEN4_4_7.RadioValue()==this.w_FLLEN4)
      this.oPgFrm.Page4.oPag.oFLLEN4_4_7.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oLEN4_4_8.value==this.w_LEN4)
      this.oPgFrm.Page4.oPag.oLEN4_4_8.value=this.w_LEN4
    endif
    if not(this.oPgFrm.Page4.oPag.oDEC4_4_9.value==this.w_DEC4)
      this.oPgFrm.Page4.oPag.oDEC4_4_9.value=this.w_DEC4
    endif
    if not(this.oPgFrm.Page4.oPag.oFLEDI4_4_10.RadioValue()==this.w_FLEDI4)
      this.oPgFrm.Page4.oPag.oFLEDI4_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oEDIT4_4_11.RadioValue()==this.w_EDIT4)
      this.oPgFrm.Page4.oPag.oEDIT4_4_11.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFLSEL4_4_12.RadioValue()==this.w_FLSEL4)
      this.oPgFrm.Page4.oPag.oFLSEL4_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oSELECTED4_4_13.value==this.w_SELECTED4)
      this.oPgFrm.Page4.oPag.oSELECTED4_4_13.value=this.w_SELECTED4
    endif
    if not(this.oPgFrm.Page4.oPag.oFLUNS4_4_14.RadioValue()==this.w_FLUNS4)
      this.oPgFrm.Page4.oPag.oFLUNS4_4_14.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oUNSELECTED4_4_15.value==this.w_UNSELECTED4)
      this.oPgFrm.Page4.oPag.oUNSELECTED4_4_15.value=this.w_UNSELECTED4
    endif
    if not(this.oPgFrm.Page4.oPag.oFLFNT4_4_16.RadioValue()==this.w_FLFNT4)
      this.oPgFrm.Page4.oPag.oFLFNT4_4_16.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFNTNAME4_4_17.value==this.w_FNTNAME4)
      this.oPgFrm.Page4.oPag.oFNTNAME4_4_17.value=this.w_FNTNAME4
    endif
    if not(this.oPgFrm.Page4.oPag.oFNTSIZE4_4_18.value==this.w_FNTSIZE4)
      this.oPgFrm.Page4.oPag.oFNTSIZE4_4_18.value=this.w_FNTSIZE4
    endif
    if not(this.oPgFrm.Page4.oPag.oFLGLF4_4_20.RadioValue()==this.w_FLGLF4)
      this.oPgFrm.Page4.oPag.oFLGLF4_4_20.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGLBFONT4_4_21.RadioValue()==this.w_GLBFONT4)
      this.oPgFrm.Page4.oPag.oGLBFONT4_4_21.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFLTOP4_4_22.RadioValue()==this.w_FLTOP4)
      this.oPgFrm.Page4.oPag.oFLTOP4_4_22.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTOP4_4_23.value==this.w_TOP4)
      this.oPgFrm.Page4.oPag.oTOP4_4_23.value=this.w_TOP4
    endif
    if not(this.oPgFrm.Page4.oPag.oFLLEF4_4_24.RadioValue()==this.w_FLLEF4)
      this.oPgFrm.Page4.oPag.oFLLEF4_4_24.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oLEFT4_4_25.value==this.w_LEFT4)
      this.oPgFrm.Page4.oPag.oLEFT4_4_25.value=this.w_LEFT4
    endif
    if not(this.oPgFrm.Page4.oPag.oFLHGT4_4_26.RadioValue()==this.w_FLHGT4)
      this.oPgFrm.Page4.oPag.oFLHGT4_4_26.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oHEIGHT4_4_27.value==this.w_HEIGHT4)
      this.oPgFrm.Page4.oPag.oHEIGHT4_4_27.value=this.w_HEIGHT4
    endif
    if not(this.oPgFrm.Page4.oPag.oFLWDT4_4_28.RadioValue()==this.w_FLWDT4)
      this.oPgFrm.Page4.oPag.oFLWDT4_4_28.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oWIDTH4_4_29.value==this.w_WIDTH4)
      this.oPgFrm.Page4.oPag.oWIDTH4_4_29.value=this.w_WIDTH4
    endif
    if not(this.oPgFrm.Page4.oPag.oSELECT4_4_30.RadioValue()==this.w_SELECT4)
      this.oPgFrm.Page4.oPag.oSELECT4_4_30.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oFLCLR6_6_1.RadioValue()==this.w_FLCLR6)
      this.oPgFrm.Page6.oPag.oFLCLR6_6_1.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLEXE5_5_1.RadioValue()==this.w_FLEXE5)
      this.oPgFrm.Page5.oPag.oFLEXE5_5_1.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oEXEC5_5_3.value==this.w_EXEC5)
      this.oPgFrm.Page5.oPag.oEXEC5_5_3.value=this.w_EXEC5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLTYP5_5_4.RadioValue()==this.w_FLTYP5)
      this.oPgFrm.Page5.oPag.oFLTYP5_5_4.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oTYPE_BTN_5_6.RadioValue()==this.w_TYPE_BTN)
      this.oPgFrm.Page5.oPag.oTYPE_BTN_5_6.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLTTL5_5_7.RadioValue()==this.w_FLTTL5)
      this.oPgFrm.Page5.oPag.oFLTTL5_5_7.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oTITLE5_5_9.value==this.w_TITLE5)
      this.oPgFrm.Page5.oPag.oTITLE5_5_9.value=this.w_TITLE5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLPIC5_5_10.RadioValue()==this.w_FLPIC5)
      this.oPgFrm.Page5.oPag.oFLPIC5_5_10.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPICTURE5_5_12.value==this.w_PICTURE5)
      this.oPgFrm.Page5.oPag.oPICTURE5_5_12.value=this.w_PICTURE5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLFOR5_5_13.RadioValue()==this.w_FLFOR5)
      this.oPgFrm.Page5.oPag.oFLFOR5_5_13.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFORMAT5_5_14.RadioValue()==this.w_FORMAT5)
      this.oPgFrm.Page5.oPag.oFORMAT5_5_14.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLINV5_5_17.RadioValue()==this.w_FLINV5)
      this.oPgFrm.Page5.oPag.oFLINV5_5_17.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oINVIS5_5_19.RadioValue()==this.w_INVIS5)
      this.oPgFrm.Page5.oPag.oINVIS5_5_19.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oDCLICK5_5_21.RadioValue()==this.w_DCLICK5)
      this.oPgFrm.Page5.oPag.oDCLICK5_5_21.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLSHP5_5_22.RadioValue()==this.w_FLSHP5)
      this.oPgFrm.Page5.oPag.oFLSHP5_5_22.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oSHAPE5_5_23.RadioValue()==this.w_SHAPE5)
      this.oPgFrm.Page5.oPag.oSHAPE5_5_23.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLEDI5_5_25.RadioValue()==this.w_FLEDI5)
      this.oPgFrm.Page5.oPag.oFLEDI5_5_25.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oEDITABLE5_5_26.value==this.w_EDITABLE5)
      this.oPgFrm.Page5.oPag.oEDITABLE5_5_26.value=this.w_EDITABLE5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLUNS5_5_27.RadioValue()==this.w_FLUNS5)
      this.oPgFrm.Page5.oPag.oFLUNS5_5_27.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oUNSEL5_5_28.value==this.w_UNSEL5)
      this.oPgFrm.Page5.oPag.oUNSEL5_5_28.value=this.w_UNSEL5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLPAS5_5_29.RadioValue()==this.w_FLPAS5)
      this.oPgFrm.Page5.oPag.oFLPAS5_5_29.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPASMOU5_5_30.value==this.w_PASMOU5)
      this.oPgFrm.Page5.oPag.oPASMOU5_5_30.value=this.w_PASMOU5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLPRS5_5_31.RadioValue()==this.w_FLPRS5)
      this.oPgFrm.Page5.oPag.oFLPRS5_5_31.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPRESSMOU5_5_32.value==this.w_PRESSMOU5)
      this.oPgFrm.Page5.oPag.oPRESSMOU5_5_32.value=this.w_PRESSMOU5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLLBR5_5_33.RadioValue()==this.w_FLLBR5)
      this.oPgFrm.Page5.oPag.oFLLBR5_5_33.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oBRDWID5_5_34.value==this.w_BRDWID5)
      this.oPgFrm.Page5.oPag.oBRDWID5_5_34.value=this.w_BRDWID5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLCRV5_5_35.RadioValue()==this.w_FLCRV5)
      this.oPgFrm.Page5.oPag.oFLCRV5_5_35.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oRADIUS5_5_36.value==this.w_RADIUS5)
      this.oPgFrm.Page5.oPag.oRADIUS5_5_36.value=this.w_RADIUS5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLCBR5_5_37.RadioValue()==this.w_FLCBR5)
      this.oPgFrm.Page5.oPag.oFLCBR5_5_37.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLCSF5_5_39.RadioValue()==this.w_FLCSF5)
      this.oPgFrm.Page5.oPag.oFLCSF5_5_39.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLFNT5_5_41.RadioValue()==this.w_FLFNT5)
      this.oPgFrm.Page5.oPag.oFLFNT5_5_41.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFNTNAME5_5_42.value==this.w_FNTNAME5)
      this.oPgFrm.Page5.oPag.oFNTNAME5_5_42.value=this.w_FNTNAME5
    endif
    if not(this.oPgFrm.Page5.oPag.oFNTSIZE5_5_43.value==this.w_FNTSIZE5)
      this.oPgFrm.Page5.oPag.oFNTSIZE5_5_43.value=this.w_FNTSIZE5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLGLF5_5_47.RadioValue()==this.w_FLGLF5)
      this.oPgFrm.Page5.oPag.oFLGLF5_5_47.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oGLBFONT5_5_48.RadioValue()==this.w_GLBFONT5)
      this.oPgFrm.Page5.oPag.oGLBFONT5_5_48.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLTOP5_5_52.RadioValue()==this.w_FLTOP5)
      this.oPgFrm.Page5.oPag.oFLTOP5_5_52.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oTOP5_5_53.value==this.w_TOP5)
      this.oPgFrm.Page5.oPag.oTOP5_5_53.value=this.w_TOP5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLLEF5_5_54.RadioValue()==this.w_FLLEF5)
      this.oPgFrm.Page5.oPag.oFLLEF5_5_54.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oLEFT5_5_55.value==this.w_LEFT5)
      this.oPgFrm.Page5.oPag.oLEFT5_5_55.value=this.w_LEFT5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLHGT5_5_56.RadioValue()==this.w_FLHGT5)
      this.oPgFrm.Page5.oPag.oFLHGT5_5_56.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oHEIGHT5_5_57.value==this.w_HEIGHT5)
      this.oPgFrm.Page5.oPag.oHEIGHT5_5_57.value=this.w_HEIGHT5
    endif
    if not(this.oPgFrm.Page5.oPag.oFLWDT5_5_58.RadioValue()==this.w_FLWDT5)
      this.oPgFrm.Page5.oPag.oFLWDT5_5_58.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oWIDTH5_5_59.value==this.w_WIDTH5)
      this.oPgFrm.Page5.oPag.oWIDTH5_5_59.value=this.w_WIDTH5
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLOR6_6_3.value==this.w_COLOR6)
      this.oPgFrm.Page6.oPag.oCOLOR6_6_3.value=this.w_COLOR6
    endif
    if not(this.oPgFrm.Page6.oPag.oFLBRW6_6_7.RadioValue()==this.w_FLBRW6)
      this.oPgFrm.Page6.oPag.oFLBRW6_6_7.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oBRDWID6_6_8.value==this.w_BRDWID6)
      this.oPgFrm.Page6.oPag.oBRDWID6_6_8.value=this.w_BRDWID6
    endif
    if not(this.oPgFrm.Page6.oPag.oFLINI6_6_9.RadioValue()==this.w_FLINI6)
      this.oPgFrm.Page6.oPag.oFLINI6_6_9.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oINILINE6_6_10.RadioValue()==this.w_INILINE6)
      this.oPgFrm.Page6.oPag.oINILINE6_6_10.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oFLFIN6_6_12.RadioValue()==this.w_FLFIN6)
      this.oPgFrm.Page6.oPag.oFLFIN6_6_12.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oFINLINE6_6_13.RadioValue()==this.w_FINLINE6)
      this.oPgFrm.Page6.oPag.oFINLINE6_6_13.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oFLSTY6_6_16.RadioValue()==this.w_FLSTY6)
      this.oPgFrm.Page6.oPag.oFLSTY6_6_16.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oSTYLINE6_6_17.RadioValue()==this.w_STYLINE6)
      this.oPgFrm.Page6.oPag.oSTYLINE6_6_17.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oFLCUR6_6_18.RadioValue()==this.w_FLCUR6)
      this.oPgFrm.Page6.oPag.oFLCUR6_6_18.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCURVE6_6_19.value==this.w_CURVE6)
      this.oPgFrm.Page6.oPag.oCURVE6_6_19.value=this.w_CURVE6
    endif
    if not(this.oPgFrm.Page7.oPag.oFLEXE7_7_1.RadioValue()==this.w_FLEXE7)
      this.oPgFrm.Page7.oPag.oFLEXE7_7_1.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oEXEC7_7_3.value==this.w_EXEC7)
      this.oPgFrm.Page7.oPag.oEXEC7_7_3.value=this.w_EXEC7
    endif
    if not(this.oPgFrm.Page7.oPag.oFLTYP7_7_4.RadioValue()==this.w_FLTYP7)
      this.oPgFrm.Page7.oPag.oFLTYP7_7_4.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oTYPE_BMP_7_6.RadioValue()==this.w_TYPE_BMP)
      this.oPgFrm.Page7.oPag.oTYPE_BMP_7_6.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oFLTTL7_7_7.RadioValue()==this.w_FLTTL7)
      this.oPgFrm.Page7.oPag.oFLTTL7_7_7.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oTITLE7_7_9.value==this.w_TITLE7)
      this.oPgFrm.Page7.oPag.oTITLE7_7_9.value=this.w_TITLE7
    endif
    if not(this.oPgFrm.Page7.oPag.oFLPIC7_7_10.RadioValue()==this.w_FLPIC7)
      this.oPgFrm.Page7.oPag.oFLPIC7_7_10.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oPICTURE7_7_12.value==this.w_PICTURE7)
      this.oPgFrm.Page7.oPag.oPICTURE7_7_12.value=this.w_PICTURE7
    endif
    if not(this.oPgFrm.Page7.oPag.oFLTOP7_7_16.RadioValue()==this.w_FLTOP7)
      this.oPgFrm.Page7.oPag.oFLTOP7_7_16.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oTOP7_7_17.value==this.w_TOP7)
      this.oPgFrm.Page7.oPag.oTOP7_7_17.value=this.w_TOP7
    endif
    if not(this.oPgFrm.Page7.oPag.oFLLEF7_7_18.RadioValue()==this.w_FLLEF7)
      this.oPgFrm.Page7.oPag.oFLLEF7_7_18.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oLEFT7_7_19.value==this.w_LEFT7)
      this.oPgFrm.Page7.oPag.oLEFT7_7_19.value=this.w_LEFT7
    endif
    if not(this.oPgFrm.Page7.oPag.oFLHGT7_7_20.RadioValue()==this.w_FLHGT7)
      this.oPgFrm.Page7.oPag.oFLHGT7_7_20.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oHEIGHT7_7_21.value==this.w_HEIGHT7)
      this.oPgFrm.Page7.oPag.oHEIGHT7_7_21.value=this.w_HEIGHT7
    endif
    if not(this.oPgFrm.Page7.oPag.oFLWDT7_7_22.RadioValue()==this.w_FLWDT7)
      this.oPgFrm.Page7.oPag.oFLWDT7_7_22.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oWIDTH7_7_23.value==this.w_WIDTH7)
      this.oPgFrm.Page7.oPag.oWIDTH7_7_23.value=this.w_WIDTH7
    endif
    if not(this.oPgFrm.Page5.oPag.oSELECT5_5_60.RadioValue()==this.w_SELECT5)
      this.oPgFrm.Page5.oPag.oSELECT5_5_60.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oBRDCLR5_5_76.value==this.w_BRDCLR5)
      this.oPgFrm.Page5.oPag.oBRDCLR5_5_76.value=this.w_BRDCLR5
    endif
    if not(this.oPgFrm.Page5.oPag.oBCKCLR5_5_77.value==this.w_BCKCLR5)
      this.oPgFrm.Page5.oPag.oBCKCLR5_5_77.value=this.w_BCKCLR5
    endif
    if not(this.oPgFrm.Page6.oPag.oSELECT6_6_20.RadioValue()==this.w_SELECT6)
      this.oPgFrm.Page6.oPag.oSELECT6_6_20.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oSELECT7_7_28.RadioValue()==this.w_SELECT7)
      this.oPgFrm.Page7.oPag.oSELECT7_7_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFNTCOLOR1_1_37.value==this.w_FNTCOLOR1)
      this.oPgFrm.Page1.oPag.oFNTCOLOR1_1_37.value=this.w_FNTCOLOR1
    endif
    if not(this.oPgFrm.Page2.oPag.oFNTCOLOR2_2_68.value==this.w_FNTCOLOR2)
      this.oPgFrm.Page2.oPag.oFNTCOLOR2_2_68.value=this.w_FNTCOLOR2
    endif
    if not(this.oPgFrm.Page3.oPag.oFNTCOLOR3_3_74.value==this.w_FNTCOLOR3)
      this.oPgFrm.Page3.oPag.oFNTCOLOR3_3_74.value=this.w_FNTCOLOR3
    endif
    if not(this.oPgFrm.Page4.oPag.oFNTCOLOR4_4_51.value==this.w_FNTCOLOR4)
      this.oPgFrm.Page4.oPag.oFNTCOLOR4_4_51.value=this.w_FNTCOLOR4
    endif
    if not(this.oPgFrm.Page5.oPag.oFNTCOLOR5_5_80.value==this.w_FNTCOLOR5)
      this.oPgFrm.Page5.oPag.oFNTCOLOR5_5_80.value=this.w_FNTCOLOR5
    endif
    if not(this.oPgFrm.Page7.oPag.oFLFOR7_7_40.RadioValue()==this.w_FLFOR7)
      this.oPgFrm.Page7.oPag.oFLFOR7_7_40.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oFORMAT7_7_41.RadioValue()==this.w_FORMAT7)
      this.oPgFrm.Page7.oPag.oFORMAT7_7_41.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oINIANGLE6_6_34.value==this.w_INIANGLE6)
      this.oPgFrm.Page6.oPag.oINIANGLE6_6_34.value=this.w_INIANGLE6
    endif
    if not(this.oPgFrm.Page6.oPag.oINISIZE6_6_35.value==this.w_INISIZE6)
      this.oPgFrm.Page6.oPag.oINISIZE6_6_35.value=this.w_INISIZE6
    endif
    if not(this.oPgFrm.Page6.oPag.oFINANGLE6_6_36.value==this.w_FINANGLE6)
      this.oPgFrm.Page6.oPag.oFINANGLE6_6_36.value=this.w_FINANGLE6
    endif
    if not(this.oPgFrm.Page6.oPag.oFINSIZE6_6_37.value==this.w_FINSIZE6)
      this.oPgFrm.Page6.oPag.oFINSIZE6_6_37.value=this.w_FINSIZE6
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_UNSEL5>=0)  and (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oUNSEL5_5_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile inserire valori negativi")
          case   not(.w_PASMOU5>=0)  and (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPASMOU5_5_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile inserire valori negativi")
          case   not(.w_PRESSMOU5>=0)  and (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPRESSMOU5_5_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile inserire valori negativi")
          case   not(.w_BRDWID5>=0)  and (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oBRDWID5_5_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile inserire valori negativi")
          case   not(.w_RADIUS5>=0)  and (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oRADIUS5_5_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile inserire valori negativi")
          case   not(.w_BRDWID6>=0)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oBRDWID6_6_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile inserire valori negativi")
          case   not(.w_CURVE6>=0)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oCURVE6_6_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile inserire valori negativi")
          case   not(.w_INIANGLE6>0 AND .w_INIANGLE6<=180)  and (.w_INILINE6<>0)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oINIANGLE6_6_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_INISIZE6>0 AND .w_INISIZE6<=180)  and (.w_INILINE6<>0)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oINISIZE6_6_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FINANGLE6>0 AND .w_FINANGLE6<=180)  and (.w_FINLINE6<>0)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oFINANGLE6_6_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FINSIZE6>0 AND .w_FINSIZE6<=180)  and (.w_FINLINE6<>0)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oFINSIZE6_6_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SELECT1 = this.w_SELECT1
    this.o_ARCHIVE2 = this.w_ARCHIVE2
    this.o_SELECT2 = this.w_SELECT2
    this.o_SELECT3 = this.w_SELECT3
    this.o_SELECT4 = this.w_SELECT4
    this.o_SELECT5 = this.w_SELECT5
    this.o_SELECT6 = this.w_SELECT6
    this.o_SELECT7 = this.w_SELECT7
    return

enddefine

* --- Define pages as container
define class tcp_objmaskoptionsPag1 as StdContainer
  Width  = 713
  height = 512
  stdWidth  = 713
  stdheight = 512
  resizeXpos=513
  resizeYpos=440
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLTTL1_1_1 as StdCheck with uid="QJSZDQHEFF",rtseq=1,rtrep=.f.,left=10, top=47, caption=" ",;
    HelpContextID = 195942633,;
    cFormVar="w_FLTTL1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLTTL1_1_1.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTTL1_1_1.GetRadio()
    this.Parent.oContained.w_FLTTL1 = this.RadioValue()
    return .t.
  endfunc

  func oFLTTL1_1_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTTL1==.T.,1,;
      0)
  endfunc

  func oFLTTL1_1_1.mHide()
    with this.Parent.oContained
      return (.w_HIDE1=1)
    endwith
  endfunc

  add object oTITLE1_1_2 as StdField with uid="ATLUYJXNLG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TITLE1", cQueryName = "TITLE1",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Titolo della stringa",;
    HelpContextID = 70104809,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=119, Top=43, InputMask=replicate('X',254)

  add object oFLALG1_1_3 as StdCheck with uid="LYAEKOMWNM",rtseq=3,rtrep=.f.,left=10, top=79, caption=" ",;
    HelpContextID = 102422761,;
    cFormVar="w_FLALG1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLALG1_1_3.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLALG1_1_3.GetRadio()
    this.Parent.oContained.w_FLALG1 = this.RadioValue()
    return .t.
  endfunc

  func oFLALG1_1_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLALG1==.T.,1,;
      0)
  endfunc

  func oFLALG1_1_3.mHide()
    with this.Parent.oContained
      return (.w_HIDE1=1)
    endwith
  endfunc


  add object oALIGN1_1_4 as StdCombo with uid="ESVBUIUPWM",rtseq=4,rtrep=.f.,left=119,top=75,width=111,height=22;
    , ToolTipText = "Tipo di allineamento";
    , HelpContextID = 215143401;
    , cFormVar="w_ALIGN1",RowSource=""+""+MSG_RIGHT+","+""+MSG_CENTRED+","+""+MSG_LEFT+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oALIGN1_1_4.RadioValue()
    return(iif(this.value =1,"R",;
    iif(this.value =2,"C",;
    iif(this.value =3,"L",;
    space(1)))))
  endfunc
  func oALIGN1_1_4.GetRadio()
    this.Parent.oContained.w_ALIGN1 = this.RadioValue()
    return .t.
  endfunc

  func oALIGN1_1_4.SetRadio()
    this.Parent.oContained.w_ALIGN1=trim(this.Parent.oContained.w_ALIGN1)
    this.value = ;
      iif(this.Parent.oContained.w_ALIGN1=="R",1,;
      iif(this.Parent.oContained.w_ALIGN1=="C",2,;
      iif(this.Parent.oContained.w_ALIGN1=="L",3,;
      0)))
  endfunc

  add object oFLFNT1_1_5 as StdCheck with uid="YISMNQBYKX",rtseq=5,rtrep=.f.,left=10, top=120, caption=" ",;
    HelpContextID = 54515945,;
    cFormVar="w_FLFNT1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLFNT1_1_5.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFNT1_1_5.GetRadio()
    this.Parent.oContained.w_FLFNT1 = this.RadioValue()
    return .t.
  endfunc

  func oFLFNT1_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFNT1==.T.,1,;
      0)
  endfunc

  func oFLFNT1_1_5.mHide()
    with this.Parent.oContained
      return (.w_HIDE1=1)
    endwith
  endfunc

  add object oFNTNAME1_1_6 as StdField with uid="KVYFALNNJY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FNTNAME1", cQueryName = "FNTNAME1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(180), bMultilanguage =  .f.,;
    HelpContextID = 263324610,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=119, Top=116, InputMask=replicate('X',180)

  add object oFNTSIZE1_1_7 as StdField with uid="DURLAKAFOD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FNTSIZE1", cQueryName = "FNTSIZE1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 123864002,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=305, Top=116, cSayPict='"99"', cGetPict='"99"'


  add object oBtn_1_8 as StdButton with uid="BIRBEWMVIT",left=205, top=146, width=95,height=22,;
    caption="Change font", nPag=1;
    , ToolTipText = "Selezione font";
    , HelpContextID = 120067223;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Font",1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT .w_GLBFONT1)
      endwith
    endif
  endfunc

  add object oFLGLF1_1_9 as StdCheck with uid="JTBTPNTOSO",rtseq=8,rtrep=.f.,left=10, top=150, caption=" ",;
    HelpContextID = 86038761,;
    cFormVar="w_FLGLF1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGLF1_1_9.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLGLF1_1_9.GetRadio()
    this.Parent.oContained.w_FLGLF1 = this.RadioValue()
    return .t.
  endfunc

  func oFLGLF1_1_9.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLGLF1==.T.,1,;
      0)
  endfunc

  func oFLGLF1_1_9.mHide()
    with this.Parent.oContained
      return (.w_HIDE1=1)
    endwith
  endfunc


  add object oGLBFONT1_1_10 as StdCombo with uid="TFUGDPRAOL",rtseq=9,rtrep=.f.,left=119,top=146,width=55,height=22;
    , ToolTipText = "Se attivo utilizza il font globale";
    , HelpContextID = 230415693;
    , cFormVar="w_GLBFONT1",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGLBFONT1_1_10.RadioValue()
    return(iif(this.value =1,.T.,;
    iif(this.value =2,.F.,;
    .f.)))
  endfunc
  func oGLBFONT1_1_10.GetRadio()
    this.Parent.oContained.w_GLBFONT1 = this.RadioValue()
    return .t.
  endfunc

  func oGLBFONT1_1_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GLBFONT1==.T.,1,;
      iif(this.Parent.oContained.w_GLBFONT1==.F.,2,;
      0))
  endfunc

  add object oFLTOP1_1_11 as StdCheck with uid="PNIBMTRWDS",rtseq=10,rtrep=.f.,left=10, top=208, caption=" ",;
    HelpContextID = 257808617,;
    cFormVar="w_FLTOP1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLTOP1_1_11.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTOP1_1_11.GetRadio()
    this.Parent.oContained.w_FLTOP1 = this.RadioValue()
    return .t.
  endfunc

  func oFLTOP1_1_11.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTOP1==.T.,1,;
      0)
  endfunc

  func oFLTOP1_1_11.mHide()
    with this.Parent.oContained
      return (.w_HIDE1=1)
    endwith
  endfunc

  add object oTOP1_1_12 as StdField with uid="FOHBXVGHZT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_TOP1", cQueryName = "TOP1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione della stringa nella maschera (in alto)",;
    HelpContextID = 42330391,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=99, Top=205, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLLEF1_1_13 as StdCheck with uid="GJNPQTEZRE",rtseq=12,rtrep=.f.,left=10, top=239, caption=" ",;
    HelpContextID = 79026409,;
    cFormVar="w_FLLEF1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLLEF1_1_13.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLEF1_1_13.GetRadio()
    this.Parent.oContained.w_FLLEF1 = this.RadioValue()
    return .t.
  endfunc

  func oFLLEF1_1_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLEF1==.T.,1,;
      0)
  endfunc

  func oFLLEF1_1_13.mHide()
    with this.Parent.oContained
      return (.w_HIDE1=1)
    endwith
  endfunc

  add object oLEFT1_1_14 as StdField with uid="SLPHJFECXA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_LEFT1", cQueryName = "LEFT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione della stringa nella maschera (a sinistra)",;
    HelpContextID = 10448617,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=99, Top=236, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLHGT1_1_15 as StdCheck with uid="EHGWBXLKCN",rtseq=14,rtrep=.f.,left=223, top=208, caption=" ",;
    HelpContextID = 47306985,;
    cFormVar="w_FLHGT1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLHGT1_1_15.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLHGT1_1_15.GetRadio()
    this.Parent.oContained.w_FLHGT1 = this.RadioValue()
    return .t.
  endfunc

  func oFLHGT1_1_15.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLHGT1==.T.,1,;
      0)
  endfunc

  func oFLHGT1_1_15.mHide()
    with this.Parent.oContained
      return (.w_HIDE1=1)
    endwith
  endfunc

  add object oHEIGHT1_1_16 as StdField with uid="QRCGGXUJWK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_HEIGHT1", cQueryName = "HEIGHT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica l'altezza della stringa",;
    HelpContextID = 114453274,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=311, Top=204, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLWDT1_1_17 as StdCheck with uid="CYVPGCVOZG",rtseq=16,rtrep=.f.,left=223, top=239, caption=" ",;
    HelpContextID = 45144297,;
    cFormVar="w_FLWDT1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLWDT1_1_17.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLWDT1_1_17.GetRadio()
    this.Parent.oContained.w_FLWDT1 = this.RadioValue()
    return .t.
  endfunc

  func oFLWDT1_1_17.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLWDT1==.T.,1,;
      0)
  endfunc

  func oFLWDT1_1_17.mHide()
    with this.Parent.oContained
      return (.w_HIDE1=1)
    endwith
  endfunc

  add object oWIDTH1_1_18 as StdField with uid="GHBFCBMWQI",rtseq=17,rtrep=.f.,;
    cFormVar = "w_WIDTH1", cQueryName = "WIDTH1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la larghezza della stringa",;
    HelpContextID = 127777257,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=311, Top=235, cSayPict='"9999"', cGetPict='"9999"'

  add object oSELECT1_1_19 as StdRadio with uid="JTNCTTGSEQ",rtseq=18,rtrep=.f.,left=357, top=8, width=189,height=36;
    , cFormVar="w_SELECT1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELECT1_1_19.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption=""+MSG_SELECT_ALL_TEXT+""
      this.Buttons(1).HelpContextID = 28669466
      this.Buttons(1).Top=0
      this.Buttons(2).Caption=""+MSG_DESELECT_ALL+""
      this.Buttons(2).HelpContextID = 28669466
      this.Buttons(2).Top=17
      this.SetAll("Width",187)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECT1_1_19.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    "N")))
  endfunc
  func oSELECT1_1_19.GetRadio()
    this.Parent.oContained.w_SELECT1 = this.RadioValue()
    return .t.
  endfunc

  func oSELECT1_1_19.SetRadio()
    this.Parent.oContained.w_SELECT1=trim(this.Parent.oContained.w_SELECT1)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT1=="S",1,;
      iif(this.Parent.oContained.w_SELECT1=="N",2,;
      0))
  endfunc

  func oSELECT1_1_19.mHide()
    with this.Parent.oContained
      return (.w_HIDE1=1)
    endwith
  endfunc


  add object oBtn_1_20 as StdButton with uid="DINXPCKVLA",left=553, top=462, width=48,height=45,;
    CpPicture="BMP\applica.ico", caption="", nPag=1;
    , ToolTipText = "Premere per applicare le modifiche";
    , HelpContextID = 172758090;
    , caption="\<Applica";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Save",1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="KFKRTKGBHY",left=608, top=462, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 98971159;
    , caption="\<Ok";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="KRWSHEDFOQ",left=663, top=462, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare le operazioni svolte";
    , HelpContextID = 250656791;
    , caption="\<Esci";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object zLbl as cp_szoombox with uid="DGZLZDOYLY",left=503, top=7, width=208,height=447,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="objmaskoptions",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomFile="default",bOptions=.f.,;
    cEvent = "Pagina1",;
    nPag=1;
    , HelpContextID = 204127767

  add object oFNTCOLOR1_1_37 as StdField with uid="RGAGFSBBZF",rtseq=227,rtrep=.f.,;
    cFormVar = "w_FNTCOLOR1", cQueryName = "FNTCOLOR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 39964840,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=305, Top=146


  add object lbl as cp_calclbl with uid="XLXAHFEEVQ",left=10, top=8, width=262,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=0,fontname="Arial",fontsize=9,fontBold=.t.,fontItalic=.t.,fontUnderline=.f.,bGlobalfont =.f.,;
    nPag=1;
    , HelpContextID = 204127767


  add object oBtn_1_56 as StdButton with uid="TXQUMLLIMN",left=337, top=146, width=23,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Modifica colore font";
    , HelpContextID = 96083735;
  , bGlobalFont=.t.

    proc oBtn_1_56.Click()
      this.parent.oContained.NotifyEvent("FontColor1")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_23 as StdString with uid="FMOMKQKXSV",Visible=.t., Left=39, Top=47,;
    Alignment=0, Width=33, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="GKKKSTNZOM",Visible=.t., Left=39, Top=79,;
    Alignment=0, Width=75, Height=18,;
    Caption="Allineamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="NNXESEKAYH",Visible=.t., Left=39, Top=120,;
    Alignment=0, Width=78, Height=18,;
    Caption="Font Options:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="PFSJFHAHAT",Visible=.t., Left=39, Top=150,;
    Alignment=0, Width=62, Height=18,;
    Caption="Global font:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="IWJFKGJJNW",Visible=.t., Left=46, Top=175,;
    Alignment=0, Width=155, Height=18,;
    Caption="Posizione e Dimensione"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="TLPBLSXKKZ",Visible=.t., Left=39, Top=209,;
    Alignment=0, Width=59, Height=18,;
    Caption="Top:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="GARVUKUQHZ",Visible=.t., Left=39, Top=240,;
    Alignment=0, Width=64, Height=18,;
    Caption="Left:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="FPKGTGJVZX",Visible=.t., Left=246, Top=208,;
    Alignment=0, Width=59, Height=18,;
    Caption="Height:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="TJPVQPSCUP",Visible=.t., Left=246, Top=239,;
    Alignment=0, Width=68, Height=18,;
    Caption="Width:"  ;
  , bGlobalFont=.t.

  add object oBox_1_28 as StdBox with uid="MZAAMONEJX",left=32, top=197, width=381,height=1

  add object oBox_1_52 as StdBox with uid="AUKKVGUEYL",left=10, top=33, width=286,height=2
enddefine
define class tcp_objmaskoptionsPag2 as StdContainer
  Width  = 713
  height = 512
  stdWidth  = 713
  stdheight = 512
  resizeXpos=520
  resizeYpos=419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLVAR2_2_1 as StdCheck with uid="MWDUZVVCFG",rtseq=19,rtrep=.f.,left=10, top=47, caption=" ",;
    HelpContextID = 8378601,;
    cFormVar="w_FLVAR2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLVAR2_2_1.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVAR2_2_1.GetRadio()
    this.Parent.oContained.w_FLVAR2 = this.RadioValue()
    return .t.
  endfunc

  func oFLVAR2_2_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVAR2==.T.,1,;
      0)
  endfunc

  func oFLVAR2_2_1.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oVAR2_2_2 as StdField with uid="CNSHOJVNZZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_VAR2", cQueryName = "VAR2",;
    bObbl = .f. , nPag = 2, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome della variabile",;
    HelpContextID = 41207575,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=119, Top=43, InputMask=replicate('X',25)

  add object oFLTYP2_2_3 as StdCheck with uid="UDUYZSIDGU",rtseq=21,rtrep=.f.,left=10, top=74, caption=" ",;
    HelpContextID = 268294377,;
    cFormVar="w_FLTYP2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLTYP2_2_3.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTYP2_2_3.GetRadio()
    this.Parent.oContained.w_FLTYP2 = this.RadioValue()
    return .t.
  endfunc

  func oFLTYP2_2_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTYP2==.T.,1,;
      0)
  endfunc

  func oFLTYP2_2_3.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc


  add object oTYPE_V_2_4 as StdCombo with uid="NDUJIYZNLH",rtseq=22,rtrep=.f.,left=119,top=70,width=111,height=22;
    , ToolTipText = "Tipo della variabile";
    , HelpContextID = 38095127;
    , cFormVar="w_TYPE_V",RowSource=""+""+MSG_NUMERIC+","+""+MSG_CHARACTER+","+""+MSG_DATE+"", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTYPE_V_2_4.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"C",;
    iif(this.value =3,"D",;
    space(1)))))
  endfunc
  func oTYPE_V_2_4.GetRadio()
    this.Parent.oContained.w_TYPE_V = this.RadioValue()
    return .t.
  endfunc

  func oTYPE_V_2_4.SetRadio()
    this.Parent.oContained.w_TYPE_V=trim(this.Parent.oContained.w_TYPE_V)
    this.value = ;
      iif(this.Parent.oContained.w_TYPE_V=="N",1,;
      iif(this.Parent.oContained.w_TYPE_V=="C",2,;
      iif(this.Parent.oContained.w_TYPE_V=="D",3,;
      0)))
  endfunc

  add object oFLLEN2_2_5 as StdCheck with uid="KTKERCBNKH",rtseq=23,rtrep=.f.,left=254, top=74, caption=" ",;
    HelpContextID = 213244137,;
    cFormVar="w_FLLEN2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLLEN2_2_5.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLEN2_2_5.GetRadio()
    this.Parent.oContained.w_FLLEN2 = this.RadioValue()
    return .t.
  endfunc

  func oFLLEN2_2_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLEN2==.T.,1,;
      0)
  endfunc

  func oFLLEN2_2_5.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oLEN2_2_6 as StdField with uid="YLHMFLHTKA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_LEN2", cQueryName = "LEN2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza",;
    HelpContextID = 41455895,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=316, Top=70

  add object oDEC2_2_7 as StdField with uid="GLLIZYEMIC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DEC2", cQueryName = "DEC2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di decimali",;
    HelpContextID = 42178839,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=383, Top=70

  func oDEC2_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TYPE_V='N')
    endwith
   endif
  endfunc

  add object oFLEDI2_2_8 as StdCheck with uid="UYLNGKBIXK",rtseq=26,rtrep=.f.,left=10, top=101, caption=" ",;
    HelpContextID = 127850729,;
    cFormVar="w_FLEDI2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLEDI2_2_8.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLEDI2_2_8.GetRadio()
    this.Parent.oContained.w_FLEDI2 = this.RadioValue()
    return .t.
  endfunc

  func oFLEDI2_2_8.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLEDI2==.T.,1,;
      0)
  endfunc

  func oFLEDI2_2_8.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc


  add object oEDIT2_2_9 as StdCombo with uid="ZGSRVXKOQN",rtseq=27,rtrep=.f.,left=119,top=97,width=55,height=22;
    , ToolTipText = "Se attivo la variabile � editabile";
    , HelpContextID = 27416553;
    , cFormVar="w_EDIT2",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oEDIT2_2_9.RadioValue()
    return(iif(this.value =1,"Edit",;
    iif(this.value =2,"Show",;
    space(4))))
  endfunc
  func oEDIT2_2_9.GetRadio()
    this.Parent.oContained.w_EDIT2 = this.RadioValue()
    return .t.
  endfunc

  func oEDIT2_2_9.SetRadio()
    this.Parent.oContained.w_EDIT2=trim(this.Parent.oContained.w_EDIT2)
    this.value = ;
      iif(this.Parent.oContained.w_EDIT2=="Edit",1,;
      iif(this.Parent.oContained.w_EDIT2=="Show",2,;
      0))
  endfunc

  add object oFLZER2_2_10 as StdCheck with uid="ARWGLNCJAT",rtseq=28,rtrep=.f.,left=202, top=101, caption=" ",;
    HelpContextID = 12835049,;
    cFormVar="w_FLZER2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLZER2_2_10.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLZER2_2_10.GetRadio()
    this.Parent.oContained.w_FLZER2 = this.RadioValue()
    return .t.
  endfunc

  func oFLZER2_2_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLZER2==.T.,1,;
      0)
  endfunc

  func oFLZER2_2_10.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc


  add object oZERO2_2_11 as StdCombo with uid="OUVPKFSTEG",rtseq=29,rtrep=.f.,left=316,top=97,width=55,height=22;
    , ToolTipText = "Se attivo la variabile viene riempita di zeri";
    , HelpContextID = 22772969;
    , cFormVar="w_ZERO2",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oZERO2_2_11.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oZERO2_2_11.GetRadio()
    this.Parent.oContained.w_ZERO2 = this.RadioValue()
    return .t.
  endfunc

  func oZERO2_2_11.SetRadio()
    this.Parent.oContained.w_ZERO2=trim(this.Parent.oContained.w_ZERO2)
    this.value = ;
      iif(this.Parent.oContained.w_ZERO2=="S",1,;
      iif(this.Parent.oContained.w_ZERO2=="N",2,;
      0))
  endfunc

  func oZERO2_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TYPE_V='C')
    endwith
   endif
  endfunc

  add object oFLFOR2_2_12 as StdCheck with uid="QXUABCIYCU",rtseq=30,rtrep=.f.,left=10, top=128, caption=" ",;
    HelpContextID = 22010089,;
    cFormVar="w_FLFOR2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLFOR2_2_12.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFOR2_2_12.GetRadio()
    this.Parent.oContained.w_FLFOR2 = this.RadioValue()
    return .t.
  endfunc

  func oFLFOR2_2_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFOR2==.T.,1,;
      0)
  endfunc

  func oFLFOR2_2_12.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oPICTURE2_2_13 as StdField with uid="ZQGRUQSXWI",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PICTURE2", cQueryName = "PICTURE2",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Formato della variabile",;
    HelpContextID = 77379150,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=119, Top=124, InputMask=replicate('X',30)

  add object oFLARC2_2_14 as StdCheck with uid="AOTTCWFLPO",rtseq=32,rtrep=.f.,left=10, top=155, caption=" ",;
    HelpContextID = 41605353,;
    cFormVar="w_FLARC2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLARC2_2_14.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLARC2_2_14.GetRadio()
    this.Parent.oContained.w_FLARC2 = this.RadioValue()
    return .t.
  endfunc

  func oFLARC2_2_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLARC2==.T.,1,;
      0)
  endfunc

  func oFLARC2_2_14.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oARCHIVE2_2_15 as StdField with uid="JAHSSSMMDU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ARCHIVE2", cQueryName = "ARCHIVE2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Selezione archivio link",;
    HelpContextID = 136497330,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=119, Top=151, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="cpttbls", oKey_1_1="FileName", oKey_1_2="this.w_ARCHIVE2"

  func oARCHIVE2_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCHIVE2_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCHIVE2_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpttbls','*','FileName',cp_AbsName(this.parent,'oARCHIVE2_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabelle",'cp_objmaskoptions.cpttbls_VZM',this.parent.oContained
  endproc

  add object oFLFVL1_2_16 as StdCheck with uid="MMXMDRLJUJ",rtseq=34,rtrep=.f.,left=10, top=199, caption=" ",;
    HelpContextID = 197122281,;
    cFormVar="w_FLFVL1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLFVL1_2_16.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFVL1_2_16.GetRadio()
    this.Parent.oContained.w_FLFVL1 = this.RadioValue()
    return .t.
  endfunc

  func oFLFVL1_2_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFVL1==.T.,1,;
      0)
  endfunc

  func oFLFVL1_2_16.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oFIELD1_2_17 as StdField with uid="BAIJBGUHZQ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_FIELD1", cQueryName = "FIELD1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Primo campo chiave",;
    HelpContextID = 52340969,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=44, Top=199, InputMask=replicate('X',10)

  add object oVALUE1_V_2_18 as StdField with uid="TZAGGDFZVG",rtseq=36,rtrep=.f.,;
    cFormVar = "w_VALUE1_V", cQueryName = "VALUE1_V",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Valore del primo campo chiave",;
    HelpContextID = 78986920,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=139, Top=199, InputMask=replicate('X',40)

  add object oFLFVL2_2_19 as StdCheck with uid="UKYLOGSATV",rtseq=37,rtrep=.f.,left=10, top=225, caption=" ",;
    HelpContextID = 197122281,;
    cFormVar="w_FLFVL2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLFVL2_2_19.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFVL2_2_19.GetRadio()
    this.Parent.oContained.w_FLFVL2 = this.RadioValue()
    return .t.
  endfunc

  func oFLFVL2_2_19.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFVL2==.T.,1,;
      0)
  endfunc

  func oFLFVL2_2_19.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oFIELD2_2_20 as StdField with uid="KJTKBKFNPW",rtseq=38,rtrep=.f.,;
    cFormVar = "w_FIELD2", cQueryName = "FIELD2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Secondo campo chiave",;
    HelpContextID = 52340969,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=44, Top=225, InputMask=replicate('X',10)

  add object oVALUE2_V_2_21 as StdField with uid="DONBUZMBCM",rtseq=39,rtrep=.f.,;
    cFormVar = "w_VALUE2_V", cQueryName = "VALUE2_V",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Valore del secondo campo chiave",;
    HelpContextID = 78986920,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=139, Top=225, InputMask=replicate('X',40)

  add object oFLFVL3_2_22 as StdCheck with uid="CTDGIYFEFC",rtseq=40,rtrep=.f.,left=10, top=251, caption=" ",;
    HelpContextID = 197122281,;
    cFormVar="w_FLFVL3", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLFVL3_2_22.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFVL3_2_22.GetRadio()
    this.Parent.oContained.w_FLFVL3 = this.RadioValue()
    return .t.
  endfunc

  func oFLFVL3_2_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFVL3==.T.,1,;
      0)
  endfunc

  func oFLFVL3_2_22.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oFIELD3_2_23 as StdField with uid="JIQKIRTSBM",rtseq=41,rtrep=.f.,;
    cFormVar = "w_FIELD3", cQueryName = "FIELD3",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Terzo campo chiave",;
    HelpContextID = 52340969,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=44, Top=251, InputMask=replicate('X',10)

  add object oVALUE3_V_2_24 as StdField with uid="BAIPBUCJDA",rtseq=42,rtrep=.f.,;
    cFormVar = "w_VALUE3_V", cQueryName = "VALUE3_V",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Valore del terzo campo chiave",;
    HelpContextID = 78986920,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=139, Top=251, InputMask=replicate('X',40)

  add object oFLFVL4_2_25 as StdCheck with uid="SGUBZYRMZN",rtseq=43,rtrep=.f.,left=10, top=277, caption=" ",;
    HelpContextID = 197122281,;
    cFormVar="w_FLFVL4", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLFVL4_2_25.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFVL4_2_25.GetRadio()
    this.Parent.oContained.w_FLFVL4 = this.RadioValue()
    return .t.
  endfunc

  func oFLFVL4_2_25.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFVL4==.T.,1,;
      0)
  endfunc

  func oFLFVL4_2_25.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oFIELD4_2_26 as StdField with uid="IIVGGEFRLS",rtseq=44,rtrep=.f.,;
    cFormVar = "w_FIELD4", cQueryName = "FIELD4",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Campo risultato del link",;
    HelpContextID = 52340969,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=44, Top=277, InputMask=replicate('X',10)

  add object oFLFVL5_2_27 as StdCheck with uid="WQDEQZIHGW",rtseq=45,rtrep=.f.,left=10, top=319, caption=" ",;
    HelpContextID = 197122281,;
    cFormVar="w_FLFVL5", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLFVL5_2_27.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFVL5_2_27.GetRadio()
    this.Parent.oContained.w_FLFVL5 = this.RadioValue()
    return .t.
  endfunc

  func oFLFVL5_2_27.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFVL5==.T.,1,;
      0)
  endfunc

  func oFLFVL5_2_27.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oFIELD5_2_28 as StdField with uid="CAAEDVSSNR",rtseq=46,rtrep=.f.,;
    cFormVar = "w_FIELD5", cQueryName = "FIELD5",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Campo da riportare",;
    HelpContextID = 52340969,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=44, Top=319, InputMask=replicate('X',10)

  add object oVALUE5_V_2_29 as StdField with uid="ISAUQDUKJE",rtseq=47,rtrep=.f.,;
    cFormVar = "w_VALUE5_V", cQueryName = "VALUE5_V",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Valore campo da riportare",;
    HelpContextID = 78986920,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=139, Top=319, InputMask=replicate('X',40)

  add object oFLFNT2_2_30 as StdCheck with uid="UQTWNHAMBE",rtseq=48,rtrep=.f.,left=10, top=370, caption=" ",;
    HelpContextID = 54515945,;
    cFormVar="w_FLFNT2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLFNT2_2_30.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFNT2_2_30.GetRadio()
    this.Parent.oContained.w_FLFNT2 = this.RadioValue()
    return .t.
  endfunc

  func oFLFNT2_2_30.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFNT2==.T.,1,;
      0)
  endfunc

  func oFLFNT2_2_30.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oFNTNAME2_2_31 as StdField with uid="VEHZZLTFEO",rtseq=49,rtrep=.f.,;
    cFormVar = "w_FNTNAME2", cQueryName = "FNTNAME2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(180), bMultilanguage =  .f.,;
    HelpContextID = 263324594,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=119, Top=366, InputMask=replicate('X',180)

  add object oFNTSIZE2_2_32 as StdField with uid="ZYMVMLXDDT",rtseq=50,rtrep=.f.,;
    cFormVar = "w_FNTSIZE2", cQueryName = "FNTSIZE2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 123863986,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=309, Top=366, cSayPict='"99"', cGetPict='"99"'


  add object oBtn_2_33 as StdButton with uid="FOXHXXTUHT",left=205, top=393, width=95,height=22,;
    caption="Change font", nPag=2;
    , ToolTipText = "Selezione font";
    , HelpContextID = 120067223;
  , bGlobalFont=.t.

    proc oBtn_2_33.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Font",2)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT .w_GLBFONT2)
      endwith
    endif
  endfunc

  add object oFLGLF2_2_34 as StdCheck with uid="TAQBBVEQUQ",rtseq=51,rtrep=.f.,left=10, top=397, caption=" ",;
    HelpContextID = 86038761,;
    cFormVar="w_FLGLF2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLGLF2_2_34.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLGLF2_2_34.GetRadio()
    this.Parent.oContained.w_FLGLF2 = this.RadioValue()
    return .t.
  endfunc

  func oFLGLF2_2_34.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLGLF2==.T.,1,;
      0)
  endfunc

  func oFLGLF2_2_34.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc


  add object oGLBFONT2_2_35 as StdCombo with uid="ECVDIIJIQE",rtseq=52,rtrep=.f.,left=119,top=393,width=55,height=22;
    , ToolTipText = "Se attivo utilizza il font globale";
    , HelpContextID = 230415709;
    , cFormVar="w_GLBFONT2",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oGLBFONT2_2_35.RadioValue()
    return(iif(this.value =1,.T.,;
    iif(this.value =2,.F.,;
    .f.)))
  endfunc
  func oGLBFONT2_2_35.GetRadio()
    this.Parent.oContained.w_GLBFONT2 = this.RadioValue()
    return .t.
  endfunc

  func oGLBFONT2_2_35.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GLBFONT2==.T.,1,;
      iif(this.Parent.oContained.w_GLBFONT2==.F.,2,;
      0))
  endfunc

  add object oFLTOP2_2_36 as StdCheck with uid="OZAOEGJJJX",rtseq=53,rtrep=.f.,left=10, top=454, caption=" ",;
    HelpContextID = 257808617,;
    cFormVar="w_FLTOP2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLTOP2_2_36.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTOP2_2_36.GetRadio()
    this.Parent.oContained.w_FLTOP2 = this.RadioValue()
    return .t.
  endfunc

  func oFLTOP2_2_36.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTOP2==.T.,1,;
      0)
  endfunc

  func oFLTOP2_2_36.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oTOP2_2_37 as StdField with uid="VOXAIIDABP",rtseq=54,rtrep=.f.,;
    cFormVar = "w_TOP2", cQueryName = "TOP2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione della variabile nella maschera (in alto)",;
    HelpContextID = 41281815,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=451, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLLEF2_2_38 as StdCheck with uid="JBZVEIVBQL",rtseq=55,rtrep=.f.,left=10, top=485, caption=" ",;
    HelpContextID = 79026409,;
    cFormVar="w_FLLEF2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLLEF2_2_38.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLEF2_2_38.GetRadio()
    this.Parent.oContained.w_FLLEF2 = this.RadioValue()
    return .t.
  endfunc

  func oFLLEF2_2_38.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLEF2==.T.,1,;
      0)
  endfunc

  func oFLLEF2_2_38.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oLEFT2_2_39 as StdField with uid="UCXGHANJUT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_LEFT2", cQueryName = "LEFT2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione della variabile nella maschera (a sinistra)",;
    HelpContextID = 27225833,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=482, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLHGT2_2_40 as StdCheck with uid="LTOKKESXRD",rtseq=57,rtrep=.f.,left=220, top=454, caption=" ",;
    HelpContextID = 47306985,;
    cFormVar="w_FLHGT2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLHGT2_2_40.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLHGT2_2_40.GetRadio()
    this.Parent.oContained.w_FLHGT2 = this.RadioValue()
    return .t.
  endfunc

  func oFLHGT2_2_40.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLHGT2==.T.,1,;
      0)
  endfunc

  func oFLHGT2_2_40.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oHEIGHT2_2_41 as StdField with uid="OJQRUFUBPD",rtseq=58,rtrep=.f.,;
    cFormVar = "w_HEIGHT2", cQueryName = "HEIGHT2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica l'altezza della variabile",;
    HelpContextID = 114453275,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=297, Top=450, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLWDT2_2_42 as StdCheck with uid="AVXWLRHFXO",rtseq=59,rtrep=.f.,left=220, top=485, caption=" ",;
    HelpContextID = 45144297,;
    cFormVar="w_FLWDT2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLWDT2_2_42.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLWDT2_2_42.GetRadio()
    this.Parent.oContained.w_FLWDT2 = this.RadioValue()
    return .t.
  endfunc

  func oFLWDT2_2_42.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLWDT2==.T.,1,;
      0)
  endfunc

  func oFLWDT2_2_42.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc

  add object oWIDTH2_2_43 as StdField with uid="MFVASIJLTA",rtseq=60,rtrep=.f.,;
    cFormVar = "w_WIDTH2", cQueryName = "WIDTH2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la variabile della variabile",;
    HelpContextID = 127777257,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=297, Top=481, cSayPict='"9999"', cGetPict='"9999"'

  add object oSELECT2_2_44 as StdRadio with uid="DDBDXWDVHW",rtseq=61,rtrep=.f.,left=357, top=8, width=193,height=35;
    , cFormVar="w_SELECT2", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELECT2_2_44.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption=""+MSG_SELECT_ALL_TEXT+""
      this.Buttons(1).HelpContextID = 28669467
      this.Buttons(1).Top=0
      this.Buttons(2).Caption=""+MSG_DESELECT_ALL+""
      this.Buttons(2).HelpContextID = 28669467
      this.Buttons(2).Top=16
      this.SetAll("Width",191)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECT2_2_44.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    "N")))
  endfunc
  func oSELECT2_2_44.GetRadio()
    this.Parent.oContained.w_SELECT2 = this.RadioValue()
    return .t.
  endfunc

  func oSELECT2_2_44.SetRadio()
    this.Parent.oContained.w_SELECT2=trim(this.Parent.oContained.w_SELECT2)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT2=="S",1,;
      iif(this.Parent.oContained.w_SELECT2=="N",2,;
      0))
  endfunc

  func oSELECT2_2_44.mHide()
    with this.Parent.oContained
      return (.w_HIDE2=1)
    endwith
  endfunc


  add object oBtn_2_45 as StdButton with uid="WMHVAAGYEL",left=553, top=462, width=48,height=45,;
    CpPicture="BMP\applica.ico", caption="", nPag=2;
    , ToolTipText = "Premere per applicare le modifiche";
    , HelpContextID = 172758090;
    , caption="\<Applica";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_45.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Save",2)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_46 as StdButton with uid="QVVDBULUFP",left=608, top=462, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 98971159;
    , caption="\<Ok";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_46.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_47 as StdButton with uid="JGMBYGSITF",left=663, top=462, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare le operazioni svolte";
    , HelpContextID = 250656791;
    , caption="\<Esci";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_47.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFNTCOLOR2_2_68 as StdField with uid="VWPPHEFVOX",rtseq=232,rtrep=.f.,;
    cFormVar = "w_FNTCOLOR2", cQueryName = "FNTCOLOR2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 39964584,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=309, Top=393


  add object zTxt as cp_szoombox with uid="QZSSPJVVVH",left=503, top=7, width=208,height=447,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="objmaskoptions",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomFile="default",bOptions=.f.,;
    cEvent = "Pagina2",;
    nPag=2;
    , HelpContextID = 204127767


  add object txt as cp_calclbl with uid="KTOYKOMRWV",left=10, top=8, width=262,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 204127767


  add object oBtn_2_81 as StdButton with uid="WXVGMOVTCQ",left=345, top=393, width=23,height=22,;
    caption="...", nPag=2;
    , ToolTipText = "Modifica colore font";
    , HelpContextID = 96083735;
  , bGlobalFont=.t.

    proc oBtn_2_81.Click()
      this.parent.oContained.NotifyEvent("FontColor2")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_48 as StdString with uid="TRDEOFHMQR",Visible=.t., Left=42, Top=47,;
    Alignment=0, Width=51, Height=18,;
    Caption="Variabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="JCQWUVQAHG",Visible=.t., Left=42, Top=74,;
    Alignment=0, Width=29, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="KCIDFTJXNL",Visible=.t., Left=42, Top=370,;
    Alignment=0, Width=73, Height=18,;
    Caption="Font Options:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="FMBSTMOWPV",Visible=.t., Left=42, Top=397,;
    Alignment=0, Width=62, Height=18,;
    Caption="Global font:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="NBZJOILJXP",Visible=.t., Left=47, Top=421,;
    Alignment=0, Width=174, Height=18,;
    Caption="Posizione e Dimensione"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="UNEPEDNHYL",Visible=.t., Left=283, Top=74,;
    Alignment=0, Width=27, Height=18,;
    Caption="Lun.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="ZIOUJPVZXH",Visible=.t., Left=349, Top=74,;
    Alignment=0, Width=28, Height=18,;
    Caption="Dec.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="MTKCAMBMEE",Visible=.t., Left=42, Top=101,;
    Alignment=0, Width=51, Height=18,;
    Caption="Editabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="YHIMLQBAHW",Visible=.t., Left=222, Top=101,;
    Alignment=0, Width=88, Height=18,;
    Caption="Riempi con zeri:"    , caption=MSG_FILL_WITH_ZEROES+MSG_FS;
  ;
  , bGlobalFont=.t.

  add object oStr_2_58 as StdString with uid="YNJWVDHCAE",Visible=.t., Left=42, Top=128,;
    Alignment=0, Width=49, Height=18,;
    Caption="Formato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="PJPMNNFRBZ",Visible=.t., Left=42, Top=155,;
    Alignment=0, Width=49, Height=18,;
    Caption="Archivio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="SCRZYGUQST",Visible=.t., Left=44, Top=180,;
    Alignment=0, Width=68, Height=18,;
    Caption="Chiave fissa"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="OFPPXQXTIK",Visible=.t., Left=139, Top=180,;
    Alignment=0, Width=68, Height=18,;
    Caption="Valori"  ;
  , bGlobalFont=.t.

  add object oStr_2_62 as StdString with uid="IVPFBOFKCC",Visible=.t., Left=44, Top=300,;
    Alignment=0, Width=110, Height=18,;
    Caption="Campo da riportare"  ;
  , bGlobalFont=.t.

  add object oStr_2_63 as StdString with uid="VFDUVQXLOO",Visible=.t., Left=42, Top=455,;
    Alignment=0, Width=70, Height=18,;
    Caption="Top:"  ;
  , bGlobalFont=.t.

  add object oStr_2_64 as StdString with uid="QOICPFJFEY",Visible=.t., Left=42, Top=486,;
    Alignment=0, Width=61, Height=18,;
    Caption="Left:"  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="SHFXKWKYWR",Visible=.t., Left=241, Top=454,;
    Alignment=0, Width=66, Height=18,;
    Caption="Height:"  ;
  , bGlobalFont=.t.

  add object oStr_2_66 as StdString with uid="COMKPFQWHF",Visible=.t., Left=244, Top=485,;
    Alignment=0, Width=54, Height=18,;
    Caption="Width:"  ;
  , bGlobalFont=.t.

  add object oBox_2_53 as StdBox with uid="OVSPCNZAYT",left=33, top=443, width=381,height=1

  add object oBox_2_79 as StdBox with uid="XWTJOBLMSM",left=10, top=33, width=286,height=2
enddefine
define class tcp_objmaskoptionsPag3 as StdContainer
  Width  = 713
  height = 512
  stdWidth  = 713
  stdheight = 512
  resizeXpos=518
  resizeYpos=421
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLVAR3_3_1 as StdCheck with uid="VTKSCNZHFC",rtseq=62,rtrep=.f.,left=10, top=47, caption=" ",;
    HelpContextID = 8378601,;
    cFormVar="w_FLVAR3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVAR3_3_1.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVAR3_3_1.GetRadio()
    this.Parent.oContained.w_FLVAR3 = this.RadioValue()
    return .t.
  endfunc

  func oFLVAR3_3_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVAR3==.T.,1,;
      0)
  endfunc

  func oFLVAR3_3_1.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVAR3_3_2 as StdField with uid="NHODQAXPFB",rtseq=63,rtrep=.f.,;
    cFormVar = "w_VAR3", cQueryName = "VAR3",;
    bObbl = .f. , nPag = 3, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome variabile",;
    HelpContextID = 40158999,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=119, Top=43, InputMask=replicate('X',25)

  add object oFLTYP3_3_3 as StdCheck with uid="ZANERHCJRB",rtseq=64,rtrep=.f.,left=10, top=74, caption=" ",;
    HelpContextID = 268294377,;
    cFormVar="w_FLTYP3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLTYP3_3_3.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTYP3_3_3.GetRadio()
    this.Parent.oContained.w_FLTYP3 = this.RadioValue()
    return .t.
  endfunc

  func oFLTYP3_3_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTYP3==.T.,1,;
      0)
  endfunc

  func oFLTYP3_3_3.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc


  add object oTYPE_CB_3_4 as StdCombo with uid="ZIAXJAVOIV",rtseq=65,rtrep=.f.,left=119,top=70,width=111,height=22;
    , ToolTipText = "Tipo variabile";
    , HelpContextID = 230340395;
    , cFormVar="w_TYPE_CB",RowSource=""+""+MSG_NUMERIC+","+""+MSG_CHARACTER+","+""+MSG_DATE+"", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTYPE_CB_3_4.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"C",;
    iif(this.value =3,"D",;
    space(1)))))
  endfunc
  func oTYPE_CB_3_4.GetRadio()
    this.Parent.oContained.w_TYPE_CB = this.RadioValue()
    return .t.
  endfunc

  func oTYPE_CB_3_4.SetRadio()
    this.Parent.oContained.w_TYPE_CB=trim(this.Parent.oContained.w_TYPE_CB)
    this.value = ;
      iif(this.Parent.oContained.w_TYPE_CB=="N",1,;
      iif(this.Parent.oContained.w_TYPE_CB=="C",2,;
      iif(this.Parent.oContained.w_TYPE_CB=="D",3,;
      0)))
  endfunc

  add object oFLLEN3_3_5 as StdCheck with uid="IOBGEGOKGY",rtseq=66,rtrep=.f.,left=252, top=74, caption=" ",;
    HelpContextID = 213244137,;
    cFormVar="w_FLLEN3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLLEN3_3_5.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLEN3_3_5.GetRadio()
    this.Parent.oContained.w_FLLEN3 = this.RadioValue()
    return .t.
  endfunc

  func oFLLEN3_3_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLEN3==.T.,1,;
      0)
  endfunc

  func oFLLEN3_3_5.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oLEN3_3_6 as StdField with uid="GORTNMXHYN",rtseq=67,rtrep=.f.,;
    cFormVar = "w_LEN3", cQueryName = "LEN3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza",;
    HelpContextID = 40407319,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=313, Top=70

  add object oDEC3_3_7 as StdField with uid="RIDXWKYKTG",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DEC3", cQueryName = "DEC3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di decimali",;
    HelpContextID = 41130263,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=380, Top=70

  add object oFLEDI3_3_8 as StdCheck with uid="DCCLEBMLBD",rtseq=69,rtrep=.f.,left=10, top=101, caption=" ",;
    HelpContextID = 127850729,;
    cFormVar="w_FLEDI3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLEDI3_3_8.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLEDI3_3_8.GetRadio()
    this.Parent.oContained.w_FLEDI3 = this.RadioValue()
    return .t.
  endfunc

  func oFLEDI3_3_8.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLEDI3==.T.,1,;
      0)
  endfunc

  func oFLEDI3_3_8.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc


  add object oEDIT3_3_9 as StdCombo with uid="JMANBRGCCD",rtseq=70,rtrep=.f.,left=119,top=97,width=55,height=22;
    , ToolTipText = "Se attivo la variabile � editabile";
    , HelpContextID = 44193769;
    , cFormVar="w_EDIT3",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oEDIT3_3_9.RadioValue()
    return(iif(this.value =1,"Edit",;
    iif(this.value =2,"Show",;
    space(4))))
  endfunc
  func oEDIT3_3_9.GetRadio()
    this.Parent.oContained.w_EDIT3 = this.RadioValue()
    return .t.
  endfunc

  func oEDIT3_3_9.SetRadio()
    this.Parent.oContained.w_EDIT3=trim(this.Parent.oContained.w_EDIT3)
    this.value = ;
      iif(this.Parent.oContained.w_EDIT3=="Edit",1,;
      iif(this.Parent.oContained.w_EDIT3=="Show",2,;
      0))
  endfunc

  add object oFLVDS1_3_10 as StdCheck with uid="PEMYOYVNEW",rtseq=71,rtrep=.f.,left=10, top=140, caption=" ",;
    HelpContextID = 28301545,;
    cFormVar="w_FLVDS1", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS1_3_10.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS1_3_10.GetRadio()
    this.Parent.oContained.w_FLVDS1 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS1_3_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS1==.T.,1,;
      0)
  endfunc

  func oFLVDS1_3_10.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE1_C_3_11 as StdField with uid="MVXXGTINVO",rtseq=72,rtrep=.f.,;
    cFormVar = "w_VALUE1_C", cQueryName = "VALUE1_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Primo valore",;
    HelpContextID = 78986616,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=140, InputMask=replicate('X',30)

  add object oDESCRI1_C_3_12 as StdField with uid="ADASGZRLDY",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DESCRI1_C", cQueryName = "DESCRI1_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione primo valore",;
    HelpContextID = 258166774,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=140, InputMask=replicate('X',60)

  add object oFLVDS2_3_13 as StdCheck with uid="FPYHXDXYZC",rtseq=74,rtrep=.f.,left=10, top=162, caption=" ",;
    HelpContextID = 28301545,;
    cFormVar="w_FLVDS2", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS2_3_13.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS2_3_13.GetRadio()
    this.Parent.oContained.w_FLVDS2 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS2_3_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS2==.T.,1,;
      0)
  endfunc

  func oFLVDS2_3_13.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE2_C_3_14 as StdField with uid="XYKBEQWEGT",rtseq=75,rtrep=.f.,;
    cFormVar = "w_VALUE2_C", cQueryName = "VALUE2_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Secondo valore",;
    HelpContextID = 78986616,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=162, InputMask=replicate('X',30)

  add object oDESCRI2_C_3_15 as StdField with uid="GVDYEQDIIA",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DESCRI2_C", cQueryName = "DESCRI2_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione secondo valore",;
    HelpContextID = 258166773,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=162, InputMask=replicate('X',60)

  add object oFLVDS3_3_16 as StdCheck with uid="KWRMJOQQSV",rtseq=77,rtrep=.f.,left=10, top=184, caption=" ",;
    HelpContextID = 28301545,;
    cFormVar="w_FLVDS3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS3_3_16.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS3_3_16.GetRadio()
    this.Parent.oContained.w_FLVDS3 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS3_3_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS3==.T.,1,;
      0)
  endfunc

  func oFLVDS3_3_16.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE3_C_3_17 as StdField with uid="YMRIFIPVKO",rtseq=78,rtrep=.f.,;
    cFormVar = "w_VALUE3_C", cQueryName = "VALUE3_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Terzo valore",;
    HelpContextID = 78986616,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=184, InputMask=replicate('X',30)

  add object oDESCRI3_C_3_18 as StdField with uid="BTZWQBPYFK",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESCRI3_C", cQueryName = "DESCRI3_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione terzo valore",;
    HelpContextID = 258166772,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=184, InputMask=replicate('X',60)

  add object oFLVDS4_3_19 as StdCheck with uid="DIFYLQEHNF",rtseq=80,rtrep=.f.,left=10, top=206, caption=" ",;
    HelpContextID = 28301545,;
    cFormVar="w_FLVDS4", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS4_3_19.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS4_3_19.GetRadio()
    this.Parent.oContained.w_FLVDS4 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS4_3_19.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS4==.T.,1,;
      0)
  endfunc

  func oFLVDS4_3_19.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE4_C_3_20 as StdField with uid="AZEBTOINTF",rtseq=81,rtrep=.f.,;
    cFormVar = "w_VALUE4_C", cQueryName = "VALUE4_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Quarto valore",;
    HelpContextID = 78986616,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=206, InputMask=replicate('X',30)

  add object oDESCRI4_C_3_21 as StdField with uid="DFYTUSQZBQ",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESCRI4_C", cQueryName = "DESCRI4_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione quarto valore",;
    HelpContextID = 258166771,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=206, InputMask=replicate('X',60)

  add object oFLVDS5_3_22 as StdCheck with uid="KIAIJGYSJF",rtseq=83,rtrep=.f.,left=10, top=228, caption=" ",;
    HelpContextID = 240133911,;
    cFormVar="w_FLVDS5", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS5_3_22.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS5_3_22.GetRadio()
    this.Parent.oContained.w_FLVDS5 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS5_3_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS5==.T.,1,;
      0)
  endfunc

  func oFLVDS5_3_22.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE5_C_3_23 as StdField with uid="XFIOLXXYRT",rtseq=84,rtrep=.f.,;
    cFormVar = "w_VALUE5_C", cQueryName = "VALUE5_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Quinto valore",;
    HelpContextID = 78986616,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=228, InputMask=replicate('X',30)

  add object oDESCRI5_C_3_24 as StdField with uid="OACTCUZQDY",rtseq=85,rtrep=.f.,;
    cFormVar = "w_DESCRI5_C", cQueryName = "DESCRI5_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione quinto valore",;
    HelpContextID = 258166770,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=228, InputMask=replicate('X',60)

  add object oFLVDS6_3_25 as StdCheck with uid="JXHUBIOYMH",rtseq=86,rtrep=.f.,left=10, top=250, caption=" ",;
    HelpContextID = 240133911,;
    cFormVar="w_FLVDS6", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS6_3_25.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS6_3_25.GetRadio()
    this.Parent.oContained.w_FLVDS6 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS6_3_25.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS6==.T.,1,;
      0)
  endfunc

  func oFLVDS6_3_25.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE6_C_3_26 as StdField with uid="FHPJAVPRVJ",rtseq=87,rtrep=.f.,;
    cFormVar = "w_VALUE6_C", cQueryName = "VALUE6_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Sesto valore",;
    HelpContextID = 189448840,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=250, InputMask=replicate('X',30)

  add object oDESCRI6_C_3_27 as StdField with uid="OAQRBZPPQV",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESCRI6_C", cQueryName = "DESCRI6_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione sesto valore",;
    HelpContextID = 258166769,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=250, InputMask=replicate('X',60)

  add object oFLVDS7_3_28 as StdCheck with uid="TBYKNQRBEF",rtseq=89,rtrep=.f.,left=10, top=272, caption=" ",;
    HelpContextID = 240133911,;
    cFormVar="w_FLVDS7", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS7_3_28.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS7_3_28.GetRadio()
    this.Parent.oContained.w_FLVDS7 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS7_3_28.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS7==.T.,1,;
      0)
  endfunc

  func oFLVDS7_3_28.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE7_C_3_29 as StdField with uid="VHUURSSQAK",rtseq=90,rtrep=.f.,;
    cFormVar = "w_VALUE7_C", cQueryName = "VALUE7_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Settimo valore",;
    HelpContextID = 189448840,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=272, InputMask=replicate('X',30)

  add object oDESCRI7_C_3_30 as StdField with uid="DZOAUCPJJW",rtseq=91,rtrep=.f.,;
    cFormVar = "w_DESCRI7_C", cQueryName = "DESCRI7_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione settimo valore",;
    HelpContextID = 258166768,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=272, InputMask=replicate('X',60)

  add object oFLVDS8_3_31 as StdCheck with uid="DWXARKBJYY",rtseq=92,rtrep=.f.,left=10, top=294, caption=" ",;
    HelpContextID = 240133911,;
    cFormVar="w_FLVDS8", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS8_3_31.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS8_3_31.GetRadio()
    this.Parent.oContained.w_FLVDS8 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS8_3_31.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS8==.T.,1,;
      0)
  endfunc

  func oFLVDS8_3_31.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE8_C_3_32 as StdField with uid="WPMOMJMWUM",rtseq=93,rtrep=.f.,;
    cFormVar = "w_VALUE8_C", cQueryName = "VALUE8_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Ottavo valore",;
    HelpContextID = 189448840,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=294, InputMask=replicate('X',30)

  add object oDESCRI8_C_3_33 as StdField with uid="HVCVRSDCLP",rtseq=94,rtrep=.f.,;
    cFormVar = "w_DESCRI8_C", cQueryName = "DESCRI8_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione ottavo valore",;
    HelpContextID = 258166767,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=294, InputMask=replicate('X',60)

  add object oFLVDS9_3_34 as StdCheck with uid="GEOVPHSKPJ",rtseq=95,rtrep=.f.,left=10, top=316, caption=" ",;
    HelpContextID = 240133911,;
    cFormVar="w_FLVDS9", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS9_3_34.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS9_3_34.GetRadio()
    this.Parent.oContained.w_FLVDS9 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS9_3_34.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS9==.T.,1,;
      0)
  endfunc

  func oFLVDS9_3_34.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE9_C_3_35 as StdField with uid="NWJIHYPLQS",rtseq=96,rtrep=.f.,;
    cFormVar = "w_VALUE9_C", cQueryName = "VALUE9_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nono valore",;
    HelpContextID = 189448840,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=316, InputMask=replicate('X',30)

  add object oDESCRI9_C_3_36 as StdField with uid="DMUJGEOIRZ",rtseq=97,rtrep=.f.,;
    cFormVar = "w_DESCRI9_C", cQueryName = "DESCRI9_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nono valore",;
    HelpContextID = 258166766,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=316, InputMask=replicate('X',60)

  add object oFLVDS10_3_37 as StdCheck with uid="IGQXPLWMFU",rtseq=98,rtrep=.f.,left=10, top=338, caption=" ",;
    HelpContextID = 28301593,;
    cFormVar="w_FLVDS10", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVDS10_3_37.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVDS10_3_37.GetRadio()
    this.Parent.oContained.w_FLVDS10 = this.RadioValue()
    return .t.
  endfunc

  func oFLVDS10_3_37.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVDS10==.T.,1,;
      0)
  endfunc

  func oFLVDS10_3_37.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oVALUE10_C_3_38 as StdField with uid="CQCDPQTTXJ",rtseq=99,rtrep=.f.,;
    cFormVar = "w_VALUE10_C", cQueryName = "VALUE10_C",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decimo valore",;
    HelpContextID = 79004169,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=32, Top=338, InputMask=replicate('X',30)

  add object oDESCRI10_C_3_39 as StdField with uid="KCVKLRPJLV",rtseq=100,rtrep=.f.,;
    cFormVar = "w_DESCRI10_C", cQueryName = "DESCRI10_C",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione decimo valore",;
    HelpContextID = 257885926,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=135, Top=338, InputMask=replicate('X',60)

  add object oFLFNT3_3_40 as StdCheck with uid="TXJSFYJTFW",rtseq=101,rtrep=.f.,left=10, top=375, caption=" ",;
    HelpContextID = 54515945,;
    cFormVar="w_FLFNT3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLFNT3_3_40.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFNT3_3_40.GetRadio()
    this.Parent.oContained.w_FLFNT3 = this.RadioValue()
    return .t.
  endfunc

  func oFLFNT3_3_40.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFNT3==.T.,1,;
      0)
  endfunc

  func oFLFNT3_3_40.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oFNTNAME3_3_41 as StdField with uid="FACNPNZYMX",rtseq=102,rtrep=.f.,;
    cFormVar = "w_FNTNAME3", cQueryName = "FNTNAME3",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(180), bMultilanguage =  .f.,;
    HelpContextID = 263324578,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=119, Top=371, InputMask=replicate('X',180)

  add object oFNTSIZE3_3_42 as StdField with uid="SGQDHESEBQ",rtseq=103,rtrep=.f.,;
    cFormVar = "w_FNTSIZE3", cQueryName = "FNTSIZE3",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 123863970,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=308, Top=371, cSayPict='"99"', cGetPict='"99"'


  add object oBtn_3_43 as StdButton with uid="AZERLKMGBQ",left=205, top=398, width=95,height=22,;
    caption="Change font", nPag=3;
    , ToolTipText = "Selezione font";
    , HelpContextID = 120067223;
  , bGlobalFont=.t.

    proc oBtn_3_43.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Font",3)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT .w_GLBFONT3)
      endwith
    endif
  endfunc

  add object oFLGLF3_3_44 as StdCheck with uid="JGSDUZKITB",rtseq=104,rtrep=.f.,left=10, top=402, caption=" ",;
    HelpContextID = 86038761,;
    cFormVar="w_FLGLF3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLGLF3_3_44.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLGLF3_3_44.GetRadio()
    this.Parent.oContained.w_FLGLF3 = this.RadioValue()
    return .t.
  endfunc

  func oFLGLF3_3_44.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLGLF3==.T.,1,;
      0)
  endfunc

  func oFLGLF3_3_44.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc


  add object oGLBFONT3_3_45 as StdCombo with uid="YOCNOIADUY",rtseq=105,rtrep=.f.,left=119,top=398,width=55,height=22;
    , ToolTipText = "Se attivo utilizza il font globale";
    , HelpContextID = 230415725;
    , cFormVar="w_GLBFONT3",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oGLBFONT3_3_45.RadioValue()
    return(iif(this.value =1,.T.,;
    iif(this.value =2,.F.,;
    .f.)))
  endfunc
  func oGLBFONT3_3_45.GetRadio()
    this.Parent.oContained.w_GLBFONT3 = this.RadioValue()
    return .t.
  endfunc

  func oGLBFONT3_3_45.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GLBFONT3==.T.,1,;
      iif(this.Parent.oContained.w_GLBFONT3==.F.,2,;
      0))
  endfunc

  add object oFLTOP3_3_46 as StdCheck with uid="MTVVGMTKRD",rtseq=106,rtrep=.f.,left=10, top=456, caption=" ",;
    HelpContextID = 257808617,;
    cFormVar="w_FLTOP3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLTOP3_3_46.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTOP3_3_46.GetRadio()
    this.Parent.oContained.w_FLTOP3 = this.RadioValue()
    return .t.
  endfunc

  func oFLTOP3_3_46.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTOP3==.T.,1,;
      0)
  endfunc

  func oFLTOP3_3_46.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oTOP3_3_47 as StdField with uid="WQZDNCHWQD",rtseq=107,rtrep=.f.,;
    cFormVar = "w_TOP3", cQueryName = "TOP3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione della combo nella maschera (in alto)",;
    HelpContextID = 40233239,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=100, Top=453, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLLEF3_3_48 as StdCheck with uid="EHGOCAMMRJ",rtseq=108,rtrep=.f.,left=10, top=487, caption=" ",;
    HelpContextID = 79026409,;
    cFormVar="w_FLLEF3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLLEF3_3_48.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLEF3_3_48.GetRadio()
    this.Parent.oContained.w_FLLEF3 = this.RadioValue()
    return .t.
  endfunc

  func oFLLEF3_3_48.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLEF3==.T.,1,;
      0)
  endfunc

  func oFLLEF3_3_48.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oLEFT3_3_49 as StdField with uid="WQPNHTLXJT",rtseq=109,rtrep=.f.,;
    cFormVar = "w_LEFT3", cQueryName = "LEFT3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione della combo nella maschera (a sinistra)",;
    HelpContextID = 44003049,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=100, Top=484, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLHGT3_3_50 as StdCheck with uid="YXSXRVACJV",rtseq=110,rtrep=.f.,left=224, top=456, caption=" ",;
    HelpContextID = 47306985,;
    cFormVar="w_FLHGT3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLHGT3_3_50.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLHGT3_3_50.GetRadio()
    this.Parent.oContained.w_FLHGT3 = this.RadioValue()
    return .t.
  endfunc

  func oFLHGT3_3_50.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLHGT3==.T.,1,;
      0)
  endfunc

  func oFLHGT3_3_50.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oHEIGHT3_3_51 as StdField with uid="LDXOYJFHAD",rtseq=111,rtrep=.f.,;
    cFormVar = "w_HEIGHT3", cQueryName = "HEIGHT3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica l'altezza della combo",;
    HelpContextID = 114453276,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=299, Top=452, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLWDT3_3_52 as StdCheck with uid="LDQXWPQYHM",rtseq=112,rtrep=.f.,left=224, top=487, caption=" ",;
    HelpContextID = 45144297,;
    cFormVar="w_FLWDT3", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLWDT3_3_52.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLWDT3_3_52.GetRadio()
    this.Parent.oContained.w_FLWDT3 = this.RadioValue()
    return .t.
  endfunc

  func oFLWDT3_3_52.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLWDT3==.T.,1,;
      0)
  endfunc

  func oFLWDT3_3_52.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc

  add object oWIDTH3_3_53 as StdField with uid="QEXCGQGYNO",rtseq=113,rtrep=.f.,;
    cFormVar = "w_WIDTH3", cQueryName = "WIDTH3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la larghezza della combo",;
    HelpContextID = 127777257,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=299, Top=483, cSayPict='"9999"', cGetPict='"9999"'

  add object oSELECT3_3_54 as StdRadio with uid="YJEFNETXSG",rtseq=114,rtrep=.f.,left=357, top=8, width=186,height=35;
    , cFormVar="w_SELECT3", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oSELECT3_3_54.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption=""+MSG_SELECT_ALL_TEXT+""
      this.Buttons(1).HelpContextID = 28669468
      this.Buttons(1).Top=0
      this.Buttons(2).Caption=""+MSG_DESELECT_ALL+""
      this.Buttons(2).HelpContextID = 28669468
      this.Buttons(2).Top=16
      this.SetAll("Width",184)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECT3_3_54.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    "N")))
  endfunc
  func oSELECT3_3_54.GetRadio()
    this.Parent.oContained.w_SELECT3 = this.RadioValue()
    return .t.
  endfunc

  func oSELECT3_3_54.SetRadio()
    this.Parent.oContained.w_SELECT3=trim(this.Parent.oContained.w_SELECT3)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT3=="S",1,;
      iif(this.Parent.oContained.w_SELECT3=="N",2,;
      0))
  endfunc

  func oSELECT3_3_54.mHide()
    with this.Parent.oContained
      return (.w_HIDE3=1)
    endwith
  endfunc


  add object oBtn_3_55 as StdButton with uid="EBMHXLUEML",left=553, top=462, width=48,height=45,;
    CpPicture="BMP\applica.ico", caption="", nPag=3;
    , ToolTipText = "Premere per applicare le modifiche";
    , HelpContextID = 172758090;
    , caption="\<Applica";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_55.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Save",3)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_56 as StdButton with uid="FRPKSITUPP",left=608, top=462, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 98971159;
    , caption="\<Ok";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_56.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_57 as StdButton with uid="PFVOIHBNZW",left=663, top=462, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per annullare le operazioni svolte";
    , HelpContextID = 250656791;
    , caption="\<Esci";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_57.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFNTCOLOR3_3_74 as StdField with uid="KCLBGQSKLH",rtseq=237,rtrep=.f.,;
    cFormVar = "w_FNTCOLOR3", cQueryName = "FNTCOLOR3",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 39964328,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=308, Top=398


  add object zCombo as cp_szoombox with uid="WMIQDTHXNO",left=503, top=7, width=208,height=447,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="objmaskoptions",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomFile="default",bOptions=.f.,;
    cEvent = "Pagina3",;
    nPag=3;
    , HelpContextID = 204127767


  add object combo as cp_calclbl with uid="LLVFCSGVNJ",left=10, top=8, width=262,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,;
    nPag=3;
    , HelpContextID = 204127767


  add object oBtn_3_87 as StdButton with uid="NZUTCSFPRV",left=343, top=398, width=23,height=22,;
    caption="...", nPag=3;
    , ToolTipText = "Modifica colore font";
    , HelpContextID = 96083735;
  , bGlobalFont=.t.

    proc oBtn_3_87.Click()
      this.parent.oContained.NotifyEvent("FontColor3")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_3_58 as StdString with uid="IEYDNTIFTU",Visible=.t., Left=42, Top=47,;
    Alignment=0, Width=90, Height=18,;
    Caption="Variabile:"  ;
  , bGlobalFont=.t.

  add object oStr_3_59 as StdString with uid="VLKICBJTFX",Visible=.t., Left=42, Top=74,;
    Alignment=0, Width=67, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_60 as StdString with uid="TESGOHAUQF",Visible=.t., Left=42, Top=375,;
    Alignment=0, Width=73, Height=18,;
    Caption="Font Options:"  ;
  , bGlobalFont=.t.

  add object oStr_3_61 as StdString with uid="XTIKVOQFWG",Visible=.t., Left=42, Top=402,;
    Alignment=0, Width=62, Height=18,;
    Caption="Global font:"  ;
  , bGlobalFont=.t.

  add object oStr_3_62 as StdString with uid="UMUTMSZQCN",Visible=.t., Left=47, Top=423,;
    Alignment=0, Width=178, Height=18,;
    Caption="Posizione e Dimensione"  ;
  , bGlobalFont=.t.

  add object oStr_3_64 as StdString with uid="LOJJRQWQNZ",Visible=.t., Left=280, Top=74,;
    Alignment=0, Width=27, Height=18,;
    Caption="Lun.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_65 as StdString with uid="KHXEBOUJNV",Visible=.t., Left=346, Top=74,;
    Alignment=0, Width=28, Height=18,;
    Caption="Dec:"  ;
  , bGlobalFont=.t.

  add object oStr_3_66 as StdString with uid="OOCGGUPCTK",Visible=.t., Left=42, Top=101,;
    Alignment=0, Width=82, Height=18,;
    Caption="Editabile:"  ;
  , bGlobalFont=.t.

  add object oStr_3_67 as StdString with uid="FPCZFIXWEF",Visible=.t., Left=39, Top=122,;
    Alignment=0, Width=31, Height=18,;
    Caption="Valori"  ;
  , bGlobalFont=.t.

  add object oStr_3_68 as StdString with uid="YDCGORSYHE",Visible=.t., Left=145, Top=122,;
    Alignment=0, Width=61, Height=18,;
    Caption="Descrizioni"  ;
  , bGlobalFont=.t.

  add object oStr_3_69 as StdString with uid="UHGZSCUWKC",Visible=.t., Left=42, Top=457,;
    Alignment=0, Width=62, Height=18,;
    Caption="Top:"  ;
  , bGlobalFont=.t.

  add object oStr_3_70 as StdString with uid="YDXAKJFFTF",Visible=.t., Left=42, Top=488,;
    Alignment=0, Width=58, Height=18,;
    Caption="Left:"  ;
  , bGlobalFont=.t.

  add object oStr_3_71 as StdString with uid="KUHKWSQQYJ",Visible=.t., Left=243, Top=456,;
    Alignment=0, Width=84, Height=18,;
    Caption="Height:"  ;
  , bGlobalFont=.t.

  add object oStr_3_72 as StdString with uid="KFAJHSOCSQ",Visible=.t., Left=243, Top=487,;
    Alignment=0, Width=68, Height=18,;
    Caption="Width:"  ;
  , bGlobalFont=.t.

  add object oBox_3_63 as StdBox with uid="FKZCZGUZEJ",left=33, top=445, width=381,height=1

  add object oBox_3_85 as StdBox with uid="ODSQBVFQGN",left=10, top=33, width=286,height=2
enddefine
define class tcp_objmaskoptionsPag4 as StdContainer
  Width  = 713
  height = 512
  stdWidth  = 713
  stdheight = 512
  resizeXpos=511
  resizeYpos=439
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLVAR4_4_1 as StdCheck with uid="ECPJHQVBOX",rtseq=115,rtrep=.f.,left=10, top=47, caption=" ",;
    HelpContextID = 8378601,;
    cFormVar="w_FLVAR4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLVAR4_4_1.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVAR4_4_1.GetRadio()
    this.Parent.oContained.w_FLVAR4 = this.RadioValue()
    return .t.
  endfunc

  func oFLVAR4_4_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVAR4==.T.,1,;
      0)
  endfunc

  func oFLVAR4_4_1.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oVAR4_4_2 as StdField with uid="DOSXIRYQKE",rtseq=116,rtrep=.f.,;
    cFormVar = "w_VAR4", cQueryName = "VAR4",;
    bObbl = .f. , nPag = 4, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome della variabile",;
    HelpContextID = 39110423,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=119, Top=43, InputMask=replicate('X',25)

  add object oFLTTL4_4_3 as StdCheck with uid="PIAAKLQTAG",rtseq=117,rtrep=.f.,left=10, top=74, caption=" ",;
    HelpContextID = 195942633,;
    cFormVar="w_FLTTL4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLTTL4_4_3.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTTL4_4_3.GetRadio()
    this.Parent.oContained.w_FLTTL4 = this.RadioValue()
    return .t.
  endfunc

  func oFLTTL4_4_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTTL4==.T.,1,;
      0)
  endfunc

  func oFLTTL4_4_3.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oTITLE4_4_4 as StdField with uid="CJVTKRCTNS",rtseq=118,rtrep=.f.,;
    cFormVar = "w_TITLE4", cQueryName = "TITLE4",;
    bObbl = .f. , nPag = 4, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Titolo della checkbox",;
    HelpContextID = 70104809,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=119, Top=70, InputMask=replicate('X',100)

  add object oFLTYP4_4_5 as StdCheck with uid="UJRFNGTTOV",rtseq=119,rtrep=.f.,left=10, top=101, caption=" ",;
    HelpContextID = 268294377,;
    cFormVar="w_FLTYP4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLTYP4_4_5.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTYP4_4_5.GetRadio()
    this.Parent.oContained.w_FLTYP4 = this.RadioValue()
    return .t.
  endfunc

  func oFLTYP4_4_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTYP4==.T.,1,;
      0)
  endfunc

  func oFLTYP4_4_5.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc


  add object oTYPE_CH_4_6 as StdCombo with uid="CXXAXTISQG",rtseq=120,rtrep=.f.,left=119,top=97,width=111,height=22;
    , ToolTipText = "Tipo della variabile";
    , HelpContextID = 230340401;
    , cFormVar="w_TYPE_CH",RowSource=""+""+MSG_NUMERIC+","+""+MSG_CHARACTER+","+""+MSG_DATE+"", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTYPE_CH_4_6.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"C",;
    iif(this.value =3,"D",;
    space(1)))))
  endfunc
  func oTYPE_CH_4_6.GetRadio()
    this.Parent.oContained.w_TYPE_CH = this.RadioValue()
    return .t.
  endfunc

  func oTYPE_CH_4_6.SetRadio()
    this.Parent.oContained.w_TYPE_CH=trim(this.Parent.oContained.w_TYPE_CH)
    this.value = ;
      iif(this.Parent.oContained.w_TYPE_CH=="N",1,;
      iif(this.Parent.oContained.w_TYPE_CH=="C",2,;
      iif(this.Parent.oContained.w_TYPE_CH=="D",3,;
      0)))
  endfunc

  add object oFLLEN4_4_7 as StdCheck with uid="ITOOOXAOTM",rtseq=121,rtrep=.f.,left=260, top=101, caption=" ",;
    HelpContextID = 213244137,;
    cFormVar="w_FLLEN4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLLEN4_4_7.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLEN4_4_7.GetRadio()
    this.Parent.oContained.w_FLLEN4 = this.RadioValue()
    return .t.
  endfunc

  func oFLLEN4_4_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLEN4==.T.,1,;
      0)
  endfunc

  func oFLLEN4_4_7.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oLEN4_4_8 as StdField with uid="VJUHOMJQGH",rtseq=122,rtrep=.f.,;
    cFormVar = "w_LEN4", cQueryName = "LEN4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza",;
    HelpContextID = 39358743,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=318, Top=97

  add object oDEC4_4_9 as StdField with uid="NDNAKZWXCW",rtseq=123,rtrep=.f.,;
    cFormVar = "w_DEC4", cQueryName = "DEC4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di decimali",;
    HelpContextID = 40081687,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=385, Top=97

  add object oFLEDI4_4_10 as StdCheck with uid="PKAYIOWCJO",rtseq=124,rtrep=.f.,left=10, top=128, caption=" ",;
    HelpContextID = 127850729,;
    cFormVar="w_FLEDI4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLEDI4_4_10.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLEDI4_4_10.GetRadio()
    this.Parent.oContained.w_FLEDI4 = this.RadioValue()
    return .t.
  endfunc

  func oFLEDI4_4_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLEDI4==.T.,1,;
      0)
  endfunc

  func oFLEDI4_4_10.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc


  add object oEDIT4_4_11 as StdCombo with uid="ADMFFXSQPR",rtseq=125,rtrep=.f.,left=119,top=124,width=55,height=22;
    , ToolTipText = "Se attivo la variabile � editabile";
    , HelpContextID = 60970985;
    , cFormVar="w_EDIT4",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oEDIT4_4_11.RadioValue()
    return(iif(this.value =1,"Edit",;
    iif(this.value =2,"Show",;
    space(4))))
  endfunc
  func oEDIT4_4_11.GetRadio()
    this.Parent.oContained.w_EDIT4 = this.RadioValue()
    return .t.
  endfunc

  func oEDIT4_4_11.SetRadio()
    this.Parent.oContained.w_EDIT4=trim(this.Parent.oContained.w_EDIT4)
    this.value = ;
      iif(this.Parent.oContained.w_EDIT4=="Edit",1,;
      iif(this.Parent.oContained.w_EDIT4=="Show",2,;
      0))
  endfunc

  add object oFLSEL4_4_12 as StdCheck with uid="EKSVDIMLSX",rtseq=126,rtrep=.f.,left=10, top=155, caption=" ",;
    HelpContextID = 180148457,;
    cFormVar="w_FLSEL4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLSEL4_4_12.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLSEL4_4_12.GetRadio()
    this.Parent.oContained.w_FLSEL4 = this.RadioValue()
    return .t.
  endfunc

  func oFLSEL4_4_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLSEL4==.T.,1,;
      0)
  endfunc

  func oFLSEL4_4_12.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oSELECTED4_4_13 as StdField with uid="NLBPHOGGIC",rtseq=127,rtrep=.f.,;
    cFormVar = "w_SELECTED4", cQueryName = "SELECTED4",;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Valore da assegnare se selezionato",;
    HelpContextID = 28683886,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=189, Top=151, InputMask=replicate('X',30)

  add object oFLUNS4_4_14 as StdCheck with uid="QMVQGDLYNK",rtseq=128,rtrep=.f.,left=10, top=182, caption=" ",;
    HelpContextID = 38721769,;
    cFormVar="w_FLUNS4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLUNS4_4_14.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLUNS4_4_14.GetRadio()
    this.Parent.oContained.w_FLUNS4 = this.RadioValue()
    return .t.
  endfunc

  func oFLUNS4_4_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLUNS4==.T.,1,;
      0)
  endfunc

  func oFLUNS4_4_14.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oUNSELECTED4_4_15 as StdField with uid="ALYNDBWTFS",rtseq=129,rtrep=.f.,;
    cFormVar = "w_UNSELECTED4", cQueryName = "UNSELECTED4",;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Valore da assegnare se non selezionato",;
    HelpContextID = 183865964,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=189, Top=178, InputMask=replicate('X',30)

  add object oFLFNT4_4_16 as StdCheck with uid="JSCDPLXUFX",rtseq=130,rtrep=.f.,left=10, top=243, caption=" ",;
    HelpContextID = 54515945,;
    cFormVar="w_FLFNT4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLFNT4_4_16.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFNT4_4_16.GetRadio()
    this.Parent.oContained.w_FLFNT4 = this.RadioValue()
    return .t.
  endfunc

  func oFLFNT4_4_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFNT4==.T.,1,;
      0)
  endfunc

  func oFLFNT4_4_16.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oFNTNAME4_4_17 as StdField with uid="WWNUECRMXF",rtseq=131,rtrep=.f.,;
    cFormVar = "w_FNTNAME4", cQueryName = "FNTNAME4",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(180), bMultilanguage =  .f.,;
    HelpContextID = 263324562,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=119, Top=239, InputMask=replicate('X',180)

  add object oFNTSIZE4_4_18 as StdField with uid="QDNWLRRGUA",rtseq=132,rtrep=.f.,;
    cFormVar = "w_FNTSIZE4", cQueryName = "FNTSIZE4",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 123863954,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=308, Top=239, cSayPict='"99"', cGetPict='"99"'


  add object oBtn_4_19 as StdButton with uid="TIBIFVFYVJ",left=205, top=266, width=95,height=22,;
    caption="Change font", nPag=4;
    , ToolTipText = "Selezione font";
    , HelpContextID = 120067223;
  , bGlobalFont=.t.

    proc oBtn_4_19.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Font",4)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT .w_GLBFONT4)
      endwith
    endif
  endfunc

  add object oFLGLF4_4_20 as StdCheck with uid="WAJTCXRHGX",rtseq=133,rtrep=.f.,left=10, top=270, caption=" ",;
    HelpContextID = 86038761,;
    cFormVar="w_FLGLF4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLGLF4_4_20.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLGLF4_4_20.GetRadio()
    this.Parent.oContained.w_FLGLF4 = this.RadioValue()
    return .t.
  endfunc

  func oFLGLF4_4_20.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLGLF4==.T.,1,;
      0)
  endfunc

  func oFLGLF4_4_20.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc


  add object oGLBFONT4_4_21 as StdCombo with uid="JQRJQIWKGP",rtseq=134,rtrep=.f.,left=119,top=266,width=55,height=22;
    , ToolTipText = "Se attivo utilizza il font globale";
    , HelpContextID = 230415741;
    , cFormVar="w_GLBFONT4",RowSource=""+"No,"+"Si", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oGLBFONT4_4_21.RadioValue()
    return(iif(this.value =1,.F.,;
    iif(this.value =2,.T.,;
    .f.)))
  endfunc
  func oGLBFONT4_4_21.GetRadio()
    this.Parent.oContained.w_GLBFONT4 = this.RadioValue()
    return .t.
  endfunc

  func oGLBFONT4_4_21.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GLBFONT4==.F.,1,;
      iif(this.Parent.oContained.w_GLBFONT4==.T.,2,;
      0))
  endfunc

  add object oFLTOP4_4_22 as StdCheck with uid="XQHDWFXIIW",rtseq=135,rtrep=.f.,left=10, top=328, caption=" ",;
    HelpContextID = 257808617,;
    cFormVar="w_FLTOP4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLTOP4_4_22.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTOP4_4_22.GetRadio()
    this.Parent.oContained.w_FLTOP4 = this.RadioValue()
    return .t.
  endfunc

  func oFLTOP4_4_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTOP4==.T.,1,;
      0)
  endfunc

  func oFLTOP4_4_22.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oTOP4_4_23 as StdField with uid="XYJYYMHBVM",rtseq=136,rtrep=.f.,;
    cFormVar = "w_TOP4", cQueryName = "TOP4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione della checkbox nella maschera (in alto)",;
    HelpContextID = 39184663,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=95, Top=325, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLLEF4_4_24 as StdCheck with uid="CUVRZHMRYC",rtseq=137,rtrep=.f.,left=10, top=359, caption=" ",;
    HelpContextID = 79026409,;
    cFormVar="w_FLLEF4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLLEF4_4_24.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLEF4_4_24.GetRadio()
    this.Parent.oContained.w_FLLEF4 = this.RadioValue()
    return .t.
  endfunc

  func oFLLEF4_4_24.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLEF4==.T.,1,;
      0)
  endfunc

  func oFLLEF4_4_24.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oLEFT4_4_25 as StdField with uid="JPVGAQUDDR",rtseq=138,rtrep=.f.,;
    cFormVar = "w_LEFT4", cQueryName = "LEFT4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione della checkbox nella maschera (a sinistra)",;
    HelpContextID = 60780265,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=95, Top=356, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLHGT4_4_26 as StdCheck with uid="OVUAPSYWUS",rtseq=139,rtrep=.f.,left=189, top=328, caption=" ",;
    HelpContextID = 47306985,;
    cFormVar="w_FLHGT4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLHGT4_4_26.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLHGT4_4_26.GetRadio()
    this.Parent.oContained.w_FLHGT4 = this.RadioValue()
    return .t.
  endfunc

  func oFLHGT4_4_26.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLHGT4==.T.,1,;
      0)
  endfunc

  func oFLHGT4_4_26.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oHEIGHT4_4_27 as StdField with uid="FOQMFZDHUT",rtseq=140,rtrep=.f.,;
    cFormVar = "w_HEIGHT4", cQueryName = "HEIGHT4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica l'altezza della checkbox",;
    HelpContextID = 114453277,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=271, Top=324, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLWDT4_4_28 as StdCheck with uid="RNLUHCYQTP",rtseq=141,rtrep=.f.,left=189, top=359, caption=" ",;
    HelpContextID = 45144297,;
    cFormVar="w_FLWDT4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLWDT4_4_28.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLWDT4_4_28.GetRadio()
    this.Parent.oContained.w_FLWDT4 = this.RadioValue()
    return .t.
  endfunc

  func oFLWDT4_4_28.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLWDT4==.T.,1,;
      0)
  endfunc

  func oFLWDT4_4_28.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc

  add object oWIDTH4_4_29 as StdField with uid="SOVDEDSBZC",rtseq=142,rtrep=.f.,;
    cFormVar = "w_WIDTH4", cQueryName = "WIDTH4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la larghezza della checkbox",;
    HelpContextID = 127777257,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=271, Top=355, cSayPict='"9999"', cGetPict='"9999"'

  add object oSELECT4_4_30 as StdRadio with uid="BIVDIMPEEW",rtseq=143,rtrep=.f.,left=357, top=8, width=200,height=35;
    , cFormVar="w_SELECT4", ButtonCount=2, bObbl=.f., nPag=4;
  , bGlobalFont=.t.

    proc oSELECT4_4_30.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption=""+MSG_SELECT_ALL_TEXT+""
      this.Buttons(1).HelpContextID = 28669469
      this.Buttons(1).Top=0
      this.Buttons(2).Caption=""+MSG_DESELECT_ALL+""
      this.Buttons(2).HelpContextID = 28669469
      this.Buttons(2).Top=16
      this.SetAll("Width",198)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECT4_4_30.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    "N")))
  endfunc
  func oSELECT4_4_30.GetRadio()
    this.Parent.oContained.w_SELECT4 = this.RadioValue()
    return .t.
  endfunc

  func oSELECT4_4_30.SetRadio()
    this.Parent.oContained.w_SELECT4=trim(this.Parent.oContained.w_SELECT4)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT4=="S",1,;
      iif(this.Parent.oContained.w_SELECT4=="N",2,;
      0))
  endfunc

  func oSELECT4_4_30.mHide()
    with this.Parent.oContained
      return (.w_HIDE4=1)
    endwith
  endfunc


  add object oBtn_4_31 as StdButton with uid="PTOWJMBUNX",left=553, top=462, width=48,height=45,;
    CpPicture="BMP\applica.ico", caption="", nPag=4;
    , ToolTipText = "Premere per applicare le modifiche";
    , HelpContextID = 172758090;
    , caption="\<Applica";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_31.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Save",4)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_32 as StdButton with uid="RBGNKEAVKS",left=608, top=462, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 98971159;
    , caption="\<Ok";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_32.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_33 as StdButton with uid="GYWAAMDHTJ",left=663, top=462, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per annullare le operazioni svolte";
    , HelpContextID = 250656791;
    , caption="\<Esci";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFNTCOLOR4_4_51 as StdField with uid="GEGSQSJXQP",rtseq=242,rtrep=.f.,;
    cFormVar = "w_FNTCOLOR4", cQueryName = "FNTCOLOR4",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 39964072,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=308, Top=266


  add object zCheck as cp_szoombox with uid="SRKTXCZXCF",left=503, top=7, width=208,height=447,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="objmaskoptions",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomFile="default",bOptions=.f.,;
    cEvent = "Pagina4",;
    nPag=4;
    , HelpContextID = 204127767


  add object check as cp_calclbl with uid="APZIKVMBOT",left=10, top=8, width=262,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,;
    nPag=4;
    , HelpContextID = 204127767


  add object oBtn_4_63 as StdButton with uid="GAHDVRGIVM",left=343, top=266, width=23,height=22,;
    caption="...", nPag=4;
    , ToolTipText = "Modifica colore font";
    , HelpContextID = 96083735;
  , bGlobalFont=.t.

    proc oBtn_4_63.Click()
      this.parent.oContained.NotifyEvent("FontColor4")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_4_34 as StdString with uid="ERFNTZPYCO",Visible=.t., Left=34, Top=47,;
    Alignment=0, Width=79, Height=18,;
    Caption="Variabile:"  ;
  , bGlobalFont=.t.

  add object oStr_4_35 as StdString with uid="ENALNCPZTM",Visible=.t., Left=34, Top=101,;
    Alignment=0, Width=45, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_36 as StdString with uid="RSNOYIUNUT",Visible=.t., Left=285, Top=101,;
    Alignment=0, Width=27, Height=18,;
    Caption="Lun.:"  ;
  , bGlobalFont=.t.

  add object oStr_4_37 as StdString with uid="ACQZBMHTFT",Visible=.t., Left=351, Top=101,;
    Alignment=0, Width=28, Height=18,;
    Caption="Dec:"  ;
  , bGlobalFont=.t.

  add object oStr_4_38 as StdString with uid="HCRZSVUNAK",Visible=.t., Left=34, Top=128,;
    Alignment=0, Width=76, Height=18,;
    Caption="Editabile:"  ;
  , bGlobalFont=.t.

  add object oStr_4_39 as StdString with uid="VSKCQSIODJ",Visible=.t., Left=34, Top=74,;
    Alignment=0, Width=35, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_40 as StdString with uid="IRDYCYJYOI",Visible=.t., Left=34, Top=155,;
    Alignment=0, Width=121, Height=18,;
    Caption="Valore se selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_4_41 as StdString with uid="XQEYXVXRHN",Visible=.t., Left=34, Top=182,;
    Alignment=0, Width=145, Height=18,;
    Caption="Valore se non selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_4_42 as StdString with uid="YWZBSPEXJV",Visible=.t., Left=34, Top=243,;
    Alignment=0, Width=73, Height=18,;
    Caption="Font Options:"  ;
  , bGlobalFont=.t.

  add object oStr_4_43 as StdString with uid="ERNHKYNNMY",Visible=.t., Left=34, Top=270,;
    Alignment=0, Width=62, Height=18,;
    Caption="Global font:"  ;
  , bGlobalFont=.t.

  add object oStr_4_44 as StdString with uid="GDORQATMEZ",Visible=.t., Left=46, Top=295,;
    Alignment=0, Width=171, Height=18,;
    Caption="Posizione e Dimensione"  ;
  , bGlobalFont=.t.

  add object oStr_4_46 as StdString with uid="LLIXXCAMJR",Visible=.t., Left=34, Top=329,;
    Alignment=0, Width=50, Height=18,;
    Caption="Top:"  ;
  , bGlobalFont=.t.

  add object oStr_4_47 as StdString with uid="WLCURUUNOT",Visible=.t., Left=34, Top=360,;
    Alignment=0, Width=61, Height=18,;
    Caption="Left:"  ;
  , bGlobalFont=.t.

  add object oStr_4_48 as StdString with uid="DRGWLWVAEU",Visible=.t., Left=213, Top=328,;
    Alignment=0, Width=69, Height=18,;
    Caption="Height:"  ;
  , bGlobalFont=.t.

  add object oStr_4_49 as StdString with uid="OZFYQJMOKG",Visible=.t., Left=213, Top=359,;
    Alignment=0, Width=88, Height=18,;
    Caption="Width:"  ;
  , bGlobalFont=.t.

  add object oBox_4_45 as StdBox with uid="LEKCKTEFKC",left=32, top=317, width=381,height=1

  add object oBox_4_61 as StdBox with uid="WQGOSGHMLV",left=10, top=33, width=286,height=2
enddefine
define class tcp_objmaskoptionsPag5 as StdContainer
  Width  = 713
  height = 512
  stdWidth  = 713
  stdheight = 512
  resizeXpos=531
  resizeYpos=409
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLEXE5_5_1 as StdCheck with uid="FUVTXVIRXP",rtseq=145,rtrep=.f.,left=10, top=47, caption=" ",;
    HelpContextID = 81713385,;
    cFormVar="w_FLEXE5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLEXE5_5_1.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLEXE5_5_1.GetRadio()
    this.Parent.oContained.w_FLEXE5 = this.RadioValue()
    return .t.
  endfunc

  func oFLEXE5_5_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLEXE5==.T.,1,;
      0)
  endfunc

  func oFLEXE5_5_1.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oEXEC5_5_3 as StdField with uid="OYMJLGNZGH",rtseq=146,rtrep=.f.,;
    cFormVar = "w_EXEC5", cQueryName = "EXEC5",;
    bObbl = .f. , nPag = 5, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Espressione eseguita dal bottone",;
    HelpContextID = 59742185,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=119, Top=43, InputMask=replicate('X',50)

  add object oFLTYP5_5_4 as StdCheck with uid="QYHUMLHKNK",rtseq=147,rtrep=.f.,left=10, top=74, caption=" ",;
    HelpContextID = 268294377,;
    cFormVar="w_FLTYP5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLTYP5_5_4.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTYP5_5_4.GetRadio()
    this.Parent.oContained.w_FLTYP5 = this.RadioValue()
    return .t.
  endfunc

  func oFLTYP5_5_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTYP5==.T.,1,;
      0)
  endfunc

  func oFLTYP5_5_4.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc


  add object oTYPE_BTN_5_6 as StdCombo with uid="OJDJVWXFYI",rtseq=148,rtrep=.f.,left=119,top=70,width=146,height=22;
    , ToolTipText = "Tipo del bottone";
    , HelpContextID = 230341661;
    , cFormVar="w_TYPE_BTN",RowSource=""+""+MSG_PROGRAM+","+""+MSG_DIALOG_WINDOW+","+""+MSG_ZOOM+","+""+MSG_VFM_DRILLDOWN+","+""+MSG_VFM_DRILLUP+","+""+MSG_VFM_HOME+","+""+MSG_QUERY_BS_REPORT+"", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oTYPE_BTN_5_6.RadioValue()
    return(iif(this.value =1,"PRG",;
    iif(this.value =2,"MSK",;
    iif(this.value =3,"ZOOM",;
    iif(this.value =4,"DOWN",;
    iif(this.value =5,"UP",;
    iif(this.value =6,"HOME",;
    iif(this.value =7,"QRY",;
    space(4)))))))))
  endfunc
  func oTYPE_BTN_5_6.GetRadio()
    this.Parent.oContained.w_TYPE_BTN = this.RadioValue()
    return .t.
  endfunc

  func oTYPE_BTN_5_6.SetRadio()
    this.Parent.oContained.w_TYPE_BTN=trim(this.Parent.oContained.w_TYPE_BTN)
    this.value = ;
      iif(this.Parent.oContained.w_TYPE_BTN=="PRG",1,;
      iif(this.Parent.oContained.w_TYPE_BTN=="MSK",2,;
      iif(this.Parent.oContained.w_TYPE_BTN=="ZOOM",3,;
      iif(this.Parent.oContained.w_TYPE_BTN=="DOWN",4,;
      iif(this.Parent.oContained.w_TYPE_BTN=="UP",5,;
      iif(this.Parent.oContained.w_TYPE_BTN=="HOME",6,;
      iif(this.Parent.oContained.w_TYPE_BTN=="QRY",7,;
      0)))))))
  endfunc

  add object oFLTTL5_5_7 as StdCheck with uid="BRTAQZYAYB",rtseq=149,rtrep=.f.,left=10, top=101, caption=" ",;
    HelpContextID = 195942633,;
    cFormVar="w_FLTTL5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLTTL5_5_7.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTTL5_5_7.GetRadio()
    this.Parent.oContained.w_FLTTL5 = this.RadioValue()
    return .t.
  endfunc

  func oFLTTL5_5_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTTL5==.T.,1,;
      0)
  endfunc

  func oFLTTL5_5_7.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oTITLE5_5_9 as StdField with uid="LVJJCYRLIR",rtseq=150,rtrep=.f.,;
    cFormVar = "w_TITLE5", cQueryName = "TITLE5",;
    bObbl = .f. , nPag = 5, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Titolo del bottone",;
    HelpContextID = 70104809,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=119, Top=97, InputMask=replicate('X',100)

  add object oFLPIC5_5_10 as StdCheck with uid="NMZFCWMTOJ",rtseq=151,rtrep=.f.,left=9, top=128, caption=" ",;
    HelpContextID = 33151209,;
    cFormVar="w_FLPIC5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLPIC5_5_10.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLPIC5_5_10.GetRadio()
    this.Parent.oContained.w_FLPIC5 = this.RadioValue()
    return .t.
  endfunc

  func oFLPIC5_5_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLPIC5==.T.,1,;
      0)
  endfunc

  func oFLPIC5_5_10.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc


  add object oObj_5_11 as cp_askpictfile with uid="RRGRYJIMVW",left=366, top=124, width=23,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_PICTURE5",;
    nPag=5;
    , ToolTipText = "Selezione file Bitmap";
    , HelpContextID = 96083735

  add object oPICTURE5_5_12 as StdField with uid="NQDLAYVEQI",rtseq=152,rtrep=.f.,;
    cFormVar = "w_PICTURE5", cQueryName = "PICTURE5",;
    bObbl = .f. , nPag = 5, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Immagine del bottone",;
    HelpContextID = 77379198,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=119, Top=124, InputMask=replicate('X',150)

  add object oFLFOR5_5_13 as StdCheck with uid="TTXQCWFFUQ",rtseq=153,rtrep=.f.,left=401, top=126, caption=" ",;
    HelpContextID = 246425367,;
    cFormVar="w_FLFOR5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLFOR5_5_13.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFOR5_5_13.GetRadio()
    this.Parent.oContained.w_FLFOR5 = this.RadioValue()
    return .t.
  endfunc

  func oFLFOR5_5_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFOR5==.T.,1,;
      0)
  endfunc

  func oFLFOR5_5_13.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1 or EMPTY(.w_PICTURE5) OR LOWER(JUSTEXT(.w_PICTURE5))<>"ico")
    endwith
  endfunc


  add object oFORMAT5_5_14 as StdCombo with uid="PTKAXJSSQY",rtseq=154,rtrep=.f.,left=421,top=124,width=83,height=22;
    , HelpContextID = 3934494;
    , cFormVar="w_FORMAT5",RowSource=""+"16x16,"+"24x24,"+"32x32,"+"48x48,"+"64x64,"+"96x96,"+"128x128,"+"192x192,"+"256x256", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oFORMAT5_5_14.RadioValue()
    return(iif(this.value =1,16,;
    iif(this.value =2,24,;
    iif(this.value =3,32,;
    iif(this.value =4,48,;
    iif(this.value =5,64,;
    iif(this.value =6,96,;
    iif(this.value =7,128,;
    iif(this.value =8,192,;
    iif(this.value =9,256,;
    0))))))))))
  endfunc
  func oFORMAT5_5_14.GetRadio()
    this.Parent.oContained.w_FORMAT5 = this.RadioValue()
    return .t.
  endfunc

  func oFORMAT5_5_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FORMAT5==16,1,;
      iif(this.Parent.oContained.w_FORMAT5==24,2,;
      iif(this.Parent.oContained.w_FORMAT5==32,3,;
      iif(this.Parent.oContained.w_FORMAT5==48,4,;
      iif(this.Parent.oContained.w_FORMAT5==64,5,;
      iif(this.Parent.oContained.w_FORMAT5==96,6,;
      iif(this.Parent.oContained.w_FORMAT5==128,7,;
      iif(this.Parent.oContained.w_FORMAT5==192,8,;
      iif(this.Parent.oContained.w_FORMAT5==256,9,;
      0)))))))))
  endfunc

  func oFORMAT5_5_14.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PICTURE5) OR LOWER(JUSTEXT(.w_PICTURE5))<>"ico")
    endwith
  endfunc

  add object oFLINV5_5_17 as StdCheck with uid="TXPJWICWAS",rtseq=155,rtrep=.f.,left=9, top=155, caption=" ",;
    HelpContextID = 180168471,;
    cFormVar="w_FLINV5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLINV5_5_17.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLINV5_5_17.GetRadio()
    this.Parent.oContained.w_FLINV5 = this.RadioValue()
    return .t.
  endfunc

  func oFLINV5_5_17.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLINV5==.T.,1,;
      0)
  endfunc

  func oFLINV5_5_17.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc


  add object oINVIS5_5_19 as StdCombo with uid="AVRTOCGSAX",value=2,rtseq=156,rtrep=.f.,left=119,top=151,width=55,height=22;
    , ToolTipText = "Se attivo il bottone non � visibile";
    , HelpContextID = 234882071;
    , cFormVar="w_INVIS5",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oINVIS5_5_19.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,0,;
    0)))
  endfunc
  func oINVIS5_5_19.GetRadio()
    this.Parent.oContained.w_INVIS5 = this.RadioValue()
    return .t.
  endfunc

  func oINVIS5_5_19.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_INVIS5==1,1,;
      iif(this.Parent.oContained.w_INVIS5==0,2,;
      0))
  endfunc

  func oINVIS5_5_19.mHide()
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) or  lower(.w_CLASSOBJ5)<>"button")
    endwith
  endfunc


  add object oDCLICK5_5_21 as StdCombo with uid="OUCUYUZKGR",value=2,rtseq=157,rtrep=.f.,left=167,top=151,width=55,height=22;
    , ToolTipText = "Se attivo il bottone esegue l'espressione al doppio click";
    , HelpContextID = 235583714;
    , cFormVar="w_DCLICK5",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oDCLICK5_5_21.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,0,;
    0)))
  endfunc
  func oDCLICK5_5_21.GetRadio()
    this.Parent.oContained.w_DCLICK5 = this.RadioValue()
    return .t.
  endfunc

  func oDCLICK5_5_21.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DCLICK5==1,1,;
      iif(this.Parent.oContained.w_DCLICK5==0,2,;
      0))
  endfunc

  func oDCLICK5_5_21.mHide()
    with this.Parent.oContained
      return (not empty(nvl(.w_CLASSOBJ5,"")) AND lower(.w_CLASSOBJ5)="button")
    endwith
  endfunc

  add object oFLSHP5_5_22 as StdCheck with uid="JCEBBNRKTZ",rtseq=158,rtrep=.f.,left=9, top=191, caption=" ",;
    HelpContextID = 250403049,;
    cFormVar="w_FLSHP5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLSHP5_5_22.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLSHP5_5_22.GetRadio()
    this.Parent.oContained.w_FLSHP5 = this.RadioValue()
    return .t.
  endfunc

  func oFLSHP5_5_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLSHP5==.T.,1,;
      0)
  endfunc

  func oFLSHP5_5_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oFLSHP5_5_22.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc


  add object oSHAPE5_5_23 as StdCombo with uid="WGPNFIERTC",rtseq=159,rtrep=.f.,left=119,top=187,width=153,height=22;
    , ToolTipText = "Modello del mediabutton";
    , HelpContextID = 73049577;
    , cFormVar="w_SHAPE5",RowSource=""+"Process,"+"Decision,"+"Document,"+"MultipleDocuments,"+"PredefinedProcess,"+"Terminator,"+"ManualInput,"+"Preparation,"+"PaperTape,"+"Database,"+"ManualOperation,"+"Merge,"+"Connector,"+"ArrowUp,"+"ArrowDown,"+"Cloud", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oSHAPE5_5_23.RadioValue()
    return(iif(this.value =1,'Process',;
    iif(this.value =2,'Decision',;
    iif(this.value =3,'Document',;
    iif(this.value =4,'MultipleDocuments',;
    iif(this.value =5,'PredefinedProcess',;
    iif(this.value =6,'Terminator',;
    iif(this.value =7,'ManualInput',;
    iif(this.value =8,'Preparation',;
    iif(this.value =9,'PaperTape',;
    iif(this.value =10,'Database',;
    iif(this.value =11,'ManualOperation',;
    iif(this.value =12,'Merge',;
    iif(this.value =13,'Connector',;
    iif(this.value =14,'ArrowUp',;
    iif(this.value =15,'ArrowDown',;
    iif(this.value =16,'Cloud',;
    space(20))))))))))))))))))
  endfunc
  func oSHAPE5_5_23.GetRadio()
    this.Parent.oContained.w_SHAPE5 = this.RadioValue()
    return .t.
  endfunc

  func oSHAPE5_5_23.SetRadio()
    this.Parent.oContained.w_SHAPE5=trim(this.Parent.oContained.w_SHAPE5)
    this.value = ;
      iif(this.Parent.oContained.w_SHAPE5=='Process',1,;
      iif(this.Parent.oContained.w_SHAPE5=='Decision',2,;
      iif(this.Parent.oContained.w_SHAPE5=='Document',3,;
      iif(this.Parent.oContained.w_SHAPE5=='MultipleDocuments',4,;
      iif(this.Parent.oContained.w_SHAPE5=='PredefinedProcess',5,;
      iif(this.Parent.oContained.w_SHAPE5=='Terminator',6,;
      iif(this.Parent.oContained.w_SHAPE5=='ManualInput',7,;
      iif(this.Parent.oContained.w_SHAPE5=='Preparation',8,;
      iif(this.Parent.oContained.w_SHAPE5=='PaperTape',9,;
      iif(this.Parent.oContained.w_SHAPE5=='Database',10,;
      iif(this.Parent.oContained.w_SHAPE5=='ManualOperation',11,;
      iif(this.Parent.oContained.w_SHAPE5=='Merge',12,;
      iif(this.Parent.oContained.w_SHAPE5=='Connector',13,;
      iif(this.Parent.oContained.w_SHAPE5=='ArrowUp',14,;
      iif(this.Parent.oContained.w_SHAPE5=='ArrowDown',15,;
      iif(this.Parent.oContained.w_SHAPE5=='Cloud',16,;
      0))))))))))))))))
  endfunc

  func oSHAPE5_5_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  add object oFLEDI5_5_25 as StdCheck with uid="NGGQYKROET",rtseq=160,rtrep=.f.,left=9, top=218, caption=" ",;
    HelpContextID = 127850729,;
    cFormVar="w_FLEDI5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLEDI5_5_25.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLEDI5_5_25.GetRadio()
    this.Parent.oContained.w_FLEDI5 = this.RadioValue()
    return .t.
  endfunc

  func oFLEDI5_5_25.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLEDI5==.T.,1,;
      0)
  endfunc

  func oFLEDI5_5_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oFLEDI5_5_25.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oEDITABLE5_5_26 as StdField with uid="AQTYDRGTFF",rtseq=161,rtrep=.f.,;
    cFormVar = "w_EDITABLE5", cQueryName = "EDITABLE5",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Condizione di editabilit�",;
    HelpContextID = 10654085,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=119, Top=214, InputMask=replicate('X',30)

  func oEDITABLE5_5_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  add object oFLUNS5_5_27 as StdCheck with uid="XZSKVFEVBL",rtseq=162,rtrep=.f.,left=9, top=245, caption=" ",;
    HelpContextID = 229713687,;
    cFormVar="w_FLUNS5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLUNS5_5_27.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLUNS5_5_27.GetRadio()
    this.Parent.oContained.w_FLUNS5 = this.RadioValue()
    return .t.
  endfunc

  func oFLUNS5_5_27.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLUNS5==.T.,1,;
      0)
  endfunc

  func oFLUNS5_5_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oFLUNS5_5_27.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oUNSEL5_5_28 as StdSpinner with uid="SBHVKDFQGL",rtseq=163,rtrep=.f.,;
    cFormVar = "w_UNSEL5", cQueryName = "UNSEL5",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile inserire valori negativi",;
    HelpContextID = 180160489,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=167, Top=241, cSayPict='"999"', cGetPict='"999"'

  func oUNSEL5_5_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oUNSEL5_5_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_UNSEL5>=0)
    endwith
    return bRes
  endfunc

  add object oFLPAS5_5_29 as StdCheck with uid="DOMAYHTQQZ",rtseq=164,rtrep=.f.,left=9, top=272, caption=" ",;
    HelpContextID = 243672855,;
    cFormVar="w_FLPAS5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLPAS5_5_29.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLPAS5_5_29.GetRadio()
    this.Parent.oContained.w_FLPAS5 = this.RadioValue()
    return .t.
  endfunc

  func oFLPAS5_5_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLPAS5==.T.,1,;
      0)
  endfunc

  func oFLPAS5_5_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oFLPAS5_5_29.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oPASMOU5_5_30 as StdSpinner with uid="JGIWJKLCOJ",rtseq=165,rtrep=.f.,;
    cFormVar = "w_PASMOU5", cQueryName = "PASMOU5",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile inserire valori negativi",;
    HelpContextID = 238826270,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=167, Top=268, cSayPict='"999"', cGetPict='"999"'

  func oPASMOU5_5_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oPASMOU5_5_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PASMOU5>=0)
    endwith
    return bRes
  endfunc

  add object oFLPRS5_5_31 as StdCheck with uid="XLOLIQOZST",rtseq=166,rtrep=.f.,left=9, top=299, caption=" ",;
    HelpContextID = 225847063,;
    cFormVar="w_FLPRS5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLPRS5_5_31.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLPRS5_5_31.GetRadio()
    this.Parent.oContained.w_FLPRS5 = this.RadioValue()
    return .t.
  endfunc

  func oFLPRS5_5_31.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLPRS5==.T.,1,;
      0)
  endfunc

  func oFLPRS5_5_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oFLPRS5_5_31.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oPRESSMOU5_5_32 as StdSpinner with uid="NCBCYMNBZE",rtseq=167,rtrep=.f.,;
    cFormVar = "w_PRESSMOU5", cQueryName = "PRESSMOU5",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile inserire valori negativi",;
    HelpContextID = 42958216,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=167, Top=295, cSayPict='"999"', cGetPict='"999"'

  func oPRESSMOU5_5_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oPRESSMOU5_5_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRESSMOU5>=0)
    endwith
    return bRes
  endfunc

  add object oFLLBR5_5_33 as StdCheck with uid="JEZCQHTASJ",rtseq=168,rtrep=.f.,left=249, top=245, caption=" ",;
    HelpContextID = 259663639,;
    cFormVar="w_FLLBR5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLLBR5_5_33.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLBR5_5_33.GetRadio()
    this.Parent.oContained.w_FLLBR5 = this.RadioValue()
    return .t.
  endfunc

  func oFLLBR5_5_33.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLBR5==.T.,1,;
      0)
  endfunc

  func oFLLBR5_5_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oFLLBR5_5_33.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oBRDWID5_5_34 as StdSpinner with uid="BVCBZUIRVP",rtseq=169,rtrep=.f.,;
    cFormVar = "w_BRDWID5", cQueryName = "BRDWID5",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile inserire valori negativi",;
    HelpContextID = 147731742,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=366, Top=241, cSayPict='"999"', cGetPict='"999"'

  func oBRDWID5_5_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oBRDWID5_5_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_BRDWID5>=0)
    endwith
    return bRes
  endfunc

  add object oFLCRV5_5_35 as StdCheck with uid="VPFULYUSQX",rtseq=170,rtrep=.f.,left=249, top=272, caption=" ",;
    HelpContextID = 176367383,;
    cFormVar="w_FLCRV5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLCRV5_5_35.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLCRV5_5_35.GetRadio()
    this.Parent.oContained.w_FLCRV5 = this.RadioValue()
    return .t.
  endfunc

  func oFLCRV5_5_35.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLCRV5==.T.,1,;
      0)
  endfunc

  func oFLCRV5_5_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oFLCRV5_5_35.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oRADIUS5_5_36 as StdSpinner with uid="WXFVXSVOUF",rtseq=171,rtrep=.f.,;
    cFormVar = "w_RADIUS5", cQueryName = "RADIUS5",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile inserire valori negativi",;
    HelpContextID = 65877278,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=366, Top=268, cSayPict='"999"', cGetPict='"999"'

  func oRADIUS5_5_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oRADIUS5_5_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RADIUS5>=0)
    endwith
    return bRes
  endfunc

  add object oFLCBR5_5_37 as StdCheck with uid="GWAYELEQAN",rtseq=172,rtrep=.f.,left=249, top=299, caption=" ",;
    HelpContextID = 260253463,;
    cFormVar="w_FLCBR5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLCBR5_5_37.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLCBR5_5_37.GetRadio()
    this.Parent.oContained.w_FLCBR5 = this.RadioValue()
    return .t.
  endfunc

  func oFLCBR5_5_37.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLCBR5==.T.,1,;
      0)
  endfunc

  func oFLCBR5_5_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oFLCBR5_5_37.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc


  add object oBtn_5_38 as StdButton with uid="PTFDAFTJPO",left=391, top=295, width=23,height=22,;
    caption="...", nPag=5;
    , ToolTipText = "Modifica colore bordo";
    , HelpContextID = 96083735;
  , bGlobalFont=.t.

    proc oBtn_5_38.Click()
      this.parent.oContained.NotifyEvent("ChangeColor1")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLCSF5_5_39 as StdCheck with uid="BXPPQWQWMR",rtseq=173,rtrep=.f.,left=249, top=326, caption=" ",;
    HelpContextID = 93116649,;
    cFormVar="w_FLCSF5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLCSF5_5_39.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLCSF5_5_39.GetRadio()
    this.Parent.oContained.w_FLCSF5 = this.RadioValue()
    return .t.
  endfunc

  func oFLCSF5_5_39.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLCSF5==.T.,1,;
      0)
  endfunc

  func oFLCSF5_5_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) OR lower(.w_CLASSOBJ5)<>"button")
    endwith
   endif
  endfunc

  func oFLCSF5_5_39.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc


  add object oBtn_5_40 as StdButton with uid="LJEBJPNWPV",left=391, top=322, width=23,height=22,;
    caption="...", nPag=5;
    , ToolTipText = "Modifica colore sfondo";
    , HelpContextID = 96083735;
  , bGlobalFont=.t.

    proc oBtn_5_40.Click()
      this.parent.oContained.NotifyEvent("ChangeColor2")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLFNT5_5_41 as StdCheck with uid="KNLQDNLBVX",rtseq=174,rtrep=.f.,left=9, top=360, caption=" ",;
    HelpContextID = 213919511,;
    cFormVar="w_FLFNT5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLFNT5_5_41.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFNT5_5_41.GetRadio()
    this.Parent.oContained.w_FLFNT5 = this.RadioValue()
    return .t.
  endfunc

  func oFLFNT5_5_41.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFNT5==.T.,1,;
      0)
  endfunc

  func oFLFNT5_5_41.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oFNTNAME5_5_42 as StdField with uid="REHBCCYQCO",rtseq=175,rtrep=.f.,;
    cFormVar = "w_FNTNAME5", cQueryName = "FNTNAME5",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(180), bMultilanguage =  .f.,;
    HelpContextID = 263324546,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=119, Top=356, InputMask=replicate('X',180)

  add object oFNTSIZE5_5_43 as StdField with uid="RYRBZLRNVR",rtseq=176,rtrep=.f.,;
    cFormVar = "w_FNTSIZE5", cQueryName = "FNTSIZE5",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 123863938,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=309, Top=356, cSayPict='"99"', cGetPict='"99"'


  add object oBtn_5_44 as StdButton with uid="WWGMZWZGLB",left=205, top=383, width=95,height=22,;
    caption="Change font", nPag=5;
    , ToolTipText = "Selezione font";
    , HelpContextID = 120067223;
  , bGlobalFont=.t.

    proc oBtn_5_44.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Font",5)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_44.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT .w_GLBFONT5)
      endwith
    endif
  endfunc

  add object oFLGLF5_5_47 as StdCheck with uid="MAPUYHCAVB",rtseq=177,rtrep=.f.,left=9, top=387, caption=" ",;
    HelpContextID = 86038761,;
    cFormVar="w_FLGLF5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLGLF5_5_47.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLGLF5_5_47.GetRadio()
    this.Parent.oContained.w_FLGLF5 = this.RadioValue()
    return .t.
  endfunc

  func oFLGLF5_5_47.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLGLF5==.T.,1,;
      0)
  endfunc

  func oFLGLF5_5_47.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc


  add object oGLBFONT5_5_48 as StdCombo with uid="HEMZMWLLON",rtseq=178,rtrep=.f.,left=119,top=383,width=55,height=22;
    , ToolTipText = "Se attivo utilizza il font globale";
    , HelpContextID = 230415757;
    , cFormVar="w_GLBFONT5",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oGLBFONT5_5_48.RadioValue()
    return(iif(this.value =1,.T.,;
    iif(this.value =2,.F.,;
    .f.)))
  endfunc
  func oGLBFONT5_5_48.GetRadio()
    this.Parent.oContained.w_GLBFONT5 = this.RadioValue()
    return .t.
  endfunc

  func oGLBFONT5_5_48.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GLBFONT5==.T.,1,;
      iif(this.Parent.oContained.w_GLBFONT5==.F.,2,;
      0))
  endfunc

  add object oFLTOP5_5_52 as StdCheck with uid="GOUHHNXQRH",rtseq=179,rtrep=.f.,left=9, top=445, caption=" ",;
    HelpContextID = 257808617,;
    cFormVar="w_FLTOP5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLTOP5_5_52.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTOP5_5_52.GetRadio()
    this.Parent.oContained.w_FLTOP5 = this.RadioValue()
    return .t.
  endfunc

  func oFLTOP5_5_52.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTOP5==.T.,1,;
      0)
  endfunc

  func oFLTOP5_5_52.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oTOP5_5_53 as StdField with uid="DVMDLUMZNK",rtseq=180,rtrep=.f.,;
    cFormVar = "w_TOP5", cQueryName = "TOP5",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione del bottone nella maschera (in alto)",;
    HelpContextID = 38136087,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=95, Top=442, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLLEF5_5_54 as StdCheck with uid="RRVQRBPGKM",rtseq=181,rtrep=.f.,left=9, top=476, caption=" ",;
    HelpContextID = 79026409,;
    cFormVar="w_FLLEF5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLLEF5_5_54.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLEF5_5_54.GetRadio()
    this.Parent.oContained.w_FLLEF5 = this.RadioValue()
    return .t.
  endfunc

  func oFLLEF5_5_54.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLEF5==.T.,1,;
      0)
  endfunc

  func oFLLEF5_5_54.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oLEFT5_5_55 as StdField with uid="OVGJEVNXXD",rtseq=182,rtrep=.f.,;
    cFormVar = "w_LEFT5", cQueryName = "LEFT5",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione del bottone nella maschera (a sinistra)",;
    HelpContextID = 77557481,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=95, Top=473, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLHGT5_5_56 as StdCheck with uid="NFGHKDQUPN",rtseq=183,rtrep=.f.,left=219, top=445, caption=" ",;
    HelpContextID = 221128471,;
    cFormVar="w_FLHGT5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLHGT5_5_56.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLHGT5_5_56.GetRadio()
    this.Parent.oContained.w_FLHGT5 = this.RadioValue()
    return .t.
  endfunc

  func oFLHGT5_5_56.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLHGT5==.T.,1,;
      0)
  endfunc

  func oFLHGT5_5_56.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oHEIGHT5_5_57 as StdField with uid="SAYCUKGJRU",rtseq=184,rtrep=.f.,;
    cFormVar = "w_HEIGHT5", cQueryName = "HEIGHT5",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica l'altezza del bottone",;
    HelpContextID = 114453278,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=302, Top=441, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLWDT5_5_58 as StdCheck with uid="IXZHYKAIUC",rtseq=185,rtrep=.f.,left=219, top=476, caption=" ",;
    HelpContextID = 223291159,;
    cFormVar="w_FLWDT5", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLWDT5_5_58.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLWDT5_5_58.GetRadio()
    this.Parent.oContained.w_FLWDT5 = this.RadioValue()
    return .t.
  endfunc

  func oFLWDT5_5_58.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLWDT5==.T.,1,;
      0)
  endfunc

  func oFLWDT5_5_58.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc

  add object oWIDTH5_5_59 as StdField with uid="OYDOZAEQWQ",rtseq=186,rtrep=.f.,;
    cFormVar = "w_WIDTH5", cQueryName = "WIDTH5",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la larghezza del bottone",;
    HelpContextID = 127777257,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=302, Top=472, cSayPict='"9999"', cGetPict='"9999"'

  add object oSELECT5_5_60 as StdRadio with uid="GOSFURDJWM",rtseq=214,rtrep=.f.,left=357, top=8, width=198,height=35;
    , cFormVar="w_SELECT5", ButtonCount=2, bObbl=.f., nPag=5;
  , bGlobalFont=.t.

    proc oSELECT5_5_60.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption=""+MSG_SELECT_ALL_TEXT+""
      this.Buttons(1).HelpContextID = 28669470
      this.Buttons(1).Top=0
      this.Buttons(2).Caption=""+MSG_DESELECT_ALL+""
      this.Buttons(2).HelpContextID = 28669470
      this.Buttons(2).Top=16
      this.SetAll("Width",196)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECT5_5_60.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    "N")))
  endfunc
  func oSELECT5_5_60.GetRadio()
    this.Parent.oContained.w_SELECT5 = this.RadioValue()
    return .t.
  endfunc

  func oSELECT5_5_60.SetRadio()
    this.Parent.oContained.w_SELECT5=trim(this.Parent.oContained.w_SELECT5)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT5=="S",1,;
      iif(this.Parent.oContained.w_SELECT5=="N",2,;
      0))
  endfunc

  func oSELECT5_5_60.mHide()
    with this.Parent.oContained
      return (.w_HIDE5=1)
    endwith
  endfunc


  add object oBtn_5_66 as StdButton with uid="PPFBXLCFEX",left=563, top=462, width=48,height=45,;
    CpPicture="BMP\applica.ico", caption="", nPag=5;
    , ToolTipText = "Premere per applicare le modifiche";
    , HelpContextID = 172758090;
    , caption="\<Applica";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_66.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Save",5)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_5_67 as StdButton with uid="JMDIXUVWCY",left=613, top=462, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 98971159;
    , caption="\<Ok";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_67.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_5_68 as StdButton with uid="BSPHAHXUNR",left=663, top=462, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per annullare le operazioni svolte";
    , HelpContextID = 250656791;
    , caption="\<Esci";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_68.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object zBtn as cp_szoombox with uid="EXZRHYSXRJ",left=503, top=7, width=208,height=447,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="objmaskoptions",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomFile="default",bOptions=.f.,;
    cEvent = "Pagina5",;
    nPag=5;
    , HelpContextID = 204127767

  add object oBRDCLR5_5_76 as StdField with uid="WWZIPHXQQD",rtseq=216,rtrep=.f.,;
    cFormVar = "w_BRDCLR5", cQueryName = "BRDCLR5",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 177091870,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=366, Top=295, cSayPict='"999"', cGetPict='"999"', bNoDisabledColor = .T.

  add object oBCKCLR5_5_77 as StdField with uid="RSFKSWVKWF",rtseq=217,rtrep=.f.,;
    cFormVar = "w_BCKCLR5", cQueryName = "BCKCLR5",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 177489182,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=366, Top=322, cSayPict='"999"', cGetPict='"999"', bNoDisabledColor = .T.

  add object oFNTCOLOR5_5_80 as StdField with uid="WYZCLZVGYY",rtseq=247,rtrep=.f.,;
    cFormVar = "w_FNTCOLOR5", cQueryName = "FNTCOLOR5",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 39963816,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=309, Top=383


  add object btn as cp_calclbl with uid="RYTQBKNDCN",left=10, top=8, width=262,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,;
    nPag=5;
    , HelpContextID = 204127767


  add object oBtn_5_93 as StdButton with uid="RLPPINVZQQ",left=513, top=462, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=5;
    , ToolTipText = "Salva propriet� in file di configurazione";
    , HelpContextID = 48707049;
    , caption="\<Salva";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_93.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"MaskConf",5)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_93.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not EMPTY(.w_CNFFILE))
      endwith
    endif
  endfunc

  func oBtn_5_93.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT empty(nvl(.w_CLASSOBJ5,"")) AND lower(.w_CLASSOBJ5)="button")
     endwith
    endif
  endfunc


  add object oBtn_5_95 as StdButton with uid="ENHCSHAUCQ",left=345, top=383, width=23,height=22,;
    caption="...", nPag=5;
    , ToolTipText = "Modifica colore font";
    , HelpContextID = 96083735;
  , bGlobalFont=.t.

    proc oBtn_5_95.Click()
      this.parent.oContained.NotifyEvent("FontColor5")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_5_2 as StdString with uid="AYWJILVUZV",Visible=.t., Left=34, Top=47,;
    Alignment=0, Width=48, Height=18,;
    Caption="Esegue:"  ;
  , bGlobalFont=.t.

  add object oStr_5_5 as StdString with uid="NSNLIMSYWS",Visible=.t., Left=34, Top=74,;
    Alignment=0, Width=41, Height=18,;
    Caption="Tipo:"    , caption=MSG_TYPE+MSG_FS;
  ;
  , bGlobalFont=.t.

  add object oStr_5_8 as StdString with uid="XNPARPLVXH",Visible=.t., Left=34, Top=101,;
    Alignment=0, Width=35, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_5_15 as StdString with uid="WZNFOWYFWP",Visible=.t., Left=421, Top=107,;
    Alignment=0, Width=55, Height=18,;
    Caption="Formato:"  ;
  , bGlobalFont=.t.

  func oStr_5_15.mHide()
    with this.Parent.oContained
      return (LOWER(JUSTEXT(nvl(.w_PICTURE5,"")))<>"ico")
    endwith
  endfunc

  add object oStr_5_16 as StdString with uid="EGCJEBSXVL",Visible=.t., Left=34, Top=128,;
    Alignment=0, Width=43, Height=18,;
    Caption="Bitmap:"  ;
  , bGlobalFont=.t.

  add object oStr_5_18 as StdString with uid="YYBOXULHFP",Visible=.t., Left=34, Top=155,;
    Alignment=0, Width=51, Height=18,;
    Caption="Invisibile:"    , caption=MSG_INVISIBLE+MSG_FS;
  ;
  , bGlobalFont=.t.

  func oStr_5_18.mHide()
    with this.Parent.oContained
      return (empty(nvl(.w_CLASSOBJ5,"")) or  lower(.w_CLASSOBJ5)<>"button")
    endwith
  endfunc

  add object oStr_5_20 as StdString with uid="NEUHHGCSND",Visible=.t., Left=34, Top=155,;
    Alignment=0, Width=123, Height=18,;
    Caption="Esegui al doppio click:"  ;
  , bGlobalFont=.t.

  func oStr_5_20.mHide()
    with this.Parent.oContained
      return (not empty(nvl(.w_CLASSOBJ5,"")) AND lower(.w_CLASSOBJ5)="button")
    endwith
  endfunc

  add object oStr_5_24 as StdString with uid="ZEZMQOERBK",Visible=.t., Left=34, Top=191,;
    Alignment=0, Width=48, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_5_45 as StdString with uid="OKXJVDQYOD",Visible=.t., Left=34, Top=218,;
    Alignment=0, Width=72, Height=18,;
    Caption="Editabile:"  ;
  , bGlobalFont=.t.

  add object oStr_5_46 as StdString with uid="ISUFHQGPLN",Visible=.t., Left=34, Top=360,;
    Alignment=0, Width=73, Height=18,;
    Caption="Font Options:"  ;
  , bGlobalFont=.t.

  add object oStr_5_49 as StdString with uid="IOLGBVJYNW",Visible=.t., Left=34, Top=387,;
    Alignment=0, Width=62, Height=18,;
    Caption="Global font:"  ;
  , bGlobalFont=.t.

  add object oStr_5_50 as StdString with uid="SWUCWZALEQ",Visible=.t., Left=47, Top=412,;
    Alignment=0, Width=173, Height=18,;
    Caption="Posizione e Dimensione"  ;
  , bGlobalFont=.t.

  add object oStr_5_61 as StdString with uid="DTKSVYHCGH",Visible=.t., Left=34, Top=446,;
    Alignment=0, Width=58, Height=18,;
    Caption="Top:"  ;
  , bGlobalFont=.t.

  add object oStr_5_62 as StdString with uid="PPYAXEPTHQ",Visible=.t., Left=34, Top=477,;
    Alignment=0, Width=63, Height=18,;
    Caption="Left:"  ;
  , bGlobalFont=.t.

  add object oStr_5_63 as StdString with uid="AKWZSOACBL",Visible=.t., Left=242, Top=445,;
    Alignment=0, Width=82, Height=18,;
    Caption="Height:"  ;
  , bGlobalFont=.t.

  add object oStr_5_64 as StdString with uid="BWUOFGIZOK",Visible=.t., Left=243, Top=476,;
    Alignment=0, Width=64, Height=18,;
    Caption="Width:"  ;
  , bGlobalFont=.t.

  add object oStr_5_70 as StdString with uid="WNDOYDZLGY",Visible=.t., Left=34, Top=245,;
    Alignment=0, Width=92, Height=18,;
    Caption="Non selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_5_71 as StdString with uid="WNLTHDRYZB",Visible=.t., Left=34, Top=272,;
    Alignment=0, Width=125, Height=18,;
    Caption="Passaggio del mouse:"  ;
  , bGlobalFont=.t.

  add object oStr_5_72 as StdString with uid="WISIYRXZMO",Visible=.t., Left=34, Top=299,;
    Alignment=0, Width=122, Height=18,;
    Caption="Pressione del mouse:"  ;
  , bGlobalFont=.t.

  add object oStr_5_73 as StdString with uid="KEDQGDATTY",Visible=.t., Left=269, Top=245,;
    Alignment=0, Width=94, Height=18,;
    Caption="Larghezza bordo:"  ;
  , bGlobalFont=.t.

  add object oStr_5_74 as StdString with uid="MPUFCKJXAN",Visible=.t., Left=269, Top=272,;
    Alignment=0, Width=59, Height=18,;
    Caption="Curvatura:"  ;
  , bGlobalFont=.t.

  add object oStr_5_75 as StdString with uid="QBTSUHOYEL",Visible=.t., Left=269, Top=299,;
    Alignment=0, Width=75, Height=18,;
    Caption="Colore bordo:"  ;
  , bGlobalFont=.t.

  add object oStr_5_78 as StdString with uid="DHDYLJSWLJ",Visible=.t., Left=269, Top=326,;
    Alignment=0, Width=94, Height=18,;
    Caption="Colore di sfondo:"  ;
  , bGlobalFont=.t.

  add object oBox_5_51 as StdBox with uid="KZYPLZUQDE",left=33, top=434, width=381,height=1

  add object oBox_5_90 as StdBox with uid="KQRTTUBUEJ",left=10, top=33, width=286,height=2

  add object oBox_5_94 as StdBox with uid="NBUXROZMUD",left=10, top=179, width=389,height=2
enddefine
define class tcp_objmaskoptionsPag6 as StdContainer
  Width  = 713
  height = 512
  stdWidth  = 713
  stdheight = 512
  resizeXpos=530
  resizeYpos=434
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLCLR6_6_1 as StdCheck with uid="NALHERXTFA",rtseq=144,rtrep=.f.,left=10, top=270, caption=" ",;
    HelpContextID = 249767703,;
    cFormVar="w_FLCLR6", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oFLCLR6_6_1.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLCLR6_6_1.GetRadio()
    this.Parent.oContained.w_FLCLR6 = this.RadioValue()
    return .t.
  endfunc

  func oFLCLR6_6_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLCLR6==.T.,1,;
      0)
  endfunc

  func oFLCLR6_6_1.mHide()
    with this.Parent.oContained
      return (.w_HIDE6=1)
    endwith
  endfunc


  add object oBtn_6_2 as StdButton with uid="HCQWMIWQJI",left=172, top=270, width=23,height=22,;
    caption="...", nPag=6;
    , ToolTipText = "Modifica colore sfondo";
    , HelpContextID = 96083735;
  , bGlobalFont=.t.

    proc oBtn_6_2.Click()
      this.parent.oContained.NotifyEvent("ChangeColor3")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOLOR6_6_3 as StdField with uid="VQSGAVJBYN",rtseq=187,rtrep=.f.,;
    cFormVar = "w_COLOR6", cQueryName = "COLOR6",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 246020631,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=146, Top=270, bNoDisabledColor = .T.

  add object oFLBRW6_6_7 as StdCheck with uid="RHGMIGRVAS",rtseq=188,rtrep=.f.,left=10, top=243, caption=" ",;
    HelpContextID = 159655703,;
    cFormVar="w_FLBRW6", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oFLBRW6_6_7.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLBRW6_6_7.GetRadio()
    this.Parent.oContained.w_FLBRW6 = this.RadioValue()
    return .t.
  endfunc

  func oFLBRW6_6_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLBRW6==.T.,1,;
      0)
  endfunc

  func oFLBRW6_6_7.mHide()
    with this.Parent.oContained
      return (.w_HIDE6=1)
    endwith
  endfunc

  add object oBRDWID6_6_8 as StdSpinner with uid="ODOUZGIBCX",rtseq=189,rtrep=.f.,;
    cFormVar = "w_BRDWID6", cQueryName = "BRDWID6",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile inserire valori negativi",;
    HelpContextID = 147731743,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=146, Top=242

  func oBRDWID6_6_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_BRDWID6>=0)
    endwith
    return bRes
  endfunc

  add object oFLINI6_6_9 as StdCheck with uid="VDVKBFUNIW",rtseq=190,rtrep=.f.,left=10, top=69, caption=" ",;
    HelpContextID = 129836823,;
    cFormVar="w_FLINI6", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oFLINI6_6_9.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLINI6_6_9.GetRadio()
    this.Parent.oContained.w_FLINI6 = this.RadioValue()
    return .t.
  endfunc

  func oFLINI6_6_9.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLINI6==.T.,1,;
      0)
  endfunc

  func oFLINI6_6_9.mHide()
    with this.Parent.oContained
      return (.w_HIDE6=1)
    endwith
  endfunc


  add object oINILINE6_6_10 as StdCombo with uid="CGICCQLTSQ",value=4,rtseq=191,rtrep=.f.,left=146,top=65,width=129,height=22;
    , HelpContextID = 136511374;
    , cFormVar="w_INILINE6",RowSource=""+""+MSG_ARROW+","+""+MSG_HOLLOWARROW+","+""+MSG_OPENARROW+","+""+MSG_NONEARROW+"", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oINILINE6_6_10.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,0,;
    0)))))
  endfunc
  func oINILINE6_6_10.GetRadio()
    this.Parent.oContained.w_INILINE6 = this.RadioValue()
    return .t.
  endfunc

  func oINILINE6_6_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_INILINE6==1,1,;
      iif(this.Parent.oContained.w_INILINE6==2,2,;
      iif(this.Parent.oContained.w_INILINE6==3,3,;
      iif(this.Parent.oContained.w_INILINE6==0,4,;
      0))))
  endfunc

  add object oFLFIN6_6_12 as StdCheck with uid="XDWRPIKHZQ",rtseq=192,rtrep=.f.,left=10, top=141, caption=" ",;
    HelpContextID = 51390231,;
    cFormVar="w_FLFIN6", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oFLFIN6_6_12.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFIN6_6_12.GetRadio()
    this.Parent.oContained.w_FLFIN6 = this.RadioValue()
    return .t.
  endfunc

  func oFLFIN6_6_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFIN6==.T.,1,;
      0)
  endfunc

  func oFLFIN6_6_12.mHide()
    with this.Parent.oContained
      return (.w_HIDE6=1)
    endwith
  endfunc


  add object oFINLINE6_6_13 as StdCombo with uid="XJZSWHCYZI",value=4,rtseq=193,rtrep=.f.,left=146,top=137,width=129,height=22;
    , HelpContextID = 136817806;
    , cFormVar="w_FINLINE6",RowSource=""+""+MSG_ARROW+","+""+MSG_HOLLOWARROW+","+""+MSG_OPENARROW+","+""+MSG_NONEARROW+"", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oFINLINE6_6_13.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,0,;
    0)))))
  endfunc
  func oFINLINE6_6_13.GetRadio()
    this.Parent.oContained.w_FINLINE6 = this.RadioValue()
    return .t.
  endfunc

  func oFINLINE6_6_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FINLINE6==1,1,;
      iif(this.Parent.oContained.w_FINLINE6==2,2,;
      iif(this.Parent.oContained.w_FINLINE6==3,3,;
      iif(this.Parent.oContained.w_FINLINE6==0,4,;
      0))))
  endfunc

  add object oFLSTY6_6_16 as StdCheck with uid="XMYOPQDDML",rtseq=194,rtrep=.f.,left=10, top=189, caption=" ",;
    HelpContextID = 122890007,;
    cFormVar="w_FLSTY6", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oFLSTY6_6_16.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLSTY6_6_16.GetRadio()
    this.Parent.oContained.w_FLSTY6 = this.RadioValue()
    return .t.
  endfunc

  func oFLSTY6_6_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLSTY6==.T.,1,;
      0)
  endfunc

  func oFLSTY6_6_16.mHide()
    with this.Parent.oContained
      return (.w_HIDE6=1)
    endwith
  endfunc


  add object oSTYLINE6_6_17 as StdCombo with uid="ASWGHSTGDI",value=3,rtseq=195,rtrep=.f.,left=146,top=186,width=129,height=22;
    , HelpContextID = 137587086;
    , cFormVar="w_STYLINE6",RowSource=""+""+MSG_DASHED+","+""+MSG_DOTTED+","+""+MSG_SOLID+"", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oSTYLINE6_6_17.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,0,;
    0))))
  endfunc
  func oSTYLINE6_6_17.GetRadio()
    this.Parent.oContained.w_STYLINE6 = this.RadioValue()
    return .t.
  endfunc

  func oSTYLINE6_6_17.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_STYLINE6==1,1,;
      iif(this.Parent.oContained.w_STYLINE6==2,2,;
      iif(this.Parent.oContained.w_STYLINE6==0,3,;
      0)))
  endfunc

  add object oFLCUR6_6_18 as StdCheck with uid="DYVWYVGRLR",rtseq=196,rtrep=.f.,left=10, top=216, caption=" ",;
    HelpContextID = 240330519,;
    cFormVar="w_FLCUR6", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oFLCUR6_6_18.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLCUR6_6_18.GetRadio()
    this.Parent.oContained.w_FLCUR6 = this.RadioValue()
    return .t.
  endfunc

  func oFLCUR6_6_18.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLCUR6==.T.,1,;
      0)
  endfunc

  func oFLCUR6_6_18.mHide()
    with this.Parent.oContained
      return (.w_HIDE6=1)
    endwith
  endfunc

  add object oCURVE6_6_19 as StdSpinner with uid="WZVMQOMHOH",rtseq=197,rtrep=.f.,;
    cFormVar = "w_CURVE6", cQueryName = "CURVE6",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile inserire valori negativi",;
    HelpContextID = 187931159,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=146, Top=214

  func oCURVE6_6_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CURVE6>=0)
    endwith
    return bRes
  endfunc

  add object oSELECT6_6_20 as StdRadio with uid="KCFARHGZOJ",rtseq=218,rtrep=.f.,left=357, top=8, width=195,height=35;
    , cFormVar="w_SELECT6", ButtonCount=2, bObbl=.f., nPag=6;
  , bGlobalFont=.t.

    proc oSELECT6_6_20.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption=""+MSG_SELECT_ALL_TEXT+""
      this.Buttons(1).HelpContextID = 28669471
      this.Buttons(1).Top=0
      this.Buttons(2).Caption=""+MSG_DESELECT_ALL+""
      this.Buttons(2).HelpContextID = 28669471
      this.Buttons(2).Top=16
      this.SetAll("Width",193)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECT6_6_20.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    "N")))
  endfunc
  func oSELECT6_6_20.GetRadio()
    this.Parent.oContained.w_SELECT6 = this.RadioValue()
    return .t.
  endfunc

  func oSELECT6_6_20.SetRadio()
    this.Parent.oContained.w_SELECT6=trim(this.Parent.oContained.w_SELECT6)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT6=="S",1,;
      iif(this.Parent.oContained.w_SELECT6=="N",2,;
      0))
  endfunc

  func oSELECT6_6_20.mHide()
    with this.Parent.oContained
      return (.w_HIDE6=1)
    endwith
  endfunc


  add object oBtn_6_22 as StdButton with uid="GQIGNTUCOM",left=563, top=462, width=48,height=45,;
    CpPicture="BMP\applica.ico", caption="", nPag=6;
    , ToolTipText = "Premere per applicare le modifiche";
    , HelpContextID = 172758090;
    , caption="\<Applica";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_22.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Save",6)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_23 as StdButton with uid="SYAAVLOMIT",left=613, top=462, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 98971159;
    , caption="\<Ok";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_23.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_24 as StdButton with uid="YSTXDFQIAF",left=663, top=462, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per annullare le operazioni svolte";
    , HelpContextID = 250656791;
    , caption="\<Esci";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object zLine as cp_szoombox with uid="TNRUMXFCPH",left=503, top=7, width=208,height=447,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="objmaskoptions",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomFile="default",bOptions=.f.,;
    cEvent = "Pagina6",;
    nPag=6;
    , HelpContextID = 204127767


  add object line as cp_calclbl with uid="XLYSHQOQOZ",left=10, top=8, width=262,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,;
    nPag=6;
    , HelpContextID = 204127767


  add object oBtn_6_33 as StdButton with uid="CTYMAZPEIL",left=513, top=462, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=6;
    , ToolTipText = "Salva propriet� in file di configurazione";
    , HelpContextID = 48707049;
    , caption="\<Salva";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_33.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"MaskConf",6)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not EMPTY(.w_CNFFILE))
      endwith
    endif
  endfunc

  add object oINIANGLE6_6_34 as StdField with uid="ILOEWKFSHR",rtseq=268,rtrep=.f.,;
    cFormVar = "w_INIANGLE6", cQueryName = "INIANGLE6",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Angolo della freccia di inizio linea",;
    HelpContextID = 59558267,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=312, Top=45, cSayPict='"999"', cGetPict='"999"', bHasZoom = .t. 

  func oINIANGLE6_6_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INILINE6<>0)
    endwith
   endif
  endfunc

  func oINIANGLE6_6_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_INIANGLE6>0 AND .w_INIANGLE6<=180)
    endwith
    return bRes
  endfunc

  proc oINIANGLE6_6_34.mZoom
    this.parent.oContained.w_INIANGLE6 = iif(this.parent.oContained.w_INILINE6<>3,36,60)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oINISIZE6_6_35 as StdField with uid="AADHXILJCV",rtseq=269,rtrep=.f.,;
    cFormVar = "w_INISIZE6", cQueryName = "INISIZE6",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza della freccia di inizio linea",;
    HelpContextID = 124584050,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=312, Top=86, cSayPict='"999"', cGetPict='"999"', bHasZoom = .t. 

  func oINISIZE6_6_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INILINE6<>0)
    endwith
   endif
  endfunc

  func oINISIZE6_6_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_INISIZE6>0 AND .w_INISIZE6<=180)
    endwith
    return bRes
  endfunc

  proc oINISIZE6_6_35.mZoom
    this.parent.oContained.w_INISIZE6 = iif(this.parent.oContained.w_INILINE6<>3,15,20)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFINANGLE6_6_36 as StdField with uid="NPKXTKSURR",rtseq=270,rtrep=.f.,;
    cFormVar = "w_FINANGLE6", cQueryName = "FINANGLE6",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Angolo della freccia di fine linea",;
    HelpContextID = 59251835,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=312, Top=116, cSayPict='"999"', cGetPict='"999"', bHasZoom = .t. 

  func oFINANGLE6_6_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FINLINE6<>0)
    endwith
   endif
  endfunc

  func oFINANGLE6_6_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FINANGLE6>0 AND .w_FINANGLE6<=180)
    endwith
    return bRes
  endfunc

  proc oFINANGLE6_6_36.mZoom
    this.parent.oContained.w_FINANGLE6 = iif(this.parent.oContained.w_FINLINE6<>3,36,60)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFINSIZE6_6_37 as StdField with uid="DLVSIVXENE",rtseq=271,rtrep=.f.,;
    cFormVar = "w_FINSIZE6", cQueryName = "FINSIZE6",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza della freccia di fine linea",;
    HelpContextID = 124277618,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=312, Top=158, cSayPict='"999"', cGetPict='"999"', bHasZoom = .t. 

  func oFINSIZE6_6_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FINLINE6<>0)
    endwith
   endif
  endfunc

  func oFINSIZE6_6_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FINSIZE6>0 AND .w_FINSIZE6<=180)
    endwith
    return bRes
  endfunc

  proc oFINSIZE6_6_37.mZoom
    this.parent.oContained.w_FINSIZE6 = iif(this.parent.oContained.w_FINLINE6<>3,15,20)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oStr_6_4 as StdString with uid="FPMUBHYNVQ",Visible=.t., Left=45, Top=243,;
    Alignment=0, Width=94, Height=18,;
    Caption="Larghezza bordo:"  ;
  , bGlobalFont=.t.

  add object oStr_6_5 as StdString with uid="FHWILJOBXL",Visible=.t., Left=45, Top=216,;
    Alignment=0, Width=59, Height=18,;
    Caption="Curvatura:"  ;
  , bGlobalFont=.t.

  add object oStr_6_6 as StdString with uid="GLKXGHFDDH",Visible=.t., Left=45, Top=270,;
    Alignment=0, Width=94, Height=18,;
    Caption="Colore di sfondo:"  ;
  , bGlobalFont=.t.

  add object oStr_6_11 as StdString with uid="MRFNWGVRYE",Visible=.t., Left=45, Top=69,;
    Alignment=0, Width=61, Height=18,;
    Caption="Inizio linea:"  ;
  , bGlobalFont=.t.

  add object oStr_6_14 as StdString with uid="VADXGWNCLQ",Visible=.t., Left=45, Top=141,;
    Alignment=0, Width=61, Height=18,;
    Caption="Fine linea:"  ;
  , bGlobalFont=.t.

  add object oStr_6_15 as StdString with uid="BZPULQYXLN",Visible=.t., Left=45, Top=189,;
    Alignment=0, Width=61, Height=18,;
    Caption="Stile linea:"  ;
  , bGlobalFont=.t.

  add object oStr_6_45 as StdString with uid="INCRLFDYWR",Visible=.t., Left=398, Top=49,;
    Alignment=0, Width=49, Height=18,;
    Caption="Angolo"  ;
  , bGlobalFont=.t.

  add object oStr_6_46 as StdString with uid="MIVJKJOFWP",Visible=.t., Left=398, Top=120,;
    Alignment=0, Width=49, Height=18,;
    Caption="Angolo"  ;
  , bGlobalFont=.t.

  add object oStr_6_47 as StdString with uid="IRZZSWDGAE",Visible=.t., Left=398, Top=90,;
    Alignment=0, Width=59, Height=18,;
    Caption="Lunghezza"  ;
  , bGlobalFont=.t.

  add object oStr_6_48 as StdString with uid="QZQLWTXWCE",Visible=.t., Left=398, Top=162,;
    Alignment=0, Width=59, Height=18,;
    Caption="Lunghezza"  ;
  , bGlobalFont=.t.

  add object oBox_6_31 as StdBox with uid="ESOZSFBFGH",left=10, top=33, width=286,height=2

  add object oBox_6_38 as StdBox with uid="NKGFYIXBOS",left=291, top=55, width=2,height=44

  add object oBox_6_39 as StdBox with uid="VZXRWSTGED",left=293, top=55, width=17,height=2

  add object oBox_6_40 as StdBox with uid="PJWOJTVBPF",left=280, top=76, width=11,height=2

  add object oBox_6_41 as StdBox with uid="PRMMRMDMCR",left=291, top=127, width=2,height=44

  add object oBox_6_42 as StdBox with uid="ZJQKYGYRGK",left=293, top=169, width=17,height=2

  add object oBox_6_43 as StdBox with uid="TNZQZUIWWL",left=293, top=127, width=17,height=2

  add object oBox_6_44 as StdBox with uid="OXGVHKVCJK",left=280, top=148, width=11,height=2

  add object oBox_6_53 as StdBox with uid="YGORHKYSAM",left=291, top=98, width=19,height=2
enddefine
define class tcp_objmaskoptionsPag7 as StdContainer
  Width  = 713
  height = 512
  stdWidth  = 713
  stdheight = 512
  resizeXpos=523
  resizeYpos=430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLEXE7_7_1 as StdCheck with uid="JMHUVBEWKW",rtseq=198,rtrep=.f.,left=10, top=47, caption=" ",;
    HelpContextID = 186722071,;
    cFormVar="w_FLEXE7", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oFLEXE7_7_1.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLEXE7_7_1.GetRadio()
    this.Parent.oContained.w_FLEXE7 = this.RadioValue()
    return .t.
  endfunc

  func oFLEXE7_7_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLEXE7==.T.,1,;
      0)
  endfunc

  func oFLEXE7_7_1.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1)
    endwith
  endfunc

  add object oEXEC7_7_3 as StdField with uid="ZTWTNLQRMF",rtseq=199,rtrep=.f.,;
    cFormVar = "w_EXEC7", cQueryName = "EXEC7",;
    bObbl = .f. , nPag = 7, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Espressione eseguita dall'immagine",;
    HelpContextID = 93296617,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=96, Top=43, InputMask=replicate('X',50)

  add object oFLTYP7_7_4 as StdCheck with uid="UZZANQAVXK",rtseq=200,rtrep=.f.,left=10, top=74, caption=" ",;
    HelpContextID = 141079,;
    cFormVar="w_FLTYP7", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oFLTYP7_7_4.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTYP7_7_4.GetRadio()
    this.Parent.oContained.w_FLTYP7 = this.RadioValue()
    return .t.
  endfunc

  func oFLTYP7_7_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTYP7==.T.,1,;
      0)
  endfunc

  func oFLTYP7_7_4.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1)
    endwith
  endfunc


  add object oTYPE_BMP_7_6 as StdCombo with uid="HWRHZVHXKS",rtseq=201,rtrep=.f.,left=96,top=70,width=146,height=22;
    , ToolTipText = "Tipo dell'immagine";
    , HelpContextID = 230341686;
    , cFormVar="w_TYPE_BMP",RowSource=""+""+MSG_PROGRAM+","+""+MSG_DIALOG_WINDOW+","+""+MSG_ZOOM+","+""+MSG_VFM_DRILLDOWN+","+""+MSG_VFM_DRILLUP+","+""+MSG_VFM_HOME+","+""+MSG_QUERY_BS_REPORT+"", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oTYPE_BMP_7_6.RadioValue()
    return(iif(this.value =1,"PRG",;
    iif(this.value =2,"MSK",;
    iif(this.value =3,"ZOOM",;
    iif(this.value =4,"DOWN",;
    iif(this.value =5,"UP",;
    iif(this.value =6,"HOME",;
    iif(this.value =7,"Q/R",;
    space(4)))))))))
  endfunc
  func oTYPE_BMP_7_6.GetRadio()
    this.Parent.oContained.w_TYPE_BMP = this.RadioValue()
    return .t.
  endfunc

  func oTYPE_BMP_7_6.SetRadio()
    this.Parent.oContained.w_TYPE_BMP=trim(this.Parent.oContained.w_TYPE_BMP)
    this.value = ;
      iif(this.Parent.oContained.w_TYPE_BMP=="PRG",1,;
      iif(this.Parent.oContained.w_TYPE_BMP=="MSK",2,;
      iif(this.Parent.oContained.w_TYPE_BMP=="ZOOM",3,;
      iif(this.Parent.oContained.w_TYPE_BMP=="DOWN",4,;
      iif(this.Parent.oContained.w_TYPE_BMP=="UP",5,;
      iif(this.Parent.oContained.w_TYPE_BMP=="HOME",6,;
      iif(this.Parent.oContained.w_TYPE_BMP=="Q/R",7,;
      0)))))))
  endfunc

  add object oFLTTL7_7_7 as StdCheck with uid="JILWEQFPRM",rtseq=202,rtrep=.f.,left=10, top=101, caption=" ",;
    HelpContextID = 72492823,;
    cFormVar="w_FLTTL7", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oFLTTL7_7_7.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTTL7_7_7.GetRadio()
    this.Parent.oContained.w_FLTTL7 = this.RadioValue()
    return .t.
  endfunc

  func oFLTTL7_7_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTTL7==.T.,1,;
      0)
  endfunc

  func oFLTTL7_7_7.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1)
    endwith
  endfunc

  add object oTITLE7_7_9 as StdField with uid="LYYAAVSJXG",rtseq=203,rtrep=.f.,;
    cFormVar = "w_TITLE7", cQueryName = "TITLE7",;
    bObbl = .f. , nPag = 7, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Titolo dell'immagine",;
    HelpContextID = 198330647,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=96, Top=97, InputMask=replicate('X',100)

  add object oFLPIC7_7_10 as StdCheck with uid="UBMKTTOUJS",rtseq=204,rtrep=.f.,left=9, top=128, caption=" ",;
    HelpContextID = 235284247,;
    cFormVar="w_FLPIC7", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oFLPIC7_7_10.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLPIC7_7_10.GetRadio()
    this.Parent.oContained.w_FLPIC7 = this.RadioValue()
    return .t.
  endfunc

  func oFLPIC7_7_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLPIC7==.T.,1,;
      0)
  endfunc

  func oFLPIC7_7_10.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1)
    endwith
  endfunc


  add object oObj_7_11 as cp_askpictfile with uid="KLVILBQOZQ",left=347, top=124, width=23,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_PICTURE7",;
    nPag=7;
    , ToolTipText = "Selezione file Bitmap";
    , HelpContextID = 96083735

  add object oPICTURE7_7_12 as StdField with uid="VYVIWRCYTE",rtseq=205,rtrep=.f.,;
    cFormVar = "w_PICTURE7", cQueryName = "PICTURE7",;
    bObbl = .f. , nPag = 7, value=space(300), bMultilanguage =  .f.,;
    ToolTipText = "Immagine associata",;
    HelpContextID = 77379230,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=96, Top=124, InputMask=replicate('X',300)

  add object oFLTOP7_7_16 as StdCheck with uid="ITOZUFQVBO",rtseq=206,rtrep=.f.,left=9, top=199, caption=" ",;
    HelpContextID = 10626839,;
    cFormVar="w_FLTOP7", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oFLTOP7_7_16.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLTOP7_7_16.GetRadio()
    this.Parent.oContained.w_FLTOP7 = this.RadioValue()
    return .t.
  endfunc

  func oFLTOP7_7_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLTOP7==.T.,1,;
      0)
  endfunc

  func oFLTOP7_7_16.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1)
    endwith
  endfunc

  add object oTOP7_7_17 as StdField with uid="LKPNTTUDNC",rtseq=207,rtrep=.f.,;
    cFormVar = "w_TOP7", cQueryName = "TOP7",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione dell'immagine nella maschera (in alto)",;
    HelpContextID = 36038935,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=97, Top=195, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLLEF7_7_18 as StdCheck with uid="EPVYCVKFPO",rtseq=208,rtrep=.f.,left=9, top=229, caption=" ",;
    HelpContextID = 189409047,;
    cFormVar="w_FLLEF7", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oFLLEF7_7_18.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLLEF7_7_18.GetRadio()
    this.Parent.oContained.w_FLLEF7 = this.RadioValue()
    return .t.
  endfunc

  func oFLLEF7_7_18.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLLEF7==.T.,1,;
      0)
  endfunc

  func oFLLEF7_7_18.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1)
    endwith
  endfunc

  add object oLEFT7_7_19 as StdField with uid="LFGKQWQVZV",rtseq=209,rtrep=.f.,;
    cFormVar = "w_LEFT7", cQueryName = "LEFT7",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione dell'immagine nella maschera (a sinistra)",;
    HelpContextID = 111111913,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=97, Top=226, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLHGT7_7_20 as StdCheck with uid="DRGHCGTQQL",rtseq=210,rtrep=.f.,left=217, top=199, caption=" ",;
    HelpContextID = 221128471,;
    cFormVar="w_FLHGT7", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oFLHGT7_7_20.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLHGT7_7_20.GetRadio()
    this.Parent.oContained.w_FLHGT7 = this.RadioValue()
    return .t.
  endfunc

  func oFLHGT7_7_20.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLHGT7==.T.,1,;
      0)
  endfunc

  func oFLHGT7_7_20.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1)
    endwith
  endfunc

  add object oHEIGHT7_7_21 as StdField with uid="TWMETYZLJA",rtseq=211,rtrep=.f.,;
    cFormVar = "w_HEIGHT7", cQueryName = "HEIGHT7",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica l'altezza dell'immagine",;
    HelpContextID = 114453280,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=298, Top=194, cSayPict='"9999"', cGetPict='"9999"'

  add object oFLWDT7_7_22 as StdCheck with uid="KNIFJSQFLV",rtseq=212,rtrep=.f.,left=217, top=229, caption=" ",;
    HelpContextID = 223291159,;
    cFormVar="w_FLWDT7", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oFLWDT7_7_22.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLWDT7_7_22.GetRadio()
    this.Parent.oContained.w_FLWDT7 = this.RadioValue()
    return .t.
  endfunc

  func oFLWDT7_7_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLWDT7==.T.,1,;
      0)
  endfunc

  func oFLWDT7_7_22.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1)
    endwith
  endfunc

  add object oWIDTH7_7_23 as StdField with uid="WYKTYRWQRB",rtseq=213,rtrep=.f.,;
    cFormVar = "w_WIDTH7", cQueryName = "WIDTH7",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la larghezza dell'immagine",;
    HelpContextID = 140658199,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=298, Top=225, cSayPict='"9999"', cGetPict='"9999"'

  add object oSELECT7_7_28 as StdRadio with uid="IOLBCMIZIO",rtseq=219,rtrep=.f.,left=357, top=8, width=193,height=35;
    , cFormVar="w_SELECT7", ButtonCount=2, bObbl=.f., nPag=7;
  , bGlobalFont=.t.

    proc oSELECT7_7_28.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption=""+MSG_SELECT_ALL_TEXT+""
      this.Buttons(1).HelpContextID = 28669472
      this.Buttons(1).Top=0
      this.Buttons(2).Caption=""+MSG_DESELECT_ALL+""
      this.Buttons(2).HelpContextID = 28669472
      this.Buttons(2).Top=16
      this.SetAll("Width",191)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECT7_7_28.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    "N")))
  endfunc
  func oSELECT7_7_28.GetRadio()
    this.Parent.oContained.w_SELECT7 = this.RadioValue()
    return .t.
  endfunc

  func oSELECT7_7_28.SetRadio()
    this.Parent.oContained.w_SELECT7=trim(this.Parent.oContained.w_SELECT7)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT7=="S",1,;
      iif(this.Parent.oContained.w_SELECT7=="N",2,;
      0))
  endfunc

  func oSELECT7_7_28.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1)
    endwith
  endfunc


  add object oBtn_7_30 as StdButton with uid="HWVTXSFHLW",left=553, top=462, width=48,height=45,;
    CpPicture="BMP\applica.ico", caption="", nPag=7;
    , ToolTipText = "Premere per applicare le modifiche";
    , HelpContextID = 172758090;
    , caption="\<Applica";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_30.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"Save",7)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_7_31 as StdButton with uid="GJKCKRRLZE",left=608, top=462, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=7;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 98971159;
    , caption="\<Ok";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_31.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_7_32 as StdButton with uid="EPIHDSZPKQ",left=663, top=462, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=7;
    , ToolTipText = "Premere per annullare le operazioni svolte";
    , HelpContextID = 250656791;
    , caption="\<Esci";
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_32.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object zBitmap as cp_szoombox with uid="ELITNHDWKU",left=503, top=7, width=208,height=447,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="objmaskoptions",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomFile="default",bOptions=.f.,;
    cEvent = "Pagina7",;
    nPag=7;
    , HelpContextID = 204127767


  add object bitmap as cp_calclbl with uid="PGYEGESXTO",left=10, top=8, width=262,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,;
    nPag=7;
    , HelpContextID = 204127767

  add object oFLFOR7_7_40 as StdCheck with uid="NVWSOETSMV",rtseq=266,rtrep=.f.,left=386, top=131, caption=" ",;
    HelpContextID = 246425367,;
    cFormVar="w_FLFOR7", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oFLFOR7_7_40.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLFOR7_7_40.GetRadio()
    this.Parent.oContained.w_FLFOR7 = this.RadioValue()
    return .t.
  endfunc

  func oFLFOR7_7_40.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLFOR7==.T.,1,;
      0)
  endfunc

  func oFLFOR7_7_40.mHide()
    with this.Parent.oContained
      return (.w_HIDE7=1 or EMPTY(.w_PICTURE7) OR LOWER(JUSTEXT(.w_PICTURE7))<>"ico")
    endwith
  endfunc


  add object oFORMAT7_7_41 as StdCombo with uid="IRXPEODPHR",rtseq=267,rtrep=.f.,left=410,top=124,width=83,height=22;
    , HelpContextID = 3934496;
    , cFormVar="w_FORMAT7",RowSource=""+"16x16,"+"24x24,"+"32x32,"+"48x48,"+"64x64,"+"96x96,"+"128x128,"+"192x192,"+"256x256", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oFORMAT7_7_41.RadioValue()
    return(iif(this.value =1,16,;
    iif(this.value =2,24,;
    iif(this.value =3,32,;
    iif(this.value =4,48,;
    iif(this.value =5,64,;
    iif(this.value =6,96,;
    iif(this.value =7,128,;
    iif(this.value =8,192,;
    iif(this.value =9,256,;
    0))))))))))
  endfunc
  func oFORMAT7_7_41.GetRadio()
    this.Parent.oContained.w_FORMAT7 = this.RadioValue()
    return .t.
  endfunc

  func oFORMAT7_7_41.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FORMAT7==16,1,;
      iif(this.Parent.oContained.w_FORMAT7==24,2,;
      iif(this.Parent.oContained.w_FORMAT7==32,3,;
      iif(this.Parent.oContained.w_FORMAT7==48,4,;
      iif(this.Parent.oContained.w_FORMAT7==64,5,;
      iif(this.Parent.oContained.w_FORMAT7==96,6,;
      iif(this.Parent.oContained.w_FORMAT7==128,7,;
      iif(this.Parent.oContained.w_FORMAT7==192,8,;
      iif(this.Parent.oContained.w_FORMAT7==256,9,;
      0)))))))))
  endfunc

  func oFORMAT7_7_41.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PICTURE7) OR LOWER(JUSTEXT(.w_PICTURE7))<>"ico")
    endwith
  endfunc

  add object oStr_7_2 as StdString with uid="XPVGNMSGBB",Visible=.t., Left=37, Top=47,;
    Alignment=0, Width=48, Height=18,;
    Caption="Esegue:"  ;
  , bGlobalFont=.t.

  add object oStr_7_5 as StdString with uid="VBJERWKHKI",Visible=.t., Left=37, Top=74,;
    Alignment=0, Width=46, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_7_8 as StdString with uid="WEOLMNHMYQ",Visible=.t., Left=37, Top=101,;
    Alignment=0, Width=35, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_7_13 as StdString with uid="OTIXXZXBYC",Visible=.t., Left=37, Top=128,;
    Alignment=0, Width=43, Height=18,;
    Caption="Bitmap:"  ;
  , bGlobalFont=.t.

  add object oStr_7_14 as StdString with uid="QLWOSFFVEY",Visible=.t., Left=47, Top=165,;
    Alignment=0, Width=176, Height=18,;
    Caption="Posizione e Dimensione"  ;
  , bGlobalFont=.t.

  add object oStr_7_24 as StdString with uid="SRBGHSIMWG",Visible=.t., Left=37, Top=199,;
    Alignment=0, Width=63, Height=18,;
    Caption="Top:"  ;
  , bGlobalFont=.t.

  add object oStr_7_25 as StdString with uid="XNTTCAFBCL",Visible=.t., Left=37, Top=230,;
    Alignment=0, Width=62, Height=18,;
    Caption="Left:"  ;
  , bGlobalFont=.t.

  add object oStr_7_26 as StdString with uid="ULHGWTIOTN",Visible=.t., Left=242, Top=198,;
    Alignment=0, Width=67, Height=18,;
    Caption="Height:"  ;
  , bGlobalFont=.t.

  add object oStr_7_27 as StdString with uid="QNLSZTOEWF",Visible=.t., Left=242, Top=229,;
    Alignment=0, Width=60, Height=18,;
    Caption="Width:"  ;
  , bGlobalFont=.t.

  add object oStr_7_42 as StdString with uid="LFTFVKARND",Visible=.t., Left=410, Top=110,;
    Alignment=0, Width=55, Height=18,;
    Caption="Formato:"  ;
  , bGlobalFont=.t.

  func oStr_7_42.mHide()
    with this.Parent.oContained
      return (EMPTY(nvl(.w_PICTURE7,"")) OR LOWER(JUSTEXT(nvl(.w_PICTURE7,"")))<>"ico")
    endwith
  endfunc

  add object oBox_7_15 as StdBox with uid="RXUFSSDZTL",left=33, top=187, width=381,height=1

  add object oBox_7_38 as StdBox with uid="IWVIBPHSBS",left=10, top=33, width=286,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_objmaskoptions','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_objmaskoptions
Proc cp_FontColor(oObj, nVar)
   local l_obj
   l_obj = oObj.GetCtrl("w_FNTCOLOR"+m.nVar)
   l_obj.DisabledBackcolor = oObj.w_FNTCOLOR&nVar
   l_obj.DisabledForecolor = oObj.w_FNTCOLOR&nVar
   l_obj=.null.
EndProc
* --- Fine Area Manuale
