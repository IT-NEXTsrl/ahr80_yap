* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_foxchartfields                                               *
*              FoxChart                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-07-29                                                      *
* Last revis.: 2012-06-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tcp_foxchartfields")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tccp_foxchartfields")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tccp_foxchartfields")
  return

* --- Class definition
define class tcp_foxchartfields as StdPCForm
  Width  = 593
  Height = 211
  Top    = 6
  Left   = 6
  cComment = "FoxChart"
  cPrg = "cp_foxchartfields"
  HelpContextID=38428073
  add object cnt as tccp_foxchartfields
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tscp_foxchartfields as PCContext
  w_FDSERIAL = space(10)
  w_CPROWORD = 0
  w_FD_VALUE = space(25)
  w_FDCOLOR = 0
  w_FDLEGEND = space(25)
  w_FD_SHAPE = 0
  w_FDVALONS = space(1)
  proc Save(i_oFrom)
    this.w_FDSERIAL = i_oFrom.w_FDSERIAL
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_FD_VALUE = i_oFrom.w_FD_VALUE
    this.w_FDCOLOR = i_oFrom.w_FDCOLOR
    this.w_FDLEGEND = i_oFrom.w_FDLEGEND
    this.w_FD_SHAPE = i_oFrom.w_FD_SHAPE
    this.w_FDVALONS = i_oFrom.w_FDVALONS
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_FDSERIAL = this.w_FDSERIAL
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_FD_VALUE = this.w_FD_VALUE
    i_oTo.w_FDCOLOR = this.w_FDCOLOR
    i_oTo.w_FDLEGEND = this.w_FDLEGEND
    i_oTo.w_FD_SHAPE = this.w_FD_SHAPE
    i_oTo.w_FDVALONS = this.w_FDVALONS
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tccp_foxchartfields as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 593
  Height = 211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-13"
  HelpContextID=38428073
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  cpfchart_IDX = 0
  cpfchart_IDX = 0
  cFile = "cpfchart"
  cKeySelect = "FDSERIAL"
  cKeyWhere  = "FDSERIAL=this.w_FDSERIAL"
  cKeyDetail  = "FDSERIAL=this.w_FDSERIAL"
  cKeyWhereODBC = '"FDSERIAL="+cp_ToStrODBC(this.w_FDSERIAL)';

  cKeyDetailWhereODBC = '"FDSERIAL="+cp_ToStrODBC(this.w_FDSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"cpfchart.FDSERIAL="+cp_ToStrODBC(this.w_FDSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'cpfchart.CPROWORD '
  cPrg = "cp_foxchartfields"
  cComment = "FoxChart"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FDSERIAL = space(10)
  w_CPROWORD = 0
  w_FD_VALUE = space(25)
  w_FDCOLOR = 0
  o_FDCOLOR = 0
  w_FDLEGEND = space(25)
  w_FD_SHAPE = 0
  w_FDVALONS = .F.
  w_oHeaderDetail = .NULL.
  w_oBckColorChartL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_foxchartfields
  w_cCursorAlias = SYS(2015)
  
  Proc SetColor(pVar)
     Local L_ExecCMD, l_Color
     L_ExecCMD= 'this.w_'+pVar
     l_Color = GetColor(&L_ExecCMD)
     If l_Color = -1
       l_Color = RGB(255,255,255)
     EndIf
     If l_Color <> -1
       L_ExecCMD= L_ExecCMD+'= m.l_COLOR'
       &L_ExecCMD
     EndIf
     this.mCalc(.t.)
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_foxchartfieldsPag1","cp_foxchartfields",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    this.w_oBckColorChartL = this.oPgFrm.Pages(1).oPag.oBckColorChartL
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      this.w_oBckColorChartL = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='cpfchart'
    this.cWorkTables[2]='cpfchart'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.cpfchart_IDX,5],7]
    this.nPostItConn=i_TableProp[this.cpfchart_IDX,3]
  return

  procedure NewContext()
    return(createobject('tscp_foxchartfields'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from cpfchart where FDSERIAL=KeySet.FDSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.cpfchart_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cpfchart_IDX,2],this.bLoadRecFilter,this.cpfchart_IDX,"cp_foxchartfields")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('cpfchart')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "cpfchart.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' cpfchart '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FDSERIAL',this.w_FDSERIAL  )
      select * from (i_cTable) cpfchart where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FDSERIAL = NVL(FDSERIAL,space(10))
        cp_LoadRecExtFlds(this,'cpfchart')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_FD_VALUE = NVL(FD_VALUE,space(25))
          .w_FDCOLOR = NVL(FDCOLOR,0)
          .w_FDLEGEND = NVL(FDLEGEND,space(25))
          .w_FD_SHAPE = NVL(FD_SHAPE,0)
          .w_FDVALONS = !empty(NVL(FDVALONS,.f.))
        .oPgFrm.Page1.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_FDCOLOR, .w_FDCOLOR)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_FDSERIAL=space(10)
      .w_CPROWORD=10
      .w_FD_VALUE=space(25)
      .w_FDCOLOR=0
      .w_FDLEGEND=space(25)
      .w_FD_SHAPE=0
      .w_FDVALONS=.f.
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,2,.f.)
        .w_FD_VALUE = ' '
        .w_FDCOLOR = RGB(255,255,255)
        .DoRTCalc(5,5,.f.)
        .w_FD_SHAPE = 5
        .w_FDVALONS = .T.
        .oPgFrm.Page1.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_FDCOLOR, .w_FDCOLOR)
      endif
    endwith
    cp_BlankRecExtFlds(this,'cpfchart')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_2_8.enabled = i_bVal
      .Page1.oPag.oBtn_1_3.enabled = i_bVal
      .Page1.oPag.oBckColorChartL.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'cpfchart',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.cpfchart_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FDSERIAL,"FDSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(3);
      ,t_FD_VALUE N(3);
      ,t_FDLEGEND C(25);
      ,t_FD_SHAPE N(3);
      ,t_FDVALONS N(3);
      ,CPROWNUM N(10);
      ,t_FDCOLOR N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tcp_foxchartfieldsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFD_VALUE_2_2.controlsource=this.cTrsName+'.t_FD_VALUE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFDLEGEND_2_4.controlsource=this.cTrsName+'.t_FDLEGEND'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFD_SHAPE_2_5.controlsource=this.cTrsName+'.t_FD_SHAPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFDVALONS_2_6.controlsource=this.cTrsName+'.t_FDVALONS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(42)
    this.AddVLine(209)
    this.AddVLine(378)
    this.AddVLine(545)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.cpfchart_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cpfchart_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.cpfchart_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cpfchart_IDX,2])
      *
      * insert into cpfchart
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'cpfchart')
        i_extval=cp_InsertValODBCExtFlds(this,'cpfchart')
        i_cFldBody=" "+;
                  "(FDSERIAL,CPROWORD,FD_VALUE,FDCOLOR,FDLEGEND"+;
                  ",FD_SHAPE,FDVALONS,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_FDSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_FD_VALUE)+","+cp_ToStrODBC(this.w_FDCOLOR)+","+cp_ToStrODBC(this.w_FDLEGEND)+;
             ","+cp_ToStrODBC(this.w_FD_SHAPE)+","+cp_ToStrODBC(this.w_FDVALONS)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'cpfchart')
        i_extval=cp_InsertValVFPExtFlds(this,'cpfchart')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'FDSERIAL',this.w_FDSERIAL)
        INSERT INTO (i_cTable) (;
                   FDSERIAL;
                  ,CPROWORD;
                  ,FD_VALUE;
                  ,FDCOLOR;
                  ,FDLEGEND;
                  ,FD_SHAPE;
                  ,FDVALONS;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_FDSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_FD_VALUE;
                  ,this.w_FDCOLOR;
                  ,this.w_FDLEGEND;
                  ,this.w_FD_SHAPE;
                  ,this.w_FDVALONS;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.cpfchart_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cpfchart_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) and nvl(t_FD_VALUE,1) > 1) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'cpfchart')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'cpfchart')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and nvl(t_FD_VALUE,1) > 1) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update cpfchart
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'cpfchart')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",FD_VALUE="+cp_ToStrODBC(this.w_FD_VALUE)+;
                     ",FDCOLOR="+cp_ToStrODBC(this.w_FDCOLOR)+;
                     ",FDLEGEND="+cp_ToStrODBC(this.w_FDLEGEND)+;
                     ",FD_SHAPE="+cp_ToStrODBC(this.w_FD_SHAPE)+;
                     ",FDVALONS="+cp_ToStrODBC(this.w_FDVALONS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'cpfchart')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,FD_VALUE=this.w_FD_VALUE;
                     ,FDCOLOR=this.w_FDCOLOR;
                     ,FDLEGEND=this.w_FDLEGEND;
                     ,FD_SHAPE=this.w_FD_SHAPE;
                     ,FDVALONS=this.w_FDVALONS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.cpfchart_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cpfchart_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and nvl(t_FD_VALUE,1) > 1) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete cpfchart
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and nvl(t_FD_VALUE,1) > 1) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.cpfchart_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cpfchart_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        if .o_FDCOLOR<>.w_FDCOLOR
        .oPgFrm.Page1.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_FDCOLOR, .w_FDCOLOR)
        endif
        * --- Area Manuale = Calculate
        * --- cp_foxchartfields
        this.w_oBckColorChartL.Calculate('      1234567890     ',this.w_FDCOLOR, this.w_FDCOLOR)
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_FDCOLOR with this.w_FDCOLOR
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_FDCOLOR, .w_FDCOLOR)
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_FDCOLOR, .w_FDCOLOR)
    endwith
  return
  proc Calculate_VDPBCTVWFT()
    with this
          * --- ADDFOXCHARTFIELDS
          cp_setfoxchartproperties(this;
              ,"ADDFOXCHARTFIELDS";
             )
    endwith
  endproc
  proc Calculate_IRCMUHJUYR()
    with this
          * --- REDRAWFOXCHARTS
          cp_setfoxchartproperties(this;
              ,"REDRAWFOXCHARTS";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_8.enabled =this.oPgFrm.Page1.oPag.oBtn_2_8.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("ADDFOXCHARTFIELDS")
          .Calculate_VDPBCTVWFT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("REDRAWFOXCHARTS")
          .Calculate_IRCMUHJUYR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBckColorChartL.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_VALUE_2_2.RadioValue()==this.w_FD_VALUE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_VALUE_2_2.SetRadio()
      replace t_FD_VALUE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_VALUE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFDLEGEND_2_4.value==this.w_FDLEGEND)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFDLEGEND_2_4.value=this.w_FDLEGEND
      replace t_FDLEGEND with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFDLEGEND_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_SHAPE_2_5.RadioValue()==this.w_FD_SHAPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_SHAPE_2_5.SetRadio()
      replace t_FD_SHAPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_SHAPE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFDVALONS_2_6.RadioValue()==this.w_FDVALONS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFDVALONS_2_6.SetRadio()
      replace t_FDVALONS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFDVALONS_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'cpfchart')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD)) and nvl(t_FD_VALUE,1) > 1
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FDCOLOR = this.w_FDCOLOR
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and nvl(t_FD_VALUE,1) > 1)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999,cp_maxroword()+10)
      .w_FD_VALUE=space(25)
      .w_FDCOLOR=0
      .w_FDLEGEND=space(25)
      .w_FD_SHAPE=0
      .w_FDVALONS=.f.
      .DoRTCalc(1,2,.f.)
        .w_FD_VALUE = ' '
        .w_FDCOLOR = RGB(255,255,255)
      .DoRTCalc(5,5,.f.)
        .w_FD_SHAPE = 5
        .w_FDVALONS = .T.
        .oPgFrm.Page1.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_FDCOLOR, .w_FDCOLOR)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_FD_VALUE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_VALUE_2_2.RadioValue(.t.)
    this.w_FDCOLOR = t_FDCOLOR
    this.w_FDLEGEND = t_FDLEGEND
    this.w_FD_SHAPE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_SHAPE_2_5.RadioValue(.t.)
    this.w_FDVALONS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFDVALONS_2_6.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_FD_VALUE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_VALUE_2_2.ToRadio()
    replace t_FDCOLOR with this.w_FDCOLOR
    replace t_FDLEGEND with this.w_FDLEGEND
    replace t_FD_SHAPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFD_SHAPE_2_5.ToRadio()
    replace t_FDVALONS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFDVALONS_2_6.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tcp_foxchartfieldsPag1 as StdContainer
  Width  = 589
  height = 211
  stdWidth  = 589
  stdheight = 211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=2, width=574,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Riga",Field2="FD_VALUE",Label2="Field value",Field3="FDLEGEND",Label3="Field legend",Field4="FD_SHAPE",Label4="Shape",Field5="FDVALONS",Label5="Show",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 81248007


  add object oBtn_1_3 as StdButton with uid="MHYBREUBWU",left=484, top=181, width=77,height=25,;
    caption="Apply", nPag=1;
    , HelpContextID = 233630023;
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        .NotifyEvent("ADDFOXCHARTFIELDS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=21,;
    width=572+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=22,width=571+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_8 as StdButton with uid="KJTASBKMYO",width=25,height=21,;
   left=216, top=181,;
    caption="...", nPag=2;
    , ToolTipText = ""+MSG_SELECT_COLOR+"";
    , HelpContextID = 35211705;
  , bGlobalFont=.t.

    proc oBtn_2_8.Click()
      with this.Parent.oContained
        .SetColor('FDCOLOR')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_8.mCond()
    with this.Parent.oContained
      return (this.Parent.oContained.oParentObject .w_COLORTYPE=2)
    endwith
  endfunc

  add object oBckColorChartL as cp_calclbl with uid="EVFCJWFLHA",width=118,height=21,;
   left=92, top=181,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=2;
    , ToolTipText = "background color";
    , HelpContextID = 125179719

  add object oStr_2_7 as StdString with uid="CDFIWNDROJ",Visible=.t., Left=18, Top=183,;
    Alignment=1, Width=69, Height=18,;
    Caption="Color:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tcp_foxchartfieldsBodyRow as CPBodyRowCnt
  Width=562
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="RHGTJTCPYZ",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 100864039,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=-2, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oFD_VALUE_2_2 as StdTrsTableComboALias with uid="HYVTGQPBHJ",rtrep=.t.,;
    cFormVar="w_FD_VALUE", tablefilter="" , ;
    ToolTipText = "Field value",;
    HelpContextID = 193383700,;
    Height=21, Width=163, Left=35, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(25);
  , bGlobalFont=.t.



  add object oFDLEGEND_2_4 as StdTrsField with uid="HHXMAZIKFR",rtseq=5,rtrep=.t.,;
    cFormVar="w_FDLEGEND",value=space(25),;
    ToolTipText = "Field legend",;
    HelpContextID = 156644053,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=164, Left=203, Top=0, InputMask=replicate('X',25)

  add object oFD_SHAPE_2_5 as StdTrsCombo with uid="GSCQCGRDYE",rtrep=.t.,;
    cFormVar="w_FD_SHAPE", RowSource=""+"Closed circle,"+"Closed square,"+"Closed triangle,"+"Plus sign,"+"Star,"+"Square with plus sign inside,"+"Open square,"+"Open circle with dot inside,"+"Lightning,"+"Man,"+"Dot" , ;
    ToolTipText = "Field shape",;
    HelpContextID = 79088921,;
    Height=22, Width=162, Left=372, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oFD_SHAPE_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FD_SHAPE,&i_cF..t_FD_SHAPE),this.value)
    return(iif(xVal =1,1,;
    iif(xVal =2,2,;
    iif(xVal =3,3,;
    iif(xVal =4,4,;
    iif(xVal =5,5,;
    iif(xVal =6,6,;
    iif(xVal =7,7,;
    iif(xVal =8,8,;
    iif(xVal =9,9,;
    iif(xVal =10,10,;
    iif(xVal =11,11,;
    0))))))))))))
  endfunc
  func oFD_SHAPE_2_5.GetRadio()
    this.Parent.oContained.w_FD_SHAPE = this.RadioValue()
    return .t.
  endfunc

  func oFD_SHAPE_2_5.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_FD_SHAPE==1,1,;
      iif(this.Parent.oContained.w_FD_SHAPE==2,2,;
      iif(this.Parent.oContained.w_FD_SHAPE==3,3,;
      iif(this.Parent.oContained.w_FD_SHAPE==4,4,;
      iif(this.Parent.oContained.w_FD_SHAPE==5,5,;
      iif(this.Parent.oContained.w_FD_SHAPE==6,6,;
      iif(this.Parent.oContained.w_FD_SHAPE==7,7,;
      iif(this.Parent.oContained.w_FD_SHAPE==8,8,;
      iif(this.Parent.oContained.w_FD_SHAPE==9,9,;
      iif(this.Parent.oContained.w_FD_SHAPE==10,10,;
      iif(this.Parent.oContained.w_FD_SHAPE==11,11,;
      0))))))))))))
  endfunc

  func oFD_SHAPE_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFDVALONS_2_6 as StdTrsCheck with uid="RZIUGACZBZ",rtrep=.t.,;
    cFormVar="w_FDVALONS",  caption="",;
    ToolTipText = "Show values on shape",;
    HelpContextID = 31444027,;
    Left=539, Top=0, Width=18,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oFDVALONS_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FDVALONS,&i_cF..t_FDVALONS),this.value)
    return(iif(xVal =1,.T.,;
    .f.))
  endfunc
  func oFDVALONS_2_6.GetRadio()
    this.Parent.oContained.w_FDVALONS = this.RadioValue()
    return .t.
  endfunc

  func oFDVALONS_2_6.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_FDVALONS==.T.,1,;
      0))
  endfunc

  func oFDVALONS_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_foxchartfields','cpfchart','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FDSERIAL=cpfchart.FDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_foxchartfields
* ---------------------------------------------------
*               Displayng Field
* ---------------------------------------------------
define class StdTrsTableComboALias as StdTrsTableCombo
  proc Init
    This.Clear
  	IF VARTYPE(this.bNoBackColor)='U'
		  This.backcolor = i_EBackColor
	  ENDIF
    this.ToolTipText=cp_Translate(this.ToolTipText)
    local l_numf, l_i
    DIMENSION this.combovalues(1)
    This.nValues = 1
    this.combovalues[1] = ' '
    this.AddItem(this.combovalues[1])
  endproc
  Proc Popola(pCurs)
    local l_count, l_i
    if used(pCurs)
      select (pCurs)
      AFIELDS(l_ArrField, pCurs)
      l_count = ALEN(l_ArrField , 1)
      this.nValues=l_count+1
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1 
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear   
      this.AddItem(' ')
      this.combovalues[1] = ' '
      l_i = 1
      do while l_i <= l_count
        this.AddItem(l_ArrField[l_i,1])
        this.combovalues[l_i+1]=trim(l_ArrField[l_i,1])
        l_i = l_i + 1
      enddo  
    endif  
  endproc
enddefine

Proc PickColor(pParent, oCtrl, cField)
  local l_color, oCtrl
  l_color = GETCOLOR(pParent.&cField)
  If l_color <> -1
    oCtrl.BackColor = l_color
    oCtrl.ForeColor = l_color
    pParent.&cField = l_color
  Endif
EndProc
* --- Fine Area Manuale
