* ---------------------------------------------------------------------------- *
* #%&%#Build:  56
*                                                                              *
*   Procedure: CPIG_TAB                                                        *
*              Gestione tabelle                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 30/1/97                                                         *
* Last revis.: 30/1/97                                                         *
* Translated : 03/03/2000
*                                                                              *
* ---------------------------------------------------------------------------- *

#include "cp_app_lang.inc"

Lparam i_cTable
If cp_fileexist(i_cTable+'.vfm')
    Return(Createobject("tCPIG_TAB",i_cTable))
Else
    cp_msg (cp_MsgFormat(MSG_DIALOG_WINDOW_MISSING_FOR_TABLE__,i_cTable),.F.,.F.,.F.,.F.)
Endif
Return

* --- Definizione della classe
Define Class tCPIG_TAB As StdForm
    Top    = 14
    Left   = 0

    * --- Standard Properties
    Width  = 100
    Height = 100
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    * --- Constants Properties
    table_IDX = 0
    cFile = ""
    cKeySelect = ""
    cKeyWhere  = ""
    cKeyWhereODBC = ""
    cPrg = "CPIG_TAB"
    cComment = ""
    Icon = "tabe.ico"
    * --- Local Variables
    Dimension w_flds[1],w_values[1]
    w_flds[1]=""
    w_values[1]=""
    bGetFlds=.T.
    bFox26=.F.

    * --- Define Page Frame
    Add Object oPgFrm As StdPageFrame With PageCount=2, Width=This.Width, Height=This.Height
    Proc Init(i_cTable)
        This.cFile=i_cTable
        This.cComment= cp_MsgFormat(MSG__ADMIN,i_cTable)
        DoDefault()
        Return
    Proc oPgFrm.Init
        StdPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tCPIG_TABPag1")
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption="Pag.1"
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag
    Endproc

    Proc Show()
        If This.bGetFlds
            Nodefault
        Else
            DoDefault()
        Endif
        Return

        * --- Appertura delle tabelle
    Function OpenWorkTables()
        Local i_nidx
        This.table_IDX=cp_OpenTable(This.cFile)
        If This.table_IDX<>0
            This.nOpenedTables=1
            This.cWorkTables[1]=This.cFile
        Endif
        Return(This.table_IDX<>0)

    Proc GetTableFields()
        Local i,i_nConn,i_cTable
        * --- recupera il tracciato record
        i_nConn = i_TableProp[this.table_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.table_IDX,2])
        If i_nConn<>0
            i_nRes = cp_SQL(i_nConn,"select * from "+i_cTable+" where 1=0",This.cCursor)
        Else
            Select * From (i_cTable) Where 1=0 Into Cursor (This.cCursor) nofilter
        Endif
        Dimension This.w_flds[fcount()],This.w_values[fcount()]
        For i=1 To Fcount()
            This.w_flds[i]=Field(i)
            This.w_values[i]=Eval(Fields(i))
        Next
        This.cKeySelect=This.w_flds[1]
        This.cKeyWhere=This.w_flds[1]+"=this.w_values[1]"
        This.cKeyWhereODBC='this.w_flds[1]+"="+cp_ToStrODBC(this.w_values[1])'
        Local h,c,o,l,xcg
        If At('vm_exec',Lower(Set('proc')))=0
            Set Proc To vm_exec Additive
        Endif

        This.oPgFrm.Page1.oPag.AddObject('cnt','vm_frm_exec_cnt')
        h=Fopen(This.cFile+'.vfm')
        l=Fseek(h,0,2)
        Fseek(h,0)
        c=Fread(h,l)
        =Fclose(h)
        xcg=Createobject('pxcg','load',c)
        This.oPgFrm.Page1.oPag.Cnt.Serialize(xcg)
        * --- le differenze tra maschere e tabelle
        This.oPgFrm.Page1.oPag.Cnt.oContained=This
        This.oPgFrm.Page1.oPag.Cnt.Visible=.T.
        This.oPgFrm.Page1.oPag.Cnt.btnOk.Visible=.F.
        This.oPgFrm.Page1.oPag.Cnt.btnCan.Visible=.F.
        Thisform.Height=Thisform.Height+22
        This.oPgFrm.Width=Thisform.Width
        This.oPgFrm.Height=Thisform.Height
        This.oPgFrm.Page1.oPag.Height=This.oPgFrm.Page1.oPag.Height+22
        This.cComment=This.Caption
        This.SetPosition()
        This.Show()
        * --- abilita il campo chiave per la ricerca
        c=This.oPgFrm.Page1.oPag.Cnt.GetCtrl('W_'+This.w_flds[1])
        If c<>0
            This.oPgFrm.Page1.oPag.Cnt.Controls(c).oObserver=This
            *this.oFirstControl = this.oPgFrm.Page1.oPag.cnt.Controls(c)
        Endif
        Return

    Procedure SetPostItConn()
        This.bPostIt=i_ServerConn[i_TableProp[this.table_IDX,5],7]
        This.nPostItConn=i_TableProp[this.table_IDX,3]
        Return

    Procedure SetWorkFromKeySet()
        * --- Assegnazione delle variabili di work dal KeySet che verranno utilizzate per caricare il record
        Select (This.cKeySet)
        This.w_values[1] = Eval(Field(1))
        This.xKey[1] = This.w_values[1]
        Return

        * --- Lettura del record e assegnazione alle variabili del Form
    Procedure LoadRec()
        Local i,i_cKey,i_nRes,i_cTable,i_nConn
        This.bUpdated=.F.
        * --- La select che legge il record
        i_nConn = i_TableProp[this.table_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.table_IDX,2])
        If i_nConn<>0
            i_cKey = This.cKeyWhereODBC
            i_nRes = cp_SQL(i_nConn,"select * from "+i_cTable+" where "+&i_cKey,This.cCursor)
            This.bLoaded = i_nRes<>-1 And Not(Eof())
        Else
            i_cKey = This.cKeyWhere
            Select * From (i_cTable) Where &i_cKey Into Cursor (This.cCursor) nofilter
            This.bLoaded = Not(Eof())
        Endif
        * --- Copia i valori nelle variabili di work
        If This.bLoaded
            For i=1 To Alen(This.w_flds)
                This.w_values[i]=Nvl(Eval(This.w_flds[i]),cp_NullValue(This.w_values[i]))
            Next
            This.SetControlsValue()
        Else
            This.BlankRec()
        Endif
    Endproc

    * --- Blank delle variabili del form
    Procedure BlankRec()
        Local i
        For i=1 To Alen(This.w_flds)
            This.w_values[i]=cp_NullValue(This.w_values[i])
        Next
        This.SetControlsValue()
    Endproc

    * --- Procedura di abilitazione campi
    *     cOp = Operazione di settaggio
    *     Parametri possibili : Load - Query - Edit - Filter
    *     Load e Filter settano tutti i campi
    *
    Procedure SetEnabled(i_cOp)
        Local i_bVal,c,i
        i_bVal = i_cOp<>"Query"
        * --- Disattivazione pagina Elenco per situazioni <> da Query
        This.oPgFrm.Pages(This.oPgFrm.PageCount).Enabled=Not(i_bVal)
        With This.oPgFrm
            * tutti a i_bVal
            For i=1 To Alen(This.w_flds)
                c=This.oPgFrm.Page1.oPag.Cnt.GetCtrl('w_'+This.w_flds[i])
                If c<>0
                    This.oPgFrm.Page1.oPag.Cnt.Controls(c).Enabled=i_bVal
                Endif
            Next
            If i_cOp = "Edit"
                * --- chiave disbilitata
                c=This.oPgFrm.Page1.oPag.Cnt.GetCtrl('w_'+This.w_flds[1])
                If c<>0
                    This.oPgFrm.Page1.oPag.Cnt.Controls(c).Enabled=.F.
                Endif
            Endif
            If i_cOp = "Query"
                * --- chiave abilitata
                c=This.oPgFrm.Page1.oPag.Cnt.GetCtrl('w_'+This.w_flds[1])
                If c<>0
                    This.oPgFrm.Page1.oPag.Cnt.Controls(c).Enabled=.T.
                Endif
            Endif
        Endwith
    Endproc

    * --- fase di ricerca
    Func ObservedChanged(cName)
        Local i_nConn,i_cFlt,c,x
        If This.cFunction="Query"
            x=This.oPgFrm.Page1.oPag.Cnt.GetVar('w_'+This.w_flds[1])
            If !Empty(x) And (Empty(This.xKey[1]) Or x<>This.xKey[1])
                i_nConn = i_TableProp[this.table_IDX,3]
                i_cFlt = cp_BuildWhere('',x,This.w_flds[1],i_nConn)
                This.QueryKeySet(i_cFlt,'')
                This.LoadRecWarn()
                Return(.T.)
            Endif
        Endif
        Return(.F.)

        * --- Genera il filtro
    Func BuildFilter()
        Local i_cFlt,i_nConn,i
        i_cFlt = ""
        i_nConn = i_TableProp[this.table_IDX,3]
        For i=1 To Alen(This.w_flds)
            If This.oPgFrm.Page1.oPag.Cnt.IsVar('w_'+This.w_flds[i])
                i_cFlt = cp_BuildWhere(i_cFlt,This.oPgFrm.Page1.oPag.Cnt.GetVar('w_'+This.w_flds[i]),This.w_flds[i],i_nConn)
            Endif
        Next
        Return (i_cFlt)
    Endfunc

    * --- Creazione del cursore cKeySet con solo la chiave primaria
    Proc QueryKeySet(i_cWhere,i_cOrderBy)
        Local i_cKey,i_cTable,i_nConn,i_cDatabaseType
        If This.bGetFlds
            This.bGetFlds=.F.
            This.GetTableFields()
        Endif
        i_cWhere = Iif(Not(Empty(i_cWhere)),' where '+i_cWhere,'')
        i_cKey = This.cKeySelect
        *
        i_nConn = i_TableProp[this.table_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.table_IDX,2])
        i_cDatabaseType = i_ServerConn[i_TableProp[this.table_IDX,5],6]
        *
        * select CODALT from altra where ? order by ?
        *
        If i_nConn<>0
            Thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_cWhere+" "+i_cOrderBy,This.cKeySet,i_cDatabaseType)
        Else
            Select &i_cKey From (i_cTable) &i_cWhere &i_cOrderBy Into Cursor (This.cKeySet)
        Endif
    Endproc

    * --- Insert del nuovo Record
    Function mInsert()
        Local i_cKey,i_nConn,i_cTable,i,i_fld,i_upd
        This.GetControlsValue()
        If This.bUpdated
            i_nConn = i_TableProp[this.table_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.table_IDX,2])
            If i_nConn<>0
                i_fld=''
                i_upd=''
                For i=1 To Alen(This.w_flds)
                    i_fld=i_fld+Iif(Empty(i_fld),'',',')+This.w_flds[i]
                    i_upd=i_upd+Iif(Empty(i_upd),'',',')+cp_ToStrODBC(This.w_values[i])
                Next
                =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+;
                    "("+i_fld+") VALUES ("+i_upd+")")
            Else
                i_fld=''
                i_upd=''
                For i=1 To Alen(This.w_flds)
                    i_fld=i_fld+Iif(Empty(i_fld),'',',')+This.w_flds[i]
                    i_upd=i_upd+Iif(Empty(i_upd),'',',')+cp_ToStr(This.w_values[i])
                Next
                cp_CheckDeletedKey(i_cTable,0,This.w_flds[1],This.w_values[1])
                Insert Into (i_cTable);
                    (&i_fld) ;
                    VALUES ;
                    (&i_upd)
            Endif
        Endif
        Return(Not(bTrsErr))

        * --- Aggiornamento del Database
    Function mReplace(i_bEditing)
        Local i,i_upd,i_cWhere,i_NF,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
        This.GetControlsValue()
        If This.bUpdated And i_bEditing
            i_nConn = i_TableProp[this.table_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.table_IDX,2])
            i_NF = This.cCursor
            i_OldCCCHK = Iif(i_bEditing,&i_NF..CPCCCHK,'')
            If i_nConn<>0
                i_upd=''
                For i=2 To Alen(This.w_flds)
                    If Upper(This.w_flds[i])<>'CPCCCHK'
                        i_upd=i_upd+Iif(Empty(i_upd),'',',')+This.w_flds[i]+'='+cp_ToStrODBC(This.w_values[i])
                    Endif
                Next
                i_cWhere = This.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET "+;
                    i_upd+ ;
                    ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
            Else
                i_upd=''
                For i=2 To Alen(This.w_flds)
                    If Upper(This.w_flds[i])<>'CPCCCHK'
                        i_upd=i_upd+Iif(Empty(i_upd),'',',')+This.w_flds[i]+'='+cp_ToStr(This.w_values[i])
                    Endif
                Next
                i_cWhere = This.cKeyWhere
                Update (i_cTable) Set;
                    &i_upd ;
                    ,CPCCCHK=cp_NewCCChk() Where &i_cWhere And CPCCCHK==i_OldCCCHK
                i_nModRow=_Tally
            Endif
            =cp_CheckMultiuser(i_nModRow)
        Endif
        Return(bTrsErr)

        * --- Delete Records
    Function mDelete()
        Local i_cWhere,i_NF,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
        If Not(bTrsErr)
            i_NF = This.cCursor
            i_OldCCCHK = &i_NF..CPCCCHK
            i_nConn = i_TableProp[this.table_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.table_IDX,2])
            If i_nConn<>0
                i_cWhere = This.cKeyWhereODBC
                i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                    " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
            Else
                i_cWhere = This.cKeyWhere
                Delete From (i_cTable) Where &i_cWhere And CPCCCHK==i_OldCCCHK
                i_nModRow=_Tally
            Endif
            =cp_CheckMultiuser(i_nModRow)
        Endif
        This.mDeleteWarnings()
        Return

        * --- Calcoli
    Function mCalc(i_bUpd)
        Return

        * --- Abilita controls condizionati
    Procedure mEnableControls()
        Return

    Function SetControlsValue()
        Local i
        For i=1 To Alen(This.w_flds)
            If This.oPgFrm.Page1.oPag.Cnt.IsVar('w_'+This.w_flds[i])
                This.oPgFrm.Page1.oPag.Cnt.SetVar('w_'+This.w_flds[i],This.w_values[i])
            Endif
        Next
        Return

    Function GetControlsValue()
        Local i,v
        This.bUpdated=.F.
        For i=1 To Alen(This.w_flds)
            If This.oPgFrm.Page1.oPag.Cnt.IsVar('w_'+This.w_flds[i])
                v=This.oPgFrm.Page1.oPag.Cnt.GetVar('w_'+This.w_flds[i])
                If !This.bUpdated And v<>This.w_values[i]
                    If Type("v")<>'C' Or !Trim(v)==Trim(This.w_values[i])
                        This.bUpdated=.T.
                    Endif
                Endif
                This.w_values[i]=v
            Endif
        Next
        Return

Enddefine

* --- Definizione delle pagine come container
Define Class tCPIG_TABPag1 As StdContainer
    Width  = 632
    Height = 64+35
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
Enddefine

