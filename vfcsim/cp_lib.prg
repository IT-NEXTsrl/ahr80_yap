* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_LIB
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 08/04/98
* Traduzione    : 01/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Libreria di funzioni
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Proc cp_ErrorMsg(i_cMsg,i_xIcon,i_cTitle,i_lTranslate)
    i_lTranslate=Iif(Parameters()<4,.T.,i_lTranslate)
    If Type("i_NOMESSAGE")='L' And i_NOMESSAGE
        Return
    Endif
    *** Zucchetti INIZIO - gestione messaggi per scehdulatore
    If (Type('g_SCHEDULER')='C' And g_SCHEDULER='S')
        g_MSG=g_MSG+i_cMsg+Chr(13)+Chr(10)
        Return
    Endif
    *** Zucchetti FINE - gestione messaggi per scehdulatore
    *--- Zucchetti Aulla Inizio - gestione messaggi per AH_Service
    If Type('g_Service')='C' And g_Service='S'
        If i_codute=0 && Login non ancora effettuata
        	WriteLog(i_cMsg)
        	If i_bSrvTrusted && Se la login non � Trusted mostra cmq il messaggio
        		Return
        	Endif
        Else
        	g_MSG=g_MSG+i_cMsg+Chr(13)+Chr(10)
        	Return
        Endif
    Endif
    *--- Zucchetti Aulla Fine - gestione messaggi per AH_Service
    Do Case
        Case Type("i_xIcon")='C'
            Do Case
                Case Lower(i_xIcon)='stop'
                    i_xIcon=16
                Case i_xIcon='?'
                    i_xIcon=32
                Case i_xIcon='!'
                    i_xIcon=48
                Case i_xIcon='i'
                    i_xIcon=64
                Otherwise
                    i_xIcon=48
            Endcase
        Case Type("i_xIcon")='N'
            i_xIcon=Iif(i_xIcon=0, 48, i_xIcon)
        Otherwise
            i_xIcon=48
    Endcase
    i_cTitle=Iif(Parameters()>2 And Not Empty(i_cTitle) ,i_cTitle,i_MsgTitle)
    * --- Messaggio di errore con conferma
    If Type('i_ErrorMessage')='C'
        i_cMsg=Strtran(i_cMsg,"%ERRORMESSAGE%",i_ErrorMessage)
    Endif
    If Not (Type('g_UserScheduler')='L' And g_UserScheduler)
        *--- Zucchetti Aulla Inizio - [Gestione errori molto lunghi]
        Local cMsg, xIcon, cTitle, oShowMessagesMsk
        cMsg = Iif(i_lTranslate,cp_Translate(i_cMsg),i_cMsg)
        cTitle = Iif(Empty(i_cTitle),cp_Translate(MSG_ERROR),cp_Translate(i_cTitle))
        If !(Type("g_bNeverCustomMessagebox")<>'U' And g_bNeverCustomMessagebox) And ((Type("g_bAlwaysCustomMessagebox")<>'U' And g_bAlwaysCustomMessagebox) Or Len(m.cMsg)>1024 Or (Occurs(Chr(10)+Chr(13),m.cMsg)>30 Or Occurs(Chr(13)+Chr(10),m.cMsg)>30 Or Occurs(Chr(10),m.cMsg)>30 Or Occurs(Chr(13),m.cMsg)>30) )
            Dimension L_ArrParameters(3)
            L_ArrParameters(1) = m.cMsg
            L_ArrParameters(2) = m.cTitle
            L_ArrParameters(3) = m.i_xIcon
            oShowMessagesMsk = cp_showmessages(@L_ArrParameters)
            Release L_ArrParameters
            If !(Vartype(m.oShowMessagesMsk)='O' And m.oShowMessagesMsk.bSec1)
                Messagebox(m.cMsg, m.i_xIcon, m.cTitle)
            Endif
            oShowMessagesMsk = .Null.
        Else
            Messagebox(m.cMsg, m.i_xIcon, m.cTitle)
        Endif
        *--- Zucchetti Aulla Fine - [Gestione errori molto lunghi]
    Endif
    Return

    *--- Se p_NoBtnSel = .t. la messagebox avr� il bottone No selezionato
Func cp_YesNo(i_cMsg,i_xIcon,i_lTranslate,p_NoBtnSel,p_MsgTitle)
    i_lTranslate=Iif(Parameters()<3,.T.,i_lTranslate)
    * --- Dialog box con opzioni si/no, torna TRUE se si e' scelto si
    Local i_ret
    If Type("i_NOMESSAGE")='L' And i_NOMESSAGE
        Return(!p_NoBtnSel)
    Endif
    *** Zucchetti INIZIO - gestione messaggi per schedulatore
    If (Type('g_SCHEDULER')='C' And g_SCHEDULER='S') Or (Type('g_Service')='C' And g_Service='S')
        g_MSG=g_MSG+i_cMsg+Iif(p_NoBtnSel,' No',' Yes')+Chr(13)+Chr(10)
        Return(!p_NoBtnSel)
    Endif
    *** Zucchetti FINE - gestione messaggi per schedulatore
    Do Case
        Case Type("i_xIcon")='C'
            Do Case
                Case Lower(i_xIcon)='stop'
                    i_xIcon=16
                Case i_xIcon='?'
                    i_xIcon=32
                Case i_xIcon='!'
                    i_xIcon=48
                Otherwise
                    i_xIcon=32
            Endcase
        Case Vartype(i_xIcon)='N'
            * non fa nulla
        Otherwise
            i_xIcon=32
    Endcase
    If Not (Type('g_UserScheduler')='L' And g_UserScheduler)
        *--- Zuchetti Aulla Inizio - Gestione bottone No Messagebox
        *i_r=MessageBox(iif(i_lTranslate,cp_Translate(i_cMsg),i_cMsg),4+i_xIcon,iif(type("i_msgtitle")='C',cp_Translate(i_msgtitle),''))
        *--- Zuchetti Aulla Inizio - Gestione parametro p_MsgTitle (era scambiato con i_MsgTitle)
        i_r=Messagebox(Iif(i_lTranslate,cp_Translate(i_cMsg),i_cMsg),4+i_xIcon+Iif(p_NoBtnSel,256,0),Iif(Vartype(p_MsgTitle)='C',cp_Translate(p_MsgTitle),Iif(Vartype(i_MsgTitle)='C',cp_Translate(i_MsgTitle),'')))
        *--- Zuchetti Aulla Fine - Gestione parametro p_MsgTitle
        *--- Zuchetti Aulla Inizio - Gestione bottone No Messagebox
    Else
        i_r=6
    Endif
    *** Zucchetti Fine - parametrizzazione bitmap message
    Return(i_r=6)

Function cp_ConfermicancellazionePostINselezionati()
    Return(cp_YesNo(MSG_POSTIN_DELETE_FROM_FOLDER))

Function cp_WarningMsg(i_cMsg)
    Return cp_YesNo(cp_Translate(i_cMsg)+". "+cp_Translate(MSG_ACCEPT_ANYWAY),32,.F.)
Endfunc

Proc cp_Msg(i_cMsg,i_lNowait,i_lNoclear,i_nTimeout,i_lTranslate)
    Local i_cWait, i_oWWMonitor
    *--- Zucchetti Aulla Inizio - Il servizio non deve visualizzare wait window (ma le registra sul log)
    If Type('g_Service')='C' And g_Service='S'
    	WriteLog(i_cMsg)
        Return
    Endif
    *--- Zucchetti Aulla Fine - Il servizio non deve visualizzare wait window (ma le registra sul log)
    * --- messaggio senza conferma
    i_lNowait=Iif(Parameters()<2,.T.,i_lNowait)
    If Type("i_NOMESSAGE")='L' And i_NOMESSAGE
        i_lNowait=.T.
    Endif
    *** Zucchetti INIZIO - gestione messaggi per schedulatore
    If Type('g_SCHEDULER')='C' And g_SCHEDULER='S'
        i_lNowait=.T.
    Endif
    *** Zucchetti FINE - gestione messaggi per schedulatore

    * --- Zucchetti Aulla - Test_Vqr (se impossibile aprire tabella non d� l'avviso)
    If Type("g_testvar")<>'C'
        *--- Disabilito gli alert
        If .T. Or (Vartype(g_Alertmanager)<>'O' Or ( Vartype(g_DisableNewWaitWind)='L' And g_DisableNewWaitWind))
            If i_bWaitWindowTheme And i_VisualTheme<>-1
                i_oWWMonitor= Createobject("cp_WaitWindow")
                i_oWWMonitor.Initialize()
                Wait Clear
            Endif
            i_cWait='wait window (iif(i_lTranslate,cp_Translate(i_cMsg),i_cMsg))'+Iif(i_lNowait,' Nowait','')+Iif(i_lNoclear,' NOCLEAR','')+Iif(Type('i_nTimeout')='N',' TIMEOUT i_nTimeout','')
            &i_cWait
            i_oWWMonitor = .Null.
        Else
            With g_Alertmanager.WaitAlert
                m.i_cMsg = Iif(Vartype(m.i_cMsg)="C", m.i_cMsg, "" )
                m.i_lNowait = Iif(Vartype(m.i_lNowait)="L", m.i_lNowait, .T. )
                m.i_lNoclear = Iif(Vartype(m.i_lNoclear)="L", m.i_lNoclear, .F. )
                m.i_nTimeout = Iif(Vartype(m.i_nTimeout)="N", m.i_nTimeout, 0 )
                m.i_lTranslate = Iif(Vartype(m.i_lTranslate)="L", m.i_lTranslate, .T. )
                .TextAssign( Iif( m.i_lTranslate , cp_Translate(m.i_cMsg), m.i_cMsg) )
                .bNoWait = m.i_lNowait
                .bNoClear = m.i_lNoclear
                .nTimeOut = m.i_nTimeout
                .DoAlert()
            Endwith
        Endif
    Else
        g_testvar=cp_Translate(i_cMsg)
    Endif

    Return

    * Costante passata a RegCreateKeyEx
    #Define SECURITY_ACCESS_MASK  983103
    #Define HKEY_LOCAL_MACHINE 2147483650

Proc cp_help()
    Local i_cPathHelp,nKeyHandle,ret,nKeyPos
    nKeyHandle = 0
    nKeyPos = 0
    ret = 0
    If Type('i_PathHelp')='C'
        i_cPathHelp=i_PathHelp
    Else
        i_cPathHelp=''
    Endif

    *Verifico presenza del file *.chm; se presente utilizzo quest'ultimo
    If !Isnull(i_curform)
        If cp_fileexist(i_cPathHelp+i_curform.GetHelpFile()+'.chm',.T.)
            i_HelpExt ='.chm'
        Endif
    Else
        If cp_fileexist(i_cPathHelp+i_CpDic+'.chm',.T.)
            i_HelpExt ='.chm'
        Endif
    Endif

    *Verifico dove si trovano gli help
    If i_HelpExt='.chm' And (Drivetype(Sys(5)+i_cPathHelp) = 4 Or ('\\' $ Sys(5) And Not("'"+Alltrim(Substr(Sys(0),1,At(Chr(35),Sys(0))-1))+"'" $ Sys(5))))
        *Verifico la presenza della chiave nel registro di sistema
        ret=RegOpenKey(HKEY_LOCAL_MACHINE,"SOFTWARE\Microsoft\HTMLHelp\1.x\ItssRestrictions",@nKeyHandle)
        If ret <> 0
            *Non esiste la chiave, devo crearla
            ret=RegCreateKeyEx(HKEY_LOCAL_MACHINE,"SOFTWARE\Microsoft\HTMLHelp\1.x\ItssRestrictions",0,'',0,SECURITY_ACCESS_MASK,0,@nKeyHandle,@nKeyPos)
            If ret = 0
                *Assegno il valore MaxAllowedZone (impostato di default a 3), alla chiave
                ret=RegSetValueEx(nKeyHandle,"MaxAllowedZone",0,4,NUM2DWORD(3),4)
                If ret <> 0
                    *Si � verificato un errore nella assegnamento del valore alla nuova chiave
                    cp_Msg(cp_Translate(MSG_CANNOT_OPEN_HELP),.F.)
                Endif
            Else
                *Si � verificato un errore durante la creazione della nuova chiave
                cp_Msg(cp_Translate(MSG_CANNOT_CONFIG_REG_FOR_HELP),.F.)
            Endif
        Endif
        RegCloseKey(nKeyHandle)
    Endif

    If !Isnull(i_curform) And Lower(i_curform.Class)<>"vm_frm_exec"
        Local h,T,p
        h=i_cPathHelp+i_curform.GetHelpFile()
        T=i_curform.GetHelpTopic()
        T=Chrtran(T,'()','[]')
        If i_HelpExt = '.hlp' And !cp_fileexist(h+'.hlp',.T.)
            *Se non esiste il file .hlp e non sono in Vista forzo il formato del help a .chm
            i_HelpExt ='.chm'
        Endif
        Do Case
            Case i_HelpExt ='.chm'
                If cp_fileexist(h+'.chm',.T.)
                    OpenCHM(h+'.chm',T)
                Endif
            Case i_HelpExt ='.hlp'
                If cp_fileexist(h+'.hlp',.T.)
                    Set Help To (h)
                    Help &T
                Endif
        Endcase
    Else
        If cp_fileexist(i_cPathHelp+i_CpDic+'.hlp',.T.) Or cp_fileexist(i_cPathHelp+i_CpDic+'.chm',.T.)
            If i_HelpExt ='.hlp' And !cp_fileexist(i_cPathHelp+i_CpDic+'.hlp',.T.)
                *Se non esiste il file .hlp e non sono in Vista forzo il formato del help a .chm
                i_HelpExt ='.chm'
            Endif
            If i_HelpExt='.chm'
                OpenCHM(i_cPathHelp+i_CpDic+'.chm')
            Else
                Set Help To (i_cPathHelp+i_CpDic)
                Help Id 0
            Endif
        Else
            Set Help To
        Endif
    Endif
    Return

    * Zucchetti Aulla inizio (Funzioni per cp_help)
    #Define GMEM_FIXED  0
    #Define HH_KEYWORD_LOOKUP 13
    #Define HH_DISPLAY_TOC 1

    * typedef struct tagHH_AKLINK
    * {
    *     int      cbStruct
    *     BOOL     fReserved
    *     LPCTSTR  pszKeywords
    *     LPCTSTR  pszUrl
    *     LPCTSTR  pszMsgText
    *     LPCTSTR  pszMsgTitle
    *     LPCTSTR  pszWindow
    *     BOOL     fIndexOnFail
    * } HH_AKLINK

Proc OpenCHM(pathfile, keysStr)
    Declare Integer GlobalAlloc In kernel32 Integer wFlags, Integer dwBytes
    Declare Integer GlobalFree In kernel32 Integer Hmem
    Declare RtlMoveMemory In kernel32 As Str2Mem Integer, String @, Integer
    Declare Long HtmlHelpA In hhctrl.ocx Long, String, Long, String @
    Local HH_AKLINK_struct, pszKeywords, lnBuf, CMD
    * Allocazione memoria per stringa con parametri
    If Vartype(keysStr) <> 'L'
        pszKeywords = keysStr + Chr(0)
        lnBuf = GlobalAlloc(GMEM_FIXED, Len(pszKeywords))
        Str2Mem(lnBuf, @pszKeywords, Len(pszKeywords))
        CMD = HH_KEYWORD_LOOKUP
    Else
        lnBuf = 0
        CMD = HH_DISPLAY_TOC
    Endif
    * Costruzione struttura HH_AKLINK
    HH_AKLINK_struct = NUM2DWORD (0) + NUM2DWORD(lnBuf);
        + Replicate(NUM2DWORD(0), 4) + NUM2DWORD (1)
    HH_AKLINK_struct = NUM2DWORD(Len(HH_AKLINK_struct) + 4) + HH_AKLINK_struct
    * Apertura Help (_VFP.hWnd)
    HtmlHelpA(0, pathfile, CMD , @HH_AKLINK_struct)
    * Libera la memoria usata per la stringa di parametri
    If Vartype(keysStr) <> 'L'
        GlobalFree(lnBuf)
    Endif
Endproc


* Converte un valore numerico nel corrispondente valore DWORD
Function NUM2DWORD
    Lparameters m.tnValue
    #Define k0 0x100
    #Define k1 0x10000
    #Define k2 0x1000000
    If m.tnValue < 0
        m.tnValue = 0x100000000 + m.tnValue
    Endif
    Local lnB0, lnB1, lnB2, lnB3
    lnB3 = Int(m.tnValue/k2)
    lnB2 = Int((m.tnValue - m.lnB3 * k2) / k1)
    lnB1 = Int((m.tnValue - m.lnB3 * k2 - m.lnB2 * k1) / k0)
    lnB0 = Mod(m.tnValue, k0)
    Return Chr(m.lnB0)+Chr(m.lnB1)+Chr(m.lnB2)+Chr(m.lnB3)
    * Zucchetti Aulla fine


Func cp_BuildWhere(i_cCmd,i_xValue,i_cName,i_nConn)
    * --- costruisce un pezzo di clausola where se la variabile che
    * --- viene passata contiene un valore valido
    Local i_t,i_n
    If Not(Empty(i_xValue))
        i_t = Type("i_xValue")
        i_n = ''
        Do Case
            Case i_t='C' .Or. i_t='M'
                If i_nConn<>0
                    * -------- Zucchetti Aulla Inizio - Ricerca da tasto dx (Apri/Modifica)
                    If Vartype(i_ExactSearch)='L' And i_ExactSearch
                        * -- La ricerca avviene attraverso un criterio di uguaglianza
                        i_n = i_cName+'='+cp_ToStrODBC(i_xValue)
                    Else
                        * -- Inserimento iniziale della '%'
                        *i_n = i_cName+" like "+cp_ToStrODBC(trim(i_xValue)+'%')
                        i_n = i_cName+" like "+cp_ToStrODBC(Iif(g_RicPerCont$'TL', '%', '')+Trim(i_xValue)+'%')
                    Endif
                    * -------- Zucchetti Aulla Fine - Ricerca da tasto dx (Apri/Modifica)
                Else
                    If At('%',i_xValue)<>0
                        *i_n = i_cName+" like trim(this.w_"+i_cName+")+'%'"
                        i_n = i_cName+" like "+cp_ToStr(Trim(i_xValue)+'%')
                    Else
                        i_n = i_cName+'='+cp_ToStr(Trim(i_xValue))
                    Endif
                Endif
            Otherwise && case i_t='N'
                If i_nConn<>0
                    i_n = i_cName+'='+cp_ToStrODBC(i_xValue)
                Else
                    *i_n = i_cName+"=this.w_"+i_cName
                    i_n = i_cName+"="+cp_ToStr(i_xValue)
                Endif
        Endcase
        If Not(Empty(i_n))
            i_cCmd=i_cCmd+Iif(Empty(i_cCmd),'',' and ')+i_n
        Endif
    Endif
    Return(i_cCmd)

Func cp_BuildKeyWhere(i_oObj,i_cKey,i_nConn)
    Private i_cCmd,i_olderr
    LOCAL TestMacro
    i_olderr=On('error')
    On Error i_cCmd=''
    * --- cerca di prelevare il filtro di query dall' oggetto
    i_cCmd=i_oObj.cQueryFilter
    On Error &i_olderr
    * --- se l' oggetto non definiva il filtro allora viene lasciato vuoto
    i=At(',',i_cKey)
    Do While i<>0
        N=Left(i_cKey,i-1)
        i_cKey=Substr(i_cKey,i+1)
        i=At(',',i_cKey)
        TestMacro=i_oObj.w_&N
        If !Empty(TestMacro)
            i_cCmd=cp_BuildWhere(i_cCmd,i_oObj.w_&N,N,i_nConn)
        Endif
    Enddo
    N=i_cKey
    i_cCmd=cp_BuildWhere(i_cCmd,i_oObj.w_&N,N,i_nConn)
    Return(i_cCmd)

Function cp_QueryEntityFilter(i_cPrg,i_bAddAnd,i_cTable)
    Private i_cFlt, i_result
    Local i_olderr
    i_olderr=On('error')
    On Error i_cFlt=""
    i_cFlt=""
    i_result=""
    *** nel caso di zoom on zoom con routine non trovo la query filter da applicare
    Do getEntityType In &i_cPrg With i_result
    If (i_result="Routine" Or i_result="Dialog Window" Or Empty(i_cPrg)) And Vartype(i_cTable)='C'
        i_cFlt=cp_AppQueryFilter(i_cPrg,i_cTable,'')
        If !Empty(i_cFlt) And i_bAddAnd
            i_cFlt=' ('+i_cFlt+') and '
        Endif
    Else
        If !Empty(i_cPrg)
            Do QueryFilter In &i_cPrg With i_cFlt
            If !Empty(i_cFlt) And i_bAddAnd
                i_cFlt=' ('+i_cFlt+') and '
            Endif
        Endif
    Endif
    On Error &i_olderr
    Return(i_cFlt)

Proc cp_DoAction(i_cOp)
    If Vartype(i_curform)='U'
        Cancel
    Endif
    If Vartype(i_stopFK)='U'
        Public i_stopFK
    Endif
    Local i_errsav,i_doaction,i_ret
    Private i_err
    If Not(Isnull(i_curform)) And Not(i_stopFK)
        i_stopFK=.T.
        *i_curform.&i_p_op
        *i_stopFK=.f.
        i_errsav=On('ERROR')
        i_err=.F.
        i_doaction=.F.
        On Error i_err=.T.
        i_ret=1
        If !Inlist(i_cOp,"ecpQuit")
            If i_cOp='ecpZoom'
                *i_curform.bDontReportError=.t.
                i_curform.ActiveControl.Parent.oContained.bDontReportError=.T.
                i_curform.ActiveControl.Valid()
            Else
                i_ret=i_curform.ActiveControl.Valid()
                * Problema F10 senza uscire dal control posto in movimentazione: non salvava il valore del campo corrente
                If Lower(i_curform.ActiveControl.Name)='obody' And Type('i_curform.ActiveControl.oBodyCol.oRow.activecontrol')='O' And Inlist(i_curform.cFunction,"Edit","Load")
                    i_curform.ActiveControl.oBodyCol.oRow.ActiveControl.Valid()
                Endif
                * Fine Problema F10 senza uscire dal control posto in movimentazione
            Endif
        Endif
        i_err=Iif(Vartype(i_ret)='N',Iif(i_ret=0,.T.,.F.),Not(i_ret))
        i_doaction=i_curform.HasCPEvents(i_cOp)
        On Error &i_errsav
        If !i_err And i_doaction
            Createobject('DoIndirectAction',i_cOp)
            **I campi memo ed i postin hanno uno zoom proprio
            If Type('i_curform.activecontrol')='O' And Upper(i_curform.ActiveControl.Class)='STDMEMO'  And i_cOp='ecpZoom' And (Type('i_curform.cfunction')='U' Or i_curform.cFunction<>'Query')
                *--- Apertura ZoomOnZoom
                Local l_sec
                *--- Inserito ritardo per evitare che la keyf9 venga eseguita prima che scatti il timer della DoIndirectAction
                *--- altrimenti viene aperto lo ZoomOnZoom
                l_sec = Seconds()
                Do While (Seconds()-l_sec<0.1)
                    *--- Possibile C0000005 con interfaccia nuova
                    *--- Non la posso togliere perch� altrimenti non funzionano
                    *--- le frasi modello su i postin (doppio F9)
                    DoEvents Force
                Enddo
                *i_curform.ActiveControl.keyf9()
            Endif
        Else
            i_stopFK=.F.
        Endif
    Endif
Endproc

Define Class DoIndirectAction As Timer
    oThis=.Null.
	Interval=i_nDoActionDelay
    cOp=''
    Proc Init(i_cOp)
        This.oThis=This
        This.cOp=i_cOp
    Proc Timer()
        This.Interval=0
        Local i_op,i_err,i_sfk
        i_sfk=Createobject('UnstopFK')
        If Not(Isnull(i_curform))
            *i_err=on('ERROR')
            *on error =.t.
            i_op=This.cOp
            Public i_lastindirectaction
            i_lastindirectaction=i_op
            i_curform.&i_op
            *on error &i_err
        Endif
        i_stopFK=.F.
        This.oThis=.Null.
        This.Destroy()
Enddefine

Define Class UnstopFK As Timer
    Interval=200
    Proc Timer()
        This.Interval=0
        i_stopFK=.F.
Enddefine

Func cp_BeginTrs()
    If Vartype(bTrsErr)='U'
        Public bTrsErr
    Endif
    If Vartype(i_TrsMsg)='U'
        Public i_TrsMsg
    Endif
    If Vartype(nTrsErrn)='U'
        Public nTrsErrn
    Endif
    If Vartype(bTrsErrSave)='U'
        Public bTrsErrSave,nTrsConnCnt,nTrsConn
    Endif
    If Vartype(nTrsConnCnt)='U'
        Public nTrsConnCnt
    Endif
    If Vartype(nTrsConn)='U'
        Public nTrsConn
    Endif
    Local i
    If Vartype(bTrsVisualFoxPro)='U'
        Public bTrsVisualFoxPro
        bTrsVisualFoxPro=.F.
        For i=1 To i_nServers
            If i_ServerConn[i,2]=0
                bTrsVisualFoxPro=.T.
            Endif
        Next
    Endif
    *if txnlevel()>0
    *  bTrsErr=.t.
    *  i_TrsMsg='Nested transaction'
    *  nTrsErrn=-2
    *  return
    *endif
    bTrsErr=.F.
    If Vartype(nTrsConnCnt)='L'
        nTrsConnCnt=0  && prima inizializzazione
    Endif
    If nTrsConnCnt<>0
        cp_Msg(cp_Translate(MSG_TRANSACTION_ON_TRANSACTION_QM),.F.)
        bTrsErr=.T.
        i_TrsMsg= 'Nested transaction'
        nTrsErrn=-2
        Return
    Endif
    * --- inizializza le transazioni ODBC
    nTrsConnCnt=1
    Dimension nTrsConn[1]
    nTrsConn[1]=0
    * --- inizializza le transazioni VFP
    If bTrsVisualFoxPro
        Begin Transaction
    Endif
    * --- Prepara la lista delle tabelle temporanee da cancellare a fine transazione (ORACLE sotto transazione esegue una COMMIT se si danno comandi DDL)
    cp_DropOnCloseNewTransaction()
    * ---
    bTrsErrSave=On('ERROR')
    On Error =cp_TrsError(Error(),Message(),Program(),Line())
    *--- Activity log Tracciatura dell'inizio della transazione
    If i_nACTIVATEPROFILER>0
        cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "TBT", "Begin transaction", Iif(Type("i_curform")="C",i_curform,Space(10)), i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""))
    Endif
    *--- Activity log
    Return(.T.)

Func cp_EndTrs(i_bDontReportError)
    Local i,r
    If bTrsErr
        * --- errore, rollback
        For i=1 To nTrsConnCnt
            If nTrsConn[i]<>0
                r=Sqlrollback(nTrsConn[i])
            Endif
        Next
        * --- chiude le transazioni VFP
        If bTrsVisualFoxPro
            Rollback
        Endif
        If !i_bDontReportError
            =cp_TranslateDBMessage(i_TrsMsg)
        Endif
        *--- Activity log Tracciatura della rollback della transazione
        If i_nACTIVATEPROFILER>0
            cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "TRT", "Rollback", Iif(Type("i_curform")="C",i_curform,Space(10)), i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""))
        Endif
        *--- Activity log
    Else
        * --- tutto Ok, commit
        For i=1 To nTrsConnCnt
            If nTrsConn[i]<>0
                r=Sqlcommit(nTrsConn[i])
            Endif
        Next
        * --- chiude le transazioni VFP
        If bTrsVisualFoxPro
            End Transaction
        Endif
        *--- Activity log Tracciatura della commit della transazione
        If i_nACTIVATEPROFILER>0
            cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "TCT", "Commit", Iif(Type("i_curform")="C",i_curform,Space(10)), i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""))
        Endif
        *--- Activity log
    Endif
    * --- ripristina le transazioni automatiche
    For i=1 To nTrsConnCnt
        If nTrsConn[i]<>0
            =SQLSetprop(nTrsConn[i],"Transactions",1)
        Endif
    Next
    nTrsConnCnt=0
    * --- chiude gli eventuali server che sono stati aperti durante la transazione
    For i=2 To i_nServers
        cp_CloseOneServer(i)
    Next
    * --- ripristina la gestione degli errori
    On Error &bTrsErrSave
    * --- Cancella le tabelle temporanee "accodate"
    cp_DropOnCloseCloseTransaction()
    Return(.T.)

Func cp_TrsError(nErr,cMsg,cPrg,nLine)
    If Not(bTrsErr)
        bTrsErr=.T.
        nTrsErrn=nErr
        i_TrsMsg=cMsg+Iif(!Empty(cPrg),'[prg='+cPrg+',line='+Iif(Empty(nLine),'0',Alltrim(Str(nLine)))+']','')
        ***** Zucchetti Aulla Inizio
        * Se attivato il DebugMode la procedura mostra tutti gli errori occorsi durante la
        * sua elaborazione
        If Type('g_DEBUGMODE')='N'
            Do Case
                Case g_DEBUGMODE=1
                    cp_Msg(Left(i_TrsMsg,254),.T.)
                Case g_DEBUGMODE=2
                    cp_Msg(Left(i_TrsMsg,254),.F.)
            Endcase
            * Copia il contenuto del messaggio sulla ClipBoard
            _Cliptext=i_TrsMsg
        Endif
        i_TrsMsg=cp_TranslateDBMessage(i_TrsMsg,.T.)
        * Zucchetti Aulla Fine
    Endif
    Return(.T.)

    * Aggiunto parametro cOnlyTranslate da utilizzare passandolo a .t. se vogliamo che
    * la funzione restituisca il messaggio trasformato piuttosto ch� il messaggio a video
Func cp_TranslateDBMessage(cMsg,cOnlyTranslate)
    *cp_dbg(cMsg)
    * Messaggio in caso di errore sotto transazione
    * che non rispetta il CASE
    Local L_Msg
    L_Msg=cMsg
    * che non rispetta il CASE
    If Empty(cMsg)
        cMsg=MSG_UNDEFINED
    Else
        * In MSSQL2005 i messaggi di errore contengono i doppi apici dove sql 2000 ha i singoli apici
        cMsg=Strtran(Lower(cMsg),'"',"'")
        Do Case
                **************** Viene inserita la cp_Translate perch� la cp_ErrorMsg ha come quarto parametro .f. e quindi non traduce)
            Case L_Msg=MSG_RECORD_CHANGED_BY_ANOTHER_USER
                cMsg=cp_Translate(MSG_RECORD_CHANGED_BY_ANOTHER_USER)
            Case "unique"$cMsg Or "unici"$cMsg Or "duplicat"$cMsg
                cMsg=cp_Translate(MSG_KEY_DUPLICATED)
            Case "validation rule"$cMsg Or "check constraint"$cMsg
                cMsg=cp_Translate(MSG_DATABASE_REFUSES_DATA)
            Case "permission denied"$cMsg
                cMsg=cp_Translate(MSG_ACCESS_DENIED)
            Case "communication"$cMsg
                cMsg=cp_Translate(MSG_COMMUNICATION_PROBLEMS)
            Case ("delete"$cMsg Or "integri"$cMsg Or "update"$cMsg Or "cancel"$cMsg Or "insert"$cMsg) And ("constraint"$cMsg Or "restri"$cMsg Or "reference"$cMsg Or "vincol"$cMsg Or "referenz"$cMsg)
                Private CanRead,a,b,c,d,cTable,cTableNew,nTestVirg,Idx_Tbl,cField,Idx_Fld, cTableDescr,cTableDescr_app
                Private cTableDescr_new,cFieldDescr,cFieldDescr_app,cChartofind,iIntToFind,cIntToFind,cStrToSearch,s,v,NextUnd
                * --- mi segnala se sono in grado di decifrare il msg d'errore...
                CanRead=.F.
                cTableDescr=""
                cFieldDescr=""
                cChartofind='fk_'
                * non case sensitive
                a=Atc(cChartofind, Lower(cMsg))
                b=cMsg
                * ---
                If a>0
                    * --- cerco l'underscore successivo..
                    If CP_DBTYPE='Oracle'
                        c = At("_",Substr(cMsg,a+3),2)
                        c = Iif(c>0,c,At("_",Substr(cMsg,a+3)))
                        cTable = Substr(cMsg,a+3,c-1)
                    Else
                        c = At("'",Substr(cMsg,a+3))
                        NextUnd=Rat("_",Substr(cMsg,a+3,c))
                        cTable = Substr(cMsg,a+3,NextUnd-1)
                    Endif
                    *d=substr(b,a+3,((len(b)-a)-3))
                    d=cTable
                    * --- Decurto il codice azienda..
                    *--- cTable potrebbe essere xxxTable oppure Tablexxx, devo eliminare il codice azienda xxx
                    nLenAzi = Len(Alltrim(i_CODAZI))
                    If Upper(Left(cTable, nLenAzi)) == Upper(Alltrim(i_CODAZI))
                        cTable = Upper(Substr(cTable, nLenAzi+1))
                    Else
                        If Upper(Right(cTable, nLenAzi)) == Upper(Alltrim(i_CODAZI))
                            cTable = Upper(Left(cTable, Len(cTable)-nLenAzi))
                        Endif
                    Endif
                    * ---TROVO IL NUMERO DEL CONSTRAINT COIVOLTO:oracle/sqlserv
                    If CP_DBTYPE='Oracle'
                        cStrToSearch=b
                    Else
                        s=At(cChartofind,cMsg)
                        v=At("'",Substr(cMsg,s+1))
                        cStrToSearch=Substr(cMsg,(s+1),v)
                    Endif
                    cIntToFind=Substr(cStrToSearch,(Rat("_",cStrToSearch)+1),2)
                    If !Empty(cIntToFind)
                        * --- indici in doppia cifra
                        If ")" $cIntToFind Or "'" $cIntToFind
                            cIntToFind=Left(cIntToFind,1)
                        Endif
                        iIntToFind=Val(cIntToFind)
                    Endif

                    If "figli"$cMsg Or "child"$cMsg Or "delete"$cMsg
                        * --- siamo in delete
                        Idx_Tbl = i_Dcx.GetTableIdx(cTable)
                        If Idx_Tbl=0
                            * --- Se non lo trovo potrebbe essere un archivio multi azienda quindi
                            * --- re inserisco nel nome la parte eliminata
                            cTable = Upper(Left(d,c-1) )
                            Idx_Tbl = i_Dcx.GetTableIdx(cTable)
                        Endif
                        * --- determino la descrizione
                        If Idx_Tbl<>0
                            cTableDescr = i_Dcx.GetTableDescr( Idx_Tbl )
                            CanRead=.T.
                            *--- Zuchetti Aulla Inizio - Sincronizzazione
                            * Nel caso in cui l'aggiornamento sul DB coinvolga un'azienda<>i_CODAZI
                            * valorizzo la descrizione con il nome fisico della tabella
                        Else
                            cTableDescr = cTable
                            *--- Zuchetti Aulla Fine - Sincronizzazione
                        Endif
                        If Vartype(iIntToFind)="N" And iIntToFind>0
                            cTableDescr_app=i_Dcx.GetFKPhTable(cTable,iIntToFind,i_CODAZI)
                            cTableDescr_new=Upper( Substr(cTableDescr_app,Len(Alltrim(i_CODAZI))+1))
                            cFieldDescr_app=i_Dcx.GetFKKey(cTable,iIntToFind)
                            nTestVirg=At(",",cFieldDescr_app)
                            * --- chiave composta
                            If nTestVirg <>0
                                cFieldDescr=i_Dcx.FieldsToDescr(Idx_Tbl,(cFieldDescr_app))
                            Else
                                Idx_Fld = i_Dcx.GetFieldIdx(cTable,cFieldDescr_app)
                                If Idx_Fld<>0
                                    cFieldDescr = i_Dcx.GetFieldDescr(cTable,Idx_Fld)
                                Endif
                            Endif

                            CanRead=.T.
                        Endif
                        * --- siamo in insert
                    Else

                        If Vartype(iIntToFind)<>"U" And iIntToFind>0
                            cTableDescr_app=i_Dcx.GetFKPhTable(cTable,iIntToFind,i_CODAZI)
                            cTableNew = Upper( Substr(cTableDescr_app,Len(Alltrim(i_CODAZI))+1))
                            Idx_Tbl = i_Dcx.GetTableIdx(cTableNew)
                            If Idx_Tbl<>0
                                cTableDescr = i_Dcx.GetTableDescr( Idx_Tbl )
                                CanRead=.T.
                            Endif
                            If Vartype(iIntToFind)<>"U" And iIntToFind>0
                                cTableDescr_app=i_Dcx.GetFKPhTable(cTable,iIntToFind,i_CODAZI)
                                cTableDescr_new=Upper( Substr(cTableDescr_app,Len(Alltrim(i_CODAZI))+1))
                                cFieldDescr_app=i_Dcx.GetFKExtKey(cTable,iIntToFind)
                                nTestVirg=At(",",cFieldDescr_app)
                                * --- chiave composta
                                If nTestVirg <>0 And Idx_Tbl<>0
                                    cFieldDescr=i_Dcx.FieldsToDescr(Idx_Tbl,(cFieldDescr_app))
                                Else
                                    Idx_Fld = i_Dcx.GetFieldIdx(cTableDescr_new,cFieldDescr_app)
                                    If Idx_Fld<>0
                                        cFieldDescr = i_Dcx.GetFieldDescr(cTableDescr_new,Idx_Fld)
                                    Endif
                                Endif
                            Endif
                        Endif
                        *--DETTAGLI CAMPO
                    Endif
                Endif
                If CanRead
                    b= Iif(Empty(cFieldDescr), cp_msgformat(MSG_TABLE_CL__,cTableDescr), cp_msgformat(MSG_TABLE_CL__FIELD_CL__,cTableDescr,cFieldDescr))
                    * --- Zucchetti Aulla Inizio - Gestione messagistica Import database contiene l'elenco dei campi che contengono i dati errati
                    If Vartype(g_IMPOFLD)='C' And iIntToFind>0
                        g_IMPOFLD=i_Dcx.GetFKKey(cTable,iIntToFind)
                    Endif
                    * --- Zucchetti Aulla Fine
                Else
                    * --- non l'ho trovato, msg generico
                    b= cp_msgformat(MSG_TABLE_CL__,Substr(cMsg, 30))
                Endif
                *--- Zuchetti Aulla Inizio - Sincronizzazione
                **** Se � vuoto il nome della tabella lo recupero nuovamente dal messaggio originale
                If Occurs('[]',b)=1
                    If Occurs("'",cMsg)>0
                        tbname = At("'",Substr(cMsg,a+3))
                        NextUnd=Rat("_",Substr(cMsg,a+3,c))
                        tbname = Substr(cMsg,a+3,NextUnd-1)
                    Else
                        c = At("_",Substr(cMsg,a+3),2)
                        c = Iif(c>0,c,At("_",Substr(cMsg,a+3)))
                        tbname = Substr(cMsg,a+3,c-1)
                    Endif
                    b=Strtran(b,'[]','['+tbname+']')
                Endif
                *--- Zuchetti Aulla Fine - Sincronizzazione
                * b � gi� tradotto quindi viene usato come parametro
                If "figli"$cMsg Or "child"$cMsg Or "delete"$cMsg
                    cMsg=cp_Translate(MSG_RECORD_ALREADY_EXISTS_IN_OTHER_TABLES_C_CANNOT_DELETE)+Chr(13)+b
                Else
                    cMsg= cp_msgformat(MSG_RELATIONSHIP_WITH__IS_WRONG_C_CANNOT_UPDATE,b)
                Endif
                * --- accodiamo il messaggio del RDBMS oppure no a seconda di quanto settato nel .cnf
                If Vartype(g_AttivaMessDb)='L' And g_AttivaMessDb
                    cMsg=cMsg +Chr(13)+Chr(13)+i_TrsMsg
                Endif
            Otherwise
                cMsg=cp_msgformat(MSG_ERROR_CL__,L_Msg)
        Endcase
    Endif
    If Vartype(cOnlyTranslate)='L' And cOnlyTranslate
        Return cMsg
    Else
        * la cp_errormsg con parametro .f. non traduce
        cp_ErrorMsg(cMsg,'','',.F.)
    Endif
    Return(.T.)

Func cp_CheckMultiuser(i_nModRow)
    * controlla se il numero di righe variate e' >= a 1, se non sono
    * state variate delle righe, allora c'e' conflitto in mutiutenza
    If Not(bTrsErr) And (i_nModRow<1)
        =cp_TrsError(-1,MSG_RECORD_CHANGED_BY_ANOTHER_USER)
    Endif
    Return(.T.)

    *--- Zucchetti Aulla Inizio - in Postgres il carattere escape '\' deve essere raddoppiato nella like
    *--- con l'impostazione standard_conforming_strings=on (se off (non usato in adhoc) dovrebbe essere quadruplicato)
Func ah_PosLastP(ctext As String, cPar) As Integer
    * Questa funzione restituisce la posizione dell'ultima parentesi di chiusura
    Local cpAP,cpCH,rpos,nlen,npar,i_char
    If Vartype(cPar)="C" And (cPar="[" Or cPar="]")
        cpAP="["
        cpCH="]"
    Else
        cpAP="("
        cpCH=")"
    Endif
    rpos=At(cpAP,ctext)
    nlen=Len(ctext)
    npar=Iif(rpos>0,1,0)
    Do While npar>0 And rpos<=nlen
        rpos=rpos+1
        i_char=Substr(ctext,rpos,1)
        npar=npar+Icase(i_char=cpAP,1,i_char=cpCH,-1,0)
    Enddo
    Return rpos
Endfunc

Func cp_SQL_like(cSelect As String, dbconn) As String
    * In PostgreSQL sostituisco like con ilike per fare la ricerca case-insensitive
    * Inoltre � necessario il carattere escape '\' deve essere raddoppiato
    Local i_cLikeSelect,ctdbconn,i_use_ilike
    ctdbconn=Vartype(dbconn)
    If ctdbconn="L" And Not dbconn
        * se dbconn=.f. return immediata
        Return cSelect
    Endif
    i_use_ilike=Vartype(g_USE_ILIKE)="C" And g_USE_ILIKE='S'
    If (i_use_ilike Or "\" $ cSelect) And " like " $ Lower(cSelect) And " where " $ Lower(cSelect)
        If ctdbconn="N"
            * se dbconn=handle connessione, cerco il tipo di database
            dbconn=SQLXGetDatabaseType(dbconn)
            ctdbconn="C"
        Endif
        If ctdbconn<>"C" Or dbconn<>"PostgreSQL"
            * se dbconn<>"PostgreSQL" o parametro non valido, return immediata
            Return cSelect
        Endif
        Dimension cConst(1)
        i_cLikeSelect = cp_encode_str_const(cSelect, @cConst)
        If Not Empty(i_cLikeSelect)
            cSelect = cp_decode_str_const(i_cLikeSelect, @cConst, .T.)
        Endif
    Endif
    Return cSelect
Endfunc

Function cp_encode_str_const(cexpr As String, cConst) As String
    * questa funzione elimina le costanti stringa racchiuse in una frase SQL � le codifica nell'array aConst
    * la funzione inversa per ricostrostruire la frase sql originale � cp_encode_str_const
    Local i_char,i_conc,i_cwork,csxarg,cdxarg,i_cSearch,i_pc,i_pl,i_pr,plen,i_np,i_cOK,i_nAp,i_pa,i_paa,nindc,nOccur,i_cCost,nindc,i_pfi
    nindc = 0
    i_cwork = cexpr
    i_nAp=Occurs("'",i_cwork)
    If i_nAp<=1
        Return ''
    Else
        Dimension cConst(Int(i_nAp/2))
        i_pa=At("'",i_cwork)
        Do While i_pa>0
            i_cCost="'"
            crest=Substr(i_cwork,i_pa+1)
            i_paa=At("''",crest)
            i_pa=At("'",crest)
            Do While i_paa>0 And i_paa<=i_pa
                i_cCost=i_cCost+Left(crest,i_paa)+"'"
                crest=Substr(crest,i_paa+2)
                i_paa=At("''",crest)
                i_pa=At("'",crest)
            Enddo
            i_pfi=At("'",crest)
            If i_pfi=0
                * errore nella stringa
                Return ''
            Else
                i_cCost=i_cCost+Left(crest,i_pfi)
                nindc=nindc+1
                cConst[nindc]=i_cCost
                i_cwork=Strtran(i_cwork,i_cCost,Chr(254)+Alltrim(Str(nindc,3,0))+Chr(254),1,1)
            Endif
            i_pa=At("'",i_cwork)
        Enddo
    Endif
    Return i_cwork
Endfunc

Function cp_decode_str_const(cexpr As String, cConst, bFromSqlLike ) As String
    * questa funzione � la funzione inversa della cp_encode_str_const
    * e serve per ricostrostruire le costanti stringa della frase sql originale
    Local i_cwork,i_pa,nindc,i_nLike,noccurslike,poslike
    i_cwork = cexpr
    nindc = Alen(cConst)
    If nindc>0
        If m.bFromSqlLike &&sostituzione like e caratteri speciali
            cMatchlike=" like "
            nLenml=Len(cMatchlike)
            poslike=At(cMatchlike,Lower(i_cwork))
            noccurslike=1
            Do While poslike>0
                i_nLike=Val(Strextract(Substr(i_cwork,poslike),Chr(254),Chr(254)))
                * nella like di Postgres \ � un carattere speciale e deve essere sostituito con \\
                cConst[i_nLike] = Strtran(cConst[i_nLike],"\","\\")
                noccurslike=noccurslike+1
                poslike=At(cMatchlike,Lower(i_cwork),noccurslike)
            Enddo
            If Vartype(g_USE_ILIKE)="C" And g_USE_ILIKE='S'
                i_cwork = Strtran(i_cwork, cMatchlike," ilike ",1,-1,1)
            Endif
        Endif
        For i_pa=1 To nindc
            If Not Empty(cConst[i_pa])
                i_cwork=Strtran(i_cwork,Chr(254)+Alltrim(Str(i_pa,3,0))+Chr(254),cConst[i_pa])
            Endif
        Next
    Endif
    Return i_cwork
Endfunc


*--- Zucchetti Aulla Fine - in Postgres il carattere escape '\' deve essere raddoppiato nella like

*--- Zucchetti Aulla Inizio - Gestione automatica campi UTCC,UTCV,UTDC,UTCV
Func ah_SQL_UTFLD(i_cCmd As String) As String
    Local i_pos,cfields,cReplacefields,cfieldsReplaced,cReplacevalues,cvaluesReplaced,cUTCC,cUTCV,cUTDC,cUTCV,IniPos,FinPos,cLwrcmd,cTmpTbl
    Store "" To cfields,cReplacefields,cfieldsReplaced,cReplacevalues,cvaluesReplaced,cUTCC,cUTCV,cUTDC,cUTCV
    cLwrcmd = Lower(i_cCmd)
    If Left(cLwrcmd,6)="update"
        If " from cptmp_" $ cLwrcmd
            cTmpTbl = " from cptmp_"
        Else
            IniPos = At(" from " , cLwrcmd)
            FinPos = At("cptmp_" , cLwrcmd)
            If IniPos>0 And FinPos>0 And IniPos<FinPos And FinPos-IniPos<35
                cTmpTbl = Substr(cLwrcmd,IniPos,FinPos-IniPos+7)
            Else
                cTmpTbl = " from cptmp_"
            Endif
        Endif
        If cTmpTbl $ cLwrcmd
            *---  Update da tabella temporanea: verifico se devo aggiungere i campi UTDV,UTCV
            i_cTable=Alltrim(Lower(Substr(i_cCmd, At(" ",i_cCmd),At(" ",i_cCmd,2)-At(" ",i_cCmd)))) && tra primo e secondo spazio
            riftable=Iif(i_cTable=Lower(Alltrim(i_CODAZI)),Substr(i_cTable,Len(Alltrim(i_CODAZI))+1),i_cTable)
            i_i=Iif(Empty(riftable),0,Ascan(i_TableProp, riftable, -1, -1, 1, 14))
            Store "" To cUTCV,cUTDV,cUTCVvalue,cUTDVvalue
            * Nel comportamento standard (g_UPDSYSFLD="DEF" oppure g_UPDSYSFLD non definita) l'utente schedulatore non aggiorna i campi
            bUTDV = TYPE("g_SCHEDULER")<>"C" OR g_SCHEDULER<>"S"
            bUTDV = IIF(TYPE("g_UPDSYSFLD")="C", g_UPDSYSFLD="ON" OR g_UPDSYSFLD="DEF" and m.bUTDV, m.bUTDV)
            bUTDV = m.bUTDV And ",cpccchk" $ cLwrcmd And i_i>0 And i_TableProp[i_i,8]=1
            If m.bUTDV
                i_pos=At(",cpccchk) = (select ", cLwrcmd)
                If Occurs(cTmpTbl,i_cCmd)>1 And i_pos>1
                    cfields=Left(i_cCmd, i_pos+10)
                    cValues=Substr(i_cCmd, i_pos+11)
                    cValues=Left(cValues, At(cTmpTbl,cValues)-1)
                    cReplacefields=cfields
                    cfieldsReplaced=cfields
                    cfields=Upper(cfields)
                    If Not ",UTCV," $ cfields And Not "(UTCV)" $ cfields And Not ",UTCV)" $ cfields And Not "(UTCV," $ cfields
                        cUTCV=",UTCV"
                        cUTCVvalue=","+cp_ToStrODBC(i_CODUTE)
                    Endif
                    If Not ",UTDV," $ cfields And Not "(UTDV)" $ cfields And Not ",UTDV)" $ cfields And Not "(UTDV," $ cfields
                        cUTDV=",UTDV"
                        cUTDVvalue=","+cp_ToStrODBC(SetInfoDate(g_CALUTD))
                    Endif
                    If Not Empty(cValues) And Not Empty(cUTCVvalue+cUTDVvalue)
                        cfieldsReplaced=Strtran(cReplacefields, ",cpccchk)", ",cpccchk"+cUTCV+cUTDV+")")
                        cReplacevalues=cValues
                        cvaluesReplaced=cValues+cUTCVvalue+cUTDVvalue
                    Endif
                Else
                    i_pos=At(",cpccchk=",cLwrcmd)
                    cfields=Left(i_cCmd, i_pos+20)
                    *--- aggiungo fino alla where per vedere se sono gi� presenti i vampi UTCV e UTDV
                    cReplacefields=Substr(i_cCmd,i_pos)
                    ctmp=Substr(i_cCmd,i_pos)
                    ctmp=Left(ctmp,At(" where ",Lower(ctmp)))
                    If Len(ctmp)>21
                        cfields=cfields + Substr(ctmp,22)
                        cReplacefields=Left(cReplacefields,At(" where ",Lower(cReplacefields)))
                    Endif
                    cfieldsReplaced=cReplacefields
                    cfields=Upper(cfields)
                    If Not ",UTCV =" $ cfields And Not " UTCV =" $ cfields
                        cfieldsReplaced=cfieldsReplaced + ",UTCV =" +cp_ToStrODBC(i_CODUTE)
                    Endif
                    If Not ",UTDV =" $ cfields And Not " UTDV =" $ cfields
                        cfieldsReplaced=cfieldsReplaced + ",UTDV =" +cp_ToStrODBC(SetInfoDate(g_CALUTD))
                    Endif
                Endif
            Endif
        Else
            *---  la frase deve essere del tipo: update tabella set campo1 =valore1,....,cpccchk ='xxxxxxxx',UTCV =n,UTDV ={datetime} [ where .... ]
            *    Se il campo � presente due volte nella frase sql, elimino quello aggiunto automaticamente (dopo il cpccchk)
            nOcc=Occurs(" where ",cLwrcmd)
            Do Case
                Case nOcc=0
                    cfields=i_cCmd
                Case nOcc=1
                    cfields=Left(i_cCmd, At(" where ",cLwrcmd))
                Otherwise
                    ctmp=Substr(i_cCmd,At("CPCCCHK",Upper(i_cCmd)))
                    cfields=Left(ctmp, At(" where ",Lower(ctmp)))
            Endcase
            cReplacefields=Substr(cfields,At("CPCCCHK",Upper(cfields)))
            cfieldsReplaced=cReplacefields
        Endif
        If Occurs("UTCV =",Upper(cfields))>1
            IniPos=Rat(",UTCV =",Upper(cfieldsReplaced))
            FinPos=Rat(",UTDV",Upper(cfieldsReplaced))
            cUTCV=Iif(IniPos>0 And FinPos>0, Substr(cfieldsReplaced,IniPos,FinPos-IniPos), "")
            If Not Empty(cUTCV)
                cfieldsReplaced=Strtran(cfieldsReplaced,cUTCV,"")
            Endif
        Endif
        If Occurs("UTDV =",Upper(cfields))>1
            IniPos=Rat(",UTDV =",Upper(cfieldsReplaced))
            cUTDV=Iif(IniPos>0, Substr(cfieldsReplaced,IniPos), "")
            IniPos=At(cTmpTbl, LOWER(cUTDV))-1
            If IniPos>0
                cUTDV=Left(cUTDV,IniPos)
            Endif
            If Not Empty(cUTDV) And Len(cUTDV)<=8+Len(cp_ToStrODBC(Datetime()))
                cfieldsReplaced=Strtran(cfieldsReplaced,cUTDV," ")
            Endif
        Endif
        If Not Empty(cfieldsReplaced)
            i_cCmd=Strtran(i_cCmd,cReplacefields,cfieldsReplaced)
        Endif
        If Not Empty(cvaluesReplaced)
            i_cCmd=Strtran(i_cCmd,cReplacevalues,cvaluesReplaced)
        Endif
    Endif
    If Left(cLwrcmd,6)="insert"
        *---  la frase deve essere del tipo: insert into tabella (campo1,campo2,....,cpccchk,UTCC,UTDC) values (valore1,valore2,....,'xxxxxxxx',n,{datetime})
        If Occurs("UTCC",Upper(i_cCmd))>1 Or Occurs("UTDC",Upper(i_cCmd))>1
            IniPos=At(",CPCCCHK",i_cCmd)
            FinPos=At(") values (",Lower(Substr(i_cCmd,IniPos,30)))
            If FinPos>0
                cReplacefields=Substr(i_cCmd,IniPos,FinPos+1)
                cfieldsReplaced=cReplacefields
                cReplacevalues=Substr(i_cCmd,Rat(",",i_cCmd,3))
                cvaluesReplaced=cReplacevalues
                If (Occurs("UTCC,",Upper(i_cCmd))+Occurs("UTCC)",Upper(i_cCmd))>1) And Occurs(",UTCC",Upper(cfieldsReplaced))>0
                    cfieldsReplaced=Strtran(cfieldsReplaced,",UTCC","")
                    cvaluesReplaced=Left(cvaluesReplaced,At(",",cvaluesReplaced,2))+Substr(cvaluesReplaced,Rat(",",cvaluesReplaced)+1)
                Endif
                If (Occurs("UTDC,",Upper(i_cCmd))+Occurs("UTDC)",Upper(i_cCmd))>1) And Occurs(",UTDC",Upper(cfieldsReplaced))>0
                    cfieldsReplaced=Strtran(cfieldsReplaced,",UTDC","")
                    cvaluesReplaced=Left(cvaluesReplaced,Rat(",",cvaluesReplaced)-1)+")" && si dovrebbero aggiungere tutti i caratteri dopo la "}", ma c'� solo la ")"
                Endif
                If Not Empty(cfieldsReplaced) And Not Empty(cvaluesReplaced)
                    i_cCmd=Strtran(i_cCmd,cReplacefields,cfieldsReplaced)
                    i_cCmd=Strtran(i_cCmd,cReplacevalues,cvaluesReplaced)
                Endif
            Endif
        Endif
    Endif
    Return i_cCmd
Endfunc
*--- Zucchetti Aulla Fine - Gestione automatica campi UTCC,UTCV,UTDC,UTCV


Func cp_TrsSQLExec(nConn,cCmd) &&!!! da togliere
    Return(cp_TrsSQL(nConn,cCmd))

Func cp_TrsSQL(i_nConn,i_cCmd)
    Local i_i, i_j, i_fRecord, i_fHandle, i_fTime, i_fNameFile, l_cEncodeCmd
    i_i=0
    If Not(bTrsErr)
        *MessageBox('i_cCmd='+i_cCmd)
        *--- Zucchetti Aulla Inizio - Gestione automatica campi UTCC,UTCV,UTDC,UTCV
        *-- Codifica stringhe fisse frasi SQL
        Dimension cConst[1]
        m.l_cEncodeCmd = cp_encode_str_const(i_cCmd, @cConst)
        If Not Empty(m.l_cEncodeCmd)
            m.l_cEncodeCmd = ah_SQL_UTFLD(m.l_cEncodeCmd)
            *-- Decodifica stringhe fisse frasi SQL
            i_cCmd = cp_decode_str_const(m.l_cEncodeCmd, @cConst)
        Endif
        *--- Zucchetti Aulla Fine - Gestione automatica campi UTCC,UTCV,UTDC,UTCV
        i_j=Ascan(nTrsConn,i_nConn)
        If i_j=0
            * --- aggiunge tra le connessioni in cui dovra' chiudere la transazione
            nTrsConnCnt=nTrsConnCnt+1
            Dimension nTrsConn[nTrsConnCnt]
            nTrsConn[nTrsConnCnt]=i_nConn
            =SQLSetprop(nTrsConn[nTrsConnCnt],"Transactions",2)
        Endif
        *--- Activity log
        If i_nACTIVATEPROFILER>0
            i_fTime=Datetime()
            i_fTime_sec=Seconds()
        Endif
        *--- Activity log
        * --- Zucchetti Aulla Segnalo attivit� sessione
        If (Vartype(g_CHKACTQRY)<>'C' Or g_CHKACTQRY='S') And Vartype(g_LASTVERACT)='N'
            Local i_nChkSeconds
            i_nChkSeconds=Seconds()
            If (i_nChkSeconds-g_LASTVERACT<0 Or i_nChkSeconds-g_LASTVERACT>10*60)
                chkzuusr(.Null.,'ACTIVE')
            Endif
        Endif
        * ---
        * --- Zucchetti Aulla - inizio gestione riconnessione automatica
        Local nCoderr
        nCoderr=0
        * --- Zucchetti Aulla - fine gestione riconnessione automatica
        i_i=SQLXInsUpdDel(i_nConn,i_cCmd , @nCoderr) && Zucchetti aulla aggiunto secondo parametro
        * --- Zucchetti Aulla - inizio gestione riconnessione automatica
        If (nCoderr=1205 And Type("i_DEADREQUERY")='L' And i_DEADREQUERY=.T. And CP_DBTYPE='SQLServer'  ) Or (nCoderr=60 And Type("i_DEADREQUERY")='L' And i_DEADREQUERY=.T. And CP_DBTYPE='Oracle' )
            *Se si � verificato un deadlock non si impostano le variabili di errore ma tenta di riesguire la query
            *nCoderr=1205  significa che si � verificato il deadlock
            i_i=cp_Trsdeadlock (i_nConn,i_cCmd, @nCoderr)
            nCoderr= Iif (CP_DBTYPE='SQLServer'   , 1205 , 60 ) &&codice di errore dei deadlock restituito da microsoft sql server
        Endif
        * --- Zucchetti Aulla Fine
        If i_i=-1
            bTrsErr=.T.
            nTrsErrn=Error()
            i_TrsMsg=cp_TranslateDBMessage(Message(),.T.)
            * --- Zucchetti Aulla - inizio gestione riconnessione automatica
            *Se � caduta la connessione la funzione cp_autoconnect riattiva la connessione
            cp_autoconnect (i_nConn,i_cCmd,' ','N', @nCoderr)
            bTrsErr=.T.
            * ---Zucchetti Aulla -fine gestione riconnessione automatica
        Endif
        *--- Activity log Traccia le operazioni su database
        If i_nACTIVATEPROFILER>0
            cp_ActivityLogger(i_fTime, i_fTime_sec, Seconds(), "IUD", i_cCmd, Iif(Type("i_curform")="C",i_curform,Space(10)), i_nConn, i_i, i_TrsMsg)
        Endif
        *--- Activity log
    Endif
    Return(i_i)

Function cp_sqlexec (i_nConn,i_cSelect,i_cCursor , i_caller, i_bAsync )
    *--- Riconnessione automatica
    *--- i_caller  contiene il riferimento all oggetto che ha lanciato la query -- Zucchetti Aulla Fine Riconnessione automatica  --
    Local i_result, i_fRecord, i_fHandle, i_fTime, i_fNameFile , i_newConn , i_numconn , objcon , i_transazione , i_isconnected ,i_attesa
    *--- Activity log  e Riconnessione automatica
    *--- i_bAsync = .T. indica che la chiamata proviene da cp_SQLX2 nei casi in cui la chiamata stessa
    *--- � seguita dalla WaitForReply. Tale valore � passato come parametro alla cp_ActivityLogger per
    *--- indicare che la query tracciata � asincrona
    *--- memorizza il tempo per visualizzare nell activity log il tempo trascorso tra ogni query
    Local i_fTime_sec
    If i_nACTIVATEPROFILER>0
        i_fTime=Datetime()
        i_fTime_sec=Seconds()
    Endif
    * --- Zucchetti Aulla Segnalo attivit� sessione
    If (Vartype(g_CHKACTQRY)<>'C' Or g_CHKACTQRY='S') And Vartype(g_LASTVERACT)='N' And Vartype(i_bFirstCtrlSeconds)<>'L'
        Local i_nChkSeconds
        i_nChkSeconds=Seconds()
        i_bFirstCtrlSeconds=.T.
        If (i_nChkSeconds-g_LASTVERACT<0 Or i_nChkSeconds-g_LASTVERACT>10*60)
            chkzuusr(.Null.,'ACTIVE')
        Endif
    Endif
    * ---
    If (Alen(i_ServerConnBis)>2)
        i_nConn=cp_SearchConn (i_nConn)  &&Se la connessione � caduta allora rintraccia la connessione corrente dall array contenente lo storico delle connessioni cadute
    Endif
    *--- Riconnessione automatica

    If i_nConn>-1
        *--- Zucchetti Aulla Inizio - in Postgres il carattere escape '\' deve essere raddoppiato nella like
        If " like " $ Lower(i_cSelect) And " where " $ Lower(i_cSelect)
            i_cSelect=cp_SQL_like(i_cSelect, i_nConn)
        Endif
        *--- Zucchetti Aulla Fine - in Postgres il carattere escape '\' deve essere raddoppiato nella like
        If Type("i_cCursor")="C"
            i_result=SQLExec(i_nConn, i_cSelect, i_cCursor)
        Else
            i_result=SQLExec(i_nConn, i_cSelect)
        Endif
    Else
        *--- Se la connessione � caduta non eseguo nessuna frase sql
        i_result = -1
    Endif
    *--- Gestione riconnessione automatica
    *--- Se � caduta la connessione la funzione cp_autoconnect riattiva la connessione
    If i_result<0
        i_result=cp_autoconnect (i_nConn,i_cSelect,i_cCursor,'S',i_caller )
    Endif
    *--- Gestione riconnessione automatica
    *--- Activity log
    If i_nACTIVATEPROFILER>0
        cp_ActivityLogger(i_fTime, i_fTime_sec, Seconds(), "SEL", i_cSelect, Iif(Type("i_curform")="C",i_curform,Space(10)), i_nConn, i_result, Iif(i_result=-1,Message(),''), , Iif(Type("i_cCursor")="C",i_cCursor,""), " ", i_bAsync)
    Endif
    *--- Activity log
    Return(i_result)

    *--- Riconessione automatica
    *Imposta i valori di default di tutte le variabili usate nella gestione dei deadlock e la gestione della riconnessione automatica
Procedure cp_SetVariable ()
    *Gestione dei DEADLOCK
    If Type("i_DEADREQUERY")='L' And i_DEADREQUERY=.T.
        *** Se i parametri di configurazione non sono stati impostati allora si impostano per default a i_NUMDEADREQUERY=3  e  i_DELAYDEADREQUERY=3
        If Not Type("i_NUMDEADREQUERY")='N'
            Public i_NUMDEADREQUERY
            i_NUMDEADREQUERY=3
        Endif
        If Not Type("i_DELAYDEADREQUERY")='N'
            Public i_DELAYDEADREQUERY
            i_DELAYDEADREQUERY=3
        Endif
    Endif

    *Gestione riconnessione automatica
    *** Se i parametri di configurazione non sono stati impostati allora si impostano per default a i_NUMRICONNECT=3  e  i_DELAYRICONNECT=3
    If Not Type("i_NUMRICONNECT")='N'
        Public i_NUMRICONNECT
        i_NUMRICONNECT=3
    Endif
    If Not Type("i_DELAYRICONNECT")='N'
        Public i_DELAYRICONNECT
        i_DELAYRICONNECT=3
    Endif
    If Not Type("i_ConnectTimeOut")='N'
        Public i_ConnectTimeOut
        i_ConnectTimeOut=3
    Endif

    If Not Type("i_CONNECTMSG")='C'
        Public i_CONNECTMSG
        i_CONNECTMSG='N'
    Endif
Endproc

* --- Zucchetti Aulla Inizio, Activity log e riconnessione
*Gestisce i deadlock che si sono verificati sotto transazione nella cp_TrsSQL. Se � caduta la connessione il parametro nCoderr contiene il codice di errore
*1025, in tal caso se � atttiva la gestione dei deadlock viene riprovata l' esecuzione della query tramite la funcione SQLXInsUpdDel che esegue la frase tramite
* chiamata diretta ai metodi dell ODBC
Function cp_Trsdeadlock (i_nConn,i_cCmd, nCoderr)
    Local i_result , i_numconn , i_transazione
    cp_SetVariable ()
    i_numconn=0
    i_result=-1
    i_transazione = Not (Type("nTrsConnCnt")="U" Or nTrsConnCnt=0)
    *Se il deadlock si � verificata sotto transazione allora la transazione verr�  abortita
    If i_transazione
        cp_EndTrs (.T.)
        **Con i deadlock sotto transazione il server fa il rollback dell intera transazione percio non si pu� ripetere la query
        If (nCoderr=1205 And CP_DBTYPE='SQLServer'  ) Or ( nCoderr=60 And  CP_DBTYPE='Oracle'  )
            cp_BeginTrs()
            bTrsErr=.T.
            i_TrsMsg=cp_TranslateDBMessage("Si � verificato un deadlock sotto transazione perci� l'operazione � stata abortita" ,.T.)
            cp_ErrorMsg(i_TrsMsg)
            Return -1
        Endif
    Endif
    Return  -1

    *Nel caso in cui sia caduta la connessione cp_autoconnect gestisce la riconessione al DB
    *Il parametro i_requery='S' indica alla procedura di riprovare ad eseguire la query
    *il parametro i_caller contiene il riferimento agli oggetti di tipo zoom che usano connessioni secondarie e che dispongono del metotdo setconnection() da invocare per aprire le connessioni secondarie
    *il parametro n_coderr � usato per la gestione dei deadloc, se n_coderr=1025 significa che si � verificato un deadlock
Function cp_autoconnect (i_nConn,i_cSelect,i_cCursor , i_requery , i_caller, nCoderr)
    Local i_result, i_fRecord, i_fHandle, i_fTime, i_fNameFile , i_newConn , i_numconn , objcon , i_transazione , i_isconnected ,i_attesa , i_oldnconn , i_j  , ntab
    i_oldnconn=i_nConn
    Aerror(erroreSql)
    i_transazione = Not (Type("nTrsConnCnt")="U" Or nTrsConnCnt=0)
    *GESTIONE DEI DEADLOCK. Se � attivata la gestione dei deadlock allora si ritenta l esecuzione delle query
    If Type("i_DEADREQUERY")='L' And i_DEADREQUERY=.T. And (CP_DBTYPE='SQLServer' Or  CP_DBTYPE='Oracle')
        cp_SetVariable ()
        * I Deadlock che si sono verificati sotto transazione nella cp_Trsql sono gi� stati gestiti con la cp_Trsdeadlock quindi viene gestita solo la caduta di connessione
        * relativa a query
        If ( Type("erroreSql[5]")='N' And erroreSql[5]=1205 And CP_DBTYPE='SQLServer' ) Or (Type("erroreSql[5]")='N' And erroreSql[5]=60  And CP_DBTYPE='Oracle'  )
            i_numconn=0
            i_result=-1
            *Se il deadlock si � verificata sotto transazione allora la transazione verr�  abortita
            If i_transazione
                cp_EndTrs (.T.)
                **Con i deadlock sotto transazione il server fa il rollback dell intera transazione percio non si pu� ripetere la query
                cp_BeginTrs()
                bTrsErr=.T.
                i_TrsMsg=cp_TranslateDBMessage("Si � verificato un deadlock sotto transazione perci� l'operazione � stata abortita" ,.T.)
                cp_ErrorMsg(i_TrsMsg)
                Return -1
            Endif
            Do While i_numconn<i_NUMDEADREQUERY And ((Type("erroreSql[5]")='N' And erroreSql[5]=1205 And CP_DBTYPE='SQLServer' ) Or (Type("erroreSql[5]")='N' And erroreSql[5]=60 And CP_DBTYPE='Oracle'))	And i_result=-1
                *--- Activity log
                If i_nACTIVATEPROFILER>0
                    i_fTime=Datetime()
                    i_fTime_sec=Seconds()
                Endif
                *---  Activity log
                *********************Per testare il deadlock e rilasciare la risorsa (per testare il deadlock in Oracle ho dovuto usare i lock di tabella e bisogna rilasciarlO esplicitamente altrimenti gli altri processi rimangono bloccati)
                *********************le successive righe si possono rimuovere dopo il test
                If  Vartype(debug_gestione_deadlock)='C' And debug_gestione_deadlock="debug_gestione_deadlock"
                    Sqlcommit(i_nConn)
                Endif
                ***********************

                cp_connecMSG  ('Tentativo di esecuzione dopo DeadLock ' + Alltrim(Str(i_numconn + 1 )), i_DELAYDEADREQUERY)
                If Vartype(i_cCursor)="C"
                    i_result=SQLExec(i_nConn, i_cSelect, i_cCursor)
                Else
                    i_result=SQLExec(i_nConn, i_cSelect)
                Endif
                Aerror(erroreSql)
                *--- Activity log
                If i_nACTIVATEPROFILER>0
                    cp_ActivityLogger(i_fTime, i_fTime_sec, Seconds(), "   ", i_cSelect, Iif(Type("i_curform")="C",i_curform,Space(10)), i_nConn, i_result, Iif(i_result=-1,Message(),''), , Iif(Type("i_cCursor")="C",i_cCursor,""),"D")
                Endif
                *--- Activity log

            Enddo
            *Se l errore era stato causato da un deadlock ed � stato risolto con successo allora termina
            If i_result<>-1
                Return i_result
            Endif
        Endif
    Endif

    *Se non � stata impostata la riconnessione automatica termina
    If  Type("i_AUTORICONNECT")<>'L' Or i_AUTORICONNECT<>.T.
        Return -1
    Endif
    * ho un errore devo capire se � caduta la connessione (se i_AUTORICONNECT=.T. tenta la riconnessione automatica)
    i_result=-1
    *---
    Local msgErr_tmp
    msgErr_tmp = Message()
    *---
    i_isconnected=Cp_IsConnected(i_nConn)
    If Not i_isconnected
        *Setta le variabili usate dalla gestione automatica riconnessioni
        cp_SetVariable ()
    Else
        *--- Le SQLSetprop/SQLGetprop sbiancano la message() quindi devo rivalorizzarla
        *--- altrimenti non viene mostrato l'errore sql
        Local l_old_err, tt
        l_old_err = On('ERROR')
        On Error tt=1
        Error msgErr_tmp
        On Error &l_old_err
        Return -1
    Endif
    *** cerca di riconnettersi
    i_numconn=0

    *Imposta il timeout per il tentativo di connessione e non visualizza la maschera di login (le riconessioni dovrebbero avvenire in modo trasparente)
    SQLSetprop(0, "ConnectTimeOut", i_ConnectTimeOut )

    *** se i_transazione=.T. significa che ci si trova sotto transazione
    i_transazione = Not (Type("nTrsConnCnt")="U" Or nTrsConnCnt=0)

    *** Finch� la frase SQL lanciata sul DB d� errore e l errore � causato dalla
    *** caduta della connessione allora e non si � superato il numero di tentativi
    *** ritenta la connessione e lancia la frase. Se ci si trova sotto transazione allora
    *** bisogna chiudere la transazione facendo il Rollback (painter), riattivare la connessione,
    *** riaprire la transazione, mettere btrserr=.T. e rieseguire la frase. Se non � attiva una
    *** transazione allora basta ripristinare la connessione e riprovare ala rilanciare la frase

    i_isconnected=Cp_IsConnected(i_nConn)
    Do While i_result<0 And i_numconn<i_NUMRICONNECT And Not i_isconnected
        ***ciclo a vuoto per prima di effettuare un nuovo tentativo di connessione
        *--- Activity log
        If i_nACTIVATEPROFILER>0
            i_fTime=Datetime()
            i_fTime_sec=Seconds()
        Endif
        *--- Activity log

        cp_connecMSG  ('Tentativo di riconnessione ' + Alltrim(Str(i_numconn + 1 )), i_DELAYRICONNECT )
        *Se la caduta di connessione � dovuta alla chiamata a SQLXPrimaryKey nella ricostruzione  database bisogna ritirare su la connessione e richiamare SQLXPrimaryKey
        If Vartype(i_requery )='C' And i_requery ='SQLXPrimaryKey'
            *L unica chiamata a SQLXPrimaryKey si trova all interno di cp_DBADM
            i_nConn=Cp_DoConnect(i_nConn)
            cfrom=SQLXPrimaryKey(i_nConn,i_cSelect,i_cCursor )
        Else
            *Se la caduta di connessione si � verificata sotto transazione allora la transazione verr�  abortita
            If i_transazione
                cp_EndTrs (.T.)
            Endif
            If Vartype(i_caller)<>'O'
                *Nel Caso connessione Sincrona si riattiva la connessione principale
                i_nConn=cp_SearchConn(  i_nConn)
                If (Not Cp_IsConnected(i_nConn))
                    i_nConn=Cp_DoConnect(i_nConn)
                Endif
            Else
                *GESTIONE DELLE CADUTE DELLE CONNESSIONI  SECONDARIE
                cp_SearchConn=cp_SearchConn(i_caller.o_nConn )
                *SE la principale � caduta allora bisogna riattivarla dopodiche attivare anche la secondaria
                *Aggiorna la connessione dell oggetto (potrebbe essere stata cambiata precedentemente a causa della caduta in qualche altro punto del programma)

                If Not Cp_IsConnected(i_caller.o_nConn )
                    *Se la connessione principale  � caduta Cerca di riconnettersi
                    i_nConn=Cp_DoConnect(i_caller.o_nConn )
                    *reimposta la connessione principale dello zoom
                    i_caller.o_nConn =i_nConn
                Endif
                *riapre la connessione secondaria
                i_caller.SetConn ( i_caller.o_nConn, i_caller.o_cDatabaseType , .T.)
                i_nConn = i_caller.nConn
                If Vartype(i_caller.cDatabaseType)='C' And i_caller.cDatabaseType='SQLServer' And i_caller.bAsyncConn
                    Do Case
                        Case i_caller.cDatabaseType='SQLServer'
                            i_result=SQLExec(i_caller.nConn,'set transaction isolation level read uncommitted','',i_caller)
                        Case i_caller.cDatabaseType='DB2'
                            *cp_sqlexec(this.nConn,'set current txnisolation=1')
                    Endcase
                Endif
            Endif
            i_numconn=i_numconn + 1

            If i_transazione
                *Se cade la connessione all interno della transazione considera annullata l'operazione , per evitare errore utente
                *riapre la transazione gi� annullata
                cp_BeginTrs()
                bTrsErr=.T.
                i_TrsMsg=cp_TranslateDBMessage('Salvataggio interrotto per la caduta della connessione al server. Rilanciare il salvataggio!' ,.T.)
            Endif

            *rirpistina il contenuto dell array contenente le connessioni usate in transazioni
            If Vartype(nTrsConn) <> 'U'
                i_j=Ascan(nTrsConn,i_oldnconn)
                If i_nConn <>i_oldnconn And i_j<>0
                    * --- aggiunge tra le connessioni in cui dovra' chiudere la transazione
                    nTrsConn[i_j]=i_nConn
                    =SQLSetprop(nTrsConn[i_j],"Transactions",2)
                Endif
            Endif
            i_oldnconn=i_nConn

            *Se la riconnessione ha avuto successo allora non si considerano gli errori che si sono verificati a causa della caduta di connessione
            If Vartype(i_err)='L' And Not i_transazione
                i_err=.F.
            Endif
            *** riprova a rilanciare la frase SQL che ha causato errore
            If (i_requery='S')
                If Vartype(i_cCursor)="C"
                    i_result=SQLExec(i_nConn, i_cSelect, i_cCursor)
                Else
                    i_result=SQLExec(i_nConn, i_cSelect)
                Endif
            Endif

        Endif
        i_isconnected=Cp_IsConnected(i_nConn)
        *--- Activity log (tentativo di riconnessione)
        If i_nACTIVATEPROFILER>0
            Local xnConn,nElemens
            xnConn=cp_SearchConn(Iif (Vartype(i_caller)<>'O' ,i_nConn  , i_caller.o_nConn  ))
            * ricerco la stringa di connessione da concatenare alla frase...
            nElements = Ascan(i_ServerConn, xnConn, -1, -1, 2, 8)
            *se � arrivato un numero di connessione sbagliato (che non esiste piu si cerca la stringa di connessione nell array contenente lo storico delle connessioni
            If  Vartype(nElements)='L' Or nElements<=0
                nElements = Ascan(i_ServerConnBis, xnConn, -1, -1, 1, 8)
                If  Vartype(nElements)='N' And nElements>0
                    nElements = Ascan(i_ServerConn, i_ServerConnBis[nElements ,2], -1, -1, 8, 8)
                Endif
            Endif

            cp_ActivityLogger(i_fTime, i_fTime_sec, Seconds(), "   ", i_cSelect+Chr(13)+Chr(10)+'Connection string: '+Iif(nElements>0,i_ServerConn[nElements,8],'No connection string avaiable'), Iif(Type("i_curform")="C",i_curform,Space(10)), i_nConn, i_result, Iif(i_result=-1,Message(),''), , Iif(Type("i_cCursor")="C",i_cCursor,""),"R")
        Endif
        *--- Activity log
    Enddo

    *Se i tentativi di riconessioneb automatica sono falliti allora si disattiva il meccanismo di riconessione altrimenti
    *verrebbe richiamatao da ogni query lanciata sul DB (per esempio un batch con tante query terminerebbe dopo aver provato
    *la riconessione per ogni query fallita con i relativi tempi di delay usati in ogni tentativo di riconnessione)
    If Not i_isconnected
        If cp_YesNo(cp_Translate("La connessione � caduta e il tentativo di riconnessione � fallito. Vuoi uscire dall'applicazione ?") + Message(),32,.F.)
            On Error 1=1
            Do KillProcess
        Endif
    Else
        *** Zucchetti Aulla - sistemo le informazioni di connessione
        Local i_cCursorWork
        If Vartype(i_cCursor)="C"
            i_cCursorWork=Sys(2015)
            Select * From &i_cCursor Into Cursor &i_cCursorWork
        Endif
        INSUSERAPP()
        If Vartype(i_cCursorWork)="C"
            Select * From &i_cCursorWork Into Cursor &i_cCursor
            Use In &i_cCursorWork
        Endif
    Endif
    Return (i_result)
Endfunc

*Riceve come parametro il nome della tabella di cui deve recuperarne la connessione associata a tale tabella.
*Se la connessione nconn � caduta allora � stata memorizzata nell array i_ServerConnBis contenente i riferimenti di tutte le connessioni cadute
*Usando l' array i_ServerConnBis si risale alla connessione che � stata ripristinata al posto di nconn
Function cp_SearchConn 	 ( nConn)
    Local nElements
    *Cerca di recuperare la connessione principale sapendo il nome della tabella
    nElements = Ascan(i_ServerConn, nConn, -1, -1, 2, 8)
    *se � arrivato un numero di connessione sbagliato (che non esiste piu si cerca la stringa di connessione nell array contenente lo storico delle connessioni
    If  Vartype(nElements)='L' Or nElements<=0
        nElements = Ascan(i_ServerConnBis, nConn, -1, -1, 1, 8)
        If  Vartype(nElements)='N' And nElements>0
            nElements = Ascan(i_ServerConn, i_ServerConnBis[nElements ,2], -1, -1, 8, 8)
        Endif
    Endif

    If Vartype(nElements)='N' And nElements>0
        nConn=i_ServerConn[nElements  , 2]
    Endif

    Return nConn
Endfunc

*Visaulizza messaggi relativi alla gestione riconnessione autoamtica e  gestione deadlock
Procedure cp_connecMSG (msg,tempo)
    Local nSeconds , gnErrFile
    If i_CONNECTMSG='W'
        cp_Msg (msg )
    Endif
    If i_CONNECTMSG='D'
        cp_DBG (msg)
    Endif
    If i_CONNECTMSG='F'
        cp_connecFile (msg)
    Endif
    nSeconds=Seconds()
    * --- da gestire se prossimo alla mezzanotte...
    Do While Seconds()-nSeconds<=tempo
        *--- attendo passivamente che passino i secondi richiesti
    Enddo
Endproc

*Memorizza su file i messaggi relativi alla gestione riconnessione autoamtica e  gestione deadlock
Procedure cp_connecFile (msg)
    Local  gnErrFile
    If i_CONNECTMSG='F'
        *Apre il file di log dei messaggi relativi alla riconnessione automatica
        If File(TempAdhoc()+'\Log_Riconnessione.TXT')  && Does file exist?
            gnErrFile = Fopen(TempAdhoc()+'\Log_Riconnessione.TXT',12)  && If so, open read/write
        Else
            gnErrFile = Fcreate(TempAdhoc()+'\Log_Riconnessione.TXT')  && If not, create it
        Endif
        If gnErrFile < 0  && Check for error opening file
            Wait 'Cannot open or create output file' Window Nowait
        Else  && If no error, write to file
            Fseek (gnErrFile,0,2)
            Fputs(gnErrFile,  Alltrim(Ttoc(Datetime()))+"  :  "+ msg )
        Endif
        Fclose(gnErrFile)  && Close file
    Endif
Endproc

*--- Gestione riconnessione automatica

*--- Data la connessione recupera il nome del server...
Function cp_GetServerConn (nConn)
    Local nIdx
    nIdx=Ascan(i_ServerConn, nConn, -1, -1, 2, 8)
    * --- Tra la connessione effettiva e la valorizzazione di i_Serverconn
    * --- possono partire frasi, in questo caso metto sconosciuto...
    Return (Iif(nIdx<=0,MSG_UNKNOWN+'('+Alltrim(Str(nConn,10,0))+')',i_ServerConn[nIdx,1]))
    *--- fine gestione riconnessione automatica

Func cp_SQL(i_nConn,i_cCmd,i_cCursorName,i_nMaxRec,i_bDontReportError)
    Local i_nPar,i_olderr,i_errmess,i_typemsgwnd,i_typesched
    Private i_i
    i_nPar=Pcount()
    *i_cCmd=cp_AggiungiTraduzioni(i_cCmd)
    i_typemsgwnd=Type('msgwnd')
    ***** Zucchetti Aulla Inizio - MSGWND Type che sporca la Message()
    i_typesched=Type('g_SCHEDULER')
    ***** Zucchetti Aulla Fine -  MSGWND Type che sporca la Message()
    *activate screen
    *? i_cCmd
    *IF i_nConn<1000000
    If i_nPar>=4 And Vartype(i_nMaxRec)='N'
        CursorSetProp('MaxRecords',i_nMaxRec,0)
        *--- Zucchetti Aulla Inizio - Modifica i_cCmd per aggiungere TOP
        *--- Gestisco Oracle, SQLServer, DB2
        Local i_cDatabaseType, l_nSel
        i_cDatabaseType = Upper(cp_GetDatabaseType(i_nConn))
        Do Case
            Case Upper(i_cDatabaseType) = 'SQLSERVER'
                l_nSel = At('select ', i_cCmd)
                i_cCmd = Stuff(i_cCmd, l_nSel, 7, "select top "+Alltrim(Str(i_nMaxRec))+" ")
            Case Upper(i_cDatabaseType) = 'ORACLE'
                l_nSel = At(' where', i_cCmd)
                i_cCmd = Stuff(i_cCmd, l_nSel, 6, " where rownum<="+Alltrim(Str(i_nMaxRec))+" and")
            Case Upper(i_cDatabaseType) = 'DB2' Or Upper(i_cDatabaseType) = 'POSTGRESQL'
                i_cCmd = i_cCmd + " FETCH FIRST "+Alltrim(Str(i_nMaxRec))+" ROWS ONLY"
        Endcase
        *--- Zucchetti Aulla Fine - Modifica i_cCmd per aggiungere TOP
    Endif
    i_i=0
    i_olderr=On('ERROR')
    On Error i_i=-1
    *t=SECONDS()
    If i_nPar>=3 And Vartype(i_cCursorName)='C'
        i_i=cp_sqlexec(i_nConn,i_cCmd,i_cCursorName)
    Else
        i_i=cp_sqlexec(i_nConn,i_cCmd)
    Endif
    *? "QUERY=",SECONDS()-T
    On Error &i_olderr
    If i_nPar>=4 And Vartype(i_nMaxRec)='N'
        CursorSetProp('MaxRecords',-1,0)
    Endif
    If i_i=-1 And !i_bDontReportError
        Private ae
        Dimension ae[1]
        Aerror(ae)
        If  Type("ae(5)")='N' And ae(5)=10054 And (Type("i_AUTORICONNECT")<>'L' Or i_AUTORICONNECT<>.T.)
            If cp_YesNo(MSG_CONNECTION_TO_SERVER_FALLEN_F_EXIT_QP,32,.T.,.F.,MSG_ERROR)
                Quit
            Endif
        Else
            * Gestione Log Errori
            i_errmess=Message()
            If i_typemsgwnd='O' And Type('msgwnd.class')='C' And Lower(msgwnd.Class)='mkdbmsg'
                msgwnd.Add_err(i_cCmd+Chr(13)+Chr(10)+i_errmess)
            Else
                ***** Zucchetti Aulla Inizio - Schedulatore
                If ((Vartype(g_JBSH)='C' And g_JBSH='S') Or (Vartype(g_IZCP)='C' And g_IZCP='S')) And i_typesched ='C' And (Vartype(g_SCHEDULER)='C' And g_SCHEDULER='S')
                    g_MSG=g_MSG+i_cCmd+Chr(13)+Chr(10)+i_errmess+Chr(13)
                Else
                    _Cliptext=i_cCmd
                    cp_ErrorMsg(Right(i_cCmd+Chr(13)+Chr(10)+i_errmess,254),32,MSG_ERROR,.F.)
                    *****Zucchetti Aulla Fine - Schedulatore
                Endif
            Endif
        Endif
    Endif
    *ELSE
    *  i_i=i_oRemoteConn[i_nConn-1000000].Query(i_cCmd,i_cCursorName)
    *ENDIF
    Return(i_i)

Function cp_AggiungiTraduzioni(i_cCmd)
    Local i_p,i_p2,i_s,i_fld,i_bSetName
    i_p=At('T#',i_cCmd)
    Do While i_p<>0
        i_s=Substr(i_cCmd,i_p+2)
        i_p2=At('#',i_s)
        i_fld=Substr(i_cCmd,i_p+2,i_p2-1)
        *i_bSetName=LOWER(SUBSTR(i_cCmd,i_p+i_p2+2,4))<>' as '
        i_bSetName=Left(Alltrim(Substr(i_cCmd,i_p+i_p2+2,10)),1)==',' Or Lower(Left(Alltrim(Substr(i_cCmd,i_p+i_p2+2,10)),4))='from'
        * --- impone la traduzione
        i_fld='coalesce((select TradTrad from traduzioni where CodTrad='+i_fld+"_trad and CodLng='"+i_cLanguage+"'),"+i_fld+')'+Iif(i_bSetName,' as '+i_fld,'')
        * --- ricompone il comando
        i_cCmd=Left(i_cCmd,i_p-1)+i_fld+Substr(i_cCmd,i_p+i_p2+2)
        i_p=At('T#',i_cCmd)
    Enddo
    Return i_cCmd

Func cp_SetTrsOp(cOp,cFldName,cVarName,xVarValue,cTrsType,nConn)
    If nConn=0
        * --- operazioni per VFP
        Do Case
            Case cTrsType='update'
                Do Case
                    Case cOp='+' Or cOp='-'
                        Return(cFldName+cOp+'('+cVarName+')')
                    Case cOp='='
                        Return(cVarName)
                    Case cOp=' '
                        Return(cFldName)
                    Otherwise
                        Error 'transaction variable operation'
                Endcase
            Case cTrsType='restore'
                Do Case
                    Case cOp='+'
                        Return(cFldName+'-('+cVarName+')')
                    Case cOp='-'
                        Return(cFldName+'+('+cVarName+')')
                    Case cOp='=' Or cOp=' '
                        Return(cFldName)
                    Otherwise
                        Error 'transaction variable operation'
                Endcase
            Case cTrsType='insert'
                Do Case
                    Case cOp='+' Or cOp='='
                        Return(cVarName)
                    Case cOp='-'
                        Return('-('+cVarName+')')
                    Case cOp=' '
                        Return(cp_ToStr(cp_NullValue(xVarValue)))
                    Otherwise
                        Error 'transaction variable operation'
                Endcase
            Otherwise
                Error 'transaction variable operation'
        Endcase
    Else
        * --- operazioni per ODBC
        Do Case
            Case cTrsType='update'
                Do Case
                    Case cOp='+' Or cOp='-'
                        Return(cFldName+cOp+'('+cp_ToStrODBC(xVarValue)+')')
                    Case cOp='='
                        Return(cp_ToStrODBC(xVarValue))
                    Case cOp=' '
                        Return(cFldName)
                    Otherwise
                        Error 'transaction variable operation'
                Endcase
            Case cTrsType='restore'
                Do Case
                    Case cOp='+'
                        Return(cFldName+'-('+cp_ToStrODBC(xVarValue)+')')
                    Case cOp='-'
                        Return(cFldName+'+('+cp_ToStrODBC(xVarValue)+')')
                    Case cOp='=' Or cOp=' '
                        Return(cFldName)
                    Otherwise
                        Error 'transaction variable operation'
                Endcase
            Case cTrsType='insert'
                Do Case
                    Case cOp='+' Or cOp='='
                        Return(cp_ToStrODBC(xVarValue))
                    Case cOp='-'
                        Return('-('+cp_ToStrODBC(xVarValue)+')')
                    Case cOp=' '
                        Return(cp_ToStrODBC(cp_NullValue(xVarValue)))
                    Otherwise
                        Error 'transaction variable operation'
                Endcase
            Otherwise
                Error 'transaction variable operation'
        Endcase
    Endif
    Return(cFldName)

Func cp_NullLink(i_cValue,i_cTable,i_cField)
    Local i_olderr,i_v
    Private i_err
    i_err=.F.
    i_olderr=On('error')
    On Error i_err=.T.
    If Left(i_cValue,4)="{d '" Or Left(i_cValue,5)="{ts '"
        i_v=.T.
    Else
        i_v=!Empty(Eval(i_cValue))
    Endif
    On Error &i_olderr
    If i_v Or i_err
        Return(i_cValue)
    Endif
    cp_ReadXdc()
    If i_Dcx.IsFieldNullable(i_cTable,i_cField)
        Return('NULL')
    Endif
    Return(i_cValue)

Func cp_NullValue(i_cs)
    * --- risponde il valore nullo per un campo
    Local i_t
    i_t=Type('i_cs')
    Return cp_NullValueForType(i_t,Iif((i_t='C' Or i_t='M') And !Isnull(i_cs),Len(i_cs),1))

Function cp_NullValueForType(i_type,i_len)
    Local i_res
    i_res=""
    Do Case
        Case i_type='C'
            i_res=Repl(' ',i_len)
        Case i_type='M'
            i_res=' '
        Case i_type='N' Or i_type='Y'
            i_res=0.00
        Case i_type='D' Or i_type='T'
            i_res={  /  /  }
        Case i_type='L'
            i_res=.F.
    Endcase
    Return(i_res)

Func cp_ToStr(cs,ty)
    Local res,T,tmp
    T=Vartype(cs)
    Do Case
            *case isnull(cs)
            *  res=cp_ToStr(cp_NullValue(cs))
        Case T='C' Or T='M'
            If Pcount()>=2 And ty=1
                res=Trim(cs)
            Else
                If "'"$cs
                    res='"'+cs+'"'
                Else
                    res="'"+cs+"'"
                Endif
            Endif
        Case T='N' Or T='Y'
            *res=alltrim(str(cs))
            res=Strtran(Trim(Strtran(Str(cs,16,16),'0',' ')),' ','0')
            res=Strtran(res,',','.')
            If Right(res,1)='.'
                res=Left(res,Len(res)-1)
            Endif
            If Empty(res)
                res='0'
            Endif
        Case T='D'
            *res=dtoc(cs)
            *res='{'+dtoc(cs)+'}'
            res=Dtos(cs)
            If Empty(res)
                res='{}'
            Else
                res='{^'+Left(res,4)+'-'+Substr(res,5,2)+'-'+Right(res,2)+'}'
            Endif
        Case T='L'
            res=Iif(cs,'.T.','.F.')
        Case T='T'
            *res=ttoc(cs) modificato il 16.05.97 in
            If Empty(cs)
                res='{}'
            Else
                res=Ttoc(cs,1)
                res='{^'+Left(res,4)+'-'+Substr(res,5,2)+'-'+Substr(res,7,2)+' '+ ;
                    substr(res,9,2)+':'+Substr(res,11,2)+':'+Right(res,2)+'}'
            Endif
    Endcase
    Return(res)

Func cp_ToStrODBC(cs,ty)
    Local res,T,tmp
    T=Vartype(cs)
    Do Case
        Case Isnull(cs) &&or empty(cs)
            res='NULL'
        Case T='C' Or T='M'
            If T='M' And Empty(cs)
                res='NULL'
            Else
                cs=Strtran(cs,"'","''")
                If CP_DBTYPE='MySQL'
                    cs=Strtran(cs,"\","\\")
                Endif
                If Vartype(ty)='N' And ty=1
                    res=Trim(cs)
                Else
                    If !Empty(cs) And ((Vartype(i_OracleVarchar2)='L' And i_OracleVarchar2) Or !(Vartype(i_bNoTrimStrODBC)='L' And i_bNoTrimStrODBC))
                        res="'"+Trim(cs)+"'"
                    Else
                        res="'"+cs+"'"
                    Endif
                Endif
            Endif
        Case T='N' Or T='Y'
            If Empty(cs)
                res='0'
            Else
                * --- Utilizzo la STR fino a 16 (precisione assoluta)
                * --- se il numero non supera le 15 cifre decimali..
                If Abs(cs)<=999999999999999
                    res=Str(cs,16,16)
                Else
                    res=Str(cs,17,17)
                Endif
                * il separatore � sempre il punto..
                res=Strtran(res,',','.')
                * elimino gli zeri oltre l'ultimo decimale se ho porzione decimale..
                If At('.',res)>0
                    #If Version(5)>=900
                        res=Trim(res,'0')
                    #Else
                        Do While Right(res,1)='0'
                            res=Left(res,Len(res)-1)
                        Enddo
                    #Endif
                Endif
                * elimino l'eventuale punto finale
                If Right(res,1)='.'
                    res=Left(res,Len(res)-1)
                Endif
            Endif
            * controllo su valore di default in uscita
            If Empty(res)
                res='0'
            Endif
        Case T='D'
            If Empty(cs)
                res='NULL'
            Else
                tmp=Dtos(cs)
                If i_ServerConn[1,6]='Interbase'
                    res="'"+Left(tmp,4)+"-"+Substr(tmp,5,2)+"-"+Substr(tmp,7,2)+"'"
                Else
                    res="{d '"+Left(tmp,4)+"-"+Substr(tmp,5,2)+"-"+Substr(tmp,7,2)+"'}"
                Endif
            Endif
        Case T='L'
            *res=iif(cs,'TRUE','FALSE')
            res=Iif(cs,'1','0')
        Case T='T'
            Do Case
                Case Empty(cs)
                    res='NULL'
                Case Pcount()>=2 And ty='D'
                    tmp=Dtos(cs)
                    If i_ServerConn[1,6]='Interbase'
                        res="'"+Left(tmp,4)+"-"+Substr(tmp,5,2)+"-"+Substr(tmp,7,2)+"'"
                    Else
                        res="{d '"+Left(tmp,4)+"-"+Substr(tmp,5,2)+"-"+Substr(tmp,7,2)+"'}"
                    Endif
                Otherwise
                    * ---- il formato timestamp di ODBC e' {ts '1996-09-16 12:34:56'}
                    *      senza AM o PM e sempre con quattro cifre nell' anno.
                    *      VFP invece da la data con 2 o 4 cifre a seconda del set century e
                    *      l' ora ha un AM o PM in fondo. L' ora 00:00:00 viene poi indicata con 12:00:00 AM
                    res=Ttoc(cs,1)
                    If i_ServerConn[1,6]='Interbase'
                        res="'"+Left(res,4)+"-"+Substr(res,5,2)+"-"+Substr(res,7,2)+" "+Substr(res,9,2)+":"+Substr(res,11,2)+":"+Right(res,2)+"'"
                    Else
                        res="{ts '"+Left(res,4)+"-"+Substr(res,5,2)+"-"+Substr(res,7,2)+" "+Substr(res,9,2)+":"+Substr(res,11,2)+":"+Right(res,2)+"'}"
                    Endif
            Endcase
    Endcase
    Return(res)

Func cp_ToStrODBCNull(cs)
    If Empty(cs)
        Return('NULL')
    Endif
    Return(cp_ToStrODBC(cs))

Func cp_ToDate(i_date)
    Local res
    res = i_date
    If Type('i_date')='T'
        res = Nvl(Ttod(i_date),{})
    Else
        If Isnull(i_date) And Type('i_date')='D'
            res = {}
        Endif
    Endif
    Return(res)

Func cp_InScreenPostIT(Name)
    * --- controlla se un form con questo nome gia' esiste
    Local i,res
    res=.F.
    For i=1 To _Screen.FormCount
        If _Screen.Forms[i].Class=='Postit'
            If _Screen.Forms[i].oPstCnt.cVar=Name
                res=.T.
            Endif
        Endif
    Next
    Return(res)

Proc cp_AskUser()
    Local i_nUsers
    i_nUsers=0
    Local i_oldlang
    i_oldlang=i_cLanguage
    * --- controlla se ci sono utenti
    If i_ServerConn[1,2]<>0
        =cp_SQL(i_ServerConn[1,2],'select count(*) as nusers from cpusers','__usr__')
    Else
        Select Count(*) As nusers From cpusers Where Not(Deleted()) Into Cursor __usr__
    Endif
    If Used('__usr__')
        i_nUsers=Iif(Type('__usr__.nusers')='C',Val(__usr__.nusers),__usr__.nusers)
        Use
    Endif
    * --- tenta di eseguire CP_LOGIN
    Private i_err,i_errsave
    i_err=''
    i_errsave=On('ERROR')
    On Error i_err=Iif(Empty(i_err),Message(),i_err)
    i_CODUTE=Iif(i_nUsers=0,1,0) && se non ci sono utenti la cp_login non e' obbligata a settare l' utente
    * Inizio Cp_Login fuori dall'eseguibile
    Local l_sPrg
    l_sPrg="cp_login()"
    &l_sPrg
    Release l_sPrg
    * Fine Cp_Login fuori dall'eseguibile
    On Error &i_errsave
    * --- gestione standard
    If "cp_login"$Lower(i_err)
        If i_nUsers=0
            i_CODUTE=1
            cp_FillGroupsArray()
            cp_set_get_right(.T.) && Zucchetti Aulla monitor framework
            Return
        Endif
        o=Createobject('stdsUserAuth')
        o.Show()
    Endif
    If i_oldlang='auto' And Empty(i_cLanguage)
        i_cLanguage='auto'
    Endif
    If i_CODUTE<>0
        * --- Zucchetti Aulla - Inizio Interfaccia (disabilito informazioni su toolbar)
        *oCpToolBar.boxute.value=MSG_TB_USER+chr(10)+alltrim(str(i_codute))
        *oCpToolBar.boxazi.value=MSG_TB_COMPANY+chr(10)+alltrim(i_codazi)
        * --- Zucchetti Aulla - Fine Interfaccia
        cp_FillGroupsArray()
        cp_set_get_right(.T.) &&  monitor framework
        *--- Carico configurazione interfaccia utente
        *l_sPrg="cp_LoadGUI" && Zucchetti Aulla commentate tre righe
        *DO (l_sPrg) WITH .Null. , 'U'
        *release l_sPrg
    Endif
    Return

Proc cp_ShowWarn(kk,i_IDX)
    Local i_oPostIT,i_oAC,i_bBody,i_cName,dStop,bCancel,i_nConn
    * Zucchetti Aulla Inizio - Schedulatore
    If Type('g_SCHEDULER')='C' And g_SCHEDULER='S'
        Return
    ENDIF
    * Zucchetti Aulla Fine - Schedulatore
    If Type("i_bDisablePostIn")='L' And i_bDisablePostIn
        Return
    Endif
    If i_ServerConn[i_TableProp[i_IDX,5],7]
        i_nConn=i_TableProp[i_IDX,3]
        If i_nConn<>0
            =cp_SQL(i_nConn,"select * from postit where code in (select warncode from cpwarn where tablecode="+cp_ToStrODBC(kk)+") and status like 'W%' and  (datestart <= "+cp_ToStrODBC(i_dPostInFilterDate)+" or datestart IS NULL) and (datestop >= "+cp_ToStrODBC(i_dPostInFilterDate)+" or datestop IS NULL)","postit_c")
        Else
            Select * From postit Where Code In (Select warncode From cpwarn Where tablecode=kk) And Status Like 'W%' And (TTOD(datestart) <=i_dPostInFilterDate Or Empty(datestart)) And (datestop >=i_dPostInFilterDate Or Empty(datestop)) Into Cursor postit_c
        Endif
        If Used('postit_c')
            Scan
                If Not(cp_InScreenPostIT(postit_c.Code))
                    bCancel=.F.
                    dStop = Iif(Isnull(postit_c.datestop) Or postit_c.datestop=Ctod('  /  /  '), Date(), postit_c.datestop)
                    i_oPostIT=Createobject('postit','set',i_TableProp[i_IDX,3])
                    If dStop<Date()
                        *---Post-IN scaduto
                        If cp_YesNo(MSG_POSTIN_EXPIRED_QM+Chr(10)+Chr(13)+MSG_DELETE_IT_QP ) Then
                            bCancel = .T.
                        Endif
                    Endif
                    If bCancel
                        i_oPostIT.Release()
                        If i_TableProp[i_IDX,3]<>0
                            =cp_SQL(i_TableProp[i_IDX,3],"delete from postit where code="+cp_ToStr(postit_c.Code))
                            =cp_SQL(i_TableProp[i_IDX,3],"delete from cpwarn where warncode='"+postit_c.Code+"'")
                        Else
                            Delete From postit Where Code=postit_c.Code
                            Delete From cpwarn Where warncode=postit_c.Code
                        Endif
                    Else
                        cp_Msg(cp_Translate(MSG_PRESS_ANY_KEY_TO_CONTINUE),.F.)
                        *--- Il postIT potrebbe essere stato cancellato utilizzando il bottone (F5) della toolbar
                        If Vartype(i_oPostIT)='O' And !Isnull(i_oPostIT)
                            i_oPostIT.Release()
                        Endif
                    Endif
                Else
                    cp_Msg(cp_Translate(MSG_WARNING_CL_MESSAGE_ALREADY_ON_SCREEN),.F.)
                Endif
            Endscan
            Use
        Endif
    Endif
Endproc

Func cp_KeyToSQL(i_xBaseKey,i_bUnique)
    Local i,p,i_SQLKey
    i_bUnique=.F.
    If Left(i_xBaseKey,7)="UNIQUE "
        i_xBaseKey=Substr(i_xBaseKey,8)
        i_bUnique=.T.
    Endif
    i_SQLKey=''
    i=At('+',i_xBaseKey)
    Do While i<>0
        p=Left(i_xBaseKey,i-1)
        i_SQLKey=cp_AddToSQLKey(p,i_SQLKey)
        i_xBaseKey=Substr(i_xBaseKey,i+1)
        i=At('+',i_xBaseKey)
    Enddo
    i_SQLKey=cp_AddToSQLKey(i_xBaseKey,i_SQLKey)
    Return(i_SQLKey)

Func cp_AddToSQLKey(p,s)
    Local c,i
    c=Iif(Empty(s),'',',')
    i=At('(',p)
    If i<>0
        p=Substr(p,i+1)
        i=1
        Do While Not(Inlist(Substr(p,i,1),")",",")) And (i<=Len(p))
            i=i+1
        Enddo
        p=Left(p,i-1)
    Endif
    Return(s+c+p)

Proc cp_KeyToVFP(i_cTable,i_cIdxExp,i_bUnique)
    Local p,i_cFld,i_cRes
    i_bUnique=.F.
    If Left(i_cIdxExp,7)="UNIQUE "
        i_cIdxExp=Substr(i_cIdxExp,8)
        i_bUnique=.T.
    Endif
    i_cRes=i_cIdxExp
    p=At(',',i_cIdxExp)
    If p<>0
        i_cFld=Left(i_cIdxExp,p-1)
        If i_Dcx.GetFieldIdx(i_cTable,i_cFld)<>0
            * --- il pezzo fino alla prima virgola e' un campo, quindi e' un indice in sintassi SQL
            i_cRes=''
            * --- e' un indice composto da piu' campi, quindi convertire in stringa
            Do While p<>0
                i_cFld=Left(i_cIdxExp,p-1)
                i_cRes=i_cRes+cp_AddVFPIdxFld(i_cTable,i_cFld)
                i_cIdxExp=Substr(i_cIdxExp,p+1)
                p=At(',',i_cIdxExp)
            Enddo
            i_cRes=Substr(i_cRes+cp_AddVFPIdxFld(i_cTable,i_cIdxExp),2)
        Endif
    Endif
    Return(i_cRes)

Func cp_AddVFPIdxFld(i_cTable,i_cFld)
    Local T,r,N
    N=i_Dcx.GetFieldIdx(i_cTable,i_cFld)
    r='+'+i_cFld
    T=i_Dcx.GetFieldType(i_cTable,N)
    Do Case
        Case T='N'
            r='+str('+i_cFld+','+Alltrim(Str(i_Dcx.GetFieldLen(i_cTable,N)))+','+Alltrim(Str(i_Dcx.GetFieldDec(i_cTable,N)))+')'
        Case T='D'
            r='+dtos('+i_cFld+')'
        Case T='T'
            r='+ttoc('+i_cFld+',1)'
    Endcase
    Return(r)

    * --- Zucchetti Aulla inizio - restituisce il tipo di un campo dal dizionario
Func cp_FieldType(i_cTable,i_cFld)
    Local nidxtable, nidxfield, cRes
    cRes = ""
    If Empty(i_cFld) And "." $ i_cTable
        i_cFld = Substr(i_cTable, At(".",i_cTable)+1)
        i_cTable = Left(i_cTable, At(".",i_cTable)-1)
    Endif
    nidxtable = i_Dcx.GetTableIdx(i_cTable)
    If nidxtable>0
        nidxfield = i_Dcx.GetFieldIdxIdx(nidxtable,i_cFld)
        If nidxfield>0
            cRes = i_Dcx.GetFieldType(i_cTable,nidxfield,nidxtable)
            cRes = cRes + "(" + Alltrim(Str(i_Dcx.GetFieldLen(i_cTable,nidxfield,nidxtable),4,0))
            If cRes = "N"
                cRes = cRes + "," + Alltrim(Str(i_Dcx.GetFieldDec(i_cTable,nidxfield,nidxtable),4,0))
            Endif
            cRes = cRes + ")"
        Endif
    Endif
    Return cRes
Endfunc
* --- Zucchetti Aulla fine

Func cp_NewCCChk()
    Local i,s
    If Type("__ccchk__")='U'
        Public __ccchk__
        =Rand(-1)
        __cccchk__=' '
    Endif
    s=''
    For i=1 To 10
        s=s+Chr(Int(Rand()*26+97))
    Next
    Return(s)

Proc cp_CheckDeletedKey(i_cFile,i_nCpRowNum,i_cK1,i_xV1,i_cK2,i_xV2,i_cK3,i_xV3,i_cK4,i_xV4,i_cK5,i_xV5,i_cK6,i_xV6,i_cK7,i_xV7,i_cK8,i_xV8,i_cK9,i_xV9,i_cK10,i_xV10,i_cK11,i_xV11,i_cK12,i_xV12)
    * --- questa routine controlla se esiste un record cancellato con la chiave specificata
    *     la INSERT-SQL di fox fallisce (uniquness violated) se esiste un record cancellato
    *     con la chiave che stiamo inserendo. Quindi bisogna "sporcare" la chiave del record
    *     cancellato.
    Local i_tmp,i_cDel,i,k,N,v,i_bNew
    LOCAL TestMacro
    i_tmp=Select()
    If Not(Used(i_cFile))
        i_bNew=.T.
        Select 0
        Use (i_cFile)
    Endif
    Select (i_cFile)
    If Lower(Tag(1))<>'idx1'
        * --- se il primo tag non e' idx1 e' una tabella esterna
        Select (i_tmp)
        Return
    Endif
    i_cDel=Set('DELETED')
    Set Deleted Off
    Set Order To idx1
    *i_oForm.LocateDeletedKey(i_nRowNum)
    k=Strtran(Key(),'CPROWNUM','I_NCPROWNUM')
    For i=1 To 12
        N=Alltrim(Str(i,2,0))
        If Type("i_cK"+N)='C'
            *v=i_cK&n
            *private ww_&v
            *ww_&v=i_xV&n
            TestMacro=i_cK&N
            If Upper(TestMacro)='CPROWNUM'
                i_cK&N='I_NCPROWNUM'
            Endif
            k=Strtran(k,Upper(i_cK&N),'i_xV'+N)
        Endif
    Next
    Seek &k
    If Found() And Deleted()
        *recall
        *replace CPCCCHK with iif(empty(CPCCCHK),'A',' ')
        Local k,p,N
        k=cp_KeyToSQL(Key())
        p=At(',',k)
        Do While p<>0
            N=Left(k,p-1)
            k=Substr(k,p+1)
            p=At(',',k)
            cp_ScrambleField(N)
        Enddo
        cp_ScrambleField(k)
    Endif
    Set Deleted &i_cDel
    If i_bNew
        Use
    Endif
    Select (i_tmp)
    Return

Proc cp_ScrambleField(cName)
    Local i_t,i_oe,i_i
    Private i_err
    i_oe=On('ERROR')
    On Error i_err=.T.
    i_t=Type(cName)
    For i_i=1 To 10  && tenta 10 scritture a caso, esce alla prima buona
        i_err=.F.
        Do Case
            Case i_t='C' Or i_t='M'
                Replace &cName With Left(Right(Sys(2015),4)+Right(Sys(2015),4)+Right(Sys(2015),4),Fsize(cName))
            Case i_t='N' Or i_t='Y'
                If Fsize(cName)<=2
                    Replace &cName With Int(Rand()*10^(Fsize(cName)))
                Else
                    Replace &cName With -Int(Rand()*(10^(Fsize(cName)-1)))
                Endif
            Case i_t='D' Or i_t='T'
                Replace &cName With Ctod('"'+Str(Int(Rand()*12)+1,2)+'-'+Str(Int(Rand()*12)+1,2)+'-'+Str(Int(Rand()*100),2)+'"')
        Endcase
        If !i_err
            On Error &i_oe
            Return
        Endif
    Next
    On Error &i_oe
    Return

Proc cp_StandardFunction(oObj,cCmd)
    Do Case
        Case cCmd='Query'
            oObj.Parent.oContained.ecpQuery()
        Case cCmd='Edit'
            oObj.Parent.oContained.ecpEdit()
        Case cCmd='Delete'
            oObj.Parent.oContained.ecpDelete()
        Case cCmd='Load'
            oObj.Parent.oContained.ecpLoad()
        Case cCmd='Save'
            oObj.Parent.oContained.ecpSave()
        Case cCmd='Quit'
            oObj.Parent.oContained.ecpQuit()
        Case cCmd='Help'
            oObj.Parent.oContained.ecpHelp()
        Case cCmd='PgUp'
            oObj.Parent.oContained.ecpPgUp()
        Case cCmd='PgDn'
            oObj.Parent.oContained.ecpPgDn()
        Case cCmd='ZoomPrev' Or cCmd='ZoomNext'
            Local i,j
            j=0
            For i=1 To oObj.Parent.ControlCount
                If oObj.Parent.Controls(i).Name==oObj.Name
                    j=i
                Endif
            Next
            If j<>0
                oObj.Parent.Controls(j+Iif(cCmd='ZoomNext',1,-1)).mZoom()
            Endif
    Endcase
    Return

Func cp_DBG(i_cMsg)
    Activate Screen
    ? i_cMsg
    Return(.T.)

Proc cp_AskProg(i_oObj,i_nConn,i_cTable,i_cProg)
    cp_NextProg_(i_oObj,i_nConn,i_cTable,i_cProg,.F.)
    Return

Proc cp_NextProg(i_oObj,i_nConn,i_cTable,i_cProg)
    cp_NextProg_(i_oObj,i_nConn,i_cTable,i_cProg,.T.)
    Return

Proc cp_NextProg_(i_oObj,i_nConn,i_cTable,i_cProg,i_bSetProg)
    Local i_i,i_n,i_cFilter,i_cProgFld
    LOCAL TestMacro
    * --- costruisce il "filtro"
    i_cFilter=''
    i_i=At(',',i_cProg)
    Do While i_i<>0
        i_n=Left(i_cProg,i_i-1)
        If Lower(Left(i_n,2))=='w_'
            i_n=Substr(i_n,3)
        Endif
        If i_nConn<>0
            i_cFilter=i_cFilter+Iif(Empty(i_cFilter),'',' and ')+i_n+'='+cp_ToStrODBC(i_oObj.w_&i_n)
        Else
            i_cFilter=i_cFilter+Iif(Empty(i_cFilter),'',' and ')+i_n+'=='+cp_ToStr(i_oObj.w_&i_n)
        Endif
        i_cProg=Substr(i_cProg,i_i+1)
        i_i=At(',',i_cProg)
    Enddo
    i_cProgFld=i_cProg
    If Lower(Left(i_cProgFld,2))=='w_'
        i_cProgFld=Substr(i_cProgFld,3)
    Endif
    * --- controlla se viene "forzato" un valore
    TestMacro=i_oObj.w_&i_cProgFld
    If !Empty(TestMacro) And i_bSetProg
        If Type("i_oObj.w_"+i_cProgFld)='C'
            i_l=Len(i_oObj.w_&i_cProgFld)
            i_oObj.w_&i_cProgFld=cp_StrZeroPad(Val(i_oObj.w_&i_cProgFld),i_l)
        Endif
        Return
    Endif
    * --- calcola il prossimo valore
    i_p=Select()
    If i_nConn<>0
        If !Empty(i_cFilter)
            cp_SQL(i_nConn,"select max("+i_cProgFld+") as nextval from "+i_cTable+" where "+i_cFilter,"__prog__")
        Else
            cp_SQL(i_nConn,"select max("+i_cProgFld+") as nextval from "+i_cTable,"__prog__")
        Endif
    Else
        If !Empty(i_cFilter)
            Select Max(&i_cProgFld) As Nextval From (i_cTable) Where Not(Deleted()) And &i_cFilter Into Cursor __prog__
        Else
            Select Max(&i_cProgFld) As Nextval From (i_cTable) Where Not(Deleted()) Into Cursor __prog__
        Endif
    Endif
    If Type("i_oObj.w_"+i_cProgFld)='C'
        i_l=Len(i_oObj.w_&i_cProgFld)
        i_r=cp_StrZeroPad(Val(Nvl(Nextval,''))+1,i_l)
        TestMacro=i_bSetProg And i_oObj.w_&i_cProgFld<>i_r
        If TestMacro
            cp_Msg(cp_msgformat(MSG_PROGRESSIVE__,i_r))
        Endif
    Else
        i_r=Nvl(Nextval,0)+1
        TestMacro=i_oObj.w_&i_cProgFld
        If i_bSetProg And TestMacro<>i_r
            cp_Msg(cp_msgformat(MSG_PROGRESSIVE__,Alltrim(Str(i_r))))
        Endif
    Endif
    Use
    Select (i_p)
    i_oObj.w_&i_cProgFld=i_r
    Return

Proc cp_AskTableProg(i_oObj,i_nConn,i_cProgId,i_cProg)
    cp_NextTableProg_(i_oObj,i_nConn,i_cProgId,i_cProg,.F.)
    Return

Proc cp_NextTableProg(i_oObj,i_nConn,i_cProgId,i_cProg,i_DisMsg)
    * Aggiunto il parametro i_DisMsg per disabilitare la visualizzazione del messaggio "Progressivo..."
    cp_NextTableProg_(i_oObj,i_nConn,i_cProgId,i_cProg,.T.,i_DisMsg)
    Return

Proc cp_GetDatabaseType(i_nConn)
    Local i,j
    j='Unknown'
    For i=1 To i_nServers
        If i_ServerConn[i,2]=i_nConn
            j=i_ServerConn[i,6]
        Endif
    Next
    Return(j)

Proc cp_NextTableProg_(i_oObj,i_nConn,i_cProgId,i_cProg,i_bSetProg,i_DisMsg)
    * Aggiunto il parametro i_DisMsg per disabilitare la visualizzazione del messaggio "Progressivo..."
    Local i_i,i_n,i_cFilter,i_cProgFld,i_v,i_cAzi
    LOCAL TestMacro
    i_cFilter=''
    i_cAzi=''
    i_i=At(',',i_cProg)
    * --- gestione del caso speciale 'guid'
    If i_cProgId='guid'
    TestMacro=i_oObj.&i_cProg
        If Empty(TestMacro)
            i_i=Len(i_oObj.&i_cProg)
            i_n=cp_NewCCChk()
            Do While Len(i_n)<i_i
                i_n=i_n+cp_NewCCChk()
            Enddo
            i_oObj.&i_cProg=Left(i_n,i_i)
        Endif
        Return
    Endif
    * ---
    Do While i_i<>0
        i_n=Left(i_cProg,i_i-1)
        If Lower(Left(i_n,2))=='w_'
            i_n=Substr(i_n,3)
            i_cFilter=i_cFilter+'\'+cp_ToStrODBC(i_oObj.w_&i_n)
        Endif
        If Lower(i_n)=='i_codazi'
            * --- Zucchetti Aulla Aggiunto Alltrim a i_codazi
            i_cFilter=i_cFilter+'\'+cp_ToStrODBC(Alltrim(i_CODAZI))
            i_cAzi=i_CODAZI
        Endif
        i_cProg=Substr(i_cProg,i_i+1)
        i_i=At(',',i_cProg)
    Enddo
    i_cProgFld=i_cProg
    If Lower(Left(i_cProgFld,2))=='w_'
        i_cProgFld=Substr(i_cProgFld,3)
    Endif
    * --- ore sistema il progressivo
    Local i_cProgKey,i_cProgValue,i_cProgType,i_p
    i_p=Select()
    i_cProgType=Type("i_oObj.w_"+i_cProgFld)
    * --- Se il tipo � Undefined allora provo a valorizzare i_cProgValue senza riferimento all'oggetto
    If i_cProgType<>'U'
        i_cProgValue=i_oObj.w_&i_cProgFld
    Else
        i_cProgType=Type("w_"+i_cProgFld)
        i_cProgValue=w_&i_cProgFld
    Endif
    i_nProgValue=Iif(i_cProgType='C',Val(i_cProgValue),i_cProgValue)
    i_cProgKey="prog\"+i_cProgId+i_cFilter
    TestMacro=!Empty(i_cProgValue) And (Type("i_oObj.op_"+i_cProgFld)='U' Or (i_cProgValue<>i_oObj.op_&i_cProgFld)) And i_bSetProg && solo se sto scrivendo
    If TestMacro
        * --- forza il progressivo
        If i_nProgValue>0
            If i_nConn<>0
                cp_SQL(i_nConn,"select autonum from cpwarn where tablecode="+cp_ToStrODBC(i_cProgKey),"__prog__")
            Else
                Select autonum From cpwarn Where tablecode==i_cProgKey Into Cursor __prog__
            Endif
            If Eof()
                If i_nConn<>0
                    cp_TrsSQL(i_nConn,"insert into cpwarn (tablecode,warncode,autonum) values ("+cp_ToStrODBC(i_cProgKey)+","+Iif(Empty(i_cAzi),"'xxx'",cp_ToStrODBC(i_cAzi))+","+cp_ToStrODBC(i_nProgValue)+")")
                Else
                    Insert Into cpwarn (tablecode,warncode,autonum) Values (i_cProgKey,Iif(Empty(i_cAzi),'xxx',i_cAzi),i_nProgValue)
                Endif
                i_r=Iif(i_cProgType='C',cp_StrZeroPad(i_nProgValue,Len(i_cProgValue)),i_nProgValue)
            Else
                If i_nProgValue>autonum
                    If i_nConn<>0
                        cp_TrsSQL(i_nConn,"update cpwarn set autonum="+cp_ToStrODBC(i_nProgValue)+" where tablecode="+cp_ToStrODBC(i_cProgKey))
                    Else
                        Update cpwarn Set autonum=i_nProgValue Where tablecode=i_cProgKey
                    Endif
                Endif
                i_r=Iif(i_cProgType='C',cp_StrZeroPad(i_nProgValue,Len(i_cProgValue)),i_nProgValue)
            Endif
            Use
        Else
            i_r=Iif(i_cProgType='C',i_cProgValue,i_nProgValue)
        Endif
    Else
        * --- cerca il prossimo valore
        i_n=1
        If i_bSetProg
            If i_nConn<>0
                i_n=cp_TrsSQL(i_nConn,"update cpwarn set autonum=autonum+1 where tablecode="+cp_ToStrODBC(i_cProgKey))
            Else
                Update cpwarn Set autonum=autonum+1 Where tablecode=i_cProgKey
                i_n=_Tally
            Endif
        Endif
        If i_n=0
            * --- nuovo valore
            If i_nConn<>0
                cp_TrsSQL(i_nConn,"insert into cpwarn (tablecode,warncode,autonum) values ("+cp_ToStrODBC(i_cProgKey)+","+Iif(Empty(i_cAzi),"'xxx'",cp_ToStrODBC(i_cAzi))+",1)")
            Else
                Insert Into cpwarn (tablecode,warncode,autonum) Values (i_cProgKey,Iif(Empty(i_cAzi),'xxx',i_cAzi),1)
            Endif
            i_r=Iif(i_cProgType='C',cp_StrZeroPad(1,Len(i_cProgValue)),1)
        Else
            * --- legge il valore per il progressivo
            If i_nConn<>0
                If i_bSetProg
                    cp_SQL(i_nConn,"select autonum as nextval from cpwarn where tablecode="+cp_ToStrODBC(i_cProgKey),"__tmpanum__")
                Else
                    * --- legge il valore per proporre il progressivo di default, senza rispettare i lock
                    Local db
                    db=cp_GetDatabaseType(i_nConn)
                    Do Case
                        Case db='SQLServer'
                            cp_SQL(i_nConn,"select autonum as nextval from cpwarn (nolock) where tablecode="+cp_ToStrODBC(i_cProgKey),"__tmpanum__")
                        Otherwise
                            cp_SQL(i_nConn,"select autonum as nextval from cpwarn where tablecode="+cp_ToStrODBC(i_cProgKey),"__tmpanum__")
                    Endcase
                Endif
            Else
                Select autonum As Nextval From cpwarn Where tablecode=i_cProgKey Into Cursor __tmpanum__
            Endif
            If i_bSetProg
                * --- progressivo in assegnamento
                i_r=Iif(i_cProgType='C',cp_StrZeroPad(Nvl(Nextval,0),Len(i_cProgValue)),Nvl(Nextval,0))
                Use
            Else
                * --- progressivo in lettura (puo' fallire se la tabella progressivi e' vuota)
                If Used("__tmpanum__")
                    i_r=Iif(i_cProgType='C',cp_StrZeroPad(Nvl(Nextval,0)+1,Len(i_cProgValue)),Nvl(Nextval,0)+1)
                    Use
                Else
                    i_r=Iif(i_cProgType='C',cp_StrZeroPad(1,Len(i_cProgValue)),1)
                Endif
            Endif
        Endif
    Endif
    * Zucchetti Aulla Inizio: calcolo prefisso autonumber per Logistica remota
    If g_APPLICATION="ADHOC REVOLUTION" And g_LORE = 'S' And Alen(g_aLoreProg)>0 And Ascan(g_aLoreProg, i_cProgId)<>0 And Not Empty(g_cPrefiProg) And i_cProgType='C'
        Local i_cProgPrefixValue
        i_cProgPrefixValue = Alltrim(g_cPrefiProg)+ Alltrim(Substr(Alltrim(i_r),3))
    Else
        i_cProgPrefixValue = i_r
    Endif
    * Zucchetti Aulla Fine
    Select (i_p)
    If i_r<>i_cProgValue And i_bSetProg
        *Disabilita il messaggio
        If !i_DisMsg
            If i_cProgType='C'
                cp_Msg(cp_msgformat(MSG_PROGRESSIVE__,i_r))
            Else
                cp_Msg(cp_msgformat(MSG_PROGRESSIVE__,Alltrim(Str(i_r))))
            Endif
        Endif
        If Type("i_oObj.op_"+i_cProgFld)<>'U'
            * Zucchetti Aulla inizio: remmato codice e sostituito con assegnamento: i_oObj.op_&i_cProgFld = i_cProgPrefixValue
            *i_oObj.op_&i_cProgFld=i_r && setta il valore anche per la op_ per evitare il problema che nasce dall' eventuale fallimento della transazione
            && il progressivo deve restare come "non forzato" anche in caso di ripetizione della transazione
            i_oObj.op_&i_cProgFld = i_cProgPrefixValue
            * Zucchetti Aulla fine
        Endif
    Endif
    * --- Zucchetti Aulla Inizio
    If Type("i_oObj.w_"+i_cProgFld) <> 'U'
        * Zucchetti Aulla Inizio: remmato assegnamento e sostituito con quello successivo ed aggiunto chiamata a SetControlValues
        *i_oObj.w_&i_cProgFld=i_r
        i_oObj.w_&i_cProgFld=i_cProgPrefixValue
        i_oObj.SetControlsValue()
        * Zucchetti Aulla fine
    Else
        w_&i_cProgFld=i_r
    Endif
    * --- Zucchetti Aulla commentate due righe sotto se lancio calcolo autonum da funzione
    *i_oObj.w_&i_cProgFld=i_r
    *i_oObj.SetControlsValue()
    Return

Func cp_StrZeroPad(i_nValue,i_nLen)
    * Nel caso la cifra superi i 10 caratteri la Str, senza sepcificare la lunghezza, la trasforma in formato esponenziale
    Return(Right(Replicate('0',i_nLen)+Alltrim(Str(i_nValue,i_nLen)),i_nLen))

Proc cp_GetSecurity(i_oObj,i_cCode)
    * --- default, si accede
    i_oObj.bSec1=.T.
    i_oObj.bSec2=.T.
    i_oObj.bSec3=.T.
    i_oObj.bSec4=.T.
    Local i_oldarea
    i_oldarea = Select()
    If i_oObj.bFox26=.T. Or (Type('i_bFox26')='L' And i_bFox26)
        Return
    Endif
    If cp_IsAdministrator()
        Select(i_oldarea)
        Return
    Endif
    Local i_shortCode,i_p, i_clprogname,i_clcCode,i_clshortCode
    If At('cp_query',Lower(Set('proc')))=0
        Set Proc To cp_query Additive
    Endif
    i_p=At("_",i_cCode)
    If i_p<>0
        i_shortCode=Left(i_cCode,i_p-1)+'*'
    Else
        i_shortCode=i_cCode
    Endif
    i_clprogname  = 'LOWER(progname)'
    i_clcCode     = cp_ToStrODBC(Lower(i_cCode))
    i_clshortCode = cp_ToStrODBC(Lower(i_shortCode))
    * --- controlla se ci sono definizioni della sicurezza per questo programma
    *** Zucchetti Aulla - modifiche mobile controllo l'accesso per utente mobile
    *** se non esiste definizione di sicurezza e se non c'� una gestione nel modulo mobyle deve essere bloccato l'accesso
    If i_CODUTE>0 And i_bMobileMode And Inlist(Lower(i_oObj.ParentClass),'stdform','stdtrsform') And At("moby\",Lower(i_oObj.ClassLibrary))=0 And Not(Type("i_oObj.cPrg")='C' And (Lower(i_oObj.cPrg)='gs___ute' or Lower(i_oObj.cPrg)='gsrv_ute'))
        *** lavoro in modalit� restrittiva nelle form per cui definisco che ci sono sempre sicurezze impostate
        i_oObj.bSec1=.F.
        i_oObj.bSec2=.F.
        i_oObj.bSec3=.F.
        i_oObj.bSec4=.F.
    Else
        If i_ServerConn[1,2]<>0
            *** Zucchetti Aulla - modifiche mobile controllo l'accesso per utente mobile
            cp_sqlexec(i_ServerConn[1,2],"select count(*) as cnt from cpprgsec where " + Iif(i_bMobileMode,"","grpcode>=0 and usrcode>=0 and ") +"("+i_clprogname+"="+i_clcCode+Iif(i_clshortCode<>i_clcCode," or "+i_clprogname+"="+i_clshortCode,"")+")",'__sec__')
        Else
            Select Count(*) As Cnt From cpprgsec Where Lower(progname)=Lower(i_cCode) Or Lower(progname)=Lower(i_shortCode) Into Cursor __sec__
        Endif
        If Used('__sec__') And Iif(Type('__sec__.cnt')='C',Val(__sec__.Cnt),__sec__.Cnt)>0
            * --- ci sono definizioni, di default non si accede
            i_oObj.bSec1=.F.
            i_oObj.bSec2=.F.
            i_oObj.bSec3=.F.
            i_oObj.bSec4=.F.
            Select __sec__
            Use
        Else
            * --- non ci sono definizioni, libero accesso
            If Used('__sec__')
                Select __sec__
                Use
            Endif
            Select(i_oldarea)
            Return
        Endif
    Endif
    * --- controlla la sicurezza dell' utente
    Local i_grparr
    i_grparr=cp_getGroupsArray()
    If i_ServerConn[1,2]<>0
        *** Zucchetti Aulla - modifiche mobile controllo l'accesso per utente mobile
        If i_bMobileMode
            cp_SQL(i_ServerConn[1,2],"select sum(sec1) as s1,sum(sec2) as s2,sum(sec3) as s3,sum(sec4) as s4 from cpprgsec "+;
                " where (((grpcode in ("+i_grparr+") or grpcode/-10 in ("+i_grparr+")) and grpcode<>0) or usrcode="+cp_ToStrODBC(i_CODUTE)+" or usrcode="+cp_ToStrODBC(-10*i_CODUTE)+") and ("+i_clprogname+"="+i_clcCode+Iif(i_clshortCode<>i_clcCode," or "+i_clprogname+"="+i_clshortCode+")",")"),'__sec__')
        Else
            cp_SQL(i_ServerConn[1,2],"select sum(sec1) as s1,sum(sec2) as s2,sum(sec3) as s3,sum(sec4) as s4 from cpprgsec "+;
                " where ((grpcode in ("+i_grparr+") and grpcode<>0) or usrcode="+cp_ToStrODBC(i_CODUTE)+") and ("+i_clprogname+"="+i_clcCode+Iif(i_clshortCode<>i_clcCode," or "+i_clprogname+"="+i_clshortCode+")",")"),'__sec__')
        Endif
    Else
        Select Sum(sec1) As s1,Sum(sec2) As s2 ,Sum(sec3) As s3,Sum(sec4) As s4 From cpprgsec ;
            where ((grpcode In (&i_grparr) And grpcode<>0) Or usrcode=i_CODUTE) And (Trim(Lower(progname))==Trim(Lower(i_cCode) Or Lower(progname)=Lower(i_shortCode)));
            Into Cursor __sec__
    Endif
    If Used('__sec__')
        Select __sec__
        If !Eof()
            i_oObj.bSec1=Nvl(__sec__.s1,0)>0
            i_oObj.bSec2=Nvl(__sec__.s2,0)>0
            i_oObj.bSec3=Nvl(__sec__.s3,0)>0
            i_oObj.bSec4=Nvl(__sec__.s4,0)>0
        Endif
        Use
    Endif
    Select(i_oldarea)
    Return

Func cp_IsAdministrator(bDontAsk, nCode)
    Local bRes, i_oldarea, VarIsAdministrator, nCodute
    bRes=.F.
    nCodute=Evl(nCode,i_CODUTE)
    VarIsAdministrator='g_cp_IsAdministrator_'+Alltrim(Str(i_CODUTE))
    If Type(VarIsAdministrator)='L' And nCodute==i_CODUTE
        bRes=&VarIsAdministrator
    Else
        i_oldarea=Select()
        If i_ServerConn[1,2]<>0
            cp_SQL(i_ServerConn[1,2],"select count(*) as cnt from cpusrgrp where groupcode=1 and usercode="+cp_ToStrODBC(nCodute),'__sec__')
        Else
            Select Count(*) As Cnt From cpusrgrp Where groupcode=1 And usercode=nCodute Into Cursor __sec__
        Endif
        If Used('__sec__')
            bRes=Iif(Type('__sec__.cnt')='C',Val(__sec__.Cnt),__sec__.Cnt)=1
            Use
            If !bRes And !bDontAsk
                * --- l' utente corrente non e' un amministratore, ma c'e' almeno un amministratore?
                If i_ServerConn[1,2]<>0
                    cp_SQL(i_ServerConn[1,2],"select count(*) as cnt from cpusrgrp where groupcode=1",'__sec__')
                Else
                    Select Count(*) As Cnt From cpusrgrp Where groupcode=1 Into Cursor __sec__
                Endif
                If Used('__sec__')
                    bRes=Iif(Type('__sec__.cnt')='C',Val(__sec__.Cnt),__sec__.Cnt)=0
                    Use
                    If bRes
                        Private i_bIsAdmin
                        i_bIsAdmin=''
                        o=Createobject('askadminpassword')
                        o.Show()
                        o=.Null.
                        bRes=i_bIsAdmin
                    Endif
                Endif
            Endif
        Endif
        Select(i_oldarea)
        If !bDontAsk And nCodute==i_CODUTE
            *** Se Viene creata la variabile su db vuoto e pwd superuser non
            *** richiesta l'utente 1 e 0 (utilizzati per la creazione degli altri utenti)
            *** non saranno amministratori impedendo la creazione di qualsiasi utente
            Public &VarIsAdministrator
            &VarIsAdministrator = bRes
        Endif
    Endif
    Return(bRes)

    * Ritorna true se sei uno sviluppatore
    * Ritorna true se hai tutti i diritti
Function cp_IsDeveloper()
    * --- Se definita la variabile globale il sistema � attivo altrimenti no
    If Vartype(i_MonitorFramework)='L' And i_MonitorFramework=.T.
        Return cp_set_get_right()>4
    Else
        Return(.T.)
    Endif
Endfunc


*---
* Creazione e\o recupero diritti per l'utente corrente
* Ritorna il livello di diritto dell'utente
*---
Function cp_set_get_right(forced)
    Local __tmp__right
    If Vartype(i_MonitorFramework)='L' And i_MonitorFramework=.T.
        * Se il controllo � forzato o se non esiste la creo
        If forced Or Vartype(g_user_right)=='U'
            Public g_user_right
            g_user_right = 1
            If i_ServerConn[1,2]<>0
                =cp_SQL(i_ServerConn[1,2],'select * from cpusers where code='+cp_ToStr(i_CODUTE),'__tmp__right')
            Else
                Select * From cpusers Where Code=i_CODUTE Into Cursor __tmp__right
            Endif
            Used('__tmp__right')
            g_user_right = __tmp__right.userright
            Use In Select('__tmp__right')
            If g_user_right>5 Or g_user_right<1
                g_user_right = 1
            Endif
        Endif
    Else
        * --- Se monitor disattivo tutte le sicurezza aperte
        If Vartype(g_user_right)=='U'
            Public g_user_right
            g_user_right = 5
        Else
            g_user_right = 5
        Endif
    Endif
    Return g_user_right
Endfunc

Define Class askadminpassword As Form
    WindowType=1
    Caption=''
    AutoCenter=.T.
    Width=250
    Height=50
    Icon=i_cBmpPath+i_cStdIcon
    Add Object lbl As Label With Caption='',Top=12,Alignment=1,Width=70
    Add Object txt As TextBox With Left=70,Width=100,Top=10,PasswordChar='*'
    Proc txt.Valid()
        If Lower(Trim(This.Value))==i_cSuperPwd
            i_bIsAdmin=.T.
        Else
            i_bIsAdmin=.F.
        Endif
        Thisform.Hide()

        **traduzioni
    Procedure Init
        This.Caption=cp_Translate(MSG_SYSTEM_ADMINISTRATOR)
        This.lbl.Caption=cp_Translate(MSG_PASSWORD)+cp_Translate(MSG_FS)
        DoDefault()
    Endproc

Enddefine

Func cp_FillGroupsArray()
    If Vartype(i_aUsrGrps)='U' Or i_aUsrGrps[1]=0 Or (i_GROUPROLE<>0 And (i_aUsrGrps[1]<>i_GROUPROLE))
        Release i_aUsrGrps
        * --- non sono mai stati chiesti i gruppi per questo utente,
        *     gli chiede e memorizza in un array per non doverli piu' chiedere
        Public i_aUsrGrps,i_aUsrRoles
        Dimension i_aUsrGrps[1]
        i_aUsrGrps[1]=-1000
        Dimension i_aUsrRoles[1]
        i_aUsrRoles[1]=''
        Local i,j
        If i_ServerConn[1,2]<>0
            =cp_SQL(i_ServerConn[1,2],"select groupcode,name,grptype from cpusrgrp,cpgroups where cpgroups.code=cpusrgrp.groupcode and usercode="+cp_ToStrODBC(i_CODUTE)+" order by groupcode","__grp__")
        Else
            Select groupcode,Name,grptype From cpusrgrp,cpgroups Where cpgroups.Code=cpusrgrp.groupcode And usercode=i_CODUTE Into Cursor __grp__ Order By groupcode
        Endif
        If Used('__grp__')
            If i_GROUPROLE<>0
                i_aUsrGrps[1]=i_GROUPROLE
                i=1
            Else
                i=0
            Endif
            j=0
            Select __grp__
            Scan
                If i_GROUPROLE<>__grp__.groupcode
                    i=i+1
                    Dimension i_aUsrGrps[i]
                    i_aUsrGrps[i]=__grp__.groupcode
                Endif
                If __grp__.grptype='R'
                    j=j+1
                    Dimension i_aUsrRoles[j]
                    i_aUsrRoles[j]=__grp__.Name
                Endif
            Endscan
            Use
            If i=0
                Dimension i_aUsrGrps[1]
                i_aUsrGrps[1]=0
                Dimension i_aUsrRoles[1]
                i_aUsrRoles[1]=''
            Endif
        Endif
    Endif
    Return

Func cp_getGroupsArray()
    cp_FillGroupsArray()
    Local i,i_res
    i_res=''
    For i=1 To Alen(i_aUsrGrps)
        i_res=i_res+','+Alltrim(Str(i_aUsrGrps[i]))
    Next
    Return(Substr(i_res,2))

Func cp_getRolesArray()
    cp_FillGroupsArray()
    Local i,i_res
    i_res=''
    For i=1 To Alen(i_aUsrRoles)
        i_res=i_res+",'"+i_aUsrRoles[i]+"'"
    Next
    Return(Substr(i_res,2))

Func IsInRole(i_cRole)
    cp_FillGroupsArray()
    Local i,i_res
    i_res=.F.
    For i=1 To Alen(i_aUsrRoles)
        If Trim(i_aUsrRoles[i])==Trim(i_cRole)
            i_res=.T.
        Endif
    Next
    Return(i_res)

    *---
    *   Funzione cp_FindNamedDefaultFile riscritta per integrazione e potenziamento Framework monitor
    *   La ricerca dentro la cartella default � condizionata dal valore della variabile g_folder_default
    *   se � .f. la ricerca non viene effettuata.
    *---
Func cp_FindNamedDefaultFile(cName,cExt)
    Local result
    If Vartype(i_oCacheFile)='O'
        result=i_oCacheFile.GetNameFile(Forceext(cName,cExt))
        If Not Empty(result)
            result=Iif(result="##FileNotFound##","",result)
            Return (result)
        Endif
    Endif
    If Left(cName,3)='..\'
        result=cp_FindNamedDefaultFile(Right(cName,Len(cName)-3),cExt)
        If Not Empty(result)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(cName,cExt),result)
            Endif
            Return (result)
        Endif
    Endif
    * --- Zucchetti Aulla - se attiva gestione mobile ricerco se file modificato per mobile
    If i_bMobileMode And Not('moby\'$cName)
        result=cp_FindNamedDefaultFile(Iif(g_ADHOCONE,'..\','')+'..\moby\exe\'+Justfname(cName),cExt)
        If Not Empty(result)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(cName,cExt),result)
            Endif
            Return (result)
        Endif
    Endif
    If i_MonitorFramework
        Local i
        If Empty(Alltrim(cName))
            Return ("")
        Endif
        * Cerco il file per utente
        If cp_IsPFile(cName+'_'+Alltrim(Str(i_CODUTE))+'.'+cExt)
            * Controllo la presenza del carattere '|' all'interno di i_pathFindFile
            If At("|",Alltrim(i_pathFindFile))!=0
                * Ritorno cName+'_'+ALLTRIM(STR(i_codute) perch� ho trovato il file, ma non conosco il percorso esatto
                If Vartype(i_oCacheFile)='O'
                    i_oCacheFile.PutNameFile(Forceext(cName,cExt),cName+'_'+Alltrim(Str(i_CODUTE)))
                Endif
                Return(cName+'_'+Alltrim(Str(i_CODUTE)))
            Else
                * Ritorno il percorso esatto recuperato dalla variabile i_pathFindFile senza estensione
                If Vartype(i_oCacheFile)='O'
                    i_oCacheFile.PutNameFile(Forceext(cName,cExt),Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                Endif
                Return(Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
            Endif
        Endif
        * Controllo se occorre gestire anche la cartella 'default'
        If Vartype(g_folder_default)='L' And g_folder_default == .T.
            If cp_IsPFile('default\'+cName+'_'+Alltrim(Str(i_CODUTE))+'.'+cExt)
                If At("|",Alltrim(i_pathFindFile))!=0
                    If Vartype(i_oCacheFile)='O'
                        i_oCacheFile.PutNameFile(Forceext(cName,cExt),'default\'+cName+'_'+Alltrim(Str(i_CODUTE)))
                    Endif
                    Return('default\'+cName+'_'+Alltrim(Str(i_CODUTE)))
                Else
                    If Vartype(i_oCacheFile)='O'
                        i_oCacheFile.PutNameFile(Forceext(cName,cExt),Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                    Endif
                    Return(Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                Endif
            Endif
        Endif
        If i_CODUTE<>0 && se l' utente e' gia' stato caricato, si possono provare i default per utente e gruppi di appartenenza
            cp_FillGroupsArray()
            For i=1 To Alen(i_aUsrGrps)
                If cp_IsPFile(cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt)
                    If At("|",Alltrim(i_pathFindFile))!=0
                        If Vartype(i_oCacheFile)='O'
                            i_oCacheFile.PutNameFile(Forceext(cName,cExt),cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                        Endif
                        Return(cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                    Else
                        If Vartype(i_oCacheFile)='O'
                            i_oCacheFile.PutNameFile(Forceext(cName,cExt),Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                        Endif
                        Return(Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                    Endif
                Endif
            Next
            If Vartype(g_folder_default)='L' And g_folder_default == .T.
                For i=1 To Alen(i_aUsrGrps)
                    If cp_IsPFile('default\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt)
                        If At("|",Alltrim(i_pathFindFile))!=0
                            If Vartype(i_oCacheFile)='O'
                                i_oCacheFile.PutNameFile(Forceext(cName,cExt),'default\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                            Endif
                            Return('default\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                        Else
                            If Vartype(i_oCacheFile)='O'
                                i_oCacheFile.PutNameFile(Forceext(cName,cExt),Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                            Endif
                            Return(Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                        Endif
                    Endif
                Next
            Endif
        Endif
        If cp_IsPFile(cName+'.'+cExt)
            If At("|",Alltrim(i_pathFindFile))!=0
                If Vartype(i_oCacheFile)='O'
                    i_oCacheFile.PutNameFile(Forceext(cName,cExt),cName)
                Endif
                Return(cName)
            Else
                If Vartype(i_oCacheFile)='O'
                    i_oCacheFile.PutNameFile(Forceext(cName,cExt),Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                Endif
                Return(Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
            Endif
        Endif
        If Vartype(g_folder_default)='L' And g_folder_default == .T.
            If cp_IsPFile('default\'+cName+'.'+cExt)
                If At("|",Alltrim(i_pathFindFile))!=0
                    If Vartype(i_oCacheFile)='O'
                        i_oCacheFile.PutNameFile(Forceext(cName,cExt),'default\'+cName)
                    Endif
                    Return('default\'+cName)
                Else

                    If Vartype(i_oCacheFile)='O'
                        i_oCacheFile.PutNameFile(Forceext(cName,cExt),Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                    Endif
                    Return(Addbs(Lower(Alltrim(Justpath(i_pathFindFile))))+Lower(Alltrim(Juststem(i_pathFindFile))))
                Endif
            Endif
        Endif
        * --- Cerco il i_cpdic.vmn al livello superiore
        If cp_fileexist('..\'+cName+'.'+cExt,.T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(cName,cExt),'..\'+cName)
            Endif
            Return('..\'+cName)
        Endif
        Return('')
    Else
        Local i
        If cp_IsPFile(cName+'_'+Alltrim(Str(i_CODUTE))+'.'+cExt)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(cName,cExt),cName+'_'+Alltrim(Str(i_CODUTE)))
            Endif
            Return(cName+'_'+Alltrim(Str(i_CODUTE)))
        Endif
        * Controllo se occorre gestire anche la cartella 'default'
        If Vartype(g_folder_default)='L' And g_folder_default == .T.
            If cp_IsPFile('default\'+cName+'_'+Alltrim(Str(i_CODUTE))+'.'+cExt)
                If Vartype(i_oCacheFile)='O'
                    i_oCacheFile.PutNameFile(Forceext(cName,cExt),'default\'+cName+'_'+Alltrim(Str(i_CODUTE)))
                Endif
                Return('default\'+cName+'_'+Alltrim(Str(i_CODUTE)))
            Endif
        Endif
        If i_CODUTE<>0 && se l' utente e' gia' stato caricato si possono provare i default per utente e gruppi di appartenenza
            cp_FillGroupsArray()
            For i=1 To Alen(i_aUsrGrps)
                If cp_IsPFile(cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt)
                    If Vartype(i_oCacheFile)='O'
                        i_oCacheFile.PutNameFile(Forceext(cName,cExt),cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                    Endif
                    Return(cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                Endif
            Next
            * Controllo se occorre gestire anche la cartella 'default'
            If Vartype(g_folder_default)='L' And g_folder_default == .T.
                For i=1 To Alen(i_aUsrGrps)
                    If cp_IsPFile('default\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt)
                        If Vartype(i_oCacheFile)='O'
                            i_oCacheFile.PutNameFile(Forceext(cName,cExt),'default\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                        Endif
                        Return('default\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                    Endif
                Next
            Endif
        Endif
        If cp_IsPFile(cName+'.'+cExt)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(cName,cExt),cName)
            Endif
            Return(cName)
        Endif
        * Controllo se occorre gestire anche la cartella 'default'
        If Vartype(g_folder_default)='L' And g_folder_default == .T.
            If cp_IsPFile('default\'+cName+'.'+cExt)
                If Vartype(i_oCacheFile)='O'
                    i_oCacheFile.PutNameFile(Forceext(cName,cExt),'default\'+cName)
                Endif
                Return('default\'+cName)
            Endif
        Endif
        * --- Cerco il i_cpdic.vmn al livello superiore
        If cp_fileexist('..\'+cName+'.'+cExt,.T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(cName,cExt),'..\'+cName)
            Endif
            Return('..\'+cName)
        Endif
        Return('')
        * --- Zucchetti Aulla Inizio - Framework Monitor
    Endif
Endfunc
* --- Zucchetti Aulla Fine - Framework Monitor

Func cp_FindDefault(cExt)
    Return(cp_FindNamedDefaultFile('default',cExt))

Proc cp_backgroundmask(bNoWaitWindow)
    Local F
    If Vartype(i_oVfmManager)<>'U'
        Release i_oVfmManager
    Endif
    If Not bNoWaitWindow
        cp_Msg(cp_Translate(MSG_LOADING_BACKGROUNDMASK),.T.,.F.)
    Endif
    If Vartype(i_bShowStartScreen)<>'U' And i_bShowStartScreen
        F=cp_FindNamedDefaultFile('start','vfm')
        If Empty(F)
            F=cp_FindDefault('vfm')
        Endif
    Else
        F=cp_FindDefault('vfm')
    Endif
    If Not(Empty(F))
        vm_exec(F)
    Endif
    Return

Proc vx_exec(cProg,oObj)
    Local i_cExt,i_p,i_bPreview,i_asci,i_ascifile,i_cQry,i_olderr,i_batchmodequery, i_bWinState
    Private i_err
    cProg=Trim(cProg)
    i_bPreview=.F.
    i_batchmodequery=Iif(Type('i_bDisableAsyncConn')='L',i_bDisableAsyncConn,.F.)
    If Upper(Right(cProg,8))=' PREVIEW'
        i_bPreview=.T.
        cProg=Left(cProg,Len(cProg)-8)
    Endif
    * --- Zucchetti Aulla inizio - Gestione maschera massimizzata per grafico vfc
    i_bWinState=.F.
    If Upper(Right(cProg,10))=' MAXIMIZED'
        i_bWinState=.T.
        cProg=Left(cProg,Len(cProg)-10)
    Endif
    * --- Zucchetti Aulla Fine
    i_asci=.F.
    If At(' ASCII ',cProg)<>0
        i_asci=.T.
        i_ascifile=Substr(cProg,At(' ASCII ',cProg)+7)
        cProg=Left(cProg,At(' ASCII ',cProg)-1)
    Endif
    * --- Zucchetti Aulla inizio - Gestione query InfoPublisher
    If Right(cProg,1) = ','
        cProg=Left(cProg,Len(cProg)-1)
    Endif
    * --- Zucchetti Aulla Fine
    i_cExt = Lower(Justext(cProg))
    Do Case
            * --- Zucchetti Aulla inizio - Gestione query InfoPublisher
        Case i_cExt="irp"
            * Eseguo una Query InfoPublisher
            If Type('g_IRDR')='C' And g_IRDR = 'S'
                cProg=Iif(At(',',cProg)>0,Left(cProg,At(',',cProg)-1),cProg)
                l_sPrg="cp_chprn"
                Do (l_sPrg) With cProg,'I',oObj
            Endif
            * --- Zucchetti Aulla fine
        Case i_cExt="vqr"
            * Eseguo una Query
            * Zucchetti Aulla inizio
            * vq_exec(left(cProg,len(cProg)-4),oObj,.f.,.f.,i_bPreview,i_batchmodequery,i_asci,i_ascifile)
            vq_exec(Left(cProg,Len(cProg)-4),oObj,.F.,.F.,i_bPreview,Iif(Type('oObj.sync')<>'U',.T.,i_batchmodequery),i_asci,i_ascifile)
            * Zucchetti Aulla fine
        Case i_cExt="vfm"
            * Eseguo una Maschera interpretata
            vm_exec(Left(cProg,Len(cProg)-4),oObj)
        Case i_cExt="vzm"
            * Eseguo uno Zoom
            vz_exec(Left(cProg,Len(cProg)-4),oObj)
        Case i_cExt="frx" Or i_cExt="fxp"
            * Esegue una Query specificando il Report (separati da virgola)
            i_p=At(',',cProg)
            If i_p<>0
                * Zucchetti Aulla inizio
                If i_cExt<>"fxp"
                    * vq_exec(left(cProg,i_p-5),oObj,.f.,substr(cProg,i_p+1,len(cProg)-4-i_p),i_bPreview,i_batchmodequery,i_asci,i_ascifile)
                    vq_exec(Left(cProg,i_p-5),oObj,.F.,Substr(cProg,i_p+1,Len(cProg)-4-i_p),i_bPreview,Iif(Type('oObj.sync')<>'U',.T.,i_batchmodequery),i_asci,i_ascifile)
                Else
                    vq_exec(Left(cProg,i_p-5),oObj,.F.,Substr(cProg,i_p+1,Len(cProg)-i_p),i_bPreview,Iif(Type('oObj.sync')<>'U',.T.,i_batchmodequery),i_asci,i_ascifile)
                Endif
                * Zucchetti Aulla fine
            Else
                i_olderr=On('ERROR')
                i_err=""
                On Error i_err=Message()
                If i_bPreview
                    Push Key Clear
                    i_curform=.Null.
                    Push Menu _Msysmenu
                    *set sysmenu to _mview
                    Set Sysmenu To
                    Report Form (Alltrim(cProg)) Preview
                    Pop Menu _Msysmenu
                    Pop Key
                Else
                    If i_asci
                        Report Form (Alltrim(cProg)) To File (i_ascifile) Ascii
                    Else
                        Report Form (Alltrim(cProg)) Noconsole Noeject To Printer Prompt
                    Endif
                Endif
                On Error &i_olderr
                If Not Empty(i_err)
                    cp_Msg(cp_msgformat(MSG_ERROR_EXECUTING_REPORT__, Chr(13), i_err),.F.)
                Endif
            Endif
        Case i_cExt="doc" Or i_cExt="docx"
            * Esegue una Query specificando il MailMerge (separati da virgola)
            i_p=At(',',cProg)
            If i_p<>0
                vq_exec(Left(cProg,i_p-5),oObj,.F.,Substr(cProg,i_p+1,Len(cProg)-Iif(i_cExt='docx' ,5,4)-i_p),i_bPreview,i_batchmodequery,i_asci,i_ascifile)
            Endif
        Case i_cExt="vmn"
            *--- Non pi� gestito
            *vu_exec(cProg)
            cp_ErrorMsg(MSG_FEATURE_NOT_AVAILABLE)
        Case i_cExt="vgr"
            i_p=At(',',cProg)
            If i_p<>0
                i_cQry=Substr(cProg,i_p+1)
                vg_exec(Left(cProg,i_p-5),oObj,Left(i_cQry,Len(i_cQry)-4))
            Else
                vg_exec(Left(cProg,Len(cProg)-4),oObj)
            Endif
            * --- Zucchetti Aulla - Gestione Office/OpenOffice
        Case i_cExt$'xlt-stc-xltx'
            i_p=At(',',cProg)
            If i_p<>0
                i_cQry=Substr(cProg,i_p+1)
                ve_exec(Alltrim(Left(i_cQry,Len(i_cQry)-Iif(i_cExt='xltx' ,5,4))),oObj,Alltrim(Left(cProg,i_p- 5)),Iif(i_cExt='xltx' ,'XLTX','XLT'))
            Else
                ve_exec(Left(cProg,Len(cProg)-Iif (i_cExt='xltx',5,4)),oObj)
            Endif
        Case i_cExt="vfc"
            Local l_mskvfc, l_cursvfc
            i_p=At(',',cProg)
            If i_p<>0
                l_cursvfc = ""
                If cp_fileexist(Forceext(Left(cProg,i_p-1), 'VQR'))
                    l_cursvfc = Sys(2015)
                    vq_exec(Left(cProg,i_p-1),oObj, l_cursvfc)
                Else
                    If Used(Left(cProg,i_p-1))
                        l_cursvfc = Left(cProg,i_p-1)
                    Endif
                Endif
                If !Empty(l_cursvfc)
                    l_mskvfc = cp_mskvfc(oObj)
                    If i_bWinState
                        l_mskvfc.WindowState=2
                    Endif
                    l_mskvfc.Hide()
                    l_mskvfc.w_SOURCE=l_cursvfc && ALLTRIM(left(cProg,i_p-1))
                    l_mskvfc.w_FILECFG=Alltrim(Substr(cProg,i_p+1))
                    l_mskvfc.SetConfigValues()
                    l_mskvfc.mCalc(.T.)
                    l_mskvfc.Show(1)
                Endif
            Else
                cp_mskvfc(cProg)
            Endif
    Endcase
    Return

Func cp_AbsTop(i_oObj,i_nTop)
    Do Case
        Case i_oObj.BaseClass='Form'
            Return(i_nTop)
        Case i_oObj.BaseClass='Pageframe'
            Return(cp_AbsTop(i_oObj.Parent,Iif(i_oObj.PageCount>1,i_nTop+29,i_nTop)))
        Case i_oObj.BaseClass='Page'
            Return(cp_AbsTop(i_oObj.Parent,i_nTop))
    Endcase
    Return(cp_AbsTop(i_oObj.Parent,i_nTop+i_oObj.Top))

Func cp_AbsName(i_oObj,i_cName)
    If i_oObj.BaseClass='Form'
        Return('frm'+i_oObj.Name+'.'+i_cName)
    Endif
    Return(cp_AbsName(i_oObj.Parent,i_oObj.Name+'.'+i_cName))

Func cp_OpenFuncTables(nTbls)
    Local i,N,w
    * --- cerca di aprire le tabelle
    For i=1 To nTbls
        w=i_cWorktables[i]
        N=cp_OpenTable(w)
        If N=0
            Return(.F.)
        Endif
        If i_bSecurityRecord And i_runtime_filters=0 And Type("i_TablePropSec[n,1]")='C'
            Do cp_secctrl With Alltrim(i_cWorktables[i])+'@'+i_func_name+'@'
            Return .F.
        Endif
        &w._idx=N
        i_nOpenedTables=i
    Next
    Return(.T.)

Func cp_CloseFuncTables()
    Local i_i
    * --- chiude le eventuali transazioni pendenti
    *if txnlevel()>i_nTrs
    If Vartype(nTrsConnCnt)<>"U" And Iif(nTrsConnCnt=0,0,1)<>i_nTrs
        cp_Msg(cp_Translate(MSG_ROLLBACK_FORCED),.F.)
        bTrsErr=.T.
        cp_EndTrs(.T.)
    Endif
    * --- chiude le tabelle
    For i_i=1 To i_nOpenedTables
        cp_CloseTable(i_cWorktables[i_i])
    Next
    Return(.T.)

Func cp_PKFox(i_tb,i_k1,i_v1,i_k2,i_v2,i_k3,i_v3,i_k4,i_v4,i_k5,i_v5,i_k6,i_v6,i_k7,i_v7,i_k8,i_v8,i_k9,i_v9,i_k10,i_v10,i_k11,i_v11,i_k12,i_v12)
    Local i_k,i_o,i_e,i_p,i_i,i_f,i_s
    i_k=''
    If Used(i_tb)
        i_o=Select()
        Select (i_tb)
        i_e=On('error')
        On Error =.F.
        Set Order To idx1
        i_k=Key()
        On Error &i_e
    Endif
    i_p=0
    i_s=0
    If !Empty(i_k)
        * cerca di ottimizzare l' espressione della chiave del tipo COD1+COD2+COD2='aa'+'bbb'
        * possono esserci piu' campi che costanti
        i_f=i_k
        i_k=Strtran(i_k,'+',Chr(1))
        i_k1=Upper(i_k1)
        p=At(i_k1,i_k)
        If p<>0
            i_k=Strtran(i_k,i_k1,cp_ToStr(i_v1))
            *i_p=max(i_p,occurs('+',left(i_f,p))+1)
            i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
        Endif
        i_s=1
        If !Empty(i_k2)
            i_k2=Upper(i_k2)
            p=At(i_k2,i_k)
            If p<>0
                i_k=Strtran(i_k,i_k2,cp_ToStr(i_v2))
                i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
            Endif
            i_s=2
            If !Empty(i_k3)
                i_k3=Upper(i_k3)
                p=At(i_k3,i_k)
                If p<>0
                    i_k=Strtran(i_k,i_k3,cp_ToStr(i_v3))
                    *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                    i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                Endif
                i_s=3
                If !Empty(i_k4)
                    i_k4=Upper(i_k4)
                    p=At(i_k4,i_k)
                    If p<>0
                        i_k=Strtran(i_k,i_k4,cp_ToStr(i_v4))
                        *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                        i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                    Endif
                    i_s=4
                    If !Empty(i_k5)
                        i_k5=Upper(i_k5)
                        p=At(i_k5,i_k)
                        If p<>0
                            i_k=Strtran(i_k,i_k5,cp_ToStr(i_v5))
                            *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                            i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                        Endif
                        i_s=5
                        If !Empty(i_k6)
                            i_k6=Upper(i_k6)
                            p=At(i_k6,i_k)
                            If p<>0
                                i_k=Strtran(i_k,i_k6,cp_ToStr(i_v6))
                                *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                                i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                            Endif
                            i_s=6
                            If !Empty(i_k7)
                                i_k7=Upper(i_k7)
                                p=At(i_k7,i_k)
                                If p<>0
                                    i_k=Strtran(i_k,i_k7,cp_ToStr(i_v7))
                                    *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                                    i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                                Endif
                                i_s=7
                                If !Empty(i_k8)
                                    i_k8=Upper(i_k8)
                                    p=At(i_k8,i_k)
                                    If p<>0
                                        i_k=Strtran(i_k,i_k8,cp_ToStr(i_v8))
                                        *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                                        i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                                    Endif
                                    i_s=8
                                    If !Empty(i_k9)
                                        i_k9=Upper(i_k9)
                                        p=At(i_k9,i_k)
                                        If p<>0
                                            i_k=Strtran(i_k,i_k9,cp_ToStr(i_v9))
                                            *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                                            i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                                        Endif
                                        i_s=9
                                        If !Empty(i_k10)
                                            i_k10=Upper(i_k10)
                                            p=At(i_k10,i_k)
                                            If p<>0
                                                i_k=Strtran(i_k,i_k10,cp_ToStr(i_v10))
                                                *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                                                i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                                            Endif
                                            i_s=10
                                            If !Empty(i_k11)
                                                i_k11=Upper(i_k11)
                                                p=At(i_k11,i_k)
                                                If p<>0
                                                    i_k=Strtran(i_k,i_k11,cp_ToStr(i_v11))
                                                    *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                                                    i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                                                Endif
                                                i_s=11
                                                If !Empty(i_k12)
                                                    i_k12=Upper(i_k12)
                                                    p=At(i_k12,i_k)
                                                    If p<>0
                                                        i_k=Strtran(i_k,i_k12,cp_ToStr(i_v12))
                                                        *i_p=max(i_p,occurs('+',left(i_f,p))+1)
                                                        i_p=Max(i_p,Occurs(Chr(1),Left(i_k,p))+1)
                                                    Endif
                                                    i_s=12
                                                Endif
                                            Endif
                                        Endif
                                    Endif
                                Endif
                            Endif
                        Endif
                    Endif
                Endif
            Endif
        Endif
        i_i=At(Chr(1),i_k,Max(i_p,1))
        If i_i<>0
            * --- toglie la parte di chiave che non viene utilizzata
            i_k=Left(i_k,i_i-1)
        Endif
        i_k=i_f+' = '+Strtran(i_k,Chr(1),'+')
        Select (i_o)
    Endif
    If i_p=0 Or i_p<>i_s && la chiave non e' nota oppure la sostituzione ha lasciato dei campi prima di qualche costante
        * crea una frase fatta di COND and COND and ...
        i_k=i_k1+' = '+cp_ToStr(i_v1)
        If !Empty(i_k2)
            i_k=i_k+' and '+i_k2+' = '+cp_ToStr(i_v2)
            If !Empty(i_k3)
                i_k=i_k+' and '+i_k3+' = '+cp_ToStr(i_v3)
                If !Empty(i_k4)
                    i_k=i_k+' and '+i_k4+' = '+cp_ToStr(i_v4)
                    If !Empty(i_k5)
                        i_k=i_k+' and '+i_k5+' = '+cp_ToStr(i_v5)
                        If !Empty(i_k6)
                            i_k=i_k+' and '+i_k6+' = '+cp_ToStr(i_v6)
                            If !Empty(i_k7)
                                i_k=i_k+' and '+i_k7+' = '+cp_ToStr(i_v7)
                                If !Empty(i_k8)
                                    i_k=i_k+' and '+i_k8+' = '+cp_ToStr(i_v8)
                                    If !Empty(i_k9)
                                        i_k=i_k+' and '+i_k9+' = '+cp_ToStr(i_v9)
                                        If !Empty(i_k10)
                                            i_k=i_k+' and '+i_k10+' = '+cp_ToStr(i_v10)
                                            If !Empty(i_k11)
                                                i_k=i_k+' and '+i_k11+' = '+cp_ToStr(i_v11)
                                                If !Empty(i_k12)
                                                    i_k=i_k+' and '+i_k12+' = '+cp_ToStr(i_v12)
                                                Endif
                                            Endif
                                        Endif
                                    Endif
                                Endif
                            Endif
                        Endif
                    Endif
                Endif
            Endif
        Endif
    Endif
    *ACTIVATE SCREEN
    *? i_k
    Return(i_k)

Proc cpluutkx(i_cFile)
    vx_exec(i_cFile)
    Return

Func cp_GetFormat(i_cString)
    Local i_c,i_i
    i_c=&i_cString
    If Left(i_c,1)='@'
        i_i=At(' ',i_c)
        If i_i<>0
            i_c=Substr(i_c,2,i_i-1)
        Else
            i_c=Substr(i_c,2)
        Endif
    Else
        i_c=''
    Endif
    Return(i_c)

Func cp_GetMask(i_cString)
    Local i_c,i_i
    i_c=&i_cString
    If Left(i_c,1)='@'
        i_i=At(' ',i_c)
        If i_i<>0
            i_c=Substr(i_c,i_i+1)
        Else
            i_c=''
        Endif
    Endif
    Return(i_c)

Procedure cp_LoadRuntimeUserFilters()
    * --- se non ho caricato i filtri dell' utente o dell'azienda corrente i filtri non sono pronti ad essere utilizzati
    If (Vartype(i_currfiltertableuser)='U' Or i_currfiltertableuser<>i_CODUTE Or Vartype(i_currfiltertableazi)='U' Or i_currfiltertableazi<>i_CODAZI) And Vartype(i_nTables)='N' And i_nTables<>0
        * -- o non ho mai caricato i filtri o ho caricato quelli di un altro utente
        Public i_currfiltertableuser, i_currfiltertableazi
        i_currfiltertableuser=i_CODUTE
        i_currfiltertableazi=i_CODAZI
        cp_LoadTableSecArray()
    Endif
    Return

Func cp_SetAzi(i_cName,i_bApplyUserFilters,i_nTablePos,i_cProcName)
    * --- la cp_SetAzi � l' unica procedura che utilizza l' array dei filtri; dobbiamo essere sicuri di avere i filtri per l' utente corrente.
    If i_bSecurityRecord And i_bApplyUserFilters And Vartype(i_nTablePos)='N' And i_nTablePos<>0
        cp_LoadRuntimeUserFilters()
        * --- ora devo applicare il filtro selezionato per l' utente
        If i_nTablePos<=Alen(i_TablePropSec,1) And (Vartype(i_TablePropSec[i_nTablePos,1])='C' Or i_TablePropSec[i_nTablePos,4])
            Local i_result, i_cPrimaryKey, i_cAliasTable, i_cWhereKey, i_cVir, i_procresult
            i_procresult=''
            *** se i_TablePropSec[i_nTablePos,4] devo vedere se c'� una regola per procedura
            If i_TablePropSec[i_nTablePos,4] And Vartype(i_cProcName)='C'
                i_procresult=cp_ProcQueryFilter(i_cProcName,i_TableProp[i_nTablePos,1], .T.)
            Endif
            If Vartype(i_TablePropSec[i_nTablePos,1])='C' And Not i_TablePropSec[i_nTablePos,3]
                i_result=i_TablePropSec[i_nTablePos,1]
                If Lower(Right(i_result,4))='.vqr'
                    *** la query fornisce direttamente i record visibili dall' utente
                    Private i_runtime_filters
                    i_runtime_filters=2
                    i_TablePropSec[i_nTablePos,1]="("+cp_GetSQLFromQuery(i_result)+")"
                    *** la condizione da mettere nel "QueryFilter" in questo caso va costruita come una exist
                    If Type('i_dcx')='U'
                        cp_ReadXdc()
                    Endif
                    i_cPrimaryKey=cp_KeyToSQL(i_Dcx.GetIdxDef(i_TableProp[i_nTablePos,1],1))
                    If Empty(i_cPrimaryKey)
                        * Non ho trovato la chiave primaria forzo aggiornamento dizionario
                        Release i_Dcx
                        cp_ReadXdc()
                        i_cPrimaryKey=cp_KeyToSQL(i_Dcx.GetIdxDef(i_TableProp[i_nTablePos,1],1))
                    Endif
                    i_cAliasTable=Upper(cp_NewCCChk())
                    i_cWhereKey=''
                    i_cVir=At(',',i_cPrimaryKey)
                    Do While i_cVir>0
                        i_cWhereKey=i_cWhereKey+i_cAliasTable+'.'+Left(i_cPrimaryKey, i_cVir-1)+'='+Alltrim(i_TableProp[i_nTablePos,1])+'.'+Left(i_cPrimaryKey, i_cVir-1)+' AND '
                        i_cPrimaryKey=Right(i_cPrimaryKey, Len(i_cPrimaryKey)-i_cVir)
                        i_cVir=At(',',i_cPrimaryKey)
                    Enddo
                    i_cWhereKey=i_cWhereKey+i_cAliasTable+'.'+i_cPrimaryKey+'='+Alltrim(i_TableProp[i_nTablePos,1])+'.'+i_cPrimaryKey
                    i_TablePropSec[i_nTablePos,2]=" exists (select 1 from ("+i_TablePropSec[i_nTablePos,1]+") "+i_cAliasTable+" where "+i_cWhereKey+")"
                Else
                    Do cp_SetSQLFunc In cp_query With i_result, CP_DBTYPE
                    *** la select con i dati visibili all' utente
                    i_TablePropSec[i_nTablePos,1]="(select * from "+Trim(Strtran(i_TableProp[i_nTablePos,2],'xxx',Trim(i_CODAZI)))+" "+i_TableProp[i_nTablePos,1]+"  where "+i_result+")"
                    *** la condizione � gi� il "QueryFilter"
                    i_TablePropSec[i_nTablePos,2]=i_result
                Endif
                * --- Filtro sul dato via valutato ed attribuito...
                i_TablePropSec[i_nTablePos,3]=.T.
            Endif
            * --- i_cName ora contiene o il nome tabella o la subselect filtrata
            If Not Empty(i_procresult)
                Return(i_procresult)
            Else
                * --- dato gia "trasformato" con l'azienda corrente...
                If Vartype(i_TablePropSec[i_nTablePos,1])='C'
                    Return(i_TablePropSec[i_nTablePos,1])
                Endif
            Endif
        Endif
    Endif
    * --- qui siamo certi che i filtri nell' array dei filtri utente sono quelli impostati per l' utente corrente
    Return(Trim(Strtran(i_cName,'xxx',Trim(i_CODAZI))))

    * -----------------------------------------------------------------------------*
    *        Nome: cp_GetProg                                                      *
    * Descrizione: Assegna un nuovo progressivo. Questa procedura � stata disegnata*
    *              per essere utilizzata all'interno dei batch.                    *
    *              ATTENZIONE: deve essere chiamata sotto transazione              *
    *                                                                              *
    * Risposta:    Nuovo progressivo tipo Numerico o Alfanumerico dipende tipo     *
    *              della variabile i_var.                                          *
    * Parametri:   i_cFile : Nome della tabella                                    *
    *              i_cTable: Nome tabella progressivo                              *
    *              i_var   : Variabile per progressivo                             *
    *              i_x1..5 : Parti fisse della chiave                              *
    *                                                                              *
    * Esempi:                                                                      *
    *  a) this.w_newprog=cp_GetProg('articoli','prart',this.w_newprog)             *
    *  b) this.w_newprog=cp_GetProg('articoli','prart',this.w_newprog,this.magart) *
    *                                                                              *
    * Nota: la variabile w_newprog deve essere creata all'interno del batch con la *
    *       lunghezza e il tipo corretto del progressivo.                          *
    *       per chiedere un progressivo bisogna essere sotto transazione           *
    * -----------------------------------------------------------------------------*
Func cp_GetProg(i_cFile,i_cTable,i_var,i_x1,i_x2,i_x3,i_x4,i_x5)
    Local i_cProgKey, i_n, i_cAzi, i_cProgType, i_cProgValue, i_nProgValue, i_bTrs
    i_bTrs=.F.
    *if txnlevel()=0
    If Vartype(nTrsConnCnt)="U" Or nTrsConnCnt=0
        i_bTrs=.T.
        cp_BeginTrs()
    Endif
    i_cAzi = i_CODAZI
    i_cProgKey='prog\'+i_cTable
    If Type("i_x1")<>'L'
        * --- Zucchetti Aulla inizio - Il primo parametro dovrebbe essere i_CODAZI (non � detto ad oggi per ad hoc � sempre cos�)
        * --- lo trimmo se di tipo carattere..
        If Type("i_x1")='C'
            i_x1=Trim(i_x1)
        Endif
        * --- Zucchetti Aulla Fine
        i_cProgKey=i_cProgKey+'\'+cp_ToStrODBC(i_x1)
        If Type("i_x2")<>'L'
            i_cProgKey=i_cProgKey+'\'+cp_ToStrODBC(i_x2)
            If Type("i_x3")<>'L'
                i_cProgKey=i_cProgKey+'\'+cp_ToStrODBC(i_x3)
                If Type("i_x4")<>'L'
                    i_cProgKey=i_cProgKey+'\'+cp_ToStrODBC(i_x4)
                    If Type("i_x5")<>'L'
                        i_cProgKey=i_cProgKey+'\'+cp_ToStrODBC(i_x5)
                    Endif
                Endif
            Endif
        Endif
    Endif
    * --- cerca il prossimo valore
    i_n=0
    i_nConn=i_TableProp[cp_ascan(@i_TableProp,lower(i_cFile),i_nTables),3]
    Local i_cProgKey,i_cProgValue,i_cProgType
    i_cProgType=Type("i_var")
    i_cProgValue=i_var
    i_nProgValue=Iif(i_cProgType='C',Val(i_cProgValue),i_cProgValue)
    If i_nConn<>0
        i_n=cp_TrsSQL(i_nConn,"update cpwarn set autonum=autonum+1 where tablecode="+cp_ToStrODBC(i_cProgKey))
    Else
        Update cpwarn Set autonum=autonum+1 Where tablecode=i_cProgKey
        i_n=_Tally
    Endif
    If i_n=0
        * --- nuovo valore
        If i_nConn<>0
            cp_TrsSQL(i_nConn,"insert into cpwarn (tablecode,warncode,autonum) values ("+cp_ToStrODBC(i_cProgKey)+","+Iif(Empty(i_cAzi),"'xxx'",cp_ToStrODBC(i_cAzi))+",1)")
        Else
            Insert Into cpwarn (tablecode,warncode,autonum) Values (i_cProgKey,Iif(Empty(i_cAzi),'xxx',i_cAzi),1)
        Endif
        i_r=Iif(i_cProgType='C',cp_StrZeroPad(1,Len(i_cProgValue)),1)
    Else
        * --- legge il valore per il progressivo
        If i_nConn<>0
            cp_SQL(i_nConn,"select autonum as nextval from cpwarn where tablecode="+cp_ToStrODBC(i_cProgKey),"__tmpanum__")
        Else
            Select autonum As Nextval From cpwarn Where tablecode=i_cProgKey Into Cursor __tmpanum__
        Endif
        i_r=Iif(i_cProgType='C',cp_StrZeroPad(Nvl(Nextval,0),Len(i_cProgValue)),Nvl(Nextval,0))
        Use
    Endif
    * Zucchetti Aulla Inizio: calcolo prefisso autonumber per Logistica remota
    If g_LORE = 'S' And Alen(g_aLoreProg)>0 And Ascan(g_aLoreProg, i_cTable)<>0 And Not Empty(g_cPrefiProg) And i_cProgType='C'
        i_r = Alltrim(g_cPrefiProg)+ Alltrim(Substr(Alltrim(i_r),3))
    Endif
    * Zucchetti Aulla Fine
    If i_bTrs
        cp_EndTrs()
    Endif
    Return(i_r)

Proc cp_ModifyReport(i_cName)
    Push Key Clear
    Push Menu _Msysmenu
    Set Sysmenu To _Mfile,_Medit,_mview
    Local l_MainMenuVisible
    If i_VisualTheme<>-1 And Vartype(i_MenuToolbar)<>'U'
        l_MainMenuVisible=i_MenuToolbar.Visible
        Set Sysmenu On
        i_MenuToolbar.Visible=.F.
    Endif
    * Nasconde Toolbar
    If i_VisualTheme<>-1 And Vartype(i_ThemesManager)='O'
        i_ThemesManager.HideToolbar()
    Else
        If Type('oDesktopbar.name')='C'
            oDesktopbar.Hide()
        Endif
        oCPToolbar.Hide()
    Endif
    *--- Zucchetti Aulla Inizio - Multireport, gestione nuovo engine VFP9
    Local l_bExitModify
    l_bExitModify=.T.
    Do While l_bExitModify
        Modi Report (i_cName) Noenvironment Save
        l_bExitModify=.F.
        For gnCount=1 To _Screen.FormCount
            If Upper(_Screen.Forms(gnCount).Class)='FRXPREVIEWFORM'
                l_bExitModify=.T.
                Exit
            Endif
        Endfor
        *--- Se esiste il browse del cursore __TMP__ ciclo all'infinito
        Do While Wexist(cp_Translate(MSG_VIEW_QUERY))
            DoEvents Force
            l_bExitModify=.T.
        Enddo
    Enddo
    * Visualizza Toolbar
    If i_VisualTheme<>-1 And Vartype(i_ThemesManager)='O'
        i_ThemesManager.ShowToolbar()
        If Type('oDesktopbar.name')='C'
            oDesktopbar.SetAfterCpToolbar()
        Endif
    Else
        oCPToolbar.Show()
        If Type('oDesktopbar.name')='C'
            oDesktopbar.Show()
        Endif
    Endif
    If i_VisualTheme<>-1 And Vartype(i_MenuToolbar)<>'U'
        Set Sysmenu Off
        i_MenuToolbar.Visible = l_MainMenuVisible
    Endif
    *modi report (i_cName)
    *--- Zucchetti Aulla Fine - Multireport, gestione nuovo engine VFP9
    Pop Menu _Msysmenu
    Pop Key
    Return

Func cp_getqueryparam(i_cName)
    Local i_xRes,i_err,i_i
    i_xRes=''
    i_err=On('ERROR')
    On Error =.T.
    i_cName=Lower(i_cName)
    For i_i=1 To i_paramqry.nFilterParam
        If Trim(Lower(i_paramqry.i_FilterParam[i_i,1]))==i_cName
            i_xRes=i_paramqry.i_FilterParam[i_i,6]
        Endif
    Next
    On Error &i_err
    Return(i_xRes)

    * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    * Nuova funzione cp_ispfile per integrazione Framework monitor e potenziamento
    * Oltre al vecchio comportamento, memorizza nella variabile i_pathFindFile
    * il percorso del file trovato oppure il nome del file cercato assieme a dei messaggi specifici
    * secondo il seguente schema:
    *
    * i_pathFindFile =>  [MSG_<?>|]<nome file cercato>
    *              ? =>  FILEEXIST , NOT_FILEEXIST , NOT_ABS
    *
    * MSG_FILEEXIST : file trovato mediante l'invocazione della funzione cp_fileexist senza il secondo parametro
    * MSG_NOT_FILEEXIST: file non trovato mediante l'invocazione della funzione cp_fileexist senza il secondo parametro
    * MSG_NOT_ABS: file non trovato mediante l'invocazione della funzione cp_fileexist con il secondo parametro a .t.
    *              (il file cercato aveva un percorso assoluto)
    * "" : file trovato all'interno di una cartella a partire dalla custom
    *---
Func cp_IsPFile(i_cName)
    * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    If Vartype(i_oCacheFile)='O'
        Local bResult
        bResult=i_oCacheFile.IsFile(i_cName)
        If bResult
            If i_MonitorFramework
                i_pathFindFile = Alltrim(Upper(i_cName))
            Endif
            Return (.T.)
        Endif
    Endif
    If i_MonitorFramework
        Local parameter_path, removed_path
        *Controllo se i_cName � un percorso assoluto
        If Not Empty( Justdrive( i_cName ) )
            If cp_fileexist(i_cName,.T.)
                i_pathFindFile = Alltrim(Upper(i_cName))
                Return (.T.)
            Else
                i_pathFindFile = Alltrim(MSG_NOT_ABS)+"|"+Alltrim(Upper(i_cName))
                Return (.F.)
            Endif
        Else
            *---
            * Memorizzo il percorso del file i_cName
            * Ci� � reso necessario per alcune ricerce che passano come parametro un file comprensivo di percorso relativo
            parameter_path = Addbs(Lower(Alltrim(Justpath(i_cName))))
            removed_path = .F.
            *Rimuovo il path da i_cName solo se non trovo default nel suo path
            If Len(Alltrim(parameter_path))!=0 And At("default",parameter_path)==0
                i_cName = Alltrim(Justfname(i_cName))
                removed_path = .T.
            Endif
            * Controllo se g_NOCUSTOMAZI � indefinita e se esiste il file dentro la cartella aziendale
            If Type('g_NOCUSTOMAZI') = 'U' And cp_fileexist(Alltrim(i_usrpath)+Alltrim(i_CODAZI)+"\"+Alltrim(i_cName),.T.)
                i_pathFindFile = Upper(Alltrim(i_usrpath)+Alltrim(i_CODAZI)+"\"+Alltrim(i_cName))
                Return (.T.)
            Endif
            * Controllo se esiste il file dentro la cartella utente
            If cp_fileexist(Alltrim(i_usrpath)+Alltrim(i_cName),.T.)
                i_pathFindFile = Upper(Alltrim(i_usrpath)+Alltrim(i_cName))
                Return (.T.)
            Endif
            * Controllo se g_NOCUSTOMAZI � indefinita e se esiste il file dentro la cartella aziendale
            If Type('g_NOCUSTOMAZI') = 'U' And cp_fileexist(Alltrim(i_customPath)+Alltrim(i_CODAZI)+"\"+Alltrim(i_cName),.T.)
                i_pathFindFile = Upper(Alltrim(i_customPath)+Alltrim(i_CODAZI)+"\"+Alltrim(i_cName))
                Return (.T.)
            Endif
            If removed_path And Type('g_NOCUSTOMAZI') = 'U' And cp_fileexist(Alltrim(i_customPath)+Alltrim(i_CODAZI)+"\"+Alltrim(parameter_path)+Alltrim(i_cName),.T.)
                i_pathFindFile = Upper(Alltrim(i_customPath)+Alltrim(i_CODAZI)+"\"+Alltrim(parameter_path)+Alltrim(i_cName))
                Return (.T.)
            Endif
            * Controllo se esiste dentro la cartella custom
            If cp_fileexist(Alltrim(i_customPath)+Alltrim(i_cName),.T.)
                i_pathFindFile = Upper(Alltrim(i_customPath)+Alltrim(i_cName))
                Return (.T.)
            Endif
            If removed_path And cp_fileexist(Alltrim(i_customPath)+Alltrim(parameter_path)+Alltrim(i_cName),.T.)
                i_pathFindFile = Upper(Alltrim(i_customPath)+Alltrim(parameter_path)+Alltrim(i_cName))
                Return (.T.)
            Endif
            If cp_fileexist("std\"+Alltrim(i_cName),.T.)
                i_pathFindFile = Upper("std\"+Alltrim(i_cName))
                Return (.T.)
            Endif
            * Se avevo levato il path lo reinserisco
            If removed_path
                i_cName = Alltrim(parameter_path)+Alltrim(i_cName)
            Endif
            If cp_fileexist(Alltrim(i_cName))
                && ###start remove from setup###
                If g_ADHOCONE And Left(i_cName,3)='..\' And cp_fileexist('..\'+Alltrim(i_cName),.T.)
                    i_pathFindFile = '..\'+Alltrim(i_cName)
                Else
                    && ###end remove from setup###
                    i_pathFindFile = Upper(Alltrim(MSG_FILEEXIST)+"|"+Alltrim(i_cName))
                    && ###start remove from setup###
                Endif
                && ###end remove from setup###
                Return (.T.)
            Else
                i_pathFindFile = Upper(Alltrim(MSG_NOT_FILEEXIST)+"|"+Alltrim(i_cName))
                Return (.F.)
            Endif
        Endif
    Else
        * --- Zucchetti Aulla fine - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
        * --- controlla se un file esiste nel disco o nella persistenza
        If Not Empty( Justdrive( i_cName ) )
            Return(cp_fileexist(i_cName,.T.))
        Else
            * --- Zucchetti Aulla Inizio - Custom per Azienda
            If cp_fileexist(i_cName) Or cp_fileexist('custom\'+i_cName,.T.) Or cp_fileexist('std\'+i_cName,.T.) Or ( Type('g_NOCUSTOMAZI') = 'U' And cp_fileexist('custom\'+Alltrim(i_CODAZI)+'\'+i_cName,.T.))
                Return (.T.)
            Else
                *i_cName=upper(i_cName)
                *l=at(curdir(),i_cName)
                *if l<>0
                *  i_cName=substr(i_cName,l+len(curdir()))
                *endif
                *if i_ServerConn[1,2]<>0
                *  cp_SQL(i_ServerConn[1,2],"select count(*) as cnt from persist where code='UTK:"+trim(i_cName)+"'",'_pfile_',.f.,.t.)
                *else
                *  local i_olderr
                *  i_olderr=on('ERROR')
                *  on error =.t.
                *  select count(*) as cnt from persist where code='UTK:'+i_cName into cursor _pfile_
                *  on error &i_olderr
                *endif
                *if used('_pfile_')
                *  local r
                *  r=(_pfile_.cnt>=1)
                *  use
                *  return(r)
                *endif
            Endif
        Endif
        Return(.F.)
        * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    Endif
Endfunc

*---
* Funzione nuova cp_GetP_GetStd_File per integrazione Framework monitor e potenziamento.
* Integra il comportamento delle due vecchie funzioni cp_getpfile e cp_getstdFile
* a seconda del valore del secondo parametro:
* p_std = p => comportamento di cp_getpfile
* p_std = std => comportamento di cp_getstdfile
*---
Function cp_GetP_GetStd_File(i_cName,p_std)
    *---
    * Punto di ottimizzazione perch� se la ricerca del file � gi� stata fatta
    * questo controllo permette di non eseguirla nuovamente:
    *
    * Se non ho traccia del nome del file i_cName dentro la variabile i_pathFindFile
    * eseguo la ricerca del file
    *---
    If At(Upper(Alltrim(i_cName)),Upper(Alltrim(i_pathFindFile))) == 0
        cp_IsPFile(i_cName)
    Endif
    Local i_c,i_h,i_l,temp_var
    * Se il file � stato trovato a partire dalla cartella custom
    If At("|",Alltrim(i_pathFindFile))==0
        If Upper(p_std)=="P"
            i_h=Fopen(i_pathFindFile)
            i_l=Fseek(i_h,0,2)
            Fseek(i_h,0)
            i_c=Fread(i_h,i_l)
            =Fclose(i_h)
            i_pathFindFile = ""
            Return(i_c)
        Endif
        If Upper(p_std)="STD"
            temp_var = Alltrim(i_pathFindFile)
            i_pathFindFile = ""
            Return(Lower(Addbs(Alltrim(Justpath(temp_var))))+Alltrim(Juststem(temp_var)))
        Endif
    Endif
    * Se non ho trovato il file
    If At(MSG_NOT_FILEEXIST,i_pathFindFile)!=0 Or At(MSG_NOT_ABS,i_pathFindFile)!=0
        i_pathFindFile = ""
        Return("")
    Endif
    * Se ho trovato il file mediante l'invocazione della funzione cp_fileexist senza il secondo parametro
    If At(MSG_FILEEXIST,i_pathFindFile)!=0
        i_pathFindFile = ""
        If Upper(p_std)=="P"
            i_h=Fopen(i_cName)
            i_l=Fseek(i_h,0,2)
            Fseek(i_h,0)
            i_c=Fread(i_h,i_l)
            =Fclose(i_h)
            Return(i_c)
        Endif
        If Upper(p_std)="STD"
            Return(Lower(Addbs(Alltrim(Justpath(i_cName))))+Alltrim(Juststem(i_cName)))
        Endif
    Endif
Endfunc
* --- Zucchetti Aulla fine - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework

Func cp_GetPFile(i_cName)
    * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    Local i_c,i_h,i_l
    If Vartype(i_oCacheFile)='O'
        i_c=i_oCacheFile.Getfile(i_cName)
        If Not Empty(i_c)
            Return(i_c)
        Endif
    Endif
    If i_MonitorFramework
        i_c=cp_GetP_GetStd_File(i_cName,"p")
    Else
        * --- Zucchetti Aulla fine - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
        * --- legge un file, da disco o dalla persistenza
        i_cName=Upper(i_cName)
        i_c=''
        If Not Empty( Justdrive( i_cName ))
            i_h=Fopen(i_cName)
            i_l=Fseek(i_h,0,2)
            Fseek(i_h,0)
            i_c=Fread(i_h,i_l)
            =Fclose(i_h)
        Else
            Do Case
                    * --- Zucchetti Aulla Inizio Custom per Azienda
                Case (Type('g_NOCUSTOMAZI') = 'U' And cp_fileexist('custom\'+Alltrim(i_CODAZI)+'\'+i_cName,.T.))
                    i_h=Fopen('custom\'+Alltrim(i_CODAZI)+'\'+i_cName)
                    i_l=Fseek(i_h,0,2)
                    Fseek(i_h,0)
                    i_c=Fread(i_h,i_l)
                    =Fclose(i_h)
                    * --- Zucchetti Aulla Fine
                Case cp_fileexist('custom\'+i_cName,.T.)
                    i_h=Fopen('custom\'+i_cName)
                    i_l=Fseek(i_h,0,2)
                    Fseek(i_h,0)
                    i_c=Fread(i_h,i_l)
                    =Fclose(i_h)
                Case cp_fileexist(i_cName)
                    i_h=Fopen(i_cName)
                    i_l=Fseek(i_h,0,2)
                    Fseek(i_h,0)
                    i_c=Fread(i_h,i_l)
                    =Fclose(i_h)
                Case cp_fileexist('std\'+i_cName,.T.)
                    i_h=Fopen('std\'+i_cName)
                    i_l=Fseek(i_h,0,2)
                    Fseek(i_h,0)
                    i_c=Fread(i_h,i_l)
                    =Fclose(i_h)
                    *otherwise
                    *  i_l=at(curdir(),i_cName)
                    *  if i_l<>0
                    *    i_cName=substr(i_cName,i_l+len(curdir()))
                    *  endif
                    *  if i_ServerConn[1,2]<>0
                    *    cp_SQL(i_ServerConn[1,2],"select * from persist where code='UTK:"+trim(i_cName)+"'",'_pfile_')
                    *  else
                    *    select * from persist where code='UTK:'+i_cName into cursor _pfile_
                    *  endif
                    *  if used('_pfile_')
                    *    i_c=_pfile_.persist
                    *    use
                    *  endif
            Endcase
        Endif
        * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    Endif
    i_oCacheFile.Putfile(i_cName, i_c)
    Return(i_c)
Endfunc
*---
* Potenziamento Framework
*---
* --- Zucchetti Aulla fine - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework

Func cp_IsStdFile(i_cFile,i_cExt)
    * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    If i_MonitorFramework
        Return(cp_IsPFile(Forceext(Alltrim(i_cFile),Alltrim(i_cExt))))
    Else
        * --- Zucchetti Aulla fine - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
        If Not Empty( Justdrive( Forceext(Alltrim(i_cFile),Alltrim(i_cExt)) ))
            Return(cp_fileexist(Forceext(Alltrim(i_cFile),Alltrim(i_cExt)) , .T.))
        Else
            * --- Zucchetti Aulla Inizio - Custom per Azienda
            Return(( Type('g_NOCUSTOMAZI') = 'U' And cp_fileexist(Forceext('custom\'+Alltrim(i_CODAZI)+'\'+i_cFile,i_cExt),.T.)) Or cp_fileexist(Forceext('custom\'+i_cFile,i_cExt),.T.) Or cp_fileexist(Forceext(i_cFile,i_cExt)) Or cp_fileexist(Forceext('std\'+i_cFile,i_cExt),.T.))
        Endif
    Endif

Func cp_GetStdFile(i_cFile,i_cExt)
    * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    If Empty(Nvl(i_cFile,''))
        Return ""
    Endif
    i_cFile = Alltrim(i_cFile) && Aggiunta alltrim perch� non tutte le chiamate passano il percorso pulito
    i_cExt = Alltrim(i_cExt) && stessa cosa vale per l'estensione
    If i_MonitorFramework
        Return(cp_GetP_GetStd_File(i_cFile+"."+i_cExt,"std"))
    Else
        * --- Zucchetti Aulla fine - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
        Local i_res
        i_res=""
        If Not Empty( Justdrive( i_cFile+'.'+i_cExt ))
            i_res=i_cFile
        Else
            Do Case
                    * --- Zucchetti Aulla Inizio Custom per Azienda
                Case ( Type('g_NOCUSTOMAZI') = 'U' And cp_fileexist('custom\'+Alltrim(i_CODAZI)+'\'+i_cFile+'.'+i_cExt,.T.))
                    i_res='custom\'+Alltrim(i_CODAZI)+'\'+i_cFile
                    * --- Zucchetti Aulla Fine
                Case cp_fileexist('custom\'+i_cFile+'.'+i_cExt,.T.)
                    i_res='custom\'+i_cFile
                Case cp_fileexist(i_cFile+'.'+i_cExt)
                    && ###start remove from setup###
                    If g_ADHOCONE And Left(i_cFile,3)='..\' And cp_fileexist('..\'+i_cFile+'.'+i_cExt,.T.)
                        i_res='..\'+i_cFile
                    Else
                        && ###end remove from setup###
                        i_res=i_cFile
                        && ###start remove from setup###
                    Endif
                    && ###end remove from setup###
                Case cp_fileexist('std\'+i_cFile+'.'+i_cExt,.T.)
                    i_res='std\'+i_cFile
            Endcase
        Endif
        Return(i_res)
        * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    Endif
Endfunc
* --- Zucchetti Aulla fine - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework

Func cp_GetStdImg(i_cFile,i_cExt)
    Local i_cFile_custom, i_JustStemFName, result, i_cNameTheme, i_cThemeCheck
    i_JustStemFName = Juststem(i_cFile)
    If Vartype(i_oCacheFile)='O'
        result=i_oCacheFile.GetNameFile(Forceext(i_JustStemFName,i_cExt))
        If Not Empty(result)
            result=Iif(result="##FileNotFound##","",result)
            Return (result)
        Endif
    Endif
    If Vartype(i_ThemesManager)='O' And Empty(i_ThemesManager.ThemeName)
        If i_VisualTheme=-1
            i_ThemesManager.ThemeName='Standard'
        Else
            * --- Zucchetti Aulla Inizio - elimino cursore immagini
            i_cThemeCheck=i_ThemesManager.GetProp(0,5)
            If Vartype(i_cThemeCheck)='N'
                Local l_OldErr, l_err
                l_err = .F.
                l_OldErr = On("error")
                On Error l_err=.T.
                L_FH = Fcreate(Addbs(Alltrim(i_ThemesManager.Imgpath))+"bDelGraphics.fla" )
                Fclose(L_FH)
                i_ThemesManager.remove_graphics(.T.,.T.)
                i_ThemesManager.Init_ThemesArray()
                On Error &l_OldErr
                If l_err
                    cp_ErrorMsg("Impossibile eliminare dati impostazioni grafiche")
                    KillProcess()
                Endif
            Endif
            i_ThemesManager.ThemeName=Strtran(i_ThemesManager.GetProp(0,5),' ','')
            * --- Zucchetti Aulla Fine - elimino cursore immagini
        Endif
    Endif
    i_cFile_custom=Iif(Left(i_cFile,3)='..\', Substr(i_cFile,4,Len(i_cFile)-3), i_cFile)
    Do Case
        Case i_bEnableCustomImage And cp_fileexist('custom\'+i_cFile_custom+'.'+i_cExt,.T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),'custom\'+i_cFile_custom)
            Endif
            Return 'custom\'+i_cFile_custom
            && ###start remove from setup###
        Case g_ADHOCONE And i_VisualTheme<>-1 And Vartype(i_ThemesManager)='O' And cp_fileexist('..\..\vfcsim\Themes\'+i_ThemesManager.ThemeName+'\bmp\'+i_JustStemFName+'.'+i_cExt, .T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),'..\..\vfcsim\Themes\'+i_ThemesManager.ThemeName+'\bmp\'+i_JustStemFName)
            Endif
            Return '..\..\vfcsim\Themes\'+i_ThemesManager.ThemeName+'\bmp\'+i_JustStemFName
            && ###end remove from setup###
        Case i_VisualTheme<>-1 And Vartype(i_ThemesManager)='O' And cp_fileexist('..\vfcsim\Themes\'+i_ThemesManager.ThemeName+'\bmp\'+i_JustStemFName+'.'+i_cExt, .T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),'..\vfcsim\Themes\'+i_ThemesManager.ThemeName+'\bmp\'+i_JustStemFName)
            Endif
            Return '..\vfcsim\Themes\'+i_ThemesManager.ThemeName+'\bmp\'+i_JustStemFName
        Case cp_fileexist('bmp\'+i_JustStemFName+'.'+i_cExt, .T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),'bmp\'+i_JustStemFName)
            Endif
            Return 'bmp\'+i_JustStemFName
            && ###start remove from setup###
        Case g_ADHOCONE And cp_fileexist('..\..\exe\bmp\'+i_JustStemFName+'.'+i_cExt, .T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),'..\..\exe\bmp\'+i_JustStemFName)
            Endif
            Return '..\..\exe\bmp\'+i_JustStemFName
        Case g_ADHOCONE And cp_fileexist('..\..\vfcsim\'+i_JustStemFName+'.'+i_cExt, .T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),'..\..\vfcsim\'+i_JustStemFName)
            Endif
            Return '..\..\vfcsim\'+i_JustStemFName
        Case g_ADHOCONE And cp_fileexist('..\..\exe\'+i_JustStemFName+'.'+i_cExt, .T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),'..\..\exe\'+i_JustStemFName)
            Endif
            Return '..\..\exe\'+i_JustStemFName
        Case g_ADHOCONE And cp_fileexist('..\..\' + Strtran(Strtran(i_cFile, Addbs(Sys(5)+Sys(2003)), '',1,-1,1), Left(Addbs(Sys(5)+Sys(2003)), Len(Addbs(Sys(5)+Sys(2003)))-4),'',1,-1,1)+'.'+i_cExt)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),'..\..\' + Strtran(Strtran(i_cFile, Addbs(Sys(5)+Sys(2003)), '',1,-1,1), Left(Addbs(Sys(5)+Sys(2003)), Len(Addbs(Sys(5)+Sys(2003)))-4),'',1,-1,1))
            Endif
            Return  '..\..\' + Strtran(Strtran(i_cFile, Addbs(Sys(5)+Sys(2003)), '',1,-1,1), Left(Addbs(Sys(5)+Sys(2003)), Len(Addbs(Sys(5)+Sys(2003)))-4),'',1,-1,1)
            && ###end remove from setup###
        Case cp_fileexist('..\vfcsim\'+i_JustStemFName+'.'+i_cExt, .T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),'..\vfcsim\'+i_JustStemFName)
            Endif
            Return '..\vfcsim\'+i_JustStemFName
        Case cp_fileexist(i_cBmpPath+i_JustStemFName+'.'+i_cExt, .T.)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),i_cBmpPath+i_JustStemFName)
            Endif
            Return i_cBmpPath+i_JustStemFName
        Case cp_fileexist(i_cFile+'.'+i_cExt)
            If Vartype(i_oCacheFile)='O'
                i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),i_cFile)
            Endif
            Return i_cFile
    Endcase
    If Vartype(i_oCacheFile)='O'
        i_oCacheFile.PutNameFile(Forceext(i_JustStemFName,i_cExt),"")
    Endif
    Return ""


Func cp_msgformat(i_cFmt,i_cP1,i_cP2,i_cP3,i_cP4,i_cP5)
    Local i_p
    i_cFmt=cp_Translate(i_cFmt)
    i_p=At('%0',i_cFmt)
    If i_p<>0
        i_cFmt=Strtran(i_cFmt,'%0',Chr(13)+Chr(10))
    Endif
    i_p=At('%1',i_cFmt)
    If i_p<>0 And Type('i_cP1')='C'
        *i_cFmt=left(i_cFmt,i_p-1)+cp_Translate(i_cP1)+substr(i_cFmt,i_p+2)
        i_cFmt=Strtran(i_cFmt,'%1',i_cP1)
    Endif
    i_p=At('%2',i_cFmt)
    If i_p<>0 And Type('i_cP2')='C'
        *i_cFmt=left(i_cFmt,i_p-1)+cp_Translate(i_cP2)+substr(i_cFmt,i_p+2)
        i_cFmt=Strtran(i_cFmt,'%2',i_cP2)
    Endif
    i_p=At('%3',i_cFmt)
    If i_p<>0 And Type('i_cP3')='C'
        *i_cFmt=left(i_cFmt,i_p-1)+cp_Translate(i_cP3)+substr(i_cFmt,i_p+2)
        i_cFmt=Strtran(i_cFmt,'%3',i_cP3)
    Endif
    i_p=At('%4',i_cFmt)
    If i_p<>0 And Type('i_cP4')='C'
        *i_cFmt=left(i_cFmt,i_p-1)+cp_Translate(i_cP4)+substr(i_cFmt,i_p+2)
        i_cFmt=Strtran(i_cFmt,'%4',i_cP4)
    Endif
    i_p=At('%5',i_cFmt)
    If i_p<>0 And Type('i_cP5')='C'
        *i_cFmt=left(i_cFmt,i_p-1)+cp_Translate(i_cP5)+substr(i_cFmt,i_p+2)
        i_cFmt=Strtran(i_cFmt,'%5',i_cP5)
    Endif
    Return(i_cFmt)

    * Funzione di gestione traduzione a runtime --------------------------------------------------------------------------
Func cp_Translate(p_cStr,p_cOrigLang,p_cDestLang)
    #Define i_nPAD 200
    * --- inizio controlli preliminari ------------------------------
    *** Zucchetti Aulla - se non c'� stato bisogno di tradurre senza passare i paremtri lingua non effettuo pi� i controlli
    If Parameters( )=1 And (g_PROJECTLANGUAGE=i_cLanguage Or Empty(i_cLanguage)) And Vartype(g_bNoTranslateDefault)='C' And g_bNoTranslateDefault='S'
        Return p_cStr
    Endif
    If Empty(p_cStr)
        Return p_cStr
    Endif
    If Vartype(g_PROJECTLANGUAGE)<>'C'
        g_PROJECTLANGUAGE=_STDPROJECTLANGUAGE
    Endif
    If Vartype(p_cOrigLang)<>'C' Or Empty(p_cOrigLang) Or i_cLanguage="auto"
        p_cOrigLang = g_PROJECTLANGUAGE
    Endif
    If Vartype(p_cDestLang)<>'C' Or Empty(p_cDestLang) Or i_cLanguage="auto"
        p_cDestLang = Iif(Empty(i_cLanguage),g_PROJECTLANGUAGE,i_cLanguage)
    Endif
    * --- Se la lingua determinata � vuota, restituisce la stringa originale
    If p_cOrigLang==p_cDestLang And p_cOrigLang<>"auto"              && No translation needed
        If Parameters( )=1 And Vartype(g_bNoTranslateDefault)<>'C'
            Public g_bNoTranslateDefault
            g_bNoTranslateDefault='S'
        Endif
        Return p_cStr
    Endif
    Local i_oldexact, i_bColon, i_nOldArea, i_lAutoapprende, i_rspaces, i_inispaces, i_finspaces
    i_oldexact=Set('EXACT')
    i_nOldArea=Select()
    * --- fine controlli preliminari -------------------------------
    * --- Per eliminare errore su = con Visual FoxPro 8
    If '='==Left(Rtrim(Left(p_cStr,2)),1) And (Version(5)>=800)
        p_cStr=Strtran(p_cStr,'=',' =')
    Endif
    * --- memorizza il numero di spazi finali
    i_finspaces=Strtran(p_cStr,Rtrim(p_cStr),'')
    * --- memorizza il numero di spazi iniziali
    i_inispaces=Strtran(p_cStr,Ltrim(p_cStr),'')
    *---elimina gli spazi
    p_cStr=Alltrim(p_cStr)
    * --- Elimina eventuali ':' finali ...
    i_bColon = (Right(p_cStr,1) == ':')
    If i_bColon && toglie i ':' finali
        p_cStr = Left(p_cStr, Len(p_cStr)-1)
    Endif
    If Empty(p_cStr)
        Return i_inispaces+Iif(i_bColon,':',p_cStr)+i_finspaces
    Endif
    * --- memorizza il numero di spazi a destra
    i_rspaces=Strtran(p_cStr,Rtrim(p_cStr),'')
    * --- Traduzione (x sicurezza)
    If !Used('cp_lang')
        Select 0
        Use ..\dictionary\cp_lang && Zucchetti Aulla - Aggiunta cartella Dictionary
    Endif
    * --- Distingue il caso autoappendimento da traduzione
    If p_cDestLang="auto"
        * --- Caso autoappendimento
        Set Exact Off
        Select cp_lang
        If Not(Seek(Left(p_cStr+Space(i_nPAD),i_nPAD),'cp_lang',1))
            Append Blank
            Replace cp_lang->orig_str With p_cStr
        Endif
    Else
        * --- Caso traduzione
        Local rn,rc
        rn=0
        * --- Zucchetti Aulla inizo
        If Vartype(g_DISABLEDLLTRANSLATION)='L' And g_DISABLEDLLTRANSLATION
            * --- Zucchetti Aulla fine
            Set Exact On
            If Seek(Left(p_cStr+Space(i_nPAD),i_nPAD)+p_cDestLang,'cp_lang',1)
                If Not(Empty(cp_lang->tran_str))
                    p_cStr = Rtrim(cp_lang->tran_str)+i_rspaces
                Endif
            Endif
            * --- Zucchetti Aulla inizo
        Else
            rc=Space(i_nPAD)
            rn=Find_Translation_AH(Rtrim(p_cStr),Rtrim(p_cDestLang),@rc,i_nPAD)
            If Not Empty(rc)
                p_cStr = Rtrim(Strtran(rc,Chr(0),''))+i_rspaces
            Endif
        Endif
        * --- Zucchetti Aulla fine
    Endif
    * --- Riposiziona area
    Select (i_nOldArea)
    Set Exact &i_oldexact
    * --- rimette i ':' finali
    If i_bColon
        p_cStr = p_cStr + ':'
    Endif
    * --- rimette gli spazi iniziali e finali
    p_cStr = i_inispaces+p_cStr +i_finspaces
    * --- Risultato
    Return p_cStr

Func cp_RetrieveMacroVcx(i_cMacroName)
    Local i_cReturnValue
    Do Case
        Case i_cMacroName = "MSG_QUERY_FILE"
            i_cReturnValue = cp_Translate(MSG_QUERY_FILE)
        Case i_cMacroName = "MSG__BELONGS_TO_PRIMARY_KEY_F_DRAG_AND_DROP_WILL_BE_DISABLED_C_CONTINUE_QP"
            i_cReturnValue = cp_Translate(MSG__BELONGS_TO_PRIMARY_KEY_F_DRAG_AND_DROP_WILL_BE_DISABLED_C_CONTINUE_QP)
        Case i_cMacroName = "MSG_CONFIGURATION_FILE_MISSING_QM"
            i_cReturnValue =cp_Translate(MSG_CONFIGURATION_FILE_MISSING_QM)
        Case i_cMacroName = "MSG_POSTIN_FOLDER"
            i_cReturnValue = cp_Translate(MSG_POSTIN_FOLDER)
        Case i_cMacroName = "MSG_DELETE"
            i_cReturnValue = cp_Translate(MSG_DELETE)
        Case i_cMacroName = "MSG_FORMAT"
            i_cReturnValue = cp_Translate(MSG_FORMAT)
        Case i_cMacroName = "MSG_OK"
            i_cReturnValue = cp_Translate(MSG_OK)
        Case i_cMacroName = "MSG_HEIGHT"
            i_cReturnValue = cp_Translate(MSG_HEIGHT)
        Case i_cMacroName = "MSG_TITLE"
            i_cReturnValue = cp_Translate(MSG_TITLE)
        Case i_cMacroName = "MSG_CANCEL"
            i_cReturnValue = cp_Translate(MSG_CANCEL)
        Case i_cMacroName = "MSG_SELECT"
            i_cReturnValue = cp_Translate(MSG_SELECT)
        Case i_cMacroName = "MSG_ORDER_BY"
            i_cReturnValue = cp_Translate(MSG_ORDER_BY)
        Case i_cMacroName = "MSG_FIELDS"
            i_cReturnValue = cp_Translate(MSG_FIELDS)
        Case i_cMacroName = "MSG_FIELD"
            i_cReturnValue = cp_Translate(MSG_FIELD)
        Case i_cMacroName = "MSG_FILE"
            i_cReturnValue = cp_Translate(MSG_FILE)
        Case i_cMacroName = "MSG_SQL"
            i_cReturnValue = cp_Translate(MSG_SQL)
        Case i_cMacroName = "MSG_EDIT_SQL_SENTENCE"
            i_cReturnValue = cp_Translate(MSG_EDIT_SQL_SENTENCE)
        Case i_cMacroName = "MSG_OR"
            i_cReturnValue = cp_Translate(MSG_OR)
        Case i_cMacroName = "MSG_QUERY"
            i_cReturnValue = cp_Translate(MSG_QUERY)
        Case i_cMacroName = "MSG_SAVE"
            i_cReturnValue = cp_Translate(MSG_SAVE)
        Case i_cMacroName = "MSG_S_SAVE"
            i_cReturnValue = cp_Translate(MSG_S_SAVE)
        Case i_cMacroName = "MSG_MODIFY_BS_CREATE_MSWORD"
            i_cReturnValue = cp_Translate(MSG_MODIFY_BS_CREATE_MSWORD)
        Case i_cMacroName = "MSG_MODIFY_BS_CREATE_REPORT"
            i_cReturnValue = cp_Translate(MSG_MODIFY_BS_CREATE_REPORT)
        Case i_cMacroName = "MSG_KEEP_DIMENSIONS"
            i_cReturnValue = cp_Translate(MSG_KEEP_DIMENSIONS)
        Case i_cMacroName = "MSG_ASK_FOR_PARAMETERS"
            i_cReturnValue = cp_Translate(MSG_ASK_FOR_PARAMETERS)
        Case i_cMacroName == "MSG_DEFAULT"
            i_cReturnValue = cp_Translate(MSG_DEFAULT)
        Case i_cMacroName == "MSG_DEFAULT_HEIGHT_GRID"
            i_cReturnValue = cp_Translate(MSG_DEFAULT_HEIGHT_GRID)
        Case i_cMacroName = "MSG_TEXT_COLOR"
            i_cReturnValue = cp_Translate(MSG_TEXT_COLOR)
        Case i_cMacroName = "MSG_COLUMN_TITLE"
            i_cReturnValue = cp_Translate(MSG_COLUMN_TITLE)
        Case i_cMacroName = "MSG_WIDTH"
            i_cReturnValue = cp_Translate(MSG_WIDTH)
        Case i_cMacroName = "MSG_EDITABLE_LB_IN_EDITABLE_ZOOM_RB"
            i_cReturnValue = cp_Translate(MSG_EDITABLE_LB_IN_EDITABLE_ZOOM_RB)
        Case i_cMacroName = "MSG_BACKGROUND_COLOR"
            i_cReturnValue = cp_Translate(MSG_BACKGROUND_COLOR)
        Case i_cMacroName = "MSG_CANNOT_ORDER_USING_MEMO_FIELDS_QM"
            i_cReturnValue = cp_Translate(MSG_CANNOT_ORDER_USING_MEMO_FIELDS_QM)
        Case i_cMacroName = "MSG_YOU_CANNOT_ADD_THIS_MEMO_TO_THE_FOLDER"
            i_cReturnValue = cp_Translate(MSG_YOU_CANNOT_ADD_THIS_MEMO_TO_THE_FOLDER)
        Case i_cMacroName = "MSG_OPTIONS"
            i_cReturnValue = cp_Translate(MSG_OPTIONS)
        Case i_cMacroName = "MSG_OPEN"
            i_cReturnValue = cp_Translate(MSG_OPEN)
        Case i_cMacroName = "MSG_ADVANCED"
            i_cReturnValue = cp_Translate(MSG_ADVANCED)
        Case i_cMacroName = "MSG_VALUE_NOT_CORRECT_QM"
            i_cReturnValue = cp_Translate(MSG_VALUE_NOT_CORRECT_QM)
        Case i_cMacroName = "MSG_DATE"
            i_cReturnValue = cp_Translate(MSG_DATE)
    Endcase
    Return (i_cReturnValue)

Func vrtClass(i_oObj)
    Local i_aCls,i
    Dimension i_aCls[1]
    For i=1 To Aclass(i_aCls,i_oObj)
        If Inlist(i_aCls[i],"STDDYNAMICCHILDCONTAINER","STDFORM","STDPCFORM","STDTRSFORM",'STDFIELD','STDMEMO','STDTRSFIELD','STDTRSMEMO','STDSTRING','STDCHECK','STDTRSCHECK','STDRADIO','STDTRSRADIO','STDCOMBO','STDTRSCOMBO','STDBUTTON','STDBOX',"STDPCCONTAINER","STDPCTRSCONTAINER")
            Return(Left(i_aCls[i],1)+Lower(Substr(i_aCls[i],2)))
        Endif
    Next
    Return('notAVrtClass')

Func cp_GetGlobalVar(i_cName,i_cType)
    Local i_cName
    If Type(i_cName)<>'U'
        res=Eval(i_cName)
    Else
        If Vartype(i_cType)='C'
            Do Case
                Case i_cType='C' Or i_cType='M'
                    res=''
                Case i_cType='N' Or i_cType='Y'
                    res=0.00
                Case i_cType='D' Or i_cType='T'
                    res={  /  /  }
                Case i_cType='L'
                    res=.F.
            Endcase
            Public &i_cName
            &i_cName = res
        Else
            Error "Undefined global variable "+i_cName
        Endif
    Endif
    Return(res)

Proc cp_SetGlobalVar(i_cName,i_xValue)
    If Type(i_cName)='U'
        Public &i_cName
    Endif
    &i_cName = i_xValue
    Return

Func cp_InsertInto_(i_nConn,i_cTable,i_cTmpName, i_nTblIdx)
    Local i_nRows,i_cFields,i_i
    i_nRows=0
    * --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC,UTCC
    Local bUTDC,cUTCC,cUTDC,cUTCCvalue,cUTDCvalue,riftable,InsertSQL
    * --- riftable dovrebbe essere passato come parametro, ma si deve modificare il template
    riftable=Alltrim(Lower(Iif(i_cTable=Alltrim(i_CODAZI),Substr(i_cTable,Len(Alltrim(i_CODAZI))+1),i_cTable)))
    i_i=Iif(Empty(riftable),0,Ascan(i_TableProp, riftable, -1, -1, 1, 14))
    Store "" To cUTCC,cUTDV,cUTCCvalue,cUTDVvalue
    bUTDC=i_i>0 And i_TableProp[i_i,8]=1
    If m.bUTDC
        * Nel comportamento standard (g_UPDSYSFLD="DEF" oppure g_UPDSYSFLD non definita) l'utente schedulatore non aggiorna i campi
        bUTDC = Type("g_SCHEDULER")<>"C" Or g_SCHEDULER<>"S"
        bUTDC = Iif(Type("g_UPDSYSFLD")="C", g_UPDSYSFLD="ON" Or g_UPDSYSFLD="DEF" And m.bUTDC, m.bUTDC)
    Endif
    If m.bUTDC
        cUTCC=",UTCC"
        cUTDC=",UTDC"
        cUTCCvalue=","+cp_ToStrODBC(i_CODUTE)
        cUTDCvalue=","+cp_ToStrODBC(SetInfoDate(g_CALUTD))
    Endif
    * --- Zucchetti Aulla Fine - Verifico presenza campo UTDC,UTCC
    * --- prende la lista dei campi da inserire (e' necessario questo giro perche' la insert into diretta e' posizionale!)
    cp_SQL(i_nConn,'select * from '+i_cTmpName+' where 1=0')
    i_cFields=''
    For i_i=1 To Fcount()
        If Lower(Field(i_i))<>'cpccchk'
            i_cFields=i_cFields+','+Field(i_i)
        Endif
        * --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC,UTCC
        If bUTDC
            If Lower(Field(i_i))='utcc'
                cUTCC=""
                cUTCCvalue=""
            Endif
            If Lower(Field(i_i))='utdc'
                cUTDC=""
                cUTDCvalue=""
            Endif
        Endif
        * --- Zucchetti Aulla Fine - Verifico presenza campo UTDC,UTCC
    Next
    Use
    * --- ora e' pronto per inserire dalla tabella temporanea
    i_cFields=Substr(i_cFields,2)
    * --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC,UTCC
    If bUTDC
        If Type('i_nTblIdx')<>'N' Or i_TableProp[i_nTblIdx,7]=1
            InsertSQL='insert into '+i_cTable+' ('+i_cFields+',cpccchk'+cUTCC+cUTDC+')(select '+i_cFields+",'"+cp_NewCCChk()+"'"+cUTCCvalue+cUTDCvalue+" from "+i_cTmpName+')'
        Else
            InsertSQL='insert into '+i_cTable+' ('+i_cFields+cUTCC+cUTDC+')(select '+i_cFields+cUTCCvalue+cUTDCvalue+" from "+i_cTmpName+')'
        Endif
    Else
        If Type('i_nTblIdx')<>'N' Or i_TableProp[i_nTblIdx,7]=1
            InsertSQL='insert into '+i_cTable+' ('+i_cFields+',cpccchk)(select '+i_cFields+",'"+cp_NewCCChk()+"' from "+i_cTmpName+')'
        Else
            InsertSQL='insert into '+i_cTable+' ('+i_cFields+')(select '+i_cFields+' from '+i_cTmpName+')'
        Endif
    Endif
    i_nRows=cp_TrsSQL(i_nConn,InsertSQL)
    * --- Zucchetti Aulla Fine - Verifico presenza campo UTDC,UTCC
    cp_DropTempTable(i_nConn,i_cTmpName)
    Return(i_nRows)

Func cp_InsertIntoSQL(i_nConn,i_cTable,i_cFields,i_cSel,i_nTblIdx)
    Local i_nRows,i_cTmpName
    i_nRows=0
    i_cTmpName=cp_getTempTableName(i_nConn)
    * --- crea una tabella temporanea per avere la lista dei campi
    If cp_CreateTempTable(i_nConn,i_cTmpName,i_cFields,i_cSel)
        i_nRows=cp_InsertInto_(i_nConn,i_cTable,i_cTmpName,i_nTblIdx)
    Endif
    Return(i_nRows)

Func cp_InsertIntoVQR(i_oParentObject,i_nConn,i_cTable,i_cQuery,i_nTblIdx)
    Local i_nRows,i_cTmpName,i_i
    i_nRows=0
    i_cTmpName=cp_getTempTableName(i_nConn)
    vq_exec(i_cQuery,i_oParentObject,.F.,.F.,.F.,.T.,.F.,.F.,i_cTmpName)
    i_nRows=cp_InsertInto_(i_nConn,i_cTable,i_cTmpName,i_nTblIdx)
    Return(i_nRows)

    * --- standard functions
Func LRTRim(s)
    Return(Alltrim(s))

Func CharToBool(v)
    Return(Upper(Left(v,1))='T')

Func BoolToChar(v)
    Return Iif(v,'True','False')

Func CharToDate(v)
    Return Ctod(v)

Func DateToChar(v)
    Return(Dtos(v))

Func Dayofweek(v)
    Return(Dow(v))

Func NullDate(v)
    Return(Ctod("  /  /  "))

Func NullDateTime(v)
    Return(Ctot(""))


Func cp_PartialKey(i_k1,i_k2)
    * --- crea una chiave parziale con tanti campi di k1 quanti ce ne sono in k2
    Local i_p
    i_p=At(',',i_k1,Occurs(',',i_k2)+1)
    If i_p>0
        i_k1=Left(i_k1,i_p-1)
    Endif
    Return(i_k1)

Func cp_FixSubqueryKey(i_cKey,i_cDatabaseType)
    Do Case
        Case i_cDatabaseType='VFP' And At(',',i_cKey)<>0
            *i_cKey=Strtran(i_cKey,',','+')
            *--- Converto tutto in carattere
            i_cKey='Transform('+Strtran(i_cKey,',',')+Transform(')+')'
        Case i_cDatabaseType='SQLServer' And At(',',i_cKey)<>0
            *i_cKey='CAST('+Strtran(i_cKey,',',' AS CHAR),CAST(')+' AS CHAR)'
            i_cKey='CAST('+Strtran(i_cKey,',',' AS CHAR)+CAST(')+' AS CHAR)'
    Endcase
    Return(i_cKey)

Function cp_getEntityType(i_cEntityName)
    Local i_olderr,i_res
    i_res='unkown'
    i_olderr=On('error')
    On Error =.T.
    Do getEntityType In &i_cEntityName With i_res
    On Error &i_olderr
    Return (i_res)

    * ---- Resize

Procedure cp_ResizeContainer(i_obj, i_nPageResizeContainer)
    *ACTIVATE SCREEN
    *? 'Resize'
    *? i_obj.width,i_obj.height,i_obj.Pages(1).oPag.width,i_obj.Pages(1).oPag.height
    Local i_dx,i_dy,i_oldw,i_oldh,i_i,i_j,i_c,i_class,i_bRefresh,l_dx, l_dy, i_nPageResizeContainerEnd
    i_oldw=i_obj.Pages(1).oPag.Width
    i_oldh=i_obj.Pages(1).oPag.Height
    i_dx=i_obj.Width-Iif(Lower(i_obj.Class)<>"cp_opgfrm",6,0)-i_oldw
    i_dy=i_obj.Height-Iif(i_obj.PageCount>1 And Lower(i_obj.Class)<>"cp_opgfrm",35,0)-i_obj.Pages(1).oPag.Height
    * ---
    If i_oldw+i_dx<i_obj.Pages(1).oPag.stdwidth
        i_dx=i_obj.Pages(1).oPag.stdwidth-i_oldw
    Endif
    If i_oldh+i_dy<i_obj.Pages(1).oPag.stdheight
        i_dy=i_obj.Pages(1).oPag.stdheight-i_oldh
    Endif
    *? 'dx->',i_dx,'dy->',i_dy
    *--
    Local i_i,i_j,i_c,i_class
    Local i_bRefresh
    Local i_c_Top, i_c_Left, i_c_Width, i_c_Height
    i_bRefresh=.F.
    If i_dx<>0 Or i_dy<>0
        If Vartype(i_nPageResizeContainer)='N' And i_nPageResizeContainer<>0
            i_nPageResizeContainerEnd=i_nPageResizeContainer
        Else
            i_nPageResizeContainer=1
            i_nPageResizeContainerEnd=i_obj.PageCount
        Endif
        For i_j=i_nPageResizeContainer To i_nPageResizeContainerEnd
            If Vartype(i_obj.Pages(i_j).oPag)='O'
                i_obj.Pages(i_j).oPag.Width=i_obj.Pages(i_j).oPag.Width+i_dx
                i_obj.Pages(i_j).oPag.Height=i_obj.Pages(i_j).oPag.Height+i_dy
            Endif
            If Vartype(i_obj.Pages(i_j).oPag)='O' And ((i_dx<>0 And i_obj.Pages(i_j).oPag.resizeXpos<>-1) Or (i_dy<>0 And i_obj.Pages(i_j).oPag.resizeYpos<>-1))
                * -- ora muove i control
                For i_i=1 To i_obj.Pages(i_j).oPag.ControlCount
                    i_c=i_obj.Pages(i_j).oPag.Controls(i_i)
                    i_class=Upper(i_c.Class)
                    i_c_Top = i_c.Top
                    i_c_Left = i_c.Left
                    i_c_Width = i_c.Width
                    i_c_Height = i_c.Height
                    *? i_class
                    *if inlist(i_class,'STDBUTTON','STDBOX','STDFIELD','STDSTRING','STDEDIT','STDCOMBO','STDCHECK','STDTABLECOMBO','IMAGE','CP_ZOOMBOX')
                    * --- Non sposto bottoncini contestuali ne bordi per campi obbligatori (si spostano da se)
                    *-- Gestisco solo i campi che non sono stati calcolati nella SetAnchorForm
                    If Type("i_c.Anchor")<>"U" And Type("i_c.bAnchorFormDefault")="U" And !InList(i_class, "MANYHEADER") && And i_class<>'BTNZOOM' And i_class<>'OBLSHAPE' And i_class<>'BTNTRANS' And i_class<>'MANYHEADER'
                        If i_class='CP_ZOOMBOX' Or i_class='CP_SZOOMBOX'
                            i_c.cHeight=0
                            i_c.AdjustToSize()
                        Endif
                        *--- l_dx, l_dy servono per far allargare solo i controlli attraversati dalle righe di resize (i_c.cpResize(l_dx, l_dy))
                        Local l_dx, l_dy
                        l_dx = 0
                        l_dy = 0
                        If i_dx<>0 And i_obj.Pages(i_j).oPag.resizeXpos<>-1
                            If i_c_Left>i_obj.Pages(i_j).oPag.resizeXpos
                                i_c_Left=Iif(i_c_Left+i_dx<0, 0, i_c_Left+i_dx)
                            Else
                                If i_c_Left<i_obj.Pages(i_j).oPag.resizeXpos And i_c_Left+i_c_Width>i_obj.Pages(i_j).oPag.resizeXpos And i_class<>'STDCHECK'
                                    i_c_Width=Iif(i_c_Width+i_dx<0, 0, i_c_Width+i_dx)
                                    l_dx = i_dx
                                Endif
                            Endif
                        Endif
                        If i_dy<>0 And i_obj.Pages(i_j).oPag.resizeYpos<>-1
                            If i_c_Top>i_obj.Pages(i_j).oPag.resizeYpos
                                i_c_Top=Iif(i_c_Top+i_dy<0, 0, i_c_Top+i_dy)
                            Else
                                If i_c_Top<i_obj.Pages(i_j).oPag.resizeYpos And i_c_Top+i_c_Height>i_obj.Pages(i_j).oPag.resizeYpos And i_class<>'STDCHECK'
                                    i_c_Height=Iif(i_c_Height+i_dy<0, 0, i_c_Height+i_dy)
                                    l_dy = i_dy
                                Endif
                            Endif
                        Endif
                        *--- utilizzando la move eseguo un solo resize
                        If Pemstatus(i_c, "Move", 5)
                            i_c.Move(i_c_Left, i_c_Top, i_c_Width, i_c_Height)
                        Endif
                        If i_class='CP_ZOOMBOX' Or i_class='CP_SZOOMBOX'
                            i_c.cHeight=i_c_Height
                            i_c.AdjustToSize()
                        Endif
                        If i_class='CP_BROWSER'
                            i_c.AdjustToSize()
                        Endif
                        If i_class='CP_GADGETCALENDAR'
                            i_c.CalHeader.AdjustDate()
                            i_c.CalGrid.Resize()
                        Endif
                        If i_class='CP_HEADERGADGET'
                            i_c.AdjustLblPage()
                        Endif
                        If i_class='STDDYNAMICCHILDCONTAINER' Or Type('i_c.oPgFrm')='O' Or (Type('i_c.child')='O' And Type('i_c.child.oPgFrm')='O') Or i_class='CP_TREEVIEW' Or i_class='CP_TOOLBARTREE' Or i_class='CP_GRAPH'
                            If l_dx<>0 Or l_dy<>0
                                i_c.cpResize(l_dx, l_dy)
                            Endif
                        Endif
                        If i_class='STDBODY' And Vartype(i_c.oBodyCol)<>"U"
                            Local i_k, i_cTrs, i_classTrs
                            Local i_cTrs_Top, i_cTrs_Left, i_cTrs_Width, i_cTrs_Height
                            i_c.oBodyCol.Width = Iif(i_c.oBodyCol.Width + i_dx<=0, i_c.oBodyCol.Width, i_c.oBodyCol.Width + i_dx)
                            i_c.oBodyCol.oRow.Width = Iif(i_c.oBodyCol.oRow.Width + i_dx<=0, i_c.oBodyCol.oRow.Width, i_c.oBodyCol.oRow.Width + i_dx)
                            For i_k = 1 To i_c.oBodyCol.oRow.ControlCount
                                i_cTrs = i_c.oBodyCol.oRow.Controls(i_k)
                                i_classTrs = Upper(i_cTrs.Class)
                                i_cTrs_Top = i_cTrs.Top
                                i_cTrs_Left = i_cTrs.Left
                                i_cTrs_Width = i_cTrs.Width
                                i_cTrs_Height = i_cTrs.Height
                                If Inlist(i_classTrs,'STDBUTTON','STDTRSFIELD','STDTRSMEMO','STDTRSCOMBO','STDTRSCHECK','STDTRSTABLECOMBO','STDTRSRADIO','CP_ASKFILE','CP_ASKPICTFILE','CP_WORDDOCUMENT')
                                    If i_dx<>0 And i_obj.Pages(i_j).oPag.resizeXpos<>-1
                                        If i_cTrs_Left>i_obj.Pages(i_j).oPag.resizeXpos
                                            i_cTrs_Left=Iif(i_cTrs_Left+i_dx<0, 0, i_cTrs_Left+i_dx)
                                        Else
                                            If i_cTrs_Left<i_obj.Pages(i_j).oPag.resizeXpos And i_cTrs_Left+i_cTrs_Width>i_obj.Pages(i_j).oPag.resizeXpos And i_classTrs<>'STDTRSCHECK'
                                                i_cTrs_Width=Iif(i_cTrs_Width+i_dx<0, 0, i_cTrs_Width+i_dx)
                                            Endif
                                        Endif
                                    Endif
                                    If Pemstatus(i_cTrs, "Move", 5)
                                        i_cTrs.Move(i_cTrs_Left, i_cTrs_Top, i_cTrs_Width, i_cTrs_Height)
                                    Endif
                                Endif
                            Endfor
                            i_bRefresh=.T.
                        Endif
                        *ENDIF
                        * --- Zucchetti Aulla - Chiude IF per bottoncini/shape...
                    Endif
                Next
            Endif
        Next
    Endif
    If i_bRefresh
        i_obj.Refresh()
    Endif
    Return

    *-- Zucchetti Aulla Inizio: Gestione ancoraggio oggetti
Proc Cp_SetAnchorForm(i_obj, i_nPage)
    Local nLastPage, i, j, k, oCtrl, XPos, YPos, cClass
    If Vartype(m.i_nPage)='N' And m.i_nPage<>0
        nLastPage=i_nPage
    Else
        i_nPage=1
        nLastPage=i_obj.oPgFrm.PageCount
    EndIf
    For m.j=m.i_nPage To m.nLastPage
        If Vartype(m.i_obj.oPgFrm.Pages(m.j).oPag)='O'
            XPos = i_obj.oPgFrm.Pages(m.j).oPag.resizeXpos
            YPos = i_obj.oPgFrm.Pages(m.j).oPag.resizeYpos
            *-- Setto la propriet� Anchor di tutti gli oggetti della pagina
            *-- Per motivi di complessit� della resize non ancoro gli oggetti Body, i figli integrati e gli zoom
            *-- Verranno gestiti nella cp_ResizeContainer
            For i=1 To m.i_obj.oPgFrm.Pages(m.j).oPag.ControlCount
                oCtrl=i_obj.oPgFrm.Pages(m.j).oPag.Controls(m.i)
                If Type("oCtrl.Anchor")<>"U"
                    *-- Escludo anche gli oggetti con Anchor specificata nel def
                    cClass = Upper(oCtrl.Class)
                    If cClass="CP_TREEVIEW"
                        oCtrl = oCtrl.oTree
                    EndIf
                    If oCtrl.Anchor=0 And (Type("oCtrl.bNoSetAnchorForm")<>"L" Or Not oCtrl.bNoSetAnchorForm)
                        *-- Aggiungo la propriet� 'a runtime' per distinguere gli oggetti dalla cp_ResizeContainer
                        AddProperty(oCtrl,"bAnchorFormDefault",.T.)
                        *-- Confronto con Resize verticale
                        If oCtrl.Left + oCtrl.Width > m.XPos And m.XPos>-1
                            oCtrl.Anchor = oCtrl.Anchor + 8
                            If oCtrl.Left < m.XPos
                                oCtrl.Anchor = oCtrl.Anchor + 2
                            Endif
                        Endif
                        *-- Confronto con Resize orizzontale
                        If oCtrl.Top + oCtrl.Height > m.YPos And m.YPos>-1
                            oCtrl.Anchor = oCtrl.Anchor + 4
                            If oCtrl.Top < m.YPos
                                oCtrl.Anchor = oCtrl.Anchor + 1
                            Endif
                        Endif
                    Endif
                Endif
            Next
        Endif
    Next
Endproc
*-- Zucchetti Aulla Inizio: Gestione ancoraggio oggetti

* ---- Aggiunta campi

Procedure cp_LoadExtDict()
    If Vartype(i_extdict)='U'
        Public i_extdict
        i_extdict=Createobject('DCX')
        i_extdict.bRunTimeFields=.T.
        * ---Zucchetti Aulla Inizio - Framework monitor
        If i_MonitorFramework
            If cp_fileexist(Alltrim(i_usrpath)+'cp_added_fields.xdc', .T.)
                i_ok=i_extdict.Load(Alltrim(i_usrpath)+'cp_added_fields')
            Endif
        Else
            If cp_fileexist('cp_added_fields.xdc', .T.)
                i_ok=i_extdict.Load('cp_added_fields')
            Endif
        Endif
        * ---Zucchetti Aulla Fine - Framework monitor
    Endif
    Return

Procedure cp_AddExtFldsTab(i_obj,i_tblname,i_procname)
    Local i_ntb,i_n,i_i,i_c,i_x,i_l,i_t,i_v,i_pict,i_vpos,i_wi
    cp_LoadExtDict()
    i_ntb=i_extdict.GetFieldsCount(i_tblname)
    If i_ntb<>0
        i_n=i_obj.PageCount
        i_obj.PageCount=i_n+1
        i_obj.ExtFldsPage=i_n
        i_obj.Pages(i_n).AddObject("oPag","stdcontainer",i_procname,i_n)
        i_obj.Pages(i_n).oPag.Height=i_obj.Height
        i_obj.Pages(i_n).oPag.Width=i_obj.Width
        i_wi=i_obj.Width
        i_obj.Pages(i_n).oPag.Visible=.T.
        i_vpos=0
        Local rt_seq
        rt_seq=Iif(Vartype(i_obj.Parent.max_rt_seq)<>'U',i_obj.Parent.max_rt_seq,10000)
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_t=i_extdict.GetFieldType(i_tblname,i_i)
            i_l=i_extdict.GetFieldLen(i_tblname,i_i)
            i_d=i_extdict.GetFieldDec(i_tblname,i_i)
            i_x="label"+Alltrim(Str(i_i))
            i_obj.Pages(i_n).oPag.AddObject(i_x,"StdString")
            i_obj.Pages(i_n).oPag.&i_x..Caption=i_extdict.GetFieldDescr(i_tblname,i_i)+':'
            i_obj.Pages(i_n).oPag.&i_x..Top=12+i_vpos
            i_obj.Pages(i_n).oPag.&i_x..Left=10
            i_obj.Pages(i_n).oPag.&i_x..Width=100
            i_obj.Pages(i_n).oPag.&i_x..Height=25
            i_obj.Pages(i_n).oPag.&i_x..Alignment=1
            i_obj.Pages(i_n).oPag.&i_x..AddProperty('uid','XLBL_'+i_c)
            i_obj.Pages(i_n).oPag.&i_x..Visible=.T.
            * -- aggiunge il campo
            i_x="xfld_"+i_c
            i_v=cp_NullValueForType(i_t,i_l)
			IF i_t='M'
			   i_obj.Pages(i_n).oPag.AddObject(i_x,"StdMEMO")
			ELSE
			   i_obj.Pages(i_n).oPag.AddObject(i_x,"StdField")
			ENDIF
            i_obj.Pages(i_n).oPag.&i_x..Value=i_v
            i_obj.Parent.AddProperty('w_'+i_c,i_v)
            i_obj.Pages(i_n).oPag.&i_x..cFormVar='w_'+i_c
            i_obj.Pages(i_n).oPag.&i_x..Height=22
            i_obj.Pages(i_n).oPag.&i_x..Top=10+i_vpos
            i_obj.Pages(i_n).oPag.&i_x..Left=111
            i_obj.Pages(i_n).oPag.&i_x..Width=10*i_l
            i_wi=Max(i_wi,111+10*i_l)
            i_obj.Pages(i_n).oPag.&i_x..Height=22
            i_obj.Pages(i_n).oPag.&i_x..AddProperty('uid','XTXT_'+i_c)
            i_obj.Pages(i_n).oPag.&i_x..AddProperty('rtseq',rt_seq)
            Do Case
                Case i_t='N'
                    * --- forma la picture di default per Say e Get
                    i_pict='"'+Replicate('9',i_l-i_d)+Iif(i_d<>0,'.'+Replicate('9',i_d),'')+'"'
                    i_obj.Pages(i_n).oPag.&i_x..cSayPict=i_pict
                    i_obj.Pages(i_n).oPag.&i_x..cGetPict=i_pict
                    i_obj.Pages(i_n).oPag.&i_x..InputMask=Trim(cp_GetMask(i_pict))
                    i_obj.Pages(i_n).oPag.&i_x..Format='K'+cp_GetFormat(i_pict)
                Case i_t='C'
                    i_obj.Pages(i_n).oPag.&i_x..InputMask=Replicate("X", i_l)
                Case i_t='M'
                    i_obj.Pages(i_n).oPag.&i_x..Height=22+30
                    i_obj.Pages(i_n).oPag.&i_x..Width=250
                    i_wi=Max(i_wi,111+250)
                    i_vpos=i_vpos+30
            Endcase
            i_obj.Pages(i_n).oPag.&i_x..Visible=.T.
            i_vpos=i_vpos+25
        Next
        If i_obj.Pages(i_n).oPag.Height<i_vpos+10
            i_obj.Pages(i_n).oPag.Height=i_vpos+10
        Endif
        If i_obj.Pages(i_n).oPag.Width<i_wi
            i_obj.Pages(i_n).oPag.Width=i_wi
        Endif
    Endif
    Return

Procedure cp_SetControlsValueExtFlds(i_obj,i_tblname)
    Local i_ntb,i_n,i_i,i_c,i_p,i_t
    LOCAL TestMacro
    i_p=i_obj.oPgFrm.ExtFldsPage
    If i_p<>-1
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_t=i_extdict.GetFieldType(i_tblname,i_i)
            If i_t='C' Or i_t='M'
             TestMacro=Not(i_obj.oPgFrm.Pages(i_p).oPag.xfld_&i_c..Value==i_obj.w_&i_c)
                If TestMacro
                    i_obj.oPgFrm.Pages(i_p).oPag.xfld_&i_c..Value=i_obj.w_&i_c
                Endif
            ELSE
            TestMacro=i_obj.oPgFrm.Pages(i_p).oPag.xfld_&i_c..Value<>i_obj.w_&i_c
                If TestMacro
                    i_obj.oPgFrm.Pages(i_p).oPag.xfld_&i_c..Value=i_obj.w_&i_c
                Endif
            Endif
        Next
    Endif
    Return

Procedure cp_SetEnabledExtFlds(i_obj,i_tblname,i_cOp)
    Local i_ntb,i_v,i_i,i_c,i_p
    i_p=i_obj.oPgFrm.ExtFldsPage
    If i_p<>-1
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        i_v = i_cOp<>"Query"
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_obj.oPgFrm.Pages(i_p).oPag.xfld_&i_c..Enabled=i_v
        Next
    Endif
    Return

Procedure cp_LoadRecExtFlds(i_obj,i_tblname)
    Local i_ntb,i_i,i_c,i_t,i_l
    If i_obj.oPgFrm.ExtFldsPage<>-1
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_t=i_extdict.GetFieldType(i_tblname,i_i)
            i_l=i_extdict.GetFieldLen(i_tblname,i_i)
            If i_t='D'
                i_obj.w_&i_c=Nvl(&i_c.,cp_NullValueForType(i_t,i_l))
            Else
                i_obj.w_&i_c=Nvl(cp_ToDate(&i_c.),cp_NullValueForType(i_t,i_l))
            Endif
        Next
    Endif
    Return

Procedure cp_BlankRecExtFlds(i_obj,i_tblname)
    Local i_ntb,i_i,i_c,i_t,i_l
    If i_obj.oPgFrm.ExtFldsPage<>-1
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_t=i_extdict.GetFieldType(i_tblname,i_i)
            i_l=i_extdict.GetFieldLen(i_tblname,i_i)
            i_obj.w_&i_c=cp_NullValueForType(i_t,i_l)
        Next
    Endif
    Return

Function cp_ReplaceODBCExtFlds(i_obj,i_tblname)
    Local i_ntb,i_i,i_c,i_res
    i_res=""
    If i_obj.oPgFrm.ExtFldsPage<>-1
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_res=i_res+','+i_c+'='+cp_ToStrODBC(i_obj.w_&i_c.)
        Next
    Endif
    Return i_res

Function cp_ReplaceVFPExtFlds(i_obj,i_tblname)
    Local i_ntb,i_i,i_c,i_res
    i_res=""
    If i_obj.oPgFrm.ExtFldsPage<>-1
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_res=i_res+','+i_c+'=this.w_'+i_c
        Next
    Endif
    Return i_res

Function cp_InsertFldODBCExtFlds(i_obj,i_tblname)
    Local i_ntb,i_i,i_c,i_res
    i_res=""
    If i_obj.oPgFrm.ExtFldsPage<>-1
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_res=i_res+','+i_c
        Next
    Endif
    Return i_res

Function cp_InsertFldVFPExtFlds(i_obj,i_tblname)
    Return cp_InsertFldODBCExtFlds(i_obj,i_tblname)

Function cp_InsertValODBCExtFlds(i_obj,i_tblname)
    Local i_ntb,i_i,i_c,i_res
    i_res=""
    If i_obj.oPgFrm.ExtFldsPage<>-1
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_res=i_res+','+cp_ToStrODBC(i_obj.w_&i_c.)
        Next
    Endif
    Return i_res

Function cp_InsertValVFPExtFlds(i_obj,i_tblname)
    Local i_ntb,i_i,i_c,i_res
    i_res=""
    If i_obj.oPgFrm.ExtFldsPage<>-1
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        For i_i=1 To i_ntb
            i_c=i_extdict.GetFieldName(i_tblname,i_i)
            i_res=i_res+',this.w_'+i_c
        Next
    Endif
    Return i_res

    *** Gestione tasto destro ***
Define Class IM_ContextMenu As Custom
    * --- Oggetto padre (chiamante)
    oParentObject=.Null.
    *nome della gestione da lanciare nello zoomOnZoom
    cZoomOnZoom=''
    *Tipo Entit� abbinata allo ZoomonZoom
    cEntity=''
    *Oggetto control passato
    oCtrl=.Null.
    *Oggetto contained (maschera)
    oContained=.Null.
    *numero di chiavi
    nKeyCount=0
    *Tipo su cui � stato effettuato il rightclick
    *1=Control
    *2=Gestione
    *3=Zoom
    nType=0
    *Array contenente, nella prima colonna, l'elenco dei campi chiave; nella seconda colonna, il nome delle variabili
    *di tali campi presenti nel contesto; nella terza colonna il valore corrispondente.
    Dimension oKey(1,3)
    oKeyLength=0
    *Nome del file di menu
    cMenuFile=''
    *Nome dell'archivio linkato
    cLinkFile=''
    * Variabile utilizzata nel caso in cui nello ZoomOnZoom viene lanciato un batch.
    * Assume 'A': Apri, 'M': Modifica, 'C': Carica. Vuota se non viene lanciato un batch
    cBatchType = ' '
    * Variabili per il ripristino delle labelKey
    cOldLabel1=''
    cOldLabel2=''
    * Variabile che indica se ripristinare o meno le labelKey
    bRecLabel=.T.
    *contatore
    j=0
    * Zucchetti Aulla Inizio Personalizzazione Menu
    *Array contenente, nella prima colonna, l'elenco dei campi Aggiunti;
    *nella terza colonna il valore corrispondente.
    Dimension oFields[1,3]
    *Clausola di Where per recuperare tutti i dati dall'archivio linkato
    cWhere=''
    bLinkTable=.F.
    CustomProg	= ''
    cCursor=''
    * Zucchetti Aulla Fine Personalizzazione Menu
    Procedure Init
        Parameters pCtrl,pContained,pMenuFile,pType

        Local F, oKey_1, oKey_2,keyValue, cTempOnZoom

        This.oCtrl=pCtrl
        This.oContained=pContained
        This.nType=pType
        This.cMenuFile=pMenuFile
        This.j=1

        Do Case
            Case pType=1 &&Control
                This.cZoomOnZoom=Iif(Type('This.oCtrl.cZoomOnZoom')='C',This.oCtrl.cZoomOnZoom,'')
                This.cLinkFile=Iif(Type('This.oCtrl.cLinkFile')='C',This.oCtrl.cLinkFile,'')
                ** Inizializzo oKey[]
                F=1
                Do While Type('this.oCtrl.oKey_'+Alltrim(Str(F))+'_1')<>'U' And Type('this.oCtrl.oKey_'+Alltrim(Str(F))+'_1')<>'L'
                    oKey_1='this.oCtrl.oKey_'+Alltrim(Str(F))+'_1'
                    oKey_2='this.oCtrl.oKey_'+Alltrim(Str(F))+'_2'
                    This.oKeyLength=This.oKeyLength+1
                    Dimension This.oKey(This.oKeyLength,3)
                    This.oKey[f,1]=Alltrim(&oKey_1)
                    This.oKey[f,2]=Alltrim(Strtran(&oKey_2,'this.',''))
                    keyValue="this.oCtrl.parent.oContained."+Alltrim(Strtran(&oKey_2,'this.',''))
                    This.oKey[f,3]=&keyValue
                    This.nKeyCount=This.nKeyCount+1
                    F=F+1
                Enddo

                * --- Inizializzo oParentObject con oContained per avere i riferimenti con le
                * --- variabili sulla maschera
                This.oParentObject=This.oContained

            Case pType=2  &&Gestione
                This.cZoomOnZoom=''
                This.cLinkFile=This.oContained.oContained.cFile
                This.nKeyCount = Occurs(",", This.oContained.oContained.cKeySelect) +1
                **inizializza la variabile oKey
                If Not Empty(This.oContained.oContained.cKeySelect)
                    This.add_oKey(This.oContained.oContained.cKeySelect)
                Endif

                * --- Inizializzo oParentObject con oContained(propriet� di questa classe).oContained(propriet� della forms)
                * --- per avere i riferimenti con le
                * --- variabili sulla maschera
                This.oParentObject=This.oContained.oContained

            Case pType=3  &&Zoom
                ** Se l'oggetto su cui si esegue il click destro non appartiene alla griglia,
                * il programma va in errore
                * L'ggetto in cui si cerca cZoomOnZoom, nel caso si agisca sulla griglia, � this.oContained
                * altrimenti � This.oContained.pages[This.oContained.pagecount].controls[1]
                * L'oggetto giusto � assegnato a cToBeAssigned, utilizzata in questo ramo case al posto di this.oContained
                If Type("This.oContained.cZoomOnZoom")<>"U"
                    cToBeAssignedObj=This.oContained
                Else
                    If Type("This.oContained.pages[This.oContained.pagecount].controls[1]")<>"U"
                        cToBeAssignedObj=This.oContained.Pages[This.oContained.pagecount].Controls[1]
                    Endif
                Endif
                This.cZoomOnZoom=cToBeAssignedObj.cZoomOnZoom
                **Per permettere di visualizzare il menu personalizzato presente sul control
                ** anche nello zoom associato..
                This.cLinkFile=cToBeAssignedObj.cSymFile
                This.nKeyCount = Occurs(",", cToBeAssignedObj.cKeyFields) +1
                **inizializza la variabile oKey
                If Not Empty(cToBeAssignedObj.cKeyFields)
                    This.add_oKey(cToBeAssignedObj.cKeyFields)
                Endif
                This.oParentObject=cToBeAssignedObj.oParentObject
                * --- Zucchetti Aulla Inizio - Personalizzazione Menu Treeview
            Case pType=4  &&Treeview
                This.oParentObject=This.oContained
            Case pType=5  &&Treeview Nodes
                This.oParentObject=This.oContained
            Case pType=6  && FoxCharts
                This.oParentObject=This.oContained
            Case pType=7  && ZoomHeader
                This.oParentObject=This.oContained
            Case pType=8  && DetailHeader
                This.oParentObject=This.oContained
                * --- Zucchetti Aulla Fine - Personalizzazione Menu Treeview
        Endcase
        * Se ZoomOnZoom determino il tipo dell'entit� collegata..
        *Tipo Entit� abbinata allo ZoomonZoom
        If Not Empty(This.cZoomOnZoom)
            cTempOnZoom = This.cZoomOnZoom
            If At('(',This.cZoomOnZoom) <> 0
                cTempOnZoom = Alltrim(Substr(This.cZoomOnZoom, 1, At('(',This.cZoomOnZoom)-1 ))
            Endif
            This.cEntity=cp_getEntityType(cTempOnZoom)
        Endif
        * --- Zucchetti Aulla Inizio Personalizzazione Men�
        * --- Se la tabella linkata fa parte dell'elenco che serve per estendere il menu
        * --- facciamo una read per recuperare la struttura della tabella e dati
        This.bLinkTable = Upper(This.cLinkFile) $ Upper(Alltrim(g_LISTGOMENUTABLE))+Upper(Alltrim(g_CUSTGOMENUTABLE))
        If This.bLinkTable
            cCurs=This.Get_LinkInfo_oFields(This.oCtrl, This.cLinkFile)
            This.Add_oFields(cCurs)
        Endif
        * --- Zucchetti Aulla Fine
    Endproc
    * --- Zucchetti Aulla Inizio
    Function Get_LinkInfo_oFields
        Param pCtrl, pTableLink
        Local lCtrl, lProg, cCurs, lTableName, lTableIDX, cWhere
        lCtrl = pCtrl
        lProg = This.oParentObject
        lTableName=Upper(Alltrim(pTableLink))
        cCurs=Sys(2015)
        h=1
        * --- Costruisco la clausola di where
        If Empty(This.cWhere)
            Do While h<=This.nKeyCount
                keyValue=This.oKey[h,3]
                This.cWhere = This.cWhere + This.oKey[h,1]+"="+cp_ToStrODBC(keyValue)+" AND "
                h=h+1
            Enddo
            This.cWhere = Left(This.cWhere , Len(This.cWhere)-5)
        Endif
        * --- IDX della tabella
        lTableIDX=cp_OpenTable(lTableName)
        If lTableIDX = 0
            Return('')
        Endif
        i_nConn=i_TableProp[lTableIDX,3]
        i_cTable=cp_SetAzi(i_TableProp[lTableIDX,2])
        i_sSQL="SELECT * FROM "+ i_cTable +" WHERE " + This.cWhere
        cp_SQL(i_nConn,i_sSQL,cCurs)
        cp_CloseTable(lTableName)

        This.CustomProg="cp_Contexts_"+lTableName
        Return(cCurs)
    Endfunc

    * --- Aggiunge i valori all'array oFields partendo da un cursore
    Function Add_oFields
        Param pCurs
        Local cTblNameIdx
        Local l_FieldValue, l_FieldName, cCurs
        If Type('pCurs')<>"C"
            Return
        Endif
        cCurs = pCurs
        If Used(cCurs)
            Select (cCurs)
            Locate For 1=1
            l_z=Alen(This.oFields, 1)+ Iif(Alen(This.oFields, 1)=1, 0, 1)
            nFields=Afields(ArrCursStru, cCurs)
            *				l_z=1
            For l_i = 1 To nFields
                * -- Se il tipo del campo non � memo e non � il campo cpccchk
                Select (cCurs)
                If Upper(ArrCursStru(l_i,2))<>"M" And Lower(ArrCursStru(l_i,1))<>"cpccchk"
                    l_FieldName=Upper(Alltrim(ArrCursStru(l_i,1)))
                    Dimension This.oFields(l_z,3)
                    This.oFields[l_z,1]=Upper(Alltrim(ArrCursStru(l_i,1)))
                    This.oFields[l_z,2]=Space(1)
                    Select (cCurs)
                    This.oFields[l_z,3]=Nvl(&cCurs..&l_FieldName, cp_NullValue(&cCurs..&l_FieldName))
                    l_z=l_z+1
                Endif
            Endfor
            Use In (cCurs)
        Endif
    Endfunc
    * --- Zucchetti Aulla Fine

    **inizializza la variabile oKey; genera le differenze tra gestione e zoom
    Function add_oKey
        Param ref

        Local lx,h,err

        If This.nKeyCount>1
            This.oKeyLength=This.oKeyLength+1
            Dimension This.oKey(This.oKeyLength,3)
            This.oKey[1,1] = Alltrim(Substr(ref,1,At(",",ref)-1))
            If This.nType=2
                This.oKey[1,2] = "w_"+Alltrim(Substr(ref,1,At(",",ref)-1))
                lx="this.oContained.parent.parent.parent."+This.oKey[1,2]
                This.oKey[1,3] = &lx
            Else
                This.oKey[1,2] = Alltrim(Substr(ref,1,At(",",ref)-1))
                If Alen(This.oCtrl.xKey)>0
                    This.oKey[1,3] = This.oCtrl.xKey[1]
                Endif
            Endif
            h=2
            Do While h<=This.nKeyCount
                ref=Alltrim(Strtran(ref,This.oKey[h-1,1]+","))
                This.oKeyLength=This.oKeyLength+1
                Dimension This.oKey(This.oKeyLength,3)
                This.oKey[h,1]=ref
                err=At(",",This.oKey[h,1])
                If err != 0
                    This.oKey[h,1]=Substr(This.oKey[h,1],1,err-1)
                Endif
                If This.nType=2
                    This.oKey[h,2] = "w_"+Alltrim(This.oKey[h,1]) &&Alltrim(Strtran(ref,This.oKey[h-1,1]+","))
                    lx="this.oContained.parent.parent.parent."+This.oKey[h,2]
                    This.oKey[h,3] = &lx
                Else
                    This.oKey[h,2] = Alltrim(This.oKey[h,1]) &&Alltrim(Strtran(ref,This.oKey[h-1,1]+","))
                    If Alen(This.oCtrl.xKey)>=h
                        This.oKey[h,3] = This.oCtrl.xKey[h]
                    Endif
                Endif
                h=h+1
            Enddo
        Else
            This.oKey[1,1] = Alltrim(ref)
            If This.nType=2
                This.oKey[1,2] = "w_"+Alltrim(ref)
                lx="this.oContained.parent.parent.parent."+This.oKey[1,2]
                This.oKey[1,3] = &lx
            Else
                This.oKey[1,2] = Alltrim(ref)
                If Alen(This.oCtrl.xKey)>0
                    This.oKey[1,3] = This.oCtrl.xKey[1]
                Endif
            Endif
        Endif
    Endfunc

    **Restituisce, passato un indice, il nome del campo memorizzato in quella posizione;
    **Ritorna la stringa vuota se non presente
    Function getByIndexKeyName
        Param k
        If Type("this.oKey") <> 'L' And Type("this.oKey") <> 'U'
            If Type("this.oKey[k,1]") <> 'L' And Type("this.oKey[k,1]") <> 'U'
                Return(This.oKey[k,1])
            Else
                Return('')
            Endif
        Else
            Return ('')
        Endif
    Endfunc

    **Restituisce, passato un indice, il valore della variabile memorizzata in quella posizione;
    **Ritorna la stringa vuota se non presente
    Function getByIndexKeyValue
        Param k
        If Type("this.oKey") <> 'L' And Type("this.oKey")<>'U'
            If Type("this.oKey[k,3]") <> 'L' And Type("this.oKey[k,3]") <> 'U'
                Return(This.oKey[k,3])
            Else
                Return('')
            Endif
        Else
            Return ('')
        Endif
    Endfunc

    **Restituisce, passato il nome del campo, il valore corrispondente.
    **Ritorna la stringa vuota se il campo non � presente
    Function getValue
        Param fieldName
        Local k
        For k=1 To Alen(This.oKey,1)
            If Upper(This.oKey[k,1])==Upper(fieldName)
                Return (This.oKey[k,3])
            Endif
        Endfor
        Return('')
    Endfunc
    * --- Zucchetti Aulla Inizio - Personalizzazione Menu
    Function getFieldsValue
        Param fieldName
        Local k
        For k=1 To Alen(This.oFields,1)
            If Upper(This.oFields[k,1])==Upper(fieldName)
                Return (This.oFields[k,3])
            Endif
        Endfor
        Return('')
    Endfunc
    * --- Zucchetti Aulla Fine - Personalizzazione Menu

    ** Compone il nome del menu da caricare
    ** Return: il nome del file del menu
    Function menuFile
        Local i_nTest, i_cSearch, I_cNomeFile
        Dimension Itm(3)
        If Empty(This.cMenuFile)
            This.cMenuFile = 'Default'
        Endif
        Do Case
            Case This.nType=1 &&controllo
                m.i_nTest=3
            Case This.nType=2 &&gestione
                m.i_nTest=1
            Case This.nType=3 &&zoom
                m.i_nTest=0
                * --- Zucchetti Aulla Inizio - Personalizzazione Menu
            Case This.nType=4 &&Treeview
                m.i_nTest=0
            Case This.nType=5 && Treeview Nodes
                m.i_nTest=1
            Case This.nType=6 && FoxCharts
                m.i_nTest=0
            Case This.nType=7 && ZoomHeader
                m.i_nTest=0
            Case This.nType=8 && DetailHeader
                m.i_nTest=0
                * --- Zucchetti Aulla Fine - Personalizzazione Menu
        Endcase
        Itm[1]=This.cMenuFile
        Do While m.i_nTest>=0
            Do Case
                Case m.i_nTest=3
                    Itm[2]=Alltrim(Strtran(Upper(This.oCtrl.cFormVar),"W_"))
                    Itm[3]=This.oContained.cPrg
                Case m.i_nTest=2
                    Itm[2]=Alltrim(Strtran(Upper(This.oCtrl.cFormVar),"W_"))
                    Itm[3]=''
                    * --- Zucchetti Aulla Inizio - Personalizzazione Menu
                Case m.i_nTest=1
                    Do Case
                        Case This.nType=5 && Treeview Nodes
                            Itm[2]=Alltrim(Strtran(Upper(This.oCtrl.Name),"W_"))
                            Itm[3]='NODE'
                        Otherwise
                            If This.nType=1 &&siamo in un control
                                Itm[2]=This.oContained.cPrg
                            Else
                                Itm[2]=This.oContained.Parent.Parent.Parent.cPrg
                            Endif
                            Itm[3]=''
                    Endcase
                    * --- Zucchetti Aulla Fine - Personalizzazione Menu
                Case m.i_nTest=0
                    Do Case
                        Case This.nType=4 Or This.nType=5 Or This.nType=6 && Treeview and Node  and FoxCharts
                            Itm[2]=Alltrim(Strtran(Upper(This.oCtrl.Name),"W_"))
                        Otherwise
                            Itm[2]=This.cLinkFile
                    Endcase
                    Itm[3]=''
            Endcase
            m.i_cSearch=This.componiNome(@Itm)
            If Upper(Alltrim(m.i_cSearch))=='DEFAULT'
                m.i_cSearch=''
            Endif
            m.I_cNomeFile=cp_findNamedDefaultFileMenu(m.i_cSearch,'vmn') &&funziona solamente per i file contenuti nella \exe
            If Empty(m.I_cNomeFile)
                m.i_nTest=m.i_nTest-1
            Else
                Return (m.I_cNomeFile+'.vmn')
            Endif
        Enddo
        Return('')
    Endfunc

    ** Compone il nome del file di menu da ricercare, leggendo i dati da un array passato
    Function componiNome
        Parameters  p_itm
        Local nomeFile
        nomeFile=''
        For h=1 To Alen(p_itm)
            If Not Empty(p_itm[h])
                nomeFile=nomeFile+p_itm[h]+'_'
            Endif
        Endfor
        Return (Left(nomeFile,Rat('_',nomeFile)-1))

        ** Zucchetti Aulla Inizio Personalizzazione Menu contestuale
        ** Esegue il Programma Standard Legato alla tabella
    Function BeforeCreaMenu
        Local i_olderr,i_res, cFileName
        If This.bLinkTable And !Empty(This.CustomProg)
            i_res='unkown'
            i_olderr=On('error')
            On Error =.T.
            l_Macro=This.CustomProg && + "()"
            Do &l_Macro With This.oParentObject
            On Error &i_olderr
        Endif
        This.CustomCreaMenu()
        This.creaMenu()
    Endfunc

    ** Esegue il Programma per la personalizzazione legato alla tabella
    Function CustomCreaMenu
        Local i_olderr,i_res, cFileName
        i_res='unkown'
        i_olderr=On('error')
        On Error =.T.
        cFileName="cp_Contextp_"+Upper(Alltrim(This.cLinkFile))
        Do &cFileName With This.oParentObject
        On Error &i_olderr
    Endfunc
    ** Zucchetti Aulla Fine Personalizzazione Menu

    ** Inizializza il menu contestuale, crea l'eventuale menu personalizzato
    ** o di default caricandolo da file e chiama i metodi corretti per la creazione degli ulteriori
    ** menu forniti dall'applicazione
    Function creaMenu
        Local funzione,FLGT,FLGC,FLGI,FLGM,mfile,k, loPopupMenu
        k=This.j
        mfile=This.menuFile()
        If i_VisualTheme<>-1
            *--- Creo nuovo popup
            loPopupMenu = Createobject("cbPopupMenu")
            This.j = loPopupMenu
            If Not Empty(mfile)
                vu_context(mfile, loPopupMenu)
            Endif
        Else
            Define Popup contextmenu From Mrow(),Mcol() shortcut Margin
            If Not Empty(mfile)
                vu_context(mfile,.F.,'contextmenu',@k)
                This.j=k
                Define Bar (This.j) Of contextmenu Prompt "\-"
                This.j=This.j+1
            Endif
        Endif

        Do Case
            Case This.nType=1 &&CONTROL
                * --- chiamata metodo caricamento bottoni se inizialiazzata la variabile i_bLoadFuncButton e questa vale true
                * oppure  non esiste un men� contestuale per la gestione selezionata.
                If i_VisualTheme<>-1
                    k = loPopupMenu
                Else
                    * --- Salviamo il j in k per poi passarlo come riferimento
                    k=This.j
                Endif
                If i_bLoadFuncButton
                    *- Inserisco le funzionalit� associate ai bottoni
                    If Type('g_Omenu.oParentobject.bNoMenuFunction')='L'And Not g_Omenu.oParentObject.bNoMenuFunction
                        This.ButtonGest(@k)
                        *--- Zucchetti Aulla Inizio Frasi modello
                        If Upper(This.oCtrl.BaseClass)="EDITBOX" And !This.oCtrl.bdisablefrasimodello
                            This.CreaMenuFrasiModello(@k)
                        Endif
                        *--- Zucchetti Aulla Fine Frasi modello
                    Endif
                Endif
                If Vartype(This.oCtrl.bMultilanguage)='L' And This.oCtrl.bMultilanguage And Not Empty(i_aCpLangs[1]) And (Upper(This.oContained.cFunction)==Upper("Query") Or (Upper(This.oContained.cFunction)==Upper("Edit") And "Detail"$cp_getEntityType(This.oContained.cPrg))) And This.oContained.bLoaded
                    This.CreateMenuTranslate(@k)
                Endif
                * --- riaggiorniamo il j
                This.j=k
                This.controlMenu(This.j,'contextmenu')
            Case This.nType=2 &&Gestione
                * --- chiamata metodo caricamento bottoni se inizialiazzata la variabile i_bLoadFuncButton e questa vale true
                * oppure  non esiste un men� contestuale per la gestione selezionata.
                * --- Salviamo il j in k per poi passarlo come riferimento
                If i_VisualTheme<>-1
                    k = loPopupMenu
                Else
                    * --- Salviamo il j in k per poi passarlo come riferimento
                    k=This.j
                Endif
                If i_bLoadFuncButton
                    *- Inserisco le funzionalit� associate ai bottoni
                    If Type('g_Omenu.oParentobject.bNoMenuFunction')='L' And Not g_Omenu.oParentObject.bNoMenuFunction
                        This.ButtonGest(@k)
                    Endif
                    * --- riaggiorniamo il j
                    This.j=k
                Endif
                *--- Zucchetti Aulla Inizio - Configurazione gestione
                * --- Salviamo il j in k per poi passarlo come riferimento
                k=This.j
                *--- Il menu compare solo in edit
                *IF (UPPER(i_curform.cFunction)=="EDIT" AND cp_getEntityType(i_curform.cPrg)=="Dialog Window") AND TYPE("i_curform.cnt")<>"O"
                If Type("i_curform.cnt")<>"O"
                    CreaMenuCfgGestione(@k, 'M')
                Endif
                *--- Zucchetti Aulla Fine - Configurazione gestione
                *--- Zucchetti Aulla Inizio - Schedulatore di Job
                If  ((Vartype(g_JBSH)='C' And g_JBSH='S') Or (Vartype(g_IZCP)='C' And g_IZCP='S')) And Type("i_curform.cnt")<>"O"
                    If i_VisualTheme<>-1
                        Local loMenuItem
                        loMenuItem = loPopupMenu.AddMenuItem()
                        loMenuItem.Caption = cp_Translate(MSG_JOB_SCHEDULATOR)
                        loMenuItem.BeginGroup = .T.
                        loMenuItem.Picture='.\bmp\sched_job.ico'
                        loMenuItem.Visible = .T.
                        loMenuItem.OnClick = [do cp_DoAction with "ecpReadPar"]
                    Else
                        Define Bar (k) Of contextmenu Prompt cp_Translate(MSG_JOB_SCHEDULATOR) Picture g_Omenu.RetBmpForMenu('.\bmp\sched_job.bmp')
                        On Sele Bar (k) Of contextmenu Do cp_DoAction With "ecpReadPar"
                        k=k+1
                        * --- Separatore
                        Define Bar (k) Of contextmenu Prompt "\-"
                        k=k+1
                    Endif
                Endif
                * --- riaggiorniamo il j
                This.j=k
                *--- Zucchetti Aulla Inizio - Schedulatore di Job
                This.gestioneMenu(This.j,'contextmenu')
            Case This.nType=3 &&Zoom
                * --- chiamata metodo caricamento bottoni se inizialiazzata la variabile i_bLoadFuncButton e questa vale true
                * oppure  non esiste un men� contestuale per la gestione selezionata.
                If i_VisualTheme<>-1
                    This.j=loPopupMenu
                Endif
                If g_Omenu.oContained.Class <>'Cp_szoombox'
                    If i_bLoadFuncButton
                        * --- Salviamo il j in k per poi passarlo come riferimento
                        k=This.j
                        *- Inserisco le funzionalit� associate ai bottoni
                        If Type('g_Omenu.oContained.bNoMenuFunction')='L'And Not g_Omenu.oContained.bNoMenuFunction
                            This.ButtonGest(@k)
                        Endif
                        * --- riaggiorniamo il j
                        This.j=k
                        If i_VisualTheme<>-1
                            k=.Null.
                        Endif
                    Endif
                Endif
                This.zoomMenu(This.j,'contextmenu')
                * --- Zucchetti Aulla Inizio - Personalizzazione Menu Treeview
            Case This.nType=4 Or This.nType=5
                * --- chiamata metodo caricamento bottoni se inizialiazzata la variabile i_bLoadFuncButton e questa vale true
                * oppure  non esiste un men� contestuale per la gestione selezionata.
                If i_VisualTheme<>-1
                    k = loPopupMenu
                Else
                    * --- Salviamo il j in k per poi passarlo come riferimento
                    k=This.j
                Endif
                This.j=k
                If i_VisualTheme<>-1
                    k=.Null.
                Endif
                This.TreeViewMenu(This.j,'contextmenu')
                * --- Zucchetti Aulla Fine - Personalizzazione Menu Treeview
            Case This.nType=6 && FoxCharts
                * --- chiamata metodo caricamento FoxCharts se inizialiazzata la variabile i_bLoadFuncButton e questa vale true
                * oppure  non esiste un men� contestuale per la gestione selezionata.
                If i_VisualTheme<>-1
                    This.j=loPopupMenu
                Endif
                If Not(Inlist(g_Omenu.oContained.Class , 'cp_FoxCharts' , 'Cp_szoombox'))
                    If i_bLoadFuncButton
                        * --- Salviamo il j in k per poi passarlo come riferimento
                        k=This.j
                        *- Inserisco le funzionalit� associate ai bottoni
                        If Type('g_Omenu.oContained.bNoMenuFunction')='L' And Not g_Omenu.oContained.bNoMenuFunction
                            This.ButtonGest(@k)
                        Endif
                        * --- riaggiorniamo il j
                        This.j=k
                        If i_VisualTheme<>-1
                            k=.Null.
                        Endif
                    Endif
                Endif
                This.FoxChartsMenu(This.j,'contextmenu')
            Case This.nType=7 && ZoomHeader
                * --- menu per la gestione dei parametri aggiuntivi degli zoom gestibile su tasto destro header
                If i_VisualTheme<>-1
                    This.j=loPopupMenu
                Endif
                This.ZoomHeaderMenu(This.j,'contextmenu')
            Case This.nType=8 && DetailHeader
                * --- menu per la gestione dei parametri aggiuntivi dei detail gestibile su tasto destro header
                If i_VisualTheme<>-1
                    This.j=loPopupMenu
                Endif
                This.DetailHeaderMenu(This.j,'contextmenu')
        Endcase
        If i_VisualTheme<>-1
            This.j = .Null.
            loPopupMenu.Destroy()
            loPopupMenu = .Null.
        Endif
    Endfunc

    * --- Zucchetti Aulla Inizio
    * --- Crea il menu contestuale per gli oggetti Treeview
    Function TreeViewMenu
        Param j,contextmenu
        Local funzione, cFunzione
        cFunzione = Sys(2015)
        Public &cFunzione
        &cFunzione = ""
        funzione=''

        If Type('g_Omenu.octrl.bNoMenuProperty')='L'And Not g_Omenu.oCtrl.bNoMenuProperty
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_PROPERTY)
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.

                loMenuItem.OnClick = cFunzione+[ = 'P']
                l_BeginGroup = .F.
            Else
                *--- Non serve aggiungere il separatore perch� viene aggiunto
                *--- in precedenza dalla funzione creaMenu
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PROPERTY)
                On Sele Bar (j) Of contextmenu funzione='P'
            Endif
        Endif
        If i_VisualTheme<>-1
            * --- Disattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('D')
            m.j.InitMenu(.T.)
            m.j.ShowMenu()
            * --- Riattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('A')
            funzione = &cFunzione
        Else
            If Cntbar('contextmenu')>0
                * --- Disattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('D')
                Activate Popup contextmenu
                Deactivate Popup contextmenu
                Release Popups contextmenu
                * --- Riattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('A')
            Endif
        Endif
        This.cBatchType = funzione
        Do Case
            Case Left(funzione,1)='P'
                Cp_InfoTreeView(This)
        Endcase
        Release &cFunzione
    Endfunc


    * --- Crea il menu ZoomHeader
    Function ZoomHeaderMenu
        Param j,contextmenu
        Local funzione, cFunzione, loMenuItem, loZoomHeaderMenu, k
        cFunzione = Sys(2015)
        Public &cFunzione
        &cFunzione = ""
        funzione=''
        k=0
        * --- Menu per gestione ordinamenti
        If g_Omenu.oCtrl.Parent.Parent.Parent.bQueryOnDblClick
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate('Ordinamento')
                loMenuItem.BeginGroup = .F.
                loMenuItem.Visible = .T.
            Else
                Define Bar (j) Of contextmenu Prompt cp_Translate('Ordinamento') Picture g_Omenu.RetBmpForMenu('')         &&Definisco il sottomen� "Ordinamento"
                On Bar j Of contextmenu Activate Popup Ordinamento
                Define Popup Ordinamento shortcut Margin
                j=j+1
            Endif

            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Ordina crescente')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_AddOrderZoom with g_oMenu.oCtrl"
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Ordina crescente'))
                On Sele Bar (k) Of Ordinamento Do cp_AddOrderZoom With g_Omenu.oCtrl
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Ordina decrescente')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_AddOrderZoom with g_oMenu.oCtrl, .t."
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Ordina decrescente'))
                On Sele Bar (k) Of Ordinamento Do cp_AddOrderZoom With g_Omenu.oCtrl, .T.
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Ordina come primo crescente')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_AddFirstOrderZoom with g_oMenu.oCtrl"
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Ordina come primo crescente'))
                On Sele Bar (k) Of Ordinamento Do cp_AddFirstOrderZoom With g_Omenu.oCtrl
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Ordina come primo decrescente')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_AddFirstOrderZoom with g_oMenu.oCtrl,.t."
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Ordina come primo decrescente'))
                On Sele Bar (k) Of Ordinamento Do cp_AddFirstOrderZoom With g_Omenu.oCtrl,.T.
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Inverti ordinamento')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_InvertOrderZoom with g_oMenu.oCtrl"
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Inverti ordinamento'))
                On Sele Bar (k) Of Ordinamento Do cp_InvertOrderZoom With g_Omenu.oCtrl
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate("Annulla l'ordinamento")
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_BlankOrderZoom with g_oMenu.oCtrl.parent"
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate("Annulla l'ordinamento"))
                On Sele Bar (k) Of Ordinamento Do cp_BlankOrderZoom With g_Omenu.oCtrl.Parent
            Endif
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate("Ripristina l'ordinamento di default")
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_BlankAllOrderZoom with g_oMenu.oCtrl.Parent, .t."
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate("Ripristina l'ordinamento di default"))
                On Sele Bar (k) Of Ordinamento Do cp_BlankAllOrderZoom With g_Omenu.oCtrl.Parent, .T.
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate("Annulla tutti gli ordinamenti")
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_BlankAllOrderZoom with g_oMenu.oCtrl.parent"
            Else
                cStyle = Iif(TIPO = 'F', 'I', 'N')
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate("Annulla tutti gli ordinamenti"))
                On Sele Bar (k) Of Ordinamento Do cp_BlankAllOrderZoom With g_Omenu.oCtrl.Parent
            Endif
        Endif
        * ---

        * --- menu per gestione filtri
        If g_Omenu.oCtrl.Parent.Parent.Parent.bOptions
            k=0
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate('Filtro')
                loMenuItem.BeginGroup = .F.
                loMenuItem.Visible = g_Omenu.oCtrl.Parent.Parent.Parent.bOptions
            Else
                Define Bar (j) Of contextmenu Prompt cp_Translate('Filtro') Picture g_Omenu.RetBmpForMenu('')         &&Definisco il sottomen� "Filtro"
                On Bar j Of contextmenu Activate Popup Filtro
                Define Popup Filtro shortcut Margin
                j=j+1
            Endif
            If (g_Omenu.oCtrl.Parent.Parent.Parent.bAdvancedHeaderZoom Or i_AdvancedHeaderZoom) And i_cZoomMode='S' And g_Omenu.oCtrl.Parent.Parent.Parent.bFilterOnClick
                k=k+1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Personalizza')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_showsearchfilterform with g_oMenu.oCtrl.parent"
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Valore pieno')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_FilterEmpty with g_oMenu.oCtrl.parent,.f."
            Else
                Define Bar (k) Of Filtro Prompt Alltrim(cp_Translate('Valore pieno'))
                On Sele Bar (k) Of Filtro Do cp_FilterEmpty With g_Omenu.oCtrl.Parent,.F.
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Valore vuoto')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_FilterEmpty with g_oMenu.oCtrl.parent,.t."
            Else
                Define Bar (k) Of Filtro Prompt Alltrim(cp_Translate('Valore vuoto'))
                On Sele Bar (k) Of Filtro Do cp_FilterEmpty With g_Omenu.oCtrl.Parent,.T.
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Annulla filtro')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_blankWhereZoom with g_oMenu.oCtrl.parent"
            Else
                Define Bar (k) Of Filtro Prompt Alltrim(cp_Translate('Annulla filtro'))
                On Sele Bar (k) Of Filtro Do cp_blankWhereZoom With g_Omenu.oCtrl.Parent
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Annulla tutti i filtri aggiunti')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_blankAllWhereZoom with g_oMenu.oCtrl.parent, .t."
            Else
                Define Bar (k) Of Filtro Prompt Alltrim(cp_Translate('Annulla tutti i filtri aggiunti'))
                On Sele Bar (k) Of Filtro Do cp_blankAllWhereZoom With g_Omenu.oCtrl.Parent, .T.
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Annulla tutti i filtri')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_blankAllWhereZoom with g_oMenu.oCtrl.parent"
            Else
                Define Bar (k) Of Filtro Prompt Alltrim(cp_Translate('Annulla tutti i filtri'))
                On Sele Bar (k) Of Filtro Do cp_blankAllWhereZoom With g_Omenu.oCtrl.Parent
            Endif
        Endif
        * ---

        * --- menu per gestione colonne
        If g_Omenu.oCtrl.Parent.Parent.Parent.bOptions And g_Omenu.oCtrl.Parent.Parent.Parent.adv.bSec1
            k=0
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate('Gestione colonne')
                loMenuItem.BeginGroup = .F.
                loMenuItem.Visible = g_Omenu.oCtrl.Parent.Parent.Parent.bOptions
            Else
                Define Bar (j) Of contextmenu Prompt cp_Translate('Gestione colonne') Picture g_Omenu.RetBmpForMenu('')         &&Definisco il sottomen� "Filtro"
                On Bar j Of contextmenu Activate Popup Filtro
                Define Popup Filtro shortcut Margin
                j=j+1
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Elimina colonna')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_DeleteColumnZoom with g_oMenu.oCtrl.parent,.f."
            Else
                Define Bar (k) Of Filtro Prompt Alltrim(cp_Translate('Elimina colonna'))
                On Sele Bar (k) Of Filtro Do cp_DeleteColumnZoom With g_Omenu.oCtrl.Parent,.F.
            Endif
        Endif
        * ---

        If Not g_Omenu.oCtrl.Parent.Parent.Parent.bNoMenuHeaderProperty
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_PROPERTY)
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.

                loMenuItem.OnClick = "do cp_askcolumntitleproc with g_oMenu.oCtrl"
                l_BeginGroup = .F.
            Else
                * --- Separatore
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
                *--- Non serve aggiungere il separatore perch� viene aggiunto
                *--- in precedenza dalla funzione creaMenu
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PROPERTY)
                On Sele Bar (j) Of contextmenu Do cp_askcolumntitleproc With g_Omenu.oCtrl
            Endif
        Endif

        If i_VisualTheme<>-1
            * --- Disattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('D')
            m.j.InitMenu(.T.)
            m.j.ShowMenu()
            * --- Riattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('A')
            funzione = &cFunzione
        Else
            If Cntbar('contextmenu')>0
                * --- Disattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('D')
                Activate Popup contextmenu
                Deactivate Popup contextmenu
                Release Popups contextmenu
                * --- Riattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('A')
            Endif
        Endif
        Release &cFunzione
    Endfunc

    * --- Crea il menu DetailHeader
    Function DetailHeaderMenu
        Param j,contextmenu
        Local funzione, cFunzione, loMenuItem, loZoomHeaderMenu, k
        cFunzione = Sys(2015)
        Public &cFunzione
        &cFunzione = ""
        funzione=''

        * --- menu per gestione filtri
        k=0
        If i_VisualTheme <> -1
            loMenuItem = m.j.AddMenuItem()
            loMenuItem.Caption = cp_Translate('Filtro')
            loMenuItem.BeginGroup = .F.
            loMenuItem.Visible = g_Omenu.oCtrl.Parent.Parent.Parent.bOptions
        Else
            Define Bar (j) Of contextmenu Prompt cp_Translate('Filtro') Picture g_Omenu.RetBmpForMenu('')         &&Definisco il sottomen� "Filtro"
            On Bar j Of contextmenu Activate Popup Filtro
            Define Popup Filtro shortcut Margin
            j=j+1
        Endif

        If i_VisualTheme <> -1
            loZoomHeaderMenu = loMenuItem.AddMenuItem()
            loZoomHeaderMenu.Caption = cp_Translate('Personalizza')
            loZoomHeaderMenu.Visible = .T.
            loZoomHeaderMenu.OnClick = "do cp_DetailAddFilter with g_oMenu.oCtrl"
        Else
            Define Bar (k) Of Filtro Prompt Alltrim(cp_Translate('Personalizza'))
            On Sele Bar (k) Of Filtro Do cp_DetailAddFilter With g_Omenu.oCtrl
            k=k+1
        Endif
        If i_VisualTheme <> -1
            loZoomHeaderMenu = loMenuItem.AddMenuItem()
            loZoomHeaderMenu.Caption = cp_Translate('Annulla filtro')
            loZoomHeaderMenu.Visible = .T.
            loZoomHeaderMenu.OnClick = "do cp_DetailRemoveFilter with g_oMenu.oCtrl.parent"
        Else
            Define Bar (k) Of Filtro Prompt Alltrim(cp_Translate('Annulla filtro'))
            On Sele Bar (k) Of Filtro Do cp_DetailRemoveFilter With g_Omenu.oCtrl.Parent
            k=k+1
        Endif
        If i_VisualTheme <> -1
            loZoomHeaderMenu = loMenuItem.AddMenuItem()
            loZoomHeaderMenu.Caption = cp_Translate("Vai all'inizio")
            loZoomHeaderMenu.Visible = .T.
            loZoomHeaderMenu.OnClick = "do cp_GoPosition with g_oMenu.oCtrl.parent, 'TOP'"
        Else
            Define Bar (j) Of Filtro Prompt Alltrim(cp_Translate("Vai all'inizio"))
            On Sele Bar (j) Of Filtro Do cp_GoPosition With g_Omenu.oCtrl.Parent, 'TOP'
            k=k+1
        Endif
        If i_VisualTheme <> -1
            loZoomHeaderMenu = loMenuItem.AddMenuItem()
            loZoomHeaderMenu.Caption = cp_Translate("Vai alla fine")
            loZoomHeaderMenu.Visible = .T.
            loZoomHeaderMenu.OnClick = "do cp_GoPosition with g_oMenu.oCtrl.parent, 'BOTTOM'"
        Else
            Define Bar (j) Of Filtro Prompt Alltrim(cp_Translate("Vai alla fine"))
            On Sele Bar (j) Of Filtro Do cp_GoPosition With g_Omenu.oCtrl.Parent, 'BOTTOM'
            k=k+1
        Endif
        * --- menu per gestione ordinamento
        If Upper(g_Omenu.oCtrl.Parent.Parent.Parent.Parent.Parent.cFunction)="QUERY" And Type("g_Omenu.oCtrl.Parent.Parent.Parent.Parent.Parent.cOrderByDett")="C" And g_Omenu.oCtrl.bIsField And g_Omenu.oCtrl.Parent.Parent.Parent.Parent.Parent.bApplyOrderDetailCustom
            k=0
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate('Ordinamento')
                loMenuItem.BeginGroup = .F.
                loMenuItem.Visible = .T.
            Else
                Define Bar (j) Of contextmenu Prompt cp_Translate('Ordinamento') Picture g_Omenu.RetBmpForMenu('')         &&Definisco il sottomen� "Ordinamento"
                On Bar j Of contextmenu Activate Popup Ordinamento
                Define Popup Ordinamento shortcut Margin
                j=j+1
            Endif

            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Ordina crescente')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_AddOrderDetail with g_oMenu.oCtrl"
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Ordina crescente'))
                On Sele Bar (k) Of Ordinamento Do cp_AddOrderDetail With g_Omenu.oCtrl
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Ordina decrescente')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_AddOrderDetail with g_oMenu.oCtrl, .t."
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Ordina decrescente'))
                On Sele Bar (k) Of Ordinamento Do cp_AddOrderDetail With g_Omenu.oCtrl, .T.
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Ordina come primo crescente')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_AddFirstOrderDetail with g_oMenu.oCtrl"
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Ordina come primo crescente'))
                On Sele Bar (k) Of Ordinamento Do cp_AddFirstOrderDetail With g_Omenu.oCtrl
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Ordina come primo decrescente')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_AddFirstOrderDetail with g_oMenu.oCtrl,.t."
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Ordina come primo decrescente'))
                On Sele Bar (k) Of Ordinamento Do cp_AddFirstOrderDetail With g_Omenu.oCtrl,.T.
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate('Inverti ordinamento')
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_InvertOrderDetail with g_oMenu.oCtrl"
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate('Inverti ordinamento'))
                On Sele Bar (k) Of Ordinamento Do cp_InvertOrderDetail With g_Omenu.oCtrl
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate("Annulla l'ordinamento")
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_BlankOrderDetail with g_oMenu.oCtrl"
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate("Annulla l'ordinamento"))
                On Sele Bar (k) Of Ordinamento Do cp_BlankOrderDetail With g_Omenu.oCtrl
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate("Ripristina l'ordinamento di default")
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_DefaultOrderDetail with g_oMenu.oCtrl.Parent"
            Else
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate("Ripristina l'ordinamento di default"))
                On Sele Bar (k) Of Ordinamento Do cp_DefaultOrderDetail With g_Omenu.oCtrl.Parent
            Endif
            k=k+1
            If i_VisualTheme <> -1
                loZoomHeaderMenu = loMenuItem.AddMenuItem()
                loZoomHeaderMenu.Caption = cp_Translate("Annulla tutti gli ordinamenti")
                loZoomHeaderMenu.Visible = .T.
                loZoomHeaderMenu.OnClick = "do cp_BlankAllOrderDetail with g_oMenu.oCtrl.parent"
            Else
                cStyle = Iif(TIPO = 'F', 'I', 'N')
                Define Bar (k) Of Ordinamento Prompt Alltrim(cp_Translate("Annulla tutti gli ordinamenti"))
                On Sele Bar (k) Of Ordinamento Do cp_BlankAllOrderDetail With g_Omenu.oCtrl.Parent
            Endif
        Endif
        * ---

        If i_VisualTheme<>-1
            * --- Disattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('D')
            m.j.InitMenu(.T.)
            m.j.ShowMenu()
            * --- Riattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('A')
            funzione = &cFunzione
        Else
            If Cntbar('contextmenu')>0
                * --- Disattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('D')
                Activate Popup contextmenu
                Deactivate Popup contextmenu
                Release Popups contextmenu
                * --- Riattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('A')
            Endif
        Endif
        Release &cFunzione
    Endfunc

    * --- Crea il menu contestuale per gli oggetti FoxCharts
    Function FoxChartsMenu
        Param j,contextmenu
        Local funzione, cFunzione
        cFunzione = Sys(2015)
        Public &cFunzione
        &cFunzione = ""
        funzione=''


        Local l_BeginGroup
        l_BeginGroup = .T.

        If Not(This.oCtrl.bDesignMode) And Not(This.oCtrl.bReadOnly)
            && Menu edit Graph
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_EDIT_FOXCHARTS )
                loMenuItem.Picture="ModiftblT.ico.ico"
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.

                loMenuItem.OnClick = cFunzione+[ = 'E']
            Else
                #If Version(5)>600
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_EDIT_FOXCHARTS )Picture g_Omenu.RetBmpForMenu("ModiftblT.ico.bmp")
                #Else
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_EDIT_FOXCHARTS )
                #Endif
                On Sele Bar (j) Of contextmenu funzione='E'
                j=j+1
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
            Endif
        Endif

        && Menu Copy
        If i_VisualTheme <> -1
            loMenuItem = m.j.AddMenuItem()
            loMenuItem.Caption = cp_Translate(MSG_COPY)
            loMenuItem.Picture="copy.ico"
            loMenuItem.BeginGroup = .F.
            loMenuItem.Visible = .T.

            loMenuItem.OnClick = cFunzione+[ = 'C']
            l_BeginGroup = .F.
        Else
            #If Version(5)>600
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_COPY)Picture g_Omenu.RetBmpForMenu("copy.bmp")
            #Else
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_COPY)
            #Endif
            On Sele Bar (j) Of contextmenu funzione='C'
            j=j+1
        Endif
        && Menu Save Report
        If i_VisualTheme <> -1
            loMenuItem = m.j.AddMenuItem()
            loMenuItem.Caption = cp_Translate(MSG_PRINT)
            loMenuItem.Picture="print.ico"
            loMenuItem.BeginGroup = .T.
            loMenuItem.Visible = .T.

            loMenuItem.OnClick = cFunzione+[ = 'P']
            l_BeginGroup = .F.
        Else
            #If Version(5)>600
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PRINT)Picture g_Omenu.RetBmpForMenu("print.bmp")
            #Else
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PRINT)
            #Endif
            On Sele Bar (j) Of contextmenu funzione='P'
            j=j+1
        Endif
        && Menu Save Image
        If i_VisualTheme <> -1
            loMenuItem = m.j.AddMenuItem()
            loMenuItem.Caption = cp_Translate(MSG_SAVE_AS)
            loMenuItem.Picture="saveT.ico"
            loMenuItem.BeginGroup = .T.
            loMenuItem.Visible = .T.

            loMenuItem.OnClick = cFunzione+[ = 'S']
            l_BeginGroup = .F.
        Else
            #If Version(5)>600
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SAVE_AS)Picture g_Omenu.RetBmpForMenu("saveT.bmp")
            #Else
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SAVE_AS)
            #Endif
            On Sele Bar (j) Of contextmenu funzione='S'
            j=j+1
        Endif



        If i_VisualTheme<>-1
            * --- Disattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('D')
            m.j.InitMenu(.T.)
            m.j.ShowMenu()
            * --- Riattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('A')
            funzione = &cFunzione
        Else
            If Cntbar('contextmenu')>0
                * --- Disattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('D')
                Activate Popup contextmenu
                Deactivate Popup contextmenu
                Release Popups contextmenu
                * --- Riattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('A')
            Endif
        Endif
        This.cBatchType = funzione
        Do Case
            Case Left(funzione,1)='E'
                If Used(This.oCtrl.oChart.SourceAlias)
                    Do vfcbuild With This.oCtrl.oChart.SourceAlias, This.oCtrl.cFileCfg
                Else
                    Do vfcbuild With "", This.oCtrl.cFileCfg
                Endif
            Case Left(funzione,1)='C'
                This.oCtrl.oChart.oBmp.ToClipboard()
            Case Left(funzione,1)='P'
                This.oCtrl.PrintFoxCharts()
            Case Left(funzione,1)='S'
                Local lcImgFormatTypes, lcFile, i_cCurrDir
                lcImgFormatTypes = "Bmp;Jpg;Gif;Png;Tif;Emf"
                i_cCurrDir=Sys(5)+Sys(2003)
                lcFile = Putfile("Save Image as...", "", lcImgFormatTypes)
                Cd (i_cCurrDir)
                If Empty(lcFile) && Invalid File Name
                    Return .F.
                Endif
                This.oCtrl.oChart.SaveToFile(m.lcFile, 100)
                If cp_fileexist(m.lcFile)
                    This.oCtrl.ShellEx(m.lcFile)
                Endif
        Endcase
        Release &cFunzione
    Endfunc
    * --- Zucchetti Aulla Fine

    ** Crea il menu contestuale per gli oggetti che hanno il control
    Function controlMenu
        Param j,contextmenu
        ** Abilitazione disabilitazione funzioni
        Local lEntity,obj,k,Batch,err,objname,cStyle,cBmpFile,m
        Public FLGT,FLGC,FLGI,FLGS,FLGM
        Local funzione, cFunzione
        cFunzione = Sys(2015)
        Public &cFunzione
        &cFunzione = ""
        funzione=''
        FLGT=.F. &&Taglia
        FLGC=.F. &&Copia
        FLGI=.F. &&Incolla
        FLGS=.F. &&Seleziona tutto
        FLGM=.F. &&Frasi modello

        *--- RightClick
        If Inlist(Upper(This.oCtrl.Class),"STDFIELD", "STDMEMO","STDTRSFIELD", "STDTRSMEMO")
            If Empty(_Cliptext) &&AND (NOT (this.oCtrl.enabled) OR this.oCtrl.readOnly)
                FLGI=.T.
            Endif
            If Empty(This.oCtrl.SelText)
                FLGC=.T.
                FLGT=.T.
            Endif
            If Not(This.oCtrl.Enabled)
                FLGI=.T.
                FLGT=.T.
                FLGM=.T.
            Endif
            If This.oCtrl.Enabled And This.oCtrl.ReadOnly
                FLGI=.T.
                FLGT=.T.
                FLGM=.T.
            Endif
            If Empty(This.oCtrl.Value)
                FLGS = .T.
            Endif
        Else
            FLGT=.T. &&Taglia
            FLGC=.T. &&Copia
            FLGI=.T. &&Incolla
            FLGS=.T. &&Seleziona tutto
            FLGM=.T. &&Frasi modello
        Endif
        **Se il control ha associato uno zoom, inserisco nel tasto destro anche la voce elenco che mi permette
        ** di visualizzare lo zoom. Inoltre, la voce, viene visualizzata solamente quando siamo in fase di
        **modifica o caricamento e il campo � in stato di editabile.
        **Qualora la variabile cLinkFile associata al campo sia vuota, visualizzo come voce di menu "(F9)"
        If This.oCtrl.bhasZoom And (Upper(This.oContained.cFunction)==Upper("Edit") Or Upper(This.oContained.cFunction)==Upper("Load") ) And This.oCtrl.Enabled
            If i_VisualTheme<>-1
                Local loMenuItem
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = Iif(Empty(This.cLinkFile),"(F9)", cp_Translate(MSG_LIST_F9))
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = cFunzione+[ = 'E']
            Else
                Define Bar (j) Of contextmenu Prompt Iif(Empty(This.cLinkFile),"(F9)", cp_Translate(MSG_LIST_F9))
                On Selection Bar (j) Of contextmenu funzione = 'E'
                j=j+1
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
            Endif
        Endif
        lEntity = This.cEntity
        If Not Empty(This.cZoomOnZoom)And Type('g_Omenu.octrl.bNoMenuAction')='L'And Not g_Omenu.oCtrl.bNoMenuAction
            Do Case
                    *  --- Zucchetti Aulla Inizio ( OR This.cEntity=='Routine')- Batch Zoom On Zoom Gestiti tramite propriet� cBatchType
                Case This.cEntity=='Master File' Or This.cEntity=='Master/Detail' Or This.cEntity=='Detail File' Or This.cEntity=='Routine'
                    If i_VisualTheme <>-1
                        loMenuItem = m.j.AddMenuItem()
                        loMenuItem.Caption = cp_Translate(MSG_OPEN)
                        loMenuItem.SkipFor = "Empty(g_Omenu.oCtrl.Value)"
                        loMenuItem.BeginGroup = .T.
                        loMenuItem.Visible = .T.
                        loMenuItem.OnClick = cFunzione+[ = 'A']

                        loMenuItem = m.j.AddMenuItem()
                        loMenuItem.Caption = cp_Translate(MSG_CHANGE)
                        loMenuItem.SkipFor = "Empty(g_Omenu.oCtrl.Value)"
                        loMenuItem.Visible = .T.
                        loMenuItem.OnClick = cFunzione+[ = 'M']

                        loMenuItem = m.j.AddMenuItem()
                        loMenuItem.Caption = cp_Translate(MSG_LOAD)
                        loMenuItem.SkipFor = ""
                        loMenuItem.Visible = .T.
                        loMenuItem.OnClick = cFunzione+[ = 'L']
                    Else
                        Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_OPEN) Skip For Empty(This.oCtrl.Value)
                        On Sele Bar (j) Of contextmenu funzione = 'A'
                        j=j+1
                        Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_CHANGE) Skip For Empty(This.oCtrl.Value)
                        On Sele Bar (j) Of contextmenu funzione='M'
                        j=j+1
                        Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_LOAD)
                        On Sele Bar (j) Of contextmenu funzione='L'
                        j=j+1
                    Endif
                    *  --- Zucchetti Aulla Inizio ( OR This.cEntity=='Routine')- Batch Zoom On Zoom Gestiti tramite propriet� cBatchType
                Case This.cEntity=='Dialog Window'
                    If i_VisualTheme <>-1
                        loMenuItem = m.j.AddMenuItem()
                        loMenuItem.Caption = cp_Translate(MSG_OPEN)
                        loMenuItem.SkipFor = "Empty(g_Omenu.oCtrl.Value)"
                        loMenuItem.Visible = .T.
                        loMenuItem.OnClick = cFunzione+[ = 'A']
                    Else
                        Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_OPEN) Skip For Empty(This.oCtrl.Value)
                        On Sele Bar (j) Of contextmenu funzione='A'
                        j=j+1
                    Endif
                Otherwise
                    Return
            Endcase
            If i_VisualTheme =-1
                * --- Separatore
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
            Endif
        Endif
        * Zucchetti Aulla - Inizio - Visualizzazione query InfoPublisher su gestione
        If Type('g_IRDR') = 'C' And g_IRDR = 'S'
            * --- sicuramente this.oContained esiste: ricerchiamo quindi la propriet� cCursorInfo
            obj=This.oContained
            Do While Not Isnull(obj) And Type('obj.cCurInfo')<>'C'
                * --- se non essite, siamo in un figlio integrato ( e cos� a salire...)
                obj=obj.oParentObject
            Enddo
            If Not Isnull(obj)
                * --- Lancio il batch che valorizzer� il cursore delle query
                If !obj.bCurForm Or ! Used(obj.cCurInfo)
                    I_sPrg="GSIR_BIC"
                    Do (I_sPrg) With obj, obj.GetSecurityCode()
                    obj.bCurForm = .F.
                Endif
                If Used(obj.cCurInfo)  &&Cerco nome tabella
                    Select (obj.cCurInfo)
                    If Reccount() = 0 And Not Empty(This.cLinkFile) And Not Empty(This.oKey(1,3))
                        Select (obj.cCurInfo)
                        Use
                        I_sPrg="GSIR_BIC"
                        * ---- Trasformo i valori dei campi chiave in stringhe
                        Do (I_sPrg) With obj, This.cLinkFile+','+Alltrim(Transform(This.oKey(1,3)))
                        obj.bCurForm = .F.
                    Endif
                    Select (obj.cCurInfo)
                    If Reccount() > 0
                        * --- Creo menu PopUp
                        Public oObjContext,lZoom
                        oObjContext=obj
                        lZoom=.F.
                        j=CreateMenuInfoPublisher(obj,j,contextmenu)
                        obj=.Null.
                        If i_VisualTheme =-1
                            * --- Separatore
                            Define Bar (j) Of contextmenu Prompt "\-"
                            j=j+1
                        Endif
                    Endif
                Endif
            Endif
        Endif
        * Zucchetti Aulla - Fine
        Local l_BeginGroup
        l_BeginGroup = .T.
        If Type('g_Omenu.octrl.bNoMenuCut')='L'And Not g_Omenu.oCtrl.bNoMenuCut
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_CUT)
                loMenuItem.Picture="cut.ico"
                loMenuItem.SkipFor = "FLGT"
                loMenuItem.BeginGroup = l_BeginGroup
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = cFunzione+[ = 'T']
                l_BeginGroup = .F.
            Else
                #If Version(5)>600
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_CUT) Picture g_Omenu.RetBmpForMenu("cut.bmp") Skip For FLGT
                #Else
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_CUT) Skip For FLGT
                #Endif
                On Sele Bar (j) Of contextmenu funzione='T'
                j=j+1
            Endif
        Endif
        If Type('g_Omenu.octrl.bNoMenuCopy')='L'And Not g_Omenu.oCtrl.bNoMenuCopy
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_COPY)
                loMenuItem.Picture="copy.ico"
                loMenuItem.SkipFor = "FLGC"
                loMenuItem.BeginGroup = l_BeginGroup
                loMenuItem.Visible = .T.

                loMenuItem.OnClick = cFunzione+[ = 'C']
                l_BeginGroup = .F.
            Else
                #If Version(5)>600
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_COPY) Picture g_Omenu.RetBmpForMenu("copy.bmp")  Skip For FLGC
                #Else
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_COPY) Skip For FLGC
                #Endif
                On Sele Bar (j) Of contextmenu funzione='C'
                j=j+1
            Endif
        Endif
        If Type('g_Omenu.octrl.bNoMenuPaste')='L'And Not g_Omenu.oCtrl.bNoMenuPaste
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_PASTE)
                loMenuItem.Picture="paste.ico"
                loMenuItem.SkipFor = "FLGI"
                loMenuItem.BeginGroup = l_BeginGroup
                loMenuItem.Visible = .T.

                loMenuItem.OnClick = cFunzione+[ = 'I']
                l_BeginGroup = .F.
            Else
                #If Version(5)>600
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PASTE) Picture g_Omenu.RetBmpForMenu("paste.bmp") Skip For FLGI
                #Else
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PASTE) Skip For FLGI
                #Endif
                On Sele Bar (j) Of contextmenu funzione='I'
                j=j+1
            Endif
        Endif
        If Type('g_Omenu.octrl.bNoMenuSelAll')='L'And Not g_Omenu.oCtrl.bNoMenuSelAll
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_SELECT_ALL_TEXT)
                loMenuItem.SkipFor = "FLGS"
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.

                loMenuItem.OnClick = cFunzione+[ = 'S']
                l_BeginGroup = .F.
            Else
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SELECT_ALL_TEXT) Skip For FLGS
                On Sele Bar (j) Of contextmenu funzione='S'
                j=j+1
            Endif
        Endif
        * Zucchetti Aulla - Inizio - Gestione controllo flussi
        If Type('g_CTFL') = 'C' And g_CTFL = 'S'
            If i_VisualTheme <>-1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_VIEW_FLOW)
                loMenuItem.SkipFor = ""
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = cFunzione+[ = 'F']
            Else
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_VIEW_FLOW)
                On Sele Bar (j) Of contextmenu funzione='F'
            Endif
        Endif
        * Zucchetti Aulla - Fine - Gestione controllo flussi
        If Type('g_Omenu.octrl.bNoMenuProperty')='L'And Not g_Omenu.oCtrl.bNoMenuProperty
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_PROPERTY)
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.

                loMenuItem.OnClick = cFunzione+[ = 'P']
                l_BeginGroup = .F.
            Else
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PROPERTY)
                On Sele Bar (j) Of contextmenu funzione='P'
            Endif
        Endif
        If i_VisualTheme<>-1
            * --- Disattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('D')
            m.j.InitMenu(.T.)
            m.j.ShowMenu()
            * --- Riattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('A')
            funzione = &cFunzione
        Else
            If Cntbar('contextmenu')>0
                * --- Disattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('D')
                Activate Popup contextmenu
                Deactivate Popup contextmenu
                Release Popups contextmenu
                * --- Riattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('A')
            Endif
        Endif
        This.cBatchType = funzione
        Do Case
            Case Left(funzione,1)='E'
                objname = This.oCtrl.Name
                This.oCtrl.Parent.oContained.bDontReportError=.T.
                Public i_lastindirectaction
                i_lastindirectaction='ECPZOOM'
                objname = "this.oCtrl.Parent."+objname+".mzoom()"
                &objname
            Case Left(funzione,1)='A'
                contextm(This,funzione,.T.)  &&Apri
            Case Left(funzione,1)='M'
                contextm(This,funzione,.T.)  &&Modifica
            Case Left(funzione,1)='L'
                contextm(This,funzione,.T.) &&Load
            Case Left(funzione,1)='T'
                _Cliptext=This.oCtrl.SelText  &&Taglia
                This.oCtrl.SelText=''
            Case Left(funzione,1)='C'
                _Cliptext=This.oCtrl.SelText  &&Copia
            Case Left(funzione,1)='I'
                This.oCtrl.SelText=_Cliptext  &&Incolla
            Case Left(funzione,1)='S'               &&Seleziona tutto
                This.oCtrl.SelStart=0
                If Type('this.oCtrl.value')='C'
                    This.oCtrl.SelLength=Len(This.oCtrl.Value)
                Endif
                If Type('this.oCtrl.value')='D'
                    This.oCtrl.SelLength=Len(Dtoc(This.oCtrl.Value))
                Endif
                If Type('this.oCtrl.value')='N'
                    This.oCtrl.SelLength=Len(Str(This.oCtrl.Value))
                Endif
                If Type('this.oCtrl.value')='T'
                    This.oCtrl.SelLength=Len(Ttoc(This.oCtrl.Value))
                Endif
            Case Left(funzione,1)='P'
                Cp_InfoCtrl(This)
                * Zucchetti Aulla - Inizio - Gestione controllo flussi
            Case Left(funzione,1)='F'
                Local actctrl, actform, fldname, fldnameform, tipsel
                actctrl = This.oCtrl
                actform = actctrl.formflus()
                If Vartype(actform)="O"
                    fldname = Iif(Pemstatus(actctrl,"cqueryname",5), actctrl.cqueryname, "")
                    tipsel = "T"
                    If Empty(fldname) Or "," $ fldname
                        If "," $ fldname
                            fldname=Substr(fldname, Rat(",", fldname)+1)
                        Endif
                        If Pemstatus(actctrl,"cformvar",5)
                            fldnameform=actctrl.cFormVar
                            If fldnameform="w_"
                                fldnameform=Substr(fldnameform, 3)
                                If Empty(fldname)
                                    fldname=fldnameform
                                    tipsel = "R"
                                Else
                                    If fldname<>fldnameform
                                        fldname=fldnameform
                                    Endif
                                Endif
                            Endif
                        Endif
                    Endif
                    I_sPrg="GSCF_BCF"
                    Do (I_sPrg) With actform , "VW", fldname, tipsel
                Endif
                * Zucchetti Aulla - Fine - Gestione controllo flussi
        Endcase
        Release FLGT
        Release FLGC
        Release FLGI
        Release FLGS
        Release &cFunzione
        * Zucchetti Aulla - Inizio - Rilascio oggetti (frasi modello / inforeadee)
        Release FLGM
        If Vartype(g_IRDR) = 'C' And g_IRDR = 'S'
            Release oObjContext
            Release lZoom
        Endif
        * Zucchetti Aulla - Fine
    Endfunc

    Function RetBmpForMenu(cFile)
        If Vartype(i_ThemesManager) = 'O'
            Return i_ThemesManager.RetBmpFromIco(m.cFile, 16)
        Else
            Return m.cFile
        Endif
    Endfunc

    **Crea il menu contestuale per la gestione (speculare alla toolbar). In pi� aggiungiamo la voce Refresh abilitata
    ** solo quando � disabilitata la funzione salva.
    Function gestioneMenu
        Param j,contextmenu
        Local obj,k,cStyle,cBmpFile,m,T
        Local funzione, cFunzione
        cFunzione = Sys(2015)
        Public &cFunzione
        &cFunzione = ""
        funzione =''
        *Abilito Menu Azioni
        If Type('g_Omenu.oParentobject.bNoMenuAction')='L'And Not g_Omenu.oParentObject.bNoMenuAction
            If i_VisualTheme<>-1
                Local loMenuItemCfg
                loMenuItemCfg = m.j.AddMenuItem()
                loMenuItemCfg.Caption = cp_Translate(MSG_ACTION)
                loMenuItemCfg.BeginGroup = .T.
                loMenuItemCfg.Visible = .T.
            Else
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_ACTION)
                Define Popup azioni Margin shortcut
                On Bar (j) Of contextmenu Activate Popup azioni
                j=j+1
                T=1
            Endif
            * --- Refresh
            Local loMenuItem
            If i_VisualTheme<>-1
                loMenuItem = m.loMenuItemCfg.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_REFRESH_F11)
                loMenuItem.Picture="new_interrog.ico"
                loMenuItem.OnClick = [Do cp_DoAction With 'ecpRefresh']
                loMenuItem.Visible = .T.
            Else
                #If Version(5)>600
                    Define Bar (T) Of azioni Prompt cp_Translate(MSG_REFRESH_F11) Picture g_Omenu.RetBmpForMenu("new_interrog.bmp")
                #Else
                    Define Bar (T) Of azioni Prompt cp_Translate(MSG_REFRESH_F11)
                #Endif
                On Selection Bar T Of azioni Do cp_DoAction With 'ecpRefresh'
                T=T+1
            Endif
            * --- Modifica
            If oCPToolbar.b2.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b2.ToolTipText
                    loMenuItem.Picture="modiftblT.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpEdit']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b2.ToolTipText Picture g_Omenu.RetBmpForMenu("modiftblT.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b2.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpEdit'
                    T=T+1
                Endif
            Endif
            * --- Carica
            If oCPToolbar.b3.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b3.ToolTipText
                    loMenuItem.Picture="addtablT.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpLoad']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b3.ToolTipText Picture g_Omenu.RetBmpForMenu("addtablT.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b3.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpLoad'
                    T=T+1
                Endif
            Endif
            * --- Salva
            If oCPToolbar.b4.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b4.ToolTipText
                    loMenuItem.Picture="SaveT.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpSave']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b4.ToolTipText Picture g_Omenu.RetBmpForMenu("SaveT.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b4.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpSave'
                    T=T+1
                Endif
            Endif
            * --- Cancella
            If oCPToolbar.b5.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b5.ToolTipText
                    loMenuItem.Picture="cancT.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpDelete']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b5.ToolTipText Picture g_Omenu.RetBmpForMenu("cancT.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b5.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpDelete'
                    T=T+1
                Endif
            Endif
            * --- F6
            If oCPToolbar.b6.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b6.ToolTipText
                    loMenuItem.Picture="print.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpPrint']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b6.ToolTipText Picture g_Omenu.RetBmpForMenu("print.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b6.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpPrint'
                    T=T+1
                Endif
            Endif
            * --- Precedente
            If oCPToolbar.b7.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b7.ToolTipText
                    loMenuItem.Picture="priorT.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpPrior']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b7.ToolTipText Picture g_Omenu.RetBmpForMenu("priorT.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b7.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpPrior'
                    T=T+1
                Endif
            Endif
            * --- Seguente
            If oCPToolbar.b8.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b8.ToolTipText
                    loMenuItem.Picture="nextT.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpNext']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b8.ToolTipText Picture g_Omenu.RetBmpForMenu("nextT.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b8.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpNext'
                    T=T+1
                Endif
            Endif
            * --- Zoom
            If oCPToolbar.b9.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b9.ToolTipText
                    loMenuItem.Picture="zoomT.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpZoom']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b9.ToolTipText Picture g_Omenu.RetBmpForMenu("zoomT.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b9.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpZoom'
                    T=T+1
                Endif
            Endif
            * --- PgUp
            If oCPToolbar.b10.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b10.ToolTipText
                    loMenuItem.Picture="pgupT.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpPgUp']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b10.ToolTipText Picture g_Omenu.RetBmpForMenu("pgupT.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b10.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpPgUp'
                    T=T+1
                Endif
            Endif
            * --- PgDn
            If oCPToolbar.b11.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b11.ToolTipText
                    loMenuItem.Picture="pgdnT.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpPgDn']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b11.ToolTipText Picture g_Omenu.RetBmpForMenu("pgdnT.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b11.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpPgDn'
                    T=T+1
                Endif
            Endif
            * --- Filtro
            If oCPToolbar.b12.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b12.ToolTipText
                    loMenuItem.Picture="filtra.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpFilter']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b12.ToolTipText Picture g_Omenu.RetBmpForMenu("filtra.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b12.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpFilter'
                    T=T+1
                Endif
            Endif
            * --- Ripristina posizione
            If Type("oMemoryFormPosition")="O" And i_cConfSavePosForm<>'D' And Vartype(i_curform)='O' And i_curform.WindowState<>1 And !i_curform.MDIForm And ;
            	i_curform.Left<_Screen.Width And i_curform.Left+i_curform.Width>0 And i_curform.Top<_Screen.Height And i_curform.Top+i_curform.Height>0 AND i_cViewMode="S"
            	
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_RESTORE_POSITION)
                    loMenuItem.Picture="mask.ico"
                    loMenuItem.OnClick = "oMemoryFormPosition.RestorePosition(i_curform)"
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt cp_Translate(MSG_RESTORE_POSITION) Picture g_Omenu.RetBmpForMenu("mask.ico")
                    #Else
                        Define Bar (T) Of azioni Prompt cp_Translate(MSG_RESTORE_POSITION)
                    #Endif
                    On Selection Bar T Of azioni oMemoryFormPosition.RestorePosition(i_curform)
                    T=T+1
                Endif
            Endif
            * --- Salva posizione
            If Type("oMemoryFormPosition")="O" And i_cConfSavePosForm='N' And Vartype(i_curform)='O' And i_curform.WindowState<>1 And !i_curform.MDIForm And ;
            	i_curform.Left<_Screen.Width And i_curform.Left+i_curform.Width>0 And i_curform.Top<_Screen.Height And i_curform.Top+i_curform.Height>0  AND i_cViewMode="S"
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_SAVE_POSITION)
                    loMenuItem.Picture="anag.ico"
                    loMenuItem.OnClick = "oMemoryFormPosition.InsertPosition(i_curform)"
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt cp_Translate(MSG_SAVE_POSITION) Picture g_Omenu.RetBmpForMenu("anag.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt cp_Translate(MSG_SAVE_POSITION)
                    #Endif
                    On Selection Bar T Of azioni oMemoryFormPosition.InsertPosition(i_curform)
                    T=T+1
                Endif
            Endif
            * --- Esc
            If   oCPToolbar.b13.Enabled
                If i_VisualTheme<>-1
                    loMenuItem = m.loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = oCPToolbar.b13.ToolTipText
                    loMenuItem.Picture="esct.ico"
                    loMenuItem.OnClick = [Do cp_DoAction With 'ecpQuit']
                    loMenuItem.Visible = .T.
                Else
                    #If Version(5)>600
                        Define Bar (T) Of azioni Prompt oCPToolbar.b13.ToolTipText Picture g_Omenu.RetBmpForMenu("esct.bmp")
                    #Else
                        Define Bar (T) Of azioni Prompt oCPToolbar.b13.ToolTipText
                    #Endif
                    On Selection Bar T Of azioni Do cp_DoAction With 'ecpQuit'
                    T=T+1
                Endif
            Endif
        Endif
        * Zucchetti Aulla - Inizio - Visualizzazione query InfoPublisher su gestione
        If Type('g_IRDR') = 'C' And g_IRDR = 'S'
            * --- Inizializziamo obj con la pagina principale: non generer� errore...
            obj=This.oContained.oContained
            Do While Not Isnull(obj) And Type('obj.cCurInfo')<>'C'
                * --- siamo in un figlio integrato (e cerchiamo la prop cCursorInfo a salire...).
                obj=obj.oParentObject
            Enddo
            If Not Isnull(obj)
                * --- Lancio il batch che valorizzar� il cursore delle query
                If !obj.bCurForm Or ! Used(obj.cCurInfo)
                    I_sPrg="GSIR_BIC"
                    Do (I_sPrg) With obj, obj.GetSecurityCode()
                    obj.bCurForm = .F.
                Endif
                If Used(obj.cCurInfo)  &&Cerco nome tabella
                    Select (obj.cCurInfo)
                    If Reccount() = 0 And Not Empty(This.cLinkFile) And Not Empty(This.oKey(1,3))
                        Select (obj.cCurInfo)
                        Use
                        I_sPrg="GSIR_BIC"
                        Do (I_sPrg) With obj, This.cLinkFile+','+Alltrim(Transform(This.oKey(1,3)))
                        obj.bCurForm = .F.
                    Endif
                    Select (obj.cCurInfo)
                    If Reccount() > 0
                        If i_VisualTheme = -1
                            * --- Separatore
                            Define Bar (j) Of contextmenu Prompt "\-"
                            j=j+1
                        Endif
                        Public oObjContext,lZoom
                        oObjContext=obj
                        lZoom=.F.
                        * --- Creo menu PopUp
                        j=CreateMenuInfoPublisher(obj,j,contextmenu)
                        obj=.Null.
                    Endif
                Endif
            Endif
        Endif
        * Zucchetti Aulla - Fine

        If Type('g_Omenu.oParentobject.bNoMenuAction')='L'And Not g_Omenu.oParentObject.bNoMenuAction And i_VisualTheme = -1
            * --- Separatore
            Define Bar (j) Of contextmenu Prompt "\-"
            j=j+1
        Endif
        If Type('g_Omenu.oParentobject.bNoMenuAutorizaction')='L'And Not g_Omenu.oParentObject.bNoMenuAutorizaction
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_AUTORITY)
                loMenuItem.SkipFor = [!cp_IsAdministrator()]
                loMenuItem.BeginGroup = Type('g_Omenu.oParentobject.bNoMenuAction')='L' And Not g_Omenu.oParentObject.bNoMenuAction
                loMenuItem.OnClick = [do cp_DoAction with "ecpSecurity"]
                loMenuItem.Visible = .T.
            Else
                * --- Autorizzazione
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_AUTORITY) Skip For Not(cp_IsAdministrator())
                On Sele Bar (j) Of contextmenu Do cp_DoAction With "ecpSecurity"
                j=j+1
            Endif

            * ---- Sicurezza a livello di record...
            If cp_IsAdministrator() And i_bSecurityRecord And Type('g_Omenu.oParentobject.cFunction')='C' And g_Omenu.oParentObject.cFunction="Query"
                If i_VisualTheme <> -1
                    loMenuItem = m.j.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_DATA_AUTORITY)
                    loMenuItem.SkipFor = [!cp_isAdministrator()]
                    loMenuItem.OnClick = [do cp_DoAction with "ecpSecurityRecord"]
                    loMenuItem.Visible = .T.

                    loMenuItem = m.j.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_VIEW_DATA_AUTORITY)
                    loMenuItem.SkipFor = [!cp_isAdministrator()]
                    loMenuItem.OnClick = [do cp_recseczoom with i_curform]
                    loMenuItem.Visible = .T.

                Else
                    * --- Autorizzazione sul dato...
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_DATA_AUTORITY) Skip For Not(cp_IsAdministrator())
                    On Sele Bar (j) Of contextmenu Do cp_DoAction With "ecpSecurityRecord"
                    j=j+1
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_VIEW_DATA_AUTORITY) Skip For Not(cp_IsAdministrator())
                    On Sele Bar (j) Of contextmenu Do cp_recseczoom With i_curform
                    j=j+1

                Endif
            Endif

            * --- Zucchetti Aulla Inizio - Gestione controllo flussi
            l_BeginGroup = .F.
            If Vartype(g_CTFL)='C' And g_CTFL='S' And Type('g_Omenu.oParentobject.cFunction')='C'
                Local l_gscf_mre, cSkipFor
                l_BeginGroup = .T.
                l_gscf_mre=Createobject("CUSTOM")
                l_gscf_mre.AddProperty("bSec1")
                l_gscf_mre.AddProperty("bSec2")
                l_gscf_mre.AddProperty("bSec3")
                l_gscf_mre.AddProperty("bSec4")
                l_gscf_mre.AddProperty("bFox26")
                l_gscf_mre.AddProperty("cPrg")
                l_gscf_mre.cPrg = "gscf_mre"
                Do cp_GetSecurity With l_gscf_mre, l_gscf_mre.cPrg
                If i_VisualTheme<>-1
                    Local loMenuItemFls
                    loMenuItemFls = m.j.AddMenuItem()
                    loMenuItemFls.Caption = cp_Translate(MSG_MENU_FLOW)
                    loMenuItemFls.BeginGroup = .T.
                    loMenuItemFls.Visible = .T.

                    loMenuItem = m.loMenuItemFls.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_LOAD_FLOW)
                    loMenuItem.SkipFor = Iif(l_gscf_mre.bSec1 And l_gscf_mre.bSec2 ,".F.",".T.")
                    loMenuItem.OnClick = [do cp_DoAction with "ecpLoadFlus"]
                    loMenuItem.BeginGroup = .F.
                    loMenuItem.Visible = .T.

                    loMenuItem = m.loMenuItemFls.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_CTRL_FLOW)
                    loMenuItem.SkipFor = Iif(l_gscf_mre.bSec1,".F.",".T.")
                    loMenuItem.OnClick = [do cp_DoAction with "ecpCtrlFlus"]
                    loMenuItem.BeginGroup = .F.
                    loMenuItem.Visible = .T.

                    loMenuItem = m.loMenuItemFls.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_VIEW_FLOW)
                    loMenuItem.OnClick = [do cp_DoAction with "ecpViewFlus"]
                    loMenuItem.BeginGroup = .T.
                    loMenuItem.Visible = .T.
                Else
                    * --- Separatore
                    Define Bar (j) Of contextmenu Prompt "\-"
                    j=j+1

                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_MENU_FLOW)
                    Define Popup ctrlflussi Margin shortcut
                    On Bar (j) Of contextmenu Activate Popup ctrlflussi
                    j=j+1
                    T=1

                    cSkipFor = Iif(l_gscf_mre.bSec1 And l_gscf_mre.bSec2 ,".F.",".T.")
                    Define Bar (T) Of ctrlflussi Prompt cp_Translate(MSG_LOAD_FLOW) Skip For &cSkipFor
                    On Sele Bar (T) Of ctrlflussi Do cp_DoAction With "ecpLoadFlus"
                    T=T+1

                    cSkipFor = Iif(l_gscf_mre.bSec1,".F.",".T.")
                    Define Bar (T) Of ctrlflussi Prompt cp_Translate(MSG_CTRL_FLOW) Skip For &cSkipFor
                    On Sele Bar (T) Of ctrlflussi Do cp_DoAction With "ecpCtrlFlus"
                    T=T+1

                    Define Bar (T) Of ctrlflussi Prompt "\-"
                    T=T+1

                    Define Bar (T) Of ctrlflussi Prompt cp_Translate(MSG_VIEW_FLOW)
                    On Sele Bar (T) Of ctrlflussi Do cp_DoAction With "ecpViewFlus"
                    T=T+1

                Endif
                Release l_gscf_mre
            Endif
            * --- Zucchetti Aulla Fine - Gestione controllo flussi
            * --- Zucchetti Aulla Inizio - sincronizzazione
            If Vartype(g_SINC)='C' And g_SINC='S' And cp_IsAdministrator() And Type('g_Omenu.oParentobject.cFunction')='C' And g_Omenu.oParentObject.cFunction='Query'
                If i_VisualTheme <> -1
                    loMenuItem = m.j.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_SINC_CREATE_RULE)
                    loMenuItem.SkipFor = [!cp_isAdministrator()]
                    loMenuItem.OnClick = [do cp_DoAction with "ecpSinc"]
                    loMenuItem.BeginGroup = l_BeginGroup
                    loMenuItem.Visible = .T.

                Else
                    If l_BeginGroup
                        Define Bar (j) Of contextmenu Prompt "\-"
                        j=j+1
                    Endif

                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SINC_CREATE_RULE) Skip For Not(cp_IsAdministrator())
                    On Sele Bar (j) Of contextmenu Do cp_DoAction With "ecpSinc"
                    j=j+1

                Endif
                l_BeginGroup = .F.
            Endif
            * --- Zucchetti Aulla Fine- sincronizzazione
        Endif

        * --- Zucchetti Aulla Inizio Informazioni Mirror logistica remota..
        * --- se Revolution, se modulo attivo, se non � una maschera, se sono amministratore e se non � la spalla dei gadget...
        If g_APPLICATION="ADHOC REVOLUTION" And Type("g_LORE")='C' And g_LORE='S' And Not oCPToolbar.b4.Enabled And cp_IsAdministrator() And (Type('g_Omenu.oParentobject.cPrg')<>'C' Or Upper(g_Omenu.oParentobject.cPrg)<>"GSUT_KMG")
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate('Mirror logistica remota')
                loMenuItem.OnClick = cFunzione+[ ='X']
                loMenuItem.BeginGroup = l_BeginGroup
                loMenuItem.Visible = .T.
            Else
                If l_BeginGroup
                    Define Bar (j) Of contextmenu Prompt "\-"
                    j=j+1
                Endif

                Define Bar (j) Of contextmenu Prompt cp_Translate('Mirror logistica remota')
                On Sele Bar (j) Of contextmenu funzione='X'
                j=j+1
            Endif
            l_BeginGroup = .F.
        Endif
        * --- Zucchetti Aulla Fine Informazioni Mirror logistica remota..

        If Type('g_Omenu.oParentobject.bNoMenuProperty')='L'And Not g_Omenu.oParentObject.bNoMenuProperty
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_PROPERTY)
                loMenuItem.BeginGroup = .T.
                loMenuItem.OnClick = [Do cp_DoAction With 'ecpInfo']
                loMenuItem.Visible = .T.
            Else
                * --- Separatore
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
                * --- Propriet�
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PROPERTY)
                On Sele Bar (j) Of contextmenu Do cp_DoAction With 'ecpInfo'
                j=j+1
            Endif
        Endif
        * Disattivo le funzioni associate ai tasti 'ESC' e 'F1'
        If i_VisualTheme <> -1
            * --- Disattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('D')
            m.j.InitMenu(.T.)
            m.j.ShowMenu()
            * --- Riattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('A')
            funzione = &cFunzione
        Else
            If Cntbar('contextmenu')>0
                *se ho attiva almeno una voce
                This.GESTPOPUP('D')
                Activate Popup contextmenu
                Deactivate Popup contextmenu
                Release Popups contextmenu
                *Riattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('A')
            Endif
        Endif
        If Type("funzione")="C"
            If funzione='R'
                If cp_getEntityType(This.oContained.oContained.cPrg) <> "Dialog Window"
                    This.oContained.oContained.loadRecWarn()
                Endif
                This.oContained.oContained.Refresh()
            Endif

            * --- Zucchetti Aulla Inizio Informazioni Mirror logistica remota..
            If funzione='X'
                Local l_sPrg
                l_sPrg="DO gslr_kin WITH this.oContained.oContained"
                &l_sPrg
            Endif
            * --- Zucchetti Aulla Fine Informazioni Mirror logistica remota..
            Release &cFunzione
        Endif
        * --- Zucchetti Aulla Inizio rilascio oggetti Inforeader
        If Vartype(g_IRDR) = 'C' And g_IRDR = 'S'
            Release oObjContext
            Release lZoom
        Endif
        * --- Zucchetti Aulla Fine rilascio oggetti Inforeader
    Endfunc

    ** Crea il menu per gli zoom
    Function zoomMenu
        Param j,contextmenu
        Local cStyle,cBmpFile,m, bKeyValid, n_i, obj
        Local funzione, cFunzione
        cFunzione = Sys(2015)
        Public &cFunzione
        &cFunzione = ""
        funzione=''
        **Visualizziamo il menu solo se siamo in uno zoom.
        **Sarebbe possibile inserire le funzionalit� del tasto destro anche nel caso in cui ci trovassimo
        **in una pagina "Elenco", abilitando il controllo "remmato". In realt� per� tale controllo genererebbe errore
        **nel caso di tasto destro da zoom integrato, in quanto la proprit� caption non � in quella posizione, ma in
        **This.oContained.
        If Not Empty(This.cZoomOnZoom)And Type('g_Omenu.oContained.bNoMenuAction')='L'And Not g_Omenu.oContained.bNoMenuAction &&Or Upper(cp_translate(This.oContained.Parent.Caption)) == Upper(cp_translate(MSG_LIST))
            bKeyValid=This.nKeyCount>=1
            For n_i=1 To This.nKeyCount
                bKeyValid=bKeyValid And Not Empty( This.getByIndexKeyValue(n_i))
            Endfor

            If bKeyValid And Not Empty(This.cZoomOnZoom) &&Or Upper(cp_translate(This.oContained.Parent.Caption)) == Upper(cp_translate(MSG_LIST))
                Do Case
                        *  --- Zucchetti Aulla Inizio ( OR This.cEntity=='Routine')- Batch Zoom On Zoom Gestiti tramite propriet� cBatchType
                    Case This.cEntity=='Master File' Or This.cEntity=='Master/Detail' Or This.cEntity=='Detail File' Or This.cEntity=='Routine'
                        If i_VisualTheme <> -1
                            loMenuItem = m.j.AddMenuItem()
                            loMenuItem.Caption = cp_Translate(MSG_OPEN)
                            loMenuItem.BeginGroup = .T.
                            loMenuItem.Visible = .T.

                            loMenuItem.OnClick = cFunzione+[ = 'A']

                            loMenuItem = m.j.AddMenuItem()
                            loMenuItem.Caption = cp_Translate(MSG_CHANGE)
                            loMenuItem.Visible = .T.

                            loMenuItem.OnClick = cFunzione+[= 'M']

                            loMenuItem = m.j.AddMenuItem()
                            loMenuItem.Caption = cp_Translate(MSG_LOAD)
                            loMenuItem.Visible = .T.

                            loMenuItem.OnClick = cFunzione+[= 'L']
                        Else
                            Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_OPEN)
                            On Sele Bar (j) Of contextmenu funzione ='A'
                            j=j+1
                            Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_CHANGE)
                            On Sele Bar (j) Of contextmenu funzione='M'
                            j=j+1
                            Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_LOAD)
                            On Sele Bar (j) Of contextmenu funzione='L'
                            j=j+1
                            Define Bar (j) Of contextmenu Prompt "\-"
                            j=j+1
                        Endif
                        *  --- Zucchetti Aulla Inizio ( OR This.cEntity=='Routine')- Batch Zoom On Zoom Gestiti tramite propriet� cBatchType
                    Case This.cEntity=='Dialog Window'
                        If i_VisualTheme <> -1
                            loMenuItem = m.j.AddMenuItem()
                            loMenuItem.Caption = cp_Translate(MSG_OPEN)
                            loMenuItem.BeginGroup = .T.
                            loMenuItem.Visible = .T.

                            loMenuItem.OnClick = cFunzione+[= 'A']
                        Else
                            Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_OPEN)
                            On Sele Bar (j) Of contextmenu funzione ='A'
                            j=j+1
                            Define Bar (j) Of contextmenu Prompt "\-"
                            j=j+1
                        Endif
                    Otherwise
                        Release funzione
                        Return
                Endcase
            Endif
        Endif
        * Zucchetti Aulla - Inizio - Visualizzazione query InfoPublisher su gestione
        If Type('g_IRDR') = 'C' And g_IRDR = 'S'
            Public lZoom
            lZoom = .T.
            * --- se siamo in una pagina elenco, non generer� errore...
            If Type('this.oContained.parent.parent.parent.cCurInfo') = 'C'
                obj=This.oContained.Parent.Parent.Parent
            Else
                * --- siamo in uno zoom integrato.
                If Type("this.oContained.oParentObject")<>"U"
                    obj=This.oContained.oParentObject
                Else
                    obj=.Null.
                Endif
                * --- Cerchiamo la propriet� cCursorInfo a salire...
                Do While Not Isnull(obj) And Type('obj.cCurInfo')<>'C'
                    * --- Controllo oParentoObject per gli zoom lanciati da maschere disegnate con User ToolKit
                    * --- In questo caso infatti oParentObject non esiste e non posso gestire infoPublisher
                    If Type('obj.oParentObject')='O'
                        obj=obj.oParentObject
                    Else
                        Exit
                    Endif
                Enddo
            Endif
            * --- obj.cCurInfo potrebbe essere ancora vuoto se la pressione del tasto destro � su uno zoom lanciato
            * --- da maschere disegnate con User ToolKit
            If Not Isnull(obj) And Type('obj.cCurInfo')='C'
                * --- Cerco se esistono delle query associate allo zoom
                * --- Lancio il batch che valorizzar� il cursore delle query
                If obj.bCurForm Or ! Used(obj.cCurInfo)  &&Cerco nome zoom
                    I_sPrg="GSIR_BIC"
                    Do (I_sPrg) With obj, This.oContained.cZoomName+'.'+This.oContained.cSymFile
                    obj.bCurForm = .F.
                Endif
                If Used(obj.cCurInfo)  &&Cerco nome gestione
                    Select (obj.cCurInfo)
                    If Reccount() = 0
                        Select (obj.cCurInfo)
                        Use
                        I_sPrg="GSIR_BIC"
                        Do (I_sPrg) With obj, obj.GetSecurityCode()
                        lZoom = .F.
                        obj.bCurForm = .F.
                    Endif
                Endif
                If Used(obj.cCurInfo)  &&Cerco nome tabella
                    Select (obj.cCurInfo)
                    If Reccount() = 0 And Not Empty(This.cLinkFile) And Not Empty(This.oKey(1,3))
                        Select (obj.cCurInfo)
                        Use
                        I_sPrg="GSIR_BIC"
                        * ---- Trasformo i valori dei campi chiave in stringhe
                        Do (I_sPrg) With obj, This.cLinkFile+','+Alltrim(Transform(This.oKey(1,3)))
                        lZoom = .F.
                        obj.bCurForm = .F.
                    Endif
                    * --- Creo menu PopUp
                    Select (obj.cCurInfo)
                    If Reccount() > 0
                        Public oObjContext
                        oObjContext=This.oContained
                        j=CreateMenuInfoPublisher(obj,j,contextmenu)
                        obj=.Null.
                        If i_VisualTheme = -1
                            * --- Separatore
                            Define Bar (j) Of contextmenu Prompt "\-"
                            j=j+1
                        Endif
                    Endif
                Endif
            Endif
        Endif
        This.CreateMenuSZoom()
        *--- Zucchetti Aulla - Inizio export su excel/calc
        If Type("g_OFFICE")='C' And (g_OFFICE=='M' Or g_OFFICE=='O')
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = Iif(g_OFFICE=='M', cp_Translate(MSG_EXCEL_EXPORT), cp_Translate(MSG_CALC_EXPORT))
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = "g_Omenu.oContained.ExcelCalcExport()"
            Else
            ***--- Zucchetti Aulla - Inizio - nel caso in cui non ci sia installato n� office ne openoffice non bisogna aggiungere la voce di menu
            IF g_OFFICE=='M' OR g_OFFICE=='O'
                Define Bar (j) Of contextmenu Prompt Iif(g_OFFICE=='M', cp_Translate(MSG_EXCEL_EXPORT), cp_Translate(MSG_CALC_EXPORT))
                On Sele Bar (j) Of contextmenu g_Omenu.oContained.ExcelCalcExport()
                j=j+1
                * --- Separatore
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
               ENDIF
             ***--- Zucchetti Aulla - Fine - nel caso in cui non ci sia installato n� office ne openoffice non bisogna aggiungere la voce di menu
            Endif
        Endif
        *--- Zucchetti Aulla - Fine export su excel/calc
        If Type('g_Omenu.oContained.bNoMenuProperty')='L' And Not g_Omenu.oContained.bNoMenuProperty
            If Type('g_oMenu.oCtrl.parent.parent.parent.bOptions')='L' And g_Omenu.oCtrl.Parent.Parent.Parent.bOptions
                * --- Voci menu gestione zoom
                If i_VisualTheme <> -1
                    loMenuItem = m.j.AddMenuItem()
                    loMenuItem.BeginGroup = .T.
                    *--- Zucchetti Aulla - Inizio - tasto destro sull' elenchi dei clienti con IRDR non attivo
                    Select(g_Omenu.oContained.cCursor)
                    *--- Zucchetti Aulla - Fine -  tasto destro sull' elenchi dei clienti con IRDR non attivo
                    If g_Omenu.oContained.grd.ActiveRow>0
                        loMenuItem.Caption = cp_Translate(MSG_ADD_FILTER)
                        loMenuItem.Visible = .T.
                        loMenuItem.OnClick = "g_Omenu.oCtrl.Parent.Parent.Parent.AddFilterFast(g_Omenu.oContained)"
                        loMenuItem = m.j.AddMenuItem()
                    Endif
                    loMenuItem.Caption = cp_Translate(MSG_ASK_FOR_PARAMETERS)
                    loMenuItem.Visible = .T.
                    loMenuItem.OnClick = "g_Omenu.oContained.sel.click()"
                    loMenuItem = m.j.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_SETTINGS)
                    loMenuItem.Visible = .T.
                    loMenuItem.OnClick = "g_Omenu.oContained.dir.click()"
                    loMenuItem = m.j.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_EXECUTE_REPORT)
                    loMenuItem.Visible = .T.
                    loMenuItem.OnClick = "g_Omenu.oContained.rep.click()"
                    loMenuItem = m.j.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_QUERY_BUTTON)
                    loMenuItem.Visible = .T.
                    loMenuItem.OnClick = "g_Omenu.oContained.qry.click()"
                    If g_Omenu.oContained.zz.Visible
                        loMenuItem = m.j.AddMenuItem()
                        loMenuItem.Caption = cp_Translate(MSG_OPEN_FORM)
                        loMenuItem.Visible = .T.
                        loMenuItem.OnClick = "g_Omenu.oContained.zz.click()"
                    Endif
                Else
                    * --- Separatore
                    Define Bar (j) Of contextmenu Prompt "\-"
                    *--- Zucchetti Aulla - Inizio - tasto destro sull' elenchi dei clienti con IRDR non attivo
                    Select(g_Omenu.oContained.cCursor)
                    *--- Zucchetti Aulla - Fine -  tasto destro sull' elenchi dei clienti con IRDR non attivo
                    If g_Omenu.oContained.grd.ActiveRow>0
                        j=j+1
                        Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_ADD_FILTER)
                        On Sele Bar (j) Of contextmenu g_Omenu.oCtrl.Parent.Parent.Parent.AddFilterFast(g_Omenu.oContained)
                    Endif
                    j=j+1
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_ASK_FOR_PARAMETERS)
                    On Sele Bar (j) Of contextmenu g_Omenu.oContained.sel.Click()
                    j=j+1
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SETTINGS)
                    On Sele Bar (j) Of contextmenu g_Omenu.oContained.Dir.Click()
                    j=j+1
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_EXECUTE_REPORT)
                    On Sele Bar (j) Of contextmenu g_Omenu.oContained.rep.Click()
                    j=j+1
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_QUERY_BUTTON)
                    On Sele Bar (j) Of contextmenu g_Omenu.oContained.qry.Click()
                    j=j+1
                    If g_Omenu.oContained.zz.Visible
                        Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_OPEN_FORM)
                        On Sele Bar (j) Of contextmenu g_Omenu.oContained.zz.Click()
                        j=j+1
                    Endif
                Endif
            Endif
            * --- Voce property
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_PROPERTY)
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.

                loMenuItem.OnClick = cFunzione+[ = 'P']
            Else
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PROPERTY)
                On Sele Bar (j) Of contextmenu funzione='P'
                j=j+1
            Endif
        Endif
        If i_VisualTheme <> -1
            * --- Disattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('D')
            m.j.InitMenu(.T.)
            m.j.ShowMenu()
            * --- Riattivo le funzione originarie dei tasti ESC e F1
            This.GESTPOPUP('A')
            funzione = &cFunzione
        Else
            If Cntbar('contextmenu')>0
                * --- Disattivo le funzioni associate ai tasti 'ESC' e 'F1'
                This.GESTPOPUP('D')
                Activate Popup contextmenu
                Deactivate Popup contextmenu
                Release Popups contextmenu
                * --- Riattivo le funzione originarie dei tasti ESC e F1
                This.GESTPOPUP('A')
            Endif
        Endif
        This.cBatchType=funzione
        Do Case
            Case funzione='A'
                contextm(This,funzione,.T.)  &&Apri
            Case funzione='M'
                contextm(This,funzione,.T.)  &&Modifica
            Case funzione='L'
                contextm(This,funzione,.T.) &&Load
            Case funzione='P'
                b='Cp_InfoZoom(this)'
                &b
        Endcase
        Release &cFunzione
        * --- Zucchetti Aulla inizio rilascio oggetti Inforeader
        If Type('g_IRDR') = 'C' And g_IRDR = 'S'
            Release oObjContext
            Release lZoom
        Endif
        * --- Zucchetti Aulla fine rilascio oggetti Inforeader
        obj=.Null.
    Endfunc

    * Procedura per l'esecuzione di una azione su un oggetto, sia l'azione che l'oggetto sono
    * parametrici

    Procedure DoObjAction
        Parameters obj, Action
        obj = Strtran(obj,"'",'')
        Action = Strtran(Action,"'",'')
        * diversifico il comportamento in base alla tipologia dell'oggetto (Control, Gestione)
        If Type('g_Omenu.oParentObject') = 'O'
            If Upper(obj) = "OPARENTOBJECT"
                Do Case
                    Case Lower(Action) = "ecpsave"
                        g_Omenu.oParentObject.ecpSave()
                    Case Lower(Action) = "ecpquit"
                        g_Omenu.oParentObject.ecpQuit()
                    Case Lower(Action) = "ecpedit"
                        g_Omenu.oParentObject.ecpEdit()
                Endcase
            Else
                * Gestione NotifyEvent su tasto destro
                If Upper(obj) = "EVENT" And Not Empty(Action)
                    g_Omenu.oParentObject.NotifyEvent(Action)
                Else
                    Oggetto = g_Omenu.oParentObject.getctrl(obj)
                    If Type('Oggetto') = 'O'
                        Do Case
                            Case Lower(Action)= "click"
                                Oggetto.Click()
                            Case Lower(Action)= "Enabled"
                                If Oggetto.Enabled
                                    Oggetto.Enabled = .F.
                                Else
                                    Oggetto.Enabled = .T.
                                Endif
                        Endcase
                    Endif
                Endif
            Endif
        Else
            cp_ErrorMsg(MSG_CANNOT_CREATE_OBJECT_INSTANCES,48,'o_Menu')
        Endif
    Endproc

    * Procedura per la verifica di una condizione associata ad un oggetto, entrambi parametrici

    Procedure ObjInfo
        Parameters obj, Condition
        Local EDITABILE
        EDITABILE = .F.
        * diversifico il comportamento in base alla tipologia dell'oggetto (Control, Gestione)
        If Type('g_Omenu.OparentObject') = 'O'
            Oggetto = g_Omenu.oParentObject.getctrl(&obj)
            If Type('Oggetto') = 'O'
                Do Case
                    Case Lower(Condition) = "'condedit'"
                        EDITABILE = Oggetto.mCond()
                    Case Lower(Condition) = "'hide'"
                        EDITABILE = Oggetto.mHide()
                    Case Lower(Condition) = "'check'"
                        EDITABILE = Oggetto.Check()
                Endcase
            Endif
        Endif
        Return EDITABILE
    Endproc

    *--- Zucchetti Aulla - Inizio Frasi modello
    *--- Funzione frasi modello (abilita la ricerca di frasi modello nei campi memo)
    Function CreaMenuFrasiModello
        Parameters j
        If i_VisualTheme <>-1
            Local loMenuItem
            loMenuItem = m.j.AddMenuItem()
            loMenuItem.Caption = cp_Translate(MSG_PHRASE_MODEL)
            loMenuItem.SkipFor = "FLGM"
            loMenuItem.BeginGroup = .T.
            loMenuItem.Visible = .T.

            loMenuItem.OnClick = [g_oMenu.FrasiModello(g_oMenu.octrl)]

            loMenuItem = m.j.AddMenuItem()
            loMenuItem.Caption = cp_Translate(MSG_NEW_PHRASE_MODEL)
            loMenuItem.SkipFor = "FLGM"
            loMenuItem.BeginGroup = .F.
            loMenuItem.Visible = .T.

            loMenuItem.OnClick = [g_oMenu.NuovaFraseModello(g_oMenu.octrl)]
        Else
            Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_PHRASE_MODEL) Skip For FLGM
            On Sele Bar (j) Of contextmenu g_Omenu.FrasiModello(g_Omenu.oCtrl)
            j=j+1
            Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_NEW_PHRASE_MODEL) Skip For FLGM
            On Sele Bar (j) Of contextmenu g_Omenu.NuovaFraseModello(g_Omenu.oCtrl)
            j=j+1
            * --- Separatore
            Define Bar (j) Of contextmenu Prompt "\-"
            j = j + 1
        Endif
    Endfunc
    Function FrasiModello(pParent)
        *passo propriet� per parametri query
        pParent.Parent.oContained.AddProperty("ptipo",'C')
        pParent.Parent.oContained.AddProperty("pgest", Iif(Pemstatus(pParent.Parent.oContained, 'getSecurityCode', 5), pParent.Parent.oContained.GetSecurityCode(), pParent.Parent.oContained.cPrg))
        Public i_lastindirectaction
        i_lastindirectaction='ecpZoom'
        Do cp_zoom With 'FRA_MODE','*','FMSERIAL',cp_AbsName(pParent.Parent,pParent.Name),.F.,'GSUT_AFM',"MODELLI FRASI",'GSUT_AFM.FRA_MODE_VZM',pParent.Parent.oContained
        Removeproperty(pParent.Parent.oContained, "ptipo")
        Removeproperty(pParent.Parent.oContained, "pgest")
    Endfunc
    Function NuovaFraseModello(pParent)
        Local lanc
        lanc = 'GSUT_BCF(pParent.parent.oContained)'
        &lanc
    Endfunc
    *--- Zucchetti Aulla - Fine Frasi modello

    Procedure CreateMenuSZoom
        *--- Le voci di selezione/deselezione dei men� contestuali possono essere disattivate impostando la propriet�  DisableSelMenu=.T. dello zoom
        *--- Zucchetti Aulla - export su excel/calc
        If Type("This.oContained.class")='C' And Lower(This.oContained.Class)=Lower("cp_szoombox") And ( Type("This.oContained.DisableSelMenu")<>'L' Or  This.oContained.DisableSelMenu=.F.)
            If i_VisualTheme <> -1
                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_SELECT_ALL_TEXT)
                loMenuItem.BeginGroup = .T.
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = "g_Omenu.oContained.CheckAll()"

                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_SELECT_UNTIL)
                loMenuItem.BeginGroup = .F.
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = "g_Omenu.oContained.CheckTo()"

                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_SELECT_FROM)
                loMenuItem.BeginGroup = .F.
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = "g_Omenu.oContained.CheckFrom()"

                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_DESELECT_ALL)
                loMenuItem.BeginGroup = .F.
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = "g_Omenu.oContained.UnCheckAll()"

                loMenuItem = m.j.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_INVERT_SELECTION)
                loMenuItem.BeginGroup = .F.
                loMenuItem.Visible = .T.
                loMenuItem.OnClick = "g_Omenu.oContained.InvertSelection()"
            Else
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SELECT_ALL_TEXT)
                On Sele Bar (j) Of contextmenu g_Omenu.oContained.CheckAll()
                j=j+1
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SELECT_UNTIL)
                On Sele Bar (j) Of contextmenu g_Omenu.oContained.CheckTo()
                j=j+1
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SELECT_FROM)
                On Sele Bar (j) Of contextmenu g_Omenu.oContained.CheckFrom()
                j=j+1
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_DESELECT_ALL)
                On Sele Bar (j) Of contextmenu g_Omenu.oContained.UnCheckAll()
                j=j+1
                Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_INVERT_SELECTION)
                On Sele Bar (j) Of contextmenu g_Omenu.oContained.InvertSelection()
                j=j+1
                * --- Separatore
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
            Endif
        Endif
    Endproc

    Function CreateMenuTranslate
        Parameters j
        If i_VisualTheme <>-1
            Local loMenuItem
            loMenuItem = m.j.AddMenuItem()
            loMenuItem.Caption = cp_Translate(MSG_CONTEXT_TRANSLATE)
            loMenuItem.SkipFor = ""
            loMenuItem.BeginGroup = .T.
            loMenuItem.Visible = .T.

            loMenuItem.OnClick = [g_oMenu.cp_TranslateMask(g_oMenu.octrl)]

        Else
            Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_CONTEXT_TRANSLATE)
            On Sele Bar (j) Of contextmenu g_Omenu.cp_TranslateMask(g_Omenu.oCtrl)
            j=j+1
            * --- Separatore
            Define Bar (j) Of contextmenu Prompt "\-"
            j = j + 1
        Endif
    Endfunc
    Function cp_TranslateMask(pParent)
        Local lanc
        lanc = 'cp_TranslateMask(pParent)'
        &lanc
    Endfunc
    *- Funzione per il caricamento automatico delle voci di men� associate alle funzionalit� dei bottoni del form
    Function ButtonGest
        Parameters j
        Local i, k, i_Testo, i_Voce, PagElenco, TotPagForm, m, Abilitata ,T,cUid
        * --- controlliamo se esiste oParentObject (non esiste nelle pagine elenco)
        If Type('g_Omenu.oParentObject.OpgFrm')='O'
            TotPagForm = g_Omenu.oParentObject.oPgFrm.PageCount
            *Cerco la pagina di elenco
            PagElenco = TotPagForm
            Do While Type('g_Omenu.oParentObject.Opgfrm.pages(PagElenco).autozoom')='U' And PagElenco>0
                PagElenco = PagElenco - 1
            Enddo

            * --- inizializzazione variabile per creazione menu contestuale funzionalit�
            T=1
            *Per ogni pagina della gestione eccetto quella di elenco estraggo i bottoni che avviano le funzionalit�
            *inerenti alla gestione su cui si vuole creare il men� contestuale.
            For k = 1 To TotPagForm
                *Verifico se la pagina  attuale � quella di elenco oppure se i_bPageFrmButton � true
                *e la pagina attuale 'k' non � quella attiva, in questi casi passo direttamente alla
                *pagina successiva del page frame
                If k =  PagElenco Or (i_bPageFrmButton And k <> g_Omenu.oParentObject.oPgFrm.ActivePage)
                    Loop
                Endif
                *Ricerco le eventuali funzionalit� lanciate da bottoni presenti nella pagina
                If Type("g_Omenu.oParentObject.Opgfrm.pages(k).opag")="O" &&Testo solo le pagine che hanno opag (es, Allegati ed Elenco devono essere saltate)
                    For i = 1 To g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.ControlCount
                        *Verifico se l'oggetto i_esimo � un bottone
                        If g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).BaseClass = 'Commandbutton';
                                AND g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).Class = 'Stdbutton';
                                OR g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).BaseClass = 'Container';
                                AND g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).Class = 'Stdbutton';
                                OR g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).Class = 'Buttonmenu';
                                OR g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).Class = 'Advbuttonmenu'
                            *-- Inserisco l'applicazione associato al bottone nel men� contestuale
                            i_Testo=g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).Caption
                            cUid=g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).uid
                            *- Bottoni senza Caption (vedi bottoni per cattura immagini). Utilizzo il ToolTip per costruire il men�
                            If Empty(i_Testo)
                                i_Testo=Left(g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).ToolTipText,100)
                            Endif
                            *- Ricavo il nome da dare alla nuova voce del men� contestuale (in pratica elimino caratteri '\' e'<')
                            i_Voce = i_Testo
                            *i_Voce = Strtran(cp_Translate(i_Voce),'\<','')
                            If ! Inlist(Upper(Strtran(cp_Translate(i_Voce),'\<','')),cp_Translate('...'),Upper(Strtran(cp_Translate(MSG_OK_BUTTON),'\<','')),Upper(Strtran(cp_Translate(MSG_CANCEL_BUTTON),'\<','')),;
                                    Upper(Strtran(cp_Translate(MSG_EXIT_1_BUTTON),'\<',''))) And g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).Visible
                                If T=1
                                    * --- creazione popup Funzionalit�
                                    If i_VisualTheme<>-1
                                        funz = m.j.AddMenuItem()
                                        funz.Caption = cp_Translate(MSG_FUNCTION)
                                        funz.Visible = .T.
                                        funz.BeginGroup = .T.
                                    Else
                                        Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_FUNCTION)
                                        Define Popup funz Margin shortcut
                                        On Bar j Of contextmenu Activate Popup funz
                                        j=j+1
                                    Endif
                                Endif
                                *- Gestisco l'abilitazione o meno del bottone
                                * --- Definiamo le bar per la presentazione dei bottoni della maschera nel menu contestuale
                                If i_VisualTheme<>-1
                                    If g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).Class = 'Advbuttonmenu'
                                        *Gli ADVButtonmenu hanno un menu associato percio si deve aggiungere un sottomenu con tutte le funzionalit� associate
                                        g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).AddMenu (funz , k , i)
                                    Else
                                        loMenuItem = funz.AddMenuItem()
                                        loMenuItem.Caption = cp_Translate(i_Voce)
                                        loMenuItem.SkipFor = "!g_Omenu.oParentObject.Opgfrm.pages("+Transform(k)+").opag.controls("+Transform(i)+").enabled"
                                        loMenuItem.Visible = .T.

                                        loMenuItem.Description = g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).ToolTipText
                                        loMenuItem.OnClick = [g_omenu.DoObjAction("]+cUid+[",'click')]
                                    Endif
                                Else
                                    If g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).Class = 'Advbuttonmenu'
                                        *Gli ADVButtonmenu hanno un menu associato percio si deve aggiungere un sottomenu con tutte le funzionalit� associate
                                        g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).AddMenu (T , k , i )
                                    Else
                                        If g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).Enabled
                                            Define Bar (T) Of funz Prompt cp_Translate(i_Voce) Skip For .F. Message g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).ToolTipText
                                        Else
                                            Define Bar (T) Of funz Prompt cp_Translate(i_Voce) Skip For .T. Message g_Omenu.oParentObject.oPgFrm.Pages(k).oPag.Controls(i).ToolTipText
                                        Endif

                                        m="on sele BAR ("+Alltrim(Str(T))+") of funz g_omenu.DoObjAction('"+cUid+"','click')"
                                        &m
                                    Endif
                                Endif
                                T=T+1
                                * --- Aggiorniamo sempre il contatore

                            Endif
                        Endif
                    Endfor
                Endif &&TYPE("g_Omenu.oParentObject.Opgfrm.pages(k).opag")="O"
            Endfor
            If T<>1 And i_VisualTheme=-1
                * --- Separatore
                Define Bar (j) Of contextmenu Prompt "\-"
                j = j + 1
            Endif
        Else
            Return
        Endif
    Endfunc

    *-- Metodo per il cambio della funzione associata ai tasti 'ESC' e 'F1'
    Proc GESTPOPUP
        Parameters Operazione
        Local lab1,lab2
        * --- Controllo se devo ripristinare o meno le chiavi
        * --- Il ripristino avviene anche nella Init della StdForm
        If Not(This.bRecLabel)
            Return
        Endif
        Do Case
            Case Operazione = 'D'
                * --- Memorizzo le vecchie impostazioni
                This.cOldLabel1=On('KEY','ESC')
                This.cOldLabel2=On('Key','F1')
                * Disabilito le funzioni assegnate ai tasti ESC e F1 di default
                On Key Label ESC Deactivate Popups contextmenu
                On Key Label F1
            Case Operazione = 'A'
                * --- Devo usare due varibili locali, altrimenti la macro non funziona
                lab1=This.cOldLabel1
                lab2=This.cOldLabel2
                * Riabilito le funzioni assegnate ai tasti ESC e F1 di default
                On Key Label ESC &lab1
                On Key Label F1  &lab2
        Endcase
    Endproc

Enddefine

*--- Zucchetti Aulla Inizio - Configurazioni gestione
*--- CreaMenuCfgGestione
Function CreaMenuCfgGestione
    Parameters j, pTipo, nRow, nCol
    *--- pTipo='M' = Menu, pTipo='D' = Toolbar
    Local i, l_res, l_Macro,l_sPrg, bMenu
    Local loMenuItem
    bMenu=.F.  &&Mi dice se nel caso pTipo='D' � stato creato il menu
    If (Vartype(g_bDisableCfgGest)="U" Or !g_bDisableCfgGest) And Type( "i_curform.cprg" )='C'
        *--- Lancio controllo cfg, l_res=-1
        l_sPrg="GSUT_BCG(i_curform, 'T')"
        l_res=&l_sPrg
        If l_res>=0
            *--- Ho il modello, se esistono cfg per l'utente l_res>0
            If pTipo='M'
                If i_VisualTheme <> -1
                    Local loMenuItemCfg
                    loMenuItemCfg = m.j.AddMenuItem()
                    loMenuItemCfg.Caption = cp_Translate(MSG_SETTINGS)
                    loMenuItemCfg.BeginGroup = .T.
                    loMenuItemCfg.Picture='.\bmp\configure_small.ico'
                    loMenuItemCfg.Visible = .T.
                Else
                    Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SETTINGS) Picture g_Omenu.RetBmpForMenu('.\bmp\configure_small.bmp')
                    On Bar j Of contextmenu Activate Popup CfgGestione
                    Define Popup CfgGestione shortcut Margin
                    j=j+1
                Endif
            Else
                If i_VisualTheme <> -1
                    Local loPopupMenu, loMenuItemCfg
                    If Type("_screen.PopupMenu")<>"U"
                        _Screen.PopupMenu.Delete()
                    Endif
                    loPopupMenu = Createobject("cbPopupMenu")
                    loMenuItemCfg = loPopupMenu.AddMenuItem()
                    loMenuItemCfg.Caption = cp_Translate(MSG_SETTINGS)
                    loMenuItemCfg.BeginGroup = .T.
                    loMenuItemCfg.Picture='.\bmp\configure_small.ico'
                    loMenuItemCfg.Visible = .T.
                Else
                    Define Popup CfgGestione From nRow, nCol In Screen shortcut Margin
                Endif
                bMenu=.T.
            Endif
            If i_VisualTheme<>-1
                loMenuItem = loMenuItemCfg.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_SAVE)
                loMenuItem.SkipFor = [IIF(!ISNULL(i_curform),EMPTY(i_curform.nCfgGest), .t.)]
                loMenuItem.OnClick = [GSUT_BCG(i_curform, 'F')]
                loMenuItem.Visible = .T.

                loMenuItem = loMenuItemCfg.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_SAVE_AS)
                loMenuItem.SkipFor = [IIF(!ISNULL(i_curform),.f., .t.)]
                loMenuItem.OnClick = [GSUT_BCG(i_curform, 'A')]
                loMenuItem.Visible = .T.

                loMenuItem = loMenuItemCfg.AddMenuItem()
                loMenuItem.Caption = cp_Translate(MSG_CFG_OPEN)
                loMenuItem.SkipFor = [IIF(!ISNULL(i_curform),.f., .t.)]
                loMenuItem.OnClick = [OpenGest("A", "GSUT_MCG", "CGNOMGES", PADR(UPPER(i_curform.getSecurityCode()), 20))]
                loMenuItem.Visible = .T.
            Else
                i=1
                Define Bar (i) Of CfgGestione Prompt cp_Translate(MSG_SAVE) Skip For Iif(!Isnull(i_curform),Empty(i_curform.nCfgGest), .T.)
                l_sPrg="GSUT_BCG(i_curform, 'F')"
                On Sele Bar (i) Of CfgGestione &l_sPrg
                i=i+1
                Define Bar (i) Of CfgGestione Prompt cp_Translate(MSG_SAVE_AS) Skip For Iif(!Isnull(i_curform),.F., .T.)
                l_sPrg="GSUT_BCG(i_curform, 'A')"
                On Sele Bar (i) Of CfgGestione &l_sPrg
                i=i+1
                Define Bar (i) Of CfgGestione Prompt cp_Translate(MSG_CFG_OPEN) Skip For Iif(!Isnull(i_curform),.F., .T.)
                On Sele Bar (i) Of CfgGestione OpenGest("A", "GSUT_MCG", "CGNOMGES", Padr(Upper(i_curform.GetSecurityCode()), 20))
                i=i+1
            Endif
            Local l_bFirst,	l_cStyle
            l_bFirst=.T.
            If Used("CURSCFGGEST")
                Select("CURSCFGGEST")
                Locate For 1=1
                Do While Not(Eof())
                    If i_VisualTheme=-1
                        If l_bFirst
                            * --- Separatore
                            Define Bar (i) Of CfgGestione Prompt "\-"
                            i=i+1
                            l_bFirst=.F.
                        Endif
                    Endif
                    If i_VisualTheme<>-1
                        loMenuItem = loMenuItemCfg.AddMenuItem()
                        loMenuItem.Caption = Iif(CURSCFGGEST.CGDEFCFG = 'S', "("+Alltrim(CURSCFGGEST.CGDESCFG)+")", Alltrim(CURSCFGGEST.CGDESCFG))
                        loMenuItem.SkipFor = [IIF(!ISNULL(i_curform),.f., .t.)]
                        loMenuItem.BeginGroup = l_bFirst
                        loMenuItem.Picture=Iif(Empty(Nvl(CURSCFGGEST.CGCODAZI,"")), "", "bmp\magaz.ico")
                        loMenuItem.OnClick = [GSUT_BCG(i_curform, 'L',]+Alltrim(Str(CURSCFGGEST.CPROWNUM))+[)]
                        loMenuItem.MarkFor = "i_curform.nCfgGest="+Alltrim(Str(CURSCFGGEST.CPROWNUM))
                        loMenuItem.Visible = .T.
                        l_bFirst=.F.
                    Else
                        l_cStyle = Iif(CURSCFGGEST.CGDEFCFG = 'S', "B", "")
                        l_cStyle = l_cStyle + Iif(Empty(Nvl(CURSCFGGEST.CGCODAZI,"")), "U", "")
                        l_Macro="Define Bar (i) Of CfgGestione Prompt ALLTRIM(CURSCFGGEST.CGDESCFG) STYLE '" + l_cStyle +"'"
                        &l_Macro
                        Define Bar (i) Of CfgGestione Prompt Alltrim(CURSCFGGEST.CGDESCFG) Skip For Iif(!Isnull(i_curform),.F., .T.)
                        l_Macro="On Sele Bar (i) Of CfgGestione GSUT_BCG(i_curform, 'L',"+Alltrim(Str(CURSCFGGEST.CPROWNUM))+")"
                        &l_Macro
                        *--- Check su cfg corrente
                        Set Mark Of Bar i Of CfgGestione To i_curform.nCfgGest=CURSCFGGEST.CPROWNUM
                        i=i+1
                    Endif
                    Select("CURSCFGGEST")
                    Continue
                Enddo
                Use
            Endif
            If pTipo='M' And i_VisualTheme=-1
                * --- Separatore
                Define Bar (j) Of contextmenu Prompt "\-"
                j=j+1
            Endif
        Else
            *--- Non ho il modello, abilito solo Caricamento modello se sono amministratore
            If cp_IsAdministrator()
                If pTipo='M'
                    If i_VisualTheme <> -1
                        Local loMenuItemCfg
                        loMenuItemCfg = m.j.AddMenuItem()
                        loMenuItemCfg.Caption = cp_Translate(MSG_SETTINGS)
                        loMenuItemCfg.BeginGroup = .T.
                        loMenuItemCfg.Picture='.\bmp\configure_small.ico'
                        loMenuItemCfg.Visible = .T.
                    Else
                        Define Bar (j) Of contextmenu Prompt cp_Translate(MSG_SETTINGS) Picture g_Omenu.RetBmpForMenu('.\bmp\configure_small.bmp')
                        On Bar j Of contextmenu Activate Popup CfgGestione
                        Define Popup CfgGestione shortcut Margin
                        j=j+1
                    Endif
                Else
                    If i_VisualTheme <> -1
                        Local loPopupMenu, loMenuItemCfg
                        If Type("_screen.PopupMenu")<>"U"
                            _Screen.PopupMenu.Delete()
                        Endif
                        loPopupMenu = Createobject("cbPopupMenu")
                        loMenuItemCfg = loPopupMenu.AddMenuItem()
                        loMenuItemCfg.Caption = cp_Translate(MSG_SETTINGS)
                        loMenuItemCfg.BeginGroup = .T.
                        loMenuItemCfg.Picture='.\bmp\configure_small.ico'
                        loMenuItemCfg.Visible = .T.
                    Else
                        Define Popup CfgGestione From nRow, nCol In Screen shortcut Margin
                    Endif
                    bMenu=.T.
                Endif
                If i_VisualTheme <> -1
                    loMenuItem = loMenuItemCfg.AddMenuItem()
                    loMenuItem.Caption = cp_Translate(MSG_CFG_MODEL)
                    loMenuItem.SkipFor = [IIF(!ISNULL(i_curform),.f., .t.)]
                    loMenuItem.OnClick = [GSUT_BCG(i_curform, 'M')]
                    loMenuItem.Visible = .T.
                Else
                    Define Bar (1) Of CfgGestione Prompt cp_Translate(MSG_CFG_MODEL) Skip For Iif(!Isnull(i_curform),.F., .T.)
                    l_sPrg="GSUT_BCG(i_curform, 'M')"
                    On Sele Bar (1) Of CfgGestione &l_sPrg
                    If pTipo='M'
                        * --- Separatore
                        Define Bar (j) Of contextmenu Prompt "\-"
                        j=j+1
                    Endif
                Endif
            Endif
        Endif
        *--- Attivo il menu
        If  pTipo='D' And bMenu
            If i_VisualTheme <> -1
                m.loPopupMenu.InitMenu(.T.)
                m.loPopupMenu.ShowMenu()
            Else
                Activate Popup CfgGestione
                Deactivate Popup CfgGestione
            Endif
        Endif
    Endif
Endfunc
*--- Zucchetti Aulla Fine - Configurazioni gestione

Define Class Open_Gest As Custom
    cZoomOnZoom=''
    nKeyCount=0
    Dimension oKey(1,3)
Enddefine

Procedure OpenGest(pFlag,pProgram,pKey1,pKeyValue1,pKey2,pKeyValue2,pKey3,pKeyValue3,pKey4,pKeyValue4,pKey5,pKeyValue5;
        ,pKey6,pKeyValue6,pKey7,pKeyValue7,pKey8,pKeyValue8,pKey9,pKeyValue9,pKey10,pKeyValue10)

    * --- I PARAMETRI 'pFlag' e 'pProgram' SONO OBBLIGATORI
    * --- pFlag pu� assumere:
    * --- 'S' per apertura entit� in Show (sola apertura senza filtri)
    * --- 'A' per apertura entit� filtrata (ecpfilter())
    * --- 'M' per apertura entit� filtrata in modifica (ecpEdit())
    * --- 'L' per apertura entit� in caricamento
    * --- 'P' per l'apertura di una dialog con valorizzazione di variabili
    * --- PER APERTURA DIALOG WINDOW � VALIDA QUALUNQUE SINTASSI
    * --- pProgram � il nome dell'entit� da lanciare
    * --- pKey(n) � il campo chiave (senza 'W_'!!)
    * --- pKeyValue(n) � il valore del campo
    Local l_n, l_i, l_m,nParam
    Local ObjeGest As Object

    objGest=Createobject('Open_Gest')
    objGest.cZoomOnZoom = pProgram
    nParam=Pcount()
    * --- Se Sono presenti delle chiavi, provvedo ad inizializare
    * --- un array di chiavi per apertura filtrata maschera.
    If nParam>2
        objGest.nKeyCount=(nParam - 2)/2
        Dimension objGest.oKey(objGest.nKeyCount,3)
        For l_i = 1 To objGest.nKeyCount
            l_m= 'pKey'+Alltrim(Str(l_i))
            objGest.oKey(l_i,1)= &l_m
            l_m= 'pKeyValue'+Alltrim(Str(l_i))
            objGest.oKey(l_i,3)= &l_m
        Endfor
    Else
        * --- Se non sono presenti chiavi, modifico il parametro pFlag
        * --- per una corretta apertura maschera
        pFlag=Iif(pFlag$'A-M','S', pFlag)
    Endif
    Local i_errsav, i_bError
    i_errsav=On('ERROR')
    i_bError=.F.
    On Error i_bError =.T.
    contextm(objGest, pFlag,.T.)
    On Error &i_errsav
    If i_bError
        cp_ErrorMsg(MSG_CANNOT_ACCESS)
    Endif
Endproc



** Procedura che permette di aprire le gestioni corrette.
Procedure   contextm
    Param pParent, Flag, pCallHasCPEvents
    * --- Local variables
    Local maskName, numberKey, queryKey, n_i,maskTemp,cparam
    Local obj As Object
    Local sEntity
    Local L_cTrsName,locNUMROW
    maskName=Alltrim(pParent.cZoomOnZoom)

    *  Per Gestire le maschere con parametri
    Local tMaskName, lMaskFill
    If Type('Flag')<>"C"
        Flag=Space(1)
    Endif

    lMaskFill = .T.
    * --- Per consentire l'apertura di gestioni con parametri
    maskName=Strtran(maskName,"*","'")
    * --- Mi memorizzo il valore del controllo
    tMaskName=Alltrim(maskName)
    * --- Se trovo una parentesi, la elimino per controllo esistenza file
    If At('(',maskName)>0
        maskTemp=Alltrim(Substr(maskName,1,At('(',maskName)-1))
    Else
        * --- Se non trovo parentesi, le aggiungo per corretta apertura maschera
        cparam=' '
        maskTemp=maskName
        If Vartype(i_curform)='O' And Lower(i_curform.Class)=Lower('t'+maskName)
            cparam=Upper(i_curform.GetSecurityCode())
        Endif
        If Occurs(',',cparam)>0
            maskName=maskName+ "('" + Substr(cparam,At(',',cparam)+1) + "')"
        Else
            maskName=maskName+'()'
        Endif
    Endif
    sEntity=cp_getEntityType(maskTemp)
    If cp_fileexist(maskTemp+".fxp")
        * --- Zucchetti Aulla Inizio - Per Gestire le maschere con parametri
        * --- Nel caso di Dialog Window per non perdere il controllo delle maschera
        * --- simulo di essere l'utente schedulatore
        If sEntity='Dialog Window'
            Local g_OLDSCHEDULER
            If Not(Type("g_SCHEDULER")="C")
                Public g_SCHEDULER
                g_SCHEDULER = 'N'
            Endif
            g_OLDSCHEDULER=Iif(Empty(g_SCHEDULER) Or g_SCHEDULER='N','N','S')
            g_SCHEDULER="S"
            * --- Lancio la gestione
            obj = MakeContextEntity(maskName, tMaskName)
            *obj = &maskName
            * --- Zucchetti Aulla Fine (riga sopra originale commentata) - Per Gestire le maschere con parametri
            * --- Zucchetti Aulla Inizio - Per Gestire le maschere con parametri
            * --- Ripristino il vecchio valore dell'utente schedulatore
            g_SCHEDULER = g_OLDSCHEDULER
            * --- Mi posiziono sul primo controllo
            If Vartype(obj)='O' And Not Isnull(obj)
                obj.GetFirstControl()
                * --- Visualizzo la maschera in primo piano ed effettuo il refresh
                Show Windows (m.obj.Name) Refresh Top
            Endif
        Else
            * --- Lancio la gestione
            obj = MakeContextEntity(maskName, tMaskName)
        Endif
        * --- Zucchetti Aulla Fine - Per Gestire le maschere con parametri
        ** Nel caso la Gestione non venga aperta.
        If sEntity="Routine"
            **� appena stato lanciato un batch...
            Return
        Endif
    Else
        cp_Msg(cp_msgformat(MSG__FXP_MISSING, pParent.cZoomOnZoom),.F.)
        Return
    Endif
    * --- Controllo accesso procedura
    If Type('obj.bSec1')='U' Or !(obj.bSec1)
        Return
    Endif
    * --- Se l'entit� che andiamo ad aprire � una Dialog Window, non facciamo nulla
    If sEntity!='Dialog Window'
        * ---  Apre in Interroga (maschera vuota)
        If Flag="S"
            Return
        Endif
        * --- Apre la gestione in modalit� "Carica"
        If Flag="L" And (!pCallHasCPEvents Or obj.HasCPEvents("ecpLoad"))
            obj.ecpLoad()
            Return
        Endif
        n_i=1
        If (!pCallHasCPEvents Or obj.HasCPEvents("ecpFilter"))
            obj.ecpFilter()
        Endif
    Else
        * --- Con Flag='P'
        * --- Apre la dialog e compila la gestione
        * --- Con Flag<>"P"
        * --- Apre la dialog e non compila i campi (maschera vuota)
        lMaskFill = sEntity!='Dialog Window' Or Flag="P"
        n_i=1
    Endif

    If lMaskFill
        locNUMROW=0
        Do While n_i<=pParent.nKeyCount
            lx="obj.w_"+pParent.oKey[n_i,1]
            If Upper(pParent.oKey[n_i,1])='CPROWNUM'
                locNUMROW=pParent.oKey[n_i,3]
            Endif
            &lx=pParent.oKey[n_i,3]
            If Flag="P"
                SetContextCtrlValue(obj, Alltrim(pParent.oKey[n_i,1]))
            Endif
            n_i=n_i+1
        Enddo
    Endif

    If sEntity!='Dialog Window'
        * --- Zucchetti Aulla Inizio - Ricerca da tasto dx (Apri/Modifica)
        If Vartype(i_ExactSearch)='U'
            * --- Variabile usata nella BuildWhere: indica se la ricerca avviene o meno da tasto dx (Apri/Modifica)
            Public i_ExactSearch
        Endif
        i_ExactSearch = .T.
        * --- Zucchetti Aulla Fine - Ricerca da tasto dx (Apri/Modifica)
        If (!pCallHasCPEvents Or obj.HasCPEvents("ecpSave"))
            obj.ecpSave()
        Endif
        * --- Zucchetti Aulla Inizio - Ricerca da tasto dx (Apri/Modifica)
        i_ExactSearch = .F.
        * --- Zucchetti Aulla Fine

        * --- Apre la gestione in modifica
        If Flag="M" And  obj.bLoaded
            If (!pCallHasCPEvents Or obj.HasCPEvents("ecpEdit"))
                obj.ecpEdit()
            Endif
        Endif
        If Type("obj.cTrsname")='C' And locNUMROW<>0
            L_cTrsName=obj.cTrsname
            Select (L_cTrsName)
            Go Top
            Locate For CPROWNUM= locNUMROW
            obj.oPgFrm.Page1.oPag.oBody.Refresh()
            obj.WorkFromTrs()
            obj.SaveDependsOn()
            obj.SetControlsValue()
            obj.mHideControls()
            obj.ChildrenChangeRow()
            *obj.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.GotFocus()
        Endif
    Else
        * --- Visualizzo la gestione
        obj.Show
    Endif
Endproc

* Zucchetti Aulla - Inizio - Per Gestire le maschere con parametri

Func MakeContextEntity
    Param paskName, ptMaskname
    Local obj As Object
    If At('(',ptMaskname)>0
        * --- Lancio la gestione
        obj = &ptMaskname
    Else
        * --- Lancio la gestione
        obj = &paskName
    Endif
    Return(obj)

    * --- Nel caso di Dialog Window allora recupero il controllo ed eseguo tutte le operazioni
    * --- necessarie per popolare la maschera
Procedure SetContextCtrlValue
    Param obj, p_varname
    Local l_VarName, l_cTrl, l_Macro
    * --- Recupero il Controllo
    l_VarName = "w_"+ p_varname && ALLTRIM(pParent.oKey[n_i,1])
    l_cTrl = obj.getctrl(l_VarName)
    If Type("l_cTrl")="O" And !Isnull(l_cTrl)
        * --- i_cTrl.SetFocus()
        *     Se � un Campo con link allora la Invoco
        If Not Empty(l_cTrl.cLinkFile)
            * --- recupero il nome della funzione
            l_Macro= "obj.Link"+Right(l_cTrl.Name, Len(l_cTrl.Name) - Rat("_", l_cTrl.Name ,2)+1)+"('Full')"
            &l_Macro
            l_Macro= "obj.NotifyEvent('"+Alltrim(l_VarName)+" Changed')"
            &l_Macro
            obj.mCalc(.T.)
            obj.SaveDependsOn()
        Else
            l_cTrl.bUpd=.T.
            obj.SetControlsValue()
            l_cTrl.Valid()
        Endif
    Endif
    obj =.Null.
    l_cTrl = .Null.
Endproc

** Funzione di ricerca per la composizione dei menu da rightclick

Func cp_findNamedDefaultFileMenu(cName,cExt)
    * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    If i_MonitorFramework
        Local i
        If cp_fileexist (Alltrim(i_usrpath)+'rightClick\'+cName+'_'+Alltrim(Str(i_CODUTE))+'.'+cExt,.T.)
            i_pathFindFile = Alltrim(i_usrpath)+'rightClick\'+cName+'_'+Alltrim(Str(i_CODUTE))+'.'+cExt
            Return(Lower(Addbs(Justpath(i_pathFindFile))+(Juststem(i_pathFindFile))))
        Endif
        If i_CODUTE<>0 && se l' utente e' gia' stato caricato si possono provare i default per utente e gruppi di appartenenza
            cp_FillGroupsArray()
            For i=1 To Alen(i_aUsrGrps)
                If cp_fileexist(Alltrim(i_usrpath)+'rightClick\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt,.T.)
                    i_pathFindFile = Alltrim(i_usrpath)+'rightClick\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt
                    Return(Lower(Addbs(Justpath(i_pathFindFile))+(Juststem(i_pathFindFile))))
                Endif
            Next
        Endif
        If cp_fileexist('rightClick\'+cName+'.'+cExt,.T.) Or cp_fileexist('custom\rightClick\'+cName+'.'+cExt,.T.)
            i_pathFindFile = 'rightClick\'+cName+'.'+cExt
            Return(Lower(Addbs(Justpath(i_pathFindFile))+(Juststem(i_pathFindFile))))
        Endif
        && ###start remove from setup###
        If cp_fileexist('..\..\exe\rightClick\'+cName+'.'+cExt,.T.)
            i_pathFindFile = '..\..\exe\rightClick\'+cName+'.'+cExt
            Return(Lower(Addbs('..\..\exe\rightClick\'+Justpath(i_pathFindFile))+(Juststem(i_pathFindFile))))
        Endif
        && ###end remove from setup###
        i_pathFindFile = Alltrim(MSG_NOT_FILEEXIST)+'|'+Alltrim(cName)+'.'+Alltrim(cExt)
        Return('')
    Else
        * --- Zucchetti Aulla fine - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
        Local i
        If cp_fileexist ('rightClick\'+cName+'_'+Alltrim(Str(i_CODUTE))+'.'+cExt,.T.) Or cp_fileexist ('custom\rightClick\'+cName+'_'+Alltrim(Str(i_CODUTE))+'.'+cExt,.T.)
            Return(cName+'_'+Alltrim(Str(i_CODUTE)))
        Endif
        If i_CODUTE<>0 && se l' utente e' gia' stato caricato si possono provare i default per utente e gruppi di appartenenza
            cp_FillGroupsArray()
            For i=1 To Alen(i_aUsrGrps)
                If cp_fileexist('rightClick\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt,.T.) Or cp_fileexist('custom\rightClick\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt,.T.)
                    Return(cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                Endif
            Next
        Endif
        If cp_fileexist('rightClick\'+cName+'.'+cExt,.T.) Or cp_fileexist('custom\rightClick\'+cName+'.'+cExt,.T.)
            Return(cName)
        Endif
        && ###start remove from setup###
        If cp_fileexist('..\..\exe\rightClick\'+cName+'.'+cExt,.T.)
            Return('..\..\exe\rightClick\'+cName)
        Endif
        && ###end remove from setup###
        Return('')
        * --- Zucchetti Aulla Inizio - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework
    Endif
Endfunc
* --- Zucchetti Aulla fine - Monitor Framework (riviste le routine di ricerca per velocizzare l'accesso ai file di framework

Function cp_maxroword()
    Local i_a,i_r
    i_a=Select()
    Select Max(t_cproword) As mo From Alias() Into Cursor __maxord__
    i_r=__maxord__.mo
    Use
    Select (i_a)
    Return (i_r)

    * --- Funzione selezione directory..
Function Cp_getDir(cDir,ctext,cCaption,nFLags,bRootOnly )
    Local nParam
    nParam=Pcount()
    If nParam<=4
        bRootOnly=.F.
    Endif

    If nParam<=3
        * --- Di default adotto il nuovo Layout
        nFLags=64+1
    Endif

    If nParam<=2
        cCaption=i_MsgTitle
    Else
        cCaption=cp_Translate(i_MsgTitle)
    Endif

    If nParam<=1 Or Empty(ctext)
        ctext=cp_Translate(MSG_OPEN_PATH)
    Else
        ctext=cp_Translate(ctext)
    Endif

    If nParam<=0
        cDir=''
    Endif
    #If Version(5)>=800
        Return(Getdir(cDir,ctext,cCaption,nFLags,bRootOnly))
    #Else
        * --- In VFP 6.0 la GetDir accetta solo due parametri..
        Return(Getdir(cDir,ctext))
    #Endif
Endfunc

*--- Converte una stringa in una data
*--- 1� Stringa da convertire
*--- 2� Formato della data del 1� parametro
*--- Ritorna un Date
*--- Es. cp_CharToDate(31-12-2005), cp_CharToDate(2005/12/31, 'yyyy-mm-dd')
Function cp_CharToDate(cStr, cSetDate)
    Local cNewDate, yyyy, dd, mm, yy, nyy, nRollOver
    Local cStr_1, cStr_2, cStr_3, nStat, cMacro, ddStat, mmStat, yyStat, l_n
    Local L_errsav, L_errmsg, retDat,carat

    If Type("cSetDate")<>'C'
        cNewDate='dd-mm-yy'
    Else
        cNewDate=Lower(cSetDate)
    Endif
    If (Occurs('d',cNewDate)+Occurs('y',cNewDate)+Occurs('m',cNewDate))=Len(cNewDate)
        * aggiungo separatore
        If At('y',cNewDate)>1
            cNewDate=Strtran(cNewDate,'y','-y',1,1)
            carat=Substr(cStr,At('y',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1)
            cStr=Stuff(cStr,At('y',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1,'-'+ carat)
        Endif
        If At('m',cNewDate)>1
            cNewDate=Strtran(cNewDate,'m','-m',1,1)
            carat=Substr(cStr,At('m',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1)
            cStr=Stuff(cStr,At('m',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1,'-'+ carat)
        Endif
        If At('d',cNewDate)>1
            cNewDate=Strtran(cNewDate,'d','-d',1,1)
            carat=Substr(cStr,At('d',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1)
            cStr= Stuff(cStr,At('d',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1,'-'+ carat)
        Endif
    Endif
    *--- Determino Rollover
    nRollOver=Iif(Type("g_Rollover")='N', g_Rollover, 30)

    *--- Se la stringa contenente la data � di lunghezza maggiore della stringa del formato
    *--- significa che l'anno � scritto in forma estesa
    If Len(cStr)>Len(cNewDate)
        cNewDate=Stuff(cNewDate, At('y',cNewDate),0,"yy")
    Endif
    cStr_1=''
    cStr_2=''
    cStr_3=''
    nStat=1
    For l_n =1 To Len(cStr)
        ctmp=Substr(cStr,l_n,1)
        If Isdigit(ctmp)
            cMacro="cStr_"+Alltrim(Str(nStat))+"=cStr_"+Alltrim(Str(nStat))+"+cTmp"
            &cMacro
        Else
            If Not Empty(ctmp)
                * Potrebbe essere una data costruita con valori nulli
                * In questo if devo considerare solo i separatori
                nStat=nStat+1
            Endif
        Endif
    Endfor

    *--- Se una delle parti � vuota ritono data vuota
    If Empty(cStr_1) Or Empty(cStr_2) Or Empty(cStr_3)
        Return {}
    Endif
    *--- Determino il formato
    nStat=1
    dFind=.F.
    mFind=.F.
    yFind=.F.
    For l_n =1 To Len(cNewDate)
        ctmp=Substr(cNewDate,l_n,1)
        Do Case
            Case ctmp='d' And !dFind
                ddStat=nStat
                dFind=.T.
                nStat=nStat+1
            Case ctmp='m' And !mFind
                mmStat=nStat
                mFind=.T.
                nStat=nStat+1
            Case ctmp='y' And !yFind
                yyStat=nStat
                yFind=.T.
                nStat=nStat+1
        Endcase
    Endfor
    *--- Calcolo yyyy (rollover)
    If Occurs("y", cNewDate)<>4
        cMacro="yy=cStr_"+Alltrim(Str(yyStat))
        &cMacro
        nyy=Val(yy)
        cMacro="cStr_"+Alltrim(Str(yyStat))+"=IIF(nyy>=nRollOver and nyy<=99, ALLTRIM(STR(1900+nyy)), ALLTRIM(STR(2000+nyy)))"
        &cMacro
    Endif

    cMacro="dd=cStr_"+Alltrim(Str(ddStat))
    &cMacro

    cMacro="mm=cStr_"+Alltrim(Str(mmStat))
    &cMacro

    cMacro="yyyy=cStr_"+Alltrim(Str(yyStat))
    &cMacro
    * Test data vuota
    If Val(mm)=0 Or Val(dd)=0
        Return {}
    Endif
    L_errsav=On('ERROR')
    L_errmsg=''
    On Error L_errmsg="error"
    retDat=Evaluate("{^"+yyyy+"-"+mm+"-"+dd+"}")
    On Error &L_errsav
    If L_errmsg="error"
        *--- Errore nella generazione della data ritorno data vuota
        Return {}
    Else
        Return retDat
    Endif
Endfunc

*--- Converte una stringa in una data
*--- 1� Stringa da convertire
*--- 2� Formato della data del 1� parametro
*--- Ritorna un DateTime
*--- Es. cp_CharToDatetime(31-12-2005), cp_CharToDatetime(2005/12/31 12:33:45, 'yyyy-mm-dd hh:nn:ss')
Function cp_CharToDatetime(cStr, cSetDate)
    Local cNewDate, yyyy, dd, mm, yy, hh, nn, ss, nyy, nRollOver
    Local cStr_1, cStr_2, cStr_3, nStat, cMacro, ddStat, mmStat, yyStat
    Local cStr_3, cStr_4, cStr_5, hhStat, nnStat, ssStat
    Local L_errsav, L_errmsg, retDat,carat

    If Type("cSetDate")<>'C'
        cNewDate='dd-mm-yy hh:nn:ss'
    Else
        cNewDate=Lower(cSetDate)
    Endif
    *--- Se passo una stringa senza time, lo aggiungo
    If Len(cStr)<=10
        cStr=cStr+" 00:00:00"
    Endif
    *--- Se passo una stringa senza i secomdi (hh:mm), li aggiungo, altrimenti scatta il rollover
    If Len(cStr)<=16
        cStr=cStr+":00"
    Endif
    If (Occurs('d',cNewDate)+Occurs('y',cNewDate)+Occurs('m',cNewDate)+Occurs('h',cNewDate)+Occurs('n',cNewDate)+Occurs('s',cNewDate))=Len(cNewDate)
        * aggiungo separatore
        If At('y',cNewDate)>1
            cNewDate=Strtran(cNewDate,'y','-y',1,1)
            carat=Substr(cStr,At('y',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1)
            cStr=Stuff(cStr,At('y',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1,'-'+ carat)
        Endif
        If At('m',cNewDate)>1
            cNewDate=Strtran(cNewDate,'m','-m',1,1)
            carat=Substr(cStr,At('m',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1)
            cStr=Stuff(cStr,At('m',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1,'-'+ carat)
        Endif
        If At('d',cNewDate)>1
            cNewDate=Strtran(cNewDate,'d','-d',1,1)
            carat=Substr(cStr,At('d',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1)
            cStr= Stuff(cStr,At('d',cNewDate)-Occurs('-',cNewDate)+Occurs('-',cStr),1,'-'+ carat)
        Endif
        If At('h',cNewDate)>1
            cNewDate=Strtran(cNewDate,'h',' h',1,1)
        Endif
        If At('n',cNewDate)>1
            cNewDate=Strtran(cNewDate,'n',':n',1,1)
        Endif
        If At('s',cNewDate)>1
            cNewDate=Strtran(cNewDate,'s',':s',1,1)
        Endif

    Endif
    *--- Determino Rollover
    nRollOver=Iif(Type("g_Rollover")='N', g_Rollover, 30)

    *--- Se la stringa contenente la data � di lunghezza maggiore della stringa del formato
    *--- significa che l'anno � scritto in forma estesa
    If Len(cStr)>Len(cNewDate)
        cNewDate=Stuff(cNewDate, At('y',cNewDate),0,"yy")
    Endif
    cStr_1=''
    cStr_2=''
    cStr_3=''
    cStr_4=''
    cStr_5=''
    cStr_6=''
    nStat=1
    For i=1 To Len(cStr)
        ctmp=Substr(cStr,i,1)
        If Isdigit(ctmp)
            cMacro="cStr_"+Alltrim(Str(nStat))+"=cStr_"+Alltrim(Str(nStat))+"+cTmp"
            &cMacro
        Else
            nStat=nStat+1
        Endif
    Endfor

    *--- Se una delle parti � vuota ritono data vuota
    If Empty(cStr_1) Or Empty(cStr_2) Or Empty(cStr_3)
        Return {/:}
    Endif

    *--- Determino il formato
    nStat=1
    dFind=.F.
    mFind=.F.
    yFind=.F.
    hFind=.F.
    nFind=.F.
    sFind=.F.
    For i=1 To Len(cNewDate)
        ctmp=Substr(cNewDate,i,1)
        Do Case
            Case ctmp='d' And !dFind
                ddStat=nStat
                dFind=.T.
                nStat=nStat+1
            Case ctmp='m' And !mFind
                mmStat=nStat
                mFind=.T.
                nStat=nStat+1
            Case ctmp='y' And !yFind
                yyStat=nStat
                yFind=.T.
                nStat=nStat+1
            Case ctmp='h' And !hFind
                hhStat=nStat
                hFind=.T.
                nStat=nStat+1
            Case ctmp='n' And !nFind
                nnStat=nStat
                nFind=.T.
                nStat=nStat+1
            Case ctmp='s' And !sFind
                ssStat=nStat
                sFind=.T.
                nStat=nStat+1
        Endcase
    Endfor
    *--- Calcolo yyyy (rollover)
    If Occurs("y", cNewDate)<>4
        cMacro="yy=cStr_"+Alltrim(Str(yyStat))
        &cMacro
        nyy=Val(yy)
        cMacro="cStr_"+Alltrim(Str(yyStat))+"=IIF(nyy>=nRollOver and nyy<=99, ALLTRIM(STR(1900+nyy)), ALLTRIM(STR(2000+nyy)))"
        &cMacro
    Endif

    cMacro="dd=cStr_"+Alltrim(Str(ddStat))
    &cMacro

    cMacro="mm=cStr_"+Alltrim(Str(mmStat))
    &cMacro

    cMacro="yyyy=cStr_"+Alltrim(Str(yyStat))
    &cMacro

    cMacro="hh=cStr_"+Alltrim(Str(hhStat))
    &cMacro

    cMacro="nn=cStr_"+Alltrim(Str(nnStat))
    &cMacro

    cMacro="ss=cStr_"+Alltrim(Str(ssStat))
    &cMacro

    * Test data vuota
    If Val(mm)=0 Or Val(dd)=0
        Return {/:}
    Endif
    L_errsav=On('ERROR')
    L_errmsg=''
    On Error L_errmsg="error"
    retDat=Evaluate("{^"+yyyy+"-"+mm+"-"+dd+" "+hh+":"+nn+":"+ss+"}")
    On Error &L_errsav
    If L_errmsg="error"
        *--- Errore nella generazione della data ritorno data vuota
        Return {/:}
    Else
        Return retDat
    Endif
Endfunc

* --- Zucchetti Aulla inizio
* ----------------------------------------------------------------------------------------------
* --- Funzione per determinare se nel campo USER � indicato il codice ISO della lingua
* --- nella quale � stato sviluppato il report
* ----------------------------------------------------------------------------------------------
Func cp_ReportLanguage(p_ReportName)
    Local i_cRet,i_cOldArea,i_cOldError,i_lErr,i_cDefLanguage
    i_cOldArea=Alias()
    i_cOldError=On('ERROR')
    i_lErr=.F.
    i_cDefLanguage=Iif(Vartype(g_DEFAULTLANGUAGE)<>'C' Or Empty(g_DEFAULTLANGUAGE), 'ITA', g_DEFAULTLANGUAGE)
    i_cRet=i_cDefLanguage
    * cerca la lingua del report nel campo USER
    If cp_fileexist(Forceext(Alltrim(p_ReportName),".frx"))
        * Set errori
        p_ReportName=cp_GetStdFile(p_ReportName,Iif(Lower(Right(Rtrim(p_ReportName),4))=".frx","","FRX"))
        On Error i_lErr=.T.
        * Apri il report come .DBF
        Use (Forceext(Alltrim(p_ReportName),".frx")) In 0 Alias translate_report Shared
        If Not(i_lErr)
            Select translate_report
            Go Top
            * Cerca
            Locate For Objtype=9 And Objcode=1
            If Found() And Not(Empty(User))
                i_cRet = Alltrim(User)
            Endif
        Endif
        * Check Error
        On Error &i_cOldError
        * Chiusura report
        If Used('translate_report')
            Use In translate_report
        Endif
        * Riposiziona area
        If Not(Empty(i_cOldArea))
            Select &i_cOldArea
        Endif
    Endif
    * Risultato
    Return(i_cRet)


    * ----------------------------------------------------------------------------------------------
    * --- Funzione per determinare se per il report � disattivo il gdi
    * ----------------------------------------------------------------------------------------------
Func cp_ReportGdi(p_ReportName)
    Local i_cRet,i_cOldArea,i_cOldError,i_lErr,i_cDefGdi
    If Vartype(i_bnogdi)='L' And i_bnogdi
        Return 'BNOGDI'
    Endif
    i_cOldArea=Alias()
    i_cOldError=On('ERROR')
    i_lErr=.F.
    i_cDefGdi='90'
    i_cRet=i_cDefGdi
    * cerca la lingua del report nel campo USER
    If cp_fileexist(Forceext(Alltrim(p_ReportName),".frx"))
        * Set errori
        On Error i_lErr=.T.
        * Apri il report come .DBF
        Use (Forceext(Alltrim(p_ReportName),".frx")) In 0 Alias gdi_report Shared
        If Not(i_lErr)
            Select gdi_report
            Go Top
            * Cerca
            Locate For Objtype=9 And Objcode=4
            If Found() And Not(Empty(User))
                i_cRet = Alltrim(User)
            Endif
        Endif
        * Check Error
        On Error &i_cOldError
        * Chiusura report
        If Used('gdi_report')
            Use In gdi_report
        Endif
        * Riposiziona area
        If Not(Empty(i_cOldArea))
            Select &i_cOldArea
        Endif
    Endif
    * Risultato
    Return(i_cRet)


    * ----------------------------------------------------------------------------------------------
    * --- Zucchetti Aulla - funzione di creazione context menu per InfoPublisher*
    * ----------------------------------------------------------------------------------------------
Function CreateMenuInfoPublisher(obj,j,contextmenu)
    Local cCurInfo_valuemenu,posizione_menu,var_menu,k,arr_i
    LOCAL TestMacro
    * --- Salvo il puntatore alla form, thisform non pu� essere utilizzato direttamente
    If Used(obj.cCurInfo)
        Dimension vocimenuinfo(1,2)
        Select (obj.cCurInfo)
        If Reccount() > 0
            * --- Se ho delle query associate alla gestione creo il menu
            If i_VisualTheme <> -1
                Local loInfoPublisher, loMenuItem
                loInfoPublisher = m.j.AddMenuItem()
                loInfoPublisher.Caption = cp_Translate('InfoPublisher')
                loInfoPublisher.Picture='bmp\IReader.ico'
                loInfoPublisher.BeginGroup = .T.
                loInfoPublisher.Visible = .T.
            Else
                Define Bar (j) Of contextmenu Prompt cp_Translate('InfoPublisher') Picture g_Omenu.RetBmpForMenu('bmp\IReader.bmp')         &&Definisco il sottomen� "InfoPublisher"
                On Bar j Of contextmenu Activate Popup InfoPublisher
                Define Popup InfoPublisher shortcut Margin
                j=j+1
            Endif
            k = 0
            * --- Per ciascuna query aggiungo una voce al men�
            Select (obj.cCurInfo)
            cCurInfo_valuemenu=obj.cCurInfo
            cCurInfo_valuemenu=cCurInfo_valuemenu+'.MENU'
            Locate For 1=1
            Do While Not(Eof())
				TestMacro=&cCurInfo_valuemenu
                If TestMacro<>Replicate('@',254)
                    nNum=Alines( arr_menu, &cCurInfo_valuemenu, 0, '\')
                    var_menu=''
                    For arr_i=1 To nNum
                        posizione_padre=Ascan(vocimenuinfo,var_menu,1,0,1,8)
                        var_menu=var_menu+Iif(Not Empty(var_menu),'_','')+Strtran(Alltrim(arr_menu(arr_i)),' ','_')
                        posizione_menu=Ascan(vocimenuinfo,var_menu,1,0,1,8)
                        If posizione_menu=0
                            If arr_i=1
                                k=k+1
                            Endif
                            Dimension vocimenuinfo(Alen(vocimenuinfo,1)+1,2)
                            vocimenuinfo(Alen(vocimenuinfo,1),1)=var_menu
                            vocimenuinfo(Alen(vocimenuinfo,1),2)=0
                            posizione_menu=Alen(vocimenuinfo,1)
                            If posizione_padre<>0
                                vocimenuinfo(posizione_padre,2)=vocimenuinfo(posizione_padre,2)+1
                            Endif
                            If i_VisualTheme<>-1
                                m="Local loInfo"+Alltrim(vocimenuinfo(posizione_menu,1))
                                &m
                                m="loInfo"+Iif(arr_i=1,'Publisher',Alltrim(vocimenuinfo(posizione_padre,1)))+".AddMenuItem()"
                                loMenuItem = &m
                                m = "loInfo"+Alltrim(vocimenuinfo(posizione_menu,1))+"=loMenuItem"
                                &m
                                loMenuItem.Caption = Alltrim(arr_menu(arr_i))
                                loMenuItem.Visible = .T.

                            Else
                                m="DEFINE BAR "+Alltrim(Str(Iif(arr_i=1,k,vocimenuinfo(posizione_padre,2))))+" OF Info"+Iif(arr_i=1,'Publisher',Alltrim(vocimenuinfo(posizione_padre,1)))+" prompt ALLTRIM(arr_menu(arr_i))"
                                &m
                                m="On bar "+Alltrim(Str(Iif(arr_i=1,k,vocimenuinfo(posizione_padre,2))))+" Of Info"+Iif(arr_i=1,'Publisher',Alltrim(vocimenuinfo(posizione_padre,1)))+" activate Popup Info"+Alltrim(vocimenuinfo(posizione_menu,1))
                                &m
                                m="DEFINE POPUP Info"+Alltrim(vocimenuinfo(posizione_menu,1))+" SHORTCUT"
                                &m
                            Endif
                        Endif
                    Endfor

                    vocimenuinfo(posizione_menu,2)=vocimenuinfo(posizione_menu,2)+1
                    If i_VisualTheme <> -1
                        m= 'loInfo'+Alltrim(vocimenuinfo(posizione_menu,1))
                        loMenuItem = &m
                        loMenuItem = loMenuItem.AddMenuItem()
                        loMenuItem.Caption = Alltrim(AQDES)
                        loMenuItem.Picture=Iif(TIPO='F', "filter_small.ico","")
                        loMenuItem.Visible = .T.

                        loMenuItem.OnClick = 'GSIR_BIR (.null., "' + AQCODICE + '", oObjContext , "' + TIPO + '" , ' + Alltrim(Str(CPROWNUM)) + ', lZoom)'
                    Else
                        * --- Modifico lo stile del font per distinguere le query filtrate da quelle std
                        cStyle = Iif(TIPO = 'F', 'I', 'N')
                        m='Define Bar '+Alltrim(Str(vocimenuinfo(posizione_menu,2)))+' Of Info'+Alltrim(vocimenuinfo(posizione_menu,1))+' Prompt ALLTRIM(AQDES) Style cStyle'
                        &m
                        * --- Utilizzo una macro altrimenti viene sempre lanciato il batch con i parametri dell' ultima query
                        * --- Utilizzando la macro riesco a risolvere il problema
                        m = 'On Sele Bar '+Alltrim(Str(vocimenuinfo(posizione_menu,2)))+' Of Info'+Alltrim(vocimenuinfo(posizione_menu,1))+' GSIR_BIR (.null., "' + AQCODICE + '", oObjContext , "' + TIPO + '" , ' + Alltrim(Str(CPROWNUM)) + ', lZoom)'
                        &m
                    Endif
                    Select (obj.cCurInfo)
                    Continue

                Else
                    k=k+1
                    If i_VisualTheme <> -1
                        loMenuItem = loInfoPublisher.AddMenuItem()
                        loMenuItem.Caption = Alltrim(AQDES)
                        loMenuItem.Picture=Iif(TIPO='F', "filter_small.ico","")
                        loMenuItem.Visible = .T.
                        loMenuItem.OnClick = 'GSIR_BIR (.null., "' + AQCODICE + '", oObjContext , "' + TIPO + '" , ' + Alltrim(Str(CPROWNUM)) + ', lZoom)'
                    Else
                        * --- Modifico lo stile del font per distinguere le query filtrate da quelle std
                        cStyle = Iif(TIPO = 'F', 'I', 'N')
                        Define Bar (k) Of InfoPublisher Prompt Alltrim(AQDES) Style cStyle
                        * --- Utilizzo una macro altrimenti viene sempre lanciato il batch con i parametri dell' ultima query
                        * --- Utilizzando la macro riesco a risolvere il problema
                        m = 'On Sele Bar ('+Alltrim(Str(k))+') Of InfoPublisher GSIR_BIR (.null., "' + AQCODICE + '", oObjContext , "' + TIPO + '" , ' + Alltrim(Str(CPROWNUM)) + ', lZoom)'
                        &m
                    Endif
                    Select (obj.cCurInfo)
                    Continue
                Endif
            Enddo
        Endif
    Endif
    Return j
Endfunc
* --- Zucchetti Aulla fine

* --- Se bNoSetPath e .t. non ricerca nell'intero path definito
Function cp_fileexist(i_cName, bNoSetPath)
    * --- Elimino eventuali spazi in coda..
    i_cName=Alltrim(i_cName)
    If Vartype(g_VFPfile)<>'U' And g_VFPfile
        bNoSetPath=.F.
    Else
        * --- Se file compreso di path assoluto non ricerco sul path
        bNoSetPath=bNoSetPath Or Not Empty(Justdrive(i_cName))
    Endif

    If bNoSetPath
        If Vartype(i_initvfpfileexist)='U'
            Public i_initvfpfileexist
            i_initvfpfileexist=.T.
            Declare Integer VFPFileExist In VFPFileExist.Dll String filename
        Endif
        Return(VFPFileExist( i_cName )=1)
    Else
        Return( File( i_cName ) )
    Endif

Procedure Func_SetCCCHKVarsInsert(i_ccchkf,i_ccchkv,i_nTblIdx,i_nConn)
    Local i_NF
    If i_TableProp[i_nTblIdx,7]=1
        i_ccchkf=',CPCCCHK'
        i_ccchkv=','+cp_ToStrODBC(cp_NewCCChk())
    Else
        i_ccchkf=''
        i_ccchkv=''
    Endif
    *ACTIVATE SCREEN
    *? '-->',i_ccchkf,i_ccchkv
    Return

Procedure Func_SetCCCHKVarsWrite(i_ccchkf,i_nTblIdx,i_nConn)
    Local i_NF
    If i_TableProp[i_nTblIdx,7]=1
        i_ccchkf=',CPCCCHK='+cp_ToStrODBC(cp_NewCCChk())
    Else
        i_ccchkf=''
    Endif
    *ACTIVATE SCREEN
    *? '-->',i_ccchkf
    Return

    *--- Interfaccia - Font
    *--- Sceglie il font in base al contesto
    *--- Sceglie la dimensione del font in base al contesto
Procedure SetFont(pType, oObj)
    Do Case
            *--- L Label, R Radio, X Check, H GrdHeader
        Case Inlist(pType, "L", "R", "H", "X")
            oObj.FontName = i_cLblFontName
            oObj.FontSize = i_nLblFontSize
            oObj.FontItalic = i_bLblFontItalic
            oObj.FontBold = i_bLblFontBold
            *--- C ComboBox
        Case pType = "C"
            oObj.FontName = i_cCboxFontName
            oObj.FontSize = i_nCboxFontSize
            oObj.FontItalic = i_bCboxFontItalic
            oObj.FontBold = i_bCboxFontBold
            *--- P Page
        Case pType = "P"
            oObj.FontName = i_cPageFontName
            oObj.FontSize = i_nPageFontSize
            oObj.FontItalic = i_bPageFontItalic
            oObj.FontBold = i_bPageFontBold
            *--- B Button
        Case pType = "B"
            oObj.FontName = i_cBtnFontName
            oObj.FontSize = i_nBtnFontSize
            oObj.FontItalic = i_bBtnFontItalic
            oObj.FontBold = i_bBtnFontBold
            *--- G Grid
        Case pType = "G"
            oObj.FontName = i_cGrdFontName
            oObj.FontSize = i_nGrdFontSize
            oObj.FontItalic = i_bGrdFontItalic
            oObj.FontBold = i_bGrdFontBold
            *--- T Text, M Memo
        Case Inlist(pType, "T", "M")
            *--- Zucchetti Aulla - non serve perch� gi� fatto assegnamento
            oObj.FontName = i_cFontName
            oObj.FontSize = i_nFontSize
            oObj.FontItalic = i_bFontItalic
            oObj.FontBold = i_bFontBold
    Endcase
Endproc

*--- Interfaccia - Font
*--- Sceglie il font in base al contesto
Function SetFontName(pType, pFontName, bGlobalFontName)
    Local l_FontName
    *--- Se bGlobalFontName forzo il font name del controllo, non applico la configurazione interfaccia
    If !m.bGlobalFontName
        Return m.pFontName
    Endif
    l_FontName = m.i_cFontName
    Do Case
            *--- L Label, R Radio, X Check, H GrdHeader
        Case Inlist(pType, "L", "R", "H", "X")
            l_FontName = i_cLblFontName
            *--- C ComboBox
        Case pType = "C"
            l_FontName = i_cCboxFontName
            *--- P Page
        Case pType = "P"
            l_FontName = i_cPageFontName
            *--- B Button
        Case pType = "B"
            l_FontName = i_cBtnFontName
            *--- G Grid
        Case pType = "G"
            l_FontName = i_cGrdFontName
            *--- T Text, M Memo --- Zucchetti Aulla - non serve perch� gi� fatto assegnamento
        Case .F. And Inlist(pType, "T", "M")
            l_FontName = i_cFontName
    Endcase
    Return l_FontName

    *--- Sceglie la dimensione del font in base al contesto
Function SetFontSize(pType, pFontSize, bGlobalFontSize)
    *--- Se bGlobalFontSize forzo il font size del controllo, non applico la configurazione interfaccia
    If !m.bGlobalFontSize
        Return m.pFontSize
    Endif
    Local l_FontSize, l_AppFontSize
    l_FontSize = m.pFontSize
    *--- Se il font � lo stesso proporziono la taglia
    Do Case
            *--- L Label, R Radio, X Check, H GrdHeader
        Case Inlist(pType, "L", "R", "H", "X")
            l_FontSize = i_nLblFontSize
            *--- C ComboBox
        Case pType = "C"
            l_FontSize = i_nCboxFontSize
            *--- P Page
        Case pType = "P"
            l_FontSize = i_nPageFontSize
            *--- B Button
        Case pType = "B"
            l_FontSize = i_nBtnFontSize
            *--- G Grid
        Case pType = "G"
            l_FontSize = i_nGrdFontSize
            *--- T Text, M Memo
        Case Inlist(pType, "T", "M")
            l_FontSize = i_nFontSize
    Endcase
    Return l_FontSize

Function SetFontItalic(pType, pFontItalic, bGlobalFont)
    *--- Se bGlobalFont forzo il font italic del controllo, non applico la configurazione interfaccia
    If !m.bGlobalFont
        Return m.pFontItalic
    Endif
    Local l_FontItalic
    l_FontItalic = m.pFontItalic
    Do Case
            *--- L Label, R Radio, X Check, H GrdHeader
        Case Inlist(pType, "L", "R", "H", "X")
            l_FontItalic = i_bLblFontItalic
            *--- C ComboBox
        Case pType = "C"
            l_FontItalic = i_bCboxFontItalic
            *--- P Page
        Case pType = "P"
            l_FontItalic = i_bPageFontItalic
            *--- B Button
        Case pType = "B"
            l_FontItalic = i_bBtnFontItalic
            *--- G Grid
        Case pType = "G"
            l_FontItalic = i_bGrdFontItalic
            *--- T Text, M Memo
        Case Inlist(pType, "T", "M")
            l_FontItalic = i_bFontItalic
    Endcase
    Return l_FontItalic

Function SetFontBold(pType, pFontBold, bGlobalFont)
    *--- Se bGlobalFont forzo il font bold del controllo, non applico la configurazione interfaccia
    If !m.bGlobalFont
        Return m.pFontBold
    Endif
    Local l_FontBold
    l_FontBold = m.pFontBold
    Do Case
            *--- L Label, R Radio, X Check, H GrdHeader
        Case Inlist(pType, "L", "R", "H", "X")
            l_FontBold = i_bLblFontBold
            *--- C ComboBox
        Case pType = "C"
            l_FontBold = i_bCboxFontBold
            *--- P Page
        Case pType = "P"
            l_FontBold = i_bPageFontBold
            *--- B Button
        Case pType = "B"
            l_FontBold = i_bBtnFontBold
            *--- G Grid
        Case pType = "G"
            l_FontBold = i_bGrdFontBold
            *--- T Text, M Memo
        Case Inlist(pType, "T", "M")
            l_FontBold = i_bFontBold
    Endcase
    Return l_FontBold


    *--- Verifica font per configurazione interfaccia
Function TestFont(pType, pFontName, pFontSize)
    If !Afont(afnt, pFontName, pFontSize) Or pFontSize > 127 Or pFontSize < 4
        Return -1
    Else
        Local l_AppFontSize, l_AppFontName
        l_AppFontName = i_cProjectFontName
        l_AppFontSize = i_nProjectFontSize
        l_AppLblWidth = cp_LabelWidth2("ABCDEFGHILMNOPQRSTUVZ1234567890", l_AppFontName, l_AppFontSize)
        l_LblWidth = cp_LabelWidth2("ABCDEFGHILMNOPQRSTUVZ1234567890", pFontName, pFontSize)
        If l_AppLblWidth + (l_AppLblWidth*i_nThresholdFontSize/100) < l_LblWidth
            Return 0	&&Rispetta la soglia
        Else
            Return 1	&&Warning non rispetta la soglia
        Endif
    Endif
Endfunc

Function cp_LabelWidth(cFontName, nFontSize, ctext)
    Local l_Form, l_ret, l_OldErr
    l_ret = 0
    l_Form = Createobject("Form")
    l_Form.AddObject("Lbl", "Label")
    l_Form.lbl.AutoSize = .T.
    l_Form.lbl.Caption = ctext
    If Vartype(m.cFontName)=='C' And !Empty(m.cFontName)
    	l_Form.lbl.FontName = cFontName
    Endif
    l_OldErr = On("Error")
    On Error l_ret=10000
    If Vartype(m.nFontSize)=='N' And m.nFontSize>0
    	l_Form.lbl.FontSize = nFontSize
    Endif
    If l_ret=10000
        cp_Msg(MSG_WRONG_FONT_DIM)
    Else
        l_Form.lbl.Visible=.T.
        l_ret = l_Form.lbl.Width
    Endif
    On Error &l_OldErr
    l_Form.Release()
    l_Form = .Null.
    Return l_ret
Endfunc

FUNCTION cp_LabelWidth2(tcString, tcFont, tnSize, tcStyle)
	* Determines the width in pixels of a given text string, based on
	* a given font, font style and point size.

	* Parameters: text string, font name, size in points, font style in
	* format used by FONTMETRIC() (e.g. "B" for bold, "BI" for bold italic;
	* defaults to normal).
	LOCAL lnTextWidth, lnAvCharWidth
	IF EMPTY(tcStyle)
	    tcStyle = ""
	ENDIF
	lnTextWidth = TXTWIDTH(tcString, tcFont, tnSize, tcStyle)
	lnAvCharWidth = FONTMETRIC(6, tcFont, tnSize, tcStyle)
	RETURN Int(lnTextWidth * lnAvCharWidth)+1
ENDFUNC

FUNCTION cp_GetTextSize(nWidth, nHeight, cText, cFontName, nFontSize, cFontStyle, nWordWrapWidth)
*--- Determina larghezza e altezza di una label che possa contenere il testo cText
*--- In caso nWordWrapWidth#0 utilizza il metodo "numero minimo di righe"
*--- Larghezza ed altezza devono essere passate per riferimento
*--- Esempio:
*--- Local nWidth, nHeight
*--- nWidth=0
*--- nHeight=0
*--- cp_GetTextSize(@nWidth, @nHeight, "Questa � una prova", "Tahoma", 10, "BI", 120)
*--- ?nWidth, nHeight	&&88, 34
	IF EVL(m.nWordWrapWidth, 0)=0
		nWidth = ROUND(cp_LabelWidth2(m.cText, m.cFontName, m.nFontSize, m.cFontStyle),0) + 2
		nHeight = FONTMETRIC(1, m.cFontName, m.nFontSize, m.cFontStyle) + 2
	ELSE
	*--- Word wrap - "numero minimo di righe"
		LOCAL nSpaceLeft, nSpaceWidth, nWord, nIndex, nWordWidth, nNumLine, cTextLine
		nNumLine = 1
		nSpaceWidth = cp_LabelWidth2(" ", m.cFontName, m.nFontSize, m.cFontStyle)
		nWord = ALINES(aWord, m.cText, 1, " ")
		nWidth = 0
		cTextLine = aWord[1]
		nSpaceLeft = m.nWordWrapWidth - (cp_LabelWidth2(m.cTextLine, m.cFontName, m.nFontSize, m.cFontStyle)+m.nSpaceWidth)
		FOR nIndex=2 TO m.nWord
			nWordWidth = cp_LabelWidth2(aWord[m.nIndex], m.cFontName, m.nFontSize, m.cFontStyle)
			IF (m.nWordWidth+m.nSpaceWidth) > m.nSpaceLeft
				*--- word wrap
				nWidth = MAX(m.nWidth, cp_LabelWidth2(m.cTextLine, m.cFontName, m.nFontSize, m.cFontStyle) + 2)
				cTextLine = aWord[m.nIndex]
				nNumLine = m.nNumLine + 1 &&Aggiungo una riga
				nSpaceLeft = m.nWordWrapWidth - m.nWordWidth
			ELSE
				nSpaceLeft = m.nSpaceLeft - (m.nWordWidth+m.nSpaceWidth)
				cTextLine = m.cTextLine + " " + aWord[m.nIndex]
			EndIF
		NEXT
		nHeight = FONTMETRIC(1, m.cFontName, m.nFontSize, m.cFontStyle)*m.nNumLine + 2
		nWidth = EVL(ROUND(m.nWidth,0), cp_LabelWidth2(m.cTextLine, m.cFontName, m.nFontSize, m.cFontStyle) + 2)
	EndIf
	RETURN .T.
ENDFUNC

Function SearchButton(oForm, nPag, aBtnList, bNoChildSearch, nItem, cKey)
    *--- Cerco bottoni sulla maschera
    *--- Valorizza array passato per riferimento con la lista dei bottoni visibili
    *--- 	aBtnList[n] = oBtn
    *--- Ritorna Il bottone che ha come tasto mnemonico quello indicato in cKey
    *--- Parametri :
    *--- 	oForm = Gestione/Container sul quale ricercare i bottoni
    *--- 	nPag = Numero pagina nella quale cercare, se 0 cerca su tutte le pagine, -1 cerca sulla activepage
    *--- 	aBtnList = Array passato per riferimento
    *--- 	bNoChildSearch = Cerco nei figli integrati?
    *--- 	nItem = Numero elementi array
    *--- 	cKey = Tasto mnemonico da ricercare
    If !Isnull(m.oForm) And (Vartype(m.oForm.oPgFrm)='O' Or Type("m.oForm.cnt.Opgfrm")='O')
        Local l_i, l_j, l_nPagStart, l_nPagEnd, l_oRet, l_nItem, l_oForm
        l_oRet = .Null.
        l_oForm = Iif(Type("m.oForm.cnt.oPgFrm")='O', m.oForm.Cnt, m.oForm)
        *--- Dimensione array, se non sto cercando in un figlio e la lunghezza � uno vuole dire che l'array � vuoto
        l_nItem = Iif(Vartype(m.nItem)='L' Or m.nItem<0, 0, m.nItem)
        m.l_nPagZoom = - 1
        Do Case
            Case m.nPag = 0
                *--- Cerca in tutte le pagine
                l_nPagStart = 1
                l_nPagEnd = m.l_oForm.oPgFrm.PageCount
                *--- Cerco la pagina di elenco
                If Upper(m.oForm.BaseClass) = "FORM"
                    l_nPagZoom = l_nPagEnd
                    Do While Type('m.oForm.Opgfrm.pages(m.l_nPagZoom).autozoom')='U' And m.l_nPagZoom>0
                        l_nPagZoom = m.l_nPagZoom - 1
                    Enddo
                Endif	&& !bNoFirst
            Case m.nPag = -1
                *--- Cerca nella pagina attiva ActivePage
                l_nPagStart = m.l_oForm.oPgFrm.ActivePage
                l_nPagEnd = m.l_nPagStart
            Otherwise
                *--- Cerca sulla pagina indicata
                l_nPagStart = nPag
                l_nPagEnd = nPag
        Endcase
        For l_i = m.l_nPagStart To m.l_nPagEnd
            If m.l_i<>m.l_nPagZoom
                If Type("m.l_oForm.oPgFrm.Pages(m.l_i).oPag")="O" &&Testo solo le pagine che hanno opag (es, Allegati ed Elenco devono essere saltate)
                    With m.l_oForm.oPgFrm.Pages(m.l_i).oPag
                        For l_j = 1 To .ControlCount
                            Do Case
                                Case (Upper(.Controls(l_j).Class) = 'STDBUTTON' Or Upper(.Controls(l_j).Class) = 'BUTTONMENU'  Or Upper(.Controls(l_j).Class) ='ADVBUTTONMENU') And .Controls(l_j).Visible
                                    *--- Bottone o ParentChild
                                    If Vartype(m.cKey)='C' And !Empty(m.cKey) And .Controls(l_j).Enabled And At("\<"+Upper(m.cKey), Upper(.Controls(l_j).Caption))<>0
                                        Return .Controls(l_j)
                                    Endif
                                    m.l_nItem = m.l_nItem+1
                                    Dimension aBtnList[m.l_nItem]	&&Aumento la dimensione dell'array
                                    aBtnList[m.l_nItem] = .Controls(l_j)
                                Case !bNoChildSearch And (Upper(.Controls(l_j).Class) = 'STDDYNAMICCHILDCONTAINER' Or Upper(.Controls(l_j).Class) ='ADVBUTTONMENU') And .Controls(l_j).Visible And Vartype(.Controls(l_j).Child)='O' And !Isnull(.Controls(l_j).Child)
                                    *--- Figlio integrato
                                    l_oRet = SearchButton(.Controls(l_j).Child, Iif(m.nPag<>0, .Controls(l_j).Child.oPgFrm.ActivePage, 0), @aBtnList, m.bNoChildSearch, m.l_nItem, m.cKey)
                            Endcase
                            *--- Se ho trovato un bottone che contiene il tasto mnemonico ritorno subito
                            If !Isnull(m.l_oRet)
                                Return m.l_oRet
                            Endif
                        Next	&& l_j = 1 TO m.l_oForm.oPgFrm.Pages(m.l_i).oPag.ControlCount
                    Endwith
                Endif	&&TYPE("m.l_oForm.oPgFrm.Pages(m.l_i).oPag")="O"
            Endif	&& m.l_i<>m.l_nPagZoom
        Next	&& l_i = l_nPagStart TO l_nPagEnd
    Endif	&& !ISNULL(m.l_oForm) AND VARTYPE(m.l_oForm.Opgfrm)='O'
    Return .Null.
Endfunc

Function cp_AppQueryFilter(i_procname,i_tablename,i_alias)
    cp_LoadRuntimeUserFilters()
    Local i_NT,i_res
    i_res=""
    i_NT=cp_TablePropScan(i_tablename)
    If i_NT<>0 And (Vartype(i_TablePropSec[i_NT,2])='C' Or i_TablePropSec[i_NT,4]=.T.)
        If i_TablePropSec[i_NT,4] And Vartype(i_procname)='C'
            *** esiste una regola per procedura
            i_res=cp_ProcQueryFilter(i_procname,i_tablename)
        Endif
        If Vartype(i_TablePropSec[i_NT,2])='C' And Lower(Right(i_TablePropSec[i_NT,2],4))='.vqr'
            *** la query non � stata ancora valutata
            cp_SetAzi(i_tablename,.T.,i_NT)
        Endif
        i_res=Iif(Empty(i_res),Iif(Vartype(i_TablePropSec[i_NT,2])='C',i_TablePropSec[i_NT,2],''),i_res)
    Endif
    Return i_res

Function cp_ProcQueryFilter(i_procname,i_tablename,i_bTable)
    Local i_res,i_findrec,i_pos,i_nTablePos,i_cAliasTable,i_cPrimaryKey,i_cWhereKey,i_cVir,i_nNumbArray
    i_res=''
    If Type("i_TablePropSecProc")='U'
        * --- Prima apertura popolo l'array di sistema con tutte le sicurezze...
        Public i_TablePropSecProc
        Dimension i_TablePropSecProc[1,5]
        vq_exec("CP_SECRECTAB2.VQR",,"_Curs_secrectab")
        If Used("_Curs_secrectab") And Reccount("_Curs_secrectab")>0
            i_nNumbArray=Reccount("_Curs_secrectab")
            Dimension i_TablePropSecProc[i_nNumbArray,5]
            Select tablename,progname,QueryFilter,QueryFilter,.F. As flagload From _Curs_secrectab Into Array i_TablePropSecProcTemp
            =Acopy(i_TablePropSecProcTemp,i_TablePropSecProc)
            Select _Curs_secrectab
            Use
        Endif
    Endif
    * --- Ricerco la sicurezza del record per la tabella...
    i_pos=1
    i_findrec=.F.
    Do While Not i_findrec And i_pos<>0

        #If Version(5)>=900
            i_pos=Ascan(i_TablePropSecProc, i_tablename, i_pos, -1, 1, 15)
        #Else
            Local i_i, nIdx
            nIdx=0
            For i_i=i_pos To Alen(i_TablePropSecProc,1)
                If Lower(i_TablePropSecProc[i_i,1])==Lower(i_tablename)
                    nIdx=i_i
                    Exit
                Endif
            Next
            i_pos=nIdx
        #Endif


        If i_pos<>0
            i_findrec=Lower(i_procname)$Lower(Alltrim(i_TablePropSecProc[i_pos,2])) Or Lower(Alltrim(i_TablePropSecProc[i_pos,2]))$Lower(i_procname)
            If Not i_findrec
                i_pos=i_pos+1
            Endif
        Endif
    Enddo
    If i_findrec
        i_res=Iif(Not i_bTable, Alltrim(Iif(i_findrec,i_TablePropSecProc[i_pos,3],'')), Alltrim(Iif(i_findrec,i_TablePropSecProc[i_pos,4],'')))
        i_nTablePos=cp_TablePropScan(i_tablename)
        If Not i_TablePropSecProc[i_pos,5]
            If Lower(Right(i_res,4))='.vqr'
                *** la query fornisce direttamente i record visibili dall' utente
                Private i_runtime_filters
                i_runtime_filters=2
                i_TablePropSecProc[i_pos,4]="("+cp_GetSQLFromQuery(i_res)+")"
                *** la condizione da mettere nel "QueryFilter" in questo caso va costruita come una exist
                i_cPrimaryKey=cp_KeyToSQL(i_Dcx.GetIdxDef(i_TableProp[i_nTablePos,1],1))
                i_cAliasTable=Upper(cp_NewCCChk())
                i_cWhereKey=''
                i_cVir=At(',',i_cPrimaryKey)
                Do While i_cVir>0
                    i_cWhereKey=i_cWhereKey+i_cAliasTable+'.'+Left(i_cPrimaryKey, i_cVir-1)+'='+Alltrim(i_TableProp[i_nTablePos,1])+'.'+Left(i_cPrimaryKey, i_cVir-1)+' AND '
                    i_cPrimaryKey=Right(i_cPrimaryKey, Len(i_cPrimaryKey)-i_cVir)
                    i_cVir=At(',',i_cPrimaryKey)
                Enddo
                i_cWhereKey=i_cWhereKey+i_cAliasTable+'.'+i_cPrimaryKey+'='+Alltrim(i_TableProp[i_nTablePos,1])+'.'+i_cPrimaryKey
                i_TablePropSecProc[i_pos,3]=" exists (select 1 from ("+i_TablePropSecProc[i_pos,4]+") "+i_cAliasTable+" where "+i_cWhereKey+")"
                i_res=Iif(Not i_bTable, Alltrim(Iif(i_findrec,i_TablePropSecProc[i_pos,3],'')), Alltrim(Iif(i_findrec,i_TablePropSecProc[i_pos,4],'')))
            Else
                Do cp_SetSQLFunc In cp_query With i_res, CP_DBTYPE
                *** la select con i dati visibili all' utente
                i_TablePropSecProc[i_pos,4]="(select * from "+Trim(Strtran(i_TableProp[i_nTablePos,2],'xxx',Trim(i_CODAZI)))+" "+i_TableProp[i_nTablePos,1]+" where "+i_res+")"
                *** la condizione � gi� il "QueryFilter"
                i_TablePropSecProc[i_pos,3]=i_res
                i_res=Iif(Not i_bTable, i_TablePropSecProc[i_pos,3], i_TablePropSecProc[i_pos,4])
            Endif
            i_TablePropSecProc[i_pos,5]=.T.
        Endif
    Endif
    Return i_res

Function cp_GetSQLFromQuery(i_queryname,i_nofilter)
    Local i_qry,i_loader,i_sql,i_nofilters
    If At('cp_query',Lower(Set('proc')))=0
        Set Proc To cp_query Additive
    Endif
    i_nofilters=Createobject('noqueryfilters')
    i_qry=Createobject('cpquery')
    i_loader=Createobject('cpqueryloader')
    i_loader.LoadQuery(Left(i_queryname,Len(i_queryname)-4),i_qry,i_nofilters, .T. ) && le sub query non devono avere ordinamento...
    i_sql=i_qry.mDoQuery('nocursor','Get',.Null.)
    Return i_sql

Define Class noqueryfilters As Custom
    runtime_filters=2
Enddefine

Function cp_TransLoadField(i_FldName)
    Local i_result, i_transName
    i_transName=i_FldName
    If Type(i_transName)='C' And Not Empty(i_cLanguageData)
        i_transName=i_FldName+'_'+i_cLanguageData
        i_result=&i_transName
        If Isnull(i_result)
            i_result=&i_FldName
        Endif
    Else
        i_result=&i_FldName
    Endif
    Return i_result

Function cp_TransInsFldName(i_FldName)
    Local i_result
    i_result=''
    If Not Empty(i_cLanguageData)
        i_result=','+i_FldName+'_'+i_cLanguageData
    Endif
    Return i_result

Function cp_TransInsFldValue(i_FldValueData)
    Local i_result
    i_result=''
    If Not Empty(i_cLanguageData)
        i_result=','+cp_ToStrODBCNull(i_FldValueData)
    Endif
    Return i_result

Function cp_TransUpdFldName(i_FldName,i_FldValue)
    Local i_result
    If Not Empty(i_cLanguageData)
        i_result='COALESCE('+i_FldName+','+cp_ToStrODBCNull(i_FldValue)+'), '+i_FldName+'_'+i_cLanguageData+'='+cp_ToStrODBCNull(i_FldValue)
    Else
        i_result=cp_ToStrODBC(i_FldValue)
    Endif
    Return i_result

Function cp_TransLinkFldName(i_FldName)
    Local i_result
    i_result=i_FldName
    If Not Empty(i_cLanguageData)
        i_result='COALESCE('+i_FldName+'_'+i_cLanguageData+','+i_FldName+')'
    Endif
    Return i_result

Function cp_TransWhereFldName(i_FldName,i_FldValue)
    Local i_result
    i_result=''
    If Not Empty(i_cLanguageData)
        i_result=" OR "+i_FldName+"_"+i_cLanguageData+" LIKE "+cp_ToStrODBC(i_FldValue+'%')
    Endif
    Return i_result

    * --- move to a particolar position of a local table..
Proc cp_GoTo(i_nRec, cAlias)
    Local nRec, nOldarea
    If Empty(cAlias)
        nRec=Reccount()
    Else
        nRec=Reccount(cAlias)
        nOldarea=Select()
        Select (cAlias)
    Endif
    If i_nRec<=nRec
        Goto i_nRec
    Else
        Go Bottom
        If Not Eof(cAlias)
            Skip
        Endif
    Endif
    If Not Empty(cAlias)
        Select(nOldarea)
    Endif
    Return

    * --- Crea cartelle, crea ricorsivamente le cartelle se non esistono, in caso di errore ritorna False
Function cp_makedir(pPath)
    Private w_NUMPATH
    w_NUMPATH=0
    Private w_LOOP
    w_LOOP=0
    Private w_PATH
    w_PATH=Space(254)
    pPath = Alltrim(m.pPath)
    #If Version(5)>=900
        w_NUMPATH = Alines(aPath, m.pPath,1,"\")
    #Else
        Local ctext
        * --- Sostituisco le occorrenze di "\" con Chr(13)+Chr(10)
        ctext=Strtran(m.pPath , "\", Chr(13)+Chr(10))
        w_NUMPATH =  Alines(aPath, ctext)
    #Endif
    Local l_OldErr, L_bErr
    l_OldErr= On("Error")
    L_bErr=.F.
    On Error L_bErr=.T.
    * --- Ciclo su
    If  "\\" $ m.pPath
        * --- Network
        w_LOOP = 4
        w_PATH = "\\"+aPath[3] + "\"
    Else
        * --- FileSystem
        w_LOOP = 1
        w_PATH = ""
    Endif
    Do While m.w_LOOP <= m.w_NUMPATH
        w_PATH = m.w_PATH + aPath[m.w_LOOP] + "\"
        #If Version(5)>=900
            If ! Directory(m.w_PATH,1)
                * --- La cartella non esiste, provo a crearla
                Md (m.w_PATH)
            Endif
        #Else
            * --- x le versioni precedenti non verifico la presenza di cartelle nascoste
            If ! Directory(m.w_PATH)
                * --- La cartella non esiste, provo a crearla
                Md (m.w_PATH)
            Endif
        #Endif
        If L_bErr
            Exit
        Endif
        w_LOOP = m.w_LOOP + 1
    Enddo
    * --- Ripristino errore

    On Error &l_OldErr
    * --- Ritorno l'esisto dell'operazione
    Return !L_bErr

    *--- Programmatically Register, unregister an OCX control
    *--- In caso di errore ritorna il messaggio di errore altrimenti ""
Function cp_RegSvr(pcOCX,plReg)
    Local llReg, lpRegProc
    llReg = Iif( Pcount()>1, m.plReg, .T. ) && Default to register

    Local lhLib
    lhLib = LoadLibraryA( m.pcOCX )
    If (m.lhLib < 32) && HINSTANCE_ERROR
        Return cp_msgformat(cp_Translate(MSG_ERROR_COULD_NOT_LOAD_LIBRARY__), m.pcOCX)
    Endif

    *--- Find the entry point.
    If m.llReg	&&Register
        lpRegProc = GetProcAddress(lhLib, 'DllRegisterServer')
        If lpRegProc>0 && Procedure exists!
            Declare Integer DllRegisterServer In &pcOCX As OcxReg
            lnRes = OcxReg()
            If lnRes=0
                Return ""
            Else
                Return cp_msgformat(cp_Translate(MSG_ERROR_DLLREGISTERSERVER_RETURNED__), Tran(lnRes))
            Endif
        Else
            *--- unable to locate entry point
            Return cp_msgformat(cp_Translate(MSG_ERROR_LIBRARY___HAS_NO_ENTRY_POINT_FOR_DLLREGISTERSERVER), m.pcOCX)
        Endif
    Else && Unregister
        lpRegProc = GetProcAddress(lhLib, 'DllUnregisterServer')
        If lpRegProc>0 && Procedure exists!
            Declare Integer DllUnregisterServer In &pcOCX As OcxUnReg
            lnRes = OcxUnReg()
            If lnRes=0
                Return ""
            Else
                Return cp_msgformat(cp_Translate(MSG_ERROR_DLLUNREGISTERSERVER_RETURNED__), Tran(lnRes))
            Endif
        Else
            *--- unable to locate entry point
            Return cp_msgformat(cp_Translate(MSG_ERROR_LIBRARY___HAS_NO_ENTRY_POINT_FOR_DLLUNREGISTERSERVER), m.pcOCX)
        Endif
    Endif
Endfunc

Define Class _OSource As Custom
    Dimension xKey[15]
Enddefine

* --- Setta il valore di una propriet� ed eseue il Link
*     pTipo indica se :
*                      Master o Detail
*                      Full o Part link, T esegue sia la Drop che il link ti tipo Full
*     pObj oggetto su cui valorizzare i control
*     pCtrlName Nome del control
*     pCtrlValue Valore del control
*     pKeyFixed(i) Chiave fissa per link
* --- Esegue la valorizzazione
Func setvaluelinked
    Param pTipo,pObj,pCtrlName,pCtrlValue,pKeyFixed1,pKeyFixed2,pKeyFixed3
    Private w_LINKTYPE,w_LINKNAME,w_OLDPOSTIN,w_OLDPOSTIN,w_NUMMARK,w_OBJECT,w_OBJCTRL,w_OSOURCE,w_OBJECT
    w_numparam=Parameters()
    w_OBJECT = m.pObj
    If Left(m.pTipo, 1) = "M"
        w_OBJCTRL = m.w_OBJECT.getctrl( m.pCtrlName )
    Else
        w_OBJCTRL = m.w_OBJECT.GetBodyCtrl( m.pCtrlName )
    Endif
    w_LINKTYPE = Iif(Len(Alltrim(m.pTipo))<2, "", Iif( Right(Alltrim(m.pTipo),1)="F", "F", Iif( Right(Alltrim(m.pTipo),1)="P", "P", Iif(Right(Alltrim(m.pTipo),1)="T", "T", "" )) ))
    If Vartype( m.w_OBJCTRL )="O"
        If Empty(m.w_LINKTYPE) Or m.w_LINKTYPE="T"
            * --- Disabilito gli avvisi delle valid...
            *     Questo asssegnamento � necesario in quanto sotto alcune condizioni
            *     scatta il msg campo obbligatorio sul magazzino x articoli forfettari...
            *     (non serve re impostarlo messo a .f. al termine della Valid..)
            If Vartype(g_DEBUGMODE)="N" And g_DEBUGMODE=2

                cp_DBG("Pre  "+ m.pCtrlName)
                cp_DBG( m.pCtrlValue )
            Endif
            w_OBJECT.bDontReportError = .T.
            * --- Valorizzo il Control..
            *     Se il controllo ha un link utilizzo la EcpDrop per evitare di eseguire il link
            *     con query  sull'archivio filtrata tramite operatore Like (Problema dei codice con _ o % all'interno) .
            *
            *     Se non ha un Link utilizzo
            *     La Valid (il check non interroga il database)
            If Not Empty( m.w_OBJCTRL.cLinkFile )
                w_OSOURCE = Createobject("_oSource")
                If w_numparam<5
                    w_OSOURCE.xKey( 1 ) = m.pCtrlValue
                Else
                    w_OSOURCE.xKey( 1 ) = m.pKeyFixed1
                    If w_numparam<6
                        w_OSOURCE.xKey( 2 ) = m.pCtrlValue
                    Else
                        w_OSOURCE.xKey( 2 ) = m.pKeyFixed2
                        If w_numparam<7
                            w_OSOURCE.xKey( 3 ) = m.pCtrlValue
                        Else
                            w_OSOURCE.xKey( 3 ) = m.pKeyFixed3
                            w_OSOURCE.xKey( 4 ) = m.pCtrlValue
                        Endif
                    Endif
                Endif
                w_OLDPOSTIN = i_ServerConn[1,7]
                i_ServerConn[1,7]=.F.
                If Vartype(m.w_OBJECT.nMarkpos) = "N"
                    *
                    w_NUMMARK = m.w_OBJECT.nMarkpos
                    w_OBJECT.nMarkpos = 0
                Endif
                w_OBJCTRL.ecpDrop(m.w_OSOURCE)
                w_OBJECT.SaveDependsOn()
                If Vartype(m.w_OBJECT.nMarkpos) = "N"
                    w_OBJECT.nMarkpos = m.w_NUMMARK
                Endif
                i_ServerConn[1,7]= m.w_OLDPOSTIN
            Else
                If Inlist( Lower(m.w_OBJCTRL.Class), "stdcombo", "stdcheck", "stdradio", "stdtablecombo")
                    Local w_cformvar
                    w_cformvar="w_OBJCTRL.parent.ocontained."+m.w_OBJCTRL.cFormVar
                    &w_cformvar = m.pCtrlValue
                    w_OBJCTRL.SetRadio()
                Else
                    w_OBJCTRL.Value = m.pCtrlValue
                Endif
                * --- Considero il control sempre modificato per gestire i valori da proporre
                *     (se da penna imposto un valore uguale a quello da proporre la procedura
                *     non fa scattare vari eventi..)
                w_OBJCTRL.bUpd = .T.
                * --- Valid sul campo, esegue Check, NotifyEvent Changed, mCalc e SaveDependsOn
                If Vartype(m.w_OBJECT.nMarkpos) = "N"
                    w_NUMMARK = m.w_OBJECT.nMarkpos
                    w_OBJECT.nMarkpos = 0
                    w_OBJCTRL.Valid()
                    w_OBJECT.nMarkpos = m.w_NUMMARK
                Else
                    w_OBJCTRL.Valid()
                Endif
            Endif
            If Vartype(g_DEBUGMODE)="N" And g_DEBUGMODE=2
                cp_DBG("Post  "+ m.pCtrlName)
            Endif
            If m.w_LINKTYPE<>"T"
                Return(.T.)
            Endif
        Endif
        If !Empty(m.w_LINKTYPE)
            w_LINKNAME = Right(Alltrim(m.w_OBJCTRL.Name), Len(Alltrim(m.w_OBJCTRL.Name))-Rat("_",Alltrim(m.w_OBJCTRL.Name),2))
            l_Macro = "w_OBJECT.Link_"+ m.w_LINKNAME +"('"+Iif(m.w_LINKTYPE $ "F-T", "Full", "Part") + "')"
            &l_Macro
            Release l_Macro
            Return(.T.)
        Endif
    Else
        Return(.F.)
    Endif
    Return(.T.)

    *--- cp_GetCompanyList
    *--- Restituisce una stringa con l'elenco delle aziende separate da virgola
Function cp_GetCompanyList()
    Local l_tmp_azi, l_oldArea, l_ret
    l_ret = ''
    *--- Salvo la vecchia area di lavoro
    l_oldArea = Select()
    l_tmp_azi = Sys(2015)
    If i_ServerConn[1,2]<>0
        cp_sqlexec(i_ServerConn[1,2],"select * from cpazi", l_tmp_azi)
    Else
        Select * From cpazi Into Cursor &l_tmp_azi nofilter
    Endif
    Select(l_tmp_azi)
    Scan
        l_ret = l_ret + Alltrim(&l_tmp_azi..codazi) + ","
        Select(l_tmp_azi)
    Endscan
    *--- Elimino ultima virgola
    l_ret = Left(l_ret, Len(l_ret)-1)
    *--- Ripristino l'area di lavoro
    Select(l_oldArea)
    Return l_ret
Endfunc

*--- Cp_GetFont: Enhanced GetFont dialog
*--- Parametro aInfoFont utilizzato per inizializzare la getfont
*--- aInfoFont sar� modificato con le info sul font
*--- aInfoFont[1] = FontName
*--- aInfoFont[2] = FontSize
*--- aInfoFont[3] = FontColor
*--- aInfoFont[4] = FontBold
*--- aInfoFont[5] = FontItalic
*--- aInfoFont[6] = FontUnderline
*--- aInfoFont[7] = FontStrikeout
*--- Ritorna True se � stato scelto un font
Function cp_GetFont(aInfoFont)

    #Define GMEM_FIXED                 0
    #Define LF_FACESIZE               32
    #Define FW_NORMAL                400
    #Define FW_BOLD                	 700
    #Define DEFAULT_CHARSET            1
    #Define OUT_DEFAULT_PRECIS         0
    #Define CLIP_DEFAULT_PRECIS        0
    #Define DEFAULT_QUALITY            0
    #Define DEFAULT_PITCH              0
    #Define CF_SCREENFONTS             1
    #Define CF_INITTOLOGFONTSTRUCT    64
    #Define CF_EFFECTS               256
    #Define CF_FORCEFONTEXIST      65536

    *| typedef struct {
    *|   DWORD        lStructSize;     4
    *|   HWND         hwndOwner;       4
    *|   HDC          hDC;             4
    *|   LPLOGFONT    lpLogFont;       4
    *|   INT          iPointSize;      4
    *|   DWORD        Flags;           4
    *|   COLORREF     rgbColors;       4
    *|   LPARAM       lCustData;       4
    *|   LPCFHOOKPROC lpfnHook;        4
    *|   LPCTSTR      lpTemplateName;  4
    *|   HINSTANCE    hInstance;       4
    *|   LPTSTR       lpszStyle;       4
    *|   WORD         nFontType;       2
    *|   WORD         ___MISSING_ALIGNMENT__; 2
    *|   INT          nSizeMin;        4
    *|   INT          nSizeMax;        4
    *| } CHOOSEFONT, *LPCHOOSEFONT; total: 60 bytes

    *| typedef struct tagLOGFONT {
    *|   LONG lfHeight;                   4
    *|   LONG lfWidth;                    4
    *|   LONG lfEscapement;               4
    *|   LONG lfOrientation;              4
    *|   LONG lfWeight;                   4
    *|   BYTE lfItalic;                   1
    *|   BYTE lfUnderline;                1
    *|   BYTE lfStrikeOut;                1
    *|   BYTE lfCharSet;                  1
    *|   BYTE lfOutPrecision;             1
    *|   BYTE lfClipPrecision;            1
    *|   BYTE lfQuality;                  1
    *|   BYTE lfPitchAndFamily;           1
    *|   TCHAR lfFaceName[LF_FACESIZE];  32
    *| } LOGFONT, *PLOGFONT; total: 60 bytes

    Local lcChooseFont, lcLogFont, hLogFont, lcFontFace, l_ret
    l_ret = .F.  &&Valore di ritorno

    Local l_hdc, l_lfHeight
    l_hdc = GetDC(0)
    l_lfHeight = -MulDiv(Iif(Vartype(aInfoFont[2])='N', aInfoFont[2], 12), GetDeviceCaps(l_hdc, 90), 72)
    = ReleaseDC(0, l_hdc)


    * initializing LOGFONT structure:
    * Times New Roman, Italic
    lcLogFont = NUM2DWORD(l_lfHeight) +;
        NUM2DWORD(0)  +;
        NUM2DWORD(0)  +;
        NUM2DWORD(0)  +;
        NUM2DWORD(Iif(aInfoFont[4], FW_BOLD, FW_NORMAL)) +;
        Chr(Iif(aInfoFont[5], 1, 0)) +;
        Chr(Iif(aInfoFont[6], 1, 0)) +;
        Chr(Iif(aInfoFont[7], 1, 0)) +;
        Chr(DEFAULT_CHARSET)     +;
        Chr(OUT_DEFAULT_PRECIS)  +;
        Chr(CLIP_DEFAULT_PRECIS) +;
        Chr(DEFAULT_QUALITY)     +;
        Chr(DEFAULT_PITCH)       +;
        Padr(Iif(Vartype(aInfoFont[1])='C', Alltrim(aInfoFont[1]), "Arial")+Chr(0), 32)

    * copying the LOGFONT data into the global memory object
    * because CHOOSEFONT structure uses the pointer
    lnLogFontSize = 60
    hLogFont = GlobalAlloc(GMEM_FIXED, lnLogFontSize)

    * this function is re-declared below
    * with different parameter types
    = String2Heap (hLogFont, @lcLogFont, lnLogFontSize)

    * initializing CHOOSEFONT structure
    lcChooseFont = NUM2DWORD(60) +;
        NUM2DWORD(0) +;
        NUM2DWORD(0) +;
        NUM2DWORD(hLogFont) +;
        NUM2DWORD(Iif(Vartype(aInfoFont[2])='N', aInfoFont[2], 0)*10) +;
        NUM2DWORD(CF_SCREENFONTS + CF_EFFECTS +;
        CF_INITTOLOGFONTSTRUCT + CF_FORCEFONTEXIST) +;
        NUM2DWORD(Iif(Vartype(aInfoFont[3])='N', aInfoFont[3], 0)) +;
        NUM2DWORD(0) +;
        NUM2DWORD(0) +;
        NUM2DWORD(0) +;
        NUM2DWORD(0) +;
        NUM2DWORD(0) +;
        NUM2DWORD(0) +;
        NUM2DWORD(0) +;
        NUM2DWORD(0)

    If ChooseFont(@lcChooseFont) <> 0
        * displaying selection
        * re-declaring API function with different parameters
        * copying data from the global memory object to VFP string
        = Heap2String (@lcLogFont, hLogFont, lnLogFontSize)
        aInfoFont[2] = Int(buf2dword(Substr(lcChooseFont, 17,4))/10)
        aInfoFont[3] = buf2dword(Substr(lcChooseFont, 25,4))
        aInfoFont[4] = (buf2dword(Substr(lcLogFont, 17,4)))<>FW_NORMAL
        aInfoFont[5] = (Asc(Substr(lcLogFont, 21,1))!=0)
        aInfoFont[6] = (Asc(Substr(lcLogFont, 22,1))!=0)
        aInfoFont[7] = (Asc(Substr(lcLogFont, 23,1))!=0)

        lcFontFace = Substr(lcLogFont, 29)
        lcFontFace = Substr(lcFontFace, 1, At(Chr(0),lcFontFace)-1)
        aInfoFont[1] = lcFontFace
        l_ret = .T.
    Endif
    * releasing system resources
    = GlobalFree (hLogFont)
    Return l_ret
Endfunc

Function  buf2dword (lcBuffer)
    Return Asc(Substr(lcBuffer, 1,1)) + ;
        Asc(Substr(lcBuffer, 2,1)) * 256 +;
        Asc(Substr(lcBuffer, 3,1)) * 65536 +;
        Asc(Substr(lcBuffer, 4,1)) * 16777216

    *** Zucchetti INIZIO - correzione errore funzione round di Fox
    *** http://support.microsoft.com/kb/157954
    *** La soluzione proposta da microsoft non funziona!!!
    *** Basta provare round(1509,499999,0) che restituisce 1510
    *** l'errore dipende dalla set decimal e dall'arrotondamento intrinseco della funzione val()
Function cp_Round(nValue, nDecimal)
    If g_disable_cp_Round Or nDecimal<0 Or nDecimal>20
        Return Round(nValue, nDecimal)
    Else
        Local nInt, nDec, nDecInt, sInt, sDec, sDecInt
        nInt=Int(nValue)
        If nInt>nValue
            *!* errore su funzione INT()
            Return Round(nValue, nDecimal)
        Else
            *!* sDec = parte decimale in stringa
            sDec=Alltrim(Str(Abs(nValue-nInt),10,8))
            If Padr(sDec,1,"0")=="1"
                *!* il primo carattere deve essere "0", se � 1 c'� stato un errore sulla int()
                nInt=nInt+Sign(nValue)
            Endif
            sDecInt=Padr(Substr(sDec,nDecimal+3,1),1,"0")
            nDec=Val(Padr(Substr(sDec,3,nDecimal),nDecimal,"0"))
            If nValue<0
                If sDecInt>="5"
                    nDec=-nDec-1
                Else
                    nDec=-nDec
                Endif
            Else
                If sDecInt>="5"
                    nDec=nDec+1
                Endif
            Endif
            Return Round(nInt+1.00000000*nDec/10^nDecimal,nDecimal)
        Endif
    Endif
Endfunc
*** Zucchetti FINE - correzione errore funzione round di Fox

Define Class prm_container As Custom
    Func IsVar(cVarName)
        Return(Type("this."+cVarName)<>'U')
    Func GetVar(cVarName)
        Return(This.&cVarName)
Enddefine

*--- Classe timer per eseguire operazioni ritardate, utilizzata nella procedura cp_CallAfter
Define Class cp_CallAfterTimer As Timer
    oThis = .Null.
    cCommand = ""
    nOccurrence = 0
    Enabled = .F.
    Proc Init(cCommand, nMs, nOccurrence)
        With This
            .oThis = This
            .cCommand = m.cCommand
            .Interval = m.nMs
            .nOccurrence = m.nOccurrence
            .Enabled = .T.
        Endwith
    Endproc

    Proc Timer()
        With This
            .Enabled = .F.
            Execscript(.cCommand)
            .nOccurrence = .nOccurrence - 1
            If .nOccurrence<1
                .oThis = .Null.
            Else
                .Enabled = .T.
            Endif
        Endwith
    Endproc
Enddefine

*--- Esegue il comando cCommand dopo nMs millisecondi (default 300ms), nOccurrence volte (default 1)
Proc cp_CallAfter(cCommand, nMs, nOccurrence)
    Local oTimer, nNumMs, nNumOccur
    nNumMs = Evl(m.nMs, 300)
    nNumOccur = Evl(m.nOccurrence, 1)
    If !Empty(m.cCommand)
        oTimer = Createobject("cp_CallAfterTimer", m.cCommand, m.nNumMs, m.nNumOccur)
    Endif
Endproc

*--- Funzioni RGB
Function GetRColor(nColor)
    Return Bitand(255,Bitrshift(m.nColor,0))

Function GetGColor(nColor)
    Return Bitand(255,Bitrshift(m.nColor,8))

Function GetBColor(nColor)
    Return Bitand(255,Bitrshift(m.nColor,16))

*--- nH, nS, nL passati per riferimento
Proc RGBtoHSL(nColor, nH, nS, nL)
	Local nRed, nGreen, nBlu, nMax, nMin

    nRed = GetRColor(m.nColor)/51*.2
    nGreen = GetGColor(m.nColor)/51*.2
    nBlu = GetBColor(m.nColor)/51*.2

    nMax=MAX(m.nRed, m.nGreen, m.nBlu)
    nMin=Min(m.nRed, m.nGreen, m.nBlu)

    m.nL = (nMax+nMin)/2

    IF m.nMax==m.nMin
		m.nS=0
		m.nH=0
    ELSE
    	If m.nL < .5
			m.nS=(m.nMax-m.nMin)/(m.nMax+m.nMin)
		else
			m.nS=(m.nMax-m.nMin)/(2-m.nMax-m.nMin)
		ENDIF
		DO CASE
			case m.nRed==m.nMax
				m.nH = (m.nGreen-m.nBlu)/(m.nMax-m.nMin)
			case m.nGreen==m.nMax
				m.nH = 2+((m.nBlu-m.nRed)/(m.nMax-m.nMin))
			case m.nBlu==m.nMax
				m.nH = 4+((m.nRed-m.nGreen)/(m.nMax-m.nMin))
		ENDCASE
    endif
	m.nH=MOD(Round(m.nH*60,0), 360)
	m.nS=Round(m.nS*100,0)
	m.nL=Round(m.nL*100,0)
EndProc

*--- HSLtoRGB
*--- Ritorna rgb da HSL
Function HSLtoRGB(nH, nS, nL)
	LOCAL p1,p2

	nL = m.nL*0.01
	nS = m.nS*0.01
	if (m.nL<=0.5)
		p2 = m.nL*(1+m.nS)
	else
		p2 = m.nL+m.nS-(m.nL*m.nS)
	ENDIF
	p1 = 2*m.nL-m.p2
	if (m.nS==0)
		nCol = ROUND(m.nL*255,0)
		RETURN RGB(m.nCol, m.nCol, m.nCol)
	else
		RETURN RGB(ROUND(FindRGB(m.p1,m.p2,m.nH+120)*255,0), ROUND(FindRGB(m.p1,m.p2,m.nH)*255,0), ROUND(FindRGB(m.p1,m.p2,m.nH-120)*255,0))
	Endif
EndFunc

function FindRGB(q1, q2, hue)
	LOCAL nHue
	nHue=MOD(m.hue,360)
	DO case
		case (m.nHue<60)
			return (m.q1+(m.q2-m.q1)*m.nHue/60)
		case (m.nHue<180)
			return(m.q2)
		case (m.nHue<240)
			return(m.q1+(m.q2-m.q1)*(240-m.nHue)/60)
		OTHERWISE
			return(m.q1)
	ENDCASE
ENDFUNC
*--- HSLtoRGB

*--- GetBrightnessDiff
Function GetBrightnessDiff(nBackColor, nForeColor)
    Local nBackColorY, nForeColorY
    nBackColorY = ((GetRColor(m.nBackColor) * 299) + (GetGColor(m.nBackColor) * 587) + (GetBColor(m.nBackColor) * 114)) * 0.001
    nForeColorY = ((GetRColor(m.nForeColor) * 299) + (GetGColor(m.nForeColor) * 587) + (GetBColor(m.nForeColor) * 114)) * 0.001
    Return Abs(m.nBackColorY-m.nForeColorY)
Endfunc

*--- Restituisce il colore pi� leggibile tra RGB(243,243,243) e RGB(40,40,40) dato il colore di sfondo nBackColor
FUNCTION GetBestForeColor(nBackColor)
    LOCAL WhiteDiff, BlackDiff, nForeColor
	WhiteDiff = GetBrightnessDiff(m.nBackColor, RGB(243,243,243))
	BlackDiff = GetBrightnessDiff(m.nBackColor, RGB(40,40,40))
    RETURN IIF(m.WhiteDiff >= m.BlackDiff, RGB(243,243,243), RGB(40,40,40))  	
EndFunc

*--- RGB AlphaBlending
Function RGBAlphaBlending(nBackColor, nForeColor, nAlpha)
    Local nRed, nGreen, nBlu
    nRed = Int((GetRColor(m.nForeColor)*m.nAlpha) + (GetRColor(m.nBackColor)*(1 - m.nAlpha)))
    nGreen = Int((GetGColor(m.nForeColor)*m.nAlpha) + (GetGColor(m.nBackColor)*(1 - m.nAlpha)))
    nBlu = Int((GetBColor(m.nForeColor)*m.nAlpha) + (GetBColor(m.nBackColor)*(1 - m.nAlpha)))
    Return Rgb(m.nRed, m.nGreen, m.nBlu)
Endfunc
*--- RGB AlphaBlending

*--- Classi per gestire transizioni di propriet�

*--- cp_SingleTransition
*--- Effettua una singola transizione
*--- pObj: Oggetto target
*--- pPropMethod: Propriet�/Metodo da variare/richiamare (i metodi davono avere due parametri Example(oTrs, nPercentage))
*--- pValueEnd: Valore finale
*--- pType: Tipo di pPropMethod (N Numeric, M Method, C Color, T Text)
*--- pTransitionTime: Durata transizione in ms
*--- pTransitionMethod: Metodo di transizione (L Linear, A Acceleration, D Deceleration, E EaseInEasyOut)
*--- bWait: Attende la fine della transizione prima di restituire il controllo a vfp
Function cp_SingleTransition(pObj, pPropMethod, pValueEnd, pType, pTransitionTime, pTransitionMethod, bWait)
    Local tm
    tm = Createobject("cp_TransitionManager")
    tm.Add(m.pObj, m.pPropMethod, m.pValueEnd, m.pType, m.pTransitionTime, m.pTransitionMethod)
    RETURN tm.Start(m.bWait) &&ritorna variabile pubblica
Endfunc

*--- Gestore delle transizioni
Define Class cp_TransitionManager As Custom
    oThis = .Null.
    Add Object aTransition As Collection
    Add Object oTimer As Timer With Interval = 5, Enabled = .F.

    oCompletedObj = .Null.
    cCompletedMethod = ""

    bCompleted = .F.
    cSectionId = ""

    Procedure Init()
        This.oThis = This
        This.cSectionId = SYS(2015)
    Endproc

	*--- Ritorna il nome di una variabile pubblica che punta alla transizione
    Function Start(bWait)
        Local oTransition , nSeconds, nTransitionTimeTotal, l_nIndex
        nTransitionTimeTotal = 0
        nSeconds = Seconds()
        For l_nIndex=1 TO This.aTransition.Count
        	oTransition = This.aTransition.Item(m.l_nIndex)
            *---Tutte le transizioni iniziano allo stesso tempo
	        m.oTransition.nStartTime = m.nSeconds
            *--- Se devo attendere calcolo il tempo totale di tutte le transizioni
	        nTransitionTimeTotal = m.nTransitionTimeTotal + m.oTransition.nTransitionTime
        Next
        oTransition = .Null.
        This.bCompleted = .F.
        This.oTimer.Enabled = .T.
        If m.bWait
            Local OldCursor
            OldCursor = Set("Cursor")
            Set Cursor Off
            Local nSeconds
            nSeconds = Seconds()
            IF !Wexist("Debugger")
	            Do While .T. &&(Seconds() - m.nSeconds)*1000 <= m.nTransitionTimeTotal
	                Inkey(0.001, 'H')
	                IF This.bCompleted
	                	Exit
	                ENDIF
	            Enddo
			ELSE
				IF This.oTimer.Enabled
					This.oTimer.Timer()
				Endif
			ENDIF
            Set Cursor &OldCursor
            RETURN ""
        ELSE
			LOCAL cVarPublic
			cVarPublic = This.cSectionId
			PUBLIC (cVarPublic)
			&cVarPublic = This
			Return This.cSectionId
        Endif
    Endfunc

    Procedure Add(pObj, pPropMethod, pValueEnd, pType, pTransitionTime, pTransitionMethod)
        Local oTransition
        oTransition = Createobject("cp_Transition", m.pObj, m.pPropMethod, m.pValueEnd, m.pType, m.pTransitionTime, m.pTransitionMethod)
        This.aTransition.Add(m.oTransition)
    Endproc

    Procedure OnCompleted(oObj, cMethod)
        This.oCompletedObj = m.oObj
        This.cCompletedMethod = m.cMethod
    Endproc

    Procedure oTimer.Timer()
        Local bStop, oTransition, hCur, l_nIndex
        bStop = .T.
        This.Enabled = .F.
        For l_nIndex=1 TO This.Parent.aTransition.Count
	        oTransition = This.Parent.aTransition.Item(m.l_nIndex)
            If !m.oTransition.bCompleted
                m.oTransition.OnTimer()
                bStop = m.bStop And m.oTransition.bCompleted
            Endif
        Next
        This.Enabled = !m.bStop
        oTransition = .Null.
        If m.bStop
            If !Isnull(This.Parent.oCompletedObj)
                Local cMethod
                cMethod = This.Parent.cCompletedMethod
                This.Parent.oCompletedObj.&cMethod
                This.Parent.oCompletedObj = .Null.
            ENDIF
            This.Parent.bCompleted = .T.
            This.Parent.Release()
        Endif
    Endproc

    Procedure Release()
        This.oThis = .Null.
        Local oTransition, nIndex, nCount
        nCount = This.aTransition.Count
        This.oCompletedObj = .Null.
        For nIndex=m.nCount TO 1 STEP -1
	        oTransition = This.aTransition.Item(m.nIndex)
            m.oTransition.oObj = .Null.
            This.aTransition.Remove(m.nIndex)
        Next
        oTransition = .Null.
        RELEASE (This.cSectionId)
    Endproc
Enddefine

*--- Singole transizioni
Define Class cp_Transition As Custom
    nStartTime = 0 &&Inizio transizione (seconds)
    nTransitionTime = 0  &&Durata totale transizione (ms)
    oObj = .Null.	&&Oggetto da modificare
    cPropMethod ="" &&Metodo o propriet� da chiamare/variare
    xValueEnd = .Null.	&&Valore finale
    xValueStart = .Null. &&Valore iniziale
    cType = "N"  &&N Numeric, M Method, C Color, T Text
    cTransitionMethod = "Linear"
    * - Linear

    *--- Quadratic easing p^2
    * - QuadraticEaseIn
    * - QuadraticEaseOut
    * - QuadraticEaseInOut

    *--- Cubic easing p^3
    * - CubicEaseIn
    * - CubicEaseOut
    * - CubicEaseInOut

    *--- Quartic easing p^4
    * - QuarticEaseIn
    * - QuarticEaseOut
    * - QuarticEaseInOut

    *--- Quintic easing p^5
    * - QuinticEaseIn
    * - QuinticEaseOut
    * - QuinticEaseInOut

    *--- Sine wave easing sin(p * PI/2)
    * - SineEaseIn
    * - SineEaseOut
    * - SineEaseInOut

    *--- Circular easing sqrt(1 - p^2)
    * - CircularEaseIn
    * - CircularEaseOut
    * - CircularEaseInOut

    *--- Exponential easing base 2
    * - ExponentialEaseIn
    * - ExponentialEaseOut
    * - ExponentialEaseInOut

    *--- Exponentially-damped sine wave easing
    * - ElasticEaseIn
    * - ElasticEaseOut
    * - ElasticEaseInOut

    *--- Overshooting cubic easing
    * - BackEaseIn
    * - BackEaseOut
    * - BackEaseInOut

    *--- Exponentially-decaying bounce easing
    * - BounceEaseIn
    * - BounceEaseOut
    * - BounceEaseInOut
    bCompleted = .F. &&Stato transizione

    Procedure Init(pObj, pPropMethod, pValueEnd, pType, pTransitionTime, pTransitionMethod)
        With This
            .nTransitionTime = Evl(m.pTransitionTime,300)
            .oObj = m.pObj
            .cPropMethod = m.pPropMethod
            .xValueEnd = m.pValueEnd
            .cType = Evl(m.pType, "N")
            If .cType <> "M"
                .xValueStart = .oObj.&pPropMethod
                *--- se valore iniziale � gi� uguale a quello finale non faccio nulla
                .bCompleted = (.xValueStart == .xValueEnd)
            Endif
            .cTransitionMethod = Evl(m.pTransitionMethod,"Linear")
        Endwith
    Endproc

    Proc OnTimer()
        *--- Effettua il calcolo della percentuale di completamento ed aggiorna i valori
        With This
            If  Vartype(.oObj)="O" And !Isnull(.oObj)
                Local nTime, nPercentage, nElapsed, bCompleted, nElapsed_1
                Local nFirstHalfTime, nSecondHalfTime
                nTime = (Seconds() - .nStartTime) * 1000
                nElapsed = (m.nTime / .nTransitionTime)

                If m.nElapsed >=1.0 OR Wexist("Debugger")
                    *--- Ho finito la transizione oppure il debugger � aperto
                    m.nPercentage = 1.0
                    .bCompleted = .T.
                Else
                    Do Case
                        Case .cTransitionMethod == "Linear"
                            *--- Lineare (y = x)
                            nPercentage = m.nElapsed
                        Case .cTransitionMethod == "QuadraticEaseIn"
                            *--- Quadratic (y = x^2)
                            nPercentage = m.nElapsed * m.nElapsed
                        Case .cTransitionMethod == "QuadraticEaseOut"
                            *--- Quadratic (y = -x^2 + 2x)
                            nPercentage = m.nElapsed * (2.0 - m.nElapsed)
                        Case .cTransitionMethod == "QuadraticEaseInOut"
                            *--- Quadratic
                            *--- y = (1/2)((2x)^2)	[0, 0.5)
                            *--- y = -(1/2)((2x-1)*(2x-3) - 1)	[0.5, 1]
                            If(m.nElapsed < 0.5)
                                nPercentage = 2 * m.nElapsed * m.nElapsed
                            Else
                                nPercentage = (-2 * m.nElapsed * m.nElapsed) + (4 * m.nElapsed) - 1
                            Endif
                        Case .cTransitionMethod == "CubicEaseIn"
                            *--- Cubic (y = x^3)
                            nPercentage = m.nElapsed * m.nElapsed * m.nElapsed
                        Case .cTransitionMethod == "CubicEaseOut"
                            *--- Cubic (y = (x - 1)^3 + 1)
                            nElapsed_1 = (m.nElapsed - 1)
                            nPercentage = m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 + 1
                        Case .cTransitionMethod == "CubicEaseInOut"
                            *--- Cubic
                            *--- y = (1/2)((2x)^3)	[0, 0.5)			(4x^3)
                            *--- y = (1/2)((2x-2)^3 + 2)	[0.5, 1]	(4(x-1)^3+1)
                            If m.nElapsed < 0.5
                                nPercentage = 4 * m.nElapsed * m.nElapsed * m.nElapsed
                            Else
                                nElapsed_1 = (m.nElapsed - 1)
                                nPercentage = 4 * m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 + 1
                            Endif
                        Case .cTransitionMethod == "QuarticEaseIn"
                            *--- Quartic (y = x^4)
                            nPercentage = m.nElapsed * m.nElapsed * m.nElapsed * m.nElapsed
                        Case .cTransitionMethod == "QuarticEaseOut"
                            *--- Quartic (y = 1 - (x - 1)^4)
                            nElapsed_1 = (m.nElapsed - 1)
                            nPercentage = m.nElapsed_1  * m.nElapsed_1  * m.nElapsed_1 * (1 - m.nElapsed) + 1
                        Case .cTransitionMethod == "QuarticEaseInOut"
                            *--- Quartic
                            *--- y = (1/2)((2x)^4)	[0, 0.5)
                            *--- y = -(1/2)((2x-2)^4 - 2)	[0.5, 1]
                            If m.nElapsed < 0.5
                                nPercentage = 8 * m.nElapsed * m.nElapsed * m.nElapsed * m.nElapsed
                            Else
                                nElapsed_1 = (m.nElapsed - 1)
                                nPercentage = -8 * m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 + 1
                            Endif
                        Case .cTransitionMethod == "QuinticEaseIn"
                            *--- Quintic (y = x^5)
                            nPercentage = m.nElapsed * m.nElapsed * m.nElapsed * m.nElapsed * m.nElapsed
                        Case .cTransitionMethod == "QuinticEaseOut"
                            *--- Quintic (y = (x - 1)^5 + 1)
                            nElapsed_1 = (m.nElapsed - 1)
                            nPercentage = m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 + 1
                        Case .cTransitionMethod == "QuinticEaseInOut"
                            *--- Quintic
                            *--- y = (1/2)((2x)^5)	[0, 0.5)
                            *--- y = (1/2)((2x-2)^5 + 2)	[0.5, 1]
                            If m.nElapsed < 0.5
                                nPercentage = 16 * m.nElapsed * m.nElapsed * m.nElapsed * m.nElapsed * m.nElapsed
                            Else
                                nElapsed_1 = ((2 * m.nElapsed) - 2)
                                nPercentage = 0.5 * m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 + 1
                            Endif
                        Case .cTransitionMethod == "SineEaseIn"
                            *--- Sine
                            nPercentage = Sin((m.nElapsed - 1) * Pi()*0.5) + 1
                        Case .cTransitionMethod == "SineEaseOut"
                            *--- Sine
                            nPercentage = Sin(m.nElapsed * Pi()*0.5)
                        Case .cTransitionMethod == "SineEaseInOut"
                            *--- Sine
                            nPercentage = 0.5 * (1 - Cos(m.nElapsed * Pi()))
                        Case .cTransitionMethod == "CircularEaseIn"
                            *--- Circular
                            nPercentage = 1 - Sqrt(1 - (m.nElapsed * m.nElapsed))
                        Case .cTransitionMethod == "CircularEaseOut"
                            *--- Circular
                            nPercentage = Sqrt((2 - m.nElapsed) * m.nElapsed)
                        Case .cTransitionMethod == "CircularEaseInOut"
                            *--- Circular
                            *--- y = (1/2)(1 - sqrt(1 - 4x^2))	[0, 0.5)
                            *--- y = (1/2)(sqrt(-(2x - 3)*(2x - 1)) + 1)	[0.5, 1]
                            If m.nElapsed < 0.5
                                nPercentage = 0.5 * (1 - Sqrt(1 - 4 * (m.nElapsed * m.nElapsed)))
                            Else
                                nPercentage = 0.5 * (Sqrt(-((2 * m.nElapsed) - 3) * ((2 * m.nElapsed) - 1)) + 1)
                            Endif
                        Case .cTransitionMethod == "ExponentialEaseIn"
                            *--- Exponential (y = 2^(10(x - 1)))
                            nPercentage = Iif(m.nElapsed = 0.0, m.nElapsed, 2^(10 * (m.nElapsed - 1)))
                        Case .cTransitionMethod == "ExponentialEaseOut"
                            *--- Exponential (y = -2^(-10x) + 1)
                            nPercentage = Iif(m.nElapsed = 1.0, m.nElapsed, 1 - 2^(-10 * m.nElapsed))
                        Case .cTransitionMethod == "ExponentialEaseInOut"
                            *--- Exponential
                            *--- y = (1/2)2^(10(2x - 1))	[0,0.5)
                            *--- y = -(1/2)*2^(-10(2x - 1))) + 1	[0.5,1]
                            If m.nElapsed = 0.0 Or m.nElapsed = 1.0
                                nPercentage = m.nElapsed
                            Else
                                If m.nElapsed < 0.5
                                    nPercentage =  0.5 * 2^((20 * m.nElapsed) - 10)
                                Else
                                    nPercentage = -0.5 * 2^((-20 * m.nElapsed) + 10) + 1
                                Endif
                            Endif
                        Case .cTransitionMethod == "ElasticEaseIn"
                            *--- Elastic (y = sin(13pi/2*x)*pow(2, 10 * (x - 1)))
                            nPercentage = Sin(13 * Pi()*0.5 * m.nElapsed) * 2^(10 * (m.nElapsed - 1))
                        Case .cTransitionMethod == "ElasticEaseOut"
                            *--- Elastic (y = sin(-13pi/2*(x + 1))*pow(2, -10x) + 1)
                            nPercentage = Sin(-13 * Pi()*0.5 * (m.nElapsed + 1)) * 2^(-10 * m.nElapsed) + 1
                        Case .cTransitionMethod == "ElasticEaseInOut"
                            *--- Elastic
                            *--- y = (1/2)*sin(13pi/2*(2*x))*pow(2, 10 * ((2*x) - 1))	[0,0.5)
                            *--- y = (1/2)*(sin(-13pi/2*((2x-1)+1))*pow(2,-10(2*x-1)) + 2)	[0.5, 1]
                            If m.nElapsed < 0.5
                                nPercentage =  0.5 * Sin(13 * Pi() * m.nElapsed) * 2^(10 * ((2 * m.nElapsed) - 1))
                            Else
                                nPercentage = 0.5 * (Sin(-13 * Pi()*0.5 * ((2 * m.nElapsed - 1) + 1)) * 2^(-10 * (2 * m.nElapsed - 1)) + 2)
                            Endif
                        Case .cTransitionMethod == "BackEaseIn"
                            *--- Back (y = x^3-x*sin(x*pi))
                            nPercentage = m.nElapsed * m.nElapsed * m.nElapsed - (m.nElapsed * Sin(m.nElapsed * Pi()))
                        Case .cTransitionMethod == "BackEaseOut"
                            *--- Back (y = 1-((1-x)^3-(1-x)*sin((1-x)*pi)))
                            nElapsed_1 = 1 - m.nElapsed
                            nPercentage = 1 - (m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 - (m.nElapsed_1 * Sin(m.nElapsed_1 * Pi())))
                        Case .cTransitionMethod == "BackEaseInOut"
                            *--- Back
                            *--- y = (1/2)*((2x)^3-(2x)*sin(2*x*pi))	[0,0.5)
                            *--- y = (1/2)*(1-((1-x)^3-(1-x)*sin((1-x)*pi))+1)	[0.5, 1]
                            If m.nElapsed < 0.5
                                nElapsed_1 = 2 * m.nElapsed
                                nPercentage =  0.5 * (m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 - (m.nElapsed_1 * Sin(m.nElapsed_1 * Pi())))
                            Else
                                nElapsed_1 = (1 - m.nElapsed)
                                nPercentage = 0.5 * (1 - (m.nElapsed_1 * m.nElapsed_1 * m.nElapsed_1 - m.nElapsed_1 * Sin(m.nElapsed_1 * Pi())) + 1)
                            Endif
                        Case .cTransitionMethod == "BounceEaseIn"
                            nPercentage = 1 - .BounceEaseOut(1 - m.nElapsed)
                        Case .cTransitionMethod == "BounceEaseOut"
                            nPercentage = .BounceEaseOut(m.nElapsed)
                        Case .cTransitionMethod == "BounceEaseInOut"
                            If m.nElapsed < 0.5
                                nPercentage = 0.5 * (1 - .BounceEaseOut(1 - m.nElapsed * 2))
                            Else
                                nPercentage = 0.5 * .BounceEaseOut(m.nElapsed * 2 - 1) + 0.5
                            Endif
                        Otherwise
                            *--- Lineare (y = x)
                            nPercentage = m.nElapsed
                    Endcase

                    .bCompleted = .F.
                Endif

                *--- Effettuo la variazione
                Local pPropMethod
                pPropMethod = .cPropMethod
                Do Case
                    Case .cType = "N"
                        *--- Numeric
                        .oObj.&pPropMethod = .Interpolate(.xValueStart, .xValueEnd, m.nPercentage)
                    Case .cType = "C"
                        *--- Color
                        *--- Componenti RGB del colore iniziale
                        *--- (Potrebbe essere fatto solo la prima volta)
                        Local StartColorRed, StartColorGreen, StartColorBlu
                        StartColorRed = Bitand(.xValueStart, 0x0000FF)
                        StartColorGreen = Bitrshift(Bitand(.xValueStart, 0x00FF00),8)
                        StartColorBlu = Bitrshift(Bitand(.xValueStart, 0xFF0000),16)
                        *--- Componenti RGB del colore finale
                        *--- (Potrebbe essere fatto solo la prima volta)
                        Local EndColorRed, EndColorGreen, EndColorBlu
                        EndColorRed = Bitand(.xValueEnd, 0x0000FF)
                        EndColorGreen = Bitrshift(Bitand(.xValueEnd, 0x00FF00),8)
                        EndColorBlu = Bitrshift(Bitand(.xValueEnd, 0xFF0000),16)

                        *--- Interpolazione dei componenti RGB
                        Local ColorRed, ColorGreen, ColorBlu
                        ColorRed = .Interpolate(m.StartColorRed , m.EndColorRed, m.nPercentage)
                        ColorGreen = .Interpolate(m.StartColorGreen , m.EndColorGreen, m.nPercentage)
                        ColorBlu = .Interpolate(m.StartColorBlu , m.EndColorBlu, m.nPercentage)

                        .oObj.&pPropMethod = Rgb(m.ColorRed, m.ColorGreen, m.ColorBlu)

                    Case .cType = "T"
                        *--- Testo
                        Local strStart, strEnd, nStartLength, nEndLength, nLength, i
                        Local cStart, cEnd, nStart, nEnd, cInterpolated, nInterpolated, result
                        strStart = .xValueStart
                        strEnd = .xValueEnd

                        *--- Determino la lunghezza della stringa interpolata
                        nStartLength = Len(m.strStart)
                        nEndLength = Len(m.strEnd)
                        nLength = .Interpolate(m.nStartLength, m.nEndLength, m.nPercentage)

                        *--- Interpolazione delle lettere delle due stringhe
                        result = ""
                        For i = 1 To m.nLength
                            *--- Seleziono le singole lettere
                            cStart = 'a'
                            If m.i <= m.nStartLength
                                cStart = Substr(m.strStart, m.i, 1)
                            Endif
                            cEnd = 'a'
                            If m.i <= m.nEndLength
                                cEnd = Substr(m.strEnd, m.i, 1)
                            Endif

                            *--- Intepolo le due lettere
                            If m.cEnd == ' '
                                *--- Se il carattere finale � uno spazio non lo interpolo, rende la stringa pi� leggibile migliorando l'effetto
                                cInterpolated = ' '
                            Else
                                *--- Interpolazione
                                nStart = Asc(m.cStart)
                                nEnd = Asc(m.cEnd)
                                nInterpolated = .Interpolate(m.nStart, m.nEnd, m.nPercentage)
                                cInterpolated = Chr(m.nInterpolated)
                            Endif
                            *--- Concateno il risultato
                            result = m.result + m.cInterpolated
                        Next
                        .oObj.&pPropMethod = m.result

                    Case .cType = "M"
                        *--- Method
                        *--- Il metodo viene semplicemente richiamato
                        *--- passandogli come parametri l'oggetto intepolazione e la percentuale di completamento
                        Local cCmd
                        cCmd = ".oObj."+pPropMethod+"(This, m.nPercentage"+Iif(!Empty(.xValueEnd),","+.xValueEnd,"")+")"
                        &cCmd
                Endcase
            Else
                *--- Se l'oggetto target non esiste termino la transizione
                .bCompleted = .T.
            Endif
        Endwith
    Endproc

    *--- Funzione di interpolazione
    Function BounceEaseOut(nElapsed)
        If m.nElapsed < 4/11.0
            Return (121 * m.nElapsed * m.nElapsed)/16.0
        Else
            If m.nElapsed < 8/11.0
                Return (363/40.0 * m.nElapsed * m.nElapsed) - (99/10.0 * m.nElapsed) + 17/5.0
            Else
                If m.nElapsed < 9/10.0
                    Return (4356/361.0 * m.nElapsed * m.nElapsed) - (35442/1805.0 * m.nElapsed) + 16061/1805.0
                Else
                    Return (54/5.0 * m.nElapsed * m.nElapsed) - (513/25.0 * m.nElapsed) + 268/25.0
                Endif
            Endif
        Endif
    Endfunc

    Function Interpolate(n1, n2, nPercentage)
        Return m.n1 + ((m.n2 - m.n1) * m.nPercentage)
    Endfunc
Enddefine

*--- Retrieves the signed x-coordinate from the specified LPARAM value
Function Get_X_LParam(Lparam)
    Local _x
    _x = Bitand(m.lParam, 0x0000ffff)
    If _x>32767
        _x=_x-65536
    Endif
    Return _x
Endfunc

*--- Retrieves the signed y-coordinate from the specified LPARAM value
Function Get_Y_LParam(Lparam)
    Local _y
    _y = Bitand(Int(m.lParam / 0x10000), 0xFFFF)
    If _y>32767
        _y=_y-65536
    Endif
    Return _y
Endfunc

*--- Restituisce il percorso dell'immagine creata a partire da quella passata
*--- dopo averci applicato il colore richiesto (miglior effetto sul nero, inutile sul bianco)
Function cp_TmpColorizeImg(cImg, nForeColor)
	Local l_Ext, l_SrcImg, l_DstImg, l_oldArea
	m.l_DstImg = ""
	If !Empty(m.cImg) And m.nForeColor>=0 And m.nForeColor<=Rgb(255,255,255)
		m.cImg = Alltrim(m.cImg)
		m.l_Ext = JustExt(m.cImg)
		m.l_SrcImg = ForceExt(cp_GetStdImg(m.cImg, l_Ext),l_Ext)
		If cp_FileExist(m.l_SrcImg)
			*--- Gestione Cache
			If Vartype(i_TmpColImgCur)<>'C' Or Empty(i_TmpColImgCur) Or !Used(i_TmpColImgCur)
				Public i_TmpColImgCur
				i_TmpColImgCur = Sys(2015)
				Create Cursor (i_TmpColImgCur) (SrcImg C(254), Colorize N(8), DstImg C(254))
			Endif
			m.l_SrcImg = Lower(Strtran(m.l_SrcImg, '/', '\'))
			m.l_oldArea = Select()
			Select(i_TmpColImgCur)
			Locate For Alltrim(SrcImg)==Alltrim(m.l_SrcImg) And Colorize==m.nForeColor
			If Found()
				m.l_DstImg = DstImg
			Else
				m.l_DstImg = ForceExt(Addbs(_Screen.CP_THEMESMANAGER.ImgPath)+JustStem(m.cImg)+Sys(2015),l_Ext)
				cb_ColorizeBmp(m.l_SrcImg, m.l_DstImg, m.nForeColor)
				*--- Inserisco la nuova immagine nel cursore
				Insert into (i_TmpColImgCur) Values(m.l_SrcImg, m.nForeColor, m.l_DstImg)
				*--- Nel caso in cui si tratti di un bmp cerco e sposto anche la maschera
				If Lower(m.l_Ext)=='bmp' And cp_FileExist(ForceExt(m.l_SrcImg,'msk'))
					Copy File ForceExt(m.l_SrcImg,'msk') to ForceExt(m.l_DstImg,'msk')
				Endif
			Endif
			Select(m.l_oldArea)
		Endif
	Endif
	Return m.l_DstImg
Endfunc

*** Zucchetti Aulla INIZIO - Funzione per sapere se si possiedono i diritti di come amministratore di Windows
Function cp_IsAdminWin()
	Local loAPI, lcVal

	Declare Integer IsUserAnAdmin In Shell32
	Try
		lnResult = IsUserAnAdmin()
	Catch
		*** OLD OLD Version of Windows assume .T.
		lnResult = 1
	Endtry
	If lnResult = 0
		Return .F.
	Endif

	Return .T.
Endfunc
*** Zucchetti Aulla FINE - Funzione per sapere se si possiedono i diritti di come amministratore di Windows

*** Zucchetti Aulla Inizio - variabile per la gestione automatica (nei batch) dei campi UTCC,UTCV,UTDC,UTDV
Procedure ah_SysFld
	Lparameters cSetting
	* gestisce l'aggiornamento automatico nei batch dei campi di sistema UTCC,UTCV,UTDC,UTDV
	* Valori per cSetting: ON (sempre attivo), OFF (sempre spento), DEF (spento se g_SCHEDULER='S', attivo altrimenti)
	If Inlist(cSetting,'ON','OFF','DEF')
		If Type('g_UPDSYSFLD')='U'
			Public g_UPDSYSFLD
		Endif
		g_UPDSYSFLD = cSetting
	Else
		Release g_UPDSYSFLD
	Endif
Endproc
*** Zucchetti Aulla Fine - variabile per la gestione automatica (nei batch) dei campi UTCC,UTCV,UTDC,UTDV
