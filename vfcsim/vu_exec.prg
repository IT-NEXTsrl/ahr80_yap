* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VU_EXEC
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 21/09/98
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Interprete di menu
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

* ---- i _menu = Nome del men� al quale collegarsi
* ---- i_Padname  = Nome del Pad
* ---- i_optname = Caption voce di menu
* ---- i_action = Campo Directory
* ---- i_Subname = Programma a lanciare o nome popup
* ---- i = Posizione relativa della voce di men� (indice univoco sequenziale fratelli)
* --- i_cMod = espressione per valutare se abilitare o meno la voce
* --- i_bEnabled = voce abilitata o meno
* --- i_cPicture = eventuale Bitmap
* --- bContext, se a .t. utilizzato per caricare men� contestuale
Proc LoadMenuItem(i_menu,i_padname,i_optname,i_action,i_subname,i,i_cMod,i_bEnabled, i_cPicture, bContext)
    Local i_pos,i_evd,i_cOpz,k,i_cSkip, i_bModuleInstalled, oOldErr, bErrM
    i_cSkip=Iif(i_bEnabled,'',' SKIP FOR .t.')
    If Left( i_optname ,1 )="{"
        Private L_MACRO, L_OLDERR, L_NewErr
        L_OLDERR = On("ERROR")
        On Error L_NewErr=.T.
        L_MACRO = Chrtran(i_optname,"{}","")
        i_optname = &L_MACRO
        On Error &L_OLDERR
        Release L_MACRO, L_OLDERR, L_NewErr
    Endif
    If Vartype(i_cPicture)<>'C' Or ;
            LOWER(Alltrim(i_cPicture))=Lower( g_MNODEBMP ) Or;
            LOWER(Alltrim(i_cPicture))=Lower( g_MLEAFBMP )
        i_cPicture=''
    Endif
    If Not(Type("g_DisableMenuImage")='L' And g_DisableMenuImage) And Not Empty(i_cPicture) And Vartype(i_ThemesManager)=='O'
        If Left( Alltrim( m.i_cPicture ) ,1 )="{"
            Private L_MACRO, L_OLDERR, L_NewErr
            L_OLDERR = On("ERROR")
            On Error L_NewErr=.T.
            L_MACRO = Chrtran(m.i_cPicture,"{}","")
            i_cPicture = &L_MACRO
            On Error &L_OLDERR
            Release L_MACRO, L_OLDERR, L_NewErr
        Endif
        i_cPicture = i_ThemesManager.RetBmpFromIco(i_cPicture, 16)
    Endif
    * --- Gestione menu su variabili globali
    i_bModuleInstalled=i_bEnabled
    If !Empty(i_cMod)
        If Upper(Left(i_cMod,8)) = 'G_OMENU'
            i_cMod  = Strtran(i_cMod,Chr(253),"'")
            PosVirg = Rat(',',i_cMod)
            * Estraggo l'oggetto dalla stringa i_cmod
            Par1 = Substr(i_cMod,9,PosVirg - 9)
            * Estraggo la condizione da testare sull'oggetto
            Par2 = Substr(i_cMod,PosVirg+1,Len(i_cMod)-(PosVirg+1))
            * verifico la condizione sull'oggetto
            If g_Omenu.ObjInfo(Par1,Par2)
                i_cSkip=''
                i_bModuleInstalled=.T.
            Else
                i_cSkip=' SKIP FOR .t.'
                i_bModuleInstalled=.F.
            Endif
        Else
            If At('(',i_cMod)>0
                i_cMod  = Strtran(i_cMod,"*","'")
		            i_cMod  = Strtran(i_cMod,"�","'")
                i_cSkip = Iif(Eval(i_cMod), i_cSkip, ' SKIP FOR .t.')
                i_bModuleInstalled=Eval(i_cMod)
            Else
                If At(','+i_cMod+',',','+i_cModules+',')=0
                    i_cSkip=' SKIP FOR .t.'
                    i_bModuleInstalled=.F.
                Endif
            Endif
        Endif
    Endif
    i_cOpz=Iif(Empty(i_cLanguage) Or g_PROJECTLANGUAGE=i_cLanguage, i_optname, cp_Translate(i_optname))
    i_pos = At("&",i_cOpz)
    i_evd = Iif(i_pos<>0,Subs(i_cOpz,i_pos+1,1),Left(i_cOpz,1))
    *  Non mostriamo accanto alla voce di men� il tasto di scelta rapida ALT-?(aggiunto ,"")
    k = 'ALT+'+i_evd+',""'
    i_cOpz=Iif(i_pos<>0,Left(i_cOpz,i_pos-1)+'\<'+Trim(Right(i_cOpz,Len(i_cOpz)-i_pos)),'\<'+Alltrim(i_cOpz))
    If i_optname="------------"
        i_action=9
    Endif
    oOldErr=On('error')
    bErrM=.F.
    On Error bErrM=.T.
    If Lower(i_menu)=Iif(bContext,'contextmenu','_msysmenu')
        *--- Zucchetti Aulla Inizio - Window Menu
        *--- Determino se aggiungere la voce di menu prima o dopo Window, il menu Window viene sempre posto
        *--- Prima della voce '\<?' se quest'ultima non esiste il menu Window viene posto come ultima voce
        mPosition = Iif(i_cOpz == '\<?', 'AFTER _MWINDOW', 'BEFORE _MWINDOW')
        *--- Zucchetti Aulla Fine - Window Menu
        If i_action<>9
            If bContext
                #If Version(5)>600
                    Define Bar (i) Of &i_menu Prompt i_cOpz Picture i_cPicture Key &k &i_cSkip
                #Else
                    Define Bar (i) Of &i_menu Prompt i_cOpz Key &k &i_cSkip
                #Endif
            Else
                #If Version(5)>600
                    *--- Zucchetti Aulla Inizio - Window Menu
                    Define Pad (i_padname) Of &i_menu Prompt i_cOpz &mPosition Picture i_cPicture Key &k &i_cSkip
                    *--- Zucchetti Aulla Fine - Window Menu
                #Else
                    Define Pad (i_padname) Of &i_menu Prompt i_cOpz Key &k &i_cSkip
                #Endif
                *--- Zucchetti Aulla Inizio - Window Menu
                Define Pad (i_padname) Of &i_menu Prompt i_cOpz &mPosition Picture i_cPicture Key &k &i_cSkip
                *--- Zucchetti Aulla Fine - Window Menu
            Endif
        Else
            If i_bModuleInstalled
                If bContext
                    Define Bar (i) Of &i_menu Prompt "\-"
                Else
                    #If Version(5)>600
                        *--- Zucchetti Aulla Inizio - Window Menu
                        Define Pad (i_padname) Of &i_menu Prompt i_cOpz &mPosition Picture i_cPicture Key &k Skip
                        *--- Zucchetti Aulla Fine - Window Menu
                    #Else
                        Define Pad (i_padname) Of &i_menu Prompt i_cOpz Key &k Skip
                    #Endif
                Endif
            Endif
        Endif
        Do Case
            Case i_action=1
                i_subname = Strtran(Trim(i_subname),'#',' with ')
                If "'"$i_subname
                    If i_bModuleInstalled Then
                        If bContext
                            On Selection Bar (i) Of &i_menu Do mhit In cp_menu With "&i_subname"
                        Else
                            * --- Zucchetti Aulla, Gestione recenti, aggiunto Prompt()
                            On Selection Pad (i_padname) Of &i_menu Do mhit In cp_menu With "&i_subname", Prompt()
                        Endif
                    Endif
                Else
                    If i_bModuleInstalled Then
                        If bContext
                            On Selection Bar (i) Of &i_menu Do mhit In cp_menu With '&i_subname'
                        Else
                            * --- Zucchetti Aulla, Gestione recenti, aggiunto Prompt()
                            On Selection Pad (i_padname) Of &i_menu Do mhit In cp_menu With '&i_subname', Prompt()
                        Endif
                    Endif
                Endif
            Case i_action=2 Or i_action=3
                If i_bModuleInstalled Then
                    If bContext
                        On Bar (i) Of &i_menu Activate Popup (i_subname)
                    Else
                        On Pad (i_padname) Of &i_menu Activate Popup (i_subname)
                    Endif
                Endif
        Endcase
    Else
        If i_action<>9
            #If Version(5)>600
                Define Bar i Of (i_menu) Prompt '   '+i_cOpz+'   ' &i_cSkip  Picture i_cPicture
            #Else
                Define Bar i Of (i_menu) Prompt '   '+i_cOpz+'   ' &i_cSkip
            #Endif
        Else
            #If Version(5)>600
                Define Bar i Of (i_menu) Prompt "\-"  Picture i_cPicture
            #Else
                Define Bar i Of (i_menu) Prompt "\-"
            #Endif
        Endif
        Do Case
            Case i_action=1
                i_subname = Strtran(Trim(i_subname),'#',' with ')
                If Not(Empty(i_subname))
                    If "'"$i_subname
                        * --- Zucchetti Aulla, Gestione recenti, aggiunto Prompt()
                        On Selection Bar (i) Of (i_menu) Do mhit In cp_menu With "&i_subname", Prompt()
                    Else
                        * --- Zucchetti Aulla, Gestione recenti, aggiunto Prompt()
                        On Selection Bar (i) Of (i_menu) Do mhit In cp_menu With '&i_subname', Prompt()
                    Endif
                Endif
            Case i_action=2 Or i_action=3
                If i_bModuleInstalled Then
                    On Bar (i) Of (i_menu) Activate Popup (i_subname)
                Endif
        Endcase
    Endif
    On Error &oOldErr
    Return


    * --- Questa procedura, dato un file di men� definito lo trasforma in
    * --- un temporaneo Visual Fox Pro (cCursorName)
Procedure  Vmn_To_Cur( cMenuFile, cCursorName )
    Local xcg,nCount,i_cName, i_nLevel,i_cGest,i_nDir,n_i,i_nElement,i_bAbilitato,i_cProg,i_cBitmap,i_cMod,i_cInsertPoint
    Local i_cPos, i_cDeleted,bErrIns, i_olderr,c_read, cLvlKey,cInsertion, noldLevel
    Local n_k,varTemp
    Local i_cNote, i_cMru, i_cTearOff, l_n
    * --- Controllo che il cursore non sia gia presente..
    If Vartype(cCursorName)<>'C' Or Vartype(cMenuFile)<>'C'
        CP_MSG(cp_Translate(MSG_CANNOT_LOAD_VISUAL_MENU_CL) + Chr(13)+ cp_Translate(MSG_VERIFY_PARAMETERS),.F.)
        Return .F.
    Endif

    If Used( cCursorName )
        CP_MSG(cp_Translate(MSG_CANNOT_LOAD_VISUAL_MENU_CL) + Chr(13)+ CP_MSGFORMAT(MSG_CURSOR__ALREADY_EXISTS,cCursorName),.F.)
        Return .F.
    Endif

    * --- Carico il men� in memoria...
    c_read=cp_GetPFile(cMenuFile)
    xcg=Createobject('pxcg','load',c_read)

    * --- Creo il cursore...
    cp_Create_Cur_Menu(cCursorName)

    Do new_root In Vu_build With cCursorName
    xcg.LoadHeader("CodePainter Revolution Menu",4,1)
    If xcg.nMajor>=3
        xcg.Load("C","")
        xcg.Load("C","")
        xcg.Load("C","")
        xcg.Load("C","")
        xcg.Load("C","")
    Endif
    nCount = xcg.Load("N",0)
    If xcg.nMajor>=3
        xcg.Eoc()
    Endif
    i_cName = xcg.Load("C","")
    i_nLevel = xcg.Load("N",0)
    i_cGest = xcg.Load("C","")
    i_nDir = xcg.Load("N",0)
    If xcg.nMajor>=3
        xcg.Load("N",0)
        xcg.Load("N",0)
        xcg.Load("C","")
        xcg.Load("L",.T.)
        xcg.Load("N",0)
        xcg.Load("C","")
        xcg.Load("C","")
        xcg.Load("C","")
        xcg.Load("C","")
        xcg.Load("C","")
        xcg.Load("C","")
        xcg.Load("C","")
        xcg.Load("D",Ctod("  /  /  "))
        xcg.Load("D",Ctod("  /  /  "))
    Endif
    xcg.Eoc()
    * --- al massimo gestiamo fino al livello 10 per quanto riguarda
    * --- la costruzione del path della voce...
    Dimension VociMenu(10)

    cLvlKey = "001"
    noldLevel = 0
    n_i=1
    Do While n_i<=nCount-1
        i_cName = xcg.Load("C","")
        i_nLevel = xcg.Load("N",0)
        i_cGest = xcg.Load("C","")
        i_nDir = xcg.Load("N",0)
        If xcg.nMajor>=3
            xcg.Load("N",0)
            i_nElement = xcg.Load("N",0)
            xcg.Load("C","")
            i_bAbilitato = xcg.Load("L",.T.)
            i_cProg = xcg.Load("N",0)
            i_cBitmap = xcg.Load("C","")
            xcg.Load("C","")
            i_cMod = xcg.Load("C","")
            * --- Caricamento Punto d'Inserzione
            i_cInsertPoint = Alltrim(Strtran(xcg.Load("C",""),"@"))
            If Not Empty(i_cInsertPoint)
                i_cDeleted = Alltrim(Right(i_cInsertPoint,1))
                i_cInsertPoint = Alltrim(Left(i_cInsertPoint,Rat("�",i_cInsertPoint,3)-1))
                i_cPos = Alltrim(Right(i_cInsertPoint,1))
                i_cInsertPoint = Alltrim(Strtran(i_cInsertPoint,"###"+i_cPos))
            Else
                i_cPos = ""
                i_cDeleted = "T"
            Endif
            i_cNote=xcg.Load("C","")	&&Note Utente
            *--- Nelle note tecniche inserisco MRU e Tear Off
            #If Version(5)>=900
                l_n = Alines(l_aCMD, xcg.Load("C",""),1, "|")
            #Else
                Local cText
                * --- Sostituisco le occorrenze di | con Chr(13)+Chr(10)
                cText=Strtran(xcg.Load("C","") , '|' , Chr(13)+Chr(10))
                l_n = Alines(l_aCMD, cText )
            #Endif
            i_cMru = 'N'
            i_cTearOff = 'N'
            If l_n=2
                i_cMru = Iif(l_aCMD[1] = 'MRU=S', 'S', 'N')
                i_cTearOff = Iif(l_aCMD[2] = 'Tear Off=S', 'S', 'N')
            Endif
            xcg.Load("C","")
        Endif
        xcg.Eoc()
        i_nDir = Iif(i_cName="------------",4,i_nDir)
        * ---- Determino LvlKey e path assoluto voce...
        If i_nLevel > noldLevel
            If i_nLevel<>1
                cLvlKey = cLvlKey+".001"
                VociMenu[i_nLEVEL]=Upper(Alltrim(i_cName))
            Endif
        Else
            If i_nLevel=noldLevel
                varTemp = Alltrim(Substr(cLvlKey,Rat(".",cLvlKey)+1))
                varTemp = Int(Val(varTemp))+1
                cLvlKey =Alltrim(Substr(cLvlKey,1,Rat('.',cLvlKey)))+Padl(Alltrim(Str(varTemp)),3,'0')
            Else
                * --- i_nLEVEL < noldLevel
                varTemp = Alltrim(Substr(cLvlKey,1,At(".",cLvlKey,i_nLevel)-1))
                varTemp = Alltrim(Substr(varTemp,Rat(".",varTemp)+1))
                varTemp = Int(Val(varTemp))+1
                cLvlKey =Alltrim(Substr(cLvlKey,1,At('.',cLvlKey,i_nLevel-1)))+Padl(Alltrim(Str(varTemp)),3,'0')
            Endif
            * --- Cmq ricostruisco
            *varTemp = int(val(varTemp))+1
            *cLvlKey =alltrim(substr(cLvlKey,1,at('.',cLvlKey,LEVEL-1)))+PADL(alltrim(str(varTemp)),3,'0')
            VociMenu[i_nLEVEL]=Upper(Alltrim(i_cName))
        Endif
        noldLevel = i_nLevel
        n_k=2
        cInsertion = ""
        Do While n_k<=i_nLevel
            cInsertion = cInsertion+Iif(Empty(cInsertion),"",g_MENUSEP)+VociMenu[n_k]
            n_k=n_k+1
        Enddo
        cInsertion=Strtran(cInsertion,"&")

        i_olderr=On('ERROR')
        bErrIns=.F.
        On Error bErrIns = .T.
        * --- Determino il bitmap corretto, se vuoto allora mi baso sul campo
        * --- directory, se 2 nodo altrimenti foglia.
        If Empty( i_cBitmap )
            i_cBitmap=Iif( i_nDir=2, g_MNODEBMP ,g_MLEAFBMP )
        Endif
        Insert Into (cCursorName) (VOCEMENU,Level,NAMEPROC,Directory,ELEMENTI,Id,Enabled,PROGRESS,CPBmpName,MODULO,INS_POINT,POSITION,Deleted,LVLKEY,INSERTION, Note, MRU, TEAROFF);
            VALUES (i_cName,Int(i_nLevel),i_cGest,Int(i_nDir),Int(i_nElement),Id,i_bAbilitato,Int(i_cProg),i_cBitmap,i_cMod,i_cInsertPoint,i_cPos,i_cDeleted,Alltrim(cLvlKey),cInsertion, i_cNote, i_cMru, i_cTearOff)
        On Error &i_olderr
        If bErrIns
            CP_MSG(cp_Translate(MSG_ERROR_LOADING_DATA_CL) + Message(),.F.)
            Return .F.
        Endif
        n_i=n_i+1
    Enddo
    * ---- Rendo Scrivibile il cursore
    Local n_Area,cName

    cName = Sys(2015)
    Select( cCursorName )
    n_Area = Select()
    Use Dbf() Again In 0 Alias ( cName )
    Use
    Select( cName )
    Use Dbf() Again In ( n_Area ) Alias ( cCursorName )
    Use
    Select( cCursorName )

    * --- per finire ordino il risultato sul LvlKey, senza questo ordinamento
    * --- la merge avrebbe problemi
    Select ( cCursorName )
    Index On LVLKEY Tag LVLKEY COLLATE "MACHINE"
    Return .T.

Endproc

* ---- Dati due cursori contenenti men�, la procedura
* ---- mergia sul primo il secondo.
* ---- L'analisi del secondo mmen� si ferma al primo livello!
Procedure Merge( cDefa, cMerge )
    Local  cKeyMerge, cInsert , cKeyDefa, cKeyDefaOld, nPos, bCanc, nPasso, cInsertPath, cCurs_Name

    * --- Controlli formali
    If Vartype(cDefa)<>'C' Or Vartype(cMerge)<>'C'
        CP_MSG(cp_Translate(MSG_CANNOT_MERGE_VISUAL_MENU_CL) + Chr(13)+ cp_Translate(MSG_VERIFY_PARAMETERS),.F.)
        Return .F.
    Endif

    If Not Used( cDefa ) Or Not Used( cMerge )
        CP_MSG(cp_Translate(MSG_CANNOT_MERGE_VISUAL_MENU_CL) + Chr(13)+ cp_Translate(MSG_CURSORS_NOT_PRESENTS),.F.)
        Return .F.
    Endif

    * ---- Marco tutte le righe come da non passare...
    Select (cMerge)
    Replace PROGRESS With -1 For 1=1
    Go Top
    * ---- Scorro tutti gli elementi di primo livello (LEVEL=2) del men� da mergare...
    * ---- Una voce di men� di primo livello senza aggancio non verr� mai mergiata
    cCurs_Name=Sys(2015)
    Select LVLKEY,INS_POINT,POSITION,Deleted From (cMerge)  ;
        WHERE Level=2 And Not Empty(INS_POINT) And Not Deleted();
        INTO Cursor (cCurs_Name) NOFILTER

    Select (cCurs_Name)
    Scan
        cKeyMerge=Alltrim(&cCurs_Name..LVLKEY)
        cInsert=&cCurs_Name..INS_POINT
        * ---- Cerco nel men� principale il punto di inserzione..
        If Left(cInsert,1)==g_MENUSEP Then
        	cInsert=Padr(Substr(cInsert,2),Len(cInsert))
        Endif
        Select (cDefa)
        Go Top
        Locate For INSERTION==cInsert
        * ---- Se non lo trovo passo al prossimo...
        * ---- se lo trovo invece devo capire dove agganciarlo..
        If Found()
            * ---- E' da cancellare ('F') o da inserire ('T') ?
            cKeyDefa=Alltrim(&cDefa..LVLKEY)
            * --- la posizione dell'ultimo punto � fondamentale per determinare la seq. dei fratelli..
            * --- da qua in poi non cambia, rimango sempre a questo livello...
            nPos=Rat('.',cKeyDefa)
            If &cCurs_Name..Deleted='F'
                * --- da cancelllare, elimino dal men� principale tutti gli elementi
                * --- che hanno lvlkey che inizia per la chiave trovata
                Delete From (cDefa) Where Left(LVLKEY,Len(cKeyDefa))=cKeyDefa
                bCanc=.T.
                * --- memorizzo il primo fratello da spostare
                cKeyDefaOld=cKeyDefa
                * --- Ho cancellato l'i-esimo elemento devo partire dall'i-esimo+1..
                cKeyDefa=Left(cKeyDefa,nPos) + Padl(Alltrim(Str(Val(Right(cKeyDefa,Len(cKeyDefa)-nPos))+1,3,0)),3,'0')
                * --- Se cancello l'ultimo elemento non devo svolgere spostamenti
                bUltCanc=.T.
            Else
                bCanc=.F.
                cInsertPath=Left(&cDefa..INSERTION , Rat( g_MENUSEP , &cDefa..INSERTION)-1 )
                * --- da inserire, prima ('P') o dopo ('D') ?
                * --- cmq devo shiftare in avanti tutti i fratelli del nodo successivi
                * --- Se prima, parto da quello individuato, se dopo, parto dal successivo
                If &cCurs_Name..POSITION='D'
                    * --- Dopo (determino successivo): quindi aumento di uno l'ultimo livello
                    cKeyDefa=Left(cKeyDefa,nPos) + Padl(Alltrim(Str(Val(Right(cKeyDefa,Len(cKeyDefa)-nPos))+1,3,0)),3,'0')
                Endif
                Select (cMerge)
                Replace LVLKEY With cKeyDefa + Right( LVLKEY , Len(LVLKEY)-Len( cKeyMerge ) ), INSERTION With IIF (NOT EMPTY(cInsertPath),cInsertPath +g_MENUSEP+Alltrim(INSERTION),Alltrim(INSERTION) );
                    PROGRESS With 1 ;
                    FOR Left(LVLKEY,Len(Alltrim(cKeyMerge)))=Alltrim(cKeyMerge) And PROGRESS=-1
                * --- memorizzo il primo fratello da spostare
                cKeyDefaOld=cKeyDefa
                bUltCanc=.F.
            Endif

            * --- Devo recuperare il primo numero libero tra i fratelli..
            Do While .T.
                Select (cDefa)
                Locate For LVLKEY=cKeyDefa
                * --- Se non lo trovo sono arrivato all'ultimo fratello oppure ho incontrato
                * --- un buco (caso eliminazione)
                If Not Found()
                    Exit
                Else
                    * ---- logicamente questa operazione corrisponde a ckeydefa=ckeydefa+1
                    cKeyDefa=Left(cKeyDefa,nPos) + Padl(Alltrim(Str(Val(Right(cKeyDefa,Len(cKeyDefa)-nPos))+1,3,0)),3,'0')
                    bUltCanc=.F.
                Endif
            Enddo
            * --- Se uguali significa che l'elemento da inserire non esiste nel default (es. dopo ultimo..)
            * --- quindi non devo fare niente sul default..
            If cKeyDefaOld<>cKeyDefa And Not bUltCanc
                * ---- Lo spostamento pu� essere dal basso verso l'alto (inserimento nuova voce)
                * ---- Devo spostare di uno tutti i fratelli tra ckeyDefaOld e cKeyDefa
                * ---- (dal basso verso l'alto 10 diventa 11,9 diventa 10,8 diventa 9,...)
                * ---- oppure cancellazione quindi per avere le voci in sequenza devo riempire il buco
                * ----- rinominando dall'alto verso il basso quindi 3 diventa 2,  4 diventa 3, 5 diventa 4...
                Local cUpd_key,cFilterKey,cTmp
                If bCanc
                    * --- cancellazione
                    nPasso=1
                    * ---- logicamente questa operazione corrisponde a ckeydefa=ckeydefa-1
                    * ---- la While sopra arriva la primo numero non utilizzato, parto quindi dal precedente
                    cKeyDefa=Left(cKeyDefa,nPos) + Padl(Alltrim(Str(Val(Right(cKeyDefa,Len(cKeyDefa)-nPos))-1,3,0)),3,'0')
                    * ---- Swappo cKeyDefa con cKeyDefaold
                    cTmp=cKeyDefa
                    cKeyDefa=cKeyDefaOld
                    cKeyDefaOld=cTmp
                Else
                    * --- Inserimento nuova voce
                    nPasso=-1
                Endif
                Do While .T.
                    * --- controllo se cKeyDefa+1 esiste, ovvero se cKeyDefa ha dei fratelli.
                    * --- prima salvo la chiave corrente
                    cUpd_key=cKeyDefa
                    * ---- logicamente questa operazione corrisponde a ckeydefa=ckeydefa+nPasso
                    cKeyDefa=Left(cKeyDefa,nPos) + Padl(Alltrim(Str(Val(Right(cKeyDefa,Len(cKeyDefa)-nPos))+nPasso,3,0)),3,'0')
                    cFilterKey=cKeyDefa
                    Select (cDefa)
                    Replace LVLKEY With cUpd_key+Right(LVLKEY,Len(LVLKEY)-Len(cUpd_key)) ;
                        FOR Left(LVLKEY,Len(cFilterKey))=cFilterKey
                    * --- Se raggiungo cKeyDefaOld esco...
                    If cFilterKey=cKeyDefaOld
                        Exit
                    Endif
                Enddo
            Endif

        Endif
        * --- Ho aggiornato la chiave, se non cancellazione aggiorno il men� principale..
        * --- aggiungo le voci in cDefa e le cancello da cMerge per non
        * --- ributtarle dentro
        If Not bCanc
            #If Version(5)>=800
                Insert Into (cDefa) Select * From (cMerge) Where PROGRESS=1 And Not Deleted()
            #Else
                Select * From (cMerge) Where PROGRESS=1 And Not Deleted() Into Cursor __merge__
                Scan
                    Scatter To r_merge Memo
                    Sele (cDefa)
                    Append Blank
                    Gather From r_merge Memo
                Endscan
                If Used('__merge__')
                    Sele __merge__
                    Use
                Endif
            #Endif
            Delete From (cMerge) Where PROGRESS=1
        Endif
    Endscan

    * --- Chiudo il temporaneo...
    If Used(cCurs_Name)
        Select(cCurs_Name)
        Use
    Endif

Endproc


* ---- Routine per gneerare il men� completo
* ---- Scorre tutti i moduli contenuti in i_cModules, e per ognuno
* ---- svolge il merge...
* ---- Se bNoCustom posto a .f. non carica eventuali men� differenziali utente
Procedure Merge_all( cDefa , cModules , bNoCustom )

    *bnoCustom=.t.
    If Isalt()
        If Right(cModules,4)<>'ALTE'
            cModules=Strtran(cModules,'ALTE,','')+',ALTE'
        Endif
        If Right(cModules,4)<>'ANTI' And g_ANTI='S'
            cModules=Strtran(cModules,'ANTI,','')+',ANTI'
        Endif
        If Right(cModules,4)<>'BIBL' And g_BIBL='S'
            cModules=Strtran(cModules,'BIBL,','')+',BIBL'
        Endif
    Endif
    * --- Controlli formali
    If Vartype(cDefa)<>'C' Or Vartype(cModules)<>'C'
        CP_MSG(cp_Translate(MSG_CANNOT_MERGE_VISUAL_MENU_MODULES_CL) + Chr(13)+ cp_Translate(MSG_VERIFY_PARAMETERS),.F.)
        Return .F.
    Endif

    If Not Used( cDefa )
        CP_MSG(cp_Translate(MSG_CANNOT_MERGE_VISUAL_MENU_MODULES_CL) + Chr(13)+ cp_Translate(MSG_CURSOR_MISSING),.F.)

        Return .F.
    Endif
    Local cPathTemp, n_k, cModulo, nMod, cMenuFile, bRes, cTmpModulo, bExistMenu,lmod,noc

    nMod=Occurs(',',cModules)+1
    lmod=cModules
    noc=At(',',lmod)

    For n_k=1 To nMod
        * --- Determino nome modulo...
        If n_k<nMod
            cModulo=Left(lmod,noc-1)
        Else
            * --- L'ultimo lo calcolo direttamente ...
            cModulo=Alltrim(lmod)
        Endif
        lmod=Substr(lmod,noc+1)
        noc=At(',',lmod)
        * --- nome del temporaneo Fox d'appoggio
        cTmpModulo='_tmp_'+cModulo

        * --- nome del visual men�
        cPathTemp = cHomeDir+'..\'
        cMenuFile=cPathTemp+cModulo+'\'+cModulo+'_default.vmn'
        bExistMenu=.T.
        If Not cp_fileexist(cMenuFile,.T.)
            cMenuFile=cPathTemp+cModulo+'\EXE\'+cModulo+'_default.vmn'
            If Not cp_fileexist(cMenuFile,.T.)
                * --- Zucchetti Aulla Inizio, ricerca anche in cartella superiore alla cartella che
                * --- contiene l'applicativo (era bExistMenu=.f.)
                If Not Empty( p_Application )
                    cPathTemp = cHomeDir+'..\..\'
                    cMenuFile=cPathTemp+cModulo+'\EXE\'+cModulo+'_default.vmn'
                    bExistMenu=cp_fileexist(cMenuFile,.T.)
                Else
                    * --- Zucchetti Aulla Fine
                    * ---- Gestione eccezione (da rendere opzionale ?)
                    bExistMenu=.F.
                    * --- Zucchetti Aulla Inizio
                Endif
                * --- Zucchetti Aulla fine
            Endif
        Endif
        * ---- Se ho trovato il men� lo carico...
        If 	bExistMenu
            bRes=Vmn_To_Cur( cMenuFile, cTmpModulo )
            If bRes
                * ---- Mergio il cursore creato con il men� principale
                bRes=Merge( cDefa, cTmpModulo )
                * ---- Rimuovo il temporaneo vfp
                If Used(cTmpModulo)
                    Select (cTmpModulo)
                    Use
                Endif
                If Not bRes
                    * ---- Gestione errore caricamento
                    CP_MSG(CP_MSGFORMAT(MSG_ERROR_MERGING_MENU_MODULE__, cModulo),.F.)
                    Return .F.
                Endif
            Else
                * ---- Gestione errore caricamento
                CP_MSG(CP_MSGFORMAT(MSG_ERROR_LOADING_MENU_MODULE__,cModulo),.F.)
                Return .F.
            Endif
        Endif

    Endfor

    * --- Se non disabilitato carico il men� Custom...
    If Not bNoCustom
        * ---- Carico il men� custom di applicazione se esiste
        cMenuFile=cp_FindNamedDefaultFile('Application','vmn')
        * --- nome del temporaneo Fox d'appoggio
        cTmpModulo='_tmp_Application'
        If Not Empty(cMenuFile)
            bRes=Vmn_To_Cur( cMenuFile+'.vmn', cTmpModulo )
            If bRes
                * ---- Mergio il cursore creato con il men� principale
                bRes=Merge( cDefa, cTmpModulo )
                * ---- Rimuovo il temporaneo vfp
                If Used(cTmpModulo)
                    Select (cTmpModulo)
                    Use
                Endif
                If Not bRes
                    * ---- Gestione errore caricamento
                    CP_MSG(cp_Translate(MSG_ERROR_MERGING_MENU_CUSTOM),.F.)
                    Return .F.
                Endif
            Else
                * ---- Gestione errore caricamento
                CP_MSG(cp_Translate(MSG_ERROR_LOADING_MENU_CUSTOM), .F.)
                Return .F.
            Endif
        Endif

        * ---- Carico il men� custom di azienda se esiste
        cMenuFile=cp_FindNamedDefaultFile('Application_'+Trim(i_codazi),'vmn')
        * --- nome del temporaneo Fox d'appoggio
        cTmpModulo='_tmp_Application_'+Trim(i_codazi)
        If Not Empty(cMenuFile)
            bRes=Vmn_To_Cur( cMenuFile+'.vmn', cTmpModulo )
            If bRes
                * ---- Mergio il cursore creato con il men� principale
                bRes=Merge( cDefa, cTmpModulo )
                * ---- Rimuovo il temporaneo vfp
                If Used(cTmpModulo)
                    Select (cTmpModulo)
                    Use
                Endif
                If Not bRes
                    * ---- Gestione errore caricamento
                    CP_MSG(cp_Translate(MSG_ERROR_MERGING_MENU_CUSTOM),.F.)
                    Return .F.
                Endif
            Else
                * ---- Gestione errore caricamento
                CP_MSG(cp_Translate(MSG_ERROR_LOADING_MENU_CUSTOM), .F.)
                Return .F.
            Endif
        Endif

        cMenuFile=cp_FindNamedDefaultFile('custom','vmn')
        * --- nome del temporaneo Fox d'appoggio
        cTmpModulo='_tmp_Custom'
        If Not Empty(cMenuFile)
            bRes=Vmn_To_Cur( cMenuFile+'.vmn', cTmpModulo )
            If bRes
                * ---- Mergio il cursore creato con il men� principale
                bRes=Merge( cDefa, cTmpModulo )
                * ---- Rimuovo il temporaneo vfp
                If Used(cTmpModulo)
                    Select (cTmpModulo)
                    Use
                Endif
                If Not bRes
                    * ---- Gestione errore caricamento
                    CP_MSG(cp_Translate(MSG_ERROR_MERGING_MENU_CUSTOM),.F.)
                    Return .F.
                Endif
            Else
                * ---- Gestione errore caricamento
                CP_MSG(cp_Translate(MSG_ERROR_LOADING_MENU_CUSTOM), .F.)
                Return .F.
            Endif
        Endif
    Endif

Endproc

* ---- Dato un temporaneo lo trasforma nel men� principale dell'applicazione
Procedure CaricaMenu(cDefa,bHide)
    If Not Used( cDefa )
        CP_MSG(cp_Translate(CANNOT_CREATE_MASTER_MENU_CL) + Chr(13)+ cp_Translate(MSG_CURSOR_MISSING),.F.)
        Return .F.
    Endif
    If i_VisualTheme<>-1 And Vartype(_Screen.cp_ThemesManager)=="O"
        CP_MSG(cp_Translate(MSG_LOADING_MENU),.T.,.F.)
        Local l_Menu
        Set Sysmenu Off
        If Vartype(i_MenuToolbar)=='O' And !Isnull(i_MenuToolbar)
            i_MenuToolbar.Visible=.F.
            cb_Destroy(i_MenuToolbar.HWnd)
            i_MenuToolbar.oPopupMenu.oParentObject=.Null.
            i_MenuToolbar.Destroy()
            i_MenuToolbar = .Null.
            Release i_MenuToolbar
        Endif
        m.l_Menu = Createobject("PopupMenu", cDefa)
        l_Menu.InitMenuFromCursor()
        Public i_MenuToolbar
        i_MenuToolbar= Createobject("Menubar", l_Menu)
        If i_bWindowMenu
            ShowWinMenu()
        Endif
        i_MenuToolbar.Dock(0,-1,0)
        If i_bMenuFix
            i_MenuToolbar.Movable=.F.
            i_MenuToolbar.Gripper = .F.
        Endif
        i_MenuToolbar.Visible= g_bShowMenu
        Wait Clear
    Else
        *--- VFP STD
        If At('cp_menu',Lower(Set('proc')))=0
            Set Proc To cp_menu Additive
        Endif

        Resetmenu()
        Release Proc cp_menu
        Release Pad _Msm_edit Of _Msysmenu
        Release Pad _mview Of _Msysmenu

        *--- Zucchetti Aulla Inizio - Window Menu
        If i_VisualTheme = -1 And i_bWindowMenu
            ShowWinMenu()
        Endif
        *--- Zucchetti Aulla Fine - Window Menu
        Set Sysmenu Automatic
        * nascondi menu al caricamento
        If bHide
            Hide Menu _Msysmenu
        Endif

        Select(cDefa)
        Go Top
        Scan For Not Deleted()
            Local cPrompt, nAction,cAction, nIndex , cMod, bEnabled, cPadName, cMenu, bMakePopUp,cPicture
            cPrompt=Alltrim(VOCEMENU)
            nAction=Directory
            nIndex=Val( Right(Alltrim(LVLKEY),3) )
            cMod=Alltrim(MODULO)
            bEnabled=Enabled
            cPicture=Alltrim(CPBmpName)
            * ---- Livello elemento...
            If Occurs('.',LVLKEY)=1
                * --- Elemento del men� a livello pi� alto (Archivi/Contabilit�/../Utility)
                cPadName='MP'+Strtran(Alltrim(LVLKEY) ,'.','_')
                cMenu='_msysMenu'
            Else
                * ---- Elemento interno al men�..
                cPadName=''
                cMenu='PM'+Strtran(Left(Alltrim(LVLKEY),Len(Alltrim(LVLKEY))-4) ,'.','_')
            Endif

            If Directory=2
                * ---- Voce di Men� di PopUp
                cAction='PM'+Strtran(Alltrim(LVLKEY),'.','_')
                bMakePopUp=.T.
            Else
                * ---- Voce di Men� Normale
                cAction=Alltrim(NAMEPROC)
                bMakePopUp=.F.
            Endif

            * --- e' una voce di pop up, definisco il pop up..
            If bMakePopUp
                Define Popup (cAction) In Screen
            Endif
            * ---- Creo la voce di men�
            LoadMenuItem(cMenu,cPadName,cPrompt,nAction,cAction,nIndex,cMod,bEnabled,cPicture)
        Endscan

        *SET SYSMENU Automatic
        If bHide
            Show Menu _Msysmenu
        Endif
        If i_VisualTheme = -1 And !g_bShowMenu
            Hide Menu _Msysmenu
        Endif
    Endif
Endproc

* --- Crea il Cursore base per il men�
Procedure cp_Create_Cur_Menu(cCursorName)
    Create Cursor (cCursorName) (VOCEMENU C (60), Level N (5,0), NAMEPROC M,Directory N (1), RIFPADRE N (5,0);
        ,ELEMENTI N (5,0),Id C (10), Enabled L, PROGRESS N (5,0), CPBmpName C (254), MODULO M;
        ,INS_POINT C (254),POSITION C (6),Deleted C (1),LVLKEY C (100), INSERTION C (254), Note C(254), MRU C(1), TEAROFF C(1))
Endproc

* --- Zucchetti Aulla - Windows men�
Procedure ShowWinMenu()
    If i_VisualTheme<>-1
        Set Sysmenu To _Medit, _Mwindow
        Local l_WndItem, l_name
        l_WndItem=i_MenuToolbar.addbutton("_window_")
        l_WndItem.nButtonType=3
        l_WndItem.cBuildCustomList="OnBuildCustomList_Wnd"
        l_WndItem.Caption=cp_Translate("&Window")
        l_name = "ON KEY LABEL ALT+W i_MenuToolbar.OnKeyLabel(" + Transform(i_MenuToolbar.CommandBarButtons.Count) + " , 'W')"
        &l_name
        *--- Cerco la voce "?"
        Local l_oBtn, l_i
        For l_i=1 To i_MenuToolbar.CommandBarButtons.Count
            l_oBtn = i_MenuToolbar.CommandBarButtons.Item(l_i)
            If "?"$m.l_oBtn.Caption
                l_WndItem.ZOrder(0)
            Endif
            If m.l_oBtn.Name<>l_WndItem.Name
                m.l_oBtn.ZOrder(0)
            Endif
        Next
        i_MenuToolbar.oMargin.ZOrder(0)
        *--- Search Menu
        If Vartype(i_MenuToolbar.oSearchMenu)='O'
            i_MenuToolbar.oSearchMenu.ZOrder(0)
        Endif
        i_MenuToolbar.nWidthItem = i_MenuToolbar.nWidthItem + l_WndItem.Width
        l_WndItem.Visible= .T.
    Else
        *--- Sistemo il Window Menu: Traduzione, Chiudi Tutto, Nascondi Tutto, Minimizza Tutto, Visualizza..., Recenti
        *--- Elimino le voci che non mi interessano
        *--- Define delle voci
        #Define _HIDEALL 2
        #Define _SHOWALL 7
        #Define _MINALL 3
        #Define _NORMALL 8
        #Define _CLOSEALL 1
        #Define _SP100 4
        #Define _VISUAL 5
        #Define _SP200 6
        #Define _CPTOOLBAR 1
        #Define _DESKTOPBAR 2
        #Define _TOOLMENU 3
        *--- Elimino le voci indesiderate
        Release Bar _MWI_DOCKABLE Of _Mwindow
        Release Bar _Mwi_sp200 Of _Mwindow
        Release Bar _Mwi_clear Of _Mwindow
        Release Bar _Mwi_sp200 Of _Mwindow
        Release Bar _Mwi_cmd Of _Mwindow
        Release Bar _Mwi_view Of _Mwindow
        Release Bar _MWI_PROPERTIES Of _Mwindow
        *--- Traduco le voci restanti
        Define Bar _MWI_CASCADE Of _Mwindow Prompt cp_Translate(MSG_CASCADE) Message cp_Translate(MSG_ARRANGE_WINDOWS_AS_CASCADING_TILES)
        Define Bar _Mwi_arran Of _Mwindow Prompt cp_Translate(MSG_ARRANGE_ALL) Message cp_Translate(MSG_ARRANGE_WINDOWS_AS_NON_OVERLAPPING_TILES)
        Define Bar _Mwi_hide Of _Mwindow Prompt cp_Translate(MSG_HIDE) Message cp_Translate(MSG_HIDE_ACTIVE_WINDOW)
        Define Bar _Mwi_rotat Of _Mwindow Prompt cp_Translate(MSG_SUCCESSIVE) Message cp_Translate(MSG_CYCLE_THROUGH_OPEN_WINDOWS)
        *--- Aggiungo Nuove Voci

        * Hide All
        Define Bar _HIDEALL Of _Mwindow Prompt cp_Translate(MSG_HIDE_ALL) Message cp_Translate(MSG_HIDE_ALL_OPEN_WINDOWS)
        On Selection Bar _HIDEALL Of _Mwindow ActionAll('H')
        * Show All
        Define Bar _SHOWALL Of _Mwindow Prompt cp_Translate(MSG_SHOW_ALL) Message cp_Translate(MSG_SHOW_ALL_OPEN_WINDOWS)
        On Selection Bar _SHOWALL Of _Mwindow ActionAll('S')
        * Min All
        Define Bar _MINALL Of _Mwindow Prompt cp_Translate(MSG_MINIMIZE_ALL) Message cp_Translate(MSG_MINIMIZE_ALL_OPEN_WINDOWS) Key CTRL+F4
        On Selection Bar _MINALL Of _Mwindow ActionAll('M')
        * Normal All
        Define Bar _NORMALL Of _Mwindow Prompt cp_Translate(MSG_RESTORE_ALL) Message cp_Translate(MSG_RESTORE_ALL_OPEN_WINDOWS)
        On Selection Bar _NORMALL Of _Mwindow ActionAll('X')
        * Close All
        Define Bar _CLOSEALL Of _Mwindow Prompt cp_Translate(MSG_CLOSE_ALL) Message cp_Translate(MSG_CLOSE_ALL_OPEN_WINDOWS)
        On Selection Bar _CLOSEALL Of _Mwindow ActionAll('C')
        * Separatore
        Define Bar _SP100 Of _Mwindow Prompt '\-'
        * Visualizza
        Define Bar _VISUAL Of _Mwindow Prompt cp_Translate(MSG_S_VIEW)
        On Bar _VISUAL Of _Mwindow Activate Popup Visualizza
        Define Popup Visualizza SHORTCUT Margin
        * CpToolBar
        Define Bar _CPTOOLBAR Of Visualizza Prompt cp_Translate(MSG_TOOL_BAR) Skip For !i_bShowCPToolBar Message cp_Translate(MSG_SHOW_BS_HIDE_TOOLBAR)
        On Selection Bar _CPTOOLBAR Of Visualizza SetBar(_CPTOOLBAR)
        Set Mark Of Bar _CPTOOLBAR Of Visualizza To i_bShowCPToolBar
        * DeskTopBar
        Define Bar _DESKTOPBAR Of Visualizza Prompt cp_Translate(MSG_APPLICATION_BAR) Skip For !i_bShowDeskTopBar Message cp_Translate(MSG_SHOW_BS_HIDE_APPLICATION_BAR)
        On Selection Bar _DESKTOPBAR Of Visualizza SetBar(_DESKTOPBAR)
        Set Mark Of Bar _DESKTOPBAR Of Visualizza To i_bShowDeskTopBar
        * ToolMenu
        Define Bar _TOOLMENU Of Visualizza Prompt cp_Translate(MSG_TOOL_MENU) Skip For !i_bShowToolMenu Message cp_Translate(MSG_SHOW_BS_HIDE_TOOL_MENU)
        On Selection Bar _TOOLMENU Of Visualizza SetBar(_TOOLMENU)
        Set Mark Of Bar _TOOLMENU Of Visualizza To .F.
        * Separatore
        Define Bar _SP200 Of _Mwindow Prompt '\-'
    Endif
Endproc

*--- Classe per gestire le finestere aperte
Define Class _MENUITEMWnd As _MENUITEM
    Procedure OnBuildCustomList
        Lparameters taItems
        Local l_j, oForm, l_curForm
        l_i=0
        l_curForm = Upper(Wontop())
        For l_j=1 To _Screen.FormCount
            oForm = _Screen.Forms[l_j]
            If Upper(oForm.BaseClass) = "FORM"
                l_i = l_i+ 1
                Dimension taItems[ l_i, 5]
                taItems[ l_i, 1] = Iif(l_i=1, 4, 0)
                taItems[ l_i, 1] = taItems[ l_i, 1]+ Iif(Upper(oForm.Name)==l_curForm, 1, 0)
                taItems[ l_i, 2] = "&"+Transform(l_i)+ " " + Strtran(oForm.Caption, "&", "")
                taItems[ l_i, 3] = "i_oDeskmenu.ShowWnd(_screen.Forms["+Alltrim(Str(l_j))+"])"
                taItems[ l_i, 4] = "cp_Translate('Activate specified window)"
                taItems[ l_i, 5] = 0
            Endif
        Next
        oForm =.Null.
        If l_i = 0
            Return .F.
        Endif
    Endproc
Enddefine

*--- Zucchetti Aulla Fine - Window Menu
