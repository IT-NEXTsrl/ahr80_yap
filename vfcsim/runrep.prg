* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: runrep                                                          *
*              Stampa il report                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-05                                                      *
* Last revis.: 2013-12-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_nomerep,i_TipoRep,i_NomeFil,i_NumeroCopie,i_Printer,i_bStmpFile,i_ReportBehavior
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("trunrep",oParentObject,m.i_nomerep,m.i_TipoRep,m.i_NomeFil,m.i_NumeroCopie,m.i_Printer,m.i_bStmpFile,m.i_ReportBehavior)
return(i_retval)

define class trunrep as StdBatch
  * --- Local variables
  w_XFRXPREVIEW = .f.
  i_nomerep = space(10)
  i_TipoRep = space(10)
  i_NomeFil = space(10)
  i_NumeroCopie = space(10)
  i_Printer = space(10)
  i_bStmpFile = space(10)
  i_ReportBehavior = 0
  w_NomeFil = space(10)
  w_MultiRep = space(10)
  w_CurName = space(10)
  w_CurDir = space(10)
  w_NoReset = space(10)
  w_nNumCopy = space(10)
  w_nomerep = space(10)
  w_i = 0
  w_VarEnv = space(10)
  w_x = 0
  w_VarValue = space(10)
  w_bVarEnv = .f.
  w_nextPrinter = space(10)
  w_curPrinter = space(10)
  w_next = 0
  w_first = .f.
  w_oldPrinter = space(10)
  w_REPORTBEHAVIOR = space(10)
  w_NumVar = 0
  w_GDI_DISABLED = .f.
  w_GDI_FORCED = .f.
  w_RUNREP_SESSION = .NULL.
  w_curPrinter = space(10)
  w_PRINTER_ARRAY_LINE = 0
  w_CurName = space(10)
  w_CurDir = space(10)
  w_oldpath = space(10)
  w_REPORTGDI = .f.
  w_REPORTST = space(1)
  cNOMEREPORTST = .NULL.
  w_SELECTEDST = 0
  nIndexReportst = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- * --- Parametri: w_nomerep = nome del Report da Stampare (FRX)
    *       * ---            i_ObjCaller= si pu� lasciare sempre a NULL
    *       * ---            i_TipoRep = Tipo Scelta stampa effettuata
    *       * ---                        'V' = Video
    *       * ---                        'S' = Stampa
    *       * ---                        'F' = File
    *       * ---            i_NomeFil = Eventuale Nome Del File di Output
    *       * ---            i_nomerep=nome report
    * --- Ogetto utilizzato da XFRX
    LOCAL l_VarName , lcSetConsole , pageDown, pageUp , doesc
    * --- Salvo la stampante per poi rimpostarla
    this.w_oldPrinter = SET("PRINTER", IIF(TYPE("g_VfpDefPrinter")<>"U" and g_VfpDefPrinter, 3, 2))
    * --- Cambio REPORTBEHAVIOR, per tutti i casi diversi da anteprima il valore viene definito quando si esegue la lettura 
    *     cell'associazione report stampante (SetAllPrinter)
    m.lcSetConsole = ALLTRIM(SET("CONSOLE")) 
 SET CONSOLE OFF 
 pageDown =ON("Key", "PGDN") 
 pageUp=ON("Key", "PGUP") 
 doesc =ON("Key", "ESC") 
 ON KEY LABEL PGDN 
 ON KEY LABEL PGUP 
 On Key Label ESC 
    this.w_first = True
    if this.i_TipoRep="V"
      this.w_REPORTBEHAVIOR = SET("REPORTBEHAVIOR")
      this.w_GDI_DISABLED = FALSE
      this.w_GDI_FORCED = FALSE
      if NOT empty(this.i_ReportBehavior)
        do case
          case this.i_ReportBehavior=80
            this.w_GDI_DISABLED = TRUE
          case this.i_ReportBehavior=90
            this.w_GDI_FORCED = TRUE
        endcase
      endif
      * --- Se ho un solo report non uso il multireport
      if this.i_nomerep.GetNumReport()>1 OR (NOT this.w_GDI_DISABLED AND (this.i_nomerep.cReportBehavior="90" OR this.w_GDI_FORCED))
        SET REPORTBEHAVIOR 90
      else
        SET REPORTBEHAVIOR 80
      endif
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Utilizzato da XFRX
    do while this.i_NumeroCopie>0
      this.w_i = 0
      i_NroCopie=this.i_NumeroCopie
      * --- Ciclo su tutte le stampe, !(w_i=l_CountRep AND i_NumeroCopie=1) mi dice se devo utilizzare NOPAGEEJECT 
      do while this.w_i<>-1
        this.w_i = this.i_nomerep.NextReport(this.w_i, this.i_TipoRep="V")
        if this.w_i<>-1
          if this.w_XFRXPREVIEW AND VARTYPE(this.w_RUNREP_SESSION)<>"O"
            this.w_RUNREP_SESSION = CREATEOBJECT("zxfrxMakeDocument")
          endif
          * --- Numero copie
          this.w_nNumCopy = this.i_nomerep.GetNumCopy(this.w_i)
          * --- Setto i_paramqry per permettere l'uso di cp_queryparam()  
          i_paramqry=this.i_nomerep.GetParamqry(this.w_i)
          * --- Creo l'environment con le variabili l_
          this.w_VarEnv = this.i_nomerep.GetVarEnv(this.w_i)
          this.w_bVarEnv = .F.
          if NOT ISNULL(this.w_VarEnv) AND NOT this.w_VarEnv.IsEmpty()
            this.w_NumVar = this.w_VarEnv.GetNumVar()
            * --- Creo le variabili
            this.w_x = 1
            do while this.w_x <= this.w_NumVar
              l_VarName=this.w_VarEnv.GetVarName(this.w_x) 
 &l_VarName=this.w_VarEnv.GetVarValue(this.w_x)
              this.w_x = this.w_x + 1
            enddo
            this.w_bVarEnv = .T.
          endif
          * --- Setto la stampante
          this.w_curPrinter = this.i_nomerep.GetRepPrinter(this.w_i)
          this.w_next = this.i_nomerep.NextReport(this.w_i, this.i_TipoRep="V")
          if this.w_next<>-1
            this.w_nextPrinter = this.i_nomerep.GetRepPrinter(this.w_next)
          else
            this.w_nextPrinter = ""
          endif
          * --- Se passo una stampante uso sempre quella (Postalite, ArchEasy)
          if TYPE("i_Printer")="C" AND NOT EMPTY(this.i_Printer)
            SET PRINTER TO NAME (ALLTRIM(this.i_Printer))
            this.w_nextPrinter = ""
            this.w_curPrinter = ""
          else
            if not(empty(this.w_curPrinter))
               LOCAL l_old_Err, i_err 
 i_err = "" 
 l_old_Err = on("ERROR") 
 on error i_err=MESSAGE() 
 SET PRINTER TO NAME (ALLTRIM(this.w_curPrinter)) 
 on error &l_old_Err 
              if !EMPTY(i_err)
                 ah_ErrorMsg("Stampante non presente. Verr� utilizzata quella di default") 
 SET printer to default
              endif
            else
              SET printer to default
            endif
          endif
          * --- Lancio la stampa
          * --- * ---            w_MultiRep = .T. Esego NOPAGEEJECT
          *       * ---            w_NoReset = .T. Eseguo NoReset del numero pagina
          *       *                w_nNumCopy = Numero copie del report
          this.w_nomerep = cp_GetStdFile(this.i_nomerep.GetReport(this.w_i),"FRX")
          this.w_MultiRep = !((this.w_next=-1 AND (this.i_NumeroCopie=1 OR this.i_TipoRep="V")) OR (this.i_TipoRep="S" AND ALLTRIM(UPPER(this.w_nextPrinter))<>ALLTRIM(UPPER(this.w_curPrinter))))
          this.w_CurName = this.i_nomerep.GetCursorName(this.w_i)
          this.w_NoReset = not this.w_first And this.i_nomerep.GetNoReset(this.w_i)
          this.w_nNumCopy = IIF(this.i_nomerep.bNumCopy or this.i_bStmpFile, 1, this.w_nNumCopy)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_oldPrinter = this.i_nomerep.GetRepPrinter(this.w_i)
          * --- Rivalorizzo le variabili dell' environment in modo da poterle poi interrogare alla fine della stampa
          if this.w_bVarEnv &&Se � true ho creato le variabili quindi le risalvo all"interno dell"oggetto
            this.w_x = 1
            do while this.w_x <=this.w_NumVar
              l_VarName=this.w_VarEnv.GetVarName(this.w_x)
              this.w_VarEnv.SetVarValue(&l_VarName,this.w_x)     
              this.w_x = this.w_x + 1
            enddo
          endif
        endif
        this.w_first = False
      enddo
      this.i_NumeroCopie = IIF(this.i_TipoRep="V" or !this.i_nomerep.bNumCopy, 0, this.i_NumeroCopie - 1)
    enddo
    * --- Controlla se sul sistema c'� la stampante richiesta
    APRINTER( L_PRINTERS_ARRAY )
    this.w_PRINTER_ARRAY_LINE = ASCAN( L_PRINTERS_ARRAY, this.w_oldPrinter,1,ALEN(L_PRINTERS_ARRAY,1),1,9)
    if this.w_PRINTER_ARRAY_LINE <> 0
      * --- Ripristino la stampante originaria
      SET PRINTER TO NAME (ALLTRIM(this.w_oldPrinter))
    else
      this.w_oldPrinter = ""
       set printer to default
    endif
    * --- Ripristino vecchio valore
    if this.i_TipoRep="V"
      SET REPORTBEHAVIOR (this.w_REPORTBEHAVIOR)
    endif
    SET CONSOLE &lcSetConsole 
 ON KEY LABEL PGDN &pageDown 
 ON KEY LABEL PGUP &pageUp 
 ON KEY LABEL ESC &doesc 
 
    this.w_RUNREP_SESSION = .NULL.
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     SET CONSOLE OFF 
 local i_olderr
    * --- ***** Zucchetti Aulla Inizio
    *       * variabile per risolvere problema della perdita del path di default
    *       * nel caso di stampa su PDF
    this.w_oldpath = sys(5)+sys(2003)
    * --- ***** Zucchetti Aulla Fine
    *       *********************************************************
     private i_err
    this.w_CurDir = sys(5)+Sys(2003)
     i_err="" 
 i_olderr=on("ERROR") 
 on error i_err=MESSAGE() 
 SELECT(this.w_CurName) 
 PUBLIC g_CurCursor 
 g_CurCursor=this.w_CurName
    do case
      case this.i_TipoRep = "V"
        * --- Anteprima
        if this.w_XFRXPREVIEW
          push key clear
        endif
         i_curform=.null. 
 PUSH MENU _MSYSMENU
        * --- set sysmenu to _mview
        set sysmenu to
        * --- Se Multireport utilizzo NOPAGEEJECT per non chiudere il job di stampa
        *     KEYBOARD "{Ctrl+F10}"
        if this.w_MultiRep
          if this.w_XFRXPREVIEW
            * --- cp_ReportGdi(w_NOMEREP) controlla se sul report � disativato il gdiplus
            REP_FORM(this, this.w_NOMEREP, "PREVIEW NOPAGEEJECT" + IIF(this.w_NoReset," NORESET",""))
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            if this.w_NoReset
              REPORT FORM (this.w_nomerep) NORESET PREVIEW NOPAGEEJECT
            else
              REPORT FORM (this.w_nomerep) PREVIEW NOPAGEEJECT 
            endif
          endif
        else
          if this.w_XFRXPREVIEW
            REP_FORM(this, this.w_NOMEREP, "PREVIEW" + IIF(this.w_NoReset," NORESET",""))
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            if this.w_NoReset
              REPORT FORM (this.w_nomerep) NORESET PREVIEW
            else
              REPORT FORM (this.w_nomerep) PREVIEW 
            endif
          endif
        endif
        * --- ***** Zucchetti Aulla Inizio
        *           * Risolve problema della perdita del copia/incolla
         set sysmenu to _medit 
        * --- ***** Zucchetti Aulla Fine
        POP MENU _MSYSMENU
        if this.w_XFRXPREVIEW
          pop key
        endif
      case this.i_TipoRep = "S" OR this.i_TipoRep = "O"
        push key 
        * --- * --- Stampa / Stampa con Opzioni
        *           * --- Costruisco il comando REPORT FORM
         LOCAL l_macro
        this.w_x = 1
        do while this.w_x<=this.w_nNumCopy
          l_nomerep=this.w_NOMEREP
          l_macro=""
          if this.i_TipoRep = "S"
            l_macro = l_macro + "NOCONSOLE TO PRINTER "
          else
            l_macro = l_macro + "NOCONSOLE TO PRINTER PROMPT "
          endif
          if this.w_NoReset
            l_macro = l_macro + "NORESET "
          endif
          if this.w_MultiRep OR this.w_x<>this.w_nNumCopy
            l_macro = l_macro + "NOPAGEEJECT"
          endif
          * --- Eseguo la stampa
          if this.w_XFRXPREVIEW
            REP_FORM(this, this.w_NOMEREP, L_MACRO )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            l_macro="REPORT FORM (l_nomerep) "+l_macro
            &l_macro
          endif
          this.w_x = this.w_x + 1
        enddo
         pop key
      case this.i_TipoRep = "F"
        * --- L'opzione TO FILE deve essere utilizzata con REPORTBEHAVIOR = 80
        this.w_REPORTBEHAVIOR = SET("REPORTBEHAVIOR")
        SET REPORTBEHAVIOR 80
        * --- File
        push key 
        if this.w_MultiRep
          if this.w_NoReset
            REPORT FORM (this.w_nomerep) NORESET NOCONSOLE TO FILE (this.i_NomeFil) ASCII NOPAGEEJECT 
          else
            REPORT FORM (this.w_nomerep) NOCONSOLE TO FILE (this.i_NomeFil) ASCII NOPAGEEJECT 
          endif
        else
          if this.w_NoReset
            REPORT FORM (this.w_nomerep) NORESET NOCONSOLE TO FILE (this.i_NomeFil) ASCII
          else
            REPORT FORM (this.w_nomerep) NOCONSOLE TO FILE (this.i_NomeFil) ASCII 
          endif
        endif
         pop key
        SET REPORTBEHAVIOR (this.w_REPORTBEHAVIOR)
    endcase
     RELEASE g_CurCursor 
 on error &i_olderr
    if NOT EMPTY(i_err)
      cp_msg(cp_MsgFormat(MSG_ERROR_EXECUTING_REPORT__2, CHR(13),i_err, this.w_nomerep),.f.)
    endif
    * --- ***** Zucchetti Aulla Inizio
    *       * vengono risettati il path sulla directori di default e la stampante predefinita
    set default to (this.w_oldpath) 
 set printer to default
    * --- ***** Zucchetti Aulla Fine
     CD (this.w_CurDir)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if VARTYPE(g_xfrxpreview)="U"
      public g_xfrxpreview
      g_xfrxpreview = .T.
    endif
    * --- Condizione per GDI
    if vartype(this.oParentObject)="O"
      if lower(this.oParentObject.class)<>"trun2rep"
        this.w_REPORTGDI = ( this.oParentObject.w_TREPORT<>"I" AND !bReportVuoto AND IIF(VARTYPE(this.i_ReportBehavior)="N",this.i_ReportBehavior,90)=90 AND this.oParentObject.cNOMEREPORT.cReportBehavior="90" and (NOT this.oParentObject.cNomeReport.GetText( this.oParentObject.nIndexReport ) AND cp_PrintCondition (this.oParentObject , "Btn_Ante_Edit") ) AND this.oParentObject.w_SELECTED>0)
      else
        this.w_REPORTST = this.oparentobject.oparentobject.w_TREPORT
        this.cNOMEREPORTST = this.oparentobject.oparentobject.cNOMEREPORT
        this.w_SELECTEDST = this.oparentobject.oparentobject.w_SELECTED
        this.w_REPORTGDI = ( this.w_REPORTST<>"I" AND !bReportVuoto AND IIF(VARTYPE(this.i_ReportBehavior)="N",this.i_ReportBehavior,90)=90 AND this.cNOMEREPORTST.cReportBehavior="90" and ( cp_PrintCondition (this.oParentObject.oParentObject , "Btn_Ante_Edit") ) AND this.w_SELECTEDST>0)
      endif
    else
      this.w_REPORTGDI = .f.
    endif
    this.w_XFRXPREVIEW = this.w_REPORTGDI and g_xfrxpreview and ( Type("g_NewXFRX")<>"L" Or g_NewXFRX )
  endproc


  proc Init(oParentObject,i_nomerep,i_TipoRep,i_NomeFil,i_NumeroCopie,i_Printer,i_bStmpFile,i_ReportBehavior)
    this.i_nomerep=i_nomerep
    this.i_TipoRep=i_TipoRep
    this.i_NomeFil=i_NomeFil
    this.i_NumeroCopie=i_NumeroCopie
    this.i_Printer=i_Printer
    this.i_bStmpFile=i_bStmpFile
    this.i_ReportBehavior=i_ReportBehavior
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="i_nomerep,i_TipoRep,i_NomeFil,i_NumeroCopie,i_Printer,i_bStmpFile,i_ReportBehavior"
endproc
