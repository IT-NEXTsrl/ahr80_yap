* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VU_BUILD
* Ver      : 3.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : GiorGio Montali
* Data creazione: 20/03/97
* Aggiornato il : 09/07/2005
* Translated    :
* #%&%#Build:  56
* ----------------------------------------------------------------------------
* Disegnatore di menu
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

* --- Zucchetti aulla inizio Framework monitor
If cp_set_get_right()<2
    cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
    Return
Endif
* --- Zucchetti aulla fine Framework monitor

If Vartype(i_cMenuLoaded)='U'
    Public i_cMenuLoaded
    i_cMenuLoaded=''
Endif

* --- Contiene il nome del cursore che contiene il men� in alto..
If Vartype(i_CUR_MENU)='U'
    Public i_CUR_MENU
    i_CUR_MENU=''
Endif

* --- Separatore voci di men� (�)
If Vartype(g_MENUSEP)='U'
    Public g_MENUSEP
    g_MENUSEP=Chr(172)
Endif

If Vartype(i_bFox26)='L'
    If Vartype(i_cModules)='U'
        Public i_cModules
        i_cModules=''
    Endif
    cp_menu()
Endif

If At('vq_lib',Lower(Set('proc')))=0
    Set Proc To vq_lib Additive
Endif

oDesignmenu=Createobject('menu_toolbar_new',Type("i_bFox26")<>'U',Vartype(_Screen.cp_ThemesManager)=="O" And i_VisualTheme<>-1)

If Type('oDesignmenu')<>'L'
    i_curform=oDesignmenu
    * --- Zucchetti aulla inizio Framework monitor
    If (Type('i_designfile')='C')
        oDesignmenu.cFileName = Alltrim(i_designfile)
        oDesignmenu.EditMenu()
        oDesignmenu.SetTitle()
        oDesignmenu.LoadDoc()
    Else
        * --- Zucchetti aulla fine Framework monitor
        oDesignmenu.EditMenu()
        * --- Zucchetti aulla inizio Framework monitor
    Endif
    * --- Zucchetti aulla fine Framework monitor
Endif
Return

Define Class menu_toolbar_new As microtool_toolbar
    oForm=.Null.
    oThis=.Null.
    HelpContextID=3
    cExt='vmn'
    bFox26=.F.

    bSec1=.T.
    bSec2=.T.
    bSec3=.T.
    bSec4=.T.

    Proc Init(i_bFox26,bStyle)
        DoDefault(m.bStyle)
        This.cToolNAME = cp_Translate(MSG_VISUAL_MENU)
        This.bFox26=i_bFox26
        If Type("i_bFox26")='U'
            cp_GetSecurity(This,'VisualMenu')
        Endif
        This.Dock(i_nCPToolBarPos)
        This.oThis=This
        Return

    Proc EditMenu()
        If !This.bSec1
            cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
            This.Exit()
        Else
            This.Show()
            This.oForm = Cp_DesignMenu(This)
            * ---- Carico la maschera...
            This.oForm.Notifyevent('Avvio')
        Endif
        Return

    Proc Exit()
        * ---- Rimuovo i cursori delle treeview
        Select (This.oForm.w_USRCURSOR)
        Use
        * ---- NON ELIMINO IL TEMPORANEO CHE CONTIENE IL MENU DI SISTEMA
        If This.oForm.w_SYSCURSOR<>i_CUR_MENU
            Select (This.oForm.w_SYSCURSOR)
            Use
        Endif
        * ---- Eseguo Ripristina se non eseguito
        If This.oForm.bTestMenu
            This.oForm.Notifyevent('Ripristina')
        Endif
        This.oForm.EcpQuit()
        This.oForm=.Null.
        This.oThis=.Null.
        i_curform=.Null.
        This.Visible=.F.
        This.Destroy()
        On Key Label RIGHTMOUSE
        Return

    Func GetHelpTopic()
        Return('vu_build')

    Proc SetTitle()
        This.oForm.Caption=cp_Translate(MSG_VISUAL_MENU)+Iif(Not Empty(This.cFileName),cp_Translate(MSG_FS)+' '+This.cFileName,'')
        Return
    Proc ecpSecurity()
        Createobject('stdsProgAuth','VisualMenu')
        Return

    Proc NewDoc()
        * --- spostiamo per sicurezza il focus sulla TreeView di destra...
        This.oForm.w_sysTreeView.SetFocus()
        * --- Dato che non ho modo di selezionare fisicamente il nodo
        * --- del Menu Principale, ma solo simulare il focus, riempio manualmente
        * --- i campi
        This.SetInitValue()
        * --- Se maschera gia aperta azzero la tree view utente (di sinistra)..
        If Used( This.oForm.w_USRCURSOR  )
            Use In Select(This.oForm.w_USRCURSOR)
            * --- Re inizializzo l'elenco delle immagini
            Dimension This.oForm.w_USRTreeView.aBmpIndex[1,2]
            This.oForm.w_USRTreeView.aBmpIndex[1,2]=''
            This.oForm.w_USRTreeView.ImgList.ListImages.Clear
            This.oForm.w_USRTreeView.ImgList.Init()
        Endif
        * --- Inserisco il nome del menu per L'utente
        This.oForm.w_menuutente=cp_Translate(MSG_NEW)
        * --- Alla visualizzazione della maschera inizializza il cursore del menu utente con la voce Menu principale.
        * --- Inizializza anche la USRTreeView
        This.oForm.w_USRCURSOR = Sys(2015)
        * --- Creo un men� vuoto (lancio il click nuovo men�..)
        * Travaso il contenuto nel cursore d'appoggio
        #If Version(5)>=700
            Select * From ( This.oForm.w_SYSCURSOR ) Into Cursor ( This.oForm.w_USRCURSOR ) Where 1=0 NOFILTER Readwrite
        #Else
            Local w_nome
            w_nome = Sys(2015)
            Select * From ( This.oForm.w_SYSCURSOR ) Into Cursor (w_nome) Where 1=0 NOFILTER
            Use Dbf() Again In 0 Alias  ( This.oForm.w_USRCURSOR )
            Use
            Select ( This.oForm.w_USRCURSOR )
        #Endif

        * --- Creo il nodo principale...
        New_Root(This.oForm.w_USRCURSOR)
        This.oForm.w_USRTreeView.cCursor = This.oForm.w_USRCURSOR
        * --- Indicizzo il temporaneo...
        Index On LVLKEY Tag LVLKEY COLLATE "MACHINE"
        This.oForm.Notifyevent("UsrMenu")
        * --- Attivazione campi e bottoni
        This.setInitControl()
        Return

    Proc LoadDoc()
        Local xcg,h,c,l,c_,f_ext
        * --- spostiamo per sicurezza il focus sulla TreeView di destra...
        This.oForm.w_sysTreeView.SetFocus()
        * --- Dato che non ho modo di selezionare fisicamente il nodo
        * --- del Menu Principale, ma solo simulare il focus, riempio manualmente i campi
        This.SetInitValue()
        * --- Errore in caso apertura File non di tipo VMN
        c_=Rat('\',This.cFileName)+1
        fName=Substr(This.cFileName,c_,Len(This.cFileName)-(c_+3))
        * --- Inserisco il nome del menu per L'utente
        This.oForm.w_menuutente=Upper(Alltrim(fName))
        f_ext=Right(Substr(This.cFileName,c_),3)
        If(f_ext<>'VMN')
            cp_ErrorMsg(MSG_CANNOT_OPEN_FILE)
            Return
        Endif
        Use In Select (This.oForm.w_USRCURSOR)
        Do Vmn_To_Cur In vu_exec With This.cFileName, This.oForm.w_USRCURSOR
        This.oForm.Notifyevent('UsrMenu')
        This.SetTitle()
        This.bModified=.T.
        * --- Attivazione campi e bottoni
        This.setInitControl()
        Return

    Proc SaveDoc()
        Local xcg,h,c_,fName
		cp_CreateCacheFile()
        * --- effettuiamo, per sicurezza, un ulteriore salvataggio...
        This.oForm.w_USRTreeView.SetFocus
        c_=Rat('\',This.cFileName)+1
        fName=Substr(This.cFileName,c_,Len(This.cFileName)-(c_+3))
        * --- Inserisco il nome del menu per L'utente
        This.oForm.w_menuutente=Upper(Alltrim(fName))
        * --- lancio la lostFocus per essere sicuro di aver salvato tutti i dati
        This.oForm.Notifyevent('w_BITMAP LostFocus')
        xcg=Createobject('pxcg','save')
        This.Serialize(xcg)
        This.bModified=.F.
        h=Fcreate(This.cFileName)
        =Fwrite(h,xcg.cText)
        =Fclose(h)
        Return

    Proc Serialize(xcg)
        Local i_cInsertPoint, i_cPos,counter, l_oldDateFormat
        xcg.SaveHeader('CodePainter Revolution Menu',3,9)
        xcg.Save('C','')
        xcg.Save('C','')
        xcg.Save('C','')
        xcg.Save('C','')
        xcg.Save('C','')
        xcg.Save('N',This.oForm.w_USRTreeView.oTree.nodes.Count())
        xcg.Eoc()
        * --- Salvataggio file...
        Do cp_Create_Cur_Menu With "__TmpSer__"	In vu_exec

        This.oForm.w_USRTreeView.rifPadre=-1
        This.oForm.w_USRTreeView.contatore=1
        This.oForm.w_USRTreeView.visita(This.oForm.w_USRTreeView.oTree.nodes(1),"__TmpSer__",0)
        Select ("__TmpSer__")
        Go Top
        Scan Rest
            xcg.Save('C',Alltrim(__TmpSer__.VOCEMENU))
            xcg.Save('N',__TmpSer__.Level)
            xcg.Save('C',Alltrim(__TmpSer__.NAMEPROC))
            xcg.Save('N',__TmpSer__.Directory)

            xcg.Save('N',__TmpSer__.rifPadre)
            xcg.Save('N',__TmpSer__.ELEMENTI)
            xcg.Save('C',Alltrim(__TmpSer__.Id))
            xcg.Save('L',__TmpSer__.Enabled)
            xcg.Save('N',Recno('__TmpSer__')-1)
            * --- Gestione bitmap
            xcg.Save('C',Alltrim(__TmpSer__.CPBmpName))
            xcg.Save('C','')
            xcg.Save('C',Alltrim(__TmpSer__.MODULO))
            If Empty(__TmpSer__.INS_POINT)
                xcg.Save('C','')
            Else
                xcg.Save('C','@'+Alltrim(__TmpSer__.INS_POINT)+'###'+Iif(Empty(__TmpSer__.POSITION) Or Upper(Alltrim(__TmpSer__.POSITION))=='#','P',Alltrim(__TmpSer__.POSITION))+'���'+Iif(Empty(__TmpSer__.Deleted),'T',Alltrim(__TmpSer__.Deleted)))
            Endif
            xcg.Save('C',Alltrim(__TmpSer__.Note))	&&Note Utente
            xcg.Save('C','MRU='+Alltrim(__TmpSer__.MRU)+'|'+'Tear Off='+Alltrim(__TmpSer__.TEAROFF))	&&Note Tecniche
            xcg.Save('C','')
            * --- Inserimento data modifica menu per controllo caricamento
            If __TmpSer__.Level = 1
                l_oldDateFormat = Set('DATE')
                Set Date Ansi
                xcg.Save('C',Strtran(Ttoc(Datetime()),'.','-'))
                Set Date &l_oldDateFormat
                xcg.Save('C','')
            Endif
            xcg.Eoc()
        Endscan
        xcg.Eoc()
        Use In Select('__TmpSer__')
        Return

        * --- Carico un menu o ne creo uno da Nuovo, simulo la valorizzazione dei campi
        * --- del menu principale
    Procedure SetInitValue()
        This.oForm.w_VOCEMENU=cp_Translate(MSG_MAIN_MENU)
        This.oForm.w_NAMEPROC=''
        This.oForm.w_INSERTPOINT=''
        This.oForm.w_MODULO=''
        This.oForm.w_BITMAP=g_MNODEBMP
    Endproc

    * --- Simula il fatto di aver selezionato il primo nodo nel Menu di sisnistra:
    * --- inizializza l'indice della TreeView, il valore della directory e lancia
    * --- il metodo di controllo per l'attivazione dei campi e dei bottoni.
    Procedure setInitControl()
        * --- setto l'indice del nodo sul primo nodo sicuramente presente
        This.oForm.w_INDEX=1
        * --- Setto di conseguenza il valore di DIRECTORY come submenu
        This.oForm.w_DIRECTORY=2
        * --- Setto il focus sulla TreeView di sinistra
        This.oForm.w_USRTreeView.SetFocus()
        * --- Lancio la procedura per verifica attivazione campi e controlli
        This.oForm.mEnableControls()
    Endproc

Enddefine

* ---- crea una nuova root...
* ---- nel temporaneo passato..
Proc New_Root(cCursor)
    Insert Into ( cCursor );
        (VOCEMENU,Level,NAMEPROC,Directory,ELEMENTI,Id,Enabled,PROGRESS,CPBmpName,;
        MODULO,INS_POINT,POSITION,Deleted,LVLKEY,INSERTION);
        VALUES (cp_Translate(MSG_MAIN_MENU),1,"",0,0,"",.T.,0, g_MNODEBMP ;
        ,"","","","T","001","")
Endproc
