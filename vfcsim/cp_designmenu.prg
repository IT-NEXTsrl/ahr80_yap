* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_designmenu                                                   *
*              Men� Visuale                                                    *
*                                                                              *
*      Author: GiorGio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language: Visual Fox Pro                                                  *
*          OS: Windows                                                         *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-16                                                      *
* Last revis.: 2013-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_designmenu",oParentObject))

* --- Class definition
define class tcp_designmenu as StdForm
  Top    = 9
  Left   = 64

  * --- Standard Properties
  Width  = 587
  Height = 589
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-01-22"
  HelpContextID=250088799
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_designmenu"
  cComment = "Men� Visuale"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_COND_AGGANCIO = .F.
  w_VOCEMENU = space(60)
  w_NAMEPROC = space(250)
  w_INSERTPOINT = space(254)
  w_POSITION = space(6)
  w_DELETED = .F.
  w_BITMAP = space(254)
  w_MODULO = space(254)
  w_ABILITATO = .F.
  w_MRU = space(1)
  w_TEAROFF = space(1)
  w_NOTE = space(254)
  w_SYSCURSOR = space(10)
  w_MENUNAME = space(25)
  o_MENUNAME = space(25)
  w_INDEX = 0
  w_USRCURSOR = space(10)
  w_DIRECTORY = 0
  w_MENUUTENTE = space(25)
  w_CUR_MENU = space(10)
  w_OLD_MENU = space(25)
  w_USRTreeView = .NULL.
  w_SYSTreeView = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_designmenu
  * --- Disabilito la possibilit� di chiudere la maschera con la crocetta
  * --- in alto a destra
  closable=.f.
  autocenter=.t.
  bTestMenu=.f.
  * --- Disabilito la Toolbar all'attivarsi della maschera
  Proc SetStatus()
   DoDefault()
   oCpToolBar.Enable(.f.)
  EndProc
  
  * --- Disabilito la Toolbar all'attivarsi della maschera
  * --- inoltre valorizzo i_curform facendola puntare alla toolbar
  * --- che ha lanciato la maschera
  Proc Activate()
   i_CurForm=this.oParentObject
   this.SelectCursor()
   oCpToolBar.Enable(.f.)
   *--- tasto destro non funziona, lo devo disabilitare
   ON KEY LABEL RIGHTMOUSE wait window ""
  EndProc
  
  procedure editbtn()
    if this.w_COND_AGGANCIO
     * ---- Se sono sulla Root non ho padri, devo quindi testare il tipo della propriet�
     * ---- prima di utilizzarla
     if TYPE('this.w_USRTreeView.oTree.selectedItem.parent.index')='N' AND this.w_USRTreeView.oTree.selectedItem.parent.index=1
  	   return(.T.) &&(this.w_DIRECTORY<>4 and this.w_VOCEMENU<>'------------')
     ELSE
     	 RETURN(.F.)
     endif	
    else
      return(.F.)
    endif
  endproc
  
    * --- Carica il bitmap, restituisce il path relativo alla EXE..
    proc loadBit
      this.w_BITMAP = STRTRAN(getfile("bmp",cp_Translate(MSG_FILE_NAME+MSG_FS),cp_Translate(MSG_OPEN),0,cp_Translate(MSG_OPEN)),sys(5)+sys(2003)+'\')
      this.notifyEvent('w_BITMAP LostFocus')
    endProc
  
      proc loadMn
        local pathmenu
        pathmenu=getfile("vmn",cp_Translate(MSG_FILE_NAME+MSG_FS),cp_Translate(MSG_OPEN),0,cp_Translate(MSG_OPEN))
        if not empty(pathmenu)
          this.w_menuname=UPPER(alltrim(left(pathmenu,len(pathmenu)-4)))
          this.notifyEvent("Ricarica")
        endif
      endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_designmenuPag1","cp_designmenu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVOCEMENU_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_designmenu
        * --- Diminuisco dimensione Maschera
        IF TYPE("i_bFox26")='L' and i_bFox26
        	this.Parent.Height=415
        endif
    
     * --- Translate interface...
     this.parent.cComment=cp_Translate(MSG_VISUAL_MENU)
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_USRTreeView = this.oPgFrm.Pages(1).oPag.USRTreeView
    this.w_SYSTreeView = this.oPgFrm.Pages(1).oPag.SYSTreeView
    DoDefault()
    proc Destroy()
      this.w_USRTreeView = .NULL.
      this.w_SYSTreeView = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- cp_designmenu
    * Disabilito Gruppi voci di Men�
     this.bNoMenuFunction=.t.
     this.bNoMenuAction=.t.
     this.bNoMenuProperty=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COND_AGGANCIO=.f.
      .w_VOCEMENU=space(60)
      .w_NAMEPROC=space(250)
      .w_INSERTPOINT=space(254)
      .w_POSITION=space(6)
      .w_DELETED=.f.
      .w_BITMAP=space(254)
      .w_MODULO=space(254)
      .w_ABILITATO=.f.
      .w_MRU=space(1)
      .w_TEAROFF=space(1)
      .w_NOTE=space(254)
      .w_SYSCURSOR=space(10)
      .w_MENUNAME=space(25)
      .w_INDEX=0
      .w_USRCURSOR=space(10)
      .w_DIRECTORY=0
      .w_MENUUTENTE=space(25)
      .w_CUR_MENU=space(10)
      .w_OLD_MENU=space(25)
        .w_COND_AGGANCIO = at('DEFAULT', .w_MENUNAME)<>0 Or at(upper( i_CpDic) , .w_MENUNAME)<>0
      .oPgFrm.Page1.oPag.USRTreeView.Calculate()
          .DoRTCalc(2,4,.f.)
        .w_POSITION = 'P'
          .DoRTCalc(6,8,.f.)
        .w_ABILITATO = .T.
      .oPgFrm.Page1.oPag.SYSTreeView.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_51.Calculate(cp_translate(MSG_USER_MENU)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_52.Calculate(cp_translate(MSG_ENTRY_TITLE))
      .oPgFrm.Page1.oPag.oObj_1_53.Calculate(cp_translate(MSG_MENU)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_54.Calculate(cp_translate(MSG_TITLE)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_55.Calculate(cp_translate(MSG_PROCEDURE_NAME))
      .oPgFrm.Page1.oPag.oObj_1_56.Calculate(cp_translate(MSG_PROC)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_57.Calculate(cp_translate(MSG_PATH_LINK))
      .oPgFrm.Page1.oPag.oObj_1_58.Calculate(cp_translate(MSG_PATH)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_59.Calculate(cp_translate(MSG_REL_POSITION))
      .oPgFrm.Page1.oPag.oObj_1_60.Calculate(cp_translate(MSG_POSITION)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_61.Calculate(cp_translate(MSG_DELETED))
      .oPgFrm.Page1.oPag.oObj_1_62.Calculate(cp_translate(MSG_BITMAP_MENU))
      .oPgFrm.Page1.oPag.oObj_1_63.Calculate(cp_translate(MSG_BITMAP)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_64.Calculate(cp_translate(MSG_COND_ACTIVATION))
      .oPgFrm.Page1.oPag.oObj_1_65.Calculate(cp_translate(MSG_ACTIVATION)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_66.Calculate(cp_translate(MSG_ENABLED_MENU))
      .oPgFrm.Page1.oPag.oObj_1_67.Calculate(cp_translate(MSG_VISIBILITY_MRU))
      .oPgFrm.Page1.oPag.oObj_1_68.Calculate(cp_translate(MSG_POPUP_UNBOUND))
      .oPgFrm.Page1.oPag.oObj_1_69.Calculate(cp_translate(MSG_VISIBILITY_MRU_TOOLTIPTEXT))
      .oPgFrm.Page1.oPag.oObj_1_70.Calculate(cp_translate(MSG_POPUP_UNBOUND_TOOLTIPTEXT))
      .oPgFrm.Page1.oPag.oObj_1_71.Calculate(cp_translate(MSG_MENU_NOTE_TOOLTIPTEXT))
    endwith
    this.DoRTCalc(10,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_MENUNAME<>.w_MENUNAME
            .w_COND_AGGANCIO = at('DEFAULT', .w_MENUNAME)<>0 Or at(upper( i_CpDic) , .w_MENUNAME)<>0
        endif
        .oPgFrm.Page1.oPag.USRTreeView.Calculate()
        .oPgFrm.Page1.oPag.SYSTreeView.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(cp_translate(MSG_USER_MENU)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(cp_translate(MSG_ENTRY_TITLE))
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate(cp_translate(MSG_MENU)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate(cp_translate(MSG_TITLE)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(cp_translate(MSG_PROCEDURE_NAME))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(cp_translate(MSG_PROC)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(cp_translate(MSG_PATH_LINK))
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(cp_translate(MSG_PATH)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(cp_translate(MSG_REL_POSITION))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(cp_translate(MSG_POSITION)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(cp_translate(MSG_DELETED))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate(cp_translate(MSG_BITMAP_MENU))
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(cp_translate(MSG_BITMAP)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate(cp_translate(MSG_COND_ACTIVATION))
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate(cp_translate(MSG_ACTIVATION)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate(cp_translate(MSG_ENABLED_MENU))
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate(cp_translate(MSG_VISIBILITY_MRU))
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate(cp_translate(MSG_POPUP_UNBOUND))
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate(cp_translate(MSG_VISIBILITY_MRU_TOOLTIPTEXT))
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate(cp_translate(MSG_POPUP_UNBOUND_TOOLTIPTEXT))
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate(cp_translate(MSG_MENU_NOTE_TOOLTIPTEXT))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.USRTreeView.Calculate()
        .oPgFrm.Page1.oPag.SYSTreeView.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(cp_translate(MSG_USER_MENU)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(cp_translate(MSG_ENTRY_TITLE))
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate(cp_translate(MSG_MENU)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate(cp_translate(MSG_TITLE)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(cp_translate(MSG_PROCEDURE_NAME))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(cp_translate(MSG_PROC)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(cp_translate(MSG_PATH_LINK))
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(cp_translate(MSG_PATH)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(cp_translate(MSG_REL_POSITION))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(cp_translate(MSG_POSITION)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(cp_translate(MSG_DELETED))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate(cp_translate(MSG_BITMAP_MENU))
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(cp_translate(MSG_BITMAP)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate(cp_translate(MSG_COND_ACTIVATION))
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate(cp_translate(MSG_ACTIVATION)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate(cp_translate(MSG_ENABLED_MENU))
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate(cp_translate(MSG_VISIBILITY_MRU))
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate(cp_translate(MSG_POPUP_UNBOUND))
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate(cp_translate(MSG_VISIBILITY_MRU_TOOLTIPTEXT))
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate(cp_translate(MSG_POPUP_UNBOUND_TOOLTIPTEXT))
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate(cp_translate(MSG_MENU_NOTE_TOOLTIPTEXT))
    endwith
  return

  proc Calculate_JWIZBYPABW()
    with this
          * --- Legge le informazioni del nodo selezionato
          .w_INDEX = .w_USRTREEVIEW.oTree.selectedItem.index
          cp_SuppMenu(this;
              ,'USRSEL';
             )
    endwith
  endproc
  proc Calculate_KUVMTVSARB()
    with this
          * --- Carica Men� (avvio e da bottone in alto a destra)
          Cp_SuppMenu(this;
              ,'INITMENU';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVOCEMENU_1_8.enabled = this.oPgFrm.Page1.oPag.oVOCEMENU_1_8.mCond()
    this.oPgFrm.Page1.oPag.oNAMEPROC_1_9.enabled = this.oPgFrm.Page1.oPag.oNAMEPROC_1_9.mCond()
    this.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.enabled = this.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPOSITION_1_12.enabled = this.oPgFrm.Page1.oPag.oPOSITION_1_12.mCond()
    this.oPgFrm.Page1.oPag.oDELETED_1_13.enabled = this.oPgFrm.Page1.oPag.oDELETED_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBITMAP_1_14.enabled = this.oPgFrm.Page1.oPag.oBITMAP_1_14.mCond()
    this.oPgFrm.Page1.oPag.oMODULO_1_15.enabled = this.oPgFrm.Page1.oPag.oMODULO_1_15.mCond()
    this.oPgFrm.Page1.oPag.oABILITATO_1_16.enabled = this.oPgFrm.Page1.oPag.oABILITATO_1_16.mCond()
    this.oPgFrm.Page1.oPag.oMRU_1_17.enabled = this.oPgFrm.Page1.oPag.oMRU_1_17.mCond()
    this.oPgFrm.Page1.oPag.oTEAROFF_1_18.enabled = this.oPgFrm.Page1.oPag.oTEAROFF_1_18.mCond()
    this.oPgFrm.Page1.oPag.oNOTE_1_19.enabled = this.oPgFrm.Page1.oPag.oNOTE_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.visible=!this.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPOSITION_1_12.visible=!this.oPgFrm.Page1.oPag.oPOSITION_1_12.mHide()
    this.oPgFrm.Page1.oPag.oDELETED_1_13.visible=!this.oPgFrm.Page1.oPag.oDELETED_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBITMAP_1_14.visible=!this.oPgFrm.Page1.oPag.oBITMAP_1_14.mHide()
    this.oPgFrm.Page1.oPag.oMODULO_1_15.visible=!this.oPgFrm.Page1.oPag.oMODULO_1_15.mHide()
    this.oPgFrm.Page1.oPag.oABILITATO_1_16.visible=!this.oPgFrm.Page1.oPag.oABILITATO_1_16.mHide()
    this.oPgFrm.Page1.oPag.oMRU_1_17.visible=!this.oPgFrm.Page1.oPag.oMRU_1_17.mHide()
    this.oPgFrm.Page1.oPag.oTEAROFF_1_18.visible=!this.oPgFrm.Page1.oPag.oTEAROFF_1_18.mHide()
    this.oPgFrm.Page1.oPag.oNOTE_1_19.visible=!this.oPgFrm.Page1.oPag.oNOTE_1_19.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_21.visible=!this.oPgFrm.Page1.oPag.oBtn_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_23.visible=!this.oPgFrm.Page1.oPag.oBtn_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_47.visible=!this.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.USRTreeView.Event(cEvent)
      .oPgFrm.Page1.oPag.SYSTreeView.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
        if lower(cEvent)==lower("w_usrtreeview NodeSelected")
          .Calculate_JWIZBYPABW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Avvio") or lower(cEvent)==lower("Ricarica")
          .Calculate_KUVMTVSARB()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_69.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_70.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_71.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVOCEMENU_1_8.value==this.w_VOCEMENU)
      this.oPgFrm.Page1.oPag.oVOCEMENU_1_8.value=this.w_VOCEMENU
    endif
    if not(this.oPgFrm.Page1.oPag.oNAMEPROC_1_9.value==this.w_NAMEPROC)
      this.oPgFrm.Page1.oPag.oNAMEPROC_1_9.value=this.w_NAMEPROC
    endif
    if not(this.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.value==this.w_INSERTPOINT)
      this.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.value=this.w_INSERTPOINT
    endif
    if not(this.oPgFrm.Page1.oPag.oPOSITION_1_12.RadioValue()==this.w_POSITION)
      this.oPgFrm.Page1.oPag.oPOSITION_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDELETED_1_13.RadioValue()==this.w_DELETED)
      this.oPgFrm.Page1.oPag.oDELETED_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBITMAP_1_14.value==this.w_BITMAP)
      this.oPgFrm.Page1.oPag.oBITMAP_1_14.value=this.w_BITMAP
    endif
    if not(this.oPgFrm.Page1.oPag.oMODULO_1_15.value==this.w_MODULO)
      this.oPgFrm.Page1.oPag.oMODULO_1_15.value=this.w_MODULO
    endif
    if not(this.oPgFrm.Page1.oPag.oABILITATO_1_16.RadioValue()==this.w_ABILITATO)
      this.oPgFrm.Page1.oPag.oABILITATO_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMRU_1_17.RadioValue()==this.w_MRU)
      this.oPgFrm.Page1.oPag.oMRU_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTEAROFF_1_18.RadioValue()==this.w_TEAROFF)
      this.oPgFrm.Page1.oPag.oTEAROFF_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_19.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_19.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oMENUNAME_1_29.value==this.w_MENUNAME)
      this.oPgFrm.Page1.oPag.oMENUNAME_1_29.value=this.w_MENUNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oMENUUTENTE_1_45.value==this.w_MENUUTENTE)
      this.oPgFrm.Page1.oPag.oMENUUTENTE_1_45.value=this.w_MENUUTENTE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MENUNAME = this.w_MENUNAME
    return

enddefine

* --- Define pages as container
define class tcp_designmenuPag1 as StdContainer
  Width  = 583
  height = 589
  stdWidth  = 583
  stdheight = 589
  resizeXpos=179
  resizeYpos=256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object USRTreeView as cp_TreeviewDis_Menu with uid="QFWBFQPPXP",left=13, top=30, width=269,height=304,;
    caption='TreeView',;
   bGlobalFont=.t.,;
    cLeafBmp=g_MLEAFBMP,nIndent=20,cCursor="",cShowFields="VOCEMENU",cNodeShowField="",cLeafShowField="",cNodeBmp=g_MNODEBMP,cLvlSep=".",;
    cEvent = "UsrMenu",;
    nPag=1;
    , HelpContextID = 48488278


  add object oBtn_1_3 as StdButton with uid="NVOBHVNQQU",left=13, top=338, width=50,height=20,;
    caption="Menu", nPag=1;
    , ToolTipText = ""+MSG_ADD_MENU+"";
    , HelpContextID = 200803745;
    , Caption=MSG_MENU_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"ADD",cp_Translate(MSG_NEW_MENU),1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not Empty (.w_INDEX))
      endwith
    endif
  endfunc


  add object oBtn_1_4 as StdButton with uid="XDQIKLVXWR",left=65, top=338, width=64,height=20,;
    caption="Opzione", nPag=1;
    , ToolTipText = ""+MSG_ADD_OPT+"";
    , HelpContextID = 108284075;
    , Caption=MSG_OPTION_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"ADD",cp_Translate(MSG_OPTION),2)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_INDEX))
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="DJGIJGOPLJ",left=130, top=338, width=48,height=20,;
    caption="Sep", nPag=1;
    , ToolTipText = ""+MSG_ADD_SEP+"";
    , HelpContextID = 100142319;
    , Caption=MSG_SEP_MENU;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"ADD","------------",2)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not Empty (.w_INDEX))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="TUHAANZWGM",left=181, top=338, width=48,height=20,;
    caption="Seq +", nPag=1;
    , ToolTipText = ""+MSG_MOVEDOWN_MENU+"";
    , HelpContextID = 100130798;
    , Caption=MSG_SEQ_MENU+' \<+';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        .w_USRTreeView.Move_Nodo(  .w_USRTreeView.oTree.Nodes( .w_INDEX ) , 1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_INDEX>1 and empty(.w_INSERTPOINT))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="PEQGUJPZDH",left=230, top=338, width=48,height=20,;
    caption="Seq -", nPag=1;
    , ToolTipText = ""+MSG_MOVEUP_MENU+"";
    , HelpContextID = 100130286;
    , Caption=MSG_SEQ_MENU+' \<-';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        .w_USRTreeView.Move_Nodo(  .w_USRTreeView.oTree.Nodes( .w_INDEX ) , -1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_INDEX>1 and empty(.w_INSERTPOINT))
      endwith
    endif
  endfunc

  add object oVOCEMENU_1_8 as StdField with uid="FHONHFRZWB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_VOCEMENU", cQueryName = "VOCEMENU",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Titolo voce",;
    HelpContextID = 44733492,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=79, Top=367, InputMask=replicate('X',60)

  func oVOCEMENU_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INDEX>1 and .w_DIRECTORY<>4 and .w_VOCEMENU<>'------------')
    endwith
   endif
  endfunc

  add object oNAMEPROC_1_9 as StdField with uid="LSRVZPLTRN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NAMEPROC", cQueryName = "NAMEPROC",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Nome procedura",;
    HelpContextID = 160196414,;
   bGlobalFont=.t.,;
    Height=21, Width=213, Left=361, Top=367, InputMask=replicate('X',250)

  func oNAMEPROC_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INDEX>1 AND .w_DIRECTORY=1 and .w_VOCEMENU<>'------------')
    endwith
   endif
  endfunc

  add object oINSERTPOINT_1_10 as StdField with uid="SMHZUAJWBD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_INSERTPOINT", cQueryName = "INSERTPOINT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso dal quale svolgere l'operazione di inserimento o di cancellazione",;
    HelpContextID = 239962520,;
   bGlobalFont=.t.,;
    Height=21, Width=429, Left=79, Top=395, InputMask=replicate('X',254)

  func oINSERTPOINT_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.editbtn())
    endwith
   endif
  endfunc

  func oINSERTPOINT_1_10.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc


  add object oBtn_1_11 as StdButton with uid="BOQGKTOANU",left=515, top=396, width=59,height=20,;
    caption="Percorso", nPag=1;
    , ToolTipText = ""+MSG_SET_PATH_ITEM+"";
    , HelpContextID = 26048445;
    , Caption=MSG_PATH_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"INS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.editbtn())
      endwith
    endif
  endfunc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
     endwith
    endif
  endfunc


  add object oPOSITION_1_12 as StdCombo with uid="GUZUTLVZXT",rtseq=5,rtrep=.f.,left=79,top=423,width=98,height=21;
    , ToolTipText = "Posizione relativa del menu che si vuole inserire";
    , HelpContextID = 205249412;
    , cFormVar="w_POSITION",RowSource=""+""+MSG_BEFORE+","+""+MSG_AFTER+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPOSITION_1_12.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    space(6))))
  endfunc
  func oPOSITION_1_12.GetRadio()
    this.Parent.oContained.w_POSITION = this.RadioValue()
    return .t.
  endfunc

  func oPOSITION_1_12.SetRadio()
    this.Parent.oContained.w_POSITION=trim(this.Parent.oContained.w_POSITION)
    this.value = ;
      iif(this.Parent.oContained.w_POSITION=='P',1,;
      iif(this.Parent.oContained.w_POSITION=='D',2,;
      0))
  endfunc

  func oPOSITION_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.editbtn() and not empty(.w_INSERTPOINT) And not .w_DELETED)
    endwith
   endif
  endfunc

  func oPOSITION_1_12.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc

  add object oDELETED_1_13 as StdCheck with uid="LVRVBVWTCW",rtseq=6,rtrep=.f.,left=198, top=419, caption="Cancellato",;
    ToolTipText = ""+MSG_DELETED_ITEM_MENU+"",;
    HelpContextID = 189831997,;
    cFormVar="w_DELETED", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDELETED_1_13.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oDELETED_1_13.GetRadio()
    this.Parent.oContained.w_DELETED = this.RadioValue()
    return .t.
  endfunc

  func oDELETED_1_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DELETED==.T.,1,;
      0)
  endfunc

  func oDELETED_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIRECTORY<>4 and .w_VOCEMENU<>'------------' and not empty(.w_INSERTPOINT) and .editbtn())
    endwith
   endif
  endfunc

  func oDELETED_1_13.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc

  add object oBITMAP_1_14 as StdField with uid="JXDYJOOYSR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_BITMAP", cQueryName = "BITMAP",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Bitmap associata al menu",;
    HelpContextID = 116574011,;
   bGlobalFont=.t.,;
    Height=21, Width=193, Left=353, Top=423, InputMask=replicate('X',254)

  func oBITMAP_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INDEX>1 AND .w_DIRECTORY<>4 and .w_VOCEMENU<>'------------')
    endwith
   endif
  endfunc

  func oBITMAP_1_14.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc

  add object oMODULO_1_15 as StdField with uid="XOHAMZSSRD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MODULO", cQueryName = "MODULO",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Condizione per l'attivazione",;
    HelpContextID = 67974197,;
   bGlobalFont=.t.,;
    Height=21, Width=399, Left=79, Top=448, InputMask=replicate('X',254)

  func oMODULO_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INDEX>1 AND .w_DIRECTORY<>4 and .w_VOCEMENU<>'------------')
    endwith
   endif
  endfunc

  func oMODULO_1_15.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc

  add object oABILITATO_1_16 as StdCheck with uid="ECOLHGTDUG",rtseq=9,rtrep=.f.,left=487, top=448, caption="Abilitato",;
    HelpContextID = 57769814,;
    cFormVar="w_ABILITATO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oABILITATO_1_16.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oABILITATO_1_16.GetRadio()
    this.Parent.oContained.w_ABILITATO = this.RadioValue()
    return .t.
  endfunc

  func oABILITATO_1_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ABILITATO==.T.,1,;
      0)
  endfunc

  func oABILITATO_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INDEX>1 AND .w_DIRECTORY<>4 and .w_VOCEMENU<>'------------')
    endwith
   endif
  endfunc

  func oABILITATO_1_16.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc

  add object oMRU_1_17 as StdCheck with uid="HVDVAAIMFN",rtseq=10,rtrep=.f.,left=79, top=475, caption="Visibilit� MRU",;
    ToolTipText = "Specifica se la voce di menu � raramente usata e non � visibile perch� � attiva l'espansione del menu",;
    HelpContextID = 67629814,;
    cFormVar="w_MRU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMRU_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMRU_1_17.GetRadio()
    this.Parent.oContained.w_MRU = this.RadioValue()
    return .t.
  endfunc

  func oMRU_1_17.SetRadio()
    this.Parent.oContained.w_MRU=trim(this.Parent.oContained.w_MRU)
    this.value = ;
      iif(this.Parent.oContained.w_MRU=='S',1,;
      0)
  endfunc

  func oMRU_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INDEX>1)
    endwith
   endif
  endfunc

  func oMRU_1_17.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oTEAROFF_1_18 as StdCheck with uid="QWMADVRTNB",rtseq=11,rtrep=.f.,left=355, top=475, caption="Popup sganciabile",;
    ToolTipText = "Permette al sub-popup di essere sganciato e di essere usato separatamente come una toolbar",;
    HelpContextID = 78469374,;
    cFormVar="w_TEAROFF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTEAROFF_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTEAROFF_1_18.GetRadio()
    this.Parent.oContained.w_TEAROFF = this.RadioValue()
    return .t.
  endfunc

  func oTEAROFF_1_18.SetRadio()
    this.Parent.oContained.w_TEAROFF=trim(this.Parent.oContained.w_TEAROFF)
    this.value = ;
      iif(this.Parent.oContained.w_TEAROFF=='S',1,;
      0)
  endfunc

  func oTEAROFF_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INDEX>1 And .w_DIRECTORY=2)
    endwith
   endif
  endfunc

  func oTEAROFF_1_18.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oNOTE_1_19 as StdField with uid="KABTHNYAYN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Specifica il testo che viene mostrato nella status bar quando il puntatore del mouse si muove sopra una voce di menu.",;
    HelpContextID = 84408133,;
   bGlobalFont=.t.,;
    Height=79, Width=571, Left=5, Top=501, InputMask=replicate('X',254)

  func oNOTE_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INDEX>1)
    endwith
   endif
  endfunc

  func oNOTE_1_19.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc


  add object oBtn_1_20 as StdButton with uid="LHYPBMLFXP",left=554, top=425, width=20,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 184028465;
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      with this.Parent.oContained
        .loadBit()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_INDEX>1 AND .w_DIRECTORY<>4 and .w_VOCEMENU<>'------------')
      endwith
    endif
  endfunc

  func oBtn_1_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
     endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="UESYSCUXNU",left=298, top=338, width=52,height=20,;
    caption="Menu ^", nPag=1;
    , ToolTipText = ""+MSG_ADD_MENU_BEFORE_ITEM+"";
    , HelpContextID = 200410529;
    , Caption=MSG_ADD_MENU_BEFORE;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"ADD",cp_Translate(MSG_NEW_MENU),1,.t.,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_COND_AGGANCIO)
      endwith
    endif
  endfunc

  func oBtn_1_21.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
     endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="HVJUPXMDUP",left=352, top=338, width=54,height=20,;
    caption="Menu v", nPag=1;
    , ToolTipText = ""+MSG_ADD_MENU_AFTER_ITEM+"";
    , HelpContextID = 200312225;
    , Caption=MSG_ADD_MENU_AFTER;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"ADD",cp_Translate(MSG_NEW_MENU),1,.t.,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_COND_AGGANCIO)
      endwith
    endif
  endfunc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="MQONVJWEXR",left=408, top=338, width=53,height=20,;
    caption="Opt ^", nPag=1;
    , ToolTipText = ""+MSG_ADD_OPT_BEFORE_ITEM+"";
    , HelpContextID = 101208853;
    , Caption=MSG_ADD_OPT_BEFORE;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"ADD",cp_Translate(MSG_OPTION),2,.t.,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_COND_AGGANCIO)
      endwith
    endif
  endfunc

  func oBtn_1_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
     endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="TUNBGSYVIU",left=463, top=338, width=53,height=20,;
    caption="Opt v", nPag=1;
    , ToolTipText = ""+MSG_ADD_OPT_AFTER_ITEM+"";
    , HelpContextID = 101214997;
    , Caption=MSG_ADD_OPT_AFTER;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"ADD",cp_Translate(MSG_OPTION),2,.t.,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_COND_AGGANCIO)
      endwith
    endif
  endfunc

  func oBtn_1_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
     endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="ZDZUFSYWAE",left=518, top=338, width=54,height=20,;
    caption="Elimina", nPag=1;
    , ToolTipText = ""+MSG_ERASE_ITEM+"";
    , HelpContextID = 59751718;
    , Caption=MSG_ADD_ERASE_MENU;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"ADD",cp_Translate(MSG_OPTION),2,.t.,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_COND_AGGANCIO)
      endwith
    endif
  endfunc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
     endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="ACOIJVSEMS",left=486, top=6, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = ""+MSG_OPEN+"";
    , HelpContextID = 184028465;
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      with this.Parent.oContained
        .loadMn()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object SYSTreeView as cp_TreeviewDis_Menu with uid="PNTOTRGEIT",left=299, top=30, width=273,height=304,;
    caption='TreeView',;
   bGlobalFont=.t.,;
    cLeafBmp=g_MLEAFBMP,nIndent=20,cCursor="",cShowFields="VOCEMENU",cNodeShowField="",cLeafShowField="",cNodeBmp=g_MNODEBMP,cLvlSep='.',;
    cEvent = "SysMenu",;
    nPag=1;
    , HelpContextID = 48488278

  add object oMENUNAME_1_29 as StdField with uid="ONKECAAQKI",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MENUNAME", cQueryName = "MENUNAME",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    HelpContextID = 123120065,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=365, Top=5, InputMask=replicate('X',25), readonly = .t.,tabstop = .f.


  add object oObj_1_39 as cp_runprogram with uid="FHKPWORUAA",left=268, top=643, width=333,height=22,;
    caption='cp_SuppMenu(USRDEL)',;
   bGlobalFont=.t.,;
    prg="cp_SuppMenu('USRDEL')",;
    cEvent = "w_usrtreeview NodeClick",;
    nPag=1;
    , HelpContextID = 35195570


  add object oObj_1_40 as cp_runprogram with uid="BHBJAIUXSK",left=268, top=595, width=333,height=22,;
    caption='cp_SuppMenu(LOST)',;
   bGlobalFont=.t.,;
    prg="cp_SuppMenu('LOST')",;
    cEvent = "w_VOCEMENU LostFocus,w_INSERTPOINT LostFocus,w_POSITION LostFocus,w_NAMEPROC LostFocus,w_MODULO LostFocus, w_BITMAP LostFocus,w_ABILITATO LostFocus,w_DELETED LostFocus,w_NOTE LostFocus,w_MRU LostFocus,w_TEAROFF LostFocus",;
    nPag=1;
    , HelpContextID = 219698139


  add object oObj_1_41 as cp_runprogram with uid="HEZRGXKBCY",left=268, top=619, width=333,height=22,;
    caption='cp_SuppMenu(SYSCLICK)',;
   bGlobalFont=.t.,;
    prg="cp_SuppMenu('SYSCLICK')",;
    cEvent = "w_systreeview NodeClick",;
    nPag=1;
    , HelpContextID = 81297688

  add object oMENUUTENTE_1_45 as StdField with uid="HUCJXUTUDP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MENUUTENTE", cQueryName = "MENUUTENTE",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    HelpContextID = 221416767,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=96, Top=5, InputMask=replicate('X',25), readonly = .t.,tabstop = .f.


  add object oBtn_1_47 as StdButton with uid="TJEHKLWKFJ",left=222, top=6, width=59,height=20,;
    caption="Prova", nPag=1;
    , ToolTipText = ""+MSG_SET_TRY_ITEM+"";
    , HelpContextID = 117988208;
    , Caption=MSG_TRY,tabstop = .f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      with this.Parent.oContained
        cp_SuppMenu(this.Parent.oContained,"TRY")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_USRTreeView.OTREE.Nodes.Count > 1)
      endwith
    endif
  endfunc

  func oBtn_1_47.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
     endwith
    endif
  endfunc


  add object oObj_1_50 as cp_runprogram with uid="VDEDXIYXYC",left=268, top=667, width=333,height=22,;
    caption='cp_SuppMenu(TRY)',;
   bGlobalFont=.t.,;
    prg="cp_SuppMenu('TRY')",;
    cEvent = "Ripristina",;
    nPag=1;
    , ToolTipText = "Esegue ripristina in uscita";
    , HelpContextID = 146938843


  add object oObj_1_51 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=587, top=149, width=38,height=38,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Menu utente:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_52 as cp_setobjprop with uid="SKPUZOAYUU",left=666, top=201, width=71,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_VOCEMENU",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_53 as cp_setCtrlObjprop with uid="STMQUKDDXO",left=587, top=175, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Men�:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_54 as cp_setCtrlObjprop with uid="ILZANBVMJA",left=587, top=201, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Titolo:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_55 as cp_setobjprop with uid="FHEBSEOCGP",left=666, top=227, width=71,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_NAMEPROC",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_56 as cp_setCtrlObjprop with uid="XYLPWDHILE",left=587, top=227, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Procedura:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_57 as cp_setobjprop with uid="LDYNYTFXXY",left=666, top=252, width=71,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_INSERTPOINT",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_58 as cp_setCtrlObjprop with uid="CNXPIZVNEX",left=587, top=253, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Path:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_59 as cp_setobjprop with uid="KWYZDTTRZC",left=666, top=278, width=71,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_POSITION",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_60 as cp_setCtrlObjprop with uid="SHTOZWNFYJ",left=587, top=279, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Posizione:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_61 as cp_setCtrlObjprop with uid="CNLSINMLZA",left=587, top=305, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Cancellato",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_62 as cp_setobjprop with uid="JEDGXBHRZN",left=666, top=329, width=71,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_BITMAP",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_63 as cp_setCtrlObjprop with uid="DPZKIOSYYB",left=587, top=331, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Bitmap:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_64 as cp_setobjprop with uid="VLOELJFJJH",left=666, top=355, width=71,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_MODULO",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_65 as cp_setCtrlObjprop with uid="KAYDNLAKGB",left=587, top=357, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Attivazione:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_66 as cp_setCtrlObjprop with uid="DJWDWAMSIC",left=587, top=383, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Abilitato",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_67 as cp_setCtrlObjprop with uid="HCUNAFCVWK",left=587, top=409, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Visibilit� MRU",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_68 as cp_setCtrlObjprop with uid="YETOGWCJLS",left=587, top=435, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Popup sganciabile",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_69 as cp_setobjprop with uid="HDTXYZDDKQ",left=666, top=407, width=71,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_MRU",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_70 as cp_setobjprop with uid="QVMERADSUL",left=666, top=433, width=71,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_TEAROFF",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363


  add object oObj_1_71 as cp_setobjprop with uid="RRYDKQFRMB",left=666, top=459, width=71,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_NOTE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101686363

  add object oStr_1_32 as StdString with uid="WAJVPFIJKM",Visible=.t., Left=293, Top=6,;
    Alignment=1, Width=70, Height=18,;
    Caption="Men�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="PVIHHYBQQP",Visible=.t., Left=1, Top=395,;
    Alignment=1, Width=72, Height=18,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="XFASJSVNSA",Visible=.t., Left=2, Top=423,;
    Alignment=1, Width=71, Height=18,;
    Caption="Posizione:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="GGHGAZBYUT",Visible=.t., Left=3, Top=367,;
    Alignment=1, Width=70, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="PMOKJZSZVT",Visible=.t., Left=284, Top=367,;
    Alignment=1, Width=75, Height=18,;
    Caption="Procedura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="JQIHGCEWXO",Visible=.t., Left=1, Top=448,;
    Alignment=1, Width=72, Height=18,;
    Caption="Attivazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="HSCAQNDISJ",Visible=.t., Left=297, Top=423,;
    Alignment=1, Width=54, Height=18,;
    Caption="Bitmap:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (TYPE("i_bFox26")='L' AND i_bfox26)
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="LYGHWPDYAV",Visible=.t., Left=6, Top=6,;
    Alignment=1, Width=87, Height=18,;
    Caption="Menu utente:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_designmenu','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_designmenu
*** Definizione sottoclasse della cp_treeview per la gestione del menu ***

**Generali: la propriet� tag di ogni nodo � utilizzata per memorizzare tutte le informazioni della voce
**di menu, per quel nodo.
**Le informazioni sono concatenate e separate dal simbolo �...
**L'ordine di concatenazione � (per i nomi vedere cursore): 
**NAMEPROC+'�'+ALLTRIM(STR(DIRECTORY))+'�'+IIF(ENABLED,'T','F')+'�'+BITMAP+'�'+MODULO+'�'+INS_POINT+'�'+POSITION+'�'+DELETED
define class cp_TreeviewDis_Menu as cp_TreeView


**Inizializzo un contatore a 1 per il calcolo del livello.
contatore=1
rifPadre=-1

*** Aggiunge un nodo figlio rispetto al nodo sul quale si � posizionati.
*** NON aggiorna il cursore contenente le voci della TreeView.
*** @Param cText: il nome testuale del nodo.
*** @Param relazione: indica la posizione d'inserimento del nodo:
***  1 se il nodo deve essere posizionato in fondo a tutti i nodi allo stesso livello della chiave i_key
***  2 se il nodo deve essere posizionato dopo i_key
***  4 se il nodo deve essere un figlio del nodo i_key
*** @Param tipo: indica se devo creare un submenu o una voce
*** se tipo=1 creo un submenu; se uguale a 2 creo una opzione
*** (serve solamente per determinare la bitmap corretta)
*** @Param i_key: la chiave del nodo al quale si vuole aggiungere un figlio
*** @return il numero della chiave inserita 
PROCEDURE addElement(cText,relazione,tipo,i_key)
 IF type('i_Key')=='L'
	keyNode=this.oTree.selectedItem.Key
  ELSE
  	keyNode=i_key
  endif	
    keyNew=this.generateKey(keyNode,relazione)	
   	this.oTree.Nodes.Add(keyNode,relazione,keyNew,cp_translate(cText),tipo) 
   	return(keyNew)	
ENDPROC

**Ritorna il valore del campo del nodo selezionato
**@param cField: il nome del campo che si vuole avere.
**@return: il valore di cField del nodo selezionato.
**Se non � possibile trovare il valore, ritorna una stringa vuota
PROCEDURE getVar(cField)
   	keyNode=this.oTree.selectedItem.Key
	cCursor=this.cCursor
	SELECT (cCursor)
	SEEK ALLTRIM(SUBSTR(keyNode,2))
	IF FOUND()
		return(&cCursor->&cField)
	ELSE
		return('')
	endif
ENDPROC  

**Elimina la voce selezionata dalla TreeView
**Elimina anche tutte le sottovoci legate alla voce selezionata o passata
PROCEDURE delElement(delIndex)
    IF type('delIndex')=='L'
    	this.oTree.nodes.remove(this.oTree.selectedItem.Index)
    else
    	this.oTree.nodes.remove(delIndex)    	
    endif	
ENDPROC

**Espande la voce di menu selezionata o la voce con indice passato
PROCEDURE expandElement(expIndice)
    IF TYPE('expIndice')=='L'
		this.oTree.selectedItem.Expanded=.T.
	ELSE
		this.oTree.nodes(expIndice).Expanded=.T.
	endif	
ENDPROC

proc oTree.DblClick()
  local cKey,cCursor,cOldErr
  private err
  err=.f.
    if not isnull(this.SelectedItem) and this.SelectedItem.Index > 0
      if this.SelectedItem.Children = 0
        cCursor=this.parent.cCursor
        cOldErr=on('ERROR')
        on error err=.t.
        cKey=this.SelectedItem.Tag
        on error &cOldErr
        if err
          cKey=this.Nodes.Item(1).Tag
        endif
        select (this.parent.cCursor)
        cOldErr=on('ERROR')
        * --- Nel Tag non ho il Recno()
        * --- ma informazioni strtturate
        * --- relative al nodo
        on error err=.t.
        go (cKey)
        on error &cOldErr
        cOldErr=on('ERROR')
        on error err=.t.
        i_Value=&cCursor->cpexec
        on error &cOldErr
        if not err
          *---Esiste il campo "cpExec": eseguo la procedura contenuta nel campo
          do &i_Value
          if err
            cp_errormsg(cp_MsgFormat(MSG_ERROR_WHILE_CALLING_PROCEDURE__,upper(i_Value)),"!","MSG_ERROR",.F.)
          endif
        endif
      endif
      if this.parent.nDblClick=2
        this.parent.parent.oContained.NotifyEvent("w_"+lower(this.parent.Name)+" NodeClick")
        this.parent.nDblClick=0
      endif
      this.parent.bRightMouse=.f.
    endif
  *endIf
  return

proc FillTree
  local oNodX,cFathKey,aLev,cLevel,bIsContained,i,nIdx,cCursor,cTmp
  local nBmpIndex,nPrevIdx,cNodeFields,cLeafFields,cPrevLeafText,bBmp,cOldErr
  private err
  cFathkey=''
  bIsContained=.f.
  dimension aLev[1]
  dimension cNodeFields[1]
  dimension cLeafFields[1]
  aLev[1]=''
  cNodeFields[1]=''
  cLeafFields[1]=''
  bBmp=.t.
  *---Compongo la lista dei campi da visualizzare nei nodi
  cTmp=this.cNodeShowField+iif(empty(this.cNodeShowField) or empty(this.cShowFields),'','+')+this.cShowFields+','
  i=0
  do while at(",",cTmp)<>0
    i=i+1
    dimension cNodeFields[i]
    cNodeFields[i]=left(cTmp,at(",",cTmp)-1)
    cTmp=substr(cTmp,at(",",cTmp)+1)
  enddo
  *---Compongo la lista dei campi da visualizzare nelle foglie
  cTmp=this.cLeafShowField+iif(empty(this.cLeafShowField) or empty(this.cShowFields),'','+')+this.cShowFields+','
  i=0
  do while at(",",cTmp)<>0
    i=i+1
    dimension cLeafFields[i]
    cLeafFields[i]=left(cTmp,at(",",cTmp)-1)
    cTmp=substr(cTmp,at(",",cTmp)+1)
  enddo
  cCursor=this.cCursor
  *--- Cancella tutti i nodi esistenti
  this.oTree.Nodes.Clear
  select (cCursor)
  set order to LvlKey
  nPrevIdx=1
  cPrevLeafText=''
  *--- Verifica se esiste il campo "CPBmpName" dei bitmap parametrici
  err=.f.
  cOldErr=on('ERROR')
  on error err=.t.
  cLevel=&cCursor..CPBmpName
  on error &cOldErr
  if err
    bBmp=.f.
  endif
  scan
    cLevel="k"+trim(&cCursor->lvlkey)
    nIdx=0
    for i=alen(aLev) to 1 step -1
       if this.clvlSep+aLev[i]+this.clvlsep$this.clvlsep+cLevel
        nIdx=i
        exit for
      endif
    next
    dimension aLev[nIdx+1]
    aLev[nIdx+1]=cLevel
    *---Conta il nodo per decidere quale campo visualizzare
    i=len(cLevel)-len(strtran(cLevel,this.clvlSep,''))+1
    if i>alen(cNodeFields)
      i=1
    endif
    cTmp=''
    if !bBmp or empty(&cCursor..CPBmpName)
      if nIdx<=nPrevIdx and this.oTree.Nodes.Count>0  &&devo aggiornare il precedente perch� non � un nodo con subitems
        if this.bHasElementBmp and this.ImgList.ListImages.Count>0
          this.oTree.Nodes(this.oTree.Nodes.Count).Image=this.ImgList.ListImages.Count
        endif
        this.oTree.Nodes(this.oTree.Nodes.Count).Text=cPrevLeafText
      endif
      if this.ImgList.ListImages.Count>0
        nBmpIndex=nIdx%this.ListImageCount()+1
        cTmp=","+alltrim(str(nBmpIndex))
      else
        nBmpIndex=1
      endif
    else
      * --- Bitmap paramentrici
      cTmp=","+alltrim(str(this.GetBitmapIndex(&cCursor..CPBmpName)))
    endif
    if nIdx=0
      oNodX=this.oTree.Nodes.Add(,,aLev[nIdx+1],iif(isnull(&cNodeFields[i]),'',cp_Translate(cp_ToStr(&cNodeFields[i],1)))&cTmp)
    else
      oNodX=this.oTree.Nodes.Add(aLev[nIdx],4,aLev[nIdx+1],iif(isnull(&cNodeFields[i]),'',cp_Translate(cp_ToStr(&cNodeFields[i],1)))&cTmp)
    ENDIF
    * --- Nel Tag vado a mettere
    * --- le informazione del nodo    
    oNodX.Tag=ALLTRIM(NAMEPROC)+'�'+ALLTRIM(STR(DIRECTORY))+'�'+IIF(ENABLED,'T','F')+'�'+ALLTRIM(CPBMPNAME)+'�'+alltrim(MODULO)+'�'+ALLTRIM(INS_POINT)+'�'+ALLTRIM(POSITION)+'�'+ALLTRIM(INSERTION)+'�'+ALLTRIM(DELETED) ;
              +'�'+ALLTRIM(NOTE)+'�'+ALLTRIM(MRU)+'�'+ALLTRIM(TEAROFF)
    nPrevIdx=nIdx
    cPrevLeafText=iif(isnull(&cLeafFields[i]),'',cp_Translate(cp_ToStr(&cLeafFields[i],1)))
    *onodx.ensurevisible
  endscan
  if this.oTree.Nodes.Count>0  &&devo aggiornare il precedente perch� non � un nodo con subitems
    if this.bHasElementBmp
      this.oTree.Nodes(this.oTree.Nodes.Count).Image=this.ImgList.ListImages.Count
    endif
    this.oTree.Nodes(this.oTree.Nodes.Count).Text=cPrevLeafText
  endif
  return

proc visita(nodo,cursore,padre)
	LOCAL w_VOCEMENU,w_NAMEPROC,w_DIRECTORY,w_ENABLED,w_BITMAP,w_MODULO,w_INSERTPOINT,w_POSITION,w_INSERTION,w_DELETED, w_NOTE, w_MRU, w_TEAROFF
    LOCAL k,done
    k=1
    *children=nodo.children
	this.getValues(nodo,@w_VOCEMENU,@w_NAMEPROC,@w_DIRECTORY,@w_ENABLED,@w_BITMAP,@w_MODULO,@w_INSERTPOINT,@w_POSITION,@w_INSERTION,@w_DELETED,@w_NOTE,@w_MRU,@w_TEAROFF)
	**inserisco in un cursore le voci del nodo analizzato
	INSERT INTO (cursore) (VOCEMENU,LEVEL,NAMEPROC,DIRECTORY,RIFPADRE,ELEMENTI,ID,ENABLED,CPBMPNAME,MODULO,INS_POINT,POSITION,INSERTION,DELETED,NOTE,MRU,TEAROFF);
	VALUES (cp_Translate(w_VOCEMENU),this.contatore,w_NAMEPROC,w_DIRECTORY,padre,nodo.children,SYS(2015),w_ENABLED,w_BITMAP,w_MODULO,w_INSERTPOINT,w_POSITION,w_INSERTION,w_DELETED,w_NOTE,w_MRU,w_TEAROFF)
	**se il nodo ha figli...
	this.rifPadre=this.rifPadre+1
	IF nodo.children<>0
	    padre=this.rifPadre
	    done=.F.
	    nodo=nodo.child
		FOR k=1 TO nodo.parent.children
	    	**calcolo il primo figlio
	    	IF NOT done
	    		this.contatore=this.contatore+1
	    		*this.rifPadre=this.rifPadre+1
	    		this.visita(nodo,cursore,Padre)
	    		done=.T.
	    	ELSE
	    		*this.rifPadre=this.rifPadre+1
	    		nodo=nodo.next
	    		this.visita(nodo,cursore,Padre)
	    	endif
	    endfor
	    this.contatore=this.contatore-1
    endif
    *WAIT WINDOW 'prima ricorsione '+ ALLTRIM(STR(nodo.parent.children))+ ALLTRIM(STR(k))
return

* --- Calcola il tag date le informazioni e lo associa al nodo..
Procedure SetTag(oNodo,cNameProc,cDirectory,bAbilitato,cBitmap,cModulo,cInsertPoint,cPosition,cInsertion,cDeleted,cNote,cMRU,cTearOff)
	oNodo.tag = ALLTRIM(cNameProc)+"�"+ALLTRIM(STR(cDirectory))+"�"+IIF(bAbilitato,"T","F")+"�"+ALLTRIM(cBitMap)+"�"+alltrim(cModulo)+;
		"�"+ALLTRIM(cInsertPoint)+"�"+ALLTRIM(cPosition)+"�"+ALLTRIM(cInsertion)+"�"+ALLTRIM(cDeleted)+"�"+ALLTRIM(cNote)+"�"+ALLTRIM(cMRU)+"�"+ALLTRIM(cTearOff)
EndProc

* --- Dato un oNodo interpreta il tag valorizzando le singole propriet�
FUNCTION getValues(oNodo,cVoceMenu,cNameProc,cDirectory,bAbilitato,cBitmap,cModulo,cInsertPoint,cPosition,pInsertion,cDeleted,cNote,cMRU,cTearOff)
Local cTag
    cTag = alltrim(oNodo.tag)
    cVoceMenu = cp_Translate(oNodo.text)
    cNameProc = alltrim(left(cTag,at("�",cTag)-1))
    cTag = strtran(cTag,cNameProc+"�","",1,1)
    cDirectory = int(val(alltrim(left(cTag,at("�",cTag)-1))))
    cTag = strtran(cTag,alltrim(str(cDirectory))+"�","",1,1)
    bAbilitato = alltrim(left(cTag,at("�",cTag)-1))="T"
    cTag = strtran(cTag,iif(bAbilitato,"T","F")+"�","",1,1)
    cBitmap = alltrim(left(cTag,at("�",cTag)-1))
    cTag = strtran(cTag,cBitmap+"�","",1,1)
    cModulo = alltrim(left(cTag,at("�",cTag)-1))
    cTag = strtran(cTag,cModulo+"�","",1,1)
    cInsertPoint = alltrim(left(cTag,at("�",cTag)-1))
    cTag = strtran(cTag,cInsertPoint+"�","",1,1)
    cPosition = alltrim(left(cTag,at("�",cTag)-1))
    cTag = strtran(cTag,cPosition+"�","",1,1)
    pInsertion=ALLTRIM(LEFT(cTag,AT("�",cTag)-1))
    cTag = strtran(cTag,pInsertion+"�","",1,1)
    cDeleted = alltrim(left(cTag,at("�",cTag)-1))
    cTag = strtran(cTag,cDeleted+"�","",1,1)
    cNote = alltrim(left(cTag,at("�",cTag)-1))
    cTag = strtran(cTag,cNote +"�","",1,1)
    cMRU = alltrim(left(cTag,at("�",cTag)-1))
    cTag = strtran(cTag,cMRU+"�","",1,1)
    cTearOff = cTag
endfunc

* --- Muove oNodo in direzione ascedente o discendente all'interno della
* --- tree view
Procedure Move_Nodo(oNodo , nDirection)
	Local oZio, oNode
	* --- Se sono sulla root non posso svolgere nessun spostamento
	If oNodo.Index<>oNodo.Root.Index
		if nDirection=1
			oNode=oNodo.Next
		else
			oNode=oNodo.Previous
		endif
		* --- Esiste un fratello successivo al nodo ?
		If Not IsNull(oNode)
			* --- E' espanso oppure non ha figli
			If oNode.Expanded And oNode.Children>0
				* --- Se espanso lo inserisco come primo figlio del nodo successivo
				oZio=oNode.Child
				this.SpostaNodo( oNodo , oZio ,iif(nDirection=1,3,1)  )
			Else
				* --- Se non espanso lo sposto dopo il nodo ad esso successivo
				This.SpostaNodo( oNodo , oNode , iif(nDirection=1,2,3)   )
			Endif
		Else
			* --- Non esiste un fratello successivo al nodo
			* --- Sposto il nodo dopo suo padre se non � figlio
			* --- della root
			oZio=oNodo.Parent
			* --- l'antenato non � la Root ?
			If oZio.Root.Index<>oZio.Index
				this.SpostaNodo( oNodo , oZio , iif(nDirection=1,2,3)   )
			Endif
		Endif
	Endif	
EndProc

* --- Dato un nodo (riferimento oggetto) lo sposta in base
* --- alla relazione (1,2,3,4) come
* --- fratello precedente (2) o successivo (3) o ultimo fratello (1) o primo figlio (4) di oRNodo (Riferimento oggetto)
Procedure SpostaNodo( oPNodo , oRNodo, nRelazione )
Local cCurName,cOldLevel,nIndexTemp,oPadreNodo,n_K, cText,cTsg,nBMpIndex,nIndex,bExp
cCurName=Sys(2015)
nIndexTemp=oPnodo.Index

* --- Se non ho figli devo spostare il singolo nodo
* --- altrimenti devo, tramite una visita, recuperare tutti i nodi suoi eredi
* --- e spostarli uno per uno
if oPNodo.children=0
      cText = oPNodo.text
      ctag = oPNodo.Tag
      bExp=oPNodo.Expanded
      * --- Recupero il nome del bitmap associato al nodo direttamente
      * --- dalle variabili della maschera (sul nodo selectedimage non � accessibile)
      This.oTree.Nodes.add( oRnodo.Index , nRelazione ,, cp_Translate(cText) , This.GetBitmapIndex(iif(empty(this.parent.oContained.w_BITMAP),iif( this.parent.oContained.w_DIRECTORY=1 Or this.parent.oContained.w_DIRECTORY=4 ,g_MLEAFBMP,g_MNODEBMP),this.parent.oContained.w_BITMAP) ))      
      this.oTree.nodes(this.oTree.nodes.count()).tag = ctag
      * --- L'indice va decrementato xcg� al termine elimino un nodo sempre
      nIndex = this.oTree.nodes.count()-1
else
    #if version(5)>=700
	 Select * From ( this.cCursor ) Where 1=0 Into Cursor &cCurName NoFilter READWRITE
    #else
      local w_nome
      w_NOME = sys(2015)
      Select * From ( this.cCursor ) Into Cursor (w_nome) Where 1=0 NOFILTER      
      use dbf() again in 0 alias ( cCurName )
      USE
      SELECT ( cCurName )
    #endif
      * --- Visito l'albero a partire dal nodo che devo spostare e salvo gli elementi da spostare in un &cCurName..
      *     E' importante, prima della visita, settare a 1 la propriet� contatore della TreeView per il calcolo corretto
      *     del LEVEL del menu.
      this.rifPadre=-1
      this.contatore = 1
      this.visita(oPNodo,cCurName,0)
      select (cCurName)
 go top 
 scan
      * ---- Sono nel primo elemento, questo sar� effettivamente legato, le altre righe
      * ---- saranno semplicemente figli di questo nodo...
      if recno(cCurName)=1
         bExp=oPNodo.Expanded      
	       This.oTree.Nodes.add( oRnodo.Index , nRelazione ,, cp_Translate(alltrim(&cCurName..VOCEMENU)) , This.GetBitmapIndex(iif(empty(&cCurName..CPBmpName),iif( &cCurName..DIRECTORY=1 Or &cCurName..DIRECTORY=4 ,g_MLEAFBMP,g_MNODEBMP),&cCurName..CPBmpName) ))
         * --- L'indice va decrementato xch� al termine elimino tanti nodi quanti nel cursore..
         * --- svolgendo la Remove..
         nIndex = this.oTree.nodes.count()-RecCount(cCurName)         
      else
        if &cCurName..LEVEL=cOldLevel
          * --- Se il livello del nodo che devo inserire � uguale al nodo precedente, inserisco il nodo subito dopo
          this.oTree.nodes.add(nIndexTemp,2,,cp_Translate(alltrim(&cCurName..VOCEMENU)),This.GetBitmapIndex(iif(empty(&cCurName..CPBmpName),iif( &cCurName..DIRECTORY=1 Or &cCurName..DIRECTORY=4 ,g_MLEAFBMP,g_MNODEBMP),&cCurName..CPBmpName) ))	  
        else
          if &cCurName..LEVEL>cOldLevel
            * --- Se il livello del nodo che devo inserire � maggiore del nodo precedente, inserisco il nodo all'interno dello stesso
            this.oTree.nodes.add(nIndexTemp,4,,alltrim(&cCurName..VOCEMENU),This.GetBitmapIndex(iif(empty(&cCurName..CPBmpName),iif( &cCurName..DIRECTORY=1 Or &cCurName..DIRECTORY=4 ,g_MLEAFBMP,g_MNODEBMP),&cCurName..CPBmpName) ))	  
          else
            * --- Calcolo l'index del nodo all'interno del quale devo inserire la voce
            oPadreNodo = this.oTree.nodes(nIndexTemp).parent
            n_K = cOldLevel-&cCurName..LEVEL
            do while n_k>0
              oPadreNodo = oPadreNodo.parent
              n_K = n_K - 1
            enddo
            nIndexTemp = oPadreNodo.Index
            this.oTree.nodes.add(nIndexTemp,4,,cp_Translate(alltrim(&cCurName..VOCEMENU)),This.GetBitmapIndex(iif(empty(&cCurName..CPBmpName),iif( &cCurName..DIRECTORY=1 Or &cCurName..DIRECTORY=4 ,g_MLEAFBMP,g_MNODEBMP),&cCurName..CPBmpName) ))	  
          endif
        endif
      endif
      * --- tengo aggiornato oldLevel e la chiave
      cOldLevel = &cCurName..LEVEL
      * --- Portiamo l'indice sul nodo appena creato
      nIndexTemp = this.oTree.nodes.count()
      * --- Salviamo il Tag della TreeView
      this.SetTag(this.oTree.nodes( nIndexTemp ) , &cCurName..NAMEPROC , &cCurName..DIRECTORY , &cCurName..ENABLED , &cCurName..CPBMPNAME , &cCurName..MODULO , &cCurName..INS_POINT , &cCurName..POSITION , "" , &cCurName..DELETED, &cCurName..NOTE, &cCurName..MRU, &cCurName..TEAROFF)
      endscan
      * --- Elimino il cursore d'appoggio
      Use in Select( cCurName )
endif      
    * --- Elimino il nodo dalla TreeView
    this.delElement(oPNodo.Index)
    * ---- valorizzo w_INDEX
    this.parent.oContained.w_INDEX= nIndex
    * --- Il nodo spostato lo espando se lo era
    this.otree.Nodes(nIndex).Expanded=bExp
EndProc

  proc oTree.DragDrop(oSource,nX,nY)
	  local cOldFName,bOk,cFName,cDefPath
	  cDefPath=SYS(5)+SYS(2003)+'\CUSTOM\'
	  cFName=''
	  if upper(oSource.name)='STDSGRP'
	    * drag iniziato nella lista dei gruppi del tool di gestione utenti
	    cFName='CUSTOM_G'+alltrim(str(_cpgroups_.code))+'.VMN'
	  else
	    if upper(oSource.name)='STDSUSR'
	      * drag iniziato nella lista degli utenti del tool di gestione utenti
	      cFName='CUSTOM_'+alltrim(str(_cpusers_.code))+'.VMN'
		endif
	  endif
	  if at(cFName,thisform.oParentObject.cFileName)<>0
	    cFName=thisform.oParentObject.cFileName
	  endif
	  if not empty(cFName)
	    cOldFName=thisform.oParentObject.cFileName
	    thisform.oParentObject.cFileName=cFName
	    bOk=.t.
	    if at("\",cFname)<>0
	      cDefPath=''
	    ELSE 
	      thisform.oParentObject.cFileName=cDefPath+cFName
	    endif
        *--- Zucchetti Aulla Inizio - Monitor Framework
        *--- Se il monitor � attivo verifico i diritti
        LOCAL l_UsrRight
        l_UsrRight = cp_set_get_right()
        DO CASE 
        	CASE l_UsrRight = 2   &&Read Only
        		thisform.oParentObject.cFileName=cOldFName
        		RETURN
        	CASE l_UsrRight = 3
        		IF upper(oSource.name)='STDSUSR'
	        		IF i_CodUte<>_cpusers_.code
		        		thisform.oParentObject.cFileName=cOldFName
		        		RETURN          		 
	        		ENDIF        		
        		ELSE 
	        		IF ASCAN(i_aUsrGrps, _cpgroups_.code) = 0
		        		thisform.oParentObject.cFileName=cOldFName
		        		RETURN          		 
	        		ENDIF
	        	ENDIF
        		cFName = FORCEPATH(upper(cFName), FULLPATH(i_usrpath))
        	CASE l_UsrRight = 4
        		cFName = FORCEPATH(upper(cFName), FULLPATH(i_usrpath))
        	OTHERWISE
        		cFName = IIF(not EMPTY(cDefPath), FORCEPATH(upper(cFName), cDefPath), upper(cFName))
        ENDCASE
        thisform.oParentObject.cFileName=cFName
        if cp_fileexist(cFName)
          bOk=cp_YesNo(cp_MsgFormat(MSG_FILE____ALREADY_EXIST_C_SUBSTITUTE_QP, cFName, ''),48,.f.,.f.,MSG_SAVE)
        else
          bOk=cp_YesNo(cp_MsgFormat(MSG_SAVE_FILE__QP, cFName),32,.f.,.f.,MSG_SAVE)
        ENDIF
        *--- Zucchetti Aulla Fine - Monitor Framework 
	    if bOk
	      thisform.oParentObject.SaveDoc()
	    else
	      thisform.oParentObject.bModified=.f.
	    endif   
	    thisform.oParentObject.cFileName=cOldFName
	  ENDIF
  return 



enddefine
* --- Fine Area Manuale
