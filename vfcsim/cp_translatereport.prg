* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_TRANSLATEREPORT
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Zucchetti Spa (SM)
* Data creazione: 06/01/2006
* Aggiornato il :
* Translated    : 06/01/2006
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Traduzione a runtime di un report foxpro
* ----------------------------------------------------------------------------
#include "cp_app_lang.inc"
Parameters p_ReportName,p_cOrigLang,p_cDestLang,p_cSessionCode
*
p_cSessionCode=Iif(Vartype(p_cSessionCode)<>'C' Or Empty(p_cSessionCode), Sys(2015), p_cSessionCode)
*
Local i_cExpr, i_cdirReport, i_cNewReport, i_cTmp1, i_cTmp2, o_cExpr, i_cLanguageReport, i_cOldError, i_lErr, i_nErr, i_cOldArea
i_cExpr=""
i_cNewReport=""
i_cdirReport=""
i_cOldArea=Alias()
i_cOldError=On('ERROR')
i_lErr=.F.
i_nErr=0
p_ReportName = Alltrim(p_ReportName)
* Check parametri
If Empty(p_cOrigLang) Or Empty(p_cDestLang) Or Empty(p_ReportName)
    * Errore, restituisce nome report vuoto !
    i_cNewReport=""
    i_lErr = .T.
Endif
* Controllo preliminare sulla lingua del report
If Not(i_lErr) And p_cOrigLang=p_cDestLang
    * Il report � gi� nella lingua desiderata, va bene cos� !
    i_cNewReport=p_ReportName
    i_lErr = .T.
Endif
* Esegue traduzione report
If Not(i_lErr)
    * Nuovo nome report
*--- Zucchetti Aulla Inizio
    *i_cdirReport = Alltrim(Addbs(cp_AdvancedDir(Addbs(Cp_getAppData())+Addbs(i_cAppDir),'REPORT','')))
    i_cdirReport = TEMPADHOC()+'\'
*--- Zucchetti Aulla Fine
    i_cNewReport = i_cdirReport+Substr(p_ReportName, Rat('\',p_ReportName)+1)+'_'+Alltrim(p_cDestLang)+'_'+Alltrim(p_cSessionCode)
    If Not(cp_fileexist(i_cNewReport+'.frx')) And (Cp_makedir (i_cdirReport))
        *
        * Esegue copia report originale
*--- Zucchetti Aulla Inizio
        *i_cTmp1 = Fullpath(Forceext(p_ReportName,"frx"))
        *i_cTmp1 = Justpath(i_cTmp1)+"\"+Juststem(i_cTmp1)+'.f*'
        *i_cTmp2 = i_cNewReport+'.*'
*--- Zucchetti Aulla Fine

        i_cTmp1 = p_ReportName+'.frx'
        i_cTmp2 = i_cNewReport+'.frx'
        * Set errori
        On Error i_lErr=.T.
        * copia il file originale
*--- Zucchetti Aulla Inizio
        *Copy File &i_cTmp1 To &i_cTmp2
        Copy File "&i_cTmp1" To "&i_cTmp2"
        *  Esegue copia frt del report originale
        i_cTmp1 = p_ReportName+'.frt'
        i_cTmp2 = i_cNewReport+'.frt'
        * Set errori
        On Error i_lErr=.T.
        * copia il file originale
        Copy File "&i_cTmp1" To "&i_cTmp2"
*--- Zucchetti Aulla Fien
        * Esegue traduzione
        If Not(i_lErr) And cp_fileexist(i_cNewReport+".frx")
            * Apre il report come un .DBF
            Use (i_cNewReport+".frx") In 0 Alias translate_report
            Select translate_report
            Go Top
            Scan For Not(i_lErr)
                Wait Window "Translating report ["+Str(Recno(),5,0)+"] ..." Nowait
                Do Case
                    Case objtype=5   						&& label ----------------------------------------------
                        * Label
                        i_cExpr=Alltrim(Expr)
                        If !Empty(Alltrim(i_cExpr))
                            * Elimina gli apici
                            i_cExpr=Alltrim(Substr(i_cExpr,2,Len(i_cExpr) - 2))
                            o_cExpr=i_cExpr
                            * Esegue traduzione
                            i_cExpr=cp_Translate(i_cExpr,p_cOrigLang,p_cDestLang)
                            * Sostituzione
                            If Not(i_cExpr==o_cExpr)
                                Do cp_UpdateReport With i_cExpr,'expr',objtype
                            Endif
                        Endif
                    Case objtype=9 And Objcode=1			&& user, memorizza langua su report generato ----------
                        Do cp_UpdateReport With p_cDestLang,'user',objtype
                    Case Inlist(objtype,8,18)   			&& expression and variables ---------------------------
                        * Expression
                        i_cExpr=Alltrim(Expr)
                        o_cExpr=i_cExpr
                        If !Empty(Alltrim(i_cExpr)) And (At(Chr(39),i_cExpr)>0 Or At(Chr(34),i_cExpr)>0)
                            i_cExpr=TranslateExpression(i_cExpr,p_cOrigLang,p_cDestLang)
                            * Sostituzione
                            If Len(i_cExpr)<=255
                                If Not(i_cExpr==o_cExpr)
                                    Do cp_UpdateReport With i_cExpr,'expr',objtype
                                Endif
                            Else
                                i_lErr=.T.
                                If Vartype(g_DEBUGTRANSLATEREPORT)='L' And g_DEBUGTRANSLATEREPORT
                                    i_nErr=1
                                Endif
                            Endif
                        Endif
                        * Tag (only variables)
                        i_cExpr=Alltrim(Tag)
                        o_cExpr=i_cExpr
                        If !Empty(Alltrim(i_cExpr)) And (At(Chr(39),i_cExpr)>0 Or At(Chr(34),i_cExpr)>0)
                            i_cExpr=TranslateExpression(i_cExpr,p_cOrigLang,p_cDestLang)
                            * Sostituzione
                            If Len(i_cExpr)<=255
                                If Not(i_cExpr==o_cExpr)
                                    Do cp_UpdateReport With i_cExpr,'tag',objtype
                                Endif
                            Else
                                i_lErr=.T.
                                If Vartype(g_DEBUGTRANSLATEREPORT)='L' And g_DEBUGTRANSLATEREPORT
                                    i_nErr=1
                                Endif
                            Endif
                        Endif
                Endcase
            Endscan
            * Chiusura report
            If Used('translate_report')
                Use In translate_report
            Endif
            * In caso di errore ...
            If i_lErr
                i_cNewReport=""
                Do Case
                    Case i_nErr=1
                        Do cp_ErrorMsg With cp_MsgFormat(MSG_REPORT_TRANSLATED_STRING_TOO_LONG,Alltrim(Str(Recno(),5,0)),Alltrim(i_cExpr),Alltrim(Str(Len(o_cExpr),5,0)),Alltrim(Str(Len(i_cExpr),5,0))),48,"",.F.
                    Otherwise
                        Do cp_ErrorMsg With cp_MsgFormat(MSG_CANNOT_TRANSLATE_REPORT,p_cDestLang),48,"",.F.
                Endcase
            Endif
        Else
            * Svuota nome report ...
            i_cNewReport=""
            Do cp_ErrorMsg With cp_MsgFormat(MSG_CANNOT_COPY_REPORT,Chr(10),i_cTmp1,i_cTmp2),,48,"",.F.
        Endif
    Endif
Endif
* Check Error
On Error &i_cOldError
* Riposiziona area
If Not(Empty(i_cOldArea))
    Select &i_cOldArea
Endif
Wait Clear
* Ritorna eventuale nome report tradotto
Return(i_cNewReport)


* ----------------------------------------------------------------------------------------------
* --- Funzione che determina la stringa di stile di una stringa (dal fontstyle di un report)
* ....................................................................................................................................
* 				Normale		Grassetto	Corsivo		Grassetto+Corsivo
* =========================================================================
* normale			0			1			2			3
* Sottolineato		4			5			6			7
* Barrato			128			129			130			131
* Sott+Barrato		132			133			134			135
* ----------------------------------------------------------------------------------------------
Function cp_StringStyle
    Parameters pStyle
    Local res, cStyle, cGras, cCors, cSott, cBarr
    * inizializza
    cStyle = pStyle
    cGras  = ""
    cCors  = ""
    cSott  = ""
    cBarr  = ""
    * Tratta sottolineato e barrato
    If cStyle>0
        Do Case
            Case cStyle>=132
                cSott='U'
                cBarr='-'
                cStyle=cStyle-132
            Case cStyle>=128
                cBarr='-'
                cStyle=cStyle-128
            Case cStyle>=4
                cSott='U'
                cStyle=cStyle-4
        Endcase
        * Tratta grassetto, corsivo
        Do Case
            Case cStyle=1
                cGras="B"
            Case cStyle=2
                cCors="I"
            Case cStyle=3
                cCors="I"
                cGras="B"
        Endcase
    Endif
    * Risultato
    res = cGras+cCors+cSott+cBarr
    Return (res)
    * ----------------------------------------------------------------------------------------------

    * ----------------------------------------------------------------------------------------------
    * --- Funzione sostituisce, nel report, un'espressione con un'altra
    * --- Usa il report gi� aperto (come DBF) e lo aggiorna
    * ----------------------------------------------------------------------------------------------
Function cp_UpdateReport(pString,pColumn,pObjtype)
    Local lres, cPicture, nWidth, nHPos, cAlign, nConstPix, nNewWidth, cStyle, cFont, nDim
    *
    lres     = .T.
    * Distingue il caso objtype=5 (relativo alle label, che vengono anche ridimensionate) dagli altri casi
    If pObjtype=5
        * Aggiorna stringa ----------------
        Replace &pColumn With '"'+pString+'"'
        * label (vengono anche ridimensionate) -------------
        nConstPix= 3125/30
        cPicture = Alltrim(Picture)
        nWidth   = Width
        nHPos    = HPos
        cStyle   = cp_StringStyle(Nvl(FontStyle,0))
        cFont    = Alltrim(FontFace)
        nDim     = FontSize
        * Considera l'allineamento al centro come sx, tanto viene ridimensionato allo stesso modo
        cAlign   = Iif("J"$cPicture, "dx", "sx")
        * Aggiorna dimensioni e posizionamento
        nNewWidth = Txtwidth(pString,cFont,nDim,cStyle)*Fontmetric(6,cFont,nDim,cStyle)
        nNewWidth = Round(nNewWidth*nConstPix,3)
        * Se allineato a destra aggiusta coordinata sinistra
        If cAlign = 'dx'
            Replace HPos With HPos-(nNewWidth-Width)
        Endif
        Replace Width With nNewWidth
        * Fine
    Else
        * tutti i casi escluse label
        Replace &pColumn With pString
    Endif
    * risultato
    Return (lres)

    * ----------------------------------------------------------------------------------------------
Function TranslateExpression(cString,cOrigLang,cDestLang)
    Local cRes,nIndArr,nIndex,cCurStr,cDelim
    Dimension cArrStr(1,2)
    nIndArr=0
    * Pulizia preliminare funzioni indesiderate
    cRes=cString
    cRes=ExtractFunctions(cRes,'looktab(',@cArrStr,@nIndArr)
    cRes=ExtractFunctions(cRes,'transform(',@cArrStr,@nIndArr)
    cRes=ExtractFunctions(cRes,'strtran(',@cArrStr,@nIndArr)
    cRes=ExtractFunctions(cRes,'nvl(',@cArrStr,@nIndArr)
    cRes=ExtractFunctions(cRes,'cp_getqueryparam(',@cArrStr,@nIndArr)
    cRes=ExtractFunctions(cRes,'tran(',@cArrStr,@nIndArr)
    * Parsing iif
    cRes=ExtractIIF(cRes,@cArrStr,@nIndArr)
    * Stringhe residue
    cRes=ExtractStrings(cRes,0,@cArrStr,@nIndArr)
    * Cicla sulle stringhe da tradurre
    For nIndex=nIndArr To 1 Step -1
        * Stringa in esame
        cCurStr=cArrStr(nIndex,1)
        * Traduce stringa
        If cArrStr(nIndex,2)
            cDelim=Left(cCurStr,1)
            cCurStr=Substr(cCurStr,2,Len(cCurStr) - 2)
            cCurStr=Chr(34)+Strtran(cp_Translate(cCurStr,cOrigLang,cDestLang),Chr(34),Chr(39))+Chr(34)
        Endif
        * Aggiorna stringa originale
        cRes=Strtran(cRes,'�'+Alltrim(Str(nIndex,5,0))+'�',cCurStr)
    Endfor
    Return(cRes)

    * ----------------------------------------------------------------------------------------------
Function ExtractIIF(cString,aTransArr,nIndexArr)
    Local cCurIIF,nBegIIF,nEndIIF,nBegStr,nEndStr,nVirg1,nVirg2,nMemIndex,cTmp,nTmp,nSubIndex
    *
    Do While Rat('IIF(',Upper(cString))>0
        * isola IIF pi� a destra
        nBegIIF=Rat('IIF(',Upper(cString))
        nEndIIF=nBegIIF+BalanceDelimiter(Substr(cString,nBegIIF),'(',')')
        cCurIIF=Substr(cString,nBegIIF,nEndIIF-nBegIIF)
        * --- Tratta il primo/secondo parametro ---------------------------------
        If Not(Empty(cCurIIF))
            * riverva posizione in array per IIF
            nMemIndex=nIndexArr
            * Posizione virgola che inizia la prima espressione IIF
            nVirg1=FindChr(cCurIIF,',')
            nVirg2=nVirg1+FindChr(Substr(cCurIIF,nVirg1+1),',')
            If nVirg1>0 And nVirg2>nVirg1
                cFirstExpr=Substr(cCurIIF,nVirg1+1,nVirg2-nVirg1-1)
                If At(Chr(34),cFirstExpr)>0 Or At(Chr(39),cFirstExpr)>0
                    * nTmp
                    cFirstExpr=ExtractStrings(cFirstExpr,0,@aTransArr,@nIndexArr)
                    * Sostiuisci prima espressione
                    cString=Stuff(cString,nBegIIF+nVirg1,nVirg2-nVirg1-1,'�'+Alltrim(Str(nIndexArr,5,0))+'�')
                Endif
            Endif
            * Secondo parametro
            nEndIIF=nBegIIF+BalanceDelimiter(Substr(cString,nBegIIF),'(',')')
            cCurIIF=Substr(cString,nBegIIF,nEndIIF-nBegIIF)
            nVirg1=FindChr(cCurIIF,',')
            nVirg2=nVirg1+FindChr(Substr(cCurIIF,nVirg1+1),',')
            If nVirg1>0 And nVirg2>nVirg1
                cSecondExpr=Substr(cCurIIF,nVirg2+1,Len(cCurIIF)-nVirg2-1)
                If At(Chr(34),cSecondExpr)>0 Or At(Chr(39),cSecondExpr)>0
                    * nTmp
                    cSecondExpr=ExtractStrings(cSecondExpr,0,@aTransArr,@nIndexArr)
                    * Sostiuisci seconda espressione
                    cString=Stuff(cString,nBegIIF+nVirg2,Len(cCurIIF)-nVirg2-1,'�'+Alltrim(Str(nIndexArr,5,0))+'�')
                Endif
            Endif
            * Isola e sostituisce intero IIF
            nEndIIF=nBegIIF+BalanceDelimiter(Substr(cString,nBegIIF),'(',')')
            cCurIIF=Substr(cString,nBegIIF,nEndIIF-nBegIIF)
            nIndexArr=nIndexArr+1
            Dimension aTransArr(nIndexArr,2)
            aTransArr(nIndexArr,1)=cCurIIF
            aTransArr(nIndexArr,2)=.F.
            * Aggiorna espressione
            cString=Stuff(cString,nBegIIF,nEndIIF-nBegIIF,'�'+Alltrim(Str(nIndexArr,5,0))+'�')
        Endif
    Enddo
    *
    Return(cString)

    * -----------------------------------------------------------------------------------------------------
Function ExtractStrings(cString,nOffset,aTransArr,nIndexArr)
    Local cCurChr,nIndex,nBegStr,nEndStr,cDelimStr,pArrIndex,nMemIndex
    cDelimStr=""
    nBegStr=0
    nEndStr=0
    nIndex=1
    nMemIndex=nIndexArr
    Do While nIndex<=Len(cString)
        cCurChr=Substr(cString,nIndex,1)
        If Inlist(cCurChr,Chr(34),Chr(39)) And nBegStr=0
            * Apertura stringa
            nBegStr=nIndex
            cDelimStr=cCurChr
        Else
            If cCurChr==cDelimStr
                * Chiusura stringa
                nEndStr=nIndex
            Endif
        Endif
        If nBegStr>0 And nEndStr>nBegStr
            * Dimensiona array
            nIndexArr=nIndexArr+1
            Dimension aTransArr(nIndexArr,2)
            * Aggiorna array
            aTransArr(nIndexArr,1)=Substr(cString,nBegStr,nEndStr-nBegStr+1)
            aTransArr(nIndexArr,2)=.T.
            * Aggiorna stringa
            cString=Stuff(cString,nOffset+nBegStr,nEndStr-nBegStr+1,'�'+Alltrim(Str(nIndexArr,5,0))+'�')
            * Pulizia
            nBegStr=0
            nEndStr=0
            cDelimStr=""
            nIndex=0
        Endif
        *
        nIndex=nIndex+1
    Enddo
    *
    If nIndexArr>nMemIndex
        * Dimensiona array
        nIndexArr=nIndexArr+1
        Dimension aTransArr(nIndexArr,2)
        aTransArr(nIndexArr,1)=cString
        aTransArr(nIndexArr,2)=.F.
        cString='�'+Alltrim(Str(nIndexArr,5,0))+'�'
    Endif
    Return(cString)

    * ----------------------------------------------------------------------------------------------
    * --- Funzione che sostituisce una funzione obiettivo con un corrispondente numero di �
    * ----------------------------------------------------------------------------------------------
Function ExtractFunctions(cString,cFunc,aTransArr,nIndexArr)
    Local cRes,nBegStr,nEndStr
    cRes=cString
    *
    Do While At(Lower(cFunc),Lower(cRes))>0
        nBegStr=At(Lower(cFunc),Lower(cRes))
        nEndStr=nBegStr+BalanceDelimiter(Substr(cRes,nBegStr),'(',')')
        * Dimensiona array
        nIndexArr=nIndexArr+1
        Dimension aTransArr(nIndexArr,2)
        * Aggiorna array
        aTransArr(nIndexArr,1)=Substr(cRes,nBegStr,nEndStr-nBegStr)
        aTransArr(nIndexArr,2)=.F.
        *
        cRes=Stuff(cRes,nBegStr,nEndStr-nBegStr,'�'+Alltrim(Str(nIndexArr,5,0))+'�')
        nBegStr=0
        nEndStr=0
    Enddo
    * Risultato
    Return(cRes)

    * ----------------------------------------------------------------------------------------------
    * --- Funzione che sostituisce tutte le sottostringhe con �
    * --- 	Esempio: IIF(A='STR','POPO'+'PIPI', STRTRAN(B,'CUCU','COCO'))
    * --- 	Diventa: IIF(A=�����,������+������, STRTRAN(B,������,������))
    * ----------------------------------------------------------------------------------------------
Function RemoveStrings(cString)
    Local cRes,cDelimStr,nIndex,cCurChr,nBegStr,nEndStr
    cRes=cString
    cDelimStr=""
    nBegStr=0
    nEndStr=0
    nIndex=1
    Do While nIndex<=Len(cRes)
        cCurChr=Substr(cRes,nIndex,1)
        If Inlist(cCurChr,Chr(34),Chr(39)) And nBegStr=0
            cDelimStr=cCurChr
            nBegStr=nIndex
        Else
            If cCurChr==cDelimStr
                nEndStr=nIndex
            Endif
        Endif
        If nBegStr>0 And nEndStr>nBegStr
            cRes=Stuff(cRes,nBegStr,nEndStr-nBegStr+1,Replicate("�",nEndStr-nBegStr+1))
            nBegStr=0
            nEndStr=0
            cDelimStr=""
        Endif
        nIndex=nIndex+1
    Enddo
    Return(cRes)


    * ----------------------------------------------------------------------------------------------
    * --- Funzione che sostituisce tutte le sottostringhe con �
    * --- 	Esempio: IIF(A='STR','POPO'+'PIPI', STRTRAN(B,'CUCU','COCO'))
    * --- 	Diventa: IIF(A=�����,������+������, STRTRAN(B,������,������))
    * ----------------------------------------------------------------------------------------------
Function RemoveFunctions(cString,cFunc)
    Local cRes,cDelimStr,nIndex,cCurChr,nBegStr,nEndStr
    cRes=cString
    nBegStr=0
    nEndStr=0
    nIndex=1
    Do While At(Lower(cFunc),Lower(cRes))>0
        nBegStr=At(Lower(cFunc),Lower(cRes))
        nEndStr=nBegStr+BalanceDelimiter(Substr(cRes,nBegStr),'(',')')
        * Dimensiona array
        cRes=Stuff(cRes,nBegStr,nEndStr-nBegStr,Replicate('�',nEndStr-nBegStr))
        nBegStr=0
        nEndStr=0
    Enddo
    Return(cRes)

    * ----------------------------------------------------------------------------------------------
    * --- Restituisce la posizione della parentesi chiusa in una stringa
    * ----------------------------------------------------------------------------------------------
Function BalanceDelimiter(cString,cStartDelim,cEndDelim)
    Local balanced,nRes,nIndex,cCurChr
    * Rimuove le stringhe per evitare finti bilanciamenti
    cString=RemoveStrings(cString)
    * Bilancia
    nRes=0
    balanced=1
    nIndex=At(cStartDelim,cString)+1
    Do While nIndex<=Len(cString) And balanced>0
        cCurChr=Substr(cString,nIndex,1)
        Do Case
            Case cCurChr==cStartDelim
                balanced=balanced+1
            Case cCurChr==cEndDelim
                balanced=balanced-1
                nRes=nIndex
        Endcase
        *
        nIndex=nIndex+1
    Enddo
    * Risultato
    Return(nRes)

    * -------------------------------------------------------------------------------------
Function FindChr(cString,cChr)
    Local nRet,cCurChr,nIndex,lOpenStr,cDelimStr
    nRet=0
    cDelimStr=""
    lOpenStr=.F.
    cString=RemoveFunctions(cString,'strtran(')
    cString=RemoveFunctions(cString,'tran(')
    cString=RemoveFunctions(cString,'nvl(')
    cString=RemoveFunctions(cString,'left(')
    cString=RemoveFunctions(cString,'right(')
    nIndex=1
    Do While nRet=0 And nIndex<=Len(cString)
        cCurChr=Substr(cString,nIndex,1)
        If cCurChr==cChr And Not(lOpenStr)
            nRet=nIndex
        Else
            If lOpenStr
                If cCurChr==cDelimStr
                    lOpenStr=.F.
                    cDelimStr=""
                Endif
            Else
                If Inlist(cCurChr,Chr(34),Chr(39))
                    lOpenStr=.T.
                    cDelimStr=cCurChr
                Endif
            Endif

        Endif
        nIndex=nIndex+1
    Enddo
    Return(nRet)
