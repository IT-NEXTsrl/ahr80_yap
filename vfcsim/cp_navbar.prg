* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_NAVBAR
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Fabrizio Pollina
* Data creazione: 20/03/2008
* Aggiornato il : 01/09/2008
* Traduzione    : 21/03/2008
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Classi per destkop men� (CTRL-D)
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"
#Define WM_THEMECHANGED   0x031A
#Define IDI_INFORMATION   32516
#Define IDI_ERROR          32513
#Define IDI_EXCLAMATION    32515
#Define IDI_QUESTION       32514
#Define WM_NULL        0
#Define WM_SYSCOMMAND  0x112
#Define WM_LBUTTONUP   0x202
#Define MOUSE_MOVE     0xf012
#Define WM_MOUSEHOVER	0x2A1
#Define GWL_EXSTYLE	-20
#Define WS_EX_LAYERED	0x80000
#Define LWA_ALPHA	0x2
#Define WM_MOUSEMOVE	0x200
#Define WM_LBUTTONDOWN	0x201
#Define WM_KEYDOWN 0x0100
#Define GCL_STYLE	-26
#Define	CS_DROPSHADOW	0x20000
#Define WM_NCACTIVATE	0x86
#Define GWL_WNDPROC	-4
#Define WM_CREATE	0x1
#Define WM_SHOWWINDOW	0x18
#Define WM_DESTROY	0x2
#Define WM_MOVE	0x3
#Define WM_WINDOWPOSCHANGED 0x47
#Define SC_MAXIMIZE  		  0xF030
#Define SC_RESTORE 			  0xF120
#Define WM_NCLBUTTONDBLCLK 0xA3

#Define NAVBAR_BORDERCOLOR	1
#Define CONTAINER_BORDERCOLOR	2
#Define TITLE_BORDERCOLOR	3
#Define TITLE_FONTCOLOR	4
#Define TITLE_BACKGROUND_LEFT	5
#Define TITLE_BACKGROUND_WIDTH	6
#Define PANELVERTICAL_BACKCOLOR	7
#Define PANELVERTICAL_FONTCOLOR	8
#Define PANELBUTTON_SELECTED_FONTCOLOR	9
#Define PANELBUTTON_NOTSELECTED_FONTCOLOR	10
#Define FORM_BACKGROUND_PICTURE	11
#Define OVERFLOWPANELBUTTON_FOCUSEDNOTSELECTED_PICTURE	12
#Define OVERFLOWPANELBUTTON_FOCUSEDSELECTED_PICTURE	13
#Define OVERFLOWPANELBUTTON_NOTFOCUSEDSELECTED_PICTURE	14
#Define OVERFLOWPANEL_BACKGROUND_PICTURE	15
#Define OVERFLOWPANEL_MENUBUTTON_PICTURE	16
#Define PANELBUTTON_FOCUSEDNOTSELECTED_PICTURE	17
#Define PANELBUTTON_FOCUSEDSELECTED_PICTURE	18
#Define PANELBUTTON_NOTFOCUSEDSELECTED_PICTURE	19
#Define PANELBUTTON_NOTFOCUSEDNOTSELECTED_PICTURE	20
#Define SHRINKBUTTON_FOCUSEDNOTSHRUNK_PICTURE	21
#Define SHRINKBUTTON_FOCUSEDSHRUNK_PICTURE	22
#Define SHRINKBUTTON_NOTFOCUSEDNOTSHRUNK_PICTURE	23
#Define SHRINKBUTTON_NOTFOCUSEDSHRUNK_PICTURE	24
#Define SPLITTER_BACKGROUND_PICTURE	25
#Define SPLITTER_GRIPPER_PICTURE	26
#Define TITLE_BACKGROUND_PICTURE	27
#Define VERTICAL_SPLITTER_BACKGROUND_PICTURE	28
#Define VERTICAL_SPLITTER_GRIPPER_PICTURE	29
#Define CONTAINER_BACKCOLOR	30
#Define VERTICALSHRINKBUTTON_SHRUNK_PICTURE	31
#Define VERTICALSHRINKBUTTON_NOTSHRUNK_PICTURE	32
#Define PANELTITLE_NOTFOCUSEDBACKGROUND_PICTURE	33
#Define PANELTITLE_FOCUSEDBACKGROUND_PICTURE	34
#Define PANELTITLE_BOTTOMLINE_COLOR	35
#Define PANELTITLE_TOPLINE_COLOR	36
#Define PANELTITLE_FONTCOLOR	37
#Define CLOSEBUTTON_PICTURE	38
#Define CLOSEBUTTONFOCUSED_PICTURE	39
#Define PINBUTTON_PICTURE	40
#Define PINBUTTON_FOCUSED_PICTURE	41
#Define PINBUTTON_PINNED_PICTURE	42
#Define PINBUTTON_FOCUSEDPINNED_PICTURE	43
#Define DELETEBUTTON_PICTURE	44
#Define DELETEBUTTONFOCUSED_PICTURE	45
#Define OKBUTTON_PICTURE	46
#Define OKBUTTONFOCUSED_PICTURE	47
#Define OVERFLOWPANELFOCUSED_MENUBUTTON_PICTURE	48
#Define DOTSBUTTON_PICTURE	49
#Define DOTSBUTTONFOCUSED_PICTURE	50
#Define TOOLBAR_BTN_HOVER 51
#Define TOOLBAR_BTN_DOWN 52
#Define TOOLBAR_BACKGRD_V 53
#Define TOOLBAR_BACKGRD_H 54
#Define TOOLBAR_BORDERCOLOR 55
#Define TOOLBAR_BACKGRD_H 54
#Define TOOLBARFRAME_COLOR 56
#Define FORM_TBMDI_BACKGROUND_PICTURE 64
#Define MDIMINBUTTONFOCUSED_PICTURE 65
#Define MDIMINBUTTON_PICTURE 66
#Define MDIMAXBUTTONFOCUSED_PICTURE 67
#Define MDIMAXBUTTON_PICTURE 68
#Define MDICLOSEBUTTONFOCUSED_PICTURE 69
#Define MDICLOSEBUTTON_PICTURE 70
#Define UPBUTTON_PICTURE	81
#Define UPBUTTONFOCUSED_PICTURE	82
#Define DWNBUTTON_PICTURE	83
#Define DWNBUTTONFOCUSED_PICTURE	84
#Define OVERFLOWPANEL_MENUBUTTONRSS_PICTURE	85
#Define OVERFLOWPANEL_MENUBUTTONFOCUSEDRSS_PICTURE	86
#Define ZOOMT_PICTURE	87
#Define ZOOMTFOCUSED_PICTURE	88


#Define BLACKNESS	0x42
#Define DSTINVERT	0x550009
#Define MERGECOPY	0xC000CA
#Define MERGEPAINT	0xBB0226
#Define NOTSRCCOPY	0x330008
#Define NOTSRCERASE	0x1100A6
#Define PATCOPY		0xF00021
#Define PATINVERT	0x5A0049
#Define PATPAINT	0xFB0A09
#Define SRCAND		0x8800C6
#Define SRCCOPY		0xCC0020
#Define SRCERASE	0x440328
#Define SRCINVERT	0x660046
#Define SRCPAINT	0xEE0086
#Define WHITENESS	0xFF0062
#Define PATINVERT	0x5A0049

#Define HKEY_LOCAL_MACHINE	0x80000002
#Define HKEY_CLASSES_ROOT	0x80000000
#Define ERROR_SUCCESS	0
#Define KEY_READ	131097

#Define DI_NORMAL	0x3
#Define DI_MASK	0x1
#Define DI_IMAGE	0x2

Parameters bCtrlD && Se .t. lanciato attaverso CTRL-D, in questo caso, se oggetto non creato va creato e mostrato...
*--- Il DeskMenu funziona solo con la nuova interfaccia (se l'utente non definito significa che sono in fase di selezione utente, non deve quindi partire il desk men�)
If i_VisualTheme = -1 Or i_cDeskMenu='H' Or i_codute=0
    Return
Endif
*--- Primo avvio, creo l'oggetto DeskMenuManager
If Vartype(i_oDeskmenu)="U"
    Public i_oDeskmenu
    _Screen.Newobject("NavBar","cp_NavBar")
    i_oDeskmenu = _Screen.NavBar
    Local numpanel
    numpanel=1
    With i_oDeskmenu.maINPANE
        L_Obj = .addpanel("StandardPanel","Menu navigator",numpanel,.T.,.F.,"","")
        L_Obj.Height = 217
        L_Obj.OriginalHeight = 217
        L_Obj.Body.AddObject("SubMenuNav","_SubMenuNavigator")
        *--- Menu dati recenti
        If i_bRecentMenu_navbar
            numpanel=numpanel+1
            L_Obj = .addpanel("StandardPanel",Iif(isalt(),MSG_RECENT_PR, MSG_RECENT),numpanel,.T.,.F.,"","")
            L_Obj.Height = 40
            L_Obj.OriginalHeight = 40
            If Vartype(cCursRecen)<>"C"
                Public cCursRecen
                cCursRecen=""
            Endif
            If Empty(cCursRecen)
                cCursRecen = Sys(2015)
            Endif
            L_Obj.Body.AddObject("oSearchRecent","SearchRecent",cCursRecen,"NAVBAR")
            L_Obj.Body.oSearchRecent.Width=L_Obj.Body.Width
            L_Obj.Body.oSearchRecent.Anchor=10
            L_Obj.Body.oSearchRecent.Visible=.T.
            i_oDeskmenu.oSearchRecent = L_Obj.Body.oSearchRecent
            * CREO IL CURSORE DEI RECENTI
            If Not Used(cCursRecen) Or Recno(cCursRecen)=0
                GSUT_BMR('C')
            Endif
        Endif
		
		*--- SearchMenuItem
		If i_bSearchMenuDesk
		    numpanel=numpanel+1
            L_Obj = .addpanel("StandardPanel","Cerca funzionalit�",numpanel,.T.,.F.,"","")
            L_Obj.Height = 40
            L_Obj.OriginalHeight = 40
			i_oDeskmenu.oSearchMenuPanel = L_Obj
		Endif		

        If g_bDskRSS
            Local l_oldErr, l_Err, l_TestObj
            l_oldErr = On("Error")
            l_Err = .F.
            On Error l_Err = .T.
            *--- Se si verifica un errore nella creazione del feed manager non aggiungo il controllo
            l_TestObj = Createobject("FeedsManager")
            l_rootFolder = l_TestObj.RootFolder
            If !l_Err And Vartype( l_rootFolder)='O'
                numpanel=numpanel+1
                L_Obj = .addpanel("StandardPanel","RSS Viewer",numpanel,.T.,.F.,"","")
                L_Obj.Height = 201
                L_Obj.OriginalHeight = 201
                L_Obj.Body.AddObject("oRSSViewer","RSSViewer")
                L_Obj.Body.oRSSViewer.Width = L_Obj.Body.Width
                L_Obj.Body.oRSSViewer.Anchor=10
                L_Obj.Body.oRSSViewer.Visible=.T.
                i_oDeskmenu.oRSSViewer = L_Obj.Body.oRSSViewer
                numpanel=numpanel+1
                L_Obj = .addpanel("StandardPanel","Windows manager",numpanel,.T.,.F.,"","")
            Else
                ah_ErrorMsg("Feed manager non disponibile,verificare versione di internet explorer")
                g_bDskRSS=.F.
            Endif
            l_TestObj = .Null.
            *--- Ripristino error
            On Error &l_oldErr
        Else
            numpanel=numpanel+1
            L_Obj = .addpanel("StandardPanel","Windows manager",numpanel,.T.,.F.,"","")
        Endif
        L_Obj.Body.AddObject("oListWnd","ListWndOpen")
        i_oDeskmenu.oListWnd = L_Obj.Body.oListWnd
    Endwith
    *--- TBMDI
    _Screen.AddObject( "ImgBackground", "PaneBackground")	&&Gradiente di sfondo
    _Screen.Newobject("TBMDI", "TitleBarMDI")
    _Screen.imgBackground.ZOrder( 1)
    If Not bCtrlD && Se lanciato dal CTRL-D devo anche visualizzarlo
        Return
    Endif
Endif

If !i_oDeskmenu.bLoadedMenu
    Local cMenuFile
    cMenuFile = ''
    cMenuFile = cp_FindNamedDefaultFileDeskMenu("DeskMenu", "vmn")
    If !Empty(m.cMenuFile)
        i_oDeskmenu.LoadMenu(cMenuFile)
    Else
        *--- Se non trovo un menu deskmenu passo alla funzione il cursore del menu principale
        If Vartype(i_CUR_MENU)<>'U'
            i_oDeskmenu.LoadMenu(i_CUR_MENU, .T.)
        Endif
    Endif
    *--- Carico RSS Feed in RSS Feed Viewer
    If g_bDskRSS
        i_oDeskmenu.LoadRSS()
    Endif
Endif

*--- SearchMenuItem
If i_bSearchMenuDesk and TYPE("i_oDeskmenu.oSearchMenuPanel.Body.oSearchMenu")<>"O"
	i_oDeskmenu.oSearchMenuPanel.Body.AddObject("oSearchMenu","SearchRecent",i_MenuToolbar.oPopupMenu.cCursorName,"NAVBAR",.t.)
	i_oDeskmenu.oSearchMenuPanel.Body.oSearchMenu.Width=i_oDeskmenu.oSearchMenuPanel.Body.Width
	i_oDeskmenu.oSearchMenuPanel.Body.oSearchMenu.Anchor=10
	i_oDeskmenu.oSearchMenuPanel.Body.oSearchMenu.Visible=.T.
Endif
		
*--- Successivi avvii, Mostro/Nascondo DeskMenu
*--- prima di nasconderlo/mostrarlo do il focus sull'oggetto dummy
*--- altrimenti potrebbe rimanere visibile una parte del controllo (cursore lampeggiante)
i_oDeskmenu.maINPANE.btnDummy.SetFocus()
i_oDeskmenu.Visible = !i_oDeskmenu.Visible

Func cp_FindNamedDefaultFileDeskMenu(cName,cExt)
    If i_MonitorFramework
        Local i
        If cp_fileexist(Alltrim(i_usrpath)+'deskmenu\'+cName+'_'+Alltrim(Str(i_codute))+'.'+cExt,.T.)
            i_pathFindFile = Alltrim(i_usrpath)+'deskmenu\'+cName+'_'+Alltrim(Str(i_codute))+'.'+cExt
            Return(Lower(Addbs(Justpath(i_pathFindFile))+(Juststem(i_pathFindFile))))
        Endif
        If cp_fileexist('deskmenu\'+cName+'_'+Alltrim(Str(i_codute))+'.'+cExt,.T.)
            i_pathFindFile = 'deskmenu\'+cName+'_'+Alltrim(Str(i_codute))+'.'+cExt
            Return(Lower(Addbs(Justpath(i_pathFindFile))+(Juststem(i_pathFindFile))))
        Endif
        If i_codute<>0 && se l' utente e' gia' stato caricato si possono provare i default per utente e gruppi di appartenenza
            cp_FillGroupsArray()
            For i=1 To Alen(i_aUsrGrps)
                If cp_fileexist(Alltrim(i_usrpath)+'deskmenu\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt,.T.)
                    i_pathFindFile = Alltrim(i_usrpath)+'deskmenu\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt
                    Return(Lower(Addbs(Justpath(i_pathFindFile))+(Juststem(i_pathFindFile))))
                Endif
                If cp_fileexist('deskmenu\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt,.T.)
                    i_pathFindFile = 'deskmenu\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt
                    Return(Lower(Addbs(Justpath(i_pathFindFile))+(Juststem(i_pathFindFile))))
                Endif
            Next
        Endif
        If cp_fileexist('deskmenu\'+cName+'.'+cExt,.T.)
            i_pathFindFile = 'deskmenu\'+cName+'.'+cExt
            Return(Lower(Addbs(Justpath(i_pathFindFile))+(Juststem(i_pathFindFile))))
        Endif
        i_pathFindFile = Alltrim(MSG_NOT_FILEEXIST)+'|'+Alltrim(cName)+'.'+Alltrim(cExt)
        Return('')
    Else
        Local i
        If cp_fileexist ('deskmenu\'+cName+'_'+Alltrim(Str(i_codute))+'.'+cExt,.T.)
            Return('deskmenu\'+cName+'_'+Alltrim(Str(i_codute)))
        Endif
        If i_codute<>0 && se l' utente e' gia' stato caricato si possono provare i default per utente e gruppi di appartenenza
            cp_FillGroupsArray()
            For i=1 To Alen(i_aUsrGrps)
                If cp_fileexist('deskmenu\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i]))+'.'+cExt,.T.)
                    Return('deskmenu\'+cName+'_g'+Alltrim(Str(i_aUsrGrps[i])))
                Endif
            Next
        Endif
        If cp_fileexist('deskmenu\'+cName+'.'+cExt,.T.)
            Return('deskmenu\'+cName)
        Endif
        Return('')
    Endif
Endfunc

**************************************************
* Class: cp_NavBar
#Define SHOW_MORE "Mostra pi� pulsanti"
#Define SHOW_LESS "Mostra meno pulsanti"
#Define MIN_BARSIZE 37
Define Class CP_NAVBAR As Container

    Left = 3
    Top = 3
    Anchor = 7
    Width = 200
    Height = 400
    BackColor = Rgb(255,255,255)

    *-- Numero massimo di pulsanti mostrati nel pannello.
    maxshowedbuttons = 5
    *-- Pulsante attualmente selezionato.
    selectedbutton = 0
    *-- Numero di pulsanti attualmente visualizzati
    showedbuttons = 0
    * Numero di pulsanti nascosti da operazioni di resize
    ResizeHideButtons = 0
    *-- Testo mostrato nel popup men� per visualizzare pi� pulsanti.
    mnushowmoretext = SHOW_MORE
    *-- Testo mostrato nel popup men� per visualizzare meno pulsanti.
    mnushowlesstext = SHOW_LESS
    Name = "cp_NavBar"
    NumButtons = 0
    bLoadedMenu = .F.
    cMenu = ""
    cCursor = ""

    oListWnd = .Null.
    oRSSViewer = .Null.
    oSearchRecent = .Null.
	oSearchMenuPanel = .Null.

    Add Object panelvertical As panelvertical With ;
        Top = 27, Left = 0, Name = "PanelVertical", ;
        lblCaption.Name = "lblCaption", Visible=.F.

    Add Object Title As Title With ;
        Top = 0,  Name = "Title", ;
        lblCaption.Name = "lblCaption"

    Add Object maINPANE As PanelManager With ;
        top = 28, Left = 1,Width = 198, ;
        height = 332, BorderWidth=0

    Add Object Panel As Panel With ;
        Top = 380, ;
        Left = 1, ;
        Height = 0, ;
        Name = "Panel"

    Add Object overflowpanel As overflowpanel With ;
        Top = 368, ;
        Left = 1, ;
        Width = 195, ;
        Anchor = 14, ;
        Name = "OverflowPanel", ;
        MenuButton.imgPicture.Name = "imgPicture", ;
        MenuButton.Name = "MenuButton"

    Add Object HSplitter As splitterhorizontal With ;
        Top = 360, Left = 0, ;
        width = 195 , ;
        objectabove = "This.Parent.MainPane", ;
        objectbelow = "This.Parent.Panel", ;
        Name = "HSplitter", ;
        imgBackground.Name = "imgBackground", ;
        imgGripper.Name = "imgGripper"

    Add Object VSplitter As splittervertical With ;
        Top = 1, ;
        Left = 193, ;
        Name = "VSplitter",;
        rangemax = 0.5, ;
        rangemin = MIN_BARSIZE , ;
        Width=7

    Add Object shrinkbutton As shrinkbutton With ;
        Anchor = 9, ;
        Top = 5, ;
        Left = 176, ;
        Name = "ShrinkButton"

    Add Object LineBorder As Line With Top=0, Left=0, Width=0
    Add Object LineBottom As Line With Left=0, Height=0

    Protected Procedure Init
        If Not This.Parent.BaseClass=="Form"
            cp_ErrorMsg(MSG_CPNAVBAR_INIT)
            Return .F.
        Else
            *Catturo l'evento cambio tema del ThemeManager
            i_ThemesManager.AddInstance(This, "ChangeTheme")
            With Thisform
                .LockScreen = .T.
                If .BaseClass=="Form"
                    .MinHeight = This.Title.Height + This.overflowpanel.Height + This.HSplitter.Height + 1
                    .MinWidth = This.Width + 4
                Endif
                .ShowTips = .T.
                With This
                    * Inizializzo il tema durante la creazione

                    .ChangeTheme()
                    .HSplitter.Anchor = 0
                    .HSplitter.Width = .Width
                    .HSplitter.Anchor = 14
                    .Anchor=0
                    .Height= .Parent.Height - 6
                    .VSplitter.Height= .Height
                    .VSplitter.Anchor = 13
                    .Anchor=7
                    .LineBorder.Height = .Height
                    .LineBorder.Anchor = 7
                    .LineBottom.Top = .Height - 1
                    .LineBottom.Width = .Width
                    .LineBottom.Anchor = 14
                Endwith
                .LockScreen = .F.
            Endwith
        Endif
    Endproc

    Procedure ChangeSettings
        If This.shrinkbutton.ISShrunk()
            This.shrinkbutton.Tag = Alltrim(Str(i_nDeskMenuInitDim))
        Else
            This.Anchor=0
            This.Width = i_nDeskMenuInitDim
            This.Anchor=7
            This.OnResize()
        Endif
        If ((This.shrinkbutton.ISShrunk() And i_cDeskMenuStatus='A') Or (!This.shrinkbutton.ISShrunk() And i_cDeskMenuStatus='C'))
            This.shrinkbutton.Click()
        Endif
        If This.maxshowedbuttons<>i_nDeskMenuMaxButton
            This.maxshowedbuttons=i_nDeskMenuMaxButton
            If 	This.ResizeHideButtons = 0 And This.showedbuttons>0
                Local nShowMoreButton
                nShowMoreButton = This.maxshowedbuttons - This.showedbuttons
                Do While m.nShowMoreButton<>0
                    If m.nShowMoreButton>0
                        This.ShowMore()
                        m.nShowMoreButton = m.nShowMoreButton - 1
                    Else
                        This.ShowLess()
                        m.nShowMoreButton = m.nShowMoreButton + 1
                    Endif
                Enddo
            Endif
        Endif
        This.maINPANE.ChangeSettings()
    Endproc

    Procedure ResizeNavBar
        This.Height = Max(_Screen.Height - 6, 0)
*!*	        This.OnResize()
    Endproc

    Procedure OnResize()
    	LockWindowUpdate(_screen.HWnd)
        Local lnWidth
        lnWidth = Iif( This.Visible, This.Width, 0)
        With _Screen
            If Vartype(.TBMDI)<>"U"
                .TBMDI.Move( This.Left + m.lnWidth, This.Top, Max( 0, .Width - m.lnWidth - 6 ), This.Height)
            Endif
            If Vartype(.imgBackground)<>'U' AND Not(Vartype(_Screen.Cnt)<>"U" AND _Screen.Cnt.cScreen='FullScreen')
                If Vartype(.TBMDI) <> "U" And .TBMDI.Visible
                    .imgBackground.Move( 0, 0, .Width, .Height)
                Else
                    .imgBackground.Move( 0, 0, This.Left + m.lnWidth, .Height)
                Endif
            ENDIF

            *--- Sposto la backgruond mask se presente
            If Vartype(.Cnt)<>"U" AND NOT ISNULL(.Cnt)
            		.Cnt.Resize()
*!*		                Do Case
*!*		                    Case Vartype(_Screen.Cnt.nAlignmentCnt)="N" And _Screen.Cnt.nAlignmentCnt = 1
*!*		                        _Screen.Cnt.Move(Int((_Screen.Width - m.lnWidth-_Screen.Cnt.Width)/2) + This.Left + m.lnWidth)
*!*		                    Case Vartype(_Screen.Cnt.nAlignmentCnt)="N" And _Screen.Cnt.nAlignmentCnt = 2
*!*		                        _Screen.Cnt.Move(This.Left + m.lnWidth)
*!*		                    Case Vartype(_Screen.Cnt.nAlignmentCnt)="N" And _Screen.Cnt.nAlignmentCnt = 3
*!*		                        *--- lascio stare, allineata a destra
*!*		                    Otherwise
*!*		                        _Screen.Cnt.Move(_Screen.Cnt.nLeft + This.Left + m.lnWidth)
*!*		                Endcase
            ENDIF
        ENDWITH
        LockWindowUpdate(0)
    Endproc

    Procedure Resize()
        DoDefault()
        This.OnResize()
    Endproc

    Function EvalModules(i_cMod, i_bEnabled)
        Local PosVirg, Par1, Par2, i_cSkip
        If !Empty(m.i_cMod)
            If Upper(Left(m.i_cMod,8)) = 'G_OMENU'
                i_cMod  = Strtran(m.i_cMod,Chr(253),"'")
                PosVirg = Rat(',',m.i_cMod)
                * Estraggo l'oggetto dalla stringa i_cmod
                Par1 = Substr(m.i_cMod,9,m.PosVirg - 9)
                * Estraggo la condizione da testare sull'oggetto
                Par2 = Substr(m.i_cMod,m.PosVirg+1,Len(m.i_cMod)-(m.PosVirg+1))
                * verifico la condizione sull'oggetto
                i_cSkip=!g_Omenu.ObjInfo(m.Par1,m.Par2)
            Else
                If At('(',m.i_cMod)>0
                    i_cMod  = Strtran(m.i_cMod,"*","'")
                    i_cSkip = !Eval(m.i_cMod)
                Else
                    i_cSkip=At(','+i_cMod+',',','+i_cModules+',')=0
                Endif
            Endif  &&UPPER(LEFT(m.i_cmod,8)) = 'G_OMENU'
        Endif
        Return m.i_cSkip Or !i_bEnabled
    Endfunc

    Procedure LoadMenu(cMenuFile, bDefault)
        This.bLoadedMenu = .T.
        *--- Carico menu
        With This
            .cMenu = m.cMenuFile
            *--- Carico menu
            If m.bDefault
                .cCursor = m.cMenuFile
            Else
                .cCursor = Sys(2015)
                Do Vmn_To_Cur In vu_exec With .cMenu+'.VMN', .cCursor
            Endif
            *--- Aggiungo i bottoni delle voci principali
            Select * From (.cCursor) Where  Occurs('.',LVLKEY)=1 Order By LVLKEY Into Cursor __BTN_CUR__
            Local l_i, cPane, l_Name, l_BmpName, l_oBtn, l_cClientPane, cMod, bEnabled, i_cSkip, cIco, cCmd, l_BmpName24
            l_i = 1
            Select("__BTN_CUR__")
            Scan
                m.cIco = "DirCl.ico"
                m.cMod=Alltrim(MODULO)
                m.bEnabled=This.EvalModules(m.cMod, Enabled)
                m.l_Name = Alltrim(Chrtran(cp_Translate(__BTN_CUR__.VOCEMENU), "&", ""))
                m.l_BmpName = Alltrim(__BTN_CUR__.CPBMPNAME)
                m.cIco = evl(m.l_BmpName,m.cIco)
                m.l_BmpName = ""
                m.l_BmpName24 = ""
                If __BTN_CUR__.Directory = 1
                    m.cCmd = "1#"+Alltrim(__BTN_CUR__.NAMEPROC)
                Else
                    m.cCmd = Alltrim(Str(__BTN_CUR__.Directory))+"#"+Alltrim(__BTN_CUR__.LVLKEY)+"#"+l_Name
                Endif
                *If !Empty(m.l_BmpName)
                *    If cp_fileexist(Addbs(Justpath(l_BmpName))+Juststem(l_BmpName)+"_24."+Justext(l_BmpName))
                *        m.l_BmpName24 = Addbs(Justpath(l_BmpName))+Juststem(l_BmpName)+"_24."+Justext(l_BmpName)
                *    Else
                *        m.l_BmpName24 = m.l_BmpName
                *    Endif
                *Endif
                .addbutton(m.l_Name, m.l_BmpName24 , l_BmpName, m.cIco, __BTN_CUR__.Directory=1, "C", m.cCmd, !m.bEnabled)

                l_i = l_i + 1
                Select("__BTN_CUR__")
            Endscan
            Use In Select("__BTN_CUR__")
        Endwith
    Endproc

    *--- Carica i Feed in RSS Feed Viewer
    Procedure LoadRSS()
        Local l_oldArea
        l_oldArea = Select()
        vq_exec("QUERY\GSUT_KRS.VQR", .Null., "_RSSFeedToAdd_")
        Select("_RSSFeedToAdd_")
        Scan
            i_oDeskmenu.oRSSViewer.AddFeedURL(_RSSFeedToAdd_.RFRSSURL, _RSSFeedToAdd_.RFMAXITM)
            Select("_RSSFeedToAdd_")
        Endscan
        Use In Select("_RSSFeedToAdd_")
        i_oDeskmenu.oRSSViewer.ReLoadRSS()
    Endproc

    *-- Aggiunge un bottone con relativo panel
    Procedure addbutton
        Lparameters cCaption, cPicture24, cPicture16, cIcon, bNotSelectable, cCmdType, cCmd, bEnabled
        Local bIconBmp, cBmp24, cBmp16
        bIconBmp=Vartype(cIcon)='C' And !Empty(cIcon)
        If bIconBmp
            *Aggiunta del pulsante passando un icona e non due
            *bitmap, leggo le informazioni dell'icona, trasferisco
            *in due bitmap d'appoggio e le passo al pulsante, poi
            *le due bitmap d'appoggio verranno eliminate
            *Asseieme alle due bitmap d'appoggio verranno create
            *le relative maschere di visibilit� del fox.

            cBmp24 = i_ThemesManager.GetBmpFromIco(cp_GetStdImg(Forceext(cIcon,''), 'ico')+'.ico', 24)
            cBmp16 = i_ThemesManager.GetBmpFromIco(cp_GetStdImg(Forceext(cIcon,''),'ico')+'.ico', 16)
        Else
            cBmp24 = cp_GetStdImg(Forceext(cPicture24,''),'bmp')
            cBmp16 = cp_GetStdImg(Forceext(cPicture16,''),'bmp')
        Endif

        With This
            * Disancoro lo splitter orizzontale altrimenti ridimensionando
            * viene riposizionato male
            .HSplitter.Anchor = 0
            .Panel.addbutton(m.cCaption, cBmp24+".bmp", Iif(m.bIconBmp, m.cIcon, m.cPicture24), m.bNotSelectable, m.cCmdType, m.cCmd, m.bEnabled)
            .NumButtons = .NumButtons + 1
            .overflowpanel.addbutton(cCaption, cBmp16+".bmp",Iif(bIconBmp, cIcon, m.cPicture16), m.bNotSelectable, m.cCmdType, m.cCmd, m.bEnabled)
            If .showedbuttons<.maxshowedbuttons
                .ShowMore()
            Else
                .ReArrangeButtons()
            Endif
            * Ripristino l'ancoramento dello splitter orizzontale
            .HSplitter.Anchor = 14
        Endwith
    Endproc

    * Se viene riassegnato il pulsante attualmente selezionato cambio
    * proriet� dell'oggetto e background del controllo
    Hidden Procedure selectedbutton_assign
        Lparameters vNewVal
        With This
            If .Panel.Controls(m.vNewVal).Enabled
                Local nOldSelectedButton, cTitle, cPicture24
                nOldSelectedButton = .selectedbutton
                If .Panel.Controls(m.vNewVal).bSelectable
                    .selectedbutton = m.vNewVal
                Endif
                With .Panel
                    If nOldSelectedButton>0 And This.selectedbutton<>nOldSelectedButton
                        .Controls(nOldSelectedButton).ChangeBackground()
                    Endif
                    .Controls(m.vNewVal).ChangeBackground()
                    .Controls(m.vNewVal).DoAction()
                    cTitle = .Controls(m.vNewVal).lblCaption.Caption
                    cPicture24 = .Controls(m.vNewVal).imgPicture.Picture
                Endwith
                With .overflowpanel
                    If nOldSelectedButton>0
                        .Controls(nOldSelectedButton+1).ChangeBackground()
                    Endif
                    .Controls(m.vNewVal+1).ChangeBackground()
                Endwith
                .Title.Caption = cTitle
                .ButtonClicked(m.vNewVal,cTitle,cPicture24)
            Endif
        Endwith

    Endproc

    * Riposiziona i pulsanti nella barra di trabocco
    Procedure ReArrangeButtons
        _Screen.LockScreen=.T.
        With This.overflowpanel
            Local nLeft, nMaxShowedButtons, nControlCount, nControl, bVisible
            With .MenuButton
                nLeft = .Left
                nMaxShowedButtons = Int(.Left / .Width)
            Endwith
            nControlCount = .ControlCount
            For nControl=nControlCount To 2 Step -1
                With .Controls(nControl)
                    bVisible = .F.
                    If .TabIndex > This.showedbuttons
                        If nControl <= (This.showedbuttons + nMaxShowedButtons + 1)
                            nLeft = nLeft - .Width
                            .Anchor = 0
                            .Left = nLeft
                            .Anchor = 9
                            bVisible = .T.
                        Endif
                    Endif
                    .Visible = bVisible
                Endwith
            Endfor
        Endwith
        _Screen.LockScreen=.F.
    Endproc

    * Mostra pi� pulsanti nel pannello
    Procedure ShowMore
        If This.showedbuttons<This.maxshowedbuttons And This.NumButtons>This.showedbuttons
            With This.HSplitter
                .Anchor = 0
                .Top = .Top - 32
                .Anchor = 14
            Endwith
        Endif
    Endproc

    * Mostra meno pulsanti nel pannello
    Procedure ShowLess
        If This.showedbuttons>0
            With This.HSplitter
                .Anchor = 0
                .Top = .Top + 32
                .Anchor = 14
            Endwith
        Endif
    Endproc

    * Aggiornamento grafica su cambio tema
    Procedure ChangeTheme
        This.BorderColor = _Screen.cp_Themesmanager.GetProp(NAVBAR_BORDERCOLOR)
        This.maINPANE.BackColor= _Screen.cp_Themesmanager.GetProp(NAVBAR_BORDERCOLOR)
        This.VSplitter.BackColor = _Screen.cp_Themesmanager.GetProp(CONTAINER_BORDERCOLOR)
        This.VSplitter.BorderColor = _Screen.cp_Themesmanager.GetProp(CONTAINER_BORDERCOLOR)
        This.LineBorder.BorderColor = _Screen.cp_Themesmanager.GetProp(CONTAINER_BORDERCOLOR)
        This.LineBottom.BorderColor = _Screen.cp_Themesmanager.GetProp(CONTAINER_BORDERCOLOR)
    Endproc

    * Pulsante cliccato
    Procedure ButtonClicked
        Lparameters lnNumber, lcCaption, lcPicture24
    Endproc

    * Cambio stato cp_NavBar (Chiusa/Aperta)
    Procedure viewmodechanged
        Lparameters bShrunk
    Endproc

    * Cambio la visualizzazione se la cp_NavBar viene chiusa o aperta
    Procedure changeviewmode
        This.shrinkbutton.changeviewmode()
    Endproc

    Protected Procedure Destroy
        *Rilascio la verifica del messaggio di cambio tema
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
        *Unbindevents(_Screen.Hwnd, WM_MOVE)
        *Unbindevents(_Screen.Hwnd, WM_WINDOWPOSCHANGED)
    Endproc

    Procedure HSplitter.splittermoved
        With This.Parent
            If This.Top < .maINPANE.BottomMostTitle + 64
                If .ResizeHideButtons<.maxshowedbuttons
                    .ShowLess()
                    .Panel.Top = .Panel.Top + 32
                    .Panel.Height = Max(.Panel.Height - 32, 0)
                    .ResizeHideButtons = .ResizeHideButtons + 1
                Endif
            Else
                If .ResizeHideButtons>0 And This.Top>.maINPANE.BottomMostTitle + 96 And .showedbuttons<.maxshowedbuttons
                    .ShowMore()
                    .Panel.Top = .Panel.Top - 32
                    .Panel.Height = Max(.Panel.Height + 32, 0)
                    .ResizeHideButtons = .ResizeHideButtons - 1
                Endif
            Endif
            .showedbuttons = Int(.Panel.Height/32)
            .ReArrangeButtons()
            This.ObjectAboveMinSize = .overflowpanel.Top - (Min(.maxshowedbuttons,.NumButtons)*32) - 	(This.Height - 1) - .maINPANE.Top
            .panelvertical.Height = .maINPANE.Height +2
            .maINPANE.Move(.maINPANE.Left,.maINPANE.Top,.maINPANE.Width,Max(This.Top-.maINPANE.Top,0))
        Endwith
    Endproc

    Procedure Visible_assign
    Lparameters bNewVal
    Local OldLock
    If This.Visible<>m.bNewVal
    	This.Visible=m.bNewVal
    	If This.Visible
    		This.ChangeSettings()
    	Endif
    	m.OldLock = _Screen.LockScreen
    	_Screen.LockScreen=.T.
    	Activate Screen
    	? ""
    	Clear
    	_Screen.LockScreen=m.OldLock
    	*--- Sposto in secondo piano la backgruond mask se presente
    	If Vartype(_Screen.Cnt)<>"U"
    		_Screen.Cnt.ZOrder(1)
    	Endif
    	*** Zucchetti Aulla - gestione immagine desktop
    	If Type("_SCREEN.ahlogo")="O"
    		_Screen.ahlogo.ZOrder(1)
    	Endif
    	If Type("_screen.bckImg")="O"
    		_Screen.bckImg.ZOrder(1)
    	Endif
    	If Vartype(_Screen.imgBackground)='O'
    		_Screen.imgBackground.Visible= This.Visible Or _Screen.TBMDI.Visible
    		This.OnResize()
    	Endif
    Endif
    Endproc

Enddefine
*
* fine classe: cp_NavBar
**************************************************


**************************************************
* Class:	overflowpanel

Define Class overflowpanel As Container

    Width = 198
    Height = 31
    BorderWidth = 0

    Name = "overflowpanel"

    Add Object MenuButton As overflowpanelbutton With ;
        Anchor = 9, ;
        Top = 0, ;
        Left = 177, ;
        Name = "MenuButton", ;
        imgPicture.Name = "imgPicture"

    * Aggiunge un pulsante al pannello
    Procedure addbutton
        Lparameters cCaption, cPicture, cOrigPict, bNotSelectable, cCmdType, cCmd, bEnabled
        Local cMacro
        With This
            Local nControlCount
            nControlCount = .ControlCount + 1
            .Newobject("Button"+Alltrim(Str(nControlCount - 1)), "OverflowPanelButton",,, m.cCaption,m.cPicture, m.bNotSelectable, m.cCmdType, m.cCmd, m.bEnabled)
            cMacro = ".Button" +Alltrim(Str(nControlCount - 1)) +".cOrigImg= m.cOrigPict"
            &cMacro
        Endwith
    Endproc

    * Notifica cambio tema
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.Picture = .GetProp(OVERFLOWPANEL_BACKGROUND_PICTURE)
            This.MenuButton.imgPicture.Picture = .GetProp(OVERFLOWPANEL_MENUBUTTON_PICTURE)
        Endwith
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

    Protected Procedure Init
        * Inizializzo il controllo, catturo la notifica del cambio tema del ThemeManager
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        * Inizializzo le propriet� del controllo in base al tema utilizzato
        This.ChangeTheme()
    Endproc

Enddefine

* Fine classe overflowpanel
**************************************************


**************************************************
* Class:	overflowpanelbutton

Define Class overflowpanelbutton As Container

    Width = 22
    Height = 31
    BackStyle = 0
    BorderWidth = 0
    MousePointer = 15
    cOrigImg = ""
    bSelectable = .T.
    Hidden cCmdType
    cCmdType = ""
    Hidden cCmd
    cCmd = ""
    Enabled=.T.

    Name = "overflowpanelbutton"

    * Immagine del pulsante se attivo e selezionato
    Add Object imgFocusedSelected As Image With ;
        Left = 0, Top = 0, ;
        Width = 22, Height = 31, ;
        MousePointer = 15 , ;
        Stretch = 2, Visible = .F.

    * Immagine del pulsante se attivo e non selezionato
    Add Object imgFocusedNotSelected As Image With ;
        Left = 0, Top = 0, ;
        Width = 22, Height = 31, ;
        MousePointer = 15 , ;
        Stretch = 2, Visible = .F.

    * Immagine del pulsante se non attivo e selezionato
    Add Object imgNotFocusedSelected As Image With ;
        Left = 0, Top = 0, ;
        Width = 22, Height = 31, ;
        MousePointer = 15 , ;
        Stretch = 2, Visible = .F.

    Add Object imgPicture As Image With ;
        BackStyle = 0, ;
        Height = 16, ;
        Left = 3, ;
        MousePointer = 15, ;
        Top = 7, ;
        Width = 16, ;
        Name = "imgPicture"

    Protected Procedure Init
        Lparameters cToolTipText, cPicture, bNotSelectable, cCmdType, cCmd, bEnabled
        * Catturo l'evento cambio tema
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            If Upper( .Name)==Upper("MenuButton")
                .ChangeTheme()
            Else
                .TabIndex = .TabIndex - 1
                .Top = 0
                .SetAll("ToolTipText",cToolTipText)
                .imgPicture.Picture = m.cPicture
                .bSelectable = !m.bNotSelectable
                .cCmdType = m.cCmdType
                .cCmd = m.cCmd
                .Enabled = m.bEnabled
                .ChangeTheme()
                If .TabIndex == 1
                    .ChangeSelectedButton(.T.)
                Endif
            Endif
        Endwith
    Endproc

    Procedure AssignCmd
        Lparameters cCmdType, cCmd
        If Vartype(m.cCmdType)<>'C' Or Vartype(m.cCmd)<>'C'
            m.cCmdType=""
            m.cCmd=""
        Endif
        With This
            .cCmdType = m.cCmdType
            .cCmd = m.cCmd
        Endwith
    Endproc

    *-- Change selected button.
    Procedure ChangeSelectedButton
        Lparameters bNoAction
        *Se il pulsante selezionato � il pulsante del men� lo gestisco
        Local oBar
        oBar = This.Parent.Parent
        If Upper(This.Name)==Upper("MenuButton")
            Local nButtonCount, cBmp16, IdxGarbage, cMacro, cComando, cParametri, loPopupMenu, loMenuItemCfg
            Dimension ImgsGarbage[1]
            nButtonCount = This.Parent.ControlCount - 1
            If nButtonCount > 0
                loPopupMenu = Createobject("cbPopupMenu")
                loMenuItemCfg = loPopupMenu.AddMenuItem()
                loMenuItemCfg.Caption = oBar.mnushowmoretext
                loMenuItemCfg.SkipFor = Iif(oBar.showedbuttons>=Min(nButtonCount,oBar.maxshowedbuttons), ".T.", ".F.")
                loMenuItemCfg.OnClick="i_oDeskmenu.ShowMore()"
                loMenuItemCfg.Visible = .T.

                loMenuItemCfg = loPopupMenu.AddMenuItem()
                loMenuItemCfg.Caption = oBar.mnushowlesstext
                loMenuItemCfg.SkipFor = Iif(oBar.showedbuttons=0, ".T.", ".F.")
                loMenuItemCfg.OnClick="i_oDeskmenu.ShowLess()"
                loMenuItemCfg.Visible = .T.
                Local oMenu[nButtonCount, 3]
                oMenu=""
                Local oButton, pExt
                For Each oButton In This.Parent.Controls
                    With oButton
                        If Upper(.Class) == Upper("OverflowPanelButton") And .Name <> This.Name
                            oMenu[.TabIndex, 1] = .imgPicture.ToolTipText
                            m.pExt = Justext(.cOrigImg)
                            m.cBmp16 = Forceext(cp_GetStdImg(Forceext(.cOrigImg,''), m.pExt), m.pExt)
                            oMenu[.TabIndex, 2] = m.cBmp16
                            oMenu[.TabIndex, 3] = .Enabled
                        Endif
                    Endwith
                Endfor
                oButton = Null
                Local nMenuItem
                For nMenuItem=1 To Alen(oMenu,1)
                    loMenuItemCfg = loPopupMenu.AddMenuItem()
                    loMenuItemCfg.Caption = oMenu[nMenuItem, 1]
                    loMenuItemCfg.BeginGroup =  (m.nMenuItem=1)
                    *loMenuItemCfg.MarkFor = IIF(nMenuItem=oBar.Selectedbutton,".T.",".F.")
                    loMenuItemCfg.OnClick="i_oDeskmenu.Selectedbutton="+Alltrim(Str(m.nMenuItem))
                    loMenuItemCfg.SkipFor = Iif(oMenu[nMenuItem, 3], '.F.', '.T.')
                    loMenuItemCfg.Picture= oMenu[nMenuItem, 2]
                    loMenuItemCfg.Visible = .T.
                Endfor
                loPopupMenu.InitMenu(.T.)
                loPopupMenu.ShowMenu(_Screen.Left+This.Left+This.Width-3, _Screen.Top+This.Parent.Top+2)
                **************************
                *!*					Define Popup ShortcutMenu From Mrow(), Mcol() Relative Shortcut
                *!*					Define Bar 1 Of ShortcutMenu Prompt (oBar.mnuShowMoreText) Skip For oBar.Showedbuttons>=Min(nButtonCount,oBar.MaxShowedbuttons)
                *!*					On Selection Bar 1 Of ShortcutMenu oBar.ShowMore()
                *!*					Define Bar 2 Of ShortcutMenu Prompt (oBar.mnuShowLessText) Skip For oBar.Showedbuttons=0
                *!*					On Selection Bar 2 Of ShortcutMenu oBar.ShowLess()
                *!*					Define Bar 3 Of ShortcutMenu Prompt "\-"
                *!*					Local oMenu[nButtonCount, 2]
                *!*					oMenu=""
                *!*					Local oButton
                *!*					For Each oButton In This.Parent.Controls
                *!*						With oButton
                *!*							If Upper(.Class) == Upper("OverflowPanelButton") And .Name <> This.Name
                *!*								oMenu[.TabIndex, 1] = .imgPicture.ToolTipText
                *!*								if UPPER(ALLTRIM(JUSTEXT(.cOrigImg)))=="ICO"
                *!*									m.cBmp16 = _Screen.cp_Themesmanager.GetBmpFromIco(.cOrigImg, 16)
                *!*									if !cp_FileExist(m.cBmp16+".bmp")
                *!*										m.cBmp16= _Screen.cp_Themesmanager.ImgPath+m.cBmp16
                *!*									endif
                *!*								ELSE
                *!*									cBmp16= .cOrigImg
                *!*								ENDIF
                *!*								If cp_FileExist(m.cBmp16)
                *!*									oMenu[.TabIndex, 2] = m.cBmp16
                *!*								else
                *!*									oMenu[.TabIndex, 2] = ""
                *!*								endif
                *!*							Endif
                *!*						Endwith
                *!*					Endfor
                *!*					oButton = Null
                *!*					Local nMenuItem
                *!*					For nMenuItem=1 To Alen(oMenu,1)
                *!*						Define Bar (nMenuItem+5) Of ShortcutMenu Prompt (oMenu[nMenuItem, 1]) ;
                *!*									Style (IIF(nMenuItem=oBar.Selectedbutton,"B","")) ;
                *!*									Picture (oMenu[nMenuItem, 2])
                *!*						On Selection Bar (nMenuItem+5) Of ShortcutMenu oBar.Selectedbutton=(Bar()-5)
                *!*					Endfor
                *!*					Activate Popup ShortcutMenu
                *!*					Clear Popups
            Endif
        Else
            If This.bSelectable
                oBar.selectedbutton = This.TabIndex
            Else
                If !m.bNoAction
                    This.DoAction()
                    This.ChangeBackground()
                Endif
            Endif
        Endif
        oBar = Null
    Endproc

    Procedure DoAction
        Local cComando, cParametri, cMacro
        With This
            If !Empty(.cCmdType) And !Empty(.cCmd) And .Enabled
                Do Case
                    Case .cCmdType='B'
                        If At('#',.cCmd)>0
                            m.cComando=Left(.cCmd, At('#',.cCmd))
                            m.cParametri=Right(.cCmd, Len(.cCmd)-At('#',.cCmd))
                        Else
                            m.cComando=.cCmd
                            m.cParametri=""
                        Endif
                        m.cMacro=m.cComando + '(this,"' + .cCmdType + Iif(Empty(m.cParametri), '', '", '+m.cParametri) + ')'
                    Case .cCmdType='F'
                        If At('#',.cCmd)>0
                            m.cComando=Left(.cCmd, At('#',.cCmd))
                            m.cParametri=Right(.cCmd, Len(.cCmd)-At('#',.cCmd))
                        Else
                            m.cComando=.cCmd
                            m.cParametri=""
                        Endif
                        m.cMacro=m.cComando + '("' + .cCmdType + Iif(Empty(m.cParametri), '', '", '+m.cParametri) + ')'
                    Otherwise

                        Local subname, cDirectory, cTagValue, l_cClientPane
                        m.cDirectory = Left(.cCmd, 1)
                        m.cTagValue = Substr(.cCmd,3)
                        Do Case
                            Case cDirectory = "1"
                                *--- Lancia la voce di menu
                                m.cMacro="Do mhit In cp_menu With ["+Strtran(Trim(Alltrim(m.cTagValue)),'#',' with ')+"]"
                            Case cDirectory = "2"
                                With i_oDeskmenu.maINPANE.Panel1.Body
                                    .SubMenuNav.PathCombo.Clear()
                                    .SubMenuNav.SubMenuList.Clear()
                                Endwith
                                *--- Aggiorno SubMenuNav
                                Local l_Name, l_Key
                                l_Key = Left(cTagValue, At("#", cTagValue)-1)
                                l_Name = Right(cTagValue, Len(cTagValue)-Rat("#", cTagValue))
                                With i_oDeskmenu.maINPANE.Panel1.Body
                                    .SubMenuNav.aPath.Remove(-1)
                                    .SubMenuNav.FillList(l_Key, l_Name, i_oDeskmenu.cCursor)
                                Endwith
                                m.cMacro=""
                        Endcase
                        *!*m.cMacro=.cCmd
                Endcase
                &cMacro
            Endif
        Endwith
    Endproc

    * Cambio immagine di sfondo del pulsante
    Procedure ChangeBackground
        Lparameters bGotFocus
        #Define nFocused 8
        #Define nNotFocused 16
        #Define nSelected 32
        #Define nNotSelected 64
        Local nState, cImage
        With This
            nState = (Iif(bGotFocus,nFocused,nNotFocused) + ;
                Iif(.Parent.Parent.selectedbutton==.TabIndex,nSelected,nNotSelected))

            .imgFocusedSelected.Visible=.F.
            .imgFocusedNotSelected.Visible=.F.
            If .Enabled
                .imgNotFocusedSelected.Visible=.F.
                Do Case
                    Case nState = (nFocused + nSelected)
                        .imgFocusedSelected.Visible=.T.
                    Case nState = (nFocused + nNotSelected)
                        .imgFocusedNotSelected.Visible=.T.
                    Case nState = (nNotFocused + nSelected)
                        .imgNotFocusedSelected.Visible=.T.
                Endcase
            Endif
            .BackStyle = Iif(Empty(cImage),0,1)
        Endwith
    Endproc

    * Notifica cambio tema
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.imgFocusedSelected.Picture =  .GetProp(OVERFLOWPANELBUTTON_FOCUSEDSELECTED_PICTURE)
            This.imgFocusedNotSelected.Picture = .GetProp(OVERFLOWPANELBUTTON_FOCUSEDNOTSELECTED_PICTURE)
            This.imgNotFocusedSelected.Picture = .GetProp(OVERFLOWPANELBUTTON_NOTFOCUSEDSELECTED_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

    Procedure Click
        This.ChangeSelectedButton()
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.F.)
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.T.)
    Endproc

    Procedure imgPicture.Click
        This.Parent.ChangeSelectedButton()
    Endproc

    Procedure Enabled_Assign
        Lparameters xValue
        This.Enabled = m.xValue
    Endproc

Enddefine

* Fine classe overflowpanelbutton
**************************************************

**************************************************
* Class:	panel

Define Class Panel As Container

    Width = 198
    Height = 32
    BorderWidth = 0

    Name = "panel"

    * Aggiunge pulsante al pannello
    Procedure addbutton
        Lparameters cCaption, cPicture, cOrigPict, bNotSelectable, cCmdType, cCmd, bEnabled
        Local cMacro
        With This
            .Newobject("Button"+Alltrim(Str(.ControlCount + 1)),;
                "PanelButton",,,m.cCaption,m.cPicture, m.bNotSelectable, m.cCmdType, m.cCmd, m.bEnabled)
            cMacro= ".Button" +Alltrim(Str(.ControlCount)) +".cOrigImg= m.cOrigPict"
            &cMacro
        Endwith
    Endproc

    * Notifica cambio tema
    Procedure ChangeTheme

    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

    Protected Procedure Init
        * Cattura l'evento cambio tema
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        * Inizializzo le propriet� degli oggetti in base al tema corrente
        This.ChangeTheme()
    Endproc

Enddefine

* Fine classe: panel
**************************************************


**************************************************
* Class:	panelbutton

Define Class PanelButton As Container


    Width = 198
    Height = 32
    BackStyle = 1
    BorderWidth = 0
    MousePointer = 15
    cOrigImg = ""
    bSelectable = .T.
    Enabled=.T.

    *Colore del testo del pulsante selezionato
    Hidden lblselectedcolor
    lblselectedcolor = .F.
    *Colore del testo del pulsante non selezionato
    Hidden lblnotselectedcolor
    lblnotselectedcolor = .F.

    Hidden cCmdType
    cCmdType = ""
    Hidden cCmd
    cCmd = ""

    Name = "panelbutton"

    *Percorso e nome immagine pulsante attivo e selezionato
    Add Object imgFocusedSelected As Image With ;
        Left = 0, Top = 0, ;
        Width = 198, Height = 32, ;
        MousePointer = 15 ,  Anchor = 10, ;
        Stretch = 2, Visible = .F.

    *Percorso e nome immagine pulsante attivo e non selezionato
    Add Object imgFocusedNotSelected As Image With ;
        Left = 0, Top = 0, ;
        Width = 198, Height = 32, ;
        MousePointer = 15 ,  Anchor = 10, ;
        Stretch = 2, Visible = .F.

    *Percorso e nome immagine pulsante non attivo e selezionato
    Add Object imgNotFocusedSelected As Image With ;
        Left = 0, Top = 0, ;
        Width = 198, Height = 32, ;
        MousePointer = 15 , Anchor = 10, ;
        Stretch = 2, Visible = .F.

    Add Object lblCaption As Label With ;
        FontBold = .T., ;
        Anchor = 10, ;
        BackStyle = 0, ;
        Caption = "", ;
        Height = 17, ;
        Left = 35, ;
        MousePointer = 15, ;
        Top = 7, ;
        Width = 159, ;
        Name = "lblCaption"


    Add Object imgPicture As Image With ;
        BackStyle = 0, ;
        Height = 24, ;
        Left = 4, ;
        MousePointer = 15, ;
        Top = 4, ;
        Width = 24, ;
        Name = "imgPicture"

    Add Object BottomLine As Line With ;
        Height = 0, Width = 198, ;
        left=0, Top = 31, ;
        MousePointer = 15, ;
        visible=.T., Anchor = 10, BorderColor=0


    Protected Procedure Init
        Lparameters cCaption, cPicture, bNotSelectable, cCmdType, cCmd, bEnabled
        *Catturo l'evento cambio tema
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            .Anchor = 0
            .Left = 0
            .Top = ((.TabIndex*(.Height))-.Height)
            .Anchor = 10
            .lblCaption.Caption = cCaption
            .imgPicture.Picture = cPicture
            .bSelectable = !m.bNotSelectable
            .cCmdType = m.cCmdType
            .cCmd = m.cCmd
            .Enabled = m.bEnabled
            .ChangeTheme()
            .Visible = .T.
        Endwith
    Endproc

    * Cambio del pulsante selezionato
    Procedure ChangeSelectedButton
        If This.bSelectable
            This.Parent.Parent.selectedbutton = This.TabIndex
        Else
            This.DoAction()
        Endif
    Endproc

    Procedure AssignCmd
        Lparameters cCmdType, cCmd
        If Vartype(m.cCmdType)<>'C' Or Vartype(m.cCmd)<>'C'
            m.cCmdType=""
            m.cCmd=""
        Endif
        With This
            .cCmdType = m.cCmdType
            .cCmd = m.cCmd
        Endwith
    Endproc

    *Cambio immagine di sfondo del pulsante
    Procedure ChangeBackground
        Lparameters bGotFocus
        #Define nFocused 8
        #Define nNotFocused 16
        #Define nSelected 32
        #Define nNotSelected 64
        Local nState, cImage, nColor
        With This
            nState = (Iif(bGotFocus,nFocused,nNotFocused) + ;
                IIF(.Parent.Parent.selectedbutton==.TabIndex,nSelected,nNotSelected))
            .imgNotFocusedSelected.Visible = .F.
            .imgFocusedNotSelected.Visible = .F.
            If .Enabled
                .imgFocusedSelected.Visible = .F.
                nColor = .lblnotselectedcolor
                Do Case
                    Case nState = (nFocused + nSelected)
                        .imgFocusedSelected.Visible = .T.
                        nColor = .lblselectedcolor
                    Case nState = (nFocused + nNotSelected)
                        .imgFocusedNotSelected.Visible = .T.
                        nColor = iif(i_ThemesManager.GetProp(105)=1, .lblselectedcolor, .lblnotselectedcolor)
                    Case nState = (nNotFocused + nSelected)
                        .imgNotFocusedSelected.Visible = .T.
                        nColor = .lblselectedcolor
                Endcase
                *.BackStyle = Iif(Empty(cImage),0,1)
                *.Picture = cImage
                .lblCaption.ForeColor = nColor
            Endif
        Endwith
    Endproc

    Procedure Enabled_Assign
        Lparameters bEnabled
        This.Enabled = m.bEnabled
        This.lblCaption.ForeColor=Rgb(180,177,163)
    Endproc

    Procedure SetClicked
        Lparameters bEnabled
        With This
            If .Enabled
                .imgFocusedSelected.Visible = m.bEnabled
                .imgNotFocusedSelected.Visible = .F.
                .imgFocusedNotSelected.Visible = !m.bEnabled
            Endif
        Endwith
    Endproc

    Procedure DoAction
        Local cComando, cParametri, cMacro
        With This
            If !Empty(.cCmdType) And !Empty(.cCmd) And .Enabled
                Do Case
                    Case .cCmdType='B'
                        If At('#',.cCmd)>0
                            m.cComando=Left(.cCmd, At('#',.cCmd))
                            m.cParametri=Right(.cCmd, Len(.cCmd)-At('#',.cCmd))
                        Else
                            m.cComando=.cCmd
                            m.cParametri=""
                        Endif
                        m.cMacro=m.cComando + '(this,"' + .cCmdType + Iif(Empty(m.cParametri), '', '", '+m.cParametri) + ')'
                    Case .cCmdType='F'
                        If At('#',.cCmd)>0
                            m.cComando=Left(.cCmd, At('#',.cCmd))
                            m.cParametri=Right(.cCmd, Len(.cCmd)-At('#',.cCmd))
                        Else
                            m.cComando=.cCmd
                            m.cParametri=""
                        Endif
                        m.cMacro=m.cComando + '("' + .cCmdType + Iif(Empty(m.cParametri), '', '", '+m.cParametri) + ')'
                    Otherwise
                        Local subname, cDirectory, cTagValue, l_cClientPane
                        m.cDirectory = Left(.cCmd, 1)
                        m.cTagValue = Substr(.cCmd,3)
                        Do Case
                            Case cDirectory = "1"
                                *--- Lancia la voce di menu
                                m.cMacro="Do mhit In cp_menu With ["+Strtran(Trim(Alltrim(m.cTagValue)),'#',' with ')+"]"
                            Case cDirectory = "2"
                                *--- Aggiorno SubMenuNav
                                With i_oDeskmenu.maINPANE.Panel1.Body
                                    .SubMenuNav.PathCombo.Clear()
                                    .SubMenuNav.SubMenuList.Clear()
                                Endwith
                                Local l_Name, l_Key
                                l_Key = Left(cTagValue, At("#", cTagValue)-1)
                                l_Name = Right(cTagValue, Len(cTagValue)-Rat("#", cTagValue))
                                With i_oDeskmenu.maINPANE.Panel1.Body
                                    .SubMenuNav.aPath.Remove(-1)
                                    .SubMenuNav.FillList(l_Key, l_Name, i_oDeskmenu.cCursor)
                                Endwith
                                m.cMacro = ""
                        Endcase
                        *!*m.cMacro=.cCmd
                Endcase
                &cMacro
            Endif
        Endwith
    Endproc

    *-- Occurs when theme change.
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.imgFocusedSelected.Picture = .GetProp(PANELBUTTON_FOCUSEDSELECTED_PICTURE)
            This.imgFocusedNotSelected.Picture = .GetProp(PANELBUTTON_FOCUSEDNOTSELECTED_PICTURE)
            This.imgNotFocusedSelected.Picture = .GetProp(PANELBUTTON_NOTFOCUSEDSELECTED_PICTURE)
            This.lblselectedcolor = .GetProp(PANELBUTTON_SELECTED_FONTCOLOR)
            This.lblnotselectedcolor = .GetProp(PANELBUTTON_NOTSELECTED_FONTCOLOR)
            This.Picture = .GetProp(PANELBUTTON_NOTFOCUSEDNOTSELECTED_PICTURE)
            This.BottomLine.BorderColor = .GetProp(TITLE_BORDERCOLOR)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

    Procedure Click
        This.ChangeSelectedButton()
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.T.)
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.F.)
    Endproc

    Procedure lblCaption.Click
        This.Parent.ChangeSelectedButton()
        This.Parent.ChangeBackground(.T.)
    Endproc

    Procedure imgPicture.Click
        This.Parent.ChangeSelectedButton()
        This.Parent.ChangeBackground(.T.)
    Endproc

    Procedure imgFocusedSelected.Click
        This.Parent.ChangeSelectedButton()
        This.Parent.ChangeBackground(.T.)
    Endproc

    Procedure imgFocusedNotSelected.Click
        This.Parent.ChangeSelectedButton()
        This.Parent.ChangeBackground(.T.)
    Endproc

    Procedure imgNotFocusedSelected.Click
        This.Parent.ChangeSelectedButton()
        This.Parent.ChangeBackground(.T.)
    Endproc

    Procedure BottomLine.Click
        This.Parent.ChangeSelectedButton()
        This.Parent.ChangeBackground(.T.)
    Endproc

    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.SetClicked(.T.)
    Endproc

    Procedure lblCaption.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.SetClicked(.T.)
    Endproc

    Procedure imgPicture.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.SetClicked(.T.)
    Endproc

    Procedure imgFocusedSelected.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.SetClicked(.T.)
    Endproc

    Procedure imgFocusedNotSelected.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.SetClicked(.T.)
    Endproc

    Procedure imgNotFocusedSelected.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.SetClicked(.T.)
    Endproc

    Procedure BottomLine.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.SetClicked(.T.)
    Endproc

    Procedure MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If !This.bSelectable
            This.SetClicked(.F.)
        Endif
    Endproc

    Procedure lblCaption.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If !This.Parent.bSelectable
            This.Parent.SetClicked(.F.)
        Endif
    Endproc

    Procedure imgPicture.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If !This.Parent.bSelectable
            This.Parent.SetClicked(.F.)
        Endif
    Endproc

    Procedure imgFocusedSelected.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If !This.Parent.bSelectable
            This.Parent.SetClicked(.F.)
        Endif
    Endproc

    Procedure imgFocusedNotSelected.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If !This.Parent.bSelectable
            This.Parent.SetClicked(.F.)
        Endif
    Endproc

    Procedure imgNotFocusedSelected.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If !This.Parent.bSelectable
            This.Parent.SetClicked(.F.)
        Endif
    Endproc

    Procedure BottomLine.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        If !This.Parent.bSelectable
            This.Parent.SetClicked(.F.)
        Endif
    Endproc

Enddefine

* Fine classe: panelbutton
**************************************************


**************************************************
* Class:	panelvertical

Define Class panelvertical As Control

    #Define NAVIGATION_PANE_CAPTION "Desktop men�"
    Width = MIN_BARSIZE - 2
    Height = 331
    BorderWidth = 1
    * Colore di sfondo del pannello selezionato
    Hidden focusedbackcolor
    focusedbackcolor = .F.
    * Colore di sfondo del pannello non selezionato
    Hidden notfocusedbackcolor
    notfocusedbackcolor = .F.
    BackStyle=0
    Name = "panelvertical"

    Add Object lblCaption As Label With ;
        FontBold = .T., ;
        FontSize = 14, ;
        Anchor = 7, ;
        BackStyle = 0, ;
        Caption = NAVIGATION_PANE_CAPTION, ;
        Height = 325, ;
        Left = 3, ;
        Top = 3, ;
        Width = 23, ;
        Rotation = 90, ;
        Name = "lblCaption"

    *Cambio tema
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            *This.Visible = .GetProp(PANELVERTICAL_VISIBLE)
            If This.Visible
                This.focusedbackcolor = 0 &&.GetProp(PANELVERTICAL_FOCUSED_BACKCOLOR)
                This.notfocusedbackcolor = 0 &&.GetProp(PANELVERTICAL_NOTFOCUSED_BACKCOLOR)
                This.lblCaption.ForeColor = .GetProp(PANELVERTICAL_FONTCOLOR)
                This.BackColor = .GetProp(PANELVERTICAL_BACKCOLOR)
            Endif
        Endwith
    Endproc

    Protected Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        *This.BackColor = This.NotFocusedBackcolor
    Endproc

    Protected Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        *This.BackColor = This.FocusedBackcolor
    Endproc

    Protected Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.ChangeTheme()
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine

* fine classe: panelvertical
**************************************************


**************************************************
* Class:	shrinkbutton

Define Class shrinkbutton As Container

    #Define SB_STDWIDTH 18
    #Define SB_STDHEIGHT 18
    Width = SB_STDWIDTH
    Height = SB_STDHEIGHT
    BorderWidth = 0
    BackStyle = 0
    MousePointer = 15

    *-- Path and name of the button's image displayed when button is focused and shrunk.
    Add Object imgfocusedshrunk As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0
    *-- Path and name of the button's image displayed when button is not focused and shrunk.
    Add Object imgnotfocusedshrunk As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0
    *-- Path and name of the button's image displayed when button is focused and not shrunk.
    Add Object imgfocusednotshrunk As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0
    *-- Path and name of the button's image displayed when button is not focused and not shrunk.
    Add Object imgnotfocusednotshrunk As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0
    *-- Specifies if the button was shrunk or not.
    shrunk = .F.

    Name = "shrinkbutton"

    *-- Occurs when theme change.
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.imgfocusedshrunk.Picture = .GetProp(SHRINKBUTTON_FOCUSEDSHRUNK_PICTURE)
            This.imgfocusednotshrunk.Picture = .GetProp(SHRINKBUTTON_FOCUSEDNOTSHRUNK_PICTURE)
            This.imgnotfocusedshrunk.Picture = .GetProp(SHRINKBUTTON_NOTFOCUSEDSHRUNK_PICTURE)
            This.imgnotfocusednotshrunk.Picture = .GetProp(SHRINKBUTTON_NOTFOCUSEDNOTSHRUNK_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    *-- Change button's background image.
    Procedure ChangeBackground
        Lparameters llGotFocus
        #Define lnFocused 8
        #Define lnNotFocused 16
        #Define lnShrunk 32
        #Define lnNotShrunk 64
        Local lnState
        With This
            m.lnState = (Iif(m.llGotFocus,lnFocused,lnNotFocused) + ;
                Iif(.shrunk,lnShrunk,lnNotShrunk))
            .imgfocusedshrunk.Visible=.F.
            .imgfocusednotshrunk.Visible=.F.
            .imgnotfocusedshrunk.Visible=.F.
            .imgnotfocusednotshrunk.Visible=.F.
            Do Case
                Case m.lnState = (lnFocused + lnShrunk)
                    .imgfocusedshrunk.Visible=.T.
                Case m.lnState = (lnFocused + lnNotShrunk)
                    .imgfocusednotshrunk.Visible=.T.
                Case m.lnState = (lnNotFocused + lnShrunk)
                    .imgnotfocusedshrunk.Visible=.T.
                Case m.lnState = (lnNotFocused + lnNotShrunk)
                    .imgnotfocusednotshrunk.Visible=.T.
            Endcase
        Endwith
    Endproc

    Procedure shrunk_assign
        Lparameters vNewVal
        This.shrunk = m.vNewVal
        This.ChangeBackground(.F.)
    Endproc

    Procedure imgfocusedshrunk.Click
        This.Parent.Click()
    Endproc

    Procedure imgfocusednotshrunk.Click
        This.Parent.Click()
    Endproc

    *-- Change button state (shrunk or not).
    Procedure changeviewmode
        This.shrunk = Not This.shrunk
    Endproc

    Procedure ISShrunk
        Return This.shrunk
    Endproc

    Protected Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.ChangeTheme()
    Endproc

    Procedure Click
        With This.Parent
            If !This.shrunk
                .Anchor = 0
                This.Tag = Alltrim(Str(.Width))
                .Width = MIN_BARSIZE
                .Anchor = 7
                *.MainPane.SetAll("Visible",.F.)
                .panelvertical.Visible=.T.
                .maINPANE.Visible=.F.
                .Title.lblCaption_Visible(.F.)
            Else
                .Anchor = 0
                .Width = Int(Val(This.Tag))
                .Anchor = 7
                *.MainPane.SetAll("Visible",.T.)
                .panelvertical.Visible=.F.
                .Title.lblCaption_Visible(.T.)
                .maINPANE.Visible = .T.
            Endif
            .ReArrangeButtons()
            .viewmodechanged(!This.shrunk)
            *--- Evento resize
            .OnResize()
        Endwith
        This.changeviewmode()
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc


    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.T.)
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.F.)
    Endproc

Enddefine
*
*-- EndDefine: shrinkbutton
**************************************************


**************************************************
* Class:	splitterhorizontal

Define Class splitterhorizontal As Container

    Width = 198
    Height = 8
    BorderWidth = 0
    BorderColor = 0
    MousePointer = 7
    Anchor = 0

    *-- Object placed above the splitter.
    objectabove = .F.
    *-- Object placed below the splitter.
    objectbelow = .F.
    *-- Specifies if control was initialised.
    Hidden initialised
    initialised = .F.
    *-- Specifies the minimum height that ObjectAbove can be resized to.
    ObjectAboveMinSize = 32
    *-- Specifies the minimum height that ObjectBelow can be resized to.
    objectbelowminsize = 0
    *-- Minimum mumber of pixels that Splitter will move when mouse moves.
    steps = 32

    Name = "splitterhorizontal"

    Add Object imgBackground As Image With ;
        Top = 1, Left = 1 , ;
        Anchor = 15, ;
        Stretch = 2, ;
        Height = 7, ;
        MousePointer = 7, ;
        Width = 196, ;
        Name = "imgBackground"

    Add Object imgGripper As Image With ;
        Anchor = 768, ;
        BackStyle = 1, ;
        Height = 4, Top = 3, ;
        Left = 81, ;
        MousePointer = 7, ;
        Width = 35, ;
        Name = "imgGripper"

    Add Object TopLine As Line With ;
        Height = 0, Width = 198, ;
        left=0, Top = 0, ;
        MousePointer = 15, ;
        visible=.T., Anchor = 10, BorderColor=0

    Add Object BottomLine As Line With ;
        Height = 0, Width = 198, ;
        left=0, Top = 7, ;
        MousePointer = 15, ;
        visible=.T., Anchor = 10, BorderColor=0

    Protected Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            .initialised = .T.
            .ChangeTheme()
        Endwith
    Endproc

    *-- Evaluate the content of the string passed as a parameter and return the object if the content evaluates to an object.
    Hidden Procedure getreference
        *========================================================
        * Returns an object reference
        *========================================================
        Lparameter toReference
        Do Case
            Case Vartype(m.toReference) == "X"
                Return m.toReference
            Case Vartype(m.toReference) == "O"
                Return m.toReference
            Case Vartype(m.toReference) == "C"
                Return Evaluate(m.toReference)
            Otherwise
                Error 107
        Endcase
    Endproc

    Procedure Width_assign
        Lparameters nNewValue
        With This
            .imgBackground.Anchor=0
            .Width=nNewValue
            .imgBackground.Width=.Width
            .imgGripper.Left=.Width/2-.imgGripper.Width/2
            .imgBackground.Anchor=15
        Endwith
    Endproc

    *-- Occurs when theme change.
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.imgBackground.Picture = .GetProp(SPLITTER_BACKGROUND_PICTURE)
            This.imgGripper.Picture = .GetProp(SPLITTER_GRIPPER_PICTURE)
            This.TopLine.BorderColor = .GetProp(CONTAINER_BORDERCOLOR)
            This.BottomLine.BorderColor = .GetProp(CONTAINER_BORDERCOLOR)
        Endwith
    Endproc


    Hidden Procedure objectabove_access
        Return This.getreference(This.objectabove)
    Endproc


    Hidden Procedure objectbelow_access
        Return This.getreference(This.objectbelow)
    Endproc


    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
        With This
            .objectabove = Null
            .objectbelow = Null
        Endwith
    Endproc

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord

        If nButton == 1
            With This
                Local lnTop, nStep
                lnTop = Min(Max(nYCoord,.objectabove.Top+.ObjectAboveMinSize-Iif(.steps==1,0,.steps)),;
                    .objectbelow.Top+.objectbelow.Height-.objectbelowminsize-.Height)
                With .objectbelow
                    nStep = Mod(Max(.Height+.Top-lnTop-This.Height,0),This.steps)
                Endwith
                .Anchor = 0
                If nStep<5 And .Parent.ResizeHideButtons=0
                    .Top = lnTop+nStep
                Endif
                .Anchor = 14
            Endwith
        Endif
    Endproc


    Procedure Moved
        With This
            If .initialised
                With .objectabove
                    .Anchor = 0
                    If This.Top-.Top>0
                        .Move(.Left,.Top,.Width,Max(This.Top-.Top,0))
                    Endif
                    .Anchor = 11
                Endwith
                With .objectbelow
                    .Anchor = 0
                    .Move(.Left,This.Top+This.Height,.Width,Max(.Height+.Top-This.Top-This.Height,0))
                    .Anchor = 14
                Endwith
                .splittermoved()
            Endif
        Endwith
    Endproc


    *-- Occurs after splitter is moved.
    Procedure splittermoved
    Endproc


    Procedure imgBackground.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseMove(nButton, nShift, nXCoord, nYCoord)
    Endproc


    Procedure imgGripper.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseMove(nButton, nShift, nXCoord, nYCoord)
    Endproc


Enddefine
*
*-- EndDefine: splitterhorizontal
**************************************************

**************************************************
* Class:	title

Define Class Title As Container


    Anchor = 11
    Width = 200
    Height = 28
    BackStyle = 0
    *-- Specifies the text displayed in an object's caption.
    Caption = .F.

    Name = "title"

    Add Object imgbackgroundFocused As Image With ;
        Anchor = 15, ;
        Stretch = 2, ;
        Height = 26, ;
        Left = 1, ;
        Top = 1, ;
        Width = 198, ;
        Visible = .F., ;
        Name = "imgBackgroundFocused"

    Add Object imgbackgroundNotFocused As Image With ;
        Anchor = 15, ;
        Stretch = 2, ;
        Height = 26, ;
        Left = 1, ;
        Top = 1, ;
        Width = 198, ;
        Visible = .T., ;
        Name = "imgBackgroundNotFocused"

    Add Object lblCaption As Label With ;
        FontBold = .T., ;
        FontSize = 12, ;
        Anchor = 11, ;
        BackStyle = 0, ;
        Caption = "", ;
        Height = 21, ;
        Left = 7, ;
        Top = 3, ;
        Width = 165, ;
        Name = "lblCaption"

    *--- Doppio click sul titolo
    Procedure lblCaption.DblClick()
        This.Parent.Parent.DblClick()
    Endproc

    Procedure imgbackgroundNotFocused.DblClick()
        This.Parent.Parent.DblClick()
    Endproc

    Procedure imgbackgroundFocused.DblClick()
        This.Parent.Parent.DblClick()
    Endproc

    *-- Occurs when theme change.
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.BorderColor = .GetProp(TITLE_BORDERCOLOR)
            This.imgbackgroundNotFocused.Left = .GetProp(TITLE_BACKGROUND_LEFT)
            This.imgbackgroundNotFocused.Width = This.Width - This.imgbackgroundNotFocused.Left - 1
            This.imgbackgroundNotFocused.Picture = .GetProp(TITLE_BACKGROUND_PICTURE)
            This.imgbackgroundNotFocused.Anchor = 0
            This.imgbackgroundNotFocused.Anchor = 15
            This.lblCaption.ForeColor = .GetProp(TITLE_FONTCOLOR)
        Endwith
    Endproc

    Proc lblCaption_Visible
        Lparameters bVisible
        This.lblCaption.Visible=bVisible
    Endproc

    Hidden Procedure caption_assign
        Lparameters vNewVal
        With This
            Store m.vNewVal To .Caption, .lblCaption.Caption
        Endwith
    Endproc


    Protected Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.ChangeTheme()
    Endproc


    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc


Enddefine
*
*-- EndDefine: title
**************************************************

**************************************************
* Class:	splittervertical

Define Class splittervertical As Container


    Height = 203
    Width = 7
    MousePointer = 9
    SpecialEffect = 0
    BorderWidth=0
    *BorderStyle=1
    BackStyle = 1
    *-- The splitter can be dragged to the left side down to this value. Pixels from the left side of the parent. Value < 1 is percent of parent width.
    rangemin = 0.2
    *-- The splitter can be dragged to the right side up to this value. Pixels from the left side of the parent. Value < 1 is percent of parent width.
    rangemax = 0.8
    Name = "splittervertical"

    *-- Array of controls
    Hidden hiddencontrols[1,1]
    Hidden DragStatus
    DragStatus = .F.

    Add Object imgBackground As Image With ;
        Anchor = 13, ;
        Stretch = 2, ;
        Height = 203, ;
        Left = 1,;
        MousePointer = 9, ;
        Width = 6, ;
        Name = "imgBackground",;
        RotateFlip = 0

    Add Object imgGripper As Image With ;
        Anchor = 768, ;
        BackStyle = 1, ;
        Height = 35, ;
        Left = 2, ;
        Top = 83,;
        MousePointer = 9, ;
        Width = 5, ;
        Name = "imgGripper"

    Add Object RightLine As Line With ;
        Top = 0, Left = 6, Width = 0, ;
        Height = 203, Anchor = 5

    Protected Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        * API FUNCTIONS - declare only one for all splitter with this class
        This.ChangeTheme()
        If Vartype(_Screen.___SplitterApi)="O"
            _Screen.___SplitterApi.nInstances = _Screen.___SplitterApi.nInstances + 1
        Else
            _Screen.AddObject("___SplitterApi","SplitterAPI")
        Endif
        Return Vartype(_Screen.___SplitterApi)="O"
    Endproc

    *-- Occurs when theme change.
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.imgBackground.Picture = .GetProp(VERTICAL_SPLITTER_BACKGROUND_PICTURE)
            This.imgGripper.Picture = .GetProp(VERTICAL_SPLITTER_GRIPPER_PICTURE)
            This.BorderColor = .GetProp(CONTAINER_BORDERCOLOR)
            This.RightLine.BorderColor = .GetProp(CONTAINER_BORDERCOLOR)
        Endwith
    Endproc

    *-- internal use
    Hidden Procedure showcontrols
        * show temporary hidden objects and clear list (collection)
        Lparameters toRoot

        If Alen(This.hiddencontrols,1)<2
            Return
        Endif
        For i=2 To Alen(This.hiddencontrols,1)
            If Type("THIS.hiddencontrols[i]")="O"
                This.hiddencontrols[i].Visible=.T.
            Endif
        Endfor

        Dimension This.hiddencontrols[1]
    Endproc


    *-- This method is called at the end of a split operation
    Procedure Split
        *default behaviour
    Endproc

    Procedure imgBackground.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseDown(nButton, nShift, nXCoord, nYCoord)
    Endproc


    Procedure imgGripper.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseDown(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure imgGripper.DblClick
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Parent.shrinkbutton.Click()
    Endproc

    Procedure imgBackground.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseMove(nButton, nShift, nXCoord, nYCoord)
    Endproc


    Procedure imgGripper.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseMove(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Procedure imgBackground.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseUp(nButton, nShift, nXCoord, nYCoord)
    Endproc


    Procedure imgGripper.MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.MouseUp(nButton, nShift, nXCoord, nYCoord)
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.DragStatus = nButton=1
    Endproc

    Procedure MouseUp
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.DragStatus = .F.
    Endproc

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        Local lcWindowName,lnScaleMode,lnMinCol,lnMaxCol,lnMCol1,lnMCol2
        Local lnColumns,lnLeft,lnOldLeft,lnMin,oldMCol2
        Local llLockScreen,lnMousePointer
        Local lhDC,lhMemDC,lhMemBmp,lHWnd,lnBmpWidth,nLeftOffset,nTopOffset,oContainer,xWidth
        Local lcOldFormName,lhMemSplit, bDragStarted, oPadre
        bDragStarted =.F.
        If !This.DragStatus
            Return
        Endif
        m.oPadre = This.Parent.Parent
        lcWindowName= m.oPadre.Name
        lnScaleMode=m.oPadre.ScaleMode
        m.oPadre.ScaleMode=3  && pixels

        oContainer=m.oPadre
        nLeftOffset=Objtoclient(This,2)-This.Left
        nTopOffset=Objtoclient(This,1)-This.Top

        lnMCol1=Mcol(lcWindowName,3)
        If Type("lnMCol1")#"N" Or lnMCol1<=0
            m.oPadre.ScaleMode=lnScaleMode
            Return
        Endif

        lnMCol1=lnMCol1-nLeftOffset
        If lnMCol1 <> This.Left+1
            lnMCol1=This.Left+1
            Mouse At Mrow(lcWindowName,3), lnMCol1+nLeftOffset Pixels Window (lcWindowName)
        Endif

        * set some vars
        llLockScreen			=	_Screen.LockScreen
        lnMousePointer			=	_Screen.MousePointer
        _Screen.MousePointer	=	9
        lnLeft					=	This.Left

        * check two parent level for width - suppose that parent form always present
        If Pemstatus(oContainer,"Width",5)
            xWidth	=	oContainer.Width
        Else
            If Pemstatus(oContainer.Parent,"PageWidth",5)
                xWidth	=	oContainer.Parent.PageWidth
            Else
                * if error that oContainer.width and oContainer.parent.width not exist, something wrong
                xWidth	=	oContainer.Parent.Width
            Endif
        Endif

        * RangeMin (RangeMax) < 1
        * 		RangeMin (RangeMax) are used as coeficient (%/100)
        * RangeMin (RangeMax) > 1
        *		RangeMin (RangeMax) are used as absolute offset in pixels
        * RangeMin (RangeMax) =0
        *		RangeMin (RangeMax) are ignored - let 2 times this.width
        Do Case
            Case This.rangemin <= 0
                lnMinCol=This.Width*2
            Case This.rangemin > 1
                lnMinCol=Max(This.Width*2,Int(This.rangemin))
            Case This.rangemin < 1
                lnMinCol=Max(This.Width*2,Int(This.rangemin*xWidth))
        Endcase

        Do Case
            Case This.rangemax <= 0
                lnMaxCol=xWidth-(This.Width*3)
            Case This.rangemax > 1
                lnMaxCol=Min(xWidth-(This.Width*3),This.rangemax)
            Case This.rangemax < 1
                lnMaxCol=Min(xWidth-(This.Width*3),This.rangemax*xWidth)
        Endcase

        If lnMinCol>lnMaxCol
            * nothing to move!!!
            m.oPadre.ScaleMode=lnScaleMode
            Return
        Endif

        lnMCol2=lnMCol1
        oldMCol2=lnMCol2

        Local x,Y
        * API CALLS
        If m.oPadre.ShowWindow= 2
            * workaround, when showwindow=2 the handle is not the right one...
            * worst if you have a toolbar!
            *#define GW_HWNDFIRST        0
            #Define GW_HWNDLAST         1
            *#define GW_HWNDNEXT         2
            *#define GW_HWNDPREV         3
            *#define GW_OWNER            4
            #Define GW_CHILD            5

            lHWnd=m.oPadre.HWnd
            lHWnd=GS_SplitGetWindow(lHWnd,GW_CHILD)
            lHWnd=GS_SplitGetWindow(lHWnd,GW_HWNDLAST)
            m.x=0
            m.y=0
        Else
            lHWnd=0
            m.x=m.oPadre.Left
            m.y=m.oPadre.Top
        Endif
        m.lhDC = GS_SplitGetDC(m.lHWnd)
        m.lhMemDC = GS_SplitCreateCompatibleDC(m.lhDC)
        * Take a copy of the portion of the form that can be dragged over
        m.lnBmpWidth=This.Width-2
        m.lhMemBmp = GS_SplitCreateCompatibleBitmap(m.lhDC, m.lnBmpWidth, This.Height)
        *lhMemSplit = GS_SplitCreateCompatibleBitmap(m.lhDC, m.lnBmpWidth, THIS.HEIGHT)
        = GS_SplitSelectObject(m.lhMemDC , m.lhMemBmp)
        = GS_SplitBitBlt(m.lhMemDC, 0, 0, m.lnBmpWidth, This.Height, ;
            M.lhDC, m.x+m.lnMCol1+m.nLeftOffset-2, m.y+This.Top+m.nTopOffset, SRCCOPY)
        *= GS_SplitSelectObject(lhMemDC , lhMemSplit)
        *= GS_SplitBitBlt(lhMemDC, 0, 0, lnBmpWidth, THIS.HEIGHT, ;
        *	lhDC, lnMCol1+nLeftOffset-1, THIS.TOP+nTopOffset, SRCCOPY)

        * Stop fox drawing in the screen
        _Screen.LockScreen=.T.

        * update the display while dragging
        Do While Mdown()
            _Screen.MousePointer	=	9
            DoEvents
            lnMCol2=Mcol(lcWindowName,3)-nLeftOffset
            If Type("lnMCol2")#"N" Or lnMCol2=0
                Loop
            Endif
            If lnMCol2<=lnMinCol
                *force the mouse to stay at this position
                Mouse At Mrow(m.lcWindowName,3), m.lnMinCol+m.nLeftOffset Pixels Window (m.lcWindowName)
                lnMCol2=lnMinCol+1
            Endif
            If lnMCol2>=(lnMaxCol-This.Width)
                *force the mouse to stay at this position
                Mouse At Mrow(m.lcWindowName,3), m.lnMaxCol-This.Width+m.nLeftOffset+1 Pixels Window (m.lcWindowName)
                m.lnMCol2=m.lnMaxCol-This.Width
            Endif
            m.lnMCol2=Min(Max(m.lnMCol2,m.lnMinCol),m.lnMaxCol)
            If m.oldMCol2=m.lnMCol2
                Loop
            Else
                * on mouse move, redraw a part of the screen from the memory copy
                * and draw "this" image at the mouse position
                * bitblt (dest...source...)
                If Abs(m.lnMCol2-m.lnMCol1)>m.lnBmpWidth/2 Or m.bDragStarted
                    m.bDragStarted = .T.
                    With This
                        .Left=m.lnLeft+(m.lnMCol2-m.lnMCol1)
                        *restore
                        = GS_SplitSelectObject(m.lhMemDC , m.lhMemBmp)
                        = GS_SplitBitBlt(m.lhDC, m.x+m.oldMCol2+m.nLeftOffset-2, m.y+.Top+nTopOffset, m.lnBmpWidth, .Height,;
                            M.lhMemDC, 0, 0,  SRCCOPY)
                        *take a new copy
                        = GS_SplitBitBlt(m.lhMemDC, 0, 0, m.lnBmpWidth, This.Height, ;
                            M.lhDC, m.x+m.lnMCol2+m.nLeftOffset-2, m.y+This.Top+m.nTopOffset, SRCCOPY)
                        *draw the bar
                        *= GS_SplitSelectObject(lhMemDC , lhMemSplit)
                        *= GS_SplitBitBlt(lhDC, .LEFT+nLeftOffset-1, .TOP+nTopOffset, lnBmpWidth, .HEIGHT,;
                        *	lhMemDC, 0, 0,  SRCCOPY)
                        = GS_patblt(m.lhDC, m.x+.Left+m.nLeftOffset-1, m.y+.Top+m.nTopOffset,  m.lnBmpWidth, .Height, PATINVERT)
                    Endwith
                    m.oldMCol2=m.lnMCol2
                Endif
            Endif
        Enddo
        With This
            *restore
            = GS_SplitSelectObject(m.lhMemDC , m.lhMemBmp)
            = GS_SplitBitBlt(m.lhDC, m.x+m.oldMCol2+m.nLeftOffset-2, m.y+.Top+nTopOffset, m.lnBmpWidth, .Height,;
                M.lhMemDC, 0, 0,  SRCCOPY)
        Endwith
        If m.lnMCol2<0
            m.lnMCol2=m.lnMCol1
        Endif
        m.lnColumns=m.lnMCol2-m.lnMCol1
        If m.bDragStarted
            This.Left=m.lnLeft + m.lnColumns + m.lnBmpWidth
        Endif
        m.oPadre.ScaleMode=m.lnScaleMode
        m.oPadre.MousePointer=m.lnMousePointer
        If m.bDragStarted
            This.Split()
        Endif
        _Screen.LockScreen=m.llLockScreen

        * free the memory
        = GS_SplitReleaseDC(m.lHWnd, m.lhDC)
        = GS_SplitDeleteObject(m.lhMemBmp)
        = GS_SplitDeleteObject(m.lhMemSplit)
        = GS_SplitDeleteDC(m.lhMemDC)
    Endproc

    Procedure Split
        Local nOldAnchor
        With This
            If .Left-.Width-1<=.rangemin
                If ! .Parent.shrinkbutton.ISShrunk()
                    .Parent.shrinkbutton.Click()
                Endif
            Else
                If .Parent.shrinkbutton.ISShrunk()
                    .Parent.shrinkbutton.Tag = Alltrim(Str(.Left))
                    .Parent.shrinkbutton.Click()
                Endif
                m.nOldAnchor = .Parent.Anchor
                .Parent.Anchor = 0
                .Parent.Width = .Left
                .Parent.Anchor = m.nOldAnchor
                .Parent.ReArrangeButtons()
                *--- Evento resize
                .Parent.OnResize()
            Endif
        Endwith
    Endproc


    Procedure Destroy
        * decrement instance counter, if 0 release object (this.release Dlls)
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
        If Type("_screen.___SplitterApi")="O" And !Isnull(_Screen.___SplitterApi)
            _Screen.___SplitterApi.nInstances = _Screen.___SplitterApi.nInstances - 1
            If _Screen.___SplitterApi.nInstances <= 0
                _Screen.___SplitterApi.nInstances = .Null.
                _Screen.RemoveObject("___SplitterApi")
            Endif
        Endif
    Endproc


    Procedure Error
        Lparameters nError, cMethod, nLine

        If nError = 1 && file xxx do not exist
            * re-declare the dlls if this is the problem
            cMess = Upper(Message( ))
            If At("GS_SPLIT",cMess) > 0

                If !Vartype(_Screen.___SplitterApi)="O"
                    _Screen.AddObject("___SplitterApi","SplitterAPI")
                Else
                    _Screen.___SplitterApi.Init()
                Endif

                Retry
            Endif
        Endif

        ** default error behaviour ...
        * Display information about other errors.
*        cMsg="Error:" + Alltrim(Str(nError)) + Chr(13) ;
*           + Message()+Chr(13)+"Program:"+Program()

*        nAnswer = Messagebox(cMsg, 2+48+512, "Error")
*       Do Case
*            Case nAnswer = 3   &&Abort
*                Cancel
*            Case nAnswer = 4   &&Retry
*                Retry
*           Otherwise       && Ignore
*                Return
*        Endcase
    Endproc


Enddefine
*
*-- EndDefine: splittervertical
**************************************************


**************************************************
* Class:	splitterapi

Define Class splitterapi As Custom


    Height = 15
    Width = 27
    nInstances = 1
    Name = "splitterapi"


    Procedure Init
        Declare Long GetDC					In Win32API As GS_SplitGetDC Long HWnd
        Declare Long CreateCompatibleDC		In Win32API As GS_SplitCreateCompatibleDC Long hdc
        Declare Long CreateCompatibleBitmap In Win32API As GS_SplitCreateCompatibleBitmap  Long hdc, Long nWidth, Long nHeight
        Declare Long SelectObject 			In Win32API As GS_SplitSelectObject  Long hdc, Long hObject
        Declare Long ReleaseDC 				In Win32API As GS_SplitReleaseDC  Long HWnd, Long hdc
        Declare Long DeleteDC 				In Win32API As GS_SplitDeleteDC  Long hdc
        Declare Long BitBlt 				In Win32API As GS_SplitBitBlt  ;
            Long hDestDC, Long x, Long Y, Long nWidth, Long nHeight, ;
            Long hSrcDC, Long xSrc, Long ySrc, Long dwRop
        Declare Integer DeleteObject 		In Win32API As GS_SplitDeleteObject  Long hObject
        Declare Integer GetWindow 			In user32 As GS_SplitGetWindow  ;
            INTEGER HWnd,;
            INTEGER wFlag
        Declare Integer PatBlt In  ;
            Win32API As GS_PatBlt  ;
            INTEGER, Integer, Integer,  ;
            INTEGER, Integer,  ;
            INTEGER
    Endproc


    Procedure Destroy
        Clear Dlls	;
            "GS_SplitGetDC",;
            "GS_SplitCreateCompatibleDC",;
            "GS_SplitCreateCompatibleBitmap",;
            "GS_SplitSelectObject",;
            "GS_SplitReleaseDC",;
            "GS_SplitDeleteDC",;
            "GS_SplitBitBlt",;
            "GS_SplitGetWindow",;
            "GS_SplitDeleteObject",;
            "GS_PatBlt"
    Endproc


Enddefine

* Fine classe: splitterapi
**************************************************

**************************************************
* Class:	PanelManager

Define Class PanelManager As Container
    Top = 28
    Left = 1
    Width = 198
    Height = 332
    BorderWidth=0
    PanelVerticalSpace = 0
    PanelLeftSpace = 0
    PanelRightSpace = 0
    Hidden bReorging
    bReorging = .F.
    Hidden nNumPanels
    nNumPanels = 0
    BottomMostTitle = 0

    Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1

    Procedure addpanel
        Lparameters cClass, cCaption, nIndex, bVisible, bShrunk, cImage, cIcon
        Local cMacro
        _Screen.LockScreen=.T.
        If Vartype(nIndex)<>'N' Or nIndex<=0
            nIndex = This.nNumPanels
        Endif
        This.ReorgIndex(nIndex)
        With This
            .nNumPanels = .nNumPanels + 1
            m.cMacro="Panel"+Alltrim(Str(.nNumPanels))
            .Newobject(m.cMacro, cClass,,, .PanelLeftSpace, .PanelRightSpace, cCaption, nIndex, bVisible, bShrunk, cImage, cIcon)
            m.cMacro="this." + m.cMacro
            .ReorgPanels()
        Endwith
        _Screen.LockScreen=.F.
        Return &cMacro
    Endproc

    Procedure ReorgIndex
        Lparameters NewIndex
        Local PanelIndex
        For PanelIndex=1 To This.ControlCount
            If "PANEL"$Upper(This.Controls[PanelIndex].Name)
                If This.Controls[PanelIndex].Index=NewIndex
                    NewIndex = This.Controls[PanelIndex].Index
                    This.Controls[PanelIndex].Index= This.Controls[PanelIndex].Index + 1
                Endif
            Endif
        Next
    Endproc

    Procedure MovePanels
        Lparameters nDest, nOrig
        If nDest<nOrig
            This.MoveDownPanels(nDest, nOrig)
        Else
            This.MoveUpPanels(nDest, nOrig)
        Endif
    Endproc

    Procedure MoveDownPanels
        Lparameters nStartIndex, nSkipIndex
        Local PanelIndex
        For PanelIndex=1 To This.ControlCount
            If "PANEL"$Upper(This.Controls[PanelIndex].Name)
                If This.Controls[PanelIndex].Index=nSkipIndex
                    This.Controls[PanelIndex].Index=nStartIndex
                Else
                    If This.Controls[PanelIndex].Index>=nStartIndex And This.Controls[PanelIndex].Index<nSkipIndex
                        This.Controls[PanelIndex].Index=This.Controls[PanelIndex].Index+1
                    Endif
                Endif
            Endif
        Next
        This.ReorgPanels()
    Endproc

    Procedure MoveUpPanels
        Lparameters nStartIndex, nSkipIndex
        Local PanelIndex
        For PanelIndex=This.ControlCount To 1 Step -1
            If "PANEL"$Upper(This.Controls[PanelIndex].Name)
                If This.Controls[PanelIndex].Index=nSkipIndex
                    This.Controls[PanelIndex].Index=nStartIndex
                Else
                    If This.Controls[PanelIndex].Index<=nStartIndex And This.Controls[PanelIndex].Index>nSkipIndex
                        This.Controls[PanelIndex].Index=This.Controls[PanelIndex].Index-1
                    Endif
                Endif
            Endif
        Next
        This.ReorgPanels()
    Endproc

    Procedure ReorgPanels
        Local TopAtt, PanelIndex, nIndexAtt, bPanelOK, oCol
        If This.bReorging
            Return
        Endif
        This.bReorging=.T.
        TopAtt = 0
        nIndexAtt = 1
        #If Version(5)>=900
            oCol=Newobject("Collection")
        #Else
            oCol=Newobject("Cp_Collection")
        #Endif

        For Each oPanels In This.Controls
            If "PANEL"$Upper(oPanels.Name)
                oCol.Add(oPanels, Right('00'+Alltrim(Str(oPanels.Index)),2))
            Endif
        Next
        oCol.KeySort=2

        For Each oItem In oCol
            With oItem
                .Top=TopAtt
                If !.Index=oCol.Count
                    .Height = .OriginalHeight
                Endif
                This.BottomMostTitle = TopAtt + .TitleBar.Height + This.PanelVerticalSpace
                If .TitleBar.shrinkbutton.ISShrunk()
                    TopAtt=TopAtt + .TitleBar.Height
                Else
                    TopAtt=TopAtt + .Body.Height
                Endif
                TopAtt = TopAtt + This.PanelVerticalSpace

            Endwith
        Next
        oCol = .Null.
        This.bReorging=.F.
        This.Resize()
    Endproc

    Procedure Resize
        If !This.bReorging
            For Each oPanels In This.Controls
                If Left(Upper(oPanels.Name),5)=="PANEL" And oPanels.Index = This.nNumPanels
                    With oPanels
                        If .Parent.Height - .Top - This.PanelVerticalSpace > .OriginalHeight
                            .Height = .Parent.Height - .Top - This.PanelVerticalSpace
                        Endif
                    Endwith
                    Exit
                Endif
            Next
        Endif
    Endproc

    Procedure ChangeSettings
        If !This.bReorging
            For Each oPanels In This.Controls
                If Upper(oPanels.Name)="PANEL"
                    With oPanels
                        .ChangeSettings()
                    Endwith
                Endif
            Next
        Endif
    Endproc

Enddefine
* Fine classe: PanelManager
**************************************************

**************************************************
* Class:	StandardPanel

Define Class StandardPanel As Container

    Caption = ""
    Index = 0
    Visible = .T.
    Movable = .T.
    shrunk = .F.
    cPicture = ""
    Left = 2
    Top = 2
    Width = 200
    Height = 100
    BorderWidth = 0
    BackStyle = 0
    OriginalHeight = 100

    Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1

    Add Object Body As StandardPanelBody With ;
        Height = 100

    Add Object TitleBar As StandardPanelTitle With ;
        visible=.F.

    Protected Procedure Init
        Lparameters nLeft, nRight, cCaption, nIndex, bVisible, bShrunk, cImage, cIcon
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        _Screen.LockScreen = .T.
        With This
            .Index = nIndex
            .Caption = cCaption
            .Visible = bVisible
            .Left = m.nLeft
            .Width = .Parent.Width - m.nRight - m.nLeft - 5
            .TitleBar.Width = .Width
            .Body.Anchor = 0
            .Body.Width = .Width
            .Body.Top=.TitleBar.Height
            If Vartype(cImage)=='C' And !Empty(cImage)
                .TitleBar.cOrigImg=cImage
            Endif
            If Vartype(cIcon)=='C' And !Empty(cIcon)
                .TitleBar.cOrigImg=cIcon
            Endif
            .TitleBar.Visible = bVisible
            .TitleBar.Anchor = 10
            .Body.Visible = bVisible
            .Body.Anchor = 15
            .Anchor = 10
        Endwith
        This.ChangeTheme()
        _Screen.LockScreen = .F.
    Endproc

    Procedure shrunk_assign
        Lparameters bNewVal
        With This
            .shrunk = bNewVal
            .Body.Visible=!bNewVal
            If bNewVal
                .Height = .TitleBar.Height
            Else
                .Height = Max(.Body.Height, .OriginalHeight)
            Endif
            .Parent.ReorgPanels()
        Endwith
    Endproc

    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.BorderColor = .GetProp(TITLE_BORDERCOLOR)
            This.BackColor = .GetProp(CONTAINER_BACKCOLOR)
        Endwith
    Endproc

    Procedure top_assign
        Lparameters nValue
        With This
            .Top=nValue
            .Parent.ReorgPanels()
        Endwith
    Endproc

    Procedure caption_assign
        Lparameters vNewVal
        With This
            Store m.vNewVal To .Caption, .TitleBar.Caption
        Endwith
    Endproc

    Procedure Visible_assign
        Lparameters vNewVal
        With This
            Store m.vNewVal To .Visible , .Body.Visible, .TitleBar.Visible
        Endwith
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

    Procedure ChangeSettings
        This.Body.ChangeSettings()
    Endproc

Enddefine
* Fine classe: StandardPanel
**************************************************

**************************************************
* Class:	StandardPanelTitle

#Define STANDARD_PANEL_TITLE_HEIGHT 18

Define Class StandardPanelTitle As Container
    Height = STANDARD_PANEL_TITLE_HEIGHT
    BorderWidth=0
    Caption=""
    cOrigImg = ""

    Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1

    Add Object Title As Title With ;
        Top = 0, Left = 0,;
        Height = STANDARD_PANEL_TITLE_HEIGHT, ;
        BorderWidth = 0, ;
        lblCaption.FontSize = 8, ;
        lblCaption.MousePointer = 15, ;
        imgbackgroundNotFocused.MousePointer = 15, ;
        imgbackgroundFocused.MousePointer = 15

    Add Object TitleImg As Image With ;
        Top = 0,  Left= 4, Width=16, Height=16, ;
        Name = "TitleImg", Visible = .F., MousePointer = 15


    Add Object shrinkbutton As VerticalShrinkButton With ;
        Anchor = 9, ;
        Top = 1, ;
        Left = 1, ;
        width = SB_STDWIDTH, Height = SB_STDHEIGHT , ;
        backstyle = 0, ;
        Name = "ShrinkButton", ;
        Visible = .T.

    Add Object BottomLine As Line With ;
        Height = 0, Width = 198, ;
        left=0, Top = 17, ;
        MousePointer = 15, ;
        visible=.T., Anchor = 10, BorderColor=0

    Add Object TopLine As Line With ;
        Height = 0, Width = 198, ;
        left=0, Top = 0, ;
        MousePointer = 15, ;
        visible=.T., Anchor = 10, BorderColor=0

    Add Object LeftLine As Line With ;
        Height = STANDARD_PANEL_TITLE_HEIGHT, ;
        Width = 0, Left=0, Top = 0, ;
        MousePointer = 15, ;
        visible=.T., Anchor = 7, BorderColor=0

    Protected Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        With This
            .Anchor=0
            .TitleImg.Top = (.Height - .TitleImg.Height) / 2
            .Anchor=15
            .Title.Anchor = 0
            .Title.Width= .Width
            .Title.lblCaption.Width=.Width
            .Title.lblCaption.Anchor = 15
            .Title.Anchor = 15
            .shrinkbutton.Anchor = 0
            .shrinkbutton.Left = .Width - .shrinkbutton.Width - 2
            .shrinkbutton.Anchor = 9
        Endwith
        This.ChangeTheme()
    Endproc

    Procedure Title.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ChangeBackground(.T.)
    Endproc

    Procedure Title.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ChangeBackground(.F.)
    Endproc

    Procedure Title.lblCaption.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Parent.ChangeBackground(.T.)
    Endproc

    Procedure Title.lblCaption.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Parent.ChangeBackground(.F.)
    Endproc

    Procedure Title.lblCaption.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton = 1 And nShift=1
            This.Parent.Parent.Parent.Drag(1)
        Endif
    Endproc

    Procedure Title.lblCaption.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        Local nIndexTmp
        With This
            If Vartype(oSource)='O' And oSource.Name<>.Parent.Parent.Name
                .Parent.Parent.Parent.Parent.MovePanels(.Parent.Parent.Parent.Index,oSource.Index)
            Endif
        Endwith
    Endproc

    Procedure shrinkbutton.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ChangeBackground(.T.)
    Endproc

    Procedure shrinkbutton.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ChangeBackground(.F.)
    Endproc

    Procedure shrinkbutton.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton = 1 And nShift=1
            This.Parent.Parent.Drag(1)
        Endif
    Endproc

    Procedure shrinkbutton.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        Local nIndexTmp
        With This
            If Vartype(oSource)='O' And oSource.Name<>.Parent.Name
                .Parent.Parent.Parent.MovePanels(.Parent.Parent.Index,oSource.Index)
            Endif
        Endwith
    Endproc

    Procedure TitleImg.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ChangeBackground(.T.)
    Endproc

    Procedure TitleImg.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ChangeBackground(.F.)
    Endproc

    Procedure TitleImg.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton = 1 And nShift=1
            This.Parent.Parent.Drag(1)
        Endif
    Endproc

    Procedure TitleImg.DragDrop
        Lparameters oSource, nXCoord, nYCoord
        Local nIndexTmp
        With This
            If Vartype(oSource)='O' And oSource.Name<>.Parent.Name
                .Parent.Parent.Parent.MovePanels(.Parent.Parent.Index,oSource.Index)
            Endif
        Endwith
    Endproc

    Procedure Title.lblCaption.Click
        This.Parent.Parent.shrinkbutton.changeviewmode()
    Endproc

    Procedure TitleImg.Click
        This.Parent.shrinkbutton.changeviewmode()
    Endproc

    Procedure Title.imgbackgroundNotFocused.Click
        This.Parent.Parent.shrinkbutton.changeviewmode()
    Endproc

    Procedure Title.imgbackgroundFocused.Click
        This.Parent.Parent.shrinkbutton.changeviewmode()
    Endproc

    Procedure shrinkbutton.ImgShrunk.Click
        This.Parent.changeviewmode()
    Endproc

    Procedure shrinkbutton.ImgNotShrunk.Click
        This.Parent.changeviewmode()
    Endproc

    Procedure ChangeBackground
        Lparameters bGotFocus
        With This
            If bGotFocus
                .Title.imgbackgroundFocused.Visible=.T.
                .Title.imgbackgroundNotFocused.Visible=.F.
            Else
                .Title.imgbackgroundFocused.Visible=.F.
                .Title.imgbackgroundNotFocused.Visible=.T.
            Endif
        Endwith
    Endproc

    *-- Occurs when theme change.
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.BorderColor = .GetProp(TITLE_BORDERCOLOR)
            This.Title.imgbackgroundFocused.Picture = .GetProp(PANELTITLE_FOCUSEDBACKGROUND_PICTURE)
            This.Title.imgbackgroundNotFocused.Picture = .GetProp(PANELTITLE_NOTFOCUSEDBACKGROUND_PICTURE)
            This.Title.lblCaption.ForeColor = .GetProp(PANELTITLE_FONTCOLOR)
            This.BottomLine.BorderColor = .GetProp(PANELTITLE_BOTTOMLINE_COLOR)
            This.TopLine.BorderColor = .GetProp(PANELTITLE_TOPLINE_COLOR)
            This.LeftLine.BorderColor = .GetProp(PANELTITLE_TOPLINE_COLOR)
        Endwith
    Endproc

    Procedure TitleImg.Visible_assign
        Lparameters bVisible
        With This.Parent
            If Vartype(.cOrigImg)=='C' And !Empty(.cOrigImg) And File(.cOrigImg)
                .TitleImg.Visible = bVisible
            Else
                .TitleImg.Visible = .F.
            Endif
        Endwith
    Endproc

    Procedure cOrigImg_assign
        Lparameters vNewVal
        With This
            .cOrigImg = m.vNewVal
            .Title.lblCaption.Anchor = 0
            If Vartype(.cOrigImg)=='C' And !Empty(.cOrigImg) And File(.cOrigImg)
                If Upper(Alltrim(Justext(.cOrigImg)))=="ICO"
                    cBmp16 = _Screen.cp_Themesmanager.GetBmpFromIco(.cOrigImg, 16)
                    If !File(cBmp16+".bmp")
                        cBmp16= _Screen.cp_Themesmanager.ImgPath+cBmp16
                    Endif
                Else
                    cBmp16= .cOrigImg
                Endif
                If cp_fileexist(cBmp16+".bmp")
                    .TitleImg.Picture = cBmp16+".bmp"
                    .Title.lblCaption.Left= .TitleImg.Width + 7
                    .TitleImg.Visible = .T.
                Else
                    .TitleImg.Visible = .F.
                    .Title.lblCaption.Left= 7
                Endif
            Else
                .TitleImg.Visible = .F.
                .Title.lblCaption.Left= 7
            Endif
            .Title.lblCaption.Anchor = 3
        Endwith
    Endproc

    Procedure caption_assign
        Lparameters vNewVal
        With This
            Store m.vNewVal To .Caption, .Title.lblCaption.Caption
        Endwith
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

Enddefine

* Fine classe: StandardPanelTitle
**************************************************

**************************************************
* Class:	StandardPanelBody

Define Class StandardPanelBody As Container

    Visible = .T.
    Width = 200
    Height = 100
    BorderWidth = 0
    Anchor = 15


    Add Object btnDummy As CommandButton With ;
        TOP = 0, Left = 0 , Height = 1, Width = 1, ;
        Style = 1

    Protected Procedure Init
        Lparameters cCaption, nIndex, bVisible, bMovable, bShrunk, cImage, cIcon
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.ChangeTheme()
    Endproc

    *-- Occurs when theme change.
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.BorderColor = .GetProp(TITLE_BORDERCOLOR)
            This.BackColor = .GetProp(CONTAINER_BACKCOLOR)
        Endwith
    Endproc

    Procedure ChangeSettings
        For Each oContained In This.Objects
            #If Version(5)>=900
                Try
                    oContained.ChangeSettings()
                Catch
                Endtry
            #Else
                Local old_err
                old_err=On("error")
                On Error
                oContained.ChangeSettings()
                On Error &old_err
            #Endif
        Next
    Endproc

    Procedure Visible_assign
        Lparameters vNewVal
        With This
            vNewVal = vNewVal And !.Parent.TitleBar.shrinkbutton.ISShrunk()
            Store m.vNewVal To .Visible
        Endwith
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

    Procedure MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        If nButton = 1 And nShift=1
            This.Parent.Drag(1)
        Endif
    Endproc

    Procedure DragDrop
        Lparameters oSource, nXCoord, nYCoord
        Local nIndexTmp
        With This
            If Vartype(oSource)='O' And oSource.Name<>.Parent.Name
                .Parent.Parent.MovePanels(.Parent.Index, oSource.Index)
            Endif
        Endwith
    Endproc
Enddefine
* Fine classe: StandardPanelBody
**************************************************


**************************************************
* Class:	VerticalShrinkButton

Define Class VerticalShrinkButton As shrinkbutton


    Add Object ImgShrunk As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0, ;
        MousePointer = 15
    Add Object ImgNotShrunk As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Anchor = 15, Visible = .F., BackStyle=0, ;
        MousePointer = 15


    Width = 7
    Height = 8
    *-- Change button's background image.
    Procedure ChangeBackground
        Lparameters bGotFocus
        With This
            If .shrunk
                .ImgNotShrunk.Visible=.F.
                .ImgShrunk.Visible=.T.
            Else
                .ImgNotShrunk.Visible=.T.
                .ImgShrunk.Visible=.F.
            Endif
        Endwith
    Endproc

    *-- Occurs when theme change.
    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgShrunk.Picture = .GetProp(VERTICALSHRINKBUTTON_SHRUNK_PICTURE)
            This.ImgNotShrunk.Picture = .GetProp(VERTICALSHRINKBUTTON_NOTSHRUNK_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure changeviewmode
        DoDefault()
        With This
            .Parent.Parent.shrunk=.ISShrunk()
        Endwith
    Endproc

Enddefine

* Fine classe: VerticalShrinkButton
**************************************************


Define Class ListGrdItem As Container
    Top = 0
    Left = 0
    Width = 201
    Height = 20
    Visible = .T.
    BorderWidth = 0

    Add Object Img As Image With ;
        top=2, ;
        Left=5, ;
        width=16, ;
        height=16, ;
        mousepointer=15, ;
        Stretch = 1,;
        BackStyle=1,;
        Visible=.T.

    Add Object Text As Label With ;
        top=4, ;
        Left=23, ;
        width=175, ;
        height=20, ;
        mousepointer=15, ;
        FontName="Arial",;
        FontSize = 9,;
        ForeColor = Rgb(0,0,0),;
        BackStyle=0,;
        Anchor=10,;
        AutoSize = .T.,;
        Visible=.T.

    BackStyle=0
    *Backcolor = RGB(230, 230, 223)

    cCmd = ""

    Procedure Init
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.SetFont()
        This.ChangeTheme()
        DoDefault()
    Endproc

    Procedure ChangeTheme
        This.Picture = i_ThemesManager.GetProp(TOOLBAR_BTN_HOVER)
        This.BorderColor = i_ThemesManager.GetSingleProp(OVERFLOWPANELBUTTON_FOCUSEDSELECTED_PICTURE,2)
    Endproc

    Procedure Destroy
        i_ThemesManager.RemoveInstance(This, "ChangeTheme")
    Endproc

    Procedure SetFont()
        This.Text.FontName = SetFontName('L', Iif(Vartype(i_cWindowManagerFontName)='C', Alltrim(i_cWindowManagerFontName), This.Text.FontName))
        This.Text.FontSize = SetFontSize('L', Iif(Vartype(i_nWindowManagerFontSize)='N',i_nWindowManagerFontSize , This.Text.FontSize))
    Endproc

    Procedure Text.caption_assign
        Lparameters cNewVal
        This.Visible=.F.
        This.Caption= m.cNewVal
        This.Visible= .T.
        This.Parent.Img.Visible = This.Visible 
        This.Parent.Width=This.Width +This.Left
        This.Parent.Parent.Width=Max(This.Width+This.Left ,This.Parent.Parent.Width)
    Endproc

    Procedure Text.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This.Parent
            .BackStyle = 1
            .BorderWidth = 1
        Endwith

    Endproc

    Procedure Text.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This.Parent
            .BackStyle = 0
            .BorderWidth = 0
        Endwith
    Endproc

    Procedure Text.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Parent.ExecCmd(This.Parent.cCmd)
    Endproc

    Procedure Img.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This.Parent
            .BackStyle = 1
            .BorderWidth = 1
        Endwith
    Endproc

    Procedure Img.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        With This.Parent
            .BackStyle = 0
            .BorderWidth = 0
        Endwith
    Endproc

    Procedure Img.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Parent.ExecCmd(This.Parent.cCmd)
    Endproc

Enddefine

Define Class ListGrd As Container
    Top=0
    Left=1
    Width = 201
    Height = 0
    #If Version(5)>=900
        Add Object oItems As Collection
    #Else
        Add Object oItems As Cp_Collection
    #Endif

    Visible=.T.
    BackStyle=0
    BorderWidth = 0

    Function AddItem(cLabel, cBmp, cCmd)
        With This
            Local l_oName, l_HPlus
            l_HPlus = 0
            l_oName = Sys(2015)
            .AddObject(l_oName, "ListGrdItem")
            .oItems.Add(.&l_oName, l_oName)
            .&l_oName..Text.Caption = cLabel
            .&l_oName..Img.Picture = cBmp
            .&l_oName..cCmd = cCmd
            If .oItems.Count>1
                .&l_oName..Top = .oItems[.oItems.Count-1].Top + .oItems[.oItems.Count-1].Height
            Else
                .&l_oName..Top = 5
                l_HPlus = 10
            Endif
            .&l_oName..Width = .Width
            .&l_oName..Left = 0
            .&l_oName..Anchor = 10
            .Height=.Height + .&l_oName..Height + l_HPlus
            Return .oItems.Count
        Endwith
    Endfunc

    Procedure RenameItem(cText, nIndex)
        This.oItems[m.nIndex].Text.Caption = cText
    Endproc

    Procedure SelectItem(nIndex)
        Local l_i, oItem
        *--- Seleziono l'item
        This.oItems[m.nIndex].Text.FontBold = .T.
    Endproc

    Procedure DeSelectItem(nIndex)
        Local l_i, oItem
        *--- DeSeleziono l'item
        This.oItems[m.nIndex].Text.FontBold = .F.
    Endproc

    Procedure ChangeIconItem(cIcon, nIndex)
        This.oItems[m.nIndex].Img.Picture = cIcon
    Endproc

    Procedure RemoveItem(nIndex)
        Local l_i, oItem, l_H
        With This
            l_H = .oItems[m.nIndex].Height
            .RemoveObject(.oItems[m.nIndex].Name)
            For l_i = m.nIndex To .oItems.Count
                oItem = .oItems[l_i]
                oItem.Top = oItem.Top - oItem.Height
            Next
            .Height=.Height - l_H
            oItem = .Null.
            If .oItems.Count = 0
                .Height = .Height - 10
            Endif
        Endwith
    Endproc

    Procedure Clear()
        Local l_i
        For l_i=1 To This.oItems.Count
            This.RemoveItem(l_i)
        Next
    Endproc

    Procedure ExecCmd(vCmd)
    Endproc

Enddefine

Define Class ListWndOpen As ListGrd

    PrevWndFunc = 0

    #If Version(5)>=900
        Add Object aForm_hWnd As Collection
    #Else
        Add Object aForm_hWnd As Cp_Collection
    #Endif

    Visible=.T.

    Procedure Init
        DoDefault()
        *--- Declare Win32 API functions
        *!*			Declare Integer CallWindowProc In Win32API Integer lpPrevWndFunc, Integer HWnd, Integer Msg, Integer wParam, Integer Lparam
        *!*			Declare Integer GetWindowLong In Win32API Integer HWnd, Integer nIndex
        *!*			*--- Store handle for use in CallWindowProc
        *!*			This.PrevWndFunc = GetWindowLong(_vfp.HWnd, GWL_WNDPROC)

        This.AddActiveForms()
        *Bindevent(0, WM_CREATE, This, "OnCreateForm")
    Endproc

    Procedure ChangeSettings

    Endproc

    Procedure Destroy
        *UNBINDEVENTS(0, WM_CREATE)
    Endproc

    *!*		PROCEDURE OnCreateForm(HWnd As Integer, Msg As Integer, wParam As Integer, Lparam As Integer)
    *!*			BINDEVENT(m.hWnd, WM_SHOWWINDOW, this, 'AddFormItem' , 4)
    *!*			CallWindowProc(this.PrevWndFunc, m.hWnd , m.Msg , m.wParam , m.lParam)
    *!*		ENDPROC

    *--- Aggiungo le form attive
    Procedure AddActiveForms
        Local oForm

        For Each oForm In _Screen.Forms
            If Not oForm.Caption == '' And oForm.ShowWindow <> 2 And Upper(oForm.BaseClass) == 'FORM' And ;
               (!PEMSTATUS(oForm, "bSysForm", 5) OR !oForm.bSysForm)&&Upper(oForm.Class)<>"CP_STATUSBARFORM"
                If oForm.WindowType = 1
                    *--- Non aggiungo una form modale
                    Loop
                Endif
                This.AddFormItem(oForm.HWnd)
                Bindevent(oForm.HWnd, WM_DESTROY, This, 'OnDestroyForm', 4)
            Endif
        Endfor
        oForm = .Null.
    Endproc

    *--- Aggiungo una nuova form alla lista
    Procedure AddFormItem(HWnd)

        Local cKey, oForm
        lAddFormOK = .F.
        *--- Ricerco la form che ha handler hwnd
        cKey = Transform(m.hWnd)
        For Each oForm In _Screen.Forms
            If oForm.HWnd = m.hWnd And Not oForm.ShowWindow = 2 And Upper(oForm.BaseClass) == 'FORM' And ;
               (!PEMSTATUS(oForm, "bSysForm", 5) OR !oForm.bSysForm)&&Upper(oForm.Class)<>"CP_STATUSBARFORM"
                *--- Ricerco form non di tipo top level form
                If oForm.WindowType = 1
                    *--- Non aggiungo una form modale
                    Exit
                Endif
                *--- Controllo se ho gi� inserito la form
                If (This.aForm_hWnd.GetKey(cKey) = 0)
                    This.SelectItem(This.AddItem(Iif(!Empty(oForm.Caption), Alltrim(oForm.Caption), "Wait..."), oForm.Icon, m.hWnd))
                    This.aForm_hWnd.Add(m.hWnd, cKey)
                    *--- Bind Activate and Caption
                    Bindevent(oForm, 'Activate', This, 'OnActivateForm',0)
                    Bindevent(oForm, 'Deactivate', This, 'OnDeactivateForm',1)
                    Bindevent(oForm, 'Caption' , This, 'OnCaptionForm', 1)
                    Bindevent(m.hWnd, WM_DESTROY, This, 'OnDestroyForm', 4)
                    Exit
                Endif
            Endif
        Endfor

        oForm = .Null.
    Endproc

    *--- E' cambiata la caption di una form
    Procedure OnCaptionForm
        Local Array aEv[1]
        Local l_cHWnd, oForm, l_Index

        Aevents(aEv,0)
        If Upper(aEv[2]) = 'CAPTION' And aEv[3] = 0
            l_cHWnd = aEv[1].HWnd
            l_Index = This.aForm_hWnd.GetKey(Transform(l_cHWnd))
            If l_Index<>0
                oForm = aEv[1]
                This.RenameItem(Alltrim(oForm.Caption), l_Index)
                This.ChangeIconItem(oForm.Icon, l_Index)
            Endif
            oForm = .Null.
        Endif
    Endproc

    Procedure OnActivateForm
        Local Array aEv[1]
        Local l_cHWnd, l_Index

        Aevents(aEv,0)
        If Upper(aEv[2]) = 'ACTIVATE' And aEv[3] = 0
            l_cHWnd = aEv[1].HWnd
            l_Index = This.aForm_hWnd.GetKey(Transform(l_cHWnd))
            If l_Index<>0
                This.SelectItem(l_Index)
            Endif
        Endif
    Endproc

    Procedure OnDeactivateForm
        Local Array aEv[1]
        Local l_cHWnd, l_Index

        Aevents(aEv,0)
        If Upper(aEv[2]) = 'DEACTIVATE' And aEv[3] = 0
            l_cHWnd = aEv[1].HWnd
            l_Index = This.aForm_hWnd.GetKey(Transform(l_cHWnd))
            If l_Index<>0
                This.DeSelectItem(l_Index)
            Endif
        Endif
    Endproc

    Procedure OnDestroyForm
        Lparameters HWnd , Msg , wParam , Lparam

        Local l_Index
        Unbindevents(HWnd, WM_DESTROY)

        l_Index = This.aForm_hWnd.GetKey(Transform(m.hWnd))
        If l_Index<>0
            This.aForm_hWnd.Remove(Transform(m.hWnd))
            This.RemoveItem(l_Index)
        Endif

        *CallWindowProc(this.PrevWndFunc, hWnd , Msg , wParam , lParam)
    Endproc

    Procedure ExecCmd(vCmd)
        Local l_cHWnd, cKey, oForm
        cKey = Transform(m.vCmd)
        l_cHWnd = This.aForm_hWnd.Item(cKey)
        For Each oForm In _Screen.Forms
            If oForm.HWnd == l_cHWnd
                This.ShowWnd(oForm)
            Endif
        Endfor
        oForm = .Null.
    Endproc

    Procedure ShowWnd(oForm)
        If Vartype(_Screen.TBMDI)='O' And _Screen.TBMDI.Visible And oForm.bTBMDI
            Raiseevent(m.oForm, "Activate")
        Else
            m.oForm.WindowState=0
            m.oForm.Show()
        Endif
    Endproc

    Procedure Height_assign
        Lparameters vNewVal
        This.Height = m.vNewVal
    Endproc
Enddefine


Define Class _SubMenuNavigator As Container

    Width = 211
    Height = 200
    BackStyle = 1
    BorderWidth = 0
    Anchor = 0
    Name = "SubMenuNavigator"
    Caption = "Menu Navigator"
    cCursor = ""
    Visible = .T.

    Add Object PathCombo As ComboBox With ;
        FontName = "Arial", ;
        FontSize = 8, ;
        FontItalic = .T., ;
        Height = 22, ;
        Left = 24, ;
        SpecialEffect = 0, ;
        Top = 3, ;
        Width = 183, ;
        Anchor = 10,;
        Style = 2,;
        Name = "PathCombo"

    Add Object SubMenuList As ListBox With ;
        FontName = "Arial", ;
        FontSize = 9, ;
        Height = 169, ;
        Left = 3, ;
        SpecialEffect = 0, ;
        Top = 28, ;
        Width = 205, ;
        SelectedItemBackColor = Rgb(230, 230, 223),;
        SelectedItemForeColor = Rgb(0, 0, 0),;
        AutoHideScrollBar=1 ,;
        Anchor = 15 , ;
        Name = "SubMenuList"

    Add Object PrevImg As Image With ;
        Picture = "", ;
        Height = 20, ;
        Left = 3, ;
        Top = 3, ;
        Width = 20, ;
        Anchor = 3, ;
        MousePointer = 15,  ;
        backstyle=0,;
        Stretch = 2,;
        Tooltiptext = "Folder up",;
        Name = "PrevImg", Enabled= .F.

    #If Version(5)>=900
        Add Object aPath As Collection
    #Else
        Add Object aPath As Cp_Collection
    #Endif



    Add Object ImgHide As Image With BackStyle= 0, Visible=.F.

    Procedure Init
        DoDefault()
        With This
            .Width = .Parent.Width
            .Anchor = 15
            i_ThemesManager.AddInstance(This, "OnVisualThemeChanged")
            .OnVisualThemeChanged()
            .PrevImg.Enabled=.F.
        Endwith
    Endproc

    Procedure ChangeSettings
        This.SetFont()
    Endproc

    Procedure OnVisualThemeChanged
        This.SetFont()
        With This
            i_ThemesManager.PutBmp(.PrevImg, "Folder_up.bmp", 16)
            .BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
        Endwith
    Endproc

    Procedure SetFont
        This.SubMenuList.FontName = SetFontName('L', Alltrim(i_cMenuNavFontName))
        This.SubMenuList.FontSize = SetFontSize('L', i_nMenuNavFontSize)
        l_oldFontName = This.PathCombo.FontName
        This.PathCombo.FontName = SetFontName('C', Alltrim(i_cMenuNavFontName))
        This.PathCombo.FontSize = SetFontSize('C', i_nMenuNavFontSize)
    Endproc

    Procedure Destroy
        i_ThemesManager.RemoveInstance(This, "OnVisualThemeChanged")
    Endproc

    Procedure FillList(cKey, cName, cCursorMenu)
        Local cPic, cOldPic, l_ItemLbl
        With This
            .cCursor = Iif(Vartype(cCursorMenu)="C", cCursorMenu, .cCursor)
            .SubMenuList.Clear()
            Select CPBMPNAME, Alltrim(VOCEMENU) As VOCEMENU, LVLKEY, Directory, NAMEPROC, MODULO, Enabled From (.cCursor) Where Len(Alltrim(LVLKEY))= Len(cKey) + 4 And cKey==Left(Alltrim(LVLKEY),Len(cKey)) Order By LVLKEY Into Cursor __SUBMENU_CUR__
            Scan
                If __SUBMENU_CUR__.Directory <> 4
                    If !i_oDeskmenu.EvalModules(Alltrim(MODULO), Enabled)	&&Se modulo disattivo disabilito la voce
                        l_ItemLbl = ""

                    Else
                        l_ItemLbl = "\"
                    Endif
                    l_ItemLbl = l_ItemLbl + Chrtran(Trim(cp_Translate(__SUBMENU_CUR__.VOCEMENU)),'&','')
                    .SubMenuList.AddItem(l_ItemLbl)
                    cPic = Alltrim(__SUBMENU_CUR__.CPBMPNAME)
                    If Empty(cPic) Or Upper(Justfname(cPic)) = Upper(g_MLEAFBMP)
                        .SubMenuList.Picture[.SubMenuList.ListCount] = i_ThemesManager.GetBmpFromIco(cp_GetStdImg(Forceext("Empty.ico",''), 'ico')+'.ico', 16)+".Bmp"
                    Else
                        .SubMenuList.Picture[.SubMenuList.ListCount] = i_ThemesManager.GetBmpFromIco(cp_GetStdImg(Forceext(cPic,''), 'ico')+'.ico', 16)+".Bmp"
                    Endif
                    If __SUBMENU_CUR__.Directory = 2
                        .SubMenuList.Picture[.SubMenuList.ListCount] = i_ThemesManager.GetBmpFromIco(cp_GetStdImg(Forceext("Arrow.ico",''), 'ico')+'.ico', 16)+".Bmp"
                    Endif
                Else
                    .SubMenuList.AddItem("\ "+Replicate("-",200))
                Endif
                Select __SUBMENU_CUR__
            Endscan
            .FillCombo(cKey, cName)
        Endwith
    Endproc

    Procedure SubMenuList.DblClick
        Local l_Item, nCnt, subname
        With This
            l_Item = .ListIndex
            Select __SUBMENU_CUR__
            Goto l_Item
            Do Case
                Case __SUBMENU_CUR__.Directory = 1
                    *--- Aggiorno PathCombo

                    *--- Lancia la voce di menu
                    subname = Strtran(Trim(__SUBMENU_CUR__.NAMEPROC),'#',' with ')
                    If "'"$subname
                        Do mhit In cp_menu With "&subname", Alltrim(Strtran(cp_translate(__SUBMENU_CUR__.VOCEMENU),'&'))
                    Else
                        Do mhit In cp_menu With '&subname', Alltrim(Strtran(cp_translate(__SUBMENU_CUR__.VOCEMENU),'&'))
                    Endif
                Case __SUBMENU_CUR__.Directory = 2
                    *--- Aggiorno SubMenuNav
                    .Parent.FillList(Alltrim(__SUBMENU_CUR__.LVLKEY), .List[l_Item])
            Endcase
        Endwith
    Endproc

    Procedure FillCombo(cPathKey, cPathName)
        With This
            .aPath.Add(cPathName, cPathKey)
            .PathCombo.AddItem(cPathName)
            .PathCombo.Value = cPathName
            .PrevImg.Enabled = .aPath.Count > 1
        Endwith
    Endproc

    Procedure PrevImg.Enabled_Assign()
        Lparameters bNewVal
        This.Enabled = bNewVal
        If This.Enabled
            i_ThemesManager.PutBmp(.PrevImg, "Folder_up.bmp", 16)
        Else
            This.Picture = Forceext(i_ThemesManager.GrayScaleBmp(i_ThemesManager.RetBmpFromIco("Folder_up.bmp", 16),16,0,.T.),"bmp")
        Endif
    Endproc

    Procedure PathCombo.InteractiveChange
        Local l_Key, l_Name
        With This.Parent
            l_Key = .aPath.GetKey(This.ListIndex)
            l_Name = .aPath[This.ListIndex]
            .ChangePath(This.ListIndex, l_Key, l_Name)
        Endwith
    Endproc

    Procedure PrevImg.Click
        Local l_Key, l_Name
        With This.Parent
            If .aPath.Count>1
                .aPath.Remove(.aPath.Count)
                .PathCombo.RemoveItem(.aPath.Count)
                l_Key = .aPath.GetKey(.aPath.Count)
                l_Name = .aPath[.aPath.Count]
                .ChangePath(.aPath.Count, l_Key, l_Name)
            Endif
        Endwith
    Endproc

    Procedure ChangePath(nIndex, cKey, cName)
        Local l_i
        With This
            For l_i = .aPath.Count To nIndex Step -1
                .aPath.Remove(l_i)
                .PathCombo.RemoveItem(l_i)
            Next
            .FillList(cKey, cName)
        Endwith
    Endproc

    Procedure Resize
        DoDefault()
        This.SubMenuList.Height = Max(0, This.Height - This.SubMenuList.Top - 2)
    Endproc
Enddefine


*--- TBMDI
Define Class TitleMDI As Title
    Add Object ImgIcon As Image  With ;
        top = 4,;
        left = 4,;
        width=18,;
        height=18,;
        BackStyle=0,;
        Stretch=2,;
        visible=.T.

    Procedure Init
        DoDefault()
        *--- Sposto la label per fare spazio alla icona
        This.lblCaption.Anchor=0
        This.lblCaption.Left= 25
        This.lblCaption.Anchor=11
    Endproc

    Procedure SetPicture(xValue)
        This.ImgIcon.Picture=xValue
    Endproc
Enddefine

*--- TitleBarMDI
Define Class TitleBarMDI As Container

    Add Object ImgClose As cp_ImgButtonStd With ;
        top = 6,;
        left = 0,;
        visible=.T.

    Add Object ImgMin As cp_ImgButtonStd With ;
        top = 6,;
        left = 0,;
        visible=.T.

    Add Object ImgMax As cp_ImgButtonStd With ;
        top = 6,;
        left = 0,;
        visible=.T.

    Add Object Header As TitleMDI With ;
        Top = 0, ;
        Left = 0, ;
        showpicture = .T.

    #If Version(5)>=900
        Add Object aCycle As Collection
    #Else
        Add Object aCycle As Cp_Collection
    #Endif

    Caption = ""
    Icon = ""

    FormTop = 0
    FormLeft = 0
    FormWidth = 0
    FormHeight = 0

    PrevWndFunc = 0

    oForm = -1

    OldFormTop = 0
    OldFormLeft = 0
    OldFormWidth = 0
    OldFormHeight = 0

    OldTitleBar = 0
    OldBorderStyle = 0

    *--- Cambio icona
    Procedure icon_assign
        Lparameters xVal
        This.Header.SetPicture(m.xVal)
        This.Icon = m.xVal
    Endproc

    *--- Cambio caption
    Procedure caption_assign
        Lparameters xVal
        This.Header.Caption = m.xVal
        This.Caption = This.Header.Caption
    Endproc

    Procedure DblClick()
        If This.ImgMax.Visible
            This.RestoreStd("R")
        Endif
    Endproc

    *--- oForm_access
    Procedure oForm_Access
        Local l_hwnd, l_oForm
        l_hwnd = This.oForm
        If This.oForm<>-1
            For Each l_oForm In _Screen.Forms
                If l_oForm.HWnd = l_hwnd
                    Return l_oForm
                Endif
            Next
        Endif
        Return .Null.
    Endproc

    Procedure oForm_assign
        Lparameters xValue
        If Type("xValue") = "O" And !Isnull(xValue)
            This.oForm = xValue.HWnd
        Else
            This.oForm = -1
        Endif
    Endproc

    Procedure LocalizeString
        With This
            .ImgClose.ToolTipText = cp_Translate(MSG_CLOSE)
            .ImgMin.ToolTipText = cp_Translate(MSG_MINIMIZE)
            .ImgMax.ToolTipText = cp_Translate(MSG_RESTORE)
        Endwith
    Endproc

    *--- Resize Titlebar
    Procedure Resize
        With This
            .Header.Move(-1, 0, Max(0, .Width+1))
            *--- Sposto la system command _[]X
            .ImgClose.Move(Max(36, .Header.Width - 21))
            .ImgMax.Move(Max(18, .Header.Width - 39))
            .ImgMin.Move(Max(0, .Header.Width - 57))
            *--- Ricalcolo le nuove dimensioni della ClientArea
            .FormTop = This.Top + This.Header.Height
            .FormLeft = This.Left
            .FormWidth = .Width - 1
            .FormHeight = Max(0, _Screen.NavBar.Height - This.Header.Height-1)
            *--- Ridimensione la form associata alla titlebar
            If !Isnull(.oForm)
                Local l_oldLockScreen
                l_oldLockScreen = _Screen.LockScreen
                _Screen.LockScreen = .T.
                .oForm.Move(.FormLeft, .FormTop, .FormWidth, .FormHeight)
                _Screen.LockScreen = l_oldLockScreen
            Endif
        Endwith
        DoDefault()
    Endproc

    Procedure Init
        DoDefault()
        *!*			*--- Declare Win32 API functions
        *!*			Declare Integer CallWindowProc In Win32API Integer lpPrevWndFunc, Integer HWnd, Integer Msg, Integer wParam, Integer Lparam
        *!*			Declare Integer GetWindowLong In Win32API Integer HWnd, Integer nIndex
        *!*			*--- Store handle for use in CallWindowProc
        *!*			This.PrevWndFunc = GetWindowLong(_vfp.HWnd, GWL_WNDPROC)
        This.Header.ZOrder(1)
        This.LocalizeString()
        This.Header.ChangeTheme()
        This.ChangeTheme()
        i_ThemesManager.AddInstance(This, "changetheme")
    Endproc

    Procedure Destroy
        DoDefault()
        i_ThemesManager.RemoveInstance(This, "changetheme")
    Endproc

    Procedure ChangeTheme
        This.BorderColor=i_ThemesManager.GetProp(55)
    Endproc

    Procedure Header.ChangeTheme
        DoDefault()
        This.Parent.Resize()
    Endproc

    Procedure Visible_assign
        Lparameters xVal
	if This.Visible <> m.xVal
            This.Visible = m.xVal
            _Screen.NavBar.OnResize()
            _Screen.imgBackground.Visible = This.Visible Or (Vartype(_Screen.NavBar)<>"U" And _Screen.NavBar.Visible)
            *--- Sposto in secondo piano la backgruond mask se presente
            If Vartype(_Screen.Cnt)<>"U"
                _Screen.Cnt.ZOrder(1)
            Endif
		    If TYPE("_SCREEN.ahlogo")="O"
			    _SCREEN.ahlogo.ZOrder(1)
		    Endif
		    If TYPE("_screen.bckImg")="O"
			    _SCREEN.bckImg.ZOrder(1)
		    Endif		
            *--- Nascondo bottoni min/max
            This.ImgMin.Visible = i_cViewMode<>"I"
            This.ImgMax.Visible = i_cViewMode<>"I"
            *--- Creo array per Cycle
            If This.Visible
                Local oForm
                For Each oForm In _Screen.Forms
                    If Pemstatus(oForm, "cPrg", 5) And oForm.MaxButton And oForm.Closable And oForm.Dockable = 0
                        If This.aCycle.GetKey(Transform(.oForm.HWnd))=0
                            This.aCycle.Add(.oForm.HWnd, Transform(.oForm.HWnd))
                        Endif
                    Endif
                Next
            Else
                *--- Svuoto l'array
                This.aCycle.Remove(-1)
            Endif
            i_bClientAreaMode = This.Visible
	EndIf
    Endproc

    *--- Cattura l'evento WM_NCLBUTTONDBLCLK
    Procedure OnNCButtonDblClickForm
        Parameters HWnd As Integer, Msg As Integer, wParam As Integer, Lparam As Integer
        *--- 	SYSCOMMAND
        This.OnSysCommandForm(m.hWnd , 0, 0xF030, 0)
    Endproc

    *--- Cattura l'evento SYSCOMMAND
    Procedure OnSysCommandForm
        Parameters HWnd As Integer, Msg As Integer, wParam As Integer, Lparam As Integer
        *--- Se sto maximizzando
        Local oForm, l_CallWndProc
        l_CallWndProc = .T.
        For Each oForm In _Screen.Forms
            If oForm.HWnd = m.hWnd
                If m.wParam = SC_MAXIMIZE
                    If oForm.MaxButton And oForm.WindowType = 0 And oForm.Dockable = 0 And i_MenuToolbar.ShowModalWnd=0 And;
                            TYPE('oForm.oPgFrm.Pages(1).oPag.resizeYpos')='N' And Type('oForm.oPgFrm.Pages(1).oPag.resizeYpos')='N' And oForm.oPgFrm.Pages(1).oPag.resizeYpos<>-1 And oForm.oPgFrm.Pages(1).oPag.resizeYpos<>-1
                        If oForm.WindowState=1	&&Se da ridotto a icona, devo prima normalizzare
                            CallWindowProc(oForm.nPrevWndFunc, m.hWnd , m.Msg , SC_RESTORE, m.lParam)
                        Endif
                        If !Pemstatus(m.oForm,"LinkPCClick",5)
                            If This.Visible And !Isnull(This.oForm)
                                This.RestoreForm()
                            Endif
                            *--- Catturo la form
                            This.BindForms(oForm)
                            l_CallWndProc = .F.
                            m.oForm.bTBMDI = .T.
                        Else
                            m.oForm.bTBMDI = .F.
                        Endif
                    Else
                        oForm.bTBMDI = .F.
                    Endif
                    Exit
                Else
                    *--- Ripropago l'evento
                    l_CallWndProc = .F.
                    CallWindowProc(m.oForm.nPrevWndFunc, m.hWnd , m.Msg , m.wParam , m.lParam)
                    Exit
                Endif
            Else
                oForm = .Null.
            Endif
        Next
        *--- Se non sono entrato in modalit� ClientArea devo ribaltare l'evento alla _vfp
        If !Isnull(oForm) And l_CallWndProc
            CallWindowProc(oForm.nPrevWndFunc, m.hWnd , m.Msg , m.wParam , m.lParam)
        Endif
        oForm = .Null.
    Endproc

    *--- Gestisco la form nella ClientArea
    Procedure BindForms(oForm)
        *--- Catturo gli eventi Destroy, Caption e Icon della form
        Bindevent(m.oForm, "Destroy", This, "OnDestroyForm", 4)
        Bindevent(m.oForm, "Caption", This, "OnCaptionForm", 1)
        Bindevent(m.oForm, "Icon", This, "OnIconForm", 1)
        With This
            .oForm = m.oForm
            Local oScreen_Form
            *--- Nascondo le form e catturo l'activate
            For Each oScreen_Form In _Screen.Forms
                If Pemstatus(oScreen_Form, "cPrg", 5) And oScreen_Form.Closable
                    oScreen_Form.Visible=.F.
                    Bindevent(oScreen_Form, "Activate", This, "OnActivateForm", 0)
                Endif
            Next
            .oForm.Visible=.F.
            Local l_oldLockScreen
            l_oldLockScreen = _Screen.LockScreen
            _Screen.LockScreen = .T.
            *--- Modifico il layout della form per farla diventare la ClientArea
            
            *--- Zucchetti Aulla FormDecorator Inizio
            If Pemstatus(m.oForm, "oDec", 5) And m.oForm.oDec.nTitlebar#-1
                .OldTitleBar = m.oForm.oDec.nTitlebar
            Else
                .OldTitleBar = m.oForm.TitleBar
            Endif
            *--- Zucchetti Aulla FormDecorator Fine
        
            .OldBorderStyle = m.oForm.BorderStyle
            .oForm.TitleBar = 0
            .oForm.BorderStyle = 0
            .Caption = oForm.Caption
            .Icon = oForm.Icon
            If m.oForm.WindowState = 1
                m.oForm.WindowState = 0
            Endif
            *--- Salvo le vecchi dimensioni
            .OldFormTop = m.oForm.Top
            .OldFormLeft = m.oForm.Left
            .OldFormWidth = m.oForm.Width
            .OldFormHeight = m.oForm.Height
            .oForm.Move(.FormLeft, .FormTop, .FormWidth, .FormHeight)
            .oForm.Resize()
            .Visible = .T.
            .oForm.Visible=.T.
            If .aCycle.GetKey(Transform(.oForm.HWnd))=0
                .aCycle.Add(.oForm.HWnd, Transform(.oForm.HWnd))
            Endif
            *--- Abilito i bottoni
            .ImgMin.Enabled = .oForm.MinButton
            .ImgMax.Enabled = .oForm.MaxButton
            .ImgClose.Enabled = .oForm.Closable
            _Screen.LockScreen = l_oldLockScreen
        Endwith
    Endproc

    *--- Disattivo la modalit� ClientArea, SysCmd = "R" restore, "M" minimizzo
    Procedure RestoreStd(SysCmd)
        Local oScreen_Form
        Local l_oldLockScreen
        l_oldLockScreen = _Screen.LockScreen
        _Screen.LockScreen = .T.
        For Each oScreen_Form In _Screen.Forms
            If Pemstatus(oScreen_Form, "cPrg", 5) And oScreen_Form.Closable
                oScreen_Form.Visible=.T.
                Unbindevent(oScreen_Form, "Activate", This, "OnActivateForm")
            Endif
        Next
        With This
            .Visible=.F.
            If !Isnull(.oForm)
                Unbindevent(.oForm, "Caption", This, "OnCaptionForm")
                Unbindevent(.oForm, "Icon", This, "OnIconForm")
                Unbindevent(.oForm, "Destroy", This, "OnDestroyForm")
                .oForm.Visible = .F.
                .oForm.TitleBar = .OldTitleBar
                .oForm.BorderStyle = .OldBorderStyle
                .oForm.Move(.OldFormLeft, .OldFormTop, .OldFormWidth, .OldFormHeight)
                If Vartype(.oForm.oPgFrm.nOldWz) <> "U"
                    .oForm.oPgFrm.nOldWz = 0
                    .oForm.oPgFrm.nOldHz = 0
                    .oForm.oPgFrm.nOldWs = 0
                    .oForm.oPgFrm.nOldHs = 0
                Endif
                Raiseevent(.oForm, "Resize")
                If m.SysCmd = "M"	&&Minimizzo?
                    .oForm.WindowState = 1
                Endif
                .oForm.Visible = .T.
                .oForm = .Null.
            Endif
        Endwith
        _Screen.LockScreen = l_oldLockScreen
    Endproc

    *--- Gestore evento destroy della form
    Procedure OnDestroyForm
        Local Array aEv[1]
        Local oForm, l_oldLockScreen

        Aevents(aEv,0)
        m.oForm = aEv[1]
        Unbindevent(oForm, "Caption", This, "OnCaptionForm")
        Unbindevent(oForm, "Icon", This, "OnIconForm")
        Unbindevent(oForm, "Destroy", This, "OnDestroyForm")
        If This.Visible
            *--- Passo alla finestra successiva
            If This.aCycle.GetKey(Transform(m.oForm.HWnd))<>0
                This.aCycle.Remove(Transform(m.oForm.HWnd))
            Endif
            This.NextForm(m.oForm)
        Endif
    Endproc

    *--- Gestore cambio caption
    Procedure OnCaptionForm
        Local Array aEv[1]
        Local oForm

        Aevents(aEv,0)
        m.oForm = aEv[1]
        If Upper(aEv[2]) = 'CAPTION' And aEv[3] = 0
            This.Caption = oForm.Caption
        Endif
    Endproc

    *--- Gestore cambio icona
    Procedure OnIconForm
        Local Array aEv[1]
        Local oForm

        Aevents(aEv,0)
        m.oForm = aEv[1]
        If Upper(aEv[2]) = 'ICON' And aEv[3] = 0
            This.Icon = oForm.Icon
        Endif
    Endproc

    Procedure OnActivateForm
        Local Array aEv[1]
        Local oForm
        Aevents(aEv,0)
        m.oForm = aEv[1]
        If m.oForm <> This.oForm
            m.oForm.Visible=.F.
            This.RestoreForm()
            This.BindForms(m.oForm)
        Endif
    Endproc

    Procedure RestoreForm
        Local l_oldLockScreen
        l_oldLockScreen = _Screen.LockScreen
        _Screen.LockScreen = .T.
        With This
            .Visible = .F.
            If !Isnull(.oForm)
                .oForm.Visible = .F.
                .oForm.TitleBar = .OldTitleBar
                .oForm.BorderStyle = .OldBorderStyle
                .oForm.Move(.OldFormLeft, .OldFormTop, .OldFormWidth, .OldFormHeight)
                Raiseevent(.oForm, "Resize")
            Endif
        Endwith
        _Screen.LockScreen = l_oldLockScreen
    Endproc

    Procedure NextForm(oForm)
        Local oScreen_Form, l_hwnd, l_NextForm, l_Restore
        l_NextForm = .Null.
        For Each oScreen_Form In _Screen.Forms
            l_hwnd = m.oForm.HWnd
            If Isnull(l_NextForm) And Pemstatus(oScreen_Form, "cPrg", 5) And l_hwnd <> oScreen_Form.HWnd And oScreen_Form.MaxButton And oScreen_Form.Closable And oScreen_Form.Dockable = 0 And oScreen_Form.bTBMDI And ;
                    TYPE('oScreen_Form.oPgFrm.Pages(1).oPag.resizeYpos')='N' And Type('oScreen_Form.oPgFrm.Pages(1).oPag.resizeYpos')='N' And oScreen_Form.oPgFrm.Pages(1).oPag.resizeYpos<>-1 And oScreen_Form.oPgFrm.Pages(1).oPag.resizeYpos<>-1
                l_NextForm = oScreen_Form
                Exit
            Endif
        Next
        If !Isnull(l_NextForm)
            This.RestoreForm()
            This.BindForms(l_NextForm)
        Else
            This.RestoreStd("R")
        Endif
    Endproc

    Procedure Cycle
        Local l_last, l_next, l_NextForm
        l_last = This.aCycle.GetKey(Transform(This.oForm.HWnd))+1
        l_next = Iif(l_last<=This.aCycle.Count, l_last, 1)
        l_hwnd = This.aCycle.Item(l_next)
        For Each l_NextForm In _Screen.Forms
            If l_hwnd == l_NextForm.HWnd
                Exit
            Endif
        Next
        This.RestoreForm()
        This.BindForms(l_NextForm)
    Endproc

    Procedure ImgMin.Click
        This.Parent.RestoreStd("M")
    Endproc

    *-- Occurs when theme change.
    Procedure ImgMin.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Visible=.F.
            This.ImgNotFocused.Visible=.F.
            This.ImgFocused.Picture = ""
            This.ImgNotFocused.Picture = ""
            This.ImgFocused.Picture = .GetProp(MDIMINBUTTONFOCUSED_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(MDIMINBUTTON_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure ImgMax.Click
        This.Parent.RestoreStd("R")
    Endproc

    *-- Occurs when theme change.
    Procedure ImgMax.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Picture = .GetProp(MDIMAXBUTTONFOCUSED_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(MDIMAXBUTTON_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure ImgClose.Click
        Local oForm
        oForm = This.Parent.oForm
        If m.oForm.QueryUnload() And !Isnull(m.oForm)
            m.oForm.Release()
        Endif
    Endproc

    *-- Occurs when theme change.
    Procedure ImgClose.ChangeTheme
        With _Screen.cp_Themesmanager
            This.ImgFocused.Picture = .GetProp(MDICLOSEBUTTONFOCUSED_PICTURE)
            This.ImgNotFocused.Picture = .GetProp(MDICLOSEBUTTON_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure Header.lblCaption.MouseDown
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.Parent.oForm.Show()
    Endproc

Enddefine

*--- Sfondo NavigationPane
Define Class PaneBackground As Container
    BorderWidth = 0

    Add Object ImageBck As Image With ;
        Top = 0, ;
        Left = 0, ;
        Stretch = 2, ;
        Visible = .T.

    _pictureBackground = ""

    Procedure Init
        This.BackColor = i_ThemesManager.GetProp(7)
        i_ThemesManager.AddInstance(This, "updateBackgroundPicture")
        This.updateBackgroundPicture()
    Endproc

    *--- Creo bmp di sfondo
    Procedure updateBackgroundPicture
        Local lOldLockScreen
        With _Screen
            lOldLockScreen = .LockScreen
            .LockScreen = .T.
            This.ImageBck.Visible = .F.
            This.ImageBck.Picture = ""
            This._pictureBackground = i_ThemesManager.GetProp(FORM_TBMDI_BACKGROUND_PICTURE)
            This.ImageBck.Picture = This._pictureBackground
            This.ImageBck.Visible = .T.
            .LockScreen = m.lOldLockScreen
        Endwith
    Endproc

    Procedure Resize
        This.ImageBck.Move(0, 0, This.Width, This.Height)
    Endproc

    Procedure Destroy
        i_ThemesManager.RemoveInstance(This, "updateBackgroundPicture")
    Endproc

Enddefine

*---
Define Class cp_ImgButtonStd As Container
    BorderWidth = 0
    BackStyle = 0
    Anchor = 0

    Add Object ImgFocused As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Visible = .F., BackStyle=0

    Add Object ImgNotFocused As Image With ;
        Top = 0, Left = 0, ;
        Width = SB_STDWIDTH, Height = SB_STDHEIGHT, ;
        Visible = .F., BackStyle=0

    Protected Procedure Init()
        i_ThemesManager.AddInstance(This, "ChangeTheme")
        This.Width = SB_STDWIDTH
        This.Height = SB_STDHEIGHT
        This.ChangeTheme()
    Endproc

    Procedure ChangeTheme
        With _Screen.cp_Themesmanager
            *This.ImgFocused.Picture = .GetProp(CLOSEBUTTONFOCUSED_PICTURE)
            *This.ImgNotFocused.Picture = .GetProp(CLOSEBUTTON_PICTURE)
            This.ChangeBackground(.F.)
        Endwith
    Endproc

    Procedure ChangeBackground
        Lparameters llGotFocus
        #Define lnFocused 8
        #Define lnNotFocused 16
        Local lnState
        With This
            m.lnState = Iif(m.llGotFocus,lnFocused,lnNotFocused)
            .ImgFocused.Visible=.F.
            .ImgNotFocused.Visible=.F.
            Do Case
                Case m.lnState = (lnFocused)
                    .ImgFocused.Visible=.T.
                Case m.lnState = (lnNotFocused)
                    .ImgNotFocused.Visible=.T.
            Endcase
        Endwith
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.T.)
        This.Parent.MouseEnter()
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ChangeBackground(.F.)
        This.Parent.MouseLeave()
    Endproc

    Procedure ImgFocused.Click
        If This.Parent.Enabled
            This.Parent.Click()
        Endif
    Endproc

    Protected Procedure Destroy
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc

    Procedure ToolTipText_Assign(xValue)
        This.ToolTipText = m.xValue
        This.ImgFocused.ToolTipText = m.xValue
        This.ImgNotFocused.ToolTipText = m.xValue
    Endproc

Enddefine

#Define COUNTMSGRSS 3

*--- MsgRSSCnt
*--- Container di MSGRSS
Define Class MsgRSSCnt As Container
    Top = 0
    BackStyle = 1
    BorderWidth = 1
    BackColor=Rgb(255,255,255)
    Width = 196
    Height = 135
    Anchor = 10
    Add Object oMsgRRS1 As MsgRSS With Left=1
    Add Object oMsgRRS2 As MsgRSS With Left=1, Top = 45
    Add Object oMsgRRS3 As MsgRSS With Left=1, Top = 90
Enddefine

*--- SearchRSS
Define Class SearchRSS As Container
    Top = 0
    Width = 200
    Height = 23
    BackStyle=0
    BorderWidth=0
    Anchor = 10
    Add Object oText As TextBox With Top = 1, Left = 2, Width=176, Anchor=10
    Add Object oBtn As cp_ImgButtonStd With Top = 3, Left = 180, Anchor=8

    Procedure Init
        With This
            .oBtn.ImgFocused.Picture = _Screen.cp_Themesmanager.GetProp(ZOOMTFOCUSED_PICTURE)
            .oBtn.ImgNotFocused.Picture = _Screen.cp_Themesmanager.GetProp(ZOOMT_PICTURE)
        Endwith
    Endproc
Enddefine

*--- RRS Viewer
*--- Container per visualizzare msg RSS
Define Class RSSViewer As Container
    Top=0
    Width=201
    Height=183
    Left=0
    BackStyle = 1
    BorderWidth = 0

    Add Object oSearchRSS As SearchRSS With ;
        Top = 3

    Add Object oMsgRSSCnt As MsgRSSCnt With ;
        top = 28, Left = 2

    Add Object LnSep As Line With ;
        Top = 165, ;
        Left = 0, ;
        Width = 201, ;
        Height = 0, ;
        Anchor = 10

    Add Object oRRSMng As RSSManager With Top = 165

    cRecordSource = ""
    cSearchSource = ""
    nStartFeedItem = 1
    nNumItem = 0
    Dimension aFeedURL[1,2]
    nFeedURL = 0
    cRead = 'A' &&A All, R Read, N NoRead

    Procedure LoadRSS(oFeedFolder, bDownLoad)
        Local l_FeedItem, l_Feed, l_Count, lcFeedName, loItems, l_Feeds, loSubFolders, l_nFeedItem, l_OldErrDL, l_ErrDL
        With This
            m.loSubFolders = m.oFeedFolder.SubFolders
            For Each m.loSubFolder In m.loSubFolders
                This.LoadRSS(m.loSubFolder, m.bDownLoad)
            Next
            l_Feeds = oFeedFolder.Feeds
            For Each l_Feed In m.l_Feeds
                l_nFeedItem = Ascan(.aFeedURL, Lower(Alltrim(l_Feed.URL)), 1, -1, 1, 10)
                If m.l_nFeedItem <> 0
                    If m.bDownLoad
                        l_Feed.MaxItemCount = Iif(.aFeedURL[m.l_nFeedItem, 2]=0, g_MaxItemFeedCount, .aFeedURL[m.l_nFeedItem, 2])
                        l_OldErrDL = On("Error")
                        l_ErrDL = .F.
                        On Error l_ErrDL = .T.
                        ah_msg("Download in corso...", .T., .T.)
                        l_Feed.Download()
                        Wait Clear
                        On Error &l_OldErrDL
                        If l_ErrDL
                            ah_ErrorMsg("Si � verificato un errore durante il download")
                            Exit
                        Endif
                    Endif
                    If l_Feed.ItemCount > 0
                        m.lcFeedName = m.l_Feed.Name
                        m.loItems = m.l_Feed.Items
                        For Each l_FeedItem In m.loItems
                            Insert Into (.cRecordSource) (Date, Title, Feed, Author, Link, Descript, Isread) ;
                                VALUES (m.l_FeedItem.PubDate, m.l_FeedItem.Title, m.lcFeedName, m.l_FeedItem.Author, m.l_FeedItem.Link, m.l_FeedItem.Description, m.l_FeedItem.Isread)
                        Endfor
                    Endif
                Endif
            Endfor
            Select * From (.cRecordSource) Order By Date Desc, Feed, Title Into Cursor (.cRecordSource) Readwrite
        Endwith
    Endproc

    Procedure ShowFeedItem()
        Local l_i, l_oldArea, l_cursor, L_Obj, l_cursor, l_Num
        LOCAL TestMacro
        l_oldArea = Select()
        With This
            m.l_i = 1
            If .nNumItem > 0
                l_cursor = .cSearchSource
                Select(l_cursor)
                If .nStartFeedItem > .nNumItem
                    .nStartFeedItem = 1
                Endif
                Go Record .nStartFeedItem
                Do While m.l_i<COUNTMSGRSS+1
                    L_Obj=Evaluate("This.oMsgRSSCnt.oMsgRRS"+Alltrim(Str(l_i)))
                    L_Obj.cTitle = &l_cursor..Title
                    L_Obj.cFeedName = &l_cursor..Feed
                    L_Obj.tDateTime = &l_cursor..Date
                    L_Obj.bIsRead= &l_cursor..Isread
                    TestMacro=Isblank(&l_cursor..Title) And Isblank(&l_cursor..Feed) And Isblank(&l_cursor..Date)
                    If TestMacro
                        L_Obj.nRecNo = 0
                    Else
                        L_Obj.nRecNo = Recno()
                    Endif
                    Select(l_cursor)
                    Skip
                    l_i = l_i + 1
                Enddo
                l_Num = Min(.nStartFeedItem + COUNTMSGRSS - 1, .nNumItem)
                .oRRSMng.LblCounter.Caption = Alltrim(Str(Ceiling(.nStartFeedItem/COUNTMSGRSS)))+"/"+Alltrim(Str(Ceiling(.nNumItem/COUNTMSGRSS)))
            Else
                *--- Svuoto la lista se non ho nessun FeedItem
                Do While m.l_i<COUNTMSGRSS+1
                    L_Obj=Evaluate("This.oMsgRSSCnt.oMsgRRS"+Alltrim(Str(l_i)))
                    L_Obj.cTitle = ""
                    L_Obj.cFeedName = ""
                    L_Obj.tDateTime = {}
                    L_Obj.bIsRead= .F.
                    L_Obj.nRecNo = 0
                    l_i = l_i + 1
                Enddo
            Endif
        Endwith
        Select(l_oldArea)
    Endproc

    Procedure ReLoadRSS(bDownLoad)
        Local l_rootFolder, l_oldArea, l_i, l_oldSafety
        l_oldArea = Select()
        With This
            l_rootFolder = .oFeedsMng.RootFolder
            Select(.cRecordSource)
            l_oldSafety = Set("Safety")
            Set Safety Off
            Zap
            Set Safety &l_oldSafety
            .LoadRSS(l_rootFolder, m.bDownLoad)
            Select(.cRecordSource)
            l_Rec = Mod(Reccount(.cRecordSource), COUNTMSGRSS)
            If Reccount(.cRecordSource) = 0
                l_Rec = COUNTMSGRSS
            Endif
            If l_Rec<>0
                For l_i=1 To COUNTMSGRSS-l_Rec
                    Select(.cRecordSource)
                    Append Blank
                Endfor
            Endif
            Select * From (.cRecordSource) Order By Date Desc, Feed, Title Into Cursor (.cSearchSource) Readwrite
            Select(.cSearchSource)
            Count For !Empty(Feed) To .nNumItem
            .nStartFeedItem = 1
            .ShowFeedItem()
        Endwith
        Select(l_oldArea)
    Endproc

    Procedure Init()
        Local l_rootFolder
        With This
            .AddObject("oFeedsMng", "FeedsManager")
            .cRecordSource = "FeedItems"+Sys(2015)
            .cSearchSource = "FeedItemsSearch"+Sys(2015)
            Create Cursor (.cRecordSource) (Date T, Title C(240), Feed C(240), Author C(240), Link M, Descript M, Isread L)
            Create Cursor (.cSearchSource) (Date T, Title C(240), Feed C(240), Author C(240), Link M, Descript M, Isread L)
            *.ReLoadRSS()
            *--- FeedURL to see
            .aFeedURL[1,1]=''
            .aFeedURL[1,2]=0
            .oRRSMng.Init()
            i_ThemesManager.AddInstance(This, "ChangeTheme")
            .ChangeTheme()
        Endwith
        DoDefault()
    Endproc

    *--- Aggiungo gli URL dei Feed che voglio visualizzare, gli altri verranno filtrati
    Procedure AddFeedURL(cFeedURL, nMaxItem)
        If Vartype(m.cFeedURL)='C' And !Empty(m.cFeedURL)
            With This
                .nFeedURL = .nFeedURL + 1
                Dimension .aFeedURL[.nFeedURL, 2]
                .aFeedURL[.nFeedURL, 1] = Lower(Alltrim(cFeedURL))
                .aFeedURL[.nFeedURL, 2] = nMaxItem
            Endwith
        Endif
    Endproc

    Procedure ChangeTheme
        With This
            .BackColor = i_ThemesManager.GetProp(PANELVERTICAL_BACKCOLOR)
            .oMsgRSSCnt.BorderColor = _Screen.cp_Themesmanager.GetProp(CONTAINER_BORDERCOLOR)
            .LnSep.BorderColor = _Screen.cp_Themesmanager.GetProp(CONTAINER_BORDERCOLOR)
            .oMsgRSSCnt.oMsgRRS1.ShpSelected.BorderColor = _Screen.cp_Themesmanager.GetProp(CONTAINER_BORDERCOLOR)
            .oMsgRSSCnt.oMsgRRS2.ShpSelected.BorderColor = _Screen.cp_Themesmanager.GetProp(CONTAINER_BORDERCOLOR)
            .oMsgRSSCnt.oMsgRRS3.ShpSelected.BorderColor = _Screen.cp_Themesmanager.GetProp(CONTAINER_BORDERCOLOR)
        Endwith
    Endproc

    Procedure ShowItem(nRecNo)
        Local l_oldArea, l_oForm
        If m.nRecNo<>0
            l_oldArea = Select()
            l_oForm = Createobject("FeedItemForm")
            Select(This.cSearchSource)
            Goto Record m.nRecNo
            l_oForm.ShowItem(Title, Feed, Author, Link, Descript)
            l_oForm.Show(1)
            Select(l_oldArea)
        Endif
    Endproc

    Procedure SearchFeedItem(cSearchVaule, cRead)
        Local l_SearchValue, l_bRead
        With This
            .cRead = Iif(Vartype(m.cRead)='C', m.cRead, .cRead)
            l_SearchValue = Alltrim(Nvl(m.cSearchVaule, ""))
            If !Empty(m.l_SearchValue)
                l_SearchValue = "%" + m.l_SearchValue + "%"
                Select * From (.cRecordSource) Order By Date Desc, Feed, Title ;
                    WHERE Title Like l_SearchValue Or Feed Like l_SearchValue Or Descript Like l_SearchValue And ;
                    (.cRead='A' Or (.cRead='S' And Isread) Or (.cRead='N' And !Isread)) ;
                    Into Cursor (.cSearchSource) Readwrite
            Else
                Select * From (.cRecordSource) Order By Date Desc, Feed, Title ;
                    WHERE 1=1 And (.cRead='A' Or (.cRead='S' And Isread) Or (.cRead='N' And !Isread)) ;
                    Into Cursor (.cSearchSource) Readwrite
            Endif
            Select(.cSearchSource)
            l_Rec = Mod(Reccount(.cSearchSource), COUNTMSGRSS)
            If Reccount(.cSearchSource) = 0
                l_Rec = COUNTMSGRSS
            Endif
            If l_Rec<>0
                For l_i=1 To COUNTMSGRSS-l_Rec
                    Select(.cSearchSource)
                    Append Blank
                Endfor
            Endif
            Select(.cSearchSource)
            Count For !Empty(Feed) To .nNumItem
            .nStartFeedItem = 1
            .ShowFeedItem()
        Endwith
    Endproc

    Procedure oSearchRSS.oBtn.Click()
        This.Parent.Parent.SearchFeedItem(This.Parent.oText.Value)
    Endproc

    Procedure Destroy()
        Use In Select(This.cRecordSource)
        Use In Select(This.cSearchSource)
        i_ThemesManager.RemoveInstance(This,"ChangeTheme")
    Endproc
Enddefine

*--- Messaggio RSS, contiene Titolo, FeedName, data e IDRSS
Define Class MsgRSS As Container

    Top = 0
    Left = 0
    Width = 196
    Height = 45
    BackStyle = 0
    BorderWidth = 0
    Anchor = 10
    Name = "MsgRSS"
    Visible = .T.

    cTitle = "RSS Title"
    cFeedName = "FeedName"
    tDateTime = Datetime()
    nRecNo = 0
    bIsRead = .F.

    Add Object ShpSelected As Shape With ;
        Top = 1, ;
        Left = 1, ;
        Height = 43, ;
        Width = 192, ;
        BackStyle = 0, ;
        BorderStyle = 3, ;
        Curvature = 4,;
        Visible = .F., ;
        Anchor = 10, ;
        Name = "shpSelected"


    Add Object lbltitle As Label With ;
        FontBold = .T., ;
        FontSize = 10, ;
        Anchor = 10, ;
        BackStyle = 0, ;
        Caption = "", ;
        Height = 17, ;
        Left = 3, ;
        Top = 6, ;
        Width = 188, ;
        Name = "LblTitle"


    Add Object lblfeed As Label With ;
        FontItalic = .T., ;
        FontSize = 7, ;
        Anchor = 10, ;
        BackStyle = 0, ;
        Caption = "", ;
        Height = 17, ;
        Left = 3, ;
        Top = 24, ;
        Width = 90, ;
        Name = "LblFeed"


    Add Object lbldate As Label With ;
        FontItalic = .T., ;
        FontSize = 7, ;
        Anchor = 8, ;
        Alignment = 1, ;
        BackStyle = 0, ;
        Caption = "", ;
        Height = 17, ;
        Left = 95, ;
        Top = 24, ;
        Width = 96, ;
        Name = "LblDate"

    *--- Se nn letto � grassetto
    Procedure bIsRead_Assign(xValue)
        This.bIsRead = m.xValue
        This.lbltitle.FontBold = !m.xValue
    Endproc

    Procedure cTitle_Assign(xValue)
        This.cTitle = Alltrim(Chrtran(m.xValue, Chr(10)+Chr(13)+Chr(9),""))
        This.lbltitle.Caption = This.cTitle

    Procedure cFeedName_Assign(xValue)
        This.cFeedName = Alltrim(Chrtran(m.xValue, Chr(10)+Chr(13),""))
        This.lblfeed.Caption = This.cFeedName

    Procedure tDateTime_Assign(xValue)
        This.tDateTime = m.xValue
        This.lbldate.Caption = Iif(Empty(m.xValue),"",Ttoc(m.xValue))

    Procedure ItemMouseHover(bSelected)
        This.ShpSelected.Visible= m.bSelected And This.nRecNo<>0

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ItemMouseHover(.T.)

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ItemMouseHover(.F.)

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.ItemMouseHover(.T.)

    Procedure lbldate.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ItemMouseHover(.T.)

    Procedure lbldate.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ItemMouseHover(.F.)

    Procedure lbldate.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ItemMouseHover(.T.)

    Procedure lblfeed.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ItemMouseHover(.T.)

    Procedure lblfeed.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ItemMouseHover(.F.)

    Procedure lblfeed.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ItemMouseHover(.T.)

    Procedure lbltitle.MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ItemMouseHover(.T.)

    Procedure lbltitle.MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ItemMouseHover(.F.)

    Procedure lbltitle.MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.Parent.ItemMouseHover(.T.)

    Procedure lbltitle.caption_assign
        Lparameters tAssign
        This.Caption=m.tAssign
        This.ToolTipText=m.tAssign

    Procedure lblfeed.caption_assign
        Lparameters tAssign
        This.Caption=m.tAssign
        This.ToolTipText=m.tAssign

    Procedure lbldate.caption_assign
        Lparameters tAssign
        This.Caption = m.tAssign
        This.ToolTipText = m.tAssign

    Procedure lbltitle.Click()
        This.Parent.Click()
    Endproc

    Procedure lblfeed.Click()
        This.Parent.Click()
    Endproc

    Procedure lbldate.Click()
        This.Parent.Click()
    Endproc

    Procedure Click()
        This.Parent.Parent.ShowItem(This.nRecNo)
        This.ItemMouseHover(.F.)
    Endproc

Enddefine

Define Class RSSManager As Container

    Width = 200
    Height = 24
    BackStyle = 0
    BorderWidth = 0
    Anchor = 10
    Name = "RSSManager"

    Add Object LblCounter As Label With ;
        Alignment = 2, ;
        BackStyle = 0, ;
        Caption = "1/--", ;
        Height = 17, ;
        Left = 20, ;
        Top = 2, ;
        Width = 50, ;
        Anchor=2, ;
        Name = "LblCounter"

    Add Object ShpUp As cp_ImgButtonStd With ;
        Top = 1, ;
        Left = 2, ;
        Anchor=2, ;
        Name = "ShpUp"

    Add Object ShpDwn As cp_ImgButtonStd With ;
        Top = 1, ;
        Left = 72, ;
        Anchor=2, ;
        Name = "ShpDwn"

    Add Object MenuButton As cp_ImgButtonStd With ;
        Anchor = 9, ;
        Top = 1, ;
        Left = 185, ;
        Anchor=8, ;
        Name = "MenuButton"

    Procedure Init
        With This
            .ShpUp.ImgFocused.Picture = _Screen.cp_Themesmanager.GetProp(UPBUTTONFOCUSED_PICTURE)
            .ShpUp.ImgNotFocused.Picture = _Screen.cp_Themesmanager.GetProp(UPBUTTON_PICTURE)

            .ShpDwn.ImgFocused.Picture = _Screen.cp_Themesmanager.GetProp(DWNBUTTONFOCUSED_PICTURE)
            .ShpDwn.ImgNotFocused.Picture = _Screen.cp_Themesmanager.GetProp(DWNBUTTON_PICTURE)

            .MenuButton.ImgFocused.Picture = _Screen.cp_Themesmanager.GetProp(OVERFLOWPANEL_MENUBUTTONFOCUSEDRSS_PICTURE)
            .MenuButton.ImgNotFocused.Picture = _Screen.cp_Themesmanager.GetProp(OVERFLOWPANEL_MENUBUTTONRSS_PICTURE)
        Endwith
    Endproc

    Procedure ShpUp.Click()
        Local l_Num
        With This.Parent
            If .Parent.nNumItem > 0
                If .Parent.nStartFeedItem - COUNTMSGRSS<1
                    If Mod(.Parent.nNumItem, COUNTMSGRSS)<>0
                        .Parent.nStartFeedItem = .Parent.nNumItem - Mod(.Parent.nNumItem, COUNTMSGRSS) + 1
                    Else
                        .Parent.nStartFeedItem = .Parent.nNumItem - COUNTMSGRSS + 1
                    Endif
                Else
                    .Parent.nStartFeedItem = .Parent.nStartFeedItem - COUNTMSGRSS
                Endif
                .Parent.ShowFeedItem()
            Endif
        Endwith
    Endproc

    Procedure ShpDwn.Click()
        With This.Parent
            If .Parent.nNumItem > 0
                .Parent.nStartFeedItem = Iif(.Parent.nStartFeedItem + COUNTMSGRSS> .Parent.nNumItem, 1, .Parent.nStartFeedItem + COUNTMSGRSS)
                .Parent.ShowFeedItem()
            Endif
        Endwith
    Endproc

    Procedure MenuButton.Click
        *--- Open menu
        Local oBtnMenu, oMenuItem, x, Y
        m.oBtnMenu = Createobject("cbPopupMenu")
        m.oBtnMenu.oPopupMenu.cOnClickProc="Eval(NAMEPROC)"
        m.oMenuItem = m.oBtnMenu.AddMenuItem()
        m.oMenuItem.Caption = Ah_Msgformat('Visualizza solo non letti')
        m.oMenuItem.OnClick = "i_oDeskmenu.oRSSViewer.SearchFeedItem('', 'N')"
        m.oMenuItem.MarkFor = Iif(i_oDeskmenu.oRSSViewer.cRead="N", ".T.",".F.")
        m.oMenuItem.Visible = .T.
        m.oMenuItem = m.oBtnMenu.AddMenuItem()
        m.oMenuItem.Caption = Ah_Msgformat('Visualizza solo letti')
        m.oMenuItem.OnClick = "i_oDeskmenu.oRSSViewer.SearchFeedItem('', 'S')"
        m.oMenuItem.MarkFor = Iif(i_oDeskmenu.oRSSViewer.cRead="S", ".T.",".F.")
        m.oMenuItem.Visible = .T.
        m.oMenuItem = m.oBtnMenu.AddMenuItem()
        m.oMenuItem.Caption = Ah_Msgformat('Visualizza tutti')
        m.oMenuItem.OnClick = "i_oDeskmenu.oRSSViewer.SearchFeedItem('', 'A')"
        m.oMenuItem.MarkFor = Iif(i_oDeskmenu.oRSSViewer.cRead="A", ".T.",".F.")
        m.oMenuItem.Visible = .T.
        m.oMenuItem = m.oBtnMenu.AddMenuItem()
        m.oMenuItem.Caption = Ah_Msgformat('Download feed')
        m.oMenuItem.OnClick = "i_oDeskmenu.oRSSViewer.ReLoadRSS(.T.)"
        m.oMenuItem.BeginGroup = .T.
        m.oMenuItem.Visible = .T.
        m.oMenuItem = m.oBtnMenu.AddMenuItem()
        m.oMenuItem.Caption = Ah_Msgformat('Apri RSS Viewer')
        m.oMenuItem.OnClick = "GSUT_KRS()"
        m.oMenuItem.Visible = .T.
        m.oBtnMenu.InitMenu()
        m.x = cb_GetWindowLeft(_Screen.HWnd)+Objtoclient(This, 2)
        m.y = cb_GetWindowTop(_Screen.HWnd)+Objtoclient(This, 1)+This.Height-2
        m.oBtnMenu.ShowMenu(m.x, m.y, 0)
    Endproc
Enddefine

Define Class ShpArrow As Shape
    nDisabledColor = Rgb(236,233,216)
    nSelectedColor = Rgb(172,168,153)
    BorderColor = Rgb(172,168,153)

    Procedure DrawArrow(nArrow)
        Do Case
            Case m.nArrow = 1
                *--- Up
                Dimension aUp[3,2]
                aUp[1,1]=1
                aUp[1,2]=100
                aUp[2,1]=100
                aUp[2,2]=100
                aUp[3,1]=50
                aUp[3,2]=1
                This.Polypoints = "aUp"
            Otherwise
                *--- Down
                Dimension aDwn[3,2]
                aDwn[1,1]=1
                aDwn[1,2]=1
                aDwn[2,1]=100
                aDwn[2,2]=1
                aDwn[3,1]=50
                aDwn[3,2]=100
                This.Polypoints = "aDwn"
        Endcase
    Endproc

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.BackColor = This.nSelectedColor

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.BackColor = This.nDisabledColor

    Procedure MouseMove
        Lparameters nButton, nShift, nXCoord, nYCoord
        This.BackColor = This.nSelectedColor
Enddefine

Define Class FeedItemForm As CPForm
    HalfHeightCaption = .T.
    AutoCenter=.T.
    Height = 300
    Width = 550
    AlwaysOnTop = .T.
    Icon = "Mask.ico"

    Add Object oHTMLViewer As OLEBrowser With ;
        Top = 0, ;
        Left = 0, ;
        Height = 300, ;
        Width = 550, ;
        Anchor = 15

    Procedure ShowItem(cTitle, cFeed, cAuthor, cLink, cDescript)
        This.Caption = Alltrim(cTitle)
        This.oHTMLViewer.Navigate("about:blank")
        DoEvents Force
        This.oHTMLViewer.Document.Write(Thisform.GetHTML(m.cTitle, m.cFeed, m.cAuthor, m.cLink, m.cDescript))
    Endproc

    Function GetHTML(cTitle, cFeed, cAuthor, cLink, cDescript)
        Local lcHTML
        m.lcHTML = ""
        TEXT TO m.lcHTML TEXTMERGE NOSHOW PRETEXT 7
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
		<html>
			<head>
				<title><<EVALUATE("m.cTitle")>></title>
				<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
				<style type="text/css">body {margin:0; padding:0; background:#fff; color:#000}td,body,.form {font-family:Tahoma; letter-spacing:1.1lm;font-size: 10pt}A {color:#336699; text-decoration: none;}</style>
			</head>
		   <body scroll="no">
				<table height="100%" width="100%" border="0">
				  <tr>
					<td height="50" style="border: 1px solid #FF9933;" bgcolor="#FFCC66">
						<div style="height:100%;width:100%;padding:10px 10px;">
							<font size="1" face="Verdana, Arial, Helvetica, sans-serif">
		        				<strong>Feed:</strong>
						      	<<EVALUATE("m.cFeed")>><br>
		        				<strong>Author:</strong>
						      	<<EVALUATE("m.cAuthor")>><br>
						      	<strong>Title:</strong>
						      	<a href="<<EVALUATE('m.cLink')>>" target="_blank">
						      		<<EVALUATE("m.cTitle")>>
						      	</a>
					      	</font>
				      	</div>
					</td>
				  </tr>
				  <tr>
				    <td>
		          		<div style="height:100%;width:100%;overflow:auto;padding:10px 10px;">
				    		<font face="Verdana, Arial, Helvetica, sans-serif">
								<<EVALUATE("m.cDescript")>><br>
							</font>
	               		</div>
				    </td>
				  </tr>
				</table>
			</body>
		</html>
        ENDTEXT
        Return m.lcHTML
    Endfunc
Enddefine


