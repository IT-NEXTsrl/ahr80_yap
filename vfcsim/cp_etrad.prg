* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_ETRAD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 01/04/99
* Aggiornato il : 01/04/99
* Translated    : 03/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Maschera per il sistema di traduzione
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

If !Used('cp_lang')
    Sele 0
    * --- Zucchetti Aulla (cp_lang in dictionary)
    Use dictionary\cp_lang
Endif
Return(Createobject("tGES_TRA"))

* --- Definizione della classe
Define Class tGES_TRA As StdTrsForm
    Top    = 12
    Left   = 101

    * --- Standard Properties
    Width  = 638
    Height = 358+35
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="1/4/99"

    * --- Movements Properties
    cTrsName=''

    * --- Constants Properties
    CP_LANG_IDX=0
    cFile = "cp_lang"
    cKeySelect = "ORIG_STR"
    *cKeyWhere  = "ORIG_STR=this.w_ORIG_STR"
    *cKeyDetail  = "ORIG_STR=this.w_ORIG_STR and LANGUAGE=this.w_LANGUAGE"
    *cKeyWhereODBC = '"ORIG_STR="+cp_ToStrODBC(this.w_ORIG_STR)';

    cPrg = "GES_TRA"
    cComment = MSG_TRANSLATIONS
    i_nRowNum = 0
    i_nRowPerPage = 13
    Icon = "movi.ico"
    i_lastcheckrow = 0

    * --- Local Variables
    w_ORIG_STR = Space(200)
    w_LANGUAGE = Space(3)
    w_TRAN_STR = Space(200)

    * --- Define Page Frame
    Add Object oPgFrm As StdPageFrame With PageCount=2, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tGES_TRAPag1","GES_TRA",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate(MSG_TRANSLATIONS)
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oORIG_STR_1_2
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
    Endproc

    * --- Appertura delle tabelle
    Function OpenWorkTables()
        Local i
        i=cp_AddTableDef('cp_lang')
        This.CP_LANG_IDX=i
        This.bPostIt=.F.
        i_TableProp[i,2]='cp_lang'
        i_TableProp[i,3]=0
        Return(.T.)

    Procedure CloseAllTables()
        cp_RemoveTableDef('cp_lang')
        Return

    Procedure SetWorkFromKeySet
        * --- Assegnazione delle variabili di work dal KeySet che verranno utilizzate per caricare il record
        Select (This.cKeySet)
        With This
            .w_ORIG_STR = Nvl(ORIG_STR,Space(200))
        Endwith
    Endproc

    * --- Lettura del record e assegnazione alle variabili del Form
    Procedure LoadRec()
        Local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
        This.bUpdated=.F.
        This.bHeaderUpdated=.F.
        Select * From cp_lang Where ORIG_STR+Language=This.w_ORIG_STR Order By Language Into Cursor (This.cCursor) nofilter
        This.bLoaded = Not(Eof())
        i_cTF = This.cCursor
        * --- Copia i valori nelle variabili di work
        If This.bLoaded
            With This
                .w_ORIG_STR = Nvl(ORIG_STR,Space(200))
            Endwith
            * === TRANSITORIO
            Select (This.cTrsName)
            Zap
            Select (This.cCursor)
            This.i_nRowNum = 0
            Scan
                Select (This.cTrsName)
                Append Blank
                With This
                    .w_LANGUAGE = Nvl(&i_cTF..Language,Space(3))
                    .w_TRAN_STR = Nvl(&i_cTF..TRAN_STR,Space(200))
                    .TrsFromWork()
                    Replace Language With .w_LANGUAGE
                    Replace I_SRV With " "
                    .nLastRow = Recno()
                    Select (This.cCursor)
                Endwith
            Endscan
            This.nFirstrow=1
            Select (This.cCursor)
            Go Top
            Select (This.cTrsName)
            Go Top
            With This
                .oPgFrm.Page1.oPag.oBody.Refresh()
                .WorkFromTrs()
                .SetControlsValue()
                .mHideControls()
                .oPgFrm.Page1.oPag.oBody.nAbsRow=1
                .oPgFrm.Page1.oPag.oBody.nRelRow=1
                .NotifyEvent('Load')
            Endwith
        Else
            This.BlankRec()
        Endif
    Endproc

    * --- Blank delle variabili del form
    Procedure BlankRec()
        Select (This.cTrsName)
        Zap
        Append Blank
        This.nLastRow  = Recno()
        This.nFirstrow = 1
        Replace I_SRV    With "A"
        With This
            .w_ORIG_STR=Space(200)
            .w_LANGUAGE=Space(3)
            .w_TRAN_STR=Space(200)
        Endwith
        This.SetControlsValue()
        This.TrsFromWork()
        This.mHideControls()
        This.NotifyEvent('Blank')
    Endproc

    Procedure SetEnabled(i_cOp)
        Local i_bVal
        i_bVal = i_cOp<>"Query"
        * --- Disattivazione pagina Elenco per situazioni <> da Query
        This.oPgFrm.Pages(This.oPgFrm.PageCount).Enabled=Not(i_bVal)
        With This.oPgFrm
            .Page1.oPag.oORIG_STR_1_2.Enabled = i_bVal
            .Page1.oPag.oBody.Enabled = .T.
            .Page1.oPag.oBody.oBodyCol.Enabled = i_bVal
            If i_cOp = "Edit"
                .Page1.oPag.oORIG_STR_1_2.Enabled = .F.
            Endif
            If i_cOp = "Query"
                .Page1.oPag.oORIG_STR_1_2.Enabled = .T.
            Endif
        Endwith
    Endproc

    * ------ PROCEDURE RIDEFINITE DEL FORM STANDARD

    * --- Genera il filtro
    Func BuildFilter()
        Local i_cFlt
        i_cFlt = ""
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_ORIG_STR,"ORIG_STR",0)
        Return (i_cFlt)
    Endfunc

    * --- Creazione del cursore cKeySet con solo la chiave primaria
    Proc QueryKeySet(i_cWhere,i_cOrderBy)
        Local i_cKey,i_cTable,i_nConn,i_cDatabaseType
        This.cLastWhere = i_cWhere
        This.cLastOrderBy = i_cOrderBy
        i_cWhere = Iif(Not(Empty(i_cWhere)),' where '+i_cWhere,'')
        i_cOrderBy = Iif(Not(Empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
        Select Distinct ORIG_STR From cp_lang &i_cWhere &i_cOrderBy Into Cursor (This.cKeySet)
        Locate For 1=1 && per il problema dei cursori che riusano il file principale
    Endproc

    *
    *  --- PROCEDURE DELLE MOVIMENTAZIONI
    *
    Proc CreateTrs
        This.cTrsName=Sys(2015)
        Create Cursor (This.cTrsName) (;
            t_LANGUAGE C(3);
            ,t_TRAN_STR C(200);
            ,Language C(3);
            ,I_SRV C(1),I_READ L(1),I_TRSDEL C(1))
        This.oPgFrm.Page1.oPag.oBody.ColumnCount=0
        This.oPgFrm.Page1.oPag.oBody.RecordSource=This.cTrsName
        This.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','column')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tGES_TRAbodyrow')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Width=This.oPgFrm.Page1.oPag.oBody.Width
        This.oPgFrm.Page1.oPag.oBody.RowHeight=This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.Height
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.F.
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Visible=.T.
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=This
        * --- Caratteristiche della riga
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLANGUAGE_2_1.ControlSource=This.cTrsName+'.t_LANGUAGE'
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRAN_STR_2_2.ControlSource=This.cTrsName+'.t_TRAN_STR'
        This.oPgFrm.Page1.oPag.oBody.ZOrder(1)
        This.oPgFrm.Page1.oPag.oBody3D.ZOrder(1)
        This.oPgFrm.Page1.oPag.pagebmp.ZOrder(1)
        This.oPgFrm.Page1.oPag.oBody.oFirstControl=This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLANGUAGE_2_1
        Return

    Function mInsert()
        Return(Not(bTrsErr))

        * --- Insert del nuovo Record sul Detail
    Function mInsertDetail(i_nCntLine)
        Local i_cKey,i_nConn,i_cTable,i_TN
        If This.bUpdated .Or. This.IsAChildUpdated()
            i_nConn = 0
            i_cTable = 'cp_lang'
            i_TN = This.cTrsName
            This.NotifyEvent('Insert row start')
            cp_CheckDeletedKey('cp_lang',0,'ORIG_STR',This.w_ORIG_STR,'LANGUAGE',This.w_LANGUAGE)
            Insert Into (i_cTable) (;
                ORIG_STR;
                ,Language;
                ,TRAN_STR;
                ) Values (;
                this.w_ORIG_STR;
                ,This.w_LANGUAGE;
                ,This.w_TRAN_STR;
                )
            This.NotifyEvent('Insert row end')
        Endif
        Return(Not(bTrsErr))

        * --- Aggiornamento del Database
    Function mReplace(i_bEditing)
        Local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK
        Local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
        If This.bUpdated
            i_nModRow = 1
            If i_bEditing .And. This.bHeaderUpdated
                This.mRestoreTrs()
            Endif
            If This.bHeaderUpdated
                This.mUpdateTrs()
            Endif

            * --- Aggiornamento del Detail
            If i_nModRow>0   && Il master e' stato aggiornato con successo
                Set Delete Off
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_bUpdAll = .F.
                Scan For (t_LANGUAGE<>Space(3)) And ((I_SRV="U" Or i_bUpdAll) Or I_SRV="A" Or Deleted())
                    This.WorkFromTrs()
                    i_nRec = Recno()
                    Set Delete On
                    If Deleted()
                        If I_SRV<>"A"
                            * --- Cancella dal database una riga cancellata
                            This.NotifyEvent('Delete row start')
                            This.mRestoreTrsDetail()
                            Delete From cp_lang Where ORIG_STR=This.w_ORIG_STR;
                                and Language=&i_TN.->Language
                            i_nModRow=_Tally
                            This.NotifyEvent('Delete row end')
                        Endif
                    Else
                        If I_SRV="A"
                            * --- Inserisce una nuova riga nel database
                            This.mUpdateTrsDetail()
                            i_NR = 0
                            Replace Language With This.w_LANGUAGE
                            =This.mInsertDetail(i_NR)
                        Else
                            This.NotifyEvent('Update row start')
                            This.mRestoreTrsDetail()
                            This.mUpdateTrsDetail()
                            Update cp_lang Set ;
                                TRAN_STR=This.w_TRAN_STR;
                                ,Language=This.w_LANGUAGE;
                                WHERE ORIG_STR=This.w_ORIG_STR;
                                and Language=&i_TN.->Language
                            i_nModRow=_Tally
                            This.NotifyEvent('Update row end')
                        Endif
                    Endif
                    If i_nModRow<1
                        Exit
                    Endif
                    Set Delete Off
                Endscan
                Set Delete On
            Endif
        Endif
        If Not(bTrsErr)
        Endif
        Return(bTrsErr)

        * --- Restore Transaction
    Proc mRestoreTrs
    Endproc

    * --- Restore Detail Transaction
    Proc mRestoreTrsDetail
    Endproc

    * --- Delete Records
    Function mDelete()
        Local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
        Local i_cDel,i_nRec
        If Not(bTrsErr)
            Select (This.cTrsName)
            i_TN = This.cTrsName
            i_nModRow = 1
            i_cDel = Set('DELETED')
            Set Delete Off
            Scan For (t_LANGUAGE<>Space(3)) And I_SRV<>'A'
                This.WorkFromTrs()
                This.NotifyEvent('Delete row start')
                Delete From cp_lang Where ORIG_STR=This.w_ORIG_STR;
                    and Language=&i_TN.->Language
                i_nModRow=_Tally
                This.NotifyEvent('Delete row end')
                If i_nModRow<1
                    Exit
                Endif
            Endscan
            Set Delete &i_cDel
            If Not(bTrsErr)
                This.mRestoreTrs()
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_cDel = Set('DELETED')
                Set Delete Off
                Scan For (t_LANGUAGE<>Space(3)) And I_SRV<>'A'
                    This.mRestoreTrsDetail()
                Endscan
                Set Delete &i_cDel
            Endif
        Endif
        Return

        * --- Calcoli
    Function mCalc(i_bUpd)
        If i_bUpd
            This.SetControlsValue()
            This.bUpdated=This.bUpdated .Or. Inlist(This.cFunction,'Edit','Load')
            This.mEnableControls()
        Endif
        Return

    Proc mCalcObjs()
        Return

        * --- Abilita controls condizionati
    Procedure mEnableControls()
        This.mEnableControlsFixed()
        This.mHideControls()
        Return

        * --- Abilita controls condizionati per la Grid e per i campi FixedPos
    Procedure mEnableControlsFixed()
        This.mHideRowControls()
        Return

    Procedure mHideControls()
        This.mHideRowControls()
        Return

    Procedure mHideRowControls()
        Return


    Function SetControlsValue()
        Select (This.cTrsName)
        If Not(This.oPgFrm.Page1.oPag.oORIG_STR_1_2.Value==This.w_ORIG_STR)
            This.oPgFrm.Page1.oPag.oORIG_STR_1_2.Value=This.w_ORIG_STR
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLANGUAGE_2_1.Value==This.w_LANGUAGE)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLANGUAGE_2_1.Value=This.w_LANGUAGE
            Replace t_LANGUAGE With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLANGUAGE_2_1.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRAN_STR_2_2.Value==This.w_TRAN_STR)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRAN_STR_2_2.Value=This.w_TRAN_STR
            Replace t_TRAN_STR With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRAN_STR_2_2.Value
        Endif
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Return(.T.)

        * --- CheckRow
    Func CheckRow()
        Return(.T.)

        * --- FullRow
    Func FullRow()
        Select (This.cTrsName)
        Return(t_LANGUAGE<>Space(3))
    Endfunc

    * --- InitRow
    Proc InitRow()
        Select (This.cTrsName)
        Append Blank
        This.nLastRow = Recno()
        Replace I_SRV    With "A"
        With This
            .w_LANGUAGE=Space(3)
            .w_TRAN_STR=Space(200)
        Endwith
        This.TrsFromWork()
        This.NotifyEvent("Init Row")

        * --- WorkFromTrs
    Proc WorkFromTrs()
        Select (This.cTrsName)
        This.w_LANGUAGE = t_LANGUAGE
        This.w_TRAN_STR = t_TRAN_STR
    Endproc

    * --- TrsFromWork
    Proc TrsFromWork()
        Select (This.cTrsName)
        Replace t_LANGUAGE With This.w_LANGUAGE
        Replace t_TRAN_STR With This.w_TRAN_STR
        Replace Language With This.w_LANGUAGE
    Endproc

    * --- SubtractTotals
    Proc SubtractTotals()
    Endproc
Enddefine

* --- Definizione delle pagine come container
Define Class tGES_TRAPag1 As StdContainer
    Width  = 638
    Height = 358+35
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    oldlanguage=''

    Add Object oORIG_STR_1_2 As  StdField  With ;
        cFormVar = "w_ORIG_STR", cQueryName = "ORIG_STR",     bObbl = .F. , nPag = 1, Value=Space(60),;
        Height=21, Width=400, Left=93, Top=6  , InputMask=Replicate('X',200)

    Add Object chkauto As Checkbox With Caption='',Top=6,Left=500,Width=150
    Proc chkauto.Init()
        This.Caption=cp_Translate(MSG_AUTOLEARNING)
        This.Value=i_cLanguage='auto'
    Endproc
    Proc chkauto.Click()
        If !Empty(This.Value)
            If i_cLanguage<>'auto'
                This.Parent.oldlanguage=i_cLanguage
            Endif
            i_cLanguage='auto'
        Else
            i_cLanguage=This.Parent.oldlanguage
        Endif
    Endproc

    Add Object oStr_1_3 As StdString With Visible=.T., Left=13, Top=6,;
        Alignment=1, Width=77, Hight=15,;
        Caption=""

    Add Object oStr_1_4 As StdString With Visible=.T., Left=19, Top=40,;
        Alignment=0, Width=133, Hight=15,;
        Caption=""

    Add Object oStr_1_5 As StdString With Visible=.T., Left=84, Top=40,;
        Alignment=0, Width=77, Hight=15,;
        Caption=""

    Add Object oBox_1_1 As StdBox With Left=8, Top=29, Width=623,Height=1

    Add Object oBox_1_6 As StdBox With Left=74, Top=57, Width=0,Height=287

    Add Object oEndHeader As BodyKeyMover With nDirection=1

    Procedure Init(cPrgName,nPag)
        This.oStr_1_3.Caption=cp_Translate(MSG_ORIGINAL+MSG_FS)
        This.oStr_1_4.Caption=cp_Translate(MSG_CODE)
        This.oStr_1_5.Caption=cp_Translate(MSG_TRANSLATION)
        DoDefault(cPrgName,nPag)
    Endproc

    *
    * --- CORPO movimentazione
    *
    Add Object oBody3D As Shape With Left=7,Top=56,;
        width=619,Height=Int(Fontmetric(1,"Arial",9,"")*13*1.5)-2,SpecialEffect=0

    Add Object oBody As StdBody Noinit With ;
        left=8,Top=57,Width=617,Height=Int(Fontmetric(1,"Arial",9,"")*13*1.5)-4,ColumnCount=0,GridLines=1,;
        HeaderHeight=0,DeleteMark=.F.,RecordMark=.F.,ScrollBars=0,Enabled=.F.,;
        cLinkFile=''

    Proc oBody.SetCurrentRow()
        Thisform.LockScreen=.T.
        Select (This.Parent.oContained.cTrsName)
        If Recno()<>This.nAbsRow
            If This.nAbsRow<>0 And Inlist(This.Parent.oContained.cFunction,'Edit','Load')
                If This.nAbsRow<>This.Parent.oContained.i_lastcheckrow And !This.Parent.oContained.CheckRow()
                    This.Parent.oContained.i_lastcheckrow=0
                    This.Parent.oContained.__dummy__.Enabled=.T.
                    This.Parent.oContained.__dummy__.SetFocus()
                    Select (This.Parent.oContained.cTrsName)
                    Go (This.nAbsRow)
                    This.SetFullFocus()
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                    This.Parent.oContained.__dummy__.Enabled=.F.
                    Thisform.LockScreen=.F.
                    Return
                Endif
            Endif
            This.Parent.oContained.WorkFromTrs()
            This.Parent.oContained.mEnableControlsFixed()
            This.Parent.oContained.mCalcObjs()
            This.nAbsRow=Recno()
        Endif
        If !Isnull(This.Parent.oContained.oNewFocus)
            This.Parent.oContained.oNewFocus.SetFocus()
            This.Parent.oContained.oNewFocus=.Null.
        Endif
        If This.RelativeRow<>0
            This.nRelRow=This.RelativeRow
        Endif
        If This.nBeforeAfter=0
            Thisform.LockScreen=.F.
        Else
            This.nBeforeAfter=This.nBeforeAfter-1
        Endif
        This.Parent.oContained.i_lastcheckrow=0
    Endproc
    Func oBody.GetDropTarget(cFile,nX,nY)
        Local oDropInto
        oDropInto=.Null.
        Do Case
        Endcase
        Return(oDropInto)
    Endfunc


    Add Object oBeginFooter As BodyKeyMover With nDirection=-1
Enddefine

* --- Definizione della riga del Corpo
Define Class tGES_TRABodyRow As Container
    Width=617
    Height=Int(Fontmetric(1,"Arial",9,"")*1*1.5)
    BackStyle=0                                                && 0=trasparente
    BorderWidth=0                                              && Spessore Bordo
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    oContained = .Null.

    Add Object oLANGUAGE_2_1 As  StdTrsField  With ;
        cFormVar="w_LANGUAGE",Value=Space(3),nZero=3,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        Height=17, Width=52, Left=-2, Top=0  , InputMask=Replicate('X',3)  , bHasZoom = .F.

    *  proc oLANGUAGE_2_1.mZoom
    *    private i_cWhere
    *
    *    do cp_zoom with 'LangDB','*','LANGUAGE',cp_AbsName(this.parent,'oLANGUAGE_2_1'),.f.,'ges_lin',''
    *  endproc

    Add Object oTRAN_STR_2_2 As  StdTrsField  With ;
        cFormVar="w_TRAN_STR",Value=Space(200),    cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F.,;
        Height=17, Width=562, Left=70, Top=0  , InputMask=Replicate('X',200)
    Add Object oLast As LastKeyMover
    * ---
    Func oLANGUAGE_2_1.When()
        Return(.T.)
    Proc oLANGUAGE_2_1.GotFocus()
        If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
            This.Parent.Parent.Parent.SetCurrentRow()
            This.Parent.oContained.SetControlsValue()
        Endif
        DoDefault()
    Proc oLANGUAGE_2_1.KeyPress(nKeyCode,nShift)
        DoDefault(nKeyCode,nShift)
        Do Case
            Case Inlist(nKeyCode,5,15)
                Nodefault
                If This.Valid()=0
                    Return
                Endif
                If This.Parent.oContained.CheckRow()
                    If Recno()>This.Parent.oContained.nFirstrow
                        This.Parent.oContained.i_lastcheckrow=This.Parent.Parent.Parent.nAbsRow
                        Thisform.LockScreen=.T.
                        Skip -1
                    Else
                        This.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
                    Endif
                Else
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                Endif
            Case nKeyCode=24
                Nodefault
                If This.Valid()=0
                    Return
                Endif
                If This.Parent.oContained.CheckRow()
                    If Recno()=This.Parent.oContained.nLastRow
                        If This.Parent.oContained.FullRow()
                            Thisform.LockScreen=.T.
                            This.Parent.oContained.__dummy__.Enabled=.T.
                            This.Parent.oContained.__dummy__.SetFocus()
                            This.Parent.oContained.InitRow()
                            This.Parent.Parent.Parent.SetFocus()
                            This.Parent.oContained.__dummy__.Enabled=.F.
                        Else
                            This.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
                        Endif
                    Else
                        Thisform.LockScreen=.T.
                        This.Parent.oContained.i_lastcheckrow=This.Parent.Parent.Parent.nAbsRow
                        Skip
                    Endif
                Else
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                Endif
        Endcase
Enddefine
