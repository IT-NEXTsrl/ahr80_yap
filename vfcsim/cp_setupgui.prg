* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_setupgui                                                     *
*              Configurazione interfaccia                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-24                                                      *
* Last revis.: 2009-11-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_setupgui",oParentObject))

* --- Class definition
define class tcp_setupgui as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 555
  Height = 489
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-25"
  HelpContextID=212666138
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  cpsetgui_IDX = 0
  cPrg = "cp_setupgui"
  cComment = "Configurazione interfaccia"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODUTE1 = space(10)
  o_CODUTE1 = space(10)
  w_CODUTE2 = space(10)
  w_CODUTE = 0
  o_CODUTE = 0
  w_DESUTE = space(20)
  w_DESUTE1 = space(100)
  w_FLDELCAC = space(1)

  * --- Children pointers
  cp_setgui = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to cp_setgui additive
    with this
      .Pages(1).addobject("oPag","tcp_setupguiPag1","cp_setupgui",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODUTE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure cp_setgui
    * --- Area Manuale = Init Page Frame
    * --- cp_setupgui
    * --- Translate interface...
     this.parent.cComment=cp_Translate(MSG_SETTING_GUI)
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='cpsetgui'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.cp_setgui = CREATEOBJECT('stdDynamicChild',this,'cp_setgui',this.oPgFrm.Page1.oPag.oLinkPC_1_4)
    this.cp_setgui.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.cp_setgui)
      this.cp_setgui.DestroyChildrenChain()
      this.cp_setgui=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_4')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.cp_setgui.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.cp_setgui.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.cp_setgui.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.cp_setgui.SetKey(;
            .w_CODUTE,"usrcode";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .cp_setgui.ChangeRow(this.cRowID+'      1',1;
             ,.w_CODUTE,"usrcode";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.cp_setgui)
        i_f=.cp_setgui.BuildFilter()
        if !(i_f==.cp_setgui.cQueryFilter)
          i_fnidx=.cp_setgui.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.cp_setgui.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.cp_setgui.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.cp_setgui.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.cp_setgui.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_cp_setgui(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.cp_setgui.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_cp_setgui(i_ask)
    if this.cp_setgui.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP))
      cp_BeginTrs()
      this.cp_setgui.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODUTE1=space(10)
      .w_CODUTE2=space(10)
      .w_CODUTE=0
      .w_DESUTE=space(20)
      .w_DESUTE1=space(100)
      .w_FLDELCAC=space(1)
        .w_CODUTE1 = i_CODUTE
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODUTE1))
          .link_1_1('Full')
        endif
        .w_CODUTE2 = i_CODUTE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODUTE2))
          .link_1_2('Full')
        endif
        .w_CODUTE = IIF( Empty( .w_CODUTE2)  ,-1 ,  .w_CODUTE2 )
      .cp_setgui.NewDocument()
      .cp_setgui.ChangeRow('1',1,.w_CODUTE,"usrcode")
      if not(.cp_setgui.bLoaded)
        .cp_setgui.SetKey(.w_CODUTE,"usrcode")
      endif
          .DoRTCalc(4,4,.f.)
        .w_DESUTE1 = IIF( .w_CODUTE=-1 , MSG_INSTALLATION_SETTINGS   , .w_DESUTE )
        .w_FLDELCAC = 'N'
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_VALID_FOR + MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_VALID_FOR_TOOLTIP)
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_APPLY_TO + MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_DELETE_TEMPORARY_FILES)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.cp_setgui.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.cp_setgui.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        if .o_CODUTE1<>.w_CODUTE1
            .w_CODUTE = IIF( Empty( .w_CODUTE2)  ,-1 ,  .w_CODUTE2 )
        endif
        .DoRTCalc(4,4,.t.)
        if .o_CODUTE<>.w_CODUTE
            .w_DESUTE1 = IIF( .w_CODUTE=-1 , MSG_INSTALLATION_SETTINGS   , .w_DESUTE )
        endif
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_VALID_FOR + MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_VALID_FOR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_APPLY_TO + MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_DELETE_TEMPORARY_FILES)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_CODUTE<>.o_CODUTE
          .Save_cp_setgui(.t.)
          .cp_setgui.NewDocument()
          .cp_setgui.ChangeRow('1',1,.w_CODUTE,"usrcode")
          if not(.cp_setgui.bLoaded)
            .cp_setgui.SetKey(.w_CODUTE,"usrcode")
          endif
        endif
      endwith
      this.DoRTCalc(6,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_VALID_FOR + MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_VALID_FOR_TOOLTIP)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_APPLY_TO + MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_DELETE_TEMPORARY_FILES)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODUTE1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE1)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE1 = NVL(_Link_.CODE,space(10))
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE1 = space(10)
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE2
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpsetgui_IDX,3]
    i_lTable = "cpsetgui"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2], .t., this.cpsetgui_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select usrcode";
                   +" from "+i_cTable+" "+i_lTable+" where usrcode="+cp_ToStrODBC(this.w_CODUTE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'usrcode',this.w_CODUTE2)
            select usrcode;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE2 = NVL(_Link_.usrcode,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE2 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])+'\'+cp_ToStr(_Link_.usrcode,1)
      cp_ShowWarn(i_cKey,this.cpsetgui_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_3.RadioValue()==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE1_1_7.value==this.w_DESUTE1)
      this.oPgFrm.Page1.oPag.oDESUTE1_1_7.value=this.w_DESUTE1
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDELCAC_1_12.RadioValue()==this.w_FLDELCAC)
      this.oPgFrm.Page1.oPag.oFLDELCAC_1_12.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .cp_setgui.CheckForm()
      if i_bres
        i_bres=  .cp_setgui.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- cp_setupgui
      *--- Cancella cartella bmp
      If i_bRes And .w_FLDELCAC='S' And VarType(i_themesmanager)='O'
         L_FH = FCREATE(ADDBS(ALLTRIM(i_themesmanager.Imgpath))+"bDelGraphics.fla" )
         FCLOSE(L_FH)
      EndIf
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODUTE1 = this.w_CODUTE1
    this.o_CODUTE = this.w_CODUTE
    * --- cp_setgui : Depends On
    this.cp_setgui.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tcp_setupguiPag1 as StdContainer
  Width  = 551
  height = 489
  stdWidth  = 551
  stdheight = 489
  resizeXpos=424
  resizeYpos=438
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCODUTE_1_3 as StdCombo with uid="OHDKSFOZIM",rtseq=3,rtrep=.f.,left=123,top=10,width=101,height=21;
    , ToolTipText = "Impostazioni per utente o generali (applicate all'avvio)";
    , HelpContextID = 98958270;
    , cFormVar="w_CODUTE",RowSource=""+"Utente,"+"Installazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODUTE_1_3.RadioValue()
    return(iif(this.value =1,i_CODUTE,;
    iif(this.value =2,-1,;
    0)))
  endfunc
  func oCODUTE_1_3.GetRadio()
    this.Parent.oContained.w_CODUTE = this.RadioValue()
    return .t.
  endfunc

  func oCODUTE_1_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CODUTE==i_CODUTE,1,;
      iif(this.Parent.oContained.w_CODUTE==-1,2,;
      0))
  endfunc


  add object oLinkPC_1_4 as stdDynamicChildContainer with uid="FBYAQRAVBA",left=7, top=75, width=535, height=373, bOnScreen=.t.;


  add object oDESUTE1_1_7 as StdField with uid="ZPEAJLCWAJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESUTE1", cQueryName = "DESUTE1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 71773374,;
   bGlobalFont=.t.,;
    Height=21, Width=392, Left=123, Top=42, InputMask=replicate('X',100)


  add object oBtn_1_9 as StdButton with uid="KTQEUHINHK",left=434, top=455, width=50,height=20,;
    caption="Salva", nPag=1;
    , HelpContextID = 253097851;
    , caption=MSG_S_SAVE;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="VNDHAOKDHY",left=489, top=455, width=50,height=20,;
    caption="Esci", nPag=1;
    , HelpContextID = 120059674;
    , caption=MSG_S_EXIT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="YWHDTQKVAO",left=7, top=455, width=50,height=20,;
    caption="Default", nPag=1;
    , ToolTipText = ""+MSG_PRESS_TO_SET_DEFAULT_VALUE+"";
    , HelpContextID = 112758193;
    , caption=MSG_DEFAULT;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .cp_setgui.NotifyEvent('SetDefault')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLDELCAC_1_12 as StdCheck with uid="NXJIZVNZSP",rtseq=6,rtrep=.f.,left=129, top=455, caption="Elimina temporaneo immagini",;
    ToolTipText = "Se attivo, svuota la cartella delle immagini temporanee presenti sul pc corrente alla chiusura della procedura",;
    HelpContextID = 96300182,;
    cFormVar="w_FLDELCAC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDELCAC_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLDELCAC_1_12.GetRadio()
    this.Parent.oContained.w_FLDELCAC = this.RadioValue()
    return .t.
  endfunc

  func oFLDELCAC_1_12.SetRadio()
    this.Parent.oContained.w_FLDELCAC=trim(this.Parent.oContained.w_FLDELCAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLDELCAC=='S',1,;
      0)
  endfunc


  add object oObj_1_13 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=5, top=494, width=284,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Validi per:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 220331709


  add object oObj_1_14 as cp_setobjprop with uid="SKPUZOAYUU",left=293, top=494, width=257,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_CODUTE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 220331709


  add object oObj_1_15 as cp_setCtrlObjprop with uid="YWNLGUFSWM",left=5, top=520, width=284,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Applicate su:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 220331709


  add object oObj_1_16 as cp_setCtrlObjprop with uid="CHUEWDVMCO",left=5, top=545, width=284,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="w_FLDELCAC",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 220331709

  add object oStr_1_5 as StdString with uid="DACPYCDNIM",Visible=.t., Left=20, Top=10,;
    Alignment=1, Width=99, Height=18,;
    Caption="Validi per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="QHXEZTNSJZ",Visible=.t., Left=20, Top=42,;
    Alignment=1, Width=99, Height=18,;
    Caption="Applicate su:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_setupgui','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
