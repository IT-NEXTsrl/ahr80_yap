* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_askcolumntitle                                               *
*              Titolo della colonna                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-11-18                                                      *
* Last revis.: 2012-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_askcolumntitle",oParentObject))

* --- Class definition
define class tcp_askcolumntitle as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 860
  Height = 368
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-09"
  HelpContextID=181089404
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_askcolumntitle"
  cComment = "Titolo della colonna"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_cFld = space(100)
  o_cFld = space(100)
  w_cTitle = space(100)
  w_FontGlobal = space(1)
  o_FontGlobal = space(1)
  w_ApplyAllColumn = space(1)
  w_cLabelFont = space(50)
  w_nDimFont = 0
  w_bItalicFont = space(1)
  w_bBoldFont = space(1)
  w_FontGlobalC = space(1)
  o_FontGlobalC = space(1)
  w_ApplyAllColumnC = space(1)
  w_cLabelFontC = space(50)
  w_nDimFontC = 0
  w_bItalicFontC = space(1)
  w_bBoldFontC = space(1)
  w_cFormat = space(20)
  w_nHeight = 0
  w_nWidth = 0
  w_bIsImage = space(1)
  o_bIsImage = space(1)
  w_cForce = space(200)
  w_cBack = space(200)
  w_check1 = space(1)
  w_cZoomName = space(200)
  w_cHyperLink = space(200)
  w_cHyperTarget = space(200)
  w_cLayerContent = space(200)
  w_nColumnType = 0
  w_cToolTip = space(200)
  w_cFld_old = space(10)
  w_ZoomTitle = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_askcolumntitlePag1","cp_askcolumntitle",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.ocTitle_1_12
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_askcolumntitle
    this.Parent.autocenter=.t.
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomTitle = this.oPgFrm.Pages(1).oPag.ZoomTitle
    DoDefault()
    proc Destroy()
      this.w_ZoomTitle = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_cFld=space(100)
      .w_cTitle=space(100)
      .w_FontGlobal=space(1)
      .w_ApplyAllColumn=space(1)
      .w_cLabelFont=space(50)
      .w_nDimFont=0
      .w_bItalicFont=space(1)
      .w_bBoldFont=space(1)
      .w_FontGlobalC=space(1)
      .w_ApplyAllColumnC=space(1)
      .w_cLabelFontC=space(50)
      .w_nDimFontC=0
      .w_bItalicFontC=space(1)
      .w_bBoldFontC=space(1)
      .w_cFormat=space(20)
      .w_nHeight=0
      .w_nWidth=0
      .w_bIsImage=space(1)
      .w_cForce=space(200)
      .w_cBack=space(200)
      .w_check1=space(1)
      .w_cZoomName=space(200)
      .w_cHyperLink=space(200)
      .w_cHyperTarget=space(200)
      .w_cLayerContent=space(200)
      .w_nColumnType=0
      .w_cToolTip=space(200)
      .w_cFld_old=space(10)
      .oPgFrm.Page1.oPag.ZoomTitle.Calculate()
        .w_cFld = .w_ZoomTitle.getVar('cFldZoom')
        .w_cTitle = .w_ZoomTitle.getVar('cTitle')
        .w_FontGlobal = iif(EMPTY(.w_ZoomTitle.getVar('cLabelFont')), 'S', ' ')
        .w_ApplyAllColumn = iif(recno(.w_ZoomTitle.cCursor)=1,' ',.w_ApplyAllColumn)
        .w_cLabelFont = iif(.w_bIsImage='S' or .w_FontGlobal='S', "", .w_ZoomTitle.getVar('cLabelFont'))
        .w_nDimFont = iif(.w_bIsImage='S' or .w_FontGlobal='S', 0, .w_ZoomTitle.getVar('nDimFont'))
        .w_bItalicFont = iif(.w_bIsImage='S' or .w_FontGlobal='S', ' ', .w_ZoomTitle.getVar('bItalicFont'))
        .w_bBoldFont = iif(.w_bIsImage='S' or .w_FontGlobal='S', ' ', .w_ZoomTitle.getVar('bBoldFont'))
        .w_FontGlobalC = iif(EMPTY(.w_ZoomTitle.getVar('cLabelFontC')), 'S', ' ')
        .w_ApplyAllColumnC = iif(recno(.w_ZoomTitle.cCursor)=1,' ',.w_ApplyAllColumnC)
        .w_cLabelFontC = iif(.w_bIsImage='S' or .w_FontGlobalC='S', "", .w_ZoomTitle.getVar('cLabelFontC'))
        .w_nDimFontC = iif(.w_bIsImage='S' or .w_FontGlobalC='S', 0, .w_ZoomTitle.getVar('nDimFontC'))
        .w_bItalicFontC = iif(.w_bIsImage='S' or .w_FontGlobalC='S', ' ', .w_ZoomTitle.getVar('bItalicFontC'))
        .w_bBoldFontC = iif(.w_bIsImage='S' or .w_FontGlobalC='S', ' ', .w_ZoomTitle.getVar('bBoldFontC'))
        .w_cFormat = .w_ZoomTitle.getVar('cFormat')
        .w_nHeight = .w_ZoomTitle.getVar('nHeight')
        .w_nWidth = .w_ZoomTitle.getVar('nWidth')
        .w_bIsImage = .w_ZoomTitle.getVar('bIsImage')
        .w_cForce = .w_ZoomTitle.getVar('cForce')
        .w_cBack = .w_ZoomTitle.getVar('cBack')
        .w_check1 = .w_ZoomTitle.getVar('check1')
        .w_cZoomName = iif(varType(this.oParentObject.parent.parent.parent.parent.class) = 'C' And Upper(This.oParentObject.Parent.Parent.Parent.Parent.Class) = 'VZ_FRM_EXEC', Upper(Alltrim(This.oParentObject.Parent.Parent.Parent.cZoomName))+'.VZM', Upper(Alltrim(This.oParentObject.Parent.Parent.Parent.cZoomName))+'.'+Upper(Alltrim(This.oParentObject.Parent.Parent.Parent.cSymFile))+'_VZM')
        .w_cHyperLink = .w_ZoomTitle.getVar('cHyperLink')
        .w_cHyperTarget = .w_ZoomTitle.getVar('cHyperTarget')
        .w_cLayerContent = .w_ZoomTitle.getVar('cLayerContent')
        .w_nColumnType = .w_ZoomTitle.getVar('nColumnType')
        .w_cToolTip = .w_ZoomTitle.getVar('cToolTip')
        .w_cFld_old = .w_cFld
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomTitle.Calculate()
            .w_cFld = .w_ZoomTitle.getVar('cFldZoom')
        if .o_cFld<>.w_cFld
          .Calculate_RWFVMQPSFE()
        endif
        if .o_cFld<>.w_cFld
            .w_cTitle = .w_ZoomTitle.getVar('cTitle')
        endif
        if .o_cFld<>.w_cFld
            .w_FontGlobal = iif(EMPTY(.w_ZoomTitle.getVar('cLabelFont')), 'S', ' ')
        endif
        if .o_cFld<>.w_cFld
            .w_ApplyAllColumn = iif(recno(.w_ZoomTitle.cCursor)=1,' ',.w_ApplyAllColumn)
        endif
        if .o_cFld<>.w_cFld.or. .o_bIsImage<>.w_bIsImage.or. .o_FontGlobal<>.w_FontGlobal
            .w_cLabelFont = iif(.w_bIsImage='S' or .w_FontGlobal='S', "", .w_ZoomTitle.getVar('cLabelFont'))
        endif
        if .o_cFld<>.w_cFld.or. .o_FontGlobal<>.w_FontGlobal
            .w_nDimFont = iif(.w_bIsImage='S' or .w_FontGlobal='S', 0, .w_ZoomTitle.getVar('nDimFont'))
        endif
        if .o_cFld<>.w_cFld.or. .o_FontGlobal<>.w_FontGlobal
            .w_bItalicFont = iif(.w_bIsImage='S' or .w_FontGlobal='S', ' ', .w_ZoomTitle.getVar('bItalicFont'))
        endif
        if .o_cFld<>.w_cFld.or. .o_FontGlobal<>.w_FontGlobal
            .w_bBoldFont = iif(.w_bIsImage='S' or .w_FontGlobal='S', ' ', .w_ZoomTitle.getVar('bBoldFont'))
        endif
        if .o_cFld<>.w_cFld
            .w_FontGlobalC = iif(EMPTY(.w_ZoomTitle.getVar('cLabelFontC')), 'S', ' ')
        endif
        .DoRTCalc(10,10,.t.)
        if .o_cFld<>.w_cFld.or. .o_bIsImage<>.w_bIsImage.or. .o_FontGlobalC<>.w_FontGlobalC
            .w_cLabelFontC = iif(.w_bIsImage='S' or .w_FontGlobalC='S', "", .w_ZoomTitle.getVar('cLabelFontC'))
        endif
        if .o_cFld<>.w_cFld.or. .o_FontGlobalC<>.w_FontGlobalC
            .w_nDimFontC = iif(.w_bIsImage='S' or .w_FontGlobalC='S', 0, .w_ZoomTitle.getVar('nDimFontC'))
        endif
        if .o_cFld<>.w_cFld.or. .o_FontGlobalC<>.w_FontGlobalC
            .w_bItalicFontC = iif(.w_bIsImage='S' or .w_FontGlobalC='S', ' ', .w_ZoomTitle.getVar('bItalicFontC'))
        endif
        if .o_cFld<>.w_cFld.or. .o_FontGlobalC<>.w_FontGlobalC
            .w_bBoldFontC = iif(.w_bIsImage='S' or .w_FontGlobalC='S', ' ', .w_ZoomTitle.getVar('bBoldFontC'))
        endif
        if .o_cFld<>.w_cFld
            .w_cFormat = .w_ZoomTitle.getVar('cFormat')
        endif
        if .o_cFld<>.w_cFld
            .w_nHeight = .w_ZoomTitle.getVar('nHeight')
        endif
        if .o_cFld<>.w_cFld
            .w_nWidth = .w_ZoomTitle.getVar('nWidth')
        endif
        if .o_cFld<>.w_cFld
            .w_bIsImage = .w_ZoomTitle.getVar('bIsImage')
        endif
        if .o_cFld<>.w_cFld
            .w_cForce = .w_ZoomTitle.getVar('cForce')
        endif
        if .o_cFld<>.w_cFld
            .w_cBack = .w_ZoomTitle.getVar('cBack')
        endif
        if .o_cFld<>.w_cFld.or. .o_bIsImage<>.w_bIsImage
            .w_check1 = .w_ZoomTitle.getVar('check1')
        endif
        .DoRTCalc(22,23,.t.)
        if .o_cFld<>.w_cFld
            .w_cHyperTarget = .w_ZoomTitle.getVar('cHyperTarget')
        endif
        .DoRTCalc(25,25,.t.)
        if .o_cFld<>.w_cFld
            .w_nColumnType = .w_ZoomTitle.getVar('nColumnType')
        endif
        if .o_cFld<>.w_cFld
            .w_cToolTip = .w_ZoomTitle.getVar('cToolTip')
        endif
        if .o_cFld<>.w_cFld
            .w_cFld_old = .w_cFld
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomTitle.Calculate()
    endwith
  return

  proc Calculate_PVCVHNZBAV()
    with this
          * --- Caricamento zoom
          CP_ASKCOLUMNTITLER(this;
              ,'Init';
             )
    endwith
  endproc
  proc Calculate_RWFVMQPSFE()
    with this
          * --- Salvataggio riga in temporaneo
          CP_ASKCOLUMNTITLER(this;
              ,'SaveInfo';
             )
    endwith
  endproc
  proc Calculate_NLEWDAIHEM()
    with this
          * --- Calcolo nWidth
          .w_nWidth = cp_LabelWidth(i_cGrdFontName, i_nGrdFontSize,ALLTRIM(.w_cTitle))+6
    endwith
  endproc
  proc Calculate_FKZMVQDBCX()
    with this
          * --- Controllo campi chiave
          CP_ASKCOLUMNTITLER(this;
              ,'CheckKey';
             )
    endwith
  endproc
  proc Calculate_OEECXSYHPT()
    with this
          * --- Controllo campi chiave
          CP_ASKCOLUMNTITLER(this;
              ,'CheckAllKey';
             )
    endwith
  endproc
  proc Calculate_JNREOAIBLY()
    with this
          * --- Se ci sono cambiamenti ai numero campi
          .oParentObject.Parent.Parent.Parent.bChangedFld = .t.
    endwith
  endproc
  proc Calculate_VGKZSQWCBU()
    with this
          * --- Permo esc e non ci sono cambiamenti ai numero campi
          .oParentObject.Parent.Parent.Parent.bChangedFld = .F.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.ocLabelFont_1_15.enabled = this.oPgFrm.Page1.oPag.ocLabelFont_1_15.mCond()
    this.oPgFrm.Page1.oPag.onDimFont_1_16.enabled = this.oPgFrm.Page1.oPag.onDimFont_1_16.mCond()
    this.oPgFrm.Page1.oPag.obItalicFont_1_17.enabled = this.oPgFrm.Page1.oPag.obItalicFont_1_17.mCond()
    this.oPgFrm.Page1.oPag.obBoldFont_1_18.enabled = this.oPgFrm.Page1.oPag.obBoldFont_1_18.mCond()
    this.oPgFrm.Page1.oPag.ocLabelFontC_1_21.enabled = this.oPgFrm.Page1.oPag.ocLabelFontC_1_21.mCond()
    this.oPgFrm.Page1.oPag.onDimFontC_1_22.enabled = this.oPgFrm.Page1.oPag.onDimFontC_1_22.mCond()
    this.oPgFrm.Page1.oPag.obItalicFontC_1_23.enabled = this.oPgFrm.Page1.oPag.obItalicFontC_1_23.mCond()
    this.oPgFrm.Page1.oPag.obBoldFontC_1_24.enabled = this.oPgFrm.Page1.oPag.obBoldFontC_1_24.mCond()
    this.oPgFrm.Page1.oPag.ocFormat_1_25.enabled = this.oPgFrm.Page1.oPag.ocFormat_1_25.mCond()
    this.oPgFrm.Page1.oPag.ocheck1_1_34.enabled = this.oPgFrm.Page1.oPag.ocheck1_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oApplyAllColumn_1_14.visible=!this.oPgFrm.Page1.oPag.oApplyAllColumn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oApplyAllColumnC_1_20.visible=!this.oPgFrm.Page1.oPag.oApplyAllColumnC_1_20.mHide()
    this.oPgFrm.Page1.oPag.onHeight_1_26.visible=!this.oPgFrm.Page1.oPag.onHeight_1_26.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomTitle.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_PVCVHNZBAV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZoomTitle row checked")
          .Calculate_NLEWDAIHEM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZoomTitle row unchecked")
          .Calculate_FKZMVQDBCX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ZoomTitle rowinvertselection") or lower(cEvent)==lower("ZoomTitle rowuncheckall")
          .Calculate_OEECXSYHPT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZoomTitle row checked") or lower(cEvent)==lower("w_ZoomTitle row unchecked") or lower(cEvent)==lower("ZoomTitle rowcheckall") or lower(cEvent)==lower("ZoomTitle rowinvertselection") or lower(cEvent)==lower("ZoomTitle rowuncheckall")
          .Calculate_JNREOAIBLY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_VGKZSQWCBU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.ocFld_1_10.value==this.w_cFld)
      this.oPgFrm.Page1.oPag.ocFld_1_10.value=this.w_cFld
    endif
    if not(this.oPgFrm.Page1.oPag.ocTitle_1_12.value==this.w_cTitle)
      this.oPgFrm.Page1.oPag.ocTitle_1_12.value=this.w_cTitle
    endif
    if not(this.oPgFrm.Page1.oPag.oFontGlobal_1_13.RadioValue()==this.w_FontGlobal)
      this.oPgFrm.Page1.oPag.oFontGlobal_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oApplyAllColumn_1_14.RadioValue()==this.w_ApplyAllColumn)
      this.oPgFrm.Page1.oPag.oApplyAllColumn_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ocLabelFont_1_15.RadioValue()==this.w_cLabelFont)
      this.oPgFrm.Page1.oPag.ocLabelFont_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.onDimFont_1_16.value==this.w_nDimFont)
      this.oPgFrm.Page1.oPag.onDimFont_1_16.value=this.w_nDimFont
    endif
    if not(this.oPgFrm.Page1.oPag.obItalicFont_1_17.RadioValue()==this.w_bItalicFont)
      this.oPgFrm.Page1.oPag.obItalicFont_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.obBoldFont_1_18.RadioValue()==this.w_bBoldFont)
      this.oPgFrm.Page1.oPag.obBoldFont_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFontGlobalC_1_19.RadioValue()==this.w_FontGlobalC)
      this.oPgFrm.Page1.oPag.oFontGlobalC_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oApplyAllColumnC_1_20.RadioValue()==this.w_ApplyAllColumnC)
      this.oPgFrm.Page1.oPag.oApplyAllColumnC_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ocLabelFontC_1_21.RadioValue()==this.w_cLabelFontC)
      this.oPgFrm.Page1.oPag.ocLabelFontC_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.onDimFontC_1_22.value==this.w_nDimFontC)
      this.oPgFrm.Page1.oPag.onDimFontC_1_22.value=this.w_nDimFontC
    endif
    if not(this.oPgFrm.Page1.oPag.obItalicFontC_1_23.RadioValue()==this.w_bItalicFontC)
      this.oPgFrm.Page1.oPag.obItalicFontC_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.obBoldFontC_1_24.RadioValue()==this.w_bBoldFontC)
      this.oPgFrm.Page1.oPag.obBoldFontC_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ocFormat_1_25.value==this.w_cFormat)
      this.oPgFrm.Page1.oPag.ocFormat_1_25.value=this.w_cFormat
    endif
    if not(this.oPgFrm.Page1.oPag.onHeight_1_26.value==this.w_nHeight)
      this.oPgFrm.Page1.oPag.onHeight_1_26.value=this.w_nHeight
    endif
    if not(this.oPgFrm.Page1.oPag.onWidth_1_27.value==this.w_nWidth)
      this.oPgFrm.Page1.oPag.onWidth_1_27.value=this.w_nWidth
    endif
    if not(this.oPgFrm.Page1.oPag.obIsImage_1_28.RadioValue()==this.w_bIsImage)
      this.oPgFrm.Page1.oPag.obIsImage_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ocForce_1_29.value==this.w_cForce)
      this.oPgFrm.Page1.oPag.ocForce_1_29.value=this.w_cForce
    endif
    if not(this.oPgFrm.Page1.oPag.ocBack_1_31.value==this.w_cBack)
      this.oPgFrm.Page1.oPag.ocBack_1_31.value=this.w_cBack
    endif
    if not(this.oPgFrm.Page1.oPag.ocheck1_1_34.RadioValue()==this.w_check1)
      this.oPgFrm.Page1.oPag.ocheck1_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ocZoomName_1_35.value==this.w_cZoomName)
      this.oPgFrm.Page1.oPag.ocZoomName_1_35.value=this.w_cZoomName
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_nDimFont))  and (NOT EMPTY(.w_cLabelFont))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onDimFont_1_16.SetFocus()
            i_bnoObbl = !empty(.w_nDimFont)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   (empty(.w_nDimFontC))  and (NOT EMPTY(.w_cLabelFontC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onDimFontC_1_22.SetFocus()
            i_bnoObbl = !empty(.w_nDimFontC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_cFld = this.w_cFld
    this.o_FontGlobal = this.w_FontGlobal
    this.o_FontGlobalC = this.w_FontGlobalC
    this.o_bIsImage = this.w_bIsImage
    return

enddefine

* --- Define pages as container
define class tcp_askcolumntitlePag1 as StdContainer
  Width  = 856
  height = 368
  stdWidth  = 856
  stdheight = 368
  resizeXpos=155
  resizeYpos=315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomTitle as cp_szoombox with uid="QEUEOWVVEB",left=-1, top=0, width=262,height=369,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",bRetriveAllRows=.t.,cZoomFile="cp_askcolumntitle",bOptions=.f.,bAdvOptions=.f.,cTable="cpazi",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",bQueryOnDblClick=.f.,bSearchFilterOnClick=.f.,bNoMenuHeaderProperty=.t.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 250953844

  add object ocFld_1_10 as StdField with uid="AVXMVIUVTF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_cFld", cQueryName = "cFld",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 68840332,;
   bGlobalFont=.t.,;
    Height=21, Width=448, Left=373, Top=8, InputMask=replicate('X',100)

  add object ocTitle_1_12 as StdField with uid="XVYKUBXXZH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_cTitle", cQueryName = "cTitle",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 149124212,;
   bGlobalFont=.t.,;
    Height=21, Width=448, Left=373, Top=36, InputMask=replicate('X',100)

  add object oFontGlobal_1_13 as StdCheck with uid="VNFKVBWLXE",rtseq=3,rtrep=.f.,left=373, top=62, caption="Default font etichetta",;
    HelpContextID = 202297597,;
    cFormVar="w_FontGlobal", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFontGlobal_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFontGlobal_1_13.GetRadio()
    this.Parent.oContained.w_FontGlobal = this.RadioValue()
    return .t.
  endfunc

  func oFontGlobal_1_13.SetRadio()
    this.Parent.oContained.w_FontGlobal=trim(this.Parent.oContained.w_FontGlobal)
    this.value = ;
      iif(this.Parent.oContained.w_FontGlobal=='S',1,;
      0)
  endfunc

  add object oApplyAllColumn_1_14 as StdCheck with uid="TINDFTTEGM",rtseq=4,rtrep=.f.,left=648, top=62, caption="Applica a tutte le colonne",;
    HelpContextID = 170871968,;
    cFormVar="w_ApplyAllColumn", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oApplyAllColumn_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oApplyAllColumn_1_14.GetRadio()
    this.Parent.oContained.w_ApplyAllColumn = this.RadioValue()
    return .t.
  endfunc

  func oApplyAllColumn_1_14.SetRadio()
    this.Parent.oContained.w_ApplyAllColumn=trim(this.Parent.oContained.w_ApplyAllColumn)
    this.value = ;
      iif(this.Parent.oContained.w_ApplyAllColumn=='S',1,;
      0)
  endfunc

  func oApplyAllColumn_1_14.mHide()
    with this.Parent.oContained
      return (recno(.w_ZoomTitle.cCursor)<>1)
    endwith
  endfunc


  add object ocLabelFont_1_15 as StdTableComboFont with uid="XEWFUYKPVZ",rtseq=5,rtrep=.f.,left=373,top=84,width=164,height=21;
    , HelpContextID = 255678038;
    , cFormVar="w_cLabelFont",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  func ocLabelFont_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_bIsImage<>'S' and not .w_FontGlobal='S')
    endwith
   endif
  endfunc

  add object onDimFont_1_16 as StdField with uid="KTPMHIBSVZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_nDimFont", cQueryName = "nDimFont",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    HelpContextID = 227375326,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=648, Top=84, cSayPict='"99"', cGetPict='"99"'

  func onDimFont_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_cLabelFont))
    endwith
   endif
  endfunc

  add object obItalicFont_1_17 as StdCheck with uid="ZVWDSPGSBR",rtseq=7,rtrep=.f.,left=716, top=84, caption="Italic",;
    HelpContextID = 130476233,;
    cFormVar="w_bItalicFont", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func obItalicFont_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func obItalicFont_1_17.GetRadio()
    this.Parent.oContained.w_bItalicFont = this.RadioValue()
    return .t.
  endfunc

  func obItalicFont_1_17.SetRadio()
    this.Parent.oContained.w_bItalicFont=trim(this.Parent.oContained.w_bItalicFont)
    this.value = ;
      iif(this.Parent.oContained.w_bItalicFont=='S',1,;
      0)
  endfunc

  func obItalicFont_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_cLabelFont))
    endwith
   endif
  endfunc

  add object obBoldFont_1_18 as StdCheck with uid="YLOPRWXCNT",rtseq=8,rtrep=.f.,left=774, top=84, caption="Bold",;
    HelpContextID = 6868675,;
    cFormVar="w_bBoldFont", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func obBoldFont_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func obBoldFont_1_18.GetRadio()
    this.Parent.oContained.w_bBoldFont = this.RadioValue()
    return .t.
  endfunc

  func obBoldFont_1_18.SetRadio()
    this.Parent.oContained.w_bBoldFont=trim(this.Parent.oContained.w_bBoldFont)
    this.value = ;
      iif(this.Parent.oContained.w_bBoldFont=='S',1,;
      0)
  endfunc

  func obBoldFont_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_cLabelFont))
    endwith
   endif
  endfunc

  add object oFontGlobalC_1_19 as StdCheck with uid="NJMIZQEHQH",rtseq=9,rtrep=.f.,left=374, top=111, caption="Default font colonna",;
    HelpContextID = 197906685,;
    cFormVar="w_FontGlobalC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFontGlobalC_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFontGlobalC_1_19.GetRadio()
    this.Parent.oContained.w_FontGlobalC = this.RadioValue()
    return .t.
  endfunc

  func oFontGlobalC_1_19.SetRadio()
    this.Parent.oContained.w_FontGlobalC=trim(this.Parent.oContained.w_FontGlobalC)
    this.value = ;
      iif(this.Parent.oContained.w_FontGlobalC=='S',1,;
      0)
  endfunc

  add object oApplyAllColumnC_1_20 as StdCheck with uid="TKOGAUGUUJ",rtseq=10,rtrep=.f.,left=648, top=111, caption="Applica a tutte le colonne",;
    HelpContextID = 170872035,;
    cFormVar="w_ApplyAllColumnC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oApplyAllColumnC_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oApplyAllColumnC_1_20.GetRadio()
    this.Parent.oContained.w_ApplyAllColumnC = this.RadioValue()
    return .t.
  endfunc

  func oApplyAllColumnC_1_20.SetRadio()
    this.Parent.oContained.w_ApplyAllColumnC=trim(this.Parent.oContained.w_ApplyAllColumnC)
    this.value = ;
      iif(this.Parent.oContained.w_ApplyAllColumnC=='S',1,;
      0)
  endfunc

  func oApplyAllColumnC_1_20.mHide()
    with this.Parent.oContained
      return (recno(.w_ZoomTitle.cCursor)<>1)
    endwith
  endfunc


  add object ocLabelFontC_1_21 as StdTableComboFont with uid="SPCDPACTCI",rtseq=11,rtrep=.f.,left=374,top=133,width=164,height=22;
    , HelpContextID = 251287126;
    , cFormVar="w_cLabelFontC",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  func ocLabelFontC_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_bIsImage<>'S' and not .w_FontGlobalC='S')
    endwith
   endif
  endfunc

  add object onDimFontC_1_22 as StdField with uid="ICLTNLKVFW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_nDimFontC", cQueryName = "nDimFontC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    HelpContextID = 227358174,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=649, Top=133, cSayPict='"99"', cGetPict='"99"'

  func onDimFontC_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_cLabelFontC))
    endwith
   endif
  endfunc

  add object obItalicFontC_1_23 as StdCheck with uid="EDKFASAYGO",rtseq=13,rtrep=.f.,left=717, top=133, caption="Italic",;
    HelpContextID = 60221641,;
    cFormVar="w_bItalicFontC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func obItalicFontC_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func obItalicFontC_1_23.GetRadio()
    this.Parent.oContained.w_bItalicFontC = this.RadioValue()
    return .t.
  endfunc

  func obItalicFontC_1_23.SetRadio()
    this.Parent.oContained.w_bItalicFontC=trim(this.Parent.oContained.w_bItalicFontC)
    this.value = ;
      iif(this.Parent.oContained.w_bItalicFontC=='S',1,;
      0)
  endfunc

  func obItalicFontC_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_cLabelFontC))
    endwith
   endif
  endfunc

  add object obBoldFontC_1_24 as StdCheck with uid="KCQNAJELNY",rtseq=14,rtrep=.f.,left=775, top=133, caption="Bold",;
    HelpContextID = 7143107,;
    cFormVar="w_bBoldFontC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func obBoldFontC_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func obBoldFontC_1_24.GetRadio()
    this.Parent.oContained.w_bBoldFontC = this.RadioValue()
    return .t.
  endfunc

  func obBoldFontC_1_24.SetRadio()
    this.Parent.oContained.w_bBoldFontC=trim(this.Parent.oContained.w_bBoldFontC)
    this.value = ;
      iif(this.Parent.oContained.w_bBoldFontC=='S',1,;
      0)
  endfunc

  func obBoldFontC_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_cLabelFontC))
    endwith
   endif
  endfunc

  add object ocFormat_1_25 as StdField with uid="DCGQVOPDKW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_cFormat", cQueryName = "cFormat",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 164140264,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=373, Top=163, InputMask=replicate('X',20)

  func ocFormat_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_bIsImage<>'S')
    endwith
   endif
  endfunc

  add object onHeight_1_26 as StdField with uid="WNSSERIEPS",rtseq=16,rtrep=.f.,;
    cFormVar = "w_nHeight", cQueryName = "nHeight",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 215040024,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=517, Top=163, cSayPict='"999"', cGetPict='"999"'

  func onHeight_1_26.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object onWidth_1_27 as StdField with uid="PMLOMUSTZK",rtseq=17,rtrep=.f.,;
    cFormVar = "w_nWidth", cQueryName = "nWidth",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 1855628,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=648, Top=163, cSayPict='"999"', cGetPict='"999"'

  add object obIsImage_1_28 as StdCheck with uid="RSYPRXQLHK",rtseq=18,rtrep=.f.,left=373, top=187, caption="Immagine",;
    HelpContextID = 121424427,;
    cFormVar="w_bIsImage", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func obIsImage_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func obIsImage_1_28.GetRadio()
    this.Parent.oContained.w_bIsImage = this.RadioValue()
    return .t.
  endfunc

  func obIsImage_1_28.SetRadio()
    this.Parent.oContained.w_bIsImage=trim(this.Parent.oContained.w_bIsImage)
    this.value = ;
      iif(this.Parent.oContained.w_bIsImage=='S',1,;
      0)
  endfunc

  add object ocForce_1_29 as StdField with uid="XFALLRQBIH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_cForce", cQueryName = "cForce",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 264803444,;
   bGlobalFont=.t.,;
    Height=21, Width=448, Left=373, Top=208, InputMask=replicate('X',200)


  add object oBtn_1_30 as StdButton with uid="YDCPYGSYKS",left=826, top=208, width=22,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore";
    , HelpContextID = 177873036;
  , bGlobalFont=.t.

    proc oBtn_1_30.Click()
      with this.Parent.oContained
        .w_cForce=alltrim(str(Getcolor()))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object ocBack_1_31 as StdField with uid="HCUMBYNOVF",rtseq=20,rtrep=.f.,;
    cFormVar = "w_cBack", cQueryName = "cBack",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 113923188,;
   bGlobalFont=.t.,;
    Height=21, Width=448, Left=373, Top=234, InputMask=replicate('X',200)


  add object oBtn_1_32 as StdButton with uid="BJVWNBZGMA",left=826, top=234, width=22,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore";
    , HelpContextID = 177873036;
  , bGlobalFont=.t.

    proc oBtn_1_32.Click()
      with this.Parent.oContained
        .w_cBack=alltrim(str(Getcolor()))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object ocheck1_1_34 as StdCheck with uid="GNABPSJLYB",rtseq=21,rtrep=.f.,left=373, top=258, caption="Editabile (in zoom editabile)",;
    HelpContextID = 114340980,;
    cFormVar="w_check1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func ocheck1_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ocheck1_1_34.GetRadio()
    this.Parent.oContained.w_check1 = this.RadioValue()
    return .t.
  endfunc

  func ocheck1_1_34.SetRadio()
    this.Parent.oContained.w_check1=trim(this.Parent.oContained.w_check1)
    this.value = ;
      iif(this.Parent.oContained.w_check1=='S',1,;
      0)
  endfunc

  func ocheck1_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_bIsImage<>'S')
    endwith
   endif
  endfunc

  add object ocZoomName_1_35 as StdField with uid="AABHJPJMUN",rtseq=22,rtrep=.f.,;
    cFormVar = "w_cZoomName", cQueryName = "cZoomName",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 107331419,;
   bGlobalFont=.t.,;
    Height=21, Width=448, Left=373, Top=292, InputMask=replicate('X',200)


  add object oBtn_1_41 as StdButton with uid="GPQYTBCQYS",left=749, top=318, width=48,height=45,;
    CpPicture="bmp\save.bmp", caption="", nPag=1;
    , ToolTipText = "Save";
    , HelpContextID = 67029900;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        CP_ASKCOLUMNTITLER(this.Parent.oContained,"SaveAll")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_42 as StdButton with uid="CZAXUUBTZR",left=800, top=318, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 254204812;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="ZNRAHIDANQ",Visible=.t., Left=326, Top=10,;
    Alignment=1, Width=44, Height=18,;
    Caption="Campo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="YLEAOQGHXS",Visible=.t., Left=326, Top=38,;
    Alignment=1, Width=44, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="VGEAJGQKCN",Visible=.t., Left=321, Top=165,;
    Alignment=1, Width=49, Height=18,;
    Caption="Formato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="AHLFZYLKFT",Visible=.t., Left=471, Top=165,;
    Alignment=1, Width=44, Height=18,;
    Caption="Altezza:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="BPOHDCLRRZ",Visible=.t., Left=567, Top=165,;
    Alignment=1, Width=80, Height=18,;
    Caption="Larghezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="CQJUBIMMZJ",Visible=.t., Left=263, Top=210,;
    Alignment=1, Width=107, Height=18,;
    Caption="Colore del testo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="DMBPPRXTQZ",Visible=.t., Left=263, Top=236,;
    Alignment=1, Width=107, Height=18,;
    Caption="Colore di sfondo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="PMYRVTOJCS",Visible=.t., Left=300, Top=294,;
    Alignment=1, Width=70, Height=18,;
    Caption="Zoom name:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="EYUYQDCQFF",Visible=.t., Left=546, Top=86,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="ADMMUSPOPS",Visible=.t., Left=270, Top=86,;
    Alignment=1, Width=100, Height=18,;
    Caption="Font etichette:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="WYNARBIEDX",Visible=.t., Left=547, Top=135,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DQSUDAZGGE",Visible=.t., Left=271, Top=135,;
    Alignment=1, Width=100, Height=18,;
    Caption="Font colonna:"  ;
  , bGlobalFont=.t.

  add object oBox_1_53 as StdBox with uid="MEICGNBQMJ",left=263, top=2, width=589,height=59

  add object oBox_1_54 as StdBox with uid="UIQDIHMUKL",left=263, top=59, width=589,height=51

  add object oBox_1_55 as StdBox with uid="HGIXUFMFSP",left=263, top=108, width=589,height=51

  add object oBox_1_56 as StdBox with uid="WQKSWAYFBB",left=263, top=157, width=589,height=129
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_askcolumntitle','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_askcolumntitle
define class StdTableComboFont as StdTableCombo
  proc Init
  	IF VARTYPE(this.bNoBackColor)='U'
		  This.backcolor = i_nEBackColor
	  ENDIF
    this.ToolTipText=cp_Translate(this.ToolTipText)
    local l_numf, l_i
    AFONT(this.combovalues)
    This.nValues = ALEN(this.combovalues)
    l_i = 1
    do while l_i <= This.nValues
        this.AddItem(this.combovalues[l_i])
        l_i = l_i + 1
    enddo
    This.SetFont()
  endproc
enddefine
* --- Fine Area Manuale
