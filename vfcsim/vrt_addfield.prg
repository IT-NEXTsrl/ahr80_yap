* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: VRT_ADDFIELD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* procedura per aggiungere campi al runtime
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Local i_oForm
* -- Zucchetti Aulla inizio monitor framework, solo il developer pu� aggiungere campi
*IF cp_IsAdministrator(.t.)
If cp_set_get_right()=5
    * -- Zucchetti Aulla fine monitor framework
    If Type("i_curform")='O' And !Isnull(i_curform)
        If Type('i_CurForm.cPrg')<>'C' Or cp_getEntityType(i_curform.cPrg)="Dialog Window"
            CP_MSG(cp_Translate(MSG_CANNOT_ADD_FIELDS_ON_THIS_MASK),.F.)
        Else
            Local i_class
            i_class=vrtClass(i_curform)
            If Inlist(i_class,"Stdform","Stdpcform","Stdtrsform")
                Do Case
                    Case Inlist(i_class,"Stdform","Stdtrsform")
                        i_oForm=i_curform
                    Case i_class=="Stdpcform"
                        i_oForm=i_curform.Cnt
                Endcase
                Local ooo
                Createobject('AddFieldForm',i_oForm)
            Else
                CP_MSG(cp_Translate(MSG_CANNOT_ADD_FIELDS_ON_THIS_MASK),.F.)
            Endif
        Endif
    Else
        *wait window "No form selected"
    Endif
Else
    * -- Zucchetti Aulla inizio monitor framework, solo il developer pu� aggiungere campi
    cp_ErrorMsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
    *CP_MSG(cp_Translate(MSG_FUNCTION_FOR_ADMINISTRATOR),.F.)
    * -- Zucchetti Aulla fine monitor framework, solo il developer pu� aggiungere campi

Endif
Return

* --- Zucchetti Aulla inizio - Monitor framework
Procedure AddFieldsTable(cTable)
    Createobject("AddFieldFormMonitor", cTable)
Endproc

Define Class AddFieldFormMonitor As AddFieldForm
    Procedure Init(cTable)
        This.oThis=This
        This.tblname=cTable
        This.FillFields()
        This.Caption="Add fields to table "+This.tblname
        This.Show()
Enddefine
* --- Zucchetti Aulla fine - Monitor framework

Define Class AddFieldForm As CPSysform
    Top=100
    Left=100
    Width=400
    Height=400
    Caption="Add fields"
    oThis=.Null.
    tblname=''
    Add Object fldlst As ListBox With Top=5,Left=5,Width=390,Height=368,ColumnCount=5,ColumnWidths="120,20,20,20",columntitles="Name,Type,Len,Dec"
    Add Object addbtn As cpcommandbutton With Caption=MSG_ADD,Top=375,Left=5,Width=60,Height=23
    Add Object delbtn As cpcommandbutton With Caption=MSG_DELETE,Top=375,Left=70,Width=50,Height=23
    Add Object okbtn  As cpcommandbutton With Caption=MSG_OK,Top=375,Left=310,Width=30,Height=23
    Add Object cancelbtn  As cpcommandbutton With Caption=MSG_CANCEL,Top=375,Left=345,Width=50,Height=23
    Procedure Init(i_oForm)
        This.oThis=This
        This.tblname=i_oForm.cFile
        This.FillFields()
        This.Caption="Add fields to table "+This.tblname
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
        This.Show()
    Procedure Resize()
        This.fldlst.Height=This.Height-30
        This.fldlst.Width=This.Width-10
        This.addbtn.Top=This.Height-23
        This.delbtn.Top=This.Height-23
        This.okbtn.Top=This.Height-23
        This.okbtn.Left=This.Width-90
        This.cancelbtn.Top=This.Height-23
        This.cancelbtn.Left=This.Width-55
        Return
    Procedure Destroy()
        This.oThis=.Null.
        Return
    Procedure okbtn.Click()
        Thisform.SaveFile()
        Thisform.oThis=.Null.
        Return
    Procedure cancelbtn.Click()
        Thisform.oThis=.Null.
        Return
    Procedure FillFields()
        Local i_i,i_ntb,i_c,i_t,i_l,i_d,i_x,i_tblname
        i_tblname=This.tblname
        cp_LoadExtDict()
        i_ntb=i_extdict.GetFieldsCount(i_tblname)
        If i_ntb<>0
            For i_i=1 To i_ntb
                i_c=i_extdict.GetFieldName(i_tblname,i_i)
                i_t=i_extdict.GetFieldType(i_tblname,i_i)
                i_l=i_extdict.GetFieldLen(i_tblname,i_i)
                i_d=i_extdict.GetFieldDec(i_tblname,i_i)
                i_x=i_extdict.GetFieldDescr(i_tblname,i_i)
                This.fldlst.AddItem(i_c)
                This.fldlst.ListItem(This.fldlst.ListCount,2)=i_t
                This.fldlst.ListItem(This.fldlst.ListCount,3)=Alltrim(Str(i_l))
                This.fldlst.ListItem(This.fldlst.ListCount,4)=Alltrim(Str(i_d))
                This.fldlst.ListItem(This.fldlst.ListCount,5)=i_x
            Next
        Endif
        Return
    Procedure delbtn.Click()
        Local i_i
        For i_i=Thisform.fldlst.ListCount To 1 Step -1
            If Thisform.fldlst.Selected(i_i)
                Thisform.fldlst.RemoveItem(i_i)
            Endif
        Next
        Return
    Procedure fldlst.DblClick()
        Local i_i,i_j
        i_j=0
        For i_i=1 To This.ListCount
            If This.Selected(i_i)
                i_j=i_i
            Endif
        Next
        If i_j<>0
            Local i_ooo
            i_ooo=Createobject('EditField',This,i_j)
            i_ooo.Show()
        Endif
        Return
    Procedure addbtn.Click()
        Local i_j,i_ooo
        Thisform.fldlst.AddItem('newfld')
        i_j=Thisform.fldlst.ListCount
        Thisform.fldlst.ListItem(i_j,2)='C'
        Thisform.fldlst.ListItem(i_j,3)='10'
        Thisform.fldlst.ListItem(i_j,4)='0'
        Thisform.fldlst.ListItem(i_j,5)='New Field'
        i_ooo=Createobject('EditField',Thisform.fldlst,i_j)
        i_ooo.Show()
        If Thisform.fldlst.ListItem(i_j,1)='newfld'
            Thisform.fldlst.RemoveItem(i_j)
        Else
            Thisform.fldlst.Selected(i_j)=.T.
        Endif
        Return
    Procedure InsertFields(i_wh)
        Local i_i,i_n
        i_n=This.fldlst.ListCount
        Fputs(i_wh,' '+Alltrim(Str(i_n)))
        For i_i=1 To i_n
            Fputs(i_wh,'"'+Trim(This.fldlst.ListItem(i_i,1))+'"')
            Fputs(i_wh,'"'+This.fldlst.ListItem(i_i,2)+'"')
            Fputs(i_wh,' '+This.fldlst.ListItem(i_i,3))
            Fputs(i_wh,' '+This.fldlst.ListItem(i_i,4))
            Fputs(i_wh,'"'+Trim(This.fldlst.ListItem(i_i,5))+'"')
            Fputs(i_wh,'""')
            Fputs(i_wh,'""')
            Fputs(i_wh,'.F.')
            Fputs(i_wh,'*---*')
        Next
        Return
    Procedure SaveFile()
        * if new file
        *   new header with 1 file
        *   insert table definitions
        *   insert new fields
        *   insert end table definitions
        * else
        *   if new table
        *     skip header from original
        *     new header with files+1
        *     copy original to endoffile
        *     insert table definitions
        *     insert new fields
        *     insert end table definitions
        *   else
        *     skip header from original
        *     new header with same number of files
        *     copy original to table position
        *     copy table definitions
        *     skip fields
        *     insert new fields
        *     copy original to endoffile
        *   end
        * end
        Local i_rh,i_wh,i_nf,i_bNewFile,Tab_ass,Nloop,TableName,NameFile
        * Zucchetti Aulla inizio aggiunta campi tabella associata
        Nloop=1
        If cp_ExistTableDef('TAB_RUNT')
            Cur_Tab =READTABLE('TAB_RUNT',"RTTABASS",,,,"RTCODTAB= '" + Alltrim(This.tblname)+ "'")
            If Used((Cur_Tab))
                Select((Cur_Tab))
                Tab_ass = Nvl(&Cur_Tab..RTTABASS,Space(30))
                If Not Empty(Tab_ass) And cp_ExistTableDef(Alltrim(Tab_ass))
                    Nloop=2
                Endif
                Select((Cur_Tab))
                Use In  (Cur_Tab)
            Endif
        Endif
        For i=1 To Nloop
            * --- Framework monitor
            If i_monitorframework
                i_rh=Fopen(Alltrim(i_usrpath)+'cp_added_fields.xdc')
            Else
                i_rh=Fopen('cp_added_fields.xdc')
            Endif
            i_wh=Fcreate('cp_new.$$$')
            If i_wh<>-1
                i_bNewFile=(i_rh=-1)
                * Zucchetti Aulla inizio aggiunta campi tabella associata
                If i=1
                    TableName=This.tblname
                Else
                    TableName=Tab_ass
                Endif
                * Zucchetti Aulla fine aggiunta campi tabella associata
                If i_bNewFile Or i_extdict.GetTableIdx(TableName)=0
                    If i_bNewFile
                        i_nf=0
                    Else
                        i_nf=This.SkipHeader(i_rh)
                    Endif
                    This.NewHeader(i_wh,i_nf+1)
                    If !i_bNewFile
                        This.CopyToEndOfFile(i_rh,i_wh)
                    Endif
                    This.InsertTableDef(i_wh,TableName)
                    This.InsertFields(i_wh)
                    This.InsertCloseTableDefs(i_wh,TableName)
                Else
                    i_nf=This.SkipHeader(i_rh)
                    This.NewHeader(i_wh,i_nf)
                    This.CopyToTable(i_rh,i_wh,TableName)
                    This.CopyTableDefinitions(i_rh,i_wh,TableName)
                    This.SkipFields(i_rh)
                    This.InsertFields(i_wh)
                    This.CopyCloseTableDefs(i_rh,i_wh)
                    This.CopyToEndOfFile(i_rh,i_wh)
                Endif
                Fclose(i_rh)
                Fclose(i_wh)
                * cancella il vecchio e rinomina il nuovo
                * --- Framework monitor
                If i_monitorframework
                    Delete File Alltrim(i_usrpath)+'cp_added_fields.xdc'
                    Rename cp_new.$$$ To Alltrim(i_usrpath)+'cp_added_fields.xdc'
                Else
                    Delete File cp_added_fields.xdc
                    Rename cp_new.$$$ To cp_added_fields.xdc
                Endif
                If i=Nloop
                    CP_MSG(cp_Translate(MSG_NEEDED_RESTART_PROCEDURE),.F.)
                Endif
            Else
                * chiude l' eventuale file lasciato aperto
                If i_rh<>-1
                    Fclose(i_rh)
                Endif
                If i_wh<>-1
                    Fclose(i_wh)
                Endif
                CP_MSG(cp_Translate(MSG_ERROR_WRITING_FILE_F_CANNOT_SAVE))
            Endif
            * Zucchetti Aulla inizio aggiunta campi tabella associata
        Endfor
        * Zucchetti Aulla fine aggiunta campi tabella associata
        Return
    Function SkipHeader(i_rh)
        Local i_nFiles
        i_nFiles=0
        Fgets(i_rh)
        Fgets(i_rh)
        i_nFiles=Val(Fgets(i_rh))
        Return i_nFiles
    Procedure NewHeader(i_wh,i_nFiles)
        Fputs(i_wh,"* --- Code Painter Extended Dictionary")
        Fputs(i_wh,'"'+Ttoc(Datetime(),1)+'"')
        Fputs(i_wh," " +Alltrim(Str(i_nFiles)))
        Return
    Procedure CopyToEndOfFile(i_rh,i_wh)
        Local i_s
        Do While !Feof(i_rh)
            i_s=Fgets(i_rh)
            Fputs(i_wh,i_s)
        Enddo
        Return
    Procedure CopyToTable(i_rh,i_wh,i_cTable)
        Local i_s,i_stop
        i_stop=.F.
        Do While !i_stop And !Feof(i_rh)
            i_s=Fgets(i_rh)
            If i_s='"'+i_cTable+'"'
                i_stop=.T.
            Else
                Fputs(i_wh,i_s)
            Endif
        Enddo
        Return
    Procedure CopyTableDefinitions(i_rh,i_wh,i_cTable)
        Local i_s,i_i
        Fputs(i_wh,'"'+i_cTable+'"')
        For i_i=1 To 4
            i_s=Fgets(i_rh)
            Fputs(i_wh,i_s)
        Next
        Return
    Procedure SkipFields(i_rh)
        Local i_nf,i_i,i_j
        i_nf=Val(Fgets(i_rh))
        For i_i=1 To i_nf
            For i_j=1 To 9
                Fgets(i_rh)
            Next
        Next
        Return
    Procedure InsertTableDef(i_wh,i_cTable)
        Fputs(i_wh,'"'+i_cTable+'"')
        Fputs(i_wh,'.F.')
        Fputs(i_wh,'.F.')
        Fputs(i_wh,'.F.')
        Fputs(i_wh,'"'+i_cTable+'"')
        Return
    Procedure InsertCloseTableDefs(i_wh,i_cTable)
        Fputs(i_wh,' 0')
        Fputs(i_wh,' 0')
        Fputs(i_wh,'"User added fields for table '+i_cTable+'"')
        Fputs(i_wh,'"'+Ttoc(Datetime(),1)+'"')
        Fputs(i_wh,'""')
        Fputs(i_wh,'.F.')
        Fputs(i_wh,'.F.')
        Fputs(i_wh,'.F.')
        Fputs(i_wh,'.F.')
        Fputs(i_wh,'"xxx"')
        Fputs(i_wh,'"M"')
        Fputs(i_wh,'*---*')
        Return
    Procedure CopyCloseTableDefs(i_rh,i_wh)
        Local i_i,i_s
        For i_i=1 To 3
            i_s=Fgets(i_rh)
            Fputs(i_wh,i_s)
        Next
        Fgets(i_rh)
        Fputs(i_wh,'"'+Ttoc(Datetime(),1)+'"')
        For i_i=1 To 8
            i_s=Fgets(i_rh)
            Fputs(i_wh,i_s)
        Next
        Return
Enddefine

Define Class EditField As cpsysform
    Width=370
    Height=175
    Top=150
    Left=150
    Caption="Edit Field"
    WindowType=1
    nRow=0
    oList=.Null.
    Add Object l1 As cplabel With Caption='Name:',Top=8,Left=5,Width=50,Alignment=1,BackStyle=0
    Add Object l2 As cplabel With Caption='Type:',Top=8+28,Left=5,Width=50,Alignment=1,BackStyle=0
    Add Object l3 As cplabel With Caption='Len.:',Top=8+28*2,Left=5,Width=50,Alignment=1,BackStyle=0
    Add Object l4 As cplabel With Caption='Dec.:',Top=8+28*3,Left=5,Width=50,Alignment=1,BackStyle=0
    Add Object l5 As cplabel With Caption='Descr:',Top=8+28*4,Left=5,Width=50,Alignment=1,BackStyle=0
    Add Object txtname As cptextbox With Top=5,Left=60,Width=100,Height=23,Format='K'
    Add Object cmbtype As ComboBox With Top=5+28,Left=60,Width=100,Height=23
    Add Object txtlen As cptextbox With Top=5+28*2,Left=60,Width=30,Height=23,InputMask='999',Format='K'
    Add Object txtdec As cptextbox With Top=5+28*3,Left=60,Width=30,Height=23,InputMask='99',Format='K'
    Add Object txtdescr As cptextbox With Top=5+28*4,Left=60,Width=300,Height=23,Format='K'
    Add Object btnok As cpcommandbutton With Top=5+28*5,Left=370-90,Width=30,Height=23,Caption=MSG_OK
    Add Object btncancel As cpcommandbutton With Top=5+28*5,Left=370-55,Width=50,Height=23,Caption=MSG_CANCEL
    Procedure Init(i_oList,i_nRow)
        This.oList=i_oList
        This.nRow=i_nRow
        This.cmbtype.AddItem("Char")
        This.cmbtype.AddItem("Numeric")
        This.cmbtype.AddItem("Date")
        This.cmbtype.AddItem("DateTime")
        This.cmbtype.AddItem("Logic")
        This.cmbtype.AddItem("Memo")
        This.GetValues()
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
        Return
    Procedure GetValues()
        This.txtname.Value=This.oList.List(This.nRow,1)
        Local i_n
        i_n=Left(This.oList.List(This.nRow,2),1)
        Do Case
            Case i_n='C'
                i_n='Char'
            Case i_n='M'
                i_n='Memo'
            Case i_n='N'
                i_n='Numeric'
            Case i_n='T'
                i_n='DateTime'
            Case i_n='D'
                i_n='Date'
            Otherwise
                i_n='Logic'
        Endcase
        This.cmbtype.Value=i_n
        This.txtlen.Value=Val(This.oList.List(This.nRow,3))
        This.txtdec.Value=Val(This.oList.List(This.nRow,4))
        This.txtdescr.Value=This.oList.List(This.nRow,5)
        Return
    Procedure SetValues()
        This.oList.List(This.nRow,1)=This.txtname.Value
        Local i_n
        i_n=This.cmbtype.Value
        If i_n='DateTime'
            i_n='T'
        Endif
        This.oList.List(This.nRow,2)=Left(i_n,1)
        This.oList.List(This.nRow,3)=Alltrim(Str(This.txtlen.Value))
        If i_n='N'
            This.oList.List(This.nRow,4)=Alltrim(Str(This.txtdec.Value))
        Else
            This.oList.List(This.nRow,4)='0'
        Endif
        This.oList.List(This.nRow,5)=This.txtdescr.Value
        Return
    Procedure btncancel.Click()
        Thisform.Hide()
        Return
    Procedure btnok.Click()
        Thisform.SetValues()
        Thisform.Hide()
        Return
Enddefine
