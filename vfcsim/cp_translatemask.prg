* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_translatemask                                                *
*              Traduzione del dato                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-30                                                      *
* Last revis.: 2012-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_translatemask",oParentObject))

* --- Class definition
define class tcp_translatemask as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 178
  Height = 92
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-02-23"
  HelpContextID=32673569
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_translatemask"
  cComment = "Traduzione del dato"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NameTable = space(200)
  w_VARIABLE = space(50)
  w_cWhere = space(200)
  w_CPCCCHK = space(10)
  * --- Area Manuale = Declare Variables
  * --- cp_translatemask
  dimension Ctrls[(alen(i_aCpLangs)+1)*2]
  nY=5
  autocenter=.t.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_translatemaskPag1","cp_translatemask",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_translatemask
    * --- Translate interface...
     this.parent.cComment=cp_Translate(MSG_TRANSLATIONS_DATA)
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NameTable=space(200)
      .w_VARIABLE=space(50)
      .w_cWhere=space(200)
      .w_CPCCCHK=space(10)
        .w_NameTable = iif(cp_getEntityType(this.oparentobject.parent.ocontained.cprg)="Master/Detail" and this.oParentObject.nPag=2, this.oparentobject.parent.ocontained.cFileDetail, this.oparentobject.parent.ocontained.cFile)
        .w_VARIABLE = substr( this.oParentObject.cFormvar, 3 )
        .w_cWhere = iif("Detail"$cp_getEntityType(this.oparentobject.parent.ocontained.cprg) and this.oParentObject.nPag=2, this.oParentObject.parent.oContained.GetcKeyDetailWhereODBC(), this.oParentObject.parent.oContained.GetcKeyWhereODBC())
    endwith
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_translatemask
      endproc
      proc Init(oParentObject)
        private nObj
        if type("oParentObject")<>'L'
          this.oParentObject=oParentObject
        endif
        m.nObj=alen(i_aCpLangs)+1
        Dimension This.Ctrls[(alen(i_aCpLangs)+1)*2]    
        for i=1 to m.nObj
           *** aggiungo label
           this.addobject('this.Ctrls[i*2-1]','StdString')
           if i=1
             this.Ctrls[i*2-1].caption=alltrim(cp_Translate(MSG_DEFAULT_VALUE+MSG_FS))       
           else
             this.Ctrls[i*2-1].caption=alltrim(looktab("cplangs","name","code",trim(i_aCpLangs(i-1))))
             if empty(this.Ctrls[i*2-1].caption)
                this.Ctrls[i*2-1].caption=trim(i_aCpLangs(i-1))+':'
             else 
                this.Ctrls[i*2-1].caption=this.Ctrls[i*2-1].caption+':'
             endif
           endif
           this.Ctrls[i*2-1].Move(5,iif(i=1,10,25)+((max(this.oParentObject.height,23)+5)*(i-1)))
           this.Ctrls[i*2-1].width=120
           this.Ctrls[i*2-1].fontbold=.f.
           this.Ctrls[i*2-1].alignment=1
           this.Ctrls[i*2-1].visible=.t.       
           *** aggiungo text box
           this.Addobject('this.Ctrls[i*2]','textbox')
           this.Ctrls[i*2].Move(130,iif(i=1,10,25)+((max(this.oParentObject.height,23)+5)*(i-1)))
           this.Ctrls[i*2].fontbold=.f.
           this.Ctrls[i*2].width=this.oParentObject.width
           this.Ctrls[i*2].height=max(this.oParentObject.height,23)
           this.Ctrls[i*2].InputMask=this.oParentObject.InputMask
           this.Ctrls[i*2].value=this.oParentObject.value
           this.Ctrls[i*2].FontName=this.oParentObject.FontName
           this.Ctrls[i*2].FontSize=this.oParentObject.FontSize
           this.Ctrls[i*2].FontBold=this.oParentObject.FontBold       
           this.Ctrls[i*2].FontItalic=this.oParentObject.FontItalic       
           this.Ctrls[i*2].FontUnderline=this.oParentObject.FontUnderline 
           this.Ctrls[i*2].FontStrikeThru=this.oParentObject.FontStrikeThru
           this.Ctrls[i*2].visible=.t.  
           if UPPER(this.oparentobject.parent.ocontained.cFunction)=UPPER('Edit') and iif(i=1,g_PROJECTLANGUAGE,i_aCpLangs(i-1))=iif(empty(i_cLanguageData),g_PROJECTLANGUAGE,i_cLanguageData)
             this.Ctrls[i*2].enabled=.f.
           endif
        endfor
        this.height=this.height+((max(this.oParentObject.height,23)+10)*(m.nObj-1))
        this.oPgFrm.height=this.height
        this.width=this.width+this.oParentObject.width
        this.oPgFrm.width=this.width
        
        DoDefault()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_GKMFCTJJIV()
    with this
          * --- Lettura campi multilingua
          cp_ReadMult(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_GKMFCTJJIV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_translatemaskPag1 as StdContainer
  Width  = 174
  height = 92
  stdWidth  = 174
  stdheight = 92
  resizeXpos=30
  resizeYpos=60
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="YCTTRQWDEB",left=34, top=65, width=63,height=23,;
    caption="Salva", nPag=1;
    , HelpContextID = 191195282;
    , Caption=MSG_S_SAVE;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        do cp_WriteMult with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_2 as StdButton with uid="TAPXNQTUEA",left=101, top=65, width=63,height=23,;
    caption="Esci", nPag=1;
    , HelpContextID = 25356146;
    , Caption=MSG_CANCEL_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_translatemask','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
