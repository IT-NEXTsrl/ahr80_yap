* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_mngdatabase_b                                                *
*              Gestione tabelle/database                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-10-11                                                      *
* Last revis.: 2015-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_mngdatabase_b",oParentObject,m.pParam)
return(i_retval)

define class tcp_mngdatabase_b as StdBatch
  * --- Local variables
  pParam = space(20)
  w_COUNT = 0
  w_PH = space(10)
  w_OK = .f.
  w_STRING = space(30)
  w_TYPEDB = .f.
  w_FINDDS = .f.
  w_COUNTDB = 0
  w_COUNT_TYPEDB = 0
  w_SRVDESC = space(200)
  w_SRVODBC = space(30)
  w_SRVTYPE = space(30)
  w_SRVCONN = 0
  w_SRVPOSTIT = space(1)
  w_FILE = 0
  w_SRVNAME = space(10)
  w_NAMETABLE = space(10)
  w_BFOUNDTABLE = .f.
  w_NTRANSFTABLE = 0
  nCntTable = 0
  bFind = .f.
  nConn = 0
  cList = space(50)
  cNomeFisico = space(50)
  cTbl = space(30)
  cTbl2 = space(30)
  cTblProto = space(30)
  cDateMod = space(14)
  cDateModTable = space(14)
  oCombo = .NULL.
  cStatus = space(4)
  nColor = 0
  nChecked = 0
  cMsgWrite = space(50)
  bTProp3 = .f.
  bTProp4 = .f.
  bTProp5 = .f.
  * --- WorkFile variables
  cpttbls_idx=0
  cptsrvr_idx=0
  mngdatabase_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private cDBType
    m.cDBType = space(10)
    private w_NSERVERS
    m.w_NSERVERS = 0
    private cMsg
    m.cMsg = space(50)
    private bReDo
    m.bReDo = .f.
    * --- Batch unico chiamato da CP_MNGDATABASE, CP_MNGSERVER e CP_MNGTABLE
    * --- Gestione tabelle/database
    * --- Parametro pParam:
    *     
    *     Update Reg - Aggiorna registri
    *     Init ZTab - Init tabella temporanea
    *     Server View - Vista zoom server
    *     SelectBeforeQ - Salvataggio Cursore
    *     SelectAfterQ - Ripristina Cursore
    *     Close ZTab - Drop tabella temporanea
    *     Resize - Ridimensionamento vista server
    *     Update DB - Aggiorna database
    *     Ver Conn - Verifica connessione
    *     Mod Tab - Modifica tabella
    *     Add Server - Aggiungi server
    *     Mod Server - Modifica server
    *     Delete Server - Elimina server
    *     
    *             ------------
    *     
    *     Tab Init - Tabella Inizializzazione
    *     Tab Save - Tabella salvataggio
    *     
    *             ------------
    *     
    *     Srv Init - Server Inizializzazione
    *     Srv ChangeODBC - Server cambio ODBC
    *     Srv Ok - Server ok
    *     
    *             ------------
    *     
    *     Azi Ok - Azienda ok
    *     
    * --- Variabili da passare per riferimento
    DIMENSION xServers[1,3]
    do case
      case this.pParam="Update Reg"
        cp_msg(cp_Translate(MSG_UPDATING_REGISTER))
        this.w_OK = .T.
        this.w_COUNT = 1
        do while this.w_COUNT<=i_dcx.nfi
          this.bFind = .T.
          this.cTbl = i_dcx.Tbls[this.w_COUNT,1]
          * --- Read from cpttbls
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.cpttbls_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PhName"+;
              " from "+i_cTable+" cpttbls where ";
                  +"FileName = "+cp_ToStrODBC(this.cTbl);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PhName;
              from (i_cTable) where;
                  FileName = this.cTbl;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.cNomeFisico = NVL(cp_ToDate(_read_.PhName),cp_NullValue(_read_.PhName))
            use
            if i_Rows=0
              this.bFind = .F.
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if not i_dcx.IsTableAzi(this.cTbl)
            this.w_OK = this.w_OK AND cp_VerifyLinkTables(this.w_COUNT,this.cTbl,i_dcx)
          endif
          if this.w_OK
            if NOT this.bFind
              * --- * --- inserisce il nuovo file nel registro
              this.w_PH = Iif(i_dcx.Tbls[this.w_COUNT,19],this.cTbl+"_proto",i_dcx.Tbls[this.w_COUNT,2])
              this.bTProp3 = i_dcx.Tbls[this.w_COUNT,3]
              this.bTProp4 = i_dcx.Tbls[this.w_COUNT,4]
              this.bTProp5 = i_dcx.Tbls[this.w_COUNT,5]
              if not i_dcx.Tbls[this.w_COUNT,19]
                * --- Insert into cpttbls
                i_nConn=i_TableProp[this.cpttbls_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cpttbls_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"FileName"+",PhName"+",FlagYear"+",FlagUser"+",FlagComp"+",ServerName"+",DateMod"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.cTbl),'cpttbls','FileName');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PH),'cpttbls','PhName');
                  +","+cp_NullLink(cp_ToStrODBC(Iif(this.bTProp3,"T","N")),'cpttbls','FlagYear');
                  +","+cp_NullLink(cp_ToStrODBC(Iif(this.bTProp4,"T","N")),'cpttbls','FlagUser');
                  +","+cp_NullLink(cp_ToStrODBC(Iif(this.bTProp5,"T","N")),'cpttbls','FlagComp');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'cpttbls','ServerName');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'cpttbls','DateMod');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'FileName',this.cTbl,'PhName',this.w_PH,'FlagYear',Iif(this.bTProp3,"T","N"),'FlagUser',Iif(this.bTProp4,"T","N"),'FlagComp',Iif(this.bTProp5,"T","N"),'ServerName'," ",'DateMod'," ")
                  insert into (i_cTable) (FileName,PhName,FlagYear,FlagUser,FlagComp,ServerName,DateMod &i_ccchkf. );
                     values (;
                       this.cTbl;
                       ,this.w_PH;
                       ,Iif(this.bTProp3,"T","N");
                       ,Iif(this.bTProp4,"T","N");
                       ,Iif(this.bTProp5,"T","N");
                       ," ";
                       ," ";
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
            else
              this.w_PH = i_dcx.Tbls[this.w_COUNT,2]
              * --- * --- Zucchetti Aulla (La richiesta x ad hoc non ha mai senso)
              if this.w_PH<>this.cNomeFisico AND 1=0
                if cp_YesNo(cp_Translate(MSG_PHYSICAL_NAME_OF_TABLE_CHANGED_FROM_DESIGN_PHASE_F)+Chr(13)+Chr(10)+; 
 cp_MsgFormat(MSG_DESIGN_CL___C_DATABASE_CL___F,this.w_ph,Trim(this.cNomeFisico))+Chr(13)+Chr(10)+; 
 cp_Translate(MSG_KEEP_NEW_ONE_QM),32,.f.)
                  this.w_PH = this.cNomeFisico
                endif
              endif
              * --- Write into cpttbls
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.cpttbls_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.cpttbls_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PhName ="+cp_NullLink(cp_ToStrODBC(this.w_PH),'cpttbls','PhName');
                +",FlagYear ="+cp_NullLink(cp_ToStrODBC(Iif(i_dcx.Tbls[this.w_COUNT,3],"T","N")),'cpttbls','FlagYear');
                +",FlagUser ="+cp_NullLink(cp_ToStrODBC(Iif(i_dcx.Tbls[this.w_COUNT,4],"T","N")),'cpttbls','FlagUser');
                +",FlagComp ="+cp_NullLink(cp_ToStrODBC(Iif(i_dcx.Tbls[this.w_COUNT,5],"T","N")),'cpttbls','FlagComp');
                    +i_ccchkf ;
                +" where ";
                    +"FileName = "+cp_ToStrODBC(this.cTbl);
                       )
              else
                update (i_cTable) set;
                    PhName = this.w_PH;
                    ,FlagYear = Iif(i_dcx.Tbls[this.w_COUNT,3],"T","N");
                    ,FlagUser = Iif(i_dcx.Tbls[this.w_COUNT,4],"T","N");
                    ,FlagComp = Iif(i_dcx.Tbls[this.w_COUNT,5],"T","N");
                    &i_ccchkf. ;
                 where;
                    FileName = this.cTbl;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          this.w_COUNT = this.w_COUNT+1
        enddo
        this.oParentObject.w_ZSRV.Visible = .F.
      case this.pParam="Init ZTab"
        cp_msg(cp_Translate(MSG_LOADING_TABLES))
        * --- Create temporary table mngdatabase
        i_nIdx=cp_AddTableDef('mngdatabase') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.cpttbls_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"FileName,PhName,cast(' ' as char(4)) as status,cast(' ' as char(50)) as msg, 0 as color,ServerName, cast(' ' as char(14)) as xdcts, 0 as chkd "," from "+i_cTable;
              +" where 1=0";
              )
        this.mngdatabase_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- * --- In testa le tabella da aggiornare..
        For this.w_COUNT=1 TO i_dcx.nfi
        this.bFind = .T.
        this.cTbl = i_dcx.Tbls[this.w_COUNT,1]
        this.cTbl2 = i_dcx.Tbls[this.w_COUNT,2]
        this.cDateMod = i_dcx.Tbls[this.w_COUNT,13]
        * --- Read from cpttbls
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.cpttbls_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ServerName,DateMod"+;
            " from "+i_cTable+" cpttbls where ";
                +"FileName = "+cp_ToStrODBC(this.cTbl);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ServerName,DateMod;
            from (i_cTable) where;
                FileName = this.cTbl;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SRVNAME = NVL(cp_ToDate(_read_.ServerName),cp_NullValue(_read_.ServerName))
          this.cDateModTable = NVL(cp_ToDate(_read_.DateMod),cp_NullValue(_read_.DateMod))
          use
          if i_Rows=0
            this.bFind = .F.
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case NOT this.bFind AND NOT i_dcx.Tbls[this.w_COUNT,19]
            * --- * --- non e' nel registro e non e' una tabella temporanea
            m.cMsg = cp_Translate(MSG_ERROR_IN_FILE_REGISTER)
            * --- Insert into mngdatabase
            i_nConn=i_TableProp[this.mngdatabase_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.mngdatabase_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"filename"+",phname"+",status"+",msg"+",color"+",servername"+",xdcts"+",chkd"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.cTbl),'mngdatabase','filename');
              +","+cp_NullLink(cp_ToStrODBC(this.cTbl2),'mngdatabase','phname');
              +","+cp_NullLink(cp_ToStrODBC("Err"),'mngdatabase','status');
              +","+cp_NullLink(cp_ToStrODBC(m.cMsg),'mngdatabase','msg');
              +","+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','color');
              +","+cp_NullLink(cp_ToStrODBC(Nvl(this.w_SRVNAME,"")),'mngdatabase','servername');
              +","+cp_NullLink(cp_ToStrODBC(""),'mngdatabase','xdcts');
              +","+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'filename',this.cTbl,'phname',this.cTbl2,'status',"Err",'msg',m.cMsg,'color',0,'servername',Nvl(this.w_SRVNAME,""),'xdcts',"",'chkd',0)
              insert into (i_cTable) (filename,phname,status,msg,color,servername,xdcts,chkd &i_ccchkf. );
                 values (;
                   this.cTbl;
                   ,this.cTbl2;
                   ,"Err";
                   ,m.cMsg;
                   ,0;
                   ,Nvl(this.w_SRVNAME,"");
                   ,"";
                   ,0;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          case i_dcx.Tbls[this.w_COUNT,15]
            * --- * --- tabella esterna
            m.cMsg = cp_Translate(MSG_EXTERNAL_TABLE)
            * --- Insert into mngdatabase
            i_nConn=i_TableProp[this.mngdatabase_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.mngdatabase_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"filename"+",phname"+",status"+",msg"+",color"+",servername"+",xdcts"+",chkd"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.cTbl),'mngdatabase','filename');
              +","+cp_NullLink(cp_ToStrODBC(this.cTbl2),'mngdatabase','phname');
              +","+cp_NullLink(cp_ToStrODBC("Ext"),'mngdatabase','status');
              +","+cp_NullLink(cp_ToStrODBC(m.cMsg),'mngdatabase','msg');
              +","+cp_NullLink(cp_ToStrODBC(1),'mngdatabase','color');
              +","+cp_NullLink(cp_ToStrODBC(Nvl(this.w_SRVNAME,"")),'mngdatabase','servername');
              +","+cp_NullLink(cp_ToStrODBC(""),'mngdatabase','xdcts');
              +","+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'filename',this.cTbl,'phname',this.cTbl2,'status',"Ext",'msg',m.cMsg,'color',1,'servername',Nvl(this.w_SRVNAME,""),'xdcts',"",'chkd',0)
              insert into (i_cTable) (filename,phname,status,msg,color,servername,xdcts,chkd &i_ccchkf. );
                 values (;
                   this.cTbl;
                   ,this.cTbl2;
                   ,"Ext";
                   ,m.cMsg;
                   ,1;
                   ,Nvl(this.w_SRVNAME,"");
                   ,"";
                   ,0;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          case i_dcx.Tbls[this.w_COUNT,19]
            * --- * --- tabella temporanea
            this.cTblProto = Alltrim(this.cTbl)+"_proto"
            m.w_NSERVERS = cp_GetServerIdx(this.w_SRVNAME)
            if m.w_NSERVERS<>0 And i_ServerConn[m.w_NSERVERS,2]<>-1
              m.cMsg = cp_Translate(MSG_TEMPORARY_TABLE)
              if cp_CheckTable(i_dcx.Tbls[this.w_COUNT,1],i_dcx.Tbls[this.w_COUNT,1]+"_proto",i_ServerConn[m.w_NSERVERS,2],i_dcx,i_ServerConn[m.w_NSERVERS,6])
                * --- Insert into mngdatabase
                i_nConn=i_TableProp[this.mngdatabase_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.mngdatabase_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"filename"+",phname"+",status"+",msg"+",color"+",servername"+",xdcts"+",chkd"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.cTbl),'mngdatabase','filename');
                  +","+cp_NullLink(cp_ToStrODBC(this.cTblProto),'mngdatabase','phname');
                  +","+cp_NullLink(cp_ToStrODBC("Ext"),'mngdatabase','status');
                  +","+cp_NullLink(cp_ToStrODBC(m.cMsg),'mngdatabase','msg');
                  +","+cp_NullLink(cp_ToStrODBC(1),'mngdatabase','color');
                  +","+cp_NullLink(cp_ToStrODBC(Nvl(this.w_SRVNAME,"")),'mngdatabase','servername');
                  +","+cp_NullLink(cp_ToStrODBC(""),'mngdatabase','xdcts');
                  +","+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'filename',this.cTbl,'phname',this.cTblProto,'status',"Ext",'msg',m.cMsg,'color',1,'servername',Nvl(this.w_SRVNAME,""),'xdcts',"",'chkd',0)
                  insert into (i_cTable) (filename,phname,status,msg,color,servername,xdcts,chkd &i_ccchkf. );
                     values (;
                       this.cTbl;
                       ,this.cTblProto;
                       ,"Ext";
                       ,m.cMsg;
                       ,1;
                       ,Nvl(this.w_SRVNAME,"");
                       ,"";
                       ,0;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              else
                * --- Insert into mngdatabase
                i_nConn=i_TableProp[this.mngdatabase_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.mngdatabase_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"filename"+",phname"+",status"+",msg"+",color"+",servername"+",xdcts"+",chkd"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.cTbl),'mngdatabase','filename');
                  +","+cp_NullLink(cp_ToStrODBC(this.cTblProto),'mngdatabase','phname');
                  +","+cp_NullLink(cp_ToStrODBC("Ext"),'mngdatabase','status');
                  +","+cp_NullLink(cp_ToStrODBC(m.cMsg),'mngdatabase','msg');
                  +","+cp_NullLink(cp_ToStrODBC(1),'mngdatabase','color');
                  +","+cp_NullLink(cp_ToStrODBC(Nvl(this.w_SRVNAME,"")),'mngdatabase','servername');
                  +","+cp_NullLink(cp_ToStrODBC(""),'mngdatabase','xdcts');
                  +","+cp_NullLink(cp_ToStrODBC(1),'mngdatabase','chkd');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'filename',this.cTbl,'phname',this.cTblProto,'status',"Ext",'msg',m.cMsg,'color',1,'servername',Nvl(this.w_SRVNAME,""),'xdcts',"",'chkd',1)
                  insert into (i_cTable) (filename,phname,status,msg,color,servername,xdcts,chkd &i_ccchkf. );
                     values (;
                       this.cTbl;
                       ,this.cTblProto;
                       ,"Ext";
                       ,m.cMsg;
                       ,1;
                       ,Nvl(this.w_SRVNAME,"");
                       ,"";
                       ,1;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
            else
              m.cMsg = cp_Translate(MSG_SERVER_NOT_FOUND)
              * --- Insert into mngdatabase
              i_nConn=i_TableProp[this.mngdatabase_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.mngdatabase_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"filename"+",phname"+",status"+",msg"+",color"+",servername"+",xdcts"+",chkd"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.cTbl),'mngdatabase','filename');
                +","+cp_NullLink(cp_ToStrODBC(this.cTblProto),'mngdatabase','phname');
                +","+cp_NullLink(cp_ToStrODBC("Conn"),'mngdatabase','status');
                +","+cp_NullLink(cp_ToStrODBC(m.cMsg),'mngdatabase','msg');
                +","+cp_NullLink(cp_ToStrODBC(2),'mngdatabase','color');
                +","+cp_NullLink(cp_ToStrODBC(Nvl(this.w_SRVNAME,"")),'mngdatabase','servername');
                +","+cp_NullLink(cp_ToStrODBC(""),'mngdatabase','xdcts');
                +","+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'filename',this.cTbl,'phname',this.cTblProto,'status',"Conn",'msg',m.cMsg,'color',2,'servername',Nvl(this.w_SRVNAME,""),'xdcts',"",'chkd',0)
                insert into (i_cTable) (filename,phname,status,msg,color,servername,xdcts,chkd &i_ccchkf. );
                   values (;
                     this.cTbl;
                     ,this.cTblProto;
                     ,"Conn";
                     ,m.cMsg;
                     ,2;
                     ,Nvl(this.w_SRVNAME,"");
                     ,"";
                     ,0;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          case this.cDateMod > this.cDateModTable Or Empty(this.cDateMod)
            * --- * --- da aggiornare (Zucchetti Aulla > al posto di diverso)
            m.cMsg = cp_Translate(MSG_UPDATE)
            * --- Insert into mngdatabase
            i_nConn=i_TableProp[this.mngdatabase_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.mngdatabase_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"filename"+",phname"+",status"+",msg"+",color"+",servername"+",xdcts"+",chkd"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.cTbl),'mngdatabase','filename');
              +","+cp_NullLink(cp_ToStrODBC(this.cTbl2),'mngdatabase','phname');
              +","+cp_NullLink(cp_ToStrODBC("Ver"),'mngdatabase','status');
              +","+cp_NullLink(cp_ToStrODBC(m.cMsg),'mngdatabase','msg');
              +","+cp_NullLink(cp_ToStrODBC(2),'mngdatabase','color');
              +","+cp_NullLink(cp_ToStrODBC(Nvl(this.w_SRVNAME,"")),'mngdatabase','servername');
              +","+cp_NullLink(cp_ToStrODBC(this.cDateMod),'mngdatabase','xdcts');
              +","+cp_NullLink(cp_ToStrODBC(1),'mngdatabase','chkd');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'filename',this.cTbl,'phname',this.cTbl2,'status',"Ver",'msg',m.cMsg,'color',2,'servername',Nvl(this.w_SRVNAME,""),'xdcts',this.cDateMod,'chkd',1)
              insert into (i_cTable) (filename,phname,status,msg,color,servername,xdcts,chkd &i_ccchkf. );
                 values (;
                   this.cTbl;
                   ,this.cTbl2;
                   ,"Ver";
                   ,m.cMsg;
                   ,2;
                   ,Nvl(this.w_SRVNAME,"");
                   ,this.cDateMod;
                   ,1;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          otherwise
            m.w_NSERVERS = cp_GetServerIdx(this.w_SRVNAME)
            if m.w_NSERVERS<>0 And i_ServerConn[m.w_NSERVERS,2]<>-1
              if cp_CheckTable(i_dcx.Tbls[this.w_COUNT,1],i_dcx.Tbls[this.w_COUNT,2],i_ServerConn[m.w_NSERVERS,2],i_dcx,i_ServerConn[m.w_NSERVERS,6])
                * --- * --- Ok
                m.cMsg = cp_Translate(MSG_OK)
                * --- Insert into mngdatabase
                i_nConn=i_TableProp[this.mngdatabase_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.mngdatabase_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"filename"+",phname"+",status"+",msg"+",color"+",servername"+",xdcts"+",chkd"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.cTbl),'mngdatabase','filename');
                  +","+cp_NullLink(cp_ToStrODBC(this.cTbl2),'mngdatabase','phname');
                  +","+cp_NullLink(cp_ToStrODBC("Ok"),'mngdatabase','status');
                  +","+cp_NullLink(cp_ToStrODBC(cp_Translate(MSG_OK)),'mngdatabase','msg');
                  +","+cp_NullLink(cp_ToStrODBC(3),'mngdatabase','color');
                  +","+cp_NullLink(cp_ToStrODBC(Nvl(this.w_SRVNAME,"")),'mngdatabase','servername');
                  +","+cp_NullLink(cp_ToStrODBC(this.cDateMod),'mngdatabase','xdcts');
                  +","+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'filename',this.cTbl,'phname',this.cTbl2,'status',"Ok",'msg',cp_Translate(MSG_OK),'color',3,'servername',Nvl(this.w_SRVNAME,""),'xdcts',this.cDateMod,'chkd',0)
                  insert into (i_cTable) (filename,phname,status,msg,color,servername,xdcts,chkd &i_ccchkf. );
                     values (;
                       this.cTbl;
                       ,this.cTbl2;
                       ,"Ok";
                       ,cp_Translate(MSG_OK);
                       ,3;
                       ,Nvl(this.w_SRVNAME,"");
                       ,this.cDateMod;
                       ,0;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              else
                * --- * --- da aggiornare
                m.cMsg = cp_Translate(MSG_CREATE_OR_UPDATE)
                * --- Insert into mngdatabase
                i_nConn=i_TableProp[this.mngdatabase_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.mngdatabase_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"filename"+",phname"+",status"+",msg"+",color"+",servername"+",xdcts"+",chkd"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.cTbl),'mngdatabase','filename');
                  +","+cp_NullLink(cp_ToStrODBC(this.cTbl2),'mngdatabase','phname');
                  +","+cp_NullLink(cp_ToStrODBC("Ver"),'mngdatabase','status');
                  +","+cp_NullLink(cp_ToStrODBC(cp_Translate(MSG_CREATE_OR_UPDATE)),'mngdatabase','msg');
                  +","+cp_NullLink(cp_ToStrODBC(2),'mngdatabase','color');
                  +","+cp_NullLink(cp_ToStrODBC(Nvl(this.w_SRVNAME,"")),'mngdatabase','servername');
                  +","+cp_NullLink(cp_ToStrODBC(this.cDateMod),'mngdatabase','xdcts');
                  +","+cp_NullLink(cp_ToStrODBC(1),'mngdatabase','chkd');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'filename',this.cTbl,'phname',this.cTbl2,'status',"Ver",'msg',cp_Translate(MSG_CREATE_OR_UPDATE),'color',2,'servername',Nvl(this.w_SRVNAME,""),'xdcts',this.cDateMod,'chkd',1)
                  insert into (i_cTable) (filename,phname,status,msg,color,servername,xdcts,chkd &i_ccchkf. );
                     values (;
                       this.cTbl;
                       ,this.cTbl2;
                       ,"Ver";
                       ,cp_Translate(MSG_CREATE_OR_UPDATE);
                       ,2;
                       ,Nvl(this.w_SRVNAME,"");
                       ,this.cDateMod;
                       ,1;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
            else
              * --- * --- server irraggiungibile
              m.cMsg = cp_Translate(MSG_SERVER_NOT_FOUND)
              * --- Insert into mngdatabase
              i_nConn=i_TableProp[this.mngdatabase_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.mngdatabase_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"filename"+",phname"+",status"+",msg"+",color"+",servername"+",xdcts"+",chkd"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.cTbl),'mngdatabase','filename');
                +","+cp_NullLink(cp_ToStrODBC(this.cTbl2),'mngdatabase','phname');
                +","+cp_NullLink(cp_ToStrODBC("Conn"),'mngdatabase','status');
                +","+cp_NullLink(cp_ToStrODBC(cp_Translate(MSG_SERVER_NOT_FOUND)),'mngdatabase','msg');
                +","+cp_NullLink(cp_ToStrODBC(2),'mngdatabase','color');
                +","+cp_NullLink(cp_ToStrODBC(Nvl(this.w_SRVNAME,"")),'mngdatabase','servername');
                +","+cp_NullLink(cp_ToStrODBC(""),'mngdatabase','xdcts');
                +","+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'filename',this.cTbl,'phname',this.cTbl2,'status',"Conn",'msg',cp_Translate(MSG_SERVER_NOT_FOUND),'color',2,'servername',Nvl(this.w_SRVNAME,""),'xdcts',"",'chkd',0)
                insert into (i_cTable) (filename,phname,status,msg,color,servername,xdcts,chkd &i_ccchkf. );
                   values (;
                     this.cTbl;
                     ,this.cTbl2;
                     ,"Conn";
                     ,cp_Translate(MSG_SERVER_NOT_FOUND);
                     ,2;
                     ,Nvl(this.w_SRVNAME,"");
                     ,"";
                     ,0;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
        endcase
        Next
      case this.pParam="Server View"
        SELECT (this.oParentObject.w_ZSRV.cCursor)
        if RECCOUNT() >0
          this.oParentObject.w_NSRV = .T.
          this.oParentObject.w_ZSRV.Visible = this.oParentObject.w_SIZE
          this.oParentObject.w_SIZE = NOT this.oParentObject.w_SIZE
        endif
      case this.pParam="SelectBeforeQ"
        * --- In caso di ordinamento da parte dell'utente, salvo le modifiche allo zoom
        if Used(this.oParentObject.w_ZTAB.cCursor)
          Select * from (this.oParentObject.w_ZTAB.cCursor) into cursor "SaveMod"
        endif
      case this.pParam="SelectAfterQ"
        * --- Ripristino cursore alle modifiche precedenti la riesecuzione della query
        if Used(this.oParentObject.w_ZTAB.cCursor) and Used("SaveMod")
          UPDATE (this.oParentObject.w_ZTAB.cCursor) SET XCHK=CHKD
          * --- Se si viene dall'aggiornamento DB seguo solo il check della tabella
          if Not this.oParentObject.w_UPDATED
            UPDATE (this.oParentObject.w_ZTAB.cCursor) SET XCHK=1 WHERE filename in (select filename from "SaveMod" where xchk=1)
            Update (this.oParentObject.w_ZTAB.cCursor) set XCHK=0 where filename in (select filename from "SaveMod" where xchk=0)
          else
            this.oParentObject.w_UPDATED = .F.
          endif
          use in select ("SaveMod")
        endif
        Select (this.oParentObject.w_ZTAB.cCursor) 
 Go Top
        this.oParentObject.w_ZTAB.grd.Refresh()     
      case this.pParam="Close ZTab"
        * --- Drop temporary table mngdatabase
        i_nIdx=cp_GetTableDefIdx('mngdatabase')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('mngdatabase')
        endif
        if used("_azi_")
          select "_azi_"
          use
        endif
        if Type("g_CfgGestMod")<>"U"
          if g_CfgGestMod
            * --- La variabile non era definita
            Release g_bDisableCfgGest
          else
            g_bDisableCfgGest=.F.
          endif
          Release g_CfgGestMod
        endif
      case this.pParam="Resize"
        this.oParentObject.w_ZSRV.Visible = this.oParentObject.w_SIZE
        this.oParentObject.w_SIZE = NOT this.oParentObject.w_SIZE
      case this.pParam="Update DB" or this.pParam="AutoexecUpdate DB" and this.oParentObject.w_PARAM_MNGDB="AUTOEXEC"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParam="Ver Conn"
        * --- classe su cp_dbadm, finestra con messaggio di errore
        Public msgwnd 
 msgwnd=Createobject("mkdbmsg") 
 msgwnd.Show()
        m.w_NSERVERS = 0
        select (this.oParentObject.w_ZTAB.cCursor)
        GO TOP
        Scan For xchk=0 And Status="Conn"
        this.w_NAMETABLE = TRIM(FILENAME)
        this.w_SRVNAME = TRIM(SERVERNAME)
        m.cDBType = " "
        this.nConn = cp_OpenServerForRebuild(this.w_SRVNAME,@m.cDBType,@xServers,@m.w_nServers)
        select (this.oParentObject.w_ZTAB.cCursor)
        if this.nConn<0
          this.w_OK = .F.
        else
          this.cNomeFisico = Iif(Empty(PhName),this.w_NAMETABLE,Trim(PhName))
          this.w_OK = cp_CheckTable(this.w_NAMETABLE,this.cNomeFisico,this.nConn,i_dcx,m.cDBType)
        endif
        * --- * --- leggo l'indice della tabella temporanea
        this.MNGDATABASE_idx=cp_gettabledefidx("MNGDATABASE")
        if this.w_OK
          * --- * --- segna la tabella come aggiornata
          this.cMsgWrite = cp_Translate(MSG_OK)
          * --- Write into mngdatabase
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.mngdatabase_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"chkd ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
            +",status ="+cp_NullLink(cp_ToStrODBC("Ok"),'mngdatabase','status');
            +",msg ="+cp_NullLink(cp_ToStrODBC(this.cMsgWrite),'mngdatabase','msg');
            +",color ="+cp_NullLink(cp_ToStrODBC(3),'mngdatabase','color');
                +i_ccchkf ;
            +" where ";
                +"filename = "+cp_ToStrODBC(this.w_NAMETABLE);
                   )
          else
            update (i_cTable) set;
                chkd = 0;
                ,status = "Ok";
                ,msg = this.cMsgWrite;
                ,color = 3;
                &i_ccchkf. ;
             where;
                filename = this.w_NAMETABLE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- * --- segna la tabella come non aggiornata
          this.nChecked = Iif(this.nConn<0,0,1)
          this.cMsgWrite = Iif(this.nConn<0,cp_Translate(MSG_CONNECTION_ERROR),cp_Translate(MSG_CREATE_OR_UPDATE))
          this.cStatus = Iif(this.nConn<0,"Err","Ver")
          this.nColor = Iif(this.nConn<0,0,3)
          * --- Write into mngdatabase
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.mngdatabase_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"chkd ="+cp_NullLink(cp_ToStrODBC(this.nChecked),'mngdatabase','chkd');
            +",status ="+cp_NullLink(cp_ToStrODBC(this.cStatus),'mngdatabase','status');
            +",msg ="+cp_NullLink(cp_ToStrODBC(this.cMsgWrite),'mngdatabase','msg');
            +",color ="+cp_NullLink(cp_ToStrODBC(this.nColor),'mngdatabase','color');
                +i_ccchkf ;
            +" where ";
                +"filename = "+cp_ToStrODBC(this.w_NAMETABLE);
                   )
          else
            update (i_cTable) set;
                chkd = this.nChecked;
                ,status = this.cStatus;
                ,msg = this.cMsgWrite;
                ,color = this.nColor;
                &i_ccchkf. ;
             where;
                filename = this.w_NAMETABLE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        Endscan
        =cp_CloseAllServerForRebuild(@xServers,m.w_nServers)
        msgwnd.End(cp_Translate(MSG_LINK_CHECKING_TERMINATED))
        this.oparentobject.NotifyEvent("AggiornaTab")
      case this.pParam="Mod Tab"
        * --- * Lancio maschera di configurazione tabella
        cp_mngtable(this.oParentObject)
      case this.pParam="Add Server"
        * --- * Lancio maschera di configurazione server
        this.oParentObject.w_ADDSRV = .T.
        cp_mngserver(this.oParentObject)
        this.oParentObject.w_ADDSRV = .F.
      case this.pParam="Mod Server"
        * --- * Lancio maschera di configurazione server
        cp_mngserver(this.oParentObject)
      case this.pParam="Delete Server"
        this.w_SRVNAME = this.oParentObject.w_SERVER
        * --- Select from cpttbls
        i_nConn=i_TableProp[this.cpttbls_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select count(*) as Cnt  from "+i_cTable+" cpttbls ";
              +" where ServerName="+cp_ToStrODBC(this.w_SRVNAME)+"";
               ,"_Curs_cpttbls")
        else
          select count(*) as Cnt from (i_cTable);
           where ServerName=this.w_SRVNAME;
            into cursor _Curs_cpttbls
        endif
        if used('_Curs_cpttbls')
          select _Curs_cpttbls
          locate for 1=1
          do while not(eof())
          if TYPE("Cnt")="C"
            this.w_COUNT = VAL(Cnt)
          else
            this.w_COUNT = Cnt
          endif
          if this.w_COUNT=0
            * --- Delete from cptsrvr
            i_nConn=i_TableProp[this.cptsrvr_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cptsrvr_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"servername = "+cp_ToStrODBC(this.w_SRVNAME);
                     )
            else
              delete from (i_cTable) where;
                    servername = this.w_SRVNAME;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            this.oparentobject.NotifyEvent("AggiornaSrv")
          else
            cp_msg(cp_Translate(MSG_TABLES_ON_SERVER_C_YOU_CANNOT_DELETE_IT),.F.)
          endif
            select _Curs_cpttbls
            continue
          enddo
          use
        endif
      case this.pParam="Tab Init"
        * --- Select from cpttbls
        i_nConn=i_TableProp[this.cpttbls_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select FileName,PhName,FlagYear,FlagUser,FlagComp,ServerName,DateMod  from "+i_cTable+" cpttbls ";
              +" where FileName="+cp_ToStrODBC(this.oParentObject.w_FILENAME)+"";
               ,"_Curs_cpttbls")
        else
          select FileName,PhName,FlagYear,FlagUser,FlagComp,ServerName,DateMod from (i_cTable);
           where FileName=this.oParentObject.w_FILENAME;
            into cursor _Curs_cpttbls
        endif
        if used('_Curs_cpttbls')
          select _Curs_cpttbls
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_TABLE = nvl(_Curs_cpttbls.FileName," ")
          this.oParentObject.w_PATH = nvl(_Curs_cpttbls.PhName," ")
          this.oParentObject.w_YEAR = iif(nvl(_Curs_cpttbls.FlagYear,"N")="N", "N", "S")
          this.oParentObject.w_USER = iif(nvl(_Curs_cpttbls.FlagUser,"N")="N", "N", "S")
          this.oParentObject.w_COMPANY = iif(nvl(_Curs_cpttbls.FlagComp,"N")="N", "N", "S")
          this.oParentObject.w_SERVER = nvl(_Curs_cpttbls.ServerName," ")
          this.oParentObject.w_LASTREL = nvl(_Curs_cpttbls.DateMod," ")
            select _Curs_cpttbls
            continue
          enddo
          use
        endif
      case this.pParam="Tab Save"
        * --- Write into cpttbls
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.cpttbls_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.cpttbls_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PhName ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PATH),'cpttbls','PhName');
          +",ServerName ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERVER),'cpttbls','ServerName');
              +i_ccchkf ;
          +" where ";
              +"FileName = "+cp_ToStrODBC(this.oParentObject.w_TABLE);
                 )
        else
          update (i_cTable) set;
              PhName = this.oParentObject.w_PATH;
              ,ServerName = this.oParentObject.w_SERVER;
              &i_ccchkf. ;
           where;
              FileName = this.oParentObject.w_TABLE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- * --- leggo l'indice della tabella temporanea
        this.MNGDATABASE_idx=cp_gettabledefidx("MNGDATABASE")
        * --- Write into mngdatabase
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.mngdatabase_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"phname ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PATH),'mngdatabase','phname');
          +",servername ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERVER),'mngdatabase','servername');
              +i_ccchkf ;
          +" where ";
              +"filename = "+cp_ToStrODBC(this.oParentObject.w_TABLE);
                 )
        else
          update (i_cTable) set;
              phname = this.oParentObject.w_PATH;
              ,servername = this.oParentObject.w_SERVER;
              &i_ccchkf. ;
           where;
              filename = this.oParentObject.w_TABLE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.oparentobject.oparentobject.NotifyEvent("AggiornaTab")
      case this.pParam="Srv Init"
        if not this.oParentObject.w_ADDSRV
          * --- Select from cptsrvr
          i_nConn=i_TableProp[this.cptsrvr_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cptsrvr_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select servername,serverdesc,ODBCDataSource,WhenConn,PostIt,DatabaseType  from "+i_cTable+" cptsrvr ";
                +" where servername="+cp_ToStrODBC(this.oParentObject.w_SERVER)+"";
                 ,"_Curs_cptsrvr")
          else
            select servername,serverdesc,ODBCDataSource,WhenConn,PostIt,DatabaseType from (i_cTable);
             where servername=this.oParentObject.w_SERVER;
              into cursor _Curs_cptsrvr
          endif
          if used('_Curs_cptsrvr')
            select _Curs_cptsrvr
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_NAME = NVL(_Curs_cptsrvr.servername," ")
            this.oParentObject.w_DESCRI = NVL(_Curs_cptsrvr.serverdesc," ")
            this.oParentObject.w_TYPE = NVL(_Curs_cptsrvr.DatabaseType," ")
            this.oParentObject.w_CONNECT = iif(_Curs_cptsrvr.WhenConn=1,"S"," ")
            this.oParentObject.w_POSTIT = iif(NVL(_Curs_cptsrvr.PostIt," ")="Y", "S", "N")
            this.w_STRING = NVL(_Curs_cptsrvr.ODBCDataSource," ")
            this.oParentObject.w_CHECKODBC = iif(not empty (this.w_STRING) AND ("DRIVER"$UPPER(this.w_STRING) OR "DSN"$UPPER(this.w_STRING)) AND ";"$this.w_STRING,"S","N")
            if this.oParentObject.w_CHECKODBC="S"
              this.oParentObject.w_TEXTODBC = this.w_STRING
            else
              this.oParentObject.w_ODBC = this.w_STRING
            endif
              select _Curs_cptsrvr
              continue
            enddo
            use
          endif
        endif
      case this.pParam="Srv ChangeODBC"
        this.w_TYPEDB = .T.
        this.w_FINDDS = .T.
        this.w_COUNTDB = 0
        * --- * --- Cerca nella tabella dei data sources il database di appartenenza
        this.oCombo = this.oparentobject.getctrl("w_ODBC")
        do while this.w_COUNTDB<this.oCombo.ListCount and this.w_FINDDS
          this.w_COUNTDB = this.w_COUNTDB+1
          if this.oParentObject.w_ODBC=this.oparentobject.aDatasources[this.w_COUNTDB,1]
            this.w_FINDDS = .F.
          endif
        enddo
        this.w_COUNT_TYPEDB = 1
        this.oParentObject.w_TYPE = " "
        if NOT this.w_FINDDS
          this.oCombo = this.oparentobject.getctrl("w_TYPE")
          do while this.w_COUNT_TYPEDB<=this.oCombo.ListCount AND this.w_TYPEDB
            if RATC(IIF(this.oCombo.List(this.w_COUNT_TYPEDB)=="SQLServer","SQL Server",this.oCombo.List(this.w_COUNT_TYPEDB)),this.oparentobject.aDatasources[this.w_COUNTDB,2])<>0
              this.oParentObject.w_TYPE = this.oCombo.List(this.w_COUNT_TYPEDB)
              this.w_TYPEDB = .F.
            endif
            this.w_COUNT_TYPEDB = this.w_COUNT_TYPEDB+1
          enddo
        endif
      case this.pParam="Srv Ok"
        this.w_SRVNAME = TRIM(this.oParentObject.w_NAME)
        this.w_SRVDESC = TRIM(this.oParentObject.w_DESCRI)
        if this.oParentObject.w_CHECKODBC="S"
          this.w_SRVODBC = TRIM(this.oParentObject.w_TEXTODBC)
        else
          this.w_SRVODBC = TRIM(this.oParentObject.w_ODBC)
        endif
        this.w_SRVTYPE = TRIM(this.oParentObject.w_TYPE)
        this.w_SRVCONN = IIF(TRIM(this.oParentObject.w_CONNECT)="S",1,0)
        this.w_SRVPOSTIT = IIF(TRIM(this.oParentObject.w_POSTIT)="S","Y","N")
        if not empty(this.w_SRVNAME)
          if this.oParentObject.w_ADDSRV
            * --- Insert into cptsrvr
            i_nConn=i_TableProp[this.cptsrvr_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cptsrvr_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cptsrvr_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"servername"+",serverdesc"+",ODBCDataSource"+",WhenConn"+",PostIt"+",DatabaseType"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_SRVNAME),'cptsrvr','servername');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SRVDESC),'cptsrvr','serverdesc');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SRVODBC),'cptsrvr','ODBCDataSource');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SRVCONN),'cptsrvr','WhenConn');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SRVPOSTIT),'cptsrvr','PostIt');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SRVTYPE),'cptsrvr','DatabaseType');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'servername',this.w_SRVNAME,'serverdesc',this.w_SRVDESC,'ODBCDataSource',this.w_SRVODBC,'WhenConn',this.w_SRVCONN,'PostIt',this.w_SRVPOSTIT,'DatabaseType',this.w_SRVTYPE)
              insert into (i_cTable) (servername,serverdesc,ODBCDataSource,WhenConn,PostIt,DatabaseType &i_ccchkf. );
                 values (;
                   this.w_SRVNAME;
                   ,this.w_SRVDESC;
                   ,this.w_SRVODBC;
                   ,this.w_SRVCONN;
                   ,this.w_SRVPOSTIT;
                   ,this.w_SRVTYPE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Write into cptsrvr
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.cptsrvr_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cptsrvr_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.cptsrvr_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"serverdesc ="+cp_NullLink(cp_ToStrODBC(this.w_SRVDESC),'cptsrvr','serverdesc');
              +",ODBCDataSource ="+cp_NullLink(cp_ToStrODBC(this.w_SRVODBC),'cptsrvr','ODBCDataSource');
              +",WhenConn ="+cp_NullLink(cp_ToStrODBC(this.w_SRVCONN),'cptsrvr','WhenConn');
              +",PostIt ="+cp_NullLink(cp_ToStrODBC(this.w_SRVPOSTIT),'cptsrvr','PostIt');
              +",DatabaseType ="+cp_NullLink(cp_ToStrODBC(this.w_SRVTYPE),'cptsrvr','DatabaseType');
                  +i_ccchkf ;
              +" where ";
                  +"servername = "+cp_ToStrODBC(this.w_SRVNAME);
                     )
            else
              update (i_cTable) set;
                  serverdesc = this.w_SRVDESC;
                  ,ODBCDataSource = this.w_SRVODBC;
                  ,WhenConn = this.w_SRVCONN;
                  ,PostIt = this.w_SRVPOSTIT;
                  ,DatabaseType = this.w_SRVTYPE;
                  &i_ccchkf. ;
               where;
                  servername = this.w_SRVNAME;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.oparentobject.oparentobject.NotifyEvent("AggiornaSrv")
        else
          cp_msg(cp_Translate(MSG_SERVERNAME_MISSING),.F.)
        endif
      case this.pParam="Azi Ok"
        if NOT EMPTY(TRIM(this.oParentObject.w_NAME))
          cp_CreateAzi(Trim(this.oParentObject.w_NAME))
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo cursore delle aziende per le funzioni che derivano dalla vecchia gestione
    vq_exec("CP_AZI.VQR",,"_azi_")
    if cp_dbtype="Interbase"
      cp_CloseSecondConn()
    endif
    PRIVATE storedprocedures
    storedprocedures=" "
    if i_ServerConn[1,2]=0 And Not(this.oParentObject.w_BNEWPROCS) and not (i_compiledprog)
      this.w_FILE = FOPEN("cp_dbprc.prg")
      storedprocedures=FREAD(this.w_FILE,1000000)
      FCLOSE(this.w_FILE)
    endif
    this.oParentObject.w_BNEWPROCS = .F.
    CP_SP_UPDATESTDPROC()
    * --- classe su cp_dbadm, finestra con messaggio di errore
    Public msgwnd 
 msgwnd=Createobject("mkdbmsg") 
 msgwnd.Show()
    m.w_NSERVERS = 0
    * --- Le tabelle esterne non vanno aggiornate
    this.MNGDATABASE_idx=cp_gettabledefidx("MNGDATABASE")
    * --- Write into mngdatabase
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.mngdatabase_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"chkd ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
          +i_ccchkf ;
      +" where ";
          +"status = "+cp_ToStrODBC("Ext");
          +" and msg <> "+cp_ToStrODBC(cp_Translate(MSG_TEMPORARY_TABLE));
             )
    else
      update (i_cTable) set;
          chkd = 0;
          &i_ccchkf. ;
       where;
          status = "Ext";
          and msg <> cp_Translate(MSG_TEMPORARY_TABLE);

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    Update (this.oParentObject.w_ZTAB.cCursor) Set XCHK=0 Where STATUS="Ext" And !(msg==cp_Translate(MSG_TEMPORARY_TABLE))
    * --- * trasferisci le tabelle da non aggiornare nell'array di quelle trasferite
    DIMENSION i_acTableTransfer[1]
    select (this.oParentObject.w_ZTAB.cCursor)
    GO TOP
    SCAN FOR xchk=0
    * --- * Inserisci la tabella nell'array delle tabelle trasferite
    this.w_NTRANSFTABLE = this.w_NTRANSFTABLE+1
    DIMENSION i_acTableTransfer[this.w_NTRANSFTABLE]
    i_acTableTransfer[this.w_NTRANSFTABLE]=trim(FILENAME)
    ENDSCAN
    m.bReDo = .F.
    this.nCntTable = this.w_NTRANSFTABLE
    this.bFind = .T.
    do while this.bFind AND this.nCntTable<=i_dcx.GetTablesCount()
      this.bFind = .F.
      DIMENSION i_Ref[1,4]
      select (this.oParentObject.w_ZTAB.cCursor)
      GO TOP
      SCAN FOR xchk=1
      this.w_NAMETABLE = TRIM(FILENAME)
      this.w_SRVNAME = TRIM(SERVERNAME)
      * --- * verifica se la tabella corrente ('w_NAMETABLE') � da trasferire
      this.w_BFOUNDTABLE = cp_VerifyTransferTable(this.w_NAMETABLE,@i_acTableTransfer,this.w_nTransfTable)
      if NOT this.w_BFOUNDTABLE
        i_Ref[ALEN(i_Ref,1),1]=this.w_NAMETABLE
        i_Ref[ALEN(i_Ref,1),2]=TRIM(PHNAME)
        i_Ref[ALEN(i_Ref,1),3]=-1
        DIMENSION i_Ref[ALEN(i_Ref,1)+1,4]
      endif
      * --- * se la tabella corrente � da trasferire
      if this.w_BFOUNDTABLE
        this.bFind = .T.
        * --- * incrementa il contatore di tabelle tasferite
        this.nCntTable = this.nCntTable+1
        * --- * Inserisci la tabella nell'array delle tabelle trasferite
        this.w_NTRANSFTABLE = this.w_NTRANSFTABLE+1
        DIMENSION i_acTableTransfer[this.w_NTRANSFTABLE]
        i_acTableTransfer[this.w_nTransfTable]=Trim(FILENAME)
        this.w_OK = db_CreateTableStruct(this.w_NAMETABLE,Trim(PhName),this.w_SRVNAME,this.nConn,i_dcx,@m.cMsg,"all",@xServers,@m.w_nServers)
        if this.w_OK=0
          select (this.oParentObject.w_ZTAB.cCursor)
          this.cDateMod = TRIM (XDCTS)
          * --- Write into cpttbls
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.cpttbls_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.cpttbls_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DateMod ="+cp_NullLink(cp_ToStrODBC(this.cDateMod),'cpttbls','DateMod');
                +i_ccchkf ;
            +" where ";
                +"FileName = "+cp_ToStrODBC(this.w_NAMETABLE);
                   )
          else
            update (i_cTable) set;
                DateMod = this.cDateMod;
                &i_ccchkf. ;
             where;
                FileName = this.w_NAMETABLE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- * --- segna la tabella come aggiornata
          if not empty( xdcts )
            this.cMsgWrite = cp_Translate(MSG_OK)
            * --- * --- leggo l'indice della tabella temporanea
            this.MNGDATABASE_idx=cp_gettabledefidx("MNGDATABASE")
            * --- Write into mngdatabase
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.mngdatabase_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"chkd ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
              +",status ="+cp_NullLink(cp_ToStrODBC("Ok"),'mngdatabase','status');
              +",msg ="+cp_NullLink(cp_ToStrODBC(this.cMsgWrite),'mngdatabase','msg');
              +",color ="+cp_NullLink(cp_ToStrODBC(3),'mngdatabase','color');
                  +i_ccchkf ;
              +" where ";
                  +"filename = "+cp_ToStrODBC(this.w_NAMETABLE);
                     )
            else
              update (i_cTable) set;
                  chkd = 0;
                  ,status = "Ok";
                  ,msg = this.cMsgWrite;
                  ,color = 3;
                  &i_ccchkf. ;
               where;
                  filename = this.w_NAMETABLE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            if Vartype(msgwnd)="O"
              if status<>"Ext"
                msgwnd.add_Err(Trim(FILENAME)+cp_Translate(MSG_TABLE_WITHOUT_TIMESTAMP))
              else
                * --- * --- le tabelle temporanee vanno marcate come aggiornate
                * --- * --- leggo l'indice della tabella temporanea
                this.MNGDATABASE_idx=cp_gettabledefidx("MNGDATABASE")
                * --- Write into mngdatabase
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.mngdatabase_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"chkd ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
                      +i_ccchkf ;
                  +" where ";
                      +"filename = "+cp_ToStrODBC(this.w_NAMETABLE);
                         )
                else
                  update (i_cTable) set;
                      chkd = 0;
                      &i_ccchkf. ;
                   where;
                      filename = this.w_NAMETABLE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
        else
          if this.w_OK=2
            * --- * --- segna la tabella con errore
            this.cMsgWrite = cp_MsgFormat(MSG_ERROR_IN_TABLE__,m.cMsg)
            * --- * --- leggo l'indice della tabella temporanea
            this.MNGDATABASE_idx=cp_gettabledefidx("MNGDATABASE")
            * --- Write into mngdatabase
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.mngdatabase_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"chkd ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
              +",status ="+cp_NullLink(cp_ToStrODBC("Err"),'mngdatabase','status');
              +",msg ="+cp_NullLink(cp_ToStrODBC(this.cMsgWrite),'mngdatabase','msg');
              +",color ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','color');
                  +i_ccchkf ;
              +" where ";
                  +"filename = "+cp_ToStrODBC(this.w_NAMETABLE);
                     )
            else
              update (i_cTable) set;
                  chkd = 0;
                  ,status = "Err";
                  ,msg = this.cMsgWrite;
                  ,color = 0;
                  &i_ccchkf. ;
               where;
                  filename = this.w_NAMETABLE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            if this.w_OK=1
              m.bReDo = .T.
            endif
          endif
        endif
      endif
      ENDSCAN
    enddo
    * --- * se le tabelle contengono dei link incrociati
    if this.nCntTable<i_dcx.GetTablesCount()
      MSGWND.add_err(chr(13)+chr(10)+cp_translate(MSG_WARN_LINK_DOUBLECROSSED_QM))
      MSGWND.add_err(cp_Translate(MSG_TABLES_INVOLVED_WITH_DOUBLECROSSED_LINKS_CL))
      this.cList = " "
      For this.w_COUNT=1 to ALEN(i_Ref,1)-1
      this.cList = this.cList+Iif(Not Empty(this.cList),",","")+i_Ref[this.w_COUNT,1]
      Next
      msgwnd.add_Err(this.cList+Chr(13)+Chr(10)+Chr(13)+Chr(10))
      For this.w_COUNT=1 to ALEN(i_Ref,1)-1
      i_Ref[this.w_COUNT,3]=db_CreateTableStruct(i_Ref[this.w_COUNT,1],i_Ref[this.w_COUNT,2],this.w_SRVNAME,this.nConn,i_dcx,@m.cMsg,"table",@xServers,@m.w_nServers)
      i_Ref[this.w_COUNT,4]=m.cMsg
      Next
      For this.w_COUNT=1 to ALEN(i_Ref,1)-1
      if i_Ref[this.w_COUNT,3]=0
        i_Ref[this.w_COUNT,3]=db_CreateTableStruct(i_Ref[this.w_COUNT,1],i_Ref[this.w_COUNT,2],this.w_SRVNAME,this.nConn,i_dcx,@m.cMsg,"index",@xServers,@m.w_nServers)
        i_Ref[this.w_COUNT,4]=m.cMsg
      endif
      Next
      Set Order To 0
      select (this.oParentObject.w_ZTAB.cCursor)
      GO TOP
      SCAN Nooptimize For XCHK=1
      this.w_NAMETABLE = Trim(FILENAME)
      For this.w_COUNT=1 to ALEN(i_Ref,1)-1
      if i_Ref[this.w_COUNT,1]==this.w_NAMETABLE
        if i_Ref[this.w_COUNT,3]=0
          * --- Write into cpttbls
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.cpttbls_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpttbls_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.cpttbls_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DateMod ="+cp_NullLink(cp_ToStrODBC(xdcts),'cpttbls','DateMod');
                +i_ccchkf ;
            +" where ";
                +"FileName = "+cp_ToStrODBC(this.w_NAMETABLE);
                   )
          else
            update (i_cTable) set;
                DateMod = xdcts;
                &i_ccchkf. ;
             where;
                FileName = this.w_NAMETABLE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- * --- segna la tabella come aggiornata
          if not empty( xdcts )
            this.cMsgWrite = cp_Translate(MSG_OK)
            * --- * --- leggo l'indice della tabella temporanea
            this.MNGDATABASE_idx=cp_gettabledefidx("MNGDATABASE")
            * --- Write into mngdatabase
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.mngdatabase_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"chkd ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
              +",status ="+cp_NullLink(cp_ToStrODBC("Ok"),'mngdatabase','status');
              +",msg ="+cp_NullLink(cp_ToStrODBC(this.cMsgWrite),'mngdatabase','msg');
              +",color ="+cp_NullLink(cp_ToStrODBC(3),'mngdatabase','color');
                  +i_ccchkf ;
              +" where ";
                  +"filename = "+cp_ToStrODBC(this.w_NAMETABLE);
                     )
            else
              update (i_cTable) set;
                  chkd = 0;
                  ,status = "Ok";
                  ,msg = this.cMsgWrite;
                  ,color = 3;
                  &i_ccchkf. ;
               where;
                  filename = this.w_NAMETABLE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            if Vartype(msgwnd)="O"
              if status<>"Ext"
                msgwnd.add_Err(Trim(FILENAME)+cp_Translate(MSG_TABLE_WITHOUT_TIMESTAMP))
              else
                * --- * --- le tabelle temporanee vanno marcate come aggiornate
                * --- * --- leggo l'indice della tabella temporanea
                this.MNGDATABASE_idx=cp_gettabledefidx("MNGDATABASE")
                * --- Write into mngdatabase
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.mngdatabase_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"chkd ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
                      +i_ccchkf ;
                  +" where ";
                      +"filename = "+cp_ToStrODBC(this.w_NAMETABLE);
                         )
                else
                  update (i_cTable) set;
                      chkd = 0;
                      &i_ccchkf. ;
                   where;
                      filename = this.w_NAMETABLE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
        else
          if i_Ref[this.w_COUNT,3]=2
            * --- * --- segna la tabella con errore
            this.cMsgWrite = cp_MsgFormat(MSG_ERROR_IN_TABLE__,i_Ref[this.w_COUNT,4])
            * --- * --- leggo l'indice della tabella temporanea
            this.MNGDATABASE_idx=cp_gettabledefidx("MNGDATABASE")
            * --- Write into mngdatabase
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.mngdatabase_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.mngdatabase_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.mngdatabase_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"chkd ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','chkd');
              +",status ="+cp_NullLink(cp_ToStrODBC("Err"),'mngdatabase','status');
              +",msg ="+cp_NullLink(cp_ToStrODBC(this.cMsgWrite),'mngdatabase','msg');
              +",color ="+cp_NullLink(cp_ToStrODBC(0),'mngdatabase','color');
                  +i_ccchkf ;
              +" where ";
                  +"filename = "+cp_ToStrODBC(this.w_NAMETABLE);
                     )
            else
              update (i_cTable) set;
                  chkd = 0;
                  ,status = "Err";
                  ,msg = this.cMsgWrite;
                  ,color = 0;
                  &i_ccchkf. ;
               where;
                  filename = this.w_NAMETABLE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            if i_Ref[this.w_COUNT,3]=1
              m.bReDo = .T.
            endif
          endif
        endif
      endif
      Next
      ENDSCAN
      select (this.oParentObject.w_ZTAB.cCursor)
      GO TOP
    endif
    if i_ServerConn[1,2]=0
      cp_PackSystemTables()
    endif
    cp_CloseAllServerForRebuild(@xServers,m.w_nServers)
    cp_UpdateRoles(i_ServerConn[1,2],i_dcx)
    Local i_p,i_newpersist, i_r 
 If i_ServerConn[1,2]<>0 
 i_r=cp_SQL(i_ServerConn[1,2],"select * from persist where code='PlanCheckDate'","__tmp__") 
 If i_r<>-1 
 If Reccount()=0 
 =cp_SQL(i_ServerConn[1,2],"insert into persist (code,persist) values ('PlanCheckDate',"+cp_ToStr(i_dcx.cPlanDate)+")") 
 Else 
 i_newpersist=cp_UpdatePersistPlanDate(i_dcx.cPlanDate,persist) 
 *WAIT WINDOW "nuovo="+i_newpersist+" "+" vecchio="+persist 
 =cp_SQL(i_ServerConn[1,2],"update persist set persist="+cp_ToStr(i_newpersist)+" where code='PlanCheckDate'") 
 Endif 
 Use 
 Endif 
 Else 
 Select * From persist Where Code="PlanCheckDate" Into Cursor __tmp__ 
 If Reccount()=0 
 Insert Into persist (Code,persist) Values ("PlanCheckDate",i_dcx.cPlanDate) 
 Else 
 i_p=i_DCX.cPlanDate 
 Update persist Set persist=i_p Where Code="PlanCheckDate" 
 Endif 
 Use 
 Endif
    *** Aggiornamento nella tabella persist delle lingue soggette a traduzione dato 
 Local i_ListLang 
 *costruisco lista lingue traducibili 
 i_ListLang=cp_CalcListLang() 
 If i_ServerConn[1,2]<>0 
 i_r=cp_SQL(i_ServerConn[1,2],"select * from persist where code='ListLanguage'","__tmp__") 
 If i_r<>-1 
 If Reccount()=0 
 =cp_SQL(i_ServerConn[1,2],"insert into persist (code,persist) values ('ListLanguage',"+cp_ToStr(i_ListLang)+")") 
 Else 
 =cp_SQL(i_ServerConn[1,2],"update persist set persist="+cp_ToStr(i_ListLang)+" where code='ListLanguage'") 
 Endif 
 Use 
 Endif 
 Else 
 Select * From persist Where Code="ListLanguage" Into Cursor __tmp__ 
 If Reccount()=0 
 Insert Into persist (Code,persist) Values ("ListLanguage",i_ListLang) 
 Else 
 Update persist Set persist=i_ListLang Where Code="ListLanguage" 
 Endif 
 Use 
 Endif
    this.oParentObject.w_ENOK = .T.
    msgwnd.End(cp_Translate(MSG_DATABASE_REBUILT))
    if Not Empty(msgwnd.Edt_err.Text)
      cp_errormsg(MSG_DATABASE_REBUILT_ERROR,"!")
    endif
    * --- * --- aggiorna le stored procedures VFP
    if i_ServerConn[1,2]=0 And Not(i_compiledprog)
      this.w_FILE = Fcreate("cp_dbprc.prg")
      Fwrite(this.w_FILE,storedprocedures)
      FCLOSE(this.w_FILE)
      Compile cp_dbprc
      * --- * non deve essere inclusa automaticamente nell' eseguibile
      Set Proc To ("cp_dbprc") Additive
    endif
    this.oParentObject.w_UPDATED = .T.
    this.oparentobject.NotifyEvent("AggiornaTab")
    * --- * --- segnala se bisogna rilanciare la procedura
    if cp_dbtype="Interbase"
      cp_OpenSecondConn(pConn)
    endif
    if m.bReDo
      cp_ErrorMsg(MSG_SOME_TABLES_NOT_UPDATED_C_RUN_DATABASE_UPDATING_AGAIN)
    endif
    if this.pParam="AutoexecUpdate DB" and this.oParentObject.w_PARAM_MNGDB="AUTOEXEC"
      if Empty(msgwnd.Edt_err.Text)
        * --- Nessun errore: chiudo la finestra
        this.oparentobject.oparentobject.w_RETVAL = "ok: "
        msgwnd.btnOk.click()
      else
        * --- Restituisco l'errore sull'oggetto chiamante
        this.oparentobject.oparentobject.w_RETVAL = "ko: " + msgwnd.Edt_err.Text
      endif
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='cpttbls'
    this.cWorkTables[2]='cptsrvr'
    this.cWorkTables[3]='*mngdatabase'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_cpttbls')
      use in _Curs_cpttbls
    endif
    if used('_Curs_cpttbls')
      use in _Curs_cpttbls
    endif
    if used('_Curs_cptsrvr')
      use in _Curs_cptsrvr
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
