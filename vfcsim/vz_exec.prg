* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VZ_EXEC
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 06/03/2000
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Interprete per gli zoom
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

#Define TOOLBAR_BORDERCOLOR 55

Lparam cFile,oParentObject
Local o,h,l,c,xcg

If At('cp_ppx',Lower(Set('proc')))=0
    Set Proc To cp_ppx Additive
Endif
o=Createobject('vz_frm_exec',oParentObject)
If Vartype(cFile)<>'C' Or cFile='?'
    cFile=Getfile('vzm')
    cFile=Left(cFile,Len(cFile)-4)
Endif
If Lower(Right(cFile,4))='.vzm'
    cFile=Left(cFile,Len(cFile)-4)
Endif
If !cp_IsPFile(cFile+'.vzm')
    cp_msg(cp_MsgFormat(MSG_CANNOT_FIND_ZOOM__,cFile),.F.)
    Return(.Null.)
Endif
c=cp_GetPFile(cFile+'.vzm')
xcg=Createobject('pxcg','load',c)
o.Serialize(xcg)
o.cConfigFile=cFile+'.vzm'
o.Zoom.cZoomName=cFile
o.Show()
Return(o)

#Define PRG_BATCH 1
#Define PRG_CPOBJ 2
#Define PRG_RETURN 3

Define Class vz_frm_exec As CpSysform
    Width=400
    Height=250
    *closable=.f.
    oTool=.Null.
    Caption=""
    Icon=i_cBmpPath+i_cStdIcon
    oThis=.Null.
    * --- gestione del programma associato
    oCPObj=.Null.
    cCPPrg=''
    nCPPrgType=0
    bOptions=.T.
    cConfigFile=''

	*--- Zucchetti Aulla Inizio - Form decorator
	bApplyFormDecorator=.t.	
	*-- Zucchetti Aulla Fine - Form decorator

    *
    Add Object __sqlx__ As sqlx
    Add Object Zoom As zoombox With Height=240,Width=390,BorderWidth=0
    Proc Init(oParentObject)
        This.oThis=This
        This.Zoom.oParentObject=oParentObject
        This.Caption=cp_Translate(MSG_DRAW)
        If i_VisualTheme<>-1
            If i_bGradientBck
                With Thisform.ImgBackGround
                    .Width = Thisform.Width
                    .Height = Thisform.Height
                    .Anchor = 15 && Resize Width and Height
                    .Picture = i_ThemesManager.GetProp(11)
                    .ZOrder(1)
                    .Visible = .T.
                Endwith
            Endif
            Thisform.BackColor = i_ThemesManager.GetProp(7)
        Endif
    	*--- Form Decorator
    	IF This.bApplyFormDecorator And i_ThemesManager.GetProp(123)=0
    		This.AddObject("oDec", "cp_FormDecorator")	
        ELSE
            This.bApplyFormDecorator = .F.    		
    	ENDIF	
        Local cObjManyHeaderName
        cObjManyHeaderName="oManyHeader_"+This.Zoom.Name
        If i_AdvancedHeaderZoom And Type("this."+cObjManyHeaderName)<>"O"
            This.AddObject(cObjManyHeaderName,"ManyHeader")
            This.Zoom.oRefHeaderObject=This.&cObjManyHeaderName
            This.&cObjManyHeaderName .InitHeader(This.Zoom.grd,.T.)
        Endif
    Proc Destroy()
        Local i
        If Vartype(i_curform)='O' And !Isnull(i_curform) And i_curform.Name=Thisform.Name
            i_curform=.Null.
        Endif
        If !Isnull(This.oCPObj)
            If This.oCPObj.cFunction='Query'
                This.oCPObj.ecpQuit()
            Endif
            *this.oCPObj.ecpQuit()
        Endif
        This.oThis=.Null.
        This.oCPObj=.Null.
        Local cObjManyHeaderName
        cObjManyHeaderName="oManyHeader_"+This.Zoom.Name
        If i_AdvancedHeaderZoom And Type("this."+cObjManyHeaderName)="O"
            This.RemoveObject(cObjManyHeaderName)
        Endif
        Return
        * --
    Proc Activate()
        If This.nCPPrgType=PRG_CPOBJ
            oCpToolBar.SetQuery()
        Endif
        i_curform=This
        Return
    Func HasCPEvents(i_cOp)
        Return(.T.)
    Proc ecpQuit()
        This.Release()
    Proc ecpEdit()
        Local o,N
        Do Case
            Case This.nCPPrgType=PRG_RETURN
                If !Isnull(This.Zoom.oParentObject)
                    Select (This.Zoom.cCursor)
                    For i=1 To Fcount()
                        If This.Zoom.oParentObject.IsVar(Fields(i))
                            This.Zoom.oParentObject.SetVar(Fields(i),Eval(Fields(i)))
                            This.Zoom.oParentObject.NotifyEvent(Fields(i)+' Changed')  && Notifica che la variabile e'  cambiata
                        Endif
                        If This.Zoom.oParentObject.IsVar('w_'+Fields(i))
                            This.Zoom.oParentObject.SetVar('w_'+Fields(i),Eval(Fields(i)))
                            This.Zoom.oParentObject.NotifyEvent('w_'+Fields(i)+' Changed')  && Notifica che la variabile e'  cambiata
                        Endif
                    Next
                Endif
                This.Hide()
                This.Destroy()
            Case This.nCPPrgType=PRG_BATCH
                N=Substr(This.cCPPrg,2)
                &N(This)
            Case This.nCPPrgType=PRG_CPOBJ
                If Isnull(This.oCPObj) And !Empty(This.cCPPrg)
                    N=This.cCPPrg
                    This.oCPObj=&N()
                Endif
                o=This.oCPObj
                If !Isnull(o)
                    If o.cFunction='Query'
                        o.ecpFilter()
                        This.SetKeyWorkVars()
                        o.ecpSave()
                        o.ecpEdit()
                        o.Show()
                    Else
                        cp_msg(cp_Translate(MSG_OBJECT_IN_CHANGING_BS_LOADING_MODE),.F.)
                    Endif
                Endif
        Endcase
        Return
    Proc ecpLoad()
        Local o,N
        If This.nCPPrgType=PRG_CPOBJ
            If Isnull(This.oCPObj) And !Empty(This.cCPPrg)
                N=This.cCPPrg
                This.oCPObj=&N()
            Endif
            o=This.oCPObj
            If !Isnull(o)
                If o.cFunction='Query'
                    o.ecpLoad()
                    o.Show()
                Else
                    cp_msg(cp_Translate(MSG_OBJECT_IN_CHANGING_BS_LOADING_MODE),.F.)
                Endif
            Endif
        Endif
        Return
    Proc ecpDelete()
        cp_msg(cp_Translate(MSG_CANNOT_DELETE_IT),.F.)
        Return
    Proc ecpInfo()
        cp_msg(cp_MsgFormat(MSG_ZOOM_CL__,This.cConfigFile),.F.)
    Proc SetKeyWorkVars()
        * --- imposta la chiave nell' oggetto di arrivo
        Local o,i,c,k
        o=This.oCPObj
        Select (This.Zoom.cCursor)
        k=This.Zoom.cKeyFields
        i=At(',',k)
        Do While i<>0
            c=Left(k,i-1)
            o.w_&c = &c
            k=Substr(k,i+1)
            i=At(',',k)
        Enddo
        o.w_&k = &k
        Return
        *
    Proc Resize()
        This.Zoom.Width=This.Width
        This.Zoom.Height=This.Height
    Proc Serialize(xcg)
        Local N
        If xcg.bIsStoring
        Else
            This.Top=xcg.Load('N',0)
            This.Left=xcg.Load('N',0)
            This.Width=xcg.Load('N',0)
            This.Height=xcg.Load('N',0)
            This.Caption=cp_Translate(xcg.Load('C','Zoom'))
            This.cCPPrg=xcg.Load('C','')
            This.bOptions=xcg.Load('L',.T.)
            xcg.Eoc()
            This.Zoom.Serialize(xcg)
            This.Zoom.Query()
            This.Zoom.bOptions=This.bOptions
            This.Zoom.Resize()
            * --- inizializzazioni varie
            This.nCPPrgType=0
            This.SetPrgType()
        Endif
    Proc SetPrgType()
        Local N
        This.oCPObj=.Null.
        N=This.cCPPrg
        Do Case
            Case Empty(N)
                This.nCPPrgType=0 && nessun programma associato
            Case Left(N,1)='*'
                This.nCPPrgType=PRG_BATCH && batch da lanciare al return sulla riga
            Case Lower(N)='return'
                This.nCPPrgType=PRG_RETURN && oggetto painter
                This.WindowType=1
            Otherwise
                This.nCPPrgType=PRG_CPOBJ && oggetto painter
                *this.oCPObj=&n() && con questa riga apre la gestione contestualmente allo zoom
        Endcase
    Func IsVar(cVarName)
        Return(Type(This.Zoom.cCursor+'.'+cVarName)<>'U')
    Func GetVar(cVarName)
        cVarName=This.Zoom.cCursor+'.'+cVarName
        Return(&cVarName)
    Proc SetVar(cVarName,xValue)
        Return
        * ---
    Proc ecpF6()
        Return
    Proc ecpFilter()
        Return
    Proc ecpNext()
        Return
    Proc ecpPgUp()
        Return
    Proc ecpPgDn()
        Return
    Proc ecpPrint()
        Return
    Proc ecpPrior()
        Return
    Proc ecpSave()
        Return
    Proc ecpZoom()
        If This.Zoom.zz.Enabled
            This.Zoom.zz.Click()
        Endif
        Return
    Proc ecpZoomOnZoom()
        Return
    Proc ecpSecurity()
        Return
    Func GetHelpFile()
        Return(i_CpDic)
    Func GetHelpTopic()
        Return(This.Caption)
Enddefine

