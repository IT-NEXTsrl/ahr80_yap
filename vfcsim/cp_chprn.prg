* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_CHPRN
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Nestor J. Juncos
* Data creazione: 01/04/98
* Aggiornato il : 02/04/98
* Translated    : 01/03/2000
* #%&%#Build:  55
* ----------------------------------------------------------------------------
* Scelta Stampante/Export su file
* ----------------------------------------------------------------------------



#include "cp_app_lang.inc"
***** Zucchetti Aulla Inizio: Aggiunti Paramentro tReport

Parameter nReport, tipReport, noParent, nAutom
Private oPrint,i_p,i_FDFFile,RptTxt_Search
Private pd_PrintSystem,pd_Codice,pd_nAttivita,pd_Obbligatorio,pd_LogProcesso,pd_StrProcesso
Private pd_KeyIstanza,pd_nTotDoc,pd_nTotDocOk,pd_nTotDocKo,pd_ISOLangField
Private pd_StrControl,pd_AnteSN
Private tReport1 ,  noParentObject1 , nAzAutom1 , l_multireport1 , g_oPrintForm1 , i_LoadMenu1, bReportVuoto1, printsys
*PUBLIC tempo
*tempo=SECONDS()
***** Zucchetti Aulla Fine
** Dalla printsystem � stata lanciata un altra printsystem (le variabili globali vengono sovrascritte)
printsys=.F.
If Vartype(tReport)<>'U'
    tReport1=tReport
    noParentObject1=noParentObject
    nAzAutom1=nAzAutom
    l_multireport1=l_multireport
    g_oPrintForm1=g_oPrintForm
    i_LoadMenu1=i_LoadMenu
    bReportVuoto1=bReportVuoto
    printsys=.T.
Endif

Public tReport ,  noParentObject , nAzAutom , l_multireport , g_oPrintForm , i_LoadMenu, bReportVuoto

noParentObject=noParent
tReport=tipReport
nAzAutom=nAutom
bReportVuoto=.F.
If Vartype(nReport)="L"
    nReport = ''
Endif
*SET COVERAGE TO c:\LOGFOX
*--- Zucchetti Aulla Inizio - MultiReport
* --- Aggiunti riferimenti al tipo di Stampa tReport : 'S' Stampa formato solo Testo altrimenti grafica
If Vartype(tReport)<>"C"
    tReport = ' '
Endif
If Empty(tReport)
    tReport = ' '
Endif
* controllo report infobusiness
If tReport<>'I' And Type("nReport")='C' And Upper(Justext(nReport))=='IRP'
    tReport='I'
Endif
*--- Se nReport non � un oggetto allora devo gestire i multireport
If Vartype(nReport)='C'
    Local l_RepName, n_result, n_macro , l_cdescr
    l_RepName=nReport
    l_cdescr=""
    nReport=Createobject("MultiReport")
    *	PROCEDURE AddNewReport(cReportName, cCursor, oParamqry, oVarEnv, bNoReset, bMain, bText,;
    cSplit, cLink, cStmpOrder, nDettOrder, bChgPrinter, nIndex, bNoTrad)
    If Type("noParent.w_odes")='C'
        l_cdescr=noParent.w_odes
    ENDIF
    *se non sono nel multireport documentale l'informazione delle stampe solo testo non arriva dall'oggetto
    *quindi verifico l'estensione del file
    tReport= IIF(EMPTY(tReport)and UPPER(JUSTEXT(l_RepName))='FXP','S',tReport)
    n_result=nReport.AddNewReport(l_RepName, l_cdescr, "__tmp__", Iif(Type("i_paramqry")='O', i_paramqry, .Null.), .Null., .F., .T.,;
        tReport='S','','','',1, Iif(tReport<>"I", .T., .F.))
    If n_result=0 And Type('__tmp__.LUCODISO')='C'
        && Metto fisso l'alias del campo contenete il codice ISO della lingua in cui tradurre il report
        n_macro='__tmp__.LUCODISO'
        && Assegno il codice ISO della lingua alla variabile globale per essere gestito in modo appropriato nella combo delle lingue
        g_DEMANDEDLANGUAGE = Iif(Empty(g_DEMANDEDLANGUAGE),&n_macro,g_DEMANDEDLANGUAGE)
        nReport.SetCodISOrep (g_DEMANDEDLANGUAGE,nReport.getnumreport())
    Endif
Endif
*--- Apertura print system
Local oMReport, nIndex, bFirst
nIndex=1
nIndexold=1
nTotReport=0
bFirst = .T.
Do While nIndex<>0
    oMReport= .Null.
    oMReport=Createobject("MultiReport")
      IF nIndex<>0
  g_DEMANDEDLANGUAGE = IIF(EMPTY(nReport.GetCodISOrep (nIndex)) , "" ,nReport.GetCodISOrep (nIndex))
  endif
    nIndex=nReport.GetMRepPrnSysChange(@oMReport, nIndex)
   
    If bFirst
        oMReport.cError = nReport.cError
        bFirst = .F.
    Endif
    If oMReport.CountAllReportsNotSkipped()>0
    **controllo che i report dello stesso flusso siano tutti nella stessa lingua
    nIndexold=1
    nTotReport=oMReport.GetNumReport()
    DO WHILE nIndexold <= nTotReport
    	IF (g_DEMANDEDLANGUAGE <> oMReport.GetCodISOrep (nIndexold))
	    	g_DEMANDEDLANGUAGE =g_PROJECTLANGUAGE    
	    ENDIF
	    nIndexold =nIndexold  + 1
    ENDdo
       PrnSystem(oMReport, tReport, noParentObject, nAzAutom)
    Endif
Enddo

* Variabili publiche per mail
i_EMAIL = ' '
i_EMAILSUBJECT = ' '
i_EMAILTEXT = ' '
*--- Resetto variabile lingua
g_DEMANDEDLANGUAGE=""

**Se la print sysetm � staaa lanciata da un altra print system allora si ripristina il valore delle variabili globali e non si rilasciano
If printsys
    tReport=tReport1
    noParentObject=noParentObject1
    nAzAutom=nAzAutom1
    l_multireport=l_multireport1
    g_oPrintForm=g_oPrintForm1
    i_LoadMenu=i_LoadMenu1
    bReportVuoto=bReportVuoto1
Else
    Release tReport ,  noParentObject , nAzAutom,l_multireport , g_oPrintForm , i_LoadMenu , bReportVuoto
Endif
*AH_errormsg ("%1",,, SECONDS() - tempo )
Return

Procedure PrnSystem(oReport, tReport, noParentObject, nAzAutom)
    *--- Controllo presenza errori nella costruzione del MultiReport, propriet� cError valorizzata
    If Not Empty(oReport.cError)
        *--- Mostro errori e esco
        ah_ErrorMsg("%1", "stop", "", oReport.cError)
        Return
    Endif
    *--- Zucchetti Aulla Fine - MultiReport

    ***** Zucchetti Aulla Inizio
    *--- Controllo se l'utente ha i diritti per aprire la query InfoBusiness
    If tReport = 'I'
        l_sPrg="GSIR_BIC(.null., '', .t., LEFT(oReport.FirstReport(), LEN(oReport.FirstReport())-4))"
        If !&l_sPrg
            Return
        Endif
    Endif

    *--- Zucchetti Aulla Inizio - Controllo Temporaneo
    *--- Se _TMP_ � vuoto evindenzio un messaggio
    *--- Se report infobusiness non mostro messaggi
    If tReport<>'I' And oReport.IsEmptyCursor()  And At('.FDF',Upper (oReport.FirstReport()))=0
        *--- Il cursore � vuoto
        bReportVuoto=.T.
        If cp_isAdministrator()
            *--- Sono amministratore quindi do la possibilit� di scelta
            If cp_YesNo(MSG_NO_DATA_TO_PRINT_C_DISCARD_REPORT_QP)
                *--- ATTENZIONE - Lo schedulatore risponde sempre affermativamente alle domande, quindi per evitare
                *--- la stampa di pagine vuote occorre entrare nell'if rispondendo Si
                Return
            Endif
        Else
            *--- Non sono amministratore, dato che non posso modificare i report non ha senso far comparire la PrintSystem
            cp_ErrorMsg(MSG_NO_DATA_TO_PRINT,'i')
            Return
        ENDIF
    Else
        *--- Il cursore non � vuoto
        bReportVuoto=.F.        
    Endif
    *--- Zucchetti Aulla Fine - Controllo Temporaneo

    * --- Variabili private per stampe solo testo
    Private ts_Enabled
    Private ts_10Cpi,ts_12Cpi,ts_15Cpi,ts_Comp,ts_RtComp,ts_Reset,ts_Inizia,ts_ForPag,ts_RowPag
    Private ts_RowOk,ts_StBold,ts_StDoub,ts_StItal,ts_StUnde,ts_FiBold,ts_FiDoub,ts_FiItal,ts_FiUnde
    ts_Enabled = .F.
    ***** Zucchetti Aulla Fine
    *--- Zucchetti Aulla Inizio - MultiReport
    oReport.UpperReport()
  
    ***** Zucchetti Aulla Inizio
    Private oRepPlite
    * prima verifica se il report ha estensione FDF (ZTAM)
    i_FDFFile=At('.FDF',oReport.FirstReport())>0
    If i_FDFFile
        * --- Zucchetti Aulla Inizio - x Ricerca nell'archivio dei report di Postalite
        oRepPlite=oReport.FirstReport()
        * --- Zucchetti Aulla Fine - x Ricerca nell'archivio dei report di Postalite
        Declare Integer ShellExecute In shell32.Dll Integer nHwnd, String cOperation, String cFileName, String cParms, String cDir, Integer nShowCmd
        Declare Integer FindWindow In win32api String cNull, String cWinName
        oReport.SetExtReport('FDF', 1)
    Else
        *--- Forza estensione se diversa da '.FRX','.FXP','.XLT','.DOC','.STC','.OTS'
        oReport.ForceReportExt()
        Private CustFile
        * --- Zucchetti Aulla Inizio - x Ricerca nell'archivio dei report di Postalite
        oRepPlite = oReport.FirstReport()
        * --- Zucchetti Aulla Fine - x Ricerca nell'archivio dei report di Postalite
        *--- Cerca report nella custom
        If tReport<>'I'
            oReport.CheckCustomReport()
        Endif
    Endif

    * --- Verifica se esiste un processo documentale per il report corrente =======================================
    * --- Se si, crea cursore __pdtmp__ con i dati filtrati
    * --- check processo rilanciabile e verifica filtri avanzati
    pd_StrProcesso = ' '
    * Dichiaro nuova variabile per condizionare la stampa standard
    NoStampa = .F.

    If g_DOCM='S' And Not oReport.IsEmptyCursor()
        Local nomrep,l_ii
        pd_KeyIstanza  = Padl(Alltrim(Str(i_codute,5,0)),5,'_')+Sys(2015)
        Private DM_MReport
        DM_MReport = .Null.
        If CHKPROCD(oReport, @DM_MReport, pd_KeyIstanza, noParentObject, tReport='I')
			*--- Settaggio stampanti
            DM_MReport.SetAllPrinter()
            *--- Rimuove l'estensione dei report
            DM_MReport.RemoveReportExt()
            ***** Zucchetti Aulla Inizio: Modificata Classe derivata oPrint1 anziche' oPrint

            *oPrint = cp_CHP()

            l_multireport=DM_MReport
            If  i_bNewPrintSystem
                oPrint = CP_CHP3(DM_MReport)

            Else
                oPrint = CP_CHP()
            ENDIF
            **imposta come form corrente poiche a volte foxpro non lo setta            
            i_curform=oPrint
            oPrint.i_FDFFile=i_FDFFile
            If i_bNewPrintSystem
                oPrint.w_treport=Evl(Nvl(tReport,"")," ")
            Endif
            oPrint.noParentObject=noParentObject
            oPrint.nAzAutom=nAzAutom
            oPrint.cTipoReport = Upper(tReport)
            oPrint.cNomeReport = DM_MReport	&&Inserisco in oPrint.cNomeReport l'oggetto Multireport
            oPrint.oOrigMReport = oReport	&&Salvo il Multireport originale
            oPrint.pdObbligatorio = DM_MReport.bPDObbl
            oPrint.pdMantieniLog  = DM_MReport.bPDLog
            oPrint.cAnteprimaSN   = DM_MReport.bPDAnte
            * Propriet� particolari per processo documentale
            oPrint.pdProcesso     = Iif(!DM_MReport.bPDObbl, DM_MReport.bPDProc, .T.)
            oPrint.pdKeyIstanza   = pd_KeyIstanza
            oPrint.pdnTotDoc      = DM_MReport.nPDTotDoc
            oPrint.pdnTotDocOk    = DM_MReport.nPDTotDocOk
            oPrint.pdnTotDocKo    = DM_MReport.nPDTotDocKo
            oPrint.pdISOLang = DM_MReport.bPDISOLan
            oPrint.pdoParentObject= Iif(Vartype(noParentObject)='O',noParentObject,Null)
            If i_bNewPrintSystem
                oPrint.w_Txt_Copies=oPrint.cNomeReport.nNumCopy
            Endif
            ***** Zucchetti Aulla - INIZIO NOME FILE ALLEGATO: SE � STATO MODIFICATO IL DEFAULT SULLA PRINT SYSTEM PRENDE IL NUOVO FILE INSERITO;
            *****                            SE RIMANE IL DEFA+I_CODUTE PRENDE, SE ESISTE, LA DESCRIZIONE DELL'OUTPUT UTENTE
            Local L_MASK
            L_MASK=noParentObject
            Do While Type('L_MASK.oparentobject')='O' And Type('L_MASK.ccomment')<>'C'
                L_MASK=L_MASK.oParentobject
            Enddo

           	If Type('L_MASK.ccomment')='C' Or Type('L_MASK')='C'
		       	Local L_CAPTION, L_indexBS
		       	If Type('L_MASK')='O'
		       		L_CAPTION = Iif (Type('L_MASK.caption')='C',L_MASK.Caption,L_MASK.ccomment)
		       		L_indexBS = Rat(MSG_BS,m.L_CAPTION)
		       		If m.L_indexBS>0
		       			L_CAPTION = ALLTRIM(Left(m.L_CAPTION ,m.L_indexBS-1))
		       		Endif
		       	Else
		       		L_CAPTION = ALLTRIM(L_MASK)
		       	Endif
	            oPrint.CDESCREPORT = L_CAPTION
                oPrint.CDESCREPORT = oPrint.CDESCREPORT + Iif(Type('L_MASK.W_ODES')='C',' ('+Alltrim(L_MASK.w_odes)+')','')
                oPrint.prFile= path_print_system() + chkstrch(Chrtran(oPrint.CDESCREPORT, ' ', '_'), .T.) +'.'+oPrint.w_Opt_File &&Sostituisco gli spazi con _
            Else
                oPrint.CDESCREPORT = ''
            Endif
            oPrint.oTxt_File.Value=oPrint.prFile
            oPrint.w_Txt_File=oPrint.prFile
            L_MASK=.Null.

            LoadConfig(oPrint)
            oPrint.Notifyevent ("AggiornaZoom")
            IF TYPE("oPrint.w_Zoom_Printer")<>'U'
             DM_MReport.SetInfoZoom(oPrint.w_Zoom_Printer.CCURSOR)
             oPrint.oTxt_Device.enabled=DM_MReport.bStampanti
            Endif
            ***** FINE
            **Nasconde la visualizzazione per riaprire la finestra in visualizzazione modale
            oPrint.Hide()
            If(Type('g_SCHEDULER')='C' And g_SCHEDULER='S')
                If i_bNewPrintSystem
                    cp_chpfun (oPrint,'Schedulatore')
                Else
                    oPrint.Schedulatore()
                Endif
            Else
                ***Zucchetti Aulla-Automatizza printsystem Inizio***
                If Vartype(nAzAutom)='L' And Vartype(noParentObject)='N'
                    oPrint.pdnAzAutom=noParentObject
                    * Mantengo l'assegnamento per il controllo successivo
                    nAzAutom=noParentObject
                    noParentObject=.Null.
                Endif
                If Vartype(nAzAutom)='L'
                    oPrint.mcalc(.T.)
                    If  i_bNewPrintSystem
                    **inizializza il campo della stampante (le chiamate a setallprinter hanno configutrato correttamente l oggetto multireport)
                    GSUT_BRL (oPrint,'S',oPrint.cNomeReport) && aggiorna le informazioni della stampante nel multireport
                    cp_chpfun3(oPrint,'SStpClick') && aggiorna la combo box della stampante sulla print system
                    ENDIF
                    
                    oPrint.Show(1)
                    *SET COVERAGE TO
                Else
                    Automatizza(oPrint)
                Endif
                ***Zucchetti Aulla-Automatizza printsystem Fine***
            Endif
            && Rimuovo l'oggetto oPrint
            IF VARTYPE(oPrint)='O'
               oPrint.ecpquit()
            endif
            Release oPrint
            *Inibisco la stampa standard, poich� gi� gestita da DOCM
            NoStampa = .T.
        Endif
    Endif

    If ! NoStampa
        ***** Zucchetti Aulla Fine
		*--- Settaggio stampanti e REPORTBEHAVIOR, appena ho un report con behavior a 90 utilizzo 90
        oReport.SetAllPrinter()
        *--- Rimuove l'estensione dei report
        oReport.RemoveReportExt()
        *** Zucchetti Aulla inizio
        * RptTxt_Search  � utilizzato per ricercare eventuali associazioni report /stampanti
        * per stampe solo testo. Se la stampa � nella cartella CUSTOM allora la ricerca va svolta con
        * il nome originale passato alla CP_CHPRN e non con CUSTOM\<nome.Fxp>
        *!*	IF LEFT(nReport.FirstReport(),7)='CUSTOM\'
        *!*		RptTxt_Search=SUBSTR(nReport.FirstReport(),8,LEN(nReport.FirstReport())-7)
        *!*	ELSE
        *!*		RptTxt_Search=nReport.FirstReport()
        *!*	Endif
        *** Zucchetti Aulla Fine

        *
        ***** Zucchetti Aulla Inizio: Modificata Classe derivata oPrint1 anziche' oPrint
        If i_bNewPrintSystem
            l_multireport=oReport
            oPrint = CP_CHP3(oReport)
        Else
            oPrint = CP_CHP()
        ENDIF
        **imposta come form corrente poiche a volte foxpro non lo setta
        i_curform=oPrint
        oPrint.i_FDFFile=i_FDFFile
        If i_bNewPrintSystem
            oPrint.w_treport=Evl(Nvl(tReport,"")," ")
        Endif
        oPrint.noParentObject=noParentObject
        oPrint.nAzAutom=nAzAutom
        oPrint.cTipoReport = Upper(tReport)
        oPrint.cNomeReport = oReport	&&Inserisco in oPrint.cNomeReport l'oggetto Multireport
        If i_bNewPrintSystem
            oPrint.w_Txt_Copies=oPrint.cNomeReport.nNumCopy
        Endif
        ***** Zucchetti Aulla - INIZIO NOME FILE ALLEGATO: SE � STATO MODIFICATO IL DEFAULT SULLA PRINT SYSTEM PRENDE IL NUOVO FILE INSERITO;
        *****                            SE RIMANE IL DEFA+I_CODUTE PRENDE, SE ESISTE, LA DESCRIZIONE DELL'OUTPUT UTENTE
        Local L_MASK
        L_MASK=noParentObject
        Do While Type('L_MASK.oparentobject')='O' And Type('L_MASK.ccomment')<>'C'
            L_MASK=L_MASK.oParentobject
        Enddo
        
       	If Type('L_MASK.ccomment')='C' Or Type('L_MASK')='C'
	       	Local L_CAPTION, L_indexBS
	       	If Type('L_MASK')='O'
	       		L_CAPTION = Iif (Type('L_MASK.caption')='C',L_MASK.Caption,L_MASK.ccomment)
	       		L_indexBS = Rat(MSG_BS,m.L_CAPTION)
	       		If m.L_indexBS>0
	       			L_CAPTION = ALLTRIM(Left(m.L_CAPTION ,m.L_indexBS-1))
	       		Endif
	       	Else
	       		L_CAPTION = ALLTRIM(L_MASK)
	       	Endif
            oPrint.CDESCREPORT = L_CAPTION
            oPrint.CDESCREPORT = oPrint.CDESCREPORT + Iif(Type('L_MASK.W_ODES')='C',' ('+Alltrim(L_MASK.w_odes)+')','')
	    If IsAlt() And  Type('L_MASK')='O' And (Upper(L_MASK.class)='TGSVE_MDV' Or (Upper(L_MASK.class)='TGSAL_KRD' And L_MASK.w_ParStampa='N')) 
	       oPrint.prFile=Chrtran(Alltrim(L_MASK.w_DESDOC),' ', '_')+'_n.'+Alltrim(str(L_MASK.w_MVNUMDOC))+iif(!empty(L_MASK.w_MVALFDOC), '_'+Alltrim(L_MASK.w_MVALFDOC), '')+'_del_'+Chrtran(Dtoc(L_MASK.w_MVDATDOC), '/', '.')+'.'+oPrint.w_Opt_File
	    Else
               oPrint.prFile=path_print_system()+chkstrch(Chrtran(oPrint.CDESCREPORT, ' ', '_'), .T.) +'.'+oPrint.w_Opt_File  &&Sostituisco gli spazi con _
	    Endif
            oPrint.w_Txt_File=oPrint.prFile
        Else
            oPrint.CDESCREPORT = ''
        Endif
        oPrint.oTxt_File.Value=oPrint.prFile
        oPrint.w_Txt_File=oPrint.prFile
        L_MASK=.Null.
        LoadConfig(oPrint)
        oPrint.Notifyevent ("AggiornaZoom")
        IF TYPE("oPrint.w_Zoom_Printer")<>'U'
            oReport.SetInfoZoom(oPrint.w_Zoom_Printer.CCURSOR)
            oPrint.oTxt_Device.enabled=oReport.bStampanti
        Endif
        **Nasconde la visualizzazione per riaprire la finestra in visualizzazione modale
        oPrint.Hide()
        *--- Zucchetti Aulla Inizio - MultiReport
        *--- Se si verifica un errore durante la fascicolazione non apro la print system
        If Isnull(oPrint)
            Return
        Endif
        *--- Zucchetti Aulla Fine - MultiReport
        ***** FINE

        ****Zucchetti Aulla-Schedulatore Inizio****
        If(Type('g_SCHEDULER')='C' And g_SCHEDULER='S')
            If i_bNewPrintSystem
                cp_chpfun (oPrint,'Schedulatore')
            Else
                oPrint.Schedulatore()
            Endif
        Else
            ***Zucchetti Aulla-Automatizza printsystem Inizio***
            If Vartype(nAzAutom)='L' And Vartype(noParentObject)='N'
                oPrint.pdnAzAutom=noParentObject
                * Mantengo l'assegnamento per il controllo successivo
                nAzAutom=noParentObject
                noParentObject=.Null.
            Endif
            If Vartype(nAzAutom)='L'

                oPrint.mcalc(.T.)
                If  i_bNewPrintSystem
                **inizializza il campo della stampante (le chiamate a setallprinter hanno configutrato correttamente l oggetto multireport)
                GSUT_BRL (oPrint,'S',oPrint.cNomeReport) && aggiorna le informazioni della stampante nel multireport
                cp_chpfun3(oPrint,'SStpClick') && aggiorna la combo box della stampante sulla print system
                ENDIF
                oPrint.Show(1)
                *SET COVERAGE TO
            Else
                Automatizza(oPrint)
            Endif
            ***Zucchetti Aulla-Automatizza printsystem Fine***
            && Rimuovo l'oggetto oPrint
            Release oPrint
        Endif
        ****Zucchetti Aulla-Schedulatore Fine****
    Endif &&! NoStampa
    if VARTYPE(g_maskcall)='C' AND lower(g_maskcall)<>'tgswd_sdp' AND i_VisualTheme=-1 AND (i_cViewMode="A" Or i_cViewMode="M")  
            * --- Riassegna la vecchia windowstate alla maschera chiamante (la print system � rimpicciolita per problemi di adattamento che causavano fatal error)
            X = 1
            CONTA = 0
            do while x <= _Screen.FORMCOUNT
              if LOWER(_Screen.FORMS(x).BaseClass)="form" AND _Screen.FORMS(x).MDIFORM and LOWER(_Screen.FORMS(x).CLASS)<>"tcp_chp3"
                CONTA = CONTA +1
                if LOWER(_Screen.FORMS(X).CLASS)<>"tcp_chp3"
                  * --- Ribadisco il controllo perch� al primo cambio del windowstate l'oggetto su cui � puntato torna a essere il cp_chp3
                  if CONTA=1
                    RIFFORM = _Screen.FORMS(X)
                  endif
                  _Screen.FORMS(X).WindowState= IIF(!(VARTYPE(g_OLDSTATE)='N'),0,g_OLDSTATE) 
                endif
                if i_cViewMode="M" OR g_OLDSTATE=2
                  if CONTA=2
                    RIFFORM.Show()
                  endif
                endif
              endif
              X = X +1
            enddo
          endif
Endproc


