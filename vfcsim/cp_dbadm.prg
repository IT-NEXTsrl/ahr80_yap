* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_DBADM
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 01/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Operazioni di amministrazione del database
* ----------------------------------------------------------------------------
#include "cp_app_lang.inc"

#Define SQL_DROP                          1
#Define SQL_NTS                         (-3)
#Define TABLEPROPDIM 8
* --- i_TableProp: Array Publico per la gestione delle Tabelle
*     1 = Nome Logico
*     2 = Nome Fisico
*     3 = Nro. Connessione (0 = Database Locale Visual Foxpro)
*     4 = Contatore Nro. Aperture
*     5 = Nro. Server (Se esiste si deve aprire la connesione all'utilizzo)
*     6 = Tabella temporanea
*     7 = 0 - tabella da controllare, 1 - tabella con CPCCCHK, -1 - tabella senza il campo CPCCCHK
*     8 = 0 - tabella da controllare, 1 - tabella con UTDC, -1 - tabella senza il campo UTDC

#Define SERVERCONNDIM 8
* --- i_ServerConn: Array Publico per la gestione dei Server
*     1 = Nome Server
*     2 = Nro. Connessione ODBC
*     3 = ODBC Data Source
*     4 = Quando Aprire il Database (1=Partenza, 2=Programma, 3=F10)
*     5 = Contatore Nro. Aperture
*     6 = Tipo di database (Oracle, SQLServer, Access, VFP, DB2, SAPDB, Interbase, PostgreSQL, MySQL, DBMaker ...)
*     7 = usa/non usa Post-IN e warnings
*     8 = Propriet� della connessione da utilizzare nel caso di caduta

#Define SYSTABLES 9 && Zucchetti Aulla - Monitor framework aggiunge una tabella
* --- il numero di tabelle di sistema automaticamente aggiunte alla lista

Func cp_OpenDatabase()
	* --- definizione di True e False
	Public True,False,pConn
	True=.T.
	False=.F.
	Local i_ok
	cp_SetLanguageSystem()
	*--- Zucchetti Aulla Monitor framework -Setup tabella FrmWkMonitor (CP_MONITOR.dbf)
	If i_MonitorFramework
		cp_SetFWMonitor()
	Endif
	* --- apre il database principale
	If Empty(i_ServerConn[1,3])
		i_ok=cp_OpenDatabaseVFP()
	Else
		i_ok=cp_OpenDatabaseODBC()
	Endif
	If Not(i_ok)
		*wait window "Impossibile aprire il database principale"
		Return(.F.)
	Endif
	* --- controlla se e' una prima amministrazione
	* --- il check sulla ricostruzione per le traduzioni del dato va svolto solo se esistono le tabelle di sistema
	If (cp_VerifySystemTables(i_ServerConn[1,2]) And cp_ListLangModified()) Or Not(cp_VerifySystemTables(i_ServerConn[1,2])) Or Not(cp_UpdatedPlanCheck(i_ServerConn[1,2]))
		* --- mancano delle tabelle di sistema, prima amministrazione
		If !cp_ReadXdc()
			Return(.F.)
		Endif
		* --- prima di eseguire la creazione da zero, tenta di lanciare la procedura di install.
		*     se torna .f. si ferma
		If !cp_dbinst()
			Return(.F.)
		Endif
		*---
		If cp_OpenSchema('exclusive')
			cp_CreateSystemTables(i_ServerConn[1,2],i_ServerConn[1,6])
			cp_ManageOpenDB(i_dcx)
			cp_OpenSchema('shared noupdate')
		Else
			cp_msg(cp_Translate(MSG_CANNOT_OPEN_DATABASE_WITH_ADMIN_RIGHTS),.F.)
			Return(.F.)
		Endif
		*---
	Else
		* --- segnala che non e' stata eseguita l' amministrazione in apertura
		If Type('bexternaldatabaseadmin')<>'U'
			bexternaldatabaseadmin=.F.
		Endif
	Endif
	* --- carica array server e tabelle
	i_ok=cp_LoadServerArray()
	i_ok=cp_LoadTableArray()
	If Not(i_ok)
		cp_msg(cp_Translate(MSG_CANNOT_LOAD_FILES_TABLE),.F.)
		Return(.F.)
	Endif
	i_ok=cp_OpenServers()
	If Not(i_ok)
		cp_msg(cp_Translate(MSG_CANNOT_OPEN_ALL_SERVERS),.F.)
		*=cp_CloseServers() && continua, per poter fare amministrazione, anche se non tutti i server sono "aperti"
		*return(.f.)
	Endif
	*--- Zucchetti Aulla inizio Monitor Framework
	* Controllo presenza campo userright nella tabella cpusers
	*---
	check_cpusersfieldright()
	*--- Zucchetti Aulla Fine Monitor Framework
	Return(.T.)

	*------------------------- Crea dbf con informazioni Framework monitor
Proc cp_SetFWMonitor()
	If Not(cp_fileexist(Sys(5)+Sys(2003)+'\custom\userdir\cp_monitor.dbf'))
		If Not Directory(Sys(5)+Sys(2003)+'\custom\userdir')
			Md Sys(5)+Sys(2003)+'\custom\userdir\'
		Endif
		Create Table Sys(5)+Sys(2003)+'\custom\userdir\CP_MONITOR.dbf' Free (FILENAME C (254), FILEDESC C(80), FLSTATUS C(1), FLCUSTOM C(1), FILETYPE C(4), FLDESUTE C(20), FLDESGRP C(20), FLDESAZI C(40), FLORGNAM C (254),  FLCODUTE N(6),  FLCODGRP N(4),  FLCODAZI C(5), FLREFFIL C(254), FL_ERASE C(1), FLMODFIL C(1), FLNEWFIL C(1), FLREVFIL N(2,1), FLAUTHOR C(50), FL_NOTES M)
		Use In ('cp_monitor')
	Endif
	If Not(Used('cp_monitor'))
		Use Sys(5)+Sys(2003)+'\custom\userdir\cp_monitor.dbf' Shared In 0
	Endif

	*---
	* variabili pubbliche utilizzate principalmente dalla funzione loadfile
	* dichiarate qui per evitare l'esecuzione delle istruzioni per la parametrizzazione
	* degli indici dei campi della tabella cp_monitor
	* (sono usate come discriminanti)
	*---
	Public i_defaultPath, i_usrPath, i_customPath, i_varPathNew
	Public i_pathFindFile, MSG_FILEEXIST , MSG_NOT_FILEEXIST , MSG_NOT_ABS
	Public Array i_ext(13,2)

	* variabile che contiene il percorso dei file cercati cp_ispfile e cp_findnameddefaultfilemenu
	i_pathFindFile = ""
	* variabili di esito delle ricerche
	MSG_FILEEXIST = "MSG_FILEEXIST"
	MSG_NOT_FILEEXIST = "MSG_NOT_FILEEXIST"
	MSG_NOT_ABS = "MSG_NOT_ABS"
	If Type("i_stddir")='U'
		Public Array i_stddir(1,2)
	Endif
	* popolo l'array stddir con le cartelle standard e anche la variabile var_path_new
	i_varPathNew = cp_monitor_GetStdDir(@i_stddir)

	* Il secondo campo dell'array indica se l'estensione � da utilizzare nella procedura
	i_ext[1,1] = "vqr" && visual query
	i_ext[1,2] = .T.
	i_ext[2,1] = "vfm" && visual mask
	i_ext[2,2] = .T.
	i_ext[3,1] = "vgr" && grafici
	i_ext[3,2] = .T.
	i_ext[4,1] = "vmn" && visual men�
	i_ext[4,2] = .T.
	i_ext[5,1] = "vzm" && visual zoom
	i_ext[5,2] = .T.
	i_ext[6,1] = "_vzm" && visual zoom
	i_ext[6,2] = .T.
	i_ext[7,1] = "frx" && report
	i_ext[7,2] = .T.
	i_ext[8,1] = "doc" && modello word
	i_ext[8,2] = .T.
	i_ext[9,1] = "xlt" && modello excel
	i_ext[9,2] = .T.
	i_ext[10,1] = "stc" && modello openoffice Calc
	i_ext[10,2] = .T.
	i_ext[11,1] = "sxw" && modello openoffice Writer
	i_ext[11,2] = .T.
	i_ext[12,1] = "xdc" && aggiunta campi
	i_ext[12,2] = .T.
	i_ext[13,1] = "vrt" && visual real-time
	i_ext[13,2] = .T.

	*---
	* elenco variabili pubbliche dichiarate nella procedura 'aggiorna zoom'
	*---
	i_defaultPath = Addbs(Sys(5)+Sys(2003))
	i_customPath =".\custom\"
	i_usrPath = i_customPath+"userdir\"
Endproc

*---
* Zucchetti Aulla inizio - monitor Framework
* Controlla l'esistenza del campo userright nella tabella cpusers
* Se non esiste lo aggiunge
*---
Function check_cpusersfieldright()
	Local _right_cursor_
	If i_ServerConn[1,2]<>0
		*Abbiamo la connessione
		*Controlliamo se il campo userright esiste
		=cp_SQL(i_ServerConn[1,2],"select * from cpusers",'_right_cursor_')
		Used('_right_cursor_')
		If Type('_right_cursor_.userright')!='N'
			* Aggiungiamo il campo
			=cp_SQL(i_ServerConn[1,2],"alter table cpusers add userRight "+db_FieldType(cp_dbtype,'N',1,0),.F.,.F.,.T.)
			=cp_SQL(i_ServerConn[1,2],"update cpusers set userright='5'")
		Endif
		Use In Select('_right_cursor_')
	Else
		Select * From cpusers Into Cursor _right_cursor_
		Used('_right_cursor_')
		If Type('_right_cursor_.userright')!='N'
			Alter Table cpusers Add userright Numeric(1,0)
			Update cpusers Set userright='5'
		Endif
		Use In Select('_right_cursor_')
	Endif
Endfunc
* Zucchetti Aulla Fine - monitor Framework

Proc cp_SetLanguageSystem()
	If Vartype(i_cLanguage)='U'
		Public i_cLanguage
		i_cLanguage=''
	Endif
	If Vartype(i_cLanguageData)='U'
		Public i_cLanguageData
		i_cLanguageData=''
	Endif
	* --- Zucchetti Aulla Inizio (la CP_LANG � sempre creata alla'avvio)
	If Not(cp_fileexist(Sys(5)+Sys(2003)+'\..\dictionary\cp_lang.dbf'))
		If Not Directory(Sys(5)+Sys(2003)+'\..\dictionary\')
			Md Sys(5)+Sys(2003)+'\..\dictionary\'
		Endif
		Create Table Sys(5)+Sys(2003)+'\..\dictionary\cp_lang.dbf' Free (Language C(3),orig_str C(200), tran_str C(200))
		Index On orig_str+Language Tag idx1 COLLATE "MACHINE"
		Use In cp_lang
	Endif
	* --- Zucchetti Aulla Fine (la CP_LANG � sempre creata alla'avvio)
	If Not(Used('cp_lang'))
		Select 0
		Use Sys(5)+Sys(2003)+'\..\dictionary\cp_lang.dbf'
		If Len(orig_str)<200
			Local i_olderr
			Private i_err
			i_err=.F.
			i_olderr = On("ERROR")
			On Error i_err= .T.
			Use
			Use Sys(5)+Sys(2003)+'\..\dictionary\cp_lang.dbf' Exclusive
			If !i_err
				Alter Table Sys(5)+Sys(2003)+'\..\dictionary\cp_lang.dbf' Alter Column orig_str Char(200) Not Null
			Endif
			Use
			Use Sys(5)+Sys(2003)+'\..\dictionary\cp_lang.dbf'
			On Error &i_olderr
		Endif
	Endif
	*select cp_lang
	*set order to 1
	Return

Proc cp_LanguageTranslateData(bCalcAzi)
	**********************************************************
	*** Creo array con lingue per traduzione dati
	If Type("i_aCpLangs")='U'
		Public i_aCpLangs
		Dimension i_aCpLangs[1]
		i_aCpLangs[1]=''
		*** Zucchetti Aulla - modifica perch� la nostra tabella delle lingue � aziendale
		Public i_aCpLangsPainter
		Dimension i_aCpLangsPainter[1]
		i_aCpLangsPainter[1]=''		
		* --- Recupero solo le lingue con il check attivo...
		If i_ServerConn[1,2]<>0
			If bCalcAzi
				SQLExec(i_ServerConn[1,2],"select distinct code from cplangs where code in (select LUCODISO from "+cp_SetAzi(i_TableProp[cp_TablePropScan("LINGUE"),2])+" where LUABTRDA="+cp_tostrodbc('S')+") and datalanguage="+cp_tostrodbc('S'),"_curs_cplangs")
				SQLExec(i_ServerConn[1,2],"select distinct code from cplangs where datalanguage="+cp_tostrodbc('S'),"_curs_cplangspainter")
			Else
				SQLExec(i_ServerConn[1,2],"select distinct code from cplangs where datalanguage="+cp_tostrodbc('S'),"_curs_cplangs")
			Endif
		Else
			Select Distinct Code From cplangs Where datalanguage='S'Into Cursor _curs_cplangs
		Endif
		If Used("_curs_cplangs")
			Select * From _curs_cplangs Into Array i_aCpLangs
			If bCalcAzi and Used("_curs_cplangspainter")
				Select * From _curs_cplangsPainter Into Array i_aCpLangsPainter
				use in _curs_cplangspainter
			else
				Select * From _curs_cplangs Into Array i_aCpLangsPainter
			endif
			use in _curs_cplangs
		Endif
	Endif
	Return

Func cp_dbinst()
	Local i_res,i_cOldErr
	Private i_err
	i_err=''
	i_cOldErr=On('ERROR')
	On Error i_err=Iif(Empty(i_err),Message(),i_err)
	i_res=cp_inst()
	If "cp_inst"$Lower(i_err)
		i_res=.T.
	Endif
	On Error &i_cOldErr
	Return(i_res)

Func cp_OpenDatabaseVFP()
	Local i_ok,i_SPPlanDate
	Private i_res
	Public i_compiledprog
	* --- Controllo Versione Visual FoxPro
	If Version(5)>=800
		Set ENGINEBEHAVIOR 70      && Per eseguire le query come VFP 7
		Set Notify Cursor Off      && Evitiamo che vengano mostrati i messaggi nella toolbar in basso
	Endif
	i_res = .T.
	i_compiledprog = .F.
	If Not(File(i_CpDic+'.dbc'))
		Create Database (i_CpDic)
	Endif
	* --- apre il database di VFP
	On Error i_res=.F.
	Open Data (i_CpDic) Shared Noupdate
	i_ok=i_res
	Set Proc To ("cp_dbprc") Additive && non deve essere inclusa automaticamente nell' eseguibile
	If i_ok
		i_ServerConn[1,2]=0
		i_ServerConn[1,5]=0
		i_ServerConn[1,6]='VFP'
	Endif
	* --- verifica le "pseudo" stored procedures
	If i_ok
		* --- trova se la procedura e' stata compilata o viene eseguita dall' ambiente di sviluppo VFP
		On Error i_compiledprog=(Error()=1001)
		Compile Database (i_CpDic)
		If i_compiledprog
			i_SPPlanDate='???'
			i_SPPlanDate=cp_SPPlanDate()  && potrebbe dare errore se le stored procedures non esistono
			If cp_CheckStoredProcDate(i_SPPlanDate)
				cp_msg(cp_Translate(MSG_DATA_DICTIONARY_AND_STORED_PROCEDURES_INCOMPATIBLE),.F.)
				i_ok=.F.
			Endif
		Endif
	Endif
	On Error
	Return(i_ok)

Func cp_CheckStoredProcDate(i_date)
	Local d
	d=cp_GetXdcDate(i_CpDic)
	If d='?'
		Return(.F.)
	Endif
	If  Left(i_date,14)<>d
		Return(.T.)
	Endif
	If Vartype(i_cModules)='C' And Not(Empty(i_cModules))
		Local i_p,i_m,i_mm
		i_m=i_cModules
		i_p=At(',',i_m)
		Do While i_p<>0
			i_mm=Left(i_m,i_p-1)
			If cp_UpdatedModuleCheck(i_date,i_mm,cp_GetXdcDate(cp_FindModule(i_mm)),.T.)
				Return(.T.)
			Endif
			i_m=Substr(i_m,i_p+1)
			i_p=At(',',i_m)
		Enddo
		If cp_UpdatedModuleCheck(i_date,i_m,cp_GetXdcDate(cp_FindModule(i_m)),.T.)
			Return(.T.)
		Endif
	Endif
	Return(.F.)

Function cp_OpenDatabaseConnection(i_cConnection)
	Local i_nConn,i_rm
	***** Zucchetti Aulla Inizio - Termina in modo silente se il DBMS non � in linea
	**Quando si utilizza lo schedulatore e ADHOC viene avviato come servizio pu� succedere che il DBMS non sia ancora attivo
	**percio in questo caso si vuole che la procedura termini in modo che window possa ritentare l esecuzione del servizio
	**La variabile i_TERMINASILENTE serve per far terminare l applicazione in modo silente nel caso in cuin il DBMS non sia ancora attivo
	**i_TERMINASILENTE v� usato con una stringa di connessione nel caso in cui si avvia la procedura come servizio per lo schedulatore
	If Vartype(i_TERMINASILENTE)='C' And i_TERMINASILENTE='S'
		SQLSetprop(0, "DispLogin", 3 )
	Endif
	***** Zucchetti Aulla Fine
	Do Case
		Case Upper(Left(i_cConnection,7))='REMOTE:'
			Set Proc To cp_remoteconn Additive
			i_rm=Createobject("remotedb",i_cConnection)
			If i_rm.nConn<>-1
				If Type("i_nRemoteConn")="U"
					Public i_nRemoteConn,i_oRemoteConn
					i_nRemoteConn=1
				Else
					i_nRemoteConn=i_nRemoteConn+1
				Endif
				Dimension i_oRemoteConn[i_nRemoteConn]
				i_oRemoteConn[i_nRemoteConn]=i_rm
				i_nConn=1000000+i_nRemoteConn
			Else
				i_nConn=-1
			Endif
		Case ("DRIVER"$Upper(i_cConnection) Or "DSN"$Upper(i_cConnection)) And ";"$i_cConnection
			i_nConn=Sqlstringconnect(i_cConnection)
		Otherwise
			i_nConn=SQLConnect(i_cConnection)
	Endcase
	***** Zucchetti Aulla Inizio - Termina in modo silente se il DBMS non � in linea
	If Vartype(i_TERMINASILENTE)='C' And i_TERMINASILENTE='S' And i_nConn=-1
		Return -1
	Endif
	***** Zucchetti Aulla Fine
	cp_PostOpenConn(i_nConn)
	Return(i_nConn)

Procedure cp_PostOpenConn(i_nConn)
	Local i_dbtype,i_nResult
	If i_nConn>0
		i_dbtype=Lower(SQLXGetDatabaseType(i_nConn))
		If 'oracle'$i_dbtype
		
			cp_sqlexec(i_nConn,"Alter Session SET NLS_TERRITORY = AMERICA")
            If Vartype(g_ORACLEORDER_CI)='L' And g_ORACLEORDER_CI
            ****influisce sull'ordinamento delle stringhe
               cp_sqlexec(i_nConn,"ALTER SESSION SET NLS_SORT=BINARY_CI")    	
            Endif
			If Vartype(g_ORACLECASEINSENSITIVE)='L' And g_ORACLECASEINSENSITIVE
			 ****influisce su come vengono fatti i confronti
				cp_sqlexec(i_nConn,"ALTER SESSION SET NLS_COMP=LINGUISTIC")
			Endif
			* --- Zucchetti Aulla Inizio - Bind variable
			If Vartype(g_BINDVARIABLE)<>'L' Or g_BINDVARIABLE
				cp_sqlexec(i_nConn,'ALTER SESSION SET CURSOR_SHARING="FORCE"')
			Endif
			If Vartype(g_ORACLE_UNICODE)='L' And g_ORACLE_UNICODE=.T.
				cp_sqlexec(i_nConn,"Alter Session SET NLS_LENGTH_SEMANTICS= CHAR")
			Endif
			* --- Zucchetti Aulla Inizio Fine
		Endif
		If Lower(i_dbtype)='mysql'
			cp_sqlexec(i_nConn,'SET GLOBAL sql_mode = "PIPES_AS_CONCAT"')
		Endif
		***** Zucchetti Aulla Inizio
		* Gestione Accesso sicurtato (application roles)
		If i_dbtype="sqlserver" And Type('g_APPROLE')<>'U' And Not Empty(g_APPROLE) And Type('g_CRYPT')='C' And g_CRYPT='A'
			i_nResult=AH_ApplicationRole(i_nConn)
		Endif
		***** Zucchetti Aulla Inizio - Problema con la configurazione di SQLServer
		* � fondamentale avere l'impostazione xact_abort spenta perch� gli errori su istruzioni sql farebbero rollback automatica
		If i_dbtype='sqlserver'
			cp_sqlexec(i_nConn,'set xact_abort off')
		Endif
		***** Zucchetti Aulla Fine
		*--- Zucchetti Aulla inizio - su db Postgres forzo standard_conforming_strings a ON
		* standard_conforming_strings=ON (default a partire da PostgreSQL 9.1) => non si deve raddoppiare il carattere \
		* standard_conforming_strings=OFF => si deve raddoppiare il carattere \ Es. "query\gsve_ord.vqr" si scrive "query\\gsve_ord.vqr"
		If i_dbtype="postgresql"		
			SQLExec(i_nConn,"set standard_conforming_strings=on ")
		ENDIF
		*--- Zucchetti Aulla fine - impostazione standard_conforming_strings di Postgres
	Endif
	Return

Func cp_OpenDatabaseODBC()
	Private i_nConn,i_cDB
	i_nConn=cp_OpenDatabaseConnection(i_ServerConn[1,3])
	If i_nConn<>-1
		i_ServerConn[1,2]=i_nConn
		i_ServerConn[1,5]=0
		i_cDB=SQLXGetDatabaseType(i_nConn)
		If Empty(i_ServerConn[1,6])
			i_ServerConn[1,6]=i_cDB
			cp_dbtype=i_cDB
		Endif
		If i_cDB<>i_ServerConn[1,6]
			cp_msg(cp_MsgFormat(MSG_DETECTED_DATABASE__C_YOU_SAID_IT_WAS__,i_cDB,i_ServerConn[1,6]),.F.)
		Endif

		*--- Riconnessione automatica - mi salvo propriet� connessione per ritentare connessione nel caso di scollegamento
		i_ServerConn[1,8]=SQLGetprop(i_nConn,'ConnectString')

		*--- Riconnessione automatica problema Oracle di modifica parametri di connessione
		If 'oracle'$Lower(SQLXGetDatabaseType(i_nConn))
			param_connect=i_ServerConn[1,8]
			SQLDisconn(i_nConn)
			i_nConn=Sqlstringconnect(param_connect)
			cp_PostOpenConn(i_nConn)
		Endif

		*--- Zucchetti Aulla inizio - Verifico la correttezza dei parametri di connessione db Postgres
		If i_cDB="PostgreSQL"
			* Verifico la correttezza dei parametri di connessione
			Local i_msgerr
			i_msgerr=ah_CheckODBCPar(i_ServerConn[1,8])
			If Not Empty(i_msgerr)
			    ah_errormsg("Parametri di connessione ODBC non corretti:%0%1","STOP",,i_msgerr)
			    Return .F.
			Endif
		Endif
		*--- Zucchetti Aulla fine - Verifico la correttezza dei parametri di connessione db Postgres

		pConn=i_nConn
		*---  Controllo Single User
		*--- Solo se SQLServer
		Local l_Db_Name, CurAppo, err, l_errsav, l_version
		If i_cDB="SQLServer" And (Type('i_bDisableAsyncConn')='U' Or !i_bDisableAsyncConn)
			*--- Determino nome DB
			l_errsav=On('ERROR')
			On Error err=-1
			CurAppo=Sys(2015)
			err=cp_SQLEXEC(i_nConn, "select distinct DB_NAME() as name from cpttbls where filename='cpusers'", CurAppo)
			If err>0
				Select(CurAppo)
				l_Db_Name=Alltrim(Name)
				Use In Select(CurAppo)
				*--- Determino se il db � in single user
				l_version = Alltrim(SQLXGetDatabaseVersion(i_nConn))
				If VAL(Left(l_version ,3))>=11
					*--- In sql 2012 sp_dboption � deprecato
					err=cp_SQLEXEC(i_nConn, "SELECT user_access_desc FROM sys.databases WHERE name = '"+l_Db_Name+"'", CurAppo)
					If err>0
						Select(CurAppo)
						l_bSingle = (Upper(Alltrim(user_access_desc))=="SINGLE_USER")
						Use In Select(CurAppo)
						If l_bSingle And cp_yesno(MSG_DATABASE_IN_SINGLE_USER)
							*--- Elimino Single User
							err=cp_SQLEXEC(i_nConn, "ALTER DATABASE ["+ALLTRIM(l_Db_Name)+"] set MULTI_USER", CurAppo)
							If err<0
								*--- Non riesco ad eliminare Single User
								cp_errormsg(cp_MsgFormat(MSG_CONTACT_ADMINISTRATOR__, Message()),"!","",.F.)
							Endif
						Endif
					Else
						*--- Non posso determinare se in Single User, non ho i diritti?
						*cp_errormsg("Non posso determinare se il DB � Single User","!")
					Endif
				Else
				err=cp_sqlexec(i_nConn, "sp_dboption "+l_Db_Name+", 'single user'", CurAppo)
				If err>0
					Select(CurAppo)
					l_bSingle= Lower(CurrentSetting)="on"
					Use In Select(CurAppo)
					If l_bSingle And cp_yesno(MSG_DATABASE_IN_SINGLE_USER)
						*--- Elimino Single User
						err=cp_sqlexec(i_nConn, "sp_dboption "+l_Db_Name+", 'single user', false", CurAppo)
						If err<0
							*--- Non riesco ad eliminare Single User
							cp_errormsg(cp_MsgFormat(MSG_CONTACT_ADMINISTRATOR__, Message()),"!","",.F.)
						Endif
					Endif
				Else
					*--- Non posso determinare se in Single User, non ho i diritti?
					*cp_errormsg("Non posso determinare se il DB � Single User","!")
				Endif
                Endif
			Else
				*--- Non posso determinare il nome del DB
				*cp_errormsg("Non posso recuperare il nome del DB","!")
			Endif
			Use In Select(CurAppo)
			*--- Ripristino error precedente
			On Error &l_errsav
		Endif &&i_cDB="SQLServer"
		*--- Zucchetti Aulla Inizio - Controllo database
		*--- Verifica che il database sia di tipo congruente all'installazione...
		*--- Determino nome DB
		l_errsav=On('ERROR')
		On Error err=-1
		CurAppo=Sys(2015)
		err=cp_sqlexec(i_nConn, "select MOENGINE as MOENGINE from ACTKEY Where MOCODICE="+cp_tostrodbc('0000000001'), CurAppo)
		* --- Ho un errore, non trovo il campo quindi non posso sapere di quale prodotto � il database...
		If err>0 And MOENGINE<>'X'
			Select (CurAppo)
			Do Case
				Case MOENGINE='R' And Not IsAhr()
					i_nConn=-1
				Case MOENGINE='E' And Not IsAhe()
					i_nConn=-1
				Case MOENGINE='A' And Not IsAlt()
					i_nConn=-1
				Case MOENGINE='P' And Not IsApa()
					i_nConn=-1
			Endcase
			If i_nConn=-1
				=SetMsgTitle()
				cp_msg(cp_MsgFormat(MSG_DETECTED_DATABASE__APP_YOU_SAID_IT_WAS__ ,i_MSGTITLE),.F.)
			Endif
		Endif
		Use In Select(CurAppo)
		On Error &l_errsav
		*--- Zucchetti Aulla fine - Controllo database
		cp_OpenSecondConn(pConn)
	Endif
	Return(i_nConn<>-1)

Proc cp_CloseDatabase()
	For i=1 To i_nTables
		If i_TableProp[i,6]=1 And i_TableProp[i,2]<>'*'
			cp_DropTempTable(i_TableProp[i,3],i_TableProp[i,2])
		Endif
	Next
	cp_CloseServers()
	Return

Func cp_OpenServers()
	Local i_ok,i_tmp
	* --- apre servers
	i_ok=.T.
	For i_tmp=1 To i_nServers
		If i_ServerConn[i_tmp,4]=1
			* --- e' da connettere alla partenza dell'applicazione
			i_ok=i_ok And cp_OpenOneServer(i_tmp)
		Endif
	Next
	Return(i_ok)

Func cp_OpenOneServer(i_nSrv)
	Local i_nConn,i_ok,i_cDB
	i_ok=.T.
	If i_ServerConn[i_nSrv,2]=-1
		*wait window "apre server "+str(i_nSrv)+i_ServerConn[i_nSrv,3]
		If !Empty(i_ServerConn[i_nSrv,3])
			i_nConn=cp_OpenDatabaseConnection(i_ServerConn[i_nSrv,3])
			If i_nConn<>-1
				i_ServerConn[i_nSrv,2]=i_nConn
				i_ServerConn[i_nSrv,5]=0
				i_cDB=SQLXGetDatabaseType(i_nConn)
				If Empty(i_ServerConn[i_nSrv,6])
					i_ServerConn[i_nSrv,6]=i_cDB
				Endif
				If i_ServerConn[i_nSrv,7]
					=cp_CheckPostItTables(i_ServerConn[i_nSrv,1],i_nConn,i_ServerConn[i_nSrv,6])
				Endif

				*--- Riconnessione automatica - mi salvo propriet� connessione per ritentare connessione nel caso di scollegamento
				i_ServerConn[i_nSrv,8]=SQLGetprop(i_nConn,'ConnectString')

			Else
				i_ok=.F.
				cp_msg(cp_MsgFormat(MSG_ERROR_OPENING_SERVER_A__A_QM,i_ServerConn[i_nSrv,1]),.F.)
			Endif
		Else
			If Inlist(Upper(i_ServerConn[i_nSrv,6]),'VFP','DBF') Or Empty(i_ServerConn[i_nSrv,6])
				i_ServerConn[i_nSrv,2]=0
				If Empty(i_ServerConn[i_nSrv,6])
					i_ServerConn[i_nSrv,6]='VFP'
				Endif
			Else
				cp_msg( cp_Translate(MSG_LOCAL_SERVERS_MUST_HAVE_DBF_FORMAT_QM),.F.)
			Endif
		Endif
	Endif
	Return(i_ok)

Func cp_CheckPostItTables(nServer,nConn,cDatabaseType)
	If Not(cp_ExistTable('postit',nConn)) Or Not(cp_ExistTable('cpwarn',nConn))
		If cp_YesNo(cp_MsgFormat(MSG_SERVER__HAS_TO_MANAGE_POSTIN_BUT_SYSTEM_FILES_ARE_MISSING_F_CREATE_THEM_QP,Trim(nServer)),32,.f.,.f.,MSG_SYSTEM_TABLES)
			=cp_CreatePostItTables(nConn,cDatabaseType)
		Endif
	Endif
	Return(.T.)

Func cp_LoadServerArray()
	* --- Cerca i vari server nella table CPTSRVR e carica l'array i_ServerCon
	*     aprendo le connesioni.
	Local i_tmp
	If i_ServerConn[1,2]<>0
		=cp_SQL(i_ServerConn[1,2],"select * from cptsrvr","_Tmp_")
	Else
		Select * From cptsrvr Where Not(Deleted()) Into Cursor _Tmp_
	Endif
	If i_ServerConn[1,7] And Vartype(i_bDisablePostIn)="L"
		i_ServerConn[1,7]=!i_bDisablePostIn
	Endif
	If Reccount()>0
		i_nServers=Reccount()+1
		Dimension i_ServerConn[i_nServers,SERVERCONNDIM]
		Go Top
		i_tmp=1
		Scan
			i_tmp=i_tmp+1
			*wait window "caricato server "+_tmp_.servername
			i_ServerConn[i_tmp,1]=_Tmp_.ServerName
			i_ServerConn[i_tmp,2]=-1
			i_ServerConn[i_tmp,3]=Trim(_Tmp_.ODBCDataSource)
			i_ServerConn[i_tmp,4]=_Tmp_.WhenConn
			i_ServerConn[i_tmp,5]=0
			i_ServerConn[i_tmp,6]=Trim(_Tmp_.DatabaseType)
			i_ServerConn[i_tmp,7]=_Tmp_.PostIt<>'N'
			If i_ServerConn[i_tmp,7] And Vartype(i_bDisablePostIn)="L"
				i_ServerConn[i_tmp,7]=!i_bDisablePostIn
			Endif
			*--- Riconnessione automatica - stringa di connessione da utilizzare nel caso di caduta del server
			i_ServerConn[i_tmp,8]=""
		Endscan
	Endif
	Use
	Return(.T.)

Func cp_LoadTableArray()
	Local i_ok,i
	i_ok=.T.
	If i_ServerConn[1,2]<>0
		=cp_SQL(i_ServerConn[1,2],"select * from cpttbls","_Tmp_")
	Else
		Select * From cpttbls Where Not(Deleted()) Into Cursor _Tmp_
	Endif
	Dimension i_TableProp[max(1,reccount()),TABLEPROPDIM]
	i=0
	Scan
		i=i+1
		i_TableProp[i,1]=Lower(Trim(_Tmp_.FILENAME))
		i_TableProp[i,2]=Trim(_Tmp_.PhName)
		i_TableProp[i,3]=-1 && non connesso
		i_TableProp[i,4]=0
		i_TableProp[i,5]=cp_GetServerIdx(_Tmp_.ServerName)
		i_TableProp[i,6]=0
		i_TableProp[i,7]=0
		* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
		i_TableProp[i,8]=0
		* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
		*--
		N=i_TableProp[i,5]
		*--
	Endscan
	i_nTables = i
	If i_nTables=0
		cp_msg(cp_Translate(MSG_WARNING_NO_REGISTERED_TABLES_FOUND_YOU_SHOULD_RUN_SYSTEM_ADMIN), .F.)
	Endif
	Use
	* --- tabella UTENTI
	If cp_ascan(@i_TableProp,'cpusers',i_nTables)=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cpusers'
		i_TableProp[i_nTables,2]='cpusers'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
		i_TableProp[i_nTables,8]=-1
		* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
	Endif
	* --- tabella GRUPPI
	If cp_ascan(@i_TableProp,'cpgroups',i_nTables)=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cpgroups'
		i_TableProp[i_nTables,2]='cpgroups'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
		i_TableProp[i_nTables,8]=-1
		* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
	Endif
	* --- tabella UTENTI/GRUPPI
	If cp_ascan(@i_TableProp,'cpusrgrp',i_nTables)=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cpusrgrp'
		i_TableProp[i_nTables,2]='cpusrgrp'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
		i_TableProp[i_nTables,8]=-1
		* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
	Endif
	* --- tabella AZIENDE
	If cp_ascan(@i_TableProp,'cpazi',i_nTables)=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cpazi'
		i_TableProp[i_nTables,2]='cpazi'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
		i_TableProp[i_nTables,8]=-1
		* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
	Endif
	* --- tabella REPORT
	If cp_ascan(@i_TableProp,'cpusrrep',i_nTables)=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cpusrrep'
		i_TableProp[i_nTables,2]='cpusrrep'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
		i_TableProp[i_nTables,8]=-1
		* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
	Endif
	* --- tabella SINGLE SIGN ON MAPPING
	If cp_ascan(@i_TableProp,'cpssomap',i_nTables)=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cpssomap'
		i_TableProp[i_nTables,2]='cpssomap'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
		i_TableProp[i_nTables,8]=-1
		* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
	Endif
	* --- tabella LINGUE PER TRADUZIONE DATI
	If cp_ascan(@i_TableProp,'cplangs',i_nTables)=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cplangs'
		i_TableProp[i_nTables,2]='cplangs'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
	Endif
	* --- tabella GRUPPI di GRUPPI
	If cp_ascan(@i_TableProp,'cpgrpgrp',i_nTables)=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='cpgrpgrp'
		i_TableProp[i_nTables,2]='cpgrpgrp'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
		i_TableProp[i_nTables,8]=-1
		* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
	Endif
	* --- Zucchetti Aulla inizio tabella Monitor Framework
	i_nTables=i_nTables+1
	Dimension i_TableProp[i_nTables,TABLEPROPDIM]
	i_TableProp[i_nTables,1]='cp_monitor'
	i_TableProp[i_nTables,2]='custom\userdir\cp_monitor'
	i_TableProp[i_nTables,3]=0
	i_TableProp[i_nTables,4]=0
	i_TableProp[i_nTables,5]=1
	i_TableProp[i_nTables,6]=0
	i_TableProp[i_nTables,7]=-1
	* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
	i_TableProp[i_nTables,8]=-1
	* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
	* --- Zucchetti Aulla fine tabella Monitor Framework
	Return(i_ok)

Proc cp_LoadTableSecArray()
	Local i_NT, i_result, i_onlyprocfilter, i_numproc
	Release i_TablePropSec
	Public i_TablePropSec
	If Type("i_TablePropSecProc")<>'U'
		Release i_TablePropSecProc
	Endif
	If Vartype(i_nTables)='N' And i_nTables<>0
		*** i_TablePropSec array per la definizione di politiche di sicurezza sul record
		*** 1 - query per select * from
		*** 2 - query per exists(select * from )
		*** 3 - flag query caricata
		*** 4 - flag esistono regole per procedura
		Dimension i_TablePropSec[i_nTables,TABLEPROPDIM]
		If Not cp_IsAdministrator(.T.)
			* --- Zucchetti Aulla: se costruisco delle regole non posso disabilitare la sicurezza perch�
			* --- modificando il cnf darei la possibilit� all'utente generico di vedere tutti i dati
			If i_bSecurityRecord Or .T.
				vq_exec('CP_SECRECTAB.VQR',,'_Curs_secrectab')
				If Used("_Curs_secrectab")
					Select _Curs_secrectab
					Scan
						* --- Zucchetti Aulla: se esite una regola abilito sempre la sicurezza
						i_bSecurityRecord=.T.
						i_NT=cp_GetTableDefIdx(Alltrim(_Curs_secrectab.tablename))
						i_result = Alltrim(_Curs_secrectab.queryfilter)
						i_onlyprocfilter = Iif(Nvl(_Curs_secrectab.procfilter,'')='0', .F., .T.)
						i_numproc = Nvl(_Curs_secrectab.numproc,0)
						If i_NT<>0 And i_numproc>=1
							i_TablePropSec[i_NT,1]=Iif(i_onlyprocfilter, .F., i_result)
							i_TablePropSec[i_NT,2]=Iif(i_onlyprocfilter, .F., i_result)
							i_TablePropSec[i_NT,3]=.F.
							i_TablePropSec[i_NT,4]=Iif(i_numproc=1 And Not i_onlyprocfilter,.F.,.T.)
						Endif
					Endscan
					Select _Curs_secrectab
					Use
				Endif
			Endif
		Endif
	Endif
	* --- da questo momento sono utilizzabili i filtri utente
	Return

Func cp_GetServerIdx(cSrvName)
	Local i,j
	* --- Cerca il server nella tabella delle connesioni "i_ServerCon"
	If Len(Trim(cSrvName))=0 Or Isnull(cSrvName)
		Return(1)
	Else
		i=cp_ascan(@i_ServerConn,cSrvName,i_nServers)
	Endif
	Return(i)

Proc cp_CloseServers()
	Local i_tmp,i_olderr
	For i_tmp=1 To i_nServers
		If i_ServerConn[i_tmp,2]=0
			Close Data
		Else
			If i_ServerConn[i_tmp,2]>0
				i_olderr=On('ERROR')
				On Error =.T.
				=SQLDisconn(i_ServerConn[i_tmp,2])
				On Error &i_olderr
			Endif
		Endif
		i_ServerConn[i_tmp,5]=0
	Next
	cp_CloseSecondConn()
Endproc

Proc cp_CloseOneServer(i_NS)
	If i_ServerConn[i_NS,5]=0 And i_ServerConn[i_NS,4]=0 And (Type('nTrsConnCnt')='U' Or nTrsConnCnt=0)
		If i_ServerConn[i_NS,2]<>-1
			*wait window "Chiude il server "+i_ServerConn[i_NS,1]
			SQLDisconn(i_ServerConn[i_NS,2])
			i_ServerConn[i_NS,2]=-1
		Endif
	Endif
	Return

Function cp_OpenSchema(mode)
	Private res
	res=.T.
	If i_ServerConn[1,2]=0
		* --- apre il data dictionary di VFP nel modo specificato
		On Error res=.F.
		Close Data
		res=.T.
		Open Database (i_CpDic) &mode
		On Error
	Else
		If mode='exclusive'
			If i_ServerConn[1,2]<1000000
				cp_sqlexec(i_ServerConn[1,2],'drop table cpchkadmrght')
				If cp_sqlexec(i_ServerConn[1,2],'create table cpchkadmrght (codice Char(1))')=-1
					res=.F.
				Endif
				cp_sqlexec(i_ServerConn[1,2],'drop table cpchkadmrght')
				If i_ServerConn[1,6]="Interbase"
					cp_CloseSecondConn()
				Endif
			Else
				i_res=.F.
			Endif
		Else
			If i_ServerConn[1,6]="Interbase"
				cp_OpenSecondConn(i_ServerConn[1,2])
			Endif
		Endif
	Endif
	Return(res)

Function cp_ascan(i_array, i_src, i_len)
	Local i_i
	i_src=Trim(i_src)
	#If Version(5)>=900
		i_i=Ascan(i_array, i_src, -1, -1, 1, 14)
		Return(i_i)
	#Else
		For i_i=1 To i_len
			If Trim(i_array[i_i,1])==i_src
				Return(i_i)
			Endif
		Next
	#Endif
	Return(0)

Function cp_TablePropScan(i_src)
	*--- torna la posizione in cui si trova la tabella nel' array delle proriet� delle tabelle
	*    la ricerca � case insensitive
	Local i_i
	i_src=Trim(Lower(i_src))
	* --- ora cerca la posizione della tabella
	#If Version(5)>=900
		Return Ascan(i_TableProp, i_src, -1, -1, 1, 15)
	#Else
		For i_i=1 To i_nTables
			If Lower(i_TableProp[i_i,1])==Lower(i_src)
				Return(i_i)
			Endif
		Next
		Return(0)
	#Endif


Function cp_getTempTableName(i_nConn)
	Local i_cTmpTable
	i_cTmpTable='cptmp_'+Strtran(cp_NewCCChk(),"xxx","xyz")
	If cp_getDatabaseType(i_nConn)="SQLServer"
		i_cTmpTable='##'+i_cTmpTable
	Endif
	Return(i_cTmpTable)

Function cp_ExistTableDef(i_cTableName)
	Local i_NT
	i_NT=cp_TablePropScan(i_cTableName)
	Return(i_NT<>0)

Function cp_AddTableDef(i_cTableName,i_bKeepIfExists)
	Local i_NT,i_cDatabaseType,i_cTmpTable
	i_NT=cp_TablePropScan(i_cTableName)
	If i_NT<>0
		*wait window "Non e' possibile aggiungere 2 volte la stessa tabella temporanea"
		If !i_bKeepIfExists
			cp_DropTempTable(i_ServerConn[1,2],i_TableProp[i_NT,2])
		Endif
		i_TableProp[i_NT,4]=0
		Return(i_NT)
	Endif
	i_NT=cp_TablePropScan('*unused')
	If i_NT=0
		* --- aggiunge nuova definizione
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_NT=i_nTables
		i_TableProp[i_NT,3]=i_ServerConn[1,2]
		i_TableProp[i_NT,4]=0
		i_TableProp[i_NT,5]=1
		i_TableProp[i_NT,6]=1
		i_TableProp[i_NT,7]=1
		* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
		i_TableProp[i_NT,8]=-1
		* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
	Endif
	i_TableProp[i_NT,1]=Lower(i_cTableName)
	i_TableProp[i_NT,2]=cp_getTempTableName(i_ServerConn[1,2])
	Return(i_NT)

Function cp_RemoveTableDef(i_cTableName)
	Local i_NT
	i_NT=cp_TablePropScan(i_cTableName)
	If At('cptmp_',Lower(i_TableProp[i_NT,2]))=0
		* -- drop solo di tabelle temporanee
		Return
	Endif
	If i_NT<>0
		i_TableProp[i_NT,1]='*unused'
		i_TableProp[i_NT,2]='*'
		i_TableProp[i_NT,4]=0
	Endif
	Return

	***** Zucchetti Aulla Inizio - Aggiunto parametro per semplificare la query di costruzione della tabella temporanea nel caso oracle
	*function cp_CreateTempTable(i_nConn,i_cTempTable,i_cFields,i_cSel,i_sqlx,i_indexes)
Function cp_CreateTempTable(i_nConn,i_cTempTable,i_cFields,i_cSel,i_sqlx,i_indexes,i_Csel_AHE)
	* creazione di una tabella temporanea
	Local i_cDatabaseType,i_bRes,i_cCreateCmd,i_olderr,i_newconn,i_addcpccchk,i_r,i_bCreateIndexes,i_cIFld,i_i,i_innodb,i_bSetCpccchk
	i_bRes=.T.
	i_addcpccchk=.T.
	i_newconn=0
	i_bCreateIndexes=.T.
	i_bSetCpccchk=.T.
	If i_nConn<>0
		i_cDatabaseType=cp_getDatabaseType(i_nConn)
		Do Case
			Case i_cDatabaseType='SQLServer'
				* SELECT <elenco campi> INTO #<tabella temporanea> FROM ....
				i_cSel="select "+i_cFields+" into "+i_cTempTable+i_cSel
			Case i_cDatabaseType='Interbase'
				i_cSel="insert into "+i_cTempTable+'('+"select "+i_cFields+i_cSel+')'
			Case Inlist(i_cDatabaseType,'Oracle','MySQL','PostgreSQL')
				Local i_cSel2
				* Oracle chiude le transazioni pendenti sulla connessione per tutte le frasi che amministrano la base dati.
				i_innodb=Iif(i_cDatabaseType='MySQL',' type=InnoDB ','')
				If (Vartype(nTrsConnCnt)='U' Or nTrsConnCnt=0) And Not(Vartype(g_ORACLECREATETEMPTABLE)="L" And g_ORACLECREATETEMPTABLE)
					* possiamo utilizzare la connesione corrente
					* CREATE TABLE <tabella temporanea> AS SELECT <elenco campi> FROM ...
					i_cSel="create table "+i_cTempTable+i_innodb+" as select "+i_cFields+i_cSel
				Else
					* Apriamo un nuova connessione per la creazione della nuova tabella che poi verra' riempita con una frase INSERT
					* sulla connessione corrente
					i_newconn=Sqlstringconnect(SQLGetprop(i_nConn,'ConnectString'))
					cp_PostOpenConn(i_newconn)
					* fa la query, senza la order by per metterci meno
					i_cSel2=i_cSel
					i_p=At('order by',Lower(i_cSel2))
					If i_p<>0
						i_cSel2=Left(i_cSel2,i_p-1)
					Endif
					* versione che inserisce i dati e poi li cancella
					*i_r=cp_sqlexec(i_newconn,"create table "+i_cTempTable+i_innodb+" as select "+i_cFields+i_cSel2)
					* cancella i dati
					*i_r=cp_sqlexec(i_newconn,'delete from '+i_cTempTable+' where 1>0')
					* versione che non inserisce i dati
					If i_cDatabaseType='Oracle'
						* --- Zucchetti aulla - Semplifica query su Oracle
						If Type('i_Csel_AHE')='C' And Not Empty(i_Csel_AHE) And Not(Type("g_OracleCreateSubQuery")='L' And g_OracleCreateSubQuery)
							i_r=cp_sqlexec(i_newconn,"create table "+i_cTempTable+" as select "+i_cFields+i_Csel_AHE)
						Else
							* --- Zucchetti aulla - Semplifica query su Oracle
							i_r=cp_sqlexec(i_newconn,"create table "+i_cTempTable+" as select * from (select "+i_cFields+i_cSel2+") where 1=0")
							* --- Zucchetti aulla - Semplifica query su Oracle
						Endif
						* --- Zucchetti aulla - Semplifica query su Oracle
					Else
						i_r=cp_sqlexec(i_newconn,"create table "+i_cTempTable+i_innodb+" as (select "+i_cFields+i_cSel2+") limit 0")
					Endif
					* prende la lista dei campi
					i_r=cp_sqlexec(i_newconn,"select * from "+i_cTempTable+" where 1=0","__oracletmp__")
					i_cIFld=''
					If i_r<>-1 And Used('__oracletmp__')
						Select __oracletmp__
						i_cIFld=' ('
						For i_i=1 To Fcount()
							i_cIFld=i_cIFld+Iif(i_i=1,'',',')+Fields(i_i)
						Next
						i_cIFld=i_cIFld+')'
						Use
					Endif
					* aggiunge il cpccchk
					i_bSetCpccchk=At('CPCCCHK',Upper(i_cIFld))=0
					If i_bSetCpccchk
						i_r=cp_addcpccchk(i_newconn,i_cDatabaseType,i_cTempTable)
						* i_r=cp_sqlexec(i_newconn,"alter table "+i_cTempTable+" add cpccchk char(10) default '12avxgd'")
					Endif
					* prepara la frase che inserira' i valori
					i_cSel='insert into '+i_cTempTable+i_cIFld+' select '+i_cFields+i_cSel
					i_bCreateIndexes=.F.
					cp_CreateTempIndexes(i_cDatabaseType,i_newconn,i_cTempTable,@i_indexes,i_sqlx)
					i_addcpccchk=.F.
				Endif
				*
			Case i_cDatabaseType='DB2/390'
				*
				* crea la tabella
				Local i_cSel2,i_p
				i_cSel2=i_cSel
				i_p=At('order by',Lower(i_cSel2))
				If i_p<>0
					i_cSel2=Left(i_cSel2,i_p-1)
				Endif
				* --- crea una vista
				i_cCreateCmd="create  view "+i_cTempTable+"_v as (select "+i_cFields+i_cSel2+")"
				If Type("i_sqlx")='L'
					i_r=cp_sqlexec(i_nConn,i_cCreateCmd)
				Else
					i_r=i_sqlx.SQLExec(i_nConn,i_cCreateCmd,.F.,i_cDatabaseType)
				Endif
				* --- crea la tabella dalla vista
				i_cCreateCmd="create table "+i_cTempTable+" like "+i_cTempTable+"_v"
				If Type("i_sqlx")='L'
					i_r=cp_sqlexec(i_nConn,i_cCreateCmd)
				Else
					i_r=i_sqlx.SQLExec(i_nConn,i_cCreateCmd,.F.,i_cDatabaseType)
				Endif
				* --- drop della vista
				i_cCreateCmd="drop view "+i_cTempTable+"_v"
				If Type("i_sqlx")='L'
					cp_sqlexec(i_nConn,i_cCreateCmd)
				Else
					i_sqlx.SQLExec(i_nConn,i_cCreateCmd,.F.,i_cDatabaseType)
				Endif

				* ---
				i_bCreateIndexes=.F.
				cp_CreateTempIndexes('DB2',i_nConn,i_cTempTable,@i_indexes,i_sqlx)
				If i_r=1
					* la riempie
					i_cSel="insert into "+i_cTempTable+" select "+i_cFields+i_cSel2
				Else
					i_cSel=""
				Endif
			Case i_cDatabaseType='DB2'
				* crea la tabella
				Local i_cSel2,i_p
				i_cSel2=i_cSel
				i_p=At('order by',Lower(i_cSel2))
				If i_p<>0
					i_cSel2=Left(i_cSel2,i_p-1)
				Endif
				i_cCreateCmd="create  table "+i_cTempTable+" as (select "+i_cFields+i_cSel2+") definition only"
				If Type("i_sqlx")='L'
					i_r=cp_sqlexec(i_nConn,i_cCreateCmd)
				Else
					i_r=i_sqlx.SQLExec(i_nConn,i_cCreateCmd,.F.,i_cDatabaseType)
				Endif
				If i_r=-1 && Aggiunto controllo errore su creazione tabella su db2: con errore non creo indici
					i_bRes=.F.
				Else
					i_bCreateIndexes=.F.
					cp_CreateTempIndexes('DB2',i_nConn,i_cTempTable,@i_indexes,i_sqlx)
					If i_r=1
						* la riempie
						i_cSel="insert into "+i_cTempTable+" select "+i_cFields+i_cSel2
					Else
						i_cSel=""
					Endif
				Endif
			Case i_cDatabaseType='Access'
				* SELECT <elenco campi> INTO <tabella temporanea> FROM ....
				* non c'� il concetto di tabella temporanea (deve essere cancellata alla chiusura
				* della connessione
				i_cSel="select "+i_cFields+" into "+i_cTempTable+i_cSel
				*case i_cDatabaseType='Interbase'
				*  if type('nTrsConnCnt')='U' or nTrsConnCnt=0
				*    i_cSel="create table "+i_cTempTable+" as select "+i_cFields+i_cSel
				*  ELSE
				*    * --- come ORACLE!
				*    i_newconn=sqlstringconnect(sqlgetprop(i_nConn,'ConnectString'))
				*    i_r=cp_sqlexec(i_newconn,"create table "+i_cTempTable+" as select "+i_cFields+i_cSel)
				*    i_r=cp_sqlexec(i_newconn,'delete from '+i_cTempTable+' where 1>0')
				*    i_r=cp_sqlexec(i_newconn,"alter table "+i_cTempTable+" add cpccchk char(10) default '12avxgd'")
				*    i_addcpccchk=.f.
				*    i_cSel='insert into '+i_cTempTable+' select '+i_cFields+i_cSel
				*    i_bCreateIndexes=.f.
				*    cp_CreateTempIndexes('Interbase',i_newconn,i_cTempTable,@i_indexes,i_sqlx)
				*  ENDIF
			Case i_cDatabaseType='DBMaker'
				i_cSel="select "+i_cFields+' into '+i_cTempTable+i_cSel
			Otherwise
				i_cSel="create table "+i_cTempTable+" as select "+i_cFields+i_cSel
		Endcase
		If !Empty(i_cSel) And i_bRes  && Con errori non proseguo nella gestione della tabella temporanea
			If Type("i_sqlx")='L'
				i_r=cp_sqlexec(i_nConn,i_cSel)
				If i_r=-1
					i_bRes=.F.
				Else
					If i_addcpccchk
						* aggiunge il cpccchk, se ci riesce (non c'era), lo inizializza
						i_bSetCpccchk=(cp_addcpccchk(i_nConn,i_cDatabaseType,i_cTempTable)=1)
						*i_bSetCpccchk=(cp_sqlexec(i_nConn,"alter table "+i_cTempTable+" add cpccchk char(10) default '12avxgd'")=1)
					Endif
					If i_bSetCpccchk
						* il cpccchk e' stato aggiunto perche' non viene fornito dalla query iniziale, bisogna quindi forzare un valore
						cp_sqlexec(i_nConn,'update '+i_cTempTable+" set cpccchk='12avxgd'")
					Endif
				Endif
			Else
				If i_sqlx.SQLExec(i_nConn,i_cSel,.F.,i_cDatabaseType)=-1
					i_bRes=.F.
				Else
					If i_addcpccchk
						* aggiunge il cpccchk, se ci riesce (non c'era), lo inizializza
						i_bSetCpccchk=(cp_addcpccchk(i_nConn,i_cDatabaseType,i_cTempTable)=1)
						*i_bSetCpccchk=(i_sqlx.sqlexec(i_nConn,"alter table "+i_cTempTable+" add cpccchk char(10) default '12avxgd'")=1)
					Endif
					If i_bSetCpccchk
						cp_sqlexec(i_nConn,'update '+i_cTempTable+" set cpccchk='12avxgd'")
					Endif
				Endif
			Endif
		Endif
		If i_bCreateIndexes And i_bRes  && Con errori non proseguo nella gestione della tabella temporanea
			cp_CreateTempIndexes(i_cDatabaseType,i_nConn,i_cTempTable,@i_indexes,i_sqlx)
		Endif
	Else
		Select &i_cFields. &i_cSel. Into Table &i_cTempTable
		i_olderr=On('ERROR')
		On Error =.T.
		* --- questa frase puo' dare errore: vuol dire che il campo esiste gia nella tabella temporanea
		Alter Table &i_cTempTable Add cpccchk Char(10) Default '12avxgd'
		On Error &i_olderr
	Endif
	If i_newconn<>0
		SQLDisconn(i_newconn)
	Endif
	* --- Se va in errore la CreateTempTable lo segnalo
	* --- esempio se DB2 va in crash alla creazione di
	* --- temporanei molto grandi
	If Not i_bRes
		If Vartype(bTrsErr)='U'
			Public bTrsErr
		Endif
		bTrsErr=.T.
		Error(Message())
	Endif
	Return(i_bRes)

Function cp_addcpccchk(i_nConn,i_cDatabaseType,i_cTempTable)
	Local i_r
	Do Case
		Case i_cDatabaseType='PostgreSQL' Or i_cDatabaseType='Access'
			i_r=cp_sqlexec(i_nConn,"alter table "+i_cTempTable+" add cpccchk char(10)")
			If i_r=1
				cp_sqlexec(i_nConn,"alter table "+i_cTempTable+" alter column cpccchk set default '12avxgd'")
			Endif
		Case i_cDatabaseType='DBMaker'
			i_r=1
		Otherwise
			i_r=cp_sqlexec(i_nConn,"alter table "+i_cTempTable+" add cpccchk char(10) default '12avxgd'")
	Endcase
	Return(i_r)

Function cp_InsertIntoTempTable(i_nConn,i_cTempTable,i_cFields,i_cSel,i_sqlx)
	Local i_cmd,i_res
	i_res=0
	If i_nConn<>0
		i_cmd="insert into "+i_cTempTable+" select "+i_cFields+i_cSel
		If Type('i_sqlx')='L'
			i_res=cp_sqlexec(i_nConn,i_cmd)
		Else
			i_res=i_sqlx.SQLExec(i_nConn,i_cmd)
		Endif
	Else
		Error "not yet implemented"
	Endif
	Return(i_res>=0)

Function cp_DropTempTable(i_nConn,i_cTempTable)
	Local i_cDatabaseType,i_i
	i_cDatabaseType=cp_getDatabaseType(i_nConn)
	If At('cptmp_',Lower(i_cTempTable))=0
		* -- drop solo di tabelle temporanee
		Return
	Endif
	*wait window "delete temp table "+i_cTempTable
	If i_nConn<>0
		If Inlist(i_cDatabaseType,'Oracle','PostgreSQL') And Vartype(nTrsConnCnt)<>'U' And nTrsConnCnt<>0
			* Oracle chiude le transazioni pendenti sulla connessione per tutte le frasi che amministrano la base dati.
			* Canelliamo i dati dalla tabella temporane ed accodiamo la DROP per eseguirla alla fine della transazione
			cp_DropOnCloseTransaction(i_nConn,i_cTempTable)
			* --- rimuove la definizione della tabella, in modo che non sia piu' raggiungibile
			* --- verra' fisicamente cancellata solo a fine transazione
			For i_i=1 To i_nTables
				If i_TableProp[i_i,2]=i_cTempTable
					i_TableProp[i_i,2]=cp_getTempTableName(i_nConn)
				Endif
			Next
		Else
			*cp_SQLEXEC(i_nConn,'drop table '+i_cTempTable)
			cp_sqlexec(i_nConn,'drop table '+i_cTempTable+Iif(i_cDatabaseType='Oracle' And (Vartype(g_ORACLEVERS)<>'C' Or g_ORACLEVERS > '9'), ' purge', ''))
		Endif
	Else
		If Used(i_cTempTable)
			Select (i_cTempTable)
			Use
		Endif
		Delete File &i_cTempTable..Dbf
	Endif
	Return

Proc cp_CreateTempIndexes(i_cDatabaseType,i_nConn,i_cTableName,i_indexes,i_sqlx)
	Local i_i,i_cmd
	***** Controllo creazione indici da variabile globale g_NoCreateIndexes
	If Type('i_indexes')<>'L' And (Vartype(g_NoCreateIndexes)<>'L' Or g_NoCreateIndexes=.F.)
		For i_i=1 To Alen(i_indexes)
			i_cmd='create index '+Alltrim(i_cTableName)+'_tmp'+Alltrim(Str(i_i))+' on '+i_cTableName+'('+i_indexes[i_i]+')'
			If Type('i_sqlx')='L'
				cp_sqlexec(i_nConn,i_cmd)
			Else
				i_sqlx.SQLExec(i_nConn,i_cmd)
			Endif
		Next
	Endif
	Return

Function cp_GetTableDefIdx(i_cTableName)
	Return(cp_TablePropScan(i_cTableName))

Function cp_OpenTable(i_cTable,i_bDontReportError)
	Local i_NT,i_ok,i_NS,i_res,i_err, i_cPhTable
	i_res=0
	i_NT=cp_TablePropScan(i_cTable)
	If i_NT<>0
		i_NS=i_TableProp[i_NT,5]
		i_cPhTable=cp_SetAzi(i_TableProp[i_NT,2])
		If cp_OpenOneServer(i_NS)
			i_TableProp[i_NT,3]=i_ServerConn[i_NS,2]
			i_TableProp[i_NT,4]=i_TableProp[i_NT,4]+1
			i_ServerConn[i_NS,5]=i_ServerConn[i_NS,5]+1
			If i_TableProp[i_NT,3]=0
				* --- e' un archivio foxpro, lo apre per ottimizzare l' uso delle chiavi
				If !Used(i_cPhTable)
					* --- mai usato, se esiste lo apre
					If cp_ExistTable(i_cPhTable,0)
						Select 0
						i_err=On('ERROR')
						On Error =.F.
						Use (i_cPhTable) &&alias (i_TableProp[i_NT,1])
						On Error &i_err
						* --- tutto Ok, controlla versione demo
						If db_CheckDemo(i_cTable,i_cPhTable,i_TableProp[i_NT,3])
							i_res=i_NT
						Endif
						db_CheckCPCCCHKExistence(i_NT)
					Else
						i_TableProp[i_NT,4]=i_TableProp[i_NT,4]-1
					Endif
				Else
					* --- gia' utilizzato
					If db_CheckDemo(i_cTable,i_cPhTable,i_TableProp[i_NT,3])
						i_res=i_NT
					Endif
				Endif
			Else
				If cp_ExistTable(i_cPhTable,i_TableProp[i_NT,3])
					* --- tutto OK, setta la variabile di risposta
					If db_CheckDemo(i_cTable,i_cPhTable,i_TableProp[i_NT,3])
						i_res=i_NT
					Endif
					db_CheckCPCCCHKExistence(i_NT)
				Else
					* --- chiude il server
					i_TableProp[i_NT,4]=i_TableProp[i_NT,4]-1
					i_ServerConn[i_NS,5]=i_ServerConn[i_NS,5]-1
					cp_CloseOneServer(i_NS)
				Endif
			Endif
		Endif
	Endif
	If i_res=0 And !i_bDontReportError
		cp_msg(cp_MsgFormat(MSG_CANNOT_OPEN_TABLE_A___A_QM,Trim(i_cTable)),.F.)
	Endif
	Return(i_res)

Function db_CheckDemo(i_cTable,i_cPhTable,i_nConn)
	Local i_maxrec,i_p,i_s,i_r
	If Vartype(i_demolimits)='C' And Len(Trim(i_demolimits))>0
		i_p=At(',',i_demolimits)
		If i_p<>0
			i_maxrec=Val(Left(i_demolimits,i_p-1))
		Else
			i_maxrec=Val(i_demolimits)
		Endif
		i_p=At(Lower(i_cTable)+'=',Lower(i_demolimits))
		If i_p<>0
			i_s=Substr(i_demolimits,i_p+Len(i_cTable)+1)
			i_p=At(',',i_s)
			If i_p<>0
				i_maxrec=Val(Left(i_s,i_p-1))
			Else
				i_maxrec=Val(i_s)
			Endif
		Endif
		* --- query multicompany
		i_cPhTable=Strtran(i_cPhTable,'@@@###!!!^^^@@@','xxx')
		* --- query multicompany
		If i_maxrec<>0
			If i_nConn<>0
				cp_SQL(i_nConn,"select count(*) as recnum from "+i_cPhTable,"__dmo__")
			Else
				Select Count(*) As recnum From (i_cPhTable) Into Cursor __dmo__
			Endif
			If Used('__dmo__')
				Select __dmo__
				If Type('__dmo__.recnum')='C'
					i_r=Val(__dmo__.recnum)
				Else
					i_r=__dmo__.recnum
				Endif
				Use
			Else
				i_r=0
			Endif
			Do Case
				Case i_r>i_maxrec And i_maxrec<>0
					cp_msg(cp_MsgFormat(MSG_DEMO_LIMIT_IN_RECORD_NO_FOR_TABLE__,i_cTable),.F.)
					Return(.F.)
				Case i_r*3/2>i_maxrec And i_maxrec<>0
					cp_msg(cp_MsgFormat(MSG_TABLE__LOCKED_UNDER_DEMO,i_cTable),.F.)
			Endcase
		Endif
	Endif
	Return(.T.)

Procedure db_CheckCPCCCHKExistence(i_nPos,i_cTable)
	Local i_nConn,i_olderr,i_tb,i_oldarea
	Private i_err
	
	If Vartype(i_stopCheckCPCCCHKExistence)='L' And i_stopCheckCPCCCHKExistence
		* --- La variabile i_StopCheck.. impostata a .t.
		* --- � necessaria per evitare che le Cp_Opentable invocate all'avvio della procedura
		* --- carichino il dizionario prima che i_cmodules via valorizzato all'interno della blackbox
		* --- aggiunto check sulla presenza del dizionario dati, se presente il check va fatto sempre
		* --- sulle tabelle esterne
		If Vartype(i_dcx)='U'
			* --- Zucchetti aulla inizio - evita caricamento dizionario all'avvio
			If i_TableProp[i_nPos,1]='cpazi' &&OR i_TableProp[i_nPos,1]='postit'  &&Ora la tabella postit ha il cpccchk
				* La tabella cpazi � invocata ma non ha il cpccchk
				i_TableProp[i_nPos,7]=-1
			Else
				* --- Zucchetti aulla Fine
				i_TableProp[i_nPos,7]=1
				* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
				i_TableProp[i_nPos,8]=0
				* --- Zucchetti Aulla Fine - Verifico presenza campo UTDC
				* --- Zucchetti aulla inizio -  - evita caricamento dizionario all'avvio
			Endif
			* --- Zucchetti aulla fine
			Return
		Endif
	Endif
	* ---
	If !Empty(i_cTable) And Empty(i_nPos)
		i_nPos=cp_ascan(@i_TableProp,Lower(i_cTable),i_nTables)
	Endif
	If !Empty(i_nPos) And i_TableProp[i_nPos,7]=0
		If !cp_ReadXdc()
			Return(.F.)
		Endif
		If i_dcx.GetTableExternal(i_TableProp[i_nPos,1])
			i_oldarea=Select()
			i_nConn=i_TableProp[i_nPos,3]
			If i_nConn<>0
				i_r=cp_sqlexec(i_nConn,"select cpccchk from "+cp_SetAzi(i_TableProp[i_nPos,2])+" where 1=0","__tmpccchk__")
			Else
				i_olderr=On('error')
				i_err=.F.
				On Error i_err=.T.
				i_tb=cp_SetAzi(i_TableProp[i_nPos,2])
				Select cpccchk From &i_tb Where 1=0 Into Cursor __tmpccchk__
				On Error &i_olderr
			Endif
			If Used('__tmpccchk__')
				Select ('__tmpccchk__')
				Use
				i_r=1
			Else
				i_r=-1
			Endif
			Select(i_oldarea)
			i_TableProp[i_nPos,7]=Iif(i_r=-1,-1,1)
		Else
			i_TableProp[i_nPos,7]=1
		Endif
	Endif
	* --- Zucchetti Aulla Inizio - Verifico presenza campo UTDC
	If !Empty(i_nPos) AND i_TableProp[i_nPos,7]=1 AND i_TableProp[i_nPos,8]=0 AND VARTYPE(i_dcx)="O"
		UTDCpos=i_dcx.GetFieldIdx(i_TableProp[i_nPos,1],"UTDC")
		i_TableProp[i_nPos,8]=IIF(UTDCpos>0,1,-1)
	Endif
	* --- Zucchetti Aulla Fine - Verifico presenza campo UTCC
	Return

Procedure chk_demo
	Parameters i_file
	Return

Function cp_CloseTable(i_cTable)
	Local i_NT,i_ok, i_NS
	If Vartype(i_codazi)='U' && il programma si e' piantato!
		Return(.F.)
	Endif
	i_NT=cp_TablePropScan(i_cTable)
	If i_NT=0 && il programma si e' piantato o il file non esiste
		Return(.F.)
	Endif
	i_NS=i_TableProp[i_NT,5]
	i_TableProp[i_NT,4]=Iif(i_TableProp[i_NT,4]=0,0,i_TableProp[i_NT,4]-1) && una apertura in meno sulla tabella
	i_ServerConn[i_NS,5]=i_ServerConn[i_NS,5]-1          && una apertura in meno sul server
	cp_CloseOneServer(i_NS)
	If i_TableProp[i_NT,4]=0 And i_TableProp[i_NT,3]=0   && Se il contatore rimane a zero, e
		If Used(cp_SetAzi(i_TableProp[i_NT,2]))
			i_NS=cp_SetAzi(i_TableProp[i_NT,2])              && siamo su VFoxPro Chiudo l'area
			Use In (i_NS)
		Endif
	Endif
	Return(i_ok)
*--- Zucchetti Aulla Inizio - Forzo il controllo bypassando la variabile i_existenttbls
Func cp_ExistTable(i_cName,nConn,i_fld1,i_fld2,i_fld3, bForceCheck)
*Func cp_ExistTable(i_cName,nConn,i_fld1,i_fld2,i_fld3)
*--- Zucchetti Aulla Fine - Forzo il controllo bypassando la variabile i_existenttbls
	Local errsav,r,nElements,i_newconn, cCurName
	Private res
	If Vartype(i_existenttbls)='U'
		Public i_existenttbls
		i_existenttbls=''
	Else
*--- Zucchetti Aulla Inizio - Forzo il controllo bypassando la variabile i_existenttbls
		If  !m.bForceCheck AND '|'+i_cName+Alltrim(Str(nConn))+'|'$i_existenttbls+'|'
*		If  '|'+i_cName+Alltrim(Str(nConn))+'|'$i_existenttbls+'|'
*--- Zucchetti Aulla Fine - Forzo il controllo bypassando la variabile i_existenttbls		
			Return(.T.)
		Endif
	Endif
	Local i_cFlds
	i_cFlds='count(*) as cnt'
	If !Empty(i_fld1)
		i_cFlds=i_fld1
	Endif
	If !Empty(i_fld2)
		i_cFlds=i_cFlds+','+i_fld2
	Endif
	If !Empty(i_fld3)
		i_cFlds=i_cFlds+','+i_fld3
	Endif
	* --- query multicompany
	i_cName=Strtran(i_cName,'@@@###!!!^^^@@@','xxx')
	* --- query multicompany
	cCurName=Sys(2015)
	If nConn<>0
		If nConn<1000000
			r=cp_SQL(nConn,'select '+i_cFlds+' from '+i_cName+' where 1=0',m.cCurName,.F.,.T.)
			If r=-1
				res = .F.
			Else
				res = .T.
				Use In Select( m.cCurName )
			Endif
		Else
			res = .T.
		Endif
	Else
		res=File(i_cName+'.dbf')
		If res And i_cFlds<>'count(*)'
			Local i_olderr
			Private i_err
			i_err=.F.
			i_olderr=On('ERROR')
			On Error i_err=.T.
			Select &i_cFlds From (i_cName) Where 1=0 Into Cursor __tmp__
			On Error &i_olderr
			If Used('__tmp__')
				res=.T.
				Use
			Else
				res=.F.
			Endif
		Endif
	Endif
	If res
	*--- Zucchetti Aulla Inizio - Forzo il controllo bypassando la variabile i_existenttbls
		If  !m.bForceCheck Or !('|'+i_cName+Alltrim(Str(nConn))+'|'$i_existenttbls+'|')
			i_existenttbls=i_existenttbls+'|'+i_cName+Alltrim(Str(nConn))
		ENDIF
	*--- Zucchetti Aulla Fine - Forzo il controllo bypassando la variabile i_existenttbls		
	Endif
	Return(res)

	*--- Gestione riconnessione
Function Cp_Connected(nConn)

	* Verifica se l'handle di connessione � ancora valido
	* ad oggi il metodo pi� generale sarebbe la SQLTABLES che per�
	* provoca l'errore c0005 se lanciata 2 volte, riepieghiamo temporalmente
	* su di una sqlexec su cpazi (non � il massimo
	Local nres,sTmp, sOldarea, i_newconn, nElements
	nres=Cp_IsConnected(nConn)
	If nres=-1
		Cp_DoConnect(nConn)
	Endif

	Select(sOldarea)
	Return(nres>=0)
Endfunc

Function Cp_IsConnected(nConn)
	* Verifica se l'handle di connessione � ancora valido
	* Invia una frase SQL sbagliata al database e si aspetta il codice di errore dell'ODBC.
	Local  nHdbc,nHstmt,cSqlState,nNativeError,nErrorMsgSize
	Do SQLXodbcapi
	nHdbc=SQLGetprop(nConn,"ODBCHdbc")
	hs=0
	r=SQLAllocStmt(nHdbc,@hs)
	SQLExecDirect(hs,'FAVE',SQL_NTS)
	cSqlState=Replicate(Chr(0),255)
	nNativeError=0
	cErrorMsg=Replicate(Chr(0),255)
	nErrorMsgSize = 0
	SQLError(Val(Sys(3053)),nHdbc,hs,@cSqlState,@nNativeError,@cErrorMsg,Len(cErrorMsg),@nErrorMsgSize)
	*Il valore di nNativeError  restituito da SQLError deve essere un numero maggiore di 0 poiche la frase inviata sul DB non � una frase SQL
	*e quindi deve dare  errore inoltre se il codice � 10054 per SqlServer o 3114 per Oracle  allora si tratta di un messaggio esplicito di caduta connessione.
	*A volte(qunado � caduta la connessione)il server risponde con un codice di errore 0 (per SQLServer) o 1012 (per oracle), tali codici di erore
	*non corrispondono ai codici di errore di frase SQL sbagliata
	SQLFreeStmt(hs,SQL_DROP)
	If (cp_dbtype="SQLServer"  And  (nNativeError=10054 Or nNativeError=0 And ('08S01'$cSqlState))) Or ( (nNativeError=1012 Or nNativeError=3114 ) And 'S100'$cSqlState And Upper(cp_dbtype)='ORACLE') Or (nNativeError=-99999 And '08003'$cSqlState And Upper(cp_dbtype)='DB2')
		Return .F.
	Endif
	Return .T.
Endfunc

Function Cp_DoConnect(nConn)
	* Chiude la connessione ricevuta come parametro, ne apre una nuova e restituisce l handler della nuova connessione
	* Per aprire la connessione deve recuperare l handler di connessione dall array delle connessioni i_serverconn. La connessione nconn
	* ricevuta come aprametro � utilizzata come indice per recuperare la stringa di connessione(� un meccanismo del painter). Se la connessione
	* � caduta precedetnemete allora si risale alla connessione che l ha sostituita mediant l array i_ServerConnBis che contiene lo storico delle connessioni cadute

	Local nres,sOldarea, nnewConn, nElements , npagine , nciclo , ncontrol ,  ncolomn , nnewdim
	sOldarea=Select()

	If Type("i_AUTORICONNECT")='L' And i_AUTORICONNECT=.T.
		If Vartype(i_CONNECTLOGIN) ='C' And i_CONNECTLOGIN='S'
			SQLSetprop(0, "DispLogin", 2 )
		Else
			SQLSetprop(0, "DispLogin", 3 )
		Endif
	Endif

	nElements = Ascan(i_ServerConn, nConn, -1, -1, 2, 8)
	*se � arrivato un numero di connessione sbagliato (che non esiste piu si cerca la stringa di connessione nell array contenente lo storico delle connessioni
	If  Vartype(nElements)='L' Or nElements<=0
		nElements = Ascan(i_ServerConnBis, nConn, -1, -1, 1, 8)
		If  Vartype(nElements)='N' And nElements>0
			nElements = Ascan(i_ServerConn, i_ServerConnBis[nElements ,2], -1, -1, 8, 8)
		Endif
	Endif
	nnewConn=Sqlstringconnect(i_ServerConn[nElements,8])
	* se la connessione non � andata a buon esce subito dalla procedura
	If Type('nnewConn')<>'N' Or nnewConn=-1
		If Vartype(i_CONNECTMSG)='C' And i_CONNECTMSG='D'
			cp_dbg(Message()) && Se non va a buon fine a video mostro l'errore...
		Endif
		If Vartype(i_CONNECTMSG)='C' And i_CONNECTMSG='F'
			cp_connecFile (Message())&& Se non va a buon fine a video mostro l'errore...
		Endif
		Return nConn
	Endif




	**Se le connessioni che sono cadute vengono disconnesse alcune istruzioni (sqlexec e sqlsetprop ) potrebbero dare errore nell utilizzare handle
	**sqldisconn(nConn)

	cp_PostOpenConn(nnewConn)
	If nElements=1
		If Type("i_AUTORICONNECT")='L' And i_AUTORICONNECT=.T.
			SQLSetprop(0, "DispLogin", 3 )
		Endif
		cp_OpenSecondConn(nnewConn)
	Endif
	*sostituisce la vecchia connessione negli array di sistema e si inserisce la connessione che � caduta nell array dello storico i_ServerConnBis
	nnewdim=Alen(i_ServerConnBis )/2 + 1
	Dimension i_ServerConnBis (nnewdim , 2)
	i_ServerConnBis[nnewdim,1]=i_ServerConn[nElements ,2]
	i_ServerConnBis[nnewdim,2]=i_ServerConn[nElements ,8]
	i_ServerConn[nElements ,2]=nnewConn

	Select(sOldarea)
	Return nnewConn
Endfunc
*--- Gestione riconnessione

Func cp_FldLen(i_cName,nConn,i_fld)
	Local res
	If nConn<>0
		r=cp_SQL(nConn,'select '+i_fld+' from '+i_cName+' where 1=0',"__tmp__",.F.,.T.)
	Else
		Select &i_fld From (i_cName) Where 1=0 Into Cursor __tmp__
	Endif
	res=Len(__tmp__.&i_fld.)
	Return(res)

Func cp_CreateSystemTables(nConn,cDatabaseType)
	Local i_ok,r,db,i_olderr,s
	i_ok=.T.
	db=cDatabaseType
	If Not(cp_ExistTable('persist',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"persist"))
		If nConn<>0
			cp_SQL(nConn,"create table persist (code "+db_FieldType(db,'C',40,0)+",usercode "+db_FieldType(db,'N',6,0)+",persist "+db_FieldType(db,'M',0,0)+")"+cp_PostTable(cDatabaseType))
			cp_SQL(nConn,"create index persist1 on persist(code)")
			cp_SQL(nConn,"create index persist2 on persist(usercode)")
		Else
			Create Table persist (Code C(40),usercode N(4),persist M)
			Index On Code Tag tag1
			Index On usercode Tag tag2
			Use
		Endif
	Endif
	If Not(cp_ExistTable('cpusers',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cpusers"))
		If nConn<>0
			langfld=Iif(i_ServerConn[1,6]='DBMaker',"lang","language")
			cp_SQL(nConn,"create table cpusers (code "+db_FieldType(db,'N',6,0)+" not null,name "+db_FieldType(db,'C',20,0)+", passwd "+db_FieldType(db,'C',20,0)+","+langfld+" "+db_FieldType(db,'C',3,0);
				+",enabled "+db_FieldType(db,'C',1,0)+", companies "+db_FieldType(db,'C',60,0)+" "+", printserver "+db_FieldType(db,'C',60,0)+" ";
				+",datestart "+db_FieldType(db,'D',1,0)+" "+",datestop "+db_FieldType(db,'D',1,0)+;
				+",failedlogins "+db_FieldType(db,'N',3,0)+", lastfailedlogin "+db_FieldType(db,'T',0,0)+", lastsuccessfullogin "+db_FieldType(db,'T',0,0)+",forcepwdchange "+db_FieldType(db,'L',1,0)+" ";
				+", pwdcreate "+db_FieldType(db,'T',0,0)+", pwdcomplexity "+db_FieldType(db,'N',3,0)+", pwddaysduration "+db_FieldType(db,'N',4,0)+", pwdciclicity "+db_FieldType(db,'N',2,0)+", captchalevel "+db_FieldType(db,'N',4,0)+" ";
				+",cpccchk "+db_FieldType(db,'C',10,0)+" ";
				+",webtype "+db_FieldType(db,'C',1,0)+",cp_login "+db_FieldType(db,'C',20,0)+" "; && * --- Zucchetti Aulla, aggiunto campo webtype C(1) e cp_login C(20)
			+db_InlinePrimaryKey("cpusers","cpusers","code",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpusers on cpusers(code)')
			Endif
			db_CreatePrimaryKey(nConn,"cpusers","cpusers","code",cDatabaseType)
			*cp_SQL(nConn,"alter table cpusers "+db_PrimaryKeyConstraint("cpusers","cpusers","code",cDatabaseType))
		Else
			Create Table cpusers (Code N(4),Name Char(20),passwd Char(20),Language Char(3),Enabled Char(1), datestart d, datestop d,companies Char(60),printserver Char(60),;
				failedlogins N(3), lastfailedlogin T, lastsuccessfullogin T,forcepwdchange L, pwdcreate T, pwdcomplexity N(3), pwddaysduration N(4), pwdciclicity N(4), captchalevel N(4),;
				cpccchk C(10))
			Index On Code Tag tag1
			Use
		Endif
	Else
		If nConn<>0
			cp_sqlexec(nConn,'select * from cpusers','__tmp__')
			langfld=Iif(i_ServerConn[1,6]='DBMaker',"lang","language")
			Select __tmp__
			If Type("__tmp__."+langfld)<>'C'
				cp_SQL(nConn,"alter table cpusers add "+langfld+" "+db_FieldType(db,'C',3,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.enabled")<>'C'
				cp_SQL(nConn,"alter table cpusers add enabled "+db_FieldType(db,'C',1,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.datestart")<>'D'
				cp_SQL(nConn,"alter table cpusers add datestart "+db_FieldType(db,'D',8,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.datestop")<>'D'
				cp_SQL(nConn,"alter table cpusers add datestop "+db_FieldType(db,'D',8,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.companies")<>'C'
				cp_SQL(nConn,"alter table cpusers add companies "+db_FieldType(db,'C',60,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.printserver")<>'C'
				cp_SQL(nConn,"alter table cpusers add printserver "+db_FieldType(db,'C',60,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.failedlogins")<>'N'
				cp_SQL(nConn,"alter table cpusers add failedlogins "+db_FieldType(db,'N',3,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.lastfailedlogin")<>'T'
				cp_SQL(nConn,"alter table cpusers add lastfailedlogin "+db_FieldType(db,'T',0,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.lastsuccessfullogin")<>'T'
				cp_SQL(nConn,"alter table cpusers add lastsuccessfullogin "+db_FieldType(db,'T',0,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.forcepwdchange")<>'L' And Type("__tmp__.forcepwdchange")<>'N'
				cp_SQL(nConn,"alter table cpusers add forcepwdchange "+db_FieldType(db,'L',0,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.pwdcreate")<>'T'
				cp_SQL(nConn,"alter table cpusers add pwdcreate "+db_FieldType(db,'T',0,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.pwdcomplexity")<>'N'
				cp_SQL(nConn,"alter table cpusers add pwdcomplexity "+db_FieldType(db,'N',3,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.pwddaysduration")<>'N'
				cp_SQL(nConn,"alter table cpusers add pwddaysduration "+db_FieldType(db,'N',4,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.pwdciclicity")<>'N'
				cp_SQL(nConn,"alter table cpusers add pwdciclicity "+db_FieldType(db,'N',4,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.captchalevel")<>'N'
				cp_SQL(nConn,"alter table cpusers add captchalevel "+db_FieldType(db,'N',4,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.cpccchk")<>'C'
				cp_SQL(nConn,"alter table cpusers add cpccchk "+db_FieldType(db,'C',10,0),.F.,.F.,.T.)
			Endif
		Else
			Select * From cpusers Into Cursor __tmp__
			i_olderr=On('ERROR')
			On Error =.T.
			If Used('cpusers')
				Select cpusers
				Use
			Endif
			If Type("__tmp__.language")<>'C'
				Alter Table cpusers Add Language C(3)
			Endif
			If Type("__tmp__.enabled")<>'C'
				Alter Table cpusers Add Enabled C(1)
			Endif
			If Type("__tmp__.datestart")<>'D'
				Alter Table cpusers Add datestart d
			Endif
			If Type("__tmp__.datestop")<>'D'
				Alter Table cpusers Add datestop d
			Endif
			If Type("__tmp__.companies")<>'C'
				Alter Table cpusers Add companies C(60)
			Endif
			If Type("__tmp__.printserver")<>'C'
				Alter Table cpusers Add printserver C(60)
			Endif
			If Type("__tmp__.failedlogins")<>'N'
				Alter Table cpusers Add failedlogins N(3)
			Endif
			If Type("__tmp__.lastfailedlogin")<>'T'
				Alter Table cpusers Add lastfailedlogin T
			Endif
			If Type("__tmp__.lastsuccessfullogin")<>'T'
				Alter Table cpusers Add lastsuccessfullogin T
			Endif
			If Type("__tmp__.forcepwdchange")<>'L'
				Alter Table cpusers Add forcepwdchange L
			Endif
			If Type("__tmp__.pwdcreate")<>'T'
				Alter Table cpusers Add pwdcreate T
			Endif
			If Type("__tmp__.pwdcomplexity")<>'N'
				Alter Table cpusers Add pwdcomplexity N(3)
			Endif
			If Type("__tmp__.pwddaysduration")<>'N'
				Alter Table cpusers Add pwddaysduration N(4)
			Endif
			If Type("__tmp__.pwdciclicity")<>'N'
				Alter Table cpusers Add pwdciclicity N(4)
			Endif
			If Type("__tmp__.captchalevel")<>'N'
				Alter Table cpusers Add captchalevel N(4)
			Endif
			If Type("__tmp__.cpccchk")<>'C'
				Alter Table cpusers Add cpccchk C(10)
			Endif
			On Error &i_olderr
		Endif
		Select __tmp__
		Use
	Endif
	If Not(cp_ExistTable('cpgroups',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cpgroups"))
		If nConn<>0
			cp_SQL(nConn,"create table cpgroups (code "+db_FieldType(db,'N',6,0)+" not null,name "+db_FieldType(db,'C',20,0);
				+",grptype "+db_FieldType(db,'C',1,0)+" "+",datestart "+db_FieldType(db,'D',1,0)+" "+",datestop "+db_FieldType(db,'D',1,0)+" ";
				+",pwdcomplexity "+db_FieldType(db,'N',3,0)+", pwddaysduration "+db_FieldType(db,'N',4,0)+",pwdciclicity "+db_FieldType(db,'N',4,0)+", captchalevel "+db_FieldType(db,'N',4,0)+" ";
				+",cpccchk "+db_FieldType(db,'C',10,0)+" ";
				+db_InlinePrimaryKey("cpgroups","cpgroups","code",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpgroups on cpgroups(code)')
			Endif
			db_CreatePrimaryKey(nConn,"cpgroups","cpgroups","code",cDatabaseType)
			*cp_SQL(nConn,"alter table cpgroups "+db_PrimaryKeyConstraint("cpgroups","cpgroups","code",cDatabaseType))
		Else
			Create Table cpgroups (Code N(4),Name Char(20),grptype Char(1), datestart d, datestop d, pwdcomplexity N(3), pwddaysduration N(4), pwdciclicity N(4), captchalevel N(4), cpccchk C(10))
			Index On Code Tag tag1
			Use
		Endif
	Else
		If nConn<>0
			cp_sqlexec(nConn,"select * from cpgroups","__tmp__")
			If Type("__tmp__.grptype")<>'C'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				cp_SQL(nConn,"alter table cpgroups add grptype "+db_FieldType(db,'C',1,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.datestart")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				cp_SQL(nConn,"alter table cpgroups add datestart "+db_FieldType(db,'D',1,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.datestop")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				cp_SQL(nConn,"alter table cpgroups add datestop "+db_FieldType(db,'D',1,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.pwdcomplexity")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				cp_SQL(nConn,"alter table cpgroups add pwdcomplexity "+db_FieldType(db,'N',3,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.pwddaysduration")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				cp_SQL(nConn,"alter table cpgroups add pwddaysduration "+db_FieldType(db,'N',4,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.pwdciclicity")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				cp_SQL(nConn,"alter table cpgroups add pwdciclicity "+db_FieldType(db,'N',4,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.captchalevel")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				cp_SQL(nConn,"alter table cpgroups add captchalevel "+db_FieldType(db,'N',4,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.cpccchk")<>'C'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				cp_SQL(nConn,"alter table cpgroups add cpccchk "+db_FieldType(db,'C',10,0),.F.,.F.,.T.)
			Endif
		Else
			Select * From cpgroups Into Cursor __tmp__
			i_olderr=On('ERROR')
			On Error =.T.
			If Used('cpgroups')
				Select cpgroups
				Use
			Endif
			If Type("__tmp__.grptype")<>'C'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				Alter Table cpgroups Add grptype C(1)
			Endif
			If Type("__tmp__.datestart")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				Alter Table cpgroups Add datestart d
			Endif
			If Type("__tmp__.datestop")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				Alter Table cpgroups Add datestop d
			Endif
			If Type("__tmp__.pwdcomplexity")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				Alter Table cpgroups Add pwdcomplexity N(3)
			Endif
			If Type("__tmp__.pwddaysduration")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				Alter Table cpgroups Add pwddaysduration N(4)
			Endif
			If Type("__tmp__.pwdciclicity")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				Alter Table cpgroups Add pwdciclicity N(4)
			Endif
			If Type("__tmp__.captchalevel")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				Alter Table cpgroups Add captchalevel N(4)
			Endif
			If Type("__tmp__.cpccchk")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
				Alter Table cpgroups Add cpccchk C(10)
			Endif
		Endif
		Select __tmp__
		Use
	Endif
	If Not(cp_ExistTable('cpgrpgrp',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cpusrgrp"))
		If nConn<>0
			cp_SQL(nConn,"create table cpgrpgrp (groupcode "+db_FieldType(db,'N',6,0)+" not null,groupbase "+db_FieldType(db,'N',6,0)+" not null "+",cpccchk "+db_FieldType(db,'C',10,0)+db_InlinePrimaryKey("cpgrpgrp","cpgrpgrp","groupcode,groupbase",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpgrpgrp on(groupcode,groupbase)')
			Endif
			db_CreatePrimaryKey(nConn,"cpgrpgrp","cpgrpgrp","groupcode,groupbase",cDatabaseType)
			*cp_SQL(nConn,"alter table cpgroups "+db_PrimaryKeyConstraint("cpgroups","cpgroups","code",cDatabaseType))
		Else
			Create Table cpgrpgrp (groupcode N(4),groupbase N(4),cpccchk C(10))
			Index On groupcode Tag tag1
			Index On groupbase Tag tag2
			Use
		Endif
	Else
		If nConn<>0
			SQLExec(nConn,"select * from cpgrpgrp","__tmp__")
			If Type("__tmp__.cpccchk")<>'C'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgrpgrp'))
				cp_SQL(nConn,"alter table cpgrpgrp add cpccchk "+db_FieldType(db,'C',10,0),.F.,.F.,.T.)
			Endif
		Else
			Select * From cpgrpgrp Into Cursor __tmp__
			i_olderr=On('ERROR')
			On Error =.T.
			If Used('cpgrpgrp')
				Select cpgrpgrp
				Use
			Endif
			If Type("__tmp__.cpccchk")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpgrpgrp'))
				Alter Table cpgrpgrp Add cpccchk C(10)
			Endif
		Endif
		Select __tmp__
		Use
	Endif
	If Not(cp_ExistTable('cpusrgrp',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cpusrgrp"))
		If nConn<>0
			cp_SQL(nConn,"create table cpusrgrp (groupcode "+db_FieldType(db,'N',6,0)+" not null,usercode "+db_FieldType(db,'N',6,0)+" not null ";
				+",datestart "+db_FieldType(db,'D',1,0)+" "+",datestop "+db_FieldType(db,'D',1,0)+",companies "+db_FieldType(db,'C',60,0)+",cpccchk "+db_FieldType(db,'C',10,0)+" ";
				+db_InlinePrimaryKey("cpusrgrp","cpusrgrp","groupcode,usercode",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpusrgrp on cpusrgrp(groupcode,usercode)')
			Endif
			db_CreatePrimaryKey(nConn,"cpusrgrp","cpusrgrp","groupcode,usercode",cDatabaseType)
			*cp_SQL(nConn,"alter table cpusrgrp "+db_PrimaryKeyConstraint("cpusrgrp","cpusrgrp","groupcode,usercode",cDatabaseType))
			cp_SQL(nConn,"create index cpusrgrp1 on cpusrgrp(groupcode)")
			cp_SQL(nConn,"create index cpusrgrp2 on cpusrgrp(usercode)")
		Else
			Create Table cpusrgrp (groupcode N(4),usercode N(4),datestart d,datestop d,companies C(60),cpccchk C(10))
			Index On groupcode Tag tag1
			Index On usercode Tag tag2
			Use
		Endif
	Else
		If nConn<>0
			SQLExec(nConn,"select * from cpusrgrp","__tmp__")
			If Type("__tmp__.datestart")<>'C'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpusrgrp'))
				cp_SQL(nConn,"alter table cpusrgrp add datestart "+db_FieldType(db,'D',1,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.datestart")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpusrgrp'))
				cp_SQL(nConn,"alter table cpusrgrp add datestart "+db_FieldType(db,'D',1,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.datestop")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpusrgrp'))
				cp_SQL(nConn,"alter table cpusrgrp add datestop "+db_FieldType(db,'D',1,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.companies")<>'C'
				cp_SQL(nConn,"alter table cpusrgrp add companies "+db_FieldType(db,'C',60,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.cpccchk")<>'C'
				cp_SQL(nConn,"alter table cpusrgrp add cpccchk "+db_FieldType(db,'C',10,0),.F.,.F.,.T.)
			Endif
		Else
			Select * From cpusrgrp Into Cursor __tmp__
			i_olderr=On('ERROR')
			On Error =.T.
			If Used('cpusrgrp')
				Select cpusrgrp
				Use
			Endif
			If Type("__tmp__.datestart")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpusrgrp'))
				Alter Table cpusrgrp Add datestart d
			Endif
			If Type("__tmp__.datestop")<>'D'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpusrgrp'))
				Alter Table cpusrgrp Add datestop d
			Endif
			If Type("__tmp__.companies")<>'C'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpusrgrp'))
				Alter Table cpusrgrp Add companies C(60)
			Endif
			If Type("__tmp__.cpccchk")<>'C'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpusrgrp'))
				Alter Table cpusrgrp Add cpccchk C(10)
			Endif
		Endif
		Select __tmp__
		Use
	Endif
	If Not(cp_ExistTable('cpprgsec',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cpprgsec"))
		If nConn<>0
			cp_SQL(nConn,"create table cpprgsec (progname "+db_FieldType(db,'C',50,0)+" not null, grpcode "+db_FieldType(db,'N',6,0)+" not null, usrcode "+db_FieldType(db,'N',6,0)+" not null, sec1 "+db_FieldType(db,'N',6,0)+",sec2 "+db_FieldType(db,'N',6,0)+",sec3 "+db_FieldType(db,'N',6,0)+",sec4 "+db_FieldType(db,'N',6,0)+db_InlinePrimaryKey("cpprgsec","cpprgsec","progname,grpcode,usrcode",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpprgsec on cpprgsec(progname,grpcode,usrcode)')
			Endif
			db_CreatePrimaryKey(nConn,"cpprgsec","cpprgsec","progname,grpcode,usrcode",cDatabaseType)
			*cp_SQL(nConn,"alter table cpprgsec "+db_PrimaryKeyConstraint("cpprgsec","cpprgsec","progname,grpcode",cDatabaseType))
			cp_SQL(nConn,"create index cpprgsec1 on cpprgsec(progname)")
			cp_SQL(nConn,"create index cpprgsec2 on cpprgsec(grpcode)")
			cp_SQL(nConn,"create index cpprgsec3 on cpprgsec(usrcode)")
		Else
			Create Table cpprgsec (progname C(50), grpcode N(6), usrcode N(6), sec1 N(6), sec2 N(6), sec3 N(6), sec4 N(6))
			Index On progname Tag tag1
			Index On grpcode Tag tag2
			Index On usrcode Tag tag3
			Use
		Endif
	Else
		Local i_bModify
		If nConn<>0
			cp_SQL(nConn,"select * from cpprgsec","__tmpsec__")
			r=db_ModifyColumn("progname",db_FieldType(db,'C',50,0),db,nConn)
			i_bModify=Type('__tmpsec__.usrcode')='U'
			If i_bModify And Used('__tmpsec__') And !Empty(r)
				s=db_DropPrimaryKey("cpprgsec","cpprgsec",db)
				cp_SQL(nConn,"alter table cpprgsec "+s)
				s=db_DropIndex("cpprgsec","cpprgsec","cpprgsec1",db)
				cp_SQL(nConn,"drop index "+s)
				cp_SQL(nConn,"alter table cpprgsec "+r+Iif(Inlist(db,'SQLServer'),' not null',''))
				r=db_ModifyColumn("sec1",db_FieldType(db,'N',6,0),db,nConn)
				cp_SQL(nConn,"alter table cpprgsec "+r)
				r=db_ModifyColumn("sec2",db_FieldType(db,'N',6,0),db,nConn)
				cp_SQL(nConn,"alter table cpprgsec "+r)
				r=db_ModifyColumn("sec3",db_FieldType(db,'N',6,0),db,nConn)
				cp_SQL(nConn,"alter table cpprgsec "+r)
				r=db_ModifyColumn("sec4",db_FieldType(db,'N',6,0),db,nConn)
				cp_SQL(nConn,"alter table cpprgsec "+r)
				cp_SQL(nConn,"alter table cpprgsec add usrcode "+db_FieldType(db,'N',6,0)+" default 0 not null")
				cp_SQL(nConn,"update cpprgsec set usrcode=0")
				db_CreatePrimaryKey(nConn,"cpprgsec","cpprgsec","progname,grpcode,usrcode",cDatabaseType)
				*cp_SQL(nConn,"alter table cpprgsec "+db_PrimaryKeyConstraint("cpprgsec","cpprgsec","progname,grpcode,usrcode",cDatabaseType))
				cp_SQL(nConn,"create index cpprgsec1 on cpprgsec(progname)")
				cp_SQL(nConn,"create index cpprgsec3 on cpprgsec(usrcode)")
			Endif
			If Used('__tmpsec__')
				Use In __tmpsec__
			Endif
		Else
			i_olderr=On('ERROR')
			On Error =.T.
			Select * From cpprgsec Into Cursor __tmpsec__
			i_bModify=Type('__tmpsec__.usrcode')='U'
			Use
			If Used('cpprgsec')
				Select cpprgsec
				Use
			Endif
			If i_bModify
				cp_msg(cp_MsgFormat(MSG_UPDATING__,cpprgsec))
				* --- modifica il tracciato record
				Use cpprgsec Excl
				Select * From cpprgsec Into Cursor __tmpsec__
				Alter Table cpprgsec Alter Column progname C(50)
				Alter Table cpprgsec Alter Column sec1 N(6)
				Alter Table cpprgsec Alter Column sec2 N(6)
				Alter Table cpprgsec Alter Column sec3 N(6)
				Alter Table cpprgsec Alter Column sec4 N(6)
				Alter Table cpprgsec Add Column usrcode N(6)
				Update cpprgsec Set usrcode=0
				Select cpprgsec
				Index On usrcode Tag tag3
				* --- travasa i dati
				Select __tmpsec__
				Go Top
				Select cpprgsec
				Go Top
				Do While Not(Eof())
					Replace sec1 With Iif(__tmpsec__.sec1,1,0)
					Replace sec2 With Iif(__tmpsec__.sec2,1,0)
					Replace sec3 With Iif(__tmpsec__.sec3,1,0)
					Replace sec4 With Iif(__tmpsec__.sec4,1,0)
					Skip
					Skip In __tmpsec__
				Enddo
				Use In __tmpsec__
				Use In cpprgsec
			Endif
			On Error &i_olderr
		Endif
	Endif
	If Not(cp_ExistTable('cpttbls',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cpttbls"))
		If nConn<>0
			cp_SQL(nConn,"create table cpttbls (filename "+db_FieldType(db,'C',30,0)+" not null, phname "+db_FieldType(db,'C',50,0)+", flagyear "+db_FieldType(db,'C',1,0)+", flaguser "+db_FieldType(db,'C',1,0)+", flagcomp "+db_FieldType(db,'C',1,0)+", servername "+db_FieldType(db,'C',10,0)+", datemod "+db_FieldType(db,'C',14,0)+", replica "+db_FieldType(db,'C',10,0)+", stato "+db_FieldType(db,'N',6,0)+", mastertable "+db_FieldType(db,'C',10,0)+", replkey "+db_FieldType(db,'C',10,0);
				+", tstrigger "+db_FieldType(db,'N',3,0)+", fkcount "+db_FieldType(db,'N',3,0)+", tbcheck "+db_FieldType(db,'N',3,0);
				+db_InlinePrimaryKey("cpttbls","cpttbls","filename",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpttbls on cpttbls(filename)')
			Endif
			db_CreatePrimaryKey(nConn,"cpttbls","cpttbls","filename",cDatabaseType)
			*cp_SQL(nConn,"alter table cpttbls "+db_PrimaryKeyConstraint("cpttbls","cpttbls","filename",cDatabaseType))
		Else
			Create Table cpttbls (FILENAME C(30), PhName C(50), FlagYear C(1), FlagUser C(1), FlagComp C(1), ServerName C(10), DateMod C(14), Replica C(10), Stato N(1), MasterTable C(10), ReplKey C(10), tstrigger N(3), fkcount N(3), tbcheck N(3))
			Index On FILENAME Tag tag1
			Use
		Endif
	Else
		If nConn<>0
			SQLExec(nConn,"select * from cpttbls","__tmp__")
			If Type("__tmp__.tstrigger")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpttbls'))
				cp_SQL(nConn,"alter table cpttbls add tstrigger "+db_FieldType(db,'N',3,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.fkcount")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpttbls'))
				cp_SQL(nConn,"alter table cpttbls add fkcount "+db_FieldType(db,'N',3,0),.F.,.F.,.T.)
			Endif
			If Type("__tmp__.tbcheck")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpttbls'))
				cp_SQL(nConn,"alter table cpttbls add tbcheck "+db_FieldType(db,'N',3,0),.F.,.F.,.T.)
			Endif
		Else
			Select * From cpttbls Into Cursor __tmp__
			i_olderr=On('ERROR')
			On Error =.T.
			If Used('cpttbls')
				Select cpttbls
				Use
			Endif
			If Type("__tmp__.tstrigger")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpttbls'))
				Alter Table cpttbls Add tstrigger N(3)
			Endif
			If Type("__tmp__.fkcount")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpttbls'))
				Alter Table cpttbls Add fkcount N(3)
			Endif
			If Type("__tmp__.tbcheck")<>'N'
				cp_msg( cp_MsgFormat(MSG_UPDATING__,'cpttbls'))
				Alter Table cpttbls Add tbcheck N(3)
			Endif
		Endif
		Select __tmp__
		Use
	Endif
	If Not(cp_ExistTable('cptsrvr',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cptsrvr"))
		If nConn<>0
			cp_SQL(nConn,"create table cptsrvr (servername "+db_FieldType(db,'C',10,0)+" not null, serverdesc "+db_FieldType(db,'C',30,0)+", odbcdatasource "+db_FieldType(db,'C',254,0)+", whenconn "+db_FieldType(db,'N',6,0)+", postit "+db_FieldType(db,'C',1,0)+", databasetype "+db_FieldType(db,'C',60,0)+db_InlinePrimaryKey("cptsrvr","cptsrvr","servername",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cptsrvr on cptsrvr(ServerName)')
			Endif
			db_CreatePrimaryKey(nConn,"cptsrvr","cptsrvr","servername",cDatabaseType)
			*cp_SQL(nConn,"alter table cptsrvr "+db_PrimaryKeyConstraint("cptsrvr","cptsrvr","ServerName",cDatabaseType))
		Else
			Create Table cptsrvr (ServerName C(10), ServerDesc C(30), ODBCDataSource C(254), WhenConn N(1), PostIt C(1), DatabaseType C(30))
			Index On ServerName Tag tag1
			Use
		Endif
	Else
		If nConn<>0
			r=db_ModifyColumn("odbcdatasource",db_FieldType(db,'C',254,0),db,nConn)
			If !Empty(r)
				cp_SQL(nConn,"alter table cptsrvr "+r)
			Endif
			r=db_ModifyColumn("databasetype",db_FieldType(db,'C',60,0),db,nConn)
			If !Empty(r)
				cp_SQL(nConn,"alter table cptsrvr "+r)
			Endif
		Else
			i_olderr=On('ERROR')
			On Error =.T.
			If Used('cptsrvr')
				Select cptsrvr
				Use
			Endif
			Alter Table cptsrvr Alter Column ODBCDataSource C(254)
			On Error &i_olderr
		Endif
	Endif
	If Not(cp_ExistTable('cpazi',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cpazi"))
		If nConn<>0
			cp_SQL(nConn,"create table cpazi (codazi "+db_FieldType(db,'C',10,0)+" not null, desazi "+db_FieldType(db,'C',30,0)+db_InlinePrimaryKey("cpazi","cpazi","codazi",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpazi on cpazi(codazi)')
			Endif
			db_CreatePrimaryKey(nConn,"cpazi","cpazi","codazi",cDatabaseType)
			*cp_SQL(nConn,"alter table cpazi "+db_PrimaryKeyConstraint("cpazi","cpazi","codazi",cDatabaseType))
		Else
			Create Table cpazi(codazi C(10),desazi C(30))
			Index On codazi Tag tag1
			Use
		Endif
	Endif
	If Not(cp_ExistTable('cpusrrep',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cpusrrep"))
		If nConn<>0
			*** Zucchetti Aulla - campo gestione solo testo e Report behavior ("80"/"90" )
			cp_SQL(nConn,"create table cpusrrep (nroasso "+db_FieldType(db,'C',6,0)+" not null, desasso "+db_FieldType(db,'C',40,0)+", usrasso "+db_FieldType(db,'N',4,0)+", aziasso "+db_FieldType(db,'C',10,0)+", wstasso "+db_FieldType(db,'C',10,0)+", repasso "+db_FieldType(db,'C',200,0)+", devasso "+db_FieldType(db,'C',200,0)+", copasso "+db_FieldType(db,'N',3,0)+", modasso "+db_FieldType(db,'C',200,0)+", behasso "+db_FieldType(db,'C',2,0)+",cpccchk "+db_FieldType(db,'C',10,0)+db_InlinePrimaryKey("cpusrrep","cpusrrep","nroasso",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpusrrep on cpusrrep(nroasso)')
			Endif
			db_CreatePrimaryKey(nConn,"cpusrrep","cpusrrep","nroasso",cDatabaseType)
			*cp_SQL(nConn,"alter table cpusrrep "+db_PrimaryKeyConstraint("cpusrrep","cpusrrep","nroasso",cDatabaseType))
			cp_SQL(nConn,"create index cpusrrep1 on cpusrrep(desasso)")
		Else
			*** Zucchetti Aulla - campo gestione solo testo e Report behavior ("80"/"90" )
			Create Table cpusrrep(nroasso C(6), desasso C(40), usrasso N(4), aziasso C(10), wstasso C(10), repasso C(200), devasso C(200), copasso N(3), modasso C(200), behasso C(2), cpccchk C(10))
			Index On nroasso Tag idx1
			Index On desasso Tag tag2
			Use
		Endif
	Else
		* --- !!!
		If nConn<>0
			r=db_ModifyColumn("repasso",db_FieldType(db,'C',200,0),db,nConn)
			If !Empty(r)
				cp_SQL(nConn,"alter table cpusrrep "+r)
			Endif
			r=db_ModifyColumn("devasso",db_FieldType(db,'C',200,0),db,nConn)
			If !Empty(r)
				cp_SQL(nConn,"alter table cpusrrep "+r)
			Endif
			*** Zucchetti Aulla - campo gestione solo testo
			cp_SQL(nConn,"alter table cpusrrep add modasso " + db_FieldType(db, "C", 200, 0),.F.,.F.,.T.)			
			*** Zucchetti Aulla - Report behavior ("80"/"90" )			
			cp_SQL(nConn,"alter table cpusrrep add behasso " + db_FieldType(db, "C", 2, 0),.F.,.F.,.T.)				
		Else
			Alter Table cpusrrep Alter repasso C(200)
			Alter Table cpusrrep Alter devasso C(200)
			*** Zucchetti Aulla - campo gestione solo testo
			Alter Table cpusrrep Add modasso C(200)
			*** Zucchetti Aulla - Report behavior ("80"/"90" )			
			Alter Table cpusrrep Add behasso C(2)
		Endif
	Endif
	If Not(cp_ExistTable('cpssomap',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cpssomap"))
		If nConn<>0
			cp_SQL(nConn,"create table cpssomap (ssoid "+db_FieldType(db,'C',100,0)+"not null, userid "+db_FieldType(db,'N',6,0)+" "+db_InlinePrimaryKey("cpssomap","cpssomap","ssoid",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpssomap on cpssomap(ssoid)')
			Endif
			db_CreatePrimaryKey(nConn,"cpssomap","cpssomap","ssoid",cDatabaseType)
		Else
			Create Table cpssomap(ssoid C(100),Userid N(6,0))
			Index On ssoid Tag tag1
			Use
		Endif
	Endif
	* --- Creazione tabella lingue per traduzione dati
	If Not(cp_ExistTable('cplangs',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cplangs"))
		If nConn<>0
			*-- Risolto il problema in cp_langs...in posgreSQL creava la tabella con un campo chiave inesistente
			cp_SQL(nConn,"create table cplangs (code "+db_FieldType(db,'C',3,0)+" not null, name "+db_FieldType(db,'C',24,0)+", datalanguage "+db_FieldType(db,'C',1,0)+", ptranslation "+db_FieldType(db,'C',1,0)+", cpccchk "+db_FieldType(db,'C',10,0)+db_InlinePrimaryKey("cplangs","cplangs","code",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cplangs on cplangs(code)')
			Endif
			db_CreatePrimaryKey(nConn,"cplangs","cplangs","code",cDatabaseType)
		Else
			Create Table cplangs (Code C(3), Name C(24), datalanguage C(1),ptranslation C(1),cpccchk C(10))
			Index On Code Tag tag1
			Use
		Endif
	Else
		If nConn<>0
			cp_sqlexec(nConn,"select * from cplangs","__tmp__")
			If Type("__tmp__.datalanguage")<>'C'
				cp_SQL(nConn,"alter table cplangs add datalanguage "+db_FieldType(db,'C',1,0),.F.,.F.,.T.)
				cp_SQL(nConn,"UPDATE cplangs SET datalanguage='N'",.F.,.F.,.T.)
			Endif
			If Type("__tmp__.ptranslation")<>'C'
				cp_SQL(nConn,"alter table cplangs add ptranslation "+db_FieldType(db,'C',1,0),.F.,.F.,.T.)
				cp_SQL(nConn,"UPDATE cplangs SET ptranslation='N'",.F.,.F.,.T.)
			Endif
		Else
			Select * From cplangs Into Cursor __tmp__
			i_olderr=On('ERROR')
			On Error =.T.
			If Used('cplangs')
				Select cplangs
				Use
			Endif
			If Type("__tmp__.datalanguage")<>'C'
				Alter Table cplangs Add datalanguage C(1)
				Update cplangs Set datalanguage='N'
			Endif
			If Type("__tmp__.ptranslation")<>'C'
				Alter Table cplangs Add ptranslation C(1)
				Update cplangs Set ptranslation='N'
			Endif
		Endif
		Select __tmp__
		Use
	Endif
	If Not(cp_ExistTable('cprecsed',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cprecsed"))
		If nConn<>0
			cp_SQL(nConn,"create table cprecsed (tablename "+db_FieldType(db,'C',50,0)+" not null, progname "+db_FieldType(db,'C',50,0)+" not null, rfrownum "+db_FieldType(db,'N',6,0)+" not null, grpcode "+db_FieldType(db,'N',5,0)+" not null, usrcode "+db_FieldType(db,'N',5,0)+" not null "+db_InlinePrimaryKey("cprecsed","cprecsed","tablename,progname,rfrownum,grpcode,usrcode",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cprecsed on cprecsed(tablename,progname,rfrownum,grpcode,usrcode)')
			Endif
			db_CreatePrimaryKey(nConn,"cprecsed","cprecsed","tablename,progname,rfrownum,grpcode,usrcode",cDatabaseType)
	        cp_SQL(nConn,"create index cprecsed1 on cprecsed(tablename)")
			cp_SQL(nConn,"create index cprecsed2 on cprecsed(progname)")
			cp_SQL(nConn,"create index cprecsed3 on cprecsed(rfrownum)")
			cp_SQL(nConn,"create index cprecsed4 on cprecsed(grpcode)")
			cp_SQL(nConn,"create index cprecsed5 on cprecsed(usrcode)")
		Else
			Create Table cprecsed(tablename C(50), progname C(50), rfrownum N(6), grpcode N(5), usrcode N(5))
			Index On tablename Tag tag1
			Index On progname Tag tag2
			Index On rfrownum Tag tag3
			Index On grpcode Tag tag4
			Index On usrcode Tag tag5
			Use
		Endif
	Endif
	If Not(cp_ExistTable('cprecsec',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cprecsec"))
		If nConn<>0
			cp_SQL(nConn,"create table cprecsec (tablename "+db_FieldType(db,'C',50,0)+" not null, progname "+db_FieldType(db,'C',50,0)+" not null, checkact "+db_FieldType(db,'C',1,0)+db_InlinePrimaryKey("cprecsec","cprecsec","tablename,progname",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cprecsec on cprecsec(tablename,progname)')
			Endif
			db_CreatePrimaryKey(nConn,"cprecsec","cprecsec","tablename,progname",cDatabaseType)
	        cp_SQL(nConn,"create index cprecsec1 on cprecsec(tablename)")
			cp_SQL(nConn,"create index cprecsec2 on cprecsec(progname)")
		Else
			Create Table cprecsec(tablename C(50), progname C(50), checkact C(1))
			Index On tablename Tag tag1
			Index On progname Tag tag2
			Use
		Endif
	Endif
	If Not(cp_ExistTable('cprecsecd',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"cprecsecd"))
		If nConn<>0
			cp_SQL(nConn,"create table cprecsecd (tablename "+db_FieldType(db,'C',50,0)+" not null, progname "+db_FieldType(db,'C',50,0)+" not null, cprownum "+db_FieldType(db,'N',6,0)+" not null, cproword "+db_FieldType(db,'N',6,0)+", descri "+db_FieldType(db,'C',150,0)+", codazi "+db_FieldType(db,'C',5,0)+", queryfilter "+db_FieldType(db,'C',254,0)+db_InlinePrimaryKey("cprecsecd","cprecsecd","tablename,progname,cprownum",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cprecsecd on cprecsecd(tablename,progname,cprownum)')
			Endif
			db_CreatePrimaryKey(nConn,"cprecsecd","cprecsecd","tablename,progname,cprownum",cDatabaseType)
	        cp_SQL(nConn,"create index cprecsecd1 on cprecsecd(tablename)")
			cp_SQL(nConn,"create index cprecsecd2 on cprecsecd(progname)")
			cp_SQL(nConn,"create index cprecsecd3 on cprecsecd(cprownum)")
		Else
			Create Table cprecsecd(tablename C(50), progname C(50), cprownum N(6), cproword N(6), descri C(150), codazi C(5), queryfilter C(254))
			Index On tablename Tag tag1
			Index On progname Tag tag2
			Index On cprownum Tag tag3
			Use
		Endif
	Endif
	i_ok=i_ok And cp_CreatePostItTables(nConn,cDatabaseType)
	Return(i_ok)

Func cp_CreatePostItTables(nConn,cDatabaseType)
	Local db,i_olderr
	db=cDatabaseType
	If Not(cp_ExistTable('postit',nConn))
		cp_msg( cp_MsgFormat(MSG_CREATING_TABLE___D,"postit"))
		If nConn<>0
			*--- Zucchetti Aulla Inizio - Postit
			cp_SQL(nConn,"create table postit (code "+db_FieldType(db,'C',10,0)+" not null,usercode "+db_FieldType(db,'N',6,0)+", created "+db_FieldType(db,'T',0,0)+",createdby "+db_FieldType(db,'N',6,0)+",postit "+db_FieldType(cDatabaseType,'M',0,0)+;
				",status "+db_FieldType(db,'C',3,0)+",px "+db_FieldType(db,'N',6,0)+",py "+db_FieldType(db,'N',6,0)+",wi "+db_FieldType(db,'N',6,0)+",he "+db_FieldType(db,'N',6,0)+;
                ",datestart "+db_FieldType(db, "T", 0, 0)+",datestop "+db_FieldType(db, "T", 0, 0)+",cplevel "+db_FieldType(db, "C", 1, 0)+",cpimage "+db_FieldType(db, "C", 100, 0)+",subject "+db_FieldType(db,'C',250,0)+",cpccchk "+db_FieldType(db,'C',10,0)+", parent " + db_FieldType(db, "C", 10, 0)+;
				db_InlinePrimaryKey("postit","postit","code",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			*--- Zucchetti Aulla Fine - Postit
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_postit on postit(code)')
			Endif
			db_CreatePrimaryKey(nConn,"postit","postit","code",cDatabaseType)
			*cp_SQL(nConn,"alter table postit "+db_PrimaryKeyConstraint("postit","postit","code",cDatabaseType))
			cp_SQL(nConn,"create index postit1 on postit(usercode)")
		Else
			*--- Zucchetti Aulla Inizio - Postit
            Create Table PostIt (Code c(10),usercode N(4), created T,createdby N(4),PostIt M,Status c(3),px N(4),py N(4),wi N(4),he N(4),datestart T,datestop T,cplevel c(1),cpimage c(100), subject C(250), cpccchk C(10), parent C(10))
			*--- Zucchetti Aulla Fine - Postit
			Index On Code Tag tag1
			Index On usercode Tag tag2
			Use
		Endif
	Else
		cp_msg( cp_MsgFormat(MSG_UPDATING_TABLE___D,"postit"))
		If nConn<>0
			cp_SQL(nConn,"alter table postit add datestart " + db_FieldType(db, "T", 0, 0),.F.,.F.,.T.)
			cp_SQL(nConn,"alter table postit add datestop " + db_FieldType(db, "T", 0, 0),.F.,.F.,.T.)
			cp_SQL(nConn,"alter table postit add cplevel " + db_FieldType(db, "C", 1, 0),.F.,.F.,.T.)
			cp_SQL(nConn,"alter table postit add cpimage " + db_FieldType(db, "C", 100, 0),.F.,.F.,.T.)
			*--- Zucchetti Aulla Inizio - Postit
			cp_SQL(nConn,"alter table postit add subject " + db_FieldType(db, "C", 250, 0),.F.,.F.,.T.)
            cp_SQL(nConn,"alter table postit add cpccchk " + db_FieldType(db, "C", 10, 0),.F.,.F.,.T.)
			cp_SQL(nConn,"alter table postit add parent " + db_FieldType(db, "C", 10, 0),.F.,.F.,.T.)
			*--- Zucchetti Aulla Fine - Postit
		Else
			i_olderr=On('ERROR')
			On Error =.T.
			If Used('postit')
				Select PostIt
				Use
			Endif
			Alter Table PostIt Add datestart t
			Alter Table PostIt Add datestop t
			Alter Table PostIt Add cplevel C(1)
			Alter Table PostIt Add cpimage C(100)
			*--- Zucchetti Aulla Inizio - Postit
			Alter Table PostIt Add subject C(250)
            Alter Table PostIt Add cpccchk C(10)
			*--- Zucchetti Aulla Fine - Postit
			On Error &i_olderr
		Endif
	Endif
	If Not(cp_ExistTable('cpwarn',nConn))
		cp_msg(cp_MsgFormat(MSG_CREATING_TABLE___D,"cpwarn"))
		If nConn<>0
			* --- Zucchetti Aulla - Allargato il campo AutoNum a N(15)
			cp_SQL(nConn,"create table cpwarn (tablecode "+db_FieldType(db,'C',50,0)+" not null, warncode "+db_FieldType(db,'C',10,0)+" not null, autonum "+db_FieldType(db,'N',15,0)+db_InlinePrimaryKey("cpwarn","cpwarn","tablecode,warncode",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			If cDatabaseType='DB2/390'
				r=cp_SQL(nConn,'create unique index pk_cpwarn on cpwarn(tablecode,warncode)')
			Endif
			db_CreatePrimaryKey(nConn,"cpwarn","cpwarn","tablecode,warncode",cDatabaseType)
			*cp_SQL(nConn,"alter table cpwarn "+db_PrimaryKeyConstraint("cpwarn","cpwarn","tablecode,warncode",cDatabaseType))
			*cp_SQL(nConn,"create index cpwarn1 on cpwarn(tablecode)")
			*cp_SQL(nConn,"create index cpwarn2 on cpwarn(warncode)")
		Else
			* --- Zucchetti Aulla - Allargato il campo AutoNum a N(15)
			Create Table cpwarn (tablecode C(100), warncode C(10), autonum N(15,0))
			Index On tablecode Tag tag1
			Index On warncode Tag tag2
			Use
		Endif
	Else
		Local i_dbtype
		i_dbtype=""
		If nConn<>0
			i_dbtype=Lower(SQLXGetDatabaseType(nConn))
		Endif
		If i_dbtype="sqlserver"
			cp_SQL(nConn,"drop index cpwarn.cpwarn1","",.F.,.T.)
			cp_SQL(nConn,"drop index cpwarn.cpwarn2","",.F.,.T.)
		Endif
		If i_dbtype="oracle"
			cp_SQL(nConn,"drop index cpwarn1","",.F.,.T.)
			cp_SQL(nConn,"drop index cpwarn2","",.F.,.T.)
		Endif
	Endif
	Return(.T.)

Func cp_VerifySystemTables(nConn)
	Local i_ok
	i_ok=.T.
	If Not(cp_ExistTable('persist',nConn))
		i_ok=.F.
	Endif
    If Not(cp_ExistTable('postit',nConn,'datestart','cpccchk'))
		i_ok=.F.
	Endif
	If Not(cp_ExistTable('cpusers',nConn,Iif(i_ServerConn[1,6]='DBMaker',"lang","language")))
		i_ok=.F.
	Endif
	If Not(cp_ExistTable('cpgroups',nConn))
		i_ok=.F.
	Endif
	If Not(cp_ExistTable('cpusrgrp',nConn))
		i_ok=.F.
	Endif
	If Not(cp_ExistTable('cpwarn',nConn))
		i_ok=.F.
	Endif
	If Not(cp_ExistTable('cpprgsec',nConn))
		i_ok=.F.
	Else
		If cp_FldLen('cpprgsec',nConn,'progname')<50
			i_ok=.F.
		Endif
	Endif
	If Not(cp_ExistTable('cpttbls',nConn))
		i_ok=.F.
	Endif
	If Not(cp_ExistTable('cptsrvr',nConn))
		i_ok=.F.
	Else
		If cp_FldLen('cptsrvr',nConn,'ODBCDataSource')<254
			i_ok=.F.
		Endif
	Endif
	If Not(cp_ExistTable('cpazi',nConn))
		i_ok=.F.
	Endif
	If Not(cp_ExistTable('cpusrrep',nConn))
		i_ok=.F.
	Endif
	If Not(cp_ExistTable('cpssomap',nConn))
		i_ok=.F.
	Endif
	If Not(cp_ExistTable('cplangs',nConn))
		i_ok=.F.
	ENDIF
	If Not(cp_ExistTable('cprecsed',nConn))
		i_ok=.F.
	ENDIF
	If Not(cp_ExistTable('cprecsec',nConn))
		i_ok=.F.
	ENDIF
	If Not(cp_ExistTable('cprecsecd',nConn))
		i_ok=.F.
	Endif
	Return(i_ok)

Proc cp_PackSystemTables()
	cp_PackSystemTable('persist')
	cp_PackSystemTable('postit')
	cp_PackSystemTable('cpusers')
	cp_PackSystemTable('cpgroups')
	cp_PackSystemTable('cpusrgrp')
	cp_PackSystemTable('cpwarn')
	cp_PackSystemTable('cpprgsec')
	cp_PackSystemTable('cpttbls')
	cp_PackSystemTable('cpazi')
	cp_PackSystemTable('cpusrrep')
	cp_PackSystemTable('cpssomap')
	cp_PackSystemTable('cplangs')
	*cp_PackSystemTable('cptsrvr')
	Return

Proc cp_PackSystemTable(cName)
	If Used(cName)
		Select (cName)
		Use (cName) Excl
		Pack
		Use (cName)
	Else
		Select 0
		Use (cName) Excl
		Pack
		Use
	Endif
	Return

Func cp_GetXdcDate(i_cModule)
	Local i_f,i_s,i_d
	i_d='?'
	i_f=Fopen(i_cModule+'.xdc')
	If i_f<>-1
		i_s=Fgets(i_f)
		If i_s="* --- Code Painter Extended Dictionary"
			i_d=Fgets(i_f)
			i_d=Substr(i_d,2,Len(i_d)-2)
		Endif
		Fclose(i_f)
	Endif
	Return(i_d)

Func cp_UpdatedPlanCheck(nConn)
	Local i_ok,i_s,i_res,i_msg,i_date,i_p,i_m,i_mm,d
	i_ok=.T.
	i_res=0
	If nConn<>0
		i_res=cp_SQL(nConn,"select * from persist where code='PlanCheckDate'","__tmp__")
		If i_res<>-1
			i_date=__tmp__.persist
			Use
		Else
			i_ok=.F.
		Endif
	Else
		Select * From persist Where Code='PlanCheckDate' Into Cursor __tmp__
		i_date=__tmp__.persist
		i_ok=Not(Eof())
		Use
	Endif
	If i_ok
		i_msg=''
		d=cp_GetXdcDate(i_CpDic)
		If d='?'
			Return(.T.)
		Endif
		If Trim(Left(i_date,14))<d
			i_ok=.F.
			i_msg='Analisi'
		Endif
		If Vartype(i_cModules)='C' And Not(Empty(i_cModules))
			i_m=i_cModules
			i_p=At(',',i_m)
			Do While i_p<>0
				i_mm=Left(i_m,i_p-1)
				If cp_UpdatedModuleCheck(i_date,i_mm,cp_GetXdcDate(cp_FindModule(i_mm)))
					i_ok=.F.
					i_msg=i_msg+Iif(Empty(i_msg),'',',')+i_mm
				Endif
				i_m=Substr(i_m,i_p+1)
				i_p=At(',',i_m)
			Enddo
			If cp_UpdatedModuleCheck(i_date,i_m,cp_GetXdcDate(cp_FindModule(i_m)))
				i_ok=.F.
				i_msg=i_msg+Iif(Empty(i_msg),'',',')+i_m
			Endif
		Endif
		*--- Zucchetti Aulla Inizio - Framework monitor
		* Modifiche: aggiunta del path 'custom\userdir\' in testa a 'cp_added_fields.xdc'
		If i_MonitorFramework
			If cp_fileexist(Alltrim(i_usrPath)+'cp_added_fields.xdc') And cp_UpdatedModuleCheck(i_date,'cp_added_fields',cp_GetXdcDate(Alltrim(i_usrPath)+'cp_added_fields'))
				i_ok=.F.
				i_msg=i_msg+Iif(Empty(i_msg),'',',')+'cp_added_fields'
			Endif
		Else
			If cp_fileexist('cp_added_fields.xdc') And cp_UpdatedModuleCheck(i_date,'cp_added_fields',cp_GetXdcDate('cp_added_fields'))
				i_ok=.F.
				i_msg=i_msg+Iif(Empty(i_msg),'',',')+'cp_added_fields'
			Endif
		Endif
		*--- Zucchetti Aulla Fine - Framework monitor
        *--- Zucchetti Aulla Inizio - iRevolution XR_REVI.XDC
        *--- Se presente carico il dizionario delle tabelle ZR
        If TYPE("g_REVI")='C' AND g_REVI = 'S' AND cp_fileexist('..\revi\exe\zr_revi.xdc') And cp_UpdatedModuleCheck(i_date,'zr_revi',cp_GetXdcDate('..\revi\exe\zr_revi'))
			i_ok=.F.
			i_msg=i_msg+Iif(Empty(i_msg),'',',')+'zr_revi'
        Endif
        *--- Zucchetti Aulla Fine - iRevolution XR_REVI.XDC		
		
		If Not(i_ok)
			cp_msg(cp_MsgFormat(MSG_DATABASE_MUST_BE_UPDATED_LB_TABLES_CL___RB,i_msg),.F.)
		Endif
	Else
		cp_msg(cp_Translate(MSG_DATABASE_MUST_BE_CREATED),.F.)
	Endif
	Return(i_ok)

Func cp_UpdatedModuleCheck(i_date,i_module,i_moduledate,i_bEq)
	Local i_p
	i_p=Atc(','+i_module+'=',i_date)
	If i_p=0
		Return(.T.)
	Endif
	If i_bEq
		If Substr(i_date,i_p+2+Len(i_module),14)<>i_moduledate
			Return(.T.)
		Endif
	Else
		*WAIT WINDOW "i_date="+i_date+' i_moduledate='+i_moduledate
		If Substr(i_date,i_p+2+Len(i_module),14)<i_moduledate
			Return(.T.)
		Endif
	Endif
	Return(.F.)

Proc cp_ManageDB()
	Local i_tmp,i_cnt
	If Not(cp_OpenSchema('exclusive'))
		cp_msg(cp_Translate(MSG_CANNOT_OPEN_DATABASE_WITH_ADMIN_RIGHTS),.F.)
		Return
	Endif
	* --- carica dizionario dati
	If !cp_ReadXdc()
		Return(.F.)
	Endif
	* --- controlla che non ci siano tabelle "aperte"
	i_cnt=0
	For i_tmp=1 To i_nTables
		i_cnt=i_cnt+i_TableProp[i_tmp,4]
	Next
	If i_cnt<>0
		cp_msg(cp_Translate(MSG_TABLES_IN_USE_C_CLOSE_ALL_WINDOWS),.F.)
		Return
	Endif
	* --- effettua l' amministrazione
	cp_ManageOpenDB(i_dcx)
	* --- riapre il database
	cp_OpenSchema('shared noupdate')
	cp_LoadServerArray()
	cp_LoadTableArray()
	cp_OpenServers()
	Return

Proc cp_ManageOpenDB(i_dcx)
	*o=Createobject('stdaManageDB',i_dcx)
	*o.Show()
    l_batch=  "cp_mngdatabase()"
    &l_batch
	Return

Define Class stdaCreateAzi As CPsysform
	WindowType=1
	Height=30
	Width=210
	Top=180
	Left=280
	Icon=i_cBmpPath+i_cStdIcon
	Caption='Create Company'
	oCaller=.Null.
	Add Object lbl As Label With Caption='Name:',Left=5,Top=8,Width=40,Align=1,Height=20, BackStyle=0
	Add Object aziname As TextBox With Left=45,Width=100,Top=5,Margin=0
	Add Object btncreate As CommandButton With Caption='Create',Left=150,Top=5,Width=50,Height=20
	Proc btncreate.Click()
		If !Empty(Trim(Thisform.aziname.Value))
			cp_CreateAzi(Trim(Thisform.aziname.Value))
			Thisform.oCaller.AziFill()
		Endif
		Return
	Procedure Init
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
	Endproc
Enddefine

Define Class stdaManageDB As CPsysform
	Top=10
	Left=10
	Width=590-120
	Height=235
	MinHeight=100
	BackColor=Rgb(192,192,192)
	Caption= ''
	WindowType=1
	oDCX=.Null.
	Icon=i_cBmpPath+i_cStdIcon
	bNewProcs=.F.
	Add Object TblGrd As dbTblGrd With Top=5,Left=5
	Add Object SrvGrd As dbSrvGrd With Top=5,Left=410,Visible=.F.
	Add Object btnUpd As CommandButton With FontBold=.F.,Top=210,Left=5,Height=20,Width=140,Caption=''
	Add Object btnCon As CommandButton With FontBold=.F.,Top=210,Left=150,Height=20,Width=140,Caption=''
	Add Object btnEdt As CommandButton With FontBold=.F.,Top=210,Left=295,Height=20,Width=55,Caption=''
	Add Object btnAll As CommandButton With FontBold=.F.,Top=210,Left=355,Height=20,Width=50,Caption=''
	Add Object btnUnsAll As CommandButton With FontBold=.F.,Top=210,Left=410,Height=20,Width=60,Caption=""
	Add Object btnSrv As CommandButton With FontBold=.F.,Top=70,Left=This.Width-55,Height=20,Width=50,Caption=""
	Add Object btnSAdd As CommandButton With FontBold=.F.,Top=100,Left=This.Width-55,Height=20,Width=50,Caption='',Visible=.F.
	Add Object btnSMod As CommandButton With FontBold=.F.,Top=130,Left=This.Width-55,Height=20,Width=50,Caption='',Visible=.F.
	Add Object btnSDel As CommandButton With FontBold=.F.,Top=160,Left=This.Width-55,Height=20,Width=50,Caption='',Visible=.F.
	Add Object btnOk As CommandButton With FontBold=.F.,Top=5,Left=This.Width-55,Height=20,Width=50,Caption='',Enabled=.F.
	Add Object btnCan As CommandButton With FontBold=.F.,Top=30,Left=This.Width-55,Height=20,Width=50,Caption=''
	Add Object btnazi As CommandButton With FontBold=.F.,Top=120,Left=This.Width-55,Height=20,Width=55,Caption='Crea. Azi',Visible=.F.
	Proc btnOk.Click()
		If Used('_cpfiles_')
			Select _cpfiles_
			Use
		Endif
		If Used('_cpsrv_')
			Select _cpsrv_
			Use
		Endif
		If Used('_azi_')
			Select _azi_
			Use
		Endif
		* ---
		*wait window "Deve sistemare gli array dei file e dei server"
		Thisform.Release()
	Proc btnCan.Click()
		Thisform.btnOk.Click()
	Proc btnazi.Click()
		Local azifrm
		azifrm=Createobject('stdaCreateAzi')
		azifrm.oCaller=Thisform
		azifrm.Show()
		Return
	Proc Init(oDCX)
		If Vartype(bexternaldatabaseadmin)<>'U'
			This.btnazi.Visible=.T.
		Endif
		This.oDCX=oDCX
		cp_msg(cp_Translate(MSG_UPDATING_REGISTER))
		This.UpdateReg()
		cp_msg(cp_Translate(MSG_LOADING_TABLES))
		This.TblGrd.Fill()
		cp_msg(cp_Translate(MSG_LOADING_SERVERS))
		This.SrvGrd.Fill()
		cp_msg(cp_Translate(MSG_LOADING_COMPANIES))
		This.AziFill()
		This.Caption=cp_Translate(MSG_INSTALL_TABLES_AND_DATABASE)
		This.btnUpd.Caption=cp_Translate(MSG_UPDATE_DATABASE)
		This.btnCon.Caption=cp_Translate(MSG_CHECK_LINKS)
		This.btnEdt.Caption=cp_Translate(MSG_CHANGE_BUTTON)
		This.btnAll.Caption=cp_Translate(MSG_SELECT_ALL)
		This.btnUnsAll.Caption=cp_Translate(MSG_UNSELECT_ALL)
		This.btnSrv.Caption=cp_Translate(MSG_SERVERS_BUTTON)
		This.btnSAdd.Caption=cp_Translate(MSG_NEW_BUTTON)
		This.btnSMod.Caption=cp_Translate(MSG_CHANGE_BUTTON)
		This.btnSDel.Caption=cp_Translate(MSG_DELETE_BUTTON)
		This.btnOk.Caption=cp_Translate(MSG_OK_BUTTON)
		This.btnCan.Caption=cp_Translate(MSG_CANCEL_BUTTON)
		* --- chiude i servers, verranno riaperti con la possibilita' di specificare password
		* --- con diritti di amministrazione
		Local i_tmp
		For i_tmp=2 To i_nServers
			If i_ServerConn[i_tmp,2]>0
				=SQLDisconn(i_ServerConn[i_tmp,2])
			Endif
		Next
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
		This.Resize()

	Proc Resize()
		This.LockScreen=.T.
		This.SrvGrd.Height=Max(This.Height-35,200)
		This.SrvGrd.Width=Max(This.Width - This.SrvGrd.Left -60,120)
		This.TblGrd.Height=Max(This.Height-35,200)
		This.TblGrd.Width=Max(This.Width-This.TblGrd.Left-Iif(This.SrvGrd.Visible ,This.SrvGrd.Width ,0)-60-5,400)
		This.btnUpd.Top=Max(This.Height-25,210)
		This.btnCon.Top=Max(This.Height-25,210)
		This.btnEdt.Top=Max(This.Height-25,210)
		This.btnAll.Top=Max(This.Height-25,210)
		This.btnUnsAll.Top=Max(This.Height-25,210)
		This.btnOk.Left=Max(This.Width,590-Iif(Not Thisform.SrvGrd.Visible,120,0))-55
		This.btnSrv.Left=Max(This.Width,590-Iif(Not Thisform.SrvGrd.Visible,120,0))-55
		This.btnCan.Left=Max(This.Width,590-Iif(Not Thisform.SrvGrd.Visible,120,0))-55
		This.btnazi.Left=Max(This.Width,590-Iif(Not Thisform.SrvGrd.Visible,120,0))-55
		This.btnSAdd.Left=Max(This.Width,590-Iif(Not Thisform.SrvGrd.Visible,120,0))-55
		This.btnSMod.Left=Max(This.Width,590-Iif(Not Thisform.SrvGrd.Visible,120,0))-55
		This.btnSDel.Left=Max(This.Width,590-Iif(Not Thisform.SrvGrd.Visible,120,0))-55
		This.btnSDel.Left=Max(This.Width,590-Iif(Not Thisform.SrvGrd.Visible,120,0))-55
		This.LockScreen=.F.
	Proc SetServer()
		Select _cpfiles_
		Replace Server With _cpsrv_.ServerName
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"update cpttbls set servername="+cp_ToStr(_cpsrv_.ServerName)+" where filename="+cp_ToStr(_cpfiles_.FILENAME))
		Else
			Update cpttbls Set ServerName=_cpsrv_.ServerName Where FILENAME=_cpfiles_.FILENAME
		Endif
	Proc UpdateReg()
		Local i,ph,i_phname
		bOk=.T.
		For i=1 To This.oDCX.nfi
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"select * from cpttbls where filename="+cp_ToStr(This.oDCX.Tbls[i,1]),"_tmp_")
			Else
				Select * From cpttbls Where FILENAME==This.oDCX.Tbls[i,1] Into Cursor _Tmp_
			Endif
			If !This.oDCX.IsTableAzi(This.oDCX.Tbls[i,1])
				bOk = bOk And cp_VerifyLinkTables(i,This.oDCX.Tbls[i,1],This.oDCX)
			Endif
			If bOk
				If Reccount()=0
					* --- inserisce il nuovo file nel registro
					*wait window "Inserisce nel registro di "+this.oDCX.Tbls[i,1]
					i_phname=Iif(This.oDCX.Tbls[i,19],This.oDCX.Tbls[i,1]+'_proto',This.oDCX.Tbls[i,2])
					If !This.oDCX.Tbls[i,19] && tabelle non temporanee
						If i_ServerConn[1,2]<>0
							=cp_SQL(i_ServerConn[1,2],"insert into cpttbls (filename,phname,flagyear,flaguser,flagcomp,datemod,servername) values ("+cp_ToStr(This.oDCX.Tbls[i,1])+","+cp_ToStr(i_phname)+","+Iif(This.oDCX.Tbls[i,3],"'T'","'N'")+","+Iif(This.oDCX.Tbls[i,4],"'T'","'N'")+","+Iif(This.oDCX.Tbls[i,5],"'T'","'N'")+",'','')")
						Else
							Insert Into cpttbls (FILENAME,PhName,FlagYear,FlagUser,FlagComp,DateMod,ServerName) Values (This.oDCX.Tbls[i,1],i_phname,Iif(This.oDCX.Tbls[i,3],"T","N"),Iif(This.oDCX.Tbls[i,4],"T","N"),Iif(This.oDCX.Tbls[i,5],"T","N"),'','')
						Endif
					Endif
				Else
					ph=This.oDCX.Tbls[i,2]
					* --- Zucchetti Aulla (La richiesta x ad hoc non ha mai senso)
					If ph<>Trim(_Tmp_.PhName) And 1=0
						If cp_YesNo(cp_Translate(MSG_PHYSICAL_NAME_OF_TABLE_CHANGED_FROM_DESIGN_PHASE_F)+Chr(13)+Chr(10)+;
								cp_MsgFormat(MSG_DESIGN_CL___C_DATABASE_CL___F,ph,Trim(_Tmp_.PhName))+Chr(13)+Chr(10)+;
								cp_Translate(MSG_KEEP_NEW_ONE_QM),32,.f.)
							ph=_Tmp_.PhName
						Endif
					Endif
					If i_ServerConn[1,2]<>0
						=cp_SQL(i_ServerConn[1,2],"update cpttbls set phname="+cp_ToStr(ph)+",flagyear="+Iif(This.oDCX.Tbls[i,3],"'T'","'N'")+",flaguser="+Iif(This.oDCX.Tbls[i,4],"'T'","'N'")+",flagcomp="+Iif(This.oDCX.Tbls[i,5],"'T'","'N'")+" where filename="+cp_ToStr(This.oDCX.Tbls[i,1]))
					Else
						Update cpttbls Set PhName=ph,FlagYear=Iif(This.oDCX.Tbls[i,3],"T","N"),FlagUser=Iif(This.oDCX.Tbls[i,4],"T","N"),FlagComp=Iif(This.oDCX.Tbls[i,5],"T","N") Where FILENAME==This.oDCX.Tbls[i,1]
					Endif
				Endif
			Endif
		Next
	Proc btnUpd.Click()
		Local i,N,nf,s,h,ok,cMsg,nConn,cDatabaseType,xServers[1,3],nServers,bReDo,i_Ref
		Local i_bFoundTable,i_acTableTransfer,i_nTransfTable,i_nCntTable,i_bFind,i_cList
		*
		If cp_dbtype="Interbase"
			cp_CloseSecondConn()
		Endif
		Private storedprocedures
		storedprocedures=''
		If i_ServerConn[1,2]=0 And Not(This.Parent.bNewProcs) And Not(i_compiledprog)
			h=Fopen('cp_dbprc.prg')
			storedprocedures=Fread(h,1000000)
			Fclose(h)
		Endif
		This.Parent.bNewProcs=.F.
		cp_sp_updatestdproc()
		*
		Public msgwnd
		msgwnd=Createobject('mkdbmsg')
		msgwnd.Show()
		nServers=0
		i_bFoundTable=.F.
		* trasferisci le tabelle da non aggiornare nell'array di quelle trasferite
		i_nTransfTable=0
		Dimension i_acTableTransfer[1]
		Select _cpfiles_
		Scan For _cpfiles_.upd=.F.
			* Inserisci la tabella nell'array delle tabelle trasferite
			i_nTransfTable=i_nTransfTable+1
			Dimension i_acTableTransfer[i_nTransfTable]
			i_acTableTransfer[i_nTransfTable]=Trim(_cpfiles_.FILENAME) &&+IIF(_cpfiles_.status='Ext','/Ext','')
		Endscan
		bReDo=.F.
		i_nCntTable=i_nTransfTable
		i_bFind=.T.
		Do While i_bFind .And. i_nCntTable<=i_dcx.GetTablesCount()
			i_bFind=.F.
			Dimension i_Ref[1,4]
			Scan For _cpfiles_.upd=.T.
				N=Trim(_cpfiles_.FILENAME)
				s=Trim(_cpfiles_.Server)
				* ---
				* verifica se la tabella corrente ('n') � da trasferire
				i_bFoundTable=cp_VerifyTransferTable(N,@i_acTableTransfer,i_nTransfTable)
				If !i_bFoundTable
					a=Alen(i_Ref,1)
					i_Ref[a,1]=N
					i_Ref[a,2]=Trim(_cpfiles_.PhName)
					i_Ref[a,3]=-1
					Dimension i_Ref[a+1,4]
				Endif
				* se la tabella corrente � da trasferire
				If i_bFoundTable
					i_bFind=.T.
					i_nCntTable=i_nCntTable+1   && incrementa il contatore di tabelle tasferite
					* Inserisci la tabella nell'array delle tabelle trasferite
					i_nTransfTable=i_nTransfTable+1
					Dimension i_acTableTransfer[i_nTransfTable]
					i_acTableTransfer[i_nTransfTable]=Trim(_cpfiles_.FILENAME)
					ok=db_CreateTableStruct(N,Trim(_cpfiles_.PhName),s,nConn,Thisform.oDCX,@cMsg,'all',@xServers,@nServers)
					db_UpdateSysTable(ok,cMsg,@bReDo,msgwnd)
				Endif
			Endscan
		Enddo
		* se le tabelle contengono dei link incrociati
		If i_nCntTable<i_dcx.GetTablesCount()
			msgwnd.add_Err(Chr(13)+Chr(10)+cp_Translate(MSG_WARN_LINK_DOUBLECROSSED_QM))
			msgwnd.add_Err(cp_Translate(MSG_TABLES_INVOLVED_WITH_DOUBLECROSSED_LINKS_CL))
			i_cList=''
			For i=1 To Alen(i_Ref,1)-1
				i_cList=i_cList+Iif(Not Empty(i_cList),',','')+i_Ref[i,1]
			Next
			msgwnd.add_Err(i_cList+Chr(13)+Chr(10)+Chr(13)+Chr(10))
			For i=1 To Alen(i_Ref,1)-1
				i_Ref[i,3]=db_CreateTableStruct(i_Ref[i,1],i_Ref[i,2],s,nConn,Thisform.oDCX,@cMsg,'table',@xServers,@nServers)
				i_Ref[i,4]=cMsg
			Next
			For i=1 To Alen(i_Ref,1)-1
				If i_Ref[i,3]=0
					i_Ref[i,3]=db_CreateTableStruct(i_Ref[i,1],i_Ref[i,2],s,nConn,Thisform.oDCX,@cMsg,'index',@xServers,@nServers)
					i_Ref[i,4]=cMsg
				Endif
			Next
			Select _cpfiles_
			Set Order To 0 && IMPORTANTE: la routine db_UpdateSysTable cambia il file _cpfiles_, se non si toglie l' ordine salta dei record!
			Scan Nooptimize For _cpfiles_.upd=.T.
				N=Trim(_cpfiles_.FILENAME)
				For i=1 To Alen(i_Ref,1)-1
					If i_Ref[i,1]==N
						db_UpdateSysTable(i_Ref[i,3],i_Ref[i,4],@bReDo)
					Endif
				Next
			Endscan
			Set Order To 1
		Endif
		If i_ServerConn[1,2]=0
			cp_PackSystemTables()
		Endif
		cp_CloseAllServerForRebuild(@xServers,nServers)
		cp_UpdateRoles(i_ServerConn[1,2],Thisform.oDCX)
		Thisform.UpdatePlanCheckDate(i_ServerConn[1,2])
		* -- Aggiorno lista lingue per traduzione data in persist
		Thisform.UpdateListLang(i_ServerConn[1,2])
		Thisform.btnOk.Enabled=.T.
		msgwnd.End(cp_Translate(MSG_DATABASE_REBUILT))
		If Not Empty(msgwnd.Edt_err.Text)
			cp_errormsg(MSG_DATABASE_REBUILT_ERROR,'!')
		Endif
		* --- aggiorna le stored procedures VFP
		If i_ServerConn[1,2]=0 And Not(i_compiledprog)
			h=Fcreate('cp_dbprc.prg')
			Fwrite(h,storedprocedures)
			Fclose(h)
			Compile cp_dbprc
			Set Proc To ("cp_dbprc") Additive && non deve essere inclusa automaticamente nell' eseguibile
		Endif
		* --- segnala se bisogna rilanciare la procedura
		If cp_dbtype="Interbase"
			cp_OpenSecondConn(pConn)
		Endif
		If bReDo
			cp_ErrorMsg(MSG_SOME_TABLES_NOT_UPDATED_C_RUN_DATABASE_UPDATING_AGAIN)
		Endif
	Proc btnCon.Click()
		Local i,N,nf,s,ok,nConn,cDatabaseType,xServers[1,3],nServers
		Public msgwnd
		msgwnd=Createobject('mkdbmsg')
		msgwnd.Show()
		nServers=0
		Select _cpfiles_
		Scan For _cpfiles_.upd=.F. And _cpfiles_.Status='Conn'
			N=Trim(_cpfiles_.FILENAME)
			s=Trim(_cpfiles_.Server)
			* ---
			cDatabaseType=''
			nConn=cp_OpenServerForRebuild(s,@cDatabaseType,@xServers,@nServers)
			If nConn<0
				ok=.F.
			Else
				nf=Iif(Empty(_cpfiles_.PhName),N,Trim(_cpfiles_.PhName)) && crea con nome fisico
				ok=cp_CheckTable(N,nf,nConn,Thisform.oDCX,cDatabaseType)
			Endif
			* ---
			If ok
				* --- segna la tabella come aggiornata
				Select _cpfiles_
				Replace upd With .F.
				Replace Status With 'Ok'
				Replace msg With cp_Translate(MSG_OK)
				Replace Color With Rgb(0,192,0)
			Else
				* --- segna la tabella come non aggiornata
				Select _cpfiles_
				Replace upd With Iif(nConn<0,.F.,.T.)
				Replace Status With Iif(nConn<0,'Err','Ver')
				Replace msg With Iif(nConn<0,cp_Translate(MSG_CONNECTION_ERROR),cp_Translate(MSG_CREATE_OR_UPDATE))
				Replace Color With Iif(nConn<0,Rgb(255,0,0),Rgb(0,192,0))
			Endif
		Endscan
		=cp_CloseAllServerForRebuild(@xServers,nServers)
		msgwnd.End(cp_Translate(MSG_LINK_CHECKING_TERMINATED))
	Proc btnSrv.Click()
		If Thisform.SrvGrd.Visible
			Thisform.SrvGrd.Visible=.F.
			Thisform.btnSAdd.Visible=.F.
			Thisform.btnSMod.Visible=.F.
			Thisform.btnSDel.Visible=.F.
			Thisform.Width=Thisform.Width-120
		Else
			Thisform.SrvGrd.Visible=.T.
			Thisform.btnSAdd.Visible=.T.
			Thisform.btnSMod.Visible=.T.
			Thisform.btnSDel.Visible=.T.
			Thisform.Width=Thisform.Width+120
		Endif
	Proc btnEdt.Click()
		Local o
		o=Createobject('stdaManageTB')
		o.GetValues()
		o.Show()
	Proc btnAll.Click()
		Select _cpfiles_
		Replace All upd With .T. For Status<>'Ext' Or (Status='Ext' And msg=cp_Translate(MSG_TEMPORARY_TABLE))
		This.Parent.bNewProcs=.T.
	Proc btnUnsAll.Click()
		Select _cpfiles_
		Replace All upd With .F. For Status<>'Ext' Or (Status='Ext' And msg=cp_Translate(MSG_TEMPORARY_TABLE))
		This.Parent.bNewProcs=.F.
	Proc btnSAdd.Click()
		Local o
		o=Createobject('stdaManageSRV')
		o.InitValues()
		o.Show()
		Thisform.SrvGrd.Refresh()
	Proc btnSMod.Click()
		Local o
		o=Createobject('stdaManageSRV')
		o.GetValues()
		o.Show()
	Proc btnSDel.Click()
		Local sn,i_cnt
		sn=_cpsrv_.ServerName
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select count(*) as cnt from cpttbls where servername="+cp_ToStr(sn),"_tmp_")
		Else
			Select Count(*) As Cnt From cpttbls Where ServerName=sn Into Cursor _Tmp_
		Endif
		If Type("_tmp_.cnt")='C'
			i_cnt=Val(_Tmp_.Cnt)
		Else
			i_cnt=_Tmp_.Cnt
		Endif
		If i_cnt=0
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"delete from cptsrvr where servername="+cp_ToStr(sn))
			Else
				Delete From cptsrvr Where ServerName=sn
			Endif
			Delete From _cpsrv_ Where ServerName=sn
			Thisform.SrvGrd.Refresh()
		Else
			cp_msg(cp_Translate(MSG_TABLES_ON_SERVER_C_YOU_CANNOT_DELETE_IT),.F.)
		Endif
	Proc UpdatePlanCheckDate(nConn)
		Local i_p,i_newpresist
		If nConn<>0
			i_r=cp_SQL(nConn,"select * from persist where code='PlanCheckDate'","__tmp__")
			If i_r<>-1
				If  Reccount()=0
					=cp_SQL(nConn,"insert into persist (code,persist) values ('PlanCheckDate',"+cp_ToStr(This.oDCX.cPlanDate)+")")
				Else
					i_newpersist=cp_UpdatePersistPlanDate(This.oDCX.cPlanDate,persist)
					*WAIT WINDOW 'nuovo='+i_newpersist+' '+' vecchio='+persist
					=cp_SQL(nConn,"update persist set persist="+cp_ToStr(i_newpersist)+" where code='PlanCheckDate'")
				Endif
				Use
			Endif
		Else
			Select * From persist Where Code='PlanCheckDate' Into Cursor __tmp__
			If Reccount()=0
				Insert Into persist (Code,persist) Values ('PlanCheckDate',This.oDCX.cPlanDate)
			Else
				i_p=This.oDCX.cPlanDate
				Update persist Set persist=i_p Where Code='PlanCheckDate'
			Endif
			Use
		Endif
	Endproc
	Proc UpdateListLang(nConn)
		*** Aggiornamento nella tabella persist delle lingue soggette a traduzione dato
		Local i_r, i_ListLang
		*costruisco lista lingue traducibili
		i_ListLang=cp_CalcListLang()
		If nConn<>0
			i_r=cp_SQL(nConn,"select * from persist where code='ListLanguage'","__tmp__")
			If i_r<>-1
				If  Reccount()=0
					=cp_SQL(nConn,"insert into persist (code,persist) values ('ListLanguage',"+cp_ToStr(i_ListLang)+")")
				Else
					=cp_SQL(nConn,"update persist set persist="+cp_ToStr(i_ListLang)+" where code='ListLanguage'")
				Endif
				Use
			Endif
		Else
			Select * From persist Where Code='ListLanguage' Into Cursor __tmp__
			If Reccount()=0
				Insert Into persist (Code,persist) Values ('ListLanguage',i_ListLang)
			Else
				Update persist Set persist=i_ListLang Where Code='ListLanguage'
			Endif
			Use
		Endif
	Endproc
	Proc AziFill()
		If i_ServerConn[1,2]<>0
			cp_sqlexec(i_ServerConn[1,2],"select * from cpazi","_azi_")
		Else
			Select * From cpazi Into Cursor _azi_ nofilter
		Endif
Enddefine

Function cp_UpdatePersistPlanDate(i_cNew,i_cOld)
	Local i_p,i_m
	i_p=At(',',i_cOld)
	Do While i_p<>0
		* --- isola il modulo
		i_cOld=Substr(i_cOld,i_p)
		i_p=At('=',i_cOld)
		i_m=Left(i_cOld,i_p)
		* --- controlla se c'e' nella nuova definizione
		If Atc(i_m,i_cNew)=0
			* --- non c'e': lo aggiunge
			i_cNew=i_cNew+i_m+Substr(i_cOld,i_p+1,14)
		Endif
		i_cOld=Substr(i_cOld,i_p+14)
		i_p=At(',',i_cOld)
	Enddo
	Return(i_cNew)

Procedure cp_UpdateRoles(nConn,oDCX)
	Local i_cRoles,i_p
	cp_InsertAdminsRole(nConn)
	i_cRoles=oDCX.GetRoles()
	i_p=At(',',i_cRoles)
	Do While i_p<>0
		cp_UpdateARole(nConn,Left(i_cRoles,i_p-1))
		i_cRoles=Substr(i_cRoles,i_p+1)
		i_p=At(',',i_cRoles)
	Enddo
	cp_UpdateARole(nConn,i_cRoles)
	Return

Procedure cp_UpdateARole(nConn,cRole)
	Local i_oldsel,i_r
	If !Empty(cRole)
		i_oldsel=Select()
		If Used('__tmproles__')
			Use In __tmproles__
		Endif
		If nConn<>0
			cp_SQL(nConn,"select * from cpgroups where name='"+cRole+"'","__tmproles__")
		Else
			Select * From cpgroups Where Name==cRole Into Cursor __tmproles__
		Endif
		If !Eof()
			* --- aggiorna
			If __tmproles__.grptype<>'R'
				If nConn<>0
					cp_SQL(nConn,"UPDATE cpgroups SET grptype='R' WHERE name='"+cRole+"'")
				Else
					Update cpgroups Set grptype='R' Where Name==cRole
				Endif
			Endif
		Else
			* --- aggiunge
			Use In __tmproles__
			If nConn<>0
				cp_SQL(nConn,"SELECT MAX(code)+1 as newcode FROM cpgroups","__tmproles__")
				cp_SQL(nConn,"INSERT INTO cpgroups (code,name,grptype) VALUES ("+Alltrim(Str((Nvl(__tmproles__.newcode,1)),10,0))+",'"+cRole+"','R')")
			Else
				Select Max(Code)+1 As newcode From cpgroups Into Cursor __tmproles__
				Insert Into cpgroups (Code,Name,grptype) Values (Nvl(__tmproles__.newcode,1),cRole,'R')
			Endif
		Endif
		Use
		Select (i_oldsel)
	Endif
	Return

Proc cp_InsertAdminsRole(nConn)
	Local i_oldsel,i_olderr
	i_oldsel=Select()
	If Used('__tmproles__')
		Use In __tmproles__
	Endif
	If nConn<>0
		cp_SQL(nConn,"SELECT * FROM cpgroups","__tmp__")
		If Type("__tmp__.grptype")<>'C'
			i_cDatabaseType=cp_getDatabaseType(nConn)
			cp_msg(cp_MsgFormat(MSG_UPDATING__,'cpgroups'))
			cp_SQL(nConn,"alter table cpgroups add grptype "+db_FieldType(i_cDatabaseType,'C',1,0),.F.,.F.,.T.)
		Endif
	Else
		Select * From cpgroups Into Cursor __tmproles__
		i_olderr=On('ERROR')
		On Error =.T.
		If Used('cpgroups')
			Select cpgroups
			Use
		Endif
		If Type("__tmp__.grptype")<>'C'
			cp_msg(cp_MsgFormat(MSG_UPDATING__,cpgroups))
			Alter Table cpgroups Add grptype C(1)
		Endif
	Endif
	If nConn<>0
		cp_SQL(nConn,"SELECT MAX(code) as maxcode FROM cpgroups","__tmproles__")
	Else
		Select Max(Code) As maxcode From cpgroups Into Cursor __tmproles__
	Endif
	If Used('__tmproles__')
		If Nvl(__tmproles__.maxcode,0)=0
			If nConn<>0
				cp_SQL(nConn,"INSERT INTO cpgroups (code,name,grptype) VALUES (1,'Admins','R')")
			Else
				Insert Into cpgroups (Code,Name,grptype) Values (1,'Admins','R')
			Endif
		Endif
		Use In __tmproles__
	Endif
	Select (i_oldsel)
	Return

Proc db_CreateTableStruct(cTable,cPhName,cServer,nConn,oDCX,cMsg,cType,xServers,nServers)
	Local ok,nf,i_cOldAzi
	cDatabaseType=''
	nConn=cp_OpenServerForRebuild(cServer,@cDatabaseType,@xServers,@nServers)
	If oDCX.IsTableAzi(cTable) && la tabella e' multiaziendale
		* --- cicla su tutte le aziende
		i_cOldAzi=i_codazi
		Select _azi_
		Scan
			cMsg=''
			ok=0
			i_codazi=_azi_.codazi
			nf=Trim(Strtran(Iif(Empty(cPhName),cTable,cPhName),'xxx',Trim(_azi_.codazi))) && crea con nome fisico
			Do Case
				Case cType=='all'
					cp_RebuildOneTable(cTable,nf,nConn,cDatabaseType,oDCX,@cMsg,@ok,Trim(_azi_.codazi))
				Case cType=='index'
					ok=cp_MakeForeingKey(cTable,nf,nConn,oDCX,cDatabaseType,Trim(_azi_.codazi))
					If ok=2
						cMsg=cp_Translate(MSG_RELATIONSHIPS)
					Endif
				Case cType=='table'
					If nConn<0
						ok=2
						cMsg=cp_Translate(MSG_CONNECTING_TO_SERVER)
					Else
						If !cp_MakeTable(cTable,nf,nConn,oDCX,cDatabaseType,Trim(_azi_.codazi))
							cMsg=cp_Translate(MSG_REBUILDING_TABLE)
							ok=2
						Else
							If !cp_MakeIndexes(cTable,nf,nConn,oDCX,cDatabaseType,Trim(_azi_.codazi))
								cMsg=cp_Translate(MSG_INDEXES)
								ok=2
							Else
								ok=0
							Endif
						Endif
					Endif
			Endcase
		Endscan
		i_codazi=i_cOldAzi
	Endif
	i_cOldAzi=i_codazi
	i_codazi='xxx'
	cMsg=''  && ora l' azienda XXX
	ok=0
	nf=Trim(Iif(Empty(cPhName),cTable,cPhName)) && crea con nome fisico
	Do Case
		Case cType=='all'
			cp_RebuildOneTable(cTable,nf,nConn,cDatabaseType,oDCX,@cMsg,@ok,'xxx')
		Case cType=='index'
			ok=cp_MakeForeingKey(cTable,nf,nConn,oDCX,cDatabaseType,'xxx')
			If ok=2
				cMsg=cp_Translate(MSG_RELATIONSHIPS)
			Endif
		Case cType=='table'
			If nConn<0
				ok=2
				cMsg=cp_Translate(MSG_CONNECTING_TO_SERVER)
			Else
				If !cp_MakeTable(cTable,nf,nConn,oDCX,cDatabaseType,'xxx')
					cMsg=cp_Translate(MSG_REBUILDING_TABLE)
					ok=2
				Else
					If !cp_MakeIndexes(cTable,nf,nConn,oDCX,cDatabaseType,'xxx')
						cMsg=cp_Translate(MSG_INDEXES)
						ok=2
					Else
						ok=0
					Endif
				Endif
			Endif
	Endcase
	i_codazi=i_cOldAzi
	Return(ok)
Endproc

Proc db_CreateAziStruct(cTable,cPhName,cAziName,nConn,cDatabaseType,oDCX,cMsg,cType)
	Local ok,nf,i
	If oDCX.IsTableAzi(cTable)
		* --- cicla su tutte le aziende
		cMsg=''
		ok=0
		nf=Trim(Strtran(Iif(Empty(cPhName),cTable,cPhName),'xxx',cPhName)) && crea con nome fisico
		Do Case
			Case cType=='all'
				cp_RebuildOneTable(cTable,nf,nConn,cDatabaseType,oDCX,@cMsg,@ok,cAziName)
			Case cType=='index'
				ok=cp_MakeForeingKey(cTable,nf,nConn,oDCX,cDatabaseType,cAziName)
				If ok=2
					cMsg=cp_Translate(MSG_RELATIONSHIPS)
				Endif
			Case cType=='table'
				If nConn<0
					ok=2
					cMsg=cp_Translate(MSG_CONNECTING_TO_SERVER)
				Else
					If !cp_MakeTable(cTable,nf,nConn,oDCX,cDatabaseType,cAziName)
						cMsg=cp_Translate(MSG_REBUILDING_TABLE)
						ok=2
					Else
						If !cp_MakeIndexes(cTable,nf,nConn,oDCX,cDatabaseType,cAziName)
							cMsg=cp_Translate(MSG_INDEXES)
							ok=2
						Else
							ok=0
						Endif
					Endif
				Endif
		Endcase
	Endif
	Return(ok)
Endproc

Proc db_UpdateSysTable(ok,cMsg,bReDo,msgwnd)
	If ok=0
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"update cpttbls set datemod="+cp_ToStr(_cpfiles_.xdcts)+" where filename="+cp_ToStr(Trim(_cpfiles_.FILENAME)))
		Else
			Update cpttbls Set DateMod=_cpfiles_.xdcts Where FILENAME==Trim(_cpfiles_.FILENAME)
		Endif
		* --- segna la tabella come aggiornata
		Select _cpfiles_
		If !Empty(_cpfiles_.xdcts)
			Replace upd With .F.
			Replace Status With 'Ok'
			Replace msg With cp_Translate(MSG_OK)
			Replace Color With Rgb(0,192,0)
		Else
			If Vartype(msgwnd)='O'
				If _cpfiles_.Status<>'Ext'
					msgwnd.add_Err(Trim(_cpfiles_.FILENAME)+cp_Translate(MSG_TABLE_WITHOUT_TIMESTAMP))
				Else
					* --- le tabelle temporanee vanno marcate come aggiornate
					Select _cpfiles_
					Replace upd With .F.
				Endif
			Endif
		Endif
	Else
		If ok=2
			* --- segna la tabella con errore
			Select _cpfiles_
			Replace upd With .F.
			Replace Status With 'Err'
			Replace msg With cp_MsgFormat(MSG_ERROR_IN_TABLE__,cMsg)
			Replace Color With Rgb(255,0,0)
		Else
			If ok=1
				bReDo=.T.
			Endif
		Endif
	Endif
	Return
Endproc

Proc cp_RebuildOneTable(cTable,cPhTable,nConn,cDatabaseType,oDCX,cMsg,ok,cAzi)
	cMsg=''
	ok=0
	If nConn<0
		ok=2
		cMsg=cp_Translate(MSG_CONNECTING_TO_SERVER)
	Else
		If !cp_MakeTable(cTable,cPhTable,nConn,oDCX,cDatabaseType,cAzi)
			cMsg=cp_Translate(MSG_REBUILDING_TABLE)
			ok=2
		Else
			If !cp_MakeIndexes(cTable,cPhTable,nConn,oDCX,cDatabaseType,cAzi)
				cMsg=cp_Translate(MSG_INDEXES)
				ok=2
			Else
				ok=cp_MakeForeingKey(cTable,cPhTable,nConn,oDCX,cDatabaseType,cAzi)
				If ok=2
					cMsg=cp_Translate(MSG_RELATIONSHIPS)
				Endif
			Endif
		Endif
	Endif
	Return

Define Class dbTblGrd As Grid
	ColumnCount=0
	FontBold=.F.
	DeleteMark=.F.
	Width=400
	Height=200
	cOldIcon=''
	Proc Fill()
		Local i,i_NS
		Create Cursor _cpfiles_ (FILENAME C(30),PhName C(50),upd L,Status C(4),msg C(50),Color i,Server C(10),xdcts C(14))
		* --- In testa le tabella da aggiornare..
		Index On Iif(upd,'A','B')+Iif(Status='Ver','A','B')+FILENAME Tag tag1
		For i=1 To This.Parent.oDCX.nfi
			If i_ServerConn[1,2]<>0
				=cp_SQL(i_ServerConn[1,2],"select * from cpttbls where filename="+cp_ToStr(This.Parent.oDCX.Tbls[i,1]),"_tmp_")
			Else
				Select * From cpttbls Where FILENAME==This.Parent.oDCX.Tbls[i,1] Into Cursor _Tmp_
			Endif
			Do Case
					* --- non e' nel registro e non e' una tabella temporanea
				Case Reccount()=0 And !This.Parent.oDCX.Tbls[i,19]
					Insert Into _cpfiles_ (FILENAME,PhName,upd,Status,msg,Color,Server) Values (This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,2],.F.,'Err',cp_Translate(MSG_ERROR_IN_FILE_REGISTER),Rgb(255,0,0),Nvl(_Tmp_.ServerName,''))
					* --- tabella esterna
				Case This.Parent.oDCX.Tbls[i,15]
					Insert Into _cpfiles_ (FILENAME,PhName,upd,Status,msg,Color,Server) Values (This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,2],.F.,'Ext',cp_Translate(MSG_EXTERNAL_TABLE),Rgb(255,128,64),Nvl(_Tmp_.ServerName,''))
					* --- tabella temporanea
				Case This.Parent.oDCX.Tbls[i,19]
					* --- insert into _cpfiles_ (filename,phname,upd,status,msg,color,server) values (this.parent.oDCX.Tbls[i,1],this.parent.oDCX.Tbls[i,2],.f.,'Ext',cp_Translate(MSG_TEMPORARY_TABLE),rgb(255,128,64),nvl(_tmp_.ServerName,''))
					i_NS=cp_GetServerIdx(_Tmp_.ServerName)
					If i_NS<>0 And i_ServerConn[i_NS,2]<>-1
						If cp_CheckTable(This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,1]+'_proto',i_ServerConn[i_NS,2],This.Parent.oDCX,i_ServerConn[i_NS,6])
							Insert Into _cpfiles_ (FILENAME,PhName,upd,Status,msg,Color,Server) Values (This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,1]+'_proto',.F.,'Ext',cp_Translate(MSG_TEMPORARY_TABLE),Rgb(255,128,64),Nvl(_Tmp_.ServerName,''))
						Else
							Insert Into _cpfiles_ (FILENAME,PhName,upd,Status,msg,Color,Server) Values (This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,1]+'_proto',.T.,'Ext',cp_Translate(MSG_TEMPORARY_TABLE),Rgb(255,128,64),Nvl(_Tmp_.ServerName,''))
						Endif
					Else
						Insert Into _cpfiles_ (FILENAME,PhName,upd,Status,msg,Color,Server) Values (This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,1]+'_proto',.F.,'Conn',cp_Translate(MSG_SERVER_NOT_FOUND),Rgb(255,255,0),Nvl(_Tmp_.ServerName,''))
					Endif
					* --- da aggiornare (Zucchetti Aulla > al posto di diverso)
				Case This.Parent.oDCX.Tbls[i,13]>_Tmp_.DateMod Or Empty(This.Parent.oDCX.Tbls[i,13])
					Insert Into _cpfiles_ (FILENAME,PhName,upd,Status,msg,Color,Server,xdcts) Values (This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,2],.T.,'Ver',cp_Translate(MSG_UPDATE),Rgb(255,255,0),Nvl(_Tmp_.ServerName,''),This.Parent.oDCX.Tbls[i,13])
				Otherwise
					i_NS=cp_GetServerIdx(_Tmp_.ServerName)
					If i_NS<>0 And i_ServerConn[i_NS,2]<>-1
						If cp_CheckTable(This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,2],i_ServerConn[i_NS,2],This.Parent.oDCX,i_ServerConn[i_NS,6])
							* --- Ok
							Insert Into _cpfiles_ (FILENAME,PhName,upd,Status,msg,Color,Server,xdcts) Values (This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,2],.F.,'Ok',cp_Translate(MSG_OK),Rgb(0,192,0),Nvl(_Tmp_.ServerName,''),This.Parent.oDCX.Tbls[i,13])
						Else
							* --- da aggiornare
							Insert Into _cpfiles_ (FILENAME,PhName,upd,Status,msg,Color,Server,xdcts) Values (This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,2],.T.,'Ver',cp_Translate(MSG_CREATE_OR_UPDATE),Rgb(255,255,0),Nvl(_Tmp_.ServerName,''),This.Parent.oDCX.Tbls[i,13])
						Endif
					Else
						* --- server irraggiungibile
						Insert Into _cpfiles_ (FILENAME,PhName,upd,Status,msg,Color,Server) Values (This.Parent.oDCX.Tbls[i,1],This.Parent.oDCX.Tbls[i,2],.F.,'Conn',cp_Translate(MSG_SERVER_NOT_FOUND),Rgb(255,255,0),Nvl(_Tmp_.ServerName,''))
					Endif
			Endcase
		Next
		If Used('_tmp_')
			Select _Tmp_
			Use
		Endif
		Select _cpfiles_
		Go Top
		This.RecordSource='_cpfiles_'
		* ---
		This.AddObject('column1','column')
		This.Column1.ControlSource='filename'
		This.Column1.FontBold=.F.
		This.Column1.Header1.Caption=cp_Translate(MSG_TABLE)
		This.Column1.Header1.FontBold=.F.
		This.Column1.Visible=.T.
		This.Column1.ReadOnly=.T.
		This.Column1.RemoveObject('Text1')
		This.Column1.AddObject('Text1','dbTblTxt')
		This.Column1.Text1.Visible=.T.
		* ---
		This.AddObject('column2','column')
		This.Column2.Width=14
		This.Column2.ControlSource='upd'
		This.Column2.Sparse=.F.
		This.Column2.AddObject('chk','dbTblChk')
		This.Column2.CurrentControl='chk'
		This.Column2.chk.Caption=''
		This.Column2.chk.Visible=.T.
		This.Column2.FontBold=.F.
		This.Column2.Header1.FontBold=.F.
		This.Column2.Header1.Caption=cp_Translate(MSG_UPDATE)
		This.Column2.Visible=.T.
		* ---
		This.AddObject('column3','column')
		This.Column3.Width=200
		This.Column3.ControlSource='msg'
		This.Column3.FontBold=.F.
		This.Column3.DynamicBackColor="_cpfiles_.color"
		This.Column3.Header1.FontBold=.F.
		This.Column3.Header1.Caption=cp_Translate(MSG_STATE)
		This.Column3.Visible=.T.
		This.Column3.ReadOnly=.T.
		This.Column3.RemoveObject('Text1')
		This.Column3.AddObject('Text1','dbTblTxt')
		This.Column3.Text1.Visible=.T.
		* ---
		This.AddObject('column4','column')
		This.Column4.Width=100
		This.Column4.ControlSource='server'
		This.Column4.FontBold=.F.
		This.Column4.Header1.FontBold=.F.
		This.Column4.Header1.Caption=cp_Translate(MSG_SERVER)
		This.Column4.Visible=.T.
		This.Column4.ReadOnly=.T.
		This.Column4.RemoveObject('Text1')
		This.Column4.AddObject('Text1','dbTblTxt')
		This.Column4.Text1.Visible=.T.
	Proc DragDrop(oSource,nXCoord,nYCoord,nState)
		If oSource.Name=='SERVERNAME'
			This.Parent.SetServer()
		Endif
	Proc DragOver(oSource,nXCoord,nYCoord,nState)
		Do Case
			Case nState=0   && Enter
				This.cOldIcon=oSource.DragIcon
				If oSource.Name=='SERVERNAME'
					oSource.DragIcon=i_cBmpPath+"cross01.cur"
				Endif
			Case nState=1   && Leave
				oSource.DragIcon=This.cOldIcon
		Endcase
Enddefine

Define Class dbTblChk As Checkbox
	Proc Click()
		If _cpfiles_.Status='Ext' And _cpfiles_.msg<>cp_Translate(MSG_TEMPORARY_TABLE)
			This.Value=.F.
		Endif
Enddefine

Define Class dbTblTxt As TextBox
	Proc DblClick()
		Thisform.btnEdt.Click()
Enddefine

Define Class dbSrvGrd As Grid
	ColumnCount=0
	FontBold=.F.
	DeleteMark=.F.
	Width=120
	Height=200
	Proc Fill()
		Create Cursor _cpsrv_ (ServerName C(10))
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select * from cptsrvr","_tmp_")
		Else
			Select * From cptsrvr Where Not(Deleted()) Into Cursor _Tmp_
		Endif
		If Reccount()>0
			This.Visible=.T.
			This.Parent.Width=590
			This.Parent.btnOk.Left=590-55
			This.Parent.btnCan.Left=590-55
			This.Parent.btnazi.Left=590-55
			This.Parent.btnSrv.Visible=.F.
			This.Parent.btnSAdd.Visible=.T.
			This.Parent.btnSMod.Visible=.T.
			This.Parent.btnSDel.Visible=.T.
		Endif
		Scan
			Insert Into _cpsrv_ (ServerName) Value (_Tmp_.ServerName)
		Endscan
		Select _cpsrv_
		Go Top
		This.RecordSource='_cpsrv_'
		* ---
		This.AddObject('column1','column')
		This.Column1.Width=120
		This.Column1.ControlSource='servername'
		This.Column1.FontBold=.F.
		This.Column1.Header1.Caption=cp_Translate(MSG_SERVERS)
		This.Column1.Header1.FontBold=.F.
		This.Column1.Visible=.T.
		This.Column1.RemoveObject('text1')
		This.Column1.AddObject('ServerName','dbSrvTxt')
		This.Column1.ServerName.Visible=.T.
		This.Column1.ReadOnly=.T.
Enddefine

Define Class dbSrvTxt As TextBox
	nSeconds=0
	Proc DblClick()
		Thisform.btnSMod.Click()
	Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
		If nButton=1
			Nodefault
		Endif
	Proc MouseUp(nButton, nShift, nXCoord, nYCoord)
		Local ot
		If nButton=1
			If Seconds()-0.3<This.nSeconds
				This.DblClick()
			Endif
			This.nSeconds=Seconds()
		Endif
	Proc MouseMove(nButton, nShift, nXCoord, nYCoord)
		Local kf,errsav,oldarea
		Private err
		If nButton=1
			Nodefault
			This.DragIcon=i_cBmpPath+"DragMove.cur"
			This.Drag(1)
		Endif
Enddefine

Func cp_DateTime()
	Local s,T
	T=Time()
	s=Dtos(Date())+Left(T,2)+Substr(T,4,2)+Right(T,2)
	Return(s)

Func cp_OpenServerForRebuild(cServer,cDatabaseType,xServers,nServers)
	* --- Questa funzione apre una connessione con un server per la ricostruzione/verifica
	* --- delle tabelle. E' tenuta separata perche' qui si potrebbe usare una connessione
	* --- con diritti superiori al normale nel database di arrivo.
	Local nConn,r,i,j
	nConn=-1
	If Not(Empty(cServer))
		* --- cerca nella lista locale
		i=0
		For j=1 To nServers
			If xServers[j,1]=cServer
				i=j
			Endif
		Next
		If i=0
			If i_ServerConn[1,2]<>0
				r=cp_SQL(i_ServerConn[1,2],"select * from cptsrvr where servername="+cp_ToStr(cServer),"_Tmp_")
			Else
				r=1
				Select * From cptsrvr Where ServerName=cServer Into Cursor _Tmp_
			Endif
			If r<>-1
				If Reccount()<>0
					If ("DRIVER"$Upper(Trim(_Tmp_.ODBCDataSource)) Or "DSN"$Upper(Trim(_Tmp_.ODBCDataSource))) And ";"$Trim(_Tmp_.ODBCDataSource)
						nConn=Sqlstringconnect(Trim(_Tmp_.ODBCDataSource))
					Else
						nConn=SQLConnect(Trim(_Tmp_.ODBCDataSource))
					Endif
					cp_PostOpenConn(nConn)
					cDatabaseType=Trim(_Tmp_.DatabaseType)
					If _Tmp_.PostIt<>'N'
						=cp_CheckPostItTables(cServer,nConn,cDatabaseType)
					Endif
				Endif
				Use
			Endif
			* --- salva la nuova connessione
			If nConn<>-1
				nServers=nServers+1
				Dimension xServers[nServers,3]
				xServers[nServers,1]=cServer
				xServers[nServers,2]=nConn
				xServers[nServers,3]=cDatabaseType
				*wait window "Aperto server "+cServer+str(nConn)+' '+cDatabaseType
			Endif
		Else
			* --- connessione gia' aperta
			nConn=xServers[i,2]
			cDatabaseType=xServers[i,3]
		Endif
	Else
		nConn=i_ServerConn[1,2]
		cDatabaseType=i_ServerConn[1,6]
	Endif
	Return(nConn)

Func cp_CloseAllServerForRebuild(xServers,nServers)
	Local i
	For i=1 To nServers
		=SQLDisconn(xServers[i,2])
		*wait window "chiuso il server "+xServers[i,1]
	Next
	Return(.T.)

Func cp_CheckTable(cName,cPhName,nConn,oDCX,cDatabaseType)
	Local r,i,nfld,Current[1],p,fldname,fldtype,fldlen,flddec,res,k,i_oldexact,i_IsFieldTranslate,i_nidx
	If Empty(cPhName)
		cPhName=cName
	Endif
	If Not(cp_ExistTable(cPhName,nConn))
		Return(.F.)
	Endif
	If Vartype(i_aCpLangs)="U"
		cp_LanguageTranslateData()
	Endif
	If nConn<>0
		r=cp_SQL(nConn,'select * from '+cPhName+' where 1=2','_chk_')
		If r=-1
			Return(.F.)
		Endif
	Else
		Select * From (cPhName) Where .F. Into Cursor _chk_
	Endif
	res=.T.
	nfld=Afields(Current)
	i_oldexact=Set('EXACT')
	Set Exact On
	i_nidx=oDCX.GetTableIdx(cName)
	For i=1 To oDCX.GetFieldsCount(cName,i_nidx)
		fldname=oDCX.GetFieldName(cName,i,i_nidx)
		fldtype=oDCX.GetFieldType(cName,i,i_nidx)
		i_IsFieldTranslate=fldtype='C' And oDCX.IsFieldTranslate(cName,i,i_nidx) And Vartype(i_IsListLangModified)='L' And i_IsListLangModified
		fldlen=oDCX.GetFieldLen(cName,i,i_nidx)
		flddec=oDCX.GetFieldDec(cName,i,i_nidx)
		p=Ascan(Current,Upper(fldname))
		If p=0 .Or. cp_IsFldModified(Current(p+1),Current(p+2),Current(p+3),fldtype,fldlen,flddec) Or i_IsFieldTranslate
			res=.F.
			Exit && esco dal ciclo...
		Endif
	Next
	Set Exact &i_oldexact
	Use In Select('_chk_')
	Return(m.res)

Func cp_MakeTable(cName,cPhName,nConn,oDCX,cDatabaseType,cAzi)
	* --- crea o aggiorna la tabella specificata
	Local i,p,s,Current[1],nfld,fldname,fldlen,flddec,fldtype,fldtypedef,fldcheck,flddefault,fldnull,r,nDcxFld,bTypeMod,newvalue,i_numberfields,i_orig_fldname
	Local i_cErrSave,i_oldexact
	Local i_chkname
	Private i_bErr
	*************************************************************************************************
	*** Zucchetti Aulla - se la tabella � temporanea cerco di eliminarla e ricostruirla per non farmi dare errori sui cambiamenti di chiave
	If Right(Alltrim(cPhName),6)='_proto'
		r=cp_sqlexec(nConn,"drop table "+cPhName)
		If r<>-1
			i_existenttbls=Strtran(i_existenttbls,'|'+cPhName+Alltrim(Str(nConn))+'|','|')
		Endif
	Endif
	*************************************************************************************************
	*--- Zucchetti Aulla Inizio - Durante la costruzione delle tabelle verifico che la tabella esista senza utilizzare la varaibile pubblica i_existenttbls
	If Not(cp_ExistTable(cPhName,nConn,.F.,.F.,.F.,.T.))
*	If Not(cp_ExistTable(cPhName,nConn))
	*--- Zucchetti Aulla Inizio - Durante la costruzione delle tabelle verifico che la tabella esista senza utilizzare la varaibile pubblica i_existenttbls	
		* --- il file e' da creare
		s=oDCX.GetFieldsDef(cName,cPhName,cDatabaseType)
		If nConn<>0
			s=s+',cpccchk Char(10)'
			r=cp_SQL(nConn,"create table "+cPhName+" ("+s+db_InlinePrimaryKey(cName,cPhName,cp_KeyToSQL(oDCX.GetIdxDef(cName,1)),cDatabaseType)+')'+cp_PostTable(cDatabaseType))
			msgwnd.Add("create table "+cPhName+" ("+s+")",r)
			*!!!cp_sp_ODBCTableTriggers(cPhName,nConn,oDCX,cDatabaseType)
			s=oDCX.GetTableCheck(cName,cDatabaseType)
			If !Empty(s)
				i_chkname=cp_TableCheckName(cPhName,cDatabaseType)
				=cp_SQL(nConn,"alter table "+cPhName+" add constraint "+i_chkname+" check ("+s+')')
				msgwnd.Add("alter table "+cPhName+" add constraint "+i_chkname+" check ("+s+')')
			Endif
			*** Zucchetti Aulla Inizio - Indice unico su lower(pk) - solo PostgreSQL
			s=db_CreateLowerIndex(cName,cPhName,cp_KeyToSQL(oDCX.GetIdxDef(cName,1)),oDCX,cDatabaseType)
			if not empty(s)
				r=cp_SQL(nConn,"create unique index l_pk_"+ cPhName+ s)
				msgwnd.Add("create unique index l_pk_"+ cPhName+ s)
			Endif
			*** Zucchetti Aulla Fine - Indice unico su lower(pk) - solo PostgreSQL
		Else
			s=s+',cpccchk C(10)'
			Create Table &cPhName (&s)
			msgwnd.Add("create table "+cPhName+" ("+s+")")
			Use
			cp_sp_createtrigger(cName,cPhName,'update')
			cp_sp_triggerset(cName,'update','cpccchk',"cpccchk<>oldval('cpccchk') or type('i_cppacking')='T'",cp_Translate(MSG_CANNOT_CHANGE_A_CPCCCHK_A))
			cp_sp_triggerset(cName,'update','unmodpk',cp_unmodpkexpr(oDCX,cName),cp_Translate(MSG_YOU_CANNOT_CHANGE_THE_PRIMARY_KEY))
			s=oDCX.GetTableCheck(cName,cDatabaseType)
			If !Empty(s)
				Alter Table &cPhName Set Check &s
			Endif
		Endif
	Else
		* --- il file e' da modificare
		cp_RemoveIndexes(cName,cPhName,nConn,oDCX,cDatabaseType,cAzi)  && bisogna rimuovere le chiavi altrimenti non si possono modificare i campi
		* --- legge l' archivio "cName" per sapere il tipo attuale dei campi
		If nConn<>0
			i_chkname=cp_TableCheckName(cPhName,cDatabaseType)
			cp_sqlexec(nConn,"alter table "+cPhName+" drop constraint "+i_chkname)
			s=oDCX.GetTableCheck(cName,cDatabaseType)
			If !Empty(s)
				=cp_SQL(nConn,"alter table "+cPhName+" add constraint "+i_chkname+" check ("+s+")")
				msgwnd.Add("alter table "+cPhName+" add constraint "+i_chkname+" check ("+s+")")
			Endif
			* --- limita il numero di record da scaricare
			=cp_SQL(nConn,"select * from "+cPhName+" where 1=2")
		Else
			cp_SafeUseExcl(cPhName)
			cp_sp_createtrigger(cName,cPhName,'update')
			cp_sp_triggerset(cName,'update','cpccchk',"cpccchk<>oldval('cpccchk') or type('i_cppacking')='T'",cp_Translate(MSG_CANNOT_CHANGE_A_CPCCCHK_A))
			cp_sp_triggerset(cName,'update','unmodpk',cp_unmodpkexpr(oDCX,cName),cp_Translate(MSG_YOU_CANNOT_CHANGE_THE_PRIMARY_KEY))
			s=oDCX.GetTableCheck(cName,cDatabaseType)
			If !Empty(s)
				Alter Table &cPhName Set Check &s
			Endif
		Endif
		nfld=Afields(Current)
		i_nidx=oDCX.GetTableIdx(cName)
		nDcxFld=oDCX.GetFieldsCount(cName,i_nidx)
		For i=1 To nDcxFld+1
			If i<=nDcxFld
				fldname=oDCX.GetFieldName(cName,i,i_nidx)
				fldtype=oDCX.GetFieldType(cName,i,i_nidx)
				fldlen=oDCX.GetFieldLen(cName,i,i_nidx)
				flddec=oDCX.GetFieldDec(cName,i,i_nidx)
				flddefault=oDCX.GetFieldDefault(cName,i,cDatabaseType,i_nidx)
				fldnull=oDCX.GetFieldNull(cName,i,cDatabaseType,i_nidx)
				fldcheck=oDCX.GetFieldCheck(cName,i,cDatabaseType,i_nidx)
			Else
				fldname='CPCCCHK'
				fldtype='C'
				fldlen=10
				flddec=0
				flddefault=''
				fldnull=''
				fldcheck=''
			Endif
			*************************************************************
			*** Controllo se campo traducibile devo ciclare sulle lingue
			If Type("i_aCpLangs")="U"
				cp_LanguageTranslateData()
			Endif
			i_numberfields=Iif(oDCX.IsFieldTranslate(cName,i,i_nidx) And Not Empty(i_aCpLangs[1]),0,Alen(i_aCpLangs))
			i_orig_fldname=Iif(i<=nDcxFld,oDCX.GetFieldName(cName,i,i_nidx),fldname)
			Do While i_numberfields<=Alen(i_aCpLangs)
				i_numberfields=i_numberfields+1
				If i_numberfields=Alen(i_aCpLangs)+1
					fldname=i_orig_fldname
				Else
					fldname=Alltrim(i_orig_fldname)+"_"+Alltrim(i_aCpLangs[i_numberfields])
				Endif
				i_oldexact=Set('EXACT')
				Set Exact On
				p=Ascan(Current,Upper(fldname))
				Set Exact &i_oldexact
				bTypeMod=.F.
				If p<>0
					bTypeMod=cp_IsFldModified(Current(p+1),Current(p+2),Current(p+3),fldtype,fldlen,flddec)
				Endif
				If p=0 Or !Empty(fldcheck) Or bTypeMod
					fldtypedef=db_FieldType(cDatabaseType,fldtype,fldlen,flddec)
					If nConn<>0
						If p=0
							* --- nuova definizione della colonna + check
							r=cp_SQL(nConn,"alter table &cPhName add &fldname &fldtypedef &flddefault &fldcheck ") && non messo fldnull tra default e check
							msgwnd.Add("alter table &cPhName add &fldname &fldtypedef &flddefault &fldcheck ",r)
							If !Empty(flddefault)
								* --- aggiunge i valori di default ai campi dei record gia' esistenti
								newvalue=Substr(flddefault,9)
								r=cp_SQL(nConn,"update &cPhName set &fldname = &newvalue")
								msgwnd.Add("update &cPhName set &fldname = &newvalue",r)
							Endif
						Else
							If bTypeMod
								* --- solo alcuni database prevedono la modifica della colonna
								k=db_ModifyColumn(fldname,fldtypedef,cDatabaseType,nConn)
								If Empty(k)
									cp_msg(cp_MsgFormat(MSG__DATABASE_DOES_NOT_ALLOW_CHANGING_TYPE_OF__F__,cDatabaseType,cPhName,fldname))
									* Log Errori
									msgwnd.add_Err(cp_MsgFormat(MSG__DATABASE_DOES_NOT_ALLOW_CHANGING_TYPE_OF__F__ ,cDatabaseType,cPhName,fldname))
									* Fine Log Errori
								Else
									If Lower(cDatabaseType)='sqlserver' And !Empty(fldnull)
										k=k+fldnull
									Endif
									r=cp_SQL(nConn,"alter table &cPhName "+k)
									msgwnd.Add("alter table &cPhName "+k,r)
								Endif
							Endif
							If !Empty(fldcheck)
								* --- setta solamente il check
								cp_sqlexec(nConn,"alter table &cPhName drop constraint CK_&cPhName._&fldname.")
								If cDatabaseType='PostgreSQL'
									* --- Postgres non riesce a cancellare il check precedente
									r=cp_SQL(nConn,"alter table &cPhName add constraint CK_&cPhName._&fldname. &fldcheck",.F.,.F.,.T.)
									If r=-1 And At('Duplicate CHECK constraint name',Message())<>0
										*WAIT WINDOW "il database postgres non permette..."
									Else
										msgwnd.Add("alter table &cPhName add &fldcheck",r)
									Endif
								Else
									r=cp_SQL(nConn,"alter table &cPhName add constraint CK_&cPhName._&fldname. &fldcheck")
									msgwnd.Add("alter table &cPhName add &fldcheck",r)
								Endif
							Endif
						Endif
					Else
						If p=0
							* --- nuova colonna, aggiunge
							Alter Table &cPhName Add &fldname &fldtypedef &flddefault &fldcheck
							msgwnd.Add("alter table &cPhName add &fldname &fldtypedef &flddefault &fldcheck ")
						Else
							i_cErrSave=On('ERROR')
							i_bErr=.F.
							On Error i_bErr=.T.
							If bTypeMod
								* --- modifica una colonna esistente
								Alter Table &cPhName Alter Column &fldname &fldtypedef &fldcheck
								msgwnd.Add("alter table &cPhName alter column &fldname &fldtypedef &fldcheck")
							Else
								* --- cambia solo il check
								Alter Table &cPhName Alter Column &fldname Set &fldcheck
								msgwnd.Add("alter table &cPhName alter column &fldname set &fldcheck")
							Endif
							On Error &i_cErrSave
							If i_bErr
								*cp_ErrorMsg(cp_MsgFormat(MSG_ERROR__ALTERING_TABLE__,message(),cPhName),16,MSG_DATABASE)
								* Log Errori
								msgwnd.add_Err(cp_MsgFormat(MSG_ERROR__ALTERING_TABLE__,Message(),cPhName))
								* Fine Log Errori
							Endif
						Endif
					Endif
				Endif
			Enddo
		Next
		i_cppacking=Datetime()
		If nConn=0
			Pack
		Endif
		Use
		i_cppacking=' '
	Endif
	Return(.T.)

Function cp_TableCheckName(cPhName,cDatabaseType)
	Local res
	res='cp_'+cPhName+'_tblchk'
	If cDatabaseType='Informix'
		res='TC'+cPhName
	Endif
	Return(res)

Proc cp_RemoveIndexes(cName,cPhName,nConn,oDCX,cDatabaseType,cAzi)
	Local i,idxname,r,k
	Private res
	If nConn=0
		* --- apre il file
		cp_SafeUseExcl(cPhName)
	Endif
	* --- rimuove le chiavi
	* --- modifica per errore riduzione indici su oracle
	For i=1 To Max(oDCX.GetIdxCount(cName),10)
		If nConn<>0
			idxname = cPhName+Alltrim(Str(i))
			If i=1
				* --- controlla se la chiave primaria e' stata modificata
				If cp_IsPrimaryKeyModified(cName,cPhName,nConn,oDCX,cDatabaseType)
					k=db_DropPrimaryKey(cName,cPhName,cDatabaseType)
					r=cp_sqlexec(nConn,"alter table "+cPhName+" "+k)
					If r=-1
						* --- non e' riuscito a cancellare la chiave primaria, ci sono delle referenze
						* --- droppa tutte le foreign key, che tanto poi dovranno essere ricostruite
						cp_RemoveReferences(cName,cPhName,nConn,oDCX,cDatabaseType,cAzi)
						r=cp_SQL(nConn,"alter table "+cPhName+" "+k)
					Endif
					msgwnd.Add("alter table "+cPhName+" "+k,r)
					* --- cancellazione indice se su oracle 10
					If cDatabaseType='DB2/390' Or cDatabaseType='Oracle'
						r=cp_sqlexec(nConn,"drop index pk_"+cPhName)
						msgwnd.Add("drop index pk_"+cPhName,r)
					Endif
					*** Zucchetti Aulla Inizio - Indice unico su lower(pk) - solo PostgreSQL
					* --- cancellazione indice unico dei campi lower(pk)
					If cDatabaseType='PostgreSQL'
						r=cp_sqlexec(nConn,"drop index l_pk_"+cPhName)
						msgwnd.Add("drop index l_pk_"+cPhName,r)
					Endif
					*** Zucchetti Aulla Fine - Indice unico su lower(pk) - solo PostgreSQL
				Endif
			Else
				k=db_DropIndex(cName,cPhName,idxname,cDatabaseType)
				r=cp_sqlexec(nConn,"drop index "+k)
				If i>oDCX.GetIdxCount(cName) And r=-1
					Exit
				Endif
				msgwnd.Add("drop index "+k,r)
			Endif
		Else
			idxname = 'IDX'+Alltrim(Str(i))
			On Error res=.F.
			Delete Tag &idxname
			If i=1
				Delete Tag idxdel
				Delete Tag idx1bis
			Endif
			On Error
		Endif
	Next
	If nConn=0
		* --- chiude il file
		Use
	Endif
	Return

Func cp_RemoveReferences(cName,cPhName,nConn,oDCX,cDatabaseType,cAzi)
	Local j,T,p,x,kk
	For j=1 To oDCX.GetIFKCount(cName)
		* --- droppa tutte le referenze
		T=oDCX.GetIFKTable(cName,j)
		p=oDCX.GetPhTable(oDCX.GetTableIdx(T),cAzi)
		x=oDCX.GetIFKIndIdx(cName,j)
		db_DropFK(nConn,T,p,x,cDatabaseType)
		If oDCX.IsTableAzi(T) And !oDCX.IsTableAzi(cName)
			*wait window "deve ciclare!"
			If i_ServerConn[1,2]<>0
				cp_sqlexec(i_ServerConn[1,2],"select * from cpazi","_riazi_")
			Else
				Select * From cpazi Into Cursor _riazi_ nofilter
			Endif
			Scan
				p=oDCX.GetPhTable(oDCX.GetTableIdx(T),_riazi_.codazi)
				db_DropFK(nConn,T,p,x,cDatabaseType)
				Select _riazi_
			Endscan
			Use
		Endif
	Next
	Return

Func cp_MakeIndexes(cName,cPhName,nConn,oDCX,cDatabaseType,cAzi)
	Local idxname,idxexp,SQLidxexp,i,r,k,i_bUnique
	If nConn=0
		* --- apre il file
		cp_SafeUseExcl(cPhName)
		*i_cppacking=datetime()
		*pack
		*i_cppacking=' '
	Endif
	For i=1 To oDCX.GetIdxCount(cName)
		idxexp=oDCX.GetIdxDef(cName,i)
		If Not(Empty(idxexp))
			If nConn<>0
				idxname = cPhName+Alltrim(Str(i))
				SQLidxexp=cp_KeyToSQL(idxexp,@i_bUnique)
				If i=1
					If cp_IsPrimaryKeyModified(cName,cPhName,nConn,oDCX,cDatabaseType)
						If cDatabaseType='DB2/390'
							r=cp_SQL(nConn,'create unique index pk_'+cPhName+' on '+cPhName+'('+SQLidxexp+')')
							msgwnd.Add("create unique index pk_"+cPhName+" on "+cPhName+"("+SQLidxexp+")",r)
						Endif
						r=db_CreatePrimaryKey(nConn,cName,cPhName,SQLidxexp,cDatabaseType)
						*k=db_PrimaryKeyConstraint(cName,cPhName,SQLidxexp,cDatabaseType)
						*r=cp_SQL(nConn,"alter table "+cPhName+" "+k)
						msgwnd.Add("alter table "+cPhName+" add "+db_PrimaryKeyConstraint(cName,cPhName,SQLidxexp,cDatabaseType),r)
						*** Zucchetti Aulla Inizio - Indice unico su lower(pk) - solo PostgreSQL
						k=db_CreateLowerIndex(cName,cPhName,SQLidxexp,oDCX,cDatabaseType)
						if not empty(k)
							r=cp_SQL(nConn,"create unique index l_pk_"+cPhName+ k)
							msgwnd.Add("create unique index l_pk_"+cPhName+ k)
						Endif
						*** Zucchetti Aulla Fine - Indice unico su lower(pk) - solo PostgreSQL
					Endif
				Else
					k=db_CreateIndex(cName,cPhName,idxname,SQLidxexp,cDatabaseType)
					r=cp_SQL(nConn,"create "+Iif(i_bUnique,'unique','')+" index "+k)
					msgwnd.Add("create "+Iif(i_bUnique,'unique','')+" index "+k,r)
				Endif
			Else
				idxname = 'IDX'+Alltrim(Str(i))
				i_bUnique=.F.
				VFPidxexp=cp_KeyToVFP(cName,idxexp,@i_bUnique)
				If i=1
					msgwnd.Add("alter table &cPhName add primary key &VFPidxexp tag IDX1")
					Alter Table &cPhName Add Primary Key &VFPidxexp Tag idx1
					If At('+',idxexp)<>0
						* --- fox e' molto poco "furbo" con le chiavi composte, per sicurezza generiamo un indice
						*     aggiuntivo sulla prima parte della chiave in modo che sia comunque mantenuta una certa
						*     efficienza
						SQLidxexp=Left(VFPidxexp,At('+',VFPidxexp)-1)
						SQLidxexp=cp_KeyToSQL(SQLidxexp)
						msgwnd.Add("index on &SQLidxexp tag idx1bis")
						Index On &SQLidxexp Tag idx1bis
					Endif
					msgwnd.Add("index on deleted() tag idxdel")
					Index On Deleted() Tag idxdel
				Else
					If i_bUnique
						idxname=idxname+' CANDIDATE'
					Endif
					msgwnd.Add("index on &VFPidxexp tag &idxname")
					Index On &VFPidxexp Tag &idxname
				Endif
			Endif
		Endif
	Next
	If nConn=0
		* --- chiude il file
		Use
	Endif
	Return(.T.)

Func cp_MakeForeingKey(cName,cPhName,nConn,oDCX,cDatabaseType,cAzi)
	Local i,cKeyDef,cFKTable,cFKPhTable,i_ok,k,i_bCycleAzi,cModule,p,r,i_cCreate,i_kk
	i_ok=0
	For i=1 To Max(40,oDCX.GetFKCount(cName)+10)
		If nConn<>0
			db_DropFK(nConn,cName,cPhName,i,cDatabaseType)
		Else
			cp_sp_clearRItriggers(cName,i)
		Endif
	Next
	If nConn<>0 And cDatabaseType='MySQL'
		* --- MySQL (vers 4.1) non cancella le vecchie foreign key perche' cambia il nome al costraint
		*     evitiamo di creare piu' volte la stessa chiave
		i_cCreate=''
		cp_sqlexec(nConn,'show create table '+cPhName,'__mysqltmp__')
		If Used('__mysqltmp__')
			i_cCreate=__mysqltmp__.create_table
			Use In __mysqltmp__
		Endif
	Endif
	cModule=oDCX.GetTableModule(cName)
	For i=1 To oDCX.GetFKCount(cName)
		cFKTable=oDCX.GetFKTable(cName,i)
		If Not(oDCX.GetTableExternal(cFKTable)) And Not(oDCX.GetTableHistorical(cFKTable) And oDCX.GetFKIsChild(cName,i))
			If Upper(oDCX.GetFKExtKey(cName,i))==Upper(cp_KeyToSQL(oDCX.GetIdxDef(cFKTable,1)))
				cFKPhTable=oDCX.GetPhTable(oDCX.GetTableIdx(cFKTable),cAzi)
				If cp_ExistTable(cFKPhTable,nConn)
					If nConn<>0
						If cDatabaseType='MySQL'
							* --- mysql obbliga ad avere degli indici sui campi in cui viene imposto un vincolo di integrita'
							cp_sqlexec(nConn,'drop index FKIdx'+Alltrim(Str(i))+' on '+cName)
							cp_sqlexec(nConn,'create index FKIdx'+Alltrim(Str(i))+' on '+cName+'('+oDCX.GetFKKey(cName,i)+')')
							* --- mysql (4.1) non cancella le foreign key, controlliamo se una foreign key gia' esiste per non scriverla piu' volte
							k=db_CreateForeignKey(cName,cPhName,i,oDCX.GetFKKey(cName,i),oDCX.GetFKPhTable(cName,i,cAzi),oDCX.GetFKExtKey(cName,i),cDatabaseType)
							i_kk=Strtran(Strtran(Strtran(Strtran(Strtran(Strtran(k,",","`,`"),"(","(`"),")","`)"),"foreign key","FOREIGN KEY"),oDCX.GetFKPhTable(cName,i,cAzi),"`"+oDCX.GetFKPhTable(cName,i,cAzi)+"` "),"references","REFERENCES")
							i_kk=Substr(i_kk,At('FOREIGN KEY',i_kk))
							If At(i_kk,i_cCreate)=0
								r=cp_SQL(nConn,"alter table "+cPhName+" "+k)
								msgwnd.Add("alter table "+cPhName+" "+k)
							Endif
						Else
							k=db_CreateForeignKey(cName,cPhName,i,oDCX.GetFKKey(cName,i),oDCX.GetFKPhTable(cName,i,cAzi),oDCX.GetFKExtKey(cName,i),cDatabaseType)
							r=cp_SQL(nConn,"alter table "+cPhName+" "+k)
							msgwnd.Add("alter table "+cPhName+" "+k)
						Endif
					Else
						* i trigger di insert ed update utilizzano la GetFKExprVFP poiche' sanno di referenziare la chiave primaria, quindi risolvono il problema dell' ottimizzatore di VFP in caso di chiave composta
						cp_sp_createtrigger(cName,cPhName,'insert')
						cp_sp_triggerset(cName,'insert','FK_'+cFKTable+'_'+Alltrim(Str(i)),oDCX.GetFKEmptyVFP(cName,i)+' or cp_RIChk("'+cPhName+'","'+cFKPhTable+'","'+cp_BreakLongStr(oDCX.GetFKExprVFP(cName,cFKTable,i,cFKPhTable,cPhName))+'",.f.,"")',cp_MsgFormat(MSG_FIELD_VALUE_MISSING_IN___,cFKTable))
						cp_sp_createtrigger(cName,cPhName,'update')
						cp_sp_triggerset(cName,'update','FK_'+cFKTable+'_'+Alltrim(Str(i)),oDCX.GetFKEmptyVFP(cName,i)+' or cp_RIChk("'+cPhName+'","'+cFKPhTable+'","'+cp_BreakLongStr(oDCX.GetFKExprVFP(cName,cFKTable,i,cFKPhTable,cPhName))+'",.f.,"")',cp_MsgFormat(MSG_FIELD_VALUE_MISSING_IN___,cFKTable))
						i_bCycleAzi=!oDCX.IsTableAzi(cFKTable) And oDCX.IsTableAzi(cName)
						cp_sp_createtrigger(cFKTable,cFKPhTable,'delete')
						* il trigger di delete viene creato utilizzando la GetFKExpr perche' non e' detto che esista la chiave "rovescia" per verificare l' integrita'
						cp_sp_triggerset(cFKTable,'delete','FK_'+cName+'_'+Alltrim(Str(i)),'at(",'+cModule+',",","+i_cModules+",,")=0 or !cp_RIChk("'+cFKPhTable+'","'+cPhName+'","'+cp_BreakLongStr(oDCX.GetFKExpr(cName,i,cFKPhTable,cPhName))+'"'+Iif(i_bCycleAzi,',.t.',',.f.')+',"'+cModule+'"'+')',cp_MsgFormat(MSG_KEY_IN_USE_IN___,cName))
						msgwnd.Add("alter table "+cPhName+" add constraint FK_"+cPhName+"_"+Alltrim(Str(i))+" foreign key ("+oDCX.GetFKKey(cName,i)+") references "+oDCX.GetFKPhTable(cName,i,cAzi)+'('+oDCX.GetFKExtKey(cName,i)+')')
					Endif
				Else
					cp_msg(cp_Translate(MSG_TABLE_RELATIONSHIP_STILL_NOT_CREATED))
					i_ok=1
				Endif
			Else
				* Log Errori
				*cp_ErrorMsg(cp_MsgFormat(MSG_RELATIONSHIP_KEY_WRONG_BETWEEN__AND__,cName,cFKTable),16,MSG_DATABASE)
				msgwnd.add_Err(cp_MsgFormat(MSG_RELATIONSHIP_KEY_WRONG_BETWEEN__AND__,cName,cFKTable))
				* Fine Log Errori
			Endif
		Endif
	Next
	Return(i_ok)

Function cp_PostTable(i_cDatabaseType)
	Local i_res
	i_res=''
	If i_cDatabaseType='MySQL'
		i_res=' type=InnoDB'
	Endif
	Return(i_res)

Procedure cp_BreakLongStr(i_cStr)
	If Len(i_cStr)>250
		i_cStr=Strtran(i_cStr,'=','="+"')
	Endif
	Return(i_cStr)

Proc cp_SafeUseExcl(cPhName)
	If Used(cPhName)
		Select (cPhName)
		Use
	Endif
	Select 0
	Use (cPhName) Exclusive
	Return

Func db_FieldType(db,ty,le,de)
	Local s
	s=""
	Do Case
		Case m.db='VFP' Or m.db='DBF'
			s=m.ty
			Do Case
				Case m.ty='C'
					s=m.s+'('+Alltrim(Str(m.le))+')'
				Case m.ty='N'
					s=m.s+'('+Alltrim(Str(Max(m.le,m.de+2)))+','+Alltrim(Str(m.de))+')'
			Endcase
		Case m.db='Access' Or m.db='SQLServer'
			Do Case
				Case m.ty='C'
					s='Char('+Alltrim(Str(m.le))+')'
				Case m.ty='N' Or m.ty='B' Or m.ty='I'
					Do Case
							* e' preferibile evitare gli smallint: e' difficile controllare se sono stati allargati perche' tornano sempre come INT
							*case le<=4 and de=0
							*  s='SmallInt'
						Case m.le<=9 And m.de=0
							s='Int'
							***** Zucchetti Aulla Inizio
							* Disabilita la creazione del tipo Money su SQL
							* Commentate le due rige sotto
							*CASE le=18 AND de=4 AND db="SQLServer"
							*  s="money"
							***** Zucchetti Aulla Fine
						Otherwise
							If m.db='Access'
								s='Float'
							Else
								s='Decimal('+Alltrim(Str(m.le))+','+Alltrim(Str(m.de))+')'
							Endif
					Endcase
				Case m.ty='D' Or m.ty='T'
					s='DateTime'
				Case m.ty='L'
					s='Bit'
				Case m.ty='M'
					s=Iif(m.db='Access','Long','')+'Text'
			Endcase
		Case m.db='Oracle' Or At('DB2',m.db)=1 Or m.db='Informix'
			Do Case
				Case m.ty='C'
					If m.db='Oracle' And Type('i_OracleVarchar2')='L' And m.i_OracleVarchar2
						* --- la variabile globale i_OracleVarchar2 forza il database Oracle ad usare il tipo VarChar2 per i campi Carattere
						s='VarChar2('+Alltrim(Str(m.le))+')'
					Else
						s=Iif(At('DB2',m.db)=1,'VAR','')+'Char('+Alltrim(Str(m.le))+')'
					Endif
					*--- Zucchetti Aulla Inizio - tipo campo double e int
					*       case m.ty='N'
				Case m.ty='N' Or m.ty='B' Or m.ty='I'
					*--- Zucchetti Aulla Fine - tipo campo double e int
					If m.db='Oracle' And m.le=10 And m.de=0
						* --- Oracle sceglie un integer per len=10 e dec=0, il che da poi errore se si tenta di inserire dati al limite delle 10 cifre
						le=11
					Endif
					s='Numeric('+Alltrim(Str(Max(m.le,m.de+1)))+','+Alltrim(Str(m.de))+')'
				Case m.ty='D'
					s='Date'
				Case m.ty='T'
					If At('DB2',m.db)=1
						s='TimeStamp'
					Else
						s='Date'
					Endif
				Case m.ty='L'
					s='Numeric(1,0)'
				Case m.ty='M'
					Do Case
						Case m.db='Informix'
							s='Text'
							*s='NVARCHAR(200)'
						Case m.db='Oracle'
							*** Zucchetti Aulla - Modifica per avere pi� spazio nei campi memo di oracle
							*              s='VARCHAR2(2000)'
							s='VARCHAR2(4000)'
						Otherwise
							s='VARCHAR(4000)'
					Endcase
			Endcase
		Case m.db='Adabas'
			Do Case
				Case m.ty='C'
					s='Char('+Alltrim(Str(m.le))+')'
				Case m.ty='N'
					s='Fixed('+Alltrim(Str(Max(m.le,m.de+1)))+','+Alltrim(Str(m.de))+')'
				Case m.ty='D' Or m.ty='T'
					s='Date'
				Case m.ty='L'
					s='Boolean'
				Case m.ty='M'
					s='varchar(254)'
			Endcase
		Case m.db='SAPDB'
			Do Case
				Case m.ty='C'
					s='Char('+Alltrim(Str(m.le))+')'
				Case m.ty='N'
					s='Fixed('+Alltrim(Str(Max(m.le,m.de+1)))+','+Alltrim(Str(m.de))+')'
				Case m.ty='D'
					s='Date'
				Case m.ty='L'
					s='Boolean'
				Case m.ty='M'
					*          s='Varchar(2000)'
					s='Varchar(1024)'
				Case m.ty='T'
					s='TimeStamp'
			Endcase
		Case m.db='Interbase'
			Do Case
				Case m.ty='C'
					s='Char('+Alltrim(Str(m.le))+')'
				Case m.ty='N' Or m.ty='B' Or m.ty='I'
					Do Case
							* e' preferibile evitare gli smallint: e' difficile controllare se sono stati allargati perche' tornano sempre come INT
							*case le<=4 and de=0
							*  s='SmallInt'
						Case m.le<=9 And m.de=0
							s='Int'
						Otherwise
							s='Float'
					Endcase
				Case m.ty='D'
					s='Date'
				Case m.ty='T'
					s='Timestamp'
				Case m.ty='L'
					s='Decimal(1)'
				Case m.ty='M'
					s='Varchar(8000)'
			Endcase
		Case m.db='PostgreSQL'
			Do Case
				Case m.ty='C'
					s='Char('+Alltrim(Str(m.le))+')' && Zucchetti Aulla: Char anzich� VarChar
				Case m.ty='N'
					If m.de=0
						If m.le<10
							s='integer'
						Else
							s='numeric('+Alltrim(Str(m.le))+',0)'
						Endif
					Else
						s='Decimal('+Alltrim(Str(Max(m.le,m.de+1)))+','+Alltrim(Str(m.de))+')'
					Endif
				Case m.ty='D'
					s='Date'
				Case m.ty='T'
					s='Timestamp'
				Case m.ty='L'
					*s='Bool'
					s='integer'
				Case m.ty='M'
					s='text'
			Endcase
		Case db='MySQL'
			Do Case
				Case m.ty='C'
					s='VarChar('+Alltrim(Str(m.le))+')'
				Case m.ty='N'
					If m.de=0 And m.le<10
						s='INT'
					Else
						s='Decimal('+Alltrim(Str(Max(m.le,m.de+1)))+','+Alltrim(Str(m.de))+')'
					Endif
				Case m.ty='D'
					s='Date'
				Case m.ty='T'
					s='DateTime'
				Case m.ty='L'
					s='tinyint'
				Case m.ty='M'
					s='mediumtext'
			Endcase
		Case m.db='DBMaker'
			Do Case
				Case m.ty='C'
					s='Char('+Alltrim(Str(m.le))+')'
				Case m.ty='N'
					s='Decimal('+Alltrim(Str(Max(m.le,m.de+1)))+','+Alltrim(Str(m.de))+')'
				Case m.ty='D'
					s='Date'
				Case m.ty='T'
					s='Timestamp'
				Case m.ty='L'
					s='Decimal(1)'
				Case m.ty='M'
					s='long varchar'
			Endcase
		Otherwise
			Do Case
				Case m.ty='C'
					s='Char('+Alltrim(Str(m.le))+')'
				Case m.ty='N'
					s='Decimal('+Alltrim(Str(Max(m.le,m.de+1)))+','+Alltrim(Str(m.de))+')'
				Case m.ty='D'
					s='Date'
				Case m.ty='T'
					s='Timestamp'
				Case m.ty='L'
					s='Decimal(1)'
				Case m.ty='M'
					s='CHAR(256)'
			Endcase
	Endcase
	If Empty(m.s)
		cp_errormsg(MSG_UNKNOWN_SQL_TYPE)
	Endif
	Return(m.s)

Func db_CreatePrimaryKey(nConn,cName,cPhName,cKey,cDatabaseType)
	Local r
	r=0
	* Zucchetti Aulla Inizio - nelle cp_CreateSystemTables questa funzione � chiamata incondizionatamente,
	* ma su PostgreSQL la pk � creata contestualmente alla tabella dalla funzione db_InlinePrimaryKey
	* Nelle tabelle utente, invece, questa funzione viene chiamata solo se la PK � stata modificata (cp_IsPrimaryKeyModified)
	* Prima cp_IsPrimaryKeyModified su PostgreSQL restituiva sempre .F.
	* quindi non faceva ricostruire la PK su una tabella esistente.
	If cDatabaseType<>'PostgreSQL' OR NOT INLIST(cPhName,"cpusers","cpgroups","cpgrpgrp","cpusrgrp","cpprgsec","cpprgsec","cpttbls","cptsrvr","cpazi","cpusrrep","cpssomap","cplangs","cprecsed","cprecsec","cprecsecd","postit","cpwarn")
	* Zucchetti Aulla Fine
		r=cp_SQL(nConn,"alter table "+cPhName+" add "+db_PrimaryKeyConstraint(cName,cPhName,cKey,cDatabaseType))
	Endif
	Return(r)

Func db_InlinePrimaryKey(cName,cPhName,cKey,cDatabaseType)
	If cDatabaseType=='PostgreSQL' AND NOT EMPTY(cKey)
		Return ','+db_PrimaryKeyConstraint(cName,cPhName,cKey,cDatabaseType)
	Endif
	Return(" ")

Func db_PrimaryKeyConstraint(cName,cPhName,cKey,cDatabaseType)
	Local k
	k='constraint pk_'+cPhName+' primary key('+cKey+')'
	Do Case
		Case Inlist(cDatabaseType,'Adabas','SAPDB')
			k='primary key('+cKey+')'
		Case Inlist(cDatabaseType,'Informix')
			k='constraint (primary key('+cKey+'))'
			* --- Errore su DB2 chiave primaria tabella temporanee
		Case Inlist(cDatabaseType,'DB2/390','DB2')
			k='primary key('+cKey+')'
*** Zucchetti Aulla - commentare le due righe sotto perch� inutili (uguali al primo assegnamento)
*		Case Inlist(cDatabaseType,'PostgreSQL')
*			k='constraint pk_'+cPhName+' primary key('+cKey+')'
	Endcase
	Return(k)
*** Zucchetti Aulla Inizio - Indice unico su lower(pk) - solo PostgreSQL
Func db_CreateLowerIndex(cName,cPhName,SQLidxexp,oDCX,cDatabaseType)
	Local k,SQLlowerkey,i_nidx,idxfld,i_nfield,i_oklowerkey
	k=""
	i_oklowerkey=.F.
	If cDatabaseType=='PostgreSQL' And Not Empty(SQLidxexp)
		SQLlowerkey=""
		i_nidx=oDCX.GetTableIdx(cName)
		i_nfield=Alines(akeys,SQLidxexp,",")
		For i=1 To i_nfield
			idxfld=oDCX.GetFieldIdx(cName,akeys[i])
			If idxfld>0
				If oDCX.GetFieldType(cName,idxfld,i_nidx)="C"
					SQLlowerkey=SQLlowerkey + "lower(" + akeys[i] + "),"
					i_oklowerkey=.T.
				ELSE
					SQLlowerkey=SQLlowerkey + akeys[i] + ","
				Endif
			Endif
		Next
		If i_oklowerkey
			SQLlowerkey=Left(SQLlowerkey,Len(SQLlowerkey)-1)
			k=' on '+cPhName+' ('+SQLlowerkey+')'
		Endif
	Endif
	Return(k)
*** Zucchetti Aulla Fine - Indice unico su lower(pk) - solo PostgreSQL

Func db_DropPrimaryKey(cName,cPhName,cDatabaseType)
	Local k
	k='drop constraint pk_'+cPhName
	* --- Errore su DB2 chiave primaria tabella temporanee
	If Inlist(cDatabaseType,'Adabas','Informix','DB2/390','SAPDB','MySQL','DB2')
		k='drop primary key'
	Endif
	Return(k)

Func db_CreateIndex(cName,cPhName,cIdx,cExpr,cDatabaseType)
	Local k
	k=cIdx+' on '+cPhName+'('+cExpr+')'
	Return(k)

Func db_DropIndex(cName,cPhName,cIdx,cDatabaseType)
	Local k
	k=cIdx+' on '+cPhName
	Do Case
		Case Inlist(cDatabaseType,'SQLServer','Unknown')
			k=cPhName+'.'+cIdx
		Case Inlist(cDatabaseType,'DB2','Informix','Oracle','Interbase','PostgreSQL')
			k=cIdx
	Endcase
	Return(k)

Func db_CreateForeignKey(cName,cPhName,i,cExpr,cFKPhTable,cFKExpr,cDatabaseType)
	Local k
	k=" add constraint FK_"+cPhName+"_"+Alltrim(Str(i))+" foreign key ("+cExpr+") references "+cFKPhTable+'('+cFKExpr+')'
	Do Case
		Case cDatabaseType='DB2/390'
			k=" add foreign key FK_"+Alltrim(Str(i))+" ("+cExpr+") references "+cFKPhTable+'('+cFKExpr+') on DELETE RESTRICT'
		Case cDatabaseType='DB2'
			* k=" add constraint F"+alltrim(str(i))+cPhName+" foreign key ("+cExpr+") references "+cFKPhTable+'('+cFKExpr+')'
			k=" add constraint FK_CP_"+Alltrim(Str(i))+" foreign key ("+cExpr+") references "+cFKPhTable+'('+cFKExpr+')'
		Case cDatabaseType='Adabas' Or cDatabaseType='SAPDB'
			k="foreign key FK_"+cPhName+"_"+Alltrim(Str(i))+" ("+cExpr+") references "+cFKPhTable+'('+cFKExpr+')'
		Case cDatabaseType='Informix'
			k=" add constraint (foreign key ("+cExpr+") references "+cFKPhTable+'('+cFKExpr+') constraint F'+Alltrim(Str(i))+cPhName+')'
	Endcase
	Return(k)

	* --- Aggiunto parametro bType, se .t. costruisce
	* ---                 il constraints nel vecchio modo
Func db_DropForeignKey(cName,cPhName,i,cDatabaseType,bType)
	Local k
	k=' drop constraint FK_'+cPhName+'_'+Alltrim(Str(i))
	Do Case
		Case cDatabaseType='DB2/390'
			k=' drop foreign key FK_'+Alltrim(Str(i))
		Case cDatabaseType='Adabas' Or cDatabaseType='SAPDB' Or cDatabaseType='MySQL'
			k=' drop foreign key FK_'+cPhName+'_'+Alltrim(Str(i))
		Case cDatabaseType='DB2'
			* --- Aggiunto if e ramo else, problema nome constraints
			If bType
				k=' drop constraint F'+Alltrim(Str(i))+cPhName
			Else
				k=' drop constraint FK_CP_'+Alltrim(Str(i))
			Endif
		Case cDatabaseType='Informix'
			k=' drop constraint F'+Alltrim(Str(i))+cPhName
	Endcase
	Return(k)

Func db_DropForeignKeyOldName(cName,cPhName,i,cDatabaseType)
	Local k
	k=' drop constraint FK_CPRCONST_'+Alltrim(Str(i))
	Return(k)

Proc db_DropFK(i_nConn,T,p,x,cDatabaseType)
	Local kk,r
	If Left(cDatabaseType,3)='DB2'
		* --- nel caso di DB2 testiamo se la foriegn key esiste perche' cancellando un costraint che non esiste si riempie la tabella dei messaggi di sistema dell' AS/400
		kk='F'+Alltrim(Str(x))+p
		If cDatabaseType=='DB2' Or db_FindDB2FK(i_nConn,kk,p)
			kk=db_DropForeignKey(T,p,x,cDatabaseType,.T.)
			*wait window "alter table "+p+" "+kk
			cp_sqlexec(i_nConn,"alter table "+p+" "+kk)
		Endif
		kk='FK_CP_'+Alltrim(Str(x))
		If cDatabaseType=='DB2' Or db_FindDB2FK(i_nConn,kk,p)
			kk=db_DropForeignKey(T,p,x,cDatabaseType,.F.)
			*wait window "alter table "+p+" "+kk
			cp_sqlexec(i_nConn,"alter table "+p+" "+kk)
		Endif
		kk='FK_CPRCONST_'+Alltrim(Str(x))
		If cDatabaseType=='DB2' Or db_FindDB2FK(i_nConn,kk,p)
			kk=db_DropForeignKeyOldName(T,p,x,cDatabaseType)
			cp_sqlexec(i_nConn,"alter table "+p+" "+kk)
		Endif
	Else
		kk=db_DropForeignKey(T,p,x,cDatabaseType)
		*wait window "alter table "+p+" "+kk
		If !Empty(kk)
			cp_sqlexec(i_nConn,"alter table "+p+" "+kk)
		Endif
	Endif
	Return

Func db_FindDB2FK(i_nConn,i_fk,p)
	Local r,res,s
	res=.T.
	s=Select()
	r=cp_sqlexec(i_nConn,"select * from syskeycst where constraint_name='"+Upper(i_fk)+"' and table_name='"+Upper(p)+"'",'__tmpas400__')
	If r=1 And Used('__tmpas400__')
		Select ('__tmpas400__')
		Locate For 1=1
		res=!Eof()
		Use
		Select (s)
	Endif
	Return(res)

Func db_ModifyColumn(cFld,cFldType,cDatabaseType,nConn)
	Local k
	k=''
	Do Case
		Case cDatabaseType='Oracle'
			k='modify('+cFld+' '+cFldType+')'
		Case cDatabaseType='Adabas' Or cDatabaseType='SAPDB'
			k='column '+cFld+' '+cFldType
		Case cDatabaseType='SQLServer' And Val(Left(SQLXGetDatabaseVersion(nConn),2))>=7
			k='alter column '+cFld+' '+cFldType
		Case cDatabaseType='MySQL'
			k='modify '+cFld+' '+cFldType
		Case cDatabaseType='PostgreSQL'
			k='alter '+cFld+' type '+cFldType
			* --- Se VARCHAR Db2 ammette la modifica del campo
		Case cDatabaseType='DB2'And Left(cFldType,3)='VAR'
			k='alter column '+cFld+' SET DATA TYPE '+cFldType
	Endcase
	Return(k)

Func cp_IsFldModified(oldtype,oldlen,olddec,fldtype,fldlen,flddec)
	Local res
	res=.T.
	Do Case
		Case oldtype='C' And fldtype='C'
			res=Not(oldlen>=fldlen)
		Case (oldtype='T' Or oldtype='D') And fldtype='D'
			res=.F.
		Case oldtype='T' And fldtype='T'
			res=.F.
		Case oldtype='N' And fldtype='N'
			res=(fldlen>oldlen+Iif(cp_dbtype='Oracle' And fldlen<19,-2,0)) Or (flddec>olddec)
		Case oldtype='B' And fldtype='N' And (flddec<>0 Or fldlen>9)
			res=.F.
		Case oldtype='I' And (fldtype='N' And flddec=0 And fldlen<=9) Or fldtype='L'
			res=.F.
		Case oldtype='M' And fldtype='M'
			res=.F.
		Case (oldtype='L' Or oldtype='N') And fldtype='L'
			res=.F.
		Case oldtype='Y' And fldtype='N' And ((fldlen=18 And flddec=4) Or flddec<=2)
			res=.F.
	Endcase
	*IF res
	*  wait window 'vecchio='+oldtype+str(oldlen)+str(olddec)+' nuovo='+fldtype+str(fldlen)+str(flddec)
	*endif
	Return(res)

Define Class stdaManageTb As CPsysform
	Top = 13
	Left = 16
	Height = 285
	Width = 440
	DoCreate = .T.
	BackColor = Rgb(192,192,192)
	Caption = ''
	WindowType = 1                       && Modale
	Icon=i_cBmpPath+i_cStdIcon
	Add Object oalias As TextBox With FontBold=.F.,Height=24,Left=96,Top=12,Width=109,Enabled=.F.
	Add Object ofilename As TextBox With FontBold=.F.,Height=24,Left=96,Top=40,Width=331
	Add Object label1 As Label With AutoSize=.T.,FontBold=.F.,BackColor = Rgb(192,192,192),Caption = '',Height=18,Left=12,Top=12,Width=67, BackStyle=0
	Add Object label2 As Label With AutoSize=.T.,FontBold=.F.,BackColor=Rgb(192,192,192),Caption='',Height=18,Left=60,Top=42,Width=35, BackStyle=0
	Add Object onote As EditBox With FontBold=.F.,Height=90,Left=12,TabIndex=8,Top=156,Width=415,Enabled=.F.
	Add Object shape1 As Shape With BackColor = Rgb(192,192,192), BackStyle = 1, BorderStyle = 1, Height = 78, Left = 12, Top = 72, Width = 109, SpecialEffect = 0, BackStyle=0
	Add Object oflute As Checkbox With Top = 102, Left = 24, Height = 18, Width = 57, FontBold = .F., AutoSize = .T., BackColor = Rgb(192,192,192), Caption = '', TabIndex = 4, Enabled = .F.,BackStyle=0
	Add Object oflanno As Checkbox With Top = 84, Left = 24, Height = 18, Width = 49, FontBold = .F., AutoSize = .T., BackColor = Rgb(192,192,192), Caption = '', TabIndex = 3, Enabled = .F., BackStyle=0
	Add Object oflazie As Checkbox With Top = 120, Left = 24, Height = 18, Width = 66, FontBold = .F., AutoSize = .T., BackColor = Rgb(192,192,192), Caption = '', TabIndex = 5, Enabled = .F., BackStyle=0
	Add Object shape2 As Shape With BackColor = Rgb(192,192,192), BackStyle = 1, BorderStyle = 1, BorderWidth = 1, Height = 78, Left = 124, Top = 72, Width = 301, SpecialEffect = 0, BackStyle=0
	*ADD OBJECT oserver AS textbox WITH FontBold = .F., Height = 24, Left = 224, TabIndex = 6, Top = 84, Width = 183
	Add Object oserver As ComboBox With FontBold = .F., Height = 24, Left = 224, TabIndex = 6, Top = 84, Width = 183,RowSourceType=2,RowSource='_cpsrv_', BackStyle=0
	Add Object olastrel As TextBox With FontBold = .F., Height = 24, Left = 224, TabIndex = 7, Top = 114, Width = 117, Enabled = .F.
	Add Object label3 As Label With AutoSize = .T., FontBold = .F., BackColor = Rgb(192,192,192), Caption = '', Height = 18, Left = 176,	Top = 84,Width = 44,TabIndex = 0, BackStyle=0
	Add Object label4 As Label With AutoSize=.T.,FontBold=.F.,BackColor=Rgb(192,192,192),Caption='',Height=18,Left=136,Top=114,Width=83,TabIndex=0, BackStyle=0
	Add Object btnOk As CommandButton With Top=256,Left=313,Height=21,Width=53,FontBold=.F.,Caption=''
	Add Object btncanc As CommandButton With Top=256,Left=372,Height=21,Width=53,FontBold=.F.,Caption=''

	Proc Init()
		This.Caption=cp_Translate(MSG_TABLE_SET_UP)
		This.label1.Caption=cp_Translate(MSG_TABLE_NAME)+cp_Translate(MSG_FS)
		This.label2.Caption=cp_Translate(MSG_PATH)+cp_Translate(MSG_FS)
		This.oflute.Caption=cp_Translate(MSG_USER_BUTTON)
		This.oflanno.Caption=cp_Translate(MSG_YEAR)
		This.oflazie.Caption=cp_Translate(MSG_COMPANY_BUTTON)
		This.label3.Caption=cp_Translate(MSG_SERVER)+cp_Translate(MSG_FS)
		This.label4.Caption=cp_Translate(MSG_LAST_MODIFIED)+cp_Translate(MSG_FS)
		This.btnOk.Caption=cp_Translate(MSG_OK_BUTTON)
		This.btncanc.Caption=cp_Translate(MSG_CANCEL_BUTTON)
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
		DoDefault()
		Return

	Procedure GetValues()
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select * from cpttbls where filename="+cp_ToStr(_cpfiles_.FILENAME),"_tmp_")
		Else
			Select * From cpttbls Where FILENAME=_cpfiles_.FILENAME Into Cursor _Tmp_
		Endif
		This.oalias.Value=Nvl(_Tmp_.FILENAME,'')
		This.ofilename.Value=Nvl(_Tmp_.PhName,'')
		This.olastrel.Value=Nvl(_Tmp_.DateMod,'')
		This.oserver.Value=Nvl(_Tmp_.ServerName,'')
		This.oflanno.Value=Nvl(_Tmp_.FlagYear,'N')<>'N'
		This.oflute.Value=Nvl(_Tmp_.FlagUser,'N')<>'N'
		This.oflazie.Value=Nvl(_Tmp_.FlagComp,'N')<>'N'
		Use
	Procedure SaveValues()
		Local fn,sn
		Select _cpfiles_
		Replace Server With This.oserver.Value
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"update cpttbls set phname="+cp_ToStr(Trim(This.ofilename.Value))+", servername="+cp_ToStr(Trim(This.oserver.Value))+" where filename="+cp_ToStr(Trim(This.oalias.Value)))
		Else
			fn=This.ofilename.Value
			sn=This.oserver.Value
			Update cpttbls Set PhName=(fn), ServerName=(sn) Where FILENAME=This.oalias.Value
		Endif
		Return
	Procedure btnOk.Click
		Thisform.SaveValues()
		Thisform.Release()
		Return
	Procedure btncanc.Click
		Thisform.Release()
		Return
Enddefine


#Define SQL_MAX_DSN_LENGTH               32
#Define SQL_FETCH_NEXT                    1
#Define SQL_SUCCESS                       0
#Define SQL_SUCCESS_WITH_INFO             1
#Define SQL_NO_DATA_FOUND               100

Define Class stdaManageSRV As CpSysform
	Width=360
	Height=230
	BackColor=Rgb(192,192,192)
	WindowType=1
	bIsInsert=.F.
	Caption=MSG_SERVER_SET_UP
	AutoCenter=.T.
	Icon=i_cBmpPath+i_cStdIcon
	Add Object lblName As Label With Top=5,Height=24,Left=5,FontBold=.F.,Alignment=1,Width=60,Caption='',BackColor=Rgb(192,192,192), BackStyle=0
	Add Object lblDesc As Label With Top=35,Height=24,Left=5,FontBold=.F.,Alignment=1,Width=60,Caption='',BackColor=Rgb(192,192,192), BackStyle=0
	Add Object lblODBC As Label With Top=95,Height=24,Left=5,FontBold=.F.,Alignment=1,Width=60,Caption='',BackColor=Rgb(192,192,192), BackStyle=0
	Add Object lblType As Label With Top=125,Height=24,Left=5,FontBold=.F.,Alignment=1,Width=60,Caption='',BackColor=Rgb(192,192,192), BackStyle=0
	Add Object txtName As TextBox With Top=5,Height=24,Left=70,FontBold=.F.,Width=150,Enabled=.F.,MaxLength=10
	Add Object txtDesc As TextBox With Top=35,Height=24,Left=70,FontBold=.F.,Width=150,MaxLength=30
	Add Object chkODBC As Checkbox With Top=65,Height=25,Left=70,Caption='',FontBold=.F.,BackColor=Rgb(192,192,192),Width=220, BackStyle=0
	Add Object cmbODBC As cmbDataSource With Top=95,Height=24,Left=70,FontBold=.F.,Style=2
	Add Object txtODBC As TextBox With Top=95,Height=24,Left=70,FontBold=.F.,Width=250,Visible=.F.,MaxLength=200
	Add Object cmbType As cmbTypeDB With Top=125,Height=24,Left=70,FontBold=.F.
	Add Object chkConn As Checkbox With Top=162,Height=25,Left=70,Caption='',FontBold=.F.,BackColor=Rgb(192,192,192),Width=190, BackStyle=0
	Add Object chkPostit As Checkbox With Top=185,Left=70,Caption='' ,FontBold=.F.,BackColor=Rgb(192,192,192),Width=120, BackStyle=0
	Add Object btnOk As CommandButton With Top=5,Left=This.Width-55,Height=20,Width=50,FontBold=.F.,Caption=''
	Add Object btnCan As CommandButton With Top=30,Left=This.Width-55,Height=20,Width=50,FontBold=.F.,Caption=''

	Proc Init()
		This.Caption=cp_Translate(MSG_SERVER_SET_UP)
		This.lblName.Caption=cp_Translate(MSG_NAME)+cp_Translate(MSG_FS)
		This.lblDesc.Caption=cp_Translate(MSG_DESCR_F)+cp_Translate(MSG_FS)
		This.lblODBC.Caption=cp_Translate(MSG_ODBC)+cp_Translate(MSG_FS)
		This.lblType.Caption=cp_Translate(MSG_TYPE)+cp_Translate(MSG_FS)
		This.chkODBC.Caption=cp_Translate(MSG_IT_DSN_LESS)
		This.chkConn.Caption=cp_Translate(MSG_IT_CONNECTS_ON_PROGRAM_START)
		This.chkPostit.Caption=cp_Translate(MSG_IT_MANAGES_POSTIN)
		This.btnOk.Caption=cp_Translate(MSG_OK_BUTTON)
		This.btnCan.Caption=cp_Translate(MSG_CANCEL_BUTTON)
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
		DoDefault()
		Return

	Proc InitValues()
		Local i_nCount
		This.bIsInsert=.T.
		This.txtName.Enabled=.T.
		This.txtName.Value=''
		This.txtDesc.Value=''
		This.cmbODBC.Value=''
		*this.cmbType.value=''
		This.chkODBC.Value=.F.
		This.chkConn.Value=.T.
		This.chkPostit.Value=.T.
	Proc GetValues()
		If i_ServerConn[1,2]<>0
			=cp_SQL(i_ServerConn[1,2],"select * from cptsrvr where servername="+cp_ToStr(_cpsrv_.ServerName),"_tmp_")
		Else
			Select * From cptsrvr Where ServerName=_cpsrv_.ServerName Into Cursor _Tmp_
		Endif
		This.txtName.Value=Nvl(_Tmp_.ServerName,'')
		This.txtDesc.Value=Nvl(_Tmp_.ServerDesc,'')
		This.SetODBC(Nvl(_Tmp_.ODBCDataSource,''))
		This.cmbType.Value=Nvl(_Tmp_.DatabaseType,'')
		This.chkConn.Value=_Tmp_.WhenConn=1
		This.chkPostit.Value=Nvl(_Tmp_.PostIt,'N')<>'N'
		Return
	Proc SetODBC(p_cData)
		Local l_bDsnLess
		l_bDsnLess=!Empty(p_cData) And ("DRIVER"$Upper(p_cData) Or "DSN"$Upper(p_cData)) And ";"$p_cData
		This.EnableODBC(l_bDsnLess)
		If l_bDsnLess
			This.txtODBC.Value=p_cData
		Else
			This.cmbODBC.Value=p_cData
		Endif
	Proc chkODBC.Click()
		Thisform.EnableODBC(This.Value)
	Proc EnableODBC(p_bDsnLess)
		This.cmbODBC.Visible=!p_bDsnLess
		This.txtODBC.Visible=p_bDsnLess
		This.chkODBC.Value=p_bDsnLess
	Proc cmbODBC.Valid()
		Local i_nCountTypeDB,i_bTypeDB,i_nCountDB,i_bFindDS
		i_bTypeDB=.T.
		i_bFindDS=.T.
		i_nCountDB=0
		* --- Cerca nella tabella dei data sources il database di appartenenza
		Do While i_nCountDB<This.ListCount .And. i_bFindDS
			i_nCountDB=i_nCountDB+1
			If This.Value==This.i_aDatasources[i_nCountDB,1]
				i_bFindDS=.F.
			Endif
		Enddo
		i_nCountTypeDB=1
		Thisform.cmbType.Value=''
		If Not i_bFindDS
			Do While i_nCountTypeDB<=Thisform.cmbType.ListCount .And. i_bTypeDB
				C=This.i_aDatasources[i_nCountDB,2]
				r=Ratc(Iif(Thisform.cmbType.List(i_nCountTypeDB)=='SQLServer','SQL Server',Thisform.cmbType.List(i_nCountTypeDB)),This.i_aDatasources[i_nCountDB,2])
				If r<>0
					Thisform.cmbType.Value=Thisform.cmbType.List(i_nCountTypeDB)
					i_bTypeDB=.F.
				Endif
				i_nCountTypeDB=i_nCountTypeDB+1
			Enddo
		Endif
		Return
	Proc btnOk.Click()
		Local sn,sd,so,st,cc,cp
		sn=Trim(Thisform.txtName.Value)
		sd=Trim(Thisform.txtDesc.Value)
		If Thisform.chkODBC.Value && dsn-less
			so=Trim(Thisform.txtODBC.Value)
		Else
			so=Trim(Thisform.cmbODBC.Value)
		Endif
		st=Trim(Thisform.cmbType.Value)
		cc=Iif(Thisform.chkConn.Value,1,0)
		cp=Iif(Thisform.chkPostit.Value,'Y','N')
		If !Empty(sn)
			If Thisform.bIsInsert
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],"insert into cptsrvr (servername,ServerDesc,ODBCDataSource,DatabaseType,WhenConn,PostIT) values ("+cp_ToStr(sn)+","+cp_ToStr(sd)+","+cp_ToStr(so)+","+cp_ToStr(st)+","+cp_ToStr(cc)+","+cp_ToStr(cp)+")")
				Else
					Insert Into cptsrvr (ServerName,ServerDesc,ODBCDataSource,DatabaseType,WhenConn,PostIt) Values (sn,sd,so,st,cc,cp)
				Endif
				Insert Into _cpsrv_ (ServerName) Values (sn)
			Else
				If i_ServerConn[1,2]<>0
					=cp_SQL(i_ServerConn[1,2],"update cptsrvr set ServerDesc="+cp_ToStr(sd)+",ODBCDataSource="+cp_ToStr(so)+",DatabaseType="+cp_ToStr(st)+",whenconn="+cp_ToStr(cc)+",postit="+cp_ToStr(cp)+" where servername="+cp_ToStr(sn))
				Else
					Update cptsrvr Set ServerDesc=sd,ODBCDataSource=so,DatabaseType=st,WhenConn=cc,PostIt=cp Where ServerName=sn
				Endif
			Endif
			Thisform.Release()
		Else
			cp_msg(cp_Translate(MSG_SERVERNAME_MISSING),.F.)
		Endif
	Proc btnCan.Click()
		Thisform.Release()
Enddefine

Define Class mkdbmsg_noerr As CpSysForm
	Top=10
	Left=10
	Width=400
	Height=330
	Caption=''
	Icon=i_cBmpPath+i_cStdIcon
	BackColor=Rgb(192,192,192)
	Add Object edt As EditBox With Top=5,Left=5,Width=380,Height=300,FontBold=.F.,Margin=1
	Add Object btnOk As CommandButton With FontBold=.F.,Caption='',Width=50,Left=Thisform.Width-60,Top=310,Enabled=.F.
	Add Object copytoclipboard As CommandButton With FontBold=.F.,Caption="Copy to clipboard",Width=100,Left=10,Top=310
	Proc btnOk.Click()
		Thisform.Hide()
		Thisform.Release()
	Proc Add(s,r)
		* --- accorcia i messaggi troppo lunghi, errore a 900000 caratteri
		If Len(This.edt.Value) > 100000
			This.edt.Value=Right(This.edt.Value,100000)
		Endif
		This.edt.Value=This.edt.Value+s+Chr(13)+Chr(10)
		If Type("r")='N' And r=-1
			This.edt.Value=This.edt.Value+Message()+Chr(13)+Chr(10)
		Endif
		This.edt.SelStart=Len(This.edt.Value)
	Proc End(cMsg)
		This.btnOk.Enabled=.T.
		This.Add(cMsg)
		*this.Hide()
		*this.windowtype=1
		*this.show()
	Proc Resize()
		This.edt.Width=This.Width-10
		This.edt.Height=This.Height-30
		This.btnOk.Left=This.Width-60
		This.btnOk.Top=This.Height-20
		This.copytoclipboard.Top=This.Height-20
	Procedure copytoclipboard.Click()
		_Cliptext=Thisform.edt.Value

	Proc Init()
		This.Caption=cp_Translate(MSG_REBUILDING_DATABASE)
		This.btnOk.Caption=cp_Translate(MSG_OK_BUTTON)
		If i_VisualTheme<>-1
			If i_bGradientBck
				With Thisform.ImgBackGround
					.Width = Thisform.Width
					.Height = Thisform.Height
					.Anchor = 15 && Resize Width and Height
					.Picture = i_ThemesManager.GetProp(11)
					.ZOrder(1)
					.Visible = .T.
				Endwith
			Endif
			Thisform.BackColor = i_ThemesManager.GetProp(7)
		Endif
		DoDefault()
		Return
Enddefine

* finestra con messaggio di errore
Define Class mkdbmsg As mkdbmsg_noerr
	Width=780
	Add Object Edt_err As EditBox With Top=20,Left=395,Width=380,Height=285,FontBold=.F.,Margin=1
	Add Object label1 As Label With AutoSize=.T.,FontBold=.F.,BackColor = Rgb(192,192,192),Caption = '',Height=18,Left=395,Top=3,Width=67, BackStyle = 0
	Add Object btnSave As CommandButton With FontBold=.F.,Caption='',ShowTips=.T.,ToolTipText='',Width=50,Left=Thisform.Width-60,Top=310,Enabled=.T.

	Proc btnSave.Click()
		Local NomeFile,Hfile, FLERR, msg_1, msg_2
		FLERR=.F.
		NomeFile=Putfile('',Sys(5),"TXT")
		If Not Empty(NomeFile)
			If cp_fileexist(NomeFile)  && il file esiste lo cancello
				On Error FLERR=.T.
				Delete File ( NomeFile )
				On Error
			Endif
			If Not FLERR
				* creo il file
				Hfile = Fcreate(NomeFile)
				If Hfile < 0  && Controllo che non sia stato generato un errore nell'apertura/creazione del file
					cp_errormsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,NomeFile),'','',.F.)
				Else  && NON ci sono stati errori, avvio la procedura di scrittura del messaggio
					msg_1=cp_MsgFormat(MSG_ERROR_LOG_DATABASE_REBUILDING___,Dtoc(Date()), Leftc(Time(),8))
					msg_2=cp_Translate(MSG_LOG_END)
					lenmsg_1=(73-Len(msg_1))/2
					lenmsg_2=(73-Len(msg_2))/2
					Fputs(Hfile,Replicate('*',75))
					Fwrite(Hfile,'*' + Replicate(' ',Iif(lenmsg_1<1,0,lenmsg_1))+ msg_1 + Replicate(' ',Iif(lenmsg_1<1,0,lenmsg_1) ) + '*')
					Fputs(Hfile,Replicate('*',75))
					Fputs(Hfile,' ')
					Fputs(Hfile, Thisform.Edt_err.Text)
					Fputs(Hfile,' ')
					Fputs(Hfile,Replicate('*',75))
					Fputs(Hfile,'*'+ Replicate(' ',Iif(lenmsg_2<1,0,lenmsg_2) )+ msg_2 + Replicate(' ',Iif(lenmsg_2<1,0,lenmsg_2) ) + '*')
					Fputs(Hfile,Replicate('*',75))
					Fclose(Hfile)  && Chiudo il file
					cp_errormsg(MSG_WRITING_FINISHED_CORRECTLY)
				Endif
			Else
				cp_errormsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,NomeFile),'','',.F.)
			Endif
		Endif
	Endproc

	Proc add_Err(s,r)
		* --- accorcia i messaggi troppo lunghi, errore a 900000 caratteri
		If Len(This.Edt_err.Value) > 100000
			This.Edt_err.Value=Right(This.edt.Value,100000)
		Endif
		This.Edt_err.Value=This.Edt_err.Value+s+Chr(13)+Chr(10)+Chr(13)+Chr(10)
		If Type("r")='N' And r=-1
			This.Edt_err.Value=This.Edt_err.Value+Message()+Chr(13)+Chr(10)+Chr(13)+Chr(10)
		Endif
		This.Edt_err.SelStart=Len(This.Edt_err.Value)
	Endproc

	Proc Resize()
		This.edt.Width=Int(This.Width-20)/2
		This.edt.Height=This.Height-30
		This.Edt_err.Width=Int(This.Width-20)/2
		This.Edt_err.Left=This.edt.Left+This.edt.Width+10
		This.label1.Left=This.Edt_err.Left
		This.Edt_err.Height=This.edt.Height-15

		This.btnOk.Left=This.edt.Width-30
		This.btnOk.Top=This.Height-20
		This.btnSave.Left=This.Width-60
		This.btnSave.Top=This.Height-20
		This.copytoclipboard.Top=This.Height-20
		This.copytoclipboard.Left=10
	Endproc

	Proc Init()
		This.label1.Caption=cp_Translate(MSG_ERROR_LOG)
		This.btnSave.Caption=cp_Translate(MSG_S_SAVE)
		This.btnSave.ToolTipText=cp_Translate(MSG_PRESS_TO_SAVE_ERROR_LOG)
		DoDefault()
		Return

Enddefine

*
* --- procedure per la gestione delle stored procedures in Visual FoxPro
*
Proc cp_sp_createtrigger(i_cName,i_cPhName,i_cFor)
	* --- setta il trigger nell' archivio
	If Used(i_cPhName)
		Select (i_cPhName)
		Use
		Create Trigger On &i_cPhName For &i_cFor As cp_&i_cName._&i_cFor.()
		*msgwnd.Add("create trigger on "+i_cPhName+" for "+i_cFor+" as cp_"+i_cName+"_"+i_cFor+"()")
		Use (i_cPhName) Exclusive
	Else
		Create Trigger On &i_cPhName For &i_cFor As cp_&i_cName._&i_cFor.()
		*msgwnd.Add("create trigger on "+i_cPhName+" for "+i_cFor+" as cp_"+i_cName+"_"+i_cFor+"()")
	Endif
	* --- crea un trigger nelle stored procedures, se non esiste
	Local i,j,p
	i=At('func cp_'+i_cName+'_'+i_cFor+'()',storedprocedures) && cerca il trigger, se esiste non lo aggiunge
	If i<>0
		Return
	Endif
	i=At('* --- Triggers '+i_cName+Chr(13)+Chr(10),storedprocedures)
	If i=0
		* --- nuovo blocco di triggers
		p=storedprocedures+Chr(13)+Chr(10)
	Else
		* --- modifica un blocco esistente
		p=Substr(storedprocedures,1,i-1)
	Endif
	p=p+'* --- Triggers '+i_cName+Chr(13)+Chr(10)
	p=p+'func cp_'+i_cName+'_'+i_cFor+'()'+Chr(13)+Chr(10)
	p=p+'  return(.t.)'+Chr(13)+Chr(10)
	If i<>0
		p=p+Substr(storedprocedures,i+15+Len(i_cName)+2)
	Endif
	storedprocedures=p
	Return

Proc cp_sp_triggerset(i_cName,i_cFor,i_cComment,i_cExp,i_cErr,i_bExitOnNotFound)
	* --- inserisce in un trigger un controllo specifico
	Local i,j,p,L
	i=At('func cp_'+i_cName+'_'+i_cFor+'()',storedprocedures) && cerca il trigger, se esiste non lo aggiunge
	If i=0 And i_bExitOnNotFound
		Return
	Endif
	If i=0
		cp_sp_createtrigger(i_cName,i_cFor)
		i=At('func cp_'+i_cName+'_'+i_cFor+'()',storedprocedures) && cerca il trigger, se esiste non lo aggiunge
	Endif
	p=Substr(storedprocedures,1,i-1)+'func cp_'+i_cName+'_'+i_cFor+'()'+Chr(13)+Chr(10)
	L=Mline(storedprocedures,1,i-1)
	L=Mline(storedprocedures,1,_Mline)
	Do While L<>'  * --- '+i_cComment And L<>'  return(.t.)'
		p=p+L+Chr(13)+Chr(10)
		L=Mline(storedprocedures,1,_Mline)
	Enddo
	If !Empty(i_cExp)
		p=p+'  * --- '+i_cComment+Chr(13)+Chr(10)
		p=p+'  if !('+i_cExp+')'+Chr(13)+Chr(10)
		p=p+'    cp_SPError(-2,"'+i_cErr+'")'+Chr(13)+Chr(10)
		p=p+'    return(.f.)'+Chr(13)+Chr(10)
		p=p+'  endif'+Chr(13)+Chr(10)
	Endif
	If L<>'  return(.t.)'
		L=Mline(storedprocedures,1,_Mline)
		L=Mline(storedprocedures,1,_Mline)
		L=Mline(storedprocedures,1,_Mline)
		L=Mline(storedprocedures,1,_Mline)
	Else
		p=p+L+Chr(13)+Chr(10)
	Endif
	p=p+Substr(storedprocedures,_Mline+2)
	storedprocedures=p
	Return

Proc cp_sp_clearRItriggers(i_cName,i_nLink)
	Local i_cComment,i_nCommentLen,i_cLine,i_nTrigPos,i_nTrigPos2,i_cFKTable
	i_nTrigPos=At('func cp_'+i_cName+'_insert()',storedprocedures) && cerca il trigger
	If i_nTrigPos=0
		Return
	Endif
	i_cLine=Mline(storedprocedures,1,i_nTrigPos-1)
	i_cLine=Mline(storedprocedures,1,_Mline)
	i_cComment='_'+Alltrim(Str(i_nLink))
	i_nCommentLen=Len(i_cComment)
	Do While Not(Left(i_cLine,11)+Right(i_cLine,i_nCommentLen)=='  * --- FK_'+i_cComment) And i_cLine<>'  return(.t.)'
		i_cLine=Mline(storedprocedures,1,_Mline)
	Enddo
	If i_cLine<>'  return(.t.)'
		i_nTrigPos=Rat('_',i_cLine,2)
		i_nTrigPos2=Rat('_',i_cLine)
		i_cFKTable=Substr(i_cLine,i_nTrigPos+1,i_nTrigPos2-i_nTrigPos-1)
		cp_sp_triggerset(i_cName,'insert',"FK_"+i_cFKTable+'_'+Alltrim(Str(i_nLink)),'','',.T.)
		cp_sp_triggerset(i_cName,'update',"FK_"+i_cFKTable+'_'+Alltrim(Str(i_nLink)),'','',.T.)
		cp_sp_triggerset(i_cFKTable,'delete',"FK_"+i_cName+'_'+Alltrim(Str(i_nLink)),'','',.T.)
	Endif
	Return

Func cp_unmodpkexpr(oDCX,i_cName)
	* --- costruisce l' espressione che controlla se la chiave primaria e' stata modificata
	Local i,i_nf,i_cPkExpr,i_cPk,i_cFldName
	i_cPkExpr=''
	i_cPk=oDCX.GetIdxDef(i_cName,1)
	i_nf=oDCX.GetFieldsCount(i_cName)
	For i=1 To i_nf
		i_cFldName=oDCX.GetFieldName(i_cName,i)
		If i_cFldName$i_cPk
			i_cPkExpr=i_cPkExpr+Iif(Empty(i_cPkExpr),'',' and ')+"oldval('"+i_cFldName+"')=="+i_cFldName
		Endif
	Next
	Return(i_cPkExpr)

Proc cp_sp_updatestdproc()
	Local i,p,L,k
	cp_ReadXdc()
	Set Memowidth To 1024
	p='* --- CPSTDPROC Start'+Chr(13)+Chr(10)
	p=p+'func cp_RIChk(i_cThisXXX,i_cTable,i_cExp,i_bCycleAzi,i_cMod)'+Chr(13)+Chr(10)
	p=p+'  if not(empty(i_cMod)) and (type("i_cModules")<>"C" or at(","+i_cMod+",",","+i_cModules+",")=0)'+Chr(13)+Chr(10)
	p=p+'    return(.t.)'+Chr(13)+Chr(10)
	p=p+'  endif'+Chr(13)+Chr(10)
	p=p+'  local i_cAzi,r,i_aAzi'+Chr(13)+Chr(10)
	p=p+'  i_cAzi=cp_SPGetAzi(i_cThisXXX)'+Chr(13)+Chr(10)
	p=p+'  if !i_bCycleAzi'+Chr(13)+Chr(10)
	p=p+'    return(cp_RIChk1(i_cThisXXX,i_cTable,i_cExp,i_cAzi))'+Chr(13)+Chr(10)
	p=p+'  else'+Chr(13)+Chr(10)
	p=p+'    r=.f.'+Chr(13)+Chr(10)
	p=p+'    dimension i_aAzi[1]'+Chr(13)+Chr(10)
	p=p+'    select codazi from cpazi into array i_aAzi'+Chr(13)+Chr(10)
	p=p+'    for i=1 to alen(i_aAzi)'+Chr(13)+Chr(10)
	p=p+'      r=r or cp_RIChk1(i_cThisXXX,i_cTable,i_cExp,i_aAzi[i])'+Chr(13)+Chr(10)
	p=p+'    next'+Chr(13)+Chr(10)
	p=p+'  endif'+Chr(13)+Chr(10)
	p=p+'  return(r)'+Chr(13)+Chr(10)
	p=p+'func cp_RIChk1(i_cThisXXX,i_cTable,i_cExp,i_cAzi)'+Chr(13)+Chr(10)
	p=p+'  local i_nSel,i_bRes,i_bNew,i_nRec,i_cExact'+Chr(13)+Chr(10)
	p=p+'  i_nSel=select()'+Chr(13)+Chr(10)
	p=p+'  i_cAzi=trim(i_cAzi)'+Chr(13)+Chr(10)
	p=p+'  i_cTable=strtran(upper(i_cTable),"XXX",i_cAzi)'+Chr(13)+Chr(10)
	p=p+'  i_cExp=strtran(upper(i_cExp),"XXX",i_cAzi)'+Chr(13)+Chr(10)
	p=p+'  i_bNew=!used(i_cTable)'+Chr(13)+Chr(10)
	p=p+'  if i_bNew'+Chr(13)+Chr(10)
	p=p+'    select 0'+Chr(13)+Chr(10)
	p=p+'    use (i_cTable) again'+Chr(13)+Chr(10)
	p=p+'  else'+Chr(13)+Chr(10)
	p=p+'    select (i_cTable)'+Chr(13)+Chr(10)
	p=p+'    i_nRec=iif(eof(),-1,recno())'+Chr(13)+Chr(10)
	p=p+'  endif'+Chr(13)+Chr(10)
	p=p+'  i_cExact=set("EXACT")'+Chr(13)+Chr(10)
	p=p+'  set exact on'+Chr(13)+Chr(10)
	p=p+'  locate for '+'&'+'i_cExp and not(deleted())'+Chr(13)+Chr(10)
	p=p+'  i_bRes=found()'+Chr(13)+Chr(10)
	p=p+'  set exact '+'&'+'i_cExact'+Chr(13)+Chr(10)
	p=p+'  if i_bNew'+Chr(13)+Chr(10)
	p=p+'    use'+Chr(13)+Chr(10)
	p=p+'  else'+Chr(13)+Chr(10)
	p=p+'    if i_nRec=-1'+Chr(13)+Chr(10)
	p=p+'      go bottom'+Chr(13)+Chr(10)
	p=p+'      if !eof()'+Chr(13)+Chr(10)
	p=p+'        skip'+Chr(13)+Chr(10)
	p=p+'      endif'+Chr(13)+Chr(10)
	p=p+'    else'+Chr(13)+Chr(10)
	p=p+'      goto i_nRec'+Chr(13)+Chr(10)
	p=p+'    endif'+Chr(13)+Chr(10)
	p=p+'  endif'+Chr(13)+Chr(10)
	p=p+'  select (i_nSel)'+Chr(13)+Chr(10)
	p=p+'  return(i_bRes)'+Chr(13)+Chr(10)
	p=p+'proc cp_SPError(i_nErr,i_cErr)'+Chr(13)+Chr(10)
	p=p+'  if type("bTrsErr")="U"'+Chr(13)+Chr(10)
	p=p+'    cp_ErrorMsg(i_cErr,16,"Error")'+Chr(13)+Chr(10)
	p=p+'  else'+Chr(13)+Chr(10)
	p=p+'    cp_TrsError(i_nErr,i_cErr)'+Chr(13)+Chr(10)
	p=p+'  endif'+Chr(13)+Chr(10)
	p=p+'  return'+Chr(13)+Chr(10)
	p=p+'proc cp_SPGetAzi(i_cTable)'+Chr(13)+Chr(10)
	p=p+'  if type("i_codazi")="C"'+Chr(13)+Chr(10)
	p=p+'    return(i_codazi)'+Chr(13)+Chr(10)
	p=p+'  else'+Chr(13)+Chr(10)
	p=p+'    local p,t'+Chr(13)+Chr(10)
	p=p+'    p=at("xxx",lower(i_cTable))'+Chr(13)+Chr(10)
	p=p+'    t=alias()'+Chr(13)+Chr(10)
	p=p+'    if p<>0'+Chr(13)+Chr(10)
	p=p+'      t=alias()'+Chr(13)+Chr(10)
	p=p+'      return(substr(t,p,3+len(t)-len(i_cTable)))'+Chr(13)+Chr(10)
	p=p+'    endif'+Chr(13)+Chr(10)
	p=p+'  endif'+Chr(13)+Chr(10)
	p=p+'  return("")'+Chr(13)+Chr(10)
	p=p+'func cp_SPPlanDate()'+Chr(13)+Chr(10)
	p=p+'  return("'+i_dcx.cPlanDate+'")'+Chr(13)+Chr(10)
	p=p+'* --- CPSTDPROC End'+Chr(13)+Chr(10)
	k=.T.
	i=At('* --- CPSTDPROC Start',storedprocedures)
	If i<>0
		p=Substr(storedprocedures,1,i-1)+p
		i=At('* --- CPSTDPROC End',storedprocedures)
		storedprocedures=p+Substr(storedprocedures,i+21)
	Else
		storedprocedures=p+storedprocedures
	Endif
	Return

Proc cp_sp_ODBCTableTriggers(cName,nConn,oDCX,cDatabaseType)
	Do Case
		Case cDatabaseType='SQLServer'
			cp_sqlexec(nConn,"drop trigger CP_UPTRG_"+cName)
			cp_sqlexec(nConn,"create trigger CP_UPTRG_"+cName+" on "+cName+" for update as ";
				+"if (select cpccchk from deleted)=(select cpccchk from inserted) raiserror(cp_Translate(MSG_CANNOT_CHANGE_A_CPCCCHK_A),16,-1)")
	Endcase
	Return

Proc cp_IsPrimaryKeyModified_(cName,cPhName,nConn,oDCX,cDatabaseType)
	Local i,r
	Do Case
		Case cDatabaseType='SQLServer'
			i=cp_KeyToSQL(oDCX.GetIdxDef(cName,1))
			cp_sqlexec(nConn,'sp_helpindex '+cName)
			If Lower(Alias())<>'sqlresult'
				Return(.T.)
			Endif
			Locate For index_name='pk_'+cName
			r=Strtran(sqlresult.Index_keys,' ','')
			Use
			Return(i<>r)
		Case cDatabaseType='PostgreSQL'
			Return (.F.)
	Endcase
	Return(.T.)

Proc cp_IsPrimaryKeyModified(cName,cPhName,nConn,oDCX,cDatabaseType)
	Local i,cOldPK,cNewPK
	cOldPK=''
	Do Case
		Case cDatabaseType='Oracle'
			cp_sqlexec(nConn,"select * from USER_IND_COLUMNS where INDEX_NAME='PK_"+Upper(cPhName)+"'")
			If Lower(Alias())<>'sqlresult'
				Return(.T.)
			Endif
			Scan
				cOldPK=cOldPK+Alltrim(sqlresult.COLUMN_NAME)+','
			Endscan
			Use
			cOldPK=Left(cOldPK,Len(cOldPK)-1)
			* --- Su DB2 chiave primaria tabella temporanee
		Case cDatabaseType='DB2/390'
			cp_sqlexec(nConn,"select * from SYSKEYCST where TABLE_NAME='"+Upper(cPhName)+"' AND CONSTRAINT_NAME='PK_"+Upper(cPhName)+"'")
			If Lower(Alias())<>'sqlresult'
				Return(.T.)
			Endif
			Scan
				cOldPK=cOldPK+Alltrim(sqlresult.COLUMN_NAME)+','
			Endscan
			Use
			cOldPK=Left(cOldPK,Len(cOldPK)-1)
*** Zucchetti Aulla Inizio - Verifico la PK anche per PostgreSQL (caso otherwise)
*		Case cDatabaseType='PostgreSQL'
*			Return (.F.)
*** Zucchetti Aulla Fine - Verifico la PK anche per PostgreSQL (caso otherwise)
		Otherwise
			cOldPK=SQLXPrimaryKey(nConn,cPhName,cDatabaseType)
			If Vartype(cOldPK)='N' And cOldPK=-1
				cp_autoconnect (nConn,cPhName,cDatabaseType, 'SQLXPrimaryKey' , ,@cOldPK )
			Endif
	Endcase
	* --- controlla i campi della chiave primaria
	*WAIT WINDOW ('select '+cOldPK+' from '+cPhName+' where 1=0')
	cp_sqlexec(nConn,'select '+cOldPK+' from '+cPhName+' where 1=0','__tmppk__')
	If Used('__tmppk__')
		Select ('__tmppk__')
		Local r,nfld,Current[1],p,fldname,fldtype,fldlen,flddec,i_oldexact
		nfld=Afields(Current)
		i_oldexact=Set('EXACT')
		Set Exact On
		For i=1 To Fcount()
			r=oDCX.GetFieldIdx(cName,Field(i))
			*WAIT WINDOW cName+' '+FIELD(i)+STR(r)
			fldtype=oDCX.GetFieldType(cName,r)
			fldlen=oDCX.GetFieldLen(cName,r)
			flddec=oDCX.GetFieldDec(cName,r)
			p=Ascan(Current,Upper(Field(i)))
			If p=0 .Or. cp_IsFldModified(Current(p+1),Current(p+2),Current(p+3),fldtype,fldlen,flddec)
				Set Exact &i_oldexact
				Use In Select ('__tmppk__')
				Return(.T.)
			Endif
		Next
		Set Exact &i_oldexact
		Use In Select ('__tmppk__')
	Endif
	* --- controlla che la chiave non sia cambiata
	cNewPK=cp_KeyToSQL(oDCX.GetIdxDef(cName,1))
	*wait window 'La chiave primaria ='+cNewPK+' vecchia='+cOldPK
	If Upper(cOldPK)==Upper(cp_KeyToSQL(oDCX.GetIdxDef(cName,1)))
		Return(.F.)
	Else
		Return(.T.)
	Endif

Func cp_VerifyTransferTable(i_cTableName,i_acTableTransfer,i_nTrasnfTable)
	*--- verifica che la tabella 'i_cTableName' non sia contenuta
	*--- nell'array 'i_acTableTransfer' delle tabelle trasferite
	*--- Ritorna il nome della tabella 'i_cTableName'
	*--- Se la tabella pu� essere trasferita ritorna '.t.' in 'i_bFoundTable'
	Local i_w,niFKNum,nFKnum,nTransf,i_cFKTable
	bTransf=.T.
	i_w=1
	i_bFoundTable=.T.
	* verifica che la tabella corrente non sia stata ancora trasferita
	Do While i_bFoundTable .And. i_w<=i_nTrasnfTable
		If i_acTableTransfer[i_w]==i_cTableName
			i_bFoundTable=.F.
		Endif
		i_w=i_w+1
	Enddo
	If i_bFoundTable
		* verifica che le foreign keys della tabella corrente siano gi�
		* state trasferite
		nFKnum=i_dcx.GetFKCount(i_cTableName)
		niFKNum=0
		nTransf=0
		Do While niFKNum<nFKnum  && per ogni foreign key della tabella
			niFKNum=niFKNum+1
			i_w=1
			bTransf=.T.
			i_cFKTable=i_dcx.GetFKTable(i_cTableName,niFKNum)
			Do While bTransf .And. i_w<=i_nTrasnfTable
				If i_cFKTable==i_acTableTransfer[i_w]
					bTransf=.F.
					nTransf=nTransf+1
				Endif
				i_w=i_w+1
			Enddo
		Enddo
		* se le tabelle a cui fanno riferimento le FK sono state gi� trasferite
		If nTransf<>nFKnum
			*wait window "la tabella '"+i_cFKTable+"' referente '"+i_cTableName+"'non � stata trovata"
			i_bFoundTable=.F.
		Endif
	Endif
	Return(i_bFoundTable)

Func cp_VerifyLinkTables(i_nidx,i_cName,oDCX)
	Local i
	bErr=.F.
	For i=1 To oDCX.GetFKCount(i_cName)
		If oDCX.IsTableAzi(oDCX.GetFKTable(i_cName,i))
			bErr =.T.
		Endif
	Next
	If bErr
		cp_ErrorMsg(cp_MsgFormat(MSG_TABLE__HAS_RELATIONSHIPS_WITH_COMPANY_TABLES_F,i_cName)+Chr(13)+cp_Translate(MSG_CHANGE_DESIGN_PHASE_F),48,MSG_DATABASE,.f.)
		Return(.F.)
	Endif
	Return(.T.)

Func cp_CreateAzi(i_cName,i_cDescr)
	Local i,i_acTableTransfer,i_nTransfTable,i_nDaTrasferire,i_cList
	Local ok,nConn,cDatabaseType,N,nf,cMsg
	i_cName=Trim(i_cName)
	If Type("i_cDescr")<>'C'
		i_cDescr=i_cName
	Endif
	* --- controllo esistenza
	If cp_ExistAzi(i_cName)
		cp_msg(cp_Translate(MSG_COMPANY_NAME_ALREADY_IN_USE))
		Return(.F.)
	Endif
	If Empty(i_cName)
		Return(.F.)
	Endif
	If !cp_OpenSchema('exclusive')
		cp_msg(cp_Translate(MSG_CANNOT_OPEN_DATABASE_WITH_ADMIN_RIGHTS))
		Return(.F.)
	Endif
	cp_msg(cp_MsgFormat(MSG_CREATING_COMPANY____,i_cName,i_cDescr))
	* --- inserisce nella tabella aziende
	If i_ServerConn[1,2]<>0
		=cp_sqlexec(i_ServerConn[1,2],"insert into cpazi (codazi,desazi) values("+cp_tostrodbc(i_cName)+","+cp_tostrodbc(Left(i_cDescr,30))+")")
	Else
		Insert Into cpazi (codazi,desazi) Values(i_cName,i_cDescr)
	Endif
	* --- le stored procedures non cambiano, ma devono esistere per le routine di amministrazione
	Private storedprocedures
	storedprocedures=''
	If i_ServerConn[1,2]=0
		Copy Proc To 'proc.tmp'
		h=Fopen('proc.tmp')
		storedprocedures=Fread(h,5000000)
		Fclose(h)
	Endif
	cp_sp_updatestdproc()
	* --- costruisce rispettando l'ordine di dipendenza dato dalle relazioni
	Dimension i_acTableTransfer[1]
	i_nTransfTable=0
	i_nDaTrasferire=0
	If !cp_ReadXdc()
		Return(.F.)
	Endif
	For i=1 To i_dcx.GetTablesCount()
		If i_dcx.IsTableAziIdx(i) And Not(i_dcx.Tbls[i,15]) && evita la creazione di tabelle esterne multiaziendali
			i_nDaTrasferire=i_nDaTrasferire+1
		Else
			* --- le tabelle che non dipendono dall' azienda sono da considerarsi gia' traferite
			i_nTransfTable=i_nTransfTable+1
			Dimension i_acTableTransfer[i_nTransfTable]
			i_acTableTransfer[i_nTransfTable]=i_dcx.GetTable(i)
		Endif
	Next
	* ---
	Public msgwnd
	msgwnd=Createobject('mkdbmsg')
	msgwnd.Show()
	i_bFind=.T.
	i_nCntTable=0
	nConn=i_ServerConn[1,2]
	cDatabaseType=i_ServerConn[1,6]
	Do While i_bFind .And. i_nCntTable<i_nDaTrasferire
		i_bFind=.F.
		Dimension i_Ref[1,4]
		For i=1 To i_dcx.GetTablesCount()
			If i_dcx.IsTableAziIdx(i) And Not(i_dcx.Tbls[i,15]) && evita la creazione di tabelle esterne multiaziendali
				* verifica se la tabella corrente ('n') � da trasferire
				N=i_dcx.GetTable(i)
				nf=i_dcx.GetPhTable(i,i_cName)
				i_bXfer=cp_VerifyTransferTable(N,@i_acTableTransfer,i_nTransfTable)
				If !i_bXfer
					a=Alen(i_Ref,1)
					i_Ref[a,1]=N
					i_Ref[a,2]=nf
					Dimension i_Ref[a+1,4]
				Endif
				* se la tabella corrente � da trasferire
				If i_bXfer
					i_bFind=.T.
					i_nCntTable=i_nCntTable+1   && incrementa il contatore di tabelle tasferite
					i_nTransfTable=i_nTransfTable+1
					Dimension i_acTableTransfer[i_nTransfTable]
					i_acTableTransfer[i_nTransfTable]=i_dcx.GetTable(i)
					ok=0
					cMsg=''
					cp_RebuildOneTable(N,nf,nConn,cDatabaseType,i_dcx,@cMsg,@ok,i_cName)
					* ---
					cp_msg(cp_MsgFormat(MSG_TABLE__CREATED,nf))
				Endif
			Endif
		Next
	Enddo
	If i_nCntTable<i_nDaTrasferire
		msgwnd.Add(Chr(13)+Chr(10)+cp_Translate(MSG_WARN_LINK_DOUBLECROSSED_QM))
		msgwnd.Add(cp_Translate(MSG_TABLES_INVOLVED_WITH_DOUBLECROSSED_LINKS_CL))
		i_cList=''
		For i=1 To Alen(i_Ref,1)-1
			i_cList=i_cList+Iif(Not Empty(i_cList),',','')+i_Ref[i,1]
		Next
		msgwnd.Add(i_cList+Chr(13)+Chr(10)+Chr(13)+Chr(10))
		For i=1 To Alen(i_Ref,1)-1
			i_Ref[i,3]=db_CreateAziStruct(i_Ref[i,1],i_Ref[i,2],i_cName,nConn,cDatabaseType,i_dcx,@cMsg,'table')
			i_Ref[i,4]=cMsg
		Next
		For i=1 To Alen(i_Ref,1)-1
			If i_Ref[i,3]=0
				i_Ref[i,3]=db_CreateAziStruct(i_Ref[i,1],i_Ref[i,2],i_cName,nConn,cDatabaseType,i_dcx,@cMsg,'index')
				i_Ref[i,4]=cMsg
			Endif
		Next
	Endif
	msgwnd.End(cp_MsgFormat(MSG_COMPANY__CREATED,i_cName))
	cp_OpenSchema('shared noupdate')
	Return(.T.)

Func cp_ChangeAzi(i_cName)
	* --- Zucchetti Aulla inizio i_CODAZI senza TRIM rispetto allo standard, per problemi
	* --- nel caso di confronti in VFP tra dati letti sul database (es. ESCODAZI) e I_CODAZI
	*i_cName=trim(i_cName)
	i_cName=Padr(i_cName,5,' ')
	* --- Zucchetti Aulla inizio i_CODAZI senza TRIM rispetto allo standard, per problemi
	* --- nel caso di confronti in VFP tra dati letti sul database (es. ESCODAZI) e I_CODAZI
	If !cp_ExistAzi(i_cName)
		cp_msg(cp_Translate(MSG_COMPANY_DOES_NOT_EXIST),.F.)
		Return(.F.)
	Endif
	i_codazi=i_cName
	* --- Zucchetti Aulla - Inizio Interfaccia
	*oCpToolBar.boxazi.value=i_codazi
	* --- Zucchetti Aulla - Fine Interfaccia
	* --- Zucchetti Aulla - gestione cachefile
	If Vartype(g_NoCacheFile)<>'L' Or !g_NoCacheFile
		If Vartype(g_nNumbCacheFile)='N'
			i_oCacheFile=Createobject("cp_CacheFile",g_nNumbCacheFile)
		Else
			i_oCacheFile=Createobject("cp_CacheFile")		
		Endif
	Endif	
	* --- Zucchetti Aulla - problema della gestione dei iso lingua della tabella gestionale
	If Type("i_aCpLangs")<>'U'
		Release i_aCpLangs
	Endif
	cp_LanguageTranslateData(.T.)
	Return(.T.)

Func cp_DeleteAzi(i_cAzi)
	Local i,cName,p,cTmp,nConta
	i_cAzi=Trim(i_cAzi)
	nConta=1
	If !cp_ExistAzi(i_cAzi)
		cp_msg(cp_Translate(MSG_COMPANY_DOES_NOT_EXIST),.F.)
		Return(.F.)
	Endif
	* --- carica dizionario dati
	If !cp_ReadXdc()
		Return(.F.)
	Endif
	* ---
	If !cp_OpenSchema('exclusive')
		cp_msg(cp_Translate(MSG_CANNOT_OPEN_DATABASE_WITH_ADMIN_RIGHTS),.F.)
		Return(.F.)
	Endif
	Public msgwnd
	msgwnd=Createobject('mkdbmsg')
	msgwnd.Caption=cp_MsgFormat(MSG_COMPANY__DELETING,i_cAzi)
	msgwnd.Show()
	For i=1 To i_dcx.GetTablesCount()
		* --- rimuove tutte le referenze
		cName=i_dcx.GetTable(i)
		If i_dcx.IsTableAzi(cName)
			p=i_dcx.GetPhTable(i_dcx.GetTableIdx(cName),i_cAzi)
			If i_ServerConn[1,2]<>0
				cp_RemoveReferences(cName,p,i_ServerConn[1,2],i_dcx,i_ServerConn[1,6],i_cAzi)
			Endif
		Endif
	Next
	For i=1 To i_dcx.GetTablesCount()
		* --- rimuove tutte le tabelle
		cName=i_dcx.GetTable(i)
		If i_dcx.IsTableAzi(cName) and !'(reference)'$lower(cName)
			p=i_dcx.GetPhTable(i_dcx.GetTableIdx(cName),i_cAzi)
		*--- Zucchetti Aulla inizio -controllo esistenza tabelle da cancellare
			if used("curscommand") && creato nel batch chiamante (gsut_bda)
				select count(*) as conta from curscommand where upper(alltrim(table_name))==upper(alltrim(p)) into cursor cercaazienda
				nConta=cercaazienda.conta
				use in select('cercaazienda')
			ENDIF
			if nConta>0
				if used("curscommand")
					DELETE from curscommand where upper(alltrim(table_name))==upper(alltrim(p))
				endif
		*--- Zucchetti Aulla fine -controllo esistenza tabelle da cancellare
				If i_ServerConn[1,2]<>0
					r=cp_SQL(i_ServerConn[1,2],"drop table "+p)
					msgwnd.Add("drop table "+p,r)
				Else
					Drop Table &p
					msgwnd.Add("drop table "+p)
				Endif
				*Toglie dalla stringa delle tabelle aperte la tabella cancellata
				If At(p,i_existenttbls)<>0
					cTmp=Substr(i_existenttbls,At(p,i_existenttbls)-1)
					If Rat("|",cTmp)<>1
						cTmp=Left(cTmp,At("|",cTmp,2)-1)
					Endif
					i_existenttbls=Strtran(i_existenttbls,cTmp,'')
				ENDIF
			Endif
		Endif
	Next
	If i_ServerConn[1,2]<>0
		=cp_sqlexec(i_ServerConn[1,2],"delete from cpazi where codazi="+cp_tostrodbc(i_cAzi))
		=cp_sqlexec(i_ServerConn[1,2],"delete from cpwarn where warncode="+cp_tostrodbc(i_cAzi))
	Else
		Delete From cpazi Where codazi==i_cAzi
		Delete From cpwarn Where warncode==i_cAzi
	Endif
	msgwnd.End(cp_MsgFormat(MSG_COMPANY__DELETED,i_cAzi))
	cp_OpenSchema('shared noupdate')
	Return(.T.)

Func cp_ExistAzi(i_cName)
	Local r,i_cnt
	If !Empty(i_cName)
		If i_ServerConn[1,2]<>0
			r=cp_sqlexec(i_ServerConn[1,2],"select count(*) as cnt from cpazi where codazi="+cp_tostrodbc(i_cName),"__AZI__")
		Else
			Select Count(*) As Cnt From cpazi Where codazi==i_cName Into Cursor __AZI__
		Endif
		If Used('__AZI__')
			If Type('__AZI__.cnt')='C'
				i_cnt=Val(__AZI__.Cnt)
			Else
				i_cnt=__AZI__.Cnt
			Endif
			r=i_cnt<>0
			Use
			Return(r)
		Endif
	Endif
	Return(.F.)

Define Class cmbDataSource As ComboBox
	Dimension i_aDatasources[1,2]
	Proc Init
		Local i_cDSN,i_nDSNSize,i_cDescription,i_nDescriptionSize,i_nCont,i_nHenv,i
		Do SQLXodbcapi
		*** Initialitation variables
		i_cDSN = Replicate( Chr(0), SQL_MAX_DSN_LENGTH )
		i_nDSNSize = 0
		i_cDescription = Replicate( Chr(0), SQL_MAX_DSN_LENGTH )
		i_nDescriptionSize = 0
		i_nCont = 0
		*** ODBC Environment Handle
		i_nHenv = Val( Sys(3053) )
		*** ODBC API for get Datasources
		Do While Inlist( SQLDataSources( i_nHenv, ;
				SQL_FETCH_NEXT, ;
				@i_cDSN, ;
				LEN( i_cDSN ), ;
				@i_nDSNSize, ;
				@i_cDescription, ;
				LEN( i_cDescription ), ;
				@i_nDescriptionSize ), ;
				SQL_SUCCESS, SQL_SUCCESS_WITH_INFO )
			i_nCont = i_nCont + 1
			Dimension This.i_aDatasources[i_nCont,2]
			This.i_aDatasources[i_nCont,1] = Substr( i_cDSN, 1, i_nDSNSize )
			This.i_aDatasources[i_nCont,2] = Substr( i_cDescription, 1, i_nDescriptionSize )
		Enddo
		* --- Popola la il combobox delle connessioni attive
		For i=1 To i_nCont
			This.AddItem(This.i_aDatasources[i,1])
		Next
		Return
Enddefine

Define Class cmbTypeDB As ComboBox
	Proc Init()
		This.List(1)='Access'
		This.List(2)='Adabas'
		This.List(3)='DB2'
		This.List(4)='Oracle'
		This.List(5)='SQLServer'
		This.List(6)='VFP'
		This.List(7)='Interbase'
		This.List(8)='SAPDB'
		This.List(9)='Informix'
		This.List(10)='PostgreSQL'
		This.List(11)='MySQL'
		Return
Enddefine

Proc SQLXodbcapi
	Declare SHORT SQLDataSources In odbc32.Dll ;
		INTEGER  nHenv, ;
		INTEGER  nDirection, ;
		STRING  @cDSN, ;
		INTEGER  nDSNMax, ;
		INTEGER @nDSNSize, ;
		STRING  @cDescription, ;
		INTEGER  nDescriptionMax, ;
		INTEGER @nDescription
	Return

Func cp_OpenSecondConn(i_nConn)
	Public i_sqlx2nconn,i_sqlx2ndupl,i_sqlx2dupltimestamp
	Local i_useasync
	i_useasync=Type('i_bDisableAsyncConn')='U' Or !i_bDisableAsyncConn
	If i_nConn<1000000 And i_useasync
		i_sqlx2nconn=i_nConn
		i_sqlx2ndupl=Sqlstringconnect(SQLGetprop(i_nConn,'ConnectString'))
		SQLSetprop(i_sqlx2ndupl,'Async',.T.)
		i_sqlx2dupltimestamp=Str(Seconds())
		cp_PostOpenConn(i_sqlx2ndupl)
	Else
		i_sqlx2nconn=i_nConn
		i_sqlx2ndupl=0
	Endif
	Return(.T.)

Func cp_CloseSecondConn()
	If i_ServerConn[1,2]>0 And i_ServerConn[1,2]<1000000
		*Chiude la connessione secondaria
		If Type('i_sqlx2ndupl')<>'U' And i_sqlx2ndupl<>0
			SQLCancel(i_sqlx2ndupl)
			SQLDisconn(i_sqlx2ndupl)
		Endif
	Endif
	Return(.T.)

Proc cp_DropOnCloseNewTransaction()
	Public i_nDropOnClose
	Public i_nDropOnCloseConn[1]
	Public i_cDropOnCloseTable[1]
	i_nDropOnClose=0
	i_nDropOnCloseConn[1]=0
	i_cDropOnCloseTable[1]=''
Endproc

Proc cp_DropOnCloseCloseTransaction()
	Local i_i
	For i_i=1 To i_nDropOnClose
		If !Empty(i_cDropOnCloseTable[i_i])
			cp_DropTempTable(i_nDropOnCloseConn[i_i],i_cDropOnCloseTable[i_i])
		Endif
	Next
	i_nDropOnClose=0
Endproc

Proc cp_DropOnCloseTransaction(i_nConn,i_cTable)
	i_nDropOnClose=i_nDropOnClose+1
	Dimension i_nDropOnCloseConn[i_nDropOnClose]
	Dimension i_cDropOnCloseTable[i_nDropOnClose]
	i_nDropOnCloseConn[i_nDropOnClose]=i_nConn
	i_cDropOnCloseTable[i_nDropOnClose]=i_cTable
Endproc

*-- Gestione traduzione del dato verifica se tabelle da ricostruire
Func cp_ListLangModified()
	Local i_listlanguage, i_ok, i_res, i_IdxLang, i_nConn
	If Type("i_aCpLangs")="U"
		cp_LanguageTranslateData()
	Endif
	i_nConn=i_ServerConn[1,2]
	i_IdxLang=0
	If i_nConn<>0
		i_res=cp_SQL(i_nConn,"select * from persist where code='ListLanguage'","__tmp__")
		If i_res<>-1
			i_listlanguage=__tmp__.persist
			Use
		Else
			i_ok=.T.
		Endif
	Else
		Select * From persist Where Code='ListLanguage' Into Cursor __tmp__
		i_listlanguage=__tmp__.persist
		i_ok=Eof()
		Use
	Endif
	*** verifico se le lingue nell'array ci sono tutte nell'aggionamento effettuato in precedenza
	If Not i_ok And Not Empty(i_aCpLangs[1])
		Local i_ListLang
		*costruisco lista lingue traducibili
		i_ListLang=cp_CalcListLang()
		i_ok=Not(Upper(i_ListLang) == Upper(i_listlanguage))
	Endif
	Public i_IsListLangModified
	i_IsListLangModified = i_ok
	Return(i_ok)
Endfunc

*** Zucchetti Aulla - modifica perch� la nostra tabella delle lingue � aziendale
Func cp_CalcListLang()
	Private i_ListLang, i
	*costruisco lista lingue traducibili
	i_ListLang=''
	For i=1 To Alen(i_aCpLangsPainter)
		i_ListLang=i_ListLang+i_aCpLangsPainter[i]+','
	Endfor
	Return (i_ListLang)
Endfunc

*--- Zucchetti Aulla Inizio - GetCurrentTimeDB
Function cp_GetCurrentTimeDB()
    Local i_cCmd, i_nConn, i_retTime

    Do Case
        Case cp_dbtype="SQLServer" Or cp_dbtype="PostgreSQL"
            i_cCmd = "select CURRENT_TIMESTAMP as cp_timedb from cpusers where code = "+cp_tostrodbc(i_codute)
        Case Upper(cp_dbtype)="ORACLE"
            i_cCmd = "select sysdate as cp_timedb from cpusers where code = "+cp_tostrodbc(i_codute)
    Endcase

    i_retTime = {- - :}
    i_nConn=i_ServerConn[1,2]
    If SQLExec(i_nConn, i_cCmd, "_tmp_time_") # -1
        If Used("_tmp_time_")
            i_retTime = _tmp_time_.cp_timedb
        Endif
        Use In Select("_tmp_time_")
    Endif

    Return i_retTime
Endfunc
