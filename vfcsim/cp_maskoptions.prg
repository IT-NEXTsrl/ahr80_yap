* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_maskoptions                                                  *
*              Opzioni maschera                                                *
*                                                                              *
*      Author: Nicol� Mercanti                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-03-10                                                      *
* Last revis.: 2015-05-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_maskoptions",oParentObject))

* --- Class definition
define class tcp_maskoptions as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 472
  Height = 406
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-05-29"
  HelpContextID=171808531
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_maskoptions"
  cComment = "Opzioni maschera"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TITLE = space(40)
  w_MODAL = space(1)
  w_SCREEN = 0
  w_FULLSCREEN = 0
  w_ALIGN = 0
  w_BCKIMG = space(100)
  w_GRD = space(1)
  w_GRDTYPE = 0
  w_FCOLOR = 0
  w_LCOLOR = 0
  w_HSTEP = 0
  o_HSTEP = 0
  w_VSTEP = 0
  o_VSTEP = 0
  w_SNAPGRID = 0
  o_SNAPGRID = 0
  w_SHOWGRID = 0
  w_CNFFILE = space(100)
  o_CNFFILE = space(100)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_maskoptionsPag1","cp_maskoptions",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTITLE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- cp_maskoptions
    CREATE CURSOR CurOptions (PropName c (15), FieldName c(15))
    
    Insert into CurOptions (PropName,FieldName) values ('caption' ,'title')
    Insert into CurOptions (PropName,FieldName) values ('cmodal' ,'modal')
    Insert into CurOptions (PropName,FieldName) values ('cscreen' ,'screen')
    Insert into CurOptions (PropName,FieldName) values ('cscreen' ,'fullscreen')
    Insert into CurOptions (PropName,FieldName) values ('nalignmentcnt' ,'align')
    Insert into CurOptions (PropName,FieldName) values ('cbckimage' ,'bckimg')
    Insert into CurOptions (PropName,FieldName) values ('ccnffile' ,'cnffile')
    Insert into CurOptions (PropName,FieldName) values ('cgradient' ,'grd')
    Insert into CurOptions (PropName,FieldName) values ('ngradienttype' ,'grdtype')
    Insert into CurOptions (PropName,FieldName) values ('nfcolor' ,'fcolor')
    Insert into CurOptions (PropName,FieldName) values ('nlcolor' ,'lcolor')
    Insert into CurOptions (PropName,FieldName) values ('ngridhpoint' ,'hstep')
    Insert into CurOptions (PropName,FieldName) values ('ngridvpoint' ,'vstep')
    Insert into CurOptions (PropName,FieldName) values ('nsnapgrid' ,'snapgrid')
    Insert into CurOptions (PropName,FieldName) values ('nshowgrid' ,'showgrid')
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        cp_objmaskoptions_b(this,"MaskSave")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TITLE=space(40)
      .w_MODAL=space(1)
      .w_SCREEN=0
      .w_FULLSCREEN=0
      .w_ALIGN=0
      .w_BCKIMG=space(100)
      .w_GRD=space(1)
      .w_GRDTYPE=0
      .w_FCOLOR=0
      .w_LCOLOR=0
      .w_HSTEP=0
      .w_VSTEP=0
      .w_SNAPGRID=0
      .w_SHOWGRID=0
      .w_CNFFILE=space(100)
          .DoRTCalc(1,10,.f.)
        .w_HSTEP = IIF( .w_SNAPGRID=1, .w_HSTEP ,1)
        .w_VSTEP = IIF( .w_SNAPGRID=1, .w_VSTEP ,1)
          .DoRTCalc(13,13,.f.)
        .w_SHOWGRID = IIF(.w_SNAPGRID=1, IIF(.w_HSTEP>1 AND .w_VSTEP>1,.w_SHOWGRID,0),0)
        .w_CNFFILE = FORCEEXT(.w_CNFFILE,"DBF")
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
        if .o_SNAPGRID<>.w_SNAPGRID
            .w_HSTEP = IIF( .w_SNAPGRID=1, .w_HSTEP ,1)
        endif
        if .o_SNAPGRID<>.w_SNAPGRID
            .w_VSTEP = IIF( .w_SNAPGRID=1, .w_VSTEP ,1)
        endif
        .DoRTCalc(13,13,.t.)
        if .o_VSTEP<>.w_VSTEP.or. .o_HSTEP<>.w_HSTEP.or. .o_SNAPGRID<>.w_SNAPGRID
            .w_SHOWGRID = IIF(.w_SNAPGRID=1, IIF(.w_HSTEP>1 AND .w_VSTEP>1,.w_SHOWGRID,0),0)
        endif
        if .o_CNFFILE<>.w_CNFFILE
            .w_CNFFILE = FORCEEXT(.w_CNFFILE,"DBF")
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_GUQYYQOICD()
    with this
          * --- Cambia secondo colore
          .w_LCOLOR = getcolor()
    endwith
  endproc
  proc Calculate_RPKADVNESJ()
    with this
          * --- Cambia primo colore
          .w_FCOLOR = getcolor()
    endwith
  endproc
  proc Calculate_UJQLSMFITJ()
    with this
          * --- Done
          cp_objmaskoptions_b(this;
              ,"MaskDone";
             )
    endwith
  endproc
  proc Calculate_XIWRLIIQCI()
    with this
          * --- Init
          cp_objmaskoptions_b(this;
              ,"MaskInit";
             )
    endwith
  endproc
  proc Calculate_VVUHYYVSWN()
    with this
          * --- HSTEP mai a 0
          .w_HSTEP = iif(.w_HSTEP=0,1,.w_HSTEP)
    endwith
  endproc
  proc Calculate_XGLJGBAIUV()
    with this
          * --- VSTEP mai a 0
          .w_VSTEP = iif(.w_VSTEP=0,1,.w_VSTEP)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSHOWGRID_1_30.enabled = this.oPgFrm.Page1.oPag.oSHOWGRID_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ChangeColor2")
          .Calculate_GUQYYQOICD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ChangeColor1")
          .Calculate_RPKADVNESJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_UJQLSMFITJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_XIWRLIIQCI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_HSTEP Changed")
          .Calculate_VVUHYYVSWN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_VSTEP Changed")
          .Calculate_XGLJGBAIUV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- cp_maskoptions
    IF cEvent=='ChangeColor1' or cEvent=='Init'
       local l_obj
       l_obj = This.GetCtrl("w_FCOLOR")
       l_obj.DisabledBackcolor = This.w_FCOLOR
       l_obj.DisabledForecolor = This.w_FCOLOR
       l_obj=.null.
    ENDIF
    
    IF cEvent=='ChangeColor2' or cEvent=='Init'
       local l_obj
       l_obj = This.GetCtrl("w_LCOLOR")
       l_obj.DisabledBackcolor = This.w_LCOLOR
       l_obj.DisabledForecolor = This.w_LCOLOR
       l_obj=.null.
    ENDIF
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTITLE_1_2.value==this.w_TITLE)
      this.oPgFrm.Page1.oPag.oTITLE_1_2.value=this.w_TITLE
    endif
    if not(this.oPgFrm.Page1.oPag.oMODAL_1_3.RadioValue()==this.w_MODAL)
      this.oPgFrm.Page1.oPag.oMODAL_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCREEN_1_4.RadioValue()==this.w_SCREEN)
      this.oPgFrm.Page1.oPag.oSCREEN_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFULLSCREEN_1_5.RadioValue()==this.w_FULLSCREEN)
      this.oPgFrm.Page1.oPag.oFULLSCREEN_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oALIGN_1_7.RadioValue()==this.w_ALIGN)
      this.oPgFrm.Page1.oPag.oALIGN_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBCKIMG_1_10.value==this.w_BCKIMG)
      this.oPgFrm.Page1.oPag.oBCKIMG_1_10.value=this.w_BCKIMG
    endif
    if not(this.oPgFrm.Page1.oPag.oGRD_1_12.RadioValue()==this.w_GRD)
      this.oPgFrm.Page1.oPag.oGRD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRDTYPE_1_14.RadioValue()==this.w_GRDTYPE)
      this.oPgFrm.Page1.oPag.oGRDTYPE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFCOLOR_1_17.value==this.w_FCOLOR)
      this.oPgFrm.Page1.oPag.oFCOLOR_1_17.value=this.w_FCOLOR
    endif
    if not(this.oPgFrm.Page1.oPag.oLCOLOR_1_18.value==this.w_LCOLOR)
      this.oPgFrm.Page1.oPag.oLCOLOR_1_18.value=this.w_LCOLOR
    endif
    if not(this.oPgFrm.Page1.oPag.oHSTEP_1_25.value==this.w_HSTEP)
      this.oPgFrm.Page1.oPag.oHSTEP_1_25.value=this.w_HSTEP
    endif
    if not(this.oPgFrm.Page1.oPag.oVSTEP_1_26.value==this.w_VSTEP)
      this.oPgFrm.Page1.oPag.oVSTEP_1_26.value=this.w_VSTEP
    endif
    if not(this.oPgFrm.Page1.oPag.oSNAPGRID_1_29.RadioValue()==this.w_SNAPGRID)
      this.oPgFrm.Page1.oPag.oSNAPGRID_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHOWGRID_1_30.RadioValue()==this.w_SHOWGRID)
      this.oPgFrm.Page1.oPag.oSHOWGRID_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCNFFILE_1_32.value==this.w_CNFFILE)
      this.oPgFrm.Page1.oPag.oCNFFILE_1_32.value=this.w_CNFFILE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_HSTEP>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oHSTEP_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VSTEP>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVSTEP_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_HSTEP = this.w_HSTEP
    this.o_VSTEP = this.w_VSTEP
    this.o_SNAPGRID = this.w_SNAPGRID
    this.o_CNFFILE = this.w_CNFFILE
    return

enddefine

* --- Define pages as container
define class tcp_maskoptionsPag1 as StdContainer
  Width  = 468
  height = 406
  stdWidth  = 468
  stdheight = 406
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTITLE_1_2 as StdField with uid="MWYOKFPWSM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TITLE", cQueryName = "TITLE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Titolo della maschera",;
    HelpContextID = 155334812,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=16, Top=33, InputMask=replicate('X',40)

  add object oMODAL_1_3 as StdCheck with uid="AZKTDHSBXI",rtseq=2,rtrep=.f.,left=339, top=7, caption="Modale",;
    ToolTipText = "Se attivo indica la maschera modale",;
    HelpContextID = 113075038,;
    cFormVar="w_MODAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMODAL_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMODAL_1_3.GetRadio()
    this.Parent.oContained.w_MODAL = this.RadioValue()
    return .t.
  endfunc

  func oMODAL_1_3.SetRadio()
    this.Parent.oContained.w_MODAL=trim(this.Parent.oContained.w_MODAL)
    this.value = ;
      iif(this.Parent.oContained.w_MODAL=='S',1,;
      0)
  endfunc

  add object oSCREEN_1_4 as StdCheck with uid="ZMNODKAXNT",rtseq=3,rtrep=.f.,left=339, top=32, caption="Sfondo",;
    ToolTipText = "Se attivo indica la maschera di sfondo",;
    HelpContextID = 107990666,;
    cFormVar="w_SCREEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCREEN_1_4.RadioValue()
    return(iif(this.value =1,1,;
    0))
  endfunc
  func oSCREEN_1_4.GetRadio()
    this.Parent.oContained.w_SCREEN = this.RadioValue()
    return .t.
  endfunc

  func oSCREEN_1_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SCREEN==1,1,;
      0)
  endfunc

  add object oFULLSCREEN_1_5 as StdCheck with uid="XELBMOBITN",rtseq=4,rtrep=.f.,left=339, top=57, caption="Schermo pieno",;
    ToolTipText = "Se attivo la maschera � a schermo intero",;
    HelpContextID = 207218570,;
    cFormVar="w_FULLSCREEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFULLSCREEN_1_5.RadioValue()
    return(iif(this.value =1,1,;
    0))
  endfunc
  func oFULLSCREEN_1_5.GetRadio()
    this.Parent.oContained.w_FULLSCREEN = this.RadioValue()
    return .t.
  endfunc

  func oFULLSCREEN_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FULLSCREEN==1,1,;
      0)
  endfunc


  add object oALIGN_1_7 as StdCombo with uid="MNUIAEMYAR",rtseq=5,rtrep=.f.,left=96,top=65,width=132,height=22;
    , ToolTipText = "Allineamento della maschera";
    , HelpContextID = 113065233;
    , cFormVar="w_ALIGN",RowSource=""+""+MSG_ALIGN_CENTER+","+""+MSG_ALIGN_LEFT+","+""+MSG_ALIGN_RIGHT+","+""+MSG_ALIGN_TOP+","+""+MSG_ALIGN_BOTTOM+","+""+MSG_NO_ALIGN+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oALIGN_1_7.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    0)))))))
  endfunc
  func oALIGN_1_7.GetRadio()
    this.Parent.oContained.w_ALIGN = this.RadioValue()
    return .t.
  endfunc

  func oALIGN_1_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ALIGN==1,1,;
      iif(this.Parent.oContained.w_ALIGN==2,2,;
      iif(this.Parent.oContained.w_ALIGN==3,3,;
      iif(this.Parent.oContained.w_ALIGN==4,4,;
      iif(this.Parent.oContained.w_ALIGN==5,5,;
      iif(this.Parent.oContained.w_ALIGN==6,6,;
      0))))))
  endfunc


  add object oBtn_1_9 as StdButton with uid="LWHTRRWTKQ",left=339, top=111, width=27,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Selezione file bitmap";
    , HelpContextID = 113403359;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        .w_BCKIMG=getpict()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBCKIMG_1_10 as StdField with uid="WBIDEROCGU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_BCKIMG", cQueryName = "BCKIMG",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "File immagine",;
    HelpContextID = 108415738,;
   bGlobalFont=.t.,;
    Height=21, Width=315, Left=16, Top=111, InputMask=replicate('X',100)

  add object oGRD_1_12 as StdCheck with uid="DVJOZCSELS",rtseq=7,rtrep=.f.,left=18, top=145, caption="Gradiente di sfondo",;
    ToolTipText = "Se attivo viene visualizzato il gradiente di sfondo",;
    HelpContextID = 155032485,;
    cFormVar="w_GRD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGRD_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGRD_1_12.GetRadio()
    this.Parent.oContained.w_GRD = this.RadioValue()
    return .t.
  endfunc

  func oGRD_1_12.SetRadio()
    this.Parent.oContained.w_GRD=trim(this.Parent.oContained.w_GRD)
    this.value = ;
      iif(this.Parent.oContained.w_GRD=='S',1,;
      0)
  endfunc


  add object oGRDTYPE_1_14 as StdCombo with uid="XNFBFIRBUB",rtseq=8,rtrep=.f.,left=201,top=141,width=132,height=22;
    , ToolTipText = "Tipo del gradiente di sfondo";
    , HelpContextID = 233013157;
    , cFormVar="w_GRDTYPE",RowSource=""+""+MSG_CENTRED+","+""+MSG_VERTICAL+","+""+MSG_HORIZONTAL+","+""+MSG_FORWARDDIAGONAL+","+""+MSG_BACKWARDDIAGONAL+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGRDTYPE_1_14.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    0))))))
  endfunc
  func oGRDTYPE_1_14.GetRadio()
    this.Parent.oContained.w_GRDTYPE = this.RadioValue()
    return .t.
  endfunc

  func oGRDTYPE_1_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GRDTYPE==1,1,;
      iif(this.Parent.oContained.w_GRDTYPE==2,2,;
      iif(this.Parent.oContained.w_GRDTYPE==3,3,;
      iif(this.Parent.oContained.w_GRDTYPE==4,4,;
      iif(this.Parent.oContained.w_GRDTYPE==5,5,;
      0)))))
  endfunc

  add object oFCOLOR_1_17 as StdField with uid="PEUUGSWMNE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FCOLOR", cQueryName = "FCOLOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 160749638,;
   bGlobalFont=.t.,;
    Height=21, Width=21, Left=98, Top=170, bNoDisabledColor = .T.

  add object oLCOLOR_1_18 as StdField with uid="RKZGWZDVJE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_LCOLOR", cQueryName = "LCOLOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 107685818,;
   bGlobalFont=.t.,;
    Height=21, Width=21, Left=312, Top=170, bNoDisabledColor = .T.


  add object oBtn_1_21 as StdButton with uid="PTFDAFTJPO",left=126, top=170, width=23,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Modifica primo colore";
    , HelpContextID = 113403359;
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      this.parent.oContained.NotifyEvent("ChangeColor1")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="JBKNHUIMNI",left=339, top=170, width=23,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Modifica secondo colore";
    , HelpContextID = 113403359;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      this.parent.oContained.NotifyEvent("ChangeColor2")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oHSTEP_1_25 as StdField with uid="CMXKIEALKC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_HSTEP", cQueryName = "HSTEP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Step orizzontale della griglia",;
    HelpContextID = 155378086,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=121, Top=234, cSayPict='"999"', cGetPict='"999"'

  func oHSTEP_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_HSTEP>0)
    endwith
    return bRes
  endfunc

  add object oVSTEP_1_26 as StdField with uid="TQUNDIYMZT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_VSTEP", cQueryName = "VSTEP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Step verticale della griglia",;
    HelpContextID = 155378086,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=121, Top=256, cSayPict='"999"', cGetPict='"999"'

  func oVSTEP_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VSTEP>0)
    endwith
    return bRes
  endfunc

  add object oSNAPGRID_1_29 as StdCheck with uid="GVTSQUUVSH",rtseq=13,rtrep=.f.,left=190, top=233, caption="Griglia a step",;
    ToolTipText = "Se selezionato � attivo lo step di griglia",;
    HelpContextID = 35937137,;
    cFormVar="w_SNAPGRID", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSNAPGRID_1_29.RadioValue()
    return(iif(this.value =1,1,;
    0))
  endfunc
  func oSNAPGRID_1_29.GetRadio()
    this.Parent.oContained.w_SNAPGRID = this.RadioValue()
    return .t.
  endfunc

  func oSNAPGRID_1_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SNAPGRID==1,1,;
      0)
  endfunc

  add object oSHOWGRID_1_30 as StdCheck with uid="XXWYMLWYVW",rtseq=14,rtrep=.f.,left=190, top=255, caption="Mostra griglia",;
    ToolTipText = "Se selezionato la griglia � visibile (solo se non � presente un gradiente di sfondo)",;
    HelpContextID = 35939147,;
    cFormVar="w_SHOWGRID", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSHOWGRID_1_30.RadioValue()
    return(iif(this.value =1,1,;
    0))
  endfunc
  func oSHOWGRID_1_30.GetRadio()
    this.Parent.oContained.w_SHOWGRID = this.RadioValue()
    return .t.
  endfunc

  func oSHOWGRID_1_30.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SHOWGRID==1,1,;
      0)
  endfunc

  func oSHOWGRID_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HSTEP>1 AND .w_VSTEP>1 AND .w_SNAPGRID=1)
    endwith
   endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="AATAGDOPAM",left=339, top=310, width=27,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Selezione file";
    , HelpContextID = 113403359;
  , bGlobalFont=.t.

    proc oBtn_1_31.Click()
      with this.Parent.oContained
        .w_CNFFILE=getfile('dbf','File di configurazione ')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCNFFILE_1_32 as StdField with uid="YGBEAWNUNP",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CNFFILE", cQueryName = "CNFFILE",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "File di configurazione dati maschera, mediabutton e linenode",;
    HelpContextID = 35753535,;
   bGlobalFont=.t.,;
    Height=21, Width=315, Left=18, Top=310, InputMask=replicate('X',100)


  add object oBtn_1_34 as StdButton with uid="RLPPINVZQQ",left=294, top=355, width=52,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Salva propriet� in file di configurazione";
    , HelpContextID = 112974796;
    , caption="\<Salva";
  , bGlobalFont=.t.

    proc oBtn_1_34.Click()
      with this.Parent.oContained
        cp_objmaskoptions_b(this.Parent.oContained,"MaskConf",0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not EMPTY(.w_CNFFILE))
      endwith
    endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="WIQMVFXXBT",left=350, top=355, width=52,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 113404034;
    , caption="\<Ok";
  , bGlobalFont=.t.

    proc oBtn_1_39.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_40 as StdButton with uid="YRGRYSGPUZ",left=406, top=355, width=52,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 4140191;
    , caption="\<Esci";
  , bGlobalFont=.t.

    proc oBtn_1_40.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="NFQYREMGSY",Visible=.t., Left=16, Top=12,;
    Alignment=0, Width=41, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="BZANYCIJQR",Visible=.t., Left=16, Top=69,;
    Alignment=0, Width=87, Height=18,;
    Caption="Allineamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="NIVUPAEYRE",Visible=.t., Left=16, Top=94,;
    Alignment=0, Width=59, Height=18,;
    Caption="Immagine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="QJFDGAJALQ",Visible=.t., Left=18, Top=292,;
    Alignment=0, Width=116, Height=18,;
    Caption="File di configurazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="MGHAVDDRFS",Visible=.t., Left=167, Top=145,;
    Alignment=0, Width=45, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="NQYILVANOR",Visible=.t., Left=18, Top=174,;
    Alignment=0, Width=81, Height=18,;
    Caption="Primo colore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="DCAXJDZHVB",Visible=.t., Left=215, Top=174,;
    Alignment=0, Width=97, Height=18,;
    Caption="Secondo colore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="TNJDLBMWRJ",Visible=.t., Left=16, Top=211,;
    Alignment=0, Width=107, Height=18,;
    Caption="Impostazioni griglia"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="QGIEGQZXZQ",Visible=.t., Left=16, Top=238,;
    Alignment=0, Width=106, Height=18,;
    Caption="Passo orizzontale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="KDIVUHJKRG",Visible=.t., Left=16, Top=260,;
    Alignment=0, Width=84, Height=18,;
    Caption="Passo verticale"  ;
  , bGlobalFont=.t.

  add object oBox_1_24 as StdBox with uid="PLZKMGPMVB",left=16, top=204, width=352,height=2

  add object oBox_1_36 as StdBox with uid="ANIORHHLFH",left=16, top=285, width=352,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_maskoptions','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
