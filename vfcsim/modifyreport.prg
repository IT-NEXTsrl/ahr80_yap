* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: modifyreport                                                    *
*              Modifica report                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-11                                                      *
* Last revis.: 2014-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,l_pName,l_UsrRight
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tmodifyreport",oParentObject,m.l_pName,m.l_UsrRight)
return(i_retval)

define class tmodifyreport as StdBatch
  * --- Local variables
  l_pName = space(0)
  l_UsrRight = space(0)
  w_cName = space(0)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- * --------------------------------------------------------------------------
    *     * Routine cp_ModifyReport ridefinita rispetto a quella standard CODEPAINTER
    *     * --------------------------------------------------------------------------
    *     * Procedure : cp_ModifyReport
    *     * Ver       : 1.0
    *     * Autore    : Zucchetti Aulla (MS)
    *     * Data      : 01/02/2001
    *     * Agg       : 01/02/2001
    *     * --------------------------------------------------------------------------
    *     * Descrizione
    *     *    1) Utilizza file di risorse RPTOOL.DBF
    *     *    2) Utilizza menu ridotto
    *     *    3) Utilizza finestra modale
    *     *    4) Richiama modify report o modify label in funzione della estensione
    *     *    5) Richiama tool FixLayout
    *     *
    *     * Parametri
    *     *    i_pName: file report
    *     *    l_UsrRight: Diritti Monitor Framework
    *     * --------------------------------------------------------------------------
     private cResource 
 private nIndex 
 private cEstensione 
 private cOldError 
 private wModiReport 
 private nOldArea 
 private oRigResource 
 private oResStatus 
 private cResourcedbf 
 private cResourcefpt 
 private bfound
    if m.l_UsrRight<>5 AND i_MonitorFramework
      * --- Faccio una copia del report
      if !(VARTYPE(i_MonitorFW_NoMsgReport)="L" AND !i_MonitorFW_NoMsgReport)
        cp_ErrorMsg(MSG_MONFW_MSGREPORT)
      endif
      this.w_cName =  ADDBS(tempadhoc()) + JUSTFNAME(m.l_pName)
      * --- Try
      local bErr_04FFBA18
      bErr_04FFBA18=bTrsErr
      this.Try_04FFBA18()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
         *--- Errore copia 
 *RETURN 
      endif
      bTrsErr=bTrsErr or bErr_04FFBA18
      * --- End
    else
      this.w_cName = m.l_pName
    endif
    * --- CRC File FRX e FRT
    LOCAL l_CRCFrx, l_CRCFrt
    * --- Try
    local bErr_0386EA30
    bErr_0386EA30=bTrsErr
    this.Try_0386EA30()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
       l_CRCFrx = "" 
 l_CRCFrt = ""
    endif
    bTrsErr=bTrsErr or bErr_0386EA30
    * --- End
     push key clear 
 push menu _msysmenu 
 oRigResource=SYS(2005) 
 oResStatus=SET("RESOURCE")
    * ---  Chiude FoxUSER e apre copia file di risorse RPTOOL a sola lettura  
     cResource = cHomeDir+"QUERY\RPTOOL.DBF" 
 bfound=cp_fileexist(cResource,.T.)
    if bfound
      * --- sposta i files rptool.dbf e rptool.fpt nella tmp dell'utente
       cResourcedbf=ADDBS(tempadhoc())+"RPTOOL.DBF" 
 cResourcefpt=ADDBS(tempadhoc())+"RPTOOL.FPT" 
 COPY file chomeDir+"QUERY\RPTOOL.DBF" to (cResourcedbf) 
 COPY file chomeDir+"QUERY\RPTOOL.FPT" to (cResourcefpt) 
 set resource to (cResourcedbf)
    endif
    * --- Zucchetti Aulla Inizio - Interfaccia
    LOCAL l_MainMenuVisible
    if i_VisualTheme<>-1
       l_MainMenuVisible=i_MenuToolbar.Visible 
 SET SYSMENU ON 
 i_MenuToolbar.Visible=.F. 
    endif
    * ---  Zucchetti Aulla Fine - Interfaccia  
     set sysmenu to _mfile,_mview,_medit
    * --- Elimino voci menu
    if i_MonitorFramework AND m.l_UsrRight<> 5
       RELEASE BAR _MFI_SAVAS OF _MFILE 
 RELEASE BAR _MFI_SAVE OF _MFILE
      if m.l_UsrRight<>2
        DEFINE BAR _MFI_CLOSE OF _MFILE PROMPT "Save & Close" KEY CTRL+F4
      endif
    endif
    * --- Estensione - aggiunge se non specificata
    this.w_cName = alltrim (this.w_cName)
     cEstensione = JUSTEXT(this.w_cName)
    if EMPTY(cEstensione)
       cEstensione = "FRX"
      this.w_cName = FORCEEXT(this.w_cName,"FRX")
    endif
    * --- Nasconde Toolbar
    if  i_VisualTheme<>-1 AND VARTYPE(i_ThemesManager)="O"
      i_ThemesManager.HideToolbar()
    else
      if type("oDesktopbar.name")="C"
         oDesktopbar.Hide()
      endif
      oCPToolbar.Hide()
    endif
    * --- Visualizza Report Controls e Report Designer
    if wexist("Report Controls")
      show window "Report Controls" 
    endif
    if wexist("Report Designer")
      show window "Report Designer"
    endif
    * --- Definizione finestra
     zoom window (_screen.name) max 
 define window "dwModiReport" ; 
 title "REPORT " + Upper(alltrim(this.w_cName)) ; 
 System close GROW NOFLOAT name wModiReport ; 
 from 0,0 to Wrow(""),Wcols("") 
 wModiReport.AutoCenter=.t. 
 wModiReport.WindowType=1 && Modal 
 wModiReport.AlwaysOnTop=.t. 
 zoom window dwModiReport max 
 oldkey=ON ("KEY","ALT+F2") 
 ON KEY LABEL ALT+F2 gsar_klt() 
 
    * ---   * Ripristina gestione errore
    *       * Se Vfp8 infatti con la gestione errori (on error)
    *       * eventuali errori in anteprima provocano la chiusura della finestra e
    *       * del report
    #if Not "09.00"$VERSION(1) 
 cOldError=on("ERROR") 
 on error w_err=1 
 #endif
    * ---   *--- Zucchetti Aulla - Inizio Miltireport
    *       *--- Gestione nuovo Engine report VFP9.0
     l_bExitModify=.t.
    do while l_bExitModify OR WEXIST(wModiReport.Caption)
      if cEstensione=".LBX"
        modi label (this.w_cName) window dwModiReport NOENVIRONMENT
      else
        modi report (this.w_cName) window dwModiReport NOENVIRONMENT SAVE
      endif
      * --- Se esiste il browse del cursore __TMP__ ciclo all'infinito 
      do while WEXIST(wModiReport.Caption) OR WEXIST(CP_TRANSLATE(MSG_VIEW_QUERY)) 
         DOEVENTS FORCE 
 l_bExitModify=.t.
      enddo
       l_bExitModify=.F.
    enddo
    if WEXIST("gsar_klt")
      RELEASE WINDOWS gsar_klt
    endif
    RELEASE WINDOWS dwModiReport 
    * --- Zucchetti Aulla - Fine Miltireport
    *     Visualizza Toolbar
    if i_VisualTheme<>-1 AND VARTYPE(i_ThemesManager)="O"
      i_ThemesManager.ShowToolbar()
      if type("oDesktopbar.name")="C"
        odesktopbar.SetAfterCpToolbar()
      endif
    else
       oCPToolbar.show()
      if type("oDesktopbar.name")="C"
         oDesktopbar.show()
      endif
    endif
    * --- Zucchetti Aulla Inizio - Interfaccia
    if  i_VisualTheme<>-1
       SET SYSMENU OFF 
 i_MenuToolbar.Visible = l_MainMenuVisible
    endif
    * --- Zucchetti Aulla Fine - Interfaccia
    *       * Impostazioni layout
    *       *private appmes
    *       *appmes="Associo alla stampa le caratteristiche del driver"+chr(13)
    *       *appmes=appmes+"della stampante predefinita?"+chr(13)
    *       *appmes=appmes+"(es. numero cassetto, formato del foglio, ecc.)"+chr(13)+chr(13)
    *       *appmes=appmes+"(Consigliati:"+chr(13)
    *       *appmes=appmes+"(SI - Se si utilizza il report sempre sulla solita stampante)"+chr(13)
    *       *appmes=appmes+"(NO - Se si utilizza il report su diverse stampanti)"+chr(13) 
    *       *--- Controllo CRC del file report per sapere se il file � stato modificato, in caso affermativo do il msg
    if EMPTY(l_CRCFrx) OR EMPTY(l_CRCFrt) OR (l_CRCFrx <> SYS(2007, FILETOSTR(FORCEEXT(this.w_cName, "FRX")), -1, 1) OR l_CRCFrt <> SYS(2007, FILETOSTR(FORCEEXT(this.w_cName, "FRT")), -1, 1))
      if !cp_yesno(cp_MsgFormat(MSG_COMBINE_TO_PRINT_DRIVER_FEATURES_OF_PREDEFINED_PRINTER_QP_1,chr(13))+;
         cp_MsgFormat(MSG_COMBINE_TO_PRINT_DRIVER_FEATURES_OF_PREDEFINED_PRINTER_QP_2,chr(13)),"?",.F.) 
 do FixLayout with this,this.w_cName
      endif
      * --- Monitor Framework
      if i_MonitorFramework AND m.l_UsrRight<> 5
        SaveRepMonFW(this,m.l_pName, this.w_cName)
      endif
    endif
    * --- Chiude finestre e toolbar
    if wexist("Report Controls")
      hide window "Report Controls"
    endif
    if wexist("Report Designer")
      hide window "Report Designer"
    endif
    * --- * Ripristina FoxUSER in locale (vedi CONFIG.FPW)
    set resource off && Non salva modifiche al file di resource corrente
    if  bfound
       delete file &cResourcedbf 
 delete file &cResourcefpt
    endif
    if oResStatus="ON"
      * --- riprstino il file di resource originario
      set resource to (oRigResource)
    endif
    * ---   * Ripristina gestione errore
    *       * Se non Vfp8 
    #if Not "09.00"$VERSION(1) 
 on error &cOldError 
 #endif
     pop menu _msysmenu 
 pop key 
 ON KEY LABEL ALT+F2 &oldkey 
 return
  endproc
  proc Try_04FFBA18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
     COPY FILE FORCEEXT(m.l_pName, "FRX") TO FORCEEXT(this.w_cName, "FRX") 
 COPY FILE FORCEEXT(m.l_pName, "FRT") TO FORCEEXT(this.w_cName, "FRT")
    return
  proc Try_0386EA30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
     l_CRCFrx = SYS(2007, FILETOSTR(FORCEEXT(this.w_cName, "FRX")), -1, 1) 
 l_CRCFrt = SYS(2007, FILETOSTR(FORCEEXT(this.w_cName, "FRT")), -1, 1)
    return


  proc Init(oParentObject,l_pName,l_UsrRight)
    this.l_pName=l_pName
    this.l_UsrRight=l_UsrRight
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="l_pName,l_UsrRight"
endproc
