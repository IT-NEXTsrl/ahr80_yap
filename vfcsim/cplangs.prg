* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cplangs                                                         *
*              Lingue                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-16                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcplangs"))

* --- Class definition
define class tcplangs as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 383
  Height = 96+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=165970787
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  cplangs_IDX = 0
  cFile = "cplangs"
  cKeySelect = "code"
  cKeyWhere  = "code=this.w_code"
  cKeyWhereODBC = '"code="+cp_ToStrODBC(this.w_code)';

  cKeyWhereODBCqualified = '"cplangs.code="+cp_ToStrODBC(this.w_code)';

  cPrg = "cplangs"
  cComment = "Lingue"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_code = space(3)
  w_name = space(30)
  w_datalanguage = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'cplangs','cplangs')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcplangsPag1","cplangs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Lingua")
      .Pages(1).HelpContextID = 7366719
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.ocode_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- cplangs
    * --- Translate interface...
    this.parent.cComment=cp_Translate(MSG_LANGUAGES)
    this.Pages(1).Caption=cp_Translate(MSG_LANGUAGE)
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='cplangs'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.cplangs_IDX,5],7]
    this.nPostItConn=i_TableProp[this.cplangs_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_code = NVL(code,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from cplangs where code=KeySet.code
    *
    i_nConn = i_TableProp[this.cplangs_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('cplangs')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "cplangs.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' cplangs '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'code',this.w_code  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_code = NVL(code,space(3))
        .w_name = NVL(name,space(30))
        .w_datalanguage = NVL(datalanguage,space(1))
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(cp_translate(MSG_TRANSLATIONS_TOOLTIP_CODE))
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(cp_translate(MSG_LANGUAGE)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_DESCRIPTION))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_DESCRIPTION)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_TRANSLATIONS_TOOLTIP_ACTIVE))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(cp_translate(MSG_TRANSLATIONS_DATA))
        cp_LoadRecExtFlds(this,'cplangs')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_code = space(3)
      .w_name = space(30)
      .w_datalanguage = space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(cp_translate(MSG_TRANSLATIONS_TOOLTIP_CODE))
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(cp_translate(MSG_LANGUAGE)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_DESCRIPTION))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_DESCRIPTION)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_TRANSLATIONS_TOOLTIP_ACTIVE))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(cp_translate(MSG_TRANSLATIONS_DATA))
      endif
    endwith
    cp_BlankRecExtFlds(this,'cplangs')
    this.DoRTCalc(1,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.ocode_1_2.enabled = i_bVal
      .Page1.oPag.oname_1_4.enabled = i_bVal
      .Page1.oPag.odatalanguage_1_5.enabled = i_bVal
      .Page1.oPag.oObj_1_6.enabled = i_bVal
      .Page1.oPag.oObj_1_7.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oObj_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_10.enabled = i_bVal
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.ocode_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.ocode_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'cplangs',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.cplangs_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_code,"code",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_name,"name",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_datalanguage,"datalanguage",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.cplangs_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
    i_lTable = "cplangs"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.cplangs_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.cplangs_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.cplangs_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into cplangs
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'cplangs')
        i_extval=cp_InsertValODBCExtFlds(this,'cplangs')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(code,name,datalanguage "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_code)+;
                  ","+cp_ToStrODBC(this.w_name)+;
                  ","+cp_ToStrODBC(this.w_datalanguage)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'cplangs')
        i_extval=cp_InsertValVFPExtFlds(this,'cplangs')
        cp_CheckDeletedKey(i_cTable,0,'code',this.w_code)
        INSERT INTO (i_cTable);
              (code,name,datalanguage  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_code;
                  ,this.w_name;
                  ,this.w_datalanguage;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.cplangs_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.cplangs_IDX,i_nConn)
      *
      * update cplangs
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'cplangs')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " name="+cp_ToStrODBC(this.w_name)+;
             ",datalanguage="+cp_ToStrODBC(this.w_datalanguage)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'cplangs')
        i_cWhere = cp_PKFox(i_cTable  ,'code',this.w_code  )
        UPDATE (i_cTable) SET;
              name=this.w_name;
             ,datalanguage=this.w_datalanguage;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.cplangs_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.cplangs_IDX,i_nConn)
      *
      * delete cplangs
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'code',this.w_code  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.cplangs_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(cp_translate(MSG_TRANSLATIONS_TOOLTIP_CODE))
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(cp_translate(MSG_LANGUAGE)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_DESCRIPTION))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_DESCRIPTION)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_TRANSLATIONS_TOOLTIP_ACTIVE))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(cp_translate(MSG_TRANSLATIONS_DATA))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(cp_translate(MSG_TRANSLATIONS_TOOLTIP_CODE))
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(cp_translate(MSG_LANGUAGE)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_DESCRIPTION))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(cp_translate(MSG_DESCRIPTION)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_TRANSLATIONS_TOOLTIP_ACTIVE))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(cp_translate(MSG_TRANSLATIONS_DATA))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.odatalanguage_1_5.visible=!this.oPgFrm.Page1.oPag.odatalanguage_1_5.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.ocode_1_2.value==this.w_code)
      this.oPgFrm.Page1.oPag.ocode_1_2.value=this.w_code
    endif
    if not(this.oPgFrm.Page1.oPag.oname_1_4.value==this.w_name)
      this.oPgFrm.Page1.oPag.oname_1_4.value=this.w_name
    endif
    if not(this.oPgFrm.Page1.oPag.odatalanguage_1_5.RadioValue()==this.w_datalanguage)
      this.oPgFrm.Page1.oPag.odatalanguage_1_5.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'cplangs')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcplangsPag1 as StdContainer
  Width  = 379
  height = 96
  stdWidth  = 379
  stdheight = 96
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object ocode_1_2 as StdField with uid="JDPNPMKTMI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_code", cQueryName = "code",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 166411958,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=141, Top=12, InputMask=replicate('X',3)

  add object oname_1_4 as StdField with uid="EUUQBJLLBI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_name", cQueryName = "name",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 166414049,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=141, Top=36, InputMask=replicate('X',30)

  add object odatalanguage_1_5 as StdCheck with uid="OCJOUXTUFG",rtseq=3,rtrep=.f.,left=143, top=61, caption="Traduzione del dato",;
    HelpContextID = 242076508,;
    cFormVar="w_datalanguage", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func odatalanguage_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func odatalanguage_1_5.GetRadio()
    this.Parent.oContained.w_datalanguage = this.RadioValue()
    return .t.
  endfunc

  func odatalanguage_1_5.SetRadio()
    this.Parent.oContained.w_datalanguage=trim(this.Parent.oContained.w_datalanguage)
    this.value = ;
      iif(this.Parent.oContained.w_datalanguage=='S',1,;
      0)
  endfunc

  func odatalanguage_1_5.mHide()
    with this.Parent.oContained
      return (empty(CP_DBTYPE) or CP_DBTYPE='VFP')
    endwith
  endfunc

  func odatalanguage_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_datalanguage<>'S')
         bRes=(cp_WarningMsg(thisform.msgFmt(""+MSG_TRANSLATIONS_WARNING_ACTIVE+"")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc


  add object oObj_1_6 as cp_setobjprop with uid="SKPUZOAYUU",left=2, top=108, width=321,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_code",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 26100690


  add object oObj_1_7 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=2, top=134, width=321,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Lingua:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 26100690


  add object oObj_1_8 as cp_setobjprop with uid="PVHCXHZQRC",left=2, top=178, width=321,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_name",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 26100690


  add object oObj_1_9 as cp_setCtrlObjprop with uid="EYUZRJSFMA",left=2, top=204, width=321,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Descrizione:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 26100690


  add object oObj_1_10 as cp_setobjprop with uid="JIYJTKVSZC",left=2, top=247, width=321,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_datalanguage",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 26100690


  add object oObj_1_11 as cp_setobjprop with uid="ZDVHIOYSCI",left=2, top=273, width=321,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_datalanguage",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 26100690

  add object oStr_1_1 as StdString with uid="DKKCRKJQJS",Visible=.t., Left=22, Top=12,;
    Alignment=1, Width=116, Height=18,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="AWKJMMDCSE",Visible=.t., Left=5, Top=36,;
    Alignment=1, Width=133, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cplangs','cplangs','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".code=cplangs.code";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
