* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_addfilter                                                    *
*              Editor filtri                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-11                                                      *
* Last revis.: 2012-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- cp_addfilter
PARAMETERS pCurName, pFltExp, pField, pParent
* --- Fine Area Manuale
return(createobject("tcp_addfilter"))

* --- Class definition
define class tcp_addfilter as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 797
  Height = 274+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-08"
  HelpContextID=55442673
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  FILTERSB_IDX = 0
  cFile = "FILTERSB"
  cKeySelect = "FBCURNAM"
  cKeyWhere  = "FBCURNAM=this.w_FBCURNAM"
  cKeyDetail  = "FBCURNAM=this.w_FBCURNAM"
  cKeyWhereODBC = '"FBCURNAM="+cp_ToStrODBC(this.w_FBCURNAM)';

  cKeyDetailWhereODBC = '"FBCURNAM="+cp_ToStrODBC(this.w_FBCURNAM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"FILTERSB.FBCURNAM="+cp_ToStrODBC(this.w_FBCURNAM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'FILTERSB.CPROWNUM '
  cPrg = "cp_addfilter"
  cComment = "Editor filtri"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  windowtype = 1
  minbutton = .f.
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FBCURNAM = space(15)
  w_FBFLDNAM = space(254)
  o_FBFLDNAM = space(254)
  w_FBCONDIT = space(10)
  o_FBCONDIT = space(10)
  w_FBVALFLT = space(254)
  w_FBCONDAO = space(3)
  w_FBOLDEXP = space(254)
  w_FBOBJECT = space(10)
  w_FBPARENT = .F.
  w_FBMANFLT = space(0)
  w_FBTABLE1 = space(20)
  w_NRELEXT1 = 0
  w_FBTABLE2 = space(20)
  w_NRELEXT2 = 0
  w_IDTABKEY = space(20)
  w_IDFLDKEY = space(20)
  w_IDVALATT = space(254)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_addfilter
  pCurName = ' '
  pFltExp = ' '
  pField = ' '
  pParent = .null.
  bQuitting = .F.
  
  Proc EcpQuit()
     local i_oldFunc
     i_oldFunc = This.cFunction
     this.bQuitting = .T.
     DoDefault()
     If i_oldFunc='Load'
       This.cFunction='Query'
       This.bUpdated = .F.
       This.EcpQuit()
     EndIf
  EndProc
  
  Proc EcpSave()
     this.NotifyEvent('SaveAndClose')
     *Per evitare msg "Abbandoni Modifiche.."
     if pemstatus(pParent,"bSaveWhere",5)
       pParent.bSaveWhere=.t.
     endif
     This.cFunction='Query'
     this.bUpdated = .F.
     this.bQuitting = .T.
     This.EcpQuit()
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'FILTERSB','cp_addfilter')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_addfilterPag1","cp_addfilter",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Editor filtro")
      .Pages(1).HelpContextID = 128351275
      .Pages(2).addobject("oPag","tcp_addfilterPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Modifica manuale filtro")
      .Pages(2).HelpContextID = 146552130
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- cp_addfilter
    this.Parent.autocenter=.t.
    
    With This.Parent
       .pCurName = pCurName
       .pFltExp = pFltExp
       .pField = pField
       .pParent = pParent
    EndWith
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='FILTERSB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.FILTERSB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.FILTERSB_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_FBCURNAM = NVL(FBCURNAM,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from FILTERSB where FBCURNAM=KeySet.FBCURNAM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.FILTERSB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2],this.bLoadRecFilter,this.FILTERSB_IDX,"cp_addfilter")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('FILTERSB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "FILTERSB.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' FILTERSB '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FBCURNAM',this.w_FBCURNAM  )
      select * from (i_cTable) FILTERSB where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FBOLDEXP = .pFltExp
        .w_FBOBJECT = .pField
        .w_FBPARENT = .pParent
        .w_FBMANFLT = space(0)
        .w_FBTABLE1 = IIF(TYPE(".w_FBPARENT.w_TABLE1")="C", .w_FBPARENT.w_TABLE1, SPACE(20))
        .w_NRELEXT1 = IIF(EMPTY(.w_FBTABLE1), 0, i_DCX.GetFKCount(.w_FBTABLE1))
        .w_FBTABLE2 = IIF(TYPE(".w_FBPARENT.w_TABLE2")="C", .w_FBPARENT.w_TABLE2, SPACE(20))
        .w_NRELEXT2 = IIF(EMPTY(.w_FBTABLE2), 0, i_DCX.GetFKCount(.w_FBTABLE2))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FBCURNAM = NVL(FBCURNAM,space(15))
        cp_LoadRecExtFlds(this,'FILTERSB')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_IDTABKEY = space(20)
          .w_IDFLDKEY = space(20)
          .w_IDVALATT = space(254)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_FBFLDNAM = NVL(FBFLDNAM,space(254))
          .w_FBCONDIT = NVL(FBCONDIT,space(10))
          .w_FBVALFLT = NVL(FBVALFLT,space(254))
          .w_FBCONDAO = NVL(FBCONDAO,space(3))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_5.enabled = .oPgFrm.Page1.oPag.oBtn_2_5.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_6.enabled = .oPgFrm.Page1.oPag.oBtn_2_6.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_7.enabled = .oPgFrm.Page1.oPag.oBtn_2_7.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_6.enabled = .oPgFrm.Page1.oPag.oBtn_1_6.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_7.enabled = .oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        .oPgFrm.Page2.oPag.oBtn_4_2.enabled = .oPgFrm.Page2.oPag.oBtn_4_2.mCond()
        .oPgFrm.Page2.oPag.oBtn_4_3.enabled = .oPgFrm.Page2.oPag.oBtn_4_3.mCond()
        .oPgFrm.Page2.oPag.oBtn_4_4.enabled = .oPgFrm.Page2.oPag.oBtn_4_4.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_FBCURNAM=space(15)
      .w_FBFLDNAM=space(254)
      .w_FBCONDIT=space(10)
      .w_FBVALFLT=space(254)
      .w_FBCONDAO=space(3)
      .w_FBOLDEXP=space(254)
      .w_FBOBJECT=space(10)
      .w_FBPARENT=.f.
      .w_FBMANFLT=space(0)
      .w_FBTABLE1=space(20)
      .w_NRELEXT1=0
      .w_FBTABLE2=space(20)
      .w_NRELEXT2=0
      .w_IDTABKEY=space(20)
      .w_IDFLDKEY=space(20)
      .w_IDVALATT=space(254)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FBCURNAM = .pCurName
        .DoRTCalc(2,2,.f.)
        .w_FBCONDIT = ' '
        .w_FBVALFLT = IIF(EMPTY(.w_FBCONDIT), "", .w_FBVALFLT )
        .w_FBCONDAO = 'AND'
        .w_FBOLDEXP = .pFltExp
        .w_FBOBJECT = .pField
        .w_FBPARENT = .pParent
        .DoRTCalc(9,9,.f.)
        .w_FBTABLE1 = IIF(TYPE(".w_FBPARENT.w_TABLE1")="C", .w_FBPARENT.w_TABLE1, SPACE(20))
        .w_NRELEXT1 = IIF(EMPTY(.w_FBTABLE1), 0, i_DCX.GetFKCount(.w_FBTABLE1))
        .w_FBTABLE2 = IIF(TYPE(".w_FBPARENT.w_TABLE2")="C", .w_FBPARENT.w_TABLE2, SPACE(20))
        .w_NRELEXT2 = IIF(EMPTY(.w_FBTABLE2), 0, i_DCX.GetFKCount(.w_FBTABLE2))
      endif
    endwith
    cp_BlankRecExtFlds(this,'FILTERSB')
    this.DoRTCalc(14,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_5.enabled = this.oPgFrm.Page1.oPag.oBtn_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_6.enabled = this.oPgFrm.Page1.oPag.oBtn_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_7.enabled = this.oPgFrm.Page1.oPag.oBtn_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_2.enabled = this.oPgFrm.Page2.oPag.oBtn_4_2.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_3.enabled = this.oPgFrm.Page2.oPag.oBtn_4_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_4.enabled = this.oPgFrm.Page2.oPag.oBtn_4_4.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- cp_addfilter
    If not this.bQuitting
       This.EcpLoad()
    EndIf
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page2.oPag.oFBMANFLT_4_1.enabled = i_bVal
      .Page1.oPag.oBtn_2_5.enabled = .Page1.oPag.oBtn_2_5.mCond()
      .Page1.oPag.oBtn_2_6.enabled = .Page1.oPag.oBtn_2_6.mCond()
      .Page1.oPag.oBtn_2_7.enabled = .Page1.oPag.oBtn_2_7.mCond()
      .Page1.oPag.oBtn_1_6.enabled = .Page1.oPag.oBtn_1_6.mCond()
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      .Page2.oPag.oBtn_4_2.enabled = .Page2.oPag.oBtn_4_2.mCond()
      .Page2.oPag.oBtn_4_3.enabled = .Page2.oPag.oBtn_4_3.mCond()
      .Page2.oPag.oBtn_4_4.enabled = .Page2.oPag.oBtn_4_4.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'FILTERSB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- cp_addfilter
    * disabilito il tab elenco
    this.oPgFrm.Pages(this.oPgFrm.PageCount).enabled=.f.
    this.oPgFrm.Pages(this.oPgFrm.PageCount).Caption=""
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.FILTERSB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FBCURNAM,"FBCURNAM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FILTERSB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
    i_lTable = "FILTERSB"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.FILTERSB_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_FBFLDNAM C(254);
      ,t_FBCONDIT N(3);
      ,t_FBVALFLT C(254);
      ,t_FBCONDAO N(3);
      ,CPROWNUM N(10);
      ,t_IDTABKEY C(20);
      ,t_IDFLDKEY C(20);
      ,t_IDVALATT C(254);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tcp_addfilterbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1.controlsource=this.cTrsName+'.t_FBFLDNAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.controlsource=this.cTrsName+'.t_FBCONDIT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.controlsource=this.cTrsName+'.t_FBVALFLT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.controlsource=this.cTrsName+'.t_FBCONDAO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(275)
    this.AddVLine(363)
    this.AddVLine(707)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FILTERSB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.FILTERSB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
      *
      * insert into FILTERSB
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'FILTERSB')
        i_extval=cp_InsertValODBCExtFlds(this,'FILTERSB')
        i_cFldBody=" "+;
                  "(FBCURNAM,FBFLDNAM,FBCONDIT,FBVALFLT,FBCONDAO,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_FBCURNAM)+","+cp_ToStrODBC(this.w_FBFLDNAM)+","+cp_ToStrODBC(this.w_FBCONDIT)+","+cp_ToStrODBC(this.w_FBVALFLT)+","+cp_ToStrODBC(this.w_FBCONDAO)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'FILTERSB')
        i_extval=cp_InsertValVFPExtFlds(this,'FILTERSB')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'FBCURNAM',this.w_FBCURNAM)
        INSERT INTO (i_cTable) (;
                   FBCURNAM;
                  ,FBFLDNAM;
                  ,FBCONDIT;
                  ,FBVALFLT;
                  ,FBCONDAO;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_FBCURNAM;
                  ,this.w_FBFLDNAM;
                  ,this.w_FBCONDIT;
                  ,this.w_FBVALFLT;
                  ,this.w_FBCONDAO;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.FILTERSB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_FBFLDNAM))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'FILTERSB')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'FILTERSB')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_FBFLDNAM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update FILTERSB
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'FILTERSB')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " FBFLDNAM="+cp_ToStrODBC(this.w_FBFLDNAM)+;
                     ",FBCONDIT="+cp_ToStrODBC(this.w_FBCONDIT)+;
                     ",FBVALFLT="+cp_ToStrODBC(this.w_FBVALFLT)+;
                     ",FBCONDAO="+cp_ToStrODBC(this.w_FBCONDAO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'FILTERSB')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      FBFLDNAM=this.w_FBFLDNAM;
                     ,FBCONDIT=this.w_FBCONDIT;
                     ,FBVALFLT=this.w_FBVALFLT;
                     ,FBCONDAO=this.w_FBCONDAO;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.FILTERSB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_FBFLDNAM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete FILTERSB
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_FBFLDNAM))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.FILTERSB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,3,.t.)
        if .o_FBCONDIT<>.w_FBCONDIT
          .w_FBVALFLT = IIF(EMPTY(.w_FBCONDIT), "", .w_FBVALFLT )
        endif
        if .o_FBFLDNAM<>.w_FBFLDNAM
          .Calculate_ELAKRCDPFS()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_IDTABKEY with this.w_IDTABKEY
      replace t_IDFLDKEY with this.w_IDFLDKEY
      replace t_IDVALATT with this.w_IDVALATT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_LRHIKWJFWE()
    with this
          * --- Trasforma il filtro da parametro a righe e inizializza tabella temporanea zoom
          cp_AddFilterR(this;
              ,'NEW_RECORD';
             )
    endwith
  endproc
  proc Calculate_WOIVNPSMAC()
    with this
          * --- Elimina tabella temporanea
          cp_AddFilterR(this;
              ,"CLOSE_MASK";
             )
    endwith
  endproc
  proc Calculate_FUOAQULCGV()
    with this
          * --- Salvataggio delle modifiche nell'oggetto della gestione chiamante
          cp_AddFilterR(this;
              ,"SAVEANDCLS";
             )
    endwith
  endproc
  proc Calculate_JBCOLAHMPQ()
    with this
          * --- Riporta le righe dell'editor nel campo per le modifiche manuali
          cp_AddFilterR(this;
              ,"EDITOR2MAN";
             )
    endwith
  endproc
  proc Calculate_ELAKRCDPFS()
    with this
          * --- SETFKTABLE
          cp_addfilterr(this;
              ,"SETFKTABLE";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFBVALFLT_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFBVALFLT_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oBtn_2_10.enabled =this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oBtn_2_10.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(1=2 OR 1=3)
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Modifica manuale filtro"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oBtn_2_10.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oBtn_2_10.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("New record")
          .Calculate_LRHIKWJFWE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_WOIVNPSMAC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("SaveAndClose")
          .Calculate_FUOAQULCGV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_JBCOLAHMPQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page2.oPag.oFBMANFLT_4_1.value==this.w_FBMANFLT)
      this.oPgFrm.Page2.oPag.oFBMANFLT_4_1.value=this.w_FBMANFLT
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1.value==this.w_FBFLDNAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1.value=this.w_FBFLDNAM
      replace t_FBFLDNAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.RadioValue()==this.w_FBCONDIT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.SetRadio()
      replace t_FBCONDIT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.value==this.w_FBVALFLT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.value=this.w_FBVALFLT
      replace t_FBVALFLT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.RadioValue()==this.w_FBCONDAO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.SetRadio()
      replace t_FBCONDAO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'FILTERSB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_FBFLDNAM)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_FBFLDNAM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FBFLDNAM = this.w_FBFLDNAM
    this.o_FBCONDIT = this.w_FBCONDIT
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_FBFLDNAM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_FBFLDNAM=space(254)
      .w_FBCONDIT=space(10)
      .w_FBVALFLT=space(254)
      .w_FBCONDAO=space(3)
      .w_IDTABKEY=space(20)
      .w_IDFLDKEY=space(20)
      .w_IDVALATT=space(254)
      .DoRTCalc(1,2,.f.)
        .w_FBCONDIT = ' '
        .w_FBVALFLT = IIF(EMPTY(.w_FBCONDIT), "", .w_FBVALFLT )
        .w_FBCONDAO = 'AND'
    endwith
    this.DoRTCalc(6,16,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_FBFLDNAM = t_FBFLDNAM
    this.w_FBCONDIT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.RadioValue(.t.)
    this.w_FBVALFLT = t_FBVALFLT
    this.w_FBCONDAO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.RadioValue(.t.)
    this.w_IDTABKEY = t_IDTABKEY
    this.w_IDFLDKEY = t_IDFLDKEY
    this.w_IDVALATT = t_IDVALATT
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_FBFLDNAM with this.w_FBFLDNAM
    replace t_FBCONDIT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.ToRadio()
    replace t_FBVALFLT with this.w_FBVALFLT
    replace t_FBCONDAO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.ToRadio()
    replace t_IDTABKEY with this.w_IDTABKEY
    replace t_IDFLDKEY with this.w_IDFLDKEY
    replace t_IDVALATT with this.w_IDVALATT
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tcp_addfilterPag1 as StdContainer
  Width  = 793
  height = 274
  stdWidth  = 793
  stdheight = 274
  resizeXpos=484
  resizeYpos=155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=9, width=780,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="FBFLDNAM",Label1="Nome campo",Field2="FBCONDIT",Label2="Condizione",Field3="FBVALFLT",Label3="Valore",Field4="FBCONDAO",Label4="And/Or",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 110925273


  add object oBtn_1_6 as StdButton with uid="ZGAJYUAGXG",left=672, top=226, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare le modifiche effettuate";
    , HelpContextID = 54593941;
    , caption='Sa\<lva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="XOOSNRSXLS",left=724, top=226, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare le modifiche effettuate";
    , HelpContextID = 254248516;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=28,;
    width=776+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=29,width=775+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_5 as StdButton with uid="EAGCZXLWAI",width=48,height=45,;
   left=6, top=226,;
    CpPicture="BMP\UP.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per spostare in alto di una riga la condizione selezionata";
    , HelpContextID = 121701135;
    , caption='\<Su';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        cp_AddFilterR(this.Parent.oContained,"FILTER_UP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBtn_2_6 as StdButton with uid="CRHSFBPJWF",width=48,height=45,;
   left=60, top=226,;
    CpPicture="BMP\DOWN.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per spostare in basso di una riga la condizione selezionata";
    , HelpContextID = 92208369;
    , caption='\<Gi�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      with this.Parent.oContained
        cp_AddFilterR(this.Parent.oContained,"FILTERDOWN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBtn_2_7 as StdButton with uid="LEHLVRFBAF",width=48,height=45,;
   left=114, top=226,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per testare il filtro";
    , HelpContextID = 145685629;
    , caption='\<Test';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_7.Click()
      with this.Parent.oContained
        cp_AddFilterR(this.Parent.oContained,"FILTERTEST")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tcp_addfilterPag2 as StdContainer
    Width  = 793
    height = 274
    stdWidth  = 793
    stdheight = 274
  resizeXpos=594
  resizeYpos=184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFBMANFLT_4_1 as StdMemo with uid="XKVOFEBLAU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FBMANFLT", cQueryName = "FBMANFLT",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 63573552,;
   bGlobalFont=.t.,;
    Height=215, Width=783, Left=6, Top=8


  add object oBtn_4_2 as StdButton with uid="IDGCXLPLNB",left=9, top=226, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per importare il filtro modificato manualmente nelle righe dell'editor";
    , HelpContextID = 178160525;
    , caption='\<Editor';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_2.Click()
      with this.Parent.oContained
        cp_AddFilterR(this.Parent.oContained,"MAN2EDITOR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_3 as StdButton with uid="FWJFZEEEGM",left=672, top=226, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per salvare le modifiche effettuate";
    , HelpContextID = 54593941;
    , caption='Sa\<lva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_4 as StdButton with uid="LFCTXPRHIY",left=724, top=226, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per annullare le modifiche effettuate";
    , HelpContextID = 254248516;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

* --- Defining Body row
define class tcp_addfilterBodyRow as CPBodyRowCnt
  Width=766
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oFBFLDNAM_2_1 as StdTrsField with uid="FOKQNQXLHB",rtseq=2,rtrep=.t.,;
    cFormVar="w_FBFLDNAM",value=space(254),;
    HelpContextID = 205363813,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=264, Left=-2, Top=0, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oFBFLDNAM_2_1.mZoom
      with this.Parent.oContained
        this.Parent.oContained.w_FBFLDNAM = cp_AddFilterR(this.Parent.oContained,"ZOOMFIELDS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFBCONDIT_2_2 as StdTrsCombo with uid="JYJCEVRFSR",rtrep=.t.,;
    cFormVar="w_FBCONDIT", RowSource=""+","+"=,"+">=,"+">,"+"<>,"+"<,"+"<=,"+"like,"+"not like,"+"in,"+"not in,"+"exist,"+"not exist,"+"between" , ;
    HelpContextID = 63560766,;
    Height=22, Width=83, Left=267, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oFBCONDIT_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FBCONDIT,&i_cF..t_FBCONDIT),this.value)
    return(iif(xVal =1,' ',;
    iif(xVal =2,'=',;
    iif(xVal =3,'>=',;
    iif(xVal =4,'>',;
    iif(xVal =5,'<>',;
    iif(xVal =6,'<',;
    iif(xVal =7,'<=',;
    iif(xVal =8,'like',;
    iif(xVal =9,'not like',;
    iif(xVal =10,'in',;
    iif(xVal =11,'not in',;
    iif(xVal =12,'exist',;
    iif(xVal =13,'not exist',;
    iif(xVal =14,'between',;
    space(10))))))))))))))))
  endfunc
  func oFBCONDIT_2_2.GetRadio()
    this.Parent.oContained.w_FBCONDIT = this.RadioValue()
    return .t.
  endfunc

  func oFBCONDIT_2_2.ToRadio()
    this.Parent.oContained.w_FBCONDIT=trim(this.Parent.oContained.w_FBCONDIT)
    return(;
      iif(this.Parent.oContained.w_FBCONDIT=='',1,;
      iif(this.Parent.oContained.w_FBCONDIT=='=',2,;
      iif(this.Parent.oContained.w_FBCONDIT=='>=',3,;
      iif(this.Parent.oContained.w_FBCONDIT=='>',4,;
      iif(this.Parent.oContained.w_FBCONDIT=='<>',5,;
      iif(this.Parent.oContained.w_FBCONDIT=='<',6,;
      iif(this.Parent.oContained.w_FBCONDIT=='<=',7,;
      iif(this.Parent.oContained.w_FBCONDIT=='like',8,;
      iif(this.Parent.oContained.w_FBCONDIT=='not like',9,;
      iif(this.Parent.oContained.w_FBCONDIT=='in',10,;
      iif(this.Parent.oContained.w_FBCONDIT=='not in',11,;
      iif(this.Parent.oContained.w_FBCONDIT=='exist',12,;
      iif(this.Parent.oContained.w_FBCONDIT=='not exist',13,;
      iif(this.Parent.oContained.w_FBCONDIT=='between',14,;
      0)))))))))))))))
  endfunc

  func oFBCONDIT_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFBVALFLT_2_3 as StdTrsField with uid="ERNMTUVHQY",rtseq=4,rtrep=.t.,;
    cFormVar="w_FBVALFLT",value=space(254),;
    HelpContextID = 204861936,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=326, Left=355, Top=0, InputMask=replicate('X',254), bHasZoom = .t. 

  func oFBVALFLT_2_3.mCond()
    with this.Parent.oContained
      return (!EMPTY(.w_FBCONDIT))
    endwith
  endfunc

  proc oFBVALFLT_2_3.mZoom
      with this.Parent.oContained
        this.Parent.oContained.w_FBVALFLT = cp_AddFilterR(this.Parent.oContained,"ZOOMFIELDS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFBCONDAO_2_4 as StdTrsCombo with uid="DFIBXKFYUY",rtrep=.t.,;
    cFormVar="w_FBCONDAO", RowSource=""+"AND,"+"OR" , ;
    HelpContextID = 63200318,;
    Height=22, Width=62, Left=699, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oFBCONDAO_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FBCONDAO,&i_cF..t_FBCONDAO),this.value)
    return(iif(xVal =1,'AND',;
    iif(xVal =2,'OR',;
    space(3))))
  endfunc
  func oFBCONDAO_2_4.GetRadio()
    this.Parent.oContained.w_FBCONDAO = this.RadioValue()
    return .t.
  endfunc

  func oFBCONDAO_2_4.ToRadio()
    this.Parent.oContained.w_FBCONDAO=trim(this.Parent.oContained.w_FBCONDAO)
    return(;
      iif(this.Parent.oContained.w_FBCONDAO=='AND',1,;
      iif(this.Parent.oContained.w_FBCONDAO=='OR',2,;
      0)))
  endfunc

  func oFBCONDAO_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oBtn_2_10 as StdButton with uid="LUXVEFXKLO",width=14,height=22,;
   left=680, top=1,;
    CpPicture="arrow.bmp", caption="", nPag=2;
    , ToolTipText = "g_BtnZoomArrow";
    , HelpContextID = 233898767;
    , Width = i_nZBtnWidth, BackColor=Rgb(206,219,255), ImgSize=16, top=0, Height=17;
  , bGlobalFont=.t.

    proc oBtn_2_10.Click()
      with this.Parent.oContained
        cp_addfilterr(this.Parent.oContained,"ZOOMTABLE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_10.mCond()
    with this.Parent.oContained
      return (!EMPTY(.w_FBCONDIT) AND not empty(.w_IDFLDKEY))
    endwith
  endfunc

  func oBtn_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FBTABLE1))
    endwith
   endif
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oFBFLDNAM_2_1.When()
    return(.t.)
  proc oFBFLDNAM_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oFBFLDNAM_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_addfilter','FILTERSB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FBCURNAM=FILTERSB.FBCURNAM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
