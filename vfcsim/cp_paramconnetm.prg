* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_paramconnetm                                                 *
*              Riconnessione Automatica                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-02                                                      *
* Last revis.: 2009-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tcp_paramconnetm")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tccp_paramconnetm")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tccp_paramconnetm")
  return

* --- Class definition
define class tcp_paramconnetm as StdPCForm
  Width  = 469
  Height = 254
  Top    = 5
  Left   = 36
  cComment = "Riconnessione Automatica"
  cPrg = "cp_paramconnetm"
  HelpContextID=46875722
  add object cnt as tccp_paramconnetm
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tscp_paramconnetm as PCContext
  w_RCSERIAL = space(5)
  w_RCAUTORC = space(1)
  w_RCNUMRIC = 0
  w_RCDELAYR = 0
  w_RCCNTIME = 0
  w_RCDEADRQ = space(1)
  w_RCNUMDRQ = 0
  w_RCDELAYD = 0
  w_RCCONMSG = space(1)
  proc Save(oFrom)
    this.w_RCSERIAL = oFrom.w_RCSERIAL
    this.w_RCAUTORC = oFrom.w_RCAUTORC
    this.w_RCNUMRIC = oFrom.w_RCNUMRIC
    this.w_RCDELAYR = oFrom.w_RCDELAYR
    this.w_RCCNTIME = oFrom.w_RCCNTIME
    this.w_RCDEADRQ = oFrom.w_RCDEADRQ
    this.w_RCNUMDRQ = oFrom.w_RCNUMDRQ
    this.w_RCDELAYD = oFrom.w_RCDELAYD
    this.w_RCCONMSG = oFrom.w_RCCONMSG
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_RCSERIAL = this.w_RCSERIAL
    oTo.w_RCAUTORC = this.w_RCAUTORC
    oTo.w_RCNUMRIC = this.w_RCNUMRIC
    oTo.w_RCDELAYR = this.w_RCDELAYR
    oTo.w_RCCNTIME = this.w_RCCNTIME
    oTo.w_RCDEADRQ = this.w_RCDEADRQ
    oTo.w_RCNUMDRQ = this.w_RCNUMDRQ
    oTo.w_RCDELAYD = this.w_RCDELAYD
    oTo.w_RCCONMSG = this.w_RCCONMSG
    PCContext::Load(oTo)
enddefine

define class tccp_paramconnetm as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 469
  Height = 254
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-02"
  HelpContextID=46875722
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  paramric_IDX = 0
  cFile = "paramric"
  cKeySelect = "RCSERIAL"
  cKeyWhere  = "RCSERIAL=this.w_RCSERIAL"
  cKeyWhereODBC = '"RCSERIAL="+cp_ToStrODBC(this.w_RCSERIAL)';

  cKeyWhereODBCqualified = '"paramric.RCSERIAL="+cp_ToStrODBC(this.w_RCSERIAL)';

  cPrg = "cp_paramconnetm"
  cComment = "Riconnessione Automatica"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RCSERIAL = space(5)
  w_RCAUTORC = space(1)
  w_RCNUMRIC = 0
  w_RCDELAYR = 0
  w_RCCNTIME = 0
  w_RCDEADRQ = space(1)
  w_RCNUMDRQ = 0
  w_RCDELAYD = 0
  w_RCCONMSG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_paramconnetmPag1","cp_paramconnetm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 43222762
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRCAUTORC_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='paramric'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.paramric_IDX,5],7]
    this.nPostItConn=i_TableProp[this.paramric_IDX,3]
  return

  procedure NewContext()
    return(createobject('tscp_paramconnetm'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from paramric where RCSERIAL=KeySet.RCSERIAL
    *
    i_nConn = i_TableProp[this.paramric_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('paramric')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "paramric.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' paramric '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RCSERIAL',this.w_RCSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RCSERIAL = NVL(RCSERIAL,space(5))
        .w_RCAUTORC = NVL(RCAUTORC,space(1))
        .w_RCNUMRIC = NVL(RCNUMRIC,0)
        .w_RCDELAYR = NVL(RCDELAYR,0)
        .w_RCCNTIME = NVL(RCCNTIME,0)
        .w_RCDEADRQ = NVL(RCDEADRQ,space(1))
        .w_RCNUMDRQ = NVL(RCNUMDRQ,0)
        .w_RCDELAYD = NVL(RCDELAYD,0)
        .w_RCCONMSG = NVL(RCCONMSG,space(1))
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_AUTORICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_NUMRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_DELAYRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_CONNECTTIMEOUT)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_DEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_NUMDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_DELAYDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_CNF_MSG)
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_NUMRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_DELAYRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_SHOW_CONNECTTIMEOUT)
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_SHOW_DEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_SHOW_NUMDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_SHOW_DELAYDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_SHOW_MSG)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_SHOW_AUTORICONNECT)
        cp_LoadRecExtFlds(this,'paramric')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_RCSERIAL = space(5)
      .w_RCAUTORC = space(1)
      .w_RCNUMRIC = 0
      .w_RCDELAYR = 0
      .w_RCCNTIME = 0
      .w_RCDEADRQ = space(1)
      .w_RCNUMDRQ = 0
      .w_RCDELAYD = 0
      .w_RCCONMSG = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,8,.f.)
        .w_RCCONMSG = 'N'
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_AUTORICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_NUMRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_DELAYRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_CONNECTTIMEOUT)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_DEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_NUMDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_DELAYDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_CNF_MSG)
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_NUMRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_DELAYRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_SHOW_CONNECTTIMEOUT)
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_SHOW_DEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_SHOW_NUMDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_SHOW_DELAYDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_SHOW_MSG)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_SHOW_AUTORICONNECT)
      endif
    endwith
    cp_BlankRecExtFlds(this,'paramric')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oRCAUTORC_1_2.enabled = i_bVal
      .Page1.oPag.oRCNUMRIC_1_3.enabled = i_bVal
      .Page1.oPag.oRCDELAYR_1_5.enabled = i_bVal
      .Page1.oPag.oRCCNTIME_1_7.enabled = i_bVal
      .Page1.oPag.oRCDEADRQ_1_9.enabled = i_bVal
      .Page1.oPag.oRCNUMDRQ_1_10.enabled = i_bVal
      .Page1.oPag.oRCDELAYD_1_12.enabled = i_bVal
      .Page1.oPag.oRCCONMSG_1_14.enabled = i_bVal
      .Page1.oPag.oObj_1_16.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      .Page1.oPag.oObj_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_20.enabled = i_bVal
      .Page1.oPag.oObj_1_21.enabled = i_bVal
      .Page1.oPag.oObj_1_22.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      .Page1.oPag.oObj_1_24.enabled = i_bVal
      .Page1.oPag.oObj_1_25.enabled = i_bVal
      .Page1.oPag.oObj_1_26.enabled = i_bVal
      .Page1.oPag.oObj_1_27.enabled = i_bVal
      .Page1.oPag.oObj_1_28.enabled = i_bVal
      .Page1.oPag.oObj_1_29.enabled = i_bVal
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'paramric',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.paramric_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RCSERIAL,"RCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RCAUTORC,"RCAUTORC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RCNUMRIC,"RCNUMRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RCDELAYR,"RCDELAYR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RCCNTIME,"RCCNTIME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RCDEADRQ,"RCDEADRQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RCNUMDRQ,"RCNUMDRQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RCDELAYD,"RCDELAYD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RCCONMSG,"RCCONMSG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.paramric_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.paramric_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into paramric
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'paramric')
        i_extval=cp_InsertValODBCExtFlds(this,'paramric')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RCSERIAL,RCAUTORC,RCNUMRIC,RCDELAYR,RCCNTIME"+;
                  ",RCDEADRQ,RCNUMDRQ,RCDELAYD,RCCONMSG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RCSERIAL)+;
                  ","+cp_ToStrODBC(this.w_RCAUTORC)+;
                  ","+cp_ToStrODBC(this.w_RCNUMRIC)+;
                  ","+cp_ToStrODBC(this.w_RCDELAYR)+;
                  ","+cp_ToStrODBC(this.w_RCCNTIME)+;
                  ","+cp_ToStrODBC(this.w_RCDEADRQ)+;
                  ","+cp_ToStrODBC(this.w_RCNUMDRQ)+;
                  ","+cp_ToStrODBC(this.w_RCDELAYD)+;
                  ","+cp_ToStrODBC(this.w_RCCONMSG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'paramric')
        i_extval=cp_InsertValVFPExtFlds(this,'paramric')
        cp_CheckDeletedKey(i_cTable,0,'RCSERIAL',this.w_RCSERIAL)
        INSERT INTO (i_cTable);
              (RCSERIAL,RCAUTORC,RCNUMRIC,RCDELAYR,RCCNTIME,RCDEADRQ,RCNUMDRQ,RCDELAYD,RCCONMSG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RCSERIAL;
                  ,this.w_RCAUTORC;
                  ,this.w_RCNUMRIC;
                  ,this.w_RCDELAYR;
                  ,this.w_RCCNTIME;
                  ,this.w_RCDEADRQ;
                  ,this.w_RCNUMDRQ;
                  ,this.w_RCDELAYD;
                  ,this.w_RCCONMSG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.paramric_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.paramric_IDX,i_nConn)
      *
      * update paramric
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'paramric')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RCAUTORC="+cp_ToStrODBC(this.w_RCAUTORC)+;
             ",RCNUMRIC="+cp_ToStrODBC(this.w_RCNUMRIC)+;
             ",RCDELAYR="+cp_ToStrODBC(this.w_RCDELAYR)+;
             ",RCCNTIME="+cp_ToStrODBC(this.w_RCCNTIME)+;
             ",RCDEADRQ="+cp_ToStrODBC(this.w_RCDEADRQ)+;
             ",RCNUMDRQ="+cp_ToStrODBC(this.w_RCNUMDRQ)+;
             ",RCDELAYD="+cp_ToStrODBC(this.w_RCDELAYD)+;
             ",RCCONMSG="+cp_ToStrODBC(this.w_RCCONMSG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'paramric')
        i_cWhere = cp_PKFox(i_cTable  ,'RCSERIAL',this.w_RCSERIAL  )
        UPDATE (i_cTable) SET;
              RCAUTORC=this.w_RCAUTORC;
             ,RCNUMRIC=this.w_RCNUMRIC;
             ,RCDELAYR=this.w_RCDELAYR;
             ,RCCNTIME=this.w_RCCNTIME;
             ,RCDEADRQ=this.w_RCDEADRQ;
             ,RCNUMDRQ=this.w_RCNUMDRQ;
             ,RCDELAYD=this.w_RCDELAYD;
             ,RCCONMSG=this.w_RCCONMSG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.paramric_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.paramric_IDX,i_nConn)
      *
      * delete paramric
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RCSERIAL',this.w_RCSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.paramric_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_AUTORICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_NUMRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_DELAYRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_CONNECTTIMEOUT)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_DEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_NUMDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_DELAYDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_CNF_MSG)
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_NUMRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_DELAYRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_SHOW_CONNECTTIMEOUT)
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_SHOW_DEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_SHOW_NUMDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_SHOW_DELAYDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_SHOW_MSG)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_SHOW_AUTORICONNECT)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_AUTORICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_NUMRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_DELAYRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_CONNECTTIMEOUT)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_DEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_NUMDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_DELAYDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_CNF_MSG)
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_NUMRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_DELAYRICONNECT)
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_SHOW_CONNECTTIMEOUT)
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_SHOW_DEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_SHOW_NUMDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_SHOW_DELAYDEADREQUERY)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_SHOW_MSG)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_SHOW_AUTORICONNECT)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRCNUMRIC_1_3.enabled = this.oPgFrm.Page1.oPag.oRCNUMRIC_1_3.mCond()
    this.oPgFrm.Page1.oPag.oRCDELAYR_1_5.enabled = this.oPgFrm.Page1.oPag.oRCDELAYR_1_5.mCond()
    this.oPgFrm.Page1.oPag.oRCCNTIME_1_7.enabled = this.oPgFrm.Page1.oPag.oRCCNTIME_1_7.mCond()
    this.oPgFrm.Page1.oPag.oRCNUMDRQ_1_10.enabled = this.oPgFrm.Page1.oPag.oRCNUMDRQ_1_10.mCond()
    this.oPgFrm.Page1.oPag.oRCDELAYD_1_12.enabled = this.oPgFrm.Page1.oPag.oRCDELAYD_1_12.mCond()
    this.oPgFrm.Page1.oPag.oRCCONMSG_1_14.enabled = this.oPgFrm.Page1.oPag.oRCCONMSG_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRCAUTORC_1_2.RadioValue()==this.w_RCAUTORC)
      this.oPgFrm.Page1.oPag.oRCAUTORC_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRCNUMRIC_1_3.value==this.w_RCNUMRIC)
      this.oPgFrm.Page1.oPag.oRCNUMRIC_1_3.value=this.w_RCNUMRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oRCDELAYR_1_5.value==this.w_RCDELAYR)
      this.oPgFrm.Page1.oPag.oRCDELAYR_1_5.value=this.w_RCDELAYR
    endif
    if not(this.oPgFrm.Page1.oPag.oRCCNTIME_1_7.value==this.w_RCCNTIME)
      this.oPgFrm.Page1.oPag.oRCCNTIME_1_7.value=this.w_RCCNTIME
    endif
    if not(this.oPgFrm.Page1.oPag.oRCDEADRQ_1_9.RadioValue()==this.w_RCDEADRQ)
      this.oPgFrm.Page1.oPag.oRCDEADRQ_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRCNUMDRQ_1_10.value==this.w_RCNUMDRQ)
      this.oPgFrm.Page1.oPag.oRCNUMDRQ_1_10.value=this.w_RCNUMDRQ
    endif
    if not(this.oPgFrm.Page1.oPag.oRCDELAYD_1_12.value==this.w_RCDELAYD)
      this.oPgFrm.Page1.oPag.oRCDELAYD_1_12.value=this.w_RCDELAYD
    endif
    if not(this.oPgFrm.Page1.oPag.oRCCONMSG_1_14.RadioValue()==this.w_RCCONMSG)
      this.oPgFrm.Page1.oPag.oRCCONMSG_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'paramric')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_paramconnetmPag1 as StdContainer
  Width  = 465
  height = 254
  stdWidth  = 465
  stdheight = 254
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRCAUTORC_1_2 as StdCheck with uid="SJCHGAWBQV",rtseq=2,rtrep=.f.,left=20, top=14, caption="Attiva riconnessione automatica",;
    HelpContextID = 75387192,;
    cFormVar="w_RCAUTORC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRCAUTORC_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oRCAUTORC_1_2.GetRadio()
    this.Parent.oContained.w_RCAUTORC = this.RadioValue()
    return .t.
  endfunc

  func oRCAUTORC_1_2.SetRadio()
    this.Parent.oContained.w_RCAUTORC=trim(this.Parent.oContained.w_RCAUTORC)
    this.value = ;
      iif(this.Parent.oContained.w_RCAUTORC=='S',1,;
      0)
  endfunc

  add object oRCNUMRIC_1_3 as StdField with uid="DLUVLDMJCK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RCNUMRIC", cQueryName = "RCNUMRIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 195518008,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=290, Top=45, cSayPict='"9999999999"', cGetPict='"9999999999"'

  func oRCNUMRIC_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RCAUTORC = 'S')
    endwith
   endif
  endfunc

  add object oRCDELAYR_1_5 as StdField with uid="LKKYDIEWLM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RCDELAYR", cQueryName = "RCDELAYR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 177558584,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=290, Top=68, cSayPict='"9999999999"', cGetPict='"9999999999"'

  func oRCDELAYR_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RCAUTORC = 'S')
    endwith
   endif
  endfunc

  add object oRCCNTIME_1_7 as StdField with uid="AXFVMPZZOQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RCCNTIME", cQueryName = "RCCNTIME",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 253616952,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=290, Top=91, cSayPict='"9999999999"', cGetPict='"9999999999"'

  func oRCCNTIME_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RCAUTORC = 'S')
    endwith
   endif
  endfunc

  add object oRCDEADRQ_1_9 as StdCheck with uid="XWVMZRMTUW",rtseq=6,rtrep=.f.,left=20, top=126, caption="Attiva gestione deadlock",;
    HelpContextID = 62542904,;
    cFormVar="w_RCDEADRQ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRCDEADRQ_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oRCDEADRQ_1_9.GetRadio()
    this.Parent.oContained.w_RCDEADRQ = this.RadioValue()
    return .t.
  endfunc

  func oRCDEADRQ_1_9.SetRadio()
    this.Parent.oContained.w_RCDEADRQ=trim(this.Parent.oContained.w_RCDEADRQ)
    this.value = ;
      iif(this.Parent.oContained.w_RCDEADRQ=='S',1,;
      0)
  endfunc

  add object oRCNUMDRQ_1_10 as StdField with uid="DPKPCYHERC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RCNUMDRQ", cQueryName = "RCNUMDRQ",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 63397432,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=290, Top=157, cSayPict='"9999999999"', cGetPict='"9999999999"'

  func oRCNUMDRQ_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RCDEADRQ = 'S')
    endwith
   endif
  endfunc

  add object oRCDELAYD_1_12 as StdField with uid="TVKGSBUBYJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_RCDELAYD", cQueryName = "RCDELAYD",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 177558584,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=290, Top=180, cSayPict='"9999999999"', cGetPict='"9999999999"'

  func oRCDELAYD_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RCDEADRQ = 'S')
    endwith
   endif
  endfunc


  add object oRCCONMSG_1_14 as StdCombo with uid="TWIBGWPGHC",rtseq=9,rtrep=.f.,left=290,top=221,width=165,height=21;
    , HelpContextID = 89649976;
    , cFormVar="w_RCCONMSG",RowSource=""+"Disattivato,"+"Visualizza su desktop,"+"Visualizza su wait window", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRCCONMSG_1_14.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'W',;
    space(1)))))
  endfunc
  func oRCCONMSG_1_14.GetRadio()
    this.Parent.oContained.w_RCCONMSG = this.RadioValue()
    return .t.
  endfunc

  func oRCCONMSG_1_14.SetRadio()
    this.Parent.oContained.w_RCCONMSG=trim(this.Parent.oContained.w_RCCONMSG)
    this.value = ;
      iif(this.Parent.oContained.w_RCCONMSG=='N',1,;
      iif(this.Parent.oContained.w_RCCONMSG=='D',2,;
      iif(this.Parent.oContained.w_RCCONMSG=='W',3,;
      0)))
  endfunc

  func oRCCONMSG_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RCDEADRQ = 'S' Or .w_RCAUTORC = 'S')
    endwith
   endif
  endfunc


  add object oObj_1_16 as cp_setCtrlObjprop with uid="JWCOTWJJSP",left=518, top=29, width=142,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="RCAUTORC",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 186745819


  add object oObj_1_17 as cp_setCtrlObjprop with uid="ILVEVHZWIV",left=518, top=79, width=142,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Numero tentativi di riconnessione:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 186745819


  add object oObj_1_18 as cp_setCtrlObjprop with uid="VITSABODHF",left=518, top=104, width=142,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Delay di riconnessione:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 186745819


  add object oObj_1_19 as cp_setCtrlObjprop with uid="NNNQDMVSSN",left=518, top=129, width=142,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Timeout di connessione:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 186745819


  add object oObj_1_20 as cp_setCtrlObjprop with uid="UJHTJIDMCE",left=518, top=54, width=142,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="RCDEADRQ",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 186745819


  add object oObj_1_21 as cp_setCtrlObjprop with uid="DTPYQPGYDP",left=518, top=154, width=142,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Numero tentativi per gestione deadlock:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 186745819


  add object oObj_1_22 as cp_setCtrlObjprop with uid="TDLGGOCDDN",left=518, top=179, width=142,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Delay per gestione deadlock:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 186745819


  add object oObj_1_23 as cp_setCtrlObjprop with uid="KWKMXLYNTN",left=518, top=204, width=142,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Visualizzazione messaggi in fase di riconnessione:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 186745819


  add object oObj_1_24 as cp_setCtrlObjprop with uid="GWXLLSZZAG",left=669, top=53, width=142,height=26,;
    caption='RCNUMRIC',;
   bGlobalFont=.t.,;
    cProp="Tooltiptext",cObj="w_RCNUMRIC",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 195518008


  add object oObj_1_25 as cp_setCtrlObjprop with uid="MXVKTREMSA",left=669, top=78, width=142,height=26,;
    caption='RCDELAYR',;
   bGlobalFont=.t.,;
    cProp="Tooltiptext",cObj="w_RCDELAYR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 177558584


  add object oObj_1_26 as cp_setCtrlObjprop with uid="OVROXUKMMN",left=669, top=103, width=142,height=26,;
    caption='RCCNTIME',;
   bGlobalFont=.t.,;
    cProp="Tooltiptext",cObj="w_RCCNTIME",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 253616952


  add object oObj_1_27 as cp_setCtrlObjprop with uid="FIWBVBAQNV",left=669, top=128, width=142,height=26,;
    caption='RCDEADRQ',;
   bGlobalFont=.t.,;
    cProp="Tooltiptext",cObj="w_RCDEADRQ",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 62542904


  add object oObj_1_28 as cp_setCtrlObjprop with uid="HSQVSXYJPJ",left=669, top=153, width=142,height=26,;
    caption='RCNUMDRQ',;
   bGlobalFont=.t.,;
    cProp="Tooltiptext",cObj="w_RCNUMDRQ",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 63397432


  add object oObj_1_29 as cp_setCtrlObjprop with uid="MMWDUATBQS",left=669, top=178, width=142,height=26,;
    caption='RCDELAYD',;
   bGlobalFont=.t.,;
    cProp="Tooltiptext",cObj="w_RCDELAYD",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 177558584


  add object oObj_1_30 as cp_setCtrlObjprop with uid="IGHACVLJAT",left=669, top=203, width=142,height=26,;
    caption='RCCONMSG',;
   bGlobalFont=.t.,;
    cProp="Tooltiptext",cObj="w_RCCONMSG",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 89649976


  add object oObj_1_31 as cp_setCtrlObjprop with uid="JRJOGYRNGG",left=669, top=28, width=142,height=26,;
    caption='RCAUTORC',;
   bGlobalFont=.t.,;
    cProp="Tooltiptext",cObj="w_RCAUTORC",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 75387192

  add object oStr_1_4 as StdString with uid="EUUEFXQKPF",Visible=.t., Left=102, Top=48,;
    Alignment=1, Width=187, Height=18,;
    Caption="Numero tentativi di riconnessione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="GKDPNLPGZV",Visible=.t., Left=160, Top=71,;
    Alignment=1, Width=129, Height=18,;
    Caption="Delay di riconnessione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="PPUFCEKESR",Visible=.t., Left=153, Top=94,;
    Alignment=1, Width=136, Height=18,;
    Caption="Timeout di connessione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="MJHIWCQYFW",Visible=.t., Left=72, Top=160,;
    Alignment=1, Width=217, Height=18,;
    Caption="Numero tentativi per gestione deadlock:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="IXLTASRJYB",Visible=.t., Left=130, Top=183,;
    Alignment=1, Width=159, Height=18,;
    Caption="Delay per gestione deadlock:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="UZBWWNLJUF",Visible=.t., Left=9, Top=223,;
    Alignment=1, Width=280, Height=18,;
    Caption="Visualizzazione messaggi in fase di riconnessione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_paramconnetm','paramric','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RCSERIAL=paramric.RCSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
