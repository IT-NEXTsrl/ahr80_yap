* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_runrep                                                       *
*              Esegue report                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-07                                                      *
* Last revis.: 2011-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_nomerep,i_TipoRep,i_NomeFil,i_nrocopiepar
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_runrep",oParentObject,m.i_nomerep,m.i_TipoRep,m.i_NomeFil,m.i_nrocopiepar)
return(i_retval)

define class tcp_runrep as StdBatch
  * --- Local variables
  i_nomerep = space(10)
  i_TipoRep = space(10)
  i_NomeFil = space(10)
  i_nrocopiepar = 0
  i_cCurrDir = 0
  w_NomeRep = space(0)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametri: i_NomeRep = nome del Report da Stampare (FRX)
    *                 i_TipoRep = Tipo Scelta stampa effettuata
    *                            'V' = Video
    *                             'S' = Stampa
    *                             'F' = File
    *                 i_NomeFil = Eventuale Nome Del File di Output
    this.i_cCurrDir = sys(5)+Sys(2003)
    L_MsgError = ""
    i_olderr=on("ERROR")
    on error L_MsgError = Message()
    select "__tmp__"
    this.w_NomeRep = this.i_NomeRep
    Set Console Off
    do case
      case this.i_TipoRep = "V"
        * --- Anteprima
         push key clear 
 i_curform=.null. 
 PUSH MENU _MSYSMENU 
 set sysmenu to 
 REPORT FORM (this.w_NomeRep) PREVIEW 
 POP MENU _MSYSMENU 
 pop key
      case this.i_TipoRep = "S"
        * --- Stampa
        do while  this.i_nrocopiepar>0
          i_NroCopie= this.i_nrocopiepar
          REPORT FORM (this.w_NomeRep) NOCONSOLE TO PRINTER
          this.i_nrocopiepar = this.i_nrocopiepar - 1
        enddo
      case this.i_TipoRep = "O"
        * --- Stampa con Opzioni
         REPORT FORM (this.w_NomeRep) NOCONSOLE TO PRINTER PROMPT
      case this.i_TipoRep = "F"
        * --- File
         REPORT FORM (this.w_NomeRep) NOCONSOLE TO FILE (this.i_NomeFil) ASCII
    endcase
    on error &i_olderr
    if !Empty(L_MsgError)
      cp_Msg("Errors executing report:"+CHR(13)+Alltrim(L_MsgError), .F.)
    endif
    CD (this.i_cCurrDir)
    i_retcode = 'stop'
    return
  endproc


  proc Init(oParentObject,i_nomerep,i_TipoRep,i_NomeFil,i_nrocopiepar)
    this.i_nomerep=i_nomerep
    this.i_TipoRep=i_TipoRep
    this.i_NomeFil=i_NomeFil
    this.i_nrocopiepar=i_nrocopiepar
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="i_nomerep,i_TipoRep,i_NomeFil,i_nrocopiepar"
endproc
