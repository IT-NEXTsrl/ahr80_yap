* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_addlog_b                                                     *
*              Union dei file di log                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-07-29                                                      *
* Last revis.: 2011-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM1
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_addlog_b",oParentObject,m.pPARAM1)
return(i_retval)

define class tcp_addlog_b as StdBatch
  * --- Local variables
  pPARAM1 = space(10)
  w_RIGHESTRUTTURA = 0
  w_ELENCOCAMPI = space(250)
  w_RIGACORRENTE = 0
  w_aldattim = ctot("")
  w_al____ms = 0
  w_al__tipo = space(3)
  w_alcmdsql = space(0)
  w_alserver = space(10)
  w_altipcon = space(1)
  w_altypedb = space(10)
  w_alproced = space(50)
  w_alcaptio = space(150)
  w_altottms = 0
  w_altottsc = 0
  w_alrowaff = 0
  w_alcursor = space(10)
  w_alerror = space(0)
  w_alcodute = 0
  w_alcodazi = space(10)
  w_alnumrig = 0
  w_alcrdati = space(10)
  w_alcolore = 0
  w_aldearic = space(1)
  w_alsqlchr = space(254)
  w_alerrchr = space(254)
  w_allibnam = space(50)
  w_al__host = space(100)
  w_alpostaz = space(100)
  w_alaziend = space(10)
  w_alnumber = 0
  * --- WorkFile variables
  show_log_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pPARAM1 = "Init"
        * --- Selezione della cartellacontenente i file di log
        this.oParentObject.w_CARTELLA = CP_GETDIR()
        ADIR(aElencoFile,this.oParentObject.w_CARTELLA+"*.dbf")
        CD (cHomeDir)
        * --- Crea cursore con l'elenco dei file della cartella selezionata
        w_ERROR = .F.
        w_OLDERROR = ON("ERROR")
        ON ERROR w_ERROR = .T.
        CREATE CURSOR ElencoDBF (Nomefile c(250), Dimensione n(10), giorno D(8), ORA C(10), TIPO C(10))
        APPEND FROM ARRAY aElencoFile
        SELECT ElencoDBF
        GO top
        do while NOT EOF()
          * --- Per ogni cursore controlla se si tratta di un file di log
          L_NOMEDBF = this.oParentObject.w_CARTELLA+ALLTRIM(ElencoDBF.NOMEFILE)
          L_ALIAS = JUSTFNAME(ElencoDBF.NOMEFILE)
          L_ALIAS = LEFT( L_ALIAS, LEN(L_ALIAS) - 4 )
          AH_MSG( "Analisi del file %1", .T., , , L_NOMEDBF)
          SELECT * FROM (L_NOMEDBF) INTO CURSOR APPOGGIO
          if USED( L_ALIAS )
            SELECT( L_ALIAS )
            USE
          endif
          AFIELDS(aElencoCampi, "APPOGGIO")
          this.w_RIGHESTRUTTURA = ALEN(aElencoCampi ,1)
          this.w_ELENCOCAMPI = "|"
          this.w_RIGACORRENTE = 1
          * --- Se mancano i campi tipici dei file di log il DBF non viene proposto nell'elenco
          do while this.w_RIGACORRENTE<=this.w_RIGHESTRUTTURA
            this.w_ELENCOCAMPI = this.w_ELENCOCAMPI + ALLTRIM( UPPER(aElencoCampi(this.w_RIGACORRENTE, 1)))+" "+ aElencoCampi(this.w_RIGACORRENTE, 2)+ "|"
            this.w_RIGACORRENTE = this.w_RIGACORRENTE + 1
          enddo
          if "ALDATTIM T" $ this.w_ELENCOCAMPI
            * --- Inserisce il record nello zoom
            SELECT ElencoDBF
            Scatter MEMVAR
            SELECT (this.oParentObject.w_ZOOM.cCURSOR)
            APPEND BLANK
            GATHER MEMVAR MEMO
          endif
          SELECT ElencoDBF
          SKIP
        enddo
        ON ERROR &w_OLDERROR
        * --- Elimina cursori
        if USED( "ElencoDBF" )
          SELECT ElencoDBF
          USE
        endif
        if USED( "APPOGGIO" )
          SELECT APPOGGIO
          USE
        endif
        * --- Si posiziona all'inizio dello zoom
        SELECT (this.oParentObject.w_ZOOM.cCURSOR)
        GO TOP
        this.oParentObject.w_ZOOM.Refresh()     
      case this.pPARAM1 = "TODB"
        * --- Esegue un ciclo sui DBF selezionati e ne trasferisce il contenuto sul database
        this.w_alpostaz = LEFT( SYS( 0 ) + SPACE( 50 ), 50 )
        this.w_alaziend = i_CODAZI
        this.w_alnumber = 0
        w_ERROR = .F.
        w_OLDERROR = ON("ERROR")
        ON ERROR w_ERROR = .T.
        * --- Delete from show_log
        i_nConn=i_TableProp[this.show_log_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.show_log_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"alpostaz = "+cp_ToStrODBC(this.w_alpostaz);
                +" and alaziend = "+cp_ToStrODBC(this.w_alaziend);
                 )
        else
          delete from (i_cTable) where;
                alpostaz = this.w_alpostaz;
                and alaziend = this.w_alaziend;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        SELECT (this.oParentObject.w_ZOOM.cCURSOR)
        GO TOP
        do while NOT EOF()
          if XCHK = 1
            * --- Interroga il DBF
            L_NOMEDBF = this.oParentObject.w_CARTELLA+ALLTRIM(NOMEFILE)
            L_ALIAS = ALLTRIM(NOMEFILE)
            L_ALIAS = LEFT( L_ALIAS, LEN(L_ALIAS) - 4 )
            AH_MSG( "Lettura del file del file %1", .T., , , L_NOMEDBF)
            SELECT * FROM (L_NOMEDBF) INTO CURSOR APPOGGIO
            SELECT APPOGGIO
            GO TOP
            do while NOT EOF()
              this.w_aldattim = aldattim
              this.w_al____ms = al____ms
              this.w_al__tipo = al__tipo
              this.w_alcmdsql = alcmdsql
              this.w_alserver = alserver
              this.w_altipcon = altipcon
              this.w_altypedb = altypedb
              this.w_alproced = alproced
              this.w_alcaptio = alcaptio
              this.w_altottms = altottms
              this.w_altottsc = altottsc
              this.w_alrowaff = alrowaff
              this.w_alcursor = alcursor
              this.w_alerror = alerror
              this.w_alcodute = alcodute
              this.w_alcodazi = alcodazi
              this.w_alnumrig = alnumrig
              this.w_alcrdati = alcrdati
              this.w_alcolore = alcolore
              this.w_aldearic = aldearic
              this.w_alsqlchr = IIF( TYPE("alsqlchr") = "U", SPACE(254), alsqlchr )
              this.w_alerrchr = IIF( TYPE("alerrchr") = "U", SPACE(254), alerrchr )
              this.w_allibnam = IIF( TYPE("allibnam") = "U", SPACE(50), allibnam )
              this.w_al__host = IIF( TYPE("al__host") = "U", SPACE(100), al__host )
              AH_MSG( "Importazione del record n. %1", .T., , , ALLTRIM(STR( this.w_alnumber ) ) )
              this.w_alnumber = this.w_alnumber + 1
              cp_WriteLOG( this.w_aldattim, this.w_al____ms, this.w_al__tipo, this.w_alcmdsql, this.w_alserver, this.w_altipcon,; 
 this.w_altypedb, this.w_alproced, this.w_alcaptio, this.w_altottms, this.w_altottsc,; 
 this.w_alrowaff, this.w_alcursor, this.w_alerror, this.w_alcrdati,; 
 this.w_alcolore, this.w_aldearic)
              if USED( L_ALIAS )
                SELECT( L_ALIAS )
                USE
              endif
              SELECT APPOGGIO
              * --- Passa al record successivo del DBF
              SKIP
            enddo
            if USED( "APPOGGIO" )
              SELECT APPOGGIO
              USE
            endif
          endif
          SELECT (this.oParentObject.w_ZOOM.cCURSOR)
          * --- Passa al DBF successivo
          SKIP
        enddo
        ON ERROR &w_OLDERROR
      case this.pPARAM1 = "SELE"
        * --- Seleziona tutto
        SELECT (this.oParentObject.w_ZOOM.cCURSOR)
        GO TOP
        do while NOT EOF()
          REPLACE XCHK WITH 1
          SKIP
        enddo
        GO TOP
      case this.pPARAM1 = "DESE"
        * --- Deseleziona tutto
        SELECT (this.oParentObject.w_ZOOM.cCURSOR)
        GO TOP
        do while NOT EOF()
          REPLACE XCHK WITH 0
          SKIP
        enddo
        GO TOP
      case this.pPARAM1 = "INVE"
        * --- Inverte selezione
        SELECT (this.oParentObject.w_ZOOM.cCURSOR)
        GO TOP
        do while NOT EOF()
          REPLACE XCHK WITH 1 - XCHK
          SKIP
        enddo
        GO TOP
    endcase
  endproc


  proc Init(oParentObject,pPARAM1)
    this.pPARAM1=pPARAM1
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='show_log'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM1"
endproc
