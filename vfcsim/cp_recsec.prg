* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_recsec                                                       *
*              Configurazione accessi record                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-18                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- cp_recsec
*Questa gestione la pu� usare solo l'amministratore
if not cp_IsAdministrator(.t.) Or Not i_bSecurityRecord
  cp_ERRORMSG(MSG_ACCESS_DENIED,'stop',MSG_SECURITY_ADMIN)
  return null
endif

=Cp_ReadXdc()
* --- Fine Area Manuale
return(createobject("tcp_recsec"))

* --- Class definition
define class tcp_recsec as StdTrsForm
  Top    = 12
  Left   = 14

  * --- Standard Properties
  Width  = 753
  Height = 384+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=107182394
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  cprecsec_IDX = 0
  cprecsecd_IDX = 0
  cpazi_IDX = 0
  cFile = "cprecsec"
  cFileDetail = "cprecsecd"
  cKeySelect = "tablename,progname"
  cKeyWhere  = "tablename=this.w_tablename and progname=this.w_progname"
  cKeyDetail  = "tablename=this.w_tablename and progname=this.w_progname"
  cKeyWhereODBC = '"tablename="+cp_ToStrODBC(this.w_tablename)';
      +'+" and progname="+cp_ToStrODBC(this.w_progname)';

  cKeyDetailWhereODBC = '"tablename="+cp_ToStrODBC(this.w_tablename)';
      +'+" and progname="+cp_ToStrODBC(this.w_progname)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"cprecsecd.tablename="+cp_ToStrODBC(this.w_tablename)';
      +'+" and cprecsecd.progname="+cp_ToStrODBC(this.w_progname)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'cprecsecd.cproword,cprecsecd.descri'
  cPrg = "cp_recsec"
  cComment = "Configurazione accessi record"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_tablename = space(50)
  o_tablename = space(50)
  w_progname = space(50)
  w_TBCOMMENT = space(60)
  w_checkact = space(1)
  w_cproword = 0
  w_queryfilter = space(254)
  w_descri = space(150)
  w_codazi = space(5)
  w_AZRAGAZI = space(40)

  * --- Children pointers
  cp_recsed = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'cprecsec','cp_recsec')
    stdPageFrame::Init()
    *set procedure to cp_recsed additive
    with this
      .Pages(1).addobject("oPag","tcp_recsecPag1","cp_recsec",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).HelpContextID = 39922290
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.otablename_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure cp_recsed
    * --- Area Manuale = Init Page Frame
    * --- cp_recsec
    * --- Translate interface...
    this.parent.cComment=cp_Translate(MSG_ACCESS_FILTER_RULE)
    this.Pages(1).Caption=cp_Translate(MSG_GENERAL)
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='cpazi'
    this.cWorkTables[2]='cprecsec'
    this.cWorkTables[3]='cprecsecd'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.cprecsec_IDX,5],7]
    this.nPostItConn=i_TableProp[this.cprecsec_IDX,3]
  return

  function CreateChildren()
    this.cp_recsed = CREATEOBJECT('stdDynamicChild',this,'cp_recsed',this.oPgFrm.Page1.oPag.oLinkPC_2_5)
    this.cp_recsed.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.cp_recsed)
      this.cp_recsed.DestroyChildrenChain()
      this.cp_recsed=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_5')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.cp_recsed.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.cp_recsed.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.cp_recsed.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .cp_recsed.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_tablename,"tablename";
             ,.w_progname,"progname";
             ,.w_cprownum,"rfrownum";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_tablename = NVL(tablename,space(50))
      .w_progname = NVL(progname,space(50))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from cprecsec where tablename=KeySet.tablename
    *                            and progname=KeySet.progname
    *
    i_nConn = i_TableProp[this.cprecsec_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2],this.bLoadRecFilter,this.cprecsec_IDX,"cp_recsec")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('cprecsec')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "cprecsec.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"cprecsecd.","cprecsec.")
      i_cTable = i_cTable+' cprecsec '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'tablename',this.w_tablename  ,'progname',this.w_progname  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_AZRAGAZI = space(40)
        .w_tablename = NVL(tablename,space(50))
        .w_progname = NVL(progname,space(50))
        .w_TBCOMMENT = i_dcx.Gettabledescr( i_dcx.GettableIdx( .w_tablename ) )
        .w_checkact = NVL(checkact,space(1))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_TABLENAME_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_TABLE+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_TABLENAME_DESC_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_PROGRAM+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(cp_translate(MSG_PROGRAM_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(cp_translate(MSG_ACTIVATE_RULE))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_ACTIVATE_RULE)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_ROW_LABEL)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_FILTER_RULE_DESC)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_COMPANY)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(cp_translate(MSG_RULE_DESC))
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_COMPANY+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_COMPANY_RULE))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'cprecsec')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from cprecsecd where tablename=KeySet.tablename
      *                            and progname=KeySet.progname
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.cprecsecd_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cprecsecd_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('cprecsecd')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "cprecsecd.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" cprecsecd"
        link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'tablename',this.w_tablename  ,'progname',this.w_progname  )
        select * from (i_cTable) cprecsecd where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_cproword = NVL(cproword,0)
          .w_queryfilter = NVL(queryfilter,space(254))
          .w_descri = NVL(descri,space(150))
          .w_codazi = NVL(codazi,space(5))
          if link_2_4_joined
            this.w_codazi = NVL(CODAZI204,NVL(this.w_codazi,space(5)))
            this.w_AZRAGAZI = NVL(DESAZI204,space(40))
          else
          .link_2_4('Load')
          endif
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_TBCOMMENT = i_dcx.Gettabledescr( i_dcx.GettableIdx( .w_tablename ) )
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_TABLENAME_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_TABLE+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_TABLENAME_DESC_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_PROGRAM+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(cp_translate(MSG_PROGRAM_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(cp_translate(MSG_ACTIVATE_RULE))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_ACTIVATE_RULE)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_ROW_LABEL)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_FILTER_RULE_DESC)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_COMPANY)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(cp_translate(MSG_RULE_DESC))
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_COMPANY+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_COMPANY_RULE))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_tablename=space(50)
      .w_progname=space(50)
      .w_TBCOMMENT=space(60)
      .w_checkact=space(1)
      .w_cproword=10
      .w_queryfilter=space(254)
      .w_descri=space(150)
      .w_codazi=space(5)
      .w_AZRAGAZI=space(40)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_progname = '*'
        .w_TBCOMMENT = i_dcx.Gettabledescr( i_dcx.GettableIdx( .w_tablename ) )
        .w_checkact = 'S'
        .DoRTCalc(5,6,.f.)
        .w_descri = .w_TBCOMMENT
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_codazi))
         .link_2_4('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_TABLENAME_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_TABLE+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_TABLENAME_DESC_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_PROGRAM+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(cp_translate(MSG_PROGRAM_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(cp_translate(MSG_ACTIVATE_RULE))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_ACTIVATE_RULE)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_ROW_LABEL)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_FILTER_RULE_DESC)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_COMPANY)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(cp_translate(MSG_RULE_DESC))
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_COMPANY+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_COMPANY_RULE))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'cprecsec')
    this.DoRTCalc(9,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.otablename_1_1.enabled = i_bVal
      .Page1.oPag.oprogname_1_2.enabled = i_bVal
      .Page1.oPag.ocheckact_1_6.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oObj_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_10.enabled = i_bVal
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page1.oPag.oObj_1_12.enabled = i_bVal
      .Page1.oPag.oObj_1_13.enabled = i_bVal
      .Page1.oPag.oObj_1_14.enabled = i_bVal
      .Page1.oPag.oObj_1_15.enabled = i_bVal
      .Page1.oPag.oObj_1_16.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      .Page1.oPag.oObj_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_20.enabled = i_bVal
      .Page1.oPag.oObj_1_21.enabled = i_bVal
      .Page1.oPag.oObj_1_22.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.otablename_1_1.enabled = .f.
        .Page1.oPag.oprogname_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.otablename_1_1.enabled = .t.
        .Page1.oPag.oprogname_1_2.enabled = .t.
      endif
    endwith
    this.cp_recsed.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'cprecsec',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.cp_recsed.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.cprecsec_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_tablename,"tablename",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_progname,"progname",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_checkact,"checkact",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.cprecsec_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
    i_lTable = "cprecsec"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.cprecsec_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_cproword N(6);
      ,t_queryfilter C(254);
      ,t_descri C(150);
      ,t_codazi C(5);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tcp_recsecbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1.controlsource=this.cTrsName+'.t_cproword'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oqueryfilter_2_2.controlsource=this.cTrsName+'.t_queryfilter'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.odescri_2_3.controlsource=this.cTrsName+'.t_descri'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.ocodazi_2_4.controlsource=this.cTrsName+'.t_codazi'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(58)
    this.AddVLine(532)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.cprecsec_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into cprecsec
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'cprecsec')
        i_extval=cp_InsertValODBCExtFlds(this,'cprecsec')
        local i_cFld
        i_cFld=" "+;
                  "(tablename,progname,checkact"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_tablename)+;
                    ","+cp_ToStrODBC(this.w_progname)+;
                    ","+cp_ToStrODBC(this.w_checkact)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'cprecsec')
        i_extval=cp_InsertValVFPExtFlds(this,'cprecsec')
        cp_CheckDeletedKey(i_cTable,0,'tablename',this.w_tablename,'progname',this.w_progname)
        INSERT INTO (i_cTable);
              (tablename,progname,checkact &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_tablename;
                  ,this.w_progname;
                  ,this.w_checkact;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.cprecsecd_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cprecsecd_IDX,2])
      *
      * insert into cprecsecd
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(tablename,progname,cproword,queryfilter,descri"+;
                  ",codazi,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_tablename)+","+cp_ToStrODBC(this.w_progname)+","+cp_ToStrODBC(this.w_cproword)+","+cp_ToStrODBC(this.w_queryfilter)+","+cp_ToStrODBC(this.w_descri)+;
             ","+cp_ToStrODBCNull(this.w_codazi)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'tablename',this.w_tablename,'progname',this.w_progname)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_tablename,this.w_progname,this.w_cproword,this.w_queryfilter,this.w_descri"+;
                ",this.w_codazi,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.cprecsec_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update cprecsec
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'cprecsec')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " checkact="+cp_ToStrODBC(this.w_checkact)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'cprecsec')
          i_cWhere = cp_PKFox(i_cTable  ,'tablename',this.w_tablename  ,'progname',this.w_progname  )
          UPDATE (i_cTable) SET;
              checkact=this.w_checkact;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_queryfilter)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.cprecsecd_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.cprecsecd_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.cp_recsed.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_tablename,"tablename";
                     ,this.w_progname,"progname";
                     ,this.w_cprownum,"rfrownum";
                     )
              this.cp_recsed.mDelete()
              *
              * delete from cprecsecd
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update cprecsecd
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " cproword="+cp_ToStrODBC(this.w_cproword)+;
                     ",queryfilter="+cp_ToStrODBC(this.w_queryfilter)+;
                     ",descri="+cp_ToStrODBC(this.w_descri)+;
                     ",codazi="+cp_ToStrODBCNull(this.w_codazi)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      cproword=this.w_cproword;
                     ,queryfilter=this.w_queryfilter;
                     ,descri=this.w_descri;
                     ,codazi=this.w_codazi;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (NOT EMPTY(t_queryfilter))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.cp_recsed.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_tablename,"tablename";
             ,this.w_progname,"progname";
             ,this.w_cprownum,"rfrownum";
             )
        this.cp_recsed.mReplace()
        this.cp_recsed.bSaveContext=.f.
      endscan
     this.cp_recsed.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_queryfilter)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- cp_recsed : Deleting
        this.cp_recsed.bSaveContext=.f.
        this.cp_recsed.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_tablename,"tablename";
               ,this.w_progname,"progname";
               ,this.w_cprownum,"rfrownum";
               )
        this.cp_recsed.bSaveContext=.t.
        this.cp_recsed.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.cprecsecd_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cprecsecd_IDX,2])
        *
        * delete cprecsecd
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.cprecsec_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
        *
        * delete cprecsec
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_queryfilter)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.cprecsec_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_tablename<>.w_tablename
          .w_TBCOMMENT = i_dcx.Gettabledescr( i_dcx.GettableIdx( .w_tablename ) )
        endif
        .DoRTCalc(4,6,.t.)
        if .o_TABLENAME<>.w_TABLENAME
          .w_descri = .w_TBCOMMENT
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_TABLENAME_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_TABLE+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_TABLENAME_DESC_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_PROGRAM+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(cp_translate(MSG_PROGRAM_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(cp_translate(MSG_ACTIVATE_RULE))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_ACTIVATE_RULE)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_ROW_LABEL)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_FILTER_RULE_DESC)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_COMPANY)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(cp_translate(MSG_RULE_DESC))
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_COMPANY+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_COMPANY_RULE))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(cp_translate(MSG_TABLENAME_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_TABLE+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(cp_translate(MSG_TABLENAME_DESC_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_PROGRAM+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(cp_translate(MSG_PROGRAM_TOOLTIP))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(cp_translate(MSG_ACTIVATE_RULE))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_ACTIVATE_RULE)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_ROW_LABEL)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_FILTER_RULE_DESC)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_COMPANY)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(cp_translate(MSG_RULE_DESC))
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_COND_QUERY_FILTER))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_COMPANY+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_COMPANY_RULE))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=codazi
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpazi_IDX,3]
    i_lTable = "cpazi"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpazi_IDX,2], .t., this.cpazi_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpazi_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codazi) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpazi')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CODAZI like "+cp_ToStrODBC(trim(this.w_codazi)+"%");

          i_ret=cp_SQL(i_nConn,"select CODAZI,DESAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODAZI',trim(this.w_codazi))
          select CODAZI,DESAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_codazi)==trim(_Link_.CODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_codazi) and !this.bDontReportError
            deferred_cp_zoom('cpazi','*','CODAZI',cp_AbsName(oSource.parent,'ocodazi_2_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODAZI,DESAZI";
                     +" from "+i_cTable+" "+i_lTable+" where CODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODAZI',oSource.xKey(1))
            select CODAZI,DESAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codazi)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODAZI,DESAZI";
                   +" from "+i_cTable+" "+i_lTable+" where CODAZI="+cp_ToStrODBC(this.w_codazi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODAZI',this.w_codazi)
            select CODAZI,DESAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codazi = NVL(_Link_.CODAZI,space(5))
      this.w_AZRAGAZI = NVL(_Link_.DESAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_codazi = space(5)
      endif
      this.w_AZRAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpazi_IDX,2])+'\'+cp_ToStr(_Link_.CODAZI,1)
      cp_ShowWarn(i_cKey,this.cpazi_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codazi Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.cpazi_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.cpazi_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CODAZI as CODAZI204"+ ",link_2_4.DESAZI as DESAZI204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on cprecsecd.codazi=link_2_4.CODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and cprecsecd.codazi=link_2_4.CODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.otablename_1_1.value==this.w_tablename)
      this.oPgFrm.Page1.oPag.otablename_1_1.value=this.w_tablename
    endif
    if not(this.oPgFrm.Page1.oPag.oprogname_1_2.value==this.w_progname)
      this.oPgFrm.Page1.oPag.oprogname_1_2.value=this.w_progname
    endif
    if not(this.oPgFrm.Page1.oPag.oTBCOMMENT_1_5.value==this.w_TBCOMMENT)
      this.oPgFrm.Page1.oPag.oTBCOMMENT_1_5.value=this.w_TBCOMMENT
    endif
    if not(this.oPgFrm.Page1.oPag.ocheckact_1_6.RadioValue()==this.w_checkact)
      this.oPgFrm.Page1.oPag.ocheckact_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZRAGAZI_2_6.value==this.w_AZRAGAZI)
      this.oPgFrm.Page1.oPag.oAZRAGAZI_2_6.value=this.w_AZRAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1.value==this.w_cproword)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1.value=this.w_cproword
      replace t_cproword with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oqueryfilter_2_2.value==this.w_queryfilter)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oqueryfilter_2_2.value=this.w_queryfilter
      replace t_queryfilter with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oqueryfilter_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.odescri_2_3.value==this.w_descri)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.odescri_2_3.value=this.w_descri
      replace t_descri with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.odescri_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocodazi_2_4.value==this.w_codazi)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocodazi_2_4.value=this.w_codazi
      replace t_codazi with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocodazi_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'cprecsec')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .cp_recsed.CheckForm()
      if NOT EMPTY(.w_queryfilter)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_tablename = this.w_tablename
    * --- cp_recsed : Depends On
    this.cp_recsed.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_queryfilter))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_cproword=MIN(999999,cp_maxroword()+10)
      .w_queryfilter=space(254)
      .w_descri=space(150)
      .w_codazi=space(5)
      .DoRTCalc(1,6,.f.)
        .w_descri = .w_TBCOMMENT
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_codazi))
        .link_2_4('Full')
      endif
    endwith
    this.DoRTCalc(9,9,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_cproword = t_cproword
    this.w_queryfilter = t_queryfilter
    this.w_descri = t_descri
    this.w_codazi = t_codazi
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_cproword with this.w_cproword
    replace t_queryfilter with this.w_queryfilter
    replace t_descri with this.w_descri
    replace t_codazi with this.w_codazi
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tcp_recsecPag1 as StdContainer
  Width  = 749
  height = 384
  stdWidth  = 749
  stdheight = 384
  resizeXpos=452
  resizeYpos=245
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object otablename_1_1 as StdField with uid="NRBQWSBWLH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_tablename", cQueryName = "tablename",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome della tabella su cui applicare la politica di sicurezza",;
    HelpContextID = 96828647,;
   bGlobalFont=.t.,;
    Height=21, Width=599, Left=85, Top=7, InputMask=replicate('X',50)

  func otablename_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(i_dcx.GetTableIdx( Alltrim( .w_tablename ) )>0)
         bRes=(cp_WarningMsg(thisform.msgFmt("La tabella selezionata non � disponibile all'interno del dizionario dati")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oprogname_1_2 as StdField with uid="ESJNKXIVCU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_progname", cQueryName = "tablename,progname",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome programma su cui applicare la politica di sicurezza",;
    HelpContextID = 243475315,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=85, Top=55, InputMask=replicate('X',50)

  add object oTBCOMMENT_1_5 as StdField with uid="GWXCAUQGAN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TBCOMMENT", cQueryName = "TBCOMMENT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tabella",;
    HelpContextID = 69969445,;
   bGlobalFont=.t.,;
    Height=21, Width=599, Left=85, Top=31, InputMask=replicate('X',60)

  add object ocheckact_1_6 as StdCheck with uid="YNWBKZRIGR",rtseq=4,rtrep=.f.,left=85, top=79, caption="Regola attiva",;
    ToolTipText = "Regola attiva",;
    HelpContextID = 188249945,;
    cFormVar="w_checkact", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func ocheckact_1_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..checkact,&i_cF..t_checkact),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func ocheckact_1_6.GetRadio()
    this.Parent.oContained.w_checkact = this.RadioValue()
    return .t.
  endfunc

  func ocheckact_1_6.ToRadio()
    this.Parent.oContained.w_checkact=trim(this.Parent.oContained.w_checkact)
    return(;
      iif(this.Parent.oContained.w_checkact=='S',1,;
      0))
  endfunc

  func ocheckact_1_6.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oObj_1_8 as cp_setobjprop with uid="SKPUZOAYUU",left=894, top=9, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_tablename",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_9 as cp_setCtrlObjprop with uid="WTFHXDANKA",left=792, top=10, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Tabella:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_10 as cp_setobjprop with uid="SAFEMKZHUJ",left=894, top=35, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_TBCOMMENT",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_11 as cp_setCtrlObjprop with uid="IMXMZFRSVJ",left=792, top=59, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Programma:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_12 as cp_setobjprop with uid="SIDWZYGKOQ",left=894, top=59, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_progname",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_13 as cp_setobjprop with uid="RKGBESZNIH",left=792, top=83, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_checkact",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_14 as cp_setobjprop with uid="KAENEUPWTM",left=894, top=83, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="caption",cObj="w_checkact",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_15 as cp_setCtrlObjprop with uid="YRZXFUYBXN",left=791, top=114, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Riga",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_16 as cp_setCtrlObjprop with uid="AAEXIHZLDH",left=894, top=114, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Filtro da applicare / descrizione regola",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_17 as cp_setCtrlObjprop with uid="TNTKFGFRZH",left=992, top=114, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Azienda",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_18 as cp_setobjprop with uid="YFKZLBUTEW",left=894, top=139, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_queryfilter",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_19 as cp_setobjprop with uid="WPKILLVJVN",left=894, top=158, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_TBCOMMENT",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_20 as cp_setobjprop with uid="CGPHDVALPP",left=992, top=139, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_queryfilter",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_21 as cp_setCtrlObjprop with uid="BINUQMREVJ",left=791, top=331, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Azienda:",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oObj_1_22 as cp_setobjprop with uid="FUEPHDVJAN",left=890, top=331, width=93,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="tooltiptext",cObj="w_AZRAGAZI",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 56425398


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=101, width=597,height=38,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="cproword",Label1="Riga",Field2="queryfilter",Label2="Filtro da applicare",Field3="descri",Label3="Descrizione regola",Field4="codazi",Label4="Azienda",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 150002328

  add object oStr_1_3 as StdString with uid="CJOAMTEIGX",Visible=.t., Left=2, Top=7,;
    Alignment=1, Width=77, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="JLRACMPNQZ",Visible=.t., Left=1, Top=365,;
    Alignment=1, Width=58, Height=18,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="YRTLUFAJCL",Visible=.t., Left=1, Top=55,;
    Alignment=1, Width=78, Height=18,;
    Caption="Programma:"  ;
  , bGlobalFont=.t.

  add object oBox_1_24 as StdBox with uid="GKJPEZNFID",left=600, top=100, width=149,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("cp_recsed",lower(this.oContained.cp_recsed.class))=0
        this.oContained.cp_recsed.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=138,;
    width=608,height=int(fontmetric(1,"Arial",9,"")*10*1.5000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=139,width=607,height=int(fontmetric(1,"Arial",9,"")*10*1.5000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=0,enabled=.f.,;
    cLinkFile='cpazi|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='cpazi'
        oDropInto=this.oBodyCol.oRow.ocodazi_2_4
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_5 as stdDynamicChildContainer with uid="VYZLQSNUZT",bOnScreen=.t.,width=143,height=210,;
   left=604, top=140;


  add object oAZRAGAZI_2_6 as StdField with uid="OTGJVDLKOA",rtseq=9,rtrep=.f.,;
    cFormVar="w_AZRAGAZI",value=space(40),enabled=.f.,;
    ToolTipText = "Azienda",;
    HelpContextID = 84177568,;
    cTotal="", bFixedPos=.t., cQueryName = "AZRAGAZI",;
    bObbl = .f. , nPag = 2, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=18, Width=517, Left=62, Top=365, InputMask=replicate('X',40)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tcp_recsecBodyRow as CPBodyRowCnt
  Width=598
  Height=int(fontmetric(1,"Arial",9,"")*2*1.5000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object ocproword_2_1 as StdTrsField with uid="LWZYBISXQD",rtseq=5,rtrep=.t.,;
    cFormVar="w_cproword",value=0,;
    HelpContextID = 134608488,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=0

  add object oqueryfilter_2_2 as StdTrsField with uid="GZWBCSNQQP",rtseq=6,rtrep=.t.,;
    cFormVar="w_queryfilter",value=space(254),;
    ToolTipText = "Condizione da applicare nella Query Filter (doppio click per la selezione della query)",;
    HelpContextID = 90040097,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=470, Left=55, Top=0, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oqueryfilter_2_2.mZoom
    this.parent.oContained.w_queryfilter=GetFile( "VQR" )
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object odescri_2_3 as StdTrsField with uid="WYUVXGPKUX",rtseq=7,rtrep=.t.,;
    cFormVar="w_descri",value=space(150),;
    ToolTipText = "Descrizione regola",;
    HelpContextID = 230276938,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=470, Left=55, Top=20, InputMask=replicate('X',150)

  add object ocodazi_2_4 as StdTrsField with uid="ZZAHVSUYCS",rtseq=8,rtrep=.t.,;
    cFormVar="w_codazi",value=space(5),;
    ToolTipText = "Nome azienda su cui applicare il filtro",;
    HelpContextID = 99098698,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=531, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="cpazi", oKey_1_1="CODAZI", oKey_1_2="this.w_codazi"

  func ocodazi_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc ocodazi_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc ocodazi_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpazi','*','CODAZI',cp_AbsName(this.parent,'ocodazi_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func ocproword_2_1.When()
    return(.t.)
  proc ocproword_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc ocproword_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_recsec','cprecsec','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".tablename=cprecsec.tablename";
  +" and "+i_cAliasName2+".progname=cprecsec.progname";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
