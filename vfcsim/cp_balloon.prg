
#INCLUDE "ctl32.h"

Define Class cp_balloontip As Container
    Width = 120
    Height = 48
    Visible = .F.
    ForeColor = Rgb(255,0,245)
    BackColor = Rgb(140,200,200)
    ctltitle = "BalloonTip title"
    ctlicon = "0"
    ctlactive = .T.
    ctlhidedelay = 10000
    *-- Sets the top, left, bottom, and right margins for a ToolTip window. A margin is the distance, in pixels, between the ToolTip window border and the text contained within the ToolTip window.
    ctlmargin = 0
    *-- Sets the maximum width for a ToolTip window.
    ctlmaxwidth = 255
    *-- Specifies the Window handle of the Window.
    ctlhwnd = 0
    ctlleft = 0
    ctltop = 0
    ctltext = "BalloonTip text"
    ctlcapslockstyle = .F.
    ctlcontrol = ""
    ctlpositionstyle = 1
    ctlfadeout = .F.
    ctlfadein = .F.
    ctlvisible = .F.
    ctlversion = 20070711
    ctllangid = 0
    ctllink = ""
    ctlclosebutton = .F.
    ctllinkurl = ""
    ctllinklabel = ""
    ctliconlarge = .F.
    *-- 1 = Balloon; 2 = Rectangular; 3 = Rectangular with no borders
    ctlstyle = 1
    ctlabout = "Zucchetti S.p.A."
    ctlbackcolor = -1
    ctlforecolor = -1
    ctlfontname = ""
    ctlfontsize = ""
    ctlfontbold = ""
    ctlfontitalic = ""
    ctloffsety = 0.75
    ctloffsetx = 0.75
    ctlmargintop = 0
    ctlmarginbottom = 0
    ctlmarginleft = 0
    ctlmarginright = 0
    ctlposition = 3
    ctlalignment = 3
    ctlfontunderline = ""
    ctlfontstrikethru = ""
    ctlfontcharset = ""
    ctlshowdelay = 200
    Name = "cp_balloontip"

    *-- XML Metadata for customizable properties
    _MemberData = .F.


    Add Object tmrhide As Timer With ;
        Top = 0, ;
        Left = 120, ;
        Height = 23, ;
        Width = 23, ;
        Enabled = .F., ;
        Interval = 0, ;
        Name = "tmrHide"


    Add Object lblname As Label With ;
        AutoSize = .T., ;
        FontName = "Tahoma", ;
        FontSize = 8, ;
        BackStyle = 0, ;
        Caption = "cp_balloontip", ;
        Height = 15, ;
        Left = 6, ;
        Top = 3, ;
        Width = 77, ;
        ForeColor = Rgb(0,0,128), ;
        Name = "lblname"


    Add Object tmrshow As Timer With ;
        Top = 24, ;
        Left = 192, ;
        Height = 23, ;
        Width = 23, ;
        Enabled = .F., ;
        Interval = 200, ;
        Name = "tmrShow"


    Hidden Procedure _create
        *!* _Create()

        *!*	CON_STYLE_BALLOON						1
        *!*	CON_STYLE_RECT							2
        *!*	CON_STYLE_NOBORDER						3


        *!* This procedure creates the Window common control and sets its initial properties

        *!* Destroy current tooltip window if present
        If This._ControlHwnd <> 0
            apiDestroyWindow(This._ControlHwnd)
            This._ControlHwnd = 0
        Endif

        This._Creating = .T.

        Local ;
            dwExStyle As Integer, ;
            lpClassName As String, ;
            lpWindowName As String, ;
            dwStyle As Integer, ;
            hWndParent As Integer, ;
            hMenu As Integer, ;
            hInstance As Integer, ;
            lpParam As Integer

        *!* Set apiCreateWindowEx parameters
        m.dwExStyle = 0
        m.lpClassName = TOOLTIPS_CLASSA
        m.lpWindowName = ""

        *!* 20070830 TTS_USEVISUALSTYLE property removed, makes text all blue in Vista
        m.dwStyle = Bitor(TTS_NOANIMATE, TTS_NOPREFIX)

        If This.ctlstyle = CON_BTSTYLE_BALLOON
            m.dwStyle = Bitor(m.dwStyle, TTS_BALLOON)
        Endif

        If This.ctlclosebutton
            m.dwStyle = Bitor(m.dwStyle, TTS_CLOSE)
        Endif

        If !This.ctlfadein
            m.dwStyle = Bitor(m.dwStyle, TTS_NOFADE)
        Endif

        m.hWndParent = 0

        m.hMenu = 0
        m.hInstance = 0
        m.lpParam = 0

        This._ControlHwnd = apiCreateWindowEx( ;
            m.dwExStyle, ;
            m.lpClassName, ;
            m.lpWindowName, ;
            m.dwStyle, ;
            CW_USEDEFAULT, ;
            CW_USEDEFAULT, ;
            CW_USEDEFAULT, ;
            CW_USEDEFAULT, ;
            m.hWndParent, ;
            m.hMenu, ;
            m.hInstance, ;
            m.lpParam)

        *!* Add TOOL
        This.oToolInfo.uFlags   = Bitor(TTF_IDISHWND, TTF_TRACK, TTF_TRANSPARENT, TTF_PARSELINKS, TTF_BITMAP)

        *!* If tooltip does not have balloon style, then position is absolute:
        If This.ctlstyle <> CON_BTSTYLE_BALLOON
            This.oToolInfo.uFlags   = Bitor(This.oToolInfo.uFlags, TTF_ABSOLUTE)
        Endif

        *!* Check if we have a centered balloon tip:
        If This.ctlstyle = CON_BTSTYLE_BALLOON ;
                And Inlist(This.ctlalignment, CON_ALIGN_MIDDLECENTER, CON_ALIGN_TOPCENTER, CON_ALIGN_BOTTOMCENTER)
            This.oToolInfo.uFlags   = Bitor(This.oToolInfo.uFlags, TTF_CENTERTIP)
        Endif

        This.oToolInfo.HWnd			= This._HosthWnd
        This.oToolInfo.hInst    	= 0
        This.oToolInfo.uId      	= This._HosthWnd
        This.oToolInfo.Rect.Left	= 0
        This.oToolInfo.Rect.Top		= 0
        This.oToolInfo.Rect.Right	= 0
        This.oToolInfo.Rect.Bottom	= 0

        This.oToolInfo.lpszText	= NULCHAR
        This.oToolInfo.Lparam			= 0

        apiSendMessageInteger(This._ControlHwnd, TTM_ADDTOOLA, 0, This.oToolInfo.Address)

        *!* 20070608 Assign colors before text or color change fails:
        This.ctlbackcolor = This.ctlbackcolor
        This.ctlforecolor = This.ctlforecolor

        *!* Save default system font for tooltips:
        This._GetFontDefault()

        *!* Set font of tooltip:
        This._SetFont()

        This.ctlicon = 		This.ctlicon && also assigns ctlTitle
        This.ctltext = 		This.ctltext
        This.ctlhidedelay = This.ctlhidedelay
        This.ctlshowdelay = This.ctlshowdelay
        This.ctlmaxwidth = 	This.ctlmaxwidth

        If This.ctlmargintop <> 0 ;
                Or This.ctlmarginleft <> 0 ;
                Or This.ctlmarginright <> 0 ;
                Or This.ctlmarginbottom <> 0

            This._SetMargins()

        Endif

        If This.ctlmargin <> 0
            This.ctlmargin = 	This.ctlmargin
        Endif

        *!* 20071022 changed bitclear to bitxor/bitor

        *If This.ctlStyle = CON_BTSTYLE_NOBORDER
        *!* Borders have no effect on Vista
        If ctlGetOsVersion() < NTDDI_VISTA
            m.dwStyle = apiGetWindowLong(This.ctlhwnd, GWL_STYLE)

            If This.ctlstyle = CON_BTSTYLE_NOBORDER

                m.dwStyle = Bitxor(Bitor(m.dwStyle, WS_BORDER), WS_BORDER)

            Endif

            *!* Have to remove TTS_CLOSE button:
            If !This.ctlclosebutton
                m.dwStyle = Bitxor(Bitor(m.dwStyle, TTS_CLOSE), TTS_CLOSE)
            Endif

            apiSetWindowLong(This.ctlhwnd, GWL_STYLE, m.dwStyle)

            *!* This makes the window "refresh"
            *        apiSendMessageInteger(This.ctlHWnd, TTM_UPDATE, 0, 0)
            *        apiSetWindowPos(This.ctlHWnd, ;
            0, ;
            0, ;
            0, ;
            0, ;
            0, ;
            BITOR(SWP_NOMOVE, SWP_NOSIZE, SWP_NOZORDER, SWP_NOACTIVATE, SWP_FRAMECHANGED))
        Endif

        This._Creating = .F.
    Endproc


    Hidden Procedure ctlhidedelay_assign
        Lparameters m.vNewVal

        If m.vNewVal < 0
            m.vNewVal = 0
        Endif

        This.ctlhidedelay = m.vNewVal
        This.tmrhide.Interval = m.vNewVal
    Endproc


    Hidden Procedure ctlactive_assign
        Lparameters vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        This.ctlactive = m.vNewVal

        If This._ControlHwnd = 0
            Return
        Endif

        If This.ctlcapslockstyle And This.ctlactive And Capslock()
            This.ctlvisible = .T.
        Endif

        *!* Hide if not Active
        If !This.ctlactive
            This.ctlvisible = .F.
        Else
            apiSendMessageInteger(This._ControlHwnd, TTM_ACTIVATE, 1, 0)
        Endif
    Endproc


    Hidden Procedure ctlmargin_assign
        *!* Sets the margin between the text and the borders of the balloon tip

        Lparameters vNewVal

        This.ctlmargin = m.vNewVal

        With This
            .ctlmargintop		= .ctlmargin
            .ctlmarginleft		= .ctlmargin
            .ctlmarginright		= .ctlmargin
            .ctlmarginbottom	= .ctlmargin
        Endwith
    Endproc


    Hidden Procedure ctlmaxwidth_assign
        Lparameters vNewVal

        *!*	If m.vNewVal < 0
        *!*		m.vNewVal = 0
        *!*	Endif

        This.ctlmaxwidth = m.vNewVal

        If This._ControlHwnd = 0
            Return
        Endif

        apiSendMessageInteger(This._ControlHwnd, TTM_SETMAXTIPWIDTH , 0, This.ctlmaxwidth)
    Endproc


    Hidden Procedure ctlicon_assign
        *!* ctlIcon_Assign()

        *!*	TTI_NONE								0
        *!*	TTI_INFO								1
        *!*	TTI_WARNING								2
        *!*	TTI_ERROR								3
        *!*	TTI_INFO_LARGE          				4		&& VISTA
        *!*	TTI_WARNING_LARGE       				5		&& VISTA
        *!*	TTI_ERROR_LARGE         				6		&& VISTA

        Lparameters vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal < 0
                m.vNewVal = 0
            Endif
            *!*	    If m.vNewVal > 6
            *!*	        m.vNewVal = 0
            *!*	    Endif
        Endif

        This.ctlicon = m.vNewVal

        This.oIcon = Null

        If Vartype(This.ctlicon) = T_CHARACTER
            If ctlGetOsVersion() >= NTDDI_WINXPSP2	&& Custom icons only work in xp sp2 and up
                If cp_FileExist(This.ctlicon)
                    This.oIcon = Null
                    This.oIcon = LoadPicture(This.ctlicon)
                    This.ctlicon = This.oIcon.Handle
                Else
                    This.ctlicon = 0
                Endif
            Else
                This.ctlicon = 0
            Endif
        Endif

        This._SetIconAndTitle()
    Endproc


    Hidden Procedure ctltitle_assign
        Lparameters vNewVal

        This.ctltitle = m.vNewVal

        If This._ControlHwnd = 0
            Return
        Endif

        This._SetIconAndTitle()
    Endproc


    Hidden Procedure ctlhwnd_access
        Return This._ControlHwnd
    Endproc


    Hidden Procedure ctlhwnd_assign
        Lparameters m.vNewVal
        Return
    Endproc


    Hidden Procedure ctltext_assign
        Lparameters m.vNewVal

        Local lcText As String

        This.ctltext = m.vNewVal
        m.lcText = m.vNewVal

        If This._ControlHwnd = 0
            Return
        Endif

        Local lcText

        *!* Replace TABS with four spaces:
        m.lcText = Strtran(m.lcText, Chr(9), Space(4))

        *!* Add one space at end of each line if font is italic to prevent clipping
        If This.ctlfontitalic
            m.lcText = Strtran(m.lcText, CRLF, " " + CRLF)
            m.lcText = m.lcText + " "
        Endif

        *!* Add terminating nul
        If Right(m.lcText, 1) <> NULCHAR
            m.lcText = m.lcText + NULCHAR
        Endif

        This.oToolInfo.lpszText = m.lcText

        apiSendMessageInteger(This._ControlHwnd, TTM_UPDATETIPTEXTA, 0, This.oToolInfo.Address)
    Endproc


    Hidden Procedure _bindevents
        *!* _BindEvents()

        Bindevent(Thisform, "RESIZE", This, "_EventHandlerFormResize", 1)
        Bindevent(Thisform, "MOVED", This, "_EventHandlerFormMoved", 1)
        Bindevent(_Screen, "MOVED", This, "_EventHandlerScreenMoved", 1)


        *Fabrizio*
        *Bindevent(Thisform.HWnd, WM_KEYDOWN, This, "_MessageHandlerFormWm_KeyDown", 1)
        *Bindevent(This._HosthWnd, WM_LBUTTONDOWN, This, "_MessageHandlerHostWm_lButtonDown", 1)

        *!* Catch clicks in links

        If !Pemstatus(Thisform,"_WMNOTIFYHWND", 5)
            Thisform.AddProperty("_WmNotifyHwnd", 0)
            Thisform.AddProperty("_WmNotifyMsg", 0)
            Thisform.AddProperty("_WmNotifyWparam", 0)
            Thisform.AddProperty("_WmNotifyLparam", 0)
        Endif

        Bindevent(This._HosthWnd, WM_NOTIFY, This, "_MessageHandlerHostWm_Notify", 1)
        Bindevent(Thisform, "_WmNotifyMsg", This, "_PropertyHandlerFormWmNotifyMsg", 1)
    Endproc


    Hidden Procedure _eventhandlerformmoved
        *!* _EventHandlerFormMoved

        If This.ctlvisible And This.ctlactive
            This.ctlvisible = .F.
            Raiseevent(This, "ctlHide", 6)
        Endif
    Endproc


    Hidden Procedure ctlvisible_assign
        Lparameters vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        This.ctlvisible = m.vNewVal

        If This._ControlHwnd = 0
            Return
        Endif

        If This.ctlvisible
            *!* We have a timer call This._Show() to overcome the
            *!* bug of setting the balloon Visible in an object getfocus,
            *!* the cursor (caret) is still in the previous control.

            *!* If ctlPositionStyle = CON_BTPOS_SYS1270, save object reference now,
            *!* This is done in case user moves mouse away from control.
            If This.ctlpositionstyle = CON_BTPOS_SYS1270
                This.ctlcontrol = Sys(1270)
            Endif

            This.tmrshow.Enabled = .T.
            *!* This timer just calls _Show()
        Else
            This.tmrshow.Enabled = .F.
            *!* Here we hide balloontip:
            If !This.ctlfadeout
                *!* We first hide the tooltip window to prevent fade out effect, TTF_NOFADE seems to
                *!* have no effect on fade out, only on fade in
                apiShowWindow(This._ControlHwnd, SW_HIDE)
            Endif

            apiSendMessageInteger(This._ControlHwnd, TTM_TRACKACTIVATE, 0, This.oToolInfo.Address)
            This._TrackActive = 0

            This.tmrhide.Enabled = .F.
        Endif
    Endproc


    Hidden Procedure _messagehandlerformwm_keydown
        *!* _MessageHandlerFormWm_KeyDown

        Lparameters pnHWnd As Integer, pnMsg As Integer, pnWparam As Integer, pnLparam As Integer

        Local lnResult As Number

        If This.ctlactive
            If This.ctlcapslockstyle
                *!* If last key pressed was CAPS LOCK
                If m.pnWparam = VK_CAPITAL
                    *!* If CAPS LOCK was set to on:
                    If Bittest(apiGetKeyState(VK_CAPITAL), 0)
                        This.ctlvisible = .T.
                    Endif
                    *!* If CAPS LOCK was set to off and the balloontip is visible:
                    If !Bittest(apiGetKeyState(VK_CAPITAL), 0) And This.ctlvisible
                        This.ctlvisible = .F.
                        Raiseevent(This, "ctlHide", 5)
                    Endif
                Else
                    *!* Key pressed was not CAPS LOCK, hide the balloontip is visible
                    If This.ctlvisible
                        This.ctlvisible = .F.
                        Raiseevent(This, "ctlHide", 4)
                    Endif
                Endif
            Else
                *!* Not ctlCapsLockStyle, Hide balloontip with any key
                If This.ctlvisible
                    This.ctlvisible = .F.
                    Raiseevent(This, "ctlHide", 4)
                Endif
            Endif

        Endif

        m.lnResult  = apiCallWindowProc(This._VFPWndProc, m.pnHWnd, m.pnMsg, m.pnWparam, m.pnLparam)

        Return m.lnResult
    Endproc


    Hidden Procedure _eventhandlerformresize
        *!* _EventHandlerFormResize

        If This.ctlvisible And This.ctlactive
            This.ctlvisible = .F.
            Raiseevent(This, "ctlHide", 6)
        Endif
    Endproc


    Hidden Procedure _messagehandlerhostwm_lbuttondown
        *!* _MessageHandlerHostWm_lButtonDown

        *!*	Here we detect a mouseclick on the form. The Balloontip transfers its mouse events to the form
        *!*	HWND. When the user clicks on the form or the balloontip, we hide the balloontip.
        *!*	IF the user clicked on the form, the form gets the click. If the user clicked on the balloontip
        *!*	we discard the click to prevent undesired clicks on the underlying form. For example, if underneath
        *!*	the balloontip there is a button, and the user clicks the balloontip to hide it, the button
        *!*	will get the click. This method prevents that.

        Lparameters pnHWnd As Integer, pnMsg As Integer, pnWparam As Integer, pnLparam As Integer

        Local lnResult As Number, lnX, lnY, lcPoint, lnOption

        m.lnOption = -1		&& not used

        If This.ctlvisible And This.ctlactive

            m.lnOption = 0		&& do not discard click

            *!* Get form coordinates of mouse click from lParam

            m.lnX = ctlGet_X_lParam(m.pnLparam)
            m.lnY = ctlGet_Y_lParam(m.pnLparam)

            *!* Convert form coordinates to screen coordinates:
            m.lcPoint = ctlMakePoint(m.lnX, m.lnY)
            *!* 20070603 Changed Thisform.hWnd to This._HosthWnd
            apiClientToScreen(This._HosthWnd, @m.lcPoint)
            m.lnX = ctlGetXFromPoint(m.lcPoint)
            m.lnY = ctlGetYFromPoint(m.lcPoint)

            *!* apiWindowFromPoint only works on XP and up, in 98 and 2K returns hwnd of form
            If ctlGetOsVersion() >= NTDDI_WINXP
                *!* We will check if the mouse click is in the ballontip or in the form
                *!* If the mouse click is on the balloontip, we do not pass the click to the form
                If apiWindowFromPoint(m.lnX, m.lnY) = This.ctlhwnd
                    m.lnOption = 1	&& discard
                Endif
            Else
                *!* Not XP, use WindowRect that is not as exact, but close

                apiGetWindowRect(This.ctlhwnd, This.oRect.Address)

                If Between(m.lnY, This.oRect.Top, This.oRect.Bottom) And ;
                        between(m.lnX, This.oRect.Left, This.oRect.Right)
                    m.lnOption = 1 	&& Discard
                Endif
            Endif

            *!* Hide BalloonTip
            This.ctlvisible = .F.

        Endif

        Do Case

            Case m.lnOption = -1	&& BalloonTip not visible and active
                m.lnResult  = apiCallWindowProc(This._VFPWndProc, m.pnHWnd, m.pnMsg, m.pnWparam, m.pnLparam)

            Case m.lnOption = 0		&& BalloonTip visible, click was outside balloontip
                m.lnResult  = apiCallWindowProc(This._VFPWndProc, m.pnHWnd, m.pnMsg, m.pnWparam, m.pnLparam)
                Raiseevent(This, "ctlHide", 2)

            Case m.lnOption = 1		&& BalloonTip visible, click was inside balloontip
                m.lnResult  = 1
                Raiseevent(This, "ctlHide", 1)
        Endcase

        Return m.lnResult
    Endproc


    Hidden Procedure _setlocalestrings
        *!* _SetLocaleStrings()

        *!* 20061011 Added space at end in some strings to avoid compilation errors in DBCS Windows
        *!* language strings extracted from comctl32.dll version 6 using String Extractor: http://www.zexersoft.com/products.html

        Local lcLocaleInfo As Character, ;
            lcTitle As Character ;
            lcText As Character, ;
            lnLangID As Number, ;
            lnPrimaryLangID As Number, ;
            lnSubLangID As Number


        If This.ctllangid = 0x0
            m.lnPrimaryLangID = ctlGetPrimaryLangID(LOCALE_USER_DEFAULT)
            m.lnSubLangID     = ctlGetSubLangID(LOCALE_USER_DEFAULT)
        Else
            *!* If ctlLangID is <> 0 then it overrides the system locale language
            m.lnPrimaryLangID = ctlGetPrimaryLangIDFromLangID(This.ctllangid)
            m.lnSubLangID = 	ctlGetSubLangIDFromLangID(This.ctllangid)
        Endif

        Do Case
            Case m.lnPrimaryLangID = LANG_BASQUE	&& 0x2d
                m.lcTitle = "Blok Maius piztuta dago"
                m.lcText = "Blok Maius piztuta edukiz gero, zure pasahitza behar ez den bezala sar dezakezu.\r\n\r\nSakatu Blok Maius piztuta egon ez dadila zure pasahitza sartu baino lehen."

            Case m.lnPrimaryLangID = LANG_CATALAN	&& 0x03
                m.lcTitle = "La tecla Bloq Maj est� activada"
                m.lcText = "Si teniu la tecla Bloq Maj activada, podr�eu introduir la contrasenya incorrectament.\r\n\r\nHaur�eu de pr�mer Bloq Maj per desactivar-la abans d'escriure la vostra contrasenya."

            Case m.lnPrimaryLangID = LANG_CHINESE And m.lnSubLangID = SUBLANG_CHINESE_TRADITIONAL && 0x04  0x01 Traditional Chinese for Taiwan and Hong Kong
                m.lcTitle = "�j�g��w�w�ҥ� "
                m.lcText = "�ҥΤj�g��w�i�వ���z���K�X��J���~�C\r\n\r\n�z���ӫ��@�U�j�g��w�A�N�������A��J�z���K�X�C "

            Case m.lnPrimaryLangID = LANG_CHINESE And m.lnSubLangID = SUBLANG_CHINESE_SIMPLIFIED && 0x04 0x02
                m.lcTitle = "��д�����򿪡� "
                m.lcText = "���ִ�д�����򿪿��ܻ�ʹ�������������롣\r\n\r\n��������ǰ����Ӧ�ð���Caps Lock����������رա� "

            Case m.lnPrimaryLangID = LANG_CROATIAN And m.lnSubLangID = SUBLANG_CROATIAN_CROATIA		&& 0x1a 0x01
                m.lcTitle = "Caps Lock je ukljucen"
                m.lcText = "Ako je ukljucen Caps Lock mo�e se dogoditi da upi�ete krivu lozinku.\r\n\r\nTrebate pritisnuti Caps Lock kako biste ga iskljucili prije unosa lozinke."

            Case m.lnPrimaryLangID = LANG_CZECH		&& 0x05
                m.lcTitle = Trim("Re�im Caps Lock je aktivn� ")
                m.lcText = "Zapnut� re�imu Caps Lock mu�e zpusobit nespr�vn� zad�n� hesla.\r\n\r\nPred zad�n�m hesla vypnete re�im Caps Lock stisknut�m kl�vesy Caps Lock."

            Case m.lnPrimaryLangID = LANG_DANISH	&& 0x06
                m.lcTitle = "Caps Lock er sl�et til"
                m.lcText = "Hvis Caps Lock er sl�et til, kan det det medf�re, at adgangskoden bliver skrevet forkert.\r\n\r\nDu b�r trykke p� Caps Lock for at sl� den fra, f�r du skriver adgangskoden."

            Case m.lnPrimaryLangID = LANG_DUTCH		&& 0x13
                m.lcTitle = "De toets CapsLock staat aan"
                m.lcText = "Als de toets CapsLock is ingeschakeld, wordt het wachtwoord mogelijk onjuist ingevoerd.\r\n\r\nDruk op CapsLock alvorens het wachtwoord in te voeren."

            Case m.lnPrimaryLangID = LANG_ENGLISH	&& 0x09
                m.lcTitle = "Caps Lock is On"
                m.lcText = "Having Caps Lock on may cause you to enter your password incorrectly.\r\n\r\nYou should press Caps Lock to turn it off before entering your password."

            Case m.lnPrimaryLangID = LANG_ESTONIAN	&& 0x25
                m.lcTitle = "Suurt�helukk on sisse l�litatud"
                m.lcText = "Sissel�litatud suurt�helukk v�ib p�hjustada teie parooli vale sisestamise.\r\n\r\nEnne parooli sisestamist peaksite suurt�heluku klahvi Caps Lock vajutamisega v�lja l�litama."

            Case m.lnPrimaryLangID = LANG_FINNISH	&& 0x0b
                m.lcTitle = Trim("Caps Lock on p��ll� ")
                m.lcText = "Jos Caps Lock on p��ll� salasanan isot ja pienet kirjaimet saattavat vaihtua.\r\n\r\nOta Caps Lock pois p��lt� ennen salasanan kirjoittamista."

            Case m.lnPrimaryLangID = LANG_FRENCH	&& 0x0c
                m.lcTitle = "La touche Verr. Maj. est active"
                m.lcText = "La touche Verr. Maj. est enfonc�e, ce qui peut fausser la saisie de votre mot de passe.\r\n\r\nAppuyez sur Verr. Maj. pour la d�sactiver, puis entrez votre mot de passe."

            Case m.lnPrimaryLangID = LANG_GALICIAN	&& 0x56
                m.lcTitle = "A tecla Bloq Mai�s est� activada"
                m.lcText = "Coa tecla Bloq Mai�s activada � prob�bel que introduza o seu contrasinal de forma incorrecta.\r\n\r\nPr�maa para a desactivar antes de introducir o seu contrasinal."

            Case m.lnPrimaryLangID = LANG_GERMAN	&& 0x07
                m.lcTitle = "Feststelltaste ist aktiviert"
                m.lcText = "Das Kennwort wird eventuell falsch eingegeben, wenn die Feststelltaste aktiviert ist.\r\n\r\nSie sollten die Feststelltaste deaktivieren, bevor Sie ein Kennwort eingeben."

            Case m.lnPrimaryLangID = LANG_HUNGARIAN	&& 0x0e
                m.lcTitle = "A Caps Lock be van kapcsolva"
                m.lcText = "Ha be van kapcsolva a Caps Lock, elofordulhat, hogy hib�san adja meg a jelsz�t.\r\n\r\nA jelsz� be�r�sa elott �rdemes kikapcsolnia a Caps Lock billentyu megnyom�s�val."

            Case m.lnPrimaryLangID = LANG_ICELANDIC	&& 0x0f
                m.lcTitle = "Stafal�s er virkur"
                m.lcText = "Ef stafal�s er virkur g�ti a�gangsor� veri� f�rt inn � rangan h�tt.\r\n\r\nSty�ja �tti � stafal�s til a� gera hann �virkan ��ur en a�gangsor�i� er f�rt inn."

            Case m.lnPrimaryLangID = LANG_INDONESIAN	&& 0x21
                m.lcTitle = "Caps Lock dalam posisi On"
                m.lcText = "Bila Caps Lock dalam posisi ON, ada kemungkinan Anda keliru memasukkan sandi.\r\n\r\nAnda harus menekan Caps Lock untuk mematikannya sebelum memasukkan sandi."

            Case m.lnPrimaryLangID = LANG_ITALIAN	&& 0x10
                m.lcTitle = "BLOC MAIUSC - Attivato"
                m.lcText = "Se il tasto BLOC MAIUSC � attivo pu� accadere di digitare la password in modo errato.\r\n\r\nPremere il tasto BLOC MAIUSC per disattivarlo prima di digitare la password."

            Case m.lnPrimaryLangID = LANG_LATVIAN	&& 0x26
                m.lcTitle = "Ir ieslegts taustin� Caps Lock"
                m.lcText = "Ja ir ieslegts taustin� Caps Lock, parole, iespejams, tiks ievadita nepareizi.\r\n\r\nLai to izslegtu, pirms paroles ievadi�anas taustin� Caps Lock janospie�."

            Case m.lnPrimaryLangID = LANG_LITHUANIAN	&& 0x27
                m.lcTitle = "Ijungtas mygtukas Caps Lock"
                m.lcText = "Jei mygtukas Caps Lock yra ijungtas, del to galite neteisingai ivesti slapta�odi.\r\n\r\nPrie� ivesdami slapta�odi spustelekite mygtuka Caps Lock, kad ji i�jungtumete."

            Case m.lnPrimaryLangID = LANG_MALAY		&& 0x3e
                m.lcTitle = "Caps Lock Dipasang"
                m.lcText = "Penggunaan Caps Lock mungkin akan menyebabkan anda memasukkan kata laluan yang salah.\r\n\r\nAnda sepatutnya menekan Caps Lock untuk mematikannya sebelum memasukkan kata laluan."

            Case m.lnPrimaryLangID = LANG_NORWEGIAN		&& 0x14
                m.lcTitle = Trim("Caps Lock er p� ")
                m.lcText = "Hvis Caps Lock er p�, kan det f�re til at passord skrives inn feil.\r\n\r\nPass p� at Caps Lock er sl�tt av f�r du skriver inn passordet."

            Case m.lnPrimaryLangID = LANG_POLISH	&& 0x15
                m.lcTitle = "Klawisz Caps Lock jest wlaczony"
                m.lcText = "Wpisywanie przy wlaczonym klawiszu Caps Lock moze spowodowac niepoprawne wprowadzenie hasla.\r\n\r\nPrzed wprowadzeniem hasla nacisnij klawisz Caps Lock, aby go wylaczyc."

            Case m.lnPrimaryLangID = LANG_PORTUGUESE And m.lnSubLangID = SUBLANG_PORTUGUESE_BRAZILIAN	&& 0x16 0x01
                m.lcTitle = "Caps Lock est� ativada"
                m.lcText = "Se Caps Lock estiver ativado, isso pode fazer com que voc� digite a senha incorretamente.\r\n\r\nVoc� deve pressionar a tecla Caps Lock para desativ�-la antes de digitar a senha."

            Case m.lnPrimaryLangID = LANG_PORTUGUESE And m.lnSubLangID = SUBLANG_PORTUGUESE_PORTUGAL	&& 0x16 0x02
                m.lcTitle = "Caps Lock est� ligado"
                m.lcText = "Ter Caps Lock ligado pode fazer com que introduza incorrectamente a palavra-passe.\r\n\r\nDeve premir Caps Lock para desactivar antes de introduzir a sua palavra-passe."

            Case m.lnPrimaryLangID = LANG_ROMANIAN	&& 0x18
                m.lcTitle = "Tasta Caps Lock este activata"
                m.lcText = "Daca tasta Caps Lock este activata exista posibilitatea sa introduceti parola incorect.\r\n\r\nApasati tasta Caps Lock pentru a o dezactiva �nainte de a introduce parola."

                *!* Andrew L. Shalgochev andluska@hotbox.ru
            Case m.lnPrimaryLangID = LANG_RUSSIAN	&& 0x19
                * russian text in transliteration :) so as understandable
                * Caps Lock vkluchen
                * Parol mozet byt' vveden nepravylno iz-za nazatoi klavishi <Caps Lock>.
                * Otkluchite <Caps Lock> pered tem, kak vvodit' parol.

                * the same text encoded by Base64 binary
                m.lcTitle = Strconv("Q2FwcyBMb2NrIOLq6/735e0=", 14)
                m.lcText = Strconv("z+Dw7uv8IOzu5uXyIOH78vwg4uLl5OXtIO3l7/Dg4ujr/O3uIOjnLefgIO3g5uDy7ukg6uvg4uj46CA8Q2FwcyBMb2NrPi5cclxuXHJcbs7y6uv+9+jy5SA8Q2FwcyBMb2NrPiDv5fDl5CDy5ewsIOrg6iDi4u7k6PL8IO/g8O7r/C4=", 14)

                * on select, but i thinking in hexbinary is simple and long
                * maybe keep in reserve this code ;-)
                *!*	        * the same text encoded by hexbinary
                *!*	        m.lcTitle = STRCONV("43617073204C6F636B20E2EAEBFEF7E5ED", 16)
                *!*	        m.lcText = STRCONV("CFE0F0EEEBFC20ECEEE6E5F220E1FBF2FC20E2E2E5E4E5ED20EDE5EFF0E0E2E8EBFCEDEE20E8E72DE7E020EDE0E6E0F2EEE920EAEBE0E2E8F8E8203C43617073204C6F636B3E2E5C725C6E5C725C6ECEF2EAEBFEF7E8F2E5203C43617073204C6F636B3E20EFE5F0E5E420F2E5EC2C20EAE0EA20E2E2EEE4E8F2FC20EFE0F0EEEBFC2E", 16)

            Case m.lnPrimaryLangID = LANG_SERBIAN And m.lnSubLangID = SUBLANG_SERBIAN_LATIN		&& 0x1a 0x02
                m.lcTitle = "Taster Caps Lock je ukljucen"
                m.lcText = "Ako je ukljucen taster Caps Lock, mo�e se desiti da unesete pogre�nu lozinku.\r\n\r\nTrebalo bi da pritisnete taster Caps Lock da biste ga iskljucili pre uno�enja lozinke."

            Case m.lnPrimaryLangID = LANG_SLOVAK	&& 0x1b
                m.lcTitle = Trim("Kl�ves Caps Lock je zapnut� ")
                m.lcText = "Ak je zapnut� kl�ves Caps Lock, heslo m��e byt zadan� nespr�vne.\r\n\r\nPred zadan�m hesla vypnite kl�ves Caps Lock."

            Case m.lnPrimaryLangID = LANG_SLOVENIAN	&& 0x24
                m.lcTitle = "Tipka Caps Lock je vkljucena"
                m.lcText = "Ce je vkljucena tipka Caps Lock, se lahko zgodi, da boste nepravilno vnesli svoje geslo.\r\n\r\nPreden vnesete geslo, pritisnite tipko Caps Lock, da izklopite funkcijo."

            Case m.lnPrimaryLangID = LANG_SPANISH	&& 0x0a
                m.lcTitle = "Bloq May�s activado"
                m.lcText = "Si tiene activada la tecla Bloq May�s es posible que escriba incorrectamente su contrase�a.\r\n\r\nPresione la tecla Bloq May�s para desactivarla antes de escribir su contrase�a."

            Case m.lnPrimaryLangID = LANG_SWEDISH	&& 0x1d
                m.lcTitle = "Caps Lock �r aktiverat"
                m.lcText = "Om Caps Lock �r aktiverat kanske du skriver in ditt l�senord felaktigt.\r\n\r\nInaktivera Caps Lock innan du anger ditt l�senord."

            Case m.lnPrimaryLangID = LANG_TURKISH	&& 0x1f
                m.lcTitle = "Caps Lock A�ik"
                m.lcText = "A�ik Caps Lock parolanizi yanlis girmenize sebep olabilir.\r\n\r\nParolanizi girmeden �nce  kapatmak i�in Caps Lock tusuna basmalisiniz."

            Otherwise
                m.lcTitle = "Caps Lock is On"
                m.lcText = "Having Caps Lock on may cause you to enter your password incorrectly.\r\n\r\nYou should press Caps Lock to turn it off before entering your password."

        Endcase

        This._LangTitle = m.lcTitle
        This._LangText  = Strtran(m.lcText, "\r\n", CRLF)
    Endproc


    Hidden Procedure ctlcapslockstyle_assign
        Lparameters m.vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        This.ctlcapslockstyle = m.vNewVal

        If This.ctlcapslockstyle
            This.ctlvisible = .F.
            *!* Set HideDelay
            This.ctlhidedelay = 5000

            *!* Set TipPositionStyle to CARET
            This.ctlpositionstyle = CON_BTPOS_CARET

            *!* Set Icon
            This.ctlicon = TTI_WARNING

            *!* Set title and text
            This.ctltitle = This._LangTitle
            This.ctltext = This._LangText
        Else
            This.ctlvisible = .F.
        Endif

        If This.ctlcapslockstyle And This.ctlactive And Capslock()
            This.ctlvisible = .T.
        Endif
    Endproc


    Hidden Procedure _addproperties
        *!* _AddProperties()

        *!* Here we add properties to the class that will be used internally

        *!* Stores the hWnd of the host window
        This.AddProperty("_HosthWnd", ctlGetHostHWnd(This))

        *!* Indicates we are in the creation stage
        This.AddProperty("_Creating", .F.)

        *!* Stores the value of VFP window procedure
        This.AddProperty("_VFPWndProc", apiGetWindowLong(_vfp.HWnd, GWL_WNDPROC))

        *!* Stores hWnd of Windows common control created by the class
        This.AddProperty("_ControlHwnd", 0)

        *!* These 2 properties hold the title and text for the capslock balloontip
        This.AddProperty("_LangTitle", "")
        This.AddProperty("_LangText", "")

        *!* Specifies the state of the TTM_TRACKACTIVATE flag
        This.AddProperty("_TrackActive", 0)

        *!* Saves old TrackPosition, used by ctlTrack()
        This.AddProperty("_OldTrackPosition", 0)

        *!* Font handling properties:

        *!* This indicates if the default system font for tooltips has
        *!* already been saved in _DefaultLogFont
        This.AddProperty("_DefaultFontSaved", .F.)

        *!* This stores a LogFont structure of the default system font as a string
        *!* for tooltips
        This.AddProperty("_DefaultLogFont", "")

        *!* This indicates if _SetFont is allowed to continue
        This.AddProperty("_SetFontEnable", .T.)
    Endproc


    Hidden Procedure ctllangid_assign
        Lparameters m.vNewVal

        This.ctllangid = m.vNewVal

        This._setlocalestrings()
    Endproc


    Hidden Procedure ctlfadein_assign
        Lparameters m.vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        This.ctlfadein = m.vNewVal

        *!* Recreate BalloonTip
        This._create()
    Endproc


    Hidden Procedure ctlfadeout_assign
        Lparameters m.vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        This.ctlfadeout = m.vNewVal
    Endproc


    Hidden Procedure _messagehandlerhostwm_notify
        *!* _MessageHandlerHostWm_Notify

        Lparameters pnHWnd As Integer, pnMsg As Integer, pnWparam As Integer, pnLparam As Integer

        Local lnResult As Integer

        m.lnResult  = apiCallWindowProc(This._VFPWndProc, m.pnHWnd, m.pnMsg, m.pnWparam, m.pnLparam)

        Thisform._WmNotifyHwnd		= m.pnHWnd
        Thisform._WmNotifyWparam	= m.pnWparam
        Thisform._WmNotifyLparam	= m.pnLparam
        Thisform._WmNotifyMsg		= m.pnMsg

        Return m.lnResult
    Endproc


    Procedure ctlhide
        *!* ctlHide()

        Lparameters m.tnHideCode As Integer

        Do Case
            Case m.tnHideCode = -1		&& Link CLICKED
                This.ctlOpenLink()

            Case m.tnHideCode = 0		&& Hide Delay timeout
            Case m.tnHideCode = 1		&& Click Inside BalloonTip
            Case m.tnHideCode = 2		&& Click Outside BalloonTip
            Case m.tnHideCode = 3		&& Click X Close Button
            Case m.tnHideCode = 4		&& Key Pressed
            Case m.tnHideCode = 5		&& CAPS LOCK key set to off
            Case m.tnHideCode = 6		&& Form moved, resized
                *!* 20070610 New Hide Code
            Case m.tnHideCode = 7		&& ctlVisible set to FALSE
        Endcase

        *Fabrizio
        Unbindevent(Thisform.HWnd, WM_KEYDOWN)
        Unbindevent(This._HosthWnd, WM_LBUTTONDOWN)
    Endproc


    Procedure ctlOpenLink
        *!* ctlOpenLink

        *!* 20071114 removed ["] + This.ctlLinkURL + ["]
        *!* Always pass string variables to api functions and not object properties

        Local lcLink As String

        m.lcLink = This.ctllinkurl

        If Not Empty(m.lcLink)
            If Left(Lower(m.lcLink), 5)=='http:'
                ?apiShellExecute(0, 0, m.lcLink, 0, 0, SW_SHOWNORMAL)
            Else
                &lcLink
            Endif
        Endif
    Endproc


    Hidden Procedure ctlclosebutton_assign
        Lparameters m.vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        This.ctlclosebutton = m.vNewVal

        *!* Recreate BalloonTip
        This._create()
    Endproc


    Hidden Procedure ctllink_access
        *!* Store Link URL in ctlLinkURL
        *!* Store Link Label in ctlLinkLabel
        *!* Retrieve complete Link form ctlLink

        Local lcLink

        m.lcLink = ""

        If ctlGetOsVersion() >= NTDDI_WINXP
            Do Case
                Case Not Empty(This.ctllinkurl) And Not Empty(This.ctllinklabel)
                    m.lcLink =[<A href="] + Alltrim(This.ctllinkurl) + [">] + Alltrim(This.ctllinklabel) + "</A>"

                Case Not Empty(This.ctllinkurl) And Empty(This.ctllinklabel)
                    m.lcLink = "<A>" + Alltrim(This.ctllinklabel) + "</A>"

                Case Empty(This.ctllinkurl) And Not Empty(This.ctllinklabel)
                    m.lcLink =Alltrim(This.ctllinklabel)

            Endcase
        Else
            m.lcLink = Alltrim(Alltrim(This.ctllinklabel) + " " + Alltrim(This.ctllinkurl))
        Endif

        Return m.lcLink
    Endproc


    Hidden Procedure _eventhandlerscreenmoved
        *!* _EventHandlerScreenMoved

        If This.ctlvisible And This.ctlactive
            This.ctlvisible = .F.
            Raiseevent(This, "ctlHide", 6)
        Endif
    Endproc


    Hidden Procedure _SetIconAndTitle
        *!* _SetIconAndTitle()

        If This._ControlHwnd = 0
            Return
        Endif

        Local ;
            lnIcon As Integer, ;
            lcTitle As String

        m.lnIcon = This.ctlicon

        *!* Check if a large icon is desired
        If ctlGetOsVersion() >= NTDDI_VISTA And This.ctliconlarge
            If Between(m.lnIcon, 1, 3)
                m.lnIcon = m.lnIcon + 3
            Endif
        Endif

        *!* 20080812
        *!* When calling TTM_SETTITLE, the string pointed to by pszTitle
        *!* must not exceed 100 TCHARs in length, including the terminating NULL.

        m.lcTitle = Left(Alltrim(Transform(This.ctltitle)), 99)

        If Empty(m.lcTitle) And (This.ctlicon > 0 Or This.ctlclosebutton )
            m.lcTitle = " "
        Endif

        m.lcTitle = m.lcTitle + NULA

        apiSendMessageString(This._ControlHwnd, TTM_SETTITLEA, m.lnIcon, m.lcTitle)
    Endproc


    Hidden Procedure ctliconlarge_assign
        Lparameters m.vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        If This.ctliconlarge <> m.vNewVal
            This.ctliconlarge = m.vNewVal
            This._SetIconAndTitle()
        Endif
    Endproc


    Hidden Procedure ctlstyle_assign
        *!*	#Define CON_BTSTYLE_BALLOON					1
        *!*	#Define CON_BTSTYLE_RECT					2
        *!*	#Define CON_BTSTYLE_NOBORDER				3

        Lparameters vNewVal

        If This.ctlstyle <> m.vNewVal

            If m.vNewVal > 3
                m.vNewVal = CON_BTSTYLE_BALLOON
            Endif

            If m.vNewVal < 1
                m.vNewVal = CON_BTSTYLE_BALLOON
            Endif

            This.ctlstyle = m.vNewVal
        Else
            Return
        Endif

        This._create()
    Endproc


    Procedure _show
        *!* _Show()

        *!*	CON_BTPOS_NONE 							1
        *!*	CON_BTPOS_LEFTTOP						1
        *!*	CON_BTPOS_ACTIVECTRL 					2
        *!*	CON_BTPOS_CARET							3
        *!*	CON_BTPOS_SYS1270						4
        *!*	CON_BTPOS_CTRLREF						5
        *!*	CON_BTPOS_MOUSE							6

        Lparameters m.tnPositionStyle As Integer

        *!* If balloonTip is not active, dont show, just return
        If !This.ctlactive
            Return
        Endif

        Local ;
            lbReturn As Boolean, ;
            lnPositionStyle As Integer, ;
            lnLeft As Integer, ;
            lnTop As Integer, ;
            lnWidth As Integer, ;
            lnHeight As Integer, ;
            loControl As Control, ;
            lcPoint As String, ;
            lnOffsetX As Integer, ;
            lnOffsetY As Integer, ;
            lnPosition As Integer

        *!* lbReturn is set to TRUE when this procedure is called again
        *!* with a different position style
        m.lbReturn = .F.

        *!* If we have a parameter, this method has called itself with a
        *!* different position style as a parameter

        If Pcount() = 0
            m.lnPositionStyle = This.ctlpositionstyle
        Else
            m.lnPositionStyle = m.tnPositionStyle
        Endif

        *!* Now we check which positioning method we should use:

        Do Case

            Case m.lnPositionStyle = CON_BTPOS_LEFTTOP && 1
                *!* This.ctlLeft and This.ctlTop should have been defined
                *!* by user before getting here.
                m.lnTop  = This.ctltop
                m.lnLeft = This.ctlleft

            Case m.lnPositionStyle = CON_BTPOS_ACTIVECTRL && 2
                If Vartype(Thisform.ActiveControl) = T_OBJECT
                    m.loControl = Thisform.ActiveControl
                Else
                    This._show(CON_BTPOS_SYS1270)
                    m.lbReturn = .T.
                Endif

            Case m.lnPositionStyle = CON_BTPOS_CARET && 3
                *!* If object has selected text, just call this method again with CON_BTPOS_ACTIVECTRL

                *!* 20070608 added check for caret in control:
                If Vartype(Thisform.ActiveControl) = T_OBJECT ;
                        And Pemstatus(Thisform.ActiveControl, "SelLength", CON_PEMSTAT_DEFINED) ;
                        And Thisform.ActiveControl.SelLength = 0

                    m.lcPoint = Space(8)
                    *!* Get position of caret
                    apiGetCaretPos(@m.lcPoint)
                    *!* Save coordinates of caret
                    m.lnLeft = ctlGetXFromPoint(m.lcPoint) + 5

                    If This.ctlstyle = CON_BTSTYLE_BALLOON
                        m.lnTop  = ctlGetYFromPoint(m.lcPoint) + 10
                    Else
                        m.lnTop  = ctlGetYFromPoint(m.lcPoint) + 20
                    Endif
                Else
                    This._show(CON_BTPOS_ACTIVECTRL)
                    m.lbReturn = .T.
                Endif

            Case m.lnPositionStyle = CON_BTPOS_SYS1270 && 4

                *!* This control reference was saved in CtlVisible_Assign
                If Vartype(This.ctlcontrol) = T_OBJECT
                    m.loControl = This.ctlcontrol

                    If m.loControl.BaseClass = "Form"
                        This._show(CON_BTPOS_MOUSE)
                        m.lbReturn = .T.
                    Endif
                Else
                    This._show(CON_BTPOS_MOUSE)
                    m.lbReturn = .T.
                Endif

            Case m.lnPositionStyle = CON_BTPOS_CTRLREF && 5
                If Vartype(This.ctlcontrol) = T_OBJECT
                    m.loControl = This.ctlcontrol
                Else
                    This._show(CON_BTPOS_SYS1270)
                    m.lbReturn = .T.
                Endif

            Case m.lnPositionStyle >= CON_BTPOS_MOUSE && 6
                m.lcPoint = Space(8)
                *!* Get position of mouse cursor
                apiGetCursorPos(@m.lcPoint)
                *!* 20070603 Changed Thisform.hWnd to This._HosthWnd
                apiScreenToClient(This._HosthWnd, @m.lcPoint)
                m.lnLeft = ctlGetXFromPoint(m.lcPoint)
                m.lnTop  = ctlGetYFromPoint(m.lcPoint)

                *!* 20070612
            Otherwise
                This._show(CON_BTPOS_MOUSE)
                m.lbReturn = .T.
        Endcase

        *!* If _Show was called again with another position style, just return now:
        If m.lbReturn
            m.loControl = Null
            Return
        Endif

        *!* If the position style is active control, control under mouse, or control reference,
        *!* we calculate position based on control position and size

        Do Case

            Case m.lnPositionStyle = CON_BTPOS_LEFTTOP
                m.lnWidth 	= 0
                m.lnHeight 	= 0
                m.lnPosition = This.ctlalignment

            Case m.lnPositionStyle = CON_BTPOS_CARET
                m.lnWidth 	= 0
                m.lnHeight 	= 0
                m.lnPosition = This.ctlalignment

            Case m.lnPositionStyle = CON_BTPOS_MOUSE
                m.lnWidth 	= 16
                m.lnHeight 	= 16
                m.lnPosition = This.ctlalignment

            Otherwise
                *!* Get position and size of control:
                m.lnLeft 	= Objtoclient(m.loControl, CON_OBJTOCLI_LEFT)
                m.lnTop 	= Objtoclient(m.loControl, CON_OBJTOCLI_TOP)
                m.lnWidth 	= Objtoclient(m.loControl, CON_OBJTOCLI_WIDTH)
                m.lnHeight 	= Objtoclient(m.loControl, CON_OBJTOCLI_HEIGHT)

                *!* Get offset dimensions from size of control:
                m.lnPosition = This.ctlposition
        Endcase

        m.lnOffsetX = m.lnWidth  * This.ctloffsetx - m.lnWidth
        m.lnOffsetY = m.lnHeight * This.ctloffsety - m.lnHeight
        *!* Check ctlPosition and adjust left and top:

        *!*	*!* Alignment Values
        *!*	CON_ALIGN_MIDDLELEFT		0
        *!*	CON_ALIGN_MIDDLERIGHT		1
        *!*	CON_ALIGN_MIDDLECENTER		2
        *!*	CON_ALIGN_AUTOMATIC			3
        *!*	CON_ALIGN_TOPLEFT			4
        *!*	CON_ALIGN_TOPRIGHT			5
        *!*	CON_ALIGN_TOPCENTER			6
        *!*	CON_ALIGN_BOTTOMLEFT		7
        *!*	CON_ALIGN_BOTTOMRIGHT		8
        *!*	CON_ALIGN_BOTTOMCENTER		9

        *!* Vertical:
        Do Case

                *!* Middle (0,1,2):
            Case Inlist(m.lnPosition, CON_ALIGN_MIDDLELEFT, CON_ALIGN_MIDDLERIGHT, CON_ALIGN_MIDDLECENTER)
                m.lnTop  = m.lnTop + Int(m.lnHeight / 2)

                *!* Top (4,5,6):
            Case Inlist(m.lnPosition, CON_ALIGN_TOPLEFT, CON_ALIGN_TOPRIGHT, CON_ALIGN_TOPCENTER)
                m.lnTop  = m.lnTop - m.lnOffsetY

                *!* Bottom (7,8,9):
            Case Inlist(m.lnPosition, CON_ALIGN_BOTTOMLEFT, CON_ALIGN_BOTTOMRIGHT, CON_ALIGN_BOTTOMCENTER)
                m.lnTop  = m.lnTop + m.lnHeight + m.lnOffsetY

            Otherwise
                *!* (3) Defaults to Bottom:
                m.lnTop  = m.lnTop + m.lnHeight + m.lnOffsetY

        Endcase

        *!* Horizontal:
        Do Case

                *!* Left (0,4,7):
            Case Inlist(m.lnPosition, CON_ALIGN_MIDDLELEFT, CON_ALIGN_TOPLEFT, CON_ALIGN_BOTTOMLEFT)
                m.lnLeft = m.lnLeft - m.lnOffsetX

                *!* Right (1,5,8):
            Case Inlist(m.lnPosition, CON_ALIGN_MIDDLERIGHT, CON_ALIGN_TOPRIGHT, CON_ALIGN_BOTTOMRIGHT)
                m.lnLeft = m.lnLeft + m.lnWidth + m.lnOffsetX

                *!* Center (2,6,9):
            Case Inlist(m.lnPosition, CON_ALIGN_MIDDLECENTER, CON_ALIGN_TOPCENTER, CON_ALIGN_BOTTOMCENTER)
                m.lnLeft = m.lnLeft + Int(m.lnWidth / 2)

            Otherwise
                *!* (3) Defaults to Right:
                m.lnLeft = m.lnLeft + m.lnWidth + m.lnOffsetX

        Endcase

        *!* Now that we have our Left, Top coordinates, we continue

        *!* Now we have two different methods, if the style is ballon, or if the style is
        *!* rectangular tooltip.

        *!* For balloon tips, we have to calculate the x,y values to where the tip will point.

        If This.ctlstyle = CON_BTSTYLE_BALLOON

            This._ShowStyle1(m.lnPositionStyle, m.lnLeft, m.lnTop)

        Else && This.ctlStyle = CON_STYLE_BALLOON

            This._ShowStyle2(m.lnPositionStyle, m.lnLeft, m.lnTop)
        Endif

        *!* Reset HIDE timer and enable it, so tooltip autohides.
        *!* if tmrHide.Interval is 0 the tooltip will not autohide
        If This.ctlhidedelay > 0
            This.tmrhide.Reset()
            This.tmrhide.Enabled = .T.
        Endif

        *Fabrizio
        Bindevent(Thisform.HWnd, WM_KEYDOWN, This, "_MessageHandlerFormWm_KeyDown", 1)
        Bindevent(This._HosthWnd, WM_LBUTTONDOWN, This, "_MessageHandlerHostWm_lButtonDown", 1)

        m.loControl = Null
    Endproc


    Hidden Procedure ctlbackcolor_assign
        *!* ctlBackColor_Assign

        Lparameters m.vNewVal

        If Vartype(m.vNewVal) <> T_NUMERIC
            m.vNewVal = -1
        Endif

        This.ctlbackcolor = m.vNewVal

        If This.ctlbackcolor = -1
            This.ctlbackcolor = apiGetSysColor(COLOR_INFOBK)
        Endif

        If This._ControlHwnd = 0
            Return
        Endif

        If This._ControlHwnd > 0
            apiSendMessageInteger(This._ControlHwnd, TTM_SETTIPBKCOLOR, This.ctlbackcolor, 0)
        Endif
    Endproc


    Hidden Procedure ctlforecolor_assign
        *!* ctlForecolor_Assign

        Lparameters m.vNewVal

        If Vartype(m.vNewVal) <> T_NUMERIC
            m.vNewVal = -1
        Endif

        This.ctlforecolor = m.vNewVal

        If m.vNewVal = -1
            This.ctlforecolor = apiGetSysColor(COLOR_INFOTEXT)
        Endif

        If This._ControlHwnd = 0
            Return
        Endif

        If This._ControlHwnd > 0
            apiSendMessageInteger(This._ControlHwnd, TTM_SETTIPTEXTCOLOR, This.ctlforecolor, 0)
        Endif
    Endproc


    Hidden Procedure ctlfontname_assign
        Lparameters m.vNewVal

        This.ctlfontname = m.vNewVal

        This._SetFont()
    Endproc


    Hidden Procedure ctlfontsize_assign
        Lparameters m.vNewVal

        This.ctlfontsize = m.vNewVal

        This._SetFont()
    Endproc


    Hidden Procedure _SetFont
        *!* _SetFont()

        *!*	typedef struct tagLOGFONT {
        *!*	  LONG lfHeight;
        *!*	  LONG lfWidth;
        *!*	  LONG lfEscapement;
        *!*	  LONG lfOrientation;
        *!*	  LONG lfWeight;
        *!*	  BYTE lfItalic;
        *!*	  BYTE lfUnderline;
        *!*	  BYTE lfStrikeOut;
        *!*	  BYTE lfCharSet;
        *!*	  BYTE lfOutPrecision;
        *!*	  BYTE lfClipPrecision;
        *!*	  BYTE lfQuality;
        *!*	  BYTE lfPitchAndFamily;
        *!*	  TCHAR lfFaceName[LF_FACESIZE];
        *!*	} LOGFONT, *PLOGFONT;

        *!*	    *!* Get default font for tooltips
        *!*	    Local m.lcNonClientMetrics As String

        *!*	    m.lcNonClientMetrics = This.oNonClientMetrics._Struct
        *!*	    ?apiSystemParametersInfo(SPI_GETNONCLIENTMETRICS, Len(m.lcNonClientMetrics), @m.lcNonClientMetrics, 0)
        *!*	    This.oNonClientMetrics._Struct = m.lcNonClientMetrics

        *!*	    m.lcLogFont = This.oNonClientMetrics._lfStatusFontStruct


        If !This._SetFontEnable
            Return
        Endif

        If This._ControlHwnd = 0
            Return
        Endif

        This._SetFontEnable = .F.

        Local lcLogFont As String

        *!* Set default values:
        This.oLogFont.Value = This._DefaultLogFont

        *!* Fill members of font structure:
        With This.oLogFont
            .FontBold			= This.ctlfontbold
            .FontCharSet		= This.ctlfontcharset
            .FontItalic 		= This.ctlfontitalic
            .FontName 			= This.ctlfontname
            .FontSize			= This.ctlfontsize
            .FontStrikethru 	= This.ctlfontstrikethru
            .FontUnderline 		= This.ctlfontunderline
        Endwith

        m.lcLogFont = This.oLogFont.Value

        *!* Release Font object if we already have one:
        If This.oFont <> 0
            apiDeleteObject(This.oFont)
        Endif

        *!* Create a new font object:
        This.oFont = apiCreateFontIndirect(@m.lcLogFont)

        *!* Set tooltip font to this font object:
        apiSendMessageInteger(This._ControlHwnd, WM_SETFONT, This.oFont, 1)

        This._SetFontEnable = .T.
    Endproc


    Hidden Procedure ctlfontbold_assign
        Lparameters m.vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal >= FW_BOLD
                m.vNewVal = .T.
            Else
                m.vNewVal = .F.
            Endif
        Endif

        This.ctlfontbold = m.vNewVal

        This._SetFont()
    Endproc


    Hidden Procedure ctlfontitalic_assign
        Lparameters m.vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        This.ctlfontitalic = m.vNewVal

        This._SetFont()
    Endproc


    Hidden Procedure _GetFontDefault
        *!* _GetFontDefault()

        *!* Here we save the original system default font for tooltips, so we can
        *!* use it later in ctlSetDefaultFont()

        *!* This method is called only from _Create()

        *!* If default system font for tooltips has been saved, return:
        If This._DefaultFontSaved
            Return
        Endif

        Local ;
            lnFont As Integer, ;
            lcLogFont As String, ;
            lnLen As Integer

        *!* Get a handle to the current font of the tooltip:
        m.lnFont = apiSendMessageInteger(This._ControlHwnd, WM_GETFONT, 0, 0)

        *!* Initialize an empty logfont structure:
        m.lcLogFont = This.oLogFont.Value
        m.lnLen = Len(m.lcLogFont)

        *!* Get font info from font handle, as a logfont stucture:
        apiGetobject(m.lnFont, m.lnLen, @m.lcLogFont)

        *!* Save logfont structure:
        This._DefaultLogFont = m.lcLogFont

        *!* Use oLogFont object to obtain structure members
        This.oLogFont.Value = m.lcLogFont

        *!* Disable font updates to the tooltip:
        This._SetFontEnable = .F.

        *!* Here we fill the empty font properties with the default values:
        If Empty(This.ctlfontbold)
            This.ctlfontbold = This.oLogFont.FontBold
        Endif

        If Empty(This.ctlfontcharset)
            This.ctlfontcharset = This.oLogFont.FontCharSet
        Endif

        If Empty(This.ctlfontitalic)
            This.ctlfontitalic = This.oLogFont.FontItalic
        Endif

        If Empty(This.ctlfontname)
            This.ctlfontname = This.oLogFont.FontName
        Endif

        If Empty(This.ctlfontsize)
            This.ctlfontsize = This.oLogFont.FontSize
        Endif

        If Empty(This.ctlfontstrikethru)
            This.ctlfontstrikethru = This.oLogFont.FontStrikethru
        Endif

        If Empty(This.ctlfontunderline)
            This.ctlfontunderline = This.oLogFont.FontUnderline
        Endif

        This._SetFontEnable = .T.

        m.lnFont = Null

        This._DefaultFontSaved = .T.
    Endproc


    Procedure ctltrack
        *!* ctlTrack()

        Lparameters m.tcText

        Local ;
            lcPoint As String, ;
            lnLeft As Integer, ;
            lnWidth As Integer, ;
            lnHeight As Integer, ;
            lnOffsetX As Integer, ;
            lnOffsetY As Integer, ;
            lnTop As Integer, ;
            lnParam As Integer

        *!* If balloonTip is not active, dont show, just return
        If !This.ctlactive
            Return
        Endif

        If This._ControlHwnd = 0
            Return
        Endif

        If Pcount() > 0 And Vartype(m.tcText) = T_CHARACTER

            *!* 20070714 Check to see if text changed
            If Not This.ctltext == m.tcText
                This.ctltext = m.tcText
            Endif
        Endif

        *!* ctlTrack ignores ctlPosition and ctlAlignment

        m.lcPoint = Space(8)
        *!* Get position of mouse cursor
        apiGetCursorPos(@m.lcPoint)

        m.lnLeft = ctlGetXFromPoint(m.lcPoint)
        m.lnTop =  ctlGetYFromPoint(m.lcPoint)

        m.lnWidth 	= 16
        m.lnHeight 	= 16

        m.lnOffsetX = m.lnWidth  * This.ctloffsetx - m.lnWidth
        m.lnOffsetY = m.lnHeight * This.ctloffsety - m.lnHeight

        m.lnTop  = Int(m.lnTop + m.lnHeight + m.lnOffsetY)
        m.lnLeft = Int(m.lnLeft + m.lnWidth + m.lnOffsetX)

        *!* Make DWORD for TTM_TRACKPOSITION message (MAKELPARAM)
        m.lnParam = ctlMakelParam(m.lnLeft, m.lnTop)

        *!* Set tooltip position
        *!* 20070714 Check to see if m.lnParam (TrackPosition) changed
        If This._OldTrackPosition <> m.lnParam
            apiSendMessageInteger(This._ControlHwnd, TTM_TRACKPOSITION, 0, m.lnParam)
            This._OldTrackPosition = m.lnParam
        Endif

        If This._TrackActive = 0

            *!* Activate tooltip:
            apiSendMessageInteger(This._ControlHwnd, TTM_TRACKACTIVATE, 1, This.oToolInfo.Address)
            This._TrackActive = 1

        Endif
    Endproc


    Hidden Procedure ctlmargintop_assign
        Lparameters vNewVal

        This.ctlmargintop = m.vNewVal

        This._SetMargins()
    Endproc


    Hidden Procedure ctlmarginbottom_assign
        Lparameters vNewVal

        This.ctlmarginbottom = m.vNewVal

        This._SetMargins()
    Endproc


    Hidden Procedure ctlmarginleft_assign
        Lparameters vNewVal

        This.ctlmarginleft = m.vNewVal

        This._SetMargins()
    Endproc


    Hidden Procedure ctlmarginright_assign
        Lparameters vNewVal

        This.ctlmarginright = m.vNewVal

        This._SetMargins()
    Endproc


    Hidden Procedure _SetMargins
        *!* _SetMargins()

        If This._ControlHwnd = 0
            Return
        Endif

        With This.oRect
            .Top 		= This.ctlmargintop
            .Left 		= This.ctlmarginleft
            .Right 		= This.ctlmarginright
            .Bottom 	= This.ctlmarginbottom
        Endwith

        apiSendMessageInteger(This._ControlHwnd, TTM_SETMARGIN, 0, This.oRect.Address)
    Endproc


    Hidden Procedure _ShowStyle1
        *!* _ShowStyle1()

        *!* For Balloontips

        *!* Check ctlAlignment

        *!* As this is a balloon shaped tip, in the case of alignment I have not found a
        *!* way to force the orientation of the balloon in respect to the tip.
        *!* What we do is first create the balloontip in certain positions of the screen
        *!* that we now will give us a balloon with the desired orientation, for example
        *!* if we choose tip coordinates equal to thw width and height of the screen,
        *!* we get a balloon with the tip in its lower right corner

        Lparameters m.lnPositionStyle As Integer, m.lnLeft As Integer, m.lnTop As Integer

        Local ;
            lcPoint As String, ;
            lnX As Integer, ;
            lnY As Integer, ;
            lnX2 As Integer, ;
            lnY2 As Integer, ;
            lbReposition As Boolean, ;
            lnParam As Integer

        *!* Convert form coordinates to screen coordinates:
        m.lcPoint = ctlMakePoint(m.lnLeft, m.lnTop)

        *!* 20070603 Changed Thisform.hWnd to This._HosthWnd
        apiClientToScreen(This._HosthWnd, @m.lcPoint)
        m.lnX = ctlGetXFromPoint(m.lcPoint)
        m.lnY = ctlGetYFromPoint(m.lcPoint)

        m.lbReposition = .F.

        *!* Vertical: (No middle alignment for balloon tips)

        Do Case
                *!*	    Case Inlist(m.lnPositionStyle, CON_BTPOS_CARET)
                *!*	        m.lnY2 = m.lnY

                *!* Top :
            Case Inlist(This.ctlalignment, CON_ALIGN_TOPLEFT, CON_ALIGN_TOPRIGHT, CON_ALIGN_TOPCENTER)
                m.lnY2 = 8192
                m.lbReposition = .T.

                *!* Bottom :
            Case Inlist(This.ctlalignment, CON_ALIGN_BOTTOMLEFT, CON_ALIGN_BOTTOMRIGHT, CON_ALIGN_BOTTOMCENTER)
                m.lnY2 = m.lnY

            Otherwise
                *!* Defaults to Bottom:
                m.lnY2 = m.lnY
        Endcase

        *!* Horizontal:
        Do Case
                *!*	    Case Inlist(m.lnPositionStyle, CON_BTPOS_CARET)
                *!*	        m.lnX2 = m.lnX

                *!* Left :
            Case Inlist(This.ctlalignment, CON_ALIGN_MIDDLELEFT, CON_ALIGN_TOPLEFT, CON_ALIGN_BOTTOMLEFT)
                m.lnX2 = Sysmetric(1) - 32
                m.lbReposition = .T.

                *!* Right :
            Case Inlist(This.ctlalignment, CON_ALIGN_MIDDLERIGHT, CON_ALIGN_TOPRIGHT, CON_ALIGN_BOTTOMRIGHT)
                m.lnX2 = m.lnX

                *!* Center : (TTF_CENTERTIP style set in _Create())
            Case Inlist(This.ctlalignment, CON_ALIGN_MIDDLECENTER, CON_ALIGN_TOPCENTER, CON_ALIGN_BOTTOMCENTER)
                m.lnX2 = m.lnX

            Otherwise
                *!* Defaults to Right:
                m.lnX2 = m.lnX
        Endcase

        **************************************************************************

        *!* First position BallonTip in temp coordinates so ballontip gets created with
        *!* the tip in the desired position:

        *!* Make DWORD for TTM_TRACKPOSITION message (MAKELPARAM)
        m.lnParam = ctlMakelParam(m.lnX2, m.lnY2)

        *!* Set tooltip position
        apiSendMessageInteger(This._ControlHwnd, TTM_TRACKPOSITION, 0, m.lnParam)

        *!* Activate tooltip so we can get size of window:
        apiSendMessageInteger(This._ControlHwnd, TTM_TRACKACTIVATE, 1, This.oToolInfo.Address)
        This._TrackActive = 1

        *!* Do we have to reposition the balloon tip to its final position?
        If m.lbReposition
            *!* Get size of balloontip:

            apiGetWindowRect(This.ctlhwnd, This.oRect.Address)

            *!* Check vertical position of tip
            If This.oRect.Top < m.lnY2
                *!* Tip is on bottom
                m.lnY = m.lnY - This.oRect.Height
            Else
                *!* Tip is on top
                m.lnY = m.lnY
            Endif

            *!* Check horizontal position of tip
            If Inlist(This.ctlalignment, CON_ALIGN_MIDDLECENTER, CON_ALIGN_TOPCENTER, CON_ALIGN_BOTTOMCENTER)
                *!* Tip is centered
                m.lnX = m.lnX - Int(This.oRect.Width / 2)
            Else
                If This.oRect.Left < m.lnX2 - 16
                    *!* Tip is on right
                    m.lnX = m.lnX - This.oRect.Width + 16
                Else
                    *!* Tip is on left
                    m.lnX = m.lnX - 16
                Endif
            Endif

            **! Reposition balloon tip
            apiSetWindowPos(This._ControlHwnd, ;
                0, ;
                m.lnX, ;
                m.lnY, ;
                0, ;
                0, ;
                Bitor(SWP_NOSIZE, SWP_NOZORDER, SWP_NOACTIVATE))
        Endif
    Endproc


    Hidden Procedure _gettrackposparam
        *!* _GetTrackPosParam()

        Lparameters m.tnX As Integer, m.tnY As Integer

        Local ;
            lnX As Integer, ;
            lnY As Integer, ;
            lnParam As Integer

        Local lcPoint As String

        *!* Convert form coordinates to screen coordinates:
        m.lcPoint = ctlMakePoint(m.tnX, m.tnY)
        *!* 20070603 Changed Thisform.hWnd to This._HosthWnd
        apiClientToScreen(This._HosthWnd, @m.lcPoint)
        m.lnX = ctlGetXFromPoint(m.lcPoint)
        m.lnY = ctlGetYFromPoint(m.lcPoint)

        *!* Make DWORD for TTM_TRACKPOSITION message (MAKELPARAM)
        m.lnParam = ctlMakelParam(m.lnX, m.lnY)

        Return m.lnParam
    Endproc


    Hidden Procedure ctlalignment_assign
        Lparameters m.vNewVal

        Local ;
            lnAlignmentNew As Integer, ;
            lnAlignmentOld As Integer, ;
            lbRecreate As Boolean

        m.lbRecreate = .F.

        m.lnAlignmentOld = This.ctlalignment
        m.lnAlignmentNew = m.vNewVal

        This.ctlalignment = m.vNewVal

        If This.ctlstyle = CON_BTSTYLE_BALLOON
            If Inlist(m.lnAlignmentOld, CON_ALIGN_MIDDLECENTER, CON_ALIGN_TOPCENTER, CON_ALIGN_BOTTOMCENTER)
                If Not Inlist(m.lnAlignmentNew, CON_ALIGN_MIDDLECENTER, CON_ALIGN_TOPCENTER, CON_ALIGN_BOTTOMCENTER)
                    m.lbRecreate = .T.
                Endif
            Else
                If Inlist(m.lnAlignmentNew, CON_ALIGN_MIDDLECENTER, CON_ALIGN_TOPCENTER, CON_ALIGN_BOTTOMCENTER)
                    m.lbRecreate = .T.
                Endif
            Endif
        Endif

        If m.lbRecreate
            *!* Recreate BalloonTip
            This._create()
        Endif
    Endproc


    Hidden Procedure _ShowStyle2
        *!* _ShowStyle2()

        *!* For rectangular tooltips

        Lparameters m.lnPositionStyle As Integer, m.lnLeft As Integer, m.lnTop As Integer


        Local ;
            lnParam As Integer, ;
            lnTipHeight As Integer, ;
            lnTipWidth As Integer

        *!* We activate tooltip off screen to get tooltip size:


        *!* Set Tooltip position way off screen
        m.lnParam = ctlMakelParam(16000, 16000)

        *!* Set tooltip position
        apiSendMessageInteger(This._ControlHwnd, TTM_TRACKPOSITION, 0, m.lnParam)

        *!* Activate tooltip
        apiSendMessageInteger(This._ControlHwnd, TTM_TRACKACTIVATE, 1, This.oToolInfo.Address)

        This._TrackActive = 1

        *!* Now Get size of balloontip:
        apiGetWindowRect(This.ctlhwnd, This.oRect.Address)


        m.lnTipHeight = This.oRect.Height
        m.lnTipWidth  = This.oRect.Width

        **************************************************************************

        *!* Check ctlAlignment:

        *!* Vertical:
        Do Case
                *!* Middle :
            Case Inlist(This.ctlalignment, CON_ALIGN_MIDDLELEFT, CON_ALIGN_MIDDLERIGHT, CON_ALIGN_MIDDLECENTER)
                m.lnTop  = m.lnTop - Ceiling(m.lnTipHeight / 2)

                *!* Top :
            Case Inlist(This.ctlalignment, CON_ALIGN_TOPLEFT, CON_ALIGN_TOPRIGHT, CON_ALIGN_TOPCENTER)
                m.lnTop  = m.lnTop - m.lnTipHeight

                *!* Bottom :
            Case Inlist(This.ctlalignment, CON_ALIGN_BOTTOMLEFT, CON_ALIGN_BOTTOMRIGHT, CON_ALIGN_BOTTOMCENTER)
                m.lnTop  = m.lnTop

            Otherwise
                m.lnTop  = m.lnTop
        Endcase

        *!* Horizontal:
        Do Case
                *!* Left :
            Case Inlist(This.ctlalignment, CON_ALIGN_MIDDLELEFT, CON_ALIGN_TOPLEFT, CON_ALIGN_BOTTOMLEFT)
                m.lnLeft = m.lnLeft - m.lnTipWidth

                *!* Right :
            Case Inlist(This.ctlalignment, CON_ALIGN_MIDDLERIGHT, CON_ALIGN_TOPRIGHT, CON_ALIGN_BOTTOMRIGHT)
                m.lnLeft = m.lnLeft

                *!* Center :
            Case Inlist(This.ctlalignment, CON_ALIGN_MIDDLECENTER, CON_ALIGN_TOPCENTER, CON_ALIGN_BOTTOMCENTER)
                m.lnLeft = m.lnLeft - Int(m.lnTipWidth / 2)

            Otherwise
                m.lnLeft = m.lnLeft
        Endcase

        **************************************************************************

        *!* Now we position the tip:

        *!* Get lParam for TTM_TRACKPOSITION message
        m.lnParam = This._gettrackposparam(m.lnLeft, m.lnTop)

        *!* Set tooltip position, already activated off screen
        apiSendMessageInteger(This._ControlHwnd, TTM_TRACKPOSITION, 0, m.lnParam)
    Endproc


    Procedure ctlshow
        *!* ctlShow()

        *!* Displays/hides tip
        Lparameters ;
            m.tnPositionStyle As Integer, ;
            m.tcText As String , ;
            m.tcTitle As String, ;
            m.tuIcon As Variant

        This.ctlvisible = .F.

        If Pcount() > 0
            This.ctlpositionstyle = m.tnPositionStyle
        Else
            m.tnPositionStyle = 0
        Endif

        If Pcount() > 1
            This.ctltext = m.tcText
        Endif

        If Pcount() > 2
            This.ctltitle = m.tcTitle
        Endif

        If Pcount() > 3
            This.ctlicon = m.tuIcon
        Endif

        *!* If the first parameter is 0 then we hide tooltip

        If m.tnPositionStyle = 0
            This.ctlvisible = .F.
        Else
            This.ctlvisible = .T.
        Endif
    Endproc


    Procedure ctlsetdefaultfont
        This._SetFontEnable = .F.

        This.oLogFont.Value 	= This._DefaultLogFont
        This.ctlfontbold 		= This.oLogFont.FontBold
        This.ctlfontcharset		= This.oLogFont.FontCharSet
        This.ctlfontitalic 		= This.oLogFont.FontItalic
        This.ctlfontname 		= This.oLogFont.FontName
        This.ctlfontsize 		= This.oLogFont.FontSize
        This.ctlfontstrikethru 	= This.oLogFont.FontStrikethru
        This.ctlfontunderline	= This.oLogFont.FontUnderline

        This._SetFontEnable = .T.

        This._SetFont()
    Endproc


    Hidden Procedure ctlfontstrikethru_assign
        Lparameters m.vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        This.ctlfontstrikethru = m.vNewVal

        This._SetFont()
    Endproc


    Hidden Procedure ctlfontunderline_assign
        Lparameters m.vNewVal

        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif

        This.ctlfontunderline = m.vNewVal

        This._SetFont()
    Endproc


    Hidden Procedure ctlfontcharset_assign
        Lparameters m.vNewVal

        This.ctlfontcharset = m.vNewVal

        This._SetFont()
    Endproc


    Hidden Procedure ctlshowdelay_assign
        Lparameters vNewVal

        If m.vNewVal < 200
            m.vNewVal = 200
        Endif

        This.ctlshowdelay = m.vNewVal

        This.tmrshow.Interval = m.vNewVal
    Endproc


    Procedure ctlmakelink
        Lparameters m.tcLinkLabel As String, m.tcLinkURL As String

        This.ctllinklabel = m.tcLinkLabel
        This.ctllinkurl = m.tcLinkURL

        Return This.ctllink
    Endproc


    Procedure _addobjects
        *!* _AddObjects()

        *!* This will hold an object reference to a custom icon to display in balloontip
        This.AddProperty("oIcon", 0)

        *!* Add oToolInfo object
        This.AddProperty("oToolInfo", Createobject("_TOOLINFO"))

        *!* Rect structure
        This.AddProperty("oRect", Createobject("_RECT"))

        *!* nonclientmetrics structure
        *!* This.AddProperty("oNonClientMetrics", Newobject("_NonClientMetrics", m.lcVcxStructs))

        *!* logfont structure
        This.AddProperty("oLogFont", Createobject("_LOGFONT"))

        *!* This stores a font object reference that we apply to the tooltip
        *!* it gets released at Destroy() event
        This.AddProperty("oFont", 0)
    Endproc


    Procedure _propertyhandlerformwmnotifymsg
        *!* _PropertyHandlerFormWmNotifyMsg

        *!* This fires when the WM_NOTIFY_MSG property of the form changes

        Local lnCode, loNmhdr As _NMHDR

        m.loNmhdr = Createobject("_NMHDR", Thisform._WmNotifyLparam)

        If This._ControlHwnd <> m.loNmhdr.hwndFrom
            Return
        Endif

        m.lnCode = m.loNmhdr.Code

        *!* 20080813
        Do Case

            Case m.lnCode = TTN_LINKCLICK
                This.ctlvisible = .F.
                Raiseevent(This, "ctlHide", -1)

            Case m.lnCode = TTN_POP
                This.ctlvisible = .F.
                Raiseevent(This, "ctlHide", 3)

            Case m.lnCode = TTN_SHOW

                Local ;
                    lnSize As Integer, ;
                    loRect As _RECT

                m.lnSize = apiSendMessageInteger(This._ControlHwnd, TTM_GETBUBBLESIZE, 0, This.oToolInfo.Address)

                m.loRect = Newobject("_RECT")

                m.loRect.Left = 0
                m.loRect.Top = 0
                m.loRect.Right = ctlLoWord(m.lnSize)
                m.loRect.Bottom = ctlHiWord(m.lnSize)

                apiSendMessageInteger(This._ControlHwnd, TTM_ADJUSTRECT, 0, m.loRect.Address)

                m.loRect = Null
                Release m.loRect

                *!*			?ctlLoWord(m.lnSize), m.loRect.Width, ctlLoWord(m.lnSize) - m.loRect.Width
                *!*			?ctlHiWord(m.lnSize), m.loRect.Height,ctlHiWord(m.lnSize) - m.loRect.Height

        Endcase

        m.loNmhdr = .Null.
        Release m.loNmhdr
    Endproc


    Procedure ctlsavestate
        *!* ctlSaveState

        If !Pemstatus(This, "aProperties", 5)
            This.AddProperty("aProperties[50]", .F.)
        Endif

        With This
            .aProperties(01) =	.ctlactive
            .aProperties(02) =	.ctlalignment
            .aProperties(03) =	.ctlbackcolor
            .aProperties(04) =	.ctlcapslockstyle
            .aProperties(05) =	.ctlclosebutton
            .aProperties(06) =	.ctlcontrol
            .aProperties(07) =	.ctlfadein
            .aProperties(08) =	.ctlfadeout
            .aProperties(09) =	.ctlfontbold
            .aProperties(10) =	.ctlfontcharset
            .aProperties(11) =	.ctlfontitalic
            .aProperties(12) =	.ctlfontsize
            .aProperties(13) =	.ctlfontstrikethru
            .aProperties(14) =	.ctlfontunderline
            .aProperties(15) =	.ctlforecolor
            .aProperties(16) =	.ctlhidedelay
            .aProperties(17) =	.ctlicon
            .aProperties(18) =	.ctliconlarge
            .aProperties(19) =	.ctllangid
            .aProperties(20) =	.ctlleft
            .aProperties(21) =	.ctllinklabel
            .aProperties(22) =	.ctllinkurl
            .aProperties(23) =	.ctlmargin
            .aProperties(24) =	.ctlmarginbottom
            .aProperties(25) =	.ctlmarginleft
            .aProperties(26) =	.ctlmarginright
            .aProperties(27) =	.ctlmargintop
            .aProperties(28) =	.ctlmaxwidth
            .aProperties(29) =	.ctloffsetx
            .aProperties(30) =	.ctloffsety
            .aProperties(31) =	.ctlposition
            .aProperties(32) =	.ctlpositionstyle
            .aProperties(33) =	.ctlshowdelay
            .aProperties(34) =	.ctlstyle
            .aProperties(35) =	.ctltext
            .aProperties(36) =	.ctltitle
            .aProperties(37) =	.ctltop
            .aProperties(38) =	.ctlvisible
        Endwith
    Endproc


    Procedure ctlreset
        If Pemstatus(This, "aProperties", 5) = .F.
            Return
        Endif

        With This
            .ctlactive			=	.aProperties(01)
            .ctlalignment		=	.aProperties(02)
            .ctlbackcolor		=	.aProperties(03)
            .ctlcapslockstyle	=	.aProperties(04)
            .ctlclosebutton		=	.aProperties(05)
            .ctlcontrol			=	.aProperties(06)
            .ctlfadein			=	.aProperties(07)
            .ctlfadeout			=	.aProperties(08)
            .ctlfontbold		=	.aProperties(09)
            .ctlfontcharset		=	.aProperties(10)
            .ctlfontitalic		=	.aProperties(11)
            .ctlfontsize		=	.aProperties(12)
            .ctlfontstrikethru	=	.aProperties(13)
            .ctlfontunderline	=	.aProperties(14)
            .ctlforecolor		=	.aProperties(15)
            .ctlhidedelay		=	.aProperties(16)
            .ctlicon			=	.aProperties(17)
            .ctliconlarge		=	.aProperties(18)
            .ctllangid			=	.aProperties(19)
            .ctlleft			=	.aProperties(20)
            .ctllinklabel		=	.aProperties(21)
            .ctllinkurl			=	.aProperties(22)
            .ctlmargin			=	.aProperties(23)
            .ctlmarginbottom	=	.aProperties(24)
            .ctlmarginleft		=	.aProperties(25)
            .ctlmarginright		=	.aProperties(26)
            .ctlmargintop		=	.aProperties(27)
            .ctlmaxwidth		=	.aProperties(28)
            .ctloffsetx			=	.aProperties(29)
            .ctloffsety			=	.aProperties(30)
            .ctlposition		=	.aProperties(31)
            .ctlpositionstyle	=	.aProperties(32)
            .ctlshowdelay		=	.aProperties(33)
            .ctlstyle			=	.aProperties(34)
            .ctltext			=	.aProperties(35)
            .ctltitle			=	.aProperties(36)
            .ctltop				=	.aProperties(37)
            .ctlvisible			=	.aProperties(38)
        Endwith
    Endproc


    Procedure Init
        *!* If we have a parameter, Status Bar is creating this
        *!* Lparameters m.tnParam
        If Not(Lower("vfp2c32.fll")$Lower(Set('LIBRARY')))
            Set Library To ("vfp2c32.fll") Additive
        Endif

        If !InitVFP2C32(0xFFFFFFFF)
            Debugout "InitVFP2C32 FAILED!"
        Endif

        This._addobjects()
        This._addproperties()
        This._setlocalestrings()

		This._create()
        This._bindevents()
    Endproc


    Procedure Destroy
        apiDestroyWindow(This._ControlHwnd)

        *!* Disable timer just in case (Forum/rcanop/2006-11-26)
        This.tmrshow.Enabled = .F.

        *!* Control is an object reference to the control to use to locate the balloontip
        This.ctlcontrol = Null

        *!* oToolInfo is an instance of the _toolinfo class of ctl32_structs
        This.oToolInfo = Null

        *!* loIcon is an object reference to an icon file if a custom icon was used
        This.oIcon = Null

        *!* oFont stores a handle to a Logical Font in memory
        If This.oFont <> 0
            apiDeleteObject(This.oFont)
        Endif
    Endproc


    Procedure tmrhide.Timer
        If This.Parent.ctlvisible
            This.Parent.ctlvisible = .F.
            Raiseevent(This.Parent, "ctlHide", 0)
        Endif
    Endproc


    Procedure lblname.Init
        Return .F.
    Endproc


    Procedure tmrshow.Timer
        *!* Disable this timer
        This.Enabled = .F.
        This.Parent._show()
    Endproc


Enddefine

****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************
****************************************************************************************************************************

Function apiCallWindowProc
    Lparameters lpPrevWndFunc, nhWnd, Msg, wParam, Lparam
    Declare Integer CallWindowProc In win32api As apiCallWindowProc ;
        Integer lpPrevWndFunc, ;
        Integer nhWnd, ;
        Integer msg, ;
        Integer wParam, ;
        Integer Lparam
    Return apiCallWindowProc(m.lpPrevWndFunc, m.nhWnd, m.Msg, m.wParam, m.lParam)
Endfunc

Function apiClientToScreen
    Lparameters nhWnd, lpPoint
    Declare Integer ClientToScreen In win32api As apiClientToScreen ;
        Integer nhWnd, ;
        String  @lpPoint
    Return apiClientToScreen(m.nhWnd, @m.lpPoint)
Endfunc

Function apiCreateFont
    Lparameters nHeight, nWidth, nEscapement, nOrientation, fnWeight, fdwItalic, fdwUnderline, fdwStrikeOut, fdwCharSet, fdwOutputPrecision, fdwClipPrecision, fdwQuality, fdwPitchAndFamily, lpszFace
    *!* lpszFace
    *!* [in] Pointer to a null-terminated String that specifies the typeface
    *!* name of the font. The length of this String must not exceed 32 characters,
    *!* including the terminating null character.
    Declare Integer CreateFont In win32api As apiCreateFont ;
        Integer nHeight, ;
        Integer nWidth, ;
        Integer nEscapement, ;
        Integer nOrientation, ;
        Integer fnWeight, ;
        Integer fdwItalic, ;
        Integer fdwUnderline, ;
        Integer fdwStrikeOut, ;
        Integer fdwCharSet, ;
        Integer fdwOutputPrecision, ;
        Integer fdwClipPrecision, ;
        Integer fdwQuality, ;
        Integer fdwPitchAndFamily, ;
        String  lpszFace
    Return apiCreateFont(m.nHeight, m.nWidth, m.nEscapement, m.nOrientation, m.fnWeight, m.fdwItalic, m.fdwUnderline, m.fdwStrikeOut, m.fdwCharSet, m.fdwOutputPrecision, m.fdwClipPrecision, m.fdwQuality, m.fdwPitchAndFamily, m.lpszFace)
Endfunc

Function apiCreateFontIndirect
    Lparameters lpLogFont
    Declare Integer CreateFontIndirect In win32api As apiCreateFontIndirect ;
        String  lpLogFont
    Return apiCreateFontIndirect(m.lpLogFont)
Endfunc

Function apiCreateWindowEx
    Lparameters dwExStyle, lpClassName, lpWindowName, dwStyle, nx, ny, nWidth, nHeight, nHwndParent, hMenu, hInstance, lpParam
    Declare Integer CreateWindowEx In win32api As apiCreateWindowEx ;
        Integer dwExStyle, ;
        String  lpClassName, ;
        String  lpWindowName, ;
        Integer dwStyle, ;
        Integer nx, ;
        Integer ny, ;
        Integer nWidth, ;
        Integer nHeight, ;
        Integer nHwndParent, ;
        Integer hMenu, ;
        Integer hInstance, ;
        Integer lpParam
    Return apiCreateWindowEx(m.dwExStyle, m.lpClassName, m.lpWindowName, m.dwStyle, m.nx, m.ny, m.nWidth, m.nHeight, m.nHwndParent, m.hMenu, m.hInstance, m.lpParam)
Endfunc

Function apiDeleteObject
    Lparameters hObject
    Declare Integer DeleteObject In win32api As apiDeleteObject;
        Integer hObject
    Return apiDeleteObject(m.hObject)
Endfunc

Function apiDestroyWindow
    Lparameters nhWnd
    Declare Integer DestroyWindow In win32api As apiDestroyWindow ;
        Integer nhWnd
    Return apiDestroyWindow(m.nhWnd)
Endfunc

Function apiGetCaretPos
    Lparameters lpPoint
    Declare Integer GetCaretPos In win32api As apiGetCaretPos;
        String  @lpPoint
    Return apiGetCaretPos(@m.lpPoint)
Endfunc

Function apiGetCursorPos
    Lparameters lpPoint
    Declare Integer GetCursorPos In win32api As apiGetCursorPos;
        String  @lpPoint
    Return apiGetCursorPos(@m.lpPoint)
Endfunc

Function apiGetDC
    Lparameters nhWnd
    Declare Integer GetDC In win32api As apiGetDC ;
        Integer nhWnd
    Return apiGetDC(m.nhWnd)
Endfunc

Function apiGetDeviceCaps
    Lparameters hdc, nIndex
    Declare Integer GetDeviceCaps In win32api As apiGetDeviceCaps ;
        Integer hdc, ;
        Integer nIndex
    Return apiGetDeviceCaps(m.hdc, m.nIndex)
Endfunc

Function apiGetKeyState
    Lparameters nVirtKey
    Declare Integer GetKeyState In win32api As apiGetKeyState ;
        Integer nVirtKey
    Return apiGetKeyState(m.nVirtKey)
Endfunc

Function apiGetLocaleInfo
    Lparameters Locale, LCType, lpLCData, cchData
    Declare Integer GetLocaleInfo In win32api As apiGetLocaleInfo;
        Integer Locale, ;
        Integer LCType, ;
        String  @lpLCData, ;
        Integer cchData
    Return apiGetLocaleInfo(m.Locale, m.LCType, @m.lpLCData, m.cchData)
Endfunc

Function apiGetobject
    Lparameters hObject, nCount, lpObject
    Declare Integer GetObject In gdi32 As _apiGetObject ;
    	Integer hObject, ;
    	Integer nCount, ;
    	String  @lpObject
    Return _apiGetobject(m.hObject, m.nCount, @m.lpObject)
Endfunc

Function apiGetSysColor
    Lparameters nIndex
    Declare Integer GetSysColor In win32api As apiGetSysColor ;
        Integer nIndex
    Return apiGetSysColor(m.nIndex)
Endfunc

Function apiGetWindow
    Lparameters nhWnd, wCmd
    Declare Integer GetWindow In win32api As apiGetWindow;
        Integer nhWnd, ;
        Integer wCmd
    Return apiGetWindow(m.nhWnd, m.wCmd)
Endfunc

Function apiGetWindowLong
    Lparameters nhWnd, nIndex
    Declare Integer GetWindowLong In win32api As apiGetWindowLong;
        Integer nhWnd, ;
        Integer nIndex
    Return apiGetWindowLong(m.nhWnd, m.nIndex)
Endfunc

Function apiGetWindowPlacement
    Lparameters nhWnd, lpwndpl
    Declare Integer GetWindowPlacement In win32api As apiGetWindowPlacement ;
        Integer nhWnd, ;
        Integer lpwndpl
    Return apiGetWindowPlacement(m.nhWnd, m.lpwndpl)
Endfunc

Function apiGetWindowRect
    Lparameters nhWnd, lpRect
    Declare Integer GetWindowRect In win32api As apiGetWindowRect ;
        Integer nhWnd, ;
        Integer lpRect
    Return apiGetWindowRect(m.nhWnd, m.lpRect)
Endfunc

Function apiMulDiv
    Lparameters nNumber, nNumerator, nDenominator
    Declare Integer MulDiv In win32api As apiMulDiv ;
        Integer nNumber, ;
        Integer nNumerator, ;
        Integer nDenominator
    Return apiMulDiv(m.nNumber, m.nNumerator, m.nDenominator)
Endfunc

Function apiReleaseDC
    Lparameters nhWnd, hdc
    Declare Integer ReleaseDC In win32api As apiReleaseDC ;
        Integer nhWnd, ;
        Integer hdc
    Return apiReleaseDC(m.nhWnd, m.hdc)
Endfunc

Function apiScreenToClient
    Lparameters nhWnd, lpPoint
    Declare Integer ScreenToClient In win32api As apiScreenToClient ;
        Integer nhWnd, ;
        String  @lpPoint
    Return apiScreenToClient(m.nhWnd, @m.lpPoint)
Endfunc

Function apiSendMessageInteger
    Lparameters nhWnd, Msg, wParam, Lparam
    Declare Integer SendMessage In win32api As apiSendMessageInteger ;
        Integer nhWnd, ;
        Integer Msg, ;
        Integer wParam, ;
        Integer Lparam
    Return apiSendMessageInteger(m.nhWnd, m.Msg, m.wParam, m.lParam)
Endfunc

Function apiSendMessageString
    Lparameters nhWnd, Msg, wParam, Lparam
    Declare Integer SendMessage In win32api As apiSendMessageString ;
        Integer nhWnd, ;
        Integer Msg, ;
        Integer wParam, ;
        String  @Lparam
    Return apiSendMessageString(m.nhWnd, m.Msg, m.wParam, @m.lParam)
Endfunc

Function apiSetWindowLong
    Lparameters nhWnd, nIndex, dwNewLong
    Declare Integer SetWindowLong In win32api As apiSetWindowLong ;
        Integer nhWnd, ;
        Integer nIndex, ;
        Integer dwNewLong
    Return apiSetWindowLong(m.nhWnd, m.nIndex, m.dwNewLong)
Endfunc

Function apiSetWindowPos
    Lparameters nhWnd, nHwndInsertAfter, nx, ny, cx, cy, wFlags
    Declare Integer SetWindowPos In win32api As apiSetWindowPos ;
        Integer nhWnd, ;
        Integer nHwndInsertAfter, ;
        Integer nx, ;
        Integer ny, ;
        Integer cx, ;
        Integer cy, ;
        Integer wFlags
    Return apiSetWindowPos(m.nhWnd, m.nHwndInsertAfter, m.nx, m.ny, m.cx, m.cy, m.wFlags)
Endfunc

Function apiShellExecute
    Lparameters nhWnd, lpOperation, lpFile, lpParameters, lpDirectory, nShowCmd
    Declare Integer ShellExecute In win32api As apiShellExecute ;
        Integer nhWnd, ;
        String  lpOperation, ;
        String  lpFile, ;
        String  lpParameters, ;
        String  lpDirectory, ;
        Integer nShowCmd
    Return apiShellExecute(m.nhWnd, m.lpOperation, m.lpFile, m.lpParameters, m.lpDirectory, m.nShowCmd)
Endfunc

Function apiShowWindow
    Lparameters nhWnd, nCmdShow
    Declare Integer ShowWindow In win32api As apiShowWindow ;
        Integer nhWnd, ;
        Integer nCmdShow
    Return apiShowWindow(m.nhWnd, m.nCmdShow)
Endfunc

Function apiWindowFromPoint
    Lparameters pointx, pointy
    Declare Integer WindowFromPoint In win32api As apiWindowFromPoint ;
        Integer PointX, ;
        Integer PointY
    Return apiWindowFromPoint(m.pointx, m.pointy)
Endfunc

********************************************************************************
********************************************************************************


********************************************************************************
*!* ctlGetOsVersion()
********************************************************************************
*!* Returns the operating system version in a NTDDI format
********************************************************************************
*!* NTDDI version constants
*!* /http://forums.microsoft.com/MSDN/ShowPost.aspx?PostID=2095548&SiteID=1
*!* #define NTDDI_WIN2K                         05000000
*!* #define NTDDI_WIN2KSP1                      05000100
*!* #define NTDDI_WIN2KSP2                      05000200
*!* #define NTDDI_WIN2KSP3                      05000300
*!* #define NTDDI_WIN2KSP4                      05000400

*!* #define NTDDI_WINXP                         05010000
*!* #define NTDDI_WINXPSP1                      05010100
*!* #define NTDDI_WINXPSP2                      05010200
*!* #define NTDDI_WINXPSP3                      05010300
*!* #define NTDDI_WINXPSP4                      05010400

*!* #define NTDDI_WS03                          05020000
*!* #define NTDDI_WS03SP1                       05020100
*!* #define NTDDI_WS03SP2                       05020200
*!* #define NTDDI_WS03SP3                       05020300
*!* #define NTDDI_WS03SP4                       05020400

*!* #define NTDDI_WIN6                          06000000
*!* #define NTDDI_WIN6SP1                       06000100
*!* #define NTDDI_WIN6SP2                       06000200
*!* #define NTDDI_WIN6SP3                       06000300
*!* #define NTDDI_WIN6SP4                       06000400
*!* #define NTDDI_VISTA                         NTDDI_WIN6
*!* #define NTDDI_VISTASP1                      NTDDI_WIN6SP1
*!* #define NTDDI_VISTASP2                      NTDDI_WIN6SP2
*!* #define NTDDI_VISTASP3                      NTDDI_WIN6SP3
*!* #define NTDDI_VISTASP4                      NTDDI_WIN6SP4
*!* #define NTDDI_WS08                          NTDDI_WIN6SP1
*!* #define NTDDI_WS08SP2                       NTDDI_WIN6SP2
*!* #define NTDDI_WS08SP3                       NTDDI_WIN6SP3
*!* #define NTDDI_WS08SP4                       NTDDI_WIN6SP4
*!* #define NTDDI_LONGHORN  					NTDDI_VISTA
********************************************************************************
Function ctlGetOsVersion()
    Return Val(Os(3)) * 1000000 + Val(Os(4)) * 10000 + Val(Os(8)) * 100 + Val(Os(9))
Endfunc


********************************************************************************
Function ctlFontHeightToPoints(pnHeight As Integer)

    Local ;
        lnDc As Integer, ;
        lnLogPixelsY As Integer

    m.lnDc = apiGetDC(HWND_DESKTOP)
    m.lnLogPixelsY = apiGetDeviceCaps(m.lnDc, LOGPIXELSY)
    apiReleaseDC(HWND_DESKTOP, m.lnDc)
    Return Round(72 * m.pnHeight / m.lnLogPixelsY * (-1), 0)
Endfunc

********************************************************************************
Function ctlFontPointsToHeight(pnPoints As Integer)

    Local ;
        lnDc As Integer, ;
        lnLogPixelsY As Integer

    m.lnDc = apiGetDC(HWND_DESKTOP)
    m.lnLogPixelsY = apiGetDeviceCaps(m.lnDc, LOGPIXELSY)
    apiReleaseDC(HWND_DESKTOP, m.lnDc)
    Return apiMulDiv(m.pnPoints, m.lnLogPixelsY, 72) * (-1)
Endfunc


********************************************************************************
*!* ctlGetLangId
********************************************************************************
*!* pnLocale could be LOCALE_USER_DEFAULT, LOCALE_SYSTEM_DEFAULT
*!* /http://msdn.microsoft.com/library/default.asp?url=/library/en-us/intl/nls_34rz.asp
*!* /http://msdn.microsoft.com/library/default.asp?url=/library/en-us/intl/nls_8xo3.asp
*!* /http://msdn.microsoft.com/library/default.asp?url=/library/en-us/intl/nls_61df.asp
*!* Sets the value of strings that are language dependant.
********************************************************************************
Function ctlGetLangId(m.pnLocale As Integer, m.pnWhatToReturn As Integer)

    Local ;
        m.lnLocale As Integer, ;
        m.lcLocaleInfo As String, ;
        m.lnCharsRet As Integer, ;
        m.lnLangID As Integer, ;
        m.lnPrimaryLangID As Integer, ;
        m.lnSubLangID As Integer, ;
        m.lnRetVal As Integer

    If Pcount() > 0 .And. Vartype(m.pnLocale) = T_NUMERIC
        m.lnLocale = m.pnLocale
    Else
        m.lnLocale = LOCALE_SYSTEM_DEFAULT
    Endif

    m.lcLocaleInfo = Replicate(NULCHAR, 5)
    m.lnCharsRet = apiGetLocaleInfo(m.pnLocale , LOCALE_ILANGUAGE, @m.lcLocaleInfo, Len(m.lcLocaleInfo))
    *!* remove nul chars
    m.lcLocaleInfo = Strtran(m.lcLocaleInfo, NULCHAR, "")
    *!* m.lcLocaleInfo is now a character representation of a hex number
    m.lnLangID = Evaluate("0x" + m.lcLocaleInfo)
    *!*	m.lnPrimaryLangID = Bitand(m.lnLangID, 0x3FF)
    *!*	m.lnSubLangID = Bitrshift(m.lnLangID, 10)

    Do Case
        Case Pcount() < 2 Or m.pnWhatToReturn = 1
            m.lnRetVal =  m.lnLangID
        Case m.pnWhatToReturn = 2
            m.lnRetVal =  Bitand(m.lnLangID, 0x3FF)
        Case m.pnWhatToReturn = 3
            m.lnRetVal =  Bitrshift(m.lnLangID, 10)
        Otherwise
            m.lnRetVal =  m.lnLangID
    Endcase

    Return m.lnRetVal
Endfunc

********************************************************************************
*!* pnLocale could be LOCALE_USER_DEFAULT, LOCALE_SYSTEM_DEFAULT
********************************************************************************
Function ctlGetPrimaryLangID(m.pnLocale As Integer)
    Return ctlGetLangId(m.pnLocale, 2)
Endfunc

********************************************************************************
*!* pnLocale could be LOCALE_USER_DEFAULT, LOCALE_SYSTEM_DEFAULT
********************************************************************************
Function ctlGetSubLangID(m.pnLocale As Integer)
    Return ctlGetLangId(m.pnLocale, 3)
Endfunc

********************************************************************************
Function ctlGetPrimaryLangIDFromLangID(m.pnLangID As Integer)
    Return Bitand(m.pnLangID, 0x3FF)
Endfunc

********************************************************************************
Function ctlGetSubLangIDFromLangID(m.pnLangID As Integer)
    Return Bitrshift(m.pnLangID, 10)
Endfunc


***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************

********************************************************************************
*!* ctlGetHostHWnd(Object)
********************************************************************************
*!* Returns the HWnd of the form that contains an object, if any
*!* If the object has no parent, returns _VFP.HWnd
*!* If the form is a top level form or has scrollbars,
*!* returns the HWnd of the inner window.
*!* Parameter can be a form, toolbar or control
********************************************************************************
Function ctlGetHostHWnd(m.poObject As Object)

    Local m.nhWnd As Integer

    *!* traverse the object hierarchy until we find the form:
    *!* Modified 20070603 to check for toolbars
    *!*		Do While !Inlist(Upper(m.poObject.BaseClass), "FORM", "TOOLBAR")
    *!*			m.poObject = m.poObject.Parent
    *!*		Enddo

    *!* 20080405 modified to check for HWnd property of parent
    *!* and to check if parent is an object

    *!* Check if we already have a form or toolbar:
    If Inlist(Upper(m.poObject.BaseClass), "FORM", "TOOLBAR")
        m.nhWnd = m.poObject.HWnd
    Else
        *!* Check if object is contained in another object:
        If Vartype(m.poObject.Parent) = T_UNDEFINED
            m.nhWnd = _vfp.HWnd
        Else
            *!* Get first parent:
            m.poObject = m.poObject.Parent

            *!* Traverse object hierarchy upwards until we find an object with a HWnd:
            Do While !Pemstatus(m.poObject, "HWnd", 5)
                If Vartype(m.poObject.Parent) = T_UNDEFINED
                    m.nhWnd = _vfp.HWnd
                    Exit
                Else
                    m.poObject = m.poObject.Parent
                Endif
            Enddo
            m.nhWnd = m.poObject.HWnd
        Endif
    Endif

    *!* If we have a form, and the form is a top level form, or it has scrollbars,
    *!* get hWnd of inner window:
    If Upper(m.poObject.BaseClass) = "FORM"
        If m.poObject.ShowWindow = 2 Or m.poObject.ScrollBars > 0
            *!* Get hWnd of client window of Top Level Form //Craig Boyd//
            If Version(CON_VER_NUM) >= 900
                m.nhWnd = Sys(2327, Sys(2325, Sys(2326, m.poObject.HWnd)))
            Else
                m.nhWnd = apiGetWindow(m.poObject.HWnd, GW_CHILD)
            Endif
        Endif
    Endif

    m.poObject = .Null.
    Release m.poObject

    Return m.nhWnd
Endfunc

********************************************************************************
*!* ctlGetHostHWnd()
*!* Returns the HWnd of a form, or the HWnd of the inner window in case of
*!* top level forms or forms with scrollbars
*!* Parameter can be a form, toolbar or control
********************************************************************************
Function ctlGetHostHWnd(m.poObject As Object)

    Local ;
        m.loForm As Form, ;
        m.nhWnd As Integer

    *!* traverse the object hierarchy until we find the form:
    *!* Modified 20070603 to check for toolbars
    Do While !Inlist(Upper(m.poObject.BaseClass), "FORM", "TOOLBAR")
        m.poObject = m.poObject.Parent
    Enddo

    m.loForm = m.poObject
    m.poObject = Null
    m.nhWnd = m.loForm.HWnd

    *!* If the form is a top level form, or it has scrollbars, get hWnd of inner window:
    If Upper(m.loForm.BaseClass) = "FORM"
        If m.loForm.ShowWindow = 2 Or m.loForm.ScrollBars > 0
            *!* Get hWnd of client window of Top Level Form //Craig Boyd//
            If Version(CON_VER_NUM) >= 900
                m.nhWnd = Sys(2327, Sys(2325, Sys(2326, m.loForm.HWnd)))
            Else
                m.nhWnd = apiGetWindow(m.loForm.HWnd, GW_CHILD)
            Endif
        Endif
    Endif

    m.loForm = Null

    Return m.nhWnd
Endfunc



***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************


********************************************************************************
*!*	int GET_X_LPARAM(
*!*	    LPARAM lParam
*!*	);
*!*	Parameters
*!*	lParam
*!*	Specifies the value to be converted.
*!*	Return Value
*!*	The return value is the low-order int of the specified value.
********************************************************************************
Function ctlGet_X_lParam(m.pnLparam As Integer)
    Return Bitand(m.pnLparam, 0xFFFF)
Endfunc



********************************************************************************
*!*	int GET_Y_LPARAM(
*!*	    LPARAM lParam
*!*	);
*!*	Parameters
*!*	lParam
*!*	Specifies the value to be converted.
*!*	Return Value
*!*	The return value is the high-order int of the specified value.
********************************************************************************
Function ctlGet_Y_lParam(m.pnLparam As Integer)
    Return Bitrshift(m.pnLparam, 16)		&& Bitand(Int(m.pnlParam / 0x10000), 0xFFFF)
Endfunc

********************************************************************************
*!*	Word HIWORD(
*!*	DWORD dwValue
*!*	);
*!*	Parameters
*!*	dwValue
*!*	Specifies the Value To be converted.
*!*	Return Value
*!*	the Return Value Is the High-Order Word Of the specified Value.
********************************************************************************
Function ctlHiWord(m.pnLparam As Integer)
    Return Bitrshift(m.pnLparam, 16)		&& Bitand(Int(m.pnlParam / 0x10000), 0xFFFF)
Endfunc

********************************************************************************
*!*	Word LOWORD(
*!*	DWORD dwValue
*!*	);
*!*	Parameters
*!*	dwValue
*!*	Specifies the Value To be converted.
*!*	Return Value
*!*	the Return Value Is the low-Order Word Of the specified Value.
********************************************************************************
Function ctlLoWord(m.pnLparam As Integer)
    Return Bitand(m.pnLparam, 0xFFFF)
Endfunc

********************************************************************************
*!*	LPARAM MAKELPARAM(
*!*	    WORD wLow,
*!*	    WORD wHigh
*!*	);
*!*	Parameters
*!*	wLow
*!*	Specifies the low-order word of the new value.
*!*	wHigh
*!*	Specifies the high-order word of the new value.
*!*	Return Value
*!*	The return value is an LPARAM value.
********************************************************************************
Function ctlMakelParam(m.pnLow As Integer, m.pnHigh As Integer)
    Return m.pnLow + m.pnHigh * 0x10000	&& Bitor(m.wLow, BITLSHIFT(m.wHigh, 16)
Endfunc

********************************************************************************
*!*	POINT
*!*	The POINT structure defines the x- and y- coordinates of a point.
*!*	typedef struct tagPOINT {
*!*	  LONG x;
*!*	  LONG y;
*!*	} POINT, *PPOINT;
*!*	Members
*!*	x
*!*	Specifies the x-coordinate of the point.
*!*	y
*!*	Specifies the y-coordinate of the point.
*!* This is not a Windows Macro
********************************************************************************
Function ctlMakePoint(m.pnX As Integer, m.pnY As Integer)
    Return BinToC(m.pnX, "4RS") + BinToC(m.pnY, "4RS")
Endfunc


********************************************************************************
*!*	POINT
*!*	The POINT structure defines the x- and y- coordinates of a point.
*!*	typedef struct tagPOINT {
*!*	  LONG x;
*!*	  LONG y;
*!*	} POINT, *PPOINT;
*!*	Members
*!*	x
*!*	Specifies the x-coordinate of the point.
*!*	y
*!*	Specifies the y-coordinate of the point.
*!* This is not a Windows Macro
********************************************************************************
Function ctlGetXFromPoint(m.pcPoint As String)
    Return CToBin(Substr(m.pcPoint, 1, 4), "4RS")
Endfunc

********************************************************************************
*!*	POINT
*!*	The POINT structure defines the x- and y- coordinates of a point.
*!*	typedef struct tagPOINT {
*!*	  LONG x;
*!*	  LONG y;
*!*	} POINT, *PPOINT;
*!*	Members
*!*	x
*!*	Specifies the x-coordinate of the point.
*!*	y
*!*	Specifies the y-coordinate of the point.
*!* This is not a Windows Macro
********************************************************************************
Function ctlGetYFromPoint(m.pcPoint As String)
    Return CToBin(Substr(m.pcPoint, 5, 4), "4RS")
Endfunc


********************************************************************************
*!* END ctl32_functions
********************************************************************************




********************************************************************************
*!* This is the base class for all structure classes below
********************************************************************************
Define Class _STRUCTBASE As Exception

    Address = 0
    InitMembersDone = .F.
    Protected Embedded
    Embedded = .F.
    Name = "STRUCTBASE"
    SizeOf = 0
    Value = .F.

    Protected _MemberData
    _MemberData = '<VFPData>' + ;
        '<memberdata name="address" type="property" display="Address"/>' + ;
        '<memberdata name="initmembersdone" type="method" display="InitMembersDone"/>' + ;
        '<memberdata name="embedded" type="method" display="Embedded"/>' + ;
        '<memberdata name="name" type="property" display="Name"/>' + ;
        '<memberdata name="sizeof" type="property" display="SizeOf"/>' + ;
        '<memberdata name="value" type="property" display="Value"/>' + ;
        '</VFPData>'

    Protected Procedure Init(pnAddress)
        If Vartype(m.pnAddress) # "N"
            This.Address = AllocMem(This.SizeOf)
            If This.Address = 0
                Error(43)
                Return .F.
            Endif
        Else
            This.Address = m.pnAddress
            This.Embedded = .T.
        Endif
        This.InitMembers()
        This.InitMembersDone = .T.
    Endproc

    Protected Procedure Destroy()
        This.FreeMembers()
        If !This.Embedded
            FreeMem(This.Address)
        Endif
    Endproc

    Procedure InitMembers()
        *!* Here we set up initial values, like size of structure member
        *!* And also create aditional substructures
    Endproc

    Procedure FreeMembers()
        *!* Here we free aditional substructures
    Endproc

    Hidden Procedure Value_Access()
        *!* This returns the whole structure
        Return Sys(2600, This.Address, This.SizeOf)
    Endproc

    Hidden Procedure Value_Assign(pnNewVal)
        *!* This assigns a string to the structure as a whole
        Sys(2600, This.Address, This.SizeOf, m.pnNewVal)
    Endproc

    *!*		Protected Procedure AllocMem(pnLength)
    *!*			*!* Allocates memory
    *!*			Return apiHeapAlloc(apiGetProcessHeap(), HEAP_ZERO_MEMORY, m.pnLength)
    *!*		Endproc

    *!*		Protected Procedure FreeMem(pnAddress)
    *!*			*!* Frees allocated memory
    *!*			Return apiHeapFree(apiGetProcessHeap(), 0, m.pnAddress)
    *!*		Endproc

    *!*		Protected Procedure ReadCharArray(pnAddress, pnLength)
    *!*			*!* Reads a string array of m.pnLength, stops at first nul char
    *!*			Local m.lcString As String
    *!*			m.lcString = Sys(2600, m.pnAddress, m.pnLength)
    *!*			Return Left(m.lcString , At(NULCHAR, m.lcString) - 1)
    *!*		Endproc

    *!*		Protected Procedure WriteCharArray(pnAddress, pcCharArray, pnLength)
    *!*			*!* Writes a string array of m.pnLength
    *!*			=Sys(2600, m.pnAddress, Min(Len(m.pcCharArray), m.pnLength) , m.pcCharArray)
    *!*			Return m.pnAddress
    *!*		Endproc

    *!*		Protected Procedure FreePMem(pnAddress)
    *!*			This.FreeMem(This.ReadUInt(m.pnAddress))
    *!*		Endproc

    *!*		Protected Procedure ReadPCString(pnAddress)
    *!*			*!* pnAddress is a pointer to a memory pointer
    *!*			Local m.lnAddress, m.lnLength, m.lcString
    *!*			*!* Get address pointer
    *!*			m.lnAddress = This.ReadUInt(m.pnAddress)
    *!*			If m.lnAddress <> 0
    *!*				*!* Get length of memory pointer:
    *!*				m.lnLength = apiHeapSize(apiGetProcessHeap(), 0, m.lnAddress)
    *!*			Else
    *!*				m.lnLength = 0
    *!*			Endif
    *!*			*!* Read value of string pointer, then reads string
    *!*			If m.lnLength > 0
    *!*				m.lcString = Sys(2600, m.lnAddress, m.lnLength)
    *!*				m.lcString = Left(m.lcString , At(NULCHAR, m.lcString) - 1)
    *!*			Else
    *!*				m.lcString = ""
    *!*			Endif
    *!*			?"m.lcString", m.lcString
    *!*			Return m.lcString
    *!*		Endproc


    Protected Procedure ReadPCString(pnAddress, pnLen)
        *!* pnAddress is a pointer to a memory pointer
        Local m.lnAddress, m.lnLength, m.lcString
        *!* Get address pointer
        m.lnAddress = ReadUInt(m.pnAddress)
        If m.lnAddress <> 0
            m.lcString = Sys(2600, m.lnAddress, m.pnLen)
        Else
            m.lcString = ""
        Endif
        Return m.lcString
    Endproc

    *!*		Protected Procedure WritePCString(pnAddress, pcString)
    *!*			Local m.lnAdress As Integer
    *!*			*!* Release memory
    *!*			This.FreePMem(m.pnAddress)
    *!*			*!* Get a new handle for the string:
    *!*			m.lnAddress = This.AllocMem(Len(m.pcString))
    *!*			*!* Save string to handle
    *!*			=Sys(2600, m.lnAddress, Len(m.pcString), m.pcString)
    *!*			*!* Save value of handle in memory pointer
    *!*			This.WriteUInt(m.pnAddress, m.lnAddress)
    *!*		Endproc

    *!*		Protected Procedure ReadUInt(pnAddress)
    *!*			*!* Reads an unsigned integer (4 bytes)
    *!*			Local m.lnUInt As Integer
    *!*			m.lnUInt = CToBin(Sys(2600, m.pnAddress, 4), "4rs")
    *!*			If m.lnUInt < 0
    *!*				m.lnUInt = m.lnUInt + 0x100000000
    *!*			Endif
    *!*			Return m.lnUInt
    *!*		Endproc

    *!*		Protected Procedure WriteUInt(pnAddress, pnUInt)
    *!*			*!* Writes an unsigned integer (4 bytes)
    *!*			=Sys(2600, m.pnAddress, 4, BinToC(m.pnUInt, "4rs"))
    *!*		Endproc

    *!*		Protected Procedure ReadUShort(pnAddress)
    *!*			Local m.lnShort As Short
    *!*			m.lnShort = CToBin(Sys(2600, m.pnAddress, 2), "2rs")
    *!*			If m.lnShort < 0
    *!*				m.lnShort = m.lnShort + 0x10000
    *!*			Endif
    *!*			Return m.lnShort
    *!*		Endproc

    *!*		Protected Procedure WriteUShort(pnAddress, pnShort)
    *!*			*!* Writes an unsigned short (2 bytes)
    *!*			=Sys(2600, m.pnAddress, 2, BinToC(m.pnShort, "2rs"))
    *!*		Endproc

    *!*		Protected Procedure ReadPointer(pnAddress)
    *!*			*!* Reads an integer (4 bytes)
    *!*			Return CToBin(Sys(2600, m.pnAddress, 4), "4rs")
    *!*		Endproc

    *!*		Protected Procedure WritePointer(pnAddress, pnInt)
    *!*			*!* Writes an integer (4 bytes)
    *!*			=Sys(2600, m.pnAddress, 4, BinToC(m.pnInt, "4rs"))
    *!*		Endproc

    *!*		Protected Procedure ReadInt(pnAddress)
    *!*			*!* Reads an integer (4 bytes)
    *!*			Return CToBin(Sys(2600, m.pnAddress, 4), "4rs")
    *!*		Endproc

    *!*		Protected Procedure WriteInt(pnAddress, pnInt)
    *!*			*!* Writes an integer (4 bytes)
    *!*			=Sys(2600, m.pnAddress, 4, BinToC(m.pnInt, "4rs"))
    *!*		Endproc

    Protected Procedure ReadByte(pnAddress)
        *!* Reads an integer (1 bytes)
        Return CToBin(Sys(2600, m.pnAddress, 1), "1rs")
    Endproc

    Protected Procedure WriteByte(pnAddress, pnInt)
        *!* Writes an integer (1 bytes)
        =Sys(2600, m.pnAddress, 1, BinToC(m.pnInt, "1rs"))
    Endproc

    *!*		Protected Procedure ReadPointer(pnAddress)
    *!*			*!* Reads an integer (4 bytes)
    *!*			Return This.ReadUInt(m.pnAddress)
    *!*		Endproc

    *!*		Protected Procedure WritePointer(pnAddress, pnInt)
    *!*			*!* Writes an integer (4 bytes)
    *!*			Return This.WriteUInt(m.pnAddress, m.pnInt)
    *!*		Endproc

Enddefine

********************************************************************************
*!*	typedef struct tagLOGFONT {
*!*	  LONG lfHeight;
*!*	  LONG lfWidth;
*!*	  LONG lfEscapement;
*!*	  LONG lfOrientation;
*!*	  LONG lfWeight;
*!*	  BYTE lfItalic;
*!*	  BYTE lfUnderline;
*!*	  BYTE lfStrikeOut;
*!*	  BYTE lfCharSet;
*!*	  BYTE lfOutPrecision;
*!*	  BYTE lfClipPrecision;
*!*	  BYTE lfQuality;
*!*	  BYTE lfPitchAndFamily;
*!*	  TCHAR lfFaceName[LF_FACESIZE];
*!*	} LOGFONT, *PLOGFONT;
********************************************************************************
Define Class _LOGFONT As _STRUCTBASE
    SizeOf = 60
    Name = "LOGFONT"
    && structure fields
    lfHeight = .F.
    lfWidth = .F.
    lfEscapement = .F.
    lfOrientation = .F.
    lfWeight = .F.
    lfItalic = .F.
    lfUnderline = .F.
    lfStrikeOut = .F.
    lfCharSet = .F.
    lfOutPrecision = .F.
    lfClipPrecision = .F.
    lfQuality = .F.
    lfPitchAndFamily = .F.
    lfFaceName = .F.
    && extra properties
    FontBold = .F.
    FontCharSet = 1
    FontItalic = .F.
    FontName = ""
    FontSize = 0
    FontStrikethru = .F.
    FontUnderline = .F.

    _MemberData = '<VFPData>' + ;
        '<memberdata name="lfheight" type="property" display="lfHeight"/>' + ;
        '<memberdata name="lfwidth" type="property" display="lfWidth"/>' + ;
        '<memberdata name="lfescapement" type="property" display="lfEscapement"/>' + ;
        '<memberdata name="lforientation" type="property" display="lfOrientation"/>' + ;
        '<memberdata name="lfweight" type="property" display="lfWeight"/>' + ;
        '<memberdata name="lfitalic" type="property" display="lfItalic"/>' + ;
        '<memberdata name="lfunderline" type="property" display="lfUnderline"/>' + ;
        '<memberdata name="lfstrikeout" type="property" display="lfStrikeOut"/>' + ;
        '<memberdata name="lfcharset" type="property" display="lfCharSet"/>' + ;
        '<memberdata name="lfoutprecision" type="property" display="lfOutPrecision"/>' + ;
        '<memberdata name="lfclipprecision" type="property" display="lfClipPrecision"/>' + ;
        '<memberdata name="lfquality" type="property" display="lfQuality"/>' + ;
        '<memberdata name="lfpitchandfamily" type="property" display="lfPitchAndFamily"/>' + ;
        '<memberdata name="lffacename" type="property" display="lfFaceName"/>' + ;
        '<memberdata name="fontbold" type="property" display="FontBold"/>' + ;
        '<memberdata name="fontcharset" type="property" display="FontCharSet"/>' + ;
        '<memberdata name="fontitalic" type="property" display="FontItalic"/>' + ;
        '<memberdata name="fontname" type="property" display="FontName"/>' + ;
        '<memberdata name="fontsize" type="property" display="FontSize"/>' + ;
        '<memberdata name="fontstrikethru" type="property" display="FontStrikethru"/>' + ;
        '<memberdata name="fontunderline" type="property" display="FontUnderline"/>' + ;
        '</VFPData>'

    Procedure FontBold_Access()
        Return This.lfWeight >= FW_BOLD
    Endproc

    Procedure FontBold_Assign(vNewVal)
        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal < FW_BOLD
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif
        If m.vNewVal
            This.lfWeight = FW_BOLD
        Else
            This.lfWeight = FW_NORMAL
        Endif
    Endproc

    Procedure FontCharset_Access()
        Return This.lfCharSet
    Endproc

    Procedure FontCharset_Assign(vNewVal)
        This.lfCharSet = m.vNewVal
    Endproc

    Procedure FontItalic_Access()
        Return This.lfItalic <> 0
    Endproc

    Procedure FontItalic_Assign(vNewVal)
        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif
        If m.vNewVal
            This.lfItalic = 1
        Else
            This.lfItalic = 0
        Endif
    Endproc

    Procedure FontName_Access()
        Return This.lfFaceName
    Endproc

    Procedure FontName_Assign(vNewVal)
        If Right(m.vNewVal, 1) <> NULCHAR
            m.vNewVal = m.vNewVal + NULCHAR
        Endif
        This.lfFaceName = m.vNewVal
    Endproc

    Procedure FontSize_Access()
        Return ctlFontHeightToPoints(This.lfHeight)
    Endproc

    Procedure FontSize_Assign(vNewVal)
        This.lfHeight = ctlFontPointsToHeight(m.vNewVal)
    Endproc

    Procedure FontStrikethru_Access()
        Return This.lfStrikeOut <> 0
    Endproc

    Procedure FontStrikethru_Assign(vNewVal)
        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif
        If m.vNewVal
            This.lfStrikeOut = 1
        Else
            This.lfStrikeOut = 0
        Endif
    Endproc

    Procedure FontUnderline_Access()
        Return This.lfUnderline <> 0
    Endproc

    Procedure FontUnderline_Assign(vNewVal)
        If Vartype(m.vNewVal) = T_NUMERIC
            If m.vNewVal = 0
                m.vNewVal = .F.
            Else
                m.vNewVal = .T.
            Endif
        Endif
        If m.vNewVal
            This.lfUnderline = 1
        Else
            This.lfUnderline = 0
        Endif
    Endproc

    Procedure lfHeight_Access()
        Return ReadInt(This.Address)
    Endproc

    Procedure lfHeight_Assign(pnNewVal)
        WriteInt(This.Address, m.pnNewVal)
    Endproc

    Procedure lfWidth_Access()
        Return ReadInt(This.Address + 4)
    Endproc

    Procedure lfWidth_Assign(pnNewVal)
        WriteInt(This.Address + 4, m.pnNewVal)
    Endproc

    Procedure lfEscapement_Access()
        Return ReadInt(This.Address + 8)
    Endproc

    Procedure lfEscapement_Assign(pnNewVal)
        WriteInt(This.Address + 8, m.pnNewVal)
    Endproc

    Procedure lfOrientation_Access()
        Return ReadInt(This.Address + 12)
    Endproc

    Procedure lfOrientation_Assign(pnNewVal)
        WriteInt(This.Address + 12, m.pnNewVal)
    Endproc

    Procedure lfWeight_Access()
        Return ReadInt(This.Address + 16)
    Endproc

    Procedure lfWeight_Assign(pnNewVal)
        WriteInt(This.Address + 16, m.pnNewVal)
    Endproc

    Procedure lfItalic_Access()
        Return This.ReadByte(This.Address + 20)
    Endproc

    Procedure lfItalic_Assign(pnNewVal)
        This.WriteByte(This.Address + 20, m.pnNewVal)
    Endproc

    Procedure lfUnderline_Access()
        Return This.ReadByte(This.Address + 21)
    Endproc

    Procedure lfUnderline_Assign(pnNewVal)
        This.WriteByte(This.Address + 21, m.pnNewVal)
    Endproc

    Procedure lfStrikeOut_Access()
        Return This.ReadByte(This.Address + 22)
    Endproc

    Procedure lfStrikeOut_Assign(pnNewVal)
        This.WriteByte(This.Address + 22, m.pnNewVal)
    Endproc

    Procedure lfCharSet_Access()
        Return This.ReadByte(This.Address + 23)
    Endproc

    Procedure lfCharSet_Assign(pnNewVal)
        This.WriteByte(This.Address + 23, m.pnNewVal)
    Endproc

    Procedure lfOutPrecision_Access()
        Return This.ReadByte(This.Address + 24)
    Endproc

    Procedure lfOutPrecision_Assign(pnNewVal)
        This.WriteByte(This.Address + 24, m.pnNewVal)
    Endproc

    Procedure lfClipPrecision_Access()
        Return This.ReadByte(This.Address + 25)
    Endproc

    Procedure lfClipPrecision_Assign(pnNewVal)
        This.WriteByte(This.Address + 25, m.pnNewVal)
    Endproc

    Procedure lfQuality_Access()
        Return This.ReadByte(This.Address + 26)
    Endproc

    Procedure lfQuality_Assign(pnNewVal)
        This.WriteByte(This.Address + 26, m.pnNewVal)
    Endproc

    Procedure lfPitchAndFamily_Access()
        Return This.ReadByte(This.Address + 27)
    Endproc

    Procedure lfPitchAndFamily_Assign(pnNewVal)
        This.WriteByte(This.Address + 27, m.pnNewVal)
    Endproc

    Procedure lfFaceName_Access()
        Return ReadCharArray(This.Address + 28, 32)
    Endproc

    Procedure lfFaceName_Assign(pnNewVal)
        WriteCharArray(This.Address + 28, m.pnNewVal, 32)
    Endproc

Enddefine

********************************************************************************
*!*	typedef struct _RECT {
*!*	  LONG Left;
*!*	  LONG Top;
*!*	  LONG Right;
*!*	  LONG Bottom;
*!*	} RECT, *PRECT;
********************************************************************************
Define Class _RECT As _STRUCTBASE

    SizeOf = 16
    Name = "RECT"
    *!*  structure fields
    Left = .F.
    Top = .F.
    Right = .F.
    Bottom = .F.
    && Extra properties
    Height = .F.
    Width = .F.

    Protected _MemberData
    _MemberData = '<VFPData>' + ;
        '<memberdata name="left" type="property" display="Left"/>' + ;
        '<memberdata name="top" type="property" display="Top"/>' + ;
        '<memberdata name="right" type="property" display="Right"/>' + ;
        '<memberdata name="bottom" type="property" display="Bottom"/>' + ;
        '<memberdata name="height" type="property" display="Height"/>' + ;
        '<memberdata name="width" type="property" display="Width"/>' + ;
        '</VFPData>'

    Hidden Procedure Left_Access()
        Return ReadInt(This.Address)
    Endproc

    Hidden Procedure Left_Assign(pnNewVal)
        WriteInt(This.Address, m.pnNewVal)
    Endproc

    Hidden Procedure Top_Access()
        Return ReadInt(This.Address + 4)
    Endproc

    Hidden Procedure Top_Assign(pnNewVal)
        WriteInt(This.Address + 4, m.pnNewVal)
    Endproc

    Hidden Procedure Right_Access()
        Return ReadInt(This.Address + 8)
    Endproc

    Hidden Procedure Right_Assign(pnNewVal)
        WriteInt(This.Address + 8, m.pnNewVal)
    Endproc

    Hidden Procedure Bottom_Access()
        Return ReadInt(This.Address + 12)
    Endproc

    Hidden Procedure Bottom_Assign(pnNewVal)
        WriteInt(This.Address + 12, m.pnNewVal)
    Endproc

    Hidden Procedure Height_Access()
        Return This.Bottom - This.Top
    Endproc

    Hidden Procedure Height_Assign(pnNewVal)
        This.Height = m.pnNewVal
        This.Bottom = This.Top + m.pnNewVal
    Endproc

    Hidden Procedure Width_Access()
        Return This.Right - This.Left
    Endproc

    Hidden Procedure Width_Assign(pnNewVal)
        This.Width = m.pnNewVal
        This.Right = This.Left + m.pnNewVal
    Endproc

Enddefine

********************************************************************************
*!*	typedef struct tagNMHDR {
*!*	  HWND hwndFrom;
*!*	  UINT_PTR idFrom;
*!*	  UINT code;
*!*	} NMHDR;
********************************************************************************
Define Class _NMHDR As _STRUCTBASE

    SizeOf = 12
    Name = "NMHDR"
    && structure fields
    hwndFrom = .F.
    idFrom = .F.
    Code = .F.

    _MemberData = '<VFPData>' + ;
        '<memberdata name="hwndfrom" type="property" display="hwndFrom"/>' + ;
        '<memberdata name="idfrom" type="property" display="idFrom"/>' + ;
        '<memberdata name="code" type="property" display="Code"/>' + ;
        '</VFPData>'

    Procedure hwndFrom_Access()
        Return ReadInt(This.Address)
    Endproc

    Procedure hwndFrom_Assign(pnNewVal)
        WriteInt(This.Address, m.pnNewVal)
    Endproc

    Procedure idFrom_Access()
        Return ReadUInt(This.Address + 4)
    Endproc

    Procedure idFrom_Assign(pnNewVal)
        WriteUInt(This.Address + 4, m.pnNewVal)
    Endproc

    Procedure Code_Access()
        *!* Code is a UINT but it should be an INT
        Return ReadInt(This.Address + 8)
    Endproc

    Procedure Code_Assign(pnNewVal)
        WriteInt(This.Address + 8, m.pnNewVal)
    Endproc

Enddefine

********************************************************************************
*!*	typedef struct tagTOOLINFO{
*!*	    UINT      cbSize;
*!*	    UINT      uFlags;
*!*	    HWND      hwnd;
*!*	    UINT_PTR  uId;
*!*	    RECT      rect;
*!*	    HINSTANCE hinst;
*!*	    LPTSTR    lpszText;
*!*	#if (_WIN32_IE >= 0x0300)
*!*	    LPARAM lParam;
*!*	#endif
*!*	#if (_WIN32_WINNT >= 0x0501)
*!*	    void *lpReserved;
*!*	#endif
*!*	} TOOLINFO, NEAR *PTOOLINFO, *LPTOOLINFO;
********************************************************************************
Define Class _TOOLINFO As _STRUCTBASE
    SizeOf = 48
    Name = "TOOLINFO"
    && structure fields
    cbSize = .F.
    uFlags = .F.
    HWnd = .F.
    uId = .F.
    Rect = .Null.
    hInst = .F.
    lpszText = .F.
    Lparam = .F.
    *!* if ctlGetOsVersion >= 5010000
    lpReserved = .F.
    *!* Endif

    _MemberData = '<VFPData>' + ;
        '<memberdata name="cbsize" type="property" display="cbSize"/>' + ;
        '<memberdata name="uflags" type="property" display="uFlags"/>' + ;
        '<memberdata name="hwnd" type="property" display="hwnd"/>' + ;
        '<memberdata name="uid" type="property" display="uId"/>' + ;
        '<memberdata name="rect" type="property" display="rect"/>' + ;
        '<memberdata name="hinst" type="property" display="hinst"/>' + ;
        '<memberdata name="lpsztext" type="property" display="lpszText"/>' + ;
        '<memberdata name="lparam" type="property" display="lParam"/>' + ;
        '<memberdata name="*lpreserved" type="property" display="*lpReserved"/>' + ;
        '</VFPData>'

    Procedure InitMembers()

        *!* 20080818
        *!*			If ctlGetOsVersion() < NTDDI_WINXP
        *!*				This.SizeOf = 44
        *!*			Else
        *!*				This.SizeOf = 48
        *!*			Endif
        *!*			This.cbSize = This.SizeOf

        If ctlGetOsVersion() < NTDDI_WINXP
            This.cbSize = 44
        Else
            This.cbSize = 48
        Endif

        This.Rect = Createobject("_RECT", This.Address + 16)
    Endproc

    Procedure FreeMembers()
        This.Rect = .Null.
        FreePMem(This.Address + 36)
    Endproc

    Procedure Address_Assign(lnAddress)
        This.Address = m.lnAddress
        If This.InitMembersDone
            This.Rect.Address = m.lnAddress + 16
        Endif
    Endproc

    Procedure cbSize_Access()
        Return ReadUInt(This.Address)
    Endproc

    Procedure cbSize_Assign(pnNewVal)
        WriteUInt(This.Address, m.pnNewVal)
    Endproc

    Procedure uFlags_Access()
        Return ReadUInt(This.Address + 4)
    Endproc

    Procedure uFlags_Assign(pnNewVal)
        WriteUInt(This.Address + 4, m.pnNewVal)
    Endproc

    Procedure hwnd_Access()
        Return ReadInt(This.Address + 8)
    Endproc

    Procedure hwnd_Assign(pnNewVal)
        WriteInt(This.Address + 8, m.pnNewVal)
    Endproc

    Procedure uId_Access()
        Return ReadUInt(This.Address + 12)
    Endproc

    Procedure uId_Assign(pnNewVal)
        WriteUInt(This.Address + 12, m.pnNewVal)
    Endproc

    Procedure hinst_Access()
        Return ReadPointer(This.Address + 32)
    Endproc

    Procedure hinst_Assign(pnNewVal)
        WritePointer(This.Address + 32, m.pnNewVal)
    Endproc

    Procedure lpszText_Access()
        Return ReadPCString(This.Address + 36)
    Endproc

    Procedure lpszText_Assign(pnNewVal)
        WritePCString(This.Address + 36, m.pnNewVal)
    Endproc

    Procedure lParam_Access()
        Return ReadInt(This.Address + 40)
    Endproc

    Procedure lParam_Assign(pnNewVal)
        WriteInt(This.Address + 40, m.pnNewVal)
    Endproc

    Procedure lpReserved_Access()
        Return ReadPointer(This.Address  +  44)
    Endproc

    Procedure lpReserved_Assign(pnNewVal)
        WritePointer(This.Address + 44, m.pnNewVal)
Enddefine

*!* don't call this function ...
*!* it's only purpose is to make the function names visible for compilation
*!* the functions are DECLARE'd inside the library automatically
Function VFP2CDummyProc
    Declare WriteChar In vfp2c32.fll Integer, String
    Declare WritePChar In vfp2c32.fll Integer, String
    Declare WriteInt8 In vfp2c32.fll Integer, Integer
    Declare WritePInt8 In vfp2c32.fll Integer, Integer
    Declare WriteUInt8 In vfp2c32.fll Integer, Integer
    Declare WritePUInt8 In vfp2c32.fll Integer, Integer
    Declare WriteShort In vfp2c32.fll Integer, Short
    Declare WritePShort In vfp2c32.fll Integer, Short
    Declare WriteUShort In vfp2c32.fll Integer, Short
    Declare WritePUShort In vfp2c32.fll Integer, Short
    Declare WriteInt In vfp2c32.fll Integer, Integer
    Declare WritePInt In vfp2c32.fll Integer, Integer
    Declare WriteUInt In vfp2c32.fll Integer, Integer
    Declare WritePUInt In vfp2c32.fll Integer, Integer
    Declare WritePointer In vfp2c32.fll Integer, Integer
    Declare WritePPointer In vfp2c32.fll Integer, Integer
    Declare WriteFloat In vfp2c32.fll Integer, Single
    Declare WritePFloat In vfp2c32.fll Integer, Single
    Declare WriteDouble In vfp2c32.fll Integer, Double
    Declare WritePDouble In vfp2c32.fll Integer, Double
    Declare Short ReadInt8 In vfp2c32.fll Integer
    Declare Short ReadPInt8 In vfp2c32.fll Integer
    Declare Short ReadUInt8 In vfp2c32.fll Integer
    Declare Short ReadPUInt8 In vfp2c32.fll Integer
    Declare Short ReadShort In vfp2c32.fll Integer
    Declare Short ReadPShort In vfp2c32.fll Integer
    Declare Integer ReadUShort In vfp2c32.fll Integer
    Declare Integer ReadPUShort In vfp2c32.fll Integer
    Declare Integer ReadInt In vfp2c32.fll Integer
    Declare Integer ReadPInt In vfp2c32.fll Integer
    Declare Single ReadFloat In vfp2c32.fll Integer
    Declare Single ReadPFloat In vfp2c32.fll Integer
    Declare Double ReadDouble In vfp2c32.fll Integer
    Declare Double ReadPDouble In vfp2c32.fll Integer

    Declare AAverage In vfp2c32.fll
    Declare AbortUrlDownloadToFileEx In vfp2c32.fll
    Declare ADesktopArea In vfp2c32.fll
    Declare ADesktops In vfp2c32.fll
    Declare ADialUpConnections In vfp2c32.fll
    Declare ADirectoryInfo In vfp2c32.fll
    Declare ADirEx In vfp2c32.fll
    Declare ADisplayDevices In vfp2c32.fll
    Declare ADriveInfo In vfp2c32.fll
    Declare AErrorEx In vfp2c32.fll
    Declare AFHandlesEx In vfp2c32.fll
    Declare AFileAttributes In vfp2c32.fll
    Declare AFileAttributesEx In vfp2c32.fll
    Declare AHeapBlocks In vfp2c32.fll
    Declare AIPAddresses In vfp2c32.fll
    Declare AllocHGlobal In vfp2c32.fll
    Declare AllocMem In vfp2c32.fll
    Declare AllocMemTo In vfp2c32.fll
    Declare AMax In vfp2c32.fll
    Declare AMemBlocks In vfp2c32.fll
    Declare AMin In vfp2c32.fll
    Declare ANetFiles In vfp2c32.fll
    Declare ANetServers In vfp2c32.fll
    Declare APrinterForms In vfp2c32.fll
    Declare APrintersEx In vfp2c32.fll
    Declare APrinterTrays In vfp2c32.fll
    Declare APrintJobs In vfp2c32.fll
    Declare AProcesses In vfp2c32.fll
    Declare AProcessHeaps In vfp2c32.fll
    Declare AProcessModules In vfp2c32.fll
    Declare AProcessThreads In vfp2c32.fll
    Declare ARegistryKeys In vfp2c32.fll
    Declare ARegistryValues In vfp2c32.fll
    Declare AResolutions In vfp2c32.fll
    Declare AResourceLanguages In vfp2c32.fll
    Declare AResourceNames In vfp2c32.fll
    Declare AResourceTypes In vfp2c32.fll
    Declare AServices In vfp2c32.fll
    Declare AServiceStatus In vfp2c32.fll
    Declare ASplitStr In vfp2c32.fll
    Declare ASQLDataSources In vfp2c32.fll
    Declare ASQLDrivers In vfp2c32.fll
    Declare ASum In vfp2c32.fll
    Declare AsyncInvoke In vfp2c32.fll
    Declare ATimeZones In vfp2c32.fll
    Declare AWindowProps In vfp2c32.fll
    Declare AWindows In vfp2c32.fll
    Declare AWindowsEx In vfp2c32.fll
    Declare AWindowStations In vfp2c32.fll
    Declare BindEventsEx In vfp2c32.fll
    Declare CancelFileChange In vfp2c32.fll
    Declare CancelRegistryChange In vfp2c32.fll
    Declare CenterWindowEx In vfp2c32.fll
    Declare ChangeSQLDataSource In vfp2c32.fll
    Declare CloseRegistryKey In vfp2c32.fll
    Declare CloseServiceHandle In vfp2c32.fll
    Declare CLSIDFromProgID In vfp2c32.fll
    Declare CLSIDFromString In vfp2c32.fll
    Declare Colors2RGB In vfp2c32.fll
    Declare CompactMem In vfp2c32.fll
    Declare CompareFileTimes In vfp2c32.fll
    Declare ContinueService In vfp2c32.fll
    Declare ControlService In vfp2c32.fll
    Declare CopyFileEx In vfp2c32.fll
    Declare CreateCallbackFunc In vfp2c32.fll
    Declare CreateGuid In vfp2c32.fll
    Declare CreateRegistryKey In vfp2c32.fll
    Declare CreateSQLDataSource In vfp2c32.fll
    Declare Decimals In vfp2c32.fll
    Declare DeleteDirectory In vfp2c32.fll
    Declare DeleteFileEx In vfp2c32.fll
    Declare DeleteRegistryKey In vfp2c32.fll
    Declare DeleteSQLDataSource In vfp2c32.fll
    Declare DestroyCallbackFunc In vfp2c32.fll
    Declare Double2DT In vfp2c32.fll
    Declare Double2Str In vfp2c32.fll
    Declare DT2Double In vfp2c32.fll
    Declare DT2FT In vfp2c32.fll
    Declare DT2ST In vfp2c32.fll
    Declare DT2Timet In vfp2c32.fll
    Declare DT2UTC In vfp2c32.fll
    Declare ExpandEnvironmentStrings In vfp2c32.fll
    Declare FChSizeEx In vfp2c32.fll
    Declare FCloseEx In vfp2c32.fll
    Declare FCreateEx In vfp2c32.fll
    Declare FEoFEx In vfp2c32.fll
    Declare FFlushEx In vfp2c32.fll
    Declare FGetsEx In vfp2c32.fll
    Declare FHandleEx In vfp2c32.fll
    Declare FindFileChange In vfp2c32.fll
    Declare FindRegistryChange In vfp2c32.fll
    Declare Float2Str In vfp2c32.fll
    Declare FLockFile In vfp2c32.fll
    Declare FLockFileEx In vfp2c32.fll
    Declare FOpenEx In vfp2c32.fll
    Declare FormatMessageEx In vfp2c32.fll
    Declare FPutsEx In vfp2c32.fll
    Declare FReadEx In vfp2c32.fll
    Declare FreeHGlobal In vfp2c32.fll
    Declare FreeMem In vfp2c32.fll
    Declare FreePMem In vfp2c32.fll
    Declare FreeRefArray In vfp2c32.fll
    Declare FSeekEx In vfp2c32.fll
    Declare FT2DT In vfp2c32.fll
    Declare FUnlockFile In vfp2c32.fll
    Declare FUnlockFileEx In vfp2c32.fll
    Declare FWriteEx In vfp2c32.fll
    Declare GetCursorPosEx In vfp2c32.fll
    Declare GetFileAttributes In vfp2c32.fll
    Declare GetFileOwner In vfp2c32.fll
    Declare GetFileSize In vfp2c32.fll
    Declare GetFileTimes In vfp2c32.fll
    Declare GetIUnknown In vfp2c32.fll
    Declare GetLongPathName In vfp2c32.fll
    Declare GetOpenFileName In vfp2c32.fll
    Declare GetSaveFileName In vfp2c32.fll
    Declare GetServerTime In vfp2c32.fll
    Declare GetShortPathName In vfp2c32.fll
    Declare GetSystemDirectory In vfp2c32.fll
    Declare GetSystemTime In vfp2c32.fll
    Declare GetWindowRectEx In vfp2c32.fll
    Declare GetWindowsDirectory In vfp2c32.fll
    Declare GetWindowTextEx In vfp2c32.fll
    Declare IcmpPing In vfp2c32.fll
    Declare InitVFP2C32 In vfp2c32.fll
    Declare Int64_Add In vfp2c32.fll
    Declare Int64_Div In vfp2c32.fll
    Declare Int64_Mod In vfp2c32.fll
    Declare Int64_Mul In vfp2c32.fll
    Declare Int64_Sub In vfp2c32.fll
    Declare Invoke In vfp2c32.fll
    Declare Ip2MacAddress In vfp2c32.fll
    Declare IsEqualGuid In vfp2c32.fll
    Declare LockHGlobal In vfp2c32.fll
    Declare Long2Str In vfp2c32.fll
    Declare MarshalArrayCharArray In vfp2c32.fll
    Declare MarshalArrayCString In vfp2c32.fll
    Declare MarshalArrayDouble In vfp2c32.fll
    Declare MarshalArrayFloat In vfp2c32.fll
    Declare MarshalArrayInt In vfp2c32.fll
    Declare MarshalArrayLogical In vfp2c32.fll
    Declare MarshalArrayShort In vfp2c32.fll
    Declare MarshalArrayUInt In vfp2c32.fll
    Declare MarshalArrayUShort In vfp2c32.fll
    Declare MarshalArrayWCharArray In vfp2c32.fll
    Declare MarshalArrayWString In vfp2c32.fll
    Declare MarshalCursorCharArray In vfp2c32.fll
    Declare MarshalCursorCString In vfp2c32.fll
    Declare MarshalCursorDouble In vfp2c32.fll
    Declare MarshalCursorFloat In vfp2c32.fll
    Declare MarshalCursorInt In vfp2c32.fll
    Declare MarshalCursorLogical In vfp2c32.fll
    Declare MarshalCursorShort In vfp2c32.fll
    Declare MarshalCursorUInt In vfp2c32.fll
    Declare MarshalCursorUShort In vfp2c32.fll
    Declare MarshalCursorWCharArray In vfp2c32.fll
    Declare MarshalCursorWString In vfp2c32.fll
    Declare MoveFileEx In vfp2c32.fll
    Declare Num2Binary In vfp2c32.fll
    Declare OnLoad In vfp2c32.fll
    Declare OnUnload In vfp2c32.fll
    Declare OpenRegistryKey In vfp2c32.fll
    Declare OpenService In vfp2c32.fll
    Declare PauseService In vfp2c32.fll
    Declare PG_ByteA2Str In vfp2c32.fll
    Declare PG_Str2ByteA In vfp2c32.fll
    Declare ProgIDFromCLSID In vfp2c32.fll
    Declare ReadBytes In vfp2c32.fll
    Declare ReadChar In vfp2c32.fll
    Declare ReadCharArray In vfp2c32.fll
    Declare ReadCString In vfp2c32.fll
    Declare ReadInt64AsDouble In vfp2c32.fll
    Declare ReadLogical In vfp2c32.fll
    Declare ReadPChar In vfp2c32.fll
    Declare ReadPCString In vfp2c32.fll
    Declare ReadPInt64AsDouble In vfp2c32.fll
    Declare ReadPLogical In vfp2c32.fll
    Declare ReadPointer In vfp2c32.fll
    Declare ReadPPointer In vfp2c32.fll
    Declare ReadProcessMemoryEx In vfp2c32.fll
    Declare ReadPUInt In vfp2c32.fll
    Declare ReadPUInt64AsDouble In vfp2c32.fll
    Declare ReadPWString In vfp2c32.fll
    Declare ReadRegistryKey In vfp2c32.fll
    Declare ReadUInt In vfp2c32.fll
    Declare ReadUInt64AsDouble In vfp2c32.fll
    Declare ReadWCharArray In vfp2c32.fll
    Declare ReadWString In vfp2c32.fll
    Declare ReAllocHGlobal In vfp2c32.fll
    Declare ReAllocMem In vfp2c32.fll
    Declare RegisterActiveObject In vfp2c32.fll
    Declare RegisterObjectAsFileMoniker In vfp2c32.fll
    Declare RegistryHiveToObject In vfp2c32.fll
    Declare RegistryValuesToObject In vfp2c32.fll
    Declare ResolveHostToIp In vfp2c32.fll
    Declare RevokeActiveObject In vfp2c32.fll
    Declare RGB2Colors In vfp2c32.fll
    Declare SetFileAttributes In vfp2c32.fll
    Declare SetFileTimes In vfp2c32.fll
    Declare SetSystemTime In vfp2c32.fll
    Declare SetSystemTimeEx In vfp2c32.fll
    Declare SHBrowseFolder In vfp2c32.fll
    Declare SHCopyFiles In vfp2c32.fll
    Declare SHDeleteFiles In vfp2c32.fll
    Declare SHMoveFiles In vfp2c32.fll
    Declare Short2Str In vfp2c32.fll
    Declare SHRenameFiles In vfp2c32.fll
    Declare SHSpecialFolder In vfp2c32.fll
    Declare SizeOfMem In vfp2c32.fll
    Declare SQLExecEx In vfp2c32.fll
    Declare SQLGetPropEx In vfp2c32.fll
    Declare SQLSetPropEx In vfp2c32.fll
    Declare ST2DT In vfp2c32.fll
    Declare StartService In vfp2c32.fll
    Declare StopService In vfp2c32.fll
    Declare Str2Double In vfp2c32.fll
    Declare Str2Float In vfp2c32.fll
    Declare Str2Long In vfp2c32.fll
    Declare Str2Short In vfp2c32.fll
    Declare Str2ULong In vfp2c32.fll
    Declare Str2UShort In vfp2c32.fll
    Declare StringFromCLSID In vfp2c32.fll
    Declare SyncToServerTime In vfp2c32.fll
    Declare SyncToSNTPServer In vfp2c32.fll
    Declare Timet2DT In vfp2c32.fll
    Declare ULong2Str In vfp2c32.fll
    Declare UnBindEventsEx In vfp2c32.fll
    Declare UnlockHGlobal In vfp2c32.fll
    Declare UnMarshalArrayCharArray In vfp2c32.fll
    Declare UnMarshalArrayCString In vfp2c32.fll
    Declare UnMarshalArrayDouble In vfp2c32.fll
    Declare UnMarshalArrayFloat In vfp2c32.fll
    Declare UnMarshalArrayInt In vfp2c32.fll
    Declare UnMarshalArrayLogical In vfp2c32.fll
    Declare UnMarshalArrayShort In vfp2c32.fll
    Declare UnMarshalArrayUInt In vfp2c32.fll
    Declare UnMarshalArrayUShort In vfp2c32.fll
    Declare UnMarshalArrayWCharArray In vfp2c32.fll
    Declare UnMarshalArrayWString In vfp2c32.fll
    Declare UnMarshalCursorCharArray In vfp2c32.fll
    Declare UnMarshalCursorCString In vfp2c32.fll
    Declare UnMarshalCursorDouble In vfp2c32.fll
    Declare UnMarshalCursorFloat In vfp2c32.fll
    Declare UnMarshalCursorInt In vfp2c32.fll
    Declare UnMarshalCursorLogical In vfp2c32.fll
    Declare UnMarshalCursorShort In vfp2c32.fll
    Declare UnMarshalCursorUInt In vfp2c32.fll
    Declare UnMarshalCursorUShort In vfp2c32.fll
    Declare UnMarshalCursorWCharArray In vfp2c32.fll
    Declare UnMarshalCursorWString In vfp2c32.fll
    Declare UrlDownloadToFileEx In vfp2c32.fll
    Declare UShort2Str In vfp2c32.fll
    Declare UTC2DT In vfp2c32.fll
    Declare ValidateMem In vfp2c32.fll
    Declare Value2Variant In vfp2c32.fll
    Declare Variant2Value In vfp2c32.fll
    Declare VFP2CSys In vfp2c32.fll
    Declare WriteBytes In vfp2c32.fll
    Declare WriteCharArray In vfp2c32.fll
    Declare WriteCString In vfp2c32.fll
    Declare WriteInt64 In vfp2c32.fll
    Declare WriteLogical In vfp2c32.fll
    Declare WritePCString In vfp2c32.fll
    Declare WritePInt64 In vfp2c32.fll
    Declare WritePLogical In vfp2c32.fll
    Declare WritePUInt64 In vfp2c32.fll
    Declare WritePWChar In vfp2c32.fll
    Declare WritePWString In vfp2c32.fll
    Declare WriteRegistryKey In vfp2c32.fll
    Declare WriteUInt64 In vfp2c32.fll
    Declare WriteWChar In vfp2c32.fll
    Declare WriteWCharArray In vfp2c32.fll
    Declare WriteWString In vfp2c32.fll
Endfunc
