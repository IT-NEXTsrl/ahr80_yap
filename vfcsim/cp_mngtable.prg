* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_mngtable                                                     *
*              Configurazione Tabella                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-10-11                                                      *
* Last revis.: 2013-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tcp_mngtable",oParentObject))

* --- Class definition
define class tcp_mngtable as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 471
  Height = 294
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-05"
  HelpContextID=85613947
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cptsrvr_IDX = 0
  cPrg = "cp_mngtable"
  cComment = "Configurazione Tabella"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TABLE = space(30)
  w_PATH = space(40)
  w_YEAR = space(10)
  w_USER = space(10)
  w_COMPANY = space(10)
  w_SERVER = space(20)
  w_LASTREL = space(15)
  w_NOTE = space(0)
  w_FILENAME = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_mngtablePag1","cp_mngtable",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATH_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='cptsrvr'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        cp_mngdatabase_b(this,"Tab Save")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TABLE=space(30)
      .w_PATH=space(40)
      .w_YEAR=space(10)
      .w_USER=space(10)
      .w_COMPANY=space(10)
      .w_SERVER=space(20)
      .w_LASTREL=space(15)
      .w_NOTE=space(0)
      .w_FILENAME=space(10)
      .w_FILENAME=oParentObject.w_FILENAME
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_TABLE_NAME)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate(cp_translate(MSG_PATH)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_YEAR))
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate(cp_translate(MSG_USER_BUTTON))
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate(cp_translate(MSG_COMPANY_BUTTON))
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate(cp_translate(MSG_SERVER)+MSG_FS)
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate(cp_translate(MSG_LAST_MODIFIED)+MSG_FS)
    endwith
    this.DoRTCalc(1,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_FILENAME=.w_FILENAME
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_TABLE_NAME)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(cp_translate(MSG_PATH)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_YEAR))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(cp_translate(MSG_USER_BUTTON))
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(cp_translate(MSG_COMPANY_BUTTON))
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(cp_translate(MSG_SERVER)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(cp_translate(MSG_LAST_MODIFIED)+MSG_FS)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(cp_translate(MSG_TABLE_NAME)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(cp_translate(MSG_PATH)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(cp_translate(MSG_YEAR))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(cp_translate(MSG_USER_BUTTON))
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(cp_translate(MSG_COMPANY_BUTTON))
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(cp_translate(MSG_SERVER)+MSG_FS)
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(cp_translate(MSG_LAST_MODIFIED)+MSG_FS)
    endwith
  return

  proc Calculate_YMUKKZMUYY()
    with this
          * --- Init
          cp_mngdatabase_b(this;
              ,'Tab Init';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_YMUKKZMUYY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTABLE_1_2.value==this.w_TABLE)
      this.oPgFrm.Page1.oPag.oTABLE_1_2.value=this.w_TABLE
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_4.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_4.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oYEAR_1_5.RadioValue()==this.w_YEAR)
      this.oPgFrm.Page1.oPag.oYEAR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUSER_1_6.RadioValue()==this.w_USER)
      this.oPgFrm.Page1.oPag.oUSER_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPANY_1_7.RadioValue()==this.w_COMPANY)
      this.oPgFrm.Page1.oPag.oCOMPANY_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSERVER_1_8.RadioValue()==this.w_SERVER)
      this.oPgFrm.Page1.oPag.oSERVER_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLASTREL_1_9.value==this.w_LASTREL)
      this.oPgFrm.Page1.oPag.oLASTREL_1_9.value=this.w_LASTREL
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_12.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_12.value=this.w_NOTE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_mngtablePag1 as StdContainer
  Width  = 467
  height = 294
  stdWidth  = 467
  stdheight = 294
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTABLE_1_2 as StdField with uid="LJZPOGNEPP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TABLE", cQueryName = "TABLE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 246443318,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=13, Top=27, InputMask=replicate('X',30)

  add object oPATH_1_4 as StdField with uid="OHBZCOOCTU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55284357,;
   bGlobalFont=.t.,;
    Height=21, Width=292, Left=168, Top=27, InputMask=replicate('X',40)

  add object oYEAR_1_5 as StdCheck with uid="XMLLVXGHND",rtseq=3,rtrep=.f.,left=27, top=64, caption="          ", enabled=.f.,;
    HelpContextID = 258698619,;
    cFormVar="w_YEAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oYEAR_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oYEAR_1_5.GetRadio()
    this.Parent.oContained.w_YEAR = this.RadioValue()
    return .t.
  endfunc

  func oYEAR_1_5.SetRadio()
    this.Parent.oContained.w_YEAR=trim(this.Parent.oContained.w_YEAR)
    this.value = ;
      iif(this.Parent.oContained.w_YEAR=='S',1,;
      0)
  endfunc

  add object oUSER_1_6 as StdCheck with uid="LYBWCRVXFI",rtseq=4,rtrep=.f.,left=27, top=87, caption="             ", enabled=.f.,;
    HelpContextID = 177171835,;
    cFormVar="w_USER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUSER_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oUSER_1_6.GetRadio()
    this.Parent.oContained.w_USER = this.RadioValue()
    return .t.
  endfunc

  func oUSER_1_6.SetRadio()
    this.Parent.oContained.w_USER=trim(this.Parent.oContained.w_USER)
    this.value = ;
      iif(this.Parent.oContained.w_USER=='S',1,;
      0)
  endfunc

  add object oCOMPANY_1_7 as StdCheck with uid="IHKNBIMDPD",rtseq=5,rtrep=.f.,left=27, top=110, caption="               ", enabled=.f.,;
    HelpContextID = 48303962,;
    cFormVar="w_COMPANY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOMPANY_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oCOMPANY_1_7.GetRadio()
    this.Parent.oContained.w_COMPANY = this.RadioValue()
    return .t.
  endfunc

  func oCOMPANY_1_7.SetRadio()
    this.Parent.oContained.w_COMPANY=trim(this.Parent.oContained.w_COMPANY)
    this.value = ;
      iif(this.Parent.oContained.w_COMPANY=='S',1,;
      0)
  endfunc


  add object oSERVER_1_8 as StdTableCombo with uid="KOQWTLCVTA",rtseq=6,rtrep=.f.,left=176,top=91,width=153,height=22;
    , HelpContextID = 26122218;
    , cFormVar="w_SERVER",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='cptsrvr',cKey='servername',cValue='servername',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  add object oLASTREL_1_9 as StdField with uid="HTYBXFFSBT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LASTREL", cQueryName = "LASTREL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 38265639,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=337, Top=91, InputMask=replicate('X',15)

  add object oNOTE_1_12 as StdMemo with uid="TWVSXJBSNN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 69833349,;
   bGlobalFont=.t.,;
    Height=109, Width=447, Left=13, Top=144


  add object oBtn_1_13 as StdButton with uid="NDPFCUNHLU",left=306, top=261, width=71,height=25,;
    caption="Ok", nPag=1;
    , HelpContextID = 236285307;
    , Caption=MSG_OK_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="GNPWSQXSRS",left=385, top=261, width=71,height=25,;
    caption="Annulla", nPag=1;
    , HelpContextID = 850609;
    , Caption=MSG_CANCEL_BUTTON;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_20 as cp_setCtrlObjprop with uid="STMQUKDDXO",left=485, top=-12, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nome tabella",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 190487080


  add object oObj_1_21 as cp_setCtrlObjprop with uid="JKZKBJCYLA",left=485, top=14, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Path",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 190487080


  add object oObj_1_22 as cp_setCtrlObjprop with uid="BYKHPJGJMO",left=485, top=40, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Anno",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 190487080


  add object oObj_1_23 as cp_setCtrlObjprop with uid="FOKKKNGPII",left=485, top=66, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Utente",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 190487080


  add object oObj_1_24 as cp_setCtrlObjprop with uid="IHLOSIUUNA",left=485, top=92, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Azienda",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 190487080


  add object oObj_1_25 as cp_setCtrlObjprop with uid="AZJFQITTYG",left=485, top=118, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Server",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 190487080


  add object oObj_1_26 as cp_setCtrlObjprop with uid="RDOAZDAWVI",left=485, top=144, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Aggiornato",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 190487080

  add object oStr_1_1 as StdString with uid="CFTRCNVTMA",Visible=.t., Left=13, Top=9,;
    Alignment=0, Width=87, Height=18,;
    Caption="Nome tabella"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="FYYSSXNHUK",Visible=.t., Left=168, Top=9,;
    Alignment=0, Width=45, Height=18,;
    Caption="Path"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="ZJKWRZCHLT",Visible=.t., Left=176, Top=72,;
    Alignment=0, Width=40, Height=18,;
    Caption="Server"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="RDRVLDQAQB",Visible=.t., Left=337, Top=72,;
    Alignment=0, Width=65, Height=18,;
    Caption="Aggiornato"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="XLWYBMEGON",Visible=.t., Left=45, Top=65,;
    Alignment=0, Width=41, Height=18,;
    Caption="Anno"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="OLXUPDZBOL",Visible=.t., Left=45, Top=88,;
    Alignment=0, Width=49, Height=18,;
    Caption="Utente"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="SUIQRYZZOE",Visible=.t., Left=45, Top=111,;
    Alignment=0, Width=62, Height=18,;
    Caption="Azienda"  ;
  , bGlobalFont=.t.

  add object oBox_1_18 as StdBox with uid="JTCXDDTONS",left=13, top=58, width=153,height=83

  add object oBox_1_19 as StdBox with uid="VHUZBVIQXH",left=168, top=58, width=292,height=83
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_mngtable','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_mngtable
* ---------------------------------------
#Define SQL_MAX_DSN_LENGTH               50
#Define SQL_FETCH_NEXT                    1
#Define SQL_SUCCESS                       0
#Define SQL_SUCCESS_WITH_INFO             1

Define Class stdcmbDataSource as StdTableCombo

	Proc Init()
		This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
		This.ToolTipText=cp_Translate(This.ToolTipText)  

		This._declareSQLApi()
		*--- Inizialmente popolo con 'SQL Server'	
		This.Populate('SQL Server')

		If This.bSetFont
			This.SetFont()
		Endif		
	EndProc

	Proc Populate(parType)
		Local i_cDSN, i_nDSNSize, i_cDescription, i_nDescriptionSize, i_nCont, i_nHenv, i, xDataSource
		Local bAll
		bAll = Empty(parType)
		This.Clear()
		If (parType=='SQLServer')
			parType = 'SQL Server'
		EndIf
		*** Initialitation variables
		i_cDSN = REPLICATE( chr(0), SQL_MAX_DSN_LENGTH )
		i_nDSNSize = 0
		i_cDescription = REPLICATE( chr(0), SQL_MAX_DSN_LENGTH )
		i_nDescriptionSize = 0
		*** ODBC Environment Handle
		i_nHenv = VAL( SYS(3053) )
		This.nValues = 0
		*** ODBC API for get Datasources
		Do While INLIST( SQLDataSources( i_nHenv, ;
									 SQL_FETCH_NEXT, ;
									 @i_cDSN, ;
									 LEN( i_cDSN ), ;
									 @i_nDSNSize, ;
									 @i_cDescription, ;
									 LEN( i_cDescription ), ;
									 @i_nDescriptionSize ), ;
					 SQL_SUCCESS, SQL_SUCCESS_WITH_INFO )
			xDataSource = SUBSTR( i_cDSN, 1, i_nDSNSize )
			If (bAll or (parType $ SUBSTR(i_cDescription, 1, i_nDescriptionSize)))
				This.AddItem(xDataSource)
				This.nValues = This.nValues + 1			
				Dimension This.combovalues[This.nValues]
				This.combovalues[This.nValues] = xDataSource		
			EndIf		
		EndDo
		* -- Se non ho inserito nessun elemento per via del filtro metto un elemento dummy
		If This.ListCount=0
		   This.AddItem(" ")
		   This.nValues = 1
		   Dimension This.combovalues[1]
		   This.comboValues[1]=" "
		EndIf
		This.DisplayValue = this.comboValues[1]
    ThisForm.w_SERVER = ""
	EndProc

	*--- Declare funzioni odbc32.dll
	Proc _declareSQLApi()
		DECLARE SHORT SQLDataSources IN odbc32.dll ;
			INTEGER  nHenv, ;
			INTEGER  nDirection, ;
			STRING  @cDSN, ;
			INTEGER  nDSNMax, ;
			INTEGER @nDSNSize, ;
			STRING  @cDescription, ;
			INTEGER  nDescriptionMax, ;
			INTEGER @nDescription
	EndProc
EndDefine

* --- Fine Area Manuale
