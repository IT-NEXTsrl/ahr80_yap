* ----------------------------------------------------------------------------
* Modulo   : Visual Tools
* Programma: VO_BUILD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Emiliano Orrico
* Data creazione: 27/01/2004
* Aggiornato il : 27/01/2004
* #%&%#Build:  58
* ----------------------------------------------------------------------------
* Costruttore foglio di calcolo OpenOffice/StarOffice
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Lparam i_cCursor,i_cFileName,i_bCreateModel,i_bDisplayModel,i_oQry
Return(Createobject('SpreadSheet',i_cCursor,i_cFileName,i_bCreateModel,i_bDisplayModel,i_oQry))

Define Class SpreadSheet As Custom

    i_cCursor=''
    i_cModel=''
    i_bCreateModel=.F.
    i_bDisplayModel=.F.
    oSheet=.Null.

    Proc Init(i_cCursor,i_cModel,i_bCreateModel,i_bDisplayModel,i_oQry)
        Local i,k,i_nRow,i_cModelName,i_cOpen,i_nCursRow,i_nStep,i_nStepLim, i_ntimeexec, i_npercent, i_npercentmsg, i_Ext
        Local loSManager, loSDesktop, loReflection, loPropertyValue, loStarCalc, loStarWrk
        Local Array laNoArgs[1]
		i_ntimeexec=seconds()
		i_npercent=0
		i_npercentmsg=1
        This.i_cCursor=i_cCursor
        This.i_cModel=i_cModel
        This.i_bCreateModel=i_bCreateModel
        This.i_bDisplayModel=i_bDisplayModel

        loSManager = Createobject( "com.sun.star.ServiceManager" )
        loSDesktop = loSManager.createInstance( "com.sun.star.frame.Desktop" )
        Comarray( loSDesktop, 10 )
        loReflection = loSManager.createInstance( "com.sun.star.reflection.CoreReflection" )
        Comarray( loReflection, 10 )
        This.oSheet=Createobject('cp_SpreadSheet')
        Dimension i_aFld[1,4]
        i_nRow=0
        i_Ext=''
        If Type('this.i_cModel')='C' and not empty(this.i_cModel)
            If Inlist(Upper(Right(This.i_cModel,4)),'.OTS','.STC','.XLT')
                This.i_cModel=Left(This.i_cModel,Len(This.i_cModel)-4)
            Endif
            i_cModelName=This.i_cModel
			Do Case
				Case cp_IsStdFile(i_cModelName,'ots')
					i_Ext='ots'
				Case cp_IsStdFile(i_cModelName,'stc')
                    i_Ext='stc'
				Case cp_IsStdFile(i_cModelName,'xlt')
					i_Ext='xlt'
				Otherwise
					i_Ext='stc'
			Endcase
			i_cModelName=cp_GetStdFile(i_cModelName,i_Ext)
			* Nel caso sia attivo il MonitorFramework e il file non viene trovato, il valore restituito dalla cp_GetStdFile sarebbe vuoto
			* In questo caso si verifica che il nome del modello sia sempre valorizzato
			i_cModelName=IIF(EMPTY(i_cModelName),This.i_cModel,i_cModelName) 		 
            If This.i_bCreateModel AND NOT empty(i_cModelName) AND NOT cp_IsStdFile(i_cModelName,i_Ext)
                i_cModelName=forceext(This.i_cModel,i_Ext)
                loStarCalc=This.oSheet.CreateModel(i_cModelName,This.i_cCursor,i_oQry)
            Else
                i_cModelName=forceext(i_cModelName,i_Ext)
            Endif
			loStarCalc=This.oSheet.StartOffice(i_cModelName,.T.)			
            i_cOpen='BODY'
			loStarWrk=This.oSheet.StartOffice(i_cModelName,.F.)			
			sheets = loStarWrk.sheets.CreateEnumeration
			Sheet = sheets.nextElement
            This.oSheet.i_nBody=Sheet.getCellRangeByName("BODY").RangeAddress().StartRow
            This.oSheet.i_nEndBody=Sheet.getCellRangeByName("ENDBODY").RangeAddress().StartRow
        Else
			loStarWrk=This.oSheet.StartOffice("",.F.)		
            i_cOpen='NOBODY'
			sheets = loStarWrk.sheets.CreateEnumeration
			Sheet = sheets.nextElement
            This.oSheet.i_nBody=0
            This.oSheet.i_nEndBody=1
        Endif
        Dimension i_aFld[fcount(),4]
        For i=1 To Fcount()
            i_aFld[i,1]=Field(i)
            If i_cOpen=='BODY'
                This.oSheet.GetFieldPos(Upper(Field(i)),@i_aFld,i,loStarCalc)
			Else
			    i_aFld[i,2]=This.oSheet.i_nEndBody
				if vartype(i_oQry)='O' and (lower(i_oQry.class)='zoombox' or lower(i_oQry.class)='cp_szoombox' Or lower(i_oQry.class)='cp_zoombox') and (vartype(i_oQry.ocpquery)='O' AND i_oQry.ocpquery.nfields>0)
					k=i_oQry.GetCol(i_aFld[i,1])
					if k>0
						i_aFld[i,3]=i_oQry.grd.columns(k).columnorder - IIF( lower(i_oQry.class)='cp_szoombox', 2, 1) &&Elimino la colonna XCHK
						i_aFld[i,4]=1
					else
						i_aFld[i,3]=-1
						i_aFld[i,4]=0
					endif
				else
					i_aFld[i,3]=i-1
				i_aFld[i,4]=1
            Endif
				
            Endif
        Next
		If i_cOpen='NOBODY'
			Local i
			For i=1 To Alen(i_aFld,1)
				If Type('i_aFld[i,1]')='C' and i_aFld[i,3]>=0
					cell = Sheet.getCellByPosition(i_aFld[i,3], 0)
					cell.CharWeight = 150
					cell.cellbackcolor=RGB(MOD(int(i_ThemesManager.GetProp(7)/256/256),256),MOD(int(i_ThemesManager.GetProp(7)/256),256),MOD(i_ThemesManager.GetProp(7),256))
					if vartype(i_oQry)='O' and (lower(i_oQry.class)='zoombox' or lower(i_oQry.class)='cp_szoombox' Or lower(i_oQry.class)='cp_zoombox') and (vartype(i_oQry.ocpquery)='O' AND i_oQry.ocpquery.nfields>0)
						k=i_oQry.GetCol(i_aFld[i,1])
						if k>0
							cell.setString(i_oQry.cColTitle(k))
						endif
					else
						cell.setString(i_aFld[i,1])
					endif					
				endif
			Next
		Endif
        Local _nFile,_hFile,_ttot
        _nFile=''
        _ttot=0
        * Disabilita il refresh su openoffice ------------
        loStarWrk.addActionLock
        loStarWrk.LockControllers
        *
		If Vartype(g_LOG_OPOF)='L' And g_LOG_OPOF
			_nFile='__Log_OOffice_'+Sys(2015)+'.txt'
			_ttot=Seconds()
			*
			If File(_nFile)
				_hFile=Fopen(_nFile,12)
				=Fseek(_hFile,0,2)
			Else
				_hFile=Fcreate(_nFile)
			Endif
			*
			=Fputs(_hFile,'=============================================')
			=Fputs(_hFile,' OpenOffice Calc - '+Dtoc(Date())+' '+Time())
			=Fputs(_hFile,'=============================================')
			=Fputs(_hFile,' ')
			*
			=Fclose(_hFile)
		Endif
		i_nRow=This.oSheet.i_nEndBody
		i=0
		SELECT (i_cCursor)
		Go Top
		i_nCursRow=Reccount(i_cCursor)
		Scan
			If i=0 and i_cOpen=='BODY'
				This.oSheet.AddDataFixed(This.i_cCursor,@i_aFld,loStarWrk)
				* crea lo spazio per tutte le righe del cursore, anzich� farlo uno alla volta in adddatabody()
				sheets = loStarWrk.sheets.CreateEnumeration
				Sheet = sheets.nextElement
				Sheet.Rows.insertByIndex(This.oSheet.i_nEndBody,i_nCursRow)
				**
				i_nStep=100
				If i_nCursRow>i_nStep
					* i primi cento (i_nSTep) li fa uno alla volta
					oRangeOrg = Sheet.getCellRangeByName('A'+Alltrim(Str(This.oSheet.i_nBody+2))+':IV'+Alltrim(Str(This.oSheet.i_nBody+2))).RangeAddress
					For k=1 To i_nStep
						oCellCpy = Sheet.getCellByPosition(0,This.oSheet.i_nBody+1+k).CellAddress()
						Sheet.CopyRange(oCellCpy, oRangeOrg)
					Next
					* gli altri (da i_nSTep+1 in poi) li fa a gruppi di cento (i_nSTep)
					oRangeOrg = Sheet.getCellRangeByName('A'+Alltrim(Str(This.oSheet.i_nBody+2))+':IV'+Alltrim(Str(This.oSheet.i_nBody+2+i_nStep))).RangeAddress
					i_nStepLim=i_nCursRow-Mod(i_nCursRow,i_nStep)
					For k=i_nStep+1 To i_nStepLim Step i_nStep
						oCellCpy = Sheet.getCellByPosition(0,This.oSheet.i_nBody+1+k).CellAddress()
						Sheet.CopyRange(oCellCpy, oRangeOrg)
						cp_Msg(ah_msgFormat("Esecuzione esportazione %1/%2", Str(k,6,0), Alltrim(Str(i_nCursRow,10,0))),.t.,.t.,.f.)
					Next
					* ora completa le celle dopo i multipli di 100 (i_nSTep)
					oRangeOrg = Sheet.getCellRangeByName('A'+Alltrim(Str(This.oSheet.i_nBody+2))+':IV'+Alltrim(Str(This.oSheet.i_nBody+2))).RangeAddress
					For k=i_nStepLim+1 To i_nCursRow
						oCellCpy = Sheet.getCellByPosition(0,This.oSheet.i_nBody+1+k).CellAddress()
						Sheet.CopyRange(oCellCpy, oRangeOrg)
					Next
				Else
					oRangeOrg = Sheet.getCellRangeByName('A'+Alltrim(Str(This.oSheet.i_nBody+2))+':IV'+Alltrim(Str(This.oSheet.i_nBody+2))).RangeAddress
					For k=1 To i_nCursRow
						oCellCpy = Sheet.getCellByPosition(0,This.oSheet.i_nBody+1+k).CellAddress()
						Sheet.CopyRange(oCellCpy, oRangeOrg)
						If Mod(k,50)=0
							cp_Msg(ah_msgFormat("Esecuzione esportazione %1/%2", Str(k,6,0), Alltrim(Str(i_nCursRow,10,0))),.t.,.t.,.f.)
						Endif
					Next
				Endif
				*
				i=1
			Endif
			* messaggio log
			i_npercent=round(i_nRow*100/i_nCursRow,2)
			i_npercent=iif(i_npercent>100,100,i_npercent)
			If i_npercentmsg<round(i_npercent/10,0)
				i_npercentmsg=i_npercentmsg+1
				cp_Msg(ah_msgformat("Esecuzione esportazione al %1 %",alltrim(str(i_npercent,3,2))),.t.,.t.,,.f.)
			Endif
			* aggiunta riga
			This.oSheet.AddDataBody(This.i_cCursor,@i_aFld,i_nRow,loStarWrk,_nFile)
			i_nRow=i_nRow+1+This.oSheet.i_nDsp
		Endscan
		If i_cOpen=='BODY'
			This.oSheet.DeleteModelRow(loStarWrk,i_nRow)
		Endif
        * Riabilita il refresh su openoffice ------------
        loStarWrk.UnlockControllers
        loStarWrk.removeActionLock
        If Vartype(g_LOG_OPOF)='L' And g_LOG_OPOF
            _ttot=Seconds()-_ttot
            If File(_nFile)
                _hFile=Fopen(_nFile,12)
                =Fseek(_hFile,0,2)
            Else
                _hFile=Fcreate(_nFile)
            Endif
            *
            =Fputs(_hFile,'=============================================')
            =Fputs(_hFile,' Totale= '+Alltrim(Str(_ttot,10,3))+' sec - '+Alltrim(Str(_ttot/60,10,1))+' min')
            =Fputs(_hFile,'=============================================')
            *
            =Fclose(_hFile)
        Endif
		ah_errormsg("Esportazione completata: tempo di esecuzione %1 secondi",,,alltrim(str(seconds()-i_ntimeexec)))
		WAIT CLEAR
        Return
Enddefine

Define Class cp_SpreadSheet As Custom
    o=.Null.
    i_nBody=0
    i_nEndBody=0
    i_nDsp=0
    i_cSel=''
    i_cWrkModel=''
	i_loStarCalc=.null.
    Proc Init(i_oExcl)
        This.o=i_oExcl
    Proc Save(i_cFile)
        If Type('i_cFile')='C'
            sheetm.storeAsUrl(i_cFile, @laNoArgs)
        Else
            sheetm.Store
        Endif
    Return
    Func GetFieldPos(i_cField,i_aFld,i_nIdx,oStarCalc)
        Local i_errsav, cell
        Private i_err
        i_errsav=On('ERROR')
        i_err=.F.
        On Error i_err=.T.
        sheets = oStarCalc.sheets.CreateEnumeration
        Sheet = sheets.nextElement
        cell = Sheet.getCellRangeByName(i_cField)
        oStarCalc.CurrentController.Select(cell)
        If i_err
            i_err=.F.
            cell = Sheet.getCellRangeByName(Lower(i_cField))
            oStarCalc.CurrentController.Select(cell)
        Endif
        On Error &i_errsav
        If !i_err
            i_aFld[i_nIdx,2]=cell.getCellAddress().Row
            i_aFld[i_nIdx,3]=cell.getCellAddress().Column
            If cell.getCellAddress().Row>This.i_nBody And cell.getCellAddress().Row<This.i_nEndBody
                i_aFld[i_nIdx,4]=1
            Else
                i_aFld[i_nIdx,4]=0
            Endif
        Else
            Return(.F.)
        Endif
        i_err=.F.
        Return
    Proc AddDataBody(i_cCursor,i_aFld,i_nFirstRow,oStarWrk,_nFile)
        Local i,i_cSel,i_cValue
        i_cValue=""
        Local _tt1,_tt2,_ttt
        _ttt=Seconds()
        sheets = oStarWrk.sheets.CreateEnumeration
        Sheet = sheets.nextElement
        _tt1=Seconds()-_ttt
        _ttt=Seconds()
        For i=1 To Alen(i_aFld,1)
            If Type('i_aFld[i,1]')='C'
                If Type('i_aFld[i,4]')='N' And i_aFld[i,4]=1
                    i_cValue=&i_cCursor..&i_aFld[i,1]
                    If Type('i_cValue')<>'D' Or !Empty(Nvl(i_cValue,""))
                        Do Case
                            Case Type('i_cValue')='C'
                                ** Non scrive se non � necessario
                                If Not(Empty(Nvl(i_cValue,"")))
                                    Sheet.getCellByPosition(i_aFld[i,3],i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)).String="'"+Alltrim(Nvl(i_cValue,""))
                                Endif
                            Case Type('i_cValue')$'TD'
                                ** Non scrive se non � necessario
                                If Not(Empty(Nvl(i_cValue,Ctot(""))))
                                    Sheet.getCellByPosition(i_aFld[i,3],i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)).Value=Nvl(i_cValue,0)
									Sheet.getCellByPosition(i_aFld[i,3],i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)).NumberFormat=36
                                Endif
                            Otherwise
                                ** gli altri valori, ad esempio numerici, li scrive sempre
                                Sheet.getCellByPosition(i_aFld[i,3],i_nFirstRow+(i_aFld[i,2]-This.i_nBody-1)).Value=Nvl(i_cValue,0)
                        Endcase
                    Endif
                Endif
            Endif
        Next
        _tt2=Seconds()-_ttt
        If Vartype(g_LOG_OPOF)='L' And g_LOG_OPOF And Mod(i_nFirstRow,100)=0
            If File(_nFile)
                _hFile=Fopen(_nFile,12)
                =Fseek(_hFile,0,2)
            Else
                _hFile=Fcreate(_nFile)
            Endif
            *
            =Fputs(_hFile,'CopyRange= '+Str(_tt1,7,3))
            =Fputs(_hFile,'WriteRowCells= '+Str(_tt2,7,3))
            =Fputs(_hFile,'------'+Alltrim(Str(i_nFirstRow,6,0))+'------')
            *
            =Fclose(_hFile)
        Endif
        Return
    Proc DeleteModelRow(loStarWrk,i_nRow)
        sheets = loStarWrk.sheets.CreateEnumeration
        Sheet = sheets.nextElement
        cell = Sheet.getCellByPosition(0,1)
        loStarWrk.NamedRanges.RemoveByName("BODY")
        Sheet.Rows.removeByIndex(i_nRow,1)
        Sheet.Rows.removeByIndex(This.i_nBody,2)
        loStarWrk.NamedRanges.RemoveByName("ENDBODY")
        Return
    Proc AddDataFixed(i_cCursor,i_aFld,loStarWrk)
        Local i,i_cValue
        sheets = loStarWrk.sheets.CreateEnumeration
        Sheet = sheets.nextElement

        For i=1 To Alen(i_aFld,1)
            If Type('i_aFld[i,1]')='C'
                If Type('i_aFld[i,4]')='N' And i_aFld[i,4]=0
                    i_cValue=&i_cCursor..&i_aFld[i,1]
                    If Type('i_cValue')<>'D' Or !Empty(i_cValue)
                        Do Case
                            Case Type('i_cValue')='C'
								Sheet.getCellByPosition(i_aFld[i,3],i_aFld[i,2]).String="'"+Alltrim(i_cValue)
                            Case Type('i_cValue')$'TD'
								Sheet.getCellByPosition(i_aFld[i,3],i_aFld[i,2]).Value=Nvl(i_cValue,0)
								Sheet.getCellByPosition(i_aFld[i,3],i_aFld[i,2]).NumberFormat=36							
							Otherwise
								Sheet.getCellByPosition(i_aFld[i,3],i_aFld[i,2]).Value=i_cValue
                        Endcase
                    Endif
                Endif
            Endif
        Next
        Return
    Function CreateModel(i_cModel,i_cCursor,i_oQry)
        Local loSManager, loSDesktop, loReflection, loPropertyValue, loStarCalc, i_nNumberRow
        Local Array laNoArgs[2]

        loSManager = Createobject("Com.Sun.Star.ServiceManager")
        loSDesktop = loSManager.createInstance("com.sun.star.frame.Desktop")
        loSFrame = loSManager.createInstance("com.sun.star.frame")
        Comarray(loSDesktop, 10)
        loReflection = loSManager.createInstance("com.sun.star.reflection.CoreReflection" )
        Comarray( loReflection, 10 )

        loPropertyValue = This.createStruct( @loReflection, "com.sun.star.beans.PropertyValue" )
        laNoArgs[1] = loPropertyValue
        laNoArgs[1].Name = "ReadOnly"
        laNoArgs[1].Value = .F.
        laNoArgs[2] = loPropertyValue
        laNoArgs[2].Name = "MacroExecutionMode"
        laNoArgs[2].Value = 4

        loStarCalc = loSDesktop.loadComponentFromUrl( 'private:factory/scalc', "_blank", 0, @laNoArgs )
        Comarray( loStarCalc, 10)

        sheetsm = loStarCalc.sheets.CreateEnumeration
        sheetm = sheetsm.nextElement

        oRangeCollection = loStarCalc.NamedRanges
        nas = 64
        cLetter=""

		*** Se provengo da query aggiungo il nome del modello con la descrizione della prima tabella
		if vartype(i_oQry)='O'
		    i_nNumberRow=1
			cell = sheetm.getCellByPosition(0, 0)
			cell.setString(i_oQry.oPage.Page1.opag.TblTables.List(1,1))
			sheetm.getCellByPosition(0, 0).CharWeight = 150
		else
			i_nNumberRow=0
		endif

        Dimension i_aFld[fcount(),4]
        For i=1 To Fcount()
            i_aFld[i, 1] = Field(i)
            cell = sheetm.getCellByPosition(i-1, i_nNumberRow)
            if vartype(i_oQry)='O' and i_oQry.oPage.page2.opag.fdlfields.ListCount>0
               cell.setString(i_oQry.oPage.page2.opag.fdlfields.List(i,1))
            else
               cell.setString(i_aFld[i,1])
            endif
            sheetm.getCellByPosition(i-1, i_nNumberRow).CharWeight = 150
            sheetm.getCellByPosition(i-1, i_nNumberRow).cellbackcolor=RGB(MOD(int(i_ThemesManager.GetProp(7)/256/256),256),MOD(int(i_ThemesManager.GetProp(7)/256),256),MOD(i_ThemesManager.GetProp(7),256))
            sheetm.Columns(i-1).OptimalWidth = True 
            If i < 27
                cLetter = Chr(nas + i)
            Else
                cLetter = Chr(Int(i/26)+nas)+Chr(Mod(i,26)+nas)
            Endif
            cellStr = sheetm.Name+"."+cLetter+alltrim(str(i_nNumberRow+3))+":"+cLetter+alltrim(str(i_nNumberRow+3))
            oRangeCollection.addNewByName( i_aFld[i, 1], cellStr, cell.getCellAddress() , 0 )
            cFld=Field(i)
            If Inlist(Type('&i_cCursor..&cFld'),'N','Y')
                sheetm.getCellByPosition(i-1,i_nNumberRow+4).formula="=sum("+cLetter+alltrim(str(i_nNumberRow+3))+":"+cLetter+alltrim(str(i_nNumberRow+4))+")"
            Endif
        Endfor

        cell=sheetm.getCellByPosition(0,i_nNumberRow+1)
        cell.setString("BODY")
        oRangeCollection.addNewByName( "BODY", sheetm.Name+".A"+alltrim(str(i_nNumberRow+2))+":A"+alltrim(str(i_nNumberRow+2)), cell.getCellAddress() , 0 )
        cell=sheetm.getCellByPosition(0,i_nNumberRow+3)
        cell.setString("ENDBODY")
        oRangeCollection.addNewByName( "ENDBODY", sheetm.Name+".A"+alltrim(str(i_nNumberRow+4))+":A"+alltrim(str(i_nNumberRow+4)), cell.getCellAddress() , 0 )
        cFld=Field(1)
        If !Inlist(Type('&i_cCursor..&cFld'),'N','Y')
            cell=sheetm.getCellByPosition(0,i_nNumberRow+4)
            cell.setString("Totale")
        Endif
        laNoArgs[1].Name = "Overwrite"
        laNoArgs[1].Value = .T.

        o_FileUrl = "file:///"+Strtran(i_cModel,Chr(92),Chr(47))
        loStarCalc.storeAsUrl( o_FileUrl, @laNoArgs )
        loStarCalc.dispose()
        Return ( loStarCalc )
    Function StartOffice(OpenFile, boolReadOnly)
        Local loSManager, loSDesktop, loReflection, loPropertyValue, loStarCalc
        Local Array NoArgs[2]

        If Not Empty( OpenFile )
            OpenFile = "file:///"+Strtran(OpenFile,Chr(92),Chr(47))
        Else
            OpenFile = "private:factory/scalc"
        Endif

        loSManager = Createobject("Com.Sun.Star.ServiceManager")
        loSDesktop = loSManager.createInstance("com.sun.star.frame.Desktop")
        loSFrame = loSManager.createInstance("com.sun.star.frame")
        Comarray(loSDesktop, 10)
        loReflection = loSManager.createInstance("com.sun.star.reflection.CoreReflection" )
        Comarray( loReflection, 10 )
        NoArgs[1] = This.createStruct( @loReflection,"com.sun.star.beans.PropertyValue" )
        NoArgs[1].Name = "ReadOnly"
        If boolReadOnly
            NoArgs[1].Value = .T.
        Else
            NoArgs[1].Value = .F.
        Endif
        NoArgs[2] = This.createStruct(@loReflection,"com.sun.star.beans.PropertyValue" )
        NoArgs[2].Name = "MacroExecutionMode"
        NoArgs[2].Value = 4
        loStarCalc = loSDesktop.loadComponentFromUrl( OpenFile, "_blank", 0, @NoArgs )

        Return( loStarCalc )
    Endproc
    Function createStruct( toReflection, tcTypeName )
        Local loPropertyValue, loTemp
        loPropertyValue = Createobject( "relation")
        toReflection.forName( tcTypeName ).Createobject( @loPropertyValue )
        Return( loPropertyValue )
    Endproc
Enddefine