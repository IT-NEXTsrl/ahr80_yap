* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_paramconnect                                                 *
*              "+MSG_PARCON_PROPERTY+"                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-05                                                      *
* Last revis.: 2013-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_paramconnect
cp_ReadXdc()
* --- Fine Area Manuale
return(createobject("tcp_paramconnect",oParentObject))

* --- Class definition
define class tcp_paramconnect as StdForm
  Top    = 200
  Left   = 12

  * --- Standard Properties
  Width  = 522
  Height = 346
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-01-23"
  HelpContextID=215696458
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "cp_paramconnect"
  cComment = ""+MSG_PARCON_PROPERTY+""
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_NOMCNF = space(200)
  w_AUTORICONNECT = .F.
  w_NUMRICONNECT = 0
  w_DELAYRICONNECT = 0
  w_CONNECTTIMEOUT = 0
  w_DEADREQUERY = .F.
  w_NUMDEADREQUERY = 0
  w_DELAYDEADREQUERY = 0
  w_CONNECTMSG = space(1)
  w_CONNECTLOGIN = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_paramconnectPag1","cp_paramconnect",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNOMCNF_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NOMCNF=space(200)
      .w_AUTORICONNECT=.f.
      .w_NUMRICONNECT=0
      .w_DELAYRICONNECT=0
      .w_CONNECTTIMEOUT=0
      .w_DEADREQUERY=.f.
      .w_NUMDEADREQUERY=0
      .w_DELAYDEADREQUERY=0
      .w_CONNECTMSG=space(1)
      .w_CONNECTLOGIN=space(1)
        .w_NOMCNF = IIF ('\' $i_cfileCnF OR '/' $i_cfileCnF OR ':' $i_cfileCnF , i_cfileCnF , ALLTRIM(sys(5)+sys(2003)+'\'+i_cfileCnF))
        .w_AUTORICONNECT = IIF (VARTYPE (i_AUTORICONNECT)<>'L' , .F. , i_AUTORICONNECT)
        .w_NUMRICONNECT = IIF (vartype (i_NUMRICONNECT)='N' , i_NUMRICONNECT , 0)
        .w_DELAYRICONNECT = IIF (vartype (i_DELAYRICONNECT) ='N' , i_DELAYRICONNECT ,0)
        .w_CONNECTTIMEOUT = IIF (vartype (i_CONNECTTIMEOUT) ='N' , i_CONNECTTIMEOUT , 0)
        .w_DEADREQUERY = IIF (VARTYPE (i_DEADREQUERY)<>'L' , .F. , i_DEADREQUERY )
        .w_NUMDEADREQUERY = IIF (vartype (i_NUMDEADREQUERY)='N' , i_NUMDEADREQUERY , 0)
        .w_DELAYDEADREQUERY = IIF (vartype (i_DELAYDEADREQUERY)='N' , i_DELAYDEADREQUERY , 0)
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .w_CONNECTMSG = IIF (vartype (i_CONNECTMSG)='C' , i_CONNECTMSG , 'N')
        .w_CONNECTLOGIN = IIF (vartype (i_CONNECTLOGIN) ='C' , i_CONNECTLOGIN ,'N')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNUMRICONNECT_1_3.enabled = this.oPgFrm.Page1.oPag.oNUMRICONNECT_1_3.mCond()
    this.oPgFrm.Page1.oPag.oDELAYRICONNECT_1_4.enabled = this.oPgFrm.Page1.oPag.oDELAYRICONNECT_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCONNECTTIMEOUT_1_5.enabled = this.oPgFrm.Page1.oPag.oCONNECTTIMEOUT_1_5.mCond()
    this.oPgFrm.Page1.oPag.oNUMDEADREQUERY_1_7.enabled = this.oPgFrm.Page1.oPag.oNUMDEADREQUERY_1_7.mCond()
    this.oPgFrm.Page1.oPag.oDELAYDEADREQUERY_1_8.enabled = this.oPgFrm.Page1.oPag.oDELAYDEADREQUERY_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCONNECTMSG_1_20.enabled = this.oPgFrm.Page1.oPag.oCONNECTMSG_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNOMCNF_1_1.value==this.w_NOMCNF)
      this.oPgFrm.Page1.oPag.oNOMCNF_1_1.value=this.w_NOMCNF
    endif
    if not(this.oPgFrm.Page1.oPag.oAUTORICONNECT_1_2.RadioValue()==this.w_AUTORICONNECT)
      this.oPgFrm.Page1.oPag.oAUTORICONNECT_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMRICONNECT_1_3.value==this.w_NUMRICONNECT)
      this.oPgFrm.Page1.oPag.oNUMRICONNECT_1_3.value=this.w_NUMRICONNECT
    endif
    if not(this.oPgFrm.Page1.oPag.oDELAYRICONNECT_1_4.value==this.w_DELAYRICONNECT)
      this.oPgFrm.Page1.oPag.oDELAYRICONNECT_1_4.value=this.w_DELAYRICONNECT
    endif
    if not(this.oPgFrm.Page1.oPag.oCONNECTTIMEOUT_1_5.value==this.w_CONNECTTIMEOUT)
      this.oPgFrm.Page1.oPag.oCONNECTTIMEOUT_1_5.value=this.w_CONNECTTIMEOUT
    endif
    if not(this.oPgFrm.Page1.oPag.oDEADREQUERY_1_6.RadioValue()==this.w_DEADREQUERY)
      this.oPgFrm.Page1.oPag.oDEADREQUERY_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDEADREQUERY_1_7.value==this.w_NUMDEADREQUERY)
      this.oPgFrm.Page1.oPag.oNUMDEADREQUERY_1_7.value=this.w_NUMDEADREQUERY
    endif
    if not(this.oPgFrm.Page1.oPag.oDELAYDEADREQUERY_1_8.value==this.w_DELAYDEADREQUERY)
      this.oPgFrm.Page1.oPag.oDELAYDEADREQUERY_1_8.value=this.w_DELAYDEADREQUERY
    endif
    if not(this.oPgFrm.Page1.oPag.oCONNECTMSG_1_20.RadioValue()==this.w_CONNECTMSG)
      this.oPgFrm.Page1.oPag.oCONNECTMSG_1_20.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_paramconnectPag1 as StdContainer
  Width  = 518
  height = 346
  stdWidth  = 518
  stdheight = 346
  resizeXpos=387
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNOMCNF_1_1 as StdField with uid="USOKHEWMBF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NOMCNF", cQueryName = "NOMCNF",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = ""+MSG_SHOW_CNF+"",;
    HelpContextID = 136888844,;
   bGlobalFont=.t.,;
    Height=21, Width=175, Left=314, Top=60, InputMask=replicate('X',200)

  add object oAUTORICONNECT_1_2 as StdCheck with uid="DJGFQBJCBL",rtseq=2,rtrep=.f.,left=314, top=90, caption=""+MSG_CNF_ATTIVA +"",;
    ToolTipText = ""+MSG_SHOW_AUTORICONNECT+"",;
    HelpContextID = 77299851,;
    cFormVar="w_AUTORICONNECT", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oAUTORICONNECT_1_2.RadioValue()
    return(iif(this.value =1,.T.,;
    iif(this.value =2,.T.,;
    .F.)))
  endfunc
  func oAUTORICONNECT_1_2.GetRadio()
    this.Parent.oContained.w_AUTORICONNECT = this.RadioValue()
    return .t.
  endfunc

  func oAUTORICONNECT_1_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AUTORICONNECT==.T.,1,;
      iif(this.Parent.oContained.w_AUTORICONNECT==.T.,2,;
      0))
  endfunc

  add object oNUMRICONNECT_1_3 as StdField with uid="JLOLDFOPRK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NUMRICONNECT", cQueryName = "NUMRICONNECT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = ""+MSG_SHOW_NUMRICONNECT+"",;
    HelpContextID = 156715534,;
   bGlobalFont=.t.,;
    Height=21, Width=175, Left=314, Top=116

  func oNUMRICONNECT_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTORICONNECT)
    endwith
   endif
  endfunc

  add object oDELAYRICONNECT_1_4 as StdField with uid="ZGXJEPKUWX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DELAYRICONNECT", cQueryName = "DELAYRICONNECT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = ""+MSG_SHOW_DELAYRICONNECT+"",;
    HelpContextID = 120176505,;
   bGlobalFont=.t.,;
    Height=21, Width=175, Left=314, Top=139

  func oDELAYRICONNECT_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTORICONNECT)
    endwith
   endif
  endfunc

  add object oCONNECTTIMEOUT_1_5 as StdField with uid="ULVVZIGRKQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CONNECTTIMEOUT", cQueryName = "CONNECTTIMEOUT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = ""+MSG_SHOW_CONNECTTIMEOUT+"",;
    HelpContextID = 20523266,;
   bGlobalFont=.t.,;
    Height=21, Width=175, Left=314, Top=163

  func oCONNECTTIMEOUT_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTORICONNECT)
    endwith
   endif
  endfunc

  add object oDEADREQUERY_1_6 as StdCheck with uid="YLUBTYZYIL",rtseq=6,rtrep=.f.,left=314, top=189, caption=""+MSG_CNF_ATTIVA +"",;
    ToolTipText = ""+MSG_SHOW_DEADREQUERY+"",;
    HelpContextID = 147562415,;
    cFormVar="w_DEADREQUERY", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oDEADREQUERY_1_6.RadioValue()
    return(iif(this.value =1,.T.,;
    iif(this.value =2,.T.,;
    .F.)))
  endfunc
  func oDEADREQUERY_1_6.GetRadio()
    this.Parent.oContained.w_DEADREQUERY = this.RadioValue()
    return .t.
  endfunc

  func oDEADREQUERY_1_6.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DEADREQUERY==.T.,1,;
      iif(this.Parent.oContained.w_DEADREQUERY==.T.,2,;
      0))
  endfunc

  add object oNUMDEADREQUERY_1_7 as StdField with uid="XBFEHCUWDM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMDEADREQUERY", cQueryName = "NUMDEADREQUERY",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = ""+MSG_SHOW_NUMDEADREQUERY+"",;
    HelpContextID = 23394473,;
   bGlobalFont=.t.,;
    Height=21, Width=175, Left=314, Top=214

  func oNUMDEADREQUERY_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DEADREQUERY)
    endwith
   endif
  endfunc

  add object oDELAYDEADREQUERY_1_8 as StdField with uid="YFEYTVJFMX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DELAYDEADREQUERY", cQueryName = "DELAYDEADREQUERY",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = ""+MSG_SHOW_DELAYDEADREQUERY+"",;
    HelpContextID = 210995538,;
   bGlobalFont=.t.,;
    Height=21, Width=175, Left=314, Top=239

  func oDELAYDEADREQUERY_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DEADREQUERY)
    endwith
   endif
  endfunc


  add object oObj_1_19 as cp_askfile with uid="OGPUROVHMF",left=493, top=64, width=16,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_NOMCNF",;
    nPag=1;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 215683900


  add object oCONNECTMSG_1_20 as StdCombo with uid="JNHXCDKLYF",rtseq=9,rtrep=.f.,left=314,top=267,width=175,height=22;
    , ToolTipText = ""+MSG_SHOW_MSG+"";
    , HelpContextID = 73469012;
    , cFormVar="w_CONNECTMSG",RowSource=""+""+MSG_CNF_OPT1+","+""+MSG_CNF_OPT2+","+""+MSG_CNF_OPT3+","+""+MSG_CNF_OPT4+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCONNECTMSG_1_20.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'W',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oCONNECTMSG_1_20.GetRadio()
    this.Parent.oContained.w_CONNECTMSG = this.RadioValue()
    return .t.
  endfunc

  func oCONNECTMSG_1_20.SetRadio()
    this.Parent.oContained.w_CONNECTMSG=trim(this.Parent.oContained.w_CONNECTMSG)
    this.value = ;
      iif(this.Parent.oContained.w_CONNECTMSG=='N',1,;
      iif(this.Parent.oContained.w_CONNECTMSG=='D',2,;
      iif(this.Parent.oContained.w_CONNECTMSG=='W',3,;
      iif(this.Parent.oContained.w_CONNECTMSG=='F',4,;
      0))))
  endfunc

  func oCONNECTMSG_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTORICONNECT OR .w_DEADREQUERY)
    endwith
   endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="YCTTRQWDEB",left=409, top=297, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = ""+MSG_SHOW_CNFOK+"";
    , HelpContextID = 227407923;
    , Caption=MSG_SAVE;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        Settaggio( this.parent.parent.parent.parent )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="TAPXNQTUEA",left=461, top=297, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = ""+MSG_PRESS_TO_EXIT+"";
    , HelpContextID = 21936890;
    , Caption=MSG_CANCEL_BUTTON;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_9 as StdString with uid="ZSGXKEPLFV",Visible=.t., Left=8, Top=89,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_AUTORICONNECT+":"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="OYREPMHCTV",Visible=.t., Left=8, Top=60,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_CNF_FILE+":"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="UEWIDQGVLQ",Visible=.t., Left=12, Top=10,;
    Alignment=0, Width=464, Height=18,;
    Caption=""+MSG_CNF_INFO+""  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="CSGUWKINZV",Visible=.t., Left=8, Top=163,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_CONNECTTIMEOUT+":"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="RHZTAJVQEC",Visible=.t., Left=8, Top=239,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_DELAYDEADREQUERY+":"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KFQQAZYJHZ",Visible=.t., Left=8, Top=214,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_NUMDEADREQUERY+":"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="YMKICHSDRF",Visible=.t., Left=8, Top=139,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_DELAYRICONNECT+":"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="FFBNAJVRKR",Visible=.t., Left=8, Top=188,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_DEADREQUERY+":"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="TQWDNGNLBG",Visible=.t., Left=8, Top=116,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_NUMRICONNECT+":"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="RMIADRMZLJ",Visible=.t., Left=13, Top=29,;
    Alignment=0, Width=463, Height=18,;
    Caption=""+MSG_CNF_INFO1+""  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="MEYGLYHEYZ",Visible=.t., Left=8, Top=266,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_CNF_MSG+":"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="XYPEWTFPKJ",Visible=.t., Left=537, Top=193,;
    Alignment=1, Width=301, Height=18,;
    Caption=""+MSG_CONNECTLOGIN+":"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_paramconnect','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_paramconnect
  *imposta le variabili globali e salva la configurazione nel file CNF
Procedure Settaggio (masc)
local fileapp , fileorig, filedest , letto 
Public	i_DEADREQUERY ,	i_NUMDEADREQUERY,	i_DELAYDEADREQUERY ,i_CONNECTMSG , i_AUTORICONNECT , i_CONNECTLOGIN
Public i_NUMRICONNECT ,	i_NUMRICONNECT ,	i_DELAYRICONNECT ,	i_ConnectTimeOutEndProc
i_DEADREQUERY=masc.w_DEADREQUERY
i_NUMDEADREQUERY=masc.w_NUMDEADREQUERY
i_DELAYDEADREQUERY=masc.w_DELAYDEADREQUERY
i_AUTORICONNECT=masc.w_AUTORICONNECT
i_NUMRICONNECT=masc.w_NUMRICONNECT
i_DELAYRICONNECT=masc.w_DELAYRICONNECT
i_ConnectTimeOut=masc.w_ConnectTimeOut
i_CONNECTMSG=masc.w_CONNECTMSG
i_AUTORICONNECT=masc.w_AUTORICONNECT
i_CONNECTLOGIN =masc.w_CONNECTLOGIN
fileapp= alltrim (masc.w_NOMCNF) + '_COPIA' 
if cp_fileexist(masc.w_NOMCNF)
   DELETE FILE fileapp
ELSE 
   cp_ErrorMsg('Il file cnf specificato non esiste')
return
ENDIF

COPY FILE (masc.w_NOMCNF) TO (fileapp)
*Salvataggio nel CNF selezionato
IF  !empty(fileapp)  
	 if cp_fileexist(fileapp)
     DELETE FILE masc.w_NOMCNF
     filedest = FCREATE(masc.w_NOMCNF) 
     fileorig=fopen (fileapp)
	   do while !feof(fileorig)
	     letto=fgets(fileorig)
       IF NOT ('I_DEADREQUERY'$ UPPER (letto) OR 'I_NUMDEADREQUERY' $ UPPER (letto) OR 'I_DELAYDEADREQUERY'$ UPPER (letto) OR   'I_AUTORICONNECT'$ UPPER(letto) OR  'I_NUMRICONNECT'$ UPPER(letto) OR  'I_DELAYRICONNECT'$ UPPER(letto) OR  'I_CONNECTTIMEOUT'$ UPPER(letto)  OR  'I_CONNECTMSG'$ UPPER(letto) OR 'I_CONNECTLOGIN'$ UPPER(letto)  )
         FPUTS(filedest , letto) 
       ENDIF
     enddo
     FWRITE(filedest , CHR(13)+CHR(10)+"Public i_DEADREQUERY , i_NUMDEADREQUERY ,i_DELAYDEADREQUERY , i_AUTORICONNECT ,  i_NUMRICONNECT ,i_DELAYRICONNECT  , i_ConnectTimeOut , i_CONNECTMSG ")   
     FWRITE(filedest , CHR(13)+CHR(10)+"i_DEADREQUERY=" + IIF (i_DEADREQUERY,'.T.','.F.') )   
     FWRITE(filedest , CHR(13)+CHR(10)+"i_NUMDEADREQUERY="+ alltrim (STR ( i_NUMDEADREQUERY)))   
     FWRITE(filedest , CHR(13)+CHR(10)+"i_DELAYDEADREQUERY="+ alltrim (STR (i_DELAYDEADREQUERY )) )  
     FWRITE(filedest , CHR(13)+CHR(10)+"i_AUTORICONNECT="+ IIF (i_AUTORICONNECT,'.T.','.F.'))
     FWRITE(filedest , CHR(13)+CHR(10)+"i_NUMRICONNECT="+ alltrim (STR ( i_NUMRICONNECT)) )
     FWRITE(filedest , CHR(13)+CHR(10)+"i_DELAYRICONNECT="+ alltrim (STR ( i_DELAYRICONNECT)))  
     FWRITE(filedest , CHR(13)+CHR(10)+"i_ConnectTimeOut="+ alltrim (STR ( i_ConnectTimeOut)))   
     FWRITE(filedest , CHR(13)+CHR(10)+"i_CONNECTMSG='"+ alltrim (i_CONNECTMSG)+"'")       
     fclose(fileorig)
     fclose(filedest)
     DELETE FILE fileapp
    ENDIF
Endif

ON ERROR
masc.ecpquit()
ENDPROC
* --- Fine Area Manuale
