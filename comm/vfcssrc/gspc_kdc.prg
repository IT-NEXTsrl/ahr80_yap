* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_kdc                                                        *
*              Dettaglio configurazione costi                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_39]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-03                                                      *
* Last revis.: 2008-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_kdc",oParentObject))

* --- Class definition
define class tgspc_kdc as StdForm
  Top    = 7
  Left   = 21

  * --- Standard Properties
  Width  = 675
  Height = 412
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-19"
  HelpContextID=82466921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  TIPCOSTO_IDX = 0
  cPrg = "gspc_kdc"
  cComment = "Dettaglio configurazione costi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODCOS = space(5)
  o_CODCOS = space(5)
  w_CODCOM = space(15)
  w_TOTPREV = 0
  w_TOTCONS = 0
  w_VALCOM = space(5)
  w_SERIAL = space(10)
  w_ORIGINE = space(10)
  w_PARAM = space(3)
  w_CODICECOS = space(5)
  w_DESCOSTO = space(30)
  w_DATREG = ctod('  /  /  ')
  w_SOTTOCON = space(15)
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_kdcPag1","gspc_kdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TIPCOSTO'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODCOS=space(5)
      .w_CODCOM=space(15)
      .w_TOTPREV=0
      .w_TOTCONS=0
      .w_VALCOM=space(5)
      .w_SERIAL=space(10)
      .w_ORIGINE=space(10)
      .w_PARAM=space(3)
      .w_CODICECOS=space(5)
      .w_DESCOSTO=space(30)
      .w_DATREG=ctod("  /  /  ")
      .w_SOTTOCON=space(15)
      .w_CODCOS=oParentObject.w_CODCOS
      .w_CODCOM=oParentObject.w_CODCOM
      .w_VALCOM=oParentObject.w_VALCOM
      .w_DATREG=oParentObject.w_DATREG
      .oPgFrm.Page1.oPag.Zoom.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
          .DoRTCalc(1,5,.f.)
        .w_SERIAL = Nvl(.w_ZOOM.GetVar('SERIAL'),Space(10))
        .w_ORIGINE = Nvl(.w_ZOOM.GetVar('ORIGINE'),Space(10))
        .w_PARAM = Nvl(.w_ZOOM.GetVar('PARAM'),Space(3))
        .w_CODICECOS = .w_CODCOS
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODICECOS))
          .link_1_19('Full')
        endif
          .DoRTCalc(10,11,.f.)
        .w_SOTTOCON = Nvl(.w_ZOOM.GetVar('CODATT'),Space(15))
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODCOS=.w_CODCOS
      .oParentObject.w_CODCOM=.w_CODCOM
      .oParentObject.w_VALCOM=.w_VALCOM
      .oParentObject.w_DATREG=.w_DATREG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(1,5,.t.)
            .w_SERIAL = Nvl(.w_ZOOM.GetVar('SERIAL'),Space(10))
            .w_ORIGINE = Nvl(.w_ZOOM.GetVar('ORIGINE'),Space(10))
            .w_PARAM = Nvl(.w_ZOOM.GetVar('PARAM'),Space(3))
        if .o_CODCOS<>.w_CODCOS
            .w_CODICECOS = .w_CODCOS
          .link_1_19('Full')
        endif
        .DoRTCalc(10,11,.t.)
            .w_SOTTOCON = Nvl(.w_ZOOM.GetVar('CODATT'),Space(15))
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCODICECOS_1_19.visible=!this.oPgFrm.Page1.oPag.oCODICECOS_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDESCOSTO_1_20.visible=!this.oPgFrm.Page1.oPag.oDESCOSTO_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICECOS
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_lTable = "TIPCOSTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2], .t., this.TIPCOSTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICECOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICECOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODICECOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODICECOS)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICECOS = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOSTO = NVL(_Link_.TCDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODICECOS = space(5)
      endif
      this.w_DESCOSTO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCOSTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICECOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_8.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_8.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPREV_1_12.value==this.w_TOTPREV)
      this.oPgFrm.Page1.oPag.oTOTPREV_1_12.value=this.w_TOTPREV
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCONS_1_13.value==this.w_TOTCONS)
      this.oPgFrm.Page1.oPag.oTOTCONS_1_13.value=this.w_TOTCONS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICECOS_1_19.value==this.w_CODICECOS)
      this.oPgFrm.Page1.oPag.oCODICECOS_1_19.value=this.w_CODICECOS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOSTO_1_20.value==this.w_DESCOSTO)
      this.oPgFrm.Page1.oPag.oDESCOSTO_1_20.value=this.w_DESCOSTO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODCOS = this.w_CODCOS
    return

enddefine

* --- Define pages as container
define class tgspc_kdcPag1 as StdContainer
  Width  = 671
  height = 412
  stdWidth  = 671
  stdheight = 412
  resizeXpos=224
  resizeYpos=318
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Zoom as cp_zoombox with uid="YNJUPZWIWW",left=-4, top=73, width=681,height=288,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MAT_MAST",cZoomFile="GSPC_KDC",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryonLoad=.f.,bQueryOnDblClick=.f.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="GSZM_BCC",bRetriveAllRows=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 95530726

  add object oCODCOM_1_8 as StdField with uid="FVUJAHLUYL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 45270234,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=530, Top=9, InputMask=replicate('X',15)


  add object oObj_1_11 as cp_runprogram with uid="DAKIINCABC",left=683, top=144, width=135,height=20,;
    caption='GSPC_BDC',;
   bGlobalFont=.t.,;
    prg='GSPC_BDC("ZOOM")',;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Riempie lo zoom";
    , HelpContextID = 55443369

  add object oTOTPREV_1_12 as StdField with uid="YBHHCFEWHE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TOTPREV", cQueryName = "TOTPREV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale preventivi",;
    HelpContextID = 93010998,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=275, Top=366, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oTOTCONS_1_13 as StdField with uid="IWTGXKEKIH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TOTCONS", cQueryName = "TOTCONS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale consuntivi",;
    HelpContextID = 240008246,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=428, Top=366, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oCODICECOS_1_19 as StdField with uid="UZHFHQENBL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODICECOS", cQueryName = "CODICECOS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 76759205,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=149, Top=9, InputMask=replicate('X',5), cLinkFile="TIPCOSTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODICECOS"

  func oCODICECOS_1_19.mHide()
    with this.Parent.oContained
      return (Val( .w_CODCOS ) = 0)
    endwith
  endfunc

  func oCODICECOS_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCOSTO_1_20 as StdField with uid="UGVLRGOHMS",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCOSTO", cQueryName = "DESCOSTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 55452037,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=204, Top=9, InputMask=replicate('X',30)

  func oDESCOSTO_1_20.mHide()
    with this.Parent.oContained
      return (Val( .w_CODCOS ) = 0)
    endwith
  endfunc


  add object oBtn_1_25 as StdButton with uid="LOXLNHYYAV",left=6, top=363, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione";
    , HelpContextID = 8478998;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSPC_BDC(this.Parent.oContained,"DETTAGLIO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_SERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="XQUXQVUQUA",left=613, top=363, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 8478998;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_27 as cp_runprogram with uid="RAYSLYIIVE",left=683, top=167, width=135,height=20,;
    caption='GSPC_BDC',;
   bGlobalFont=.t.,;
    prg='GSPC_BDC("DETTAGLIO")',;
    cEvent = "w_zoom selected",;
    nPag=1;
    , ToolTipText = "Riempie lo zoom";
    , HelpContextID = 55443369

  add object oStr_1_3 as StdString with uid="NSAVGNBKNH",Visible=.t., Left=68, Top=9,;
    Alignment=0, Width=152, Height=15,;
    Caption="Materiali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (Not .w_CODCOS='MA')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="SNXQVSZNCH",Visible=.t., Left=68, Top=9,;
    Alignment=0, Width=152, Height=15,;
    Caption="Manodopera"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (Not .w_CODCOS='MD')
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="KJXJCZINAT",Visible=.t., Left=68, Top=9,;
    Alignment=0, Width=152, Height=15,;
    Caption="Costi generali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (Not .w_CODCOS='PNOTA')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="MDNSAQLOVC",Visible=.t., Left=68, Top=9,;
    Alignment=0, Width=152, Height=15,;
    Caption="Appalti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (Not .w_CODCOS='AP')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="WKEHFAWUSC",Visible=.t., Left=68, Top=9,;
    Alignment=0, Width=77, Height=15,;
    Caption="Altro"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (Val( .w_CODCOS ) = 0)
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="XDMSUMYONT",Visible=.t., Left=8, Top=9,;
    Alignment=1, Width=58, Height=15,;
    Caption="Voce:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (!(alltrim(.w_CODCOS)$'MA-MD-AP-PNOTA' or  Val( .w_CODCOS ) <> 0))
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="ODUHKTJEZQ",Visible=.t., Left=427, Top=9,;
    Alignment=1, Width=100, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="JATYZKJUNI",Visible=.t., Left=185, Top=366,;
    Alignment=1, Width=87, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="YCNTXFBDWX",Visible=.t., Left=15, Top=36,;
    Alignment=0, Width=238, Height=17,;
    Caption="In rosso i movimenti previsionali"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="VOBACHYVKT",Visible=.t., Left=15, Top=53,;
    Alignment=0, Width=238, Height=17,;
    Caption="In giallo i preventivi forfettari"    , ForeColor=RGB(245,245,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_kdc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
