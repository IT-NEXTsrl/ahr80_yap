* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_smm                                                        *
*              Visualizza schede movimenti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_103]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-28                                                      *
* Last revis.: 2012-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_smm",oParentObject))

* --- Class definition
define class tgspc_smm as StdForm
  Top    = -1
  Left   = 9

  * --- Standard Properties
  Width  = 752
  Height = 491
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-10"
  HelpContextID=76916631
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  VALUTE_IDX = 0
  CPAR_DEF_IDX = 0
  cPrg = "gspc_smm"
  cComment = "Visualizza schede movimenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPATT = space(1)
  w_CODCOM = space(15)
  o_CODCOM = space(15)
  w_CODATT = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_TIPMOV = space(1)
  w_DESCOM = space(30)
  w_DESATT = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_SERIAL = space(10)
  w_TIPOSTRU = space(1)
  w_TIPDOC = space(15)
  w_TOTPREV = 0
  w_CODVAL = space(3)
  w_DECTOT = 0
  o_DECTOT = 0
  w_CALCPICT = space(10)
  w_CODATT1 = space(10)
  w_CODATT2 = space(10)
  w_TOTCONS = 0
  w_TOTIMP = 0
  w_DESCRI = space(0)
  w_CALCPICT = 0
  w_ROWNUM = 0
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_TIPDOC2 = space(15)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_smmPag1","gspc_smm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CPAR_DEF'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPATT=space(1)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_TIPMOV=space(1)
      .w_DESCOM=space(30)
      .w_DESATT=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_SERIAL=space(10)
      .w_TIPOSTRU=space(1)
      .w_TIPDOC=space(15)
      .w_TOTPREV=0
      .w_CODVAL=space(3)
      .w_DECTOT=0
      .w_CALCPICT=space(10)
      .w_CODATT1=space(10)
      .w_CODATT2=space(10)
      .w_TOTCONS=0
      .w_TOTIMP=0
      .w_DESCRI=space(0)
      .w_CALCPICT=0
      .w_ROWNUM=0
      .w_FLVEAC=space(1)
      .w_CLADOC=space(2)
      .w_TIPDOC2=space(15)
      .w_CODCOM=oParentObject.w_CODCOM
      .w_CODATT=oParentObject.w_CODATT
      .w_DATAINI=oParentObject.w_DATAINI
      .w_DATAFIN=oParentObject.w_DATAFIN
      .w_DESCOM=oParentObject.w_DESCOM
      .w_DESATT=oParentObject.w_DESATT
      .w_TIPOSTRU=oParentObject.w_TIPOSTRU
        .w_TIPATT = 'A'
        .w_CODCOM = g_CODCOM
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCOM))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODATT))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_DATAFIN = i_DATSYS
        .w_TIPMOV = 'C'
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
          .DoRTCalc(7,8,.f.)
        .w_OBTEST = i_DATSYS
        .w_SERIAL = NVL(.w_ZOOM.Getvar("MASERIAL"),'')
          .DoRTCalc(11,11,.f.)
        .w_TIPDOC = NVL(.w_ZOOM.Getvar("MATIPMOV"),'')
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_CODVAL))
          .link_1_21('Full')
        endif
          .DoRTCalc(15,15,.f.)
        .w_CALCPICT = DEFPIP(.w_DECTOT)
          .DoRTCalc(17,20,.f.)
        .w_DESCRI = Nvl(.w_ZOOM.Getvar("MADESCRI"),Space(10))
        .w_CALCPICT = DEFPIP(.w_DECTOT)
        .w_ROWNUM = NVL(.w_ZOOM.Getvar("CPROWNUM"),'')
        .w_FLVEAC = NVL(.w_ZOOM.Getvar("MVFLVEACF"),'')
        .w_CLADOC = NVL(.w_ZOOM.Getvar("MVCLADOCF"),'')
      .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_TIPDOC2 = NVL(.w_ZOOM.Getvar("MATIPMO2"),'')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODCOM=.w_CODCOM
      .oParentObject.w_CODATT=.w_CODATT
      .oParentObject.w_DATAINI=.w_DATAINI
      .oParentObject.w_DATAFIN=.w_DATAFIN
      .oParentObject.w_DESCOM=.w_DESCOM
      .oParentObject.w_DESATT=.w_DESATT
      .oParentObject.w_TIPOSTRU=.w_TIPOSTRU
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CODCOM<>.w_CODCOM
            .w_CODATT = ' '
          .link_1_3('Full')
        endif
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .DoRTCalc(4,9,.t.)
            .w_SERIAL = NVL(.w_ZOOM.Getvar("MASERIAL"),'')
        .DoRTCalc(11,11,.t.)
            .w_TIPDOC = NVL(.w_ZOOM.Getvar("MATIPMOV"),'')
        .DoRTCalc(13,13,.t.)
          .link_1_21('Full')
        .DoRTCalc(15,15,.t.)
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIP(.w_DECTOT)
        endif
        .DoRTCalc(17,20,.t.)
            .w_DESCRI = Nvl(.w_ZOOM.Getvar("MADESCRI"),Space(10))
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIP(.w_DECTOT)
        endif
            .w_ROWNUM = NVL(.w_ZOOM.Getvar("CPROWNUM"),'')
            .w_FLVEAC = NVL(.w_ZOOM.Getvar("MVFLVEACF"),'')
            .w_CLADOC = NVL(.w_ZOOM.Getvar("MVCLADOCF"),'')
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
            .w_TIPDOC2 = NVL(.w_ZOOM.Getvar("MATIPMO2"),'')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
    endwith
  return

  proc Calculate_LXOPMHQUAP()
    with this
          * --- Cambio zoom
          gspc_blm(this;
              ,"Titolo";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_TIPMOV Changed")
          .Calculate_LXOPMHQUAP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCOM
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_CODVAL = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_CODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_3'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_2.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_2.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_3.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_3.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_4.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_4.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_5.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_5.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPMOV_1_6.RadioValue()==this.w_TIPMOV)
      this.oPgFrm.Page1.oPag.oTIPMOV_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_11.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_11.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_13.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_13.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPREV_1_20.value==this.w_TOTPREV)
      this.oPgFrm.Page1.oPag.oTOTPREV_1_20.value=this.w_TOTPREV
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCONS_1_27.value==this.w_TOTCONS)
      this.oPgFrm.Page1.oPag.oTOTCONS_1_27.value=this.w_TOTCONS
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMP_1_28.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oTOTIMP_1_28.value=this.w_TOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_29.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_29.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFIN_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODCOM = this.w_CODCOM
    this.o_DECTOT = this.w_DECTOT
    return

enddefine

* --- Define pages as container
define class tgspc_smmPag1 as StdContainer
  Width  = 748
  height = 491
  stdWidth  = 748
  stdheight = 491
  resizeXpos=296
  resizeYpos=257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM_1_2 as StdField with uid="KPOBNNLEJU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 154322138,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=81, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_3('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCODATT_1_3 as StdField with uid="DJULSFJDPS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 31769818,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=81, Top=35, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODATT_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT
     i_obj.ecpSave()
  endproc

  add object oDATAINI_1_4 as StdField with uid="FZHOQKZPFW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale",;
    HelpContextID = 124529974,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=482, Top=9

  add object oDATAFIN_1_5 as StdField with uid="KWEQXHLUOD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale",;
    HelpContextID = 37498166,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=482, Top=35


  add object oTIPMOV_1_6 as StdCombo with uid="IJNEWMVGSD",rtseq=6,rtrep=.f.,left=608,top=9,width=133,height=21;
    , ToolTipText = "Tipo movimento (solo evasi mostra solo gli impegni completamente evasi)";
    , HelpContextID = 2623946;
    , cFormVar="w_TIPMOV",RowSource=""+"Preventivo,"+"Consuntivo,"+"Impegni finanziari,"+"Solo impegni evasi,"+"Tutti i costi,"+"Ricavi preventivi,"+"Ricavi consuntivi,"+"Ricavi residui,"+"Tutti i ricavi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPMOV_1_6.RadioValue()
    return(iif(this.value =1,"P",;
    iif(this.value =2,"C",;
    iif(this.value =3,"I",;
    iif(this.value =4,"E",;
    iif(this.value =5,"T",;
    iif(this.value =6,"O",;
    iif(this.value =7,"F",;
    iif(this.value =8,"A",;
    iif(this.value =9,"R",;
    space(1)))))))))))
  endfunc
  func oTIPMOV_1_6.GetRadio()
    this.Parent.oContained.w_TIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oTIPMOV_1_6.SetRadio()
    this.Parent.oContained.w_TIPMOV=trim(this.Parent.oContained.w_TIPMOV)
    this.value = ;
      iif(this.Parent.oContained.w_TIPMOV=="P",1,;
      iif(this.Parent.oContained.w_TIPMOV=="C",2,;
      iif(this.Parent.oContained.w_TIPMOV=="I",3,;
      iif(this.Parent.oContained.w_TIPMOV=="E",4,;
      iif(this.Parent.oContained.w_TIPMOV=="T",5,;
      iif(this.Parent.oContained.w_TIPMOV=="O",6,;
      iif(this.Parent.oContained.w_TIPMOV=="F",7,;
      iif(this.Parent.oContained.w_TIPMOV=="A",8,;
      iif(this.Parent.oContained.w_TIPMOV=="R",9,;
      0)))))))))
  endfunc


  add object oBtn_1_7 as StdButton with uid="FEXHDYTGKP",left=693, top=35, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegui interrogazione";
    , HelpContextID = 14596586;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSPC_BLM(this.Parent.oContained,"Requery")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CODCOM))
      endwith
    endif
  endfunc


  add object ZOOM as cp_zoombox with uid="DLDXJMHYIL",left=-1, top=85, width=751,height=320,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='MAT_MAST',cZoomFile='GSPC_SMM',bOptions=.F.,bAdvOptions=.F.,bReadOnly=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.F.,;
    cEvent = "Reload",;
    nPag=1;
    , HelpContextID = 13521178


  add object oBtn_1_9 as StdButton with uid="WEKDDWSULJ",left=320, top=439, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il movimento selezionato";
    , HelpContextID = 35739791;
    , Caption='\<Dettaglio';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSPC_BLM(this.Parent.oContained,"Lancio")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_SERIAL))
      endwith
    endif
  endfunc

  add object oDESCOM_1_11 as StdField with uid="DSQUMQEKVI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 154263242,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=213, Top=9, InputMask=replicate('X',30)

  add object oDESATT_1_13 as StdField with uid="ELPTWPSSVE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 31710922,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=213, Top=35, InputMask=replicate('X',30)

  add object oTOTPREV_1_20 as StdField with uid="PXLHICHSTL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_TOTPREV", cQueryName = "TOTPREV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale preventivo",;
    HelpContextID = 16040906,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=359, Top=409, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oTOTCONS_1_27 as StdField with uid="NBNBRMTQGQ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_TOTCONS", cQueryName = "TOTCONS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale consuntivo",;
    HelpContextID = 137479114,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=487, Top=409, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oTOTIMP_1_28 as StdField with uid="GEHMGHHIJG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_TOTIMP", cQueryName = "TOTIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale impegnato",;
    HelpContextID = 105628618,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=614, Top=409, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oDESCRI_1_29 as StdMemo with uid="XUUSANIQVY",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 218226378,;
   bGlobalFont=.t.,;
    Height=63, Width=239, Left=56, Top=409


  add object oBtn_1_33 as StdButton with uid="DEMANOHLBY",left=422, top=439, width=48,height=45,;
    CpPicture="BMP\COSTI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza totali per tipo di costo";
    , HelpContextID = 195119910;
    , Tabstop=.F., Caption='\<Costi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      do GSPC_KVC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_SERIAL) and not .w_TIPMOV $ 'O-F-A-R')
      endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="ELIAPFPRJG",left=371, top=439, width=48,height=45,;
    CpPicture="BMP\MODIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Modifica i dati del documento di origine visualizzato";
    , HelpContextID = 194683865;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      do GSPC_KVA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPDOC2 = "DOCUMXXX")
      endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="KOTWJAFPMC",left=693, top=439, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 84234054;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_40 as cp_runprogram with uid="YDNENDUPKE",left=255, top=493, width=247,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPC_BLM('Lancio')",;
    cEvent = "w_ZOOM selected",;
    nPag=1;
    , HelpContextID = 13521178


  add object oObj_1_41 as cp_runprogram with uid="AXSEJAOWDU",left=258, top=525, width=247,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPC_BLM('Chiudi')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 13521178

  add object oStr_1_10 as StdString with uid="CAYWGXFPIL",Visible=.t., Left=3, Top=9,;
    Alignment=1, Width=74, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="AHPZKYFISF",Visible=.t., Left=3, Top=35,;
    Alignment=1, Width=74, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RMBNINOGIM",Visible=.t., Left=438, Top=9,;
    Alignment=1, Width=39, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="GGNHLPBXRB",Visible=.t., Left=438, Top=35,;
    Alignment=1, Width=39, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="NUELMSWUPE",Visible=.t., Left=565, Top=9,;
    Alignment=1, Width=40, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ISJJMAUIBH",Visible=.t., Left=300, Top=409,;
    Alignment=1, Width=56, Height=18,;
    Caption="Totali:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="EYERPFQUKA",Visible=.t., Left=3, Top=409,;
    Alignment=1, Width=50, Height=15,;
    Caption="Descr.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_smm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
