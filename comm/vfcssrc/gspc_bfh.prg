* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bfh                                                        *
*              Elabora scadenze commesse                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-18                                                      *
* Last revis.: 2005-12-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bfh",oParentObject)
return(i_retval)

define class tgspc_bfh as StdBatch
  * --- Local variables
  w_NUMSCA = 0
  * --- WorkFile variables
  BFH1_idx=0
  BFH3_idx=0
  RAT_COMM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Scadenze Commesse per Cash Flow (da GSPC_KCF)
    AH_Msg("Eliminazione scadenze da riaggiornare...")
    this.oParentObject.w_DATINI = IIF(EMPTY(this.oParentObject.w_DATINI), i_INIDAT, this.oParentObject.w_DATINI)
    * --- Try
    local bErr_037289B0
    bErr_037289B0=bTrsErr
    this.Try_037289B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ERRORMSG("Elaborazione abbandonata. Errore: %1",16,"",Message())
    endif
    bTrsErr=bTrsErr or bErr_037289B0
    * --- End
    this.oParentObject.w_DATINI = cp_CharToDate("  -  -  ")
  endproc
  proc Try_037289B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Delete from RAT_COMM
    i_nConn=i_TableProp[this.RAT_COMM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAT_COMM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".SCSERIAL = "+i_cQueryTable+".SCSERIAL";
    
      do vq_exec with '..\COMM\EXE\QUERY\GSPC8BFH',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errrore Eliminazione Rate Scadenze Commessa'
      return
    endif
    AH_Msg("Ricerca scadenze documenti...")
    * --- Leggo Tutte le Rate dei Documenti selezionati, dettagliandole per le Commesse di ciascun documento
    * --- Create temporary table BFH1
    i_nIdx=cp_AddTableDef('BFH1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\COMM\EXE\QUERY\GSPC1BFH',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.BFH1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Ricerco il Totale delle Rate di ciascun documento (rielaborando il temporaneo precedente)
    AH_Msg("Ricerca totale importi documenti...")
    * --- Create temporary table BFH3
    i_nIdx=cp_AddTableDef('BFH3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\COMM\EXE\QUERY\GSPC3BFH',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.BFH3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    AH_Msg("Scrive totale importi documenti...")
    * --- Write into BFH1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.BFH1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BFH1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.BFH3_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.BFH1_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="BFH1.SERIAL = _t2.SERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTRAT = _t2.TOTRAT";
          +i_ccchkf;
          +" from "+i_cTable+" BFH1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="BFH1.SERIAL = _t2.SERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BFH1, "+i_cQueryTable+" _t2 set ";
          +"BFH1.TOTRAT = _t2.TOTRAT";
          +Iif(Empty(i_ccchkf),"",",BFH1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="BFH1.SERIAL = _t2.SERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BFH1 set ";
          +"TOTRAT = _t2.TOTRAT";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERIAL = "+i_cQueryTable+".SERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTRAT = (select TOTRAT from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Scrittura Totale Importi Documenti'
      return
    endif
    * --- Drop temporary table BFH3
    i_nIdx=cp_GetTableDefIdx('BFH3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('BFH3')
    endif
    AH_Msg("Scrive dettaglio rata importi di commessa...")
    * --- Create temporary table BFH3
    i_nIdx=cp_AddTableDef('BFH3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\COMM\EXE\QUERY\GSPC4BFH',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.BFH3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into BFH1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.BFH1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BFH1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.BFH3_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.BFH1_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="BFH1.SERIAL = _t2.SERIAL";
              +" and "+"BFH1.NUMRAT = _t2.NUMRAT";
              +" and "+"BFH1.CODCOM = _t2.CODCOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DETCOM = _t2.DETCOM";
          +i_ccchkf;
          +" from "+i_cTable+" BFH1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="BFH1.SERIAL = _t2.SERIAL";
              +" and "+"BFH1.NUMRAT = _t2.NUMRAT";
              +" and "+"BFH1.CODCOM = _t2.CODCOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BFH1, "+i_cQueryTable+" _t2 set ";
          +"BFH1.DETCOM = _t2.DETCOM";
          +Iif(Empty(i_ccchkf),"",",BFH1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="BFH1.SERIAL = _t2.SERIAL";
              +" and "+"BFH1.NUMRAT = _t2.NUMRAT";
              +" and "+"BFH1.CODCOM = _t2.CODCOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BFH1 set ";
          +"DETCOM = _t2.DETCOM";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERIAL = "+i_cQueryTable+".SERIAL";
              +" and "+i_cTable+".NUMRAT = "+i_cQueryTable+".NUMRAT";
              +" and "+i_cTable+".CODCOM = "+i_cQueryTable+".CODCOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DETCOM = (select DETCOM from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore scrittura Dettaglio Rate Importi di Commessa'
      return
    endif
    * --- Drop temporary table BFH3
    i_nIdx=cp_GetTableDefIdx('BFH3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('BFH3')
    endif
    this.w_NUMSCA = 0
    * --- Try
    local bErr_03729E80
    bErr_03729E80=bTrsErr
    this.Try_03729E80()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Se non ho scadenze la Insert da Errore
      if this.w_NUMSCA=0
        * --- accept error
        bTrsErr=.f.
      endif
    endif
    bTrsErr=bTrsErr or bErr_03729E80
    * --- End
    * --- Eseguo questa WRITE assolutamente inutile solo per valorizzare il campo
    *     CPCCCHK
    * --- Select from RAT_COMM
    i_nConn=i_TableProp[this.RAT_COMM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAT_COMM_idx,2],.t.,this.RAT_COMM_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" RAT_COMM ";
          +" where CPCCCHK Is Null";
           ,"_Curs_RAT_COMM")
    else
      select * from (i_cTable);
       where CPCCCHK Is Null;
        into cursor _Curs_RAT_COMM
    endif
    if used('_Curs_RAT_COMM')
      select _Curs_RAT_COMM
      locate for 1=1
      do while not(eof())
      * --- Write into RAT_COMM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RAT_COMM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RAT_COMM_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RAT_COMM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCCONFER ="+cp_NullLink(cp_ToStrODBC(_Curs_RAT_COMM.SCCONFER),'RAT_COMM','SCCONFER');
            +i_ccchkf ;
        +" where ";
            +"SCSERIAL = "+cp_ToStrODBC(_Curs_RAT_COMM.SCSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(_Curs_RAT_COMM.CPROWNUM);
            +" and SCCODCOM = "+cp_ToStrODBC(_Curs_RAT_COMM.SCCODCOM);
               )
      else
        update (i_cTable) set;
            SCCONFER = _Curs_RAT_COMM.SCCONFER;
            &i_ccchkf. ;
         where;
            SCSERIAL = _Curs_RAT_COMM.SCSERIAL;
            and CPROWNUM = _Curs_RAT_COMM.CPROWNUM;
            and SCCODCOM = _Curs_RAT_COMM.SCCODCOM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_RAT_COMM
        continue
      enddo
      use
    endif
    * --- Drop temporary table BFH1
    i_nIdx=cp_GetTableDefIdx('BFH1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('BFH1')
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_NUMSCA=0
      ah_ERRORMSG("Non esistono scadenze da inserire",48)
    else
      ah_ERRORMSG("Elaborazione completata%0Inserire n.%1 scadenze",64, "", ALLTRIM(STR(this.w_NUMSCA)))
    endif
    return
  proc Try_03729E80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into RAT_COMM
    i_nConn=i_TableProp[this.RAT_COMM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAT_COMM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COMM\EXE\QUERY\GSPC7BFH",this.RAT_COMM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in Inserimento Rate Scadenze'
      return
    endif
    this.w_NUMSCA = i_ROWS
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='*BFH1'
    this.cWorkTables[2]='*BFH3'
    this.cWorkTables[3]='RAT_COMM'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_RAT_COMM')
      use in _Curs_RAT_COMM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
