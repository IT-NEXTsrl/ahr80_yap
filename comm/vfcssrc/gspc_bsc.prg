* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bsc                                                        *
*              Stampa riepilogo costi                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_223]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-11                                                      *
* Last revis.: 2006-02-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bsc",oParentObject)
return(i_retval)

define class tgspc_bsc as StdBatch
  * --- Local variables
  w_LIVELLO = space(100)
  w_IMPORTO_MA = 0
  w_NUMPOINT = 0
  w_CONS_MA = 0
  w_ORDI_MA = 0
  w_LUNGH = 0
  w_IMPORTO_MD = 0
  w_TIPPRE = space(1)
  w_CONS_MD = 0
  w_ORDI_MD = 0
  w_POSREC = 0
  w_IMPORTO_AP = 0
  w_CONS_AP = 0
  w_ORDI_AP = 0
  w_PADRE = space(15)
  w_IMPORTO_AL = 0
  w_CONS_AL = 0
  w_ORDI_AL = 0
  w_RICERCA = space(100)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_TIPSTR = space(1)
  w_RIFKEY = space(100)
  w_DOCVAL = space(3)
  w_CAOVAL = 0
  w_NUMREC = .f.
  w_DATDOC = ctod("  /  /  ")
  w_CAOCOMM = 0
  w_IMP_MA = 0
  w_ORD_MA = 0
  w_IMP_MD = 0
  w_ORD_MD = 0
  w_IMP_AP = 0
  w_ORD_AP = 0
  w_IMP_AL = 0
  w_ORD_AL = 0
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Riepilogo Costi
    * --- Flag per scelta selezione da data a data
    * --- Parametro valuta commessa
    * --- Parametro di inizio periodo selezione
    * --- Parametro di fine periodo selezione
    * --- Parametro di inizio selezione costi
    * --- Parametro di fine selezione costi
    * --- Segnala al GSPC_BS3 se il cursore della treeview per la commessa selezionata ha elementi
    if IsNull( this.oParentObject.w_OBJPADRE )
      * --- Se lanciata da menu calcolo la struttura
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Prendo il cursore della Tree View
      this.oParentObject.w_CURSORNA = this.oParentObject.w_OBJPADRE.w_CURSORNA
    endif
    if Not Used ( this.oParentObject.w_CURSORNA )
      if ! IsNull( this.oParentObject.w_OBJPADRE )
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        ah_ErrorMsg("Commessa senza elementi","","")
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Metto la struttura nel cursore TEMP
    Select * From ( this.oParentObject.w_CURSORNA ) Order By LvlKey Into Cursor Temp NoFilter
    if IsNull(this.oParentObject.w_OBJPADRE) and used(this.oParentObject.w_CURSORNA)
      Use in this.oParentObject.w_CURSORNA 
    endif
    ah_Msg("Lettura importi...",.T.)
    * --- Se preventivo iniziale prendo tutte le attivit� (anche quelle con padre forfettario)
    this.w_TIPPRE = IIF(this.oParentObject.w_TIPO="I","N"," ")
    * --- Calcolo gli Importi da stampare a preventivo
    Vq_Exec ("..\Comm\Exe\Query\Gspc_Bs1.Vqr",This,"F1")
    Vq_Exec ("..\Comm\Exe\Query\Gspc_Bs2.Vqr",This,"F2")
    Select F1.CODATT AS CODATT, F1.TIPATT AS TIPATT, F1.FLPREV AS FLPREV, ; 
 NVL(F2.FORF_MA, 0) AS FORF_MA, NVL(F2.FORF_MD, 0) AS FORF_MD, ; 
 NVL(F2.FORF_AP, 0) AS FORF_AP, NVL(F2.FORF_AL, 0) AS FORF_AL ; 
 FROM F1 LEFT OUTER JOIN F2 ON F1.CODATT=F2.CODATT INTO CURSOR F3
    if USED("F1")
      SELECT F1
      USE
    endif
    if USED("F2")
      SELECT F2
      USE
    endif
    if this.oParentObject.w_FILTRO="N"
      * --- Caso per nessuna selezione
      Vq_Exec ("..\Comm\Exe\Query\Gspc1Bsc.Vqr",This,"Importi")
    else
      * --- Caso per selezione da data a data
      this.oParentObject.w_TIPO = "A"
      * --- Valorizzo comunque il tipo di stampa ad Aggiornato
      Vq_Exec ("..\Comm\Exe\Query\Gspc2Bsc.Vqr",This,"Importi2") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc3Bsc.Vqr",This,"Importi3") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc4Bsc.Vqr",This,"Importi4") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc5Bsc.Vqr",This,"Importi5") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc6Bsc.Vqr",This,"Importi6") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc8Bsc.Vqr",This,"Importi8") 
 Select * from Importi2 ; 
 union all select * from Importi3 ; 
 union all select * from Importi4 ; 
 union all select * from Importi5 ; 
 union all select * from Importi6 ; 
 union all select * from Importi8 ; 
 into cursor Importi nofilter
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Entra con w_FILTRO='D'
    endif
    * --- Raggruppo i vari Costi relativi alle Attivit� e ai sottoconti
    Select CodAtt,TipAtt, Min( FlPrev ) As FlPrev, Sum(Forf_MA) As Forf_MA, Sum(Costi_MA) As Costi_MA, ; 
 Sum(Forf_MD) As Forf_MD, Sum(Costi_MD) As Costi_MD, Sum(Forf_AP) As Forf_AP, Sum(Costi_AP) As Costi_AP, ; 
 Sum(Forf_AL) As Forf_AL, Sum(Costi_AL) As Costi_AL, ; 
 Sum(Cons_MA) As Cons_MA, Sum(Cons_MD) As Cons_MD, Sum(Cons_AP) As Cons_AP, Sum(Cons_AL) As Cons_AL, ; 
 Sum(Ordi_MA) As Ordi_MA, Sum(Ordi_MD) As Ordi_MD, Sum(Ordi_AP) As Ordi_AP, Sum(Ordi_AL) As Ordi_AL ; 
 from Importi Group By CodAtt,TipAtt Into Cursor Importi NoFilter
    * --- Costruisco il Cursore di Stampa - Solo Codice Attivit� e Tipo (La commessa � uguale)
    * --- All'interno dei Campi Preventivo_xx verranno posti gli Importi (Sub Totali)
    * --- Il campo CODPAD viene riempito nel calcolo dei totali (mi dice chi � il padre del nodo)
    Select Temp.*, Importi.FLPrev, AtCodAtt As CodPad, Occurs(".",LvlKey) As NumPoint, ; 
 IIF(Importi.FLPrev="S",Importi.Forf_MA,Importi.Costi_MA) As Preventivo_MA, ; 
 IIF(Importi.FLPrev="S",Importi.Forf_MD,Importi.Costi_MD) As Preventivo_MD, ; 
 IIF(Importi.FLPrev="S",Importi.Forf_AP,Importi.Costi_AP) As Preventivo_AP, ; 
 IIF(Importi.FLPrev="S",Importi.Forf_AL,Importi.Costi_AL) As Preventivo_AL, ; 
 Importi.Cons_Ma ,Importi.Cons_Md , Importi.Cons_Ap , Importi.Cons_Al, ; 
 Importi.Ordi_Ma , Importi.Ordi_Md , Importi.Ordi_Ap , Importi.Ordi_Al ; 
 from Temp Left Outer Join Importi On Importi.TipAtt=Temp.AtTipAtt And Importi.CodAtt=Temp.AtCodAtt ; 
 Order By LvlKey Desc Into Cursor Temp NoFilter 
 Cur = WrCursor ("Temp")
    if this.oParentObject.w_TIPO="A" and this.oParentObject.w_WITHATTI="S"
      * --- Si memorizzano gli importi attivit� perch� devono essere cancellati 
      *     da TEMP se il preventivo � aggiornato.
      *     Saranno reinseriti alla fine.
      select attipatt,atcodatt,preventivo_ma,preventivo_md, ; 
 preventivo_ap,preventivo_al from temp ; 
 where attipatt="A" and (nvl(preventivo_ma,0)<>0 or nvl(preventivo_md,0)<>0 ; 
 or nvl(preventivo_ap,0)<>0 or nvl(preventivo_al,0)<>0) ; 
 into cursor RIPRISTINO
    endif
    Select TEMP.LVLKEY, F3.* from TEMP inner join F3 ; 
 on TEMP.ATCODATT=F3.CODATT and ; 
 TEMP.ATTIPATT=F3.TIPATT into cursor FORFAIT
    Select FORFAIT
    Go Top
    Scan
    if this.oParentObject.w_TIPO="I" or (this.oParentObject.w_TIPO="A" and FLPREV="S")
      update TEMP set ; 
 TEMP.PREVENTIVO_MA=FORFAIT.FORF_MA, ; 
 TEMP.PREVENTIVO_MD=FORFAIT.FORF_MD, ; 
 TEMP.PREVENTIVO_AP=FORFAIT.FORF_AP, ; 
 TEMP.PREVENTIVO_AL=FORFAIT.FORF_AL ; 
 where TEMP.ATCODATT=FORFAIT.CODATT and ; 
 TEMP.ATTIPATT=FORFAIT.TIPATT
      L_CHIAVE=alltrim(LvlKey ) 
 L_LUNGH=len(L_CHIAVE)
      update TEMP set ; 
 TEMP.PREVENTIVO_MA=0, ; 
 TEMP.PREVENTIVO_MD=0, ; 
 TEMP.PREVENTIVO_AP=0, ; 
 TEMP.PREVENTIVO_AL=0 ; 
 where left(alltrim(TEMP.LVLKEY),L_LUNGH)=L_CHIAVE ; 
 and ATTIPCOM="A"
    endif
    EndScan
    Select Temp
    Go Top
    * --- Calcolo Gli Importi. La procedura Scorre riga per riga e per ognuna
    * --- Vado anche a riempire CodPad con il conto padre della riga
    * --- Il cursore deve essere ordinato in modo da consultare prima i figli dei padri (lvlkey DESC)
    * --- Ogni figlio infatti cerca il padre e in esso somma il suo importo
    Scan
    this.w_LIVELLO = RTrim(Temp.LvlKey)
    this.w_LUNGH = Len(this.w_LIVELLO)
    this.w_POSREC = RecNo()
    this.w_NUMPOINT = Occurs(".",this.w_LIVELLO)
    ah_Msg("Calcolo totali di stampa...: %1",.T.,,,this.w_PADRE)
    * --- Per ogni record aggiungo nel padre i vari importi
    if this.w_NUMPOINT>0
      * --- Questa variabile contiene il LEVELKEY del padre
      this.w_RICERCA = Left( Left ( this.w_LIVELLO , AT ( "." , this.w_LIVELLO , this.w_NUMPOINT ) -1 )+space(200),200)
      this.w_CONS_MA = Nvl(Cons_MA,0)
      this.w_ORDI_MA = Nvl(Ordi_MA,0)
      this.w_IMPORTO_MA = Nvl(Preventivo_MA,0)
      this.w_CONS_MD = Nvl(Cons_MD,0)
      this.w_ORDI_MD = Nvl(Ordi_MD,0)
      this.w_IMPORTO_MD = Nvl(Preventivo_MD,0)
      this.w_CONS_AP = Nvl(Cons_AP,0)
      this.w_ORDI_AP = Nvl(Ordi_AP,0)
      this.w_IMPORTO_AP = Nvl(Preventivo_AP,0)
      this.w_CONS_AL = Nvl(Cons_AL,0)
      this.w_ORDI_AL = Nvl(Ordi_AL,0)
      this.w_IMPORTO_AL = Nvl(Preventivo_AL,0)
      Select Temp
      Go Top
      Locate For LvlKey = this.w_RICERCA
      if Found()
        this.w_PADRE = Temp.AtCodAtt
        Replace Preventivo_MA With Nvl(Preventivo_MA,0)+Nvl(this.w_IMPORTO_MA,0)
        Replace Preventivo_MD With Nvl(Preventivo_MD,0)+Nvl(this.w_IMPORTO_MD,0)
        Replace Preventivo_AP With Nvl(Preventivo_AP,0)+Nvl(this.w_IMPORTO_AP,0)
        Replace Preventivo_AL With Nvl(Preventivo_AL,0)+Nvl(this.w_IMPORTO_AL,0)
        Replace Cons_MA With Nvl(Cons_MA,0)+Nvl(this.w_CONS_MA,0)
        Replace Cons_MD With Nvl(Cons_MD,0)+Nvl(this.w_CONS_MD,0)
        Replace Cons_AP With Nvl(Cons_AP,0)+Nvl(this.w_CONS_AP,0)
        Replace Cons_AL With Nvl(Cons_AL,0)+Nvl(this.w_CONS_AL,0)
        Replace Ordi_MA With Nvl(Ordi_MA,0)+Nvl(this.w_ORDI_MA,0)
        Replace Ordi_MD With Nvl(Ordi_MD,0)+Nvl(this.w_ORDI_MD,0)
        Replace Ordi_AP With Nvl(Ordi_AP,0)+Nvl(this.w_ORDI_AP,0)
        Replace Ordi_AL With Nvl(Ordi_AL,0)+Nvl(this.w_ORDI_AL,0)
        Go this.w_POSREC
        Select Temp
        Replace CodPad With this.w_PADRE
      endif
    endif
    EndScan
    * --- Passo tutto al cursore di stampa (Elimino le Attivit�) Stampo a livelli
    do case
      case this.oParentObject.w_WITHATTI = "S"
        * --- Stampo anche le Attivit�
        Select *,Occurs(".",LvlKey) As Ordine From Temp Order By Ordine,LvlKey Into Cursor bsc_curs NoFilter
      case this.oParentObject.w_WITHATTI="N"
        * --- Stampo fino ai Sotto Conti
        Select *,Occurs(".",LvlKey) As Ordine from Temp where AttipAtt<>"A" Order By Ordine,LvlKey Into Cursor bsc_curs NoFilter
      otherwise
        * --- Solo primo livello
        Select *,Occurs(".",LvlKey) As Ordine From Temp Where len(alltrim(LvlKey))<=5 ; 
 Order By Ordine,LvlKey Into Cursor bsc_curs NoFilter
    endcase
    if ! IsNull( this.oParentObject.w_OBJPADRE ) and !empty(this.oParentObject.w_CODCONTO)
      * --- Se lanciato da Gestione Progetto, occorre fare filtro sul conto
      select bsc_curs 
 go top 
 locate for ATCODATT=this.oParentObject.w_CODCONTO and ATTIPATT="P"
      if found()
        L_NODOPADRE=alltrim(LVLKEY) 
 select * from bsc_curs where ; 
 left(alltrim(LVLKEY),len(L_NODOPADRE)) = L_NODOPADRE and; 
 len(alltrim(LVLKEY)) >= len(L_NODOPADRE) ; 
 order by ORDINE, LVLKEY into cursor bsc_curs nofilter
      else
        L_NODOPADRE=alltrim(LVLKEY) 
 select * from bsc_curs where .f. into cursor bsc_curs nofilter
      endif
    endif
    if this.oParentObject.w_TIPO="A" and this.oParentObject.w_WITHATTI="S"
      * --- Si reinseriscono gli importi attivit� perch� devono sono stati cancellati 
      *     da TEMP se il preventivo � aggiornato.
      =WrCursor("bsc_curs") 
 select ripristino 
 scan 
 update bsc_curs set ; 
 bsc_curs.preventivo_ma=ripristino.preventivo_ma,; 
 bsc_curs.preventivo_md=ripristino.preventivo_md,; 
 bsc_curs.preventivo_ap=ripristino.preventivo_ap,; 
 bsc_curs.preventivo_al=ripristino.preventivo_al; 
 where bsc_curs.attipatt=ripristino.attipatt and ; 
 bsc_curs.atcodatt=ripristino.atcodatt 
 endscan 
 use 
 select bsc_curs 
    endif
    if this.oParentObject.w_NOZERI="S"
      * --- Stampo solo voci con importi diversi da 0
      Select * from bsc_curs into cursor APPOGGIO 
 use in bsc_curs 
 Select * From APPOGGIO Where ; 
 Preventivo_MA<>0 Or Preventivo_MD<>0 Or Preventivo_AP<>0 Or Preventivo_AL<>0 Or; 
 Cons_MA<>0 Or Cons_MD<>0 Or Cons_AP<>0 Or Cons_AL<>0 Or; 
 Ordi_MA<>0 Or Ordi_MD<>0 Or Ordi_AP<>0 Or Ordi_AL<>0 ; 
 Order By Ordine,LvlKey Into Cursor bsc_curs NoFilter 
 use in APPOGGIO 
 select bsc_curs 
    endif
    Select * from bsc_curs into cursor __TMP__ 
 use in bsc_curs 
 Select __TMP__
    * --- Selezioni nel report
    if used("commesse")
      L_COMMESSA = commesse.cncodcan 
 L_DESCOM = commesse.cndescan 
 L_CODVAL = commesse.cncodval 
 L_DECIMALI = commesse.vadectot
    else
      L_COMMESSA = this.oParentObject.w_CODCOM 
 L_DESCOM = this.oParentObject.w_DESCOM 
 L_CODVAL = this.oParentObject.w_CODVAL 
 L_DECIMALI = this.oParentObject.w_DECTOT
    endif
    L_DATAINI = this.oParentObject.w_DATAINI 
 L_DATAFIN = this.oParentObject.w_DATAFIN 
 l_CODCOSTO1 = this.oParentObject.w_DESCOS1 
 l_CODCOSTO2 = this.oParentObject.w_DESCOS2 
 L_FILTRO = this.oParentObject.w_FILTRO 
 L_TIPO = this.oParentObject.w_TIPO 
 L_CODCONTO = this.oParentObject.w_CODCONTO 
 L_DESCON = this.oParentObject.w_DESCON 
 L_WITHATTI = this.oParentObject.w_WITHATTI 
 L_NOZERI = this.oParentObject.w_NOZERI
    if used("Selezioni")
      use in Selezioni
    endif
    create cursor selezioni ( L_COMMESSA C (15), L_DESCOM C(30), L_DECIMALI N(1,0), L_CODVAL C(3))
    =WrCursor("Selezioni")
    insert into Selezioni from memvar
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea Cursore Preventivo
    this.oParentObject.w_CURSORNA = SYS(2015)
    * --- Definizione parametri da passare alla CP_EXPDB
    this.w_COUTCURS = this.oParentObject.w_CURSORNA
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = "CPROWORD"
    * --- Altri campi (campo per ordinamento)
    this.w_TIPSTR = "P"
    ah_Msg("Caricamento progetto...",.T.)
    if Empty ( this.oParentObject.w_CODCONTO )
      * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
      VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
    else
      * --- Recupero solo la parte di struttura identificata dal conto
      VQ_EXEC("..\COMM\EXE\QUERY\GSPC0BSC.VQR",this,"query")
    endif
    Select "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if used("__tmp__")
        use in __tmp__
      endif
    else
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_Msg("La struttura della commessa %1 � vuota",.F.,.F.,1,alltrim(this.oParentObject.w_CODCOM))
      this.oParentObject.w_OK = .F.
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Traduzione Doc. Valuta diversa e parzialmente importati a valore (Simil Pag2 GSPC_BRS)
    * --- Leggo il tasso di conversione della valuta (Se letto nella maschera a volte non lo prende)
    if this.oParentObject.w_CODVAL<>g_PERVAL
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VACAOVAL"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_CODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VACAOVAL;
          from (i_cTable) where;
              VACODVAL = this.oParentObject.w_CODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAOCOMM = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_CAOCOMM = g_CAOVAL
    endif
    * --- Recupero i documenti evasi parzialmente a valore con valuta diversa dalla valurta di commessa
    Vq_Exec ( "..\Comm\Exe\Query\Gspc7Bsc.Vqr" , This , "Curs_Val7" )
    Vq_Exec ( "..\Comm\Exe\Query\Gspc11sc.Vqr" , This , "Curs_Val11" )
    select * from Curs_Val7 ;
    union all ;
    select * from Curs_Val11 ;
    into cursor Curs_Val nofilter
    Select "Curs_Val"
    Cur = WrCursor ("Curs_Val")
    Go Top
    this.w_NUMREC = RECCOUNT()>0
    Scan
    this.w_CAOVAL = Nvl ( CAOVAL , 0 )
    this.w_DATDOC = Cp_ToDate(DATDOC)
    * --- Se Valuta del documento inesistente non applico la conversione (Dovrebbe esserci sempre)
    this.w_DOCVAL = Nvl ( CODVAL , this.oParentObject.w_CODVAL )
    this.w_IMP_MA = Nvl ( IMP_MA , 0 )
    this.w_IMP_MD = Nvl ( IMP_MD , 0 )
    this.w_IMP_AP = Nvl ( IMP_AP , 0 )
    this.w_IMP_AL = Nvl ( IMP_AL , 0 )
    * --- Eseguo la conversione della Riga
    this.w_ORD_MA = IIF ( this.oParentObject.w_CODVAL=this.w_DOCVAL,this.w_IMP_MA , VAL2MON(this.w_IMP_MA,this.w_CAOVAL,this.w_CAOCOMM,this.w_DATDOC,this.oParentObject.w_CODVAL) )
    this.w_ORD_MD = IIF ( this.oParentObject.w_CODVAL=this.w_DOCVAL,this.w_IMP_MD , VAL2MON(this.w_IMP_MD,this.w_CAOVAL,this.w_CAOCOMM,this.w_DATDOC,this.oParentObject.w_CODVAL) )
    this.w_ORD_AP = IIF ( this.oParentObject.w_CODVAL=this.w_DOCVAL,this.w_IMP_AP , VAL2MON(this.w_IMP_AP,this.w_CAOVAL,this.w_CAOCOMM,this.w_DATDOC,this.oParentObject.w_CODVAL) )
    this.w_ORD_AL = IIF ( this.oParentObject.w_CODVAL=this.w_DOCVAL,this.w_IMP_AL , VAL2MON(this.w_IMP_AL,this.w_CAOVAL,this.w_CAOCOMM,this.w_DATDOC,this.oParentObject.w_CODVAL) )
    Select "Curs_Val"
    Replace ORD_MA With this.w_ORD_MA
    Replace ORD_MD With this.w_ORD_MD
    Replace ORD_AP With this.w_ORD_AP
    Replace ORD_AL With this.w_ORD_AL
    EndScan
    if this.w_NUMREC
      * --- Metto tutto Assieme
      Select CodAtt, TipAtt, "N" As FlPrev, ;
      Ord_MA*0 As Costi_MA, Ord_MD*0 As Costi_MD, Ord_AP*0 As Costi_AP, Ord_AL*0 As Costi_AL, ;
      Ord_MA*0 As Forf_MA, Ord_MD*0 As Forf_MD, Ord_AP*0 As Forf_AP, Ord_AL*0 As Forf_AL, ;
      CONS_MA*0 As Cons_MA, CONS_MD*0 As Cons_MD, CONS_AP*0 As Cons_AP, CONS_AL*0 As Cons_AL, ;
      Sum(Ord_MA) As Ordi_MA, Sum(Ord_MD) As Ordi_MD, Sum(Ord_AP) As Ordi_AP, Sum(Ord_AL) As Ordi_AL ;
      From Curs_Val Group By CodAtt ;
      Union all ;
      Select Importi.* From Importi into Cursor Cur_Temp NoFilter
      * --- Raggruppo Ulteriormente
      Select CodAtt, TipAtt, FlPrev, ;
      Sum (Costi_MA) As Costi_MA, Sum(Costi_MD) As Costi_MD, Sum(Costi_AP) As Costi_AP, Sum(Costi_AL) As Costi_AL, ;
      Sum(Forf_MA) As Forf_MA, Sum(Forf_MD) As Forf_MD, Sum(Forf_AP) As Forf_AP, Sum(Forf_AL) As Forf_AL, ;
      Sum(Cons_MA) As Cons_MA, Sum(Cons_MD) As Cons_MD, Sum(Cons_AP) As Cons_AP, Sum(Cons_AL) As Cons_AL, ;
      Sum(Ordi_MA) As Ordi_MA, Sum(Ordi_MD) As Ordi_MD, Sum(Ordi_AP) As Ordi_AP, Sum(Ordi_AL) As Ordi_AL ;
      From Cur_Temp Group By CodAtt Into Cursor Importi NoFilter
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rilascio cursori
    if Used("Importi")
      Select ("Importi")
      Use
    endif
    if Used("FORFAIT")
      Select ("FORFAIT")
      Use
    endif
    if Used("F3")
      Select ("F3")
      Use
    endif
    if Used("Temp")
      Select ("Temp")
      Use
    endif
    if Used("Cur_Temp")
      Select ("Cur_Temp")
      Use
    endif
    if Used("Curs_Val")
      Select ("Curs_Val")
      Use
    endif
    if Used("Importi2")
      Select ("Importi2")
      Use
    endif
    if Used("Importi3")
      Select ("Importi3")
      Use
    endif
    if Used("Importi4")
      Select ("Importi4")
      Use
    endif
    if Used("Importi5")
      Select ("Importi5")
      Use
    endif
    if Used("Importi6")
      Select ("Importi6")
      Use
    endif
    if Used("Importi8")
      Select ("Importi8")
      Use
    endif
    if Used("Curs_Val7")
      Select ("Curs_Val7")
      Use
    endif
    if Used("Curs_Val11")
      Select ("Curs_Val11")
      Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
