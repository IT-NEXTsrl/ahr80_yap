* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_sac                                                        *
*              Avanzamento commessa                                            *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_237]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-20                                                      *
* Last revis.: 2012-10-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_sac",oParentObject))

* --- Class definition
define class tgspc_sac as StdForm
  Top    = 1
  Left   = 6

  * --- Standard Properties
  Width  = 790
  Height = 481+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-25"
  HelpContextID=124409961
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=64

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  CPAR_DEF_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gspc_sac"
  cComment = "Avanzamento commessa"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READPAR = space(10)
  w_DEFCOM = space(15)
  w_CODCOM = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_CODCLI = space(15)
  w_TPREVEN = space(1)
  w_PRINT = .F.
  w_WHATGR = space(16)
  w_TXT1 = space(10)
  w_TXT2 = space(10)
  w_TXT3 = space(10)
  w_M_PERCCONS_MA = space(18)
  w_M_PERCCONS_MD = space(18)
  w_M_PERCCONS_AP = space(18)
  w_M_PERCCONS_AL = space(18)
  w_M_PERCCONSORDI_MA = space(18)
  w_M_PERCCONSORDI_MD = space(18)
  w_M_PERCCONSORDI_AP = space(18)
  w_M_PERCCONSORDI_AL = space(18)
  w_M_PERCPREVENTIVO_MA = space(18)
  w_M_PERCPREVENTIVO_MD = space(18)
  w_M_PERCPREVENTIVO_AP = space(18)
  w_M_PERCPREVENTIVO_AL = space(18)
  w_M_PERCORDI_MA = space(18)
  w_M_PERCORDI_MD = space(18)
  w_M_PERCORDI_AP = space(18)
  w_M_PERCORDI_AL = space(18)
  w_M_FATTURATOATTUALE = space(18)
  w_M_MATURATONONFATTURATO = space(18)
  w_M_PERCFATTURATOATTUALE = space(18)
  w_M_PERCMATURATONONFATTURATO = space(18)
  w_M_AVANTECNNONDISP = space(10)
  w_M_CONSUNTIVOPAG4 = space(18)
  w_M_IMPEGNIFINANZIARIPAG4 = space(18)
  w_M_PERCCONSUNTIVOPAG4 = space(18)
  w_M_PERCIMPEGNIFINANZIARIPAG4 = space(18)
  w_COUTCURS = space(100)
  w_CODVAL = space(3)
  w_FLSTAMP1 = .F.
  w_TIPOCOST = space(1)
  w_CODCOM = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_CODCOM = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_CODCOM = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_TIPOGRA = space(1)
  w_CODCOM = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_TIPOGRA = space(1)
  w_CODCOM = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_TIPOGRA = space(1)
  w_CODCOM = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_TIPOGRA = space(1)
  w_Graph11 = .NULL.
  w_Lbl1Pag1 = .NULL.
  w_Lbl2Pag1 = .NULL.
  w_Lbl3Pag1 = .NULL.
  w_GRAPH41 = .NULL.
  w_Graph21 = .NULL.
  w_Graph22 = .NULL.
  w_GRAPH31 = .NULL.
  w_GRAPH42 = .NULL.
  w_GRAPH43 = .NULL.
  w_GRAPH44 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=7, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_sacPag1","gspc_sac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Avanzamento")
      .Pages(2).addobject("oPag","tgspc_sacPag2","gspc_sac",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Costi x tipologia")
      .Pages(3).addobject("oPag","tgspc_sacPag3","gspc_sac",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Ricavi / costi")
      .Pages(4).addobject("oPag","tgspc_sacPag4","gspc_sac",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Margine attuale")
      .Pages(5).addobject("oPag","tgspc_sacPag5","gspc_sac",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Margine residuo")
      .Pages(6).addobject("oPag","tgspc_sacPag6","gspc_sac",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Margine atteso")
      .Pages(7).addobject("oPag","tgspc_sacPag7","gspc_sac",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("% Margine")
      .Pages(7).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Graph11 = this.oPgFrm.Pages(1).oPag.Graph11
    this.w_Lbl1Pag1 = this.oPgFrm.Pages(1).oPag.Lbl1Pag1
    this.w_Lbl2Pag1 = this.oPgFrm.Pages(1).oPag.Lbl2Pag1
    this.w_Lbl3Pag1 = this.oPgFrm.Pages(1).oPag.Lbl3Pag1
    this.w_GRAPH41 = this.oPgFrm.Pages(4).oPag.GRAPH41
    this.w_Graph21 = this.oPgFrm.Pages(2).oPag.Graph21
    this.w_Graph22 = this.oPgFrm.Pages(2).oPag.Graph22
    this.w_GRAPH31 = this.oPgFrm.Pages(3).oPag.GRAPH31
    this.w_GRAPH42 = this.oPgFrm.Pages(5).oPag.GRAPH42
    this.w_GRAPH43 = this.oPgFrm.Pages(6).oPag.GRAPH43
    this.w_GRAPH44 = this.oPgFrm.Pages(7).oPag.GRAPH44
    DoDefault()
    proc Destroy()
      this.w_Graph11 = .NULL.
      this.w_Lbl1Pag1 = .NULL.
      this.w_Lbl2Pag1 = .NULL.
      this.w_Lbl3Pag1 = .NULL.
      this.w_GRAPH41 = .NULL.
      this.w_Graph21 = .NULL.
      this.w_Graph22 = .NULL.
      this.w_GRAPH31 = .NULL.
      this.w_GRAPH42 = .NULL.
      this.w_GRAPH43 = .NULL.
      this.w_GRAPH44 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CPAR_DEF'
    this.cWorkTables[4]='ATTIVITA'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR=space(10)
      .w_DEFCOM=space(15)
      .w_CODCOM=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODCLI=space(15)
      .w_TPREVEN=space(1)
      .w_PRINT=.f.
      .w_WHATGR=space(16)
      .w_TXT1=space(10)
      .w_TXT2=space(10)
      .w_TXT3=space(10)
      .w_M_PERCCONS_MA=space(18)
      .w_M_PERCCONS_MD=space(18)
      .w_M_PERCCONS_AP=space(18)
      .w_M_PERCCONS_AL=space(18)
      .w_M_PERCCONSORDI_MA=space(18)
      .w_M_PERCCONSORDI_MD=space(18)
      .w_M_PERCCONSORDI_AP=space(18)
      .w_M_PERCCONSORDI_AL=space(18)
      .w_M_PERCPREVENTIVO_MA=space(18)
      .w_M_PERCPREVENTIVO_MD=space(18)
      .w_M_PERCPREVENTIVO_AP=space(18)
      .w_M_PERCPREVENTIVO_AL=space(18)
      .w_M_PERCORDI_MA=space(18)
      .w_M_PERCORDI_MD=space(18)
      .w_M_PERCORDI_AP=space(18)
      .w_M_PERCORDI_AL=space(18)
      .w_M_FATTURATOATTUALE=space(18)
      .w_M_MATURATONONFATTURATO=space(18)
      .w_M_PERCFATTURATOATTUALE=space(18)
      .w_M_PERCMATURATONONFATTURATO=space(18)
      .w_M_AVANTECNNONDISP=space(10)
      .w_M_CONSUNTIVOPAG4=space(18)
      .w_M_IMPEGNIFINANZIARIPAG4=space(18)
      .w_M_PERCCONSUNTIVOPAG4=space(18)
      .w_M_PERCIMPEGNIFINANZIARIPAG4=space(18)
      .w_COUTCURS=space(100)
      .w_CODVAL=space(3)
      .w_FLSTAMP1=.f.
      .w_TIPOCOST=space(1)
      .w_CODCOM=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_CODCOM=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_CODCOM=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_TIPOGRA=space(1)
      .w_CODCOM=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_TIPOGRA=space(1)
      .w_CODCOM=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_TIPOGRA=space(1)
      .w_CODCOM=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_TIPOGRA=space(1)
        .w_READPAR = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODCOM = .w_DEFCOM
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODCOM))
          .link_1_3('Full')
        endif
        .w_DATAINI = i_DATSYS
          .DoRTCalc(5,5,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.Graph11.Calculate()
          .DoRTCalc(7,7,.f.)
        .w_TPREVEN = 'A'
        .w_PRINT = .f.
        .w_WHATGR = ''
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
      .oPgFrm.Page1.oPag.Lbl1Pag1.Calculate(.w_TXT1)
      .oPgFrm.Page1.oPag.Lbl2Pag1.Calculate(.w_TXT2)
      .oPgFrm.Page1.oPag.Lbl3Pag1.Calculate(.w_TXT3)
        .DoRTCalc(11,40,.f.)
        if not(empty(.w_CODVAL))
          .link_1_60('Full')
        endif
        .w_FLSTAMP1 = .f.
        .w_TIPOCOST = 'C'
      .oPgFrm.Page4.oPag.GRAPH41.Calculate()
          .DoRTCalc(43,51,.f.)
        .w_TIPOGRA = 'I'
      .oPgFrm.Page2.oPag.Graph21.Calculate()
      .oPgFrm.Page2.oPag.Graph22.Calculate()
          .DoRTCalc(53,55,.f.)
        .w_TIPOGRA = 'I'
          .DoRTCalc(57,59,.f.)
        .w_TIPOGRA = 'I'
          .DoRTCalc(61,63,.f.)
        .w_TIPOGRA = 'I'
      .oPgFrm.Page3.oPag.GRAPH31.Calculate()
      .oPgFrm.Page5.oPag.GRAPH42.Calculate()
      .oPgFrm.Page6.oPag.GRAPH43.Calculate()
      .oPgFrm.Page7.oPag.GRAPH44.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_8.enabled = this.oPgFrm.Page2.oPag.oBtn_2_8.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_5.enabled = this.oPgFrm.Page3.oPag.oBtn_3_5.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_7.enabled = this.oPgFrm.Page3.oPag.oBtn_3_7.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_8.enabled = this.oPgFrm.Page3.oPag.oBtn_3_8.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_1.enabled = this.oPgFrm.Page4.oPag.oBtn_4_1.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_9.enabled = this.oPgFrm.Page4.oPag.oBtn_4_9.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_1.enabled = this.oPgFrm.Page5.oPag.oBtn_5_1.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_2.enabled = this.oPgFrm.Page5.oPag.oBtn_5_2.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_9.enabled = this.oPgFrm.Page5.oPag.oBtn_5_9.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_11.enabled = this.oPgFrm.Page4.oPag.oBtn_4_11.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_1.enabled = this.oPgFrm.Page6.oPag.oBtn_6_1.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_2.enabled = this.oPgFrm.Page6.oPag.oBtn_6_2.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_9.enabled = this.oPgFrm.Page6.oPag.oBtn_6_9.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_1.enabled = this.oPgFrm.Page7.oPag.oBtn_7_1.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_2.enabled = this.oPgFrm.Page7.oPag.oBtn_7_2.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_9.enabled = this.oPgFrm.Page7.oPag.oBtn_7_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.Graph11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.Lbl1Pag1.Calculate(.w_TXT1)
        .oPgFrm.Page1.oPag.Lbl2Pag1.Calculate(.w_TXT2)
        .oPgFrm.Page1.oPag.Lbl3Pag1.Calculate(.w_TXT3)
        .DoRTCalc(2,39,.t.)
          .link_1_60('Full')
        .oPgFrm.Page4.oPag.GRAPH41.Calculate()
        .oPgFrm.Page2.oPag.Graph21.Calculate()
        .oPgFrm.Page2.oPag.Graph22.Calculate()
        .oPgFrm.Page3.oPag.GRAPH31.Calculate()
        .oPgFrm.Page5.oPag.GRAPH42.Calculate()
        .oPgFrm.Page6.oPag.GRAPH43.Calculate()
        .oPgFrm.Page7.oPag.GRAPH44.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(41,64,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Graph11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.Lbl1Pag1.Calculate(.w_TXT1)
        .oPgFrm.Page1.oPag.Lbl2Pag1.Calculate(.w_TXT2)
        .oPgFrm.Page1.oPag.Lbl3Pag1.Calculate(.w_TXT3)
        .oPgFrm.Page4.oPag.GRAPH41.Calculate()
        .oPgFrm.Page2.oPag.Graph21.Calculate()
        .oPgFrm.Page2.oPag.Graph22.Calculate()
        .oPgFrm.Page3.oPag.GRAPH31.Calculate()
        .oPgFrm.Page5.oPag.GRAPH42.Calculate()
        .oPgFrm.Page6.oPag.GRAPH43.Calculate()
        .oPgFrm.Page7.oPag.GRAPH44.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_5.enabled = this.oPgFrm.Page3.oPag.oBtn_3_5.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_8.enabled = this.oPgFrm.Page3.oPag.oBtn_3_8.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_1.enabled = this.oPgFrm.Page4.oPag.oBtn_4_1.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_9.enabled = this.oPgFrm.Page4.oPag.oBtn_4_9.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_1.enabled = this.oPgFrm.Page5.oPag.oBtn_5_1.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_9.enabled = this.oPgFrm.Page5.oPag.oBtn_5_9.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_1.enabled = this.oPgFrm.Page6.oPag.oBtn_6_1.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_9.enabled = this.oPgFrm.Page6.oPag.oBtn_6_9.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_1.enabled = this.oPgFrm.Page7.oPag.oBtn_7_1.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_9.enabled = this.oPgFrm.Page7.oPag.oBtn_7_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Graph11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.Lbl1Pag1.Event(cEvent)
      .oPgFrm.Page1.oPag.Lbl2Pag1.Event(cEvent)
      .oPgFrm.Page1.oPag.Lbl3Pag1.Event(cEvent)
      .oPgFrm.Page4.oPag.GRAPH41.Event(cEvent)
      .oPgFrm.Page2.oPag.Graph21.Event(cEvent)
      .oPgFrm.Page2.oPag.Graph22.Event(cEvent)
      .oPgFrm.Page3.oPag.GRAPH31.Event(cEvent)
      .oPgFrm.Page5.oPag.GRAPH42.Event(cEvent)
      .oPgFrm.Page6.oPag.GRAPH43.Event(cEvent)
      .oPgFrm.Page7.oPag.GRAPH44.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_CODCLI = NVL(_Link_.CNCODCON,space(15))
      this.w_CODVAL = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_CODCLI = space(15)
      this.w_CODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_3.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_3.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_4.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_4.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_9.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_9.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oTPREVEN_1_15.RadioValue()==this.w_TPREVEN)
      this.oPgFrm.Page1.oPag.oTPREVEN_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPOCOST_2_1.RadioValue()==this.w_TIPOCOST)
      this.oPgFrm.Page2.oPag.oTIPOCOST_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCOM_2_2.value==this.w_CODCOM)
      this.oPgFrm.Page2.oPag.oCODCOM_2_2.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDATAINI_2_3.value==this.w_DATAINI)
      this.oPgFrm.Page2.oPag.oDATAINI_2_3.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_5.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_5.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page3.oPag.oCODCOM_3_1.value==this.w_CODCOM)
      this.oPgFrm.Page3.oPag.oCODCOM_3_1.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page3.oPag.oDATAINI_3_2.value==this.w_DATAINI)
      this.oPgFrm.Page3.oPag.oDATAINI_3_2.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCOM_3_4.value==this.w_DESCOM)
      this.oPgFrm.Page3.oPag.oDESCOM_3_4.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oCODCOM_4_3.value==this.w_CODCOM)
      this.oPgFrm.Page4.oPag.oCODCOM_4_3.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oDATAINI_4_4.value==this.w_DATAINI)
      this.oPgFrm.Page4.oPag.oDATAINI_4_4.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCOM_4_6.value==this.w_DESCOM)
      this.oPgFrm.Page4.oPag.oDESCOM_4_6.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page5.oPag.oCODCOM_5_3.value==this.w_CODCOM)
      this.oPgFrm.Page5.oPag.oCODCOM_5_3.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page5.oPag.oDATAINI_5_4.value==this.w_DATAINI)
      this.oPgFrm.Page5.oPag.oDATAINI_5_4.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page5.oPag.oDESCOM_5_6.value==this.w_DESCOM)
      this.oPgFrm.Page5.oPag.oDESCOM_5_6.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page6.oPag.oCODCOM_6_3.value==this.w_CODCOM)
      this.oPgFrm.Page6.oPag.oCODCOM_6_3.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page6.oPag.oDATAINI_6_4.value==this.w_DATAINI)
      this.oPgFrm.Page6.oPag.oDATAINI_6_4.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page6.oPag.oDESCOM_6_6.value==this.w_DESCOM)
      this.oPgFrm.Page6.oPag.oDESCOM_6_6.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page7.oPag.oCODCOM_7_3.value==this.w_CODCOM)
      this.oPgFrm.Page7.oPag.oCODCOM_7_3.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page7.oPag.oDATAINI_7_4.value==this.w_DATAINI)
      this.oPgFrm.Page7.oPag.oDATAINI_7_4.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page7.oPag.oDESCOM_7_6.value==this.w_DESCOM)
      this.oPgFrm.Page7.oPag.oDESCOM_7_6.value=this.w_DESCOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINI_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATAINI)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgspc_sacPag1 as StdContainer
  Width  = 786
  height = 481
  stdWidth  = 786
  stdheight = 481
  resizeXpos=287
  resizeYpos=301
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM_1_3 as StdField with uid="AZJKYNVCBT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 87213274,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=79, Top=14, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDATAINI_1_4 as StdField with uid="SEICIEKUKM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data avanzamento",;
    HelpContextID = 191638838,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=493, Top=14


  add object oBtn_1_5 as StdButton with uid="BBAOQHTXNI",left=729, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il grafico";
    , HelpContextID = 35807485;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"VISUA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (.w_CODCOM))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="COGVCVGCCI",left=683, top=433, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare (visualizzare tutti i grafici)";
    , HelpContextID = 17379622;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"PRINT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSTAMP1)
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="CKEEXDXLZC",left=734, top=433, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Annulla";
    , HelpContextID = 117092538;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCOM_1_9 as StdField with uid="FKSUBIMSOW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 87154378,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=209, Top=14, InputMask=replicate('X',30)


  add object Graph11 as cp_FoxCharts with uid="GBIHLTEONO",left=8, top=65, width=769,height=352,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_Avanz_",cFileCfg="..\COMM\exe\query\gspc_sac_i",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "GraphPag1",;
    nPag=1;
    , HelpContextID = 53587686


  add object oTPREVEN_1_15 as StdCombo with uid="ODNZSENJIX",rtseq=8,rtrep=.f.,left=620,top=14,width=103,height=21;
    , ToolTipText = "Tipo di calcolo preventivo";
    , HelpContextID = 213902026;
    , cFormVar="w_TPREVEN",RowSource=""+"Aggiornato,"+"Iniziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTPREVEN_1_15.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'I',;
    ' ')))
  endfunc
  func oTPREVEN_1_15.GetRadio()
    this.Parent.oContained.w_TPREVEN = this.RadioValue()
    return .t.
  endfunc

  func oTPREVEN_1_15.SetRadio()
    this.Parent.oContained.w_TPREVEN=trim(this.Parent.oContained.w_TPREVEN)
    this.value = ;
      iif(this.Parent.oContained.w_TPREVEN=='A',1,;
      iif(this.Parent.oContained.w_TPREVEN=='I',2,;
      0))
  endfunc


  add object oObj_1_27 as cp_runprogram with uid="KREHEAHBUT",left=795, top=148, width=120,height=46,;
    caption='GSPC_BGR',;
   bGlobalFont=.t.,;
    prg="GSPC_BGR('EXIT_')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 13500344


  add object Lbl1Pag1 as cp_calclbl with uid="YHQRYRYVUQ",left=189, top=400, width=170,height=24,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="",FontName="Arial",FontSize=9,FontBold=True,Alignment=1,ForeColor=0,;
    nPag=1;
    , HelpContextID = 248368871


  add object Lbl2Pag1 as cp_calclbl with uid="JFYTOLZDEX",left=478, top=400, width=170,height=24,;
    caption='Lbl2Pag1',;
   bGlobalFont=.t.,;
    caption="",FontName="Arial",FontSize=9,FontBold=True,Alignment=1,ForeColor=0,;
    nPag=1;
    , HelpContextID = 248434407


  add object Lbl3Pag1 as cp_calclbl with uid="VUKYMYZGUT",left=37, top=400, width=170,height=24,;
    caption='Lbl3Pag1',;
   bGlobalFont=.t.,;
    caption="",FontName="Arial",FontSize=9,FontBold=True,Alignment=1,ForeColor=0,;
    nPag=1;
    , HelpContextID = 248499943

  add object oStr_1_8 as StdString with uid="QNJDIUUKFI",Visible=.t., Left=1, Top=14,;
    Alignment=1, Width=76, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="NMRERORRPB",Visible=.t., Left=440, Top=14,;
    Alignment=1, Width=51, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DHNDBOIGCN",Visible=.t., Left=575, Top=14,;
    Alignment=1, Width=42, Height=15,;
    Caption="Prev.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="VOUSSUSIFS",Visible=.t., Left=402, Top=427,;
    Alignment=0, Width=261, Height=15,;
    Caption="Totale ore lavorate / totale ore preventivate"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="GCHCLKDONE",Visible=.t., Left=402, Top=445,;
    Alignment=0, Width=276, Height=15,;
    Caption="( Consuntivi + impegni finanz. ) / totale preventivi"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="CYRZBUCRWR",Visible=.t., Left=73, Top=445,;
    Alignment=0, Width=238, Height=15,;
    Caption="Ricavi conseguiti / ricavi totali previsti"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="DPVKEBSTZV",Visible=.t., Left=73, Top=427,;
    Alignment=0, Width=229, Height=15,;
    Caption="% Completamento in base ai millesimi"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="UKVJQXRDCH",Visible=.t., Left=4, Top=427,;
    Alignment=1, Width=64, Height=15,;
    Caption="Tecnico:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="IWOHEGMMGV",Visible=.t., Left=4, Top=445,;
    Alignment=1, Width=64, Height=15,;
    Caption="Ricavi:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="XDIYVPCFYF",Visible=.t., Left=315, Top=427,;
    Alignment=1, Width=82, Height=15,;
    Caption="Manodopera:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="MAFFXNOEWZ",Visible=.t., Left=315, Top=445,;
    Alignment=1, Width=82, Height=15,;
    Caption="Costi:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_14 as StdBox with uid="BZQRFSTTUP",left=3, top=60, width=781,height=362
enddefine
define class tgspc_sacPag2 as StdContainer
  Width  = 786
  height = 481
  stdWidth  = 786
  stdheight = 481
  resizeXpos=356
  resizeYpos=305
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOCOST_2_1 as StdCombo with uid="FVUUTEKQBC",rtseq=42,rtrep=.f.,left=600,top=14,width=128,height=21;
    , ToolTipText = "Tipi di costo selezionati";
    , HelpContextID = 203028106;
    , cFormVar="w_TIPOCOST",RowSource=""+"Consuntivi,"+"Consuntivi+impegni", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPOCOST_2_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oTIPOCOST_2_1.GetRadio()
    this.Parent.oContained.w_TIPOCOST = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCOST_2_1.SetRadio()
    this.Parent.oContained.w_TIPOCOST=trim(this.Parent.oContained.w_TIPOCOST)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCOST=='C',1,;
      iif(this.Parent.oContained.w_TIPOCOST=='E',2,;
      0))
  endfunc

  add object oCODCOM_2_2 as StdField with uid="LTNLWAXUUQ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 87213274,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=80, Top=14, InputMask=replicate('X',15)

  add object oDATAINI_2_3 as StdField with uid="IJYREOSQWE",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data avanzamento",;
    HelpContextID = 191638838,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=469, Top=14

  add object oDESCOM_2_5 as StdField with uid="UPKTQJVIER",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 87154378,;
   bGlobalFont=.t.,;
    Height=21, Width=211, Left=210, Top=14, InputMask=replicate('X',30)


  add object oBtn_2_6 as StdButton with uid="KURSPWCIHX",left=683, top=433, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 17379622;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"PRINT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSTAMP1)
      endwith
    endif
  endfunc


  add object oBtn_2_8 as StdButton with uid="JJVAUBVZKV",left=734, top=433, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Annulla";
    , HelpContextID = 117092538;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_10 as StdButton with uid="TIVAHILJGQ",left=735, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare i grafici";
    , HelpContextID = 35807485;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"VISUA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (.w_CODCOM))
      endwith
    endif
  endfunc


  add object Graph21 as cp_FoxCharts with uid="FXIUDIWIJA",left=10, top=72, width=309,height=335,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_Costi_",cFileCfg="..\COMM\EXE\QUERY\GSPC_SAC_P",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "GraphPag2",;
    nPag=2;
    , HelpContextID = 53587686


  add object Graph22 as cp_FoxCharts with uid="HHGPMPJAEN",left=328, top=72, width=449,height=335,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_ImpCos_",cFileCfg="..\COMM\EXE\QUERY\GSPC1SAC_I",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "GraphPag2",;
    nPag=2;
    , HelpContextID = 53587686

  add object oStr_2_4 as StdString with uid="TNERGXRMAB",Visible=.t., Left=1, Top=14,;
    Alignment=1, Width=77, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="IEXVFTJNCP",Visible=.t., Left=425, Top=14,;
    Alignment=1, Width=42, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="SNTBPTXDUJ",Visible=.t., Left=552, Top=14,;
    Alignment=1, Width=45, Height=15,;
    Caption="Costi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="VASASHKBHA",Visible=.t., Left=37, Top=424,;
    Alignment=0, Width=279, Height=18,;
    Caption="Ripartizione costi consuntivi per tipologia"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="QCNAIXEOQB",Visible=.t., Left=372, Top=427,;
    Alignment=0, Width=299, Height=15,;
    Caption="Confronto consuntivi per tipologia / costi preventivi"  ;
  , bGlobalFont=.t.

  add object oBox_2_11 as StdBox with uid="IOHIIXISXP",left=324, top=68, width=461,height=343

  add object oBox_2_12 as StdBox with uid="NNKASYUWZZ",left=5, top=68, width=319,height=343
enddefine
define class tgspc_sacPag3 as StdContainer
  Width  = 786
  height = 481
  stdWidth  = 786
  stdheight = 481
  resizeXpos=270
  resizeYpos=328
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM_3_1 as StdField with uid="FTRQCXDGPT",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 87213274,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=91, Top=14, InputMask=replicate('X',15)

  add object oDATAINI_3_2 as StdField with uid="DUYHBHHZYQ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data avanzamento",;
    HelpContextID = 191638838,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=507, Top=14

  add object oDESCOM_3_4 as StdField with uid="SXKBKMFFQE",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 87154378,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=221, Top=14, InputMask=replicate('X',30)


  add object oBtn_3_5 as StdButton with uid="YFRCDYINCM",left=683, top=433, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 17379622;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_5.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"PRINT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSTAMP1)
      endwith
    endif
  endfunc


  add object oBtn_3_7 as StdButton with uid="ZSZJSCHNVL",left=734, top=433, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Annulla";
    , HelpContextID = 117092538;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_8 as StdButton with uid="TKTWPLLTLO",left=728, top=13, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per visualizzare i grafici";
    , HelpContextID = 35807485;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_8.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"VISUA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (.w_CODCOM))
      endwith
    endif
  endfunc


  add object GRAPH31 as cp_FoxCharts with uid="OJXQQVDOLN",left=7, top=70, width=765,height=332,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_CosRic_",cFileCfg="..\COMM\EXE\QUERY\GSPC2SAC_I",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "GraphPag3",;
    nPag=3;
    , HelpContextID = 53587686

  add object oStr_3_3 as StdString with uid="LVFKCEPBSE",Visible=.t., Left=4, Top=14,;
    Alignment=1, Width=85, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="HIKTLLUTKL",Visible=.t., Left=454, Top=14,;
    Alignment=1, Width=51, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="QLTMUQZSQH",Visible=.t., Left=81, Top=413,;
    Alignment=0, Width=219, Height=18,;
    Caption="Ricavi conseguiti; costi sostenuti"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="BUGRNLRHPA",Visible=.t., Left=425, Top=413,;
    Alignment=0, Width=157, Height=18,;
    Caption="Ricavi e costi futuri"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="XEDTHZUKGT",Visible=.t., Left=81, Top=436,;
    Alignment=0, Width=277, Height=18,;
    Caption="Ricavi attuali + futuri; costi attuali + futuri"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="RGDRYPLWOB",Visible=.t., Left=9, Top=412,;
    Alignment=1, Width=71, Height=18,;
    Caption="Attuali:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_14 as StdString with uid="EMVPHQQNIB",Visible=.t., Left=359, Top=412,;
    Alignment=1, Width=61, Height=18,;
    Caption="Residui:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_15 as StdString with uid="PXWWUARGZV",Visible=.t., Left=9, Top=435,;
    Alignment=1, Width=71, Height=18,;
    Caption="Attesi:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_9 as StdBox with uid="HLZKOUPVCN",left=3, top=65, width=777,height=343
enddefine
define class tgspc_sacPag4 as StdContainer
  Width  = 786
  height = 481
  stdWidth  = 786
  stdheight = 481
  resizeXpos=365
  resizeYpos=330
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_4_1 as StdButton with uid="YVGBBZPLFX",left=683, top=433, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 17379622;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_1.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"PRINT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSTAMP1)
      endwith
    endif
  endfunc


  add object GRAPH41 as cp_FoxCharts with uid="XVPISLVAFZ",left=8, top=68, width=764,height=333,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_MarAgg_",cFileCfg="..\COMM\EXE\QUERY\GSPC3SAC_I",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "GraphPag4",;
    nPag=4;
    , HelpContextID = 53587686

  add object oCODCOM_4_3 as StdField with uid="ZQCOYJKKPK",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 87213274,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=89, Top=14, InputMask=replicate('X',15)

  add object oDATAINI_4_4 as StdField with uid="GRPCCKAMOJ",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data avanzamento",;
    HelpContextID = 191638838,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=511, Top=14

  add object oDESCOM_4_6 as StdField with uid="GVNDCIDAZH",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 87154378,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=219, Top=14, InputMask=replicate('X',30)


  add object oBtn_4_9 as StdButton with uid="QPCXULEWFM",left=724, top=13, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per visualizzare i grafici";
    , HelpContextID = 35807485;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_9.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"VISUA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (.w_CODCOM))
      endwith
    endif
  endfunc


  add object oBtn_4_11 as StdButton with uid="HGWDDERBBF",left=734, top=433, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Annulla";
    , HelpContextID = 117092538;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_4_5 as StdString with uid="AEIRAZWQSU",Visible=.t., Left=1, Top=14,;
    Alignment=1, Width=86, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_4_7 as StdString with uid="EAOHQQCUCU",Visible=.t., Left=458, Top=14,;
    Alignment=1, Width=51, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="ZDVFYFWGKT",Visible=.t., Left=21, Top=440,;
    Alignment=0, Width=201, Height=18,;
    Caption="Margine = ( ricavi - costi ) / ricavi"  ;
  , bGlobalFont=.t.

  add object oBox_4_12 as StdBox with uid="QXELWYDNKO",left=2, top=63, width=777,height=343
enddefine
define class tgspc_sacPag5 as StdContainer
  Width  = 786
  height = 481
  stdWidth  = 786
  stdheight = 481
  resizeXpos=284
  resizeYpos=257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_5_1 as StdButton with uid="MFLXOVBFDC",left=683, top=433, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 17379622;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_1.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"PRINT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSTAMP1)
      endwith
    endif
  endfunc


  add object oBtn_5_2 as StdButton with uid="MNNLDMHTZZ",left=734, top=433, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=5;
    , ToolTipText = "Annulla";
    , HelpContextID = 117092538;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODCOM_5_3 as StdField with uid="DVZFUKSKDX",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 87213274,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=89, Top=14, InputMask=replicate('X',15)

  add object oDATAINI_5_4 as StdField with uid="YGOELJAMUV",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data avanzamento",;
    HelpContextID = 191638838,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=511, Top=14

  add object oDESCOM_5_6 as StdField with uid="NZAHEYOBHE",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 87154378,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=219, Top=14, InputMask=replicate('X',30)


  add object oBtn_5_9 as StdButton with uid="MOUGCJRMLI",left=724, top=13, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per visualizzare i grafici";
    , HelpContextID = 35807485;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_9.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"VISUA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (.w_CODCOM))
      endwith
    endif
  endfunc


  add object GRAPH42 as cp_FoxCharts with uid="FOZFIEKZSD",left=8, top=68, width=764,height=333,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_MarFin_",cFileCfg="..\COMM\EXE\QUERY\GSPC4SAC_I",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "GraphPag5",;
    nPag=5;
    , HelpContextID = 53587686

  add object oStr_5_5 as StdString with uid="FWVWEVMZUB",Visible=.t., Left=1, Top=14,;
    Alignment=1, Width=86, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_5_7 as StdString with uid="FEASRZSRSG",Visible=.t., Left=458, Top=14,;
    Alignment=1, Width=51, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_5_10 as StdString with uid="EVBPLMUITA",Visible=.t., Left=21, Top=440,;
    Alignment=0, Width=201, Height=18,;
    Caption="Margine = ( ricavi - costi ) / ricavi"  ;
  , bGlobalFont=.t.

  add object oBox_5_12 as StdBox with uid="MRVYYCTOVZ",left=2, top=63, width=777,height=343
enddefine
define class tgspc_sacPag6 as StdContainer
  Width  = 786
  height = 481
  stdWidth  = 786
  stdheight = 481
  resizeXpos=362
  resizeYpos=261
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_6_1 as StdButton with uid="JLSDRGCYOP",left=683, top=433, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 17379622;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_1.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"PRINT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSTAMP1)
      endwith
    endif
  endfunc


  add object oBtn_6_2 as StdButton with uid="IEIWRPLHDD",left=734, top=433, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=6;
    , ToolTipText = "Annulla";
    , HelpContextID = 117092538;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODCOM_6_3 as StdField with uid="SLSMEXPNJI",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 87213274,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=89, Top=14, InputMask=replicate('X',15)

  add object oDATAINI_6_4 as StdField with uid="TRNANSDJLZ",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data avanzamento",;
    HelpContextID = 191638838,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=511, Top=14

  add object oDESCOM_6_6 as StdField with uid="TBDBBUUDOX",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 87154378,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=219, Top=14, InputMask=replicate('X',30)


  add object oBtn_6_9 as StdButton with uid="DSPNQEADMI",left=724, top=13, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per visualizzare i grafici";
    , HelpContextID = 35807485;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_9.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"VISUA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (.w_CODCOM))
      endwith
    endif
  endfunc


  add object GRAPH43 as cp_FoxCharts with uid="POGMBBGZXT",left=8, top=68, width=764,height=333,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_MarAtt_",cFileCfg="..\COMM\EXE\QUERY\GSPC5SAC_I",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "GraphPag6",;
    nPag=6;
    , HelpContextID = 53587686

  add object oStr_6_5 as StdString with uid="ONSAZYULAF",Visible=.t., Left=1, Top=14,;
    Alignment=1, Width=86, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_6_7 as StdString with uid="EJFOIIWIXN",Visible=.t., Left=458, Top=14,;
    Alignment=1, Width=51, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_6_10 as StdString with uid="UIETXBSHEV",Visible=.t., Left=21, Top=440,;
    Alignment=0, Width=201, Height=18,;
    Caption="Margine = ( ricavi - costi ) / ricavi"  ;
  , bGlobalFont=.t.

  add object oBox_6_12 as StdBox with uid="QXDHSLSDKP",left=2, top=63, width=777,height=343
enddefine
define class tgspc_sacPag7 as StdContainer
  Width  = 786
  height = 481
  stdWidth  = 786
  stdheight = 481
  resizeXpos=330
  resizeYpos=241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_7_1 as StdButton with uid="KHXATEUDLQ",left=683, top=433, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=7;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 17379622;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_1.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"PRINT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_7_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSTAMP1)
      endwith
    endif
  endfunc


  add object oBtn_7_2 as StdButton with uid="WAYEJGETMN",left=734, top=433, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=7;
    , ToolTipText = "Annulla";
    , HelpContextID = 117092538;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODCOM_7_3 as StdField with uid="OXLBEBUHFO",rtseq=61,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 87213274,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=89, Top=14, InputMask=replicate('X',15)

  add object oDATAINI_7_4 as StdField with uid="RXMBWVIBTW",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data avanzamento",;
    HelpContextID = 191638838,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=511, Top=14

  add object oDESCOM_7_6 as StdField with uid="LDZOLTWUXM",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 87154378,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=219, Top=14, InputMask=replicate('X',30)


  add object oBtn_7_9 as StdButton with uid="MZGMJYOASK",left=724, top=13, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=7;
    , ToolTipText = "Premere per visualizzare i grafici";
    , HelpContextID = 35807485;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_9.Click()
      with this.Parent.oContained
        GSPC_BGR(this.Parent.oContained,"VISUA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_7_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (.w_CODCOM))
      endwith
    endif
  endfunc


  add object GRAPH44 as cp_FoxCharts with uid="UZFHRABOXA",left=8, top=68, width=764,height=333,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_MarPer_",cFileCfg="..\COMM\EXE\QUERY\GSPC6SAC_I",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "GraphPag7",;
    nPag=7;
    , HelpContextID = 53587686

  add object oStr_7_5 as StdString with uid="ZLTOBXDDOA",Visible=.t., Left=1, Top=14,;
    Alignment=1, Width=86, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_7_7 as StdString with uid="AATYBYWSVG",Visible=.t., Left=458, Top=14,;
    Alignment=1, Width=51, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_7_10 as StdString with uid="RSUMAPNYYW",Visible=.t., Left=21, Top=440,;
    Alignment=0, Width=201, Height=18,;
    Caption="Margine = ( ricavi - costi ) / ricavi"  ;
  , bGlobalFont=.t.

  add object oBox_7_12 as StdBox with uid="WQKXYEOCGG",left=2, top=63, width=777,height=343
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_sac','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
