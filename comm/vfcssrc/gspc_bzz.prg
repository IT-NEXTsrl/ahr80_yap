* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bzz                                                        *
*              Zoom on zoom attivita                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_31]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-27                                                      *
* Last revis.: 2006-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bzz",oParentObject)
return(i_retval)

define class tgspc_bzz as StdBatch
  * --- Local variables
  w_GESTIONE = .NULL.
  w_TIPSTR = space(1)
  w_TIPCOM = space(1)
  w_CODICE = space(20)
  w_TIPCOM = space(1)
  w_CODATT = space(20)
  w_PROG = .NULL.
  * --- WorkFile variables
  ATTIVITA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato negli Zoom on Zoom
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
      this.w_TIPCOM = Nvl(g_oMenu.oKey(2,3),"")
      this.w_CODATT = Nvl(g_oMenu.oKey(3,3),"")
      do case
        case this.w_TIPCOM="A"
          this.w_PROG = GSPC_AAT()
        otherwise
          * --- Read from ATTIVITA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ATTIPCOM"+;
              " from "+i_cTable+" ATTIVITA where ";
                  +"ATCODCOM = "+cp_ToStrODBC(this.w_CODICE);
                  +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPCOM);
                  +" and ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ATTIPCOM;
              from (i_cTable) where;
                  ATCODCOM = this.w_CODICE;
                  and ATTIPATT = this.w_TIPCOM;
                  and ATCODATT = this.w_CODATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_TIPATT = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if w_TIPATT = "S"
            this.w_PROG = GSPC_ASC()
          else
            this.w_PROG = GSPC_ACM()
          endif
      endcase
      * --- Controllo se ha passato il test di accesso
      if !(this.w_PROG.bSec1)
        i_retcode = 'stop'
        return
      endif
      * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
      *     presente nella variabile
      if g_oMenu.cBatchType$"AM"
        * --- Apri o Modifica
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_ATCODCOM = this.w_CODICE
        this.w_PROG.w_ATCODATT = this.w_CODATT
        do case
          case this.w_TIPCOM="A"
            this.w_PROG.w_ATTIPATT = this.w_TIPCOM
            this.w_PROG.w_ATTIPCOM = this.w_TIPCOM
          case this.w_TIPCOM="S"
            this.w_PROG.w_ATTIPCOM = this.w_TIPCOM
            this.w_PROG.w_ATTIPATT = "P"
          otherwise
            this.w_PROG.w_ATTIPATT = "P"
        endcase
        this.w_PROG.ecpSave()     
        if g_oMenu.cBatchType="M"
          * --- Modifica
          this.w_PROG.ecpEdit()     
        endif
      endif
      if g_oMenu.cBatchType="L"
        * --- Carica
        this.w_PROG.ecpLoad()     
      endif
    else
      if not IsNull(i_curform)
        * --- Cursore associato allo zoom
        cCurs = i_curform.z1.cCursor
        * --- Verifica se esiste il campo
        if CHKFLDCU("ATTIPATT",cCurs)
          this.w_TIPSTR = NVL( &cCurs..ATTIPATT , " ")
          if this.w_TIPSTR="A" or empty(this.w_TIPSTR)
            * --- Lancio la gestione Attivit� 
            this.w_GESTIONE = GSPC_AAT()
          else
            * --- Non attiivit�
            * --- Leggo il campo ATTIPCOM e decido quale gestione lanciare
            if CHKFLDCU("ATTIPCOM",cCurs)
              this.w_TIPCOM = &cCurs..ATTIPCOM
              if this.w_TIPCOM="S"
                * --- Sotto Conto
                this.w_GESTIONE = GSPC_ASC()
              else
                * --- Capo Progetto o Conto / Macro attivit�
                this.w_GESTIONE = GSPC_ACM()
              endif
            endif
          endif
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ATTIVITA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
