* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bgc                                                        *
*              Generazione commesse                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_21]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-05                                                      *
* Last revis.: 2008-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bgc",oParentObject)
return(i_retval)

define class tgspc_bgc as StdBatch
  * --- Local variables
  w_MESS = space(100)
  w_PROGR = space(1)
  w_VINCOL = space(3)
  w_CNDESCAN = space(30)
  * --- WorkFile variables
  ATTIVITA_idx=0
  CPAR_DEF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Automatica commesse alla fine inserimento (da GSAR_ACN)
    if g_COMM="S"
      * --- La descrizione dell'attivit� � pi� piccola di quella della commessa
      this.w_CNDESCAN = LEFT(this.oParentObject.w_CNDESCAN + SPACE(30) , 30)
      * --- Generazione Attivita'
      this.w_PROGR = " "
      * --- Read from CPAR_DEF
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PDPROGR"+;
          " from "+i_cTable+" CPAR_DEF where ";
              +"PDCHIAVE = "+cp_ToStrODBC("TAM");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PDPROGR;
          from (i_cTable) where;
              PDCHIAVE = "TAM";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PROGR = NVL(cp_ToDate(_read_.PDPROGR),cp_NullValue(_read_.PDPROGR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_VINCOL = IIF(this.w_PROGR="A", "PPP", "PTP")
      * --- Try
      local bErr_033CA068
      bErr_033CA068=bTrsErr
      this.Try_033CA068()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_033CA068
      * --- End
    endif
  endproc
  proc Try_033CA068()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATTIVITA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ATCODCOM"+",ATCODATT"+",ATTIPATT"+",ATTIPCOM"+",ATDESCRI"+",AT_PRJVW"+",ATVINCOL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CNCODCAN),'ATTIVITA','ATCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CNCODCAN),'ATTIVITA','ATCODATT');
      +","+cp_NullLink(cp_ToStrODBC("P"),'ATTIVITA','ATTIPATT');
      +","+cp_NullLink(cp_ToStrODBC("P"),'ATTIVITA','ATTIPCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CNDESCAN),'ATTIVITA','ATDESCRI');
      +","+cp_NullLink(cp_ToStrODBC("S"),'ATTIVITA','AT_PRJVW');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VINCOL),'ATTIVITA','ATVINCOL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ATCODCOM',this.oParentObject.w_CNCODCAN,'ATCODATT',this.oParentObject.w_CNCODCAN,'ATTIPATT',"P",'ATTIPCOM',"P",'ATDESCRI',this.w_CNDESCAN,'AT_PRJVW',"S",'ATVINCOL',this.w_VINCOL)
      insert into (i_cTable) (ATCODCOM,ATCODATT,ATTIPATT,ATTIPCOM,ATDESCRI,AT_PRJVW,ATVINCOL &i_ccchkf. );
         values (;
           this.oParentObject.w_CNCODCAN;
           ,this.oParentObject.w_CNCODCAN;
           ,"P";
           ,"P";
           ,this.w_CNDESCAN;
           ,"S";
           ,this.w_VINCOL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore Creazione Capo Progetto, Inserire Manualmente!'
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ATTIVITA'
    this.cWorkTables[2]='CPAR_DEF'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
