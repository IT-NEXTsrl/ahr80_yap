* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bac                                                        *
*              Avanzamento commessa                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-03                                                      *
* Last revis.: 2013-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bac",oParentObject,m.pTipo)
return(i_retval)

define class tgspc_bac as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_MESS = space(250)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_TIPSTR = space(1)
  w_TIPCOM = space(1)
  w_RICERCA = space(100)
  w_TIPPRE = space(1)
  w_TIPPRE1 = space(1)
  w_PREVEN = 0
  w_LIVELLO = space(100)
  w_PADRE = space(15)
  w_TOTPREV = 0
  w_RIFKEY = space(100)
  w_LUNGH = 0
  w_PERCOMP = 0
  w_TOTCONS = 0
  w_TOTCOST = 0
  w_POSREC = 0
  w_PERIMP = 0
  w_TOTORDI = 0
  w_PREVAL = 0
  w_NUMPOINT = 0
  w_RICAGG = 0
  w_COSAGG = 0
  w_PREVMD = 0
  w_TOTALE = 0
  w_RICAFIN = 0
  w_COSAFIN = 0
  w_PREVAP = 0
  w_PREVMA = 0
  w_RICATT = 0
  w_COSATT = 0
  w_STOP = .f.
  w_Graph41Vis = .f.
  w_Graph42Vis = .f.
  w_Graph43Vis = .f.
  w_PunPad = .NULL.
  w_UMDFLT = space(3)
  w_TOTPREV = 0
  w_TOTCONS = 0
  w_CODART = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_PREV = 0
  w_CONS = 0
  * --- WorkFile variables
  CPAR_DEF_idx=0
  ART_ICOL_idx=0
  ATTIVITA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Avanzamento Commessa (da GSPC_SAC)
    do case
      case this.pTipo="PAG1"
        * --- Elimino i cursori dei grafici
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not "PAG1" $ this.oParentObject.w_WHATGR
          this.oParentObject.w_WHATGR = this.oParentObject.w_WHATGR+"PAG1"
          this.oParentObject.w_PRINT = Len (this.oParentObject.w_WHATGR)=12
        endif
        * --- Riempio il grafico
        This.oParentobject.Notifyevent("GraphPag1")
        * --- Rimuovo il cursore
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo="PAG2"
        * --- % Costi per Tipologia e Importi Costi per Tipologia
        * --- Elimino i cursori dei grafici
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not "PAG2" $ this.oParentObject.w_WHATGR
          this.oParentObject.w_WHATGR = this.oParentObject.w_WHATGR+"PAG2"
          this.oParentObject.w_PRINT = Len (this.oParentObject.w_WHATGR)=12
        endif
        * --- Riempio il grafico
        This.oParentobject.Notifyevent("GraphPag2")
        * --- Rimuovo il cursore
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo="Done"
        * --- Elimino i cursori dei grafici
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Rimuovo il cursore
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo="PRINT"
        this.Pag9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo="PAG3"
        this.Pag10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_TIPOGRA="I"
          * --- Se impostata la visualizzazione importi allora modifico gli oggetti
          * --- In pratica punta ad un altro grafico con la sola propriet� della visualizzazione etichetta cambiata
          * --- In attesa di trovare il nome della propriet� che governa questa cosa
          this.oParentObject.w_GRAPH41.Nomegraph = "Secondo"
          this.oParentObject.w_GRAPH42.Nomegraph = "Secondo"
          this.oParentObject.w_GRAPH43.Nomegraph = "Secondo"
          this.oParentObject.w_GRAPH41.Titolo = "Importi Margine Attuale"
          this.oParentObject.w_GRAPH42.Titolo = "Importi Margine Residuo"
          this.oParentObject.w_GRAPH43.Titolo = "Importi Margine Totale"
        else
          this.oParentObject.w_GRAPH41.Nomegraph = "Margine"
          this.oParentObject.w_GRAPH42.Nomegraph = "Margine"
          this.oParentObject.w_GRAPH43.Nomegraph = "Margine"
          this.oParentObject.w_GRAPH41.Titolo = "% Margine Attuale"
          this.oParentObject.w_GRAPH42.Titolo = "% Margine Residuo"
          this.oParentObject.w_GRAPH43.Titolo = "% Margine Totale"
        endif
        if Not "PAG3" $ this.oParentObject.w_WHATGR
          this.oParentObject.w_WHATGR = this.oParentObject.w_WHATGR+"PAG3"
          this.oParentObject.w_PRINT = Len (this.oParentObject.w_WHATGR)=12
        endif
        * --- Riempio i Grafici (3 eventi, perch� uno solo dava problemi per grafici sulla solita pagina)
        This.oParentobject.Notifyevent("GraphPag3")
        This.oParentobject.Notifyevent("GraphPag4")
        This.oParentobject.Notifyevent("GraphPag5")
        This.oParentobject.Notifyevent("GraphPag6")
        * --- Elimino i cursori dei grafici
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo percentuale tecnico
    * --- La procedura Scorre riga per riga e per ognuna
    * --- somma la percentuale sul padre
    * --- Il cursore deve essere ordinato in modo da consultare prima i figli dei padri (lvlkey DESC)
    Select _Tecn_
    Scan
    this.w_LIVELLO = RTrim(_Tecn_.LvlKey)
    this.w_LUNGH = Len(this.w_LIVELLO)
    this.w_POSREC = RecNo()
    this.w_NUMPOINT = Occurs(".",this.w_LIVELLO)
    Wait Window "Calcolo Avanzamento Tecnico...:"+this.w_PADRE NoWait
    * --- Per ogni record aggiungo nel padre i vari importi
    if this.w_NUMPOINT>0
      * --- Questa variabile contiene il LEVELKEY del padre
      this.w_RICERCA = Padr( Left ( this.w_LIVELLO , AT ( "." , this.w_LIVELLO , this.w_NUMPOINT ) -1 ),200)
      * --- w_PERCOMP contiene la percentuale di completamento dell'attivit� o la fraione del conto in millesimi
      if  _Tecn_.ATTIPCOM="A"
        this.w_PERCOMP = Nvl( _Tecn_.ATPERCOM , 0 )*Nvl ( _Tecn_.STMILLES , 0 )/100
      else
        this.w_PERCOMP = Nvl ( _Tecn_.STMILLES , 0 )*Nvl ( _Tecn_.AvanTec , 0 )/1000
      endif
      Select _Tecn_
      Go Top
      Locate For LvlKey = this.w_RICERCA
      if Found()
        this.w_PADRE = _Tecn_.AtCodAtt
        Replace AvanTec With Nvl(AvanTec,0)+this.w_PERCOMP
        Go this.w_POSREC
      endif
    endif
    Select _Tecn_
    EndScan
    Select _Tecn_
    Go Bottom
    * --- Leggo il totale sul padre
    this.w_PERCOMP = Nvl ( _Tecn_.AvanTec , 0 )/10
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Costi
    * --- Calcolo il totale delle attivit� (della Commessa)
    * --- Calcolo il preventivo (Iniziale) per farlo utilizzo la solita Query del calcolo preventivo leggermente modificata (raggruppo)
    * --- Poi metto a 100 il preventivo e proporziono le altre voci (Impegnato e ordinato)
    * --- Preventivo Iniziale (se messo a 'N' preventivo iniziale )
    this.w_TIPPRE = IIF(this.oParentObject.w_TPREVEN="I","N"," ")
    this.w_TIPPRE1 = IIF(this.oParentObject.w_TPREVEN="A","S"," ")
    Vq_Exec ("..\Comm\Exe\Query\Gspc_Bac.Vqr",This,"Importi")
    * --- Raggruppo i vari Costi relativi alle Attivit� e ai sottoconti
    Select ; 
 Sum(Prev_Ma) As Preventivo_Ma , Sum(Prev_Md) As Preventivo_Md , ; 
 Sum(Prev_Ap) As Preventivo_Ap , Sum(Prev_Al) As Preventivo_Al, ; 
 Sum(Cons_Ma) As Cons_Ma , Sum(Cons_Md) As Cons_Md , ; 
 Sum(Cons_Ap) As Cons_Ap , Sum(Cons_Al) As Cons_Al, ; 
 Sum(Ordi_Ma) As Ordi_Ma , Sum(Ordi_Md) As Ordi_Md , ; 
 Sum(Ordi_AP) As Ordi_AP , Sum(Ordi_AL) As Ordi_AL ; 
 from Importi Into Cursor Importi NoFilter 
 Sum Preventivo_MA+Preventivo_MD+Preventivo_AP+Preventivo_AL TO this.w_PREVEN 
 Sum Cons_MA+Cons_MD+Cons_AP+Cons_AL TO this.w_TOTCOST 
 Sum Ordi_MA+Ordi_MD+Ordi_AP+Ordi_AL TO this.w_TOTORDI
    * --- Calcolo i totali del preventivo divisi per Tipo
    Sum Preventivo_MA TO this.w_PREVMA
    Sum Preventivo_MD TO this.w_PREVMD
    Sum Preventivo_AP TO this.w_PREVAP
    Sum Preventivo_AL TO this.w_PREVAL
    * --- Riporto tutto in percentuale
    this.w_TOTCONS = ( this.w_TOTCOST / this.w_PREVEN )*100
    this.w_TOTORDI = (this.w_TOTORDI / this.w_PREVEN)*100
    this.w_TOTPREV = 100
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuovo i cursori
    if Used("_Tecn_")
      Use in _Tecn_
    endif
    if Used("Query")
      Use in Query
    endif
    if Used("Importi")
      Use in Importi
    endif
    if Used("_ore_")
      Use in _ore_
    endif
    if Used(this.w_COUTCURS)
      Use in ( this.w_COUTCURS )
    endif
    if Used("_Ricavi_")
      Use in _Ricavi_
    endif
    if Used("_Costi1_")
      Use in _Costi1_
    endif
    if Used("__PAG1__")
      Use in __PAG1__
    endif
    if Used("__PAG2__")
      Use in __PAG2__
    endif
    if Used("__PAG3__")
      Use in __PAG3__
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina cursori grafici
    if Used("_Avanz_")
      Use in _Avanz_
    endif
    if Used("_Costi_")
      Use in _Costi_
    endif
    if Used("_ImpCos_")
      Use in _ImpCos_
    endif
    if Used("_CosRic_")
      Use in _CosRic_
    endif
    if Used("_MarAgg_")
      Use in _MarAgg_
    endif
    if Used("_MarFin_")
      Use in _MarFin_
    endif
    if Used("_MarAtt_")
      Use in _MarAtt_
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione parametri da passare alla CP_EXPDB
    this.w_COUTCURS = Sys(2015)
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = "STMILLES"
    * --- Altri campi
    * --- Read from CPAR_DEF
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDUMCOMM"+;
        " from "+i_cTable+" CPAR_DEF where ";
            +"PDCHIAVE = "+cp_ToStrODBC("TAM");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDUMCOMM;
        from (i_cTable) where;
            PDCHIAVE = "TAM";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UMDFLT = NVL(cp_ToDate(_read_.PDUMCOMM),cp_NullValue(_read_.PDUMCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TIPSTR = "P"
    this.w_TIPCOM = "P"
    * --- Verifica dell'esistenza (del capoprogetto) della struttura. Se la struttura non esiste l'export non viene effettuato.
    * --- La struttura pu� non esistere per i seguenti motivi:
    * --- 1. la struttura gestionale non � stata creata dopo essere passati da una a tre strutture nella gestione del progetto
    * --- 2. la commessa � stata creata dall'analitica e non sono stati creati i capiprogetto
    this.w_STOP = .T.
    * --- Select from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
          +" where ATCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM)+" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR)+" and ATTIPCOM="+cp_ToStrODBC(this.w_TIPCOM)+"";
           ,"_Curs_ATTIVITA")
    else
      select * from (i_cTable);
       where ATCODCOM=this.oParentObject.w_CODCOM and ATTIPATT=this.w_TIPSTR and ATTIPCOM=this.w_TIPCOM;
        into cursor _Curs_ATTIVITA
    endif
    if used('_Curs_ATTIVITA')
      select _Curs_ATTIVITA
      locate for 1=1
      do while not(eof())
      * --- Se esiste il capoprogetto permetto al batch di continuare
      this.w_STOP = .F.
        select _Curs_ATTIVITA
        continue
      enddo
      use
    endif
    if this.w_STOP
      * --- Nel caso in cui la struttura sia vuota l'export non viene eseguito
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_MESS = "La struttura non esiste."+chr(13)
      this.w_MESS = this.w_MESS+"Impossibile visualizzare o stampare gli avanzamenti di commessa."
      cp_ErrorMsg("La struttura non esiste. %0Impossibile visualizzare o stampare gli avanzamenti di commessa",,"ATTENZIONE")
      i_retcode = 'stop'
      return
    endif
    * --- Se la struttura esiste proseguo con la visualizzazione degli avanzamenti di commessa
    * --- Costruisco con una query contentente la base per l'esplosione
    VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
    SELECT "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if USED("__tmp__")
        SELECT "__tmp__" 
 USE
      endif
    else
      cp_ErrorMsg("Non esistono dati da Visualizzare",,"ATTENZIONE")
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select *, 1000*0 As AvanTec from ( this.w_COUTCURS ) Into Cursor _Tecn_ Order By LvlKey Desc NoFilter
    Cur =WrCursor ("_Tecn_")
    * --- Calcolo la percentuale di avanzamento del Tecnico
    * --- Eseguo il solito algoritmo (con qualche variante) per calcolare il totale applicato alle percentuali
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Create Cursor _Avanz_ (tipo C(15) , CONSUNTIVO C(6), IMPEGNATO C(6) )
    Cur =WrCursor ("_Avanz_")
    * --- Tipo vale Tecnico , Ricavi, Manodop., Costi
    * --- Tutto nel caso di tecnico vale 100
    * --- Costruisco il record per i valori del Tecnico
    Append Blank
    Replace Tipo With "Tecnico  ", CONSUNTIVO With Str(this.w_PERCOMP,6,2), IMPEGNATO With " "
    * --- Calcolo i Ricavi
    * --- Leggo gli ordini a cliente aventi sulla riga la commessa (e non l'attivit�)
    * --- Il 100 sar� dato dal totale dei documenti di questo tipo
    * --- Mentre la percentuale sar� data dal totale con data eff. evasione inferiore o uguale alla data nella maschera
    Vq_Exec ("..\Comm\Exe\Query\Gspc3Bac.Vqr",This,"_Ricavi2_") 
 =wrcursor("_Ricavi2_") 
 scan for mvcodval<>this.oParentObject.w_CODVAL 
 replace PerComm with VAL2CAM(PerComm, mvcodval, this.oParentObject.w_CODVAL, mvcaoval, cp_todate(mvdatdoc)) 
 replace TotComm with VAL2CAM(TotComm, mvcodval, this.oParentObject.w_CODVAL, mvcaoval, cp_todate(mvdatdoc)) 
 endscan 
 Select Sum(TotComm) As TotComm, Sum(PerComm) As PerComm ; 
 from _Ricavi2_ Into Cursor _Ricavi_ NoFilter 
 use in _Ricavi2_
    this.w_PERCOMP = 100 * ( Nvl(_Ricavi_.PerComm,0) / Nvl(_Ricavi_.TotComm,0) )
    Select _Avanz_
    Append Blank
    Replace Tipo With "Ricavi  "
    Replace CONSUNTIVO With Str(this.w_PERCOMP,6,2)
    Replace IMPEGNATO With " "
    * --- Calcolo la Manodopera
    * --- Recupero i dati esclusivamente dai movimenti preventivi e consuntivi di commessa con Costo di tipo manodopera
    this.w_TOTPREV = 0
    * --- Contatore per totale preventivato (tempi!!!)
    this.w_TOTCONS = 0
    * --- Contatore per totale consuntivato (tempi!!!)
    Vq_Exec ("..\Comm\Exe\Query\Gspc2Bac.Vqr",This,"_Ore_")
    SELECT "_Ore_"
    GO TOP
    SCAN
    if MAUNIMIS=this.w_UMDFLT
      * --- L'UM � gi� quella giusta: incremento direttamente i contatori
      this.w_TOTPREV = this.w_TOTPREV+PREV
      this.w_TOTCONS = this.w_TOTCONS+CONS
    else
      * --- Faccio le verifiche sull'articolo (una delle due unit� di misura deve essere quella di default
      * --- a seconda dei casi si usa il corretto fattore di conversione
      this.w_CODART = MACODART
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
        this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
        this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
        use
      else
        * --- Error: sql sentence error.
        i_Error = 'impossibile leggere da ART_ICOL'
        return
      endif
      select (i_nOldArea)
      * --- leggo le UM e i dati per la conversione sull'articolo
      * --- N.B.: UM movimento <> UM default
      if nvl(this.w_UNMIS1,"")=MAUNIMIS and nvl(this.w_UNMIS2,"")=this.w_UMDFLT
        * --- l'UM del movimento � la prima UM (UNIMIS1)
        this.w_PREV = IIF(this.w_OPERAT="*",PREV*this.w_MOLTIP,IIF(this.w_OPERAT="/",PREV/this.w_MOLTIP,0))
        this.w_CONS = IIF(this.w_OPERAT="*",CONS*this.w_MOLTIP,IIF(this.w_OPERAT="/",CONS/this.w_MOLTIP,0))
      else
        if nvl(this.w_UNMIS2,"")=MAUNIMIS and nvl(this.w_UNMIS1,"")=this.w_UMDFLT
          * --- l'UM del movimento � la seconda UM (UNIMIS2)
          this.w_PREV = IIF(this.w_OPERAT="*",PREV/this.w_MOLTIP,IIF(this.w_OPERAT="/",PREV*this.w_MOLTIP,0))
          this.w_CONS = IIF(this.w_OPERAT="*",CONS/this.w_MOLTIP,IIF(this.w_OPERAT="/",CONS*this.w_MOLTIP,0))
        else
          this.w_PREV = 0
          this.w_CONS = 0
        endif
      endif
      this.w_TOTPREV = this.w_TOTPREV+this.w_PREV
      this.w_TOTCONS = this.w_TOTCONS+this.w_CONS
    endif
    ENDSCAN
    * --- SU _Ore_
    if this.w_TOTPREV <>0 AND this.w_TOTCONS <>0
      this.w_PERCOMP = (this.w_TOTCONS / this.w_TOTPREV)*100
    else
      this.w_PERCOMP = 0
    endif
    Select _Avanz_
    Append Blank
    Replace Tipo With "Manodop. "
    Replace CONSUNTIVO With Str(this.w_PERCOMP,6,2)
    Replace IMPEGNATO With " "
    * --- Calcolo i Costi
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select _Avanz_
    Append Blank
    Replace Tipo With "Costi  "
    Replace CONSUNTIVO With Str(this.w_TOTCONS,6,2)
    Replace IMPEGNATO With Str(this.w_TOTORDI,6,2)
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Grafici Seconda Pagina
    * --- _impCos_ Grafico Importi mentre _Costi_ grafico torta
    Vq_Exec ("..\Comm\Exe\Query\Gspc5Bac.Vqr",This,"_Costi1_")
    Cur =WrCursor ("_Costi1_")
    * --- Importi Costi per tipologia (o Consuntivi e Impegni oppure solo consuntivi)
    if this.oParentObject.w_TIPOCOST="E"
      Select Tipo,Importo+Impegno As Costi,Importo as Preventivo From _Costi1_ Into Cursor _ImpCos_ NoFilter
      Select Tipo,Importo+Impegno As Importo,Importo as Preventivo From _Costi1_ Into Cursor _Costi_ NoFilter
    else
      Select Tipo,Importo As Costi,Importo as Preventivo From _Costi1_ Into Cursor _ImpCos_ NoFilter
      Select Tipo,Importo As Importo From _Costi1_ Into Cursor _Costi_ NoFilter
    endif
    Cur =WrCursor ("_Costi_")
    Cur =WrCursor ("_ImpCos_")
    Select _ImpCos_
    * --- Calcolo Preventivi
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Update _ImpCos_ Set Preventivo=this.w_PREVMA Where TIPO="Materiali"
    Update _ImpCos_ Set Preventivo=this.w_PREVMD Where TIPO="Manodopera"
    Update _ImpCos_ Set Preventivo=this.w_PREVAP Where TIPO="Appalti   "
    Update _ImpCos_ Set Preventivo=this.w_PREVAL Where TIPO="Altro     "
    * --- Calcolo la percentuale
    Select _Costi_
    Sum Importo To this.w_TOTALE
    * --- Modifico il cursore scrivendo le percentuali
    Update _Costi_ Set Importo=(Importo / this.w_TOTALE ) * 100
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa grafici
    * --- La procedura stampa tutti i grafici pi� i valori come tabelle
    * --- Elimino i cursori dei grafici
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Costruisco un cursore contenente un campo GENERAL con i grafici e  altri campi per gli importi
    * --- Il campo general � ripetuto per tutte le righe di dati che lo riguardano
    * --- Il cursore � composto da 5 campi uno di tipo Carattere e 4 di tipo Importo
    * --- Grafico pagina 1
    Select 1 as Tipgraph, Val( _Avanz_.Consuntivo ) As Importo1, ;
    Val( _Avanz_.Impegnato ) As Importo2, Val( _Avanz_.Consuntivo )*0 As Importo3, Val( _Avanz_.Consuntivo )*0 As Importo4,;
    Left(_Avanz_.Tipo+Space(15),15) as Tipo , olegraph As Graph from _avanz_, (this.oParentObject.w_GRAPH11.tname) into cursor __PAG1__ NoFilter
    this.Pag8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Grafici pagina 2 (Prima quello di sinistra e poi quello di destra)
    Select 2 as Tipgraph, _Costi_.Importo As Importo1, ;
     _Costi_.Importo *0 As Importo2, _Costi_.Importo *0 As Importo3, _Costi_.Importo *0 As Importo4,;
    Left(_Costi_.Tipo+Space(15),15) as Tipo , olegraph As Graph from _Costi_, (this.oParentObject.w_GRAPH21.tname) ;
    Union ;
    Select 3 as Tipgraph, _ImpCos_.Costi As Importo1, ;
     _ImpCos_.Preventivo As Importo2, _ImpCos_.Preventivo*0 As Importo3, _ImpCos_.Preventivo *0 As Importo4,;
    Left( _ImpCos_.Tipo + Space(15) ,15 ) as Tipo , olegraph As Graph from _ImpCos_, (this.oParentObject.w_GRAPH22.tname) ;
    into cursor __PAG2__ NoFilter
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Grafici pagine 3,4,5,6
    Select 4 as Tipgraph, _CosRic_.Ricavi As Importo1, ;
     _CosRic_.Costi As Importo2, _CosRic_.Costi*0 As Importo3, _CosRic_.Costi*0 As Importo4,;
    Left( _CosRic_.Tipo+Space(15),15) as Tipo , olegraph As Graph from _CosRic_, (this.oParentObject.w_GRAPH31.tname) ;
    Union ;
    Select 5 as Tipgraph, _MarAgg_.PerCent As Importo1, ;
    _MarAgg_.PerCent*0 As Importo2, _MarAgg_.PerCent*0 As Importo3, _MarAgg_.PerCent*0 As Importo4,;
    Left (_MarAgg_.Tipo +Space(15) , 15 ) as Tipo , olegraph As Graph from _MarFin_, (this.oParentObject.w_GRAPH41.tname) ;
    Union ;
    Select 6 as Tipgraph, _MarFin_.PerCent As Importo1, ;
    _MarFin_.PerCent*0 As Importo2, _MarFin_.PerCent*0 As Importo3, _MarAtt_.PerCent*0 As Importo4,;
    Left (_MarFin_.Tipo+Space(15) , 15 ) as Tipo , olegraph As Graph from _MarFin_, (this.oParentObject.w_GRAPH42.tname) ;
    Union ;
    Select 7 as Tipgraph, _MarAtt_.PerCent As Importo1, ;
    _MarAtt_.PerCent*0 As Importo2, _MarAtt_.PerCent*0 As Importo3, _MarAtt_.PerCent*0 As Importo4,;
    Left (_MarAtt_.Tipo+ Space(15) , 15 ) as Tipo , olegraph As Graph from _MarAtt_, (this.oParentObject.w_GRAPH43.tname) ;
    into cursor __PAG3__ NoFilter
    * --- Metto tutto Assieme
    Select * From __PAG1__ ;
    Union ;
    Select * From __PAG2__;
    Union ;
    Select * From __PAG3__;
    into cursor __TMP__ Order By 1 NoFilter
    L_CodCom=this.oParentObject.w_CodCom
    CP_CHPRN("..\Comm\Exe\Query\Gspc_Bac.Frx", " ", this)
    * --- Rimuovo il cursore
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Grafico Costi e Ricavi
    * --- Calcolo i Ricavi dal Database (Importo Evaso dall'Ordine PERCOMM ) e TOTCOMM-PERCOMM importo da evadere
    Vq_Exec ("..\Comm\Exe\Query\Gspc3Bac.Vqr",This,"_Ricavi2_") 
 =wrcursor("_Ricavi2_") 
 scan for mvcodval<>this.oParentObject.w_CODVAL 
 replace PerComm with VAL2CAM(PerComm, mvcodval, this.oParentObject.w_CODVAL, mvcaoval, cp_todate(mvdatdoc)) 
 replace TotComm with VAL2CAM(TotComm, mvcodval, this.oParentObject.w_CODVAL, mvcaoval, cp_todate(mvdatdoc)) 
 endscan 
 Select Sum(TotComm) As TotComm, Sum(PerComm) As PerComm ; 
 from _Ricavi2_ Into Cursor _Ricavi_ NoFilter 
 use in _Ricavi2_
    this.w_RICAGG = Nvl( _Ricavi_.PerComm , 0 )
    this.w_RICAFIN = Nvl( _Ricavi_.TotComm , 0 )-this.w_RICAGG
    this.w_RICATT = this.w_RICAGG+this.w_RICAFIN
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_COSAGG = this.w_TOTCOST
    this.w_COSAFIN = this.w_PREVEN-this.w_COSAGG
    this.w_COSATT = this.w_COSAGG+this.w_COSAFIN
    * --- Costruisco il Cursore per il grafico Costi / Ricavi
    Create Cursor _CosRic_ (Tipo C (15), Ricavi N (18,4) , Costi N (18,4) )
    Select _CosRic_
    Append Blank
    Replace Tipo With "Attuali", Ricavi With this.w_RICAGG, Costi With this.w_COSAGG
    Append Blank
    Replace Tipo With "Residui", Ricavi With this.w_RICAFIN, Costi With this.w_COSAFIN
    Append Blank
    Replace Tipo With "Attesi", Ricavi With this.w_RICATT, Costi With this.w_COSATT
    * --- Costruisco i cursori per i grafici a torta (Aggiornato)
    Create Cursor _MarAgg_ (Tipo C (15), PerCent N (18,4) )
    Select _MarAgg_
    Append Blank
    Replace Tipo With "Costi", PerCent With this.w_COSAGG
    Append Blank
    Replace Tipo With "Margine", PerCent With this.w_RICAGG-this.w_COSAGG
    this.w_Graph41Vis = this.w_RICAGG>=this.w_COSAGG
    this.oParentObject.w_GRAPH41.Visible = this.w_Graph41Vis
    this.oParentObject.w_LABEL41.Visible = not this.w_Graph41Vis
    if this.w_Graph41Vis
      this.oParentObject.w_TEXT41 = ""
    else
      this.oParentObject.w_TEXT41 = "Costi Sostenuti > Ricavi Conseguiti"+chr(13)+chr(13)
      this.oParentObject.w_TEXT41 = this.oParentObject.w_TEXT41 + "Costi Sostenuti = " + trans(this.w_COSAGG,v_pv(40+VVL)) + chr(13)
      this.oParentObject.w_TEXT41 = this.oParentObject.w_TEXT41 + "Ricavi Conseguiti = " + trans(this.w_RICAGG,v_pv(40+VVL)) + chr(13) + chr(13)
      this.oParentObject.w_TEXT41 = this.oParentObject.w_TEXT41 + "Differenza = " + trans(this.w_COSAGG-this.w_RICAGG,v_pv(40+VVL))
    endif
    * --- Costruisco il Cursore per il grafico TORTA margine  A FINIRE
    Create Cursor _MarFin_ (Tipo C (15), PerCent N (18,4) )
    Select _MarFin_
    Append Blank
    Replace Tipo With "Costi"
    Replace PerCent With this.w_COSAFIN
    Append Blank
    Replace Tipo With "Margine"
    Replace PerCent With this.w_RICAFIN-this.w_COSAFIN
    this.w_Graph42Vis = this.w_RICAFIN>=this.w_COSAFIN
    this.oParentObject.w_GRAPH42.Visible = this.w_Graph42Vis
    this.oParentObject.w_LABEL42.Visible = not this.w_Graph42Vis
    if this.w_Graph42Vis
      this.oParentObject.w_TEXT42 = ""
    else
      this.oParentObject.w_TEXT42 = "Costi Futuri > Ricavi Futuri"+chr(13)
      this.oParentObject.w_TEXT42 = this.oParentObject.w_TEXT42 + "Costi Futuri = " + trans(this.w_COSAFIN,v_pv(40+VVL)) + chr(13)
      this.oParentObject.w_TEXT42 = this.oParentObject.w_TEXT42 + "Ricavi Futuri = " + trans(this.w_RICAFIN,v_pv(40+VVL)) + chr(13) + chr(13)
      this.oParentObject.w_TEXT42 = this.oParentObject.w_TEXT42 + "Costi Futuri = " + trans(this.w_COSAFIN-this.w_COSAFIN,v_pv(40+VVL))
    endif
    * --- Costruisco il Cursore per il grafico TORTA margine  ATTESO
    Create Cursor _MarAtt_ (Tipo C (15), PerCent N (18,4) )
    Select _MarAtt_
    Append Blank
    Replace Tipo With "Costi", PerCent With this.w_COSATT
    Append Blank
    Replace Tipo With "Margine", PerCent With this.w_RICATT-this.w_COSATT
    this.w_Graph43Vis = this.w_RICATT>=this.w_COSATT
    this.oParentObject.w_GRAPH43.Visible = this.w_Graph43Vis
    this.oParentObject.w_LABEL43.Visible = not this.w_Graph43Vis
    if this.w_Graph43Vis
      this.oParentObject.w_TEXT43 = ""
    else
      this.oParentObject.w_TEXT43 = "Costi Totali > Ricavi Totali"+chr(13)
      this.oParentObject.w_TEXT43 = this.oParentObject.w_TEXT43 + "Costi Totali = " + trans(this.w_COSATT,v_pv(40+VVL)) + chr(13)
      this.oParentObject.w_TEXT43 = this.oParentObject.w_TEXT43 + "Ricavi Totali = " + trans(this.w_RICATT,v_pv(40+VVL)) + chr(13) + chr(13)
      this.oParentObject.w_TEXT43 = this.oParentObject.w_TEXT43 + "Costi Totali = " + trans(this.w_COSATT-this.w_RICATT,v_pv(40+VVL))
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CPAR_DEF'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='ATTIVITA'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
