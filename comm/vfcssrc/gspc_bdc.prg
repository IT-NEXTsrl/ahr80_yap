* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bdc                                                        *
*              Dettagli configurazione costi                                   *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_55]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-03                                                      *
* Last revis.: 2008-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bdc",oParentObject,m.pTipo)
return(i_retval)

define class tgspc_bdc as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_OBJMOV = .NULL.
  w_PARDOC = space(3)
  w_FLVEAC = space(1)
  w_CLADOC = space(1)
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dettaglio Configurazione Costi (da GSPC_KDC)
    do case
      case this.pTipo="ZOOM"
        do case
          case this.oParentObject.w_CODCOS <> "PNOTA"
            * --- Recupero i Dati Dai movimenti di magazzino / Documenti e Movimenti Preventivi
            Vq_Exec ( "..\Comm\Exe\Query\GSPC_KDC.VQR", This , "_Importi_" )
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_CODCOS="PNOTA"
            * --- Recupero i dati dal'analitica
            Vq_Exec ( "..\Comm\Exe\Query\GSPC1BDC.VQR", This , "PNota" )
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            Select NumReg, DatReg, Cons,Prev,Serial,Space(15) As CodAtt, Origine, Param, Tipo;
            From Pnota Into Cursor _Importi_ NoFilter
            * --- Rilascio il cursore
            Use In Pnota
        endcase
        * --- Svuoto il Cursore dello Zoom
        Select (this.oParentObject.w_ZOOM.cCursor)
        Zap
        Select _Importi_
        * --- Se importi Vuoto d� errore la Insert (non trova l'Array)
        if RecCount()>0
          Select _Importi_
          Go Top
          Scan
          Scatter Memvar
          Select (this.oParentObject.w_ZOOM.cCursor)
          Append blank
          Gather memvar
          Endscan
        endif
        Select (this.oParentObject.w_ZOOM.cCursor)
        Sum Cons, Prev To this.oParentObject.w_TOTCONS, this.oParentObject.w_TOTPREV
        Go Top
        * --- Refresho lo Zoom
        this.oParentObject.w_ZOOM.Refresh()     
        if used("_Importi_")
          use in ("_Importi_")
        endif
      case this.pTipo="DETTAGLIO"
        * --- Lancia la gestione dell'elemento dello Zoom Selezionato
        * --- w_ORIGINE
        *     MM Movimenti di magazzino
        *     MR Movimenti di ripartizione
        *     DO Documenti
        *     PN Primanota
        *     MA Movimenti manuali di analitica 
        *     MC Movimenti di commessa
        *     PF Preventivi a forfait
        do case
          case this.oParentObject.w_ORIGINE = "MM" Or (this.oParentObject.w_ORIGINE = "MR" And this.oParentObject.w_PARAM="MM")
            * --- Movimento di Magazzino
            gsar_bzm(this,this.oParentObject.w_SERIAL,-10)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_ORIGINE = "DO" Or (this.oParentObject.w_ORIGINE = "MR" And this.oParentObject.w_PARAM="DO")
            * --- Documenti
            gsar_bzm(this,this.oParentObject.w_SERIAL,-20)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case ( this.oParentObject.w_ORIGINE = "MR" And this.oParentObject.w_PARAM="PN" ) Or this.oParentObject.w_ORIGINE = "PN"
            * --- Prima Nota
            gsar_bzp(this,this.oParentObject.w_SERIAL)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_ORIGINE ="MA" Or (this.oParentObject.w_ORIGINE ="MR" And this.oParentObject.w_PARAM="MA")
            * --- Movimenti Manuali di Analitica
            this.w_OBJMOV = GSCA_ACM()
            * --- Testo subito se l'utente pu� aprire la gestione
            if !( this.w_OBJMOV.bSec1 )
              i_retcode = 'stop'
              return
            endif
            * --- Lancio l'Oggetto in Interrogazione
            this.w_OBJMOV.EcpFilter()     
            this.w_OBJMOV.w_CMCODICE = this.oParentObject.w_SERIAL
            this.w_OBJMOV.QueryKeySet("CMCODICE='"+this.oParentObject.w_SERIAL+ "'","")     
            this.w_OBJMOV.LoadRecWarn()     
          case this.oParentObject.w_ORIGINE ="MC"
            * --- Movimenti Di Commessa
            this.w_OBJMOV = GSPC_MAT(this.oParentObject.w_PARAM)
            * --- Testo subito se l'utente pu� aprire la gestione
            if !( this.w_OBJMOV.bSec1 )
              i_retcode = 'stop'
              return
            endif
            * --- Lancio l'Oggetto in Interrogazione
            this.w_OBJMOV.cFunction = "Query"
            this.w_OBJMOV.w_MASERIAL = this.oParentObject.w_SERIAL
            this.w_OBJMOV.QueryKeySet("MASERIAL='"+this.oParentObject.w_SERIAL+ "'","")     
            this.w_OBJMOV.LoadRecWarn()     
            oCpToolBar.SetQuery()
          case this.oParentObject.w_ORIGINE = "PF"
            this.w_OBJMOV = GSPC_ASC()
            if !( this.w_OBJMOV.bSec1 )
              i_retcode = 'stop'
              return
            endif
            * --- Lancio l'Oggetto in Interrogazione
            this.w_OBJMOV.cFunction = "Query"
            this.w_OBJMOV.w_ATCODCOM = this.oParentObject.w_CODCOM
            this.w_OBJMOV.w_ATCODATT = this.oParentObject.w_SOTTOCON
            this.w_OBJMOV.QueryKeySet("ATTIPATT='P' AND ATCODCOM='"+this.oParentObject.w_CODCOM+"' AND ATCODATT='"+this.oParentObject.w_SOTTOCON+"'","")     
            this.w_OBJMOV.LoadRecWarn()     
            oCpToolBar.SetQuery()
        endcase
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converto Importi nella valuta di Commessa
    Cur = WrCursor ( "PNota" )
    Scan For CodVal<>this.oParentObject.w_VALCOM
    * --- Se Movimento in valuta diversa lo converto nella valuta della commessa
    * --- L'altra possibile valuta � comunque di conto (Se valuta Commessa LIre allora Euro o viceversa)
    if Nvl(Prev,0)<>0
      Replace Prev With VAL2CAM( Prev , CodVal , this.oParentObject.w_VALCOM , 0 , i_DATSYS )
    endif
    if Nvl(Cons,0)<>0
      Replace Cons With VAL2CAM( Cons , CodVal , this.oParentObject.w_VALCOM , 0 , i_DATSYS )
    endif
    Select Pnota
    EndScan
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Cur = WrCursor ( "_Importi_" ) 
 Scan For Serial="SOTTOCONTO" 
 replace DatReg with cp_CharToDate("  -  -  ") 
 EndScan
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
