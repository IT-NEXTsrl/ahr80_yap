* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_btp                                                        *
*              Tempificazione con MS-PROJECT                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-26                                                      *
* Last revis.: 2015-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_btp",oParentObject)
return(i_retval)

define class tgspc_btp as StdBatch
  * --- Local variables
  w_TIPODB = space(10)
  w_cCursor = space(10)
  w_TEST = 0
  w_RISPOSTA = 0
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_TIPSTR = space(1)
  w_RIFKEY = space(100)
  w_CURSORNA = space(100)
  w_UNIQUE = space(1)
  w_STOP = .f.
  w_parent = .NULL.
  w_CODCOM = space(15)
  w_CALENDARIO = space(50)
  w_START_DFLT = ctod("  /  /  ")
  w_FINISH_DFLT = ctod("  /  /  ")
  w_WBSSUB = space(100)
  w_CODATTPRE = space(15)
  w_TIPATTPRE = space(1)
  w_CALENDARID = 0
  w_PROJECTID = 0
  w_IDSUCC = 0
  w_IDPRED = 0
  w_OUTLN_LVL = 0
  w_WBS = space(100)
  w_TASK_NAME = space(50)
  w_DATESQL1 = space(15)
  w_DATESQL2 = space(15)
  w_DEPENDENCYID = 0
  w_TIPAT_PRE = space(1)
  w_SEGNO = space(1)
  w_NODATA = .f.
  w_PRJLAN = space(1)
  w_PROGR = space(1)
  w_PRJREL = space(4)
  w_PRJUSR = space(40)
  w_PRJFILE = space(200)
  w_PRJPWD = space(40)
  w_ConnHandle = 0
  w_SQLstring = space(254)
  w_MESS = space(254)
  w_oMSP = .NULL.
  w_VERSION = 0
  w_oPRJ = .NULL.
  w_oRESOURCE = .NULL.
  w_oEXCEPT = .NULL.
  w_oTASK = .NULL.
  w_CONVDUR = 0
  w_STARTDATE = ctod("  /  /  ")
  w_ELABORA = space(1)
  w_ATSTATO = space(1)
  w_FINISHDATE = ctod("  /  /  ")
  w_PRED_NAME = space(15)
  w_DURATION = 0
  w_SUCC_NAME = space(15)
  w_CONSTYPE = space(3)
  w_LAGTYPE = space(2)
  w_MILESTONE = space(1)
  w_LAG = 0
  w_TASKID = 0
  w_PRJVW = space(1)
  w_PERCENTCOMPL = 0
  w_TEMPCONTO = space(15)
  w_CONSTDATE = ctod("  /  /  ")
  w_TIPAT = space(1)
  w_MSGBOXCHOICE = 0
  w_NUMREC = 0
  w_FOUND = .f.
  w_NEW_PRED_ID = 0
  w_NEW_SUCC_ID = 0
  w_OLD_DEPENDENCYID = 0
  w_NEW_TASKID = 0
  w_LEN1 = 0
  w_LEN2 = 0
  w_ERROR = 0
  w_MESSERR = space(0)
  w_oERRORLOG = .NULL.
  w_EXPMCOMM = .f.
  GSPC_KTP = .NULL.
  w_DINI = ctod("  /  /  ")
  w_DFIN = ctod("  /  /  ")
  w_CNPRJMOD = space(200)
  L_PRJMOD = space(200)
  w_ESISTE = .f.
  w_ESISTEMOD = .f.
  w_NUMREC = 0
  w_TASK = .NULL.
  w_CODICE = space(50)
  w_UID = 0
  w_PREDEC = space(20)
  w_NPREDEC = 0
  w_POSITION = 0
  w_STRPR = space(250)
  w_TYPEPRED = space(2)
  w_RITDAYS = 0
  w_MESS1 = space(10)
  w_TMPID = 0
  w_TMPUID = 0
  w_TIPCOM = space(1)
  w_PREDSTR = space(250)
  * --- WorkFile variables
  ATTIVITA_idx=0
  ATT_PREC_idx=0
  CAN_TIER_idx=0
  CPAR_DEF_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per la rappresentazione dello sviluppo temporale con MS-Project
    *     Il batch prepara i tracciati record adatti all'import / export di dati nei confronti di MS Project
    *     (lanciato da GSPC_KTP)
    this.GSPC_KTP = this.oParentObject
    this.w_EXPMCOMM = .F.
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.GSPC_KTP.oPgFrm.page3.setfocus()     
    this.GSPC_KTP.Refresh()     
    * --- Database utilizzato da project
    this.w_TIPODB = IIF(UPPER(CP_DBTYPE)="SLQSERVER","SQLSERVER","ORACLE")
    if this.oParentObject.w_CHOICE $ "I-E" 
      * --- Svuoto la Msg nel caso di nuova esportazione/importazione
      if TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
        G_MSG=""
      else
        this.GSPC_KTP.w_Msg = ""
        this.GSPC_KTP.SetControlsValue()     
      endif
      * --- Variabile che mi indica se devo esportare una o pi� commesse
      this.w_EXPMCOMM = .F.
      * --- Recupero codice commessa dalla maschera
      if this.oParentObject.w_SELEZMUL<>"S"
        this.w_CODCOM = this.GSPC_KTP.w_CODCOM
        this.w_CALENDARIO = this.GSPC_KTP.w_CALENDARIO
        this.w_START_DFLT = this.GSPC_KTP.w_START_DFLT
        this.w_FINISH_DFLT = this.GSPC_KTP.w_FINISH_DFLT
      else
        this.w_cCursor = this.GSPC_KTP.w_Zoom.cCursor
        * --- Commesse da esportare
        SELECT * , 9999 as CHOICE from (this.w_cCursor) into cursor COMMESSA ReadWrite where xchk=1
        this.w_TEST = RECCOUNT("COMMESSA")
        if this.w_TEST = 0
          AddMsgNL("Selezionare almeno una commessa ",this)
          i_retcode = 'stop'
          return
        endif
        this.w_EXPMCOMM = this.w_TEST > 1
      endif
      if this.oParentObject.w_CHOICE="E"
        * --- Caso di esportazione, leggo parametri di default
        * --- Read from CPAR_DEF
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PDMSPROJ,PDPROGR,PDPRJREL,PDUNIQUE,PDPRJUSR,PDPRJPWD"+;
            " from "+i_cTable+" CPAR_DEF where ";
                +"PDCHIAVE = "+cp_ToStrODBC("TAM");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PDMSPROJ,PDPROGR,PDPRJREL,PDUNIQUE,PDPRJUSR,PDPRJPWD;
            from (i_cTable) where;
                PDCHIAVE = "TAM";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PRJLAN = NVL(cp_ToDate(_read_.PDMSPROJ),cp_NullValue(_read_.PDMSPROJ))
          this.w_PROGR = NVL(cp_ToDate(_read_.PDPROGR),cp_NullValue(_read_.PDPROGR))
          this.w_PRJREL = NVL(cp_ToDate(_read_.PDPRJREL),cp_NullValue(_read_.PDPRJREL))
          this.w_UNIQUE = NVL(cp_ToDate(_read_.PDUNIQUE),cp_NullValue(_read_.PDUNIQUE))
          this.w_PRJUSR = NVL(cp_ToDate(_read_.PDPRJUSR),cp_NullValue(_read_.PDPRJUSR))
          this.w_PRJPWD = NVL(cp_ToDate(_read_.PDPRJPWD),cp_NullValue(_read_.PDPRJPWD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Leggo i dati relativi alla versione di Project usata (parametri di default)
        * --- Read from CPAR_DEF
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PDMSPROJ,PDPROGR,PDPRJREL,PDUNIQUE,PDPRJUSR,PDPRJPWD"+;
            " from "+i_cTable+" CPAR_DEF where ";
                +"PDCHIAVE = "+cp_ToStrODBC("TAM");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PDMSPROJ,PDPROGR,PDPRJREL,PDUNIQUE,PDPRJUSR,PDPRJPWD;
            from (i_cTable) where;
                PDCHIAVE = "TAM";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PRJLAN = NVL(cp_ToDate(_read_.PDMSPROJ),cp_NullValue(_read_.PDMSPROJ))
          this.w_PROGR = NVL(cp_ToDate(_read_.PDPROGR),cp_NullValue(_read_.PDPROGR))
          this.w_PRJREL = NVL(cp_ToDate(_read_.PDPRJREL),cp_NullValue(_read_.PDPRJREL))
          this.w_UNIQUE = NVL(cp_ToDate(_read_.PDUNIQUE),cp_NullValue(_read_.PDUNIQUE))
          this.w_PRJUSR = NVL(cp_ToDate(_read_.PDPRJUSR),cp_NullValue(_read_.PDPRJUSR))
          this.w_PRJPWD = NVL(cp_ToDate(_read_.PDPRJPWD),cp_NullValue(_read_.PDPRJPWD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Verifico se sto facendo un export oppure un import
    do case
      case this.oParentObject.w_CHOICE="I"
        AddMsgNL("Importing ",this)
        AddMsgNL("Elaborazione iniziata alle %1 ",this,Time() )
        if this.w_PRJREL<>"2003" Or (this.w_PRJREL="2003" AND this.oParentObject.w_SELEZMUL<>"S")
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          SELECT COMMESSA
          GO TOP 
 SCAN
          this.w_CODCOM = NVL(COMMESSA.CNCODCAN, SPACE(15))
          this.w_START_DFLT = CP_TODATE(COMMESSA.CNDATINI)
          this.w_FINISH_DFLT = CP_TODATE(COMMESSA.CNDATFIN)
          AddMsgNL("Analisi progetto "+alltrim(this.w_CODCOM)+" in corso... ",this)
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_PRJREL="2003"
            * --- Chiudo il progetto attivo senza salvare
            if ! this.oParentObject.w_SELEZMUL<>"S"
              w_olderr=ON("ERROR")
              w_err =.f.
              ON ERROR w_err=.t.
              this.w_oMSP.FileCloseAll(0)     
              this.w_oMSP.Quit(0)     
              this.w_oPRJ = .NULL.
              ON ERROR &w_olderr
            endif
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT COMMESSA
          ENDSCAN
          if this.oParentObject.w_SELEZMUL<>"S"
            this.w_oMSP.Visible = .t.
            this.w_oPRJ.Activate()     
          else
            this.w_oMSP = .NULL.
          endif
          AddMsgNL("Elaborazione terminata alle: %1" ,this,Time() )
          if USED("COMMESSA")
            USE IN COMMESSA
          endif
        endif
        if this.w_oERRORLOG.ISFULLLOG()
          if ah_YesNo("Ci sono messaggi da stampare. Si desidera eseguire la stampa?")
            this.w_oERRORLOG.PRINTLOG(THIS , ah_msgformat("Commessa %1 %2 - Messaggi di importazione da MSProject" , alltrim(this.oParentObject.w_CODCOM) , alltrim(this.oParentObject.w_DESCOM)) , .f.)     
          endif
        endif
      case this.oParentObject.w_CHOICE="E"
        AddMsgNL("Exporting...... ",this)
        AddMsgNL("Elaborazione iniziata alle %1 ",this,Time())
        * --- EXPORT DATI VERSO PROJECT
        if this.w_PRJREL<>"2003" Or (this.w_PRJREL="2003" AND this.oParentObject.w_SELEZMUL<>"S")
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          SELECT COMMESSA
          GO TOP 
 SCAN
          this.w_CODCOM = NVL(COMMESSA.CNCODCAN, SPACE(15))
          this.w_START_DFLT = CP_TODATE(COMMESSA.CNDATINI)
          this.w_FINISH_DFLT = CP_TODATE(COMMESSA.CNDATFIN)
          * --- Nel caso di esportazione di pi� commesse leggo la risposta dal cursore
          this.w_RISPOSTA = NVL(COMMESSA.CNTIPOPE, 0)
          do case
            case this.w_RISPOSTA=1
              this.w_RISPOSTA = 6
            case this.w_RISPOSTA=2
              this.w_RISPOSTA = 7
            case this.w_RISPOSTA=3
              this.w_RISPOSTA = 2
          endcase
          this.w_MSGBOXCHOICE=this.w_RISPOSTA
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT COMMESSA
          ENDSCAN
          if USED("COMMESSA")
            USE IN COMMESSA
          endif
        endif
        AddMsgNL("Elaborazione terminata alle: %1 ",this,Time())
        if this.w_PRJREL="2003"
          if this.w_oERRORLOG.ISFULLLOG()
            if ah_YesNo("Si desidera stampare il resoconto dell'esportazione?")
              this.w_oERRORLOG.PRINTLOG(THIS , ah_msgformat("Commessa %1 %2 - Messaggi di esportazione su MSProject" , alltrim(this.oParentObject.w_CODCOM) , alltrim(this.oParentObject.w_DESCOM)) , .f.)     
            endif
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rilascio dei cursori utilizzati durante il batch e chiusura della connessione ODBC
    if used("cursalfa")
      select("cursalfa")
      Use
    endif
    if used("curs0")
      select("curs0")
      Use
    endif
    if used("EXPORT")
      select("EXPORT")
      Use
    endif
    if used("CurPreds")
      select("CurPreds")
      Use
    endif
    if used(this.w_COUTCURS)
      select(this.w_COUTCURS)
      Use
    endif
    if used("Buffer")
      select("Buffer")
      Use
    endif
    if used("Curs1Project")
      select("Curs1Project")
      Use
    endif
    if used("CurPredsFinale")
      select("CurPredsFinale")
      Use
    endif
    if used("Curs2Project")
      select("Curs2Project")
      Use
    endif
    if used("Curs3Project")
      select("Curs3Project")
      Use
    endif
    if used("IMP_SUCC")
      select("IMP_SUCC")
      Use
    endif
    if used("IMP_PRED")
      select("IMP_PRED")
      Use
    endif
    if used("TasksInProject")
      select("TasksInProject")
      Use
    endif
    if used("TasksInProject2")
      select("TasksInProject2")
      Use
    endif
    if used("LinksInProject")
      select("LinksInProject")
      Use
    endif
    if used("MESSAGGI")
      select("MESSAGGI")
      Use
    endif
    if used("__TMP__")
      select("__TMP__")
      Use
    endif
    if used("Query")
      select("Query")
      Use
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esplosione della treeview per costruzione campo WBS nell'export verso project
    * --- Crea Cursore Preventivo
    * --- Assegno al cursore da usare un nome di cursore libero
    if USED(this.w_CURSORNA)
      USE IN (this.w_CURSORNA)
    endif
    this.w_CURSORNA = SYS(2015)
    * --- Definizione parametri da passare alla CP_EXPDB
    this.w_COUTCURS = this.w_CURSORNA
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = "CPROWORD"
    this.w_TIPSTR = iif(g_APPLICATION="ADHOC REVOLUTION","P",iif(nvl(this.w_UNIQUE," ")="S","P","G"))
    * --- Verifica dell'esistenza (del capoprogetto) della struttura. Se la struttura non esiste l'export non viene effettuato.
    * --- La struttura pu� non esistere per i seguenti motivi:
    * --- 1. la struttura gestionale non � stata creata dopo essere passati da una a tre strutture nella gestione del progetto
    * --- 2. la commessa � stata creata dall'analitica e non sono stati creati i capiprogetto
    this.w_STOP = .T.
    * --- Select from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
          +" where ATCODCOM="+cp_ToStrODBC(this.w_CODCOM)+" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR)+" and ATCODATT="+cp_ToStrODBC(this.w_CODCOM)+"";
           ,"_Curs_ATTIVITA")
    else
      select * from (i_cTable);
       where ATCODCOM=this.w_CODCOM and ATTIPATT=this.w_TIPSTR and ATCODATT=this.w_CODCOM;
        into cursor _Curs_ATTIVITA
    endif
    if used('_Curs_ATTIVITA')
      select _Curs_ATTIVITA
      locate for 1=1
      do while not(eof())
      * --- Se esiste il capoprogetto permetto al batch di continuare
      this.w_STOP = .F.
        select _Curs_ATTIVITA
        continue
      enddo
      use
    endif
    if this.w_STOP
      * --- Nel caso in cui la struttura sia vuota l'export non viene eseguito
      if this.w_TIPSTR="G"
        this.w_MESS = "Attenzione: la struttura gestionale non esiste%0Impossibile effettuare l'esportazione dei dati verso MS-Project"
      else
        this.w_MESS = "Attenzione: la struttura amministrativa non esiste%0Impossibile effettuare l'esportazione dei dati verso MS-Project"
      endif
      AddMsgNL(this.w_MESS+" ",this)
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Se la struttura esiste proseguo con l'export
    AddMsgNL("Caricamento progetto "+alltrim(this.w_CODCOM)+" in corso... ",this)
    if Empty ( this.oParentObject.w_CODCONTO )
      * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
      VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
    else
      * --- Recupero solo la parte di struttura identificata dal conto
      VQ_EXEC("..\COMM\EXE\QUERY\GSPC0BSC.VQR",this,"query")
    endif
    Select "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if USED("__tmp__")
        SELECT "__tmp__" 
 USE
      endif
    else
      if this.w_PRJREL<>"2003"
        SQLDISCONNECT(this.w_ConnHandle)
      endif
      * --- Rilascio la connessione alla fonte dati ODBC
      AddMsgNL("La selezione � vuota ",this)
      * --- Raise
      i_Error="Errore"
      return
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT DATI DA PROJECT
    this.w_NODATA = .f.
    * --- Try
    local bErr_038DF4A0
    bErr_038DF4A0=bTrsErr
    this.Try_038DF4A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      if !this.w_NODATA
        AddMsgNL("Si � verificato un errore in fase di import del progetto %1 "+Message(), this, alltrim(this.w_CODCOM) )
      endif
    endif
    bTrsErr=bTrsErr or bErr_038DF4A0
    * --- End
  endproc
  proc Try_038DF4A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.w_PRJREL<>"2003"
      this.w_PRJUSR = ALLTRIM(NVL(this.oParentObject.w_ODBCUSR,""))
      this.w_PRJPWD = ALLTRIM(NVL(this.oParentObject.w_ODBCPWD,""))
      * --- Apri la connessione al data source ODBC di provenienza
      store SQLCONNECT(ALLTRIM(this.oParentObject.w_ODBCSOURCE),this.w_PRJUSR,this.w_PRJPWD) to this.w_ConnHandle
      if this.w_ConnHandle<=0
        SQLDISCONNECT(this.w_ConnHandle)
        * --- Rilascio la connessione alla fonte dati ODBC
        AddMsgNL("Impossibile effettuare la connessione",this)
        * --- Raise
        i_Error="Errore"
        return
        i_retcode = 'stop'
        return
      else
        AddMsgNL("Connessione stabilita... ",this)
        SQLSETPROP(this.w_ConnHandle, "asynchronous", .F.)
      endif
      * --- Verifico se il progetto � presente nel database e se s� estraggo il suo ID
      do case
        case this.w_PRJREL="98"
          this.w_SQLstring = "select PROJECTID,STARTDATE,FINISHDATE "
          this.w_SQLstring = this.w_SQLstring+"from PROJECT_INFORMATION "
          this.w_SQLstring = this.w_SQLstring+"where PROJECTNAME="+cp_tostrodbc(this.w_CODCOM)
        case this.w_PRJREL="2000"
          this.w_SQLstring = "select PROJ_ID as PROJECTID, "
          this.w_SQLstring = this.w_SQLstring+"PROJ_INFO_START_DATE as STARTDATE,PROJ_INFO_FINISH_DATE as FINISHDATE "
          this.w_SQLstring = this.w_SQLstring+"from MSP_PROJECTS "
          this.w_SQLstring = this.w_SQLstring+"where PROJ_NAME="+cp_tostrodbc(this.w_CODCOM)
      endcase
      Create Cursor Curs1Project (PROJECTID n(4,0),STARTDATE d, FINISHDATE d)
      SQLEXEC(this.w_ConnHandle, this.w_SQLstring,"Curs1Project")
      select ("Curs1Project")
      if reccount()=0
        * --- Non esistono dati da importare relativi al progetto specificato
        this.w_MESS = "Non esistono dati relativi alla commessa %1 da importare%0"
        SQLDISCONNECT(this.w_ConnHandle)
        * --- Rilascio la connessione alla fonte dati ODBC
        AddMsgNL(this.w_MESS,this ,alltrim(this.w_CODCOM))
        * --- Raise
        i_Error="Errore"
        return
        i_retcode = 'stop'
        return
      endif
      * --- Se il progetto � stato reperito estraggo i dati e li preparo opportunamente per l'inserimento nel database AHE
      this.w_PROJECTID = PROJECTID
      * --- Preparazione dati relativi alla tabella TASK_INFORMATION
      create cursor Curs2Project (ATCODCOM C(15), ATTIPATT C(1), ATCODATT C(15), ;
      AT__RIGA N(4,0), ATDURGIO N(6,0), ATDATINI D, ATDATFIN D, ATPERCOM N(3,0), ;
      ATVINCOL C(3), ATDATVIN D, ATCARDIN C(1) )
      AddMsgNL("Lettura tasks dal database di Project... ",this)
      cur=wrcursor("Curs2Project")
      do case
        case this.w_PRJREL="98"
          this.w_SQLstring = "select TASKID,NAME,DURATION,STARTDATE,FINISHDATE, "
          this.w_SQLstring = this.w_SQLstring+"PERCENTCOMPLETE,CONSTRAINTTYPE,CONSTRAINTDATE,"
          this.w_SQLstring = this.w_SQLstring+"MILESTONE,SUMMARY,  '" + this.w_CODCOM + "' as COMMESSA "
          this.w_SQLstring = this.w_SQLstring+"from TASK_INFORMATION "
          this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
          * --- +" and SUMMARY=0"
          this.w_SQLstring = this.w_SQLstring+" and TASKID is not NULL and NAME is not NULL"
        case this.w_PRJREL="2000"
          this.w_SQLstring = "select TASK_ID as TASKID,TASK_NAME as NAME,TASK_DUR as DURATION, "
          this.w_SQLstring = this.w_SQLstring+"TASK_START_DATE as STARTDATE,TASK_FINISH_DATE as FINISHDATE, "
          this.w_SQLstring = this.w_SQLstring+"TASK_PCT_COMP as PERCENTCOMPLETE,TASK_CONSTRAINT_TYPE as CONSTRAINTTYPE, "
          this.w_SQLstring = this.w_SQLstring+"TASK_CONSTRAINT_DATE as CONSTRAINTDATE,  "
          this.w_SQLstring = this.w_SQLstring+"TASK_IS_MILESTONE as MILESTONE,TASK_IS_SUMMARY as SUMMARY,  '" + this.w_CODCOM + "' as COMMESSA "
          this.w_SQLstring = this.w_SQLstring+"from MSP_TASKS "
          this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
          * --- +" and TASK_IS_SUMMARY=0"
          this.w_SQLstring = this.w_SQLstring+" and TASK_ID is not NULL and TASK_ID>0 and TASK_NAME is not NULL"
      endcase
      SQLEXEC(this.w_ConnHandle, this.w_SQLstring,"EXPORT")
    else
      * --- Importazione version 2003 (senza DB)
      * --- Istanzia Application di MS-Project
      this.w_PRJFILE = ""
      this.w_ESISTE = .F.
      this.w_oPRJ = .NULL.
      this.w_oMSP = CreateObject("MsProject.Application")
      if this.oParentObject.w_SELEZMUL<>"S"
        if FILE(this.oParentObject.w_PRJMOD)
          this.w_oMSP.FileOpen(this.oParentObject.w_PRJMOD)     
        else
          AddMsgNL("File di progetto inesistente:  %1", this , alltrim(this.w_CODCOM) )
        endif
      else
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNPRJMOD"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNPRJMOD;
            from (i_cTable) where;
                CNCODCAN = this.w_CODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CNPRJMOD = NVL(cp_ToDate(_read_.CNPRJMOD),cp_NullValue(_read_.CNPRJMOD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !Empty(this.w_CNPRJMOD)
          * --- Se � presente il file di modello a livello di commessa utilizzo quello
          this.w_PRJFILE = LEFT(this.w_CNPRJMOD,ratc("\",this.w_CNPRJMOD))+this.convstr(alltrim(this.w_CODCOM))+".mpp"
          * --- Verifico se esiste il file
          this.w_ESISTE = FILE(this.w_PRJFILE)
        endif
        if !this.w_ESISTE
          * --- In alternativa provo a leggere il modello della maschera
          this.w_PRJFILE = LEFT(this.oParentObject.w_PPATHPRJ,ratc("\",this.oParentObject.w_PPATHPRJ))+this.convstr(alltrim(this.w_CODCOM))+".mpp"
          * --- Verifico se esiste il file
          this.w_ESISTE = FILE(this.w_PRJFILE)
        endif
        if this.w_ESISTE
          this.w_oMSP.FileOpen(this.w_PRJFILE)     
        else
          AddMsgNL("File di progetto inesistente:  %1", this , alltrim(this.w_PRJFILE) )
        endif
      endif
      this.w_oPRJ = this.w_oMSP.ActiveProject
      if VARTYPE(this.w_oPRJ)="O" and LEFT(ALLTRIM(this.w_oPRJ.Name),ATC(".",ALLTRIM(this.w_oPRJ.Name))-1)=this.convstr(ALLTRIM(this.w_CODCOM))
        this.w_oMSP.LevelingOptions(.f.)     
        Create Cursor Curs1Project (PROJECTID n(4,0),STARTDATE d, FINISHDATE d)
        Insert into Curs1Project (PROJECTID, STARTDATE, FINISHDATE) values (this.w_oPRJ.ID, this.w_oPRJ.Start, this.w_oPRJ.Finish)
        create cursor Curs2Project (ATCODCOM C(15), ATTIPATT C(1), ATCODATT C(15), ;
        AT__RIGA N(4,0), ATDURGIO N(6,0), ATDATINI D, ATDATFIN D, ATPERCOM N(3,0), ;
        ATVINCOL C(3), ATDATVIN D, ATCARDIN C(1) )
        CREATE CURSOR EXPORT (TASKID N(4,0), NAME C(254),DURATION N(12,0),STARTDATE D,FINISHDATE D,PERCENTCOMPLETE N(4,0),; 
 CONSTRAINTTYPE N(4,0), CONSTRAINTDATE D, MILESTONE L, SUMMARY L, COMMESSA C(15))
        this.w_NUMREC = 1
        do while this.w_NUMREC<=this.w_oPRJ.Tasks.count
          this.w_TASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
          INSERT INTO EXPORT (TASKID,NAME,DURATION,STARTDATE,FINISHDATE,PERCENTCOMPLETE,CONSTRAINTTYPE,CONSTRAINTDATE,MILESTONE,SUMMARY,COMMESSA); 
 values (this.w_TASK.UniqueID,this.w_TASK.Name, this.w_TASK.Duration,this.w_TASK.Start, this.w_TASK.Finish, this.w_TASK.PercentComplete, this.w_TASK.ConstraintType,; 
 iif(VARTYPE(this.w_TASK.ConstraintDate)="C" and this.w_TASK.ConstraintDate $ "NA-N.D.",CTOD("  -  -    "),this.w_TASK.ConstraintDate),this.w_TASK.Milestone,this.w_TASK.Summary,this.w_TASK.TEXT1)
          this.w_NUMREC = this.w_NUMREC+1
        enddo
      else
        * --- Non esistono dati da importare relativi al progetto specificato
        this.w_NODATA = .t.
        this.w_MESS = "Non esistono dati relativi alla commessa %1 da importare"
        AddMsgNL(this.w_MESS,this ,alltrim(this.w_CODCOM))
        * --- Raise
        i_Error="Errore"
        return
        i_retcode = 'stop'
        return
      endif
    endif
    select ("EXPORT")
    go top
    scan
    this.w_ELABORA = "S"
    this.w_CODCOM = IIF(TYPE("EXPORT.COMMESSA")="C", EXPORT.COMMESSA, SPACE(15))
    this.w_TASK_NAME = substr(NAME,1,15)
    this.w_TIPAT = substr(NAME,rat(")",NAME)-1,1)
    Vq_Exec("..\comm\exe\query\GSPC2BTP.vqr",this,"cursalfa")
    select "cursalfa"
    go top
    if reccount()=0
      * --- Il task estratto dal progetto Project non ha un corrispondente in AHE e quindi non pu� essere gestito."
      this.w_MESS = "Il task %1 non esiste in %2 e non pu� essere gestito. Continuare lo stesso?"
      if not ah_yesno(this.w_MESS,"",alltrim(this.w_TASK_NAME),alltrim(g_APPLICATION))
        * --- Tutti i dati relativi al progetto vengono ignorati
        if this.w_PRJREL<>"2003"
          SQLDISCONNECT(this.w_ConnHandle)
        endif
        * --- Rilascio la connessione alla fonte dati ODBC
        ah_ErrorMsg("Importazione terminata dall'utente",48)
        * --- Raise
        i_Error="Errore"
        return
        i_retcode = 'stop'
        return
      else
        * --- Vengono ignorati solo i dati relativi al task inammissibile in AHE
        AddMsgNL("Ripresa importazione dati commessa %1 ",this,alltrim(this.w_CODCOM))
        this.w_ELABORA = "N"
      endif
    else
      * --- Il task estratto esiste in AHE: prima di continuare verifico se � un'attivit� foglia o no
      this.w_PRJVW = NVL(AT_PRJVW, " ")
    endif
    select "EXPORT"
    if this.w_ELABORA="S"
      * --- Se il task considerato � ammissibile in AHE
      * --- Preparo i dati da inserire nel cursore
      this.w_TASKID = TASKID
      if this.w_PRJREL="2003"
        this.w_DURATION = INT(DURATION / 480)
      else
        this.w_DURATION = INT(DURATION / 4800)
      endif
      this.w_STARTDATE = CP_TODATE(STARTDATE)
      this.w_FINISHDATE = CP_TODATE(FINISHDATE)
      this.w_PERCENTCOMPL = PERCENTCOMPLETE
      do case
        case NVL(CONSTRAINTTYPE,0)=0
          this.w_CONSTYPE = "PPP"
        case NVL(CONSTRAINTTYPE,0)=1
          this.w_CONSTYPE = "PTP"
        case NVL(CONSTRAINTTYPE,0)=2
          this.w_CONSTYPE = "DII"
        case NVL(CONSTRAINTTYPE,0)=3
          this.w_CONSTYPE = "DFI"
        case NVL(CONSTRAINTTYPE,0)=4
          this.w_CONSTYPE = "INP"
        case NVL(CONSTRAINTTYPE,0)=5
          this.w_CONSTYPE = "INO"
        case NVL(CONSTRAINTTYPE,0)=6
          this.w_CONSTYPE = "FNP"
        case NVL(CONSTRAINTTYPE,0)=7
          this.w_CONSTYPE = "FNO"
      endcase
      this.w_CONSTDATE = CP_TODATE(CONSTRAINTDATE)
      if this.w_PRJREL<>"2003"
        if MILESTONE=iif(CP_DBTYPE="SQLServer",.F.,iif(CP_DBTYPE="Oracle",0,0))
          * --- Non ancora gestito per DB2
          this.w_MILESTONE = "N"
        else
          this.w_MILESTONE = "S"
        endif
      else
        if MILESTONE=.f.
          this.w_MILESTONE = "N"
        else
          this.w_MILESTONE = "S"
        endif
      endif
      * --- inserisco i dati per la macro attivit�
      select "Curs2Project"
      Insert into Curs2Project ;
      (ATCODCOM, ATTIPATT, ATCODATT, AT__RIGA, ATDURGIO, ATDATINI, ATDATFIN, ;
      ATPERCOM, ATVINCOL, ATDATVIN, ATCARDIN) ;
      Values ;
      (this.w_CODCOM,this.w_TIPAT,this.w_TASK_NAME, this.w_TASKID,this.w_DURATION,this.w_STARTDATE, this.w_FINISHDATE, ;
      this.w_PERCENTCOMPL, this.w_CONSTYPE, this.w_CONSTDATE, this.w_MILESTONE)
      if this.w_PRJVW="F"
        * --- Se l'attivit� � una macro attivit� foglia in AHE, occorre anche scrivere i suoi dati su tutte le sue sottoattivit�
        * --- Per fare questo preparo un cursore con l'esplosione della treeview
        this.w_TEMPCONTO = this.oParentObject.w_CODCONTO
        * --- Salvo l'attivit� selezionata (essendo l'import dovrebbe essere sempre vuota)
        this.oParentObject.w_CODCONTO = this.w_TASK_NAME
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        select (this.w_COUTCURS)
        go top
        scan
        * --- Estraggo l'ATCODATT e l'ATTIPATT delle sottoattivit� in AHE
        this.w_TASK_NAME = ATCODATT
        this.w_TIPAT = ATTIPATT
        if this.w_TASK_NAME<>this.oParentObject.w_CODCONTO
          select "Curs2Project"
          Insert into Curs2Project ;
          (ATCODCOM, ATTIPATT, ATCODATT, AT__RIGA, ATDURGIO, ATDATINI, ATDATFIN, ;
          ATPERCOM, ATVINCOL, ATDATVIN, ATCARDIN) ;
          Values ;
          (this.w_CODCOM,this.w_TIPAT,this.w_TASK_NAME, this.w_TASKID,this.w_DURATION,this.w_STARTDATE, this.w_FINISHDATE, ;
          this.w_PERCENTCOMPL, this.w_CONSTYPE, this.w_CONSTDATE, this.w_MILESTONE)
        endif
        select (this.w_COUTCURS)
        endscan
        * --- su w_COUTCURS
        * --- Rilascio il cursore della treeview per prepararlo al nuovo uso
        if used(this.w_COUTCURS)
          select(this.w_COUTCURS)
          Use
        endif
        this.oParentObject.w_CODCONTO = this.w_TEMPCONTO
        * --- Ripristino l'attivit� selezionata (dovrebbe essere sempre vuota)
      endif
      select "EXPORT"
    endif
    endscan
    * --- su EXPORT
    * --- Preparazione dati relativi alla tabella TASK_DEPENDENCIES
    this.Page_9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Scrittura dati nel database AHE
    if this.w_PRJREL<>"2003"
      SQLDISCONNECT(this.w_ConnHandle)
      * --- Rilascio la connessione alla fonte dati ODBC
      * --- (la scrittura viene differita in questo punto per permettere la chiusura della connessione ODBC manuale
      * --- e dunque non avere due connessioni ODBC aperte contemporaneamente)
    endif
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    AddMsgNL("L'aggiornamento del database di ad hoc %1 del progetto %2 � riuscito ", this,iif(isAHE(),"Enterprise","Revolution"), alltrim(this.w_CODCOM) )
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Commessa Selezionata
    * --- Identificatore della fonte dati ODBC
    * --- Nome di calendario per MS_Project
    * --- Data di inizio di default (=CNDATINI) - utilizzata nella progr. in avanti
    * --- Data di fine di default (=CNDATFIN) - utilizzata nella progr. all'indietro
    * --- Tipo di collegamento: 'E'=export verso Project; 'I'=import da Project
    * --- Parametro di default: programmazione in avanti o all'indietro
    * --- (serve per decidere se settare le date di inizio o di fine in Project)
    * --- Per esplosione treeview in pag3
    * --- Modello per esportazione MSProject 2007
    * --- Per effettuare il ciclo di trim sulla stringa del WBS
    * --- Codice attivit� predecessore
    * --- Tipo attivit� predecessore
    * --- ID di Calendario (calcolato a run-time)
    * --- ID di Progetto (calcolato a Run-time)
    * --- ID di riga di successore in un legame di precedenza (calcolato a run-time)
    * --- ID di riga di predecessore in un legame di precedenza (calcolato a run-time)
    * --- Livello del task in un outline (?): assume valori in [0..10]
    * --- Posizionamento nella struttura (calcolato a run-time in base alla treeview)
    * --- Codice attivit� da passare alla query per i predecessori
    * --- Buffer per accogliere le date in formato comprensibile da SQL server
    * --- (ovvero del tipo: {d 'yyyy-mm-dd'}
    * --- Identificatore unico di legame di precedenza (usato come contatore)
    * --- Segno dei giorni di ritardo dei predecessori
    * --- Test errore di transazione o import fallito per mancanza di dati
    * --- Utente di default per connessione ODBC al database di Project
    * --- Password di default per connessione ODBC al database di Project
    * --- Variabili di servizio per connessione ODBC
    * --- Handle per connessione ODBC
    * --- Stringa per query SQL
    * --- Stringa per messaggi a video
    * --- Variabili per esportazione su modello w_PRJREL='2003'
    * --- Variabili locali per import
    * --- Altre variabili locali
    * --- Risultato della msgbox a pag 6 (valori: 6=SI, 7=NO, 2=ANNULLA)
    * --- Numero di record nel cursore
    * --- Flag trovato
    * --- Nuovo id del predecessore nel link di precedenza (usato a pag.10)
    * --- Nuovo id del successore nel link di precedenza (usato a pag.10)
    * --- Vecchio id di link di precedenza (usato a pag.10)
    * --- Nuovo id da associare al task in aggiornamento (pag.7)
    * --- Buffer per gestione wbs a pag.10
    * --- CURSORE PER I MESSAGGI
    this.w_oERRORLOG=createobject("AH_ERRORLOG")
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_PRJREL<>"2003"
      * --- Fasi preliminari e determinazione del ProjectID (chiave per legare le varie tabelle)
      * --- Uso le date di inizio/fine commessa come date di default
      this.w_START_DFLT = cp_todate(this.w_START_DFLT)
      this.w_FINISH_DFLT = cp_todate(this.w_FINISH_DFLT)
      * --- Leggo i dati relativi alla versione di Project usata (parametri di default)
      * --- Se esportazione via ODBC testo la connessione....
      this.w_PRJUSR = this.oParentObject.w_ODBCUSR
      this.w_PRJPWD = alltrim(this.oParentObject.w_ODBCPWD)
      * --- Apri la connessione al data source ODBC di destinazione
      store SQLCONNECT(ALLTRIM(this.oParentObject.w_ODBCSOURCE),this.w_PRJUSR,this.w_PRJPWD) to this.w_ConnHandle
      if this.w_ConnHandle<=0
        SQLDISCONNECT(this.w_ConnHandle)
        * --- Rilascio la connessione alla fonte dati ODBC
        AddMsgNL("Impossibile effettuare la connessione",this)
        * --- Raise
        i_Error="Errore"
        return
        i_retcode = 'stop'
        return
      else
        AddMsgNL("Connessione stabilita ",this)
        SQLSETPROP(this.w_ConnHandle, "asynchronous", .F.)
      endif
      * --- Verifico se il progetto � gi� presente nel database
      do case
        case this.w_PRJREL="98"
          this.w_SQLstring = "select PROJECTID "
          this.w_SQLstring = this.w_SQLstring+"from PROJECT_INFORMATION "
          this.w_SQLstring = this.w_SQLstring+"where PROJECTNAME="+cp_tostrodbc(this.w_CODCOM)
        case this.w_PRJREL="2000"
          this.w_SQLstring = "select PROJ_ID as PROJECTID "
          this.w_SQLstring = this.w_SQLstring+"from MSP_PROJECTS "
          this.w_SQLstring = this.w_SQLstring+"where PROJ_NAME="+cp_tostrodbc(this.w_CODCOM)
      endcase
      Create Cursor Curs1Project (MAXID n(4,0))
      SQLEXEC(this.w_ConnHandle, this.w_SQLstring,"Curs1Project")
      select ("Curs1Project")
      this.w_NUMREC = reccount()
      if this.w_NUMREC>0
        * --- Se il progetto esiste gi�, chiedi se si vuole sovrascrivere il progetto (delete+insert) o no (skip)
        if ah_yesno("Il progetto relativo alla commessa %1 esiste gi�. Si desidera sovrascriverlo?","", alltrim(this.w_CODCOM))
          this.w_MSGBOXCHOICE=6
        else
          if ah_yesno("Si desidera aggiornare i soli dati relativi alla tempificazione e ai legami (premi S�) o uscire senza aggiornare i dati (premi No)?")
            this.w_MSGBOXCHOICE=7
          else
            this.w_MSGBOXCHOICE=2
          endif
        endif
      else
        * --- Se il progetto non esiste devo sempre effettuare l'inserimento dei dati
        this.w_MSGBOXCHOICE=6
      endif
      do case
        case this.w_MSGBOXCHOICE=6
          * --- primo yesno='SI' o reccount=0 (i dati relativi al vecchio progetto vengono eliminati solo nel caso di progetto gi� esistente)
          if this.w_NUMREC>0
            * --- (progetto gi� esistente)
            this.w_PROJECTID = PROJECTID
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "delete from TASK_DEPENDENCIES where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                this.w_SQLstring = "delete from TASK_INFORMATION where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                this.w_SQLstring = "delete from CALENDARS where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                this.w_SQLstring = "delete from PROJECT_INFORMATION where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
              case this.w_PRJREL="2000"
                this.w_SQLstring = "delete from MSP_LINKS where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                this.w_SQLstring = "delete from MSP_TASKS where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                this.w_SQLstring = "delete from MSP_CALENDARS where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                this.w_SQLstring = "delete from MSP_PROJECTS where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
            endcase
          endif
          * --- Prendi il primo PROJECTID libero
          do case
            case this.w_PRJREL="98"
              this.w_SQLstring = "select MAX(PROJECTID)  as MAXID from PROJECT_INFORMATION "
            case this.w_PRJREL="2000"
              this.w_SQLstring = "select MAX(PROJ_ID)  as MAXID from MSP_PROJECTS "
          endcase
          SQLEXEC(this.w_ConnHandle, this.w_SQLstring,"Curs1Project")
          select ("Curs1Project")
          this.w_PROJECTID = nvl(MAXID,0)+1
          * --- Prendi il primo CALENDARUNIQUEID libero
          select ("Curs1Project")
          ZAP
          do case
            case this.w_PRJREL="98"
              this.w_SQLstring = "select MAX(CALENDARUNIQUEID)  as MAXID "
              this.w_SQLstring = this.w_SQLstring+"from CALENDARS "
            case this.w_PRJREL="2000"
              this.w_SQLstring = "select MAX(CAL_UID)  as MAXID "
              this.w_SQLstring = this.w_SQLstring+"from MSP_CALENDARS "
          endcase
          SQLEXEC(this.w_ConnHandle, this.w_SQLstring,"Curs1Project")
          select ("Curs1Project")
          this.w_CALENDARID = nvl(MAXID,0)+1
        case this.w_MSGBOXCHOICE=7
          * --- secondo yesno= S�': la procedura aggiorner� solo i dati relativi a tempificazione e legami sul progetto esistente
          this.w_MESS = "Attenzione: i dati relativi ad attivit� gi� presenti in MSProject ma non considerate nella nuova esportazione verranno eliminati%0%0Si desidera continuare lo stesso?"
          if ah_YESNO(this.w_MESS)
            * --- Si acquisiscono quindi il ProjectId ed il CalendarId necessari
            select ("Curs1Project")
            this.w_PROJECTID = PROJECTID
            * --- Elimino dalla tabella dei progetti i dati binari per la visualizzazione dei Gantt in Project
            *     Tali dati verranno automaticamente ricalcolati da Project al caricamento del progetto
            *     e devono essere eliminati perch� altrimenti si creano dei problemi di visualizzazione.
            *     ATTENZIONE: deve essere ancora in parte verificato che non vengano eliminati altri dati significativi!!!!
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "Update PROJECT_INFORMATION "
                this.w_SQLstring = this.w_SQLstring+"set RESERVED_BINARY_DATA=NULL "
                this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
              case this.w_PRJREL="2000"
                this.w_SQLstring = "Update MSP_PROJECTS "
                this.w_SQLstring = this.w_SQLstring+"set RESERVED_BINARY_DATA=NULL "
                this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
            endcase
            this.w_MESSERR = "Impossibile aggiornare la tabella dei progetti: %2"
            this.Page_11()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Ricerca del CalendarId
            ZAP
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "select MAX(CALENDARUNIQUEID)  as MAXID "
                this.w_SQLstring = this.w_SQLstring+"from CALENDARS "
                this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
              case this.w_PRJREL="2000"
                this.w_SQLstring = "select MAX(CAL_UID)  as MAXID "
                this.w_SQLstring = this.w_SQLstring+"from MSP_CALENDARS "
                this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
            endcase
            SQLEXEC(this.w_ConnHandle, this.w_SQLstring,"Curs1Project")
            select ("Curs1Project")
            this.w_CALENDARID = MAXID
            * --- Vengono eliminati dalla tabella dei task le righe NULL che Project crea automaticamente sul salvataggio di un progetto
            *     e che possono creare problemi in aggiornamento - DA ELIMINARE ????
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "Delete from TASK_INFORMATION "
                this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
                this.w_SQLstring = this.w_SQLstring+" and NAME is NULL"
              case this.w_PRJREL="2000"
                this.w_SQLstring = "Delete from MSP_TASKS "
                this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
                this.w_SQLstring = this.w_SQLstring+" and TASK_NAME is NULL"
            endcase
            * --- Try
            local bErr_039FC540
            bErr_039FC540=bTrsErr
            this.Try_039FC540()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              AddMsgNL("Impossibile aggiornare la tabella dei task", this)
              * --- Raise
              i_Error="Errore"
              return
            endif
            bTrsErr=bTrsErr or bErr_039FC540
            * --- End
            * --- Si preparano gli asssegnamenti di risorse per gli aggiornamenti
            *     (occorre porre a negativi gli assegnamenti di risorse per evitare che gli aggiornamenti
            *     successivi facciano confusione tra vecchi e nuovi ID dei task)
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "Update ASSIGNMENT_INFORMATION set TASKUNIQUEID= - TASUNIQUEID "
                this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
              case this.w_PRJREL="2000"
                this.w_SQLstring = "Update MSP_ASSIGNMENTS set TASK_UID= - TASK_UID "
                this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
            endcase
            this.w_MESSERR = "Impossibile aggiornare la tabella degli assegnamenti di risorse: %2"
            this.Page_11()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- La procedura termina senza effettuare alcun aggiornamento
            SQLDISCONNECT(this.w_ConnHandle)
            AddMsgNL("Verranno mantenuti i vecchi dati",this)
            * --- Raise
            i_Error="Errore"
            return
            i_retcode = 'stop'
            return
          endif
        case this.w_MSGBOXCHOICE=2
          * --- secondo yesno=No: la procedura termina senza effettuare alcun aggiornamento
          SQLDISCONNECT(this.w_ConnHandle)
          * --- Rilascio la connessione alla fonte dati ODBC
          AddMsgNL("Verranno mantenuti i vecchi dati",this)
          * --- Raise
          i_Error="Errore"
          return
          i_retcode = 'stop'
          return
      endcase
    else
      * --- Esportazione version 2003 (senza DB)
      * --- Istanzia Application di MS-Project
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNDATINI,CNDATFIN,CNPRJMOD"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNDATINI,CNDATFIN,CNPRJMOD;
          from (i_cTable) where;
              CNCODCAN = this.w_CODCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DINI = NVL(cp_ToDate(_read_.CNDATINI),cp_NullValue(_read_.CNDATINI))
        this.w_DFIN = NVL(cp_ToDate(_read_.CNDATFIN),cp_NullValue(_read_.CNDATFIN))
        this.w_CNPRJMOD = NVL(cp_ToDate(_read_.CNPRJMOD),cp_NullValue(_read_.CNPRJMOD))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Test sulla variabile w_oMSP se l'istamza di MSProject � gi� stata creata la utilizzo
      if Type("this.w_oMSP")<>"O" Or (Type("this.w_oMSP")="O" And IsNull(this.w_oMSP))
        this.w_oMSP = CreateObject("MsProject.Application")
      endif
      this.w_PRJFILE = ""
      this.w_ESISTE = .F.
      this.w_ESISTEMOD = .F.
      if !Empty(this.w_CNPRJMOD)
        * --- Se � presente il file di modello a livello di commessa utilizzo quello
        this.w_PRJFILE = LEFT(this.w_CNPRJMOD,ratc("\",this.w_CNPRJMOD))+this.convstr(alltrim(this.w_CODCOM))+".mpp"
        * --- Verifico se esiste il file
        this.w_ESISTE = FILE(this.w_PRJFILE)
      endif
      if !this.w_ESISTE
        * --- In alternativa provo a leggere il modello dei parametri di default
        this.w_PRJFILE = LEFT(this.oParentObject.w_PRJMOD,ratc("\",this.oParentObject.w_PRJMOD))+this.convstr(alltrim(this.w_CODCOM))+".mpp"
        * --- Verifico se esiste il file
        this.w_ESISTE = FILE(this.w_PRJFILE)
      endif
      if this.w_ESISTE
        this.w_oMSP.ProjectSummaryInfo(, alltrim(this.w_CODCOM),,,,,, , this.w_DINI, this.w_DFIN,iif(this.w_PROGR="A",1,2), ,,,,)     
        this.w_oMSP.FileOpen(this.w_PRJFILE)     
        this.w_oPRJ = this.w_oMSP.ActiveProject
        this.w_PROJECTID = this.w_oPRJ.ID
        if this.oParentObject.w_SELEZMUL<>"S"
          if ah_yesno("Il progetto relativo alla commessa %1 esiste gi�. Si desidera sovrascriverlo?","", alltrim(this.w_CODCOM))
            this.w_MSGBOXCHOICE=6
            this.w_TASK = this.w_oPRJ.Tasks.Item(1)
            this.w_TASK.Delete()     
          else
            if ah_yesno("Si desidera aggiornare i soli dati relativi alla tempificazione e ai legami (premi S�) o uscire senza aggiornare i dati (premi No)?")
              this.w_MSGBOXCHOICE=7
            else
              this.w_MSGBOXCHOICE=2
            endif
          endif
        else
          do case
            case this.w_MSGBOXCHOICE=6
              this.w_TASK = this.w_oPRJ.Tasks.Item(1)
              this.w_TASK.Delete()     
              this.w_oMSP.ProjectSummaryInfo(, alltrim(this.w_CODCOM),,,,,, , this.w_DINI, this.w_DFIN,iif(this.w_PROGR="A",1,2), ,,,,)     
          endcase
        endif
      else
        if !Empty(this.w_CNPRJMOD)
          * --- Se � presente il file di modello a livello di commessa utilizzo quello
          this.L_PRJMOD = this.w_CNPRJMOD
          * --- Verifico se esiste il file
          this.w_ESISTEMOD = FILE(this.L_PRJMOD)
        endif
        if ! this.w_ESISTEMOD
          * --- In alternativa provo a leggere il modello dei parametri di default
          this.L_PRJMOD = this.oParentObject.w_PRJMOD
          * --- Verifico se esiste il file
          this.w_ESISTEMOD = FILE(this.L_PRJMOD)
        endif
        if this.w_ESISTEMOD
          this.w_PRJFILE = LEFT(this.L_PRJMOD,ratc("\",this.L_PRJMOD))+this.convstr(alltrim(this.w_CODCOM))+".mpp"
          this.w_oMSP.FileOpen(this.L_PRJMOD)     
          this.w_oMSP.ProjectSummaryInfo(, alltrim(this.w_CODCOM),,,,,, , this.w_DINI, this.w_DFIN,iif(this.w_PROGR="A",1,2), ,,,,)     
          this.w_oPRJ = this.w_oMSP.ActiveProject
          this.w_oPRJ.Title = AllTrim(this.w_CODCOM)
          this.w_oPRJ.SaveAs(alltrim(this.w_PRJFILE))     
          this.w_oMSP.FileCloseAll()     
          this.w_oMSP.FileOpen(this.w_PRJFILE)     
          this.w_oPRJ = this.w_oMSP.ActiveProject
        else
          this.w_PRJFILE = SYS(5)+"\"+SYS(2003)+"\"+this.convstr(alltrim(this.w_CODCOM))+".mpp"
          this.w_oPRJ = this.w_oMSP.Projects.Add()
          this.w_oPRJ.SaveAs(alltrim(this.w_PRJFILE))     
        endif
        this.w_PROJECTID = this.w_oPRJ.ID
        this.w_MSGBOXCHOICE=6
      endif
      this.w_oMSP.LevelingOptions(.f.)     
      this.w_oMSP.LevelNow(.f.)     
      if this.oParentObject.w_SELEZMUL<>"S"
        this.w_oMSP.Visible = True
      else
        if this.w_EXPMCOMM
          this.w_oMSP.Visible = False
        else
          this.w_oMSP.Visible = True
        endif
      endif
      this.w_oPRJ.Activate()     
      this.w_oMSP.ScreenUpdating = False
      if this.oParentObject.w_SELEZMUL="S"
        SELECT "COMMESSA"
        REPLACE CNTIPOPE WITH this.w_MSGBOXCHOICE
      endif
    endif
  endproc
  proc Try_039FC540()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Effettuo gli aggiornamenti al database di Project (EXPORT)
    * --- Aggiornamento della tabella PROJECT_INFORMATION
    do case
      case this.w_MSGBOXCHOICE=6
        * --- Progetto non esistente o si � scelto di sovrascrivere il progetto: preparazione della stringa SQL di inserzione
        do case
          case this.w_PRJREL="98"
            this.w_SQLstring = "Insert into PROJECT_INFORMATION "
            this.w_SQLstring = this.w_SQLstring+"(PROJECTID,PROJECTNAME,STARTDATE,FINISHDATE) "
            this.w_SQLstring = this.w_SQLstring+"values ("+cp_tostrodbc(this.w_PROJECTID)+","+cp_tostrodbc(this.w_CODCOM)+","
            this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(this.w_START_DFLT)+","+cp_tostrodbc(this.w_FINISH_DFLT)+")"
          case this.w_PRJREL="2000"
            this.w_SQLstring = "Insert into MSP_PROJECTS "
            this.w_SQLstring = this.w_SQLstring+"(PROJ_ID,PROJ_NAME,PROJ_INFO_START_DATE,PROJ_INFO_FINISH_DATE,PROJ_EXT_EDITED,PROJ_INFO_SCHED_FROM) "
            this.w_SQLstring = this.w_SQLstring+"values ("+cp_tostrodbc(this.w_PROJECTID)+","+cp_tostrodbc(this.w_CODCOM)+","
            this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(this.w_START_DFLT)+","+cp_tostrodbc(this.w_FINISH_DFLT)+",1,"+iif( this.w_PROGR="A","1","0")+")"
        endcase
      case this.w_MSGBOXCHOICE=7
        * --- Si � scelto di aggiornare il progetto: preparazione della stringa SQL di aggiornamento
        do case
          case this.w_PRJREL="98"
            this.w_SQLstring = "Update PROJECT_INFORMATION "
            this.w_SQLstring = this.w_SQLstring+"set STARTDATE="+cp_tostrodbc(this.w_START_DFLT)+","
            this.w_SQLstring = this.w_SQLstring+"FINISHDATE="+cp_tostrodbc(this.w_FINISH_DFLT)+" "
            this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
          case this.w_PRJREL="2000"
            this.w_SQLstring = "Update MSP_PROJECTS "
            this.w_SQLstring = this.w_SQLstring+"set PROJ_INFO_START_DATE="+cp_tostrodbc(this.w_START_DFLT)+","
            this.w_SQLstring = this.w_SQLstring+"PROJ_INFO_FINISH_DATE="+cp_tostrodbc(this.w_FINISH_DFLT)+", PROJ_EXT_EDITED=1,"
            this.w_SQLstring = this.w_SQLstring+"PROJ_INFO_SCHED_FROM="+iif( this.w_PROGR="A","1","0")
            this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
          case this.w_PRJREL="2003"
            if this.w_PROJECTID=0
              this.w_oPRJ = this.w_oMSP.Projects.Add(.f.)
              this.w_PROJECTID = this.w_oPRJ.ID
            endif
            this.w_oMSP.Projects.Item(w_PROJECTID).Name = this.w_CODCOM
            this.w_oMSP.Projects.Item(w_PROJECTID).Start = this.w_START_DFLT
            this.w_oMSP.Projects.Item(w_PROJECTID).Finish = this.w_FINISH_DFLT
        endcase
    endcase
    if this.w_PRJREL<>"2003"
      this.w_MESSERR = ah_Msgformat("Impossibile aggiornare la tabella dei progetti")
      this.Page_11()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Aggiornamento della tabella CALENDARS
    do case
      case this.w_MSGBOXCHOICE=6
        * --- Progetto non esistente o si � scelto di sovrascrivere il progetto: preparazione della stringa SQL di inserzione
        do case
          case this.w_PRJREL="98"
            this.w_SQLstring = "Insert into CALENDARS "
            this.w_SQLstring = this.w_SQLstring+"(PROJECTID,CALENDARUNIQUEID,CALENDARNAME) "
            this.w_SQLstring = this.w_SQLstring+"values ("+cp_tostrodbc(this.w_PROJECTID)+","+cp_tostrodbc(this.w_CALENDARID)+","+cp_tostrodbc(alltrim(this.w_CALENDARIO))+")"
          case this.w_PRJREL="2000"
            this.w_SQLstring = "Insert into MSP_CALENDARS "
            this.w_SQLstring = this.w_SQLstring+"(PROJ_ID,CAL_UID,CAL_NAME) "
            this.w_SQLstring = this.w_SQLstring+"values ("+cp_tostrodbc(this.w_PROJECTID)+","+cp_tostrodbc(this.w_CALENDARID)+","+cp_tostrodbc(alltrim(this.w_CALENDARIO))+")"
          case this.w_PRJREL="2003"
            if ALLTRIM(this.w_CALENDARIO)<>"Standard"
              this.w_oMSP.BaseCalendarCreate(this.w_CALENDARIO)     
            endif
        endcase
      case this.w_MSGBOXCHOICE=7
        * --- Si � scelto di aggiornare il progetto: preparazione della stringa SQL di aggiornamento
        do case
          case this.w_PRJREL="98"
            this.w_SQLstring = "Update CALENDARS "
            this.w_SQLstring = this.w_SQLstring+"set CALENDARNAME="+cp_tostrodbc(this.w_CALENDARIO)+" "
            this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)+" "
            this.w_SQLstring = this.w_SQLstring+"and CALENDARUNIQUEID="+cp_tostrodbc(this.w_CALENDARID)
          case this.w_PRJREL="2000"
            this.w_SQLstring = "Update MSP_CALENDARS "
            this.w_SQLstring = this.w_SQLstring+"set CAL_NAME="+cp_tostrodbc(this.w_CALENDARIO)+" "
            this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)+" "
            this.w_SQLstring = this.w_SQLstring+"and CAL_UID="+cp_tostrodbc(this.w_CALENDARID)
        endcase
    endcase
    if this.w_PRJREL<>"2003"
      this.w_MESSERR = "Impossibile aggiornare la tabella dei calendari: %2"
      this.Page_11()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Aggiornamento della tabella MPS_TASK
    if this.w_MSGBOXCHOICE=7
      * --- Prima di procedere alle scritture, se sono in aggiornamento preparo un cursore con i task attualmente presenti in Project
       
 Create Cursor TasksInProject2 (TASK_ID N(4,0), TASK_NEW_ID N(4,0), TASK_WBS C(254), ; 
 TASK_MSPROJECT C(254), TASK_NAME C(20), TASK_TYPE C(1), TASK_TOBEDELETED C(1))
      if this.w_PRJREL<>"2003"
        do case
          case this.w_PRJREL="98"
            this.w_SQLstring = "Select TASKID as TASK_ID, NAME as TASK_MSPROJECT  "
            this.w_SQLstring = this.w_SQLstring+"from TASK_INFORMATION "
            this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID) 
          case this.w_PRJREL="2000"
            * --- Non leggo i TASK messi da Project e vuoti (TASK_ID a NULL) oppure
            *     il TASK parzialmente valorizzato con TASK_ID a 0
            this.w_SQLstring = "Select TASK_UID as TASK_ID, TASK_NAME as TASK_MSPROJECT from MSP_TASKS "
            this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)+" And ( TASK_ID Is Not Null Or  TASK_ID>0)"
        endcase
        SQLEXEC(this.w_ConnHandle, this.w_SQLstring,"TasksInProject")
      else
        CREATE CURSOR TasksInProject(TASK_ID N(10), TASK_MSPROJECT C(50))
        Create Cursor LinksInProject (LINK_ID N(4,0), PRED_ID N(4,0), SUCC_ID N(4,0), LINK_TOBEDELETED C(1))
        this.w_NUMREC = 1
        do while this.w_NUMREC<=this.w_oPRJ.Tasks.count
          this.w_TASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
          this.w_CODICE = this.w_TASK.Name
          this.w_UID = this.w_TASK.UniqueID
          this.w_PREDEC = this.w_TASK.UniqueIDPredecessors
          if !EMPTY(this.w_PREDEC)
            do while ATC(",",this.w_PREDEC)>0
              this.w_NPREDEC = INT(VAL(LEFT(this.w_PREDEC,ATC(",",this.w_PREDEC)-1)))
              INSERT INTO LinksInProject (LINK_ID,PRED_ID,SUCC_ID,LINK_TOBEDELETED) values (1,this.w_NPREDEC,this.w_UID,"D")
              this.w_PREDEC = SUBSTR(this.w_PREDEC,ATC(",",this.w_PREDEC)+1,len(this.w_PREDEC)-ATC(",",this.w_PREDEC)+1)
            enddo
            this.w_NPREDEC = INT(VAL(LEFT(this.w_PREDEC,ATC(",",this.w_PREDEC)-1)))
            INSERT INTO LinksInProject (LINK_ID,PRED_ID,SUCC_ID,LINK_TOBEDELETED) values (1,this.w_NPREDEC,this.w_UID,"D")
          endif
          INSERT INTO TasksInProject(TASK_ID,TASK_MSPROJECT) values (this.w_UID,this.w_CODICE)
          this.w_NUMREC = this.w_NUMREC+1
        enddo
      endif
      * --- Estraggo il codice ed il tipo dell'attivit� come in ahe
      cur=wrcursor("TasksInProject2")
      Select "TasksInProject"
      go top
      do while Not Eof()
        * --- Assegnamenti necessari per evitare problemi con i campi memo
        this.w_TASK_NAME = TASK_MSPROJECT
        this.w_TASKID = TASK_ID
         
 Insert into TasksInProject2 (TASK_ID, TASK_NEW_ID, TASK_WBS, TASK_MSPROJECT,TASK_NAME,TASK_TYPE,TASK_TOBEDELETED) ; 
 values (this.w_TASKID, -1, "???", this.w_TASK_NAME, substr(this.w_TASK_NAME,1,15), substr(this.w_TASK_NAME,rat(")",this.w_TASK_NAME)-1,1),"D")
        Select "TasksInProject"
        if Not Eof()
          Skip
        endif
      enddo
      if this.w_PRJREL<>"2003"
        * --- Preparo anche un cursore con i link di precedenza attualmente presenti in Project (servir� per le TASK_DEPENDENCIES)
        Create Cursor LinksInProject (LINK_ID N(4,0), PRED_ID N(4,0), SUCC_ID N(4,0), LINK_TOBEDELETED C(1))
        do case
          case this.w_PRJREL="98"
            this.w_SQLstring = "Select  DEPENDENCYUNIQUEID as LINK_ID, 'D' as LINK_TOBEDELETED, "
            this.w_SQLstring = this.w_SQLstring+" PREDECESSORTASKUNIQUEID as PRED_ID, SUCCESSORTASKUNIQUEID as SUCC_ID "
            this.w_SQLstring = this.w_SQLstring+"from TASK_DEPENDENCIES "
            this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
          case this.w_PRJREL="2000"
            this.w_SQLstring = "Select  LINK_UID as LINK_ID, 'D' as LINK_TOBEDELETED,  LINK_PRED_UID as PRED_ID, LINK_SUCC_UID as SUCC_ID "
            this.w_SQLstring = this.w_SQLstring+" from MSP_LINKS where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
        endcase
        SQLEXEC(this.w_ConnHandle, this.w_SQLstring,"LinksInProject")
        * --- Inoltre cambio segno all'id dei task per prepararmi agli aggiornamenti evitando problemi di duplicazione di chiave
        do case
          case this.w_PRJREL="98"
            this.w_SQLstring = "Update TASK_INFORMATION set TASKUNQUEID=-TASKUNIQUEID where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
          case this.w_PRJREL="2000"
            this.w_SQLstring = "Update MSP_TASKS set TASK_UID=-TASK_UID where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
        endcase
        * --- Try
        local bErr_03B1E470
        bErr_03B1E470=bTrsErr
        this.Try_03B1E470()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          AddMsgNL ("Impossibile aggiornare la tabella dei task", this)
          * --- Raise
          i_Error="Errore"
          return
        endif
        bTrsErr=bTrsErr or bErr_03B1E470
        * --- End
      endif
    endif
    * --- Aggiorna/ Crea Tasks /  Assegnamenti Risosre
    select("EXPORT")
    Go Top
    do while Not Eof()
      do case
        case this.w_MSGBOXCHOICE=6
          * --- Progetto non esistente o si � scelto di sovrascrivere il progetto: preparazione della stringa SQL di inserzione
          do case
            case this.w_PRJREL="98"
              this.w_SQLstring = "Insert into TASK_INFORMATION "
              this.w_SQLstring = this.w_SQLstring+"(PROJECTID,TASKID,TASKUNIQUEID,NAME,DURATION,"
              this.w_SQLstring = this.w_SQLstring+"OUTLINELEVEL,STARTDATE,FINISHDATE,PERCENTCOMPLETE,"
              this.w_SQLstring = this.w_SQLstring+"CONSTRAINTTYPE,CONSTRAINTDATE,OUTLINENUMBER,"
              this.w_SQLstring = this.w_SQLstring+"MILESTONE,SUMMARY) "
              this.w_SQLstring = this.w_SQLstring+"values "
              this.w_SQLstring = this.w_SQLstring+"("+cp_tostrodbc(PROJECTID)+","+cp_tostrodbc(TASKID)+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(TASKUNIQUEID)+","+cp_tostrodbc(NAME)+","+cp_tostrodbc(DURATION)+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(OUTLINELEVEL)+","+cp_tostrodbc(STARTDATE)+","+cp_tostrodbc(FINISHDATE)+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(PERCENTCOMPLETE)+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(CONSTRAINTTYPE)+","+cp_tostrodbc(CONSTRAINTDATE)+","+cp_tostrodbc(OUTLINENUMBER)+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(MILESTONE)+","+cp_tostrodbc(SUMMARY)+")"
            case this.w_PRJREL="2000"
              this.w_SQLstring = "Insert into MSP_TASKS "
              this.w_SQLstring = this.w_SQLstring+"(PROJ_ID,TASK_ID,TASK_WBS,TASK_UID,TASK_NAME,TASK_DUR,"
              this.w_SQLstring = this.w_SQLstring+"TASK_ACT_DUR,TASK_REM_DUR,EXT_EDIT_REF_DATA,"
              this.w_SQLstring = this.w_SQLstring+"TASK_OUTLINE_LEVEL,TASK_START_DATE,TASK_FINISH_DATE,TASK_PCT_COMP,"
              this.w_SQLstring = this.w_SQLstring+"TASK_CONSTRAINT_TYPE,TASK_CONSTRAINT_DATE,TASK_OUTLINE_NUM,"
              this.w_SQLstring = this.w_SQLstring+"TASK_IS_MILESTONE,TASK_IS_SUMMARY,TASK_DUR_IS_EST,TASK_DUR_FMT) "
              this.w_SQLstring = this.w_SQLstring+"values "
              this.w_SQLstring = this.w_SQLstring+"("+cp_tostrodbc(PROJECTID)+","+cp_tostrodbc(TASKID)+","+cp_tostrodbc(alltrim(OUTLINENUMBER))+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(TASKUNIQUEID)+","+cp_tostrodbc(NAME)+","+cp_tostrodbc(DURATION)+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(ACT_DUR)+","+cp_tostrodbc(REM_DUR)+",'1',"
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(OUTLINELEVEL)+","+cp_tostrodbc(STARTDATE)+","+cp_tostrodbc(FINISHDATE)+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(PERCENTCOMPLETE)+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(CONSTRAINTTYPE)+","+cp_tostrodbc(CONSTRAINTDATE)+","+cp_tostrodbc(OUTLINENUMBER)+","
              this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(MILESTONE)+","+cp_tostrodbc(SUMMARY)+",0,7)"
            case this.w_PRJREL="2003"
              this.w_TASK = this.w_oPRJ.Tasks.Add(NAME)
              this.w_TASK.WBS = OUTLINENUMBER
              this.w_TASK.Duration = iif(Export.MILESTONEAHE="S",0,DURATION)
              this.w_TASK.ActualDuration = iif(Export.MILESTONEAHE="S",0,ACT_DUR)
              this.w_TASK.RemainingDuration = iif(Export.MILESTONEAHE="S",0,REM_DUR)
              this.w_TASK.OutlineLevel = OUTLINELEVEL
              this.w_TASK.TEXT1 = this.w_CODCOM
              if !EMPTY(STARTDATE) AND ATTIPATT="A"
                this.w_TASK.Start = STARTDATE
              endif
              if !EMPTY(FINISHDATE) AND ATTIPATT="A"
                this.w_TASK.Finish = FINISHDATE
              endif
              this.w_TASK.ConstraintType = CONSTRAINTTYPE
              this.w_TASK.PercentComplete = PERCENTCOMPLETE
              if !EMPTY(NVL(CONSTRAINTDATE,CTOD("  -  -    ")))
                this.w_TASK.ConstraintDate = CONSTRAINTDATE
              endif
              this.w_TASK.OutlineNumber = ALLTRIM(OUTLINENUMBER)
              this.w_TASK.Milestone = iif(Export.MILESTONEAHE="S",.t.,.f.)
              this.w_TASK.Summary = SUMMARY
              this.w_TASK.Estimated = 0
              this.w_oERRORLOG.AddMsgLOG("Inserito TASK %1%0", alltrim(NAME))     
          endcase
          if this.w_PRJREL<>"2003"
            * --- Try
            local bErr_03A83B10
            bErr_03A83B10=bTrsErr
            this.Try_03A83B10()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              AddMsgNL ("Impossibile aggiornare la tabella dei task", this)
              * --- Raise
              i_Error="Errore"
              return
            endif
            bTrsErr=bTrsErr or bErr_03A83B10
            * --- End
          endif
        case this.w_MSGBOXCHOICE=7
          * --- Si � scelto di aggiornare il progetto - Verifico se il task considerato esiste gi� in Project
          this.w_TASK_NAME = substr(NAME,1,15)
          this.w_TIPAT = substr(NAME,rat(")",NAME)-1,1)
          this.w_NEW_TASKID = TASKID
          this.w_WBS = OUTLINENUMBER
          select "TasksInProject2"
          go top
          locate for TASK_MSPROJECT=EXPORT.NAME and TASK_TYPE=this.w_TIPAT
          this.w_FOUND = Found()
          if this.w_FOUND
            * --- Se il task � gi� presente in Project prelevo il vecchio ID e marco il task per l'aggiornamento
            *     (NOTA: i task presenti in Project ma non in EXPORT rimarranno marcati a 'D' per la cancellazione)
            this.w_TASKID = TASK_ID
            Replace TASK_TOBEDELETED with "U", TASK_NEW_ID with this.w_NEW_TASKID, TASK_WBS with this.w_WBS
            * --- NOTA: associo anche il nuovo id (questa seconda parte serve per gestire l'aggiorn.to dei legami di precedenza a pag.10
            *     Inoltre annoto il codice WBS che servir� a pag.10 per gestire correttamente i link che coinvolgono attivit� summary)
          endif
          select "EXPORT"
          if this.w_FOUND
            * --- Se il task era gi� presente preparo la query per l'aggiornamento
            *     (nota: viene aggiornato anche l'ID che nel frattempo potrebbe essere cambiato)
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "Update TASK_INFORMATION "
                this.w_SQLstring = this.w_SQLstring+"set NAME="+cp_tostrodbc(NAME)+","
                this.w_SQLstring = this.w_SQLstring+"TASKID="+cp_tostrodbc(TASKID)+","
                this.w_SQLstring = this.w_SQLstring+"TASKUNIQUEID="+cp_tostrodbc(TASKUNIQUEID)+","
                this.w_SQLstring = this.w_SQLstring+"DURATION="+cp_tostrodbc(DURATION)+","
                this.w_SQLstring = this.w_SQLstring+"OUTLINELEVEL="+cp_tostrodbc(OUTLINELEVEL)+","
                this.w_SQLstring = this.w_SQLstring+"STARTDATE="+cp_tostrodbc(STARTDATE)+","
                this.w_SQLstring = this.w_SQLstring+"FINISHDATE="+cp_tostrodbc(FINISHDATE)+","
                this.w_SQLstring = this.w_SQLstring+"PERCENTCOMPLETE="+cp_tostrodbc(PERCENTCOMPLETE)+","
                this.w_SQLstring = this.w_SQLstring+"CONSTRAINTTYPE="+cp_tostrodbc(CONSTRAINTTYPE)+","
                this.w_SQLstring = this.w_SQLstring+"CONSTRAINTDATE="+cp_tostrodbc(CONSTRAINTDATE)+","
                this.w_SQLstring = this.w_SQLstring+"OUTLINENUMBER="+cp_tostrodbc(OUTLINENUMBER)+","
                this.w_SQLstring = this.w_SQLstring+"MILESTONE="+cp_tostrodbc(MILESTONE)+","
                this.w_SQLstring = this.w_SQLstring+"SUMMARY="+cp_tostrodbc(SUMMARY)+" "
                this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)+ " "
                this.w_SQLstring = this.w_SQLstring+"and TASKUNIQUEID=-"+cp_tostrodbc(this.w_TASKID)
              case this.w_PRJREL="2000"
                * --- Costruisco e aggiorno il TASK, come primo passo aggiorno il campo EXT_EDIT_REF_DATA.
                if this.w_TIPODB="SQLSERVER"
                  this.w_SQLstring = "Execute MSP_BACKUP_TASK '"+ cp_tostrodbc(this.w_PROJECTID)+"','"+cp_tostrodbc(this.w_TASKID)+"'"
                else
                  this.w_SQLstring = "CALL MSP_BACKUP_TASK '"+ cp_tostrodbc(this.w_PROJECTID)+"','"+cp_tostrodbc(this.w_TASKID)+ "')"
                endif
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                this.w_SQLstring = "Update MSP_TASKS "
                this.w_SQLstring = this.w_SQLstring+"set TASK_NAME="+cp_tostrodbc(NAME)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_ID="+cp_tostrodbc(TASKID)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_WBS="+cp_tostrodbc(alltrim(OUTLINENUMBER))+","
                this.w_SQLstring = this.w_SQLstring+"TASK_UID="+cp_tostrodbc(TASKUNIQUEID)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_DUR="+cp_tostrodbc(DURATION)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_ACT_DUR="+cp_tostrodbc(ACT_DUR)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_REM_DUR="+cp_tostrodbc(REM_DUR)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_OUTLINE_LEVEL="+cp_tostrodbc(Alltrim(OUTLINELEVEL))+","
                this.w_SQLstring = this.w_SQLstring+"TASK_START_DATE="+cp_tostrodbc(STARTDATE)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_FINISH_DATE="+cp_tostrodbc(FINISHDATE)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_PCT_COMP="+cp_tostrodbc(PERCENTCOMPLETE)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_CONSTRAINT_TYPE="+cp_tostrodbc(CONSTRAINTTYPE)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_CONSTRAINT_DATE="+cp_tostrodbc(CONSTRAINTDATE)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_OUTLINE_NUM="+cp_tostrodbc(OUTLINENUMBER)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_IS_MILESTONE="+cp_tostrodbc(MILESTONE)+","
                this.w_SQLstring = this.w_SQLstring+"TASK_IS_SUMMARY="+cp_tostrodbc(SUMMARY)+" "
                this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)+ " "
                this.w_SQLstring = this.w_SQLstring+"and TASK_UID=-"+cp_tostrodbc(this.w_TASKID)
              case this.w_PRJREL="2003"
                this.w_TASK = this.w_oPRJ.Tasks.UniqueID(this.w_TASKID)
                this.w_TASK.WBS = OUTLINENUMBER
                this.w_TASK.Duration = iif(Export.MILESTONEAHE="S",0,DURATION)
                this.w_TASK.ActualDuration = iif(Export.MILESTONEAHE="S",0,ACT_DUR)
                this.w_TASK.RemainingDuration = iif(Export.MILESTONEAHE="S",0,REM_DUR)
                this.w_TASK.OutlineLevel = OUTLINELEVEL
                this.w_TASK.PercentComplete = PERCENTCOMPLETE
                this.w_TASK.ConstraintType = CONSTRAINTTYPE
                if !EMPTY(NVL(CONSTRAINTDATE,CTOD("  -  -    ")))
                  this.w_TASK.ConstraintDate = CONSTRAINTDATE
                endif
                if this.w_PROGR="I"
                  if !EMPTY(FINISHDATE) AND ATTIPATT="A"
                    this.w_TASK.Finish = FINISHDATE
                  endif
                  if !EMPTY(STARTDATE) AND ATTIPATT="A"
                    this.w_TASK.Start = CP_TODATE("  -  -    ")
                  endif
                else
                  if !EMPTY(STARTDATE) AND ATTIPATT="A"
                    this.w_TASK.Start = STARTDATE
                  endif
                  if !EMPTY(FINISHDATE) AND ATTIPATT="A"
                    this.w_TASK.Finish = CP_TODATE("  -  -    ")
                  endif
                endif
                this.w_TASK.OutlineNumber = ALLTRIM(OUTLINENUMBER)
                this.w_TASK.Milestone = iif(Export.MILESTONEAHE="S",.t.,.f.)
                this.w_TASK.Summary = SUMMARY
                this.w_TASK.Estimated = 0
                this.w_TASK.RemainingDuration = REM_DUR
                this.w_oERRORLOG.ADDMSGLOG("Aggiornato TASK %1", alltrim(NAME))     
            endcase
            if this.w_PRJREL<>"2003"
              * --- Try
              local bErr_03ABA5A0
              bErr_03ABA5A0=bTrsErr
              this.Try_03ABA5A0()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                AddMsgNL("Impossibile aggiornare la tabella dei task", this)
                * --- Raise
                i_Error="Errore"
                return
              endif
              bTrsErr=bTrsErr or bErr_03ABA5A0
              * --- End
            endif
            * --- Aggiorno anche gli assegnamenti di risorse
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "Update ASSIGNMENT_INFORMATION set TASKUNIQUEID="+cp_tostrodbc(TASKID)+" "
                this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)+ " "
                this.w_SQLstring = this.w_SQLstring+"and TASKUNIQUEID= - "+cp_tostrodbc(this.w_TASKID)
              case this.w_PRJREL="2000"
                * --- Costruisco e aggiorno l'assegnamento, come primo passo aggiorno il campo EXT_EDIT_REF_DATA.
                if this.w_TIPODB="SQLSERVER"
                  this.w_SQLstring = "Execute MSP_BACKUP_ASSN '"+ cp_tostrodbc(this.w_PROJECTID)+"','"+cp_tostrodbc(this.w_TASKID)+"'"
                else
                  this.w_SQLstring = "CALL MSP_BACKUP_ASSN '"+ cp_tostrodbc(this.w_PROJECTID)+"','"+cp_tostrodbc(this.w_TASKID)+ "')"
                endif
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                this.w_SQLstring = "Update MSP_ASSIGNMENTS set TASK_UID="+cp_tostrodbc(TASKID)+" "
                this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)+ " "
                this.w_SQLstring = this.w_SQLstring+"and TASK_UID= - "+cp_tostrodbc(this.w_TASKID)
            endcase
            if this.w_PRJREL<>"2003"
              * --- Try
              local bErr_03AB3A00
              bErr_03AB3A00=bTrsErr
              this.Try_03AB3A00()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                AddMsgNL("Impossibile aggiornare la tabella degli assegnamenti di risorse", this)
                * --- Raise
                i_Error="Errore"
                return
              endif
              bTrsErr=bTrsErr or bErr_03AB3A00
              * --- End
            endif
          else
            * --- Se il task non era presente deve essere inserito
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "Insert into TASK_INFORMATION "
                this.w_SQLstring = this.w_SQLstring+"(PROJECTID,TASKID,TASKUNIQUEID,NAME,DURATION,"
                this.w_SQLstring = this.w_SQLstring+"OUTLINELEVEL,STARTDATE,FINISHDATE,PERCENTCOMPLETE,"
                this.w_SQLstring = this.w_SQLstring+"CONSTRAINTTYPE,CONSTRAINTDATE,OUTLINENUMBER,"
                this.w_SQLstring = this.w_SQLstring+"MILESTONE,SUMMARY) "
                this.w_SQLstring = this.w_SQLstring+"values "
                this.w_SQLstring = this.w_SQLstring+"("+cp_tostrodbc(PROJECTID)+","+cp_tostrodbc(TASKID)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(TASKUNIQUEID)+","+cp_tostrodbc(NAME)+","+cp_tostrodbc(DURATION)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(OUTLINELEVEL)+","+cp_tostrodbc(STARTDATE)+","+cp_tostrodbc(FINISHDATE)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(PERCENTCOMPLETE)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(CONSTRAINTTYPE)+","+cp_tostrodbc(CONSTRAINTDATE)+","+cp_tostrodbc(OUTLINENUMBER)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(MILESTONE)+","+cp_tostrodbc(SUMMARY)+")"
              case this.w_PRJREL="2000"
                this.w_SQLstring = "Insert into MSP_TASKS "
                this.w_SQLstring = this.w_SQLstring+"(PROJ_ID,TASK_ID,TASK_WBS,TASK_UID,TASK_NAME,TASK_DUR,"
                this.w_SQLstring = this.w_SQLstring+"TASK_ACT_DUR,TASK_REM_DUR,"
                this.w_SQLstring = this.w_SQLstring+"TASK_OUTLINE_LEVEL,TASK_START_DATE,TASK_FINISH_DATE,TASK_PCT_COMP,"
                this.w_SQLstring = this.w_SQLstring+"TASK_CONSTRAINT_TYPE,TASK_CONSTRAINT_DATE,TASK_OUTLINE_NUM,"
                this.w_SQLstring = this.w_SQLstring+"TASK_IS_MILESTONE,TASK_IS_SUMMARY,TASK_DUR_IS_EST,TASK_DUR_FMT,EXT_EDIT_REF_DATA) "
                this.w_SQLstring = this.w_SQLstring+"values ("
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(PROJECTID)+","+cp_tostrodbc(TASKID)+","+cp_tostrodbc(Alltrim(OUTLINENUMBER))+","+cp_tostrodbc(TASKUNIQUEID)+","+cp_tostrodbc(NAME)+","+cp_tostrodbc(DURATION)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(ACT_DUR)+","+cp_tostrodbc(REM_DUR)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(OUTLINELEVEL)+","+cp_tostrodbc(STARTDATE)+","+cp_tostrodbc(FINISHDATE)+","+cp_tostrodbc(PERCENTCOMPLETE)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(CONSTRAINTTYPE)+","+cp_tostrodbc(CONSTRAINTDATE)+","+cp_tostrodbc(Alltrim(OUTLINENUMBER))+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(MILESTONE)+","+cp_tostrodbc(SUMMARY)+",0,7,'1')"
              case this.w_PRJREL="2003"
                this.w_TASK = this.w_oPRJ.Tasks.Add(NAME)
                this.w_TASK.Project = PROJECTID
                this.w_TASK.WBS = OUTLINENUMBER
                this.w_TASK.Duration = iif(MILESTONE=1,0,DURATION)
                this.w_TASK.ActualDuration = iif(MILESTONE=1,0,ACT_DUR)
                this.w_TASK.RemainingDuration = iif(MILESTONE=1,0,REM_DUR)
                this.w_TASK.OutlineLevel = OUTLINELEVEL
                this.w_TASK.Start = STARTDATE
                this.w_TASK.Finish = FINISHDATE
                this.w_TASK.PercentComplete = PERCENTCOMPLETE
                this.w_TASK.ConstraintType = CONSTRAINTTYPE
                this.w_TASK.ConstraintDate = CONSTRAINTDATE
                this.w_TASK.OutlineNumber = OUTLINENUMBER
                this.w_TASK.Milestone = MILESTONE
                this.w_TASK.Summary = SUMMARY
                this.w_TASK.Estimated = 0
                this.w_TASK.RemainingDuration = REM_DUR
                this.w_oERRORLOG.AddMsgLOG("Inserito TASK %1%0", alltrim(NAME))     
            endcase
            if this.w_PRJREL<>"2003"
              this.w_MESSERR = "Impossibile inserire %1 nell'elenco dei tasks: %2"
              this.Page_11()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
      endcase
      select("EXPORT")
      if Not Eof()
        Skip
      endif
    enddo
    * --- su export
    if this.w_MSGBOXCHOICE=7
      * --- Se sono in aggiornamento, cancello dal database di Project tutti i task marcati che non sono stati esportati di nuovo
      SELECT "TasksInProject2"
      go top
      scan for TASK_TOBEDELETED="D"
      * --- N.B.: la cancellazione va fatta sul nome (stringa Project), perch� l'ID nel frattempo � stato assegnato a qualcun altro
      this.w_TASK_NAME = TASK_MSPROJECT
      do case
        case this.w_PRJREL="98"
          this.w_SQLstring = "Delete from TASK_INFORMATION "
          this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)+" and NAME="+cp_tostrodbc(this.w_TASK_NAME)
        case this.w_PRJREL="2000"
          this.w_SQLstring = "Delete from MSP_TASKS "
          this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)+" and TASK_NAME="+cp_tostrodbc(this.w_TASK_NAME)
      endcase
      if this.w_PRJREL<>"2003"
        this.w_MESSERR = "Impossibile cancellare dalla tabella dei tasks: %2"
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      SELECT "TasksInProject2"
      endscan
      * --- Inoltre cancello anche gli assegnamenti di risorse relativi a task non pi� esportati in Project e le righe null create da Project
      do case
        case this.w_PRJREL="98"
          this.w_SQLstring = "Delete from ASSIGNMENT_INFORMATION "
          this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)+" and (TASKUNIQUEID<0 or RESOURCEUNIQUEID<0) "
        case this.w_PRJREL="2000"
          this.w_SQLstring = "Delete from MSP_ASSIGNMENTS "
          this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)+" and (TASK_UID<0 or RES_UID<0) "
      endcase
      if this.w_PRJREL<>"2003"
        this.w_MESSERR = "Impossibile cancellare dalla tabella MSP_ASSIGNMENTS: %2"
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Aggiornamento della tabella TASK_DEPENDENCIES
    *     (spostato a pag.10 per ragioni di spazio nella pagina del PAINTER C/S)
    this.Page_10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_03B1E470()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
    return
  proc Try_03A83B10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
    return
  proc Try_03ABA5A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
    return
  proc Try_03AB3A00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
    return


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Effettuo gli aggiornamenti al database (IMPORT)
    * --- Scrittura dati relativi alla tabella PROJECT_INFORMATION
    if this.w_PRJREL<>"2003"
      select "Curs1Project"
      go top
      this.w_STARTDATE = STARTDATE
      this.w_FINISHDATE = FINISHDATE
      this.w_PROJECTID = PROJECTID
    else
      this.w_STARTDATE = this.w_oPRJ.Start
      this.w_FINISHDATE = this.w_oPRJ.Finish
    endif
    * --- Scrittura dati relativi alla tabella TASK_INFORMATION
    select "Curs2Project"
    go top
    SCAN
    this.w_CODCOM = ATCODCOM
    this.w_TASK_NAME = ATCODATT
    this.w_TASKID = AT__RIGA
    this.w_DURATION = ATDURGIO
    this.w_STARTDATE = ATDATINI
    this.w_FINISHDATE = ATDATFIN
    this.w_PERCENTCOMPL = ATPERCOM
    this.w_CONSTYPE = ATVINCOL
    this.w_CONSTDATE = ATDATVIN
    this.w_MILESTONE = ATCARDIN
    this.w_TIPAT = ATTIPATT
    * --- Write into CAN_TIER
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAN_TIER_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CNDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_STARTDATE),'CAN_TIER','CNDATINI');
      +",CNDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_FINISHDATE),'CAN_TIER','CNDATFIN');
          +i_ccchkf ;
      +" where ";
          +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
             )
    else
      update (i_cTable) set;
          CNDATINI = this.w_STARTDATE;
          ,CNDATFIN = this.w_FINISHDATE;
          &i_ccchkf. ;
       where;
          CNCODCAN = this.w_CODCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Impossibile aggiornare la tabella CAN_TIER'
      return
    endif
    if this.w_TIPAT="A"
      * --- LEGGE LO STATO DELL'ATTIVITA' NELLA VARIABILE w_ATSTATO
      * --- Read from ATTIVITA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AT_STATO"+;
          " from "+i_cTable+" ATTIVITA where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
              +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPAT);
              +" and ATCODATT = "+cp_ToStrODBC(this.w_TASK_NAME);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AT_STATO;
          from (i_cTable) where;
              ATCODCOM = this.w_CODCOM;
              and ATTIPATT = this.w_TIPAT;
              and ATCODATT = this.w_TASK_NAME;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ATSTATO = NVL(cp_ToDate(_read_.AT_STATO),cp_NullValue(_read_.AT_STATO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se � un attivit� devo settare la percentuale di completamento
      *     nei casi in cui essa sia editabile in funzione dello stato.
      if this.w_ATSTATO$"ZLF"
        * --- Aggiorna anche ATPERCOM
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AT__RIGA ="+cp_NullLink(cp_ToStrODBC(this.w_TASKID),'ATTIVITA','AT__RIGA');
          +",ATDURGIO ="+cp_NullLink(cp_ToStrODBC(this.w_DURATION),'ATTIVITA','ATDURGIO');
          +",ATDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_STARTDATE),'ATTIVITA','ATDATINI');
          +",ATDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_FINISHDATE),'ATTIVITA','ATDATFIN');
          +",ATPERCOM ="+cp_NullLink(cp_ToStrODBC(this.w_PERCENTCOMPL),'ATTIVITA','ATPERCOM');
          +",ATVINCOL ="+cp_NullLink(cp_ToStrODBC(this.w_CONSTYPE),'ATTIVITA','ATVINCOL');
          +",ATDATVIN ="+cp_NullLink(cp_ToStrODBC(this.w_CONSTDATE),'ATTIVITA','ATDATVIN');
          +",ATCARDIN ="+cp_NullLink(cp_ToStrODBC(this.w_MILESTONE),'ATTIVITA','ATCARDIN');
              +i_ccchkf ;
          +" where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
              +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPAT);
              +" and ATCODATT = "+cp_ToStrODBC(this.w_TASK_NAME);
                 )
        else
          update (i_cTable) set;
              AT__RIGA = this.w_TASKID;
              ,ATDURGIO = this.w_DURATION;
              ,ATDATINI = this.w_STARTDATE;
              ,ATDATFIN = this.w_FINISHDATE;
              ,ATPERCOM = this.w_PERCENTCOMPL;
              ,ATVINCOL = this.w_CONSTYPE;
              ,ATDATVIN = this.w_CONSTDATE;
              ,ATCARDIN = this.w_MILESTONE;
              &i_ccchkf. ;
           where;
              ATCODCOM = this.w_CODCOM;
              and ATTIPATT = this.w_TIPAT;
              and ATCODATT = this.w_TASK_NAME;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Impossibile aggiornare la tabella ATTIVITA'
          return
        endif
      else
        * --- NON AGGIORNA ATPERCOM
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AT__RIGA ="+cp_NullLink(cp_ToStrODBC(this.w_TASKID),'ATTIVITA','AT__RIGA');
          +",ATDURGIO ="+cp_NullLink(cp_ToStrODBC(this.w_DURATION),'ATTIVITA','ATDURGIO');
          +",ATDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_STARTDATE),'ATTIVITA','ATDATINI');
          +",ATDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_FINISHDATE),'ATTIVITA','ATDATFIN');
          +",ATVINCOL ="+cp_NullLink(cp_ToStrODBC(this.w_CONSTYPE),'ATTIVITA','ATVINCOL');
          +",ATDATVIN ="+cp_NullLink(cp_ToStrODBC(this.w_CONSTDATE),'ATTIVITA','ATDATVIN');
          +",ATCARDIN ="+cp_NullLink(cp_ToStrODBC(this.w_MILESTONE),'ATTIVITA','ATCARDIN');
              +i_ccchkf ;
          +" where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
              +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPAT);
              +" and ATCODATT = "+cp_ToStrODBC(this.w_TASK_NAME);
                 )
        else
          update (i_cTable) set;
              AT__RIGA = this.w_TASKID;
              ,ATDURGIO = this.w_DURATION;
              ,ATDATINI = this.w_STARTDATE;
              ,ATDATFIN = this.w_FINISHDATE;
              ,ATVINCOL = this.w_CONSTYPE;
              ,ATDATVIN = this.w_CONSTDATE;
              ,ATCARDIN = this.w_MILESTONE;
              &i_ccchkf. ;
           where;
              ATCODCOM = this.w_CODCOM;
              and ATTIPATT = this.w_TIPAT;
              and ATCODATT = this.w_TASK_NAME;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Impossibile aggiornare la tabella ATTIVITA'
          return
        endif
        if this.w_PERCENTCOMPL<>0
          * --- LA PERCENTUALE DI COMPLETAMENTO E' STATA
          *     IMPOSTATA DA PRJECT, MA NON PUO' ESSERE UTILIZZATA.
          this.w_oERRORLOG.AddMsgLOG("Attivit� %1:%0" , alltrim(this.w_TASK_NAME))     
          this.w_oERRORLOG.AddMsgLOG("La percentuale di completamento impostata in MSProject (%1%) non pu� essere riportata in ad hoc %2 perch� l'attivit� non � ancora stata pianificata" , alltrim( str( this.w_PERCENTCOMPL ) ),iif(isAHE(),"Enterprise","Revolution"))     
          this.w_oERRORLOG.AddMsgLOG(SPACE( 250 ))     
        endif
      endif
    else
      * --- Se � una macroattivit� scrivo direttamente i dati senza particolari considerazioni su stato e percentuale di avanzamento
      * --- Write into ATTIVITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AT__RIGA ="+cp_NullLink(cp_ToStrODBC(this.w_TASKID),'ATTIVITA','AT__RIGA');
        +",ATDURGIO ="+cp_NullLink(cp_ToStrODBC(this.w_DURATION),'ATTIVITA','ATDURGIO');
        +",ATDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_STARTDATE),'ATTIVITA','ATDATINI');
        +",ATDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_FINISHDATE),'ATTIVITA','ATDATFIN');
        +",ATPERCOM ="+cp_NullLink(cp_ToStrODBC(this.w_PERCENTCOMPL),'ATTIVITA','ATPERCOM');
        +",ATVINCOL ="+cp_NullLink(cp_ToStrODBC(this.w_CONSTYPE),'ATTIVITA','ATVINCOL');
        +",ATDATVIN ="+cp_NullLink(cp_ToStrODBC(this.w_CONSTDATE),'ATTIVITA','ATDATVIN');
        +",ATCARDIN ="+cp_NullLink(cp_ToStrODBC(this.w_MILESTONE),'ATTIVITA','ATCARDIN');
            +i_ccchkf ;
        +" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPAT);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_TASK_NAME);
               )
      else
        update (i_cTable) set;
            AT__RIGA = this.w_TASKID;
            ,ATDURGIO = this.w_DURATION;
            ,ATDATINI = this.w_STARTDATE;
            ,ATDATFIN = this.w_FINISHDATE;
            ,ATPERCOM = this.w_PERCENTCOMPL;
            ,ATVINCOL = this.w_CONSTYPE;
            ,ATDATVIN = this.w_CONSTDATE;
            ,ATCARDIN = this.w_MILESTONE;
            &i_ccchkf. ;
         where;
            ATCODCOM = this.w_CODCOM;
            and ATTIPATT = this.w_TIPAT;
            and ATCODATT = this.w_TASK_NAME;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Impossibile aggiornare la tabella ATTIVITA'
        return
      endif
    endif
    select "Curs2Project"
    ENDSCAN
    * --- su Curs2Project
    * --- Scrittura dati relativi alla tabella TASK_DEPENDENCIES
    * --- I vecchi dati relativi alle precedenze del progetto vengono eliminati
    * --- Delete from ATT_PREC
    i_nConn=i_TableProp[this.ATT_PREC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MPCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
             )
    else
      delete from (i_cTable) where;
            MPCODCOM = this.w_CODCOM;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Impossibile eliminare i vecchi dati in ATT_PREC'
      return
    endif
    select "Curs3Project"
    go top
    SCAN
    this.w_SUCC_NAME = MPCODATT
    this.w_PRED_NAME = MPATTPRE
    this.w_LAGTYPE = iif(empty(MPTIPPRE),"FI",MPTIPPRE)
    this.w_LAG = MTRITAR
    * --- Insert into ATT_PREC
    i_nConn=i_TableProp[this.ATT_PREC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATT_PREC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MPCODCOM"+",MPTIPATT"+",MPCODATT"+",MPATTPRE"+",MPTIPPRE"+",MTRITAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'ATT_PREC','MPCODCOM');
      +","+cp_NullLink(cp_ToStrODBC("A"),'ATT_PREC','MPTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SUCC_NAME),'ATT_PREC','MPCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRED_NAME),'ATT_PREC','MPATTPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LAGTYPE),'ATT_PREC','MPTIPPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LAG),'ATT_PREC','MTRITAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MPCODCOM',this.w_CODCOM,'MPTIPATT',"A",'MPCODATT',this.w_SUCC_NAME,'MPATTPRE',this.w_PRED_NAME,'MPTIPPRE',this.w_LAGTYPE,'MTRITAR',this.w_LAG)
      insert into (i_cTable) (MPCODCOM,MPTIPATT,MPCODATT,MPATTPRE,MPTIPPRE,MTRITAR &i_ccchkf. );
         values (;
           this.w_CODCOM;
           ,"A";
           ,this.w_SUCC_NAME;
           ,this.w_PRED_NAME;
           ,this.w_LAGTYPE;
           ,this.w_LAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Impossibile inserire nella tabella ATT_PREC'
      return
    endif
    ENDSCAN
    * --- su Curs2Project
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Preparazione dati relativi alla tabella TASK_DEPENDENCIES
    create cursor Curs3Project ;
    (MPCODCOM C(15), MPTIPATT C(1), MPCODATT C(15), MPATTPRE C(15), MPTIPPRE C(2), MTRITAR N(6,0))
    cur=wrcursor("Curs3Project")
    create cursor IMP_PRED ;
    (PRED_NAME C(15))
    cur=wrcursor("IMP_PRED")
    create cursor IMP_SUCC ;
    (SUCC_NAME C(15))
    cur=wrcursor("IMP_SUCC")
    do case
      case this.w_PRJREL="98"
        this.w_SQLstring = "select PREDECESSORTASKUNIQUEID,SUCCESSORTASKUNIQUEID, "
        this.w_SQLstring = this.w_SQLstring+"LINKTYPE,LINKLAG "
        this.w_SQLstring = this.w_SQLstring+"from TASK_DEPENDENCIES "
        this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
        this.w_SQLstring = this.w_SQLstring+" and PREDECESSORTASKUNIQUEID is not NULL"
        this.w_SQLstring = this.w_SQLstring+" and SUCCESSORTASKUNIQUEID is not NULL"
      case this.w_PRJREL="2000"
        this.w_SQLstring = "select LINK_PRED_UID as PREDECESSORTASKUNIQUEID, "
        this.w_SQLstring = this.w_SQLstring+"LINK_SUCC_UID as SUCCESSORTASKUNIQUEID,  "
        this.w_SQLstring = this.w_SQLstring+"LINK_TYPE as LINKTYPE,LINK_LAG as LINKLAG "
        this.w_SQLstring = this.w_SQLstring+"from MSP_LINKS "
        this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
        this.w_SQLstring = this.w_SQLstring+" and LINK_PRED_UID is not NULL"
        this.w_SQLstring = this.w_SQLstring+" and LINK_SUCC_UID is not NULL"
      case this.w_PRJREL="2003"
        CREATE CURSOR CurPreds (PREDECESSORTASKUNIQUEID N(4), SUCCESSORTASKUNIQUEID N(4),LINKTYPE C(2),LINKLAG N(4), COMMESSA C(15))
        this.w_NUMREC = 1
        do while this.w_NUMREC<=this.w_oPRJ.Tasks.count
          this.w_TASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
          this.w_CODCOM = this.w_TASK.TEXT1
          this.w_CODICE = this.w_TASK.Name
          this.w_UID = this.w_TASK.UniqueID
          this.w_PREDEC = this.w_TASK.UniqueIDPredecessors
          if !EMPTY(this.w_PREDEC)
            do while ATC(";",this.w_PREDEC)>0
              this.w_STRPR = LEFT(this.w_PREDEC,ATC(";",this.w_PREDEC)-1)
              this.w_POSITION = AT("+",this.w_STRPR)
              if this.w_POSITION>0
                this.w_SEGNO = "+"
                this.w_TYPEPRED = SUBSTR(this.w_STRPR,this.w_POSITION-2,2)
                if !this.w_TYPEPRED$"II-FI-IF-FF"
                  this.w_TYPEPRED = ""
                endif
                this.w_RITDAYS = INT(val(substr(this.w_STRPR,this.w_POSITION+1, at("g",this.w_STRPR)-this.w_POSITION-1)))
              else
                this.w_POSITION = AT("-",this.w_STRPR)
                if this.w_POSITION>0
                  this.w_SEGNO = "-"
                  this.w_TYPEPRED = SUBSTR(this.w_STRPR,this.w_POSITION-2,2)
                  if !this.w_TYPEPRED$"II-FI-IF-FF"
                    this.w_TYPEPRED = ""
                  endif
                  this.w_RITDAYS = INT(val(substr(this.w_STRPR,this.w_POSITION+1, at("g",this.w_STRPR)-this.w_POSITION-1)))
                else
                  this.w_TYPEPRED = RIGHT(this.w_STRPR,2)
                  if !this.w_TYPEPRED$"II-FI-IF-FF"
                    this.w_TYPEPRED = ""
                  endif
                  this.w_RITDAYS = 0
                endif
              endif
              this.w_NPREDEC = INT(VAL(this.w_STRPR))
              INSERT INTO CurPreds (PREDECESSORTASKUNIQUEID,SUCCESSORTASKUNIQUEID,LINKTYPE,LINKLAG, COMMESSA) values (this.w_NPREDEC,this.w_UID,this.w_TYPEPRED,iif(this.w_SEGNO="-",-this.w_RITDAYS,this.w_RITDAYS), this.w_CODCOM)
              this.w_PREDEC = SUBSTR(this.w_PREDEC,ATC(";",this.w_PREDEC)+1,len(this.w_PREDEC)-ATC(";",this.w_PREDEC)+1)
            enddo
            this.w_STRPR = STRTRAN(this.w_PREDEC,",","")
            this.w_NPREDEC = INT(VAL(this.w_STRPR))
            this.w_POSITION = AT("+",this.w_STRPR)
            if this.w_POSITION>0
              this.w_SEGNO = "+"
              this.w_TYPEPRED = SUBSTR(this.w_STRPR,this.w_POSITION-2,2)
              if !this.w_TYPEPRED$"II-FI-IF-FF"
                this.w_TYPEPRED = ""
              endif
              this.w_RITDAYS = INT(val(substr(this.w_STRPR,this.w_POSITION+1, at("g",this.w_STRPR)-this.w_POSITION-1)))
            else
              this.w_POSITION = AT("-",this.w_STRPR)
              if this.w_POSITION>0
                this.w_SEGNO = "-"
                this.w_TYPEPRED = SUBSTR(this.w_STRPR,this.w_POSITION-2,2)
                if !this.w_TYPEPRED$"II-FI-IF-FF"
                  this.w_TYPEPRED = ""
                endif
                this.w_RITDAYS = INT(val(substr(this.w_STRPR,this.w_POSITION+1, at("g",this.w_STRPR)-this.w_POSITION-1)))
              else
                this.w_TYPEPRED = RIGHT(this.w_STRPR,2)
                if !this.w_TYPEPRED$"II-FI-IF-FF"
                  this.w_TYPEPRED = ""
                endif
                this.w_RITDAYS = 0
              endif
            endif
            INSERT INTO CurPreds (PREDECESSORTASKUNIQUEID,SUCCESSORTASKUNIQUEID,LINKTYPE,LINKLAG,COMMESSA) values (this.w_NPREDEC,this.w_UID,this.w_TYPEPRED,iif(this.w_SEGNO="-",-this.w_RITDAYS,this.w_RITDAYS),this.w_CODCOM)
          endif
          this.w_NUMREC = this.w_NUMREC+1
        enddo
    endcase
    if this.w_PRJREL<>"2003"
      SQLEXEC(this.w_ConnHandle, this.w_SQLstring,"CurPreds")
    endif
    select ("CurPreds")
    go top
    scan
    * --- risalgo dagli id ai task name: se entrambi funzionano il legame pu� essere riportato in AHE
    this.w_ELABORA = "S"
    * --- PREDECESSORE
    * --- Risalgo al NAME (ATCODATT) da cercare in AHE nel cursore delle TASK_INFORMATION (EXPORT) attraverso il TASKID
    this.w_IDPRED = PREDECESSORTASKUNIQUEID
    position=recno()
    select "EXPORT"
    go top
    locate for TASKID=this.w_IDPRED
    if found()
      this.w_TASK_NAME = substr(NAME,1,15)
      this.w_TIPAT = substr(NAME,rat(")",NAME)-1,1)
      this.w_TIPAT_PRE = this.w_TIPAT
    else
      AH_Msg("Questo non dovrebbe mai accadere")
    endif
    select "CurPreds"
    go position
    Vq_Exec("..\comm\exe\query\GSPC2BTP.vqr",this,"cursalfa")
    select "cursalfa"
    if reccount()=0
      this.w_MESS = "Il task %1 non esiste in %2 e non pu� essere gestito. Continuare lo stesso?"
      if not ah_yesno(this.w_MESS,"", alltrim(this.w_TASK_NAME),iif(isAHE(),"Enterprise","Revolution"))
        * --- Tutti i dati relativi al progetto vengono ignorati
        if this.w_PRJREL<>"2003"
          SQLDISCONNECT(this.w_ConnHandle)
        endif
        * --- Rilascio la connessione alla fonte dati ODBC
        AddMsgNL("Importazione terminata dall'utente",this)
        * --- Raise
        i_Error="Errore"
        return
        i_retcode = 'stop'
        return
      else
        * --- Vengono ignorati solo i dati relativi al task inammissibile in AHE
        AH_Msg("Ripresa importazione dati commessa %1",.t.,.f.,.f.,alltrim(this.w_CODCOM))
        this.w_ELABORA = "N"
      endif
    else
      * --- Task trovato
      this.w_PRED_NAME = this.w_TASK_NAME
      this.w_PRJVW = AT_PRJVW
      if this.w_TIPAT="A"
        insert into IMP_PRED (PRED_NAME) values (this.w_PRED_NAME)
      endif
      if this.w_PRJVW="F" AND .F.
        * --- Questa parte non viene attualmente eseguita:
        *     servir� eventualmente in futuro se si decider� di importare i link relativi a macroattivit�
        * --- Se l'attivit� � una macro attivit� foglia in AHE, occorre anche scrivere i suoi dati su tutte le sue sottoattivit�
        * --- Per fare questo preparo un cursore con l'esplosione della treeview
        this.w_TEMPCONTO = this.oParentObject.w_CODCONTO
        * --- Salvo l'attivit� selezionata (essendo l'import dovrebbe essere sempre vuota)
        this.oParentObject.w_CODCONTO = this.w_PRED_NAME
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        select (this.w_COUTCURS)
        go top
        scan
        * --- Estraggo l'ATCODATT e l'ATTIPATT delle sottoattivit� in AHE
        this.w_PRED_NAME = ATCODATT
        this.w_TIPAT = ATTIPATT
        if this.w_PRED_NAME<>this.oParentObject.w_CODCONTO
          * --- Non considero di nuovo l'attivit� radice del sottoalbero
          if this.w_TIPAT="A"
            insert into IMP_PRED (PRED_NAME) values (this.w_PRED_NAME)
          endif
        endif
        select (this.w_COUTCURS)
        endscan
        * --- su w_COUTCURS
        * --- Rilascio il cursore della treeview per prepararlo al nuovo uso
        if used(this.w_COUTCURS)
          select(this.w_COUTCURS)
          Use
        endif
        this.oParentObject.w_CODCONTO = this.w_TEMPCONTO
        * --- Ripristino l'attivit� selezionata (dovrebbe essere sempre vuota)
      endif
    endif
    use in "cursalfa"
    select ("CurPreds")
    * --- SUCCESSORE
    * --- Risalgo al NAME (ATCODATT) da cercare in AHE nel cursore delle TASK_INFORMATION (EXPORT) attraverso il TASKID
    this.w_IDSUCC = SUCCESSORTASKUNIQUEID
    position=recno()
    select "EXPORT"
    go top
    locate for TASKID=this.w_IDSUCC
    if found()
      this.w_SUCC_NAME = substr(NAME,1,15)
      this.w_TIPAT = substr(NAME,rat(")",NAME)-1,1)
    else
      AH_Msg("Questo non dovrebbe mai accadere")
    endif
    select "CurPreds"
    go position
    Vq_Exec("..\comm\exe\query\GSPC2BTP.vqr",this,"cursalfa")
    select "cursalfa"
    if this.w_TIPAT_PRE<>"A"
      * --- Il task estratto dal progetto Project non ha una corrispondente attivit� in AHE e quindi non pu� essere gestito."
      this.w_MESS1 = "Macroattivit� %1:"
      this.w_oERRORLOG.AddMsgLOG(this.w_MESS1 , alltrim(this.w_TASK_NAME))     
      if this.w_TIPAT="A"
        this.w_MESS1 = "Il legame con l'attivit� %1 non pu� essere esportato in ad hoc %2"
      else
        this.w_MESS1 = "Il legame con la macroattivit� %1 non pu� essere esportato in ad hoc %2"
      endif
      this.w_oERRORLOG.AddMsgLOG(this.w_MESS1 , alltrim (this.w_SUCC_NAME), iif(isAHE(),"Enterprise","Revolution"))     
      this.w_oERRORLOG.AddMsgLOG("(non sono previsti legami di precedenza con macroattivit�)")     
      this.w_oERRORLOG.AddMsgLOG(SPACE( 250 ))     
    else
      if reccount()=0
        * --- Il task estratto dal progetto Project non ha un corrispondente in AHE e quindi non pu� essere gestito."
        this.w_MESS = "Il task %1 non esiste in %2 e non pu� essere gestito. Continuare lo stesso?"
        if not ah_yesno(this.w_MESS,"", alltrim(this.w_TASK_NAME), alltrim(g_APPLICATION) )
          * --- Tutti i dati relativi al progetto vengono ignorati
          if this.w_PRJREL<>"2003"
            SQLDISCONNECT(this.w_ConnHandle)
          endif
          * --- Rilascio la connessione alla fonte dati ODBC
          AddMsgNL("Importazione terminata dall'utente", this)
          * --- Raise
          i_Error="Errore"
          return
          i_retcode = 'stop'
          return
        else
          * --- Vengono ignorati solo i dati relativi al task inammissibile in AHE
          AddMsgNL("Ripresa importazione dati commessa %1 ",this,alltrim(this.w_CODCOM))
          this.w_ELABORA = "N"
        endif
      else
        * --- Task trovato
        this.w_PRJVW = AT_PRJVW
        if this.w_TIPAT="A"
          insert into IMP_SUCC (SUCC_NAME) values (this.w_SUCC_NAME)
        endif
        if this.w_PRJVW="F" AND .F.
          * --- Questa parte non viene attualmente eseguita:
          *     servir� eventualmente in futuro se si decider� di importare i link relativi a macroattivit�
          * --- Se l'attivit� � una macro attivit� foglia in AHE, occorre anche scrivere i suoi dati su tutte le sue sottoattivit�
          * --- Per fare questo preparo un cursore con l'esplosione della treeview
          this.w_TEMPCONTO = this.oParentObject.w_CODCONTO
          * --- Salvo l'attivit� selezionata (essendo l'import dovrebbe essere sempre vuota)
          this.oParentObject.w_CODCONTO = this.w_SUCC_NAME
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          select (this.w_COUTCURS)
          go top
          scan
          * --- Estraggo l'ATCODATT e l'ATTIPATT delle sottoattivit� in AHE
          this.w_SUCC_NAME = ATCODATT
          this.w_TIPAT = ATTIPATT
          if this.w_SUCC_NAME<>this.oParentObject.w_CODCONTO
            * --- Non considero di nuovo l'attivit� radice del sottoalbero
            if this.w_TIPAT="A"
              insert into IMP_SUCC (SUCC_NAME) values (this.w_SUCC_NAME)
            endif
          endif
          select (this.w_COUTCURS)
          endscan
          * --- su w_COUTCURS
          * --- Rilascio il cursore della treeview per prepararlo al nuovo uso
          if used(this.w_COUTCURS)
            select(this.w_COUTCURS)
            Use
          endif
          this.oParentObject.w_CODCONTO = this.w_TEMPCONTO
          * --- Ripristino l'attivit� selezionata (dovrebbe essere sempre vuota)
        endif
      endif
    endif
    select "CurPreds"
    if this.w_ELABORA="S"
      * --- Se il legame considerato � ammissibile in AHE
      * --- Preparo i dati rimanenti
      if this.w_PRJREL<>"2003"
        do case
          case LINKTYPE=0
            this.w_LAGTYPE="FF"
          case LINKTYPE=1
            this.w_LAGTYPE="FI"
          case LINKTYPE=2
            this.w_LAGTYPE="IF"
          case LINKTYPE=3
            this.w_LAGTYPE="II"
        endcase
      else
        this.w_LAGTYPE=NVL(LINKTYPE,"")
      endif
      this.w_LAG = INT (LINKLAG/iif(this.w_PRJREL<>"2003",4800,1))
      * --- inserisco i dati per la macro attivit�
      * --- (i dati devono essere inseriti per ogni coppia di attivit� semplici nei cursori IMP_PRED e IMP_SUCC)
      select "IMP_PRED"
      go top
      scan
      this.w_PRED_NAME = PRED_NAME
      select "IMP_SUCC"
      go top
      scan
      this.w_SUCC_NAME = SUCC_NAME
      select "Curs3Project"
      insert into Curs3Project ;
      (MPCODCOM, MPTIPATT,MPCODATT,MPATTPRE,MPTIPPRE,MTRITAR) ;
      values ;
      (this.w_CODCOM,"A",this.w_SUCC_NAME, this.w_PRED_NAME, this.w_LAGTYPE,this.w_LAG)
      endscan
      * --- su IMP_SUCC
      endscan
      * --- su IMP_PRED
      * --- ripulisco i cursori per prepararli alle iterate successive
      select("IMP_SUCC")
      zap
      select("IMP_PRED")
      zap
      select "CurPreds"
    endif
    endscan
    * --- su CurPreds
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento della tabella TASK_DEPENDENCIES
    if this.w_PRJREL="2003" and this.w_MSGBOXCHOICE=6 or this.w_MSGBOXCHOICE=7
      this.w_NUMREC = 1
      do while this.w_NUMREC<=this.w_oPRJ.Tasks.count
        this.w_TASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
        this.w_TASK_NAME = substr(this.w_oPRJ.Tasks.Item(this.w_NUMREC).Name,1,15)
        this.w_CODCOM = this.w_TASK.TEXT1
        this.w_TIPAT = substr(this.w_oPRJ.Tasks.Item(this.w_NUMREC).Name,rat(")",this.w_oPRJ.Tasks.Item(this.w_NUMREC).Name)-1,1)
        this.w_TMPID = this.w_TASK.ID
        this.w_TMPUID = this.w_TASK.UniqueID
        this.w_TIPCOM = substr(this.w_oPRJ.Tasks.Item(this.w_NUMREC).Name,rat(")",this.w_oPRJ.Tasks.Item(this.w_NUMREC).Name)-1,1)
        Update "EXPORT" SET TASKID=this.w_TMPID, EXPORT.TASKUNIQUEID=this.w_TMPUID where; 
 EXPORT.NAME=this.w_TASK_NAME and EXPORT.ATTIPCOM=this.w_TIPAT
        this.w_NUMREC = this.w_NUMREC+1
      enddo
      this.w_NUMREC = 1
      do while this.w_NUMREC<=this.w_oPRJ.Tasks.count
        this.w_TASK = this.w_oPRJ.Tasks.Item(this.w_NUMREC)
        this.w_CODCOM = this.w_TASK.TEXT1
        if this.w_MSGBOXCHOICE=7
          this.w_TASK.UniqueIDPredecessors = ""
        endif
        this.w_TASK_NAME = substr(this.w_oPRJ.Tasks.Item(this.w_NUMREC).Name,1,15)
        this.w_TIPAT = substr(this.w_oPRJ.Tasks.Item(this.w_NUMREC).Name,rat(")",this.w_oPRJ.Tasks.Item(this.w_NUMREC).Name)-1,1)
        this.w_PREDSTR = ""
        Vq_Exec("..\comm\exe\query\GSPC1BTP.vqr",this,"CurPreds")
        Select CurPreds
        if reccount()>0
          go top
          scan
          * --- su curpreds
          * --- Scan su EXPORT per trovare lo UniqueId del predecessore (assegnato a run-time)
          Select EXPORT
          go top
          locate for substr(NAME,1,15)==CurPreds.PREATT and ATTIPATT=CurPreds.PRETIPO
          if FOUND()
            this.w_PREDSTR = alltrim(iif(!EMPTY(this.w_PREDSTR),this.w_PREDSTR+";","")+ALLTRIM(STR(INT(TASKUNIQUEID)))+CurPreds.LINKTYPEAHE+"+"+ALLTRIM(STR(INT(CurPreds.LINKLAG)))+"g")
          endif
          endscan
          this.w_TASK.UniqueIDPredecessors = this.w_PREDSTR
        endif
        this.w_NUMREC = this.w_NUMREC+1
      enddo
    else
      if this.w_MSGBOXCHOICE=7
        * --- Se sono in aggiornamento, marco per la cancellazione tutti i link presenti
        *     (verranno cancellati solo quelli che non saranno nuovamente esportati)
        do case
          case this.w_PRJREL="98"
            this.w_SQLstring = "Update TASK_DEPENDENCIES "
            this.w_SQLstring = this.w_SQLstring+"set DEPENDENCYUNIQUEID=-DEPENDENCYUNIQUEID "
            this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)+" "
          case this.w_PRJREL="2000"
            this.w_SQLstring = "Update MSP_LINKS "
            this.w_SQLstring = this.w_SQLstring+"set LINK_UID=-LINK_UID "
            this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)+" "
        endcase
        this.w_MESSERR = "Impossibile aggiornare la tabella dei legami di precedenza: %2"
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_DEPENDENCYID = 1
      select ("CurPredsFinale")
      GO TOP
      do while Not Eof()
        do case
          case this.w_MSGBOXCHOICE=6
            * --- Progetto non esistente o si � scelto di sovrascrivere il progetto: preparazione della stringa SQL di inserzione
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "Insert into TASK_DEPENDENCIES "
                this.w_SQLstring = this.w_SQLstring+"(PROJECTID,PREDECESSORTASKUNIQUEID,SUCCESSORTASKUNIQUEID,"
                this.w_SQLstring = this.w_SQLstring+"LINKTYPE,LINKLAG,DEPENDENCYUNIQUEID) "
                this.w_SQLstring = this.w_SQLstring+"values "
                this.w_SQLstring = this.w_SQLstring+"("+cp_tostrodbc(PROJECTID)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(PREDID)+","+cp_tostrodbc(SUCCID)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(LINKTYPE)+","+cp_tostrodbc(LINKLAG)+","+cp_tostrodbc(this.w_DEPENDENCYID)+")"
              case this.w_PRJREL="2000"
                this.w_SQLstring = "Insert into MSP_LINKS "
                this.w_SQLstring = this.w_SQLstring+"(PROJ_ID,LINK_PRED_UID,LINK_SUCC_UID,"
                this.w_SQLstring = this.w_SQLstring+"LINK_TYPE,LINK_LAG,LINK_UID,EXT_EDIT_REF_DATA) "
                this.w_SQLstring = this.w_SQLstring+"values "
                this.w_SQLstring = this.w_SQLstring+"("+cp_tostrodbc(PROJECTID)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(PREDID)+","+cp_tostrodbc(SUCCID)+","
                this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(LINKTYPE)+","+cp_tostrodbc(LINKLAG)+","+cp_tostrodbc(this.w_DEPENDENCYID)+",'1')"
            endcase
          case this.w_MSGBOXCHOICE=7
            * --- Si � scelto di aggiornare il progetto
            *     Verifico se il link considerato esiste gi� in Project (n.b.: i link considerati in ahe sono esclusivamente di tipo A->A)
            *     Quindi Attivit� con attivit� (non posso specificare alcuna attivi� precedente per sottoconti o altro)
            this.w_FOUND = .F.
            this.w_NEW_PRED_ID = PREDID
            this.w_NEW_SUCC_ID = SUCCID
            * --- Estraggo i vecchi id se esistono (se non ne esiste almeno uno il link deve essere sicuramente inserito)
            select "TasksInProject2"
            go top
            locate for TASK_NEW_ID=this.w_NEW_PRED_ID
            if found()
              this.w_IDPRED = TASK_ID
              select "TasksInProject2"
              go top
              locate for TASK_NEW_ID=this.w_NEW_SUCC_ID
              if found()
                this.w_IDSUCC = TASK_ID
                select "LinksInProject"
                go top
                locate for PRED_ID=this.w_IDPRED and SUCC_ID=this.w_IDSUCC
                if found()
                  * --- Se il link � gi� presente in Project prelevo il vecchio ID
                  this.w_OLD_DEPENDENCYID = LINK_ID
                  this.w_FOUND = .T.
                  Replace LINK_TOBEDELETED with "U"
                endif
              endif
            endif
            select ("CurPredsFinale")
            if this.w_FOUND
              * --- Il legame esisteva e quindi deve essere aggiornato
              do case
                case this.w_PRJREL="98"
                  this.w_SQLstring = "Update TASK_DEPENDENCIES "
                  this.w_SQLstring = this.w_SQLstring+"set PREDECESSORTASKUNIQUEID="+cp_tostrodbc(PREDID)+","
                  this.w_SQLstring = this.w_SQLstring+"SUCCESSORTASKUNIQUEID="+cp_tostrodbc(SUCCID)+","
                  this.w_SQLstring = this.w_SQLstring+"LINKTYPE="+cp_tostrodbc(LINKTYPE)+","
                  this.w_SQLstring = this.w_SQLstring+"LINKLAG="+cp_tostrodbc(LINKLAG)+","
                  this.w_SQLstring = this.w_SQLstring+"DEPENDENCYUNIQUEID="+cp_tostrodbc(this.w_DEPENDENCYID)+" "
                  this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(PROJECTID)+" "
                  this.w_SQLstring = this.w_SQLstring+"and DEPENDENCYUNIQUEID=-"+cp_tostrodbc(this.w_OLD_DEPENDENCYID)
                case this.w_PRJREL="2000"
                  * --- Costruisco e aggiorno link, come primo passo aggiorno il campo EXT_EDIT_REF_DATA.
                  if this.w_TIPODB="SQLSERVER"
                    this.w_SQLstring = "Execute MSP_BACKUP_LINK '"+ cp_tostrodbc(this.w_PROJECTID)+"','"+cp_tostrodbc(this.w_OLD_DEPENDENCYID)+"'"
                  else
                    this.w_SQLstring = "CALL MSP_BACKUP_LINK '"+ cp_tostrodbc(this.w_PROJECTID)+"','"+cp_tostrodbc(this.w_OLD_DEPENDENCYID)+ "')"
                  endif
                  SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                  this.w_SQLstring = "Update MSP_LINKS "
                  this.w_SQLstring = this.w_SQLstring+"set LINK_PRED_UID="+cp_tostrodbc(PREDID)+","
                  this.w_SQLstring = this.w_SQLstring+"LINK_SUCC_UID="+cp_tostrodbc(SUCCID)+","
                  this.w_SQLstring = this.w_SQLstring+"LINK_TYPE="+cp_tostrodbc(LINKTYPE)+","
                  this.w_SQLstring = this.w_SQLstring+"LINK_LAG="+cp_tostrodbc(LINKLAG)+","
                  this.w_SQLstring = this.w_SQLstring+"LINK_UID="+cp_tostrodbc(this.w_DEPENDENCYID)+" "
                  this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(PROJECTID)+" "
                  this.w_SQLstring = this.w_SQLstring+"and LINK_UID=-"+cp_tostrodbc(this.w_OLD_DEPENDENCYID)
              endcase
            else
              * --- Il legame non esisteva e dunque deve essere inserito
              do case
                case this.w_PRJREL="98"
                  this.w_SQLstring = "Insert into TASK_DEPENDENCIES "
                  this.w_SQLstring = this.w_SQLstring+"(PROJECTID,PREDECESSORTASKUNIQUEID,SUCCESSORTASKUNIQUEID,"
                  this.w_SQLstring = this.w_SQLstring+"LINKTYPE,LINKLAG,DEPENDENCYUNIQUEID) "
                  this.w_SQLstring = this.w_SQLstring+"values "
                  this.w_SQLstring = this.w_SQLstring+"("+cp_tostrodbc(PROJECTID)+","
                  this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(PREDID)+","+cp_tostrodbc(SUCCID)+","
                  this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(LINKTYPE)+","+cp_tostrodbc(LINKLAG)+","+cp_tostrodbc(this.w_DEPENDENCYID)+")"
                case this.w_PRJREL="2000"
                  this.w_SQLstring = "Insert into MSP_LINKS "
                  this.w_SQLstring = this.w_SQLstring+"(PROJ_ID,LINK_PRED_UID,LINK_SUCC_UID,"
                  this.w_SQLstring = this.w_SQLstring+"LINK_TYPE,LINK_LAG,LINK_UID,EXT_EDIT_REF_DATA) "
                  this.w_SQLstring = this.w_SQLstring+"values "
                  this.w_SQLstring = this.w_SQLstring+"("+cp_tostrodbc(PROJECTID)+","
                  this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(PREDID)+","+cp_tostrodbc(SUCCID)+","
                  this.w_SQLstring = this.w_SQLstring+cp_tostrodbc(LINKTYPE)+","+cp_tostrodbc(LINKLAG)+","+cp_tostrodbc(this.w_DEPENDENCYID)+",'1')"
              endcase
            endif
        endcase
        this.w_MESSERR = "Impossibile aggiornare la tabella dei legami di precedenza: %2"
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_DEPENDENCYID = this.w_DEPENDENCYID+1
        select ("CurPredsFinale")
        if Not Eof()
          Skip
        endif
      enddo
      * --- su CurPredsFinale
      if this.w_MSGBOXCHOICE=7 
        * --- Se sono in aggiornamento, devo eliminare i link che non servono pi� da Project
        *     Pi� precisamente sono candidati alla cancellazione tutti i link marcati 'D' in LinksInProject
        *     Di questi vengono per� mantenuti i link che coinvolgono almeno una macroattivit�
        *     (che non vengono reimportati in AHE) per i quali i task coinvolti vengono entrambi riesportati verso Project
        *     Tuttavia i link che coinvolgono una summary ed un task indentato al di sotto di essa
        *     Su tali link deve essere correttamente riaggiornato il LINK_UID e gli ID dei task coinvolti
        *     devono comunque essere eliminati (legami non ammissibili in Project)
        Select "LinksInProject"
        go top
        scan for LINK_TOBEDELETED="D"
        this.w_FOUND = .F.
        this.w_IDPRED = PRED_ID
        this.w_IDSUCC = SUCC_ID
        Select "TasksInProject2"
        go top
        Locate for TASK_ID=this.w_IDPRED
        if Found()
          this.w_NEW_PRED_ID = TASK_NEW_ID
          this.w_WBS = TASK_WBS
          this.w_FOUND = TASK_TYPE<>"A"
          Select "TasksInProject2"
          go top
          Locate for TASK_ID=this.w_IDSUCC
          if Found()
            this.w_NEW_SUCC_ID = TASK_NEW_ID
            this.w_WBSSUB = TASK_WBS
            this.w_FOUND = TASK_TYPE<>"A"
          else
            * --- Se il successore non viene esportato devo eliminare il link anche se il predecessore era una macroattivit� esportata
            this.w_FOUND = .F.
          endif
        endif
        Select "LinksInProject"
        if not this.w_FOUND
          * --- Il link non coinvolge una macroattivit� o almeno uno dei due task non viene nuovamente esportato in Project
          *     Pertanto il link viene eliminato da Project
          do case
            case this.w_PRJREL="98"
              this.w_SQLstring = "Delete from TASK_DEPENDENCIES "
              this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
              this.w_SQLstring = this.w_SQLstring+" and DEPENDENCYUNIQUEID=-"+cp_tostrodbc(LINK_ID)
            case this.w_PRJREL="2000"
              this.w_SQLstring = "Delete from MSP_LINKS "
              this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
              this.w_SQLstring = this.w_SQLstring+" and LINK_UID=-"+cp_tostrodbc(LINK_ID)
          endcase
        else
          * --- Il link coinvolge almeno una macroattivit�: 
          *     in questo caso viene mantenuto in Project solo se uno dei due task non � indentato al di sopra dell'altro
          *     (NOTA: un task A � indentato al disopra di un task B se e solo se il wbs di A � il prefisso del wbs di B
          *     ovvero: WBS(A)=SUBSTR(WBS(B),1,LEN(WBS(A)))
          this.w_FOUND = .F.
          this.w_LEN1 = len(alltrim(this.w_WBS))
          this.w_LEN2 = len(alltrim(this.w_WBSSUB))
          if this.w_LEN1<this.w_LEN2
            * --- il predecessore � una summary per il successore
            this.w_FOUND = alltrim(this.w_WBS)==substr(alltrim(this.w_WBSSUB),1,this.w_LEN1)
          else
            * --- il successore � una summary per il predecessore
            this.w_FOUND = alltrim(this.w_WBSSUB)==substr(alltrim(this.w_WBS),1,this.w_LEN2)
          endif
          if this.w_FOUND
            * --- Il link coinvolge una summary ed un suo subtask e deve essere eliminato da Project
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "Delete from TASK_DEPENDENCIES "
                this.w_SQLstring = this.w_SQLstring+"where PROJECTID="+cp_tostrodbc(this.w_PROJECTID)
                this.w_SQLstring = this.w_SQLstring+" and DEPENDENCYUNIQUEID=-"+cp_tostrodbc(LINK_ID)
              case this.w_PRJREL="2000"
                this.w_SQLstring = "Delete from MSP_LINKS "
                this.w_SQLstring = this.w_SQLstring+"where PROJ_ID="+cp_tostrodbc(this.w_PROJECTID)
                this.w_SQLstring = this.w_SQLstring+" and LINK_UID=-"+cp_tostrodbc(LINK_ID)
            endcase
          else
            * --- Il link deve essere mantenuto in Project, ma il suo UID e gli ID dei task che lo compongono devono essere aggiornati
            do case
              case this.w_PRJREL="98"
                this.w_SQLstring = "Update TASK_DEPENDENCIES "
                this.w_SQLstring = this.w_SQLstring+"set DEPENDENCYUNIQUEID= "+cp_tostrodbc(this.w_DEPENDENCYID)+","
                this.w_SQLstring = this.w_SQLstring+"PREDECESSORTASKUNIQUEID= "+cp_tostrodbc(this.w_NEW_PRED_ID)+","
                this.w_SQLstring = this.w_SQLstring+"SUCCESSORTASKUNIQUEID= "+cp_tostrodbc(this.w_NEW_SUCC_ID)+" "
                this.w_SQLstring = this.w_SQLstring+" where PROJECTID= "+cp_tostrodbc(this.w_PROJECTID)
                this.w_SQLstring = this.w_SQLstring+" and DEPENDENCYUNIQUEID= -"+cp_tostrodbc(LINK_ID)
              case this.w_PRJREL="2000"
                * --- Costruisco e aggiorno link, come primo passo aggiorno il campo EXT_EDIT_REF_DATA.
                if this.w_TIPODB="SQLSERVER"
                  this.w_SQLstring = "Execute MSP_BACKUP_LINK '"+ cp_tostrodbc(this.w_PROJECTID)+"','"+cp_tostrodbc(LINK_ID)+"'"
                else
                  this.w_SQLstring = "CALL MSP_BACKUP_LINK '"+ cp_tostrodbc(this.w_PROJECTID)+"','"+cp_tostrodbc(LINK_ID)+ "')"
                endif
                SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
                this.w_SQLstring = "Update MSP_LINKS "
                this.w_SQLstring = this.w_SQLstring+"set LINK_UID= "+cp_tostrodbc(this.w_DEPENDENCYID)+","
                this.w_SQLstring = this.w_SQLstring+"LINK_PRED_UID= "+cp_tostrodbc(this.w_NEW_PRED_ID)+","
                this.w_SQLstring = this.w_SQLstring+"LINK_SUCC_UID= "+cp_tostrodbc(this.w_NEW_SUCC_ID)+" "
                this.w_SQLstring = this.w_SQLstring+" where PROJ_ID= "+cp_tostrodbc(this.w_PROJECTID)
                this.w_SQLstring = this.w_SQLstring+" and LINK_UID= -"+cp_tostrodbc(LINK_ID)
            endcase
            this.w_DEPENDENCYID = this.w_DEPENDENCYID+1
          endif
        endif
        this.w_MESSERR = "Impossibile aggiornare la tabella dei legami di precedenza: %2"
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Endscan
        * --- Su LinksInProject
      endif
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive nel DB di Project - Se errore avvisa e Esce
    this.w_ERROR = SQLEXEC(this.w_ConnHandle, this.w_SQLstring)
    if this.w_ERROR=-1
      AddMsgNL(this.w_MESSERR , this , "" , Alltrim(Nvl (Name ,"" )) , Message() )
      * --- Raise
      i_Error="Errore"
      return
    endif
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT DATI VERSO PROJECT
    * --- Try
    local bErr_03CE8748
    bErr_03CE8748=bTrsErr
    this.Try_03CE8748()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      AddMsgNL("Si � verificato un errore in fase di export del progetto %1  "+Message(),this,alltrim(this.w_CODCOM))
    endif
    bTrsErr=bTrsErr or bErr_03CE8748
    * --- End
  endproc
  proc Try_03CE8748()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CONVDUR = iif(this.w_PRJREL="2003",4.8,4800)
    * --- Preparazione del cursore relativo alla tabella TASK_INFORMATION (ordinato sul WBS per gestire la struttura)
    * --- Seleziono tutte le attivit� (ATTIPATT='A') e macroattivit� di tipo gestionale (ATTIPATT='G') della commessa voluta
    * --- C'� l'attipatt perch� serve in seguito per recuperare l'ID per gestire i legami di precedenza
    this.w_TIPSTR = iif(nvl(this.w_UNIQUE," ")="S","P","G")
    Vq_Exec("..\comm\exe\query\GSPC_BTP.vqr",this,"cursalfa")
    * --- Faccio l'esplosione della treeview per ottenere il campo lvlkey, corrispondente al campo WBS di project
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- A questo punto i dati sono nel cursore w_COUTCURS
    * --- Costruisco una inner join tra i due cursori trovati, in modo da associare ad ogni attivit� il corretto levlkey/WBS
    * --- l'OUTLINENUMBER corrisponde al vecchio WBS
    coutcurs=this.w_COUTCURS
    * --- Per evitare casini con le macro nella query successiva (n.b.: raddoppiare il punto quando si usa la macro)
    * --- n.b.: duration=duration*480 (ahe=giorni; proj=min.)
    if USED("EXPORT")
      USE IN EXPORT
    endif
    select this.w_PROJECTID as PROJECTID,RECNO() as TASKID,RECNO() as TASKUNIQUEID, cursalfa.NAME as NAME, ;
    (cursalfa.DURATION*this.w_CONVDUR) as DURATION, cursalfa.OUTLINELEVEL as OUTLINELEVEL, ;
    iif(this.w_PROGR="A",cp_todate(cursalfa.STARTDATE),cp_CharToDate(" ")) as STARTDATE, ;
    iif(this.w_PROGR="I",cp_todate(cursalfa.FINISHDATE),cp_CharToDate(" ")) as FINISHDATE, ;
    cursalfa.PERCENTCOMPLET as PERCENTCOMPLETE, cursalfa.CONS_TYPE as CONS_TYPE, ;
    cursalfa.CONSTRAINTTYPE as CONSTRAINTTYPE, cursalfa.CONSTRAINTDATE as CONSTRAINTDATE, ;
    &coutcurs..lvlkey as OUTLINENUMBER, cursalfa.MILESTONEAHE as MILESTONEAHE, cursalfa.MILESTONE as MILESTONE, ;
    cursalfa.ATTIPCOM as ATTIPCOM, cursalfa.SUMMARY as SUMMARY, cursalfa.ATTIPATT as ATTIPATT, cursalfa.AT_PRJVW as AT_PRJVW, ;
    cursalfa.DURATION*iif(this.w_PRJREL="2003",this.w_CONVDUR,48)*cursalfa.PERCENTCOMPLET as ACT_DUR, cursalfa.DURATION*iif(this.w_PRJREL="2003",this.w_CONVDUR,48)*(100-cursalfa.PERCENTCOMPLET) as REM_DUR;
    from cursalfa inner join &coutcurs ;
    on (&coutcurs..atcodcom=cursalfa.atcodcom ;
    and &coutcurs..atcodatt=SUBSTR(cursalfa.NAME,1,15) ;
    and &coutcurs..attipatt=cursalfa.attipatt) ;
    where NVL(cursalfa.AT_PRJVW," ")<>"N" ;
    Into Cursor EXPORT Order By 13 NoFilter
    * --- Creazione del cursore per accogliere i legami di precedenza
    Create cursor CurPredsFinale ;
    (DEPENDID N(6.0), PROJECTID N(18,4), PREDID N(4,0), SUCCID N(4,0), ;
    LINKTYPEAHE C(2), LINKTYPE N(6,0), LINKLAG N(6,0), ATTIPATT C(1), ATCODATT C(15) )
    cur=wrcursor("CurPredsFinale")
    * --- Riassegnamento degli ID, in conformit� all'ordinamento sul WBS, per una corretta gestione dei predecessori
    select "EXPORT"
    cur=wrcursor("EXPORT")
    go top
    scan
    * --- su export
    replace TASKID with RECNO()
    replace TASKUNIQUEID with RECNO()
    endscan
    * --- su export
    * --- Imposta le foglie detta tree-view sul valore del campo OUTLINENUMBER
    select "EXPORT"
    go bottom
    this.w_WBS = "XXX"
    do while not bof()
      if rtrim(OUTLINENUMBER) <> rtrim(this.w_WBS)
        replace AT_PRJVW with "F"
      endif
      this.w_WBS = OUTLINENUMBER
      skip -1
    enddo
    * --- Aggiustamento dei tipi per il cursore delle TASK_INFORMATION
    go top
    scan
    * --- su export
    this.w_IDSUCC = RECNO()
    * --- Per successiva gestione delle precedenze - Formattazione WBS senza spazi nella stringa
    replace OUTLINENUMBER with strtran(OUTLINENUMBER, " ", "")
    this.w_WBS = OUTLINENUMBER
    * --- Il livello di un task in un outline � pari al numero di occorrenze di '.' + 1 nel WBS
    this.w_OUTLN_LVL = occurs(".",this.w_WBS)+1
    replace OUTLINELEVEL with this.w_OUTLN_LVL
    * --- Ripulisco le date di default (01-01-1900) messe da Fox, che in Project generano errori
    if cp_todate(STARTDATE)=i_INIDAT
      replace STARTDATE with this.w_START_DFLT
    endif
    if cp_todate(FINISHDATE)=i_INIDAT
      replace FINISHDATE with this.w_FINISH_DFLT
    endif
    * --- Preparo il tipo di vincolo sull'attivit�
    do case
      case CONS_TYPE="PTP"
        * --- Pi� Tardi Possibile
        replace CONSTRAINTTYPE with 1
      case CONS_TYPE="PPP"
        * --- Pi� Presto Possibile
        replace CONSTRAINTTYPE with 0
      case CONS_TYPE="FNP"
        * --- Finire Non Prima
        replace CONSTRAINTTYPE with 6
      case CONS_TYPE="FNO"
        * --- Finire Non Oltre
        replace CONSTRAINTTYPE with 7
      case CONS_TYPE="DFI"
        * --- Deve Finire Il
        replace CONSTRAINTTYPE with 3
      case CONS_TYPE="DII"
        * --- Deve Iniziare Il
        replace CONSTRAINTTYPE with 2
      case CONS_TYPE="INP"
        * --- Iniziare Non Prima
        replace CONSTRAINTTYPE with 4
      case CONS_TYPE="INO"
        * --- Iniziare Non Oltre
        replace CONSTRAINTTYPE with 5
      case .T.
        if this.w_PROGR="A"
          * --- Se programmazione in avanti: default=Deve Iniziare Il
          replace CONSTRAINTTYPE with 2
        else
          * --- Se programmazione all'indietro: default=Deve Finire Il
          replace CONSTRAINTTYPE with 3
        endif
    endcase
    * --- Ripulisco le date di default (01-01-1900) messe da Fox, che in Project generano errori
    if ( cp_todate(CONSTRAINTDATE)=i_INIDAT or empty(nvl(CONSTRAINTDATE,cp_CharToDate("  -  -  "))) ) And Not(CONS_TYPE$ "PTP-PPP")
      if this.w_PROGR="A"
        * --- Se programmazione in avanti: default=data di inizio attivit�
        replace CONSTRAINTDATE with STARTDATE
      else
        * --- Se programmazione all'indietro: default= data di fine attivit�
        replace CONSTRAINTDATE with FINISHDATE
      endif
    endif
    * --- Su un nodo della TV sono supportati solo i valori 0,4,7
    if AT_PRJVW="S" and not inlist(CONSTRAINTTYPE, 0, 4, 7)
      if this.w_PROGR="A" or empty(nvl(CONSTRAINTDATE,{}))
        * --- Se programmazione in avanti: default=Al pi� presto possibile
        replace CONSTRAINTTYPE with 0
      else
        * --- Se programmazione all'indietro: default=Finire Non Oltre
        replace CONSTRAINTTYPE with 7
      endif
    endif
    * --- Formattazione campi logici (verificare terza condizione)
    if MILESTONEAHE="N" or MILESTONEAHE=" " or MILESTONEAHE=NULL or AT_PRJVW="F"
      replace MILESTONE with 0
    else
      replace MILESTONE with 1
    endif
    if (ATTIPCOM="C" or ATTIPCOM="P" or ATTIPCOM="S") and AT_PRJVW<>"F"
      replace SUMMARY with 1
    else
      replace SUMMARY with 0
    endif
    if AT_PRJVW="F" and DURATION=0
      * --- Tentativo di dare una durata minima ad una macroattivit� foglia che abbia durata nulla
      replace DURATION with 9600
    endif
    * --- Preparazione del cursore relativo alla tabella delle TASK_DEPENDENCIES
    *     Per il task attualmente considerato nel cursore delle TASK_INFORMATION, estraggo un cursore di suoi predecessori
    *     Per ciascuno di questi predecessori effettuo una scan sul cursore delle TASK_INFORMATION per ottenere il suo ID
    *     Seleziono tutte le relazioni di precedenza relative alla commessa voluta e al task attualmente considerato
    this.w_TASK_NAME = substr(NAME,1,15)
    this.w_TIPAT = substr(NAME,rat(")",NAME)-1,1)
    Vq_Exec("..\comm\exe\query\GSPC1BTP.vqr",this,"CurPreds")
    select "CurPreds"
    if reccount()>0
      go top
      scan
      * --- su curpreds
      this.w_CODATTPRE = ATCODATT
      * --- Selezione della chiave del predecessore per risalire all'ID appena assegnato
      this.w_TIPATTPRE = ATTIPATT
      select ("Export")
      * --- Scan su EXPORT per trovare lo UniqueId del predecessore (assegnato a run-time)
      position=recno()
      go top
      locate for NAME=this.w_CODATTPRE and ATTIPATT=this.w_TIPATTPRE
      if FOUND()
        this.w_IDPRED = TASKUNIQUEID
      else
        * --- Nel caso in cui uno solo dei due task coinvolti nel vincolo di precedenza sia esportato
        *     il link tra essi non deve essere considerato in Project
        * --- riposizionamento su export
        go position
        select "CurPreds"
        LOOP
        * --- (non viene eseguita la insert in CurPredsFinale)
      endif
      * --- riposizionamento su export
      go position
      select "CurPreds"
      replace DEPENDID with RECNO()
      replace PROJECTID with this.w_PROJECTID
      replace PREDID with this.w_IDPRED
      replace SUCCID with this.w_IDSUCC
      do case
        case LINKTYPEAHE="NN"
          * --- Lag di tipo nessuno (=FI+0g)
          replace LINKTYPE with 1
        case LINKTYPEAHE="FI"
          * --- Lag di tipo Fine-Inizio
          replace LINKTYPE with 1
        case LINKTYPEAHE="II"
          * --- Lag di tipo Inizio-Inizio
          replace LINKTYPE with 3
        case LINKTYPEAHE="FF"
          * --- Lag di tipo Fine-Fine
          replace LINKTYPE with 0
        case LINKTYPEAHE="IF"
          * --- Lag di tipo Inizio-Fine
          replace LINKTYPE with 2
      endcase
      Insert Into CurPredsFinale ;
      (DEPENDID,PROJECTID,PREDID,SUCCID, ;
      LINKTYPEAHE,LINKTYPE,LINKLAG,ATTIPATT,ATCODATT) ;
      values ;
      (CurPreds.DEPENDID,CurPreds.PROJECTID,CurPreds.PREDID,CurPreds.SUCCID, ;
      CurPreds.LINKTYPEAHE,CurPreds.LINKTYPE,CurPreds.LINKLAG*4800,CurPreds.ATTIPATT,CurPreds.ATCODATT)
      EndScan
      * --- Scan su CurPreds
    endif
    select "EXPORT"
    endscan
    * --- su export
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_PRJREL="2003"
      this.w_oPRJ.Activate()     
      this.w_oMSP.ScreenUpdating = True
      if this.w_EXPMCOMM
        if this.w_MSGBOXCHOICE<>2
          * --- Salvo e chiudo il progetto
          this.w_oMSP.FileClose(1)     
        else
          * --- Chiudo il progetto attivo senza salvare
          w_olderr=ON("ERROR")
          w_err =.f.
          ON ERROR w_err=.t.
          this.w_oMSP.FileClose(0)     
          ON ERROR &w_olderr
        endif
      else
        this.w_oPRJ.SaveAs(alltrim(this.w_PRJFILE))     
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    AddMsgNL ("L'aggiornamento del progetto %1 in MS-Project � riuscito ", this, alltrim(this.w_CODCOM))
    if this.w_PRJREL<>"2003"
      * --- Rilascio la connessione alla fonte dati ODBC
      SQLDISCONNECT(this.w_ConnHandle)
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ATTIVITA'
    this.cWorkTables[2]='ATT_PREC'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='CPAR_DEF'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gspc_btp
  func hasspchar
    && controlla se la stringa passata come parametro contiene un carattere speciale
  	param p_str
  	private c_str, b_retval
  	
  	b_retval = .f.
  	c_str = alltrim(p_str)
  
  	if occurs('\', c_str)>0 or occurs('/', c_str)>0 or occurs(':', c_str)>0 or ;
  	  occurs('*', c_str)>0 or occurs('?', c_str)>0 or occurs('"', c_str)>0 or ;
      occurs('<', c_str)>0 or occurs('>', c_str)>0 or occurs('|', c_str)>0	
  	    b_retval = .t.
  	endif
  	
  	return b_retval
  endfunc
  
  func convstr
    && converte una stringa sostituendo i caratteri speciali con '_'
    && e accoda alla stringa '_' + la somma degli asc dei caratteri  
  	param p_str
  	private c_str, n_lens, n_totc, n_count
  	
  	n_totc = 0
  	n_count = 1
  	c_str=alltrim(p_str)
  
  	if this.hasspchar(c_str)
  	
  		n_lens=LEN(ALLTRIM(c_str))
  	
  		DO while (n_count <= n_lens)
  			n_totc = n_totc + ASC(SUBSTR(c_str, n_count, 1))
  			n_count = n_count + 1
  		enddo
  
  		c_str = STRTRAN(c_str, '\', '_')
  		c_str = STRTRAN(c_str, '/', '_')
  		c_str = STRTRAN(c_str, ':', '_')
  		c_str = STRTRAN(c_str, '*', '_')
  		c_str = STRTRAN(c_str, '?', '_')
  		c_str = STRTRAN(c_str, '"', '_')
  		c_str = STRTRAN(c_str, '<', '_')
  		c_str = STRTRAN(c_str, '>', '_')
  		c_str = STRTRAN(c_str, '|', '_')
  
  		c_str = c_str + '_' + ALLTRIM(STR(n_totc))
  	endif
  			
  	return c_str
  endfunc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
