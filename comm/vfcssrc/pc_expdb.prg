* Esplosione Tree View Produzione su Commessa
*
* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: oExpDis
* Ver      : 1.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Samuele Masetto
* Data creazione: 08/03/99
* Aggiornato il : 08/03/99
* #%&%#Build:  27
* ----------------------------------------------------------------------------
* Oggetto per l'esplosione distinta base
* ----------------------------------------------------------------------------
parameters cOutCursor,cInpCursor,cRifTable,cRifKey,cExpTable,cExpKey,cRepKey,cExpField,cOtherField
*cOutCursor=nome del cursore di output contenente i dati esplosi
*cInpCursor=nome del cursore da esplodere
*cRifTable=nome della tabella di riferimento
*cRifKey=chiavi per la tabella di riferimento
*cExpTable=nome della tabella di esplosione
*cExpKey=chiavi per la tabella di esplosione
*cRepKey=chiave ripetuta della tabella di esplosione
*cExpField=nome dei campi di esplosione della tabella di esplosione (si comportano come il campo "Quantit�" dei componenti degli articoli)
*cOtherField=nome dei campi della tabella di esplosione da includere nel cursore risultato "cOutCursor"
private i_retval,i_Rows
i_retval = ''
i_Rows = 0
createobject("tOBJDIS",cOutCursor,cInpCursor,cRifTable,cRifKey,cExpTable,cExpKey,cRepKey,cExpField,cOtherField)
return(i_retval)

define class tOBJDIS as custom
  cOutCursor = ''
  nCntComp = 0
  sTmpLevel = ''
  tmpQta = 0
  sLevel = ''
  nCntLevel = 0
  nRifIdx=0
  nExpIdx=0
  cExpTable=''
  cInpCursor=''
  cExpField=''
  cRifTable=''
  cRifKey=''
  cExpKey=''
  cRepKey=''
  cOtherField=''
proc Init(cOutCursor,cInpCursor,cRifTable,cRifKey,cExpTable,cExpKey,cRepKey,cExpField,cOtherField)
  this.cOutCursor=cOutCursor
  this.cExpTable=cExpTable
  this.cInpCursor=cInpCursor
  this.cExpField=cExpField
  this.cRifTable=cRifTable
  this.nExpIdx=cp_OpenTable(this.cExpTable)
  this.nRifIdx=cp_OpenTable(this.cRifTable)
  this.cExpKey=cExpKey
  this.cRifKey=cRifKey
  this.cRepKey=cRepKey 
  this.cOtherField=iif(type('cOtherField')='L','',cOtherField)
  if used(this.cInpCursor)
    this.Explode()
    select (this.cOutCursor)
  else
    cp_ErrorMsg("Il cursore '"+this.cInpCursor+"' non � aperto.")
  endif
  return
procedure Explode()
  local i_nConn,i_cTable,cFields,i_a,cSearch,cValue,i,cExpField,nCurrRec,nNodeIdx,nTmp,nRes
  local i_bInsert,nCurrentPos,i_cOutFields
  i_nConn=i_TableProp[this.nRifIdx,3]
  *i_cTable=cp_SetAzi(i_TableProp[this.nRifIdx,2])
  nNodeIdx=1
  dimension nCurrentPos[1]
  dimension i_cOutFields[1]
  *---Creazione della tabella temporanea e dell'array dei campi della tabella temporanea
  *select * from  (i_cTable) where 1=2 into cursor __tmp__
  this.OpenCursorFields("__tmp__")
  select __tmp__
  this.CreateTmpTable(this.cOutCursor,1,this.cRifKey)
  select (this.cOutCursor)
  afields(i_cOutFields)
  *---Crea un array con la disposizione delle chiavi
  select (this.cInpCursor)
  scan &&sul cursore
    this.sLevel = str(nNodeIdx,3,0)
    this.sTmpLevel = this.sLevel
    dimension i_a[1,2]
    SelectFromTable(@i_a,this.nRifIdx,this.cRifKey,this.cRifKey,"_oDis_")
    *---Cerca se questo record � gi� stato caricato
    select (this.cInpCursor)
    cSearch=''
    for i=1 to alen(i_a,1)
      *cSearch=cSearch+cp_ConvertToStr(&i_a[i,1])
      cSearch=cSearch+cp_ToStr(&i_a[i,1])
    next
    select (this.cOutCursor)    
    nCurrRec=0
    if not eof() 
       nCurrRec=recno()
    endif
    set order to Idx_1
    seek (cSearch)
    if nCurrRec<>0
      go nCurrRec
    endif
    set order to
    if not found()
      nNodeIdx=nNodeIdx+1
      if used('_oDis_')
        select _oDis_
        locate for 1=1
        do while not(eof())
          this.AppendField(this.cOutCursor,1,@i_cOutFields) &&Scrive la riga del cursore nella tabella di esplosione
          select _oDis_
          continue
        enddo
        use
      endif
     * TAM INIZIO
      TAM_nConn=i_TableProp[this.nExpIdx,3]
      TAM_cTable=cp_SetAzi(i_TableProp[this.nExpIdx,2])
      * TAM FINE
      * INIZIO PRIMA
      * i_nConn=i_TableProp[this.nExpIdx,3]
      * i_cTable=cp_SetAzi(i_TableProp[this.nExpIdx,2])
      * FINE PRIMA
      select (this.cOutCursor)
      if not this.CheckType(TAM_nConn,TAM_cTable )
        return
      endif
      select (this.cOutCursor)
      nCurrentPos[1]=0
* vecchia riga     scan for "!"+str(nNodeIdx-1,3,0)$"!"+lvlkey  &&sulla tabella creata con i dati esplosi
* TAM INIZIO
	   scan for "!"+str(nNodeIdx-1,3,0)$"!"+lvlkey And ATTIPATT<>'A' &&sulla tabella creata con i dati esplosi
	   * tam FINE
        *---Conta il livello
        i=len(lvlkey)-len(strtran(lvlkey,'.',''))+1
        if alen(nCurrentPos)<i
          dimension nCurrentPos[i]
          nCurrentPos[i]=recno()
        else
          nCurrentPos[i]=max(recno(),nCurrentPos[i])
        endif
        this.nCntLevel = 0
        * Tam Inizio
        * Modifica alla costruzione di LVLKEY
        * vecchia riga
        * this.sLevel = alltrim(lvlkey+iif(this.nCntLevel<>0,'.'+alltrim(str(this.nCntLevel)),''))
        this.sLevel = rtrim(lvlkey+iif(this.nCntLevel<>0,'.'+str(this.nCntLevel,3,0),''))
        * Tam Fine
        this.nCntComp = 0
        this.tmpQta = qtacomp
        * TAM PER AVERE I MILLESIMI DELLA STRUTTURA
        SelectFromTable(@i_a,this.nExpIdx,this.cRifKey,this.cExpKey,"_dis_",this.cOtherField)
          * VECCHIA VERSIONE      SelectFromTable(@i_a,this.nExpIdx,this.cRifKey,this.cExpKey,"_dis_")
        * TAM FINE
        if used('_dis_')
          select _dis_
          locate for 1=1
          do while not(eof())
            cExpField=this.cExpField
			* tam inizio
  			* sto interrogando le attivit�, leggo anche altri campi
    		SelectFromTable(@i_a,this.nRifIdx,this.cRepKey,this.cRifKey+',ATTIPCOM,ATDESCRI,ATPERCOM',"_Rif_")
			*tam fine
			* prima
			* SelectFromTable(@i_a,this.nRifIdx,this.cRepKey,this.cRifKey,"_Rif_")
			* fine prima
     		if used('_Rif_') 
              select _Rif_
              locate for 1=1
              nRes=_tally  
              * tam inizio
              * l'attivit� � sempre una
             *do while not(eof())
             * tam fine
               if empty(cExpField)
                  nTmp=0
                else
                  nTmp=_Dis_.&cExpField*this.tmpQta
                endif
                this.AppendField(this.cOutCursor,nTmp,@i_cOutFields,"_dis_")
                this.nCntLevel = this.nCntLevel+1
                this.sTmpLevel = this.sLevel+iif(this.nCntLevel<>0,'.'+str(this.nCntLevel,3,0),'')
                * TAM INZIO -SPOSTATE SOPRA PER FARLE UNA VOLTA SOLA
                * i_nConn=i_TableProp[this.nExpIdx,3]
                * i_cTable=cp_SetAzi(i_TableProp[this.nExpIdx,2])
                * TAM FINE SPOSTATE SOPRA ...
                select (this.cOutCursor)
                * TAM INIZIO
                IF ATTIPATT='A'
				* se foglia non vado a leggere la struttura
				this.SegnaTab(this.cOutCursor,"P",this.sTmpLevel)
				else	
                if not this.CheckType(TAM_nConn,TAM_cTable )
                  return
                endif 
                endif     
                * TAM FINE
                * PRIMA
                *if not this.CheckType(i_nConn,i_cTable)
                 * return
                *endif     
                * FINE PRIMA
                select _Rif_
                * tam inizio
               *continue 
             * enddo
             * tam fine
              use
            endif
            if nRes<>0 
              this.nCntComp = this.nCntComp+1
           endif
            select _Dis_
            continue
          enddo
          use
        endif
        select (this.cOutCursor)
        if this.nCntComp<>0
          go nCurrentPos[i]
        endif
      endscan
    endif
    select (this.cInpCursor)
  endscan
  return
proc OpenCursorFields(i_cCursor)
  local i_a,i,i_cFields,i_cTableRif,i_cTableExp,i_nConn
  dimension i_a[1]  
  i_cTableRif=cp_SetAzi(i_TableProp[this.nRifIdx,2])
  i_cTableExp=cp_SetAzi(i_TableProp[this.nExpIdx,2])
  i_nConn=i_TableProp[this.nExpIdx,3]
  *select * from (this.cInpCursor) where 1=2 into cursor (i_cCursor) 
  select (this.cInpCursor)
  afields(i_a)
  i_cFields=''
  for i=1 to fcount()
    i_cFields=i_cFields+i_cTableRif+"."+i_a[i,1]+","
  next
  i_cFields=left(i_cFields,len(i_cFields)-1)
  i_cFields=i_cFields+iif(empty(this.cOtherField),'',',')+this.cOtherField
  if i_nConn<>0
  *  wait wind 
    sqlexec(i_nConn,"select "+i_cFields+" from "+i_cTableRif+" ,"+i_cTableExp+ " where 1=0" ,i_cCursor)
  else
    select &i_cFields from (i_cTableRif),(i_cTableExp) where 1=0 into cursor (i_cCursor)
  endif
  return
proc FillPKFields()
  local cExp,cRif,i
  this.cExpKey=''
  cp_ReadXdc()
  cExp=i_dcx.GetIdxDef(this.cExpTable,1)
  cRif=i_dcx.GetIdxDef(this.cRifTable,1)
  for i=1 to i_dcx.GetFieldsCount(this.cExpTable)
    if at(upper(i_dcx.GetFieldName(this.cExpTable,i)),upper(cExp))<>0
      this.cExpKey=this.cExpKey+i_dcx.GetFieldName(this.cExpTable,i)+","
    endif
  next
  for i=1 to i_dcx.GetFieldsCount(this.cRifTable)
    if at(i_dcx.GetFieldName(this.cRifTable,i),cRif)<>0
      this.cRifKey=this.RifKey+i_dcx.GetFieldName(this.cRifTable,i)+","
    endif
  next
  return
proc CloseCursors()
  if used('_Rif_')
    use in _Rif_
  endif
  if used('_Dis_')
    use in _Dis_
  endif
  return
proc CreateTmpTable(cTableName,nQta,cKey)
  local i_a,i_OldCursor,nLen,i,j,cIdx,i_b
  dimension i_a[1]  
  i_OldCursor=alias()
  afields(i_a)
  nLen=alen(i_a,1)
  dimension i_a[nLen+4,alen(i_a,2)]
  *---Copia la struttura dell'ultimo elemento nei nuovi campi che si devono inserire
  for i=nLen to nLen+4
    for j=1 to alen(i_a,2)
      i_a[i,j]=i_a[nLen,j]
    next
  next 
  *---Aggiunta dei nuovi campi
  i_a[nLen+1,1]="tippro"
  i_a[nLen+1,2]="C"
  i_a[nLen+1,3]="1"
  i_a[nLen+1,4]="0"
  i_a[nLen+2,1]="qtacomp"
  i_a[nLen+2,2]="N"
  i_a[nLen+2,3]="10"
  i_a[nLen+2,4]="2"
  i_a[nLen+3,1]="LvlKey"
  i_a[nLen+3,2]="C"
  * Tam Inizio  
  * X COSTRUIRE IN MODO OPPORTUNO LVLKEY
  * vecchia riga
  *  i_a[nLen+3,3]="100"
  i_a[nLen+3,3]="200"
  * Tam Fine
  i_a[nLen+3,4]="0"
  * Aggiunto il campo per gestione BitMap
  i_a[nLen+4,1]="CPBmpName"
  i_a[nLen+4,2]="C"
  i_a[nLen+4,3]="150"
  i_a[nLen+4,4]="0"
  create cursor (cTableName) from array i_a
  dimension i_b[1,2]
  CreateKeysArray(@i_b,cKey,cKey)
  cIdx=''
  for i=1 to alen(i_b,1)
    cIdx=cIdx+cp_ConvertToStr(i_b[i,1])+"+"
  next
  cIdx=left(cIdx,len(cIdx)-1)
  index on &cIdx tag Idx_1 COLLATE "MACHINE"
  index on LvlKey tag LvlKey COLLATE "MACHINE"
  return
proc AppendField(cTableName,nQta,i_cOutFields,i_cCursor)
  local i_a,i_Fields,i_bAdd,cTmp,i_cOther,i_Value,i,j,bFind,i_nIdx,i_nCount
  i_bAdd=.f.
  if parameters()>3
    i_bAdd=.t.
  endif
  dimension i_a[1]  
  dimension i_Fields[1]
  dimension i_Value[1]
  scatter to i_a
  afields(i_Fields)
  *--- Creazione dell'array dei valori
  i_nCount=0
  for i=1 to alen(i_a)
    i_nIdx=0
    for j=1 to alen(i_cOutFields,1)
      if i_cOutFields[j,1]==i_Fields[i,1]
        i_nIdx=j
        exit
      endif
    next
    if i_nIdx>0
      i_nCount=i_nCount+1
      dimension i_Value[i_nCount]
      i_Value[i_nCount]=i_a[j]
    endif
  next
  *--- Inserisce gli eventuali valori aggiuntivi
  if i_bAdd
    if not empty(this.cOtherField) and used(i_cCursor)
      *---Popola l'array dei campi aggiuntivi 
      cTmp=this.cOtherField+','
      i=0
      do while at(",",cTmp)<>0
        i=i+1
        i_cOther=left(cTmp,at(",",cTmp)-1)
        select (i_cCursor)
        i_nCount=i_nCount+1
        dimension i_Value[i_nCount]
* dava i_Count not found tam        
*        i_Value[i_Count]=&i_cOther
        i_Value[i_nCount]=&i_cOther
        cTmp=substr(cTmp,at(",",cTmp)+1)
      enddo
    endif
  endif
  select (cTableName)
  append from array i_Value
  * tam inizio
  * PRODUZIONE SU COMMESSA, IMPOSTO IL BITMAP IN BASE AL TIPO DI NODO (ATTIVITA', CONTO , CAPO PROGETTO)
  replace CPBmpName with iif(ATTIPCOM='P' Or Empty(Nvl(ATTIPCOM,'')),'BMP\ROOT.BMP',IIF(ATTIPCOM='S','BMP\CONTO.BMP',IIF(ATTIPCOM='C','BMP\LEAF.BMP','BMP\NEW1.BMP')))
  * tam fine
  replace qtacomp with nQta
  
  return
proc SegnaTab(cTableName,cType,sLevel)
  select (cTableName) 
  replace tippro with cType
  replace lvlkey with sLevel
  return
 func CheckType(i_nConn,i_cTable)
  local cFields,i_a,cSearch,i
  dimension i_a[1,2]
  CreateKeysArray(@i_a,this.cRifKey,this.cExpKey)
  cSearch=FillWhereFromArray(@i_a,i_nConn," and ")
    if i_nConn<>0
    sqlexec(i_nConn,"select "+this.cExpKey+" from "+i_cTable+" where "+cSearch,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    cFields=this.cExpKey
    select &cFields from (i_cTable) where &cSearch into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    if i_Rows=0 
      * --- Segno come materia prima
      this.SegnaTab(this.cOutCursor,"P",this.sTmpLevel)
    else
      * --- L'articolo ha dei componenti
      this.SegnaTab(this.cOutCursor,"F",this.sTmpLevel)
    endif
    select _read_
    use
  else
    * --- errore: non e' stato trovato nessun record
    return(.f.)
  endif
  return(.t.)
endfunc

enddefine

*Tam Inizio
*Vecchia chiamata
*proc SelectFromTable(i_a,i_nTableWhereIdx,i_cFieldValue,i_cFieldWhere,i_cCursor)
proc SelectFromTable(i_a,i_nTableWhereIdx,i_cFieldValue,i_cFieldWhere,i_cCursor,i_cOther)
*Tam Fine
  local i_nConn,i_cTable,cSearch,cOrderBy,cTamFields
  i_nConn=i_TableProp[i_nTableWhereIdx,3]
  i_cTable=cp_SetAzi(i_TableProp[i_nTableWhereIdx,2])
  CreateKeysArray(@i_a,i_cFieldValue,i_cFieldWhere)
  cSearch=FillWhereFromArray(@i_a,i_nConn," and ")
  * TAM INIZIO
  cOrderBy=''
  if type('i_cOther')='C'
   * il campo che aggiungo mi da anche l'ordinamento
      if Not Empty (i_cOther)
       cOrderBy=' Order By '+i_Cother  
      endif
  else
      i_cother=''
  endif
  if i_nConn<>0
    sqlexec(i_nConn,"select "+i_cFieldWhere+IIF(Not Empty(i_cother),','+i_cother,'')+" from "+i_cTable+" where "+;
        cSearch+cOrderBy,i_cCursor)
  else
    cTamFields=i_cFieldWhere+IIF(Not Empty(i_cother),','+i_cother,'')
    select &cTamFields from (i_cTable) where &cSearch &cOrderBy into cursor (i_cCursor)
  endif
  *TAM FINE
* PRIMA
*  if i_nConn<>0
 *   sqlexec(i_nConn,"select * from "+i_cTable+" where "+cSearch,i_cCursor)
 * else
  *  select * from (i_cTable) where &cSearch into cursor (i_cCursor)
 * endif
* FINE PRIMA  
  return
proc CreateKeysArray(i_a,cKey1,cKey2)
  local i,cTmp,c1,c2,n,cTmp2
  cTmp=cKey1+","
  cTmp2=iif(empty(cKey2),cKey1,cKey2)+","
  i=0
  do while at(",",cTmp)<>0
    i=i+1
    dimension i_a[i,2]
    i_a[i,1]=left(cTmp,at(",",cTmp)-1)
    cTmp=substr(cTmp,at(",",cTmp)+1)
    i_a[i,2]=left(cTmp2,at(",",cTmp2)-1)
    cTmp2=substr(cTmp2,at(",",cTmp2)+1)
  enddo
return
func FillWhereFromArray(i_a,i_nConn,cToAdd)
  local cSearch,i
  cSearch=''
  for i=1 to alen(i_a,1)
    if i_nConn<>0
      cSearch=cSearch+i_a[i,2]+"="+cp_ToStrODBC(&i_a[i,1])+cToAdd
    else
      cSearch=cSearch+i_a[i,2]+"=="+cp_ToStr(&i_a[i,1])+cToAdd
    endif
  next
  if not empty(cSearch)
    cSearch=left(cSearch,len(cSearch)-len(cToAdd))
  endif
  return(cSearch)
func cp_ConvertToStr(i_cField)
  local i_cConvert
  do case 
    case type(i_cField)='C'
      return(i_cField)
    case type(i_cField)='N'
      return('str('+i_cField+')')
    case type(i_cField)='D'
      return('dtoc('+i_cField+')')
  endcase
  return
