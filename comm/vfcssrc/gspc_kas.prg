* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_kas                                                        *
*              Gestione stato attivit�                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_229]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-07                                                      *
* Last revis.: 2008-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_kas",oParentObject))

* --- Class definition
define class tgspc_kas as StdForm
  Top    = 3
  Left   = 2

  * --- Standard Properties
  Width  = 815
  Height = 487
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-19"
  HelpContextID=132798569
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  CPAR_DEF_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gspc_kas"
  cComment = "Gestione stato attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READPAR = space(10)
  w_CODCOM = space(15)
  o_CODCOM = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_AT_STATO = space(1)
  o_AT_STATO = space(1)
  w_SELEZI = space(1)
  w_NPCOMP = 0
  w_DESCOM = space(30)
  w_TIPATT = space(10)
  w_DEFCOM = space(15)
  w_CODATT = space(15)
  w_PERCOM = space(10)
  w_CPERCOM = 0
  w_STAT1 = space(1)
  w_CALCOLA = .F.
  o_CALCOLA = .F.
  w_OCPERCOM = 0
  w_ATSTINIT = space(1)
  w_AT__STATO = space(1)
  w_FIRST = .F.
  w_APERCOM = 0
  w_Riga = 0
  w_OBTEST = ctod('  /  /  ')
  w_TIPSTR = space(1)
  w_TIPCOMS = space(1)
  w_TIPCOMC = space(1)
  w_DESSTC = space(30)
  w_CODSTC = space(15)
  w_CURSORNA = space(10)
  w_ZoomDett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_kasPag1","gspc_kas",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDett = this.oPgFrm.Pages(1).oPag.ZoomDett
    DoDefault()
    proc Destroy()
      this.w_ZoomDett = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CPAR_DEF'
    this.cWorkTables[3]='ATTIVITA'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSPC_BAS(this,"UPD")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR=space(10)
      .w_CODCOM=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_AT_STATO=space(1)
      .w_SELEZI=space(1)
      .w_NPCOMP=0
      .w_DESCOM=space(30)
      .w_TIPATT=space(10)
      .w_DEFCOM=space(15)
      .w_CODATT=space(15)
      .w_PERCOM=space(10)
      .w_CPERCOM=0
      .w_STAT1=space(1)
      .w_CALCOLA=.f.
      .w_OCPERCOM=0
      .w_ATSTINIT=space(1)
      .w_AT__STATO=space(1)
      .w_FIRST=.f.
      .w_APERCOM=0
      .w_Riga=0
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPSTR=space(1)
      .w_TIPCOMS=space(1)
      .w_TIPCOMC=space(1)
      .w_DESSTC=space(30)
      .w_CODSTC=space(15)
      .w_CURSORNA=space(10)
        .w_READPAR = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
          .link_1_1('Full')
        endif
        .w_CODCOM = .w_DEFCOM
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCOM))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_AT_STATO = 'P'
        .w_SELEZI = "D"
        .w_NPCOMP = 0
          .DoRTCalc(8,8,.f.)
        .w_TIPATT = 'A'
          .DoRTCalc(10,10,.f.)
        .w_CODATT = NVL(.w_ZoomDett.getVar('ATCODATT'), SPACE(15))
        .w_PERCOM = NVL(.w_ZoomDett.getVar('ATPERCOM'),0)
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .w_CPERCOM = NVL(.w_ZoomDett.getVar('UTPERCOM'), 0)
        .w_STAT1 = .w_AT_STATO
        .w_CALCOLA = .f.
        .w_OCPERCOM = 0
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .w_ATSTINIT = 'C'
        .w_AT__STATO = NVL(.w_ZoomDett.getVar('AT_STATO'), SPACE(1))
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_FIRST = .t.
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .w_APERCOM = NVL(.w_ZoomDett.getVar('ATPERCOM'), 0)
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .w_Riga = 1
        .w_OBTEST = i_DATSYS
        .w_TIPSTR = 'P'
        .w_TIPCOMS = 'P'
        .w_TIPCOMC = 'C'
          .DoRTCalc(26,26,.f.)
        .w_CODSTC = ' '
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CODSTC))
          .link_1_50('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomDett.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
    endwith
    this.DoRTCalc(28,28,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
        if .o_AT_STATO<>.w_AT_STATO.or. .o_CALCOLA<>.w_CALCOLA.or. .o_CODCOM<>.w_CODCOM
            .w_NPCOMP = 0
        endif
        .DoRTCalc(8,10,.t.)
            .w_CODATT = NVL(.w_ZoomDett.getVar('ATCODATT'), SPACE(15))
            .w_PERCOM = NVL(.w_ZoomDett.getVar('ATPERCOM'),0)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
            .w_CPERCOM = NVL(.w_ZoomDett.getVar('UTPERCOM'), 0)
            .w_STAT1 = .w_AT_STATO
        .DoRTCalc(15,15,.t.)
            .w_OCPERCOM = 0
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .DoRTCalc(17,17,.t.)
            .w_AT__STATO = NVL(.w_ZoomDett.getVar('AT_STATO'), SPACE(1))
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .DoRTCalc(19,19,.t.)
            .w_APERCOM = NVL(.w_ZoomDett.getVar('ATPERCOM'), 0)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .DoRTCalc(21,26,.t.)
        if .o_CODCOM<>.w_CODCOM
            .w_CODSTC = ' '
          .link_1_50('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomDett.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        * --- Area Manuale = Calculate
        * --- gspc_kas
        if .w_calcola
          .SaveDependsOn()
          .NotifyEvent('AggStato')
        endif
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.ZoomDett.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNPCOMP_1_8.enabled = this.oPgFrm.Page1.oPag.oNPCOMP_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDett.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATAINI = NVL(cp_ToDate(_Link_.CNDATINI),ctod("  /  /  "))
      this.w_DATAFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_DATAINI = ctod("  /  /  ")
      this.w_DATAFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!empty(.w_CODCOM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOM = space(15)
        this.w_DESCOM = space(30)
        this.w_DATAINI = ctod("  /  /  ")
        this.w_DATAFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSTC
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSTC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODSTC)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPSTR;
                     ,'ATCODATT',trim(this.w_CODSTC))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSTC)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSTC) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODSTC_1_50'),i_cWhere,'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPSTR<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSTC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODSTC);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPSTR;
                       ,'ATCODATT',this.w_CODSTC)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSTC = NVL(_Link_.ATCODATT,space(15))
      this.w_DESSTC = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODSTC = space(15)
      endif
      this.w_DESSTC = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSTC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_2.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_2.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_3.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_3.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_4.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_4.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oAT_STATO_1_5.RadioValue()==this.w_AT_STATO)
      this.oPgFrm.Page1.oPag.oAT_STATO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_7.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNPCOMP_1_8.value==this.w_NPCOMP)
      this.oPgFrm.Page1.oPag.oNPCOMP_1_8.value=this.w_NPCOMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_13.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_13.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSTC_1_48.value==this.w_DESSTC)
      this.oPgFrm.Page1.oPag.oDESSTC_1_48.value=this.w_DESSTC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSTC_1_50.value==this.w_CODSTC)
      this.oPgFrm.Page1.oPag.oCODSTC_1_50.value=this.w_CODSTC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODCOM)) or not(!empty(.w_CODCOM)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NPCOMP>=0 and .w_NPCOMP<=100)  and (.w_AT_STATO $ 'Z-L')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNPCOMP_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La percentuale di completamento deve essere compresa tra 0 e 100")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODCOM = this.w_CODCOM
    this.o_AT_STATO = this.w_AT_STATO
    this.o_CALCOLA = this.w_CALCOLA
    return

enddefine

* --- Define pages as container
define class tgspc_kasPag1 as StdContainer
  Width  = 811
  height = 487
  stdWidth  = 811
  stdheight = 487
  resizeXpos=372
  resizeYpos=326
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM_1_2 as StdField with uid="NPPVNNMOBE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 95601882,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=86, Top=6, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
      if .not. empty(.w_CODSTC)
        bRes2=.link_1_50('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDATAINI_1_3 as StdField with uid="WJVHPFIXKH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale intervallo inizio attivit�",;
    HelpContextID = 183250230,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=521, Top=6

  add object oDATAFIN_1_4 as StdField with uid="KFCMKASRRM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale intervallo inizio attivit�",;
    HelpContextID = 172217034,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=676, Top=6


  add object oAT_STATO_1_5 as StdCombo with uid="PAJZTSZERA",rtseq=5,rtrep=.f.,left=88,top=55,width=101,height=21;
    , ToolTipText = "Stato dell'attivit�";
    , HelpContextID = 246345813;
    , cFormVar="w_AT_STATO",RowSource=""+"Provvisoria,"+"Confermata,"+"Pianificata,"+"Lanciata,"+"Completata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAT_STATO_1_5.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'L',;
    iif(this.value =5,'F',;
    space(1)))))))
  endfunc
  func oAT_STATO_1_5.GetRadio()
    this.Parent.oContained.w_AT_STATO = this.RadioValue()
    return .t.
  endfunc

  func oAT_STATO_1_5.SetRadio()
    this.Parent.oContained.w_AT_STATO=trim(this.Parent.oContained.w_AT_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_AT_STATO=='P',1,;
      iif(this.Parent.oContained.w_AT_STATO=='C',2,;
      iif(this.Parent.oContained.w_AT_STATO=='Z',3,;
      iif(this.Parent.oContained.w_AT_STATO=='L',4,;
      iif(this.Parent.oContained.w_AT_STATO=='F',5,;
      0)))))
  endfunc


  add object oBtn_1_6 as StdButton with uid="BKBEDBPNKS",left=758, top=6, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare";
    , HelpContextID = 27418877;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        .NotifyEvent('Reload')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (! Empty(Nvl(.w_CODCOM,'')))
      endwith
    endif
  endfunc

  add object oSELEZI_1_7 as StdRadio with uid="XBGVNIVMJH",rtseq=6,rtrep=.f.,left=9, top=432, width=135,height=35;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_7.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 151014874
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 151014874
      this.Buttons(2).Top=16
      this.SetAll("Width",133)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_7.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_7.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_7.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  add object oNPCOMP_1_8 as StdField with uid="TJBQEEYFJS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NPCOMP", cQueryName = "NPCOMP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La percentuale di completamento deve essere compresa tra 0 e 100",;
    ToolTipText = "Percentuale di completamento di default da impostare in base alla selezione dello zoom",;
    HelpContextID = 46584618,;
   bGlobalFont=.t.,;
    Height=22, Width=39, Left=546, Top=437, cSayPict='"999"', cGetPict='"999"'

  func oNPCOMP_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AT_STATO $ 'Z-L')
    endwith
   endif
  endfunc

  func oNPCOMP_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NPCOMP>=0 and .w_NPCOMP<=100)
    endwith
    return bRes
  endfunc


  add object oBtn_1_9 as StdButton with uid="DPBHEQGAZE",left=588, top=437, width=23,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Applica la percentuale di completamento di default";
    , HelpContextID = 132597546;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSPC_BAS(this.Parent.oContained,"APP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_NPCOMP>0 and .w_NPCOMP<=100)
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="PWCUCFWQQP",left=707, top=436, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Conferma l'aggiornamento dello stato delle attivit�";
    , HelpContextID = 132769818;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSPC_BAS(this.Parent.oContained,"UPD")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (! Empty(Nvl(.w_CODCOM,'')) and ! Empty(Nvl(.w_CODATT,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="LNMWHHBLEZ",left=758, top=436, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 125481146;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCOM_1_13 as StdField with uid="ECNMNUQHAS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 95542986,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=220, Top=6, InputMask=replicate('X',30)


  add object oObj_1_18 as cp_runprogram with uid="UNKFJAVVYB",left=0, top=519, width=246,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSPC_BAS("SEL_DESEL")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 45199078


  add object oObj_1_20 as cp_runprogram with uid="YFANYJOAJZ",left=0, top=540, width=246,height=20,;
    caption='GSPC_BAS',;
   bGlobalFont=.t.,;
    prg="GSPC_BAS('P')",;
    cEvent = "w_CPERCOM Changed",;
    nPag=1;
    , HelpContextID = 263323719


  add object oObj_1_27 as cp_runprogram with uid="EAQDTPAQHD",left=0, top=561, width=246,height=20,;
    caption='GSPC_BAS',;
   bGlobalFont=.t.,;
    prg="GSPC_BAS('RQY')",;
    cEvent = "w_AT_STATO Changed, w_CODCOM Changed, w_CODSTC Changed",;
    nPag=1;
    , HelpContextID = 263323719


  add object oObj_1_30 as cp_runprogram with uid="VCEUQAKZRM",left=251, top=520, width=246,height=19,;
    caption='GSPC_BAS("DRC")',;
   bGlobalFont=.t.,;
    prg='GSPC_BAS("DRC")',;
    cEvent = "ZOOMDETT row checked",;
    nPag=1;
    , HelpContextID = 153852871


  add object oObj_1_31 as cp_runprogram with uid="DFAMEIYEZF",left=251, top=540, width=246,height=19,;
    caption='GSPC_BAS("DRU")',;
   bGlobalFont=.t.,;
    prg='GSPC_BAS("DRU")',;
    cEvent = "ZOOMDETT row unchecked",;
    nPag=1;
    , HelpContextID = 134978503


  add object oObj_1_33 as cp_runprogram with uid="XWDJPMKXGS",left=0, top=582, width=246,height=20,;
    caption='GSPC_BAS',;
   bGlobalFont=.t.,;
    prg="GSPC_BAS('TITLE')",;
    cEvent = "FormLoad",;
    nPag=1;
    , HelpContextID = 263323719


  add object oObj_1_35 as cp_runprogram with uid="OSMKFPFMFI",left=251, top=580, width=246,height=19,;
    caption='GSPC_BAS("AGST")',;
   bGlobalFont=.t.,;
    prg='GSPC_BAS("AGST")',;
    cEvent = "AggStato",;
    nPag=1;
    , HelpContextID = 164181090


  add object oBtn_1_44 as StdButton with uid="XKUFOZDVRH",left=276, top=436, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Apre l'anagrafica dell'attivit� selezionata";
    , HelpContextID = 173977185;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        do GSPC_BM2 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_44.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (! Empty(Nvl(.w_CODCOM,'')) and ! Empty(Nvl(.w_CODATT,'')))
      endwith
    endif
  endfunc

  add object oDESSTC_1_48 as StdField with uid="JIGETEVBJR",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESSTC", cQueryName = "DESSTC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 257023690,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=221, Top=30, InputMask=replicate('X',30)

  add object oCODSTC_1_50 as StdField with uid="ZMNSHCBPNA",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CODSTC", cQueryName = "CODSTC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Estrae le attivit� appartenenti questo nodo ( vuoto tutta la struttura )",;
    HelpContextID = 257082586,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=87, Top=30, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPSTR", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODSTC"

  func oCODSTC_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSTC_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSTC_1_50.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPSTR)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODSTC_1_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODSTC_1_50.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM
    i_obj.ATTIPATT=w_TIPSTR
     i_obj.w_ATCODATT=this.parent.oContained.w_CODSTC
     i_obj.ecpSave()
  endproc


  add object ZoomDett as cp_szoombox with uid="DOCUKGPXAE",left=1, top=80, width=815,height=347,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ATTIVITA",cZoomFile="GSPC_KAS",bOptions=.f.,bAdvancedOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.f.,cZoomOnZoom="GSPC_AAT",bAdvOptions=.t.,cMenuFile="",;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 45199078


  add object oObj_1_53 as cp_runprogram with uid="IWNKFNTFSI",left=251, top=560, width=246,height=19,;
    caption='GSPC_BAS',;
   bGlobalFont=.t.,;
    prg="GSPC_BAS('RQF')",;
    cEvent = "Reload",;
    nPag=1;
    , HelpContextID = 263323719


  add object oObj_1_54 as cp_runprogram with uid="EUJGEXYSUV",left=0, top=605, width=185,height=22,;
    caption='GSPC_BM2',;
   bGlobalFont=.t.,;
    prg="GSPC_BM2",;
    cEvent = "w_zoomdett selected",;
    nPag=1;
    , HelpContextID = 263323752

  add object oStr_1_12 as StdString with uid="WWJWWYRUJS",Visible=.t., Left=4, Top=7,;
    Alignment=1, Width=78, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RBZLTWAVGA",Visible=.t., Left=4, Top=58,;
    Alignment=1, Width=78, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="EIUSVLXSHK",Visible=.t., Left=446, Top=7,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="XVVCILANXO",Visible=.t., Left=608, Top=7,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="HJEXAUXSHP",Visible=.t., Left=382, Top=439,;
    Alignment=1, Width=159, Height=18,;
    Caption="% Completamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="GHRXHKRNEC",Visible=.t., Left=703, Top=62,;
    Alignment=2, Width=98, Height=18,;
    Caption="Completata"    , BackStyle=1, BackColor=rgb(254,80,111);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="CWZJEMIGKA",Visible=.t., Left=299, Top=62,;
    Alignment=2, Width=98, Height=18,;
    Caption="Provvisoria"    , BackStyle=1, BackColor=rgb(255,255,160);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="BSOZVBTECX",Visible=.t., Left=400, Top=62,;
    Alignment=2, Width=98, Height=18,;
    Caption="Confermata"    , BackStyle=1, BackColor=rgb(160,255,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="WKMLYTKXTF",Visible=.t., Left=501, Top=62,;
    Alignment=2, Width=98, Height=18,;
    Caption="Pianificata"    , BackStyle=1, BackColor=rgb(160,255,160);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="NSOODWDWPZ",Visible=.t., Left=602, Top=62,;
    Alignment=2, Width=98, Height=18,;
    Caption="Lanciata"    , BackStyle=1, BackColor=rgb(174,192,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="RUFSRHENUN",Visible=.t., Left=4, Top=32,;
    Alignment=1, Width=78, Height=18,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_kas','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
