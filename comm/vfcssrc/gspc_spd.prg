* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_spd                                                        *
*              Parametri di default                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_130]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-27                                                      *
* Last revis.: 2014-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_spd",oParentObject))

* --- Class definition
define class tgspc_spd as StdForm
  Top    = -3
  Left   = 3

  * --- Standard Properties
  Width  = 494
  Height = 425
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-19"
  HelpContextID=141187177
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  MAGAZZIN_IDX = 0
  UNIMIS_IDX = 0
  CENCOST_IDX = 0
  CAM_AGAZ_IDX = 0
  cPrg = "gspc_spd"
  cComment = "Parametri di default"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODCOM = space(15)
  w_CODMAG = space(5)
  w_UMCOMM = space(3)
  w_CENCOS = space(15)
  w_CAUMAG = space(5)
  w_PROGR = space(1)
  w_MSPROJ = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_DESMAG = space(30)
  w_DESUM = space(35)
  w_SVCPROJ = space(1)
  w_PRJREL = space(9)
  w_PRJMOD = space(200)
  w_PRJODBC = space(50)
  w_PRJUSR = space(254)
  w_PRJPWD = space(254)
  w_RICART = space(1)
  w_DESCEN = space(40)
  w_LOGTEC = space(1)
  w_NUMLIV = 0
  w_DESCAMAG = space(35)
  w_FLCOMM = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_spdPag1","gspc_spd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='CENCOST'
    this.cWorkTables[5]='CAM_AGAZ'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSPC_BPD(this,"Button")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODCOM=space(15)
      .w_CODMAG=space(5)
      .w_UMCOMM=space(3)
      .w_CENCOS=space(15)
      .w_CAUMAG=space(5)
      .w_PROGR=space(1)
      .w_MSPROJ=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_DESMAG=space(30)
      .w_DESUM=space(35)
      .w_SVCPROJ=space(1)
      .w_PRJREL=space(9)
      .w_PRJMOD=space(200)
      .w_PRJODBC=space(50)
      .w_PRJUSR=space(254)
      .w_PRJPWD=space(254)
      .w_RICART=space(1)
      .w_DESCEN=space(40)
      .w_LOGTEC=space(1)
      .w_NUMLIV=0
      .w_DESCAMAG=space(35)
      .w_FLCOMM=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODCOM))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODMAG))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_UMCOMM))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CENCOS))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CAUMAG))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
          .DoRTCalc(9,11,.f.)
        .w_SVCPROJ = .w_MSPROJ
        .w_PRJREL = '2000'
          .DoRTCalc(14,17,.f.)
        .w_RICART = 'N'
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate('*')
          .DoRTCalc(19,19,.f.)
        .w_LOGTEC = ' '
    endwith
    this.DoRTCalc(21,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate('*')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate('*')
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCENCOS_1_4.enabled = this.oPgFrm.Page1.oPag.oCENCOS_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPRJREL_1_20.visible=!this.oPgFrm.Page1.oPag.oPRJREL_1_20.mHide()
    this.oPgFrm.Page1.oPag.oPRJMOD_1_21.visible=!this.oPgFrm.Page1.oPag.oPRJMOD_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oPRJODBC_1_24.visible=!this.oPgFrm.Page1.oPag.oPRJODBC_1_24.mHide()
    this.oPgFrm.Page1.oPag.oPRJUSR_1_25.visible=!this.oPgFrm.Page1.oPag.oPRJUSR_1_25.mHide()
    this.oPgFrm.Page1.oPag.oPRJPWD_1_26.visible=!this.oPgFrm.Page1.oPag.oPRJPWD_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCOM
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_1'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_2'),i_cWhere,'',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UMCOMM
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UMCOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_UMCOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_UMCOMM))
          select UMCODICE,UMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UMCOMM)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UMCOMM) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oUMCOMM_1_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UMCOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UMCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UMCOMM)
            select UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UMCOMM = NVL(_Link_.UMCODICE,space(3))
      this.w_DESUM = NVL(_Link_.UMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_UMCOMM = space(3)
      endif
      this.w_DESUM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UMCOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENCOS
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CENCOS))
          select CC_CONTO,CCDESPIA,CCNUMLIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCENCOS_1_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENCOS)
            select CC_CONTO,CCDESPIA,CCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
      this.w_NUMLIV = NVL(_Link_.CCNUMLIV,0)
    else
      if i_cCtrl<>'Load'
        this.w_CENCOS = space(15)
      endif
      this.w_DESCEN = space(40)
      this.w_NUMLIV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUMAG
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CAUMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCOMM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CAUMAG))
          select CMCODICE,CMDESCRI,CMFLCOMM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCAUMAG_1_5'),i_cWhere,'',"Elenco causali magazzino",'GSPC_CAU.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCOMM";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUMAG)
            select CMCODICE,CMDESCRI,CMFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAMAG = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLCOMM = NVL(_Link_.CMFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUMAG = space(5)
      endif
      this.w_DESCAMAG = space(35)
      this.w_FLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=nvl(.w_FLCOMM,'N')='N' or empty(.w_FLCOMM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La causale di magazzino non deve gestire la commessa")
        endif
        this.w_CAUMAG = space(5)
        this.w_DESCAMAG = space(35)
        this.w_FLCOMM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_1.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_1.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_2.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_2.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oUMCOMM_1_3.value==this.w_UMCOMM)
      this.oPgFrm.Page1.oPag.oUMCOMM_1_3.value=this.w_UMCOMM
    endif
    if not(this.oPgFrm.Page1.oPag.oCENCOS_1_4.value==this.w_CENCOS)
      this.oPgFrm.Page1.oPag.oCENCOS_1_4.value=this.w_CENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUMAG_1_5.value==this.w_CAUMAG)
      this.oPgFrm.Page1.oPag.oCAUMAG_1_5.value=this.w_CAUMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGR_1_6.RadioValue()==this.w_PROGR)
      this.oPgFrm.Page1.oPag.oPROGR_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMSPROJ_1_7.RadioValue()==this.w_MSPROJ)
      this.oPgFrm.Page1.oPag.oMSPROJ_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_11.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_11.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_13.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_13.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUM_1_18.value==this.w_DESUM)
      this.oPgFrm.Page1.oPag.oDESUM_1_18.value=this.w_DESUM
    endif
    if not(this.oPgFrm.Page1.oPag.oPRJREL_1_20.RadioValue()==this.w_PRJREL)
      this.oPgFrm.Page1.oPag.oPRJREL_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRJMOD_1_21.value==this.w_PRJMOD)
      this.oPgFrm.Page1.oPag.oPRJMOD_1_21.value=this.w_PRJMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oPRJODBC_1_24.value==this.w_PRJODBC)
      this.oPgFrm.Page1.oPag.oPRJODBC_1_24.value=this.w_PRJODBC
    endif
    if not(this.oPgFrm.Page1.oPag.oPRJUSR_1_25.value==this.w_PRJUSR)
      this.oPgFrm.Page1.oPag.oPRJUSR_1_25.value=this.w_PRJUSR
    endif
    if not(this.oPgFrm.Page1.oPag.oPRJPWD_1_26.value==this.w_PRJPWD)
      this.oPgFrm.Page1.oPag.oPRJPWD_1_26.value=this.w_PRJPWD
    endif
    if not(this.oPgFrm.Page1.oPag.oRICART_1_27.RadioValue()==this.w_RICART)
      this.oPgFrm.Page1.oPag.oRICART_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCEN_1_34.value==this.w_DESCEN)
      this.oPgFrm.Page1.oPag.oDESCEN_1_34.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oLOGTEC_1_36.RadioValue()==this.w_LOGTEC)
      this.oPgFrm.Page1.oPag.oLOGTEC_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAMAG_1_43.value==this.w_DESCAMAG)
      this.oPgFrm.Page1.oPag.oDESCAMAG_1_43.value=this.w_DESCAMAG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_UMCOMM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUMCOMM_1_3.SetFocus()
            i_bnoObbl = !empty(.w_UMCOMM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(nvl(.w_FLCOMM,'N')='N' or empty(.w_FLCOMM))  and not(empty(.w_CAUMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUMAG_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La causale di magazzino non deve gestire la commessa")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgspc_spdPag1 as StdContainer
  Width  = 490
  height = 425
  stdWidth  = 490
  stdheight = 425
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM_1_1 as StdField with uid="AEJZAIQPWK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa di default",;
    HelpContextID = 164444966,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=126, Top=12, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCODMAG_1_2 as StdField with uid="FLOJWGLACI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di default (pianificazione)",;
    HelpContextID = 49756966,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=126, Top=41, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco magazzini",'',this.parent.oContained
  endproc

  add object oUMCOMM_1_3 as StdField with uid="MACYLZDOJX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_UMCOMM", cQueryName = "UMCOMM",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura per calcolo avanzamenti di commessa",;
    HelpContextID = 163129926,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=126, Top=70, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_UMCOMM"

  func oUMCOMM_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oUMCOMM_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUMCOMM_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oUMCOMM_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCENCOS_1_4 as StdField with uid="LBGSDPQBEB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CENCOS", cQueryName = "CENCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo (livello 99)",;
    HelpContextID = 265146662,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=126, Top=99, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENCOS"

  func oCENCOS_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oCENCOS_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCENCOS_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCENCOS_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCENCOS_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCAUMAG_1_5 as StdField with uid="EXBGBOPYUH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CAUMAG", cQueryName = "CAUMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "La causale di magazzino non deve gestire la commessa",;
    ToolTipText = "Causale magazzino di default (importa documenti)",;
    HelpContextID = 49823014,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=128, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", oKey_1_1="CMCODICE", oKey_1_2="this.w_CAUMAG"

  func oCAUMAG_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUMAG_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUMAG_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCAUMAG_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco causali magazzino",'GSPC_CAU.CAM_AGAZ_VZM',this.parent.oContained
  endproc

  add object oPROGR_1_6 as StdRadio with uid="PMIUUBNMKH",rtseq=6,rtrep=.f.,left=197, top=195, width=258,height=17;
    , ToolTipText = "Tipo di programmazione utilizzata (in avanti o all'indietro)";
    , cFormVar="w_PROGR", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPROGR_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="In avanti"
      this.Buttons(1).HelpContextID = 50204938
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("In avanti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="All'indietro"
      this.Buttons(2).HelpContextID = 50204938
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("All'indietro","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",17)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo di programmazione utilizzata (in avanti o all'indietro)")
      StdRadio::init()
    endproc

  func oPROGR_1_6.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'I',;
    space(1))))
  endfunc
  func oPROGR_1_6.GetRadio()
    this.Parent.oContained.w_PROGR = this.RadioValue()
    return .t.
  endfunc

  func oPROGR_1_6.SetRadio()
    this.Parent.oContained.w_PROGR=trim(this.Parent.oContained.w_PROGR)
    this.value = ;
      iif(this.Parent.oContained.w_PROGR=='A',1,;
      iif(this.Parent.oContained.w_PROGR=='I',2,;
      0))
  endfunc

  proc oPROGR_1_6.mDefault
    with this.Parent.oContained
      if empty(.w_PROGR)
        .w_PROGR = 'A'
      endif
    endwith
  endproc

  add object oMSPROJ_1_7 as StdCheck with uid="WFQSFPTBTU",rtseq=7,rtrep=.f.,left=52, top=221, caption="Utilizza MS-Project",;
    ToolTipText = "Usa\Non usa MS-Project per la tempificazione delle attivit�",;
    HelpContextID = 115146694,;
    cFormVar="w_MSPROJ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMSPROJ_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMSPROJ_1_7.GetRadio()
    this.Parent.oContained.w_MSPROJ = this.RadioValue()
    return .t.
  endfunc

  func oMSPROJ_1_7.SetRadio()
    this.Parent.oContained.w_MSPROJ=trim(this.Parent.oContained.w_MSPROJ)
    this.value = ;
      iif(this.Parent.oContained.w_MSPROJ=='S',1,;
      0)
  endfunc

  proc oMSPROJ_1_7.mDefault
    with this.Parent.oContained
      if empty(.w_MSPROJ)
        .w_MSPROJ = 'S'
      endif
    endwith
  endproc


  add object oObj_1_9 as cp_runprogram with uid="NXPIHBYZPB",left=540, top=44, width=146,height=19,;
    caption='GSPC_BPD(',;
   bGlobalFont=.t.,;
    prg='GSPC_BPD("Init")',;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Riempio la maschera e creo se non esiste un record per l'archivio";
    , HelpContextID = 3276246

  add object oDESCOM_1_11 as StdField with uid="NKWKSZUMQI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 164503862,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=257, Top=12, InputMask=replicate('X',30)

  add object oDESMAG_1_13 as StdField with uid="IUSIRFANUC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 49815862,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=187, Top=41, InputMask=replicate('X',30)

  add object oDESUM_1_18 as StdField with uid="ZHJBBBTPPU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESUM", cQueryName = "DESUM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 54517450,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=187, Top=70, InputMask=replicate('X',35)


  add object oPRJREL_1_20 as StdCombo with uid="WOKXFIKIMI",rtseq=13,rtrep=.f.,left=312,top=220,width=151,height=21;
    , ToolTipText = "I modelli sono supportati a partire dalla versione 2003";
    , HelpContextID = 138190582;
    , cFormVar="w_PRJREL",RowSource=""+"MS Project 98,"+"MS Project 2000/2002/2003,"+"MS Project da modello", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRJREL_1_20.RadioValue()
    return(iif(this.value =1,'98',;
    iif(this.value =2,'2000',;
    iif(this.value =3,'2003',;
    space(9)))))
  endfunc
  func oPRJREL_1_20.GetRadio()
    this.Parent.oContained.w_PRJREL = this.RadioValue()
    return .t.
  endfunc

  func oPRJREL_1_20.SetRadio()
    this.Parent.oContained.w_PRJREL=trim(this.Parent.oContained.w_PRJREL)
    this.value = ;
      iif(this.Parent.oContained.w_PRJREL=='98',1,;
      iif(this.Parent.oContained.w_PRJREL=='2000',2,;
      iif(this.Parent.oContained.w_PRJREL=='2003',3,;
      0)))
  endfunc

  func oPRJREL_1_20.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N')
    endwith
  endfunc

  add object oPRJMOD_1_21 as StdField with uid="LUJVECPQYL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PRJMOD", cQueryName = "PRJMOD",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso del file utilizzato per l'esportazione/importazione su MSProject",;
    HelpContextID = 14130934,;
   bGlobalFont=.t.,;
    Height=21, Width=326, Left=129, Top=248, InputMask=replicate('X',200)

  func oPRJMOD_1_21.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N' OR .w_PRJREL<>'2003')
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="QFYQHNIORB",left=460, top=249, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il file di MSProject";
    , HelpContextID = 140986154;
    , Tabstop=.f.;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .w_PRJMOD=getfile('mp?')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MSPROJ='N' OR .w_PRJREL<>'2003')
     endwith
    endif
  endfunc

  add object oPRJODBC_1_24 as StdField with uid="OKYTQBRRAP",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PRJODBC", cQueryName = "PRJODBC",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "User name di default per connessione al database di MS-Project",;
    HelpContextID = 237608694,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=129, Top=248, InputMask=replicate('X',50)

  func oPRJODBC_1_24.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N' OR .w_PRJREL='2003')
    endwith
  endfunc

  add object oPRJUSR_1_25 as StdField with uid="WLVSIBQYYI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PRJUSR", cQueryName = "PRJUSR",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "User name di default per connessione al database di MS-Project",;
    HelpContextID = 253730550,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=129, Top=274, cSayPict="repl('X',40)", cGetPict="repl('X',40)", InputMask=replicate('X',254)

  func oPRJUSR_1_25.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N' OR .w_PRJREL='2003')
    endwith
  endfunc

  add object oPRJPWD_1_26 as StdField with uid="PHJVKOYYTB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PRJPWD", cQueryName = "PRJPWD",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Password di default per connessione al database di MS-Project",;
    HelpContextID = 22716150,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=129, Top=300, cSayPict="repl('X',40)", cGetPict="repl('X',40)", InputMask=replicate('X',254)

  func oPRJPWD_1_26.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N' OR .w_PRJREL='2003')
    endwith
  endfunc

  add object oRICART_1_27 as StdCheck with uid="MBOHDDIIIJ",rtseq=18,rtrep=.f.,left=19, top=356, caption="Verifica tipologia ricavi",;
    ToolTipText = "Se attivo esclude dalla gestione ricavi commessa le righe documento aventi articoli con voce di ricavo senza tipologia",;
    HelpContextID = 16459286,;
    cFormVar="w_RICART", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRICART_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRICART_1_27.GetRadio()
    this.Parent.oContained.w_RICART = this.RadioValue()
    return .t.
  endfunc

  func oRICART_1_27.SetRadio()
    this.Parent.oContained.w_RICART=trim(this.Parent.oContained.w_RICART)
    this.value = ;
      iif(this.Parent.oContained.w_RICART=='S',1,;
      0)
  endfunc


  add object oBtn_1_29 as StdButton with uid="SEXWVKEUUD",left=382, top=374, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per settare i parametri";
    , HelpContextID = 141158426;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSPC_BPD(this.Parent.oContained,"Button")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_31 as cp_setobjprop with uid="HHZBSWMEYR",left=540, top=65, width=146,height=19,;
    caption='PRJPWD',;
   bGlobalFont=.t.,;
    cObj='w_PRJPWD',cProp='passwordchar',;
    nPag=1;
    , ToolTipText = "Per criptare la password";
    , HelpContextID = 22716150


  add object oBtn_1_32 as StdButton with uid="KOTWJAFPMC",left=433, top=374, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 133869754;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCEN_1_34 as StdField with uid="DHEEVPUHRB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 170795318,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=257, Top=99, InputMask=replicate('X',40)

  add object oLOGTEC_1_36 as StdCheck with uid="ALNBMBOHAZ",rtseq=20,rtrep=.f.,left=310, top=157, caption="Dettaglio tecnico",;
    ToolTipText = "Se attivo riporta sul log-errori il dettaglio tecnico dell'eventuale anomalia",;
    HelpContextID = 12686410,;
    cFormVar="w_LOGTEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLOGTEC_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oLOGTEC_1_36.GetRadio()
    this.Parent.oContained.w_LOGTEC = this.RadioValue()
    return .t.
  endfunc

  func oLOGTEC_1_36.SetRadio()
    this.Parent.oContained.w_LOGTEC=trim(this.Parent.oContained.w_LOGTEC)
    this.value = ;
      iif(this.Parent.oContained.w_LOGTEC=='S',1,;
      0)
  endfunc

  add object oDESCAMAG_1_43 as StdField with uid="ECUSOTIRAS",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCAMAG", cQueryName = "DESCAMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 149823869,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=190, Top=128, InputMask=replicate('X',35)

  add object oStr_1_10 as StdString with uid="MCVDCUFATZ",Visible=.t., Left=17, Top=12,;
    Alignment=1, Width=108, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="FAWGZLAXAQ",Visible=.t., Left=17, Top=41,;
    Alignment=1, Width=108, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="AZRJKOIYCA",Visible=.t., Left=4, Top=195,;
    Alignment=1, Width=185, Height=15,;
    Caption="Tipo di programmazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="SALKJEYGKL",Visible=.t., Left=5, Top=161,;
    Alignment=0, Width=174, Height=18,;
    Caption="Tempificazione attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="SQTBYZPNCQ",Visible=.t., Left=17, Top=71,;
    Alignment=1, Width=108, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="FHVQEGUXEW",Visible=.t., Left=220, Top=219,;
    Alignment=1, Width=87, Height=18,;
    Caption="Release:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="YCVCDOLPUR",Visible=.t., Left=3, Top=274,;
    Alignment=1, Width=123, Height=15,;
    Caption="User name:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N' OR .w_PRJREL='2003')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="VLRGWISBYY",Visible=.t., Left=3, Top=300,;
    Alignment=1, Width=123, Height=15,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N' OR .w_PRJREL='2003')
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="QIJCRTANJE",Visible=.t., Left=3, Top=250,;
    Alignment=1, Width=123, Height=18,;
    Caption="Data source:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N' OR .w_PRJREL='2003')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="XLOUBQDFMV",Visible=.t., Left=17, Top=102,;
    Alignment=1, Width=108, Height=15,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="LZJTMBLWDG",Visible=.t., Left=221, Top=160,;
    Alignment=1, Width=84, Height=15,;
    Caption="Log errori:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="LGHGCUVDOV",Visible=.t., Left=3, Top=250,;
    Alignment=1, Width=123, Height=18,;
    Caption="Modello MSProject:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N' OR .w_PRJREL<>'2003')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="LRNHUBTWHJ",Visible=.t., Left=8, Top=330,;
    Alignment=0, Width=170, Height=18,;
    Caption="Gestione ricavi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="LWGAHYQUWM",Visible=.t., Left=-2, Top=129,;
    Alignment=1, Width=123, Height=22,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  add object oBox_1_16 as StdBox with uid="IIUCNKWEEW",left=2, top=182, width=477,height=2

  add object oBox_1_41 as StdBox with uid="NIJIADTCDM",left=8, top=351, width=432,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_spd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
