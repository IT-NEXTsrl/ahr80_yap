* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bmm                                                        *
*              Lancia mov. da attivita                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_56]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-27                                                      *
* Last revis.: 2006-02-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bmm",oParentObject)
return(i_retval)

define class tgspc_bmm as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_OBJMOV = .NULL.
  w_PARMOV = .f.
  w_ATTDES = space(30)
  w_CENCOS = space(15)
  w_PIANIF = space(1)
  w_CODFAM = space(5)
  w_DESCOM = space(30)
  w_DTOBSO = ctod("  /  /  ")
  w_VALCOM = space(3)
  * --- WorkFile variables
  MAT_MAST_idx=0
  ATTIVITA_idx=0
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia il Movimento Preventivo Associato all'Attivita. (Lanciato da GSPC_AAT e GSPC_SGS)
    * --- Quindi dalla gestione Attivit� e dalla gestione progetto.
    * --- Se non esiste apre la Gestione in Carica con alcuni parametri settati
    * --- Variabili aggiunte per simulare il link sul codice attivit�
    * --- Read from CAN_TIER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNCODVAL,CNDESCAN,CNDTOBSO"+;
        " from "+i_cTable+" CAN_TIER where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_ATCODCOM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNCODVAL,CNDESCAN,CNDTOBSO;
        from (i_cTable) where;
            CNCODCAN = this.oParentObject.w_ATCODCOM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VALCOM = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
      this.w_DESCOM = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
      this.w_DTOBSO = NVL(cp_ToDate(_read_.CNDTOBSO),cp_NullValue(_read_.CNDTOBSO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- visualizzo anche gli importi e il listino
    this.w_PARMOV = "NP"
    * --- Cerco il movimento Preventivo associato all'attivit� se c'�
    this.w_SERIAL = ""
    * --- Select from MAT_MAST
    i_nConn=i_TableProp[this.MAT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MASERIAL  from "+i_cTable+" MAT_MAST ";
          +" where MACODCOM="+cp_ToStrODBC(this.oParentObject.w_ATCODCOM)+" AND MACODATT="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+" AND MATIPMOV='P'";
           ,"_Curs_MAT_MAST")
    else
      select MASERIAL from (i_cTable);
       where MACODCOM=this.oParentObject.w_ATCODCOM AND MACODATT=this.oParentObject.w_ATCODATT AND MATIPMOV="P";
        into cursor _Curs_MAT_MAST
    endif
    if used('_Curs_MAT_MAST')
      select _Curs_MAT_MAST
      locate for 1=1
      do while not(eof())
      * --- Leggo il seriale se c'�
      this.w_SERIAL = _Curs_MAT_MAST.MASERIAL
        select _Curs_MAT_MAST
        continue
      enddo
      use
    endif
    * --- Controllo data di Obsolescenza della Commessa
    if empty(this.w_SERIAL) and this.w_DTOBSO<i_DATSYS and not empty(dtos(this.w_DTOBSO)) 
      ah_ErrorMsg("Codice commessa incongruente, inesistente oppure obsoleto.%0impossibile creare nuovi movimenti","stop","")
      i_retcode = 'stop'
      return
    endif
    this.w_OBJMOV = GSPC_MAT( this.w_PARMOV )
    * --- Testo subito se l'utente pu� aprire la gestione
    if !(this.w_OBJMOV.bSec1)
      i_retcode = 'stop'
      return
    endif
    if Empty ( this.w_SERIAL )
      * --- Lancio l'Oggetto in Caricamento
      * --- La Apro in Caricamento
      * --- Questo pezzo di codice � preso dal metodo ECPLOAD in CP_FORMS
      * --- QUESTO PER POTER INIZIALIZZARE ALCUNI VALORI NELLA GESTIONE
      this.w_OBJMOV.oFirstControl.SetFocus()     
      this.w_OBJMOV.cFunction = "Load"
      this.w_OBJMOV.ReleaseWarn()     
      this.w_OBJMOV.SetStatus()     
      this.w_OBJMOV.mEnableControls()     
      this.w_OBJMOV.BlankRec()     
      this.w_OBJMOV.InitAutonumber()     
      this.w_OBJMOV.NotifyEvent("New record")     
      this.w_OBJMOV.Refresh()     
      * --- Inizializzo alcuni dati in testata
      this.w_OBJMOV.w_MACODCOM = this.oParentObject.w_ATCODCOM
      this.w_OBJMOV.w_VALCOM = this.w_VALCOM
      this.w_OBJMOV.O_VALCOM = Space(3)
      this.w_OBJMOV.w_MATIPATT = "A"
      this.w_OBJMOV.w_MACODATT = this.oParentObject.w_ATCODATT
      this.w_OBJMOV.w_MATIPMOV = "P"
      * --- Eseguo una lettura per simulare il link su MACODATT che altrimenti non verrebbe eseguito
      * --- Read from ATTIVITA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM"+;
          " from "+i_cTable+" ATTIVITA where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_ATCODCOM);
              +" and ATTIPATT = "+cp_ToStrODBC("A");
              +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_ATCODATT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM;
          from (i_cTable) where;
              ATCODCOM = this.oParentObject.w_ATCODCOM;
              and ATTIPATT = "A";
              and ATCODATT = this.oParentObject.w_ATCODATT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ATTDES = NVL(cp_ToDate(_read_.ATDESCRI),cp_NullValue(_read_.ATDESCRI))
        this.w_CENCOS = NVL(cp_ToDate(_read_.ATCENCOS),cp_NullValue(_read_.ATCENCOS))
        this.w_PIANIF = NVL(cp_ToDate(_read_.AT_STATO),cp_NullValue(_read_.AT_STATO))
        this.w_CODFAM = NVL(cp_ToDate(_read_.ATCODFAM),cp_NullValue(_read_.ATCODFAM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = 'Impossibile leggere dalle ATTIVITA'
        return
      endif
      select (i_nOldArea)
      this.w_OBJMOV.w_ATTDES = this.w_ATTDES
      this.w_OBJMOV.w_CENCOS = this.w_CENCOS
      this.w_OBJMOV.w_PIANIF = this.w_PIANIF
      this.w_OBJMOV.w_CODFAM = this.w_CODFAM
      this.w_OBJMOV.w_COMDES = this.w_DESCOM
      * --- Setto i valori a video
      this.w_OBJMOV.SetControlsValue()     
      this.w_OBJMOV.mCalc(.t.)     
    else
      * --- Lancio l'Oggetto in Interrogazione
      this.w_OBJMOV.cFunction = "Query"
      this.w_OBJMOV.w_MASERIAL = this.w_SERIAL
      this.w_OBJMOV.QueryKeySet("MASERIAL='"+this.w_SERIAL+ "'","")     
      this.w_OBJMOV.LoadRecWarn()     
      * --- Per abilitare il tasto di modifica nella toolbar
      oCpToolBar.SetQuery()
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MAT_MAST'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='CAN_TIER'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_MAT_MAST')
      use in _Curs_MAT_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
