* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bvu                                                        *
*              Verifica UM manodopera                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_21]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-27                                                      *
* Last revis.: 2006-02-20                                                      *
*                                                                              *
* impedisce di associare a mov di m.o. uni di mis non definite in UMTEMPO      *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bvu",oParentObject,m.pPar)
return(i_retval)

define class tgspc_bvu as StdBatch
  * --- Local variables
  pPar = space(10)
  w_UMDFLT = space(3)
  w_TIPCOS = space(2)
  w_MESS = space(10)
  * --- WorkFile variables
  TIPCOSTO_idx=0
  CPAR_DEF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Unita' di Misura (da GSPC_MAT)
    do case
      case this.pPar="change"
        * --- da GSPC_MAT, on MAUNIMIS changed
        this.oParentObject.w_TESTUM = .T.
        * --- Verifico se il tipo di costo � ManoDopera
        * --- Read from TIPCOSTO
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIPCOSTO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIPCOSTO_idx,2],.t.,this.TIPCOSTO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TCTIPCOS"+;
            " from "+i_cTable+" TIPCOSTO where ";
                +"TCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MACODCOS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TCTIPCOS;
            from (i_cTable) where;
                TCCODICE = this.oParentObject.w_MACODCOS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCOS = NVL(cp_ToDate(_read_.TCTIPCOS),cp_NullValue(_read_.TCTIPCOS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = 'Impossibile leggere da TIPCOSTO'
          return
        endif
        select (i_nOldArea)
        if this.w_TIPCOS="MD"
          * --- Se � manodopera, l'unit� di misura:
          * --- - o � uguale al default definito nei parametri di commessa
          * --- - o � uguale ad una delle due unit� di misura definite sull'articolo
          * --- (altrimenti si segnala un errore)
          * --- Read from CPAR_DEF
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PDUMCOMM"+;
              " from "+i_cTable+" CPAR_DEF where ";
                  +"PDCHIAVE = "+cp_ToStrODBC("TAM");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PDUMCOMM;
              from (i_cTable) where;
                  PDCHIAVE = "TAM";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UMDFLT = NVL(cp_ToDate(_read_.PDUMCOMM),cp_NullValue(_read_.PDUMCOMM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = 'Impossibile leggere da CPAR_DEF'
            return
          endif
          select (i_nOldArea)
          if nvl(this.oParentObject.w_MAUNIMIS,"")<>this.w_UMDFLT and nvl(this.oParentObject.w_UNMIS1,"")<>this.w_UMDFLT and nvl(this.oParentObject.w_UNMIS2,"")<>this.w_UMDFLT
            * --- L'unit� di misura del movimento e quelle dell'articolo non corrispondono a quella definita nei parametri di default
            ah_ErrorMsg("Inserire una unit� di misura in accordo al default di commessa","!","")
            * --- Impedisce l'F10 nei movimenti di commessa
            this.oParentObject.w_TESTUM = .F.
          endif
        endif
      case this.pPar="F10"
        * --- da GSPC_MAT, on F10
        this.w_MESS = "Unit� di misura non valida"
        if this.oParentObject.w_TESTUM=.F.
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
    endcase
  endproc


  proc Init(oParentObject,pPar)
    this.pPar=pPar
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='TIPCOSTO'
    this.cWorkTables[2]='CPAR_DEF'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar"
endproc
