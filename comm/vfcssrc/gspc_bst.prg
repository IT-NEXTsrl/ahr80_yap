* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bst                                                        *
*              Stampa struttura                                                *
*                                                                              *
*      Author: NICOLA GORLANDI                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_8]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-20                                                      *
* Last revis.: 2006-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bst",oParentObject)
return(i_retval)

define class tgspc_bst as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa struttura
    SELECT * FROM ( this.oParentObject.w_CURSORNA ) Order By LvlKey Into Cursor __TMP__ NoFilter
    * --- Selezioni nel report
    L_COMMESSA=this.oParentObject.w_CODCOM
    L_DESCOM=this.oParentObject.w_DESCOM
    L_STRUTTURA=IIF(this.oParentObject.w_TIPSTR="P",ah_MsgFormat("Amministrativa"),IIF(this.oParentObject.w_TIPSTR="T",ah_MsgFormat("Tecnica"),ah_MsgFormat("Gestionale")))
    CP_CHPRN("..\COMM\EXE\QUERY\GSPC_SGS.FRX","",this.oParentObject)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
