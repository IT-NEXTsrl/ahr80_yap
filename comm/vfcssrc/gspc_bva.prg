* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bva                                                        *
*              Variazione attivita                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_57]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-28                                                      *
* Last revis.: 2006-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bva",oParentObject,m.pPARAM)
return(i_retval)

define class tgspc_bva as StdBatch
  * --- Local variables
  pPARAM = space(4)
  w_OPERAZIONE = space(1)
  w_NUOVOIMP = 0
  w_CAMBIO = 0
  w_PADRE = .NULL.
  w_CTRL = .NULL.
  w_FLORCO = space(1)
  w_QTAEV1 = 0
  w_IMPEVA = 0
  w_DATGEN = ctod("  /  /  ")
  * --- WorkFile variables
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  MA_COSTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili della maschera
    do case
      case this.pPARAM="Init"
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVNUMDOC,MVALFDOC,MVDATDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVNUMDOC,MVALFDOC,MVDATDOC;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_SERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DOCNUM = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.oParentObject.w_DOCSER = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.oParentObject.w_DOCDATA = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo informazioni per aggiornare i saldi...
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPROWORD,MVFLORCO,MVFLCOCO,MVIMPCOM,MVCODCOS"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_ROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPROWORD,MVFLORCO,MVFLCOCO,MVIMPCOM,MVCODCOS;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_SERIAL;
                and CPROWNUM = this.oParentObject.w_ROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_RIGANUM = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
          this.oParentObject.w_FLAGORCO = NVL(cp_ToDate(_read_.MVFLORCO),cp_NullValue(_read_.MVFLORCO))
          this.oParentObject.w_FLAGCOCO = NVL(cp_ToDate(_read_.MVFLCOCO),cp_NullValue(_read_.MVFLCOCO))
          this.oParentObject.w_IMPORTO = NVL(cp_ToDate(_read_.MVIMPCOM),cp_NullValue(_read_.MVIMPCOM))
          this.oParentObject.w_COSTO = NVL(cp_ToDate(_read_.MVCODCOS),cp_NullValue(_read_.MVCODCOS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Sposto il focus sull'attivit�..
        *     L'attivit� come ordine di calcolo deve seguire la commessa (Link)
        *     ma alla prima apertura l'utente deve poter iniziare ad imputare dalla
        *     attivit�.
        this.w_PADRE = This.oParentObject
        this.w_CTRL = this.w_PADRE.GetCtrl("w_NUOVAATT")
        this.w_CTRL.SetFocus()     
      case this.pPARAM="Upda"
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVFLORCO,MVQTAEV1,MVIMPEVA,MVDATGEN"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_ROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVFLORCO,MVQTAEV1,MVIMPEVA,MVDATGEN;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_SERIAL;
                and CPROWNUM = this.oParentObject.w_ROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLORCO = NVL(cp_ToDate(_read_.MVFLORCO),cp_NullValue(_read_.MVFLORCO))
          this.w_QTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
          this.w_IMPEVA = NVL(cp_ToDate(_read_.MVIMPEVA),cp_NullValue(_read_.MVIMPEVA))
          this.w_DATGEN = NVL(cp_ToDate(_read_.MVDATGEN),cp_NullValue(_read_.MVDATGEN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Not Empty(this.w_FLORCO) And ( this.w_QTAEV1>0 OR this.w_IMPEVA>0 OR Not Empty(this.w_DATGEN) )
          ah_ErrorMsg("Impossibile variare una riga evasa che movimenta l'impegnato.","!","")
        else
          if this.oParentObject.w_OLDVAL=this.oParentObject.w_NUOVAVAL
            this.w_NUOVOIMP = this.oParentObject.w_IMPORTO
          else
            * --- Nel caso cambio commessa e le commesse hanno valute diverse
            *     (da Lit a Eur)
            *     La funzione CAIMPCOM considera anche le diverse valute
            this.w_CAMBIO = GETCAM(this.oParentObject.w_OLDVAL,this.oParentObject.w_DOCDATA,0)
            this.w_NUOVOIMP = CAIMPCOM( IIF(Empty(this.oParentObject.w_NUOVACOM),"S", " "), this.oParentObject.w_OLDVAL, this.oParentObject.w_NUOVAVAL, this.oParentObject.w_IMPORTO, this.w_CAMBIO, this.oParentObject.w_DOCDATA, this.oParentObject.w_DECCOM ) 
          endif
          do case
            case this.oParentObject.w_FLAGORCO="+" Or this.oParentObject.w_FLAGORCO="-"
              * --- Storna l'importo dalla vecchia commessa /attivita
              this.w_OPERAZIONE = iif( this.oParentObject.w_FLAGORCO="+" , "-" , "+" )
              * --- Write into MA_COSTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MA_COSTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_OPERAZIONE,'CSORDIN','this.oParentObject.w_IMPORTO',this.oParentObject.w_IMPORTO,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CSORDIN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSORDIN');
                    +i_ccchkf ;
                +" where ";
                    +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
                    +" and CSTIPSTR = "+cp_ToStrODBC("A");
                    +" and CSCODMAT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                    +" and CSCODCOS = "+cp_ToStrODBC(this.oParentObject.w_COSTO);
                       )
              else
                update (i_cTable) set;
                    CSORDIN = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    CSCODCOM = this.oParentObject.w_CODCOM;
                    and CSTIPSTR = "A";
                    and CSCODMAT = this.oParentObject.w_CODATT;
                    and CSCODCOS = this.oParentObject.w_COSTO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Try
              local bErr_033CB238
              bErr_033CB238=bTrsErr
              this.Try_033CB238()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_033CB238
              * --- End
              * --- Write into MA_COSTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MA_COSTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                i_cOp1=cp_SetTrsOp(this.oParentObject.w_FLAGORCO,'CSORDIN','this.w_NUOVOIMP',this.w_NUOVOIMP,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CSORDIN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSORDIN');
                    +i_ccchkf ;
                +" where ";
                    +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_NUOVACOM);
                    +" and CSTIPSTR = "+cp_ToStrODBC("A");
                    +" and CSCODMAT = "+cp_ToStrODBC(this.oParentObject.w_NUOVAATT);
                    +" and CSCODCOS = "+cp_ToStrODBC(this.oParentObject.w_COSTO);
                       )
              else
                update (i_cTable) set;
                    CSORDIN = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    CSCODCOM = this.oParentObject.w_NUOVACOM;
                    and CSTIPSTR = "A";
                    and CSCODMAT = this.oParentObject.w_NUOVAATT;
                    and CSCODCOS = this.oParentObject.w_COSTO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.oParentObject.w_FLAGCOCO="+" Or this.oParentObject.w_FLAGCOCO="-"
              * --- Storno il consuntivo sulla vecchia commessa / attivit�
              this.w_OPERAZIONE = iif( this.oParentObject.w_FLAGCOCO="+" , "-" , "+" )
              * --- Write into MA_COSTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MA_COSTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_OPERAZIONE,'CSCONSUN','this.oParentObject.w_IMPORTO',this.oParentObject.w_IMPORTO,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
                    +i_ccchkf ;
                +" where ";
                    +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
                    +" and CSTIPSTR = "+cp_ToStrODBC("A");
                    +" and CSCODMAT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                    +" and CSCODCOS = "+cp_ToStrODBC(this.oParentObject.w_COSTO);
                       )
              else
                update (i_cTable) set;
                    CSCONSUN = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    CSCODCOM = this.oParentObject.w_CODCOM;
                    and CSTIPSTR = "A";
                    and CSCODMAT = this.oParentObject.w_CODATT;
                    and CSCODCOS = this.oParentObject.w_COSTO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Try
              local bErr_033CB6E8
              bErr_033CB6E8=bTrsErr
              this.Try_033CB6E8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_033CB6E8
              * --- End
              * --- Aggiorno il saldo...
              * --- Write into MA_COSTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MA_COSTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                i_cOp1=cp_SetTrsOp(this.oParentObject.w_FLAGCOCO,'CSCONSUN','this.w_NUOVOIMP',this.w_NUOVOIMP,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
                    +i_ccchkf ;
                +" where ";
                    +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_NUOVACOM);
                    +" and CSTIPSTR = "+cp_ToStrODBC("A");
                    +" and CSCODMAT = "+cp_ToStrODBC(this.oParentObject.w_NUOVAATT);
                    +" and CSCODCOS = "+cp_ToStrODBC(this.oParentObject.w_COSTO);
                       )
              else
                update (i_cTable) set;
                    CSCONSUN = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    CSCODCOM = this.oParentObject.w_NUOVACOM;
                    and CSTIPSTR = "A";
                    and CSCODMAT = this.oParentObject.w_NUOVAATT;
                    and CSCODCOS = this.oParentObject.w_COSTO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
          endcase
          * --- AGGIORNA IL DOCUMENTO
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUOVAATT),'DOC_DETT','MVCODATT');
            +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUOVACOM),'DOC_DETT','MVCODCOM');
            +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_NUOVOIMP),'DOC_DETT','MVIMPCOM');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_ROWNUM);
                   )
          else
            update (i_cTable) set;
                MVCODATT = this.oParentObject.w_NUOVAATT;
                ,MVCODCOM = this.oParentObject.w_NUOVACOM;
                ,MVIMPCOM = this.w_NUOVOIMP;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.oParentObject.w_SERIAL;
                and CPROWNUM = this.oParentObject.w_ROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          ah_ErrorMsg("Aggiornamento completato","I","")
          * --- Aggiorno l'elenco della maschera Mastri di commessa (se aperta)
          if Type( "this.oParentObject.oParentObject.Caption" )="C"
            do GSPC_BLM with this.oParentObject.oParentObject, "Requery"
          endif
        endif
    endcase
  endproc
  proc Try_033CB238()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Crea il nuovo saldo attivit� se non esiste...
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+",CSPREVEN"+",CSCONSUN"+",CSORDIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUOVACOM),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC("A"),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUOVAATT),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COSTO),'MA_COSTI','CSCODCOS');
      +","+cp_NullLink(cp_ToStrODBC(0),'MA_COSTI','CSPREVEN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MA_COSTI','CSCONSUN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MA_COSTI','CSORDIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.oParentObject.w_NUOVACOM,'CSTIPSTR',"A",'CSCODMAT',this.oParentObject.w_NUOVAATT,'CSCODCOS',this.oParentObject.w_COSTO,'CSPREVEN',0,'CSCONSUN',0,'CSORDIN',0)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS,CSPREVEN,CSCONSUN,CSORDIN &i_ccchkf. );
         values (;
           this.oParentObject.w_NUOVACOM;
           ,"A";
           ,this.oParentObject.w_NUOVAATT;
           ,this.oParentObject.w_COSTO;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_033CB6E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Creo il nuovo saldo se non esiste...
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+",CSPREVEN"+",CSCONSUN"+",CSORDIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUOVACOM),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC("A"),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUOVAATT),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COSTO),'MA_COSTI','CSCODCOS');
      +","+cp_NullLink(cp_ToStrODBC(0),'MA_COSTI','CSPREVEN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MA_COSTI','CSCONSUN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MA_COSTI','CSORDIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.oParentObject.w_NUOVACOM,'CSTIPSTR',"A",'CSCODMAT',this.oParentObject.w_NUOVAATT,'CSCODCOS',this.oParentObject.w_COSTO,'CSPREVEN',0,'CSCONSUN',0,'CSORDIN',0)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS,CSPREVEN,CSCONSUN,CSORDIN &i_ccchkf. );
         values (;
           this.oParentObject.w_NUOVACOM;
           ,"A";
           ,this.oParentObject.w_NUOVAATT;
           ,this.oParentObject.w_COSTO;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='MA_COSTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
