* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bmk                                                        *
*              Controlli finali movimenti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_58]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-27                                                      *
* Last revis.: 2016-04-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bmk",oParentObject,m.pPARA)
return(i_retval)

define class tgspc_bmk as StdBatch
  * --- Local variables
  pPARA = space(10)
  w_MESS = space(200)
  w_Obj = .NULL.
  w_AbsRow = 0
  w_nRelRow = 0
  w_RECPOS = 0
  w_STATO = space(1)
  w_GSPC_MAT = .NULL.
  * --- WorkFile variables
  ATTIVITA_idx=0
  MAT_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CONTROLLI MOVIMENTI (da GSPC_MAT)
    do case
      case this.pPARA="CHECK"
        * --- Controlli Svolti all'interno del Batch:
        if this.oParentObject.cFunction="Load" or this.oParentObject.cFunction="Edit"
          * --- Non si possono inserire righe con quantit� a zero
          select (this.oParentObject.cTrsName) 
 go top 
 this.oParentObject.WorkFromTrs() 
 this.oParentObject.SaveDependsOn() 
 count for t_cproword<>0 and !empty(t_macodkey) and t_maqtamov=0 and not deleted() to l_numrighe 
 calculate min(t_cproword) for t_cproword<>0 and !empty(t_macodkey) and t_maqtamov=0 ; 
 and not deleted() to l_nroword
          if l_numrighe=1
            this.w_MESS = ah_MsgFormat("Riga %1 con quantit� movimentata a 0. Impossibile salvare",alltrim(str(l_nroword)))
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if l_numrighe>1
            this.w_MESS = ah_MsgFormat("Esistono %1 righe con quantit� movimentata a 0. Impossibile salvare",alltrim(str(l_numrighe)))
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        * --- Se Movimento Preventivo verifico che non ve ne sia un altro con la solita attivit�
        if This.oParentoBject.cFunction<>"Edit"
          * --- Controllo che esista un solo movimento per l'attivit� (se non in cancellazione)
          if this.oParentObject.w_MATIPMOV="P"
            this.w_MESS = ""
            * --- Select from MAT_MAST
            i_nConn=i_TableProp[this.MAT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MASERIAL,MANUMREG,MAALFREG,MADATREG  from "+i_cTable+" MAT_MAST ";
                  +" where MASERIAL<>"+cp_ToStrODBC(this.oParentObject.w_MASERIAL)+" AND MACODATT="+cp_ToStrODBC(this.oParentObject.w_MACODATT)+" AND MATIPMOV='P' AND MACODCOM="+cp_ToStrODBC(this.oParentObject.w_MACODCOM)+"";
                   ,"_Curs_MAT_MAST")
            else
              select MASERIAL,MANUMREG,MAALFREG,MADATREG from (i_cTable);
               where MASERIAL<>this.oParentObject.w_MASERIAL AND MACODATT=this.oParentObject.w_MACODATT AND MATIPMOV="P" AND MACODCOM=this.oParentObject.w_MACODCOM;
                into cursor _Curs_MAT_MAST
            endif
            if used('_Curs_MAT_MAST')
              select _Curs_MAT_MAST
              locate for 1=1
              do while not(eof())
              this.w_MESS = ah_MsgFormat("Esiste gi� un movimento preventivo per questa attivit�.%0numero registrazione %1 / %2 del %3",alltrim(str(_Curs_MAT_MAST.MANUMREG)),_Curs_MAT_MAST.MAALFREG,dtoc(_Curs_MAT_MAST.MADATREG))
              * --- Esco dal Ciclo While
              Exit
                select _Curs_MAT_MAST
                continue
              enddo
              use
            endif
            if Not Empty( this.w_MESS )
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
        * --- Se attivit� pianificata non permetto la modifica
        if this.oParentObject.w_PIANIF="S"
          this.w_MESS = ah_MsgFormat("Movimento di commessa legato ad una attivit� pianificata.%0impossibile modificare o cancellare")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pPARA="CHGCOMM" Or this.pPARA="CHGVAL" Or this.pPARA="CHGCAO"
        this.w_GSPC_MAT = this.oParentObject
        this.w_GSPC_MAT.MarkPos()     
        this.w_GSPC_MAT.FirstRow()     
        do while Not this.w_GSPC_MAT.Eof_Trs()
          this.w_GSPC_MAT.SetRow()     
          * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
          *     se non cancellate
          if this.w_GSPC_MAT.FullRow()
            if this.pPARA="CHGVAL"
              * --- Se cambia la Moneta ricalcolo il cambio
              this.oParentObject.w_MACAOVAL = GETCAM(this.oParentObject.w_MACODVAL, this.oParentObject.w_MADATREG, 7)
              * --- Azzero il listino
              this.oParentObject.w_MACODLIS = SPACE(5)
              this.oParentObject.w_LISDES = SPACE(40)
            endif
            this.oParentObject.w_MACOCOMS = this.oParentObject.w_MACODCOM
            this.oParentObject.w_MACOATTS = this.oParentObject.w_MACODATT
            this.oParentObject.w_MACODFAM = this.oParentObject.w_CODFAM
            if this.pPARA="CHGVAL" Or this.pPARA="CHGCAO"
              this.oParentObject.w_TOTDOC = this.oParentObject.w_TOTDOC - this.oParentObject.w_MAVALRIG
              this.oParentObject.w_TOTMOV = this.oParentObject.w_TOTMOV - this.oParentObject.w_TOTRIGA
              this.oParentObject.w_MAVALRIG = IIF(EMPTY(CP_TODATE(this.oParentObject.w_MADATANN)), cp_ROUND(VAL2MON(this.oParentObject.w_MAPREZZO,this.oParentObject.w_MACAOVAL,this.oParentObject.w_CAOCOM,g_DATEUR,this.oParentObject.w_VALCOM),this.oParentObject.w_DECCOM)*this.oParentObject.w_MAQTA1UM, 0)
              this.oParentObject.w_TOTRIGA = IIF(EMPTY(CP_TODATE(this.oParentObject.w_MADATANN)), cp_Round(this.oParentObject.w_MAPREZZO*this.oParentObject.w_MAQTAMOV, this.oParentObject.w_DECTOT), 0)
              this.oParentObject.w_TOTDOC = this.oParentObject.w_TOTDOC + this.oParentObject.w_MAVALRIG
              this.oParentObject.w_TOTMOV = this.oParentObject.w_TOTMOV + this.oParentObject.w_TOTRIGA
            endif
            this.w_GSPC_MAT.SaveRow()     
          endif
          this.w_GSPC_MAT.NextRow()     
        enddo
        this.w_GSPC_MAT.RePos(.t.)     
      case this.pPARA="HasEvent"
        * --- Inibisce la cancellazione e la modifica se l'attivit� � pianificata (preventivo) o finita (consuntivo)
        this.oParentObject.w_HASEVENT = (this.oParentObject.w_PIANIF $ "PC" and this.oParentObject.w_MATIPMOV="P") or (this.oParentObject.w_PIANIF $ "PCZL" and this.oParentObject.w_MATIPMOV="C")
        if NOT this.oParentObject.w_HASEVENT
          if upper(this.oParentObject.w_HASEVCOP) = "ECPEDIT"
            do case
              case this.oParentObject.w_PIANIF="Z"
                this.w_MESS = "Impossibile modificare movimenti relativi ad un'attivit� in stato pianificata"
              case this.oParentObject.w_PIANIF="L"
                this.w_MESS = "Impossibile modificare movimenti relativi ad un'attivit� in stato lanciata"
              otherwise
                this.w_MESS = "Impossibile modificare movimenti relativi ad un'attivit� in stato finita"
            endcase
          else
            do case
              case this.oParentObject.w_PIANIF="Z"
                this.w_MESS = "Impossibile cancellare movimenti relativi ad un'attivit� in stato pianificata"
              case this.oParentObject.w_PIANIF="L"
                this.w_MESS = "Impossibile cancellare movimenti relativi ad un'attivit� in stato lanciata"
              otherwise
                this.w_MESS = "Impossibile cancellare movimenti relativi ad un'attivit� in stato finita"
            endcase
          endif
          ah_ErrorMsg(this.w_MESS,"STOP","")
        else
          if upper(this.oParentObject.w_HASEVCOP) = "ECPDELETE"
            LCtrsName=this.oparentobject.ctrsname
            riga=recno() 
 select (LCtrsName) 
 scan
            if not empty(nvl(&LCtrsName..t_MADICRIF,""))
              this.w_MESS = "Impossibile cancellare. Movimento generato automaticamente da dichiarazione di manodopera"
              ah_ErrorMsg(this.w_MESS,"STOP","")
              this.oParentObject.w_HASEVENT = .F.
              exit
            endif
            endscan
          endif
        endif
    endcase
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.bUpdateParentObject = .F.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pezzo di Codice standard
    SELECT (this.oParentObject.cTrsName)
    Go Top
    this.w_Obj.WorkFromTrs()     
    this.w_Obj.SaveDependsOn()     
    this.w_Obj.SetControlsValue()     
    this.w_Obj.ChildrenChangeRow()     
    this.w_Obj.oPgFrm.Page1.oPag.oBody.Refresh()     
    this.w_Obj.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    this.w_Obj.oPgFrm.Page1.oPag.oBody.nRelRow = 1
    i_retcode = 'stop'
    return
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPARA)
    this.pPARA=pPARA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ATTIVITA'
    this.cWorkTables[2]='MAT_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_MAT_MAST')
      use in _Curs_MAT_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARA"
endproc
