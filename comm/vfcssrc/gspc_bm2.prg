* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bm2                                                        *
*              Maschera pianificazione                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_27]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-03                                                      *
* Last revis.: 2006-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bm2",oParentObject)
return(i_retval)

define class tgspc_bm2 as StdBatch
  * --- Local variables
  w_OBJMOV = .NULL.
  Gruppo = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Maschera Pianificazione (da GSPC_SP2)
    this.w_OBJMOV = GSPC_AAT()
    * --- Testo subito se l'utente pu� aprire la gestione
    if !(this.w_OBJMOV.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Lancio l'Oggetto in Interrogazione
    this.w_OBJMOV.EcpFilter()     
    this.w_OBJMOV.w_ATCODATT = this.oParentObject.w_CODATT
    this.w_OBJMOV.w_ATTIPATT = "A"
    this.w_OBJMOV.w_ATCODCOM = this.oParentObject.w_CODCOM
    this.w_OBJMOV.EcpSave()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
