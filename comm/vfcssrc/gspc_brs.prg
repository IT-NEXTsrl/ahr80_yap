* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_brs                                                        *
*              Ricostruzione saldi costi                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][21][VRS_34]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-02                                                      *
* Last revis.: 2008-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_brs",oParentObject)
return(i_retval)

define class tgspc_brs as StdBatch
  * --- Local variables
  w_TIPATT = space(1)
  w_IMP_PRE = 0
  w_CODATT = space(15)
  w_IMP_CON = 0
  w_CODCOS = space(5)
  w_IMP_ORD = 0
  w_DOCVAL = space(3)
  w_CAOVAL = 0
  w_NUMREC = .f.
  w_DATDOC = ctod("  /  /  ")
  w_IMP_DOC = 0
  w_CAOCOMM = 0
  w_PADRE = .NULL.
  w_CSCODCOM = space(15)
  * --- WorkFile variables
  MA_COSTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruzione Saldi Costi di commesa (da GSPC_SRS e GSPC_BAV)
    *     La Procedura ricostruisce i saldi legati alle attivit� leggendo i Movimenti di Commessa
    *     e i documenti (Apposito campo in valuta della commessa).
    this.w_TIPATT = "A"
    if this.oParentObject.w_NOMESS="S"
      * --- Lanciato dalla Maschera a Menu' (Sotto Transazione)
      * --- Try
      local bErr_03723D30
      bErr_03723D30=bTrsErr
      this.Try_03723D30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg( i_ErrMsg)
      endif
      bTrsErr=bTrsErr or bErr_03723D30
      * --- End
    else
      * --- Lanciato dalla Procedura di Copia Schede Tecniche
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_03723D30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    Ah_ErrorMsg( "Ricostruzione Terminata")
    * --- Chiudo la maschera
    this.w_PADRE = This.oParentObject
    this.w_PADRE.EcpQuit()     
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzero tutti i costi legati alla Commessa
    if EMPTY(this.oParentObject.w_CODCOM)
      * --- Delete from MA_COSTI
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"1 = "+cp_ToStrODBC(1);
               )
      else
        delete from (i_cTable) where;
              1 = 1;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Delete from MA_COSTI
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
               )
      else
        delete from (i_cTable) where;
              CSCODCOM = this.oParentObject.w_CODCOM;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Aggiorna gli importi (valuta di commessa)
    if g_DIBA="S" And IsAhe()
      * --- Se AHE e installato il modulo produzione considero anche gli ODL
      * --- Insert into MA_COSTI
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gspc9brs",this.MA_COSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into MA_COSTI
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gspc8brs",this.MA_COSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Traduco gli Importi nella valuta della commessa
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_NUMREC
      Select "Cur_Saldi"
      Go Top
      Scan
      this.w_CODATT = Cur_Saldi.CodAtt
      this.w_CSCODCOM = Cur_Saldi.CodCom
      this.w_CODCOS = Cur_Saldi.CodCos
      this.w_IMP_PRE = Nvl( Cur_Saldi.Imp_Pre , 0 )
      this.w_IMP_CON = Nvl( Cur_Saldi.Imp_Con , 0 )
      this.w_IMP_ORD = Nvl( Cur_Saldi.Imp_Ord , 0 )
      AH_Msg("Aggiornata attivit�: %1 costo: %2", .t.,.f.,.f., AllTrim(this.w_CODATT), this.w_CODCOS)
      * --- Try
      local bErr_04905898
      bErr_04905898=bTrsErr
      this.Try_04905898()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into MA_COSTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MA_COSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSPREVEN =CSPREVEN+ "+cp_ToStrODBC(this.w_IMP_PRE);
          +",CSCONSUN =CSCONSUN+ "+cp_ToStrODBC(this.w_IMP_CON);
          +",CSORDIN =CSORDIN+ "+cp_ToStrODBC(this.w_IMP_ORD);
              +i_ccchkf ;
          +" where ";
              +"CSCODCOM = "+cp_ToStrODBC(this.w_CSCODCOM);
              +" and CSTIPSTR = "+cp_ToStrODBC(this.w_TIPATT);
              +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATT);
              +" and CSCODCOS = "+cp_ToStrODBC(this.w_CODCOS);
                 )
        else
          update (i_cTable) set;
              CSPREVEN = CSPREVEN + this.w_IMP_PRE;
              ,CSCONSUN = CSCONSUN + this.w_IMP_CON;
              ,CSORDIN = CSORDIN + this.w_IMP_ORD;
              &i_ccchkf. ;
           where;
              CSCODCOM = this.w_CSCODCOM;
              and CSTIPSTR = this.w_TIPATT;
              and CSCODMAT = this.w_CODATT;
              and CSCODCOS = this.w_CODCOS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_04905898
      * --- End
      Select "Cur_Saldi"
      EndScan
    endif
  endproc
  proc Try_04905898()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+",CSPREVEN"+",CSCONSUN"+",CSORDIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CSCODCOM),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPATT),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOS),'MA_COSTI','CSCODCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMP_PRE),'MA_COSTI','CSPREVEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMP_CON),'MA_COSTI','CSCONSUN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMP_ORD),'MA_COSTI','CSORDIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.w_CSCODCOM,'CSTIPSTR',this.w_TIPATT,'CSCODMAT',this.w_CODATT,'CSCODCOS',this.w_CODCOS,'CSPREVEN',this.w_IMP_PRE,'CSCONSUN',this.w_IMP_CON,'CSORDIN',this.w_IMP_ORD)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS,CSPREVEN,CSCONSUN,CSORDIN &i_ccchkf. );
         values (;
           this.w_CSCODCOM;
           ,this.w_TIPATT;
           ,this.w_CODATT;
           ,this.w_CODCOS;
           ,this.w_IMP_PRE;
           ,this.w_IMP_CON;
           ,this.w_IMP_ORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Traduzione Doc. Valuta diversa e parzialmente importati a valore
    * --- Recupero i documenti evasi parzialmente a valore con valuta diversa dalla valurta di commessa
    Vq_Exec ( "..\Comm\Exe\Query\Gspc0Brs.Vqr" , This , "Curs_Val" )
    this.w_NUMREC = RECCOUNT("Curs_Val")>0
    if this.w_NUMREC
      Cur = WrCursor ("Curs_Val")
      Go Top
      Scan
      * --- Leggo il tasso di conversione della valuta (Se letto nella maschera a volte non lo prende)
      if CNCODVAL<>g_PERVAL
        this.w_CAOCOMM = VACAOVAL
      else
        this.w_CAOCOMM = g_CAOVAL
      endif
      this.w_CAOVAL = Nvl ( CAOVAL , 0 )
      this.w_DATDOC = Cp_ToDate(DATDOC)
      * --- Se Valuta del documento inesistente non applico la conversione (Dovrebbe esserci sempre)
      this.w_DOCVAL = Nvl ( CODVAL , CNCODVAL )
      this.w_IMP_DOC = Nvl ( IMP_DOC , 0 )
      * --- Eseguo la conversione della Riga
      this.w_IMP_ORD = IIF ( CNCODVAL=this.w_DOCVAL,this.w_IMP_DOC , VAL2MON(this.w_IMP_DOC,this.w_CAOVAL,this.w_CAOCOMM,this.w_DATDOC,CNCODVAL) )
      Select "Curs_Val"
      Replace IMP_ORD With this.w_IMP_ORD
      EndScan
      * --- Raggruppo 
      Select CodAtt, CodCos , CodCom, Sum (Imp_Pre) As Imp_Pre , Sum (Imp_Con) As Imp_Con, ;
      Sum (Imp_Ord) As Imp_Ord from Curs_Val Group By CodAtt,CodCos,CodCom Into Cursor Cur_Saldi NoFilter
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MA_COSTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
