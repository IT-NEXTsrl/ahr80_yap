* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bs3                                                        *
*              Lancia riep costi                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_156]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-11                                                      *
* Last revis.: 2006-02-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bs3",oParentObject)
return(i_retval)

define class tgspc_bs3 as StdBatch
  * --- Local variables
  CommessaIniziale = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSPC_BS3 INVOCATO DA GSPC_SRC
    * --- Parametro di inizio periodo selezione
    * --- Parametro di fine periodo selezione
    * --- Parametro di inizio selezione costi
    * --- Parametro di fine selezione costi
    if empty(this.oParentObject.w_CODCOM) OR empty(this.oParentObject.w_CODCOM2)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_ErrorMsg("Codice commessa non valido","!","")
      i_retcode = 'stop'
      return
    endif
    * --- Se SOLO PRIMO LIVELLO occorre azzerare questi filtri.
    if this.oParentObject.w_WITHATTI="1"
      this.oParentObject.w_CODCONTO = space(15)
      this.oParentObject.w_DESCON = space(30)
      this.oParentObject.w_CODCOS1 = space(5)
      this.oParentObject.w_CODCOS2 = space(5)
      this.oParentObject.w_DESCOS1 = SPACE(30)
      this.oParentObject.w_DESCOS2 = SPACE(30)
    endif
    * --- Ricorda il valore di w_CODCOM per riassegnarlo alla fine
    this.CommessaIniziale = this.oParentObject.w_CODCOM
    * --- Se le due commesse sono diverse occorre azzerare questi filtri.
    if this.oParentObject.w_CODCOM <> this.oParentObject.w_CODCOM2
      this.oParentObject.w_CODCONTO = space(15)
      this.oParentObject.w_CODCOS1 = space(5)
      this.oParentObject.w_CODCOS2 = space(5)
    endif
    * --- Elenco ordinato delle commesse
    vq_exec("..\comm\exe\query\gspc_com",this,"Commesse")
    if this.oParentObject.w_ANACOSTI="S"
      * --- Eseguo GSPC_BSC una volta per ogni commessa
      Select Commesse
      scan
      this.oParentObject.w_CODCOM = commesse.cncodcan
      this.oParentObject.w_OK = .T.
      do GSPC_BSC with this.oParentObject
      * --- Side effect: aggiorna la w_LOOP e la w_OK
      if this.oParentObject.w_OK
        * --- L'esplosione della treeview per la commessa attualmente selezionata ha restituito un cursore non vuoto 
        * --- Selezioni � il cursore che contiene i valori dei filtri, inizializzato in GSPC_BSC
        * --- Il risultato di GSPC_BSC viene accumulato in ResultSet
        if !used("ResultSet")
          * --- OLD: select * from __tmp__,selezioni where NOT EMPTY(NVL(atcodcom,'')) into cursor ResultSet  NoFilter
          select NVL(ATCODCOM,SPACE(15)) as ATCODCOM, NVL(ATCODATT,SPACE(15)) AS ATCODATT, ; 
 NVL(ATTIPATT, SPACE(1)) AS ATTIPATT, NVL(ATTIPCOM,SPACE(1)) AS ATTIPCOM, ; 
 NVL(ATDESCRI,SPACE(30)) AS ATDESCRI, NVL(ATPERCOM,999999999999.99999*0) AS ATPERCOM, ; 
 NVL(CPROWORD,999999999999.99999*0) AS CPROWORD, NVL(TIPPRO,SPACE(1)) AS TIPPRO, ; 
 NVL(QTACOMP,999999999999.99999*0) AS QTACOMP, NVL(LVLKEY,SPACE(200)) AS LVLKEY, ; 
 NVL(CPBMPNAME,SPACE(150)) AS CPBMPNAME, NVL(FLPREV,SPACE(1)) AS FLPREV, ; 
 NVL(CODPAD,SPACE(15)) AS CODPAD, NVL(NUMPOINT,999999999999.99999*0) AS NUMPOINT, ; 
 NVL(PREVENTIVO_MA,999999999999.99999*0) AS PREVENTIVO_MA, ; 
 NVL(PREVENTIVO_MD,999999999999.99999*0) AS PREVENTIVO_MD, ; 
 NVL(PREVENTIVO_AP,999999999999.99999*0) AS PREVENTIVO_AP, ; 
 NVL(PREVENTIVO_AL,999999999999.99999*0) AS PREVENTIVO_AL, ; 
 NVL(CONS_MA,999999999999.99999*0) AS CONS_MA, NVL(CONS_MD,999999999999.99999*0) AS CONS_MD, ; 
 NVL(CONS_AP,999999999999.99999*0) AS CONS_AP, NVL(CONS_AL,999999999999.99999*0) AS CONS_AL, ; 
 NVL(ORDI_MA,999999999999.99999*0) AS ORDI_MA, NVL(ORDI_MD,999999999999.99999*0) AS ORDI_MD, ; 
 NVL(ORDI_AP,999999999999.99999*0) AS ORDI_AP, NVL(ORDI_AL,999999999999.99999*0) AS ORDI_AL, ; 
 NVL(ORDINE,999999999999.99999*0) AS ORDINE, SELEZIONI.* ; 
 from __tmp__,selezioni where NOT EMPTY(NVL(atcodcom,"")) into cursor ResultSet NoFilter
        else
          select NVL(ATCODCOM,SPACE(15)) as ATCODCOM, NVL(ATCODATT,SPACE(15)) AS ATCODATT, ; 
 NVL(ATTIPATT, SPACE(1)) AS ATTIPATT, NVL(ATTIPCOM,SPACE(1)) AS ATTIPCOM, ; 
 NVL(ATDESCRI,SPACE(30)) AS ATDESCRI, NVL(ATPERCOM,0) AS ATPERCOM, ; 
 NVL(CPROWORD,0) AS CPROWORD, NVL(TIPPRO,SPACE(1)) AS TIPPRO, NVL(QTACOMP,0) AS QTACOMP, ; 
 NVL(LVLKEY,SPACE(200)) AS LVLKEY, NVL(CPBMPNAME,SPACE(150)) AS CPBMPNAME, ; 
 NVL(FLPREV,SPACE(1)) AS FLPREV, NVL(CODPAD,SPACE(15)) AS CODPAD, NVL(NUMPOINT,0) AS NUMPOINT, ; 
 NVL(PREVENTIVO_MA,0) AS PREVENTIVO_MA, NVL(PREVENTIVO_MD,0) AS PREVENTIVO_MD, ; 
 NVL(PREVENTIVO_AP,0) AS PREVENTIVO_AP, NVL(PREVENTIVO_AL,0) AS PREVENTIVO_AL, ; 
 NVL(CONS_MA,0) AS CONS_MA, NVL(CONS_MD,0) AS CONS_MD, NVL(CONS_AP,0) AS CONS_AP, ; 
 NVL(CONS_AL,0) AS CONS_AL, NVL(ORDI_MA,0) AS ORDI_MA, NVL(ORDI_MD,0) AS ORDI_MD, ; 
 NVL(ORDI_AP,0) AS ORDI_AP, NVL(ORDI_AL,0) AS ORDI_AL, NVL(ORDINE,0) AS ORDINE, SELEZIONI.* ; 
 from __tmp__,selezioni where NOT EMPTY(NVL(atcodcom,"")) into cursor Result2 NoFilter 
 Select * from ResultSet union ; 
 Select * from Result2 where NOT EMPTY(NVL(atcodcom,"")) into cursor ResultSet1 NoFilter 
 Select * from ResultSet1 into cursor ResultSet NoFilter
        endif
      endif
      endscan
    endif
    if this.oParentObject.w_ANARICAV="S"
      * --- Eseguo GSPC_BSL una volta per tutte le commesse
      do GSPC_BSL with this.oParentObject
      if used("__MR__")
        select __MR__.*, Commesse.cncodcan as L_commessa, left(Commesse.cndescan, 30) as L_descom, ; 
 Commesse.vadectot as L_decimali, Commesse.cncodval as L_codval ; 
 from __MR__ inner join Commesse on __MR__.atcodcom=Commesse.cncodcan into cursor ResultSetV
        use in __MR__
      endif
    endif
    if used("ResultSet")
      if used("__TMP__")
        use in __TMP__
      endif
      && Vecchia Select Remmata 
 && select * from ResultSet into cursor __TMP__ order by L_COMMESSA,Ordine,LvlKey
      do case
        case this.oParentObject.w_ANACOSTI="S" and this.oParentObject.w_ANARICAV="S"
          select ResultSet.*, "C" as MACOSRIC from ResultSet union all select ResultSetV.*,"R" as MACOSRIC ; 
 from ResultSetV into cursor __TMP__ order by 28,27,10
        case this.oParentObject.w_ANACOSTI="S" and this.oParentObject.w_ANARICAV<>"S"
          select * from ResultSet into cursor __TMP__ order by L_COMMESSA,Ordine,LvlKey
        case this.oParentObject.w_ANACOSTI<>"S" and this.oParentObject.w_ANARICAV="S"
          select * from ResultSetV into cursor __TMP__ order by L_COMMESSA,Ordine,LvlKey
      endcase
    else
      * --- Tutte le commesse selezionate non avevano elementi
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_ErrorMsg("La selezione di stampa � vuota","!","")
      i_retcode = 'stop'
      return
    endif
    if USED("__TMP__")
      * --- Verifico se ci sono dati significativi
      SELECT __TMP__
      GO TOP
      LOCATE FOR ALLTRIM(LVLKEY)="1"
      if NOT FOUND()
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Ripristina il valore di w_CODCOM
        this.oParentObject.w_CODCOM = this.CommessaIniziale
        ah_ErrorMsg("La selezione di stampa � vuota","!","")
        i_retcode = 'stop'
        return
      endif
      L_DATAINI=this.oParentObject.w_DATAINI
      L_DATAFIN=this.oParentObject.w_DATAFIN
      l_CODCOSTO1=this.oParentObject.w_DESCOS1
      l_CODCOSTO2=this.oParentObject.w_DESCOS2
      L_FILTRO=this.oParentObject.w_FILTRO
      L_TIPO = this.oParentObject.w_TIPO 
 L_CODCONTO = this.oParentObject.w_CODCONTO 
 L_DESCON = this.oParentObject.w_DESCON 
 L_WITHATTI = this.oParentObject.w_WITHATTI 
 L_NOZERI = this.oParentObject.w_NOZERI
      do case
        case this.oParentObject.w_ANACOSTI="S" and this.oParentObject.w_ANARICAV="S"
          if this.oParentObject.w_TIPSTAMPA="D"
            * --- Stampa Dettagliata
            CP_CHPRN("..\Comm\Exe\Query\Gspc2Bsc.Frx", " ", this)
          else
            * --- Stampa Sintetica
            CP_CHPRN("..\Comm\Exe\Query\Gspc1Bsc.Frx", " ", this)
          endif
        case this.oParentObject.w_ANACOSTI="S" and this.oParentObject.w_ANARICAV<>"S"
          if this.oParentObject.w_TIPSTAMPA="D"
            * --- Stampa Dettagliata
            CP_CHPRN("..\Comm\Exe\Query\GspcDBsc.Frx", " ", this)
          else
            * --- Stampa Sintetica
            CP_CHPRN("..\Comm\Exe\Query\Gspc_Bsc.Frx", " ", this)
          endif
        case this.oParentObject.w_ANACOSTI<>"S" and this.oParentObject.w_ANARICAV="S"
          * --- Predisposta Ma non implementata
      endcase
    else
      ah_ErrorMsg("La selezione di stampa � vuota","!","")
    endif
    * --- Ripristina il valore di w_CODCOM
    this.oParentObject.w_CODCOM = this.CommessaIniziale
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- RILASCIO CURSORI
    if used("ResultSet")
      use in ResultSet
    endif
    if used("ResultSetV")
      use in ResultSetV
    endif
    if used("Result2")
      use in Result2
    endif
    if used("Selezioni")
      use in Selezioni
    endif
    if used("Commesse")
      use in Commesse
    endif
    if used("Resultset1")
      use in Resultset1
    endif
    if used("query")
      use in query
    endif
    if used("Forfait")
      use in Forfait
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
