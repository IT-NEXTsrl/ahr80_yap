* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc1kcc                                                        *
*              Cruscotto commessa                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_237]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-14                                                      *
* Last revis.: 2018-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc1kcc",oParentObject))

* --- Class definition
define class tgspc1kcc as StdForm
  Top    = 1
  Left   = 7

  * --- Standard Properties
  Width  = 1207
  Height = 654
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-01-22"
  HelpContextID=102258793
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=72

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  CPAR_DEF_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gspc1kcc"
  cComment = "Cruscotto commessa"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FORMATO = space(10)
  w_READPAR = space(10)
  w_DEFCOM = space(15)
  w_CODCOM = space(15)
  w_DATAINI = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_CODCLI = space(15)
  w_TPREVEN = space(1)
  w_PRINT = .F.
  w_WHATGR = space(16)
  w_TXT1 = space(10)
  w_TXT2 = space(10)
  w_TXT3 = space(10)
  w_M_PERCCONS_MA = space(18)
  w_M_PERCCONS_MD = space(18)
  w_M_PERCCONS_AP = space(18)
  w_M_PERCCONS_AL = space(18)
  w_M_PERCCONSORDI_MA = space(18)
  w_M_PERCCONSORDI_MD = space(18)
  w_M_PERCCONSORDI_AP = space(18)
  w_M_PERCCONSORDI_AL = space(18)
  w_M_PERCPREVENTIVO_MA = space(18)
  w_M_PERCPREVENTIVO_MD = space(18)
  w_M_PERCPREVENTIVO_AP = space(18)
  w_M_PERCPREVENTIVO_AL = space(18)
  w_M_PERCORDI_MA = space(18)
  w_M_PERCORDI_MD = space(18)
  w_M_PERCORDI_AP = space(18)
  w_M_PERCORDI_AL = space(18)
  w_M_FATTURATOATTUALE = space(18)
  w_M_MATURATONONFATTURATO = space(18)
  w_M_PERCFATTURATOATTUALE = space(18)
  w_M_PERCMATURATONONFATTURATO = space(18)
  w_M_AVANTECNNONDISP = space(10)
  w_M_CONSUNTIVOPAG4 = space(18)
  w_M_IMPEGNIFINANZIARIPAG4 = space(18)
  w_M_PERCCONSUNTIVOPAG4 = space(18)
  w_M_PERCIMPEGNIFINANZIARIPAG4 = space(18)
  w_COUTCURS = space(100)
  w_CODVAL = space(3)
  w_FLSTAMP1 = .F.
  w_TOTOREPREV = 0
  w_TOTORECONS = 0
  w_TOTORERES = 0
  w_M_PREVENTIVO = space(18)
  w_TIPOGRA = space(1)
  w_TIPOCOST = space(1)
  w_M_RICAVIPREVISTI = space(18)
  w_M_RICAVICONSEGUITI = space(18)
  w_M_RICAVIRESIDUI = space(18)
  w_M_RICAVISAL = space(18)
  w_CODCOS = space(5)
  w_VALCOM = space(3)
  w_DATREG = ctod('  /  /  ')
  w_VALORE_PER_MDO = 0
  o_VALORE_PER_MDO = 0
  w_VALORE_PER_COSTI = 0
  o_VALORE_PER_COSTI = 0
  w_LOGOSCOLA = space(10)
  w_VALORE_PER_ATEC = 0
  o_VALORE_PER_ATEC = 0
  w_TIPMOVC = space(1)
  w_TIPMOVR = space(1)
  w_TIPMOVM = space(1)
  w_VALORE_PER_MARAT = 0
  o_VALORE_PER_MARAT = 0
  w_VALORE_PER_MARRE = 0
  o_VALORE_PER_MARRE = 0
  w_VALORE_PER_MARTO = 0
  o_VALORE_PER_MARTO = 0
  w_VALORE_MARAT = space(10)
  w_VALORE_MARRE = space(10)
  w_VALORE_MARTO = space(10)
  w_VALORE_AFINIRE = space(10)
  w_AVANZ_RICAVI = 0
  o_AVANZ_RICAVI = 0
  w_Graph16_LEGEND = space(10)
  w_VALORE_OBIETTIVO = 0
  o_VALORE_OBIETTIVO = 0
  w_Graph13 = .NULL.
  w_Graph16 = .NULL.
  w_Graph14 = .NULL.
  w_lbltotoreprev1 = .NULL.
  w_lblOreManPag11 = .NULL.
  w_lbltotorecons1 = .NULL.
  w_lbltotoreres1 = .NULL.
  w_Graph15 = .NULL.
  w_gaugecosti = .NULL.
  w_gaugericavi = .NULL.
  w_gaugemarat = .NULL.
  w_gaugemarre = .NULL.
  w_gaugemarto = .NULL.
  w_gaugeob = .NULL.
  w_gaugemdo = .NULL.
  w_gaugetec = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc1kccPag1","gspc1kcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Avanzamento tecnico")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Graph13 = this.oPgFrm.Pages(1).oPag.Graph13
    this.w_Graph16 = this.oPgFrm.Pages(1).oPag.Graph16
    this.w_Graph14 = this.oPgFrm.Pages(1).oPag.Graph14
    this.w_lbltotoreprev1 = this.oPgFrm.Pages(1).oPag.lbltotoreprev1
    this.w_lblOreManPag11 = this.oPgFrm.Pages(1).oPag.lblOreManPag11
    this.w_lbltotorecons1 = this.oPgFrm.Pages(1).oPag.lbltotorecons1
    this.w_lbltotoreres1 = this.oPgFrm.Pages(1).oPag.lbltotoreres1
    this.w_Graph15 = this.oPgFrm.Pages(1).oPag.Graph15
    this.w_gaugecosti = this.oPgFrm.Pages(1).oPag.gaugecosti
    this.w_gaugericavi = this.oPgFrm.Pages(1).oPag.gaugericavi
    this.w_gaugemarat = this.oPgFrm.Pages(1).oPag.gaugemarat
    this.w_gaugemarre = this.oPgFrm.Pages(1).oPag.gaugemarre
    this.w_gaugemarto = this.oPgFrm.Pages(1).oPag.gaugemarto
    this.w_gaugeob = this.oPgFrm.Pages(1).oPag.gaugeob
    this.w_gaugemdo = this.oPgFrm.Pages(1).oPag.gaugemdo
    this.w_gaugetec = this.oPgFrm.Pages(1).oPag.gaugetec
    DoDefault()
    proc Destroy()
      this.w_Graph13 = .NULL.
      this.w_Graph16 = .NULL.
      this.w_Graph14 = .NULL.
      this.w_lbltotoreprev1 = .NULL.
      this.w_lblOreManPag11 = .NULL.
      this.w_lbltotorecons1 = .NULL.
      this.w_lbltotoreres1 = .NULL.
      this.w_Graph15 = .NULL.
      this.w_gaugecosti = .NULL.
      this.w_gaugericavi = .NULL.
      this.w_gaugemarat = .NULL.
      this.w_gaugemarre = .NULL.
      this.w_gaugemarto = .NULL.
      this.w_gaugeob = .NULL.
      this.w_gaugemdo = .NULL.
      this.w_gaugetec = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CPAR_DEF'
    this.cWorkTables[4]='ATTIVITA'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FORMATO=space(10)
      .w_READPAR=space(10)
      .w_DEFCOM=space(15)
      .w_CODCOM=space(15)
      .w_DATAINI=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODCLI=space(15)
      .w_TPREVEN=space(1)
      .w_PRINT=.f.
      .w_WHATGR=space(16)
      .w_TXT1=space(10)
      .w_TXT2=space(10)
      .w_TXT3=space(10)
      .w_M_PERCCONS_MA=space(18)
      .w_M_PERCCONS_MD=space(18)
      .w_M_PERCCONS_AP=space(18)
      .w_M_PERCCONS_AL=space(18)
      .w_M_PERCCONSORDI_MA=space(18)
      .w_M_PERCCONSORDI_MD=space(18)
      .w_M_PERCCONSORDI_AP=space(18)
      .w_M_PERCCONSORDI_AL=space(18)
      .w_M_PERCPREVENTIVO_MA=space(18)
      .w_M_PERCPREVENTIVO_MD=space(18)
      .w_M_PERCPREVENTIVO_AP=space(18)
      .w_M_PERCPREVENTIVO_AL=space(18)
      .w_M_PERCORDI_MA=space(18)
      .w_M_PERCORDI_MD=space(18)
      .w_M_PERCORDI_AP=space(18)
      .w_M_PERCORDI_AL=space(18)
      .w_M_FATTURATOATTUALE=space(18)
      .w_M_MATURATONONFATTURATO=space(18)
      .w_M_PERCFATTURATOATTUALE=space(18)
      .w_M_PERCMATURATONONFATTURATO=space(18)
      .w_M_AVANTECNNONDISP=space(10)
      .w_M_CONSUNTIVOPAG4=space(18)
      .w_M_IMPEGNIFINANZIARIPAG4=space(18)
      .w_M_PERCCONSUNTIVOPAG4=space(18)
      .w_M_PERCIMPEGNIFINANZIARIPAG4=space(18)
      .w_COUTCURS=space(100)
      .w_CODVAL=space(3)
      .w_FLSTAMP1=.f.
      .w_TOTOREPREV=0
      .w_TOTORECONS=0
      .w_TOTORERES=0
      .w_M_PREVENTIVO=space(18)
      .w_TIPOGRA=space(1)
      .w_TIPOCOST=space(1)
      .w_M_RICAVIPREVISTI=space(18)
      .w_M_RICAVICONSEGUITI=space(18)
      .w_M_RICAVIRESIDUI=space(18)
      .w_M_RICAVISAL=space(18)
      .w_CODCOS=space(5)
      .w_VALCOM=space(3)
      .w_DATREG=ctod("  /  /  ")
      .w_VALORE_PER_MDO=0
      .w_VALORE_PER_COSTI=0
      .w_LOGOSCOLA=space(10)
      .w_VALORE_PER_ATEC=0
      .w_TIPMOVC=space(1)
      .w_TIPMOVR=space(1)
      .w_TIPMOVM=space(1)
      .w_VALORE_PER_MARAT=0
      .w_VALORE_PER_MARRE=0
      .w_VALORE_PER_MARTO=0
      .w_VALORE_MARAT=space(10)
      .w_VALORE_MARRE=space(10)
      .w_VALORE_MARTO=space(10)
      .w_VALORE_AFINIRE=space(10)
      .w_AVANZ_RICAVI=0
      .w_Graph16_LEGEND=space(10)
      .w_VALORE_OBIETTIVO=0
        .w_FORMATO = "99,999,999,999,999.99"
        .w_READPAR = 'TAM'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READPAR))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_CODCOM = .w_DEFCOM
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODCOM))
          .link_1_4('Full')
        endif
        .w_DATAINI = i_DATSYS
          .DoRTCalc(6,6,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(8,8,.f.)
        .w_TPREVEN = 'A'
        .w_PRINT = .f.
        .w_WHATGR = ''
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .DoRTCalc(12,41,.f.)
        if not(empty(.w_CODVAL))
          .link_1_47('Full')
        endif
        .w_FLSTAMP1 = .f.
          .DoRTCalc(43,44,.f.)
        .w_TOTORERES = .w_TOTOREPREV - .w_TOTORECONS
      .oPgFrm.Page1.oPag.Graph13.Calculate()
      .oPgFrm.Page1.oPag.Graph16.Calculate()
      .oPgFrm.Page1.oPag.Graph14.Calculate()
          .DoRTCalc(46,46,.f.)
        .w_TIPOGRA = 'I'
        .w_TIPOCOST = 'C'
          .DoRTCalc(49,53,.f.)
        .w_VALCOM = .w_CODVAL
        .w_DATREG = .w_DATAINI
        .w_VALORE_PER_MDO = (.w_TOTORECONS / iif(.w_TOTOREPREV=0, 1, .w_TOTOREPREV)) * 100
        .w_VALORE_PER_COSTI = ((cp_round(val(.w_M_CONSUNTIVOPAG4),2) + cp_round(val(.w_M_IMPEGNIFINANZIARIPAG4),2))  / IIF(val(.w_M_PREVENTIVO)=0, 1, cp_round(val(.w_M_PREVENTIVO),2))) * 100
      .oPgFrm.Page1.oPag.oObj_1_67.Calculate(TRAN(.w_TOTOREPREV, .w_FORMATO), RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.lbltotoreprev1.Calculate('Previste' , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.lblOreManPag11.Calculate('Ore manodopera' , RGB(255,0,0), RGB(255,255,255))
      .oPgFrm.Page1.oPag.lbltotorecons1.Calculate('Consuntive' , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.lbltotoreres1.Calculate('Residue' , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_72.Calculate(TRAN(.w_TOTORECONS, .w_FORMATO) , RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_73.Calculate(TRAN(.w_TOTORERES, .w_FORMATO), RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_74.Calculate(TRAN(VAL(.w_M_RICAVISAL) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_75.Calculate('SAL'  , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_76.Calculate(TRAN(VAL(.w_M_RICAVIRESIDUI) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_77.Calculate(TRAN(VAL(.w_M_RICAVICONSEGUITI) , .w_FORMATO) , RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_78.Calculate('Residui'  , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_79.Calculate('Conseguiti'  , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_80.Calculate('Ricavi' , RGB(255,0,0), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_81.Calculate('Previsti' , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_82.Calculate(TRAN(VAL(.w_M_RICAVIPREVISTI) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_83.Calculate(TRAN(VAL(.w_VALORE_AFINIRE) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_84.Calculate('A finire'  , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_85.Calculate(TRAN(VAL(.w_M_IMPEGNIFINANZIARIPAG4) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_86.Calculate(TRAN(VAL(.w_M_CONSUNTIVOPAG4) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_87.Calculate('Imp. finanziari'  , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_88.Calculate('Consuntivi'  , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_89.Calculate('Costi', RGB(255,0,0), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_90.Calculate('Preventivi' , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_91.Calculate(TRAN(VAL(.w_M_PREVENTIVO) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .w_LOGOSCOLA = ".\bmp\Logo_scola_dino.png"
      .oPgFrm.Page1.oPag.Graph15.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_94.Calculate(TRAN(VAL(.w_VALORE_MARAT) , .w_FORMATO) , RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_95.Calculate('Attuale' , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_96.Calculate('Margine' , RGB(255,0,0), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_97.Calculate('Residuo' , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_98.Calculate('Atteso' , RGB(63,63,89), RGB(255,255,128))
      .oPgFrm.Page1.oPag.oObj_1_99.Calculate(TRAN(VAL(.w_VALORE_MARRE) , .w_FORMATO)  ,  RGB(63,63,89), RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_100.Calculate(TRAN(VAL(.w_VALORE_MARTO) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
          .DoRTCalc(59,69,.f.)
        .w_AVANZ_RICAVI = cp_round(val(.w_M_RICAVICONSEGUITI) / IIF(val(.w_M_RICAVIPREVISTI)=0, 1, val(.w_M_RICAVIPREVISTI)) * 100, 0)
      .oPgFrm.Page1.oPag.gaugecosti.Calculate(.w_VALORE_PER_COSTI, .F. ,  .F., .T.)
      .oPgFrm.Page1.oPag.gaugericavi.Calculate(.w_AVANZ_RICAVI, .F. ,  .F., .T.)
      .oPgFrm.Page1.oPag.gaugemarat.Calculate(.w_VALORE_PER_MARAT, .F. ,  .F., .T.)
      .oPgFrm.Page1.oPag.gaugemarre.Calculate(.w_VALORE_PER_MARRE, .F. ,  .F., .T.)
      .oPgFrm.Page1.oPag.gaugemarto.Calculate(.w_VALORE_PER_MARTO, .F. ,  .F., .T.)
          .DoRTCalc(71,71,.f.)
        .w_VALORE_OBIETTIVO = ((VAL(.w_M_RICAVIPREVISTI) - VAL(.w_M_PREVENTIVO)) / IIF(VAL(.w_M_RICAVIPREVISTI)=0, 1, VAL(.w_M_RICAVIPREVISTI) )) * 100
      .oPgFrm.Page1.oPag.gaugeob.Calculate(.w_VALORE_OBIETTIVO, .F. ,  .F., .T.)
      .oPgFrm.Page1.oPag.gaugemdo.Calculate(.w_VALORE_PER_MDO, .F. ,  .F., .T.)
      .oPgFrm.Page1.oPag.gaugetec.Calculate(.w_VALORE_PER_ATEC, .F. ,  .F., .T.)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_126.enabled = this.oPgFrm.Page1.oPag.oBtn_1_126.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gspc1kcc
    endproc
    
    proc Resize
      dodefault()
      * wait wind MDOWN( )
    
      
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .DoRTCalc(3,40,.t.)
          .link_1_47('Full')
        .DoRTCalc(42,44,.t.)
            .w_TOTORERES = .w_TOTOREPREV - .w_TOTORECONS
        .oPgFrm.Page1.oPag.Graph13.Calculate()
        .oPgFrm.Page1.oPag.Graph16.Calculate()
        .oPgFrm.Page1.oPag.Graph14.Calculate()
        .DoRTCalc(46,53,.t.)
            .w_VALCOM = .w_CODVAL
            .w_DATREG = .w_DATAINI
            .w_VALORE_PER_MDO = (.w_TOTORECONS / iif(.w_TOTOREPREV=0, 1, .w_TOTOREPREV)) * 100
            .w_VALORE_PER_COSTI = ((cp_round(val(.w_M_CONSUNTIVOPAG4),2) + cp_round(val(.w_M_IMPEGNIFINANZIARIPAG4),2))  / IIF(val(.w_M_PREVENTIVO)=0, 1, cp_round(val(.w_M_PREVENTIVO),2))) * 100
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate(TRAN(.w_TOTOREPREV, .w_FORMATO), RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.lbltotoreprev1.Calculate('Previste' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.lblOreManPag11.Calculate('Ore manodopera' , RGB(255,0,0), RGB(255,255,255))
        .oPgFrm.Page1.oPag.lbltotorecons1.Calculate('Consuntive' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.lbltotoreres1.Calculate('Residue' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(TRAN(.w_TOTORECONS, .w_FORMATO) , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(TRAN(.w_TOTORERES, .w_FORMATO), RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate(TRAN(VAL(.w_M_RICAVISAL) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate('SAL'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(TRAN(VAL(.w_M_RICAVIRESIDUI) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate(TRAN(VAL(.w_M_RICAVICONSEGUITI) , .w_FORMATO) , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate('Residui'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate('Conseguiti'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate('Ricavi' , RGB(255,0,0), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate('Previsti' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(TRAN(VAL(.w_M_RICAVIPREVISTI) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate(TRAN(VAL(.w_VALORE_AFINIRE) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate('A finire'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate(TRAN(VAL(.w_M_IMPEGNIFINANZIARIPAG4) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate(TRAN(VAL(.w_M_CONSUNTIVOPAG4) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate('Imp. finanziari'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate('Consuntivi'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate('Costi', RGB(255,0,0), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate('Preventivi' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate(TRAN(VAL(.w_M_PREVENTIVO) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.Graph15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate(TRAN(VAL(.w_VALORE_MARAT) , .w_FORMATO) , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate('Attuale' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate('Margine' , RGB(255,0,0), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate('Residuo' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate('Atteso' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate(TRAN(VAL(.w_VALORE_MARRE) , .w_FORMATO)  ,  RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate(TRAN(VAL(.w_VALORE_MARTO) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .DoRTCalc(58,69,.t.)
            .w_AVANZ_RICAVI = cp_round(val(.w_M_RICAVICONSEGUITI) / IIF(val(.w_M_RICAVIPREVISTI)=0, 1, val(.w_M_RICAVIPREVISTI)) * 100, 0)
        if .o_VALORE_PER_COSTI<>.w_VALORE_PER_COSTI
        .oPgFrm.Page1.oPag.gaugecosti.Calculate(.w_VALORE_PER_COSTI, .F. ,  .F., .T.)
        endif
        if .o_AVANZ_RICAVI<>.w_AVANZ_RICAVI
        .oPgFrm.Page1.oPag.gaugericavi.Calculate(.w_AVANZ_RICAVI, .F. ,  .F., .T.)
        endif
        if .o_VALORE_PER_MARAT<>.w_VALORE_PER_MARAT
        .oPgFrm.Page1.oPag.gaugemarat.Calculate(.w_VALORE_PER_MARAT, .F. ,  .F., .T.)
        endif
        if .o_VALORE_PER_MARRE<>.w_VALORE_PER_MARRE
        .oPgFrm.Page1.oPag.gaugemarre.Calculate(.w_VALORE_PER_MARRE, .F. ,  .F., .T.)
        endif
        if .o_VALORE_PER_MARTO<>.w_VALORE_PER_MARTO
        .oPgFrm.Page1.oPag.gaugemarto.Calculate(.w_VALORE_PER_MARTO, .F. ,  .F., .T.)
        endif
        .DoRTCalc(71,71,.t.)
            .w_VALORE_OBIETTIVO = ((VAL(.w_M_RICAVIPREVISTI) - VAL(.w_M_PREVENTIVO)) / IIF(VAL(.w_M_RICAVIPREVISTI)=0, 1, VAL(.w_M_RICAVIPREVISTI) )) * 100
        if .o_VALORE_OBIETTIVO<>.w_VALORE_OBIETTIVO
        .oPgFrm.Page1.oPag.gaugeob.Calculate(.w_VALORE_OBIETTIVO, .F. ,  .F., .T.)
        endif
        if .o_VALORE_PER_MDO<>.w_VALORE_PER_MDO
        .oPgFrm.Page1.oPag.gaugemdo.Calculate(.w_VALORE_PER_MDO, .F. ,  .F., .T.)
        endif
        if .o_VALORE_PER_ATEC<>.w_VALORE_PER_ATEC
        .oPgFrm.Page1.oPag.gaugetec.Calculate(.w_VALORE_PER_ATEC, .F. ,  .F., .T.)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.Graph13.Calculate()
        .oPgFrm.Page1.oPag.Graph16.Calculate()
        .oPgFrm.Page1.oPag.Graph14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate(TRAN(.w_TOTOREPREV, .w_FORMATO), RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.lbltotoreprev1.Calculate('Previste' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.lblOreManPag11.Calculate('Ore manodopera' , RGB(255,0,0), RGB(255,255,255))
        .oPgFrm.Page1.oPag.lbltotorecons1.Calculate('Consuntive' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.lbltotoreres1.Calculate('Residue' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(TRAN(.w_TOTORECONS, .w_FORMATO) , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(TRAN(.w_TOTORERES, .w_FORMATO), RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate(TRAN(VAL(.w_M_RICAVISAL) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate('SAL'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(TRAN(VAL(.w_M_RICAVIRESIDUI) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate(TRAN(VAL(.w_M_RICAVICONSEGUITI) , .w_FORMATO) , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate('Residui'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate('Conseguiti'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate('Ricavi' , RGB(255,0,0), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate('Previsti' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(TRAN(VAL(.w_M_RICAVIPREVISTI) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate(TRAN(VAL(.w_VALORE_AFINIRE) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate('A finire'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate(TRAN(VAL(.w_M_IMPEGNIFINANZIARIPAG4) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate(TRAN(VAL(.w_M_CONSUNTIVOPAG4) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate('Imp. finanziari'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate('Consuntivi'  , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate('Costi', RGB(255,0,0), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate('Preventivi' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate(TRAN(VAL(.w_M_PREVENTIVO) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.Graph15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate(TRAN(VAL(.w_VALORE_MARAT) , .w_FORMATO) , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate('Attuale' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate('Margine' , RGB(255,0,0), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate('Residuo' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate('Atteso' , RGB(63,63,89), RGB(255,255,128))
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate(TRAN(VAL(.w_VALORE_MARRE) , .w_FORMATO)  ,  RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate(TRAN(VAL(.w_VALORE_MARTO) , .w_FORMATO)  , RGB(63,63,89), RGB(255,255,255))
        .oPgFrm.Page1.oPag.gaugecosti.Calculate(.w_VALORE_PER_COSTI, .F. ,  .F., .T.)
        .oPgFrm.Page1.oPag.gaugericavi.Calculate(.w_AVANZ_RICAVI, .F. ,  .F., .T.)
        .oPgFrm.Page1.oPag.gaugemarat.Calculate(.w_VALORE_PER_MARAT, .F. ,  .F., .T.)
        .oPgFrm.Page1.oPag.gaugemarre.Calculate(.w_VALORE_PER_MARRE, .F. ,  .F., .T.)
        .oPgFrm.Page1.oPag.gaugemarto.Calculate(.w_VALORE_PER_MARTO, .F. ,  .F., .T.)
        .oPgFrm.Page1.oPag.gaugeob.Calculate(.w_VALORE_OBIETTIVO, .F. ,  .F., .T.)
        .oPgFrm.Page1.oPag.gaugemdo.Calculate(.w_VALORE_PER_MDO, .F. ,  .F., .T.)
        .oPgFrm.Page1.oPag.gaugetec.Calculate(.w_VALORE_PER_ATEC, .F. ,  .F., .T.)
    endwith
  return

  proc Calculate_TJGIFLLAQM()
    with this
          * --- DblClick su Barra grafico costi
          GSPC2BCC(this;
              ,"DETTCOSTITIP";
             )
    endwith
  endproc
  proc Calculate_XMSLRGBAHP()
    with this
          * --- DblClick su Barra grafico ricavi
          GSPC2BCC(this;
              ,"DETTRICAVI";
             )
    endwith
  endproc
  proc Calculate_SZUCKVRLWY()
    with this
          * --- DblClick su Barra grafico manodopera
          GSPC2BCC(this;
              ,"DETTOREMDO";
             )
    endwith
  endproc
  proc Calculate_VVWVIWCVWI()
    with this
          * --- DblClick su gauge avanzamento tecnico
          GSPC2BCC(this;
              ,"DETTTECATT";
             )
    endwith
  endproc
  proc Calculate_HBZTTNNXJN()
    with this
          * --- DblClick su Barra grafico margine
          GSPC2BCC(this;
              ,"DETTMARGINE";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_126.enabled = this.oPgFrm.Page1.oPag.oBtn_1_126.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.Graph13.Event(cEvent)
      .oPgFrm.Page1.oPag.Graph16.Event(cEvent)
      .oPgFrm.Page1.oPag.Graph14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
      .oPgFrm.Page1.oPag.lbltotoreprev1.Event(cEvent)
      .oPgFrm.Page1.oPag.lblOreManPag11.Event(cEvent)
      .oPgFrm.Page1.oPag.lbltotorecons1.Event(cEvent)
      .oPgFrm.Page1.oPag.lbltotoreres1.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_73.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_75.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_79.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_82.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_83.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_84.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_85.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_86.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_87.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_88.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_89.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_90.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_91.Event(cEvent)
      .oPgFrm.Page1.oPag.Graph15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_94.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_95.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_96.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_97.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_98.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_99.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_100.Event(cEvent)
        if lower(cEvent)==lower("w_Graph13 SelectedFromFoxCharts")
          .Calculate_TJGIFLLAQM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Graph14 SelectedFromFoxCharts")
          .Calculate_XMSLRGBAHP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Graph16 SelectedFromFoxCharts")
          .Calculate_SZUCKVRLWY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_gaugetec SelectedFromIndicator")
          .Calculate_VVWVIWCVWI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.gaugecosti.Event(cEvent)
      .oPgFrm.Page1.oPag.gaugericavi.Event(cEvent)
      .oPgFrm.Page1.oPag.gaugemarat.Event(cEvent)
      .oPgFrm.Page1.oPag.gaugemarre.Event(cEvent)
      .oPgFrm.Page1.oPag.gaugemarto.Event(cEvent)
        if lower(cEvent)==lower("w_Graph15 SelectedFromFoxCharts")
          .Calculate_HBZTTNNXJN()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.gaugeob.Event(cEvent)
      .oPgFrm.Page1.oPag.gaugemdo.Event(cEvent)
      .oPgFrm.Page1.oPag.gaugetec.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_4'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNCODCON,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_CODCLI = NVL(_Link_.CNCODCON,space(15))
      this.w_CODVAL = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_CODCLI = space(15)
      this.w_CODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_4.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_4.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_5.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_5.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_9.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_9.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oTPREVEN_1_13.RadioValue()==this.w_TPREVEN)
      this.oPgFrm.Page1.oPag.oTPREVEN_1_13.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINI_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATAINI)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VALORE_PER_MDO = this.w_VALORE_PER_MDO
    this.o_VALORE_PER_COSTI = this.w_VALORE_PER_COSTI
    this.o_VALORE_PER_ATEC = this.w_VALORE_PER_ATEC
    this.o_VALORE_PER_MARAT = this.w_VALORE_PER_MARAT
    this.o_VALORE_PER_MARRE = this.w_VALORE_PER_MARRE
    this.o_VALORE_PER_MARTO = this.w_VALORE_PER_MARTO
    this.o_AVANZ_RICAVI = this.w_AVANZ_RICAVI
    this.o_VALORE_OBIETTIVO = this.w_VALORE_OBIETTIVO
    return

enddefine

* --- Define pages as container
define class tgspc1kccPag1 as StdContainer
  Width  = 1203
  height = 654
  stdWidth  = 1203
  stdheight = 654
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM_1_4 as StdField with uid="AZJKYNVCBT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 65062106,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=79, Top=14, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCODCOM_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM
     i_obj.ecpSave()
  endproc

  add object oDATAINI_1_5 as StdField with uid="SEICIEKUKM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data avanzamento",;
    HelpContextID = 213790006,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=493, Top=14


  add object oBtn_1_6 as StdButton with uid="BBAOQHTXNI",left=1148, top=6, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il grafico";
    , HelpContextID = 57958653;
    , anchor=8, Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        GSPC1BCC(this.Parent.oContained,"VISUA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty (.w_CODCOM))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="CKEEXDXLZC",left=1148, top=603, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Annulla";
    , HelpContextID = 94941370;
    , anchor=44, Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCOM_1_9 as StdField with uid="FKSUBIMSOW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 65003210,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=209, Top=14, InputMask=replicate('X',30)


  add object oTPREVEN_1_13 as StdCombo with uid="ODNZSENJIX",rtseq=9,rtrep=.f.,left=620,top=14,width=103,height=20;
    , ToolTipText = "Tipo di calcolo preventivo";
    , HelpContextID = 191750858;
    , cFormVar="w_TPREVEN",RowSource=""+"Aggiornato,"+"Iniziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTPREVEN_1_13.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'I',;
    ' ')))
  endfunc
  func oTPREVEN_1_13.GetRadio()
    this.Parent.oContained.w_TPREVEN = this.RadioValue()
    return .t.
  endfunc

  func oTPREVEN_1_13.SetRadio()
    this.Parent.oContained.w_TPREVEN=trim(this.Parent.oContained.w_TPREVEN)
    this.value = ;
      iif(this.Parent.oContained.w_TPREVEN=='A',1,;
      iif(this.Parent.oContained.w_TPREVEN=='I',2,;
      0))
  endfunc


  add object oObj_1_17 as cp_runprogram with uid="KREHEAHBUT",left=1228, top=151, width=151,height=21,;
    caption='GSPC1BCC',;
   bGlobalFont=.t.,;
    prg="GSPC1BCC('EXIT_')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 255852457


  add object Graph13 as cp_FoxCharts with uid="HHGPMPJAEN",left=211, top=97, width=457,height=240,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_ImpCos_",cFileCfg="..\COMM\EXE\QUERY\GSPC1KCC_COSTI",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",var="w_CODCOS",GetFldVal="TIPOCOSTO",Anchor=195,;
    cEvent = "GraphPag1",;
    nPag=1;
    , HelpContextID = 75738854


  add object Graph16 as cp_FoxCharts with uid="TJTOQKIRXL",left=671, top=390, width=464,height=258,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_GraphManod_",cFileCfg="..\COMM\EXE\QUERY\GSPC1KCC_MANOD",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",GetFldVal="TIPMOV",var="w_TIPMOVM",Anchor=44,;
    cEvent = "GraphPag1",;
    nPag=1;
    , HelpContextID = 75738854


  add object Graph14 as cp_FoxCharts with uid="OJXQQVDOLN",left=671, top=97, width=464,height=240,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_GraphRicavi_",cFileCfg="..\COMM\EXE\QUERY\GSPC1KCC_RICAVI",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",GetFldVal="TIPMOV",var="w_TIPMOVR",Anchor=105,;
    cEvent = "GraphPag3",;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_67 as cp_calclbl1 with uid="SWRKNYZGGC",left=680, top=372, width=154,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,BorderStyle=1,Anchor=12,;
    nPag=1;
    , HelpContextID = 2084583


  add object lbltotoreprev1 as cp_calclbl1 with uid="MRYBLHDMPT",left=680, top=357, width=154,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=12,;
    nPag=1;
    , HelpContextID = 75738854


  add object lblOreManPag11 as cp_calclbl1 with uid="NKWMSXTNHF",left=680, top=338, width=461,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Ore manodopera",Alignment=2,FontBold=.t.,FontSize=12,BorderStyle=1,Anchor=12,;
    nPag=1;
    , HelpContextID = 75738854


  add object lbltotorecons1 as cp_calclbl1 with uid="CXLQFPSGHN",left=833, top=357, width=154,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=12,;
    nPag=1;
    , HelpContextID = 75738854


  add object lbltotoreres1 as cp_calclbl1 with uid="NGYAUQPENG",left=986, top=357, width=155,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=12,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_72 as cp_calclbl1 with uid="IINZTAAZUB",left=833, top=372, width=154,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,BorderStyle=1,Anchor=12,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_73 as cp_calclbl1 with uid="AAOJEGUZIN",left=986, top=372, width=155,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,BorderStyle=1,Anchor=12,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_74 as cp_calclbl1 with uid="AEKYHEWGOF",left=1025, top=79, width=116,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=8,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_75 as cp_calclbl1 with uid="VCEITFVFGI",left=1025, top=64, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=8,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_76 as cp_calclbl1 with uid="EXEPEUPBFT",left=910, top=79, width=116,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=8,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_77 as cp_calclbl1 with uid="DBFJUEWCAP",left=795, top=79, width=116,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=8,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_78 as cp_calclbl1 with uid="SAWYOXRXWJ",left=910, top=64, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=8,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_79 as cp_calclbl1 with uid="KNNBPVFDLQ",left=795, top=64, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=8,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_80 as cp_calclbl1 with uid="XNPHHPAEJC",left=680, top=45, width=461,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,FontSize=12,bGlobalGont=.f.,BorderStyle=1,Anchor=8,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_81 as cp_calclbl1 with uid="ACGQXIOJLT",left=680, top=64, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=8,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_82 as cp_calclbl1 with uid="JTLFRYXYUN",left=680, top=79, width=116,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=8,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_83 as cp_calclbl1 with uid="RQQKSGAJJT",left=557, top=79, width=116,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=0,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_84 as cp_calclbl1 with uid="NPPYRGKWKO",left=557, top=64, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=0,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_85 as cp_calclbl1 with uid="DWCZCXYFTR",left=442, top=79, width=116,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=0,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_86 as cp_calclbl1 with uid="VGKNKXSHKO",left=327, top=79, width=116,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=0,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_87 as cp_calclbl1 with uid="VSLVKRMHNL",left=442, top=64, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=0,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_88 as cp_calclbl1 with uid="PSMEBOOFGZ",left=327, top=64, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=0,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_89 as cp_calclbl1 with uid="WRYGQOFBCQ",left=212, top=45, width=461,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,FontSize=12,bGlobalGont=.f.,BorderStyle=1,Anchor=0,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_90 as cp_calclbl1 with uid="AURNZLAYWR",left=212, top=64, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=0,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_91 as cp_calclbl1 with uid="WJNBDUMEDJ",left=212, top=79, width=116,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=0,;
    nPag=1;
    , HelpContextID = 2084583


  add object Graph15 as cp_FoxCharts with uid="ZZDMBOHLYJ",left=211, top=390, width=457,height=258,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="_GraphMargine_",cFileCfg="..\COMM\EXE\QUERY\GSPC1KCC_MARGINE",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",GetFldVal="TIPO",var="w_Graph16_LEGEND",Anchor=134,;
    cEvent = "GraphPag1",;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_94 as cp_calclbl1 with uid="DGPMOOOLLZ",left=212, top=372, width=154,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=6,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_95 as cp_calclbl1 with uid="EVYQFGVDCV",left=212, top=357, width=154,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=6,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_96 as cp_calclbl1 with uid="HSWURTPZME",left=212, top=338, width=461,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Margine",Alignment=2,FontBold=.t.,FontSize=12,bGlobalGont=.f.,BorderStyle=1,Anchor=6,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_97 as cp_calclbl1 with uid="DTBYJITEXM",left=365, top=357, width=154,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=6,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_98 as cp_calclbl1 with uid="OVFZVXKKDZ",left=518, top=357, width=155,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Alignment=2,FontBold=.t.,BorderStyle=1,Anchor=6,;
    nPag=1;
    , HelpContextID = 75738854


  add object oObj_1_99 as cp_calclbl1 with uid="UJZYPKJKZT",left=365, top=372, width=154,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=6,;
    nPag=1;
    , HelpContextID = 2084583


  add object oObj_1_100 as cp_calclbl1 with uid="GNCKBILFDJ",left=518, top=372, width=155,height=16,;
    caption='Lbl1Pag1',;
   bGlobalFont=.t.,;
    caption="0",FontName="Arial",FontSize=10,FontBold=True,Alignment=2,ForeColor=0,BorderStyle=1,Anchor=6,;
    nPag=1;
    , HelpContextID = 2084583


  add object gaugecosti as cp_GaugeBar with uid="SDCRMOPPZG",left=3, top=181, width=107,height=94,;
    caption='Object',;
   bGlobalFont=.t.,;
    Min=0,Max=100,Visible=.t.,nPerCentFontSize=12,;
    cEvent = "AggiornaGauge",;
    nPag=1;
    , HelpContextID = 75738854


  add object gaugericavi as cp_GaugeBar with uid="XMSTFLHRLK",left=110, top=181, width=102,height=94,;
    caption='Object',;
   bGlobalFont=.t.,;
    min=0,max=100,Visible=.t.,nPerCentFontSize=12,;
    cEvent = "AggiornaGauge",;
    nPag=1;
    , HelpContextID = 75738854


  add object gaugemarat as cp_GaugeBar with uid="GFPUZHVVHK",left=3, top=312, width=107,height=94,;
    caption='Object',;
   bGlobalFont=.t.,;
    min=0,max=100,Visible=.t.,nPerCentFontSize=12,;
    cEvent = "AggiornaGauge",;
    nPag=1;
    , HelpContextID = 75738854


  add object gaugemarre as cp_GaugeBar with uid="OSLWCWPZSL",left=110, top=312, width=102,height=94,;
    caption='Object',;
   bGlobalFont=.t.,;
    min=0,max=100,Visible=.t.,nPerCentFontSize=12,;
    cEvent = "AggiornaGauge",;
    nPag=1;
    , HelpContextID = 75738854


  add object gaugemarto as cp_GaugeBar with uid="QQUEZEYCDW",left=3, top=445, width=107,height=94,;
    caption='Object',;
   bGlobalFont=.t.,;
    min=0,max=100,Visible=.t.,nPerCentFontSize=12,;
    cEvent = "AggiornaGauge",;
    nPag=1;
    , HelpContextID = 75738854


  add object gaugeob as cp_GaugeBar with uid="RSHUCWLYWG",left=110, top=445, width=102,height=94,;
    caption='Object',;
   bGlobalFont=.t.,;
    min=0,max=100,Visible=.t.,nPerCentFontSize=12,;
    cEvent = "AggiornaGauge",;
    nPag=1;
    , HelpContextID = 75738854


  add object oBtn_1_126 as StdButton with uid="COGVCVGCCI",left=1148, top=56, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare (visualizzare tutti i grafici)";
    , HelpContextID = 39530790;
    , anchor=8,Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_126.Click()
      with this.Parent.oContained
        GSPC1BCC(this.Parent.oContained,"PRINT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_126.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSTAMP1)
      endwith
    endif
  endfunc


  add object gaugemdo as cp_GaugeBar with uid="RRWCQOWJYT",left=110, top=56, width=102,height=94,;
    caption='Object',;
   bGlobalFont=.t.,;
    min=0,max=100,Value=0,Visible=.t.,nPerCentFontSize=12,;
    cEvent = "AggiornaGauge",;
    nPag=1;
    , HelpContextID = 75738854


  add object gaugetec as cp_GaugeBar with uid="ACIYFTAURZ",left=3, top=56, width=107,height=94,;
    caption='Object',;
   bGlobalFont=.t.,;
    min=0,max=100,Value=0,Visible=.t.,nPerCentFontSize=12,;
    cEvent = "AggiornaGauge",;
    nPag=1;
    , HelpContextID = 75738854

  add object oStr_1_8 as StdString with uid="QNJDIUUKFI",Visible=.t., Left=1, Top=14,;
    Alignment=1, Width=76, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="NMRERORRPB",Visible=.t., Left=440, Top=14,;
    Alignment=1, Width=51, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="DHNDBOIGCN",Visible=.t., Left=575, Top=14,;
    Alignment=1, Width=42, Height=15,;
    Caption="Prev.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_129 as StdString with uid="CVVEJYMAXV",Visible=.t., Left=14, Top=39,;
    Alignment=0, Width=91, Height=18,;
    Caption="Avanz. tecnico"  ;
  , bGlobalFont=.t.

  add object oStr_1_130 as StdString with uid="ACGKDJDGSP",Visible=.t., Left=125, Top=39,;
    Alignment=0, Width=74, Height=18,;
    Caption="Avanz. MDO"  ;
  , bGlobalFont=.t.

  add object oStr_1_131 as StdString with uid="AVEEQKDXMT",Visible=.t., Left=20, Top=164,;
    Alignment=0, Width=71, Height=18,;
    Caption="Avanz. costi"  ;
  , bGlobalFont=.t.

  add object oStr_1_132 as StdString with uid="JMZJXGNHIC",Visible=.t., Left=125, Top=164,;
    Alignment=0, Width=74, Height=18,;
    Caption="Avanz. ricavi"  ;
  , bGlobalFont=.t.

  add object oStr_1_133 as StdString with uid="NXHFHREPVB",Visible=.t., Left=12, Top=295,;
    Alignment=0, Width=92, Height=18,;
    Caption="Margine attuale"  ;
  , bGlobalFont=.t.

  add object oStr_1_134 as StdString with uid="PTBVUWCBER",Visible=.t., Left=117, Top=295,;
    Alignment=0, Width=92, Height=18,;
    Caption="Margine residuo"  ;
  , bGlobalFont=.t.

  add object oStr_1_135 as StdString with uid="SVWTXMRQDV",Visible=.t., Left=12, Top=428,;
    Alignment=0, Width=89, Height=18,;
    Caption="Margine atteso"  ;
  , bGlobalFont=.t.

  add object oStr_1_136 as StdString with uid="XZJGZZHRYS",Visible=.t., Left=116, Top=428,;
    Alignment=0, Width=101, Height=18,;
    Caption="Margine obiettivo"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc1kcc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gspc1kcc
* --- File per le classi personalizzate --- *
#include "cp_app_lang.inc"

Define Class cp_calclbl1 as cp_calclbl

  Proc Init
    This.bGlobalfont=.f.
    DoDefault()
EndDefine
* --- Fine Area Manuale
