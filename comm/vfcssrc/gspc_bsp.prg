* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bsp                                                        *
*              Stampa preventivo                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_106]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-02                                                      *
* Last revis.: 2006-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bsp",oParentObject)
return(i_retval)

define class tgspc_bsp as StdBatch
  * --- Local variables
  w_CURSORNA = space(10)
  w_LIVELLO = space(100)
  w_IMPORTO_MA = 0
  w_NUMPOINT = 0
  w_COND = .f.
  w_LUNGH = 0
  w_IMPORTO_MD = 0
  w_TIPPRE = space(1)
  w_POSREC = 0
  w_IMPORTO_AP = 0
  w_FLPREV = space(1)
  w_PADRE = space(15)
  w_IMPORTO_AL = 0
  w_TIPCOM = space(1)
  w_RICERCA = space(100)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_TIPSTR = space(1)
  w_RIFKEY = space(100)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Preventivo (da GSPC_SSP)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select * From ( this.w_CURSORNA ) Order By LvlKey Into Cursor Temp NoFilter
    ah_Msg("Lettura importi...",.T.)
    * --- Se preventivo iniziale prendo tutte le attivit� (anche quelle con padre forfettario)
    this.w_TIPPRE = IIF(this.oParentObject.w_TIPO="I","N"," ")
    * --- Calcolo gli Importi da stampare a preventivo
    * --- Prendo gli Importi dai Sotto Conti e dalle Attivit�
    Vq_Exec ("..\Comm\Exe\Query\Gspc_Bsp.Vqr",This,"Importi")
    if RECCOUNT()>0
      * --- Esistono dati significativi per la stampa
      Select CodAtt,TipAtt, Min( FlPrev ) As FlPrev, Sum(Forf_MA) As Forf_MA, Sum(Costi_MA) As Costi_MA, ;
      Sum(Forf_MD) As Forf_MD, Sum(Costi_MD) As Costi_MD, Sum(Forf_AP) As Forf_AP, Sum(Costi_AP) As Costi_AP, ;
      Sum(Forf_AL) As Forf_AL, Sum(Costi_AL) As Costi_AL From Importi Group By CodAtt,TipAtt Into Cursor Importi NoFilter
      * --- Costruisco il Cursore di Stampa - Solo Codice Attivit� e Tipo (La commessa � uguale)
      * --- All'interno dei Campi Preventivo_xx verranno posti gli Importi (Sub Totali)
      Select Temp.*, Importi.FLPrev, AtCodAtt As CodPad, Occurs(".",LvlKey) As NumPoint, ; 
 IIF(Importi.FLPrev="S" OR IMPORTI.FORF_MA<>0,Importi.Forf_MA,Importi.Costi_MA) As Preventivo_MA, ; 
 IIF(Importi.FLPrev="S" OR IMPORTI.FORF_MD<>0,Importi.Forf_MD,Importi.Costi_MD) As Preventivo_MD, ; 
 IIF(Importi.FLPrev="S" OR IMPORTI.FORF_AP<>0,Importi.Forf_AP,Importi.Costi_AP) As Preventivo_AP, ; 
 IIF(Importi.FLPrev="S" OR IMPORTI.FORF_AL<>0,Importi.Forf_AL,Importi.Costi_AL) As Preventivo_AL,"YES" As FirstTry ; 
 From Temp Left Outer Join Importi On Importi.TipAtt=Temp.AtTipAtt And ; 
 Importi.CodAtt=Temp.AtCodAtt Order By LvlKey Desc Into Cursor Temp NoFilter
      Cur = WrCursor ("Temp")
      if this.oParentObject.w_TIPO="A" and this.oParentObject.w_WITHATTI="S"
        * --- Si memorizzano gli importi attivit� perch� devono essere cancellati 
        *     da TEMP se il preventivo � aggiornato.
        *     Saranno reinseriti alla fine.
        select attipatt,atcodatt,preventivo_ma,preventivo_md, ; 
 preventivo_ap,preventivo_al from temp ; 
 where attipatt="A" and (nvl(preventivo_ma,0)<>0 or nvl(preventivo_md,0)<>0 ; 
 or nvl(preventivo_ap,0)<>0 or nvl(preventivo_al,0)<>0) ; 
 into cursor RIPRISTINO
      endif
      Select Temp
      Go Top
      Scan for ATTIPCOM="S" or empty(nvl(ATTIPCOM,""))
      L_IMPNONZERO=nvl(preventivo_ma,0)<>0; 
 or nvl(preventivo_md,0)<>0 ; 
 or nvl(preventivo_ap,0)<>0 ; 
 or nvl(preventivo_al,0)<>0
      L_FLPREV=nvl(looktab("ATTIVITA","ATFLPREV","ATCODCOM",ATCODCOM,"ATCODATT",ATCODATT,"ATTIPATT",ATTIPATT),"")
      if empty(nvl(ATTIPCOM,""))
        L_TIPCOM=nvl(looktab("ATTIVITA","ATTIPCOM","ATCODCOM",ATCODCOM,"ATCODATT",ATCODATT,"ATTIPATT",ATTIPATT),"")
        if L_TIPCOM="S"
          replace ATTIPCOM with L_TIPCOM
        endif
      endif
      if this.oParentObject.w_TIPO="I"
        if L_IMPNONZERO or L_FLPREV="S"
          replace flprev with "S"
        else
          replace flprev with "N"
        endif
      else
        if L_FLPREV="S" and L_IMPNONZERO 
 
          replace flprev with "S"
        else
          replace flprev with "N"
        endif
      endif
      EndScan
      Select Temp
      Go Top
      * --- Calcolo Gli Importi. La procedura Scorre riga per riga e per ognuna
      * --- Vado anche a riempire CodPad con il conto padre della riga
      * --- Il cursore deve essere ordinato in modo da consultare prima i figli dei padri (lvlkey DESC)
      * --- Ogni figlio infatti cerca il padre e in esso somma il suo importo
      Scan
      this.w_LIVELLO = RTrim(Temp.LvlKey)
      this.w_LUNGH = Len(this.w_LIVELLO)
      this.w_POSREC = RecNo()
      this.w_NUMPOINT = Occurs(".",this.w_LIVELLO)
      * --- Per ogni record aggiungo nel padre i vari importi
      ah_Msg("Calcolo totali di stampa...: %1",.T.,,,this.w_PADRE)
      if this.w_NUMPOINT>0
        * --- Questa variabile contiene il LEVELKEY del padre
        this.w_RICERCA = Left( Left ( this.w_LIVELLO , AT ( "." , this.w_LIVELLO , this.w_NUMPOINT ) -1 )+space(200),200)
        this.w_IMPORTO_MA = Nvl(Preventivo_MA,0)
        this.w_IMPORTO_MD = Nvl(Preventivo_MD,0)
        this.w_IMPORTO_AP = Nvl(Preventivo_AP,0)
        this.w_IMPORTO_AL = Nvl(Preventivo_AL,0)
        Select Temp
        Go Top
        Locate For LvlKey = this.w_RICERCA
        if Found()
          this.w_PADRE = Temp.AtCodAtt
          this.w_FLPREV = nvl(Temp.FLPrev,"")
          this.w_TIPCOM = Temp.AtTipCom
          * --- Il preventivo del padre NON deve essere ricalcolato sul cursore se
          * --- 1) sto considerando un preventivo iniziale E il forfeit sul (SOTTOCONTO) padre non � nullo
          * --- 2) sto considerando un preventivo aggiornato E gli aggiornati sui figli (DEL SOTTOCONTO) sono nulli
          * --- (si noti che l'aggiornamento dai sottoconti ai conti deve essere fatto in ogni caso)
          this.w_COND = (this.w_TIPCOM="C") or (this.w_TIPCOM=" ") or (this.w_FLPREV<>"S" and this.w_TIPCOM="S")
          if this.w_COND
            if FirstTry="YES"
              * --- La prima volta che passo azzero l'importo sul padre
              Replace Preventivo_MA With Nvl(this.w_IMPORTO_MA,0)
              Replace Preventivo_MD With Nvl(this.w_IMPORTO_MD,0)
              Replace Preventivo_AP With Nvl(this.w_IMPORTO_AP,0)
              Replace Preventivo_AL With Nvl(this.w_IMPORTO_AL,0)
              Replace FirstTry With "NO"
            else
              * --- Le volte successive sommo a quello che avevo gi� tirato su dagli altri figli
              Replace Preventivo_MA With Nvl(Preventivo_MA,0)+Nvl(this.w_IMPORTO_MA,0)
              Replace Preventivo_MD With Nvl(Preventivo_MD,0)+Nvl(this.w_IMPORTO_MD,0)
              Replace Preventivo_AP With Nvl(Preventivo_AP,0)+Nvl(this.w_IMPORTO_AP,0)
              Replace Preventivo_AL With Nvl(Preventivo_AL,0)+Nvl(this.w_IMPORTO_AL,0)
            endif
          endif
          Go this.w_POSREC
          Select Temp
          Replace CodPad With this.w_PADRE
          if ! this.w_COND
            Replace Preventivo_ma With 0 
 Replace Preventivo_md With 0 
 Replace Preventivo_ap With 0 
 Replace Preventivo_al With 0
          endif
        endif
      endif
      Select Temp
      EndScan
      * --- Passo tutto al cursore di stampa (Elimino le Attivit�) Stampo a livelli
      if this.oParentObject.w_WITHATTI="S"
        * --- Stampo anche le Attivit�
        Select *,Occurs(".",LvlKey) As Ordine From Temp Order By Ordine,LvlKey Into Cursor __TMP__ NoFilter
      else
        * --- Stampo fino ai Sotto Conti
        Select *,Occurs(".",LvlKey) As Ordine From Temp Where AttipAtt<>"A" Order By Ordine,LvlKey Into Cursor __TMP__ NoFilter
      endif
      if this.oParentObject.w_TIPO="A" and this.oParentObject.w_WITHATTI="S"
        * --- Si reinseriscono gli importi attivit� perch� devono sono stati cancellati 
        *     da TEMP se il preventivo � aggiornato.
        =WrCursor("__TMP__") 
 select ripristino 
 scan 
 update __TMP__ set ; 
 __TMP__.preventivo_ma=ripristino.preventivo_ma,; 
 __TMP__.preventivo_md=ripristino.preventivo_md,; 
 __TMP__.preventivo_ap=ripristino.preventivo_ap,; 
 __TMP__.preventivo_al=ripristino.preventivo_al; 
 where __TMP__.attipatt=ripristino.attipatt and ; 
 __TMP__.atcodatt=ripristino.atcodatt 
 endscan 
 USE 
 select __TMP__
      endif
      if this.oParentObject.w_NOZERO="S"
        * --- Tolgo le righe con Importi a Zero
        Select * from __TMP__ into cursor APPOGGIO NoFilter 
 select __TMP__ 
 USE 
 Select * From APPOGGIO ; 
 Where Preventivo_MA<>0 Or Preventivo_MD<>0 Or Preventivo_AP<>0 Or Preventivo_AL<>0 ; 
 Order By Ordine,LvlKey Into Cursor __TMP__ NoFilter 
 select APPOGGIO 
 USE 
 select __TMP__
      endif
      * --- Selezioni nel report
      L_COMMESSA=this.oParentObject.w_CODCOM
      L_DESCOM=this.oParentObject.w_DESCOM
      L_DECIMALI=this.oParentObject.w_DECTOT
      L_TITOLO=iif(this.oParentObject.w_TIPO="I","Iniziale","Aggiornato")
      L_DETTATT=this.oParentObject.w_WITHATTI
      L_NOZERO=this.oParentObject.w_NOZERO
      CP_CHPRN("..\Comm\Exe\Query\Gspc_Bsp.Frx", " ", this)
    else
      * --- Non esistono dati da stampare
      ah_ErrorMsg("La selezione di stampa � vuota","!","")
    endif
    if Used("Importi")
      Select ("Importi")
      Use
    endif
    if Used("Temp")
      Select ("Temp")
      Use
    endif
    if used("__TMP__")
      Select __TMP__ 
 use
    endif
    if used(this.w_CURSORNA)
      Select (this.w_CURSORNA) 
 use
    endif
    if used(this.w_COUTCURS)
      Select (this.w_COUTCURS) 
 use
    endif
    if used(this.w_INPCURS)
      Select (this.w_INPCURS) 
 use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea Cursore Preventivo
    this.w_CURSORNA = SYS(2015)
    * --- Definizione parametri da passare alla CP_EXPDB
    this.w_COUTCURS = this.w_CURSORNA
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = "CPROWORD"
    * --- Altri campi per ordinamento
    this.w_TIPSTR = "P"
    ah_Msg("Caricamento progetto...",.T.)
    if Empty ( this.oParentObject.w_CODCONTO )
      * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
      VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
    else
      * --- Recupero solo la parte di struttura identificata dal conto
      VQ_EXEC("..\COMM\EXE\QUERY\GSPC0BSC.VQR",this,"query")
    endif
    Select "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if USED("__tmp__")
        SELECT "__tmp__" 
 USE
      endif
    else
      ah_ErrorMsg("La selezione di stampa � vuota","!","")
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
