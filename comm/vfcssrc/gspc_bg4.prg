* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bg4                                                        *
*              Copia progetto                                                  *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_506]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-18                                                      *
* Last revis.: 2011-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bg4",oParentObject,m.pTipOp)
return(i_retval)

define class tgspc_bg4 as StdBatch
  * --- Local variables
  w_CONTA = 0
  pTipOp = space(10)
  w_RINOMINA = space(15)
  w_DESCRIN = space(30)
  w_CODCONTO = space(15)
  w_CODCOM = space(15)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_RIFKEY = space(100)
  w_POSIZ = 0
  w_LVLKEY = space(200)
  w_LASTPOINT = 0
  w_CODATTCUR = space(15)
  w_RELAZ = .NULL.
  w_TIPSTRLOC = space(1)
  w_TIPO = space(1)
  w_NODO = space(15)
  w_DESNODO = space(30)
  w_CommessaDiPartenza = space(15)
  w_CommessaDiDestinazione = space(15)
  w_AttivitaPadre = space(15)
  w_AttivitaFiglio = space(15)
  w_SovraScrivi = 0
  w_NonSovrascrivere = .f.
  w_TipoAttivitaPadre = space(1)
  w_TipoAttivitaFiglio = space(1)
  w_Trovato = space(15)
  w_ProssimoCproword = 0
  w_MillesimiResidui = 0
  w_Messaggio = space(250)
  w_GestioneLegami = .NULL.
  w_GestioneNodo = .NULL.
  w_OKATT = .f.
  w_OK = .f.
  w_OKREN = .f.
  w_ExistMovPrevOrig = .f.
  w_EliminaMovPrev = 0
  w_TSTR = space(1)
  w_OKDEL = .f.
  w_PROGR = space(1)
  w_ERRNO = 0
  w_FDATINI = ctod("  /  /  ")
  w_FDATFIN = ctod("  /  /  ")
  w_D_TIPATT = space(1)
  w_D_CODATT = space(15)
  w_D_DATINI = ctod("  /  /  ")
  w_D_DATFIN = ctod("  /  /  ")
  w_D_DURGIO = 0
  w_D_RITAR = 0
  w_D_TIPPRE = space(2)
  w_O_DATINI = ctod("  /  /  ")
  w_O_DATFIN = ctod("  /  /  ")
  w_O_DURGIO = 0
  w_MillesimiResidui = 0
  w_MADATREG = ctod("  /  /  ")
  w_MATIPMOV = space(1)
  w_MA__ANNO = space(4)
  w_UTCV = 0
  w_UTCC = 0
  w_UTDV = ctod("  /  /  ")
  w_UTDC = ctod("  /  /  ")
  w_MASERIAL = space(10)
  w_MACODVAL = space(3)
  w_NEWCODVAL = space(3)
  w_ValutaCommessaDiDestinazione = space(3)
  w_MACAOVAL = 0
  w_CAOCOM = 0
  w_MACODLIS = space(5)
  w_ATTIVITA = space(15)
  CopiaMovimento = .f.
  w_NEWSERIAL = space(10)
  w_MANUMREG = 0
  w_MAALFREG = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MACODKEY = space(41)
  w_MACODART = space(20)
  w_MACODVAR = space(15)
  w_MAUNIMIS = space(3)
  w_MAQTAMOV = 0
  w_MAPREZZO = 0
  w_MADATANN = ctod("  /  /  ")
  w_MADESCRI = space(40)
  w_MA__NOTE = space(0)
  w_MACODCOS = space(5)
  w_MAOPPREV = space(1)
  w_MAOPCONS = space(1)
  w_MAQTA1UM = 0
  w_MACOATTS = space(15)
  w_MATIPATT = space(1)
  w_MACODFAM = space(5)
  w_MACOCOMS = space(15)
  w_MAVALRIG = 0
  w_MARIFKIT = 0
  w_OKMillesimi = 0
  w_MESS = space(100)
  w_PARAMETROMESSAGGIO1 = space(10)
  * --- WorkFile variables
  STRUTTUR_idx=0
  ATTIVITA_idx=0
  MAFCOSTI_idx=0
  ATT_PREC_idx=0
  VALUTE_idx=0
  MAT_MAST_idx=0
  MAT_DETT_idx=0
  CAN_TIER_idx=0
  MA_COSTI_idx=0
  DICH_MDO_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  MVM_MAST_idx=0
  MVM_DETT_idx=0
  FABBIDET_idx=0
  GESPENNA_idx=0
  MOVICOST_idx=0
  PDA_DETT_idx=0
  CPAR_DEF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Assegnamenti
    this.w_CommessaDiPartenza = this.oParentObject.w_CODCOM1
    this.w_CommessaDiDestinazione = this.oParentObject.w_CODCOM2
    this.w_OK = .T.
    * --- Read from CPAR_DEF
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDPROGR"+;
        " from "+i_cTable+" CPAR_DEF where ";
            +"PDCHIAVE = "+cp_ToStrODBC("TAM");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDPROGR;
        from (i_cTable) where;
            PDCHIAVE = "TAM";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PROGR = NVL(cp_ToDate(_read_.PDPROGR),cp_NullValue(_read_.PDPROGR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    Create Cursor AttivitaDaCanc (CODICE C(15), TIPO C(1))
    A=WRCURSOR ("AttivitaDaCanc")
    do case
      case this.pTipop="Reload1"
        this.w_CODCONTO = this.oParentObject.w_CODCONTO1
        this.w_CODCOM = this.oParentObject.w_CODCOM1
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.notifyevent("Esegui1")
      case this.pTipop="Reload2"
        this.w_CODCONTO = this.oParentObject.w_CODCONTO2
        this.w_CODCOM = this.oParentObject.w_CODCOM2
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipop="Copia"
        * --- --Eplode ramo da copiare nel cursore avente nome in w_RamoDaCopiare
        this.w_CODCONTO = this.oParentObject.w_CODATT1
        this.w_CODCOM = this.oParentObject.w_CODCOM1
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- --Esegue controlli preventivi
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- --Esegue la copia
        * --- Try
        local bErr_034519B8
        bErr_034519B8=bTrsErr
        this.Try_034519B8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          L_PuntatoreFile = fCreate("LogCopiaStruttura.TXT")
          fWrite(L_PuntatoreFile,Message())
          fClose(L_PuntatoreFile)
          !NOTEPAD "LogCopiaStruttura.TXT"
        endif
        bTrsErr=bTrsErr or bErr_034519B8
        * --- End
        * --- --Copia Schede tecniche
        if this.oParentObject.w_CopiaSchedeTecniche = "S"
          this.Pag11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- --Aggiorna TreeVeiw
        this.w_CODCONTO = this.oParentObject.w_CODCONTO2
        this.w_CODCOM = this.oParentObject.w_CODCOM2
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_TREEV2.ExpandAll(.T.)     
        * --- --Valorizza eventualmente TIPCOM1 e TIPCOM2
      case this.pTipop="Menu1"
        Private Azione 
 store space(10) to Azione
        this.Pag8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        do case
          case Azione = "Copia2"
            this.oParentObject.w_StatoAttivita = "C"
            this.oParentObject.w_CODATT1 = this.oParentObject.w_CODATT3
          case Azione = "Copia"
            this.oParentObject.w_StatoAttivita = "P"
            this.oParentObject.w_CODATT1 = this.oParentObject.w_CODATT3
          case Azione="Gestione Nodo"
            if EMPTY(NVL(this.oParentObject.w_TIPCOM1,""))
              * --- Read from ATTIVITA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ATTIVITA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ATTIPCOM"+;
                  " from "+i_cTable+" ATTIVITA where ";
                      +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM1);
                      +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT1);
                      +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT3);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ATTIPCOM;
                  from (i_cTable) where;
                      ATCODCOM = this.oParentObject.w_CODCOM1;
                      and ATTIPATT = this.oParentObject.w_TIPATT1;
                      and ATCODATT = this.oParentObject.w_CODATT3;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_TIPCOM1 = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if g_APPLICATION<>"ADHOC REVOLUTION"
              do case
                case this.oParentObject.w_TIPCOM1 $ "CP"
                  this.w_GestioneNodo = GSPC_ACM(this.oParentObject.w_TIPSTR)
                case this.oParentObject.w_TIPCOM1 = "S"
                  this.w_GestioneNodo = GSPC_ASC(this.oParentObject.w_TIPSTR)
                case this.oParentObject.w_TIPCOM1 ="A"
                  this.w_GestioneNodo = GSPC_AAT(this.oParentObject.w_TIPSTR)
              endcase
            else
              do case
                case this.oParentObject.w_TIPCOM1 $ "CP"
                  this.w_GestioneNodo = GSPC_ACM()
                case this.oParentObject.w_TIPCOM1 = "S"
                  this.w_GestioneNodo = GSPC_ASC()
                case this.oParentObject.w_TIPCOM1 ="A"
                  this.w_GestioneNodo = GSPC_AAT()
              endcase
            endif
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_GestioneNodo.bSec1)
              i_retcode = 'stop'
              return
            endif
            this.w_GestioneNodo.ecpFilter()     
            this.w_GestioneNodo.w_ATTIPATT = this.oParentObject.w_TIPATT1
            this.w_GestioneNodo.w_ATCODATT = this.oParentObject.w_CODATT3
            this.w_GestioneNodo.w_ATCODCOM = this.oParentObject.w_CODCOM1
            this.w_GestioneNodo.ecpSave()     
          case Azione="Gestione Legami"
            if this.oParentObject.w_TIPATT1 <> "A"
              * --- Lancio la gestione delle relazioni
              if g_APPLICATION<>"ADHOC REVOLUTION"
                this.w_GestioneLegami = GSPC_MST(this.oParentObject.w_TIPSTR)
              else
                this.w_GestioneLegami = GSPC_MST()
              endif
              * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
              if !(this.w_GestioneLegami.bSec1)
                i_retcode = 'stop'
                return
              endif
              * --- --Interrogazione
              this.w_GestioneLegami.EcpFilter()     
              * --- inizializzo la chiave delle relazioni
              this.w_GestioneLegami.w_STCODCOM = this.oParentObject.w_CODCOM1
              this.w_GestioneLegami.w_STTIPSTR = this.oParentObject.w_TIPATT1
              this.w_GestioneLegami.w_STATTPAD = this.oParentObject.w_CODATT3
              * --- mi metto in interrogazione
              this.w_GestioneLegami.EcpSave()     
            else
              ah_ErrorMsg("Ai nodi di tipo attivit� non possono essere legati elementi","!","")
            endif
        endcase
      case this.pTipop="Menu2"
        Private Azione 
 store space(10) to Azione
        this.Pag9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        do case
          case Azione="Incolla"
            * --- --Assegnamenti
            this.w_CommessaDiPartenza = this.oParentObject.w_CODCOM1
            this.w_CommessaDiDestinazione = this.oParentObject.w_CODCOM2
            this.w_OK = .T.
            * --- --Se la commessa di origine � capoprogetto, occorre copiare utilizzando l'apposita voce di menu.
            if EMPTY(NVL(this.oParentObject.w_TIPCOM1,""))
              * --- Read from ATTIVITA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ATTIVITA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ATTIPCOM"+;
                  " from "+i_cTable+" ATTIVITA where ";
                      +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM1);
                      +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT1);
                      +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT1);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ATTIPCOM;
                  from (i_cTable) where;
                      ATCODCOM = this.oParentObject.w_CODCOM1;
                      and ATTIPATT = this.oParentObject.w_TIPATT1;
                      and ATCODATT = this.oParentObject.w_CODATT1;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_TIPCOM1 = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if this.oParentObject.w_TIPCOM1 = "P"
              this.Pag13()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
            * --- --Eplode ramo da copiare nel cursore avente nome in w_RamoDaCopiare
            this.w_CODCONTO = this.oParentObject.w_CODATT1
            this.w_CODCOM = this.oParentObject.w_CODCOM1
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- --Esegue controlli preventivi
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- --Esegue la copia
            * --- Try
            local bErr_04688B68
            bErr_04688B68=bTrsErr
            this.Try_04688B68()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              this.oParentObject.w_CODATT1 = ""
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              L_PuntatoreFile = fCreate("LogCopiaStruttura.TXT")
              fWrite(L_PuntatoreFile,Message())
              fClose(L_PuntatoreFile)
              !NOTEPAD "LogCopiaStruttura.TXT"
            endif
            bTrsErr=bTrsErr or bErr_04688B68
            * --- End
            * --- --Copia Schede tecniche
            if this.oParentObject.w_CopiaSchedeTecniche = "S"
              this.Pag11()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.oParentObject.w_CODATT1 = ""
            SELECT "QUERY"
            * --- --Aggiorna TreeVeiw
            this.w_CODCONTO = this.oParentObject.w_CODCONTO2
            this.w_CODCOM = this.oParentObject.w_CODCOM2
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_TREEV2.ExpandAll(.T.)     
          case Azione="Rinomina"
            this.w_TSTR = "P"
            this.w_RINOMINA = SPACE(15)
            this.w_DESCRIN = SPACE(30)
            if this.oParentObject.w_CODCOM2<>this.oParentObject.w_CODATT2
              this.w_OKREN = .f.
              if this.oParentObject.w_TIPATT2="A"
                this.w_TIPO = "A"
                this.w_NODO = this.oParentObject.w_CODATT2
                this.w_DESNODO = this.oParentObject.w_ATDESC2
                this.Pag14()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if this.w_OKATT
                  this.w_RINOMINA = this.w_NODO
                  this.w_DESCRIN = this.w_DESNODO
                  do GSPC_SG4 with this
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if this.w_OKREN AND NOT EMPTY(this.w_RINOMINA) AND (this.w_RINOMINA<>this.w_NODO Or this.w_DESCRIN<>this.w_DESNODO)
                    if this.w_RINOMINA==this.w_NODO And this.w_DESCRIN<>this.w_DESNODO
                      * --- Write into ATTIVITA
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"ATDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRIN),'ATTIVITA','ATDESCRI');
                            +i_ccchkf ;
                        +" where ";
                            +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
                            +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT2);
                            +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT2);
                               )
                      else
                        update (i_cTable) set;
                            ATDESCRI = this.w_DESCRIN;
                            &i_ccchkf. ;
                         where;
                            ATCODCOM = this.oParentObject.w_CODCOM2;
                            and ATTIPATT = this.oParentObject.w_TIPATT2;
                            and ATCODATT = this.oParentObject.w_CODATT2;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                    else
                      * --- Try
                      local bErr_046D84E8
                      bErr_046D84E8=bTrsErr
                      this.Try_046D84E8()
                      * --- Catch
                      if !empty(i_Error)
                        i_ErrMsg=i_Error
                        i_Error=''
                        * --- rollback
                        bTrsErr=.t.
                        cp_EndTrs(.t.)
                        ah_ErrorMsg("Impossibile rinominare attivit� %1",,"",alltrim(this.w_NODO))
                      endif
                      bTrsErr=bTrsErr or bErr_046D84E8
                      * --- End
                    endif
                  endif
                else
                  ah_ErrorMsg(this.w_MESS,"!",,alltrim(this.oParentObject.w_CODATT2))
                endif
              else
                this.w_TIPO = "P"
                this.w_NODO = this.oParentObject.w_CODATT2
                this.w_DESNODO = this.oParentObject.w_ATDESC2
                this.w_RINOMINA = this.w_NODO
                this.w_DESCRIN = this.w_DESNODO
                do GSPC_SG4 with this
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if this.w_OKREN AND NOT EMPTY(this.w_RINOMINA) AND (this.w_RINOMINA<>this.w_NODO Or this.w_DESCRIN<>this.w_DESNODO)
                  if this.w_RINOMINA==this.w_NODO And this.w_DESCRIN<>this.w_DESNODO
                    * --- Write into ATTIVITA
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"ATDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRIN),'ATTIVITA','ATDESCRI');
                          +i_ccchkf ;
                      +" where ";
                          +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
                          +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT2);
                          +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT2);
                             )
                    else
                      update (i_cTable) set;
                          ATDESCRI = this.w_DESCRIN;
                          &i_ccchkf. ;
                       where;
                          ATCODCOM = this.oParentObject.w_CODCOM2;
                          and ATTIPATT = this.oParentObject.w_TIPATT2;
                          and ATCODATT = this.oParentObject.w_CODATT2;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  else
                    * --- Try
                    local bErr_04676738
                    bErr_04676738=bTrsErr
                    this.Try_04676738()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- rollback
                      bTrsErr=.t.
                      cp_EndTrs(.t.)
                      ah_ErrorMsg("Impossibile rinominare %1",,"",alltrim(this.oParentObject.w_CODATT2))
                    endif
                    bTrsErr=bTrsErr or bErr_04676738
                    * --- End
                  endif
                endif
              endif
            else
              ah_ErrorMsg("Impossibile rinominare un capo progetto",,"")
            endif
            this.w_CODCONTO = this.oParentObject.w_CODCONTO2
            this.w_CODCOM = this.oParentObject.w_CODCOM2
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_TREEV2.ExpandAll(.T.)     
          case Azione="Elimina"
            * --- --Elimina legame
            this.w_OKDEL = .t.
            * --- begin transaction
            cp_BeginTrs()
            GSPC_BG3 (this, this.oParentObject.w_CODCOM2, this.oParentObject.w_TIPATT2, this.oParentObject.w_TIPSTR, this.oParentObject.w_CODATT2)
            SELECT AttivitaDaCanc
             
 GO TOP 
 SCAN
            this.w_TIPO = TIPO
            this.w_NODO = CODICE
            if this.oParentObject.w_CODCOM2<>this.w_NODO
              if this.w_TIPO="A"
                this.Pag14()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if this.w_OKATT
                  * --- Try
                  local bErr_046FDFE8
                  bErr_046FDFE8=bTrsErr
                  this.Try_046FDFE8()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    this.w_OKDEL = .f.
                    this.w_MESS = "Impossibile cancellare attivit� %1 dall'anagrafica."
                  endif
                  bTrsErr=bTrsErr or bErr_046FDFE8
                  * --- End
                else
                  this.w_OKDEL = .f.
                endif
              else
                * --- Try
                local bErr_04701B88
                bErr_04701B88=bTrsErr
                this.Try_04701B88()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  this.w_OKDEL = .f.
                  this.w_MESS = "Impossibile cancellare attivit� %1 dall'anagrafica."
                endif
                bTrsErr=bTrsErr or bErr_04701B88
                * --- End
              endif
            else
              * --- Se sono sul nodo commessa allora cancello preventivamente tutti i records di ATT_PREC
              * --- Delete from ATT_PREC
              i_nConn=i_TableProp[this.ATT_PREC_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"MPCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
                       )
              else
                delete from (i_cTable) where;
                      MPCODCOM = this.oParentObject.w_CODCOM2;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
            endif
            ENDSCAN
            if this.w_OKDEL
              * --- commit
              cp_EndTrs(.t.)
            else
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg(this.w_MESS,48,"",ALLTRIM(this.w_NODO))
            endif
            this.w_CODCONTO = this.oParentObject.w_CODCONTO2
            this.w_CODCOM = this.oParentObject.w_CODCOM2
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_TREEV2.ExpandAll(.T.)     
          case Azione="Gestione Nodo"
            if EMPTY(NVL(this.oParentObject.w_TIPCOM2,""))
              * --- Read from ATTIVITA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ATTIVITA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ATTIPCOM"+;
                  " from "+i_cTable+" ATTIVITA where ";
                      +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
                      +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT2);
                      +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT2);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ATTIPCOM;
                  from (i_cTable) where;
                      ATCODCOM = this.oParentObject.w_CODCOM2;
                      and ATTIPATT = this.oParentObject.w_TIPATT2;
                      and ATCODATT = this.oParentObject.w_CODATT2;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_TIPCOM2 = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if g_APPLICATION<>"ADHOC REVOLUTION"
              do case
                case this.oParentObject.w_TIPCOM2 $ "CP"
                  this.w_GestioneNodo = GSPC_ACM(this.oParentObject.w_TIPSTR)
                case this.oParentObject.w_TIPCOM2 = "S"
                  this.w_GestioneNodo = GSPC_ASC(this.oParentObject.w_TIPSTR)
                case this.oParentObject.w_TIPCOM2 ="A"
                  this.w_GestioneNodo = GSPC_AAT(this.oParentObject.w_TIPSTR)
              endcase
            else
              do case
                case this.oParentObject.w_TIPCOM2 $ "CP"
                  this.w_GestioneNodo = GSPC_ACM()
                case this.oParentObject.w_TIPCOM2 = "S"
                  this.w_GestioneNodo = GSPC_ASC()
                case this.oParentObject.w_TIPCOM2 ="A"
                  this.w_GestioneNodo = GSPC_AAT()
              endcase
            endif
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_GestioneNodo.bSec1)
              i_retcode = 'stop'
              return
            endif
            this.w_GestioneNodo.ecpFilter()     
            this.w_GestioneNodo.w_ATTIPATT = this.oParentObject.w_TIPATT2
            this.w_GestioneNodo.w_ATCODATT = this.oParentObject.w_CODATT2
            this.w_GestioneNodo.w_ATCODCOM = this.oParentObject.w_CODCOM2
            this.w_GestioneNodo.ecpSave()     
          case Azione="Gestione Legami"
            if this.oParentObject.w_TIPATT2 <> "A"
              * --- Lancio la gestione delle relazioni
              if g_APPLICATION<>"ADHOC REVOLUTION"
                this.w_GestioneLegami = GSPC_MST(this.oParentObject.w_TIPSTR)
              else
                this.w_GestioneLegami = GSPC_MST()
              endif
              * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
              if !(this.w_GestioneLegami.bSec1)
                i_retcode = 'stop'
                return
              endif
              * --- --Interrogazione
              this.w_GestioneLegami.EcpFilter()     
              * --- inizializzo la chiave delle relazioni
              this.w_GestioneLegami.w_STCODCOM = this.oParentObject.w_CODCOM2
              this.w_GestioneLegami.w_STTIPSTR = this.oParentObject.w_TIPATT2
              this.w_GestioneLegami.w_STATTPAD = this.oParentObject.w_CODATT2
              * --- mi metto in interrogazione
              this.w_GestioneLegami.EcpSave()     
            else
              ah_ErrorMsg("Ai nodi di tipo attivit� non possono essere legati elementi","!","")
            endif
          case Azione= "Tutta la Commessa"
            this.Pag13()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
      case inlist(right(alltrim(this.pTipop), 5), "Menu3", "Menu4")
        Private Azione, pTv 
 store space(10) to Azione
        pTv = left(alltrim(this.pTipop), 3)
        this.Pag15()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        do case
          case UPPER(pTV) = "TV1"
            do case
              case Azione = "ExpAll"
                this.oParentObject.w_TREEV1.ExpandAll(.t.)     
              case Azione = "CollAll"
                this.oParentObject.w_TREEV1.ExpandAll(.f.)     
            endcase
          case UPPER(pTV) = "TV2"
            do case
              case Azione = "ExpAll"
                this.oParentObject.w_TREEV2.ExpandAll(.t.)     
              case Azione = "CollAll"
                this.oParentObject.w_TREEV2.ExpandAll(.f.)     
            endcase
        endcase
      case this.pTipop="Done"
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc
  proc Try_034519B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- --Nodo e legame principale
    this.w_AttivitaFiglio = this.oParentObject.w_CODATT1
    this.w_TipoAttivitaFiglio = this.oParentObject.w_TIPATT1
    * --- Try
    local bErr_0344C258
    bErr_0344C258=bTrsErr
    this.Try_0344C258()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0344C258
    * --- End
    if this.w_TipoAttivitaFiglio="A"
      * --- Write into ATTIVITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_StatoAttivita),'ATTIVITA','AT_STATO');
            +i_ccchkf ;
        +" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio );
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
               )
      else
        update (i_cTable) set;
            AT_STATO = this.oParentObject.w_StatoAttivita;
            &i_ccchkf. ;
         where;
            ATCODCOM = this.w_CommessaDiDestinazione;
            and ATCODATT = this.w_AttivitaFiglio ;
            and ATTIPATT = this.w_TipoAttivitaFiglio;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Try
    local bErr_0344C108
    bErr_0344C108=bTrsErr
    this.Try_0344C108()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0344C108
    * --- End
    * --- Select from MAFCOSTI
    i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2],.t.,this.MAFCOSTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MAFCOSTI ";
          +" where CFCODCOM="+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and CFCODATT="+cp_ToStrODBC(this.w_AttivitaFiglio)+" and CFTIPSTR="+cp_ToStrODBC(this.w_TipoAttivitaFiglio)+"";
           ,"_Curs_MAFCOSTI")
    else
      select Count(*) As Conta from (i_cTable);
       where CFCODCOM=this.w_CommessaDiDestinazione and CFCODATT=this.w_AttivitaFiglio and CFTIPSTR=this.w_TipoAttivitaFiglio;
        into cursor _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      select _Curs_MAFCOSTI
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_MAFCOSTI.Conta , 0)
        select _Curs_MAFCOSTI
        continue
      enddo
      use
    endif
    if this.w_CONTA=0
      * --- Write into ATTIVITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATFLPREV ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
            +i_ccchkf ;
        +" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
               )
      else
        update (i_cTable) set;
            ATFLPREV = "N";
            &i_ccchkf. ;
         where;
            ATCODCOM = this.w_CommessaDiDestinazione;
            and ATTIPATT = this.w_TipoAttivitaFiglio;
            and ATCODATT = this.w_AttivitaFiglio;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_CONTA = 0
    * --- Try
    local bErr_0344E388
    bErr_0344E388=bTrsErr
    this.Try_0344E388()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0344E388
    * --- End
    * --- Try
    local bErr_0344FC18
    bErr_0344FC18=bTrsErr
    this.Try_0344FC18()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0344FC18
    * --- End
    * --- --Esegue copia
    * --- --Inserisce tutti i conti
    Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
    if RECNO() > 1
      * --- --Il primo record � gi� inserito
      this.w_AttivitaFiglio = ATCODATT
      this.w_TipoAttivitaFiglio = ATTIPATT
      * --- Try
      local bErr_034504E8
      bErr_034504E8=bTrsErr
      this.Try_034504E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_034504E8
      * --- End
      if this.w_TipoAttivitaFiglio="A"
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_StatoAttivita),'ATTIVITA','AT_STATO');
              +i_ccchkf ;
          +" where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
              +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio );
              +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                 )
        else
          update (i_cTable) set;
              AT_STATO = this.oParentObject.w_StatoAttivita;
              &i_ccchkf. ;
           where;
              ATCODCOM = this.w_CommessaDiDestinazione;
              and ATCODATT = this.w_AttivitaFiglio ;
              and ATTIPATT = this.w_TipoAttivitaFiglio;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Try
      local bErr_03451BF8
      bErr_03451BF8=bTrsErr
      this.Try_03451BF8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03451BF8
      * --- End
      * --- Select from MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2],.t.,this.MAFCOSTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MAFCOSTI ";
            +" where CFCODCOM="+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and CFCODATT="+cp_ToStrODBC(this.w_AttivitaFiglio)+" and CFTIPSTR="+cp_ToStrODBC(this.w_TipoAttivitaFiglio)+"";
             ,"_Curs_MAFCOSTI")
      else
        select Count(*) As Conta from (i_cTable);
         where CFCODCOM=this.w_CommessaDiDestinazione and CFCODATT=this.w_AttivitaFiglio and CFTIPSTR=this.w_TipoAttivitaFiglio;
          into cursor _Curs_MAFCOSTI
      endif
      if used('_Curs_MAFCOSTI')
        select _Curs_MAFCOSTI
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_MAFCOSTI.Conta , 0)
          select _Curs_MAFCOSTI
          continue
        enddo
        use
      endif
      if this.w_CONTA=0
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATFLPREV ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
              +i_ccchkf ;
          +" where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
              +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
              +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
                 )
        else
          update (i_cTable) set;
              ATFLPREV = "N";
              &i_ccchkf. ;
           where;
              ATCODCOM = this.w_CommessaDiDestinazione;
              and ATTIPATT = this.w_TipoAttivitaFiglio;
              and ATCODATT = this.w_AttivitaFiglio;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      this.w_CONTA = 0
      * --- Try
      local bErr_03452438
      bErr_03452438=bTrsErr
      this.Try_03452438()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03452438
      * --- End
    endif
    ENDSCAN
    * --- --Inserisce i legami
    Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
    if RECNO() > 1
      * --- --Il primo record � gi� inserito
      this.w_AttivitaFiglio = ATCODATT
      this.w_TipoAttivitaFiglio = ATTIPATT
      * --- Try
      local bErr_034525E8
      bErr_034525E8=bTrsErr
      this.Try_034525E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_034525E8
      * --- End
    endif
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04688B68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- --Nodo e legame principale
    this.w_AttivitaFiglio = this.oParentObject.w_CODATT1
    this.w_TipoAttivitaFiglio = this.oParentObject.w_TIPATT1
    * --- Try
    local bErr_0469DEA8
    bErr_0469DEA8=bTrsErr
    this.Try_0469DEA8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0469DEA8
    * --- End
    if this.w_TipoAttivitaFiglio="A"
      * --- Write into ATTIVITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_StatoAttivita),'ATTIVITA','AT_STATO');
            +i_ccchkf ;
        +" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio );
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
               )
      else
        update (i_cTable) set;
            AT_STATO = this.oParentObject.w_StatoAttivita;
            &i_ccchkf. ;
         where;
            ATCODCOM = this.w_CommessaDiDestinazione;
            and ATCODATT = this.w_AttivitaFiglio ;
            and ATTIPATT = this.w_TipoAttivitaFiglio;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Try
    local bErr_0469DF38
    bErr_0469DF38=bTrsErr
    this.Try_0469DF38()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0469DF38
    * --- End
    * --- Select from MAFCOSTI
    i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2],.t.,this.MAFCOSTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MAFCOSTI ";
          +" where CFCODCOM="+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and CFCODATT="+cp_ToStrODBC(this.w_AttivitaFiglio)+" and CFTIPSTR="+cp_ToStrODBC(this.w_TipoAttivitaFiglio)+"";
           ,"_Curs_MAFCOSTI")
    else
      select Count(*) As Conta from (i_cTable);
       where CFCODCOM=this.w_CommessaDiDestinazione and CFCODATT=this.w_AttivitaFiglio and CFTIPSTR=this.w_TipoAttivitaFiglio;
        into cursor _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      select _Curs_MAFCOSTI
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_MAFCOSTI.Conta , 0)
        select _Curs_MAFCOSTI
        continue
      enddo
      use
    endif
    if this.w_CONTA=0
      * --- Write into ATTIVITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATFLPREV ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
            +i_ccchkf ;
        +" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
               )
      else
        update (i_cTable) set;
            ATFLPREV = "N";
            &i_ccchkf. ;
         where;
            ATCODCOM = this.w_CommessaDiDestinazione;
            and ATTIPATT = this.w_TipoAttivitaFiglio;
            and ATCODATT = this.w_AttivitaFiglio;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_CONTA = 0
    * --- Try
    local bErr_046B0690
    bErr_046B0690=bTrsErr
    this.Try_046B0690()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_046B0690
    * --- End
    * --- Try
    local bErr_046B0D80
    bErr_046B0D80=bTrsErr
    this.Try_046B0D80()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_046B0D80
    * --- End
    * --- --Esegue copia
    if USED(this.oParentObject.w_RamoDaCopiare) and this.w_TipoAttivitaFiglio<>"A"
      * --- --Inserisce tutti i conti
      Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
      if RECNO() > 1
        * --- --Il primo record � gi� inserito
        this.w_AttivitaFiglio = ATCODATT
        this.w_TipoAttivitaFiglio = ATTIPATT
        * --- Try
        local bErr_04693258
        bErr_04693258=bTrsErr
        this.Try_04693258()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04693258
        * --- End
        if this.w_TipoAttivitaFiglio="A"
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_StatoAttivita),'ATTIVITA','AT_STATO');
                +i_ccchkf ;
            +" where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio );
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                   )
          else
            update (i_cTable) set;
                AT_STATO = this.oParentObject.w_StatoAttivita;
                &i_ccchkf. ;
             where;
                ATCODCOM = this.w_CommessaDiDestinazione;
                and ATCODATT = this.w_AttivitaFiglio ;
                and ATTIPATT = this.w_TipoAttivitaFiglio;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Try
        local bErr_046941E8
        bErr_046941E8=bTrsErr
        this.Try_046941E8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_046941E8
        * --- End
        * --- Select from MAFCOSTI
        i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2],.t.,this.MAFCOSTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MAFCOSTI ";
              +" where CFCODCOM="+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and CFCODATT="+cp_ToStrODBC(this.w_AttivitaFiglio)+" and CFTIPSTR="+cp_ToStrODBC(this.w_TipoAttivitaFiglio)+"";
               ,"_Curs_MAFCOSTI")
        else
          select Count(*) As Conta from (i_cTable);
           where CFCODCOM=this.w_CommessaDiDestinazione and CFCODATT=this.w_AttivitaFiglio and CFTIPSTR=this.w_TipoAttivitaFiglio;
            into cursor _Curs_MAFCOSTI
        endif
        if used('_Curs_MAFCOSTI')
          select _Curs_MAFCOSTI
          locate for 1=1
          do while not(eof())
          this.w_CONTA = Nvl ( _Curs_MAFCOSTI.Conta , 0)
            select _Curs_MAFCOSTI
            continue
          enddo
          use
        endif
        if this.w_CONTA=0
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATFLPREV ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
                +i_ccchkf ;
            +" where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
                   )
          else
            update (i_cTable) set;
                ATFLPREV = "N";
                &i_ccchkf. ;
             where;
                ATCODCOM = this.w_CommessaDiDestinazione;
                and ATTIPATT = this.w_TipoAttivitaFiglio;
                and ATCODATT = this.w_AttivitaFiglio;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_CONTA = 0
        * --- Try
        local bErr_04695C88
        bErr_04695C88=bTrsErr
        this.Try_04695C88()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04695C88
        * --- End
      endif
      ENDSCAN
      * --- --Inserisce i legami
      Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
      if RECNO() > 1
        * --- --Il primo record � gi� inserito
        this.w_AttivitaFiglio = ATCODATT
        this.w_TipoAttivitaFiglio = ATTIPATT
        * --- Try
        local bErr_04690408
        bErr_04690408=bTrsErr
        this.Try_04690408()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04690408
        * --- End
      endif
      ENDSCAN
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_046D84E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG9",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into ATT_PREC
    i_nConn=i_TableProp[this.ATT_PREC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC2BG9",this.ATT_PREC_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC3BG9",this.MA_COSTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC5BG9",this.STRUTTUR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Delete from STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"STCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and STATTFIG = "+cp_ToStrODBC(this.oParentObject.w_CODATT2);
            +" and STTIPFIG = "+cp_ToStrODBC(this.oParentObject.w_TIPATT2);
             )
    else
      delete from (i_cTable) where;
            STCODCOM = this.oParentObject.w_CODCOM2;
            and STATTFIG = this.oParentObject.w_CODATT2;
            and STTIPFIG = this.oParentObject.w_TIPATT2;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and CSTIPSTR = "+cp_ToStrODBC(this.w_TIPO);
            +" and CSCODMAT = "+cp_ToStrODBC(this.w_NODO);
             )
    else
      delete from (i_cTable) where;
            CSCODCOM = this.oParentObject.w_CODCOM2;
            and CSTIPSTR = this.w_TIPO;
            and CSCODMAT = this.w_NODO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ATT_PREC
    i_nConn=i_TableProp[this.ATT_PREC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MPCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and MPTIPATT = "+cp_ToStrODBC(this.w_TIPO);
            +" and MPCODATT = "+cp_ToStrODBC(this.w_NODO);
             )
    else
      delete from (i_cTable) where;
            MPCODCOM = this.oParentObject.w_CODCOM2;
            and MPTIPATT = this.w_TIPO;
            and MPCODATT = this.w_NODO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and ATTIPATT = "+cp_ToStrODBC("A");
            +" and ATCODATT = "+cp_ToStrODBC(this.w_NODO);
             )
    else
      delete from (i_cTable) where;
            ATCODCOM = this.oParentObject.w_CODCOM2;
            and ATTIPATT = "A";
            and ATCODATT = this.w_NODO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04676738()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG9",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into MAFCOSTI
    i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC1BG9",this.MAFCOSTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC4BG9",this.STRUTTUR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Delete from STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"STCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and STTIPSTR = "+cp_ToStrODBC(this.oParentObject.w_TIPSTR);
            +" and STATTPAD = "+cp_ToStrODBC(this.oParentObject.w_CODATT2);
             )
    else
      delete from (i_cTable) where;
            STCODCOM = this.oParentObject.w_CODCOM2;
            and STTIPSTR = this.oParentObject.w_TIPSTR;
            and STATTPAD = this.oParentObject.w_CODATT2;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"STCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and STTIPSTR = "+cp_ToStrODBC(this.oParentObject.w_TIPSTR);
            +" and STATTFIG = "+cp_ToStrODBC(this.oParentObject.w_CODATT2);
            +" and STTIPFIG = "+cp_ToStrODBC(this.oParentObject.w_TIPATT2);
             )
    else
      delete from (i_cTable) where;
            STCODCOM = this.oParentObject.w_CODCOM2;
            and STTIPSTR = this.oParentObject.w_TIPSTR;
            and STATTFIG = this.oParentObject.w_CODATT2;
            and STTIPFIG = this.oParentObject.w_TIPATT2;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from MAFCOSTI
    i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CFCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and CFTIPSTR = "+cp_ToStrODBC(this.oParentObject.w_TIPATT2);
            +" and CFCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT2);
             )
    else
      delete from (i_cTable) where;
            CFCODCOM = this.oParentObject.w_CODCOM2;
            and CFTIPSTR = this.oParentObject.w_TIPATT2;
            and CFCODATT = this.oParentObject.w_CODATT2;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT2);
            +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPSTR);
             )
    else
      delete from (i_cTable) where;
            ATCODCOM = this.oParentObject.w_CODCOM2;
            and ATCODATT = this.oParentObject.w_CODATT2;
            and ATTIPATT = this.oParentObject.w_TIPSTR;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_046FDFE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and CSTIPSTR = "+cp_ToStrODBC(this.w_TIPO);
            +" and CSCODMAT = "+cp_ToStrODBC(this.w_NODO);
             )
    else
      delete from (i_cTable) where;
            CSCODCOM = this.oParentObject.w_CODCOM2;
            and CSTIPSTR = this.w_TIPO;
            and CSCODMAT = this.w_NODO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ATT_PREC
    i_nConn=i_TableProp[this.ATT_PREC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MPCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and MPTIPATT = "+cp_ToStrODBC(this.w_TIPO);
            +" and MPCODATT = "+cp_ToStrODBC(this.w_NODO);
             )
    else
      delete from (i_cTable) where;
            MPCODCOM = this.oParentObject.w_CODCOM2;
            and MPTIPATT = this.w_TIPO;
            and MPCODATT = this.w_NODO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPO);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_NODO);
             )
    else
      delete from (i_cTable) where;
            ATCODCOM = this.oParentObject.w_CODCOM2;
            and ATTIPATT = this.w_TIPO;
            and ATCODATT = this.w_NODO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04701B88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from MAFCOSTI
    i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CFCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and CFTIPSTR = "+cp_ToStrODBC(this.w_TIPO);
            +" and CFCODATT = "+cp_ToStrODBC(this.w_NODO);
             )
    else
      delete from (i_cTable) where;
            CFCODCOM = this.oParentObject.w_CODCOM2;
            and CFTIPSTR = this.w_TIPO;
            and CFCODATT = this.w_NODO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPO);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_NODO);
             )
    else
      delete from (i_cTable) where;
            ATCODCOM = this.oParentObject.w_CODCOM2;
            and ATTIPATT = this.w_TIPO;
            and ATCODATT = this.w_NODO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0344C258()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG2",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0344C108()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaPreventiviAForfait="S"
      * --- Insert into MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG5",this.MAFCOSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_0344E388()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaAttivitaPrecedenti = "S" and this.oParentObject.w_TIPCOM1 = "P"
      * --- Insert into ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG6",this.ATT_PREC_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_0344FC18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STTIPFIG"+",STATTFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CommessaDiDestinazione),'STRUTTUR','STCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoAttivitaPadre),'STRUTTUR','STTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaPadre),'STRUTTUR','STATTPAD');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(30)),'STRUTTUR','STDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoAttivitaFiglio),'STRUTTUR','STTIPFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFiglio),'STRUTTUR','STATTFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ProssimoCproword),'STRUTTUR','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MillesimiResidui),'STRUTTUR','STMILLES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.w_CommessaDiDestinazione,'STTIPSTR',this.w_TipoAttivitaPadre,'STATTPAD',this.w_AttivitaPadre,'STDESCRI',SPACE(30),'STTIPFIG',this.w_TipoAttivitaFiglio,'STATTFIG',this.w_AttivitaFiglio,'CPROWORD',this.w_ProssimoCproword,'STMILLES',this.w_MillesimiResidui)
      insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STTIPFIG,STATTFIG,CPROWORD,STMILLES &i_ccchkf. );
         values (;
           this.w_CommessaDiDestinazione;
           ,this.w_TipoAttivitaPadre;
           ,this.w_AttivitaPadre;
           ,SPACE(30);
           ,this.w_TipoAttivitaFiglio;
           ,this.w_AttivitaFiglio;
           ,this.w_ProssimoCproword;
           ,this.w_MillesimiResidui;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_034504E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG2",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03451BF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaPreventiviAForfait="S"
      * --- Insert into MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG5",this.MAFCOSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_03452438()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaAttivitaPrecedenti = "S" and this.oParentObject.w_TIPCOM1 = "P"
      * --- Insert into ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG6",this.ATT_PREC_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_034525E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC1BG2",this.STRUTTUR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0469DEA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG2",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0469DF38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaPreventiviAForfait="S"
      * --- Insert into MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG5",this.MAFCOSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_046B0690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaAttivitaPrecedenti = "S" and this.oParentObject.w_TIPCOM1 = "P"
      * --- Insert into ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG6",this.ATT_PREC_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_046B0D80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STTIPFIG"+",STATTFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CommessaDiDestinazione),'STRUTTUR','STCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoAttivitaPadre),'STRUTTUR','STTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaPadre),'STRUTTUR','STATTPAD');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(30)),'STRUTTUR','STDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoAttivitaFiglio),'STRUTTUR','STTIPFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFiglio),'STRUTTUR','STATTFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ProssimoCproword),'STRUTTUR','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MillesimiResidui),'STRUTTUR','STMILLES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.w_CommessaDiDestinazione,'STTIPSTR',this.w_TipoAttivitaPadre,'STATTPAD',this.w_AttivitaPadre,'STDESCRI',SPACE(30),'STTIPFIG',this.w_TipoAttivitaFiglio,'STATTFIG',this.w_AttivitaFiglio,'CPROWORD',this.w_ProssimoCproword,'STMILLES',this.w_MillesimiResidui)
      insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STTIPFIG,STATTFIG,CPROWORD,STMILLES &i_ccchkf. );
         values (;
           this.w_CommessaDiDestinazione;
           ,this.w_TipoAttivitaPadre;
           ,this.w_AttivitaPadre;
           ,SPACE(30);
           ,this.w_TipoAttivitaFiglio;
           ,this.w_AttivitaFiglio;
           ,this.w_ProssimoCproword;
           ,this.w_MillesimiResidui;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04693258()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG2",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_046941E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaPreventiviAForfait="S"
      * --- Insert into MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG5",this.MAFCOSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_04695C88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaAttivitaPrecedenti = "S" and this.oParentObject.w_TIPCOM1 = "P"
      * --- Insert into ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG6",this.ATT_PREC_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_04690408()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC1BG2",this.STRUTTUR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --TREEV1
    * --- Aggiorna la Treeview
    if EMPTY(this.oParentObject.w_CURSORNA1)
      this.oParentObject.w_CURSORNA1 = SYS(2015)
    endif
    * --- Definizione parametri da passare alla CP_EXPDB
    this.w_COUTCURS = this.oParentObject.w_CURSORNA1
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = "CPROWORD"
    * --- Altri campi (ordinamento)
    if Empty ( this.w_CODCONTO )
      * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
      VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
    else
      * --- Recupero solo la parte di struttura identificata dal conto
      VQ_EXEC("..\COMM\EXE\QUERY\GSPC0BSC.VQR",this,"query")
    endif
    SELECT "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if USED("__tmp__")
        SELECT "__tmp__" 
 USE
      endif
    else
      * --- Se la prima commessa specificata non ha elementi
      if Not Used ( this.oParentObject.w_CURSORNA1 )
        i_retcode = 'stop'
        return
      endif
      Select ( this.oParentObject.w_CURSORNA1 )
      Zap
    endif
    * --- Riempio la Treeview
    this.oParentObject.w_TREEV1.cCursor = this.oParentObject.w_CURSORNA1
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --TREEV2
    * --- Aggiorna la Treeview
    if EMPTY(this.oParentObject.w_CURSORNA2)
      this.oParentObject.w_CURSORNA2 = SYS(2015)
    endif
    * --- Definizione parametri da passare alla CP_EXPDB
    this.w_COUTCURS = this.oParentObject.w_CURSORNA2
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = "CPROWORD"
    * --- Altri campi (ordinamento)
    if Empty ( this.w_CODCONTO )
      * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
      VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
    else
      * --- Recupero solo la parte di struttura identificata dal conto
      VQ_EXEC("..\COMM\EXE\QUERY\GSPC0BSC.VQR",this,"query")
    endif
    SELECT "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if USED("__tmp__")
        SELECT "__tmp__" 
 USE
      endif
    else
      * --- Se la prima commessa specificata non ha elementi
      if Not Used ( this.oParentObject.w_CURSORNA2 )
        i_retcode = 'stop'
        return
      endif
      Select ( this.oParentObject.w_CURSORNA2 )
      Zap
    endif
    * --- Riempio la Treeview
    this.oParentObject.w_TREEV2.cCursor = this.oParentObject.w_CURSORNA2
    if this.pTipop<>"Reload2"
      this.w_CODCONTO = this.oParentObject.w_CODCONTO1
      this.w_CODCOM = this.oParentObject.w_CODCOM1
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.notifyevent("Esegui2")
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if EMPTY(this.oParentObject.w_RamoDaCopiare)
      this.oParentObject.w_RamoDaCopiare = SYS(2015)
    endif
    if this.oParentObject.w_TIPATT1<>"A"
      * --- Definizione parametri da passare alla CP_EXPDB
      this.w_COUTCURS = this.oParentObject.w_RamoDaCopiare
      * --- Cursore utilizzato dalla Tree View
      this.w_INPCURS = "QUERY"
      * --- Cursore di partenza
      this.w_CRIFTABLE = "ATTIVITA"
      * --- Tabella di riferimento
      this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
      * --- Chiave anagrafica componenti
      this.w_CEXPTABLE = "STRUTTUR"
      * --- Tabella di esplosione
      this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
      * --- Chiave della movimentazione contenente la struttura
      this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
      * --- Chiave ripetuta nella movimentazione contenente la struttura
      this.w_EXPFIELD = ""
      * --- Campi per espolosione
      this.w_OTHERFLD = "CPROWORD"
      * --- Altri campi (ordinamento)
      if Empty ( this.w_CODCONTO )
        * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
        VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
      else
        * --- Recupero solo la parte di struttura identificata dal conto
        VQ_EXEC("..\COMM\EXE\QUERY\GSPC0BSC.VQR",this,"query")
      endif
      SELECT "QUERY"
      if RECCOUNT()>0
        * --- Costruisco il cursore dei dati della Tree View
        PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
        if USED("__tmp__")
          SELECT "__tmp__" 
 USE
        endif
      else
        * --- Se la prima commessa specificata non ha elementi
        ah_ErrorMsg("La commessa non ha nessun elemento","!","")
      endif
    else
      VQ_EXEC("..\COMM\EXE\QUERY\GSPC_BG8.VQR",this,this.oParentObject.w_RamoDaCopiare)
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Controlli prima di eseguire la copia
    * --- --Legame con il nodo destinazione
    this.w_AttivitaPadre = this.oParentObject.w_CODATT2
    this.w_TipoAttivitaPadre = this.oParentObject.w_TIPATT2
    this.w_AttivitaFiglio = this.oParentObject.w_CODATT1
    this.w_TipoAttivitaFiglio = this.oParentObject.w_TIPATT1
    this.w_Trovato = ""
    * --- --
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- --Controlla se il nodo di partenza da copiare � gi� inserito nella struttura di destinazione
    * --- Read from STRUTTUR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STATTFIG"+;
        " from "+i_cTable+" STRUTTUR where ";
            +"STCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
            +" and STATTFIG = "+cp_ToStrODBC(this.w_AttivitaFiglio);
            +" and STTIPFIG = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
            +" and STTIPSTR = "+cp_ToStrODBC(this.oParentObject.w_TIPSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STATTFIG;
        from (i_cTable) where;
            STCODCOM = this.w_CommessaDiDestinazione;
            and STATTFIG = this.w_AttivitaFiglio;
            and STTIPFIG = this.w_TipoAttivitaFiglio;
            and STTIPSTR = this.oParentObject.w_TIPSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_Trovato = NVL(cp_ToDate(_read_.STATTFIG),cp_NullValue(_read_.STATTFIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if !EMPTY(NVL(this.w_Trovato,""))
      this.w_Messaggio = "L'elemento %1 � gi� presente nella struttura di destinazione%0per proseguire occorre rimuovere tale legame%0si desidera procedere con l'eliminazione del legame?"
      if ah_YesNo(this.w_Messaggio,"",ALLTRIM(this.w_AttivitaFiglio))
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- --Assegnamenti
        this.w_CommessaDiPartenza = this.oParentObject.w_CODCOM1
        this.w_CommessaDiDestinazione = this.oParentObject.w_CODCOM2
        this.w_OK = .T.
        if EMPTY(NVL(this.oParentObject.w_TIPCOM1,""))
          * --- Read from ATTIVITA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ATTIPCOM"+;
              " from "+i_cTable+" ATTIVITA where ";
                  +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM1);
                  +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT1);
                  +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ATTIPCOM;
              from (i_cTable) where;
                  ATCODCOM = this.oParentObject.w_CODCOM1;
                  and ATTIPATT = this.oParentObject.w_TIPATT1;
                  and ATCODATT = this.oParentObject.w_CODATT1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_TIPCOM1 = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if this.oParentObject.w_TIPCOM1 = "P"
          this.Pag13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        * --- --Eplode ramo da copiare nel cursore avente nome in w_RamoDaCopiare
        this.w_CODCONTO = this.oParentObject.w_CODATT1
        this.w_CODCOM = this.oParentObject.w_CODCOM1
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- --Esegue controlli preventivi
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- --Esegue la copia
        * --- Try
        local bErr_04829D50
        bErr_04829D50=bTrsErr
        this.Try_04829D50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          L_PuntatoreFile = fCreate("LogCopiaStruttura.TXT")
          fWrite(L_PuntatoreFile,Message())
          fClose(L_PuntatoreFile)
          !NOTEPAD "LogCopiaStruttura.TXT"
        endif
        bTrsErr=bTrsErr or bErr_04829D50
        * --- End
        * --- --Copia Schede tecniche
        if this.oParentObject.w_CopiaSchedeTecniche = "S"
          this.Pag11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- --Aggiorna TreeVeiw
        this.w_CODCONTO = this.oParentObject.w_CODCONTO2
        this.w_CODCOM = this.oParentObject.w_CODCOM2
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_TREEV2.ExpandAll(.T.)     
      endif
      this.oParentObject.w_CODATT1 = ""
      i_retcode = 'stop'
      return
    endif
    * --- --Calcola il CPROWORD e i millesimi del primo elemento da copiare
    this.w_MillesimiResidui = 1000
    this.w_ProssimoCproword = 0
    * --- Select from STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" STRUTTUR ";
          +" where STCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and STATTPAD = "+cp_ToStrODBC(this.w_AttivitaPadre)+" and STTIPSTR = "+cp_ToStrODBC(this.w_TipoAttivitaPadre)+"";
          +" order by CPROWORD";
           ,"_Curs_STRUTTUR")
    else
      select * from (i_cTable);
       where STCODCOM = this.w_CommessaDiDestinazione and STATTPAD = this.w_AttivitaPadre and STTIPSTR = this.w_TipoAttivitaPadre;
       order by CPROWORD;
        into cursor _Curs_STRUTTUR
    endif
    if used('_Curs_STRUTTUR')
      select _Curs_STRUTTUR
      locate for 1=1
      do while not(eof())
      this.w_MillesimiResidui = this.w_MillesimiResidui - _Curs_STRUTTUR.STMILLES
      this.w_ProssimoCproword = _Curs_STRUTTUR.CPROWORD + 10
        select _Curs_STRUTTUR
        continue
      enddo
      use
    endif
    if this.w_ProssimoCproword=0
      this.w_ProssimoCproword = 10
    endif
    * --- --Controlla se tutti gli elementi sono inseriti nella struttura
    if this.oParentObject.w_TIPATT1 <> "A"
      if USED(this.oParentObject.w_RamodaCopiare)
        Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
        * --- --Ogni elemento, se inserito nella struttura di destinazione, potrebbe
        *     essere presente sia come padre che come figlio.
        this.w_AttivitaFiglio = ATCODATT
        this.w_TipoAttivitaFiglio = ATTIPATT
        * --- Il capoprogetto non � inserito nella struttura ma va comunque verificato
        * --- Read from ATTIVITA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATCODATT"+;
            " from "+i_cTable+" ATTIVITA where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
                +" and ATTIPCOM = "+cp_ToStrODBC("P");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATCODATT;
            from (i_cTable) where;
                ATCODCOM = this.w_CommessaDiDestinazione;
                and ATTIPATT = this.w_TipoAttivitaFiglio;
                and ATCODATT = this.w_AttivitaFiglio;
                and ATTIPCOM = "P";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TROVATO = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !EMPTY(NVL(this.w_Trovato,""))
          this.w_Messaggio = "Uno degli elementi selezionati � uguale al capoprogetto di destinazione%0Impossibile proseguire con la copia progetto."
          ah_ErrorMsg (this.w_Messaggio,48)
          this.w_OK = .F.
        endif
        * --- --Controlla se ogni nodo da copiare � gi� inserito come figlio nella struttura di destinazione
        * --- Read from STRUTTUR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STRUTTUR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "STATTFIG"+;
            " from "+i_cTable+" STRUTTUR where ";
                +"STCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
                +" and STATTFIG = "+cp_ToStrODBC(this.w_AttivitaFiglio);
                +" and STTIPFIG = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                +" and STTIPSTR = "+cp_ToStrODBC(this.oParentObject.w_TIPSTR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            STATTFIG;
            from (i_cTable) where;
                STCODCOM = this.w_CommessaDiDestinazione;
                and STATTFIG = this.w_AttivitaFiglio;
                and STTIPFIG = this.w_TipoAttivitaFiglio;
                and STTIPSTR = this.oParentObject.w_TIPSTR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_Trovato = NVL(cp_ToDate(_read_.STATTFIG),cp_NullValue(_read_.STATTFIG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !EMPTY(NVL(this.w_Trovato,"")) AND this.w_OK
          this.w_Messaggio = "L'elemento %1 � gi� presente nella struttura di destinazione%0per proseguire occorre rimuovere tale legame%0si desidera procedere con l'eliminazione del legame?"
          if ah_YesNo(this.w_Messaggio,"",ALLTRIM(this.w_AttivitaFiglio))
            this.Pag6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            Select (this.oParentObject.w_RamoDaCopiare)
            * --- --Assegnamenti
            this.w_CommessaDiPartenza = this.oParentObject.w_CODCOM1
            this.w_CommessaDiDestinazione = this.oParentObject.w_CODCOM2
            this.w_OK = .T.
            * --- --Se la commessa di origine � capoprogetto, occorre copiare utilizzando l'apposita voce di menu.
            if EMPTY(NVL(this.oParentObject.w_TIPCOM1,""))
              * --- Read from ATTIVITA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ATTIVITA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ATTIPCOM"+;
                  " from "+i_cTable+" ATTIVITA where ";
                      +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM1);
                      +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT1);
                      +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT1);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ATTIPCOM;
                  from (i_cTable) where;
                      ATCODCOM = this.oParentObject.w_CODCOM1;
                      and ATTIPATT = this.oParentObject.w_TIPATT1;
                      and ATCODATT = this.oParentObject.w_CODATT1;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_TIPCOM1 = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if this.oParentObject.w_TIPCOM1 = "P"
              this.Pag13()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
            * --- --Eplode ramo da copiare nel cursore avente nome in w_RamoDaCopiare
            this.w_CODCONTO = this.oParentObject.w_CODATT1
            this.w_CODCOM = this.oParentObject.w_CODCOM1
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- --Esegue controlli preventivi
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- --Esegue la copia
            * --- Try
            local bErr_048005C0
            bErr_048005C0=bTrsErr
            this.Try_048005C0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              L_PuntatoreFile = fCreate("LogCopiaStruttura.TXT")
              fWrite(L_PuntatoreFile,Message())
              fClose(L_PuntatoreFile)
              !NOTEPAD "LogCopiaStruttura.TXT"
            endif
            bTrsErr=bTrsErr or bErr_048005C0
            * --- End
            * --- --Copia Schede tecniche
            if this.oParentObject.w_CopiaSchedeTecniche = "S"
              this.Pag11()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- --Aggiorna TreeVeiw
            this.w_CODCONTO = this.oParentObject.w_CODCONTO2
            this.w_CODCOM = this.oParentObject.w_CODCOM2
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_TREEV2.ExpandAll(.T.)     
          endif
          this.w_OK = .F.
        endif
        ENDSCAN
      endif
    endif
    if !this.w_OK
      this.oParentObject.w_CODATT1 = ""
      this.w_CODCONTO = this.oParentObject.w_CODCONTO2
      this.w_CODCOM = this.oParentObject.w_CODCOM2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_TREEV2.ExpandAll(.T.)     
      i_retcode = 'stop'
      return
    endif
  endproc
  proc Try_04829D50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- --Nodo e legame principale
    this.w_AttivitaFiglio = this.oParentObject.w_CODATT1
    this.w_TipoAttivitaFiglio = this.oParentObject.w_TIPATT1
    * --- Try
    local bErr_04864548
    bErr_04864548=bTrsErr
    this.Try_04864548()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04864548
    * --- End
    if this.w_TipoAttivitaFiglio="A"
      * --- Write into ATTIVITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_StatoAttivita),'ATTIVITA','AT_STATO');
            +i_ccchkf ;
        +" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio );
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
               )
      else
        update (i_cTable) set;
            AT_STATO = this.oParentObject.w_StatoAttivita;
            &i_ccchkf. ;
         where;
            ATCODCOM = this.w_CommessaDiDestinazione;
            and ATCODATT = this.w_AttivitaFiglio ;
            and ATTIPATT = this.w_TipoAttivitaFiglio;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Try
    local bErr_04865028
    bErr_04865028=bTrsErr
    this.Try_04865028()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04865028
    * --- End
    * --- Select from MAFCOSTI
    i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2],.t.,this.MAFCOSTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MAFCOSTI ";
          +" where CFCODCOM="+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and CFCODATT="+cp_ToStrODBC(this.w_AttivitaFiglio)+" and CFTIPSTR="+cp_ToStrODBC(this.w_TipoAttivitaFiglio)+"";
           ,"_Curs_MAFCOSTI")
    else
      select Count(*) As Conta from (i_cTable);
       where CFCODCOM=this.w_CommessaDiDestinazione and CFCODATT=this.w_AttivitaFiglio and CFTIPSTR=this.w_TipoAttivitaFiglio;
        into cursor _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      select _Curs_MAFCOSTI
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_MAFCOSTI.Conta , 0)
        select _Curs_MAFCOSTI
        continue
      enddo
      use
    endif
    if this.w_CONTA=0
      * --- Write into ATTIVITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATFLPREV ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
            +i_ccchkf ;
        +" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
               )
      else
        update (i_cTable) set;
            ATFLPREV = "N";
            &i_ccchkf. ;
         where;
            ATCODCOM = this.w_CommessaDiDestinazione;
            and ATTIPATT = this.w_TipoAttivitaFiglio;
            and ATCODATT = this.w_AttivitaFiglio;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_CONTA = 0
    * --- Try
    local bErr_04866EB8
    bErr_04866EB8=bTrsErr
    this.Try_04866EB8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04866EB8
    * --- End
    * --- Try
    local bErr_04867488
    bErr_04867488=bTrsErr
    this.Try_04867488()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04867488
    * --- End
    * --- --INSERIMENTO IN STRTTUR-1
    * --- --Esegue copia
    if USED(this.oParentObject.w_RamoDaCopiare) AND this.oParentObject.w_TIPATT1 <> "A"
      * --- --Inserisce tutti i conti
      Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
      if RECNO() > 1
        * --- --Il primo record � gi� inserito
        this.w_AttivitaFiglio = ATCODATT
        this.w_TipoAttivitaFiglio = ATTIPATT
        * --- Try
        local bErr_048482F8
        bErr_048482F8=bTrsErr
        this.Try_048482F8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_048482F8
        * --- End
        if this.w_TipoAttivitaFiglio="A"
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_StatoAttivita),'ATTIVITA','AT_STATO');
                +i_ccchkf ;
            +" where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio );
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                   )
          else
            update (i_cTable) set;
                AT_STATO = this.oParentObject.w_StatoAttivita;
                &i_ccchkf. ;
             where;
                ATCODCOM = this.w_CommessaDiDestinazione;
                and ATCODATT = this.w_AttivitaFiglio ;
                and ATTIPATT = this.w_TipoAttivitaFiglio;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Try
        local bErr_04848FB8
        bErr_04848FB8=bTrsErr
        this.Try_04848FB8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04848FB8
        * --- End
        * --- Select from MAFCOSTI
        i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2],.t.,this.MAFCOSTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MAFCOSTI ";
              +" where CFCODCOM="+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and CFCODATT="+cp_ToStrODBC(this.w_AttivitaFiglio)+" and CFTIPSTR="+cp_ToStrODBC(this.w_TipoAttivitaFiglio)+"";
               ,"_Curs_MAFCOSTI")
        else
          select Count(*) As Conta from (i_cTable);
           where CFCODCOM=this.w_CommessaDiDestinazione and CFCODATT=this.w_AttivitaFiglio and CFTIPSTR=this.w_TipoAttivitaFiglio;
            into cursor _Curs_MAFCOSTI
        endif
        if used('_Curs_MAFCOSTI')
          select _Curs_MAFCOSTI
          locate for 1=1
          do while not(eof())
          this.w_CONTA = Nvl ( _Curs_MAFCOSTI.Conta , 0)
            select _Curs_MAFCOSTI
            continue
          enddo
          use
        endif
        if this.w_CONTA=0
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATFLPREV ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
                +i_ccchkf ;
            +" where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
                   )
          else
            update (i_cTable) set;
                ATFLPREV = "N";
                &i_ccchkf. ;
             where;
                ATCODCOM = this.w_CommessaDiDestinazione;
                and ATTIPATT = this.w_TipoAttivitaFiglio;
                and ATCODATT = this.w_AttivitaFiglio;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_CONTA = 0
        * --- Try
        local bErr_0484A9F8
        bErr_0484A9F8=bTrsErr
        this.Try_0484A9F8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0484A9F8
        * --- End
      endif
      ENDSCAN
      * --- --Inserisce i legami
      Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
      if RECNO() > 1
        * --- --Il primo record � gi� inserito
        this.w_AttivitaFiglio = ATCODATT
        this.w_TipoAttivitaFiglio = ATTIPATT
        * --- Try
        local bErr_04845298
        bErr_04845298=bTrsErr
        this.Try_04845298()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04845298
        * --- End
      endif
      ENDSCAN
    endif
    return
  proc Try_048005C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- --Nodo e legame principale
    this.w_AttivitaFiglio = this.oParentObject.w_CODATT1
    this.w_TipoAttivitaFiglio = this.oParentObject.w_TIPATT1
    * --- Try
    local bErr_04817C08
    bErr_04817C08=bTrsErr
    this.Try_04817C08()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04817C08
    * --- End
    if this.w_TipoAttivitaFiglio="A"
      * --- Write into ATTIVITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_StatoAttivita),'ATTIVITA','AT_STATO');
            +i_ccchkf ;
        +" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio );
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
               )
      else
        update (i_cTable) set;
            AT_STATO = this.oParentObject.w_StatoAttivita;
            &i_ccchkf. ;
         where;
            ATCODCOM = this.w_CommessaDiDestinazione;
            and ATCODATT = this.w_AttivitaFiglio ;
            and ATTIPATT = this.w_TipoAttivitaFiglio;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Try
    local bErr_048183E8
    bErr_048183E8=bTrsErr
    this.Try_048183E8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_048183E8
    * --- End
    * --- Select from MAFCOSTI
    i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2],.t.,this.MAFCOSTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MAFCOSTI ";
          +" where CFCODCOM="+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and CFCODATT="+cp_ToStrODBC(this.w_AttivitaFiglio)+" and CFTIPSTR="+cp_ToStrODBC(this.w_TipoAttivitaFiglio)+"";
           ,"_Curs_MAFCOSTI")
    else
      select Count(*) As Conta from (i_cTable);
       where CFCODCOM=this.w_CommessaDiDestinazione and CFCODATT=this.w_AttivitaFiglio and CFTIPSTR=this.w_TipoAttivitaFiglio;
        into cursor _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      select _Curs_MAFCOSTI
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_MAFCOSTI.Conta , 0)
        select _Curs_MAFCOSTI
        continue
      enddo
      use
    endif
    if this.w_CONTA=0
      * --- Write into ATTIVITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATFLPREV ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
            +i_ccchkf ;
        +" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
            +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
            +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
               )
      else
        update (i_cTable) set;
            ATFLPREV = "N";
            &i_ccchkf. ;
         where;
            ATCODCOM = this.w_CommessaDiDestinazione;
            and ATTIPATT = this.w_TipoAttivitaFiglio;
            and ATCODATT = this.w_AttivitaFiglio;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_CONTA = 0
    * --- Try
    local bErr_0480A4F8
    bErr_0480A4F8=bTrsErr
    this.Try_0480A4F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0480A4F8
    * --- End
    if this.oParentObject.w_TIPSTR <> "P"
    endif
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STTIPFIG"+",STATTFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CommessaDiDestinazione),'STRUTTUR','STCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoAttivitaPadre),'STRUTTUR','STTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaPadre),'STRUTTUR','STATTPAD');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(30)),'STRUTTUR','STDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoAttivitaFiglio),'STRUTTUR','STTIPFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFiglio),'STRUTTUR','STATTFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ProssimoCproword),'STRUTTUR','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MillesimiResidui),'STRUTTUR','STMILLES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.w_CommessaDiDestinazione,'STTIPSTR',this.w_TipoAttivitaPadre,'STATTPAD',this.w_AttivitaPadre,'STDESCRI',SPACE(30),'STTIPFIG',this.w_TipoAttivitaFiglio,'STATTFIG',this.w_AttivitaFiglio,'CPROWORD',this.w_ProssimoCproword,'STMILLES',this.w_MillesimiResidui)
      insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STTIPFIG,STATTFIG,CPROWORD,STMILLES &i_ccchkf. );
         values (;
           this.w_CommessaDiDestinazione;
           ,this.w_TipoAttivitaPadre;
           ,this.w_AttivitaPadre;
           ,SPACE(30);
           ,this.w_TipoAttivitaFiglio;
           ,this.w_AttivitaFiglio;
           ,this.w_ProssimoCproword;
           ,this.w_MillesimiResidui;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- --Esegue copia
    if USED(this.oParentObject.w_RamoDaCopiare)
      * --- --Inserisce tutti i conti
      Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
      if RECNO() > 1
        * --- --Il primo record � gi� inserito
        this.w_AttivitaFiglio = ATCODATT
        this.w_TipoAttivitaFiglio = ATTIPATT
        * --- Try
        local bErr_047FC030
        bErr_047FC030=bTrsErr
        this.Try_047FC030()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_047FC030
        * --- End
        if this.w_TipoAttivitaFiglio="A"
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_StatoAttivita),'ATTIVITA','AT_STATO');
                +i_ccchkf ;
            +" where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio );
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                   )
          else
            update (i_cTable) set;
                AT_STATO = this.oParentObject.w_StatoAttivita;
                &i_ccchkf. ;
             where;
                ATCODCOM = this.w_CommessaDiDestinazione;
                and ATCODATT = this.w_AttivitaFiglio ;
                and ATTIPATT = this.w_TipoAttivitaFiglio;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Try
        local bErr_047FC5A0
        bErr_047FC5A0=bTrsErr
        this.Try_047FC5A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_047FC5A0
        * --- End
        * --- Select from MAFCOSTI
        i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2],.t.,this.MAFCOSTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MAFCOSTI ";
              +" where CFCODCOM="+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and CFCODATT="+cp_ToStrODBC(this.w_AttivitaFiglio)+" and CFTIPSTR="+cp_ToStrODBC(this.w_TipoAttivitaFiglio)+"";
               ,"_Curs_MAFCOSTI")
        else
          select Count(*) As Conta from (i_cTable);
           where CFCODCOM=this.w_CommessaDiDestinazione and CFCODATT=this.w_AttivitaFiglio and CFTIPSTR=this.w_TipoAttivitaFiglio;
            into cursor _Curs_MAFCOSTI
        endif
        if used('_Curs_MAFCOSTI')
          select _Curs_MAFCOSTI
          locate for 1=1
          do while not(eof())
          this.w_CONTA = Nvl ( _Curs_MAFCOSTI.Conta , 0)
            select _Curs_MAFCOSTI
            continue
          enddo
          use
        endif
        if this.w_CONTA=0
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATFLPREV ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
                +i_ccchkf ;
            +" where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
                   )
          else
            update (i_cTable) set;
                ATFLPREV = "N";
                &i_ccchkf. ;
             where;
                ATCODCOM = this.w_CommessaDiDestinazione;
                and ATTIPATT = this.w_TipoAttivitaFiglio;
                and ATCODATT = this.w_AttivitaFiglio;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_CONTA = 0
        * --- Try
        local bErr_047FE5B0
        bErr_047FE5B0=bTrsErr
        this.Try_047FE5B0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_047FE5B0
        * --- End
      endif
      ENDSCAN
      * --- --Inserisce i legami
      Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
      if RECNO() > 1
        * --- --Il primo record � gi� inserito
        this.w_AttivitaFiglio = ATCODATT
        this.w_TipoAttivitaFiglio = ATTIPATT
        * --- Try
        local bErr_047F8E50
        bErr_047F8E50=bTrsErr
        this.Try_047F8E50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_047F8E50
        * --- End
      endif
      ENDSCAN
    endif
    return
  proc Try_04864548()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG2",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04865028()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaPreventiviAForfait="S"
      * --- Insert into MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG5",this.MAFCOSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_04866EB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaAttivitaPrecedenti = "S" and this.oParentObject.w_TIPCOM1 = "P"
      * --- Insert into ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG6",this.ATT_PREC_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_04867488()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STTIPFIG"+",STATTFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CommessaDiDestinazione),'STRUTTUR','STCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoAttivitaPadre),'STRUTTUR','STTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaPadre),'STRUTTUR','STATTPAD');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(30)),'STRUTTUR','STDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TipoAttivitaFiglio),'STRUTTUR','STTIPFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFiglio),'STRUTTUR','STATTFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ProssimoCproword),'STRUTTUR','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MillesimiResidui),'STRUTTUR','STMILLES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.w_CommessaDiDestinazione,'STTIPSTR',this.w_TipoAttivitaPadre,'STATTPAD',this.w_AttivitaPadre,'STDESCRI',SPACE(30),'STTIPFIG',this.w_TipoAttivitaFiglio,'STATTFIG',this.w_AttivitaFiglio,'CPROWORD',this.w_ProssimoCproword,'STMILLES',this.w_MillesimiResidui)
      insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STTIPFIG,STATTFIG,CPROWORD,STMILLES &i_ccchkf. );
         values (;
           this.w_CommessaDiDestinazione;
           ,this.w_TipoAttivitaPadre;
           ,this.w_AttivitaPadre;
           ,SPACE(30);
           ,this.w_TipoAttivitaFiglio;
           ,this.w_AttivitaFiglio;
           ,this.w_ProssimoCproword;
           ,this.w_MillesimiResidui;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_048482F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG2",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04848FB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaPreventiviAForfait="S"
      * --- Insert into MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG5",this.MAFCOSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_0484A9F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaAttivitaPrecedenti = "S" and this.oParentObject.w_TIPCOM1 = "P"
      * --- Insert into ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG6",this.ATT_PREC_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_04845298()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- --INSERIMENTO IN STRTTUR-2
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC1BG2",this.STRUTTUR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04817C08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG2",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_048183E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaPreventiviAForfait="S"
      * --- Insert into MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG5",this.MAFCOSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_0480A4F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaAttivitaPrecedenti = "S" and this.oParentObject.w_TIPCOM1 = "P"
      * --- Insert into ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG6",this.ATT_PREC_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_047FC030()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG2",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_047FC5A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaPreventiviAForfait="S"
      * --- Insert into MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG5",this.MAFCOSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_047FE5B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaAttivitaPrecedenti = "S" and this.oParentObject.w_TIPCOM1 = "P"
      * --- Insert into ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG6",this.ATT_PREC_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_047F8E50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC1BG2",this.STRUTTUR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if .F.
      * --- Lancio la gestione delle relazioni
      this.w_GestioneLegami = GSPC_MST(this.oParentObject.w_TIPSTR)
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_GestioneLegami.bSec1)
        i_retcode = 'stop'
        return
      endif
      * --- --Interrogazione
      this.w_GestioneLegami.EcpFilter()     
      * --- inizializzo la chiave delle relazioni
      this.w_GestioneLegami.w_STCODCOM = this.w_CommessaDiDestinazione
      this.w_GestioneLegami.w_STTIPSTR = this.w_TipoAttivitaPadre
      this.w_GestioneLegami.w_STATTPAD = this.w_AttivitaPadre
      * --- mi metto in interrogazione
      this.w_GestioneLegami.EcpSave()     
      this.w_GestioneLegami.EcpEdit()     
      SELECT (this.w_GestioneLegami.cTrsName) 
 GO TOP 
 LOCATE FOR t_STATTFIG = this.w_AttivitaFiglio
      this.w_GestioneLegami.oPgFrm.Page1.oPag.oBody.Refresh()     
      this.w_GestioneLegami.workFromTrs()     
      this.w_GestioneLegami.SaveDependsOn()     
      this.w_GestioneLegami.SetControlsValue()     
      this.w_GestioneLegami.mHideControls()     
      this.w_GestioneLegami.ChildrenChangeRow()     
      this.w_GestioneLegami.oPgFrm.Page1.oPag.oBody.oBodyCol.SetFocus()     
    endif
    * --- --Elimina legame
    if this.w_TipoAttivitaFiglio="A"
      * --- Delete from STRUTTUR
      i_nConn=i_TableProp[this.STRUTTUR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"STCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
              +" and STTIPFIG = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
              +" and STATTFIG = "+cp_ToStrODBC(this.w_AttivitaFiglio);
              +" and STTIPSTR = "+cp_ToStrODBC(this.oParentObject.w_TIPSTR);
               )
      else
        delete from (i_cTable) where;
              STCODCOM = this.w_CommessaDiDestinazione;
              and STTIPFIG = this.w_TipoAttivitaFiglio;
              and STATTFIG = this.w_AttivitaFiglio;
              and STTIPSTR = this.oParentObject.w_TIPSTR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      SELECT AttivitaDaCanc
      INSERT INTO AttivitaDaCanc (CODICE, TIPO) VALUES (this.w_AttivitaFiglio, this.w_TipoAttivitaFiglio)
      GSPC_BG3 (this, this.w_CommessaDiDestinazione, this.w_TipoAttivitaFiglio, this.oParentObject.w_TIPSTR, this.w_AttivitaFiglio)
    endif
    this.w_CODCONTO = this.oParentObject.w_CODCONTO2
    this.w_CODCOM = this.oParentObject.w_CODCOM2
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Chiusura
    if USED(this.oParentObject.w_RamoDaCopiare)
      SELECT (this.oParentObject.w_RamoDaCopiare) 
 USE
    endif
    if USED("QUERY")
      SELECT ("QUERY") 
 USE
    endif
    if USED(this.oParentObject.w_CURSORNA1)
      SELECT (this.oParentObject.w_CURSORNA1) 
 USE
    endif
    if USED(this.oParentObject.w_CURSORNA2)
      SELECT (this.oParentObject.w_CURSORNA2) 
 USE
    endif
    if USED(this.w_COUTCURS)
      SELECT (this.w_COUTCURS) 
 USE
    endif
    if USED("AttivitaDaCanc")
      SELECT AttivitaDaCanc
      USE
    endif
    USE IN SELECT("ResultSet")
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Menu1
    if EMPTY(NVL(this.oParentObject.w_TIPCOM1,""))
      * --- Read from ATTIVITA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATTIPCOM"+;
          " from "+i_cTable+" ATTIVITA where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM1);
              +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT1);
              +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATTIPCOM;
          from (i_cTable) where;
              ATCODCOM = this.oParentObject.w_CODCOM1;
              and ATTIPATT = this.oParentObject.w_TIPATT1;
              and ATCODATT = this.oParentObject.w_CODATT1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_TIPCOM1 = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    DEFINE POPUP MenuTV from MRow()+1,MCol()+1 shortcut margin
    DEFINE BAR 1 OF MenuTV prompt ah_MsgFormat("Copia (attivit� in stato provvisorio)")
    DEFINE BAR 2 OF MenuTV prompt ah_MsgFormat("Copia (attivit� in stato confermato)")
    DEFINE BAR 3 OF MenuTV prompt ah_MsgFormat("Gestione nodo")
    DEFINE BAR 4 OF MenuTV prompt ah_MsgFormat("Gestione legami")
    ON SELE BAR 1 OF MenuTV Azione="Copia"
    ON SELE BAR 2 OF MenuTV Azione="Copia2"
    ON SELE BAR 3 OF MenuTV Azione="Gestione Nodo"
    ON SELE BAR 4 OF MenuTV Azione="Gestione Legami"
    ACTI POPUP MenuTV
    DEACTIVATE POPUP MenuTV
    RELEASE POPUPS MenuTV EXTENDED
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Menu2
    if EMPTY(NVL(this.oParentObject.w_TIPCOM1,""))
      * --- Read from ATTIVITA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATTIPCOM"+;
          " from "+i_cTable+" ATTIVITA where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM1);
              +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT1);
              +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATTIPCOM;
          from (i_cTable) where;
              ATCODCOM = this.oParentObject.w_CODCOM1;
              and ATTIPATT = this.oParentObject.w_TIPATT1;
              and ATCODATT = this.oParentObject.w_CODATT1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_TIPCOM1 = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    DEFINE POPUP MenuTV from MRow()+1,MCol()+1 shortcut margin
    if EMPTY(NVL(this.oParentObject.w_CODATT1,""))
      DEFINE BAR 1 OF MenuTV prompt ah_MsgFormat("\Incolla")
    else
      DEFINE BAR 1 OF MenuTV prompt ah_MsgFormat("Incolla")
    endif
    DEFINE BAR 2 OF MenuTV prompt ah_MsgFormat("Rinomina")
    DEFINE BAR 3 OF MenuTV prompt ah_MsgFormat("Elimina")
    DEFINE BAR 4 OF MenuTV prompt ah_MsgFormat("Gestione nodo")
    DEFINE BAR 5 OF MenuTV prompt ah_MsgFormat("Gestione legami")
    if this.oParentObject.w_TIPCOM1 = "P"
      ON SELE BAR 1 OF MenuTV Azione="Tutta la Commessa"
    else
      ON SELE BAR 1 OF MenuTV Azione="Incolla"
    endif
    ON SELE BAR 2 OF MenuTV Azione="Rinomina"
    ON SELE BAR 3 OF MenuTV Azione="Elimina"
    ON SELE BAR 4 OF MenuTV Azione="Gestione Nodo"
    ON SELE BAR 5 OF MenuTV Azione="Gestione Legami"
    ACTI POPUP MenuTV
    DEACTIVATE POPUP MenuTV
    RELEASE POPUPS MenuTV EXTENDED
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    if EMPTY(NVL(this.oParentObject.w_TIPCOM1,""))
      * --- Read from ATTIVITA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATTIPCOM"+;
          " from "+i_cTable+" ATTIVITA where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM1);
              +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT1);
              +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATTIPCOM;
          from (i_cTable) where;
              ATCODCOM = this.oParentObject.w_CODCOM1;
              and ATTIPATT = this.oParentObject.w_TIPATT1;
              and ATCODATT = this.oParentObject.w_CODATT1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_TIPCOM1 = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if EMPTY(NVL(this.oParentObject.w_TIPCOM2,""))
      * --- Read from ATTIVITA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATTIPCOM"+;
          " from "+i_cTable+" ATTIVITA where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
              +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_TIPATT2);
              +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT2);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATTIPCOM;
          from (i_cTable) where;
              ATCODCOM = this.oParentObject.w_CODCOM2;
              and ATTIPATT = this.oParentObject.w_TIPATT2;
              and ATCODATT = this.oParentObject.w_CODATT2;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_TIPCOM2 = NVL(cp_ToDate(_read_.ATTIPCOM),cp_NullValue(_read_.ATTIPCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_AttivitaPadre = this.oParentObject.w_CODATT2
    this.w_TipoAttivitaPadre = this.oParentObject.w_TIPATT2
    this.w_AttivitaFiglio = this.oParentObject.w_CODATT1
    this.w_TipoAttivitaFiglio = this.oParentObject.w_TIPATT1
    if this.w_AttivitaFiglio=this.w_AttivitaPadre AND this.w_TipoAttivitaFiglio=this.w_TipoAttivitaPadre
      this.w_Messaggio = "Gli elementi selezionati sono uguali%0non possono essere legati"
      ah_ErrorMsg(this.w_Messaggio,"!","")
      this.w_CODCONTO = this.oParentObject.w_CODCONTO2
      this.w_CODCOM = this.oParentObject.w_CODCOM2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_TREEV2.ExpandAll(.T.)     
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_TIPCOM1 = "P" and .f.
      this.w_Messaggio = "L'elemento %1 � capoprogetto%0non pu� essere agganciato ad altri nodi"
      ah_ErrorMsg(this.w_Messaggio,"!","",ALLTRIM(this.w_AttivitaFiglio))
      this.w_CODCONTO = this.oParentObject.w_CODCONTO2
      this.w_CODCOM = this.oParentObject.w_CODCOM2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_TREEV2.ExpandAll(.T.)     
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_TIPCOM1 = "C" AND this.oParentObject.w_TIPCOM2 $ "S-A"
      this.w_Messaggio = "L'elemento %1 � un conto%0pu� essere legato solo al capoprogetto o ad altri conti"
      ah_ErrorMsg(this.w_Messaggio,"!","",ALLTRIM(this.w_AttivitaFiglio))
      this.w_CODCONTO = this.oParentObject.w_CODCONTO2
      this.w_CODCOM = this.oParentObject.w_CODCOM2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_TREEV2.ExpandAll(.T.)     
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_TIPCOM1 = "S" AND this.oParentObject.w_TIPCOM2 ="A"
      this.w_Messaggio = "L'elemento %1 � un'attivit�%0ad essa non possono essere legati altri elementi"
      ah_ErrorMsg(this.w_Messaggio,"!","",ALLTRIM(this.w_Attivitapadre))
      this.w_CODCONTO = this.oParentObject.w_CODCONTO2
      this.w_CODCOM = this.oParentObject.w_CODCOM2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_TREEV2.ExpandAll(.T.)     
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Create Cursor Stampa_Resoconto (Attivita C(15) , Messaggio C(250)) 
 WrCursor("stampa_Resoconto")
    * --- Data di registrazione = Oggi
    this.w_MADATREG = i_datsys
    this.w_MA__ANNO = ALLTRIM(STR(YEAR(i_DATSYS)))
    * --- Utente
    this.w_MAALFREG = IIF(g_MAGUTE="S", 0, i_CODUTE)
    * --- Tipo Movimento = Preventivo
    this.w_MATIPMOV = "P"
    * --- Inserito e variato dall'utente corrente
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    * --- Legge il cambio della valuta nella variabile w_MACAOVAL
    this.w_NEWCODVAL = g_CODEUR
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_NEWCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL;
        from (i_cTable) where;
            VACODVAL = this.w_NEWCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MACAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- --w_CAOCOM
    * --- Read from CAN_TIER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNCODVAL"+;
        " from "+i_cTable+" CAN_TIER where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNCODVAL;
        from (i_cTable) where;
            CNCODCAN = this.oParentObject.w_CODCOM2;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ValutaCommessaDiDestinazione = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_ValutaCommessaDiDestinazione);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL;
        from (i_cTable) where;
            VACODVAL = this.w_ValutaCommessaDiDestinazione;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAOCOM = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN FOR ATTIPATT="A"
    this.w_ATTIVITA = ATCODATT
    this.w_ExistMovPrevOrig = False
    * --- Select from MAT_MAST
    i_nConn=i_TableProp[this.MAT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" MAT_MAST ";
          +" where MACODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM1)+" and MACODATT="+cp_ToStrODBC(this.w_ATTIVITA)+" and MATIPMOV='P'";
           ,"_Curs_MAT_MAST")
    else
      select * from (i_cTable);
       where MACODCOM=this.oParentObject.w_CODCOM1 and MACODATT=this.w_ATTIVITA and MATIPMOV="P";
        into cursor _Curs_MAT_MAST
    endif
    if used('_Curs_MAT_MAST')
      select _Curs_MAT_MAST
      locate for 1=1
      do while not(eof())
      this.w_ExistMovPrevOrig = True
      this.w_MASERIAL = _Curs_MAT_MAST.MASERIAL
      this.w_MACODVAL = _Curs_MAT_MAST.MACODVAL
      * --- Assegnamento eseguito come in GSPC_MAT
      this.w_MACAOVAL = IIF(this.w_MACAOVAL=0,GETCAM(this.w_MACODVAL, this.w_MADATREG, 0),this.w_MACAOVAL)
      if this.w_MACODVAL <> this.w_NEWCODVAL
        this.w_MACODLIS = SPACE(5)
      else
        this.w_MACODLIS = _Curs_MAT_MAST.MACODLIS
      endif
      * --- Contolla se esiste gi� un movimento preventivo
      vq_exec("..\COMM\EXE\QUERY\GSPC_BG4.VQR",this,"ResultSet")
      select Quanti from ResultSet into array MovimentiPreventivi
      this.CopiaMovimento = (MovimentiPreventivi = 0)
      if this.CopiaMovimento
        * --- Try
        local bErr_048FC040
        bErr_048FC040=bTrsErr
        this.Try_048FC040()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_048FC040
        * --- End
      else
        this.w_Messaggio = "Esiste gi� un movimento preventivo per l'attivit� %1%0si desidera che sia mantenuto il movimento gi� inserito?"
        this.w_NonSovrascrivere = ah_YesNo(this.w_Messaggio,"",alltrim(this.w_ATTIVITA))
        if ! this.w_NonSovrascrivere
          * --- Try
          local bErr_048F2650
          bErr_048F2650=bTrsErr
          this.Try_048F2650()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_048F2650
          * --- End
        else
          INSERT INTO Stampa_Resoconto(Attivita,Messaggio)VALUES(this.w_ATTIVITA,ah_MsgFormat("Il movimento preventivo dell'attivit� %1, gi� esistente, non � stato sosituito",alltrim(this.w_ATTIVITA)))
        endif
      endif
        select _Curs_MAT_MAST
        continue
      enddo
      use
    endif
    if Not this.w_ExistMovPrevOrig
      * --- Contolla se esiste gi� un movimento preventivo
      vq_exec("..\COMM\EXE\QUERY\GSPC_BG4.VQR",this,"ResultSet")
      select Quanti from ResultSet into array MovimentiPreventivi
      this.w_EliminaMovPrev = (MovimentiPreventivi > 0)
      if this.w_EliminaMovPrev
        this.w_Messaggio = "Esiste un movimento preventivo per l'attivit� %1%0si desidera che venga mantenuto?"
        if !ah_YesNo(this.w_Messaggio,"",alltrim(this.w_ATTIVITA))
          * --- Try
          local bErr_048ECEE8
          bErr_048ECEE8=bTrsErr
          this.Try_048ECEE8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_048ECEE8
          * --- End
        endif
      endif
    endif
    Select (this.oParentObject.w_RamoDaCopiare)
    ENDSCAN
    if USED("Stampa_Resoconto")
      SELECT ("Stampa_Resoconto") 
 USE
    endif
  endproc
  proc Try_048FC040()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_NEWSERIAL = cp_GetProg("MAT_MAST","SEMAT",this.w_NEWSERIAL,i_codazi)
    this.w_MANUMREG = cp_GetProg("MAT_MAST","NUMAT",this.w_MANUMREG,i_codazi,this.w_MA__ANNO,this.w_MAALFREG)
    if this.w_MACODVAL <> this.w_NEWCODVAL
      * --- Se � cambiata la valuta del movimento, non viene copiato il listino
      * --- Insert into MAT_MAST
      i_nConn=i_TableProp[this.MAT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MASERIAL"+",MACODCOM"+",MACODATT"+",MANUMREG"+",MAALFREG"+",MADATREG"+",MA__ANNO"+",MATIPMOV"+",MACODVAL"+",MACAOVAL"+",UTCV"+",UTCC"+",UTDV"+",UTDC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'MAT_MAST','MASERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM2),'MAT_MAST','MACODCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ATTIVITA),'MAT_MAST','MACODATT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MANUMREG),'MAT_MAST','MANUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAALFREG),'MAT_MAST','MAALFREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MADATREG),'MAT_MAST','MADATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MA__ANNO),'MAT_MAST','MA__ANNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPMOV),'MAT_MAST','MATIPMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NEWCODVAL),'MAT_MAST','MACODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MACAOVAL),'MAT_MAST','MACAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'MAT_MAST','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'MAT_MAST','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'MAT_MAST','UTDV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'MAT_MAST','UTDC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_NEWSERIAL,'MACODCOM',this.oParentObject.w_CODCOM2,'MACODATT',this.w_ATTIVITA,'MANUMREG',this.w_MANUMREG,'MAALFREG',this.w_MAALFREG,'MADATREG',this.w_MADATREG,'MA__ANNO',this.w_MA__ANNO,'MATIPMOV',this.w_MATIPMOV,'MACODVAL',this.w_NEWCODVAL,'MACAOVAL',this.w_MACAOVAL,'UTCV',this.w_UTCV,'UTCC',this.w_UTCC)
        insert into (i_cTable) (MASERIAL,MACODCOM,MACODATT,MANUMREG,MAALFREG,MADATREG,MA__ANNO,MATIPMOV,MACODVAL,MACAOVAL,UTCV,UTCC,UTDV,UTDC &i_ccchkf. );
           values (;
             this.w_NEWSERIAL;
             ,this.oParentObject.w_CODCOM2;
             ,this.w_ATTIVITA;
             ,this.w_MANUMREG;
             ,this.w_MAALFREG;
             ,this.w_MADATREG;
             ,this.w_MA__ANNO;
             ,this.w_MATIPMOV;
             ,this.w_NEWCODVAL;
             ,this.w_MACAOVAL;
             ,this.w_UTCV;
             ,this.w_UTCC;
             ,this.w_UTDV;
             ,this.w_UTDC;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Altrimenti viene copiato anche il listino
      * --- Insert into MAT_MAST
      i_nConn=i_TableProp[this.MAT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MASERIAL"+",MACODCOM"+",MACODATT"+",MANUMREG"+",MAALFREG"+",MADATREG"+",MA__ANNO"+",MATIPMOV"+",MACODVAL"+",MACAOVAL"+",UTCV"+",UTCC"+",UTDV"+",UTDC"+",MACODLIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'MAT_MAST','MASERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM2),'MAT_MAST','MACODCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ATTIVITA),'MAT_MAST','MACODATT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MANUMREG),'MAT_MAST','MANUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAALFREG),'MAT_MAST','MAALFREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MADATREG),'MAT_MAST','MADATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MA__ANNO),'MAT_MAST','MA__ANNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPMOV),'MAT_MAST','MATIPMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NEWCODVAL),'MAT_MAST','MACODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MACAOVAL),'MAT_MAST','MACAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'MAT_MAST','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'MAT_MAST','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'MAT_MAST','UTDV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'MAT_MAST','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MACODLIS),'MAT_MAST','MACODLIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_NEWSERIAL,'MACODCOM',this.oParentObject.w_CODCOM2,'MACODATT',this.w_ATTIVITA,'MANUMREG',this.w_MANUMREG,'MAALFREG',this.w_MAALFREG,'MADATREG',this.w_MADATREG,'MA__ANNO',this.w_MA__ANNO,'MATIPMOV',this.w_MATIPMOV,'MACODVAL',this.w_NEWCODVAL,'MACAOVAL',this.w_MACAOVAL,'UTCV',this.w_UTCV,'UTCC',this.w_UTCC)
        insert into (i_cTable) (MASERIAL,MACODCOM,MACODATT,MANUMREG,MAALFREG,MADATREG,MA__ANNO,MATIPMOV,MACODVAL,MACAOVAL,UTCV,UTCC,UTDV,UTDC,MACODLIS &i_ccchkf. );
           values (;
             this.w_NEWSERIAL;
             ,this.oParentObject.w_CODCOM2;
             ,this.w_ATTIVITA;
             ,this.w_MANUMREG;
             ,this.w_MAALFREG;
             ,this.w_MADATREG;
             ,this.w_MA__ANNO;
             ,this.w_MATIPMOV;
             ,this.w_NEWCODVAL;
             ,this.w_MACAOVAL;
             ,this.w_UTCV;
             ,this.w_UTCC;
             ,this.w_UTDV;
             ,this.w_UTDC;
             ,this.w_MACODLIS;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    this.Pag12()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_048F2650()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Legge il seriale della scheda tecnica nella variabile w_MASERIAL
    * --- Read from MAT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MASERIAL"+;
        " from "+i_cTable+" MAT_MAST where ";
            +"MACODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and MACODATT = "+cp_ToStrODBC(this.w_ATTIVITA);
            +" and MATIPMOV = "+cp_ToStrODBC("P");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MASERIAL;
        from (i_cTable) where;
            MACODCOM = this.oParentObject.w_CODCOM2;
            and MACODATT = this.w_ATTIVITA;
            and MATIPMOV = "P";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NEWSERIAL = NVL(cp_ToDate(_read_.MASERIAL),cp_NullValue(_read_.MASERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_MACODVAL <> this.w_NEWCODVAL
      * --- Aggiorna valuta e cambio
      * --- Write into MAT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MAT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MAT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MACODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCODVAL),'MAT_MAST','MACODVAL');
        +",MACAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MACAOVAL),'MAT_MAST','MACAOVAL');
            +i_ccchkf ;
        +" where ";
            +"MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL);
               )
      else
        update (i_cTable) set;
            MACODVAL = this.w_NEWCODVAL;
            ,MACAOVAL = this.w_MACAOVAL;
            &i_ccchkf. ;
         where;
            MASERIAL = this.w_NEWSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Aggiorna valuta, cambio e listino
      * --- Write into MAT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MAT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MAT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MACODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCODVAL),'MAT_MAST','MACODVAL');
        +",MACAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MACAOVAL),'MAT_MAST','MACAOVAL');
        +",MACODLIS ="+cp_NullLink(cp_ToStrODBC(this.w_MACODLIS),'MAT_MAST','MACODLIS');
            +i_ccchkf ;
        +" where ";
            +"MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL);
               )
      else
        update (i_cTable) set;
            MACODVAL = this.w_NEWCODVAL;
            ,MACAOVAL = this.w_MACAOVAL;
            ,MACODLIS = this.w_MACODLIS;
            &i_ccchkf. ;
         where;
            MASERIAL = this.w_NEWSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Cancella il dettaglio della scheda tecnica di destinazione
    * --- Select from MAT_DETT
    i_nConn=i_TableProp[this.MAT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2],.t.,this.MAT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" MAT_DETT ";
          +" where MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL)+"";
           ,"_Curs_MAT_DETT")
    else
      select * from (i_cTable);
       where MASERIAL = this.w_NEWSERIAL;
        into cursor _Curs_MAT_DETT
    endif
    if used('_Curs_MAT_DETT')
      select _Curs_MAT_DETT
      locate for 1=1
      do while not(eof())
      this.w_CPROWNUM = _Curs_MAT_DETT.CPROWNUM
      this.w_MACOCOMS = _Curs_MAT_DETT.MACOCOMS
      this.w_MACOATTS = _Curs_MAT_DETT.MACOATTS
      this.w_MATIPATT = _Curs_MAT_DETT.MATIPATT
      this.w_MACODCOS = _Curs_MAT_DETT.MACODCOS
      this.w_MAVALRIG = _Curs_MAT_DETT.MAVALRIG
      * --- Try
      local bErr_0493E630
      bErr_0493E630=bTrsErr
      this.Try_0493E630()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0493E630
      * --- End
      * --- Delete from MAT_DETT
      i_nConn=i_TableProp[this.MAT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        delete from (i_cTable) where;
              MASERIAL = this.w_NEWSERIAL;
              and CPROWNUM = this.w_CPROWNUM;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
        select _Curs_MAT_DETT
        continue
      enddo
      use
    endif
    this.Pag12()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    INSERT INTO Stampa_Resoconto(Attivita,Messaggio)VALUES(this.w_ATTIVITA,ah_MsgFormat("Il movimento preventivo dell'attivit� %1, gi� esistente, � stato sostituito",alltrim(this.w_ATTIVITA)))
    return
  proc Try_048ECEE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Legge il seriale della scheda tecnica nella variabile w_MASERIAL
    * --- Read from MAT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MASERIAL"+;
        " from "+i_cTable+" MAT_MAST where ";
            +"MACODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2);
            +" and MACODATT = "+cp_ToStrODBC(this.w_ATTIVITA);
            +" and MATIPMOV = "+cp_ToStrODBC("P");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MASERIAL;
        from (i_cTable) where;
            MACODCOM = this.oParentObject.w_CODCOM2;
            and MACODATT = this.w_ATTIVITA;
            and MATIPMOV = "P";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NEWSERIAL = NVL(cp_ToDate(_read_.MASERIAL),cp_NullValue(_read_.MASERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Elimina Movimento Preventivo e Storna i Saldi di Commessa
    * --- Select from MAT_DETT
    i_nConn=i_TableProp[this.MAT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2],.t.,this.MAT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" MAT_DETT ";
          +" where MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL)+"";
           ,"_Curs_MAT_DETT")
    else
      select * from (i_cTable);
       where MASERIAL = this.w_NEWSERIAL;
        into cursor _Curs_MAT_DETT
    endif
    if used('_Curs_MAT_DETT')
      select _Curs_MAT_DETT
      locate for 1=1
      do while not(eof())
      this.w_CPROWNUM = _Curs_MAT_DETT.CPROWNUM
      this.w_MACOCOMS = _Curs_MAT_DETT.MACOCOMS
      this.w_MACOATTS = _Curs_MAT_DETT.MACOATTS
      this.w_MATIPATT = _Curs_MAT_DETT.MATIPATT
      this.w_MACODCOS = _Curs_MAT_DETT.MACODCOS
      this.w_MAVALRIG = _Curs_MAT_DETT.MAVALRIG
      * --- Try
      local bErr_048E3F78
      bErr_048E3F78=bTrsErr
      this.Try_048E3F78()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_048E3F78
      * --- End
      * --- Delete from MAT_DETT
      i_nConn=i_TableProp[this.MAT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        delete from (i_cTable) where;
              MASERIAL = this.w_NEWSERIAL;
              and CPROWNUM = this.w_CPROWNUM;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
        select _Curs_MAT_DETT
        continue
      enddo
      use
    endif
    * --- Delete from MAT_MAST
    i_nConn=i_TableProp[this.MAT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL);
             )
    else
      delete from (i_cTable) where;
            MASERIAL = this.w_NEWSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    INSERT INTO Stampa_Resoconto(Attivita,Messaggio)VALUES(this.w_ATTIVITA,ah_MsgFormat("Il movimento preventivo dell'attivit� %1 � stato eliminato",alltrim(this.w_ATTIVITA)))
    return
  proc Try_0493E630()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MA_COSTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CSPREVEN =CSPREVEN- "+cp_ToStrODBC(this.w_MAVALRIG);
          +i_ccchkf ;
      +" where ";
          +"CSCODCOM = "+cp_ToStrODBC(this.w_MACOCOMS);
          +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MATIPATT);
          +" and CSCODMAT = "+cp_ToStrODBC(this.w_MACOATTS);
          +" and CSCODCOS = "+cp_ToStrODBC(this.w_MACODCOS);
             )
    else
      update (i_cTable) set;
          CSPREVEN = CSPREVEN - this.w_MAVALRIG;
          &i_ccchkf. ;
       where;
          CSCODCOM = this.w_MACOCOMS;
          and CSTIPSTR = this.w_MATIPATT;
          and CSCODMAT = this.w_MACOATTS;
          and CSCODCOS = this.w_MACODCOS;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_048E3F78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MA_COSTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CSPREVEN =CSPREVEN- "+cp_ToStrODBC(this.w_MAVALRIG);
          +i_ccchkf ;
      +" where ";
          +"CSCODCOM = "+cp_ToStrODBC(this.w_MACOCOMS);
          +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MATIPATT);
          +" and CSCODMAT = "+cp_ToStrODBC(this.w_MACOATTS);
          +" and CSCODCOS = "+cp_ToStrODBC(this.w_MACODCOS);
             )
    else
      update (i_cTable) set;
          CSPREVEN = CSPREVEN - this.w_MAVALRIG;
          &i_ccchkf. ;
       where;
          CSCODCOM = this.w_MACOCOMS;
          and CSTIPSTR = this.w_MATIPATT;
          and CSCODMAT = this.w_MACOATTS;
          and CSCODCOS = this.w_MACODCOS;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Select from MAT_DETT
    i_nConn=i_TableProp[this.MAT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2],.t.,this.MAT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" MAT_DETT ";
          +" where MASERIAL="+cp_ToStrODBC(this.w_MASERIAL)+"";
          +" order by CPROWNUM";
           ,"_Curs_MAT_DETT")
    else
      select * from (i_cTable);
       where MASERIAL=this.w_MASERIAL;
       order by CPROWNUM;
        into cursor _Curs_MAT_DETT
    endif
    if used('_Curs_MAT_DETT')
      select _Curs_MAT_DETT
      locate for 1=1
      do while not(eof())
      this.w_CPROWNUM = _Curs_MAT_DETT.CPROWNUM
      this.w_CPROWORD = _Curs_MAT_DETT.CPROWORD
      this.w_MACODKEY = _Curs_MAT_DETT.MACODKEY
      this.w_MACODART = _Curs_MAT_DETT.MACODART
      if g_APPLICATION<>"ADHOC REVOLUTION"
        this.w_MACODVAR = _Curs_MAT_DETT.MACODVAR
      endif
      this.w_MAUNIMIS = _Curs_MAT_DETT.MAUNIMIS
      this.w_MAQTAMOV = _Curs_MAT_DETT.MAQTAMOV
      if this.w_MACODVAL<>this.w_NEWCODVAL
        this.w_MAPREZZO = valcam(_Curs_MAT_DETT.MAPREZZO,this.w_MACODVAL,this.w_NEWCODVAL,i_datsys,0)
      else
        this.w_MAPREZZO = _Curs_MAT_DETT.MAPREZZO
      endif
      this.w_MADATANN = _Curs_MAT_DETT.MADATANN
      this.w_MADESCRI = _Curs_MAT_DETT.MADESCRI
      this.w_MA__NOTE = _Curs_MAT_DETT.MA__NOTE
      this.w_MACODCOS = _Curs_MAT_DETT.MACODCOS
      this.w_MAOPPREV = _Curs_MAT_DETT.MAOPPREV
      this.w_MAOPCONS = _Curs_MAT_DETT.MAOPCONS
      this.w_MAQTA1UM = _Curs_MAT_DETT.MAQTA1UM
      this.w_MACOATTS = _Curs_MAT_DETT.MACOATTS
      this.w_MATIPATT = _Curs_MAT_DETT.MATIPATT
      this.w_MACODFAM = _Curs_MAT_DETT.MACODFAM
      this.w_MACOCOMS = this.oParentObject.w_CODCOM2
      this.w_MAVALRIG = IIF(NOT EMPTY(NVL(this.w_MADATANN,cp_CharToDate("  -  -  "))),0,VAL2MON(this.w_MAPREZZO,this.w_MACAOVAL,this.w_CAOCOM,g_DATEUR)*this.w_MAQTA1UM)
      this.w_MARIFKIT = _Curs_MAT_DETT.MARIFKIT
      * --- Inserisce il movimento di dettaglio con chiave w_NEWSERIAL
      if CP_TODATE(_Curs_MAT_DETT.MADATANN)<i_datsys
        if g_APPLICATION<>"ADHOC REVOLUTION"
          * --- Insert into MAT_DETT
          i_nConn=i_TableProp[this.MAT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MASERIAL"+",CPROWNUM"+",CPROWORD"+",MACODKEY"+",MACODART"+",MACODVAR"+",MAUNIMIS"+",MAQTAMOV"+",MAPREZZO"+",MADATANN"+",MADESCRI"+",MA__NOTE"+",MACODCOS"+",MAOPPREV"+",MAOPCONS"+",MAQTA1UM"+",MAVALRIG"+",MACOATTS"+",MATIPATT"+",MACOCOMS"+",MACODFAM"+",MARIFKIT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'MAT_DETT','MASERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MAT_DETT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MAT_DETT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODKEY),'MAT_DETT','MACODKEY');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODART),'MAT_DETT','MACODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODVAR),'MAT_DETT','MACODVAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAUNIMIS),'MAT_DETT','MAUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTAMOV),'MAT_DETT','MAQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAPREZZO),'MAT_DETT','MAPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MADATANN),'MAT_DETT','MADATANN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MADESCRI),'MAT_DETT','MADESCRI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MA__NOTE),'MAT_DETT','MA__NOTE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODCOS),'MAT_DETT','MACODCOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPPREV),'MAT_DETT','MAOPPREV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPCONS),'MAT_DETT','MAOPCONS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTA1UM),'MAT_DETT','MAQTA1UM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAVALRIG),'MAT_DETT','MAVALRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACOATTS),'MAT_DETT','MACOATTS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPATT),'MAT_DETT','MATIPATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACOCOMS),'MAT_DETT','MACOCOMS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODFAM),'MAT_DETT','MACODFAM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MARIFKIT),'MAT_DETT','MARIFKIT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_NEWSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MACODKEY',this.w_MACODKEY,'MACODART',this.w_MACODART,'MACODVAR',this.w_MACODVAR,'MAUNIMIS',this.w_MAUNIMIS,'MAQTAMOV',this.w_MAQTAMOV,'MAPREZZO',this.w_MAPREZZO,'MADATANN',this.w_MADATANN,'MADESCRI',this.w_MADESCRI,'MA__NOTE',this.w_MA__NOTE)
            insert into (i_cTable) (MASERIAL,CPROWNUM,CPROWORD,MACODKEY,MACODART,MACODVAR,MAUNIMIS,MAQTAMOV,MAPREZZO,MADATANN,MADESCRI,MA__NOTE,MACODCOS,MAOPPREV,MAOPCONS,MAQTA1UM,MAVALRIG,MACOATTS,MATIPATT,MACOCOMS,MACODFAM,MARIFKIT &i_ccchkf. );
               values (;
                 this.w_NEWSERIAL;
                 ,this.w_CPROWNUM;
                 ,this.w_CPROWORD;
                 ,this.w_MACODKEY;
                 ,this.w_MACODART;
                 ,this.w_MACODVAR;
                 ,this.w_MAUNIMIS;
                 ,this.w_MAQTAMOV;
                 ,this.w_MAPREZZO;
                 ,this.w_MADATANN;
                 ,this.w_MADESCRI;
                 ,this.w_MA__NOTE;
                 ,this.w_MACODCOS;
                 ,this.w_MAOPPREV;
                 ,this.w_MAOPCONS;
                 ,this.w_MAQTA1UM;
                 ,this.w_MAVALRIG;
                 ,this.w_MACOATTS;
                 ,this.w_MATIPATT;
                 ,this.w_MACOCOMS;
                 ,this.w_MACODFAM;
                 ,this.w_MARIFKIT;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into MAT_DETT
          i_nConn=i_TableProp[this.MAT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MASERIAL"+",CPROWNUM"+",CPROWORD"+",MACODKEY"+",MACODART"+",MAUNIMIS"+",MAQTAMOV"+",MAPREZZO"+",MADATANN"+",MADESCRI"+",MA__NOTE"+",MACODCOS"+",MAOPPREV"+",MAOPCONS"+",MAQTA1UM"+",MAVALRIG"+",MACOATTS"+",MATIPATT"+",MACOCOMS"+",MACODFAM"+",MARIFKIT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'MAT_DETT','MASERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MAT_DETT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MAT_DETT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODKEY),'MAT_DETT','MACODKEY');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODART),'MAT_DETT','MACODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAUNIMIS),'MAT_DETT','MAUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTAMOV),'MAT_DETT','MAQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAPREZZO),'MAT_DETT','MAPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MADATANN),'MAT_DETT','MADATANN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MADESCRI),'MAT_DETT','MADESCRI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MA__NOTE),'MAT_DETT','MA__NOTE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODCOS),'MAT_DETT','MACODCOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPPREV),'MAT_DETT','MAOPPREV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPCONS),'MAT_DETT','MAOPCONS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTA1UM),'MAT_DETT','MAQTA1UM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAVALRIG),'MAT_DETT','MAVALRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACOATTS),'MAT_DETT','MACOATTS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPATT),'MAT_DETT','MATIPATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACOCOMS),'MAT_DETT','MACOCOMS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODFAM),'MAT_DETT','MACODFAM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MARIFKIT),'MAT_DETT','MARIFKIT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_NEWSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MACODKEY',this.w_MACODKEY,'MACODART',this.w_MACODART,'MAUNIMIS',this.w_MAUNIMIS,'MAQTAMOV',this.w_MAQTAMOV,'MAPREZZO',this.w_MAPREZZO,'MADATANN',this.w_MADATANN,'MADESCRI',this.w_MADESCRI,'MA__NOTE',this.w_MA__NOTE,'MACODCOS',this.w_MACODCOS)
            insert into (i_cTable) (MASERIAL,CPROWNUM,CPROWORD,MACODKEY,MACODART,MAUNIMIS,MAQTAMOV,MAPREZZO,MADATANN,MADESCRI,MA__NOTE,MACODCOS,MAOPPREV,MAOPCONS,MAQTA1UM,MAVALRIG,MACOATTS,MATIPATT,MACOCOMS,MACODFAM,MARIFKIT &i_ccchkf. );
               values (;
                 this.w_NEWSERIAL;
                 ,this.w_CPROWNUM;
                 ,this.w_CPROWORD;
                 ,this.w_MACODKEY;
                 ,this.w_MACODART;
                 ,this.w_MAUNIMIS;
                 ,this.w_MAQTAMOV;
                 ,this.w_MAPREZZO;
                 ,this.w_MADATANN;
                 ,this.w_MADESCRI;
                 ,this.w_MA__NOTE;
                 ,this.w_MACODCOS;
                 ,this.w_MAOPPREV;
                 ,this.w_MAOPCONS;
                 ,this.w_MAQTA1UM;
                 ,this.w_MAVALRIG;
                 ,this.w_MACOATTS;
                 ,this.w_MATIPATT;
                 ,this.w_MACOCOMS;
                 ,this.w_MACODFAM;
                 ,this.w_MARIFKIT;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Try
        local bErr_0496E180
        bErr_0496E180=bTrsErr
        this.Try_0496E180()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0496E180
        * --- End
        * --- Write into MA_COSTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MA_COSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSPREVEN =CSPREVEN+ "+cp_ToStrODBC(this.w_MAVALRIG);
              +i_ccchkf ;
          +" where ";
              +"CSCODCOM = "+cp_ToStrODBC(this.w_MACOCOMS);
              +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MATIPATT);
              +" and CSCODMAT = "+cp_ToStrODBC(this.w_MACOATTS);
              +" and CSCODCOS = "+cp_ToStrODBC(this.w_MACODCOS);
                 )
        else
          update (i_cTable) set;
              CSPREVEN = CSPREVEN + this.w_MAVALRIG;
              &i_ccchkf. ;
           where;
              CSCODCOM = this.w_MACOCOMS;
              and CSTIPSTR = this.w_MATIPATT;
              and CSCODMAT = this.w_MACOATTS;
              and CSCODCOS = this.w_MACODCOS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_MAT_DETT
        continue
      enddo
      use
    endif
  endproc
  proc Try_0496E180()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+",CSPREVEN"+",CSCONSUN"+",CSORDIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MACOCOMS),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPATT),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACOATTS),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACODCOS),'MA_COSTI','CSCODCOS');
      +","+cp_NullLink(cp_ToStrODBC(0),'MA_COSTI','CSPREVEN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MA_COSTI','CSCONSUN');
      +","+cp_NullLink(cp_ToStrODBC(0),'MA_COSTI','CSORDIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.w_MACOCOMS,'CSTIPSTR',this.w_MATIPATT,'CSCODMAT',this.w_MACOATTS,'CSCODCOS',this.w_MACODCOS,'CSPREVEN',0,'CSCONSUN',0,'CSORDIN',0)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS,CSPREVEN,CSCONSUN,CSORDIN &i_ccchkf. );
         values (;
           this.w_MACOCOMS;
           ,this.w_MATIPATT;
           ,this.w_MACOATTS;
           ,this.w_MACODCOS;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CODCONTO = this.oParentObject.w_CODATT1
    this.w_CODCOM = this.oParentObject.w_CODCOM1
    this.oParentObject.w_RamoDaCopiare = this.oParentObject.w_CURSORNA1
    * --- --Esegue controlli preventivi
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- --Esegue la copia
    * --- Try
    local bErr_04979BF0
    bErr_04979BF0=bTrsErr
    this.Try_04979BF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_CODATT1 = ""
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      L_PuntatoreFile = fCreate("LogCopiaStruttura.TXT")
      fWrite(L_PuntatoreFile,Message())
      fClose(L_PuntatoreFile)
      !NOTEPAD "LogCopiaStruttura.TXT"
    endif
    bTrsErr=bTrsErr or bErr_04979BF0
    * --- End
    do case
      case this.w_ERRNO=1
        ah_ErrorMsg("Non � stato possibile tempificare le attivit� della commessa per la presenza%0di un loop nelle relazioni di precedenza nella commessa di origine.",48)
      case this.w_ERRNO=2
        ah_ErrorMsg("Non � stato possibile tempificare le attivit� della commessa.",48)
    endcase
    * --- --Copia Schede tecniche
    if this.oParentObject.w_CopiaSchedeTecniche = "S"
      this.Pag11()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_CODATT1 = ""
    * --- --Aggiorna TreeVeiw
    this.w_CODCONTO = this.oParentObject.w_CODCONTO2
    this.w_CODCOM = this.oParentObject.w_CODCOM2
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_TREEV2.ExpandAll(.T.)     
    * --- --Valorizza eventualmente TIPCOM1 e TIPCOM2
  endproc
  proc Try_04979BF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- --Inserisce tutti i conti
    Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
    if RECNO() > 1
      * --- --Il primo record � gi� inserito
      this.w_AttivitaFiglio = ATCODATT
      this.w_TipoAttivitaFiglio = ATTIPATT
      * --- Try
      local bErr_04992F98
      bErr_04992F98=bTrsErr
      this.Try_04992F98()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04992F98
      * --- End
      if this.w_TipoAttivitaFiglio="A"
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_StatoAttivita),'ATTIVITA','AT_STATO');
              +i_ccchkf ;
          +" where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
              +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio );
              +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
                 )
        else
          update (i_cTable) set;
              AT_STATO = this.oParentObject.w_StatoAttivita;
              &i_ccchkf. ;
           where;
              ATCODCOM = this.w_CommessaDiDestinazione;
              and ATCODATT = this.w_AttivitaFiglio ;
              and ATTIPATT = this.w_TipoAttivitaFiglio;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Try
      local bErr_04993A48
      bErr_04993A48=bTrsErr
      this.Try_04993A48()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04993A48
      * --- End
      * --- Select from MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2],.t.,this.MAFCOSTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MAFCOSTI ";
            +" where CFCODCOM="+cp_ToStrODBC(this.w_CommessaDiDestinazione)+" and CFCODATT="+cp_ToStrODBC(this.w_AttivitaFiglio)+" and CFTIPSTR="+cp_ToStrODBC(this.w_TipoAttivitaFiglio)+"";
             ,"_Curs_MAFCOSTI")
      else
        select Count(*) As Conta from (i_cTable);
         where CFCODCOM=this.w_CommessaDiDestinazione and CFCODATT=this.w_AttivitaFiglio and CFTIPSTR=this.w_TipoAttivitaFiglio;
          into cursor _Curs_MAFCOSTI
      endif
      if used('_Curs_MAFCOSTI')
        select _Curs_MAFCOSTI
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_MAFCOSTI.Conta , 0)
          select _Curs_MAFCOSTI
          continue
        enddo
        use
      endif
      if this.w_CONTA=0
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATFLPREV ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
              +i_ccchkf ;
          +" where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.w_CommessaDiDestinazione);
              +" and ATTIPATT = "+cp_ToStrODBC(this.w_TipoAttivitaFiglio);
              +" and ATCODATT = "+cp_ToStrODBC(this.w_AttivitaFiglio);
                 )
        else
          update (i_cTable) set;
              ATFLPREV = "N";
              &i_ccchkf. ;
           where;
              ATCODCOM = this.w_CommessaDiDestinazione;
              and ATTIPATT = this.w_TipoAttivitaFiglio;
              and ATCODATT = this.w_AttivitaFiglio;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      this.w_CONTA = 0
      * --- Try
      local bErr_04995968
      bErr_04995968=bTrsErr
      this.Try_04995968()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04995968
      * --- End
    endif
    ENDSCAN
    * --- --Inserisce i legami con il nodo principale
    this.w_OKMillesimi = 1
    this.w_ProssimoCproword = 0
    * --- Select from STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" STRUTTUR ";
          +" where STCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND STTIPSTR = "+cp_ToStrODBC(this.oParentObject.w_TIPATT1)+" AND STATTPAD = "+cp_ToStrODBC(this.oParentObject.w_CODATT2)+"";
          +" order by CPROWORD";
           ,"_Curs_STRUTTUR")
    else
      select * from (i_cTable);
       where STCODCOM = this.oParentObject.w_CODCOM2 AND STTIPSTR = this.oParentObject.w_TIPATT1 AND STATTPAD = this.oParentObject.w_CODATT2;
       order by CPROWORD;
        into cursor _Curs_STRUTTUR
    endif
    if used('_Curs_STRUTTUR')
      select _Curs_STRUTTUR
      locate for 1=1
      do while not(eof())
      this.w_ProssimoCproword = CPROWORD
      this.w_OKMillesimi = 0
        select _Curs_STRUTTUR
        continue
      enddo
      use
    endif
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG7",this.STRUTTUR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- --Inserisce i legami
    Select (this.oParentObject.w_RamoDaCopiare) 
 GO TOP 
 SCAN
    if RECNO() > 1
      * --- --Il primo record � gi� inserito
      this.w_AttivitaFiglio = ATCODATT
      this.w_TipoAttivitaFiglio = ATTIPATT
      * --- Try
      local bErr_0498F218
      bErr_0498F218=bTrsErr
      this.Try_0498F218()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0498F218
      * --- End
    endif
    ENDSCAN
    * --- Richiamo il batch per il calcolo delle date Inizio e Fine Attivit� basandomi sulle precedenze....
    this.w_ERRNO = GSPC_BTE(this,this.w_CommessaDiDestinazione,this.oParentObject.w_DATINI,this.oParentObject.w_DATFIN)
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04992F98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG2",this.ATTIVITA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04993A48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaPreventiviAForfait="S"
      * --- Insert into MAFCOSTI
      i_nConn=i_TableProp[this.MAFCOSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAFCOSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG5",this.MAFCOSTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_04995968()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CopiaAttivitaPrecedenti = "S" and this.oParentObject.w_TIPCOM1 = "P"
      * --- Insert into ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC_BG6",this.ATT_PREC_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_0498F218()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC1BG2",this.STRUTTUR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CONTA = 0
    this.w_OKATT = .T.
    * --- Select from ATT_PREC
    i_nConn=i_TableProp[this.ATT_PREC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2],.t.,this.ATT_PREC_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" ATT_PREC ";
          +" where MPCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND MPATTPRE="+cp_ToStrODBC(this.w_NODO)+" AND MPTIPATT="+cp_ToStrODBC(this.w_TIPO)+"";
           ,"_Curs_ATT_PREC")
    else
      select Count(*) As Conta from (i_cTable);
       where MPCODCOM=this.oParentObject.w_CODCOM2 AND MPATTPRE=this.w_NODO AND MPTIPATT=this.w_TIPO;
        into cursor _Curs_ATT_PREC
    endif
    if used('_Curs_ATT_PREC')
      select _Curs_ATT_PREC
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_ATT_PREC.Conta , 0)
        select _Curs_ATT_PREC
        continue
      enddo
      use
    endif
    if this.w_CONTA>0
      this.w_PARAMETROMESSAGGIO1 = alltrim(this.w_NODO)
      if Azione="Rinomina"
        this.w_MESS = "Attivit� %1 presente nell'archivio delle attivit� precedenti. Impossibile rinominare"
      else
        this.w_MESS = "Attivit� %1 presente nell'archivio delle attivit� precedenti. Impossibile cancellare%0� necessario reinserire tale attivit� nella struttura"
      endif
      this.w_OKATT = .F.
    endif
    if g_APPLICATION<>"ADHOC REVOLUTION" and this.w_OKATT
      * --- Select from MVM_MAST
      i_nConn=i_TableProp[this.MVM_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2],.t.,this.MVM_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MVM_MAST ";
            +" where MMTCOMME="+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND MMTCOATT="+cp_ToStrODBC(this.w_NODO)+" ";
             ,"_Curs_MVM_MAST")
      else
        select Count(*) As Conta from (i_cTable);
         where MMTCOMME=this.oParentObject.w_CODCOM2 AND MMTCOATT=this.w_NODO ;
          into cursor _Curs_MVM_MAST
      endif
      if used('_Curs_MVM_MAST')
        select _Curs_MVM_MAST
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_MVM_MAST.Conta , 0)
          select _Curs_MVM_MAST
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        this.w_PARAMETROMESSAGGIO1 = alltrim(this.w_NODO)
        if Azione="Rinomina"
          this.w_MESS = "Attivit� %1 presente nei movimenti di magazzino. Impossibile rinominare"
        else
          this.w_MESS = "Attivit� %1 presente nei movimenti di magazzino. Impossibile cancellare"
        endif
        this.w_OKATT = .F.
      endif
    endif
    if this.w_OKATT
      * --- Select from MVM_DETT
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MVM_DETT ";
            +" where MMCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND MMCODATT="+cp_ToStrODBC(this.w_NODO)+" AND MMTIPATT="+cp_ToStrODBC(this.w_TIPO)+"";
             ,"_Curs_MVM_DETT")
      else
        select Count(*) As Conta from (i_cTable);
         where MMCODCOM=this.oParentObject.w_CODCOM2 AND MMCODATT=this.w_NODO AND MMTIPATT=this.w_TIPO;
          into cursor _Curs_MVM_DETT
      endif
      if used('_Curs_MVM_DETT')
        select _Curs_MVM_DETT
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_MVM_DETT.Conta , 0)
          select _Curs_MVM_DETT
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        this.w_PARAMETROMESSAGGIO1 = alltrim(this.w_NODO)
        if Azione="Rinomina"
          this.w_MESS = "Attivit� %1 presente nei movimenti di magazzino. Impossibile rinominare"
        else
          this.w_MESS = "Attivit� %1 presente nei movimenti di magazzino. Impossibile cancellare"
        endif
        this.w_OKATT = .F.
      endif
    endif
    if this.w_OKATT
      * --- Select from DOC_MAST
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DOC_MAST ";
            +" where MVTCOMME="+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND MVTCOATT="+cp_ToStrODBC(this.w_NODO)+"";
             ,"_Curs_DOC_MAST")
      else
        select Count(*) As Conta from (i_cTable);
         where MVTCOMME=this.oParentObject.w_CODCOM2 AND MVTCOATT=this.w_NODO;
          into cursor _Curs_DOC_MAST
      endif
      if used('_Curs_DOC_MAST')
        select _Curs_DOC_MAST
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_DOC_MAST.Conta , 0)
          select _Curs_DOC_MAST
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        this.w_PARAMETROMESSAGGIO1 = alltrim(this.w_NODO)
        if Azione="Rinomina"
          this.w_MESS = "Attivit� %1 presente nei documenti. Impossibile rinominare"
        else
          this.w_MESS = "Attivit� %1 presente nei documenti. Impossibile cancellare"
        endif
        this.w_OKATT = .F.
      endif
    endif
    if this.w_OKATT
      * --- Select from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DOC_DETT ";
            +" where MVCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND MVTIPATT="+cp_ToStrODBC(this.w_TIPO)+" AND MVCODATT="+cp_ToStrODBC(this.w_NODO)+"";
             ,"_Curs_DOC_DETT")
      else
        select Count(*) As Conta from (i_cTable);
         where MVCODCOM=this.oParentObject.w_CODCOM2 AND MVTIPATT=this.w_TIPO AND MVCODATT=this.w_NODO;
          into cursor _Curs_DOC_DETT
      endif
      if used('_Curs_DOC_DETT')
        select _Curs_DOC_DETT
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_DOC_DETT.Conta , 0)
          select _Curs_DOC_DETT
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        this.w_PARAMETROMESSAGGIO1 = alltrim(this.w_NODO)
        if Azione="Rinomina"
          this.w_MESS = "Attivit� %1 presente nei documenti. Impossibile rinominare"
        else
          this.w_MESS = "Attivit� %1 presente nei documenti. Impossibile cancellare"
        endif
        this.w_OKATT = .F.
      endif
    endif
    if this.w_OKATT
      * --- Select from FABBIDET
      i_nConn=i_TableProp[this.FABBIDET_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FABBIDET_idx,2],.t.,this.FABBIDET_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" FABBIDET ";
            +" where FBCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND FBCODATT="+cp_ToStrODBC(this.w_NODO)+" AND FBTIPATT="+cp_ToStrODBC(this.w_TIPO)+"";
             ,"_Curs_FABBIDET")
      else
        select Count(*) As Conta from (i_cTable);
         where FBCODCOM=this.oParentObject.w_CODCOM2 AND FBCODATT=this.w_NODO AND FBTIPATT=this.w_TIPO;
          into cursor _Curs_FABBIDET
      endif
      if used('_Curs_FABBIDET')
        select _Curs_FABBIDET
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_FABBIDET.Conta , 0)
          select _Curs_FABBIDET
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        this.w_PARAMETROMESSAGGIO1 = alltrim(this.w_NODO)
        if Azione="Rinomina"
          this.w_MESS = "Attivit� %1 presente nel dettaglio fabbisogni. Impossibile rinominare"
        else
          this.w_MESS = "Attivit� %1 presente nel dettaglio fabbisogni. Impossibile cancellare"
        endif
        this.w_OKATT = .F.
      endif
    endif
    if this.w_OKATT
      * --- Select from MOVICOST
      i_nConn=i_TableProp[this.MOVICOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2],.t.,this.MOVICOST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MOVICOST ";
            +" where MRCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND MRCODATT="+cp_ToStrODBC(this.w_NODO)+" AND MRTIPATT="+cp_ToStrODBC(this.w_TIPO)+"";
             ,"_Curs_MOVICOST")
      else
        select Count(*) As Conta from (i_cTable);
         where MRCODCOM=this.oParentObject.w_CODCOM2 AND MRCODATT=this.w_NODO AND MRTIPATT=this.w_TIPO;
          into cursor _Curs_MOVICOST
      endif
      if used('_Curs_MOVICOST')
        select _Curs_MOVICOST
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_MOVICOST.Conta , 0)
          select _Curs_MOVICOST
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        this.w_PARAMETROMESSAGGIO1 = alltrim(this.w_NODO)
        if Azione="Rinomina"
          this.w_MESS = "Attivit� %1 presente nei movimenti di analitica. Impossibile rinominare"
        else
          this.w_MESS = "Attivit� %1 presente nei movimenti di analitica. Impossibile cancellare"
        endif
        this.w_OKATT = .F.
      endif
    endif
    if this.w_OKATT
      * --- Select from PDA_DETT
      i_nConn=i_TableProp[this.PDA_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2],.t.,this.PDA_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" PDA_DETT ";
            +" where PDCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND PDCODATT="+cp_ToStrODBC(this.w_NODO)+" AND PDTIPATT="+cp_ToStrODBC(this.w_TIPO)+"";
             ,"_Curs_PDA_DETT")
      else
        select Count(*) As Conta from (i_cTable);
         where PDCODCOM=this.oParentObject.w_CODCOM2 AND PDCODATT=this.w_NODO AND PDTIPATT=this.w_TIPO;
          into cursor _Curs_PDA_DETT
      endif
      if used('_Curs_PDA_DETT')
        select _Curs_PDA_DETT
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_PDA_DETT.Conta , 0)
          select _Curs_PDA_DETT
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        this.w_PARAMETROMESSAGGIO1 = alltrim(this.w_NODO)
        if Azione="Rinomina"
          this.w_MESS = "Attivit� %1 presente nel dettaglio PDA. Impossibile rinominare"
        else
          this.w_MESS = "Attivit� %1 presente nel dettaglio PDA. Impossibile cancellare"
        endif
        this.w_OKATT = .F.
      endif
    endif
    if this.w_OKATT
      * --- Select from DICH_MDO
      i_nConn=i_TableProp[this.DICH_MDO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DICH_MDO_idx,2],.t.,this.DICH_MDO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DICH_MDO ";
            +" where RDCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM2)+" AND RDCODATT="+cp_ToStrODBC(this.w_NODO)+" AND RDTIPATT="+cp_ToStrODBC(this.w_TIPO)+"";
             ,"_Curs_DICH_MDO")
      else
        select Count(*) As Conta from (i_cTable);
         where RDCODCOM=this.oParentObject.w_CODCOM2 AND RDCODATT=this.w_NODO AND RDTIPATT=this.w_TIPO;
          into cursor _Curs_DICH_MDO
      endif
      if used('_Curs_DICH_MDO')
        select _Curs_DICH_MDO
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_DICH_MDO.Conta , 0)
          select _Curs_DICH_MDO
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        this.w_PARAMETROMESSAGGIO1 = alltrim(this.w_NODO)
        if Azione="Rinomina"
          this.w_MESS = "Attivit� %1 presente nelle dichiarazioni di manodopera. Impossibile rinominare"
        else
          this.w_MESS = "Attivit� %1 presente nelle dichiarazioni di manodopera. Impossibile cancellare"
        endif
        this.w_OKATT = .F.
      endif
    endif
  endproc


  procedure Pag15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Menu3
    DEFINE POPUP MenuTV from MRow()+1,MCol()+1 shortcut margin
    DEFINE BAR 1 OF MenuTV prompt ah_MsgFormat("Esplosione albero")
    DEFINE BAR 2 OF MenuTV prompt ah_MsgFormat("Implosione albero")
    ON SELE BAR 1 OF MenuTV Azione="ExpAll"
    ON SELE BAR 2 OF MenuTV Azione="CollAll"
    ACTI POPUP MenuTV
    DEACTIVATE POPUP MenuTV
    RELEASE POPUPS MenuTV EXTENDED
  endproc


  proc Init(oParentObject,pTipOp)
    this.pTipOp=pTipOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,19)]
    this.cWorkTables[1]='STRUTTUR'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='MAFCOSTI'
    this.cWorkTables[4]='ATT_PREC'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='MAT_MAST'
    this.cWorkTables[7]='MAT_DETT'
    this.cWorkTables[8]='CAN_TIER'
    this.cWorkTables[9]='MA_COSTI'
    this.cWorkTables[10]='DICH_MDO'
    this.cWorkTables[11]='DOC_DETT'
    this.cWorkTables[12]='DOC_MAST'
    this.cWorkTables[13]='MVM_MAST'
    this.cWorkTables[14]='MVM_DETT'
    this.cWorkTables[15]='FABBIDET'
    this.cWorkTables[16]='GESPENNA'
    this.cWorkTables[17]='MOVICOST'
    this.cWorkTables[18]='PDA_DETT'
    this.cWorkTables[19]='CPAR_DEF'
    return(this.OpenAllTables(19))

  proc CloseCursors()
    if used('_Curs_MAFCOSTI')
      use in _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      use in _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      use in _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      use in _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      use in _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      use in _Curs_MAFCOSTI
    endif
    if used('_Curs_STRUTTUR')
      use in _Curs_STRUTTUR
    endif
    if used('_Curs_MAFCOSTI')
      use in _Curs_MAFCOSTI
    endif
    if used('_Curs_MAFCOSTI')
      use in _Curs_MAFCOSTI
    endif
    if used('_Curs_MAT_MAST')
      use in _Curs_MAT_MAST
    endif
    if used('_Curs_MAT_DETT')
      use in _Curs_MAT_DETT
    endif
    if used('_Curs_MAT_DETT')
      use in _Curs_MAT_DETT
    endif
    if used('_Curs_MAT_DETT')
      use in _Curs_MAT_DETT
    endif
    if used('_Curs_MAFCOSTI')
      use in _Curs_MAFCOSTI
    endif
    if used('_Curs_STRUTTUR')
      use in _Curs_STRUTTUR
    endif
    if used('_Curs_ATT_PREC')
      use in _Curs_ATT_PREC
    endif
    if used('_Curs_MVM_MAST')
      use in _Curs_MVM_MAST
    endif
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_FABBIDET')
      use in _Curs_FABBIDET
    endif
    if used('_Curs_MOVICOST')
      use in _Curs_MOVICOST
    endif
    if used('_Curs_PDA_DETT')
      use in _Curs_PDA_DETT
    endif
    if used('_Curs_DICH_MDO')
      use in _Curs_DICH_MDO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOp"
endproc
