* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc2bcc                                                        *
*              Eventi da Cruscotto commessa                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-14                                                      *
* Last revis.: 2015-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc2bcc",oParentObject,m.pAzione)
return(i_retval)

define class tgspc2bcc as StdBatch
  * --- Local variables
  pAzione = space(10)
  w_PADRE = .NULL.
  w_PROG = .NULL.
  w_CODCOM = space(15)
  w_CODCOS = space(5)
  w_CODVAL = space(3)
  w_DATREG = ctod("  /  /  ")
  w_TIPMOVC = space(1)
  w_TIPMOVR = space(1)
  w_TIPMOVM = space(1)
  w_CURLEGGR16 = space(15)
  w_VALCOM = space(3)
  w_CODATT = space(15)
  w_DESATT = space(30)
  w_DESCOM = space(30)
  w_DATAINI = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_TIPOSTRU = space(1)
  w_TIPMOV = space(1)
  w_PREVCONS = space(1)
  w_ClsName = space(10)
  w_AT_STATO = space(1)
  w_Graph16_LEGEND = space(15)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = This.oParentObject
    this.w_ClsName = UPPER(ALLTRIM(this.w_Padre.Class))
    this.w_CODCOM = this.w_PADRE.w_CODCOM
    if this.w_ClsName="TGSPC1KCC"
      this.w_DESCOM = this.w_PADRE.w_DESCOM
      this.w_CODATT = SPACE(15)
      this.w_DESATT = SPACE(30)
      this.w_CODCOS = this.w_PADRE.w_CODCOS
      this.w_CODVAL = this.w_PADRE.w_CODVAL
      this.w_DATREG = this.w_PADRE.w_DATREG
      this.w_DATAINI = cp_CharToDate("  -  -  ")
      this.w_DATAFIN = this.w_DATREG
      this.w_TIPOSTRU = "P"
      this.w_TIPMOVC = this.w_PADRE.w_TIPMOVC
      this.w_TIPMOVR = this.w_PADRE.w_TIPMOVR
      this.w_TIPMOVM = this.w_PADRE.w_TIPMOVM
      this.w_VALCOM = this.w_CODVAL
    endif
    if this.w_ClsName="TGSPC2KDC"
      this.w_CODATT = this.w_PADRE.w_ATCODATT
      this.w_AT_STATO = this.w_PADRE.w_AT_STATO
    endif
    do case
      case this.pAzione=="DETTCOSTITIP"
        if !Empty(nvl(this.w_CODCOS,""))
          Do GSPC_KDC with this.oParentObject
        endif
      case this.pAzione="DETTCOSTI"
        do case
          case this.pAzione=="DETTCOSTIP"
            this.w_TIPMOV = "P"
          case this.pAzione=="DETTCOSTIC"
            this.w_TIPMOV = "C"
          case this.pAzione=="DETTCOSTII"
            this.w_TIPMOV = "I"
          otherwise
            this.w_TIPMOV = "T"
        endcase
        this.w_PROG = GSPC_SMM(This)
        if !this.w_PROG.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_CODCOM = this.w_CODCOM
        this.w_PROG.w_TIPMOV = this.w_TIPMOV
        this.w_PROG.mCalc(.t.)     
        Do GSPC_BLM With this.w_PROG, "Requery"
      case this.pAzione=="DETTRICAVI"
        if !Empty(nvl(this.w_TIPMOVR,""))
          do case
            case this.w_TIPMOVR="R"
              this.w_PROG = GSOR_SZM(This)
              if !this.w_PROG.bSec1
                i_retcode = 'stop'
                return
              endif
              this.w_PROG.w_CODMAG = SPACE(5)
              this.w_PROG.w_DESAPP = SPACE(30)
              this.w_PROG.w_COMMESSA = this.w_CODCOM
              this.w_PROG.w_TIPEVA = "R"
              this.w_PROG.mCalc(.t.)     
              Do GSOR_BVM With this.w_PROG , "Init"
              Do GSOR_BVM With this.w_PROG , "O"
            otherwise
              this.w_PROG = GSPC_SMM(This)
              this.w_TIPMOV = this.w_TIPMOVR
              if !this.w_PROG.bSec1
                i_retcode = 'stop'
                return
              endif
              this.w_PROG.w_CODCOM = this.w_CODCOM
              this.w_PROG.w_TIPMOV = this.w_TIPMOVR
              this.w_PROG.mCalc(.t.)     
              Do GSPC_BLM With this.w_PROG, "Requery"
          endcase
        endif
      case this.pAzione=="DETTOREMDO"
        if !Empty(nvl(this.w_TIPMOVM,""))
          this.w_CODCOS = "MD"
          this.w_TIPMOV = this.w_TIPMOVM
          this.w_PREVCONS = this.w_TIPMOVM
          this.w_PROG = GSPC2KDC(This)
          if !this.w_PROG.bSec1
            i_retcode = 'stop'
            return
          endif
        endif
      case this.pAzione=="DETTMARGINE"
        this.oParentObject.w_Graph16_LEGEND = alltrim(nvl(this.w_PADRE.w_Graph16_LEGEND,""))
        do case
          case this.oParentObject.w_Graph16_LEGEND = "Atteso"
            VX_EXEC("_MarFin_,..\COMM\EXE\QUERY\GSPC4SAC_I.VFC")
          case this.oParentObject.w_Graph16_LEGEND = "Attuale"
            VX_EXEC("_MarAgg_,..\COMM\EXE\QUERY\GSPC3SAC_I.VFC")
          case this.oParentObject.w_Graph16_LEGEND = "Residuo"
            VX_EXEC("_MarAtt_,..\COMM\EXE\QUERY\GSPC5SAC_I.VFC")
        endcase
      case this.pAzione=="DETTTECATT"
        do GSPC2KDC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="CAMBIOSTATO"
        this.w_PROG = GSPC_KAS()
        if !this.w_PROG.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_CODCOM = this.w_CODCOM
        this.w_PROG.mCalc(.t.)     
        this.w_PROG.w_AT_STATO = this.w_AT_STATO
        this.w_PROG.mCalc(.t.)     
        this.w_PROG.NotifyEvent("Reload")     
    endcase
    this.w_PROG = .NULL.
    This.bUpdateParentObject=.f.
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
