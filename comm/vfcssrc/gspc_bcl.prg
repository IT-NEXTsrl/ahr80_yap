* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bcl                                                        *
*              Calcola listino mov.                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_213]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-28                                                      *
* Last revis.: 2016-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bcl",oParentObject,m.pExec)
return(i_retval)

define class tgspc_bcl as StdBatch
  * --- Local variables
  pExec = space(1)
  w_CODART = space(20)
  w_CODLIS = space(5)
  w_DATREG = ctod("  /  /  ")
  w_OK = .f.
  w_APPO = space(12)
  w_APPO = space(12)
  w_QTAUM1 = 0
  w_OLIPREZ = 0
  w_APPREZZO = 0
  w_OBJCTRL = .NULL.
  w_CHECK = .f.
  w_PADRE = .NULL.
  w_CALPRZ = 0
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_PREZZO = 0
  w_QTALIS = 0
  * --- WorkFile variables
  LISTINI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Listini Movimenti (da GSPC_MAT)
    * --- Calcola Prezzo da Listini
    this.w_CODART = this.oParentObject.w_MACODART
    this.w_CODLIS = this.oParentObject.w_MACODLIS
    this.w_DATREG = this.oParentObject.w_MADATREG
    if EMPTY (this.oParentObject.w_MACODLIS) 
      * --- Se non Specifico il listino esce
      i_retcode = 'stop'
      return
    endif
    * --- Individua il listino Corretto
    this.w_OK = .T.
    this.w_OLIPREZ = this.oParentObject.w_MAPREZZO
    msg=""
    if NOT EMPTY(this.w_CODLIS) AND NOT EMPTY(this.oParentObject.w_MAQTAMOV)
      this.w_APPO = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3
      if this.pExec $ "UR" AND this.oParentObject.w_MAQTA1UM<>0
        * --- Modificata 1^UM
        this.w_QTAUM1 = this.oParentObject.w_MAQTA1UM
      else
        this.w_QTAUM1 = CALQTA(this.oParentObject.w_MAQTAMOV,this.oParentObject.w_MAUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_NOFRAZ, this.oParentObject.w_MODUM2, @msg, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
      endif
      if NOT EMPTY(msg)
        * --- Errore relativo alla quantit� della 1a UM
        Ah_ErrorMsg(msg)
      endif
      this.w_PADRE = this.oParentObject
      this.w_OBJCTRL = this.w_PADRE.GetCtrl( "w_MACODART" )
      L_Method_Name="this.w_PADRE.Link"+Right( this.w_OBJCTRL.Name , LEN( this.w_OBJCTRL.Name ) - rat("_" , this.w_OBJCTRL.Name , 2)+1)+"('Full',this)"
      this.w_CHECK = &L_Method_Name
      if this.oParentObject.w_TIPCOD="F"
        this.w_QTAUM1 = this.oParentObject.w_MAQTA1UM
        this.oParentObject.w_MAQTAMOV = IIF(this.oParentObject.w_MAQTAMOV=0, 1, this.oParentObject.w_MAQTAMOV)
      endif
      DECLARE ARRCALC (16,1)
      * --- Azzero l'Array che verr� riempito dalla Funzione
      ARRCALC(1)=0
      this.w_QTAUM3 = CALQTA( this.w_QTAUM1,this.oParentObject.w_UNMIS3, Space(3),IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3,this.oParentObject.w_DECUNI)
      this.w_QTAUM2 = CALQTA( this.w_QTAUM1,this.oParentObject.w_UNMIS2, this.oParentObject.w_UNMIS2,IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3,this.oParentObject.w_DECUNI)
      DIMENSION pArrUm[9]
      pArrUm [1] = this.oParentObject.w_PREZUM 
 pArrUm [2] = this.oParentObject.w_MAUNIMIS 
 pArrUm [3] = this.oParentObject.w_MAQTAMOV 
 pArrUm [4] = this.oParentObject.w_UNMIS1 
 pArrUm [5] = this.oParentObject.w_MAQTA1UM 
 pArrUm [6] = this.oParentObject.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.oParentObject.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
      this.w_CALPRZ = CalPrzli( REPL("X",16) , " " , this.w_CODLIS , this.w_CODART , " " , this.w_QTAUM1 , this.oParentObject.w_MACODVAL , this.oParentObject.w_MACAOVAL , this.oParentObject.w_MADATREG , " " , " ", this.oParentObject.w_MACODVAL, " ", " ", " ", " ", 0,"M", @ARRCALC, " ", " ","N", @pArrUm )
      * --- ARRCALC(5)  contiene il prezzo di listino
      this.w_PREZZO = ARRCALC(5)
      this.oParentObject.w_CLUNIMIS = ARRCALC(16)
      this.oParentObject.w_QTALIS = IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS1,this.w_QTAUM1, IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
      if this.oParentObject.w_MAUNIMIS<>this.oParentObject.w_CLUNIMIS AND NOT EMPTY(this.oParentObject.w_CLUNIMIS)
        if EMPTY(this.oParentObject.w_FLUSEP) AND this.oParentObject.w_PREZUM="N"
          * --- Se in anagrafica articolo la combo  U.M. Separate � impostata a No, 
          *     per determinare il prezzo di riga nella seconda, o terza u.m. si utilizza il fattore di conversione tra prima e seconda \ terza u.m..
          this.w_APPREZZO = cp_Round(CALMMLIS(this.w_PREZZO, this.w_APPO+this.oParentObject.w_MAUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+ "N"+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, 0),this.oParentObject.w_DECUNI)
        else
          if this.oParentObject.w_MAUNIMIS=this.oParentObject.w_UNMIS1 or this.oParentObject.w_CLUNIMIS<>this.oParentObject.w_UNMIS3
            this.w_APPREZZO = cp_Round(CALMMPZ(this.w_PREZZO, this.oParentObject.w_MAQTAMOV, this.oParentObject.w_QTALIS, this.oParentObject.w_IVALIS, this.oParentObject.w_PERIVA, this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
          else
            this.w_APPREZZO = cp_Round(CALMMPZ(this.w_PREZZO, this.oParentObject.w_MAQTAMOV, w_MAQTAUM1, this.oParentObject.w_IVALIS, this.oParentObject.w_PERIVA, this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
          endif
        endif
      else
        if EMPTY(this.oParentObject.w_CLUNIMIS)
          this.oParentObject.w_QTALIS = this.w_QTAUM1
        else
          this.oParentObject.w_QTALIS = this.oParentObject.w_MAQTAMOV
        endif
        this.w_APPREZZO = CALMMPZ(this.w_PREZZO, this.oParentObject.w_MAQTAMOV, this.oParentObject.w_QTALIS, this.oParentObject.w_IVALIS, this.oParentObject.w_PERIVA, this.oParentObject.w_DECUNI)
      endif
    endif
    this.oParentObject.w_LIPREZZO = 0
    if this.pExec="A" AND this.w_APPREZZO<>this.oParentObject.w_MAPREZZO AND this.oParentObject.w_MAPREZZO<>0
      this.w_OK = ah_YesNo("Ricalcolo il prezzo in base al listino di origine?")
    endif
    if this.w_OK
      this.oParentObject.w_LIPREZZO = this.w_APPREZZO
    else
      this.oParentObject.w_LIPREZZO = this.w_OLIPREZ
    endif
    if this.pExec="A" AND this.oParentObject.o_LIPREZZO=this.oParentObject.w_LIPREZZO
      * --- Forza il Ricalcolo se variate QTA
      this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO + 1
    endif
  endproc


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LISTINI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
