* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bta                                                        *
*              Check cancellazione attivita                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_36]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-11                                                      *
* Last revis.: 2006-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bta",oParentObject)
return(i_retval)

define class tgspc_bta as StdBatch
  * --- Local variables
  w_CONTA = 0
  w_MESS = space(100)
  * --- WorkFile variables
  GESPENNA_idx=0
  MOVICOST_idx=0
  FABBIDET_idx=0
  MVM_MAST_idx=0
  MVM_DETT_idx=0
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  DICH_MDO_idx=0
  PDA_DETT_idx=0
  ATT_PREC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stessi controlli usati in GSPC_BG4 a Pag.14
    * --- Impedisce la cancellazione di una attivit� se movimentata in :
    *     1) Attivit� Precedenti
    *     2) Movimenti di Magazzino
    *     3) Documenti
    *     4) Dettaglio Fabbisogni
    *     5) Movimenti di Analitica
    *     6) Dettaglio PDA
    this.w_CONTA = 0
    * --- Select from ATT_PREC
    i_nConn=i_TableProp[this.ATT_PREC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2],.t.,this.ATT_PREC_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" ATT_PREC ";
          +" where MPCODCOM="+cp_ToStrODBC(this.oParentObject.w_ATCODCOM)+" AND MPATTPRE="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+" AND MPTIPATT="+cp_ToStrODBC(this.oParentObject.w_ATTIPATT)+"";
           ,"_Curs_ATT_PREC")
    else
      select Count(*) As Conta from (i_cTable);
       where MPCODCOM=this.oParentObject.w_ATCODCOM AND MPATTPRE=this.oParentObject.w_ATCODATT AND MPTIPATT=this.oParentObject.w_ATTIPATT;
        into cursor _Curs_ATT_PREC
    endif
    if used('_Curs_ATT_PREC')
      select _Curs_ATT_PREC
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_ATT_PREC.Conta , 0)
        select _Curs_ATT_PREC
        continue
      enddo
      use
    endif
    if this.w_CONTA>0
      this.w_MESS = ah_MsgFormat("Attivit� %1 presente nelle attivit� precedenti. Impossibile cancellare",alltrim(this.oParentObject.w_ATCODATT))
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
    if g_APPLICATION<>"ADHOC REVOLUTION"
      * --- Select from MVM_MAST
      i_nConn=i_TableProp[this.MVM_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2],.t.,this.MVM_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MVM_MAST ";
            +" where MMTCOMME="+cp_ToStrODBC(this.oParentObject.w_ATCODCOM)+" AND MMTCOATT="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+"";
             ,"_Curs_MVM_MAST")
      else
        select Count(*) As Conta from (i_cTable);
         where MMTCOMME=this.oParentObject.w_ATCODCOM AND MMTCOATT=this.oParentObject.w_ATCODATT;
          into cursor _Curs_MVM_MAST
      endif
      if used('_Curs_MVM_MAST')
        select _Curs_MVM_MAST
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl ( _Curs_MVM_MAST.Conta , 0)
          select _Curs_MVM_MAST
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        this.w_MESS = ah_MsgFormat("Attivit� %1 presente nei movimenti di magazzino. Impossibile cancellare",alltrim(this.oParentObject.w_ATCODATT))
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Select from MVM_DETT
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MVM_DETT ";
          +" where MMCODCOM=w_ATCODDOM AND MMCODATT="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+" AND MMTIPATT="+cp_ToStrODBC(this.oParentObject.w_ATTIPATT)+"";
           ,"_Curs_MVM_DETT")
    else
      select Count(*) As Conta from (i_cTable);
       where MMCODCOM=w_ATCODDOM AND MMCODATT=this.oParentObject.w_ATCODATT AND MMTIPATT=this.oParentObject.w_ATTIPATT;
        into cursor _Curs_MVM_DETT
    endif
    if used('_Curs_MVM_DETT')
      select _Curs_MVM_DETT
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_MVM_DETT.Conta , 0)
        select _Curs_MVM_DETT
        continue
      enddo
      use
    endif
    if this.w_CONTA>0
      this.w_MESS = ah_MsgFormat("Attivit� %1 presente nei movimenti di magazzino. Impossibile cancellare",alltrim(this.oParentObject.w_ATCODATT))
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
    * --- Select from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DOC_MAST ";
          +" where MVTCOMME="+cp_ToStrODBC(this.oParentObject.w_ATCODCOM)+" AND MVTCOATT="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+"";
           ,"_Curs_DOC_MAST")
    else
      select Count(*) As Conta from (i_cTable);
       where MVTCOMME=this.oParentObject.w_ATCODCOM AND MVTCOATT=this.oParentObject.w_ATCODATT;
        into cursor _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_MAST')
      select _Curs_DOC_MAST
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_DOC_MAST.Conta , 0)
        select _Curs_DOC_MAST
        continue
      enddo
      use
    endif
    if this.w_CONTA>0
      this.w_MESS = ah_MsgFormat("Attivit� %1 presente nei documenti. Impossibile cancellare",alltrim(this.oParentObject.w_ATCODATT))
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DOC_DETT ";
          +" where MVCODCOM="+cp_ToStrODBC(this.oParentObject.w_ATCODCOM)+" AND MVTIPATT="+cp_ToStrODBC(this.oParentObject.w_ATTIPATT)+" AND MVCODATT="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+"";
           ,"_Curs_DOC_DETT")
    else
      select Count(*) As Conta from (i_cTable);
       where MVCODCOM=this.oParentObject.w_ATCODCOM AND MVTIPATT=this.oParentObject.w_ATTIPATT AND MVCODATT=this.oParentObject.w_ATCODATT;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_DOC_DETT.Conta , 0)
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    if this.w_CONTA>0
      this.w_MESS = ah_MsgFormat("Attivit� %1 presente nei documenti. Impossibile cancellare",alltrim(this.oParentObject.w_ATCODATT))
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
    * --- Select from FABBIDET
    i_nConn=i_TableProp[this.FABBIDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FABBIDET_idx,2],.t.,this.FABBIDET_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" FABBIDET ";
          +" where FBCODCOM="+cp_ToStrODBC(this.oParentObject.w_ATCODCOM)+" AND FBCODATT="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+" AND FBTIPATT="+cp_ToStrODBC(this.oParentObject.w_ATTIPATT)+"";
           ,"_Curs_FABBIDET")
    else
      select Count(*) As Conta from (i_cTable);
       where FBCODCOM=this.oParentObject.w_ATCODCOM AND FBCODATT=this.oParentObject.w_ATCODATT AND FBTIPATT=this.oParentObject.w_ATTIPATT;
        into cursor _Curs_FABBIDET
    endif
    if used('_Curs_FABBIDET')
      select _Curs_FABBIDET
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_FABBIDET.Conta , 0)
        select _Curs_FABBIDET
        continue
      enddo
      use
    endif
    if this.w_CONTA>0
      this.w_MESS = ah_MsgFormat("Attivit� %1 presente nel dettaglio fabbisogni. Impossibile cancellare",alltrim(this.oParentObject.w_ATCODATT))
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
    * --- Select from MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2],.t.,this.MOVICOST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MOVICOST ";
          +" where MRCODCOM="+cp_ToStrODBC(this.oParentObject.w_ATCODCOM)+" AND MRCODATT="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+" AND MRTIPATT="+cp_ToStrODBC(this.oParentObject.w_ATTIPATT)+"";
           ,"_Curs_MOVICOST")
    else
      select Count(*) As Conta from (i_cTable);
       where MRCODCOM=this.oParentObject.w_ATCODCOM AND MRCODATT=this.oParentObject.w_ATCODATT AND MRTIPATT=this.oParentObject.w_ATTIPATT;
        into cursor _Curs_MOVICOST
    endif
    if used('_Curs_MOVICOST')
      select _Curs_MOVICOST
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_MOVICOST.Conta , 0)
        select _Curs_MOVICOST
        continue
      enddo
      use
    endif
    if this.w_CONTA>0
      this.w_MESS = ah_MsgFormat("Attivit� %1 presente nei movimenti di analitica. Impossibile cancellare",alltrim(this.oParentObject.w_ATCODATT))
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
    * --- Select from PDA_DETT
    i_nConn=i_TableProp[this.PDA_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2],.t.,this.PDA_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" PDA_DETT ";
          +" where PDCODCOM="+cp_ToStrODBC(this.oParentObject.w_ATCODCOM)+" AND PDCODATT="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+" AND PDTIPATT="+cp_ToStrODBC(this.oParentObject.w_ATTIPATT)+"";
           ,"_Curs_PDA_DETT")
    else
      select Count(*) As Conta from (i_cTable);
       where PDCODCOM=this.oParentObject.w_ATCODCOM AND PDCODATT=this.oParentObject.w_ATCODATT AND PDTIPATT=this.oParentObject.w_ATTIPATT;
        into cursor _Curs_PDA_DETT
    endif
    if used('_Curs_PDA_DETT')
      select _Curs_PDA_DETT
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl ( _Curs_PDA_DETT.Conta , 0)
        select _Curs_PDA_DETT
        continue
      enddo
      use
    endif
    if this.w_CONTA>0
      this.w_MESS = ah_MsgFormat("Attivit� %1 presente nel dettaglio PDA. Impossibile cancellare",alltrim(this.oParentObject.w_ATCODATT))
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='GESPENNA'
    this.cWorkTables[2]='MOVICOST'
    this.cWorkTables[3]='FABBIDET'
    this.cWorkTables[4]='MVM_MAST'
    this.cWorkTables[5]='MVM_DETT'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='DOC_DETT'
    this.cWorkTables[8]='DICH_MDO'
    this.cWorkTables[9]='PDA_DETT'
    this.cWorkTables[10]='ATT_PREC'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_ATT_PREC')
      use in _Curs_ATT_PREC
    endif
    if used('_Curs_MVM_MAST')
      use in _Curs_MVM_MAST
    endif
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_FABBIDET')
      use in _Curs_FABBIDET
    endif
    if used('_Curs_MOVICOST')
      use in _Curs_MOVICOST
    endif
    if used('_Curs_PDA_DETT')
      use in _Curs_PDA_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
