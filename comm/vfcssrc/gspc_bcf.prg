* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bcf                                                        *
*              Cancella legami dei figli                                       *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_11]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-20                                                      *
* Last revis.: 2006-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bcf",oParentObject)
return(i_retval)

define class tgspc_bcf as StdBatch
  * --- Local variables
  w_MESS = space(100)
  w_LEVEL = space(10)
  w_CODATT = space(15)
  w_NOWLVL = space(10)
  w_RECPOS = 0
  * --- WorkFile variables
  STRUTTUR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancella i figli dei figli
    * --- Questa funzionalit� si appoggia alla treeview, quindi se la gestione non �
    * --- stata lanciata da li non viene attivata
    Obj_form=this.oParentObject.w_TREEFORM
    if type("Obj_form")="O"
      this.w_MESS = "Si desidera cancellare anche i legami sottostanti (figli dei figli)?"
      if ah_YesNo(this.w_MESS)
        * --- Cancellazione Figli dei Figli
        * --- Scorro il cursore per trovare il Padre
        SELECT (this.oParentObject.w_TREEFORM.w_CURSORNA)
        Go Top
        locate for ATCODATT=this.oParentObject.w_STATTPAD
        if FOUND()
          * --- Non pu� essere altrimenti
          * --- Leggo il livello
          this.w_LEVEL = Rtrim(LVLKEY)
          SELECT (this.oParentObject.w_TREEFORM.w_CURSORNA)
          Go Top
          * --- Prendo tutti quelli che hanno un livello con il prefisso come il mio  e che hanno almeno un punti pi� di me e non siano attivit
          SCAN FOR LEFT(LVLKEY,LEN(this.w_LEVEL))=this.w_LEVEL AND OCCURS(".",LVLKEY)>=OCCURS(".",this.w_LEVEL)+1 AND ATTIPATT<>"A"
          * --- Per ognuno determino se � padre (devo cancellare) oppure se figlio (non faccio niente)
          this.w_CODATT = ATCODATT
          this.w_NOWLVL = Rtrim(LVLKEY)+".  1"
          this.w_RECPOS = RECNO()
          SELECT (this.oParentObject.w_TREEFORM.w_CURSORNA)
          Go Top
          locate for LVLKEY=this.w_NOWLVL
          if FOUND()
            * --- Ha un figlio lo cancello dai legami - specifico solo la chiave in testata
            * --- Delete from STRUTTUR
            i_nConn=i_TableProp[this.STRUTTUR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"STCODCOM = "+cp_ToStrODBC(this.oParentObject.w_STCODCOM);
                    +" and STTIPSTR = "+cp_ToStrODBC(this.oParentObject.w_STTIPSTR);
                    +" and STATTPAD = "+cp_ToStrODBC(this.w_CODATT);
                     )
            else
              delete from (i_cTable) where;
                    STCODCOM = this.oParentObject.w_STCODCOM;
                    and STTIPSTR = this.oParentObject.w_STTIPSTR;
                    and STATTPAD = this.w_CODATT;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          SELECT (this.oParentObject.w_TREEFORM.w_CURSORNA)
          Go this.w_RECPOS
          ENDSCAN
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='STRUTTUR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
