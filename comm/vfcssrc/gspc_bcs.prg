* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bcs                                                        *
*              Check struttura                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_8]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-15                                                      *
* Last revis.: 2009-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bcs",oParentObject)
return(i_retval)

define class tgspc_bcs as StdBatch
  * --- Local variables
  w_CONTA = 0
  w_MESS = space(100)
  w_OK = .f.
  * --- WorkFile variables
  STRUTTUR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Check Struttura lanciato da GSPC_MST alla conferma  (lanciato riga per riga)
    * --- Unica verifica quella se nel database esistono altri legami aventi figli nel dettaglio.
    * --- Nel dettaglio non posso mettere due figli uguali (parte della chiave)
    * --- Controllo che il totale dei millesimi sia esattamente 1000
    * --- Controllo (in caricamento )che non esista un legame con il padre uguale a quello che sto salvando
    this.w_OK = .T.
    if this.oParentObject.w_STTIPSTR="T" or nvl(this.oParentObject.w_UNIQUE," ")="S"
      if this.oParentObject.w_TOTMIL<>1000 And this.w_OK
        * --- Controllo somma millesimi
        this.w_MESS = ah_MsgFormat("Il totale dei millesimi deve essere esattamente 1000")
        this.w_OK = .F.
      endif
    endif
    if this.w_OK
      VQ_EXEC("..\COMM\EXE\QUERY\GSPC_BCS.VQR",this,"CONTA")
      this.w_CONTA = NVL(CONTA.CONTA,0)
      if this.w_CONTA>0
        * --- Componente gi� utilizzato
        this.w_MESS = ah_MsgFormat("Il conto %1 alla riga %2 � gi� utilizzato in un altro legame",this.oParentObject.w_STATTFIG,ALLTRIM(STR(this.oParentObject.w_CPROWORD)))
        this.w_OK = .F.
      endif
      if USED("CONTA")
        SELECT ("CONTA")
        USE
      endif
    endif
    if Not this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='STRUTTUR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
