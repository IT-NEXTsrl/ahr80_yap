* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bcd                                                        *
*              Carica dettaglio relazioni                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_46]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-29                                                      *
* Last revis.: 2006-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pWhoCall
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bcd",oParentObject,m.pWhoCall)
return(i_retval)

define class tgspc_bcd as StdBatch
  * --- Local variables
  pWhoCall = space(20)
  w_ZOOMCUR = space(10)
  w_TIPO = space(1)
  w_FIRSTLAP = .f.
  w_ROWNUM = 0
  w_PADREAM = space(15)
  w_MILRIGA = 0
  w_DETTCUR = space(10)
  w_CODICE = space(15)
  w_DESCRI = space(30)
  w_AGGPAD = space(1)
  w_MILLES = 0
  w_RESTO = 0
  w_FIRSTROW = .f.
  w_PRJVW2 = space(1)
  w_UNIQIE = space(1)
  w_CONTA = 0
  * --- WorkFile variables
  STRUTTUR_idx=0
  ATTIVITA_idx=0
  CPAR_DEF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Caricamento Rapido Legami
    * --- Se vengo chiamato dalla Maschera con Tree View aggiorno direttamente
    * --- il database altrimenti scrivo nel cursore del dettaglio (il parametro assume come valori i nomi dei DEF sopra)
    this.w_ZOOMCUR = this.oParentObject.w_ZOOM.cCursor
    do case
      case this.pWhoCall="GSPC_BGS"
        * --- Attivato da GCPS_BGS esegue delle scritture direttamente nel database
        * --- Try
        local bErr_033C2C90
        bErr_033C2C90=bTrsErr
        this.Try_033C2C90()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Errore durante la scrittura del legame","stop","")
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_033C2C90
        * --- End
        * --- Non fa l'aggiornamento automatico al TIPOCARI del Batch GSPC_BGS, lo faccio io
        this.oParentObject.oparentObject.w_TIPOCARI="Save"
        * --- Al termine chiudo la maschera
        this.oParentObject.EcpQuit()
      case this.pWhoCall="GSPC_MST"
        * --- Attivato da GCPS_SCS riempie il dettaglio dei legami con gli elementi selezionati nella Zoom
        * --- Cursori: il primo � quello dello Zoom il secondo quello del dettaglio della movimentazione
        this.w_DETTCUR = this.oParentObject.oParentObject.cTrsName
        this.w_FIRSTLAP = .T.
        select (this.w_ZOOMCUR)
        Go Top
        scan for xchk=1
        this.w_TIPO = ATTIPATT
        this.w_CODICE = ATCODATT
        select (this.w_DETTCUR)
        * --- La prima volta svuoto il dettaglio
        if this.w_FIRSTLAP
          this.w_FIRSTLAP = .F.
          Zap
        endif
        this.oParentObject.oParentObject.InitRow()
        * --- Assegno le variabili
        this.oParentObject.oParentObject.w_STATTFIG=this.w_CODICE
        if g_APPLICATION="ADHOC REVOLUTION"
          this.oParentObject.oParentObject.w_TIPATT=iif(this.w_TIPO="A","A","S")
        endif
        this.oParentObject.oParentObject.w_STTIPFIG=this.w_TIPO
        * --- Lancio la mCalc per aggiornare i campi derivati
        this.oParentObject.oParentObject.mCalc(.t.)
        this.oParentObject.oParentObject.TrsFromWork()
        select (this.w_ZOOMCUR)
        endscan
        SELECT (this.oParentObject.oParentObject.cTrsName)
        GO TOP
        With this.oParentObject.oParentObject
        .WorkFromTrs()
        .SaveDependsOn()
        .SetControlsValue()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        EndWith
        * --- Alla fine chiudo la maschera
        this.oParentObject.ecpquit()
    endcase
  endproc
  proc Try_033C2C90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Legame creato","!","")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione del Dettaglio direttamente sul Database
    this.w_DESCRI = ""
    this.w_ROWNUM = 0
    Select (this.w_ZOOMCUR)
    Go Top
    Count For xChk=1 to this.w_CONTA
    * --- Calcolo il quantitativo per riga
    this.w_MILRIGA = Int(1000/ this.w_CONTA )
    this.w_RESTO = Mod (1000 , this.w_CONTA)
    this.w_FIRSTROW = .T.
    Scan For XChk=1
    this.w_ROWNUM = this.w_ROWNUM+10
    this.w_TIPO = ATTIPATT
    this.w_CODICE = ATCODATT
    * --- Se Struttura di Tipo tecnico calcolo i millesimi
    if this.oParentObject.w_STTIPSTR="T" or g_APPLICATION="ADHOC REVOLUTION"
      if this.w_FIRSTROW
        this.w_MILLES = this.w_MILRIGA+this.w_RESTO
        this.w_FIRSTROW = .F.
      else
        this.w_MILLES = this.w_MILRIGA
      endif
    else
      this.w_MILLES = 0
    endif
    * --- Insert into STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STATTFIG"+",STTIPFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STCODCOM),'STRUTTUR','STCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STTIPSTR),'STRUTTUR','STTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STATTPAD),'STRUTTUR','STATTPAD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'STRUTTUR','STDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'STRUTTUR','STATTFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPO),'STRUTTUR','STTIPFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'STRUTTUR','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MILLES),'STRUTTUR','STMILLES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.oParentObject.w_STCODCOM,'STTIPSTR',this.oParentObject.w_STTIPSTR,'STATTPAD',this.oParentObject.w_STATTPAD,'STDESCRI',this.w_DESCRI,'STATTFIG',this.w_CODICE,'STTIPFIG',this.w_TIPO,'CPROWORD',this.w_ROWNUM,'STMILLES',this.w_MILLES)
      insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STATTFIG,STTIPFIG,CPROWORD,STMILLES &i_ccchkf. );
         values (;
           this.oParentObject.w_STCODCOM;
           ,this.oParentObject.w_STTIPSTR;
           ,this.oParentObject.w_STATTPAD;
           ,this.w_DESCRI;
           ,this.w_CODICE;
           ,this.w_TIPO;
           ,this.w_ROWNUM;
           ,this.w_MILLES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    Select (this.w_ZOOMCUR)
    EndScan
    * --- Aggiorno  se Nodo foglia
    * --- Read from ATTIVITA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AT_PRJVW"+;
        " from "+i_cTable+" ATTIVITA where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_STCODCOM);
            +" and ATTIPATT = "+cp_ToStrODBC(this.oParentObject.w_STTIPSTR);
            +" and ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_STATTPAD);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AT_PRJVW;
        from (i_cTable) where;
            ATCODCOM = this.oParentObject.w_STCODCOM;
            and ATTIPATT = this.oParentObject.w_STTIPSTR;
            and ATCODATT = this.oParentObject.w_STATTPAD;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PRJVW2 = NVL(cp_ToDate(_read_.AT_PRJVW),cp_NullValue(_read_.AT_PRJVW))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CPAR_DEF
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDUNIQUE"+;
        " from "+i_cTable+" CPAR_DEF where ";
            +"PDCHIAVE = "+cp_ToStrODBC("TAM");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDUNIQUE;
        from (i_cTable) where;
            PDCHIAVE = "TAM";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_UNIQUE = NVL(cp_ToDate(_read_.PDUNIQUE),cp_NullValue(_read_.PDUNIQUE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PRJVW2 = IIF ( this.w_PRJVW2 $ "FN" , "F" ,this.w_PRJVW2 )
    GSPC_BVP(this,this.oParentObject.w_STCODCOM,this.oParentObject.w_STATTPAD,this.oParentObject.w_STTIPSTR,this.w_PRJVW2,w_UNIQUE)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pWhoCall)
    this.pWhoCall=pWhoCall
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='STRUTTUR'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='CPAR_DEF'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pWhoCall"
endproc
