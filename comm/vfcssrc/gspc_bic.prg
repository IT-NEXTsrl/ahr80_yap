* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bic                                                        *
*              Indici di commessa                                              *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_54]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-30                                                      *
* Last revis.: 2010-02-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bic",oParentObject,m.pTipo)
return(i_retval)

define class tgspc_bic as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_NOLINK = .f.
  w_NODO = space(15)
  w_TMPN = 0
  w_TUTTOZERO = .f.
  w_TIPNODO = space(1)
  w_FIRSTLAP = .f.
  w_COSTO = 0
  w_PRINODO = space(15)
  w_TMPC = space(15)
  w_CURSORNA = space(10)
  w_CURS = space(10)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_TIPSTR = space(1)
  w_RIFKEY = space(100)
  * --- WorkFile variables
  STRUTTUR_idx=0
  MA_COSTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Indice di Commessa
    do case
      case this.pTipo="ESEGUI"
        * --- Costruisco la porzione di Albero
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Cur = WrCursor ( this.w_CURSORNA )
        Select * from (this.w_CURSORNA) into cursor (this.w_CURS) readwrite
        * --- Modifico il tipo del campo Importi al massimo gestibile da Fox
        * --- Se tutti gli importi sono a zero non visualizzo il grafico
        this.w_TUTTOZERO = .T.
        if USED( this.w_CURS ) and RECCOUNT( this.w_CURS ) > 0
          Alter Table ( this.w_CURS ) Alter Column QtaComp N ( 20 , 4 )
          * --- Calcolo i costi per ogni Attivit�
          Select ( this.w_CURS )
          * --- Scorro le Attivit� e nel Campo QTACOMP metto il totale per ognuna dei costi
          Scan For AtTipAtt="A"
          this.w_COSTO = 0
          this.w_NODO = ATCODATT
          ah_Msg("Calcolo totale costi di %1",.T.,.F.,.F.,this.w_NODO)
          * --- Select from MA_COSTI
          i_nConn=i_TableProp[this.MA_COSTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2],.t.,this.MA_COSTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Sum (CSCONSUN) As Costo  from "+i_cTable+" MA_COSTI ";
                +" where CSCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM)+" And CSTIPSTR='A' And CSCODMAT="+cp_ToStrODBC(this.w_NODO)+"";
                 ,"_Curs_MA_COSTI")
          else
            select Sum (CSCONSUN) As Costo from (i_cTable);
             where CSCODCOM=this.oParentObject.w_CODCOM And CSTIPSTR="A" And CSCODMAT=this.w_NODO;
              into cursor _Curs_MA_COSTI
          endif
          if used('_Curs_MA_COSTI')
            select _Curs_MA_COSTI
            locate for 1=1
            do while not(eof())
            * --- Divido per 1000 per Evitare Numeric OverFlow
            this.w_COSTO = Nvl( _Curs_MA_COSTI.Costo , 0 ) / 1000
              select _Curs_MA_COSTI
              continue
            enddo
            use
          endif
          Select ( this.w_CURS )
          Replace QtaComp With this.w_COSTO
          this.w_TUTTOZERO = this.w_TUTTOZERO And this.w_COSTO=0
          EndScan
        endif
        if this.w_TUTTOZERO
          * --- Tutti i valori a Zero
          ah_ERRORMSG("Il nodo selezionato non ha costi",48,"")
          * --- Rimuovo i cursori
           
 USE IN SELECT (this.w_CURSORNA) 
 USE IN SELECT (this.w_CURS) 
 USE IN SELECT ("_Primo_") 
 USE IN SELECT ("Query")
          i_retcode = 'stop'
          return
        else
          * --- Raggruppo per il primo Livello
          Select Left(Left(LvlKey,7)+Space(200),200) As Livello , Sum (QtaComp) As Incidenza ;
          From ( this.w_CURS ) Where Not Empty(AtTipCom) ;
          Group By Livello Into Cursor _Primo_ NoFilter
          * --- Lo metto in Join per avere i nomi dei Conti figli di w_CODNODO
          Select left(alltrim(AtCodAtt)+" "+atdescri,25) As Conto, Incidenza From _Primo_ ;
          Inner Join (this.w_CURS) On LvlKey==Livello ;
          Into Cursor _Indice_ NoFilter
          if reccount("_Indice_")>18
            * --- Max 18 voci ne grafico. Le eccedenti vengono raggrupate sotto la voce ALTRE
             
 Select *, 100 as ordinamento from _Indice_ into cursor _SoloDiciotto_ order by Incidenza DESC 
 =WrCursor("_SoloDiciotto_") 
 Select _SoloDiciotto_ 
 Go Top 
 Scan 
 REPLACE Ordinamento With IIF(RecNo()<18,RecNo(),18) 
 REPLACE Conto With IIF(RecNo()<18,Conto,"Altri") 
 EndScan 
 Select Max(Conto) as Conto, Sum(Incidenza) as Incidenza,Ordinamento from _SoloDiciotto_ ; 
 group by Ordinamento into cursor _PrimiDiciotto_ 
 Select _SoloDiciotto_ 
 Use 
 Select Conto, Incidenza from _PrimiDiciotto_ into cursor _Indice_ 
 Select _PrimiDiciotto_ 
 Use 
 Select _Indice_
          endif
          This.oParentObject.notifyEvent("GraphPag1")
          * --- Rimuovo i cursori
           
 USE IN SELECT (this.w_CURSORNA) 
 USE IN SELECT (this.w_CURS) 
 USE IN SELECT ("_Primo_") 
 USE IN SELECT ("Query")
        endif
      case this.pTipo="RIMUOVI"
        * --- Rimuove il Cursore del grafico
        if Used ("_Indice_")
          Select _Indice_
          Use
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea Cursore Preventivo
    this.w_CURSORNA = SYS(2015)
    this.w_CURS = SYS(2015)
    * --- Definizione parametri da passare alla CP_EXPDB
    this.w_COUTCURS = this.w_CURSORNA
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = ""
    * --- Altri campi
    this.w_TIPSTR = "P"
    AH_Msg( "Caricamento progetto..." )
    * --- Costruisco con una query contentente la base per l'esplosione (Il nodo selezionato)
    VQ_EXEC("..\COMM\EXE\QUERY\GSPC3BIC.VQR",this,"query")
    Select "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if USED("__tmp__")
        SELECT "__tmp__" 
 USE
      endif
    else
      ah_ERRORMSG("Il nodo selezionato non � padre di nessun legame",48,"")
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STRUTTUR'
    this.cWorkTables[2]='MA_COSTI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_MA_COSTI')
      use in _Curs_MA_COSTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
