* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bgs                                                        *
*              Gestione strutture                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_86]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-02                                                      *
* Last revis.: 2006-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bgs",oParentObject,m.pTipOp)
return(i_retval)

define class tgspc_bgs as StdBatch
  * --- Local variables
  pTipOp = space(10)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_RIFKEY = space(100)
  w_POSIZ = 0
  w_LVLKEY = space(200)
  w_LASTPOINT = 0
  w_CODATTCUR = space(15)
  w_RELAZ = .NULL.
  w_TIPSTRLOC = space(1)
  w_ATTIVI = .NULL.
  w_TIPOSTRU = space(1)
  w_ATCODATT = space(15)
  w_ATCODCOM = space(15)
  w_TIPOCARI = space(10)
  w_STATTPAD = space(15)
  w_STCODCOM = space(15)
  w_STTIPSTR = space(1)
  w_INDEX = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore da passare alla TreeView (da GSPC_SGS)
    do case
      case this.pTipop="Reload"
        * --- Aggiorna la Treeview
        if EMPTY(this.oParentObject.w_CURSORNA)
          this.oParentObject.w_CURSORNA = SYS(2015)
        endif
        * --- Definizione parametri da passare alla CP_EXPDB
        this.w_COUTCURS = this.oParentObject.w_CURSORNA
        * --- Cursore utilizzato dalla Tree View
        this.w_INPCURS = "QUERY"
        * --- Cursore di partenza
        this.w_CRIFTABLE = "ATTIVITA"
        * --- Tabella di riferimento
        this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
        * --- Chiave anagrafica componenti
        this.w_CEXPTABLE = "STRUTTUR"
        * --- Tabella di esplosione
        this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
        * --- Chiave della movimentazione contenente la struttura
        this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
        * --- Chiave ripetuta nella movimentazione contenente la struttura
        this.w_EXPFIELD = ""
        * --- Campi per espolosione
        this.w_OTHERFLD = "CPROWORD"
        * --- Altri campi (ordinamento)
        if Empty ( this.oParentObject.w_CODCONTO )
          * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
          VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
        else
          * --- Recupero solo la parte di struttura identificata dal conto
          VQ_EXEC("..\COMM\EXE\QUERY\GSPC0BSC.VQR",this,"query")
        endif
        SELECT "QUERY"
        if RECCOUNT()>0
          * --- Costruisco il cursore dei dati della Tree View
          PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
          if USED("__tmp__")
            SELECT "__tmp__" 
 USE
          endif
          * --- Riempio la Treeview
          this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
          this.oParentObject.notifyevent("Esegui")
        else
          * --- Se la prima commessa specificata non ha elementi
          if Not Used ( this.oParentObject.w_CURSORNA )
            ah_ErrorMsg("La commessa non ha nessun elemento",,"")
            i_retcode = 'stop'
            return
          endif
          Select ( this.oParentObject.w_CURSORNA )
          Zap
          ah_ErrorMsg("La commessa non ha nessun elemento",,"")
          * --- Riempio la Treeview
          this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
          this.oParentObject.notifyevent("Esegui")
        endif
      case this.pTipop="Close"
        * --- Elimina i cursori
        if used("query")
          select "query"
          use
        endif
        if used((this.oParentObject.w_CURSORNA))
          select ( this.oParentObject.w_CURSORNA )
          use
        endif
      case this.pTipOp="Attivita"
        * --- Lancia la gestione dell'attivit� selezionata nella tree View
        * --- A seconda del tipo Componente (attivit� o meno ) lancio la gestione appropriata
        if this.oParentObject.w_TIPCOM="A"
          this.w_ATTIVI = GSPC_AAT()
        else
          if this.oParentObject.w_TIPCOM="S"
            * --- Sotto Conti hanno un def diverso (Con i Costi)
            this.w_ATTIVI = GSPC_ASC()
          else
            this.w_ATTIVI = GSPC_ACM()
          endif
        endif
        if Type ( "This.w_ATTIVI" )<>"L"
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_ATTIVI.bSec1)
            i_retcode = 'stop'
            return
          endif
          * --- inizializzo la chiave delle attivita componenti
          this.w_ATTIVI.w_ATCODCOM = this.oParentObject.w_CODCOM
          this.w_ATTIVI.w_ATCODATT = this.oParentObject.w_CODATT
          this.w_ATTIVI.w_ATTIPATT = IIF(this.oParentObject.w_TIPCOM="A","A",this.oParentObject.w_TIPSTR)
          this.w_TIPSTRLOC = IIF(this.oParentObject.w_TIPCOM="A","A",this.oParentObject.w_TIPSTR)
          * --- creo il curosre delle solo chiavi
          this.w_ATTIVI.QueryKeySet("ATCODCOM='"+this.oParentObject.w_CODCOM+"'AND ATCODATT='"+this.oParentObject.w_CODATT+"' AND ATTIPATT='"+this.w_TIPSTRLOC+ "'","")     
          * --- mi metto in interrogazione e carico eventuali Post In Allegati
          this.w_ATTIVI.LoadRecWarn()     
          this.w_ATTIVI.LoadWarn()     
          oCpToolBar.SetQuery()
        endif
      case this.pTipOp="Relazione"
        * --- Lancia la gestione della struttura con il seguente criterio
        * --- Se il nodo selezionato � una attivit� cerco il padre e visualizzo la relazione
        * --- Altrrimenti se il nodo non � una attivit� visualizzo la relazione, se esiste, di cui lui � padre
        * --- se non esiste apro la gestione relazioni in caricamento con impostato come padre
        * --- il nodo senza figli.
        * --- Alla chiusura della gestione refresho la Tree view
        * --- Per determinare se il nodo ha figli vado a leggere il campo chiave (lvlkey)
        select ( this.oParentObject.w_CURSORNA )
        this.w_LVLKEY = Rtrim(LVLKEY)
        * --- Mi tengo la posizione sulla Treeview
        this.w_POSIZ = RecNo()
        if this.oParentObject.w_TIPCOM="A"
          * --- Ho selezionato una Attivit�
          * --- In questo caso lancio la gestione relazioni, devo trovare il nodo padre
          * --- Tolgo il pezzo di stringa che va dall'ultimo punto fino alla fine
          this.w_LASTPOINT = RAT(".",this.w_LVLKEY)
          this.w_LVLKEY = LEFT(this.w_LVLKEY,this.w_LASTPOINT-1)
        else
          * --- Ho selezionato un Conto o Sotto Conto
          * --- Cerco un suo figlio
          this.w_LVLKEY = this.w_LVLKEY+".  1"
        endif
        * --- Vado a leggere il codice attivit� a quel livello
        SELECT ( this.oParentObject.w_CURSORNA )
        Go Top
        * --- Cerco di capire se ha figli !
        LOCATE FOR LvlKey=this.w_LVLKEY
        if FOUND()
          * --- Ha figli ! lancio la relazione che lo vede come padre
          * --- Se attivit� invece lancio il nodo trovato sulla tree view (padre dell'attivit�)
          this.w_CODATTCUR = iif(this.oParentObject.w_TIPCOM="A",ATCODATT,this.oParentObject.w_CODATT)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Se ho selezionato una Attivit� qua non ci verr� mai (l'attivit� ha sempre figli)
          * --- Non ha figli lancio la gestione in caricamento con alcuni dati impostati
          this.w_CODATTCUR = this.oParentObject.w_CODATT
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pTipOp="RICERCA"
        * --- Ricerca del nodo nella Struttura
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipOp="MOVPREV"
        * --- Lancia il Movimento Preventivo associato all'attivit�
        this.w_TIPOSTRU = this.oParentObject.w_TIPSTR
        this.w_ATCODATT = this.oParentObject.w_CODATT
        this.w_ATCODCOM = this.oParentObject.w_CODCOM
        * --- Lancio il Batch lanciato dalla gestione Attivit�
        do GSPC_BMM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancio la gestione delle relazioni
    this.w_RELAZ = GSPC_MST()
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_RELAZ.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- inizializzo la chiave delle relazioni
    this.w_RELAZ.w_STCODCOM = this.oParentObject.w_CODCOM
    this.w_RELAZ.w_STTIPSTR = this.oParentObject.w_TIPSTR
    this.w_RELAZ.w_STATTPAD = this.w_CODATTCUR
    this.w_RELAZ.w_TREEFORM = this.oParentObject
    * --- creo il curosore delle solo chiavi
    this.w_RELAZ.QueryKeySet("STCODCOM='"+this.oParentObject.w_CODCOM+"'AND STATTPAD='"+this.w_CODATTCUR+"' AND STTIPSTR='"+this.oParentObject.w_TIPSTR+ "'","")     
    * --- mi metto in interrogazione
    this.w_RELAZ.LoadRecWarn()     
    oCpToolBar.SetQuery()
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento Relazioni - PARTE NON UTILIZZATA LA TENGO PER RAFFRONTO
    this.w_RELAZ = GSPC_MST()
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_RELAZ.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- La Apro in Caricamento
    this.w_RELAZ.oFirstControl.SetFocus()     
    this.w_RELAZ.cFunction = "Load"
    this.w_RELAZ.ReleaseWarn()     
    this.w_RELAZ.SetStatus()     
    this.w_RELAZ.mEnableControls()     
    this.w_RELAZ.BlankRec()     
    this.w_RELAZ.InitAutonumber()     
    this.w_RELAZ.NotifyEvent("New record")     
    this.w_RELAZ.Refresh()     
    * --- Inizializzo alcuni dati in testata
    this.w_RELAZ.w_STCODCOM = this.oParentObject.w_CODCOM
    this.w_RELAZ.w_STATTPAD = LEFT(this.w_CODATTCUR+SPACE(15),15)
    * --- Inizializzo la variabile per avere il riferimento alla maschera della Tree
    this.w_RELAZ.w_TREEFORM = this.oParentObject
    * --- Setto i valori a video
    this.w_RELAZ.SetControlsValue()     
    this.w_RELAZ.mCalc(.t.)     
    oCpToolBar.SetEdit()
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento Relazioni da Maschera
    * --- La procedura lancia la maschera di riempimento relazione
    * --- Creo alcune variabili di interfaccia verso la maschera
    this.w_TIPOCARI = "GSPC_BGS"
    this.w_STCODCOM = this.oParentObject.w_CODCOM
    this.w_STATTPAD = LEFT(this.w_CODATTCUR+SPACE(15),15)
    this.w_STTIPSTR = this.oParentObject.w_TIPSTR
    * --- Lancio la maschera del caricamento rapido
    do GSPC_SCS with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_TIPOCARI="Save"
      * --- Alla chiusura della maschera eseguo il refresh solo se generato un nuovo legame
      this.oParentObject.NotifyEvent("Reload")
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricerca del Nodo
    * --- Cerco il Nodo nel Cursore
    Select ( this.oParentObject.w_CURSORNA )
    Go Top
    Locate For ATCODATT=this.oParentObject.w_NODO And ATTIPATT= this.oParentObject.w_TIPORI
    if Found()
      this.w_LVLKEY = LvlKey
      * --- Se trovato scorro il cursore ed esplodo ogni elemento
      Select ( this.oParentObject.w_CURSORNA )
      Go Top
      this.w_INDEX = 1
      do while this.oParentObject.w_NODO<>ATCODATT Or this.oParentObject.w_TIPORI<>ATTIPATT
        * --- Lo espando solo se il suo LvlKey � prefisso del Lvlkey da visualizzare
        if Alltrim(LvlKey) = Left ( Alltrim(this.w_LVLkEY) , Len( Alltrim(LvlKey) ) )
          this.oParentObject.w_TREEVIEW.oTree.Nodes(This.w_Index).Expanded = .t.
        endif
        this.w_INDEX = this.w_INDEX+1
        Select ( this.oParentObject.w_CURSORNA )
        Skip
      enddo
      * --- Lo seleziono
      this.oParentObject.w_TREEVIEW.oTree.Nodes(this.w_INDEX).selected = .T.
      * --- Refresho la Griglia
      this.oParentObject.w_TREEVIEW.SetFocus()     
    else
      ah_ErrorMsg("Nessun elemento trovato",,"")
    endif
  endproc


  proc Init(oParentObject,pTipOp)
    this.pTipOp=pTipOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOp"
endproc
