* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_ktp                                                        *
*              Pianificazione con MS Project                                   *
*                                                                              *
*      Author: Paolo Saitti                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-26                                                      *
* Last revis.: 2012-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_ktp",oParentObject))

* --- Class definition
define class tgspc_ktp as StdForm
  Top    = 50
  Left   = 34

  * --- Standard Properties
  Width  = 536
  Height = 277+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-02"
  HelpContextID=185968535
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  CPAR_DEF_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gspc_ktp"
  cComment = "Pianificazione con MS Project"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_RDPARDEF = space(10)
  o_RDPARDEF = space(10)
  w_PRJREL = space(4)
  o_PRJREL = space(4)
  w_CHOICE = space(1)
  o_CHOICE = space(1)
  w_CODCOM = space(15)
  o_CODCOM = space(15)
  w_CODCONTO = space(15)
  w_CALENDARIO = space(50)
  w_ODBCSOURCE = space(50)
  w_ODBCUSR = space(50)
  w_ODBCPWD = space(50)
  w_PRJMOD = space(200)
  w_DESCOM = space(30)
  w_START_DFLT = ctod('  /  /  ')
  w_FINISH_DFLT = ctod('  /  /  ')
  w_UNIQUE = space(1)
  w_TIPSTR = space(1)
  w_PROGR = space(1)
  w_DESCON = space(30)
  w_Msg = space(0)
  w_OBTEST = ctod('  /  /  ')
  w_SELEZI = space(1)
  w_SELEZMUL = space(1)
  w_OPERAZ = space(1)
  w_SELTIP = space(1)
  w_CHECK = 0
  w_COMMESSA = space(15)
  o_COMMESSA = space(15)
  w_CHOICE = space(1)
  w_PPATHPRJ = space(200)
  o_PPATHPRJ = space(200)
  w_CBOSEL = .F.
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_ktpPag1","gspc_ktp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgspc_ktpPag2","gspc_ktp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni multiple")
      .Pages(3).addobject("oPag","tgspc_ktpPag3","gspc_ktp",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Messaggi elaborazione")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCHOICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(2).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CPAR_DEF'
    this.cWorkTables[3]='ATTIVITA'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSPC_BTP with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RDPARDEF=space(10)
      .w_PRJREL=space(4)
      .w_CHOICE=space(1)
      .w_CODCOM=space(15)
      .w_CODCONTO=space(15)
      .w_CALENDARIO=space(50)
      .w_ODBCSOURCE=space(50)
      .w_ODBCUSR=space(50)
      .w_ODBCPWD=space(50)
      .w_PRJMOD=space(200)
      .w_DESCOM=space(30)
      .w_START_DFLT=ctod("  /  /  ")
      .w_FINISH_DFLT=ctod("  /  /  ")
      .w_UNIQUE=space(1)
      .w_TIPSTR=space(1)
      .w_PROGR=space(1)
      .w_DESCON=space(30)
      .w_Msg=space(0)
      .w_OBTEST=ctod("  /  /  ")
      .w_SELEZI=space(1)
      .w_SELEZMUL=space(1)
      .w_OPERAZ=space(1)
      .w_SELTIP=space(1)
      .w_CHECK=0
      .w_COMMESSA=space(15)
      .w_CHOICE=space(1)
      .w_PPATHPRJ=space(200)
      .w_CBOSEL=.f.
        .w_RDPARDEF = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_RDPARDEF))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CHOICE = 'E'
        .w_CODCOM = g_CODCOM
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODCOM))
          .link_1_4('Full')
        endif
        .w_CODCONTO = SPACE(15)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODCONTO))
          .link_1_5('Full')
        endif
        .w_CALENDARIO = 'Standard'
          .DoRTCalc(7,14,.f.)
        .w_TIPSTR = IIF(.w_UNIQUE='S','P','G')
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate('*')
          .DoRTCalc(16,18,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page2.oPag.Zoom.Calculate(.w_CODCOM)
        .w_SELEZI = "D"
        .w_SELEZMUL = 'N'
        .w_OPERAZ = "S"
        .w_SELTIP = "D"
        .w_CHECK = .w_Zoom.GetVar('XCHK')
        .w_COMMESSA = .w_Zoom.GetVar('CNCODCAN')
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_17.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_18.Calculate()
        .w_CHOICE = 'E'
        .w_PPATHPRJ = iif(!EMPTY(alltrim(.w_PPATHPRJ)), FULLPATH(alltrim(.w_PPATHPRJ)+iif(right(alltrim(.w_PPATHPRJ),1)="\","","\")), FULLPATH(alltrim(LEFT(.w_PRJMOD,ratc('\',.w_PRJMOD) ) ) ) )
        .w_CBOSEL = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_7.enabled = this.oPgFrm.Page2.oPag.oBtn_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_RDPARDEF<>.w_RDPARDEF
          .link_1_1('Full')
        endif
        .DoRTCalc(2,4,.t.)
        if .o_CODCOM<>.w_CODCOM
            .w_CODCONTO = SPACE(15)
          .link_1_5('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate('*')
        .oPgFrm.Page2.oPag.Zoom.Calculate(.w_CODCOM)
        .DoRTCalc(6,23,.t.)
            .w_CHECK = .w_Zoom.GetVar('XCHK')
            .w_COMMESSA = .w_Zoom.GetVar('CNCODCAN')
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        if .o_COMMESSA<>.w_COMMESSA.or. .o_CHOICE<>.w_CHOICE
          .Calculate_KBPSNYYJHZ()
        endif
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_17.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_18.Calculate()
        .DoRTCalc(26,26,.t.)
        if .o_PPATHPRJ<>.w_PPATHPRJ.or. .o_CHOICE<>.w_CHOICE
            .w_PPATHPRJ = iif(!EMPTY(alltrim(.w_PPATHPRJ)), FULLPATH(alltrim(.w_PPATHPRJ)+iif(right(alltrim(.w_PPATHPRJ),1)="\","","\")), FULLPATH(alltrim(LEFT(.w_PRJMOD,ratc('\',.w_PRJMOD) ) ) ) )
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate('*')
        .oPgFrm.Page2.oPag.Zoom.Calculate(.w_CODCOM)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_17.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_18.Calculate()
    endwith
  return

  proc Calculate_ELTYIEGLDY()
    with this
          * --- Cambio commessa
          GSPC1BTP(this;
              ,"CAMBIOCOM";
             )
    endwith
  endproc
  proc Calculate_XNBGLZKNXX()
    with this
          * --- Seleziona/deseleziona tutto
          GSPC1BTP(this;
              ,"SELEZI";
             )
    endwith
  endproc
  proc Calculate_MFNZHGNNST()
    with this
          * --- Seleziona/deseleziona tutto
          GSPC1BTP(this;
              ,"SELTIP";
             )
    endwith
  endproc
  proc Calculate_KBPSNYYJHZ()
    with this
          * --- Cambio ri riga
          GSPC1BTP(this;
              ,'CHANGEROW';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOM_1_4.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCODCONTO_1_5.enabled = this.oPgFrm.Page1.oPag.oCODCONTO_1_5.mCond()
    this.oPgFrm.Page1.oPag.oSELEZMUL_1_31.enabled = this.oPgFrm.Page1.oPag.oSELEZMUL_1_31.mCond()
    this.oPgFrm.Page2.oPag.oOPERAZ_2_6.enabled_(this.oPgFrm.Page2.oPag.oOPERAZ_2_6.mCond())
    this.oPgFrm.Page2.oPag.oSELTIP_2_8.enabled_(this.oPgFrm.Page2.oPag.oSELTIP_2_8.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_7.enabled = this.oPgFrm.Page2.oPag.oBtn_2_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODCONTO_1_5.visible=!this.oPgFrm.Page1.oPag.oCODCONTO_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCALENDARIO_1_6.visible=!this.oPgFrm.Page1.oPag.oCALENDARIO_1_6.mHide()
    this.oPgFrm.Page1.oPag.oODBCSOURCE_1_7.visible=!this.oPgFrm.Page1.oPag.oODBCSOURCE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oODBCUSR_1_8.visible=!this.oPgFrm.Page1.oPag.oODBCUSR_1_8.mHide()
    this.oPgFrm.Page1.oPag.oODBCPWD_1_9.visible=!this.oPgFrm.Page1.oPag.oODBCPWD_1_9.mHide()
    this.oPgFrm.Page1.oPag.oPRJMOD_1_10.visible=!this.oPgFrm.Page1.oPag.oPRJMOD_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_26.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oPPATHPRJ_1_34.visible=!this.oPgFrm.Page1.oPag.oPPATHPRJ_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gspc_ktp
    * --- if Inlist(cEvent , 'w_SELEZMUL Changed' , 'w_CODCOM Changed')
    * --- this.mCalc(.t.)
    * --- endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page2.oPag.Zoom.Event(cEvent)
        if lower(cEvent)==lower("w_CHOICE Changed") or lower(cEvent)==lower("w_SELEZMUL Changed")
          .Calculate_ELTYIEGLDY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_XNBGLZKNXX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_SELTIP Changed")
          .Calculate_MFNZHGNNST()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
        if lower(cEvent)==lower("Cambio Riga")
          .Calculate_KBPSNYYJHZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_15.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_16.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_17.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gspc_ktp
    this.oPgfrm.Pages(2).Enabled = Empty(this.w_CODCOM) And this.w_SELEZMUL='S'
    
        if cEvent = 'w_zoom after query' and this.w_zoom.grd.ColumnCount > 0
            local n
    
                && Aggiungo seconda selezione per velocizzare l'assegnamento dei valori della combo
                n="3"
                this.w_zoom.grd.Column&n..Visible=.t.
                this.w_zoom.grd.Column&n..Width=16
                this.w_zoom.grd.Column&n..ControlSource='cnselezi'
                this.w_zoom.grd.Column&n..AddObject('cnselezi','z_zzboxcheckbox')
                this.w_zoom.grd.Column&n..cnselezi.visible=.t.
                this.w_zoom.grd.Column&n..ReadOnly=.f.
                this.w_zoom.grd.Column&n..sparse=.f.
                this.w_zoom.grd.Column&n..CurrentControl='cnselezi'
                if this.w_CHOICE='I'
                  this.w_zoom.grd.Column&n..Visible=.f.
                endif
    
                && Aggiungo la combo
                n="4"
                this.w_zoom.grd.Column&n..Visible=.t.
                this.w_zoom.grd.Column&n..Width=130
                this.w_zoom.grd.Column&n..ControlSource='cntipope'
                this.w_zoom.grd.Column&n..AddObject('cntipope','zCommCombobox')
                this.w_zoom.grd.Column&n..cntipope.visible=.t.
                this.w_zoom.grd.Column&n..ReadOnly=.f.
                this.w_zoom.grd.Column&n..sparse=.f.
                this.w_zoom.grd.Column&n..CurrentControl='cntipope'
                if this.w_CHOICE='I'
                  this.w_zoom.grd.Column&n..Visible=.f.
               endif
                this.w_zoom.grd.Refresh()
            endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RDPARDEF
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RDPARDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RDPARDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDPROGR,PDODBCPJ,PDPRJUSR,PDPRJPWD,PDUNIQUE,PDPRJREL,PDPRJMOD";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_RDPARDEF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_RDPARDEF)
            select PDCHIAVE,PDPROGR,PDODBCPJ,PDPRJUSR,PDPRJPWD,PDUNIQUE,PDPRJREL,PDPRJMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RDPARDEF = NVL(_Link_.PDCHIAVE,space(10))
      this.w_PROGR = NVL(_Link_.PDPROGR,space(1))
      this.w_ODBCSOURCE = NVL(_Link_.PDODBCPJ,space(50))
      this.w_ODBCUSR = NVL(_Link_.PDPRJUSR,space(50))
      this.w_ODBCPWD = NVL(_Link_.PDPRJPWD,space(50))
      this.w_UNIQUE = NVL(_Link_.PDUNIQUE,space(1))
      this.w_PRJREL = NVL(_Link_.PDPRJREL,space(4))
      this.w_PRJMOD = NVL(_Link_.PDPRJMOD,space(200))
    else
      if i_cCtrl<>'Load'
        this.w_RDPARDEF = space(10)
      endif
      this.w_PROGR = space(1)
      this.w_ODBCSOURCE = space(50)
      this.w_ODBCUSR = space(50)
      this.w_ODBCPWD = space(50)
      this.w_UNIQUE = space(1)
      this.w_PRJREL = space(4)
      this.w_PRJMOD = space(200)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RDPARDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_4'),i_cWhere,'',"Commesse",'GSPC_CS.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_START_DFLT = NVL(cp_ToDate(_Link_.CNDATINI),ctod("  /  /  "))
      this.w_FINISH_DFLT = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_START_DFLT = ctod("  /  /  ")
      this.w_FINISH_DFLT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONTO
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODCONTO)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPSTR;
                     ,'ATCODATT',trim(this.w_CODCONTO))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCONTO)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCONTO) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODCONTO_1_5'),i_cWhere,'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPSTR<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODCONTO);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPSTR;
                       ,'ATCODATT',this.w_CODCONTO)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONTO = NVL(_Link_.ATCODATT,space(15))
      this.w_DESCON = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONTO = space(15)
      endif
      this.w_DESCON = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCHOICE_1_3.RadioValue()==this.w_CHOICE)
      this.oPgFrm.Page1.oPag.oCHOICE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_4.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_4.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCONTO_1_5.value==this.w_CODCONTO)
      this.oPgFrm.Page1.oPag.oCODCONTO_1_5.value=this.w_CODCONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCALENDARIO_1_6.value==this.w_CALENDARIO)
      this.oPgFrm.Page1.oPag.oCALENDARIO_1_6.value=this.w_CALENDARIO
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCSOURCE_1_7.value==this.w_ODBCSOURCE)
      this.oPgFrm.Page1.oPag.oODBCSOURCE_1_7.value=this.w_ODBCSOURCE
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCUSR_1_8.value==this.w_ODBCUSR)
      this.oPgFrm.Page1.oPag.oODBCUSR_1_8.value=this.w_ODBCUSR
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCPWD_1_9.value==this.w_ODBCPWD)
      this.oPgFrm.Page1.oPag.oODBCPWD_1_9.value=this.w_ODBCPWD
    endif
    if not(this.oPgFrm.Page1.oPag.oPRJMOD_1_10.value==this.w_PRJMOD)
      this.oPgFrm.Page1.oPag.oPRJMOD_1_10.value=this.w_PRJMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_15.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_15.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_26.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_26.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page3.oPag.oMsg_3_1.value==this.w_Msg)
      this.oPgFrm.Page3.oPag.oMsg_3_1.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_4.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZMUL_1_31.RadioValue()==this.w_SELEZMUL)
      this.oPgFrm.Page1.oPag.oSELEZMUL_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOPERAZ_2_6.RadioValue()==this.w_OPERAZ)
      this.oPgFrm.Page2.oPag.oOPERAZ_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELTIP_2_8.RadioValue()==this.w_SELTIP)
      this.oPgFrm.Page2.oPag.oSELTIP_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHOICE_2_19.RadioValue()==this.w_CHOICE)
      this.oPgFrm.Page2.oPag.oCHOICE_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPATHPRJ_1_34.value==this.w_PPATHPRJ)
      this.oPgFrm.Page1.oPag.oPPATHPRJ_1_34.value=this.w_PPATHPRJ
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CALENDARIO))  and not(.w_CHOICE<>'E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCALENDARIO_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CALENDARIO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ODBCSOURCE))  and not(.w_PRJREL='2003')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODBCSOURCE_1_7.SetFocus()
            i_bnoObbl = !empty(.w_ODBCSOURCE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ODBCUSR))  and not(.w_PRJREL='2003')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODBCUSR_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ODBCUSR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(!EMPTY(alltrim(.w_PPATHPRJ)), DIRECTORY(.w_PPATHPRJ), true))  and not(.w_PRJREL<>'2003' Or .w_CHOICE='E' Or (.w_CHOICE='I' And .w_SELEZMUL<>'S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPATHPRJ_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RDPARDEF = this.w_RDPARDEF
    this.o_PRJREL = this.w_PRJREL
    this.o_CHOICE = this.w_CHOICE
    this.o_CODCOM = this.w_CODCOM
    this.o_COMMESSA = this.w_COMMESSA
    this.o_PPATHPRJ = this.w_PPATHPRJ
    return

enddefine

* --- Define pages as container
define class tgspc_ktpPag1 as StdContainer
  Width  = 532
  height = 278
  stdWidth  = 532
  stdheight = 278
  resizeXpos=423
  resizeYpos=224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCHOICE_1_3 as StdCombo with uid="ELAKMKLWTR",rtseq=3,rtrep=.f.,left=160,top=14,width=307,height=21;
    , HelpContextID = 191634394;
    , cFormVar="w_CHOICE",RowSource=""+"Export verso Project,"+"Import da Project", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHOICE_1_3.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'I',;
    space(1))))
  endfunc
  func oCHOICE_1_3.GetRadio()
    this.Parent.oContained.w_CHOICE = this.RadioValue()
    return .t.
  endfunc

  func oCHOICE_1_3.SetRadio()
    this.Parent.oContained.w_CHOICE=trim(this.Parent.oContained.w_CHOICE)
    this.value = ;
      iif(this.Parent.oContained.w_CHOICE=='E',1,;
      iif(this.Parent.oContained.w_CHOICE=='I',2,;
      0))
  endfunc

  add object oCODCOM_1_4 as StdField with uid="WHKVCNXKCN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 45270234,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=57, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEZMUL<>'S')
    endwith
   endif
  endfunc

  func oCODCOM_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_CODCONTO)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'GSPC_CS.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oCODCONTO_1_5 as StdField with uid="DWPHFVQANP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODCONTO", cQueryName = "CODCONTO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Esame struttura a partire da questo nodo ( vuoto tutta la struttura )",;
    HelpContextID = 239942517,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=83, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPSTR", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODCONTO"

  func oCODCONTO_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODCONTO_1_5.mHide()
    with this.Parent.oContained
      return (.w_CHOICE<>'E')
    endwith
  endfunc

  func oCODCONTO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCONTO_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCONTO_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPSTR)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODCONTO_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODCONTO_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM
    i_obj.ATTIPATT=w_TIPSTR
     i_obj.w_ATCODATT=this.parent.oContained.w_CODCONTO
     i_obj.ecpSave()
  endproc

  add object oCALENDARIO_1_6 as StdField with uid="DAZMQHTXWT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CALENDARIO", cQueryName = "CALENDARIO",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome di calendario da utilizzare in MS-Project",;
    HelpContextID = 197132024,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=160, Top=110, InputMask=replicate('X',50)

  func oCALENDARIO_1_6.mHide()
    with this.Parent.oContained
      return (.w_CHOICE<>'E')
    endwith
  endfunc

  add object oODBCSOURCE_1_7 as StdField with uid="VATXNYKYQO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ODBCSOURCE", cQueryName = "ODBCSOURCE",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Identificatore della fonte dati ODBC",;
    HelpContextID = 260921960,;
   bGlobalFont=.t.,;
    Height=21, Width=326, Left=160, Top=145, InputMask=replicate('X',50)

  func oODBCSOURCE_1_7.mHide()
    with this.Parent.oContained
      return (.w_PRJREL='2003')
    endwith
  endfunc

  add object oODBCUSR_1_8 as StdField with uid="OSDZRGLTKJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ODBCUSR", cQueryName = "ODBCUSR",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Identificatore della fonte dati ODBC",;
    HelpContextID = 206761754,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=160, Top=172, cSayPict="repl('X',40)", cGetPict="repl('X',40)", InputMask=replicate('X',50)

  func oODBCUSR_1_8.mHide()
    with this.Parent.oContained
      return (.w_PRJREL='2003')
    endwith
  endfunc

  add object oODBCPWD_1_9 as StdField with uid="BJTAMGXJXK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ODBCPWD", cQueryName = "ODBCPWD",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Identificatore della fonte dati ODBC",;
    HelpContextID = 123539686,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=160, Top=199, cSayPict="repl('X',40)", cGetPict="repl('X',40)", InputMask=replicate('X',50)

  func oODBCPWD_1_9.mHide()
    with this.Parent.oContained
      return (.w_PRJREL='2003')
    endwith
  endfunc

  add object oPRJMOD_1_10 as StdField with uid="LUJVECPQYL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PRJMOD", cQueryName = "PRJMOD",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso del file utilizzato per l'esportazione/importazione su MSProject",;
    HelpContextID = 195584266,;
   bGlobalFont=.t.,;
    Height=21, Width=326, Left=160, Top=145, InputMask=replicate('X',200)

  func oPRJMOD_1_10.mHide()
    with this.Parent.oContained
      return (.w_PRJREL<>'2003' Or (.w_CHOICE='I' And .w_SELEZMUL='S'))
    endwith
  endfunc


  add object oBtn_1_11 as StdButton with uid="QFYQHNIORB",left=491, top=146, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il file di MSProject";
    , HelpContextID = 186169558;
    , Tabstop=.f.;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .w_PRJMOD=getfile('mp?')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_PRJREL<>'2003' Or (.w_CHOICE='I' And .w_SELEZMUL='S'))
     endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="JLPNSJKRAB",left=427, top=228, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la copia delle strutture selezionate";
    , HelpContextID = 259956458;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        do GSPC_BTP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_SELEZMUL<>'S' And !Empty(.w_CODCOM))
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="MONFZERFBU",left=478, top=228, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 259956458;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCOM_1_15 as StdField with uid="FXJZYHRMLN",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 45211338,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=297, Top=57, InputMask=replicate('X',30)

  add object oDESCON_1_26 as StdField with uid="MQYYBGIIOG",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 28434122,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=297, Top=83, InputMask=replicate('X',30)

  func oDESCON_1_26.mHide()
    with this.Parent.oContained
      return (.w_CHOICE<>'E')
    endwith
  endfunc


  add object oObj_1_29 as cp_setobjprop with uid="HDSNTRRVLW",left=399, top=289, width=103,height=28,;
    caption='ODBCPWD',;
   bGlobalFont=.t.,;
    cObj="w_ODBCPWD",cProp="passwordchar",;
    nPag=1;
    , ToolTipText = "Per criptare la password";
    , HelpContextID = 123539686

  add object oSELEZMUL_1_31 as StdCheck with uid="JCVUGIVGLS",rtseq=21,rtrep=.f.,left=160, top=238, caption="Seleziona pi� commesse",;
    ToolTipText = "Se attivo: abilita la selezione di pi� commesse",;
    HelpContextID = 234861170,;
    cFormVar="w_SELEZMUL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEZMUL_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSELEZMUL_1_31.GetRadio()
    this.Parent.oContained.w_SELEZMUL = this.RadioValue()
    return .t.
  endfunc

  func oSELEZMUL_1_31.SetRadio()
    this.Parent.oContained.w_SELEZMUL=trim(this.Parent.oContained.w_SELEZMUL)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZMUL=='S',1,;
      0)
  endfunc

  func oSELEZMUL_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRJREL='2003')
    endwith
   endif
  endfunc


  add object oObj_1_33 as cp_runprogram with uid="UEJMKMIEYT",left=-3, top=347, width=176,height=20,;
    caption='GSPC1BTP',;
   bGlobalFont=.t.,;
    prg="GSPC1BTP('CAMBIOCOM')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 7208886

  add object oPPATHPRJ_1_34 as StdField with uid="QUARTVKHNU",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PPATHPRJ", cQueryName = "PPATHPRJ",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso in cui sono contenuti i progetti da importare",;
    HelpContextID = 1176256,;
   bGlobalFont=.t.,;
    Height=21, Width=326, Left=160, Top=145, InputMask=replicate('X',200)

  func oPPATHPRJ_1_34.mHide()
    with this.Parent.oContained
      return (.w_PRJREL<>'2003' Or .w_CHOICE='E' Or (.w_CHOICE='I' And .w_SELEZMUL<>'S'))
    endwith
  endfunc

  func oPPATHPRJ_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(!EMPTY(alltrim(.w_PPATHPRJ)), DIRECTORY(.w_PPATHPRJ), true))
    endwith
    return bRes
  endfunc


  add object oBtn_1_35 as StdButton with uid="XUMXKKPAKR",left=491, top=146, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il percorso di destinazione";
    , HelpContextID = 186169558;
    , Tabstop=.f.;
  , bGlobalFont=.t.

    proc oBtn_1_35.Click()
      with this.Parent.oContained
        .w_PPATHPRJ=left(cp_getdir(IIF(EMPTY(.w_PPATHPRJ),sys(5)+sys(2003),.w_PPATHPRJ),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_PRJREL<>'2003' Or .w_CHOICE='E' Or (.w_CHOICE='I' And .w_SELEZMUL<>'S'))
     endwith
    endif
  endfunc

  add object oStr_1_14 as StdString with uid="HMJFYXGGXQ",Visible=.t., Left=66, Top=57,;
    Alignment=1, Width=91, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QRVXMHZWPJ",Visible=.t., Left=76, Top=113,;
    Alignment=1, Width=81, Height=15,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_CHOICE<>'E')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="OACPYFOYYW",Visible=.t., Left=36, Top=14,;
    Alignment=1, Width=121, Height=15,;
    Caption="Tipo di collegamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="NAVKMLMSFV",Visible=.t., Left=22, Top=148,;
    Alignment=1, Width=135, Height=15,;
    Caption="Fonte dati ODBC:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_PRJREL='2003')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="MVNIQQDTBE",Visible=.t., Left=88, Top=85,;
    Alignment=1, Width=69, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.w_CHOICE<>'E')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="PDMGYTSENR",Visible=.t., Left=60, Top=174,;
    Alignment=1, Width=97, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_PRJREL='2003')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="SGMVGDOIED",Visible=.t., Left=60, Top=201,;
    Alignment=1, Width=97, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_PRJREL='2003')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="LGHGCUVDOV",Visible=.t., Left=3, Top=148,;
    Alignment=1, Width=154, Height=18,;
    Caption="Modello MSProject:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_PRJREL<>'2003' Or (.w_CHOICE='I' And .w_SELEZMUL='S'))
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="BQPXSTBRUO",Visible=.t., Left=3, Top=148,;
    Alignment=1, Width=154, Height=18,;
    Caption="Progetti MSProject"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_PRJREL<>'2003' Or .w_CHOICE='E' Or (.w_CHOICE='I' And .w_SELEZMUL<>'S'))
    endwith
  endfunc

  add object oBox_1_19 as StdBox with uid="XEMYZISYWE",left=4, top=45, width=527,height=1
enddefine
define class tgspc_ktpPag2 as StdContainer
  Width  = 532
  height = 278
  stdWidth  = 532
  stdheight = 278
  resizeXpos=477
  resizeYpos=142
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Zoom as cp_szoomboxcom with uid="LCFYUHZQGF",left=0, top=38, width=530,height=188,;
    caption='Zoom',;
   bGlobalFont=.t.,;
    cZoomFile="GSPC_KTP",bOptions=.t.,bAdvOptions=.f.,cTable="CAN_TIER",bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bReadOnly=.t.,;
    cEvent = "w_CHOICE Changed, Aggiorna",;
    nPag=2;
    , HelpContextID = 193596566


  add object oBtn_2_3 as StdButton with uid="QPKNGYSWWY",left=482, top=228, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eseguire la copia delle strutture selezionate";
    , HelpContextID = 259956458;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        do GSPC_BTP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_2_4 as StdRadio with uid="XBGVNIVMJH",rtseq=20,rtrep=.f.,left=8, top=230, width=127,height=35;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 100683226
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 100683226
      this.Buttons(2).Top=16
      this.SetAll("Width",125)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_4.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_4.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_4.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  add object oOPERAZ_2_6 as StdRadio with uid="INAUENMVMS",rtseq=22,rtrep=.f.,left=306, top=230, width=144,height=49;
    , cFormVar="w_OPERAZ", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oOPERAZ_2_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sovrascrivi"
      this.Buttons(1).HelpContextID = 109294362
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Tempificazione"
      this.Buttons(2).HelpContextID = 109294362
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Nessuna operazione"
      this.Buttons(3).HelpContextID = 109294362
      this.Buttons(3).Top=30
      this.SetAll("Width",142)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oOPERAZ_2_6.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"T",;
    iif(this.value =3,"N",;
    space(1)))))
  endfunc
  func oOPERAZ_2_6.GetRadio()
    this.Parent.oContained.w_OPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oOPERAZ_2_6.SetRadio()
    this.Parent.oContained.w_OPERAZ=trim(this.Parent.oContained.w_OPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_OPERAZ=="S",1,;
      iif(this.Parent.oContained.w_OPERAZ=="T",2,;
      iif(this.Parent.oContained.w_OPERAZ=="N",3,;
      0)))
  endfunc

  func oOPERAZ_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHOICE='E')
    endwith
   endif
  endfunc


  add object oBtn_2_7 as StdButton with uid="GUUVBASEKE",left=455, top=232, width=20,height=20,;
    CpPicture="bmp\verde.bmp", caption="", nPag=2;
    , ToolTipText = "Aggiorna il tipo di operazione per le commesse selezionate";
    , HelpContextID = 186169558;
  , bGlobalFont=.t.

    proc oBtn_2_7.Click()
      with this.Parent.oContained
        GSPC1BTP(this.Parent.oContained,"AggOpe")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CHOICE='E')
      endwith
    endif
  endfunc

  add object oSELTIP_2_8 as StdRadio with uid="KXZQOSEVCK",rtseq=23,rtrep=.f.,left=154, top=230, width=147,height=35;
    , cFormVar="w_SELTIP", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELTIP_2_8.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Selez. tutti i tipi"
      this.Buttons(1).HelpContextID = 85466
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deselez.tutti i tipi"
      this.Buttons(2).HelpContextID = 85466
      this.Buttons(2).Top=16
      this.SetAll("Width",145)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELTIP_2_8.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELTIP_2_8.GetRadio()
    this.Parent.oContained.w_SELTIP = this.RadioValue()
    return .t.
  endfunc

  func oSELTIP_2_8.SetRadio()
    this.Parent.oContained.w_SELTIP=trim(this.Parent.oContained.w_SELTIP)
    this.value = ;
      iif(this.Parent.oContained.w_SELTIP=="S",1,;
      iif(this.Parent.oContained.w_SELTIP=="D",2,;
      0))
  endfunc

  func oSELTIP_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHOICE='E')
    endwith
   endif
  endfunc


  add object oObj_2_15 as cp_runprogram with uid="SOHQOESVCE",left=189, top=299, width=251,height=19,;
    caption='GSPC1BTP(ZRC)',;
   bGlobalFont=.t.,;
    prg="GSPC1BTP('ZRC')",;
    cEvent = "w_zoom row checked",;
    nPag=2;
    , HelpContextID = 54950966


  add object oObj_2_16 as cp_runprogram with uid="DFZYZGJBJD",left=189, top=317, width=251,height=19,;
    caption='GSPC1BTP(ZRU)',;
   bGlobalFont=.t.,;
    prg="GSPC1BTP('ZRU')",;
    cEvent = "w_zoom row unchecked",;
    nPag=2;
    , HelpContextID = 56130614


  add object oObj_2_17 as cp_runprogram with uid="RLKSEBVOJY",left=189, top=335, width=298,height=19,;
    caption='GSPC1BTP(TRC)',;
   bGlobalFont=.t.,;
    prg="GSPC1BTP('TRC')",;
    cEvent = "w_zoom cnselezi row checked",;
    nPag=2;
    , HelpContextID = 54949430


  add object oObj_2_18 as cp_runprogram with uid="YAZKAGQTAR",left=189, top=353, width=298,height=19,;
    caption='GSPC1BTP(TRU)',;
   bGlobalFont=.t.,;
    prg="GSPC1BTP('TRU')",;
    cEvent = "w_zoom cnselezi row unchecked",;
    nPag=2;
    , HelpContextID = 56129078


  add object oCHOICE_2_19 as StdCombo with uid="YDZTFAYSZU",rtseq=26,rtrep=.f.,left=143,top=14,width=307,height=21;
    , HelpContextID = 191634394;
    , cFormVar="w_CHOICE",RowSource=""+"Export verso Project,"+"Import da Project", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCHOICE_2_19.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'I',;
    space(1))))
  endfunc
  func oCHOICE_2_19.GetRadio()
    this.Parent.oContained.w_CHOICE = this.RadioValue()
    return .t.
  endfunc

  func oCHOICE_2_19.SetRadio()
    this.Parent.oContained.w_CHOICE=trim(this.Parent.oContained.w_CHOICE)
    this.value = ;
      iif(this.Parent.oContained.w_CHOICE=='E',1,;
      iif(this.Parent.oContained.w_CHOICE=='I',2,;
      0))
  endfunc

  add object oStr_2_20 as StdString with uid="GDIVRWBEAX",Visible=.t., Left=19, Top=14,;
    Alignment=1, Width=121, Height=15,;
    Caption="Tipo di collegamento:"  ;
  , bGlobalFont=.t.

  add object oBox_2_9 as StdBox with uid="OETZICBJBO",left=149, top=228, width=330,height=50

  add object oBox_2_10 as StdBox with uid="BTYMSLMRHE",left=2, top=228, width=143,height=50
enddefine
define class tgspc_ktpPag3 as StdContainer
  Width  = 532
  height = 278
  stdWidth  = 532
  stdheight = 278
  resizeXpos=363
  resizeYpos=200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMsg_3_1 as StdMemo with uid="CANOUVKXSC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 3, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 186421190,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=271, Width=530, Left=0, Top=5, tabstop = .f., readonly = .t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_ktp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gspc_ktp
Define class zCommCombobox as combobox

  RowSourceType = 1
  FontName   = "Tahoma"
  FontSize   = 8
  FontBold   = .f.
  FontItalic = .t.
  FontUnderline  = .f.
  FontStrikeThru = .f.
  value=2
  BorderStyle = 0
  style=2
  cRowSource=""+"Sovrascrivi,"+"Agg. legami e tempi,"+"Nessuna operazione"
  bInGrid=.f.
  nSeconds=0
  visible=.t.
  cFile=''
  dimension xKey[1]
  nSeconds=0
  bInGrid=.f.
  nIdx=0
  cCurInfo=""

   Procedure init
      IF VARTYPE(this.bNoBackColor)='U'
       This.backcolor=i_nEBackColor
      endif
      this.bInGrid=.t.
      if .t. or type('i_cLanguage')<>'U' and !empty(i_cLanguage)
        local i,j,s,o
        o=this.crowsource
        s=''
        i=at(',',o)
        do while i<>0
          *s=s+','+cp_Translate(left(o,i-1))
          this.AddItem(cp_Translate(left(o,i-1)))
          o=substr(o,i+1)
          i=at(',',o)
        enddo
        *s=s+','+cp_Translate(o)
        *this.RowSource=substr(s,2)
        this.AddItem(cp_Translate(o))
      endif
        this.StatusBarText=Padr(this.ToolTipText,100)
      DoDefault()
     EndProc
     proc GotFocus()
      Local ofrm
      ofrm=this.parent.parent.parent.parent.oContained
      ofrm.__dummy__.enabled=.t.
      ofrm.__dummy__.SetFocus()
      ofrm.__dummy__.enabled=.f.
      Dodefault()
     EndProc
     Proc GotFocus()
      Local ofrm
      ofrm=this.parent.parent.parent.parent.oContained
      ofrm.w_CBOSEL=.t.
      Dodefault()
     EndProc
enddefine


define class z_zzboxcheckbox as zzboxcheckbox
  proc click()
    local c_mCursor
    if type('this.parent.parent.parent.parent.oContained')='O'
      this.MultipleSelection()
      if this.value=1
        * --- lasciato per compatibilita'
        * --- this.parent.parent.parent.parent.oContained.NotifyEvent(this.parent.parent.parent.name+' row checked')
        * --- forma corretta
        * --- this.parent.parent.parent.parent.oContained.NotifyEvent('w_'+lower(this.parent.parent.parent.name)+' row checked')

        this.parent.parent.parent.parent.oContained.NotifyEvent('w_'+lower(this.parent.parent.parent.name)+' '+lower(alltrim(this.controlsource)) +' row checked')
      else
        * --- lasciato per compatibilita'
        * --- this.parent.parent.parent.parent.oContained.NotifyEvent(this.parent.parent.parent.name+' row unchecked')
        * --- forma corretta
        * --- this.parent.parent.parent.parent.oContained.NotifyEvent('w_'+lower(this.parent.parent.parent.name)+' row unchecked')

        this.parent.parent.parent.parent.oContained.NotifyEvent('w_'+lower(this.parent.parent.parent.name)+' '+lower(alltrim(this.controlsource)) +' row unchecked')
      endif
      c_mCursor=this.parent.parent.parent.cCursor
      select (c_mCursor)
      this.nFirstSelect=recno()
      this.nLastValue=this.Value
    endif
enddefine

define class cp_szoomboxcom as cp_szoombox

  Proc Grd.BeforeRowColChange(nIdx)
    *ACTIVATE SCREEN
    *? 'before',nIdx,RECNO(),this.nAbsRow
    return
  proc Grd.AfterRowColChange(nColIndex)
    *ACTIVATE SCREEN
    *? 'after',nIdx,RECNO(),this.nAbsRow

    LOCAL obj, l_ColActive

    l_ColActive=alltrim(str(this.ActiveColumn))
    thisform.lockscreen=.t.
       obj = this.Parent.Parent.Parent.Parent.Parent
       obj.NotifyEvent('Cambio Riga')
    IF l_ColActive="5"
      obj.__dummy__.Enabled = .t.
      obj.__dummy__.SetFocus()
      obj.__dummy__.Enabled = .f.

       IF ! obj.w_CBOSEL
        this.Refresh()
        this.Column4.cntipope.SetFocus()
       ELSE
        this.SetFocus()
        this.Refresh()
      endif
     endif
      obj.w_CBOSEL=.F.
      thisform.lockscreen=.f.
      DODEFAULT()
    Return

enddefine
* --- Fine Area Manuale
