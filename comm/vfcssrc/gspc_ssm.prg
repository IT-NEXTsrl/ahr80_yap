* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_ssm                                                        *
*              Stampa schede movimenti                                         *
*                                                                              *
*      Author: Paolo Saitti                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_169]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-20                                                      *
* Last revis.: 2008-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_ssm",oParentObject))

* --- Class definition
define class tgspc_ssm as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 519
  Height = 418
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-19"
  HelpContextID=177579927
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  ATTIVITA_IDX = 0
  TIPCOSTO_IDX = 0
  MAGAZZIN_IDX = 0
  CPAR_DEF_IDX = 0
  cPrg = "gspc_ssm"
  cComment = "Stampa schede movimenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READPAR = space(10)
  w_CODCOM1 = space(15)
  o_CODCOM1 = space(15)
  w_CODCOM2 = space(15)
  o_CODCOM2 = space(15)
  w_CODATT1 = space(15)
  o_CODATT1 = space(15)
  w_CODATT2 = space(15)
  w_CODCOS1 = space(5)
  o_CODCOS1 = space(5)
  w_CODCOS2 = space(5)
  w_CODMAG1 = space(5)
  o_CODMAG1 = space(5)
  w_CODMAG2 = space(5)
  w_DATAINI = ctod('  /  /  ')
  o_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_TIPMOV = space(1)
  o_TIPMOV = space(1)
  w_DESCOM1 = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESAT1 = space(30)
  w_TIPATT = space(10)
  w_DESAT2 = space(30)
  w_CODVAL = space(3)
  w_DECTOT = 0
  w_DECTOT1 = 0
  o_DECTOT1 = 0
  w_DESCOS1 = space(30)
  w_DESCOS2 = space(30)
  w_DESMAG1 = space(30)
  w_DESMAG2 = space(30)
  w_TECNICO = .F.
  w_VALSTAMPA = space(3)
  w_DECSTAMPA = 0
  w_DESCOM2 = space(30)
  w_CODVAL1 = space(3)
  w_CODVAL2 = space(3)
  w_DEFCOM = space(15)
  w_ANACOSTI = space(1)
  w_ANARICAV = space(1)
  w_TIPRIC = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_ssmPag1","gspc_ssm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='ATTIVITA'
    this.cWorkTables[4]='TIPCOSTO'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='CPAR_DEF'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gspc_ssm
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR=space(10)
      .w_CODCOM1=space(15)
      .w_CODCOM2=space(15)
      .w_CODATT1=space(15)
      .w_CODATT2=space(15)
      .w_CODCOS1=space(5)
      .w_CODCOS2=space(5)
      .w_CODMAG1=space(5)
      .w_CODMAG2=space(5)
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_TIPMOV=space(1)
      .w_DESCOM1=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESAT1=space(30)
      .w_TIPATT=space(10)
      .w_DESAT2=space(30)
      .w_CODVAL=space(3)
      .w_DECTOT=0
      .w_DECTOT1=0
      .w_DESCOS1=space(30)
      .w_DESCOS2=space(30)
      .w_DESMAG1=space(30)
      .w_DESMAG2=space(30)
      .w_TECNICO=.f.
      .w_VALSTAMPA=space(3)
      .w_DECSTAMPA=0
      .w_DESCOM2=space(30)
      .w_CODVAL1=space(3)
      .w_CODVAL2=space(3)
      .w_DEFCOM=space(15)
      .w_ANACOSTI=space(1)
      .w_ANARICAV=space(1)
      .w_TIPRIC=space(1)
        .w_READPAR = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
          .link_1_1('Full')
        endif
        .w_CODCOM1 = .w_DEFCOM
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCOM1))
          .link_1_2('Full')
        endif
        .w_CODCOM2 = .w_CODCOM1
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODCOM2))
          .link_1_3('Full')
        endif
        .w_CODATT1 = " "
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODATT1))
          .link_1_4('Full')
        endif
        .w_CODATT2 = .w_CODATT1
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODATT2))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODCOS1))
          .link_1_6('Full')
        endif
        .w_CODCOS2 = .w_CODCOS1
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODCOS2))
          .link_1_7('Full')
        endif
        .w_CODMAG1 = ' '
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODMAG1))
          .link_1_8('Full')
        endif
        .w_CODMAG2 = .w_CODMAG1
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODMAG2))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,10,.f.)
        .w_DATAFIN = iif(empty(.w_DATAINI), i_DATSYS, .w_DATAINI)
        .w_TIPMOV = 'T'
          .DoRTCalc(13,13,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(15,16,.f.)
        .w_TIPATT = 'A'
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_CODVAL))
          .link_1_22('Full')
        endif
        .w_DECTOT = .w_DECTOT1+1
          .DoRTCalc(21,25,.f.)
        .w_TECNICO = .t.
        .w_VALSTAMPA = g_PERVAL
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_VALSTAMPA))
          .link_1_36('Full')
        endif
        .DoRTCalc(28,30,.f.)
        if not(empty(.w_CODVAL1))
          .link_1_42('Full')
        endif
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_CODVAL2))
          .link_1_43('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
          .DoRTCalc(32,32,.f.)
        .w_ANACOSTI = "S"
        .w_ANARICAV = "S"
        .w_TIPRIC = "R"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_CODCOM1<>.w_CODCOM1
            .w_CODCOM2 = .w_CODCOM1
          .link_1_3('Full')
        endif
        if .o_CODCOM1<>.w_CODCOM1.or. .o_CODCOM2<>.w_CODCOM2
            .w_CODATT1 = " "
          .link_1_4('Full')
        endif
        if .o_CODCOM1<>.w_CODCOM1.or. .o_CODCOM2<>.w_CODCOM2.or. .o_CODATT1<>.w_CODATT1
            .w_CODATT2 = .w_CODATT1
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.t.)
        if .o_CODCOS1<>.w_CODCOS1
            .w_CODCOS2 = .w_CODCOS1
          .link_1_7('Full')
        endif
        if .o_TIPMOV<>.w_TIPMOV
            .w_CODMAG1 = ' '
          .link_1_8('Full')
        endif
        if .o_CODMAG1<>.w_CODMAG1
            .w_CODMAG2 = .w_CODMAG1
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.t.)
        if .o_DATAINI<>.w_DATAINI
            .w_DATAFIN = iif(empty(.w_DATAINI), i_DATSYS, .w_DATAINI)
        endif
        .DoRTCalc(12,18,.t.)
          .link_1_22('Full')
        if .o_DECTOT1<>.w_DECTOT1
            .w_DECTOT = .w_DECTOT1+1
        endif
        .DoRTCalc(21,26,.t.)
        if .o_CODCOM1<>.w_CODCOM1.or. .o_CODCOM2<>.w_CODCOM2
            .w_VALSTAMPA = g_PERVAL
          .link_1_36('Full')
        endif
        .DoRTCalc(28,29,.t.)
          .link_1_42('Full')
          .link_1_43('Full')
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(32,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODATT1_1_4.enabled = this.oPgFrm.Page1.oPag.oCODATT1_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCODATT2_1_5.enabled = this.oPgFrm.Page1.oPag.oCODATT2_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODMAG1_1_8.enabled = this.oPgFrm.Page1.oPag.oCODMAG1_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCODMAG2_1_9.enabled = this.oPgFrm.Page1.oPag.oCODMAG2_1_9.mCond()
    this.oPgFrm.Page1.oPag.oTIPMOV_1_12.enabled = this.oPgFrm.Page1.oPag.oTIPMOV_1_12.mCond()
    this.oPgFrm.Page1.oPag.oVALSTAMPA_1_36.enabled = this.oPgFrm.Page1.oPag.oVALSTAMPA_1_36.mCond()
    this.oPgFrm.Page1.oPag.oTIPRIC_1_50.enabled = this.oPgFrm.Page1.oPag.oTIPRIC_1_50.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM1)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM1))
          select CNCODCAN,CNDESCAN,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM1)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM1) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM1_1_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM1)
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM1 = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM1 = NVL(_Link_.CNDESCAN,space(30))
      this.w_CODVAL1 = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM1 = space(15)
      endif
      this.w_DESCOM1 = space(30)
      this.w_CODVAL1 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOM1 <= .w_CODCOM2 or empty(.w_CODCOM2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOM1 = space(15)
        this.w_DESCOM1 = space(30)
        this.w_CODVAL1 = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM2
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM2)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM2))
          select CNCODCAN,CNDESCAN,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM2)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM2) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM2_1_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM2)
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM2 = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM2 = NVL(_Link_.CNDESCAN,space(30))
      this.w_CODVAL2 = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM2 = space(15)
      endif
      this.w_DESCOM2 = space(30)
      this.w_CODVAL2 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOM1 <= .w_CODCOM2 or empty(.w_CODCOM1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOM2 = space(15)
        this.w_DESCOM2 = space(30)
        this.w_CODVAL2 = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT1
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT1)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM1;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT1))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT1)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT1)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT1)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT1) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT1_1_4'),i_cWhere,'',"Elenco attivita",'GSPC_AS.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM1<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT1);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM1;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT1)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT1 = NVL(_Link_.ATCODATT,space(15))
      this.w_DESAT1 = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT1 = space(15)
      endif
      this.w_DESAT1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODATT1 <= .w_CODATT2 or empty(.w_CODATT2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODATT1 = space(15)
        this.w_DESAT1 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT2
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT2)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM1;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT2))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT2)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT2)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT2)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT2) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT2_1_5'),i_cWhere,'GSPC_BZZ',"Elenco attivita",'GSPC_AS.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM1<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT2);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM1;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT2)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT2 = NVL(_Link_.ATCODATT,space(15))
      this.w_DESAT2 = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT2 = space(15)
      endif
      this.w_DESAT2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODATT1 <= .w_CODATT2 or empty(.w_CODATT1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODATT2 = space(15)
        this.w_DESAT2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOS1
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_lTable = "TIPCOSTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2], .t., this.TIPCOSTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIPCOSTO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CODCOS1)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CODCOS1))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOS1)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStrODBC(trim(this.w_CODCOS1)+"%");

            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStr(trim(this.w_CODCOS1)+"%");

            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOS1) and !this.bDontReportError
            deferred_cp_zoom('TIPCOSTO','*','TCCODICE',cp_AbsName(oSource.parent,'oCODCOS1_1_6'),i_cWhere,'',"Codici costo",'gspc_tc.TIPCOSTO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCOS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCOS1)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOS1 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOS1 = NVL(_Link_.TCDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOS1 = space(5)
      endif
      this.w_DESCOS1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOS1<=.w_CODCOS2 or Empty (.w_CODCOS2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOS1 = space(5)
        this.w_DESCOS1 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCOSTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOS2
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_lTable = "TIPCOSTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2], .t., this.TIPCOSTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIPCOSTO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CODCOS2)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CODCOS2))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOS2)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStrODBC(trim(this.w_CODCOS2)+"%");

            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStr(trim(this.w_CODCOS2)+"%");

            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOS2) and !this.bDontReportError
            deferred_cp_zoom('TIPCOSTO','*','TCCODICE',cp_AbsName(oSource.parent,'oCODCOS2_1_7'),i_cWhere,'',"Codici costo",'gspc_tc.TIPCOSTO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCOS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCOS2)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOS2 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOS2 = NVL(_Link_.TCDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOS2 = space(5)
      endif
      this.w_DESCOS2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOS1<=.w_CODCOS2 or Empty (.w_CODCOS1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOS2 = space(5)
        this.w_DESCOS2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCOSTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG1
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG1)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG1))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG1)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG1)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG1)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG1) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG1_1_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG1)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG1 = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG1 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG1 = space(5)
      endif
      this.w_DESMAG1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODMAG1<=.w_CODMAG2 or Empty (.w_CODMAG2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODMAG1 = space(5)
        this.w_DESMAG1 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG2
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG2)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG2))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG2)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG2)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG2)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG2) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG2_1_9'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG2)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG2 = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG2 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG2 = space(5)
      endif
      this.w_DESMAG2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODMAG1<=.w_CODMAG2 or Empty (.w_CODMAG1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODMAG2 = space(5)
        this.w_DESMAG2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT1 = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DECTOT1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALSTAMPA
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALSTAMPA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALSTAMPA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALSTAMPA))
          select VACODVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALSTAMPA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VALSTAMPA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALSTAMPA_1_36'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALSTAMPA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALSTAMPA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALSTAMPA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALSTAMPA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECSTAMPA = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALSTAMPA = space(3)
      endif
      this.w_DECSTAMPA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALSTAMPA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL1
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL1)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL1 = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT1 = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL1 = space(3)
      endif
      this.w_DECTOT1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL2
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL2)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL2 = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT1 = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL2 = space(3)
      endif
      this.w_DECTOT1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM1_1_2.value==this.w_CODCOM1)
      this.oPgFrm.Page1.oPag.oCODCOM1_1_2.value=this.w_CODCOM1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM2_1_3.value==this.w_CODCOM2)
      this.oPgFrm.Page1.oPag.oCODCOM2_1_3.value=this.w_CODCOM2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT1_1_4.value==this.w_CODATT1)
      this.oPgFrm.Page1.oPag.oCODATT1_1_4.value=this.w_CODATT1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT2_1_5.value==this.w_CODATT2)
      this.oPgFrm.Page1.oPag.oCODATT2_1_5.value=this.w_CODATT2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOS1_1_6.value==this.w_CODCOS1)
      this.oPgFrm.Page1.oPag.oCODCOS1_1_6.value=this.w_CODCOS1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOS2_1_7.value==this.w_CODCOS2)
      this.oPgFrm.Page1.oPag.oCODCOS2_1_7.value=this.w_CODCOS2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG1_1_8.value==this.w_CODMAG1)
      this.oPgFrm.Page1.oPag.oCODMAG1_1_8.value=this.w_CODMAG1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG2_1_9.value==this.w_CODMAG2)
      this.oPgFrm.Page1.oPag.oCODMAG2_1_9.value=this.w_CODMAG2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_10.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_10.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_11.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_11.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPMOV_1_12.RadioValue()==this.w_TIPMOV)
      this.oPgFrm.Page1.oPag.oTIPMOV_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM1_1_14.value==this.w_DESCOM1)
      this.oPgFrm.Page1.oPag.oDESCOM1_1_14.value=this.w_DESCOM1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAT1_1_18.value==this.w_DESAT1)
      this.oPgFrm.Page1.oPag.oDESAT1_1_18.value=this.w_DESAT1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAT2_1_21.value==this.w_DESAT2)
      this.oPgFrm.Page1.oPag.oDESAT2_1_21.value=this.w_DESAT2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOS1_1_28.value==this.w_DESCOS1)
      this.oPgFrm.Page1.oPag.oDESCOS1_1_28.value=this.w_DESCOS1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOS2_1_30.value==this.w_DESCOS2)
      this.oPgFrm.Page1.oPag.oDESCOS2_1_30.value=this.w_DESCOS2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG1_1_32.value==this.w_DESMAG1)
      this.oPgFrm.Page1.oPag.oDESMAG1_1_32.value=this.w_DESMAG1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG2_1_34.value==this.w_DESMAG2)
      this.oPgFrm.Page1.oPag.oDESMAG2_1_34.value=this.w_DESMAG2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALSTAMPA_1_36.RadioValue()==this.w_VALSTAMPA)
      this.oPgFrm.Page1.oPag.oVALSTAMPA_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM2_1_40.value==this.w_DESCOM2)
      this.oPgFrm.Page1.oPag.oDESCOM2_1_40.value=this.w_DESCOM2
    endif
    if not(this.oPgFrm.Page1.oPag.oANACOSTI_1_48.RadioValue()==this.w_ANACOSTI)
      this.oPgFrm.Page1.oPag.oANACOSTI_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANARICAV_1_49.RadioValue()==this.w_ANARICAV)
      this.oPgFrm.Page1.oPag.oANARICAV_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRIC_1_50.RadioValue()==this.w_TIPRIC)
      this.oPgFrm.Page1.oPag.oTIPRIC_1_50.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODCOM1 <= .w_CODCOM2 or empty(.w_CODCOM2))  and not(empty(.w_CODCOM1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODCOM1 <= .w_CODCOM2 or empty(.w_CODCOM1))  and not(empty(.w_CODCOM2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM2_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODATT1 <= .w_CODATT2 or empty(.w_CODATT2))  and (!empty(nvl(.w_CODCOM1,"")) and !empty(nvl(.w_CODCOM2,"")) and .w_CODCOM1=.w_CODCOM2)  and not(empty(.w_CODATT1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATT1_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODATT1 <= .w_CODATT2 or empty(.w_CODATT1))  and (!empty(nvl(.w_CODCOM1,"")) and !empty(nvl(.w_CODCOM2,"")) and .w_CODCOM1=.w_CODCOM2)  and not(empty(.w_CODATT2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATT2_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODCOS1<=.w_CODCOS2 or Empty (.w_CODCOS2))  and not(empty(.w_CODCOS1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOS1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODCOS1<=.w_CODCOS2 or Empty (.w_CODCOS1))  and not(empty(.w_CODCOS2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOS2_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODMAG1<=.w_CODMAG2 or Empty (.w_CODMAG2))  and (.w_TIPMOV<>'P')  and not(empty(.w_CODMAG1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG1_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODMAG1<=.w_CODMAG2 or Empty (.w_CODMAG1))  and (.w_TIPMOV<>'P')  and not(empty(.w_CODMAG2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG2_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATAINI<=.w_DATAFIN or empty(.w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATAFIN)) or not(.w_DATAINI<=.w_DATAFIN or empty(.w_DATAINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFIN_1_11.SetFocus()
            i_bnoObbl = !empty(.w_DATAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODCOM1 = this.w_CODCOM1
    this.o_CODCOM2 = this.w_CODCOM2
    this.o_CODATT1 = this.w_CODATT1
    this.o_CODCOS1 = this.w_CODCOS1
    this.o_CODMAG1 = this.w_CODMAG1
    this.o_DATAINI = this.w_DATAINI
    this.o_TIPMOV = this.w_TIPMOV
    this.o_DECTOT1 = this.w_DECTOT1
    return

enddefine

* --- Define pages as container
define class tgspc_ssmPag1 as StdContainer
  Width  = 515
  height = 418
  stdWidth  = 515
  stdheight = 418
  resizeXpos=336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM1_1_2 as StdField with uid="NPPVNNMOBE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCOM1", cQueryName = "CODCOM1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 53658842,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=8, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM1"

  func oCODCOM1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
      if .not. empty(.w_CODATT1)
        bRes2=.link_1_4('Full')
      endif
      if .not. empty(.w_CODATT2)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCODCOM2_1_3 as StdField with uid="OQFUNRNKNO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCOM2", cQueryName = "CODCOM2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 53658842,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM2"

  func oCODCOM2_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM2_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM2_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM2_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCODATT1_1_4 as StdField with uid="KWHLRFNCQG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODATT1", cQueryName = "CODATT1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 199541978,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=63, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM1", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT1"

  func oCODATT1_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(nvl(.w_CODCOM1,"")) and !empty(nvl(.w_CODCOM2,"")) and .w_CODCOM1=.w_CODCOM2)
    endwith
   endif
  endfunc

  func oCODATT1_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT1_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT1_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM1)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM1)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT1_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco attivita",'GSPC_AS.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oCODATT2_1_5 as StdField with uid="AALUBUIXQP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODATT2", cQueryName = "CODATT2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 199541978,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=87, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM1", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT2"

  func oCODATT2_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(nvl(.w_CODCOM1,"")) and !empty(nvl(.w_CODCOM2,"")) and .w_CODCOM1=.w_CODCOM2)
    endwith
   endif
  endfunc

  func oCODATT2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT2_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT2_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM1)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM1)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT2_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivita",'GSPC_AS.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODATT2_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM1
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT2
     i_obj.ecpSave()
  endproc

  add object oCODCOS1_1_6 as StdField with uid="TCRUHAALEV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODCOS1", cQueryName = "CODCOS1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice costo",;
    HelpContextID = 221431002,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=119, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIPCOSTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODCOS1"

  func oCODCOS1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOS1_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOS1_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPCOSTO','*','TCCODICE',cp_AbsName(this.parent,'oCODCOS1_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici costo",'gspc_tc.TIPCOSTO_VZM',this.parent.oContained
  endproc

  add object oCODCOS2_1_7 as StdField with uid="HKWVPBTHYW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODCOS2", cQueryName = "CODCOS2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice costo",;
    HelpContextID = 221431002,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=143, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIPCOSTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODCOS2"

  func oCODCOS2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOS2_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOS2_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPCOSTO','*','TCCODICE',cp_AbsName(this.parent,'oCODCOS2_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici costo",'gspc_tc.TIPCOSTO_VZM',this.parent.oContained
  endproc

  add object oCODMAG1_1_8 as StdField with uid="DAKQQMHTVE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODMAG1", cQueryName = "CODMAG1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 168346842,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=175, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG1"

  func oCODMAG1_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPMOV<>'P')
    endwith
   endif
  endfunc

  func oCODMAG1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG1_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG1_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG1_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCODMAG2_1_9 as StdField with uid="LTGPDLGHMS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODMAG2", cQueryName = "CODMAG2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 168346842,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=199, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG2"

  func oCODMAG2_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPMOV<>'P')
    endwith
   endif
  endfunc

  func oCODMAG2_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG2_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG2_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG2_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDATAINI_1_10 as StdField with uid="MCZKGEGHJW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 225193270,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=125, Top=231

  func oDATAINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAINI<=.w_DATAFIN or empty(.w_DATAFIN))
    endwith
    return bRes
  endfunc

  add object oDATAFIN_1_11 as StdField with uid="ICEJAMVIIK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 138161462,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=378, Top=231

  func oDATAFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAINI<=.w_DATAFIN or empty(.w_DATAINI))
    endwith
    return bRes
  endfunc


  add object oTIPMOV_1_12 as StdCombo with uid="IWLPMTRSLK",rtseq=12,rtrep=.f.,left=125,top=280,width=128,height=21;
    , ToolTipText = "Tipo movimento";
    , HelpContextID = 170396106;
    , cFormVar="w_TIPMOV",RowSource=""+"Imp. finanz.,"+"Costi preventivi,"+"Costi consuntivi,"+"Tutti i costi,"+"Solo impegni evasi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPMOV_1_12.RadioValue()
    return(iif(this.value =1,"I",;
    iif(this.value =2,"P",;
    iif(this.value =3,"C",;
    iif(this.value =4,"T",;
    iif(this.value =5,"E",;
    space(1)))))))
  endfunc
  func oTIPMOV_1_12.GetRadio()
    this.Parent.oContained.w_TIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oTIPMOV_1_12.SetRadio()
    this.Parent.oContained.w_TIPMOV=trim(this.Parent.oContained.w_TIPMOV)
    this.value = ;
      iif(this.Parent.oContained.w_TIPMOV=="I",1,;
      iif(this.Parent.oContained.w_TIPMOV=="P",2,;
      iif(this.Parent.oContained.w_TIPMOV=="C",3,;
      iif(this.Parent.oContained.w_TIPMOV=="T",4,;
      iif(this.Parent.oContained.w_TIPMOV=="E",5,;
      0)))))
  endfunc

  func oTIPMOV_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANACOSTI="S")
    endwith
   endif
  endfunc

  add object oDESCOM1_1_14 as StdField with uid="ECNMNUQHAS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCOM1", cQueryName = "DESCOM1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 53599946,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=258, Top=8, InputMask=replicate('X',30)

  add object oDESAT1_1_18 as StdField with uid="CLYTLHQTDU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESAT1", cQueryName = "DESAT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 249814730,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=258, Top=63, InputMask=replicate('X',30)

  add object oDESAT2_1_21 as StdField with uid="BMSPVOILUR",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESAT2", cQueryName = "DESAT2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 233037514,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=258, Top=87, InputMask=replicate('X',30)

  add object oDESCOS1_1_28 as StdField with uid="ZUJLQSAFWE",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCOS1", cQueryName = "DESCOS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 221372106,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=258, Top=119, InputMask=replicate('X',30)

  add object oDESCOS2_1_30 as StdField with uid="QFFHZESFHR",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCOS2", cQueryName = "DESCOS2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 221372106,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=258, Top=143, InputMask=replicate('X',30)

  add object oDESMAG1_1_32 as StdField with uid="SVXIERECGG",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESMAG1", cQueryName = "DESMAG1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 168287946,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=258, Top=175, InputMask=replicate('X',30)

  add object oDESMAG2_1_34 as StdField with uid="ZSZXKGHZHY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESMAG2", cQueryName = "DESMAG2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 168287946,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=258, Top=199, InputMask=replicate('X',30)


  add object oVALSTAMPA_1_36 as StdCombo with uid="UEXSZWHZFF",rtseq=27,rtrep=.f.,left=341,top=313,width=161,height=21;
    , DisabledBackColor=rgb(255,255,255);
    , ToolTipText = "Per convertire in Euro gli importi delle commesse in valuta nazionale";
    , HelpContextID = 19772086;
    , cFormVar="w_VALSTAMPA",RowSource=""+"Valuta di commessa,"+"Euro", bObbl = .f. , nPag = 1;
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oVALSTAMPA_1_36.RadioValue()
    return(iif(this.value =1,g_CODLIR,;
    iif(this.value =2,g_CODEUR,;
    space(3))))
  endfunc
  func oVALSTAMPA_1_36.GetRadio()
    this.Parent.oContained.w_VALSTAMPA = this.RadioValue()
    return .t.
  endfunc

  func oVALSTAMPA_1_36.SetRadio()
    this.Parent.oContained.w_VALSTAMPA=trim(this.Parent.oContained.w_VALSTAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_VALSTAMPA==g_CODLIR,1,;
      iif(this.Parent.oContained.w_VALSTAMPA==g_CODEUR,2,;
      0))
  endfunc

  func oVALSTAMPA_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_CODVAL1<>g_CODEUR and .w_CODVAL2<>g_CODEUR and .w_CODCOM1=.w_CODCOM2) and  !empty(nvl(.w_CODCOM1,"")) and !empty(nvl(.w_CODCOM2,"")))
    endwith
   endif
  endfunc

  func oVALSTAMPA_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALSTAMPA_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDESCOM2_1_40 as StdField with uid="PCONZTWPCD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCOM2", cQueryName = "DESCOM2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 53599946,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=258, Top=32, InputMask=replicate('X',30)


  add object oObj_1_44 as cp_outputCombo with uid="ZLERXIVPWT",left=124, top=341, width=384,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181293338


  add object oBtn_1_45 as StdButton with uid="JLPNSJKRAB",left=408, top=367, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la copia delle strutture selezionate";
    , HelpContextID = 217501402;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      with this.Parent.oContained
        do GSPC_BSL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ANACOSTI="S" or .w_ANARICAV="S" and (!empty(nvl(.w_OQRY,"")) and !empty(nvl(.w_OREP,""))))
      endwith
    endif
  endfunc


  add object oBtn_1_46 as StdButton with uid="KOTWJAFPMC",left=460, top=367, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 184897350;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oANACOSTI_1_48 as StdCheck with uid="MMHZLEARAY",rtseq=33,rtrep=.f.,left=17, top=282, caption="Analisi costi",;
    HelpContextID = 221443505,;
    cFormVar="w_ANACOSTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANACOSTI_1_48.RadioValue()
    return(iif(this.value =1,"S",;
    "X"))
  endfunc
  func oANACOSTI_1_48.GetRadio()
    this.Parent.oContained.w_ANACOSTI = this.RadioValue()
    return .t.
  endfunc

  func oANACOSTI_1_48.SetRadio()
    this.Parent.oContained.w_ANACOSTI=trim(this.Parent.oContained.w_ANACOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_ANACOSTI=="S",1,;
      0)
  endfunc

  add object oANARICAV_1_49 as StdCheck with uid="QSBEVVXZDO",rtseq=34,rtrep=.f.,left=258, top=282, caption="Analisi ricavi",;
    HelpContextID = 226751908,;
    cFormVar="w_ANARICAV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANARICAV_1_49.RadioValue()
    return(iif(this.value =1,"S",;
    "X"))
  endfunc
  func oANARICAV_1_49.GetRadio()
    this.Parent.oContained.w_ANARICAV = this.RadioValue()
    return .t.
  endfunc

  func oANARICAV_1_49.SetRadio()
    this.Parent.oContained.w_ANARICAV=trim(this.Parent.oContained.w_ANARICAV)
    this.value = ;
      iif(this.Parent.oContained.w_ANARICAV=="S",1,;
      0)
  endfunc


  add object oTIPRIC_1_50 as StdCombo with uid="PBVHIXOEXY",rtseq=35,rtrep=.f.,left=378,top=280,width=124,height=21;
    , ToolTipText = "Tipo movimento";
    , HelpContextID = 226691530;
    , cFormVar="w_TIPRIC",RowSource=""+"Ricavi residui,"+"Ricavi preventivi,"+"Ricavi consuntivi,"+"Tutti i ricavi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPRIC_1_50.RadioValue()
    return(iif(this.value =1,"A",;
    iif(this.value =2,"O",;
    iif(this.value =3,"F",;
    iif(this.value =4,"R",;
    space(1))))))
  endfunc
  func oTIPRIC_1_50.GetRadio()
    this.Parent.oContained.w_TIPRIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPRIC_1_50.SetRadio()
    this.Parent.oContained.w_TIPRIC=trim(this.Parent.oContained.w_TIPRIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRIC=="A",1,;
      iif(this.Parent.oContained.w_TIPRIC=="O",2,;
      iif(this.Parent.oContained.w_TIPRIC=="F",3,;
      iif(this.Parent.oContained.w_TIPRIC=="R",4,;
      0))))
  endfunc

  func oTIPRIC_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANARICAV="S")
    endwith
   endif
  endfunc

  add object oStr_1_13 as StdString with uid="WWJWWYRUJS",Visible=.t., Left=4, Top=9,;
    Alignment=1, Width=117, Height=18,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ITAEZXOFTT",Visible=.t., Left=4, Top=65,;
    Alignment=1, Width=117, Height=15,;
    Caption="Da attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="BLCRNRLJFB",Visible=.t., Left=4, Top=89,;
    Alignment=1, Width=117, Height=15,;
    Caption="A attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="MDMDOJWGBQ",Visible=.t., Left=4, Top=231,;
    Alignment=1, Width=117, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="HAOXVUXIRQ",Visible=.t., Left=305, Top=231,;
    Alignment=1, Width=68, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="KZORQVFZWG",Visible=.t., Left=4, Top=121,;
    Alignment=1, Width=117, Height=15,;
    Caption="Da cod. costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="PSROTEXIWN",Visible=.t., Left=4, Top=145,;
    Alignment=1, Width=117, Height=15,;
    Caption="A cod. costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HCCEMYDFMS",Visible=.t., Left=4, Top=177,;
    Alignment=1, Width=117, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="OFCZJZCUFY",Visible=.t., Left=4, Top=201,;
    Alignment=1, Width=117, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="LQXXCQQRXR",Visible=.t., Left=261, Top=314,;
    Alignment=1, Width=78, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="MOCFDQILHK",Visible=.t., Left=4, Top=343,;
    Alignment=1, Width=117, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="TDXNQWSIMS",Visible=.t., Left=4, Top=34,;
    Alignment=1, Width=117, Height=18,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="CXBHZBZZCH",Visible=.t., Left=7, Top=259,;
    Alignment=0, Width=89, Height=15,;
    Caption="Analisi"  ;
  , bGlobalFont=.t.

  add object oBox_1_51 as StdBox with uid="SGXUKFOAUK",left=7, top=274, width=499,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_ssm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
