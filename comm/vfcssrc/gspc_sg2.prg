* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_sg2                                                        *
*              Copia progetto                                                  *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_127]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-21                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gspc_sg2
* Per struttura unica consente ingresso solo a voci amministrative

 If looktab("CPAR_DEF","PDUNIQUE","PDCHIAVE","TAM")= "S" and oParentObject <> "P"
    Ah_ErrorMsg("Funzionalitą non consentita per struttura unica",'!')
    Return .t.
  EndIf



* --- Fine Area Manuale
return(createobject("tgspc_sg2",oParentObject))

* --- Class definition
define class tgspc_sg2 as StdForm
  Top    = 30
  Left   = 11

  * --- Standard Properties
  Width  = 796
  Height = 458
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-02"
  HelpContextID=23746665
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gspc_sg2"
  cComment = "Copia progetto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODCOM1 = space(15)
  o_CODCOM1 = space(15)
  w_CODCONTO1 = space(15)
  w_DESCOM1 = space(30)
  w_DESCON1 = space(30)
  w_TIPSTR = space(1)
  w_CODCOM2 = space(15)
  w_CODCONTO2 = space(15)
  w_DESCOM2 = space(30)
  w_DESCON2 = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_CURSORNA1 = space(10)
  w_CURSORNA2 = space(10)
  w_CODATT3 = space(15)
  w_TIPATT2 = space(1)
  w_TIPATT1 = space(1)
  w_RamoDaCopiare = space(10)
  w_CODATT2 = space(15)
  w_TIPCOM2 = space(1)
  w_TIPCOM1 = space(1)
  w_StatoAttivita = space(1)
  w_CopiaSchedeTecniche = space(1)
  w_CopiaPreventiviAForfait = space(1)
  w_CopiaAttivitaPrecedenti = space(1)
  w_CODATT1 = space(15)
  w_ATDESC1 = space(30)
  w_ATDESC2 = space(30)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TREEV1 = .NULL.
  w_TREEV2 = .NULL.
  w_Dest = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gspc_sg2
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSPC_SG2'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_sg2Pag1","gspc_sg2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM1_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TREEV1 = this.oPgFrm.Pages(1).oPag.TREEV1
    this.w_TREEV2 = this.oPgFrm.Pages(1).oPag.TREEV2
    this.w_Dest = this.oPgFrm.Pages(1).oPag.Dest
    DoDefault()
    proc Destroy()
      this.w_TREEV1 = .NULL.
      this.w_TREEV2 = .NULL.
      this.w_Dest = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='ATTIVITA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODCOM1=space(15)
      .w_CODCONTO1=space(15)
      .w_DESCOM1=space(30)
      .w_DESCON1=space(30)
      .w_TIPSTR=space(1)
      .w_CODCOM2=space(15)
      .w_CODCONTO2=space(15)
      .w_DESCOM2=space(30)
      .w_DESCON2=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_CURSORNA1=space(10)
      .w_CURSORNA2=space(10)
      .w_CODATT3=space(15)
      .w_TIPATT2=space(1)
      .w_TIPATT1=space(1)
      .w_RamoDaCopiare=space(10)
      .w_CODATT2=space(15)
      .w_TIPCOM2=space(1)
      .w_TIPCOM1=space(1)
      .w_StatoAttivita=space(1)
      .w_CopiaSchedeTecniche=space(1)
      .w_CopiaPreventiviAForfait=space(1)
      .w_CopiaAttivitaPrecedenti=space(1)
      .w_CODATT1=space(15)
      .w_ATDESC1=space(30)
      .w_ATDESC2=space(30)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .oPgFrm.Page1.oPag.TREEV1.Calculate()
      .oPgFrm.Page1.oPag.TREEV2.Calculate()
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODCOM1))
          .link_1_4('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCONTO1))
          .link_1_5('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_TIPSTR = this.oParentObject
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODCOM2))
          .link_1_11('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODCONTO2))
          .link_1_12('Full')
        endif
          .DoRTCalc(8,9,.f.)
        .w_OBTEST = cp_CharToDate("  -  -  ")
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
          .DoRTCalc(11,12,.f.)
        .w_CODATT3 = .w_TREEV1.GETVAR('ATCODATT')
        .w_TIPATT2 = .w_TREEV2.GETVAR('ATTIPATT')
        .w_TIPATT1 = .w_TREEV1.GETVAR('ATTIPATT')
          .DoRTCalc(16,16,.f.)
        .w_CODATT2 = .w_TREEV2.GETVAR('ATCODATT')
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .w_TIPCOM2 = .w_TREEV2.GETVAR('ATTIPCOM')
        .w_TIPCOM1 = .w_TREEV1.GETVAR('ATTIPCOM')
        .w_StatoAttivita = 'P'
        .w_CopiaSchedeTecniche = "N"
        .w_CopiaPreventiviAForfait = "N"
        .w_CopiaAttivitaPrecedenti = "N"
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
      .oPgFrm.Page1.oPag.Dest.Calculate("Destinazione")
      .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
          .DoRTCalc(24,24,.f.)
        .w_ATDESC1 = .w_TREEV1.GETVAR('ATDESCRI')
        .w_ATDESC2 = .w_TREEV2.GETVAR('ATDESCRI')
    endwith
    this.DoRTCalc(27,28,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gspc_sg2
    * Imposta il titolo della finestra
    Do Case
    Case This.w_TIPSTR='T'
      This.cComment=ah_Msgformat('Copia progetto tecnico')
    Case This.w_TIPSTR='G'
      This.cComment=ah_Msgformat('Copia progetto gestionale')
    Case This.w_TIPSTR='P'
      This.cComment=ah_Msgformat('Copia progetto amministrativo')
    EndCase
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.TREEV1.Calculate()
        .oPgFrm.Page1.oPag.TREEV2.Calculate()
        .DoRTCalc(1,1,.t.)
        if .o_CODCOM1<>.w_CODCOM1
          .link_1_5('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(3,12,.t.)
            .w_CODATT3 = .w_TREEV1.GETVAR('ATCODATT')
            .w_TIPATT2 = .w_TREEV2.GETVAR('ATTIPATT')
            .w_TIPATT1 = .w_TREEV1.GETVAR('ATTIPATT')
        .DoRTCalc(16,16,.t.)
            .w_CODATT2 = .w_TREEV2.GETVAR('ATCODATT')
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
            .w_TIPCOM2 = .w_TREEV2.GETVAR('ATTIPCOM')
            .w_TIPCOM1 = .w_TREEV1.GETVAR('ATTIPCOM')
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.Dest.Calculate("Destinazione")
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .DoRTCalc(20,24,.t.)
            .w_ATDESC1 = .w_TREEV1.GETVAR('ATDESCRI')
            .w_ATDESC2 = .w_TREEV2.GETVAR('ATDESCRI')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.TREEV1.Calculate()
        .oPgFrm.Page1.oPag.TREEV2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.Dest.Calculate("Destinazione")
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.TREEV1.Event(cEvent)
      .oPgFrm.Page1.oPag.TREEV2.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.Dest.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCOM1
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM1)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM1))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM1)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM1) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM1_1_4'),i_cWhere,'',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM1)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM1 = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM1 = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM1 = space(15)
      endif
      this.w_DESCOM1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONTO1
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONTO1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODCONTO1)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM1;
                     ,'ATTIPATT',this.w_TIPSTR;
                     ,'ATCODATT',trim(this.w_CODCONTO1))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCONTO1)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCONTO1) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODCONTO1_1_5'),i_cWhere,'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM1<>oSource.xKey(1);
           .or. this.w_TIPSTR<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONTO1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODCONTO1);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM1);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM1;
                       ,'ATTIPATT',this.w_TIPSTR;
                       ,'ATCODATT',this.w_CODCONTO1)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONTO1 = NVL(_Link_.ATCODATT,space(15))
      this.w_DESCON1 = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONTO1 = space(15)
      endif
      this.w_DESCON1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONTO1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM2
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM2)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM2))
          select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM2)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM2) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM2_1_11'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM2)
            select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM2 = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM2 = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATINI = NVL(cp_ToDate(_Link_.CNDATINI),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM2 = space(15)
      endif
      this.w_DESCOM2 = space(30)
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOM2 <> .w_CODCOM1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOM2 = space(15)
        this.w_DESCOM2 = space(30)
        this.w_DATINI = ctod("  /  /  ")
        this.w_DATFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONTO2
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONTO2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODCONTO2)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM2);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM2;
                     ,'ATTIPATT',this.w_TIPSTR;
                     ,'ATCODATT',trim(this.w_CODCONTO2))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCONTO2)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCONTO2) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODCONTO2_1_12'),i_cWhere,'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM2<>oSource.xKey(1);
           .or. this.w_TIPSTR<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM2);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONTO2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODCONTO2);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM2);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM2;
                       ,'ATTIPATT',this.w_TIPSTR;
                       ,'ATCODATT',this.w_CODCONTO2)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONTO2 = NVL(_Link_.ATCODATT,space(15))
      this.w_DESCON2 = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONTO2 = space(15)
      endif
      this.w_DESCON2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONTO2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM1_1_4.value==this.w_CODCOM1)
      this.oPgFrm.Page1.oPag.oCODCOM1_1_4.value=this.w_CODCOM1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCONTO1_1_5.value==this.w_CODCONTO1)
      this.oPgFrm.Page1.oPag.oCODCONTO1_1_5.value=this.w_CODCONTO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM1_1_6.value==this.w_DESCOM1)
      this.oPgFrm.Page1.oPag.oDESCOM1_1_6.value=this.w_DESCOM1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON1_1_8.value==this.w_DESCON1)
      this.oPgFrm.Page1.oPag.oDESCON1_1_8.value=this.w_DESCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM2_1_11.value==this.w_CODCOM2)
      this.oPgFrm.Page1.oPag.oCODCOM2_1_11.value=this.w_CODCOM2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCONTO2_1_12.value==this.w_CODCONTO2)
      this.oPgFrm.Page1.oPag.oCODCONTO2_1_12.value=this.w_CODCONTO2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM2_1_13.value==this.w_DESCOM2)
      this.oPgFrm.Page1.oPag.oDESCOM2_1_13.value=this.w_DESCOM2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON2_1_15.value==this.w_DESCON2)
      this.oPgFrm.Page1.oPag.oDESCON2_1_15.value=this.w_DESCON2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT3_1_21.value==this.w_CODATT3)
      this.oPgFrm.Page1.oPag.oCODATT3_1_21.value=this.w_CODATT3
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT2_1_26.value==this.w_CODATT2)
      this.oPgFrm.Page1.oPag.oCODATT2_1_26.value=this.w_CODATT2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODCOM1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM1_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CODCOM1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CODCOM2)) or not(.w_CODCOM2 <> .w_CODCOM1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM2_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CODCOM2)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODCOM1 = this.w_CODCOM1
    return

enddefine

* --- Define pages as container
define class tgspc_sg2Pag1 as StdContainer
  Width  = 792
  height = 458
  stdWidth  = 792
  stdheight = 458
  resizeXpos=305
  resizeYpos=384
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object TREEV1 as cp_Treeview with uid="FVJYSBPBBD",left=5, top=101, width=389,height=348,;
    caption='Object',;
   bGlobalFont=.t.,;
    cCursor="cur_str1",cShowFields="ATDESCRI",cNodeShowField="ATCODATT",cLeafShowField="ATCODATT",cNodeBmp="ROOT.BMP,CONTO.BMP,LEAF.BMP,NEW1.BMP",cLeafBmp="",;
    cEvent = "Esegui1",;
    nPag=1;
    , HelpContextID = 154250982


  add object TREEV2 as cp_Treeview with uid="QJGDBQXFYG",left=399, top=101, width=389,height=348,;
    caption='Object',;
   bGlobalFont=.t.,;
    cCursor="cur_str2",cShowFields="ATDESCRI",cNodeShowField="ATCODATT",cLeafShowField="ATCODATT",cNodeBmp="ROOT.BMP,CONTO.BMP,LEAF.BMP,NEW1.BMP",cLeafBmp="",;
    cEvent = "Esegui2",;
    nPag=1;
    , HelpContextID = 154250982

  add object oCODCOM1_1_4 as StdField with uid="PSHKVDSFCB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODCOM1", cQueryName = "CODCOM1",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa di cui si vuole visualizzare la struttura",;
    HelpContextID = 254985434,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=82, Top=3, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM1"

  func oCODCOM1_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_CODCONTO1)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM1_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM1_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM1_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco commesse",'',this.parent.oContained
  endproc

  add object oCODCONTO1_1_5 as StdField with uid="MGVYZJAETH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCONTO1", cQueryName = "CODCONTO1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Esame struttura a partire da questo nodo ( vuoto tutta la struttura )",;
    HelpContextID = 30228101,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=82, Top=25, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM1", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPSTR", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODCONTO1"

  func oCODCONTO1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCONTO1_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCONTO1_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM1)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM1)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPSTR)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODCONTO1_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODCONTO1_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM1
    i_obj.ATTIPATT=w_TIPSTR
     i_obj.w_ATCODATT=this.parent.oContained.w_CODCONTO1
     i_obj.ecpSave()
  endproc

  add object oDESCOM1_1_6 as StdField with uid="SOTYYQSHVD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCOM1", cQueryName = "DESCOM1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 254926538,;
   bGlobalFont=.t.,;
    Height=21, Width=180, Left=213, Top=3, InputMask=replicate('X',30)

  add object oDESCON1_1_8 as StdField with uid="UPIZTYJPRV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCON1", cQueryName = "DESCON1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 238149322,;
   bGlobalFont=.t.,;
    Height=21, Width=180, Left=213, Top=25, InputMask=replicate('X',30)

  add object oCODCOM2_1_11 as StdField with uid="LTOHIRPXCS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODCOM2", cQueryName = "CODCOM2",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa di qui si vuole visualizzare la struttura",;
    HelpContextID = 13450022,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=476, Top=3, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM2"

  func oCODCOM2_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
      if .not. empty(.w_CODCONTO2)
        bRes2=.link_1_12('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM2_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM2_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM2_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oCODCOM2_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM2
     i_obj.ecpSave()
  endproc

  add object oCODCONTO2_1_12 as StdField with uid="XQCZPKZTVZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODCONTO2", cQueryName = "CODCONTO2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Esame struttura a partire da questo nodo ( vuoto tutta la struttura )",;
    HelpContextID = 30228117,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=476, Top=25, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM2", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPSTR", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODCONTO2"

  func oCODCONTO2_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCONTO2_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCONTO2_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM2)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM2)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPSTR)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODCONTO2_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODCONTO2_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM2
    i_obj.ATTIPATT=w_TIPSTR
     i_obj.w_ATCODATT=this.parent.oContained.w_CODCONTO2
     i_obj.ecpSave()
  endproc

  add object oDESCOM2_1_13 as StdField with uid="WYJNYJNCCQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCOM2", cQueryName = "DESCOM2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 13508918,;
   bGlobalFont=.t.,;
    Height=21, Width=180, Left=607, Top=3, InputMask=replicate('X',30)

  add object oDESCON2_1_15 as StdField with uid="XGTNUCDDJK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCON2", cQueryName = "DESCON2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 30286134,;
   bGlobalFont=.t.,;
    Height=21, Width=180, Left=607, Top=25, InputMask=replicate('X',30)


  add object oObj_1_17 as cp_runprogram with uid="YTBMZHTGZE",left=17, top=559, width=340,height=22,;
    caption='GSPC_BG4',;
   bGlobalFont=.t.,;
    prg="GSPC_BG4('Reload1')",;
    cEvent = "w_CODCOM1 Changed,w_CODCONTO1 Changed",;
    nPag=1;
    , HelpContextID = 114163610


  add object oObj_1_20 as cp_runprogram with uid="WEFAUAMXCS",left=404, top=555, width=340,height=22,;
    caption='GSPC_BG4',;
   bGlobalFont=.t.,;
    prg="GSPC_BG4('Reload2')",;
    cEvent = "w_CODCOM2 Changed,w_CODCONTO2 Changed",;
    nPag=1;
    , HelpContextID = 114163610

  add object oCODATT3_1_21 as StdField with uid="HMNHGWZNVA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODATT3", cQueryName = "CODATT3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 136002342,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=82, Top=47, InputMask=replicate('X',15)

  add object oCODATT2_1_26 as StdField with uid="OPNDYEBLVI",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODATT2", cQueryName = "CODATT2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 136002342,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=476, Top=47, InputMask=replicate('X',15)


  add object oObj_1_28 as cp_runprogram with uid="NDRLNZYSRC",left=24, top=582, width=340,height=22,;
    caption='GSPC_BG4',;
   bGlobalFont=.t.,;
    prg="GSPC_BG4('Menu1')",;
    cEvent = "w_treev1 NodeRightClick",;
    nPag=1;
    , HelpContextID = 114163610


  add object oObj_1_29 as cp_runprogram with uid="IIFTWESISE",left=407, top=581, width=340,height=22,;
    caption='GSPC_BG4',;
   bGlobalFont=.t.,;
    prg="GSPC_BG4('Menu2')",;
    cEvent = "w_treev2 NodeRightClick",;
    nPag=1;
    , HelpContextID = 114163610


  add object oBtn_1_34 as StdButton with uid="IKOOTKPRMK",left=689, top=52, width=48,height=45,;
    CpPicture="BMP\NOTE.BMP", caption="", nPag=1;
    , ToolTipText = "Opzioni della copia progetto";
    , HelpContextID = 201933594;
    , Caption='\<Opzioni';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        do GSPC_SG3 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_38 as cp_runprogram with uid="OVAJXRZKEO",left=23, top=607, width=194,height=22,;
    caption='GSPC_BG4',;
   bGlobalFont=.t.,;
    prg="GSPC_BG4('Done')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 114163610


  add object oBtn_1_39 as StdButton with uid="RVXUFLVJSV",left=345, top=52, width=48,height=45,;
    CpPicture="BMP\REFRESH.BMP", caption="", nPag=1;
    , HelpContextID = 134904985;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        GSPC_BG4(this.Parent.oContained,"Reload1")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_40 as StdButton with uid="TAWFMCMRRJ",left=739, top=52, width=48,height=45,;
    CpPicture="BMP\REFRESH.BMP", caption="", nPag=1;
    , HelpContextID = 134904985;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        GSPC_BG4(this.Parent.oContained,"Reload2")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_42 as cp_runprogram with uid="PQHKEYVUXM",left=13, top=467, width=281,height=22,;
    caption='GSPC_BG4',;
   bGlobalFont=.t.,;
    prg="GSPC_BG4('TV1Menu3')",;
    cEvent = "w_treev1 MouseRightClick",;
    nPag=1;
    , HelpContextID = 114163610


  add object Dest as cp_calclbl with uid="KEIBSPYMGF",left=402, top=84, width=121,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Destinazione",FontName ="Arial",FontBold =.t.,FontSize =10,;
    nPag=1;
    , HelpContextID = 154250982


  add object oObj_1_44 as cp_runprogram with uid="QHQBQRUEZM",left=400, top=467, width=281,height=22,;
    caption='GSPC_BG4',;
   bGlobalFont=.t.,;
    prg="GSPC_BG4('TV2Menu4')",;
    cEvent = "w_treev2 MouseRightClick",;
    nPag=1;
    , HelpContextID = 114163610

  add object oStr_1_3 as StdString with uid="DUAGDEFNHB",Visible=.t., Left=8, Top=3,;
    Alignment=1, Width=69, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="GNCQIRYZUA",Visible=.t., Left=8, Top=25,;
    Alignment=1, Width=69, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="OEQIYPVIXV",Visible=.t., Left=402, Top=3,;
    Alignment=1, Width=69, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="UINWFENPXE",Visible=.t., Left=402, Top=25,;
    Alignment=1, Width=69, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="ETXFNNGHBL",Visible=.t., Left=17, Top=47,;
    Alignment=1, Width=60, Height=18,;
    Caption="Nodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="YFFHKOEXYC",Visible=.t., Left=411, Top=47,;
    Alignment=1, Width=60, Height=18,;
    Caption="Nodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="STKMHQTOJS",Visible=.t., Left=5, Top=71,;
    Alignment=0, Width=79, Height=19,;
    Caption="Origine"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_sg2','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
