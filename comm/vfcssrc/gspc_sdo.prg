* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_sdo                                                        *
*              Stampa dichiarazioni di MDO                                     *
*                                                                              *
*      Author: Zucchetti Spa CF                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_56]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-15                                                      *
* Last revis.: 2008-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_sdo",oParentObject))

* --- Class definition
define class tgspc_sdo as StdForm
  Top    = 5
  Left   = 9

  * --- Standard Properties
  Width  = 476
  Height = 300
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-08"
  HelpContextID=194357143
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  DIPENDEN_IDX = 0
  DICH_MDO_IDX = 0
  cPrg = "gspc_sdo"
  cComment = "Stampa dichiarazioni di MDO"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DICHINI = space(15)
  w_DICHFIN = space(15)
  w_DATDINI = ctod('  /  /  ')
  w_DATDFIN = ctod('  /  /  ')
  w_TAATTIVI = space(1)
  w_COMMINI = space(15)
  o_COMMINI = space(15)
  w_COMMFIN = space(15)
  o_COMMFIN = space(15)
  w_DESCOMF = space(30)
  w_ATTINI = space(15)
  w_ATTFIN = space(15)
  w_DESATTI = space(30)
  w_OPEINI = space(5)
  o_OPEINI = space(5)
  w_OPEFIN = space(5)
  o_OPEFIN = space(5)
  w_PROCESS = space(1)
  w_DPNOMEI = space(40)
  w_DPNOMEF = space(40)
  w_DPCOGNI = space(40)
  w_DPCOGNF = space(40)
  w_DESOPEF = space(40)
  w_DESOPEI = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DESCOMI = space(30)
  w_DESATTF = space(30)
  w_ROWINI = 0
  w_ROWFIN = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_sdoPag1","gspc_sdo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDICHINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='DIPENDEN'
    this.cWorkTables[4]='DICH_MDO'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DICHINI=space(15)
      .w_DICHFIN=space(15)
      .w_DATDINI=ctod("  /  /  ")
      .w_DATDFIN=ctod("  /  /  ")
      .w_TAATTIVI=space(1)
      .w_COMMINI=space(15)
      .w_COMMFIN=space(15)
      .w_DESCOMF=space(30)
      .w_ATTINI=space(15)
      .w_ATTFIN=space(15)
      .w_DESATTI=space(30)
      .w_OPEINI=space(5)
      .w_OPEFIN=space(5)
      .w_PROCESS=space(1)
      .w_DPNOMEI=space(40)
      .w_DPNOMEF=space(40)
      .w_DPCOGNI=space(40)
      .w_DPCOGNF=space(40)
      .w_DESOPEF=space(40)
      .w_DESOPEI=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCOMI=space(30)
      .w_DESATTF=space(30)
      .w_ROWINI=0
      .w_ROWFIN=0
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_DICHINI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_DICHFIN))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_TAATTIVI = 'A'
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_COMMINI))
          .link_1_9('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_COMMFIN))
          .link_1_11('Full')
        endif
          .DoRTCalc(8,8,.f.)
        .w_ATTINI = IIF(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN),'               ',.w_ATTINI)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_ATTINI))
          .link_1_14('Full')
        endif
        .w_ATTFIN = IIF(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN),'               ',.w_ATTINI)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_ATTFIN))
          .link_1_15('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_OPEINI))
          .link_1_18('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_OPEFIN))
          .link_1_21('Full')
        endif
        .w_PROCESS = 'T'
          .DoRTCalc(15,18,.f.)
        .w_DESOPEF = alltrim(.w_DPCOGNF) +" "+ alltrim(.w_DPNOMEF)
        .w_DESOPEI = alltrim(.w_DPCOGNI) +" "+ alltrim(.w_DPNOMEI)
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_OBTEST = I_DATSYS
    endwith
    this.DoRTCalc(22,25,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
        if .o_COMMFIN<>.w_COMMFIN.or. .o_COMMINI<>.w_COMMINI
            .w_ATTINI = IIF(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN),'               ',.w_ATTINI)
          .link_1_14('Full')
        endif
        if .o_COMMFIN<>.w_COMMFIN.or. .o_COMMINI<>.w_COMMINI
            .w_ATTFIN = IIF(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN),'               ',.w_ATTINI)
          .link_1_15('Full')
        endif
        .DoRTCalc(11,12,.t.)
        if .o_OPEINI<>.w_OPEINI
          .link_1_21('Full')
        endif
        .DoRTCalc(14,18,.t.)
        if .o_OPEFIN<>.w_OPEFIN
            .w_DESOPEF = alltrim(.w_DPCOGNF) +" "+ alltrim(.w_DPNOMEF)
        endif
        if .o_OPEINI<>.w_OPEINI
            .w_DESOPEI = alltrim(.w_DPCOGNI) +" "+ alltrim(.w_DPNOMEI)
        endif
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATTINI_1_14.enabled = this.oPgFrm.Page1.oPag.oATTINI_1_14.mCond()
    this.oPgFrm.Page1.oPag.oATTFIN_1_15.enabled = this.oPgFrm.Page1.oPag.oATTFIN_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DICHINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_lTable = "DICH_MDO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2], .t., this.DICH_MDO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICHINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DICH_MDO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RDNUMRIL like "+cp_ToStrODBC(trim(this.w_DICHINI)+"%");

          i_ret=cp_SQL(i_nConn,"select RDNUMRIL,CPROWNUM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RDNUMRIL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RDNUMRIL',trim(this.w_DICHINI))
          select RDNUMRIL,CPROWNUM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RDNUMRIL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICHINI)==trim(_Link_.RDNUMRIL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICHINI) and !this.bDontReportError
            deferred_cp_zoom('DICH_MDO','*','RDNUMRIL',cp_AbsName(oSource.parent,'oDICHINI_1_1'),i_cWhere,'',"Dichiarazioni MDO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RDNUMRIL,CPROWNUM";
                     +" from "+i_cTable+" "+i_lTable+" where RDNUMRIL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RDNUMRIL',oSource.xKey(1))
            select RDNUMRIL,CPROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICHINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RDNUMRIL,CPROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where RDNUMRIL="+cp_ToStrODBC(this.w_DICHINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RDNUMRIL',this.w_DICHINI)
            select RDNUMRIL,CPROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICHINI = NVL(_Link_.RDNUMRIL,space(15))
      this.w_ROWINI = NVL(_Link_.CPROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_DICHINI = space(15)
      endif
      this.w_ROWINI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_DICHFIN)or .w_DICHINI<=.w_DICHFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DICHINI = space(15)
        this.w_ROWINI = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])+'\'+cp_ToStr(_Link_.RDNUMRIL,1)
      cp_ShowWarn(i_cKey,this.DICH_MDO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICHINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICHFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_lTable = "DICH_MDO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2], .t., this.DICH_MDO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICHFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DICH_MDO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RDNUMRIL like "+cp_ToStrODBC(trim(this.w_DICHFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select RDNUMRIL,CPROWNUM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RDNUMRIL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RDNUMRIL',trim(this.w_DICHFIN))
          select RDNUMRIL,CPROWNUM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RDNUMRIL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICHFIN)==trim(_Link_.RDNUMRIL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICHFIN) and !this.bDontReportError
            deferred_cp_zoom('DICH_MDO','*','RDNUMRIL',cp_AbsName(oSource.parent,'oDICHFIN_1_2'),i_cWhere,'',"Dichiarazioni MDO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RDNUMRIL,CPROWNUM";
                     +" from "+i_cTable+" "+i_lTable+" where RDNUMRIL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RDNUMRIL',oSource.xKey(1))
            select RDNUMRIL,CPROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICHFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RDNUMRIL,CPROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where RDNUMRIL="+cp_ToStrODBC(this.w_DICHFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RDNUMRIL',this.w_DICHFIN)
            select RDNUMRIL,CPROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICHFIN = NVL(_Link_.RDNUMRIL,space(15))
      this.w_ROWFIN = NVL(_Link_.CPROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_DICHFIN = space(15)
      endif
      this.w_ROWFIN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_DICHINI)or .w_DICHINI<=.w_DICHFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DICHFIN = space(15)
        this.w_ROWFIN = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])+'\'+cp_ToStr(_Link_.RDNUMRIL,1)
      cp_ShowWarn(i_cKey,this.DICH_MDO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICHFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMINI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMINI))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMINI)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMMINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMMINI)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMMINI) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMINI_1_9'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMINI)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMINI = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMI = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COMMINI = space(15)
      endif
      this.w_DESCOMI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_COMMFIN)or .w_COMMINI<=.w_COMMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COMMINI = space(15)
        this.w_DESCOMI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMFIN
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMFIN))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMFIN)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMMFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMMFIN)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMMFIN) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMFIN_1_11'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMFIN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMFIN = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMF = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COMMFIN = space(15)
      endif
      this.w_DESCOMF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_COMMINI)or .w_COMMINI<=.w_COMMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COMMFIN = space(15)
        this.w_DESCOMF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTINI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTINI)+"%");
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);

          i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATTIPATT',this.w_TAATTIVI;
                     ,'ATCODATT',trim(this.w_ATTINI))
          select ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTINI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTINI) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATTINI_1_14'),i_cWhere,'',"Attivit�",'GSPC1AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TAATTIVI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',oSource.xKey(1);
                       ,'ATCODATT',oSource.xKey(2))
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTINI);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',this.w_TAATTIVI;
                       ,'ATCODATT',this.w_ATTINI)
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTINI = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATTI = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATTINI = space(15)
      endif
      this.w_DESATTI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_ATTFIN)or .w_ATTINI<=.w_ATTFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTINI = space(15)
        this.w_DESATTI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTFIN
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTFIN)+"%");
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);

          i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATTIPATT',this.w_TAATTIVI;
                     ,'ATCODATT',trim(this.w_ATTFIN))
          select ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTFIN)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTFIN) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATTFIN_1_15'),i_cWhere,'',"Attivit�",'GSPC1AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TAATTIVI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',oSource.xKey(1);
                       ,'ATCODATT',oSource.xKey(2))
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTFIN);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',this.w_TAATTIVI;
                       ,'ATCODATT',this.w_ATTFIN)
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTFIN = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATTF = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATTFIN = space(15)
      endif
      this.w_DESATTF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_ATTINI)or .w_ATTINI<=.w_ATTFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTFIN = space(15)
        this.w_DESATTF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OPEINI
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OPEINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_OPEINI)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_OPEINI))
          select DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OPEINI)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OPEINI) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oOPEINI_1_18'),i_cWhere,'',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OPEINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_OPEINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_OPEINI)
            select DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OPEINI = NVL(_Link_.DPCODICE,space(5))
      this.w_DPCOGNI = NVL(_Link_.DPCOGNOM,space(40))
      this.w_DPNOMEI = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OPEINI = space(5)
      endif
      this.w_DPCOGNI = space(40)
      this.w_DPNOMEI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_OPEFIN)or .w_OPEINI<=.w_OPEFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OPEINI = space(5)
        this.w_DPCOGNI = space(40)
        this.w_DPNOMEI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OPEINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OPEFIN
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OPEFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_OPEFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_OPEFIN))
          select DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OPEFIN)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OPEFIN) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oOPEFIN_1_21'),i_cWhere,'',"Dipoendenti",'GSPC_UTE.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OPEFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_OPEFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_OPEFIN)
            select DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OPEFIN = NVL(_Link_.DPCODICE,space(5))
      this.w_DPCOGNF = NVL(_Link_.DPCOGNOM,space(40))
      this.w_DPNOMEF = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OPEFIN = space(5)
      endif
      this.w_DPCOGNF = space(40)
      this.w_DPNOMEF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_OPEINI)or .w_OPEINI<=.w_OPEFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OPEFIN = space(5)
        this.w_DPCOGNF = space(40)
        this.w_DPNOMEF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OPEFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDICHINI_1_1.value==this.w_DICHINI)
      this.oPgFrm.Page1.oPag.oDICHINI_1_1.value=this.w_DICHINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDICHFIN_1_2.value==this.w_DICHFIN)
      this.oPgFrm.Page1.oPag.oDICHFIN_1_2.value=this.w_DICHFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDINI_1_3.value==this.w_DATDINI)
      this.oPgFrm.Page1.oPag.oDATDINI_1_3.value=this.w_DATDINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDFIN_1_4.value==this.w_DATDFIN)
      this.oPgFrm.Page1.oPag.oDATDFIN_1_4.value=this.w_DATDFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMMINI_1_9.value==this.w_COMMINI)
      this.oPgFrm.Page1.oPag.oCOMMINI_1_9.value=this.w_COMMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMMFIN_1_11.value==this.w_COMMFIN)
      this.oPgFrm.Page1.oPag.oCOMMFIN_1_11.value=this.w_COMMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMF_1_12.value==this.w_DESCOMF)
      this.oPgFrm.Page1.oPag.oDESCOMF_1_12.value=this.w_DESCOMF
    endif
    if not(this.oPgFrm.Page1.oPag.oATTINI_1_14.value==this.w_ATTINI)
      this.oPgFrm.Page1.oPag.oATTINI_1_14.value=this.w_ATTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oATTFIN_1_15.value==this.w_ATTFIN)
      this.oPgFrm.Page1.oPag.oATTFIN_1_15.value=this.w_ATTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATTI_1_16.value==this.w_DESATTI)
      this.oPgFrm.Page1.oPag.oDESATTI_1_16.value=this.w_DESATTI
    endif
    if not(this.oPgFrm.Page1.oPag.oOPEINI_1_18.value==this.w_OPEINI)
      this.oPgFrm.Page1.oPag.oOPEINI_1_18.value=this.w_OPEINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOPEFIN_1_21.value==this.w_OPEFIN)
      this.oPgFrm.Page1.oPag.oOPEFIN_1_21.value=this.w_OPEFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPROCESS_1_22.RadioValue()==this.w_PROCESS)
      this.oPgFrm.Page1.oPag.oPROCESS_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOPEF_1_27.value==this.w_DESOPEF)
      this.oPgFrm.Page1.oPag.oDESOPEF_1_27.value=this.w_DESOPEF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOPEI_1_28.value==this.w_DESOPEI)
      this.oPgFrm.Page1.oPag.oDESOPEI_1_28.value=this.w_DESOPEI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMI_1_34.value==this.w_DESCOMI)
      this.oPgFrm.Page1.oPag.oDESCOMI_1_34.value=this.w_DESCOMI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATTF_1_36.value==this.w_DESATTF)
      this.oPgFrm.Page1.oPag.oDESATTF_1_36.value=this.w_DESATTF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_DICHFIN)or .w_DICHINI<=.w_DICHFIN))  and not(empty(.w_DICHINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICHINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_DICHINI)or .w_DICHINI<=.w_DICHFIN))  and not(empty(.w_DICHFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICHFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_DATDFIN)or .w_DATDINI<=.w_DATDFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATDINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_DATDINI)or .w_DATDINI<=.w_DATDFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATDFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_COMMFIN)or .w_COMMINI<=.w_COMMFIN))  and not(empty(.w_COMMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMMINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_COMMINI)or .w_COMMINI<=.w_COMMFIN))  and not(empty(.w_COMMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMMFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_ATTFIN)or .w_ATTINI<=.w_ATTFIN))  and (!(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN)))  and not(empty(.w_ATTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTINI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_ATTINI)or .w_ATTINI<=.w_ATTFIN))  and (!(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN)))  and not(empty(.w_ATTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_OPEFIN)or .w_OPEINI<=.w_OPEFIN)  and not(empty(.w_OPEINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOPEINI_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_OPEINI)or .w_OPEINI<=.w_OPEFIN)  and not(empty(.w_OPEFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOPEFIN_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COMMINI = this.w_COMMINI
    this.o_COMMFIN = this.w_COMMFIN
    this.o_OPEINI = this.w_OPEINI
    this.o_OPEFIN = this.w_OPEFIN
    return

enddefine

* --- Define pages as container
define class tgspc_sdoPag1 as StdContainer
  Width  = 472
  height = 300
  stdWidth  = 472
  stdheight = 300
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDICHINI_1_1 as StdField with uid="JKXRUVAIWL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DICHINI", cQueryName = "DICHINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Dichiarazione MDO di inizio selezione",;
    HelpContextID = 242361654,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=111, Top=8, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="DICH_MDO", oKey_1_1="RDNUMRIL", oKey_1_2="this.w_DICHINI"

  func oDICHINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICHINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICHINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DICH_MDO','*','RDNUMRIL',cp_AbsName(this.parent,'oDICHINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dichiarazioni MDO",'',this.parent.oContained
  endproc

  add object oDICHFIN_1_2 as StdField with uid="ZDWUEXOWLG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DICHFIN", cQueryName = "DICHFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Dichiarazione MDO di fine selezione",;
    HelpContextID = 113105610,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=111, Top=31, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="DICH_MDO", oKey_1_1="RDNUMRIL", oKey_1_2="this.w_DICHFIN"

  proc oDICHFIN_1_2.mDefault
    with this.Parent.oContained
      if empty(.w_DICHFIN)
        .w_DICHFIN = .w_DICHINI
      endif
    endwith
  endproc

  func oDICHFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICHFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICHFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DICH_MDO','*','RDNUMRIL',cp_AbsName(this.parent,'oDICHFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dichiarazioni MDO",'',this.parent.oContained
  endproc

  add object oDATDINI_1_3 as StdField with uid="RVLXPRBBMJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATDINI", cQueryName = "DATDINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 242167094,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=387, Top=8

  func oDATDINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATDFIN)or .w_DATDINI<=.w_DATDFIN)
    endwith
    return bRes
  endfunc

  add object oDATDFIN_1_4 as StdField with uid="ANNCVAHSJC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATDFIN", cQueryName = "DATDFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 113300170,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=387, Top=31

  proc oDATDFIN_1_4.mDefault
    with this.Parent.oContained
      if empty(.w_DATDFIN)
        .w_DATDFIN = .w_DATDINI
      endif
    endwith
  endproc

  func oDATDFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATDINI)or .w_DATDINI<=.w_DATDFIN)
    endwith
    return bRes
  endfunc

  add object oCOMMINI_1_9 as StdField with uid="AEWJVMZWMY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COMMINI", cQueryName = "COMMINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di inizio selezione",;
    HelpContextID = 242731814,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=111, Top=54, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMINI"

  func oCOMMINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMINI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMINI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMINI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCOMMFIN_1_11 as StdField with uid="XIGYTOJLVH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_COMMFIN", cQueryName = "COMMFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di fine selezione",;
    HelpContextID = 112735450,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=111, Top=77, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMFIN"

  proc oCOMMFIN_1_11.mDefault
    with this.Parent.oContained
      if empty(.w_COMMFIN)
        .w_COMMFIN = .w_COMMINI
      endif
    endwith
  endproc

  func oCOMMFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMFIN_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMFIN_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMFIN_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oDESCOMF_1_12 as StdField with uid="LCFOJESBCV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCOMF", cQueryName = "DESCOMF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 231612726,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=234, Top=77, InputMask=replicate('X',30)

  add object oATTINI_1_14 as StdField with uid="VOGDDXFEHR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ATTINI", cQueryName = "ATTINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di inizio selezione",;
    HelpContextID = 104579066,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=111, Top=100, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATTIPATT", oKey_1_2="this.w_TAATTIVI", oKey_2_1="ATCODATT", oKey_2_2="this.w_ATTINI"

  func oATTINI_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN)))
    endwith
   endif
  endfunc

  func oATTINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTINI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTINI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TAATTIVI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TAATTIVI)
    endif
    do cp_zoom with 'ATTIVITA','*','ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATTINI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'GSPC1AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oATTFIN_1_15 as StdField with uid="TPINPKCIJU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ATTFIN", cQueryName = "ATTFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di fine selezione",;
    HelpContextID = 26132474,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=111, Top=123, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATTIPATT", oKey_1_2="this.w_TAATTIVI", oKey_2_1="ATCODATT", oKey_2_2="this.w_ATTFIN"

  func oATTFIN_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN)))
    endwith
   endif
  endfunc

  func oATTFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTFIN_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTFIN_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TAATTIVI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TAATTIVI)
    endif
    do cp_zoom with 'ATTIVITA','*','ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATTFIN_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'GSPC1AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oDESATTI_1_16 as StdField with uid="AELSWDKMAZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESATTI", cQueryName = "DESATTI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 85729590,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=234, Top=100, InputMask=replicate('X',30)

  add object oOPEINI_1_18 as StdField with uid="BUIZFKDLPR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_OPEINI", cQueryName = "OPEINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice dipendente di inizio selezione",;
    HelpContextID = 104641306,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=146, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_OPEINI"

  func oOPEINI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oOPEINI_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOPEINI_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oOPEINI_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oOPEFIN_1_21 as StdField with uid="UWAFEBASDS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_OPEFIN", cQueryName = "OPEFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice dipendente di fine selezione",;
    HelpContextID = 26194714,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=169, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_OPEFIN"

  proc oOPEFIN_1_21.mDefault
    with this.Parent.oContained
      if empty(.w_OPEFIN)
        .w_OPEFIN = .w_OPEINI
      endif
    endwith
  endproc

  func oOPEFIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oOPEFIN_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOPEFIN_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oOPEFIN_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dipoendenti",'GSPC_UTE.DIPENDEN_VZM',this.parent.oContained
  endproc


  add object oPROCESS_1_22 as StdCombo with uid="HCTCFXUHZD",rtseq=14,rtrep=.f.,left=111,top=194,width=120,height=22;
    , ToolTipText = "Stampa dichiarazioni processate";
    , HelpContextID = 215093514;
    , cFormVar="w_PROCESS",RowSource=""+"Tutte,"+"Processate,"+"Non processate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROCESS_1_22.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oPROCESS_1_22.GetRadio()
    this.Parent.oContained.w_PROCESS = this.RadioValue()
    return .t.
  endfunc

  func oPROCESS_1_22.SetRadio()
    this.Parent.oContained.w_PROCESS=trim(this.Parent.oContained.w_PROCESS)
    this.value = ;
      iif(this.Parent.oContained.w_PROCESS=='T',1,;
      iif(this.Parent.oContained.w_PROCESS=='S',2,;
      iif(this.Parent.oContained.w_PROCESS=='N',3,;
      0)))
  endfunc

  add object oDESOPEF_1_27 as StdField with uid="TRMEJLFKDQ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESOPEF", cQueryName = "DESOPEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 99230006,;
   bGlobalFont=.t.,;
    Height=21, Width=290, Left=173, Top=169, InputMask=replicate('X',40)

  add object oDESOPEI_1_28 as StdField with uid="MPMWHKMORG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESOPEI", cQueryName = "DESOPEI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 99230006,;
   bGlobalFont=.t.,;
    Height=21, Width=290, Left=173, Top=146, InputMask=replicate('X',40)


  add object oObj_1_30 as cp_outputCombo with uid="ZLERXIVPWT",left=110, top=222, width=353,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 164516122


  add object oBtn_1_31 as StdButton with uid="IDQLKXKZHR",left=359, top=250, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 200724186;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="UKWSGSSBLX",left=413, top=250, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 251567850;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCOMI_1_34 as StdField with uid="WPUVWCJTXN",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCOMI", cQueryName = "DESCOMI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 231612726,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=234, Top=54, InputMask=replicate('X',30)

  add object oDESATTF_1_36 as StdField with uid="ICAQHTUJID",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESATTF", cQueryName = "DESATTF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 85729590,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=234, Top=123, InputMask=replicate('X',30)

  add object oStr_1_6 as StdString with uid="IDDMBMYZZS",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=101, Height=15,;
    Caption="Da dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="TGJOVTKAGR",Visible=.t., Left=6, Top=31,;
    Alignment=1, Width=101, Height=15,;
    Caption="A dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="IDFOLHEDEP",Visible=.t., Left=243, Top=8,;
    Alignment=1, Width=139, Height=15,;
    Caption="Da data dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="TGCKNAZOEP",Visible=.t., Left=243, Top=31,;
    Alignment=1, Width=139, Height=15,;
    Caption="A data dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="PDIZSVTEPC",Visible=.t., Left=6, Top=77,;
    Alignment=1, Width=101, Height=15,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="BPUHPITAMJ",Visible=.t., Left=6, Top=100,;
    Alignment=1, Width=101, Height=18,;
    Caption="Da attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="NNJPSJYPVQ",Visible=.t., Left=6, Top=146,;
    Alignment=1, Width=101, Height=15,;
    Caption="Da dipendente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="METEKCOOLS",Visible=.t., Left=6, Top=169,;
    Alignment=1, Width=101, Height=15,;
    Caption="A dipendente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="PGZKNKALVF",Visible=.t., Left=6, Top=223,;
    Alignment=1, Width=101, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="FYTEGOOWAF",Visible=.t., Left=6, Top=54,;
    Alignment=1, Width=101, Height=15,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="SEICYKOMFL",Visible=.t., Left=6, Top=126,;
    Alignment=1, Width=101, Height=18,;
    Caption="A attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="OEQAPQCMSG",Visible=.t., Left=6, Top=197,;
    Alignment=1, Width=101, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_sdo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
