* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bcx                                                        *
*              Copia progetto                                                  *
*                                                                              *
*      Author: Paolo Saitti                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-09-28                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bcx",oParentObject)
return(i_retval)

define class tgspc_bcx as StdBatch
  * --- Local variables
  w_COM = space(15)
  w_DIFF = 0
  w_MILL = 0
  w_MESS = space(200)
  w_NEWDATEI = ctod("  /  /  ")
  w_TIPCOM = space(1)
  w_NEWDATEF = ctod("  /  /  ")
  w_NEWDATVI = ctod("  /  /  ")
  w_TIP = space(1)
  w_ATT = space(15)
  w_READPAR = space(3)
  w_PROGR = space(1)
  w_ERRNO = 0
  w_ATVINCOL = space(3)
  * --- WorkFile variables
  ATTIVITA_idx=0
  STRUTTUR_idx=0
  ATT_PREC_idx=0
  CPAR_DEF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili globali (il padre � la mask GSPC_KCX)
    * --- Data di inizio commessa sorgente
    * --- Data di inizio commessa destinazione
    * --- Variabili locali
    * --- Lag per aggiornamento
    * --- Nuova data di inizio commessa destinazione
    * --- Nuova data di fine commessa destinazione
    * --- Nuova data vincolo della commessa destinazione
    this.w_MESS = "Attenzione: i dati relativi alla commessa di destinazione verranno persi%0Le attivit� copiate verranno messe in stato provvisorio%0Confermi ugualmente?"
    if NOT ah_YESNO(this.w_MESS)
      * --- Operazione annullata dall'utente
      i_retcode = 'stop'
      return
    endif
    * --- Determinazione lag per date attivit� ('A') nuova commessa
    this.w_DIFF = this.oParentObject.w_DATINID-this.oParentObject.w_DATINIS
    this.w_READPAR = "TAM"
    * --- Read from CPAR_DEF
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDPROGR"+;
        " from "+i_cTable+" CPAR_DEF where ";
            +"PDCHIAVE = "+cp_ToStrODBC(this.w_READPAR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDPROGR;
        from (i_cTable) where;
            PDCHIAVE = this.w_READPAR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PROGR = NVL(cp_ToDate(_read_.PDPROGR),cp_NullValue(_read_.PDPROGR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_03698E00
    bErr_03698E00=bTrsErr
    this.Try_03698E00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ERRORMSG(i_Errmsg,16)
    endif
    bTrsErr=bTrsErr or bErr_03698E00
    * --- End
    do case
      case this.w_ERRNO=1
        ah_ErrorMsg("Non � stato possibile tempificare le attivit� della commessa per la presenza%0di un loop nelle relazioni di precedenza nella commessa di origine.",48)
      case this.w_ERRNO=2
        ah_ErrorMsg("Non � stato possibile tempificare le attivit� della commessa.",48)
    endcase
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.ecpQuit
  endproc
  proc Try_03698E00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_CHKTECN="T" and this.oParentObject.w_CHKGEST="G" and this.oParentObject.w_CHKAMM="P"
      * --- Se vengono selezionate per la copia TUTTE le strutture:
      * --- 1) Elimina tutti i record in tutti e 3 gli archivi per quella commessa
      * --- 2) Copia tutte le strutture, incluse (sempre) anche tutte le attivit�
      * --- 1) Elimina tutti i record in tutti e 3 gli archivi per quella commessa
      * --- (vengono mantenuti per� i capiprogetto: ATTIPCOM='P')
      * --- Delete from STRUTTUR
      i_nConn=i_TableProp[this.STRUTTUR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"STCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOMD);
               )
      else
        delete from (i_cTable) where;
              STCODCOM = this.oParentObject.w_CODCOMD;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Impossibile cancellare da STRUTTUR'
        return
      endif
      * --- Delete from ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MPCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOMD);
               )
      else
        delete from (i_cTable) where;
              MPCODCOM = this.oParentObject.w_CODCOMD;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Try
      local bErr_03699E50
      bErr_03699E50=bTrsErr
      this.Try_03699E50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = AH_MSGFORMAT( "Le attivit� non possono essere cancellate%0Verificare il loro utilizzo all'interno di documenti o movimenti" )
        * --- Raise
        i_Error=this.w_MESS
        return
      endif
      bTrsErr=bTrsErr or bErr_03699E50
      * --- End
      * --- 2) Copia tutte le strutture, incluse (sempre) anche tutte le attivit�
      * --- Select from ATTIVITA
      i_nConn=i_TableProp[this.ATTIVITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
            +" where ATCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMS)+" and ATTIPCOM<>'P'";
             ,"_Curs_ATTIVITA")
      else
        select * from (i_cTable);
         where ATCODCOM=this.oParentObject.w_CODCOMS and ATTIPCOM<>"P";
          into cursor _Curs_ATTIVITA
      endif
      if used('_Curs_ATTIVITA')
        select _Curs_ATTIVITA
        locate for 1=1
        do while not(eof())
        this.w_NEWDATEI = cp_todate(_Curs_ATTIVITA.ATDATINI)
        if not empty(this.w_NEWDATEI)
          this.w_NEWDATEI = this.w_NEWDATEI+this.w_DIFF
          * --- Se la data � definita si aggiunge semplicemente il lag
        else
          this.w_NEWDATEI = this.oParentObject.w_DATINID
          * --- Altrimenti si prende la data di inizio della nuova commessa
          * --- (questo per� non dovrebbe MAI accadere!)
        endif
        this.w_NEWDATEF = cp_todate(_Curs_ATTIVITA.ATDATFIN)
        if not empty(this.w_NEWDATEF)
          this.w_NEWDATEF = this.w_NEWDATEF+this.w_DIFF
          * --- Se la data di fine esiste viene aggiornata con il lag
          * --- (in caso contrario la CP_TODATE assicura il NULL)
        endif
        this.w_NEWDATVI = cp_todate(_Curs_ATTIVITA.ATDATVIN)
        if not empty(this.w_NEWDATVI)
          this.w_NEWDATVI = this.w_NEWDATVI+this.w_DIFF
        endif
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Insert into ATTIVITA
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATTIVITA_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ATCODCOM"+",ATTIPATT"+",ATCODATT"+",ATTIPCOM"+",ATDESCRI"+",ATDESSUP"+",ATCENCOS"+",ATTOTCOS"+",ATDATINI"+",ATDATFIN"+",ATDURGIO"+",ATVINCOL"+",ATDATVIN"+",ATCARDIN"+",AT__RIGA"+",ATCODFAM"+",ATFLPREV"+",AT_STATO"+",AT_OKTEC"+",AT_OKAMM"+",AT_OKGES"+",ATGRPMAN"+",AT_PRJVW"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'ATTIVITA','ATCODCOM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTIPATT),'ATTIVITA','ATTIPATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCODATT),'ATTIVITA','ATCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTIPCOM),'ATTIVITA','ATTIPCOM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDESCRI),'ATTIVITA','ATDESCRI');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDESSUP),'ATTIVITA','ATDESSUP');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCENCOS),'ATTIVITA','ATCENCOS');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTOTCOS),'ATTIVITA','ATTOTCOS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATEI),'ATTIVITA','ATDATINI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATEF),'ATTIVITA','ATDATFIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDURGIO),'ATTIVITA','ATDURGIO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ATVINCOL),'ATTIVITA','ATVINCOL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATVI),'ATTIVITA','ATDATVIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCARDIN),'ATTIVITA','ATCARDIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT__RIGA),'ATTIVITA','AT__RIGA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCODFAM),'ATTIVITA','ATCODFAM');
          +","+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
          +","+cp_NullLink(cp_ToStrODBC("P"),'ATTIVITA','AT_STATO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKTEC),'ATTIVITA','AT_OKTEC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKAMM),'ATTIVITA','AT_OKAMM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKGES),'ATTIVITA','AT_OKGES');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATGRPMAN),'ATTIVITA','ATGRPMAN');
          +","+cp_NullLink(cp_ToStrODBC("S"),'ATTIVITA','AT_PRJVW');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ATCODCOM',this.oParentObject.w_CODCOMD,'ATTIPATT',_Curs_ATTIVITA.ATTIPATT,'ATCODATT',_Curs_ATTIVITA.ATCODATT,'ATTIPCOM',_Curs_ATTIVITA.ATTIPCOM,'ATDESCRI',_Curs_ATTIVITA.ATDESCRI,'ATDESSUP',_Curs_ATTIVITA.ATDESSUP,'ATCENCOS',_Curs_ATTIVITA.ATCENCOS,'ATTOTCOS',_Curs_ATTIVITA.ATTOTCOS,'ATDATINI',this.w_NEWDATEI,'ATDATFIN',this.w_NEWDATEF,'ATDURGIO',_Curs_ATTIVITA.ATDURGIO,'ATVINCOL',this.w_ATVINCOL)
          insert into (i_cTable) (ATCODCOM,ATTIPATT,ATCODATT,ATTIPCOM,ATDESCRI,ATDESSUP,ATCENCOS,ATTOTCOS,ATDATINI,ATDATFIN,ATDURGIO,ATVINCOL,ATDATVIN,ATCARDIN,AT__RIGA,ATCODFAM,ATFLPREV,AT_STATO,AT_OKTEC,AT_OKAMM,AT_OKGES,ATGRPMAN,AT_PRJVW &i_ccchkf. );
             values (;
               this.oParentObject.w_CODCOMD;
               ,_Curs_ATTIVITA.ATTIPATT;
               ,_Curs_ATTIVITA.ATCODATT;
               ,_Curs_ATTIVITA.ATTIPCOM;
               ,_Curs_ATTIVITA.ATDESCRI;
               ,_Curs_ATTIVITA.ATDESSUP;
               ,_Curs_ATTIVITA.ATCENCOS;
               ,_Curs_ATTIVITA.ATTOTCOS;
               ,this.w_NEWDATEI;
               ,this.w_NEWDATEF;
               ,_Curs_ATTIVITA.ATDURGIO;
               ,this.w_ATVINCOL;
               ,this.w_NEWDATVI;
               ,_Curs_ATTIVITA.ATCARDIN;
               ,_Curs_ATTIVITA.AT__RIGA;
               ,_Curs_ATTIVITA.ATCODFAM;
               ,"N";
               ,"P";
               ,_Curs_ATTIVITA.AT_OKTEC;
               ,_Curs_ATTIVITA.AT_OKAMM;
               ,_Curs_ATTIVITA.AT_OKGES;
               ,_Curs_ATTIVITA.ATGRPMAN;
               ,"S";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Impossibile scrivere in ATTIVITA'
          return
        endif
          select _Curs_ATTIVITA
          continue
        enddo
        use
      endif
      * --- Select from STRUTTUR
      i_nConn=i_TableProp[this.STRUTTUR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" STRUTTUR ";
            +" where STCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMS)+"";
             ,"_Curs_STRUTTUR")
      else
        select * from (i_cTable);
         where STCODCOM=this.oParentObject.w_CODCOMS;
          into cursor _Curs_STRUTTUR
      endif
      if used('_Curs_STRUTTUR')
        select _Curs_STRUTTUR
        locate for 1=1
        do while not(eof())
        * --- Reperisco il TIPCOM dell'attivit� padre
        this.w_COM = _Curs_STRUTTUR.STCODCOM
        this.w_TIP = _Curs_STRUTTUR.STTIPSTR
        this.w_ATT = _Curs_STRUTTUR.STATTPAD
        Vq_Exec("..\comm\exe\query\GSPC_BCX.vqr",this,"Cur_Tipcom")
        select ("Cur_Tipcom")
        go top
        this.w_TIPCOM = ATTIPCOM
        if this.w_TIPCOM<>"P"
          * --- Se l'attivit� padre NON � un capo progetto allora lego normalmente le attivit� in STRUTTUR
          * --- Insert into STRUTTUR
          i_nConn=i_TableProp[this.STRUTTUR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STATTFIG"+",STTIPFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'STRUTTUR','STCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STTIPSTR),'STRUTTUR','STTIPSTR');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STATTPAD),'STRUTTUR','STATTPAD');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STDESCRI),'STRUTTUR','STDESCRI');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STATTFIG),'STRUTTUR','STATTFIG');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STTIPFIG),'STRUTTUR','STTIPFIG');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.CPROWORD),'STRUTTUR','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STMILLES),'STRUTTUR','STMILLES');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.oParentObject.w_CODCOMD,'STTIPSTR',_Curs_STRUTTUR.STTIPSTR,'STATTPAD',_Curs_STRUTTUR.STATTPAD,'STDESCRI',_Curs_STRUTTUR.STDESCRI,'STATTFIG',_Curs_STRUTTUR.STATTFIG,'STTIPFIG',_Curs_STRUTTUR.STTIPFIG,'CPROWORD',_Curs_STRUTTUR.CPROWORD,'STMILLES',_Curs_STRUTTUR.STMILLES)
            insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STATTFIG,STTIPFIG,CPROWORD,STMILLES &i_ccchkf. );
               values (;
                 this.oParentObject.w_CODCOMD;
                 ,_Curs_STRUTTUR.STTIPSTR;
                 ,_Curs_STRUTTUR.STATTPAD;
                 ,_Curs_STRUTTUR.STDESCRI;
                 ,_Curs_STRUTTUR.STATTFIG;
                 ,_Curs_STRUTTUR.STTIPFIG;
                 ,_Curs_STRUTTUR.CPROWORD;
                 ,_Curs_STRUTTUR.STMILLES;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Impossibile scrivere in STRUTTUR'
            return
          endif
        else
          * --- Se l'attivit� padre E' un capo progetto allora in STRUTTUR devo legare l'attivit�
          * --- all'attivit� capoprogetto per quella commessa (STATTPAD=w_CODCOMD)
          * --- (N.B.: per i capiprogetto attivit� e commessa corrispondono)
          * --- Insert into STRUTTUR
          i_nConn=i_TableProp[this.STRUTTUR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STATTFIG"+",STTIPFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'STRUTTUR','STCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STTIPSTR),'STRUTTUR','STTIPSTR');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'STRUTTUR','STATTPAD');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STDESCRI),'STRUTTUR','STDESCRI');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STATTFIG),'STRUTTUR','STATTFIG');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STTIPFIG),'STRUTTUR','STTIPFIG');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.CPROWORD),'STRUTTUR','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STMILLES),'STRUTTUR','STMILLES');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.oParentObject.w_CODCOMD,'STTIPSTR',_Curs_STRUTTUR.STTIPSTR,'STATTPAD',this.oParentObject.w_CODCOMD,'STDESCRI',_Curs_STRUTTUR.STDESCRI,'STATTFIG',_Curs_STRUTTUR.STATTFIG,'STTIPFIG',_Curs_STRUTTUR.STTIPFIG,'CPROWORD',_Curs_STRUTTUR.CPROWORD,'STMILLES',_Curs_STRUTTUR.STMILLES)
            insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STATTFIG,STTIPFIG,CPROWORD,STMILLES &i_ccchkf. );
               values (;
                 this.oParentObject.w_CODCOMD;
                 ,_Curs_STRUTTUR.STTIPSTR;
                 ,this.oParentObject.w_CODCOMD;
                 ,_Curs_STRUTTUR.STDESCRI;
                 ,_Curs_STRUTTUR.STATTFIG;
                 ,_Curs_STRUTTUR.STTIPFIG;
                 ,_Curs_STRUTTUR.CPROWORD;
                 ,_Curs_STRUTTUR.STMILLES;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Impossibile scrivere in STRUTTUR'
            return
          endif
        endif
          select _Curs_STRUTTUR
          continue
        enddo
        use
      endif
      * --- Select from ATT_PREC
      i_nConn=i_TableProp[this.ATT_PREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2],.t.,this.ATT_PREC_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATT_PREC ";
            +" where MPCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMS)+"";
             ,"_Curs_ATT_PREC")
      else
        select * from (i_cTable);
         where MPCODCOM=this.oParentObject.w_CODCOMS;
          into cursor _Curs_ATT_PREC
      endif
      if used('_Curs_ATT_PREC')
        select _Curs_ATT_PREC
        locate for 1=1
        do while not(eof())
        * --- Insert into ATT_PREC
        i_nConn=i_TableProp[this.ATT_PREC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATT_PREC_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MPCODCOM"+",MPTIPATT"+",MPCODATT"+",MPATTPRE"+",MPTIPPRE"+",MTRITAR"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'ATT_PREC','MPCODCOM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MPTIPATT),'ATT_PREC','MPTIPATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MPCODATT),'ATT_PREC','MPCODATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MPATTPRE),'ATT_PREC','MPATTPRE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MPTIPPRE),'ATT_PREC','MPTIPPRE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MTRITAR),'ATT_PREC','MTRITAR');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MPCODCOM',this.oParentObject.w_CODCOMD,'MPTIPATT',_Curs_ATT_PREC.MPTIPATT,'MPCODATT',_Curs_ATT_PREC.MPCODATT,'MPATTPRE',_Curs_ATT_PREC.MPATTPRE,'MPTIPPRE',_Curs_ATT_PREC.MPTIPPRE,'MTRITAR',_Curs_ATT_PREC.MTRITAR)
          insert into (i_cTable) (MPCODCOM,MPTIPATT,MPCODATT,MPATTPRE,MPTIPPRE,MTRITAR &i_ccchkf. );
             values (;
               this.oParentObject.w_CODCOMD;
               ,_Curs_ATT_PREC.MPTIPATT;
               ,_Curs_ATT_PREC.MPCODATT;
               ,_Curs_ATT_PREC.MPATTPRE;
               ,_Curs_ATT_PREC.MPTIPPRE;
               ,_Curs_ATT_PREC.MTRITAR;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Impossibile scrivere in ATT_PREC'
          return
        endif
          select _Curs_ATT_PREC
          continue
        enddo
        use
      endif
    else
      * --- Se viene selezionato solo un sottoinsieme stretto di strutture occorre tenere conto anche di cosa si � scelto per le attivit�
      if this.oParentObject.w_CHKATT="A"
        * --- Se si ricopiano le attivit� occorre:
        * --- ovvero:
        * --- 1) cancellare i selezionati
        * --- 1') Cancello tutti quelli che hanno un figlio attivit�
        * --- 2) cancellare i non selezionati che hanno un figlio attivit�
        * --- 2') Cancello tutti i selezionati
        * --- 3) Cancellare le attivit�
        * --- 4) Ricopiare le attivit�
        * --- 5) Ricopiare i selezionati
        * --- 1') Cancello tutti quelli che hanno un figlio attivit�
        * --- Select from STRUTTUR
        i_nConn=i_TableProp[this.STRUTTUR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" STRUTTUR ";
              +" where STCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMD)+" and STTIPFIG='A'";
               ,"_Curs_STRUTTUR")
        else
          select * from (i_cTable);
           where STCODCOM=this.oParentObject.w_CODCOMD and STTIPFIG="A";
            into cursor _Curs_STRUTTUR
        endif
        if used('_Curs_STRUTTUR')
          select _Curs_STRUTTUR
          locate for 1=1
          do while not(eof())
          this.w_COM = _Curs_STRUTTUR.STCODCOM
          this.w_TIP = _Curs_STRUTTUR.STTIPSTR
          this.w_ATT = _Curs_STRUTTUR.STATTPAD
          * --- Delete from STRUTTUR
          i_nConn=i_TableProp[this.STRUTTUR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"STCODCOM = "+cp_ToStrODBC(this.w_COM);
                  +" and STTIPSTR = "+cp_ToStrODBC(this.w_TIP);
                  +" and STATTPAD = "+cp_ToStrODBC(this.w_ATT);
                   )
          else
            delete from (i_cTable) where;
                  STCODCOM = this.w_COM;
                  and STTIPSTR = this.w_TIP;
                  and STATTPAD = this.w_ATT;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error='Impossibile cancellare da STRUTTUR'
            return
          endif
            select _Curs_STRUTTUR
            continue
          enddo
          use
        endif
        * --- 2') Cancello tutti i selezionati
        * --- Select from STRUTTUR
        i_nConn=i_TableProp[this.STRUTTUR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" STRUTTUR ";
              +" where STCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMD)+" and (STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_CHKTECN)+" or STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_CHKGEST)+" or STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_CHKAMM)+")";
               ,"_Curs_STRUTTUR")
        else
          select * from (i_cTable);
           where STCODCOM=this.oParentObject.w_CODCOMD and (STTIPSTR=this.oParentObject.w_CHKTECN or STTIPSTR=this.oParentObject.w_CHKGEST or STTIPSTR=this.oParentObject.w_CHKAMM);
            into cursor _Curs_STRUTTUR
        endif
        if used('_Curs_STRUTTUR')
          select _Curs_STRUTTUR
          locate for 1=1
          do while not(eof())
          this.w_COM = _Curs_STRUTTUR.STCODCOM
          this.w_TIP = _Curs_STRUTTUR.STTIPSTR
          this.w_ATT = _Curs_STRUTTUR.STATTPAD
          * --- Delete from STRUTTUR
          i_nConn=i_TableProp[this.STRUTTUR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"STCODCOM = "+cp_ToStrODBC(this.w_COM);
                  +" and STTIPSTR = "+cp_ToStrODBC(this.w_TIP);
                  +" and STATTPAD = "+cp_ToStrODBC(this.w_ATT);
                   )
          else
            delete from (i_cTable) where;
                  STCODCOM = this.w_COM;
                  and STTIPSTR = this.w_TIP;
                  and STATTPAD = this.w_ATT;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error='Impossibile cancellare da STRUTTUR'
            return
          endif
            select _Curs_STRUTTUR
            continue
          enddo
          use
        endif
        * --- Select from ATTIVITA
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
              +" where ATCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMD)+" and (ATTIPATT="+cp_ToStrODBC(this.oParentObject.w_CHKTECN)+" or ATTIPATT="+cp_ToStrODBC(this.oParentObject.w_CHKGEST)+" or ATTIPATT="+cp_ToStrODBC(this.oParentObject.w_CHKAMM)+")";
               ,"_Curs_ATTIVITA")
        else
          select * from (i_cTable);
           where ATCODCOM=this.oParentObject.w_CODCOMD and (ATTIPATT=this.oParentObject.w_CHKTECN or ATTIPATT=this.oParentObject.w_CHKGEST or ATTIPATT=this.oParentObject.w_CHKAMM);
            into cursor _Curs_ATTIVITA
        endif
        if used('_Curs_ATTIVITA')
          select _Curs_ATTIVITA
          locate for 1=1
          do while not(eof())
          this.w_COM = _Curs_ATTIVITA.ATCODCOM
          this.w_TIP = _Curs_ATTIVITA.ATTIPATT
          this.w_ATT = _Curs_ATTIVITA.ATCODATT
          * --- Delete from ATTIVITA
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"ATCODCOM = "+cp_ToStrODBC(this.w_COM);
                  +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIP);
                  +" and ATCODATT = "+cp_ToStrODBC(this.w_ATT);
                  +" and NOT ATTIPCOM = "+cp_ToStrODBC("P");
                   )
          else
            delete from (i_cTable) where;
                  ATCODCOM = this.w_COM;
                  and ATTIPATT = this.w_TIP;
                  and ATCODATT = this.w_ATT;
                  and NOT ATTIPCOM = "P";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error='Impossibile cancellare da ATTIVITA'
            return
          endif
            select _Curs_ATTIVITA
            continue
          enddo
          use
        endif
        * --- 3) Cancellare le attivit�
        * --- Select from ATT_PREC
        i_nConn=i_TableProp[this.ATT_PREC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2],.t.,this.ATT_PREC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATT_PREC ";
              +" where MPCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMD)+"";
               ,"_Curs_ATT_PREC")
        else
          select * from (i_cTable);
           where MPCODCOM=this.oParentObject.w_CODCOMD;
            into cursor _Curs_ATT_PREC
        endif
        if used('_Curs_ATT_PREC')
          select _Curs_ATT_PREC
          locate for 1=1
          do while not(eof())
          this.w_COM = _Curs_ATT_PREC.MPCODCOM
          this.w_TIP = _Curs_ATT_PREC.MPTIPATT
          this.w_ATT = _Curs_ATT_PREC.MPCODATT
          * --- Delete from ATT_PREC
          i_nConn=i_TableProp[this.ATT_PREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MPCODCOM = "+cp_ToStrODBC(this.w_COM);
                  +" and MPTIPATT = "+cp_ToStrODBC(this.w_TIP);
                  +" and MPCODATT = "+cp_ToStrODBC(this.w_ATT);
                   )
          else
            delete from (i_cTable) where;
                  MPCODCOM = this.w_COM;
                  and MPTIPATT = this.w_TIP;
                  and MPCODATT = this.w_ATT;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
            select _Curs_ATT_PREC
            continue
          enddo
          use
        endif
        * --- Select from ATTIVITA
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
              +" where ATCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMD)+" and ATTIPATT='A'";
               ,"_Curs_ATTIVITA")
        else
          select * from (i_cTable);
           where ATCODCOM=this.oParentObject.w_CODCOMD and ATTIPATT="A";
            into cursor _Curs_ATTIVITA
        endif
        if used('_Curs_ATTIVITA')
          select _Curs_ATTIVITA
          locate for 1=1
          do while not(eof())
          this.w_COM = _Curs_ATTIVITA.ATCODCOM
          this.w_TIP = _Curs_ATTIVITA.ATTIPATT
          this.w_ATT = _Curs_ATTIVITA.ATCODATT
          * --- Delete from ATTIVITA
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"ATCODCOM = "+cp_ToStrODBC(this.w_COM);
                  +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIP);
                  +" and ATCODATT = "+cp_ToStrODBC(this.w_ATT);
                  +" and NOT ATTIPCOM = "+cp_ToStrODBC("P");
                   )
          else
            delete from (i_cTable) where;
                  ATCODCOM = this.w_COM;
                  and ATTIPATT = this.w_TIP;
                  and ATCODATT = this.w_ATT;
                  and NOT ATTIPCOM = "P";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error='Impossibile cancellare da ATTIVITA'
            return
          endif
            select _Curs_ATTIVITA
            continue
          enddo
          use
        endif
        * --- 4) Ricopiare le attivit�
        * --- Select from ATTIVITA
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
              +" where ATCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMS)+" and ATTIPATT='A'";
               ,"_Curs_ATTIVITA")
        else
          select * from (i_cTable);
           where ATCODCOM=this.oParentObject.w_CODCOMS and ATTIPATT="A";
            into cursor _Curs_ATTIVITA
        endif
        if used('_Curs_ATTIVITA')
          select _Curs_ATTIVITA
          locate for 1=1
          do while not(eof())
          this.w_NEWDATEI = cp_todate(_Curs_ATTIVITA.ATDATINI)
          if not empty(this.w_NEWDATEI)
            this.w_NEWDATEI = this.w_NEWDATEI+this.w_DIFF
            * --- Se la data � definita si aggiunge semplicemente il lag
          else
            this.w_NEWDATEI = this.oParentObject.w_DATINID
            * --- Altrimenti si prende la data di inizio della nuova commessa
            * --- (questo per� non dovrebbe MAI accadere!)
          endif
          this.w_NEWDATEF = cp_todate(_Curs_ATTIVITA.ATDATFIN)
          if not empty(this.w_NEWDATEF)
            * --- Se la data di fine esiste viene aggiornata con il lag
            this.w_NEWDATEF = this.w_NEWDATEF+this.w_DIFF
            * --- (in caso contrario la CP_TODATE assicura il NULL)
          endif
          this.w_NEWDATVI = cp_todate(_Curs_ATTIVITA.ATDATVIN)
          if not empty(this.w_NEWDATVI)
            this.w_NEWDATVI = this.w_NEWDATVI+this.w_DIFF
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Insert into ATTIVITA
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"ATVINCOL"+",ATTOTCOS"+",ATTIPCOM"+",ATTIPATT"+",ATGRPMAN"+",ATFLPREV"+",ATDURGIO"+",ATDESSUP"+",ATDESCRI"+",ATDATVIN"+",ATDATINI"+",ATDATFIN"+",ATCODFAM"+",ATCODCOM"+",ATCODATT"+",ATCENCOS"+",ATCARDIN"+",AT_STATO"+",AT_OKTEC"+",AT_OKGES"+",AT_OKAMM"+",AT__RIGA"+",AT_PRJVW"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_ATVINCOL),'ATTIVITA','ATVINCOL');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTOTCOS),'ATTIVITA','ATTOTCOS');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTIPCOM),'ATTIVITA','ATTIPCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTIPATT),'ATTIVITA','ATTIPATT');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATGRPMAN),'ATTIVITA','ATGRPMAN');
            +","+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDURGIO),'ATTIVITA','ATDURGIO');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDESSUP),'ATTIVITA','ATDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDESCRI),'ATTIVITA','ATDESCRI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATVI),'ATTIVITA','ATDATVIN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATEI),'ATTIVITA','ATDATINI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATEF),'ATTIVITA','ATDATFIN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCODFAM),'ATTIVITA','ATCODFAM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'ATTIVITA','ATCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCODATT),'ATTIVITA','ATCODATT');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCENCOS),'ATTIVITA','ATCENCOS');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCARDIN),'ATTIVITA','ATCARDIN');
            +","+cp_NullLink(cp_ToStrODBC("P"),'ATTIVITA','AT_STATO');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKTEC),'ATTIVITA','AT_OKTEC');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKGES),'ATTIVITA','AT_OKGES');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKAMM),'ATTIVITA','AT_OKAMM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT__RIGA),'ATTIVITA','AT__RIGA');
            +","+cp_NullLink(cp_ToStrODBC("S"),'ATTIVITA','AT_PRJVW');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'ATVINCOL',this.w_ATVINCOL,'ATTOTCOS',_Curs_ATTIVITA.ATTOTCOS,'ATTIPCOM',_Curs_ATTIVITA.ATTIPCOM,'ATTIPATT',_Curs_ATTIVITA.ATTIPATT,'ATGRPMAN',_Curs_ATTIVITA.ATGRPMAN,'ATFLPREV',"N",'ATDURGIO',_Curs_ATTIVITA.ATDURGIO,'ATDESSUP',_Curs_ATTIVITA.ATDESSUP,'ATDESCRI',_Curs_ATTIVITA.ATDESCRI,'ATDATVIN',this.w_NEWDATVI,'ATDATINI',this.w_NEWDATEI,'ATDATFIN',this.w_NEWDATEF)
            insert into (i_cTable) (ATVINCOL,ATTOTCOS,ATTIPCOM,ATTIPATT,ATGRPMAN,ATFLPREV,ATDURGIO,ATDESSUP,ATDESCRI,ATDATVIN,ATDATINI,ATDATFIN,ATCODFAM,ATCODCOM,ATCODATT,ATCENCOS,ATCARDIN,AT_STATO,AT_OKTEC,AT_OKGES,AT_OKAMM,AT__RIGA,AT_PRJVW &i_ccchkf. );
               values (;
                 this.w_ATVINCOL;
                 ,_Curs_ATTIVITA.ATTOTCOS;
                 ,_Curs_ATTIVITA.ATTIPCOM;
                 ,_Curs_ATTIVITA.ATTIPATT;
                 ,_Curs_ATTIVITA.ATGRPMAN;
                 ,"N";
                 ,_Curs_ATTIVITA.ATDURGIO;
                 ,_Curs_ATTIVITA.ATDESSUP;
                 ,_Curs_ATTIVITA.ATDESCRI;
                 ,this.w_NEWDATVI;
                 ,this.w_NEWDATEI;
                 ,this.w_NEWDATEF;
                 ,_Curs_ATTIVITA.ATCODFAM;
                 ,this.oParentObject.w_CODCOMD;
                 ,_Curs_ATTIVITA.ATCODATT;
                 ,_Curs_ATTIVITA.ATCENCOS;
                 ,_Curs_ATTIVITA.ATCARDIN;
                 ,"P";
                 ,_Curs_ATTIVITA.AT_OKTEC;
                 ,_Curs_ATTIVITA.AT_OKGES;
                 ,_Curs_ATTIVITA.AT_OKAMM;
                 ,_Curs_ATTIVITA.AT__RIGA;
                 ,"S";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Impossibile scrivere in ATTIVITA'
            return
          endif
            select _Curs_ATTIVITA
            continue
          enddo
          use
        endif
        * --- Select from ATT_PREC
        i_nConn=i_TableProp[this.ATT_PREC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2],.t.,this.ATT_PREC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATT_PREC ";
              +" where MPCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMS)+"";
               ,"_Curs_ATT_PREC")
        else
          select * from (i_cTable);
           where MPCODCOM=this.oParentObject.w_CODCOMS;
            into cursor _Curs_ATT_PREC
        endif
        if used('_Curs_ATT_PREC')
          select _Curs_ATT_PREC
          locate for 1=1
          do while not(eof())
          * --- Insert into ATT_PREC
          i_nConn=i_TableProp[this.ATT_PREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATT_PREC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATT_PREC_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MPCODCOM"+",MPTIPATT"+",MPCODATT"+",MPATTPRE"+",MPTIPPRE"+",MTRITAR"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'ATT_PREC','MPCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MPTIPATT),'ATT_PREC','MPTIPATT');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MPCODATT),'ATT_PREC','MPCODATT');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MPATTPRE),'ATT_PREC','MPATTPRE');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MPTIPPRE),'ATT_PREC','MPTIPPRE');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATT_PREC.MTRITAR),'ATT_PREC','MTRITAR');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MPCODCOM',this.oParentObject.w_CODCOMD,'MPTIPATT',_Curs_ATT_PREC.MPTIPATT,'MPCODATT',_Curs_ATT_PREC.MPCODATT,'MPATTPRE',_Curs_ATT_PREC.MPATTPRE,'MPTIPPRE',_Curs_ATT_PREC.MPTIPPRE,'MTRITAR',_Curs_ATT_PREC.MTRITAR)
            insert into (i_cTable) (MPCODCOM,MPTIPATT,MPCODATT,MPATTPRE,MPTIPPRE,MTRITAR &i_ccchkf. );
               values (;
                 this.oParentObject.w_CODCOMD;
                 ,_Curs_ATT_PREC.MPTIPATT;
                 ,_Curs_ATT_PREC.MPCODATT;
                 ,_Curs_ATT_PREC.MPATTPRE;
                 ,_Curs_ATT_PREC.MPTIPPRE;
                 ,_Curs_ATT_PREC.MTRITAR;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Impossibile scrivere in ATT_PREC'
            return
          endif
            select _Curs_ATT_PREC
            continue
          enddo
          use
        endif
        * --- 5) Ricopiare i selezionati
        str1="ATCODCOM='"+this.oParentObject.w_CODCOMS+"' and (ATTIPATT='"+this.oParentObject.w_CHKTECN+"' or "
        str2=" ATTIPATT='"+this.oParentObject.w_CHKGEST+"' or ATTIPATT='"+this.oParentObject.w_CHKAMM+"') "
        Str = Str1+Str2+" and ATTIPCOM<>'P'"
        * --- Select from ATTIVITA
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
              +" where &str";
               ,"_Curs_ATTIVITA")
        else
          select * from (i_cTable);
           where &str;
            into cursor _Curs_ATTIVITA
        endif
        if used('_Curs_ATTIVITA')
          select _Curs_ATTIVITA
          locate for 1=1
          do while not(eof())
          this.w_NEWDATEI = cp_todate(_Curs_ATTIVITA.ATDATINI)
          if not empty(this.w_NEWDATEI)
            this.w_NEWDATEI = this.w_NEWDATEI+this.w_DIFF
            * --- Se la data � definita si aggiunge semplicemente il lag
          else
            this.w_NEWDATEI = this.oParentObject.w_DATINID
            * --- Altrimenti si prende la data di inizio della nuova commessa
            * --- (questo per� non dovrebbe MAI accadere!)
          endif
          this.w_NEWDATEF = cp_todate(_Curs_ATTIVITA.ATDATFIN)
          if not empty(this.w_NEWDATEF)
            * --- Se la data di fine esiste viene aggiornata con il lag
            this.w_NEWDATEF = this.w_NEWDATEF+this.w_DIFF
            * --- (in caso contrario la CP_TODATE assicura il NULL)
          endif
          this.w_NEWDATVI = cp_todate(_Curs_ATTIVITA.ATDATVIN)
          if not empty(this.w_NEWDATVI)
            this.w_NEWDATVI = this.w_NEWDATVI+this.w_DIFF
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Insert into ATTIVITA
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"ATCODCOM"+",ATTIPATT"+",ATCODATT"+",ATTIPCOM"+",ATDESCRI"+",ATDESSUP"+",ATCENCOS"+",ATTOTCOS"+",ATDATINI"+",ATDATFIN"+",ATDURGIO"+",ATVINCOL"+",ATDATVIN"+",ATCARDIN"+",AT__RIGA"+",ATCODFAM"+",ATFLPREV"+",AT_STATO"+",AT_OKTEC"+",AT_OKAMM"+",AT_OKGES"+",ATGRPMAN"+",AT_PRJVW"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'ATTIVITA','ATCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTIPATT),'ATTIVITA','ATTIPATT');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCODATT),'ATTIVITA','ATCODATT');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTIPCOM),'ATTIVITA','ATTIPCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDESCRI),'ATTIVITA','ATDESCRI');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDESSUP),'ATTIVITA','ATDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCENCOS),'ATTIVITA','ATCENCOS');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTOTCOS),'ATTIVITA','ATTOTCOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATEI),'ATTIVITA','ATDATINI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATEF),'ATTIVITA','ATDATFIN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDURGIO),'ATTIVITA','ATDURGIO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ATVINCOL),'ATTIVITA','ATVINCOL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATVI),'ATTIVITA','ATDATVIN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCARDIN),'ATTIVITA','ATCARDIN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT__RIGA),'ATTIVITA','AT__RIGA');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCODFAM),'ATTIVITA','ATCODFAM');
            +","+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
            +","+cp_NullLink(cp_ToStrODBC("P"),'ATTIVITA','AT_STATO');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKTEC),'ATTIVITA','AT_OKTEC');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKAMM),'ATTIVITA','AT_OKAMM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKGES),'ATTIVITA','AT_OKGES');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATGRPMAN),'ATTIVITA','ATGRPMAN');
            +","+cp_NullLink(cp_ToStrODBC("S"),'ATTIVITA','AT_PRJVW');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'ATCODCOM',this.oParentObject.w_CODCOMD,'ATTIPATT',_Curs_ATTIVITA.ATTIPATT,'ATCODATT',_Curs_ATTIVITA.ATCODATT,'ATTIPCOM',_Curs_ATTIVITA.ATTIPCOM,'ATDESCRI',_Curs_ATTIVITA.ATDESCRI,'ATDESSUP',_Curs_ATTIVITA.ATDESSUP,'ATCENCOS',_Curs_ATTIVITA.ATCENCOS,'ATTOTCOS',_Curs_ATTIVITA.ATTOTCOS,'ATDATINI',this.w_NEWDATEI,'ATDATFIN',this.w_NEWDATEF,'ATDURGIO',_Curs_ATTIVITA.ATDURGIO,'ATVINCOL',this.w_ATVINCOL)
            insert into (i_cTable) (ATCODCOM,ATTIPATT,ATCODATT,ATTIPCOM,ATDESCRI,ATDESSUP,ATCENCOS,ATTOTCOS,ATDATINI,ATDATFIN,ATDURGIO,ATVINCOL,ATDATVIN,ATCARDIN,AT__RIGA,ATCODFAM,ATFLPREV,AT_STATO,AT_OKTEC,AT_OKAMM,AT_OKGES,ATGRPMAN,AT_PRJVW &i_ccchkf. );
               values (;
                 this.oParentObject.w_CODCOMD;
                 ,_Curs_ATTIVITA.ATTIPATT;
                 ,_Curs_ATTIVITA.ATCODATT;
                 ,_Curs_ATTIVITA.ATTIPCOM;
                 ,_Curs_ATTIVITA.ATDESCRI;
                 ,_Curs_ATTIVITA.ATDESSUP;
                 ,_Curs_ATTIVITA.ATCENCOS;
                 ,_Curs_ATTIVITA.ATTOTCOS;
                 ,this.w_NEWDATEI;
                 ,this.w_NEWDATEF;
                 ,_Curs_ATTIVITA.ATDURGIO;
                 ,this.w_ATVINCOL;
                 ,this.w_NEWDATVI;
                 ,_Curs_ATTIVITA.ATCARDIN;
                 ,_Curs_ATTIVITA.AT__RIGA;
                 ,_Curs_ATTIVITA.ATCODFAM;
                 ,"N";
                 ,"P";
                 ,_Curs_ATTIVITA.AT_OKTEC;
                 ,_Curs_ATTIVITA.AT_OKAMM;
                 ,_Curs_ATTIVITA.AT_OKGES;
                 ,_Curs_ATTIVITA.ATGRPMAN;
                 ,"S";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Impossibile scrivere in ATTIVITA'
            return
          endif
            select _Curs_ATTIVITA
            continue
          enddo
          use
        endif
        * --- Select from STRUTTUR
        i_nConn=i_TableProp[this.STRUTTUR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" STRUTTUR ";
              +" where STCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMS)+" and (STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_CHKTECN)+" or STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_CHKGEST)+" or STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_CHKAMM)+")";
               ,"_Curs_STRUTTUR")
        else
          select * from (i_cTable);
           where STCODCOM=this.oParentObject.w_CODCOMS and (STTIPSTR=this.oParentObject.w_CHKTECN or STTIPSTR=this.oParentObject.w_CHKGEST or STTIPSTR=this.oParentObject.w_CHKAMM);
            into cursor _Curs_STRUTTUR
        endif
        if used('_Curs_STRUTTUR')
          select _Curs_STRUTTUR
          locate for 1=1
          do while not(eof())
          * --- Reperisco il TIPCOM dell'attivit� padre
          this.w_COM = _Curs_STRUTTUR.STCODCOM
          this.w_TIP = _Curs_STRUTTUR.STTIPSTR
          this.w_ATT = _Curs_STRUTTUR.STATTPAD
          this.w_MILL = _Curs_STRUTTUR.STMILLES
          Vq_Exec("..\comm\exe\query\GSPC_BCX.vqr",this,"Cur_Tipcom")
          select ("Cur_Tipcom")
          go top
          this.w_TIPCOM = ATTIPCOM
          if this.w_TIPCOM<>"P"
            * --- Se l'attivit� padre NON � un capo progetto allora lego normalmente le attivit� in STRUTTUR
            * --- Insert into STRUTTUR
            i_nConn=i_TableProp[this.STRUTTUR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STATTFIG"+",STTIPFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'STRUTTUR','STCODCOM');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STTIPSTR),'STRUTTUR','STTIPSTR');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STATTPAD),'STRUTTUR','STATTPAD');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STDESCRI),'STRUTTUR','STDESCRI');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STATTFIG),'STRUTTUR','STATTFIG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STTIPFIG),'STRUTTUR','STTIPFIG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.CPROWORD),'STRUTTUR','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STMILLES),'STRUTTUR','STMILLES');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.oParentObject.w_CODCOMD,'STTIPSTR',_Curs_STRUTTUR.STTIPSTR,'STATTPAD',_Curs_STRUTTUR.STATTPAD,'STDESCRI',_Curs_STRUTTUR.STDESCRI,'STATTFIG',_Curs_STRUTTUR.STATTFIG,'STTIPFIG',_Curs_STRUTTUR.STTIPFIG,'CPROWORD',_Curs_STRUTTUR.CPROWORD,'STMILLES',_Curs_STRUTTUR.STMILLES)
              insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STATTFIG,STTIPFIG,CPROWORD,STMILLES &i_ccchkf. );
                 values (;
                   this.oParentObject.w_CODCOMD;
                   ,_Curs_STRUTTUR.STTIPSTR;
                   ,_Curs_STRUTTUR.STATTPAD;
                   ,_Curs_STRUTTUR.STDESCRI;
                   ,_Curs_STRUTTUR.STATTFIG;
                   ,_Curs_STRUTTUR.STTIPFIG;
                   ,_Curs_STRUTTUR.CPROWORD;
                   ,_Curs_STRUTTUR.STMILLES;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Impossibile scrivere in STRUTTUR'
              return
            endif
          else
            * --- Se l'attivit� padre E' un capo progetto allora in STRUTTUR devo legare l'attivit�
            * --- all'attivit� capoprogetto per quella commessa (STATTPAD=w_CODCOMD)
            * --- (N.B.: per i capiprogetto attivit� e commessa corrispondono)
            * --- Insert into STRUTTUR
            i_nConn=i_TableProp[this.STRUTTUR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STATTFIG"+",STTIPFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'STRUTTUR','STCODCOM');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STTIPSTR),'STRUTTUR','STTIPSTR');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'STRUTTUR','STATTPAD');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STDESCRI),'STRUTTUR','STDESCRI');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STATTFIG),'STRUTTUR','STATTFIG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STTIPFIG),'STRUTTUR','STTIPFIG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.CPROWORD),'STRUTTUR','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_STRUTTUR.STMILLES),'STRUTTUR','STMILLES');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.oParentObject.w_CODCOMD,'STTIPSTR',_Curs_STRUTTUR.STTIPSTR,'STATTPAD',this.oParentObject.w_CODCOMD,'STDESCRI',_Curs_STRUTTUR.STDESCRI,'STATTFIG',_Curs_STRUTTUR.STATTFIG,'STTIPFIG',_Curs_STRUTTUR.STTIPFIG,'CPROWORD',_Curs_STRUTTUR.CPROWORD,'STMILLES',_Curs_STRUTTUR.STMILLES)
              insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STATTFIG,STTIPFIG,CPROWORD,STMILLES &i_ccchkf. );
                 values (;
                   this.oParentObject.w_CODCOMD;
                   ,_Curs_STRUTTUR.STTIPSTR;
                   ,this.oParentObject.w_CODCOMD;
                   ,_Curs_STRUTTUR.STDESCRI;
                   ,_Curs_STRUTTUR.STATTFIG;
                   ,_Curs_STRUTTUR.STTIPFIG;
                   ,_Curs_STRUTTUR.CPROWORD;
                   ,_Curs_STRUTTUR.STMILLES;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Impossibile scrivere in STRUTTUR'
              return
            endif
          endif
            select _Curs_STRUTTUR
            continue
          enddo
          use
        endif
      else
        * --- Se NON si ricopiano le attivit� occorre:
        * --- 1) Cancellare solo i selezionati
        * --- 2) Ricopiare solo i selezionati (in STRUTTUR solo quelli che NON hanno figli attivit�)
        * --- 1) Cancellare solo i selezionati
        * --- Select from STRUTTUR
        i_nConn=i_TableProp[this.STRUTTUR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" STRUTTUR ";
              +" where STCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMD)+" and (STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_CHKTECN)+" or STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_CHKGEST)+" or STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_CHKAMM)+")";
               ,"_Curs_STRUTTUR")
        else
          select * from (i_cTable);
           where STCODCOM=this.oParentObject.w_CODCOMD and (STTIPSTR=this.oParentObject.w_CHKTECN or STTIPSTR=this.oParentObject.w_CHKGEST or STTIPSTR=this.oParentObject.w_CHKAMM);
            into cursor _Curs_STRUTTUR
        endif
        if used('_Curs_STRUTTUR')
          select _Curs_STRUTTUR
          locate for 1=1
          do while not(eof())
          * --- Delete from STRUTTUR
          i_nConn=i_TableProp[this.STRUTTUR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"STCODCOM = "+cp_ToStrODBC(_Curs_STRUTTUR.STCODCOM);
                  +" and STTIPSTR = "+cp_ToStrODBC(_Curs_STRUTTUR.STTIPSTR);
                  +" and STATTPAD = "+cp_ToStrODBC(_Curs_STRUTTUR.STATTPAD);
                   )
          else
            delete from (i_cTable) where;
                  STCODCOM = _Curs_STRUTTUR.STCODCOM;
                  and STTIPSTR = _Curs_STRUTTUR.STTIPSTR;
                  and STATTPAD = _Curs_STRUTTUR.STATTPAD;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error='Impossibile cancellare da STRUTTUR'
            return
          endif
            select _Curs_STRUTTUR
            continue
          enddo
          use
        endif
        * --- Select from ATTIVITA
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
              +" where ATCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMD)+" and (ATTIPATT="+cp_ToStrODBC(this.oParentObject.w_CHKTECN)+" or ATTIPATT="+cp_ToStrODBC(this.oParentObject.w_CHKGEST)+" or ATTIPATT="+cp_ToStrODBC(this.oParentObject.w_CHKAMM)+")";
               ,"_Curs_ATTIVITA")
        else
          select * from (i_cTable);
           where ATCODCOM=this.oParentObject.w_CODCOMD and (ATTIPATT=this.oParentObject.w_CHKTECN or ATTIPATT=this.oParentObject.w_CHKGEST or ATTIPATT=this.oParentObject.w_CHKAMM);
            into cursor _Curs_ATTIVITA
        endif
        if used('_Curs_ATTIVITA')
          select _Curs_ATTIVITA
          locate for 1=1
          do while not(eof())
          * --- Delete from ATTIVITA
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"ATCODCOM = "+cp_ToStrODBC(_Curs_ATTIVITA.ATCODCOM);
                  +" and ATTIPATT = "+cp_ToStrODBC(_Curs_ATTIVITA.ATTIPATT);
                  +" and ATCODATT = "+cp_ToStrODBC(_Curs_ATTIVITA.ATCODATT);
                  +" and NOT ATTIPCOM = "+cp_ToStrODBC("P");
                   )
          else
            delete from (i_cTable) where;
                  ATCODCOM = _Curs_ATTIVITA.ATCODCOM;
                  and ATTIPATT = _Curs_ATTIVITA.ATTIPATT;
                  and ATCODATT = _Curs_ATTIVITA.ATCODATT;
                  and NOT ATTIPCOM = "P";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error='Impossibile cancellare da ATTIVITA'
            return
          endif
            select _Curs_ATTIVITA
            continue
          enddo
          use
        endif
        * --- 2) Ricopiare solo i selezionati (in STRUTTUR solo quelli che NON hanno figli attivit�)
        str1="ATCODCOM='"+this.oParentObject.w_CODCOMS+"' and (ATTIPATT='"+this.oParentObject.w_CHKTECN+"' "
        str2="or ATTIPATT='"+this.oParentObject.w_CHKGEST+"' or ATTIPATT='"+this.oParentObject.w_CHKAMM+"') "
        Str=Str1+Str2+" and ATTIPCOM<>'P' "
        * --- Prova ad usare una Visual Query con il filtro sopra invece di costruirlo
        * --- Select from ATTIVITA
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
              +" where &str";
               ,"_Curs_ATTIVITA")
        else
          select * from (i_cTable);
           where &str;
            into cursor _Curs_ATTIVITA
        endif
        if used('_Curs_ATTIVITA')
          select _Curs_ATTIVITA
          locate for 1=1
          do while not(eof())
          this.w_NEWDATEI = cp_todate(_Curs_ATTIVITA.ATDATINI)
          if not empty(this.w_NEWDATEI)
            this.w_NEWDATEI = this.w_NEWDATEI+this.w_DIFF
            * --- Se la data � definita si aggiunge semplicemente il lag
          else
            this.w_NEWDATEI = this.oParentObject.w_DATINID
            * --- Altrimenti si prende la data di inizio della nuova commessa
            * --- (questo per� non dovrebbe MAI accadere!)
          endif
          this.w_NEWDATEF = cp_todate(_Curs_ATTIVITA.ATDATFIN)
          if not empty(this.w_NEWDATEF)
            * --- Se la data di fine esiste viene aggiornata con il lag
            this.w_NEWDATEF = this.w_NEWDATEF+this.w_DIFF
            * --- (in caso contrario la CP_TODATE assicura il NULL)
          endif
          this.w_NEWDATVI = cp_todate(_Curs_ATTIVITA.ATDATVIN)
          if not empty(this.w_NEWDATVI)
            this.w_NEWDATVI = this.w_NEWDATVI+this.w_DIFF
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Insert into ATTIVITA
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"AT__RIGA"+",AT_OKAMM"+",AT_OKGES"+",AT_OKTEC"+",AT_STATO"+",ATCARDIN"+",ATCENCOS"+",ATCODATT"+",ATCODCOM"+",ATCODFAM"+",ATDATFIN"+",ATDATINI"+",ATDATVIN"+",ATDESCRI"+",ATDESSUP"+",ATDURGIO"+",ATFLPREV"+",ATGRPMAN"+",ATTIPATT"+",ATTIPCOM"+",ATTOTCOS"+",ATVINCOL"+",AT_PRJVW"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT__RIGA),'ATTIVITA','AT__RIGA');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKAMM),'ATTIVITA','AT_OKAMM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKGES),'ATTIVITA','AT_OKGES');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.AT_OKTEC),'ATTIVITA','AT_OKTEC');
            +","+cp_NullLink(cp_ToStrODBC("P"),'ATTIVITA','AT_STATO');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCARDIN),'ATTIVITA','ATCARDIN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCENCOS),'ATTIVITA','ATCENCOS');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCODATT),'ATTIVITA','ATCODATT');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'ATTIVITA','ATCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATCODFAM),'ATTIVITA','ATCODFAM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATEI),'ATTIVITA','ATDATFIN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATEF),'ATTIVITA','ATDATINI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWDATVI),'ATTIVITA','ATDATVIN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDESCRI),'ATTIVITA','ATDESCRI');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDESSUP),'ATTIVITA','ATDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATDURGIO),'ATTIVITA','ATDURGIO');
            +","+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','ATFLPREV');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATGRPMAN),'ATTIVITA','ATGRPMAN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTIPATT),'ATTIVITA','ATTIPATT');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTIPCOM),'ATTIVITA','ATTIPCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_ATTIVITA.ATTOTCOS),'ATTIVITA','ATTOTCOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ATVINCOL),'ATTIVITA','ATVINCOL');
            +","+cp_NullLink(cp_ToStrODBC("S"),'ATTIVITA','AT_PRJVW');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'AT__RIGA',_Curs_ATTIVITA.AT__RIGA,'AT_OKAMM',_Curs_ATTIVITA.AT_OKAMM,'AT_OKGES',_Curs_ATTIVITA.AT_OKGES,'AT_OKTEC',_Curs_ATTIVITA.AT_OKTEC,'AT_STATO',"P",'ATCARDIN',_Curs_ATTIVITA.ATCARDIN,'ATCENCOS',_Curs_ATTIVITA.ATCENCOS,'ATCODATT',_Curs_ATTIVITA.ATCODATT,'ATCODCOM',this.oParentObject.w_CODCOMD,'ATCODFAM',_Curs_ATTIVITA.ATCODFAM,'ATDATFIN',this.w_NEWDATEI,'ATDATINI',this.w_NEWDATEF)
            insert into (i_cTable) (AT__RIGA,AT_OKAMM,AT_OKGES,AT_OKTEC,AT_STATO,ATCARDIN,ATCENCOS,ATCODATT,ATCODCOM,ATCODFAM,ATDATFIN,ATDATINI,ATDATVIN,ATDESCRI,ATDESSUP,ATDURGIO,ATFLPREV,ATGRPMAN,ATTIPATT,ATTIPCOM,ATTOTCOS,ATVINCOL,AT_PRJVW &i_ccchkf. );
               values (;
                 _Curs_ATTIVITA.AT__RIGA;
                 ,_Curs_ATTIVITA.AT_OKAMM;
                 ,_Curs_ATTIVITA.AT_OKGES;
                 ,_Curs_ATTIVITA.AT_OKTEC;
                 ,"P";
                 ,_Curs_ATTIVITA.ATCARDIN;
                 ,_Curs_ATTIVITA.ATCENCOS;
                 ,_Curs_ATTIVITA.ATCODATT;
                 ,this.oParentObject.w_CODCOMD;
                 ,_Curs_ATTIVITA.ATCODFAM;
                 ,this.w_NEWDATEI;
                 ,this.w_NEWDATEF;
                 ,this.w_NEWDATVI;
                 ,_Curs_ATTIVITA.ATDESCRI;
                 ,_Curs_ATTIVITA.ATDESSUP;
                 ,_Curs_ATTIVITA.ATDURGIO;
                 ,"N";
                 ,_Curs_ATTIVITA.ATGRPMAN;
                 ,_Curs_ATTIVITA.ATTIPATT;
                 ,_Curs_ATTIVITA.ATTIPCOM;
                 ,_Curs_ATTIVITA.ATTOTCOS;
                 ,this.w_ATVINCOL;
                 ,"S";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Impossibile scrivere in ATTIVITA'
            return
          endif
            select _Curs_ATTIVITA
            continue
          enddo
          use
        endif
        * --- preparazione dei parametri per la visual query
        * --- (uso una visual query perch� la clausola where sarebbe troppo lunga)
        * --- Select from GSPC_CS1
        do vq_exec with 'GSPC_CS1',this,'_Curs_GSPC_CS1','',.f.,.t.
        if used('_Curs_GSPC_CS1')
          select _Curs_GSPC_CS1
          locate for 1=1
          do while not(eof())
          * --- Reperisco il TIPCOM dell'attivit� padre
          this.w_COM = _Curs_GSPC_CS1.STCODCOM
          this.w_TIP = _Curs_GSPC_CS1.STTIPSTR
          this.w_ATT = _Curs_GSPC_CS1.STATTPAD
          Vq_Exec("..\comm\exe\query\GSPC_BCX.vqr",this,"Cur_Tipcom")
          select ("Cur_Tipcom")
          go top
          this.w_TIPCOM = ATTIPCOM
          if this.w_TIPCOM<>"P"
            * --- Se l'attivit� padre NON � un capo progetto allora lego normalmente le attivit� in STRUTTUR
            * --- Insert into STRUTTUR
            i_nConn=i_TableProp[this.STRUTTUR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STATTFIG"+",STTIPFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'STRUTTUR','STCODCOM');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STTIPSTR),'STRUTTUR','STTIPSTR');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STATTPAD),'STRUTTUR','STATTPAD');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STATTPAD),'STRUTTUR','STDESCRI');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STATTFIG),'STRUTTUR','STATTFIG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STTIPFIG),'STRUTTUR','STTIPFIG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.CPROWORD),'STRUTTUR','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STMILLES),'STRUTTUR','STMILLES');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.oParentObject.w_CODCOMD,'STTIPSTR',_Curs_GSPC_CS1.STTIPSTR,'STATTPAD',_Curs_GSPC_CS1.STATTPAD,'STDESCRI',_Curs_GSPC_CS1.STATTPAD,'STATTFIG',_Curs_GSPC_CS1.STATTFIG,'STTIPFIG',_Curs_GSPC_CS1.STTIPFIG,'CPROWORD',_Curs_GSPC_CS1.CPROWORD,'STMILLES',_Curs_GSPC_CS1.STMILLES)
              insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STATTFIG,STTIPFIG,CPROWORD,STMILLES &i_ccchkf. );
                 values (;
                   this.oParentObject.w_CODCOMD;
                   ,_Curs_GSPC_CS1.STTIPSTR;
                   ,_Curs_GSPC_CS1.STATTPAD;
                   ,_Curs_GSPC_CS1.STATTPAD;
                   ,_Curs_GSPC_CS1.STATTFIG;
                   ,_Curs_GSPC_CS1.STTIPFIG;
                   ,_Curs_GSPC_CS1.CPROWORD;
                   ,_Curs_GSPC_CS1.STMILLES;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Impossibile scrivere in STRUTTUR'
              return
            endif
          else
            * --- Se l'attivit� padre E' un capo progetto allora in STRUTTUR devo legare l'attivit�
            * --- all'attivit� capoprogetto per quella commessa (STATTPAD=w_CODCOMD)
            * --- (N.B.: per i capiprogetto attivit� e commessa corrispondono)
            * --- Insert into STRUTTUR
            i_nConn=i_TableProp[this.STRUTTUR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRUTTUR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"STCODCOM"+",STTIPSTR"+",STATTPAD"+",STDESCRI"+",STATTFIG"+",STTIPFIG"+",CPROWORD"+",STMILLES"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'STRUTTUR','STCODCOM');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STTIPSTR),'STRUTTUR','STTIPSTR');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'STRUTTUR','STATTPAD');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STATTPAD),'STRUTTUR','STDESCRI');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STATTFIG),'STRUTTUR','STATTFIG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STTIPFIG),'STRUTTUR','STTIPFIG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.CPROWORD),'STRUTTUR','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_GSPC_CS1.STMILLES),'STRUTTUR','STMILLES');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'STCODCOM',this.oParentObject.w_CODCOMD,'STTIPSTR',_Curs_GSPC_CS1.STTIPSTR,'STATTPAD',this.oParentObject.w_CODCOMD,'STDESCRI',_Curs_GSPC_CS1.STATTPAD,'STATTFIG',_Curs_GSPC_CS1.STATTFIG,'STTIPFIG',_Curs_GSPC_CS1.STTIPFIG,'CPROWORD',_Curs_GSPC_CS1.CPROWORD,'STMILLES',_Curs_GSPC_CS1.STMILLES)
              insert into (i_cTable) (STCODCOM,STTIPSTR,STATTPAD,STDESCRI,STATTFIG,STTIPFIG,CPROWORD,STMILLES &i_ccchkf. );
                 values (;
                   this.oParentObject.w_CODCOMD;
                   ,_Curs_GSPC_CS1.STTIPSTR;
                   ,this.oParentObject.w_CODCOMD;
                   ,_Curs_GSPC_CS1.STATTPAD;
                   ,_Curs_GSPC_CS1.STATTFIG;
                   ,_Curs_GSPC_CS1.STTIPFIG;
                   ,_Curs_GSPC_CS1.CPROWORD;
                   ,_Curs_GSPC_CS1.STMILLES;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Impossibile scrivere in STRUTTUR'
              return
            endif
          endif
            select _Curs_GSPC_CS1
            continue
          enddo
          use
        endif
      endif
    endif
    if this.oParentObject.w_CHKATT="A"
      this.w_ERRNO = GSPC_BTE(this,this.oParentObject.w_CODCOMD,cp_CharToDate("  -  -    "),cp_CharToDate("  -  -    "))
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ERRORMSG("Copia completata con successo",64)
    return
  proc Try_03699E50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOMD);
            +" and NOT ATTIPCOM = "+cp_ToStrODBC("P");
             )
    else
      delete from (i_cTable) where;
            ATCODCOM = this.oParentObject.w_CODCOMD;
            and NOT ATTIPCOM = "P";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Impossibile cancellare da ATTIVITA'
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if used("CurPreds")
      select("CurPreds")
      Use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla vincoli PJ
    this.w_ATVINCOL = _Curs_ATTIVITA.ATVINCOL
    if _Curs_ATTIVITA.ATTIPATT<>"A" and not inlist(this.w_ATVINCOL, "PPP", "FNO", "INP")
      * --- I nodi della treeview di Project non supportano altri tipi di vincoli
      if this.w_PROGR="A" or empty(nvl(_Curs_ATTIVITA.ATDATVIN,{}))
        * --- Se programmazione in avanti: default=Al pi� presto possibile
        this.w_ATVINCOL = "PPP"
      else
        * --- Se programmazione all'indietro: default=Finire Non Oltre
        this.w_ATVINCOL = "FNO"
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ATTIVITA'
    this.cWorkTables[2]='STRUTTUR'
    this.cWorkTables[3]='ATT_PREC'
    this.cWorkTables[4]='CPAR_DEF'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    if used('_Curs_STRUTTUR')
      use in _Curs_STRUTTUR
    endif
    if used('_Curs_ATT_PREC')
      use in _Curs_ATT_PREC
    endif
    if used('_Curs_STRUTTUR')
      use in _Curs_STRUTTUR
    endif
    if used('_Curs_STRUTTUR')
      use in _Curs_STRUTTUR
    endif
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    if used('_Curs_ATT_PREC')
      use in _Curs_ATT_PREC
    endif
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    if used('_Curs_ATT_PREC')
      use in _Curs_ATT_PREC
    endif
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    if used('_Curs_STRUTTUR')
      use in _Curs_STRUTTUR
    endif
    if used('_Curs_STRUTTUR')
      use in _Curs_STRUTTUR
    endif
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    if used('_Curs_GSPC_CS1')
      use in _Curs_GSPC_CS1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
