* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_spf                                                        *
*              Pianificazione fabbisogni                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_51]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-03                                                      *
* Last revis.: 2011-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_spf",oParentObject))

* --- Class definition
define class tgspc_spf as StdForm
  Top    = 7
  Left   = 17

  * --- Standard Properties
  Width  = 732
  Height = 427
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-30"
  HelpContextID=141187177
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  MAGAZZIN_IDX = 0
  CPAR_DEF_IDX = 0
  TIP_DOCU_IDX = 0
  CAM_AGAZ_IDX = 0
  VALUTE_IDX = 0
  MAT_MAST_IDX = 0
  LISTINI_IDX = 0
  cPrg = "gspc_spf"
  cComment = "Pianificazione fabbisogni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READPAR = space(10)
  w_DEFCOM = space(15)
  w_CODCOM = space(15)
  w_TIPDOCU = space(5)
  w_CODMAG = space(5)
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_DOINDATA = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_DESMAG = space(30)
  w_DESTIP = space(30)
  w_FLIMPE = space(1)
  w_CATDOC = space(2)
  w_FLINTE = space(1)
  w_MVFLACCO = space(1)
  w_ALFDOC = space(2)
  w_MVFLVEAC = space(1)
  w_PRD = space(2)
  w_MVCAUCON = space(5)
  w_MVCAUMAG = space(5)
  w_FLAVA = space(1)
  w_MVFLRAGG = space(1)
  w_CODMAG1 = space(5)
  o_CODMAG1 = space(5)
  w_SELALL = 0
  w_SERIAL = space(10)
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_MV_SEGNO = space(1)
  w_DECCOM = 0
  w_FLANAL = space(1)
  w_MVEMERIC = space(1)
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_spfPag1","gspc_spf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='CPAR_DEF'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='MAT_MAST'
    this.cWorkTables[8]='LISTINI'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSPC_BPF with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gspc_spf
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR=space(10)
      .w_DEFCOM=space(15)
      .w_CODCOM=space(15)
      .w_TIPDOCU=space(5)
      .w_CODMAG=space(5)
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_DOINDATA=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESMAG=space(30)
      .w_DESTIP=space(30)
      .w_FLIMPE=space(1)
      .w_CATDOC=space(2)
      .w_FLINTE=space(1)
      .w_MVFLACCO=space(1)
      .w_ALFDOC=space(2)
      .w_MVFLVEAC=space(1)
      .w_PRD=space(2)
      .w_MVCAUCON=space(5)
      .w_MVCAUMAG=space(5)
      .w_FLAVA=space(1)
      .w_MVFLRAGG=space(1)
      .w_CODMAG1=space(5)
      .w_SELALL=0
      .w_SERIAL=space(10)
      .w_CODVAL=space(3)
      .w_CAOVAL=0
      .w_MV_SEGNO=space(1)
      .w_DECCOM=0
      .w_FLANAL=space(1)
      .w_MVEMERIC=space(1)
        .w_READPAR = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODCOM = .w_DEFCOM
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODCOM))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TIPDOCU))
          .link_1_4('Full')
        endif
        .w_CODMAG = .w_CODMAG1
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODMAG))
          .link_1_5('Full')
        endif
        .w_DATAINI = i_DATSYS-30
        .w_DATAFIN = i_DATSYS
          .DoRTCalc(8,9,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(11,20,.f.)
        if not(empty(.w_MVCAUCON))
          .link_1_27('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_MVCAUMAG))
          .link_1_28('Full')
        endif
      .oPgFrm.Page1.oPag.Zoom.Calculate()
          .DoRTCalc(22,24,.f.)
        .w_SELALL = 0
        .w_SERIAL = NVL(.w_ZOOM.Getvar("MASERIAL"),'')
      .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CODVAL))
          .link_1_37('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate(AH_Msgformat("In giallo le attivit� senza mov. preventivo"),0,65535)
    endwith
    this.DoRTCalc(28,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
        if .o_CODMAG1<>.w_CODMAG1
            .w_CODMAG = .w_CODMAG1
          .link_1_5('Full')
        endif
        .DoRTCalc(6,19,.t.)
          .link_1_27('Full')
          .link_1_28('Full')
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(22,25,.t.)
            .w_SERIAL = NVL(.w_ZOOM.Getvar("MASERIAL"),'')
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
          .link_1_37('Full')
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(AH_Msgformat("In giallo le attivit� senza mov. preventivo"),0,65535)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(AH_Msgformat("In giallo le attivit� senza mov. preventivo"),0,65535)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODMAG,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODMAG,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_CODMAG1 = NVL(_Link_.PDCODMAG,space(5))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_CODMAG1 = space(5)
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_3'),i_cWhere,'',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_CODVAL = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_CODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPDOCU
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOCU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOCU)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG,TDCATDOC,TDFLINTE,TFFLRAGG,TDFLVEAC,TDEMERIC,TDFLACCO,TDPRODOC,TDALFDOC,TDCAUCON,TD_SEGNO,TDFLANAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOCU))
          select TDTIPDOC,TDDESDOC,TDCAUMAG,TDCATDOC,TDFLINTE,TFFLRAGG,TDFLVEAC,TDEMERIC,TDFLACCO,TDPRODOC,TDALFDOC,TDCAUCON,TD_SEGNO,TDFLANAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOCU)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOCU) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOCU_1_4'),i_cWhere,'',"Causali documenti",'GSPC_SPF.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG,TDCATDOC,TDFLINTE,TFFLRAGG,TDFLVEAC,TDEMERIC,TDFLACCO,TDPRODOC,TDALFDOC,TDCAUCON,TD_SEGNO,TDFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCAUMAG,TDCATDOC,TDFLINTE,TFFLRAGG,TDFLVEAC,TDEMERIC,TDFLACCO,TDPRODOC,TDALFDOC,TDCAUCON,TD_SEGNO,TDFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOCU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG,TDCATDOC,TDFLINTE,TFFLRAGG,TDFLVEAC,TDEMERIC,TDFLACCO,TDPRODOC,TDALFDOC,TDCAUCON,TD_SEGNO,TDFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOCU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOCU)
            select TDTIPDOC,TDDESDOC,TDCAUMAG,TDCATDOC,TDFLINTE,TFFLRAGG,TDFLVEAC,TDEMERIC,TDFLACCO,TDPRODOC,TDALFDOC,TDCAUCON,TD_SEGNO,TDFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOCU = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESTIP = NVL(_Link_.TDDESDOC,space(30))
      this.w_MVCAUMAG = NVL(_Link_.TDCAUMAG,space(5))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVFLRAGG = NVL(_Link_.TFFLRAGG,space(1))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_MVEMERIC = NVL(_Link_.TDEMERIC,space(1))
      this.w_MVFLACCO = NVL(_Link_.TDFLACCO,space(1))
      this.w_PRD = NVL(_Link_.TDPRODOC,space(2))
      this.w_ALFDOC = NVL(_Link_.TDALFDOC,space(2))
      this.w_MVCAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_MV_SEGNO = NVL(_Link_.TD_SEGNO,space(1))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOCU = space(5)
      endif
      this.w_DESTIP = space(30)
      this.w_MVCAUMAG = space(5)
      this.w_CATDOC = space(2)
      this.w_FLINTE = space(1)
      this.w_MVFLRAGG = space(1)
      this.w_MVFLVEAC = space(1)
      this.w_MVEMERIC = space(1)
      this.w_MVFLACCO = space(1)
      this.w_PRD = space(2)
      this.w_ALFDOC = space(2)
      this.w_MVCAUCON = space(5)
      this.w_MV_SEGNO = space(1)
      this.w_FLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOC='DI' And .w_FLINTE='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente o incongruente")
        endif
        this.w_TIPDOCU = space(5)
        this.w_DESTIP = space(30)
        this.w_MVCAUMAG = space(5)
        this.w_CATDOC = space(2)
        this.w_FLINTE = space(1)
        this.w_MVFLRAGG = space(1)
        this.w_MVFLVEAC = space(1)
        this.w_MVEMERIC = space(1)
        this.w_MVFLACCO = space(1)
        this.w_PRD = space(2)
        this.w_ALFDOC = space(2)
        this.w_MVCAUCON = space(5)
        this.w_MV_SEGNO = space(1)
        this.w_FLANAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOCU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_5'),i_cWhere,'',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCAUCON
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLAVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVCAUCON)
            select CMCODICE,CMFLAVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCAUCON = NVL(_Link_.CMCODICE,space(5))
      this.w_FLAVA = NVL(_Link_.CMFLAVAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCAUCON = space(5)
      endif
      this.w_FLAVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCAUMAG
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLIMPE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVCAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVCAUMAG)
            select CMCODICE,CMFLIMPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_FLIMPE = NVL(_Link_.CMFLIMPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCAUMAG = space(5)
      endif
      this.w_FLIMPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECCOM = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DECCOM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_3.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_3.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOCU_1_4.value==this.w_TIPDOCU)
      this.oPgFrm.Page1.oPag.oTIPDOCU_1_4.value=this.w_TIPDOCU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_5.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_5.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_7.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_7.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_8.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_8.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDOINDATA_1_9.value==this.w_DOINDATA)
      this.oPgFrm.Page1.oPag.oDOINDATA_1_9.value=this.w_DOINDATA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_11.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_11.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_17.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_17.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIP_1_19.value==this.w_DESTIP)
      this.oPgFrm.Page1.oPag.oDESTIP_1_19.value=this.w_DESTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oSELALL_1_33.RadioValue()==this.w_SELALL)
      this.oPgFrm.Page1.oPag.oSELALL_1_33.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TIPDOCU)) or not(.w_CATDOC='DI' And .w_FLINTE='N'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPDOCU_1_4.SetFocus()
            i_bnoObbl = !empty(.w_TIPDOCU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente o incongruente")
          case   (empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODMAG1 = this.w_CODMAG1
    return

enddefine

* --- Define pages as container
define class tgspc_spfPag1 as StdContainer
  Width  = 728
  height = 427
  stdWidth  = 728
  stdheight = 427
  resizeYpos=272
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM_1_3 as StdField with uid="YFPEVUDVJL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 164444966,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=90, Top=10, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco commesse",'',this.parent.oContained
  endproc

  add object oTIPDOCU_1_4 as StdField with uid="PTBCOWYEDG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TIPDOCU", cQueryName = "TIPDOCU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente o incongruente",;
    ToolTipText = "Causale del documento da generare (documento interno senza intestatario)",;
    HelpContextID = 265221686,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=90, Top=57, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOCU"

  func oTIPDOCU_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOCU_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOCU_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOCU_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documenti",'GSPC_SPF.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oCODMAG_1_5 as StdField with uid="LYBZDKUKNT",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino del documento da generare",;
    HelpContextID = 49756966,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=458, Top=57, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco magazzini",'',this.parent.oContained
  endproc


  add object oBtn_1_6 as StdButton with uid="LHEHSFNBHF",left=673, top=10, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Prenere per visualizzare le attivit� disponibili";
    , HelpContextID = 19030269;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        .NotifyEvent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATAINI_1_7 as StdField with uid="BPFJLFGRUG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale intervallo inizio attivit�",;
    HelpContextID = 93573834,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=90, Top=92

  add object oDATAFIN_1_8 as StdField with uid="KXUCHGHDUR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale intervallo inizio attivit�",;
    HelpContextID = 87829814,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=241, Top=92

  add object oDOINDATA_1_9 as StdField with uid="BFRXSZPKHC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DOINDATA", cQueryName = "DOINDATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Se vuota, si utilizza la data della scheda tecnica",;
    HelpContextID = 220760951,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=642, Top=92


  add object oBtn_1_10 as StdButton with uid="EGRZSAELTN",left=620, top=376, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Avvia pianificazione";
    , HelpContextID = 141158426;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        do GSPC_BPF with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CODCOM) And Not Empty(.w_CODMAG) And Not Empty(.w_TIPDOCU))
      endwith
    endif
  endfunc

  add object oDESCOM_1_11 as StdField with uid="ZNZVOXFMHZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 164503862,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=222, Top=10, InputMask=replicate('X',30)

  add object oDESMAG_1_17 as StdField with uid="MMUQKUJQIK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 49815862,;
   bGlobalFont=.t.,;
    Height=21, Width=195, Left=520, Top=57, InputMask=replicate('X',30)

  add object oDESTIP_1_19 as StdField with uid="OPLPYKWZYV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESTIP", cQueryName = "DESTIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 209658166,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=158, Top=57, InputMask=replicate('X',30)


  add object Zoom as cp_szoombox with uid="IAMNNRJGUZ",left=4, top=124, width=719,height=249,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='ATTIVITA',cZoomFile='GSPC1SPF',bOptions=.F.,bAdvOptions=.F.,bReadOnly=.t.,bQueryOnLoad=.F.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 36810470

  add object oSELALL_1_33 as StdRadio with uid="ULZHLIAPFO",rtseq=25,rtrep=.f.,left=16, top=379, width=136,height=32;
    , TabStop=.f.;
    , cFormVar="w_SELALL", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELALL_1_33.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 144421414
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 144421414
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELALL_1_33.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,0,;
    0)))
  endfunc
  func oSELALL_1_33.GetRadio()
    this.Parent.oContained.w_SELALL = this.RadioValue()
    return .t.
  endfunc

  func oSELALL_1_33.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SELALL==1,1,;
      iif(this.Parent.oContained.w_SELALL==0,2,;
      0))
  endfunc


  add object oBtn_1_34 as StdButton with uid="BKKLNJTMXQ",left=161, top=376, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il movimento selezionato";
    , HelpContextID = 86069663;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSPC_BMP(this.Parent.oContained,"LANCIA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(Nvl(.w_SERIAL,'')))
      endwith
    endif
  endfunc


  add object oObj_1_36 as cp_runprogram with uid="HFNLRALJJI",left=41, top=530, width=244,height=19,;
    caption='GSPC_BMP',;
   bGlobalFont=.t.,;
    prg='GSPC_BMP("SELECTALL")',;
    cEvent = "w_SELALL Changed",;
    nPag=1;
    , ToolTipText = "Seleziona - deseleziona elementi zoom";
    , HelpContextID = 3276874


  add object oObj_1_39 as cp_calclbl with uid="EFXTUJQADW",left=247, top=379, width=285,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="In Giallo le Attivit� senza Mov. Preventivo",;
    nPag=1;
    , HelpContextID = 36810470


  add object oBtn_1_41 as StdButton with uid="KOTWJAFPMC",left=673, top=376, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 133869754;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_12 as StdString with uid="PQQKOUZZWG",Visible=.t., Left=3, Top=10,;
    Alignment=1, Width=84, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="XAAZEFPRYP",Visible=.t., Left=3, Top=92,;
    Alignment=1, Width=84, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="MZIEOPLXWF",Visible=.t., Left=175, Top=92,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DAYEGLRGGO",Visible=.t., Left=374, Top=57,;
    Alignment=1, Width=79, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="GHZYUWPQEE",Visible=.t., Left=3, Top=57,;
    Alignment=1, Width=84, Height=15,;
    Caption="Tipo doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="ZLOCPZIRUU",Visible=.t., Left=330, Top=92,;
    Alignment=1, Width=310, Height=15,;
    Caption="Data di registrazione del documento interno generato:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_spf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
