* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bsm                                                        *
*              Gestione millesimi (gspc_mst)                                   *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_13]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-18                                                      *
* Last revis.: 2008-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bsm",oParentObject)
return(i_retval)

define class tgspc_bsm as StdBatch
  * --- Local variables
  w_CONTA = 0
  w_PARENT = .NULL.
  w_MILRIGA = 0
  w_RESTO = 0
  w_MESS = space(100)
  w_FIRSTROW = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Millesimi
    this.w_PARENT = This.oParentObject
    * --- Spartisco su ogni riga i millesimi. Il calcolo divide
    * --- semplicemente 1000 per il numero di righe e riporta nella prima il resto
    SELECT (this.w_PARENT.cTrsName)
    Count For (t_STATTFIG<>space(15)) and (I_SRV="A" or not deleted()) to this.w_CONTA
    * --- Calcolo il quantitativo per riga
    if this.w_CONTA<>0
      this.w_MILRIGA = Int(1000/ this.w_CONTA )
      this.w_RESTO = Mod (1000 , this.w_CONTA)
      this.w_FIRSTROW = .T.
      * --- Aggiorno il dettaglio
      this.oParentObject.w_TOTMIL = 0
      SELECT (this.w_PARENT.cTrsName)
      Go Top
      Scan For (t_STATTFIG<>space(15)) and (I_SRV="A" or not deleted())
      this.w_PARENT.WorkFromTrs()     
      this.w_PARENT.SaveDependsOn()     
      if this.w_FIRSTROW
        this.oParentObject.w_STMILLES = this.w_MILRIGA+this.w_RESTO
        this.w_FIRSTROW = .F.
      else
        this.oParentObject.w_STMILLES = this.w_MILRIGA
      endif
      this.oParentObject.w_VALMIL = this.oParentObject.w_STMILLES
      this.oParentObject.w_TOTMIL = this.oParentObject.w_TOTMIL+this.oParentObject.w_VALMIL
      this.w_PARENT.TrsFromWork()     
      if i_SRV<>"A"
        Replace i_SRV with "U"
      endif
      EndScan
      SELECT (this.w_PARENT.cTrsName)
      Go Top
      this.w_PARENT.WorkFromTrs()     
      this.w_PARENT.SaveDependsOn()     
      this.w_PARENT.SetControlsValue()     
      this.w_PARENT.ChildrenChangeRow()     
      this.w_PARENT.oPgFrm.Page1.oPag.oBody.Refresh()     
      this.w_PARENT.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
      this.w_PARENT.oPgFrm.Page1.oPag.oBody.nRelRow = 1
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
