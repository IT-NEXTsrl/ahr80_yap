* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bim                                                        *
*              Interfaccia x mastrini mov.                                     *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-07-09                                                      *
* Last revis.: 2010-09-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pWhoCall
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bim",oParentObject,m.pWhoCall)
return(i_retval)

define class tgspc_bim as StdBatch
  * --- Local variables
  pWhoCall = space(10)
  w_CODCOM = space(15)
  w_DESCOM = space(30)
  w_DATAINI = ctod("  /  /  ")
  w_TIPMOV = space(1)
  w_OBJMAST = .NULL.
  w_CODATT = space(15)
  w_DESATT = space(30)
  w_DATAFIN = ctod("  /  /  ")
  w_TIPOSTRU = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per Invocare i mastrini, il parametro mi dice chi lo lancia e quindi quali variabili esterne utilizzare
    * --- Variabili utilizzate come esterne dalla Maschera
    * --- Di default tutto Vuoto
    this.w_DATAINI = cp_CharToDate("  -  -  ")
    this.w_DATAFIN = cp_CharToDate("  -  -  ")
    this.w_TIPMOV = ""
    do case
      case this.pWhoCall="Attivita"
        this.w_CODCOM = this.oParentObject.w_ATCODCOM
        this.w_CODATT = this.oParentObject.w_ATCODATT
        this.w_DESCOM = this.oParentObject.w_DESCOMG
        this.w_DESATT = this.oParentObject.w_ATDESCRI
        this.w_TIPOSTRU = this.oParentObject.w_TIPO
      case LEFT(this.pWhoCall,4)="Menu"
        * --- Dal Menu posso passare solo un parametro. L'ultimo caratttere mi dice il tipo struttura (P,A,G)
        this.w_TIPOSTRU = Right( Alltrim( this.pWhoCall ),1 )
        this.w_CODATT = ""
        this.w_CODCOM = ""
    endcase
    * --- Lancio la maschera mastrino
    this.w_OBJMAST = GSPC_SMM(This)
    if (Not Empty(this.w_CODCOM)) And Not Empty(this.w_CODATT)
      * --- Riempio lo Zoom se ho gi� i campi obbligatori
      this.w_OBJMAST.NotifyEvent("Reload")     
    endif
  endproc


  proc Init(oParentObject,pWhoCall)
    this.pWhoCall=pWhoCall
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- gspc_bim
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    return(lower('GSPC_BIM,Null ' + ','+iif(type("this.pWhoCall")<>'C','Null', lower(Right( Alltrim( this.pWhoCall ),1 ))  )) )
  EndFunc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pWhoCall"
endproc
