* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_src                                                        *
*              Riepilogo costi                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_115]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-11                                                      *
* Last revis.: 2010-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_src",oParentObject))

* --- Class definition
define class tgspc_src as StdForm
  Top    = 50
  Left   = 74

  * --- Standard Properties
  Width  = 467
  Height = 324
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-06-21"
  HelpContextID=107632745
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  ATTIVITA_IDX = 0
  TIPCOSTO_IDX = 0
  MAGAZZIN_IDX = 0
  CPAR_DEF_IDX = 0
  cPrg = "gspc_src"
  cComment = "Riepilogo costi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READPAR = space(10)
  w_DEFCOM = space(15)
  w_OBJPADRE = space(10)
  w_AMMI = space(1)
  w_CODCOM3 = space(15)
  w_CODCOM = space(15)
  w_CODCOM2 = space(15)
  w_CODCONTO = space(15)
  w_CODCOS1 = space(5)
  w_CODCOS2 = space(5)
  w_TIPSTAMPA = space(1)
  w_FILTRO = space(1)
  w_TIPO = space(1)
  w_WITHATTI = space(1)
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_CODMAG1 = space(5)
  w_CODMAG2 = space(5)
  w_NOZERI = space(1)
  w_DESCOM = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_CURSORNA = space(10)
  w_CODVAL = space(3)
  w_DECTOT = 0
  w_DESCON = space(30)
  w_DESCOS1 = space(30)
  w_DESCOS2 = space(30)
  w_DESCOM2 = space(30)
  w_OK = space(10)
  w_ANACOSTI = space(1)
  o_ANACOSTI = space(1)
  w_ANARICAV = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_srcPag1","gspc_src",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM3_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='ATTIVITA'
    this.cWorkTables[4]='TIPCOSTO'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='CPAR_DEF'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSPC_BS3 with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gspc_src
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR=space(10)
      .w_DEFCOM=space(15)
      .w_OBJPADRE=space(10)
      .w_AMMI=space(1)
      .w_CODCOM3=space(15)
      .w_CODCOM=space(15)
      .w_CODCOM2=space(15)
      .w_CODCONTO=space(15)
      .w_CODCOS1=space(5)
      .w_CODCOS2=space(5)
      .w_TIPSTAMPA=space(1)
      .w_FILTRO=space(1)
      .w_TIPO=space(1)
      .w_WITHATTI=space(1)
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_CODMAG1=space(5)
      .w_CODMAG2=space(5)
      .w_NOZERI=space(1)
      .w_DESCOM=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_CURSORNA=space(10)
      .w_CODVAL=space(3)
      .w_DECTOT=0
      .w_DESCON=space(30)
      .w_DESCOS1=space(30)
      .w_DESCOS2=space(30)
      .w_DESCOM2=space(30)
      .w_OK=space(10)
      .w_ANACOSTI=space(1)
      .w_ANARICAV=space(1)
        .w_READPAR = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_OBJPADRE = .oParentObject
        .w_AMMI = 'P'
        .w_CODCOM3 = IIF ( Not IsNull( .w_OBJPADRE ) , .w_OBJPADRE.w_CODCOM , .w_DEFCOM )
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODCOM3))
          .link_1_5('Full')
        endif
        .w_CODCOM = .w_CODCOM3
        .w_CODCOM2 = .w_CODCOM3
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODCOM2))
          .link_1_7('Full')
        endif
        .w_CODCONTO = IIF( Not IsNull( .w_OBJPADRE ) , .w_OBJPADRE.w_CODATT,'')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODCONTO))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODCOS1))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODCOS2))
          .link_1_10('Full')
        endif
        .w_TIPSTAMPA = 'S'
        .w_FILTRO = 'N'
        .w_TIPO = 'A'
        .w_WITHATTI = 'N'
        .w_DATAINI = i_datsys-30
        .w_DATAFIN = i_datsys
          .DoRTCalc(17,18,.f.)
        .w_NOZERI = 'N'
          .DoRTCalc(20,20,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(22,23,.f.)
        if not(empty(.w_CODVAL))
          .link_1_25('Full')
        endif
          .DoRTCalc(24,28,.f.)
        .w_OK = .T.
        .w_ANACOSTI = "S"
        .w_ANARICAV = IIF(.w_ANACOSTI='S', "S", 'X')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,5,.t.)
            .w_CODCOM = .w_CODCOM3
        .DoRTCalc(7,22,.t.)
          .link_1_25('Full')
        .DoRTCalc(24,30,.t.)
        if .o_ANACOSTI<>.w_ANACOSTI
            .w_ANARICAV = IIF(.w_ANACOSTI='S', "S", 'X')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOM3_1_5.enabled = this.oPgFrm.Page1.oPag.oCODCOM3_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODCOM2_1_7.enabled = this.oPgFrm.Page1.oPag.oCODCOM2_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCODCONTO_1_8.enabled = this.oPgFrm.Page1.oPag.oCODCONTO_1_8.mCond()
    this.oPgFrm.Page1.oPag.oTIPO_1_13.enabled = this.oPgFrm.Page1.oPag.oTIPO_1_13.mCond()
    this.oPgFrm.Page1.oPag.oWITHATTI_1_14.enabled = this.oPgFrm.Page1.oPag.oWITHATTI_1_14.mCond()
    this.oPgFrm.Page1.oPag.oANARICAV_1_46.enabled = this.oPgFrm.Page1.oPag.oANARICAV_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODCONTO_1_8.visible=!this.oPgFrm.Page1.oPag.oCODCONTO_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCODCOS1_1_9.visible=!this.oPgFrm.Page1.oPag.oCODCOS1_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCODCOS2_1_10.visible=!this.oPgFrm.Page1.oPag.oCODCOS2_1_10.mHide()
    this.oPgFrm.Page1.oPag.oTIPO_1_13.visible=!this.oPgFrm.Page1.oPag.oTIPO_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDATAINI_1_15.visible=!this.oPgFrm.Page1.oPag.oDATAINI_1_15.mHide()
    this.oPgFrm.Page1.oPag.oDATAFIN_1_16.visible=!this.oPgFrm.Page1.oPag.oDATAFIN_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_33.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oDESCOS1_1_36.visible=!this.oPgFrm.Page1.oPag.oDESCOS1_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oDESCOS2_1_38.visible=!this.oPgFrm.Page1.oPag.oDESCOS2_1_38.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM3
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM3)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM3))
          select CNCODCAN,CNDESCAN,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM3)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM3) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM3_1_5'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM3)
            select CNCODCAN,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM3 = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_CODVAL = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM3 = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_CODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOM2>=.w_CODCOM3 or empty(.w_CODCOM2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOM3 = space(15)
        this.w_DESCOM = space(30)
        this.w_CODVAL = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM2
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM2)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM2))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM2)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM2) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM2_1_7'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM2)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM2 = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM2 = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM2 = space(15)
      endif
      this.w_DESCOM2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOM2>=.w_CODCOM3
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOM2 = space(15)
        this.w_DESCOM2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONTO
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODCONTO)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_AMMI);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_AMMI;
                     ,'ATCODATT',trim(this.w_CODCONTO))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCONTO)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCONTO) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODCONTO_1_8'),i_cWhere,'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_AMMI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_AMMI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODCONTO);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_AMMI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_AMMI;
                       ,'ATCODATT',this.w_CODCONTO)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONTO = NVL(_Link_.ATCODATT,space(15))
      this.w_DESCON = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONTO = space(15)
      endif
      this.w_DESCON = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOS1
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_lTable = "TIPCOSTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2], .t., this.TIPCOSTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIPCOSTO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CODCOS1)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CODCOS1))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOS1)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStrODBC(trim(this.w_CODCOS1)+"%");

            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStr(trim(this.w_CODCOS1)+"%");

            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOS1) and !this.bDontReportError
            deferred_cp_zoom('TIPCOSTO','*','TCCODICE',cp_AbsName(oSource.parent,'oCODCOS1_1_9'),i_cWhere,'',"Codici costo",'gspc_tc.TIPCOSTO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCOS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCOS1)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOS1 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOS1 = NVL(_Link_.TCDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOS1 = space(5)
      endif
      this.w_DESCOS1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty (.w_CODCOS2) Or .w_CODCOS1<=.w_CODCOS2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOS1 = space(5)
        this.w_DESCOS1 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCOSTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOS2
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_lTable = "TIPCOSTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2], .t., this.TIPCOSTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIPCOSTO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CODCOS2)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CODCOS2))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOS2)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStrODBC(trim(this.w_CODCOS2)+"%");

            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStr(trim(this.w_CODCOS2)+"%");

            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOS2) and !this.bDontReportError
            deferred_cp_zoom('TIPCOSTO','*','TCCODICE',cp_AbsName(oSource.parent,'oCODCOS2_1_10'),i_cWhere,'',"Codici costo",'gspc_tc.TIPCOSTO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCOS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCOS2)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOS2 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOS2 = NVL(_Link_.TCDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOS2 = space(5)
      endif
      this.w_DESCOS2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty (.w_CODCOS1) Or .w_CODCOS1<=.w_CODCOS2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOS2 = space(5)
        this.w_DESCOS2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCOSTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM3_1_5.value==this.w_CODCOM3)
      this.oPgFrm.Page1.oPag.oCODCOM3_1_5.value=this.w_CODCOM3
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM2_1_7.value==this.w_CODCOM2)
      this.oPgFrm.Page1.oPag.oCODCOM2_1_7.value=this.w_CODCOM2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCONTO_1_8.value==this.w_CODCONTO)
      this.oPgFrm.Page1.oPag.oCODCONTO_1_8.value=this.w_CODCONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOS1_1_9.value==this.w_CODCOS1)
      this.oPgFrm.Page1.oPag.oCODCOS1_1_9.value=this.w_CODCOS1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOS2_1_10.value==this.w_CODCOS2)
      this.oPgFrm.Page1.oPag.oCODCOS2_1_10.value=this.w_CODCOS2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSTAMPA_1_11.RadioValue()==this.w_TIPSTAMPA)
      this.oPgFrm.Page1.oPag.oTIPSTAMPA_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILTRO_1_12.RadioValue()==this.w_FILTRO)
      this.oPgFrm.Page1.oPag.oFILTRO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_13.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oWITHATTI_1_14.RadioValue()==this.w_WITHATTI)
      this.oPgFrm.Page1.oPag.oWITHATTI_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_15.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_15.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_16.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_16.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNOZERI_1_19.RadioValue()==this.w_NOZERI)
      this.oPgFrm.Page1.oPag.oNOZERI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_21.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_21.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_33.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_33.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOS1_1_36.value==this.w_DESCOS1)
      this.oPgFrm.Page1.oPag.oDESCOS1_1_36.value=this.w_DESCOS1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOS2_1_38.value==this.w_DESCOS2)
      this.oPgFrm.Page1.oPag.oDESCOS2_1_38.value=this.w_DESCOS2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM2_1_40.value==this.w_DESCOM2)
      this.oPgFrm.Page1.oPag.oDESCOM2_1_40.value=this.w_DESCOM2
    endif
    if not(this.oPgFrm.Page1.oPag.oANACOSTI_1_45.RadioValue()==this.w_ANACOSTI)
      this.oPgFrm.Page1.oPag.oANACOSTI_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANARICAV_1_46.RadioValue()==this.w_ANARICAV)
      this.oPgFrm.Page1.oPag.oANARICAV_1_46.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODCOM2>=.w_CODCOM3 or empty(.w_CODCOM2))  and (IsNull( .w_OBJPADRE ))  and not(empty(.w_CODCOM3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM3_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODCOM2>=.w_CODCOM3)  and (IsNull( .w_OBJPADRE ))  and not(empty(.w_CODCOM2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM2_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Empty (.w_CODCOS2) Or .w_CODCOS1<=.w_CODCOS2)  and not(.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')  and not(empty(.w_CODCOS1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOS1_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Empty (.w_CODCOS1) Or .w_CODCOS1<=.w_CODCOS2)  and not(.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')  and not(empty(.w_CODCOS2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOS2_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_DATAFIN) or .w_DATAINI<=.w_DATAFIN)  and not(.w_FILTRO='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINI_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire una data non posteriore alla data di fine")
          case   ((empty(.w_DATAFIN)) or not(empty(.w_DATAINI) or .w_DATAFIN>=.w_DATAINI))  and not(.w_FILTRO='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFIN_1_16.SetFocus()
            i_bnoObbl = !empty(.w_DATAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire una data non anteriore alla data di inizio")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ANACOSTI = this.w_ANACOSTI
    return

enddefine

* --- Define pages as container
define class tgspc_srcPag1 as StdContainer
  Width  = 463
  height = 324
  stdWidth  = 463
  stdheight = 324
  resizeXpos=271
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM3_1_5 as StdField with uid="HYSKAVKLVJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODCOM3", cQueryName = "CODCOM3",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 197999398,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=101, Top=14, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM3"

  func oCODCOM3_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsNull( .w_OBJPADRE ))
    endwith
   endif
  endfunc

  func oCODCOM3_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM3_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM3_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM3_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCODCOM2_1_7 as StdField with uid="UTPFWZDMFU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODCOM2", cQueryName = "CODCOM2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 197999398,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=101, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM2"

  func oCODCOM2_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsNull( .w_OBJPADRE ))
    endwith
   endif
  endfunc

  func oCODCOM2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM2_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM2_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM2_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCODCONTO_1_8 as StdField with uid="NLKSJMXDEC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODCONTO", cQueryName = "CODCONTO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Esame struttura a partire da questo nodo ( vuoto tutta la struttura )",;
    HelpContextID = 214776693,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=101, Top=66, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_AMMI", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODCONTO"

  func oCODCONTO_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsNull( .w_OBJPADRE ))
    endwith
   endif
  endfunc

  func oCODCONTO_1_8.mHide()
    with this.Parent.oContained
      return (.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')
    endwith
  endfunc

  func oCODCONTO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCONTO_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCONTO_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_AMMI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_AMMI)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODCONTO_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco conti",'GSPC4SGS.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODCONTO_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM
    i_obj.ATTIPATT=w_AMMI
     i_obj.w_ATCODATT=this.parent.oContained.w_CODCONTO
     i_obj.ecpSave()
  endproc

  add object oCODCOS1_1_9 as StdField with uid="NKTVNTYTQM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODCOS1", cQueryName = "CODCOS1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice costo",;
    HelpContextID = 30227238,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=101, Top=92, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIPCOSTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODCOS1"

  func oCODCOS1_1_9.mHide()
    with this.Parent.oContained
      return (.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')
    endwith
  endfunc

  func oCODCOS1_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOS1_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOS1_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPCOSTO','*','TCCODICE',cp_AbsName(this.parent,'oCODCOS1_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici costo",'gspc_tc.TIPCOSTO_VZM',this.parent.oContained
  endproc

  add object oCODCOS2_1_10 as StdField with uid="AXSFCJAUOE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODCOS2", cQueryName = "CODCOS2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice costo",;
    HelpContextID = 30227238,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=101, Top=118, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIPCOSTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODCOS2"

  func oCODCOS2_1_10.mHide()
    with this.Parent.oContained
      return (.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')
    endwith
  endfunc

  func oCODCOS2_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOS2_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOS2_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPCOSTO','*','TCCODICE',cp_AbsName(this.parent,'oCODCOS2_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici costo",'gspc_tc.TIPCOSTO_VZM',this.parent.oContained
  endproc


  add object oTIPSTAMPA_1_11 as StdCombo with uid="EIURPOJMFY",rtseq=11,rtrep=.f.,left=101,top=192,width=114,height=21;
    , ToolTipText = "Se dettagliata totali divisi per tipo di costo";
    , HelpContextID = 265422186;
    , cFormVar="w_TIPSTAMPA",RowSource=""+"Sintetica,"+"Dettagliata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSTAMPA_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oTIPSTAMPA_1_11.GetRadio()
    this.Parent.oContained.w_TIPSTAMPA = this.RadioValue()
    return .t.
  endfunc

  func oTIPSTAMPA_1_11.SetRadio()
    this.Parent.oContained.w_TIPSTAMPA=trim(this.Parent.oContained.w_TIPSTAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSTAMPA=='S',1,;
      iif(this.Parent.oContained.w_TIPSTAMPA=='D',2,;
      0))
  endfunc


  add object oFILTRO_1_12 as StdCombo with uid="GIXGWRWJSR",rtseq=12,rtrep=.f.,left=295,top=192,width=134,height=21;
    , ToolTipText = "Filtro sulla selezione";
    , HelpContextID = 32590506;
    , cFormVar="w_FILTRO",RowSource=""+"Totale commessa,"+"Da data a data", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFILTRO_1_12.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oFILTRO_1_12.GetRadio()
    this.Parent.oContained.w_FILTRO = this.RadioValue()
    return .t.
  endfunc

  func oFILTRO_1_12.SetRadio()
    this.Parent.oContained.w_FILTRO=trim(this.Parent.oContained.w_FILTRO)
    this.value = ;
      iif(this.Parent.oContained.w_FILTRO=='N',1,;
      iif(this.Parent.oContained.w_FILTRO=='D',2,;
      0))
  endfunc


  add object oTIPO_1_13 as StdCombo with uid="YCWOXCZDJL",rtseq=13,rtrep=.f.,left=101,top=220,width=114,height=21;
    , ToolTipText = "Tipo preventivo (aggiornato solo costi attivit�)";
    , HelpContextID = 102107594;
    , cFormVar="w_TIPO",RowSource=""+"Iniziale,"+"Aggiornato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_13.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTIPO_1_13.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_13.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='I',1,;
      iif(this.Parent.oContained.w_TIPO=='A',2,;
      0))
  endfunc

  func oTIPO_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANACOSTI='S')
    endwith
   endif
  endfunc

  func oTIPO_1_13.mHide()
    with this.Parent.oContained
      return (.w_FILTRO<>'N')
    endwith
  endfunc


  add object oWITHATTI_1_14 as StdCombo with uid="OJNLIEAGFX",rtseq=14,rtrep=.f.,left=295,top=219,width=134,height=21;
    , ToolTipText = "Livello di dettaglio";
    , HelpContextID = 32716463;
    , cFormVar="w_WITHATTI",RowSource=""+"Primo livello,"+"Conti e sottoconti,"+"Dettaglio attivit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oWITHATTI_1_14.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oWITHATTI_1_14.GetRadio()
    this.Parent.oContained.w_WITHATTI = this.RadioValue()
    return .t.
  endfunc

  func oWITHATTI_1_14.SetRadio()
    this.Parent.oContained.w_WITHATTI=trim(this.Parent.oContained.w_WITHATTI)
    this.value = ;
      iif(this.Parent.oContained.w_WITHATTI=='1',1,;
      iif(this.Parent.oContained.w_WITHATTI=='N',2,;
      iif(this.Parent.oContained.w_WITHATTI=='S',3,;
      0)))
  endfunc

  func oWITHATTI_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANACOSTI='S')
    endwith
   endif
  endfunc

  add object oDATAINI_1_15 as StdField with uid="FLJIZTSJPY",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire una data non posteriore alla data di fine",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 60019402,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=101, Top=247

  func oDATAINI_1_15.mHide()
    with this.Parent.oContained
      return (.w_FILTRO='N')
    endwith
  endfunc

  func oDATAINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATAFIN) or .w_DATAINI<=.w_DATAFIN)
    endwith
    return bRes
  endfunc

  add object oDATAFIN_1_16 as StdField with uid="OLFJIKKOQH",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire una data non anteriore alla data di inizio",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 147051210,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=295, Top=247

  func oDATAFIN_1_16.mHide()
    with this.Parent.oContained
      return (.w_FILTRO='N')
    endwith
  endfunc

  func oDATAFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATAINI) or .w_DATAFIN>=.w_DATAINI)
    endwith
    return bRes
  endfunc

  add object oNOZERI_1_19 as StdCheck with uid="NGRDEXFLRM",rtseq=19,rtrep=.f.,left=101, top=275, caption="Voci significative",;
    ToolTipText = "Se attivo non stampate le voci a importo nullo",;
    HelpContextID = 134177834,;
    cFormVar="w_NOZERI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOZERI_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOZERI_1_19.GetRadio()
    this.Parent.oContained.w_NOZERI = this.RadioValue()
    return .t.
  endfunc

  func oNOZERI_1_19.SetRadio()
    this.Parent.oContained.w_NOZERI=trim(this.Parent.oContained.w_NOZERI)
    this.value = ;
      iif(this.Parent.oContained.w_NOZERI=='S',1,;
      0)
  endfunc

  add object oDESCOM_1_21 as StdField with uid="CIWUPAHQIP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 70377162,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=234, Top=14, InputMask=replicate('X',30)

  add object oDESCON_1_33 as StdField with uid="ZXNQSEHPKL",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 53599946,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=234, Top=66, InputMask=replicate('X',30)

  func oDESCON_1_33.mHide()
    with this.Parent.oContained
      return (.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')
    endwith
  endfunc

  add object oDESCOS1_1_36 as StdField with uid="UYWLZWHTMG",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCOS1", cQueryName = "DESCOS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 30286134,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=234, Top=92, InputMask=replicate('X',30)

  func oDESCOS1_1_36.mHide()
    with this.Parent.oContained
      return (.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')
    endwith
  endfunc

  add object oDESCOS2_1_38 as StdField with uid="KAPUCEFACX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCOS2", cQueryName = "DESCOS2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 30286134,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=234, Top=118, InputMask=replicate('X',30)

  func oDESCOS2_1_38.mHide()
    with this.Parent.oContained
      return (.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')
    endwith
  endfunc

  add object oDESCOM2_1_40 as StdField with uid="LVACNRITYP",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESCOM2", cQueryName = "DESCOM2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 198058294,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=234, Top=40, InputMask=replicate('X',30)


  add object oBtn_1_43 as StdButton with uid="YRVQSHEEGM",left=355, top=274, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 107603994;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        do GSPC_BS3 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_CODCOM) and not empty(.w_CODCOM2))
      endwith
    endif
  endfunc


  add object oBtn_1_44 as StdButton with uid="KOTWJAFPMC",left=407, top=273, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 100315322;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oANACOSTI_1_45 as StdCheck with uid="MMHZLEARAY",rtseq=30,rtrep=.f.,left=101, top=169, caption="Analisi costi",;
    HelpContextID = 30214735,;
    cFormVar="w_ANACOSTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANACOSTI_1_45.RadioValue()
    return(iif(this.value =1,"S",;
    "X"))
  endfunc
  func oANACOSTI_1_45.GetRadio()
    this.Parent.oContained.w_ANACOSTI = this.RadioValue()
    return .t.
  endfunc

  func oANACOSTI_1_45.SetRadio()
    this.Parent.oContained.w_ANACOSTI=trim(this.Parent.oContained.w_ANACOSTI)
    this.value = ;
      iif(this.Parent.oContained.w_ANACOSTI=="S",1,;
      0)
  endfunc

  add object oANARICAV_1_46 as StdCheck with uid="QSBEVVXZDO",rtseq=31,rtrep=.f.,left=295, top=169, caption="Analisi ricavi",;
    HelpContextID = 24906332,;
    cFormVar="w_ANARICAV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANARICAV_1_46.RadioValue()
    return(iif(this.value =1,"S",;
    "X"))
  endfunc
  func oANARICAV_1_46.GetRadio()
    this.Parent.oContained.w_ANARICAV = this.RadioValue()
    return .t.
  endfunc

  func oANARICAV_1_46.SetRadio()
    this.Parent.oContained.w_ANARICAV=trim(this.Parent.oContained.w_ANARICAV)
    this.value = ;
      iif(this.Parent.oContained.w_ANARICAV=="S",1,;
      0)
  endfunc

  func oANARICAV_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANACOSTI='S')
    endwith
   endif
  endfunc

  add object oStr_1_20 as StdString with uid="HPMXMXVSFE",Visible=.t., Left=5, Top=14,;
    Alignment=1, Width=92, Height=15,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="MUISPWAYNE",Visible=.t., Left=6, Top=193,;
    Alignment=1, Width=91, Height=15,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="OUYRIBWZTU",Visible=.t., Left=6, Top=221,;
    Alignment=1, Width=91, Height=15,;
    Caption="Preventivo:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_FILTRO<>'N')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="BBYAYDRCTH",Visible=.t., Left=12, Top=149,;
    Alignment=0, Width=210, Height=15,;
    Caption="Opzioni di stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="VQXGKHDUNE",Visible=.t., Left=220, Top=193,;
    Alignment=1, Width=70, Height=15,;
    Caption="Selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="GOKUSJAQOC",Visible=.t., Left=220, Top=247,;
    Alignment=1, Width=70, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_FILTRO='N')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="QAXZUKFAOR",Visible=.t., Left=5, Top=66,;
    Alignment=1, Width=92, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="WKDTTKPZUH",Visible=.t., Left=6, Top=247,;
    Alignment=1, Width=91, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_FILTRO='N')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="UHZPVRPMBZ",Visible=.t., Left=5, Top=92,;
    Alignment=1, Width=92, Height=15,;
    Caption="Da cod. costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="DKCVQTEWTB",Visible=.t., Left=5, Top=118,;
    Alignment=1, Width=92, Height=15,;
    Caption="A cod. costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_CODCOM<>.w_CODCOM2 or .w_WITHATTI='1')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="FLTYKFMKUV",Visible=.t., Left=5, Top=40,;
    Alignment=1, Width=92, Height=15,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="UVAQKXOZDU",Visible=.t., Left=220, Top=220,;
    Alignment=1, Width=70, Height=15,;
    Caption="Livello:"  ;
  , bGlobalFont=.t.

  add object oBox_1_28 as StdBox with uid="SLNEVYTVVY",left=6, top=164, width=451,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_src','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
