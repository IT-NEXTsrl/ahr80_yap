* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_mst                                                        *
*              Legami progetto                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_124]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-29                                                      *
* Last revis.: 2011-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_mst"))

* --- Class definition
define class tgspc_mst as StdTrsForm
  Top    = 8
  Left   = 33

  * --- Standard Properties
  Width  = 619
  Height = 406+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-08"
  HelpContextID=97146985
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  STRUTTUR_IDX = 0
  ATTIVITA_IDX = 0
  CAN_TIER_IDX = 0
  CPAR_DEF_IDX = 0
  cFile = "STRUTTUR"
  cKeySelect = "STTIPSTR,STCODCOM,STATTPAD"
  cKeyWhere  = "STTIPSTR=this.w_STTIPSTR and STCODCOM=this.w_STCODCOM and STATTPAD=this.w_STATTPAD"
  cKeyDetail  = "STTIPSTR=this.w_STTIPSTR and STCODCOM=this.w_STCODCOM and STATTPAD=this.w_STATTPAD and STTIPFIG=this.w_STTIPFIG and STATTFIG=this.w_STATTFIG"
  cKeyWhereODBC = '"STTIPSTR="+cp_ToStrODBC(this.w_STTIPSTR)';
      +'+" and STCODCOM="+cp_ToStrODBC(this.w_STCODCOM)';
      +'+" and STATTPAD="+cp_ToStrODBC(this.w_STATTPAD)';

  cKeyDetailWhereODBC = '"STTIPSTR="+cp_ToStrODBC(this.w_STTIPSTR)';
      +'+" and STCODCOM="+cp_ToStrODBC(this.w_STCODCOM)';
      +'+" and STATTPAD="+cp_ToStrODBC(this.w_STATTPAD)';
      +'+" and STTIPFIG="+cp_ToStrODBC(this.w_STTIPFIG)';
      +'+" and STATTFIG="+cp_ToStrODBC(this.w_STATTFIG)';

  cKeyWhereODBCqualified = '"STRUTTUR.STTIPSTR="+cp_ToStrODBC(this.w_STTIPSTR)';
      +'+" and STRUTTUR.STCODCOM="+cp_ToStrODBC(this.w_STCODCOM)';
      +'+" and STRUTTUR.STATTPAD="+cp_ToStrODBC(this.w_STATTPAD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'STRUTTUR.CPROWORD '
  cPrg = "gspc_mst"
  cComment = "Legami progetto"
  i_nRowNum = 0
  i_nRowPerPage = 13
  icon = "movi.ico"
  cAutoZoom = 'GSPCPMST'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- gspc_mst
  w_TREEFORM=.Null.
  * --- Fine Area Manuale

  * --- Local Variables
  w_READPAR = space(10)
  w_STTIPSTR = space(1)
  o_STTIPSTR = space(1)
  w_DEFCOM = space(15)
  w_STCODCOM = space(15)
  o_STCODCOM = space(15)
  w_STATTPAD = space(15)
  o_STATTPAD = space(15)
  w_STDESCRI = space(30)
  w_COMDES = space(30)
  w_DESPAD = space(30)
  w_TIPCOMP = space(1)
  w_TIPOCARI = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_CPROWORD = 0
  w_STTIPFIG = space(1)
  o_STTIPFIG = space(1)
  w_TIPATT = space(1)
  o_TIPATT = space(1)
  w_STATTFIG = space(15)
  o_STATTFIG = space(15)
  w_DESFIG = space(30)
  w_STMILLES = 0
  w_TOTMIL = 0
  w_VALMIL = 0
  w_UNIQUE = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STRUTTUR','gspc_mst')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_mstPag1","gspc_mst",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Legami progetto")
      .Pages(1).HelpContextID = 166107066
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTCODCOM_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ATTIVITA'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='CPAR_DEF'
    this.cWorkTables[4]='STRUTTUR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STRUTTUR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STRUTTUR_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_STTIPSTR = NVL(STTIPSTR,space(1))
      .w_STCODCOM = NVL(STCODCOM,space(15))
      .w_STATTPAD = NVL(STATTPAD,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from STRUTTUR where STTIPSTR=KeySet.STTIPSTR
    *                            and STCODCOM=KeySet.STCODCOM
    *                            and STATTPAD=KeySet.STATTPAD
    *                            and STTIPFIG=KeySet.STTIPFIG
    *                            and STATTFIG=KeySet.STATTFIG
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.STRUTTUR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTTUR_IDX,2],this.bLoadRecFilter,this.STRUTTUR_IDX,"gspc_mst")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STRUTTUR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STRUTTUR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STRUTTUR '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'STTIPSTR',this.w_STTIPSTR  ,'STCODCOM',this.w_STCODCOM  ,'STATTPAD',this.w_STATTPAD  )
      select * from (i_cTable) STRUTTUR where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_READPAR = 'TAM'
        .w_DEFCOM = space(15)
        .w_COMDES = space(30)
        .w_DESPAD = space(30)
        .w_TIPCOMP = space(1)
        .w_TIPOCARI = 'GSPC_MST'
        .w_OBTEST = i_INIDAT
        .w_TOTMIL = 0
        .w_UNIQUE = "S"
          .link_1_1('Load')
        .w_STTIPSTR = NVL(STTIPSTR,space(1))
        .w_STCODCOM = NVL(STCODCOM,space(15))
          if link_1_4_joined
            this.w_STCODCOM = NVL(CNCODCAN104,NVL(this.w_STCODCOM,space(15)))
            this.w_COMDES = NVL(CNDESCAN104,space(30))
          else
          .link_1_4('Load')
          endif
        .w_STATTPAD = NVL(STATTPAD,space(15))
          if link_1_6_joined
            this.w_STATTPAD = NVL(ATCODATT106,NVL(this.w_STATTPAD,space(15)))
            this.w_DESPAD = NVL(ATDESCRI106,space(30))
            this.w_TIPCOMP = NVL(ATTIPCOM106,space(1))
          else
          .link_1_6('Load')
          endif
        .w_STDESCRI = NVL(STDESCRI,space(30))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'STRUTTUR')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTMIL = 0
      scan
        with this
          .w_DESFIG = space(30)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_STTIPFIG = NVL(STTIPFIG,space(1))
        .w_TIPATT = iif(.w_STTIPFIG='A','A','S')
          .w_STATTFIG = NVL(STATTFIG,space(15))
          .link_2_4('Load')
          .w_STMILLES = NVL(STMILLES,0)
        .w_VALMIL = .w_STMILLES
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTMIL = .w_TOTMIL+.w_VALMIL
          replace STTIPFIG with .w_STTIPFIG
          replace STATTFIG with .w_STATTFIG
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_READPAR=space(10)
      .w_STTIPSTR=space(1)
      .w_DEFCOM=space(15)
      .w_STCODCOM=space(15)
      .w_STATTPAD=space(15)
      .w_STDESCRI=space(30)
      .w_COMDES=space(30)
      .w_DESPAD=space(30)
      .w_TIPCOMP=space(1)
      .w_TIPOCARI=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_CPROWORD=10
      .w_STTIPFIG=space(1)
      .w_TIPATT=space(1)
      .w_STATTFIG=space(15)
      .w_DESFIG=space(30)
      .w_STMILLES=0
      .w_TOTMIL=0
      .w_VALMIL=0
      .w_UNIQUE=space(1)
      if .cFunction<>"Filter"
        .w_READPAR = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
         .link_1_1('Full')
        endif
        .w_STTIPSTR = 'P'
        .DoRTCalc(3,3,.f.)
        .w_STCODCOM = IIF(Empty(.w_STCODCOM), .w_DEFCOM,.w_STCODCOM)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_STCODCOM))
         .link_1_4('Full')
        endif
        .w_STATTPAD = .w_STATTPAD
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_STATTPAD))
         .link_1_6('Full')
        endif
        .DoRTCalc(6,9,.f.)
        .w_TIPOCARI = 'GSPC_MST'
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .w_OBTEST = i_INIDAT
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(12,12,.f.)
        .w_STTIPFIG = IIF(.w_TIPATT='A', 'A', 'P')
        .w_TIPATT = iif(.w_STTIPFIG='A','A','S')
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_STATTFIG))
         .link_2_4('Full')
        endif
        .DoRTCalc(16,16,.f.)
        .w_STMILLES = IIF(Empty(.w_STATTFIG), 0 , .w_STMILLES)
        .DoRTCalc(18,18,.f.)
        .w_VALMIL = .w_STMILLES
        .w_UNIQUE = "S"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'STRUTTUR')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSTCODCOM_1_4.enabled = i_bVal
      .Page1.oPag.oSTATTPAD_1_6.enabled = i_bVal
      .Page1.oPag.oSTDESCRI_1_8.enabled = i_bVal
      .Page1.oPag.oBtn_1_17.enabled = i_bVal
      .Page1.oPag.oBtn_1_18.enabled = i_bVal
      .Page1.oPag.oObj_1_14.enabled = i_bVal
      .Page1.oPag.oObj_1_15.enabled = i_bVal
      .Page1.oPag.oObj_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_20.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSTCODCOM_1_4.enabled = .f.
        .Page1.oPag.oSTATTPAD_1_6.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSTCODCOM_1_4.enabled = .t.
        .Page1.oPag.oSTATTPAD_1_6.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STRUTTUR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STRUTTUR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STTIPSTR,"STTIPSTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCODCOM,"STCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STATTPAD,"STATTPAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STDESCRI,"STDESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STRUTTUR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTTUR_IDX,2])
    i_lTable = "STRUTTUR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STRUTTUR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSPC_SLE with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_TIPATT N(3);
      ,t_STATTFIG C(15);
      ,t_DESFIG C(30);
      ,t_STMILLES N(4);
      ,STTIPFIG C(1);
      ,STATTFIG C(15);
      ,t_STTIPFIG C(1);
      ,t_VALMIL N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgspc_mstbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTIPATT_2_3.controlsource=this.cTrsName+'.t_TIPATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTATTFIG_2_4.controlsource=this.cTrsName+'.t_STATTFIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESFIG_2_5.controlsource=this.cTrsName+'.t_DESFIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTMILLES_2_6.controlsource=this.cTrsName+'.t_STMILLES'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(60)
    this.AddVLine(189)
    this.AddVLine(321)
    this.AddVLine(540)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STRUTTUR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTTUR_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STRUTTUR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTTUR_IDX,2])
      *
      * insert into STRUTTUR
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STRUTTUR')
        i_extval=cp_InsertValODBCExtFlds(this,'STRUTTUR')
        i_cFldBody=" "+;
                  "(STTIPSTR,STCODCOM,STATTPAD,STDESCRI,CPROWORD"+;
                  ",STTIPFIG,STATTFIG,STMILLES,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_STTIPSTR)+","+cp_ToStrODBCNull(this.w_STCODCOM)+","+cp_ToStrODBCNull(this.w_STATTPAD)+","+cp_ToStrODBC(this.w_STDESCRI)+","+cp_ToStrODBC(this.w_CPROWORD)+;
             ","+cp_ToStrODBC(this.w_STTIPFIG)+","+cp_ToStrODBCNull(this.w_STATTFIG)+","+cp_ToStrODBC(this.w_STMILLES)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STRUTTUR')
        i_extval=cp_InsertValVFPExtFlds(this,'STRUTTUR')
        cp_CheckDeletedKey(i_cTable,0,'STTIPSTR',this.w_STTIPSTR,'STCODCOM',this.w_STCODCOM,'STATTPAD',this.w_STATTPAD,'STTIPFIG',this.w_STTIPFIG,'STATTFIG',this.w_STATTFIG)
        INSERT INTO (i_cTable) (;
                   STTIPSTR;
                  ,STCODCOM;
                  ,STATTPAD;
                  ,STDESCRI;
                  ,CPROWORD;
                  ,STTIPFIG;
                  ,STATTFIG;
                  ,STMILLES;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_STTIPSTR;
                  ,this.w_STCODCOM;
                  ,this.w_STATTPAD;
                  ,this.w_STDESCRI;
                  ,this.w_CPROWORD;
                  ,this.w_STTIPFIG;
                  ,this.w_STATTFIG;
                  ,this.w_STMILLES;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- gspc_mst
    * controllo che non vi sia un altro legame
    * con padre uguale a questo
    * e sono in caricamento
    If This.cFunction='Load'
      This.NotifyEvent("CheckPad")
    endif
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.STRUTTUR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTTUR_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (!EMPTY(t_STATTFIG)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'STRUTTUR')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " STDESCRI="+cp_ToStrODBC(this.w_STDESCRI)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and STTIPFIG="+cp_ToStrODBC(&i_TN.->STTIPFIG)+;
                 " and STATTFIG="+cp_ToStrODBC(&i_TN.->STATTFIG)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'STRUTTUR')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  STDESCRI=this.w_STDESCRI;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and STTIPFIG=&i_TN.->STTIPFIG;
                      and STATTFIG=&i_TN.->STATTFIG;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (!EMPTY(t_STATTFIG)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and STTIPFIG="+cp_ToStrODBC(&i_TN.->STTIPFIG)+;
                            " and STATTFIG="+cp_ToStrODBC(&i_TN.->STATTFIG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and STTIPFIG=&i_TN.->STTIPFIG;
                            and STATTFIG=&i_TN.->STATTFIG;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace STTIPFIG with this.w_STTIPFIG
              replace STATTFIG with this.w_STATTFIG
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update STRUTTUR
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'STRUTTUR')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " STDESCRI="+cp_ToStrODBC(this.w_STDESCRI)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",STMILLES="+cp_ToStrODBC(this.w_STMILLES)+;
                     ",STTIPFIG="+cp_ToStrODBC(this.w_STTIPFIG)+;
                     ",STATTFIG="+cp_ToStrODBC(this.w_STATTFIG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and STTIPFIG="+cp_ToStrODBC(STTIPFIG)+;
                             " and STATTFIG="+cp_ToStrODBC(STATTFIG)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'STRUTTUR')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      STDESCRI=this.w_STDESCRI;
                     ,CPROWORD=this.w_CPROWORD;
                     ,STMILLES=this.w_STMILLES;
                     ,STTIPFIG=this.w_STTIPFIG;
                     ,STATTFIG=this.w_STATTFIG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and STTIPFIG=&i_TN.->STTIPFIG;
                                      and STATTFIG=&i_TN.->STATTFIG;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- gspc_mst
    This.NotifyEvent('ElFigli')
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STRUTTUR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTTUR_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (!EMPTY(t_STATTFIG)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete STRUTTUR
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and STTIPFIG="+cp_ToStrODBC(&i_TN.->STTIPFIG)+;
                            " and STATTFIG="+cp_ToStrODBC(&i_TN.->STATTFIG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and STTIPFIG=&i_TN.->STTIPFIG;
                              and STATTFIG=&i_TN.->STATTFIG;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (!EMPTY(t_STATTFIG)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gspc_mst
    * lancio il refresh se cancello un legame
    This.NotifyEvent('Cancella')
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STRUTTUR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTTUR_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
        if .o_STCODCOM<>.w_STCODCOM
          .w_STCODCOM = IIF(Empty(.w_STCODCOM), .w_DEFCOM,.w_STCODCOM)
          .link_1_4('Full')
        endif
        if .o_STATTPAD<>.w_STATTPAD
          .w_STATTPAD = .w_STATTPAD
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(6,12,.t.)
        if .o_TIPATT<>.w_TIPATT.or. .o_STTIPSTR<>.w_STTIPSTR
          .w_STTIPFIG = IIF(.w_TIPATT='A', 'A', 'P')
        endif
        if .o_STTIPFIG<>.w_STTIPFIG
          .w_TIPATT = iif(.w_STTIPFIG='A','A','S')
        endif
        if .o_STATTFIG<>.w_STATTFIG
          .link_2_4('Full')
        endif
        .DoRTCalc(16,16,.t.)
        if .o_STATTFIG<>.w_STATTFIG
          .w_STMILLES = IIF(Empty(.w_STATTFIG), 0 , .w_STMILLES)
        endif
        .DoRTCalc(18,18,.t.)
          .w_TOTMIL = .w_TOTMIL-.w_valmil
          .w_VALMIL = .w_STMILLES
          .w_TOTMIL = .w_TOTMIL+.w_valmil
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_STTIPFIG with this.w_STTIPFIG
      replace t_VALMIL with this.w_VALMIL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSTMILLES_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSTMILLES_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STCODCOM
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_STCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_STCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oSTCODCOM_1_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_STCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_STCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_COMDES = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_STCODCOM = space(15)
      endif
      this.w_COMDES = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.CNCODCAN as CNCODCAN104"+ ",link_1_4.CNDESCAN as CNDESCAN104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on STRUTTUR.STCODCOM=link_1_4.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and STRUTTUR.STCODCOM=link_1_4.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=STATTPAD
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STATTPAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_STATTPAD)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_STCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_STTIPSTR);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATTIPCOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_STCODCOM;
                     ,'ATTIPATT',this.w_STTIPSTR;
                     ,'ATCODATT',trim(this.w_STATTPAD))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATTIPCOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STATTPAD)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STATTPAD) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oSTATTPAD_1_6'),i_cWhere,'GSPC_BZZ',"Elenco componenti",'GSPC_MST.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_STCODCOM<>oSource.xKey(1);
           .or. this.w_STTIPSTR<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATTIPCOM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATTIPCOM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATTIPCOM";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_STCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_STTIPSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATTIPCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STATTPAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATTIPCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_STATTPAD);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_STCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_STTIPSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_STCODCOM;
                       ,'ATTIPATT',this.w_STTIPSTR;
                       ,'ATCODATT',this.w_STATTPAD)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATTIPCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STATTPAD = NVL(_Link_.ATCODATT,space(15))
      this.w_DESPAD = NVL(_Link_.ATDESCRI,space(30))
      this.w_TIPCOMP = NVL(_Link_.ATTIPCOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_STATTPAD = space(15)
      endif
      this.w_DESPAD = space(30)
      this.w_TIPCOMP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STATTPAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ATTIVITA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.ATCODATT as ATCODATT106"+ ",link_1_6.ATDESCRI as ATDESCRI106"+ ",link_1_6.ATTIPCOM as ATTIPCOM106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on STRUTTUR.STATTPAD=link_1_6.ATCODATT"+" and STRUTTUR.STCODCOM=link_1_6.ATCODCOM"+" and STRUTTUR.STTIPSTR=link_1_6.ATTIPATT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and STRUTTUR.STATTPAD=link_1_6.ATCODATT(+)"'+'+" and STRUTTUR.STCODCOM=link_1_6.ATCODCOM(+)"'+'+" and STRUTTUR.STTIPSTR=link_1_6.ATTIPATT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=STATTFIG
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STATTFIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_STATTFIG)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_STCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_STTIPFIG);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_STCODCOM;
                     ,'ATTIPATT',this.w_STTIPFIG;
                     ,'ATCODATT',trim(this.w_STATTFIG))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STATTFIG)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_STATTFIG)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_STCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_STTIPFIG);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_STATTFIG)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_STCODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_STTIPFIG);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_STATTFIG) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oSTATTFIG_2_4'),i_cWhere,'GSPC_BZZ',"Elenco componenti",'GSPC_SCS.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_STCODCOM<>oSource.xKey(1);
           .or. this.w_STTIPFIG<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice insesistente oppure legame circolare padre figlio")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_STCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_STTIPFIG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STATTFIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_STATTFIG);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_STCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_STTIPFIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_STCODCOM;
                       ,'ATTIPATT',this.w_STTIPFIG;
                       ,'ATCODATT',this.w_STATTFIG)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STATTFIG = NVL(_Link_.ATCODATT,space(15))
      this.w_DESFIG = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_STATTFIG = space(15)
      endif
      this.w_DESFIG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_STATTFIG<>.w_STATTPAD OR .w_TIPATT='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice insesistente oppure legame circolare padre figlio")
        endif
        this.w_STATTFIG = space(15)
        this.w_DESFIG = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STATTFIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSTCODCOM_1_4.value==this.w_STCODCOM)
      this.oPgFrm.Page1.oPag.oSTCODCOM_1_4.value=this.w_STCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATTPAD_1_6.value==this.w_STATTPAD)
      this.oPgFrm.Page1.oPag.oSTATTPAD_1_6.value=this.w_STATTPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESCRI_1_8.value==this.w_STDESCRI)
      this.oPgFrm.Page1.oPag.oSTDESCRI_1_8.value=this.w_STDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMDES_1_10.value==this.w_COMDES)
      this.oPgFrm.Page1.oPag.oCOMDES_1_10.value=this.w_COMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAD_1_11.value==this.w_DESPAD)
      this.oPgFrm.Page1.oPag.oDESPAD_1_11.value=this.w_DESPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCOMP_1_12.RadioValue()==this.w_TIPCOMP)
      this.oPgFrm.Page1.oPag.oTIPCOMP_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTMIL_3_1.value==this.w_TOTMIL)
      this.oPgFrm.Page1.oPag.oTOTMIL_3_1.value=this.w_TOTMIL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPATT_2_3.RadioValue()==this.w_TIPATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPATT_2_3.SetRadio()
      replace t_TIPATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPATT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTATTFIG_2_4.value==this.w_STATTFIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTATTFIG_2_4.value=this.w_STATTFIG
      replace t_STATTFIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTATTFIG_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESFIG_2_5.value==this.w_DESFIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESFIG_2_5.value=this.w_DESFIG
      replace t_DESFIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESFIG_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTMILLES_2_6.value==this.w_STMILLES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTMILLES_2_6.value=this.w_STMILLES
      replace t_STMILLES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTMILLES_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'STRUTTUR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_STCODCOM))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSTCODCOM_1_4.SetFocus()
            i_bnoObbl = !empty(.w_STCODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_STATTPAD))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSTATTPAD_1_6.SetFocus()
            i_bnoObbl = !empty(.w_STATTPAD)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (!EMPTY(t_STATTFIG));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_STATTFIG<>.w_STATTPAD OR .w_TIPATT='A') and not(empty(.w_STATTFIG)) and (!EMPTY(.w_STATTFIG))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTATTFIG_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice insesistente oppure legame circolare padre figlio")
        case   not(.w_STMILLES>=0 And .w_STMILLES<=1000) and (!EMPTY(.w_STATTFIG)) and (!EMPTY(.w_STATTFIG))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTMILLES_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Percentuale espressa in millesimi (0..1000)")
      endcase
      if !EMPTY(.w_STATTFIG)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_STTIPSTR = this.w_STTIPSTR
    this.o_STCODCOM = this.w_STCODCOM
    this.o_STATTPAD = this.w_STATTPAD
    this.o_STTIPFIG = this.w_STTIPFIG
    this.o_TIPATT = this.w_TIPATT
    this.o_STATTFIG = this.w_STATTFIG
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(!EMPTY(t_STATTFIG))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_STTIPFIG=space(1)
      .w_TIPATT=space(1)
      .w_STATTFIG=space(15)
      .w_DESFIG=space(30)
      .w_STMILLES=0
      .w_VALMIL=0
      .DoRTCalc(1,12,.f.)
        .w_STTIPFIG = IIF(.w_TIPATT='A', 'A', 'P')
        .w_TIPATT = iif(.w_STTIPFIG='A','A','S')
      .DoRTCalc(15,15,.f.)
      if not(empty(.w_STATTFIG))
        .link_2_4('Full')
      endif
      .DoRTCalc(16,16,.f.)
        .w_STMILLES = IIF(Empty(.w_STATTFIG), 0 , .w_STMILLES)
      .DoRTCalc(18,18,.f.)
        .w_VALMIL = .w_STMILLES
    endwith
    this.DoRTCalc(20,20,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_STTIPFIG = t_STTIPFIG
    this.w_TIPATT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPATT_2_3.RadioValue(.t.)
    this.w_STATTFIG = t_STATTFIG
    this.w_DESFIG = t_DESFIG
    this.w_STMILLES = t_STMILLES
    this.w_VALMIL = t_VALMIL
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_STTIPFIG with this.w_STTIPFIG
    replace t_TIPATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPATT_2_3.ToRadio()
    replace t_STATTFIG with this.w_STATTFIG
    replace t_DESFIG with this.w_DESFIG
    replace t_STMILLES with this.w_STMILLES
    replace t_VALMIL with this.w_VALMIL
    if i_srv='A'
      replace STTIPFIG with this.w_STTIPFIG
      replace STATTFIG with this.w_STATTFIG
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTMIL = .w_TOTMIL-.w_valmil
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgspc_mstPag1 as StdContainer
  Width  = 615
  height = 406
  stdWidth  = 615
  stdheight = 406
  resizeXpos=349
  resizeYpos=267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTCODCOM_1_4 as StdField with uid="RNPOERDKSB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_STCODCOM", cQueryName = "STTIPSTR,STCODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 238472845,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=128, Left=99, Top=10, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_STCODCOM"

  func oSTCODCOM_1_4.mCond()
    with this.Parent.oContained
        if .nLastRow>1
          return (.f.)
        endif
    endwith
  endfunc

  func oSTCODCOM_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_STATTPAD)
        bRes2=.link_1_6('Full')
      endif
      if .not. empty(.w_STATTFIG)
        bRes2=.link_2_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSTCODCOM_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTCODCOM_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSTCODCOM_1_4.readonly and this.parent.oSTCODCOM_1_4.isprimarykey)
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oSTCODCOM_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
   endif
  endproc

  add object oSTATTPAD_1_6 as StdField with uid="RCIDBRYISZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_STATTPAD", cQueryName = "STTIPSTR,STCODCOM,STATTPAD",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Componente padre",;
    HelpContextID = 265163114,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=128, Left=99, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_STCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_STTIPSTR", oKey_3_1="ATCODATT", oKey_3_2="this.w_STATTPAD"

  func oSTATTPAD_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTATTPAD_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTATTPAD_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSTATTPAD_1_6.readonly and this.parent.oSTATTPAD_1_6.isprimarykey)
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_STCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_STTIPSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_STCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_STTIPSTR)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oSTATTPAD_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco componenti",'GSPC_MST.ATTIVITA_VZM',this.parent.oContained
   endif
  endproc
  proc oSTATTPAD_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_STCODCOM
    i_obj.ATTIPATT=w_STTIPSTR
     i_obj.w_ATCODATT=this.parent.oContained.w_STATTPAD
    i_obj.ecpSave()
  endproc

  add object oSTDESCRI_1_8 as StdField with uid="NPDOSBWYYW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_STDESCRI", cQueryName = "STDESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 45039983,;
   bGlobalFont=.t.,;
    Height=21, Width=313, Left=159, Top=77, InputMask=replicate('X',30)

  add object oCOMDES_1_10 as StdField with uid="OCNCDYWSRK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_COMDES", cQueryName = "COMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 30329638,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=231, Top=10, InputMask=replicate('X',30)

  add object oDESPAD_1_11 as StdField with uid="FGIHVRKWXG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESPAD", cQueryName = "DESPAD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 43721014,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=231, Top=40, InputMask=replicate('X',30)


  add object oTIPCOMP_1_12 as StdCombo with uid="MHBWBOOAJY",rtseq=9,rtrep=.f.,left=483,top=40,width=109,height=21, enabled=.f.;
    , ToolTipText = "Tipologia componente";
    , HelpContextID = 208533046;
    , cFormVar="w_TIPCOMP",RowSource=""+"Attivit�,"+"Sottoconto,"+"Conto,"+"Capo progetto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCOMP_1_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPCOMP,&i_cF..t_TIPCOMP),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'S',;
    iif(xVal =3,'C',;
    iif(xVal =4,'P',;
    space(1))))))
  endfunc
  func oTIPCOMP_1_12.GetRadio()
    this.Parent.oContained.w_TIPCOMP = this.RadioValue()
    return .t.
  endfunc

  func oTIPCOMP_1_12.ToRadio()
    this.Parent.oContained.w_TIPCOMP=trim(this.Parent.oContained.w_TIPCOMP)
    return(;
      iif(this.Parent.oContained.w_TIPCOMP=='A',1,;
      iif(this.Parent.oContained.w_TIPCOMP=='S',2,;
      iif(this.Parent.oContained.w_TIPCOMP=='C',3,;
      iif(this.Parent.oContained.w_TIPCOMP=='P',4,;
      0)))))
  endfunc

  func oTIPCOMP_1_12.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oObj_1_14 as cp_runprogram with uid="IAJWDKKZMY",left=640, top=157, width=312,height=20,;
    caption='GSPC_BS1',;
   bGlobalFont=.t.,;
    prg='GSPC_BS1("Treeview")',;
    cEvent = "Record Inserted,Record Updated,Cancella",;
    nPag=1;
    , ToolTipText = "Refresha la treeview se la gestione � stata lanciata da li";
    , HelpContextID = 40763287


  add object oObj_1_15 as cp_runprogram with uid="BTVLBGASBB",left=640, top=119, width=140,height=20,;
    caption='GSPC_BCF',;
   bGlobalFont=.t.,;
    prg='GSPC_BCF',;
    cEvent = "ElFigli",;
    nPag=1;
    , ToolTipText = "Elimina tutti i figli dei figli";
    , HelpContextID = 40763308


  add object oBtn_1_17 as StdButton with uid="QZGHLPNBVJ",left=510, top=64, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=1;
    , ToolTipText = "Caricamento rapido del dettaglio";
    , HelpContextID = 30813478;
    , TabStop=.f.,Caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      do GSPC_SCS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="GRRNJCOMNE",left=561, top=64, width=48,height=45,;
    CpPicture="bmp\ripart.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per reimpostare in proporzione il peso di ciascun elemento.";
    , HelpContextID = 164991968;
    , TabStop=.f.,Caption='\<Ripartizione';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        do GSPC_BSM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_19 as cp_runprogram with uid="RRNCKYODNE",left=640, top=138, width=259,height=20,;
    caption='GSPC_BCS',;
   bGlobalFont=.t.,;
    prg='GSPC_BCS',;
    cEvent = "Insert row start,Update row start",;
    nPag=1;
    , ToolTipText = "Controlli sulla struttura";
    , HelpContextID = 40763321


  add object oObj_1_20 as cp_runprogram with uid="LVSPELWDLG",left=640, top=176, width=158,height=20,;
    caption='GSPC_BS1',;
   bGlobalFont=.t.,;
    prg='GSPC_BS1("CodPad")',;
    cEvent = "CheckPad",;
    nPag=1;
    , ToolTipText = "Controlla che non esista gi� un legame con padre come questo";
    , HelpContextID = 40763287


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=111, width=597,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Riga",Field2="TIPATT",Label2="Tipo",Field3="STATTFIG",Label3="Voce",Field4="DESFIG",Label4="Descrizione",Field5="STMILLES",Label5="% Avanz.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218943866

  add object oStr_1_5 as StdString with uid="BUABBLLQHV",Visible=.t., Left=5, Top=10,;
    Alignment=1, Width=89, Height=15,;
    Caption="Commessa:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="KTUUXBLSKM",Visible=.t., Left=5, Top=40,;
    Alignment=1, Width=89, Height=15,;
    Caption="Padre:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="KKQXADARKO",Visible=.t., Left=7, Top=77,;
    Alignment=1, Width=149, Height=15,;
    Caption="Descrizione legame:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=131,;
    width=593+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=132,width=592+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ATTIVITA|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ATTIVITA'
        oDropInto=this.oBodyCol.oRow.oSTATTFIG_2_4
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTMIL_3_1 as StdField with uid="DESAQBSDAG",rtseq=18,rtrep=.f.,;
    cFormVar="w_TOTMIL",value=0,enabled=.f.,;
    ToolTipText = "Totale millesimi",;
    HelpContextID = 186137654,;
    cQueryName = "TOTMIL",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=546, Top=383, cSayPict=["9999"], cGetPict=["9999"]

  add object oStr_3_2 as StdString with uid="JKFRKFHHSL",Visible=.t., Left=458, Top=383,;
    Alignment=1, Width=83, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgspc_mstBodyRow as CPBodyRowCnt
  Width=583
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="UFHMCKUVXG",rtseq=12,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 251272298,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oTIPATT_2_3 as StdTrsCombo with uid="ZBIXFKYUFP",rtrep=.t.,;
    cFormVar="w_TIPATT", RowSource=""+"Conti/sottoconti,"+"Attivit�" , ;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 62649910,;
    Height=21, Width=125, Left=53, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTIPATT_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPATT,&i_cF..t_TIPATT),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'A',;
    'S')))
  endfunc
  func oTIPATT_2_3.GetRadio()
    this.Parent.oContained.w_TIPATT = this.RadioValue()
    return .t.
  endfunc

  func oTIPATT_2_3.ToRadio()
    this.Parent.oContained.w_TIPATT=trim(this.Parent.oContained.w_TIPATT)
    return(;
      iif(this.Parent.oContained.w_TIPATT=='S',1,;
      iif(this.Parent.oContained.w_TIPATT=='A',2,;
      0)))
  endfunc

  func oTIPATT_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oSTATTFIG_2_4 as StdTrsField with uid="ELJHCHIWMG",rtseq=15,rtrep=.t.,;
    cFormVar="w_STATTFIG",value=space(15),isprimarykey=.t.,;
    HelpContextID = 171044499,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice insesistente oppure legame circolare padre figlio",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=182, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_STCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_STTIPFIG", oKey_3_1="ATCODATT", oKey_3_2="this.w_STATTFIG"

  proc oSTATTFIG_2_4.mDefault
    with this.Parent.oContained
      if empty(.w_STATTFIG)
        .w_STATTFIG = ''
      endif
    endwith
  endproc

  func oSTATTFIG_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTATTFIG_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSTATTFIG_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSTATTFIG_2_4.readonly and this.parent.oSTATTFIG_2_4.isprimarykey)
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_STCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_STTIPFIG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_STCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_STTIPFIG)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oSTATTFIG_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco componenti",'GSPC_SCS.ATTIVITA_VZM',this.parent.oContained
   endif
  endproc
  proc oSTATTFIG_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_STCODCOM
    i_obj.ATTIPATT=w_STTIPFIG
     i_obj.w_ATCODATT=this.parent.oContained.w_STATTFIG
    i_obj.ecpSave()
  endproc

  add object oDESFIG_2_5 as StdTrsField with uid="JZDOOBACKG",rtseq=16,rtrep=.t.,;
    cFormVar="w_DESFIG",value=space(30),enabled=.f.,;
    HelpContextID = 101785910,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=214, Left=314, Top=0, InputMask=replicate('X',30)

  add object oSTMILLES_2_6 as StdTrsField with uid="GXWXXIMFQW",rtseq=17,rtrep=.t.,;
    cFormVar="w_STMILLES",value=0,;
    ToolTipText = "Percentuale espressa in millesimi dell'avanzamento tecnico",;
    HelpContextID = 188993913,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Percentuale espressa in millesimi (0..1000)",;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=534, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  func oSTMILLES_2_6.mCond()
    with this.Parent.oContained
      return (!EMPTY(.w_STATTFIG))
    endwith
  endfunc

  func oSTMILLES_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_STMILLES>=0 And .w_STMILLES<=1000)
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=12
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_mst','STRUTTUR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".STTIPSTR=STRUTTUR.STTIPSTR";
  +" and "+i_cAliasName2+".STCODCOM=STRUTTUR.STCODCOM";
  +" and "+i_cAliasName2+".STATTPAD=STRUTTUR.STATTPAD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
