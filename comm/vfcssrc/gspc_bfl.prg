* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bfl                                                        *
*              Cash flow di commessa                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_74]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-03                                                      *
* Last revis.: 2006-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bfl",oParentObject,m.pTipo)
return(i_retval)

define class tgspc_bfl as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_TOTINCA = 0
  w_TOTPAG = 0
  w_DATAPER = ctod("  /  /  ")
  w_NUMPER = 0
  w_POS = 0
  w_MESE = 0
  w_ANNO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cash Flow di Commessa
    do case
      case this.pTipo="GRAPH"
        if EMPTY(this.oParentObject.w_DATAINI) OR EMPTY(this.oParentObject.w_DATAFIN) OR this.oParentObject.w_DATAINI>this.oParentObject.w_DATAFIN
          ah_ErrorMsg("Intervallo di date incongruente",,"")
          i_retcode = 'stop'
          return
        endif
        * --- Elimino i cursori grafici
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Costruisco il Grafico
        * --- Recupero gli incassi/Pagamenti
        Vq_Exec("..\COMM\EXE\QUERY\GSPC_BFL.VQR",This,"_Temp_")
        if this.oParentObject.w_PERIODO="S"
          UPDATE _Temp_ SET DATA=CP_TODATE(DATA)+(7-DOW(CP_TODATE(DATA),2))
          this.w_NUMPER = Week(Cp_ToDate(this.oParentObject.w_DataIni))
        else
          SELECT _Temp_ 
          GO TOP
          SCAN
          this.w_NUMPER = Cp_ToDate(DATA)
          this.w_MESE = MONTH(this.w_NUMPER)+1
          this.w_ANNO = YEAR(this.w_NUMPER)
          if this.w_MESE>12
            this.w_MESE = 1
            this.w_ANNO = this.w_ANNO+1
          endif
          this.w_MESE = RIGHT("00"+ALLTRIM(STR(this.w_MESE)), 2)
          this.w_ANNO = RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4)
          this.w_NUMPER = cp_CharToDate("01-" +this.w_MESE + "-" + this.w_ANNO) -1
          REPLACE DATA WITH this.w_NUMPER
          ENDSCAN
          this.w_NUMPER = Month(Cp_ToDate(this.oParentObject.w_DataIni))
        endif
        SELECT _Temp_ 
        GO TOP
        * --- Costruisco Cursore per il grafico (Per avere un tipo campo che accetti Null)
        *     Sommo tutti i periodi precedenti alla data di inizio
        Select CP_TODATE(Data) AS DATA, SUM(INCASSI) AS INCASSI, SUM(PAGAMENTI) AS PAGAMENTI, ;
        iif(this.oParentObject.w_PERIODO="S", WEEK(CP_TODATE(DATA)), MONTH(CP_TODATE(DATA))) AS PERIODO, YEAR(DATA) AS ANNO ;
        From _Temp_ GROUP BY DATA Where Data>this.oParentObject.w_DATAINI ;
        Union ;
        Select this.oParentObject.w_DATAINI As DATA,Sum(Incassi) As INCASSI, Sum(Pagamenti) As PAGAMENTI, this.w_NumPer As PERIODO, ;
        Year(this.oParentObject.w_DATAINI) As ANNO Where Data<=this.oParentObject.w_DATAINI From _Temp_ ;
        Order By 1 Into Cursor _Graph_ NoFilter
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Aggiungo il tutto ai periodi calcolati
        Select Periodi.Data As Data ,Incassi,Pagamenti From _Graph_ Right Join ;
        Periodi On _Graph_.Periodo = Periodi.Periodo And Periodi.Anno=_Graph_.Anno ;
        Order By Periodi.Data Into Cursor _Graph_ NoFilter
        * --- Costruisco la progressione (riempio i periodi vuoti)
        Cur = WrCursor ("_Graph_")
        Select _Graph_
        Go Top
        this.w_TOTINCA = 0
        this.w_TOTPAG = 0
        * --- Costruisco la progressione e reimpio i periodi vuoti
        Scan
        this.w_TOTINCA = this.w_TOTINCA+ Nvl ( _Graph_.Incassi ,0)
        this.w_TOTPAG = this.w_TOTPAG+ Nvl ( _Graph_.Pagamenti ,0)
        Replace Incassi With this.w_TOTINCA
        Replace Pagamenti With this.w_TOTPAG
        EndScan
        Select * From _Graph_ Order By Data Into Cursor _Cash_ NoFilter
        * --- Aggiorno il Grafico
        This.oParentObject.NotifyEvent("Esegui")
        this.oParentObject.w_GRAPH = .T.
        * --- Rimuovo i Cursori
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipo="STAMPA"
        * --- Lancio la stampa
        Select * From _Cash_ Order By Data Into Cursor __Tmp__ NoFilter
        L_CodCom = this.oParentObject.w_CODCOM
        L_DECTOT= this.oParentObject.w_DECTOT
        Cp_Chprn ( "..\COMM\EXE\QUERY\GSPC_BFL.FRX", " ", this)
      case this.pTipo="DONE"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Used("_Temp_")
      Select _Temp_
      Use
    endif
    if Used("_Graph_")
      Select _Graph_
      Use
    endif
    if Used("PERIODI")
      Select PERIODI
      Use
    endif
    if Used("__Tmp__")
      Select __Tmp__
      Use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimino Cursori Grafici
    ah_Msg("Elimino dati appoggio per grafico..",.T.)
    if Used("_Cash_")
      Select _Cash_
      Use
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruzione Cursore con tutti gli intervalli del periodo da visualizzare
    Create Cursor Periodi (Periodo N(10) , Data D , Anno N(4) )
    if this.oParentObject.w_PERIODO="S"
      this.w_DATAPER = this.oParentObject.w_DATAINI+(7-DOW(CP_TODATE(this.oParentObject.w_DATAINI),2))
    else
      this.w_DATAPER = this.oParentObject.w_DATAINI
      this.w_MESE = MONTH(this.w_DATAPER)+1
      this.w_ANNO = YEAR(this.w_DATAPER)
      if this.w_MESE>12
        this.w_MESE = 1
        this.w_ANNO = this.w_ANNO+1
      endif
      this.w_MESE = RIGHT("00"+ALLTRIM(STR(this.w_MESE)), 2)
      this.w_ANNO = RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4)
      this.w_DATAPER = cp_CharToDate("01-" +this.w_MESE + "-" + this.w_ANNO) -1
    endif
    do while this.w_DATAPER<=this.oParentObject.w_DATAFIN
      Select Periodi
      Append Blank
      Replace Periodo With this.w_NUMPER
      Replace Data With this.w_DATAPER
      Replace Anno With Year(this.w_DATAPER)
      if this.oParentObject.w_PERIODO="S"
        this.w_DATAPER = this.w_DATAPER + 7
      else
        this.w_DATAPER = this.w_DATAPER + 1
        this.w_MESE = MONTH(this.w_DATAPER)+1
        this.w_ANNO = YEAR(this.w_DATAPER)
        if this.w_MESE>12
          this.w_MESE = 1
          this.w_ANNO = this.w_ANNO+1
        endif
        this.w_MESE = RIGHT("00"+ALLTRIM(STR(this.w_MESE)), 2)
        this.w_ANNO = RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4)
        this.w_DATAPER = cp_CharToDate("01-" +this.w_MESE + "-" + this.w_ANNO) -1
      endif
      this.w_NUMPER = iif( this.oParentObject.w_PERIODO="S" ,Week(this.w_DATAPER) , Month(this.w_DATAPER) )
    enddo
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
