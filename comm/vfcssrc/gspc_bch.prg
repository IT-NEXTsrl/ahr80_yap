* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bch                                                        *
*              Check strutture                                                 *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_15]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-05                                                      *
* Last revis.: 2006-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bch",oParentObject)
return(i_retval)

define class tgspc_bch as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla che in ogni struttura siano contenute tutte le attivit�
    Vq_Exec("..\COMM\EXE\QUERY\GSPC_BCH.VQR",This,"__TMP__")
    * --- Variabili da passare al Report
    L_CODCOM=this.oParentObject.w_CODCOM
    L_DATAINI=this.oParentObject.w_DATAINI
    L_DATAFIN=this.oParentObject.w_DATAFIN
    Cp_ChPrn("..\COMM\EXE\QUERY\GSPC_BCH.FRX", " ", this)
    if USED("__TMP__")
      Select __TMP__
      Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
