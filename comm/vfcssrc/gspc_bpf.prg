* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bpf                                                        *
*              Batch pianificazione                                            *
*                                                                              *
*      Author: NICOLA GORLANDI                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_134]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-19                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bpf",oParentObject)
return(i_retval)

define class tgspc_bpf as StdBatch
  * --- Local variables
  w_oErrorLog = .NULL.
  w_MSGPRM1 = space(10)
  w_CENCOS = space(15)
  w_OLDSERIAL = space(10)
  w_CODATT = space(15)
  w_DOCMAKE = 0
  w_DATREG = ctod("  /  /  ")
  w_MVSERIAL = space(10)
  w_CODLIS = space(5)
  w_MVNUMDOC = 0
  w_CODKEY = space(20)
  w_MVNUMREG = 0
  w_CODART = space(20)
  w_MVANNDOC = space(4)
  w_MVALFDOC = space(10)
  w_UNIMIS = space(3)
  w_MVPRP = space(2)
  w_QTAMOV = 0
  w_MVTIPREG = space(1)
  w_PREZZO = 0
  w_NOTE = space(0)
  w_OK = .f.
  w_CODCOS = space(5)
  w_MESS = space(200)
  w_QTA1UM = 0
  w_ERRORI = 0
  w_VALRIG = 0
  w_IMPNAZ = 0
  w_VOCCOS = space(15)
  w_KEYSAL = space(20)
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_CODMAG1 = space(5)
  w_ROWORD = 0
  w_MVCODUTE = 0
  w_DATINI = ctod("  /  /  ")
  w_MVCODESE = space(4)
  w_CA__TIPO = space(1)
  w_MVPRD = space(2)
  w_MADESCRI = space(30)
  w_TIPATT = space(1)
  w_CODCLA = space(2)
  w_MVFLULCA = space(1)
  w_CATCON = space(5)
  w_MVFLULPV = space(1)
  w_CODIVA = space(5)
  w_MVVALULT = 0
  w_LOCCODVAL = space(3)
  w_DECTOT = 0
  w_IMPCOM = 0
  w_PESNET = 0
  w_NOMENC = space(8)
  w_FLTRAS = space(1)
  w_UMSUPP = space(3)
  w_MOLSUP = 0
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_MASERIAL = space(10)
  w_DATADOCU = ctod("  /  /  ")
  w_MVFLSCOM = space(1)
  w_VALMAG = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  ATTIVITA_idx=0
  CAU_CONT_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  LISTINI_idx=0
  MAT_MAST_idx=0
  SALDIART_idx=0
  VALUTE_idx=0
  SALDICOM_idx=0
  SALOTCOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione documenti interni pianificazione (da GSPC_SPF)
    * --- La procedura crea per ogni attivit� con data inizio all'interno dell'intervallo
    * --- un documento partendo dal movimento di preventivo associato ad essa
    * --- La procedura marca come tempificate queste attivit�
    * --- Non sono gestiti documenti con Causale di Magazzino Collegata !
    * --- Numero Documenti Creati
    this.w_DOCMAKE = 0
    this.w_ERRORI = 0
    * --- Crea il File delle Messaggistiche di Errore
    this.w_oErrorLog=createobject("AH_ERRORLOG")
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_DOCMAKE>0 Or this.w_ERRORI>0
      if this.w_DOCMAKE=1
        this.w_MESS = "Creato un documento interno"
      else
        this.w_MESS = "Creati %1 documenti interni"
        this.w_MSGPRM1 = Alltrim( Str ( this.w_DOCMAKE) )
      endif
      if this.w_ERRORI>0
        if this.w_ERRORI=1
          if this.w_DOCMAKE=1
            this.w_MESS = "Creato un documento interno con un errore%0Si desidera stampare i messaggi di errore?"
          else
            this.w_MESS = "Creati %1 documenti interni con un errore%0Si desidera stampare i messaggi di errore?"
          endif
        else
          if this.w_DOCMAKE=1
            this.w_MESS = "Creato un documento interno con %2 errori%0Si desidera stampare i messaggi di errore?"
          else
            this.w_MESS = "Creati %1 documenti interni con %2 errori%0Si desidera stampare i messaggi di errore?"
          endif
        endif
        if ah_YESNO( this.w_MESS,"", Alltrim(Str(this.w_DOCMAKE)), Alltrim(Str( this.w_ERRORI )))
          this.w_oErrorLog.PRINTLOG(THIS , "Segnalazioni in fase di generazione")     
        endif
      else
        ah_ErrorMsg(this.w_MESS,"i","" , Alltrim(Str(this.w_DOCMAKE)), Alltrim(Str( this.w_ERRORI )) )
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Chiudo la Maschera
      This.oParentObject.EcpQuit()
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuovo i cursori
    if Used ("_CursMov_")
      Select _CursMov_
      Use
    endif
    if Used ("_CursMast_")
      Select _CursMast_
      Use
    endif
    if Used ("MessErr")
      Select MessErr
      Use
    endif
    if Used ("_TempMov_")
      Select _TempMov_
      Use
    endif
    if Used ("_MovTemp_")
      Select _MovTemp_
      Use
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dichiarazione variabili Locali
    * --- Data del documento generato
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguo la Query per recuperare tutti i movimenti Preventivi e le loro righe
    * --- Eseguo un ciclo sul cusore - se l'attivit� non ha mov. associato la tempifico direttamente
    Select (This.oParentObject.w_ZOOM.cCursor)
    Scan For Xchk=1
    this.w_CODATT = ATCODATT
    if Empty( Nvl ( MASERIAL,"" ) )
      * --- Non ha un movimento Associato la tempifico comunque
      * --- Try
      local bErr_0412BCF8
      bErr_0412BCF8=bTrsErr
      this.Try_0412BCF8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_ERRORI = this.w_ERRORI+1
        this.w_MESS = "%1%2Attivit�: %3%0%4"
        this.w_oErrorLog.ADDMSGLOG(this.w_MESS , Alltrim( i_ErrMsg ) , chr( 32 ) , Alltrim( this.w_CODATT ) , Message( ))     
      endif
      bTrsErr=bTrsErr or bErr_0412BCF8
      * --- End
    else
      * --- ha un movimento associato: modificata la query filtrando sul maserial
      * --- (prima se c'erano pi� movimenti con commessa,attivit� e date ok perdeva il filtro su xchk)
      this.w_MASERIAL = MASERIAL
      Vq_Exec ( "..\COMM\EXE\QUERY\GSPC_BPF.VQR" , This , "_TempMov_" )
      if Used("_MovTemp_")
        Select _TempMov_
        Go Top
        if RecCount()>0
          Select * From _TempMov_ ;
          Into Array Movimenti
          Insert into _MovTemp_ From Array Movimenti
        endif
      else
        Select * From _TempMov_ ;
        Into Cursor _MovTemp_ NoFilter
        Cur=WrCursor ("_MovTemp_")
      endif
    endif
    EndScan
    if Used("_MovTemp_")
      * --- Ordinato per il seriale
      Select * from _MovTemp_ Into Cursor _CursMov_ NoFilter Order By 1
      Select _CursMov_
      Go Top
      if RecCount()=0
        ah_ErrorMsg("Non esistono movimenti da elaborare",,"")
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
    else
      ah_ErrorMsg("Non esistono movimenti da elaborare",,"")
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    this.w_CODMAG1 = this.oParentObject.w_CODMAG
    this.w_TIPATT = "A"
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    * --- Costruisco il cursor contenente solo i dati di testata
    Select * From _CursMov_ Group By MASERIAL Into Cursor _CursMast_
    Select _CursMast_
    Go Top
    Scan
    this.w_OK = .T.
    * --- Campi legati all'articolo della prima riga (voce e centro di costo di testata sono calcolati sulla prma riga)
    * --- Aggiornamento Testata
    this.w_SERIAL = _CursMast_.MASERIAL
    * --- Try
    local bErr_040B1258
    bErr_040B1258=bTrsErr
    this.Try_040B1258()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.w_ERRORI = this.w_ERRORI+1
      this.w_MESS = "%1%2Attivit�: %3%0%4"
      this.w_oErrorLog.ADDMSGLOG(this.w_MESS , Alltrim( i_ErrMsg ) , chr( 32 ) , Alltrim( this.w_CODATT ) , Message( ))     
    endif
    bTrsErr=bTrsErr or bErr_040B1258
    * --- End
    Select _CursMast_
    EndScan
  endproc
  proc Try_0412BCF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ATTIVITA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AT_STATO ="+cp_NullLink(cp_ToStrODBC("Z"),'ATTIVITA','AT_STATO');
          +i_ccchkf ;
      +" where ";
          +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
          +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPATT);
          +" and ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
             )
    else
      update (i_cTable) set;
          AT_STATO = "Z";
          &i_ccchkf. ;
       where;
          ATCODCOM = this.oParentObject.w_CODCOM;
          and ATTIPATT = this.w_TIPATT;
          and ATCODATT = this.w_CODATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_040B1258()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Al cambio del Movimento creo un nuovo documento
    this.w_CENCOS = Nvl ( _CursMast_.ATCENCOS , "" )
    this.oParentObject.w_CODCOM = Nvl ( _CursMast_.MACODCOM , "" )
    * --- In realt� non cambia mai (� un filtro)
    this.w_CODATT = Nvl ( _CursMast_.MACODATT , "" )
    if EMPTY(CP_TODATE(this.oParentObject.w_DOINDATA))
      this.w_DATADOCU = Nvl ( _CursMast_.MADATREG , "" )
    else
      this.w_DATADOCU = this.oParentObject.w_DOINDATA
    endif
    * --- La data registrazione non pu� essere inferiore alla data documento
    this.w_DATREG = max(this.w_DATADOCU,i_datsys)
    if Not Empty(CHKCONS(this.oParentObject.w_MVFLVEAC+iif(this.oParentObject.w_FLIMPE="+","M","") + iif(this.oParentObject.w_FLANAL="S","C",""),this.w_DATREG,"B","N"))
      * --- Raise
      i_Error=CHKCONS(this.oParentObject.w_MVFLVEAC+iif(this.oParentObject.w_FLIMPE="+","M","")+iif(this.oParentObject.w_FLANAL="S","C",""),this.w_DATREG,"B","N")
      return
    endif
    this.w_CODLIS = Nvl ( _CursMast_.MACODLIS , "" )
    this.w_DATINI = CP_TODATE( _CursMast_.ATDATINI)
    this.w_MVANNDOC = AllTrim(Str( Year(i_Datsys) ) )
    this.w_MVSERIAL = Space(10)
    this.w_MVNUMDOC = 0
    this.w_MVNUMREG = 0
    this.w_MVCODUTE = g_CODUTE
    this.w_MVCODESE = g_CODESE
    this.w_MVPRD = this.oParentObject.w_PRD
    this.w_MVALFDOC = this.oParentObject.w_ALFDOC
    this.w_LOCCODVAL = Nvl ( _CursMast_.MACODVAL , "" )
    if Empty( this.w_LOCCODVAL )
      * --- Se vuota la valuta del Movimento (non eseguito aggiornamento Database) valuta di commessa
      this.w_LOCCODVAL = this.oParentObject.w_CODVAL
    endif
    * --- Se il cambio sul movimento � 0 o null prendo il cambio della valuta di commessa dalla maschera
    if Nvl ( _CursMast_.MACAOVAL , 0 ) <> 0
      this.oParentObject.w_CAOVAL = Nvl ( _CursMast_.MACAOVAL , 0 )
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- Calcolo gli AutoNumber
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEDOC", "i_codazi,w_MVSERIAL")
    cp_NextTableProg(this, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    this.w_MVNUMREG = this.w_MVNUMDOC
    * --- Creo le variabili necessarie alla testata
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCTIPREG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCAUCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCTIPREG;
        from (i_cTable) where;
            CCCODICE = this.oParentObject.w_MVCAUCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_TIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVPRP = IIF(w_TIPREG="A" AND w_MVCLADOC $ "FA-NC", "AC", "NN")
    * --- Calcola Sconti su Omaggio
    this.w_MVFLSCOM = IIF(g_FLSCOM="S" AND this.w_DATADOCU<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)
    * --- Try
    local bErr_040CEEA8
    bErr_040CEEA8=bTrsErr
    this.Try_040CEEA8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_OK = .F.
      * --- Raise
      i_Error=AH_MSGFORMAT("Errore in inserimento della testata documento")
      return
    endif
    bTrsErr=bTrsErr or bErr_040CEEA8
    * --- End
    if this.w_OK
      * --- Inserisco il Dettaglio
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_LOCCODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.w_LOCCODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      Select _CursMov_
      Scan For MASERIAL=this.w_SERIAL
      this.w_CODKEY = Nvl ( _CursMov_.MACODKEY , "" )
      this.w_CODART = Nvl ( _CursMov_.MACODART , "" )
      this.w_UNIMIS = Nvl ( _CursMov_.MAUNIMIS , "" )
      this.w_QTAMOV = Nvl ( _CursMov_.MAQTAMOV , 0 )
      this.w_PREZZO = Nvl ( _CursMov_.MAPREZZO , 0 )
      this.w_NOTE = Nvl ( _CursMov_.MA__NOTE , "" )
      this.w_CODCOS = Nvl ( _CursMov_.MACODCOS , "" )
      this.w_QTA1UM = Nvl ( _CursMov_.MAQTA1UM , 0 )
      * --- Funzione per calcolo MVVALRIG: passo 0 al posto dei 4 sconti.
      this.w_VALRIG = CAVALRIG(this.w_PREZZO,this.w_QTAMOV, 0,0,0,0,this.w_DECTOT)
      this.w_CODIVA = CALCODIV(this.w_CODART, " ", Space(15), Space(10), iif( this.oParentObject.w_MVFLVEAC="V", this.w_DATREG, this.w_DATADOCU))
      * --- Non Avendo scorporo passo la percentuale a 0 (� ininfluente)
      this.w_VALMAG = CAVALMAG(" ", this.w_VALRIG, 0, 0, 0, this.w_DECTOT, Space(3), 0 )
      this.w_ROWNUM = Nvl ( _CursMov_.CPROWNUM , "" )
      this.w_ROWORD = Nvl ( _CursMov_.CPROWORD , "" )
      this.w_CA__TIPO = Nvl ( _CursMov_.CA__TIPO , "" )
      this.w_MADESCRI = Nvl ( _CursMov_.MADESCRI , "" )
      this.w_CODCLA = Nvl ( _CursMov_.ARCODCLA , "" )
      * --- Funzione per il calcolo di MVIMPNAZ: poich� � sempre un documento di vendita 
      *     non passo il codice Iva Agevolato, la sua percentuale Iva indetraibile e nemmeno la percentuale Iva Indetr. di Riga
      this.w_IMPNAZ = CAIMPNAZ(this.oParentObject.w_MVFLVEAC, this.w_VALMAG, this.oParentObject.w_CAOVAL, g_CAOVAL, this.w_DATREG, g_PERVAL, this.w_LOCCODVAL, " ", 0, 0, 0, 0 )
      this.w_IMPCOM = CAIMPCOM( IIF(Empty(this.oParentObject.w_CODCOM),"S", " "), g_PERVAL, this.oParentObject.w_CODVAL, this.w_IMPNAZ, g_CAOVAL, this.w_DATREG, this.oParentObject.w_DECCOM ) 
      this.w_KEYSAL = IIF( this.w_CA__TIPO="R" And Not Empty(this.oParentObject.w_FLIMPE) , this.w_CODART , Space (20) )
      this.w_CODMAG1 = IIF( this.w_CA__TIPO="R" And Not Empty(this.oParentObject.w_FLIMPE) , this.oParentObject.w_CODMAG , Space (5) )
      this.w_CATCON = Nvl ( _CursMov_.ARCATCON , "" )
      * --- Voce di Costo
      this.w_VOCCOS = Nvl ( _CursMov_.CAVOCCOS , "" )
      * --- Dati INTRA
      this.w_PESNET = NVL(_CursMov_.ARPESNET, 0)
      this.w_NOMENC = NVL(_CursMov_.ARNOMENC, SPACE(8))
      this.w_FLTRAS = IIF(NVL(_CursMov_.ARDATINT,"F")="S","Z",IIF( NVL(_CursMov_.ARDATINT,"F")="F"," ",IIF( NVL(_CursMov_.ARDATINT,"F")="I","I","S")))
      this.w_UMSUPP = NVL(_CursMov_.ARUMSUPP, SPACE(3))
      this.w_MOLSUP = NVL(_CursMov_.ARMOLSUP, 0)
      * --- Il Documento generato non aumenta mai ne il carico ne lo scarico
      this.w_MVFLULCA = " "
      this.w_MVFLULPV = " "
      this.w_MVVALULT = IIF( this.w_QTA1UM=0, 0, cp_ROUND( this.w_IMPNAZ / this.w_QTA1UM, 2))
      * --- Try
      local bErr_04106B30
      bErr_04106B30=bTrsErr
      this.Try_04106B30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error=AH_MSGFORMAT("Errore in inserimento del dettaglio documento riga %1" , Alltrim(Str(this.w_ROWNUM)) )
        return
      endif
      bTrsErr=bTrsErr or bErr_04106B30
      * --- End
      * --- Aggiorno i Saldi
      if Not Empty(this.w_CODMAG1)
        * --- Try
        local bErr_04108C00
        bErr_04108C00=bTrsErr
        this.Try_04108C00()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error=AH_MSGFORMAT("Errore in inserimento nei saldi articoli: %1" , Alltrim(this.w_CODART))
          return
        endif
        bTrsErr=bTrsErr or bErr_04108C00
        * --- End
        * --- Try
        local bErr_041087E0
        bErr_041087E0=bTrsErr
        this.Try_041087E0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error=AH_MSGFORMAT("Errore in inserimento nei saldi commessa: %1" , Alltrim(this.w_CODART))
          return
        endif
        bTrsErr=bTrsErr or bErr_041087E0
        * --- End
      endif
      Select _CursMov_
      EndScan
    endif
    * --- Marca come tempificate le Attivita - Un movimento preventivo, una Attivit�
    * --- Try
    local bErr_040D01C8
    bErr_040D01C8=bTrsErr
    this.Try_040D01C8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_OK = .F.
      * --- Raise
      i_Error=AH_MSGFORMAT( "Errore in scrittura del flag dello stato dell'attivit�: %1" , this.w_CODATT )
      return
    endif
    bTrsErr=bTrsErr or bErr_040D01C8
    * --- End
    if this.w_OK
      this.w_DOCMAKE = this.w_DOCMAKE+1
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_040CEEA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",MVCODESE"+",MVCODUTE"+",MVNUMREG"+",MVDATREG"+",MVTIPDOC"+",MVCLADOC"+",MVFLVEAC"+",MVFLACCO"+",MVFLINTE"+",MVFLPROV"+",MVPRD"+",MVPRP"+",MVNUMDOC"+",MVALFDOC"+",MVDATDOC"+",MVANNDOC"+",MVANNPRO"+",MVTCAMAG"+",MVTCOLIS"+",MVCAUCON"+",MVCODVAL"+",MVCAOVAL"+",MVVALNAZ"+",MVFLGIOM"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",MVALFEST"+",MVCONCON"+",MVTIPCON"+",MVFLSCOR"+",MVFLRTRA"+",MVFLRIMB"+",MVTIPORN"+",MVTFRAGG"+",MVMOVCOM"+",MVACCONT"+",MVACCPRE"+",MVRIFACC"+",MVFLSCOM"+",MVEMERIC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_MAST','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(g_CODESE),'DOC_MAST','MVCODESE');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','MVCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMREG),'DOC_MAST','MVNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'DOC_MAST','MVDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPDOCU),'DOC_MAST','MVTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC("DI"),'DOC_MAST','MVCLADOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLACCO),'DOC_MAST','MVFLACCO');
      +","+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVFLINTE');
      +","+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPRD),'DOC_MAST','MVPRD');
      +","+cp_NullLink(cp_ToStrODBC("NN"),'DOC_MAST','MVPRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'DOC_MAST','MVALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATADOCU),'DOC_MAST','MVDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVANNDOC),'DOC_MAST','MVANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVANNDOC),'DOC_MAST','MVANNPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCAUMAG),'DOC_MAST','MVTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODLIS),'DOC_MAST','MVTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCAUCON),'DOC_MAST','MVCAUCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCCODVAL),'DOC_MAST','MVCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAOVAL),'DOC_MAST','MVCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'DOC_MAST','MVVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DOC_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC("  "),'DOC_MAST','MVALFEST');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVCONCON');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLSCOR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLRTRA');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLRIMB');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVTIPORN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLRAGG),'DOC_MAST','MVTFRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'DOC_MAST','MVMOVCOM');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_MAST','MVACCONT');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_MAST','MVACCPRE');
      +","+cp_NullLink(cp_ToStrODBC("          "),'DOC_MAST','MVRIFACC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVEMERIC),'DOC_MAST','MVEMERIC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_MVSERIAL,'MVCODESE',g_CODESE,'MVCODUTE',i_CODUTE,'MVNUMREG',this.w_MVNUMREG,'MVDATREG',this.w_DATREG,'MVTIPDOC',this.oParentObject.w_TIPDOCU,'MVCLADOC',"DI",'MVFLVEAC',this.oParentObject.w_MVFLVEAC,'MVFLACCO',this.oParentObject.w_MVFLACCO,'MVFLINTE',"N",'MVFLPROV',"N",'MVPRD',this.w_MVPRD)
      insert into (i_cTable) (MVSERIAL,MVCODESE,MVCODUTE,MVNUMREG,MVDATREG,MVTIPDOC,MVCLADOC,MVFLVEAC,MVFLACCO,MVFLINTE,MVFLPROV,MVPRD,MVPRP,MVNUMDOC,MVALFDOC,MVDATDOC,MVANNDOC,MVANNPRO,MVTCAMAG,MVTCOLIS,MVCAUCON,MVCODVAL,MVCAOVAL,MVVALNAZ,MVFLGIOM,UTCC,UTDC,UTCV,UTDV,MVALFEST,MVCONCON,MVTIPCON,MVFLSCOR,MVFLRTRA,MVFLRIMB,MVTIPORN,MVTFRAGG,MVMOVCOM,MVACCONT,MVACCPRE,MVRIFACC,MVFLSCOM,MVEMERIC &i_ccchkf. );
         values (;
           this.w_MVSERIAL;
           ,g_CODESE;
           ,i_CODUTE;
           ,this.w_MVNUMREG;
           ,this.w_DATREG;
           ,this.oParentObject.w_TIPDOCU;
           ,"DI";
           ,this.oParentObject.w_MVFLVEAC;
           ,this.oParentObject.w_MVFLACCO;
           ,"N";
           ,"N";
           ,this.w_MVPRD;
           ,"NN";
           ,this.w_MVNUMDOC;
           ,this.w_MVALFDOC;
           ,this.w_DATADOCU;
           ,this.w_MVANNDOC;
           ,this.w_MVANNDOC;
           ,this.oParentObject.w_MVCAUMAG;
           ,this.w_CODLIS;
           ,this.oParentObject.w_MVCAUCON;
           ,this.w_LOCCODVAL;
           ,this.oParentObject.w_CAOVAL;
           ,g_PERVAL;
           ," ";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,"  ";
           ," ";
           ," ";
           ," ";
           ," ";
           ," ";
           ," ";
           ,this.oParentObject.w_MVFLRAGG;
           ,this.w_SERIAL;
           ,0;
           ,0;
           ,"          ";
           ,this.w_MVFLSCOM;
           ,this.oParentObject.w_MVEMERIC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04106B30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVDESART"+",MVDESSUP"+",MVUNIMIS"+",MVCATCON"+",MVCAUMAG"+",MVCODCLA"+",MVCODMAG"+",MVCODLIS"+",MVQTAMOV"+",MVQTAUM1"+",MVPREZZO"+",MVCODIVA"+",MVVALRIG"+",MVFLOMAG"+",MVVALMAG"+",MVIMPNAZ"+",MVKEYSAL"+",MVFLCASC"+",MVF2CASC"+",MVFLORDI"+",MVF2ORDI"+",MVFLIMPE"+",MVF2IMPE"+",MVFLRISE"+",MVF2RISE"+",MVVOCCEN"+",MVCODCEN"+",MVCODCOM"+",MVFLORCO"+",MVFLCOCO"+",MVIMPCOM"+",MVQTASAL"+",MVDATEVA"+",MVIMPEVA"+",MVTIPATT"+",MVCODATT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_DETT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DOC_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(-20),'DOC_DETT','MVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DOC_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CA__TIPO),'DOC_DETT','MVTIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODKEY),'DOC_DETT','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'DOC_DETT','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MADESCRI),'DOC_DETT','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'DOC_DETT','MVDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'DOC_DETT','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATCON),'DOC_DETT','MVCATCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLA),'DOC_DETT','MVCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG1),'DOC_DETT','MVCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODLIS),'DOC_DETT','MVCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAMOV),'DOC_DETT','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTA1UM),'DOC_DETT','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZO),'DOC_DETT','MVPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODIVA),'DOC_DETT','MVCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALRIG),'DOC_DETT','MVVALRIG');
      +","+cp_NullLink(cp_ToStrODBC("X"),'DOC_DETT','MVFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALMAG),'DOC_DETT','MVVALMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPNAZ),'DOC_DETT','MVIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'DOC_DETT','MVKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2CASC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLORDI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2ORDI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLIMPE),'DOC_DETT','MVFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2IMPE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2RISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VOCCOS),'DOC_DETT','MVVOCCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CENCOS),'DOC_DETT','MVCODCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM),'DOC_DETT','MVCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLORCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLCOCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCOM),'DOC_DETT','MVIMPCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTA1UM),'DOC_DETT','MVQTASAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'DOC_DETT','MVDATEVA');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
      +","+cp_NullLink(cp_ToStrODBC("A"),'DOC_DETT','MVTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'DOC_DETT','MVCODATT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_MVSERIAL,'CPROWNUM',this.w_ROWNUM,'MVNUMRIF',-20,'CPROWORD',this.w_ROWORD,'MVTIPRIG',this.w_CA__TIPO,'MVCODICE',this.w_CODKEY,'MVCODART',this.w_CODART,'MVDESART',this.w_MADESCRI,'MVDESSUP',this.w_NOTE,'MVUNIMIS',this.w_UNIMIS,'MVCATCON',this.w_CATCON,'MVCAUMAG',this.oParentObject.w_MVCAUMAG)
      insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVDESART,MVDESSUP,MVUNIMIS,MVCATCON,MVCAUMAG,MVCODCLA,MVCODMAG,MVCODLIS,MVQTAMOV,MVQTAUM1,MVPREZZO,MVCODIVA,MVVALRIG,MVFLOMAG,MVVALMAG,MVIMPNAZ,MVKEYSAL,MVFLCASC,MVF2CASC,MVFLORDI,MVF2ORDI,MVFLIMPE,MVF2IMPE,MVFLRISE,MVF2RISE,MVVOCCEN,MVCODCEN,MVCODCOM,MVFLORCO,MVFLCOCO,MVIMPCOM,MVQTASAL,MVDATEVA,MVIMPEVA,MVTIPATT,MVCODATT &i_ccchkf. );
         values (;
           this.w_MVSERIAL;
           ,this.w_ROWNUM;
           ,-20;
           ,this.w_ROWORD;
           ,this.w_CA__TIPO;
           ,this.w_CODKEY;
           ,this.w_CODART;
           ,this.w_MADESCRI;
           ,this.w_NOTE;
           ,this.w_UNIMIS;
           ,this.w_CATCON;
           ,this.oParentObject.w_MVCAUMAG;
           ,this.w_CODCLA;
           ,this.w_CODMAG1;
           ,this.w_CODLIS;
           ,this.w_QTAMOV;
           ,this.w_QTA1UM;
           ,this.w_PREZZO;
           ,this.w_CODIVA;
           ,this.w_VALRIG;
           ,"X";
           ,this.w_VALMAG;
           ,this.w_IMPNAZ;
           ,this.w_KEYSAL;
           ," ";
           ," ";
           ," ";
           ," ";
           ,this.oParentObject.w_FLIMPE;
           ," ";
           ," ";
           ," ";
           ,this.w_VOCCOS;
           ,this.w_CENCOS;
           ,this.oParentObject.w_CODCOM;
           ," ";
           ," ";
           ,this.w_IMPCOM;
           ,this.w_QTA1UM;
           ,this.w_DATINI;
           ,0;
           ,"A";
           ,this.w_CODATT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Scrivo gli altri campi
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALULT),'DOC_DETT','MVVALULT');
      +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLULPV),'DOC_DETT','MVFLULPV');
      +",MVFLULCA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLULCA),'DOC_DETT','MVFLULCA');
      +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
      +",MVFLRAGG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLRAGG),'DOC_DETT','MVFLRAGG');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
      +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLELGM');
      +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLELAN');
      +",MVFLARIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
      +",MVCODCOS ="+cp_NullLink(cp_ToStrODBC(this.w_CODCOS),'DOC_DETT','MVCODCOS');
      +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
      +",MVPESNET ="+cp_NullLink(cp_ToStrODBC(this.w_PESNET),'DOC_DETT','MVPESNET');
      +",MVNOMENC ="+cp_NullLink(cp_ToStrODBC(this.w_NOMENC),'DOC_DETT','MVNOMENC');
      +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(this.w_FLTRAS),'DOC_DETT','MVFLTRAS');
      +",MVUMSUPP ="+cp_NullLink(cp_ToStrODBC(this.w_UMSUPP),'DOC_DETT','MVUMSUPP');
      +",MVMOLSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MOLSUP),'DOC_DETT','MVMOLSUP');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
          +" and MVNUMRIF = "+cp_ToStrODBC(-20);
             )
    else
      update (i_cTable) set;
          MVVALULT = this.w_MVVALULT;
          ,MVFLULPV = this.w_MVFLULPV;
          ,MVFLULCA = this.w_MVFLULCA;
          ,MVFLRIPA = " ";
          ,MVFLRAGG = this.oParentObject.w_MVFLRAGG;
          ,MVFLEVAS = " ";
          ,MVFLERIF = " ";
          ,MVFLELGM = " ";
          ,MVFLELAN = " ";
          ,MVFLARIF = " ";
          ,MVCODCOS = this.w_CODCOS;
          ,MV_SEGNO = this.oParentObject.w_MV_SEGNO;
          ,MVPESNET = this.w_PESNET;
          ,MVNOMENC = this.w_NOMENC;
          ,MVFLTRAS = this.w_FLTRAS;
          ,MVUMSUPP = this.w_UMSUPP;
          ,MVMOLSUP = this.w_MOLSUP;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;
          and CPROWNUM = this.w_ROWNUM;
          and MVNUMRIF = -20;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04108C00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_040FD140
    bErr_040FD140=bTrsErr
    this.Try_040FD140()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_QTA1UM);
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG1);
               )
      else
        update (i_cTable) set;
            SLQTIPER = SLQTIPER + this.w_QTA1UM;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_KEYSAL;
            and SLCODMAG = this.w_CODMAG1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_040FD140
    * --- End
    return
  proc Try_041087E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARSALCOM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARSALCOM;
        from (i_cTable) where;
            ARCODART = this.w_CODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_SALCOM="S"
      if empty(nvl(this.oParentObject.w_CODCOM,""))
        this.oParentObject.w_CODCOM = this.w_COMMDEFA
      endif
      * --- Try
      local bErr_0410AA90
      bErr_0410AA90=bTrsErr
      this.Try_0410AA90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_QTA1UM);
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG1);
              +" and SCCODCAN = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
                 )
        else
          update (i_cTable) set;
              SCQTIPER = SCQTIPER + this.w_QTA1UM;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_KEYSAL;
              and SCCODMAG = this.w_CODMAG1;
              and SCCODCAN = this.oParentObject.w_CODCOM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa'
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_0410AA90
      * --- End
    endif
    return
  proc Try_040D01C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ATTIVITA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AT_STATO ="+cp_NullLink(cp_ToStrODBC("Z"),'ATTIVITA','AT_STATO');
          +i_ccchkf ;
      +" where ";
          +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
          +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPATT);
          +" and ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
             )
    else
      update (i_cTable) set;
          AT_STATO = "Z";
          &i_ccchkf. ;
       where;
          ATCODCOM = this.oParentObject.w_CODCOM;
          and ATTIPATT = this.w_TIPATT;
          and ATCODATT = this.w_CODATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_040FD140()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+",SLQTAPER"+",SLQTRPER"+",SLQTOPER"+",SLQTIPER"+",SLVALUCA"+",SLDATUCA"+",SLCODVAA"+",SLVALUPV"+",SLDATUPV"+",SLCODVAV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG1),'SALDIART','SLCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTAPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTRPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTOPER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTA1UM),'SALDIART','SLQTIPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLVALUCA');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'SALDIART','SLDATUCA');
      +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'SALDIART','SLCODVAA');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLVALUPV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'SALDIART','SLDATUPV');
      +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'SALDIART','SLCODVAV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_KEYSAL,'SLCODMAG',this.w_CODMAG1,'SLQTAPER',0,'SLQTRPER',0,'SLQTOPER',0,'SLQTIPER',this.w_QTA1UM,'SLVALUCA',0,'SLDATUCA',cp_CharToDate("  -  -  "),'SLCODVAA',g_PERVAL,'SLVALUPV',0,'SLDATUPV',cp_CharToDate("  -  -  "),'SLCODVAV',g_PERVAL)
      insert into (i_cTable) (SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER,SLQTOPER,SLQTIPER,SLVALUCA,SLDATUCA,SLCODVAA,SLVALUPV,SLDATUPV,SLCODVAV &i_ccchkf. );
         values (;
           this.w_KEYSAL;
           ,this.w_CODMAG1;
           ,0;
           ,0;
           ,0;
           ,this.w_QTA1UM;
           ,0;
           ,cp_CharToDate("  -  -  ");
           ,g_PERVAL;
           ,0;
           ,cp_CharToDate("  -  -  ");
           ,g_PERVAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0410AA90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+",SCQTAPER"+",SCQTRPER"+",SCQTOPER"+",SCQTIPER"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG1),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'SALDICOM','SCCODART');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTAPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTRPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTOPER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTA1UM),'SALDICOM','SCQTIPER');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_KEYSAL,'SCCODMAG',this.w_CODMAG1,'SCCODCAN',this.oParentObject.w_CODCOM,'SCCODART',this.w_CODART,'SCQTAPER',0,'SCQTRPER',0,'SCQTOPER',0,'SCQTIPER',this.w_QTA1UM)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART,SCQTAPER,SCQTRPER,SCQTOPER,SCQTIPER &i_ccchkf. );
         values (;
           this.w_KEYSAL;
           ,this.w_CODMAG1;
           ,this.oParentObject.w_CODCOM;
           ,this.w_CODART;
           ,0;
           ,0;
           ,0;
           ,this.w_QTA1UM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='MAT_MAST'
    this.cWorkTables[8]='SALDIART'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='SALDICOM'
    this.cWorkTables[11]='SALOTCOM'
    return(this.OpenAllTables(11))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
