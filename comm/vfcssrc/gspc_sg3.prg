* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_sg3                                                        *
*              Opzioni copia progetto                                          *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-21                                                      *
* Last revis.: 2007-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_sg3",oParentObject))

* --- Class definition
define class tgspc_sg3 as StdForm
  Top    = 59
  Left   = 112

  * --- Standard Properties
  Width  = 311
  Height = 155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-11-09"
  HelpContextID=23746665
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gspc_sg3"
  cComment = "Opzioni copia progetto"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CopiaSchedeTecniche = space(1)
  w_CopiaPreventiviAForfait = space(1)
  w_CopiaAttivitaPrecedenti = space(1)
  w_TIPSTR = space(1)
  o_TIPSTR = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_sg3Pag1","gspc_sg3",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCopiaSchedeTecniche_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='ATTIVITA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CopiaSchedeTecniche=space(1)
      .w_CopiaPreventiviAForfait=space(1)
      .w_CopiaAttivitaPrecedenti=space(1)
      .w_TIPSTR=space(1)
      .w_CopiaSchedeTecniche=oParentObject.w_CopiaSchedeTecniche
      .w_CopiaPreventiviAForfait=oParentObject.w_CopiaPreventiviAForfait
      .w_CopiaAttivitaPrecedenti=oParentObject.w_CopiaAttivitaPrecedenti
      .w_TIPSTR=oParentObject.w_TIPSTR
    endwith
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CopiaSchedeTecniche=.w_CopiaSchedeTecniche
      .oParentObject.w_CopiaPreventiviAForfait=.w_CopiaPreventiviAForfait
      .oParentObject.w_CopiaAttivitaPrecedenti=.w_CopiaAttivitaPrecedenti
      .oParentObject.w_TIPSTR=.w_TIPSTR
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPSTR<>.w_TIPSTR
            .w_CopiaPreventiviAForfait = iif(.w_TIPSTR<>'P','N',.w_CopiaPreventiviAForfait)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCopiaPreventiviAForfait_1_4.visible=!this.oPgFrm.Page1.oPag.oCopiaPreventiviAForfait_1_4.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCopiaSchedeTecniche_1_1.RadioValue()==this.w_CopiaSchedeTecniche)
      this.oPgFrm.Page1.oPag.oCopiaSchedeTecniche_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCopiaPreventiviAForfait_1_4.RadioValue()==this.w_CopiaPreventiviAForfait)
      this.oPgFrm.Page1.oPag.oCopiaPreventiviAForfait_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCopiaAttivitaPrecedenti_1_5.RadioValue()==this.w_CopiaAttivitaPrecedenti)
      this.oPgFrm.Page1.oPag.oCopiaAttivitaPrecedenti_1_5.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPSTR = this.w_TIPSTR
    return

enddefine

* --- Define pages as container
define class tgspc_sg3Pag1 as StdContainer
  Width  = 307
  height = 155
  stdWidth  = 307
  stdheight = 155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCopiaSchedeTecniche_1_1 as StdCheck with uid="FUJYXOPUVK",rtseq=1,rtrep=.f.,left=18, top=29, caption="Copia schede tecniche",;
    ToolTipText = "Schede tecniche",;
    HelpContextID = 29856887,;
    cFormVar="w_CopiaSchedeTecniche", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCopiaSchedeTecniche_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCopiaSchedeTecniche_1_1.GetRadio()
    this.Parent.oContained.w_CopiaSchedeTecniche = this.RadioValue()
    return .t.
  endfunc

  func oCopiaSchedeTecniche_1_1.SetRadio()
    this.Parent.oContained.w_CopiaSchedeTecniche=trim(this.Parent.oContained.w_CopiaSchedeTecniche)
    this.value = ;
      iif(this.Parent.oContained.w_CopiaSchedeTecniche=='S',1,;
      0)
  endfunc


  add object oBtn_1_2 as StdButton with uid="AULBKDHRDT",left=197, top=106, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 67199254;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="AYUBAIGGVO",left=251, top=106, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 67199254;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCopiaPreventiviAForfait_1_4 as StdCheck with uid="BVDZXUPJZA",rtseq=2,rtrep=.f.,left=18, top=2, caption="Copia preventivi a forfait",;
    ToolTipText = "Preventivi a forfait",;
    HelpContextID = 241239156,;
    cFormVar="w_CopiaPreventiviAForfait", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCopiaPreventiviAForfait_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCopiaPreventiviAForfait_1_4.GetRadio()
    this.Parent.oContained.w_CopiaPreventiviAForfait = this.RadioValue()
    return .t.
  endfunc

  func oCopiaPreventiviAForfait_1_4.SetRadio()
    this.Parent.oContained.w_CopiaPreventiviAForfait=trim(this.Parent.oContained.w_CopiaPreventiviAForfait)
    this.value = ;
      iif(this.Parent.oContained.w_CopiaPreventiviAForfait=='S',1,;
      0)
  endfunc

  func oCopiaPreventiviAForfait_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPSTR<>'P')
    endwith
  endfunc

  add object oCopiaAttivitaPrecedenti_1_5 as StdCheck with uid="AGDUUEWPHX",rtseq=3,rtrep=.f.,left=18, top=78, caption="Copia attivit� precedenti",;
    ToolTipText = "Attivit� precedenti",;
    HelpContextID = 135474241,;
    cFormVar="w_CopiaAttivitaPrecedenti", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCopiaAttivitaPrecedenti_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCopiaAttivitaPrecedenti_1_5.GetRadio()
    this.Parent.oContained.w_CopiaAttivitaPrecedenti = this.RadioValue()
    return .t.
  endfunc

  func oCopiaAttivitaPrecedenti_1_5.SetRadio()
    this.Parent.oContained.w_CopiaAttivitaPrecedenti=trim(this.Parent.oContained.w_CopiaAttivitaPrecedenti)
    this.value = ;
      iif(this.Parent.oContained.w_CopiaAttivitaPrecedenti=='S',1,;
      0)
  endfunc

  add object oStr_1_6 as StdString with uid="OYLYVSDDRH",Visible=.t., Left=18, Top=56,;
    Alignment=1, Width=157, Height=18,;
    Caption="Solo su capoprogetto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_sg3','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
