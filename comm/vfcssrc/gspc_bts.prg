* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bts                                                        *
*              Stato attivita                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_30]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-27                                                      *
* Last revis.: 2006-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Par
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bts",oParentObject,m.Par)
return(i_retval)

define class tgspc_bts as StdBatch
  * --- Local variables
  Par = space(1)
  w_MESS = space(200)
  w_SERIAL = space(10)
  w_REG = 0
  w_DATA = ctod("  /  /  ")
  w_VEAC = space(1)
  w_DATREG = ctod("  /  /  ")
  * --- WorkFile variables
  MAT_MAST_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSPC_BTS lanciato da GSPC_AAT agli eventi w_ATPERCOM CHANGED
    * --- e w_AT_STATO CHANGED
    * --- Stato precedente la variazione a mano
    if this.Par="P"
      * --- Variata la percentuale
      do case
        case this.oParentObject.w_AT_STATO $ "PC"
          ah_ErrorMsg("Operazione non consentita se l'attivit� non � pianificata","!","")
        case this.oParentObject.w_ATPERCOM>0 and this.oParentObject.w_ATPERCOM<100 and this.oParentObject.w_AT_STATO $ "ZLF"
          this.oParentObject.w_AT_STATO = "L"
          this.oParentObject.w_STAT1 = "L"
        case this.oParentObject.w_ATPERCOM=100 and this.oParentObject.w_AT_STATO $ "ZLF"
          this.oParentObject.w_AT_STATO = "F"
          this.oParentObject.w_STAT1 = "F"
        case this.oParentObject.w_ATPERCOM=0 and this.oParentObject.w_AT_STATO $ "ZLF"
          this.oParentObject.w_AT_STATO = "Z"
          this.oParentObject.w_STAT1 = "Z"
      endcase
    else
      * --- Variato lo stato
      do case
        case this.oParentObject.w_STAT1="P" and this.oParentObject.w_AT_STATO="C" or this.oParentObject.w_STAT1="C" and this.oParentObject.w_AT_STATO="P"
        case this.oParentObject.w_STAT1 $ "PC" and this.oParentObject.w_AT_STATO $ "ZLF"
          this.w_MESS = "Operazione non eseguibile manualmente%0per pianificare l'attivit� occorre eseguire l'apposita%0procedura di pianificazione attivit�"
          ah_ErrorMsg(this.w_MESS,"!","")
          this.oParentObject.w_AT_STATO = this.oParentObject.w_STAT1
        case this.oParentObject.w_STAT1 = "Z" and this.oParentObject.w_AT_STATO $ "PC"
          this.w_SERIAL = SPACE(10)
          * --- Read from MAT_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MASERIAL"+;
              " from "+i_cTable+" MAT_MAST where ";
                  +"MACODATT = "+cp_ToStrODBC(this.oParentObject.w_ATCODATT);
                  +" and MACODCOM = "+cp_ToStrODBC(this.oParentObject.w_ATCODCOM);
                  +" and MATIPMOV = "+cp_ToStrODBC("P");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MASERIAL;
              from (i_cTable) where;
                  MACODATT = this.oParentObject.w_ATCODATT;
                  and MACODCOM = this.oParentObject.w_ATCODCOM;
                  and MATIPMOV = "P";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERIAL = NVL(cp_ToDate(_read_.MASERIAL),cp_NullValue(_read_.MASERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_SERIAL)
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVNUMDOC,MVDATDOC,MVFLVEAC,MVDATREG"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVMOVCOM = "+cp_ToStrODBC(this.w_SERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVNUMDOC,MVDATDOC,MVFLVEAC,MVDATREG;
                from (i_cTable) where;
                    MVMOVCOM = this.w_SERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_REG = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
              this.w_DATA = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
              this.w_VEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
              this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows<>0
              if this.w_VEAC="V"
                this.w_MESS = "Occorre cancellare il documento generato dalla pianificazione%0tipo: documento interno - ciclo vendite%0documento numero %1 del %2"
                ah_ErrorMsg(this.w_MESS,"!","",Alltrim(Str(this.w_REG,15)),Dtoc(this.w_DATA))
              else
                this.w_MESS = "Occorre cancellare il documento generato dalla pianificazione%0tipo: documento interno - ciclo acquisti%0documento numero %1 del %2registrazione del %3"
                ah_ErrorMsg(this.w_MESS,"!","",Alltrim(Str(this.w_REG,15)),Dtoc(this.w_DATA),Dtoc(this.w_DATREG))
              endif
              this.oParentObject.w_AT_STATO = this.oParentObject.w_STAT1
            endif
          endif
        case this.oParentObject.w_STAT1 $ "LF" and this.oParentObject.w_AT_STATO $ "PC"
          this.w_MESS = "Operazione non consentita"
          ah_ErrorMsg(this.w_MESS,"!","")
          this.oParentObject.w_AT_STATO = this.oParentObject.w_STAT1
        case this.oParentObject.w_STAT1 $ "ZLF" and this.oParentObject.w_AT_STATO $ "ZLF"
          this.w_MESS = "Per modificare lo stato occorre agire sulla percentuale di completamento"
          ah_ErrorMsg(this.w_MESS,"!","")
          this.oParentObject.w_AT_STATO = this.oParentObject.w_STAT1
      endcase
    endif
  endproc


  proc Init(oParentObject,Par)
    this.Par=Par
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MAT_MAST'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Par"
endproc
