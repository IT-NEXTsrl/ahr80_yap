* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_mrd                                                        *
*              Inserimento dichiarazioni di MDO                                *
*                                                                              *
*      Author: Zucchetti Spa - CF                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_76]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-11                                                      *
* Last revis.: 2013-07-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_mrd"))

* --- Class definition
define class tgspc_mrd as StdTrsForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 627
  Height = 307+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-30"
  HelpContextID=113924201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DICH_MDO_IDX = 0
  CAN_TIER_IDX = 0
  DIPENDEN_IDX = 0
  ATTIVITA_IDX = 0
  cFile = "DICH_MDO"
  cKeySelect = "RDNUMRIL"
  cKeyWhere  = "RDNUMRIL=this.w_RDNUMRIL"
  cKeyDetail  = "RDNUMRIL=this.w_RDNUMRIL"
  cKeyWhereODBC = '"RDNUMRIL="+cp_ToStrODBC(this.w_RDNUMRIL)';

  cKeyDetailWhereODBC = '"RDNUMRIL="+cp_ToStrODBC(this.w_RDNUMRIL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DICH_MDO.RDNUMRIL="+cp_ToStrODBC(this.w_RDNUMRIL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DICH_MDO.CPROWORD '
  cPrg = "gspc_mrd"
  cComment = "Inserimento dichiarazioni di MDO"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RDNUMRIL = space(15)
  w_RDOPERIL = 0
  w_RDDATRIL = ctod('  /  /  ')
  o_RDDATRIL = ctod('  /  /  ')
  w_RDORARIL = space(8)
  w_RDDIPEND = space(5)
  w_RDCODCOM = space(15)
  w_RDAGGIMM = space(1)
  w_RDRILAUT = space(1)
  w_CPROWORD = 0
  w_RDCODESE = space(4)
  w_RDPROCES = space(1)
  w_RDTIPATT = space(1)
  w_RDCODATT = space(15)
  w_PIANIF = space(1)
  w_RDSECON = 0
  w_RDDATINI = ctod('  /  /  ')
  w_RDORAINI = space(8)
  w_RDDATFIN = ctod('  /  /  ')
  w_RDORAFIN = space(8)
  w_RDTEMPOR = 0
  w_CODUTE = 0
  w_DETCARI = space(10)
  w_DESCAN = space(30)
  w_DESDIPE = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_UTENTE = 0
  w_COMMOBS = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_RDNUMRIL = this.W_RDNUMRIL
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gspc_mrd
   Procedure ecpedit 
    NUMREC=this.search("nvl(t_RDPROCES,space(1))='S'")
    IF NUMREC>0
     ah_errormsg( "Impossibile modificare. Dichiarazione gi� processata.",48,0)
       else
     dodefault()
         endif
   endproc
   Procedure ecpdelete
     NUMREC=this.search("nvl(t_RDPROCES,space(1))='S'")
    IF NUMREC>0
     ah_errormsg( "Impossibile eliminare. Dichiarazione gi� processata.",48,0)
    else
     dodefault()
       endif
   endproc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DICH_MDO','gspc_mrd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_mrdPag1","gspc_mrd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dichiarazione MDO")
      .Pages(1).HelpContextID = 133802859
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRDNUMRIL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='ATTIVITA'
    this.cWorkTables[4]='DICH_MDO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DICH_MDO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DICH_MDO_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_RDNUMRIL = NVL(RDNUMRIL,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DICH_MDO where RDNUMRIL=KeySet.RDNUMRIL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2],this.bLoadRecFilter,this.DICH_MDO_IDX,"gspc_mrd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DICH_MDO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DICH_MDO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DICH_MDO '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RDNUMRIL',this.w_RDNUMRIL  )
      select * from (i_cTable) DICH_MDO where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PIANIF = space(1)
        .w_CODUTE = 0
        .w_DETCARI = 'N'
        .w_DESCAN = space(30)
        .w_DESDIPE = space(40)
        .w_OBTEST = i_datsys
        .w_UTENTE = 0
        .w_COMMOBS = ctod("  /  /  ")
        .w_RDNUMRIL = NVL(RDNUMRIL,space(15))
        .op_RDNUMRIL = .w_RDNUMRIL
        .w_RDOPERIL = NVL(RDOPERIL,0)
        .w_RDDATRIL = NVL(cp_ToDate(RDDATRIL),ctod("  /  /  "))
        .w_RDORARIL = NVL(RDORARIL,space(8))
        .w_RDDIPEND = NVL(RDDIPEND,space(5))
          .link_1_6('Load')
        .w_RDCODCOM = NVL(RDCODCOM,space(15))
          if link_1_7_joined
            this.w_RDCODCOM = NVL(CNCODCAN107,NVL(this.w_RDCODCOM,space(15)))
            this.w_DESCAN = NVL(CNDESCAN107,space(30))
            this.w_RDAGGIMM = NVL(CNFLAGIM107,space(1))
            this.w_COMMOBS = NVL(cp_ToDate(CNDTOBSO107),ctod("  /  /  "))
          else
          .link_1_7('Load')
          endif
        .w_RDAGGIMM = NVL(RDAGGIMM,space(1))
        .w_RDRILAUT = NVL(RDRILAUT,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DICH_MDO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_RDCODESE = NVL(RDCODESE,space(4))
          .w_RDPROCES = NVL(RDPROCES,space(1))
          .w_RDTIPATT = NVL(RDTIPATT,space(1))
          .w_RDCODATT = NVL(RDCODATT,space(15))
          .link_2_5('Load')
          .w_RDSECON = NVL(RDSECON,0)
          .w_RDDATINI = NVL(cp_ToDate(RDDATINI),ctod("  /  /  "))
          .w_RDORAINI = NVL(RDORAINI,space(8))
          .w_RDDATFIN = NVL(cp_ToDate(RDDATFIN),ctod("  /  /  "))
          .w_RDORAFIN = NVL(RDORAFIN,space(8))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
          .w_RDTEMPOR = NVL(RDTEMPOR,0)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_15.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RDNUMRIL=space(15)
      .w_RDOPERIL=0
      .w_RDDATRIL=ctod("  /  /  ")
      .w_RDORARIL=space(8)
      .w_RDDIPEND=space(5)
      .w_RDCODCOM=space(15)
      .w_RDAGGIMM=space(1)
      .w_RDRILAUT=space(1)
      .w_CPROWORD=10
      .w_RDCODESE=space(4)
      .w_RDPROCES=space(1)
      .w_RDTIPATT=space(1)
      .w_RDCODATT=space(15)
      .w_PIANIF=space(1)
      .w_RDSECON=0
      .w_RDDATINI=ctod("  /  /  ")
      .w_RDORAINI=space(8)
      .w_RDDATFIN=ctod("  /  /  ")
      .w_RDORAFIN=space(8)
      .w_RDTEMPOR=0
      .w_CODUTE=0
      .w_DETCARI=space(10)
      .w_DESCAN=space(30)
      .w_DESDIPE=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_UTENTE=0
      .w_COMMOBS=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_RDOPERIL = i_CODUTE
        .w_RDDATRIL = i_DATSYS
        .w_RDORARIL = time()
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_RDDIPEND))
         .link_1_6('Full')
        endif
        .w_RDCODCOM = g_CODCOM
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_RDCODCOM))
         .link_1_7('Full')
        endif
        .DoRTCalc(7,7,.f.)
        .w_RDRILAUT = 'M'
        .DoRTCalc(9,9,.f.)
        .w_RDCODESE = CALCESER(.w_RDDATRIL, g_CODESE)
        .DoRTCalc(11,11,.f.)
        .w_RDTIPATT = 'A'
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_RDCODATT))
         .link_2_5('Full')
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .DoRTCalc(14,21,.f.)
        .w_DETCARI = 'N'
        .DoRTCalc(23,24,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_15.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DICH_MDO')
    this.DoRTCalc(26,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRDNUMRIL_1_1.enabled = i_bVal
      .Page1.oPag.oRDDATRIL_1_4.enabled = i_bVal
      .Page1.oPag.oRDORARIL_1_5.enabled = i_bVal
      .Page1.oPag.oRDDIPEND_1_6.enabled = i_bVal
      .Page1.oPag.oRDCODCOM_1_7.enabled = i_bVal
      .Page1.oPag.oRDAGGIMM_1_9.enabled = i_bVal
      .Page1.oPag.oBtn_1_10.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_15.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRDNUMRIL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRDNUMRIL_1_1.enabled = .t.
        .Page1.oPag.oRDDATRIL_1_4.enabled = .t.
        .Page1.oPag.oRDCODCOM_1_7.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DICH_MDO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"DICCOM","i_CODAZI,w_RDNUMRIL")
      .op_CODAZI = .w_CODAZI
      .op_RDNUMRIL = .w_RDNUMRIL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDNUMRIL,"RDNUMRIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDOPERIL,"RDOPERIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDDATRIL,"RDDATRIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDORARIL,"RDORARIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDDIPEND,"RDDIPEND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDCODCOM,"RDCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDAGGIMM,"RDAGGIMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDRILAUT,"RDRILAUT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    i_lTable = "DICH_MDO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DICH_MDO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_RDCODATT C(15);
      ,t_RDDATINI D(8);
      ,t_RDORAINI C(8);
      ,t_RDDATFIN D(8);
      ,t_RDORAFIN C(8);
      ,t_RDTEMPOR N(12,3);
      ,CPROWNUM N(10);
      ,t_RDCODESE C(4);
      ,t_RDPROCES C(1);
      ,t_RDTIPATT C(1);
      ,t_RDSECON N(12,3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgspc_mrdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRDCODATT_2_5.controlsource=this.cTrsName+'.t_RDCODATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATINI_2_8.controlsource=this.cTrsName+'.t_RDDATINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRDORAINI_2_9.controlsource=this.cTrsName+'.t_RDORAINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATFIN_2_10.controlsource=this.cTrsName+'.t_RDDATFIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRDORAFIN_2_11.controlsource=this.cTrsName+'.t_RDORAFIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRDTEMPOR_2_13.controlsource=this.cTrsName+'.t_RDTEMPOR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(61)
    this.AddVLine(182)
    this.AddVLine(267)
    this.AddVLine(349)
    this.AddVLine(432)
    this.AddVLine(505)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDCODATT_2_5
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"DICCOM","i_CODAZI,w_RDNUMRIL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
      *
      * insert into DICH_MDO
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DICH_MDO')
        i_extval=cp_InsertValODBCExtFlds(this,'DICH_MDO')
        i_cFldBody=" "+;
                  "(RDNUMRIL,RDOPERIL,RDDATRIL,RDORARIL,RDDIPEND"+;
                  ",RDCODCOM,RDAGGIMM,RDRILAUT,CPROWORD,RDCODESE"+;
                  ",RDPROCES,RDTIPATT,RDCODATT,RDSECON,RDDATINI"+;
                  ",RDORAINI,RDDATFIN,RDORAFIN,RDTEMPOR,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RDNUMRIL)+","+cp_ToStrODBC(this.w_RDOPERIL)+","+cp_ToStrODBC(this.w_RDDATRIL)+","+cp_ToStrODBC(this.w_RDORARIL)+","+cp_ToStrODBCNull(this.w_RDDIPEND)+;
             ","+cp_ToStrODBCNull(this.w_RDCODCOM)+","+cp_ToStrODBC(this.w_RDAGGIMM)+","+cp_ToStrODBC(this.w_RDRILAUT)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_RDCODESE)+;
             ","+cp_ToStrODBC(this.w_RDPROCES)+","+cp_ToStrODBC(this.w_RDTIPATT)+","+cp_ToStrODBCNull(this.w_RDCODATT)+","+cp_ToStrODBC(this.w_RDSECON)+","+cp_ToStrODBC(this.w_RDDATINI)+;
             ","+cp_ToStrODBC(this.w_RDORAINI)+","+cp_ToStrODBC(this.w_RDDATFIN)+","+cp_ToStrODBC(this.w_RDORAFIN)+","+cp_ToStrODBC(this.w_RDTEMPOR)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DICH_MDO')
        i_extval=cp_InsertValVFPExtFlds(this,'DICH_MDO')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RDNUMRIL',this.w_RDNUMRIL)
        INSERT INTO (i_cTable) (;
                   RDNUMRIL;
                  ,RDOPERIL;
                  ,RDDATRIL;
                  ,RDORARIL;
                  ,RDDIPEND;
                  ,RDCODCOM;
                  ,RDAGGIMM;
                  ,RDRILAUT;
                  ,CPROWORD;
                  ,RDCODESE;
                  ,RDPROCES;
                  ,RDTIPATT;
                  ,RDCODATT;
                  ,RDSECON;
                  ,RDDATINI;
                  ,RDORAINI;
                  ,RDDATFIN;
                  ,RDORAFIN;
                  ,RDTEMPOR;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_RDNUMRIL;
                  ,this.w_RDOPERIL;
                  ,this.w_RDDATRIL;
                  ,this.w_RDORARIL;
                  ,this.w_RDDIPEND;
                  ,this.w_RDCODCOM;
                  ,this.w_RDAGGIMM;
                  ,this.w_RDRILAUT;
                  ,this.w_CPROWORD;
                  ,this.w_RDCODESE;
                  ,this.w_RDPROCES;
                  ,this.w_RDTIPATT;
                  ,this.w_RDCODATT;
                  ,this.w_RDSECON;
                  ,this.w_RDDATINI;
                  ,this.w_RDORAINI;
                  ,this.w_RDDATFIN;
                  ,this.w_RDORAFIN;
                  ,this.w_RDTEMPOR;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) and not(Empty(t_RDCODATT)) and not(Empty(t_RDTEMPOR))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DICH_MDO')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " RDOPERIL="+cp_ToStrODBC(this.w_RDOPERIL)+;
                 ",RDDATRIL="+cp_ToStrODBC(this.w_RDDATRIL)+;
                 ",RDORARIL="+cp_ToStrODBC(this.w_RDORARIL)+;
                 ",RDDIPEND="+cp_ToStrODBCNull(this.w_RDDIPEND)+;
                 ",RDCODCOM="+cp_ToStrODBCNull(this.w_RDCODCOM)+;
                 ",RDAGGIMM="+cp_ToStrODBC(this.w_RDAGGIMM)+;
                 ",RDRILAUT="+cp_ToStrODBC(this.w_RDRILAUT)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DICH_MDO')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  RDOPERIL=this.w_RDOPERIL;
                 ,RDDATRIL=this.w_RDDATRIL;
                 ,RDORARIL=this.w_RDORARIL;
                 ,RDDIPEND=this.w_RDDIPEND;
                 ,RDCODCOM=this.w_RDCODCOM;
                 ,RDAGGIMM=this.w_RDAGGIMM;
                 ,RDRILAUT=this.w_RDRILAUT;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_RDCODATT)) and not(Empty(t_RDTEMPOR))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DICH_MDO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DICH_MDO')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " RDOPERIL="+cp_ToStrODBC(this.w_RDOPERIL)+;
                     ",RDDATRIL="+cp_ToStrODBC(this.w_RDDATRIL)+;
                     ",RDORARIL="+cp_ToStrODBC(this.w_RDORARIL)+;
                     ",RDDIPEND="+cp_ToStrODBCNull(this.w_RDDIPEND)+;
                     ",RDCODCOM="+cp_ToStrODBCNull(this.w_RDCODCOM)+;
                     ",RDAGGIMM="+cp_ToStrODBC(this.w_RDAGGIMM)+;
                     ",RDRILAUT="+cp_ToStrODBC(this.w_RDRILAUT)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",RDCODESE="+cp_ToStrODBC(this.w_RDCODESE)+;
                     ",RDPROCES="+cp_ToStrODBC(this.w_RDPROCES)+;
                     ",RDTIPATT="+cp_ToStrODBC(this.w_RDTIPATT)+;
                     ",RDCODATT="+cp_ToStrODBCNull(this.w_RDCODATT)+;
                     ",RDSECON="+cp_ToStrODBC(this.w_RDSECON)+;
                     ",RDDATINI="+cp_ToStrODBC(this.w_RDDATINI)+;
                     ",RDORAINI="+cp_ToStrODBC(this.w_RDORAINI)+;
                     ",RDDATFIN="+cp_ToStrODBC(this.w_RDDATFIN)+;
                     ",RDORAFIN="+cp_ToStrODBC(this.w_RDORAFIN)+;
                     ",RDTEMPOR="+cp_ToStrODBC(this.w_RDTEMPOR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DICH_MDO')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      RDOPERIL=this.w_RDOPERIL;
                     ,RDDATRIL=this.w_RDDATRIL;
                     ,RDORARIL=this.w_RDORARIL;
                     ,RDDIPEND=this.w_RDDIPEND;
                     ,RDCODCOM=this.w_RDCODCOM;
                     ,RDAGGIMM=this.w_RDAGGIMM;
                     ,RDRILAUT=this.w_RDRILAUT;
                     ,CPROWORD=this.w_CPROWORD;
                     ,RDCODESE=this.w_RDCODESE;
                     ,RDPROCES=this.w_RDPROCES;
                     ,RDTIPATT=this.w_RDTIPATT;
                     ,RDCODATT=this.w_RDCODATT;
                     ,RDSECON=this.w_RDSECON;
                     ,RDDATINI=this.w_RDDATINI;
                     ,RDORAINI=this.w_RDORAINI;
                     ,RDDATFIN=this.w_RDDATFIN;
                     ,RDORAFIN=this.w_RDORAFIN;
                     ,RDTEMPOR=this.w_RDTEMPOR;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and not(Empty(t_RDCODATT)) and not(Empty(t_RDTEMPOR))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DICH_MDO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_RDCODATT)) and not(Empty(t_RDTEMPOR))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_RDDATRIL<>.w_RDDATRIL
          .w_RDCODESE = CALCESER(.w_RDDATRIL, g_CODESE)
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_15.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"DICCOM","i_CODAZI,w_RDNUMRIL")
          .op_RDNUMRIL = .w_RDNUMRIL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(11,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_RDCODESE with this.w_RDCODESE
      replace t_RDPROCES with this.w_RDPROCES
      replace t_RDTIPATT with this.w_RDTIPATT
      replace t_RDSECON with this.w_RDSECON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_15.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_15.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRDNUMRIL_1_1.enabled = this.oPgFrm.Page1.oPag.oRDNUMRIL_1_1.mCond()
    this.oPgFrm.Page1.oPag.oRDDATRIL_1_4.enabled = this.oPgFrm.Page1.oPag.oRDDATRIL_1_4.mCond()
    this.oPgFrm.Page1.oPag.oRDORARIL_1_5.enabled = this.oPgFrm.Page1.oPag.oRDORARIL_1_5.mCond()
    this.oPgFrm.Page1.oPag.oRDDIPEND_1_6.enabled = this.oPgFrm.Page1.oPag.oRDDIPEND_1_6.mCond()
    this.oPgFrm.Page1.oPag.oRDCODCOM_1_7.enabled = this.oPgFrm.Page1.oPag.oRDCODCOM_1_7.mCond()
    this.oPgFrm.Page1.oPag.oRDAGGIMM_1_9.enabled = this.oPgFrm.Page1.oPag.oRDAGGIMM_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RDDIPEND
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RDDIPEND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_RDDIPEND)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPCODUTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_RDDIPEND))
          select DPCODICE,DPCOGNOM,DPCODUTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RDDIPEND)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RDDIPEND) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oRDDIPEND_1_6'),i_cWhere,'GSAR_BDZ',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPCODUTE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RDDIPEND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPCODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_RDDIPEND);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_RDDIPEND)
            select DPCODICE,DPCOGNOM,DPCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RDDIPEND = NVL(_Link_.DPCODICE,space(5))
      this.w_DESDIPE = NVL(_Link_.DPCOGNOM,space(40))
      this.w_CODUTE = NVL(_Link_.DPCODUTE,0)
      this.w_UTENTE = NVL(_Link_.DPCODUTE,0)
    else
      if i_cCtrl<>'Load'
        this.w_RDDIPEND = space(5)
      endif
      this.w_DESDIPE = space(40)
      this.w_CODUTE = 0
      this.w_UTENTE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RDOPERIL=.w_UTENTE OR nvl(.w_UTENTE,0)=0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Utente non autorizzato")
        endif
        this.w_RDDIPEND = space(5)
        this.w_DESDIPE = space(40)
        this.w_CODUTE = 0
        this.w_UTENTE = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RDDIPEND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RDCODCOM
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RDCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_RDCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_RDCODCOM))
          select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RDCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RDCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oRDCODCOM_1_7'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RDCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_RDCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_RDCODCOM)
            select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RDCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
      this.w_RDAGGIMM = NVL(_Link_.CNFLAGIM,space(1))
      this.w_COMMOBS = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RDCODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
      this.w_RDAGGIMM = space(1)
      this.w_COMMOBS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !(empty (.w_COMMOBS) or .w_COMMOBS>i_datsys)
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione, la commessa selezionata � obsoleta")))
          if not(i_bRes)
            this.w_RDCODCOM = space(15)
            this.w_DESCAN = space(30)
            this.w_RDAGGIMM = space(1)
            this.w_COMMOBS = ctod("  /  /  ")
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RDCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.CNCODCAN as CNCODCAN107"+ ",link_1_7.CNDESCAN as CNDESCAN107"+ ",link_1_7.CNFLAGIM as CNFLAGIM107"+ ",link_1_7.CNDTOBSO as CNDTOBSO107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on DICH_MDO.RDCODCOM=link_1_7.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and DICH_MDO.RDCODCOM=link_1_7.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RDCODATT
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RDCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_RDCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_RDCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_RDTIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,AT_STATO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_RDCODCOM;
                     ,'ATTIPATT',this.w_RDTIPATT;
                     ,'ATCODATT',trim(this.w_RDCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,AT_STATO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RDCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RDCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oRDCODATT_2_5'),i_cWhere,'GSPC_BZZ',"Attivit� autorizzate",'gspc_zrd.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RDCODCOM<>oSource.xKey(1);
           .or. this.w_RDTIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,AT_STATO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,AT_STATO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Stato attivit� non conforme al tipo di movimento o non autorizzata")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,AT_STATO";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_RDCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_RDTIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,AT_STATO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RDCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,AT_STATO";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_RDCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_RDCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_RDTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_RDCODCOM;
                       ,'ATTIPATT',this.w_RDTIPATT;
                       ,'ATCODATT',this.w_RDCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,AT_STATO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RDCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_PIANIF = NVL(_Link_.AT_STATO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RDCODATT = space(15)
      endif
      this.w_PIANIF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=chkutatt(.w_RDCODCOM,.w_RDCODATT,.w_RDOPERIL) and (.w_PIANIF$'C|Z|L' Or .cFunction='Query')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Stato attivit� non conforme al tipo di movimento o non autorizzata")
        endif
        this.w_RDCODATT = space(15)
        this.w_PIANIF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RDCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oRDNUMRIL_1_1.value==this.w_RDNUMRIL)
      this.oPgFrm.Page1.oPag.oRDNUMRIL_1_1.value=this.w_RDNUMRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oRDOPERIL_1_3.value==this.w_RDOPERIL)
      this.oPgFrm.Page1.oPag.oRDOPERIL_1_3.value=this.w_RDOPERIL
    endif
    if not(this.oPgFrm.Page1.oPag.oRDDATRIL_1_4.value==this.w_RDDATRIL)
      this.oPgFrm.Page1.oPag.oRDDATRIL_1_4.value=this.w_RDDATRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oRDORARIL_1_5.value==this.w_RDORARIL)
      this.oPgFrm.Page1.oPag.oRDORARIL_1_5.value=this.w_RDORARIL
    endif
    if not(this.oPgFrm.Page1.oPag.oRDDIPEND_1_6.value==this.w_RDDIPEND)
      this.oPgFrm.Page1.oPag.oRDDIPEND_1_6.value=this.w_RDDIPEND
    endif
    if not(this.oPgFrm.Page1.oPag.oRDCODCOM_1_7.value==this.w_RDCODCOM)
      this.oPgFrm.Page1.oPag.oRDCODCOM_1_7.value=this.w_RDCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oRDAGGIMM_1_9.RadioValue()==this.w_RDAGGIMM)
      this.oPgFrm.Page1.oPag.oRDAGGIMM_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_18.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_18.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDIPE_1_19.value==this.w_DESDIPE)
      this.oPgFrm.Page1.oPag.oDESDIPE_1_19.value=this.w_DESDIPE
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDCODATT_2_5.value==this.w_RDCODATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDCODATT_2_5.value=this.w_RDCODATT
      replace t_RDCODATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDCODATT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATINI_2_8.value==this.w_RDDATINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATINI_2_8.value=this.w_RDDATINI
      replace t_RDDATINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATINI_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDORAINI_2_9.value==this.w_RDORAINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDORAINI_2_9.value=this.w_RDORAINI
      replace t_RDORAINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDORAINI_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATFIN_2_10.value==this.w_RDDATFIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATFIN_2_10.value=this.w_RDDATFIN
      replace t_RDDATFIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATFIN_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDORAFIN_2_11.value==this.w_RDORAFIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDORAFIN_2_11.value=this.w_RDORAFIN
      replace t_RDORAFIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDORAFIN_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDTEMPOR_2_13.value==this.w_RDTEMPOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDTEMPOR_2_13.value=this.w_RDTEMPOR
      replace t_RDTEMPOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDTEMPOR_2_13.value
    endif
    cp_SetControlsValueExtFlds(this,'DICH_MDO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RDNUMRIL))  and (.w_DETCARI<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oRDNUMRIL_1_1.SetFocus()
            i_bnoObbl = !empty(.w_RDNUMRIL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RDDATRIL))  and (.w_DETCARI<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oRDDATRIL_1_4.SetFocus()
            i_bnoObbl = !empty(.w_RDDATRIL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RDDIPEND) or not(.w_RDOPERIL=.w_UTENTE OR nvl(.w_UTENTE,0)=0))  and (.w_DETCARI<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oRDDIPEND_1_6.SetFocus()
            i_bnoObbl = !empty(.w_RDDIPEND)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Utente non autorizzato")
          case   (empty(.w_RDCODCOM))  and (.w_DETCARI<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oRDCODCOM_1_7.SetFocus()
            i_bnoObbl = !empty(.w_RDCODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) and not(Empty(t_RDCODATT)) and not(Empty(t_RDTEMPOR)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(chkutatt(.w_RDCODCOM,.w_RDCODATT,.w_RDOPERIL) and (.w_PIANIF$'C|Z|L' Or .cFunction='Query')) and not(empty(.w_RDCODATT)) and (not(Empty(.w_CPROWORD)) and not(Empty(.w_RDCODATT)) and not(Empty(.w_RDTEMPOR)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDCODATT_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Stato attivit� non conforme al tipo di movimento o non autorizzata")
        case   not(.w_RDDATINI<=.w_RDDATFIN or empty(.w_RDDATFIN)) and (not(Empty(.w_CPROWORD)) and not(Empty(.w_RDCODATT)) and not(Empty(.w_RDTEMPOR)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATINI_2_8
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_RDDATINI<=.w_RDDATFIN or empty(.w_RDDATINI)) and (not(Empty(.w_CPROWORD)) and not(Empty(.w_RDCODATT)) and not(Empty(.w_RDTEMPOR)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDDATFIN_2_10
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_CPROWORD)) and not(Empty(.w_RDCODATT)) and not(Empty(.w_RDTEMPOR))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RDDATRIL = this.w_RDDATRIL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and not(Empty(t_RDCODATT)) and not(Empty(t_RDTEMPOR)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_RDCODESE=space(4)
      .w_RDPROCES=space(1)
      .w_RDTIPATT=space(1)
      .w_RDCODATT=space(15)
      .w_RDSECON=0
      .w_RDDATINI=ctod("  /  /  ")
      .w_RDORAINI=space(8)
      .w_RDDATFIN=ctod("  /  /  ")
      .w_RDORAFIN=space(8)
      .w_RDTEMPOR=0
      .DoRTCalc(1,9,.f.)
        .w_RDCODESE = CALCESER(.w_RDDATRIL, g_CODESE)
      .DoRTCalc(11,11,.f.)
        .w_RDTIPATT = 'A'
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_RDCODATT))
        .link_2_5('Full')
      endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_15.Calculate()
    endwith
    this.DoRTCalc(14,27,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_RDCODESE = t_RDCODESE
    this.w_RDPROCES = t_RDPROCES
    this.w_RDTIPATT = t_RDTIPATT
    this.w_RDCODATT = t_RDCODATT
    this.w_RDSECON = t_RDSECON
    this.w_RDDATINI = t_RDDATINI
    this.w_RDORAINI = t_RDORAINI
    this.w_RDDATFIN = t_RDDATFIN
    this.w_RDORAFIN = t_RDORAFIN
    this.w_RDTEMPOR = t_RDTEMPOR
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_RDCODESE with this.w_RDCODESE
    replace t_RDPROCES with this.w_RDPROCES
    replace t_RDTIPATT with this.w_RDTIPATT
    replace t_RDCODATT with this.w_RDCODATT
    replace t_RDSECON with this.w_RDSECON
    replace t_RDDATINI with this.w_RDDATINI
    replace t_RDORAINI with this.w_RDORAINI
    replace t_RDDATFIN with this.w_RDDATFIN
    replace t_RDORAFIN with this.w_RDORAFIN
    replace t_RDTEMPOR with this.w_RDTEMPOR
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgspc_mrdPag1 as StdContainer
  Width  = 623
  height = 307
  stdWidth  = 623
  stdheight = 307
  resizeXpos=168
  resizeYpos=209
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRDNUMRIL_1_1 as StdField with uid="TXGPNOAVKF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RDNUMRIL", cQueryName = "RDNUMRIL",nZero=15,;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Numero dichiarazione",;
    HelpContextID = 262155934,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=82, Top=7, cSayPict="'999999999999999'", cGetPict="'999999999999999'", InputMask=replicate('X',15)

  func oRDNUMRIL_1_1.mCond()
    with this.Parent.oContained
      return (.w_DETCARI<>'S')
    endwith
  endfunc

  add object oRDOPERIL_1_3 as StdField with uid="RUBZDDFHOZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RDOPERIL", cQueryName = "RDOPERIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operatore che ha registrato la dichiarazione",;
    HelpContextID = 2432670,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=260, Top=7, cSayPict='"9999"', cGetPict='"9999"'

  add object oRDDATRIL_1_4 as StdField with uid="HDIDQRIVXF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RDDATRIL", cQueryName = "RDDATRIL",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dichiarazione produzione",;
    HelpContextID = 256167582,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=372, Top=7

  func oRDDATRIL_1_4.mCond()
    with this.Parent.oContained
      return (.w_DETCARI<>'S')
    endwith
  endfunc

  add object oRDORARIL_1_5 as StdField with uid="BUOYLXRONB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RDORARIL", cQueryName = "RDORARIL",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Ora rilevazione",;
    HelpContextID = 6495902,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=527, Top=7, cSayPict='"99:99:99"', cGetPict='"99:99:99"', InputMask=replicate('X',8)

  func oRDORARIL_1_5.mCond()
    with this.Parent.oContained
      return (.w_DETCARI<>'S')
    endwith
  endfunc

  add object oRDDIPEND_1_6 as StdField with uid="EIWPQGINSN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RDDIPEND", cQueryName = "RDDIPEND",nZero=5,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Utente non autorizzato",;
    ToolTipText = "Dipendente",;
    HelpContextID = 209505958,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=82, Top=33, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_RDDIPEND"

  func oRDDIPEND_1_6.mCond()
    with this.Parent.oContained
      return (.w_DETCARI<>'S')
    endwith
  endfunc

  func oRDDIPEND_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oRDDIPEND_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRDDIPEND_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oRDDIPEND_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oRDDIPEND_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_RDDIPEND
    i_obj.ecpSave()
  endproc

  add object oRDCODCOM_1_7 as StdField with uid="WAHECSGOYL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_RDCODCOM", cQueryName = "RDCODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa",;
    HelpContextID = 255254173,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=82, Top=59, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_RDCODCOM"

  func oRDCODCOM_1_7.mCond()
    with this.Parent.oContained
      if (.w_DETCARI<>'S')
        if .nLastRow>1
          return (.f.)
        endif
      else
        return (.f.)
      endif
    endwith
  endfunc

  func oRDCODCOM_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
      if .not. empty(.w_RDCODATT)
        bRes2=.link_2_5('Full')
      endif
      if bRes and !(empty (.w_COMMOBS) or .w_COMMOBS>i_datsys)
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione, la commessa selezionata � obsoleta")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oRDCODCOM_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRDCODCOM_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oRDCODCOM_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oRDCODCOM_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_RDCODCOM
    i_obj.ecpSave()
  endproc

  add object oRDAGGIMM_1_9 as StdCheck with uid="ZPQVNLKLAV",rtseq=7,rtrep=.f.,left=434, top=33, caption="Agg. immediato",;
    ToolTipText = "Genera immediatamente il movimento di commessa associato",;
    HelpContextID = 151977629,;
    cFormVar="w_RDAGGIMM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRDAGGIMM_1_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RDAGGIMM,&i_cF..t_RDAGGIMM),this.value)
    return(iif(xVal =1,'A',;
    'N'))
  endfunc
  func oRDAGGIMM_1_9.GetRadio()
    this.Parent.oContained.w_RDAGGIMM = this.RadioValue()
    return .t.
  endfunc

  func oRDAGGIMM_1_9.ToRadio()
    this.Parent.oContained.w_RDAGGIMM=trim(this.Parent.oContained.w_RDAGGIMM)
    return(;
      iif(this.Parent.oContained.w_RDAGGIMM=='A',1,;
      0))
  endfunc

  func oRDAGGIMM_1_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oRDAGGIMM_1_9.mCond()
    with this.Parent.oContained
      return (.w_DETCARI<>'S' and upper(.cFunction)<>'EDIT')
    endwith
  endfunc


  add object oBtn_1_10 as StdButton with uid="QZGHLPNBVJ",left=572, top=42, width=48,height=45,;
    CpPicture="BMP\Carica.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire il caricamento rapido del dettaglio";
    , HelpContextID = 108379898;
    , TabStop=.f.,Caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSPC_BRD(this.Parent.oContained,"CARICA_DET")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    with this.Parent.oContained
      return (.cfunction='Load' and not empty(.w_RDCODCOM) and not empty(.w_RDDIPEND))
    endwith
  endfunc

  add object oDESCAN_1_18 as StdField with uid="INGJQAPKCE",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione commessa",;
    HelpContextID = 193863990,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=205, Top=59, InputMask=replicate('X',30)

  add object oDESDIPE_1_19 as StdField with uid="QNLQXDJOVE",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESDIPE", cQueryName = "DESDIPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione dipendente",;
    HelpContextID = 235872566,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=146, Top=33, InputMask=replicate('X',40)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=92, width=616,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Riga",Field2="RDCODATT",Label2="Attivit�",Field3="RDDATINI",Label3="Data inizio",Field4="RDORAINI",Label4="Ora inizio ",Field5="RDDATFIN",Label5="Data fine",Field6="RDORAFIN",Label6="Ora fine",Field7="RDTEMPOR",Label7="Tempo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202166650

  add object oStr_1_2 as StdString with uid="OIJHGDNDGG",Visible=.t., Left=25, Top=7,;
    Alignment=1, Width=54, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="POZVLDQQIQ",Visible=.t., Left=205, Top=7,;
    Alignment=1, Width=52, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ONCHASAVRS",Visible=.t., Left=0, Top=59,;
    Alignment=1, Width=79, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="ZTKMRNVFKS",Visible=.t., Left=307, Top=7,;
    Alignment=1, Width=63, Height=18,;
    Caption="Data ril.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="UWPXSNUIPQ",Visible=.t., Left=452, Top=7,;
    Alignment=1, Width=73, Height=18,;
    Caption="Ora ril.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CESWDOZFWZ",Visible=.t., Left=12, Top=33,;
    Alignment=1, Width=67, Height=18,;
    Caption="Dipendente:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=113,;
    width=612+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=114,width=611+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ATTIVITA|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ATTIVITA'
        oDropInto=this.oBodyCol.oRow.oRDCODATT_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgspc_mrdBodyRow as CPBodyRowCnt
  Width=602
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="VIMXOOKEZP",rtseq=9,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,enabled=.f.,;
    HelpContextID = 234495082,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oRDCODATT_2_5 as StdTrsField with uid="VXBGRWWCTN",rtseq=13,rtrep=.t.,;
    cFormVar="w_RDCODATT",value=space(15),;
    HelpContextID = 248062314,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Stato attivit� non conforme al tipo di movimento o non autorizzata",;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=56, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_RDCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_RDTIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_RDCODATT"

  func oRDCODATT_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oRDCODATT_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRDCODATT_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_RDCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_RDTIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_RDCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_RDTIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oRDCODATT_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit� autorizzate",'gspc_zrd.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oRDCODATT_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_RDCODCOM
    i_obj.ATTIPATT=w_RDTIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_RDCODATT
    i_obj.ecpSave()
  endproc

  add object oRDDATINI_2_8 as StdTrsField with uid="EHPZXIUFNP",rtseq=16,rtrep=.t.,;
    cFormVar="w_RDDATINI",value=ctod("  /  /  "),;
    HelpContextID = 138727073,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=177, Top=0

  proc oRDDATINI_2_8.mDefault
    with this.Parent.oContained
      if empty(.w_RDDATINI)
        .w_RDDATINI = i_DATSYS
      endif
    endwith
  endproc

  func oRDDATINI_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RDDATINI<=.w_RDDATFIN or empty(.w_RDDATFIN))
    endwith
    return bRes
  endfunc

  add object oRDORAINI_2_9 as StdTrsField with uid="ICSVVAVQNK",rtseq=17,rtrep=.t.,;
    cFormVar="w_RDORAINI",value=space(8),;
    HelpContextID = 157490849,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=267, Top=0, cSayPict=["99:99:99"], cGetPict=["99:99:99"], InputMask=replicate('X',8)

  proc oRDORAINI_2_9.mDefault
    with this.Parent.oContained
      if empty(.w_RDORAINI)
        .w_RDORAINI = time()
      endif
    endwith
  endproc

  add object oRDDATFIN_2_10 as StdTrsField with uid="RLFVIAKYZD",rtseq=18,rtrep=.t.,;
    cFormVar="w_RDDATFIN",value=ctod("  /  /  "),;
    HelpContextID = 189058716,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=348, Top=0

  proc oRDDATFIN_2_10.mDefault
    with this.Parent.oContained
      if empty(.w_RDDATFIN)
        .w_RDDATFIN = i_DATSYS
      endif
    endwith
  endproc

  func oRDDATFIN_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RDDATINI<=.w_RDDATFIN or empty(.w_RDDATINI))
    endwith
    return bRes
  endfunc

  add object oRDORAFIN_2_11 as StdTrsField with uid="FBAKFUDVQA",rtseq=19,rtrep=.t.,;
    cFormVar="w_RDORAFIN",value=space(8),;
    HelpContextID = 207822492,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=427, Top=0, cSayPict=["99:99:99"], cGetPict=["99:99:99"], InputMask=replicate('X',8)

  proc oRDORAFIN_2_11.mDefault
    with this.Parent.oContained
      if empty(.w_RDORAFIN)
        .w_RDORAFIN = time()
      endif
    endwith
  endproc

  add object oObj_2_12 as cp_runprogram with uid="LMJJKKNUYH",width=179,height=22,;
   left=164, top=207,;
    caption='GSPC_BRD',;
   bGlobalFont=.t.,;
    prg="GSPC_BRD('DELTAT')",;
    cEvent = "w_RDDATFIN Changed,w_RDDATINI Changed,w_RDORAFIN Changed,w_RDORAINI Changed",;
    nPag=2;
    , HelpContextID = 23986090

  add object oRDTEMPOR_2_13 as StdTrsField with uid="IICBEOIFKW",rtseq=20,rtrep=.t.,;
    cFormVar="w_RDTEMPOR",value=0,;
    HelpContextID = 28298904,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=500, Top=0, cSayPict=["@Z 999999999.9999"], cGetPict=["999999999.9999"]

  add object oObj_2_14 as cp_runprogram with uid="BVMADIOMUT",width=179,height=22,;
   left=164, top=232,;
    caption='GSPC_BRD',;
   bGlobalFont=.t.,;
    prg="GSPC_BRD('TEMPOCH')",;
    cEvent = "w_RDTEMPOR Changed",;
    nPag=2;
    , HelpContextID = 23986090

  add object oObj_2_15 as cp_runprogram with uid="MLSGAXYQJN",width=179,height=22,;
   left=349, top=207,;
    caption='GSPC_BRD',;
   bGlobalFont=.t.,;
    prg="GSPC_BRD('IMM_MOV1')",;
    cEvent = "Record Inserted",;
    nPag=2;
    , HelpContextID = 23986090
  add object oLast as LastKeyMover
  * ---
  func oRDCODATT_2_5.When()
    return(.t.)
  proc oRDCODATT_2_5.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oRDCODATT_2_5.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_mrd','DICH_MDO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RDNUMRIL=DICH_MDO.RDNUMRIL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
