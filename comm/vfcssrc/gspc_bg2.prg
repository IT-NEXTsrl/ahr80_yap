* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bg2                                                        *
*              Eliminazione progetti                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_35]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-04                                                      *
* Last revis.: 2006-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bg2",oParentObject,m.pParam)
return(i_retval)

define class tgspc_bg2 as StdBatch
  * --- Local variables
  pParam = space(20)
  * --- WorkFile variables
  ATTIVITA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancellazione progetti
    do case
      case this.pParam="Elimina"
        * --- Try
        local bErr_03418448
        bErr_03418448=bTrsErr
        this.Try_03418448()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg="Non � possibile cancellare i capi-progetto"
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_03418448
        * --- End
      case this.pParam="CheckAtt"
        * --- Select from ATTIVITA
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" ATTIVITA ";
              +" where ATCODCOM= "+cp_ToStrODBC(this.oParentObject.w_CNCODCAN)+" And AT_STATO<>'F' And ATTIPATT='A'";
               ,"_Curs_ATTIVITA")
        else
          select Count(*) As Conta from (i_cTable);
           where ATCODCOM= this.oParentObject.w_CNCODCAN And AT_STATO<>"F" And ATTIPATT="A";
            into cursor _Curs_ATTIVITA
        endif
        if used('_Curs_ATTIVITA')
          select _Curs_ATTIVITA
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_RESCHK = Nvl( _Curs_ATTIVITA.CONTA , 0 )
            select _Curs_ATTIVITA
            continue
          enddo
          use
        endif
        if this.oParentObject.w_RESCHK<>0
          ah_ErrorMsg("Impossibile settare lo stato della commessa a <da storicizzare>, non ha tutte le attivit� in stato <completata>","i","")
        endif
    endcase
  endproc
  proc Try_03418448()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Elimina Capi-Progetto
    * --- Delete from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CNCODCAN);
            +" and ATTIPCOM = "+cp_ToStrODBC( "P");
             )
    else
      delete from (i_cTable) where;
            ATCODCOM = this.oParentObject.w_CNCODCAN;
            and ATTIPCOM =  "P";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ATTIVITA'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
