* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bca                                                        *
*              Creazione azienda                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_56]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-13                                                      *
* Last revis.: 2006-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bca",oParentObject)
return(i_retval)

define class tgspc_bca as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_C1 = 0
  w_C2 = 0
  w_C3 = 0
  w_C4 = 0
  w_C5 = 0
  w_C6 = 0
  w_C7 = 0
  w_C8 = ctod("  /  /  ")
  w_C9 = ctod("  /  /  ")
  w_C10 = space(4)
  w_C11 = space(30)
  w_C12 = space(30)
  w_C13 = space(30)
  w_C14 = space(30)
  w_C15 = space(30)
  * --- WorkFile variables
  ATTIVITA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato sotto transazione da GSUT_BCA (Creazione Azienda)
    this.w_CODAZI = this.oparentObject.oParentObject.w_CODAZI
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"PC_FAMIG"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"ATTIVITA"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"ATT_PREC"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"STRUTTUR"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"CPAR_DEF"),.F.)
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"ATTIVITA SET AT_STATO='P'")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ATTIVITA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
