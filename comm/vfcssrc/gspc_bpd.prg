* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bpd                                                        *
*              Parametri di default                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_100]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-27                                                      *
* Last revis.: 2014-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bpd",oParentObject,m.pTipo)
return(i_retval)

define class tgspc_bpd as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_CHIAVE = space(10)
  w_CHIAVE2 = space(10)
  w_MESS = space(254)
  w_OLDAREA = space(10)
  w_FOUND = .f.
  * --- WorkFile variables
  CAN_TIER_idx=0
  CPAR_DEF_idx=0
  MAGAZZIN_idx=0
  UNIMIS_idx=0
  CENCOST_idx=0
  CAM_AGAZ_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Imposta i parametri di default (da GSPC_SPD)
    * --- La procedura se non trova il record 'TAM' lo crea vuoto
    * --- U.M. per calcolo avanzamenti di commessa
    * --- Variabili aggiunte per interfaccia con MS-Project:
    * --- Connessione di default al database di MS-Project
    * --- Variabili locali
    this.w_CHIAVE = "TAM"
    do case
      case this.pTipo="Init"
        * --- All'avvio della maschera
        this.w_CHIAVE2 = ""
        * --- Read from CPAR_DEF
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PDCHIAVE,PDCODCOM,PDCODMAG,PDMSPROJ,PDPROGR,PDUMCOMM,PDPRJREL,PDPRJUSR,PDPRJPWD,PDODBCPJ,PDCENCOS,PDLOGTEC,PDPRJMOD,PDRICART,PDCAUMAG"+;
            " from "+i_cTable+" CPAR_DEF where ";
                +"PDCHIAVE = "+cp_ToStrODBC(this.w_CHIAVE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PDCHIAVE,PDCODCOM,PDCODMAG,PDMSPROJ,PDPROGR,PDUMCOMM,PDPRJREL,PDPRJUSR,PDPRJPWD,PDODBCPJ,PDCENCOS,PDLOGTEC,PDPRJMOD,PDRICART,PDCAUMAG;
            from (i_cTable) where;
                PDCHIAVE = this.w_CHIAVE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CHIAVE2 = NVL(cp_ToDate(_read_.PDCHIAVE),cp_NullValue(_read_.PDCHIAVE))
          this.oParentObject.w_CODCOM = NVL(cp_ToDate(_read_.PDCODCOM),cp_NullValue(_read_.PDCODCOM))
          this.oParentObject.w_CODMAG = NVL(cp_ToDate(_read_.PDCODMAG),cp_NullValue(_read_.PDCODMAG))
          this.oParentObject.w_MSPROJ = NVL(cp_ToDate(_read_.PDMSPROJ),cp_NullValue(_read_.PDMSPROJ))
          this.oParentObject.w_PROGR = NVL(cp_ToDate(_read_.PDPROGR),cp_NullValue(_read_.PDPROGR))
          this.oParentObject.w_UMCOMM = NVL(cp_ToDate(_read_.PDUMCOMM),cp_NullValue(_read_.PDUMCOMM))
          this.oParentObject.w_PRJREL = NVL(cp_ToDate(_read_.PDPRJREL),cp_NullValue(_read_.PDPRJREL))
          this.oParentObject.w_PRJUSR = NVL(cp_ToDate(_read_.PDPRJUSR),cp_NullValue(_read_.PDPRJUSR))
          this.oParentObject.w_PRJPWD = NVL(cp_ToDate(_read_.PDPRJPWD),cp_NullValue(_read_.PDPRJPWD))
          this.oParentObject.w_PRJODBC = NVL(cp_ToDate(_read_.PDODBCPJ),cp_NullValue(_read_.PDODBCPJ))
          this.oParentObject.w_CENCOS = NVL(cp_ToDate(_read_.PDCENCOS),cp_NullValue(_read_.PDCENCOS))
          this.oParentObject.w_LOGTEC = NVL(cp_ToDate(_read_.PDLOGTEC),cp_NullValue(_read_.PDLOGTEC))
          this.oParentObject.w_PRJMOD = NVL(cp_ToDate(_read_.PDPRJMOD),cp_NullValue(_read_.PDPRJMOD))
          this.oParentObject.w_RICART = NVL(cp_ToDate(_read_.PDRICART),cp_NullValue(_read_.PDRICART))
          this.oParentObject.w_CAUMAG = NVL(cp_ToDate(_read_.PDCAUMAG),cp_NullValue(_read_.PDCAUMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo i dati dal record TAM
        if Empty( this.w_CHIAVE2 )
          * --- Inserisco il record all'interno dell'archivio
          * --- Insert into CPAR_DEF
          i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CPAR_DEF_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PDCHIAVE"+",PDCODMAG"+",PDPROGR"+",PDMSPROJ"+",PDUMCOMM"+",PDPRJREL"+",PDPRJUSR"+",PDPRJPWD"+",PDODBCPJ"+",PDCENCOS"+",PDLOGTEC"+",PDPRJMOD"+",PDRICART"+",PDCAUMAG"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CHIAVE),'CPAR_DEF','PDCHIAVE');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CPAR_DEF','PDCODMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROGR),'CPAR_DEF','PDPROGR');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MSPROJ),'CPAR_DEF','PDMSPROJ');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UMCOMM),'CPAR_DEF','PDUMCOMM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJREL),'CPAR_DEF','PDPRJREL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJUSR),'CPAR_DEF','PDPRJUSR');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJPWD),'CPAR_DEF','PDPRJPWD');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJODBC),'CPAR_DEF','PDODBCPJ');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CENCOS),'CPAR_DEF','PDCENCOS');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LOGTEC),'CPAR_DEF','PDLOGTEC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJMOD),'CPAR_DEF','PDPRJMOD');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RICART),'CPAR_DEF','PDRICART');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAUMAG),'CPAR_DEF','PDCAUMAG');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PDCHIAVE',this.w_CHIAVE,'PDCODMAG',this.oParentObject.w_CODMAG,'PDPROGR',this.oParentObject.w_PROGR,'PDMSPROJ',this.oParentObject.w_MSPROJ,'PDUMCOMM',this.oParentObject.w_UMCOMM,'PDPRJREL',this.oParentObject.w_PRJREL,'PDPRJUSR',this.oParentObject.w_PRJUSR,'PDPRJPWD',this.oParentObject.w_PRJPWD,'PDODBCPJ',this.oParentObject.w_PRJODBC,'PDCENCOS',this.oParentObject.w_CENCOS,'PDLOGTEC',this.oParentObject.w_LOGTEC,'PDPRJMOD',this.oParentObject.w_PRJMOD)
            insert into (i_cTable) (PDCHIAVE,PDCODMAG,PDPROGR,PDMSPROJ,PDUMCOMM,PDPRJREL,PDPRJUSR,PDPRJPWD,PDODBCPJ,PDCENCOS,PDLOGTEC,PDPRJMOD,PDRICART,PDCAUMAG &i_ccchkf. );
               values (;
                 this.w_CHIAVE;
                 ,this.oParentObject.w_CODMAG;
                 ,this.oParentObject.w_PROGR;
                 ,this.oParentObject.w_MSPROJ;
                 ,this.oParentObject.w_UMCOMM;
                 ,this.oParentObject.w_PRJREL;
                 ,this.oParentObject.w_PRJUSR;
                 ,this.oParentObject.w_PRJPWD;
                 ,this.oParentObject.w_PRJODBC;
                 ,this.oParentObject.w_CENCOS;
                 ,this.oParentObject.w_LOGTEC;
                 ,this.oParentObject.w_PRJMOD;
                 ,this.oParentObject.w_RICART;
                 ,this.oParentObject.w_CAUMAG;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Leggo le descrizioni
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNDESCAN"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNDESCAN;
              from (i_cTable) where;
                  CNCODCAN = this.oParentObject.w_CODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCOM = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDESMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDESMAG;
              from (i_cTable) where;
                  MGCODMAG = this.oParentObject.w_CODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESMAG = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMDESCRI"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMDESCRI;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_CAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAMAG = NVL(cp_ToDate(_read_.CMDESCRI),cp_NullValue(_read_.CMDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from UNIMIS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.UNIMIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "UMDESCRI"+;
              " from "+i_cTable+" UNIMIS where ";
                  +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UMCOMM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              UMDESCRI;
              from (i_cTable) where;
                  UMCODICE = this.oParentObject.w_UMCOMM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESUM = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CENCOST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CENCOST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCDESPIA,CCNUMLIV"+;
              " from "+i_cTable+" CENCOST where ";
                  +"CC_CONTO = "+cp_ToStrODBC(this.oParentObject.w_CENCOS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCDESPIA,CCNUMLIV;
              from (i_cTable) where;
                  CC_CONTO = this.oParentObject.w_CENCOS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCEN = NVL(cp_ToDate(_read_.CCDESPIA),cp_NullValue(_read_.CCDESPIA))
            this.oParentObject.w_NUMLIV = NVL(cp_ToDate(_read_.CCNUMLIV),cp_NullValue(_read_.CCNUMLIV))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_SVCPROJ = this.oParentObject.w_MSPROJ
        endif
      case this.pTipo="Button"
        * --- Al Click del bottone OK sulla maschera
        * --- Write into CPAR_DEF
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CPAR_DEF_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PDCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM),'CPAR_DEF','PDCODCOM');
          +",PDCODMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CPAR_DEF','PDCODMAG');
          +",PDPROGR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROGR),'CPAR_DEF','PDPROGR');
          +",PDMSPROJ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MSPROJ),'CPAR_DEF','PDMSPROJ');
          +",PDUMCOMM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UMCOMM),'CPAR_DEF','PDUMCOMM');
          +",PDPRJREL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJREL),'CPAR_DEF','PDPRJREL');
          +",PDPRJUSR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJUSR),'CPAR_DEF','PDPRJUSR');
          +",PDPRJPWD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJPWD),'CPAR_DEF','PDPRJPWD');
          +",PDODBCPJ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJODBC),'CPAR_DEF','PDODBCPJ');
          +",PDCENCOS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CENCOS),'CPAR_DEF','PDCENCOS');
          +",PDLOGTEC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LOGTEC),'CPAR_DEF','PDLOGTEC');
          +",PDPRJMOD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRJMOD),'CPAR_DEF','PDPRJMOD');
          +",PDRICART ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RICART),'CPAR_DEF','PDRICART');
          +",PDCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAUMAG),'CPAR_DEF','PDCAUMAG');
              +i_ccchkf ;
          +" where ";
              +"PDCHIAVE = "+cp_ToStrODBC(this.w_CHIAVE);
                 )
        else
          update (i_cTable) set;
              PDCODCOM = this.oParentObject.w_CODCOM;
              ,PDCODMAG = this.oParentObject.w_CODMAG;
              ,PDPROGR = this.oParentObject.w_PROGR;
              ,PDMSPROJ = this.oParentObject.w_MSPROJ;
              ,PDUMCOMM = this.oParentObject.w_UMCOMM;
              ,PDPRJREL = this.oParentObject.w_PRJREL;
              ,PDPRJUSR = this.oParentObject.w_PRJUSR;
              ,PDPRJPWD = this.oParentObject.w_PRJPWD;
              ,PDODBCPJ = this.oParentObject.w_PRJODBC;
              ,PDCENCOS = this.oParentObject.w_CENCOS;
              ,PDLOGTEC = this.oParentObject.w_LOGTEC;
              ,PDPRJMOD = this.oParentObject.w_PRJMOD;
              ,PDRICART = this.oParentObject.w_RICART;
              ,PDCAUMAG = this.oParentObject.w_CAUMAG;
              &i_ccchkf. ;
           where;
              PDCHIAVE = this.w_CHIAVE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        ah_Msg("Parametri di default assegnati",.T.)
        * --- Abilita o disabilita la voce di menu PIANIFICAZIONE CON MS-PROJECT in base al valore di w_MSPROJ
        this.w_FOUND = .F.
        if this.oParentObject.w_MSPROJ<>this.oParentObject.w_SVCPROJ
          * --- Se il valore della variabile � cambiato rispetto al valore iniziale
          this.w_FOUND = .T.
        endif
        if this.w_FOUND
          * --- Se uno degli eventi di cui sopra ha modificato il menu
          this.w_MESS = "Aggiornamento completato! %0uscire e rientrare nella procedura%0per rendere effettivi in cambiamenti apportati"
          ah_ErrorMsg(this.w_MESS,"!","")
        endif
        this.oParentObject.EcpQuit()
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CPAR_DEF'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='CENCOST'
    this.cWorkTables[6]='CAM_AGAZ'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
