* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_aat                                                        *
*              Attivit�                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_171]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-27                                                      *
* Last revis.: 2012-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_aat"))

* --- Class definition
define class tgspc_aat as StdForm
  Top    = 22
  Left   = 46

  * --- Standard Properties
  Width  = 717
  Height = 306+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-23"
  HelpContextID=143284329
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  ATTIVITA_IDX = 0
  CAN_TIER_IDX = 0
  CENCOST_IDX = 0
  MA_COSTI_IDX = 0
  ATT_PREC_IDX = 0
  PC_FAMIG_IDX = 0
  CPAR_DEF_IDX = 0
  VALUTE_IDX = 0
  cFile = "ATTIVITA"
  cKeySelect = "ATCODCOM,ATTIPATT,ATCODATT"
  cQueryFilter="ATTIPATT='A'"
  cKeyWhere  = "ATCODCOM=this.w_ATCODCOM and ATTIPATT=this.w_ATTIPATT and ATCODATT=this.w_ATCODATT"
  cKeyWhereODBC = '"ATCODCOM="+cp_ToStrODBC(this.w_ATCODCOM)';
      +'+" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT)';
      +'+" and ATCODATT="+cp_ToStrODBC(this.w_ATCODATT)';

  cKeyWhereODBCqualified = '"ATTIVITA.ATCODCOM="+cp_ToStrODBC(this.w_ATCODCOM)';
      +'+" and ATTIVITA.ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT)';
      +'+" and ATTIVITA.ATCODATT="+cp_ToStrODBC(this.w_ATCODATT)';

  cPrg = "gspc_aat"
  cComment = "Attivit�"
  icon = "anag.ico"
  cAutoZoom = 'GSPC_AAZ'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_READPAR = space(10)
  w_DEFCOM = space(15)
  w_ATCODCOM = space(15)
  o_ATCODCOM = space(15)
  w_ATTIPATT = space(1)
  w_ATCODATT = space(15)
  w_ATDESCRI = space(30)
  w_ATCENCOS = space(15)
  w_ATCODFAM = space(5)
  w_ATTIPCOM = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DESCOMG = space(30)
  w_TIPOSTRU = space(1)
  w_DESCOM = space(30)
  w_DESPIA = space(40)
  w_ATFLPREV = space(1)
  w_AT_STATO = space(1)
  o_AT_STATO = space(1)
  w_DESFAM = space(50)
  w_AT_PRJVW = space(1)
  w_STAT1 = space(1)
  w_READPAR = space(1)
  w_MSPROJ = space(1)
  w_PROGR = space(1)
  w_ATDURGIO = 0
  o_ATDURGIO = 0
  w_ATVINCOL = space(3)
  w_ATDATVIN = ctod('  /  /  ')
  o_ATDATVIN = ctod('  /  /  ')
  w_ATDATINI = ctod('  /  /  ')
  o_ATDATINI = ctod('  /  /  ')
  w_ATDATFIN = ctod('  /  /  ')
  o_ATDATFIN = ctod('  /  /  ')
  w_ATPERCOM = 0
  w_ATCARDIN = space(1)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_ATDESSUP = space(0)
  w_TIPO = space(10)
  w_CNCODVAL = space(3)
  w_DECIMALI = 0

  * --- Children pointers
  GSPC_MCO = .NULL.
  GSPC_MAP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ATTIVITA','gspc_aat')
    stdPageFrame::Init()
    *set procedure to GSPC_MCO additive
    with this
      .Pages(1).addobject("oPag","tgspc_aatPag1","gspc_aat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Attivit�")
      .Pages(1).HelpContextID = 138830054
      .Pages(2).addobject("oPag","tgspc_aatPag2","gspc_aat",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Tempificazione")
      .Pages(2).HelpContextID = 13824681
      .Pages(3).addobject("oPag","tgspc_aatPag3","gspc_aat",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Note")
      .Pages(3).HelpContextID = 136160298
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATCODCOM_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSPC_MCO
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CENCOST'
    this.cWorkTables[3]='MA_COSTI'
    this.cWorkTables[4]='ATT_PREC'
    this.cWorkTables[5]='PC_FAMIG'
    this.cWorkTables[6]='CPAR_DEF'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='ATTIVITA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ATTIVITA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ATTIVITA_IDX,3]
  return

  function CreateChildren()
    this.GSPC_MCO = CREATEOBJECT('stdDynamicChild',this,'GSPC_MCO',this.oPgFrm.Page1.oPag.oLinkPC_1_19)
    this.GSPC_MCO.createrealchild()
    this.GSPC_MAP = CREATEOBJECT('stdDynamicChild',this,'GSPC_MAP',this.oPgFrm.Page2.oPag.oLinkPC_2_15)
    this.GSPC_MAP.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSPC_MCO)
      this.GSPC_MCO.DestroyChildrenChain()
      this.GSPC_MCO=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_19')
    if !ISNULL(this.GSPC_MAP)
      this.GSPC_MAP.DestroyChildrenChain()
      this.GSPC_MAP=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_15')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSPC_MCO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSPC_MAP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSPC_MCO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSPC_MAP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSPC_MCO.NewDocument()
    this.GSPC_MAP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSPC_MCO.SetKey(;
            .w_ATCODCOM,"CSCODCOM";
            ,.w_ATTIPATT,"CSTIPSTR";
            ,.w_ATCODATT,"CSCODMAT";
            )
      this.GSPC_MAP.SetKey(;
            .w_ATCODCOM,"MPCODCOM";
            ,.w_ATTIPATT,"MPTIPATT";
            ,.w_ATCODATT,"MPCODATT";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSPC_MCO.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATCODCOM,"CSCODCOM";
             ,.w_ATTIPATT,"CSTIPSTR";
             ,.w_ATCODATT,"CSCODMAT";
             )
      .GSPC_MAP.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATCODCOM,"MPCODCOM";
             ,.w_ATTIPATT,"MPTIPATT";
             ,.w_ATCODATT,"MPCODATT";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSPC_MCO)
        i_f=.GSPC_MCO.BuildFilter()
        if !(i_f==.GSPC_MCO.cQueryFilter)
          i_fnidx=.GSPC_MCO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSPC_MCO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSPC_MCO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSPC_MCO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSPC_MCO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSPC_MAP)
        i_f=.GSPC_MAP.BuildFilter()
        if !(i_f==.GSPC_MAP.cQueryFilter)
          i_fnidx=.GSPC_MAP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSPC_MAP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSPC_MAP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSPC_MAP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSPC_MAP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ATCODCOM = NVL(ATCODCOM,space(15))
      .w_ATTIPATT = NVL(ATTIPATT,space(1))
      .w_ATCODATT = NVL(ATCODATT,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_9_joined
    link_1_9_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ATTIVITA where ATCODCOM=KeySet.ATCODCOM
    *                            and ATTIPATT=KeySet.ATTIPATT
    *                            and ATCODATT=KeySet.ATCODATT
    *
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ATTIVITA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ATTIVITA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ATTIVITA '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ATCODCOM',this.w_ATCODCOM  ,'ATTIPATT',this.w_ATTIPATT  ,'ATCODATT',this.w_ATCODATT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_READPAR = 'TAM'
        .w_DEFCOM = space(15)
        .w_OBTEST = i_DATSYS
        .w_DESCOMG = space(30)
        .w_TIPOSTRU = 'P'
        .w_DESCOM = space(30)
        .w_DESPIA = space(40)
        .w_DESFAM = space(50)
        .w_READPAR = 'TAM'
        .w_MSPROJ = space(1)
        .w_PROGR = space(1)
        .w_DATINI = ctod("  /  /  ")
        .w_DATFIN = ctod("  /  /  ")
        .w_CNCODVAL = space(3)
        .w_DECIMALI = 0
          .link_1_1('Load')
        .w_ATCODCOM = NVL(ATCODCOM,space(15))
          if link_1_3_joined
            this.w_ATCODCOM = NVL(CNCODCAN103,NVL(this.w_ATCODCOM,space(15)))
            this.w_DESCOMG = NVL(CNDESCAN103,space(30))
            this.w_DESCOM = NVL(CNDESCAN103,space(30))
            this.w_DATINI = NVL(cp_ToDate(CNDATINI103),ctod("  /  /  "))
            this.w_DATFIN = NVL(cp_ToDate(CNDATFIN103),ctod("  /  /  "))
            this.w_CNCODVAL = NVL(CNCODVAL103,space(3))
          else
          .link_1_3('Load')
          endif
        .w_ATTIPATT = NVL(ATTIPATT,space(1))
        .w_ATCODATT = NVL(ATCODATT,space(15))
        .w_ATDESCRI = NVL(ATDESCRI,space(30))
        .w_ATCENCOS = NVL(ATCENCOS,space(15))
          if link_1_8_joined
            this.w_ATCENCOS = NVL(CC_CONTO108,NVL(this.w_ATCENCOS,space(15)))
            this.w_DESPIA = NVL(CCDESPIA108,space(40))
          else
          .link_1_8('Load')
          endif
        .w_ATCODFAM = NVL(ATCODFAM,space(5))
          if link_1_9_joined
            this.w_ATCODFAM = NVL(FACODICE109,NVL(this.w_ATCODFAM,space(5)))
            this.w_DESFAM = NVL(FADESCRI109,space(50))
          else
          .link_1_9('Load')
          endif
        .w_ATTIPCOM = NVL(ATTIPCOM,space(1))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_ATFLPREV = NVL(ATFLPREV,space(1))
        .w_AT_STATO = NVL(AT_STATO,space(1))
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .w_AT_PRJVW = NVL(AT_PRJVW,space(1))
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .w_STAT1 = .w_AT_STATO
          .link_2_1('Load')
        .w_ATDURGIO = NVL(ATDURGIO,0)
        .w_ATVINCOL = NVL(ATVINCOL,space(3))
        .w_ATDATVIN = NVL(cp_ToDate(ATDATVIN),ctod("  /  /  "))
        .w_ATDATINI = NVL(cp_ToDate(ATDATINI),ctod("  /  /  "))
        .w_ATDATFIN = NVL(cp_ToDate(ATDATFIN),ctod("  /  /  "))
        .w_ATPERCOM = NVL(ATPERCOM,0)
        .w_ATCARDIN = NVL(ATCARDIN,space(1))
        .w_ATDESSUP = NVL(ATDESSUP,space(0))
        .w_TIPO = 'P'
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
          .link_1_36('Load')
        cp_LoadRecExtFlds(this,'ATTIVITA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gspc_aat
    p_OLDCOMM=space(15)
    p_OLDSTATO=space(1)
    
    if p_OLDCOMM<>this.w_ATCODCOM
       p_OLDCOMM=this.w_ATCODCOM
    endif
    
    if p_OLDSTATO<>this.w_AT_STATO
       p_OLDSTATO=this.w_AT_STATO
    endif
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR = space(10)
      .w_DEFCOM = space(15)
      .w_ATCODCOM = space(15)
      .w_ATTIPATT = space(1)
      .w_ATCODATT = space(15)
      .w_ATDESCRI = space(30)
      .w_ATCENCOS = space(15)
      .w_ATCODFAM = space(5)
      .w_ATTIPCOM = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DESCOMG = space(30)
      .w_TIPOSTRU = space(1)
      .w_DESCOM = space(30)
      .w_DESPIA = space(40)
      .w_ATFLPREV = space(1)
      .w_AT_STATO = space(1)
      .w_DESFAM = space(50)
      .w_AT_PRJVW = space(1)
      .w_STAT1 = space(1)
      .w_READPAR = space(1)
      .w_MSPROJ = space(1)
      .w_PROGR = space(1)
      .w_ATDURGIO = 0
      .w_ATVINCOL = space(3)
      .w_ATDATVIN = ctod("  /  /  ")
      .w_ATDATINI = ctod("  /  /  ")
      .w_ATDATFIN = ctod("  /  /  ")
      .w_ATPERCOM = 0
      .w_ATCARDIN = space(1)
      .w_DATINI = ctod("  /  /  ")
      .w_DATFIN = ctod("  /  /  ")
      .w_ATDESSUP = space(0)
      .w_TIPO = space(10)
      .w_CNCODVAL = space(3)
      .w_DECIMALI = 0
      if .cFunction<>"Filter"
        .w_READPAR = 'TAM'
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_READPAR))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_ATCODCOM = IIF(p_OLDCOMM<>space(15), p_OLDCOMM, .w_DEFCOM)
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_ATCODCOM))
          .link_1_3('Full')
          endif
        .w_ATTIPATT = 'A'
        .DoRTCalc(5,7,.f.)
          if not(empty(.w_ATCENCOS))
          .link_1_8('Full')
          endif
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_ATCODFAM))
          .link_1_9('Full')
          endif
        .w_ATTIPCOM = 'A'
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
          .DoRTCalc(11,11,.f.)
        .w_TIPOSTRU = 'P'
          .DoRTCalc(13,14,.f.)
        .w_ATFLPREV = 'N'
        .w_AT_STATO = 'P'
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
          .DoRTCalc(17,17,.f.)
        .w_AT_PRJVW = 'S'
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .w_STAT1 = .w_AT_STATO
        .w_READPAR = 'TAM'
        .DoRTCalc(20,20,.f.)
          if not(empty(.w_READPAR))
          .link_2_1('Full')
          endif
          .DoRTCalc(21,22,.f.)
        .w_ATDURGIO = 1
        .w_ATVINCOL = IIF(EMPTY(.w_ATDATVIN) OR .w_AT_STATO<>'P', IIF(.w_PROGR='A', IIF(.w_AT_STATO='P', 'PPP', 'DII'), IIF(.w_AT_STATO='P', 'PTP', 'DFI')), .w_ATVINCOL)
        .w_ATDATVIN = IIF(.w_AT_STATO<>'P', IIF(.w_PROGR='A', .w_ATDATINI, .w_ATDATFIN), .w_ATDATVIN)
        .w_ATDATINI = iif(.w_PROGR='I',iif(empty(.w_ATDATFIN),nvl(.w_DATFIN,i_DATSYS),.w_ATDATFIN)-(.w_ATDURGIO-IIF(.w_ATDURGIO=0,0,1)),iif(empty(.w_ATDATINI),max(NVL(.w_DATINI,i_DATSYS),i_DATSYS),.w_ATDATINI))
        .w_ATDATFIN = iif(.w_PROGR='A',.w_ATDATINI+(.w_ATDURGIO-IIF(.w_ATDURGIO=0,0,1)),IIF(EMPTY(.w_ATDATFIN),NVL(.w_DATFIN,i_DATSYS),.w_ATDATFIN))
        .w_ATPERCOM = iif(.w_AT_STATO='F',100,.w_ATPERCOM)
        .w_ATCARDIN = 'N'
          .DoRTCalc(30,32,.f.)
        .w_TIPO = 'P'
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_CNCODVAL))
          .link_1_36('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'ATTIVITA')
    this.DoRTCalc(35,35,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oATCODCOM_1_3.enabled = i_bVal
      .Page1.oPag.oATCODATT_1_6.enabled = i_bVal
      .Page1.oPag.oATDESCRI_1_7.enabled = i_bVal
      .Page1.oPag.oATCENCOS_1_8.enabled = i_bVal
      .Page1.oPag.oATCODFAM_1_9.enabled = i_bVal
      .Page1.oPag.oAT_STATO_1_25.enabled = i_bVal
      .Page2.oPag.oATDURGIO_2_4.enabled = i_bVal
      .Page2.oPag.oATVINCOL_2_5.enabled = i_bVal
      .Page2.oPag.oATDATVIN_2_6.enabled = i_bVal
      .Page2.oPag.oATDATINI_2_7.enabled = i_bVal
      .Page2.oPag.oATDATFIN_2_8.enabled = i_bVal
      .Page2.oPag.oATPERCOM_2_9.enabled = i_bVal
      .Page2.oPag.oATCARDIN_2_10.enabled = i_bVal
      .Page3.oPag.oATDESSUP_3_1.enabled = i_bVal
      .Page1.oPag.oBtn_1_15.enabled = .Page1.oPag.oBtn_1_15.mCond()
      .Page1.oPag.oBtn_1_16.enabled = .Page1.oPag.oBtn_1_16.mCond()
      .Page1.oPag.oBtn_1_38.enabled = .Page1.oPag.oBtn_1_38.mCond()
      .Page1.oPag.oObj_1_14.enabled = i_bVal
      .Page1.oPag.oObj_1_29.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_32.enabled = i_bVal
      .Page1.oPag.oObj_1_35.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oATCODCOM_1_3.enabled = .f.
        .Page1.oPag.oATCODATT_1_6.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oATCODCOM_1_3.enabled = .t.
        .Page1.oPag.oATCODATT_1_6.enabled = .t.
        .Page1.oPag.oATDESCRI_1_7.enabled = .t.
        .Page1.oPag.oATCENCOS_1_8.enabled = .t.
      endif
    endwith
    this.GSPC_MCO.SetStatus(i_cOp)
    this.GSPC_MAP.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ATTIVITA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSPC_MCO.SetChildrenStatus(i_cOp)
  *  this.GSPC_MAP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODCOM,"ATCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPATT,"ATTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODATT,"ATCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDESCRI,"ATDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCENCOS,"ATCENCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODFAM,"ATCODFAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPCOM,"ATTIPCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFLPREV,"ATFLPREV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT_STATO,"AT_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT_PRJVW,"AT_PRJVW",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDURGIO,"ATDURGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATVINCOL,"ATVINCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATVIN,"ATDATVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATINI,"ATDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATFIN,"ATDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPERCOM,"ATPERCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCARDIN,"ATCARDIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDESSUP,"ATDESSUP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    i_lTable = "ATTIVITA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ATTIVITA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSPC_SAT with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ATTIVITA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ATTIVITA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ATTIVITA')
        i_extval=cp_InsertValODBCExtFlds(this,'ATTIVITA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS"+;
                  ",ATCODFAM,ATTIPCOM,ATFLPREV,AT_STATO,AT_PRJVW"+;
                  ",ATDURGIO,ATVINCOL,ATDATVIN,ATDATINI,ATDATFIN"+;
                  ",ATPERCOM,ATCARDIN,ATDESSUP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_ATCODCOM)+;
                  ","+cp_ToStrODBC(this.w_ATTIPATT)+;
                  ","+cp_ToStrODBC(this.w_ATCODATT)+;
                  ","+cp_ToStrODBC(this.w_ATDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_ATCENCOS)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODFAM)+;
                  ","+cp_ToStrODBC(this.w_ATTIPCOM)+;
                  ","+cp_ToStrODBC(this.w_ATFLPREV)+;
                  ","+cp_ToStrODBC(this.w_AT_STATO)+;
                  ","+cp_ToStrODBC(this.w_AT_PRJVW)+;
                  ","+cp_ToStrODBC(this.w_ATDURGIO)+;
                  ","+cp_ToStrODBC(this.w_ATVINCOL)+;
                  ","+cp_ToStrODBC(this.w_ATDATVIN)+;
                  ","+cp_ToStrODBC(this.w_ATDATINI)+;
                  ","+cp_ToStrODBC(this.w_ATDATFIN)+;
                  ","+cp_ToStrODBC(this.w_ATPERCOM)+;
                  ","+cp_ToStrODBC(this.w_ATCARDIN)+;
                  ","+cp_ToStrODBC(this.w_ATDESSUP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ATTIVITA')
        i_extval=cp_InsertValVFPExtFlds(this,'ATTIVITA')
        cp_CheckDeletedKey(i_cTable,0,'ATCODCOM',this.w_ATCODCOM,'ATTIPATT',this.w_ATTIPATT,'ATCODATT',this.w_ATCODATT)
        INSERT INTO (i_cTable);
              (ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,ATCODFAM,ATTIPCOM,ATFLPREV,AT_STATO,AT_PRJVW,ATDURGIO,ATVINCOL,ATDATVIN,ATDATINI,ATDATFIN,ATPERCOM,ATCARDIN,ATDESSUP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ATCODCOM;
                  ,this.w_ATTIPATT;
                  ,this.w_ATCODATT;
                  ,this.w_ATDESCRI;
                  ,this.w_ATCENCOS;
                  ,this.w_ATCODFAM;
                  ,this.w_ATTIPCOM;
                  ,this.w_ATFLPREV;
                  ,this.w_AT_STATO;
                  ,this.w_AT_PRJVW;
                  ,this.w_ATDURGIO;
                  ,this.w_ATVINCOL;
                  ,this.w_ATDATVIN;
                  ,this.w_ATDATINI;
                  ,this.w_ATDATFIN;
                  ,this.w_ATPERCOM;
                  ,this.w_ATCARDIN;
                  ,this.w_ATDESSUP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ATTIVITA_IDX,i_nConn)
      *
      * update ATTIVITA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ATTIVITA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ATDESCRI="+cp_ToStrODBC(this.w_ATDESCRI)+;
             ",ATCENCOS="+cp_ToStrODBCNull(this.w_ATCENCOS)+;
             ",ATCODFAM="+cp_ToStrODBCNull(this.w_ATCODFAM)+;
             ",ATTIPCOM="+cp_ToStrODBC(this.w_ATTIPCOM)+;
             ",ATFLPREV="+cp_ToStrODBC(this.w_ATFLPREV)+;
             ",AT_STATO="+cp_ToStrODBC(this.w_AT_STATO)+;
             ",AT_PRJVW="+cp_ToStrODBC(this.w_AT_PRJVW)+;
             ",ATDURGIO="+cp_ToStrODBC(this.w_ATDURGIO)+;
             ",ATVINCOL="+cp_ToStrODBC(this.w_ATVINCOL)+;
             ",ATDATVIN="+cp_ToStrODBC(this.w_ATDATVIN)+;
             ",ATDATINI="+cp_ToStrODBC(this.w_ATDATINI)+;
             ",ATDATFIN="+cp_ToStrODBC(this.w_ATDATFIN)+;
             ",ATPERCOM="+cp_ToStrODBC(this.w_ATPERCOM)+;
             ",ATCARDIN="+cp_ToStrODBC(this.w_ATCARDIN)+;
             ",ATDESSUP="+cp_ToStrODBC(this.w_ATDESSUP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ATTIVITA')
        i_cWhere = cp_PKFox(i_cTable  ,'ATCODCOM',this.w_ATCODCOM  ,'ATTIPATT',this.w_ATTIPATT  ,'ATCODATT',this.w_ATCODATT  )
        UPDATE (i_cTable) SET;
              ATDESCRI=this.w_ATDESCRI;
             ,ATCENCOS=this.w_ATCENCOS;
             ,ATCODFAM=this.w_ATCODFAM;
             ,ATTIPCOM=this.w_ATTIPCOM;
             ,ATFLPREV=this.w_ATFLPREV;
             ,AT_STATO=this.w_AT_STATO;
             ,AT_PRJVW=this.w_AT_PRJVW;
             ,ATDURGIO=this.w_ATDURGIO;
             ,ATVINCOL=this.w_ATVINCOL;
             ,ATDATVIN=this.w_ATDATVIN;
             ,ATDATINI=this.w_ATDATINI;
             ,ATDATFIN=this.w_ATDATFIN;
             ,ATPERCOM=this.w_ATPERCOM;
             ,ATCARDIN=this.w_ATCARDIN;
             ,ATDESSUP=this.w_ATDESSUP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSPC_MCO : Saving
      this.GSPC_MCO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATCODCOM,"CSCODCOM";
             ,this.w_ATTIPATT,"CSTIPSTR";
             ,this.w_ATCODATT,"CSCODMAT";
             )
      this.GSPC_MCO.mReplace()
      * --- GSPC_MAP : Saving
      this.GSPC_MAP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATCODCOM,"MPCODCOM";
             ,this.w_ATTIPATT,"MPTIPATT";
             ,this.w_ATCODATT,"MPCODATT";
             )
      this.GSPC_MAP.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSPC_MCO : Deleting
    this.GSPC_MCO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATCODCOM,"CSCODCOM";
           ,this.w_ATTIPATT,"CSTIPSTR";
           ,this.w_ATCODATT,"CSCODMAT";
           )
    this.GSPC_MCO.mDelete()
    * --- GSPC_MAP : Deleting
    this.GSPC_MAP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATCODCOM,"MPCODCOM";
           ,this.w_ATTIPATT,"MPTIPATT";
           ,this.w_ATCODATT,"MPCODATT";
           )
    this.GSPC_MAP.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ATTIVITA_IDX,i_nConn)
      *
      * delete ATTIVITA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ATCODCOM',this.w_ATCODCOM  ,'ATTIPATT',this.w_ATTIPATT  ,'ATCODATT',this.w_ATCODATT  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .DoRTCalc(2,18,.t.)
            .w_STAT1 = .w_AT_STATO
          .link_2_1('Full')
        .DoRTCalc(21,23,.t.)
        if .o_ATDATVIN<>.w_ATDATVIN.or. .o_AT_STATO<>.w_AT_STATO
            .w_ATVINCOL = IIF(EMPTY(.w_ATDATVIN) OR .w_AT_STATO<>'P', IIF(.w_PROGR='A', IIF(.w_AT_STATO='P', 'PPP', 'DII'), IIF(.w_AT_STATO='P', 'PTP', 'DFI')), .w_ATVINCOL)
        endif
        if .o_AT_STATO<>.w_AT_STATO
            .w_ATDATVIN = IIF(.w_AT_STATO<>'P', IIF(.w_PROGR='A', .w_ATDATINI, .w_ATDATFIN), .w_ATDATVIN)
        endif
        if .o_ATDURGIO<>.w_ATDURGIO.or. .o_ATDATFIN<>.w_ATDATFIN.or. .o_DATINI<>.w_DATINI
            .w_ATDATINI = iif(.w_PROGR='I',iif(empty(.w_ATDATFIN),nvl(.w_DATFIN,i_DATSYS),.w_ATDATFIN)-(.w_ATDURGIO-IIF(.w_ATDURGIO=0,0,1)),iif(empty(.w_ATDATINI),max(NVL(.w_DATINI,i_DATSYS),i_DATSYS),.w_ATDATINI))
        endif
        if .o_ATDURGIO<>.w_ATDURGIO.or. .o_ATDATINI<>.w_ATDATINI.or. .o_DATFIN<>.w_DATFIN.or. .o_ATCODCOM<>.w_ATCODCOM
            .w_ATDATFIN = iif(.w_PROGR='A',.w_ATDATINI+(.w_ATDURGIO-IIF(.w_ATDURGIO=0,0,1)),IIF(EMPTY(.w_ATDATFIN),NVL(.w_DATFIN,i_DATSYS),.w_ATDATFIN))
        endif
        if .o_AT_STATO<>.w_AT_STATO
            .w_ATPERCOM = iif(.w_AT_STATO='F',100,.w_ATPERCOM)
        endif
        .DoRTCalc(29,32,.t.)
            .w_TIPO = 'P'
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
          .link_1_36('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oATVINCOL_2_5.enabled = this.oPgFrm.Page2.oPag.oATVINCOL_2_5.mCond()
    this.oPgFrm.Page2.oPag.oATDATINI_2_7.enabled = this.oPgFrm.Page2.oPag.oATDATINI_2_7.mCond()
    this.oPgFrm.Page2.oPag.oATDATFIN_2_8.enabled = this.oPgFrm.Page2.oPag.oATDATFIN_2_8.mCond()
    this.oPgFrm.Page2.oPag.oATPERCOM_2_9.enabled = this.oPgFrm.Page2.oPag.oATPERCOM_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.GSPC_MCO.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oATVINCOL_2_5.visible=!this.oPgFrm.Page2.oPag.oATVINCOL_2_5.mHide()
    this.oPgFrm.Page2.oPag.oATDATVIN_2_6.visible=!this.oPgFrm.Page2.oPag.oATDATVIN_2_6.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_14.visible=!this.oPgFrm.Page2.oPag.oStr_2_14.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODCOM
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCODCOM))
          select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCODCOM_1_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCODCOM)
            select CNCODCAN,CNDESCAN,CNDATINI,CNDATFIN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMG = NVL(_Link_.CNDESCAN,space(30))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATINI = NVL(cp_ToDate(_Link_.CNDATINI),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
      this.w_CNCODVAL = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODCOM = space(15)
      endif
      this.w_DESCOMG = space(30)
      this.w_DESCOM = space(30)
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
      this.w_CNCODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.CNCODCAN as CNCODCAN103"+ ",link_1_3.CNDESCAN as CNDESCAN103"+ ",link_1_3.CNDESCAN as CNDESCAN103"+ ",link_1_3.CNDATINI as CNDATINI103"+ ",link_1_3.CNDATFIN as CNDATFIN103"+ ",link_1_3.CNCODVAL as CNCODVAL103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on ATTIVITA.ATCODCOM=link_1_3.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and ATTIVITA.ATCODCOM=link_1_3.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCENCOS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ATCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ATCENCOS))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oATCENCOS_1_8'),i_cWhere,'',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ATCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ATCENCOS)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ATCENCOS = space(15)
      endif
      this.w_DESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.CC_CONTO as CC_CONTO108"+ ",link_1_8.CCDESPIA as CCDESPIA108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on ATTIVITA.ATCENCOS=link_1_8.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and ATTIVITA.ATCENCOS=link_1_8.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCODFAM
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PC_FAMIG_IDX,3]
    i_lTable = "PC_FAMIG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PC_FAMIG_IDX,2], .t., this.PC_FAMIG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PC_FAMIG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_AFA',True,'PC_FAMIG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_ATCODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_ATCODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODFAM) and !this.bDontReportError
            deferred_cp_zoom('PC_FAMIG','*','FACODICE',cp_AbsName(oSource.parent,'oATCODFAM_1_9'),i_cWhere,'GSPC_AFA',"Elenco famiglie",'GSPC_FAM.PC_FAMIG_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_ATCODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_ATCODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(_Link_.FADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODFAM = space(5)
      endif
      this.w_DESFAM = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PC_FAMIG_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.PC_FAMIG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PC_FAMIG_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PC_FAMIG_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.FACODICE as FACODICE109"+ ",link_1_9.FADESCRI as FADESCRI109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on ATTIVITA.ATCODFAM=link_1_9.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and ATTIVITA.ATCODFAM=link_1_9.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=READPAR
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDPROGR,PDMSPROJ";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDPROGR,PDMSPROJ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(1))
      this.w_PROGR = NVL(_Link_.PDPROGR,space(1))
      this.w_MSPROJ = NVL(_Link_.PDMSPROJ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(1)
      endif
      this.w_PROGR = space(1)
      this.w_MSPROJ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNCODVAL
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CNCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CNCODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECIMALI = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CNCODVAL = space(3)
      endif
      this.w_DECIMALI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATCODCOM_1_3.value==this.w_ATCODCOM)
      this.oPgFrm.Page1.oPag.oATCODCOM_1_3.value=this.w_ATCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODATT_1_6.value==this.w_ATCODATT)
      this.oPgFrm.Page1.oPag.oATCODATT_1_6.value=this.w_ATCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATDESCRI_1_7.value==this.w_ATDESCRI)
      this.oPgFrm.Page1.oPag.oATDESCRI_1_7.value=this.w_ATDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oATCENCOS_1_8.value==this.w_ATCENCOS)
      this.oPgFrm.Page1.oPag.oATCENCOS_1_8.value=this.w_ATCENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODFAM_1_9.value==this.w_ATCODFAM)
      this.oPgFrm.Page1.oPag.oATCODFAM_1_9.value=this.w_ATCODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_21.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_21.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA_1_23.value==this.w_DESPIA)
      this.oPgFrm.Page1.oPag.oDESPIA_1_23.value=this.w_DESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oAT_STATO_1_25.RadioValue()==this.w_AT_STATO)
      this.oPgFrm.Page1.oPag.oAT_STATO_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_27.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_27.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page2.oPag.oATDURGIO_2_4.value==this.w_ATDURGIO)
      this.oPgFrm.Page2.oPag.oATDURGIO_2_4.value=this.w_ATDURGIO
    endif
    if not(this.oPgFrm.Page2.oPag.oATVINCOL_2_5.RadioValue()==this.w_ATVINCOL)
      this.oPgFrm.Page2.oPag.oATVINCOL_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATDATVIN_2_6.value==this.w_ATDATVIN)
      this.oPgFrm.Page2.oPag.oATDATVIN_2_6.value=this.w_ATDATVIN
    endif
    if not(this.oPgFrm.Page2.oPag.oATDATINI_2_7.value==this.w_ATDATINI)
      this.oPgFrm.Page2.oPag.oATDATINI_2_7.value=this.w_ATDATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oATDATFIN_2_8.value==this.w_ATDATFIN)
      this.oPgFrm.Page2.oPag.oATDATFIN_2_8.value=this.w_ATDATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oATPERCOM_2_9.value==this.w_ATPERCOM)
      this.oPgFrm.Page2.oPag.oATPERCOM_2_9.value=this.w_ATPERCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oATCARDIN_2_10.RadioValue()==this.w_ATCARDIN)
      this.oPgFrm.Page2.oPag.oATCARDIN_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oATDESSUP_3_1.value==this.w_ATDESSUP)
      this.oPgFrm.Page3.oPag.oATDESSUP_3_1.value=this.w_ATDESSUP
    endif
    cp_SetControlsValueExtFlds(this,'ATTIVITA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ATCODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODCOM_1_3.SetFocus()
            i_bnoObbl = !empty(.w_ATCODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATCODATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODATT_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ATCODATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ATDURGIO>0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATDURGIO_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La durata deve essere espressa con valori stretttamente positivi")
          case   not(NOT EMPTY(.w_ATDATVIN) OR .w_ATVINCOL $ 'PPP-PTP' OR .w_MSPROJ='N')  and not(.w_MSPROJ='N')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATDATVIN_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire la data vincolo")
          case   (empty(.w_ATDATINI))  and (.w_PROGR='A')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATDATINI_2_7.SetFocus()
            i_bnoObbl = !empty(.w_ATDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATDATFIN))  and (.w_PROGR='I')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATDATFIN_2_8.SetFocus()
            i_bnoObbl = !empty(.w_ATDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ATPERCOM>=0 And .w_ATPERCOM<=100)  and (.w_AT_STATO $ "ZLF")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATPERCOM_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La percentuale � espressa in centesimi (0..100)")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSPC_MCO.CheckForm()
      if i_bres
        i_bres=  .GSPC_MCO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSPC_MAP.CheckForm()
      if i_bres
        i_bres=  .GSPC_MAP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ATCODCOM = this.w_ATCODCOM
    this.o_AT_STATO = this.w_AT_STATO
    this.o_ATDURGIO = this.w_ATDURGIO
    this.o_ATDATVIN = this.w_ATDATVIN
    this.o_ATDATINI = this.w_ATDATINI
    this.o_ATDATFIN = this.w_ATDATFIN
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    * --- GSPC_MCO : Depends On
    this.GSPC_MCO.SaveDependsOn()
    * --- GSPC_MAP : Depends On
    this.GSPC_MAP.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgspc_aatPag1 as StdContainer
  Width  = 713
  height = 306
  stdWidth  = 713
  stdheight = 306
  resizeXpos=359
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATCODCOM_1_3 as StdField with uid="IEBRLGPCNL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ATCODCOM", cQueryName = "ATCODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 16175021,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=86, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCODCOM"

  func oATCODCOM_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODCOM_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODCOM_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCODCOM_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oATCODATT_1_6 as StdField with uid="CSKEMKPDCT",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ATCODATT", cQueryName = "ATCODCOM,ATTIPATT,ATCODATT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 218706010,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=86, Top=43, InputMask=replicate('X',15)

  add object oATDESCRI_1_7 as StdField with uid="VVVSOHCDAC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ATDESCRI", cQueryName = "ATDESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione breve utilizzata per ricerche per contenuto",;
    HelpContextID = 267337807,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=310, Top=43, InputMask=replicate('X',30)

  add object oATCENCOS_1_8 as StdField with uid="PIYMLJSVRU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ATCENCOS", cQueryName = "ATCENCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 6344615,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=86, Top=95, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ATCENCOS"

  func oATCENCOS_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCENCOS_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCENCOS_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oATCENCOS_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Centri di costo",'',this.parent.oContained
  endproc

  add object oATCODFAM_1_9 as StdField with uid="HGWZWXEXQC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ATCODFAM", cQueryName = "ATCODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia attivit�",;
    HelpContextID = 34156627,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=462, Top=95, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PC_FAMIG", cZoomOnZoom="GSPC_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_ATCODFAM"

  func oATCODFAM_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODFAM_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODFAM_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PC_FAMIG','*','FACODICE',cp_AbsName(this.parent,'oATCODFAM_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_AFA',"Elenco famiglie",'GSPC_FAM.PC_FAMIG_VZM',this.parent.oContained
  endproc
  proc oATCODFAM_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSPC_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_ATCODFAM
     i_obj.ecpSave()
  endproc


  add object oObj_1_14 as cp_runprogram with uid="ORVJOVQTCZ",left=107, top=344, width=347,height=19,;
    caption='GSPC_BAT',;
   bGlobalFont=.t.,;
    prg='GSPC_BAT("SON")',;
    cEvent = "w_ATCODCOM Changed,w_ATCODATT Changed,Load",;
    nPag=1;
    , ToolTipText = "Reimpie i campi del figlio";
    , HelpContextID = 5374022


  add object oBtn_1_15 as StdButton with uid="DZCGOJDBMC",left=616, top=43, width=48,height=45,;
    CpPicture="BMP\SPESE.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza movimento preventivo associato";
    , HelpContextID = 205710783;
    , tabstop=.f.,Caption='\<Preventivo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        do GSPC_BMM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_ATCODCOM) And Not Empty(.w_ATCODATT) And .cFunction<>'Load')
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="XJHZEGOQXG",left=566, top=43, width=48,height=45,;
    CpPicture="BMP\CONTO.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza mastrino dei movimenti associati";
    , HelpContextID = 135347654;
    , tabstop=.f., Caption='\<Mastri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSPC_BIM(this.Parent.oContained,"Attivita")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_ATCODCOM) AND Not Empty(.w_ATCODATT) And .cFunction<>'Load')
      endwith
    endif
  endfunc


  add object oLinkPC_1_19 as stdDynamicChildContainer with uid="VGLKNRTZIF",left=4, top=133, width=660, height=167, bOnScreen=.t.;


  func oLinkPC_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (1=0)
      endwith
    endif
  endfunc

  add object oDESCOM_1_21 as StdField with uid="BBAISMDSEA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 106028746,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=221, Top=9, InputMask=replicate('X',30)

  add object oDESPIA_1_23 as StdField with uid="PQWTADSXSX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESPIA", cQueryName = "DESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 44359370,;
   bGlobalFont=.t.,;
    Height=21, Width=185, Left=215, Top=95, InputMask=replicate('X',40)


  add object oAT_STATO_1_25 as StdCombo with uid="NDFGNALVED",rtseq=16,rtrep=.f.,left=501,top=9,width=101,height=21;
    , TabStop=.f.;
    , ToolTipText = "Stato dell'attivit�";
    , HelpContextID = 235860053;
    , cFormVar="w_AT_STATO",RowSource=""+"Provvisoria,"+"Confermata,"+"Pianificata,"+"Lanciata,"+"Completata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAT_STATO_1_25.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'L',;
    iif(this.value =5,'F',;
    space(1)))))))
  endfunc
  func oAT_STATO_1_25.GetRadio()
    this.Parent.oContained.w_AT_STATO = this.RadioValue()
    return .t.
  endfunc

  func oAT_STATO_1_25.SetRadio()
    this.Parent.oContained.w_AT_STATO=trim(this.Parent.oContained.w_AT_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_AT_STATO=='P',1,;
      iif(this.Parent.oContained.w_AT_STATO=='C',2,;
      iif(this.Parent.oContained.w_AT_STATO=='Z',3,;
      iif(this.Parent.oContained.w_AT_STATO=='L',4,;
      iif(this.Parent.oContained.w_AT_STATO=='F',5,;
      0)))))
  endfunc

  add object oDESFAM_1_27 as StdField with uid="ALBIZJSSAM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 120512202,;
   bGlobalFont=.t.,;
    Height=21, Width=147, Left=517, Top=95, InputMask=replicate('X',50)


  add object oObj_1_29 as cp_runprogram with uid="UIKJIJGZQE",left=456, top=344, width=186,height=19,;
    caption='GSPC_BAT',;
   bGlobalFont=.t.,;
    prg='GSPC_BAT("CHECK")',;
    cEvent = "Update start,Delete start",;
    nPag=1;
    , HelpContextID = 5374022


  add object oObj_1_31 as cp_runprogram with uid="YFANYJOAJZ",left=107, top=365, width=199,height=17,;
    caption='GSPC_BTS',;
   bGlobalFont=.t.,;
    prg="GSPC_BTS('P')",;
    cEvent = "w_ATPERCOM Changed",;
    nPag=1;
    , HelpContextID = 263061433


  add object oObj_1_32 as cp_runprogram with uid="MRHSHELGZH",left=309, top=365, width=188,height=19,;
    caption='GSPC_BTS',;
   bGlobalFont=.t.,;
    prg="GSPC_BTS('S')",;
    cEvent = "w_AT_STATO Changed",;
    nPag=1;
    , HelpContextID = 263061433


  add object oObj_1_35 as cp_runprogram with uid="CYJMQZNOUN",left=107, top=385, width=202,height=19,;
    caption='GSPC_BTA',;
   bGlobalFont=.t.,;
    prg="GSPC_BTA",;
    cEvent = "Delete start",;
    nPag=1;
    , ToolTipText = "Verifica se l'attivit� � utilizzata nei documenti";
    , HelpContextID = 263061415


  add object oBtn_1_38 as StdButton with uid="WJLXDVBPBJ",left=665, top=43, width=48,height=45,;
    CpPicture="bmp\legami.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il movimento preventivo associato";
    , HelpContextID = 138830054;
    , tabstop=.f.,Caption='\<Attivit�.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        do GSPC_KDA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_ATCODCOM) And Not Empty(.w_ATCODATT) And .cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_1_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_AGEN<>'S')
     endwith
    endif
  endfunc

  add object oStr_1_4 as StdString with uid="UCJWMVRMQR",Visible=.t., Left=8, Top=10,;
    Alignment=1, Width=75, Height=15,;
    Caption="Commessa:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="MAPSQIHEXA",Visible=.t., Left=8, Top=43,;
    Alignment=1, Width=75, Height=15,;
    Caption="Attivit�:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="OYHRZFVIRE",Visible=.t., Left=223, Top=43,;
    Alignment=1, Width=84, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PIXHFAJUHM",Visible=.t., Left=6, Top=115,;
    Alignment=0, Width=129, Height=15,;
    Caption="Costi:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="PSOWWJTWIE",Visible=.t., Left=8, Top=95,;
    Alignment=1, Width=75, Height=15,;
    Caption="C/Costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="BYHQCNNIWY",Visible=.t., Left=445, Top=9,;
    Alignment=1, Width=51, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="KLDXLANRCC",Visible=.t., Left=403, Top=95,;
    Alignment=1, Width=58, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.
enddefine
define class tgspc_aatPag2 as StdContainer
  Width  = 713
  height = 306
  stdWidth  = 713
  stdheight = 306
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATDURGIO_2_4 as StdField with uid="FJXWESXVMX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ATDURGIO", cQueryName = "ATDURGIO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La durata deve essere espressa con valori stretttamente positivi",;
    ToolTipText = "Durata in giorni dell'attivit�",;
    HelpContextID = 202424235,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=101, Top=39, cSayPict='"999999"', cGetPict='"999999"'

  func oATDURGIO_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ATDURGIO>0)
    endwith
    return bRes
  endfunc


  add object oATVINCOL_2_5 as StdCombo with uid="HQFZUKWRMC",rtseq=24,rtrep=.f.,left=260,top=39,width=139,height=21;
    , ToolTipText = "Tipo di vincoli";
    , HelpContextID = 6004654;
    , cFormVar="w_ATVINCOL",RowSource=""+"Deve finire il,"+"Deve iniziare il,"+"Finire non oltre il,"+"Finire non prima,"+"Pi� presto possibile,"+"Pi� tardi possibile,"+"Iniziare non oltre,"+"Iniziare non prima", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATVINCOL_2_5.RadioValue()
    return(iif(this.value =1,'DFI',;
    iif(this.value =2,'DII',;
    iif(this.value =3,'FNO',;
    iif(this.value =4,'FNP',;
    iif(this.value =5,'PPP',;
    iif(this.value =6,'PTP',;
    iif(this.value =7,'INO',;
    iif(this.value =8,'INP',;
    space(3))))))))))
  endfunc
  func oATVINCOL_2_5.GetRadio()
    this.Parent.oContained.w_ATVINCOL = this.RadioValue()
    return .t.
  endfunc

  func oATVINCOL_2_5.SetRadio()
    this.Parent.oContained.w_ATVINCOL=trim(this.Parent.oContained.w_ATVINCOL)
    this.value = ;
      iif(this.Parent.oContained.w_ATVINCOL=='DFI',1,;
      iif(this.Parent.oContained.w_ATVINCOL=='DII',2,;
      iif(this.Parent.oContained.w_ATVINCOL=='FNO',3,;
      iif(this.Parent.oContained.w_ATVINCOL=='FNP',4,;
      iif(this.Parent.oContained.w_ATVINCOL=='PPP',5,;
      iif(this.Parent.oContained.w_ATVINCOL=='PTP',6,;
      iif(this.Parent.oContained.w_ATVINCOL=='INO',7,;
      iif(this.Parent.oContained.w_ATVINCOL=='INP',8,;
      0))))))))
  endfunc

  func oATVINCOL_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AT_STATO='P')
    endwith
   endif
  endfunc

  func oATVINCOL_2_5.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N')
    endwith
  endfunc

  add object oATDATVIN_2_6 as StdField with uid="SRKPLYPFFX",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ATDATVIN", cQueryName = "ATDATVIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire la data vincolo",;
    ToolTipText = "Data vincolo",;
    HelpContextID = 218415020,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=409, Top=39

  func oATDATVIN_2_6.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N')
    endwith
  endfunc

  func oATDATVIN_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT EMPTY(.w_ATDATVIN) OR .w_ATVINCOL $ 'PPP-PTP' OR .w_MSPROJ='N')
    endwith
    return bRes
  endfunc

  add object oATDATINI_2_7 as StdField with uid="VOEBBNCEOZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ATDATINI", cQueryName = "ATDATINI",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio",;
    HelpContextID = 168083377,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=101, Top=72

  func oATDATINI_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PROGR='A')
    endwith
   endif
  endfunc

  add object oATDATFIN_2_8 as StdField with uid="WNFARBAWKE",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ATDATFIN", cQueryName = "ATDATFIN",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit�, calcolata come somma della data inizio e della durata",;
    HelpContextID = 218415020,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=260, Top=72

  func oATDATFIN_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PROGR='I')
    endwith
   endif
  endfunc

  add object oATPERCOM_2_9 as StdField with uid="HWJESEOPNG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ATPERCOM", cQueryName = "ATPERCOM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La percentuale � espressa in centesimi (0..100)",;
    ToolTipText = "Percentuale completamento",;
    HelpContextID = 2097069,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=455, Top=72, cSayPict='"999"', cGetPict='"999"'

  func oATPERCOM_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AT_STATO $ "ZLF")
    endwith
   endif
  endfunc

  func oATPERCOM_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ATPERCOM>=0 And .w_ATPERCOM<=100)
    endwith
    return bRes
  endfunc

  add object oATCARDIN_2_10 as StdCheck with uid="KVGSUZZZZG",rtseq=29,rtrep=.f.,left=538, top=39, caption="Attivit� cardine",;
    ToolTipText = "Attivit� cardine (MS Project)",;
    HelpContextID = 254070700,;
    cFormVar="w_ATCARDIN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATCARDIN_2_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATCARDIN_2_10.GetRadio()
    this.Parent.oContained.w_ATCARDIN = this.RadioValue()
    return .t.
  endfunc

  func oATCARDIN_2_10.SetRadio()
    this.Parent.oContained.w_ATCARDIN=trim(this.Parent.oContained.w_ATCARDIN)
    this.value = ;
      iif(this.Parent.oContained.w_ATCARDIN=='S',1,;
      0)
  endfunc


  add object oLinkPC_2_15 as stdDynamicChildContainer with uid="QXZXUNVKEJ",left=50, top=142, width=560, height=121, bOnScreen=.t.;


  add object oStr_2_11 as StdString with uid="GANCHUFLMH",Visible=.t., Left=9, Top=72,;
    Alignment=1, Width=89, Height=15,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="YTHPUAQAGH",Visible=.t., Left=185, Top=72,;
    Alignment=1, Width=71, Height=15,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="YXGQBQVWKM",Visible=.t., Left=9, Top=39,;
    Alignment=1, Width=89, Height=15,;
    Caption="Durata:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="UBDJQKWCSR",Visible=.t., Left=176, Top=39,;
    Alignment=1, Width=79, Height=15,;
    Caption="Vincoli:"  ;
  , bGlobalFont=.t.

  func oStr_2_14.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N')
    endwith
  endfunc

  add object oStr_2_16 as StdString with uid="YWADYTSZQC",Visible=.t., Left=10, Top=122,;
    Alignment=0, Width=189, Height=15,;
    Caption="Attivit� precedenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_18 as StdString with uid="CKQPHKCARY",Visible=.t., Left=7, Top=10,;
    Alignment=0, Width=165, Height=15,;
    Caption="Dati gestionali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_19 as StdString with uid="NPFKHESWNO",Visible=.t., Left=351, Top=69,;
    Alignment=1, Width=100, Height=18,;
    Caption="% Completam.:"  ;
  , bGlobalFont=.t.

  add object oBox_2_17 as StdBox with uid="HDYPDBFWSI",left=5, top=26, width=656,height=2
enddefine
define class tgspc_aatPag3 as StdContainer
  Width  = 713
  height = 306
  stdWidth  = 713
  stdheight = 306
  resizeXpos=510
  resizeYpos=212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATDESSUP_3_1 as StdMemo with uid="ULPXTKAZVA",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ATDESSUP", cQueryName = "ATDESSUP",;
    bObbl = .f. , nPag = 3, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 267337814,;
   bGlobalFont=.t.,;
    Height=258, Width=660, Left=3, Top=23

  add object oStr_3_2 as StdString with uid="AUHJVXTLLO",Visible=.t., Left=3, Top=5,;
    Alignment=0, Width=123, Height=15,;
    Caption="Note:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="ATTIPATT='A'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".ATCODCOM=ATTIVITA.ATCODCOM";
  +" and "+i_cAliasName+".ATTIPATT=ATTIVITA.ATTIPATT";
  +" and "+i_cAliasName+".ATCODATT=ATTIVITA.ATCODATT";
  +")"
  endif
  i_res=cp_AppQueryFilter('gspc_aat','ATTIVITA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ATCODCOM=ATTIVITA.ATCODCOM";
  +" and "+i_cAliasName2+".ATTIPATT=ATTIVITA.ATTIPATT";
  +" and "+i_cAliasName2+".ATCODATT=ATTIVITA.ATCODATT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
